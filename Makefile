export LC_CTYPE="en_US.UTF-8"

.DEFAULT_GOAL := help
.PHONY: build docs migrate

migrate:
	@cd base && python manage.py migrate

webpack:
	npm install
	cd static/travel_product/v1 && npm install && node ../../../node_modules/webpack/bin/webpack.js
	cd static/motor_product && node ../../node_modules/webpack/bin/webpack.js
	cd static/js && node ../../node_modules/webpack/bin/webpack.js
	cd static/js/motor && node ../../../node_modules/webpack/bin/webpack.js
	cd static/health_product && ./deploy.sh
	cd static/motor_product && npm install && node ../../node_modules/webpack/bin/webpack.js

deps:
	echo read http://amitu.com/python/basic-project-setup/ first
	pip install -r requirements.txt
	make migrate
	make webpack
	echo read http://amitu.com/python/basic-project-setup/ first

run:
	cd base && PYTHONIOENCODING=UTF-8 PYTHONUNBUFFERED=TRUE honcho -f procfile start redis celery django

nb:
	cd base && python manage.py shell_ipynb

run-celery:
	cd base && celery worker -A celery_app -l info

# For run tests, 'citext' and 'hstore' extensions must be installed on 'template1' database
create-postgres-extensions:
	psql -U root -d template1 -c 'create extension citext;'
	psql -U root -d template1 -c 'create extension hstore;'

create-live-test-db:
	createdb -U root -T coverfox test_coverfox

drop-live-test-db:
	dropdb -U root test_coverfox

load-motor-data:
	pg_dump -t motor_product_insurer coverfox | psql test_coverfox
	pg_dump -t motor_product_make coverfox | psql test_coverfox
	pg_dump -t motor_product_model coverfox | psql test_coverfox
	pg_dump -t motor_product_vehicle coverfox | psql test_coverfox
	pg_dump -t motor_product_vehiclemaster coverfox | psql test_coverfox
	pg_dump -t motor_product_vehiclemapping coverfox | psql test_coverfox
	pg_dump -t motor_product_rtoinsurer coverfox | psql test_coverfox
	pg_dump -t motor_product_rtomaster coverfox | psql test_coverfox
	pg_dump -t motor_product_rtomapping coverfox | psql test_coverfox

test:
	make test-account-manager
	make test-activity
	make test-alfred
	make test-apis
	make test-app-notification
	make test-bakar
	make test-blog
	make test-brands
	make test-campaign
	make test-cfadmin
	make test-cms
	make test-core
	make test-coverfox
	make test-crm
	make test-eb
	make test-health
	make test-home
	make test-hrms
	make test-kb
	make test-leads
	make test-leaflet-campaign
	make test-mailer
	make test-motor
	make test-opsapp
	make test-payment
	make test-rdesk
	make test-review
	make test-seo
	make test-seocms
	make test-statemachine
	make test-travel
	make test-wiki
	make test-humanlly

test-with-live-db:
	make drop-live-test-db && make create-live-test-db && make test

test-with-live-db-and-migrations:
	make drop-live-test-db && make create-live-test-db && make migrate && make test

test-account-manager:
	@cd base && python manage.py test account_manager --keepdb --nomigrations

test-activity:
	@cd base && python manage.py test activity --keepdb --nomigrations

test-alfred:
	@cd base && python manage.py test alfred --keepdb --nomigrations

test-apis:
	@cd base && python manage.py test apis --keepdb --nomigrations

test-app-notification:
	@cd base && python manage.py test app_notification --keepdb --nomigrations

test-bakar:
	@cd base && python manage.py test bakar --keepdb --nomigrations

test-blog:
	@cd base && python manage.py test blog --keepdb --nomigrations

test-brands:
	@cd base && python manage.py test brands --keepdb --nomigrations

test-campaign:
	@cd base && python manage.py test campaign --keepdb --nomigrations

test-cfadmin:
	@cd base && python manage.py test cfadmin --keepdb --nomigrations

test-cms:
	@cd base && python manage.py test cms --keepdb --nomigrations

test-core:
	@cd base && python manage.py test core --keepdb --nomigrations

test-coverfox:
	@cd base && python manage.py test coverfox --keepdb --nomigrations

test-crm:
	@cd base && python manage.py test crm --keepdb --nomigrations

test-eb:
	@cd base && python manage.py test eb --keepdb --nomigrations

test-health:
	@cd base && py.test tests/health

test-home:
	@cd base && python manage.py test home --keepdb --nomigrations

test-hrms:
	@cd base && python manage.py test hrms --keepdb --nomigrations

test-kb:
	@cd base && python manage.py test kb --keepdb --nomigrations

test-leads:
	@cd base && python manage.py test leads --keepdb --nomigrations

test-leaflet-campaign: ## Run leaflet_campaign tests
	@cd base && py.test tests/leaflet_campaign

test-mailer:
	@cd base && python manage.py test mailer --keepdb --nomigrations

test-motor:  ## Run motor_product tests
	@cd base && py.test tests/motor

test-opsapp:
	@cd base && python manage.py test opsapp --keepdb --nomigrations

test-payment:
	@cd base && python manage.py test payment --keepdb --nomigrations

test-rdesk:
	@cd base && python manage.py test rdesk --keepdb --nomigrations

test-review:
	@cd base && python manage.py test review --keepdb --nomigrations

test-seo:
	@cd base && python manage.py test seo --keepdb --nomigrations

test-seocms:
	@cd base && python manage.py test seocms --keepdb --nomigrations

test-statemachine:
	@cd base && python manage.py test statemachine --keepdb --nomigrations

test-travel:
	@cd base && python manage.py test travel_product --keepdb --nomigrations

test-wiki:
	@cd base && python manage.py test wiki --keepdb --nomigrations

test-humanlly:
	@cd humanlly.com && python humanlly.py test hrms --keepdb

dbshell:
	cd base && python manage.py dbshell

shell:
	cd base && python manage.py shell_plus

humanlly-shell:
	cd humanlly.com && python humanlly.py shell

pre:
	git add -u && pre-commit
	@echo "If there are new files, 'git add' them and run 'make pre' again."

build:
	sh ./build.sh

run-ops:
	cd ops.coverfox.com && make run

run-humanlly:
	cd humanlly.com && make run

run-ebenefits:
	cd ebenefits.coverfox.com && make run

run-jarvis:
	cd lms && honcho -f procfile start

docs:
	cd docs && make html

help: ## Show this help
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

test-address-bifurcation:
	cd base && py.test tests/motor/views/test_address_bifurcation.py --ds=settings --nomigrations --reuse-db
	
test-motor-caching:
	cd base && py.test tests/motor/utils/test_cache.py --ds=settings --nomigrations --reuse-db

