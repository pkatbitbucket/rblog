*rblog* is repository which servers coverfox.com website.
This repository also server `jarvis.coverfox.com`, `ops.coverfox.com`, `humanly.coverfox.com` and `ebenefits.coverfox.com`.

# Steps to setup rblog

#### Follow steps specified in below url.
http://amitu.com/python/basic-project-setup/

#### Install node.js
https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-an-ubuntu-14-04-server

### System Deps
## Ubuntu
sudo apt-get install libpython2.7-dev
sudo apt-get install libpq-dev

sudo apt-get install nodejs
sudo ln -s /usr/bin/nodejs /usr/bin/node

sudo apt-get install libncurses5-dev

sudo apt-get install libxml2-dev libxslt1-dev python-dev

## MacOSX
- Install Homebrew

#### Install redis
`sudo apt-get install redis-server`

#### Install postgresql db and pgadmin tool. pgadmin is optional.
- `pgadmin` is gui tool for postgsql database.
- Installation guide on url
http://hitulmistry.blogspot.in/2015/09/install-postgresql.html

- Create database into postgresql. Database name must be "coverfox".
- Create database user "root", with no password.
User and Database creation tutorial at url https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-14-04
- After installing postgresql also install `postgresql-contrib` for Hstore support.
- `sudo apt-get install postgresql-contrib`

#### Restore database dump to postgresql
- Get database dump from url
- Execute command `pg_restore -O -d ​<dbname> <database-dump-file-path>`

#### Clone rblog repository
`git clone git@github.com:Coverfox/rblog.git `

#### Do installations
- change directory to rblog repository directory and execute `make`.

#### Start celery
`make run-celery`

#### Setup pre-commit
- Before every commit run make pre. It will trim trailing whitespaces, check for merge conflicts, check json, Tell about debug Statements in python, check for added large files and coading guidelines.
- Precommit installation guidelines is on url http://pre-commit.com/
- To run pre-commit execute
- `make pre`

``

#### Note
After installing requirements from requirements.txt always execute below command to remove dependencies which are not in requirements.txt.
- `pip freeze | grep -v -f requirements.txt - | xargs pip uninstall -y`
