from django.apps.config import AppConfig

default_app_config = 'account_manager.AccountManagerConfig'


class AccountManagerConfig(AppConfig):
    name = 'account_manager'

    def ready(self):
        from .signals import handlers  # NOQA
