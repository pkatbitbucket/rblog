from django.contrib import admin
from account_manager.models import *


class RelationshipManagerAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'mobile', 'is_active']


class UserAccountAdmin(admin.ModelAdmin):
    search_fields = ['user__emails__email', 'user__phones__number']
    list_display = ['get_user_fullname', 'get_user_email', 'mobile']

    def get_user_email(self, obj):
        return obj.user.email
    get_user_email.admin_order_field = 'user'  # Allows column order sorting
    get_user_email.short_description = 'Email'  # Renames column head

    def get_user_fullname(self, obj):
        return obj.user.get_full_name()
    get_user_fullname.admin_order_field = 'user'  # Allows column order sorting
    get_user_fullname.short_description = 'Full name'  # Renames column head

admin.site.register(RelationshipManager, RelationshipManagerAdmin)
admin.site.register(UserAccount, UserAccountAdmin)
