from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator


alpha = RegexValidator(r'^[a-zA-Z ]+$', 'Only alphanumeric characters are allowed.')
numeric = RegexValidator(r'^[0-9]+$', 'Only numeric characters are allowed.')

class UpdateUserAccount(forms.Form):
    first_name = forms.CharField(max_length=50, validators=[alpha])
    middle_name = forms.CharField(max_length=50, required=False, validators=[alpha])
    last_name = forms.CharField(max_length=50, required=False, validators=[alpha])
    mobile = forms.CharField(max_length=10, required=False, validators=[numeric])
    address_line1 = forms.CharField(max_length=100, required=False)
    address_line2 = forms.CharField(max_length=100, required=False)
    address_landmark = forms.CharField(max_length=100, required=False, validators=[alpha])
    city = forms.CharField(max_length=100, validators=[alpha])
    state = forms.CharField(max_length=100, required=False, validators=[alpha])
    country = forms.CharField(max_length=100, required=False, validators=[alpha])
    pincode = forms.CharField(max_length=6, validators=[numeric])

    # def clean(self, *args, **kwargs):
    #     super(UpdateUserAccount, self).clean(*args, **kwargs)
    #     for key, value in self.cleaned_data.items():
    #         if not value:
    #             del self.cleaned_data[key]
    #     return self.cleaned_data


class CreateUser(forms.Form):
    first_name = forms.CharField(required=True, max_length=50, validators=[alpha])
    email = forms.EmailField(required=True)
    password = forms.CharField(required=True, max_length=20, min_length=6)