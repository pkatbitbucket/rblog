from django.conf.urls import url

from . import views as api_views

urlpatterns = [
    # For Mobile app
    url(r'^profile/$', api_views.profile, name='profile'),
    url(r'^update-profile/$', api_views.update_profile, name='update_profile'),
    url(r'^policy/$', api_views.policy, name='policy'),
    url(r'^policy/(?P<type>[a-zA-Z]+)/$', api_views.policy, name='policy'),
    url(r'^create/$', api_views.create_user, name='create_user'),

    # For LMS
    url(r'^add_offline_transaction/$', api_views.add_offline_transaction, name='add_offline_transaction'),
    url(r'^get_transactions/$', api_views.get_transactions, name='get_transactions'),
    url(r'^update_transaction/$', api_views.update_transaction, name='update_transaction'),
]
