import base64
import hmac
import json
from datetime import datetime
from hashlib import sha1

from django.apps import apps
from django.conf import settings
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Q
from django.http import HttpResponse, HttpResponseNotAllowed
from django.views.decorators.csrf import csrf_exempt
from oauth2_provider.decorators import protected_resource

from account_manager.apis.forms import CreateUser, UpdateUserAccount
from account_manager.models import (OfflineTransaction, UserAccount,
                                    UserTransaction, get_health_policy,
                                    get_motor_policy, get_travel_policy,
                                    get_user_details)
from account_manager.utils import (get_health_transactions,
                                   get_motor_transactions,
                                   get_travel_transactions)
from apis.decorators import authenticate_api
from core.models import User, Email
from utils import get_property


# Exact function to share with others to generate headers
def get_headers(path):
    request_time = datetime.utcnow()
    string_to_sign = "%s %s" % (path, request_time.strftime('%Y-%m-%dT%H:%M:%SZ'))
    h = hmac.HMAC(settings.API_AUTHENTICATION_KEY, string_to_sign, sha1)
    signature = base64.b64encode(h.digest())
    headers = {
        'Authorization': 'CVFX ' + settings.API_AUTHENTICATION_ID + ':' + signature,
        'Date': request_time.strftime('%Y-%m-%dT%H:%M:%SZ'),
    }
    return headers


@csrf_exempt
@protected_resource()
def profile(request):
    ret = {
        'data': {},
        'errorCode': None,
        'errorItem': [],
        'errorMessage': None,
        'success': False,
    }

    try:
        user = request.user
        data = get_user_details(user)
        ret['data']['profile'] = data

        ret['data']['rm'] = {
            'name': getattr(getattr(getattr(user, 'urm', None), 'rm', None), 'name', None),
            'email': getattr(getattr(getattr(user, 'urm', None), 'rm', None), 'email', None),
            'mobile': getattr(getattr(getattr(user, 'urm', None), 'rm', None), 'mobile', None),
            'photo': user.urm.rm.photo.url if getattr(getattr(getattr(user, 'urm', None), 'rm', None), 'photo', None) else None,
        }

        ret['success'] = True
    except User.DoesNotExist:
        ret['errorMessage'] = "User does not exist"

    return HttpResponse(json.dumps(ret))


@csrf_exempt
@protected_resource()
def update_profile(request):
    ret = {
        'data': {},
        'errorCode': None,
        'errorItem': [],
        'errorMessage': None,
        'success': False,
    }

    if request.body:
        try:
            user = User.objects.get_user(email=request.user.email, phone='')
            try:
                form = UpdateUserAccount(json.loads(request.body))
                if form.is_valid():
                    # update User
                    user_properties = ['first_name',
                                       'last_name', 'middle_name']
                    update = False
                    for prop in user_properties:
                        if prop in form.cleaned_data:
                            update = True
                            setattr(user, prop, form.cleaned_data[prop])
                    if update:
                        user.save()

                    # update UserAccount
                    useraccount, _ = UserAccount.objects.get_or_create(
                        user=user)
                    useraccount_properties = ['mobile', 'address_line1', 'address_line2',
                                              'address_landmark', 'city', 'state', 'country', 'pincode']
                    update = False
                    for prop in useraccount_properties:
                        if prop in form.cleaned_data:
                            update = True
                            setattr(useraccount, prop, form.cleaned_data[prop])
                    if update:
                        useraccount.save()

                    ret['errorMessage'] = "User details updated successfully"
                    ret['success'] = True
                else:
                    ret['errorItem'] = form.errors
                    ret['errorMessage'] = "Some values might not be correct. Please refer fields and their datatypes"
            except ValueError:
                ret['errorMessage'] = "Not a valid JSON. Please send a valid JSON object data in POST"
        except (Email.DoesNotExist, User.DoesNotExist):
            ret['errorMessage'] = "User does not exist"
    else:
        ret['errorMessage'] = "No POST data received. A valid JSON object is allowed as POST data"

    return HttpResponse(json.dumps(ret))


@csrf_exempt
@protected_resource()
def policy(request, type=None):
    ret = {
        'data': {},
        'errorCode': None,
        'errorItem': [],
        'errorMessage': None,
        'success': False,
    }

    try:
        user = request.user

        try:
            uts = UserTransaction.objects.filter(user=user).prefetch_related('transaction')

            policies = dict()
            for ut in uts:
                policies.setdefault(ut.type, []).append(ut.tr_json(request))

            if type is None:
                ret['data']['policies'] = policies
                ret['success'] = True
            elif type in policies:
                ret['data']['policies'] = policies[type]
                ret['success'] = True
            else:
                ret['errorMessage'] = "Policy does not exist"

        except UserTransaction.DoesNotExist:
            ret['errorMessage'] = "This user does not have any policy"

        # RM details
        ret['data']['rm'] = {
            'name': getattr(getattr(getattr(user, 'urm', None), 'rm', None), 'name', None),
            'email': getattr(getattr(getattr(user, 'urm', None), 'rm', None), 'email', None),
            'mobile': getattr(getattr(getattr(user, 'urm', None), 'rm', None), 'mobile', None),
            'photo': user.urm.rm.photo.url if getattr(getattr(getattr(user, 'urm', None), 'rm', None), 'photo', None) else None,
        }

    except User.DoesNotExist:
        ret['errorMessage'] = "User does not exist"

    return HttpResponse(json.dumps(ret, cls=DjangoJSONEncoder))

@csrf_exempt
@authenticate_api
def create_user(request):
    ret = {
        'data': {},
        'errorCode': None,
        'errorItem': [],
        'errorMessage': None,
        'success': False,
    }

    if request.method == 'POST':
        try:
            form = CreateUser(json.loads(request.body))
            if form.is_valid():
                first_name = form.cleaned_data['first_name']
                email = form.cleaned_data['email']
                password = form.cleaned_data['password']
                user, is_created = User.objects.get_or_create(email=email)
                ret['success'] = is_created
                if is_created:
                    user.first_name = first_name.title()
                    user.set_password(password)
                    user.save()
                    ret['errorMessage'] = "User created successfully"
                else:
                    ret['errorMessage'] = "User already exists"
            else:
                ret['errorItem'] = form.errors
                ret['errorMessage'] = "Some values might not be correct. Please refer fields and their datatypes"
        except ValueError:
            ret['errorMessage'] = "Not a valid JSON. Please send a valid JSON object data in POST"
        return HttpResponse(json.dumps(ret))
    else:
        return HttpResponseNotAllowed('POST')

@csrf_exempt
@authenticate_api
def add_offline_transaction(request):
    ret = {
        'data': {},
        'errorCode': None,
        'errorItem': [],
        'errorMessage': None,
        'success': False,
    }

    if request.body:
        try:
            post = json.loads(request.body)

            payment_on = get_property(post, 'extra.payment_on') or get_property(post, 'extra.payment_date')

            data = {
                'transaction_id': post.get('transaction_id', None),
                'proposer_name': post.get('proposer_name', None),
                'proposer_email': post.get('proposer_email', None),
                'proposer_mobile': post.get('proposer_mobile', None),
                'proposer_address1': post.get('proposer_address1', None),
                'proposer_address2': post.get('proposer_address2', None),
                'proposer_address_landmark': post.get('proposer_address_landmark', None),
                'proposer_city': post.get('proposer_city', None),
                'proposer_state': post.get('proposer_state', None),
                'proposer_pincode': post.get('proposer_pincode', None),
                'premium_paid': post.get('premium_paid', None),
                'payment_on': datetime.strptime(payment_on, '%d/%m/%Y') if payment_on else None,
                'type': post.get('type', None),
                'status': post.get('status', None),
                'extra': request.body,
            }

            data = dict((k, v) for k, v in data.iteritems() if v)

            if data:
                OfflineTransaction.objects.create(**data)
                ret['errorMessage'] = "Offline transaction record added successfully."
                ret['success'] = True
            else:
                ret['errorMessage'] = "No record added. Empty JSON request sent."
        except ValueError:
            ret['errorMessage'] = "Not a valid JSON. Please send a valid JSON object data in POST"
    else:
        ret['errorMessage'] = "No POST data received. A valid JSON object is allowed as POST data"

    return HttpResponse(json.dumps(ret))


@csrf_exempt
@authenticate_api
def get_transactions(request):

    if request.method == 'POST':
        ret = {
            'data': {},
            'errorCode': None,
            'errorItem': [],
            'errorMessage': None,
            'success': False,
        }
        if request.body:
            try:
                post = json.loads(request.body)
                if 'q' in post:
                    ret['data'] = {
                        'health': [get_health_policy(request, t) for t in get_health_transactions(post['q'], Q(status='PENDING') | Q(status='MANUAL COMPLETED') | Q(status='COMPLETED') )],
                        'motor': [get_motor_policy(t) for t in get_motor_transactions(post['q'], Q(status='PENDING') | Q(status='MANUAL COMPLETED') | Q(status='COMPLETED') )],
                        'travel': [get_travel_policy(t) for t in get_travel_transactions(post['q'], Q(status='PENDING') | Q(status='MANUAL COMPLETED') | Q(status='COMPLETED') )],
                    }
                    ret['success'] = True
                else:
                    ret['errorMessage'] = "Expecting `q` parameter name as query"
            except ValueError:
                ret['errorMessage'] = "Not a valid JSON. Please send a valid JSON object data in POST"
        else:
            ret['errorMessage'] = "A valid JSON object is allowed as POST data"

        return HttpResponse(json.dumps(ret))
    else:
        return HttpResponseNotAllowed('GET')


@csrf_exempt
@authenticate_api
def update_transaction(request):

    if request.method == 'POST':
        GETMODEL = {
            'motor': apps.get_model('motor_product', 'Transaction'),
            'health': apps.get_model('health_product', 'Transaction'),
        }
        ret = {
            'data': {},
            'errorCode': None,
            'errorItem': [],
            'message': None,
            'success': False,
        }

        if request.body:
            try:
                post = json.loads(request.body)
                if all (k in post for k in ('transaction_id', 'type')):
                    type = post['type']
                    transaction_id = post['transaction_id']
                    if type in GETMODEL:
                        try:
                            q = GETMODEL[type].objects.get(transaction_id=transaction_id)
                            if q.status == 'PENDING':
                                q.status = 'MANUAL COMPLETED'
                                q.save()
                                ret['message'] = 'Transaction updated successfully.'
                                ret['success'] = True
                            else:
                                ret['message'] = 'Only PENDING transaction can be modified'
                        except GETMODEL[type].DoesNotExist:
                            ret['message'] = 'Transaction does not exists.'
                    else:
                        ret['message'] = 'Transaction type does not exists.'
                else:
                    ret['errorMessage'] = "Expecting `transaction_id` and `type` parameter name as query"
            except ValueError:
                ret['errorMessage'] = "Not a valid JSON. Please send a valid JSON object data in POST"
        else:
            ret['errorMessage'] = "A valid JSON object is allowed as POST data"

        return HttpResponse(json.dumps(ret))
    else:
        return HttpResponseNotAllowed('GET')
