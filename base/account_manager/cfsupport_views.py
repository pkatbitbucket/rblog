from __future__ import absolute_import

import base64
import datetime
import json
import os
import uuid
from collections import OrderedDict
from itertools import chain

from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import UNUSABLE_PASSWORD_PREFIX
from django.core.cache import cache
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.conf import settings
from django.forms import models as django_model_forms
from django.http import (Http404, HttpResponse, HttpResponseBadRequest,
                         HttpResponseRedirect, JsonResponse)
from django.shortcuts import (get_object_or_404, redirect, render,
                              render_to_response)
from django.template import RequestContext
from django.template.loader import get_template
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.functional import cached_property
from django.utils.text import slugify
from django.views.generic import ListView

import account_manager.forms as account_manager_forms
import cf_cns
import health_product.forms as health_forms
import motor_product.forms as motor_forms
import motor_product.models as motor_models
import putils
from account_manager.signals.signals import (activate_user_account,
                                             create_user_account)
from core import models as core_models
from core.models import Email, User
from motor_product.prod_utils import _save_transaction_details
from utils import any_permission_satisfied, camel_case_to_snake_case
from wiki.models import HealthProduct

from .models import RelationshipManager, UserAccount
from .utils import (TRANSACTION_MODEL_MAPPING, created_range_query,
                    generate_transactions_report,
                    generate_transactions_vs_useraccounts_report,
                    generate_user_account_report, get_health_transactions,
                    get_motor_transactions, get_travel_transactions,
                    get_user_details, get_user_for_transaction,
                    get_user_transactions, transaction_field_mapping)

REPORT_SUCCESS_GENERATED_MSG = """\
<center><h2>Report was successfully generated at {}. <br>Please check your mail
or use <a href=\"{}\">link</a>.<br>Please try again after an hour of the last report generated if wanted to generate
the report again.</h2><br><a href=\"{}\"><< Dashboard</a></center>
"""

REPORT_PLEASE_WAIT_MSG = """\
<center><h2>Please wait...<br/>Report is being generated. You will receive an email shortly.</h2>
<br><a href=\"{}\"><< Dashboard</a>&nbsp;&nbsp;&nbsp;<a href=\"\">Reload</a></center>"""

REPORT_PLEASE_WAIT_MORE_MSG = """\
<center><h2>Please wait for sometime more...<br/>Report is being generated. You will
receive an email shortly.</h2><br><a href=\"{}\"><< Dashboard</a>&nbsp;&nbsp;&nbsp;<a href=\"\">Reload</a></center>"""

COMPLETED_STATUS = ['COMPLETED', 'MANUAL COMPLETED']

RM_PERMS = ('account_manager.add_usertransaction', 'account_manager.change_usertransaction')
AGENT_PERMS = ('account_manager.add_offline_transaction',)
ENDORSEMENT_PERMS = ('core.view_endorsementdetail',
                     'core.add_endorsementdetail',
                     'core.change_endorsementdetail')

ROWS_PER_PAGE = 100


@any_permission_satisfied(AGENT_PERMS, RM_PERMS)
def dashboard(request):
    # Date filter
    from_date = to_date = datetime.datetime.now().date()
    f = from_date
    t = f + datetime.timedelta(days=1)
    if all(k in request.GET for k in ('from_date', 'to_date')):
        from_date = datetime.datetime.strptime(request.GET['from_date'], '%Y-%m-%d').date()
        to_date = datetime.datetime.strptime(request.GET['to_date'], '%Y-%m-%d').date()
        f = from_date
        t = to_date + datetime.timedelta(days=1)

    status_query = {
        'successful': Q(status__in=COMPLETED_STATUS) & (Q(policy_document__isnull=True) | Q(policy_document='')),
        'completed': Q(status__in=COMPLETED_STATUS) & (Q(policy_document__isnull=False) | ~Q(policy_document='')),
        'cancelled': Q(status='CANCELLED'),
        'pending': Q(status='PENDING'),
    }

    report_type = ['successful', 'completed', 'cancelled', 'pending']
    product_type = ['car', 'bike', 'health', 'travel']

    # Status Report
    report_count_dict = OrderedDict()
    for report in report_type:
        count_dict = OrderedDict()
        for product in product_type:
            query = status_query[report] & created_range_query(f, t, product)
            count = TRANSACTION_MODEL_MAPPING[product].objects.filter(query).count()
            count_dict.update({product: count})
        report_count_dict.update({report: count_dict})

    # Insurer-wise Payment successful policies
    all_transactions = []
    for product in product_type:
        Transaction = TRANSACTION_MODEL_MAPPING[product]
        query = status_query['successful'] & created_range_query(f, t, product)
        transactions = Transaction.objects.filter(query).order_by('-pk')
        all_transactions.append(transactions)

    all_transactions = tuple(chain(*all_transactions))

    paginator = Paginator(all_transactions, ROWS_PER_PAGE)
    page = request.GET.get('page')
    try:
        all_transactions = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        all_transactions = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        all_transactions = paginator.page(paginator.num_pages)

    payment_success_transactions = []
    for transaction in all_transactions:
        payment_success_transactions.append(transaction_field_mapping(transaction))

    context = {
        'selected': 'dashboard',
        'paginator': all_transactions,
        'total_count': paginator.count,
        'from_date': from_date,
        'to_date': to_date,
        'report_count': report_count_dict,
        'payment_success_transactions': payment_success_transactions,
    }

    template = 'account_manager/cfsupport/dashboard.html'
    return render_to_response(template, RequestContext(request, context))


@any_permission_satisfied(RM_PERMS)
def search_transaction(request):
    context = {'selected': 'search-transaction'}

    if 'q' in request.GET:
        q = request.GET['q']
        all_transactions = OrderedDict([
            ('car', get_motor_transactions(q, filt=Q(vehicle_type='Private Car'), excl=Q(status='DRAFT'))),
            ('bike', get_motor_transactions(q, filt=Q(vehicle_type='Twowheeler'), excl=Q(status='DRAFT'))),
            ('health', get_health_transactions(q, excl=Q(status='DRAFT'))),
            ('travel', get_travel_transactions(q, excl=Q(status='DRAFT'))),
        ])

        final_trans = OrderedDict()
        for product, transactions in all_transactions.items():
            trans = []
            for transaction in transactions.iterator():
                trans.append(transaction_field_mapping(transaction))
            final_trans.update({product: trans})

        context.update({
            'all_transactions': final_trans,
            'q': q,
        })

    template = 'account_manager/cfsupport/search-transaction.html'
    return render_to_response(template, RequestContext(request, context))


@any_permission_satisfied(RM_PERMS)
def transactions(request, transaction_type):
    if transaction_type not in TRANSACTION_MODEL_MAPPING:
        raise Http404

    # Date filter
    from_date = to_date = datetime.datetime.now().date()
    f = from_date
    t = f + datetime.timedelta(days=1)
    if all(k in request.GET for k in ('from_date', 'to_date')):
        if request.GET['from_date'] and request.GET['to_date']:
            from_date = datetime.datetime.strptime(request.GET['from_date'], '%Y-%m-%d').date()
            to_date = datetime.datetime.strptime(request.GET['to_date'], '%Y-%m-%d').date()
            f = from_date
            t = to_date + datetime.timedelta(days=1)

    Transaction = TRANSACTION_MODEL_MAPPING[transaction_type]
    query = created_range_query(f, t, transaction_type) & ~Q(status='DRAFT')

    # Export CSV
    if 'export' in request.GET:
        return _export(user=request.user, export_type=transaction_type, query=query, from_date=from_date, to_date=to_date)

    transactions = Transaction.objects.filter(query).order_by('-pk')

    # pagination
    paginator = Paginator(transactions, ROWS_PER_PAGE)
    page = request.GET.get('page')
    try:
        transactions = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        transactions = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        transactions = paginator.page(paginator.num_pages)

    trans = []
    for transaction in transactions:
        trans.append(transaction_field_mapping(transaction))

    context = {
        'selected': 'transactions',
        'paginator': transactions,
        'total_count': paginator.count,
        'transaction_type': transaction_type,
        'all_transactions': {transaction_type: trans},
        'from_date': from_date,
        'to_date': to_date,
    }

    template = 'account_manager/cfsupport/transactions.html'
    return render_to_response(template, RequestContext(request, context))


@any_permission_satisfied(RM_PERMS)
def edit_transaction(request):
    if request.method == 'POST':
        transaction_type = request.POST['transaction_type']
        transaction_id = request.POST['transaction_id']
        transaction = get_object_or_404(TRANSACTION_MODEL_MAPPING[transaction_type], transaction_id=transaction_id)
        transaction_detail = transaction_field_mapping(transaction)
        user, user_account, user_transaction = get_user_for_transaction(transaction, transaction_detail.get('email'))
        form = account_manager_forms.EditTransaction(request.POST, request.FILES)

        if form.is_valid():
            cleaned_data = form.cleaned_data
            email = transaction_field_mapping(transaction).get('email', None)
            mobile = transaction_field_mapping(transaction).get('mobile', None)

            # policy update
            if transaction.status not in COMPLETED_STATUS:
                transaction.status = 'MANUAL COMPLETED'

            transaction.policy_number = cleaned_data['policy_number']
            transaction.premium_paid = cleaned_data['premium_paid']
            transaction.payment_on = cleaned_data['payment_on']

            if transaction_type in ['health', 'travel']:
                transaction.save(read_only=False)
            else:
                transaction.save()

            policy_updated = False
            if email:
                create_user_account.send(sender=transaction._meta.model, transaction=transaction)
                user = User.objects.get_user(email=email, phone=mobile)
                user_account = UserAccount.objects.get(user=user)
                policy_updated = user_account.user.is_active
                activate_user_account.send(sender=transaction._meta.model, transaction=transaction)

            # upload policy doc
            policy_doc = request.FILES['policy_document']
            transaction.update_policy(policy_doc.read(), send_mail=policy_updated)

            messages.success(request, '<b>Bingo!</b> Transaction updated successfully.')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        else:
            pass
    else:
        if all(k in request.GET for k in ('transaction_type', 'transaction_id')):
            transaction_type = request.GET['transaction_type']
            transaction_id = request.GET['transaction_id']

            if transaction_type in TRANSACTION_MODEL_MAPPING:
                transaction = get_object_or_404(TRANSACTION_MODEL_MAPPING[transaction_type], transaction_id=transaction_id)
            else:
                raise Http404
        else:
            raise Http404

        transaction_detail = transaction_field_mapping(transaction)
        user, user_account, user_transaction = get_user_for_transaction(transaction, transaction_detail.get('email'))

        payment_on = transaction_detail['payment_on']
        payment_on = payment_on.strftime('%Y/%m/%d %H:%M') if payment_on else ''
        pre_populate = {
            'policy_number': transaction_detail['policy_number'],
            're_enter_policy_number': transaction_detail['policy_number'],
            'premium_paid': transaction_detail['premium_paid'],
            're_enter_premium_paid': transaction_detail['premium_paid'],
            'payment_on': payment_on,
        }
        form = account_manager_forms.EditTransaction(initial=pre_populate)

    # Hide fields on the fly if value non-empty
    fields_hidden = {
        'premium_paid': ['premium_paid', 're_enter_premium_paid'],
        'payment_on': ['payment_on'],
    }
    for field, fields in fields_hidden.items():
        if field in transaction_detail and transaction_detail[field]:
            for f in fields:
                form.fields[f].widget = form.fields[f].hidden_widget()

    context = {
        'transaction_detail': transaction_detail,
        'user_detail': get_user_details(user) if user else None,
        'user_account': user_account,
        'user_transactions': get_user_transactions(user),
        'transaction_type': request.GET['transaction_type'],
        'completed_status': COMPLETED_STATUS,
        'form': form,
        'selected': 'search-transaction',
    }

    template = 'account_manager/cfsupport/edit-transaction.html'
    return render_to_response(template, context, context_instance=RequestContext(request))


@any_permission_satisfied(RM_PERMS)
def user_accounts(request):
    # Date filter
    from_date = to_date = datetime.datetime.now().date()
    f = from_date
    t = f + datetime.timedelta(days=1)
    if all(k in request.GET for k in ('from_date', 'to_date')):
        if request.GET['from_date'] and request.GET['to_date']:
            from_date = datetime.datetime.strptime(request.GET['from_date'], '%Y-%m-%d').date()
            to_date = datetime.datetime.strptime(request.GET['to_date'], '%Y-%m-%d').date()
            f = from_date
            t = to_date + datetime.timedelta(days=1)

    query = Q(first_payment_date__range=(f, t))

    # Account activation filter
    if 'accountactivated' in request.GET:
        if request.GET['accountactivated'] == 'Yes':
            query = query & ~Q(user__password__startswith=UNUSABLE_PASSWORD_PREFIX)
        elif request.GET['accountactivated'] == 'No':
            query = query & Q(user__password__startswith=UNUSABLE_PASSWORD_PREFIX)

    # RM filter
    if 'rm' in request.GET:
        rm = get_object_or_404(RelationshipManager, id=request.GET['rm'])
        query = query & Q(user__urm__rm=rm)

    # Export CSV
    if 'export' in request.GET:
        return _export(user=request.user, export_type='user_accounts', query=query, from_date=from_date, to_date=to_date)

    user_accounts = UserAccount.objects.filter(query).order_by('-first_payment_date')

    # Search
    q = None
    if 'q' in request.GET:
        q = request.GET['q'].strip()
        name_filter = Q()
        for n in q.split():
            name_filter |= Q(user__first_name__icontains=n)
            name_filter |= Q(user__last_name__icontains=n)

        email_objs = Email.objects.filter(email__icontains=q)
        users = [email.user for email in email_objs]

        user_accounts = UserAccount.objects.filter(name_filter |
                                                   Q(user__in=users) |
                                                   Q(mobile__icontains=q)).order_by('-first_payment_date')

    # pagination
    paginator = Paginator(user_accounts, ROWS_PER_PAGE)
    page = request.GET.get('page')
    try:
        user_accounts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        user_accounts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        user_accounts = paginator.page(paginator.num_pages)

    u_accounts = []
    for user_account in user_accounts:
        u_accounts.append({
            'user_account': get_user_details(user_account.user),
            'user_transactions': get_user_transactions(user_account.user),
        })

    context = {
        'paginator': user_accounts,
        'total_count': paginator.count,
        'user_accounts': u_accounts,
        'rms': RelationshipManager.objects.all(),
        'querystring': request.GET.urlencode(),
        'from_date': from_date,
        'to_date': to_date,
        'selected': 'user-accounts',
        'q': q,
    }

    template = 'account_manager/cfsupport/user-accounts.html'
    return render_to_response(template, RequestContext(request, context))


@any_permission_satisfied(RM_PERMS)
def user(request, user_id):
    user = get_object_or_404(User, id=user_id)
    user_account = (user.useraccount.all()[0] if user.useraccount.all() else None) if user else None

    context = {
        'user_detail': get_user_details(user),
        'user_account': user_account,
        'user_transactions': get_user_transactions(user),
        'selected': 'user-accounts',
    }

    template = 'account_manager/cfsupport/user.html'
    return render_to_response(template, RequestContext(request, context))


@any_permission_satisfied(RM_PERMS)
def transactions_vs_useraccounts(request):
    if all(k in request.GET for k in ('from_date', 'to_date')):

        from_date = datetime.datetime.strptime(request.GET['from_date'], '%Y-%m-%d')
        to_date = datetime.datetime.strptime(request.GET['to_date'], '%Y-%m-%d') + datetime.timedelta(days=1)

        return _export(user=request.user, export_type='transactions_vs_useraccounts', from_date=from_date,
                       to_date=to_date)
    else:
        context = {'selected': 'transactions-vs-useraccounts'}
        template = 'account_manager/cfsupport/transactions-vs-useraccounts.html'
        return render_to_response(template, RequestContext(request, context))


def _export(user, export_type, query=None, from_date=None, to_date=None):
    report_generated = cache.get('cfsupport_{}_report'.format(export_type))
    report_url = cache.get('cfsupport_{}_report_url'.format(export_type))

    # Messages
    MSG_SUCCESS = REPORT_SUCCESS_GENERATED_MSG.format(
        report_generated,
        report_url,
        reverse('account_manager:cfsupport_dashboard')
    )
    MSG_PLEASE_WAIT_MORE = REPORT_PLEASE_WAIT_MORE_MSG.format(reverse('account_manager:cfsupport_dashboard'))
    MSG_PLEASE_WAIT = REPORT_PLEASE_WAIT_MSG.format(reverse('account_manager:cfsupport_dashboard'))

    if report_generated:
        return HttpResponse(MSG_SUCCESS)

    if cache.get('cfsupport_{}_lock'.format(export_type)):
        return HttpResponse(MSG_PLEASE_WAIT_MORE)

    # Set lock
    cache.set('cfsupport_{}_lock'.format(export_type), True)

    # filename
    now = slugify(datetime.datetime.now())[:-6]
    if from_date and to_date:
        filename = "{}_f_{}_t_{}_{}.csv".format(export_type, from_date.strftime('%Y-%m-%d'),
                                                to_date.strftime('%Y-%m-%d'), now)
    else:
        filename = "{}_{}.csv".format(export_type, now)

    # Call report generation function
    if export_type in TRANSACTION_MODEL_MAPPING.keys():
        generate_transactions_report.delay(user, filename, export_type, query)
    elif export_type == 'user_accounts':
        generate_user_account_report.delay(user, filename, export_type, query)
    elif export_type == 'transactions_vs_useraccounts':
        generate_transactions_vs_useraccounts_report.delay(user, filename, export_type, from_date, to_date)

    return HttpResponse(MSG_PLEASE_WAIT)


@any_permission_satisfied(RM_PERMS)
def login_as(request):
    # If staff tries to login as user
    if "__impersonate" in request.POST:
        try:
            staff_user = request.user
            u = User.objects.get_user(email=request.POST['__impersonate'].strip(), phone='')

            # If staff trying to access superuser account
            if not u.is_superuser or staff_user.is_superuser:
                auth_user = authenticate(user=u, password=settings.RANDOM_SEED)
                login(request, auth_user)
                request.session['coverfoxstaff_user_id'] = staff_user.id
                return HttpResponseRedirect(reverse('account_manager:profile'))
            else:
                messages.error(request, '<b>Error!</b> You cannot access this user.')
                return HttpResponseRedirect(reverse('account_manager:login_as'))
        except (Email.DoesNotExist, User.DoesNotExist,):
            messages.error(request, '<b>Error!</b> User does not exists.')
            return HttpResponseRedirect(reverse('account_manager:login_as'))
    else:
        context = {
            'selected': 'login-as',
        }

        template = 'account_manager/cfsupport/login-as.html'
        return render_to_response(template, RequestContext(request, context))


@login_required
def staff_logout(request):
    # If staff tries to logout as user
    if "__unimpersonate" in request.GET and 'coverfoxstaff_user_id' in request.session:
        u = User.objects.get(id=request.session['coverfoxstaff_user_id'])
        auth_user = authenticate(user=u, password=settings.RANDOM_SEED)
        login(request, auth_user)
        return HttpResponseRedirect(reverse('account_manager:login_as'))
    else:
        return HttpResponseRedirect(reverse('account_manager:login_as'))


@any_permission_satisfied(AGENT_PERMS)
def add_offline_transaction(request, product_type):
    # TODO: if product_type not in ['motor', 'travel', 'health']:
    if product_type not in ['car', 'bike', 'health']:
        return HttpResponseBadRequest()

    if product_type in ['car', 'bike']:
        return add_motor_offline_transaction(request, product_type)

    if product_type == 'health':
        return add_health_offline_transaction(request)


@any_permission_satisfied(AGENT_PERMS)
def add_motor_offline_transaction(request, product_type):
    if request.method == 'POST':
        try:
            data = json.loads(request.POST['data'])
        except:
            return HttpResponseBadRequest()

        if any(item not in data for item in ('quote_details', 'proposal_details')):
            return HttpResponseBadRequest()

        resp = {}

        quote_details = data['quote_details']
        quote_form = motor_forms.OfflineQuoteForm(product_type, quote_details)
        if quote_form.is_valid():
            success = True
            quote_data = quote_form.cleaned_data

            keys = ["addon_is247RoadsideAssistance",
                    "addon_isDepreciationWaiver",
                    "addon_isDriveThroughProtected",
                    "addon_isEngineProtector",
                    "addon_isInvoiceCover",
                    "addon_isKeyReplacement",
                    "addon_isNcbProtection",
                    "cngKitValue",
                    "extra_isAntiTheftFitted",
                    "extra_isLegalLiability",
                    "extra_isMemberOfAutoAssociation",
                    "extra_isTPPDDiscount",
                    "extra_paPassenger",
                    "manufacturingDate",
                    "idv",
                    "idvElectrical",
                    "idvNonElectrical",
                    "isCNGFitted",
                    "isNCBCertificate",
                    "isNewVehicle",
                    "isUsedVehicle",
                    "newNCB",
                    "payment_mode",
                    "registrationDate",
                    "voluntaryDeductible",
                    "newPolicyStartDate",
                    "newPolicyEndDate"]

            quote_raw_data = {}
            for i, key in enumerate(keys):
                for prefix in ['addon_is', 'extra_is', 'extra_', 'is']:
                    if key.startswith(prefix):
                        key = key[len(prefix):]
                        break

                key = camel_case_to_snake_case(key)
                var_names = key.split('_')
                if var_names[0].isdigit():
                    var_names = var_names[1:]
                key = '_'.join(var_name for var_name in var_names)
                value = quote_data[key]

                if isinstance(value, datetime.date):
                    value = value.strftime('%d-%m-%Y')
                elif not isinstance(value, (bool, None.__class__)):
                    value = unicode(value)

                quote_raw_data[keys[i]] = value

            quote_raw_data['expirySlot'] = '7'
            quote_raw_data['expiry_error'] = 'false'
            quote_raw_data['third_party'] = None
            quote_raw_data['vehicleId'] = str(quote_data['vehicle'].id)
        else:
            success = False

        proposal_details = data['proposal_details']
        proposal_form = motor_forms.OfflineProposalForm(proposal_details)
        if proposal_form.is_valid():
            proposal_data = proposal_form.cleaned_data
            keys = [u'add_building_name',
                    u'add_house_no',
                    u'add_landmark',
                    u'add_pincode',
                    u'add_street_name',
                    u'cust_dob',
                    u'cust_email',
                    u'cust_first_name',
                    u'cust_gender',
                    u'cust_last_name',
                    u'cust_marital_status',
                    u'cust_phone',
                    u'is_car_financed',
                    u'nominee_age',
                    u'nominee_name',
                    u'nominee_relationship',
                    u'vehicle_chassis_no',
                    u'vehicle_engine_no']

            same_address = proposal_data['same_address']
            if not same_address:
                keys += [u'reg_add_building_name',
                         u'reg_add_house_no',
                         u'reg_add_landmark',
                         u'reg_add_pincode',
                         u'reg_add_street_name']

            user_form_details = {}
            for i, key in enumerate(keys):
                for prefix in ['loc_add_', 'add_', 'cust_', 'is_', 'vehicle_']:
                    if key.startswith(prefix):
                        key = key[len(prefix):]
                        break
                else:
                    if '_add' in key:
                        key = key[:key.find('_add')] + key[key.find('_add') + len('_add'):]

                value = proposal_data[key]

                if isinstance(value, datetime.date):
                    value = value.strftime('%d-%m-%Y')
                elif not isinstance(value, (bool, None.__class__)):
                    value = unicode(value)

                user_form_details[keys[i]] = value

            user_form_details['add_district'] = ''
            user_form_details['loc_add_city'] = str(proposal_data['city'].id)
            user_form_details['loc_add_state'] = str(proposal_data['state'].id)
            user_form_details['add_state'] = unicode(proposal_data['state'].name)
            user_form_details['add_city'] = unicode(proposal_data['city'].name)

            if same_address:
                user_form_details['loc_reg_add_city'] = user_form_details['loc_add_city']
                user_form_details['loc_reg_add_state'] = user_form_details['loc_add_state']
            else:
                user_form_details['loc_reg_add_city'] = str(proposal_data['reg_city'].id)
                user_form_details['loc_reg_add_state'] = str(proposal_data['reg_state'].id)
        else:
            success = False

        if success:
            quote_raw_data["extra_user_dob"] = user_form_details['cust_dob']
            quote_raw_data['registrationNumber[]'] = proposal_data['reg_no'].split('-')
            user_form_details["vehicle_reg_no"] = proposal_data['reg_no']
            if proposal_data['car_financed'] == 'true':
                user_form_details["car-financier"] = proposal_data['car_financier']
            quote = motor_models.Quote(
                vehicle=quote_data['vehicle'],
                raw_data=quote_raw_data,
                type=motor_models.Quote.OFFLINE,
            )
            quote.save()
            quote.raw_data['quoteId'] = quote.quote_id
            quote.save()

            transaction = motor_models.Transaction.objects.create(
                tracker=request.TRACKER,
                quote=quote,
                insurer=proposal_data['insurer'],
                vehicle_type=quote.vehicle.vehicle_type,
                status='PENDING',
                raw={'user_form_details': user_form_details},
                policy_start_date=quote_data['new_policy_start_date'],
                policy_end_date=quote_data['new_policy_end_date'],
                premium_paid=proposal_data['premium_paid'],
                payment_on=proposal_data.get('payment_on'),
                payment_done=True,
            )

            _save_transaction_details(quote.vehicle, user_form_details, transaction)
            transaction.mark_complete('MANUAL COMPLETED')

            resp['transaction_id'] = transaction.transaction_id
            quote_form = motor_forms.OfflineQuoteForm(product_type)
            proposal_form = motor_forms.OfflineProposalForm()

        bootstrap_template = get_template('account_manager/bootstrap_form.html')
        resp.update({
            'success': success,
            'quote_form_details': bootstrap_template.render({'form': quote_form}),
            'proposal_form_details': bootstrap_template.render({'form': proposal_form}),
        })
        return JsonResponse(resp)
    else:
        quote_form = motor_forms.OfflineQuoteForm(product_type)
        proposal_form = motor_forms.OfflineProposalForm()

    return render(
        request,
        'account_manager/cfsupport/offline_transactions/motor.html',
        {'quote_form': quote_form,
         'proposal_form': proposal_form,
         'selected': 'offline_transactions'}
    )


@any_permission_satisfied(AGENT_PERMS)
def add_health_offline_transaction(request):
    """
    Serves the page for offline health transaction creation.
    """
    if request.method == 'POST':

        form = health_forms.OfflineForm(request, request.POST)
        if form.is_valid():
            data = form.cleaned_data
            transaction = form.save(commit=False)

            transaction.transaction_id = uuid.uuid1().hex
            transaction.slab = data['slab']
            transaction.tracker = request.TRACKER
            transaction.status = 'PENDING'
            transaction.payment_done = True
            transaction.no_of_people_insured = len(data['members_data'])

            # preparing extra data
            transaction.extra = {
                u'city': {
                    u'id': data['proposer_city'].id,
                    # 'lat': 18.934662,
                    # 'lng': 72.836778,
                    u'name': data['proposer_city'].name,
                    u'type': 'METRO' if data['proposer_city'].is_major else 'NON-METRO'
                },
                # u'cpt_id': 22195, cover premium table id
                u'gender': request.POST.get('proposer_gender', 'MALE'),
                u'members': data['members_data'],
                u'pincode': data['proposer_pincode'],
                u'plan_data': {
                    u'cover': data['slab'].sum_assured,
                    u'insurer_id': data['insurer'].id,
                    u'plan_name': data['health_plan'].title,
                    u'premium': data['premium_paid']
                },
                u'slab_id': data['slab'].id
            }
            transaction.insured_details_json = {
                u'medical_conditions': data['health_data']
            }
            transaction.save()
            transaction.mark_complete(status='MANUAL COMPLETED')

            resp = {'status': 'ok', 'transaction_id': transaction.transaction_id}
        else:
            resp = {'status': 'error', 'errors': form.errors}

        # bootstrap_template = get_template('account_manager/bootstrap_form.html')

        return JsonResponse(resp)
    else:
        form = health_forms.OfflineForm(request)

    return render(
        request,
        'account_manager/cfsupport/offline_transactions/health.html',
        {'form': form, 'selected': 'offline_transactions'}
    )


def get_health_plans(request):
    """
    Handles the ajax requests for a given insurer and return insurance plans and there slabs.
    """
    insurer_id = request.GET.get('insurer_id', '')
    if not insurer_id:
        raise Http404

    products = HealthProduct.objects.filter(insurer_id=insurer_id).prefetch_related('slabs')
    results = [{'id': p.id,
                'title': '%s (%s)' % (p.title, dict(HealthProduct.POLICY_TYPE_CHOICES)[p.policy_type]),
                'slabs': [{'id': s.id, 'sum': s.sum_assured} for s in p.slabs.all()]}for p in products]
    return JsonResponse({'status': 'ok', 'results': results})


class EndorsementListView(ListView):
    paginate_by = 10
    template_name = 'account_manager/cfsupport/endorsements/dashboard.html'
    allow_empty = True
    model = core_models.EndorsementDetail
    ordering = 'id'

    @method_decorator(any_permission_satisfied(ENDORSEMENT_PERMS))
    def dispatch(self, *args, **kwargs):
        return super(EndorsementListView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return super(EndorsementListView, self).get_queryset().filter(
            created_date__gte=self.from_date,
            created_date__lte=self.to_date + datetime.timedelta(days=1),
        )

    def get_context_data(self, **kwargs):
        context = super(EndorsementListView, self).get_context_data(**kwargs)
        details = OrderedDict()
        for product, _ in self.model.PRODUCTS:
            endorsements = self.object_list.filter(product=product)
            details[product] = {
                'completed': endorsements.filter(completed_on__isnull=False),
                'pending': endorsements.filter(completed_on__isnull=True),
            }
        context['end_details'] = details
        context['from_date'] = self.from_date
        context['to_date'] = self.to_date
        context['selected'] = 'endorsements'
        return context

    @cached_property
    def from_date(self):
        from_date = datetime.date.today()
        if self.request.GET.get('f'):
            try:
                from_date = datetime.datetime.strptime(
                    self.request.GET['f'], '%Y-%m-%d'
                ).date()
            except:
                pass
        return from_date

    @cached_property
    def to_date(self):
        to_date = datetime.date.today()
        if self.request.GET.get('t'):
            try:
                to_date = datetime.datetime.strptime(
                    self.request.GET['t'], '%Y-%m-%d'
                ).date()
            except:
                pass
        return to_date


@any_permission_satisfied('core.add_endorsementdetail')
def endorsement_add(request):
    end_form = django_model_forms.modelform_factory(
        model=core_models.EndorsementDetail,
        exclude=('user_documents', 'reciept', 'completed_on'),
    )
    if request.method == 'POST':
        endoresement_form = end_form(
            data=request.POST, files=request.FILES
        )
        if endoresement_form.is_valid():
            end = endoresement_form.save()
            insurer_attachments = []
            for doc in request.FILES.getlist('user_documents'):
                path_ = 'uploads/core/endorsement_details/{}/{}'.format(
                    end.id, doc.name
                )
                data = doc.read()
                insurer_attachments.append({
                    'name': doc.name,
                    'type': doc.content_type,
                    'content': base64.b64encode(data)
                })
                with default_storage.open(path_, 'wb') as f:
                    f.write(data)
                    del data
                doc = core_models.UserDocument.objects.create(
                    document=path_
                )
                end.user_documents.add(doc)

            if settings.PRODUCTION:
                to_email = end.insurer_email
            else:
                to_email = end.customer_email

            cf_cns.notify(
                nid='ENDORSEMENT_NOTIFY_INSURER',
                obj=end,
                to=to_email,
                mandrill_template='newbase',
                attachments=insurer_attachments,
            )

            pdfdata = putils.generate_pdf_from_html(
                template_path="core:endorsement_pdf_template.html",
                data={'obj': end},
            )
            # saving the pdf as a backup
            default_storage.save(
                'uploads/core/acr/{}.pdf'.format(end.id), ContentFile(pdfdata)
            )
            pdfdata = base64.b64encode(pdfdata)

            param = {
                "attachments": [{
                    'name': 'ACR_{}_details.pdf'.format(end.id),
                    'type': 'application/pdf',
                    'content': pdfdata,
                }],
                "to": (end.customer_name, end.customer_email),
                "mandrill_template": 'newbase',
            }

            cf_cns.notify('ENDORSEMENT_NOTIFY_CUSTOMER', obj=end, **param)
            return redirect('account_manager:endorsement_detail', end_id=end.id)

    else:
        endoresement_form = end_form()

    initial_data = endoresement_form.data or endoresement_form.initial
    endorsement_map = core_models.RequirementKind.ENDORESEMENT_MAP
    return render(
        request,
        template_name="account_manager/cfsupport/endorsements/add.html",
        context={
            'endorsement_map': endorsement_map,
            'insurers': json.dumps(
                OrderedDict(core_models.EndorsementDetail.INSURERS)
            ),
            'initial_data': json.dumps(initial_data),
            'form': endoresement_form,
            'selected': 'endorsements',
        },
    )


@any_permission_satisfied(ENDORSEMENT_PERMS)
def endorsement_detail(request, end_id):
    end = get_object_or_404(core_models.EndorsementDetail, id=end_id)
    end_form = django_model_forms.modelform_factory(
        model=core_models.EndorsementDetail,
        exclude=(),
    )
    form = end_form(instance=end)
    return render(
        request,
        template_name='account_manager/cfsupport/endorsements/view.html',
        context={
            'form': form,
            'selected': 'endorsements',
        },
    )


@any_permission_satisfied('core.change_endorsementdetail')
def endorsement_edit(request, end_id):
    end = get_object_or_404(
        core_models.EndorsementDetail, id=end_id, completed_on__isnull=True
    )
    if request.method == 'POST':
        doc = request.FILES.get('reciept')
        if not doc:
            return HttpResponseBadRequest('Upload a file')

        _, ext = os.path.splitext(doc.name)
        path_ = 'uploads/core/endorsement_reciepts/{}{}'.format(
            end.id, ext
        )
        data = doc.read()
        attachments = [{
            'name': doc.name,
            'type': doc.content_type,
            'content': base64.b64encode(data)
        }]
        with default_storage.open(path_, 'wb') as f:
            f.write(data)
            del data
        doc = core_models.EndorsementReciept.objects.create(document=path_)
        end.reciept = doc
        end.completed_on = timezone.now()
        end.save(update_fields=['reciept', 'completed_on'])
        cf_cns.notify(
            nid='ENDORSEMENT_RECIEPT_NOTIFY_CUSTOMER',
            obj=end,
            to=end.customer_email,
            mandrill_template='newbase',
            attachments=attachments,
        )
        return redirect('account_manager:endorsement_detail', end_id=end_id)

    return render(
        request,
        template_name='account_manager/cfsupport/endorsements/edit.html',
        context={
            'selected': 'endorsements',
        }
    )
