__author__ = "Parag Tyagi"

import os
from django import forms


def validate_file_extension(value):
    ext = os.path.splitext(value.name)[1].lower()
    valid_extensions = ['.pdf']
    if ext not in valid_extensions:
        raise forms.ValidationError(
            u'Unsupported file extension. Please upload ({}) file'.format(', '.join(valid_extensions)))


class EditTransaction(forms.Form):
    policy_number = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={
                'placeholder': '(cut-copy-paste disabled)',
                'class': 'form-control cut-copy-paste-disabled',
                'type': 'password'
            }
        ),
        help_text='(refer document)'
    )
    re_enter_policy_number = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={'placeholder': '(cut-copy-paste disabled)', 'class': 'form-control cut-copy-paste-disabled'}
        )
    )
    premium_paid = forms.FloatField(
        required=True,
        widget=forms.TextInput(
            attrs={
                'placeholder': '(cut-copy-paste disabled)',
                'class': 'form-control cut-copy-paste-disabled',
                'type': 'password'
            }
        ),
        help_text='(refer document)'
    )
    re_enter_premium_paid = forms.FloatField(
        required=True,
        widget=forms.TextInput(
            attrs={'placeholder': '(cut-copy-paste disabled)', 'class': 'form-control cut-copy-paste-disabled'})
    )
    policy_document = forms.FileField(
        required=True,
        widget=forms.FileInput(attrs={'class': 'form-control', 'accept': '.pdf'}),
        validators=[validate_file_extension],
        help_text='(.pdf files only)'
    )
    payment_on = forms.DateTimeField(
        required=True,
        input_formats=['%Y/%m/%d %H:%M'],
        widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'datetimepicker_mask'}),
        help_text='(refer document)'
    )

    def clean(self):
        cleaned_data = super(EditTransaction, self).clean()

        # policy_number and re-enter
        if cleaned_data.get('policy_number') != cleaned_data.get('re_enter_policy_number'):
            self.add_error('re_enter_policy_number', u'Policy number(s) does not match.')

        # premium_paid and re-enter
        if cleaned_data.get('premium_paid') != cleaned_data.get('re_enter_premium_paid'):
            self.add_error('re_enter_premium_paid', u'Premium paid(s) does not match.')

        return cleaned_data
