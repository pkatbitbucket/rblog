import cf_cns

__author__ = "Parag Tyagi"

import base64
import copy

from django.core.management.base import BaseCommand
from django.core.mail import get_connection
from django.conf import settings
from django.template.loader import get_template

import utils
import account_manager.models as account_manager_models

logger = utils.create_logger("account_manager/rm_reassignment")


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--commit', action='store_true', dest='commit', default=False)

    def handle(self, *args, **options):
        '''
        Run this script for balancing users among all the active RMs
        '''
        is_commit = options['commit']

        logger.info("*********************** STARTING SCRIPT ***********************")
        logger.info("============= COMMITTING CHANGES: {}".format(is_commit))

        # Get all counts
        all_rm = account_manager_models.RelationshipManager.objects.all().order_by('id')
        active_rm = all_rm.filter(is_active=True)
        user_rm_count = account_manager_models.UserRelationshipManager.objects.count()
        active_rm_count = len(active_rm)
        per_rm_users = (user_rm_count/active_rm_count)

        logger.info("============= TOTAL USER ACCOUNTS: {}".format(user_rm_count))
        logger.info("============= TOTAL ACTIVE RMs: {}".format(active_rm_count))
        logger.info("============= THEREFORE, USERS per RM ({}/{} = {}):".format(user_rm_count, active_rm_count, per_rm_users))

        # Create extra User bucket
        logger.info("============= RM's AND THEIR USER COUNTs, ALSO CREATING `EXTRA USER BUCKET`:")

        extra_user_bucket = []
        active_rm_users_counts = {}
        for rm in all_rm:
            urm = account_manager_models.UserRelationshipManager.objects.filter(rm=rm).order_by('id')
            users = [u.user for u in urm]
            users_count = len(users)

            logger.info("(is_active: {}) {}: {}".format(rm.is_active, rm.email, users_count))

            extra_users = []
            if not rm.is_active:
                extra_users = users
                logger.info("({} users added to user bucket)".format(len(extra_users)))
            else:
                if users_count > per_rm_users:
                    diff = (users_count - per_rm_users)
                    extra_users = users[0:diff]
                    logger.info("({} users added to user bucket)".format(len(extra_users)))
                else:
                    logger.info("(nothing to add)".format(len(extra_users)))
                    active_rm_users_counts.update({rm.email: users_count})

            if extra_users:
                extra_user_bucket.extend(extra_users)

        user_bucket = copy.copy(extra_user_bucket)
        logger.info("============= EXTRA USER BUCKET COUNT: {}".format(len(extra_user_bucket)))

        # Assign User in `extra_user_bucket` to incomplete RM's and send mail
        logger.info("============= ASSIGNING/UN-ASSIGNING ACTIVE RM TO/FROM USERS AND SENDING MAILS TO RE-ASSIGNED USERS")

        counter = 1
        for rm_email, user_count in active_rm_users_counts.items():
            logger.info("{}. {}:".format(counter, rm_email))
            logger.info("User per RM should be: {}".format(per_rm_users))
            logger.info("Current user count: {}".format(user_count))

            required_users = per_rm_users - user_count
            rm = account_manager_models.RelationshipManager.objects.get(email=rm_email)

            if counter == len(active_rm_users_counts):
                # assign all user for last RM
                users = extra_user_bucket
            else:
                if required_users < len(extra_user_bucket):
                    users = extra_user_bucket[0:required_users]
                else:
                    users = extra_user_bucket

            count = 1
            for user in users:
                old_rm = user.urm.rm

                # Update new RM
                if is_commit:
                    user.urm.rm = rm
                    user.urm.save()

                logger.info("{}) {}: {} => {}".format(count, user.email, old_rm.email, rm.email))
                count += 1

            logger.info("Updated user count: {} ({} users assigned)".format((len(users)+user_count), required_users))
            del extra_user_bucket[0:len(users)]

            counter += 1

        logger.info("============= PENDING in USER BUCKET (IF ANY): {}".format(len(extra_user_bucket)))

        # Send mail to the list of user with re-assigned RMs
        logger.info("============= SENDING MAILS")
        if settings.PRODUCTION and is_commit:
            for user in user_bucket:
                self.send_mail(user)

    def send_mail(self, user):
        logger.info("{}".format(user.email))
        rm = user.urm.rm
        vcard_template = get_template('account_manager/rm_vcard.vcf')
        vcard_content = vcard_template.render({'rm': rm})
        user_name = utils.clean_name(user.get_full_name())

        cf_cns.notify(
            'RM_REASSIGNMENT',
            to=(user_name, user.email),
            sender_name=rm.name,
            sender_email=rm.email,
            attachments=[{
                'name': '{}.vcf'.format(rm.name),
                'type': 'text/vcard',
                'content': base64.b64encode(vcard_content),
            }],
            obj={
                'name': user_name,
                'user': user,
                'rm': rm,
            }
        )
