__author__ = 'parag'
from django.core.management.base import BaseCommand
from django.conf import settings
import os


class Command(BaseCommand):
    def handle(self, *args, **options):
        reports_folder = os.path.join(settings.STATIC_ROOT or settings.STATIC_PATH, 'reports/account_manager')
        if reports_folder.exists():
            for report in os.listdir(reports_folder):
                print 'removing {}'.format(report)
                os.remove(os.path.join(reports_folder, report))
        else:
            print 'nothing to clean'
