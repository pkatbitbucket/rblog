# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='OfflineTransaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('transaction_id', models.CharField(max_length=100)),
                ('proposer_name', models.CharField(max_length=100, null=True)),
                ('proposer_email', models.EmailField(max_length=100)),
                ('proposer_mobile', models.CharField(max_length=10)),
                ('proposer_address1', models.CharField(max_length=100, null=True)),
                ('proposer_address2', models.CharField(max_length=100, null=True)),
                ('proposer_address_landmark', models.TextField(null=True)),
                ('proposer_city', models.CharField(max_length=100, null=True)),
                ('proposer_state', models.CharField(max_length=100, null=True)),
                ('proposer_pincode', models.CharField(max_length=10, null=True)),
                ('policy_document', models.FileField(
                    null=True, upload_to=b'policy_document', blank=True)),
                ('premium_paid', models.FloatField(
                    max_length=15, null=True, blank=True)),
                ('type', models.CharField(max_length=20, choices=[
                 (b'health', b'health'), (b'motor', b'motor'), (b'travel', b'travel')])),
                ('status', models.CharField(max_length=20)),
                ('extra', jsonfield.fields.JSONField(default={}, blank=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='RelationshipManager',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('photo', models.ImageField(default=None, null=True,
                                            upload_to=b'account_manager/rm', blank=True)),
                ('type', models.CharField(max_length=20, choices=[
                 (b'health', b'health'), (b'motor', b'motor')])),
                ('mobile', models.CharField(max_length=10)),
                ('email', models.EmailField(unique=True, max_length=254)),
                ('freshdesk_responder_id', models.CharField(
                    max_length=20, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='UserAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('mobile', models.CharField(max_length=10)),
                ('address', models.TextField()),
                ('address_line1', models.CharField(max_length=100, blank=True)),
                ('address_line2', models.CharField(max_length=100, blank=True)),
                ('address_landmark', models.CharField(max_length=100, blank=True)),
                ('city', models.CharField(max_length=100)),
                ('state', models.CharField(max_length=100)),
                ('country', models.CharField(max_length=100, blank=True)),
                ('pincode', models.CharField(max_length=6)),
                ('photo', models.ImageField(
                    upload_to=b'account_manager/users', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='UserRelationshipManager',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='UserTransaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
        ),
    ]
