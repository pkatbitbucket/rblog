# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('account_manager', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='usertransaction',
            name='user',
            field=models.ForeignKey(
                related_name='transactions', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='userrelationshipmanager',
            name='rm',
            field=models.ForeignKey(to='account_manager.RelationshipManager'),
        ),
        migrations.AddField(
            model_name='userrelationshipmanager',
            name='user',
            field=models.OneToOneField(
                related_name='urm', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='useraccount',
            name='user',
            field=models.ForeignKey(
                related_name='useraccount', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='usertransaction',
            unique_together=set([('user', 'content_type', 'object_id')]),
        ),
    ]
