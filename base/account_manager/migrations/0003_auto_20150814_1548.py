# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account_manager', '0002_auto_20150807_1908'),
    ]

    operations = [
        migrations.AddField(
            model_name='relationshipmanager',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='relationshipmanager',
            name='freshdesk_responder_id',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='relationshipmanager',
            name='type',
            field=models.CharField(blank=True, max_length=20, choices=[(b'health', b'health'), (b'motor', b'motor')]),
        ),
    ]
