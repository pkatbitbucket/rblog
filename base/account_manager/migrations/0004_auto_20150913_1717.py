# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account_manager', '0003_auto_20150814_1548'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usertransaction',
            name='object_id',
            field=models.CharField(max_length=32),
        ),
    ]
