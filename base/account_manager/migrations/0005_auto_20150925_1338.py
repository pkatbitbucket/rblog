# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account_manager', '0004_auto_20150913_1717'),
    ]

    operations = [
        migrations.AddField(
            model_name='offlinetransaction',
            name='payment_on',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='useraccount',
            name='first_payment_date',
            field=models.DateTimeField(null=True),
        ),
    ]
