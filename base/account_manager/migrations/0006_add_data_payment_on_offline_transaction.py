# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from datetime import datetime
from django.db import models, migrations
from utils import get_property


def add_data_payment_on_offline_transaction(apps, schema_editor):
    OfflineTransaction = apps.get_model('account_manager', 'OfflineTransaction')

    for ot in OfflineTransaction.objects.all():
        data = json.loads(ot.extra)
        payment_on = get_property(data, 'extra.payment_on') or get_property(data, 'extra.payment_date')
        if payment_on:
            try:
                ot.payment_on = datetime.strptime(payment_on, '%d/%m/%Y')
            except ValueError:
                ot.payment_on = datetime.strptime(payment_on, '%m/%d/%Y')

            ot.save()


class Migration(migrations.Migration):

    dependencies = [
        ('account_manager', '0005_auto_20150925_1338'),
    ]

    operations = [
        migrations.RunPython(
            code=add_data_payment_on_offline_transaction,
            reverse_code=migrations.RunPython.noop,
        )
    ]
