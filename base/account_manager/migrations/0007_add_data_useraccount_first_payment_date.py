# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.contrib.contenttypes.models import ContentType

from utils import get_property


def add_data_useraccount_first_payment_date(apps, schema_editor):
    UserAccount = apps.get_model('account_manager', 'UserAccount')
    UserTransaction = apps.get_model('account_manager', 'UserTransaction')

    for ua in UserAccount.objects.all():
        uts = []
        for ut in UserTransaction.objects.filter(user=ua.user):
            Transaction = apps.get_model(ut.content_type.app_label, 'Transaction')
            try:
                transaction = Transaction.objects.get(id=ut.object_id)
            except Transaction.DoesNotExist:
                continue
            transaction.payment_on = transaction.payment_on or get_property(transaction, 'created_on') or get_property(transaction, 'created_date')
            uts.append(transaction)
            print "\n%s - %s - %s - %s - %s\n>>>>>>END" % (ut.object_id, ut.content_type.app_label, ut.content_type.model, transaction.payment_on, ua.user.email)

        if uts:
            sorted_uts = sorted(uts, key=lambda x: x.payment_on, reverse=False)
            ua.first_payment_date = sorted_uts[0].payment_on
            ua.save()


class Migration(migrations.Migration):

    dependencies = [
        ('account_manager', '0006_add_data_payment_on_offline_transaction'),
        ('health_product', '0002_auto_20150807_1908'),
        ('motor_product', '0005_auto_20150903_1733'),
        ('travel_product', '0022_delete_bajaj_individualslabs_under_family_plans'),
    ]

    operations = [
        migrations.RunPython(
            code=add_data_useraccount_first_payment_date,
            reverse_code=migrations.RunPython.noop,            
        )
    ]
