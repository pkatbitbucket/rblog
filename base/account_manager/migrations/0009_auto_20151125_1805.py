# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account_manager', '0008_auto_20151028_1336'),
    ]

    operations = [
        migrations.AddField(
            model_name='usertransaction',
            name='ticket_id',
            field=models.CharField(default='', help_text=b'Freshdesk ticket id', max_length=20, verbose_name=b'ticket id'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='usertransaction',
            name='ticket_status',
            field=models.IntegerField(help_text=b'Freshdesk ticket status', choices=[(2, b'Open'), (3, b'Pending'), (4, b'Resolved'), (5, b'Closed')], null=True, verbose_name=b'ticket status'),
        ),
    ]
