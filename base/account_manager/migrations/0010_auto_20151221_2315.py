# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account_manager', '0009_auto_20151125_1805'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='usertransaction',
            options={'permissions': (('add_offline_transaction', 'Can add offline transactions'), ('change_offline_transaction', 'Can edit offline transactions'))},
        ),
    ]
