from __future__ import absolute_import
import logging
import datetime
from copy import deepcopy
from collections import OrderedDict

from celery.contrib.methods import task_method
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator, MaxValueValidator
from jsonfield import JSONField
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.auth.hashers import UNUSABLE_PASSWORD_PREFIX
import freshdesk
from celery_app import app
from core.models import User
from health_product.models import Transaction as HealthTransaction
from motor_product.models import Transaction as MotorTransaction
from travel_product.models import Transaction as TravelTransaction
from wiki.models import Slab
from health_product.services.match_plan import MatchPlan
from utils import get_property, log_error
import health_product.queries as hqueries

logger = logging.getLogger(__name__)


class UserAccount(models.Model):
    user = models.ForeignKey(User, related_name='useraccount')
    mobile = models.CharField(max_length=10)
    address = models.TextField()
    address_line1 = models.CharField(max_length=100, blank=True)
    address_line2 = models.CharField(max_length=100, blank=True)
    address_landmark = models.CharField(max_length=100, blank=True)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    country = models.CharField(max_length=100, blank=True)
    pincode = models.CharField(max_length=6)
    photo = models.ImageField(upload_to='account_manager/users', blank=True)
    first_payment_date = models.DateTimeField(null=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True)
    updated_on = models.DateTimeField(auto_now=True, null=True)

    def __unicode__(self):
        return self.user.email

    @property
    def get_full_address(self):
        address = [self.address_line1, self.address_line2, self.address_landmark,
                   self.city, self.state, self.country, self.pincode]
        return ', '.join(['%s' % a for a in address if a])

    def activate_user(self, send_mail=True):
        if not self.user.is_active:
            self.user.is_active = True
            self.user.save()
            if send_mail:
                self.send_account_creation_mail()

    def send_account_creation_mail(self):
        from .utils import send_account_creation_mail
        send_account_creation_mail(self.user, self.user.transactions.first().transaction)


class UserTransactionManager(models.Manager):

    def get_for_transaction(self, transaction):
        content_type = ContentType.objects.get_for_model(transaction)
        return self.get(content_type__pk=content_type.id, object_id=transaction.id)

    def filter_for_transaction(self, transaction):
        content_type = ContentType.objects.get_for_model(transaction)
        return self.filter(content_type__pk=content_type.id, object_id=transaction.id)


class UserTransaction(models.Model):
    OPEN = 2
    PENDING = 3
    RESOLVED = 4
    CLOSED = 5
    _TICKET_STATUS_CHOICES = (
        (OPEN, 'Open'),
        (PENDING, 'Pending'),
        (RESOLVED, 'Resolved'),
        (CLOSED, 'Closed'),
    )
    user = models.ForeignKey(User, related_name='transactions')
    content_type = models.ForeignKey(ContentType)
    object_id = models.CharField(max_length=32)
    transaction = GenericForeignKey('content_type', 'object_id')
    created_on = models.DateTimeField(auto_now_add=True, null=True)
    updated_on = models.DateTimeField(auto_now=True, null=True)
    ticket_id = models.CharField('ticket id', max_length=20, help_text='Freshdesk ticket id')
    ticket_status = models.IntegerField('ticket status', null=True,
                                        choices=_TICKET_STATUS_CHOICES, help_text='Freshdesk ticket status')

    objects = UserTransactionManager()

    class Meta:
        # TODO: remove user from this
        unique_together = ('user', 'content_type', 'object_id')
        permissions = (
            ('add_offline_transaction', 'Can add offline transactions'),
            ('change_offline_transaction', 'Can edit offline transactions'),
        )

    def __unicode__(self):
        if self.transaction:
            return u'{} - {}'.format(self.content_type, self.transaction.transaction_id)

    def tr_json(self, request):
        if self.type == 'car' or self.type == 'bike':
            return get_motor_policy(self.transaction)
        elif self.type == 'health':
            return get_health_policy(request, self.transaction)
        elif self.type == 'travel':
            return get_travel_policy(self.transaction)
        else:
            return 'car'

    @property
    def type(self):
        if isinstance(self.transaction, MotorTransaction):
            if self.transaction.vehicle_type == 'Private Car':
                return 'car'
            elif self.transaction.vehicle_type == 'Twowheeler':
                return 'bike'
        elif isinstance(self.transaction, HealthTransaction):
            return 'health'
        elif isinstance(self.transaction, TravelTransaction):
            return 'travel'

    @app.task(filter=task_method, queue='escalations')
    def create_ticket(self):
        from .utils import transaction_field_mapping, update_transaction_ticket
        ticket_description = """\
        Transaction ID: {} ({})
        Product: {}
        """.format(self.transaction.id, self.transaction.transaction_id, self.type)
        if not self.ticket_id:
            if get_property(self.transaction, 'quote.policy_type') == 'expired':
                ticket_type = 'Offline Policy received'
            else:
                ticket_type = 'Online Policy received'

            transaction_details = transaction_field_mapping(self.transaction)

            ticket_subject = u' - '.join((
                self.type,
                'CFOX {}'.format(self.transaction.id),
                transaction_details.get('insurer_title', ''),
                u' '.join((
                    transaction_details.get('first_name', ''),
                    transaction_details.get('middle_name', ''),
                    transaction_details.get('last_name', ''),
                ))
            ))

            ticket_data = dict(
                email='alerts@coverfoxmail.com',
                subject=ticket_subject,
                description=ticket_description,
                responder_id=self.user.urm.rm.freshdesk_responder_id,
                ticket_type=ticket_type,
                priority=3
            )
            chain = freshdesk.create_ticket.s(**ticket_data) | update_transaction_ticket.s(self.id, 'create_ticket')
            return chain()

    @app.task(filter=task_method, queue='escalations')
    def close_ticket(self):
        from .utils import update_transaction_ticket
        if self.ticket_id and self.ticket_status != self.CLOSED:
            ticket_data = dict(ticket_id=self.ticket_id, status=self.CLOSED)
            chain = freshdesk.update_ticket.s(**ticket_data) | update_transaction_ticket.s(self.id, 'close_ticket')
            return chain()

    @app.task(filter=task_method, queue='escalations')
    def refresh_ticket_status(self):
        from .utils import update_transaction_ticket
        if self.ticket_id:
            ticket_data = dict(ticket_id=self.ticket_id)
            chain = freshdesk.view_ticket.s(**ticket_data) | update_transaction_ticket.s(self.id, 'refresh_ticket_status')
            return chain()


class RelationshipManager(models.Model):
    type_choices = (
        ('health', 'health'),
        ('motor', 'motor'),
    )

    name = models.CharField(max_length=100)
    photo = models.ImageField(upload_to='account_manager/rm', null=True, default=None, blank=True)
    type = models.CharField(choices=type_choices, max_length=20, blank=True)
    mobile = models.CharField(max_length=10)
    email = models.EmailField(unique=True)
    freshdesk_responder_id = models.CharField(max_length=20, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True)
    updated_on = models.DateTimeField(auto_now=True, null=True)

    def __unicode__(self):
        return self.name

    @property
    def get_customer_count(self):
        return self.userrelationshipmanager_set.count()

    @property
    def get_activated_customer_count(self):
        return self.userrelationshipmanager_set.exclude(user__password__startswith=UNUSABLE_PASSWORD_PREFIX).count()

    @property
    def get_transactions_count(self):
        return UserTransaction.objects.filter(user__urm__rm=self).count()

    @property
    def get_customer_havingpolicydoc_count(self):
        return len([ut for ut in UserTransaction.objects.filter(user__urm__rm=self) if ut.transaction.policy_document])

    def clean(self):
        if self.is_active and not self.freshdesk_responder_id:
            raise ValidationError({'freshdesk_responder_id': 'Cannot be empty if RM is active'})


class UserRelationshipManager(models.Model):
    user = models.OneToOneField(User, related_name='urm')
    rm = models.ForeignKey(RelationshipManager)
    created_on = models.DateTimeField(auto_now_add=True, null=True)
    updated_on = models.DateTimeField(auto_now=True, null=True)


class OfflineTransaction(models.Model):
    type_choices = (
        ('health', 'health'),
        ('motor', 'motor'),
        ('travel', 'travel'),
    )
    transaction_id = models.CharField(max_length=100)
    proposer_name = models.CharField(max_length=100, null=True)
    proposer_email = models.EmailField(max_length=100)
    proposer_mobile = models.CharField(max_length=10)
    proposer_address1 = models.CharField(max_length=100, null=True)
    proposer_address2 = models.CharField(max_length=100, null=True)
    proposer_address_landmark = models.TextField(null=True)
    proposer_city = models.CharField(max_length=100, null=True)
    proposer_state = models.CharField(max_length=100, null=True)
    proposer_pincode = models.CharField(max_length=10, null=True)
    policy_document = models.FileField(upload_to='policy_document', null=True, blank=True)
    premium_paid = models.FloatField(max_length=15, null=True, blank=True)
    payment_on = models.DateTimeField(null=True)
    type = models.CharField(choices=type_choices, max_length=20)
    status = models.CharField(max_length=20)
    extra = JSONField(blank=True, default={})
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)


def get_health_policy(request, transaction):
    data_to_score = hqueries.prepare_post_data_to_score(deepcopy(transaction.extra))
    slab = Slab.objects.get(id=transaction.slab_id)
    mp = MatchPlan(data_to_score, slab=slab, request=request)
    slab_details = get_property(mp.calculate_score(), '0')

    address = OrderedDict([
        ('line_1', transaction.proposer_address1),
        ('line_2', transaction.proposer_address2),
        ('landmark', transaction.proposer_address_landmark),
        ('city', transaction.proposer_city),
        ('state', transaction.proposer_state),
        ('pincode', transaction.proposer_pincode),
    ])

    hospitals = ""
    if transaction.proposer_pincode:
        try:
            hospitals = hqueries.get_city_and_hospitals(int(transaction.proposer_pincode))
        except:
            log_error(logger, msg="Internal Server Error: Unknown error received while fetching hospitals for pincode {}".format(transaction.proposer_pincode))


    return OrderedDict([
            ('id', transaction.id),
            ('transaction_id', transaction.transaction_id),
            ('policy_status', transaction.status),
            ('policy_name', get_property(transaction, 'slab.health_product.title')),
            ('policy_number', transaction.policy_number),
            ('policy_start_date', str(transaction.policy_start_date)),
            ('policy_end_date', str(transaction.policy_end_date)),
            ('policy_document', transaction.policy_document.url if transaction.policy_document else ""),
            ('policy_tenure', str(transaction.policy_end_date - transaction.policy_start_date) if transaction.policy_end_date and transaction.policy_start_date else None),
            ('policy_type', get_property(transaction, 'slab.health_product.policy_type')),
            ('policy_isrolloverportable_case', ''),
            ('policy_previous', ''),
            ('policy_zone_cover', ''),
            ('policy_covers', ', '.join([m['person'] for m in transaction.extra['members']])),
            ('policy_holder_firstname', transaction.proposer_first_name),
            ('policy_holder_lastname', transaction.proposer_last_name),
            ('policy_holder_email', transaction.proposer_email),
            ('policy_holder_mobile', transaction.proposer_mobile),
            ('policy_address_dict', address),
            ('policy_address_oneline', ', '.join(['%s' % (v) for k, v in address.iteritems() if v])),
            ('policy_category', 'health'),
            ('payment_on', str(transaction.payment_on)),
            ('premium_paid', transaction.premium_paid),
            ('insurer_slug', get_property(transaction, 'slab.health_product.insurer.slug')),
            ('insurer_title', get_property(transaction, 'slab.health_product.insurer.title')),
            ('insured', get_healthpolicy_members(transaction)),
            ('hospitals', hospitals),
            ('five_year_projection', get_property(slab_details, 'five_year_projection')),
            ('good_to_have', [g[1]['title'] for g in slab_details['score'] if g[1]['importance'] == 'MID' and g[1]['available']] if slab_details else []),
            ('additional_benefits', [g[1]['title'] for g in slab_details['score'] if g[1]['importance'] == 'LOW' and g[1]['available']] if slab_details else []),
            ('plan_conditions', {
                'since_day1': [],
                'after_2years': [con['pterm']['name'] for con in slab_details['general']['conditions'] if int(con['period']) / 12 == 2] if slab_details else [],
                'never_covered': [],
            }),
        ])


def get_healthpolicy_members(transaction):

    GENDER = {
        '1': 'Male',
        '2': 'Female',
        'male': 'Male',
        'female': 'Female',
    }

    ins = OrderedDict()
    insured = []
    if 'proposal_data' in transaction.extra:
        if 'memObjList' in transaction.extra['proposal_data']:
            for i in transaction.extra['proposal_data']['memObjList']:
                ins = OrderedDict([
                    ('Name', i['FirstName'].capitalize() +
                     ' ' + i['LastName'].capitalize()),
                    ('Relationship with policy holder', i['Relation']),
                    ('Gender', i['Gender']),
                    ('Date of Birth', i['DateOfBirth']),
                    ('Nominee', i['Nominee']),
                ])
                insured.append(ins)
    elif 'members' in transaction.extra:
        for i in transaction.extra['members']:
            ins = OrderedDict([
                ('Relationship with policy holder', i['person']),
                ('Gender', GENDER.get(str(i['gender']), i['gender'])),
                ('Age', i['age']),
            ])
            insured.append(ins)

    return insured


def get_motor_policy(transaction):
    address = OrderedDict([
            ('line_1', get_property(transaction, 'proposer.address.line_1')),
            ('line_2', get_property(transaction, 'proposer.address.line_2')),
            ('landmark', get_property(transaction, 'proposer.address.landmark')),
            ('city', get_property(transaction, 'proposer.address.city')),
            ('state', get_property(transaction, 'proposer.address.state')),
            ('pincode', get_property(transaction, 'proposer.address.pincode')),
        ])

    return OrderedDict([
                ('id', transaction.id),
                ('transaction_id', transaction.transaction_id),
                ('policy_status', transaction.status),
                ('policy_name', get_property(transaction, 'insurer.title')),
                ('policy_number', transaction.policy_number),
                ('rsa_policy_number', transaction.rsa_policy_number),
                ('policy_start_date', str(transaction.policy_start_date)),
                ('policy_end_date', str(transaction.policy_end_date)),
                ('policy_document', get_property(transaction, 'policy_document.url')),
                ('rsa_policy', get_property(transaction, 'rsa_policy.url')),
                ('policy_tenure', str(transaction.policy_end_date - transaction.policy_start_date)
                    if transaction.policy_end_date and transaction.policy_start_date else None),
                ('policy_covers', ' '.join([get_property(transaction, 'quote.vehicle.make.name') or '',
                                            get_property(transaction, 'quote.vehicle.model.name') or '']).strip()),
                ('policy_type', get_property(transaction, 'quote.vehicle.model.vehicle_type')),
                ('policy_isrolloverportable_case', ''),
                ('policy_previous', ''),
                ('policy_zone_cover', ''),
                ('policy_holder_firstname', get_property(transaction, 'proposer.first_name')),
                ('policy_holder_lastname', get_property(transaction, 'proposer.last_name')),
                ('policy_holder_email', get_property(transaction, 'proposer.email')),
                ('policy_holder_mobile', get_property(transaction, 'proposer.mobile')),
                ('policy_address_dict', address),
                ('policy_address_oneline', ', '.join(['{}'.format(v) for k, v in address.iteritems() if v])),
                ('policy_category', 'motor'),
                ('payment_on', str(transaction.payment_on)),
                ('premium_paid', transaction.premium_paid),
                ('insurer_slug', get_property(transaction, 'insurer.slug')),
                ('insurer_title', get_property(transaction, 'insurer.title')),
                ('vehicle_model', get_property(transaction, 'quote.vehicle.model.name')),
                ('vehicle_make', get_property(transaction, 'quote.vehicle.make.name')),
                ('vehicle_variant', get_property(transaction, 'quote.vehicle.variant')),
                ('vehicle_fuel_type', get_property(transaction, 'quote.vehicle.fuel_type')),
                ('vehicle_rto', get_property(transaction, 'raw.user_form_details.vehicle_rto')),
                ('vehicle_registration_number', get_property(transaction, 'raw.user_form_details.vehicle_reg_no')),
                ('vehicle_registation_date', get_property(transaction, 'raw.user_form_details.vehicle_reg_date')),
                ('vehicle_engine_number', get_property(transaction, 'raw.user_form_details.vehicle_engine_no')),
            ])


def get_travel_policy(transaction):

    policy_start_date = get_property(transaction, 'extra.search_data.from_date')
    policy_start_date = datetime.datetime.strptime(policy_start_date, '%d-%b-%Y') if policy_start_date else None
    policy_end_date = get_property(transaction, 'extra.search_data.to_date')
    policy_end_date = datetime.datetime.strptime(policy_end_date, '%d-%b-%Y') if policy_end_date else None

    return OrderedDict([
        ('id', transaction.id),
        ('transaction_id', transaction.transaction_id),
        ('policy_number', transaction.policy_number),
        ('policy_status', transaction.status),
        ('premium_paid', str(get_property(transaction, 'slab.premium'))),
        ('policy_holder_firstname', transaction.first_name),
        ('policy_holder_lastname', transaction.last_name),
        ('policy_holder_email', transaction.email),
        ('policy_holder_mobile', transaction.mobile),
        ('policy_start_date', policy_start_date),
        ('policy_end_date', policy_end_date),
        ('policy_tenure', str(policy_end_date - policy_start_date)
            if policy_end_date and policy_start_date else None),
        ('policy_document', get_property(transaction, 'policy_document.url')),
        ('policy_name', get_property(transaction, 'data.plan_details.plan_type')),
        ('payment_on', str(transaction.payment_on)),
        ('policy_category', 'travel'),
        ('insurer_slug', get_property(transaction, 'slab.plan.insurer.slug')),
        ('insurer_title', get_property(transaction, 'slab.plan.insurer.name')),
    ])


def get_user_details(user):
    useraccount = user.useraccount.all()[0] if user.useraccount.all() else None

    address = OrderedDict([
        ('address_line1', get_property(useraccount, 'address_line1')),
        ('address_line2', get_property(useraccount, 'address_line2')),
        ('address_landmark', get_property(useraccount, 'address_landmark')),
        ('city', get_property(useraccount, 'city')),
        ('state', get_property(useraccount, 'state')),
        ('country', get_property(useraccount, 'country')),
        ('pincode', get_property(useraccount, 'pincode')),
    ])

    return OrderedDict([
        ('user_id', user.id),
        ('full_name', user.get_full_name()),
        ('first_name', user.first_name),
        ('middle_name', user.middle_name),
        ('last_name', user.last_name),
        ('birth_date', user.birth_date),
        ('email', user.email),
        ('username', user.username),
        ('mobile', get_property(useraccount, 'mobile')),
        ('username', user.username),
        ('slug', user.slug),
        ('gender', user.gender),
        ('date_joined', str(user.date_joined)),
        ('last_login', str(user.last_login)),
        ('last_updated', str(user.updated_on)),
        ('address_oneline', get_property(useraccount, 'get_full_address')),
        ('address_dict', address),
    ])


def get_rm(user):
    rm = None
    try:
        rm = UserRelationshipManager.objects.get(user=user).rm
    except UserRelationshipManager.DoesNotExist:
        rm = None

    return rm
