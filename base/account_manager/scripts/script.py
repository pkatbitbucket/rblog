import os
import cf_cns

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
import csv
import itertools
import time
import datetime
import shutil
from collections import OrderedDict
from django.db.models import Q
from django.conf import settings
from django.apps import apps
from django.contrib.contenttypes.models import ContentType
from django.core.mail import get_connection
from django.core.urlresolvers import reverse
from account_manager.utils import create_useraccount_add_transaction
from core.models import Tracker, User
from health_product.models import Transaction as HealthTransaction, TRANSACTION_STATUS_CHOICES
from motor_product.models import Transaction as MotorTransaction
from account_manager.models import RelationshipManager, UserRelationshipManager, UserTransaction, UserAccount, get_user_details, get_rm, OfflineTransaction
from wiki.models import Pincode

django.setup()

GETMODEL = {
    'Motor': apps.get_model('motor_product', 'Transaction'),
    'Health': apps.get_model('health_product', 'Transaction')
}

FIELD_MAPPING = {
    'Motor': {
        'email': 'proposer__email',
        'mobile': 'proposer__mobile',
        'status': 'status',
        'premium_paid': 'premium_paid__icontains',
    },
    'Health': {
        'email': 'proposer_email',
        'mobile': 'proposer_mobile',
        'status': 'status',
        'premium_paid': 'premium_paid__icontains',
    },
}

# RUN LOCALLY
# FOR GENERATING UPDATED POLICY DATA
# UPDATED DATA - policy document path and filenames
# Basically filling policy document path (folder, subfolder, filename,
# foundstatus)


def step_1():

    policy_docs_path = '/home/parag/projects/coverfox/dropbox_policy_document'
    csvfile = 'account_manager/scripts/policy_data.csv'     # Read from
    reader = csv.DictReader(open(csvfile))

    filepath = 'account_manager/scripts/policy_data_step_1.csv'     # Write to
    if os.path.exists(filepath):
        os.remove(filepath)

    # write dict header
    f = open(filepath, 'a')

    header_added = False
    for row in reader:

        data = OrderedDict([
            ('bussiness_type', row['bussiness_type'].strip()),
            ('name', row['customer_name'].strip()),
            ('email', row['email_id'].strip()),
            ('mobile', row['phone_number'].strip()),
            ('doc_search_status', ''),
            ('doc_folder', ''),
            ('doc_subfolder', ''),
            ('doc_filename', ''),
            ('file_name', row['file_name'].strip()),
            ('file_number', row['file_number'].strip()),
            ('policy_punch_date', row['policy_punch_date'].strip()),
            ('policy_number', row['policy_number'].strip()),
            ('policy_start_date', row['policy_start_date'].strip()),
            ('policy_end_date', row['policy_end_date'].strip()),
            ('premium_amount', row['premium_amount'].strip()),
            ('policy_drag_status', row['policy_drag_status'].strip()),
            ('issuance_status', row['issuance_status'].strip()),
            ('insurance_company', row['insurance_company'].strip()),
            ('product_name', row['product_name'].strip()),
        ])

        if not header_added:
            w = csv.DictWriter(f, data.keys())
            w.writeheader()
            header_added = True

        found = False
        if data['issuance_status'] == 'ISSUED':
            sub_folder = ' - '.join([x.strip()
                                     for x in data['file_number'].split('-')])
            dir1 = 'CFOXH-2015'
            dir2 = 'CFOXM-2015'
            path1 = os.path.join(policy_docs_path, dir1, sub_folder)
            path2 = os.path.join(policy_docs_path, dir2, sub_folder)

            path = None
            if os.path.exists(path1):
                path = path1
                main_dir = dir1
            elif os.path.exists(path2):
                path = path2
                main_dir = dir2
            else:
                pass

            if path:
                path, dirs, files = os.walk(path).next()
                if len(files) == 0:
                    pass
                elif len(files) == 1:
                    found = True
                    filename = files[0]
                else:
                    pattern = '(Policy Copy)'
                    match_file_name = [f for f in files if pattern in f]
                    if match_file_name and len(match_file_name) == 1:
                        found = True
                        filename = match_file_name[0]
            else:
                pass

        data['doc_search_status'] = 'NOT FOUND'
        data['doc_folder'] = ''
        data['doc_subfolder'] = ''
        data['doc_filename'] = ''
        if found:
            data['doc_search_status'] = 'FOUND'
            data['doc_folder'] = main_dir
            data['doc_subfolder'] = sub_folder
            data['doc_filename'] = filename
        w.writerow(data)

    print "COMPLETED !!!"

# RUN ON SERVER
# Using step_1 data and filling the matching and filling transaction match data


def step_2():
    total_lines = 0
    with open('account_manager/scripts/policy_data_step_1.csv') as f:
        total_lines = sum(1 for _ in f)

    script_start_time = time.time()

    # remove log file if already exists
    files = ['log_step_2.txt', 'policy_data_step_2.csv']
    for f in files:
        filepath = 'account_manager/scripts/' + f
        if os.path.exists(filepath):
            os.remove(filepath)

    # read csv file
    csvfile = 'account_manager/scripts/policy_data_step_1.csv'      # Read from
    reader = csv.DictReader(open(csvfile))

    # start writing in updated file
    new_u_filepath = 'account_manager/scripts/policy_data_step_2.csv'       # Write to
    f = open(filepath, 'a')

    motor_count = []
    health_count = []
    header_added = False
    i = 0
    j = 0
    for row in reader:
        return_count = ''
        perfect_match = ''
        many_transactions = ''
        single_transaction = ''
        data = OrderedDict([
            ('bussiness_type', row['bussiness_type'].strip()),
            ('name', row['name'].strip()),
            ('email', row['email'].strip()),
            ('mobile', row['mobile'].strip()),
            ('doc_search_status', row['doc_search_status'].strip()),
            ('doc_folder', row['doc_folder'].strip()),
            ('doc_subfolder', row['doc_subfolder'].strip()),
            ('doc_filename', row['doc_filename'].strip()),
            ('policy_search_status', ''),
            ('transaction_id', ''),
            ('premium_paid', ''),
            ('file_name', row['file_name'].strip()),
            ('file_number', row['file_number'].strip()),
            ('policy_punch_date', row['policy_punch_date'].strip()),
            ('policy_number', row['policy_number'].strip()),
            ('policy_start_date', row['policy_start_date'].strip()),
            ('policy_end_date', row['policy_end_date'].strip()),
            ('premium_amount', row['premium_amount'].strip()),
            ('policy_drag_status', row['policy_drag_status'].strip()),
            ('issuance_status', row['issuance_status'].strip()),
            ('insurance_company', row['insurance_company'].strip()),
            ('product_name', row['product_name'].strip()),
        ])

        if not header_added:
            w = csv.DictWriter(f, data.keys())
            w.writeheader()
            header_added = True

        if (data['bussiness_type'] == 'Motor' or data['bussiness_type'] == 'Health') and data['issuance_status'] == 'ISSUED':
            return_count, perfect_match, many_transactions, single_transaction = transaction_match(
                data, data['bussiness_type'])
            if data['bussiness_type'] == 'Motor':
                motor_count.append(return_count)
            elif data['bussiness_type'] == 'Health':
                health_count.append(return_count)
            else:
                pass

            # printing progress
            i += 1
            percen = (i * 100) / total_lines
            if percen != j:
                now_time = time.time()
                diff_secs = now_time - script_start_time
                tim = str(datetime.timedelta(seconds=round(diff_secs)))
                motor_percen = (motor_count.count(1) * 100) / len(motor_count)
                health_percen = (health_count.count(1) *
                                 100) / len(health_count)
                print "COMPLETED.......................{}% (motor - {}%, health - {}%) ({})".format(str(percen), motor_percen, health_percen, tim)
            else:
                pass
            j = percen
        else:
            pass

        # write row updated CSV
        if perfect_match:
            data['transaction_id'] = single_transaction[0].transaction_id
            data['policy_search_status'] = 'PERFECT MATCH'
            data['premium_paid'] = single_transaction[0].premium_paid
        else:
            if many_transactions:
                temp = []
                for r in many_transactions:
                    temp.append(r.transaction_id)

                data['transaction_id'] = ', '.join(temp)
                data['policy_search_status'] = 'MORE THAN ONE'
            else:
                data['policy_search_status'] = 'NOT FOUND'
        w = csv.DictWriter(f, data.keys())
        w.writerow(data)

    # Printing final result
    print "Total rows (health + motor): {}".format(len(motor_count) + len(health_count))
    print "\n"

    print "Motor:"
    print "Total count:      {}".format(len(motor_count))
    print "No match:         {}".format(motor_count.count(0))
    print "Exact match:      {}".format(motor_count.count(1))
    print "More than one:    {}".format(motor_count.count(2))
    print "Match percentage: {}%".format((motor_count.count(1) * 100) / len(motor_count))
    print "\n"

    print "Health:"
    print "Total count:      {}".format(len(health_count))
    print "No match:         {}".format(health_count.count(0))
    print "Exact match:      {}".format(health_count.count(1))
    print "More than one:    {}".format(health_count.count(2))
    print "Match percentage: {}%".format((health_count.count(1) * 100) / len(health_count))
    print "\n"


def transaction_match(data, type):
    return_count = ''
    perfect_match = ''
    many_transactions = ''
    single_transaction = ''

    file_name = 'account_manager/scripts/log_step_2.txt'
    if type == 'Motor':
        field_value_mapping = {
            'proposer__email': data['email'],
            'proposer__mobile': data['mobile'],
            'status': 'COMPLETED',
            'premium_paid__icontains': data['premium_amount'],
        }

    elif type == 'Health':
        field_value_mapping = {
            'proposer_email': data['email'],
            'proposer_mobile': data['mobile'],
            'status': 'COMPLETED',
            'premium_paid__icontains': data['premium_amount'],
        }

    # All fields combination
    combs = [
        (FIELD_MAPPING[type]['email'], FIELD_MAPPING[type]['status']),
        (FIELD_MAPPING[type]['mobile'], FIELD_MAPPING[type]['status']),
    ]

    # Fill values in combinations
    kwargss = []
    for com in combs:
        temp = dict()
        for c in com:
            if c:
                temp[c] = field_value_mapping[c]
        kwargss.append(temp)

    # calculations
    many_transactions = None
    perfect_match = False
    f = open(file_name, 'a')
    f.write('>>>>>>>>>>>>>>>>>>> ROWSTART\n')
    f.write('>>>>>>>>>>>>>>>>>>> TYPE = ' + type + '\n')
    f.write('>>>>>>>>>>>>>>>>>>> ORIGINALROWDATA\n')
    f.write(str(data) + '\n')
    for kwargs in kwargss:
        kwargs = dict((k, v) for k, v in kwargs.iteritems() if v)

        if len(kwargs) == 1 and 'status' in kwargs:
            continue

        if kwargs:
            f.write('>>>>>>>>>>>>>>>>>>> KWARGS\n')
            f.write(str(kwargs) + '\n')
            single_transaction = GETMODEL[type].objects.filter(**kwargs)
            f.write('>>>>>>>>>>>>>>>>>>> SEARCHLENGTH == ' +
                    str(len(single_transaction)) + '\n')
            if len(single_transaction) == 1:
                if data['premium_amount'] and single_transaction[0].premium_paid:
                    if -5 < (float(data['premium_amount']) - float(single_transaction[0].premium_paid)) < 5:
                        perfect_match = True
                        break
                else:
                    perfect_match = True
                    break
            elif len(single_transaction) > 1:
                many_transactions = single_transaction

    # if more than one try once again with premium_amount as well
    if many_transactions:
        if data['email'] and data['mobile'] and data['premium_amount']:
            kwargs = {
                FIELD_MAPPING[type]['email']: data['email'],
                FIELD_MAPPING[type]['mobile']: data['mobile'],
                FIELD_MAPPING[type]['premium_paid']: data['premium_amount'],
            }
            t1 = GETMODEL[type].objects.filter(**kwargs)
            f.write('>>>>>>>>>>>>>>>>>>> TRYINGONCEMORE\n')
            f.write(str(kwargs) + '\n')
            f.write('>>>>>>>>>>>>>>>>>>> SEARCHLENGTH == ' + str(len(t1)) + '\n')
            if len(t1) == 1:
                single_transaction = t1
                perfect_match = True
            else:
                pass

    # perfect match
    if perfect_match:
        return_count = 1
        f.write('>>>>>>>>>>>>>>>>>>> PERFECTMATCH\n')
        for r in single_transaction:
            temp = dict()
            if type == 'Motor':
                temp['email'] = r.proposer.email if r.proposer else 'NA'
                temp['mobile'] = r.proposer.mobile if r.proposer else 'NA'
            elif type == 'Health':
                temp['email'] = r.proposer_email
                temp['mobile'] = r.proposer_mobile
            temp['tid'] = r.transaction_id
            temp['amount'] = r.premium_paid
            temp['status'] = r.status
            f.write(str(temp))
            f.write('\n')

    # not perfect (maybe not found or more than one)
    if not perfect_match:
        if many_transactions:
            return_count = 2
            f.write('>>>>>>>>>>>>>>>>>>> MORETHANONE\n')
            for r in many_transactions:
                temp = dict()
                if type == 'Motor':
                    temp['email'] = r.proposer.email if r.proposer else 'NA'
                    temp['mobile'] = r.proposer.mobile if r.proposer else 'NA'
                elif type == 'Health':
                    temp['email'] = r.proposer_email
                    temp['mobile'] = r.proposer_mobile
                temp['tid'] = r.transaction_id
                temp['amount'] = r.premium_paid
                temp['status'] = r.status
                f.write(str(temp))
                f.write('\n')
        else:
            return_count = 0
    f.write('>>>>>>>>>>>>>>>>>>> ROWEND\n\n\n\n')
    f.close()

    return return_count, perfect_match, many_transactions, single_transaction


# RUN LOCALLY
# Renaming policy document and put it in a new folder
# File managing
# policy_data_step_3.csv file comes from soman team after giving them
# policy_data_step_2.csv for QC
def step_3():
    filepath = '/home/parag/projects/coverfox/policy_data_step_2.csv'           # Read from
    reader = csv.DictReader(open(filepath))

    # start writing in updated file
    new_u_filepath = '/home/parag/projects/coverfox/rblog/base/account_manager/scripts/final_new_policy_data.csv'      # Write to
    if os.path.exists(new_u_filepath):
        os.remove(new_u_filepath)

    f = open(new_u_filepath, 'a')

    i = 1
    header_added = False
    for row in reader:
        data = OrderedDict([
            ('bussiness_type', row['bussiness_type'].strip()),
            ('name', row['name'].strip()),
            ('email', row['email'].strip()),
            ('mobile', row['mobile'].strip()),
            ('doc_search_status', row['doc_search_status'].strip()),
            ('doc_folder', row['doc_folder'].strip()),
            ('doc_subfolder', row['doc_subfolder'].strip()),
            ('doc_filename', row['doc_filename'].strip()),
            ('doc_newfilename', ''),
            ('policy_search_status', row['policy_search_status'].strip()),
            ('transaction_id', row['transaction_id'].strip()),
            ('premium_paid', row['premium_paid'].strip()),
            ('file_name', row['file_name'].strip()),
            ('file_number', row['file_number'].strip()),
            ('policy_punch_date', row['policy_punch_date'].strip()),
            ('policy_number', row['policy_number'].strip()),
            ('policy_start_date', row['policy_start_date'].strip()),
            ('policy_end_date', row['policy_end_date'].strip()),
            ('premium_amount', row['premium_amount'].strip()),
            ('policy_drag_status', row['policy_drag_status'].strip()),
            ('issuance_status', row['issuance_status'].strip()),
            ('insurance_company', row['insurance_company'].strip()),
            ('product_name', row['product_name'].strip()),
        ])

        if not header_added:
            w = csv.DictWriter(f, data.keys())
            w.writeheader()
            header_added = True

        if data['doc_search_status'] == 'FOUND' and data['policy_search_status'] == 'PERFECT MATCH':
            path = '/home/parag/projects/coverfox/dropbox_policy_document'
            current_file_path = os.path.join(path, data['doc_folder'], data[
                                             'doc_subfolder'], data['doc_filename'])
            if os.path.exists(current_file_path):
                name, ext = os.path.splitext(data['doc_filename'])
                new_path = '/home/parag/projects/coverfox/new_policy_document'
                new_filename = data['transaction_id'] + ext
                new_file_path = os.path.join(new_path, new_filename)
                if not os.path.exists(new_file_path):
                    shutil.copy2(current_file_path, new_file_path)
                    data['doc_newfilename'] = new_filename
                    print i
                    i += 1

        w = csv.DictWriter(f, data.keys())
        w.writerow(data)


def check_duplicates():
    new_u_filepath = '/home/parag/projects/coverfox/final_new_policy_data.csv'
    reader = csv.DictReader(open(new_u_filepath))
    all_transactions = []
    duplicate_transactions = []
    for row in reader:
        data = OrderedDict([
            ('bussiness_type', row['bussiness_type'].strip()),
            ('name', row['name'].strip()),
            ('email', row['email'].strip()),
            ('mobile', row['mobile'].strip()),
            ('doc_search_status', row['doc_search_status'].strip()),
            ('doc_folder', row['doc_folder'].strip()),
            ('doc_subfolder', row['doc_subfolder'].strip()),
            ('doc_filename', row['doc_filename'].strip()),
            ('doc_newfilename', ''),
            ('policy_search_status', row['policy_search_status'].strip()),
            ('transaction_id', row['transaction_id'].strip()),
            ('premium_paid', row['premium_paid'].strip()),
            ('file_name', row['file_name'].strip()),
            ('file_number', row['file_number'].strip()),
            ('policy_punch_date', row['policy_punch_date'].strip()),
            ('policy_number', row['policy_number'].strip()),
            ('policy_start_date', row['policy_start_date'].strip()),
            ('policy_end_date', row['policy_end_date'].strip()),
            ('premium_amount', row['premium_amount'].strip()),
            ('policy_drag_status', row['policy_drag_status'].strip()),
            ('issuance_status', row['issuance_status'].strip()),
            ('insurance_company', row['insurance_company'].strip()),
            ('product_name', row['product_name'].strip()),
        ])

        if data['transaction_id'] and data['policy_search_status'] == 'PERFECT MATCH':
            all_transactions.append(data['transaction_id'])

    for t in all_transactions:
        if all_transactions.count(t) > 1:
            duplicate_transactions.append(t)

    return list(set(duplicate_transactions))


def sheet_useraccounttransaction_create():
    maindata = 'account_manager/scripts/final_new_policy_data.csv'
    reader = csv.DictReader(open(maindata))

    # LOG FILE
    log_file = 'account_manager/scripts/log_sheet_useraccounttransaction_create.txt'
    log = open(log_file, 'a')

    rownumber = 1
    successnumber = 1
    for row in reader:
        print "ROW....................{}".format(rownumber)

        data = OrderedDict([
            ('bussiness_type', row['bussiness_type'].strip()),
            ('name', row['name'].strip()),
            ('email', row['email'].strip()),
            ('mobile', row['mobile'].strip()),
            ('doc_folder', row['doc_folder'].strip()),
            ('doc_subfolder', row['doc_subfolder'].strip()),
            ('doc_filename', row['doc_filename'].strip()),
            ('doc_newfilename', ''),
            ('doc_search_status', row['doc_search_status'].strip()),
            ('policy_search_status', row['policy_search_status'].strip()),
            ('email_sent', 'NOT SENT'),
            ('transaction_id', row['transaction_id'].strip()),
            ('premium_paid', row['premium_paid'].strip()),
            ('file_name', row['file_name'].strip()),
            ('file_number', row['file_number'].strip()),
            ('policy_punch_date', row['policy_punch_date'].strip()),
            ('policy_number', row['policy_number'].strip()),
            ('policy_start_date', row['policy_start_date'].strip()),
            ('policy_end_date', row['policy_end_date'].strip()),
            ('premium_amount', row['premium_amount'].strip()),
            ('policy_drag_status', row['policy_drag_status'].strip()),
            ('issuance_status', row['issuance_status'].strip()),
            ('insurance_company', row['insurance_company'].strip()),
            ('product_name', row['product_name'].strip()),
        ])

        # LOG #
        log.write('>>>>>>>>>>>>>>>>>>> ROWSTART: ' + str(rownumber) + '\n')
        log.write('>>>>>>>>>>>>>>>>>>> ORIGINALROWDATA\n')
        log.write(str(data) + '\n')
        # LOG #

        # 1: IF DOC AND POLICY FOUND
        if data['doc_search_status'] == 'FOUND' and data['policy_search_status'] == 'PERFECT MATCH':

            # 2: GET POLICY
            pol = GETMODEL[data['bussiness_type']].objects.get(
                transaction_id=data['transaction_id'])
            pol.policy_document = 'policy_document/{}.pdf'.format(
                pol.transaction_id)
            pol.save()

            # 3: GET RM
            rm = RelationshipManager.objects.get(
                type=data['bussiness_type'].lower())

            # 4: IF EMAIL
            pol_email = data['email'] or getattr(pol.proposer, 'email', '') if hasattr(
                pol, 'proposer') else pol.proposer_email
            if pol_email:
                print "SUCCESSNUMBER....................{}".format(successnumber)

                # 5: CREATE USER
                user, is_created = create_user(pol_email, pol)

                # LOG #
                log.write(str(pol_email) + '\n')
                if is_created:
                    log.write('UserCreated\n')
                else:
                    log.write('UserAlreadyExists\n')
                # LOG #

                # 6: CREATE USER PROFILE
                create_useraccount(user, pol)

                # 7: CREATE USER-RM RELATIONSHIP
                create_userrm(user, rm)

                # 8: CREATE USER-TRANSACTION
                create_usertransaction(user, pol)

                successnumber += 1

        rownumber += 1

        # LOG #
        log.write('>>>>>>>>>>>>>>>>>>> ROWEND\n\n')
        # LOG #


def create_user(email, pol):
    is_created = False
    try:
        user = User.objects.get_user(email=email, phone='')
    except User.DoesNotExist:
        user = User(
            email=email,
            username=email,
            first_name=getattr(pol.proposer, 'first_name', '') if hasattr(
                pol, 'proposer') else pol.proposer_first_name,
            middle_name=getattr(pol.proposer, 'middle_name', '') if hasattr(
                pol, 'proposer') else pol.proposer_middle_name,
            last_name=getattr(pol.proposer, 'last_name', '') if hasattr(
                pol, 'proposer') else pol.proposer_last_name,
            is_active=1,
            date_joined=datetime.datetime.now(),
            created_on=datetime.datetime.now(),
            updated_on=datetime.datetime.now(),
        )
        user.set_unusable_password()
        user.save()
        is_created = True

    return user, is_created


def create_useraccount(user, pol):
    try:
        UserAccount.objects.get(user=user)
    except UserAccount.DoesNotExist:
        address = OrderedDict([
            ('line_1', getattr(getattr(pol.proposer, 'address', None), 'line_1',
                               '') if hasattr(pol, 'proposer') else pol.proposer_address1),
            ('line_2', getattr(getattr(pol.proposer, 'address', None), 'line_2',
                               '') if hasattr(pol, 'proposer') else pol.proposer_address2),
            ('landmark', getattr(getattr(pol.proposer, 'address', None), 'landmark',
                                 '') if hasattr(pol, 'proposer') else pol.proposer_address_landmark),
        ])
        useraccount = UserAccount(
            user=user,
            address=', '.join(['%s' % (v)
                               for k, v in address.iteritems() if v]),
            address_line1=address['line_1'],
            address_line2=address['line_2'],
            address_landmark=address['landmark'],
            mobile=getattr(getattr(pol.proposer, 'address', None), 'mobile', '') if hasattr(
                pol, 'proposer') else pol.proposer_mobile,
            city=getattr(getattr(pol.proposer, 'address', None), 'city', '') if hasattr(
                pol, 'proposer') else pol.proposer_city,
            state=getattr(getattr(pol.proposer, 'address', None), 'state', '') if hasattr(
                pol, 'proposer') else pol.proposer_state,
            pincode=getattr(getattr(pol.proposer, 'address', None), 'pincode', '') if hasattr(
                pol, 'proposer') else pol.proposer_pincode,
        )
        useraccount.save()


def create_userrm(user, rm):
    try:
        UserRelationshipManager.objects.get(user=user)
    except UserRelationshipManager.DoesNotExist:
        UserRelationshipManager.objects.create(user=user, rm=rm)


def create_usertransaction(user, pol):
    try:
        UserTransaction.objects.get(
            user=user, object_id=pol.id, content_type=ContentType.objects.get_for_model(pol))
    except UserTransaction.DoesNotExist:
        user.transactions.create(transaction=pol)


def get_user_token():
    maindata = 'account_manager/scripts/final_new_policy_data.csv'
    reader = csv.DictReader(open(maindata))

    ################### EMAIL TOKEN #######################
    email_token = 'account_manager/scripts/email_token.csv'      # Write to
    e_t = open(email_token, 'a')
    ################### EMAIL TOKEN #######################

    rownumber = 1
    successnumber = 1
    for row in reader:
        print "ROW....................{}".format(rownumber)

        data = OrderedDict([
            ('bussiness_type', row['bussiness_type'].strip()),
            ('name', row['name'].strip()),
            ('email', row['email'].strip()),
            ('mobile', row['mobile'].strip()),
            ('doc_folder', row['doc_folder'].strip()),
            ('doc_subfolder', row['doc_subfolder'].strip()),
            ('doc_filename', row['doc_filename'].strip()),
            ('doc_newfilename', ''),
            ('doc_search_status', row['doc_search_status'].strip()),
            ('policy_search_status', row['policy_search_status'].strip()),
            ('email_sent', 'NOT SENT'),
            ('transaction_id', row['transaction_id'].strip()),
            ('premium_paid', row['premium_paid'].strip()),
            ('file_name', row['file_name'].strip()),
            ('file_number', row['file_number'].strip()),
            ('policy_punch_date', row['policy_punch_date'].strip()),
            ('policy_number', row['policy_number'].strip()),
            ('policy_start_date', row['policy_start_date'].strip()),
            ('policy_end_date', row['policy_end_date'].strip()),
            ('premium_amount', row['premium_amount'].strip()),
            ('policy_drag_status', row['policy_drag_status'].strip()),
            ('issuance_status', row['issuance_status'].strip()),
            ('insurance_company', row['insurance_company'].strip()),
            ('product_name', row['product_name'].strip()),
        ])

        # 1: IF DOC AND POLICY FOUND
        if data['doc_search_status'] == 'FOUND' and data['policy_search_status'] == 'PERFECT MATCH':

            # 2: GET POLICY
            pol = GETMODEL[data['bussiness_type']].objects.get(
                transaction_id=data['transaction_id'])
            pol.policy_document = 'policy_document/{}.pdf'.format(
                pol.transaction_id)
            pol.save()

            # 4: IF EMAIL
            pol_email = data['email'] or getattr(pol.proposer, 'email', '') if hasattr(
                pol, 'proposer') else pol.proposer_email
            if pol_email:
                print "SUCCESSNUMBER....................{}".format(successnumber)
                u = User.objects.get_user(email=pol_email, phone='')

                # ################## EMAIL TOKEN #######################
                et_data = OrderedDict([
                    ('email', pol_email),
                    ('token', u.token)
                ])
                et_w = csv.DictWriter(e_t, et_data.keys())
                et_w.writerow(et_data)
                # ################## EMAIL TOKEN #######################

                successnumber += 1

        rownumber += 1


def send_emails():
    email_token = 'account_manager/scripts/email_token.csv'
    reader = csv.DictReader(open(email_token))
    emaillist = []

    for r in reader:
        emaillist.append(r['email'].strip())

    uniqueemaillist = list(OrderedDict.fromkeys(emaillist))

    counter = 1
    for e in uniqueemaillist:
        print "Sending.......................{} - {}".format(counter, e)
        u = User.objects.get_user(email=e, phone='')
        send_account_creation_mail(u)
        counter += 1
        time.sleep(7)

    print "COMPLETED !!!"


def send_account_creation_mail(user):
    obj = {
        'user_id': user.id,
        'to_email': user.email,
        'name': user.get_full_name(),
        'user': user,
        'rm': user.urm.rm,
        'reset_url': settings.SITE_URL + reverse('reset_password', kwargs={'token': user.token}),
    }
    cf_cns.notify('USER_ACCOUNT_CREATION', to=(user.get_full_name(), user.email),
                  sender_name=user.urm.rm.name, sender_email=user.urm.rm.email, obj=obj)


def fix_user_token():
    email_token = 'account_manager/scripts/email_token.csv'        # Read from
    reader = csv.DictReader(open(email_token))

    log_file = 'account_manager/scripts/log_emailtoken.txt'
    if os.path.exists(log_file):
        os.remove(log_file)
    l_f = open(log_file, 'a')

    rownumber = 1
    for row in reader:
        print "ROW....................{}".format(rownumber)

        data = OrderedDict([
            ('email', row['email'].strip()),
            ('token', row['token'].strip()),
        ])

        # LOG #
        l_f.write('>>>>>>>>>>>>>>>>>>> ROWSTART\n')
        l_f.write('>>>>>>>>>>>>>>>>>>> ORIGINALROWDATA\n')
        l_f.write(str(data) + '\n')
        # LOG #

        if data['email']:
            try:
                u = User.objects.get_user(email=data['email'], phone='')
                u.token = data['token']
                u.save()

                # LOG #
                l_f.write('>>>>>>>>>>>>>>>>>>> USERTOKENUPDATED\n')
                # LOG #

                print "................................EMAIL - {}, TOKEN - {}".format(u.email, u.token)
            except User.DoesNotExist:

                # LOG #
                l_f.write('>>>>>>>>>>>>>>>>>>> USERDOESNOTEXIST\n')
                # LOG #
                pass

        rownumber += 1
        l_f.write('>>>>>>>>>>>>>>>>>>> ROWEND\n\n')


def address_populate_script():
    ut = UserTransaction.objects.all()
    count = 1
    for t in ut:
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> {} # START".format(count)
        ua = UserAccount.objects.get(user=t.user)
        print ">>>>>>>>>>>> BEFORE"
        print ua.__dict__
        if isinstance(t.transaction, HealthTransaction):
            ua.address_line1 = t.transaction.proposer_address1
            ua.address_line2 = t.transaction.proposer_address2
            ua.address_landmark = t.transaction.proposer_address_landmark
            ua.pincode = t.transaction.proposer_pincode
        elif isinstance(t.transaction, MotorTransaction):
            ua.address_line1 = t.transaction.proposer.address.line_1 if getattr(
                getattr(t.transaction, 'proposer', None), 'address', None) else ''
            ua.address_line2 = t.transaction.proposer.address.line_2 if getattr(
                getattr(t.transaction, 'proposer', None), 'address', None) else ''
            ua.address_landmark = t.transaction.proposer.address.landmark if getattr(
                getattr(t.transaction, 'proposer', None), 'address', None) else ''
            ua.pincode = t.transaction.proposer.address.pincode if getattr(
                getattr(t.transaction, 'proposer', None), 'address', None) else ''

        # update city and state
        if (not ua.city or not ua.state or ua.city.isnumeric() or ua.state.isnumeric()) and ua.pincode:
            pin = Pincode.objects.filter(pincode=ua.pincode)
            ua.city = pin[0].city if pin else ''
            ua.state = pin[0].state.name if pin else ''
        ua.save()
        print ">>>>>>>>>>>> AFTER"
        print ua.__dict__
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> {} # END\n".format(count)
        count += 1



def fix_user_rm():
    # Log file
    log_file = 'account_manager/scripts/log_fix_user_rm.txt'
    if os.path.exists(log_file):
        os.remove(log_file)
    lf = open(log_file, 'a')

    # Data file
    csv_file_path = 'account_manager/scripts/fix-user-rm.csv'
    reader = csv.DictReader(open(csv_file_path))

    counter = 1
    for row in reader:
        print "ROW..............................{}".format(counter)
        data = {
            'user_email': row['email'].strip(),
            'rm_name': row['rm_name'].strip(),
        }

        # LOG #
        lf.write('>>>>>>>>>>>>>>>>>>> ROWSTART '+ str(counter) + '\n')
        lf.write('ORIGINALDATA:' + str(data) + '\n')
        # LOG #

        try:
            u = User.objects.get_user(email=data['user_email'], phone='')
        except User.DoesNotExist:
            lf.write('User: NOT FOUND \n')
        else:
            lf.write('User: FOUND \n')
            try:
                urm = UserRelationshipManager.objects.get(user=u)
            except UserRelationshipManager.DoesNotExist:
                lf.write('UserRelationshipManager: NOT FOUND \n')
            else:
                lf.write('UserRelationshipManager: FOUND \n')
                try:
                    new_rm = RelationshipManager.objects.get(name=data['rm_name'])
                except RelationshipManager.DoesNotExist:
                    lf.write('New RM assigned: NO \n')
                else:
                    lf.write('OLDRM: ' + str(urm.rm.name) + '\n')
                    urm.rm = new_rm
                    urm.save()
                    lf.write('New RM assigned: YES \n')

        # LOG #
        lf.write('>>>>>>>>>>>>>>>>>>> ROWEND '+ str(counter) + '\n\n')
        # LOG #

        counter += 1


if __name__ == '__main__':
    fix_user_rm()