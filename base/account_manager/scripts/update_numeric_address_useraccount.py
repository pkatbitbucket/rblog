__author__ = "Parag Tyagi"

# setup environment
import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
django.setup()

import sys

import utils
import account_manager.models as account_manager_models
import account_manager.utils as account_manager_utils

logger = utils.create_logger('account_manager/update_numeric_address_useraccount')


def update_numeric_address_useraccount(commit=False):
    '''
    Updating city and state values of UserAccount models if values are numeric
    using Pincode model
    '''

    logger.info("*********************** STARTING SCRIPT ***********************")
    logger.info("============= COMMITTING CHANGES: {}".format(commit))

    counter = 1
    useraccounts = account_manager_models.UserAccount.objects.all()
    for ua in useraccounts:
        old_city = ua.city
        old_state = ua.state
        pincode = ua.pincode

        new_city, new_state = account_manager_utils.fix_numeric_city_state(old_city, old_state, pincode)
        ua.city = new_city or ''
        ua.state = new_state or ''

        is_changed = False if ((old_city, old_state) == (ua.city, ua.state)) else True
        if commit and is_changed:
            ua.save()

        logger.info('{}) {}: ({}, {}) => ({}, {}) [CHANGED: {}]'.format(counter, ua.user.email, old_city,
                                                                        old_state, ua.city, ua.state, is_changed))
        counter += 1


if __name__ == '__main__':
    commit = False
    argv = sys.argv
    if len(argv) > 1:
        if argv[1] != 'commit':
            print "Please use `commit` flag for committing changes. Kindly re-try."
            print "Example: ipython scripts/script.py commit"
            sys.exit()
        else:
            commit = True
    update_numeric_address_useraccount(commit)
