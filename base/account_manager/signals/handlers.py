from __future__ import absolute_import
import logging

from django.contrib.auth.models import AnonymousUser
from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import post_save
from django.dispatch import receiver

from core.models import User
from utils import get_property, log_error, LMSUtils
from .signals import online_transaction_completed, create_user_account, policy_updated, activate_user_account
from ..models import UserAccount, UserTransaction, UserRelationshipManager
from ..utils import (
    create_useraccount_add_transaction, send_account_creation_mail,
    transaction_field_mapping, send_policy_upload_notification
)

logger = logging.getLogger(__name__)


@receiver(online_transaction_completed)
def handle_clear_transaction(sender, **kwargs):
    """
    On Successfull transaction:-
    1) Create User, UserAccount, UserTransaction, UserRM
    2) Send UserAccountCreation / Policy Upload mail
    """
    try:
        transaction = kwargs['transaction']
        user = kwargs['user']
        transaction_field_map = transaction_field_mapping(transaction)
        email = user.email if user.is_authenticated() else transaction_field_map.get('email', '')
        mobile = user.mobile if user.is_authenticated() else transaction_field_map.get('mobile', '')

        # create everything
        if (email or mobile) and get_property(transaction, 'policy_document'):
            user, is_useraccount_created = create_useraccount_add_transaction(email, mobile, transaction)

            if is_useraccount_created:
                send_account_creation_mail(user, transaction)
            else:
                send_policy_upload_notification(user, transaction)
    except:
        log_error(logger,
                  msg="Internal Server Error: Unknown Error Received in clear transaction signal - Account Manager")


@receiver(create_user_account)
def handle_account_creation(sender, **kwargs):
    """
    On Successfull transaction:-
    1) Create User, UserAccount, UserTransaction, UserRM
    2) Send UserAccountCreation / Policy Upload mail
    """

    t = kwargs['transaction']
    t.refresh_from_db()
    user = kwargs.get('user', AnonymousUser())
    transaction_field_map = transaction_field_mapping(t)
    email = user.email if user.is_authenticated() else transaction_field_map.get('email', '')
    mobile = user.mobile if user.is_authenticated() else transaction_field_map.get('mobile', '')

    if email or mobile:
        try:
            create_useraccount_add_transaction(email, mobile, t, active=False)
        except:
            log_error(logger,
                      msg="Internal Server Error: Unknown Error Received in clear transaction signal - Account Manager")


@receiver(activate_user_account)
def handle_user_activation(sender, **kwargs):
    transaction = kwargs['transaction']
    user_account = UserAccount.objects.get(
        user__transactions__object_id=transaction.id,
        user__transactions__content_type=ContentType.objects.get_for_model(transaction)
    )
    user_account.activate_user(send_mail=True)


@receiver(policy_updated)
def handle_policy_update(sender, **kwargs):
    transaction = kwargs['transaction']
    try:
        user_transaction = UserTransaction.objects.get(object_id=transaction.id,
                                                       content_type=ContentType.objects.get_for_model(transaction))
    except UserTransaction.DoesNotExist:
        return

    if user_transaction.transaction.status in ['COMPLETED', 'MANUAL COMPLETED']:
        user_transaction.close_ticket.apply_async(countdown=5 * 60)

    if kwargs.get('send_mail', False):
        user = User.objects.filter(
            transactions__object_id=transaction.id,
            transactions__content_type=ContentType.objects.get_for_model(transaction)
        ).distinct().get()
        send_policy_upload_notification(user, kwargs['transaction'])
