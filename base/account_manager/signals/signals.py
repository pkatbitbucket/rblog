from django.dispatch import Signal

online_transaction_completed = Signal(providing_args=["transaction, user"])

create_user_account = Signal(providing_args=["transaction, user"])

activate_user_account = Signal(providing_args=["transaction"])

policy_updated = Signal(providing_args=["transaction", "send_mail"])
