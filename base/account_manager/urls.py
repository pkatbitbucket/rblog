from django.conf.urls import url, include

from . import views as account_manager_views
from . import cfsupport_views


cfsupport_urlpatterns = [
    url(r'^$', cfsupport_views.dashboard, name='cfsupport_dashboard'),
    url(r'^dashboard/$', cfsupport_views.dashboard, name='cfsupport_dashboard'),
    url(r'^search-transaction/$', cfsupport_views.search_transaction, name='search_transaction'),
    url(r'^edit-transaction/$', cfsupport_views.edit_transaction, name='edit_transaction'),
    url(r'^transactions/(?P<transaction_type>[a-zA-Z]+)/$', cfsupport_views.transactions, name='transactions'),
    url(r'^get-health-plans/', cfsupport_views.get_health_plans, name='transactions'),
    url(r'^offline-transaction/(?P<product_type>\w+)/$', cfsupport_views.add_offline_transaction, name='offline_transaction'),
    url(r'^user-accounts/$', cfsupport_views.user_accounts, name='user_accounts'),
    url(r'^transactions-vs-useraccounts/$', cfsupport_views.transactions_vs_useraccounts, name='transactions_vs_useraccounts'),
    url(r'^user/(?P<user_id>[0-9]+)$', cfsupport_views.user, name='user'),
    url(r'^login-as/$', cfsupport_views.login_as, name='login_as'),
    url(r'^staff-logout/$', cfsupport_views.staff_logout, name='staff_logout'),
    url(r'^endorsements/$', cfsupport_views.EndorsementListView.as_view(), name="endorsements"),
    url(r'^endorsements/add/$', cfsupport_views.endorsement_add, name="endorsement_add"),
    url(r'^endorsements/view/(?P<end_id>\d+)/$', cfsupport_views.endorsement_detail, name="endorsement_detail"),
    url(r'^endorsements/edit/(?P<end_id>\d+)/$', cfsupport_views.endorsement_edit, name="endorsement_edit"),
]

urlpatterns = [
    url(r'^profile/$', account_manager_views.profile, name='profile'),
    url(r'^policy/$', account_manager_views.policy, name='policy'),
    url(r'^expired/$', account_manager_views.expired, name='expired'),
    url(r'^submit-claim/$', account_manager_views.submit_claim, name='submit_claim'),
    url(r'^download-document/(?P<transaction_id>[\w-]+)/(?P<type>[a-zA-Z]+)/(?P<document_type>[a-zA-Z_]+)/$',
        account_manager_views.download_document, name='download_document'),
    url(r'^cfsupport/', include(cfsupport_urlpatterns)),
    url(r'^manage-policies/', account_manager_views.manage_policies, name='manage_policies'),
]
