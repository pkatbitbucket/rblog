from __future__ import absolute_import
import os
import uuid
import datetime
from collections import OrderedDict
from itertools import chain
import logging
from django.core.cache import cache
from django.core.files.storage import default_storage
from django.core.mail.backends import locmem
import urlparse

from django.apps import apps
from django.core.mail import EmailMessage
from django.db.models import Q
from django.contrib.contenttypes.models import ContentType
from django.core.mail import get_connection
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.db import transaction

from account_manager.signals.signals import activate_user_account
import cf_cns

import putils
from celery_app import app

from health_product.models import Transaction as HealthTransaction
from motor_product.models import Transaction as MotorTransaction
from travel_product.models import Transaction as TravelTransaction

from core.models import User, Email, Phone, Tracker
from account_manager.models import RelationshipManager, UserRelationshipManager, UserTransaction, UserAccount
from utils import get_property, UnicodeWriter, convert_to_unicode, convert_datetime_to_timezone, log_error, clean_name

extlogger = logging.getLogger('extensive')


logger = logging.getLogger(__name__)

# TRANSACTION MODEL MAPPING
TRANSACTION_MODEL_MAPPING = {
    'health': apps.get_model('health_product', 'Transaction'),
    'car': apps.get_model('motor_product', 'Transaction'),
    'bike': apps.get_model('motor_product', 'Transaction'),
    'travel': apps.get_model('travel_product', 'Transaction'),
}


def created_range_query(from_date, to_date, transaction_type):
    """
    Get created range query as per product/transaction_type
    """
    query_dict = {
        'car': Q(created_on__range=(from_date, to_date)) & Q(vehicle_type='Private Car'),
        'bike': Q(created_on__range=(from_date, to_date)) & Q(vehicle_type='Twowheeler'),
        'motor': Q(created_on__range=(from_date, to_date)),
        'health': Q(created_on__range=(from_date, to_date)),
        'travel': Q(created_date__range=(from_date, to_date)),
    }

    if transaction_type.lower() in query_dict:
        return query_dict[transaction_type]
    else:
        return Q()


def get_health_transactions(q, filt=None, excl=None, fields=list()):
    """
    :param q: search paramter
    :param filt: filter query
    :param excl: exclude query
    :param fields: which fields to query (if empty, query on all fields in `param_field`)
    :return: Queryset
    """

    q = q.strip()
    name_filter = Q()
    for n in q.split():
        name_filter |= Q(proposer_first_name__iexact=n)
        name_filter |= Q(proposer_last_name__iexact=n)

    param_field = {
        'name': name_filter,
        'email': Q(proposer_email__icontains=q),
        'mobile': Q(proposer_mobile__icontains=q),
        'transaction_id': Q(transaction_id=q),
        'policy_number': Q(policy_number__iexact=q),
    }

    full_query = Q()
    for param, field in param_field.items():
        if not fields or param in fields:
            full_query |= field

    transaction = HealthTransaction.objects.filter(full_query).order_by('-created_on')
    filt = filt or Q()
    excl = excl or Q()
    transaction = transaction.filter(filt).exclude(excl)
    return transaction


def get_motor_transactions(q, filt=None, excl=None, fields=list()):
    """
    :param q: search paramter
    :param filt: filter query
    :param excl: exclude query
    :param fields: which fields to query (if empty, query on all fields in `param_field`)
    :return: Queryset
    """

    q = q.strip()
    name_filter = Q()
    for n in q.split():
        name_filter |= Q(proposer__first_name__iexact=n)
        name_filter |= Q(proposer__last_name__iexact=n)

    field_query = {
        'name': name_filter,
        'email': Q(proposer__email__icontains=q),
        'mobile': Q(proposer__mobile__icontains=q),
        'transaction_id': Q(transaction_id=q),
        'policy_number': Q(policy_number__iexact=q),
        'rsa_policy_number': Q(rsa_policy_number__iexact=q),
    }

    full_query = Q()
    for param, query in field_query.items():
        if not fields or param in fields:
            full_query |= query

    transaction = MotorTransaction.objects.filter(full_query).order_by('-created_on')
    filt = filt or Q()
    excl = excl or Q()
    transaction = transaction.filter(filt).exclude(excl)
    return transaction


def get_travel_transactions(q, filt=None, excl=None, fields=list()):
    """
    :param q: search paramter
    :param filt: filter query
    :param excl: exclude query
    :param fields: which fields to query (if empty, query on all fields in `param_field`)
    :return: Queryset
    """

    q = q.strip()
    name_filter = Q()
    for n in q.split():
        name_filter |= Q(first_name__iexact=n)
        name_filter |= Q(last_name__iexact=n)

    field_query = {
        'name': name_filter,
        'email': Q(email__icontains=q),
        'mobile': Q(mobile__icontains=q),
        'transaction_id': Q(transaction_id=q),
        'policy_number': Q(policy_number__iexact=q),
    }

    full_query = Q()
    for param, query in field_query.items():
        if not fields or param in fields:
            full_query |= query

    transaction = TravelTransaction.objects.filter(full_query).order_by('-created_date')
    filt = filt or Q()
    excl = excl or Q()
    transaction = transaction.filter(filt).exclude(excl)
    return transaction


def fix_numeric_city_state(city, state, pincode):
    """
    Return (city, state) name if input (city or state) is either (empty or numeric-id)
    using Pincode model
    """

    city_check = (not city or str(city).isdigit())
    state_check = (not state or str(state).isdigit())

    if (city_check or state_check) and pincode:
        new_city, new_state = putils.get_city_state_from_pincode(pincode)
        city = new_city if city_check else city
        state = new_state if state_check else state

    return city, state


def get_policy_details(policy):
    product = policy.requirement_sold.kind.product

    form_data = {}
    self_form_data = {}
    address_form = {}
    vehicle_detail_form = {}
    if policy.requirement_sold.fd:
        form_data = policy.requirement_sold.fd.get_data()
        person_forms = form_data['core.forms.Person']
        # Get self data.
        if isinstance(person_forms, dict):
            if self_form_data['role'] == 'self':
                self_form_data = self_form_data

        if "core_forms.Address" in form_data:
            address_forms = form_data['core_forms.Address']
            if address_forms:
                for form in address_form:
                    # TODO: Need to modify type to role.
                    if form['type'] == 'communication':
                        address_form = form

        vehicle_detail_form = {}
        if product == 'bike':
            if "core.forms.BikeDetail" in vehicle_detail_form:
                vehicle_detail_form = form_data['core.forms.BikeDetail']
        elif product == 'motor':
            if "core.forms.CarDetail" in vehicle_detail_form:
                vehicle_detail_form = form_data['core.forms.CarDetail']

    full_address = [
        address_form.get('address_line1'),
        address_form.get('address_line2'),
        address_form.get('landmark'),
        address_form.get('city'),
        address_form.get('state'),
        address_form.get('pincode')
    ]

    policy_tenure = None
    if policy.issue_date and policy.expiry_date:
        policy_tenure = abs((policy.issue_date - policy.expiry_date).days)

    # is policy expired
    is_policy_expired = None
    if policy.expiry_date:
        if policy.expiry_date > datetime.datetime.now().date():
            is_policy_expired = True
        else:
            is_policy_expired = False

    if product in ['motor', 'bike']:
        ret = OrderedDict([
                ('id', policy.requirement_sold.pk),
                ('transaction_id', policy.requirement_sold.id),
                ('type', product),
                ('vehicle_type', product),
                ('policy_type', get_property(policy.requirement_sold.fd, 'quote.policy_type')),
                ('first_name', self_form_data.get('first_name', '')),
                ('middle_name', self_form_data.get('middle_name', '')),
                ('last_name', self_form_data.get('last_name', '')),
                ('email', self_form_data.get('email', '')),
                ('mobile', self_form_data.get('mobile', '')),
                ('dob', self_form_data.get('birth_date', '')),
                ('gender', self_form_data.get('gender', '')),
                ('address_line1', address_form.get('address_line1', '')),
                ('address_line2', address_form.get('address_line2', '')),
                ('address_landmark', address_form.get('landmark', '')),
                ('city', address_form.get('city', '')),
                ('state', address_form.get('state', '')),
                ('pincode', address_form.get('pincode', '')),
                ('full_address', full_address),

                ('policy_number', get_property(policy, 'policy_number')),
                ('rsa_policy_number', get_property(policy, 'rsa_policy_number')),
                ('policy_status', get_property(policy, 'status')),
                ('payment_done', ''),  # TODO: Need to look.
                ('policy_start_date', get_property(policy, 'issue_date')),
                ('policy_end_date', get_property(policy, 'expiry_date')),
                ('policy_tenure', policy_tenure),
                ('is_policy_expired', is_policy_expired),
                ('policy_document', get_property(policy, 'document.url')),
                ('rsa_policy', ''),
                ('premium_paid', get_property(policy, 'premium')),
                ('transaction_mode', ''),
                ('insurer_title', ''),
                ('insurer_slug', ''),
                ('policy_name', ''),
                ('policy_covers', ''),
                ('vehicle_model', ''),
                ('vehicle_make', ''),
                ('vehicle_variant', ''),
                ('vehicle_fuel_type', ''),
                ('vehicle_rto', vehicle_detail_form.get('vehicle_rto', '')),
                ('vehicle_registration_number', vehicle_detail_form.get('registration_number', '')),
                ('vehicle_registration_date', ''),
                ('vehicle_manufacturing_date', ''),
                ('vehicle_engine_number', vehicle_detail_form.get('engine_number', '')),
                ('vehicle_chassis_number', vehicle_detail_form.get('chassis_number', '')),
                ('od_premium', get_property(policy, 'premium')),  # TODO: Need to look into this.
                ('ncb', ''),
                ('proposal_link', ''),

                ('past_policy_number', vehicle_detail_form.get('past_policy_number', '')),
                ('past_policy_insurer', vehicle_detail_form.get('past_policy_insurer', '')),
                ('past_policy_claims', ''),
                ('past_policy_claims_amount', ''),

                ('payment_on', ''),
                ('transaction_created', ''),
                ('transaction_updated', ''),
            ])

    elif product == 'health':
        return OrderedDict([
                ('id', policy.requirement_sold.pk),
                ('transaction_id', policy.requirement_sold.id),
                ('type', product),
                ('first_name', self_form_data.get('first_name', '')),
                ('middle_name', self_form_data.get('middle_name', '')),
                ('last_name', self_form_data.get('last_name', '')),
                ('email', self_form_data.get('email', '')),
                ('mobile', self_form_data.get('mobile', '')),
                ('address_line1', address_form.get('address_line1', '')),
                ('address_line2', address_form.get('address_line2', '')),
                ('address_landmark', address_form.get('landmark', '')),
                ('city', address_form.get('city', '')),
                ('state', address_form.get('state', '')),
                ('pincode', address_form.get('pincode', '')),

                ('policy_number', get_property(policy, 'policy_number')),
                ('policy_status', get_property(policy, 'status')),
                ('policy_start_date', get_property(policy, 'issue_date')),
                ('policy_end_date', get_property(policy, 'expiry_date')),
                ('policy_tenure', policy_tenure),
                ('is_policy_expired', is_policy_expired),
                ('policy_document', get_property(policy, 'document.url')),
                ('premium_paid', get_property(policy, 'premium')),
                ('transaction_mode', ''),
                ('insurer_title', ''),
                ('insurer_slug', ''),
                ('policy_name', ''),
                ('policy_type', ''),
                ('policy_covers', ''),
                ('proposal_link', ''),

                ('payment_on', ''),
                ('transaction_created', ''),
                ('transaction_updated', ''),
            ])
    elif product == 'travel':
        return OrderedDict([
                ('id', policy.requirement_sold.pk),
                ('transaction_id', policy.requirement_sold.id),
                ('type', product),
                ('first_name', self_form_data.get('first_name', '')),
                ('middle_name', self_form_data.get('middle_name', '')),
                ('last_name', self_form_data.get('last_name', '')),
                ('email', self_form_data.get('email', '')),
                ('mobile', self_form_data.get('mobile', '')),
                ('address_line1', address_form.get('address_line1', '')),
                ('address_line2', address_form.get('address_line2', '')),
                ('address_landmark', address_form.get('landmark', '')),
                ('city', address_form.get('city', '')),
                ('state', address_form.get('state', '')),
                ('pincode', address_form.get('pincode', '')),

                ('policy_number', get_property(policy, 'policy_number')),
                ('policy_status', get_property(policy, 'status')),
                ('policy_start_date', get_property(policy, 'issue_date')),
                ('policy_end_date', get_property(policy, 'expiry_date')),
                ('policy_tenure', policy_tenure),
                ('is_policy_expired', is_policy_expired),
                ('policy_document', get_property(policy, 'document.url')),
                ('premium_paid', get_property(policy, 'premium')),
                ('transaction_mode', ''),
                ('insurer_title', ''),
                ('insurer_slug', ''),
                ('policy_name', ''),
                ('policy_type', ''),
                ('policy_covers', ''),
                ('proposal_link', ''),

                ('payment_on', ''),
                ('transaction_created', ''),
                ('transaction_updated', ''),

            ])
    else:
        ret = {}

    return ret


def transaction_field_mapping(transaction):
    """
    Field mapping for different Product Transactions
    """

    def calculate_tat(minutes):
        value = minutes / 60.0
        return '{} minute(s)'.format(minutes) if value < 1 else '{} hour(s)'.format(int(round(value)))

    try:
        if isinstance(transaction, HealthTransaction):

            # dates
            payment_on = convert_datetime_to_timezone(transaction.payment_on)
            transaction_created = convert_datetime_to_timezone(transaction.created_on)
            transaction_updated = convert_datetime_to_timezone(transaction.updated_on)
            dob = transaction.proposer_date_of_birth

            # policy start and end date
            policy_start_date = convert_datetime_to_timezone(transaction.policy_start_date).date() \
                if transaction.policy_start_date else None
            policy_end_date = convert_datetime_to_timezone(transaction.policy_end_date).date() \
                if transaction.policy_end_date else None

            # policy tenure
            policy_tenure = None
            if policy_start_date and policy_end_date:
                policy_tenure = abs((policy_end_date - policy_start_date).days)

            # is policy expired
            is_policy_expired = (False if (policy_end_date > datetime.datetime.now().date()) else True) \
                if policy_end_date else None

            # policy name
            policy_title = get_property(transaction, 'slab.health_product.title')
            policy_type = get_property(transaction, 'slab.health_product.policy_type')
            policy_name = ' '.join(filter(None, [policy_title, policy_type]))

            # address
            city = get_property(transaction, 'proposer_city')
            state = get_property(transaction, 'proposer_state')
            pincode = get_property(transaction, 'proposer_pincode')
            city, state = fix_numeric_city_state(city, state, pincode)
            address_line1 = get_property(transaction, 'proposer_address1')
            address_line2 = get_property(transaction, 'proposer_address2')
            address_landmark = get_property(transaction, 'proposer_address_landmark')
            full_address = ', '.join(
                filter(None, [address_line1, address_line2, address_landmark, city, state, pincode]))

            # proposal link
            transaction_id = transaction.transaction_id
            proposal_link = "{}/{}/{}/{}".format(settings.SITE_URL, 'health-plan',
                                                 'view', transaction_id)

            # my-account info
            email = get_property(transaction, 'proposer_email')
            # FIXME: Temporary fix
            try:
                user, user_account, user_transaction = get_user_for_transaction(transaction, email)
            except:
                user, user_account, user_transaction = None, None, None
            is_my_account_created = True if user_account else False
            is_my_account_activated = (True if user.has_usable_password() else False) if user_account else False
            is_added_to_my_account = True if user_transaction else False
            user_details = get_user_details(user) if user else None

            return OrderedDict([
                ('id', transaction.id),
                ('transaction_id', transaction_id),
                ('type', transaction.transaction_type),
                ('first_name', get_property(transaction, 'proposer_first_name')),
                ('middle_name', get_property(transaction, 'proposer_middle_name')),
                ('last_name', get_property(transaction, 'proposer_last_name')),
                ('email', email),
                ('mobile', get_property(transaction, 'proposer_mobile')),
                ('dob', dob),
                ('gender', get_property(transaction, 'proposer_gender')),
                ('address_line1', address_line1),
                ('address_line2', address_line2),
                ('address_landmark', address_landmark),
                ('city', city),
                ('state', state),
                ('pincode', pincode),
                ('full_address', full_address),

                ('policy_number', get_property(transaction, 'policy_number')),
                ('policy_status', get_property(transaction, 'status')),
                ('policy_start_date', policy_start_date),
                ('policy_end_date', policy_end_date),
                ('policy_tenure', policy_tenure),
                ('is_policy_expired', is_policy_expired),
                ('policy_document', get_property(transaction, 'policy_document.url')),
                ('premium_paid', get_property(transaction, 'premium_paid')),
                ('transaction_mode', transaction.transaction_mode),
                ('insurer_title', get_property(transaction, 'slab.health_product.insurer.title')),
                ('insurer_slug', get_property(transaction, 'slab.health_product.insurer.slug')),
                ('policy_name', policy_name),
                ('policy_type', policy_type),
                ('policy_covers', ', '.join([m['person'] for m in get_property(transaction, 'extra.members') or ''])),
                ('proposal_link', proposal_link),
                ('policy_tat', calculate_tat(transaction.policy_tat)),

                ('payment_on', payment_on or None),
                ('transaction_created', transaction_created or None),
                ('transaction_updated', transaction_updated or None),

                ('is_my_account_created', is_my_account_created),
                ('is_my_account_activated', is_my_account_activated),
                ('is_added_to_my_account', is_added_to_my_account),
                ('my_account_created_on', get_property(user_details, 'useraccount_created_on')),
                ('customer_full_name', get_property(user_details, 'full_name')),
                ('customer_email', get_property(user_details, 'email')),
                ('customer_mobile', get_property(user_details, 'mobile')),
                ('rm_name', get_property(user_details, 'rm_name')),
            ])
        elif isinstance(transaction, MotorTransaction):

            # dates
            payment_on = convert_datetime_to_timezone(transaction.payment_on)
            transaction_created = convert_datetime_to_timezone(transaction.created_on)
            transaction_updated = convert_datetime_to_timezone(transaction.updated_on)
            dob = get_property(transaction, 'proposer.date_of_birth')

            # policy start and end date
            policy_start_date = transaction.policy_start_date or get_property(transaction,
                                                                              'quote.new_policy_start_date')
            policy_end_date = transaction.policy_end_date
            if not policy_end_date and policy_start_date:
                policy_end_date = putils.add_years(policy_start_date, 1) - datetime.timedelta(days=1)
            policy_start_date = convert_datetime_to_timezone(policy_start_date).date() if policy_start_date else None
            policy_end_date = convert_datetime_to_timezone(policy_end_date).date() if policy_end_date else None

            # policy tenure
            policy_tenure = None
            if policy_start_date and policy_end_date:
                policy_tenure = abs((policy_end_date - policy_start_date).days)

            # is policy expired
            is_policy_expired = (False if (policy_end_date > datetime.datetime.now().date()) else True) \
                if policy_end_date else None

            # proposer data
            first_name = get_property(transaction, 'proposer.first_name') or \
                         get_property(transaction, 'raw.user_form_details.cust_first_name')
            last_name = get_property(transaction, 'proposer.last_name') or \
                        get_property(transaction, 'raw.user_form_details.cust_last_name')
            email = get_property(transaction, 'proposer.email') or \
                    get_property(transaction, 'raw.user_form_details.cust_email')
            mobile = get_property(transaction, 'proposer.mobile') or \
                     get_property(transaction, 'raw.user_form_details.cust_phone')
            gender = get_property(transaction, 'proposer.gender') or \
                     get_property(transaction, 'raw.user_form_details.cust_gender')

            # address
            house_no = get_property(transaction, 'raw.user_form_details.add_house_no')
            building_name = get_property(transaction, 'raw.user_form_details.add_building_name')
            street_name = get_property(transaction, 'raw.user_form_details.add_street_name')
            extra_line = get_property(transaction, 'raw.user_form_details.add_extra_line')
            landmark = get_property(transaction, 'raw.user_form_details.add_landmark')

            line_1 = ', '.join(filter(None, [house_no, building_name]))
            line_2 = ', '.join(filter(None, [street_name, extra_line]))

            address_line1 = get_property(transaction, 'proposer.address.line_1') \
                            or line_1
            address_line2 = get_property(transaction, 'proposer.address.line_2') \
                            or line_2
            address_landmark = get_property(transaction, 'proposer.address.landmark') \
                               or landmark

            city = get_property(transaction, 'proposer.address.city') \
                   or get_property(transaction, 'raw.user_form_details.loc_add_city')
            state = get_property(transaction, 'proposer.address.state') \
                    or get_property(transaction, 'raw.user_form_details.loc_add_state')
            pincode = get_property(transaction, 'proposer.address.pincode') \
                      or get_property(transaction, 'raw.user_form_details.add_pincode')
            city, state = fix_numeric_city_state(city, state, pincode)
            full_address = ', '.join(filter(None, [address_line1, address_line2, address_landmark, city, state, pincode]))

            VEHICLE_TYPE_TO_URL_TAG_MAP = {
                u'Private Car': 'fourwheeler',
                u'Twowheeler': 'twowheeler',
            }

            insurer_slug = get_property(transaction, 'insurer.slug')
            transaction_id = transaction.transaction_id
            vehicle_type = transaction.vehicle_type
            proposal_link = "{}/{}/{}/{}/{}/{}".format(settings.SITE_URL, 'motor',
                                                       VEHICLE_TYPE_TO_URL_TAG_MAP[vehicle_type], insurer_slug,
                                                       'desktopForm', transaction_id)

            # my-account info
            # FIXME: Temporary fix
            try:
                user, user_account, user_transaction = get_user_for_transaction(transaction, email)
            except:
                user, user_account, user_transaction = None, None, None
            is_my_account_created = True if user_account else False
            is_my_account_activated = (True if user.has_usable_password() else False) if user_account else False
            is_added_to_my_account = True if user_transaction else False
            user_details = get_user_details(user) if is_my_account_created else None

            return OrderedDict([
                ('id', transaction.id),
                ('transaction_id', transaction_id),
                ('type', transaction.transaction_type),
                ('vehicle_type', vehicle_type),
                ('policy_type', get_property(transaction, 'quote.policy_type')),
                ('first_name', first_name),
                ('middle_name', get_property(transaction, 'proposer.middle_name')),
                ('last_name', last_name),
                ('email', email),
                ('mobile', mobile),
                ('dob', dob or None),
                ('gender', gender),
                ('address_line1', address_line1),
                ('address_line2', address_line2),
                ('address_landmark', address_landmark),
                ('city', city),
                ('state', state),
                ('pincode', pincode),
                ('full_address', full_address),

                ('policy_number', get_property(transaction, 'policy_number')),
                ('rsa_policy_number', get_property(transaction, 'rsa_policy_number')),
                ('policy_status', get_property(transaction, 'status')),
                ('payment_mode', get_property(transaction, 'quote.raw_data.payment_mode')),
                ('payment_done', transaction.payment_done),
                ('policy_start_date', policy_start_date),
                ('policy_end_date', policy_end_date),
                ('policy_tenure', policy_tenure),
                ('is_policy_expired', is_policy_expired),
                ('policy_document', get_property(transaction, 'policy_document.url')),
                ('rsa_policy', get_property(transaction, 'rsa_policy.url')),
                ('premium_paid', get_property(transaction, 'premium_paid')),
                ('transaction_mode', get_property(transaction, 'transaction_mode')),
                ('insurer_title', get_property(transaction, 'insurer.title')),
                ('insurer_slug', insurer_slug),
                ('policy_name', get_property(transaction, 'insurer.title')),
                ('policy_covers', ' '.join([get_property(transaction, 'quote.vehicle.make.name') or '',
                                            get_property(transaction, 'quote.vehicle.model.name') or '']).strip()),
                ('vehicle_model', get_property(transaction, 'quote.vehicle.model.name')),
                ('vehicle_make', get_property(transaction, 'quote.vehicle.make.name')),
                ('vehicle_variant', get_property(transaction, 'quote.vehicle.variant')),
                ('vehicle_fuel_type', get_property(transaction, 'quote.vehicle.fuel_type')),
                ('vehicle_rto', get_property(transaction, 'raw.user_form_details.vehicle_rto') or
                 get_property(transaction, 'quote.rto')),
                ('vehicle_registration_number', get_property(transaction, 'raw.user_form_details.vehicle_reg_no')),
                ('vehicle_registration_date', get_property(transaction, 'quote.raw_data.registrationDate') or
                 get_property(transaction, 'raw.user_form_details.vehicle_reg_date')),
                ('vehicle_manufacturing_date', get_property(transaction, 'quote.raw_data.manufacturingDate')),
                ('vehicle_engine_number', get_property(transaction, 'raw.user_form_details.vehicle_engine_no')),
                ('vehicle_chassis_number', get_property(transaction, 'raw.user_form_details.vehicle_chassis_no')),
                ('od_premium', get_property(transaction, 'od_premium')),
                ('ncb', get_property(transaction, 'ncb')),
                ('proposal_link', proposal_link),

                ('past_policy_expiry_date', get_property(transaction, 'quote.raw_data.pastPolicyExpiryDate')),
                ('past_policy_number', get_property(transaction, 'raw.user_form_details.past_policy_number')),
                ('past_policy_insurer', get_property(transaction, 'raw.user_form_details.past_policy_insurer')),
                ('past_policy_claims', get_property(transaction, 'raw.user_form_details.past_policy_claims')),
                ('past_policy_claims_amount',
                 get_property(transaction, 'raw.user_form_details.past_policy_claims_amount')),
                ('policy_tat', calculate_tat(transaction.policy_tat)),

                ('payment_on', payment_on or None),
                ('transaction_created', transaction_created or None),
                ('transaction_updated', transaction_updated or None),

                ('is_my_account_created', is_my_account_created),
                ('is_my_account_activated', is_my_account_activated),
                ('is_added_to_my_account', is_added_to_my_account),
                ('my_account_created_on', get_property(user_details, 'useraccount_created_on')),
                ('customer_full_name', get_property(user_details, 'full_name')),
                ('customer_email', get_property(user_details, 'email')),
                ('customer_mobile', get_property(user_details, 'mobile')),
                ('rm_name', get_property(user_details, 'rm_name')),
            ])

        elif isinstance(transaction, TravelTransaction):

            # dates
            payment_on = convert_datetime_to_timezone(transaction.payment_on)
            transaction_created = convert_datetime_to_timezone(transaction.created_date)
            transaction_updated = convert_datetime_to_timezone(transaction.modified_date)

            # policy start and end date
            policy_start_date = get_property(transaction, 'extra.search_data.from_date')
            policy_start_date = datetime.datetime.strptime(policy_start_date, '%d-%b-%Y').date() \
                if policy_start_date else None
            policy_end_date = get_property(transaction, 'extra.search_data.to_date')
            policy_end_date = datetime.datetime.strptime(policy_end_date, '%d-%b-%Y').date() \
                if policy_end_date else None

            # policy tenure
            policy_tenure = None
            if policy_start_date and policy_end_date:
                policy_tenure = abs((policy_end_date - policy_start_date).days)

            # is policy expired
            is_policy_expired = (False if (policy_end_date > datetime.datetime.now().date()) else True) \
                if policy_end_date else None

            # policy name
            insurer_title = get_property(transaction, 'slab.plan.insurer.name')
            policy_type = get_property(transaction, 'data.plan_details.plan_type')
            policy_name = ' '.join(filter(None, [insurer_title, policy_type]))

            # proposal link
            transaction_id = transaction.transaction_id
            proposal_link = "{}/{}/{}/{}".format(settings.SITE_URL, 'travel-insurance',
                                                 'buy', transaction_id)

            # my-account info
            email = get_property(transaction, 'email')
            # FIXME: Temporary fix
            try:
                user, user_account, user_transaction = get_user_for_transaction(transaction, email)
            except:
                user, user_account, user_transaction = None, None, None
            is_my_account_created = True if user_account else False
            is_my_account_activated = (True if user.has_usable_password() else False) if user_account else False
            is_added_to_my_account = True if user_transaction else False
            user_details = get_user_details(user) if user else None

            return OrderedDict([
                ('id', transaction.id),
                ('transaction_id', transaction_id),
                ('type', transaction.transaction_type),
                ('first_name', get_property(transaction, 'first_name')),
                ('middle_name', ''),
                ('last_name', get_property(transaction, 'last_name')),
                ('email', email),
                ('mobile', get_property(transaction, 'mobile')),
                ('address_line1', ''),
                ('address_line2', ''),
                ('address_landmark', ''),
                ('city', ''),
                ('state', ''),
                ('pincode', ''),

                ('policy_number', get_property(transaction, 'policy_number')),
                ('policy_status', get_property(transaction, 'status')),
                ('premium_paid', get_property(transaction, 'slab.premium')),
                ('transaction_mode', transaction.transaction_mode),
                ('policy_start_date', policy_start_date),
                ('policy_end_date', policy_end_date),
                ('policy_tenure', policy_tenure),
                ('is_policy_expired', is_policy_expired),
                ('policy_document', get_property(transaction, 'policy_document.url')),
                ('insurer_title', insurer_title),
                ('insurer_slug', get_property(transaction, 'slab.plan.insurer.slug')),
                ('policy_name', policy_name),
                ('policy_type', policy_type),
                ('proposal_link', proposal_link),
                ('policy_tat', calculate_tat(transaction.policy_tat)),

                ('payment_on', payment_on or None),
                ('transaction_created', transaction_created or None),
                ('transaction_updated', transaction_updated or None),

                ('is_my_account_created', is_my_account_created),
                ('is_my_account_activated', is_my_account_activated),
                ('is_added_to_my_account', is_added_to_my_account),
                ('my_account_created_on', get_property(user_details, 'useraccount_created_on')),
                ('customer_full_name', get_property(user_details, 'full_name')),
                ('customer_email', get_property(user_details, 'email')),
                ('customer_mobile', get_property(user_details, 'mobile')),
                ('rm_name', get_property(user_details, 'rm_name')),
            ])
        else:
            return {}
    except:
        log_error(logger, msg='Unknown Error Received while fetching values from Transaction')
        return {
            'id': transaction.id,
            'transaction_id': transaction.transaction_id,
        }


def get_user_details(user):
    useraccount = user.useraccount.all()[0] if user.useraccount.all() else None

    # dates
    user_created_on = convert_datetime_to_timezone(user.created_on)
    last_login = convert_datetime_to_timezone(user.last_login)
    useraccount_created_on = get_property(useraccount, 'created_on')
    useraccount_created_on = convert_datetime_to_timezone(useraccount_created_on) if useraccount_created_on else None
    first_payment_date = get_property(useraccount, 'first_payment_date')
    first_payment_date = convert_datetime_to_timezone(first_payment_date) if first_payment_date else None
    dob = user.birth_date

    # address
    address_line1 = get_property(useraccount, 'address_line1')
    address_line2 = get_property(useraccount, 'address_line2')
    address_landmark = get_property(useraccount, 'address_landmark')
    city = get_property(useraccount, 'city')
    state = get_property(useraccount, 'state')
    country = get_property(useraccount, 'country')
    pincode = get_property(useraccount, 'pincode')
    full_address = ', '.join(
        filter(None, [address_line1, address_line2, address_landmark, city, state, country, pincode]))

    return OrderedDict([
        ('user_id', user.id),
        ('first_name', get_property(user, 'first_name')),
        ('middle_name', get_property(user, 'middle_name')),
        ('last_name', get_property(user, 'last_name')),
        ('full_name', user.full_name),
        ('email', user.email),
        ('username', get_property(user, 'username')),
        ('slug', get_property(user, 'slug')),
        ('birth_date', dob or None),
        ('mobile', get_property(useraccount, 'mobile')),
        ('address_line1', address_line1),
        ('address_line2', address_line2),
        ('address_landmark', address_landmark),
        ('city', city),
        ('state', state),
        ('country', country),
        ('pincode', pincode),
        ('full_address', full_address),
        ('rm_name', get_property(user, 'urm.rm.name')),
        ('rm_email', get_property(user, 'urm.rm.email')),
        ('rm_mobile', get_property(user, 'urm.rm.mobile')),
        ('is_active', user.is_active),
        ('is_account_activated', user.has_usable_password()),
        ('user_created_on', user_created_on or None),
        ('useraccount_created_on', useraccount_created_on or None),
        ('last_login', last_login or None),
        ('first_payment_date', first_payment_date or None),
    ])


def get_user_transactions(user):
    uts = UserTransaction.objects.filter(user=user).prefetch_related('transaction')
    expired = {}
    unexpired = {}
    for ut in uts:
        transaction = transaction_field_mapping(ut.transaction)
        if transaction:
            if transaction.get('is_policy_expired') in [None, False]:
                unexpired.setdefault(ut.type, []).append(transaction)
            else:
                expired.setdefault(ut.type, []).append(transaction)
    return {
        'expired': expired,
        'unexpired': unexpired,
    }


def get_user_for_transaction(transaction, proposer_email):
    """
    :param transaction: Transaction object (for checking User-Transaction is created or not)
    :param proposer_email: Email (for checking User exists)
    :return:
    """
    user_transaction = UserTransaction.objects.filter_for_transaction(transaction)
    if user_transaction:
        user_transaction = user_transaction.last()
        user = user_transaction.user
    else:
        try:
            user = User.objects.get_user(email=proposer_email, phone='')
        except (User.DoesNotExist, User.MultipleObjectsReturned):
            user = None
    user_account = (user.useraccount.all()[0] if user.useraccount.all() else None) if user else None

    return user, user_account, user_transaction


@transaction.atomic
def get_or_create_user(email, mobile, transaction=None, mapping={}):
    if transaction:
        mapping = transaction_field_mapping(transaction)

    if email is None:
        email = mapping.get('email')

    if mobile is None:
        mobile = mapping.get('mobile')

    if email and email.lower() == 'none':
        email = None

    if mobile and mobile.lower() == 'none':
        mobile = None

    if email is None and mobile is None and not transaction and not mapping:
        raise Exception("Please provide transaction or mapping.")

    mobile_obj = None

    email_obj = None
    if email:
        email_obj, _ = Email.objects.get_or_create(
            email=email.lower(), is_verified=True
        )

    fuckup_happened = False
    if mobile:
        mobile_obj = Phone.objects.filter(
            number=mobile, is_verified=True
        )
        if mobile_obj.count() > 1:
            fuckup_happened = True
        mobile_obj = mobile_obj.last()
        if not mobile_obj:
            mobile_obj = Phone.objects.create(
                number=mobile, is_verified=True
            )

    if not email and not mobile:
        raise Exception("Email and Mobile cannot be blank.")

    # Get mobile and email lead visitor.
    lead_email = "lead_%s" % email
    lead_mobile = "lead_%s" % mobile

    if mobile:
        lead_mobile = Tracker.objects.filter(session_key=lead_mobile).first()

    if email:
        lead_email = Tracker.objects.filter(session_key=lead_email).first()

    def do_return(user):
        if fuckup_happened:
            extlogger.info(
                "Multiple Verified Phone objects returned for %s and user=%s"
                % (mobile, user.id)
            )
        return user

    # Same user or different user.
    user = None
    if email and not mobile:
        if email_obj.user:
            if lead_email:
                lead_email.attach(email_obj.user)

            return do_return(email_obj.user)
        else:
            user = User.objects.create(
                first_name=mapping.get('first_name', ''),
                middle_name=mapping.get('middle_name', '') or '',
                last_name=mapping.get('last_name', ''),
                is_active=False,
                date_joined=timezone.now(),
                created_on=timezone.now(),
                updated_on=timezone.now(),
                last_login=timezone.now(),
            )
            email_obj.user = user
            email_obj.save()

            # Fix is_primary.
            if not user.emails.filter(is_primary=True).count():
                email_obj.is_primary = True
                email_obj.save()

            if lead_email:
                lead_email.attach(email_obj.user)

            return do_return(email_obj.user)
    elif not email and mobile:
        if mobile_obj.user:
            if lead_mobile:
                lead_mobile.attach(mobile_obj.user)

            return do_return(mobile_obj.user)
        else:
            user = User.objects.create(
                first_name=mapping.get('first_name', ''),
                middle_name=mapping.get('middle_name', '') or '',
                last_name=mapping.get('last_name', ''),
                is_active=False,
                date_joined=timezone.now(),
                created_on=timezone.now(),
                updated_on=timezone.now(),
                last_login=timezone.now(),
            )
            mobile_obj.user = user
            mobile_obj.save()

            # Fix is_primary.
            if not user.phones.filter(is_primary=True).count():
                mobile_obj.is_primary = True
                mobile_obj.save()

            if lead_mobile:
                lead_mobile.attach(mobile_obj.user)

            return do_return(mobile_obj.user)

    if email_obj.user == mobile_obj.user:
        if not email_obj.user:
            # here both email and mobile have non as user
            user = User.objects.create(
                first_name=mapping.get('first_name', ''),
                middle_name=mapping.get('middle_name', '') or '',
                last_name=mapping.get('last_name', ''),
                is_active=False,
                date_joined=timezone.now(),
                created_on=timezone.now(),
                updated_on=timezone.now(),
                last_login=timezone.now(),
            )
            user.set_unusable_password()
            user.save()

            email_obj.user = user
            email_obj.save()

            mobile_obj.user = user
            mobile_obj.save()

        user = email_obj.user
    else:
        if email_obj.user and mobile_obj.user:
            # both email and mobile has users, so we ignore the
            # email object and create a new one. we prefer mobile
            # over email now.
            email_instance, _ = Email.objects.get_or_create(
                email=email,
                is_verified=False,
                is_exists=True,
                user=user
            )

            email_instance.user = mobile_obj.user
            email_instance.save()
            user = mobile_obj.user
        elif email_obj.user and not mobile_obj.user:
            # email.user exists, mobile obj belongs to no one
            mobile_obj.user = email_obj.user
            mobile_obj.save()
            user = email_obj.user
            if mobile_obj.visitor:
                mobile_obj.visitor.attach(user)
        elif (not email_obj.user) and mobile_obj.user:
            # mobile.user exists, and email belongs to none
            email_obj.user = mobile_obj.user
            email_obj.save()
            user = mobile_obj.user
            if email_obj.visitor:
                email_obj.visitor.attach(user)

    # Attach leads to specific user.
    if user.emails.filter(email=email, is_verified=True) and lead_email:
        lead_email.attach(user)

    if user.phones.filter(number=mobile, is_verified=True) and lead_mobile:
        lead_mobile.attach(user)

    # Set users primary email and mobile.
    if not user.emails.filter(is_primary=True).count():
        email = user.emails.filter(is_verified=True).first()
        if email:
            email.is_primary = True
            email.save()

    if not user.phones.filter(is_primary=True).count():
        phone = user.phones.filter(is_verified=True).first()
        if phone:
            phone.is_primary = True
            phone.save()

    if user and not user.token:
        user.token = uuid.uuid4()
        user.token_updated_on = datetime.datetime.now()
        user.save()

    return do_return(user)


def create_useraccount_add_transaction(email, mobile, transaction, active=True):
    """
    1 - If not, create User creation
    2 - If not, create User-rm relationship
    3 - If not, create user-transaction
    4 - If not, create User profile
    5 - If not User profile, send User Account creation
    """
    # user creation
    user = get_or_create_user(email, mobile, transaction)

    # create user-transaction
    try:
        user_transaction = UserTransaction.objects.get(
            object_id=transaction.id,
            content_type=ContentType.objects.get_for_model(transaction)
        )
        create_ticket = False
    except UserTransaction.DoesNotExist:
        user_transaction = user.transactions.create(transaction=transaction)
        create_ticket = True

    # user profile creation
    is_useraccount_created = False

    """ Calculating first_payment_date """
    current_payment_on = get_property(transaction, 'payment_on') or get_property(transaction, 'created_on') \
                         or get_property(transaction, 'created_date')
    first_ut = get_first_user_transaction(user)
    first_payment_on = get_property(first_ut, 'payment_on')

    if current_payment_on.tzinfo:
        current_payment_on = timezone.make_naive(current_payment_on)
    if first_payment_on.tzinfo:
        first_payment_on = timezone.make_naive(first_payment_on)

    if current_payment_on < first_payment_on:
        first_payment_date = current_payment_on
    else:
        first_payment_date = first_payment_on

    try:
        user_account = UserAccount.objects.get(user=user)
    except UserAccount.DoesNotExist:
        is_useraccount_created = True
        address = OrderedDict([
            ('line_1', transaction_field_mapping(transaction).get('address_line1', '')),
            ('line_2', transaction_field_mapping(transaction).get('address_line2', '')),
            ('landmark', transaction_field_mapping(transaction).get('address_landmark', '')),
        ])

        # address
        city = transaction_field_mapping(transaction).get('city', '')
        state = transaction_field_mapping(transaction).get('state', '')
        pincode = transaction_field_mapping(transaction).get('pincode', '')
        city, state = fix_numeric_city_state(city, state, pincode)

        UserAccount.objects.create(
            user=user,
            address=', '.join(['%s' % v for k, v in address.iteritems() if v]),
            address_line1=address['line_1'] or '',
            address_line2=address['line_2'] or '',
            address_landmark=address['landmark'] or '',
            mobile=transaction_field_mapping(transaction).get('mobile', '') or '',
            city=city or '',
            state=state or '',
            pincode=pincode or '',
            first_payment_date=first_payment_date,
        )
    else:
        user_account.first_payment_date = first_payment_date
        user_account.save()

    # create user-rm relationship
    rm = get_leastuser_rm()
    try:
        UserRelationshipManager.objects.get(user=user)
    except UserRelationshipManager.DoesNotExist:
        UserRelationshipManager.objects.create(user=user, rm=rm)

    if create_ticket and user_transaction.type in ['car',
                                                   'bike'] and user_transaction.transaction.status == 'COMPLETED':
        user_transaction.create_ticket.delay()

    if active:
        activate_user_account.send(sender=transaction._meta.model, transaction=transaction)

    return user, is_useraccount_created


def get_leastuser_rm(filt=None, excl=None):
    """
    Get RM object having least users
    """
    rms = RelationshipManager.objects.filter(is_active=True).order_by('id')
    rms = rms.filter(filt) if filt else (rms.exclude(excl) if excl else rms)
    rm_list = [(rm, rm.get_customer_count) for rm in rms]
    sorted_list = sorted(rm_list, key=lambda x: x[1])
    return sorted_list[0][0]


def get_first_user_transaction(user):
    uts = []
    for ut in UserTransaction.objects.filter(user=user):
        ut.transaction.payment_on = ut.transaction.payment_on or get_property(ut.transaction,
                                                                              'created_on') or get_property(
            ut.transaction, 'created_date')
        uts.append(ut.transaction)

    if uts:
        sorted_uts = sorted(uts, key=lambda x: x.payment_on, reverse=False)
        return sorted_uts[0]
    else:
        return []


def send_account_creation_mail(user, transaction=None):
    if get_property(transaction, 'quote.type') == 'offline':
        # FIXME: Hack to send policy upload notification of offline transaction instead of welcome mail
        send_policy_upload_notification(user, transaction)
    else:
        # COMMENTING SINCE EMAIL CONTENT GOT CHANGED AS PER THE NEW OTP LOGIN
        # obj = {
        #     'reset_url': '{}{}'.format(settings.SITE_URL, reverse('reset_password', kwargs={'token': user.token})),
        #     'rsa_policy': get_property(transaction, 'rsa_policy'),
        #     'user_id': user.id,
        #     'to_email': user.email,
        #     'name': clean_name(user.get_full_name()),
        #     'user': user,
        #     'rm': user.urm.rm,
        # }
        obj = {
            'name': clean_name(user.get_full_name()),
            'manage_policies_url': urlparse.urljoin(settings.SITE_URL, reverse('account_manager:manage_policies')),
            'transaction_type': transaction.transaction_type,
            'rsa_policy': get_property(transaction, 'rsa_policy'),
        }
        cf_cns.notify('USER_ACCOUNT_CREATION', to=(user.get_full_name(), user.email), sender_name="Coverfox Support",
                      sender_email="help@coverfox.com", obj=obj, mandrill_template='newbase')


def send_policy_upload_notification(user, transaction):
    # COMMENTING SINCE EMAIL CONTENT GOT CHANGED AS PER THE NEW OTP LOGIN
    # all_products = ['car', 'bike', 'health', 'travel', 'life']
    # current_product = transaction.transaction_type
    # other_products = [product for product in all_products if product is not current_product]
    # data = {
    #     'transaction_type': current_product,
    #     'other_products': other_products,
    #     'user_login_url': '{}{}'.format(settings.SITE_URL, reverse('user_login')),
    #     'rsa_policy': get_property(transaction, 'vehicle_type') == 'Private Car',
    #     'name': clean_name(user.get_full_name()),
    #     'user': user,
    #     'rm': user.urm.rm,
    # }
    data = {
        'name': clean_name(user.get_full_name()),
        'manage_policies_url': urlparse.urljoin(settings.SITE_URL, reverse('account_manager:manage_policies')),
        'transaction_type': transaction.transaction_type,
        'rsa_policy': get_property(transaction, 'rsa_policy'),
    }
    cf_cns.notify('POLICY_UPLOAD_NOTIFICATION', to=(user.get_full_name(), user.email), sender_name="Coverfox Support",
                  sender_email="help@coverfox.com", obj=data, mandrill_template='newbase')

# NO USAGES FOUND
# def _send_mail(user, data):
#     obj = {
#         'user_id': user.id,
#         'bcc': ['policydump@coverfox.com'],
#         'to_email': user.email,
#         'name': user.get_full_name(),
#         'from_email': user.urm.rm.email,
#         'from_name': user.urm.rm.name,
#         'user': user,
#         'rm': user.urm.rm,
#     }
#     obj.update(data)
#     cf_cns.notify('USER_ACCOUNT_CREATION', to=(user.get_full_name(), user.email),
#                   sender_name=user.urm.rm.name, sender_email=user.urm.rm.email, obj=obj)
#     # get_connection(fail_silently=False).send_messages([obj])


# EXPORT - report
def generate_report(headers, rows, filename=''):
    """A view that streams a large CSV file."""
    # Generate a sequence of rows. The range is based on the maximum number of
    # rows that can be handled by a single sheet in most spreadsheet
    # applications.
    filename = filename or str(datetime.datetime.now())
    rows = chain(headers, rows)
    report_folder = 'reports/account_manager'
    file_path = os.path.join(report_folder, filename)
    with default_storage.open(file_path, 'wb') as out_file:
        writer = UnicodeWriter(out_file)
        rows = chain(headers, rows)
        writer.writerows(rows)
    return out_file


@app.task(queue='reports')
def generate_user_account_report(user, filename, export_type, query):
    # TODO : catch exception if any while pulling reports (only) in all below reports

    user_accounts = UserAccount.objects.filter(query).order_by('-first_payment_date').iterator()

    # headers - generator of list
    def headers():
        yield get_user_details(User()).keys()

    # values - generator of list
    def values():
        for ua in user_accounts:
            data = get_user_details(ua.user)
            values = []
            for value in data.values():
                _value = convert_to_unicode(value)
                if isinstance(value, datetime.datetime):
                    _value = _value[0:19]
                values.append(_value)
            yield values

    report = generate_report(headers(), values(), filename)

    generated_time = datetime.datetime.now()
    report_url = default_storage.url("reports/account_manager" + report.name)

    cache.set('cfsupport_{}_report'.format(export_type), generated_time, timeout=1 * 60 * 60)
    cache.set('cfsupport_{}_report_url'.format(export_type), report_url, timeout=1 * 60 * 60)

    send_report_mail(
        subject='New {} report generated at {}'.format(user_accounts, generated_time),
        message='Report download link is available at {}'.format(report_url),
        user=user,
    )
    cache.delete('cfsupport_{}_lock'.format(export_type))


@app.task(queue='reports')
def generate_transactions_report(user, filename, transaction_type, query):
    Transaction = TRANSACTION_MODEL_MAPPING[transaction_type]
    transactions = Transaction.objects.filter(query).iterator()

    # headers - generator of list
    def headers():
        yield transaction_field_mapping(Transaction()).keys()

    # values - generator of list
    def values():
        for transaction in transactions:
            data = transaction_field_mapping(transaction)
            values = []
            for value in data.values():
                _value = convert_to_unicode(value)
                if isinstance(value, datetime.datetime):
                    _value = _value[0:19]
                values.append(_value)
            yield values

    report = generate_report(headers(), values(), filename)

    generated_time = datetime.datetime.now()
    report_url = default_storage.url(report.name)

    cache.set('cfsupport_{}_report'.format(transaction_type), generated_time, timeout=1 * 60 * 60)
    cache.set('cfsupport_{}_report_url'.format(transaction_type), report_url, timeout=1 * 60 * 60)

    send_report_mail(
        subject='New {} report generated at {}'.format(transaction_type, generated_time),
        message='Report download link is available at {}'.format(report_url),
        user=user,
    )
    cache.delete('cfsupport_{}_lock'.format(transaction_type))


@app.task(queue='reports')
def generate_transactions_vs_useraccounts_report(user_obj, filename, export_type, from_date, to_date):

    fields_mapping = OrderedDict([
        ('type', 'type'),
        ('id', 'id'),
        ('transaction_id', 'transaction_id'),
        ('first_name', 'first_name'),
        ('last_name', 'last_name'),
        ('email', 'email'),
        ('mobile', 'mobile'),
        ('policy_status', 'policy_status'),
        ('policy_document', 'policy_document'),
        ('policy_number', 'policy_number'),
        ('rsa_policy', 'rsa_policy'),
        ('rsa_policy_number', 'rsa_policy_number'),
        ('policy_start_date', 'policy_start_date'),
        ('policy_end_date', 'policy_end_date'),
        ('premium_paid', 'premium_paid'),
        ('insurer_slug', 'insurer_slug'),
        ('transaction_mode', 'transaction_mode'),
        ('transaction_created', 'transaction_created'),
        ('transaction_updated', 'transaction_updated'),
        ('is_my_account_created', 'is_my_account_created'),
        ('is_my_account_activated', 'is_my_account_activated'),
        ('is_added_to_my_account', 'is_added_to_my_account'),
        ('is_my_account_created', 'is_my_account_created'),
        ('is_my_account_activated', 'is_my_account_activated'),
        ('is_added_to_my_account', 'is_added_to_my_account'),
        ('my_account_created_on', 'my_account_created_on'),
        ('customer_full_name', 'customer_full_name'),
        ('customer_email', 'customer_email'),
        ('customer_mobile', 'customer_mobile'),
        ('rm_name', 'rm_name'),
    ])

    def get_data(fields_mapping):

        for transaction_type, Transaction in TRANSACTION_MODEL_MAPPING.iteritems():

            if from_date and to_date:
                transactions = Transaction.objects.filter(created_range_query(from_date, to_date, transaction_type))

            query = Q(status='COMPLETED') | Q(status='MANUAL COMPLETED')
            transactions = transactions.filter(query).order_by('-pk')

            for transaction in transactions.iterator():
                transaction_data = transaction_field_mapping(transaction)

                data = []
                for field_name in fields_mapping.itervalues():
                    value = transaction_data.get(field_name)
                    _value = convert_to_unicode(value)
                    if isinstance(value, datetime.datetime):
                        _value = _value[0:19]
                    data.append(_value)

                yield data

    rows = get_data(fields_mapping)
    headers = iter([fields_mapping.keys()])

    report = generate_report(headers, rows, filename)

    generated_time = datetime.datetime.now()
    report_url = default_storage.url(report.name)

    cache.set('cfsupport_{}_report'.format(export_type), generated_time, timeout=1 * 60 * 60)
    cache.set('cfsupport_{}_report_url'.format(export_type), report_url, timeout=1 * 60 * 60)

    send_report_mail(
        subject='New {} report generated at {}'.format('tranasctions_vs_useraccounts', generated_time),
        message='Report download link is available at {}'.format(report_url),
        user=user_obj,
    )
    cache.delete('cfsupport_{}_lock'.format(export_type))


def send_report_mail(subject, message, user):
    email = EmailMessage(subject=subject,
                         body=message,
                         from_email='parag@coverfoxmail.com',
                         to=['{} <{}>'.format(user.full_name, user.email)],
                         bcc=['parag@coverfoxmail.com'])
    email.send()


@app.task(queue='escalations')
def update_transaction_ticket(response, ut_id, update_type):
    if not response:
        return

    ut = UserTransaction.objects.get(id=ut_id)

    if 'retry_after' in response:
        getattr(ut, update_type).apply_async(countdown=response['retry_after'])
        return False

    data = response['helpdesk_ticket']
    ut.ticket_id = data['display_id']
    ut.ticket_status = int(data['status'])
    ut.save(update_fields=['ticket_id', 'ticket_status'])

    return True


@app.task(bind=True, queue='escalations')
def monitor_user_transaction(task, ticket_created, user_transaction_id, level):
    return
    # if not ticket_created:
    #     return
    #
    # ut = UserTransaction.objects.get(id=user_transaction_id)
    # if ut.ticket_status != UserTransaction.CLOSED:
    #     if ut.transaction.policy_document:
    #         ut.close_ticket.delay()
    #     else:
    #         level += 1
    #         send_escalation_mail(user_transaction=ut, level=level)
    #         task.apply_async(args=(True, ut.id, level),
    #                          countdown=datetime.timedelta(hours=min(24, 12 * level), minutes=5).total_seconds())


def send_escalation_mail(user_transaction, level=1):
    levels = [
        ('shweta@coverfox.com', 'akhil@coverfox.com'),
        'dhinakaran@coverfoxmail.com',
        'tm@coverfoxmail.com'
    ]
    cc = []
    for email_id in levels[:level]:
        if isinstance(email_id, (list, tuple)):
            cc.extend(email_id)
        else:
            cc.append(email_id)

    email = EmailMessage(
        subject='Policy is not yet uploaded',
        body=u"""\
        Transaction id: {} ({})
        Product: {}
        Freshdesk ticket id: {}
        """.format(
            user_transaction.object_id,
            user_transaction.transaction.transaction_id,
            user_transaction.type,
            user_transaction.ticket_id,
        ),
        from_email='alerts@coverfox.com',
        to=[user_transaction.user.urm.rm.email],
        cc=cc,
        connection=get_connection(None if settings.PRODUCTION else locmem.EmailBackend),
    )

    return email.send()
