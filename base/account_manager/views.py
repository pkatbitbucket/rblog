import json
from django.db.models import Q
import os
import mimetypes

from django.http import HttpResponse, Http404, HttpResponseRedirect, \
    HttpResponseForbidden
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.decorators import login_required
from wsgiref.util import FileWrapper

from account_manager.models import UserRelationshipManager

import account_manager.utils as account_manager_utils

from freshdesk import create_ticket
from otp.decorators import otp_login_required
from utils import get_property


def impersonator_required(view_func):
    def inner(request, *args, **kwargs):
        if 'coverfoxstaff_user_id' in request.session:
            return view_func(request, *args, **kwargs)
        return HttpResponseRedirect('/user/manage-policies/')
    return inner


@impersonator_required
def profile(request):
    user = request.user
    transactions = account_manager_utils.get_user_transactions(user)
    unexpired_transaction_count = sum(len(t) for t in transactions['unexpired'].values())
    expired_transaction_count = sum(len(t) for t in transactions['expired'].values())
    context = {
        'user_detail': account_manager_utils.get_user_details(user),
        'rm': get_property(user, 'urm.rm'),
        'unexpired_transaction_count': unexpired_transaction_count,
        'expired_transaction_count': expired_transaction_count,
        'selected': 'profile',
    }
    template = 'account_manager/profile.html'
    return render_to_response(template, RequestContext(request, context))


@impersonator_required
def policy(request):
    user = request.user
    transactions = account_manager_utils.get_user_transactions(user)
    unexpired_transaction_count = sum(len(t) for t in transactions['unexpired'].values())
    expired_transaction_count = sum(len(t) for t in transactions['expired'].values())

    context = {
        'rm': get_property(user, 'urm.rm'),
        'unexpired_transaction_count': unexpired_transaction_count,
        'expired_transaction_count': expired_transaction_count,
        'transactions': transactions['unexpired'],
        'selected': 'policy',
    }
    template = 'account_manager/policy.html'
    return render_to_response(template, RequestContext(request, context))


@impersonator_required
def expired(request):
    user = request.user
    transactions = account_manager_utils.get_user_transactions(user)
    unexpired_transaction_count = sum(len(t) for t in transactions['unexpired'].values())
    expired_transaction_count = sum(len(t) for t in transactions['expired'].values())

    context = {
        'rm': get_property(user, 'urm.rm'),
        'unexpired_transaction_count': unexpired_transaction_count,
        'expired_transaction_count': expired_transaction_count,
        'transactions': transactions['expired'],
        'selected': 'expired',
    }
    template = 'account_manager/policy.html'
    return render_to_response(template, RequestContext(request, context))


def download_document(request, transaction_id, type, document_type):
    if not(request.otp_user or request.user.is_authenticated()):
        return HttpResponseForbidden()
    try:
        Transaction = account_manager_utils.TRANSACTION_MODEL_MAPPING[type]
        transaction = get_object_or_404(Transaction, transaction_id=transaction_id)
    except KeyError:
        raise Http404
    else:
        document_mapping = {
            'policy_document': get_property(transaction, 'policy_document'),
            'rsa_policy': get_property(transaction, 'rsa_policy'),
        }

        try:
            document_obj = document_mapping[document_type]
        except KeyError:
            raise Http404
        else:
            if document_obj:
                file_path = document_obj.url
                file_name = os.path.basename(document_obj.name)
                file_wrapper = FileWrapper(document_obj)
                file_mimetype = mimetypes.guess_type(file_path)
                response = HttpResponse(file_wrapper, content_type=file_mimetype)
                response['Content-Length'] = len(response.content)
                response['Content-Disposition'] = 'attachment; filename=%s' % file_name
                return response
            else:
                raise Http404


@login_required
def submit_claim(request):
    if request.is_ajax and request.POST and request.POST.get('data'):

        data = json.loads(request.POST['data'])
        transaction_type = data.get("category", None)
        transaction_id = data.get("transaction_id", None)

        if transaction_type not in account_manager_utils.TRANSACTION_MODEL_MAPPING:
            raise Http404

        Transaction = account_manager_utils.TRANSACTION_MODEL_MAPPING[transaction_type]
        transaction = get_object_or_404(Transaction, transaction_id=transaction_id)

        email = request.user.email
        cc_emails = ['soman.soni@coverfox.com']
        subject = data.get('subject', "NA")

        description = "Description:\n"
        description += data.get('description', '')
        description += "\n\nContact:"
        description += "\nCustomer mobile # 1: " + data.get("mobile_1", "NA") + \
            "\nCustomer mobile # 2: " + data.get("mobile_2", "NA")

        transaction_detail = account_manager_utils.transaction_field_mapping(transaction)

        description += "\n\n Policy Details:"
        description += "\n Transaction ID - {}".format(transaction_detail['transaction_id'])
        description += "\n Status - {}".format(transaction_detail['policy_status'])
        description += "\n Policy number - {}".format(transaction_detail['policy_number'])
        description += "\n Policy document - {}".format(transaction_detail['policy_document'])
        description += "\n Policy start date - {}".format(transaction_detail['policy_start_date'])
        description += "\n Policy end date - {}".format(transaction_detail['policy_end_date'])
        description += "\n Created on - {}".format(transaction_detail['transaction_created'])
        description += "\n First name - {}".format(transaction_detail['first_name'])
        description += "\n Last name - {}".format(transaction_detail['last_name'])
        description += "\n Email - {}".format(transaction_detail['email'])
        description += "\n Mobile - {}".format(transaction_detail['mobile'])

        try:
            rm = UserRelationshipManager.objects.get(user=request.user).rm
        except UserRelationshipManager.DoesNotExist:
            return HttpResponse(json.dumps({'success': False}))
        else:
            custom_field = {
                'policy_number_106763': transaction_detail['policy_number'],
                'policy_category_106763': transaction_type,
                'rm_name_106763': rm.name,
                'rm_mobile_106763': rm.mobile,
                'customer_name_106763': request.user.get_full_name(),
            }
            ticket_type = 'Claim Request'

            create_ticket.delay(email=email,
                                cc_emails=cc_emails,
                                subject=subject,
                                description=description,
                                responder_id=rm.freshdesk_responder_id,
                                custom_field=custom_field,
                                ticket_type=ticket_type)
            return HttpResponse(json.dumps({'success': True}))


@otp_login_required(nid='MYACCOUNT_OTP')
def manage_policies(request):
    contact = request.otp_user.contact
    filter = (Q(status='COMPLETED') | Q(status='MANUAL COMPLETED'))
    all_transactions = {
        'health': account_manager_utils.get_health_transactions(q=contact, filt=filter, fields=['email', 'mobile']),
        'car': account_manager_utils.get_motor_transactions(q=contact, filt=filter & Q(vehicle_type='Private Car'),
                                                            fields=['email', 'mobile']),
        'bike': account_manager_utils.get_motor_transactions(q=contact, filt=filter & Q(vehicle_type='Twowheeler'),
                                                             fields=['email', 'mobile']),
        'travel': account_manager_utils.get_travel_transactions(q=contact, filt=filter, fields=['email', 'mobile']),
    }

    transactions = {}
    transaction_count = 0
    for product, txns in all_transactions.items():
        for t in txns:
            transactions.setdefault(product, []).append(account_manager_utils.transaction_field_mapping(t))
            transaction_count += 1

    context = {
        'transaction_count': transaction_count,
        'transactions': transactions,
    }
    template = 'account_manager/manage_policies.html'
    return render_to_response(template, RequestContext(request, context))
