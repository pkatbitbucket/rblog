from django.contrib import admin
from activity.models import *


class DataAdmin(admin.ModelAdmin):
    pass
admin.site.register(Data, DataAdmin)


class HealthCareCenterAdmin(admin.ModelAdmin):
    pass
admin.site.register(HealthCareCenter, HealthCareCenterAdmin)
