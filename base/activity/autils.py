from celery_app import app
from .models import Data


@app.task
def save_hospital_data(cleaned_xl_data, insurer_obj):
    for cdata in cleaned_xl_data:
        cdata.pop('lineno')
        for i in cdata.keys():
            if i not in ['name', 'address', 'city', 'pin', 'std', 'phone', 'email', 'state', 'url', 'raw']:
                cdata.pop(i)
        data = Data(**cdata)
        data.by_insurer = insurer_obj
        data.save()
