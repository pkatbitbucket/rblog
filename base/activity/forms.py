from django import forms

from cf_fhurl import RequestModelForm
from django.shortcuts import get_object_or_404

from activity.models import HealthCareCenter, Data
from wiki.models import City, Insurer, State


class XlsDataUpload(forms.Form):
    words = forms.CharField()
    xls_file = forms.FileField()


class HospitalXlsValidate(forms.Form):
    lineno = forms.IntegerField()
    name = forms.CharField(max_length=255)
    address1 = forms.CharField(required=False)
    address2 = forms.CharField(required=False)
    address3 = forms.CharField(required=False)
    address4 = forms.CharField(required=False)
    address5 = forms.CharField(required=False)
    address = forms.CharField(required=False)
    city = forms.CharField(required=False)
    pin = forms.CharField(required=False)
    std = forms.CharField(required=False)
    phone = forms.CharField(required=False)
    email = forms.EmailField(required=False)
    state = forms.CharField()
    url = forms.URLField(required=False)
    raw = forms.CharField(required=False)

    def clean_state(self):
        d = self.cleaned_data
        state = State.objects.filter(name=d.get('state'))
        if not state:
            raise forms.ValidationError("%s named state not found at line no %s" % (d.get('state'), d.get('lineno')))
        else:
            return state[0]

    def clean_pin(self):
        d = self.cleaned_data
        if not d.get('pin').strip():
            return ""
        try:
            pin = int(float(d.get('pin')))
        except:
            raise forms.ValidationError("Pin code must be integer at line no %s, pin is %s" % (d.get('lineno'), d.get('pin')))
        return pin

    def clean_address(self):
        d = self.cleaned_data
        address = [d[i] for i in sorted(d.keys()) if i.startswith('address') and d[i]]
        return ','.join(address)

    def clean(self):
        d = self.cleaned_data
        if not self.errors:
            raw = u'|'.join((self.cleaned_data['name'], self.cleaned_data['address'], self.cleaned_data['city'], self.cleaned_data['state'].name, str(self.cleaned_data['pin'])))
            d['raw'] = raw
        return d


class DataAddForm(forms.Form):
    name = forms.CharField(max_length=255)
    address = forms.CharField(widget=forms.Textarea(attrs={'rows': 4, 'cols': 40}))
    city = forms.CharField(max_length=200)
    pin = forms.IntegerField()
    std = forms.CharField()
    phone = forms.CharField()
    email = forms.EmailField(required=False)
    url = forms.URLField(required=False)


class HealthCareCenterBasicForm(RequestModelForm):
    city_name = forms.CharField()
    city = forms.CharField(widget=forms.HiddenInput())
    pincode = forms.IntegerField()
    std_code = forms.IntegerField()

    class Meta:
        model = HealthCareCenter
        exclude = ('data', 'insurer')

    def clean_city(self):
        d = self.cleaned_data
        city = City.objects.filter(id=d.get('city'))
        if not city:
            raise forms.ValidationError("City not found")
        else:
            return city[0]

    def init(self, data_id):
        hcc = HealthCareCenter.objects.get(id=data_id)
        self.instance = hcc
        if self.instance.city:
            self.fields['city_name'].initial = "%s" % self.instance.city.name
            self.state_name = self.instance.city.state.name
            self.fields['city'].initial = self.instance.city.id

    def save(self):
        hcc = super(HealthCareCenterBasicForm, self).save()
        hcc.created_by = self.request.user
        hcc.save()
        return {'success': True, 'status': hcc.disposition}


class HealthCareCenterForm(RequestModelForm):
    city_name = forms.CharField()
    city = forms.CharField(widget=forms.HiddenInput())
    pincode = forms.IntegerField()
    std_code = forms.IntegerField()

    class Meta:
        model = HealthCareCenter
        exclude = ('data', 'insurer')

    def clean_city(self):
        d = self.cleaned_data
        city = City.objects.filter(id=d.get('city'))
        if not city:
            raise forms.ValidationError("City not found")
        else:
            return city[0]

    def init(self, insurer_id, data_id=None):
        self.insurer = get_object_or_404(Insurer, pk=insurer_id)
        if data_id:
            if self.request.GET.get('hospital') and self.request.GET.get('hospital') == 'true':
                hcc = get_object_or_404(HealthCareCenter, pk=data_id)
                self.rdata = hcc.data_set.all()
                self.duplicate_data_set = []
                if self.rdata:
                    self.rdata = self.rdata[0]
                    self.duplicate_data_set = Data.objects.filter(
                        duplicate=self.rdata)
                self.instance = hcc
                if self.instance.city:
                    self.fields[
                        'city_name'].initial = "%s" % self.instance.city.name
                    self.state_name = self.instance.city.state.name
                    self.fields['city'].initial = self.instance.city.id

            else:
                self.rdata = get_object_or_404(Data, pk=data_id)
                self.duplicate_data_set = Data.objects.filter(
                    duplicate=self.rdata)
                hcc = HealthCareCenter.objects.filter(data=self.rdata)
                if len(hcc) > 1:
                    raise Exception(
                        "Panic! More than one healthcenter for Data model id %s" % self.rdata.id)
                if hcc:
                    self.instance = hcc[0]
                    if self.instance.city:
                        self.fields[
                            'city_name'].initial = "%s" % self.instance.city.name
                        self.state_name = self.instance.city.state.name
                        self.fields['city'].initial = self.instance.city.id

    def save(self):
        hcc = super(HealthCareCenterForm, self).save()
        hcc.created_by = self.request.user
        hcc.save()
        if self.rdata:
            hcc.data_set.add(self.rdata)
        hcc.insurer.add(self.insurer)
        return {'success': True, 'data_id': self.rdata.id, 'status': hcc.disposition}
