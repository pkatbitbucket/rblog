# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Data',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('raw', models.TextField()),
                ('name', models.CharField(max_length=255, blank=True)),
                ('address', models.TextField(blank=True)),
                ('city', models.CharField(max_length=200, blank=True)),
                ('state', models.CharField(max_length=200, blank=True)),
                ('pin', models.TextField(blank=True)),
                ('std', models.TextField(blank=True)),
                ('phone', models.TextField(blank=True)),
                ('email', models.TextField(blank=True)),
                ('latlng', models.TextField(blank=True)),
                ('fax', models.TextField(blank=True)),
                ('url', models.CharField(max_length=255, blank=True)),
                ('tpas', models.TextField(blank=True)),
                ('insurer', models.TextField(blank=True)),
                ('search_term', models.TextField(blank=True)),
                ('places_raw', jsonfield.fields.JSONField(blank=True)),
                ('geocode_raw', jsonfield.fields.JSONField(blank=True)),
                ('latitude', models.CharField(max_length=200, blank=True)),
                ('longitude', models.CharField(max_length=200, blank=True)),
                ('is_completed', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('is_deleted', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='HealthCareCenter',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('verification_level', models.CharField(blank=True, max_length=100, choices=[(b'GIS_NOT_FOUND', b'GIS not found'), (b'PINCODE_NOT_FOUND', b'Pincode not found'), (
                    b'SPOKEN_TO', b'Spoken to'), (b'SECONDARY_SOURCES', b'Secondary sources'), (b'UNABLE_TO_CONTACT', b'Unable to contact')])),
                ('name', models.TextField(verbose_name='Hospital Name')),
                ('address1', models.TextField(verbose_name='Address(required)')),
                ('landmark', models.CharField(max_length=200, blank=True)),
                ('district', models.CharField(max_length=200, blank=True)),
                ('pincode', models.CharField(max_length=200)),
                ('std_code', models.CharField(max_length=10, null=True,
                                              verbose_name='STD Code (required)', blank=True)),
                ('phone1', models.CharField(max_length=255, null=True,
                                            verbose_name='Phone-1 (required)', blank=True)),
                ('phone2', models.CharField(max_length=255, null=True, blank=True)),
                ('fax1', models.CharField(max_length=255, null=True, blank=True)),
                ('fax2', models.CharField(max_length=255, null=True, blank=True)),
                ('email', models.EmailField(max_length=254, blank=True)),
                ('url', models.CharField(max_length=200, blank=True)),
                ('lat', models.FloatField(null=True, blank=True)),
                ('lng', models.FloatField(null=True, blank=True)),
                ('googledata', models.TextField(blank=True)),
                ('no_of_beds', models.IntegerField(null=True, blank=True)),
                ('no_of_major_ots', models.IntegerField(null=True, blank=True)),
                ('owned_ambulances', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('registered_blood_bank', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('remarks', models.TextField(blank=True)),
                ('disposition', models.CharField(max_length=100, choices=[(b'INCOMPLETE', b'Incomplete'), (b'COMPLETED', b'Completed'), (b'GIVEUP', b'Cannot be completed, give up'), (b'CALL_LATER', b'Call Later'), (
                    b'WRONG_NUMBER', b'Wrong Number'), (b'NOT_CONNECTING', b'Not Connecting'), (b'HOSPITAL_CLOSED', b'Hospital Closed'), (b'NOT_INTERESTED', b'Not Interested'), (b'FOR_CALLING', b'For Calling'), (b'DUPLICATE', b'DUPLICATE')])),
                ('is_deleted', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='IRDAList',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('raw', models.TextField()),
                ('name', models.CharField(max_length=255, blank=True)),
                ('address', models.TextField(blank=True)),
                ('city', models.CharField(max_length=200, blank=True)),
                ('district', models.CharField(max_length=200, blank=True)),
                ('state', models.CharField(max_length=200, blank=True)),
                ('pin', models.TextField(blank=True)),
                ('std', models.TextField(blank=True)),
                ('phone', models.TextField(blank=True)),
                ('email', models.TextField(blank=True)),
                ('fax', models.TextField(blank=True)),
                ('url', models.CharField(max_length=255, blank=True)),
                ('tpas', models.TextField(blank=True)),
                ('insurer', models.TextField(blank=True)),
                ('lat', models.CharField(max_length=200, blank=True)),
                ('lng', models.CharField(max_length=200, blank=True)),
                ('query', models.TextField(blank=True)),
                ('is_completed', models.BooleanField(default=False)),
                ('is_processed', models.BooleanField(default=False)),
                ('googledata', models.TextField(blank=True)),
                ('status', models.TextField(blank=True)),
                ('no_of_beds', models.IntegerField(null=True, blank=True)),
                ('no_of_major_ots', models.IntegerField(null=True, blank=True)),
                ('owned_ambulances', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('registered_blood_bank', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('remarks', models.TextField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='LeadData',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('lead_id', django_extensions.db.fields.UUIDField(
                    editable=False, blank=True)),
                ('email', models.EmailField(unique=True, max_length=254)),
                ('lead_data', jsonfield.fields.JSONField(
                    default={}, verbose_name='Lead Data', blank=True)),
                ('unsubscribed', models.BooleanField(default=False)),
                ('mail_sent_data', jsonfield.fields.JSONField(
                    default=[], verbose_name='Mail Sent Data', blank=True)),
                ('next_mail_on', models.DateTimeField(null=True, blank=True)),
                ('mail_sent', models.BooleanField(default=False)),
                ('mail_type', models.CharField(max_length=255)),
                ('last_mail_sent_on', models.DateTimeField(null=True, blank=True)),
            ],
        ),
    ]
