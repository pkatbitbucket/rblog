# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('activity', '0001_initial'),
        ('wiki', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='healthcarecenter',
            name='city',
            field=models.ForeignKey(blank=True, to='wiki.City', null=True),
        ),
        migrations.AddField(
            model_name='healthcarecenter',
            name='created_by',
            field=models.ForeignKey(
                blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='healthcarecenter',
            name='duplicate',
            field=models.ForeignKey(
                blank=True, to='activity.HealthCareCenter', null=True),
        ),
        migrations.AddField(
            model_name='healthcarecenter',
            name='insurer',
            field=models.ManyToManyField(to='wiki.Insurer', blank=True),
        ),
        migrations.AddField(
            model_name='healthcarecenter',
            name='state',
            field=models.ForeignKey(blank=True, to='wiki.State', null=True),
        ),
        migrations.AddField(
            model_name='data',
            name='assigned_to',
            field=models.ForeignKey(
                blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='data',
            name='by_insurer',
            field=models.ForeignKey(blank=True, to='wiki.Insurer', null=True),
        ),
        migrations.AddField(
            model_name='data',
            name='duplicate',
            field=models.ForeignKey(blank=True, to='activity.Data', null=True),
        ),
        migrations.AddField(
            model_name='data',
            name='health_care_center',
            field=models.ForeignKey(
                blank=True, to='activity.HealthCareCenter', null=True),
        ),
    ]
