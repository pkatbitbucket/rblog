# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('activity', '0002_auto_20150807_1908'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='healthcarecenter',
            name='verification_level',
        ),
        migrations.AlterField(
            model_name='healthcarecenter',
            name='disposition',
            field=models.CharField(max_length=100, choices=[(b'INCOMPLETE', b'Incomplete'), (b'COMPLETED', b'Completed')]),
        ),
    ]
