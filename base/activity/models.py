from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models import permalink
from django.contrib.auth.models import UserManager, AbstractBaseUser, BaseUserManager
from django.conf import settings
from django_extensions.db.fields import UUIDField
from django.core import serializers
from jsonfield import JSONField
from django.utils import timezone
import json
import hashlib
import random
import re
import datetime
from wiki.models import *
from core.models import User


# TODO: Delete this model. not in use anymore
class LeadData(models.Model):
    lead_id = UUIDField()
    email = models.EmailField(unique=True)
    # name, phonenumber and other details
    lead_data = JSONField(_('Lead Data'), blank=True, default={})
    unsubscribed = models.BooleanField(default=False)
    # array of mail sent data e.g. [{'date' : '09/09/2014 08:30pm', 'subject'
    # : 'Welcome to coverfox'}]
    mail_sent_data = JSONField(_('Mail Sent Data'), blank=True, default=[])

    # next_mail_on will be set by any process and fired by cron
    # Any process setting this time stamp must also set mail_sent to False
    next_mail_on = models.DateTimeField(blank=True, null=True)
    mail_sent = models.BooleanField(default=False)
    mail_type = models.CharField(max_length=255)

    last_mail_sent_on = models.DateTimeField(blank=True, null=True)


# TODO: Delete this model. not in use anymore
class IRDAList(models.Model):
    raw = models.TextField()
    name = models.CharField(max_length=255, blank=True)
    address = models.TextField(blank=True)
    city = models.CharField(max_length=200, blank=True)
    district = models.CharField(max_length=200, blank=True)
    state = models.CharField(max_length=200, blank=True)
    pin = models.TextField(blank=True)
    std = models.TextField(blank=True)
    phone = models.TextField(blank=True)
    email = models.TextField(blank=True)
    fax = models.TextField(blank=True)
    url = models.CharField(max_length=255, blank=True)

    tpas = models.TextField(blank=True)
    insurer = models.TextField(blank=True)

    lat = models.CharField(max_length=200, blank=True)
    lng = models.CharField(max_length=200, blank=True)
    query = models.TextField(blank=True)
    is_completed = models.BooleanField(default=False)
    is_processed = models.BooleanField(default=False)
    googledata = models.TextField(blank=True)
    status = models.TextField(blank=True)

    no_of_beds = models.IntegerField(blank=True, null=True)
    no_of_major_ots = models.IntegerField(null=True, blank=True)
    owned_ambulances = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")), blank=True, null=True)
    registered_blood_bank = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")), blank=True, null=True)
    remarks = models.TextField(blank=True)


class Data(models.Model):
    # raw search to be passed to google
    raw = models.TextField()
    name = models.CharField(max_length=255, blank=True)
    address = models.TextField(blank=True)
    city = models.CharField(max_length=200, blank=True)
    state = models.CharField(max_length=200, blank=True)
    pin = models.TextField(blank=True)
    std = models.TextField(blank=True)
    phone = models.TextField(blank=True)
    email = models.TextField(blank=True)
    latlng = models.TextField(blank=True)
    fax = models.TextField(blank=True)
    url = models.CharField(max_length=255, blank=True)

    tpas = models.TextField(blank=True)
    insurer = models.TextField(blank=True)

    search_term = models.TextField(blank=True)
    places_raw = JSONField(blank=True)
    geocode_raw = JSONField(blank=True)
    latitude = models.CharField(max_length=200, blank=True)
    longitude = models.CharField(max_length=200, blank=True)
    is_completed = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")), blank=True, null=True)
    assigned_to = models.ForeignKey(User, blank=True, null=True)
    health_care_center = models.ForeignKey(
        'HealthCareCenter', blank=True, null=True)
    # is_deleted us being used for marking data as duplicate
    is_deleted = models.BooleanField(default=False)
    duplicate = models.ForeignKey('self', null=True, blank=True)
    by_insurer = models.ForeignKey(Insurer, blank=True, null=True)

    def __unicode__(self):
        return u"%s" % (self.raw)

    def format(self):
        verb = "%s, %s, %s, %s" % (
            self.name, self.address, self.city, self.state)
        if self.pin:
            verb = "%s\n pin:%s" % (verb, self.pin)
        if self.std:
            verb = "%s\n std:%s" % (verb, self.std)
        if self.phone:
            verb = "%s\n phone:%s" % (verb, self.phone)
        if self.email:
            verb = "%s\n mail:%s" % (verb, self.email)
        if self.fax:
            verb = "%s\n fax:%s" % (verb, self.fax)
        if self.latlng:
            verb = "%s\n LatLng(%s)" % (verb, self.latlng)
        if self.url:
            verb = "%s\n web:(%s)" % (verb, self.url)

        return verb


class HealthCareCenter(models.Model):
    DISPOSITION_CHOICES = (
        ('INCOMPLETE', 'Incomplete'),
        ('COMPLETED', 'Completed'),
    )
    name = models.TextField(_('Hospital Name'))
    address1 = models.TextField(_("Address(required)"))
    landmark = models.CharField(max_length=200, blank=True)
    district = models.CharField(max_length=200, blank=True)

    city = models.ForeignKey(City, null=True, blank=True)
    state = models.ForeignKey(State, null=True, blank=True)
    pincode = models.CharField(max_length=200)

    std_code = models.CharField(
        _("STD Code (required)"), max_length=10, blank=True, null=True)
    phone1 = models.CharField(_("Phone-1 (required)"),
                              max_length=255, blank=True, null=True)
    phone2 = models.CharField(max_length=255, blank=True, null=True)
    fax1 = models.CharField(max_length=255, blank=True, null=True)
    fax2 = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(blank=True)
    url = models.CharField(max_length=200, blank=True)

    lat = models.FloatField(blank=True, null=True)
    lng = models.FloatField(blank=True, null=True)
    googledata = models.TextField(blank=True)

    no_of_beds = models.IntegerField(blank=True, null=True)
    no_of_major_ots = models.IntegerField(null=True, blank=True)
    owned_ambulances = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")), blank=True, null=True)
    registered_blood_bank = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")), blank=True, null=True)
    remarks = models.TextField(blank=True)
    disposition = models.CharField(max_length=100, choices=DISPOSITION_CHOICES)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, blank=True, null=True)
    insurer = models.ManyToManyField(Insurer, blank=True)
    is_deleted = models.BooleanField(default=False)
    duplicate = models.ForeignKey('self', null=True, blank=True)

#    name_of_chief_doctor = models.CharField(max_length=200, blank=True)
#    qualification_of_chief_doctor = models.CharField(max_length=200, blank=True)
#    contact_number = models.IntegerField(null=True, blank=True)
#    mci_registration = models.CharField(_('MCI Registration Number of Chief Doctor'),max_length=200,  blank=True)
#    local_authority = models.CharField(_('Local authority with whom hospital is registered'),max_length=200,  blank=True)
#
#    registration_number = models.CharField(max_length=200, blank=True)
#    tpa_contact = models.CharField(_('Key contact for TPA / Insurance - Name'),max_length=200,  blank=True)
#    tpa_contact_mobile = models.IntegerField(_('Key contact for insurance - Mobile phone number'), blank=True, null=True)
#    location_classification = models.CharField(max_length=200, blank=True)
#
#    category = models.CharField(max_length=200, blank=True)

#    no_of_major_ots = models.IntegerField(null=True, blank=True)
#    no_of_qualified_doctors = models.IntegerField(blank=True, null=True)
#    physician_available = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    no_of_qualified_nurses = models.IntegerField(blank=True, null=True)
#    full_equipped = models.CharField(_('Fully equipped ICU/ + ICCU/+ NICU Beds'),max_length=200,  blank=True)
#    no_of_minor_ots = models.IntegerField(null=True, blank=True)
#
#    haematology = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    biochemistry = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    microbiology = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    pathology = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    serology = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    histopathology = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    endocrine_lab = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    dialysis_unit = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    lithotripsy_nuclear_medicine = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    x_ray = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    c_arm = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#
#    ultra_sound_color_doppler = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    ct_scan = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    mri = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    is_ambulances_owned = models.NullBooleanField(_('Ambulances are owned of outsourced'), choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    casualty = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#
#    rta = models.NullBooleanField(_('(Road Traffic Accident) cases treatment'), choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    cardiac_emergency_cases_treatment = models.NullBooleanField(_('(Road Traffic Accident) cases treatment'), choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#
#    hospital_infection_control_measures = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    hopsital_waste_management_process_available = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    power_backup_available = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    his_available = models.NullBooleanField(_('Hospital Inforamtion System'), choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    internet_connectivity_available = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#    computerized_billing = models.NullBooleanField(choices=((False,"No"), (True,"Yes")),blank=True, null=True)
#
#    Accreditations = models.TextField(blank=True, null=True)
