from django.conf.urls import url

from cf_fhurl import fhurl
from activity.forms import HealthCareCenterForm, HealthCareCenterBasicForm

from utils import profile_type_only

from . import views as aviews


urlpatterns = [
    url(r'^unsubscribe/(?P<lead_id>[\w-]+)/$', aviews.lead_unsubscribe, name='lead_unsubscribe'),

    url(r'^auto-load-hospital/$', aviews.auto_load_hospital, name='auto_load_hospital'),
    url(r'^ajax-irda-hospital/$', aviews.ajax_irda_hospital, name='ajax_irda_hospital'),
    url(r'^ajax-irda-hospital-report-error/$', aviews.ajax_irda_hospital_report_error,
        name='ajax_irda_hospital_report_error'),

    # url(r'^hospital-admin/(?P<ptype>\w+)/(?P<place>[ &\w-]+)/$', aviews.hospital_admin, name='hospital_admin'),
    url(r'^hospital-admin/insurer/(?P<insurer_id>\d+)/(?P<ptype>\w+)/(?P<place>[ &\w-]+)/$',
        aviews.hospital_admin, name='hospital_admin'),

    url(r'^hospital-dashboard/$', aviews.hospital_dashboard, name='hospital_dashboard'),
    url(r'^hospital-report1/$', aviews.hospital_report1, name='hospital_report1'),
    url(r'^hospital-admin/get-city-autocomplete/$', aviews.get_city_autocomplete, name='get_city_autocomplete'),

    url(r'^hospital-admin/sanitize-dashboard/$', aviews.sanitize_dashboard, name='sanitize_dashboard'),
    url(r'^hospital-admin/manual-sanitize/city/(?P<pid>[\d]+)/$',
        aviews.manual_sanitize_city, name='manual_sanitize_city'),
    url(r'^hospital-admin/smart-sanitize/city/(?P<pid>[\d]+)/$',
        aviews.smart_sanitize_city, name='smart_sanitize_city'),
    # url(r'^hospital-admin/sanitize/hcc/(?P<id>[\d]+)/$',
    #     aviews.sanitize_healthcarecenter, name='sanitize_healthcarecenter'),

    url(r'^hospital-admin/get-matching-data/$', aviews.get_matching_data, name='get_matching_data'),
    url(r'^hospital-admin/get-hospital-suggest/$', aviews.get_hospital_suggest, name='get_hospital_suggest'),

    url(r'^hospital-admin/mark-duplicate/(?P<delete_id>[\d]+)/(?P<to_id>[\d]+)/$',
        aviews.mark_duplicate, name='mark_duplicate'),
    url(r'^hospital-admin/duplicate-detach/(?P<deleted_id>[\d]+)/(?P<to_id>[\d]+)/$',
        aviews.duplicate_detach, name='duplicate_detach'),

    url(r'^hospital-admin/detach_hcc_from_data/(?P<hcc_id>[\d]+)/(?P<data_id>[\d]+)/$',
        aviews.detach_hcc_from_data, name='detach_hcc_from_data'),

    url(r'^hospital-admin/insurer/(?P<insurer_id>\d+)/get-matching-hospitals/(?P<data_id>\d+)/$',
        aviews.get_matching_hospitals, name='get_matching_hospitals'),
    url(r'^hospital-admin/insurer-merge/(?P<insurer_id>\d+)/(?P<hcc_id>[\d]+)/(?P<data_id>[\d]+)/$',
        aviews.insurer_merge, name='insurer_merge'),

    fhurl(r'^hcc-form/(?P<insurer_id>[\d-]+)/$', HealthCareCenterForm,
          decorator=profile_type_only('HOSPITAL_DATA_ENTRY'), name='ajax_hcc_form',
          template="activity/snippets/hcc_form.html", json=True),
    fhurl(r'^hcc-form/(?P<insurer_id>[\d-]+)/(?P<data_id>[\d-]+)/$', HealthCareCenterForm,
          decorator=profile_type_only('HOSPITAL_DATA_ENTRY'), name='ajax_hcc_form',
          template="activity/snippets/hcc_form.html", json=True),

    fhurl(r'^hcc-basic-form/(?P<data_id>[\d-]+)/$', HealthCareCenterBasicForm,
          decorator=profile_type_only('HOSPITAL_DATA_ENTRY'), name='ajax_hcc_basic_form',
          template="activity/snippets/hcc_basic_form.html", json=True),

    url(r'^hospital-admin/merge_hcc/(?P<main_id>[\d]+)/(?P<hcc_id>[\d]+)/$', aviews.merge_hcc, name='merge_hcc'),
    url(r'^hospital-admin/unmerge_hcc/(?P<main_id>[\d]+)/(?P<hcc_id>[\d]+)/$', aviews.unmerge_hcc, name='unmerge_hcc'),
    url(r'delete-data/(?P<data_id>\d+)/$', aviews.delete_data, name='delete_data'),
    url(r'add-data/insurer/(?P<insurer_id>\d+)/state/(?P<state_id>\w+)/$', aviews.add_data, name='add_data'),
    url(r'hospital-list-upload/(?P<insurer_id>\d+)/$', aviews.hospital_list_save, name='hospital_list_save'),
    url(r'delete-hospitals/(?P<insurer_id>\d+)/$', aviews.delete_hospitals, name='delete_hospitals'),
]
