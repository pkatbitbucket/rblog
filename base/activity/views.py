import os
import json
import datetime
from collections import Counter
from django.core.files.storage import default_storage
import utils

from django.http import HttpResponse
from django.template import RequestContext
from django.conf import settings

from django.shortcuts import render_to_response, get_object_or_404, render
from django.db.models import Q
from django.db.models import Count

from django.utils.html import linebreaks
from django.views.decorators.cache import never_cache
from excelib import render_excel_to_file

from activity.models import LeadData, Data, HealthCareCenter, IRDAList
from .forms import DataAddForm, XlsDataUpload, HospitalXlsValidate
from wiki.models import Insurer, City, State
from .autils import save_hospital_data
import health_product.queries as hqueries
import health_product.health_utils as hutils
import putils
import xlrd


def lead_unsubscribe(request, lead_id):
    lead = get_object_or_404(LeadData, lead_id=lead_id)
    # Directly unsubscribe and then ask to re-register as customer
    lead.unsubscribed = True
    lead.save()
    return render_to_response("activity/unsubscribe.html", {
    }, context_instance=RequestContext(request))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def insurer_merge(request, insurer_id, hcc_id, data_id):
    data = Data.objects.get(id=data_id)
    insurer = Insurer.objects.get(id=insurer_id)
    hcc = HealthCareCenter.objects.get(id=hcc_id)
    data.health_care_center = hcc
    data.save()
    hcc.insurer.add(insurer)
    return HttpResponse(json.dumps({'success': True, 'data_id': data.id, 'status': hcc.disposition}))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def detach_hcc_from_data(request, hcc_id, data_id):
    data = get_object_or_404(Data, pk=data_id)
    hcc = get_object_or_404(HealthCareCenter, pk=hcc_id)
    data.health_care_center = None
    data.save()
    return HttpResponse(json.dumps({'success': True, 'data_id': data.id}))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def duplicate_detach(request, deleted_id, to_id):
    deleted_data = get_object_or_404(Data, pk=deleted_id)
    to_data = get_object_or_404(Data, pk=to_id)
    deleted_data.is_deleted = False
    deleted_data.duplicate = None
    deleted_data.save()
    return HttpResponse(json.dumps({'success': True}))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def mark_duplicate(request, delete_id, to_id):
    delete_data = get_object_or_404(Data, pk=delete_id)
    to_data = get_object_or_404(Data, pk=to_id)
    delete_data.is_deleted = True
    delete_data.duplicate = to_data
    delete_data.save()
    return HttpResponse(json.dumps({'success': True}))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def get_hospital_suggest(request):
    name = request.GET['term'].lower().strip()
    datum = HealthCareCenter.objects.filter(~Q(disposition="DUPLICATE")).filter(
        Q(name__icontains=name) | Q(address1__icontains=name)).distinct()
    json_data = [{
        'id': d.id,
        'text': ",".join([d.name, d.address1, d.city.name, d.city.state.name, d.pincode]),
    } for d in datum]
    return HttpResponse(json.dumps(json_data))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def get_matching_data(request):
    name = request.GET['term'].lower().strip()
    datum = Data.objects.filter(
        raw__icontains=name, is_deleted=False, health_care_center__isnull=True).distinct()
    json_data = [{
        'id': d.id,
        'text': linebreaks(d.format()),
        'hcc': d.health_care_center,
    } for d in datum]
    return HttpResponse(json.dumps(json_data))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def get_matching_hospitals(request, insurer_id, data_id):
    d = get_object_or_404(Data, id=data_id)
    city_match = City.objects.filter(name=d.city.lower())
    if city_match:
        hcc_universe = HealthCareCenter.objects.filter(
            ~Q(disposition="DUPLICATE")).filter(city=city_match[0])
    else:
        hcc_universe = HealthCareCenter.objects.filter(
            ~Q(disposition="DUPLICATE")).filter(city__state=State.objects.get(name__iexact=d.state))

    name = d.name.lower()
    name_parts = hutils.normalize_hospital_name(name).split(" ")
    print "NAME PARTS", name_parts
    matching_list = []
    for np in name_parts:
        np = np.strip()
        if len(np) == 1:
            matching_list.extend(
                list(hcc_universe.filter(name__icontains=" %s " % np)))
        else:
            matching_list.extend(list(hcc_universe.filter(name__icontains=np)))
    matching_list = Counter(matching_list).most_common()[:4]
    json_data = [{
        'id': hcc.id,
        'name': hcc.name,
        'city': hcc.city.name,
        'state': hcc.city.state.name,
        'address': hcc.address1,
        'pincode': hcc.pincode,
        'data_ids': [hcd.id for hcd in hcc.data_set.all()]
    } for hcc, cnt in matching_list]

    if d.health_care_center:
        hcc = d.health_care_center
        hcc_data = {
            'id': hcc.id,
            'name': hcc.name,
            'city': hcc.city.name,
            'state': hcc.city.state.name,
            'address': hcc.address1,
            'pincode': hcc.pincode,
        }
    else:
        hcc_data = None

    return HttpResponse(json.dumps({
        'data_id': data_id,
        'success': True,
        'matches': json_data,
        'raw': d.raw,
        'hcc': hcc_data
    }))


@never_cache
def get_city_autocomplete(request):
    query = Q(name__istartswith=request.GET['term'].lower())
    if request.GET.get('state_id'):
        query = query & Q(state_id=request.GET['state_id'])
    city_list = [{
        'id': city.id,
        'label': "%s (%s)" % (city.name, city.state.name),
        'city': city.name,
        'state': city.state.name,
    } for city in City.objects.filter(query)]
    return HttpResponse(json.dumps(city_list))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def hospital_report1(request):
    insurer_name_dict = dict([(ins.id, ins.title)
                              for ins in Insurer.objects.all()])
    xldata2 = []
    xldata2_heading = ["Hospital ID", "name", "address1", "landmark", "district", "city", "state", "pincode", "std code", "phone1", "phone2", "fax1", "fax2", "email",
                       "url", "lat", "lng", "url", "no of beds", "no of major ots", "owned ambulances", "registered blood bank", "remarks", "disposition", "verification level", "Data ID"]
    for data in HealthCareCenter.objects.all():
        insurer_name = ""
        ins_list = data.insurer.all()
        if ins_list:
            insurer_name = ", ".join([ins.title for ins in data.insurer.all()])
        data_ids = ""
        data_list = data.data_set.all()
        if data_list:
            data_ids = ",".join([str(d.id) for d in data.data_set.all()])
        row = [str(data.id), data.name, data.address1, data.landmark, data.district, data.city.name, data.city.state.name, data.pincode, data.std_code, data.phone1, data.phone2, data.fax1, data.fax2, data.email, data.url,
               data.lat, data.lng, data.url, data.no_of_beds, data.no_of_major_ots, data.owned_ambulances, data.registered_blood_bank, data.remarks, data.disposition, data_ids, insurer_name]
        xldata2.append(row)

    xldata1 = []
    xldata1_heading = ["Data ID", "Insurer", "name", "address", "city", "state", "pin",
                       "std", "phone", "email", "latlng", "fax", "url", "Duplicate Data Id", "Hospital ID"]
    for data in Data.objects.all():
        row = [str(data.id), insurer_name_dict.get(data.by_insurer_id, ''), data.name, data.address, data.city, data.state, data.pin, data.std, data.phone,
               data.email, data.latlng, data.fax, data.url, str(getattr(data.duplicate, 'id', '')), str(getattr(data.health_care_center, 'id', ''))]
        xldata1.append(row)

    [xldata2.append([""] * 10) for i in range(10)]
    xldata2.append(xldata1_heading)
    xldata2.extend(xldata1)

    filename = "hospital_data_dump_%s.xls" % (
        datetime.datetime.now().strftime("%d%m%Y%H%M%S"))
    with default_storage.open(os.path.join('downloads', filename), 'wb') as f:
        render_excel_to_file(f, xldata2_heading, xldata2)
    return HttpResponse(default_storage.url(f.name))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def auto_load_hospital(request):
    return render_to_response("activity/auto_load_hospitals.html", {
    }, context_instance=RequestContext(request))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def ajax_irda_hospital_report_error(request):
    if request.POST:
        h = IRDAList.objects.get(id=request.POST['id'])
        h.status = request.POST['status']
        h.query = request.POST['query']
        h.is_completed = False
        h.is_processed = True
        h.save()
        return HttpResponse(json.dumps({'id': h.id}))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def ajax_irda_hospital(request):
    if request.POST:
        h = IRDAList.objects.get(id=request.POST['id'])
        h.googledata = request.POST['googledata']
        h.query = request.POST['query']
        h.is_completed = True
        h.is_processed = True
        h.save()
        return HttpResponse(json.dumps({'id': h.id}))
    else:
        h = IRDAList.objects.filter(is_processed=False)[0]
        return HttpResponse(json.dumps({
            'raw': h.raw,
            'id': h.id,
            'place': "%s, %s" % (h.city, h.state),
            'name': hutils.normalize_hospital_name(h.name)
        }))

ACTIVITY_CITY_LIST = [
    ["Mumbai-Thane-NaviMumbai", "^mum|^navi|^thane"],
    ["Delhi-Gurgaon-Noida-Ghaziabad-Faridabad",
        "^del|^new de|^gurg|^noida|^ghazia|^farida|^dwarka|^preet|^west del|^nanglo"],
    ["Chennai", "^chenn"],
    ["Kolkata", "^kolk"],
    ["Bangalore", "^bangal"],
    ["Pune", "^pune"],
    ["Hyderabad", "^hyd"],
    ["Ahmedabad", "^ahmada|^ahmeda"],
    ["Jaipur", "^jaip"],
    ["Bhopal", "^bhop"],
    ["Nagpur", "^nagp"],
    ["Indore", "^ind"],
    ["Chandigarh", "^chandig"],
    ["Patna", "^patn"],
]


def access_supplier(user):
    cities = []
    states = []
    if user.organization in ['COVERFOX', 'ENSER']:
        cities.extend(["Chennai", "Bangalore", "Hyderabad",
                       "Bhopal", "Indore", "Jaipur", "Patna"])
        lifenet_states = ['Andaman and Nicobar Islands', 'Andhra Pradesh', 'Arunachal Pradesh', 'Assam', 'Dadra & Nagar Haveli', 'Daman & Diu',
                          'Karnataka', 'Kerala', 'Lakshadweep', 'Manipur', 'Meghalaya', 'Mizoram', 'Nagaland', 'Orissa', 'Pondicherry', 'Tamilnadu', 'West Bengal']
        states.extend(
            [state.name for state in State.objects.filter(name__in=lifenet_states)])
    if user.organization in ['COVERFOX', 'ENSER']:
        cities.extend(["Mumbai-Thane-NaviMumbai", "Delhi-Gurgaon-Noida-Ghaziabad-Faridabad",
                       "Kolkata", "Pune", "Ahmedabad", "Nagpur", "Chandigarh"])
        enser_states = ['Bihar', 'Chandigarh', 'Chhattisgarh', 'Delhi', 'Goa', 'Gujarat', 'Haryana', 'Himachal Pradesh', 'Jammu & Kashmir',
                        'Jharkhand', 'Madhya Pradesh', 'Maharashtra', 'Punjab', 'Rajasthan', 'Sikkim', 'Tripura', 'Uttar Pradesh', 'Uttaranchal']
        states.extend(
            [state.name for state in State.objects.filter(name__in=enser_states)])
    return cities, states


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def hospital_dashboard(request):
    cities, states = access_supplier(request.user)
    return render_to_response("activity/hospital_dashboard.html", {
        'cities': cities,
        'states': states,
        'insurers': Insurer.objects.all(),
    }, context_instance=RequestContext(request))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def hospital_admin(request, insurer_id, ptype, place):
    form_add_raw_data = DataAddForm(prefix='dataadd')
    insurer = Insurer.objects.get(id=insurer_id)
    data_list = []
    cities, states = access_supplier(request.user)
    if ptype == 'city' and place in cities:
        city_regex = dict(ACTIVITY_CITY_LIST)[place]
        city_obj = City.objects.filter(name__iregex=city_regex)[0]
        state_obj = city_obj.state
        data_list = Data.objects.filter(
            is_deleted=False, city__iregex=city_regex, by_insurer=insurer).order_by('name')

    if ptype == 'state' and place in states:
        q = Q()
        for city, cregex in dict(ACTIVITY_CITY_LIST).items():
            q = q & ~Q(city__iregex=cregex)
        state_obj = State.objects.get(name=place)
        state_regex = State.objects.get(name=place).name.lower()[:6]
        data_list = Data.objects.filter(
            Q(is_deleted=False, state__iregex=state_regex, by_insurer=insurer) & q).order_by('name')

    return render_to_response("activity/hospital_admin.html", {
        'data_list': data_list,
        'insurer': insurer,
        'form_add_raw_data': form_add_raw_data,
        'ptype': ptype,
        'place': place,
        'state_obj': state_obj
    }, context_instance=RequestContext(request))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
def sanitize_dashboard(request):
    data = City.objects.annotate(count=Count('healthcarecenter')).order_by(
        '-count').filter(count__gt=0).values('count', 'name', 'id')
    return render_to_response("activity/sanitize_dashboard.html", {
        'data': data,
    }, context_instance=RequestContext(request))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
def manual_sanitize_city(request, pid):
    city = City.objects.get(id=pid)
    #hcc = HealthCareCenter.objects.filter(city=city).order_by('name')

    # sort in python using normalize
    import operator
    hcc_list = [(hutils.normalize_hospital_name(h.name), h)
                for h in HealthCareCenter.objects.filter(city=city)]
    hcc_list.sort(key=operator.itemgetter(0))

    return render_to_response("activity/manual_sanitize_city.html", {
        'hcc': [h[1] for h in hcc_list],
    }, context_instance=RequestContext(request))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
def smart_sanitize_city(request, pid):
    city = City.objects.get(id=pid)
    hcc_list = HealthCareCenter.objects.filter(city=city).order_by('id')
    matching_list = {}
    matched = {}

    index = 0
    for d in hcc_list:
        if matched.get(d):
            continue
        print "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        print "Processing --- ", d.name
        matching_list[d] = []
        name = d.name.lower()
        name_parts = hutils.normalize_hospital_name(name).split(" ")
        print "NAME PARTS", name_parts
        mlist = []
        for np in name_parts:
            np = np.strip()
            if len(np) == 1:
                mlist.extend([h for h in hcc_list[index:] if h.id != d.id and h.pincode ==
                              d.pincode and " %s " % np in hutils.normalize_hospital_name(h.name)])
            else:
                mlist.extend([h for h in hcc_list[index:] if h.id != d.id and h.pincode ==
                              d.pincode and np in hutils.normalize_hospital_name(h.name)])
        mlist_top = Counter(mlist).most_common()[:5]
        for l in mlist_top:
            matched[l[0]] = True
            print "Matching ", l[0].name

        matching_list[d] = mlist_top
        index += 1

    cluster_hcc = []
    for h, d in matching_list.items():
        if d:
            t = []
            t.append(h)
            t.extend([i[0] for i in d])
            cluster_hcc.append(t)

    return render_to_response("activity/smart_sanitize_city.html", {
        'cluster_hcc': cluster_hcc,
    }, context_instance=RequestContext(request))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def merge_hcc(request, main_id, hcc_id):
    if main_id == hcc_id:
        return HttpResponse(json.dumps({'success': True, 'hcc_id': hcc_id}))

    main_hcc = get_object_or_404(HealthCareCenter, pk=main_id)
    hcc = HealthCareCenter.objects.get(id=hcc_id)

    hcc.is_deleted = True
    hcc.duplicate = main_hcc
    hcc.save()
    return HttpResponse(json.dumps({'success': True, 'hcc_id': main_hcc.id, 'duplicate_id': hcc.id}))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def unmerge_hcc(request, main_id, hcc_id):
    if main_id == hcc_id:
        return HttpResponse(json.dumps({'success': True, 'hcc_id': hcc.id}))

    main_hcc = get_object_or_404(HealthCareCenter, pk=main_id)
    hcc = HealthCareCenter.objects.get(id=hcc_id)

    hcc.is_deleted = False
    hcc.duplicate = None
    hcc.save()
    return HttpResponse(json.dumps({'success': True, 'hcc_id': hcc.id}))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def delete_data(request, data_id):
    obj = get_object_or_404(Data, pk=data_id)
    obj.delete()
    return HttpResponse(json.dumps({'success': True, 'id': data_id}))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def add_data(request, insurer_id, state_id):
    if request.method == 'POST':
        form = DataAddForm(request.POST, prefix='dataadd')
        if form.is_valid():
            insurer_obj = Insurer.objects.get(id=insurer_id)
            state_obj = State.objects.get(id=state_id)
            data = Data(
                name=form.cleaned_data['name'],
                address=form.cleaned_data['address'],
                city=form.cleaned_data['city'],
                state=state_obj,
                pin=form.cleaned_data['pin'],
                std=form.cleaned_data['std'],
                phone=form.cleaned_data['phone'],
                email=form.cleaned_data['email'],
                url=form.cleaned_data['url'],
                by_insurer=insurer_obj,
                )
            data.save()
            return HttpResponse(json.dumps({'success': 'true'}))
        else:
            return HttpResponse(json.dumps({'success': 'false', 'errors': form.errors}))
    return HttpResponse(json.dumps({'success': 'false'}))


@utils.profile_type_only('HOSPITAL_DATA_ENTRY')
@never_cache
def hospital_list_save(request, insurer_id):
    insurer_obj = get_object_or_404(Insurer, pk=insurer_id)
    if request.POST:
        form = XlsDataUpload(request.POST, request.FILES)
        cleaned_xl_data = []
        if form.is_valid():
            _, ext = os.path.splitext(request.FILES['xls_file'].name)
            if ext not in ['.xls', '.xlsx']:
                return HttpResponse(json.dumps({'success': 'false', 'errors': 'file type not supported'}))
            list_of_words = form.cleaned_data['words'].split(",")
            book = xlrd.open_workbook(file_contents=request.FILES['xls_file'].read())
            first_sheet = book.sheet_by_index(0)
            for i in range(1, first_sheet.nrows):
                if not first_sheet.cell(i, 0).value:
                    return HttpResponse(json.dumps({'success': 'false',
                                                    'errors': {'error': 'first value in row is blank at line no. %s' % i}}))
                data_dict = {}
                for index, x in enumerate(list_of_words):
                    data_dict[x] = first_sheet.cell(i, index).value
                data_dict['lineno'] = i
                hspt_frm = HospitalXlsValidate(data_dict)
                if hspt_frm.is_valid():
                    cleaned_xl_data.append(hspt_frm.cleaned_data)
                else:
                    return HttpResponse(json.dumps({'success': 'false', 'errors': hspt_frm.errors}))
            save_hospital_data.delay(cleaned_xl_data, insurer_obj)
            return HttpResponse(json.dumps({'success': 'true'}))
        else:
            return HttpResponse(json.dumps({'success': 'false', 'errors': form.errors}))
    else:
        form = XlsDataUpload()
        return render(request, 'activity/validate_xls.html', {
            "form": form,
            "states": State.objects.all(),
            "insurer_id": insurer_id},
        )


@utils.profile_type_only('ADMIN')
@never_cache
def delete_hospitals(request, insurer_id):
    insurer_obj = get_object_or_404(Insurer, pk=insurer_id)
    Data.objects.filter(by_insurer=insurer_obj).delete()
    return HttpResponse('Deletion succeed')
