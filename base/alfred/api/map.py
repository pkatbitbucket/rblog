prod_trans_form_dict = {
    'product_type': 'product_type',
    'first_name': 'proposer_first_name',
    'last_name': 'proposer_last_name',
    'email': 'email',
    'mobile': 'mobile',
}

required_data_dict = {
    'policy_no': 'policy_no',
    'policy_status': 'policy_status',
    'transaction_id': 'transaction_id',
}

prod_trans_form = {
    'is_medical_required': 'is_medical_required',
    'business_type': 'business_type',
    'first_name': 'proposer_first_name',
    'end_date': 'end_date',
    'email': 'email',
    'insurance_company': 'insurance_company',
    'insurance_product': 'insurance_product',
    'insured_name': 'insured_name',
    'last_name': 'proposer_last_name',
    'mobile': 'mobile',
    'payment_mode': 'payment_mode',
    'policy_no': 'policy_no',
    'policy_status': 'policy_status',
    'product_type': 'product_type',
    'proposal_form_mode': 'proposal_form_mode',
    'premium_amount': 'premium_amount',
    'transaction_id': 'transaction_id',
    'data': 'data'
}
