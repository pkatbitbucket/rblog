from faker import Factory
from faker import Faker
from faker.providers import BaseProvider
import fake_data_mapping
from alfred.models import Policy
from core.models import Person, Email, Phone, User
from leads.models import Transaction


class CustomProvider(BaseProvider):
    fake = Factory.create()

    def mobile(self):
        return self.fake.random_number(10)

    def policy_no(self):
        return self.fake.random_number(5)

    def formatted_date(self, format_code='%d/%m/%Y'):
        return self.fake.date_time_this_year().strftime(format_code)

    def request_type(self):
        return fake_data_mapping.REQUIRED_TYPE_CHOICES[
            self.fake.random_number(1) % len(fake_data_mapping.REQUIRED_TYPE_CHOICES)]

    def sub_request_types(self):
        return [fake_data_mapping.SUB_REQUEST_TYPES_CHOICES[
            self.fake.random_number(1) % len(fake_data_mapping.SUB_REQUEST_TYPES_CHOICES)]]

    def proposal_form_mode(self):
        return fake_data_mapping.PROPOSAL_FORM_MODE_CHOICES[
            self.fake.random_number(1) % len(fake_data_mapping.PROPOSAL_FORM_MODE_CHOICES)]

    def payment_mode(self):
        return fake_data_mapping.PAYMENT_MODE_CHOICES[
            self.fake.random_number(1) % len(fake_data_mapping.PAYMENT_MODE_CHOICES)]

    def policy_status(self):
        return fake_data_mapping.POLICY_STATUS_CHOICES[
            self.fake.random_number(1) % len(fake_data_mapping.POLICY_STATUS_CHOICES)]

    def business_type(self):
        return fake_data_mapping.BUSINESS_TYPE_CHOICES[
            self.fake.random_number(1) % len(fake_data_mapping.BUSINESS_TYPE_CHOICES)]

    def insurance_company(self):
        return fake_data_mapping.INSURANCE_COMPANY_CHOICES[
            self.fake.random_number(1) % len(fake_data_mapping.INSURANCE_COMPANY_CHOICES)]

    def insurance_product(self):
        return fake_data_mapping.INSURANCE_PRODUCT_CHOICES[
            self.fake.random_number(1) % len(fake_data_mapping.INSURANCE_PRODUCT_CHOICES)]

    def insured_name(self):
        return fake_data_mapping.INSURED_NAME_CHOICES[
            self.fake.random_number(1) % len(fake_data_mapping.INSURED_NAME_CHOICES)]

    def product_type(self):
        return fake_data_mapping.PRODUCT_TYPE_CHOICES[
            self.fake.random_number(1) % len(fake_data_mapping.PRODUCT_TYPE_CHOICES)]


def get_fake_data(data_type):
    data = {key: getattr(faker, value)() for key, value in fake_data_mapping.data_mapping.items()}
    special_rules_map = fake_data_mapping.flow_mapping[data_type]
    data.update({key: value[faker.random_number(1) % len(value)] for key, value in special_rules_map.items()})
    if data_type in ["cancellation_flow", "refund_flow", "claim_flow", "endorsement_flow", "approved_policy_flow"]:
        person = Person(first_name=data['proposer_first_name'], last_name=data['proposer_last_name'])
        person.save(user=User.objects.all()[0])
        email = Email(person=person, address=data['email'], is_primary=True)
        email.save(user=User.objects.all()[0])
        phone = Phone(person=person, number=data['mobile'], is_primary=True)
        phone.save(user=User.objects.all()[0])
        transaction = Transaction(transaction_id=data.get('transaction_id'))
        transaction.save(None)
        policy = Policy.objects.create(policy_no=data.get('policy_no'), data=data)
        policy.save()
        policy.contacts.add(person)
        transaction.policies.add(policy)
    return data


faker = Faker()
faker.add_provider(CustomProvider)
