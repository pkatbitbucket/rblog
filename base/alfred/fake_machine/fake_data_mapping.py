from alfred.models import TicketCategory, SubRequestType
data_mapping = {
    "request_type": "request_type",
    "sub_request_types": "sub_request_types",
    "email": "free_email",
    "premium_amount": "random_number",
    "mobile": "mobile",
    "proposer_first_name": "first_name",
    "proposer_last_name": "last_name",
    "insurance_company": "insurance_company",
    "insurance_product": "insurance_product",
    "payment_date": "formatted_date",
    "proposal_form_mode": "proposal_form_mode",
    "payment_mode": "payment_mode",
    "end_date": "formatted_date",
    "business_type": "business_type",
    "policy_status": "policy_status",
    "insured_name": "insured_name",
    "is_medical_required": "boolean",
    "instant_policy": "boolean",
    "transaction_id": "uuid4",
    "product_type": "product_type",
    "policy_no": "policy_no",
}

flow_mapping = {
    "policy_approve_flow": {
        "request_type": TicketCategory.objects.filter(name__in=["POLICY_APPROVAL"]).values_list('id', flat=True),
        "instant_policy": [False]
    },
    "endorsement_flow": {
        'request_type': TicketCategory.objects.filter(name__in=['ENDORSEMENT']).values_list('id', flat=True),
        'product_type': ['motor', 'health']
    },
    "cancellation_flow": {
        'request_type': TicketCategory.objects.filter(name__in=['CANCEL']).values_list('id', flat=True),
        'product_type': ['motor', 'health']
    },
    "refund_flow": {
        'request_type': TicketCategory.objects.filter(name__in=['REFUND']).values_list('id', flat=True),
        'product_type': ['motor', 'health']
    },
    "claim_flow": {
        'request_type': TicketCategory.objects.filter(name__in=['CLAIM']).values_list('id', flat=True),
        'product_type': ['motor', 'health']
    },
    "approved_policy_flow": {
        'request_type': TicketCategory.objects.filter(name__in=['APPROVED']).values_list('id', flat=True),
        'product_type': ['motor', 'health']
    },
    "offline_form": {'instant_policy': [False]}
}

INSURANCE_COMPANY_MAP = {
    "Apollo Munich Insurance Company": ["Easy Health - Premium",
                                        "Easy Health - Exclusive",
                                        "Easy Health - Standard",
                                        "Optima Restore"],
    "Bajaj Allianz": ["Health Guard"],
    "Bharti Axa": ["Smart Health Insurance Optimum", "Smart Health Insurance"],
    "Cholamandalam MS": ["Healthline - Advanced",
                         "Healthline - Superior",
                         "Healthline - Standard"],
    "HDFC Ergo": ["Health Suraksha"],
    "IFFCO-TOKIO": ["Swasthya Kavach Wider Plan",
                    "Swasthya Kavach",
                    "Medishield"],
    "L&T General Insurance": ["Medisure Super Top Up",
                              "Medisure Prime",
                              "Medisure Classic - No Sub-limits & CI",
                              "Medisure Classic with CI Benefit",
                              "Medisure Classic with No Sub-limits",
                              "Medisure Classic"],
    "Reliance Health Insurance": ["Healthwise - Standard"],
    "Religare": ["Care - No Claim Bonus Super", "Care Health Insurance", "Health Assure", "Enhance Plan"],
    "Star Health Insurance": ["Family Health Optima",
                              "Senior Citizens Red Carpet",
                              "Medi Classic",
                              "Star Accident Care",
                              "Star Comprehensive"],
    "Tata AIG": ["MediPrime"],
    "The Oriental Insurance Company Ltd": ["Happy Family Floater - Gold",
                                           "Happy Family Floater - Silver"],
    "Max Bupa": ["Health companion",
                 "Heartbeat"],
}

INSURANCE_COMPANY_CHOICES = [k for k in INSURANCE_COMPANY_MAP]
INSURANCE_PRODUCT_CHOICES = [i for k in INSURANCE_COMPANY_MAP for i in INSURANCE_COMPANY_MAP[k]]

INSURED_NAME_CHOICES = ["SELF", "KIDS", "SELF_KIDS", "SELF_SPOUSE", "SELF_SPOUSE_KIDS", "MOTHER", "FATHER",
                        "MOTHER_FATHER", "SELF_SPOUSE_KIDS_MOTHER_FATHER", "SPOUSE", "SPOUSE_KIDS",
                        "SELF_SPOUSE_MOTHER_FATHER", "SELF_MOTHER_FATHER"]

PROPOSAL_FORM_MODE_CHOICES = ["ONLINE", "OFFLINE"]

PAYMENT_MODE_CHOICES = ["ONLINE", "OFFLINE"]

BUSINESS_TYPE_CHOICES = ["FRESH", "RENEWAL", "PORTING"]

POLICY_STATUS_CHOICES = ["ISSUED", "UNDERWRITING"]

REQUIRED_TYPE_CHOICES = TicketCategory.objects.values_list('id', flat=True)

SUB_REQUEST_TYPES_CHOICES = SubRequestType.objects.values_list('id', flat=True)

PRODUCT_TYPE_CHOICES = ["health", "motor"]
