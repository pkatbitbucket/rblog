import json
import datetime

from django import forms
from django.shortcuts import get_object_or_404
from django.db.models import Q
from django.conf import settings

import utils as alfred_utils
from core import utils as core_utils
from cfutils.forms import fhurl
from core import models as core_models
from leads import models as leads_models
import models as alfred_models
from statemachine import querymaker as sm_qmaker
import uuid
# TODO: nudgespot is supposed to be in cfutils and
# and only the alfred related part of sms templates
# and events needs to be inside alfred


# Implement update function too for this form
class PolicyForm(fhurl.RequestForm):
    policy_no = forms.CharField()

    def init(self, policy_id=None):
        if policy_id:
            self.policy_id = policy_id

    def save(self):
        policy = alfred_models.Policy.objects.filter(policy_no__endswith=self.cleaned_data['policy_no'])
        if policy:
            policy = policy[0]
        else:
            policy = alfred_models.Policy(
                policy_no='MANUAL-%s' % self.cleaned_data['policy_no'],
                data={'created_by': self.request.user.username}
            )
            policy.save()
            tr = leads_models.Transaction(transaction_id=uuid.uuid1().hex)
            tr.save(None)
            policy.transactions.add(tr)
        return {'policy_id': policy.id, 'success': True}


class PolicySearchForm(fhurl.RequestForm):
    customer_email = forms.EmailField(required=False,
                                      widget=forms.TextInput(attrs={'class': 'input-sm form-control fg-input'}))
    customer_mobile = forms.CharField(max_length=20, required=False,
                                      widget=forms.TextInput(attrs={'class': 'input-sm form-control fg-input'}))
    policy_no = forms.CharField(max_length=100, required=False,
                                widget=forms.TextInput(attrs={'class': 'input-sm form-control fg-input'}))

    def save(self):
        if settings.SEARCH_POLICY_ON_PRODUCT:
            alfred_utils.query_on_product(self.cleaned_data, self.request.user)

        qs_policies = list(alfred_utils.policy_search(self.cleaned_data))
        if qs_policies:
            return {'success': True, 'results': [p.djson() for p in qs_policies], 'count': len(qs_policies)}
        else:
            return {'success': False, 'results': [], 'count': 0}


class NotesForm(fhurl.RequestForm):
    my_notes = forms.CharField(required=False, max_length=10000, widget=forms.Textarea)

    def init(self, ticket_id, key):
        self.ticket = alfred_models.Ticket.objects.get(id=ticket_id)
        self.key = key
        self.initial = {key: self.ticket.data.get(self.key, '')}

    def save(self):
        ticket = self.ticket
        ticket.data.update({self.key: self.data.get(self.key, '')})
        ticket.save(self.request.user)
        return {'ticket_id': ticket.id}


class SendSMSForm(fhurl.RequestForm):
    template = forms.ModelChoiceField(queryset=core_models.CommunicationTemplate.objects.all())
    params_map = forms.CharField()

    def init(self, ticket_id):
        self.ticket = alfred_models.Ticket.objects.get(pk=ticket_id)

    def save(self):
        rdata = self.cleaned_data
        msg_sent, success = alfred_utils.send_message(self.ticket, rdata['template'],
                                                      self.request.user, json.loads(rdata['params_map']))
        if success:
            task = core_models.Task(
                category=core_models.TaskCategory.objects.get(name='Notify Customer'),
                assigned_to=self.request.user,
                scheduled_time=datetime.datetime.now(),
                extra={"purpose": "sms only task to dispose activity", "type": "fake_task"}
            )
            task.save(self.request.user)
            activity = core_models.Activity(
                primary_task=task,
                time_range=(datetime.datetime.now(), datetime.datetime.now()),
                disposition=core_models.Disposition.objects.get(value='NOTIFICATION_TO_CUSTOMER'),
                category='sms',
                data={
                    "agent_name": self.request.user.username,
                    "snippet": msg_sent,
                    "contact": self.ticket.contacts.all()[0].primary_phone_number.number
                }
            )
            activity.save(self.request.user)
            self.ticket.activities.add(activity)

        return {'success': success, 'ticket_id': self.ticket.id}


class TicketSearchForm(fhurl.RequestForm):
    id = forms.CharField(max_length=12, required=False,
                         widget=forms.TextInput(attrs={'class': 'input-sm form-control fg-input'}))
    customer_email = forms.EmailField(required=False,
                                      widget=forms.TextInput(attrs={'class': 'input-sm form-control fg-input'}))
    customer_mobile = forms.CharField(max_length=20, required=False,
                                      widget=forms.TextInput(attrs={'class': 'input-sm form-control fg-input'}))
    customer_name = forms.CharField(max_length=100, required=False,
                                    widget=forms.TextInput(attrs={'class': 'input-sm form-control fg-input'}))
    owner = forms.ModelChoiceField(required=False, queryset=core_models.User.objects.all(),
                                   widget=forms.Select(attrs={'class': 'form-control input-sm'}))
    status = forms.ModelChoiceField(required=False, queryset=alfred_models.TicketStatus.objects.all(),
                                    widget=forms.Select(attrs={'class': 'form-control input-sm'}))
    priority = forms.ModelChoiceField(required=False, queryset=alfred_models.TicketPriority.objects.all(),
                                      widget=forms.Select(attrs={'class': 'form-control input-sm'}))

    def __init__(self, *args, **kwargs):
        super(TicketSearchForm, self).__init__(*args, **kwargs)
        self.fields['owner'].empty_label = ""
        self.fields['status'].empty_label = ""
        self.fields['priority'].empty_label = ""

    def save(self):
        qs_tickets = list(alfred_utils.ticket_search(self.cleaned_data))
        if qs_tickets:
            return {'success': True, 'results': [t.mini_json() for t in qs_tickets], 'count': len(qs_tickets)}
        else:
            return {'success': False, 'results': [], 'count': 0}


class CreateTicketForm(fhurl.RequestForm):
    PRODUCT_TYPE_CHOICES = [
        ("", "---------"),
        ("health", "Health"),
        ("motor", "Motor"),
        ("bike", "Bike"),
        ("travel", "Travel"),
        ("home", "Home"),
        ("service", 'Service'),
    ]

    policy_id = forms.CharField(required=False)
    priority = forms.ModelChoiceField(required=False, queryset=alfred_models.TicketPriority.objects.all(),
                                      widget=forms.Select(attrs={'class': 'form-control input-sm'}))
    product_type = forms.ChoiceField(choices=PRODUCT_TYPE_CHOICES,
                                     widget=forms.Select(attrs={'class': 'form-control input-sm'}))
    request_type = forms.ModelChoiceField(
        queryset=alfred_models.TicketCategory.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control input-sm'}))
    sub_request_types = forms.ModelMultipleChoiceField(queryset=alfred_models.SubRequestType.objects.all(),
                                                       widget=forms.CheckboxSelectMultiple())
    internal_notes = forms.CharField(
        required=False, max_length=1000,
        widget=forms.Textarea(attrs={'class': 'form-control input-sm p-5', 'placeholder': 'Type here...'}))

    first_name = forms.CharField(required=False, max_length=30,
                                 widget=forms.TextInput(
                                     attrs={'class': 'form-control input-sm', 'placeholder': 'First Name'}))
    last_name = forms.CharField(required=False, max_length=30,
                                widget=forms.TextInput(
                                    attrs={'class': 'form-control input-sm', 'placeholder': 'Last Name'}))
    email = forms.EmailField(required=False, max_length=30,
                             widget=forms.TextInput(attrs={'class': 'form-control input-sm', 'placeholder': 'Email id'}))
    mobile = forms.CharField(
        max_length=30, widget=forms.TextInput(attrs={'class': 'form-control input-sm', 'placeholder': 'Mobile No'}))
    transaction_id = forms.CharField(required=False, max_length=60,
                                     widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    policy_no = forms.CharField(required=False, max_length=50,
                                widget=forms.TextInput(
                                    attrs={'class': 'form-control input-sm', 'placeholder': 'Policy No'}))
    owner = forms.ModelChoiceField(required=False, queryset=core_models.User.objects.filter(
                                   queues__name__in=['Relationship Team', 'Resolution Team'], is_active=True),
                                   widget=forms.Select(attrs={'class': 'form-control input-sm'}))

    def init(self, ticket_id=None):
        self.ticket_id = ticket_id
        self.policy_id = self.request.GET.get('policy_id')

        try:
            policy = alfred_models.Policy.objects.get(id=self.policy_id)
            policy_rm = policy.relationship_manager
            if policy_rm:
                owner = policy_rm
            elif self.request and self.request.user in self.fields['owner'].queryset:
                owner = self.request.user
            else:
                owner = ""
            self.initial = {
                'policy_id': self.policy_id,
                'product_type': policy.data.get('product_type'),
                'first_name': policy.data.get('proposer_first_name'),
                'last_name': policy.data.get('proposer_last_name'),
                'mobile': policy.data.get('mobile'),
                'email': policy.data.get('email'),
                'policy_no': policy.policy_no,
                'transaction_id': policy.transactions.all()[0].transaction_id,
                'priority': alfred_models.TicketPriority.get_default(),
                'owner': owner,
            }
        except:
            self.initial = {}

        if ticket_id:
            ticket = alfred_models.Ticket.objects.get(id=ticket_id)
            self.initial.update({
                'request_type': ticket.category,
                'sub_request_types': alfred_models.SubRequestType.objects.filter(request__ticket=ticket),
            })

    def clean_mobile(self):
        phone = core_models.Phone.objects.filter(number=self.cleaned_data['mobile'])
        email = core_models.Email.objects.filter(address=self.data.get('email', ''))
        person = core_models.Person.objects.filter(Q(phones__in=phone) | Q(emails__in=email))
        if person:
            person = person[0]
        else:
            person = core_models.Person(
                first_name=self.cleaned_data['first_name'],
                last_name=self.cleaned_data['last_name'],
            )
            person.save(None)
        if phone:
            phone = phone[0]
            phone.person = person
        else:
            phone = core_models.Phone(number=self.cleaned_data['mobile'], person=person)
        if email:
            email = email[0]
            email.person = person
        else:
            email = core_models.Email(address=self.data.get('email'), person=person)

        if self.data.get('mobile'):
            phone.is_primary = True
            phone.save(None)
        if self.data.get('email'):
            email.is_primary = True
            email.save(None)

        self.cleaned_data['person'] = person
        return self.cleaned_data['mobile']

    def clean_request_type(self):
        if isinstance(self.data['request_type'], int) or isinstance(self.data['request_type'], str):
            self.cleaned_data['request_type'] = alfred_models.TicketCategory.objects.get(id=self.data['request_type'])
        return self.cleaned_data['request_type']

    def clean_sub_request_types(self):

        sbt = self.cleaned_data['sub_request_types']
        if isinstance(sbt, list):
            sbt = alfred_models.SubRequestType.objects.filter(id__in=sbt)
        return sbt

    def clean_policy_no(self):
        policy_no = self.cleaned_data['policy_no']
        transaction_id = self.cleaned_data['transaction_id']
        if self.cleaned_data['request_type'].name in ['OFFLINE_SALE', 'ONLINE_SALE']:
            tr = leads_models.Transaction(
                transaction_id=transaction_id,
                is_successful=self.cleaned_data.get('success') or False,
                payment_date=datetime.datetime.strptime(self.cleaned_data['payment_date'], '%d/%m/%Y'),
                extra=self.cleaned_data,
            )
            tr.save(None)

            policy = alfred_models.Policy(
                policy_no=policy_no,
                data=self.cleaned_data
            )
            policy.save(None)
            tr.policies.add(policy)
        elif self.cleaned_data['request_type'].name == 'FRESH':
            policy = alfred_models.Policy.get_default()
        elif policy_no:
            policy = alfred_models.Policy.objects.get(policy_no=policy_no)
        else:
            tr = leads_models.Transaction.objects.get(transaction_id=transaction_id)
            policy = tr.policies.all()[0]

        self.cleaned_data['policy'] = policy

        return policy_no

    def create_initial_task(self, ticket):
        task_category = alfred_models.StateMachineTaskCategory.objects.get(
            state=ticket.current_state,
            machine=ticket.machine
        ).task_category

        task = core_models.Task(
            category=task_category,
            scheduled_time=datetime.datetime.now(),
            extra={},
        )
        task.assigned_to = core_models.User.get_system_user()
        task.save(None)

        return task

    def add_suggested_documents(self, ticket):
        for item in ticket.category.suggested_document_categories.filter(
                sub_request_type__in=self.cleaned_data['sub_request_types']
        ).distinct():
            tdc = alfred_models.TicketDocumentCategory(ticket=ticket, category=item)
            tdc.save(None)

    def add_requests_to_ticket(self, ticket, rdata, request_user):
        for item in rdata['sub_request_types']:
            req = alfred_models.Request(
                sub_request_type=item,
                ticket=ticket,
                data={},
                start_date=datetime.datetime.now(),
                end_date=datetime.datetime.now() + item.eta
            )
            req.save(request_user)

    def save(self):

        request_user = None
        if self.request and self.request.user.is_authenticated():
            request_user = self.request.user

        rdata = self.cleaned_data
        rdata.update({'ticket_category': rdata['request_type'].name})

        if not rdata['priority']:
            self.cleaned_data['priority'] = alfred_models.TicketPriority.get_default()
        machine = sm_qmaker.get_matching_machine(rdata)

        if self.ticket_id:
            ticket = alfred_models.Ticket.objects.get(id=self.ticket_id)
        else:
            ticket = alfred_models.Ticket()

        ticket.policy = rdata['policy']
        ticket.category = rdata['request_type']
        ticket.owner = rdata.get('owner') or alfred_utils.get_ticket_owner(rdata['policy'], request_user)
        ticket.data = core_utils.serialize(rdata)
        ticket.machine = machine
        ticket.priority = rdata['priority']

        if request_user and rdata.get('internal_notes'):
            ticket.data.update({
                "chat_notes": json.dumps([{
                    "text": rdata.get('internal_notes'),
                    "timestamp": str(datetime.datetime.now()),
                    "writer": request_user.username
                }])
            })
        ticket.start_date = datetime.datetime.now()
        ticket.save(request_user)

        self.add_requests_to_ticket(ticket, rdata, request_user)
        ticket.end_date = max(alfred_models.Request.objects.filter(ticket=ticket).values_list('end_date', flat=True))

        self.add_suggested_documents(ticket)

        state_history = alfred_models.TicketState(
            ticket=ticket,
            state=machine.initial_state,
            enter_time=datetime.datetime.now(),
        )
        state_history.save()

        ticket.contacts.add(*core_models.Person.objects.filter(id=self.cleaned_data['person'].id))

        ticket.add_task()
        ticket.save(request_user)
        ticket.push_to_subscribers()

        return {'success': True, 'ticket_id': ticket.id, 'next_task_user_id': ticket.tasks.last().id}


class ProductTransactionData(CreateTicketForm):
    def init(self, tdata):
        pass

    def save(self):
        rdata = self.cleaned_data
        rdata.update({"ticket_category": self.cleaned_data['request_type']})
        response = alfred_utils.create_ticket(rdata)
        return {"ticket_id": response['ticket_id'], 'success': True}


class TicketStatusForm(fhurl.RequestForm):
    tags = forms.ModelMultipleChoiceField(required=False, queryset=alfred_models.TicketTag.objects.all(),
                                          widget=forms.CheckboxSelectMultiple())
    priority = forms.ModelChoiceField(
        queryset=alfred_models.TicketPriority.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    def init(self, ticket_id):
        self.instance = alfred_models.Ticket.objects.get(id=ticket_id)
        self.ticket_id = self.instance.id
        self.initial = {
            'tags': self.instance.tags.all(),
            'priority': self.instance.priority,
        }

    def save(self):
        ticket = self.instance
        ticket.priority = self.cleaned_data['priority']
        ticket.save(self.request.user)
        ticket.tags.clear()
        ticket.tags.add(*self.cleaned_data['tags'])

        return {'success': True}


class TaskForm(fhurl.RequestForm):
    next_task = forms.ModelChoiceField(queryset=None, widget=forms.Select(attrs={'class': 'form-control'}))
    next_task_time = forms.DateTimeField()

    def init(self, ticket_id):
        self.ticket = get_object_or_404(alfred_models.Ticket, pk=ticket_id)
        self.ticket_id = ticket_id
        self.fields['next_task_time'].widget = forms.TextInput(
            attrs={"data-format": "dd/MM/yyyy hh:mm:ss", "class": "form-control input-sm",
                   "placeholder": "eg: DD/MM/YYYY HH:MM:SS"}
        )
        smtc = alfred_models.StateMachineTaskCategory.objects.filter(
            state=self.ticket.current_state,
            machine=self.ticket.machine
        )
        self.fields['next_task'].queryset = core_models.TaskCategory.objects.filter(statemachinetaskcategory__in=smtc)
        self.initial = {
            'next_task': smtc[0].task_category if smtc else ''
        }

    def save(self):
        self.ticket.add_task(
            task_category=self.cleaned_data['next_task'],
            scheduled_time=self.cleaned_data['next_task_time'],
            from_activity=self.ticket.activities.last()
        )

        return {'success': True}


class DispositionForm(fhurl.RequestForm):
    head_disposition = forms.ModelChoiceField(queryset=core_models.Disposition.objects.all())
    sub_dispositions = forms.ModelMultipleChoiceField(queryset=core_models.SubDisposition.objects.all())

    def clean_sub_dispositions(self):
        d = self.cleaned_data
        valid_sub_dispositions = d['head_disposition'].sub_dispositions.all()
        for sd in d['sub_dispositions']:
            if sd not in valid_sub_dispositions:
                raise forms.ValidationError("Sub-disposition '%s' is not a valid choice" % sd)
        return d['sub_dispositions']

    def init(self, ticket_id):
        self.ticket = get_object_or_404(alfred_models.Ticket, pk=ticket_id)

    def save(self, request_user, ticket, form_data):
        old_task = ticket.tasks.last()
        data = self.cleaned_data
        data.update(form_data)
        activity = core_utils.dispose_task(
            data, request_user, ticket, old_task
        )
        ticket = ticket.dispose_activity(request_user, activity)
        return {'success': True}


class BaseStateForm(forms.Form):
    def __init__(self, ticket_id, *args, **kwargs):
        super(BaseStateForm, self).__init__(*args, **kwargs)
        self.ticket = get_object_or_404(alfred_models.Ticket, pk=ticket_id)

    def save(self, request_user):
        ticket = self.ticket
        tdata = ticket.data
        tdata.update(self.data)
        ticket.data = tdata
        ticket.save(request_user)


class CancelInitialForm(BaseStateForm):
    reason_for_cancellation = forms.CharField(required=False, widget=forms.Textarea(
        attrs={'class': 'form-control', 'rows': 1, 'placeholder': 'Type here...'}))

    def init(self, ticket):
        self.initial = {
            'reason_for_cancellation': ticket.data.get('reason_for_cancellation')
        }


class CancelCustomerDocumentPendingForm(BaseStateForm):
    reason_for_cancellation = forms.CharField(required=False, widget=forms.Textarea(
        attrs={'class': 'form-control', 'rows': 1, 'placeholder': 'Type here...'}))
    is_all_documents_received = forms.ChoiceField(
        required=False,
        widget=forms.RadioSelect, choices=((True, "Yes"), (False, "No"))
    )
    is_additional_requirement = forms.ChoiceField(
        required=False,
        widget=forms.RadioSelect, choices=((True, "Yes"), (False, "No"))
    )

    def init(self, ticket):
        self.initial = {
            'reason_for_cancellation': ticket.data.get('reason_for_cancellation')
        }


class CancelCustomerDocumentUploadPendingForm(BaseStateForm):
    reason_for_cancellation = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control'}))

    def init(self, ticket):
        self.initial = {
            'reason_for_cancellation': ticket.data.get('reason_for_cancellation')
        }


class CancelInsurerDecisionPendingForm(BaseStateForm):
    reason_for_rejection = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control'}))
    is_request_accepted = forms.ChoiceField(required=False, widget=forms.RadioSelect,
                                            choices=((True, "Yes"), (False, "No")))

    def init(self, ticket):
        pass


class RefundInitialForm(BaseStateForm):
    refund_amount_requested = forms.CharField(required=False,
                                              widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))

    def init(self, ticket):
        self.initial = {
            'refund_amount_requested': ticket.data.get('refund_amount_requested')
        }


class RefundInsurerConfirmationPendingForm(BaseStateForm):
    refund_amount_requested = forms.CharField(required=False,
                                              widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    bank_reference_no = forms.CharField(required=False,
                                        widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    utr_no = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))

    def init(self, ticket):
        self.initial = {
            'refund_amount_requested': ticket.data.get('refund_amount_requested')
        }


class RefundReceivedForm(BaseStateForm):
    refund_amount_requested = forms.CharField(required=False)
    bank_reference_no = forms.CharField(required=False)
    utr_no = forms.CharField(required=False)
    refund_amount_confirmed = forms.CharField(required=False,
                                              widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))

    def init(self, ticket):
        ticket_data = ticket.data
        self.initial = {
            'refund_amount_requested': ticket_data.get('refund_amount_requested'),
            'bank_reference_no': ticket_data.get('refund_amount_requested'),
            'utr_no': ticket_data.get('utr_no')
        }


class ClaimMotorInsurerIntimatedForm(BaseStateForm):
    claim_amount_requested = forms.CharField(required=False)

    def init(self, ticket):
        pass


class ClaimHealthInsurerIntimatedForm(BaseStateForm):
    claim_amount_requested = forms.CharField(required=False,
                                             widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    hospital_name = forms.CharField(required=False,
                                    widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    ailment_date = forms.DateField(required=False)
    admission_date = forms.DateField(required=False)
    discharge_date = forms.DateField(required=False)

    def init(self, ticket):
        self.fields['ailment_date'].widget = forms.TextInput(
            attrs={'class': 'form-control input-sm date-picker', 'data-toggle': 'dropdown'}
        )
        self.fields['admission_date'].widget = forms.TextInput(
            attrs={'class': 'form-control input-sm date-picker', 'data-toggle': 'dropdown'}
        )
        self.fields['discharge_date'].widget = forms.TextInput(
            attrs={'class': 'form-control input-sm date-picker', 'data-toggle': 'dropdown'}
        )


class ClaimMotorCustomerDocumentPendingForm(BaseStateForm):
    claim_amount_requested = forms.CharField(required=False)
    is_all_documents_received = forms.ChoiceField(
        required=False,
        widget=forms.RadioSelect, choices=((True, "Yes"), (False, "No"))
    )

    def init(self, ticket):
        ticket_data = ticket.data
        self.initial = {
            'claim_amount_requested': ticket_data.get('claim_amount_requested'),
        }


class ClaimHealthCustomerDocumentPendingForm(BaseStateForm):
    claim_amount_requested = forms.CharField(required=False)
    hospital_name = forms.CharField(required=False)
    ailment_date = forms.DateField(required=False)
    admission_date = forms.DateField(required=False)
    discharge_date = forms.DateField(required=False)
    is_all_documents_received = forms.ChoiceField(
        required=False,
        widget=forms.RadioSelect, choices=((True, "Yes"), (False, "No"))
    )

    def init(self, ticket):
        self.fields['ailment_date'].widget = forms.TextInput(
            attrs={'class': 'form-control date-picker', 'data-toggle': 'dropdown'}
        )
        self.fields['admission_date'].widget = forms.TextInput(
            attrs={'class': 'form-control date-picker', 'data-toggle': 'dropdown'}
        )
        self.fields['discharge_date'].widget = forms.TextInput(
            attrs={'class': 'form-control date-picker', 'data-toggle': 'dropdown'}
        )
        ticket_data = ticket.data
        self.initial = {
            'claim_amount_requested': ticket_data.get('claim_amount_requested'),
            'hospital_name': ticket_data.get('hospital_name'),
            'ailment_date': ticket_data.get('ailment_date'),
            'admission_date': ticket_data.get('admission_date'),
            'discharge_date': ticket_data.get('discharge_date'),
        }


class ClaimMotorCustomerDocumentUploadPendingForm(BaseStateForm):
    claim_amount_requested = forms.CharField(required=False)

    def init(self, ticket):
        ticket_data = ticket.data
        self.initial = {
            'claim_amount_requested': ticket_data.get('claim_amount_requested'),
        }


class ClaimHealthCustomerDocumentUploadPendingForm(BaseStateForm):
    claim_amount_requested = forms.CharField()
    hospital_name = forms.CharField(required=False)
    ailment_date = forms.DateField(required=False)
    admission_date = forms.DateField(required=False)
    discharge_date = forms.DateField(required=False)

    def init(self, ticket):
        self.fields['ailment_date'].widget = forms.TextInput(
            attrs={'class': 'form-control date-picker', 'data-toggle': 'dropdown'}
        )
        self.fields['admission_date'].widget = forms.TextInput(
            attrs={'class': 'form-control date-picker', 'data-toggle': 'dropdown'}
        )
        self.fields['discharge_date'].widget = forms.TextInput(
            attrs={'class': 'form-control date-picker', 'data-toggle': 'dropdown'}
        )
        ticket_data = ticket.data
        self.initial = {
            'claim_amount_requested': ticket_data.get('claim_amount_requested'),
            'hospital_name': ticket_data.get('hospital_name'),
            'ailment_date': ticket_data.get('ailment_date'),
            'admission_date': ticket_data.get('admission_date'),
            'discharge_date': ticket_data.get('discharge_date'),
        }


class ClaimDocumentUploadedForm(BaseStateForm):
    claim_amount_requested = forms.CharField()
    hospital_name = forms.CharField(required=False)
    ailment_date = forms.DateField(required=False)
    admission_date = forms.DateField(required=False)
    discharge_date = forms.DateField(required=False)

    def init(self, ticket):
        self.fields['ailment_date'].widget = forms.TextInput(
            attrs={'class': 'form-control date-picker', 'data-toggle': 'dropdown'}
        )
        self.fields['admission_date'].widget = forms.TextInput(
            attrs={'class': 'form-control date-picker', 'data-toggle': 'dropdown'}
        )
        self.fields['discharge_date'].widget = forms.TextInput(
            attrs={'class': 'form-control date-picker', 'data-toggle': 'dropdown'}
        )
        ticket_data = ticket.data
        self.initial = {
            'claim_amount_requested': ticket_data.get('claim_amount_requested'),
        }


class ClaimInsurerDecisionPendingForm(BaseStateForm):
    claim_amount_requested = forms.CharField()
    claim_amount_approved = forms.CharField(required=False)
    bank_reference_no = forms.CharField(required=False)
    utr_no = forms.CharField(required=False)
    is_request_accepted = forms.ChoiceField(required=False, widget=forms.RadioSelect,
                                            choices=((True, "Yes"), (False, "No")))

    def init(self, ticket):
        ticket_data = ticket.data
        self.initial = {
            'claim_amount_requested': ticket_data.get('claim_amount_requested'),
        }


class ClaimAccepted(BaseStateForm):
    bank_reference_no = forms.CharField()
    utr_no = forms.CharField()

    def init(self, ticket):
        ticket_data = ticket.data
        self.initial = {
            'bank_reference_no': ticket_data.get('refund_amount_requested'),
            'utr_no': ticket_data.get('utr_no')
        }


class ClaimRejected(BaseStateForm):
    reason_for_rejection = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control'}))

    def init(self, ticket):
        pass


class EndorsementIntiialForm(BaseStateForm):
    reason_for_endorsement = forms.CharField(required=False, widget=forms.Textarea(
        attrs={'class': 'form-control', 'rows': 1, 'placeholder': 'Type here...'}))

    def init(self, ticket):
        pass


class EndorsementCustomerDocumentPendingForm(BaseStateForm):
    reason_for_endorsement = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control'}))
    is_all_documents_received = forms.ChoiceField(
        required=False,
        widget=forms.RadioSelect, choices=((True, "Yes"), (False, "No"))
    )

    def init(self, ticket):
        self.initial = {
            'reason_for_endorsement': ticket.data.get('reason_for_endorsement')
        }


class EndorsementDocumentUploadPendingForm(BaseStateForm):
    reason_for_endorsement = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control'}))

    def init(self, ticket):
        self.initial = {
            'reason_for_endorsement': ticket.data.get('reason_for_endorsement')
        }


class EndorsementInsurerNotificationPendingForm(BaseStateForm):
    reason_for_endorsement = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control'}))

    def init(self, ticket):
        self.initial = {
            'reason_for_endorsement': ticket.data.get('reason_for_endorsement')
        }


class EndorsementInsurerConfirmationPendingForm(BaseStateForm):
    reason_for_endorsement = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control'}))
    is_request_accepted = forms.ChoiceField(required=False, widget=forms.RadioSelect,
                                            choices=((True, "Yes"), (False, "No")))

    def init(self, ticket):
        self.initial = {
            'reason_for_endorsement': ticket.data.get('reason_for_endorsement')
        }


class ApprovedPolicyQcPendingForm(BaseStateForm):
    is_passed_quality_check = forms.ChoiceField(required=False, widget=forms.RadioSelect,
                                                choices=((True, "Yes"), (False, "No")))

    def init(self, ticket):
        pass


class ApprovedPolicyUploadedForm(BaseStateForm):
    policy_dispatch_date = forms.DateField(required=False)

    def init(self, ticket):
        self.fields['policy_dispatch_date'].widget = forms.TextInput(
            attrs={'class': 'form-control date-picker', 'data-toggle': 'dropdown'}
        )


class ApprovedPolicyDispatchPendingForm(BaseStateForm):
    policy_dispatch_date = forms.DateField(required=False)
    is_policy_dispatched = forms.ChoiceField(required=False, widget=forms.RadioSelect,
                                             choices=((True, "Yes"), (False, "No")))

    def init(self, ticket):
        self.fields['policy_dispatch_date'].widget = forms.TextInput(
            attrs={'class': 'form-control date-picker', 'data-toggle': 'dropdown'}
        )
        self.initial = {
            'policy_dispatch_date': ticket.data.get('policy_dispatch_date')
        }


class ApprovedPolicyDispatchedForm(BaseStateForm):
    policy_dispatch_date = forms.DateField(required=False)

    def init(self, ticket):
        self.fields['policy_dispatch_date'].widget = forms.TextInput(
            attrs={'class': 'form-control date-picker', 'data-toggle': 'dropdown'}
        )
        self.initial = {
            'policy_dispatch_date': ticket.data.get('policy_dispatch_date')
        }


class OfflineInitialForm(BaseStateForm):
    appointment_time = forms.DateTimeField(required=False)
    is_payment_done = forms.ChoiceField(widget=forms.RadioSelect, choices=((True, "Yes"), (False, "No")))
    proposal_form_mode = forms.ChoiceField(widget=forms.RadioSelect, choices=('ONLINE', 'OFFLINE'))
    is_appointment_scheduled = forms.ChoiceField(widget=forms.RadioSelect, choices=((True, "Yes"), (False, "No")))

    def init(self, ticket):
        self.fields['appointment_time'].widget = forms.TextInput(
            attrs={'class': 'form-control date-picker', 'data-toggle': 'dropdown'}
        )
        self.initial = {
            'appointment_time': ticket.data.get('appointment_time')
        }


class OfflineAppointmentScheduledForm(BaseStateForm):
    appointment_time = forms.DateTimeField(required=False)
    is_appointment_accepted = forms.ChoiceField(
        required=False,
        widget=forms.RadioSelect,
        choices=((True, "Yes"), (False, "No"))
    )

    def init(self, ticket):
        self.fields['appointment_time'].widget = forms.TextInput(
            attrs={'class': 'form-control date-picker', 'data-toggle': 'dropdown'}
        )
        self.initial = {
            'appointment_time': ticket.data.get('appointment_time')
        }


class OfflineAppointmentAcceptedForm(BaseStateForm):
    appointment_time = forms.DateTimeField(required=False)
    is_appointment_successful = forms.ChoiceField(
        required=False,
        widget=forms.RadioSelect,
        choices=((True, "Yes"), (False, "No"))
    )

    def init(self, ticket):
        self.fields['appointment_time'].widget = forms.TextInput(
            attrs={'class': 'form-control date-picker', 'data-toggle': 'dropdown'}
        )
        self.initial = {
            'appointment_time': ticket.data.get('appointment_time')
        }


class OfflineInsurerDecisionPendingForm(BaseStateForm):
    appointment_time = forms.DateTimeField(required=False)
    reason_for_rejection = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control'}))
    is_request_accepted = forms.ChoiceField(required=False, widget=forms.RadioSelect,
                                            choices=((True, "Yes"), (False, "No")))
    is_payment_done = forms.ChoiceField(required=False, widget=forms.RadioSelect,
                                        choices=((True, "Yes"), (False, "No")))

    def init(self, ticket):
        self.fields['appointment_time'].widget = forms.TextInput(
            attrs={'class': 'form-control date-picker', 'data-toggle': 'dropdown'}
        )
        self.initial = {
            'appointment_time': ticket.data.get('appointment_time')
        }


class OfflineCustomerDocumentPendingForm(BaseStateForm):
    is_all_documents_received = forms.ChoiceField(
        required=False,
        widget=forms.RadioSelect,
        choices=((True, "Yes"), (False, "No"))
    )

    def init(self, ticket):
        pass


class OfflineRequestRejectedForm(BaseStateForm):
    reason_for_rejection = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control'}))

    def init(self, ticket):
        self.initial = {
            'reason_for_rejection': ticket.data.get('reason_for_rejection')
        }


class OfflinePaymentPendingForm(BaseStateForm):
    payment_amount = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    is_proposal_form_filled = forms.ChoiceField(widget=forms.RadioSelect, choices=((True, "Yes"), (False, "No")))
    is_payment_done = forms.ChoiceField(widget=forms.RadioSelect, choices=((True, "Yes"), (False, "No")))

    def init(self, ticket):
        pass
