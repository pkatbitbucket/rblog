from django.core.management.base import BaseCommand
from alfred.utils import post_transaction_data


class Command(BaseCommand):
    def run_command(self):
        post_transaction_data("cancellation_flow")

    def handle(self, *args, **options):
        self.run_command()
