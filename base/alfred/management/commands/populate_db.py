from __future__ import unicode_literals
from django.core.management.base import BaseCommand

from alfred.models import Tag, Bucket, CheckPoint, Process


class Command(BaseCommand):
    def populate_fake_tags(self):
        Tag.objects.all().delete()

        for item in "123456789":
            new_tag = Tag(key='key' + str(item), value='val' + str(item))
            new_tag.save()
            print "Fake Tag { '%s' : '%s' } Created!!!" % (new_tag.key, new_tag.value)

    def populate_fake_buckets(self):
        Bucket.objects.all().delete()

        for item in ['bucket1', 'bucket2', 'bucket3', 'bucket4']:
            new_process = Bucket(name=str(item), description=str(item))
            new_process.save()
            print "Fake Bucket < %s > Created!!!" % new_process.name

    def populate_fake_checkpoints(self):
        CheckPoint.objects.all().delete()

        for item in "123456789":
            new_checkpoint = CheckPoint()
            new_checkpoint.save()
            print "Fake Checkpoint < %s > Created!!!" % new_checkpoint.id

    def populate_fake_processes(self):
        Process.objects.all().delete()

        new_process1 = Process(name='process1', description='process1')
        new_process1.save()
        checkpoints = [cp for cp in CheckPoint.objects.all()[:4]]
        new_process1.add_process_checkpoint(checkpoints)
        new_process1.tags.add(Tag.objects.all()[0])
        new_process1.tags.add(Tag.objects.all()[1])
        new_process1.tags.add(Tag.objects.all()[2])
        new_process1.tags.add(Tag.objects.all()[3])
        print "Fake Process < %s > Created!!!" % new_process1.name

        new_process2 = Process(name='process2', description='process2')
        new_process2.save()
        checkpoints = [cp for cp in CheckPoint.objects.all()[2:6]]
        new_process2.add_process_checkpoint(checkpoints)
        new_process2.tags.add(Tag.objects.all()[2])
        new_process2.tags.add(Tag.objects.all()[3])
        new_process2.tags.add(Tag.objects.all()[4])
        new_process2.tags.add(Tag.objects.all()[5])
        print "Fake Process < %s > Created!!!" % new_process2.name

        new_process3 = Process(name='process3', description='process3')
        new_process3.save()
        checkpoints = [cp for cp in CheckPoint.objects.all()[4:8]]
        new_process3.add_process_checkpoint(checkpoints)
        new_process3.tags.add(Tag.objects.all()[4])
        new_process3.tags.add(Tag.objects.all()[5])
        new_process3.tags.add(Tag.objects.all()[6])
        new_process3.tags.add(Tag.objects.all()[7])
        print "Fake Process < %s > Created!!!" % new_process3.name

        new_process4 = Process(name='process4', description='process4')
        new_process4.save()
        checkpoints = [cp for cp in CheckPoint.objects.all()[6:]]
        new_process4.add_process_checkpoint(checkpoints)
        new_process4.tags.add(Tag.objects.all()[6])
        new_process4.tags.add(Tag.objects.all()[7])
        new_process4.tags.add(Tag.objects.all()[8])
        print "Fake Process < %s > Created!!!" % new_process4.name

    def handle(self, *args, **options):
        self.populate_fake_tags()
        self.populate_fake_buckets()
        self.populate_fake_checkpoints()
        self.populate_fake_processes()
