from django.core.management.base import BaseCommand
from alfred.redis_utils import push_ticket_data


class Command(BaseCommand):
    def run_command(self):
        push_ticket_data()

    def handle(self, *args, **options):
        self.run_command()
