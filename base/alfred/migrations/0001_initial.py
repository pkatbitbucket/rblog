# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0021_auto_20151219_2045'),
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(max_length=255, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='DocumentCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('params', jsonfield.fields.JSONField()),
                ('request_type', models.ManyToManyField(related_name='suggested_document_categories', to='core.RequirementKind')),
            ],
        ),
        migrations.AddField(
            model_name='document',
            name='category',
            field=models.ForeignKey(related_name='document_in_category', to='alfred.DocumentCategory'),
        ),
        migrations.AddField(
            model_name='document',
            name='policy',
            field=models.ForeignKey(related_name='documents_for_policies', to='core.Policy'),
        ),
        migrations.AddField(
            model_name='document',
            name='requirements',
            field=models.ManyToManyField(to='core.Requirement'),
        ),
    ]
