from django.db import models

from jsonfield import JSONField


class DocumentCategory(models.Model):
    name = models.CharField(max_length=50)
    request_type = models.ManyToManyField('core.RequirementKind', related_name='suggested_document_categories')
    params = JSONField()

    def __unicode__(self):
        return self.name


class Document(models.Model):
    """
    # TODO: Merge with core.
    """
    key = models.CharField(max_length=255, null=True)
    category = models.ForeignKey('DocumentCategory', related_name='document_in_category')
    policy = models.ForeignKey('core.Policy', related_name="documents_for_policies")
    requirements = models.ManyToManyField('core.Requirement')

    def mini_json(self):
        return {
            'pk': self.id,
            'file': self.key,
            'category': self.category.djson(),
            'policy': {'pk': self.policy.id},
            'requirements': [{'pk': t.id} for t in self.requirements.all()]
        }

    def __unicode__(self):
        return self.key + " - " + self.category.name

# TODO: Discuss with Deepak & Amitu migrating to core.
# class TicketDocumentCategory(crm_models.CustomBaseAbstractModel, djson.Djson):
#     ticket = models.ForeignKey('Ticket', related_name='ticket_document_category')
#     category = models.ForeignKey('DocumentCategory', related_name='ticket_document_category')
#
#     def djson(self):
#         return {
#             'pk': self.id,
#             'ticket': {'pk': self.ticket.id},
#             'category': self.category.djson(),
#         }
#
#     def __unicode__(self):
#         return "Ticket - " + str(self.ticket.id) + " - " + self.category.name
