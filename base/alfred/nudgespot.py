import time
import re
import json
import copy
import requests
from requests import request
from django.conf import settings

if settings.NUDGESPOT:
    API_KEY = '16f27c73328b02d310d3cda5a88c3092'  # live account
else:
    API_KEY = 'f79d9dc779fd802aa38a191ac705b439'  # test account

AUTH = ('api', API_KEY)
BASE_URL = 'https://api.nudgespot.com/'
HEADERS = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
}


class NudgespotException(BaseException):
    pass


class Nudge(object):
    _recent_request = None

    def __init__(self, _id):
        """
        Initialises with the id provided else sets empty string
        :param _id: id of the user for nudgespot identification
        :type _id: str
        """
        self.id = _id

    def get_user_request(self, _id=None):
        """
        :rtype : request object
        :param _id: id of the user for nudgespot identification
        :type _id: str
        """
        if not _id:
            _id = self.id
        data = {'email': _id}
        return self._send_request(method='GET',
                                  url=BASE_URL + 'subscribers',
                                  auth=AUTH,
                                  headers=HEADERS,
                                  params=data)

    def get_user_data(self):
        """
        Gets the user data from nudgespot and returns in json
        :return: user data
        :rtype : json dictionary
        """
        if self.exists():
            if requests.__version__ < '1.0.0':
                return self._recent_request.json
            return self._recent_request.json()
        return {}

    def exists(self):
        """
        Validation check of the user
        :rtype : bool
        """
        self._recent_request = self.get_user_request()
        return True if self._recent_request.status_code == 200 else False

    def create_user(self, **contacts):
        """
        Creates the user with the id given while initialising. Takes optional dictionary of contacts
        :param contacts: dictionary with contact value as the keys and contact type as values
        :type contacts: dict
        :return: :raise ValueError: if user already exists
        :rtype : bool
        """
        if not self.exists():
            data = {
                'subscriber': {
                    'email': self.id,
                    'contact': [],
                }
            }
            for value, _type in contacts.items():
                data['subscriber']['contact'].append({
                    'contact_type': _type,
                    'contact_value': value,
                })
            data = json.dumps(data)
            self._recent_request = self._send_request(method='POST',
                                                      url=BASE_URL + 'subscribers',
                                                      auth=AUTH,
                                                      headers=HEADERS,
                                                      data=data)
            if self._recent_request.status_code == 201:
                return True
            else:
                return False
        else:
            raise ValueError('user already exists')

    @classmethod
    def send_event(cls, event_name, user_id, event_data={}):
        """
        Pushes the event to nudgespot
        :param event_name: name of the event. should follow the rules of variable naming in python
        :type event_name: str
        :param user_id: id of the user for the event to associate to
        :type user_id: str
        :param event_data: optional dictionary of event data
        :type event_data: dict
        :rtype : bool
        """
        phone = str(event_data.pop('mobile', ''))
        email = str(event_data.pop('email', ''))
        event_name = str(event_name)
        event_data['communication'] = event_data.get('communication', 'sms')
        if not event_name.isdigit() and type(event_name[0]) is str:
            event_name.replace(' ', '_')
            data = {
                'activity': {
                    'user': {
                        'email': user_id,
                        },
                    'event': event_name,
                    'properties': event_data,
                    },
                }
            _request = cls._send_request(method='POST',
                                         url=BASE_URL + 'activities',
                                         auth=AUTH,
                                         headers=HEADERS,
                                         data=json.dumps(data))
            if _request.status_code == 202 or _request.status_code == 201:
                if phone or email:
                    print "reached here mutherfucker 202 or 201"
                    print ">>>>phone and email"
                    print phone, email
                    user = Nudge(user_id)
                    print ">>>>user"
                    print user.__dict__
                    return user.subscribe(**{phone: 'phone', email: 'email'})
            else:
                random_shit = 'Shit happens and it happened now.\nRequest failed with {} error saying\n{}\n{}\t{}'.format(
                    _request.status_code, _request.text, event_name, user_id)
                raise ValueError(random_shit)
        else:
            raise ValueError('Please enter a valid event name')
            # else:
            # raise ValueError("Mobile data doesn't exist. unable to send event")

    def update_or_create_user_data(self, contacts=[], properties={}):
        """
        Update the user data with contacts or properties
        :param contacts: each dictionary contains 'value','type' as keys and corresponding values. optional key 'status'
        :type contacts: list of dictionaries
        :param properties: dictionary/json of properties
        :type properties: dictionary
        :rtype : bool or None
        """
        main_keys = [
            'first_name',
            'last_name',
            'name'
        ]
        data = {
            'subscriber': {
                'email': self.id,
                'contact': [],
                'properties': {},
            }
        }
        initial_data = copy.deepcopy(data)
        for contact_data in contacts:
            contact_value = contact_data['value']
            contact_type = contact_data['type']
            contact_value = self.normalize_mobile(contact_value) if contact_type is 'phone' else contact_value
            contact_status = contact_data.get('status', 'active')
            if getattr(self, 'is_{}'.format(contact_type))(str(contact_value)) and contact_value:
                contact_data = {
                    'contact_type': contact_type,
                    'contact_value': contact_value,
                    'subscription_status': contact_status,
                }
                data['subscriber']['contact'].append(contact_data)
            elif not contact_value:
                pass
            else:
                raise ValueError('{} is not a valid contact'.format(contact_value))

        if properties:
            data['subscriber']['properties'] = properties
            for key in main_keys:
                data['subscriber'][key] = data['subscriber']['properties'].pop(key, None)

        if data != initial_data:
            try:
                self.create_user()
            except ValueError:
                # user already exists
                pass
            data = json.dumps(data)
            user_data = self.get_user_data()
            if user_data and user_data.get('href'):
                url = user_data['href']
            else:
                return False
            self._recent_request = self._send_request(method='PUT',
                                                      url=url,
                                                      auth=AUTH,
                                                      headers=HEADERS,
                                                      data=data)
            if self._recent_request.status_code == 200:
                return True
            else:
                return False
        else:
            return None

    def unsubscribe(self, **kwargs):
        """
        Unsubscribe the contacts of a user
        :rtype : bool or None
        :param kwargs: dictionary with contact value as key and contact type as value
        :type kwargs: dict
        """
        dum = [
            {'value': self.normalize_mobile(value) if _type == 'phone' else value, 'type': _type, 'status': 'inactive'}
            for value, _type in kwargs.items() if getattr(self, 'is_{}'.format(_type))(value)]
        contacts = dum
        if dum:
            for contact in dum:
                if not any(contact['value'] in contact_data['contact_value'] for contact_data in
                           self.get_user_data()['contact']):
                    # print('{} is not in the contact list'.format(contact))
                    contacts.remove(contact)
            if contacts:
                return self.update_or_create_user_data(contacts=contacts)
        return None

    def unsubscribe_mobiles(self):
        mobiles = {contact_data['contact_value']: 'phone' for contact_data in self.get_user_data()['contact'] if
                   contact_data['contact_type'] == 'phone'}
        return self.unsubscribe(**mobiles)

    def unsubscribe_emails(self):
        emails = {contact_data['contact_value']: 'email' for contact_data in self.get_user_data()['contact'] if
                  contact_data['contact_type'] == 'email'}
        return self.unsubscribe(**emails)

    def unsubscribe_all(self):
        """
        Unsubscribe all the contacts of a user
        :rtype : bool or None
        """
        contacts = {contact_data['contact_value']: contact_data['contact_type'] for contact_data in
                    self.get_user_data()['contact']}
        return self.unsubscribe(**contacts)

    def subscribe(self, **kwargs):
        """
        Subscribe the contacts of a user
        :param kwargs: dictionary with contact value as key and contact type as value
        :type kwargs: dict
        :rtype : bool or None
        """
        contacts = [
            {'value': self.normalize_mobile(value) if _type == 'phone' else value, 'type': _type, 'status': 'active'}
            for value, _type in kwargs.items() if getattr(self, 'is_{}'.format(_type))(value)]
        if contacts:
            # data = {'contacts': contacts}
            return self.update_or_create_user_data(contacts=contacts)
        return None

    def subscribe_mobiles(self):
        mobiles = {contact_data['contact_value']: 'phone' for contact_data in self.get_user_data()['contact'] if
                   contact_data['contact_type'] == 'phone'}
        return self.subscribe(**mobiles)

    def subscribe_emails(self):
        emails = {contact_data['contact_value']: 'email' for contact_data in self.get_user_data()['contact'] if
                  contact_data['contact_type'] == 'email'}
        return self.subscribe(**emails)

    def subscribe_all(self):
        """
        Subscribe all the contacts of a user
        :rtype : bool or None
        """
        contacts = {contact_data['contact_value']: contact_data['contact_type'] for contact_data in
                    self.get_user_data()['contact']}
        return self.subscribe(**contacts)

    @staticmethod
    def is_email(email):
        """
        Validation check for email
        :param email: email id
        :type email: str
        :rtype : bool
        """
        if re.match(r'^[\w\.]+@[a-zA-Z]+\.[a-zA-z\.]+$', email):
            return True
        else:
            return False

    @staticmethod
    def is_phone(phone):
        # ToDo: Currently validates for indian number. Need to generalise for international numbers
        """
        Validation check for mobile
        :param phone: phone number with or with country code
        :type phone: int
        :rtype : bool
        """
        if re.match(r'^\+?\d{0,4}[7-9]\d{9}$', str(phone)):
            return True
        else:
            return False

    @staticmethod
    def normalize_mobile(mobile):
        # ToDo: Currently normalizes to indian number. Need to generalise for international numbers
        """
        Normalise the given mobile number
        :rtype : bool
        :param mobile: phone number with or with country code
        :type mobile: int
        """
        mobile = str(mobile)
        if 9 < len(mobile) <= 15:
            if re.match(r'^\+?\d{0,4}[7-9]\d{9}$', mobile):
                return '+91{}'.format(re.search('([7-9]\d{9}$)', mobile).group())
            else:
                return None
        else:
            return None

    @classmethod
    def _send_request(cls, retries=3, **kwargs):
        """
        Prepares and send the request object. Retires specified number of times before raising an exception.
        :param retries: number of retires to be made before raising an exception
        :type retries: int
        :param kwargs: dictionary of parameters to be passed for request
        :rtype : request object
        """
        retries -= 1
        # print(request_type, kwargs)
        try:
            if not retries < 0:
                _request = request(**kwargs)
                if _request.status_code != 404 and 400 <= _request.status_code <= 599:
                    raise NudgespotException('Error raised by nudgespot.\nStatus Code: {}\nError: {}'.format(
                        _request.status_code,
                        _request.text
                    ))
                return _request
            raise NudgespotException('Unable to send request to Nudgespot')
        except requests.ConnectionError:
            # print('Connection error occurred. Retrying in 3 seconds')
            time.sleep(3)
            return cls._send_request(retries, **kwargs)
        except requests.Timeout:
            # print('Connection timed out. Retrying in 3 seconds')
            time.sleep(3)
            return cls._send_request(retries, **kwargs)
        except Exception as e:
            raise RuntimeError(e)


# TODO: This person can be insurance company too
def agent_event(person, sms_text):
    if settings.SEND_NUDGESPOT_EVENTS:
        event = 'manual_agent_sms'
        event_data = {'sms_text': sms_text, 'mobile': person.primary_phone_number.number,
                      'email': person.primary_email.address}
        response = Nudge.send_event(event, '%s_%s' % (settings.NUDGE_ID_PREFIX, str(person.id)), event_data)
        # TODO: Log this print nigga
        print ("%s event send to Nudgespot for customer_id %s with event_data %s and response was %s" % (
            event, str(person.id), json.dumps(event_data), response))
        return response
    return False


def _clean_name(name, communication='sms', length=20):
    if communication == 'sms':
        while len(name) > length:
            names = name.split(' ')
            if len(names) == 1:
                name = names[0][:length]
            else:
                name = ' '.join(names[:-1])
    return name.title()
