import time

from cfutils import redis_utils
import models as alfred_models


def push_ticket_data():
    all_users = range(3, 4)
    for user_id in all_users:
        for ticket in alfred_models.Ticket.objects.all():
            redis_utils.Publisher.publish_object('user_%s' % user_id, ticket)
            time.sleep(1)
