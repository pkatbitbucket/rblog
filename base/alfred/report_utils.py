import os
import datetime
import csv
from collections import OrderedDict

import models as alfred_models
from django.conf import settings

TICKET_HEADINGS_MAP = OrderedDict({
    "id": ["Ticket Id", "id"],
    "state": ["State", "current_state"],
    "created_on": ["Created Date", "created_on"],
    "ticket_due_date": ["Ticket Due Date", "end_date"],
    "ticket_owner": ["Ticket Owner", "owner"],
    "task_due_date": ["Task due Date", lambda x: x.tasks.filter(
        disposed_by_activities__isnull=True
    ).latest('id').scheduled_time if x.tasks.filter(disposed_by_activities__isnull=True) else ''],
    "task_assigned_to": ["Task Assigned", lambda x: x.tasks.filter(
        disposed_by_activities__isnull=True
    ).latest('id').assigned_to if x.tasks.filter(disposed_by_activities__isnull=True) else ''],
    "mobile": ["Mobile", lambda x: x.contacts.all()[0].primary_phone_number.number
               if x.contacts.all() and x.contacts.all()[0].primary_phone_number else ''],
    "email": ["Email", lambda x: x.contacts.all()[0].primary_email.address
              if x.contacts.all() and x.contacts.all()[0].primary_email else ''],
    "policy_number": ["Policy No", "policy"],
    "request_type": ["Ticket Category", "category"],
    "sub_request_types": ["Sub Request Types", lambda x: ', '.join(alfred_models.Request.objects.filter(
        ticket=x
    ).values_list('sub_request_type__name', flat=True))],
    "transaction_id": [
        "Transaction ID", lambda x: x.policy.transactions.all(
            )[0].transaction_id if x.policy and x.policy.transactions.all() else x.data.get('transaction_id')
    ],
    "priority": ["Priority", "priority"],
    "closure_date": ["Closure Date", lambda x: datetime.datetime.strftime(x.ticket_states.filter(
        state__name__in=['Close resolved', 'Close unresolved']).last().enter_time, "%d/%m/%Y %H:%M:%S"
    ) if x.ticket_states.filter(state__name__in=['Close resolved', 'Close unresolved']) else ""],
})

TICKET_HEADINGS_VALUES_LIST = [(v[0], k) for k, v in TICKET_HEADINGS_MAP.items()]


def generate_report(tickets_query, headings_list=TICKET_HEADINGS_MAP.keys()):

    HEADINGS = []
    report = [[]]
    for item in headings_list:
        HEADINGS.append(TICKET_HEADINGS_MAP[item])
        report[0].append(TICKET_HEADINGS_MAP[item][0])

    for ticket in tickets_query:
        row = []
        for item in HEADINGS:
            row.append(calculate_item(ticket, item[1]))
        report.append(row)

    return report


def export_list_to_csv(report_list):
    filename = "TICKETS_REPORT.csv"
    f = open(os.path.join(settings.UPLOAD_FOLDER, filename), 'w')
    csv_writer = csv.writer(f)
    for item in report_list:
        csv_writer.writerow([unicode(s).encode("utf-8") for s in item])
    f.close()
    return "%s%s" % (settings.UPLOAD_URL, filename)


def calculate_item(ticket, func):

    if(isinstance(func, type(lambda x: x))):
        return str(func(ticket))
    else:
        return str(getattr(ticket, func))
