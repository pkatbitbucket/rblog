var incoming_call = require('models/incoming-call')
var DISPOSITION_URL;
var CRTOBJECTID;

var store = require("models/store");

window.getExtensionInfo = function getExtensionInfo(){
    var ext_info = {
        name:  EXTENSION_NO + "_DefaultVR",
        phone: "1"
    };
    return ext_info;
}

window.customShowCrm = function customShowCrm(phone, additionalParams) {
    customerPhone = phone;
    additionalParams = additionalParams;
    var p;
    p = JSON.parse(additionalParams);
    CRTOBJECTID = p.crtObjectId;
    if(p.dstPhone){
        //window.incoming_call.openIncomingPopup(phone, additionalParams);
    }
    DISPOSITION_url = "http://192.168.2.100:8888/dacx/dispose?phone=" + phone + "&sessionId=" + p.sessionId + "&campaignId=" + p.campaignId + "&crtObjectId=" + CRTOBJECTID + "&userCrtObjectId=" + p.userCrtObjectId + "&dispositionCode=Bought";
}

function handleTransferToAQ(reason) {
}
function handleLogin(reason) {
}
function handleLogout(reason) {
}
function handleOnLoad() {
}
function handleLoginStatus(status) {
}
function handleForceLogin(reason) {
}
function handleSelectExtension(status) {
}
function handleModifyExtension(status) {
}
function handleSelectCampaign(reason) {
}
function handleAutoCallOn(status) {
}
function handleAutoCallOff(status) {
}
function handleReady(status) {
}
function handleBreak(status) {
}
function handleHangup(reason) {
}
function handleTransferToPhone(reason) {
}
function handleTransferInCall(reason) {
}
function handleTransferToIVR(reason) {
}
function handleTransferToUser(reason) {
}
function handleTransferToCampaign(reason) {
}
function handleConferWithPhone(reason) {
}
function handleConferWithTPV(reason) {
}
function handleConferWithUser(reason) {
}
function handleConferWithLocalIVR(reason) {
}

customIntegration = {};
customIntegration.showCrm = customShowCrm;
customIntegration.loginHandler = handleLogin;
customIntegration.forceLoginHandler = handleForceLogin;
customIntegration.logoutHandler = handleLogout;
customIntegration.onLoadHandler = handleOnLoad;
customIntegration.loginStatusHandler = handleLoginStatus;
customIntegration.selectExtensionHandler = handleSelectExtension;
customIntegration.modifyExtensionHandler = handleModifyExtension;
customIntegration.selectCampaignHandler = handleSelectCampaign;
customIntegration.autoCallOnHandler = handleAutoCallOn;
customIntegration.autoCallOffHandler = handleAutoCallOff;
customIntegration.readyHandler = handleReady;
customIntegration.breakHandler = handleBreak;
customIntegration.hangupHandler = handleHangup;
customIntegration.transferToPhoneHandler = handleTransferToPhone;
customIntegration.transferInCallHandler = handleTransferInCall;
customIntegration.transferToAQHandler = handleTransferToAQ;
customIntegration.transferToIVRHandler = handleTransferToIVR;
customIntegration.transferToUserHandler = handleTransferToUser;
customIntegration.transferToCampaignHandler = handleTransferToCampaign;
customIntegration.conferWithPhoneHandler = handleConferWithPhone;
customIntegration.conferWithTPVHandler = handleConferWithTPV;
customIntegration.conferWithUserHandler = handleConferWithUser;
customIntegration.conferWithLocalIVRHandler = handleConferWithLocalIVR;

registerCustomFunction("showCrm", customIntegration);
registerCustomFunction("loginHandler", customIntegration);
registerCustomFunction("logoutHandler", customIntegration);
registerCustomFunction("onLoadHandler", customIntegration);
registerCustomFunction("loginStatusHandler", customIntegration);
registerCustomFunction("forceLoginHandler", customIntegration);
registerCustomFunction("selectExtensionHandler", customIntegration);
registerCustomFunction("modifyExtensionHandler", customIntegration);
registerCustomFunction("selectCampaignHandler", customIntegration);
registerCustomFunction("autoCallOnHandler", customIntegration);
registerCustomFunction("autoCallOffHandler", customIntegration);
registerCustomFunction("readyHandler", customIntegration);
registerCustomFunction("breakHandler", customIntegration);
registerCustomFunction("hangupHandler", customIntegration);
registerCustomFunction("transferToPhoneHandler", customIntegration);
registerCustomFunction("transferInCallHandler", customIntegration);
registerCustomFunction("transferToAQHandler", customIntegration);
registerCustomFunction("transferToIVRHandler", customIntegration);
registerCustomFunction("transferToUserHandler", customIntegration);
registerCustomFunction("transferToCampaignHandler", customIntegration);
registerCustomFunction("conferWithPhoneHandler", customIntegration);
registerCustomFunction("conferWithTPVHandler", customIntegration);
registerCustomFunction("conferWithUserHandler", customIntegration);
registerCustomFunction("conferWithLocalIVRHandler", customIntegration);
