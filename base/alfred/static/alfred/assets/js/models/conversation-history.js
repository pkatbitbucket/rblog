var ko = require("knockout");
var urls = require("urls");

var ConvHistories = function(values) {
    var self = this;
    self.conv_histories = values.values;
    self.histories_per_page = ko.observable(5)
    self.start_index = ko.observable(0)
    self.end_index = ko.computed(function() {
        var index = parseInt(self.start_index()) + parseInt(self.histories_per_page());
        if(index > self.conv_histories().length){
            index = self.conv_histories().length
        }
        return index
    });
    self.show_conv_histories = ko.computed(function() {
        return self.conv_histories.slice(self.start_index(), self.end_index());
    });
    self.pagination_text = ko.computed(function() {
        return parseInt(parseInt(self.start_index()) + 1) + " - " + self.end_index() + " of " + self.conv_histories().length;
    });
    self.next_values = function(){
        next_start_index = parseInt(self.start_index()) + parseInt(self.histories_per_page());
        if(next_start_index <= self.conv_histories().length){
            self.start_index(next_start_index)
        }else{
            alert("No more Conversations to show")
        }
    }
    self.prev_values = function(){
        next_start_index = parseInt(self.start_index()) - parseInt(self.histories_per_page());
        if(next_start_index < 0){
            next_start_index = 0
        }
        self.start_index(next_start_index)
    }
}

ko.components.register('conversation-history-list', {
    viewModel: function(params) {
        var self = this;
        icon_map = {
            'sms': 'md md-textsms',
            'email': 'md md-email',
            'in_call': 'md md-call-received',
            'out_call': 'md md-call-made',
        }
        self.conv_history = params.value;
        self.conv_history.sub_dispositions_verbose = self.conv_history.sub_dispositions.join("|")
        self.conv_history.icon = icon_map[self.conv_history.type]
    },
    template: require("raw!templates/conversation-history-list.html")
});


module.exports = ConvHistories;
