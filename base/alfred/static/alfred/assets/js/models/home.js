var ko = require('knockout');
var store = require("models/store");
var urls = require('urls');


module.exports = function(){
    var self = this;
    self.iframeUrl = ko.observable(AMEYO_TOOLBAR_URL + "origin=" + window.location.origin);
    self.initialize = function(){
       setTimeout(function(){
            doLogin(USERNAME, USERNAME);
        }, 4000);
    }
    self.show_dashboard = function(){
        urls.hasher.setHash('dashboard');
    }
    self.initialize();
    self.logout = function(){
        doLogout();
    }
};
