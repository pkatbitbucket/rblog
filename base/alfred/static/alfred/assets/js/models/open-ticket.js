var ko = require('knockout')

var PolicySearchForm = function(route) {
    var self = this;
    self.ticket_id = ko.observable();
    if(route.ticket_id){
        self.ticket_id(route.ticket_id());
    }
    self.policies = ko.observableArray([]);
    self.policy_no = ko.observable();
    self.customer_mobile = ko.observable();
    self.customer_email = ko.observable();
    self.results_count = ko.observable('NA');

    self.set_show_create_ticket = function(policy_pk) {
        for(i=0;i<self.policies().length;i++) {
            self.policies()[i].show_create_ticket(false)
            if(self.policies()[i].pk == policy_pk)
                self.policies()[i].show_create_ticket(true)
        }
    };


    self.search_policy = function(){
         $.ajax({
            type: "POST",
            url: POLICY_SEARCH_URL,
            data: {
                'policy_no': self.policy_no(),
                'customer_mobile': self.customer_mobile(),
                'customer_email': self.customer_email(),
                'csrfmiddlewaretoken': CSRF_MIDDLEWARE_TOKEN
            },
            success: function(data) {
                responseData = JSON.parse(data);
                if(responseData.success) {
                    self.results_count(responseData.response.count);
                    self.policies(responseData.response.results)
                    for(i=0;i<self.policies().length;i++) {
                        if(!self.policies()[i].data){
                            self.policies()[i].data = {};
                        }
                        self.policies()[i].show_create_ticket = ko.observable(false)
                        self.policies()[i].open_create_ticket = (function(data) {
                            self.set_show_create_ticket(data.policy.pk)
                            return true
                        })
                    }
                }
            },
        });
    }

    self.force_create_policy = function(params){
        $.ajax({
            type: 'POST',
            url: POLICY_FORM_URL.replace('9090909090', '0'),
            data: {
                'policy_no': self.policy_no(),
                'csrfmiddlewaretoken': CSRF_MIDDLEWARE_TOKEN,
            },
            success: function(data){
                self.search_policy();
            }
        });
    };
}

module.exports = PolicySearchForm;
