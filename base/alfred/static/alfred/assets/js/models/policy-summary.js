var ko = require('knockout');

var PolicySummary = function(params) {
    var self = this;
    self.policy = params.value;
    self.ticket_id = ko.observable();
    if(params.ticket_id){
        self.ticket_id(params.ticket_id());
    }
    self.create_ticket_form = ko.observable();
    self.show_create_ticket_form = ko.observable(false);
    self.get_ticket_form = function(policy_id){
        $.ajax({
            type: 'GET',
            url: (function(){
                if(self.ticket_id()){
                    return UPDATE_TICKET_FORM_URL.replace('7979797979', self.ticket_id())
                }
                else{
                    return CREATE_TICKET_FORM_URL
                }
            })(),
            data: {
                'policy_id': policy_id,
            },
            success: function(data){
                self.create_ticket_form(data);
                self.show_create_ticket_form(true);
            }
        });
    }


};

module.exports = PolicySummary;
