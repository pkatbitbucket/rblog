var ko = require("knockout");
var urls = require("urls");
var store = require("models/store");

function SomeViewModel(route) {
    this.message = ko.observable('Welcome to Desktop!');
    this.tickets = store.ticketStore.tickets;
}

SomeViewModel.prototype.doSomething = function() {
    urls.hasher.setHash('about');
    this.message('You invoked doSomething() on the viewmodel.');
};

module.exports = SomeViewModel;
