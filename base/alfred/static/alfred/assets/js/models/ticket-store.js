var ko = require('knockout');
var store = require('models/store');
var Ticket = require('models/ticket');

function TicketStore() {
    var self = this;
    self.prefix = "ticket";
    self.local = "ticketStore"
    self.tickets = ko.observableArray([]);
    self.myticket_ids = ko.observableArray([]);
    self.add = function(data) {
        var existingTicket = ko.utils.arrayFirst(self.tickets(), function(item){
            return item.id == data.id;
        });
        if(existingTicket){
            existingTicket.update(data);
        }
        else{
            self.tickets.push(new Ticket(data));
        }
        self.myticket_ids.push(data.id);
    }
}
store.add(TicketStore);

module.exports = TicketStore;
