from django.test import TestCase
from django.test import Client

import json

from core.models import Permission
from core.models import User


class SimpleTest(TestCase):
    def setUp(self, *args, **kw):
        super(SimpleTest, self).setUp(*args, **kw)
        self.user = User.objects.create_superuser(
            created_by=None,
            email="foo@cfox.com",
            password="foo",
            first_name="foo"
        )
        self.c = Client()

    def login(self):
        self.c.login(username=self.user.email, password="foo")

    def _test_json(self, url, get=None, post=None, data=None, status=200):
        if post is not None:
            resp = self.c.post(
                url, content_type='application/json', data=json.dumps(post)
            )
        else:
            resp = self.c.get(url, data=post)

        self.assertEqual(resp.status_code, status)
        rjson = json.loads(str(resp.content))
        if data is not None:
            self.assertEqual(rjson, data)
        return rjson

    def test_permissions_api(self):
        self.assertEqual(Permission.objects.count(), 0)
        self._test_json("/api/permission/")
        self.login()
        data = self._test_json(
            "/api/permission/", post={"pk": None, "name": "bar"},
            status=201, data=None
        )
        self.assertEqual(data["name"], "bar")
        self.assertEqual(data["__unicode__"], "bar")
        perm = Permission.objects.get()
        self.assertEqual(perm.name, "bar")
