from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
import forms as alfred_forms
import views as alfred_views
from cfutils.forms import fhurl


urlpatterns = [
    url(r'^data-push/transaction/$', alfred_views.transaction_data_push, name='transaction_data_push'),
    url(r'^form-options/create-ticket/$', alfred_views.form_options_create_ticket, name='form_options_create_ticket'),

    fhurl.fhurl(r'^ticket/create/(?P<policy_id>[\d-]+)/$',
                alfred_forms.PolicyForm, name='policy_form', require_login=True, json=True),

    fhurl.fhurl(r'^tickets/$', alfred_forms.CreateTicketForm, template='alfred/forms/create_ticket_form.html',
                name='create_ticket', post_json=True, decorator=csrf_exempt),

    fhurl.fhurl(r'^tickets/(?P<ticket_id>[\d-]+)/$', alfred_forms.CreateTicketForm,
                template='alfred/forms/create_ticket_form.html',
                name='update_ticket', post_json=True, decorator=csrf_exempt),

    fhurl.fhurl(r'^ajax/ticket/notes/(?P<ticket_id>[\d-]+)/(?P<key>[\w\.-]+)/$', alfred_forms.NotesForm,
                name='notes_form', json=True),

    fhurl.fhurl(r'^ajax/ticket/sms/(?P<ticket_id>[\d-]+)/$', alfred_forms.SendSMSForm, name='send_sms', json=True),

    url(r'^search_policy/$', alfred_views.get_policy_search_form, name='get_policy_search_form'),
    fhurl.fhurl(r'^ajax/search_policy/$', alfred_forms.PolicySearchForm, name='policy_search', json=True),

    url(r'^search_ticket/$', alfred_views.get_ticket_search_form, name='get_ticket_search_form'),
    fhurl.fhurl(r'^ajax/search_ticket/$', alfred_forms.TicketSearchForm, name='ticket_search', json=True),

    fhurl.fhurl(r'^ajax/task-form/(?P<ticket_id>[\d-]+)/$', alfred_forms.TaskForm, template="", json=True, name="task_form"),

    fhurl.fhurl(
        r'^ajax/ticket-status-form/(?P<ticket_id>[\d-]+)/$',
        alfred_forms.TicketStatusForm,
        json=True,
        name="ticket_status_form"
    ),

    fhurl.fhurl(
        r'^ajax/dispositon-form/(?P<ticket_id>[\d-]+)/$',
        alfred_forms.DispositionForm,
        json=True,
        name="disposition_form"
    ),

    # TODO: Standardize uses for underscore and dashes in the complete project
    url(r'^ajax/get-ticket-details/(?P<ticket_id>[\d-]+)/$', alfred_views.get_ticket_details, name='ticket-detail'),
    url(
        r'^ajax/get-conversation-history/(?P<ticket_id>[\d-]+)/$',
        alfred_views.get_conversation_history,
        name='conversation_history'
    ),
    url(
        r'^ajax/save-state-form/(?P<form_name>[\w-]+)/(?P<ticket_id>[\d-]+)/$',
        alfred_views.save_state_form,
        name='save_state_form'
    ),
    url(r'^ajax/s3-download-document/(?P<doc_id>[\d-]+)/$', alfred_views.s3_download_document, name='s3_download_document'),
    url(
        r'^ajax/get-incoming-ticket-call-popup/$',
        alfred_views.get_incoming_ticket_call_popup,
        name='get_incoming_ticket_call_popup'
    ),
    url(r'^ajax/get-incoming-ticket/$', alfred_views.get_incoming_ticket, name='get_incoming_ticket'),
]
