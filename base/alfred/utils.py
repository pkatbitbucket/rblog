from django.db.models import Q
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist, ValidationError

import requests
import json
import datetime
import hmac
import base64
from hashlib import sha1
from dateutil import parser as dateutil_parser

from crm import utils as crm_utils
import fake_machine.fake_data as faker
from core import models as core_models
import models as alfred_models
import leads.models as leads_models
import nudgespot


def post_transaction_data(data_type):
    """
    compulsory parameters:
        'data_type'
        options for 'data_type': 'offline_form', 'cancellation_flow'
    """
    fake_data = faker.get_fake_data(data_type)
    print fake_data
    data = {
        'success': True,
        'data': json.dumps(fake_data),
    }
    response = requests.post(url=settings.CREATE_TICKET_API_URL, data=data)
    return response


def transaction_form_data(tdata=None, test=True):
    """
    compulsory parameters:
        'tdata'
        raw transaction data from product which will be cleaned for create_ticket use
    """
    if test:
        tdata = faker.get_fake_data('policy_approve_flow')
    form_data = {
        'priority': alfred_models.TicketPriority.objects.get(name='NORMAL'),
        'request_type': alfred_models.TicketCategory.objects.get(name='POLICY_APPROVAL'),
        'sub_request_type': 'REIMBURSEMENT',

        'product_type': tdata['product_type'],
        'first_name': tdata['proposer_first_name'],
        'last_name': tdata['proposer_last_name'],
        'mobile': tdata['mobile'],
        'email': tdata['email'],
        'policy_no': tdata['policy_no'],
    }

    return form_data


def get_state_form(machine, state, ticket_data):
    sfrs = alfred_models.StateFormRouter.objects.filter(state=state, machine=machine)
    print sfrs

    param_dict = {}
    for sfr in sfrs:
        sfr_query = sfr.query or {}
        for key, value in sfr_query.items():
            param_dict.update({key: ticket_data.get(key)})
        if param_dict == sfr_query:
            return sfr

    return None


class PolicySearch(object):

    def __init__(self, search_data):
        self.data = search_data

    def search_policy_no(self, q):
        if self.data.get('policy_no'):
            q = q & Q(policy_no__endswith=self.data['policy_no'])
        return q

    def search_transaction_id(self, q):
        if self.data.get('transaction_id'):
            q = q & Q(transaction__transaction_id=self.data['transaction_id'])
        return q

    def search_customer_email(self, q):
        if self.data.get('customer_email'):
            q = q & Q(contacts__emails__address=self.data['customer_email'])
        return q

    def search_customer_mobile(self, q):
        if self.data.get('customer_mobile'):
            q = q & Q(contacts__phones__number=self.data['customer_mobile'])
        return q

    def search_customer_name(self, q):
        if self.data.get('customer_name'):
            fn_query = Q(contacts__first_name__icontains=self.data['customer_name'])
            mn_query = Q(contacts__middle_name__icontains=self.data['customer_name'])
            ln_query = Q(contacts__last_name__icontains=self.data['customer_name'])
            name_query = fn_query | mn_query | ln_query
            q = q & name_query
        return q


def policy_search(search_data):
    q = Q()
    res = []
    policy_searcher = PolicySearch(search_data)

    for key in search_data.iterkeys():
        policy_dispatcher = getattr(policy_searcher, 'search_%s' % key, False)
        if policy_dispatcher:
            q = policy_dispatcher(q)

    if q and len(q):
        res = alfred_models.Policy.objects.filter(q).distinct()
        print res.query

    return res


class TicketSearch(object):

    def __init__(self, search_data):
        self.data = search_data

    def search_id(self, q):
        if self.data.get('id'):
            q = q & Q(id=self.data['id'])
        return q

    def search_customer_email(self, q):
        if self.data.get('customer_email'):
            q = q & Q(contacts__emails__address=self.data['customer_email'])
        return q

    def search_customer_mobile(self, q):
        if self.data.get('customer_mobile'):
            q = q & Q(contacts__phones__number=self.data['customer_mobile'])
        return q

    def search_customer_name(self, q):
        if self.data.get('customer_name'):
            fn_query = Q(contacts__first_name__icontains=self.data['customer_name'])
            mn_query = Q(contacts__middle_name__icontains=self.data['customer_name'])
            ln_query = Q(contacts__last_name__icontains=self.data['customer_name'])
            name_query = fn_query | mn_query | ln_query
            q = q & name_query
        return q

    def search_owner(self, q):
        if self.data.get('owner'):
            q = q & Q(owner=self.data['owner'])
        return q

    def search_status(self, q):
        if self.data.get('status'):
            q = q & Q(status=self.data['status'])
        return q

    def search_priority(self, q):
        if self.data.get('priority'):
            q = q & Q(priority=self.data['priority'])
        return q


def ticket_search(search_data):
    q = Q()
    res = []
    ticket_searcher = TicketSearch(search_data)

    for key in search_data.iterkeys():
        ticket_dispatcher = getattr(ticket_searcher, 'search_%s' % key, False)
        if ticket_dispatcher:
            q = ticket_dispatcher(q)

    if q and len(q):
        res = alfred_models.Ticket.objects.filter(q).distinct()
        print res.query

    return res


def get_ticket_owner(policy, request_user):

    policy_rm = policy.relationship_manager
    if policy_rm:
        user = policy_rm
    else:
        rt_queue = core_models.Queue.objects.get(name='Resolution Team')
        if request_user and request_user in rt_queue.users.all():
            user = request_user
        else:
            user = crm_utils.random_list_item(rt_queue.users.all())

    return user


def attach_documents_to_ticket(ticket, edocuments, sdocuments, request_user):

    edocuments = json.loads(edocuments)
    sdocuments = json.loads(sdocuments)
    ticket.document_set.clear()
    for sd in edocuments:
        ticket.document_set.add(alfred_models.Document.objects.get(id=sd['pk']))
    document_category_ids = list(
        alfred_models.TicketDocumentCategory.objects.filter(ticket=ticket).values_list('category', flat=True)
    )
    for doc in sdocuments:
        category_id = doc['category']['pk']
        if doc.get('fileDetails'):
            d = alfred_models.Document(
                key=doc['fileDetails']['data']['file'],
                policy=ticket.policy,
                category_id=category_id
            )
            d.save()
            d.ticket.add(ticket)

        elif doc['category']['pk'] not in document_category_ids:
            tdc = alfred_models.TicketDocumentCategory(
                ticket=ticket,
                category_id=category_id
            )
            tdc.save(request_user)

        if category_id in document_category_ids:
            document_category_ids.remove(category_id)

    if document_category_ids:
        extra_tdcs = alfred_models.TicketDocumentCategory.objects.filter(id__in=document_category_ids)
        extra_tdcs.delete()


def get_auth_headers(path):
    request_time = datetime.datetime.utcnow()
    string_to_sign = "%s %s" % (path, request_time.strftime('%Y-%m-%dT%H:%M:%SZ'))
    h = hmac.HMAC(settings.PRODUCT_API_AUTHENTICATION_KEY, string_to_sign, sha1)
    signature = base64.b64encode(h.digest())
    headers = {
        'Authorization': 'CVFX ' + settings.PRODUCT_API_AUTHENTICATION_ID + ':' + signature,
        'Date': request_time.strftime('%Y-%m-%dT%H:%M:%SZ'),
    }
    return headers


# TODO: Vishesh: Implement this internal and external policy map as API

"""
internal_policy_map has relation of Key (Email) with dict containing variable name (email) and where to store (data)
"""
internal_policy_map = {
    'Email': {'name': 'email', 'where': 'data'},
    'End Date': {'name': 'end_date', 'where': 'data'},
    'First Name': {'name': 'proposer_first_name', 'where': 'data'},
    'Insurance Product Name': {'name': 'insurance_product', 'where': 'data'},
    'Insured Members': {'name': 'insured_name', 'where': 'data'},
    'Last Name': {'name': 'proposer_last_name', 'where': 'data'},
    'Mobile Number': {'name': 'mobile', 'where': 'data'},
    'Policy Number': {'name': 'policy_no', 'where': 'object'},
    # 'Policy Start Date': {'name': 'policy_previous', 'where': 'data'},
    'Policy Status': {'name': 'status', 'where': 'object'},
    # 'Policy Type': {'name': 'policy_type', 'where': 'data'},
    # 'Premium Paid': {'name': 'premium_paid', 'where': 'data'},
    'Product Category': {'name': 'product_type', 'where': 'data'},
    # 'Transaction ID': {'name': 'transaction_id', 'where': 'data'},
    'Insurance Company': {'name': 'insurance_company', 'where': 'data'},
    'Payment Date': {'name': 'payment_date', 'where': 'data'},
    'Policy Document': {'name': 'policy_document', 'where': 'data'},
}

"""
external_policy_map will come with the API request and will contain a dict that has relation of
Key (Email) with Key of dict of API request (policy_holder_email)
"""
external_policy_map = {
    'insurer_title': 'Insurance Company',
    'payment_on': 'Payment Date',
    'policy_category': 'Product Category',
    'policy_covers': 'Insured Members',
    'policy_document': 'Policy URL',
    'policy_end_date': 'End Date',
    'policy_holder_email': 'Email',
    'policy_holder_firstname': 'First Name',
    'policy_holder_lastname': 'Last Name',
    'policy_holder_mobile': 'Mobile Number',
    'policy_name': 'Insurance Product Name',
    'policy_number': 'Policy Number',
    'policy_previous': 'Previous Policy',
    'policy_start_date': 'Policy Start Date',
    'policy_status': 'Policy Status',
    'policy_type': 'Policy Type',
    'transaction_id': 'Transaction ID',
    'policy_document': 'Policy Document',
}


# TODO: Vishesh: Log this print shit once logging is ready
def create_policy_from_maps(p_data, transaction):
    try:
        policy = alfred_models.Policy.objects.get(transactions=transaction)
        policy_no = policy.policy_no
    except ObjectDoesNotExist:
        val = p_data.get('policy_number', '').strip()
        policy_no = val if val else 'ALFRED-%s' % transaction.id
        policy = alfred_models.Policy(policy_no=policy_no)

    data = {"extra_info": {}, "origin": "product_search"}
    for p_key, p_val in p_data.items():
        e_key = external_policy_map.get(p_key, None)
        mapping_rule = internal_policy_map.get(e_key, None)
        if mapping_rule and mapping_rule['where'] == 'data':
            data.update({mapping_rule['name']: p_val})
            # print ">>>>entered '%s' in '%s' as '%s'" % (p_val, mapping_rule['where'], mapping_rule['name'])
        elif mapping_rule and mapping_rule['where'] == 'object' and not getattr(policy, mapping_rule['name']):
            setattr(policy, mapping_rule['name'], p_val)
            # print ">>>>entered '%s' in '%s' as '%s'" % (p_val, mapping_rule['where'], mapping_rule['name'])
        else:
            data["extra_info"].update({p_key: p_val})
            # print ">>>>entered '%s' in data.extra_info as '%s'" % (p_val, p_key)
    policy.data = data
    policy.policy_no = policy_no
    return policy


def query_on_product(cleaned_data, search_user):
    query = ''
    for query_name, query_text in cleaned_data.items():
        if query_text:
            query = query_text
            break

    query_data = {"q": query}
    query_domain = settings.PRODUCT_DOMAIN
    path = settings.SEARCH_TRANS_PATH
    headers = get_auth_headers(path)
    response = requests.post(url=query_domain + path, data=json.dumps(query_data), headers=headers)
    if not response.status_code == 200:
        raise ValidationError('Response Code not 200 on hitting Product Search URL', code='invalid_response_code')

    results = response.json().get("data")
    if not results:
        return []

    qs_policies = []
    for key, values in results.items():
        for value in values:
            try:
                leads_models.Transaction.objects.get(transaction_id=value.get('transaction_id'))
            except ObjectDoesNotExist:
                if value.get('policy_status', '') == 'COMPLETED':
                    email = core_models.Email.objects.filter(address=value['policy_holder_email'])
                    phone = core_models.Phone.objects.filter(number=value['policy_holder_mobile'])

                    if not email and not phone:
                        person = core_models.Person(
                            first_name=value['policy_holder_firstname'],
                            last_name=value['policy_holder_lastname']
                        )
                        person.save(search_user)
                        email = [core_models.Email(person=person, address=value['policy_holder_email'], is_primary=True)]
                        phone = [core_models.Phone(person=person, number=value['policy_holder_mobile'], is_primary=True)]
                    if email:
                        email = email[0]
                        person = email.person
                    if phone:
                        phone = phone[0]
                        person = phone.person

                    if not email:
                        email = core_models.Email(person=person, address=value['policy_holder_email'], is_primary=True)
                    if not phone:
                        phone = core_models.Phone(person=person, number=value['policy_holder_mobile'], is_primary=True)

                    email.save(search_user)
                    phone.save(search_user)

                    transaction = leads_models.Transaction(transaction_id=value.get('transaction_id'))
                    transaction.save(search_user)
                    policy = create_policy_from_maps(value, transaction)
                    policy.save()
                    policy.contacts.add(person)
                    transaction.policies.add(policy)
                    qs_policies.append(policy)

    return qs_policies


def get_params_for_text_message(ticket):
    customer = ticket.contacts.all()[0]
    appointment_time = ticket.data.get('appointment_time')
    if appointment_time:
        appointment_time = dateutil_parser.parse(appointment_time).strftime("%d/%m/%Y %H:%M")
    params = {
        'customer_name': customer.full_name,
        'request_type': ticket.category.name,
        'request_no': ticket.id,
        'customer_mobile': customer.primary_phone_number.number,
        'callback_number': ticket.owner.profile.phone,
        'appointment_time': appointment_time,
        'appointment_location': ticket.data.get('appointment_location'),
        'pending_amount': ticket.data.get('pending_amount'),
    }
    for k, v in params.items():
        params[k] = '' if not v else str(v)
    return params


def send_message(ticket, comm_template, user, params=None):
    success = False
    if not params:
        params = get_params_for_text_message(ticket)

    raw_text = comm_template.template.format(**params)
    if user == core_models.User.get_system_user():
        pass
    else:
        success = nudgespot.agent_event(ticket.contacts.all()[0], raw_text)

    communication = core_models.Communication(template=comm_template, raw_text=raw_text, params=params)
    communication.save(user)  # TODO: Get this from the activity

    return raw_text, success
