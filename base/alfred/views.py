import json
import datetime

from django.http import JsonResponse
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext
from django.template.loader import render_to_string

from django.conf import settings
import utils as alfred_utils
import forms as alfred_forms
from core import models as core_models
from statemachine import models as sm_models
from cfutils.aws import s3_upload
import models as alfred_models


@csrf_exempt
@require_POST
def transaction_data_push(request):

    form = alfred_forms.CreateTicketForm(None, json.loads(request.POST['data']))

    if form.is_valid():
        response = form.save()
    else:
        response = {'success': False, 'errors': form.errors}

    return JsonResponse(response)


@csrf_exempt
def form_options_create_ticket(request):
    request_type_id = request.GET.get('request_type_id', None)
    if request_type_id:
        request_type = alfred_models.TicketCategory.objects.get(pk=request_type_id)
        sub_requests = list(request_type.sub_request_types.values_list('id', 'verbose'))
        response = {'success': True, 'sub_requests': sub_requests}
    else:
        response = {'success': False, 'sub_requests': []}
        return JsonResponse(response)
    return JsonResponse(response)


@csrf_exempt
# @require_POST
def prod_trans_data_push(request):
    trans_form_data = alfred_utils.transaction_form_data()
    trans_form_data.pop("last_name")
    prod_trans_form = alfred_forms.ProductTransactionData(trans_form_data['policy_no'], trans_form_data)
    prod_trans_form.is_valid()
    errors = prod_trans_form.errors
    errors.pop('priority')
    errors.pop('request_type')
    if errors:
        return JsonResponse({'errors': errors, 'success': False})

    prod_trans_form.save()
    return JsonResponse({"success": True, "response": prod_trans_form.save()})


@login_required
def get_policy_search_form(request):
    policy_search_form = alfred_forms.PolicySearchForm(request)

    policy_search_form_template = render_to_string('alfred/forms/policy_search_form.html', {
        'policy_search_form': policy_search_form
    }, context_instance=RequestContext(request))

    return JsonResponse({
        'policy_search_form': policy_search_form_template,
    })


@login_required
def get_ticket_search_form(request):
    ticket_search_form = alfred_forms.TicketSearchForm(request)

    ticket_search_form_template = render_to_string('alfred/forms/ticket_search_form.html', {
        'ticket_search_form': ticket_search_form
    }, context_instance=RequestContext(request))

    return JsonResponse({
        'ticket_search_form': ticket_search_form_template,
    })


@login_required
def save_state_form(request, form_name, ticket_id):

    ticket = alfred_models.Ticket.objects.get(id=ticket_id)

    sfr = alfred_utils.get_state_form(ticket.machine, ticket.current_state, ticket.data)
    if sfr:
        state_form = getattr(alfred_forms, sfr.name)(ticket_id, json.loads(request.POST['state_form_data']))

        if state_form.is_valid():
            state_form.save(request.user)
        else:
            return JsonResponse({"errors": state_form.errors, "success": False})

    disposition_form = alfred_forms.DispositionForm(ticket_id, json.loads(request.POST['disposition_data']))

    ticket = alfred_models.Ticket.objects.get(id=ticket_id)
    if disposition_form.is_valid():
        alfred_utils.attach_documents_to_ticket(
            ticket,
            request.POST['existing_documents'],
            request.POST['suggested_documents'],
            request.user
        )
        disposition_form.save(request.user, ticket, json.loads(request.POST['state_form_data']))
        task_form = alfred_forms.TaskForm(ticket_id)
        task_form.init(ticket_id)
        task_form_template = render_to_string('alfred/forms/task_form.html', {
            'form': task_form
        }, context_instance=RequestContext(request))

        return JsonResponse({
            'ticket_id': ticket_id,
            'task_form': task_form_template,
            'success': True
        })
    else:
        return JsonResponse({"errors": disposition_form.errors, "success": False})


@login_required
def get_ticket_details(request, ticket_id):
    ticket = alfred_models.Ticket.objects.get(id=ticket_id)
    sfr = alfred_utils.get_state_form(ticket.machine, ticket.current_state, ticket.data)
    state_form_template, state_form_name = None, None
    if sfr:
        state_form_template, state_form_name = sfr.template, sfr.name

        state_form = getattr(alfred_forms, state_form_name)(ticket_id)
        state_form.init(ticket)

        state_form_template = render_to_string('alfred/forms/%s.html' % (state_form_template), {
            'form_name': state_form_name,
            'form': state_form
        }, context_instance=RequestContext(request))

    dispositions = [
        sd.disposition.djson()
        for sd in sm_models.StateDisposition.objects.filter(
            state=ticket.current_state,
            machine=ticket.machine
        )
    ]

    ticket_status_form = alfred_forms.TicketStatusForm(request)
    ticket_status_form.init(ticket_id)

    ticket_status_form_template = render_to_string('alfred/forms/ticket_status_form.html', {
        'form': ticket_status_form
    }, context_instance=RequestContext(request))

    document_categories = [tdc.djson() for tdc in alfred_models.DocumentCategory.objects.filter(request_type=ticket.category)]
    suggested_documents = [tdc.djson() for tdc in alfred_models.TicketDocumentCategory.objects.filter(ticket=ticket)]

    return JsonResponse({
        'my_notes': ticket.data.get('my_notes_' + request.user.username),
        'chat_notes': ticket.data.get('chat_notes') if ticket.data.get('chat_notes') else '[]',
        'ticket': ticket.mini_json(),
        'ticket_status_form': ticket_status_form_template,
        'dispositions': dispositions,
        'state_form': state_form_template,
        'state_form_name': state_form_name,
        'document_categories': document_categories,
        'suggested_documents': suggested_documents,
        'existing_documents': [d.djson() for d in alfred_models.Document.objects.filter(ticket=ticket)],
        'sms_templates': {
            'template_objs': [ct.djson() for ct in core_models.CommunicationTemplate.objects.filter(category='SMS')],
            'params_map': alfred_utils.get_params_for_text_message(ticket),
        }
    })


@login_required
def s3_download_document(request, doc_id):
    doc = alfred_models.Document.objects.get(id=doc_id)
    document_url = None
    if settings.USE_AMAZON_S3:
        s3uploader = s3_upload.S3Upload(settings.REQUEST_BUCKET)
        document_url = s3uploader.url(doc.key)

    return JsonResponse({
        'success': True,
        'document_id': doc_id,
        'document_url': document_url,
    })


def get_incoming_ticket(request):
    ticket = alfred_models.Ticket.objects.get(id=request.GET['ticket_id'])
    task = core_models.Task(
        category=core_models.TaskCategory.objects.get(id=41),  # Pick Incoming Call
        assigned_to=request.user,
        scheduled_time=datetime.datetime.now(),
        pushed_time=datetime.datetime.now(),
        extra=request.GET,
    )
    task.save(None)
    ticket.tasks.add(task)

    return JsonResponse({
        'success': True,
        'task_id': task.id,
    })


def get_conversation_history(request, ticket_id):

    if not alfred_models.Ticket.objects.filter(pk=ticket_id):
        return JsonResponse({
            'success': False,
            'error': 'Ticket ID %s not found' % ticket_id,
        })
    activities = [activity.djson() for activity in alfred_models.Ticket.objects.get(pk=ticket_id).activities.all()]
    """activities = [
        {'agent_name': 'advisor1', 'sub_dispositions': ['lulz', 'lolz'], 'disposition': 'chori complete',
         'type': 'in_call', 'snippet': 'just for the lulzz and lolzzz', 'created_on': '2015-09-10 15:13',
         'contact': '9819373623'},
        {'agent_name': 'advisor1', 'sub_dispositions': ['lolzzz', 'lulzz'], 'disposition': 'chori complete',
         'type': 'email', 'snippet': 'really it\'s just for lolzz and lulzzz', 'created_on': '2015-09-10 15:13',
         'contact': 'nigga@ghetto.com'},
        {'agent_name': 'advisor2', 'sub_dispositions': ['suppp', 'bruv!'], 'disposition': 'chori complete',
         'type': 'out_call', 'snippet': 'wasssup bruv???', 'created_on': '2015-09-10 15:13',
         'contact': '9819373623'},
        {'agent_name': 'advisor1', 'sub_dispositions': ['my', 'nigga'], 'disposition': 'chori complete',
         'type': 'email', 'snippet': 'how you doing my niggah', 'created_on': '2015-09-10 15:13',
         'contact': 'nigga@ghetto.com'},
        {'agent_name': 'advisor2', 'sub_dispositions': ['huhuhuhuhu', 'hahahaha'], 'disposition': 'chori complete',
         'type': 'sms', 'snippet': 'huhuhuhuh oyeeeeee hahahahaha', 'created_on': '2015-09-10 15:13',
         'contact': '9819373623'},
    ]"""
    return JsonResponse({
        'success': True,
        'activities': activities,
    })


def get_incoming_ticket_call_popup(request):
    phone = core_models.Phone.objects.filter(number=request.GET['phone'])
    tickets = []
    if phone:
        tickets = alfred_models.Ticket.objects.filter(contacts=phone[0].person)

    return JsonResponse({
        'tickets': [ticket.mini_json() for ticket in tickets],
        'success': True,
    })
