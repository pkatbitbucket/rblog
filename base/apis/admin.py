from django.contrib import admin
from apis.models import ThirdParty

admin.site.register(ThirdParty, admin.ModelAdmin)
