from django.conf.urls import url

from apis.auth import views

urlpatterns = [
    url(r'^login/$', views.login_view, name='login_api'),
]
