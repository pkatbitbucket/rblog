from django.contrib.auth import authenticate, login
from django.views.decorators.http import require_POST
from django import http
from motor_product import parser_utils
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
@require_POST
def login_view(request):
    return_data = {
        "success": False,
        "message": "Invalid username/password"
    }
    post_parameters = parser_utils.normalize_request_dict(request)
    u = authenticate(username=post_parameters.get('email'), password=post_parameters.get('password'))
    if u:
        login(request, u)
    else:
        return http.JsonResponse(return_data, status=400)
    return_data['success'] = True
    return_data.pop('message')
    return http.JsonResponse(return_data)

