import binascii
import hmac
from hashlib import sha1
import base64
import json
from datetime import datetime

from django.http import HttpResponse
from oauth2_provider.models import Application
from django.core.cache import cache

import putils
from utils import api_logger, log_error

API_AUTH_CACHE_TIMEOUT = 3600
API_AUTH_TOKEN_TIMEOUT = 120


def authenticate_api(view):
    def inner(request, *args, **kwargs):
        try:
            js_api = kwargs.get('js_api', False)
            if not js_api:
                auth_header = request.META['HTTP_AUTHORIZATION']
            else:
                del kwargs['js_api']
                auth_header = request.META['HTTP_CFAUTHORIZATION']

            auth_header_parts = auth_header.split(" ")

            if auth_header_parts[0] == "CVFX":
                auth_header_parts = auth_header_parts[1].split(":")
                api_id = auth_header_parts[0]
                api_signature = ":".join(auth_header_parts[1:])

                if not js_api:
                    request_time_str = request.META['HTTP_DATE']
                else:
                    request_time_str = request.META['HTTP_CFDATE']

                request_path = request.path

                string_to_sign = "%s %s" % (request_path, request_time_str)

                # Caching to avoid frequent db queries
                auth_key = cache.get('API_AUTH_' + api_id)
                auth_name = cache.get('API_AUTH_NAME_' + api_id)
                if auth_key == None or auth_name == None:
                    tp = Application.objects.get(client_id=api_id)
                    auth_key = tp.client_secret
                    auth_name = tp.name
                    cache.set('API_AUTH_NAME_' + api_id,
                              auth_name, API_AUTH_CACHE_TIMEOUT)
                    cache.set('API_AUTH_' + api_id, auth_key,
                              API_AUTH_CACHE_TIMEOUT)

                h = hmac.HMAC(str(auth_key), string_to_sign, sha1)
                signature = base64.b64encode(h.digest())

                if signature == api_signature:
                    request_time = datetime.strptime(
                        request_time_str, '%Y-%m-%dT%H:%M:%SZ')
                    # Time check
                    if abs((datetime.utcnow() - request_time).total_seconds()) < API_AUTH_TOKEN_TIMEOUT:
                        try:
                            request.META['THIRD_PARTY'] = auth_name
                            resp = view(request, *args, **kwargs)
                            resp['Content-Type'] = 'application/json'
                            resp.status_code = 200
                        except:
                            log_error(api_logger, request=request)
                            resp = HttpResponse(json.dumps({'success': False,
                                                            'errorCode': 500,
                                                            'errorMessage': 'Unknown server error.',
                                                            'errorItems': [],
                                                            'data': {}}), content_type='application/json')

                        if js_api:
                            key = 'CVFX' + signature
                            key = key.encode('hex')
                            content = resp.content
                            iv, encrypted = putils.Cryptor.encrypt(
                                content, key)
                            ctext = binascii.b2a_base64(encrypted).rstrip()
                            encrypted_content = "%s%s" % (ctext, iv)
                            resp.content = encrypted_content
                        return resp
        except:
            pass
        return HttpResponse(json.dumps({'success': False, 'errorCode': 401, 'errorMessage': 'Proper authentication required.', 'errorItems': [], 'data': {}}), content_type='application/json')

    return inner


def js_api(view):
    def inner(request, *args, **kwargs):
        if request.META.get('HTTP_CFJSAPI', None) == 'true':
            kwargs['js_api'] = True

        resp = view(request, *args, **kwargs)
        return resp

    return inner


def apilogger(view):
    def inner(request, *args, **kwargs):
        resp = view(request, *args, **kwargs)

        get_parameters = request.GET if request.GET else None
        post_parameters = None
        body = None

        if request.method == 'POST':
            post_parameters = request.POST
            if not post_parameters and request.body:
                body = request.body

        log_content = '%s %s %s, post_parameters: %s, get_parameters: %s, body: %s, response: %s' % (
                      resp.status_code,
                      request.path,
                      request.META.get('THIRD_PARTY', '-'),
                      post_parameters,
                      get_parameters,
                      body,
                      resp.content)

        api_logger.info(log_content)
        return resp

    return inner
