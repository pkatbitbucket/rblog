from django.db import models
from django_extensions.db.fields import UUIDField
from jsonfield import JSONField

# Create your models here.


class ThirdParty(models.Model):
    api_id = UUIDField()
    auth_key = models.CharField(max_length=70)
    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=70)
    created_on = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s (API ID: %s)" % (self.title, self.api_id)
