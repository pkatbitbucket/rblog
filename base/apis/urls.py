from django.conf.urls import include, url

from oauth2_provider import views as oauth_views

from . import views

application_patterns = [
    url(r'^$', oauth_views.ApplicationList.as_view(), name="list"),
    url(r'^register/$', oauth_views.ApplicationRegistration.as_view(), name="register"),
    url(r'^(?P<pk>\d+)/$', oauth_views.ApplicationDetail.as_view(), name="detail"),
    url(r'^(?P<pk>\d+)/delete/$', oauth_views.ApplicationDelete.as_view(), name="delete"),
    url(r'^(?P<pk>\d+)/update/$', oauth_views.ApplicationUpdate.as_view(), name="update"),
]

urlpatterns = [
    url(r'^$', views.api_home, name='api_home'),
    url(r'^auth/coverfox/token', views.TokenView.as_view(), name='coverfox_auth'),
    url(r'^auth/', include('apis.auth.urls')),
    url(r'^motor/(?P<vehicle_type>[\w]+)/', include('motor_product.apis.urls')),
    url(r'^v1/', include('apis.v1.urls')),
    url(r'^applications/', include(application_patterns, namespace='oauth2_provider')),
    url(r'^user/', include('account_manager.apis.urls')),
    url(r'^health/', include('health_product.apis.urls')),
    url(r'^send-grid/$', views.parse_send_grid_email, name='send_grid_webhook'),
    url(r'^policy-email/$', views.parse_policy_email, name='policy_email_webhook'),
    url(r'^save-event/$', views.save_lms_event, name='save_lms_event'),
    url(r'^get-current-time/$', views.get_current_time, name='get_current_time'),
]
