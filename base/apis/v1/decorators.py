from functools import wraps

from django.http import HttpResponseForbidden
from django.utils.decorators import available_attrs

from oauthlib.oauth2 import Server
from oauth2_provider.oauth2_validators import OAuth2Validator
from oauth2_provider.oauth2_backends import OAuthLibCore


def protected_resource(scopes=None, validator_cls=OAuth2Validator, server_cls=Server):
    """
    Decorator to protect views by providing OAuth2 authentication out of the box, optionally with
    scope handling.
        @protected_resource()
        def my_view(request):
            # An access token is required to get here...
            # ...
            pass
    """
    _scopes = scopes or []
    assert type(_scopes) == list

    def decorator(view_func):
        @wraps(view_func)
        def _validate(request, *args, **kwargs):
            validator = validator_cls()
            core = OAuthLibCore(server_cls(validator))
            valid, oauthlib_req = core.verify_request(request, scopes=_scopes)
            if valid:
                request.resource_owner = oauthlib_req.user
                return view_func(request, *args, **kwargs)
            if request.user.is_authenticated() and request.user.is_dealer():
                return view_func(request, *args, **kwargs)
            return HttpResponseForbidden()
        return _validate
    return decorator


def custom_csrf_exempt(view_func):
        """
            Marks a view function as being exempt from the CSRF view protection.
            The following decorator checks if there exists a header "X_SKIP_CSRF"
            The following header will be set by the the API calls.
        """
        def wrapped_view(request, *args, **kwargs):
            if request.META.get('HTTP_X_SKIP_CSRF', False) in ['true', 'True', 'TRUE']:
                wrapped_view.csrf_exempt = True
            elif request.META.get('CSRF_COOKIE', None) is not None:
                wrapped_view.csrf_exempt = False
            else:
                return HttpResponseForbidden()
            return view_func(request, *args, **kwargs)
        wrapped_view.csrf_exempt = True
        return wraps(view_func, assigned=available_attrs(view_func))(wrapped_view)
