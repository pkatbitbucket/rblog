from django.conf.urls import url

from apis.views import TokenView

from . import views

urlpatterns = [
    url(r'^auth/', TokenView.as_view(), name='oauth_authentication'),
    url(r'^proposal/inspection/documents/(?P<inspection_id>[\w-]+)/$', views.proposal_document),
    url(r'^document/$', views.document_handler),
    url(r'^document/(?P<document_id>\d+)/$', views.document_handler),
    url(r'^proposal/inspection/approval/(?P<inspection_id>[\w-]+)/$', views.approval_request),
    url(r'^proposal/inspection/approval-status/(?P<inspection_id>[\w-]+)/$', views.check_approval_status),
    url(r'^feedback/(?P<inspection_id>\d+)/$', views.user_feedback),
    url(r'^payment/(?P<requirement_id>\d+)/$', views.user_feedback),
    url(r'^policy/(?P<requirement_id>\d+)/$', views.user_feedback),
    url(r'^inspection-payment/(?P<inspection_id>[\w-]+)/$', views.inspection_payment, name='inspection_payment'),
    url(r'^transaction-payment/(?P<inspection_id>[\w-]+)/$', views.requirement_payment, name='requirement_payment'),
    url(r'^get-pending-inspections/$', views.pending_inspections, name='pending_inspections'),
]
