import sys
import datetime
import copy
import cStringIO

from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.shortcuts import get_object_or_404
from django.core.files.uploadedfile import InMemoryUploadedFile

from apis.v1.decorators import protected_resource
from apis.v1.decorators import custom_csrf_exempt
from motor_product.models import MotorInspectionDocument
from motor_product.models import Inspection
from motor_product.prod_utils import get_flat_premium
# from motor_product.models import Transaction
from payment.forms import ChequePaymentForm, MSwipeForm
from payment.utils import create_payment
from core.models import Payment
from django.contrib.auth.decorators import permission_required
from utils import api_logger, log_error


def datetime_format(date_instance=None,
                    time_offset=0, date_format='%d-%m-%Y-%H%M%S%f'):
    if not date_instance:
        date_instance = datetime.datetime.now()
    date_instance = date_instance + datetime.timedelta(minutes=time_offset)
    return date_instance.strftime(date_format)


DEFAULT_RESPONSE_DICT = {
    "count": 0,
    "data": [],
    "success": True,
    "error": {},
    "next": "",
    "prev": "",
}

DEFAULT_PAGINATION = 10


@protected_resource()
@require_http_methods(['GET'])
def proposal_document(request, inspection_id, *args, **kwargs):
    """
    To get documents that were added to the proposal
    the last time it was inspected(in case of a fail)
    """
    return_data = copy.deepcopy(DEFAULT_RESPONSE_DICT)
    for x in MotorInspectionDocument.objects.filter(
            inspection__uuid=inspection_id):
        return_data['data'].append({
            'image_url': x.document.url,
            'id': x.id,
            'document_type': x.document_type,
            'approval_status': x.approval_status
        })
        return_data['count'] += 1
    return_data.pop('next')
    return_data.pop('prev')
    return JsonResponse(return_data)


@protected_resource()
@require_http_methods(['GET', "POST", "PUT", "DELETE"])
@custom_csrf_exempt
def document_handler(request, *args, **kwargs):
    document_id = kwargs.get('document_id', None)
    if request.method in ['GET', 'PUT', 'DELETE'] and document_id is None:
        return_data = {
            'success': False,
            'error': {"invalid_query": "Please pass document id"}
        }
        api_logger.info('Unable to save. Did not pass document_id')
        return JsonResponse(return_data, status=400)
    elif request.method == 'POST' and document_id:
        return_data = {
            'success': False,
            'error': {"invalid_query": "POST does not accept document id parameter"}
        }
        api_logger.warn("POST does not accept document id")
        return JsonResponse(return_data, status=400)
    if request.method == 'GET':
        tdb = get_object_or_404(MotorInspectionDocument, id=document_id)
        return_data = {
            'image_url': tdb.document.url,
            'id': tdb.id,
            'document_type': tdb.document_type,
            'approval_status': tdb.approval_status,
        }
    elif request.method == 'POST':
        return_data = {}
        files = request.FILES
        data = request.POST
        if data.get('image'):
            try:
                image_str = data['image']
                try:
                    image_file = image_str.decode('base64')
                except:
                    image_str += '=' * (-len(image_str) % 4)
                    image_file = image_str.decode('base64')
                file = cStringIO.StringIO(image_file)
                image = InMemoryUploadedFile(file,
                                             field_name='file',
                                             name="{0} {1}.jpg".format(
                                                 datetime_format(), data.get('inspection_id', 'inspection')),
                                             content_type="image/jpeg",
                                             size=sys.getsizeof(file),
                                             charset=None)
                request.FILES[u'image'] = image
            except Exception as e:
                return_data['success'] = False
                return_data['error'] = str(e)
                api_logger.warn("POST does not accept document id")
                return JsonResponse(return_data, status=400)
        if 'document_type' in data and 'image' in files:
            try:
                inspection = Inspection.objects.get(
                    uuid=data['inspection_id'])
                tdb = MotorInspectionDocument.objects.create(
                    document=files['image'],
                    document_type=data['document_type'],
                    inspection=inspection)
            except Exception as e:
                return_data['success'] = False
                return_data['error'] = str(e)
                log_error(api_logger, request=request, msg=return_data)
                return JsonResponse(return_data, status=400)
            return_data = {
                'image_url': tdb.document.url,
                'id': tdb.id,
                'document_type': tdb.document_type,
                'approval_status': tdb.approval_status,
            }
        else:
            return_data = {
                'success': False,
                'error': "please pass both document_type and image to save the image"
            }
            api_logger.warn(return_data['error'])
            return JsonResponse(return_data, status=400)
    elif request.method == 'PUT':
        return_data = {}
        data, files = request.parse_file_upload(request.META, request)
        tdb = get_object_or_404(MotorInspectionDocument, id=document_id)
        if files.get('image', False):
            tdb.document.delete()
            tdb.document = files['image']
        tdb.document_type = data.get('document_type', tdb.document_type)
        tdb.save()
        return_data = {
            'image_url': tdb.document.url,
            'id': tdb.id,
            'document_type': tdb.document_type,
            'approval_status': tdb.approval_status,
        }
    elif request.method == 'DELETE':
        tdb = get_object_or_404(MotorInspectionDocument, id=document_id)
        tdb.delete()
        return_data = {
            "success": True,
            "message": "Image deleted sucessfully",
            "id": document_id,
        }
    return JsonResponse(return_data)


@custom_csrf_exempt
@protected_resource()
@require_http_methods(["POST"])
def approval_request(request, inspection_id, *args, **kwargs):
    inspection_instance = get_object_or_404(
        Inspection, uuid=inspection_id)
    return_data = {
        "success": True,
    }
    try:
        raw_data = {}
        if request.POST.get('remarks'):
            raw_data['remarks'] = request.POST['remarks']
        inspection_instance.status = Inspection.DOCUMENT_UPLOADED
        all_payments = inspection_instance.transaction.requirement.payment.all()
        if all_payments.filter(status=Payment.THIRD_PARTY):
            inspection_instance.status = Inspection.PAYMENT_DONE
        inspection_instance.raw = raw_data
        inspection_instance.save(user=request.user)
        http_status_code = 200
    except Exception as e:
        return_data['success'] = False
        return_data['error'] = str(e)
        log_error(api_logger, request=request, msg=return_data)
        http_status_code = 400
    return JsonResponse(return_data, status=http_status_code)


@custom_csrf_exempt
@protected_resource()
@require_http_methods(["GET"])
def check_approval_status(request, inspection_id):
    inspection = get_object_or_404(
        Inspection, uuid=inspection_id)
    return_data = {
        "message": "Waiting for approval",
        "status": inspection.status,
    }
    return return_data


def user_feedback(request, inspection_id):
    return "Not Implemented"


@custom_csrf_exempt
@protected_resource()
@require_http_methods(["GET"])
def inspection_payment(request, inspection_id):
    inspection = get_object_or_404(Inspection, uuid=inspection_id)
    if inspection.agent.user != request.user:
        return JsonResponse({"success": False, "error": "Unauthorized access"})
    else:
        # TODO: Remove get flat premium refresh from here.
        # Keeping it here so that it refreshes for existing transactions
        quote_response = inspection.transaction.raw['quote_response']
        try:
            quote_response = get_flat_premium(inspection.transaction.quote.raw_data, [quote_response])[0]
        except:
            quote_response = inspection.transaction.raw['quote_response']
        return_data = {
            'transaction_id': str(inspection.transaction.transaction_id),
            'premium_amount': quote_response.get('final_premium', 0),
            'customer_name': inspection.transaction.customer_name(),
            'customer_phone': inspection.transaction.customer_phone_number(),
            'customer_email': inspection.transaction.customer_email(),
            'insurer_id': inspection.transaction.insurer_id,
            'insurer_name': inspection.transaction.insurer.title,
            'receipt': inspection.transaction.receipt.raw if inspection.transaction.receipt else '',
            'success': True
        }
        try:
            mswipe_assigned = inspection.agent.user.mswipe_set.all()[0]
        except Exception, e:
            print e
        else:
            return_data['mswipe_id'] = mswipe_assigned.user_id
            return_data['mswipe_pwd'] = mswipe_assigned.password
        return JsonResponse(return_data)


@custom_csrf_exempt
@protected_resource()
@require_http_methods(["POST"])
def requirement_payment(request, inspection_id):
    return_data = {}
    inspection = get_object_or_404(Inspection, uuid=inspection_id)
    if inspection.agent.user != request.user:
        return JsonResponse({"success": False, "error": "Unauthorized access"})
    else:
        payment_type = request.POST.get('payment_type', 'cheque')
        if inspection.transaction.requirement.user:
            inspection_user = inspection.transaction.requirement.user
        else:
            inspection_user = request.user
        payment = create_payment(
            inspection_user, requirement=inspection.transaction.requirement,
            payment_type=payment_type, amount=request.POST.get('premium_amount', 1000))
        if payment_type == 'cheque':
            try:
                image_str = request.POST['image']
                try:
                    image_file = image_str.decode('base64')
                except:
                    image_str += '=' * (-len(image_str) % 4)
                    image_file = image_str.decode('base64')
                file = cStringIO.StringIO(image_file)
                image = InMemoryUploadedFile(file,
                                             field_name='file',
                                             name="cheque_{0}.jpg".format(
                                                 datetime_format(), request.POST['cheque_number']),
                                             content_type="image/jpeg",
                                             size=sys.getsizeof(file),
                                             charset=None)
                request.FILES[u'cheque_image'] = image
            except Exception as e:
                return_data['error'] = str(e)
                return_data['success'] = False
            form_instance = ChequePaymentForm(request.POST, request.FILES)
        elif payment_type == 'mswipe':
            form_instance = MSwipeForm(request.POST)
        if form_instance.is_valid():
            form_instance.save(uuid=payment.uuid)
            inspection.status = "PAYMENT_DONE"
            inspection.save()
        else:
            return JsonResponse({'success': False, 'errors': ', '.join(form_instance.errors)})

        return_data = {
            'success': True
        }
        return JsonResponse(return_data)


@custom_csrf_exempt
@protected_resource()
@permission_required('motor_product.change_pendinginspection', raise_exception=True)
def pending_inspections(request):
    agent = request.user.inspectionagent_set.get()
    if agent:
        inspections = Inspection.objects.filter(agent=agent, scheduled_on=datetime.datetime.today())
        print inspections
        pending_inspections = []
        for ins in inspections:
            inspection_details = {
                "status": ins.status,
                "city": ins.city.name,
                "scheduled_on": ins.scheduled_datetime(),
                "inspection_id": str(ins.uuid),
                "customer_name": ins.transaction.customer_name(),
                "customer_phone": ins.transaction.customer_phone_number(),
                "address": ins.address,
                "vehicle_number": ins.transaction.vehicle_registration_number(),
                "vehicle_info": str(ins.transaction.quote.vehicle),
            }
            pending_inspections.append(inspection_details)
        return JsonResponse({"pending_inspections": pending_inspections})
