import json
import logging
import re
import sys
import datetime

from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from oauthlib.oauth2 import OAuth2Error
from requests import HTTPError

from account_manager.models import RelationshipManager
from bs4 import BeautifulSoup
from core.models import InvalidUserEmail
from dateutil import parser as datetime_parser
from freshdesk import create_ticket
from motor_product.prod_utils import parse_motor_policy_email
from oauth2_provider import views as oauth_views
from putils import clean_email_list
from social.apps.django_app.default.models import DjangoStorage
from social.apps.django_app.utils import load_backend
from social.exceptions import WrongBackend
from social.strategies.django_strategy import DjangoStrategy
from utils import LMSUtils, log_error, convert_datetime_to_timezone

logger = logging.getLogger(__name__)


def api_home(request):
    return HttpResponse(json.dumps({'success': True}))


class InvalidProviderError(OAuth2Error):
    error = 'invalid_provider'


class InvalidTokenError(OAuth2Error):
    error = 'invalid_token'


class InvalidEmailError(OAuth2Error):
    error = 'missing_email_address'


class UsernameTakenError(OAuth2Error):
    error = 'username_taken'


class UnknownError(OAuth2Error):
    error = 'unknown_error'


class OAuth2Validator(oauth_views.TokenView.validator_class):
    def validate_user(self, username, password, client, request, *args, **kwargs):
        """
        Check username and password correspond to a valid and active User
        """
        result = super(self.__class__, self).validate_user(
            username, password, client, request, *args, **kwargs)
        if not result:
            try:
                provider, access_token = password.split(' ')
            except ValueError:
                return False
            try:
                # Available providers 'linkedin-oauth2', 'google-oauth2',
                # 'google-oauth', 'facebook', 'facebook-app'
                social_auth = load_backend(strategy=DjangoStrategy(
                    DjangoStorage), name=provider, redirect_uri='')
            except WrongBackend:
                raise InvalidProviderError(request=request)

            try:
                user = social_auth.do_auth(access_token=access_token)
            except HTTPError:
                raise InvalidTokenError(request=request)
            except InvalidUserEmail:
                raise InvalidEmailError(request=request)
            except:
                log_error(logger,
                          request,
                          msg="Internal Server Error: Unknown Error Received in API Authentication")
                raise UnknownError(request=request)

            """
            if user.is_new:
                if user.__class__.objects.filter(username=username).exclude(id=user.id).exists():
                    raise UsernameTakenError(request=request)
                else:
                    try:
                        user.username = username
                        user.save()
                    except:
                        raise UnknownError(request=request)
            if bool(user.email) and user.username == username:
                request.user = user
                return True
            """
            request.user = user
            return True
        return result


class TokenView(oauth_views.TokenView):
    validator_class = OAuth2Validator


@csrf_exempt
@require_POST
def parse_send_grid_email(request):
    envelope = json.loads(request.POST.get('envelope'))

    # Get some header information
    to_address = envelope['to'][0]
    from_address = envelope['from']

    # Now, onto the body
    html = request.POST.get('html')
    subject = request.POST.get('subject')

    # Process the attachements, if any
    num_attachments = int(request.POST.get('attachments', 0))
    attachments = []
    if num_attachments > 0:
        for num in range(1, (num_attachments + 1)):
            attachment = request.FILES.get(('attachment{}'.format(num)))
            attachments.append(attachment.read())
            # attachment will have all the parameters expected in a Flask file upload

    parse_send_grid_email_to_freshdesk(from_address, to_address, html, subject, attachments)

    return HttpResponse("OK")


def parse_send_grid_email_to_freshdesk(from_email, to_email, email_html, subject, files=None):
    if not to_email.endswith('@coverfox.com'):
        to_email = '@'.join((to_email.split('@')[0], 'coverfox.com'))
    responder_id = get_object_or_404(RelationshipManager, email__iexact=to_email).freshdesk_responder_id
    if not files:
        create_ticket.delay(email=from_email,
                            subject=subject,
                            description_html=email_html,
                            responder_id=responder_id,
                            ticket_type='VIA-API')
    return 'OK'


@csrf_exempt
@require_POST
def parse_policy_email(request):
    envelope = json.loads(request.POST.get('envelope'))

    assert envelope['to'][0] == 'motor@policy.coverfox.com', 'recieved email to {}'.format(envelope['to'][0])
    assert int(request.POST.get('attachments', 0)) != 0, 'attachments not found in policy email\nWTF'

    from_address = clean_email_list(request.POST['from'])[0]
    to_address = clean_email_list(request.POST['to'])
    cc = clean_email_list(request.POST.get('cc', ''))

    sent_to = set(to_address + cc)

    content = {
        'html': request.POST.get('html', ''),
        'text': request.POST.get('text') or BeautifulSoup(request.POST.get('html', ''), features='html.parser').text,
    }

    subject = request.POST['subject']

    headers = request.POST['headers']
    datetime_re = re.compile(
        r'[A-Z][a-z]{2},'  # weekday
        r'\s\d{1,2}\s[A-Z][a-z]{2}\s\d{4}'  # date
        r'\s\d{2}:\d{2}:\d{2}'  # time
        r'\s[+-]\d{4}'  # tzoffset
        r'(?:\s\([A-Z]+\))*'  # tz
    )
    try:
        dt = datetime_parser.parse(min(d for d in datetime_re.findall(headers)))
    except ValueError:
        # Unable to parse or no datetime info in headers
        logger.warning(
            msg='Unable to fetch policy email time', exc_info=sys.exc_info()
        )
        dt = None

    return parse_motor_policy_email(
        from_address=from_address,
        sent_to=sent_to,
        subject=subject,
        content=content,
        attachments=request.FILES.values(),
        mail_time=dt,
    )


@require_POST
def save_lms_event(request):
    try:
        data = json.loads(request.POST['data'])
    except KeyError:
        return JsonResponse({'success': False}, status=400)
    except:
        return JsonResponse({'success': False, 'message': 'Invalid JSON'}, status=400)

    if not all(key for key in ['event', 'product_type']):
        return JsonResponse({'success': False}, status=400)

    product_type = data['product_type']
    if product_type not in ['motor', 'health', 'travel']:
        return JsonResponse({'success': False, 'message': 'Invalid product type'}, status=400)

    event = data['event']
    mobile = data.get('mobile') or request.COOKIES.get('mobileNo')
    email = data.get('email') or getattr(request.TRACKER.user, 'email', None) or request.COOKIES.get('email')
    success = False
    msg = 'something went wrong'
    try:
        LMSUtils.send_lms_event(
            event=event,
            product_type=product_type,
            tracker=request.TRACKER,
            data=data,
            mobile=mobile,
            email=email,
            internal=request.IS_INTERNAL,
            request_ip=request.META['REMOTE_ADDR'],
        )
    except NotImplementedError as e:
        msg = e.message
    except:
        log_error(logger, request=request, msg='Unable to process lms event api')
    else:
        success = True
        msg = ''
    finally:
        return JsonResponse({'success': success, 'message': msg})

def get_current_time(request):
    if request.is_ajax():
        return JsonResponse({'now': convert_datetime_to_timezone(datetime.datetime.now())})
    else:
        return HttpResponseBadRequest()
