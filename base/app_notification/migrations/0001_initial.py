# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserDevice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('device_id', models.CharField(max_length=128, null=True, blank=True)),
                ('device_type', models.CharField(default=b'ANDROID', max_length=10, choices=[(b'ANDROID', 'Android'), (b'IOS', 'IOS')])),
                ('registration_id', models.CharField(max_length=256)),
                ('is_active', models.BooleanField(default=True)),
                ('user', models.ForeignKey(related_name='devices', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
