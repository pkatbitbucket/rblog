from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.users import User
# Create your models here.


class UserDevice(models.Model):
    ANDROID = 'ANDROID'
    IOS = 'IOS'
    DEVICE_CHOICES = (
        (ANDROID, _('Android')),
        (IOS, _('IOS'))
    )
    user = models.ForeignKey(User, related_name='devices')
    device_id = models.CharField(max_length=128, null=True, blank=True)
    device_type = models.CharField(max_length=10, choices=DEVICE_CHOICES, default=ANDROID)
    registration_id = models.CharField(max_length=256)
    is_active = models.BooleanField(default=True)
