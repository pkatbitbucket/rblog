from django.conf.urls import url

from app_notification import views as aviews


urlpatterns = [
    url(r'^register-device/$', aviews.register_device, name='register_device'),
]
