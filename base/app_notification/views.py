from django.shortcuts import render
from app_notification.models import *
from gcm import GCM
import settings
from django.http import HttpResponse, HttpResponseRedirect, Http404, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
# Create your views here.

gcm = GCM(settings.GCM_APIKEY)


@csrf_exempt
@login_required
def register_device(request):
    if request.method == "GET":
        return HttpResponse({'success': 1, "message": "registered agent"})
    else:
        reg_id = request.POST.get('reg_id')
        device_id = request.POST.get('device_id')
        user_device, created = UserDevice.objects.get_or_create(
            user=request.user,
            device_id=device_id
        )
        user_device.registration_id = reg_id
        user_device.save()
        return HttpResponse({'success': 1, "message": "registered agent"})


def send_message(user, message):
    user_devices = user.devices.all()
    for device in user_devices:
        if device.device_type == 'IOS':
            pass
        else:
            send_gcm(device.registration_id, message)


def send_gcm(reg_id, message):
    response = gcm.json_request(registration_ids=[reg_id], data=message)
    return response
