from django.conf.urls import url, include

from . import views

__author__ = 'vinay'

urlpatterns = [
    url(r'^say-sorry/$', views.server_error, name='server_error'),
    url(r'^whats-my-ip/$', views.whats_my_ip, name="whats_my_ip")
]

urlpatterns = [
    url(r'^story-of-coverfox/$', views.story_of_coverfox, name='story_of_coverfox'),
    url(r'^bakar/', include(urlpatterns)),
]
