from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, RequestContext
from django.views.defaults import server_error as default_server_error
from putils import get_request_ip


def page_not_found(request):
    return render(request, "maze_404/404.html", status=404)


def page_not_found_403(request):
    return render(request, "maze_404/404.html", status=403)


def story_of_coverfox(request):
    return render(request, "story-of-coverfox.html")


def server_error(request):
    template_name = 'bakar:500.html'
    try:
        if request.is_ajax():
            return HttpResponse(status=500)
        else:
            request.session['last_page'] = request.path
            if request.COOKIES.get('lms_campaign', '') not in ('', 'None', None):
                campaign = request.COOKIES['lms_campaign']
            else:
                campaign = 'motor-search{}'.format(
                    'M' if request.user_agent.is_mobile else 'D')
            return render(request, template_name, context_instance=RequestContext(request, {'campaign': campaign}), status=500)
    except:
        try:
            return render(request, template_name, status=500)
        except:
            return default_server_error(request)


def whats_my_ip(request):
    return_data = {
        "real_ip": get_request_ip(request),
        "all_ips": request.META.get('HTTP_X_FORWARDED_FOR'),
        "REMOTE_ADDR": request.META.get('REMOTE_ADDR'),
    }
    return JsonResponse(return_data)