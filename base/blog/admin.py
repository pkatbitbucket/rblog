from django.contrib import admin
from blog.models import *


class CategoryAdmin(admin.ModelAdmin):
    pass
admin.site.register(Category, CategoryAdmin)


class CampaignAdmin(admin.ModelAdmin):
    pass
admin.site.register(Campaign, CampaignAdmin)


class SubscriberAdmin(admin.ModelAdmin):
    pass
admin.site.register(Subscriber, SubscriberAdmin)


class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'publish', 'status')
    list_filter = ('publish', 'category', 'sub_category', 'status')
    search_fields = ('title', 'body')
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(Post, PostAdmin)
