from django.apps.config import AppConfig


class BlogConfig(AppConfig):
    name = 'blog'

    def ready(self):
        from .signals import handlers  # NOQA
