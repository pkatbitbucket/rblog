import random
import hashlib
import datetime

from django import forms
from django.core.urlresolvers import reverse
from django.db.models import Q
import cf_cns

from .models import Subscriber, Campaign, BlogScribe, Post, Category
import mail_utils
from core.models import User
from customdb.widgets import AdvancedFileInput
from cf_fhurl import RequestForm, RequestModelForm


class InviteForm(RequestForm):
    invited_by_email = forms.EmailField()
    email1 = forms.EmailField()
    email2 = forms.EmailField(required=False)
    email3 = forms.EmailField(required=False)

    def save(self):
        self.campaign = "INVITE"
        d = self.cleaned_data.get
        tracker = self.request.TRACKER
        if tracker.extra.get('first_name', '') and tracker.extra.get('gender', ''):
            for email in ['email1', 'email2', 'email3']:
                if not d(email):
                    continue
                subscriber = Subscriber.objects.filter(email=d(email))
                if subscriber:
                    subs = subscriber[0]
                    if subs.extra.get('invited_by'):
                        subs.extra.update(
                            {'invited_by': subs.extra['invited_by'].append(d('invited_by_email'))})
                    else:
                        subs.extra.update(
                            {'invited_by': [d('invited_by_email')]})
                    subs.is_active = True
                    subs.save()
                    subs.campaign.add(
                        Campaign.objects.get(title=self.campaign))
                    continue
                else:
                    subs = Subscriber(
                        email=d(email),
                        is_active=True,
                        code=hashlib.md5("%s%s" % (datetime.datetime.now().strftime(
                            '%d%m%Y%H%M%S'), str(random.random()))).hexdigest(),
                        extra={'invited_by': [d('invited_by_email')]}
                    )
                    subs.save()
                    subs.campaign.add(
                        Campaign.objects.get(title=self.campaign))
                    cf_cns.notify('INVITE_MAIL', to=(subs.name, subs.email), tracker=tracker)
        return {'success': True, }


class SubscribeForm(RequestModelForm):
    mtype = forms.CharField(widget=forms.HiddenInput())
    email = forms.EmailField(required=True)
    pass

    class Meta:
        model = Subscriber
        exclude = ('extra', 'is_active', 'campaign')

    def validate_unique(self):
        return

    def clean_email(self):
        d = self.cleaned_data.get
        subscriber = Subscriber.objects.filter(
            email=d('email'),
            is_active=True,
            campaign__title=self.data.get('mtype', 'BLOG')
        )
        if subscriber:
            if self.data.get('mtype', 'BLOG') == 'BLOG':
                raise forms.ValidationError(
                    "Email already subscribed for articles")
            elif self.data.get('mtype', 'BLOG') == 'REVIEW':
                raise forms.ValidationError("Email pending for review")
            else:
                raise NameError('Unknow Campaign for subscription')
        return d('email')

    def save(self):
        d = self.cleaned_data.get
        subscriber = Subscriber.objects.filter(email=d('email'))
        if subscriber:
            sf = subscriber[0]
            sf.code = hashlib.md5(
                "%s%s%s" % (
                    sf.name,
                    datetime.datetime.now().strftime('%d%m%Y%H%M%S'),
                    str(random.random())
                )
            ).hexdigest()
            sf.is_active = True
            sf.campaign.add(Campaign.objects.get(title=d('mtype', 'BLOG')))
            sf.save()
        else:
            sf = super(SubscribeForm, self).save(commit=False)
            sf.is_active = True
            sf.code = hashlib.md5(
                "%s%s%s" % (
                    sf.name,
                    datetime.datetime.now().strftime('%d%m%Y%H%M%S'),
                    str(random.random())
                )
            ).hexdigest()
            sf.save()
            sf.campaign.add(Campaign.objects.get(title=d('mtype', 'BLOG')))

        if d('mtype', 'BLOG') == 'BLOG':
            if not BlogScribe.objects.filter(sub=sf):
                bs = BlogScribe(sub=sf)
                bs.save()
            cf_cns.notify('ON_SUBSCRIPTION', to=(sf.name, sf.email), subscriber=sf)
            pass
        elif d('mtype', 'BLOG') == 'REVIEW':
            cf_cns.notify('ON_REVIEW_SIGNUP', to=(sf.name, sf.email), subscriber=sf)
            pass
        else:
            raise NameError('Unknown Campaign for subscription')
        return {'success': True, 'mesg': {'email': sf.email, 'name': sf.name, 'code': sf.code}}


class PostAddForm(RequestModelForm):
    category = forms.ModelChoiceField(queryset=Category.objects.active())
    sub_category = forms.ModelChoiceField(
        queryset=Category.objects.active(), required=False)
    thumbnail = forms.ImageField(widget=AdvancedFileInput(), required=False)
    author = forms.ModelChoiceField(
        queryset=User.objects.filter(Q(is_superuser=True) | Q(groups__name='AUTHOR')).distinct()
    )

    class Meta:
        model = Post
        exclude = ('allow_comments', 'publish', 'sub_category', 'votes')

    def init(self, post_id=None):
        if post_id:
            self.instance = Post.objects.get(id=post_id)

    def save(self):
        post = super(PostAddForm, self).save()
        post.allow_comments = True
        post.save()
        if self.request.POST['save'] == 'save':
            post_url = reverse('post_detail', kwargs={
                'category': post.category.slug,
                'slug': post.slug,
            })
            return {'success': True, 'redirect': post_url}
        if self.request.POST['save'] == 'saveedit':
            return {'success': True, 'redirect': reverse('post_edit', kwargs={'post_id': post.id})}

    def clean_slug(self):
        d = self.cleaned_data
        if self.instance and self.instance.id:
            pst = Post.objects.filter(
                Q(slug=d['slug']) & ~Q(id=self.instance.id))
        else:
            pst = Post.objects.filter(slug=d['slug'])

        if len(pst) > 0:
            raise forms.ValidationError("Slug invalid. Must be Unique")
        else:
            return d['slug']
