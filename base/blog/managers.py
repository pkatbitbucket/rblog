import datetime

import django
from cfblog import managers as cfblog_managers
from django.db.models import Manager


class PostManager(Manager):

    def published(self):
        if django.VERSION < (1, 6):
            return self.get_query_set().filter(status__gte=2, publish__lte=datetime.datetime.now())
        else:
            return self.get_queryset().filter(status__gte=2, publish__lte=datetime.datetime.now())


class CategoryManager(Manager):

    def active(self):
        if django.VERSION < (1, 6):
            return self.get_query_set().filter(is_active=True)
        else:
            return self.get_queryset().filter(is_active=True)

    def top(self):
        if django.VERSION < (1, 6):
            return self.get_query_set().filter(is_active=True, level=1)
        else:
            return self.get_queryset().filter(is_active=True, level=1)


class CfblogCategoryManager(Manager, object):
    def get_queryset(self):
        return super(CfblogCategoryManager, self).get_queryset().exclude(id=1)


class CfblogContentManager(cfblog_managers.ContentManager):
    def get_queryset(self):
        return super(CfblogContentManager, self).get_queryset().blog_posts()
