# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
import blog.models
import datetime
import customdb.thumbs
import tagging.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BlogScribe',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True)),
                ('grouping', models.CharField(max_length=250, blank=True)),
                ('group', models.CharField(max_length=250, blank=True)),
                ('batch', models.CharField(max_length=250, blank=True)),
                ('created', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
                ('updated_on', models.DateTimeField(
                    auto_now=True, verbose_name='updated')),
            ],
        ),
        migrations.CreateModel(
            name='Campaign',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('created', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name='title')),
                ('description', models.TextField(
                    verbose_name='description', blank=True)),
                ('slug', models.SlugField(verbose_name='slug')),
                ('level', models.IntegerField(
                    verbose_name='level', choices=[(1, 1), (2, 2)])),
                ('is_active', models.BooleanField(
                    default=True, verbose_name='is_active')),
            ],
            options={
                'ordering': ('title',),
                'db_table': 'blog_categories',
                'verbose_name': 'category',
                'verbose_name_plural': 'categories',
            },
        ),
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('action', models.CharField(max_length=100,
                                            verbose_name='action', blank=True)),
                ('person', jsonfield.fields.JSONField(
                    verbose_name='person', blank=True)),
                ('feedback', jsonfield.fields.JSONField(
                    verbose_name='feedback', blank=True)),
                ('created', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
                ('object_id', models.PositiveIntegerField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name='title')),
                ('slug', models.SlugField(max_length=70, verbose_name='slug')),
                ('body', models.TextField(verbose_name='body')),
                ('tease', models.TextField(
                    help_text='Concise text suggested. Does not appear in RSS feed.', verbose_name='tease', blank=True)),
                ('keywords', models.TextField(
                    help_text='List of keywords, relevant for SEO', verbose_name='keywords', blank=True)),
                ('thumbnail', customdb.thumbs.ImageWithThumbsField(
                    null=True, upload_to=blog.models.get_image_path, blank=True)),
                ('infographic', models.ImageField(null=True,
                                                  upload_to=blog.models.get_image_path, blank=True)),
                ('pdf', models.FileField(null=True,
                                         upload_to=blog.models.get_file_path, blank=True)),
                ('status', models.IntegerField(
                    default=1, verbose_name='status', choices=[(1, 'Draft'), (2, 'Public')])),
                ('allow_comments', models.BooleanField(
                    default=True, verbose_name='allow comments')),
                ('publish', models.DateTimeField(
                    default=datetime.datetime.now, verbose_name='publish')),
                ('created', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
                ('modified', models.DateTimeField(
                    auto_now=True, verbose_name='modified')),
                ('tags', tagging.fields.TagField(max_length=255, blank=True)),
                ('notes_todos', models.TextField(
                    verbose_name='Notes and Todos', blank=True)),
            ],
            options={
                'ordering': ('-publish',),
                'db_table': 'blog_posts',
                'verbose_name': 'post',
                'verbose_name_plural': 'posts',
                'get_latest_by': 'publish',
            },
        ),
        migrations.CreateModel(
            name='Subscriber',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200,
                                          verbose_name='name', blank=True)),
                ('email', models.EmailField(unique=True,
                                            max_length=255, verbose_name='email')),
                ('is_active', models.BooleanField(default=True)),
                ('extra', jsonfield.fields.JSONField(
                    default={}, verbose_name='extra', blank=True)),
                ('created', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
                ('code', models.CharField(db_index=True,
                                          max_length=100, verbose_name='code', blank=True)),
                ('campaign', models.ManyToManyField(to='blog.Campaign')),
                ('category', models.ManyToManyField(
                    to='blog.Category', blank=True)),
            ],
        ),
    ]
