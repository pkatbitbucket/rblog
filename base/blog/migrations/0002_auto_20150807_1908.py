# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='author',
            field=models.ForeignKey(
                blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='post',
            name='category',
            field=models.ForeignKey(to='blog.Category'),
        ),
        migrations.AddField(
            model_name='post',
            name='related_posts',
            field=models.ManyToManyField(
                related_name='related_posts_rel_+', to='blog.Post', blank=True),
        ),
        migrations.AddField(
            model_name='post',
            name='sub_category',
            field=models.ForeignKey(
                related_name='sub_post_set', blank=True, to='blog.Category', null=True),
        ),
        migrations.AddField(
            model_name='feedback',
            name='content_type',
            field=models.ForeignKey(
                blank=True, to='contenttypes.ContentType', null=True),
        ),
        migrations.AddField(
            model_name='category',
            name='parent',
            field=models.ForeignKey(blank=True, to='blog.Category', null=True),
        ),
        migrations.AddField(
            model_name='blogscribe',
            name='sub',
            field=models.ForeignKey(to='blog.Subscriber'),
        ),
    ]
