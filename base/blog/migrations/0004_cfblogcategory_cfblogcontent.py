# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cfblog', '0003_auto_20151126_1212'),
        ('blog', '0003_post_votes'),
    ]

    operations = [
        migrations.CreateModel(
            name='CfblogCategory',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('cfblog.category',),
        ),
        migrations.CreateModel(
            name='CfblogContent',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('cfblog.content',),
        ),
    ]
