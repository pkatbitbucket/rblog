import datetime
import os

from cfblog import models as cfblog_models
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import permalink
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from jsonfield import JSONField
from tagging.fields import TagField

import utils
from customdb.thumbs import ImageWithThumbsField

from .managers import (CategoryManager, CfblogCategoryManager,
                       CfblogContentManager, PostManager)


class Category(models.Model):
    """Category model."""
    LEVEL_CHOICES = (
        (1, 1),
        (2, 2),
    )
    title = models.CharField(_('title'), max_length=100)
    description = models.TextField(_('description'), blank=True)
    slug = models.SlugField(_('slug'))
    parent = models.ForeignKey('self', blank=True, null=True)
    level = models.IntegerField(_('level'), choices=LEVEL_CHOICES)
    is_active = models.BooleanField(_('is_active'), default=True)
    objects = CategoryManager()

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')
        db_table = 'blog_categories'
        ordering = ('title',)

    def __unicode__(self):
        return u'%s' % self.title

    @permalink
    def get_absolute_url(self):
        return 'blog_category_detail', None, {'slug': self.slug}


def get_image_path(instance, filename):
    now = datetime.datetime.now()
    monthdir = now.strftime("%Y-%m")
    fname, ext = os.path.splitext(filename)
    new_fname = utils.slugify(fname)
    newfilename = "%s-%s.%s" % (new_fname, now.strftime("%I%M%S"), ext)
    path_to_save = "uploads/blog/%s/%s" % (monthdir, newfilename)
    return path_to_save


def get_file_path(self, instance, filename):
    now = datetime.datetime.now()
    monthdir = now.strftime("%Y-%m")
    fname, ext = os.path.splitext(filename)
    new_fname = utils.slugify(fname)
    newfilename = "%s-%s.%s" % (new_fname, now.strftime("%I%M%S"), ext)
    path_to_save = "uploads/pdfs/%s/%s" % (monthdir, newfilename)
    return path_to_save


class Post(models.Model):
    """Post model."""
    STATUS_CHOICES = (
        (1, _('Draft')),
        (2, _('Public')),
    )
    title = models.CharField(_('title'), max_length=200)
    slug = models.SlugField(_('slug'), max_length=70)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
    body = models.TextField(_('body'), )
    tease = models.TextField(_('tease'), blank=True, help_text=_(
        'Concise text suggested. Does not appear in RSS feed.'))
    keywords = models.TextField(_('keywords'), blank=True, help_text=_(
        'List of keywords, relevant for SEO'))
    thumbnail = ImageWithThumbsField(upload_to=get_image_path, sizes=(
        ('s', 500, 500), ('r', 450, 600), ('t', 200, 260)), null=True, blank=True)
    infographic = models.ImageField(
        upload_to=get_image_path, null=True, blank=True)
    pdf = models.FileField(upload_to=get_file_path, null=True, blank=True)
    status = models.IntegerField(
        _('status'), choices=STATUS_CHOICES, default=1)
    allow_comments = models.BooleanField(_('allow comments'), default=True)
    publish = models.DateTimeField(_('publish'), default=datetime.datetime.now)
    created = models.DateTimeField(_('created'), auto_now_add=True)
    modified = models.DateTimeField(_('modified'), auto_now=True)
    category = models.ForeignKey(Category)
    sub_category = models.ForeignKey(
        Category, related_name='sub_post_set', blank=True, null=True)
    tags = TagField()
    related_posts = models.ManyToManyField(
        'self', symmetrical=True, blank=True)
    notes_todos = models.TextField(_('Notes and Todos'), blank=True)
    votes = models.IntegerField(
        _('votes'), default=0)
    objects = PostManager()

    class Meta:
        verbose_name = _('post')
        verbose_name_plural = _('posts')
        db_table = 'blog_posts'
        ordering = ('-publish',)
        get_latest_by = 'publish'

    def __unicode__(self):
        return u'%s' % self.title

    def get_metacontext(self):
        import putils
        seo = putils.get_seo(self)

        keywords = settings.SITE_KEYWORDS
        if getattr(seo, 'keywords', ''):
            keywords = seo.keywords.all()

        simage = ''
        if getattr(self, 'thumbnail'):
            simage = self.thumbnail.url

        meta = {
            'title': getattr(seo, 'title', self.title),
            'canonical': self.get_absolute_url(),
            'image': simage,
            'description': getattr(seo, 'description', self.tease),
            'keywords': keywords,
            'author': self.author,
            'modified_time': self.modified.strftime("%Y-%m-%d"),
            'published_time': self.created.strftime("%Y-%m-%d"),
            'section': self.category,
            'raw': getattr(seo, 'raw', ''),
        }
        return meta

    @permalink
    def get_edit_url(self):
        return ('post_edit', None, {
            'post_id': self.id
        })

    @permalink
    def get_absolute_url(self):
        if self.category.slug in ['two-wheeler-insurance', 'car-insurance', 'health-insurance', 'travel-insurance']:
            return ('product_post_detail', None, {
                        'slug': self.slug,
                        'category': self.category.slug,
                    })
        else:
            return ('post_detail', None, {
                        'slug': self.slug,
                        'category': self.category.slug,
                    })

    def get_previous_post(self):
        return self.get_previous_by_publish(status__gte=2)

    def get_next_post(self):
        return self.get_next_by_publish(status__gte=2)


class Campaign(models.Model):
    title = models.CharField(max_length=200)
    created = models.DateTimeField(_('created'), auto_now_add=True)

    def __unicode__(self):
        return "%s" % (self.title)


class Subscriber(models.Model):
    """Subscriber model."""
    name = models.CharField(_('name'), max_length=200, blank=True)
    email = models.EmailField(_('email'), max_length=255, unique=True)
    is_active = models.BooleanField(default=True)
    category = models.ManyToManyField('Category', blank=True)
    # Can be used to capture age and other details later
    extra = JSONField(_('extra'), blank=True, default={})
    created = models.DateTimeField(_('created'), auto_now_add=True)
    code = models.CharField(_('code'), max_length=100,
                            db_index=True, blank=True)
    campaign = models.ManyToManyField('Campaign')

    def __unicode__(self):
        return "%s<%s>" % (self.name, self.email)


class Feedback(models.Model):
    """Feedback model."""
    action = models.CharField(_('action'), max_length=100, blank=True)
    person = JSONField(_('person'), blank=True)
    feedback = JSONField(_('feedback'), blank=True)
    created = models.DateTimeField(_('created'), auto_now_add=True)
    content_type = models.ForeignKey(ContentType, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    def __unicode__(self):
        return "%s" % (self.person)


class BlogScribe(models.Model):
    sub = models.ForeignKey(Subscriber)
    is_active = models.BooleanField(default=True)
    grouping = models.CharField(max_length=250, blank=True)
    group = models.CharField(max_length=250, blank=True)
    batch = models.CharField(max_length=250, blank=True)
    created = models.DateTimeField(_('created'), auto_now_add=True)
    updated_on = models.DateTimeField(_('updated'), auto_now=True)

    def __unicode__(self):
        return "%s - %s" % (self.sub.name, self.group)


class CfblogCategory(cfblog_models.Category):
    objects = CfblogCategoryManager()

    class Meta(object):
        proxy = True

    @property
    def slug(self):
        return slugify(self.title)


class CfblogContent(cfblog_models.Content):
    objects = CfblogContentManager()

    class Meta(object):
        proxy = True

# from dcache import cache_signals
