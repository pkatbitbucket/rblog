import json

from django.dispatch import receiver

from cfblog.signals import pre_publish_signal
from cfblog.models import Content
from cfblog.validators import validate_and_get_template
import utils

META_TAG_LIST = [
    'meta_title',
    'meta_descr',
    'meta_gplus_title',
    'meta_gplus_desc',
    'meta_gplus_img',
    'meta_twitter_title',
    'meta_twitter_descr',
    'meta_twitter_author_handle',
    'meta_twitter_img',
    'meta_og_title',
    'meta_og_image',
    'meta_og_desc',
    'meta_fb_author_profile'
]


@receiver(pre_publish_signal, sender=Content)
def pre_publish_handler(sender, **kwargs):
    error_list = []
    warning_list = []
    cms_page = kwargs['cms_page']
    auth_data = cms_page.auth_data
    template_data = validate_and_get_template(cms_page.template).render(
        {}, utils.get_request()
    )
    for tag in META_TAG_LIST:
        try:
            if auth_data[tag] == "":
                error_list.append("{}: Empty meta tag".format(tag))
        except KeyError:
            if tag in template_data:
                error_list.append("{}: Empty meta tag".format(tag))
            else:
                warning_list.append(
                    "{}: Tag Not Available in Template".format(tag)
                )

    if len(error_list):
        return (False, "\n".join(error_list), )
    elif len(warning_list):
        return (None, "\n".join(warning_list), )

    return (True, "Success", )
