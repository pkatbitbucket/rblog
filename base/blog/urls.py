from django.conf.urls import include, url

from cf_fhurl import fhurl
from filebrowser.sites import site
from seo.forms import ObjectSEOForm

from . import views
from .forms import InviteForm, PostAddForm, SubscribeForm

urlpatterns = [
    url(r'^filebrowser/', include(site.urls)),
]

urlpatterns += [

    url('upload-image/$', views.upload_to_s3, name='upload_to_s3'),

    fhurl(r'^ajax/seo-form/(?P<app>[\w]+)/(?P<model>[\w]+)/(?P<object_id>[\d]+)/$',
          ObjectSEOForm, name='ajax_seo_object_add', json=True),
    url(r'^ajax/get-tags/$', views.get_tags, name='ajax_get_tags'),

    fhurl(r'^ajax/subscribe/$', SubscribeForm, name='ajax_subscribe', template=None, json=True),
    fhurl(r'^ajax/invite/$', InviteForm, name='ajax_invite', template=None, json=True),

    url(r'^unsubscribe/(?P<code>[-\w]+)/$', views.unsubscribe, name='unsubscribe'),
    url(r'^feedback/$', views.feedback, name='feedback'),

    url(r'^subscribe/$', views.subscribe, name='subscribe'),
    url(r'^subscribe/success/(?P<code>[-\w]+)/$', views.subscribe_success, name='subscribe_success'),

    fhurl(r'^ajax/edit/(?P<post_id>[\d]+)/$', PostAddForm,
          name='ajax_post_edit', template="blog/post_add.html", json=True),

    fhurl(r'^ajax/add/$', PostAddForm, name='ajax_post_add', template="blog/post_add.html", json=True),

    url(r'^jargon_search/$', view=views.jargon_search, name='jargon_search'),

    url(r'^search/$', view=views.search, name='post_search'),

    url(r'^$', view=views.PostListView.as_view(), name='post_index'),
    url(r'^page/(?P<page>[\d]+)/$', view=views.PostListView.as_view(), name='post_index_paginated'),

    # also change url name in ORGANIC_LANDING_PAGE_URLNAMES variable of campaign middleware
    url(r'^category/(?P<slug>[-\w]+)/$', view=views.PostCategoryListView.as_view(), name='category_post_list'),
    url(r'^category/(?P<slug>[-\w]+)/page/(?P<page>[\d]+)/$',
        view=views.PostCategoryListView.as_view(), name='category_post_list_paginated'),

    url(r'^author/(?P<slug>[-\w]+)/$', view=views.PostAuthorListView.as_view(), name='author_post_list'),
    url(r'^author/(?P<slug>[-\w]+)/page/(?P<page>[\d]+)/$',
        view=views.PostAuthorListView.as_view(), name='author_post_list_paginated'),

    url(r'^tags/(?P<tag>[-\w ]+)/$', view=views.PostTagListView.as_view(), name='tag_post_list'),
    url(r'^tags/(?P<tag>[-\w ]+)/page/(?P<page>[\d]+)/$',
        view=views.PostTagListView.as_view(), name='tag_post_list_paginated'),
    url(r'^(?P<category>[-\w]+)/(?P<slug>[-\w]+)/$', view=views.legacy_post_view, name='post_detail'),
]

urlpatterns = [
    # also change url name in ORGANIC_LANDING_PAGE_URLNAMES variable of campaign middleware
    url(r'^(?P<slug>[-\w]+)/articles/$', view=views.PostCategoryListView.as_view(), name='product_category_post_list'),
    url(r'^(?P<slug>[-\w]+)/articles/page/(?P<page>[\d]+)/$',
        view=views.PostCategoryListView.as_view(), name='product_category_post_list_paginated'),
    url(r'^articles/', include(urlpatterns)),
]
