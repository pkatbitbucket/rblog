import json

from boto.s3.connection import S3Connection
from boto.s3.key import Key
from cfblog.response import render as cfblog_render
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.http import (Http404, HttpResponse, HttpResponseBadRequest,
                         HttpResponsePermanentRedirect, HttpResponseRedirect)
from django.shortcuts import (get_object_or_404, redirect, render,
                              render_to_response)
from django.template import RequestContext
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.generic.list import ListView
from tagging.models import Tag, TaggedItem

import putils
import utils

from .models import CfblogCategory, CfblogContent, Feedback, Subscriber

User = get_user_model()


def subscribe(request):
    return render_to_response("blog/subscribe.html", {
    },
        context_instance=RequestContext(request)
    )


@never_cache
def feedback(request):
    if request.POST:
        if request.POST['submit'].upper() == "FEEDUNSUB":  # Take feedback and unsubscribe
            sf = get_object_or_404(Subscriber, code=request.POST['code'])
            sf.is_active = False
            sf.save()
            feedback = Feedback(
                action="UNSUBSCRIBE",
                content_object=sf,
                person={"email": sf.email, "name": sf.name},
                feedback=dict(request.POST.iterlists())
            )
            feedback.save()
            return render_to_response("blog/message.html", {
                'type': 'UNSUBSCRIBE_FEEDBACK_DONE'
            }, context_instance=RequestContext(request))


@never_cache
def unsubscribe(request, code):
    sf = get_object_or_404(Subscriber, code=code)
    return render_to_response("blog/unsubscribe.html", {
        'sf': sf
    }, context_instance=RequestContext(request))


@never_cache
def get_tags(request):
    term = request.GET.get('term')
    tags = [{'id': t.name, 'label': t.name, 'value': t.name}
            for t in Tag.objects.filter(name__startswith=term)]
    return HttpResponse(json.dumps(tags))


class PostCategoryListView(ListView):
    paginate_by = 10

    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(PostCategoryListView, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        cat = get_object_or_404(CfblogCategory, title=self.kwargs['slug'].title().replace('-', ' '))
        return ["blog/%s_post_list.html" % cat.slug, 'blog/post_list.html']

    def get(self, request, *args, **kwargs):
        category = self.kwargs['slug']
        products = ['two-wheeler-insurance', 'car-insurance', 'health-insurance', 'travel-insurance']
        if category == 'bike-insurance':
            category = 'two-wheeler-insurance'

        if request.path.startswith('/articles') and category in products:
            return HttpResponsePermanentRedirect('/' + category + '/articles/')
        else:
            return super(PostCategoryListView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PostCategoryListView, self).get_context_data(**kwargs)
        cat = get_object_or_404(CfblogCategory, title=self.kwargs['slug'].title().replace('-', ' '))
        context['categories'] = CfblogCategory.objects.all()
        context['recommended_reads'] = putils.get_recommended_reads(4)
        context['jargon_list'] = putils.get_jargon_list(self.request)
        context['products'] = ['two-wheeler-insurance', 'car-insurance', 'health-insurance', 'travel-insurance']
        context['selected_category'] = cat
        context.setdefault('meta', {})
        context['meta']['title'] = '%s - Articles - Page %s - Coverfox.com' % (
            cat.title, context['page_obj'].number)
        if cat.description:
            description = cat.description
        else:
            description = "Read, Share and Review %s " \
                          "Articles from Coverfox.com" % cat.title
        context['meta']['description'] = description
        context['meta']['robots'] = "noindex, follow"
        context['meta']['canonical'] = self.request.path_info.split('page/', 1)[0]
        context['breadcrumb'] = [{'url': "/", 'title': "Home"},
                                 {'url': "/articles/", 'title': "Blog"},
                                 {'url': "/articles/category/" + cat.slug + "/", 'title': cat.title}]
        return context

    def get_queryset(self):
        if self.request.user.is_superuser:
            return CfblogContent.objects.filter(category__title=self.kwargs['slug'].title().replace('-', ' '))
        else:
            return CfblogContent.objects.filter(category__title=self.kwargs['slug'].title().replace('-', ' ')).published()


class PostListView(ListView):
    paginate_by = getattr(settings, 'BLOG_PAGESIZE', 11)
    template_name = "blog/post_list.html"

    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(PostListView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PostListView, self).get_context_data(**kwargs)
        context['recommended_reads'] = putils.get_recommended_reads(4)
        context['products'] = ['two-wheeler-insurance', 'car-insurance', 'health-insurance', 'travel-insurance']
        context.setdefault('meta', {})
        if context['page_obj'].number != 1:
            context['meta'][
                'title'] = 'Articles - Page %s - Coverfox.com' % context['page_obj'].number
        else:
            context['meta']['title'] = 'Articles from Coverfox.com'

        context['meta']['description'] = 'Articles from Coverfox Insurance Blog explaining all there is to know ' \
                                         'about Health, Life, Travel, and Car Insurance'
        context['meta']['canonical'] = self.request.path_info.split('page/', 1)[0]
        context['breadcrumb'] = [
            {'url': "/", 'title': "Home"}, {'url': "/articles/", 'title': "Blog"}]
        return context

    def get_queryset(self):
        if self.request.user.is_superuser:
            return CfblogContent.objects.all()
        else:
            return CfblogContent.objects.published()


class PostTagListView(ListView):
    paginate_by = getattr(settings, 'BLOG_PAGESIZE', 10)
    template_name = "blog/post_list.html"

    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(PostTagListView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PostTagListView, self).get_context_data(**kwargs)
        context['recommended_reads'] = putils.get_recommended_reads(4)
        context['products'] = ['two-wheeler-insurance', 'car-insurance', 'health-insurance', 'travel-insurance']
        context.setdefault('meta', {})
        context['meta']['title'] = 'Coverfox | Articles | %s' % self.kwargs['tag']
        context['meta']['robots'] = "noindex, nofollow"
        return context

    def get_queryset(self):
        if self.request.user.is_superuser:
            # takes a list of tags
            return TaggedItem.objects.get_union_by_model(CfblogContent, [self.kwargs['tag']])
        else:
            # takes a list of tags
            return TaggedItem.objects.get_union_by_model(CfblogContent.objects.published(), [self.kwargs['tag']])


class PostAuthorListView(ListView):
    paginate_by = getattr(settings, 'BLOG_PAGESIZE', 10)
    template_name = "blog/post_list.html"

    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        try:
            self.author = User.objects.filter(
                Q(slug=kwargs["slug"]) & (Q(is_superuser=True) | Q(groups__name='AUTHOR'))
            ).distinct().get()

        except User.DoesNotExist:
            raise Http404
        return super(PostAuthorListView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PostAuthorListView, self).get_context_data(**kwargs)
        context['recommended_reads'] = putils.get_recommended_reads(4)
        context['products'] = ['two-wheeler-insurance', 'car-insurance', 'health-insurance', 'travel-insurance']
        context.setdefault('meta', {})
        context['meta']['title'] = 'Coverfox | Articles | %s' % self.author.get_full_name()
        context['meta']['robots'] = "noindex, follow"
        return context

    def get_queryset(self):
        if self.request.user.is_superuser:
            return CfblogContent.objects.filter(~Q(category__title="Jargon Busting") & Q(author=self.author))
        else:
            return CfblogContent.objects.filter(~Q(category__title="Jargon Busting") & Q(author=self.author)).published()


def legacy_post_view(request, category, slug):

    if request.path.startswith('/articles'):
        if category == 'bike-insurance':
            category = 'two-wheeler-insurance'

        if category in ['two-wheeler-insurance', 'car-insurance', 'health-insurance', 'travel-insurance']:
            return HttpResponsePermanentRedirect(u'/{}/articles/{}/'.format(category, slug))

    return cfblog_render(request)


def subscribe_success(request, code):
    subscriber = get_object_or_404(Subscriber, code=code)
    return render_to_response('blog/subscribe_success.html', {
        'recommended_reads': putils.get_recommended_reads(4),
        'subscriber': subscriber,
    },
        context_instance=RequestContext(request)
    )


def search(request):
    return render_to_response('blog/post_search.html', {},
                              context_instance=RequestContext(request)
                              )


@never_cache
def jargon_search(request):
    tagged_posts = []
    if request.POST:
        search_term = request.POST.get('jargon', '')
    else:
        search_term = request.GET.get('jq', '')
    if search_term:
        if request.user.is_superuser:
            tagged_posts = [p for p in CfblogContent.objects.filter(
                category__title="Jargon Busting") if search_term in p.tags]
        else:
            tagged_posts = [p for p in CfblogContent.objects.published().filter(
                category__title="Jargon Busting") if search_term in p.tags]
        if not tagged_posts:
            return HttpResponseRedirect('/articles/jargon_search/?q=%s' % search_term)
        elif len(tagged_posts) == 1:
            tp = tagged_posts[0]
            return redirect('%s' % tp.get_absolute_url(), permanent=True)
    tagged_posts = list(set(tagged_posts))
    return render_to_response('blog/jargon_search.html', {
        'tagged_posts': tagged_posts,
        'jargon_list': putils.get_jargon_alpha_dict(request),
        'searched_term': search_term,
    }, context_instance=RequestContext(request))


@utils.profile_type_only('AUTHOR')
def upload_to_s3(request):
    bucket = settings.CFBLOG_BUCKET
    connection = S3Connection(settings.CFBLOG_ACCESS_KEY, settings.CFBLOG_SECRET_ACCESS_KEY)
    try:
        bucket = connection.get_bucket(bucket)
    except:
        return HttpResponseBadRequest()

    if request.method == 'POST':
        try:
            s3_file = request.FILES['s3_file']
        except KeyError:
            return HttpResponseBadRequest()
        else:
            k = Key(bucket)
            k.key = s3_file.name
            k.set_contents_from_file(s3_file)
            return HttpResponse(u"https://{}.s3.amazonaws.com/{}".format(bucket.name, k.key))

    return render(request,
                  "blog/image_upload.html",
                  {'current_files': [{"name": k.key, "url": u"https://{}.s3.amazonaws.com/{}".format(bucket.name, k.key)}
                                     for k in bucket.get_all_keys()]})
