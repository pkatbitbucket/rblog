from django.contrib import admin
from django.contrib.contenttypes.admin import GenericStackedInline
from django import forms
from redactor.widgets import RedactorEditor

from seo.models import SEOMetadata
from .models import BrandPage, BrandProduct, BrandProductPage


class BrandPageAdminForm(forms.ModelForm):

    class Meta:
        model = BrandPage
        fields = "__all__"

        widgets = {
            'description': RedactorEditor(),
            'content': RedactorEditor(),
        }


class BrandProductPageAdminForm(forms.ModelForm):

    class Meta:
        model = BrandProduct
        fields = "__all__"
        widgets = {
            'scope_of_cover': RedactorEditor(),
            'variants': RedactorEditor(),
            'introduction': RedactorEditor(),
            'highlight': RedactorEditor(),
            'eligibility': RedactorEditor(),
            'coverage_most_important': RedactorEditor(),
            'coverage_good_to_have': RedactorEditor(),
            'coverage_value_adds': RedactorEditor(),
            'addons': RedactorEditor(),
            'exclusions': RedactorEditor(),
            'review': RedactorEditor(),
        }


class BrandProductAdminForm(forms.ModelForm):

    class Meta:
        model = BrandProduct
        fields = "__all__"

        widgets = {
            'description': RedactorEditor(),
        }


class SEOMetaDataInline(GenericStackedInline):
    model = SEOMetadata
    extra = 1
    max_num = 1


class BrandProductInline(admin.StackedInline):
    model = BrandProduct
    form = BrandProductAdminForm
    extra = 1
    classes = ('grp-collapse grp-open',)
    inline_classes = ('grp-collapse grp-open',)
    exclude = ('slug', 'highlight', 'coverage_most_important', 'coverage_good_to_have', 'coverage_value_adds',
               'exclusions', 'variants', 'review', 'scope_of_cover', 'eligibility', 'published')


class BrandPageAdmin(admin.ModelAdmin):
    form = BrandPageAdminForm

    list_display = ('title', 'slug', 'insurer', 'type',)
    prepopulated_fields = {'slug': ('title',), }

    def get_inline_instances(self, request, obj=None):
        self.inlines = [SEOMetaDataInline]
        if not (obj and obj.type in ['two-wheeler', 'motor']):
            self.inlines.append(BrandProductInline)
        return super(BrandPageAdmin, self).get_inline_instances(request, obj)


class BrandProductPageAdmin(admin.ModelAdmin):
    form = BrandProductPageAdminForm
    list_display = ('__unicode__', 'slug', 'category', 'published', 'link',)
    list_filter = ('published',)
    list_editable = ('published',)
    prepopulated_fields = {'slug': ('title',), }
    exclude = ('description', 'priority')
    search_fields = ('title', 'brand_page__title', 'slug')

    def link(self, obj):
        return "<a target='_blank' href='%s'>%s</a>" % (obj.absolute_url(), "View on site")
    link.allow_tags = True

    def category(self, obj):
        return obj.brand_page.type

admin.site.register(BrandPage, BrandPageAdmin)
admin.site.register(BrandProductPage, BrandProductPageAdmin)
