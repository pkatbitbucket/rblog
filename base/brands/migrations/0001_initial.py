# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import brands.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BrandPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('slug', models.SlugField(max_length=100)),
                ('insurer', models.CharField(max_length=100)),
                ('type', models.CharField(max_length=100, choices=[(b'health', b'health'), (
                    b'motor', b'motor'), (b'two-wheeler', b'two-wheeler'), (b'travel', b'travel')])),
                ('content', models.TextField(null=True, blank=True)),
                ('description', models.TextField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='BrandProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('full_title', models.CharField(
                    max_length=100, null=True, blank=True)),
                ('slug', models.SlugField(max_length=200)),
                ('category', models.CharField(max_length=100, choices=[(b'health', ((b'health-individual', b'Individual Health Plans'), (b'health-family', b'Family Health Plans'), (b'health-senior', b'Senior Citizen Health Plans'), (b'others', b'Other Plans'))), (b'motor', ((b'motor-four-wheelers', b'Four Wheeler Plans'), (b'motor-two-wheelers', b'Two Wheeler Plans'), (b'motor-insurance', b'Motor Insurance Plans'), (
                    b'commericial-insurance', b'Commercial Vehicle Plans'))), (b'two-wheeler', ((b'motor-two-wheelers', b'Two Wheeler Plans'),)), (b'travel', ((b'travel-individual', b'Individual Travel Plans'), (b'travel-family', b'Family Travel Plans'), (b'travel-senior', b'Senior Citizen Travel Plans'), (b'travel-annual', b'Annual Trip Plans'), (b'travel-student', b'Student Travel Plans')))])),
                ('description', models.TextField(null=True)),
                ('priority', models.PositiveIntegerField(default=1)),
                ('highlight', models.TextField(null=True, blank=True)),
                ('eligibility', models.TextField(null=True, blank=True)),
                ('coverage_most_important', models.TextField(null=True, blank=True)),
                ('coverage_good_to_have', models.TextField(null=True, blank=True)),
                ('coverage_value_adds', models.TextField(null=True, blank=True)),
                ('exclusions', models.TextField(null=True, blank=True)),
                ('review', models.TextField(null=True, blank=True)),
                ('published', models.BooleanField(default=False)),
                ('variants', models.TextField(null=True, blank=True)),
                ('scope_of_cover', models.TextField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='BrandProductPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('full_title', models.CharField(
                    max_length=100, null=True, blank=True)),
                ('slug', models.SlugField(max_length=200)),
                ('priority', models.PositiveIntegerField(default=1)),
                ('introduction', models.TextField(null=True, blank=True)),
                ('highlight', models.TextField(null=True, blank=True)),
                ('eligibility', models.TextField(null=True, blank=True)),
                ('coverage_most_important', models.TextField(null=True, blank=True)),
                ('coverage_good_to_have', models.TextField(null=True, blank=True)),
                ('coverage_value_adds', models.TextField(null=True, blank=True)),
                ('addons', models.TextField(null=True, blank=True)),
                ('exclusions', models.TextField(null=True, blank=True)),
                ('review', models.TextField(null=True, blank=True)),
                ('rating', models.FloatField(blank=True, null=True, choices=[(1, b'1'), (1.5, b'1.5'), (
                    2, b'2'), (2.5, b'2.5'), (3, b'3'), (3.5, b'3.5'), (4, b'4'), (4.5, b'4.5'), (5, b'5')])),
                ('published', models.BooleanField(default=False)),
                ('variants', models.TextField(null=True, blank=True)),
                ('scope_of_cover', models.TextField(null=True, blank=True)),
                ('brochure_link', models.FileField(max_length=1000, null=True,
                                                   upload_to=brands.models.brochure_upload_to, blank=True)),
                ('policy_document', models.FileField(max_length=1000, null=True,
                                                     upload_to=brands.models.policy_upload_to, blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('brand_page', models.ForeignKey(to='brands.BrandPage')),
            ],
        ),
        migrations.AddField(
            model_name='brandproduct',
            name='link_to',
            field=models.ForeignKey(
                verbose_name=b'Link To', blank=True, to='brands.BrandProductPage', null=True),
        ),
        migrations.AddField(
            model_name='brandproduct',
            name='page',
            field=models.ForeignKey(
                related_name='products', verbose_name=b'Brand', to='brands.BrandPage'),
        ),
    ]
