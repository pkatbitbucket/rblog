# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0001_initial'),
        ('brands', '0001_initial'),
        ('wiki', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='brandpage',
            name='_insurer',
            field=models.ForeignKey(
                verbose_name=b'points to', blank=True, to='wiki.Insurer', null=True),
        ),
        migrations.AddField(
            model_name='brandpage',
            name='_motor_insurer',
            field=models.ForeignKey(verbose_name=b'points to (for motor insurers)',
                                    blank=True, to='motor_product.Insurer', null=True),
        ),
        migrations.AlterUniqueTogether(
            name='brandproductpage',
            unique_together=set([('brand_page', 'slug')]),
        ),
        migrations.AlterUniqueTogether(
            name='brandproduct',
            unique_together=set([('page', 'slug')]),
        ),
        migrations.AlterUniqueTogether(
            name='brandpage',
            unique_together=set([('slug', 'type')]),
        ),
    ]
