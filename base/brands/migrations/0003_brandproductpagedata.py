# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('brands', '0002_auto_20150807_1908'),
    ]

    operations = [
        migrations.CreateModel(
            name='BrandProductPageData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('coverage_most_important_json', jsonfield.fields.JSONField(default={}, blank=True)),
                ('coverage_good_to_have_json', jsonfield.fields.JSONField(default={}, blank=True)),
                ('coverage_value_adds_json', jsonfield.fields.JSONField(default={}, blank=True)),
                ('exclusions_json', jsonfield.fields.JSONField(default={}, blank=True)),
                ('brand_product_page', models.ForeignKey(to='brands.BrandProductPage')),
            ],
        ),
    ]
