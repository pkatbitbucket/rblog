# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
from xml.etree import cElementTree as ET
import re
from collections import OrderedDict


def populate_data(apps, schema_editor):
    brand_products = apps.get_model("brands", "BrandProductPage").objects.all()
    brand_products_data = apps.get_model("brands", "BrandProductPageData")
    for brand_product in brand_products:
        print brand_product.id, brand_product.slug
        b = brand_products_data.objects.get_or_create(
            brand_product_page=brand_product            
        )
        #print b
        b[0].coverage_most_important_json=process_data(brand_product.coverage_most_important)
        b[0].coverage_good_to_have_json=process_data(brand_product.coverage_good_to_have)
        b[0].coverage_value_adds_json=process_data(brand_product.coverage_value_adds)
        b[0].exclusions_json=process_data(brand_product.exclusions)
        b[0].save()



def process_data(text):
    junk_tags = [
        r"<li[\w]*>[\s]*</li>",
        r"<ul[\w]*>[\s]*</ul>",
        r"<span[\w]*>[\s]*</span>",
        r"<p[\w]*>[\s]*</p>",
        r"<br>"
    ]
    t = text
    cleaned = 0
    while not cleaned:
        found = 0
        for re_tag in junk_tags:
            reg = re.compile(re_tag, re.MULTILINE)
            if reg.findall(t):
                found += 1
                t, _ = reg.subn('', t)
        if not found:
            cleaned = 1
    # print t
    t = t.encode('utf-8')
    try:
        root = ET.fromstring(t)
    except ET.ParseError:
        t = '<div>{}</div>'.format(t)
        root = ET.fromstring(t)
    data = OrderedDict()
    var = 1
    if(len(root) == 0):
        data['No Title'] = root.text
    for child in root:
        if child.text or child[0].tail:
            header = child.text
            children = child
            if not header:
                children = child[1:]
                if child[0].tag == "a":
                    header = u"<a href='{0}'>{1}</a>{2}".format(child[0].attrib['href'], child[0].text or "", child[0].tail)
                else:
                    header = u"<{0}>{1}</{0}>{2}".format(child[0].tag, child[0].text or "", child[0].tail)

            dump = re.split(ur"\u2013\s?", header)
            if len(dump) != 2:
                dump = header.split("- ", 1)
            if len(dump) == 1:
                dump.insert(0, 'No Title'+str(var))
                var += 1
            # print dump
            html = str()
            for _child in children:
                if _child.tag not in ['br', 'span']:
                    html += gen_html(_child)
            data[dump[0]] = "{}{}".format(dump[1].lstrip(), html)
    return data


def gen_html(ele):
    var = ""
    for child in ele:
        var += gen_html(child)
    if ele.tag == "a":
        return "<{0} href='{1}'>{2}{3}</{0}>{4}".format(ele.tag, ele.attrib['href'], ele.text or "", var, ele.tail or "")
    return "<{0}>{1}{2}</{0}>{3}".format(ele.tag, ele.text or "", var, ele.tail or "")


def dummy_back(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('brands', '0003_brandproductpagedata'),
    ]

    operations = [
        migrations.RunPython(populate_data, reverse_code=dummy_back),
    ]
