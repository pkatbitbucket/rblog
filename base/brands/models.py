from collections import OrderedDict
from xml.etree import cElementTree as ET
import re

from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from django.core.urlresolvers import reverse
from jsonfield import JSONField

from wiki.models import Insurer
from motor_product.models import Insurer as MotorInsurer
from seo.models import SEOMetadata


class BrandPage(models.Model):
    type_choices = (
        ('health', 'health'),
        ('motor', 'motor'),
        ('two-wheeler', 'two-wheeler'),
        ('travel', 'travel'),
    )
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    insurer = models.CharField(max_length=100)
    _insurer = models.ForeignKey(
        Insurer, verbose_name='points to', blank=True, null=True)
    _motor_insurer = models.ForeignKey(
        MotorInsurer, verbose_name='points to (for motor insurers)', blank=True, null=True)
    type = models.CharField(max_length=100, choices=type_choices)
    content = models.TextField(null=True, blank=True)
    description = models.TextField()
    seo_tags = GenericRelation(SEOMetadata)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.title

    class Meta:
        unique_together = ('slug', 'type')

    def get_absolute_url(self):
        if self.type == 'travel':
            return ''
        else:
            return reverse(self.type + '_' + 'brand_page', args=[self.slug])


class BrandProduct(models.Model):
    page = models.ForeignKey(
        BrandPage, related_name='products', verbose_name='Brand')
    link_to = models.ForeignKey(
        'BrandProductPage', verbose_name='Link To', null=True, blank=True)

    category_choices = (
        ('health', (('health-individual', 'Individual Health Plans'),
                    ('health-family', 'Family Health Plans'),
                    ('health-senior', 'Senior Citizen Health Plans'),
                    ('others', 'Other Plans'))),
        ('motor',   (('motor-four-wheelers', 'Four Wheeler Plans'),
                     ('motor-two-wheelers', 'Two Wheeler Plans'),
                     ('motor-insurance', 'Motor Insurance Plans'),
                     ('commericial-insurance', 'Commercial Vehicle Plans'))),
        ('two-wheeler',   (('motor-two-wheelers', 'Two Wheeler Plans'), ), ),
        ('travel', (('travel-individual', 'Individual Travel Plans'),
                    ('travel-family', 'Family Travel Plans'),
                    ('travel-senior', 'Senior Citizen Travel Plans'),
                    ('travel-annual', 'Annual Trip Plans'),
                    ('travel-student', 'Student Travel Plans')))
    )
    title = models.CharField(max_length=100)
    full_title = models.CharField(max_length=100, blank=True, null=True)
    slug = models.SlugField(max_length=200)
    category = models.CharField(max_length=100, choices=category_choices)
    description = models.TextField(null=True)
    priority = models.PositiveIntegerField(default=1)
    highlight = models.TextField(null=True, blank=True)
    eligibility = models.TextField(null=True, blank=True)
    coverage_most_important = models.TextField(null=True, blank=True)
    coverage_good_to_have = models.TextField(null=True, blank=True)
    coverage_value_adds = models.TextField(null=True, blank=True)
    exclusions = models.TextField(null=True, blank=True)
    review = models.TextField(null=True, blank=True)
    published = models.BooleanField(default=False)
    variants = models.TextField(null=True, blank=True)
    scope_of_cover = models.TextField(null=True, blank=True)

    class Meta:
        unique_together = ('page', 'slug')

    def absolute_url(self):
        return reverse("brand_product_page", kwargs={'insurer_slug': self.page.slug, 'product_slug': self.slug})

    def __unicode__(self):
        return ("%s - %s" % (self.page.title, self.title)) if not self.full_title else self.full_title

    def category_display(self):
        category_choices = dict(self.category_choices)[self.page.type]
        category = dict(category_choices)[self.category]
        return category


def brochure_upload_to(instance, filename):
    return "uploads/policy-brochures/" + filename.lower()


def policy_upload_to(instance, filename):
    return "uploads/policy-documents/" + filename.lower()


class BrandProductPage(models.Model):
    rating_choices = (
        (1, '1'),
        (1.5, '1.5'),
        (2, '2'),
        (2.5, '2.5'),
        (3, '3'),
        (3.5, '3.5'),
        (4, '4'),
        (4.5, '4.5'),
        (5, '5'),
    )

    brand_page = models.ForeignKey(BrandPage)
    title = models.CharField(max_length=100)
    full_title = models.CharField(max_length=100, blank=True, null=True)
    slug = models.SlugField(max_length=200)
    priority = models.PositiveIntegerField(default=1)
    introduction = models.TextField(null=True, blank=True)
    highlight = models.TextField(null=True, blank=True)
    eligibility = models.TextField(null=True, blank=True)
    coverage_most_important = models.TextField(null=True, blank=True)
    coverage_good_to_have = models.TextField(null=True, blank=True)
    coverage_value_adds = models.TextField(null=True, blank=True)
    addons = models.TextField(null=True, blank=True)
    exclusions = models.TextField(null=True, blank=True)
    review = models.TextField(null=True, blank=True)
    rating = models.FloatField(null=True, blank=True, choices=rating_choices)
    published = models.BooleanField(default=False)
    variants = models.TextField(null=True, blank=True)
    scope_of_cover = models.TextField(null=True, blank=True)
    brochure_link = models.FileField(
        upload_to=brochure_upload_to, max_length=1000, null=True, blank=True)
    policy_document = models.FileField(
        upload_to=policy_upload_to, max_length=1000, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('brand_page', 'slug')

    def absolute_url(self):
        return reverse("brand_product_page", kwargs={'insurer_slug': self.brand_page.slug, 'product_slug': self.slug})

    def __unicode__(self):
        return ("%s - %s" % (self.brand_page.title, self.title)) if not self.full_title else self.full_title

    def save(self, *args, **kwargs):
        super(BrandProductPage, self).save(*args, **kwargs)
        b, _ = BrandProductPageData.objects.get_or_create(brand_product_page=self)

        b.coverage_most_important_json = process_data(self.coverage_most_important)
        b.coverage_good_to_have_json = process_data(self.coverage_good_to_have)
        b.coverage_value_adds_json = process_data(self.coverage_value_adds)
        b.exclusions_json = process_data(self.exclusions)
        b.save()


class BrandProductPageData(models.Model):
    brand_product_page = models.ForeignKey(BrandProductPage)
    coverage_most_important_json = JSONField(blank=True, default={}, load_kwargs={'object_pairs_hook': OrderedDict})
    coverage_good_to_have_json = JSONField(blank=True, default={}, load_kwargs={'object_pairs_hook': OrderedDict})
    coverage_value_adds_json = JSONField(blank=True, default={}, load_kwargs={'object_pairs_hook': OrderedDict})
    exclusions_json = JSONField(blank=True, default={}, load_kwargs={'object_pairs_hook': OrderedDict})

    def __unicode__(self):
        return self.brand_product_page.full_title or ("%s - %s" % (self.brand_product_page.brand_page.title,
                                                                   self.brand_product_page.title))


def process_data(text):
    junk_tags = [
        r"<li[\w]*>[\s]*</li>",
        r"<ul[\w]*>[\s]*</ul>",
        r"<span[\w]*>[\s]*</span>",
        r"<p[\w]*>[\s]*</p>",
        r"<br>"
    ]
    t = text
    cleaned = 0
    while not cleaned:
        found = 0
        for re_tag in junk_tags:
            reg = re.compile(re_tag, re.MULTILINE)
            if reg.findall(t):
                found += 1
                t, _ = reg.subn('', t)
        if not found:
            cleaned = 1
    # print t
    t = t.encode('utf-8')
    try:
        root = ET.fromstring(t)
    except ET.ParseError:
        t = '<div>{}</div>'.format(t)
        root = ET.fromstring(t)
    data = OrderedDict()
    var = 1
    if(len(root) == 0):
        data['No Title'] = root.text
    for child in root:
        if child.text or child[0].tail:
            header = child.text
            children = child
            if not header:
                children = child[1:]
                if child[0].tag == "a":
                    header = u"<a href='{0}'>{1}</a>{2}".format(child[0].attrib['href'], child[0].text or "", child[0].tail)
                else:
                    header = u"<{0}>{1}</{0}>{2}".format(child[0].tag, child[0].text or "", child[0].tail)

            dump = re.split(ur"\u2013\s?", header)
            if len(dump) != 2:
                dump = header.split("- ", 1)
            if len(dump) == 1:
                dump.insert(0, 'No Title'+str(var))
                var += 1
            # print dump
            html = str()
            for _child in children:
                if _child.tag not in ['br', 'span']:
                    html += gen_html(_child)
            data[dump[0]] = "{}{}".format(dump[1].lstrip(), html)
    return data


def gen_html(ele):
    var = ""
    for child in ele:
        var += gen_html(child)
    if ele.tag == "a":
        return "<{0} href='{1}'>{2}{3}</{0}>{4}".format(ele.tag, ele.attrib['href'], ele.text or "", var, ele.tail or "")
    return "<{0}>{1}{2}</{0}>{3}".format(ele.tag, ele.text or "", var, ele.tail or "")
