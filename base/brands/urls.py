from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^health-insurance/(?P<insurer_slug>[\w-]+)-health-insurance/$',
        views.brand_page, {'vertical': 'health'}, name='health_brand_page'),
    url(r'^health-insurance/(?P<insurer_slug>[\w-]+)/$',
        views.redirect_to_brand_page, {'vertical': 'health'}, name='health_brand_page_old'),
    url(r'^car-insurance/(?P<insurer_slug>[\w-]+)-car-insurance/$', views.brand_page,
        {'vertical': 'motor', 'template': 'company-two-wheeler-page.html'}, name='motor_brand_page'),
    url(r'^car-insurance/(?P<insurer_slug>[\w-]+)/$', views.redirect_to_brand_page,
        {'vertical': 'motor'}, name='motor_brand_page_old'),
    url(r'^two-wheeler-insurance/(?P<insurer_slug>[\w-]+)/$', views.brand_page,
        {'vertical': 'two-wheeler', 'template': 'company-two-wheeler-page.html'}, name='two-wheeler_brand_page'),
    url(r'^health-insurance/(?P<insurer_slug>[\w-]+)-health-insurance/(?P<product_slug>[\w-]+)/$',
        views.health_product_page, name='brand_product_page'),
    url(r'^health-insurance/(?P<insurer_slug>[\w-]+)/(?P<product_slug>[\w-]+)/$',
        views.redirect_to_health_product_page, name='brand_product_page_old'),
]
