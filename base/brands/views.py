import settings

from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import Http404, HttpResponse, HttpResponseRedirect, HttpResponsePermanentRedirect
from django.shortcuts import render

from django.db.models import Q
from django.conf import settings
from django import template
from django.contrib.auth.decorators import login_required
import json
import hashlib
from django.views.decorators.cache import never_cache
from django.core.urlresolvers import reverse
from collections import OrderedDict
from brands.models import BrandPage, BrandProduct, BrandProductPage, BrandProductPageData
from xml.etree import cElementTree as ET
import re
import json
from flatpages.models import SEOPage
from cfblog.views import cms_page_index


insurer_logo ={
    'apollomunich' : 'apollo-munich-health-insurance.png',
    'apollo-munich' : 'apollo-munich-health-insurance.png',
    'bajajallianz' : 'bajaj-allianz-general-insurance.png',
    'bajaj-allianz' : 'bajaj-allianz-general-insurance.png',
    'bhartiaxa' : 'bharti-axa-general-insurance.png',
    'bharti-axa' : 'bharti-axa-general-insurance.png',
    'cholamandalam-ms' : 'chola-ms-health-insurance.png',
    'cignattk' : 'cigna-health-insurance.jpg',
    'cigna-ttk' : 'cigna-health-insurance.jpg',
    'hdfcergo' : 'hdfc-ergo-general-insurance.png',
    'hdfc-ergo' : 'hdfc-ergo-general-insurance.png',
    'icicilombard' : 'icici-lombard-health-insurance.png',
    'icici-lombard' : 'icici-lombard-health-insurance.png',
    'iffcotokio' : 'iffco-tokio-general-insurance.png',
    'iffco-tokio' : 'iffco-tokio-general-insurance.png',
    'l-and-t' : 'larsen-and-tubro-general-insurance.png',
    'maxbupa' : 'max-bupa-health-insurance.png',
    'max-bupa' : 'max-bupa-health-insurance.png',
    'new-india-assurance' : 'new-india-general-insurance.png',
    'oriental' : 'oriental-health-insurance.png',
    'reliance' : 'reliance-general-insurance.png',
    'religare' : 'religare-general-insurance.png',
    'star-health-and-allied-insurance' : 'star-health-general-insurance.png',
    'tataaig' : 'tata-aig-health-insurance.png',
    'tata-aig' : 'tata-aig-health-insurance.png',
    'universalsompo' : 'universal-sompo-general-insurance.png',
    'universal-sompo' : 'universal-sompo-general-insurance.png'
}

def brand_page(request, insurer_slug, vertical='health', template='company-page.html'):
    try:
        return cms_page_index(request)
    except Http404:
        pass

    brand_page = get_object_or_404(BrandPage, slug=insurer_slug, type=vertical)
    try:
        seo = brand_page.seo_tags.all()[0]
    except IndexError:
        seo = None

    categories = dict(BrandProduct.category_choices)[brand_page.type]
    breadcrumb = []
    breadcrumb.append({"title": "Home", "url": "/"})
    if vertical == "health":
        breadcrumb.append({"title": "Health Insurance",
                           "url": "/health-insurance/"})
        breadcrumb.append({"title": tirm_title(
            "Health Insurance", brand_page.title), "url": breadcrumb[1]['url'] + insurer_slug + "/"})
    elif vertical == "motor":
        breadcrumb.append({"title": "Car Insurance", "url": "/car-insurance/"})
        breadcrumb.append({"title": tirm_title(
            "Car Insurance", brand_page.title), "url": breadcrumb[1]['url'] + insurer_slug + "/"})
    else:
        breadcrumb.append({"title": "Two Wheeler Insurance",
                           "url": "/two-wheeler-insurance/"})
        breadcrumb.append({"title": tirm_title("Two Wheeler Insurance",
                                               brand_page.title), "url": breadcrumb[1]['url'] + insurer_slug + "/"})

    context = {
        'campaign': vertical,
        'insurer_slug': insurer_slug,
        'brand_page': brand_page,
        'breadcrumb': breadcrumb,
        'seo': seo,
        'products': {k[1]: [b for b in brand_page.products.all() if b.category == k[0]] for k in categories}
    }
    return render(request, template, context)

# temporary view product


def product_page(request, insurer_slug, product_slug):
    brand_product = get_object_or_404(BrandProductPage, brand_page__slug=insurer_slug,
                                      slug=product_slug, published=True, brand_page__type='health')
    breadcrumb = []
    breadcrumb.append({"title": "Home", "url": "/"})
    breadcrumb.append({"title": "Health Insurance",
                       "url": "/health-insurance/"})
    breadcrumb.append({"title": tirm_title("Health Insurance", brand_product.brand_page.title),
                       "url": breadcrumb[1]['url'] + insurer_slug + "/"})
    breadcrumb.append({"title": brand_product.title, "url": breadcrumb[
                      2]['url'] + product_slug + "/"})
    rating = brand_product.rating if brand_product.rating else 0
    context = {
        'brand_product': brand_product,
        'rating': round(rating),
        'breadcrumb': breadcrumb,
        'others': BrandProductPage.objects.filter(brand_page=brand_product.brand_page, published=True).exclude(pk=brand_product.pk)
    }
    return render(request, "health-product-page.html", context)


def tirm_title(type, title):
    return title.replace(" "+type,"")

def health_product_page(request, insurer_slug, product_slug):
    try:
        return cms_page_index(request)
    except Http404:
        pass

    brand_product = get_object_or_404(BrandProductPage, brand_page__slug=insurer_slug, slug=product_slug, published=True, brand_page__type='health')
    brand_product_data=get_object_or_404(BrandProductPageData, brand_product_page=brand_product)
    breadcrumb = []
    url=request.path    
    title = " ".join([word.capitalize() for word in product_slug.split("-")])

    breadcrumb.append({"title": "Home", "url": "/"})
    breadcrumb.append({"title": "Health Insurance","url": "/health-insurance/"})
    breadcrumb.append({"title": tirm_title("Health Insurance", brand_product.brand_page.title),"url": breadcrumb[1]['url']+insurer_slug+"/" })
    breadcrumb.append({"title": title,"url": breadcrumb[2]['url']+product_slug+"/" })
    rating = brand_product.rating if brand_product.rating  else 0

    coverage = dict()
    coverage['Most Important'] = OrderedDict()
    coverage['Most Important'] = brand_product_data.coverage_most_important_json #json.loads(brand_product.coverage_most_important_json) #process_data(brand_product.coverage_most_important)
    coverage['Good to have'] = OrderedDict()
    coverage['Good to have'] = brand_product_data.coverage_good_to_have_json #json.loads(brand_product.coverage_good_to_have_json) #process_data(brand_product.coverage_good_to_have)
    coverage['Value Adds'] = OrderedDict()
    coverage['Value Adds'] = brand_product_data.coverage_value_adds_json #json.loads(brand_product.coverage_value_adds_json) #process_data(brand_product.coverage_value_adds)
    exclusions = OrderedDict()
    exclusions = brand_product_data.exclusions_json #json.loads(brand_product.exclusions_json) #process_data(brand_product.exclusions)

    context = {
            'brand_product': brand_product,
            'logo': insurer_logo[insurer_slug],
            'rating': round(rating),
            'breadcrumb': breadcrumb,
            'others': BrandProductPage.objects.filter(brand_page=brand_product.brand_page, published=True).exclude(pk=brand_product.pk),
            'coverage': coverage,
            'exclusions': exclusions
            #'scope_of_cover':brand_product.scope_of_cover,
    }

    return render(request, "health-product-page-new.html", context)

def redirect_to_brand_page(request, insurer_slug, vertical):
    brand_page = get_object_or_404(BrandPage, slug=insurer_slug, type=vertical)
    return HttpResponsePermanentRedirect('/{0}-insurance/{1}-{0}-insurance/'.format( vertical if vertical == 'health' else 'car', insurer_slug))


def redirect_to_health_product_page(request, insurer_slug, product_slug):    
    brand_product = get_object_or_404(BrandProductPage, brand_page__slug=insurer_slug, slug=product_slug, published=True, brand_page__type='health')
    return HttpResponsePermanentRedirect('/health-insurance/{}-health-insurance/{}'.format(insurer_slug, product_slug))