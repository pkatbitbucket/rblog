from django.apps import AppConfig


class CampaignAppConfig(AppConfig):
    name = 'campaign'

    def ready(self):
        from .landing_pages import motor, bike
        motor.populate_tparty()
        motor.populate_tparty_logo_mapping()
        bike.populate_tparty()
        bike.populate_tparty_logo_mapping()
