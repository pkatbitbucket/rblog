import random
import urllib
import urlparse

from django.core.urlresolvers import reverse
from django.http.response import (HttpResponsePermanentRedirect,
                                  HttpResponseRedirect)
from openpyxl import load_workbook
from openpyxl.utils.exceptions import InvalidFileException

from .landing_pages import DEFAULT_LPS, VARIATIONS
from .models import AdcopyCreative

try:
    from math import gcd
except ImportError:
    from fractions import gcd


def route_lp(the_func):
    def route_view(self, request, *args, **kwargs):
        if self.product == 'health' and self.view_id == 'ebook':
            return HttpResponsePermanentRedirect('/health-insurance/parents/ebook/')

        self.get_data = data = request.GET
        # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> #
        # THIS IS AN EXPERIMENTTIONAL CODE
        # DISPOSE AFTER USAGE
        if self.product == 'motor':
            if data.get('brand'):
                self.view_id = 'brand'
            else:
                network = data.get('network', 'direct').lower()
                category = data.get('category', '').lower()
                url_parts = list(urlparse.urlparse(request.build_absolute_uri()))
                if self.device == 'desktop' and self.view_id != 'get-cover':
                    if not (network.startswith('facebook') or
                            network in ['affiliate', 'team-bhp'] or
                            network == 'display' or
                            (network == 'search' and category == 'sitelink')):
                        url_parts[2] = '/lp/car-insurance/get-cover/'
                        return HttpResponseRedirect(urlparse.urlunparse(url_parts))
                elif self.device == 'mobile':
                    if not (network.startswith('facebook') or
                            network in ['affiliate', 'twitter'] or
                            network == 'display' or
                            network == 'gsp' or
                            (network == 'search' and category == 'sitelink')):
                        url_parts[2] = '/motor/car-insurance/'
                        return HttpResponseRedirect(urlparse.urlunparse(url_parts))
        # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> #

        if self.view_class not in self.lp_module.__all__:
            self.view_id = DEFAULT_LPS[self.product]

        variations = VARIATIONS.get(self.device).get(self.product, {}).get(self.view_id)
        if variations:
            url_parts = list(urlparse.urlparse(request.build_absolute_uri()))
            query = data.dict()

            # seed will make sure that for the same session outcome will be same
            random.seed(request.TRACKER.session_key)
            number = get_random_key({i: value['chance']
                                     for i, value in enumerate(variations)})
            variation = variations[number]
            self.view_id = variation.get('view_id', self.view_id)

            def redirect():
                query.update(params)
                url = reverse(
                    'campaign:{}_lp'.format(self.product), args=(self.view_id,)
                )
                url_parts[2] = url
                url_parts[4] = urllib.urlencode(query)
                return HttpResponseRedirect(urlparse.urlunparse(url_parts))

            found = False
            params = variation.get('params', {})
            for k, v in params.items():
                if k in query:
                    found = True
                    if v == query[k]:
                        continue
                    return redirect()
            else:
                if not found:
                    return redirect()

        kwargs.update(data.dict())
        kwargs['view_id'] = self.view_id

        return the_func(self, request, *args, **kwargs)
    return route_view


def get_random_key(dictionary):
    dictionary = normalize_dict_values(dictionary)
    return random.choice(
        [key for key, value in dictionary.items() for _ in range(value)]
    )


def normalize_dict_values(dictionary):
    l_gcd = gcd_of_list(dictionary.values())
    return {key: value / l_gcd for key, value in dictionary.items()}


def gcd_of_list(l):
    return reduce(gcd, l) if len(l) > 1 else 1


_SAMPLE_ADCOPY_JSON = {
    "page_title": "page title",
    "adcopy_data": {
        "top_line": " ",
        "desc1": " ",
        "desc2": " "
    },
    "main_body": {
        "heading": "main body heading",
        "sub_heading": "main body sub heading",
        "points": [
            "main body point 1",
            "main body point 2",
            "main body point 3",
            "main body point 4"
        ],
    },
    "form": {
        "heading": "form heading",
        "sub_heading": "form sub heading",
        "closure_text": "form body text",
        "button_text": "button text"
    },
}


def marry_ad_copy(path_to_excel):
    """
    sample json data for the creative
    {
      "page_title": "page title",
      "adcopy_data": {
        "top_line": " ",
        "desc1": " ",
        "desc2": " "
      },
      "main_body": {
        "heading": "main body heading",
        "sub_heading": "main body sub heading",
        "points": [
          "main body point 1",
          "main body point 2",
          "main body point 3",
          "main body point 4"
        ],
      },
      "form": {
        "heading": "form heading",
        "sub_heading": "form sub heading",
        "closure_text": "form body text",
        "button_text": "button text"
      },
    }
    """
    # default structure of the input excel.
    default_structure = {
        'advert_id': lambda x: AdcopyCreative.objects.get_or_create(creative_id=x),
        'top_line': lambda x: final_json['adcopy_data'].update({'top_line': x}) if final_json.get('adcopy_data') else final_json.update({'adcopy_data': {'top_line': x}}),
        'desc1': lambda x: final_json['adcopy_data'].update({'desc1': x}) if final_json.get('adcopy_data') else final_json.update({'adcopy_data': {'desc1': x}}),
        'desc2': lambda x: final_json['adcopy_data'].update({'desc2': x}) if final_json.get('adcopy_data') else final_json.update({'adcopy_data': {'desc2': x}}),
        'page_title': lambda x: final_json.update({'page_title': x}),
        'heading': lambda x: final_json['main_body'].update({'heading': x}) if final_json.get('main_body') else final_json.update({'main_body': {'heading': x}}),
        'sub_heading': lambda x: final_json['main_body'].update({'sub_heading': x}) if final_json.get('main_body') else final_json.update({'main_body': {'sub_heading': x}}),
        'form_heading': lambda x: final_json['form'].update({'heading': x}) if final_json.get('form') else final_json.update({'form': {'heading': x}}),
        'form_sub_heading': lambda x: final_json['form'].update({'sub_heading': x}) if final_json.get('form') else final_json.update({'form': {'sub_heading': x}}),
        'action_button': lambda x: final_json['form'].update({'button_text': x}) if final_json.get('form') else final_json.update({'form': {'button_text': x}}),
        'pt1': lambda x: final_json['main_body']['points'].append(x) if final_json['adcopy_data'].get('points') else final_json['main_body'].update({'points': [x]}) if final_json.get('main_body') else final_json.update({'main_body': {'points': [x]}}),
        'pt2': lambda x: final_json['main_body']['points'].append(x) if final_json['adcopy_data'].get('points') else final_json['main_body'].update({'points': [x]}) if final_json.get('main_body') else final_json.update({'main_body': {'points': [x]}}),
        'pt3': lambda x: final_json['main_body']['points'].append(x) if final_json['adcopy_data'].get('points') else final_json['main_body'].update({'points': [x]}) if final_json.get('main_body') else final_json.update({'main_body': {'points': [x]}}),
        'pt4': lambda x: final_json['main_body']['points'].append(x) if final_json['adcopy_data'].get('points') else final_json['main_body'].update({'points': [x]}) if final_json.get('main_body') else final_json.update({'main_body': {'points': [x]}}),
    }
    # these will delete the previous data and write the new ones instead of updating the data
    # override_headings = ['pt1', 'pt2', 'pt3', 'pt4']
    try:
        excel = load_workbook(filename=path_to_excel, read_only=True)
    except IOError:
        return False, 'Not a valid path'
    except InvalidFileException:
        return False, 'Not a valid excel'
    except:
        return False, 'Some fucked up shit happened. Clean it yourself.'
    else:
        sheet = excel.active
        header_row = sheet.rows.next()
        heading_map = [heading.value for heading in header_row]
        for row in sheet.iter_rows(row_offset=1):
            try:
                for i in xrange(len(heading_map)):
                    cell_value = row[i].value
                    if i == 0:
                        creative_id = int(cell_value)
                        creative = default_structure[
                            heading_map[i]](creative_id)[0]
                        final_json = creative.data.copy()
                        if final_json.get('main_body') and 'points' in final_json['main_body']:
                            # points should always take the new data even if it
                            # is not given
                            del final_json['main_body']['points']
                    elif cell_value is not None:
                        default_structure[heading_map[i]](cell_value)
                creative.data = final_json
                creative.save()
            except (ValueError, TypeError):
                pass
        return True, 'Mission Accomplished'
