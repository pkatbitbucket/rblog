logo_dict = {
    1: 'larsen-and-tubro-general-insurance.png',
    2: 'bajaj-allianz-general-insurance.png',
    3: 'bharti-axa-general-insurance.png',
    4: 'iffco-tokio-general-insurance.png',
    5: 'hdfc-ergo-general-insurance.png',
    7: 'tata-aig-general-insurance.png',
    8: 'liberty-videocon-general-insurance.gif',
    9: 'universal-sompo-general-insurance.png',
    10: 'new-india-general-insurance.png',
    11: 'oriental-general-insurance.jpg',
    12: 'future-generali-total-insurance.png',
    13: 'icici-lombard-general-insurance.png',
    14: 'sbi-general-insurance.png',
    15: 'royal-sundaram-general-insurance.png',
    16: 'cholamandalam-ms-general-insurance.png',
    17: 'national-insurance.png',
    18: 'united-india-insurance.png',
}

brand_dict = {
    # LANDT
    1: {
        'title': 'L & T',
        'benefits': [
            'Claim survey within 4 hours, once Car in is Garage',
            'Transfer NCB of previous car to your New Car',
            'Offers NCB protection Add-on',
            'Best deal on your Car Insurance',
        ],
        'body_css': 'generic',
    },
    # BAJAJ ALLIANZ
    2: {
        'title': 'Bajaj Allianz',
        'benefits': [
            'Cashless claim settlement at over 1500 Garages',
            '75% advance payment in case of non-cashless settlement',
            'Complimentary 24x7 Road Side Assistance',
            'Instant claim assistance & SMS updates',
        ],
    },
    # BHARTI AXA
    3: {
        'title': 'Bharti AXA',
        'benefits': [
            'Cashless claim settlement at over 2000 Garages',
            'Offers Invoice Cover Add-on',
            'Instant policy in your Inbox',
            'Use the transferred NCB',
        ],
    },
    # IFFCO TOKIO
    4: {
        'title': 'Iffco Tokio',
        'benefits': [
            'Online claim tracking',
            'Instant Policy in your Inbox',
            'Cashless claim settlement at over 2000 Garages',
            '24x7 claim registration service'
        ],
        'body_css': 'generic',
    },
    # HDFC ERGO
    5: {
        'title': 'HDFC Ergo',
        'benefits': [
            'Age & Profession based discount',
            'Online claim tracking',
            'Instant policy in your Inbox',
            'Cashless claim settlement at over 1600 Garages'
        ],
    },
    # TATA AIG
    7: {
        'title': 'Tata AIG',
        'benefits': [
            'Direct settlement facility at network garages',
            'Upto 6-month accident-repair warranty',
            'Quality spares and materials, guaranteed',
            'Claim settlement in 7 days!'

        ]
    },
    # LIBERTY VIDEOCON
    8: {
        'title': 'Liberty Videocon',
        'benefits': [
            'Cashless claim settlement at over 2000 Garages',
            'Variety of Add-on products',
            'Claim settlement within 7 working days'
            '24x7 claim registration service'
        ]
    },
    # UNIVERSAL SOMPO
    9: {
        'title': 'Universal Sompo',
        'benefits': [
            'Instant Policy',
            'Variety of Add-on products',
            'Offers Zero Dep add-on till 5 years ',
            'Best deal on your Car Insurance'
        ],
        'body_css': 'generic',
    },
    # NEW INDIA
    10: {
        'title': 'New India',
        'benefits': [
            'Leading General Insurance company of India',
            'Best deals on your Car insurance',
            'Largest network of offices in India',
            'Wide variety of products',
        ],
        'body_css': 'generic',
    },
    # ORIENTAL
    11: {
        'title': 'Oriental',
        'benefits': [
            'Leading General Insurance company of India',
            'Best deals on your Car insurance',
            'Large network of offices in India',
            'Wide variety of products',
        ],
    },
    # FUTURE GENERALI
    12: {
        'title': 'Future Generali',
        'benefits': [
            'Instant Policy',
            'Cashless claim settlement at over 2000 Garages',
            'Variety of Add-on products',
            'Claim settlement within 7 working days'
        ]
    },
    # ICICI LOMBARD
    13: {
        'title': 'ICICI Lombard',
        'benefits': [
            'Instant Policy',
            'Complimentary 24x7 Road Side Assistance',
            'Cashless claim settlement at over 3300 Garages',
            'Accidental cover facility for co-passengers up-to Rs. 2 Lac',
        ]
    },
    # SBI
    14: {
        'title': 'SBI',
        'benefits': [
            'Coverage Loss of Personal Belongings',
            'NCB Protection',
            'Personal Accident Covered',
            'Variety of Add-on products'
        ]
    },
    # ROYAL SUNDARAM
    15: {
        'title': 'Royal Sundaram',
        'benefits': [
            'Instant Policy',
            'Free 24x7 Road Side Assistance',
            'Cashless claim settlement at over 2000 Garages',
            'Claim settlement within 2 working days'
        ]
    },
    # CHOLAMANDALAM MS
    16: {
        'title': 'Cholamandalam MS',
        'benefits': [
            'Instant Policy',
            'Complimentary 24x7 Road Side Assistance',
            'Cashless claim settlement wide network of garages',
            'Easy, smooth and hassle free claim settlement process',
        ]
    },
    # NATIONAL INSURANCE
    17: {
        'title': 'National Insurance',
        'benefits': [
            'Variety of Add-on products',
            'Best deals on Car Insurance',
            'Guaranteed repairs',
            'Wide variety of products'
        ]
    },
    # UNITED INDIA
    18: {
        'title': 'United India',
        'benefits': [
            'Direct settlement facility at network garages',
            'Personal Accident Cover',
            'Wide variety of products',
            'Best deals on Car Insurance'
        ]
    },
}

MOTOR_TP_DICT = {
    'jago': {
        'url': 'http://www.jagoinvestor.com',
        'event_label': 'tp_car_widget_jagoinvestor',
        'campaign': 'motor-jago',
        'redirect_delay': 10000,
    },
    'oneindia': {
        'url': 'http://www.oneindia.com',
        'event_label': 'tp_car_widget_oneindia',
        'campaign': 'motor-oneindia',
    },
    'coverfoxblog': {
        'url': 'http://www.coverfox.com/articles',
        'event_label': 'tp_car_widget_motor_coverfoxblog',
        'campaign': 'motor',
        'custom_css': 'motor_blog',
    },
}
