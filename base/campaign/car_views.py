from django.template import RequestContext
from django.http import HttpResponse
from django.template.loader import select_template
from django.views.decorators.cache import never_cache

from motor_product.models import Insurer


@never_cache
def jago_car(request):
    data_context = {
        'page_id': 'lp_car_jagoinvestor',
        'page_title': 'Best Car deals only for JagoInvestor readers',
        'main_body': {
            'heading': 'Best Car deals only for JagoInvestor readers',
            'sub_heading': 'Avail 50% discount on premium + Additional 10% discount if you buy online',
            'points': [
                'Free 24x7 Road Side Assistance service',
                'Instant Policy',
                'No documents required',
                'Dedicated Claims manager'
            ],
        },
        'form': {
            'heading': 'Calculate your car insurance premium',
            'sub_heading': 'Get Instant quotes from insurance companies',
            'closure_text': 'Maximise your savings. Talk to us.',
        },
    }
    template_path = select_template(['campaign/motor/buy-car-insurance.html'])
    if request.user_agent.is_mobile:
        data_context['page_id'] = 'lp_car_mobile'
        template_path = select_template(['campaign/motor/click-to-call.html'])
    data_context['insurer_list'] = Insurer.objects.filter(is_active_for_car=True)
    context_object = RequestContext(request, data_context)
    context_object['parent_category'] = 'Car Insurance'
    response = HttpResponse(template_path.render(context_object))
    response.set_cookie('lms_campaign', 'motor-jago')
    return response
