from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^jagoinvestor/$', views.jago_online, name='jago_online'),
    # url(r'^jagoinvestor/$', views.jagoinvestor, name='jago_online'),
    url(r'^jagoinvestor/health-insurance/$', views.jagoinvestor, name='jagoinvestor'),
    url(r'^jagoinvestor/car-insurance/$', views.jago_car, name='jago_car'),
]
