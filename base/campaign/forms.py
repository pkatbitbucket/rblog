from django import forms
from django.db.models import Q
from .models import *
from cf_fhurl import RequestModelForm
import camp_utils


class KeywordForm(RequestModelForm):
    headline = forms.CharField(max_length=255, required=False)
    sub_headline = forms.CharField(max_length=255, required=False)
    pt1 = forms.CharField(max_length=255, required=False)
    pt1_sub = forms.CharField(max_length=255, required=False)
    pt2 = forms.CharField(max_length=255, required=False)
    pt2_sub = forms.CharField(max_length=255, required=False)
    pt3 = forms.CharField(max_length=255, required=False)
    pt3_sub = forms.CharField(max_length=255, required=False)
    pt4 = forms.CharField(max_length=255, required=False)
    pt4_sub = forms.CharField(max_length=255, required=False)

    class Meta(object):
        model = Keyword
        exclude = ('extra', 'ad_group')

    def init(self, ad_group_id, pid=None):
        self.ad_group = AdGroup.objects.get(id=ad_group_id)
        if pid:
            self.instance = Keyword.objects.get(id=pid)
            for f in self.fields.keys():
                self.fields[f].initial = self.instance.extra.get(f)

    def clean_slug(self):
        d = self.cleaned_data
        if self.instance:
            if Keyword.objects.filter(Q(slug=d['slug']) & ~Q(id=self.instance.id)):
                raise forms.ValidationError("slug field must be unique!")
        else:
            if Keyword.objects.filter(slug=d['slug']):
                raise forms.ValidationError("slug field must be unique!")
        return d['slug']

    def save(self, commit=True):
        d = self.cleaned_data
        keyword = super(KeywordForm, self).save(commit=False)
        keyword.ad_group = self.ad_group
        keyword.extra = d
        keyword.save()
        return {'success': True}


class AdGroupForm(RequestModelForm):
    headline = forms.CharField(max_length=255)
    sub_headline = forms.CharField(max_length=255)
    pt1 = forms.CharField(max_length=255, required=False)
    pt1_sub = forms.CharField(max_length=255, required=False)
    pt2 = forms.CharField(max_length=255, required=False)
    pt2_sub = forms.CharField(max_length=255, required=False)
    pt3 = forms.CharField(max_length=255, required=False)
    pt3_sub = forms.CharField(max_length=255, required=False)
    pt4 = forms.CharField(max_length=255, required=False)
    pt4_sub = forms.CharField(max_length=255, required=False)

    class Meta(object):
        model = AdGroup
        exclude = ('extra',)

    def init(self, pid=None):
        if pid:
            self.instance = AdGroup.objects.get(id=pid)
            for f in self.fields.keys():
                self.fields[f].initial = self.instance.extra.get(f)

    def clean_title(self):
        d = self.cleaned_data
        if self.instance:
            if AdGroup.objects.filter(Q(title=d['title']) & ~Q(id=self.instance.id)):
                raise forms.ValidationError("title field must be unique!")
        else:
            if AdGroup.objects.filter(title=d['title']):
                raise forms.ValidationError("title field must be unique!")
        return d['title']

    def save(self, commit=True):
        d = self.cleaned_data
        ad_group = super(AdGroupForm, self).save()
        ad_group.extra = d
        ad_group.save()
        return {'success': True}


class AdcopyMarriageFileForm(forms.Form):
    xl_file = forms.FileField()

    def clean_xl_file(self):
        xl_file = self.cleaned_data.get('xl_file')
        if not xl_file.name[-4:] == '.xls' and not xl_file.name[-5:] == '.xlsx':
            raise forms.ValidationError('Upload only excel files')
        return xl_file


class AdcopyEditForm(forms.Form):

    def __init__(self, creative_id, *args, **kwargs):
        super(AdcopyEditForm, self).__init__(*args, **kwargs)
        if creative_id:
            creative_data = AdcopyCreative.objects.get(
                creative_id=creative_id).data
            attrs = {'class': 'form-control',
                     'rows': '3', 'style': 'width: inherit'}
            for key, values in camp_utils._SAMPLE_ADCOPY_JSON.items():
                if type(values) == str:
                    self.fields[key] = forms.CharField(
                        initial=creative_data.get(key), widget=forms.Textarea(attrs=attrs))
                elif type(values) == dict:
                    for _key, _values in values.items():
                        if type(_values) == str:
                            self.fields[key + '_' + _key] = forms.CharField(initial=creative_data.get(
                                key, {}).get(_key), widget=forms.Textarea(attrs=attrs))
                        elif type(_values) == list:
                            i = 1
                            while i <= len(_values):
                                self.fields[key + '_' + _key[:-1] + str(i)] = forms.CharField(initial=creative_data.get(
                                    key, {}).get(_key, [''] * len(_values))[i - 1], widget=forms.Textarea(attrs=attrs))
                                i += 1
