brand_dict = {
    # APOLLO MUNICH
    1: {
        'title': 'Apollo Munich',
        'webpage': 'www.apollomunichinsurance.com',
        'benefits': [
            'Specialized Health Insurance Company',
            'Part of the Apollo Hospitals Group',
            'Inhouse Claim Settlement. No TPA'
        ],
        'products': [
            {
                'title': 'Easy Health',
                'description': '<ul>'
                               '<li>No Room Rent Limits. No Copay</li>'
                               '<li>Wide Cashless Hospital Network</li>'
                               '<li>Lifelong Policy Renewal</li>'
                               '</ul>'
            },
            {
                'title': 'Optima Restore',
                'description': '<ul>'
                               '<li>No Room Rent Limits. No Copay</li>'
                               '<li>100% Restore, if sum insured exhausts during a policy year</li>'
                               '<li>Sum Insured increases by 50% if you don\'t claim in a policy year</li>'
                               '</ul>',
            }
        ],
    },
    # STAR HEALTH
    2: {
        'title': 'Star',
        'webpage': 'www.starhealth.in',
        'benefits': [
            "India's Largest Health Insurance Company",
            'Direct Claim Settlement. No TPA',
            'Large Network of Cashless Hospitals - 5700+',
        ],
        'products': [
            {
                'title': 'Medi Classic',
                'description': '<ul>'
                               '<li>100% Restoration of Coverage</li>'
                               '<li>Lifelong Policy Renewal</li>'
                               '</ul>',
            },
            {
                'title': 'Star Comprehensive',
                'description': '<ul>'
                               '<li>100% Restoration of Coverage</li>'
                               '<li> No Claim Bonus of 50% every year upto 100%</li>'
                               '<li>Lifelong Policy Renewal</li>'
                               '</ul>',
            },
            {
                'title': 'Star Health Family Optima Floater Plan',
                'description': '<ul>'
                               '<li>Nationwide coverage for the entire family</li>'
                               '<li>Cashless Hospitalization with no capping on room rent and treatment costs</li>'
                               '<li>Automatic Restore of coverage amount</li>'
                               '<li>Guaranteed lifetime renewals</li>'
                               '</ul>',
            },
            {
                'title': 'Star Health Senior Citizen Red Carpet',
                'description': '<ul>'
                               '<li>Exclusively meant for Senior citizens aged between 60 to 75 years</li>'
                               '<li>Guaranteed renewals beyond 75 years</li>'
                               '<li>No pre-medical hassles</li>'
                               '<li>Pre-existing diseases covered from 1st year of policy</li>'
                               '</ul>'
            },
        ],
        'body_css': '',
    },
    # BAJAJ ALLIANZ
    4: {
        'title': 'Bajaj Allianz',
        'webpage': 'www.bajajallianz.com',
        'benefits': [
            '14 years of excellent claims track record',
            'Premium of more than 4000 Crores in 2012-13',
            'Inhouse Claim Settlement. No TPA.'
        ],
        'products': [
            {
                'title': 'Health Guard',
                'description': '<ul>'
                               '<li>No Room Rent Limits. No Copay</li>'
                               '<li>Cashless treatment at 4000+ hospitals</li>'
                               '<li>Lifelong Policy Renewal</li>'
                               '</ul>',
            }
        ],
    },
    # RELIGARE
    6: {
        'title': 'Religare',
        'webpage': 'www.religare.com',
        'benefits': [
            'Automatic Restoration of Cover for unrelated claims',
            'Private Room Coverage for Life',
            'Free Health Checkup',
        ],
        'products': [
            {
                'title': "'Care' - Health Insurance Policy",
                'description': '<ul>'
                               '<li>Cashless treatment at 4100+ hospitals & Daily allowance</li>'
                               '<li>Auto recharges of your policy Sum Insured if you exhaust the cover</li>'
                               '<li>Free health checkup every year</li>'
                               '<li>Claims made (if any) will not cause a hike in premium during renewals</li>'
                               '</ul>',
            },
            # {
            #     'title': "'Assure' - Critical Illness Health Insurance Benefit",
            #     'description': '<ul>'
            #                    '<li>Covers 20 Critical Illnesses</li>'
            #                    '<li>Includes Accidental Death Cover</li>'
            #                    '<li>Free health check up every year</li>'
            #                    '</ul>'
            # },
        ],
        'body_css': 'generic',
    },
    # TATA AIG
    7: {
        'title': 'Tata AIG',
        'webpage': 'www.tataaigmotorinsurance.in',
        'benefits': [
            'India\'s Leading Insurance company',
            'Part of renowned Tata Group'
        ],
        'products': [
            {
                'title': 'MediPrime',
                'description': '<ul>'
                               '<li>No Room Rent Limits. No Copay</li>'
                               '<li>Cashless treatment at 4000+ hospitals</li>'
                               '<li>Lifelong Policy Renewal</li>'
                               '</ul>',
            }
        ],
    },
    # BHARTI AXA
    9: {
        'title': 'Bharti AXA',
        'webpage': 'www.bharti-axagi.co.in',
        'benefits': [
            'Leading Insurance Company in India',
            'Part of Bharti Airtel and Axa - World leader in Insurance and Asset Management'
        ],
        'products': [
            {
                'title': 'Smart Health Insurance',
                'description': '<ul>'
                               '<li>100% Critical Illness Lumpsum Benefit</li>'
                               '<li>Cashless treatment at 3000+ hospitals</li>'
                               '<li>Lifelong Policy Renewal</li>'
                               '</ul>',
            },
        ],
    },
    # RELIANCE
    10: {
        'title': 'Reliance',
        'webpage': 'www.reliancegeneral.co.in',
        'benefits': [
            'One of the leading general insurance companies in India',
            'Part of renowned Reliance (ADA) Group',
        ],
        'products': [
            {
                'title': 'Healthwise - Standard',
                'description': '<ul>'
                               '<li>No Room Rent Limits. No Copay.</li>'
                               '<li>Cashless treatment at 4000+ hospitals</li>'
                               '<li>Lifelong Policy Renewal</li>'
                               '</ul>',
            },
            # {
            #     'title': 'Health Gain',
            #     'description': '<ul>'
            #                    '<li>No Room Rent Limits. No Copay.</li>'
            #                    '<li>Cashless treatment at 4000+ hospitals</li>'
            #                    '<li>Lifelong Policy Renewal</li>'
            #                    '</ul>'
            #
            # },
        ],
    },
    # LANDT
    11: {
        'title': 'L & T',
        'webpage': 'www.ltinsurance.com',
        'benefits': [
            'Double Sum Insured for treatment of critical illness',
            '100% Restore of Coverage for Accident Claims',
            'Guaranteed Response for Claims',
        ],
        'products': [
            {
                'title': 'Medisure Classic Insurance',
                'description': '<ul>'
                # 'This plan comes with a host of features that cover almost all the medical expenses incurred by you. '
                               '<li>Lifetime Renewals</li>'
                               '<li>No medical tests required for customers below the age of 50 yrs</li>'
                               '<li>Covers maternity medical expenses</li>'
                               '</ul>',
            },
            {
                'title': 'Medisure Prime Insurance',
                'description': '<ul>'
                               '<li>Comes with a Service Guarantee</li>'
                               '<li>No sub-limits</li>'
                               '<li>Lifetime renewal</li>'
                               '<li>Instant policy issuance for customers below 45</li>'
                               '</ul>'
            },
        ],
        'body_css': 'generic',
    },
    # HDFC ERGO
    13: {
        'title': 'HDFC Ergo',
        'webpage': 'www.hdfcergo.com',
        'benefits': [
            'Assigned \'iAAA\' rating by ICRA indicating its highest claim paying ability',
            'Part of renowned HDFC Group',
            '109 branches spread across 89 cities'
        ],
        'products': [
            {
                'title': 'Health Suraksha',
                'description': '<ul>'
                               '<li>No Room Rent Limits. No Copay</li>'
                               '<li>Cashless treatment at 4000+ hospitals</li>'
                               '<li>Lifelong Policy Renewal</li>'
                               '</ul>',
            }
        ],
    },
    # IFFCO TOKIO
    14: {
        'title': 'Iffco Tokio',
        'webpage': 'www.iffcotokio.co.in',
        'benefits': [
            'One of the largest network of cashless hospitals',
            'Extremely affordable premium',
            'Emergency Assistance Service at no additional charges',
        ],
        'products': [
            {
                'title': 'Medishield (Individual) Policy',
                'description': '<ul>'
                               '<li>Covers medical treatment costs for diseases and accidents</li>'
                               '<li>Basic medical coverage for all illnesses (except for certain exclusions)</li>'
                               '<li>Optional cover for additional Sum Insured for critical illness like '
                               'heart surgery, kidney failure, cerebral stroke cancer, major organ '
                               'transplantation etc. where treatment costs are very high</li>'
                               '</ul>',
            },
            {
                'title': 'Swasthya Kavach (Family Health) Policy',
                'description': '<ul>'
                               '<li>Basic medical coverage for all illnesses (except for certain exclusions)</li>'
                               '<li>Optional cover for critical illness</li>'
                               '<li>Covers pre-existing diseases after 4 continuous policy years</li>'
                               '</ul>',
            },
            {
                'title': 'SWASTHYA KAVACH Wider Plan',
                'description': '<ul><li>Budget/Affordable premium product</li>'
                               '<li>Wide Cashless Hospital Network</li>'
                               '<li>Lifelong Policy Renewal</li>'
                               '</ul>',
            },
            # {
            #     'title': 'Critical Health Insurance Policy',
            #     'description': '<ul>'
            #                    '<li>Covers critical illnesses such as cancer, renal failure'
            #                    'coronary artery diseases requiring bypass surgery, '
            #                    'major organ transplant, paralytic cerebral stroke '
            #                    'as well as accidental injuries resulting in loss of limbs</li>'
            #                    '</ul>',
            #     },
        ],
        'body_css': 'generic',
    },
    # ORIENTAL
    16: {
        'title': 'Oriental',
        'webpage': 'www.orientalinsurance.org.in',
        'benefits': [
            '100% owned by Government of India',
            'Excellent track record of more than 60 years',
            'More than 7000 Cr. of Gross Premium booked in 2013-14'
        ],
        'products': [
            {
                'title': 'Happy Family Floater - Gold',
                'description': '<ul>'
                               '<li>Cover your entire family including parents in one policy</li>'
                               '<li>No Medical Test upto 60 years of age</li>'
                               '<li>Lifelong Policy Renewal</li>'
                               '</ul>',
            },
            {
                'title': 'Silver Plan',
                'description': '<ul>'
                               '<li>Cover your entire family including parents in one policy</li>'
                               '<li>No Medical Test upto 60 years of age</li>'
                               '<li>Lifelong Policy Renewal</li>'
                               '</ul>',
            },
        ]

    }
}


TP_DICT = {
    'jago': {
        'url': 'http://www.jagoinvestor.com',
        'event_label': 'tp_health_widget_jagoinvestor',
        'campaign': 'health-jago',
        'redirect_delay': 10000,
    },
    'coverfoxblog': {
        'url': 'http://www.coverfox.com/articles',
        'event_label': 'tp_health_widget_coverfoxblog',
        'campaign': 'health',
        'custom_css': 'health_blog',
    },
    'supertopup': {
        'url': 'http://www.coverfox.com/lp/health-insurance/super-topup-online/',
        'event_label': 'tp_health_widget_supertopup_lp',
        'campaign': 'health-topup',
        'custom_css': 'supertopup_landing',
    },
}
