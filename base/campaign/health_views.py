from django.conf import settings
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template.loader import select_template
from django.http import HttpResponse
from django.views.decorators.cache import never_cache
from django.views.decorators.clickjacking import xframe_options_exempt

import health_product.custom_utils as health_utils


@never_cache
def jagoinvestor(request):
    display_template = select_template(['campaign/health/jagoinvestor.html', 'campaign/health/product.html'])
    data_context = {
        'page_id': 'lp_jagoinvestor'
    }
    health_utils.amlpl_template(request, data_context)
    context_object = RequestContext(request, data_context)
    response = HttpResponse(display_template.render(context_object))
    health_utils.amlpl_response(request, response)
    response.set_cookie('utm_source', 'jagoinvestor')
    response.set_cookie('network', 'jagoinvestor')
    response.set_cookie('lms_campaign', 'health-jago')
    return response


@never_cache
def jago_online(request):
    display_template = select_template([
        'campaign/health/jago-online.html',
        'campaign/health/jagoinvestor.html',
        'campaign/health/product.html'
    ])
    data_context = {
        'headline': 'Compare Health Insurance Plans from Top 20 Companies',
        'first_prop': '<h2>70+ Plans</h2>'
                      '<h2>Instant Free Quotes</h2>'
                      '<h2>Ratings by Experts</h2>',
        'second_prop': '<h2>Save money by taking to an Expert</h2>',
        'page_id': 'lp_online_jagoinvestor'
    }
    health_utils.amlpl_template(request, data_context)
    context_object = RequestContext(request, data_context)
    response = HttpResponse(display_template.render(context_object))
    response.set_cookie('utm_source', 'jagoinvestor')
    response.set_cookie('network', 'jagoinvestor') 
    response.set_cookie('lms_campaign', 'health-jago')
    health_utils.amlpl_response(request, response)
    return response


@xframe_options_exempt
def jd_widget(request):
    """
    iFrame to be embedded on third-party sites
    """

    template_data = {
        'site_url': settings.SITE_URL,
        'iframe_id': request.GET.get('iframe_id')
    }
    health_utils.amlpl_template(request, template_data)
    response = render_to_response('campaign/marketing/jd_widget.html',
                                  context_instance=RequestContext(request, template_data))
    health_utils.amlpl_response(request, response)
    return response
