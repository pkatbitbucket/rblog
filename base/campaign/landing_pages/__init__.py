from motor_product.models import Insurer as MotorInsurer


DEFAULT_LPS = {
    'health': 'product',
    'motor': 'buy',
    'bike': 'buy',
    'travel': 'buy',
}


MASTER_COOKIES = {
    'health': {},
    'motor': {},
    'bike': {
        'mode': 'unassisted',
        'vt_home_visited': 'No'
    },
    'travel': {},
}


_SAMPLE_LANDING_PAGE_CONTEXT = {
    "page_title": "page title",
    "main_body": {
        "heading": "main body heading",
        "sub_heading": "main body sub heading",
        "points": [
            "main body point 1",
            "main body point 2",
            "main body point 3",
            "main body point 4"
        ],
    },
    "form": {
        "heading": "form heading",
        "sub_heading": "form sub heading",
        "button_text": "button text"
    },
}


class CustomDict(dict):
    def __getattr__(self, item):
        if item.startswith('_'):
            return super(CustomDict, self).__getattribute__(item)
        return self.get(item, CustomDict())

    def __setattr__(self, key, value):
        return self.__setitem__(key, value)

    def __setitem__(self, key, value):
        if isinstance(value, dict):
            value = CustomDict(value)
        return super(CustomDict, self).__setitem__(key, value)

    def __init__(self, seq=None, **kwargs):
        if seq:
            super(CustomDict, self).__init__(seq, **kwargs)
            if isinstance(seq, dict):
                kwargs.update(seq)
                self.update(kwargs)
            else:
                for k, v in seq.__iter__():
                    if isinstance(v, dict):
                        self[k] = CustomDict(**v)
                    else:
                        self[k] = v
        else:
            super(CustomDict, self).__init__(**kwargs)

        for k, v in kwargs.items():
            if isinstance(v, dict):
                self[k] = CustomDict(**v)
            else:
                self[k] = v


class LandingPageContext(CustomDict):
    def __getattr__(self, item):
        if item not in ('desktop', 'mobile') and self.desktop and self.mobile and getattr(self.desktop, item) == getattr(self.mobile, item):
            return getattr(self.desktop, item, None)
        return super(LandingPageContext, self).__getattr__(item)

    def __setattr__(self, key, value):
        if key != 'desktop' and key != 'mobile':
            if 'desktop' in self:
                setattr(self.desktop, key, value)
            if 'mobile' in self:
                setattr(self.mobile, key, value)
        else:
            super(LandingPageContext, self).__setattr__(key, value)

    def __init__(self, seq=None):
        if len(seq) != 2 or any(i not in seq for i in ('desktop', 'mobile')):
            seq = {'desktop': dict(seq), 'mobile': dict(seq)}
        super(LandingPageContext, self).__init__(seq=seq)


#######################################################
# DEFINE EXPERIMENTAL PAGE VARIATIONS AS FOLLOWS
VARIATIONS = LandingPageContext({
    'motor': {
        'get-cover': (
            {'chance': 50, 'params': {'v': '1'}},
            {'chance': 50, 'params': {'v': '2'}},
        ),
    },
})
#######################################################


class LandingPage(object):

    def __init__(self, **kwargs):
        super(LandingPage, self).__init__()
        self._context = LandingPageContext(_SAMPLE_LANDING_PAGE_CONTEXT)
        self._cookies = CustomDict({
            'vt_home_visited': ''
        })
        for k, v in kwargs.items():
            setattr(self, k, v)
        if self.view_id is None:
            self.view_id = ''

    @property
    def context(self):
        return self._context

    @context.setter
    def context(self, value):
        if not isinstance(value, CustomDict):
            self._context = LandingPageContext(value)
        else:
            self._context = value

    @property
    def cookies(self):
        return self._cookies

    @cookies.setter
    def cookies(self, value):
        if not isinstance(value, CustomDict):
            self._cookies = CustomDict(value)
        else:
            self._cookies = value


class HealthLandingPage(LandingPage):
    def __init__(self, **kwargs):
        super(HealthLandingPage, self).__init__(**kwargs)
        product_keyword = 'Mediclaim' if self.view_id == 'mediclaim' else 'Health Insurance'
        product_keyword_mobile = '<br>Mediclaim' if self.view_id == 'mediclaim' else 'Health<br>Insurance'
        page_id = 'lp_product_mediclaim' if self.view_id == 'mediclaim' else 'lp_product'

        self.context.page_id = page_id
        self.context.skip_option = False
        self.context.keyword = product_keyword

        self.context.desktop.template = 'campaign/health/product.html'

        self.context.desktop.main_body.heading = 'Get Lowest Rates for Health Insurance'
        self.context.desktop.main_body.sub_heading = '%s Plans starting @ Rs 6 / day' % product_keyword

        self.context.mobile.template = 'campaign/health/mobile-buy-health-insurance.html'

        self.context.mobile.main_body.heading = 'Compare %s Policies' % product_keyword_mobile
        self.context.mobile.main_body.sub_heading = '%s Plans<br> starting @ Rs 6 / day' % product_keyword

        if kwargs.get('segment', '').lower() == 'individual':
            self.context.desktop.main_body.heading = 'Secure Your Health'
            self.context.mobile.main_body.heading = 'Secure Your Health'
        elif kwargs.get('segment', '').lower() == 'parents':
            self.context.desktop.main_body.heading = 'Secure Your Parent\'s Health'
            self.context.mobile.main_body.heading = 'Secure Your Parent\'s Health'
        elif kwargs.get('segment', '').lower() == 'family':
            self.context.desktop.main_body.heading = 'Secure Your Family\'s Health'
            self.context.mobile.main_body.heading = 'Secure Your Family\'s Health'


class MotorLandingPage(LandingPage):
    def __init__(self, **kwargs):
        super(MotorLandingPage, self).__init__(**kwargs)
        parent_category_dict = {
            'buy': 'Car Insurance',
            'buy-auto-insurance': 'Auto Insurance',
            'buy-vehicle-insurance': 'Vehicle Insurance',
            'buy-motor-insurance': 'Motor Insurance'
        }
        self.META_DICT = {
            'policybazaar': 'PB',
            'car insurance calculator': 'CALCULATOR',
            'national insurance': 'NATIONAL',
            'policybazaar com': 'PB',
            'car insurance comparison': 'COMPARE',
            'insurance': 'CAR_INSURANCE',
            'policy bazar com': 'PB',
            'coverfox': 'COVERFOX',
            'car insurance': 'CAR_INSURANCE',
            'new india assurance': 'NEW_INDIA',
            'bajaj allianz': 'BAJAJ',
            'oriental insurance': 'ORIENTAL',
            'united india insurance': 'UNITED_INDIA',
            'insurance policy': 'CAR_INSURANCE',
            'online car insurance': 'CAR_INSURANCE',
            'policy bazaar com': 'PB',
            'policybazaar com car insurance': 'PB',
            'policybazaarcom': 'PB',
            'new india insurance': 'NEW_INDIA',
            'insurance car': 'CAR_INSURANCE',
            'car insurance online': 'CAR_INSURANCE',
            'policy bazar': 'PB',
            'national insurance car': 'NATIONAL',
            'policybazaar car insurance': 'PB',
            'l t insurance': 'L_AND_T',
            'bajaj insurance': 'BAJAJ',
            'bharti axa': 'BHARTI_AXA',
            'tata insurance': 'TATA',
            'icici lombard': 'ICICI',
            'car insurance premium calculator': 'CAR_INSURANCE'
        }
        self.context.view_id = self.view_id
        self.context.segment = kwargs.get('segment', '').lower()
        self.context.insurer_list = MotorInsurer.objects.filter(is_active_for_car=True)

        self.context.desktop.template = 'campaign/motor/switch.html'
        self.context.desktop.page_id = 'lp_car_buy'
        self.context.desktop.page_title = 'Compare Car Insurance Premium Quotes | Buy Car Insurance online'
        self.context.desktop.parent_category = parent_category_dict.get(self.view_id, 'Car Insurance')

        self.context.desktop.main_body.heading = 'Lowest Car Insurance Quotes'
        self.context.desktop.main_body.sub_heading = 'Premium starting Rs 6 / day'
        self.context.desktop.main_body.points = ['Get a Dedicated Claims Manager',
                                                 'Cashless Settlement in nearest garage',
                                                 'Free towing and pick-up services',
                                                 'Guaranteed End to End Claims Assistance']

        self.context.desktop.form.heading = 'Get Lowest Rates on Car Insurance'
        self.context.desktop.form.sub_heading = 'Avail upto 60% discount on Premium'
        self.context.desktop.form.button_text = 'Compare prices from Top Companies'

        self.context.mobile.template = 'campaign/motor/mobile-buy-car-insurance.html'
        self.context.mobile.page_id = 'lp_car_mobile'
        self.context.mobile.page_title = 'Renew Car Insurance Online | Compare Premium | Lowest Quotes'

        self.context.mobile.main_body.heading = 'Lowest Car Insurance Quotes'
        self.context.mobile.main_body.sub_heading = 'Premium starting Rs 6 / day'
        self.context.mobile.main_body.points = ['Cashless Settlement in nearest garage',
                                                'Free towing and pick-up services',
                                                'Guaranteed End to End Claims Assistance']

        self.context.mobile.form.heading = 'Get Lowest Rates on Car Insurance'
        self.context.mobile.form.sub_heading = 'Avail upto 60% discount on Premium'
        self.context.mobile.form.button_text = 'Compare Quotes Online'

        self.keyword_data = {
            'PB': {
                'page_title': 'Coverfox - Buy Car Insurance Online',
                'keywords': 'policybazaar, policy bazaar, policybazar, policy bazaar, policybazzar, policy bazzar',
                'description': 'Coverfox - Buy Car Insurance Online. '
                               'Compare plans and get lowest rates'
            },
            'CALCULATOR': {
                'page_title': 'Premium Calculator for Car Insruance',
                'keywords': 'Premium Calculator, Calculate premium, Compare premium',
                'description': 'Car Insurance premium Calculator by Coverfox lets you calculate your'
                               ' premium in 2 easy steps',
            },
            'CAR_INSURANCE': {
                'page_title': 'Buy Car Insurance Online from Coverfox',
                'keywords': 'Car Insurance, Motor Insurance, Auto Insurance, Compare Car Insurance',
                'description': 'Compare quotes from Top Companies for Free. Lowest premium rates. Heavy discounts.'
            },
            'COMPARE': {
                'page_title': 'Compare Car Insurance prices from Top Companies',
                'keywords': 'Compare Car Insurance, Compare premium, Compare Quotes, Car Insurance, Coverfox',
                'description': 'Compare Car Insurance premium on Coverfox and get upto 60% off. '
                               'Lowest premium prices guaranteed.'
            },
            'COVERFOX': {
                'page_title': 'Coverfox - Insurance Chuno, Instantly Kharido',
                'keywords': 'Coverfox, Cover Fox, Car Insurance, Health Insurance, Travel Insurance',
                'description': 'Get Motor, Health and Travel Insurance at Coverfox. '
                               'Compare policies and buy insurance online from over 20 Insurance companies.'
            },
            'BAJAJ': {
                'page_title': 'Buy Bajaj Car Insurance from Coverfox',
                'keywords': 'Bajaj Car Insurance, Bajaj Insurance, Car Insurance',
                'description': 'Get lowest rates on Coverfox. Along with Online policy, get Free add-on services '
                               'and complete claims support.'
            },
            'BHARTI_AXA': {
                'page_title': 'Buy Bharti Axa Car Insurance from Coverfox',
                'keywords': 'Bharti Insurance, Bharti Axa Car Insurance, Car Insurance',
                'description': 'Get lowest rates on Coverfox. Along with Online policy, get Free add-on services '
                               'and complete claims support.'
            },
            'ICICI': {
                'page_title': 'Compare ICICI Insurance plans at Coverfox',
                'keywords': 'ICICI Car Insurance, ICICI Insurance, Car Insurance',
                'description': 'Get lowest rates on Coverfox. Along with Online policy, get Free add-on services '
                               'and complete claims support.'
            },
            'L_AND_T': {
                'page_title': 'Buy L & T Car Insurance from Coverfox',
                'keywords': 'L & T Insurance, L and T Car Insurance, Car Insurance',
                'description': 'Get lowest rates on Coverfox. Along with Online policy, get Free add-on services '
                               'and complete claims support.'
            },
            'NATIONAL': {
                'page_title': 'Buy National Car Insurance from Coverfox',
                'keywords': 'National Insurance, National Car Insurance, Car Insurance',
                'description': 'Get lowest rates on Coverfox. Along with Online policy, get Free add-on services '
                               'and complete claims support.'
            },
            'NEW_INDIA': {
                'page_title': 'Buy New India Car Insurance from Coverfox',
                'keywords': 'New India Insurance, New India Car Insurance, Car Insurance',
                'description': 'Get lowest rates on Coverfox. Along with Online policy, get Free add-on services '
                               'and complete claims support.'
            },
            'ORIENTAL': {
                'page_title': 'Buy Oriental Car Insurance from Coverfox',
                'keywords': 'Oriental Insurance, Oriental Car Insurance, Car Insurance',
                'description': 'Get lowest rates on Coverfox. Along with Online policy, get Free add-on services '
                               'and complete claims support.'
            },
            'TATA': {
                'page_title': 'Buy Tata AIG Car Insurance from Coverfox',
                'keywords': 'Tata Insurance, Tata AIG Car Insurance, Car Insurance',
                'description': 'Get lowest rates on Coverfox. Along with Online policy, get Free add-on services '
                               'and complete claims support.'
            },
            'UNITED_INDIA': {
                'page_title': 'Compare United India Insurance plans at Coverfox',
                'keywords': 'United India Car Insurance, United India Insurance, Car Insurance',
                'description': 'Get lowest rates on Coverfox. Along with Online policy, get Free add-on services '
                               'and complete claims support.'
            }
        }


class BikeLandingPage(LandingPage):
    def __init__(self, **kwargs):
        super(BikeLandingPage, self).__init__(**kwargs)
        parent_category = 'Bike'
        if self.view_id.find('two-wheeler') >= 0:
            parent_category = 'Two Wheeler'

        self.context.page_id = 'lp_bike_buy'
        self.context.parent_category = parent_category
        self.context.page_title = 'Compare Quotes and Buy %s Insurance Online' % parent_category

        self.context.desktop.template = 'campaign/bike/buy.html'

        self.context.desktop.main_body.heading = '%s Insurance - Get Instant Policy Online' % parent_category
        self.context.desktop.main_body.sub_heading = 'Get additional discounts! Premium starts at Rs. 600/-'
        self.context.desktop.main_body.points = ['Compare quotes from top companies',
                                                 'No documents required',
                                                 'Cover against Theft',
                                                 'Instant policy in 2 mins']

        self.context.desktop.form.heading = 'Calculate your Bike Insurance Premium'
        self.context.desktop.form.sub_heading = 'Compare Instant quotes from top Insurance companies'
        self.context.desktop.form.closure_text = 'Get negotiated deals. Guaranteed savings on premium!'

        self.context.mobile.template = 'campaign/bike/mobile-bike-insurance.html'
        self.context.mobile.page_id = 'lp_car_mobile'

        self.context.mobile.main_body.heading = 'Two Wheeler Insurance <br/>Instant Policy Online'
        self.context.mobile.main_body.sub_heading = 'Get additional discounts! Premium starts at Rs. 600/-'
        self.context.mobile.main_body.points = ['Compare quotes from top companies',
                                                'No documents required',
                                                'Cover against Theft',
                                                'Instant policy in 2 mins']

        self.context.mobile.form.heading = 'Calculate your bike insurance premium'
        self.context.mobile.form.sub_heading = 'Compare Instant quotes from top Insurance companies'
        self.context.mobile.form.closure_text = 'Get negotiated deals. Guaranteed savings on premium!'


class TravelLandingPage(LandingPage):
    def __init__(self, **kwargs):
        super(TravelLandingPage, self).__init__(**kwargs)

        self.context.page_id = 'lp_travel'
        self.context.template = 'campaign/travel/travel-insurance-v1.html'
