import re
import os
from django.conf import settings
from django.contrib.staticfiles.storage import staticfiles_storage

from . import BikeLandingPage
from sys import modules
from inspect import isclass, getmembers


TPARTY_LIST = []
TPARTY_LOGO_MAPPING = {}


def populate_tparty():
    if settings.STATIC_ROOT:
        _, files = staticfiles_storage.listdir('motor_product/img/bike_tparty/')
    else:
        files = os.listdir(
            os.path.join(
                settings.SETTINGS_FILE_FOLDER,
                '../static/motor_product/img/bike_tparty/'
            )
        )

    for f in files:
        f_name, _ = os.path.splitext(f)
        TPARTY_LIST.append(f_name.replace('-logo', '').title().replace('-', ''))


def populate_tparty_logo_mapping():
    for tparty in TPARTY_LIST:
        file_name = '-'.join(word.lower() for word in re.findall('[A-Z][^A-Z]*', tparty))
        TPARTY_LOGO_MAPPING.update({tparty: 'motor_product/img/bike_tparty/{}-logo.png'.format(file_name)})


class Buy(BikeLandingPage):
    pass


class Bike(BikeLandingPage):
    def __init__(self, **kwargs):
        super(Bike, self).__init__(**kwargs)
        parent_category = 'Bike'
        if self.view_id.find('two-wheeler') >= 0:
            parent_category = 'Two Wheeler'

        self.context.desktop.page_id = 'lp_bike_bike'
        self.context.desktop.template = 'campaign/bike/buy-youth.html'
        self.context.desktop.page_title = 'Compare Quotes and Buy %s Insurance Online' % parent_category
        self.context.desktop.parent_category = parent_category

        self.context.desktop.main_body.heading = '%s Insurance - Get Instant Policy Online' % parent_category
        self.context.desktop.main_body.sub_heading = 'Get additional discounts! Premium starts at Rs. 600/-'
        self.context.desktop.main_body.points = ['Compare quotes from top companies',
                                                 'No documents required',
                                                 'Cover against Theft',
                                                 'Instant policy in 2 mins']

        self.context.desktop.form.heading = 'Calculate your bike insurance premium'
        self.context.desktop.form.sub_heading = 'Compare Instant quotes from top Insurance companies'
        self.context.desktop.form.closure_text = 'Get negotiated deals. Guaranteed savings on premium!'


class Pending(Bike):
    def __init__(self, **kwargs):
        super(Pending, self).__init__(**kwargs)

        self.context.desktop.page_id = 'lp_bike_pending'
        self.context.desktop.view_id = self.view_id

        self.context.desktop.main_body.heading = 'Bike Insurance still pending?'
        self.context.desktop.main_body.sub_heading = 'No paperwork required. Renew in 2 min.'


class Expired(Bike):
    def __init__(self, **kwargs):
        super(Expired, self).__init__(**kwargs)
        self.context.desktop.page_id = 'lp_bike_expired'
        self.context.desktop.template = 'campaign/bike/expired-bike-insurance.html'
        self.context.desktop.view_id = self.view_id

        self.context.desktop.main_body.heading = 'Renew expired Bike Insurance'
        self.context.desktop.main_body.sub_heading = 'No paperwork required. Renew in 2 min.'


class BuyV2(BikeLandingPage):
    def __init__(self, **kwargs):
        super(BuyV2, self).__init__(**kwargs)
        self.context.page_id = 'lp_bike_buy-v2'
        self.context.template = 'campaign/bike/buy-v2.html'
        self.context.desktop.main_body.heading = 'Two Wheeler Insurance - Get Instant Policy Online'
        self.context.desktop.form.heading = 'Calculate your two wheeler insurance premium'


class GetCover(BikeLandingPage):
    def __init__(self, **kwargs):
        super(GetCover, self).__init__(**kwargs)
        self.context.page_id = 'lp_bike_buy-v3'

        tparty = kwargs.get('tparty', '')
        if tparty in TPARTY_LIST:
            self.set_tparty(tparty)

        self.context.template = 'campaign/bike/buy-v3.html'
        self.context.desktop.main_body.heading = 'New Two Wheeler Insurance - Get Instant Policy Online'
        self.context.desktop.form.heading = 'New Calculate your two wheeler insurance premium'

    def set_tparty(self, tparty):
        self.context.view_id = "tparty"
        self.context.tparty = tparty
        self.context.tparty_src = TPARTY_LOGO_MAPPING.get(tparty)
        self.context.tparty_alt = tparty
        self.context.page_id = 'lp_get_cover_{}'.format(tparty.lower())


__all__ = [name for name, file_obj in getmembers(modules[__name__], isclass) if file_obj.__module__ == __name__]
