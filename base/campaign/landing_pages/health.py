__author__ = 'vinay'
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from wiki.models import Insurer
from .. import health_presets as campaign_presets
from . import HealthLandingPage


class Product(HealthLandingPage):
    pass


class Compare(HealthLandingPage):
    def __init__(self, **kwargs):
        super(Compare, self).__init__(**kwargs)
        self.context.page_id = 'lp_confused'

        self.context.desktop.template = 'campaign/health/confused.html'


class Confused(Compare):
    pass


class Mediclaim(Product):
    pass


class Brand(Product):
    def __init__(self, **kwargs):
        super(Brand, self).__init__(**kwargs)
        try:
            insurer_slug = kwargs.get('brand')
            insurer = Insurer.objects.get(slug=insurer_slug)
            brand_data = campaign_presets.brand_dict[insurer.id]
        except (ObjectDoesNotExist, MultipleObjectsReturned, KeyError):
            pass
        else:
            self.context.desktop.template = 'campaign/health/brand.html'
            self.context.desktop.insurer = insurer
            self.context.desktop.page_id = 'lp_brand_%s' % insurer_slug
            self.context.desktop.page_title = '%s Insurance' % insurer.title
            self.context.desktop.brand = brand_data
            self.cookies.update({
                'brand_id': insurer.id
            })


class BuyOnline(Product):
    pass


class ClaimsSupport(BuyOnline):
    def __init__(self, **kwargs):
        super(ClaimsSupport, self).__init__(**kwargs)

        self.context.page_id = 'lp_claims_support'
        self.context.page_title = '100 percent Claims Support from Coverfox'

        self.context.desktop.template = 'campaign/health/claims-support.html'

        self.context.desktop.main_body.heading = 'Free Claims Services from Coverfox'
        self.context.desktop.main_body.sub_heading = 'We provide end to end support for Cashless and ' \
                                                     'Reimbursement Claims'


class CalculatePremium(BuyOnline):
    def __init__(self, **kwargs):
        super(CalculatePremium, self).__init__(**kwargs)

        self.context.desktop.main_body.heading = 'Calculate Health Premium'


class FamilyHealthPlans(Product):
    pass


class SaveTaxOnline(BuyOnline):
    def __init__(self, **kwargs):
        super(SaveTaxOnline, self).__init__(**kwargs)

        self.context.desktop.page_id = 'lp_save_tax_online'
        self.context.desktop.page_title = 'Best Tax Saver Plans'

        self.context.desktop.main_body.heading = 'Best Tax Saver Plans'
        self.context.desktop.main_body.sub_heading = 'Get Tax Deduction of upto Rs. 35,000/- <br/>' \
                                                     'under section 80D of the Income Tax Act, 1961'


class HealthExpert(BuyOnline):
    def __init__(self, **kwargs):
        super(BuyOnline, self).__init__(**kwargs)

        self.context.desktop.page_id = 'lp_health_expert'
        self.context.desktop.page_title = 'Expert Rated Policies | Health Insurance | Mediclaim Policy'
        self.context.desktop.template = 'campaign/health/health-expert.html'

        self.context.desktop.main_body.heading = 'Get Free Insurance assessment from Kalpesh'
        self.context.desktop.main_body.sub_heading = '''
                Kalpesh has over 8 yrs of experience in the field<br>
                of Health Insurance. Give us your details and get a Callback<br>
                from him with your assessment within 24 hrs'''


class ThreeEasySteps(BuyOnline):
    def __init__(self, **kwargs):
        super(ThreeEasySteps, self).__init__(**kwargs)

        self.context.page_id = 'lp_three_easy_steps'
        self.context.page_title = 'Buy Health Insurance in 3 Easy Steps | Compare Mediclaim Policy'
        self.context.template = 'campaign/health/three-easy-steps.html'


class Got(HealthLandingPage):
    def __init__(self, **kwargs):
        super(Got, self).__init__(**kwargs)

        self.context.desktop.template = 'campaign/health/game-of-thrones.html'
        self.context.desktop.page_id = 'lp_got'


class SuperTopup(HealthLandingPage):
    def __init__(self, **kwargs):
        super(HealthLandingPage, self).__init__(**kwargs)

        self.context.template = 'campaign/health/super-topup-v2.html'
        self.context.page_id = 'lp_super_topup'
        self.context.page_title = 'Super Topup'
        self.context.insurer_list = [11, 6]

        self.context.desktop.main_body.heading = 'Enhance your Medical Cover @ Negligible Cost'
        self.context.desktop.main_body.sub_heading = 'Get Additional Coverage with SUPER TOP-UP'

        self.context.mobile.main_body.sub_heading = 'Get Extra Coverage with SUPER TOP-UP'

        self.cookies.lms_campaign = 'health-topup'


class SuperTopupOnline(SuperTopup):
    def __init__(self, **kwargs):
        super(SuperTopupOnline, self).__init__(**kwargs)

        self.context.template = 'campaign/health/super-topup-with-form.html'


class Ebook(HealthLandingPage):
    def __init__(self, **kwargs):
        super(HealthLandingPage, self).__init__(**kwargs)

        self.context.template = 'campaign/health/ebook.html'
        self.context.page_id = 'lp_ebook'


class ClickToCall(HealthLandingPage):
    def __init__(self, **kwargs):
        super(HealthLandingPage, self).__init__(**kwargs)
        product_keyword = 'Mediclaim' if self.view_id == 'mediclaim' else 'Health Insurance'

        self.context.template = 'campaign/health/click-to-call.html'

        self.context.desktop.page_id = 'product_mediclaim' if self.view_id == 'mediclaim' else 'product',
        self.context.desktop.skip_option = False
        self.context.desktop.keyword = product_keyword
        self.context.desktop.main_body.heading = 'Compare %s Policies' % product_keyword
        self.context.desktop.main_body.sub_heading = '%s Plans starting @ Rs 6 / day' % product_keyword


class Combo(HealthLandingPage):
    def __init__(self, **kwargs):
        super(Combo, self).__init__(**kwargs)
        self.context.template = 'campaign/health/combo.html'
        self.context.page_id = 'lp_combo'
        self.context.desktop.main_body.heading = 'Smartest trick to save loads of premium on health insurance!'
        self.context.desktop.main_body.sub_heading = 'Talk to our experts now and save Rs. 4500 a year!'


class Youtube(HealthLandingPage):
    def __init__(self, **kwargs):
        super(Youtube, self).__init__(**kwargs)

        self.context.template = 'campaign/health/product-youtube.html'
        self.context.page_id = 'lp_health_youtube_masthead'


class TopCompare(HealthLandingPage):
    def __init__(self, **kwargs):
        super(TopCompare, self).__init__(**kwargs)
        self.context.template = 'campaign/health/top-compare.html'
        self.context.page_title = 'Top Health Insurance Plans - Coverfox.com'
        self.context.page_id = 'lp_health_topcompare'


class TopCompareV2(HealthLandingPage):
    def __init__(self, **kwargs):
        super(TopCompareV2, self).__init__(**kwargs)
        self.context.template = 'campaign/health/top-compare-v2.html'
        self.context.page_title = 'Top Health Insurance Plans - Coverfox.com'
        self.context.page_id = 'lp_health_topcompare-v2'


class TopCompareV3(TopCompareV2):
    def __init__(self, **kwargs):
        super(TopCompareV3, self).__init__(**kwargs)
        self.context.template = 'campaign/health/top-compare-v3.html'
        self.context.page_id = 'lp_health_topcompare-v3'


class ProductV2(HealthLandingPage):
    def __init__(self, **kwargs):
        super(ProductV2, self).__init__(**kwargs)
        self.context.template = 'campaign/health/product-v2.html'
        self.context.page_id = 'lp_health_product-v2'


class ProductNew(HealthLandingPage):
    def __init__(self, **kwargs):
        super(ProductNew, self).__init__(**kwargs)
        self.context.template = 'campaign/health/product2.html'


class Expert(HealthLandingPage):
    def __init__(self, **kwargs):
        super(Expert, self).__init__(**kwargs)
        self.context.page_id = 'lp_health_expert_kavita'
        self.context.template = 'campaign/health/health-expert-v2.html'
        self.context.desktop.page_title = 'Expert Rated Policies | Health Insurance | Mediclaim Policy'
        self.context.desktop.main_body.heading = 'Get Free Insurance assessment from Kavita'
        self.context.desktop.main_body.sub_heading = '''
                Kavita has over 4 years of experience in the field of
                Health Insurance and she leads
                a team at Coverfox.com. Give us your details and get a call back from
                her with a no-obligation consultation within 24 hrs. '''
        self.context.mobile.page_title = 'Expert Rated Policies | Health Insurance | Mediclaim Policy'
        self.context.mobile.main_body.heading = 'Get Free Insurance assessment from Kavita'
        self.context.mobile.main_body.sub_heading = '''
                Kavita has over 4 years of experience in the field of
                Health Insurance and she leads
                a team at Coverfox.com. Give us your details and get a call back from
                her with a no-obligation consultation within 24 hrs. '''


class ProtectCancer(HealthLandingPage):
    def __init__(self, **kwargs):
        super(ProtectCancer, self).__init__(**kwargs)
        self.context.template = 'campaign/health/protect-cancer.html'
        self.context.page_id = 'lp_health_protect_cancer'
        self.context.desktop.main_body.heading = '1 out of 11 women are at a risk of Cancer'
        self.context.desktop.main_body.sub_heading = 'Protect your Wife @ Rs. 1500/yr with a Cancer Plan'
        self.context.mobile.main_body.heading = '1 out of 11 women are at a risk of Cancer'
        self.context.mobile.main_body.sub_heading = 'Protect your Wife @ Rs. 1500/yr with a Cancer Plan'


class ProtectCancer2(HealthLandingPage):
    def __init__(self, **kwargs):
        super(ProtectCancer2, self).__init__(**kwargs)
        self.context.template = 'campaign/health/cancer-plan.html'
        self.context.page_id = 'lp_health_protect_cancer2'
        self.context.desktop.main_body.heading = 'Cancer plans starting at just Rs. 2000/yr'
        self.context.mobile.main_body.heading = 'Cancer plans starting at just Rs. 2000/yr'


class ProtectCancer3(HealthLandingPage):
    def __init__(self, **kwargs):
        super(ProtectCancer3, self).__init__(**kwargs)
        self.context.template = 'campaign/health/cancer-plan-v2.html'
        self.context.page_id = 'lp_health_protect_cancer_V3'
        self.context.desktop.main_body.heading = 'Cancer Protect plan pays you cash if you get diagnosed with cancer'
        self.context.mobile.main_body.heading = 'Cancer Protect plan pays you cash if you get diagnosed with cancer'
        self.context.desktop.main_body.sub_heading = 'Get Rs. 20 Lakh cash* cover starting at Rs. 2000/yr'
        self.context.mobile.main_body.sub_heading = 'Get Rs. 20 Lakh cash* cover starting at Rs. 2000/yr'


class FamilyHealth(HealthLandingPage):
    def __init__(self, **kwargs):
        super(FamilyHealth, self).__init__(**kwargs)
        self.context.template = 'campaign/health/family-health-plans.html'
        self.context.page_id = 'lp_family_health'
        self.context.page_title = 'Top Health Insurance Plans - Coverfox.com'

class SaveTax(HealthLandingPage):
    def __init__(self, **kwargs):
        super(SaveTax, self).__init__(**kwargs)
        self.context.template = 'campaign/health/tax-save.html'
        self.context.page_id = 'lp_health_tax_saving'
        self.context.page_title = 'Top Health Insurance Plans - Coverfox.com'

class HealthGeneric(HealthLandingPage):
    def __init__(self, **kwargs):
        super(HealthGeneric, self).__init__(**kwargs)
        self.context.template = 'campaign/health/health-generic.html'
        self.context.page_id = 'lp_health_generic'
        self.context.page_title = 'Top Health Insurance Plans - Coverfox.com'

class TaxSaver(HealthLandingPage):
    def __init__(self, **kwargs):
        super(TaxSaver, self).__init__(**kwargs)
        self.context.template = 'campaign/health/health-insurance-new.html'
        self.context.page_id = 'lp_tax_saver'
        self.context.page_title = 'Top Health Insurance Plans - Coverfox.com'

class BuyHealthMobile(HealthLandingPage):
    def __init__(self, **kwargs):
        super(BuyHealthMobile, self).__init__(**kwargs)
        self.context.desktop.main_body.heading = 'Compare Health Insurance'
        self.context.mobile.main_body.heading = 'Compare <br>Health Insurance'
        self.context.desktop.page_id = "lp_buy_health_desktop"
        self.context.mobile.page_id = "lp_buy_health_mobile"


class BuyMedicalMobile(HealthLandingPage):
    def __init__(self, **kwargs):
        super(BuyMedicalMobile, self).__init__(**kwargs)
        self.context.desktop.main_body.heading = 'Compare Medical Insurance'
        self.context.mobile.main_body.heading = 'Compare <br>Medical Insurance'
        self.context.desktop.page_id = "lp_buy_medical_desktop"
        self.context.mobile.page_id = "lp_buy_medical_mobile"


class BuyMobile(HealthLandingPage):
    def __init__(self, **kwargs):
        super(BuyMobile, self).__init__(**kwargs)
        self.context.desktop.page_id = "lp_buy_desktop"
        self.context.mobile.page_id = "lp_buy_mobile"



from sys import modules
from inspect import isclass, getmembers

__all__ = [name for name, file_obj in getmembers(modules[__name__], isclass) if file_obj.__module__ == __name__]
