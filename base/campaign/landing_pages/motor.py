import os
import re
from inspect import getmembers, isclass
from sys import modules

from django.conf import settings
from django.contrib.staticfiles.storage import staticfiles_storage
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist

from motor_product.models import Insurer

from . import MotorLandingPage
from .. import car_presets as campaign_presets

TPARTY_LIST = []
TPARTY_LOGO_MAPPING = {}


def populate_tparty():
    if settings.STATIC_ROOT:
        _, files = staticfiles_storage.listdir('motor_product/img/motor_tparty/')
    else:
        files = os.listdir(
            os.path.join(
                settings.SETTINGS_FILE_FOLDER,
                '../static/motor_product/img/motor_tparty/'
            )
        )

    for f in files:
        f_name, _ = os.path.splitext(f)
        TPARTY_LIST.append(f_name.replace('-logo', '').title().replace('-', ''))


def populate_tparty_logo_mapping():
    for tparty in TPARTY_LIST:
        file_name = '-'.join(word.lower() for word in re.findall('[A-Z][^A-Z]*', tparty))
        TPARTY_LOGO_MAPPING.update({tparty: 'motor_product/img/motor_tparty/{}-logo.png'.format(file_name)})


class Buy(MotorLandingPage):
    def __init__(self, **kwargs):
        super(Buy, self).__init__(**kwargs)

        if kwargs.get('network', '').lower() == 'facebook':
            self.context.desktop.form.heading = 'Why Pay Extra to Dealer?'
            self.context.desktop.form.sub_heading = 'Renew Car Insurance Online'

            self.context.mobile.main_body.heading = 'Why Pay Extra to Dealer?'
            self.context.mobile.main_body.sub_heading = 'Renew Car Insurance Online'

        self.context.affiliate_name = kwargs.get('affiliate_name', '')


class Compare(Buy):
    def __init__(self, **kwargs):
        super(Compare, self).__init__(**kwargs)

        self.context.desktop.page_id = 'lp_compare'
        self.context.desktop.template = 'campaign/motor/buy-car-insurance-ver1.html'


class CarDekho(Buy):
    def __init__(self, **kwargs):
        super(CarDekho, self).__init__(**kwargs)

        self.context.page_id = 'lp_cardekho'
        self.context.template = 'campaign/motor/car-dekho-logo.html'


class LowestQuotes(Buy):
    def __init__(self, **kwargs):
        super(LowestQuotes, self).__init__(**kwargs)

        self.context.desktop.template = 'campaign/motor/switch.html'
        self.context.desktop.page_id = 'lp_lowest_quotes'
        self.context.desktop.page_title = 'Lowest Car Insurance Quotes | Compare Premium'

        self.context.desktop.form.heading = 'Get Lowest Car Insurance Quotes'
        self.context.desktop.form.sub_heading = 'Quotes starting from Rs 6 / day'
        self.context.desktop.form.button_text = 'Get Quick Quotes'


class Renew(Buy):
    def __init__(self, **kwargs):
        super(Renew, self).__init__(**kwargs)

        self.context.desktop.template = 'campaign/motor/renew-car-insurance.html'
        self.context.desktop.page_id = 'lp_renew'

        self.context.desktop.main_body.heading = 'Renew your Car Insurance before it expires'
        self.context.desktop.main_body.sub_heading = 'Get your policy without any paperwork in 2 min'
        self.context.desktop.main_body.points = ['Buy Online and save upto 60% on premium',
                                                 'Cashless Settlement in Nearest Garage',
                                                 'Guaranteed End to End Claims Assistance']

        self.context.desktop.form.button_text = 'Generate Best Plan >>'


class FourWheeler(Buy):
    def __init__(self, **kwargs):
        super(FourWheeler, self).__init__(**kwargs)

        self.context.desktop.template = 'campaign/motor/seo-switch.html'
        self.context.desktop.page_id = 'lp_four_wheeler'


class ExpiredCarInsurance(Buy):
    def __init__(self, **kwargs):
        super(ExpiredCarInsurance, self).__init__(**kwargs)

        self.context.template = 'campaign/motor/expired-car-insurance.html'
        self.context.desktop.page_id = 'lp_expired_car_insurance'
        self.context.desktop.page_title = 'Renew expired Car Insurance policy'

        self.context.desktop.main_body.heading = 'Get Lowest Prices On Car Insurance'
        self.context.desktop.main_body.sub_heading = 'Buy today and get ROAD SIDE ASSISTANCE product for FREE'
        self.context.desktop.main_body.points = ['Get a Dedicated Claims Manager',
                                                 'Cashless Settlement in nearest garage',
                                                 'Free towing and pick-up services',
                                                 'Guaranteed End to End Claims Assistance']


        self.context.desktop.form.button_text = 'Send me Quotes'

        self.context.mobile.page_id = 'lp_expired_car_insurance_mobile'
        self.context.mobile.page_title = 'Renew expired Car Insurance policy'

        self.context.mobile.main_body.heading = 'Get Lowest Prices On Car Insurance'
        self.context.mobile.main_body.sub_heading = 'Buy today and get ROAD SIDE ASSISTANCE product for FREE'
        self.context.mobile.main_body.points = ['Get a Dedicated Claims Manager',
                                                 'Cashless Settlement in nearest garage',
                                                 'Free towing and pick-up services',
                                                 'Guaranteed End to End Claims Assistance']
        self.context.mobile.form.button_text = 'Send me Quotes'
        if kwargs.get('network', '').upper() == 'FACEBOOK':
            self.cookies.lms_campaign = 'motor-FBoffline'
        else:
            self.cookies.lms_campaign = 'motor-offlinecases'


class ExpiredFourWheelerInsurance(ExpiredCarInsurance):
    def __init__(self, **kwargs):
        super(ExpiredFourWheelerInsurance, self).__init__(**kwargs)

        self.context.desktop.main_body.heading = 'Renew Expired Car Insurance in just 24 hrs'
        self.context.desktop.main_body.sub_heading = 'Get 100% Claims Support with Coverfox'
        self.context.desktop.main_body.points = ['Get Dedicated Relationship Manager',
                                                 'Free Towing and Pick-up Services',
                                                 'Cashless Settlement in Nearest Garage']

        self.context.desktop.form.button_text = 'Get Quick Quotes'

        if kwargs.get('network', '').upper() == 'FACEBOOK':
            self.cookies.lms_campaign = 'motor-FBoffline'
        else:
            self.cookies.lms_campaign = 'motor-offlinecases'


class FreeRsa(Buy):
    def __init__(self, **kwargs):
        super(FreeRsa, self).__init__(**kwargs)

        self.context.desktop.template = 'campaign/motor/free-rsa.html'
        self.context.desktop.page_id = 'lp_free_rsa'
        self.context.desktop.page_title = 'Get Free Road Side Assitance along with Car Insurance'

        self.context.desktop.form.heading = 'Compare Car Insurance Quotes'
        self.context.desktop.form.sub_heading = 'Buy today and get ROAD SIDE ASSISTANCE product for FREE'
        self.context.desktop.form.button_text = 'Get Instant Quotes'


class RenewExpiredPolicy(MotorLandingPage):
    def __init__(self, **kwargs):
        super(RenewExpiredPolicy, self).__init__(**kwargs)

        self.context.template = 'campaign/motor/renew-expired-policy.html'
        self.context.page_title = 'Renew expired Car Insurance policy'
        self.context.tparty = kwargs.get('tparty', None)

        self.context.desktop.page_id = 'lp_renew_expired_policy'

        self.context.mobile.page_id = 'lp_expired_policy'

        self.cookies.lms_campaign = 'motor-offlinecases'


class ClaimsSupport(MotorLandingPage):
    def __init__(self, **kwargs):
        super(ClaimsSupport, self).__init__(**kwargs)

        self.context.desktop.template = 'campaign/motor/buy-car-insurance.html'
        self.context.desktop.page_id = 'lp_car_claims_support'
        self.context.desktop.page_title = 'Get 100% Claims Support with Coverfox'
        self.context.desktop.parent_category = 'Car Insurance'

        self.context.desktop.main_body.heading = 'Get 100% Claims Support with Coverfox'
        self.context.desktop.main_body.sub_heading = 'Get Upto 60% Discount on Premium'
        self.context.desktop.main_body.points = ['Get dedicated claims manager',
                                                 'Cashless settlement in nearest Garage',
                                                 'Free towing, pick-up & quick survey']

        self.context.desktop.form.heading = 'Calculate your car insurance premium'
        self.context.desktop.form.sub_heading = 'Get Instant quotes from insurance companies'
        self.context.desktop.form.button_text = 'Show me My Quotes'


class CashlessSettlement(MotorLandingPage):
    def __init__(self, **kwargs):
        super(CashlessSettlement, self).__init__(**kwargs)

        self.context.desktop.template = 'campaign/motor/buy-car-insurance.html'
        self.context.desktop.page_id = 'lp_car_cashless_settlement'
        self.context.desktop.page_title = 'Get cashless settlement in nearest garage'
        self.context.desktop.parent_category = 'Car Insurance'

        self.context.desktop.main_body.heading = 'Get cashless settlement in nearest garage'
        self.context.desktop.main_body.sub_heading = 'Free of cost claims services for you'
        self.context.desktop.main_body.points = ['Get dedicated claims manager',
                                                 'Free towing and pick-up services for car breakdown',
                                                 'Quick survey schedules',
                                                 'Genuine Car parts guarantee']

        self.context.desktop.form.heading = 'Calculate your car insurance premium'
        self.context.desktop.form.sub_heading = 'Get Instant quotes from insurance companies'
        self.context.desktop.form.button_text = 'Show me My Quotes'


class ZeroDepCover(MotorLandingPage):
    def __init__(self, **kwargs):
        super(ZeroDepCover, self).__init__(**kwargs)

        self.context.desktop.template = 'campaign/motor/zero-dep.html'
        self.context.desktop.page_id = 'lp_car_zero_dep_cover'
        self.context.desktop.page_title = 'Save upto 60% on Zero Depreciation policy'
        self.context.desktop.parent_category = 'Car Insurance'

        self.context.desktop.main_body.heading = 'Save upto 60% on Zero Depreciation policy'
        self.context.desktop.main_body.sub_heading = 'Also available for Old Cars'
        self.context.desktop.main_body.points = ['Get Instant Policy in under 2 minutes',
                                                 'Cashless Settlement in nearest garage',
                                                 'Free towing and pick-up services',
                                                 'Guaranteed End to End Claims Assistance']

        self.context.desktop.form.heading = 'Renew Car Insurance in 2 minutes'
        self.context.desktop.form.sub_heading = 'Get Upto 60% Discount on Premium'
        self.context.desktop.form.button_text = 'Get Instant Quotes'


class OnlineDiscount(MotorLandingPage):
    def __init__(self, **kwargs):
        super(OnlineDiscount, self).__init__(**kwargs)

        self.context.desktop.template = 'campaign/motor/buy-car-insurance-ver1.html'
        self.context.desktop.page_id = 'lp_car_online_discount'
        self.context.desktop.page_title = 'Get upto 60% discount on online purchases'
        self.context.desktop.parent_category = 'Car Insurance'

        self.context.desktop.main_body.heading = 'Get upto 60% discount on online purchases'
        self.context.desktop.main_body.sub_heading = 'Compare Car Insurance quotes from leading companies'
        self.context.desktop.main_body.points = ['Get Instant Policy in under 2 minutes',
                                                 'No documents required',
                                                 'Guaranteed End to End Claims Assistance',
                                                 'Free towing and pick-up services']

        self.context.desktop.form.heading = 'Calculate your car insurance premium'
        self.context.desktop.form.sub_heading = 'Get Instant quotes from insurance companies'
        self.context.desktop.form.button_text = 'Show me My Quotes'


class EmailOnly(Buy):
    def __init__(self, **kwargs):
        super(EmailOnly, self).__init__(**kwargs)

        self.context.desktop.page_id = 'lp_email_only'
        self.context.desktop.template = 'campaign/motor/email_only.html'


class GetQuotes(Buy):
    def __init__(self, **kwargs):
        super(GetQuotes, self).__init__(**kwargs)

        self.context.page_id = 'lp_get_quotes'
        self.context.template = 'campaign/motor/affiliate.html'

        self.cookies.utm_campaign = 'Affiliate_Email_VD_Car'
        self.cookies.lms_campaign = 'motor-email'


class Basanti(Buy):
    def __init__(self, **kwargs):
        super(Basanti, self).__init__(**kwargs)

        self.context.page_id = 'lp_basanti'
        self.context.template = 'campaign/motor/basanti.html'


class BuyDirect(Buy):
    def __init__(self, **kwargs):
        super(BuyDirect, self).__init__(**kwargs)

        self.context.page_id = 'lp_buy_direct'
        self.context.template = 'campaign/motor/buy-direct.html'

        self.cookies.mode = 'unassisted'
        self.cookies.lms_campaign = 'motor-unassisted'


class BuyOnline(Buy):
    def __init__(self, **kwargs):
        super(BuyOnline, self).__init__(**kwargs)

        self.context.page_id = 'lp_buy_online'
        self.context.template = 'campaign/motor/buy-online.html'


class BuyDirectOnline(Buy):
    def __init__(self, **kwargs):
        super(BuyDirectOnline, self).__init__(**kwargs)

        self.context.page_id = 'lp_buy_direct_online'
        self.context.template = 'campaign/motor/buy-direct-online.html'
        self.cookies.mode = 'unassisted'
        self.cookies.lms_campaign = 'motor-unassisted'


class GetCover(Buy):
    def __init__(self, **kwargs):
        super(GetCover, self).__init__(**kwargs)
        self.context.page_id = 'lp_buy_direct_online_v2'
        tparty = kwargs.get('tparty', '')

        if tparty in TPARTY_LIST:
            self.set_tparty(tparty)

        if kwargs.get('network', '').lower() == 'facebook':
            self.context.desktop.form.heading = 'Why Pay Extra to Dealer?'
            self.context.desktop.form.sub_heading = 'Renew Car Insurance Online'

            self.context.mobile.form.heading = 'Why Pay Extra to Dealer?'
            self.context.mobile.form.sub_heading = 'Renew Car Insurance Online'
        else:
            self.context.desktop.form.heading = 'Get Lowest prices on Car Insurance'
            self.context.desktop.form.sub_heading = ''

            self.context.mobile.form.heading = 'Get Lowest prices on Car Insurance'
            self.context.mobile.form.sub_heading = ''

        self.context.template = 'campaign/motor/buy-direct-online-v2.html'

    def set_tparty(self, tparty):
            self.context.view_id = "tparty"
            self.context.tparty = tparty
            self.context.tparty_src = TPARTY_LOGO_MAPPING.get(tparty)
            self.context.tparty_alt = tparty
            self.context.page_id = 'lp_get_cover_{}'.format(tparty.lower())


class Brand(GetCover):
    def __init__(self, **kwargs):
        super(Brand, self).__init__(**kwargs)
        try:
            insurer_slug = kwargs.get('brand')
            insurer = Insurer.objects.get(slug=insurer_slug)
            brand_data = campaign_presets.brand_dict[insurer.id]
        except (ObjectDoesNotExist, MultipleObjectsReturned, KeyError):
            pass
        else:
            self.context.is_active = insurer.is_active_for_car
            self.context.insurer = insurer
            self.context.page_id = 'lp_brand_%s' % insurer_slug
            self.context.logo_url = campaign_presets.logo_dict.get(insurer.id, None)
            self.context.page_title = 'Buy %s Car Insurance Policy from Coverfox' % brand_data['title']
            self.context.brand = brand_data

            self.context.desktop.template = 'campaign/motor/new_brand.html'

            self.context.mobile.template = 'campaign/motor/mobile-brand.html'

            self.cookies.brand_id = insurer.id


class CrossSell(Buy):
    def __init__(self, **kwargs):
        super(CrossSell, self).__init__(**kwargs)

        self.context.desktop.page_id = 'lp_cross_sell'
        self.context.desktop.template = 'campaign/motor/cross-sell.html'
        self.context.desktop.form.heading = 'Get the CHEAPEST car insurance quote for your next renewal'
        self.context.desktop.form.sub_heading = 'Provide your details now'
        self.context.desktop.form.button_text = 'Submit'
        self.cookies.lms_campaign = 'motor-cross-cell'


class Youtube(Buy):
    def __init__(self, **kwargs):
        super(Youtube, self).__init__(**kwargs)

        self.context.page_id = 'lp_motor_youtube_masthead'
        self.context.template = 'campaign/motor/switch-youtube.html'


class Cms(Buy):
    def __init__(self, **kwargs):
        super(Cms, self).__init__(**kwargs)

        self.context.page_id = 'lp_motor_cms'
        self.context.template = 'campaign/motor/car-cms-template.html'


class BuyOnlineV2(Buy):
    def __init__(self, **kwargs):
        super(BuyOnlineV2, self).__init__(**kwargs)

        self.context.page_id = 'lp_buy_online'
        self.context.template = 'campaign/motor/buy-online-v2.html'


class EbookDownload(Buy):
    def __init__(self, **kwargs):
        super(EbookDownload, self).__init__(**kwargs)

        self.context.page_id = 'lp_buy_online'
        self.context.template = 'campaign/motor/ebook-download.html'


class BuyOtp(Buy):
    def __init__(self, **kwargs):
        super(BuyOtp, self).__init__(**kwargs)

        self.context.page_id = 'lp_buy_otp'
        self.context.template = 'campaign/motor/otp.html'
        self.context.affiliate_name = kwargs.get('affiliate_name', '')


class BuyNew(Buy):
    def __init__(self, **kwargs):
        super(BuyNew, self).__init__(**kwargs)

        self.context.page_id = 'lp_buy_new'
        self.context.template = 'campaign/motor/buy-new.html'


__all__ = [name for name, file_obj in getmembers(modules[__name__], isclass) if file_obj.__module__ == __name__]
