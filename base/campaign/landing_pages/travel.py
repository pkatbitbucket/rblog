__author__ = 'vinay'
from . import TravelLandingPage


class Buy(TravelLandingPage):
    pass

class SeniorCitizens(TravelLandingPage):
 def __init__(self, **kwargs):
    super(SeniorCitizens, self).__init__(**kwargs)
    self.context.template = 'campaign/travel/senior-citizens-travel.html'


from sys import modules
from inspect import isclass, getmembers

__all__ = [name for name, file_obj in getmembers(modules[__name__], isclass) if file_obj.__module__ == __name__]
