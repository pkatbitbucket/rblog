__author__ = 'vinay'
import logging
from collections import OrderedDict
from urlparse import urlparse

from django.template.defaultfilters import slugify
from django.conf import settings
from django.core.urlresolvers import resolve, Resolver404
from django_user_agents.utils import get_user_agent

from utils import log_error

logger = logging.getLogger(__name__)


# THESE COOKIES SHOULD ALWAYS BE PRESENT
COMPULSORY_COOKIES = {
    # looks like user_agents middleware is getting skipped in some cases
    # hence get_user_agent
    'device': lambda req, res: res.set_cookie('device', 'mobile' if get_user_agent(req).is_mobile else 'desktop'),
}

# THESE WILL BE SET BY THE URL PARAMS
TRACKING_COOKIES = [
    'utm_source',
    'utm_medium',
    'utm_campaign',
    'utm_adgroup',
    'utm_term',
    'utm_content',
    'network',
    'category',
    'gclid',
]

TRACKING_TIMED = [
    'brand',
    'hide_login'
]

# THIS IS USED TO IDENTIFY THE PRODUCT BASED ON THE URL CONTENT
PRODUCT_ALIAS = OrderedDict((
    ('health-topup', ['topup', 'top-up']),
    ('health', ['health']),
    ('motor-bike', ['bike', 'twowheeler', 'two-wheeler']),
    ('motor', ['car', 'fourwheeler']),
    ('travel', ['travel']),
    ('home', ['home', 'house']),
    ('term', ['term', 'life']),
))

# PRODUCTS FOR WHICH MULTIPLE CAMPAIGNS EXIST
PRIMRY_PRODUCTS = [
    'health',
    'motor',
    'travel',
]

NETWORKS = (
    'SEARCH',
    'SEARCH-UNASSISTED',
    'Y!SEARCH',
    'DISPLAY',
)

# FAIL SAFE FOR THE PRODUCT TYPE
DEFAULT_PRODUCT = 'motor'

ORGANIC_LANDING_PAGE_URLNAMES = (
    'category_post_list',
)

ORGANIC_LANDING_PAGE_PATHS = (
    '/car-insurance/',
    '/resources/car-insurance/',

    '/health-insurance/',
    '/resources/health-insurance/',

    '/two-wheeler-insurance/',
    '/resources/twowheeler-insurance/',

    '/resources/travel-insurance/',
)


class LMSMiddleware(object):
    def process_response(self, request, response):
        if not (request.is_ajax() or request.path.startswith(settings.STATIC_URL) or request.path.startswith(
                settings.MEDIA_URL)):
            for cookie, value_func in COMPULSORY_COOKIES.iteritems():
                if cookie not in response.cookies.keys():
                    value_func(request, response)

            for key, value in request.GET.iteritems():
                if key in TRACKING_COOKIES:
                    try:
                        response.set_cookie(key, value)
                    except:
                        log_error(logger, request,
                                  msg="Campaign Middleware error: Cannot set cookie")
                elif key in TRACKING_TIMED:
                    try:
                        response.set_cookie(key, value, expires=600)
                    except:
                        log_error(logger, request,
                                  msg="Campaign Middleware error: Cannot set Timed cookie")
                else:
                    try:
                        response.set_cookie(
                            'vt_{}'.format(key), slugify(value))
                    except:
                        log_error(logger, request,
                                  msg="Campaign Middleware error: Cannot set VT cookie")

            lms_campaign, network, product = get_campaign_and_network(request)
            if 'lms_campaign' not in response.cookies:
                response.set_cookie('lms_campaign', lms_campaign)

            if 'network' not in response.cookies:
                response.set_cookie('network', network)

            response.set_cookie('mark_product_category', product)

        return response


def get_campaign_and_network(request, current_path=None, product_type=''):
    current_path = current_path or request.path

    if product_type not in PRODUCT_ALIAS:
        for product, values in PRODUCT_ALIAS.iteritems():
            if any([value in current_path for value in values]):
                product_type = product
                break
        else:
            product_type = DEFAULT_PRODUCT

    mark_product_category = request.COOKIES.get('mark_product_category', '')
    lms_campaign = request.COOKIES.get('lms_campaign', '')
    network = request.COOKIES.get('network', 'direct')
    category = request.COOKIES.get('category', '')

    if mark_product_category != product_type or not (lms_campaign and network.upper() != 'DIRECT'):
        network = request.GET.get('network', '').upper() or network.upper()
        category = request.GET.get('category', '').upper() or category.upper()
        lms_campaign = product_type

        lp = request.COOKIES.get('landing_page_url', '') or request.build_absolute_uri()
        lp_path = urlparse(lp)[2]
        if product_type in PRIMRY_PRODUCTS:
            if network == 'FACEBOOK-TVC':
                lms_campaign = '{}-tvc-fb'.format(product_type)
            elif network == 'FACEBOOK' or network.startswith('FACEBOOK-'):
                lms_campaign = '{}-FB'.format(product_type)
            elif network == 'AFFILIATE' and product_type == 'motor':
                lms_campaign = 'motor-affiliate'
            elif network == 'GSP' and product_type == 'motor':
                lms_campaign = 'motor-GSP'
            elif network == 'Y!SEARCH' and product_type == 'motor':
                lms_campaign = 'motor-YahooSearch'
            elif network not in NETWORKS:
                if product_type not in ['travel']:
                    if network == 'YOUTUBE':
                        lms_campaign = '{}-tvc-{}'.format(product_type, network)
                    elif lp_path == '/':
                        # HACK FOR TVC
                        suffixes = {
                            'YOUTUBE': 'youtube',
                            'YAHOO-LOGIN': 'yahoo',
                            'YAHOO-PREROLL': 'yahoo',
                            'YAHOO-INBANNER': 'yahoo',
                            'TEAM-BHP': 'bhp',
                        }
                        lms_campaign = '{}-homepage'.format(product_type)
                        if network == 'TEAM-BHP':
                            product_type = 'motor'

                        if network in suffixes:
                            lms_campaign = '{}-tvc-{}'.format(product_type, suffixes[network])
                            # HACK ENDS
                    else:
                        lms_campaign = '{}-direct'.format(product_type)
            else:
                if product_type == 'motor' and network == 'SEARCH' and category in ['AGGREGATOR', 'BRAND']:
                    lms_campaign = 'motor-search{}'.format(category.title())
                elif product_type not in ['travel']:
                    lms_campaign = '{}-search{}'.format(product_type, 'M' if get_user_agent(request).is_mobile else 'D')

        try:
            url_match = resolve(lp_path)
        except Resolver404:
            # This happens in the case of append_slash redirect on the first request
            pass
        else:
            if url_match.namespaces or url_match.url_name:
                if ':'.join(url_match.namespaces + [url_match.url_name or '']) in ORGANIC_LANDING_PAGE_URLNAMES \
                        or any(lp_path.startswith(path) for path in ORGANIC_LANDING_PAGE_PATHS):
                    network = 'ORGANIC'
                    lms_campaign = '{}-organic'.format(product_type)

        if request.GET.get('utm_source', '').lower() == 'justdial':
            network = 'JUSTDIAL'
        elif request.GET.get('utm_source', '').lower() == 'youtube-masthead':
            lms_campaign = 'motor-tvc-youtube'

    return lms_campaign, network, product_type
