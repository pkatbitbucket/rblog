# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AdcopyCreative',
            fields=[
                ('last_modified', models.DateTimeField(
                    auto_now=True, auto_created=True)),
                ('created_on', models.DateTimeField(
                    auto_now_add=True, auto_created=True)),
                ('creative_id', models.CharField(max_length=255, unique=True,
                                                 serialize=False, verbose_name='Creative ID', primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Creative Name')),
                ('data', jsonfield.fields.JSONField(
                    default={}, verbose_name='Creative Data', blank=True)),
                ('hash_value', models.CharField(
                    db_index=True, max_length=255, blank=True)),
                ('is_live', models.BooleanField(default=True, db_index=True)),
            ],
        ),
        migrations.CreateModel(
            name='AdGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('camp', models.CharField(max_length=50)),
                ('title', models.CharField(unique=True,
                                           max_length=255, verbose_name='Title')),
                ('extra', jsonfield.fields.JSONField(
                    default={}, verbose_name='extra', blank=True)),
                ('template', models.CharField(max_length=255,
                                              verbose_name='Backend Template Name', blank=True)),
                ('created', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
            ],
        ),
        migrations.CreateModel(
            name='Keyword',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255,
                                           verbose_name='Title', blank=True)),
                ('slug', models.SlugField(unique=True,
                                          max_length=255, verbose_name='slug')),
                ('extra', jsonfield.fields.JSONField(
                    default={}, verbose_name='extra', blank=True)),
                ('template', models.CharField(max_length=255,
                                              verbose_name='Backend Template Name', blank=True)),
                ('created', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
            ],
        ),
        migrations.CreateModel(
            name='UtmAdGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(unique=True,
                                           max_length=255, verbose_name='Title')),
                ('slug', models.SlugField(unique=True,
                                          max_length=255, verbose_name='slug')),
                ('extra', jsonfield.fields.JSONField(
                    default={}, verbose_name='extra', blank=True)),
                ('is_verified', models.BooleanField(default=True)),
                ('template', models.CharField(max_length=255,
                                              verbose_name='Backend Template Name', blank=True)),
                ('created', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
            ],
        ),
        migrations.CreateModel(
            name='UtmCampaign',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(unique=True,
                                           max_length=255, verbose_name='Title')),
                ('slug', models.SlugField(unique=True,
                                          max_length=255, verbose_name='slug')),
                ('extra', jsonfield.fields.JSONField(
                    default={}, verbose_name='extra', blank=True)),
                ('is_verified', models.BooleanField(default=True)),
                ('template', models.CharField(max_length=255,
                                              verbose_name='Backend Template Name', blank=True)),
                ('created', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
            ],
        ),
        migrations.CreateModel(
            name='UtmKeyword',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255,
                                           verbose_name='Title', blank=True)),
                ('pattern', models.CharField(max_length=255, blank=True)),
                ('slug', models.SlugField(unique=True,
                                          max_length=255, verbose_name='slug')),
                ('extra', jsonfield.fields.JSONField(
                    default={}, verbose_name='extra', blank=True)),
                ('is_verified', models.BooleanField(default=True)),
                ('template', models.CharField(max_length=255,
                                              verbose_name='Backend Template Name', blank=True)),
                ('created', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
            ],
        ),
        migrations.CreateModel(
            name='UtmMapping',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_verified', models.BooleanField(default=True)),
                ('ad_group', models.ForeignKey(
                    blank=True, to='campaign.UtmAdGroup', null=True)),
                ('campaign', models.ForeignKey(blank=True,
                                               to='campaign.UtmCampaign', null=True)),
                ('keyword', models.ForeignKey(blank=True,
                                              to='campaign.UtmKeyword', null=True)),
            ],
        ),
    ]
