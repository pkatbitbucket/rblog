# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0001_initial'),
        ('wiki', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='utmkeyword',
            name='insurer',
            field=models.ForeignKey(blank=True, to='wiki.Insurer', null=True),
        ),
        migrations.AddField(
            model_name='utmcampaign',
            name='insurer',
            field=models.ForeignKey(blank=True, to='wiki.Insurer', null=True),
        ),
        migrations.AddField(
            model_name='utmadgroup',
            name='insurer',
            field=models.ForeignKey(blank=True, to='wiki.Insurer', null=True),
        ),
        migrations.AddField(
            model_name='keyword',
            name='ad_group',
            field=models.ForeignKey(to='campaign.AdGroup'),
        ),
        migrations.AddField(
            model_name='adgroup',
            name='insurer',
            field=models.ForeignKey(blank=True, to='wiki.Insurer', null=True),
        ),
    ]
