import hashlib

from django.db import models
from django.utils.translation import ugettext_lazy as _
from jsonfield import JSONField

from wiki.models import Insurer


class AdGroup(models.Model):
    camp = models.CharField(max_length=50)
    insurer = models.ForeignKey(Insurer, blank=True, null=True)
    title = models.CharField(_('Title'), max_length=255, unique=True)
    extra = JSONField(_('extra'), blank=True, default={})
    template = models.CharField(
        _('Backend Template Name'), max_length=255, blank=True)
    created = models.DateTimeField(_('created'), auto_now_add=True)

    def __unicode__(self):
        return self.title


class Keyword(models.Model):
    ad_group = models.ForeignKey('AdGroup')
    title = models.CharField(_('Title'), max_length=255, blank=True)
    slug = models.SlugField(_('slug'), max_length=255, unique=True)
    extra = JSONField(_('extra'), blank=True, default={})
    template = models.CharField(
        _('Backend Template Name'), max_length=255, blank=True)
    created = models.DateTimeField(_('created'), auto_now_add=True)

    def __unicode__(self):
        return self.title


class UtmKeyword(models.Model):
    insurer = models.ForeignKey(Insurer, blank=True, null=True)
    title = models.CharField(_('Title'), max_length=255, blank=True)
    pattern = models.CharField(max_length=255, blank=True)
    slug = models.SlugField(_('slug'), max_length=255, unique=True)
    extra = JSONField(_('extra'), blank=True, default={})
    is_verified = models.BooleanField(default=True)
    template = models.CharField(
        _('Backend Template Name'), max_length=255, blank=True)
    created = models.DateTimeField(_('created'), auto_now_add=True)

    def __unicode__(self):
        return self.title


class UtmAdGroup(models.Model):
    insurer = models.ForeignKey(Insurer, blank=True, null=True)
    title = models.CharField(_('Title'), max_length=255, unique=True)
    slug = models.SlugField(_('slug'), max_length=255, unique=True)
    extra = JSONField(_('extra'), blank=True, default={})
    is_verified = models.BooleanField(default=True)
    template = models.CharField(
        _('Backend Template Name'), max_length=255, blank=True)
    created = models.DateTimeField(_('created'), auto_now_add=True)

    def __unicode__(self):
        return self.title


class UtmCampaign(models.Model):
    insurer = models.ForeignKey(Insurer, blank=True, null=True)
    title = models.CharField(_('Title'), max_length=255, unique=True)
    slug = models.SlugField(_('slug'), max_length=255, unique=True)
    extra = JSONField(_('extra'), blank=True, default={})
    is_verified = models.BooleanField(default=True)
    template = models.CharField(
        _('Backend Template Name'), max_length=255, blank=True)
    created = models.DateTimeField(_('created'), auto_now_add=True)

    def __unicode__(self):
        return self.title


class UtmMapping(models.Model):
    keyword = models.ForeignKey('UtmKeyword', blank=True, null=True)
    ad_group = models.ForeignKey('UtmAdGroup', blank=True, null=True)
    campaign = models.ForeignKey('UtmCampaign', blank=True, null=True)
    is_verified = models.BooleanField(default=True)


class LiveAdcopyCreativeManager(models.Manager):

    def get_queryset(self):
        return super(LiveAdcopyCreativeManager, self).get_queryset().filter(is_live=True)


class AdcopyCreative(models.Model):
    creative_id = models.CharField(
        _('Creative ID'), primary_key=True, max_length=255, unique=True)
    name = models.CharField(_('Creative Name'), max_length=255)
    data = JSONField(_('Creative Data'), default={}, blank=True)
    hash_value = models.CharField(
        max_length=255, unique=False, blank=True, db_index=True)
    created_on = models.DateTimeField(
        auto_now_add=True, editable=False, auto_created=True)
    last_modified = models.DateTimeField(
        auto_now=True, editable=True, auto_created=True)
    is_live = models.BooleanField(default=True, db_index=True)

    live_objects = LiveAdcopyCreativeManager()
    objects = models.Manager()

    def __str__(self):
        return self.creative_id

    def get_group_members(self):
        return AdcopyCreative.objects.filter(hash_value=self.hash_value).exclude(creative_id=self.creative_id)

    def save(self, *args, **kwargs):
        group_fields = {
            'main_body': ['heading', 'sub_heading'],
            'form': ['heading', 'sub_heading', 'button_text'],
        }
        hash_key = ''
        for key, values in group_fields.items():
            if key in self.data:
                for value in values:
                    try:
                        hash_key = hash_key + \
                            self.data[key].get(value, ' ').strip()
                    except:
                        pass
        if hash_key:
            self.hash_value = hashlib.sha256(hash_key).hexdigest()

        super(AdcopyCreative, self).save(*args, **kwargs)
