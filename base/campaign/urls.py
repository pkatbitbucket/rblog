from django.conf.urls import url

from . import views

urlpatterns = [
    # For admin only
    url(r'^sem/upload/$', views.sem_adcopy_upload, name='sem_adcopy_upload'),
    url(r'^sem/view/$', views.sem_adcopy_view, name='sem_adcopy_view'),

    # Generic
    url(r'^insurance/$', views.insurance, name='insurance'),

    # Justdial
    url(r'^health-insurance/jd/$', views.jd_widget, name='jd_widget'),

    # FOR HEALTH
    url(r'^health-insurance/plan/topup/tp/(?P<tp_slug>[\w-]+)/$',
        views.tp_widget, {'product': 'health', 'plan': 'topup'}),
    url(r'^health-insurance/tp/(?P<tp_slug>[\w-]+)/$', views.tp_widget, {'product': 'health', }),
    url(r'^health-insurance/(?P<view_id>[\w-]+)/$',
        views.BaseLandingPageView.as_view(product='health'), name='health_lp'),

    # FOR CAR
    url(r'^car-insurance/tp/(?P<tp_slug>[\w-]+)/$', views.tp_widget, {'product': 'motor', }),
    url(r'^car-insurance/(?P<view_id>[\w-]+)/$', views.BaseLandingPageView.as_view(product='motor'), name='motor_lp'),

    # FOR BIKE
    url(r'^bike-insurance/(?P<view_id>[\w-]+)/$', views.BaseLandingPageView.as_view(product='bike'), name='bike_lp'),

    # FOR TRAVEL
    url(r'^travel-insurance/(?P<view_id>[\w-]+)/$',
        views.BaseLandingPageView.as_view(product='travel'), name='travel_lp'),

    # For list of all landing pages
    url(r'^all/$', views.lp_list, name='lp_list'),
]
