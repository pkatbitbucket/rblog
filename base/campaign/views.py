import os
import datetime
from importlib import import_module

from django.http import HttpResponseRedirect, Http404
from django.contrib.auth.decorators import login_required
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.views.generic import View
from django.views.generic.base import classonlymethod
from django.shortcuts import render

from .forms import AdcopyMarriageFileForm, AdcopyEditForm, AdcopyCreative
from .health_views import *
from .car_views import *
from . import health_presets
from . import car_presets
from . import camp_utils

import pkgutil
import re
import utils
import landing_pages


def get_metadata(keyword, lp):
    metadata = {
        'page_title': 'Coverfox Insurance - Compare and Buy Policy Online',
        'keywords': 'Car Insurance, Coverfox, Compare Premium, Insurance Online',
        'description': 'Get lowest Car Insurance prices on Coverfox. Get 60% discount on premium'
    }
    for key, value in lp.META_DICT.iteritems():
        if keyword in key:
            return lp.keyword_data.get(value, metadata)
    return metadata


class BaseLandingPageView(View):
    def __init__(self, **kwargs):
        super(BaseLandingPageView, self).__init__(**kwargs)
        self.lp_module = import_module('.{}'.format(self.product), 'campaign.landing_pages')

    def dispatch(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)

    @camp_utils.route_lp
    def get(self, request, *args, **kwargs):
        lp_class = getattr(self.lp_module, self.view_class)
        lp = lp_class(**kwargs)
        lp.cookies.landing_page_url = request.build_absolute_uri()

        if self.device == 'mobile':
            context = lp.context.mobile or lp.context.desktop or lp.context
            template = lp.context.mobile.template or lp.context.desktop.template or lp.context.template
        else:
            context = lp.context.desktop or lp.context.mobile or lp.context
            template = lp.context.desktop.template or lp.context.mobile.template or lp.context.template

        if hasattr(self, 'creative') and self.creative:
            context.page_id = 'lp_creative_{}'.format(self.creative.creative_id)
            context.update(self.creative.data)

            lp.cookies.creative_id = self.creative.creative_id

        keyword = self.get_data.get('keyword', '')
        if keyword and hasattr(lp, 'keyword_data'):
            # TODO: Add the keyword_data attribute as OrderedDict to respective product landing page base class
            metadata = get_metadata(keyword, lp)
            context.update(metadata)

        response = render(request, template, context)
        for cookie_name, cookie_val in lp.cookies.items():
            if cookie_name == 'mode':
                response.set_cookie(cookie_name, cookie_val, expires=600)
            else:
                response.set_cookie(cookie_name, cookie_val)

        return response

    @classonlymethod
    def as_view(cls, **initkwargs):
        @never_cache
        def view(request, *args, **kwargs):
            self = cls(**initkwargs)
            self.view_id = kwargs.pop('view_id')
            self.request = request
            self.device = 'mobile' if request.user_agent.is_mobile else 'desktop'
            self.args = args
            self.kwargs = kwargs
            return self.dispatch(request, *args, **kwargs)

        return view

    @property
    def view_class(self):
        return self.view_id.title().replace('-', '').replace('_', '')


@xframe_options_exempt
def tp_widget(request, tp_slug, product, plan=None):
    """
    iFrame to be embedded on third-party sites
    """
    _DICT = {'health': health_presets.TP_DICT,
             'motor': car_presets.MOTOR_TP_DICT}[product]
    TP_DICT = _DICT.get(tp_slug)
    TP_DICT['redirect_delay'] = TP_DICT.get('redirect_delay', 0)
    if not TP_DICT:
        raise Http404

    is_topup = (plan == 'topup')
    email_required = not is_topup
    mobile_required = not is_topup
    template_data = {
        'site_url': settings.SITE_URL,
        'tp_slug': tp_slug,
        'iframe_id': request.GET.get('iframe_id'),
        'is_topup': plan == 'topup',
        'email_required': email_required,
        'mobile_required': mobile_required,
    }
    template_data.update(TP_DICT)
    url = 'campaign/%s/tp_widget.html' % product
    return render_to_response(url, {}, context_instance=RequestContext(request, template_data))


@never_cache
def insurance(request):
    data_context = {
        'page_id': 'lp_generic',
        'title': 'Car Insurance, Health / Mediclaim Policy, Two Wheeler & Bike Insurance | Compare Quotes & Renew Online'
    }
    template_path = select_template(['campaign/marketing/insurance.html'])
    context_object = RequestContext(request, data_context)
    response = HttpResponse(template_path.render(context_object))
    master_cookies = {
        'lms_campaign': 'motor',
        'landing_page_url': request.build_absolute_uri(),
        'mode': 'assisted',
        'vt_home_visited': 'No'
    }
    for cookie_name, cookie_val in master_cookies.items():
        response.set_cookie(cookie_name, cookie_val)
    return response


@login_required
def sem_adcopy_upload(request):
    success = False
    resp = None
    if request.user.is_superuser and request.user.is_staff:
        if request.method == 'POST':
            form = AdcopyMarriageFileForm(request.POST, request.FILES)
            if form.is_valid():
                xl_file = request.FILES['xl_file']
                dump_file = os.path.join(settings.MEDIA_ROOT, 'adcopy_marriage', '{}_{}.{}'.format(
                    xl_file.name, datetime.datetime.now(), 'xlsx' if xl_file.name.endswith('xlsx') else 'xls'))
                default_storage.save(dump_file, ContentFile(xl_file.read()))
                xl_file.close()
                success, resp = camp_utils.marry_ad_copy(dump_file)
        else:
            form = AdcopyMarriageFileForm()
        return render_to_response('campaign/adcopy_marriage.html', RequestContext(request, {
            'form': form if not success else None,
            'image': 'http://s2.quickmeme.com/img/c3/c37a6cc5f88867e5387b8787aaf67afc350b3f37f357ed0a3088241488063bce.jpg' if success else None,
            'success': success,
            'resp': resp,
        }))
    return HttpResponseRedirect('/')


@login_required
def get_adcopy_data(request):
    if request.user.is_superuser and request.user.is_staff and request.is_ajax():
        pass


@login_required
def sem_adcopy_view(request):
    if request.user.is_superuser and request.user.is_staff:
        main_adcopy = None
        similar_adcopies = None
        success = False
        reason = None
        initial = False
        if request.GET:
            creative_id = request.GET['adcopy_id']
            adcopy_list = AdcopyCreative.objects.filter(
                creative_id=creative_id)
            if not adcopy_list:
                reason = 'Adcopy Id {} Not found'.format(creative_id)
            elif len(adcopy_list) > 1:
                reason = 'Shit just got real.\nCall the captain.\nAbort....... Abort........'
            else:
                main_adcopy = adcopy_list[0]
                success = True
                hash_value = main_adcopy.hash_value
                if hash_value:
                    similar_adcopies = main_adcopy.get_group_members()
        elif request.POST:
            form = AdcopyEditForm(None, request.POST)
            if form.is_valid():
                data = {}
                form_data = request.POST
                for key, values in camp_utils._SAMPLE_ADCOPY_JSON.items():
                    if type(values) == str:
                        data[key] = form_data.get(key)
                    elif type(values) == dict:
                        for _key, _values in values.items():
                            if type(_values) == str:
                                if key in data:
                                    data[key][_key] = form_data.get(
                                        key + '_' + _key)
                                else:
                                    data[key] = {
                                        _key: form_data.get(key + '_' + _key)}
                            elif type(_values) == list:
                                i = 1
                                while i <= len(_values):
                                    if key in data:
                                        if _key in data[key]:
                                            data[key][_key].append(
                                                form_data[key + '_' + _key[:-1] + str(i)])
                                        else:
                                            data[key][_key] = [
                                                form_data[key + '_' + _key[:-1] + str(i)]]
                                    else:
                                        data[key] = {
                                            _key: [form_data[key + '_' + _key[:-1] + str(i)]]}
                                    i += 1
                # print data
                adcopies = [form_data.get('main_adcopy')]
                loop = 1
                while True:
                    creative_id = form_data.get('similar_adcopy_' + str(loop))
                    if creative_id:
                        adcopies.append(creative_id)
                        loop += 1
                    else:
                        break
                # print adcopies
                for creative_id in adcopies:
                    try:
                        creative = AdcopyCreative.objects.get(
                            creative_id=creative_id)
                    except:
                        pass
                    else:
                        creative.data = data
                        creative.save()
        else:
            initial = True
            total_adcopies = AdcopyCreative.objects.all().count()
            live_adcopies = AdcopyCreative.live_objects.all().count()
        return render_to_response('campaign/view_adcopy.html', RequestContext(request, {
            'initial': initial,
            'total_adcopies': total_adcopies if initial else 0,
            'live_adcopies': live_adcopies if initial else 0,
            'main_adcopy': main_adcopy,
            'similar_adcopies': similar_adcopies,
            'reason': reason,
            'success': success,
            'edit_form': AdcopyEditForm(creative_id=creative_id) if success else None,
        }))
    return HttpResponseRedirect('/')


@utils.profile_type_only()
def lp_list(request):
    lps = dict()
    module_name = (name for _, name, _ in list(pkgutil.iter_modules(landing_pages.__path__)))
    print module_name
    for name in module_name:
        lps[name] = []
        for lpc in import_module('.{}'.format(name), 'campaign.landing_pages').__all__:
            lps[name].append({
                'name': ' '.join(re.findall('[A-Z][^A-Z]*', lpc)),
                'slug': '-'.join(word.lower() for word in re.findall('[A-Z][^A-Z]*', lpc))
            })
    print lps
    return render(request, 'campaign/lp_list.html', {'lps': lps})
