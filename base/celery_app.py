from __future__ import absolute_import

import os

from billiard import einfo
from celery import Celery
from celery.signals import setup_logging, task_revoked

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')

from django.conf import settings


app = Celery('coverfox')

app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


class _Frame(einfo._Frame):
    f_back = None

    def __init__(self, frame):
        super(_Frame, self).__init__(frame)
        # self.f_locals = frame.f_locals


def patch_celery_traceback():
    einfo.Traceback.Frame = _Frame


@setup_logging.connect
def configure_logging(sender=None, **kwargs):
    from django.utils.log import configure_logging
    configure_logging(settings.LOGGING_CONFIG, settings.LOGGING)

    patch_celery_traceback()


@task_revoked.connect
def task_revoked_handler(sender=None, body=None, **kwargs):
    request = kwargs['request']
    if request.name == 'motor_product.prod_utils.get_premium':
        from motor_product.models import Insurer, IntegrationStatistics, Quote, RTOMaster, VehicleMaster
        request_data = request.args[1]
        vehicle_type = request.args[0]
        slug = request.args[2]
        integration_statistics = IntegrationStatistics(
                insurer=Insurer.objects.get(slug=slug),
                quote=Quote.objects.get(quote_id=request_data['quoteId']),
                vehicle_type=vehicle_type,
                rto_master=RTOMaster.objects.get(rto_code="-".join(request_data['registrationNumber[]'][0:2])),
                master_vehicle_id=request_data['vehicleId'],
                status=False,
                error_code=IntegrationStatistics.TASK_REVOKED,
                required_time=0
                )
        integration_statistics.save()
