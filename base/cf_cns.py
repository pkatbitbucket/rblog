import os
import csv
import copy
import hashlib
from pprint import pprint
import urllib
import urllib2
import time
from django.conf import settings as S
from django.template import Template, Context
from utils import UnicodeWriter
import mandrill
from poster.encode import multipart_encode
from poster.streaminghttp import register_openers
from celery_app import app
from core.models import Notification

# priority constants
PRIORITY_HIGH = 'high'
PRIORITY_NORMAL = 'normal'
PRIORITY_LOW = 'low'

NOTIFICATIONS = {
    "test": {
        "about": "This is a test notification",
        "to": ["techcore@coverfoxmail.com"],
        "subject": "notifications/test/mail_subject.txt",
        "subject_content": "",
        "html": "notifications/test/mail.html",
        "html_content": "",
        "text": "notifications/test/mail.txt",
        "text_content": "",
        "mta": "mandrill",  # send via (default mandrill)

        "numbers": [
            "9820715512",  # amitu
        ],
        "sms": "notifications/test/sms.txt",
        "sms_content": "text content {{ name }}",
    },
    "test_bulk": {
        "bulk": True,   # must be true for a bulk notification.
        "to_notify": ["hemant.negi@coverfoxmail.com"], # only for bulk = True
        "about": "This is a test bulk notification",
    },
    "502": {
        "about": "This is sent to developers when something goes wrong on server.",
        "to": ["techcore@coverfoxmail.com"],
        "subject": "notifications/502/mail_subject.txt",
        "html": "notifications/502/mail.html",
        "text": "notifications/502/mail.txt",
        "numbers": [
            "9820715512",  # amitu
        ],
        "template": "notifications/502/sms.txt",
    },

    "reset_password":{
        "about": "Email containing the link to reset password.",
        "subject": "notifications/reset_password/mail_subject.txt",
        "html": "notifications/reset_password/mail.html",
        "text": "notifications/reset_password/mail.txt",
        "sender_email": "no-reply@coverfox.com",
        "sender_name": "Coverfox",
    },

    "bulk_notification_status": {
        "about": "Email for the status of email and sms sent.",
        "to": S.ADMINS
    }
}


def notify(nid, **params):
    """
    Sends a notification (emails/sms)
    :param nid: notification id as in core.models.Notification
    :param these can be either defined in Notification model or passed as params. params will override
           the values from Notification model.
        to/bcc: str/tuple/list_of_(str/tuple) email address (Try to use tuple format if name is available)
            valid egs: 'hemant.frnz@gmail.com'
                       ('Hemant Negi', 'hemant.frnz@gmail.com')
                       ['email@address.1', 'email@address.2'..]
                       [('Hemant Negi', 'hemant.frnz@gmail.com'), ...]
        numbers : (str/list) phone numbers
        subject: template path if using a template file eg: "notifications/test/mail_subject.txt"
        subject_content: subject as plain text, can use django template tags.
        html: template path if using a template file eg: "notifications/test/mail.html",
        html_content: html content as plain text, can use django template tags.
        text: template path if using a template file eg: "notifications/test/mail.txt",
        text_content: email text as plain text, can use django template tags.
        mta: "mandrill"
        sms: template path if using a template file eg: "notifications/test/sms.txt"
        sms_content: plain text sms, can use django template tags.
        sender_name :  email from name defaults from settings.DEFAULT_FROM_NAME
        sender_email : email from email address default settings.DEFAULT_FROM_EMAIL
        reply_to : email header Reply-To default: header not set)
        attachments : [{'name': 'somename.vcf',
                        'type': 'text/vcard',
                        'content': base64.b64encode(vcard_content),
                      },...]
        images : [{'name': 'imagefile.jpg',
                   'type': 'image/jpeg',
                   'content': base64.b64encode(imagecontent),
                   },...]
        sync : (bool) default is False
        priority : PRIORITY_HIGH|PRIORITY_NORMAL(default)|PRIORITY_LOW

    RETURNS: (sync, status(<AsyncResult:obj>/ status dict)

    Example: notify('some_notificaion_id', to='email@address.com')
    """
    try:
        nobj = Notification.objects.get(nid=nid)
    except Notification.DoesNotExist:
        raise TypeError('nid "%s" does not exists! Create a new notification in core.models.Notification.' % nid)

    config = nobj.__dict__
    config.pop('_state')    # remove obsolete data

    # common context data or defaults
    ctx = {
        'site_url': S.SITE_URL,
        'sync': False,
        'priority': PRIORITY_NORMAL,
        'start_time': time.time(),
        'mandrill_template': 'base'  # default mandrill template
    }
    ctx.update(config)

    # sanitize user provided params
    if 'to' in params and isinstance(params['to'], (basestring, tuple)):
        params['to'] = [params['to']]
    if 'bcc' in params and isinstance(params['bcc'], (basestring, tuple)):
        params['bcc'] = [params['bcc']]
    if 'numbers' in params and isinstance(params['numbers'], basestring):
        params['numbers'] = [params['numbers']]

    if 'to' in ctx and isinstance(ctx['to'], list):
        ctx['to'] += params.pop('to', [])
    if 'bcc' in ctx and isinstance(ctx['bcc'], list):
        ctx['bcc'] += params.pop('bcc', [])
    if 'numbers' in ctx and isinstance(ctx['numbers'], list):
        ctx['numbers'] += params.pop('numbers', [])

    ctx.update(params)

    func_params = {}
    if ctx.get('to', None) or ctx.get('bcc', None):
        func_params.update(_send_email(ctx, nobj))

    if ctx.get('numbers', None):
        func_params.update(_send_sms(ctx, nobj))

    # call all senders at once
    status = None
    if func_params:
        status = _send_notification(func_params, ctx)
    return ctx['sync'], status


def notify_bulk(nid, who, why, args, to_notify=None, **params):
    """
    send bulk emails/sms
    args = [{number: '9212312354', to: 'email@address.com', ctx_var1:'', ctx_var2:''..}, ...]
    to_notify = ['emails@address.com',..] # emails to send the status of operation.
    params = all notify() params
    eg:
    notify_bulk('test_bulk', 'hemant.negi@coverfoxmail.com', 'testing',
                [{'number': '9897167064', 'to': 'email@address.com', 'name': 'Hemant Negi' }],
                sync=True, sms_content='Hello {{name}}, your number is  {{number}}')
    """
    if nid not in NOTIFICATIONS:
        raise TypeError('nid does not exists!')
    config = copy.deepcopy(NOTIFICATIONS[nid])

    if not config.get('bulk', False):
        raise TypeError('Not a bulk notification!')

    if to_notify is None: to_notify = []

    # common context data or defaults
    ctx = {
        'site_url': S.SITE_URL,
        'sync': False,
        'priority': PRIORITY_NORMAL,
        'start_time': time.time(),
        'bulk': True,
        'who': who,
        'why': why,
        'to_notify': to_notify + config.pop('to_notify', []),
        'args': args
    }
    ctx.update(config)
    ctx.update(params)

    func_params = {}
    # if ctx.get('to', None):
    #     func_params.update(_send_email(ctx))

    ctx['total_sms'] = len([_to for _to in args if 'number' in _to])
    if ctx['total_sms']:
        func_params.update(_send_sms(ctx, None, bulk=True))

    # call all senders at once
    if func_params:
        _send_notification(func_params, ctx)


def _send_email(ctx, nobj):
    """
    send email using the provided message transfer agent (MTA)
    """
    subject = nobj.get_subject(ctx=ctx)
    html = nobj.get_html(ctx=ctx)
    text = nobj.get_text(ctx=ctx)

    if not subject:
        raise Exception('Email subject is not provided')
    if not html:
        raise Exception('Email html(body) is not provided')

    mta = ctx.get("mta", S.DEFAULT_MTA)

    func_params = {}
    if mta == "mandrill":
        # mandrill does not support email address of format "name <email@gmail.com>"
        to = generate_mandrill_to(ctx.get('to', []))
        to += generate_mandrill_to(ctx.get('bcc', []), bcc=True)
        func_params['email_mandrill'] = _send_email_using_mandrill(to, subject, html, text=text, ctx=ctx)
    else:
        raise Exception("Unknown MTA: %s" % mta)

    return func_params


def generate_mandrill_to(emails, bcc=False):
    to = []
    for addr in emails:
        d = {'email': addr[1], 'name': addr[0]} if isinstance(addr, tuple) else {'email': addr}
        d['type'] = 'bcc' if bcc else 'to'
        to.append(d)
    return to


def _send_email_using_mandrill(to, subject, html, text="", ctx=None, use_mandrill_template=True, bulk=False):
    """
    method for emails. uses mandrill to send emails
    to = {'email': 'recipient.email@example.com', 'name': 'Recipient Name(optional)',
          'type': 'to/cc/bcc(optional default is to)'}
    for bulk = True:
        to = [{name:'',email:'',type:'to/cc/bcc', context={}},..]
    mandrill Ref: https://mandrillapp.com/api/docs/messages.python.html#method=send-template
    """
    if ctx is None: ctx = {}
    to = to if isinstance(to, list) else [to]
    template_content = []
    mandrill_params = ['attachments', 'images']

    message = {
        'text': text,     # it will generate text by itself if not provided or empty
        'subject': subject,
        'to': to,
        'from_email': ctx.get('sender_email', S.DEFAULT_FROM_EMAIL),
        'from_name': ctx.get('sender_name', S.DEFAULT_FROM_NAME),
        'metadata': {'website': S.SITE_NAME.lower()},
        'tags': [ctx['nid']],
        'preserve_recipients': None,
        'merge': True
    }

    if ctx.get('reply_to', None):
        message['headers'] = {'Reply-To': ctx['reply_to']}

    for param in mandrill_params:
        if param in ctx:
            message[param] = ctx[param]

    # bypass the template stored in mandrill account and use this html as it is.
    if use_mandrill_template:
        template_content = [{'content': html, 'name': 'mail-body'},
                            {'content': subject, 'name': 'mail-title'}
                            ]
    else:
        message['html'] = html

    if bulk:
        merge_vars = []
        for rec in to:
            context = rec.pop('context', [])
            merge_vars.append({
                'rcpt': rec['email'],
                'vars': [{'name': k, 'content': v} for k, v in context.iteritems()]
            })

        message['to'] = to
        message['merge_vars'] = merge_vars

    # return the params required to call mandrill_client.messages.send method
    func_params = {'template_name': ctx['mandrill_template'],
                   'template_content': template_content,
                   'message': message,
                   'ip_pool': 'Main Pool'}

    if S.NOTIFICATION_DEBUG:
        print '\nEmail sent to : %s' % ', '.join([e['email'] for e in to])
        print 'FUNCTION CALL PARAMS: '
        pprint(func_params)

    return func_params


def _send_sms(ctx, nobj, bulk=False):
    """
    Send sms
    """
    if bulk:
        to = []
        sms = ctx.get('sms_content', '')
        if not sms:
            sms = ctx.get('sms', '')

        if not sms:
            raise ValueError('sms message is not provided.')

        for rec in ctx['args']:
            if 'number' in rec:
                _ctx = copy.deepcopy(ctx)
                _ctx.pop('args', [])
                _ctx.update(rec)
                msg = Template(sms).render(Context(_ctx))
                to.append((rec['number'], msg))

        return {'sms_gupshup': _send_sms_using_gupsuhup(to, bulk=True)}
    else:
        sms = nobj.get_sms(ctx=ctx)

        if not sms:
            raise ValueError('sms message is not provided.')

        return {'sms_gupshup': _send_sms_using_gupsuhup(ctx['numbers'], sms)}


def _send_sms_using_gupsuhup(to, msg="", bulk=False):
    """
    Send sms to user via smsgupsuhup api (this method is not intended to use directly. use send_sms instead.)
    doc: http://enterprise.smsgupshup.com/help/in/EnterpriseAPIDocument.pdf
    :param to: list containing phone numbers (digits only)
    :param msg: (str) The message to send
                NOTE: You simply can not just send any message though this api.
                the message template must be uploaded to smsgupshup first.
    if bulk = True: # different msg can be sent to each recipient
        to = [(number,msg),...]
    :return: response from the api eg. (success | 917503426754 | 2942961268251219298-190416707054848245)
    """

    resp = []
    # for large set of numbers we will upload a csv file
    if len(to) > 10 or bulk:

        register_openers()    # set streaming header in urllib2
        at_one_time = 100000  # as per docs
        if bulk:
            zipped = to
        else:
            zipped = zip(to, [msg for i in range(len(to))])

        for bucket in [zipped[i:i + at_one_time] for i in range(0, len(zipped)) if i % at_one_time == 0]:
            csv.register_dialect('gupshup', delimiter=',', quoting=csv.QUOTE_ALL)
            filename = os.path.join("/tmp/", "%s.csv" % hashlib.md5(str(time.time())).hexdigest())
            file_stream = open(filename, 'wb')
            writer = UnicodeWriter(file_stream, dialect=csv.get_dialect('gupshup'))
            writer.writerow(["PHONE", "MESSAGE"])
            for i_to, i_msg in bucket:
                writer.writerow(map(str, [i_to, i_msg]))

            file_stream.close()

            datagen, headers = multipart_encode({
                "file": open(filename, 'rb'),
                'method': 'xlsUpload',
                'filetype': 'csv',
                'msg_type': 'text',
                'v': '1.1',
                'auth_scheme': 'PLAIN',
                'userid': S.GUPSHUP_API_USER,
                'password': S.GUPSHUP_API_PASSWORD,
                'mask': S.GUPSHUP_SMS_MASK
            })
            resp.append({'url': S.GUPSHUP_API_ENDPOINT, 'data': datagen, 'headers': headers})

    else:
        data = {
            'msg': msg,
            'send_to': ",".join(to),
            'v': '1.1',
            'userid': S.GUPSHUP_API_USER,
            'password': S.GUPSHUP_API_PASSWORD,
            'msg_type': 'text',
            'method': 'sendMessage',
            'mask': S.GUPSHUP_SMS_MASK,
        }
        querystring = urllib.urlencode(data)
        resp.append({'url': S.GUPSHUP_API_ENDPOINT, 'data': querystring})

    if S.NOTIFICATION_DEBUG:
        if bulk:
            for rec in to:
                print '%s : %s' % (rec[0], rec[1])
        else:
            print '\nSMS sent to : %s' % ', '.join(to)
            print 'SMS content : %s' % msg
        print 'FUNCTION CALL PARAMS: '
        pprint(resp)

    return resp


def _send_notification(func_params, ctx):
    """
    handles behaviour sync and priority
    """
    if S.NOTIFICATION_DEBUG:
        return 'Debug mode enabled. set NOTIFICATION_DEBUG=False'

    if not ctx['sync']:
        priority = ctx.get('priority', PRIORITY_NORMAL)
        if priority in (PRIORITY_NORMAL, PRIORITY_LOW):
            return _base_send_notification_async.apply_async((func_params, ctx), queue='notifications_normal')
        elif priority == PRIORITY_HIGH:
            return _base_send_notification_async.apply_async((func_params, ctx), queue='notifications_high')
        else:
            TypeError('Invalid value for priority!')

    else:
        return _base_send_notification(func_params, ctx)


@app.task
def _base_send_notification_async(func_params, ctx):
    """async notifications"""
    return _base_send_notification(func_params, ctx)


def _base_send_notification(func_params, ctx):
    """
    The final method to make calls to respective services.
    """
    status = {}

    if 'email_mandrill' in func_params:
        try:
            email_start_time = time.time()
            mandrill_client = mandrill.Mandrill(S.MANDRILL_API_KEY)
            status['mandrill_response'] = mandrill_client.messages.send_template(**func_params['email_mandrill'])
            status['email_time'] = time.time() - email_start_time
            status['email_mandrill'] = 'ok'
        except mandrill.Error, e:
            status['email_mandrill'] = 'error'
            status['email_mandrill_error'] = '%s - %s' % (e.__class__, e)

    if 'sms_gupshup' in func_params:
        try:
            sms_start_time = time.time()
            status['sms_response'] = []
            for params in func_params['sms_gupshup']:
                request = urllib2.Request(**params)
                sms_resp = urllib2.urlopen(request).read()
                status['sms_response'].append(sms_resp)
            status['sms_time'] = time.time() - sms_start_time
            status['sms_gupshup'] = 'ok'
        except Exception as ex:
            status['sms_gupshup'] = 'error'
            status['sms_gupshup_error'] = repr(ex)

    if ctx.get('bulk', False):
        _send_status_email(ctx, status)

    status['total_time'] = time.time() - ctx['start_time']
    print status
    return status
    # create_activity("send_notificaiton",)


def _send_status_email(ctx, status):
    """
    Send status emails to admins
    """
    subject = "Bulk sms operation %s sent" % ctx['total_sms']
    html = 'Status for bulk sms operation initiated by: {who} <br>reason: {why} <br>status: {status}'
    html = html.format(who=ctx['who'], why=ctx['why'], status=status)

    notify('bulk_notification_status', to=ctx.get('to_notify', []), subject_content=subject, html_content=html)
