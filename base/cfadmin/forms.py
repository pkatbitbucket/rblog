from django import forms
from motor_product import models as motor_models
from core.models import Email


class InspectionAgentForm(forms.ModelForm):
    weekly_off = forms.MultipleChoiceField(
        choices=motor_models.InspectionAgent.WEEKDAY_OPTIONS, required=False)
    first_name = forms.CharField(max_length=50, required=False)
    middle_name = forms.CharField(max_length=50, required=False)
    last_name = forms.CharField(max_length=50, required=False)
    email = forms.EmailField(max_length=255, required=True)

    class Meta:
        model = motor_models.InspectionAgent
        exclude = ('id', 'user', 'weekly_off')

    def __init__(self, *args, **kwargs):
        super(InspectionAgentForm, self).__init__(*args, **kwargs)
        self.fields['weekly_off'].initial = kwargs.get('initial', {}).get('weekly_off')
        if kwargs.get('instance', None) is not None:
            self.fields['email'].widget.attrs['readonly'] = True

    def clean_email(self):
        d = self.cleaned_data
        try:
            user_email = self.instance.user.email
        except:
            user_email = ''

        email_obj = Email.objects.filter(email=d.get('email')).exclude(email=user_email).first()
        user = email_obj.user
        if user:
            raise forms.ValidationError("A user with this email already exists")
        pass


class DealerDealershipMapForm(forms.ModelForm):

    class Meta:
        model = motor_models.DealerDealershipMap
        exclude = ['id', 'dealer', 'dealership']


class DealerForm(forms.ModelForm):

    class Meta:
        model = motor_models.Dealer
        exclude = ['id', 'company']


class DealershipForm(forms.ModelForm):

    class Meta:
        model = motor_models.Dealership
        exclude = ['id', 'company']
