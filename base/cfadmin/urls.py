from django.conf.urls import url

from cfadmin import views as v

urlpatterns = [
    url(r'^dashboard/', v.DashboardView.as_view(), name='cfadmin_dashboard'),
    url(r'^manage-agent/new/$', v.ManageSingleInspectionAgent.as_view(), name='manage_single_agent_new'),
    url(r'^manage-agent/(?P<agent_id>\d+)/$', v.ManageSingleInspectionAgent.as_view(), name='manage_single_agent'),
    url(r'^manage-agents/$', v.ManageInspectionAgent.as_view(), name='cfadmin-manage-agents'),
    url(r'^manage-dealer/(?P<type>dealer|dealership|dealerdealership)/(?P<dealer_id>\d+)/$',
        v.ManageSingleDealerView.as_view(), name='manage_single_dealer'),
    url(r'^manage-dealers/$', v.DealerManagementView.as_view(), name='cfadmin-manage-dealers'),
    url(r'^manage-invoice/$', v.DealerInvoiceView.as_view(), name='cfadmin-manage-invoice'),
    url(r'^manage-invoice/(?P<invoice_id>\d+)/$', v.DealerSingleInvoiceView.as_view(), name='cfadmin-single-invoice'),
]
