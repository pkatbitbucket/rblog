from django.views.generic.base import TemplateView
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from motor_product.models import InspectionAgent, Dealership, DealerDealershipMap, Dealer, DealerInvoice
from django.http import HttpResponseRedirect, Http404, JsonResponse
from django.contrib.auth.models import Permission, Group
# from django.shortcuts import get_object_or_404
# from django.views.generic.edit import FormView
from cfadmin.forms import InspectionAgentForm, DealerDealershipMapForm, DealerForm, DealershipForm
from core.models import Email, User
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import permission_required


class BaseCFAdminView(TemplateView):

    def validate_user_access(self, request):
        pass

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        self.validate_user_access(self.request)
        return super(BaseCFAdminView, self).dispatch(*args, **kwargs)


class DashboardView(BaseCFAdminView):
    template_name = 'cfadmin:dashboard.html'


class ManageInspectionAgent(BaseCFAdminView):
    template_name = "cfadmin:manage_inspection_agents.html"

    @method_decorator(permission_required('motor_product.change_inspectionagent', raise_exception=True))
    def validate_user_access(self, request):
        pass

    def get(self, request, *args, **kwargs):
        context = super(ManageInspectionAgent, self).get_context_data(**kwargs)
        context['inspection_agents'] = []
        # Excluded dummy inspection agent(for dealers)
        exclude_emails = ['dummyinspectionagent@coverfoxmail.com']
        email_obj = Email.objects.filter(email__in=exclude_emails)
        exclude_emails_user = [email.user for email in email_obj]
        for agent in InspectionAgent.objects.exclude(user__in=exclude_emails_user):
            context['inspection_agents'].append({
                "name": agent.user.get_full_name(),
                "email": agent.user.email,
                "areas": ', '.join([a.name for a in agent.area.all()]),
                "agent_id": agent.id,
            })
        return self.render_to_response(context)


class ManageSingleInspectionAgent(BaseCFAdminView):
    template_name = "cfadmin:manage_single_inspection_agent.html"

    @method_decorator(permission_required('motor_product.change_inspectionagent', raise_exception=True))
    def validate_user_access(self, request):
        pass

    def get_inspection_agent(self, agent_id):
        try:
            agent = InspectionAgent.objects.get(id=agent_id)
        except InspectionAgent.DoesNotExist:
            if agent_id in ['new', None]:
                agent = None
            else:
                raise Http404
        return agent

    def get_context_data(self, **kwargs):
        context = super(ManageSingleInspectionAgent, self).get_context_data(**kwargs)
        agent = self.get_inspection_agent(kwargs.get('agent_id'))
        # agent = get_object_or_404(InspectionAgent, id=kwargs.get('agent_id'))
        context['agent'] = agent
        initial_form_data = {}
        if agent:
            context['agent_inspections'] = agent.inspections.all().order_by("scheduled_on")
            initial_form_data["email"] = agent.user.email
            initial_form_data["first_name"] = agent.user.first_name
            initial_form_data["last_name"] = agent.user.last_name
            initial_form_data["middle_name"] = agent.user.middle_name
            initial_form_data['weekly_off'] = agent.weekly_off
        context['form'] = InspectionAgentForm(instance=agent, initial=initial_form_data)
        return context

    def post(self, request, *args, **kwargs):
        data = request.POST
        agent = self.get_inspection_agent(kwargs.get('agent_id'))
        form_instance = InspectionAgentForm(data, instance=agent)
        if form_instance.is_valid():
            clean_data = form_instance.cleaned_data
            if agent:
                user = agent.user
            else:
                user = User()
                # Check if first name provided. if provided set inverse of name as password.
                # If not set password as 'coverfox'
                password_key = clean_data['first_name'] or 'xofrevoc'
                user.set_password(password_key[::-1].lower())
            user.first_name = clean_data['first_name'] or data['first_name']
            user.last_name = clean_data['last_name'] or data['last_name']
            user.middle_name = clean_data['middle_name'] or data['middle_name']
            user.save()

            user.attach_email(
                email=clean_data['email'] or data['email'],
                is_exists=True,
                is_verified=False
            )

            agent_group, is_new_group = Group.objects.get_or_create(name="INSPECTION_AGENT")
            if is_new_group:
                agent_group.permissions.add(
                    Permission.objects.get_or_create(codename='change_pendinginspection')[0]
                )
            user.groups.add(agent_group)
            agent = form_instance.save(commit=False)
            agent.user = user
            agent.weekly_off = clean_data['weekly_off']
            if agent.pk:
                agent.area.clear()
            else:
                agent.save()
            for area in clean_data.get('area', []):
                agent.area.add(area)
            agent.save()
            '''
                The inspection agent should get an email with the link to the android application,
                randomly generated password and details of how to use etc.
            '''
            return HttpResponseRedirect("/cfadmin/manage-agent/{0}".format(agent.id))
        context = self.get_context_data(**kwargs)
        context['form'] = form_instance
        return self.render_to_response(context)


class DealerManagementView(BaseCFAdminView):
    template_name = "cfadmin:manage_dealer_agents.html"

    @method_decorator(permission_required('motor_product.change_dealer', raise_exception=True))
    def validate_user_access(self, request):
        pass

    def serialize_dealer(self, instance):
        return {
            "id": instance.id,
            "dealer_name": instance.company.name,
            "percentage": instance.commission_percentage,
            "payout_frequency": instance.payout_frequency,
            "is_pre_deducted": instance.is_pre_deducted,
            "is_approved": instance.is_approved,
        }

    def get(self, request, *args, **kwargs):
        context = super(DealerManagementView, self).get_context_data(**kwargs)
        if 'json' in request.GET.get('return', []):
            return_data = {'dealer_data': []}
            dealership_id = request.GET.get('dealership_id', 0)
            dealership = get_object_or_404(Dealership, id=dealership_id)
            return_data['dealership_data'] = self.serialize_dealer(dealership)
            dealership_dealers = dealership.dealership_dealers.all()
            return_data['dealership_data']['pending_approvals'] = dealership_dealers.filter(is_approved=False).count()
            for instance in dealership_dealers:
                return_data['dealer_data'].append({
                    "id": instance.id,
                    "dealer_name": instance.dealer.company.name,
                    "percentage": instance.commission_percentage,
                    "payout_frequency": instance.payout_frequency,
                    "is_pre_deducted": instance.is_pre_deducted,
                    "is_approved": instance.is_approved,
                })
            return JsonResponse(return_data)
        context['all_dealerships'] = Dealership.objects.all()
        context['total_pending_approval'] = DealerDealershipMap.objects.filter(
            is_approved=False).count()
        return self.render_to_response(context)


class ManageSingleDealerView(BaseCFAdminView):
    template_name = "cfadmin:manage_single_dealer.html"

    @method_decorator(permission_required('motor_product.change_dealer', raise_exception=True))
    def validate_user_access(self, request):
        pass

    def form_class(self, **kwargs):
        request_type = kwargs.get('type', None)
        if request_type == 'dealer':
            return DealerForm
        elif request_type == 'dealership':
            return DealershipForm
        elif request_type == 'dealerdealership':
            return DealerDealershipMapForm
        raise Http404

    def get_instance(self, **kwargs):
        request_type = kwargs.get('type', None)
        if request_type == 'dealer':
            instance = get_object_or_404(Dealer, id=kwargs['dealer_id'])
        elif request_type == 'dealership':
            instance = get_object_or_404(Dealership, id=kwargs['dealer_id'])
        elif request_type == 'dealerdealership':
            instance = get_object_or_404(DealerDealershipMap, id=kwargs['dealer_id'])
        return instance

    def get_context_data(self, **kwargs):
        context = super(ManageSingleDealerView, self).get_context_data(**kwargs)
        instance = self.get_instance(**kwargs)
        context['name'] = instance.get_name()
        context['form'] = self.form_class(**kwargs)(instance=instance)
        context['instance'] = instance
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        instance = context['instance']
        form_instance = self.form_class(**kwargs)(request.POST, instance=instance)
        context['form'] = form_instance
        if form_instance.is_valid():
            form_instance.save()
        return self.render_to_response(context)


class DealerInvoiceView(BaseCFAdminView):
    template_name = "cfadmin:dealer_invoice.html"

    @method_decorator(permission_required('motor_product.change_dealerinvoice', raise_exception=True))
    def validate_user_access(self, request):
        pass

    def get_context_data(self, **kwargs):
        context = super(DealerInvoiceView, self).get_context_data(**kwargs)
        context['invoices'] = DealerInvoice.objects.all()
        return context


class DealerSingleInvoiceView(BaseCFAdminView):
    template_name = "cfadmin:single_invoice.html"

    @method_decorator(permission_required('motor_product.change_dealerinvoice', raise_exception=True))
    def validate_user_access(self, request):
        pass

    def get_context_data(self, **kwargs):
        context = super(DealerSingleInvoiceView, self).get_context_data(**kwargs)
        context['invoice'] = get_object_or_404(DealerInvoice, id=kwargs.get('invoice_id'))
        return context

    def post(self, request, *args, **kwargs):
        invoice = get_object_or_404(DealerInvoice, id=kwargs.get('invoice_id'))
        invoice.cleared_on = timezone.now()
        invoice.raw['paid_amount'] = request.POST['paid_amount']
        invoice.raw['cf_cheque_no'] = request.POST['cf_cheque_no']
        invoice.save()
        return HttpResponseRedirect(reverse('cfadmin-single-invoice', kwargs={'invoice_id': kwargs['invoice_id']}))
