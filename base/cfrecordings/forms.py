from django import forms


class SearchForm(forms.Form):
    customer_mobile = forms.CharField(
        max_length=15, required=False,
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    caller_username = forms.CharField(
        max_length=20, required=False,
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    created_date_from = forms.CharField(
        widget=forms.TextInput(
            attrs={"data-format": "dd/MM/yyyy", 'class': 'form-control', 'readonly': True}
        ), required=False
    )
    created_date_to = forms.CharField(
        widget=forms.TextInput(
            attrs={"data-format": "dd/MM/yyyy", 'class': 'form-control', 'readonly': True}
        ), required=False
    )
