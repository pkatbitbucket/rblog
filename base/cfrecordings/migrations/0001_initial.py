# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0014_advisor_number'),
    ]

    operations = [
        migrations.CreateModel(
            name='Recording',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('caller', models.CharField(max_length=200)),
                ('call_time', models.DateTimeField()),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('phone', models.CharField(max_length=20)),
                ('filename', models.CharField(max_length=255)),
                ('rec_id', models.UUIDField(default=uuid.uuid4, editable=False)),
                ('advisor', models.ForeignKey(to='crm.Advisor')),
            ],
        ),
    ]
