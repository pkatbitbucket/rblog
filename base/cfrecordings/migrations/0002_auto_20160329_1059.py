# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cfrecordings', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='recording',
            name='size',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='recording',
            name='advisor',
            field=models.ForeignKey(to='crm.Advisor', null=True),
        ),
    ]
