import uuid
import datetime
from django.db import models
import logging

s3_key_parsing_logger = logging.getLogger('s3_key_parsing_logger')


class RecordingManager(models.Manager):

    def parse_recname_and_create(self, keyname, bucket_name, extra={}):
        if keyname.endswith('.mp3'):
            if self.model.objects.filter(filename__endswith=keyname):
                pass
            else:
                try:
                    _, mobile, caller, dated = keyname[:-4].split('/')[-1].split("_")
                    call_time = datetime.datetime.strptime(dated, "%Y-%m-%d-%H-%M-%S")
                    filename = "https://%s.s3.amazonaws.com/%s" % (bucket_name, keyname)

                    rec = self.model(
                        phone=mobile[-10:],
                        caller=caller,
                        call_time=call_time,
                        filename=filename,
                    )
                    if extra.get('keysize'):
                        rec.size = extra['keysize']
                    rec.save()
                    return rec
                except ValueError as e:
                    s3_key_parsing_logger.info(
                        "Parsing error for key with name %s came with error message as ---%s---" % (keyname, e)
                    )
                    raise ValueError(e)
        else:
            s3_key_parsing_logger.info("Key %s is not a mp3 file with keysize %s" % (keyname, extra.get('keysize', '')))


class Recording(models.Model):
    caller = models.CharField(max_length=200)
    advisor = models.ForeignKey('crm.Advisor', null=True)
    call_time = models.DateTimeField()
    created_on = models.DateTimeField(auto_now_add=True)
    phone = models.CharField(max_length=20)
    filename = models.CharField(max_length=255)
    rec_id = models.UUIDField(default=uuid.uuid4, editable=False)
    size = models.IntegerField(null=True, blank=True)

    objects = RecordingManager()
