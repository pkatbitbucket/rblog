import os
import django
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'  # noqa
django.setup()  # noqa


import datetime
from django.db.models import Q
from cfrecordings import models as cfr_models


class RecordingSearch(object):
    def __init__(self, search_data):
        self.data = search_data

    def search_recording_id(self, q):
        if self.data.get('lead_id'):
            q = q & Q(id=self.data['recording_id'])
        return q

    def search_caller_username(self, q):
        if self.data.get('caller_username'):
            q = q & Q(caller=self.data['caller_username'])
        return q

    def search_customer_mobile(self, q):
        if self.data.get('customer_mobile'):
            mobile_query = Q(phone__iendswith=self.data['customer_mobile'])
            q = q & mobile_query
        return q

    def search_created_date_from(self, q):
        if self.data.get('created_date_from'):
            created_date_from = datetime.datetime.strptime(
                self.data['created_date_from'], "%d/%m/%Y"
            )
            q = q & Q(call_time__gte=created_date_from)
        return q

    def search_created_date_to(self, q):
        if self.data.get('created_date_to'):
            created_date_to = datetime.datetime.strptime(
                self.data['created_date_to'], "%d/%m/%Y"
            )
            q = q & Q(call_time__lte=created_date_to)
        return q


def recording_search(search_data, q=Q()):
    res = []
    recording_searcher = RecordingSearch(search_data)

    for key in search_data.iterkeys():
        recording_dispatcher = getattr(recording_searcher, 'search_%s'
                                       % key, False)
        if recording_dispatcher:
            q = recording_dispatcher(q)

    if q and len(q):
        res = cfr_models.Recording.objects.filter(q).distinct()
    return res
