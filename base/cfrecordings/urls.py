from django.conf.urls import patterns, url
import views as cviews

urlpatterns = patterns(
    'cfrecordings.views',
    url(r'^analyst/dashboard/$', cviews.analyst_dashboard, name='analyst_dashboard'),
    url(r'^manager/dashboard/$', cviews.manager_dashboard, name='manager_dashboard'),
    url(r'^caller/dashboard/$', cviews.caller_dashboard, name='caller_dashboard'),
    url(r'^ajax/play-recording/$', cviews.play_recording, name='download'),
    url(r'^ajax/dashboard/search/$', view=cviews.RecordingListView.as_view(), name='search_recordings'),
    url(r'^sns/message-receive/$', cviews.sns_message_receive, name='sns_message_receive'),
)
