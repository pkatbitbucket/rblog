import json
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.db.models import Q

import search_utils
from cfutils.aws import s3_upload
from . import models as cfr_models
from . import forms as cfr_forms
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.views.generic.list import ListView


@login_required
def analyst_dashboard(request):
    search_form = cfr_forms.SearchForm()
    return render(request, "search_recordings.html", {
        'search_form': search_form,
    })


@login_required
def manager_dashboard(request):
    search_form = cfr_forms.SearchForm()
    return render(request, "search_recordings.html", {
        'search_form': search_form,
    })


@login_required
def caller_dashboard(request):
    search_form = cfr_forms.SearchForm()
    return render(request, "search_recordings.html", {
        'search_form': search_form,
    })


@login_required
def play_recording(request):
    rec_id = request.GET['rec_id']
    rec = cfr_models.Recording.objects.get(rec_id=rec_id)
    s3_obj = s3_upload.S3Upload(
        bucket_name=settings.CFOXRECORDING_BUCKET_NAME,
        aws_access_key=settings.CFOXRECORDING_AWS_ACCESS_KEY,
        aws_secret_key=settings.CFOXRECORDING_AWS_SECRET_KEY
    )
    record_url = s3_obj.url(rec.filename)
    response = HttpResponse()
    response['Content-Type'] = "audio/mpeg"
    response['X-Accel-Redirect'] = "/cfoxrecording-s3-redirect/%s" % record_url.replace(
        "https://cfoxrecording.s3.amazonaws.com/", ""
    )
    return response


@csrf_exempt
@require_POST
def sns_message_receive(request):
    record = json.loads(json.loads(request.body)['Message'])['Records'][0]
    if record['eventName'] == 'ObjectCreated:Put':
        extra = {}
        bucket_name = record['s3']['bucket']['name']
        obj = record['s3']['object']
        keyname = obj['key']
        extra['keysize'] = obj['size']
        rec = cfr_models.Recording.objects.parse_recname_and_create(keyname, bucket_name, extra=extra)
    else:
        raise ValueError("eventName should be 'ObjectCreated:Put', the complete message from SNS is %s" % json.dumps(request.POST))

    if rec:
        return JsonResponse({'rec_id': rec.id, 'success': True})
    else:
        return JsonResponse({'success': False})


class RecordingListView(ListView):
    model = cfr_models.Recording
    context_object_name = 'recordings'
    paginate_by = 10
    template_name = "recording_results.html"

    def get_context_data(self, **kwargs):
        context = super(RecordingListView, self).get_context_data(**kwargs)
        paginator = self.get_paginator(self.get_queryset(), self.paginate_by)
        context['n'] = xrange(1, paginator.num_pages + 1)
        page_no = 1
        if self.request.GET.get('page'):
            page_no = self.request.GET.get('page')
        context['page'] = paginator.page(page_no)
        return context

    def get_queryset(self):
        q = Q()
        if 'view_all_recordings' in self.request.user.user_permissions.values_list('codename', flat=True):
            q = Q(advisor__user=self.request.user)
        search_form = cfr_forms.SearchForm(self.request.GET)
        if search_form.is_valid():
            return search_utils.recording_search(self.request.GET, q)
        else:
            return JsonResponse({'success': False, 'errors': search_form.errors})
