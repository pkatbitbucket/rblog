from django.conf import settings
import mailchimp


def get_list_index():
    try:
        m = mailchimp.Mailchimp(settings.MAILCHIMP_API_KEY)
        lists = m.lists.list()
    except mailchimp.Error, e:
        print 'An error occurred: %s - %s' % (e.__class__, e)
    if lists:
        data = lists['data']
    else:
        data = []
    return data


def get_list_members(list_id):
    try:
        m = mailchimp.Mailchimp(settings.MAILCHIMP_API_KEY)
        lists = m.lists.list({'list_id': list_id})
        lists = lists['data'][0]
        members = m.lists.members(list_id)['data']
    except mailchimp.ListDoesNotExistError:
        print "The list does not exist"
    except mailchimp.Error, e:
        print 'An error occurred: %s - %s' % (e.__class__, e)
    return members


def list_subscribe(list_id, sub_email):
    try:
        m = mailchimp.Mailchimp(settings.MAILCHIMP_API_KEY)
        m.lists.subscribe(list_id, {'email': sub_email})
        print "The email has been successfully subscribed"
    except mailchimp.ListAlreadySubscribedError:
        print "That email is already subscribed to the list"
    except mailchimp.Error, e:
        print 'An error occurred: %s - %s' % (e.__class__, e)
    return


def list_unsubscribe(list_id, sub_email):
    try:
        m = mailchimp.Mailchimp(settings.MAILCHIMP_API_KEY)
        m.lists.unsubscribe(list_id, {'email': sub_email})
        print "The email has been successfully unsubscribed"
    except mailchimp.Email_NotExists:
        print "That email is does not exist in the list"
    except mailchimp.Error, e:
        print 'An error occurred: %s - %s' % (e.__class__, e)
    return


def list_batch_subscribe(list_id, batch):
    """
    batch = [
        {
            'email':{'email':sf.email},
            'merge_vars':{
                'FNAME': sf.name,
                'groupings': [
                    {
                        'name' : 'Blog Groupings',
                        'groups' : ['Monday'],
                    },
                ]
            }
        },
        ]
    # list id = a666be07eb
    """
    try:
        m = mailchimp.Mailchimp(settings.MAILCHIMP_API_KEY)
        res = m.lists.batch_subscribe(list_id, batch, double_optin=False)
        print "The batch is successfully subscribed"
        return res
    except mailchimp.Error, e:
        print 'An error occurred: %s - %s' % (e.__class__, e)
    return 'Error'
