from django.contrib import admin
from django.contrib.contenttypes.admin import GenericStackedInline

from django_mptt_admin.admin import DjangoMpttAdmin

from core.models import Review

from .common.admin import AbstractBaseAdmin, BaseAuthorAdmin
from .forms import RelatedItemAdminForm
from .models import Page, Layout, Fragment, Category, Upload, RelatedItem, RelatedItemFilter


class CategoryAdmin(BaseAuthorAdmin, DjangoMpttAdmin):
    pass


class FragmentInline(AbstractBaseAdmin, admin.TabularInline):
    model = Fragment
    extra = 1


class ReviewInline(AbstractBaseAdmin, GenericStackedInline):
    model = Review
    extra = 1


class UploadAdmin(AbstractBaseAdmin, GenericStackedInline):
    model = Upload


class RelatedItemFilterInline(AbstractBaseAdmin, admin.TabularInline):
    model = RelatedItemFilter
    extra = 1


class RelatedItemAdmin(BaseAuthorAdmin):
    form = RelatedItemAdminForm
    inlines = (RelatedItemFilterInline, )


class LayoutAdmin(AbstractBaseAdmin, admin.ModelAdmin):
    pass


class PageAdmin(BaseAuthorAdmin):
    inlines = (FragmentInline, UploadAdmin, ReviewInline)


admin.site.register(Page, PageAdmin)
admin.site.register(Layout, LayoutAdmin)
admin.site.register(Fragment)
admin.site.register(RelatedItem, RelatedItemAdmin)
admin.site.register(Category, CategoryAdmin)
