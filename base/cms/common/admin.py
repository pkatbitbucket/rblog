from django.contrib import admin

from import_export.admin import ImportExportModelAdmin


class AbstractBaseAdmin(object):
    exclude = ('editor', 'creator', )


class BaseAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    save_as = True

    def __init__(self, *args, **kwargs):
        super(BaseAdmin, self).__init__(*args, **kwargs)
        m2m_fields = self.model._meta.many_to_many
        self.filter_horizontal = tuple(set(
            list(self.filter_horizontal) + [field.name for field in m2m_fields]))

    def save_model(self, request, obj, form, change):
        try:
            if not change:
                obj.creator = request.user
            obj.editor = request.user
        except AttributeError:
            pass
        return super(BaseAdmin, self).save_model(request, obj, form, change)

    class Media:
        js = [
            'grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            'grappelli/tinymce_setup/tinymce_setup.js',
        ]


class BaseAuthorAdmin(AbstractBaseAdmin, BaseAdmin):
    pass
