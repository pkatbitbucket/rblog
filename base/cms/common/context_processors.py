from django.conf import settings


def config_settings(request):
    return {'settings': settings}
