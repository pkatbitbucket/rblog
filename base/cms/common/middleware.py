from django.contrib.contenttypes.models import ContentType
from django.contrib.flatpages.views import flatpage
from django.http import Http404
from django.conf import settings
from django.utils.module_loading import import_by_path

from .views import Http404RedirectView


class Handle404Middleware(object):

    def process_response(self, request, response):
        if response.status_code != 404:
            # No need to check for a flatpage for non-404 responses.
            return response
        if settings.DEBUG:
            raise
        else:
            return Http404RedirectView.as_view()(request)
