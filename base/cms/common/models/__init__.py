from django.db import models
from core.models import GenericBaseModel


class UserGenericBaseModel(GenericBaseModel):
    user = models.ForeignKey('core.User')

    class Meta:
        abstract = True
        unique_together = ("user", "content_type", 'object_id')

    def __unicode__(self):
        return '%s - %s' % (self.user, self.content_object, )
