from django.core.serializers import serialize
from django.db.models import Avg
from django.db.models.query import QuerySet
from django.utils.safestring import mark_safe
from django.template import Library
from django.contrib.admin.templatetags.admin_list import mark_safe
from django.contrib.humanize.templatetags.humanize import intcomma as django_intcomma

from coverfox.serializers import DjangoQuerysetJSONEncoder

import json

register = Library()

# FILTERS


@register.filter
def dump_json(data):
    if isinstance(data, QuerySet):
        return mark_safe(serialize('json', data))
    return mark_safe(json.dumps(data, cls=DjangoQuerysetJSONEncoder))


@register.filter
def get_attribute(obj, attribute):
    return obj.get_attribute(attribute).values_list('value', flat=True)


@register.filter(is_safe=True)
def intcomma(value, use_l10n=True):
    return django_intcomma(value, use_l10n)


@register.filter
def subtract(value, arg):
    return value - arg


@register.filter('xrange')
def get_range(stop, start=None):
    return xrange(*filter(bool, [start, stop]))


@register.filter
def get(obj, attribute):
    try:
        return obj.__getitem__(attribute)
    except AttributeError:
        try:
            return obj.__getattribute__(attribute)
        except AttributeError:
            return obj.__getattr__(attribute)


@register.filter
def exists(obj, arg):
    return arg in obj if obj else False


@register.filter
def is_descendant_of(obj, arg):
    try:
        return obj.is_descendant_of(obj._meta.model.objects.get(slug=arg))
    except:
        return False


@register.filter
def average(queryset, field):
    return queryset.aggregate(Avg(field)).pop('%s__avg' % (field, ))
