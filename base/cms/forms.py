from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import ValidationError
from django.forms import widgets
import django

from .models import Layout, Page, RelatedItem


class PageCreateForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(PageCreateForm, self).__init__(*args, **kwargs)
        self.fields['url'].required = False

    layout = forms.ModelChoiceField(queryset=Layout.objects.all(
    ), to_field_name='slug', widget=forms.HiddenInput())

    class Meta:
        model = Page
        fields = ['title', 'url', 'layout', ]


class RelatedItemAdminForm(forms.ModelForm):

    object_ids = forms.ModelMultipleChoiceField(
        required=False, queryset=None, widget=FilteredSelectMultiple('Items', False))
    select_all = forms.BooleanField(required=False)

    def __init__(self, data=None, *args, **kwargs):
        super(RelatedItemAdminForm, self).__init__(data, *args, **kwargs)
        try:
            self.initial['select_all'] = self.instance.object_ids == '0'
            self.initial['object_ids'] = self.instance.object_ids.split(',')
            self.fields[
                'object_ids'].queryset = self.instance.content_type.model_class().objects.all()
        except ObjectDoesNotExist:
            self.fields[
                'content_type'].initial = content_type = ContentType.objects.get_for_model(Layout)
            self.fields[
                'object_ids'].queryset = content_type.model_class().objects.all()

    def clean(self):
        select_all = self.cleaned_data.get('select_all', False)
        if select_all:
            self.cleaned_data['object_ids'] = '0'
        else:
            content_type = self.cleaned_data['content_type']
            if self.initial and content_type != self.fields['content_type'].to_python(self.initial.get('content_type')):
                self.cleaned_data['object_ids'] = ''
                return self.cleaned_data

            model = content_type.model_class()
            objects = self.cleaned_data.get('object_ids', model.objects.none())
            try:
                assert objects.model is model
            except AssertionError:
                self.cleaned_data['object_ids'] = ''
            else:
                object_ids = objects.values_list('pk', flat=True)
                self.cleaned_data['object_ids'] = ','.join(
                    map(str, object_ids))
        return self.cleaned_data

    class Meta:
        model = RelatedItem
        if django.VERSION > (1, 7):
            fields = "__all__"
