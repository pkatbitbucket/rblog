from ast import literal_eval

from django.contrib.contenttypes.fields import (
    GenericForeignKey, GenericRelation
)


class GenericForeignKeys(GenericForeignKey):

    def __get__(self, instance, instance_type=None):
        if instance is None:
            return self

        try:
            return getattr(instance, self.cache_attr)
        except AttributeError:
            rel_objs = self.model.objects.none()
            f = self.model._meta.get_field(self.ct_field)
            ct_id = getattr(instance, f.get_attname(), None)
            if ct_id:
                ct = self.get_content_type(id=ct_id, using=instance._state.db)
                pks = literal_eval(getattr(instance, self.fk_field))
                pks = pks if hasattr(pks, '__iter__') else [pks]
                rel_objs = ct.get_all_objects_for_this_type()
                if filter(bool, pks):
                    rel_objs = rel_objs.filter(pk__in=pks)
            setattr(instance, self.cache_attr, rel_objs)
            return rel_objs


class GenericRelations(GenericRelation):

    def contribute_to_class(self, cls, name):
        super(GenericRelations, self).contribute_to_class(cls, name)
        setattr(cls, self.name, ReverseGenericRelatedObjectsDescriptor(self))
