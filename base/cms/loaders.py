from django.template import TemplateDoesNotExist
from django.template.loaders import app_directories

from cms.models import Layout

try:
    # Django >= 1.8
    get_app_template_dirs = app_directories.get_app_template_dirs
    app_template_dirs = get_app_template_dirs('templates')
except AttributeError:
    # Django <= 1.7
    app_template_dirs = app_directories.app_template_dirs

BaseAppLoader = app_directories.Loader


class CMSTemplateLayoutLoader(BaseAppLoader):

    def load_template_source(self, template_name, template_dirs=None):
        prefix = 'cms:'
        if template_name.startswith(prefix):
            layout_slug = template_name[len(prefix):]
            try:
                return (Layout.objects.get(slug=layout_slug).content, '')
            except Layout.DoesNotExist:
                raise TemplateDoesNotExist(template_name)
            except AttributeError:
                raise
        else:
            return super(CMSTemplateLayoutLoader, self).load_template_source(template_name, template_dirs)
