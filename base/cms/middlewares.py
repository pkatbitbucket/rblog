from django.conf import settings
from django.core.urlresolvers import resolve
from django.http import Http404


class CMSPageFallbackMiddleware(object):

    def process_response(self, request, response):
        #import ipdb;ipdb.set_trace()
        # print "in CMS Middleware"
        if response.status_code != 404:
            # No need to check for a flatpage for non-404 responses.
            return response
        try:
            import imp
            from cms import urls as cms_urls
            urls = imp.new_module("urls")
            urls.urlpatterns = cms_urls.page_urlpatterns
            resolver_match = resolve(request.path_info, urls)

            func, args, kwargs = resolver_match.func, resolver_match.args, resolver_match.kwargs
            return func(request, *args, **kwargs).render()
        except Http404:
            return response
        except Exception:
            if settings.DEBUG:
                raise
            return response
