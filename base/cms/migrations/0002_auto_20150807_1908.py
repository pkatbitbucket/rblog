# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('cms', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='upload',
            name='creator',
            field=models.ForeignKey(related_name='cms_upload_created',
                                    blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='upload',
            name='editor',
            field=models.ForeignKey(related_name='cms_upload_last_modified',
                                    blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='relateditemfilter',
            name='creator',
            field=models.ForeignKey(related_name='cms_relateditemfilter_created',
                                    blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='relateditemfilter',
            name='editor',
            field=models.ForeignKey(related_name='cms_relateditemfilter_last_modified',
                                    blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='relateditemfilter',
            name='related_item',
            field=models.ForeignKey(
                related_name='filters', to='cms.RelatedItem'),
        ),
        migrations.AddField(
            model_name='relateditem',
            name='content_type',
            field=models.ForeignKey(to='contenttypes.ContentType'),
        ),
        migrations.AddField(
            model_name='relateditem',
            name='creator',
            field=models.ForeignKey(related_name='cms_relateditem_created',
                                    blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='relateditem',
            name='editor',
            field=models.ForeignKey(related_name='cms_relateditem_last_modified',
                                    blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='page',
            name='category',
            field=mptt.fields.TreeForeignKey(
                blank=True, to='cms.Category', null=True),
        ),
        migrations.AddField(
            model_name='page',
            name='creator',
            field=models.ForeignKey(
                related_name='cms_page_created', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='page',
            name='editor',
            field=models.ForeignKey(related_name='cms_page_last_modified',
                                    blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='page',
            name='layout',
            field=models.ForeignKey(to='cms.Layout'),
        ),
        migrations.AddField(
            model_name='page',
            name='related_items',
            field=models.ManyToManyField(to='cms.RelatedItem', blank=True),
        ),
        migrations.AddField(
            model_name='layout',
            name='creator',
            field=models.ForeignKey(related_name='cms_layout_created',
                                    blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='layout',
            name='editor',
            field=models.ForeignKey(related_name='cms_layout_last_modified',
                                    blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='fragment',
            name='creator',
            field=models.ForeignKey(related_name='cms_fragment_created',
                                    blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='fragment',
            name='editor',
            field=models.ForeignKey(related_name='cms_fragment_last_modified',
                                    blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='fragment',
            name='page',
            field=models.ForeignKey(related_name='fragments', to='cms.Page'),
        ),
        migrations.AddField(
            model_name='category',
            name='creator',
            field=models.ForeignKey(related_name='cms_category_created',
                                    blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='category',
            name='editor',
            field=models.ForeignKey(related_name='cms_category_last_modified',
                                    blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='category',
            name='parent',
            field=mptt.fields.TreeForeignKey(
                related_name='children', default=None, blank=True, to='cms.Category', null=True),
        ),
        migrations.AlterUniqueTogether(
            name='page',
            unique_together=set([('url', 'category')]),
        ),
        migrations.AlterUniqueTogether(
            name='fragment',
            unique_together=set([('page', 'name')]),
        ),
    ]
