from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.db import models

from autoslug import AutoSlugField
from autoslug.utils import slugify
from core.models import BaseAuthorModel
from mptt.models import MPTTModel, TreeForeignKey

from .common.validators import validate_template_syntax
from .generic import GenericForeignKeys


class Editor(BaseAuthorModel):
    name = models.CharField(max_length=50, help_text=("e.g: 'Hospital list'"))
    content = models.TextField(blank=True, validators=[
                               validate_template_syntax])

    class Meta:
        abstract = True


class Layout(Editor):
    slug = AutoSlugField(populate_from='name', always_update=True, unique=True)
    description = models.TextField(
        blank=True, help_text='Description of this template.')


class Category(MPTTModel, BaseAuthorModel):
    name = models.CharField(max_length=50)
    slug = AutoSlugField(populate_from='name',
                         always_update=True, unique_with='parent')
    parent = TreeForeignKey('self', null=True, blank=True,
                            related_name='children', default=None)

    @property
    def url(self, delimiter='/'):
        ancestors_n_me = list(self.get_ancestors().values_list(
            'slug', flat=True)) + [self.slug]
        return delimiter.join(ancestors_n_me)

    class Meta:
        verbose_name_plural = 'categories'


def get_upload_path(instance, filename):
    return '/'.join([instance.content_type.app_label, instance.content_type.model, getattr(instance.content_object, 'slug', instance.content_object.__unicode__()), filename])


class Upload(BaseAuthorModel):
    file = models.FileField(upload_to=get_upload_path)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')


class RelatedItem(BaseAuthorModel):
    name = models.CharField(max_length=50, help_text=("e.g: 'Hospital list'"))
    content_type = models.ForeignKey(ContentType)
    object_ids = models.CommaSeparatedIntegerField(max_length=255)
    content_objects = GenericForeignKeys('content_type', 'object_ids')

    @property
    def queryset(self):
        return self.content_objects.filter(**dict(self.filters.values_list('filter', 'value')))

    def __unicode__(self):
        return '%s - %s' % (self.name, self.content_type.__unicode__(), )


class RelatedItemFilter(BaseAuthorModel):
    filter = models.CharField(max_length=100)
    value = models.CharField(max_length=50)
    related_item = models.ForeignKey('RelatedItem', related_name='filters')

    def __unicode__(self):
        return '%s = %s' % (self.filter, self.value)


class Page(BaseAuthorModel):
    title = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    category = TreeForeignKey(Category, blank=True, null=True)
    layout = models.ForeignKey(Layout)
    related_items = models.ManyToManyField(RelatedItem, blank=True)
    reviews = GenericRelation('core.Review')

    def save(self, **kwargs):
        self.url = slugify(self.url or self.title)
        return super(Page, self).save(**kwargs)

    def get_absolute_url(self):
        return reverse('cms:page:change', args=((self.category.url, ) if self.category else ()) + (self.url, ))

    class Meta:
        unique_together = ("url", "category")


class Fragment(Editor):
    page = models.ForeignKey(Page, related_name='fragments')

    class Meta:
        unique_together = ("page", "name")
