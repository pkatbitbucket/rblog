from django import template
from django.template import TemplateSyntaxError

from ..models import Fragment

register = template.Library()


@register.inclusion_tag('editable_tag.html', takes_context=True)
def editable(context, fragment_name, notoolbar=False, page=None):
    page = page or context.get('page', None)
    if not page:
        raise TemplateSyntaxError(
            'No page object defined for "%s" editable fragment' % (fragment_name, ))
    else:
        try:
            return {
                'fragment': Fragment.objects.get(page=page, name=fragment_name),
                'toolbar': ['false', 'true'][not notoolbar],
                'request': context['request'],
            }
        except Fragment.DoesNotExist:
            return {}
