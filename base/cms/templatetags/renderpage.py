from django import template
from django.template import TemplateSyntaxError
from django.template.loader_tags import IncludeNode
from django.utils import six

from ..models import Fragment

register = template.Library()


class RenderPageNode(IncludeNode):

    def __init__(self, token, *args, **kwargs):
        self.page_var = template.Variable(token.split_contents()[1])
        super(RenderPageNode, self).__init__(None, *args, **kwargs)

    def render(self, context):
        page = self.page_var.resolve(context)
        context['fragments'] = dict(
            page.fragments.values_list('name', 'content'))
        # print "testing.. render.."
        context['related_items'] = {}
        for item in page.related_items.all():
            context['related_items'][item.name] = item
        context['page'] = page
        #context['ancestors'] = page.category.get_ancestors(include_self=True)
        breadcrumb = []
        breadcrumb.append({'url': "/", 'title': "Home"})
        url = ""
        if page.category:
            for a in page.category.get_ancestors(include_self=True):
                print a.url
                url = "/" + a.url + "/"
                breadcrumb.append(
                    {'url': url, 'title': deslugify_and_capitalize(a.name)})
        breadcrumb.append({'url': url + page.url + "/",
                           'title': deslugify_and_capitalize(page.url)})
        context['breadcrumb'] = breadcrumb

        self.template = template.Template(page.layout.content)
        if not self.template:
            return ''
        values = dict([(name, var.resolve(context)) for name, var
                       in six.iteritems(self.extra_context)])
        if self.isolated_context:
            return self.template.render(context.new(values))
        context.update(values)
        output = self.template.render(context)
        context.pop()
        return output


@register.tag('renderpage')
def do_renderpage(parser, token):
    return RenderPageNode(token, extra_context={}, isolated_context=False)


def deslugify_and_capitalize(value):
    """Converts 'first_name' to 'First name'"""
    if not value:
        return ''
    return " ".join([x.capitalize() for x in value.replace("-", " ").split(" ")])
