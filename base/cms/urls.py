from django.conf.urls import url, include

from . import views


layout_urlpatterns = [
    url(r'^$', views.LayoutListView.as_view(), name='list'),
]

fragment_urlpatterns = [
    url(r'^(?P<page_url>[\w\-]*)/$', views.FragmentUpdateView.as_view(), name='change'),
]

page_urlpatterns = [
    url(r'^(?P<layout_slug>[\w\-]*)/add/$', views.PageCreateView.as_view(), name='add'),
    url(r'^(?P<url>[\w\-]*)/$', views.PageUpdateView.as_view(), name='change'),
    url(r'^(?P<categories>[\w\-\/]*)/(?P<url>[\w\-]*)/$', views.PageUpdateView.as_view(), name='change'),
    url(r'^(?P<url>[\w\-]*)/delete/$', views.PageDeleteView.as_view(), name='delete'),
]

upload_urlpatterns = [
    url(r'^(?P<page_url>[\w\-]*)/$', views.FileCreateView.as_view(), name='file'),
]

urlpatterns = [
    url(r'^layout/', include(layout_urlpatterns, namespace='layout')),
    url(r'^page/', include(page_urlpatterns, namespace='page')),
    url(r'^fragment/', include(fragment_urlpatterns, namespace='fragment')),
    url(r'^upload/', include(upload_urlpatterns, namespace='upload')),
]
