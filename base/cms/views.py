from django.forms import models as model_forms
from django.conf import settings
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from braces import views as braces_views
from models import Layout, Page, Fragment, Upload, Category

from . import forms
from common import views as common_views


class LayoutListView(common_views.BaseView, ListView):
    model = Layout


class PageCreateView(common_views.BaseView, CreateView):
    model = Page
    form_class = forms.PageCreateForm

    def get_initial(self):
        initial = super(PageCreateView, self).get_initial()
        initial.update({
            'layout': self.kwargs.get('layout_slug'),
        })
        return initial


class PageUpdateView(common_views.BaseView, UpdateView):
    model = Page
    slug_field = 'url'
    slug_url_kwarg = 'url'
    fields = ()

    def get_queryset(self):
        queryset = super(self.__class__, self).get_queryset()
        category_slugs = filter(bool, self.kwargs.get(
            'categories', '').split('/'))[::-1]
        filters = {}
        while category_slugs:
            category_slug = category_slugs.pop()
            filters['category' + '__parent' *
                    len(category_slugs) + '__slug'] = category_slug
        if filters:
            return queryset.filter(**filters)
        else:
            return queryset.filter(category=None)


class PageDeleteView(common_views.BaseView, DeleteView):
    model = Page
    slug_field = 'url'
    slug_url_kwarg = 'url'
    success_url = reverse_lazy('cms:layout:list')


class FragmentUpdateView(common_views.BaseView, UpdateView):
    model = Fragment

    def get_object(self):
        return self.model.objects.get_or_create(page__url=self.kwargs.get('page_url'), name=self.request.POST.get('name'))


class FileCreateView(common_views.BaseView, braces_views.JSONResponseMixin, CreateView):
    model = Upload

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(FileCreateView, self).dispatch(*args, **kwargs)

    def get_form_class(self):
        return model_forms.modelform_factory(self.model, fields=['file'])

    def form_invalid(self, form):
        return self.render_json_response({
            'error': True,
            'message': form.errors,
        })

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.content_object = Page.objects.get(
            url=self.kwargs.get('page_url'))
        form.save()
        return self.render_json_response({
            'filelink': settings.STATIC_URL + self.object.file.name,
            'filename': self.request.FILES['file'].name,
        })
