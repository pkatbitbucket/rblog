from django.conf import settings
from django.http import HttpRequest
from django.utils import timezone
from django.db import transaction as django_transaction
from django.core.mail import send_mail

import datetime as dt
import json
import logging
import logging.handlers
import re
import thread
import traceback
import cStringIO

from operator import itemgetter

import utils
from core import models as core_models
from core.requirement_engine import requirement_engine

# this log is used to send data to redshift.
activity_logger = logging.getLogger('activity_logs')
activity_logger.setLevel(logging.DEBUG)
LOG_FORMAT = (
    '%(asctime)s %(ip_address)s %(server)s %(user_id)s '
    '%(visitor_id)s %(request_id)s %(app)s %(activity_id)s %(activity_type)s '
    '%(browser)s [[%(data1)s]] ((%(data2)s)) %(json)s'
)

logger = logging.getLogger(__name__)
extlogger = logging.getLogger('extensive')

HEALTH_QUOTE_VIEWED = "health_quote_viewed"
CAR_QUOTE_VIEWED = "car_quote_viewed"
SENT_SMS = "sms_sent"
SENT_EMAIL = "email_sent"
SUCCESS = "success"
VIEWED = "viewed"
FAILED = 'failed'
INSPECTION_STATUS_CHANGED = 'inspection_status_changed'

HTTP_PORT_REGEX = re.compile(r'^[0-9]+$')
DATE_TIME_FORMAT = "%d/%b/%Y:%H:%M:%S +0000"
_request_undefined = object()


def assert_mail(subject, data=None):
    tb = cStringIO.StringIO()
    traceback.print_stack(file=tb)
    mesg = "%s ---------- \n --------- %s" % (tb, unicode(data))
    thread.start_new_thread(send_mail, (subject, mesg, "error@coverfoxmail.com", [
        'ranedk@gmail.com', 'upadhyay@gmail.com', 'sanketrathi08@gmail.com', 'gulshang1992@gmail.com'
    ]))


class _Formatter(logging.Formatter):
    converter = dt.datetime.fromtimestamp

    def formatTime(self, record, datefmt=None):
        ct = self.converter(record.created)
        ct = utils.convert_datetime_to_timezone(ct)

        if datefmt:
            s = ct.strftime(datefmt)
        else:
            t = ct.strftime("%Y-%m-%d %H:%M:%S")
            s = "%s,%03d" % (t, record.msecs)
        return s


def clean_ip(ip_addr):
    splitted_ip_addr = ip_addr.split(":")[0]
    return splitted_ip_addr


def format_time(ct, datefmt=None):
    ct = utils.convert_datetime_to_timezone(ct)

    if datefmt:
        ct = ct.strftime(datefmt)

    return ct


def get_network_params(request):
    user_agent = request.user_agent
    _os = user_agent.os.family + user_agent.os.version_string if user_agent else ''
    device = user_agent.device.family if user_agent else ''
    ip_address = request.META.get("REMOTE_ADDR", "")
    browser = '{} {}'.format(user_agent.browser.family,
                             user_agent.browser.version_string
                             ).strip()[:100] if user_agent else ''
    return _os, device, ip_address, browser


def get_referer_and_url(request):
    url = request.build_absolute_uri()
    referer = request.META.get("HTTP_REFERER", "")
    if request.is_ajax():
        url = referer or url
        referer = None

    return referer, url


def has_mobile_or_email(data):
    if data.get('mobile'):
        return True
    elif data.get('email'):
        return True
    else:
        return False


def get_user_contact_info(form_data):
    for form_name, data in form_data.iteritems():
        if form_name == "core.forms.Person":
            if not isinstance(data, list):
                data = [data]
            for d in data:
                if "role" in data:
                    if data["role"] in ["self", "proposal"]:
                        return has_mobile_or_email(d), d
                else:
                    return has_mobile_or_email(d), d
    return False, {}


def get_user(user):
    if not user:
        return None

    if hasattr(user, "is_anonymous"):
        if user.is_anonymous():
            return None

    if isinstance(user, str) or isinstance(user, unicode) or \
            isinstance(user, int) or isinstance(user, long):
        user = core_models.User.objects.filter(pk=user).first()
        if not user:
            raise Exception("Invalid user")

    if isinstance(user, core_models.User):
        return user

    return None


def get_or_create_user_for_contact_info(**kwargs):

    data = {
        'email': kwargs.get("email", ""),
        'mobile': kwargs.get("mobile", ""),
    }
    user_data = {
        'first_name': kwargs.get("first_name", ""),
        'last_name': kwargs.get("last_name", ""),
        'gender': kwargs.get("gender", None),
        'birth_date': kwargs.get("birth_date", None),
    }

    user_email = None
    user_phone = None

    if data.get('mobile'):
        user_phone = core_models.User.objects.filter(phones__number=data.get('mobile')).last()
    if data.get('email'):
        user_email = core_models.User.objects.filter(emails__email=data.get('email')).last()

    if user_phone or user_email:
        if user_phone:
            email, _ = core_models.Email.objects.get_or_create(email=data.get('email'), user=user_phone)
            if email.visitor:
                assert email.visitor.user == user_phone
                email.visitor = None
                email.save()
            return user_phone, user_phone.phones.last(), email
        else:
            phone, _ = core_models.Phone.objects.get_or_create(phone=data.get('mobile'), user=user_email)
            if phone.visitor:
                assert phone.visitor.user == user_email
                phone.visitor = None
                phone.save()
            return user_email, phone, user_email.emails.last()

    phone = None
    email = None

    user = core_models.User.objects.create(**user_data)
    if data.get("email"):
        email = user.attach_email(data.get('email'), is_verified=False)
    if data.get("mobile"):
        phone = user.attach_phone(data.get('mobile'), is_verified=False)

    return user, phone, email


def request_to_dict(request=None):
    if not request:
        request = utils.get_request()

    if isinstance(request, HttpRequest):
        # Get params from request.
        _os, device, ip_addr, browser = get_network_params(request)
        city_details = utils.ip2location(ip_addr, dictionary=True)
        referer, url = get_referer_and_url(request)
        activity_data = {
            "user": request.user,
            "os": _os,
            "device": device,
            "ip_address": ip_addr,
            "browser": browser,
            "referer": referer,
            "url": url,
            "landing_url": request.COOKIES.get('landing_page_url', '') or url,
            "mid_id": request.MID.id,
            "tracker": request.TRACKER,
            "city": utils.get_property(city_details, 'city'),
            "state": utils.get_property(city_details, 'state'),
            "isp": utils.get_property(city_details, 'isp'),
            "IS_INTERNAL": request.IS_INTERNAL,
        }

        if request.TRACKER:
            marketing_data = request.TRACKER.extra.get('marketing_data') or {}
        else:
            marketing_data = {}

        for key, field_name in core_models.Activity.ACTIVITY_PARAM_MAPPING.items():
            value = marketing_data.get(key)
            if value:
                activity_data[field_name] = value
    else:
        raise Exception("Request instance should be of request type")

    return activity_data


def override_query_params(activity_data, params):
    if params.get("ip"):
        activity_data["ip_address"] = params["ip"]

    if params.get("user"):
        activity_data["user"] = params["user"]

    if params.get("visitor"):
        activity_data["tracker"] = params["visitor"]

    if params.get("agent"):
        activity_data["agent"] = params["agent"]

    if params.get("user_id"):
        # Get user.
        activity_data["user"] = core_models.User.objects.filter(pk=params.get("user_id")).first()

    if params.get("visitor_id"):
        visitor_id = params.get("visitor_id")
        activity_data["tracker"] = core_models.Tracker.objects.filter(pk=visitor_id).first()

    if params.get("_os"):
        activity_data["os"] = params.get("_os")

    if params.get("device"):
        activity_data["device"] = params.get("device")

    if params.get("browser"):
        activity_data["browser"] = params.get("browser")

    if params.get("referer"):
        activity_data["referer"] = params.get("referer")

    if params.get("mid_id"):
        activity_data["mid_id"] = params.get("mid_id")

    marketing_params = params.get("marketing_params", {})
    if marketing_params:
        if not isinstance(params.get("marketing_params"), dict):
            raise Exception("Marketing parameters should be of dict type.")

        activity_data = dict(activity_data, **marketing_params)

    return activity_data


def get_marketing_param(request):
    if isinstance(request, HttpRequest):
        return utils.get_property(request, 'session.marketing_data') or {}
    elif isinstance(request, dict):
        request_dict = request
        marketing_params = request_dict.get("marketing_params", {})
        if not marketing_params and request_dict.get("tracker"):
            marketing_params = utils.get_property(
                request.get('tracker').extra, 'marketing_data'
            ) or {}

        new_marketing_params = {}
        if marketing_params:
            for key, val in marketing_params.items():
                field = core_models.Marketing.MID_MAPPING.get(key)
                if field:
                    new_marketing_params[field] = val

        return new_marketing_params
    else:
        raise Exception("Invalid request")


def validate_visitor(visitor):
    if not visitor:
        return None

    if (
        isinstance(visitor, basestring)
        or isinstance(visitor, (int, long))
    ):
        visitor = core_models.Tracker.objects.filter(pk=visitor).first()

    if isinstance(visitor, core_models.Tracker):
        return visitor

    return visitor


def clean_dict(json_data):
    """
    Remove json keys with None values.
    :return:
    """
    return dict(filter(itemgetter(1), json_data.items()))


def validate_user(user):
    if (
        isinstance(user, basestring)
        or isinstance(user, (int, long))
    ):
        user = core_models.User.objects.filter(pk=user).first()

    if not user:
        return None

    if not isinstance(user, core_models.User):
        return None

    if user.is_anonymous():
        return None

    return user


def validate_fd(data):
    if not data:
        return

    for key, val in data.iteritems():
        if "." not in key:
            raise Exception("Data schema is invalid.")

        if not isinstance(val, dict) and not isinstance(val, list):
            raise Exception("Data schema is invalid.")


def create_activity(
    activity_type, product, page_id, form_name,
    form_position, form_variant, data1="",
    data2="", data={}, request_dict=None,
    user=None, visitor=None, extra=None,
    requirement=None
):
    validate_fd(data)

    if not request_dict:
        request_dict = request_to_dict()

    try:
        with django_transaction.atomic():
            return _create_activity(
                activity_type=activity_type,
                product=product,
                page_id=page_id,
                form_name=form_name,
                form_position=form_position,
                form_variant=form_variant,
                data1=data1,
                data2=data2,
                data=data,
                request_dict=request_dict,
                user=user,
                visitor=visitor,
                extra=extra,
                requirement=requirement,
            )
    except Exception, e:
        utils.log_error(
            logger=logger,
            request=request_dict,
            msg='Error in requirement engine %s' % str(e)
        )


def log_activity(log_msg):
    activity_logger.info(log_msg)


def _create_activity(
    activity_type, product, page_id, form_name,
    form_position, form_variant, data1="-",
    data2="-", data={}, request_dict=None,
    user=None, visitor=None, extra=None,
    requirement=None
):
    activity_data = request_dict

    user = validate_user(user or activity_data.get("user"))
    visitor = validate_visitor(visitor or activity_data.get("tracker"))

    if not visitor and not user:
        raise Exception("User and visitor are required")

    # requirement can be passed either directly or via cookie.
    # we will remove following lines as soon as we have
    # requirement in all URL implemented.
    if not requirement and request_dict.get("requirement_id"):
        rid = request_dict.get('requirement_id')
        requirement = core_models.Requirement.objects.get(id=rid)
    elif requirement and isinstance(requirement, (int, long, basestring)):
        requirement = core_models.Requirement.objects.get(id=requirement)

    # if the logged in user is advisor, we attribute activity to the
    # requirement.user instead of to user.
    advisor = None
    advisor_tracker = None

    if user and user.advisor.exists():
        # we keep them aside for logging
        advisor = user
        advisor_tracker = visitor

        if requirement:
            if requirement.kind.product != product:
                err_msg = 'Product miss-match. Requirement(%s) product(%s) and activity product(%s) are not same'
                logger.error(err_msg % (requirement.pk, requirement.kind.product, product))
                return

            user = requirement.user
            visitor = None
            if user:
                visitor = user.tracker_set.last()
            else:
                visitor = requirement.visitor
        else:
            return

    # this condition(visitor/tracker doesn't exist) is reached logically
    # only when user is advisor (checked in condition above)
    if not visitor:
        if user:
            visitor = user.tracker_set.last()
        elif requirement and requirement.visitor:
            visitor = requirement.visitor
        else:
            raise Exception("User or visitor are required")

    # TODO: Not the right way, needs to be cleaned properly
    if 'IS_INTERNAL' in activity_data:
        del activity_data['IS_INTERNAL']

    activity_data["user"] = user
    activity_data["tracker"] = visitor
    activity_data["page_id"] = page_id
    activity_data["form_name"] = form_name
    activity_data["form_position"] = form_position
    activity_data["product"] = product
    activity_data["form_variant"] = form_variant
    activity_data["activity_type"] = activity_type

    activity_data["ip_address"] = clean_ip(activity_data["ip_address"])
    activity_data['extra'] = extra or {}

    new_activity_data = activity_data
    new_activity_data.pop('marketing_params', None)
    activity = core_models.Activity.objects.create(**new_activity_data)
    if activity.fd:
        activity_data['data'] = activity.fd.get_data()
    else:
        activity_data['data'] = {}

    activity.set_data(
        data,
        user=activity_data['user'],
        tracker=activity_data['tracker']
    )

    activity_data["form_data"] = json.dumps(data)

    marketing_param = get_marketing_param(request_dict)
    activity_data.update(marketing_param)

    asctime = format_time(activity.created, datefmt=DATE_TIME_FORMAT)
    activity_data.update({
        'data1': data1,
        'data2': data2,
        "visitor_id": utils.get_property(visitor, 'id'),
        "tracker_id": utils.get_property(visitor, 'id'),
        "session_key": utils.get_property(visitor, 'session_key'),
        "user_id": utils.get_property(user, 'id'),
        "asctime": asctime,
        "server": settings.SERVER_NAME,
        "app": settings.APP,
        "advisor": advisor,
        "advisor_tracker": advisor_tracker,
        "request_id": None,
        "browser": activity_data.get("browser", "-").replace(' ', '-'),
        "activity_id": activity.id,
    })

    vehicle_make = None
    vehicle_model = None
    vehicle_variant = None
    vehicle_type = None
    if data:
        from motor_product import models as motor_models
        if 'core.forms.CarDetail' in data or 'core.forms.BikeDetail' in data:
            if 'core.forms.CarDetail' in data:
                form_data = data['core.forms.CarDetail']
            elif 'core.forms.BikeDetail' in data:
                form_data = data['core.forms.BikeDetail']
            else:
                form_data = {}

            vehicle_master_id = form_data.get('vehicle_master')
            vehicle_master_instance = None

            if vehicle_master_id:
                vehicle_master_instance = motor_models.VehicleMaster.objects.filter(pk=vehicle_master_id).first()

            if vehicle_master_instance:
                vehicle_make = vehicle_master_instance.make.name
                vehicle_model = vehicle_master_instance.model.name
                vehicle_variant = vehicle_master_instance.variant
                vehicle_type = vehicle_master_instance.vehicle_type

    activity_data['requirement_id'] = activity.requirement_id
    if activity.requirement_id:
        activity_data['requirement_created_date'] = activity.requirement.created_date.strftime(DATE_TIME_FORMAT)

    activity_data['vehicle_make'] = vehicle_make
    activity_data['vehicle_model'] = vehicle_model
    activity_data['vehicle_variant'] = vehicle_variant
    activity_data['vehicle_type'] = vehicle_type
    activity_json_data = activity_data.copy()
    activity_json_data['form_data'] = data
    activity_json_data.pop('user', '')
    activity_json_data.pop('tracker', '')
    activity_json_data['device_model'] = activity.device_model
    activity_json_data['feed_item'] = activity.feed_item
    activity_json_data['gclid'] = activity.gclid
    activity_json_data['label'] = activity.label
    activity_json_data['match_type'] = activity.match_type
    activity_json_data['placement'] = activity.placement
    activity_json_data['target'] = activity.target
    activity_json_data['term'] = activity.term
    activity_json_data['term_category'] = activity.term_category
    activity_json_data['placement'] = activity.placement

    requirement_engine(activity=activity, requirement=requirement)
    activity_json_data['requirement_id'] = activity.requirement_id
    requirement_created_at = utils.get_property(
        activity, 'requirement.created_date'
    )
    if requirement_created_at:
        activity_json_data['requirement_created_at'] = requirement_created_at.strftime(DATE_TIME_FORMAT)

    log_format_data = {}
    log_format_data['activity_type'] = activity.activity_type
    log_format_data['product'] = activity.product
    log_format_data['page_id'] = activity.page_id
    log_format_data['form_name'] = activity.form_name
    log_format_data['form_position'] = activity.form_position
    log_format_data['form_variant'] = activity.form_variant
    log_format_data['visitor'] = activity_json_data['visitor_id']
    log_format_data['user'] = activity_json_data['user_id']
    log_format_data['ip_address'] = activity_json_data['ip_address']
    log_format_data['activity_at'] = activity_json_data['asctime']
    clean_json_data = clean_dict(activity_json_data)
    log_format_data['json'] = json.dumps(clean_json_data)

    activity_data['json'] = json.dumps(activity_json_data)
    LOG_FORMAT = " %(activity_type)s %(product)s %(page_id)s "
    LOG_FORMAT += "%(form_name)s %(form_position)s %(form_variant)s "
    LOG_FORMAT += "%(visitor)s %(user)s %(ip_address)s %(activity_at)s "
    LOG_FORMAT += "%(json)s"
    LOG_FORMAT = LOG_FORMAT.replace(' ', '^^^')
    log_str = LOG_FORMAT % log_format_data

    log_activity(log_str)
    return activity


def create_crm_activity(data, activity=None):
    """
    create_crm_activity() is to be used to mark every crm activity on our system.
    This is for analytics purpose, as well as can be used as a cross system
    signal feature, where based on events some action can be performed, eg extra
    mail or sms being sent.

    This function should be called with a dictionary as the first parameter
    which should have the following structure.
    data = {
        'user': <crm.Advisor object>,
        'category': 'OUTGOING_CALL',
        'start_time': None,
        'end_time': None,
        'primary_task': <crm.Task object>,
        'tasks_disposed': [<crm.Task object>]
        'disposition': <crm.Disposition object>,
        'sub_disposition': <crm.SubDisposition object>,
        'data': {mobile': '9920130681', 'product': 'car', 'did': '2011'},
    }
    if activity is passed, this function will update it, otherwise a new activity
    will be created
    """
    from crm import models as crm_models

    extlogger.info(
        (
            "Create CRM activity with activity=%s | user=%s | category=%s | "
            "start_time=%s | end_time=%s | primary_task=%s | disposition=%s | "
            "sub_disposition=%s | mobile=%s | product=%s | did=%s"
        ) % (
            activity.id if activity else '',
            data['user'].id if data.get('user') else '',
            data.get('category', ''),
            data['start_time'].strftime('%d/%m/%Y %H:%M:%S') if data.get('start_time') else '',
            data['end_time'].strftime('%d/%m/%Y %H:%M:%S') if data.get('end_time') else '',
            data['primary_task'].id if data.get('primary_task') else '',
            data.get('disposition', ''),
            data.get('sub_disposition', ''),
            data['data'].get('mobile', '') if data.get('data') else '',
            data['data'].get('product', '') if data.get('data') else '',
            data['data'].get('did', '') if data.get('data') else '',
        )
    )

    if not activity:
        # Check for already open activity on this and give it back
        if data.get('primary_task'):
            undisposed_activity = crm_models.Activity.objects.filter(
                primary_task=data['primary_task'],
                end_time__isnull=True,
            ).order_by('-start_time')
            if undisposed_activity:
                activity = undisposed_activity[0]
                extlogger.info(
                    "%s undisposed activities found, using "
                    "(activity=%s advisor=%s) with (primary_task=%s advisor=%s)" % (
                        undisposed_activity.count(),
                        activity.id,
                        activity.user.id,
                        data['primary_task'].id,
                        data['primary_task'].assigned_to.id if data['primary_task'].assigned_to else ''
                    )
                )
                # if activity.category == 'INCOMING_CALL':
                activity.user = data['primary_task'].assigned_to
                # else:
                # assert activity.user == data['primary_task'].assigned_to
                activity.save()
                return activity

        # If no activity on the task exists then create new activity
        start_time = data.get('start_time') or timezone.now()
        activity = crm_models.Activity.objects.create(
            user=data.get('user'),
            category=data.get('category'),
            start_time=start_time,
            end_time=data.get('end_time'),
            primary_task=data.get('primary_task'),
            disposition=data.get('disposition'),
            sub_disposition=data.get('sub_disposition'),
            data=data.get('data')
        )
        extlogger.info("Created new activity=%s" % (activity.id))

        if data.get('requirement'):
            req = data.get('requirement')
            extlogger.info("Adding activity=%s to requirement=%s" % (activity.id, req.id))
            req.crm_activities.add(activity)
        elif activity.primary_task:
            extlogger.info("Adding activity=%s to requirements of primary_task=%s" % (activity.id, activity.primary_task.id))
            activity.requirements.add(*activity.primary_task.requirements.all())
        else:
            # If primary task or requirement, both don't exist, raise exception
            extlogger.info("No requirement or primary_task for activity=%s" % activity.id)
    else:
        activity.disposition = data.get('disposition')
        activity.sub_disposition = data.get('sub_disposition')
        activity.data = data.get('data')
        activity.end_time = data.get('end_time')
        activity.save()
    # create or update crm activity
    requirement_engine(crm_activity=activity)
    return activity


def payment_success(transaction):
    fd = transaction.get_form_data()
    return create_activity(
        activity_type=SUCCESS,
        product=transaction.transaction_type,
        page_id='payment',
        data=fd,
        user=transaction.tracker.user,
        form_name='',
        form_position='',
        form_variant='',
    )
