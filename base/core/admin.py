from django import forms
from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin, UserAdmin as DjangoUsrAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.models import Group

from import_export.admin import ExportMixin

from . import models as core_models
from . import forms as core_forms

import json


class AbstractBaseAdmin(object):
    exclude = ('editor', 'creator', )


class BaseAdmin(ExportMixin, admin.ModelAdmin):

    def __init__(self, *args, **kwargs):
        super(BaseAdmin, self).__init__(*args, **kwargs)
        m2m_fields = self.model._meta.many_to_many
        self.filter_horizontal = set(
            list(self.filter_horizontal) + [field.name for field in m2m_fields])
        raw_id_fields_models = (core_models.Tracker, core_models.User, core_models.Activity)
        for field in self.model._meta.fields:
            if not field.editable:
                self.readonly_fields += (field.name, )
            elif getattr(field, 'related', None) and (field.related.model in raw_id_fields_models):
                self.raw_id_fields += (field.name, )

    def save_model(self, request, obj, form, change):
        try:
            if not change:
                obj.creator = request.user
            obj.editor = request.user
        except AttributeError:
            pass
        return super(BaseAdmin, self).save_model(request, obj, form, change)

    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
        ]


class BaseAuthorAdmin(AbstractBaseAdmin, BaseAdmin):
    pass


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(
        label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = core_models.User
        fields = ('first_name',)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = core_models.User
        fields = "__all__"

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class UserAdmin(DjangoUsrAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    search_fields = ('emails__email',)
    list_display = ('first_name', 'last_name', 'is_superuser', 'is_staff')
    list_filter = ('is_superuser', 'is_staff')
    search_fields = ('first_name', 'last_name', 'emails__email')
    fieldsets = (
        (None, {'fields': ('password',)}),
        ('Personal info', {'fields': ('first_name', 'last_name')}),
        ('Permissions', {'fields': ('is_active', 'is_superuser', 'is_staff', 'groups', 'user_permissions')}),
        ('Important dates', {'fields': ('last_login',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('first_name', 'last_name', 'password1', 'password2')}
         ),
    )
    ordering = ('id',)
    filter_horizontal = ()


class UserInline(admin.StackedInline):
    model = core_models.User.groups.through
    raw_id_fields = ('user',)
    extra = 1


class CustomGroupAdmin(GroupAdmin):
    inlines = [UserInline]


class NotificationAdmin(admin.ModelAdmin):

    form = core_forms.NotificationForm
    save_as = True

    def to_emails(self, obj):
        return ', '.join(obj.to)
    to_emails.short_description = "Default To"

    def bcc_emails(self, obj):
        return ', '.join(obj.bcc)
    bcc_emails.short_description = "BCC"

    def to_numbers(self, obj):
        return ', '.join(obj.numbers)
    to_numbers.short_description = "Numbers"

    def used_vars(self, obj):
        return ', '.join(obj.get_all_vars())
    used_vars.short_description = "Used Variables"

    list_display = ('nid', 'about', 'to_emails', 'bcc_emails', 'to_numbers', 'sms_content',
                    'get_sender', 'priority', 'sync')
    fieldsets = (
        (None, {'fields': ('nid', 'about', ('priority', 'sync'))}),
        ('Email', {'fields': ('to', 'bcc', 'subject', 'subject_content', 'html', 'html_content',
                              'text', 'text_content', ('sender_name', 'sender_email'),
                              'reply_to', ('mta', 'is_template'))}),
        ('SMS', {'fields': ('numbers', 'sms', 'sms_content')}),
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super(NotificationAdmin, self).get_form(request, obj, **kwargs)
        if obj:
            obj.to = ', '.join([str(o) for o in obj.to])
            obj.numbers = ', '.join([str(o) for o in obj.numbers])
            obj.bcc = ', '.join([str(o) for o in obj.bcc])
        return form

    def save_model(self, request, obj, form, change):
        """
        Given a model instance save it to the database.
        """
        obj.save()


class RequirementForm(forms.ModelForm):
    fd = forms.Field(required=False, widget=forms.widgets.Textarea(), label='Form Data')

    class Meta:
        model = core_models.Requirement
        fields = ('user', 'kind', 'fd', 'last_web_activity', 'last_crm_activity', 'mid')

    def __init__(self, *args, **kwargs):
        super(RequirementForm, self).__init__(*args, **kwargs)
        fd_data = self.get_form_data()
        self.initial.update({'fd': json.dumps(fd_data, indent=4)})
        self.fields['fd'].widget.attrs['readonly'] = True
        self.fields['fd'].widget.attrs['rows'] = sum(len(v) for v in fd_data.values()) + len(fd_data) + 2

    def get_form_data(self):
        if self.instance.fd:
            return self.instance.fd.get_data()
        return {}


class RequirementAdmin(BaseAdmin):
    list_display = ('__unicode__', 'user', 'visitor', 'form_data', 'last_web_activity', 'last_crm_activity', 'mid')
    raw_id_fields = ('user', 'policy', 'last_web_activity', 'last_crm_activity', 'mid')
    readonly_fields = ('current_state', 'last_web_activity', 'last_crm_activity', 'mid')
    search_fields = ('fd__sections__data__mobile', 'fd__sections__data__email')
    list_filter = ('mid',)

    form = RequirementForm

    def form_data(self, instance):
        if instance.fd:
            return instance.fd.get_data()


admin.site.register(core_models.User, UserAdmin)
admin.site.unregister(Group)
admin.site.register(Group, CustomGroupAdmin)
admin.site.register(core_models.Tracker, BaseAdmin)
admin.site.register(core_models.Activity, BaseAdmin)
admin.site.register(core_models.Employee, BaseAdmin)
admin.site.register(core_models.FormSection, BaseAdmin)
admin.site.register(core_models.Insurer, BaseAdmin)
admin.site.register(core_models.FormData, BaseAdmin)
admin.site.register(core_models.RequirementKind, BaseAdmin)
admin.site.register(core_models.Requirement, RequirementAdmin)
admin.site.register(core_models.Notification, NotificationAdmin)
