from django.core.cache import cache

def cacheable(cache_key, timeout=3600):
    """ Usage:
    class SomeClass(models.Model):
        # fields [id, name etc]
        @cacheable("SomeClass_get_some_result_%(id)s")
        def get_some_result(self):
            # do some heavy calculations
            return heavy_calculations()
        @cacheable("SomeClass_get_something_else_%(name)s")
        def get_something_else(self):
            return something_else_calculator(self)
    """
    def paramed_decorator(func):
        def decorated(self):
            # Update key __dict__ for cache key dict.
            func(self, update_key_values=True)
            key = cache_key.format(**self.__dict__)
            
            if cache.has_key(key):
                return cache.get(key)
            
            res = func(self)
            cache.set(key, res, timeout)
            return res
        
        decorated.__dict__ = func.__dict__
        decorated.__doc__ = func.__doc__
        
        return decorated
     
    return paramed_decorator
