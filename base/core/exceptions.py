class NewTrackerException(Exception):
    message = 'New activity created for current tracker'


class InvalidFormData(Exception):
    message = 'Invalid form data'


class ReadOnlyException(Exception):
    message = 'Object is not writable'
