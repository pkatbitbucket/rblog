from django import forms
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

import django

if django.VERSION < (1, 7):
    try:
        from south.modelsinspector import add_introspection_rules
        add_introspection_rules([], ["^core\.fields\.IntegerRangeField"])
        add_introspection_rules([], ["^core\.fields\.SmallIntegerRangeField"])
    except ImportError:
        pass


class SmallIntegerRangeField(models.SmallIntegerField):

    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        validators = kwargs['validators'] = kwargs.get('validators', [])
        if isinstance(max_value, int):
            validators.append(MaxValueValidator(max_value))
        if isinstance(min_value, int):
            validators.append(MinValueValidator(min_value))
        models.SmallIntegerField.__init__(
            self,
            verbose_name=verbose_name,
            name=name,
            **kwargs
        )

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value': self.max_value}
        defaults.update(kwargs)
        return super(SmallIntegerRangeField, self).formfield(**defaults)


class IntegerRangeField(SmallIntegerRangeField):
    pass


class CIFieldMixin(object):
    def db_type(self, connection):
        if connection.settings_dict['ENGINE'] == 'django.db.backends.postgresql_psycopg2':
            return 'CITEXT'
        else:
            return super(CICharField, self).db_type(connection)


class CIEmailField(CIFieldMixin, models.EmailField):
    pass


class CICharField(CIFieldMixin, models.CharField):
    pass


class LabelledModelChoiceField(forms.ModelChoiceField):
    """
    Specify a function to compute the visible field name,
    if it is required to be something other than the str representation
    """
    def __init__(self, queryset=None, field_label=None, *args, **kwargs):
        if field_label is not None:
            self.field_label = field_label
            self.label_from_instance = self.get_obj_label
        super(self.__class__, self).__init__(queryset, *args, **kwargs)

    def get_obj_label(self, obj):
        return self.field_label(obj)
