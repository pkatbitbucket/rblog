import datetime
import hashlib
import logging
import json
from django.core.validators import validate_email
from django.core.validators import validate_integer
import re
import urllib
import uuid
import sys

import django
from django import forms
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth import authenticate, get_user_model, login
from django.contrib.auth.forms import PasswordResetForm as BasePasswordResetForm
from django.contrib.auth.models import Group
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse

from core.models import *  # noqa
import models as core_models
import cf_cns
from cf_fhurl import RequestForm

from wiki import models as wiki_models
from .models import Mail, User, Email, EndorsementDetail

thismodule = sys.modules[__name__]
logger = logging.getLogger(__name__)


def split_name(name):
    name = ' '.join(name.split())
    if len(name.split(' ')) > 1:
        last_name = name.split(' ')[-1]
        first_name = ' '.join(name.split(' ')[:-1])
    else:
        first_name = name
        last_name = ''
    return first_name, last_name


class MailForm(forms.ModelForm):

    class Meta:
        model = Mail
        if django.VERSION > (1, 7):
            fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(MailForm, self).__init__(*args, **kwargs)
        if self.instance.id:
            self.fields['mtype'].widget = forms.HiddenInput()
        else:
            self.fields['mtype'].choices = [(choice[0], choice[1]) for choice in self.fields[
                'mtype'].choices if not Mail.objects.filter(is_active=True, mtype=choice[0])]

    def save(self, *args, **kwargs):
        mail = super(MailForm, self).save(*args, **kwargs)
        if mail.is_active:
            Mail.objects.filter(Q(mtype=mail.mtype) & ~Q(
                id=mail.id)).update(is_active=False)


class NotificationForm(forms.ModelForm):
    """
    Modal form for notifications admin
    """
    class Meta:
        model = Notification
        exclude = []

    def __init__(self, *args, **kwargs):
        super(NotificationForm, self).__init__(*args, **kwargs)

        self.fields['to'] = forms.CharField(required=False,  widget=forms.Textarea)
        self.fields['numbers'] = forms.CharField(required=False,  widget=forms.Textarea)
        self.fields['bcc'] = forms.CharField(required=False,  widget=forms.Textarea)

    def clean(self):
        cleaned_data = super(NotificationForm, self).clean()

        # clean to:
        to = cleaned_data['to'].split(',')
        cleaned_data['to'] = [x.strip() for x in to if x]

        # clean bcc:
        bcc = cleaned_data['bcc'].split(',')
        cleaned_data['bcc'] = [x.strip() for x in bcc if x]

        # clean numbers:
        numbers = cleaned_data['numbers'].split(',')
        cleaned_data['numbers'] = [x.strip() for x in numbers if x]

        return cleaned_data


class EndorsementDetailForm(forms.ModelForm):
    """
    Modal form for Endoresement
    """
    class Meta:
        model = EndorsementDetail
        exclude = []


class UserForm(RequestForm):
    email = forms.EmailField()
    first_name = forms.CharField(max_length=255)
    last_name = forms.CharField(max_length=255, required=False)
    about = forms.CharField(widget=forms.Textarea, required=False)
    password = forms.CharField(
        widget=forms.PasswordInput, label="Password", required=False)
    repeat_password = forms.CharField(
        widget=forms.PasswordInput, label="Re-enter Password", required=False)
    is_active = forms.BooleanField(required=False)
    is_staff = forms.BooleanField(required=False)
    groups = forms.ModelMultipleChoiceField(queryset=Group.objects.all())
    gender = forms.ChoiceField(choices=User.GENDER_CHOICES)
    organization = forms.ChoiceField(choices=User.ORGANIZATION_CHOICES)
    mugshot = forms.ImageField(required=False)
    mugshot_url = forms.CharField(max_length=255, required=False)
    facebook = forms.CharField(max_length=255, required=False)
    google = forms.CharField(max_length=255, required=False)
    twitter = forms.CharField(max_length=255, required=False)

    def init(self, uid=None):
        if uid:
            self.uid = uid
            u = User.objects.get(id=uid)
            self.user = u
            udata = u.__dict__.copy()
            udata['email'] = self.user.email
            udata.update(u.extra)
            udata.update({'groups': u.groups.all()})
            self.initial = udata
        else:
            self.uid = None
            self.user = None
            self.fields['password'].required = True
            self.fields['repeat_password'].required = True

    def clean_first_name(self):
        d = self.cleaned_data.get
        first_name = d('first_name').strip()
        pattern = "^[a-zA-Z\s]*$"
        match = re.match(pattern, first_name)
        if not match:
            raise forms.ValidationError(
                "Special characters not allowed in name.")
        return first_name

    def clean_last_name(self):
        d = self.cleaned_data.get
        last_name = d('last_name').strip()
        pattern = "^[a-zA-Z\s]*$"
        match = re.match(pattern, last_name)
        if not match:
            raise forms.ValidationError(
                "Special characters not allowed in name.")
        return last_name

    def clean_repeat_password(self):
        d = self.cleaned_data.get
        if d("repeat_password") != d("password"):
            raise forms.ValidationError("Passwords do not match.")
        return d("repeat_password")

    def save(self):
        d = self.cleaned_data
        if self.uid:
            u = User.objects.get(id=self.uid)
            u.groups.clear()
            for r in d.get('groups'):
                u.groups.add(r)
            if d.get('password'):
                u.set_password(d.get('password'))
            u.save()

        else:
            u = User.objects.create_user(
                email=d['email'],
                password=d['password'],
                first_name=d['first_name'],
            )
            for r in d.get('groups'):
                u.groups.add(r)

        user_data = d.copy()
        user_data.pop('password', '')
        user_data.pop('repeat_password', '')
        user_data.pop('groups')
        extra_fields = list(set(self.fields.keys()).difference(u.__dict__))
        extra_fields.remove('groups')
        extra_data = dict([(k, d.get(k))
                           for k in extra_fields if k != 'repeat_password'])
        user_data['extra'] = extra_data
        [user_data.pop(rem, '') for rem in extra_fields]

        User.objects.filter(id=u.id).update(**user_data)

        if self.request.FILES and d.get('mugshot'):
            u.mugshot = d['mugshot']
            u.mugshot.name = 'uploads/profile/images/%s' % d['mugshot'].name
            u.save()
        return {'success': True}


class AjaxUserRegistration(RequestForm):
    name = forms.CharField(max_length=255, required=True)
    email = forms.EmailField(required=True)

    def clean_name(self):
        d = self.cleaned_data.get
        name = d('name').strip()
        pattern = "^[a-zA-Z\s]*$"
        match = re.match(pattern, name)
        if not match:
            raise forms.ValidationError(
                "Special characters not allowed in name.")
        return name

    def clean_email(self):
        d = self.cleaned_data.get

        try:
            Email.objects.get(email=d('email'), is_verified=True)
            self.email_exists()
        except Email.DoesNotExist:
            return d('email')

    def save(self):
        d = self.cleaned_data.get
        first_name, last_name = split_name(d('name'))
        fields = {
            'email': d('email'),
            'username': d('email'),
            'first_name': first_name
        }
        user = User.objects.create_user(**fields)
        user.last_name = last_name
        if 'tracking_data' not in user.extra.keys():
            tracking_data = {
                'device': 'mobile' if self.request.user_agent.is_mobile else 'desktop',
                'utm_source': self.request.COOKIES.get('utm_source', ''),
                'utm_medium': self.request.COOKIES.get('utm_medium', ''),
                'utm_campaign': self.request.COOKIES.get('utm_campaign', ''),
                'utm_adgroup': self.request.COOKIES.get('utm_adgroup', ''),
                'utm_keyword': self.request.COOKIES.get('utm_keyword', ''),
                'utm_content': self.request.COOKIES.get('utm_content', ''),
            }
            user.extra['tracking_data'] = tracking_data
        user.save()
        u = authenticate(username=user.email, password=settings.RANDOM_SEED)
        login(self.request, u)
        return self.post_auth_success()

    def email_exists(self):
        raise forms.ValidationError(
            "Looks like this email is already registered with us. Login now!")

    def post_auth_success(self):
        user = self.request.user
        return {'success': True, 'mesg': {'email': user.email, 'name': user.first_name}}


class ProductUserRegistration(AjaxUserRegistration):
    pass

    def email_exists(self):
        d = self.cleaned_data.get
        user = None
        try:
            user = User.objects.get_user(email=d('email'), phone='')
        except User.DoesNotExist:
            raise forms.ValidationError("Invalid email/password.")

        if user.token:
            user.token_updated_on = datetime.datetime.now()
            user.save()
        else:
            user.save()
        if not user.is_verified:
            obj = {
                'user_id': user.email,
                'to_email': user.email,
                'name': user.first_name,
                'reset_url': '%s/user/%s/set-password/' % (settings.SITE_URL, user.token)
            }
            cf_cns.notify('RESET_PASSWORD', to=(user.first_name, user.email), obj=obj)
            raise forms.ValidationError("""Looks like this email is already registered with us. Since you've not set
                    your password yet, we've mailed you a link to set one""")
        else:
            raise forms.ValidationError(
                "Looks like this email is already registered with us. Login now!")

    def post_auth_success(self):
        user = self.request.user
        obj = {
            'user_id': user.email,
            'to_email': user.email,
            'name': utils.clean_name(user.first_name),
            'verification_url': '%s/user/%s/set-password/' % (settings.SITE_URL, user.token)
        }
        cf_cns.notify('UNVERIFIED_WELCOME_MAIL', to=(user.first_name, user.email), obj=obj)
        response = HttpResponse(json.dumps({
            'success': True,
            'response': {
                'mesg': {
                    'email': user.email,
                    'name': user.first_name,
                },
                'success': True,
            }}))
        response.set_cookie(key='profile', value=urllib.quote(json.dumps({
            'email': user.email,
            'type': 'coverfox',
            'name': user.first_name
        })))
        return response


class AjaxUserLogin(RequestForm):
    email = forms.EmailField(required=True)
    password = forms.CharField(
        widget=forms.PasswordInput, label="Password", required=True)

    def clean_email(self):
        d = self.cleaned_data.get

        try:
            Email.objects.get(email=d('email'), is_verified=True)
            return d('email')
        except Email.DoesNotExist:
            raise forms.ValidationError(
                "Oops! Looks like this email is not registered with us. Sign up now!")

    def post_auth_success(self):
        return {'success': True, 'mesg': {'email': user.email, 'name': user.first_name}, 'error_msg': error_msg}

    def post_auth_error(self):
        error_msg = 'Incorrect password'
        return {'success': False, 'mesg': {}, 'error_msg': error_msg}

    def save(self):
        d = self.cleaned_data.get
        try:
            u = User.objects.get_user(email=d('email'), phone='')
        except User.DoesNotExist:
            raise forms.ValidationError("Invalid email/password.")

        user = authenticate(username=u.email, password=d('password'))
        if user:
            login(self.request, user)
            return self.post_auth_success()
        else:
            return self.post_auth_error()


class ProductUserLogin(AjaxUserLogin):
    pass

    def post_auth_success(self):
        u = self.request.user
        # TODO: Legacy format for response, requires change in front-end JS

        response = HttpResponse(json.dumps({
            'success': True,
            'response': {
                'mesg': {
                    'email': u.email,
                    'name': u.first_name,
                },
                'success': True,
            }}))
        response.set_cookie(key='profile', value=urllib.quote(json.dumps({
            'email': u.email,
            'type': 'coverfox',
            'name': u.first_name
        })))
        return response

    def post_auth_error(self):
        error_msg = 'Oops! Looks like you entered an incorrect password. Please try again.'
        return {'success': False, 'mesg': {}, 'error_msg': error_msg}


class IntitatePasswordReset(RequestForm):
    email = forms.EmailField(required=True)

    def clean_email(self):
        d = self.cleaned_data.get

        try:
            email = Email.objects.filter(email=d('email'), is_verified=True)
            if not email:
                raise Email.DoesNotExist()

            if len(email) > 1:
                Email.objects.fix_duplicates(email.first().user)

        except Email.DoesNotExist:
            error_msg = """<p>There is no Coverfox Account created with this email address.
                A Coverfox Account is created only after you purchase an insurance policy from Coverfox.</p>
                <p>If you have purchased a policy and your account has not been created within 48 hours of purchase
                please write to us on contact@coverfox.com or call us on +91-7718883123</p>"""
            raise forms.ValidationError(error_msg)
        return d('email')

    def save(self):
        d = self.cleaned_data.get
        user = None
        try:
            user = User.objects.get_user(email=d('email'), phone='')
        except User.DoesNotExist:
            raise forms.ValidationError("Invalid email/password.")
        if user.token:
            user.token_updated_on = datetime.datetime.now()
            user.save()
        else:
            user.save()
        obj = {
            'name': user.first_name,
            'reset_url': settings.SITE_URL + reverse('reset_password', kwargs={'token': user.token})
        }
        cf_cns.notify('RESET_PASSWORD', to=(user.first_name, user.email), obj=obj)

        return {'success': True, 'mesg': {'email': user.email, 'name': user.first_name}}


class ResetPassword(RequestForm):
    password1 = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)
    token = forms.CharField(widget=forms.HiddenInput)

    def clean_password2(self):
        d = self.cleaned_data
        if d.get('password2') != d.get('password1'):
            raise forms.ValidationError("Passwords do not match")
        if len(d.get('password1')) < 6:
            raise forms.ValidationError(
                "The passwords should must be at least 6 character long")
        return d.get('password2')

    def save(self):
        d = self.cleaned_data.get
        user = User.objects.get(token=d('token'))
        user.set_password(d('password1'))
        user.is_verified = True
        user.token = uuid.uuid4()
        user.token_updated_on = datetime.datetime.now()
        user.save()
        user = authenticate(user=user, password=d('password1'))
        if user:
            login(self.request, user)
            # putils.kiss_identify_user(self.request)
        return {'success': True, 'mesg': {'email': user.email, 'name': user.first_name}, 'error_msg': ''}


class PasswordResetForm(BasePasswordResetForm):

    def __init__(self, *args, **kwargs):
        super(PasswordResetForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = False

    mobile = forms.IntegerField(
        required=False, min_value=7000000000, max_value=9999999999)

    def clean_mobile(self):
        return str(self.cleaned_data['mobile'])

    def clean(self):
        email = self.cleaned_data.get('email')
        mobile = self.cleaned_data.get('mobile')

        if not (mobile or email):
            raise forms.ValidationError(
                'Please enter either your email address or your mobile number.')
        else:
            return self.cleaned_data

    def save(self, *args, **kwargs):
        email = self.cleaned_data.get('email')
        mobile = self.cleaned_data.get('mobile')

        if email:
            super(PasswordResetForm, self).save(*args, **kwargs)
        if mobile:
            UserModel = get_user_model()
            for user in (self.users_cache or UserModel._default_manager.filter(mobile=mobile)):
                from utils import send_sms
                from .tokens import default_otp_generator
                # unreachable code i guess.
                send_sms(user.mobile, default_otp_generator.make_token(user))


class RequirementForm(RequestForm):
    MID_MAPPING = core_models.Marketing.MID_MAPPING
    ACTIVITY_PARAM_MAP = core_models.Activity.ACTIVITY_PARAM_MAPPING

    campaign = forms.CharField(required=False)
    flow = forms.ChoiceField(required=False)
    product = forms.ChoiceField(
        required=False,
        choices=core_models.RequirementKind.PRODUCTS
    )
    user = forms.ModelChoiceField(required=False, queryset=None)
    visitor = forms.ModelChoiceField(required=False, queryset=None)
    requirement_kind = forms.ModelChoiceField(required=False, queryset=None)
    kind = forms.ChoiceField(required=False,
                             choices=core_models.RequirementKind.KINDS)
    sub_kind = forms.ChoiceField(required=False,
                                 choices=core_models.RequirementKind.SUB_KINDS)
    form_data = forms.ModelChoiceField(required=False, queryset=None)
    policy = forms.ModelChoiceField(required=False, queryset=None)
    payment = forms.ModelMultipleChoiceField(required=False, queryset=None)
    mid = forms.ModelChoiceField(required=False, queryset=None)
    remark = forms.CharField(required=False)

    def __init__(self, *args, **kwargs):
        super(RequirementForm, self).__init__(*args, **kwargs)
        self.fields['user'].queryset = core_models.User.objects.all()
        self.fields['visitor'].queryset = core_models.Tracker.objects.all()
        self.fields['requirement_kind'].queryset = core_models.RequirementKind.objects.all()
        self.fields['form_data'].queryset = core_models.FormData.objects.all()
        self.fields['policy'].queryset = core_models.Policy.objects.all()
        self.fields['payment'].queryset = core_models.Payment.objects.all()
        self.fields['mid'].queryset = core_models.Marketing.objects.all()

    def init(self, req_id=None):
        super(RequirementForm, self).__init__(req_id)
        if req_id:
            self.requirement = core_models.Requirement.objects.get(id=req_id)
        else:
            self.requirement = None

    @staticmethod
    def get_product_name(campaign):
        if campaign.startswith('motor-bike'):
            return 'bike'
        elif campaign.startswith('motor'):
            return 'car'
        else:
            return campaign.split('-')[0]

    def get_kind_subkind_from_rdata(self, rdata):
        sub_kind = ''
        campaign = rdata.get('campaign') or ''
        if 'offline' in campaign and 'motor' in campaign:
            sub_kind = 'INSPECTION_REQUIRED'
        return core_models.RequirementKind.NEW_POLICY, sub_kind

    @classmethod
    def get_mid_obj(cls, d):
        data = {}
        done = []
        for q_param, mapped_to in cls.MID_MAPPING.items():
            if mapped_to not in done:
                value = d.get(q_param)
                if value and value.lower() is not 'none':
                    data[mapped_to] = value
                    done.append(mapped_to)

        mid = core_models.Marketing.objects.filter(
            campaign=data.get('campaign', ''),
            source=data.get('source', ''),
            medium=data.get('medium', ''),
            category=data.get('category', ''),
            brand=data.get('brand', ''),
            content=data.get('content', ''),
        )
        if mid.count() > 1:
            if settings.PRODUCTION:
                send_mail(
                    subject='Multiple objects returned for mid',
                    message='MID - %s' % mid.first().id,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    recipient_list=[a[1] for a in settings.ADMINS],
                    fail_silently=False
                )
            return mid.first()

        mid = mid.first() or core_models.Marketing.objects.create(
            campaign=data.get('campaign', ''),
            source=data.get('source', ''),
            medium=data.get('medium', ''),
            category=data.get('category', ''),
            brand=data.get('brand', ''),
            content=data.get('content', ''),
        )
        return mid

    def get_form_data(self):
        if self.data.get('user_data') or self.data.get('members'):
            user_data = self.data.get('user_data') or self.data.copy()
            self.data.update(user_data)
            if self.data.get('medical_conditions'):
                self.data['medical_conditions'] = [
                    str(item) for item in
                    wiki_models.MedicalCondition.objects.filter(
                        id__in=self.data['medical_conditions']
                    ).values_list('slug', flat=True)]
            self.data.update({'city': user_data.get('user_city')})
            parents_data = {
                'pincode': user_data.get('parents_pincode'),
                'city': user_data.get('parents_city'),
            }

            for member in user_data.get('members', []):
                if member['person'].lower() == 'you':
                    self.data.update(member)
                    li = user_data['members']
                    li.remove(member)
                    user_data['members'] = li
                    continue
                elif member['person'].lower() in ['father', 'mother']:
                    member.update(parents_data)
        else:
            user_data = {}
            pass

        fsections = []
        fsections.extend(self.get_form_sections(
            'self', self.data,
            ['Person', 'Address', 'MedicalCondition', 'HealthProductPreference']))
        for member in user_data.get('members', []):
            fsections.extend(self.get_form_sections(
                member['person'].lower(), member,
                ['Person', 'Address', 'MedicalCondition']))
        fd = None
        if fsections:
            fd = core_models.FormData.objects.create(user=self.user, visitor=self.tracker)
            fd.sections.add(*fsections)
        return fd

    def get_form_sections(self, role, d, classes_list):

        fsections = []
        if isinstance(d, dict):
            d.update({'role': role})
            for cls in classes_list:
                f = getattr(thismodule, cls)(d)
                if f.is_valid() and f.cleaned_data:
                    cleaned_data = {k: str(v) for k, v in f.cleaned_data.items() if v}
                    if not ['role'] == cleaned_data.keys():
                        fs = core_models.FormSection.objects.create(form='core.forms.' + cls, data=cleaned_data)
                        fsections.append(fs.id)
        return fsections

    def create_customer_activity(self, requirement):
        mid = self.get_mid_obj()
        activity = core_models.Activity.objects.create(
            requirement=requirement,
            tracker=self.tracker,
            extra=self.data,
            fd=self.get_form_data(),
        )
        activity.mid = mid
        for k, v in self.data.items():
            if self.ACTIVITY_PARAM_MAP.get(k) and v:
                if hasattr(activity, self.ACTIVITY_PARAM_MAP[k]):
                    setattr(activity, self.ACTIVITY_PARAM_MAP[k], v)
        activity.save()
        return activity

    def clean(self):
        if self.cleaned_data['product']:
            if not self.cleaned_data['flow']:
                self.cleaned_data['flow'] = 'default'
        else:
            self.cleaned_data['product'] = self.get_product_name(self.cleaned_data['campaign'])

        if self.cleaned_data['requirement_kind']:
            req_kind = self.cleaned_data['requirement_kind']
            self.cleaned_data['product'] = req_kind.product
        elif self.cleaned_data['kind'] and self.cleaned_data['sub_kind']:
            kind, sub_kind = self.cleaned_data['kind'], self.cleaned_data['sub_kind']
            try:
                req_kind = core_models.RequirementKind.objects.get(
                    kind=kind,
                    product=self.cleaned_data['product'],
                    sub_kind=sub_kind
                )
            except core_models.RequirementKind.DoesNotExist:
                raise ValidationError('Not a valid kind and sub_kind')
            else:
                self.cleaned_data['requirement_kind'] = req_kind
        else:
            kind, sub_kind = self.get_kind_subkind_from_rdata(self.data)

            req_kind = core_models.RequirementKind.objects.get(
                kind=kind,
                product=self.cleaned_data['product'],
                sub_kind=sub_kind
            )
            self.cleaned_data['requirement_kind'] = req_kind
        return self.cleaned_data

    def save(self):
        rk = self.cleaned_data['requirement_kind']
        requirement = core_models.Requirement.objects.create(
            user=self.cleaned_data.get('user'),
            visitor=self.cleaned_data.get('visitor'),
            mid=RequirementForm.get_mid_obj(self.data),
            kind=rk,
            current_state=rk.machine.initial_state if rk.machine else None,
            extra=self.data,
        )
        return requirement

# ----------------------------------------------------------------------------
# Form data.


class FSForm(forms.Form):
    def __init__(self, data=None, tag=None, *args, **kw):
        if tag:
            # if tag is passed, this is part of FDForm.Config
            self.tag = tag
            self.mappings = kw
            return

        super(FSForm, self).__init__(data=data, *args, **kw)

    @classmethod
    def get_name(cls):
        return "%s.%s" % (cls.__module__, cls.__name__)


class BaseForm(FSForm):
    role = forms.CharField(max_length=30, required=False)


class OptionalField(forms.CharField):
    def __init__(self, length, *args, **kw):
        kw["required"] = False
        kw["max_length"] = length
        super(OptionalField, self).__init__(*args, **kw)


class Person(BaseForm):
    salutation = OptionalField(10)
    first_name = forms.CharField(max_length=50, required=False)
    middle_name = forms.CharField(max_length=50, required=False)
    last_name = forms.CharField(max_length=50, required=False)
    mobile = forms.CharField(max_length=11, required=False)
    email = forms.CharField(max_length=255, required=False)
    landline = forms.CharField(max_length=10, required=False)
    birth_date = forms.CharField(
        required=False
    )
    gender = forms.CharField(max_length=6, required=False)
    maritial_status = forms.CharField(max_length=25, required=False)
    father_name = forms.CharField(max_length=100, required=False)
    occupation = forms.CharField(max_length=70, required=False)
    pan_no = forms.CharField(max_length=15, required=False)
    annual_income_upper = forms.CharField(max_length=50, required=False)
    annual_income_bottom = forms.CharField(max_length=50, required=False)
    age = forms.CharField(max_length=6, required=False)
    passport_number = forms.CharField(max_length=20, required=False)


class HealthProductPreference(BaseForm):
    product_type = forms.CharField(required=False)
    hospital_service = forms.CharField(required=False)
    deductible = forms.CharField(required=False)


class HealthData(BaseForm):
    height_val1 = forms.CharField(max_length=6, required=False)
    height_val2 = forms.CharField(max_length=6, required=False)
    weight = forms.CharField(max_length=6, required=False)


class TravelData(BaseForm):
    places = forms.CharField(max_length=200)
    from_date = forms.CharField(max_length=200, required=False)
    to_date = forms.CharField(max_length=200, required=False)


class Address(BaseForm):
    address_line1 = forms.CharField(max_length=200, required=False)
    address_line2 = forms.CharField(max_length=200, required=False)
    address_line3 = forms.CharField(max_length=100, required=False)
    landmark = forms.CharField(max_length=65, required=False)
    city = forms.CharField(max_length=50, required=False)
    state = forms.CharField(max_length=50, required=False)
    pincode = forms.CharField(max_length=26, required=False)


class HealthQuestions(BaseForm):
    has_diabetes_type1_insulin_depended = forms.BooleanField(required=False)
    has_asthama = forms.BooleanField(required=False)
    has_thyroid_disorder = forms.BooleanField(required=False)
    has_hernia = forms.BooleanField(required=False)
    has_hiv_aids_std = forms.BooleanField(required=False)
    has_diabetes_type2 = forms.BooleanField(required=False)
    has_anaemia = forms.BooleanField(required=False)
    has_brain_and_spinal_cord_disorders = forms.BooleanField(required=False)
    has_high_low_blood_pressure = forms.BooleanField(required=False)
    has_stroke_paralysis = forms.BooleanField(required=False)
    has_stomach_or_instestine_ulcers = forms.BooleanField(required=False)
    has_slipped_disc = forms.BooleanField(required=False)
    has_arthritis = forms.BooleanField(required=False)
    has_cancer = forms.BooleanField(required=False)
    has_multiple_sclerosis = forms.BooleanField(required=False)
    has_Parkinsons_alzheimers_epilepsy = forms.BooleanField(required=False)
    has_heart_diseases = forms.BooleanField(required=False)
    has_tuberculosis = forms.BooleanField(required=False)
    has_kidney_failure_liver_failure = forms.BooleanField(required=False)
    has_joint_replacement_diseses = forms.BooleanField(required=False)
    has_congenitial_disorders = forms.BooleanField(required=False)
    has_hepatitis = forms.BooleanField(required=False)
    has_jaundice = forms.BooleanField(required=False)
    has_medical_illness_psychiatric = forms.BooleanField(required=False)
    social_status = forms.BooleanField(required=False)

    has_kidney_desease = forms.CharField(required=False)
    has_liver_desease = forms.CharField(required=False)
    travel_hospitalized = forms.CharField(required=False)
    has_other_diseases = forms.CharField(required=False)
    has_ped = forms.CharField(required=False)
    other_diseases = forms.CharField(max_length=200, required=False)
    # TODO Merge has_kidney_failure_liver_failure and has_kidney_desease.


class BikeDetail(BaseForm):
    registration_number = forms.CharField(max_length=25, required=False)
    engine_number = forms.CharField(max_length=25, required=False)
    chassis_number = forms.CharField(max_length=25, required=False)
    loan_provider = forms.CharField(max_length=50, required=False)
    vehicle_master = forms.IntegerField(required=False)
    vehicle_registration_date = forms.CharField(max_length=30, required=False)
    vehicle_rto = forms.CharField(max_length=15, required=False)
    past_policy_number = forms.CharField(required=False)
    past_policy_start_date = forms.CharField(max_length=30, required=False)
    past_policy_end_date = forms.CharField(max_length=30, required=False)
    past_policy_cover_type = forms.CharField(required=False)
    past_policy_insurer = forms.CharField(required=False)
    past_policy_insurer_city = forms.CharField(required=False)


class CarDetail(BaseForm):
    registration_number = forms.CharField(max_length=25, required=False)
    engine_number = forms.CharField(max_length=25, required=False)
    chassis_number = forms.CharField(max_length=30, required=False)
    loan_provider = forms.CharField(max_length=50, required=False)
    vehicle_master = forms.IntegerField(required=False)
    vehicle_registration_date = forms.CharField(max_length=30, required=False)
    vehicle_rto = forms.CharField(max_length=15, required=False)
    past_policy_number = forms.CharField(required=False)
    past_policy_start_date = forms.CharField(max_length=30, required=False)
    past_policy_end_date = forms.CharField(max_length=30, required=False)
    past_policy_cover_type = forms.CharField(required=False)
    past_policy_insurer = forms.CharField(required=False)
    past_policy_insurer_city = forms.CharField(required=False)


# ----------------------------------------------------------------------
# Quote forms.
# ----------------------------------------------------------------------
class MotorQuoteForm(BaseForm):
    policy_type = OptionalField(50)
    vehicle_master = OptionalField(6)
    mobile = OptionalField(20)
    policy_expiry_date = OptionalField(20)
    # TODO: Vehicle RTO.
    expired_90_days_ago = OptionalField(20)


class FDForm(forms.Form):
    """
    With a form like this:

    class MyForm(cforms.FDForm):
        name = forms.CharField()
        nominee = forms.CharField()
        city = forms.CharField()

        class Config:
            fields = [
                cforms.Person(tag="self", first_name="name"),
                cforms.Person(tag="nominee", first_name="nominee"),
                cforms.Address
            ]

    # >>> f = MyForm({"name": "amitu", "nominee": "foo", "city": "mumbai"})
    # >>> f.get_fd()
    {
        'core.forms.Address': {'city': 'mumbai'},
        'core.forms.Person': [
            {'first_name': 'amitu', 'tag': 'self'},
            {'first_name': 'foo', 'tag': 'nominee'}
        ]
    }
    """
    def __init__(self, data=None, fd=None, *args, **kw):
        if fd:
            data = self.convert_fd(fd)

        super(FDForm, self).__init__(data=data, *args, **kw)

    def convert_fd(self, data):
        ndata = {}
        return ndata

    def get_fd(self):
        data = {}

        for fsform in self.__class__.Config.fields:
            tag = None
            if (
                not isinstance(fsform, FSForm) and not issubclass(fsform, FSForm)
            ):
                raise TypeError(
                    "Improperly configured form, %s is neither a FSForm "
                    "instance, nor FSFor subclass." % (fsform)
                )

            if isinstance(fsform, FSForm) and not fsform.tag:
                raise TypeError(
                    "Improperly configured form (%s), if passing FSForm "
                    "instance, you must have tag." % (fsform)
                )

            if isinstance(fsform, FSForm):
                mappings = fsform.mappings
                tag = fsform.tag
            else:
                mappings = {k: k for k in fsform.base_fields}

            name = fsform.get_name()

            if name in data and not isinstance(data[name], list):
                data[name] = [data[name]]

            fsdata = {}

            for k, v in mappings.items():
                if v not in self.data:
                    continue
                fsdata[k] = self.data[v]
            if tag:
                fsdata["role"] = tag

            if len(fsdata) == 1:
                if "role" in fsdata:
                    continue

            if name in data:
                data[name].append(fsdata)
            else:
                data[name] = fsdata

        return data

    def save_fd(self, user=None, tracker=None, fd=None):
        # TODO: please debug this
        from core.models import FormData, FormSection

        if not user and not tracker:
            raise ValueError("Either user or tracker is required")

        data = self.get_fd()

        fd = FormData.objects.create(user=user, tracker=tracker)

        for fname, stuff in data.item():
            if isinstance(stuff, list):
                forms = stuff
            else:
                forms = [stuff]

            fses = []
            for form in forms:
                # TODO: make this smart, if value already there, get it
                fses.append(FormSection.objects.create(form=fname, data=data))

        fd.sections = fses
        return fd


# ----------------------------------------------------------------------
# Quote forms.
# ----------------------------------------------------------------------
class TravelQuoteForm(BaseForm):
    product_type = OptionalField(10)
    places = OptionalField(100)
    trip_start_date = OptionalField(20)
    trip_end_date = OptionalField(20)
    traveller_count = OptionalField(2)
    traveller1_age = OptionalField(3)
    traveller2_age = OptionalField(3)
    traveller3_age = OptionalField(3)
    traveller4_age = OptionalField(3)
    traveller5_age = forms.CharField(3)
    traveller6_age = OptionalField(3)
    is_nri = OptionalField(10)
    trip_start_place_india = OptionalField(10)
    longest_trip_duration = OptionalField(10)


class CarQuoteForm(BaseForm):
    product_type = OptionalField(10)
    vehicle_master = OptionalField(6)
    vehicle_registration_number = OptionalField(20)
    mobile = OptionalField(20)
    vehicle_rto = OptionalField(20)
    is_cng_fitted = OptionalField(20)
    cng_value = OptionalField(10)
    is_new_car = OptionalField(20)
    past_policy_expiry_date = OptionalField(20)
    new_policy_start_date = OptionalField(20)
    is_expired = OptionalField(20)
    manufacturing_date = OptionalField(20)


class BikeQuoteForm(BaseForm):
    product_type = OptionalField(10)
    vehicle_master = OptionalField(6)
    vehicle_registration_number = OptionalField(20)
    mobile = OptionalField(20)
    vehicle_rto = OptionalField(20)
    is_cng_fitted = OptionalField(20)
    cng_value = OptionalField(10)
    is_new_car = OptionalField(20)
    past_policy_expiry_date = OptionalField(20)
    new_policy_start_date = OptionalField(20)
    is_expired = OptionalField(20)
    manufacturing_date = OptionalField(20)


class HealthQuoteForm(BaseForm):
    product_type = OptionalField(50)
    gender = OptionalField(50)
    self_insured = OptionalField(10)
    self_age = OptionalField(3)
    wife_insured = OptionalField(10)
    wife_age = OptionalField(3)
    father_insured = OptionalField(10)
    father_age = OptionalField(3)
    mother_insured = OptionalField(10)
    mother_age = OptionalField(3)
    son1_insured = OptionalField(10)
    son2_insured = OptionalField(10)
    son3_insured = OptionalField(10)
    son4_insured = OptionalField(10)
    son5_insured = OptionalField(10)
    son6_insured = OptionalField(10)
    son7_insured = OptionalField(10)
    son1_age = OptionalField(3)
    son1_age = OptionalField(3)
    son2_age = OptionalField(3)
    son3_age = OptionalField(3)
    son4_age = OptionalField(3)
    son5_age = OptionalField(3)
    son6_age = OptionalField(3)
    son7_age = OptionalField(3)
    daughter1_insured = OptionalField(10)
    daughter2_insured = OptionalField(10)
    daughter3_insured = OptionalField(10)
    daughter4_insured = OptionalField(10)
    daughter5_insured = OptionalField(10)
    daughter6_insured = OptionalField(10)
    daughter7_insured = OptionalField(10)
    daughter1_age = OptionalField(3)
    daughter1_age = OptionalField(3)
    daughter2_age = OptionalField(3)
    daughter3_age = OptionalField(3)
    daughter4_age = OptionalField(3)
    daughter5_age = OptionalField(3)
    daughter6_age = OptionalField(3)
    daughter7_age = OptionalField(3)
    son1_age_month = OptionalField(3)
    son1_age_month = OptionalField(3)
    son2_age_month = OptionalField(3)
    son3_age_month = OptionalField(3)
    son4_age_month = OptionalField(3)
    son5_age_month = OptionalField(3)
    son6_age_month = OptionalField(3)
    son7_age_month = OptionalField(3)
    daughter1_age_month = OptionalField(3)
    daughter1_age_month = OptionalField(3)
    daughter2_age_month = OptionalField(3)
    daughter3_age_month = OptionalField(3)
    daughter4_age_month = OptionalField(3)
    daughter5_age_month = OptionalField(3)
    daughter6_age_month = OptionalField(3)
    daughter7_age_month = OptionalField(3)
    sum_assured = OptionalField(40)
    pincode = OptionalField(40)


class LoginOTPForm(forms.Form):
    contact = forms.CharField(required=False)

    def clean(self):
        cleaned_data = super(LoginOTPForm, self).clean()
        try:
            validate_email(cleaned_data.get('contact'))
        except forms.ValidationError:
            try:
                validate_integer(cleaned_data.get('contact'))
            except forms.ValidationError:
                self.add_error('contact', u'Enter a valid email/mobile.')
            else:
                if not 7000000000 <= int(cleaned_data.get('contact')) <= 9999999999:
                    self.add_error('contact', u'Enter a valid email/mobile.')
        return cleaned_data
