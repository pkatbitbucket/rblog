import re
from datetime import datetime

from health_product import forms as health_forms
from motor_product import forms as motor_forms
from travel_product import forms as travel_forms
from motor_product.prod_utils import get_form_data as get_motor_form_data


def generate_travel_proposal_form_data(form_data):
    data = {}

    if "members" in form_data:
        for member in form_data['members']:
            person = {
                'first_name': member.get('cust_first_name', ''),
                'last_name': member.get('cust_last_name', ''),
                'gender': member.get('cust_gender', ''),
                'mobile': member.get('com_mobile', ''),
                'email': member.get('com_email', ''),
                'birth_date': member.get('cust_dob', ''),
                'occupation': member.get('cust_occupation', ''),
                'salutation': member.get('salutation', ''),
                'is_nri': str(form_data.get('is_nri', "")),
                'passport_no': member.get('cust_passport', ''),
            }
            role = member.get('cust_relationship_with_primary', '').lower().replace(" ", "_")

            person = {'{}_{}'.format(role, key): value for key, value in person.items()}
            data.update(person)

            # Nominee form.
            # nominee_first_name, nominee_last_name = split_name(member.get('nominee_name', ''))
            # person = {
            #     'first_name': nominee_first_name,
            #     'last_name': nominee_last_name,
            #     'role': 'nominee_%s' % member.get('cust_relationship_with_primary', '').lower().replace(" ", "_")
            # }
            # add_section(person, 'core.forms.Person', fd)

            # Insert health question answers.
            health_que = {}
            if "ped" in form_data:
                relation = member.get('cust_relationship_with_primary', '').lower()
                desease = form_data["ped"]
                if relation in desease:
                    desease = desease[relation]
                    if desease in ["None", "no"]:
                        health_que['has_other_diseases'] = False
                    elif desease in ["yes"]:
                        health_que['has_other_diseases'] = True

                    health_que["other_diseases"] = member.get("disease_name", '')
            health_que = {'{}_{}'.format(role, key): value for key, value in health_que.items()}
            data.update(health_que)

    if "member1FName" in form_data:
        person_detail = {
            "first_name": form_data.get('member1FName', ''),
            "middle_name": form_data.get('member1LName', ''),
            "last_name": form_data.get('member1LName', ''),
            "gender": form_data.get('member1gender', ''),
            "salutation": form_data.get('member1Salutation', ''),
            "passport_number": form_data.get('member1Passport', ''),
            "birth_date": form_data.get('member1DOB', ''),
            "role": "self"
        }
        person_detail = {'self_{}'.format(key): value for key, value in person_detail.items()}
        data.update(person_detail)

        if "member1PEDotherDetailsTravel" in form_data:
            health_que = {
                "has_other_diseases": form_data.get("member1PEDotherDetailsTravel", ""),
                "has_heart_diseases": form_data.get("member1PEDHealthDiseaseTravel", ""),
                "has_stroke_paralysis": form_data.get("member1PEDparalysisDetailsTravel", ""),
                "has_cancer": form_data.get("member1PEDcancerDetailsTravel", ""),
                "has_lever_desease": form_data.get("member1PEDliverDetailsTravel", ""),
                "has_kidney_desease": form_data.get("member1PEDkidneyDetailsTravel", "")
            }
            health_que = {'self_{}'.format(key): value for key, value in health_que.items()}
            data.update(health_que)

    # -----------------------------------------------------------------
    travel_data = {
        'places': form_data.get('places', ''),
        "from_date": form_data.get('from_date', ''),
        "to_date": form_data.get('from_date', ''),
    }
    data.update(travel_data)

    # -----------------------------------------------------------------
    # Get address info.
    comm_address = {}
    home_address = {}
    if "contact" in form_data:
        contact = form_data['contact']
        address_info = {}
        if "com_address_line_1" in contact:
            address_info['address_line1'] = contact.get('com_address_line_1', '')
            address_info['address_line2'] = contact.get('com_address_line_2', '')
            address_info['address_line3'] = contact.get('com_address_line_3', '')
            address_info['pincode'] = contact.get('com_pincode', '')
            address_info['city'] = contact.get('com_city', '')
            address_info['state'] = contact.get('com_state', '')
            address_info['role'] = 'communication'

            comm_address = address_info

        address_info = {}
        if "home_address_line_1" in contact:
            address_info['address_line1'] = contact.get('home_address_line_1', '')
            address_info['address_line2'] = contact.get('home_address_line_2', '')
            address_info['address_line3'] = contact.get('home_address_line_3', '')
            address_info['pincode'] = contact.get('home_pincode', '')
            address_info['passport_no'] = contact.get('cust_passport', '')
            address_info['city'] = contact.get('home_city', '')
            address_info['state'] = contact.get('home_state', '')
            address_info['role'] = 'home'

            home_address = {'home_{}'.format(key): value for key, value in address_info.items()}

    address_info = {}
    if "ResAddr1" in form_data:
        address_info['address_line1'] = form_data.get('ResAddr1', '')
        address_info['address_line2'] = form_data.get('ResAddr2', '')
        address_info['address_line3'] = form_data.get('ResAddr3', '')
        address_info['address_line3'] = form_data.get('ResAddr3', '')
        address_info['passport_no'] = form_data.get('cust_passport', '')
        address_info['city'] = form_data.get('city', '')
        address_info['state'] = form_data.get('state', '')
        address_info['pincode'] = form_data.get('pincode', '')
        address_info['role'] = 'communication'

        comm_address = address_info

    data.update(comm_address)
    data.update(home_address)

    return data


def get_travel_quote_data(json_data):
    filtered_data_dict = {}
    for key, val in json_data.iteritems():
        if isinstance(val, list):
            filtered_data_dict[key] = ','.join(val)
        else:
            filtered_data_dict[key] = val

    # Prepare traveller age.
    ages = filtered_data_dict.get("adults", "")
    if "," in ages:
        ages = ages.split(",")

    if isinstance(ages, str) or isinstance(ages, unicode):
        filtered_data_dict["traveller1_age"] = ages
        filtered_data_dict["traveller_count"] = 1
    elif isinstance(ages, list):
        i = 1
        for age in ages:
            filtered_data_dict["traveller%s_age" % i] = age
            i += 1

        filtered_data_dict["traveller_count"] = len(ages)

    filtered_data_dict["trip_start_place_india"] = "true"

    travel_quote_instance = travel_forms.TravelQuoteFDForm(data=filtered_data_dict)
    return travel_quote_instance.get_fd()


def get_motor_proposal_form_data(raw):
    from motor_product.forms import MotorFDForm
    user_form_data = raw.get('form_details', {})
    user_form_data = user_form_data or raw
    first_name = user_form_data.get('cust_first_name', '')
    last_name = user_form_data.get('cust_last_name', '')
    cust_email = user_form_data.get('cust_email', '')
    cust_phone = user_form_data.get('cust_phone', '')
    gender = user_form_data.get('cust_gender', '')
    cust_marital_status = user_form_data.get('cust_marital_status', '')
    father_fname = user_form_data.get('father_first_name', '')
    father_lname = user_form_data.get('father_last_name', '')
    dob = user_form_data.get('cust_dob', '')

    if not dob:
        day = user_form_data.get('cust_dob_dd', '')
        month = user_form_data.get('cust_dob_mm', '')
        year = user_form_data.get('cust_dob_yyyy', '')
        if day and month and year:
            dob = "%s-%s-%s" % (day, month, year)

    if '-' not in dob and '/' not in dob and " " not in dob and dob:
        day = dob[:2]
        month = dob[2:4]
        year = dob[4:]
        dob = "%s-%s-%s" % (day, month, year)

    if '-' in dob or '/' in dob:
        if " " in dob:
            dob = dob.replace(' ', '')

    occupation = user_form_data.get('cust_occupation', '')
    pan_no = user_form_data.get('cust_pan', '')

    person_form_data = {
        "first_name": first_name,
        "middle_name": '',
        "last_name": last_name,
        "mobile": cust_phone,
        "email": cust_email,
        "landline": '',
        "gender": gender,
        "birth_date": dob,
        "maritial_status": str(cust_marital_status),
        "father_name": "%s %s" % (father_fname, father_lname),
        "occupation": occupation,
        "pan_no": pan_no,
    }
    nominee_name = user_form_data.get('nominee_name', '')
    nominee_age = str(user_form_data.get('nominee_age', ''))

    splitted_nominee_name = nominee_name.split(" ")

    nominee_form_data = {
        'first_name': ''.join(splitted_nominee_name[:-1]),
        'last_name': ''.join(splitted_nominee_name[:1]),
        'role': 'nominee'
    }

    if nominee_age:
        if isinstance(nominee_age, str):
            if re.match(r'^[0-9]{1,3}$', nominee_age.replace(" ", "")):
                nominee_form_data['age'] = nominee_age.replace(" ", "")
        elif isinstance(nominee_age, int):
            nominee_form_data['age'] = nominee_age

    registration_number = user_form_data.get('vehicle_reg_no', '')
    vehicle_rto = user_form_data.get('vehicle_rto', '')
    engine_number = user_form_data.get('vehicle_engine_no', '')
    chassis_number = user_form_data.get('vehicle_chassis_no', '')
    vehicle_registration_dt = user_form_data.get('vehicle_reg_date', '')
    car_financier = user_form_data.get('car-financier-text', '')
    if not car_financier:
        car_financier = user_form_data.get('car-financier', '')

    # Past policy information.
    past_policy_number = user_form_data.get('past_policy_number', '')
    past_policy_start_date = user_form_data.get("past_policy_start_date", '')
    past_policy_end_date = user_form_data.get('past_policy_end_date', '')
    if not past_policy_end_date:
        day = user_form_data.get('past_policy_end_date_dd', '')
        month = user_form_data.get('past_policy_end_date_mm', '')
        year = user_form_data.get('past_policy_end_date_yyyy', '')
        if day and month and year:
            past_policy_end_date = "%s-%s-%s" % (day, month, year)

    past_policy_cover_type = user_form_data.get('past_policy_cover_type', '')
    past_policy_insurer = str(user_form_data.get('past_policy_insurer', ''))
    past_policy_insurer_city = user_form_data.get('past_policy_insurer_city', '')

    vehicle_master = None
    if raw.get('data') and raw['data'].get('quote'):
        quote = raw['data']['quote']
        if quote.get('vehicleId'):
            vehicle_master = str(raw['data']['quote']['vehicleId'])

    vehicle_detail_form_data = {
        'registration_number': registration_number,
        'engine_number': engine_number,
        'chassis_number': chassis_number,
        'vehicle_master': vehicle_master,
        'vehicle_registration_date': vehicle_registration_dt,
        'vehicle_rto': vehicle_rto,
        'loan_provider': car_financier,
        'past_policy_number': past_policy_number,
        'past_policy_start_date': past_policy_start_date,
        'past_policy_end_date': past_policy_end_date,
        'past_policy_cover_type': past_policy_cover_type,
        'past_policy_insurer': past_policy_insurer,
        'past_policy_insurer_city': past_policy_insurer_city,
    }
    if raw.get('campaign', '').startswith('motor-bike'):
        vehicle_detail_form_data = {'bike_{}'.format(key): value for key, value in vehicle_detail_form_data.items()}
    else:
        vehicle_detail_form_data = {'car_{}'.format(key): value for key, value in vehicle_detail_form_data.items()}

    building_no = user_form_data.get('add_building_name', '')
    add_house_no = user_form_data.get('add_house_no')
    street = user_form_data.get('add_street_name', '')
    state = user_form_data.get('add_state', '')
    pincode = user_form_data.get('add_pincode', '')

    # post_office = user_form_data.get('add_post_office', '')
    landmark = user_form_data.get('add_landmark', '')

    # district = user_form_data.get('add_district', '')
    city = user_form_data.get('add_city', '')

    # Reg address
    same_addr = user_form_data.get('add-same', 'false')
    reg_address_form_data = {}
    if same_addr == 'false':
        reg_building_no = user_form_data.get('reg_add_building_name', '')
        reg_add_house_no = user_form_data.get('reg_add_house_no', '')
        reg_street = user_form_data.get('reg_add_street_name', '')
        reg_pincode = user_form_data.get('reg_add_pincode', '')
        reg_landmark = user_form_data.get('reg_add_landmark', '')
        reg_district = user_form_data.get('reg_add_district', '')
        reg_city = user_form_data.get('reg_add_city', '')
        reg_state = user_form_data.get('reg_add_state', '')

        reg_address_form_data = {
            "reg_address_line1": reg_building_no,
            "reg_address_line2": reg_add_house_no,
            "reg_address_line3": reg_street,
            "reg_pincode": reg_pincode,
            "reg_landmark": reg_landmark,
            "reg_district": reg_district,
            "reg_city": reg_city,
            "reg_state": reg_state,
        }

    address_form_data = {
        "comm_address_line1": building_no,
        "comm_address_line2": add_house_no,
        "comm_address_line3": street,
        "comm_pincode": pincode,
        "comm_landmark": landmark,
        "comm_city": city,
        "comm_state": state,
    }
    data = {}
    data.update(person_form_data)
    data.update(vehicle_detail_form_data)
    data.update(reg_address_form_data)
    data.update(address_form_data)
    return MotorFDForm(data=data).get_fd()


def get_quote_travel_form_data(request_data, dt_instance=None):
    # Get product type.
    product_type = ""

    # Check renewal process.
    # If policy expiry date is greter than current date then it is renewal case.
    # If policy expiry date is less than current date then it is expired.
    # If is_new_vehicle is there than it is new insurance for car.
    # Get past policy date datetime instance.
    product_type = ""
    try:
        past_pol_expiry_datetime_instance = datetime.strptime(
            request_data.get("pastPolicyExpiryDate", ""),
            "%d-%m-%Y"
        )
        cur_time = None
        if dt_instance:
            cur_time = dt_instance
        else:
            cur_time = datetime.now()

        if request_data.get("isNewVehicle", "") == "1":
            product_type = "new"
        elif past_pol_expiry_datetime_instance > cur_time:
            product_type = "renewal"
        elif past_pol_expiry_datetime_instance < cur_time:
            product_type = "expired"
        else:
            product_type = ""
    except:
        request_data["product_type"] = ""

    request_data["product_type"] = product_type

    vehicle_reg = request_data.get("registrationNumber[]", [])
    vehicle_rto = ""
    if isinstance(vehicle_reg, list):
        vehicle_rto = '-'.join(vehicle_reg)

    request_data["vehicle_rto"] = vehicle_rto
    return request_data


def get_value(json_data, path=None):
    path = path.split(".")
    val = json_data
    path_len = len(path)
    i = 1
    for key in path:
        if i == path_len:
            val = val.get(key, "")
        else:
            val = val.get(key, {})

    if isinstance(val, dict) or isinstance(val, list):
        val = str('')

    if isinstance(val, int):
        val = str(val)

    return str(val.encode('utf-8'))


def create_insured_forms(proposal):
    # Get insured.
    insured_persons = proposal.get("insured", {})
    roles = insured_persons.keys()
    data = {}
    for role in roles:
        insured_json = insured_persons[role]
        person_info = {
            "fname": get_value(insured_json, "first_name.value"),
            "lname": get_value(insured_json, "last_name.value"),
            "dob": get_value(insured_json, "date_of_birth.value"),
        }
        person_info = {'insured_{}_{}'.format(role, key): value for key, value in person_info.items()}

        height_val1 = get_value(insured_json, "height.value1")
        height_val2 = get_value(insured_json, "height.value2")

        health_data = {
            "height_val1": str(height_val1),
            "height_val2": str(height_val2),
            "weight": str(get_value(insured_json, "weight.value"))
        }
        health_data = {'insured_{}_{}'.format(role, key): value for key, value in health_data.items()}
        data.update(person_info)
        data.update(health_data)
    return data


def insert_nominee_info(form_data):
    nominee = form_data.get("nominee", {})
    roles = nominee.keys()
    data = {}
    for role in roles:
        if role == 'self':
            continue
        # Create nominee person model.
        role_nominee_data = nominee[role]
        person_data = {
            "f_name": get_value(role_nominee_data, "first_name.value"),
            "l_name": get_value(role_nominee_data, "last_name.value"),
            "dob": get_value(role_nominee_data, "date_of_birth.value"),
        }
        person_data = {'nominee_{}'.format(key): value for key, value in person_data.items()}
        data.update(person_data)
    return data


def generate_form_data(form_data=None):
    data = {}
    if "form_data" in form_data:
        form_data = form_data["form_data"]
        # -------------------------------------------------------------
        # Proposal form.
        # -------------------------------------------------------------
        proposer = form_data.get("proposer", {})
        person_info = {
            "f_name": get_value(proposer, "personal.first_name.value"),
            "l_name": get_value(proposer, "personal.last_name.value"),
            "gender": get_value(proposer, "personal.gender.value"),
            "maritial_status": get_value(proposer, "personal.marital_status.value"),
            "dob": get_value(proposer, "personal.date_of_birth.value"),
            "occupation": get_value(proposer, "personal.occupation.value"),
            "mobile": get_value(proposer, "contact.mobile.value"),
            "email": get_value(proposer, "contact.email.value"),
            "landline": get_value(proposer, "contact.landline.display_value"),
        }
        person_info = {'{}_{}'.format("proposer", key): value for key, value in person_info.items()}
        # -------------------------------------------------------------
        # Insured form.
        # -------------------------------------------------------------
        insured_data = create_insured_forms(form_data)
        # -------------------------------------------------------------
        # Address form.
        # -------------------------------------------------------------
        address_info = {
            "address_line1": get_value(proposer, "address.address1.value"),
            "address_line2": get_value(proposer, "address.address1.value"),
            "state": get_value(proposer, "address.state.dependant_value") or get_value(proposer, "address.state.value"),
            "city": get_value(proposer, "address.state.value1") or get_value(proposer, "address.city.value"),
            "pincode": get_value(proposer, "address.pincode.value1") or get_value(proposer, "address.city.value"),
            "landmark": get_value(proposer, "address.landmark.value")
        }
        # -------------------------------------------------------------
        # Health nominee.
        # -------------------------------------------------------------
        nominee_data = insert_nominee_info(form_data) if "nominee" in form_data else {}
        data.update(person_info)
        data.update(insured_data)
        data.update(address_info)
        data.update(nominee_data)

    return data


def get_form_data(json_form_data, form_type, product, dt_instance=None):
    """
    :param form_data: Json data from which want to get data
    :param form_type: proposal, quote
    :return: form_data json
    """
    if not isinstance(json_form_data, dict):
        raise Exception("Invalid form data")

    if form_type not in ["proposal", "quotes"]:
        raise Exception("Invalid form type")

    if product not in ["health", "motor", "car", "travel", "bike"]:
        raise Exception("Invalid product")

    if product == "health":
        if form_type == "proposal":
            proposal_form_data = generate_form_data(form_data=json_form_data)
            return health_forms.HealthFDForm(data=proposal_form_data).get_fd()

        if form_type == "quotes":
            form_data = {}
            json_data = json_form_data
            for member in json_form_data.get("members", []):
                person = member.get("id", "").lower()
                if person.lower() == "you":
                    person = "self"

                form_data["%s_insured" % person] = str(member.get("selected", ""))
                form_data["%s_age" % person] = str(member.get("age", ""))
                form_data["%s_age_month" % person] = str(member.get("age_in_months", ""))

            json_data = dict(json_data, **form_data)
            return health_forms.HealthQuoteFDForm(data=json_data).get_fd()

    if product in ["motor", "car", "bike"]:
        if form_type == "proposal":
            return get_motor_proposal_form_data(json_form_data)

        if form_type == "quotes":
            if product == "bike":
                form_type = motor_forms.BikeQuoteFDForm
            elif product in ["car", "motor"]:
                form_type = motor_forms.CarQuoteFDForm

        req_filtered_form_data = get_motor_form_data(json_form_data)
        quote_form_instance = form_type(data=req_filtered_form_data)
        return quote_form_instance.get_fd()

    if product in ["travel"]:
        if form_type == "proposal":
            json_form_data.update(json_form_data.get('insured_details_json', {}))
            generate_travel_proposal_form_data(json_form_data)
            return travel_forms.TravelFDForm(generate_travel_proposal_form_data(json_form_data)).get_fd()

        if form_type == "quotes":
            return get_travel_quote_data(json_form_data)
