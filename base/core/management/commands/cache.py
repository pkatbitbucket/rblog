from django.core.management.base import NoArgsCommand, BaseCommand, CommandError
from django.conf import settings
from optparse import make_option
import os
import shutil
import glob
from blog.models import Post
from dcache import cache_utils


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--post',
                    dest='post',
                    help='--post <postID>, leave blank for all'),
    )

    def handle(self, *args, **options):
        if options['post']:
            p = Post.objects.get(id=options['post'].strip())
            cache_utils.regenerate(p)
        else:
            cache_utils.regenerate_all()
