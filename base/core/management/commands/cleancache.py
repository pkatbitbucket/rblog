from django.core.management.base import NoArgsCommand, BaseCommand, CommandError
from django.conf import settings

import re
import sys
import subprocess

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('cache', type=str)

    def handle(self, *args, **options):
        cache = options['cache']
        sys.stdout.write("Asking to clear %s cache....\n" % cache)
        if cache not in settings.CACHES:
            raise CommandError('Cache "%s" not found in settings' % cache)
        else:
            location = settings.CACHES[cache]['LOCATION']
            match = re.match(r"redis://(?P<host>.+?):(?P<port>\d+)/(?P<db>\d+)", location)
            if match:
                db = match.group('db')
                host = match.group('host')
                port = match.group('port')
                sys.stdout.write("Found redis-db: %s to clean...\n" % location)
                command = "redis-cli -n %s -h %s -p %s flushdb" % (db, host, port) 
            else:
                raise CommandError('Cache "%s" found in settings but cannot parse it. Only support redis for now' % cache)

        if command:
            sys.stdout.write('Going to run "%s". ' % command)
            if raw_input("Please confirm (Y/n): ") == "Y":
                try:
                    output = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True)
                except subprocess.CalledProcessError as e:
                    output = e.output
                finally:
                    print output
