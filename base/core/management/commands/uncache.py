from django.core.management.base import NoArgsCommand, BaseCommand, CommandError
from django.conf import settings
from optparse import make_option
import os
import shutil
import glob
import redis


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--css',
                    action='store_true',
                    dest='css',
                    default=False,
                    help='Delete only css cache'),
        make_option('--js',
                    action='store_true',
                    dest='js',
                    default=False,
                    help='Delete only javscript cache'),
        make_option('--html',
                    action='store_true',
                    dest='html',
                    default=False,
                    help='Delete only html cache'),
    )

    def handle(self, *args, **options):
        # db = 10 stands for compressor backend
        publisher = redis.Redis(host='localhost', port=6379, db=10)
        publisher.flushdb()
        cache_dir = os.path.join(
            settings.COMPRESS_ROOT, settings.COMPRESS_OUTPUT_DIR)
        dirs = []
        if options['css']:
            dirs.append(os.path.join(cache_dir, 'css'))
        if options['js']:
            dirs.append(os.path.join(cache_dir, 'js'))
        if options['html']:
            dirs.append(os.path.join(cache_dir, 'html'))

        if not dirs:
            dirs.append(os.path.join(cache_dir, 'css'))
            dirs.append(os.path.join(cache_dir, 'js'))
            dirs.append(os.path.join(cache_dir, 'html'))

        for d in dirs:
            for root, dirs, files in os.walk(d):
                for f in files:
                    os.unlink(os.path.join(root, f))
                for d in dirs:
                    shutil.rmtree(os.path.join(root, d))
