from __future__ import unicode_literals

import logging
from itertools import chain
from urlparse import urlparse

from cfblog.models import Content
from django.conf import settings
from django.utils.functional import SimpleLazyObject, lazy

from core import activity, models
from utils import log_error

__author__ = 'vinay'

EMPTY_VALUES = ('', None, 'null', 'none', 'None')

logger = logging.getLogger(__name__)


def clean_dict(d):
    cleaned = {}
    for key, value in d.iteritems():
        if value not in EMPTY_VALUES:
            cleaned[key] = value
    return cleaned


def _get_mid(mid_data):
    data = clean_dict(mid_data)
    if 'mid' in data:
        return models.Marketing.objects.get(id=data['mid'])
    return models.Marketing.objects.get_or_create_mid(data)


def get_mid(request):
    if not hasattr(request, '_cache_mid'):
        tracker = request.TRACKER
        tracker.refresh_from_db()
        if 'marketing_data' not in tracker.extra:
            tracker.extra['_prev_marketing_data'] = \
                request.session.get('_prev_marketing_data', {})
            tracker.extra['marketing_data'] = \
                request.session.get('marketing_data', {})
            tracker.save(update_fields=['extra'])
        mid_data = tracker.extra.get('marketing_data', {})
        prev_mid_data = tracker.extra.get('_prev_marketing_data', {})
        try:
            request._cache_mid = _get_mid(mid_data)
        except (models.Marketing.DoesNotExist, ValueError):
            log_error(logger, request=request, msg='Invalid MID passed')
            request._cache_mid = _get_mid(prev_mid_data)
    return request._cache_mid


def get_mini_dict(request):
    if not hasattr(request, '_cache_mini_dict'):
        request._cache_mini_dict = activity.request_to_dict(request)
    return request._cache_mini_dict


class CRMMiddleware(object):

    def process_response(self, request, response):
        path = request.path_info
        if (
            request.is_ajax()
            or 'text/html' not in response.get('Content-Type')
            or 'api' in path
        ):
            return response
        if request.user and request.user.is_authenticated() and request.user.advisor.exists():
            to_inject_js = "<script type='text/javascript' src='%scrm/advisor.js'>" % (settings.STATIC_URL)
            response.content = response.content + to_inject_js
        return response


class MarketingMiddleware(object):

    def process_request(self, request):
        # Should be placed after putils.TrackMiddleware
        assert hasattr(request, 'TRACKER')
        if request.is_ajax():
            pass
        else:
            self._setup(request)

        request.MID = SimpleLazyObject(lambda: get_mid(request))
        request.mini_dict = lazy(lambda: get_mini_dict(request), dict)()

    @classmethod
    def _setup(cls, request):
        request_data = clean_dict(request.GET.dict())
        if any(i in request_data
               for i in chain(models.Marketing.MID_MAPPING,
                              models.Activity.ACTIVITY_PARAM_MAPPING, ('mid',))
               if i not in ('triggered_page', 'landing_page_url')):
            return cls._set_request_data(request, request_data)

        lp = request.COOKIES.get(
            'landing_page_url', ''
        ) or request.build_absolute_uri()
        lp_path = urlparse(lp)[2]
        try:
            content = Content.objects.published().get(url=lp_path)
        except Content.DoesNotExist:
            pass
        else:
            request_data['network'] = 'ORGANIC'
            request_data['content'] = content.url
            cls._set_request_data(request, request_data)

    @staticmethod
    def _set_request_data(request, request_data):
        tracker = request.TRACKER
        tracker.extra['_prev_marketing_data'] = tracker.extra.get(
            'marketing_data', {})
        tracker.extra['marketing_data'] = request_data
        tracker.save(update_fields=['extra'])


class WebviewMiddleware(object):

    def process_response(self, request, response):
        if request.META.get("HTTP_X_COVERFOX_WEBVIEW", False):
            response.set_cookie('webview', 'true')
        return response
