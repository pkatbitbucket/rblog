# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.models
import customdb.thumbs
import django_extensions.db.fields
import jsonfield.fields
import core.fields
import django.utils.timezone
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(
                    max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(
                    null=True, verbose_name='last login', blank=True)),
                ('token', django_extensions.db.fields.UUIDField(
                    editable=False, blank=True)),
                ('token_updated_on', models.DateTimeField(auto_now=True)),
                ('email', models.EmailField(unique=True, max_length=255,
                                            verbose_name='e-mail address', db_index=True)),
                ('username', models.CharField(unique=True,
                                              max_length=255, verbose_name='username', db_index=True)),
                ('slug', models.SlugField(max_length=70, verbose_name='slug')),
                ('first_name', models.CharField(max_length=50,
                                                verbose_name='first name', blank=True)),
                ('middle_name', models.CharField(max_length=50,
                                                 verbose_name='middle name', blank=True)),
                ('last_name', models.CharField(max_length=50,
                                               verbose_name='last name', blank=True)),
                ('birth_date', models.DateField(null=True,
                                                verbose_name='birth date', blank=True)),
                ('is_active', models.BooleanField(
                    default=True, verbose_name='active')),
                ('is_staff', models.BooleanField(
                    default=False, verbose_name='staff status')),
                ('is_superuser', models.BooleanField(
                    default=False, verbose_name='superuser status')),
                ('is_verified', models.BooleanField(default=False)),
                ('date_joined', models.DateTimeField(
                    default=django.utils.timezone.now, verbose_name='date joined')),
                ('code', models.CharField(db_index=True,
                                          max_length=100, verbose_name='code', blank=True)),
                ('organization', models.CharField(max_length=100, verbose_name='organization', choices=[(b'', b''), (b'COVERFOX', b'COVERFOX'), (
                    b'LIFENET', b'LIFENET'), (b'ENSER', b'ENSER'), (b'CARDEKHO', b'CARDEKHO'), (b'GIRNARSOFT', b'GIRNARSOFT')])),
                ('gender', models.PositiveSmallIntegerField(blank=True, null=True,
                                                            verbose_name='gender', choices=[(1, b'Male'), (2, b'Female')])),
                ('mugshot', customdb.thumbs.ImageWithThumbsField(
                    null=True, upload_to=core.models.get_mugshot_path, blank=True)),
                ('extra', jsonfield.fields.JSONField(
                    default={}, verbose_name='extra', blank=True)),
                ('created_on', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
                ('updated_on', models.DateTimeField(auto_now=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('activity_id', django_extensions.db.fields.UUIDField(
                    db_index=True, editable=False, blank=True)),
                ('activity_type', models.CharField(max_length=255, blank=True)),
                ('is_processed', models.BooleanField(default=False)),
                ('extra', jsonfield.fields.JSONField(
                    default={}, verbose_name='extra', blank=True)),
                ('created', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
                ('updated_on', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Mail',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('mtype', models.CharField(max_length=255, choices=[(b'ON_SUBSCRIPTION', b'ON_SUBSCRIPTION (subscriber)'), (b'ON_REVIEW_SIGNUP', b'ON_REVIEW_SIGNUP (subscriber)'), (b'INVITE_MAIL', b'INVITE_MAIL (subscriber, invited_by)'), (b'TRANSACTION_SUCCESS_MAIL', b'TRANSACTION_SUCCESS_MAIL (obj)'), (b'TRANSACTION_FAILURE_MAIL', b'TRANSACTION_FAILURE_MAIL (obj)'), (b'TRANSACTION_DROP_MAIL', b'TRANSACTION_DROP_MAIL (obj)'), (b'RESULTS_DROP_MAIL', b'RESULTS_DROP_MAIL (obj)'), (b'RESULTS_DROP_MAIL2', b'RESULTS_DROP_MAIL2 (obj)'), (b'VERIFIED_WELCOME_MAIL', b'VERIFIED_WELCOME_MAIL (obj)'), (b'UNFERIFIED_WELCOME_MAIL', b'UNFERIFIED_WELCOME_MAIL (obj)'), (
                    b'RESET_PASSWORD', b'RESET_PASSWORD (obj)'), (b'MOTOR_TRANSACTION_SUCCESS_MAIL', b'MOTOR_TRANSACTION_SUCCESS_MAIL (obj)'), (b'MOTOR_TRANSACTION_FAILURE_MAIL', b'MOTOR_TRANSACTION_FAILURE_MAIL (obj)'), (b'MOTOR_QUOTES_MAIL', b'MOTOR_QUOTES_MAIL (obj)'), (b'MOTOR_DISCOUNT_CODE_MAIL', b'MOTOR_DISCOUNT_CODE_MAIL (obj)'), (b'MOTOR_ADMIN_MAIL', b'MOTOR_ADMIN_MAIL (obj)'), (b'RSA_STEP2_VALID_INS', b'RSA_STEP2_VALID_INS (obj)'), (b'RSA_STEP2_EXPIRED_INS', b'RSA_STEP2_EXPIRED_INS (obj)'), (b'APPOINTMENT_ALERT', b'APPOINTMENT_ALERT (obj)'), (b'USER_ACCOUNT_CREATION', b'USER_ACCOUNT_CREATION')])),
                ('body', models.TextField()),
                ('text', models.TextField()),
                ('is_active', models.BooleanField(default=False)),
                ('created', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
            ],
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('object_id', models.PositiveIntegerField()),
                ('rating', core.fields.IntegerRangeField(validators=[
                 django.core.validators.MaxValueValidator(5), django.core.validators.MinValueValidator(1)])),
                ('content', models.TextField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('creator', models.ForeignKey(related_name='core_review_created',
                                              blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('editor', models.ForeignKey(related_name='core_review_last_modified',
                                             blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Tracker',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('session_key', models.CharField(unique=True, max_length=255)),
                ('extra', jsonfield.fields.JSONField(
                    default={}, verbose_name='extra', blank=True)),
                ('created', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
                ('user', models.ForeignKey(blank=True,
                                           to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='UserRole',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('role', models.CharField(default=b'CUSTOMER', max_length=50, choices=[(b'VISITOR', b'Visitor'), (b'CUSTOMER', b'Customer'), (b'AUTHOR', b'Author'), (b'ADMIN', b'Admin'), (b'TESTER', b'Tester'), (b'HOSPITAL_DATA_ENTRY', b'Hospital Data entry'), (
                    b'WIKI_DATA_ENTRY', b'Wiki Data Entry'), (b'INSURER', b'Insurer'), (b'REPORT', b'Report'), (b'THIRDPARTY', b'Third Party'), (b'MOTOR_RSA_CREDR', b'Motor RSA From CredR'), (b'AUTO_BOT', b'Campaign Auto Bots'), (b'MOTOR_CAT', b'Coverfox Advisory Team For Motor')])),
            ],
        ),
        migrations.AddField(
            model_name='activity',
            name='tracker',
            field=models.ForeignKey(to='core.Tracker'),
        ),
        migrations.AddField(
            model_name='user',
            name='roles',
            field=models.ManyToManyField(to='core.UserRole', blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='review',
            unique_together=set([('user', 'content_type', 'object_id')]),
        ),
    ]
