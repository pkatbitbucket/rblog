# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userrole',
            name='role',
            field=models.CharField(default=b'CUSTOMER', max_length=50, choices=[(b'VISITOR', b'Visitor'), (b'CUSTOMER', b'Customer'), (b'AUTHOR', b'Author'), (b'ADMIN', b'Admin'), (b'TESTER', b'Tester'), (b'HOSPITAL_DATA_ENTRY', b'Hospital Data entry'), (b'WIKI_DATA_ENTRY', b'Wiki Data Entry'), (b'INSURER', b'Insurer'), (b'REPORT', b'Report'), (b'THIRDPARTY', b'Third Party'), (b'MOTOR_RSA_CREDR', b'Motor RSA From CredR'), (b'AUTO_BOT', b'Campaign Auto Bots'), (b'MOTOR_CAT', b'Coverfox Advisory Team For Motor'), (b'CD_ACCOUNT_ADMIN', b'CD Account Admin')]),
        ),
    ]
