# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.contrib.postgres.operations import CreateExtension
import core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_user_phone'),
    ]

    operations = [
        CreateExtension("citext"),
        migrations.AlterField(
            model_name='user',
            name='email',
            field=core.fields.CIEmailField(unique=True, max_length=255, verbose_name='e-mail address', db_index=True),
        ),
    ]
