# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def add_create_user_account_mail(apps, schema_editor):
    Mail = apps.get_model('core', 'Mail')
    mtype = 'NON_RM_USER_ACCOUNT_CREATION'
    subject = 'Coverfox account created'
    body = """Hi {{obj.name}},

Your account has been created on Coverfox.

Please set up a new password for your Coverfox Account, by opening the following link on your browser.

{{obj.reset_url}}

Cheers!
Team Coverfox."""
    html_body = """<p>Hi {{obj.name}},</p>
<p>Your account has been created on <a href="http://www.coverfox.com">Coverfox</a>.</p>
<p>Please set up a new password for your Coverfox Account, by opening the following link on your browser.</p>
<p>{{obj.reset_url}}</p>
<p>Cheers!<br /> Team Coverfox.</p>"""
    Mail.objects.create(
        mtype=mtype,
        title=subject,
        text=body,
        body=html_body,
        is_active=True,
    )


def remove_create_user_account_mail(apps, schema_editor):
    Mail = apps.get_model('core', 'Mail')
    m = Mail.objects.get(mtype='NON_RM_USER_ACCOUNT_CREATION')
    m.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20151005_1827'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mail',
            name='mtype',
            field=models.CharField(max_length=255, choices=[(b'ON_SUBSCRIPTION', b'ON_SUBSCRIPTION (subscriber)'), (b'ON_REVIEW_SIGNUP', b'ON_REVIEW_SIGNUP (subscriber)'), (b'INVITE_MAIL', b'INVITE_MAIL (subscriber, invited_by)'), (b'TRANSACTION_SUCCESS_MAIL', b'TRANSACTION_SUCCESS_MAIL (obj)'), (b'TRANSACTION_FAILURE_MAIL', b'TRANSACTION_FAILURE_MAIL (obj)'), (b'TRANSACTION_DROP_MAIL', b'TRANSACTION_DROP_MAIL (obj)'), (b'RESULTS_DROP_MAIL', b'RESULTS_DROP_MAIL (obj)'), (b'RESULTS_DROP_MAIL2', b'RESULTS_DROP_MAIL2 (obj)'), (b'VERIFIED_WELCOME_MAIL', b'VERIFIED_WELCOME_MAIL (obj)'), (b'UNFERIFIED_WELCOME_MAIL', b'UNFERIFIED_WELCOME_MAIL (obj)'), (b'RESET_PASSWORD', b'RESET_PASSWORD (obj)'), (b'MOTOR_TRANSACTION_SUCCESS_MAIL', b'MOTOR_TRANSACTION_SUCCESS_MAIL (obj)'), (b'MOTOR_TRANSACTION_FAILURE_MAIL', b'MOTOR_TRANSACTION_FAILURE_MAIL (obj)'), (b'MOTOR_QUOTES_MAIL', b'MOTOR_QUOTES_MAIL (obj)'), (b'MOTOR_DISCOUNT_CODE_MAIL', b'MOTOR_DISCOUNT_CODE_MAIL (obj)'), (b'MOTOR_ADMIN_MAIL', b'MOTOR_ADMIN_MAIL (obj)'), (b'RSA_STEP2_VALID_INS', b'RSA_STEP2_VALID_INS (obj)'), (b'RSA_STEP2_EXPIRED_INS', b'RSA_STEP2_EXPIRED_INS (obj)'), (b'APPOINTMENT_ALERT', b'APPOINTMENT_ALERT (obj)'), (b'USER_ACCOUNT_CREATION', b'USER_ACCOUNT_CREATION'), (b'NON_RM_USER_ACCOUNT_CREATION', b'NON_RM_USER_ACCOUNT_CREATION (obj)')]),
        ),
        migrations.RunPython(add_create_user_account_mail, remove_create_user_account_mail),
    ]
