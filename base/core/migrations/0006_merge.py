# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20150930_1511'),
        ('core', '0005_create_user_account_non_rm'),
    ]

    operations = [
    ]
