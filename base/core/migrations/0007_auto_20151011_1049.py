# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.contrib.auth.models import Group


def convert_roles_to_groups(apps, schema_editor):
	UserRole = apps.get_model('core', 'UserRole')
	User = apps.get_model('core', 'User')

	already_done = list(Group.objects.all().values_list('name', flat=True))
	ignore_roles = ['VISITOR', 'CUSTOMER']

	for role in UserRole.objects.all():
		if role.role in already_done or role.role in ignore_roles:
			continue
		elif role.role == 'ADMIN':
			User.objects.filter(roles=role).update(is_superuser=True)
			already_done.append('ADMIN')
		else:
			user_ids = User.objects.filter(roles=role).values_list('pk', flat=True)
			group = Group.objects.create(name=role.role)

			if len(user_ids) > 0:
				ThroughModel = group.user_set.through

				through_objs = []
				for user_id in user_ids:
					through_objs.append(ThroughModel(user_id=user_id, group_id=group.pk))

				ThroughModel.objects.bulk_create(through_objs)

			already_done.append(group.name)


def convert_groups_to_roles(apps, schema_editor):
	pass

class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_merge'),
    ]

    operations = [
    	migrations.RunPython(convert_roles_to_groups, convert_groups_to_roles)
    ]
