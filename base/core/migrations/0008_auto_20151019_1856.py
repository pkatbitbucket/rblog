# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.users
import customdb.thumbs


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20151011_1049'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='roles',
        ),
        migrations.AlterField(
            model_name='user',
            name='mugshot',
            field=customdb.thumbs.ImageWithThumbsField(null=True, upload_to=core.users.get_mugshot_path, blank=True),
        ),
        migrations.DeleteModel(
            name='UserRole',
        ),
    ]
