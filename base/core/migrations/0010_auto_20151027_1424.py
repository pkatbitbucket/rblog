# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        ('core', '0009_auto_20151019_1955'),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(help_text=b'\n        The name of the company. This is user visible name, there may be\n        duplicates, but we do not care.\n\n        This is not indexed.\n        ', max_length=200)),
                ('slug', models.SlugField(help_text=b'\n        Domain name of company.\n        ')),
                ('city', models.CharField(max_length=30, verbose_name='city', blank=True)),
                ('address', models.CharField(max_length=200, verbose_name='address', blank=True)),
                ('creator', models.ForeignKey(related_name='core_company_created', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('editor', models.ForeignKey(related_name='core_company_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('title', models.CharField(default=b'', help_text=b'\n        The company specific position of the employee. Eg VP Engineering.\n\n        No validations on this.\n        ', max_length=200, blank=True)),
                ('joined_on', models.DateField(default=datetime.datetime.today, help_text=b'\n        This can get complicated eventually, a person may accept a position on\n        one date, and join on another. For now this field means the date from\n        which the employee starts getting salary.\n        ')),
                ('employee_id', models.CharField(default=b'', help_text=b'\n        Employee ID of the company. This is company specific. Ideally we can\n        have a database constraint of unique together (company, eid), but for\n        now we are not adding it.\n        ', max_length=200, blank=True)),
                ('left_on', models.DateField(default=None, help_text=b'\n        This too is complicated. For now this means the last date, when salary\n        stops. Full and final settlement may happen later. Ideally a person\n        should be visible, to HR etc, till FNF. We will capture FNF through\n        another field when needed.\n        ', null=True, blank=True)),
                ('company', models.ForeignKey(help_text=b'\n        The company. An employee can possibly be part of same company multiple\n        times, like I had two stints with BrowserStack. So we can not put a\n        unique together on (user, company) tuple, we must do manual validation.\n        ', to='core.Company')),
                ('groups', models.ManyToManyField(related_query_name=b'user', related_name='employee_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('user_permissions', models.ManyToManyField(related_query_name=b'user', related_name='employee_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions')),
            ],
        ),
        migrations.AddField(
            model_name='company',
            name='employees',
            field=models.ManyToManyField(related_name='companies', through='core.Employee', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='company',
            name='parent',
            field=models.ForeignKey(blank=True, to='core.Company', help_text=b'\n            Parent company. This is the LEGAL parent company of a company, as in\n            by company laws of India.\n        ', null=True),
        ),
        migrations.AlterUniqueTogether(
            name='employee',
            unique_together=set([('company', 'user', 'joined_on'), ('company', 'user', 'left_on')]),
        ),
    ]
