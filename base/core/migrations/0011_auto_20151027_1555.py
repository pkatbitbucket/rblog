# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_auto_20151027_1424'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company',
            name='city',
        ),
        migrations.AlterField(
            model_name='company',
            name='address',
            field=models.TextField(max_length=200, verbose_name='address', blank=True),
        ),
    ]
