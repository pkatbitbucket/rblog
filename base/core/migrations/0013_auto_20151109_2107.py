# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.models
from django.conf import settings
import django.contrib.postgres.fields.hstore
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_new_motor_success_message'),
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=255, blank=True)),
                ('document', models.FileField(upload_to=core.models.get_document_upload_path)),
                ('app', models.CharField(max_length=255, null=True, blank=True)),
                ('is_private', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='FormData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('creator', models.ForeignKey(related_name='core_formdata_created', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('editor', models.ForeignKey(related_name='core_formdata_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='FormSection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('form', models.CharField(max_length=200)),
                ('data', django.contrib.postgres.fields.hstore.HStoreField(default={}, blank=True)),
                ('creator', models.ForeignKey(related_name='core_formsection_created', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('editor', models.ForeignKey(related_name='core_formsection_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Marketing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('device', models.CharField(default=b'desktop', help_text=b'Name of device, eg desktop', max_length=40, choices=[(b'mobile', b'Mobile'), (b'desktop', b'Desktop'), (b'mobile web', b'Mobile Web')])),
                ('network', models.CharField(help_text=b'\n            Network that sent this traffic. Examples: facebook, google,\n            mailchimp.\n        ', max_length=100)),
                ('campaign', models.CharField(help_text=b'\n            The campaign id that brought this traffic. This can be google ad id,\n            or email blast id etc.\n        ', max_length=100, blank=True)),
                ('paid', models.BooleanField(default=False)),
                ('creator', models.ForeignKey(related_name='core_marketing_created', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('editor', models.ForeignKey(related_name='core_marketing_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False)),
                ('amount', models.FloatField(default=0.0)),
                ('status', models.CharField(default=b'pending', max_length=40, choices=[(b'pending', b'Payment is pending'), (b'cancelled', b'User cancelled the Payment'), (b'timedout', b'Timed out'), (b'success', b'Payment successful'), (b'error', b'Error in payment'), (b'third_party', b"Payment is pending on third party's side")])),
                ('creator', models.ForeignKey(related_name='core_payment_created', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('editor', models.ForeignKey(related_name='core_payment_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Requirement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('kind', models.CharField(default=b'new_health_insurance', max_length=100, choices=[(b'new_health_insurance', b'new halt'), (b'change_name', b'change name'), (b'New Motor Insurance', b'New motor Inturance')])),
                ('state', models.CharField(max_length=100, verbose_name=b'new', choices=[(b'sold', b'sold'), (b'user_said_no', b'user refused')])),
                ('activity', models.ForeignKey(to='core.Activity')),
                ('creator', models.ForeignKey(related_name='core_requirement_created', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('editor', models.ForeignKey(related_name='core_requirement_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('fd', models.ForeignKey(blank=True, to='core.FormData', null=True)),
                ('mid', models.ForeignKey(blank=True, to='core.Marketing', null=True)),
                ('payment', models.ManyToManyField(to='core.Payment')),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterModelOptions(
            name='employee',
            options={'permissions': (('is_dealer', 'Is Dealer'), ('can_view_pending', 'Can View Pending'), ('can_inspect', 'Can Inspect'))},
        ),
        migrations.AlterField(
            model_name='company',
            name='slug',
            field=models.SlugField(help_text=b'\n        Domain name of company.\n        ', unique=True),
        ),
        migrations.AlterField(
            model_name='employee',
            name='groups',
            field=models.ManyToManyField(related_query_name=b'employee', related_name='employee_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups'),
        ),
        migrations.AlterField(
            model_name='employee',
            name='user',
            field=models.ForeignKey(related_name='employee_instance', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='employee',
            name='user_permissions',
            field=models.ManyToManyField(related_query_name=b'employee', related_name='employee_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions'),
        ),
        migrations.CreateModel(
            name='Policy',
            fields=[
                ('document_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.Document')),
                ('status', models.CharField(default=b'valid', max_length=100, choices=[(b'valid', b'Policy is valid'), (b'future', b'Will Start In Future'), (b'expired', b'Plicy has been expired'), (b'cancelled', b'Policy is cancelled')])),
                ('issue_date', models.DateField()),
                ('expiry_date', models.DateField()),
                ('mid', models.ForeignKey(blank=True, to='core.Marketing', null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('core.document',),
        ),
        migrations.AddField(
            model_name='formdata',
            name='sections',
            field=models.ManyToManyField(to='core.FormSection'),
        ),
        migrations.AddField(
            model_name='formdata',
            name='user',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='formdata',
            name='visitor',
            field=models.ForeignKey(blank=True, to='core.Tracker', null=True),
        ),
        migrations.AddField(
            model_name='document',
            name='creator',
            field=models.ForeignKey(related_name='core_document_created', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='document',
            name='editor',
            field=models.ForeignKey(related_name='core_document_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='requirement',
            name='policy',
            field=models.ForeignKey(blank=True, to='core.Policy', help_text=b'\n        Each requirement is about some policy. For a new purchase requirement, this will be null, and will be added\n        at the end of sale. For other requirements, like change name for this already sold policy, this will field will\n        be used.\n        ', null=True),
        ),
        migrations.AddField(
            model_name='policy',
            name='requirement_sold',
            field=models.ForeignKey(related_name='policy_sold', to='core.Requirement', help_text=b'\n        This is the requirement that lead to purchase of this policy.\n        '),
        ),
        migrations.AddField(
            model_name='policy',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
