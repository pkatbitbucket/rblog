# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def change_motor_success_mail(apps, schema_editor):
    Mail = apps.get_model('core', 'Mail')
    mail = Mail.objects.get(mtype='MOTOR_TRANSACTION_SUCCESS_MAIL')
    title = 'Payment for your {% ifequal obj.policy_type "renew" %}{{obj.vehicle_type|lower}} insurance renewal{% else %}{% ifequal obj.policy_type "new" %}new {{obj.vehicle_type|lower}} insurance{% else %}expired {{obj.vehicle_type|lower}} insurance renewal{% endifequal %}{% endifequal %} on Coverfox.com was successful'
    body = """Hello {{obj.name}},

Thank you for choosing Coverfox for your {{obj.vehicle_type}} Insurance. Your transaction was successful.
{% if obj.policy_document_url %}You can download your original policy copy by clicking on the link {{ obj.policy_document_url }}{% else %}You will receive the original policy document through email within {{ obj.policy_tat }}.{% endif %}

Following are the details of your {{obj.vehicle_type|lower}} insurance policy:

Insurance Company:  {{obj.insurer_title}}
Premium: INR {{obj.premium}}
Transaction Number:   {{obj.transaction_number}}

{% if obj.policy_document_url %}If you face any issues, please call us on our Toll Free Number: 1800-209-99-70 (Monday - Saturday from 10 a.m - 7 p.m) or email us on info@coverfox.com.{% else %}Sometimes the insurer might take longer to generate the policy document. There is nothing to worry about.
In case you do not receive the policy PDF in the next 24 hours, please call us on our Toll Free Number: 1800-209-99-70 (Monday - Saturday from 10 a.m - 7 p.m) or email us on info@coverfox.com.{% endif %}

Cheers!
Team Coverfox"""
    html_body = """<p>Hello {{obj.name}},</p>
<p>&nbsp;</p>
<p>Thank you for choosing Coverfox for your {{obj.vehicle_type}}&nbsp;Insurance. Your transaction was successful.{% if obj.policy_document_url %}You can download your original policy copy by clicking on the link below:</p>
<p><a class="btn btn-warning" href="{{ obj.policy_document_url }}" target="_blank">Download e-Policy</a>&nbsp;{%else%}</p>
<p>You will receive the original policy document through email within {{ obj.policy_tat }}.{% endif %}</p>
<p>&nbsp;</p>
<p>Following are the details of your {{obj.vehicle_type|lower}} insurance policy:</p>
<table>
<tbody>
<tr><th>Insurance Company:</th>
<td>{{obj.insurer_title}}</td>
</tr>
<tr><th>Premium:</th>
<td>INR {{obj.premium}}</td>
</tr>
<tr><th>Transaction Number:</th>
<td>{{obj.transaction_number}}</td>
</tr>
</tbody>
</table>
<p>{% if obj.policy_document_url %}If you face any issues, please call us on our Toll Free Number: <span>1800-209-99-70</span>&nbsp; (Monday - Saturday from 10 a.m - 7 p.m) or email us on info@coverfox.com.</p>
<p>{%else%}</p>
<p>Sometimes the insurer might take longer to generate the policy document. There is nothing to worry about.</p>
<p>In case you do not receive the policy PDF in the next 24 hours, please call us on our Toll Free Number: <span>1800-209-99-70</span>&nbsp;(Monday - Saturday from 10 a.m - 7 p.m) or email us on info@coverfox.com.</p>
<p>{%endif%}</p>
<p>Cheers!&nbsp;<br />Team Coverfox</p>"""
    mail.text = body
    mail.body = html_body
    mail.title = title
    mail.save()


def revert_motor_success_mail(apps, schema_editor):
    Mail = apps.get_model('core', 'Mail')
    mail = Mail.objects.get(mtype='MOTOR_TRANSACTION_SUCCESS_MAIL')
    html_body = """<p>Hi {{obj.name}},<br />Thank you so much for choosing Coverfox for your {{obj.vehicle_type}}&nbsp;Insurance. Your transaction was successful.</p>
<p>The details are -</p>
<table>
<tbody>
<tr><th>Insurance Company</th>
<td>{{obj.insurer_title}}</td>
</tr>
<tr><th>Premium</th>
<td>INR {{obj.premium}}</td>
</tr>
<tr><th>Transaction Number</th>
<td>{{obj.transaction_number}}</td>
</tr>
</tbody>
</table>
<p>{% if obj.policy_document_url %} You can download a soft copy of your policy here -</p>
<p><a class="btn btn-warning" href="http://www.coverfox.com{{ obj.policy_document_url }}" target="_blank">Download e-Policy</a>&nbsp;{%else%}</p>
<p>You will receive the original policy document through email within {{ obj.policy_tat }}.</p>
<p>In case you do not receive the PDF of the policy in the next {{ obj.policy_tat }}, call us on our Toll free number: <span>{%ifequal obj.vehicle_type 'Two Wheeler'%}1800-209-99-30{%else%}1800-209-99-30{%endifequal%}</span>&nbsp;(Available Monday - Saturday from 10 a.m -7 p.m) or mail us on info@coverfox.com</p>
<p>{%endif%}</p>
<p>Cheers!&nbsp;<br />Team Coverfox</p>"""
    body = """Hi {{obj.name}},

Thank you so much for choosing Coverfox for your {{obj.vehicle_type}} Insurance.

Your transaction was successful. The details are -

Insurance Company:  {{obj.insurer_title}}
Premium: INR {{obj.premium}}
Transaction Number:   {{obj.transaction_number}}

{% if obj.policy_document_url %}You can download the original policy document from {{ obj.policy_document_url }}
{% else %}
You will receive the original policy document through email within {{ obj.policy_tat }}.{% endif %}

In case you do not receive the PDF of  the policy in the next {{ obj.policy_tat }}, call us on our Toll Free number: {%ifequal obj.vehicle_type 'Two Wheeler'%}1800-209-99-30{%else%}1800-209-99-30{%endifequal%} (Available Monday - Saturday from 10 a.m - 7 p.m) or mail us on info@coverfox.com

Cheers!
Team Coverfox"""
    mail.text = body
    mail.body = html_body
    mail.save()


class Migration(migrations.Migration):
    dependencies = [
        ('core', '0012_new_motor_success_message'),
    ]

    operations = [
        migrations.RunPython(change_motor_success_mail, revert_motor_success_mail),
    ]
