# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_update_motor_template'),
        ('core', '0013_auto_20151109_2107'),
    ]

    operations = [
    ]
