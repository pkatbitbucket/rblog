# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requirement',
            name='activity',
            field=models.ForeignKey(blank=True, to='core.Activity', null=True),
        ),
    ]
