# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_add_policy_upload_notification_mail_template'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee',
            name='groups',
            field=models.ManyToManyField(related_query_name=b'employee', related_name='employee_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups'),
        ),
        migrations.AlterField(
            model_name='employee',
            name='user_permissions',
            field=models.ManyToManyField(related_query_name=b'employee', related_name='employee_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions'),
        ),
        migrations.AlterField(
            model_name='mail',
            name='mtype',
            field=models.CharField(max_length=255, choices=[(b'ON_SUBSCRIPTION', b'ON_SUBSCRIPTION (subscriber)'), (b'ON_REVIEW_SIGNUP', b'ON_REVIEW_SIGNUP (subscriber)'), (b'INVITE_MAIL', b'INVITE_MAIL (subscriber, invited_by)'), (b'TRANSACTION_SUCCESS_MAIL', b'TRANSACTION_SUCCESS_MAIL (obj)'), (b'TRANSACTION_FAILURE_MAIL', b'TRANSACTION_FAILURE_MAIL (obj)'), (b'TRANSACTION_DROP_MAIL', b'TRANSACTION_DROP_MAIL (obj)'), (b'RESULTS_DROP_MAIL', b'RESULTS_DROP_MAIL (obj)'), (b'RESULTS_DROP_MAIL2', b'RESULTS_DROP_MAIL2 (obj)'), (b'VERIFIED_WELCOME_MAIL', b'VERIFIED_WELCOME_MAIL (obj)'), (b'UNFERIFIED_WELCOME_MAIL', b'UNFERIFIED_WELCOME_MAIL (obj)'), (b'RESET_PASSWORD', b'RESET_PASSWORD (obj)'), (b'MOTOR_TRANSACTION_SUCCESS_MAIL', b'MOTOR_TRANSACTION_SUCCESS_MAIL (obj)'), (b'MOTOR_TRANSACTION_FAILURE_MAIL', b'MOTOR_TRANSACTION_FAILURE_MAIL (obj)'), (b'MOTOR_QUOTES_MAIL', b'MOTOR_QUOTES_MAIL (obj)'), (b'MOTOR_DISCOUNT_CODE_MAIL', b'MOTOR_DISCOUNT_CODE_MAIL (obj)'), (b'MOTOR_ADMIN_MAIL', b'MOTOR_ADMIN_MAIL (obj)'), (b'RSA_STEP2_VALID_INS', b'RSA_STEP2_VALID_INS (obj)'), (b'RSA_STEP2_EXPIRED_INS', b'RSA_STEP2_EXPIRED_INS (obj)'), (b'APPOINTMENT_ALERT', b'APPOINTMENT_ALERT (obj)'), (b'USER_ACCOUNT_CREATION', b'USER_ACCOUNT_CREATION'), (b'NON_RM_USER_ACCOUNT_CREATION', b'NON_RM_USER_ACCOUNT_CREATION (obj)'), (b'POLICY_UPLOAD_NOTIFICATION', b'POLICY_UPLOAD_NOTIFICATION')]),
        ),
    ]
