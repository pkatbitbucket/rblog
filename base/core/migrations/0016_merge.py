# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_add_policy_upload_notification_mail_template'),
        ('core', '0015_auto_20151113_1849'),
    ]

    operations = [
    ]
