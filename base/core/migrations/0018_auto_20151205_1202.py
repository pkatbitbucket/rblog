# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0017_auto_20151201_0049'),
    ]

    operations = [
        migrations.CreateModel(
            name='RequirementKind',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('kind', models.CharField(max_length=255, choices=[(b'new_policy', b'New Policy'), (b'cancellation', b'Cancellation'), (b'claim', b'Claim'), (b'Endorsement', ((b'addition', b'Addition'), (b'correction', b'Correction'), (b'change', b'Change'), (b'deletion', b'Deletion'), (b'recovery', b'Recovery')))])),
                ('sub_kind', models.CharField(blank=True, max_length=255, choices=[(b'EMAIL_ID', b'email_id'), (b'OWNER', b'owner'), (b'SALUTATION', b'salutation'), (b'OCCUPATION', b'occupation'), (b'PASSPORT_NO', b'passport_no'), (b'FUEL_TYPE', b'fuel_type'), (b'VEHICLE', b'vehicle'), (b'PED', b'ped'), (b'VOLUNTARY_DEDUCTABLE', b'voluntary_deductable'), (b'ENGINE_NO', b'engine_no'), (b'PASSENGER_COVER', b'passenger_cover'), (b'HYPOTHECATION', b'hypothecation'), (b'RELATIONSHIP', b'relationship'), (b'CONTACT_NO', b'contact_no'), (b'INSURED', b'insured'), (b'REGISTRATION_PLACE', b'registration_place'), (b'ADDRESS', b'address'), (b'NCB', b'ncb'), (b'NAME', b'name'), (b'POLICY_PERIOD', b'policy_period'), (b'DOB', b'dob'), (b'GENDER', b'gender'), (b'REGISTRATION_NO', b'registration_no'), (b'CHASSIS_NO', b'chassis_no'), (b'IDV', b'idv')])),
                ('product', models.CharField(max_length=255, choices=[(b'motor', b'Motor'), (b'health', b'Health'), (b'travel', b'Travel')])),
            ],
        ),
        migrations.AddField(
            model_name='requirement',
            name='remark',
            field=models.TextField(blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='requirementkind',
            unique_together=set([('kind', 'sub_kind', 'product')]),
        ),
        migrations.AddField(
            model_name='requirement',
            name='type',
            field=models.ForeignKey(to='core.RequirementKind', null=True),
        ),
    ]
