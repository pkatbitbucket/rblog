# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
import sys


def populate_type_from_kind(apps, schema_editor):
    Requirement = apps.get_model('core', 'Requirement')
    RequirementKind = apps.get_model('core', 'RequirementKind')

    health_requirements = Requirement.objects.filter(kind="new_health_insurance")
    req_kind, _ = RequirementKind.objects.get_or_create(kind='new_policy', product='health', sub_kind='')
    health_requirements.update(type=req_kind)

    motor_requirements = Requirement.objects.filter(kind='New Motor Insurance')
    req_kind, _ = RequirementKind.objects.get_or_create(kind='new_policy', product='motor', sub_kind='')
    motor_requirements.update(type=req_kind)

    remaining_requirements = Requirement.objects.filter(type__isnull=True)
    if remaining_requirements:
        sys.exit(
            'Unable to update type for requirements of following kinds\n{}'.format(
                ', '.join(remaining_requirements.values_list('kind', flat=True).distinct())
            )
        )


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0018_auto_20151205_1202'),
    ]

    operations = [
        migrations.RunPython(code=populate_type_from_kind, reverse_code=migrations.RunPython.noop)
    ]
