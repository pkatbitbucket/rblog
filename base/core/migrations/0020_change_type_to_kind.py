# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0019_populate_type_from_kind'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='requirement',
            name='kind',
        ),
        migrations.AlterField(
            model_name='requirement',
            name='type',
            field=models.ForeignKey(to='core.RequirementKind'),
        ),
        migrations.RenameField(
            model_name='requirement',
            old_name='type',
            new_name='kind',
        ),
        migrations.AlterField(
            model_name='requirement',
            name='payment',
            field=models.ManyToManyField(to='core.Payment', blank=True),
        ),
        migrations.AlterField(
            model_name='requirement',
            name='state',
            field=models.CharField(default=b'pending', max_length=100, verbose_name=b'status', choices=[(b'pending', b'pending'), (b'sold', b'sold'), (b'user_said_no', b'user refused'), (b'insurer_refused', b'Insurer refused')]),
        ),
    ]
