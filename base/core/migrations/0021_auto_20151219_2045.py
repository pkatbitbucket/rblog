# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('statemachine', '0002_auto_20151219_2045'),
        ('crm', '0002_auto_20151219_2045'),
        ('core', '0020_change_type_to_kind'),
    ]

    operations = [
        migrations.CreateModel(
            name='RequirementStateHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('enter_time', models.DateTimeField(default=datetime.datetime(2015, 12, 19, 20, 45, 43, 641485))),
                ('exit_time', models.DateTimeField(null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='requirement',
            name='activity',
        ),
        migrations.RemoveField(
            model_name='requirement',
            name='state',
        ),
        migrations.AddField(
            model_name='activity',
            name='requirement',
            field=models.ForeignKey(to='core.Requirement', null=True),
        ),
        migrations.AddField(
            model_name='policy',
            name='premium',
            field=models.DecimalField(null=True, max_digits=10, decimal_places=2, blank=True),
        ),
        migrations.AddField(
            model_name='requirement',
            name='crm_activities',
            field=models.ManyToManyField(to='crm.Activity', blank=True),
        ),
        migrations.AddField(
            model_name='requirement',
            name='machine',
            field=models.ForeignKey(to='statemachine.Machine', null=True),
        ),
        migrations.AddField(
            model_name='requirement',
            name='tasks',
            field=models.ManyToManyField(to='crm.Task', blank=True),
        ),
        migrations.AddField(
            model_name='requirementstatehistory',
            name='requirement',
            field=models.ForeignKey(to='core.Requirement'),
        ),
        migrations.AddField(
            model_name='requirementstatehistory',
            name='state',
            field=models.ForeignKey(to='statemachine.State'),
        ),
    ]
