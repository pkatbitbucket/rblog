# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('statemachine', '0002_auto_20151219_2045'),
        ('core', '0021_auto_20151219_2045'),
    ]

    operations = [
        migrations.AddField(
            model_name='requirement',
            name='state',
            field=models.ForeignKey(to='statemachine.State', null=True),
        ),
        migrations.AlterField(
            model_name='requirementstatehistory',
            name='enter_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 19, 20, 46, 27, 788410)),
        ),
    ]
