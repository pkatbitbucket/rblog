# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0022_auto_20151219_2046'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requirementstatehistory',
            name='enter_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 21, 15, 52, 44, 792630)),
        ),
    ]
