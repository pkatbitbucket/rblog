# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0023_auto_20151221_1552'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requirementstatehistory',
            name='enter_time',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
    ]
