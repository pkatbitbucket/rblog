# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0024_auto_20151223_1248'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requirementstatehistory',
            name='enter_time',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
