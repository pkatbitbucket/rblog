# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0024_auto_20151223_1248'),
    ]

    operations = [
        migrations.CreateModel(
            name='PolicyAmendment',
            fields=[
                ('document_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.Document')),
            ],
            options={
                'abstract': False,
            },
            bases=('core.document',),
        ),
        migrations.AddField(
            model_name='policy',
            name='policy_number',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='policy',
            name='status',
            field=models.CharField(default=b'valid', max_length=100, choices=[(b'valid', b'Policy is valid'), (b'cancelled', b'Policy is cancelled')]),
        ),
        migrations.AddField(
            model_name='policyamendment',
            name='parent_policy',
            field=models.ForeignKey(related_name='amendments', to='core.Policy'),
        ),
        migrations.AddField(
            model_name='policyamendment',
            name='requirement_sold',
            field=models.ForeignKey(related_name='amendments', to='core.Requirement', help_text=b'\n        This is the requirement that lead to the amendment.\n        '),
        ),
    ]
