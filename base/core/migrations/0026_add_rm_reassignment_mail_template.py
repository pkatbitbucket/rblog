# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def add_rm_reassignment_mail_template(apps, schema_editor):

    mtype = "RM_REASSIGNMENT"
    title = "Your New Coverfox Relationship Manager"

    html_body = """<div>Hello {{ obj.user.get_full_name }},</div>
<div>&nbsp;</div>
<div>Wish you a Merry Christmas and a Very Happy New Year.</div>
<div>&nbsp;</div>
<div>My name is {{ obj.rm.name }}, and I am your new Relationship Manager at Coverfox.&nbsp;</div>
<div>&nbsp;</div>
<div>For all things insurance, you can write to me or call me, and I will help you with those. For example, if you need to file claims or get information on a policy, please contact me, and I will coordinate with the insurance companies on your behalf.&nbsp;</div>
<div>&nbsp;</div>
<div>From&nbsp;<span class="aBn" data-term="goog_1293397956"><span class="aQJ">Monday</span></span>&nbsp;to&nbsp;<span class="aBn" data-term="goog_1293397957"><span class="aQJ">Saturday</span></span>&nbsp;you can reach me on&nbsp;<span>my mobile number <strong>{{ obj.rm.mobile }}</strong>&nbsp;between&nbsp;<span class="aBn" data-term="goog_1293397958"><span class="aQJ">10 am to 9 pm</span></span>. You can always send an email to&nbsp;</span><span>my email address <strong>{{ obj.rm.email }}</strong>, or call us toll-free at <strong>1800 209 9970</strong>&nbsp;for immediate assistance.</span></div>
<div><span>&nbsp;</span></div>
<div>Please save the phone numbers on your phone so that you can reach me whenever you needed.</div>
<div>&nbsp;</div>
<div>Best Regards,</div>
<div>{{ obj.rm.name }}</div>"""

    text_body = """Hello {{ obj.user.get_full_name }},

Wish you a Merry Christmas and a Very Happy New Year.

My name is {{ obj.rm.name }}, and I am your new Relationship Manager at Coverfox.

For all things insurance, you can write to me or call me, and I will help you with those. For example, if you need to file claims or get information on a policy, please contact me, and I will coordinate with the insurance companies on your behalf.

From Monday to Saturday you can reach me on my mobile number {{ obj.rm.mobile }} between 10 am to 9 pm. You can always send an email to my email address {{ obj.rm.email }}, or call us toll-free at 1800 209 9970 for immediate assistance.

Please save the phone numbers on your phone so that you can reach me whenever you needed.

Best Regards,
{{ obj.rm.name }}"""

    Mail = apps.get_model('core', 'Mail')

    try:
        Mail.objects.get(mtype=mtype)
    except Mail.DoesNotExist:
        Mail.objects.create(
            title=title,
            mtype=mtype,
            body=html_body,
            text=text_body,
            is_active=True
        )


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0025_auto_20151223_2034'),
    ]

    operations = [
    	migrations.RunPython(
    		code=add_rm_reassignment_mail_template,
    		reverse_code=migrations.RunPython.noop,
		)
    ]