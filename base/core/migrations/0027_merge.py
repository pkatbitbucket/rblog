# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0026_add_rm_reassignment_mail_template'),
        ('core', '0025_auto_20151223_2256'),
    ]

    operations = [
    ]
