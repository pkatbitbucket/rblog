# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0027_merge'),
        ('core', '0018_merge'),
    ]

    operations = [
    ]
