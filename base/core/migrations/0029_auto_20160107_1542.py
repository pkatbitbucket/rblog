# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0028_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='FinPolicy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('policy_number', models.CharField(max_length=100)),
                ('start_date', models.DateField(null=True, blank=True)),
                ('end_date', models.DateField(null=True, blank=True)),
                ('gross_premium', models.FloatField(null=True, blank=True)),
                ('policy_holder_name', models.CharField(max_length=200, blank=True)),
                ('product_name', models.CharField(max_length=100, blank=True)),
                ('insurer_company', models.CharField(max_length=100, blank=True)),
                ('reconciled', models.BooleanField(default=False)),
                ('filename', models.CharField(max_length=100)),
                ('extra', models.TextField()),
            ],
        ),
    ]
