# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0029_auto_20160107_1542'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mail',
            name='mtype',
            field=models.CharField(max_length=255, choices=[(b'ON_SUBSCRIPTION', b'ON_SUBSCRIPTION (subscriber)'), (b'ON_REVIEW_SIGNUP', b'ON_REVIEW_SIGNUP (subscriber)'), (b'INVITE_MAIL', b'INVITE_MAIL (subscriber, invited_by)'), (b'TRANSACTION_SUCCESS_MAIL', b'TRANSACTION_SUCCESS_MAIL (obj)'), (b'TRANSACTION_FAILURE_MAIL', b'TRANSACTION_FAILURE_MAIL (obj)'), (b'TRANSACTION_DROP_MAIL', b'TRANSACTION_DROP_MAIL (obj)'), (b'RESULTS_DROP_MAIL', b'RESULTS_DROP_MAIL (obj)'), (b'RESULTS_DROP_MAIL2', b'RESULTS_DROP_MAIL2 (obj)'), (b'VERIFIED_WELCOME_MAIL', b'VERIFIED_WELCOME_MAIL (obj)'), (b'UNFERIFIED_WELCOME_MAIL', b'UNFERIFIED_WELCOME_MAIL (obj)'), (b'RESET_PASSWORD', b'RESET_PASSWORD (obj)'), (b'MOTOR_TRANSACTION_SUCCESS_MAIL', b'MOTOR_TRANSACTION_SUCCESS_MAIL (obj)'), (b'MOTOR_TRANSACTION_FAILURE_MAIL', b'MOTOR_TRANSACTION_FAILURE_MAIL (obj)'), (b'MOTOR_QUOTES_MAIL', b'MOTOR_QUOTES_MAIL (obj)'), (b'MOTOR_DISCOUNT_CODE_MAIL', b'MOTOR_DISCOUNT_CODE_MAIL (obj)'), (b'MOTOR_ADMIN_MAIL', b'MOTOR_ADMIN_MAIL (obj)'), (b'RSA_STEP2_VALID_INS', b'RSA_STEP2_VALID_INS (obj)'), (b'RSA_STEP2_EXPIRED_INS', b'RSA_STEP2_EXPIRED_INS (obj)'), (b'APPOINTMENT_ALERT', b'APPOINTMENT_ALERT (obj)'), (b'USER_ACCOUNT_CREATION', b'USER_ACCOUNT_CREATION'), (b'NON_RM_USER_ACCOUNT_CREATION', b'NON_RM_USER_ACCOUNT_CREATION (obj)'), (b'POLICY_UPLOAD_NOTIFICATION', b'POLICY_UPLOAD_NOTIFICATION'), (b'RM_REASSIGNMENT', b'RM_REASSIGNMENT')]),
        ),
    ]
