# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0029_auto_20160107_1542'),
    ]

    operations = [
        migrations.AddField(
            model_name='finpolicy',
            name='mobile',
            field=models.CharField(max_length=20, blank=True),
        ),
        migrations.AddField(
            model_name='finpolicy',
            name='net_premium',
            field=models.FloatField(null=True, blank=True),
        ),
    ]
