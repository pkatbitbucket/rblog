# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0030_auto_20160109_2105'),
    ]

    operations = [
        migrations.AddField(
            model_name='finpolicy',
            name='registration_no',
            field=models.CharField(max_length=20, null=True),
        ),
    ]
