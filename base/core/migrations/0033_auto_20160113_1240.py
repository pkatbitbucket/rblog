# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0032_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='document',
            field=models.FileField(upload_to=core.models.get_document_upload_path, blank=True),
        ),
        migrations.AlterField(
            model_name='policy',
            name='expiry_date',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='policy',
            name='issue_date',
            field=models.DateField(null=True, blank=True),
        ),
    ]
