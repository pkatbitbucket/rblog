# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.models
import django.contrib.postgres.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('statemachine', '0002_auto_20151219_2045'),
        ('core', '0032_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='Email',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=254)),
                ('is_primary', models.BooleanField(default=False)),
                ('is_exists', models.BooleanField(default=False)),
                ('is_bogus', models.BooleanField(default=False)),
                ('is_verified', models.BooleanField(default=False)),
                ('verification_code', models.CharField(max_length=25, null=True)),
                ('user', models.ForeignKey(related_name='emails', to=settings.AUTH_USER_MODEL, null=True)),
                ('visitor', models.ForeignKey(related_name='emails', to='core.Tracker', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('nid', models.CharField(unique=True, max_length=100)),
                ('about', models.TextField(blank=True)),
                ('to_emails', django.contrib.postgres.fields.ArrayField(default=[], base_field=models.EmailField(max_length=254), size=None)),
                ('subject', models.CharField(blank=True, max_length=100, validators=[core.models.template_exists])),
                ('subject_content', models.TextField(blank=True)),
                ('html', models.CharField(blank=True, max_length=100, validators=[core.models.template_exists])),
                ('html_content', models.TextField(blank=True)),
                ('text', models.CharField(blank=True, max_length=100, validators=[core.models.template_exists])),
                ('text_content', models.TextField(blank=True)),
                ('sms', models.CharField(blank=True, max_length=100, validators=[core.models.template_exists])),
                ('sms_content', models.TextField(blank=True)),
                ('to_mobiles', django.contrib.postgres.fields.ArrayField(default=[], base_field=models.CharField(max_length=10), size=None)),
                ('mta', models.CharField(default=b'mandrill', max_length=100)),
                ('is_template', models.BooleanField(default=False, help_text=b'This tells whether an object is used as a template or not')),
                ('creator', models.ForeignKey(related_name='core_notification_created', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('editor', models.ForeignKey(related_name='core_notification_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Phone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.CharField(max_length=20)),
                ('is_primary', models.BooleanField(default=False)),
                ('is_exists', models.BooleanField(default=False)),
                ('is_bogus', models.BooleanField(default=False)),
                ('is_verified', models.BooleanField(default=False)),
                ('verification_code', models.CharField(max_length=8, null=True)),
                ('user', models.ForeignKey(related_name='phones', to=settings.AUTH_USER_MODEL, null=True)),
                ('visitor', models.ForeignKey(related_name='phones', to='core.Tracker', null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='requirement',
            name='machine',
        ),
        migrations.AddField(
            model_name='marketing',
            name='label',
            field=models.CharField(help_text=b'\n        ', max_length=100, blank=True),
        ),
        migrations.AddField(
            model_name='requirement',
            name='visitor',
            field=models.ForeignKey(blank=True, to='core.Tracker', null=True),
        ),
        migrations.AddField(
            model_name='requirementkind',
            name='machine',
            field=models.ForeignKey(to='statemachine.Machine', null=True),
        ),
        migrations.AlterField(
            model_name='marketing',
            name='device',
            field=models.CharField(default=b'desktop', help_text=b'Name of device, eg desktop', max_length=40, blank=True, choices=[(b'mobile', b'Mobile'), (b'desktop', b'Desktop'), (b'mobile web', b'Mobile Web')]),
        ),
        migrations.AlterField(
            model_name='marketing',
            name='network',
            field=models.CharField(help_text=b'\n            Network that sent this traffic. Examples: facebook, google,\n            mailchimp.\n        ', max_length=100, blank=True),
        ),
        migrations.AlterField(
            model_name='requirementkind',
            name='product',
            field=models.CharField(max_length=255, choices=[(b'motor', b'Motor'), (b'health', b'Health'), (b'bike', b'Bike'), (b'travel', b'Travel'), (b'term', b'Term'), (b'home', b'Home'), (b'life', b'Life')]),
        ),
    ]
