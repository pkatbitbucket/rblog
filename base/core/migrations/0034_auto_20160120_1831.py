# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import core.fields


def migrate_userphone_to_new_model(apps, schema_editor):
    User = apps.get_model('core', 'User')
    Phone = apps.get_model('core', 'Phone')
    for user in User.objects.filter(phone__isnull=False).exclude(phone=''):
        p = Phone.objects.create(
            user=user,
            number=user.phone,
            is_primary=True,
            is_exists=True,
            is_bogus=True,
            is_verified=True,
        )


def migrate_useremail_to_new_model(apps, schema_editor):
    User = apps.get_model('core', 'User')
    Email = apps.get_model('core', 'Email')
    for user in User.objects.filter(email__isnull=False).exclude(email=''):
        p = Email.objects.create(
            user=user,
            email=user.email,
            is_primary=True,
            is_exists=True,
            is_bogus=True,
            is_verified=True,
        )


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0033_auto_20160114_1248'),
    ]

    operations = [
        migrations.AlterField(
            model_name='email',
            name='email',
            field=core.fields.CIEmailField(max_length=255, verbose_name='e-mail address', db_index=True),
        ),
        migrations.RunPython(
            code=migrate_userphone_to_new_model,
            reverse_code=migrations.RunPython.noop,
        ),
        migrations.RunPython(
            code=migrate_useremail_to_new_model,
            reverse_code=migrations.RunPython.noop,
        ),
        migrations.RemoveField(
            model_name='user',
            name='phone',
        ),
        migrations.RemoveField(
            model_name='user',
            name='username',
        ),
    ]
