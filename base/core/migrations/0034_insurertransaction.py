# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0033_auto_20160113_1240'),
    ]

    operations = [
        migrations.CreateModel(
            name='InsurerTransaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('proposal_number', models.CharField(max_length=250, blank=True)),
                ('policy_number', models.CharField(max_length=250, blank=True)),
                ('reference_id', models.CharField(max_length=250, blank=True)),
                ('policy_token', models.CharField(max_length=250, blank=True)),
                ('payment_token', models.CharField(max_length=250, blank=True)),
                ('premium_insurance', models.CharField(max_length=10, blank=True)),
                ('premium_paid', models.CharField(max_length=10, blank=True)),
                ('raw', jsonfield.fields.JSONField(default={}, verbose_name='raw', blank=True)),
                ('policy_request', models.TextField(blank=True)),
                ('policy_response', models.TextField(blank=True)),
                ('payment_request', models.TextField(blank=True)),
                ('payment_response', models.TextField(blank=True)),
                ('payment_redirect_response', models.TextField(blank=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('fd', models.ForeignKey(to='core.FormData')),
                ('requirement', models.ForeignKey(to='core.Requirement')),
            ],
        ),
    ]
