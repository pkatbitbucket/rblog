# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def update_policy_upload_notification_mail_template(apps, schema_editor):
    mail_model = apps.get_model('core', 'Mail')

    try:
        mail = mail_model.objects.get(mtype='POLICY_UPLOAD_NOTIFICATION')
    except mail_model.DoesNotExist:
        pass
    else:
        title = "Your {{ obj.transaction_type|title }} Insurance policy document is added to your Coverfox Account"

        html_body = """<p>Dear {{ obj.user.get_full_name }},</p>
<p>Thank you for choosing Coverfox for your {{ obj.transaction_type|title }} Insurance and trusting Coverfox once again.</p>
<p>We are delighted to have you back and promise to continue making things very simple for you.</p>
<p>I, {{ obj.rm.name }}, am honoured to be your Relationship Manager and await the opportunity to assist you with all your insurance needs.</p>
<p>Your personalized Coverfox Account is already active and includes all your insurance policies for easy and quick access to them. You can access your Account using the &ldquo;MANAGE POLICIES&rdquo; link on the top right corner of our website.</p>
<p>Here is what you need to do:</p>
<p>1. Go to {{ obj.user_login_url }}.</p>
<p>2. Login to your account.</p>
<p>3. View and manage all your existing and future policies.</p>
<p>From your account you can:<br />a. Download your policy<br />b. View your policy details<br />c. Initiate a claim for your policy</p>
<p>{% if obj.rsa_policy %}<br />We have also provided you a FREE 24/7 ROADSIDE ASSISTANCE POLICY to handle all your worries in case of any emergency breakdown. The 24/7 RSA Policy can be accessed from Your Account. The detailed features are mentioned on the policy copy.<br />{% endif %}</p>
<p>Please feel free to contact me if you need clarifications regarding your policy, or to make a claim or if you need help to renew or buy a policy. You can also reach out to me to give any feedback or just to say hello.</p>
<p>I can be reached on {{ obj.rm.mobile }} between 10am to 7pm from Monday to Saturday. You can drop me a mail on {{ obj.rm.email }}.</p>
<p>Alternately in case you are not able to contact me you can call us on 1800 209 9970.</p>
<p>We would be happy to assist you with {{ obj.other_products|join:', '|title }}&nbsp;insurances too.</p>
<p>Extending the assurance of the best insurance and services always.</p>"""

        text_body = """Dear {{ obj.user.get_full_name }},

Thank you for choosing Coverfox for your {{ obj.transaction_type|title }} Insurance and trusting Coverfox once again.

We are delighted to have you back and promise to continue making things very simple for you.

I, {{ obj.rm.name }}, am honoured to be your Relationship Manager and await the opportunity to assist you with all your insurance needs.

Your personalized Coverfox Account is already active and includes all your insurance policies for easy and quick access to them. You can access your Account using the “MANAGE POLICIES” link on the top right corner of our website.

Here is what you need to do:

1. Go to {{ obj.user_login_url }}.

2. Login to your account.

3. View and manage all your existing and future policies.

From your account you can:
a. Download your policy
b. View your policy details
c. Initiate a claim for your policy

{% if obj.rsa_policy %}
We have also provided you a FREE 24/7 ROADSIDE ASSISTANCE POLICY to handle all your worries in case of any emergency breakdown. The 24/7 RSA Policy can be accessed from Your Account. The detailed features are mentioned on the policy copy.
{% endif %}

Please feel free to contact me if you need clarifications regarding your policy, or to make a claim or if you need help to renew or buy a policy. You can also reach out to me to give any feedback or just to say hello.

I can be reached on {{ obj.rm.mobile }} between 10am to 7pm from Monday to Saturday. You can drop me a mail on {{ obj.rm.email }}.

Alternately in case you are not able to contact me you can call us on 1800 209 9970.

We would be happy to assist you with {{ obj.other_products|join:', '|title }} insurances too.

Extending the assurance of the best insurance and services always."""

        mail.title = title
        mail.body = html_body
        mail.text = text_body
        mail.save()


class Migration(migrations.Migration):
    dependencies = [
        ('core', '0033_auto_20160114_1248'),
    ]

    operations = [
        migrations.RunPython(
            code=update_policy_upload_notification_mail_template,
            reverse_code=migrations.RunPython.noop,
        )
    ]
