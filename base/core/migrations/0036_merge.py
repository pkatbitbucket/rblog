# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0034_update_policy_upload_notification_mail_template'),
        ('core', '0035_auto_20160121_0005'),
        ('core', '0034_insurertransaction'),
    ]

    operations = [
    ]
