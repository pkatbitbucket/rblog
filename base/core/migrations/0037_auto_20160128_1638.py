# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0036_merge'),
    ]

    operations = [
        migrations.RenameField(
            model_name='notification',
            old_name='to_emails',
            new_name='to',
        ),
        # migrations.RemoveField(
        #     model_name='notification',
        #     name='creator',
        # ),
        # migrations.RemoveField(
        #     model_name='notification',
        #     name='editor',
        # ),
        migrations.RemoveField(
            model_name='notification',
            name='to_mobiles',
        ),
        migrations.AddField(
            model_name='notification',
            name='sync',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='notification',
            name='numbers',
            field=django.contrib.postgres.fields.ArrayField(default=[], base_field=models.CharField(max_length=12), size=None),
        ),
        migrations.AddField(
            model_name='notification',
            name='priority',
            field=models.CharField(default=b'normal', max_length=10, choices=[(b'high', b'High'), (b'normal', b'Normal'), (b'low', b'Low')]),
        ),
        migrations.AddField(
            model_name='notification',
            name='reply_to',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='notification',
            name='sender_email',
            field=models.CharField(default=b'dev@cfstatic.org', max_length=100),
        ),
        migrations.AddField(
            model_name='notification',
            name='sender_name',
            field=models.CharField(default=b'Team Coverfox', max_length=100),
        ),
        migrations.AlterField(
            model_name='notification',
            name='mta',
            field=models.CharField(default=b'mandrill', max_length=100, choices=[(b'mandrill', b'Mandrill Api')]),
        ),
    ]
