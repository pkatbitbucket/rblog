# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings

from django.db import migrations


def mail_to_notifications(apps, schema_editor):
    MAIL_CHOICES = (
        ('ON_SUBSCRIPTION', 'ON_SUBSCRIPTION (subscriber)'),
        ('ON_REVIEW_SIGNUP', 'ON_REVIEW_SIGNUP (subscriber)'),
        ('INVITE_MAIL', 'INVITE_MAIL (subscriber, invited_by)'),
        ('TRANSACTION_SUCCESS_MAIL', 'TRANSACTION_SUCCESS_MAIL (obj)'),
        ('TRANSACTION_FAILURE_MAIL', 'TRANSACTION_FAILURE_MAIL (obj)'),
        ('TRANSACTION_DROP_MAIL', 'TRANSACTION_DROP_MAIL (obj)'),
        ('RESULTS_DROP_MAIL', 'RESULTS_DROP_MAIL (obj)'),
        ('RESULTS_DROP_MAIL2', 'RESULTS_DROP_MAIL2 (obj)'),
        ('VERIFIED_WELCOME_MAIL', 'VERIFIED_WELCOME_MAIL (obj)'),
        ('UNVERIFIED_WELCOME_MAIL', 'UNVERIFIED_WELCOME_MAIL (obj)'),
        ('RESET_PASSWORD', 'RESET_PASSWORD (obj)'),
        (
            'MOTOR_TRANSACTION_SUCCESS_MAIL',
            'MOTOR_TRANSACTION_SUCCESS_MAIL (obj)'
        ),
        (
            'MOTOR_TRANSACTION_FAILURE_MAIL',
            'MOTOR_TRANSACTION_FAILURE_MAIL (obj)'
        ),
        ('MOTOR_QUOTES_MAIL', 'MOTOR_QUOTES_MAIL (obj)'),
        ('MOTOR_DISCOUNT_CODE_MAIL', 'MOTOR_DISCOUNT_CODE_MAIL (obj)'),
        ('MOTOR_ADMIN_MAIL', 'MOTOR_ADMIN_MAIL (obj)'),
        ('RSA_STEP2_VALID_INS', 'RSA_STEP2_VALID_INS (obj)'),
        ('RSA_STEP2_EXPIRED_INS', 'RSA_STEP2_EXPIRED_INS (obj)'),
        ('APPOINTMENT_ALERT', 'APPOINTMENT_ALERT (obj)'),
        ('USER_ACCOUNT_CREATION', 'USER_ACCOUNT_CREATION'),
        ('NON_RM_USER_ACCOUNT_CREATION', 'NON_RM_USER_ACCOUNT_CREATION (obj)'),
        ('POLICY_UPLOAD_NOTIFICATION', 'POLICY_UPLOAD_NOTIFICATION'),
        ('RM_REASSIGNMENT', 'RM_REASSIGNMENT'),
    )

    Notification = apps.get_model('core', 'Notification')
    Mail = apps.get_model('core', 'Mail')

    print "\nMigrating Mail --> Notification"

    for mail in Mail.objects.all():
        print mail.mtype
        notification = Notification(
            nid=mail.mtype,
            about=dict(MAIL_CHOICES).get(mail.mtype),
            subject_content=mail.title,
            html_content=mail.body,
            text_content=mail.text,
            created_date=mail.created
        )
        notification.save()


def update_notifications(apps, schema_editor):
    Notification = apps.get_model('core', 'Notification')

    for nf in Notification.objects.all():
        if nf.nid == "ON_SUBSCRIPTION":
            nf.sender_name = 'Team Coverfox'
            nf.sender_email = 'info@coverfox.com'
            nf.reply_to = 'info@coverfox.com'
            nf.save()

        elif nf.nid == "ON_REVIEW_SIGNUP":
            nf.sender_name = 'Team Coverfox'
            nf.sender_email = 'info@coverfox.com'
            nf.reply_to = 'info@coverfox.com'
            nf.save()

        elif nf.nid == "INVITE_MAIL":
            nf.sender_name = 'Team Coverfox'
            nf.sender_email = 'info@coverfox.com'
            nf.reply_to = 'info@coverfox.com'
            nf.save()

        elif nf.nid == "TRANSACTION_SUCCESS_MAIL":
            nf.sender_name = 'Team Coverfox'
            nf.sender_email = 'info@coverfox.com'
            nf.reply_to = 'info@coverfox.com'
            nf.save()

        elif nf.nid == "TRANSACTION_FAILURE_MAIL":
            nf.sender_name = 'Team Coverfox'
            nf.sender_email = 'info@coverfox.com'
            nf.reply_to = 'info@coverfox.com'
            nf.save()

        elif nf.nid == "TRANSACTION_DROP_MAIL":
            nf.sender_email = 'no-reply@coverfox.com'
            nf.save()

        elif nf.nid == "RESULTS_DROP_MAIL2":
            nf.sender_email = 'no-reply@coverfox.com'
            nf.save()

        elif nf.nid == "VERIFIED_WELCOME_MAIL":
            nf.sender_name = 'Varun from Coverfox'
            nf.sender_email = 'varun@coverfox.com'
            nf.reply_to = 'varun@coverfox.com'
            nf.save()

        elif nf.nid == "UNVERIFIED_WELCOME_MAIL":
            nf.sender_name = 'Varun from Coverfox'
            nf.sender_email = 'varun@coverfox.com'
            nf.reply_to = 'varun@coverfox.com'
            nf.save()

        elif nf.nid == "RESET_PASSWORD":
            nf.sender_name = 'Team Coverfox'
            nf.sender_email = 'no-reply@coverfox.com'
            nf.save()

        elif nf.nid == "MOTOR_TRANSACTION_SUCCESS_MAIL":
            pass

        elif nf.nid == "MOTOR_QUOTES_MAIL":
            pass

        elif nf.nid == "MOTOR_DISCOUNT_CODE_MAIL":
            pass

        elif nf.nid == "MOTOR_ADMIN_MAIL":
            pass

        elif nf.nid == "RSA_STEP2_VALID_INS":
            nf.sender_email = 'cbs@coverfox.com'
            nf.reply_to = 'cbs@coverfox.com'
            nf.sms_content = 'Congratulations on your successful registration of the Free Car Breakdown ' \
                             'service by Coverfox. To get the breakdown policy document, please send your ' \
                             'valid car insurance copy on {{obj.health_email}} from your registered email id. ' \
                             'A valid car insurance is mandatory to activate the breakdown service.'
            nf.save()

        elif nf.nid == "RSA_STEP2_EXPIRED_INS":
            nf.sender_email = 'cbs@coverfox.com'
            nf.reply_to = 'cbs@coverfox.com'
            nf.sms_content = 'Congratulations on your successful registration of the Free Car Breakdown ' \
                             'service by Coverfox. Please renew your expired car insurance for activation of ' \
                             'services. Call {{obj.health_toll_free}} to speak to our car insurance advisors ' \
                             'and renew your expired insurance without any hassles.'
            nf.save()

        elif nf.nid == "APPOINTMENT_ALERT":
            nf.about = 'Send an sms to candidate about appointment confirmation and send email to recruiters.'
            nf.sms_content = 'Thank you for showing interest in being a part of Coverfox! Your apointment ' \
                             'has been confirmed on {{obj.msg.appointment_date}} between {{obj.msg.slot}}. ' \
                             'We look forward to meeting you. Our address is: Coverfox, 3rd Floor, B-Wing, ' \
                             'Krislon House, Saki Vihar Road, Opposite Marwah Center, Saki Naka, Andheri (E). ' \
                             'Feel free to call {{obj.msg.contact_person}} from our team on ' \
                             '{{obj.msg.contact_number}} in case of any concerns or if you have trouble finding us.'
            nf.to = ['varun.salian@coverfoxmail.com', 'prajaktta@coverfoxmail.com', 'deepika.salian@coverfox.com']
            nf.save()

        elif nf.nid == "USER_ACCOUNT_CREATION":
            nf.to = ['policydump@coverfox.com']
            nf.save()

        elif nf.nid == "NON_RM_USER_ACCOUNT_CREATION":
            nf.sender_name = 'Coverfox'
            nf.sender_email = 'no-reply@coverfox.com'
            nf.save()

        elif nf.nid == "POLICY_UPLOAD_NOTIFICATION":
            pass

        elif nf.nid == "RM_REASSIGNMENT":
            nf.to = ['policydump@coverfox.com']
            nf.save()


def new_notifications(apps, schema_editor):
    Notification = apps.get_model('core', 'Notification')

    notification = Notification(
        nid='REGISTRATION_OTP_SMS',
        about='OTP for registration.',
        sms_content='Dear Customer, Thank you for showing interest in Coverfox.com. ' \
                    'Your One Time Password (OTP) for completing your registration is {{obj.otp}}. ' \
                    'It is usable once and valid for {{obj.validity}} from the request. ' \
                    'Please do not share OTP with anyone.'
    )
    notification.save()

    notification = Notification(
        nid='bulk_notification_status',
        about="Email for the status of email and sms sent.",
        to=settings.ADMINS
    )
    notification.save()


class Migration(migrations.Migration):
    dependencies = [
        ('core', '0037_auto_20160128_1638'),
    ]

    operations = [
        migrations.RunPython(mail_to_notifications, reverse_code=migrations.RunPython.noop),
        migrations.RunPython(update_notifications, reverse_code=migrations.RunPython.noop),
        migrations.RunPython(new_notifications, reverse_code=migrations.RunPython.noop)
    ]
