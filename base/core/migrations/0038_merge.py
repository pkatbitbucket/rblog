# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0035_remove_user_email'),
        ('core', '0037_auto_20160123_1247'),
    ]

    operations = [
    ]
