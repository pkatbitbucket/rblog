# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0038_merge'),
    ]

    operations = [
        migrations.RunSQL(
            """
                ALTER TABLE
                    core_email
                DROP CONSTRAINT IF EXISTS
                    core_email_email_2ccef2978257a302_uniq
            """,
        ),
    ]
