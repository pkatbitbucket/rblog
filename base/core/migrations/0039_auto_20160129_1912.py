# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django_pgjson.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0002_auto_20151219_2045'),
        ('statemachine', '0003_auto_20160129_1912'),
        ('core', '0038_merge'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='marketing',
            name='device',
        ),
        migrations.RemoveField(
            model_name='marketing',
            name='label',
        ),
        migrations.RemoveField(
            model_name='marketing',
            name='network',
        ),
        migrations.AddField(
            model_name='activity',
            name='browser',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='city',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='device',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='device_model',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='fd',
            field=models.ForeignKey(to='core.FormData', null=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='feed_item',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='form_name',
            field=models.CharField(help_text=b'\n            Examples: car_new_quote_form, car_lead_form, car_proposal_form, etc\n        ', max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='form_position',
            field=models.CharField(help_text=b'\n            Examples: hero, benefits, etc\n        ', max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='form_variant',
            field=models.CharField(help_text=b'\n            Examples: home, lp1, fast_lane\n        ', max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='gclid',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='ip_address',
            field=models.GenericIPAddressField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='isp',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='label',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='landing_url',
            field=models.URLField(max_length=2048, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='match_type',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='os',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='page_id',
            field=models.CharField(help_text=b'\n            Examples: home, lp1, car_new_thankyou, etc\n        ', max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='placement',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='product',
            field=models.CharField(help_text=b'\n            should always start with product mentioned in RequirementKind\n            Examples: car_new, car_renewal, health, health_cancer\n        ', max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='referer',
            field=models.URLField(max_length=2048, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='request_id',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='state',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='target',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='term',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='url',
            field=models.URLField(max_length=2048, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='marketing',
            name='brand',
            field=models.CharField(help_text=b'\n            When category is competition. Examples: religare, icici, etc\n        ', max_length=100, blank=True),
        ),
        migrations.AddField(
            model_name='marketing',
            name='category',
            field=models.CharField(help_text=b'\n            Examples: aggregator, brand, competition, etc\n        ', max_length=100, blank=True),
        ),
        migrations.AddField(
            model_name='marketing',
            name='content',
            field=models.CharField(help_text=b'\n            Different graphics\n        ', max_length=100, blank=True),
        ),
        migrations.AddField(
            model_name='marketing',
            name='description',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='marketing',
            name='medium',
            field=models.CharField(help_text=b'\n            Examples: affiliate, organic, cpc, banner, etc\n        ', max_length=100, blank=True),
        ),
        migrations.AddField(
            model_name='marketing',
            name='source',
            field=models.CharField(help_text=b'\n            Source of this traffic. Examples: facebook, google, etc\n        ', max_length=100, blank=True),
        ),
        migrations.AddField(
            model_name='notification',
            name='creator',
            field=models.ForeignKey(related_name='core_notification_created', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='notification',
            name='editor',
            field=models.ForeignKey(related_name='core_notification_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='requirement',
            name='current_state',
            field=models.ForeignKey(related_name='requirements', to='statemachine.State', null=True),
        ),
        migrations.AddField(
            model_name='requirement',
            name='extra',
            field=django_pgjson.fields.JsonBField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='requirement',
            name='last_crm_activity',
            field=models.ForeignKey(related_name='last_for_requirements', blank=True, to='crm.Activity', null=True),
        ),
        migrations.AddField(
            model_name='requirement',
            name='last_web_activity',
            field=models.ForeignKey(related_name='last_for_requirements', blank=True, to='core.Activity', null=True),
        ),
        migrations.AddField(
            model_name='requirement',
            name='next_crm_task',
            field=models.ForeignKey(related_name='next_for_requirements', blank=True, to='crm.Task', null=True),
        ),
    ]
