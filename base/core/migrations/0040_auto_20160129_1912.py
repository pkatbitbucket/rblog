# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('statemachine', '0003_auto_20160129_1912'),
        ('crm', '0003_auto_20160129_1912'),
        ('core', '0039_auto_20160129_1912'),
    ]

    operations = [
        migrations.AddField(
            model_name='requirement',
            name='owner',
            field=models.ForeignKey(related_name='requirements_owned', blank=True, to='crm.Advisor', null=True),
        ),
        migrations.AddField(
            model_name='requirement',
            name='state_history',
            field=models.ManyToManyField(to='statemachine.State', through='core.RequirementStateHistory'),
        ),
        migrations.AddField(
            model_name='requirementkind',
            name='flow',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='activity',
            name='activity_type',
            field=models.CharField(help_text=b'\n            Examples: viewed, dropped, email_focus, email_change, etc\n        ', max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='requirement',
            name='crm_activities',
            field=models.ManyToManyField(related_name='requirements', to='crm.Activity', blank=True),
        ),
        migrations.AlterField(
            model_name='requirement',
            name='state',
            field=models.ForeignKey(related_name='old_state_requirements', to='statemachine.State', null=True),
        ),
        migrations.AlterField(
            model_name='requirement',
            name='tasks',
            field=models.ManyToManyField(related_name='requirements', to='crm.Task', blank=True),
        ),
        migrations.AlterField(
            model_name='requirement',
            name='user',
            field=models.ForeignKey(related_name='requirements', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='requirement',
            name='visitor',
            field=models.ForeignKey(related_name='requirements', blank=True, to='core.Tracker', null=True),
        ),
    ]
