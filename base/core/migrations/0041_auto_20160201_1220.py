# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0040_auto_20160129_1912'),
    ]

    operations = [
        migrations.AddField(
            model_name='requirement',
            name='campaigndata',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='requirement',
            name='fixed',
            field=models.NullBooleanField(),
        ),
    ]
