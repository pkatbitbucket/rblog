# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0041_auto_20160130_1515'),
    ]

    operations = [
        migrations.AddField(
            model_name='activity',
            name='mid',
            field=models.ForeignKey(blank=True, to='core.Marketing', null=True),
        ),
    ]
