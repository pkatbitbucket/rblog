# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0041_auto_20160201_1220'),
        ('core', '0039_auto_20160129_1801'),
        ('core', '0037_mail_to_notifications'),
        ('core', '0040_auto_20160129_1912'),
    ]

    operations = [
    ]
