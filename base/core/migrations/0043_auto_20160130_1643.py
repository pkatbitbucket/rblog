# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0042_activity_mid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='marketing',
            name='brand',
            field=models.CharField(help_text=b'\n            When category is competition. Examples: religare, icici, etc\n        ', max_length=100, db_index=True, blank=True),
        ),
        migrations.AlterField(
            model_name='marketing',
            name='campaign',
            field=models.CharField(help_text=b'\n            The campaign id that brought this traffic. This can be google ad id,\n            or email blast id etc.\n        ', max_length=100, db_index=True, blank=True),
        ),
        migrations.AlterField(
            model_name='marketing',
            name='category',
            field=models.CharField(help_text=b'\n            Examples: aggregator, brand, competition, etc\n        ', max_length=100, db_index=True, blank=True),
        ),
        migrations.AlterField(
            model_name='marketing',
            name='content',
            field=models.CharField(help_text=b'\n            Different graphics\n        ', max_length=100, db_index=True, blank=True),
        ),
        migrations.AlterField(
            model_name='marketing',
            name='medium',
            field=models.CharField(help_text=b'\n            Examples: affiliate, organic, cpc, banner, etc\n        ', max_length=100, db_index=True, blank=True),
        ),
        migrations.AlterField(
            model_name='marketing',
            name='source',
            field=models.CharField(help_text=b'\n            Source of this traffic. Examples: facebook, google, etc\n        ', max_length=100, db_index=True, blank=True),
        ),
    ]
