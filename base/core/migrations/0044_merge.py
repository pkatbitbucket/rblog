# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0043_auto_20160130_1643'),
        ('core', '0043_remove_requirement_state'),
    ]

    operations = [
    ]
