# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0041_auto_20160201_1220'),
        ('core', '0044_merge'),
    ]

    operations = [
    ]
