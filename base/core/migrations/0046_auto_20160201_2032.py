# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0045_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requirementkind',
            name='product',
            field=models.CharField(max_length=255, choices=[(b'car', b'Car'), (b'health', b'Health'), (b'bike', b'Bike'), (b'car', b'Car'), (b'travel', b'Travel'), (b'term', b'Term'), (b'home', b'Home'), (b'life', b'Life')]),
        ),
        migrations.AlterField(
            model_name='requirementkind',
            name='sub_kind',
            field=models.CharField(blank=True, max_length=255, choices=[(b'EMAIL_ID', b'email_id'), (b'OWNER', b'owner'), (b'SALUTATION', b'salutation'), (b'OCCUPATION', b'occupation'), (b'PASSPORT_NO', b'passport_no'), (b'FUEL_TYPE', b'fuel_type'), (b'VEHICLE', b'vehicle'), (b'PED', b'ped'), (b'VOLUNTARY_DEDUCTABLE', b'voluntary_deductable'), (b'ENGINE_NO', b'engine_no'), (b'PASSENGER_COVER', b'passenger_cover'), (b'HYPOTHECATION', b'hypothecation'), (b'RELATIONSHIP', b'relationship'), (b'CONTACT_NO', b'contact_no'), (b'INSURED', b'insured'), (b'REGISTRATION_PLACE', b'registration_place'), (b'ADDRESS', b'address'), (b'NCB', b'ncb'), (b'NAME', b'name'), (b'POLICY_PERIOD', b'policy_period'), (b'DOB', b'dob'), (b'GENDER', b'gender'), (b'REGISTRATION_NO', b'registration_no'), (b'CHASSIS_NO', b'chassis_no'), (b'IDV', b'idv'), (b'MEDICAL_REQUIRED', b'medical_required'), (b'INSPECTION_REQUIRED', b'inspection_required')]),
        ),
    ]
