# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def add_new_policy_requirementkinds(apps, schema_editor):
    RequirementKind = apps.get_model('core', 'RequirementKind')
    rk_vals = [
        ('new_policy', '', 'health',),
        ('new_policy', '', 'motor',),
        ('new_policy', '', 'travel',),
        ('new_policy', '', 'car',),
        ('new_policy', 'MEDICAL_REQUIRED', 'health',),
        ('new_policy', 'INSPECTION_REQUIRED', 'car',),
        ('new_policy', '', 'life',),
        ('new_policy', '', 'term',),
        ('new_policy', '', 'bike',),
        ('new_policy', '', 'home',),
    ]
    for rkv in rk_vals:
        rk, _ = RequirementKind.objects.get_or_create(
            kind=rkv[0],
            sub_kind=rkv[1],
            product=rkv[2]
        )

class Migration(migrations.Migration):

    dependencies = [
        ('core', '0048_merge'),
    ]

    operations = [
        migrations.RunPython(
            code=add_new_policy_requirementkinds,
            reverse_code=migrations.RunPython.noop,
        )
    ]
