# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def add_csu_replacing_rm_mail_template(apps, schema_editor):
    mtype = "RM_CSU_TRANSITION"
    subject = "Important update about your Relationship Manager"

    html_body = """<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
                                <tr>
                        <td valign="top" align="left" colspan="2" style="padding:15px 30px 5px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;">
                            <p>Hi {{ obj.user.get_full_name }},</p>
                            <p>We have some news for you.<br/>
                            Now you don't have to worry about the availability of your Relationship Manager, or wait on the IVR to connect with someone. Coverfox has created<b style="color:#314451;"> a team of Service Managers</b>, who have an expertise in the field of insurance. </p>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left" colspan="2" style="padding:5px 30px;font:normal 16px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td colspan="2" valign="top" align="left" style="padding:5px;font:bold 16px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#FF704C;line-height:20px;background: #fff;">Service Managers at Coverfox?</td>
                                </tr>
                                 <tr>
                                    <td width="10" valign="top" align="center" style="padding:5px 0 5px 5px;font:bold 18px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><img src="https://s3.amazonaws.com/coverfox-mailer/csu-tick.png" width="12" alt=""></td>
                                    <td valign="top" align="left" style="padding:5px 10px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;">All Service Managers are <b style="color:#314451;">IRDAI certified insurance experts</b>, who understand the nitty-gritty of insurance to give you thoroughly informed advice.</td>
                                </tr>
                                <tr>
                                    <td width="10" valign="top" align="center" style="padding:5px 0 5px 5px;font:bold 18px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><img src="https://s3.amazonaws.com/coverfox-mailer/csu-tick.png" width="12" alt=""></td>
                                    <td valign="top" align="left" style="padding:5px 10px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;">With over <b>10 years of experience</b>, our Service Managers are experts in handling all kinds of insurance related issues. </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left" colspan="2" style="padding:5px 30px;font:normal 16px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td colspan="2" valign="top" align="left" style="padding:5px;font:bold 16px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#FF704C;line-height:20px;background: #fff;">How can the Service Managers help you?</td>
                                </tr>
                                 <tr>
                                    <td width="10" valign="top" align="center" style="padding:5px 0 5px 5px;font:bold 18px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><img src="https://s3.amazonaws.com/coverfox-mailer/csu-tick.png" width="12" alt=""></td>
                                    <td valign="top" align="left" style="padding:5px 10px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><b style="color:#314451;">Buying a new policy</b>- by helping you zero in on the plan that best fits your requirement.</td>
                                </tr>
                                <tr>
                                    <td width="10" valign="top" align="center" style="padding:5px 0 5px 5px;font:bold 18px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><img src="https://s3.amazonaws.com/coverfox-mailer/csu-tick.png" width="12" alt=""></td>
                                    <td valign="top" align="left" style="padding:5px 10px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><b style="color:#314451;">Making changes in your policy details</b>- from corrections to your misspelt name to a change in your address to shifting the registration zone of your vehicle.</td>
                                </tr>
                                 <tr>
                                    <td width="10" valign="top" align="center" style="padding:5px 0 5px 5px;font:bold 18px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><img src="https://s3.amazonaws.com/coverfox-mailer/csu-tick.png" width="12" alt=""></td>
                                    <td valign="top" align="left" style="padding:5px 10px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><b style="color:#314451;">Renewal of your policy</b>- year-after-year without missing on expiry dates.</td>
                                </tr>
                                 <tr>
                                    <td width="10" valign="top" align="center" style="padding:5px 0 5px 5px;font:bold 18px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><img src="https://s3.amazonaws.com/coverfox-mailer/csu-tick.png" width="12" alt=""></td>
                                    <td valign="top" align="left" colspan="2" style="padding:5px 10px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><b style="color:#314451;">Claim Settlement</b>- by simplifying your way through the usually tedious process.</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left" colspan="2" style="padding:5px 30px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;">
                            <p><b style="color:#FF704C;font-size:16px;">What changes for you?</b><br/>
                            Nothing much, except that you don't have to wait on the IVR anymore or worry about when your Relationship Manager would be available.<br/>
                            You can just contact our <b style="color:#314451;">Customer Service Unit</b> for anything and everything related to insurance on:</p>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left" colspan="2" style="padding:5px 30px 0;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;">
                            <b>Email</b>: <a href="mailto:help@coverfox.com" target="_target" style="color:#017ABA;outline-style:none;line-height:17px;text-decoration:none;">help@coverfox.com</a><br/>
                            <b>Toll Free No.</b>: 1800 209 9970 (Mon-Sat, 9am-9pm)
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left" colspan="2" style="padding:0 30px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;">
                            <p>Your Relationship Manager, {{ obj.rm.name }} will still be available to assist you over the next one month on <a href="tel:+91{{ obj.rm.mobile }}" style="color:#017ABA;outline-style:none;line-height:17px;text-decoration:none;">+91 {{ obj.rm.mobile }}</a>, while we effectively implement the transition.</p>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left" colspan="2" style="padding:0 30px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;">
                            <p>Warm Regards,<br/>
                            Team Coverfox</p>
                        </td>
                    </tr>
                    </table>"""

    text_body = """Hi {{ obj.user.get_full_name }},

We have some news for you.
Now you don't have to worry about the availability of your Relationship Manager, or wait on the IVR to connect with someone. Coverfox has created a team of Service Managers, who have an expertise in the field of insurance.

Service Managers at Coverfox?
All Service Managers are IRDAI certified insurance experts, who understand the nitty-gritty of insurance to give you thoroughly informed advice.
With over 10 years of experience, our Service Managers are experts in handling all kinds of insurance related issues.
How can the Service Managers help you?
Buying a new policy- by helping you zero in on the plan that best fits your requirement.
Making changes in your policy details- from corrections to your misspelt name to a change in your address to shifting the registration zone of your vehicle.
Renewal of your policy- year-after-year without missing on expiry dates.
Claim Settlement- by simplifying your way through the usually tedious process.
What changes for you?
Nothing much, except that you don't have to wait on the IVR anymore or worry about when your Relationship Manager would be available.
You can just contact our Customer Service Unit for anything and everything related to insurance on:

Email: help@coverfox.com
Toll Free No.: 1800 209 9970 (Mon-Sat, 9am-9pm)
Your Relationship Manager {{ obj.rm.name }} will still be available to assist you over the next one month on {{ obj.rm.mobile }}, while we effectively implement the transition.

Warm Regards,
Team Coverfox"""

    Notification = apps.get_model('core', 'Notification')

    try:
        Notification.objects.get(nid=mtype)
    except Notification.DoesNotExist:
        Notification.objects.create(
            nid=mtype,
            about='Sending mail to customers regarding the transition of Relationship Manager to '
                  'Customer Service Unit (CSU)',
            subject_content=subject,
            html_content=html_body,
            text_content=text_body,
            sender_name='Coverfox Support',
            sender_email='help@coverfox.com'
        )


class Migration(migrations.Migration):
    dependencies = [
        ('core', '0050_auto_20160207_1611'),
    ]

    operations = [
        migrations.RunPython(
            code=add_csu_replacing_rm_mail_template,
            reverse_code=migrations.RunPython.noop,
        )
    ]
