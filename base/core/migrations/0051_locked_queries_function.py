# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models

# source: http://big-elephants.com/2013-09/exploring-query-locks-in-postgres/

# NOTE: you may be wondering why we are adding if exists clause in revsere_sql
# since reverse is only called if forward succeeded.
#
# the answer is there can be multiple migrations that create or update this
# view. each of them will, on reverse try to delete the view, first one
# will succeed, rest will fail.

class Migration(migrations.Migration):

    dependencies = [
        ('core', '0050_auto_20160207_1611'),
    ]

    operations = [
        migrations.RunSQL(
            """
CREATE OR REPLACE VIEW cf_lock_monitor AS (
    SELECT
        COALESCE(blockingl.relation::regclass::text,blockingl.locktype) as locked_item,
        now() - blockeda.query_start AS waiting_duration, blockeda.pid AS blocked_pid,
        blockeda.query as blocked_query, blockedl.mode as blocked_mode,
        blockinga.pid AS blocking_pid, blockinga.query as blocking_query,
        blockingl.mode as blocking_mode
    FROM pg_catalog.pg_locks blockedl
    JOIN pg_stat_activity blockeda ON blockedl.pid = blockeda.pid
    JOIN pg_catalog.pg_locks blockingl ON (
        ( 
            (blockingl.transactionid=blockedl.transactionid) 
            OR (
                blockingl.relation=blockedl.relation 
                AND blockingl.locktype=blockedl.locktype
            )
        ) 
        AND blockedl.pid != blockingl.pid
    )
    JOIN pg_stat_activity blockinga ON (
        blockingl.pid = blockinga.pid
        AND blockinga.datid = blockeda.datid
    )
    WHERE (
        NOT blockedl.granted
        AND blockinga.datname = current_database()
    )
);
            """,
            reverse_sql="""
DROP VIEW IF EXISTS cf_lock_monitor
            """
        )
    ]
