# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0033_insurer'),
        ('core', '0050_auto_20160207_1611'),
    ]

    operations = [
    ]
