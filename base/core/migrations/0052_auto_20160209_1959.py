# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.models
import core.fields
import django.contrib.postgres.fields
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0051_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='Quote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('premium', models.FloatField()),
                ('actions', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=20, choices=[(b'see_details', b'see_details'), (b'see_premium_breakup', b'see_premium_breakup'), (b'add_to_compare', b'add_to_compare'), (b'buy', b'buy')]), size=None)),
                ('position', core.fields.IntegerRangeField(validators=[django.core.validators.MaxValueValidator(50), django.core.validators.MinValueValidator(1)])),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='QuoteCompare',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('quotes', models.ManyToManyField(to='core.Quote')),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='QuoteSet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('sort', models.CharField(max_length=20, choices=[(b'best_match', b'best_match'), (b'premium', b'premium')])),
                ('activity', models.ForeignKey(to='core.Activity')),
                ('last_compare', models.ForeignKey(blank=True, to='core.QuoteCompare', null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.RemoveField(
            model_name='insurer',
            name='creator',
        ),
        migrations.RemoveField(
            model_name='insurer',
            name='editor',
        ),
        migrations.AddField(
            model_name='quote',
            name='insurer',
            field=models.ForeignKey(to='core.Insurer'),
        ),
        migrations.AddField(
            model_name='quote',
            name='quote_set',
            field=models.ForeignKey(to='core.QuoteSet'),
        ),
        migrations.AddField(
            model_name='requirement',
            name='quote_set',
            field=models.ForeignKey(blank=True, to='core.QuoteSet', null=True),
        ),
    ]
