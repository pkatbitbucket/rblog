# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('statemachine', '0006_state_verbose'),
        ('core', '0051_locked_queries_function'),
    ]

    operations = [
        migrations.AddField(
            model_name='requirementstatehistory',
            name='previous_state',
            field=models.ForeignKey(related_name='previous_for_history', to='statemachine.State', null=True),
        ),
        migrations.AlterField(
            model_name='requirementstatehistory',
            name='state',
            field=models.ForeignKey(related_name='new_for_history', to='statemachine.State'),
        ),
    ]
