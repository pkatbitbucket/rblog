# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0051_add_csu_replacing_rm_mail_template'),
        ('core', '0051_locked_queries_function'),
    ]

    operations = [
    ]
