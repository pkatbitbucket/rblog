# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0053_merge'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='notification',
            name='creator',
        ),
        migrations.RemoveField(
            model_name='notification',
            name='editor',
        ),
        migrations.AddField(
            model_name='email',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, null=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='email',
            name='modified_date',
            field=models.DateTimeField(auto_now=True, null=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='phone',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, null=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='phone',
            name='modified_date',
            field=models.DateTimeField(auto_now=True, null=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='user',
            name='fd',
            field=models.ForeignKey(related_name='user_fd', blank=True, to='core.FormData', null=True),
        ),
        migrations.AlterField(
            model_name='activity',
            name='activity_type',
            field=models.CharField(help_text=b'\n            Examples: viewed, dropped, `_focus, email_change, etc\n        ', max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='notification',
            name='sync',
            field=models.BooleanField(default=False, help_text=b'Set true to send notifications synchronously'),
        ),
        migrations.AlterField(
            model_name='requirementkind',
            name='product',
            field=models.CharField(max_length=255, choices=[(b'car', b'Car'), (b'health', b'Health'), (b'bike', b'Bike'), (b'car', b'Car'), (b'travel', b'Travel'), (b'term', b'Term'), (b'home', b'Home'), (b'life', b'Life')]),
        ),
        migrations.AlterField(
            model_name='requirementkind',
            name='sub_kind',
            field=models.CharField(blank=True, max_length=255, choices=[(b'email_id', b'Email_Id'), (b'owner', b'Owner'), (b'salutation', b'Salutation'), (b'occupation', b'Occupation'), (b'passport_no', b'Passport_No'), (b'fuel_type', b'Fuel_Type'), (b'vehicle', b'Vehicle'), (b'ped', b'Ped'), (b'voluntary_deductable', b'Voluntary_Deductable'), (b'engine_no', b'Engine_No'), (b'passenger_cover', b'Passenger_Cover'), (b'hypothecation', b'Hypothecation'), (b'relationship', b'Relationship'), (b'contact_no', b'Contact_No'), (b'insured', b'Insured'), (b'registration_place', b'Registration_Place'), (b'address', b'Address'), (b'ncb', b'Ncb'), (b'name', b'Name'), (b'policy_period', b'Policy_Period'), (b'dob', b'Dob'), (b'gender', b'Gender'), (b'registration_no', b'Registration_No'), (b'chassis_no', b'Chassis_No'), (b'idv', b'Idv'), (b'MEDICAL_REQUIRED', b'medical_required'), (b'INSPECTION_REQUIRED', b'inspection_required')]),
        ),
    ]
