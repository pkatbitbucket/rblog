# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models, transaction as django_transaction


def find_and_remove_duplicate(apps, schema_editor):
    Marketing = apps.get_model('core', 'Marketing')
    mobjs = Marketing.objects.iterator()
    for m in mobjs:
        duplicate_mid = Marketing.objects.filter(
            campaign__iexact=m.campaign, source__iexact=m.source,
            medium__iexact=m.medium, category__iexact=m.category,
            brand__iexact=m.brand, content__iexact=m.content
        )
        mid1 = duplicate_mid[0]
        for i in range(1, len(duplicate_mid)):
            set_duplicate(mid1, duplicate_mid[i])


def set_duplicate(mid1, mid2):
        reverse_relations = [f for f in mid1._meta.get_fields() if (f.one_to_many or f.one_to_one) and f.auto_created]
        with django_transaction.atomic():
            for rel in reverse_relations:
                related_objs = rel.related_model._base_manager.filter(
                    **{rel.field.name: mid2}
                ).select_for_update()
                related_objs.update(**{rel.field.name: mid1})
            mid2.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0053_merge'),
    ]

    operations = [
        migrations.RunPython(
            code=find_and_remove_duplicate,
            reverse_code=migrations.RunPython.noop
        ),
    ]
