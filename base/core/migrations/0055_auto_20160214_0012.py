# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0054_auto_20160214_0008'),
    ]

    operations = [
        migrations.AddField(
            model_name='tracker',
            name='fd',
            field=models.ForeignKey(related_name='visitor_fd', blank=True, to='core.FormData', null=True),
        ),
        migrations.AlterField(
            model_name='activity',
            name='activity_type',
            field=models.CharField(help_text=b'\n            Examples: viewed, dropped, email_focus, email_change, etc\n        ', max_length=255, null=True, blank=True),
        ),
    ]
