# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings

from django.db import migrations, models
import django.contrib.postgres.fields
import core.models


def fix_backoffice_receivers(apps, schema_editor):
    Notification = apps.get_model('core', 'Notification')

    for nf in Notification.objects.all():
        if nf.nid == 'bulk_notification_status':
            nf.to = [x[1] for x in settings.ADMINS]
            nf.save()

        if nf.nid == 'MOTOR_TRANSACTION_SUCCESS_MAIL':
            nf.bcc = ['motorteam@coverfox.com']
            nf.save()

        if nf.nid == 'MOTOR_TRANSACTION_FAILURE_MAIL':
            nf.bcc = ['motorteam@coverfox.com']
            nf.save()


def add_remaining_emails(apps, schema):
    Notification = apps.get_model('core', 'Notification')

    notification = Notification(
        nid='QA_EMAILS',
        about='Email to QA about exceptions occurred.',
        to=[
            'qa@coverfoxmail.com',
            'rakesh@coverfoxmail.com',
            'jay@coverfox.com',
            'deepak@coverfoxmail.com',
            'amit.siddhu@coverfoxmail.com',
            'vinay@coverfoxmail.com',
            'rajesh@coverfoxmail.com',
            'shobhit@coverfoxmail.com',
        ],
        sender_name='Dev Bot',
        sender_email='bot@coverfox.com'
    )
    notification.save()

    notification = Notification(
        nid='HEALTH_ALERTS',
        to=settings.HEALTH_TEAM,
        about='Health Product Alert emails.',
        sender_name='Dev Bot',
        sender_email='bot@coverfox.com'
    )
    notification.save()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0055_auto_20160214_0012'),
    ]

    operations = [
        migrations.AddField(
            model_name='notification',
            name='bcc',
            field=django.contrib.postgres.fields.ArrayField(help_text=b"Email bcc. Enter email address separated by ','", base_field=models.EmailField(max_length=254), blank=True, default=[], null=True, size=None),
        ),
        migrations.AlterField(
            model_name='notification',
            name='about',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='notification',
            name='numbers',
            field=django.contrib.postgres.fields.ArrayField(help_text=b"The default receivers of sms. Enter numbers separated by ','", base_field=models.CharField(max_length=12), blank=True, default=[], null=True, size=None),
        ),
        migrations.AlterField(
            model_name='notification',
            name='reply_to',
            field=models.CharField(help_text=b'Reply to header', max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='notification',
            name='subject',
            field=models.CharField(blank=True, help_text=b'Template Name if using a template', max_length=100, validators=[core.models.template_exists]),
        ),
        migrations.AlterField(
            model_name='notification',
            name='subject_content',
            field=models.TextField(help_text=b'Email subject content', blank=True),
        ),
        migrations.AlterField(
            model_name='notification',
            name='sync',
            field=models.BooleanField(default=False, help_text=b'Set true to send notifications synchronously'),
        ),
        migrations.AlterField(
            model_name='notification',
            name='to',
            field=django.contrib.postgres.fields.ArrayField(help_text=b"The default receivers of email. Enter email address separated by ','", base_field=models.EmailField(max_length=254), blank=True, default=[], null=True, size=None),
        ),
        migrations.RunPython(fix_backoffice_receivers, reverse_code=migrations.RunPython.noop),
        migrations.RunPython(add_remaining_emails, reverse_code=migrations.RunPython.noop)

    ]
