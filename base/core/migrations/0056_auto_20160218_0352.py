# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
from django.utils import timezone


def update_null(apps, schema_migration):
    Email = apps.get_model('core', 'Email')
    emails = Email.objects.filter(created_date__isnull=True).select_for_update()
    emails.update(created_date=timezone.now())
    emails = Email.objects.filter(modified_date__isnull=True).select_for_update()
    emails.update(modified_date=timezone.now())

    Phone = apps.get_model('core', 'Phone')
    phones = Phone.objects.filter(created_date__isnull=True).select_for_update()
    phones.update(created_date=timezone.now())
    phones = Phone.objects.filter(modified_date__isnull=True).select_for_update()
    phones.update(modified_date=timezone.now())


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0055_auto_20160214_0012'),
    ]

    operations = [
        migrations.RunPython(
            code=update_null,
            reverse_code=migrations.RunPython.noop,
        ),
    ]
