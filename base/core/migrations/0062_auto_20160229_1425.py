# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0061_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='email',
            name='is_primary',
            field=models.BooleanField(default=False, db_index=True),
        ),
        migrations.AlterField(
            model_name='email',
            name='is_verified',
            field=models.BooleanField(default=False, db_index=True),
        ),
        migrations.AlterField(
            model_name='email',
            name='verification_code',
            field=models.CharField(max_length=25, null=True, db_index=True),
        ),
        migrations.AlterField(
            model_name='phone',
            name='is_primary',
            field=models.BooleanField(default=False, db_index=True),
        ),
        migrations.AlterField(
            model_name='phone',
            name='is_verified',
            field=models.BooleanField(default=False, db_index=True),
        ),
        migrations.AlterField(
            model_name='phone',
            name='verification_code',
            field=models.CharField(max_length=8, null=True, db_index=True),
        ),
    ]
