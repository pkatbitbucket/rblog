# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def add_quote_sharing_template(apps, schema_editor):
    mtype = "SHARE_QUOTES"
    subject = " Insurance quotes for your {{obj.quote.vehicle}} from Coverfox.com"

    html_body = """
                    Hi,
                    <br/>
                    <p>
                        Thanks for visiting Coverfox.com. You were checking quotes for {{obj.quote.vehicle}}. You can access your policy options anytime on {{ obj.url }}.
                        <br/>
                    </p>
                    <p>
                        For any assistance call us on {{obj.tollfree}} Toll Free.
                        <br/>
                    </p>
                    <p>
                        We look forward to your next visit.
                        <br/>
                    </p>
                    <p>
                        Thanks,<br/>
                        Team Coverfox
                    </p>
                """

    text_body = """
                    Hi,
                    Thanks for visiting Coverfox.com. You were checking quotes for {{obj.quote.vehicle}}. You can access your policy options anytime on {{ obj.url }}.
                    Thanks,
                    Team Coverfox
                """
    sms_text = """
                    Hi. Access policy options for your {{obj.quote.vehicle}} at {{obj.url}}. For any assistance call us on {{obj.tollfree}} Toll Free.
                """
    Notification = apps.get_model('core', 'Notification')

    try:
        notification_instance = Notification.objects.get(nid=mtype)
    except Notification.DoesNotExist:
        notification_instance = Notification()
    notification_instance.nid = mtype
    notification_instance.about = 'Share Quotes'
    notification_instance.subject_content = subject
    notification_instance.html_content = html_body
    notification_instance.text_content = text_body
    notification_instance.sender_name = 'Coverfox Support'
    notification_instance.sender_email = 'help@coverfox.com'
    notification_instance.sms_content = sms_text
    notification_instance.save()


class Migration(migrations.Migration):
    dependencies = [
        ('core', '0061_merge'),
    ]

    operations = [
        migrations.RunPython(
            code=add_quote_sharing_template,
            reverse_code=migrations.RunPython.noop,
        )
    ]
