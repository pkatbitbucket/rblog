# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def add_rsa_template(apps, schema_editor):
    mtype = "RSA_NOTIFY"
    subject = "Your Get Going Card has been Registered."

    html_body = """
    <b>Get Ready to Get Going!</b>
    <br/>
        Hi {{obj.first_name}} {{obj.last_name}},
        <br/>
        Congratulations! Your Coverfox Get Going Card numbered {{obj.card_id}} has been
        registered. You will now be able to avail the following services* in case of an emergency:
        <br/>
        <ul>
            <li>On call technical assistance for quick and minor repairs</li>
            <li>Free towing up to 40 kms</li>
            <li>Tyre change labour support</li>
            <li>Key lost or locked inside support</li>
            <li>Alternate battery/jump start labour support</li>
            <li>Emergency fuel arrangement</li>
            <li>Garage locator/ route guidance support</li>
            <li>Air/ train ticket/ accommodation arrangement</li>
        </ul>
        Your services will be activated in the next 72 hours.
        <br/>
        <i>*Maximum 2 claims are allowed during a 6-month membership and 4 claims during a one-year membership.</i>
        <br/>
        Please ensure that you put the sticker on your car in an area easily accessible at the time of
        emergency. Visit www.coverfox.com/ggc for details on your Get Going Card.
    <br/>
    Warm Regards,
    <br/>
    Team Coverfox

    """

    text_body = """
    Get Ready to Get Going!
        Hi {{obj.first_name}} {{obj.last_name}},
        Congratulations! Your Coverfox Get Going Card numbered {{obj.card_id}} has been
        registered. You will now be able to avail the following services* in case of an emergency:
            > On call technical assistance for quick and minor repairs.
            > Free towing up to 40 kms
            > Tyre change labour support
            > Key lost or locked inside support
            > Alternate battery/jump start labour support
            > Emergency fuel arrangement
            > Garage locator/ route guidance support
            > Air/ train ticket/ accommodation arrangement
        Your services will be activated in the next 72 hours.
        *Maximum 2 claims are allowed during a 6-month membership and 4 claims during a one-year membership.
        Please ensure that you put the sticker on your car in an area easily accessible at the time of
        emergency. Visit www.coverfox.com/ggc for details on your Get Going Card.
    Warm Regards,
    Team Coverfox
    """

    sms_text = """
    Congratulations! Your Coverfox Get Going Card {{obj.card_num}} has been registered. Your
    services will be activated in the next 72 hours.
    """
    Notification = apps.get_model('core', 'Notification')

    try:
        Notification.objects.get(nid=mtype)
    except Notification.DoesNotExist:
        Notification.objects.create(
            nid=mtype,
            about='Sending RSA Notifications',
            subject_content=subject,
            html_content=html_body,
            text_content=text_body,
            sender_name='Coverfox Support',
            sender_email='help@coverfox.com',
            sms_content=sms_text
        )


class Migration(migrations.Migration):
    dependencies = [
        ('core', '0061_merge'),
    ]

    operations = [
        migrations.RunPython(
            code=add_rsa_template,
            reverse_code=migrations.RunPython.noop,
        )
    ]
