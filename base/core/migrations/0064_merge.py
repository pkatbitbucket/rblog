# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0063_merge'),
        ('core', '0063_rsa_mail_message'),
    ]

    operations = [
    ]
