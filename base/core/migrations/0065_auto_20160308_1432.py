# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0064_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='_customer_id',
            field=models.CharField(verbose_name='customer id', max_length=32, unique=True, null=True, editable=False),
        ),
    ]
