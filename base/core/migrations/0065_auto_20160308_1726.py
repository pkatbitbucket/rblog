# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import core.models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0064_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='EndorsementDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('policy_number', models.CharField(max_length=250, blank=True)),
                ('policy_name', models.CharField(max_length=250, blank=True)),
                ('customer_name', models.CharField(max_length=250, blank=True)),
                ('customer_email', models.CharField(max_length=250, blank=True)),
                ('product', models.CharField(max_length=250, blank=True)),
                ('endorsement_type', models.CharField(max_length=250, blank=True)),
                ('field', models.CharField(max_length=250, blank=True)),
                ('current_value', models.CharField(max_length=250, blank=True)),
                ('endorsed_value', models.CharField(max_length=250, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
    ]
