# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0062_quote_share_template'),
        ('core', '0064_merge'),
    ]

    operations = [
    ]
