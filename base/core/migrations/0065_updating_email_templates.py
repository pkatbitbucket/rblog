# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def updating_email_templates(apps, schema_editor):
    Notification = apps.get_model('core', 'Notification')
    update_template(Notification, 'new')


def reverse_updating_email_templates(apps, schema_editor):
    Notification = apps.get_model('core', 'Notification')
    update_template(Notification, 'old')


def update_template(Model, version):
    for nid, template in NID_TEMPLATE_MAPPING.items():
        try:
            n = Model.objects.get(nid=nid)
        except Model.DoesNotExist:
            pass
        else:
            n.html_content = template[version]
            n.save()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0064_merge'),
    ]

    operations = [
        migrations.RunPython(
            code=updating_email_templates,
            reverse_code=reverse_updating_email_templates,
        )
    ]


NID_TEMPLATE_MAPPING = {
    'APPOINTMENT_ALERT': {
        'new': """<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
                   <tr>
                       <td valign="top" align="left" style="padding:10px 30px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#6E7C85;line-height:1.5em;background: #fff;">
                           <p>A walk-in appointment has been booked:</p>
                           <p>Name: {{obj.msg.first_name}} {{obj.msg.last_name}}<br />Date: {{obj.msg.appointment_date}}<br />Slot: {{obj.msg.slot}}<br />Mobile: {{obj.msg.mobile}}<br />Referred By: {{obj.msg.referred_by}}<br />Created On: {{obj.msg.created_on}}</p>
                       </td>
                   </tr>
                </table>""",
        'old': """<p>A walk-in appointment has been booked:</p>
                <p>Name: {{obj.msg.first_name}} {{obj.msg.last_name}}<br />Date: {{obj.msg.appointment_date}}<br />Slot: {{obj.msg.slot}}<br />Mobile: {{obj.msg.mobile}}<br />Referred By: {{obj.msg.referred_by}}<br />Created On: {{obj.msg.created_on}}</p>"""
    },

    'MOTOR_TRANSACTION_FAILURE_MAIL': {
        'new': """<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
                    <tr>
                        <td valign="top" align="left" style="padding:10px 30px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#6E7C85;line-height:1.5em;background: #fff;">
                            <p>Hi {{obj.name}},</p>
                            <p>There was some problem with the transaction. You know how technology misbehaves sometimes. We are sorry for the inconvenience.</p>
                            <p>You can take a confirmation on further process from our Executive on our Toll Free number: {% ifequal obj.vehicle_type 'Car' %}<strong>1800-209-99-20{%else%}<span>1800-209-99-20</span>{%endifequal%}</strong>&nbsp;(Available Monday - Saturday from 10 a.m -7 p.m) or mail us on help@coverfox.com.</p>
                            <p>Don't worry, you will not have to fill-up the entire form again.</p>
                            <p>Your transaction is saved here -&nbsp;</p>
                            {% ifequal obj.vehicle_type 'Car' %}
                            <p><a href="http://www.coverfox.com/motor/fourwheeler/{{obj.insurer_slug}}/desktopForm/{{obj.transaction_id}}/#stage1" style="color:#017ABA;outline-style:none;line-height:17px;text-decoration:none;" target="_blank">Visit Saved Transaction</a></p>
                            {% else %}
                            <p><a href="http://www.coverfox.com/motor/twowheeler/{{obj.insurer_slug}}/desktopForm/{{obj.transaction_id}}/#stage1" style="color:#017ABA;outline-style:none;line-height:17px;text-decoration:none;" target="_blank">Visit Saved Transaction</a></p>
                            {% endifequal %}
                            <p>Cheers,<br />Team Coverfox</p>
                        </td>
                    </tr>
                </table>""",
        'old': """<p>Hi {{obj.name}},</p>
                    <p>There was some problem with the transaction. You know how technology misbehaves sometimes. We are sorry for the inconvenience.</p>
                    <p>You can take a confirmation on further process from our Executive on our Toll Free number: {% ifequal obj.vehicle_type 'Car' %}<span>1800-209-99-20{%else%}<span>1800-209-99-20</span>{%endifequal%}</span>&nbsp;(Available Monday - Saturday from 10 a.m -7 p.m) or mail us on help@coverfox.com.</p>
                    <p>Don't worry, you will not have to fill-up the entire form again.</p>
                    <p>Your transaction is saved here -&nbsp;</p>
                    <p>{% ifequal obj.vehicle_type 'Car' %}</p>
                    <p><a class="btn btn-warning" href="http://www.coverfox.com/motor/fourwheeler/{{obj.insurer_slug}}/desktopForm/{{obj.transaction_id}}/#stage1" target="_blank">Visit Saved Transaction</a></p>
                    <p>{% else %}</p>
                    <p><a class="btn btn-warning" href="http://www.coverfox.com/motor/twowheeler/{{obj.insurer_slug}}/desktopForm/{{obj.transaction_id}}/#stage1" target="_blank">Visit Saved Transaction</a></p>
                    <p>{% endifequal %}</p>
                    <p>Cheers,<br />Team Coverfox</p>"""
    },

    'MOTOR_TRANSACTION_SUCCESS_MAIL': {
        'new': """<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
                    <tr>
                        <td valign="top" align="left" style="padding:0px 30px 0px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#6E7C85;line-height:1.5em;background: #fff;">
                            <p>Hello {{obj.name}},</p>
                            <p>Thank you for choosing Coverfox for your {{obj.vehicle_type}}&nbsp;Insurance. Your transaction was successful.{% if obj.policy_document_url %}You can download your original policy copy by clicking on the link below:</p>
                            <p><a style="font-size:16px;border:1px solid #ff704c;width:160px;padding:7px 0px;outline:none;background-color:#ff704c;color:#fff;text-decoration:none;border-radius:5px;display:inline-block;text-align:center;" href="{{ obj.policy_document_url }}" target="_blank">Download e-Policy</a>
                            </p>
                            {%else%}
                            <p>You will receive the original policy document through email within {{ obj.policy_tat }}.</p>
                            {% endif %}
                            <p>Following are the details of your {{obj.vehicle_type|lower}} insurance policy:</p>
                            <table width="100%" style="background: #eaeaea;" cellpadding="1" cellspacing="1" border="0" align="left">
                                <tbody>
                                <tr>
                                    <th style="background: #fff;">Insurance Company:</th>
                                    <td style="padding-left: 10px;background: #fff;">{{obj.insurer_title}}</td>
                                </tr>
                                <tr>
                                    <th style="background: #fff;">Premium:</th>
                                    <td style="padding-left: 10px;background: #fff;">INR {{obj.premium}}</td>
                                </tr>
                                <tr>
                                    <th style="background: #fff;">Transaction Number:</th>
                                    <td style="padding-left: 10px;background: #fff;">{{obj.transaction_number}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left" style="padding:15px 30px 10px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#6E7C85;line-height:1.5em;background: #fff;">
                            {% if obj.policy_document_url %}<p>If you face any issues, please call us on our Toll Free Number: <span>1800-209-99-70</span>&nbsp; (Monday - Saturday from 10 a.m - 7 p.m) or email us on help@coverfox.com.</p>
                            {%else%}
                            <p>Sometimes the insurer might take longer to generate the policy document. There is nothing to worry about.</p>
                            <p>In case you do not receive the policy PDF in the next 24 hours, please call us on our Toll Free Number: <span>1800-209-99-70</span>&nbsp;(Monday - Saturday from 10 a.m - 7 p.m) or email us on help@coverfox.com.</p>
                            {%endif%}
                            <p>Cheers!&nbsp;<br />Team Coverfox</p>
                        </td>
                    </tr>
                </table>""",
        'old': """<p>Hello {{obj.name}},</p>
                    <p>&nbsp;</p>
                    <p>Thank you for choosing Coverfox for your {{obj.vehicle_type}}&nbsp;Insurance. Your transaction was successful.{% if obj.policy_document_url %}You can download your original policy copy by clicking on the link below:</p>
                    <p><a class="btn btn-warning" href="{{ obj.policy_document_url }}" target="_blank">Download e-Policy</a>&nbsp;{%else%}</p>
                    <p>You will receive the original policy document through email within {{ obj.policy_tat }}.{% endif %}</p>
                    <p>&nbsp;</p>
                    <p>Following are the details of your {{obj.vehicle_type|lower}} insurance policy:</p>
                    <table>
                    <tbody>
                    <tr><th>Insurance Company:</th>
                    <td>{{obj.insurer_title}}</td>
                    </tr>
                    <tr><th>Premium:</th>
                    <td>INR {{obj.premium}}</td>
                    </tr>
                    <tr><th>Transaction Number:</th>
                    <td>{{obj.transaction_number}}</td>
                    </tr>
                    </tbody>
                    </table>
                    <p>{% if obj.policy_document_url %}If you face any issues, please call us on our Toll Free Number: <span>1800-209-99-70</span>&nbsp; (Monday - Saturday from 10 a.m - 7 p.m) or email us on help@coverfox.com.</p>
                    <p>{%else%}</p>
                    <p>Sometimes the insurer might take longer to generate the policy document. There is nothing to worry about.</p>
                    <p>In case you do not receive the policy PDF in the next 24 hours, please call us on our Toll Free Number: <span>1800-209-99-70</span>&nbsp;(Monday - Saturday from 10 a.m - 7 p.m) or email us on help@coverfox.com.</p>
                    <p>{%endif%}</p>
                    <p>Cheers!&nbsp;<br />Team Coverfox</p>"""
    },

    'RESULTS_DROP_MAIL': {
        'new': """<table width="100%" cellpadding="0" cellspacing="0" border="0" align="left">
                    <tr>
                        <td valign="top" align="left" style="padding:10px 30px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#6E7C85;line-height:1.5em;background: #fff;">
                            <p>Hi {{obj.name}},</p>
                            <p>We noticed, that you went through a couple of health insurance options, but did not decide on any one.</p>
                            <p>We have saved the results of your last search and here are the top matching plans. You can buy them directly from here, you do not need to search for the plans again.</p>
                            {% for p in obj.plan_data %}
                            <table width="100%" style="background: #eaeaea;" cellpadding="1" cellspacing="1" border="0" align="left">
                            <tbody>
                            <tr>
                                <th style="background: #fff;">Insurer</th>
                                <td style="padding-left: 10px;background: #fff;">{{p.insurer_name}}</td>
                            </tr>
                            <tr>
                                <th style="background: #fff;">Cover</th>
                                <td style="padding-left: 10px;background: #fff;">Rs. {{p.cover}}</td>
                            </tr>
                            <tr>
                                <th style="background: #fff;">Premium</th>
                                <td style="padding-left: 10px;background: #fff;">Rs. {{p.premium}}</td>
                            </tr>
                            </tbody>
                            </table>
                            <p>&nbsp;</p>
                            <p><a style="font-size:16px;border:1px solid #ff704c;width:160px;padding:7px 0px;outline:none;background-color:#ff704c;color:#fff;text-decoration:none;border-radius:5px;display:inline-block;text-align:center;" href="{{p.buy_url}}" target="_blank">Buy this plan</a></p>
                            {% endfor %}
                            <p>You can also look at other plans and compare them from where you left off -</p>
                            <p><a style="font-size:16px;border:1px solid #ff704c;width:160px;padding:7px 0px;outline:none;background-color:#ff704c;color:#fff;text-decoration:none;border-radius:5px;display:inline-block;text-align:center;" href="{{obj.results_url}}" target="_blank">View all plans</a></p>
                            <p>We would love to help you decide and one of our health insurance experts can also call you if you want.</p>
                            <p>In case you want to get in touch, you can write to us at <strong>help@coverfox.com</strong> or call us at <strong>1800-209-99-20</strong> (Monday to Saturday - 10a.m to 7p.m).</p>
                            <p>We won't leave your side once you've made an informed purchase. You'll get 100% assistance from Coverfox should you make any claims.</p>
                            <p>See you again!</p>
                            <p>Thanks,<br />Team Coverfox</p>
                        </td>
                    </tr>
                </table>""",
        'old': """<p>Hi {{obj.name}},</p>
                    <p>We noticed, that you went through a couple of health insurance options, but did not decide on any one.</p>
                    <p>We have saved the results of your last search and here are the top matching plans. You can buy them directly from here, you do not need to search for the plans again.</p>
                    <p>{% for p in obj.plan_data %}</p>
                    <table>
                    <tbody>
                    <tr><th>Insurer</th>
                    <td>{{p.insurer_name}}</td>
                    </tr>
                    <tr><th>Cover</th><th>Rs. {{p.cover}}</th></tr>
                    <tr><th>Premium</th>
                    <td>Rs. {{p.premium}}</td>
                    </tr>
                    </tbody>
                    </table>
                    <p><a class="btn btn-warning" style="background-color: #e24f05; border: 1px solid rgba(0, 0, 0, 0.06); border-radius: 4px 4px 4px 4px; color: #ffffff; font-size: 12px; font-weight: bold; line-height: 1.3em; padding: 10px;" href="{{p.buy_url}}" target="_blank">Buy this plan</a></p>
                    <p><br /> {% endfor %}</p>
                    <p>You can also look at other plans and compare them from where you left off -</p>
                    <p><a class="btn btn-warning" style="background-color: #e24f05; border: 1px solid rgba(0, 0, 0, 0.06); border-radius: 4px 4px 4px 4px; color: #ffffff; font-size: 12px; font-weight: bold; line-height: 1.3em; padding: 10px;" href="{{obj.results_url}}" target="_blank">View all plans</a></p>
                    <p>We would love to help you decide and one of our health insurance experts can also call you if you want.</p>
                    <p>In case you want to get in touch, you can write to us at help@coverfox.com or call us at <span>1800-209-99-20</span>(Monday to Saturday - 10.am to 7p.m)</p>
                    <p>We won't leave your side once you've made an informed purchase. You'll get 100% assistance from Coverfox should you make any claims.</p>
                    <p>See you again!</p>
                    <p>Thanks,<br /> Team Coverfox</p>"""
    },

    'RSA_STEP2_EXPIRED_INS': {
        'new': """
                <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
                    <tr>
                        <td valign="top" align="left" style="padding:10px 30px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#6E7C85;line-height:1.5em;background: #fff;">
                            <p>Dear {{obj.name}},</p>
                            <p>Congratulations! You have successfully registered for the free car breakdown service. You member id is: {{obj.member_id}}</p>
                            <p>You are being provided this service for free as a limited period offer from www.coverfox.com</p>
                            <p>We understand that your car insurance has expired.</p>
                            <p>The car breakdown service can be provided only with a valid insurance of your car.</p>
                            <p>But you are in luck! Coverfox Insurance advisors will assist you to get your car insured, even if you have an expired policy. Your free breakdown services will be activated immediately after.</p>
                            <p>Give us a call on our <strong>Toll Free Number: 1800-209-99-20</strong> to get Insurance for your car, and claim your Free Breakdown policy.</p>
                            <p>Pleasure to service proud car owners.<br />Team Coverfox</p>
                        </td>
                    </tr>
                </table>""",
        'old': """<p>Dear {{obj.name}},</p>
                    <p>Congratulations! You have successfully registered for the free car breakdown service. You member id is: {{obj.member_id}}</p>
                    <p>You are being provided this service for free as a limited period offer from www.coverfox.com</p>
                    <p>We understand that your car insurance has expired.</p>
                    <p>The car breakdown service can be provided only with a valid insurance of your car.</p>
                    <p>But you are in luck! Coverfox Insurance advisors will assist you to get your car insured, even if you have an expired policy. Your free breakdown services will be activated immediately after.</p>
                    <p>Give us a call on our <strong>Toll Free Number: 1800-209-99-20</strong> to get Insurance for your car, and claim your Free Breakdown policy.</p>
                    <p>Pleasure to service proud car owners.<br />Team Coverfox</p>"""
    },

    'RSA_STEP2_VALID_INS': {
        'new': """
                <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
                    <tr>
                        <td valign="top" align="left" style="padding:10px 30px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#6E7C85;line-height:1.5em;background: #fff;">
                            <p>Dear {{obj.name}},</p>
                            <p>
                                Congratulations! You have successfully registered for the free car breakdown service.
                                <br />
                                Your Membership ID is: {{obj.member_id}}
                            </p>
                            <p>You are just one step away from activating your services.</p>
                            <p><strong>Please send a copy of your current valid Car Insurance Policy document to cbs@coverfox.com.</strong></p>
                            <p>Your will receive your car breakdown service policy document on your registered email immediately after.</p>
                            <p>You are now also eligible for <strong>Additional 10% Discount</strong> on your next car insurance renewal on Coverfox.com.</p>
                            <p>Just use your Membership ID during renewal - {{obj.member_id}}.</p>
                            <p>You are being provided this service for free as a limited period offer from Coverfox.com</p>
                            <p>In case your car insurance has expired, please call Coverfox insurance advisors on the <strong>Toll Free Number: 1800-209-99-20</strong> to get your insurance issued.</p>
                            <p><br />Pleasure to service proud car owners.<br />Team Coverfox</p>
                        </td>
                    </tr>
                </table>""",
        'old': """<p><span style="color: #000000;">Dear {{obj.name}},</span></p>
                    <p><span style="color: #000000;">Congratulations! You have successfully registered for the free car breakdown service. </span><br /><span style="color: #000000;">Your Membership ID is: {{obj.member_id}}</span></p>
                    <p><span style="color: #000000;">You are just one step away from activating your services.</span></p>
                    <p><span style="color: #000000;"><strong>Please send a copy of your current valid Car Insurance Policy document to cbs@coverfox.com.</strong></span></p>
                    <p><span style="color: #000000;">Your will receive your car breakdown service policy document on your registered email immediately after.</span></p>
                    <p><span style="color: #000000;">You are now also eligible for <strong>Additional 10% Discount</strong> on your next car insurance renewal on Coverfox.com.</span></p>
                    <p><span style="color: #000000;">Just use your Membership ID during renewal - {{obj.member_id}}.</span></p>
                    <p><span style="color: #000000;">You are being provided this service for free as a limited period offer from Coverfox.com</span></p>
                    <p><span style="color: #000000;">In case your car insurance has expired, please call Coverfox insurance advisors on the Toll Free Number 1800-209-99-20 to get your insurance issued.</span></p>
                    <p><br /><span style="color: #000000;">Pleasure to service proud car owners.</span><br /><span style="color: #000000;">Team Coverfox</span></p>"""
    },

    'TRANSACTION_DROP_MAIL': {
        'new': """
                <table width="100%" cellpadding="0" cellspacing="0" border="0" align="left">
                    <tr>
                        <td valign="top" align="left" style="padding:10px 30px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#6E7C85;line-height:1.5em;background: #fff;">
                            <p>Hi {{obj.name}},</p>
                            <p>Did you face a problem? We noticed, that you were in the process of insuring your health - but left midway.</p>
                            <p>You can continue from where you left and do not not need to start all over again.</p>
                            <p><a style="font-size:16px;border:1px solid #ff704c;width:180px;padding:7px 0px;outline:none;background-color:#ff704c;color:#fff;text-decoration:none;border-radius:5px;display:inline-block;text-align:center;" href="{{obj.transaction_url}}" target="_blank">Continue transaction</a></p>
                            <p>If you're unsure of whether you should buy, here's what we think -</p>
                            <p>We don't want to tell you, that getting your health insurance is important or don't miss out etc. etc.</p>
                            <p>All we want to tell you is 2 things :</p>
                            <ul style="font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#6E7C85;line-height:1.5em;margin:5px 0 0 8px;  padding: 0 0 0 18px">
                            <li>We will stand by you, not only when you're buying and covering your health - but also if you need to claim</li>
                            <li>Our research is top-notch, and you can call us on <strong>1800-209-99-20</strong>&nbsp;(Toll Free)&nbsp;to help you select the best option.</li>
                            </ul>
                            <p>Hope to see you again. Really.</p>
                            <p>Cheers!<br /> Team Coverfox.</p>
                        </td>
                    </tr>
                </table>""",
        'old': """<p>Hi {{obj.name}},</p>
                    <p>Did you face a problem? We noticed, that you were in the process of insuring your health - but left midway.</p>
                    <p>You can continue from where you left and do not not need to start all over again.</p>
                    <p><a class="btn btn-warning" style="background-color: #e24f05; border: 1px solid rgba(0, 0, 0, 0.06); border-radius: 4px 4px 4px 4px; color: #ffffff; font-size: 12px; font-weight: bold; line-height: 1.3em; padding: 10px;" href="{{obj.transaction_url}}" target="_blank">Continue transaction</a></p>
                    <p>If you're unsure of whether you should buy, here's what we think -</p>
                    <p>We don't want to tell you, that getting your health insurance is important or don't miss out etc. etc.</p>
                    <p>All we want to tell you is 2 things :</p>
                    <ol>
                    <li>We will stand by you, not only when you're buying and covering your health - but also if you need to claim</li>
                    <li>Our research is top-notch, and you can call us on <span>1800-209-99-20 (Toll Free)</span>&nbsp;to help you select the best option.</li>
                    </ol>
                    <p>Hope to see you again. Really.</p>
                    <p>Cheers!<br /> Team Coverfox.</p>"""
    },

    'TRANSACTION_FAILURE_MAIL': {
        'new': """
                <table width="100%" cellpadding="0" cellspacing="0" border="0" align="left">
                    <tr>
                        <td valign="top" align="left" style="padding:10px 30px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#6E7C85;line-height:1.5em;background: #fff;">
                            <p>Hi {{obj.name}},</p>
                            <p>There was some problem with the transaction. You know how technology misbehaves sometimes. We are sorry for the inconvenience.</p>
                            <p>Your transaction is saved here -&nbsp;</p>
                            <p><a style="width:200px;font-size:16px;border:1px solid #ff704c;padding:7px 0px;outline:none;background-color:#ff704c;color:#fff;text-decoration:none;border-radius:5px;display:inline-block;text-align:center;" href="{{site_url}}/health-plan/buy/{{obj.transaction_id}}" target="_blank">Visit Saved Transaction</a></p>
                            <p>You can try the transaction again. Don't worry, you will not have to fill -up the entire form again.</p>
                            <p>In case you still face an issue - please all us on our Toll Free number:&nbsp;<span>1800-209-99-20</span>&nbsp;(Available Monday - Saturday from 10 a.m -7 p.m) or mail us on help@coverfox.com.</p>
                            <p>Cheers,<br /> Team Coverfox</p>
                        </td>
                    </tr>
                </table>""",
        'old': """<p>Hi {{obj.name}},</p>
                <p>There was some problem with the transaction. You know how technology misbehaves sometimes. We are sorry for the inconvenience.</p>
                <p>Your transaction is saved here -&nbsp;</p>
                <p><a class="btn btn-warning" style="background-color: #e24f05; border: 1px solid rgba(0, 0, 0, 0.06); border-radius: 4px 4px 4px 4px; color: #ffffff; font-size: 12px; font-weight: bold; line-height: 1.3em; padding: 10px;" href="{{site_url}}/health-plan/buy/{{obj.transaction_id}}" target="_blank">Visit Saved Transaction</a></p>
                <p>You can try the transaction again. Don't worry, you will not have to fill -up the entire form again.</p>
                <p>In case you still face an issue - please all us on our Toll Free number:&nbsp;<span>1800-209-99-20</span>&nbsp;(Available Monday - Saturday from 10 a.m -7 p.m) or mail us on help@coverfox.com.</p>
                <p>Cheers,<br /> Team Coverfox</p>"""
    },

    'TRANSACTION_SUCCESS_MAIL': {
        'new': """<table width="100%" cellpadding="0" cellspacing="0" border="0" align="left">
                <tr>
                    <td valign="top" align="left" style="padding:10px 30px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#6E7C85;line-height:1.5em;background: #fff;">
                        <p>Hi {{obj.name}},</p>
                        <p>Thank you so much for choosing Coverfox for your health insurance. Your transaction was successful.</p>
                        <p>The details are -</p>
                        <table width="100%" style="background: #eaeaea;" cellpadding="1" cellspacing="1" border="0" align="left">
                        <tbody>
                            <tr>
                                <th style="background: #fff;">Insurance Company</th>
                                <td style="padding-left: 10px;background: #fff;">{{obj.insurer.title}}</td>
                            </tr>
                            <tr>
                                <th style="background: #fff;">Product</th>
                                <td style="padding-left: 10px;background: #fff;">{{obj.product.title}}</td>
                            </tr>
                            <tr>
                                <th style="background: #fff;">Cover</th>
                                <td style="padding-left: 10px;background: #fff;">INR {{obj.cover}}</td>
                            </tr>
                            <tr>
                                <th style="background: #fff;">Premium</th>
                                <td style="padding-left: 10px;background: #fff;">INR {{obj.premium}}</td>
                            </tr>
                            <tr>
                                <th style="background: #fff;">Transaction Number</th>
                                <td style="padding-left: 10px;background: #fff;">{{obj.transaction_number}}</td>
                            </tr>
                        </tbody>
                        </table>
                        <p>&nbsp;</p>
                        {% if obj.policy_document_url %}
                            <p>You can download a soft copy of your policy here -</p>
                            <p><a style="width:160px;font-size:16px;border:1px solid #ff704c;padding:7px 0px;outline:none;background-color:#ff704c;color:#fff;text-decoration:none;border-radius:5px;display:inline-block;text-align:center;" href="{{ obj.policy_document_url }}" target="_blank">Download e-Policy</a></p>
                        {% elif obj.insurer.id == 16 %}
                            <p>Your service manager will process your application with insurance company and keep you posted with any additional requirement.</p>
                            <p>Scanned copy of your policy will be emailed to you within 10 working days and original copy will be couriered within 12 working days.</p>
                            <p>You will receive your electronic cashless card by email within 20 working days.</p>
                        {% else %}
                            <p>You will soon get a PDF copy of the policy from {{obj.insurer.title}}.</p>
                            <p>In case you do not receive the PDF of &nbsp;the policy in the next 48 hours, call us on our Toll Free number: 1800-209-99-70 (Available Monday - Saturday from 9AM to 9PM) or mail us on help@coverfox.com.</p>
                        {% endif %}
                        <p>Cheers! <br />Team Coverfox</p>
                    </td>
                </tr>
            </table>""",
        'old': """<p>Hi {{obj.name}},</p>
                    <p><br />Thank you so much for choosing Coverfox for your health insurance. Your transaction was successful.</p>
                    <p>The details are -</p>
                    <table>
                    <tbody>
                    <tr><th style="border-bottom: 1px solid #eaeaea;">Insurance Company</th>
                    <td style="border-bottom: 1px solid #eaeaea; padding-left: 10px;">{{obj.insurer.title}}</td>
                    </tr>
                    <tr><th style="border-bottom: 1px solid #eaeaea;">Product</th>
                    <td style="border-bottom: 1px solid #eaeaea; padding-left: 10px;">{{obj.product.title}}</td>
                    </tr>
                    <tr><th style="border-bottom: 1px solid #eaeaea;">Cover</th>
                    <td style="border-bottom: 1px solid #eaeaea; padding-left: 10px;">INR {{obj.cover}}</td>
                    </tr>
                    <tr><th style="border-bottom: 1px solid #eaeaea;">Premium</th>
                    <td style="border-bottom: 1px solid #eaeaea; padding-left: 10px;">INR {{obj.premium}}</td>
                    </tr>
                    <tr><th style="border-bottom: 1px solid #eaeaea;">Transaction Number</th>
                    <td style="border-bottom: 1px solid #eaeaea; padding-left: 10px;">{{obj.transaction_number}}</td>
                    </tr>
                    </tbody>
                    </table>
                    <p>{% if obj.policy_document_url %}</p>
                    <p>You can download a soft copy of your policy here -</p>
                    <p>&nbsp;</p>
                    <p><a class="btn btn-warning" style="background-color: #e24f05; border: 1px solid rgba(0, 0, 0, 0.06); border-radius: 4px 4px 4px 4px; color: #ffffff; font-size: 16px; font-weight: bold; line-height: 1.3em; padding: 10px;" href="{{ obj.policy_document_url }}" target="_blank">Download e-Policy</a></p>
                    <p>&nbsp;</p>
                    <p>{% elif obj.insurer.id == 16 %}</p>
                    <p>Your service manager will process your application with insurance company and keep you posted with any additional requirement.</p>
                    <p>Scanned copy of your policy will be emailed to you within 10 working days and original copy will be couriered within 12 working days.</p>
                    <p>You will receive your electronic cashless card by email within 20 working days.</p>
                    <p>{% else %}</p>
                    <p>You will soon get a PDF copy of the policy from {{obj.insurer.title}}.</p>
                    <p>In case you do not receive the PDF of &nbsp;the policy in the next 48 hours, call us on our Toll Free number: 1800-209-99-70 (Available Monday - Saturday from 9AM to 9PM) or mail us on help@coverfox.com.</p>
                    <p>{% endif %}</p>
                    <p>Cheers! <br />Team Coverfox</p>"""
    }
}
