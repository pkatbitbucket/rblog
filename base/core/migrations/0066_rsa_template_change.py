# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def add_rsa_template(apps, schema_editor):
    mtype = "RSA_NOTIFY"
    subject = "Your Get Going Card has been Registered."

    html_body = """
    <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
        <tr>
            <td valign="top" align="left" style="padding:20px 30px 0;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
                <p>Hi {{obj.first_name}} {{obj.last_name}},</p>
                <p>Congratulations! Your Coverfox Get Going Card numbered {{obj.card_id}} has been registered. You will now be able to avail the following services* in case of an emergency:</p>
            </td>
        </tr>
        <tr>
            <td valign="top" align="left" style="padding:5px 30px 0;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
                     <tr>
                        <td width="10" valign="top" align="center" style="padding:5px;font:bold 18px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><img src="https://s3.amazonaws.com/coverfox-mailer/mailer-tick.png" width="12" alt=""></td>
                        <td valign="top" align="left" style="padding:6px 5px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:21px;background: #fff;">On call technical assistance for quick and minor repairs</td>
                    </tr>
                    <tr>
                        <td width="10" valign="top" align="center" style="padding:5px;font:bold 18px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><img src="https://s3.amazonaws.com/coverfox-mailer/mailer-tick.png" width="12" alt=""></td>
                        <td valign="top" align="left" style="padding:6px 5px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:21px;background: #fff;">Free towing up to 40 kms</td>
                    </tr>
                    <tr>
                        <td width="10" valign="top" align="center"  style="padding:5px;font:bold 18px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><img src="https://s3.amazonaws.com/coverfox-mailer/mailer-tick.png" width="12" alt=""></td>
                        <td valign="top" align="left"  style="padding:6px 5px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:21px;background: #fff;">Tyre change labour support</td>
                    </tr>
                    <tr>
                        <td width="10" valign="top" align="center"  style="padding:5px;font:bold 18px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><img src="https://s3.amazonaws.com/coverfox-mailer/mailer-tick.png" width="12" alt=""></td>
                        <td valign="top" align="left"  style="padding:6px 5px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:21px;background: #fff;">Key lost or locked inside support</td>
                    </tr>
                    <tr>
                        <td width="10" valign="top" align="center"  style="padding:5px;font:bold 18px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><img src="https://s3.amazonaws.com/coverfox-mailer/mailer-tick.png" width="12" alt=""></td>
                        <td valign="top" align="left"  style="padding:6px 5px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:21px;background: #fff;">Alternate battery/jump start labour support</td>
                    </tr>
                    <tr>
                        <td width="10" valign="top" align="center"  style="padding:5px;font:bold 18px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><img src="https://s3.amazonaws.com/coverfox-mailer/mailer-tick.png" width="12" alt=""></td>
                        <td valign="top" align="left"  style="padding:6px 5px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:21px;background: #fff;">Emergency fuel arrangement</td>
                    </tr>
                    <tr>
                        <td width="10" valign="top" align="center"  style="padding:5px;font:bold 18px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><img src="https://s3.amazonaws.com/coverfox-mailer/mailer-tick.png" width="12" alt=""></td>
                        <td valign="top" align="left"  style="padding:6px 5px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:21px;background: #fff;">Garage locator/ route guidance support</td>
                    </tr>
                    <tr>
                        <td width="10" valign="top" align="center"  style="padding:5px;font:bold 18px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;"><img src="https://s3.amazonaws.com/coverfox-mailer/mailer-tick.png" width="12" alt=""></td>
                        <td valign="top" align="left"  style="padding:6px 5px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:21px;background: #fff;">Air/ train ticket/ accommodation arrangement</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" align="left" style="padding:0 30px 10px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
                <p>Your services will be activated in the next 72 hours.</p>
                <p><b>Note:</b> Maximum 2 claims are allowed during a 6-month membership and 4 claims during a one-year membership.</p>
                <p>Please ensure that you put the sticker on your car in an area easily accessible at the time of emergency. Visit <a href="http://www.coverfox.com/ggc" target="_target" style="color:#017ABA;outline:none;line-height:17px;text-decoration:none;">www.coverfox.com/ggc</a>. for details on your Get Going Card. </p>
            </td>
        </tr>
    </table>
    """

    text_body = """
    Get Ready to Get Going!
        Hi {{obj.first_name}} {{obj.last_name}},
        Congratulations! Your Coverfox Get Going Card numbered {{obj.card_id}} has been
        registered. You will now be able to avail the following services* in case of an emergency:
            > On call technical assistance for quick and minor repairs.
            > Free towing up to 40 kms
            > Tyre change labour support
            > Key lost or locked inside support
            > Alternate battery/jump start labour support
            > Emergency fuel arrangement
            > Garage locator/ route guidance support
            > Air/ train ticket/ accommodation arrangement
        Your services will be activated in the next 72 hours.
        *Maximum 2 claims are allowed during a 6-month membership and 4 claims during a one-year membership.
        Please ensure that you put the sticker on your car in an area easily accessible at the time of
        emergency. Visit www.coverfox.com/ggc for details on your Get Going Card.
    Warm Regards,
    Team Coverfox
    """

    sms_text = """
    Congratulations! Your Coverfox Get Going Card {{obj.card_num}} has been registered. Your
    services will be activated in the next 72 hours.
    """
    Notification = apps.get_model('core', 'Notification')

    try:
        notification = Notification.objects.get(nid=mtype)
    except Notification.DoesNotExist:
        Notification.objects.create(
            nid=mtype,
            about='Sending RSA Notifications',
            subject_content=subject,
            html_content=html_body,
            text_content=text_body,
            sender_name='Coverfox Support',
            sender_email='help@coverfox.com',
            sms_content=sms_text
        )
    else:
        notification.html_content = html_body
        notification.save()


class Migration(migrations.Migration):
    dependencies = [
        ('core', '0065_auto_20160308_1432'),
    ]

    operations = [
        migrations.RunPython(
            code=add_rsa_template,
            reverse_code=migrations.RunPython.noop,
        )
    ]
