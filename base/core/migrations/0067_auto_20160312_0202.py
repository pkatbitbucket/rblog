# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0066_rsa_template_change'),
    ]

    operations = [
        migrations.AddField(
            model_name='insurer',
            name='creator',
            field=models.ForeignKey(related_name='core_insurer_created', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='insurer',
            name='editor',
            field=models.ForeignKey(related_name='core_insurer_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
