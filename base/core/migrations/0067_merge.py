# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0065_merge'),
        ('core', '0066_rsa_template_change'),
    ]

    operations = [
    ]
