# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0067_merge'),
        ('core', '0067_auto_20160312_0202'),
    ]

    operations = [
    ]
