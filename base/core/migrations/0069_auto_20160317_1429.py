# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0068_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notification',
            name='sender_email',
            field=models.CharField(default=b'dev@cfstatic.org', max_length=100),
        ),
    ]
