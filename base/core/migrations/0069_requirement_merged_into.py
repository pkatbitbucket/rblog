# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0068_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='requirement',
            name='merged_into',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, blank=True, to='core.Requirement', null=True),
        ),
    ]
