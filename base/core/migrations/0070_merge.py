# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0069_auto_20160317_1429'),
        ('core', '0069_requirement_merged_into'),
    ]

    operations = [
    ]
