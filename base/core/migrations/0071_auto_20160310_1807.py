# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def add_myaccount_otp_template(apps, schema_editor):
    mtype = "MYACCOUNT_OTP"
    subject = "OTP to access your Coverfox Account"

    html_body = """<table cellpadding="0" cellspacing="0" border="0" align="left" width="580">
                            <tr>
                                <td valign="top" align="left" style="padding:20px 30px 0;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
                                    <div style="margin:14px 0;">Hi,</div>

                                    <div style="margin:14px 0;">You are almost there. </div>

                                    <div style="margin:14px 0;">Use One-Time Password (OTP) <b>{{ obj.otp }}</b> to access your Coverfox Account. The OTP is valid only for {{ obj.validity }} from the time of request.</div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left" style="padding:0 30px 20px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
                                    <div style="margin:14px 0;">Warm Regards,<br />
                                    Team Coverfox</div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left" style="padding:0">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                         <tr>
                                            <td valign="top" align="left" style="padding:10px;font:normal 12px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#6E7C85;line-height:1.4em;background: #f8f8fa;">
                                                <div style="margin:14px 0;"><b>Important Note:</b> For security reasons do not share this OTP with anyone.</div>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>"""

    text_body = """Hi,
                You are almost there.
                Use One-Time Password (OTP) {{ obj.otp }} to access your Coverfox Account. The OTP is valid only for {{ obj.validity }} from the time of request.
                Warm Regards,
                Team Coverfox"""

    sms_content = ('Hi, OTP is {{ obj.otp }} to access your Coverfox Account. '
                   'For security reasons do not share this OTP with anyone - Team Coverfox.')
    Notification = apps.get_model('core', 'Notification')

    Notification.objects.get_or_create(
        nid=mtype,
        defaults=dict(
            subject_content=subject,
            html_content=html_body,
            text_content=text_body,
            sms_content=sms_content,
            sender_name='Coverfox Support',
            sender_email='help@coverfox.com',
            mta='mandrill'
        ),
    )


def reverse_add_myaccount_otp_template(apps, schema_editor):
    Notification = apps.get_model('core', 'Notification')
    mtype = "MYACCOUNT_OTP"
    try:
        Notification.objects.get(nid=mtype).delete()
    except Notification.DoesNotExist:
        pass


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0070_merge'),
    ]

    operations = [
        migrations.RunPython(
            code=add_myaccount_otp_template,
            reverse_code=reverse_add_myaccount_otp_template,
        )
    ]
