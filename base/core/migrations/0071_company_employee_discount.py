# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0070_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='employee_discount',
            field=models.BooleanField(default=False, verbose_name='Employee Discount'),
        ),
    ]
