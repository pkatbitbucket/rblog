# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def add_endoresement_template(apps, schema_editor):
    mtype = "ENDORSEMENT_NOTIFY_CUSTOMER"
    subject = '{{ obj.customer_name|title }}, requested policy changes have been registered'

    html_body = """<table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
    <tbody>
    <tr>
        <td valign="top" align="left"
            style="padding:20px 20px 0;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
            <div style="margin:14px 0;">Hi {{ obj.customer_name|title }},</div>
            <div style="margin:14px 0;">
                Here's a summary of the changes requested in your {{ obj.policy_name|title }}[{{ obj.policy_number }}]
            </div>
        </td>
    </tr>
    <tr>
        <td valign="top" align="left" colspan="2"
            style="padding:0 20px 5px;background: #fff;">
            <table cellpadding="1" cellspacing="1" border="0" align="center" width="100%" style="background:#dedede;">
                <tbody>
                <tr>
                    <td valign="top" align="center" width="300" style="background: #F6F6F6;font:bold 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;padding:5px;">
                        Requested Change Particulars
                    </td>
                    <td valign="top" align="center" width="140" style="background: #F6F6F6;font:bold 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;padding:5px;">
                        Incorrect Record
                    </td>
                    <td valign="top" align="center" width="140" style="background: #F6F6F6;font:bold 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;padding:5px;">
                        Correct Details
                    </td>
                </tr>
                <tr>
                    <td valign="top" align="center" style="background: #fff;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#5A6974;line-height:20px;padding:5px;">
                        {{ obj.get_endorsement_type_display }} in {{ obj.get_field_display}}
                    </td>
                    <td valign="top" align="center" style="background: #fff;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#5A6974;line-height:20px;padding:5px;">
                        {{ obj.current_value }}
                    </td>
                    <td valign="top" align="center" style="background: #fff;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#5A6974;line-height:20px;padding:5px;">
                        {{ obj.endorsed_value }}
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" align="left" style="padding:20px 20px 10px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
            <div style="margin:14px 0;">
                The endorsement letter confirming the changes will reach you soon.
            </div>
            <div style="margin:14px 0;">An <b>'Acknowledgement of Changes
                Requested' (ACR)</b> has been attached with this email, to help
                you file any claim arising in this period.
            </div>
            <div style="margin:14px 0;">All you need to do is refer this
                acknowledgement along with your policy document while
                communicating with us or the insurance company.
            </div>
        </td>
    </tr>
    <tr>
        <td valign="top" align="left"
            style="padding:10px 20px 20px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
            <div style="margin:14px 0;">Warm Regards,<br>
                Team Coverfox
            </div>
        </td>
    </tr>
    </tbody>
</table>
"""
    Notification = apps.get_model('core', 'Notification')

    try:
        notification = Notification.objects.get(nid=mtype)
    except Notification.DoesNotExist:
        Notification.objects.create(
            nid=mtype,
            about='Sending ENDORESEMENT Notifications to the customer',
            subject_content=subject,
            html_content=html_body,
            sender_name='Coverfox Support',
            sender_email='help@coverfox.com',
        )
    else:
        notification.html_content = html_body
        notification.save()


def delete_endoresement_template(apps, schema_editor):
        mtype = "ENDORSEMENT_NOTIFY_CUSTOMER"
        Notification = apps.get_model('core', 'Notification')
        Notification.objects.get(nid=mtype).delete()


class Migration(migrations.Migration):
    dependencies = [
        ('core', '0065_auto_20160308_1726'),
    ]

    operations = [
        migrations.RunPython(
            code=add_endoresement_template,
            reverse_code=delete_endoresement_template,
        ),
    ]
