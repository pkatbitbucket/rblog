# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0065_updating_email_templates'),
        ('core', '0070_merge'),
    ]

    operations = [
    ]
