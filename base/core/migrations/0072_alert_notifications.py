# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models

NID_TEMPLATE_MAPPING = {
    'JARVIS_TRANSACTION_DROP': {
        'about': 'Sending Transaction Drop notification',
        'to': ['rathi@coverfoxmail.com',],
        'subject_content': 'ALERT - Transaction Drop',
        'html_content': """<table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
                                <tr>
                                    <td valign="top" align="left" style="padding:20px 30px 0;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
                                        <p>Requirement Id = {{obj.requirement_id}}</p>
                                        <p>Customer Name = {{obj.customer_name}}</p>
                                        <p>Assigned To = {{obj.assigned_to}}</p>
                                        <p>Phone Number = {{obj.phone_number}}</p>
                                    </td>
                                </tr>
                            </table>""",
        'text_content': """
                Requirement Id = {{obj.requirement_id}}
                Customer Name = {{obj.customer_name}}
                Assigned To = {{obj.assigned_to}}
                Phone Number = {{obj.phone_number}}
            """,
        'sender_name': 'Jarvis Support',
        'sender_email': 'jarvis@coverfox.com',
    },
    'JARVIS_PAYMENT_FAILURE': {
        'about': 'Sending Payment Failure notification',
        'to': ['rathi@coverfoxmail.com',],
        'subject_content': 'ALERT - Payment Failure',
        'html_content': """<table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
                                <tr>
                                    <td valign="top" align="left" style="padding:20px 30px 0;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
                                        <p>Requirement Id = {{obj.requirement_id}}</p>
                                        <p>Customer Name = {{obj.customer_name}}</p>
                                        <p>Assigned To = {{obj.assigned_to}}</p>
                                        <p>Phone Number = {{obj.phone_number}}</p>
                                    </td>
                                </tr>
                            </table>""",
        'text_content': """
                Requirement Id = {{obj.requirement_id}}
                Customer Name = {{obj.customer_name}}
                Assigned To = {{obj.assigned_to}}
                Phone Number = {{obj.phone_number}}
            """,
        'sender_name': 'Jarvis Support',
        'sender_email': 'jarvis@coverfox.com',
    },
}

def create_notification_templates(apps, schema_editor):
    Notification = apps.get_model('core', 'Notification')
    for nid, template in NID_TEMPLATE_MAPPING.items():
        n, _ = Notification.objects.get_or_create(nid=nid)
        n.html_content = template['html_content']
        n.about = template['about']
        n.to = template['to']
        n.subject_content = template['subject_content']
        n.html_content = template['html_content']
        n.text_content = template['text_content']
        n.sender_name = template['sender_name']
        n.sender_email = template['sender_email']
        n.save()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0071_merge'),
    ]

    operations = [
        migrations.RunPython(create_notification_templates, reverse_code=migrations.RunPython.noop),
    ]
