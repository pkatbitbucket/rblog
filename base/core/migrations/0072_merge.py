# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0071_endorsment_template'),
        ('core', '0070_merge'),
        ('core', '0071_auto_20160310_1807'),
        ('core', '0071_merge'),
    ]

    operations = [
    ]
