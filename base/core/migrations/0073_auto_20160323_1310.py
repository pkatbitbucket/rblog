# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0072_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='endorsementdetail',
            name='customer_email',
            field=core.fields.CIEmailField(max_length=255, verbose_name='Customer Email', db_index=True),
        ),
    ]
