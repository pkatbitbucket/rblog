# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def update_and_create_email_template(apps, schema_editor):
    Notification = apps.get_model('core', 'Notification')
    update_template(Notification, 'new')


def reverse_update_and_create_email_template(apps, schema_editor):
    Notification = apps.get_model('core', 'Notification')
    update_template(Notification, 'old')


def update_template(Model, version):
    for nid, template in NID_TEMPLATE_MAPPING.items():
        try:
            notification = Model.objects.get(nid=nid)
        except Model.DoesNotExist:
            pass
        else:
            data = template[version]
            for k, v in data.items():
                setattr(notification, k, v)
            notification.save(update_fields=data.keys())


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0072_merge'),
    ]

    operations = [
        migrations.RunPython(
            code=update_and_create_email_template,
            reverse_code=reverse_update_and_create_email_template,
        )
    ]


NID_TEMPLATE_MAPPING = {

    'USER_ACCOUNT_CREATION': {
        'new': {
            'subject_content': '{{ obj.name }}, your {{ obj.transaction_type|title }} Insurance Policy is now available',

            'html_content': """<table cellpadding="0" cellspacing="0" border="0" align="left" width="580">
                            <tr>
                                <td valign="top" align="left" style="padding:20px 30px 0;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
                                    <div style="margin:14px 0;">Hi {{ obj.name }},</div>

                                    <div style="margin:14px 0;">Your <b>{{ obj.transaction_type|title }}</b> insurance, purchased on Coverfox.com, is now available.</div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left" style="padding:0px 30px;">
                                    <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
                                        <tr>
                                            <td valign="top" align="left" style="padding:0px 0px 10px 0px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
                                            Now access and manage all your policies with the <b>Coverfox Account</b>, your <b>one-stop destination</b> to:
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left" style="padding:0px;">
                                                <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
                                                    <tr>
                                                        <td width="25" valign="top" align="left" style="padding:7px 0px 5px 0px;">
                                                            <table cellpadding="0" cellspacing="0" border="0" align="left">
                                                                <tr>
                                                                    <td valign="middle" align="left" height="6" width="6" style=" background: #FF704C;padding:0px;"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top" align="left" style="padding:0px 0px 8px 0px;font:normal 13px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#687480;line-height:1.5em;"><b>Access</b> all your insurance policies bought through Coverfox
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="25" valign="top" align="left" style="padding:7px 0px 5px 0px;">
                                                            <table cellpadding="0" cellspacing="0" border="0" align="left">
                                                                <tr>
                                                                    <td valign="middle" align="left" height="6" width="6" style=" background: #FF704C;padding:0px;"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top" align="left" style="padding:0px 0px 8px 0px;font:normal 13px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#687480;line-height:1.5em;"><b>Track</b> the expiry dates, add-on features (and a lot more) of your insurance policies
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left" style="padding:0px 30px 10px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
                                <div style="margin:14px 0;">All you have to do is click on the link below and fill in your registered email address or mobile number.</div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center" style="padding:10px 30px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;">
                                    <a href="{{ obj.manage_policies_url }}" target="_blank" title="Lowest car insurance prices" style="font-size:16px;border:1px solid #ff704c;width:185px;padding:12px 0;outline:none;background-color:#FF704C;color:#fff;text-decoration:none;border-radius:1px;display: inline-block;text-align:center; box-shadow:1px 1px 1px #C8C8C8;border-radius:5px;">View Policy Now</a>
                                </td>
                            </tr>

                            {% if obj.transaction_type == 'car' %}
                            <tr>
                                <td valign="top" align="left" style="padding:0 30px;font:bold 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
                                    <div style="margin:14px 0 0;">That's not all,</div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left" style="padding:0 30px 5px;">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" >
                                        <tr>
                                             <td valign="top" align="left" style="padding:17px 15px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;border:1px solid #6CCFCF;">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" >
                                                    <tr>
                                                        <td valign="top" width="18" align="left" style="padding:0px;">
                                                            <img src="https://cmsblogimages.s3.amazonaws.com/mailer-towing-truck.png" width="28" alt="">
                                                        </td>
                                                        <td colspan="2" valign="top" align="left" style="padding:0px 0px 5px 12px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.2em;">
                                                            We are providing you <b>24x7 Road Side Assistance</b> complimentary with your car insurance policy. The RSA policy is also available on your Coverfox account.

                                                        </td>
                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            {% endif %}

                            <tr>
                                <td valign="top" align="left" style="padding:10px 30px 20px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
                                    <div style="margin:14px 0;">Warm Regards,<br />
                                    Team Coverfox</div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left" style="padding:0">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                         <tr>
                                            <td valign="top" align="center" style="padding:10px;font:normal 12px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#6E7C85;line-height:1.4em;background: #f8f8fa;">
                                                Have questions, suggestions or complaints? Connect with our Customer Service Unit: <br /><b>Toll free no.</b>: 1800 209 9970 (Mon to Sat, &#173;9am&ndash;9pm) <b>Email</b>: <a href="mailto:help@coverfox.com" target="_target" style="color:#017ABA;outline-style:none;line-height:17px;text-decoration:none;">help@coverfox.com</a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>""",

            'text_content': """Hi {{ obj.name }},
                            Your {{ obj.transaction_type|title }} insurance, purchased on Coverfox.com, is now available.
                            Now access and manage all your policies with the Coverfox Account, your one-stop destination to:
                            Access all your insurance policies bought through Coverfox
                            Track the expiry dates, add-on features (and a lot more) of your insurance policies
                            All you have to do is click on the link below and fill in your registered email address or mobile number.
                            View Policy Now
                            {% if obj.transaction_type == 'car' %}
                            That's not all,
                                We are providing you 24x7 Road Side Assistance complimentary with your car insurance policy. The RSA policy is also available on your Coverfox account.
                            {% endif %}
                            Warm Regards,
                            Team Coverfox""",
        },
        'old': {
            'subject_content': 'Your Coverfox Account is now live',

            'html_content': """<p>Dear {{ obj.user.get_full_name }},</p>
                            <p>Welcome to the Coverfox Family!</p>
                            <p>We are delighted to have you on board and promise to make things very simple for you.</p>
                            <p>Your personalized Coverfox Account has been created and includes all your insurance policies for easy and quick access to them. You can now access and manage them from our website (https://www.coverfox.com/) with a single click using the &ldquo;MANAGE POLICIES&rdquo; link on the top right corner.</p>
                            <p>Here is what you need to do:</p>
                            <p>1. Go to {{ obj.reset_url }}?utm_source=email.</p>
                            <p>2. Create your password and login.</p>
                            <p>3. View and manage all your existing and future policies.</p>
                            <p>From your account you can:<br />a. Download your policy<br />b. View your policy details<br />c. Initiate a claim for your policy</p>
                            <p>{% if obj.rsa_policy %}<br />We have also provided you a FREE 24/7 ROADSIDE ASSISTANCE POLICY to handle all your worries in case of any emergency breakdown. The 24/7 RSA Policy can be accessed from Your Account. The detailed features are mentioned on the policy copy.<br />{% endif %}</p>
                            <p>Our team of Service Managers are available to help you through all required clarifications and issues regarding your policy and claims, or any feedback. Please call us on 1800 209 9970, anytime between 9AM and 9PM, Monday to Saturday, and we shall try and provide a swift resolution.</p>
                            <p>We would be happy to assist you with Health, Travel and Home insurances too.</p>
                            <p>Extending the assurance of the best insurance and services always.</p>""",

            'text_content': """Dear {{ obj.user.get_full_name }},

                            Welcome to the Coverfox Family!

                            We are delighted to have you on board and promise to make things very simple for you.

                            Your personalized Coverfox Account has been created and includes all your insurance policies for easy and quick access to them. You can now access and manage them from our website (https://www.coverfox.com/) with a single click using the “MANAGE POLICIES” link on the top right corner.

                            Here is what you need to do:

                            1. Go to {{ obj.reset_url }}?utm_source=email.

                            2. Create your password and login.

                            3. View and manage all your existing and future policies.

                            From your account you can:
                            a. Download your policy
                            b. View your policy details
                            c. Initiate a claim for your policy

                            {% if obj.rsa_policy %}
                            We have also provided you a FREE 24/7 ROADSIDE ASSISTANCE POLICY to handle all your worries in case of any emergency breakdown. The 24/7 RSA Policy can be accessed from Your Account. The detailed features are mentioned on the policy copy.
                            {% endif %}

                            Our team of Service Managers are available to help you through all required clarifications and issues regarding your policy and claims, or any feedback. Please call us on 1800 209 9970, anytime between 9AM and 9PM, Monday to Saturday, and we shall try and provide a swift resolution.

                            We would be happy to assist you with Health, Travel and Home insurances too.

                            Extending the assurance of the best insurance and services always.""",
        },
    },

    'POLICY_UPLOAD_NOTIFICATION': {
        'new': {
            'subject_content': '{{ obj.name }}, your {{ obj.transaction_type|title }} Insurance Policy is now available',

            'html_content': """<table cellpadding="0" cellspacing="0" border="0" align="left" width="580">
                            <tr>
                                <td valign="top" align="left" style="padding:20px 30px 0;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
                                    <div style="margin:14px 0;">Hi {{ obj.name }},</div>

                                    <div style="margin:14px 0;">Thank you for choosing Coverfox once again.</div>

                                    <div style="margin:14px 0;">Your <b>{{ obj.transaction_type|title }}</b> insurance, purchased on Coverfox.com, is now available in your <b>Coverfox Account</b>. All you have to do is click on the link below and fill in your registered mobile number.</div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center" style="padding:0 30px 20px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;">
                                    <a href="{{ obj.manage_policies_url }}" target="_blank" title="Lowest car insurance prices" style="font-size:16px;border:1px solid #ff704c;width:185px;padding:12px 0;outline:none;background-color:#FF704C;color:#fff;text-decoration:none;border-radius:1px;display: inline-block;text-align:center; box-shadow:1px 1px 1px #C8C8C8;border-radius:5px;">View Policy Now</a>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left" style="padding:0px 30px;">
                                    <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
                                        <tr>
                                            <td valign="top" align="left" style="padding:0px;">
                                                <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
                                                    <tr>
                                                        <td valign="top" align="left" colspan="2" style="padding:0px 0px 10px 0px;font:bold 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
                                                        Coverfox Account Key Features
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="25" valign="top" align="left" style="padding:7px 0px 5px 0px;">
                                                            <table cellpadding="0" cellspacing="0" border="0" align="left">
                                                                <tr>
                                                                    <td valign="middle" align="left" height="6" width="6" style=" background: #FF704C;padding:0px;"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top" align="left" style="padding:0px 0px 8px 0px;font:normal 13px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#687480;line-height:1.5em;"><b>Access</b> all your insurance policies bought through Coverfox
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="25" valign="top" align="left" style="padding:7px 0px 5px 0px;">
                                                            <table cellpadding="0" cellspacing="0" border="0" align="left">
                                                                <tr>
                                                                    <td valign="middle" align="left" height="6" width="6" style=" background: #FF704C;padding:0px;"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top" align="left" style="padding:0px 0px 8px 0px;font:normal 13px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#687480;line-height:1.5em;"><b>Track</b> the expiry dates, add-on features (and a lot more) of your insurance policies
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            {% if obj.transaction_type == 'car' %}
                            <tr>
                                <td valign="top" align="left" style="padding:0 30px;font:bold 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
                                    <div style="margin:14px 0 0;">That's not all,</div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left" style="padding:0 30px 5px;">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" >
                                        <tr>
                                             <td valign="top" align="left" style="padding:17px 15px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;border:1px solid #6CCFCF;">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" >
                                                    <tr>
                                                        <td valign="top" width="18" align="left" style="padding:0px;">
                                                            <img src="https://cmsblogimages.s3.amazonaws.com/mailer-towing-truck.png" width="28" alt="">
                                                        </td>
                                                        <td colspan="2" valign="top" align="left" style="padding:0px 0px 5px 12px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.2em;">
                                                            We are providing you <b>24x7 Road Side Assistance</b> complimentary with your car insurance policy. The RSA policy is also available on your Coverfox Account.

                                                        </td>
                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            {% endif %}

                            <tr>
                                <td valign="top" align="left" style="padding:10px 30px 20px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:1.5em;">
                                    <div style="margin:14px 0;">Warm Regards,<br />
                                    Team Coverfox</div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left" style="padding:0">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                         <tr>
                                            <td valign="top" align="center" style="padding:10px;font:normal 12px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#6E7C85;line-height:1.4em;background: #f8f8fa;">
                                                Have questions, suggestions or complaints? Connect with our Customer Service Unit: <br /><b>Toll free no.</b>: 1800 209 9970 (Mon to Sat, &#173;9am&ndash;9pm) <b>Email</b>: <a href="mailto:help@coverfox.com" target="_target" style="color:#017ABA;outline-style:none;line-height:17px;text-decoration:none;">help@coverfox.com</a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>""",

            'text_content': """Hi {{ obj.name }},
                            Thank you for choosing Coverfox once again.
                            Your {{ obj.transaction_type|title }}, purchased on Coverfox.com, is now available in your Coverfox Account. All you have to do is click on the link below and fill in your registered mobile number.
                            View Policy Now
                            Coverfox Account Key Features
                            Access all your insurance policies bought through Coverfox
                            Track the expiry dates, add-on features (and a lot more) of your insurance policies
                            {% if obj.transaction_type == 'car' %}
                            That's not all,
                            We are providing you 24x7 Road Side Assistance complimentary with your car insurance policy. The RSA policy is also available on your Coverfox Account.
                            {% endif %}
                            Warm Regards,
                            Team Coverfox""",
        },
        'old': {
            'subject_content': 'Your {{ obj.transaction_type|title }} Insurance policy document is added to your Coverfox Account',

            'html_content': """<p>Dear {{ obj.user.get_full_name }},</p>
                            <p>Thank you for choosing Coverfox for your {{ obj.transaction_type|title }} Insurance and trusting Coverfox once again.</p>
                            <p>We are delighted to have you back and promise to continue making things very simple for you.</p>
                            <p>Your personalized Coverfox Account is already active and includes all your insurance policies for easy and quick access to them. You can access your Account using the &ldquo;MANAGE POLICIES&rdquo; link on the top right corner of our website.</p>
                            <p>Here is what you need to do:</p>
                            <p>1. Go to {{ obj.user_login_url }}.</p>
                            <p>2. Login to your account.</p>
                            <p>3. View and manage all your existing and future policies.</p>
                            <p>From your account you can:<br />a. Download your policy<br />b. View your policy details<br />c. Initiate a claim for your policy</p>
                            <p>{% if obj.rsa_policy %}<br />We have also provided you a FREE 24/7 ROADSIDE ASSISTANCE POLICY to handle all your worries in case of any emergency breakdown. The 24/7 RSA Policy can be accessed from Your Account. The detailed features are mentioned on the policy copy.<br />{% endif %}</p>
                            <p>Our team of Service Managers are available to help you through all required clarifications and issues regarding your policy and claims, or any feedback. Please call us on 1800 209 9970, anytime between 9AM and 9PM, Monday to Saturday, and we shall try and provide a swift resolution.</p>
                            <p>We would be happy to assist you with {{ obj.other_products|join:', '|title }}&nbsp;insurances too.</p>
                            <p>Extending the assurance of the best insurance and services always.</p>""",

            'text_content': """Dear {{ obj.user.get_full_name }},

                            Thank you for choosing Coverfox for your {{ obj.transaction_type|title }} Insurance and trusting Coverfox once again.

                            We are delighted to have you back and promise to continue making things very simple for you.

                            Your personalized Coverfox Account is already active and includes all your insurance policies for easy and quick access to them. You can access your Account using the “MANAGE POLICIES” link on the top right corner of our website.

                            Here is what you need to do:

                            1. Go to {{ obj.user_login_url }}.

                            2. Login to your account.

                            3. View and manage all your existing and future policies.

                            From your account you can:
                            a. Download your policy
                            b. View your policy details
                            c. Initiate a claim for your policy

                            {% if obj.rsa_policy %}
                            We have also provided you a FREE 24/7 ROADSIDE ASSISTANCE POLICY to handle all your worries in case of any emergency breakdown. The 24/7 RSA Policy can be accessed from Your Account. The detailed features are mentioned on the policy copy.
                            {% endif %}

                            Our team of Service Managers are available to help you through all required clarifications and issues regarding your policy and claims, or any feedback. Please call us on 1800 209 9970, anytime between 9AM and 9PM, Monday to Saturday, and we shall try and provide a swift resolution.

                            We would be happy to assist you with {{ obj.other_products|join:', '|title }} insurances too.

                            Extending the assurance of the best insurance and services always.""",
        },
    },
}
