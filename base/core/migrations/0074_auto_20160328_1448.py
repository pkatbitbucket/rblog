# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0073_auto_20160323_1310'),
    ]

    operations = [
        migrations.CreateModel(
            name='EndorsementReciept',
            fields=[
                ('document_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.Document')),
            ],
            options={
                'abstract': False,
            },
            bases=('core.document',),
        ),
        migrations.CreateModel(
            name='UserDocument',
            fields=[
                ('document_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.Document')),
            ],
            options={
                'abstract': False,
            },
            bases=('core.document',),
        ),
        migrations.AlterModelOptions(
            name='endorsementdetail',
            options={'permissions': (('view_endorsementDetail', 'Can view endorsements'),)},
        ),
        migrations.AddField(
            model_name='endorsementdetail',
            name='completed_on',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='endorsementdetail',
            name='customer_email',
            field=core.fields.CIEmailField(max_length=254, verbose_name='Customer Email', db_index=True),
        ),
        migrations.AlterField(
            model_name='endorsementdetail',
            name='customer_name',
            field=models.CharField(max_length=30),
        ),
        migrations.AlterField(
            model_name='endorsementdetail',
            name='endorsement_type',
            field=models.CharField(max_length=30, choices=[(b'addition', b'Addition'), (b'correction', b'Correction'), (b'change', b'Change'), (b'deletion', b'Deletion'), (b'recovery', b'Recovery')]),
        ),
        migrations.AlterField(
            model_name='endorsementdetail',
            name='field',
            field=models.CharField(max_length=30, choices=[(b'email_id', b'Email_Id'), (b'owner', b'Owner'), (b'salutation', b'Salutation'), (b'occupation', b'Occupation'), (b'passport_no', b'Passport_No'), (b'fuel_type', b'Fuel_Type'), (b'vehicle', b'Vehicle'), (b'ped', b'Ped'), (b'voluntary_deductable', b'Voluntary_Deductable'), (b'engine_no', b'Engine_No'), (b'passenger_cover', b'Passenger_Cover'), (b'hypothecation', b'Hypothecation'), (b'relationship', b'Relationship'), (b'contact_no', b'Contact_No'), (b'insured', b'Insured'), (b'registration_place', b'Registration_Place'), (b'address', b'Address'), (b'ncb', b'Ncb'), (b'name', b'Name'), (b'policy_period', b'Policy_Period'), (b'dob', b'Dob'), (b'gender', b'Gender'), (b'registration_no', b'Registration_No'), (b'chassis_no', b'Chassis_No'), (b'idv', b'Idv')]),
        ),
        migrations.AlterField(
            model_name='endorsementdetail',
            name='policy_name',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='endorsementdetail',
            name='policy_number',
            field=models.CharField(max_length=30),
        ),
        migrations.AlterField(
            model_name='endorsementdetail',
            name='product',
            field=models.CharField(max_length=250, choices=[(b'car', b'car'), (b'travel', b'travel'), (b'bike', b'bike'), (b'health', b'health')]),
        ),
        migrations.AddField(
            model_name='endorsementdetail',
            name='reciept',
            field=models.OneToOneField(related_name='endorsement', null=True, blank=True, to='core.EndorsementReciept'),
        ),
        migrations.AddField(
            model_name='endorsementdetail',
            name='user_documents',
            field=models.ManyToManyField(related_name='endorsements', to='core.UserDocument', blank=True),
        ),
        migrations.AddField(
            model_name='endorsementdetail',
            name='insurer',
            field=models.CharField(default='', max_length=250, choices=[(b'car', ((b'l-t', b'L & T'), (b'iffco-tokio', b'Iffco Tokio'), (b'bharti-axa', b'Bharti Axa'), (b'tata-aig', b'Tata Aig'), (b'reliance', b'Reliance'), (b'bajaj-allianz', b'Bajaj Allianz'), (b'hdfc-ergo', b'HDFC Ergo'), (b'universal-sompo', b'Universal Sompo'), (b'liberty', b'Liberty Videocon'))), (b'bike', ((b'l-t', b'L & T'), (b'iffco-tokio', b'Iffco Tokio'), (b'bharti-axa', b'Bharti Axa'), (b'tata-aig', b'Tata Aig'), (b'reliance', b'Reliance'), (b'bajaj-allianz', b'Bajaj Allianz'), (b'hdfc-ergo', b'HDFC Ergo'), (b'universal-sompo', b'Universal Sompo'), (b'liberty', b'Liberty Videocon'))), (b'health', ((b'religare', b'Religare'), (b'l-t', b'L & T'), (b'apollo', b'Apollo Munich'), (b'max-bupa', b'Max Bupa'), (b'iffco-tokio', b'Iffco Tokio'), (b'bharti-axa', b'Bharti Axa'), (b'tata-aig', b'Tata Aig'), (b'star', b'Star Health'), (b'oriental', b'Oriental Health'), (b'bajaj-allianz', b'Bajaj Allianz'), (b'cigna-ttk', b'Cigna TTK'), (b'hdfc-ergo', b'HDFC Ergo'))), (b'travel', ((b'religare', b'Religare'), (b'apollo', b'Apollo Munich'), (b'bharti-axa', b'Bharti Axa'), (b'reliance', b'Reliance'), (b'bajaj-allianz', b'Bajaj Allianz'), (b'hdfc-ergo', b'HDFC Ergo')))]),
            preserve_default=False,
        ),
    ]
