# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0072_alert_notifications'),
        ('core', '0074_auto_20160328_1448'),
    ]

    operations = [
    ]
