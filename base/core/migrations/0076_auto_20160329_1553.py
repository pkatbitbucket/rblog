# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def add_endoresement_template(apps, schema_editor):
    mtype = "ENDORSEMENT_NOTIFY_INSURER"
    subject = 'Non-Financial Endorsement Request from Customer'

    html_body = """<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
    <tr>
        <td valign="top" align="left" colspan="2"
            style="padding:15px 30px 5px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;">
            <p>Request number: {{ obj.id }}</p>
            <p>Date of request: {{ obj.created_date|date:'DATETIME_FORMAT' }}</p>
            <p>Dear Team,</p>

            <p>We have received a request for change in policy particulars from our customer. Details are as follows:</p>
            <p>Policy number: {{ obj.policy_number }}</p>
            <p>Name of customer: {{ obj.customer_name }}</p>
        </td>
    </tr>
    <tr>
        <td valign="top" align="left" colspan="2"
            style="padding:5px 30px;font:normal 16px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;background: #fff;">
            <table cellpadding="1" cellspacing="1" border="0" align="center" width="100%" style="background:#dedede;">
                <tbody><tr>
                    <td valign="top" align="center" width="300" style="background: #F6F6F6;font:bold 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;padding:5px;">Requested change in field</td>
                    <td valign="top" align="center" width="140" style="background: #F6F6F6;font:bold 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;padding:5px;">Incorrect Record</td>
                    <td valign="top" align="center" width="140" style="background: #F6F6F6;font:bold 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;padding:5px;">Correct Details</td>
                </tr>
                 <tr>
                    <td valign="top" align="center" style="background: #fff;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#5A6974;line-height:20px;padding:5px;">{{ obj.get_endorsement_type_display }} in {{ obj.get_field_display }}</td>
                    <td valign="top" align="center" style="background: #fff;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#5A6974;line-height:20px;padding:5px;">{{ obj.current_value|default:'NA'}}</td>
                    <td valign="top" align="center" style="background: #fff;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#5A6974;line-height:20px;padding:5px;">{{ obj.endorsed_value|default:'NA'}}</td>
                </tr>
            </tbody></table>
        </td>
    </tr>
    <tr>
        <td valign="top" align="left" colspan="2"
            style="padding:15px 30px 5px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;">
            <p>Please process the same at the earliest and help us with the endorsement copy.</p>
        </td>
    </tr>
    <tr>
        <td valign="top" align="left" colspan="2"
            style="padding:0 30px;font:normal 14px 'Lato','Helvetica Neue',Helvetica,Tahoma,Arial,sans-serif;color:#314451;line-height:20px;">
            <p>Regards,<br/>
                Team Coverfox</p>
                <p>In case you have any queries here, please contact us on the following numbers: <br/>
                contact details</p>
        </td>
    </tr>
</table>
"""
    Notification = apps.get_model('core', 'Notification')

    try:
        notification = Notification.objects.get(nid=mtype)
    except Notification.DoesNotExist:
        Notification.objects.create(
            nid=mtype,
            about='Sending ENDORESEMENT Notifications to the insurer',
            subject_content=subject,
            html_content=html_body,
            sender_name='Coverfox Support',
            sender_email='help@coverfox.com',
        )
    else:
        notification.html_content = html_body
        notification.save()


def delete_endoresement_template(apps, schema_editor):
        mtype = "ENDORSEMENT_NOTIFY_INSURER"
        Notification = apps.get_model('core', 'Notification')
        Notification.objects.get(nid=mtype).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0075_merge'),
    ]

    operations = [
        migrations.RunPython(
            code=add_endoresement_template,
            reverse_code=delete_endoresement_template,
        )
    ]
