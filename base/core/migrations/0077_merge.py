# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0076_auto_20160329_1553'),
        ('core', '0074_merge'),
    ]

    operations = [
    ]
