# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import re
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0078_auto_20160404_1940'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phone',
            name='number',
            field=models.CharField(db_index=True, max_length=20, validators=[django.core.validators.RegexValidator(re.compile(b'^[7-9]\\d{9}$'), message='Enter a valid integer.', code=b'invalid')]),
        ),
    ]
