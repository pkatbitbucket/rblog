# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0071_company_employee_discount'),
        ('core', '0079_auto_20160404_2221'),
    ]

    operations = [
    ]
