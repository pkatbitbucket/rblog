import datetime
import importlib
import json
import logging
import os
import uuid
from collections import OrderedDict

from autoslug import AutoSlugField
from django.conf import settings
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import ArrayField, HStoreField
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.core.files.storage import default_storage
from django.db import transaction as django_transaction
from django.db import connection, models
from django.db.models import F, Q, Count
from django.template import TemplateDoesNotExist, engines
from django.template.loader import get_template
from django.utils import timezone
from django.utils.encoding import force_str
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import UUIDField
from django_pgjson import fields as pgjson_fields
from jsonfield import JSONField

import utils  # DO NOT DELETE THIS LINE EVER. THIS IS USED BY STATEMACHINE.models
from cfutils import redis_utils
from core import fields as custom_fields
from core.users import User
from core.validators import mobile_validator
from customdb.thumbs import ImageWithThumbsField
from otp import OTPModelMixin
from statemachine import models as sm_models

logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)
extlogger = logging.getLogger('extensive')


def get_document_upload_path(self, filename):
    return self.get_document_upload_path(filename)


def get_mugshot_path(instance, filename):
    now = datetime.datetime.now()
    fname, ext = os.path.splitext(filename)
    new_fname = utils.slugify(fname)
    newfilename = "%s-%s.%s" % (new_fname, now.strftime("%I%M%S"), ext)
    path_to_save = "uploads/profile/images/%s" % (newfilename)
    return path_to_save

AGE_LIMITS = [(i, i) for i in range(0, 101)]
AGE_LIMITS_1 = [(i, "%d months" % (i)) for i in range(0, 12)]
AGE_LIMITS_1.extend([(i, "%d years" % (i / 1000))
                     for i in range(1000, 101000, 1000)])


def cache_fun(self, data=None, extra=None):
    if data is None:
        # Get data from cache.
        cache_key = utils.get_cache_key(self, extra=extra)
        cached_data = cache.get(cache_key)

        if cached_data is None:
            return cached_data

        try:
            data = json.loads(cached_data)
        except ValueError:
            return data
        else:
            return data
    else:
        # Cache data into redis.
        cache_key = utils.get_cache_key(self, extra=extra)
        # Set cache key.
        is_dict = isinstance(data, dict)
        is_tuple = isinstance(data, tuple)
        is_list = isinstance(data, list)

        if is_dict or is_tuple or is_list:
            data = json.dumps(data)

        cache.set(cache_key, data)

models.Model.cache = cache_fun


def load_form(form_section):
    # Get form from form_section.
    form_path = form_section.form
    form_data = form_section.data

    splited_form_path = form_path.split(".")
    module = importlib.import_module('.'.join(splited_form_path[:-1]))
    form = getattr(module, splited_form_path[-1])
    return form(data=form_data)


class UUIDField32(models.UUIDField):
    def get_db_prep_value(self, value, connection, prepared=False):
        if value is None:
            return None
        if not isinstance(value, uuid.UUID):
            try:
                value = uuid.UUID(value)
            except AttributeError:
                raise TypeError(self.error_messages['invalid'] % {'value': value})
        return value.hex


class AbstractBaseModel(object):

    def __unicode__(self):
        try:
            return self.name
        except:
            try:
                return self.title
            except:
                return ''


class BaseModel(AbstractBaseModel, models.Model):

    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class BaseAuthorModel(BaseModel):
    creator = models.ForeignKey(
        'core.User', null=True, blank=True, related_name='%(app_label)s_%(class)s_created')
    editor = models.ForeignKey('core.User', null=True, blank=True,
                               related_name='%(app_label)s_%(class)s_last_modified')

    class Meta:
        abstract = True


class GenericBaseModel(BaseAuthorModel):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        abstract = True


class InvalidUserEmail(ValueError):
    pass


class Document(BaseAuthorModel):
    name = models.CharField(max_length=255, blank=True)
    document = models.FileField(upload_to=get_document_upload_path, blank=True, max_length=255)
    app = models.CharField(max_length=255, blank=True, null=True)
    is_private = models.BooleanField(default=False)

    def get_absolute_url(self):
        # TODO: Serving private files through a masked URL.
        return self.document.url

    url = property(get_absolute_url)

    def get_document_upload_path(self, filename):
        fname, ext = os.path.splitext(filename)
        r = lambda s: s.replace(' ', '_')
        return os.path.join(
            "uploads",
            self._meta.app_label,
            r(str(self._meta.verbose_name_plural)),
            r(self.name + ext),
        )

    def save(self, *args, **kwargs):
        if not self.name:
            self.name = os.path.splitext(self.document.name)[0].split(os.path.sep)[-1]
        return super(Document, self).save(*args, **kwargs)

    def __unicode__(self):
        return '%s' % (self.name, )


def get_insurer_logo_path(self, filename):
    return os.path.join(
        self._meta.app_label, self._meta.model_name, self.slug, 'logo' + os.path.splitext(filename)[1]
    )


class Insurer(BaseAuthorModel):
    name = models.CharField(max_length=200)
    slug = AutoSlugField(populate_from='name', always_update=True, unique=True, max_length=200)
    logo = ImageWithThumbsField(upload_to=get_insurer_logo_path, sizes=(('s', 300, 300), ('t', 500, 500)))
    body = models.TextField(blank=True)


class Mail(models.Model):
    MAIL_CHOICES = (
        ('ON_SUBSCRIPTION', 'ON_SUBSCRIPTION (subscriber)'),
        ('ON_REVIEW_SIGNUP', 'ON_REVIEW_SIGNUP (subscriber)'),
        ('INVITE_MAIL', 'INVITE_MAIL (subscriber, invited_by)'),
        ('TRANSACTION_SUCCESS_MAIL', 'TRANSACTION_SUCCESS_MAIL (obj)'),
        ('TRANSACTION_FAILURE_MAIL', 'TRANSACTION_FAILURE_MAIL (obj)'),
        ('TRANSACTION_DROP_MAIL', 'TRANSACTION_DROP_MAIL (obj)'),
        ('RESULTS_DROP_MAIL', 'RESULTS_DROP_MAIL (obj)'),
        ('RESULTS_DROP_MAIL2', 'RESULTS_DROP_MAIL2 (obj)'),
        ('VERIFIED_WELCOME_MAIL', 'VERIFIED_WELCOME_MAIL (obj)'),
        ('UNFERIFIED_WELCOME_MAIL', 'UNFERIFIED_WELCOME_MAIL (obj)'),
        ('RESET_PASSWORD', 'RESET_PASSWORD (obj)'),
        (
            'MOTOR_TRANSACTION_SUCCESS_MAIL',
            'MOTOR_TRANSACTION_SUCCESS_MAIL (obj)'
        ),
        (
            'MOTOR_TRANSACTION_FAILURE_MAIL',
            'MOTOR_TRANSACTION_FAILURE_MAIL (obj)'
        ),
        ('MOTOR_QUOTES_MAIL', 'MOTOR_QUOTES_MAIL (obj)'),
        ('MOTOR_DISCOUNT_CODE_MAIL', 'MOTOR_DISCOUNT_CODE_MAIL (obj)'),
        ('MOTOR_ADMIN_MAIL', 'MOTOR_ADMIN_MAIL (obj)'),
        ('RSA_STEP2_VALID_INS', 'RSA_STEP2_VALID_INS (obj)'),
        ('RSA_STEP2_EXPIRED_INS', 'RSA_STEP2_EXPIRED_INS (obj)'),
        ('APPOINTMENT_ALERT', 'APPOINTMENT_ALERT (obj)'),
        ('USER_ACCOUNT_CREATION', 'USER_ACCOUNT_CREATION'),
        ('NON_RM_USER_ACCOUNT_CREATION', 'NON_RM_USER_ACCOUNT_CREATION (obj)'),
        ('POLICY_UPLOAD_NOTIFICATION', 'POLICY_UPLOAD_NOTIFICATION'),
        ('RM_REASSIGNMENT', 'RM_REASSIGNMENT'),
        ('RM_CSU_TRANSITION', 'RM_CSU_TRANSITION'),
    )
    title = models.CharField(max_length=255)
    mtype = models.CharField(choices=MAIL_CHOICES, max_length=255)
    body = models.TextField()
    text = models.TextField()
    is_active = models.BooleanField(default=False)
    created = models.DateTimeField(_('created'), auto_now_add=True)

    def __unicode__(self):
        return u"%s - %s" % (self.mtype, self.title)


class Tracker(models.Model):
    session_key = models.CharField(max_length=255, unique=True)
    user = models.ForeignKey('User', null=True, blank=True)
    extra = JSONField(_('extra'), blank=True, default={})
    fd = models.ForeignKey('core.FormData', null=True, blank=True, related_name="visitor_fd")
    created = models.DateTimeField(_('created'), auto_now_add=True)

    def __unicode__(self):
        return u"%s" % self.session_key

    @property
    def primary_email(self):
        emails = self.emails.filter(is_primary=True)
        all_emails = self.emails.all()
        if emails:
            return emails.last().email
        else:
            return all_emails.last().email if all_emails else None

    @property
    def primary_phone(self):
        phones = self.phones.filter(is_primary=True)
        all_phones = self.phones.all()
        if phones:
            return phones.last().number
        else:
            return all_phones.last().number if all_phones else None

    def update_data(self, data):
        if not self.fd:
            self.fd = FormData.objects.create(visitor=self)
            self.save(update_fields=["fd"])
        self.fd.update(data)

    def attach(self, user):
        assert user
        try:
            utils.acquire_exclusive_lock("tracker.attach-user-%s" % user.id)
            self._attach(user)
        finally:
            utils.release_exclusive_lock("tracker.attach-user-%s" % user.id)

    def _attach(self, user):
        if user.first_name and 'first_name' not in self.extra:
            self.extra.update(**user.extra)
            self.extra['no_show'] = True

        Email.objects.filter(visitor=self).update(visitor=None, user=user)
        Email.objects.fix_duplicates(user)

        Phone.objects.filter(visitor=self).update(visitor=None, user=user)
        Phone.objects.fix_duplicates(user)

        Requirement.objects.filter(visitor=self).update(visitor=None, user=user)
        Requirement.objects.fix_duplicates(user)

        FormData.objects.filter(visitor=self).update(visitor=None, user=user)

        self.user = user
        self.save()
        user.customer_id = self.session_key
        user.save(update_fields=['_customer_id'])

    def _get_unverified_email(self, email):
        emails = self.emails.filter(email=email, is_verified=False)
        return emails, emails.count()

    def fix_duplicate_emails(self, email):
        emails = self.emails.filter(email=email)
        if emails.count() > 1:
            main_email = emails[0]
            emails = emails[1:]
            for email in emails:
                if email.is_verified:
                    main_email.is_verified = True
                elif email.is_primary:
                    main_email.is_primary = True
                elif email.is_bogus:
                    main_email.is_bogus = True
                elif not email.is_exists:
                    main_email.is_exists = False

                email.delete()

            main_email.save(
                update_fields=[
                    'is_verified', 'is_primary',
                    'is_bogus', 'is_exists'
                ]
            )

            return main_email

    def _fix_primary_email(self, email, **kwargs):
        if "is_primary" in kwargs and kwargs['is_primary']:
            self.emails.all().exlude(email, is_primary=True).update(is_primary=False)

        if not self.emails.filter(is_primary=True).count():
            self.emails.filter(email=email).update(is_primary=True)

    def attach_email(self, email, **kwargs):
        """
        :param email:
        :param kwargs:
                is_primary
                is_verified
                is_exists
                is_bogus
        :return:
        """
        from core.models import Email
        # Validate args.
        is_verified = kwargs.get("is_verified")
        is_primary = kwargs.get("is_primary")
        is_bogus = kwargs.get("is_bogus")
        is_exists = kwargs.get("is_exists")

        has_verified_key = False if is_verified is None else True
        has_primary_key = False if is_primary is None else True
        has_bogus_key = False if is_bogus is None else True
        has_exists_key = False if is_exists is None else True

        new_kwargs = {}
        if has_verified_key:
            new_kwargs["is_verified"] = is_verified

        if has_primary_key:
            new_kwargs["is_primary"] = is_primary

        if has_bogus_key:
            new_kwargs["is_bogus"] = is_bogus

        if has_exists_key:
            new_kwargs["is_exists"] = is_exists

        if not len(new_kwargs.keys()):
            raise Exception("Pass some parameter for update")

        if len(set(new_kwargs.keys()).union(set(['is_primary', 'is_bogus', 'is_exists', 'is_verified']))) > 4:
            raise Exception("Unknown field passed into kwargs. Check doc string.")

        user_emails = self.emails.filter(email=email, **new_kwargs)
        if user_emails:
            if user_emails.count() > 1:
                email = self.fix_duplicate_emails(
                    email,
                    **new_kwargs
                )

                return email

            return user_emails.first()

        if "is_verified" in kwargs and kwargs['is_verified']:
            # Check with other user email is verified true.
            emails = Email.objects.filter(email=email, is_verified=True)

            # If verified true with other user then update others with verified False.
            if emails:
                emails.update(is_verified=False)
                if self.emails.filter(email=email).count():
                    self.emails.filter(email=email).update(**new_kwargs)
                    self._fix_primary_email(email, **new_kwargs)
                    return self.emails.filter(email=email).first()
                else:
                    email = Email.objects.create(
                        email=email,
                        visitor=self,
                        **new_kwargs
                    )
                    self._fix_primary_email(email, **new_kwargs)
                    return email
            else:
                email = Email.objects.create(
                    email=email,
                    visitor=self,
                    **new_kwargs
                )

            self._fix_primary_email(email, **new_kwargs)
            return email

        elif "is_verified" in kwargs and not kwargs['is_verified']:
            emails, e_count = self._get_unverified_email(email)
            if e_count and e_count > 1:
                email = self.fix_duplicate_emails(email, **new_kwargs)
                self._fix_primary_email(email, **new_kwargs)
                return email
            else:
                email = Email.objects.create(
                    email=email,
                    visitor=self,
                    **new_kwargs
                )
                self._fix_primary_email(email, **new_kwargs)
                return email

    def _get_unverified_phones(self, phone):
        phones = self.phones.filter(number=phone, is_verified=False)
        return phones, phones.count()

    def fix_duplicate_phones(self, phone, is_verified):
        phones = self.phones.filter(number=phone)
        if phones.count() > 1:
            main_phone = phones[0]
            phones = phones[1:]
            for phone in phones:
                if phone.is_verified:
                    main_phone.is_verified = True
                elif phone.is_primary:
                    main_phone.is_primary = True
                elif phone.is_bogus:
                    main_phone.is_bogus = True
                elif not phone.is_exists:
                    main_phone.is_exists = False

                phone.delete()

            main_phone.save(
                update_fields=[
                    'is_verified', 'is_primary',
                    'is_bogus', 'is_exists'
                ]
            )

            return main_phone

    def _fix_primary_phone(self, phone, **kwargs):
        if "is_primary" in kwargs and kwargs['is_primary']:
            self.phones.all().exlude(phone, is_primary=True).update(is_primary=False)

        if not self.phones.filter(is_primary=True).count():
            self.phones.filter(number=phone).update(is_primary=True)

    def attach_phone(self, phone, **kwargs):
        """
        :param email:
        :param kwargs:
                is_primary
                is_verified
                is_exists
                is_bogus
        :return:
        """
        from core.models import Phone
        # Validate args.
        is_verified = kwargs.get("is_verified")
        is_primary = kwargs.get("is_primary")
        is_bogus = kwargs.get("is_bogus")
        is_exists = kwargs.get("is_exists")

        has_verified_key = False if is_verified is None else True
        has_primary_key = False if is_primary is None else True
        has_bogus_key = False if is_bogus is None else True
        has_exists_key = False if is_exists is None else True

        new_kwargs = {}
        if has_verified_key:
            new_kwargs["is_verified"] = is_verified

        if has_primary_key:
            new_kwargs["is_primary"] = is_primary

        if has_bogus_key:
            new_kwargs["is_bogus"] = is_bogus

        if has_exists_key:
            new_kwargs["is_exists"] = is_exists

        if not len(new_kwargs.keys()):
            raise Exception("Pass some parameter for update")

        if len(set(new_kwargs.keys()).union(set(['is_primary', 'is_bogus', 'is_exists', 'is_verified']))) > 4:
            raise Exception("Unknown field passed into kwargs. Check doc string.")

        user_phones = self.phones.filter(number=phone, **new_kwargs)
        if user_phones:
            if user_phones.count() > 1:
                email = self.fix_duplicate_phones(
                    phone,
                    **new_kwargs
                )

                return email

            return user_phones.first()

        if "is_verified" in kwargs and kwargs['is_verified']:
            # Check with other user email is verified true.
            phones = Phone.objects.filter(number=phone, is_verified=True)

            # If verified true with other user then update others with verified False.
            if phones:
                phones.update(is_verified=False)
                if self.phones.filter(number=phone).count():
                    self.phones.filter(number=phone).update(**new_kwargs)
                    self._fix_primary_phone(phone, **new_kwargs)
                    return self.emails.filter(number=phone).first()
                else:
                    email = Phone.objects.create(
                        number=phone,
                        visitor=self,
                        **new_kwargs
                    )
                    self._fix_primary_phone(phone, **new_kwargs)
                    return email
            else:
                phone_instance = Phone.objects.create(
                    number=phone,
                    visitor=self,
                    **new_kwargs
                )

            self._fix_primary_phone(phone, **new_kwargs)
            return phone_instance

        elif "is_verified" in kwargs and not kwargs['is_verified']:
            emails, e_count = self._get_unverified_phones(phone)
            if e_count and e_count > 1:
                phone = self.fix_duplicate_phones(phone, **new_kwargs)
                self._fix_primary_phone(phone, **new_kwargs)
                return phone
            else:
                phone = Phone.objects.create(
                    number=phone,
                    visitor=self,
                    **new_kwargs
                )
                self._fix_primary_phone(phone, **new_kwargs)
                return phone

    def update_extra(self, extra_data):
        self.extra = self.extra or {}
        self.extra.update(extra_data)

    def flush_otp_login(self):
        self.extra.pop('otp_login', '')
        self.save(update_fields=['extra'])


class Activity(models.Model):
    """
    RESULTS_VISITED : Who have visited results, contains the request data and
    the parsed data
    INSTANT_REVIEW : Who have done instant review, contains the data
    PLANS : Who have "shortlisted" products, added to "cart", "bought" products,
    contains data of plan and associated request
    """
    activity_id = UUIDField(
        db_index=True)  # activityId as a proxy to the model
    requirement = models.ForeignKey('core.Requirement', null=True)
    mid = models.ForeignKey('Marketing', blank=True, null=True)
    tracker = models.ForeignKey('Tracker', null=True, blank=True)
    user = models.ForeignKey('User', null=True, blank=True)
    is_processed = models.BooleanField(default=False)
    extra = JSONField(_('extra'), blank=True, default={})
    fd = models.ForeignKey('core.FormData', null=True)

    gclid = models.CharField(max_length=255, blank=True, null=True)
    term = models.CharField(max_length=255, blank=True, null=True)
    term_category = models.CharField(max_length=255, blank=True, null=True)
    match_type = models.CharField(max_length=255, blank=True, null=True)
    target = models.CharField(max_length=255, blank=True, null=True)
    placement = models.CharField(max_length=255, blank=True, null=True)
    label = models.CharField(max_length=255, blank=True, null=True)
    feed_item = models.CharField(max_length=255, blank=True, null=True)
    landing_url = models.URLField(max_length=2048, blank=True, null=True)
    url = models.URLField(max_length=2048, blank=True, null=True)

    page_id = models.CharField(
        max_length=255, blank=True, null=True, help_text="""
            Examples: home, lp1, car_new_thankyou, etc
        """
    )
    form_name = models.CharField(
        max_length=255, blank=True, null=True, help_text="""
            Examples: car_new_quote_form, car_lead_form, car_proposal_form, etc
        """
    )
    form_position = models.CharField(
        max_length=255, blank=True, null=True, help_text="""
            Examples: hero, benefits, etc
        """
    )
    product = models.CharField(
        max_length=255, blank=True, null=True, help_text="""
            should always start with product mentioned in RequirementKind
            Examples: car_new, car_renewal, health, health_cancer
        """
    )
    form_variant = models.CharField(
        max_length=255, blank=True, null=True, help_text="""
            Examples: home, lp1, fast_lane
        """
    )
    activity_type = models.CharField(
        max_length=255, blank=True, null=True, help_text="""
            Examples: viewed, dropped, email_focus, email_change, etc
        """
    )

    # from user agent
    browser = models.CharField(max_length=100, blank=True, null=True)
    os = models.CharField(max_length=100, blank=True, null=True)
    device = models.CharField(max_length=100, blank=True, null=True)
    device_model = models.CharField(max_length=100, blank=True, null=True)

    # from ip address
    ip_address = models.GenericIPAddressField(blank=True, null=True)
    state = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    isp = models.CharField(max_length=100, blank=True, null=True)

    referer = models.URLField(max_length=2048, blank=True, null=True)
    request_id = models.CharField(max_length=100, blank=True, null=True)

    created = models.DateTimeField(_('created'), auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    ACTIVITY_PARAM_MAPPING = {
        'gclid': 'gclid',

        'im_term': 'term',
        'utm_keyword': 'term',
        'vt_keyword': 'term',
        'keyword': 'term',
        'utm_term': 'term',

        'keyword_category': 'term_category',

        'vt_adposition': 'label',
        'adposition': 'label',
        'label': 'label',

        'vt_matchtype': 'match_type',
        'matchtype': 'match_type',
        'match_type': 'match_type',

        'vt_placement': 'placement',
        'placement': 'placement',

        'vt_target': 'target',
        'target': 'target',

        'feed_item_id': 'feed_item',

        'location': 'city',
        'calculated_city': 'city',

        'device': 'device',

        'device_model': 'device_model',

        'referer': 'referer',

        'triggered_page': 'url',

        'landing_page_url': 'landing_url',
    }

    def __unicode__(self):
        session_key = self.tracker.session_key if self.tracker else None
        return u"%s - %s" % (self.activity_type, session_key)

    def fd_has_contact_details(self):
        form_data = self.fd.get_data()
        for form_name, data in form_data.iteritems():
            if form_name != "core.forms.Person":
                continue

            if not isinstance(data, list):
                data = [data]

            for d in data:
                form_email = d.get("email", None)
                if form_email and form_email.lower() == 'none':
                    form_email = None

                form_mobile = d.get("mobile", None)
                if form_mobile and form_mobile.lower() == 'none':
                    form_mobile = None

                if form_email or form_mobile:
                    return True

            return False

    def is_strong(self):
        return True
        # if self.fd_has_contact_details():
        #     return True
        #
        # if self.page_id:
        #     starts_with = [
        #         "lp_", "flatpage_", "tp_", "sp_", "blog", "proposal",
        #         "policy_", "make-payment", "affiliate_leads", "step",
        #         "buy-form", "call-popup", "buy_car_insurance",
        #         "alto-800", "alto-k10", "bolero", "Buy page", "buy-form",
        #         "call-form", "call-popup", "car_manufacturer", "city",
        #         "CROSS_LEAD", "CUSTOMER_REFERENCE", "default_label",
        #         "detail-page", "email", "FACEBOOK", "faqs", "form_500",
        #         "Gold_Registration", "HAPTIK", "health_compare", "health_product",
        #         "heath-tool", "homepage_contactMe_middle", "homepage_contactMe_mobile",
        #         "homepage_contactMe_top", "hospital_list", "in-cards", "INCOMING_CALL",
        #         "JAGO", "make-payment", "medical-conditions", "nano", "offline",
        #         "OLARK_CHAT", "on_scroll_lowest_quotes", "page_id", "PERSONAL_REFERRAL",
        #         "proposal-form", "quote-form", "results_top_bar", "results-page",
        #         "upload", "travel_homepage", "travel_flatpage", "verito-vibe-cs",
        #         "MANUAL_UPLOAD_LEAD", "FB_WEBHOOK_LEAD", "home-page"
        #     ]
        #
        #     for s in starts_with:
        #         if self.page_id.startswith(s):
        #             return True
        #
        #     ends_with = ["-redirect", "_flatpage"]
        #     for s in ends_with:
        #         if self.page_id.endswith(s):
        #
        #             return True
        #
        # return False

    is_strong.boolean = True

    def set_data(self, data, user, tracker):
        assert self.fd is None

        # we attach only one of user | tracker to formdata
        args = {}
        if user:
            args["user"] = user
        else:
            args["visitor"] = tracker
        self.fd = FormData.objects.create(**args)

        self.save(update_fields=['fd'])
        self.fd.update(data)
        self.save()

    def attach(self, requirement):
        self.requirement = requirement
        self.save(update_fields=['requirement'])
        requirement.attach_activity(self)

    @property
    def req_product(self):
        return self.product.split('_', 1)[0]


class Review(GenericBaseModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    rating = custom_fields.IntegerRangeField(min_value=1, max_value=5)
    content = models.TextField()

    class Meta:
        unique_together = ("user", "content_type", 'object_id')

    def __unicode__(self):
        return '%d - %s - %s' % (self.rating, self.user, self.content_object, )


class Company(BaseAuthorModel):
    """
    Any Company is represented by this model.

    An Company has many admins. But from employee benefit point of view we may
    have a different set of admin, compared to something else. Also employees
    have other relationships, like who they are reporting to, who their peers
    are, all things needed for HR functions, but we are not capturing them here,
    we will have separate apps for them.

    The company can be in different "states", at least if we should consider it
    active or not.
    """
    name = models.CharField(
        max_length=200, help_text="""
        The name of the company. This is user visible name, there may be
        duplicates, but we do not care.

        This is not indexed.
        """
    )
    slug = models.SlugField(
        # TODO: some slugs are not allowed (eg ones containing _, or "www" etc)
        help_text="""
        Domain name of company.
        """,
        unique=True,
    )
    address = models.TextField(_('address'), max_length=200, blank=True)
    employees = models.ManyToManyField(
        User, through='Employee', related_name="companies"
    )
    parent = models.ForeignKey(
        "Company", null=True, blank=True, help_text="""
            Parent company. This is the LEGAL parent company of a company, as in
            by company laws of India.
        """
    )
    employee_discount = models.BooleanField(_("Employee Discount"), default=False)

    def __unicode__(self):
        return self.name

    def has_perm(self, user, perm, obj=None):
        try:
            emp = Employee.objects.get(company=self, user=user)
        except Employee.DoesNotExist:
            return False
        else:
            return emp.has_perm(perm, obj)
        return False


class EmployeePermissionsMixin(models.Model):
    """
    A mixin class that adds the fields and methods necessary to support
    Django's Group and Permission model using the ModelBackend.
    """
    is_superuser = models.BooleanField(
        _('superuser status'), default=False,
        help_text=_(
            'Designates that this user has all permissions without '
            'explicitly assigning them.'
        )
    )
    groups = models.ManyToManyField(
        Group, verbose_name=_('groups'),
        blank=True, help_text=_(
            'The groups this user belongs to. A user will '
            'get all permissions granted to each of '
            'their groups.'
        ),
        related_name="employee_set", related_query_name="employee"
    )
    user_permissions = models.ManyToManyField(
        Permission, verbose_name=_('user permissions'), blank=True,
        help_text=_('Specific permissions for this user.'),
        related_name="employee_set", related_query_name="employee"
    )

    class Meta:
        abstract = True

    def has_perm(self, perm, obj=None):
        """
        Returns True if the user has the specified permission. This method
        queries all available auth backends, but returns immediately if any
        backend returns True. Thus, a user who has permission from a single
        auth backend is assumed to have permission in general. If an object is
        provided, permissions for this specific object are checked.
        """

        # superusers have all permissions. TODO: We will implement this later as
        # this can give more permissions than we need.
        # if self.is_superuser:
        #     return True

        # TODO: obj is not supported
        try:
            self.user_permissions.get(codename=perm)
        except Permission.DoesNotExist:
            pass
        else:
            return True
        # TODO: can we do the following in one query?
        for group in self.groups.all():
            try:
                group.permissions.get(codename=perm)
            except Permission.DoesNotExist:
                pass
            return True
        return False


class Employee(EmployeePermissionsMixin, models.Model):
    company = models.ForeignKey(
        Company, help_text="""
        The company. An employee can possibly be part of same company multiple
        times, like I had two stints with BrowserStack. So we can not put a
        unique together on (user, company) tuple, we must do manual validation.
        """
    )
    user = models.ForeignKey(User, related_name="employee_instance")
    title = models.CharField(
        max_length=200, blank=True, default="", help_text="""
        The company specific position of the employee. Eg VP Engineering.

        No validations on this.
        """
    )
    joined_on = models.DateField(
        default=datetime.datetime.today, blank=False, help_text="""
        This can get complicated eventually, a person may accept a position on
        one date, and join on another. For now this field means the date from
        which the employee starts getting salary.
        """
    )
    employee_id = models.CharField(
        max_length=200, blank=True, default="", help_text="""
        Employee ID of the company. This is company specific. Ideally we can
        have a database constraint of unique together (company, eid), but for
        now we are not adding it.
        """
    )
    left_on = models.DateField(
        null=True, blank=True, default=None, help_text="""
        This too is complicated. For now this means the last date, when salary
        stops. Full and final settlement may happen later. Ideally a person
        should be visible, to HR etc, till FNF. We will capture FNF through
        another field when needed.
        """
    )

    def has_perm(self, perm, obj=None):
        if super(Employee, self).has_perm(perm, obj):
            return True

        if self.company.parent:
            try:
                emp = Employee.objects.get(
                    company=self.company.parent, user=self.user
                )
            except Employee.DoesNotExist:
                return False
            else:
                return emp.has_perm(perm, obj)

        return False

    class Meta:
        permissions = (
            ('is_dealer', "Is Dealer"),
            ('can_view_pending', "Can View Pending"),
            ('can_inspect', "Can Inspect")
        )
        unique_together = (
            ("company", "user", "left_on", ),
            ("company", "user", "joined_on", ),
        )

    def __unicode__(self):
        return "%s(current=%s): %s" % (
            self.user, self.left_on is None, self.company
        )


class FormSection(BaseAuthorModel):
    form = models.CharField(max_length=200)
    data = HStoreField(default={}, blank=True)

    def __unicode__(self):
        return self.form


def get_role_dict(data, form_name):
    form_data = data.get(form_name, {})
    if not form_data:
        return {}

    new_dict = {}
    if isinstance(form_data, dict):
        role = form_data.get("role", "other")
        if role == "":
            role = "other"

        new_dict[role] = form_data
    elif isinstance(form_data, list):
        for item in form_data:
            role = form_data.get("role", "other")
            if role == "":
                role = "other"
            new_dict[role] = form_data
    else:
        raise Exception("Invalid old data form. It should have dict or list as form data")

    return new_dict


def merge_dict(dict1, dict2):
    merged_dict = dict(dict1)
    merged_dict.update(dict2)
    return merged_dict


def add_to_merge_dict(merge_data, form_name, new_item):
    if not isinstance(new_item, dict):
        raise Exception("Invalid item. It must be of dict type.")

    if form_name in merge_data:
        if isinstance(merge_data[form_name], list):
            merge_data[form_name].append(new_item)
        elif isinstance(merge_data[form_name], dict):
            merge_data[form_name] = [merge_data[form_name]]
            merge_data[form_name].append(new_item)
        else:
            merge_data[form_name] = new_item
    else:
        merge_data[form_name] = new_item

    return merge_data


class FormData(BaseAuthorModel):
    user = models.ForeignKey(User, null=True, blank=True)
    visitor = models.ForeignKey(Tracker, null=True, blank=True)
    sections = models.ManyToManyField(FormSection)

    def __unicode__(self):
        if self.user:
            return "FormData - %s" % (self.user.get_full_name())
        else:
            return "FormData - %s" % (str(self.visitor))

    def _get_data(self):
        data = {}
        for section in self.sections.all():
            name = section.form
            if name in data and not isinstance(data[name], list):
                data[name] = [data[name]]

            if name in data:
                data[name].append(section.data)
            else:
                data[name] = section.data

        return data

    def get_data(self, fresh=False):
        if fresh:
            data = self._get_data()
            self.cache(data=data)
            return data

        data = self.cache()

        if not data:
            data = self._get_data()
            self.cache(data=data)

        return data

    @staticmethod
    def flatten_(data):
        fdata = {}
        for form in data:
            fses = data[form]
            if not isinstance(fses, list):
                fses = [fses]
            for fs in fses:
                suffix = ""
                if "role" in fs:
                    suffix = "__" + fs["role"]
                for k, v in fs.items():
                    fdata[k + suffix] = v
        return fdata

    def flatten(self):
        return FormData.flatten_(self.get_data())

    @staticmethod
    def merge(odata, ndata):
        if not odata:
            return ndata

        if not ndata:
            return odata

        merged_data = {}
        for form_name, form_data in ndata.iteritems():
            if form_name in odata:
                old_data_stuff = odata[form_name]
                new_data_stuff = form_data

                o_is_dict = isinstance(old_data_stuff, dict)
                n_is_dict = isinstance(new_data_stuff, dict)
                n_is_list = isinstance(new_data_stuff, list)

                if o_is_dict and n_is_dict:
                    o_role = old_data_stuff.get("role", "other") or "other"
                    n_role = new_data_stuff.get("role", "other") or "other"

                    if o_role == n_role:
                        merge_dict_data = merge_dict(
                            old_data_stuff,
                            new_data_stuff
                        )
                        merged_data = add_to_merge_dict(
                            merged_data,
                            form_name,
                            merge_dict_data
                        )
                    else:
                        merged_data = add_to_merge_dict(
                            merged_data,
                            form_name,
                            old_data_stuff
                        )
                        merged_data = add_to_merge_dict(
                            merged_data,
                            form_name,
                            new_data_stuff
                        )
                elif o_is_dict and n_is_list:
                    for item in new_data_stuff:
                        role = item.get("role", "other")

                        old_role_dict = get_role_dict(
                            odata, form_name
                        )

                        if role in old_role_dict:
                            new_dict = merge_dict(
                                old_data_stuff,
                                item
                            )
                            merged_data = add_to_merge_dict(
                                merged_data,
                                form_name,
                                new_dict
                            )
                        else:
                            merged_data = add_to_merge_dict(
                                merged_data,
                                form_name,
                                item
                            )
            else:
                if not isinstance(form_data, dict) and not isinstance(form_data, list):
                    raise Exception("Cannot merge. Invalid form data.")

                merged_data[form_name] = form_data

        # Insert all old database.
        for form_name, form_data in odata.iteritems():
            if form_name in merged_data:
                continue
            else:
                merged_data[form_name] = form_data

        return merged_data

    def update(self, fd):
        if not fd:
            return

        odata = self.get_data()

        if isinstance(fd, FormData):
            fd = fd.get_data()

        fd = FormData.merge(odata, fd)
        if fd == odata:
            return

        fses = []

        for fname, stuff in fd.items():
            if isinstance(stuff, list):
                forms = stuff
            else:
                forms = [stuff]

            for form in forms:
                # Convert everything into string.
                new_form_data = {}
                for key, val in form.iteritems():
                    new_form_data[key] = force_str(val)

                fses.append(
                    FormSection.objects.create(
                        form=fname, data=new_form_data
                    )
                )

        self.sections = fses
        self.save()


class MarketingManager(models.Manager):
    def get_or_create_mid(self, data):
        mid = self.model.mid(data=data)
        mid_id = cache.get(u'mid_{}'.format(mid))
        if mid_id:
            try:
                marketing = self.get(id=mid_id)
            except (self.model.DoesNotExist, ValueError):
                pass
            else:
                cache.set(u'mid_{}'.format(mid), marketing.id, 24 * 60 * 60)
                return marketing

        campaign, source, medium, category, content, brand = mid.split('||')
        marketing, _ = self.get_or_create(
            campaign=campaign, source=source, medium=medium, category=category, content=content, brand=brand
        )
        cache.set(u'mid_{}'.format(mid), marketing.id, 24 * 60 * 60)
        return marketing

    def get_mid(self, data):
        mid = self.model.mid(data=data)
        campaign, source, medium, category, content, brand = mid.split('||')
        return self.get(
            campaign=campaign, source=source, medium=medium,
            category=category, content=content, brand=brand
        )


class Marketing(BaseAuthorModel):
    """
        This model is to keep track of different marketing activitis that
        brought the traffic. This can be paid campaigns or it can be organic
        searches or email campaigns.

        Each activity is identified by three fields, device (mobile/deksopt),
        network(facebook/google) and campaign(specific id).
    """
    description = models.TextField(blank=True)
    campaign = custom_fields.CICharField(
        max_length=100, blank=True, db_index=True, help_text="""
            The campaign id that brought this traffic. This can be google ad id,
            or email blast id etc.
        """
    )
    source = custom_fields.CICharField(
        max_length=100, blank=True, db_index=True, help_text="""
            Source of this traffic. Examples: facebook, google, etc
        """
    )
    medium = custom_fields.CICharField(
        max_length=100, blank=True, db_index=True, help_text="""
            Examples: affiliate, organic, cpc, banner, etc
        """
    )
    category = custom_fields.CICharField(
        max_length=100, blank=True, db_index=True, help_text="""
            Examples: aggregator, brand, competition, etc
        """
    )
    brand = custom_fields.CICharField(
        max_length=100, blank=True, db_index=True, help_text="""
            When category is competition. Examples: religare, icici, etc
        """
    )
    content = custom_fields.CICharField(
        max_length=100, blank=True, db_index=True, help_text="""
            Different graphics
        """
    )
    paid = models.BooleanField(default=False)

    objects = MarketingManager()

    MID_MAPPING = OrderedDict((
        ('im_campaign', 'campaign'),
        ('utm_campaign', 'campaign'),
        ('vt_campaign', 'campaign'),
        ('campaign', 'campaign'),

        ('im_source', 'source'),
        ('vt_source', 'source'),
        ('utm_source', 'source'),
        ('network', 'source'),
        ('source', 'source'),

        ('im_medium', 'medium'),
        ('utm_medium', 'medium'),
        ('medium', 'medium'),

        ('ad-category', 'category'),
        ('category', 'category'),

        ('im_content', 'content'),
        ('utm_content', 'content'),
        ('content', 'content'),

        ('brand', 'brand'),
    ))

    def __unicode__(self):
        return self.campaign

    class Meta:
        unique_together = ('campaign', 'source', 'medium', 'category', 'brand', 'content')

    @classmethod
    def mid(cls, data):
        d = {}
        done = []
        for q_param, mapped_to in cls.MID_MAPPING.items():
            if mapped_to not in done:
                value = data.get(q_param)
                if value and value.lower() is not 'none':
                    d[mapped_to] = value
                    done.append(mapped_to)

        return "%s||%s||%s||%s||%s||%s" % (d.get('campaign', ''), d.get('source', ''),
                                           d.get('medium', ''), d.get('category', ''),
                                           d.get('content', ''), d.get('brand', ''))

    def set_duplicate(self, mid):
        if not isinstance(mid, self._meta.model):
            mid = self._default_manager.get(id=mid)

        reverse_relations = [f for f in self._meta.get_fields() if (f.one_to_many or f.one_to_one) and f.auto_created]
        with django_transaction.atomic():
            for rel in reverse_relations:
                related_objs = rel.related_model._base_manager.filter(
                    **{rel.field.name: mid}
                ).select_for_update()
                related_objs.update(**{rel.field.name: self})
            mid.delete()


class FinPolicy(models.Model):
    policy_number = models.CharField(max_length=100)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    gross_premium = models.FloatField(null=True, blank=True)
    net_premium = models.FloatField(null=True, blank=True)
    registration_no = models.CharField(null=True, max_length=20)
    mobile = models.CharField(blank=True, max_length=20)
    policy_holder_name = models.CharField(max_length=200, blank=True)
    product_name = models.CharField(max_length=100, blank=True)
    insurer_company = models.CharField(max_length=100, blank=True)
    reconciled = models.BooleanField(default=False)
    filename = models.CharField(max_length=100)
    extra = models.TextField()


class Policy(Document):
    # TODO: add policy type field. eg: Medisure, Worlwide Trip..
    VALID = "valid"  # policy is active
    CANCELLED = "cancelled"  # policy has been cancelled
    STATUS_CHOICES = (
        (VALID, "Policy is valid"),
        (CANCELLED, "Policy is cancelled")
    )
    user = models.ForeignKey(User)
    requirement_sold = models.ForeignKey(
        "Requirement", related_name="policy_sold", help_text="""
        This is the requirement that lead to purchase of this policy.
        """
    )
    status = models.CharField(max_length=100, default=VALID, choices=STATUS_CHOICES)
    policy_number = models.CharField(max_length=100)
    mid = models.ForeignKey(Marketing, blank=True, null=True)  # winner
    premium = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=10)
    issue_date = models.DateField(blank=True, null=True)
    expiry_date = models.DateField(blank=True, null=True)


class PolicyAmendment(Document):
    parent_policy = models.ForeignKey("Policy", related_name="amendments")
    requirement_sold = models.ForeignKey(
        "Requirement", related_name="amendments", help_text="""
        This is the requirement that lead to the amendment.
        """
    )


class Payment(BaseAuthorModel):
    PENDING = "pending"
    CANCELLED = "cancelled"
    TIMEDOUT = "timedout"
    THIRD_PARTY = "third_party"
    SUCCESS = "success"
    ERROR = "error"
    STATUS_CHOICES = (
        (PENDING, "Payment is pending"),
        (CANCELLED, "User cancelled the Payment"),
        (TIMEDOUT, "Timed out"),
        (SUCCESS, "Payment successful"),
        (ERROR, "Error in payment"),
        (THIRD_PARTY, "Payment is pending on third party's side")
    )
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User)
    amount = models.FloatField(default=0.0)
    status = models.CharField(
        max_length=40, choices=STATUS_CHOICES, default=PENDING
    )


class RequirementStateHistory(models.Model):
    previous_state = models.ForeignKey('statemachine.State', null=True, related_name='previous_for_history')
    state = models.ForeignKey("statemachine.State", related_name='new_for_history')
    requirement = models.ForeignKey("core.Requirement")
    enter_time = models.DateTimeField(default=timezone.now)
    exit_time = models.DateTimeField(null=True)


class RequirementKind(models.Model):
    CAR = 'car'
    MOTOR = 'car'
    BIKE = 'bike'
    HEALTH = 'health'
    TRAVEL = 'travel'
    TERM = 'term'
    HOME = 'home'
    LIFE = 'life'

    PRODUCTS = (
        (CAR, 'Car'),
        (HEALTH, 'Health'),
        (BIKE, 'Bike'),
        (MOTOR, 'Car'),
        (TRAVEL, 'Travel'),
        (TERM, 'Term'),
        (HOME, 'Home'),
        (LIFE, 'Life'),
    )

    NEW_POLICY = 'new_policy'
    CANCELLATION = 'cancellation'
    CLAIM = 'claim'
    ENDORSEMENT = 'Endorsement'

    ENDORESEMENT_MAP = {
        CAR: {
            'change': [
                'name',
                'address',
                'email_id',
                'contact_no',
                'chassis_no',
                'engine_no',
                'policy_period',
                'fuel_type',
                'idv',
                'registration_no',
                'registration_place',
                'vehicle',
                'passenger_cover',
                'owner',
                'ncb',
            ],
            'correction': [
                'name',
                'address',
                'email_id',
                'contact_no',
                'chassis_no',
                'engine_no',
                'policy_period',
                'fuel_type',
                'idv',
                'registration_no',
                'registration_place',
                'vehicle',
                'passenger_cover',
                'owner',
                'ncb',
            ],
            'addition': [
                'vehicle',
                'hypothecation',
                'voluntary_deductable',
            ],
            'deletion': [
                'vehicle',
                'hypothecation',
                'voluntary_deductable',
            ],
            'recovery': [
                'ncb',
            ],
        },
        HEALTH: {
            'change': [
                'dob',
                'name',
                'salutation',
                'gender',
                'passport_no',
                'address',
                'email_id',
                'contact_no',
                'relationship',
                'occupation',
                'policy_period',
            ],
            'correction': [
                'dob',
                'name',
                'salutation',
                'gender',
                'passport_no',
                'ped',
                'address',
                'email_id',
                'contact_no',
                'relationship',
                'occupation',
                'policy_period',
            ],
            'addition': [
                'ped',
                'insured',
            ],
            'deletion': [
                'ped',
                'insured',
            ],
        },
    }
    ENDORESEMENT_MAP[TRAVEL] = ENDORESEMENT_MAP[HEALTH]
    ENDORESEMENT_MAP[BIKE] = ENDORESEMENT_MAP[CAR]

    ENDORESEMENTS = tuple(
        (name, name.title()) for name in tuple(
            set(end_kind
                for values in ENDORESEMENT_MAP.values()  # noqa
                for end_kind in values.keys())
        )
    )

    KINDS = (
        (NEW_POLICY, 'New Policy'),
        (CANCELLATION, 'Cancellation'),
        (CLAIM, 'Claim'),
        (ENDORSEMENT, ENDORESEMENTS),
    )

    SUB_KINDS = tuple(
        (sub_kind, sub_kind.title()) for sub_kind in set(
            end_sub_kind
            for values in ENDORESEMENT_MAP.values()  # noqa
            for value_list in values.values()
            for end_sub_kind in value_list
        )
    )
    NEW_POLICY_SUB_KINDS = (
        ('MEDICAL_REQUIRED', 'medical_required'),
        ('INSPECTION_REQUIRED', 'inspection_required')
    )
    SUB_KINDS = SUB_KINDS + NEW_POLICY_SUB_KINDS

    kind = models.CharField(max_length=255, choices=KINDS)
    sub_kind = models.CharField(max_length=255, choices=SUB_KINDS, blank=True)
    product = models.CharField(max_length=255, choices=PRODUCTS)
    flow = models.CharField(max_length=255, blank=True, null=True)
    machine = models.ForeignKey('statemachine.Machine', null=True)

    class Meta:
        unique_together = ('kind', 'sub_kind', 'product')

    def __unicode__(self):
        if self.sub_kind:
            return u'{} {} {}'.format(self.product, self.kind, self.sub_kind)
        return u'{} {}'.format(self.product, self.kind)

    def save(self, *args, **kwargs):
        # TODO: need to add validations for sub-types according to product
        if self.sub_kind == self.RECOVERY and self.product not in [self.CAR, self.BIKE]:
            raise ValidationError(u'{} type is only available for {} or {} product'.format(self.type, self.CAR, self.BIKE))

        super(RequirementKind, self).save(*args, **kwargs)


for tupl in (RequirementKind.SUB_KINDS, RequirementKind.ENDORESEMENTS):
    for db_value, disp_value in tupl:
        setattr(RequirementKind, db_value.upper(), db_value)
        del db_value, disp_value
    del tupl


class RequirementManager(models.Manager):
    # STATE_PRIORITY_ENUM defines the priority of states
    # TODO: Change it so that the score is part of the statemachine
    STATE_PRIORITY_ENUM = [
        u'FRESH_ringing',
        u'FRESH_new',

        u'QUALIFIED_likelytobuy',
        u'QUALIFIED_quotessent',
        u'QUALIFIED_formsent',
        u'QUALIFIED_paymentgateway',
        u'QUALIFIED_paymentfailed',
        u'QUALIFIED_proposalfailed',
        u'QUALIFIED_transactiondrop',

        u'POLICYPENDING_paymentdone',
    ]

    def _all(self):
        return super(RequirementManager, self).get_queryset()

    def get_queryset(self):
        return super(RequirementManager, self).get_queryset().exclude(current_state=17)

    def _filter_requirements(self, qs, user, tracker, kind):
        assert kind

        user_or_tracker = Q()
        if user:
            user_or_tracker |= Q(user=user)
        if tracker:
            user_or_tracker |= Q(visitor=tracker)
            if tracker.user:
                user_or_tracker |= Q(user=tracker.user)

        qs = qs.filter(user_or_tracker)
        qs = qs.filter(kind=kind)

        return qs

    def _get_closed_requirements(self, user, tracker, kind):
        qs = self._all().filter(
            current_state__parent_state__name='CLOSED'
        )

        return self._filter_requirements(
            qs, user, tracker, kind
        )

    def _get_open_requirements(self, user, tracker, kind):
        qs = self._all().filter(
            ~Q(current_state__parent_state__name='CLOSED')
        )

        return self._filter_requirements(
            qs, user, tracker, kind
        )

    def get_lead_requirement(self, phone, product):
        """
        :return: Requirement instance
        """
        # Doing filter. In case where there are two is_verified phones we should not
        # miss lead because of this. Attaching to user with requirement.
        user = None
        requirement = None
        visitor = None
        users = User.objects.filter(phones__number=phone, phones__is_verified=True)

        req_kind = RequirementKind.objects.get(
            product=product, kind=RequirementKind.NEW_POLICY, sub_kind=''
        )
        for user in users:
            requirement = Requirement.objects.get_open_requirement(
                user, None, req_kind, product=product
            )
            if requirement:
                return user, visitor, requirement

        if users:
            return users.first(), visitor, None

        users = User.objects.filter(phones__number=phone, phones__is_verified=False)
        for user in users:
            requirement = Requirement.objects.get_open_requirement(
                user, None, req_kind, product=product
            )
            if requirement:
                return user, visitor, requirement

        visitors = Tracker.objects.filter(phones__number=phone)
        for visitor in visitors:
            requirement = requirement = Requirement.objects.get_open_requirement(
                None, visitor, req_kind, product=product
            )
            if requirement:
                return None, visitor, requirement

        return None, None, None

    def get_open_requirement(self, user, tracker, kind):
        """
        This function will return first open requirement. If no open requirement
        is found then it will return the first CLOSED_unresolved requirement
        or None.

        :return: None or requirement instance
        """
        # Get open requirements.
        open_requirement = self._get_open_requirements(
            user, tracker, kind
        ).first()

        if open_requirement:
            return open_requirement

        # Get closed requirements.
        closed_requirements = self._get_closed_requirements(
            user, tracker, kind
        )

        # Check if last closed requirement is ready to reopen.
        last_closed_req = closed_requirements.last()
        if last_closed_req and last_closed_req.is_ready_to_reopen():
            return last_closed_req

    def search_by_email(self, email, include_concluded=False):
        """
        :return Requirement objects matching the email
        """
        if include_concluded:
            req_ids = set(Requirement.objects.filter(
                user__emails__email=email
            ).values_list('id', flat=True))
            req_ids = req_ids.union(Requirement.objects.filter(
                visitor__emails__email=email
            ).values_list('id', flat=True))
        else:
            query_states = sm_models.State.objects.filter(
                name__startswith='CLOSED'
            ).values_list('id', flat=True)
            req_ids = set(Requirement.objects.exclude(
                current_state_id__in=query_states
            ).filter(user__emails__email=email).values_list('id', flat=True))
            req_ids = req_ids.union(Requirement.objects.exclude(
                current_state_id__in=query_states
            ).filter(visitor__emails__email=email).values_list('id', flat=True))

        qs = Requirement.objects.filter(id__in=req_ids)

        return qs

    def search_by_mobile(self, mobile, include_concluded=False):
        """
        :return Requirement objects matching the mobile
        """
        if include_concluded:
            req_ids = set(Requirement.objects.filter(
                user__phones__number=mobile
            ).values_list('id', flat=True))
            req_ids = req_ids.union(Requirement.objects.filter(
                visitor__phones__number=mobile
            ).values_list('id', flat=True))
        else:
            query_states = sm_models.State.objects.filter(
                name__startswith='CLOSED'
            ).values_list('id', flat=True)
            req_ids = set(Requirement.objects.exclude(
                current_state_id__in=query_states
            ).filter(user__phones__number=mobile).values_list('id', flat=True))
            req_ids = req_ids.union(Requirement.objects.exclude(
                current_state_id__in=query_states
            ).filter(visitor__phones__number=mobile).values_list('id', flat=True))

        qs = Requirement.objects.filter(id__in=req_ids)

        return qs

    def get_best_requirement(self, requirements):
        """
        For this we can choose it based on
            1) "Best State"
            2) "Most Activity"
        in the same order. If there is tie, we check which one has more
        activities and choose it. If there is still a tie, we choose the
        latest one. If there is still a tie.
        """
        sorted_requirements = sorted(requirements, key=lambda r: (
            self.STATE_PRIORITY_ENUM.index(r.current_state.name),
            r.activity_set.count() + r.crm_activities.count()
        ), reverse=True)
        return sorted_requirements[0]

    def fix_duplicates(self, user):
        return
        user_reqs = Requirement.objects._all().filter(user=user)

        for kind in set([ur.kind for ur in user_reqs]):
            oldest_duplicate_req = user_reqs.filter(kind=kind, current_state__name='DUPLICATE_deleted').order_by('-id')

            if not oldest_duplicate_req:
                continue

            if oldest_duplicate_req.count() > 1:
                continue

            oldest_duplicate_req = oldest_duplicate_req[0]

            # TODO: GROT once data has been fixed, since all requirements
            # marked Duplicated_deleted with have a merged_into
            if not oldest_duplicate_req.merged_into:
                continue

            requirements = user_reqs.filter(kind=kind)
            for r in requirements:
                if r.current_state.name in ["DUPLICATE_deleted", "CLOSED_resolved"]:
                    continue
                if r != oldest_duplicate_req:
                    r.merge(oldest_duplicate_req.merged_into)
                    r.hide()

    def create_requirement(
            self, requirement_kind, mid, user=None, visitor=None, extra={}
    ):
        """
        don't use this function without approval from central team. After create
        requirement call create activity with requirement as argument.
        """
        # User or visitor should be passed.
        assert user or visitor
        assert requirement_kind

        requirement_instance = Requirement.objects.create(
            mid=mid,
            kind=requirement_kind,
            user=user,
            visitor=visitor,
            extra=extra,
        )
        return requirement_instance


class Requirement(BaseAuthorModel):
    # States
    PENDING = "pending"
    SOLD = "sold"
    USERREFUSED = "user_said_no"
    INSURER_REFUSED = 'insurer_refused'

    STATE_CHOICES = (
        (PENDING, "pending"),
        (SOLD, "sold"),
        (USERREFUSED, "user refused"),
        (INSURER_REFUSED, "Insurer refused"),
    )

    user = models.ForeignKey(User, blank=True, null=True, related_name='requirements')
    visitor = models.ForeignKey(Tracker, blank=True, null=True, related_name='requirements')
    kind = models.ForeignKey("RequirementKind")
    fd = models.ForeignKey(FormData, blank=True, null=True)
    policy = models.ForeignKey(
        Policy, null=True, blank=True, help_text="""
        Each requirement is about some policy. For a new purchase requirement, this will be null, and will be added
        at the end of sale. For other requirements, like change name for this already sold policy, this will field will
        be used.
        """, limit_choices_to={'requirement__kind__kind': RequirementKind.NEW_POLICY}
    )
    payment = models.ManyToManyField(Payment, blank=True)
    current_state = models.ForeignKey('statemachine.State', null=True, related_name='requirements')
    state_history = models.ManyToManyField(
        'statemachine.State',
        through='RequirementStateHistory',
        through_fields=('requirement', 'state')
    )
    tasks = models.ManyToManyField("crm.Task", blank=True, related_name='requirements')
    crm_activities = models.ManyToManyField('crm.Activity', blank=True, related_name='requirements')
    last_web_activity = models.ForeignKey('Activity', null=True, blank=True, related_name='last_for_requirements')
    last_crm_activity = models.ForeignKey('crm.Activity', null=True, blank=True, related_name='last_for_requirements')
    next_crm_task = models.ForeignKey('crm.Task', null=True, blank=True, related_name='next_for_requirements')
    mid = models.ForeignKey(Marketing, blank=True, null=True)
    owner = models.ForeignKey('crm.Advisor', null=True, blank=True, related_name='requirements_owned')
    remark = models.TextField(blank=True)
    extra = pgjson_fields.JsonBField(null=True, blank=True)
    fixed = models.NullBooleanField()
    campaigndata = models.IntegerField(null=True, blank=True)
    quote_set = models.ForeignKey('QuoteSet', null=True, blank=True)
    merged_into = models.ForeignKey(
        'self', null=True, blank=True, on_delete=models.PROTECT
    )

    objects = RequirementManager()

    def __unicode__(self):
        return u'{} - {}: {}'.format(self.id, self.kind, self.current_state)

    def save(self, *args, **kwargs):
        super(Requirement, self).save(*args, **kwargs)
        if settings.CACHE_REQUIREMENT_IN_REDIS:
            redis_utils.Publisher.update_object(self)

    def is_ready_to_reopen(self):
        if self.current_state and self.current_state.name in ["CLOSED_unresolved", "DUPLICATE_deleted"]:
            return True
        else:
            return False

    def reopen(self, new_state=None, previous_state=False, initial_state=False):
        if self.kind.machine:
            if new_state:
                self.current_state = self.kind.machine.states.filter(name=new_state).first()

            if initial_state:
                self.current_state = self.kind.machine.initial_state if self.machine else None

            if previous_state and self.state_history.all():
                requirement_history = RequirementStateHistory.objects.filter(
                    requirement=self
                ).last()
                if requirement_history and requirement_history.previous_state:
                    self.current_state = requirement_history.previous_state

            if self.current_state:
                self.save(update_fields=['current_state'])
                return True

    def merge(self, merge_into):
        assert isinstance(merge_into, Requirement)
        assert self != merge_into
        assert merge_into.merged_into is None

        Activity.objects.filter(requirement=self).update(requirement=merge_into)
        self.merged_into = merge_into
        self.save(update_fields=["merged_into"])

        if self.created_date < merge_into.created_date:
            merge_into.created_date = self.created_date
            merge_into.save(update_fields=["created_date"])

    def previous_state(self):
        current_state = self.state_history.all().last().previous_state
        if current_state:
            self.current_state = current_state
            self.save(update_fields=['current_state'])

    def get_quote_url(self):
        extra = self.extra
        url = ''
        if not extra:
            pass
        elif self.kind.product == 'health':
            url = '{site_url}/health-plan/results/#{activity_id}'
        elif self.kind.product == 'car':
            url = '{site_url}/car-insurance/{quote_id}/'
        elif self.kind.product == 'bike':
            url = '{site_url}/twowheeler-insurance/{quote_id}/'
        elif self.kind.product == 'travel':
            url = '{site_url}/travel-insurance/{activity_id}/'
        return url.format(**extra)

    def hide(self):
        self.next_crm_task = None
        self.current_state = sm_models.State.objects.get(id=17)
        self.save()
        for t in self.tasks.filter(is_disposed=False):
            self.tasks.remove(t)

    def get_complete_history(self):
        activities = Activity.objects.filter(
            requirement=self,
        ).annotate(end_time=F('created')).values('end_time', 'activity_type', 'page_id')
        for act in activities:
            act['user'] = 'Customer Activity'
            act['category'] = act.get('page_id')
        crm_activities = self.crm_activities.filter(
            end_time__isnull=False
        ).annotate(activity_type=F('sub_disposition__verbose')).values(
            'end_time', 'activity_type',
            'user__user__first_name', 'user__user__last_name',
            'category'
        )
        for act in crm_activities:
            act['user'] = "%s %s" % (act['user__user__first_name'], act['user__user__last_name'])

        activities = list(activities) + list(crm_activities)
        if activities:
            activities = sorted(activities, key=lambda x: x['end_time'], reverse=True)
        return activities

    def attach_activity(self, activity):
        self.last_web_activity = activity
        if not self.fd:
            args = {}
            if activity.user:
                args["user"] = activity.user
            else:
                args["visitor"] = activity.tracker
            self.fd = FormData.objects.create(**args)

        self.save(update_fields=['last_web_activity', 'fd'])
        self.fd.update(activity.fd)
        self.update_mid(activity)

    def update_mid(self, activity):
        self.mid = activity.mid or self.mid
        self.save(update_fields=['mid'])

    def update_extra(self, extra_data):
        self.extra = self.extra or {}
        self.extra.update(extra_data)
        self.save(update_fields=['extra'])

    def mini_json(self):
        if settings.CALL_REQUIREMENT_FIX:
            self.fix()
        owner_name = ''
        owner_id = ''
        if self.owner:
            owner_name = "%s" % (
                self.owner.user.get_full_name() if self.owner.user.get_full_name() else self.owner.id
            )
            owner_id = self.owner.id
        undisposed_calls = self.crm_activities.filter(end_time__isnull=True)
        last_crm_activity = self.last_crm_activity
        call_history_count = self.crm_activities.exclude(end_time__isnull=True).count()
        next_task = self.next_crm_task
        next_task_queue = next_task.queue if next_task else None
        next_task_assigned = next_task.assigned_to if next_task else None
        last_activity = self.last_web_activity
        tstatus = self.state_history.filter(name__in=['POLICYPENDING_paymentdone']).exists()

        undisposed = self.undisposed_calls()
        taken_by_caller = undisposed.latest('start_time').user.user.get_full_name() if undisposed.exists() else '',
        # rdata = self.get_data()
        # name = rdata.get('name')
        cmobile = ''
        cemail = ''
        if self.user:
            cmobile = self.user.primary_phone
            cemail = self.user.primary_email
        elif self.visitor:
            cmobile = self.visitor.primary_phone
            cemail = self.visitor.primary_email

        return {
            'id': self.id,
            'next_task_id': next_task.id if next_task else '',
            'next_task_assigned_id': next_task_assigned.id if next_task_assigned else '',
            'created_on': utils.convert_datetime_to_timezone(self.created_date).strftime("%d %b %y %I:%M %p"),
            'notes': self.remark,
            'state': self.current_state.verbose if self.current_state else '',
            'first_name': self.user.first_name if self.user else '',
            'middle_name': self.user.middle_name if self.user else '',
            'last_name': self.user.last_name if self.user else '',
            'name': self.user.get_full_name() if self.user else '',
            'mobile': cmobile if cmobile else '',
            'email': cemail if cemail else '',
            'product_type': self.kind.product.lower(),
            'campaign': next_task_queue.name if next_task_queue else '',
            'owner_name': owner_name,
            'owner_id': owner_id,
            'next_call_time': utils.convert_datetime_to_timezone(
                next_task.scheduled_time
            ).strftime("%d %b %y %I:%M %p") if next_task else None,
            'call_history_count': call_history_count,
            'call_history_summary': [
                (
                    act.disposition.verbose if act.disposition else '',
                    act.sub_disposition.verbose if act.sub_disposition else '',
                    utils.convert_datetime_to_timezone(act.end_time).strftime("%d/%m/%Y")
                )
                for act in self.crm_activities.filter(end_time__isnull=False).order_by('-end_time')[:4]
            ],
            'last_head_disp_verbose': last_crm_activity.disposition.verbose if last_crm_activity and last_crm_activity.disposition else '',
            'last_call_disp': last_crm_activity.sub_disposition.verbose if last_crm_activity and last_crm_activity.disposition else '',
            'last_call_time': utils.convert_datetime_to_timezone(
                last_crm_activity.end_time
            ).strftime("%d %b %y %I:%M %p") if last_crm_activity else '',
            'utm_campaign': self.mid.campaign if self.mid else '',
            'label': last_activity.label if last_activity else '',
            'is_taken': undisposed_calls.exists(),
            'online_transaction_status': 'success' if tstatus else 'failure',
            'is_online_transaction_attempted': 'inline' if self.activity_set.filter(activity_type='make_payment') else 'none',
            'taken_by_caller': taken_by_caller,
            'age': '',
            'gender': '',
            'agent': '',
            'dnd_status': '',
            'vehicle_reg_no': '',
            'rto_info': '',
            'reference': '',
            'past_policy_expiry_date': '',
            'lead_status': '',
            'is_lead_status': 'none',
        }

    def undisposed_calls(self):
        return self.crm_activities.filter(end_time__isnull=True)

    def create_old_activity(self, hist_dict):
        from core.forms import RequirementForm
        import generate_form_data
        DISPOSITION_DB_EVENT_MAP = {
            'BOUGHT_ONLINE_REVIEW_PENDING': ('success', 'proposal'),
            'ONLINE_TRANSACTION_FAILED': ('failed', 'proposal'),
            'PROPOSAL_FAILED': ('bounce', 'proposal'),
            'ONLINE_PAYMENT_FAILED': ('failed', 'proposal'),
            'INSURER_DOWN': ('insurer_server_down', 'proposal'),
            'ROLLOVER_CASE': ('viewed', 'rollover_not_allowed')
        }

        try:
            ch_data = json.loads(hist_dict['changed_data'])
        except:
            ch_data = {}
        if DISPOSITION_DB_EVENT_MAP.get(hist_dict['call_disposition']):
            activity_type, page_id = DISPOSITION_DB_EVENT_MAP[
                hist_dict['call_disposition']]
        else:
            activity_type = 'success'
            if ch_data.get('transaction_id'):
                page_id = 'proposal'
            elif '/lp/' in str(ch_data.get('triggered_page', '')):
                page_id = ch_data.get('label') or 'lp_not_found'
            elif ch_data.get('page_id') or ch_data.get('quoteId'):
                page_id = 'quotes'
            else:
                page_id = 'unknown'

        form_type = page_id
        if page_id != 'proposal':
            form_type = 'quotes'

        if self.kind.product in ['car', 'bike', 'motor', 'bike', 'health']:
            fd_dict = generate_form_data.get_form_data(
                ch_data,
                form_type,
                product=self.kind.product
            )
        else:
            fd_dict = {}

        activity = Activity.objects.create(
            requirement=self,
            tracker=self.visitor,
            user=self.user,
            extra=ch_data,
            mid_id=RequirementForm.get_mid_obj(ch_data).id
        )
        for k, v in ch_data.items():
            if Activity.ACTIVITY_PARAM_MAPPING.get(k) and v:
                if hasattr(activity, Activity.ACTIVITY_PARAM_MAPPING[k]):
                    setattr(activity, Activity.ACTIVITY_PARAM_MAPPING[k], v)
        activity.activity_type = activity_type
        activity.page_id = page_id
        activity.product = self.kind.product
        activity.set_data(fd_dict, user=self.user, tracker=self.visitor)
        activity.save()
        activity.created_date = hist_dict['created_on']
        activity.save()
        self.update(activity)

        return activity

    def fix_next_crm_task(self):
        from crm import models as crm_models
        import lms_campaign_queue

        tc_callbacklater = crm_models.TaskCategory.objects.get(name='CALLBACK_LATER')
        if not self.campaigndata:
            return

        with connection.cursor() as cursor:
            cursor.execute(
                "select * from olms.core_campaigndata where id=%s",
                [str(self.campaigndata)]
            )
            cd = utils.dictfetchall(cursor)
            if not cd:
                return

            cd = cd[0]
            tasks = self.tasks.all()
            self.next_crm_task = None
            self.save()
            tasks.delete()
            scheduled_time = cd['next_call_time']
            if scheduled_time:
                assigned_to_id = cd['assigned_to_id']
                campaign_id = cd['campaign_id']
                if int(assigned_to_id) == 31:
                    assigned_to = None
                else:
                    assigned_to = self.owner
                task = crm_models.Task.objects.create(
                    category=tc_callbacklater,
                    assigned_to=assigned_to,
                    scheduled_time=scheduled_time,
                    queue_id=lms_campaign_queue.id_map.get(int(campaign_id), 117)
                )
                self.tasks.add(task)
                self.next_crm_task = task
                self.save()

    def fix(self):
        with connection.cursor() as cursor:
            with django_transaction.atomic():
                self._fix(cursor)

    def _fix(self, cursor):
        from core.forms import RequirementForm
        from crm import models as crm_models
        import lms_conf

        if self.fixed is None:
            self.fixed = True
            self.save(update_fields=["fixed"])

        if self.fixed:
            return

        core_actvities_disps = [
            'BOUGHT_ONLINE_REVIEW_PENDING',
            'ONLINE_TRANSACTION_FAILED',
            'PROPOSAL_FAILED',
            'ONLINE_PAYMENT_FAILED',
            'INSURER_DOWN',
            'ROLLOVER_CASE',
            'DUPLICATE'
        ]

        advisors_map = lms_conf.user_advisor_map

        cursor.execute(
            "select * from olms.core_campaigndata where id=%s",
            [str(self.campaigndata)]
        )
        cd = utils.dictfetchall(cursor)
        if not cd:
            pass
        cd = cd[0]
        try:
            cd_data = json.loads(cd['data'])
        except:
            cd_data = {}
        status = cd['status']

        histories = [{
            'caller_id': 5,
            'changed_data': cd['initial_data'],
            'call_disposition': 'DUPLICATE',
            'call_type': '',
            'get_call_time': '',
            'created_on': self.created_date,
        }]

        cursor.execute(
            """
                select
                    history_id
                from olms.core_campaigndata_history
                where campaigndata_id=%s
            """, [self.campaigndata]
        )
        hist_ids = tuple([int(item[0]) for item in cursor.fetchall()])
        if hist_ids:
            cursor.execute(
                "select * from  olms.core_history where id in %s order by id asc;",
                [hist_ids]
            )
            histories.extend(utils.dictfetchall(cursor))

        self.mid = RequirementForm.get_mid_obj(cd_data)

        advisor_activities = []

        lms_adv_hists_ids = []
        i = -2
        for hist in histories:
            cl_disp = hist['call_disposition']
            # 5 id in LMS is system
            try:
                ch_data = json.loads(hist['changed_data'])
            except:
                ch_data = {}
            i += 1
            if (
                str(hist['caller_id']) == '5' and cl_disp in core_actvities_disps
            ):
                # this means: this is a web activity
                hdata = json.dumps(ch_data)
                cust_act = self.create_old_activity(hist)
                if i >= 0:
                    utils.lms_histories_logger.info(
                        "core_activity-%s:olms.core_history-%s"
                        % (cust_act.id, hist_ids[i])
                    )

                continue

            lms_adv_hists_ids.append(hist['id'])

            # this is call center activity
            if hist['caller_id'] == '5':
                # call center system activity
                if hist['call_disposition'] == 'ASSIGNED_TO_CALLER':
                    category = 'TRANSFER'
                    hdata = hist['data'] or '{}'
                else:
                    category = hdata['call_disposition']
                    hdata = hist['changed_data'] or '{}'

                start_time = hist['created_on']
                disposition_id = None
                sub_disposition_id = None
            else:
                # actual person did something
                hdata = hist['changed_data'] or '{}'

                category = 'OUTGOING_CALL'
                if hist['call_type'] == 'in':
                    category = 'INCOMING_CALL'

                if hist['get_call_time']:
                    start_time = hist['get_call_time']
                else:
                    start_time = hist['created_on']

                sub_disposition, _ = crm_models.SubDisposition.objects.get_or_create(
                    value=hist['call_disposition']
                )

                if sub_disposition.head_dispositions.last():
                    disposition_id = sub_disposition.head_dispositions.last().id
                else:
                    disposition_id = None

                sub_disposition_id = sub_disposition.id

            end_time = hist['created_on']

            advisor_activities.append((
                advisors_map.get(hist['caller_id'], 62),  # 62 is unassigned
                start_time,
                end_time,
                disposition_id,
                sub_disposition_id,
                hdata,
                category,
            ))

        # CREATE ALL ADVISOR ACTIVITIES
        if advisor_activities:
            sql_query = ["""
                insert into crm_activity (
                    user_id, start_time, end_time,
                    disposition_id, sub_disposition_id, data, category
                ) values
            """]
            for act in advisor_activities:
                act = tuple([utils.myadapt(a) for a in act])
                sql_query.append(
                    """(
                        %s, TIMESTAMP %s, TIMESTAMP %s, %s, %s, %s, %s
                    ), """ % act
                )
            sql_query = " ".join(sql_query)[:-2] + " returning id;"
            cursor.execute(sql_query)
            adv_act_ids = [item[0] for item in cursor.fetchall()]
            self.crm_activities.add(*adv_act_ids)
            utils.lms_histories_logger.info(
                "crm_activity-%s:olms.core_history-%s"
                % (str(adv_act_ids), str(lms_adv_hists_ids))
            )

        if status != 'CONCLUDED' and not self.next_crm_task:
            task = crm_models.Task.objects.create(
                category=crm_models.TaskCategory.objects.get_or_create(
                    name='OUTGOING_CALL'
                )[0],
                assigned_to_id=lms_conf.user_advisor_map.get(cd['assigned_to_id']) if cd['assigned_to_id'] != 31 else None,
                scheduled_time=cd['next_call_time'] or timezone.now(),
                queue_id=(
                    lms_conf.campaign_queue_map.get(cd['campaign_id']) or crm_models.Queue.objects.last().id
                )
            )
            self.tasks.add(task)
            self.next_crm_task = task

        self.last_crm_activity = self.crm_activities.latest('start_time') if self.crm_activities.last() else None
        self.last_web_activity = self.activity_set.latest('created') if self.activity_set.last() else None
        self.owner_id = lms_conf.user_advisor_map.get(cd['assigned_to_id']) if cd['assigned_to_id'] != 31 else None
        self.created_date = cd['created_on']
        cursor.execute("select core_user_id from olms.core_customer where id=%s;", [int(cd['customer_id'])])
        core_user_id = cursor.fetchone()
        self.user_id = core_user_id[0] if core_user_id else None
        cursor.execute("select core_tracker_id from olms.core_customer where id=%s;", [int(cd['customer_id'])])
        core_visitor_id = cursor.fetchone()
        self.visitor_id = core_visitor_id[0] if core_visitor_id else None

        self.fixed = True
        self.save()


class InsurerTransaction(models.Model):
    fd = models.ForeignKey(FormData)
    requirement = models.ForeignKey(Requirement)
    proposal_number = models.CharField(max_length=250, blank=True)
    policy_number = models.CharField(max_length=250, blank=True)
    reference_id = models.CharField(max_length=250, blank=True)
    policy_token = models.CharField(max_length=250, blank=True)
    payment_token = models.CharField(max_length=250, blank=True)
    premium_insurance = models.CharField(max_length=10, blank=True)
    premium_paid = models.CharField(max_length=10, blank=True)
    raw = JSONField(_('raw'), blank=True, default={})
    policy_request = models.TextField(blank=True)
    policy_response = models.TextField(blank=True)
    payment_request = models.TextField(blank=True)
    payment_response = models.TextField(blank=True)
    payment_redirect_response = models.TextField(blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)


def template_exists(val):
    """
    pass the template path string as you pass into a context processor
    and it will validate whether that file exists and and if its a proper
    django html template
    """
    if val:
        try:
            get_template(val)
        except TemplateDoesNotExist:
            raise ValidationError(
                'Template file not found "%s"' % val
            )


class Notification(BaseModel):

    MTA = (
        ('mandrill', 'Mandrill Api'),
    )

    PRIORITY = (
        ('high', 'High'),
        ('normal', 'Normal'),
        ('low', 'Low'),
    )

    nid = models.CharField(max_length=100, unique=True)
    about = models.TextField()
    to = ArrayField(models.EmailField(), default=[], blank=True, null=True,
                    help_text="The default receivers of email. Enter email address separated by ','")
    bcc = ArrayField(models.EmailField(), default=[], blank=True, null=True,
                     help_text="Email bcc. Enter email address separated by ','")
    subject = models.CharField(max_length=100, blank=True,
                               validators=[template_exists],
                               help_text='Template Name if using a template')
    subject_content = models.TextField(blank=True, help_text='Email subject content')
    html = models.CharField(max_length=100, blank=True,
                            validators=[template_exists])
    html_content = models.TextField(blank=True)
    text = models.CharField(max_length=100, blank=True,
                            validators=[template_exists])
    text_content = models.TextField(blank=True)
    sms = models.CharField(max_length=100, blank=True,
                           validators=[template_exists])
    sms_content = models.TextField(blank=True)
    numbers = ArrayField(models.CharField(max_length=12), default=[], blank=True, null=True,
                         help_text="The default receivers of sms. Enter numbers separated by ','")
    mta = models.CharField(max_length=100, choices=MTA, default=settings.DEFAULT_MTA)
    is_template = models.BooleanField(
        default=False,
        help_text="This tells whether an object is used as a template or not"
    )

    sender_name = models.CharField(max_length=100, default=settings.DEFAULT_FROM_NAME)
    sender_email = models.CharField(max_length=100, default=settings.DEFAULT_FROM_EMAIL)
    reply_to = models.CharField(max_length=100, null=True, blank=True, help_text="Reply to header")  # reply to header.

    priority = models.CharField(max_length=10, choices=PRIORITY, default='normal')
    sync = models.BooleanField(
        default=False,
        help_text="Set true to send notifications synchronously"
    )

    def _get_template_text(self, field_name, ctx={}, raw=False):
        TEMPLATE_ENGINE = engines['django']

        is_file = False
        val = ''
        if ctx.get("%s_content" % field_name):
            val = ctx["%s_content" % field_name]
            is_file = False
        elif ctx.get(field_name):
            val = ctx[field_name]
            is_file = True
        elif getattr(self, "%s_content" % field_name):
            val = getattr(self, "%s_content" % field_name)
            is_file = False
        elif getattr(self, field_name):
            val = getattr(self, field_name)
            is_file = True

        if raw:
            if is_file:
                temp = get_template(val)
                f = open(temp.origin.name)
                return_val = f.read()
                f.close()
            else:
                return_val = val
        else:
            if is_file:
                temp = get_template(val)
            else:
                temp = TEMPLATE_ENGINE.from_string(val)
            return_val = temp.render(ctx)
        return return_val

    def get_subject(self, ctx={}, raw=False):
        return self._get_template_text('subject', ctx=ctx, raw=raw)

    def get_text(self, ctx={}, raw=False):
        return self._get_template_text('text', ctx=ctx, raw=raw)

    def get_html(self, ctx={}, raw=False):
        return self._get_template_text('html', ctx=ctx, raw=raw)

    def get_sms(self, ctx={}, raw=False):
        return self._get_template_text('sms', ctx=ctx, raw=raw)

    def get_all_vars(self):
        fields = ['subject', 'html', 'text', 'sms']
        list_of_vars = []
        for field in fields:
            val = getattr(self, "get_%s" % field)(raw=True)
            list_of_vars.extend(utils.get_vars_from_template(val))

        return list(set(list_of_vars))

    def template_json(self, ctx={}, raw=False):
        return {
            'pk': self.pk,
            'subject': self.get_subject(ctx=ctx, raw=raw),
            'html': self.get_html(ctx=ctx, raw=raw),
            'text': self.get_text(ctx=ctx, raw=raw),
            'sms': self.get_sms(ctx=ctx, raw=raw),
        }

    def get_sender(self):
        return '%s <%s>' % (self.sender_name, self.sender_email)
    get_sender.short_description = "Sender Email"


class PhoneManager(models.Manager):
    def verify(self):
        # If it fails due to any reason then return 404,
        # else verify and return None
        return None

    def fix_duplicates(self, user):
        duplicates = Phone.objects.values('number').annotate(Count('number')).filter(number__count__gt=1, user=user)
        # Sample output: [{'number': u'9999999990', 'number__count': 4}, {'number': u'9004038889', 'number__count': 3}]
        extlogger.info("Duplicates found for user=%s duplicates=%s" % (user.id, json.dumps(list(duplicates))))

        for duplicate in duplicates:
            number = duplicate['number']
            dup_phones = Phone.objects.filter(number=number, user=user)

            final_phone = dup_phones[0]
            for phone in dup_phones[1:]:
                if phone.is_verified:
                    final_phone.is_verified = True
                if phone.is_primary:
                    final_phone.is_primary = True
                if phone.is_bogus:
                    final_phone.is_bogus
                if not phone.is_exists:
                    final_phone.is_exists = False

                extlogger.info("Saving phone=%s for duplicate set for number=%s" % (final_phone.id, number))
                final_phone.save()

                for phone in dup_phones[1:]:
                    extlogger.info("Deleting duplicate: number=%s with id=%s" % (
                        phone.number, phone.id
                    ))
                    phone.delete()

            if final_phone.is_primary and Phone.objects.filter(
                is_primary=True, user=user
            ).exclude(id=final_phone.id).count():
                extlogger.info("Making final_phone=%s number=%s as primary for user=%s" % (
                    final_phone.id, final_phone.number, user.id
                ))
                final_phone.is_primary = False
                final_phone.save()


class Phone(BaseModel, OTPModelMixin):
    visitor = models.ForeignKey('Tracker', related_name='phones', null=True)
    user = models.ForeignKey('User', related_name='phones', null=True)
    number = models.CharField(max_length=20, db_index=True,
                              validators=[mobile_validator])
    is_primary = models.BooleanField(default=False, db_index=True)

    #  Mark Mobile as exists if call connects or if there is incoming call
    is_exists = models.BooleanField(default=False)

    # If call doesn't connect since number is non existent
    is_bogus = models.BooleanField(default=False)

    # Once confirmed that the number belongs to the said
    # user, mark as verified
    # Can have a provision for Verification Call
    is_verified = models.BooleanField(default=False, db_index=True)
    verification_code = models.CharField(max_length=8, null=True, db_index=True)
    objects = PhoneManager()

    def __unicode__(self):
        return "%s" % self.__dict__

    class OTPConfig(OTPModelMixin.OTPConfig):
        contact_type = 'mobile'
        contact_field = 'number'

    def send_verification_code(self, nid=None):
        import cf_cns
        otp = self._make_verification_code(6, '0123456789')
        nid = nid or 'MYACCOUNT_OTP'
        ctx = {'validity': '15 mins', 'otp': otp}
        contact_value = self.contact
        _, resp = cf_cns.notify(nid=nid, obj=ctx, sync=True,
                                numbers=contact_value)
        if isinstance(resp, basestring):
            # NOTIFICATION_DEBUG = True
            success = True
        else:
            try:
                success = 'success' in \
                          resp.get('sms_response', '')[0].strip().split('|')[0]
            except Exception as e:
                logger.debug(msg='Failed to send OTP', exc_info=e)
                success = False
        if success:
            self.verification_code = otp
            self.save(update_fields=['verification_code'])
            cache.set(
                key=self.cache_key,
                value=True,
                timeout=15 * 60
            )
        return success

    def check_verification_code(self, otp):
        in_cache = cache.get(self.cache_key) is not None
        valid = in_cache and self.verification_code == otp
        if valid:
            cache.delete(self.cache_key)
        return valid


class EmailManager(models.Manager):
    def verify(self):
        # TODO:
        # If it fails due to any reason then return 404,
        # else verify and return None
        return None

    def fix_duplicates(self, user):
        duplicates = Email.objects.values('email').annotate(Count('email')).filter(email__count__gt=1, user=user)
        # Sample output: [{'email__count': 2, 'email': 'devendra@gmail.com'}, {'email__count': 3, 'email': 'ranedk@gmail.com'}]
        extlogger.info("Duplicates found for user=%s duplicates=%s" % (user.id, json.dumps(list(duplicates))))

        for duplicate in duplicates:
            email_id = duplicate['email']
            dup_emails = Email.objects.filter(email=email_id, user=user)

            final_email = dup_emails[0]
            for email in dup_emails[1:]:
                if email.is_verified:
                    final_email.is_verified = True
                if email.is_primary:
                    final_email.is_primary = True
                if email.is_bogus:
                    final_email.is_bogus = True
                if not email.is_exists:
                    final_email.is_exists = False

            extlogger.info("Saving email=%s for duplicate set for email=%s" % (final_email.id, email_id))
            final_email.save()

            for email in dup_emails[1:]:
                extlogger.info("Deleting duplicate: email=%s with id=%s" % (
                    email.email, email.id
                ))
                email.delete()

            if final_email.is_primary and Email.objects.filter(
                is_primary=True, user=user
            ).exclude(id=final_email.id).count():
                extlogger.info("Making final_email=%s email=%s as primary for user=%s" % (
                    final_email.id, final_email.email, user.id
                ))
                final_email.is_primary = False
                final_email.save()


class Email(BaseModel, OTPModelMixin):
    visitor = models.ForeignKey('Tracker', related_name='emails', null=True)
    user = models.ForeignKey('User', related_name='emails', null=True)
    email = custom_fields.CIEmailField(
        _('e-mail address'), max_length=255,
        blank=False, db_index=True
    )
    is_primary = models.BooleanField(default=False, db_index=True)

    #  Mark Email as exists if mail doesn't bounce or
    #  we get an incoming mail from the email address
    is_exists = models.BooleanField(default=False)

    # If email bounces, mark this as bogus, delete it later
    is_bogus = models.BooleanField(default=False)

    # Once confirmed that email-address belongs to the said
    # user, mark as verified
    # Can have a provision for Verification Email
    is_verified = models.BooleanField(default=False, db_index=True)
    verification_code = models.CharField(max_length=25, null=True, db_index=True)
    objects = EmailManager()

    def __unicode__(self):
        return self.email

    class OTPConfig(OTPModelMixin.OTPConfig):
        contact_type = 'email'
        contact_field = 'email'

    def send_verification_code(self, nid=None):
        import cf_cns
        otp = self._make_verification_code(6, '0123456789')
        nid = nid or 'MYACCOUNT_OTP'
        ctx = {'validity': '15 mins', 'otp': otp}
        contact_value = self.contact
        _, resp = cf_cns.notify(nid=nid, obj=ctx, sync=False,
                                to=contact_value, mandrill_template='newbase')
        self.verification_code = otp
        self.save(update_fields=['verification_code'])
        cache.set(
            key=self.cache_key,
            value=True,
            timeout=15 * 60
        )
        return True

    def check_verification_code(self, otp):
        in_cache = cache.get(self.cache_key) is not None
        valid = in_cache and self.verification_code == otp
        if valid:
            cache.delete(self.cache_key)
        return valid

    @property
    def exists(self):
        return True


class QuoteSet(BaseModel):
    #  Extended by product
    SORT_CHOICES = utils.to_namedtuple((
        'best_match',
        'premium',
    ))

    activity = models.ForeignKey(Activity)
    sort = models.CharField(max_length=20, choices=SORT_CHOICES.__dict__.items())
    last_compare = models.ForeignKey('core.QuoteCompare', null=True, blank=True)


class Quote(BaseModel):
    #  Extended by product
    ACTION_CHOICES = utils.to_namedtuple((
        'see_details',
        'see_premium_breakup',
        'add_to_compare',
        'buy',
    ))

    quote_set = models.ForeignKey(QuoteSet)
    premium = models.FloatField()
    insurer = models.ForeignKey(Insurer)
    actions = ArrayField(base_field=models.CharField(max_length=20, choices=ACTION_CHOICES.__dict__.items()))
    position = custom_fields.IntegerRangeField(min_value=1, max_value=50)


class QuoteCompare(BaseModel):
    quotes = models.ManyToManyField(Quote)


class UserDocument(Document):
    pass


class EndorsementReciept(Document):
    pass


class EndorsementDetail(BaseModel):
    INSURERS = (
        ('car', (
            ('l-t', 'L & T'),
            ('iffco-tokio', 'Iffco Tokio'),
            ('bharti-axa', 'Bharti Axa'),
            ('tata-aig', 'Tata Aig'),
            ('reliance', 'Reliance'),
            ('bajaj-allianz', 'Bajaj Allianz'),
            ('hdfc-ergo', 'HDFC Ergo'),
            ('universal-sompo', 'Universal Sompo'),
            ('liberty', 'Liberty Videocon'),
        )),
        ('bike', (
            ('l-t', 'L & T'),
            ('iffco-tokio', 'Iffco Tokio'),
            ('bharti-axa', 'Bharti Axa'),
            ('tata-aig', 'Tata Aig'),
            ('reliance', 'Reliance'),
            ('bajaj-allianz', 'Bajaj Allianz'),
            ('hdfc-ergo', 'HDFC Ergo'),
            ('universal-sompo', 'Universal Sompo'),
            ('liberty', 'Liberty Videocon'),
        )),
        ('health', (
            ('religare', 'Religare'),
            ('l-t', 'L & T'),
            ('apollo', 'Apollo Munich'),
            ('max-bupa', 'Max Bupa'),
            ('iffco-tokio', 'Iffco Tokio'),
            ('bharti-axa', 'Bharti Axa'),
            ('tata-aig', 'Tata Aig'),
            ('star', 'Star Health'),
            ('oriental', 'Oriental Health'),
            ('bajaj-allianz', 'Bajaj Allianz'),
            ('cigna-ttk', 'Cigna TTK'),
            ('hdfc-ergo', 'HDFC Ergo'),
        )),
        ('travel', (
            ('religare', 'Religare'),
            ('apollo', 'Apollo Munich'),
            ('bharti-axa', 'Bharti Axa'),
            ('reliance', 'Reliance'),
            ('bajaj-allianz', 'Bajaj Allianz'),
            ('hdfc-ergo', 'HDFC Ergo'),
        )),
    )

    PRODUCTS = utils.to_namedtuple(dict(INSURERS).keys()).__dict__.items()

    ENDORSEMENT_FIELDS = tuple(
        (sub_kind, sub_kind.title())
        for sub_kind in set(
            end_sub_kind
            for values in RequirementKind.ENDORESEMENT_MAP.values()
            for value_list in values.values()
            for end_sub_kind in value_list)
    )

    policy_number = models.CharField(max_length=30)
    policy_name = models.CharField(max_length=100)
    customer_name = models.CharField(max_length=30)
    customer_email = custom_fields.CIEmailField(_('Customer Email'), db_index=True)
    product = models.CharField(max_length=250, choices=PRODUCTS)
    insurer = models.CharField(max_length=250, choices=INSURERS)
    endorsement_type = models.CharField(max_length=30, choices=RequirementKind.ENDORESEMENTS)

    field = models.CharField(max_length=30, choices=ENDORSEMENT_FIELDS)
    current_value = models.CharField(max_length=250, blank=True)
    endorsed_value = models.CharField(max_length=250, blank=True)

    user_documents = models.ManyToManyField(UserDocument, related_name='endorsements', blank=True)

    reciept = models.OneToOneField(EndorsementReciept, related_name='endorsement', null=True, blank=True)
    completed_on = models.DateTimeField(null=True, blank=True)

    class Meta:
        permissions = (
            ('view_endorsementDetail', 'Can view endorsements'),
        )

    def clean(self):
        super(EndorsementDetail, self).clean()
        if self.product and self.endorsement_type and self.field:
            try:
                fields = RequirementKind.ENDORESEMENT_MAP[self.product][self.endorsement_type]
            except KeyError:
                pass
            else:
                if self.field not in fields:
                    raise ValidationError(
                        '{} of is not a valid option for {} on {}'.format(
                            self.endorsement_type,
                            self.product,
                            self.field
                        )
                    )

        if self.completed_on and not self.reciept:
            raise ValidationError(
                'Cannot complete endorsement without reciept'
            )

    @property
    def insurer_email(self):
        key = '{}-{}'.format(self.product, self.insurer)
        insurer_emails = {
            'religare': 'customerfirst@religarehealthinsurance.com',
            'health-l-t': 'Haroon.Shaikh@ltinsurance.com',
            'l-t': 'noor.shaikh@ltinsurance.com',
            'apollo': 'customerservice@apollomunichinsurance.com',
            'max-bupa': 'Ajay.Kumar2@maxbupa.com',
            'iffco-tokio': 'Shubhangi.Kalambe@iffcotokio.co.in',
            'bharti-axa': 'BAGI.Service@bharti-axagi.co.in',
            'health-tata-aig': 'Mahesh.Kumar2@tata-aig.com',
            'tata-aig': 'ecomsupport@tata-aig.com',
            'star': 'ghatkopar.mumbai@starhealth.in',
            'oriental': 'srilata.menon@orientalinsurance.co.in',
            'reliance': 'rgicl.services@relianceada.com',
            'bajaj-allianz': 'Arati.Nehe@bajajallianz.co.in',
            'cigna-ttk': 'Manali.Khedekar@Cignattk.in',
            'hdfc-ergo': 'Prathamesh.Gole@hdfcergo.com',
            'universal-sompo': 'contactus@universalsompo.com',
            'liberty': 'Apeksha.Bhat@libertyvideocon.com',
        }
        return insurer_emails.get(key, insurer_emails[self.insurer])

    @property
    def acr(self):
        return default_storage.url('uploads/core/acr/{}.pdf'.format(self.id))
