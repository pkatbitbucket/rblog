from django.shortcuts import redirect
from social.pipeline.partial import partial
from core.models import User
from django.http import Http404, HttpResponse, HttpResponseRedirect

import datetime


@partial
def require_email(strategy, details, user=None, is_new=False, *args, **kwargs):
    if user and user.email:
        return
    elif is_new and not details.get('email'):
        if strategy.session_get('saved_email'):
            details['email'] = strategy.session_pop('saved_email')
        else:
            return redirect('require_email')


def create_user(strategy, details, response, uid, user=None, *args, **kwargs):
    if user:
        return {'is_new': False}

    fields = dict((name, kwargs.get(name) or details.get(name))
                  for name in strategy.setting('USER_FIELDS',
                                               []))

    if not fields:
        return

    user = None
    email = fields.get('email')
    assert fields[
        'username'], "Username/email couldn't be fetched during social auth login"
    if email:
        try:
            user = User.objects.get_user(email=email, phone='')
        except User.DoesNotExist:
            pass

    return {
        'is_new': not bool(user),
        'user': user or strategy.create_user(**fields),
    }


def load_extra_data(strategy, details, response, uid, user, *args, **kwargs):
    details.update({
        'gender': {
            'male': 1,
            'female': 2,
            None: None,
        }[response.get('gender')],
        'birth_date': datetime.datetime.strptime(response['birthday'], '%m/%d/%Y') if response.get('birthday') else None,

    })
    user.save_extra_data(response)
