from __future__ import unicode_literals

import logging

from django.db.models import Q

from core import models as core_models
from core import forms as core_forms
from statemachine.utils import state_machine
from statemachine.exceptions import StateMachineException
import utils

logger = logging.getLogger(__name__)
extlogger = logging.getLogger('extensive')


def has_contact(activity):
    """
    :param activity: core.models.Activity
    :return: bool
    """
    user = activity.user
    visitor = activity.tracker

    # Check mobile email found in user instance.
    email_mobile_found = False
    if user and user.emails.count():
        email_mobile_found = True

    if user and user.phones.count():
        email_mobile_found = True

    # Check mobile email phone in visitor.
    if visitor and visitor.emails.count():
        email_mobile_found = True

    if visitor and visitor.phones.count():
        email_mobile_found = True

    if not activity.fd:
        return email_mobile_found

    form_data = activity.fd.get_data()

    owner = {}
    if user:
        owner["user"] = user
    else:
        owner["visitor"] = visitor

    # Check into form data.
    for form_name, data in form_data.iteritems():
        if form_name != "core.forms.Person":
            continue

        if not isinstance(data, list):
            data = [data]

        email = None
        phone = None

        for d in data:
            form_email = d.get("email", None)
            if form_email and form_email.lower() == 'none':
                form_email = None

            form_mobile = d.get("mobile", None)
            if form_mobile and form_mobile.lower() == 'none':
                form_mobile = None

            if form_email:
                if user:
                    email = core_models.Email.objects.filter(
                        user=user,
                        email=form_email,
                    ).last()
                if not email and visitor:
                    email = core_models.Email.objects.filter(
                        visitor=visitor,
                        email=form_email,
                    ).last()
                if email:
                    email.user = user if user else email.user
                    email.visitor = None if user else email.visitor
                    email.save()
                if not email:
                    core_models.Email.objects.create(
                        email=form_email,
                        **owner
                    )
                email_mobile_found = True

            if form_mobile:
                if user:
                    phone = core_models.Phone.objects.filter(
                        user=user,
                        number=form_mobile,
                    ).last()
                if not phone and visitor:
                    phone = core_models.Phone.objects.filter(
                        visitor=visitor,
                        number=form_mobile,
                    ).last()
                if phone:
                    phone.user = user if user else phone.user
                    phone.visitor = None if user else phone.visitor
                    phone.save()
                if not phone:
                    core_models.Phone.objects.create(
                        number=form_mobile,
                        **owner
                    )
                email_mobile_found = True

    return email_mobile_found


def requirement_engine(activity=None, crm_activity=None, requirement=None):
    """
    :param activity: core.models.Activity
    :param crm_activity: crm.models.Activity
    :return: bool
    """
    from account_manager import utils as account_utils
    from core.activity import assert_mail

    extlogger.info("Requirement engine with activity=%s crm_activity=%s requirement=%s" % (
        activity.id if activity else '',
        crm_activity.id if crm_activity else '',
        requirement.id if requirement else ''
    ))

    # TODO: Remove this later and replace it with an assert
    if not activity and not crm_activity:
        assert_mail("Requirement engine called without activity or crm_activity", locals())
        return

    if activity:
        # Update user fd.
        if activity.user:
            activity.user.update_data(activity.fd)

        if not activity.user and activity.tracker:
            activity.tracker.update_data(activity.fd)

        if requirement:
            activity.attach(requirement)
            try:
                state_machine(requirement=requirement, web_activity=activity)
            except StateMachineException:
                extlogger.info("State Machine exception for requirement=%s | web_activity=%s" % (
                    requirement.id,
                    activity.id
                ))
                pass
            return

        if not has_contact(activity):
            extlogger.info("Activity=%s has no contact" % (activity.id))
            return

        owner = {}
        if activity.user:
            extlogger.info("Activity=%s has user=%s" % (activity.id, activity.user.id))
            owner["user"] = activity.user.id
        else:
            extlogger.info("Activity=%s has tracker=%s" % (activity.id, activity.tracker.id))
            owner["visitor"] = activity.tracker.id

        req_form = core_forms.RequirementForm(
            request=None,
            data=dict(
                product=activity.product,
                form_data=activity.fd.id if activity.fd else None,
                mid=activity.mid.id if activity.mid else None,
                **owner
            )
        )
        assert req_form.is_valid()

        if (
            activity.user
            and activity.user.is_dealer()
            and activity.activity_type == 'viewed'
            and activity.page_id == 'proposal'
        ):
            req = None
        else:
            req = core_models.Requirement.objects.get_open_requirement(
                user=activity.user,
                tracker=activity.tracker,
                kind=req_form.cleaned_data["requirement_kind"],
            )

        if req:
            looping_req = req
            loop_warning = 0
            while req.merged_into:
                assert loop_warning < 10, (
                    "merged_into looped more than 10 times, possible BUG for looping_req.id=%s" % looping_req.id
                )
                req = req.merged_into
                loop_warning += 1

        # If requirement is ready to open then open requirement with new state.
        if req and req.is_ready_to_reopen():
            req.reopen(previous_state=True)

        if req:
            extlogger.info("Attaching activity=%s to requirement=%s" % (activity.id, req.id))
            activity.attach(req)
            try:
                state_machine(requirement=req, web_activity=activity)
            except StateMachineException:
                extlogger.info("State Machine exception for requirement=%s | web_activity=%s" % (
                    requirement.id,
                    activity.id
                ))
                pass
            return
        else:
            if not activity.is_strong():
                extlogger.info("activity=%s is not strong" % (activity.id))
                return

            req = req_form.save()
            activity.attach(req)

            if activity.user:
                req.fd.update(activity.user.fd)
            else:
                req.fd.update(activity.tracker.fd)

            unbound_activites = core_models.Activity.objects.filter(
                product=activity.product, requirement__isnull=True
            )
            if activity.user:
                unbound_activites = unbound_activites.filter(
                    # ideally in this case second clause would always
                    # return nothing as on login as transfer all activities
                    # from tracker to user
                    Q(user=activity.user) | Q(tracker=activity.tracker)
                )
            else:
                unbound_activites = unbound_activites.filter(tracker=activity.tracker)

            extlogger.info("Adding requirement=%s to all unbound activities" % (req.id))
            unbound_activites.update(requirement=req)

            try:
                state_machine(requirement=req, web_activity=activity)
            except StateMachineException:
                extlogger.info("State Machine exception for requirement=%s | web_activity=%s" % (
                    requirement.id,
                    activity.id
                ))
                pass

    if crm_activity:
        requirements = crm_activity.requirements.all()
        if not requirements.exists():
            # get or create requirement
            data = crm_activity.data
            user = account_utils.get_or_create_user(
                email=None, mobile=data.get('mobile')
            )
            request = utils.get_request()
            req_form = core_forms.RequirementForm(
                request=request,
                data=dict(
                    user=user.id,
                    product=data.get('product'),
                    category=data.get('category'),
                    source=data.get('source'),
                    campaign=data.get('campaign')
                )
            )
            req_form.is_valid()
            requirement = req_form.save()
            requirement.crm_activities.add(crm_activity)
            requirement.save()
            extlogger.info("Created new requirement=%s since no requirements with crm_activity is found" % (requirement.id))

        if crm_activity.end_time:
            for requirement in requirements:
                state_machine(requirement=requirement, crm_activity=crm_activity)
                extlogger.info("CRM actirivty has end_time, pushing requirment=%s crm_activity=%s into statemachine" % (requirement.id, crm_activity.id))
        return requirement
