__author__ = "Parag Tyagi"

# setup environment
import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
django.setup()

import sys
import datetime

import utils
import cf_cns
import account_manager.models as account_manager_models


args = sys.argv
is_commit = True if 'commit' in args else False
logger = utils.create_logger(name='account_manager/mail_user_rm_to_csu_transition')


logger.info("*********************** STARTING SCRIPT ***********************")
logger.info("====================== COMMITTING CHANGES: {}".format(is_commit))

up_till = datetime.datetime(2016, 01, 25, 15, 00, 00)
urms = account_manager_models.UserRelationshipManager.objects.filter(user__created_on__lte=up_till)
total_urms = urms.count()
logger.info("================ TOTAL USERS TO SEND MAIL: {}".format(total_urms))

counter = 1
for urm in urms.iterator():
    user, rm = urm.user, urm.rm
    if is_commit:
        user_name = utils.clean_name(user.get_full_name())
        data = {'user': user, 'rm': rm, 'name': user_name}
        cf_cns.notify('RM_CSU_TRANSITION', to=(user_name, user.email),
                      obj=data, mandrill_template='newbase')

    logger.info("({} of {}) Mail sent to =>  {} [COMMIT={}]".format(counter, total_urms, user.email, is_commit))
    counter += 1
