import os
from glob import glob

from cfblog.models import Content
from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse

from brands.models import BrandPage
from motor_product.models import Garage
from seocms.models import Vehicle
from wiki.models import Hospital, WikiArticle


class BaseSitemap(Sitemap):
    changefreq = 'daily'

    def items(self):
        return self.model.objects.all()


class StaticSitemap(BaseSitemap):
    def items(self):
        return ['post_index']

    def location(self, obj):
        return reverse(obj)


class WikiSitemap(BaseSitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return WikiArticle.objects.filter(status=2)

    def lastmod(self, obj):
        return obj.modified


class FlatpagesSitemap(BaseSitemap):
    priority = 0.8

    def location(self, url):
        return url

    def items(self):
        excluded_url = ['home-insurance.html','irda-license.html', 'shipping-and-delivering.html', 'terms-and-conditions.html',
                        'privacy-policy.html', 'cancellation-and-refund-policy.html']
        path = "flatpages/templates/flatpages/"
        return filter(lambda u: not u.endswith('_/'), map(lambda t: '/' + t.split(path)[1].split('.html')[0] + '/',
                                                          [y for x in os.walk(path) for y in
                                                           glob(os.path.join(x[0], '*.html')) if
                                                           y not in list(map((lambda p: path + p), excluded_url))]))


class BrandPageSitemap(BaseSitemap):
    model = BrandPage
    priority = 0.7


class HospitalInsurersSitemap(BaseSitemap):
    model = Hospital.insurers.through

    def items(self):
        popular_city = ["delhi", "bangalore", "mumbai", "chennai", "kolkata", "hyderabad", "ernakulam", "pune"]
        self.custom_slugs = dict(BrandPage.objects.filter(
            type='health', _insurer__isnull=False).values_list('_insurer__slug', 'slug'))
        return super(self.__class__, self).items().filter(insurer__brandpage__isnull=False,
                                                          hospital__city__slug__in=popular_city).values_list(
            'insurer__slug', 'hospital__city__slug', 'hospital__state__slug').distinct()

    def location(self, args):
        args = list(args)
        if args[0] in self.custom_slugs:
            args[0] = self.custom_slugs[args[0]]
        return reverse('insurer_hospital_list', args=args)


class GarageInsurersSitemap(BaseSitemap):
    model = Garage.insurers.through

    def items(self):
        self.custom_slugs = dict(BrandPage.objects.filter(
            type='motor', _motor_insurer__isnull=False).values_list('_motor_insurer__slug', 'slug'))
        return super(GarageInsurersSitemap, self).items().filter(
            insurer__brandpage__isnull=False).values_list('insurer__slug',
                                                          'garage__city__slug',
                                                          'garage__city__state__slug').distinct()

    def location(self, args):
        args = list(args)
        if args[0] in self.custom_slugs:
            args[0] = self.custom_slugs[args[0]]
        return reverse('insurer_garage_list', args=args)


class SeoCmsSitemap(BaseSitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return Vehicle.objects.all()

    def location(self, item):
        val = {
            'brand': item.brand,
            'modelName': item.modelName
        }
        vertical = item.page.category.get_root().slug
        if vertical == 'car-insurance':
            return reverse('car_model', kwargs=val)
        else:
            return reverse('two_wheeler_model', kwargs=val)


class HomeSitemap(BaseSitemap):
    changefreq = "weekly"
    priority = 0.8

    def items(self):
        return ['index']

    def location(self, item):
        return reverse(item)


class CfblogSitemap(BaseSitemap):
    changefreq = "weekly"
    priority = 0.9

    def items(self):
        return Content.objects.published()
