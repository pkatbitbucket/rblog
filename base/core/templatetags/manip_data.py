from django import template
from django.template.defaultfilters import stringfilter
from django.contrib.contenttypes.models import ContentType
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.db.models import Q
from django.utils.encoding import force_unicode, iri_to_uri

import json
import datetime
import time
import os
import re
import collections

from core.models import *
from wiki.models import *
import putils
from utils import user_has_any_group
from health_product import prod_utils

register = template.Library()


@register.filter
def get_agent_name(value):
    agent_dict = {
        'jack': 'rajiv',
        'shweta': 'shweta',
        'ramitha': 'ramitha',
        'kunal': 'kunal',
        'amit': 'amit'
    }
    return agent_dict.get(value.lower(), 'shweta')


@register.filter
def getdisplay(value):
    CHOICE_TUP = (
        ('S-1', 'single man'),
        ('S-2', 'single woman'),
        ('M-1', 'married man'),
        ('M-2', 'married woman'),
        ('NONE', 'no kids'),
        ('NO_DEPENDENT', 'no dependent children'),
        ('ONE_DEPENDENT', 'one dependent child'),
        ('TWO_OR_MORE', 'two or more dependent children'),
        ('FINANCIALLY_INDEPENDENT', 'financially independent'),
        ('FINANCIALLY_DEPENDENT', 'financially dependent'),
        ('NO_PARENTS', 'no more'),
        ('M-MUMBAI', 'Mumbai'),
        ('M-DELHI', 'Delhi'),
        ('M-KOLKATA', 'Kolkata'),
        ('M-CHENNAI', 'Chennai'),
        ('M-BANGALORE', 'Bangalore'),
        ('M-PUNE', 'Pune'),
        ('M-HYDERABAD', 'Hyderabad'),
        ('N-OTHER', 'a non-metro city'),
        ('0', 'nil'),
        ('10', 'less than 10 lakhs'),
        ('15', 'between 10 to 25 lakhs'),
        ('35', 'between 26 to 50 lakhs'),
        ('50', 'more than 50 lakhs'),
        ('0_0-0_0', 'nil'),
        ('0_0-2_5', 'less than 2.5 lakhs'),
        ('2_5-7_5', 'between 2.5 to 7.5 lakhs'),
        ('7_5-12_0', 'between 7.6 to 12 lakhs'),
        ('12_0-20_0', 'between 12.1 to 20 lakhs'),
        ('20_0-40_0', 'more than 20 lakhs'),
    )
    stat_dict = dict(CHOICE_TUP)
    return stat_dict.get(value, '')


@register.filter
def ingroups(user, group_list):
    groups = group_list.split(",")
    if user.is_superuser:
        return True
    else:
        user_groups = user.groups.values_list('name', flat=True)
        for r in groups:
            if r in user_groups:
                return True
    return False


@register.filter
def has_any_group(user, group_names):
    return user_has_any_group(user, group_names.split(","))


@register.filter
def inroles(user, role_list):
    return user_has_any_group(user, role_list.split(","))


@register.filter
def get_dict(val, attr):
    return val.get(attr, '')


@register.filter
def get_meta_data(slab, model):
    content_type = ContentType.objects.get(model__iexact=model)
    mdata = putils.get_meta([slab], content_type)
    return mdata


@register.filter
def get_product_meta_data(product, model):
    content_type = ContentType.objects.get(model__iexact=model)
    pmds = ProductMetaData.objects.filter(
        content_type=content_type, health_product=product)
    if pmds:
        pmd = pmds[0]
    else:
        pmd = None
    return pmd


@register.filter
def get_attr(val, attr):
    if val:
        return getattr(val, attr, None)
    else:
        return None


@register.filter
@stringfilter
def remove(value, arg):
    return value.replace(arg, "")


@register.filter
@stringfilter
def labelize(value):
    return value.replace("_", " ").title()


@register.filter
@stringfilter
def paginator(value, page):
    page = str(page)
    if not re.search(r'/page/\d+/', value):
        return "%spage/%s/" % (value, page)
    else:
        return re.sub(r'/page/\d+/', '/page/%s/' % page, value)


@register.filter
@stringfilter
def get_photo(value, arg):
    """TODO: Does Something """
    v = os.path.splitext(value)
    newname = "%s%s%s" % (v[0], arg, v[1])
    return newname


@register.filter
@stringfilter
def split_line(value, arg):
    """ Returns the first arg elements of the string """
    return value.split(arg)


@register.filter
def split_list(value, arg):
    """ Returns the first arg elements of the string """
    return value[:arg]


@register.filter
@stringfilter
def split_str(value, arg):
    """ Returns the first arg elements of the string """
    return value[:arg]


@register.filter
def maketable(element):
    """
    Makes Table given a list of lists of data. The data is html escaped.
    table data can be of any form other than [{}, abc]/({}, abc)

    Each tr can be given custom attributes by appending a dictionary of params before it.
    Each td can be given custom attributes by passing a list/tuple of the form [params, value] where params is a dict like {"colspan":2}
    egs.
    1. Without any styling
    li = [[1, "yo"], ["gracious", "priest"]]
    {{li|maketable}} -> <tr> <td> 1 </td> <td> yo </td> </tr> <tr> <td> gracious </td> <td> priest </td> </tr>

    2. With styling to a row
    li = [[{"class":"row"}, ["myname", 2], 23.5]]
    {{li|maketable}} -> <tr class="row"> <td> ['myname', 2] </td> <td> 23.5 </td> </tr>

    3. With styling to a td element and a row.
    li = [[{"style":"clear:both;"}, [{"colspan":2}, "Username"]]
    {{li|maketable}} -> <tr style="clear:both;"> <td colspan="2"> Username </td> </tr>

    """

    html = u""
    print element
    for row in element:
        html += u"""
<tr """
        if type(row[0]) == type({}):
            for k, v in row[0].items():
                html += u'%s="%s" ' % (k, v)
            html += u'>'
            row = row[1:]
        else:
            html += u'>'

        for ele in row:

            tdvalue = ele
            if type(ele) in [type([]), tuple]:
                try:
                    # If you error here, it means you have packed too many
                    # elements in the list / tuple
                    params, val = ele
                    tdvalue = val
                except ValueError:
                    params = {}

                html += """
    <td """
                try:
                    for k, v in params.items():
                        html += u'%s="%s" ' % (k, v)
                except AttributeError:
                    tdvalue = ele

                html += u'>'

            else:
                html += """
    <td> """
            html += escape(str(tdvalue))
            html += " </td>"

        html += """
</tr> """

    return mark_safe(html)


@register.filter
@stringfilter
def commator(value, price=""):
    """ adds commas to a number in indian 1234556.234|commator -> Rs. 12,34,556.234
    123456|commator:auto -> Rs. 1.23 lac.
    """
    li = value.split(".")
    value = li[0]
    try:
        rest = li[1]
    except IndexError:
        rest = "00"

    strval = value[::-1]
    strret = ''

    if strval:
        strnew = strval[0]
        strval = strval[1:]

        for i in range(len(strval)):
            if i % 2 == 1:
                strnew += strval[i] + ","
            else:
                strnew += strval[i]
        strret = strnew[::-1]
    if rest:
        strret += ".%s" % rest

    pricedi = collections.OrderedDict([("none", ""), ("thousand", "K."), ("lakh", "lac."), ("crore", "cr."), (
        "arab", "ar."), ("kharab", "khr."), ("neel", "nl."), ("padma", "pd."), ("shankh", "shk.")])

    suffix = ""
    if price == "auto":
        li = strret.split(",")
        suffix = pricedi.values()[len(li) - 1]
        strret = li[0]
        if len(li) > 1:
            strret += "." + li[1][:2]
    elif price in pricedi.keys():
        # TODO: Yet to implement
        li = strret.split(",")
        iter = -1
        for k, v in pricedi:
            iter += 1
            if k == price:
                val = v
                break
        if iter > 0:
            strret = "".join(li[:0 - iter]) + "." + li[0 - iter][:2]
    else:
        pass
    #strret = u"\u20B9 "+strret+" %s" %suffix
    strret += " %s" % suffix
    if strret[0] == ',':
        strret = strret[1:]
    return strret


@register.filter
@stringfilter
def int_commator(value, price=""):
    """ adds commas to a number in indian 1234556.234|commator -> Rs. 12,34,556.234
    123456|commator:auto -> Rs. 1.23 lac.
    """
    li = value.split(".")
    value = li[0]
    try:
        rest = li[1]
    except IndexError:
        rest = "00"

    strval = value[::-1]

    if strval:
        strnew = strval[0]
        strval = strval[1:]

    for i in range(len(strval)):
        if i % 2 == 1:
            strnew += strval[i] + ","
        else:
            strnew += strval[i]
    strret = strnew[::-1]

    pricedi = collections.OrderedDict([("none", ""), ("thousand", "K."), ("lakh", "lac."), ("crore", "cr."), (
        "arab", "ar."), ("kharab", "khr."), ("neel", "nl."), ("padma", "pd."), ("shankh", "shk.")])

    suffix = ""
    if price == "auto":
        li = strret.split(",")
        suffix = pricedi.values()[len(li) - 1]
        strret = li[0]
        if len(li) > 1:
            strret += "." + li[1][:2]
    elif price in pricedi.keys():
        # TODO: Yet to implement
        li = strret.split(",")
        iter = -1
        for k, v in pricedi:
            iter += 1
            if k == price:
                val = v
                break
        if iter > 0:
            strret = "".join(li[:0 - iter]) + "." + li[0 - iter][:2]
    else:
        pass
    strret = 'INR ' + strret + " %s" % suffix
    return strret


@register.filter
@stringfilter
def jsondict(value):
    if not value:
        return ""
    else:
        return json.loads(value).items()


@register.filter
@stringfilter
def jsonize(value):
    if not value:
        return ""
    else:
        return json.loads(value)


@register.filter
def lookup(value, arg):
    return value.get(arg)


@register.filter
def make_dict(value):
    return value.split(",")


@register.filter
@stringfilter
def contains(value, arg):
    if value.find(arg) < 0:
        return False
    else:
        return True


@register.filter
def get_age(date):
    dt = datetime.datetime.now()
    delta = dt - date
    return str(delta.days)


@register.filter
def render_question(value):
    return prod_utils.render_question(value)


@register.filter
def get_tag_search_url(value):
    """
    Takes a tag (jargon term) and return its relevant
     jargon search url

    Template Syntax::

        {{ tag|get_tag_search_url }}

    """
    tag_link = "/articles/jargon_search/?jq=%s" % (
        value.lower().strip().replace(' ', '+'))
    return tag_link


@register.filter
def get_range(value):
    return range(value)


@register.filter
def item_by_index(value, index):
    try:
        return value[index]
    except (KeyError, IndexError):
        return ''


@register.filter
def addstr(value1, value2):
    return str(value1) + str(value2)


@register.filter
def divide(value, arg):
    if value:
        return int(float(value) / float(arg))
    else:
        return 0
