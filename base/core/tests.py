from django.test import SimpleTestCase

import string
import random

from core import models as core_models
from core import activity as core_activity
from account_manager import utils
ACTIVITY_LOG = ''


def log_activity(log_msg):
    global ACTIVITY_LOG
    ACTIVITY_LOG = log_msg


class RedshiftLogTest(SimpleTestCase):
    def setUp(self):
        self.temp_log_file_path = '/tmp/redshift_act.log'
        self.user = core_models.User.objects.create(
            first_name='Hitul'
        )

    def test_log_success(self):
        import json
        request_dict = {
            'ip_address': '127.0.0.1'
        }
        old_act_func = core_activity.log_activity
        core_activity.log_activity = log_activity
        core_activity.create_activity(
            'viewed', 'health', 'quote', '',
            '', '', data1="",
            data2="", data={}, request_dict=request_dict,
            user=self.user, extra=None,
        )
        core_activity.log_activity = old_act_func
        self.assertNotEqual(
            ACTIVITY_LOG, None
        )

        splitted = ACTIVITY_LOG.split('^^^')
        del splitted[0]
        self.assertEqual(
            splitted[0], 'viewed'
        )
        self.assertEqual(
            splitted[1], 'health'
        )
        self.assertEqual(
            splitted[2], 'quote'
        )

        json_data = json.loads(splitted[-1])
        self.assertEqual(
            json_data['product'], 'health'
        )
        self.assertEqual(
            json_data['user_id'], self.user.pk
        )
        self.assertEqual(
            json_data['page_id'], 'quote'
        )


class PutilsTestCase(SimpleTestCase):

    def setUp(self):
        # Send get.
        core_models.Email.objects.all().delete()
        core_models.Phone.objects.all().delete()
        core_models.Tracker.objects.filter(session_key__startswith='lead_').delete()
        core_models.User.objects.filter(
            _customer_id__startswith='lead_'
        ).delete()

    def test_user_visitor_fd_create_activity(self):
        data = {
            'core.forms.Person': {
                'first_name': 'Hitul',
                'email': 'hitul@cfox.com'
            }
        }
        user = core_models.User.objects.create(
            first_name='Hitul'
        )

        act = core_activity.create_activity(
            core_activity.SUCCESS, 'health', 'quotes', '',
            '', '', data1="-",
            data2="-", data=data, request_dict={
                'user': user,
                'ip_address': '10.10.10.10'
            },
            user=user, visitor=None, extra=None
        )

        self.assertEqual(
            act.user,
            user
        )

        self.assertEqual(
            act.user.fd.get_data(),
            act.fd.get_data()
        )

        # Update data and then test.
        data['core.forms.Person']['last_name'] = "Mistry"
        act = core_activity.create_activity(
            core_activity.SUCCESS, 'health', 'lp_home', '',
            '', '', data1="-",
            data2="-", data=data, request_dict={
                'user': user,
                'ip_address': '10.10.10.10'
            },
            user=user, visitor=None, extra=None
        )

        self.assertEqual(
            act.user.fd.get_data(),
            data
        )

        self.assertEqual(
            core_models.Requirement.objects.filter(activity=act).count(),
            1
        )

        self.assertEqual(
            act.fd.get_data(),
            data
        )


class TestGetOrCreateuser(SimpleTestCase):
    def setUp(self):
        # Create users.
        core_models.Email.objects.all().delete()
        core_models.Phone.objects.all().delete()
        core_models.Tracker.objects.filter(session_key__in=[
            "abc", "lead_hitul.xyz10@cc.com", "lead_11",
            "lead_12", "xyz12@cfox.com"
        ]).delete()

    def random_key(self):
        return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))

    def test_case1(self):
        """
        Case 1:
        Email and Phone both are verified and same users.
        """
        email = core_models.Email.objects.create(
            email="email1@coverfox.com",
            is_verified=True,
            is_exists=True,
        )
        phone = core_models.Phone.objects.create(
            number="1",
            is_exists=True,
            is_verified=True,
        )
        user = core_models.User.objects.create()

        email.user = user
        email.save()

        phone.user = user
        phone.save()

        result_user = utils.get_or_create_user(email.email, phone.number, '')

        email.refresh_from_db()
        phone.refresh_from_db()

        self.assertEqual(
            result_user.pk,
            user.pk
        )

    def test_case2(self):
        """
        Case 2:
        Email and Phone both are verified and different users.
        """
        email = core_models.Email.objects.create(
            email="email1@coverfox.com",
            is_verified=True,
            is_exists=True
        )
        phone = core_models.Phone.objects.create(
            number="1",
            is_exists=True,
            is_verified=True
        )
        user1 = core_models.User.objects.create()
        user2 = core_models.User.objects.create()

        email.user = user1
        email.save()

        phone.user = user2
        phone.save()

        result_user = utils.get_or_create_user(email.email, phone.number, '')

        email.refresh_from_db()
        phone.refresh_from_db()

        self.assertEqual(
            result_user.pk,
            user2.pk
        )

    def test_case3(self):
        """
        Case 3:
        Email verified and phone not verified.
        :return:
        """
        email = core_models.Email.objects.create(
            email="email3@coverfox.com",
            is_verified=True,
            is_exists=True
        )
        phone = core_models.Phone.objects.create(
            number="3",
            is_exists=True,
            is_verified=False
        )
        user1 = core_models.User.objects.create()
        user2 = core_models.User.objects.create()

        email.user = user1
        email.save()

        phone.user = user2
        phone.save()

        result_user = utils.get_or_create_user(email.email, phone.number, '')

        email.refresh_from_db()
        phone.refresh_from_db()
        self.assertEqual(
            result_user.pk,
            user1.pk
        )
        self.assertEqual(
            result_user.emails.all().first().is_primary,
            True
        )
        self.assertEqual(
            result_user.phones.all().first().is_primary,
            True
        )
        self.assertEqual(
            result_user.phones.all().first().visitor,
            None
        )

    def test_case4(self):
        """
        Case 3:

        :return:
        """
        visitor = core_models.Tracker.objects.create(
            session_key="abc"
        )
        email = core_models.Email.objects.create(
            email="email4@coverfox.com",
            is_verified=False,
            is_exists=True,
            visitor=visitor
        )
        phone = core_models.Phone.objects.create(
            number="4",
            is_exists=True,
            is_verified=True,
            visitor=visitor
        )
        user1 = core_models.User.objects.create()
        user2 = core_models.User.objects.create()

        email.user = user1
        email.save()

        phone.user = user2
        phone.save()

        result_user = utils.get_or_create_user(email.email, phone.number, '')

        email.refresh_from_db()
        phone.refresh_from_db()
        self.assertEqual(
            result_user.pk,
            user2.pk
        )

        self.assertEqual(
            result_user.emails.all().first().is_primary,
            True
        )
        self.assertEqual(
            result_user.phones.all().first().is_primary,
            True
        )
        self.assertEqual(
            result_user.emails.all().first().visitor,
            None
        )

    def test_case5(self):
        """
        Case 5:
        Both verified False.
        :return:
        """
        email = core_models.Email.objects.create(
            email="email5@coverfox.com",
            is_verified=False,
            is_exists=True
        )
        phone = core_models.Phone.objects.create(
            number="5",
            is_exists=True,
            is_verified=False
        )
        user1 = core_models.User.objects.create()
        user2 = core_models.User.objects.create()

        email.user = user1
        email.save()

        phone.user = user2
        phone.save()

        result_user = utils.get_or_create_user(email.email, phone.number, '')

        email.refresh_from_db()
        phone.refresh_from_db()
        self.assertEqual(
            result_user.pk,
            core_models.User.objects.filter(emails__email=email.email, emails__is_verified=True).first().pk
        )

        self.assertNotEqual(
            result_user.pk,
            user1.pk
        )

        self.assertNotEqual(
            result_user.pk,
            user2.pk
        )

    def test_only_phone_case6(self):
        """
        """
        phone = '6'
        core_models.User.objects.create()

        result_user = utils.get_or_create_user('', "6", '')
        self.assertEqual(
            result_user.phones.all().first().number,
            phone
        )
        self.assertEqual(
            result_user.phones.all().first().is_primary,
            True
        )

    def test_only_phone_case7(self):
        """
        """
        phone = '7'
        user = core_models.User.objects.create()

        # Already verified phone exist.
        core_models.Phone.objects.create(
            number=phone,
            is_verified=True,
            user=user
        )

        result_user = utils.get_or_create_user('', phone, '')
        self.assertEqual(
            result_user.phones.all().count(),
            1
        )

        self.assertEqual(
            result_user.phones.all().first().user.pk,
            user.pk
        )

    def test_only_email_case8(self):
        """
        """
        email = 'hitul.xyz8@cc.com'
        core_models.User.objects.create()

        result_user = utils.get_or_create_user(email, "", '')
        self.assertEqual(
            result_user.emails.all().first().email,
            email
        )
        self.assertEqual(
            result_user.emails.all().first().is_primary,
            True
        )

    def test_mapping9(self):
        """
        """
        email = 'hitul.xyz9@cc.com'
        core_models.User.objects.create()

        result_user = utils.get_or_create_user(None, None, mapping={'email': email})
        self.assertEqual(
            result_user.emails.all().first().email,
            email
        )

    def test_visitor_email_attach10(self):
        email = 'hitul.xyz10@cc.com'
        visitor = core_models.Tracker.objects.create(
            session_key="lead_%s" % email,
        )

        result_user = utils.get_or_create_user(None, None, mapping={'email': email})
        self.assertEqual(
            result_user.emails.all().first().email,
            email
        )

        visitor.refresh_from_db()
        self.assertEqual(
            visitor.user,
            result_user
        )

    def test_visitor_phone_attach11(self):
        mobile = "11"
        visitor = core_models.Tracker.objects.create(
            session_key="lead_%s" % mobile,
        )

        result_user = utils.get_or_create_user(None, None, mapping={'mobile': mobile})
        self.assertEqual(
            result_user.phones.all().first().number,
            mobile
        )

        visitor.refresh_from_db()
        self.assertEqual(
            visitor.user,
            result_user
        )

    def test_visitor_email_phone_attach12(self):
        email = "xyz12@cfox.com"
        mobile = "12"

        visitor = core_models.Tracker.objects.create(
            session_key="lead_%s" % mobile,
        )

        result_user = utils.get_or_create_user(email, mobile, mapping={})
        self.assertEqual(
            result_user.phones.all().first().number,
            mobile
        )
        self.assertEqual(
            result_user.emails.all().first().email,
            email
        )

        visitor.refresh_from_db()
        self.assertEqual(
            visitor.user,
            result_user
        )

    # -----------------------------------------------------------------
    # Attach email test cases
    # -----------------------------------------------------------------
    def test_email_attach_function_case1(self):
        """
        Attach function add verified true email and no verified
        user exists.
        :return:
        """
        email = "email.test1@xyz.com"
        user = core_models.User.objects.create(
            first_name="Hitul"
        )

        user.attach_email(email, is_verified=True)
        self.assertEqual(
            user.emails.filter(email=email, is_verified=True).count(),
            1
        )
        self.assertEqual(
            user.emails.filter(email=email, is_primary=True).count(),
            1
        )

    def test_email_attach_function_case2(self):
        """
        Attach function add verified False email and verified
        user exists.
        :return:
        """
        email = "email.test2@xyz.com"
        user = core_models.User.objects.create(
            first_name="Hitul"
        )
        user.attach_email(email, is_verified=False)
        self.assertEqual(
            user.emails.filter(email=email, is_verified=False).count(),
            1
        )

    def test_email_attach_function_case3(self):
        """
        Attach function add verified True and already other users has
        verified true email exists.
        :return:
        """
        email = "email.test3@xyz.com"

        user1 = core_models.User.objects.create(
            first_name="Hitul"
        )
        user2 = core_models.User.objects.create(
            first_name="Hitul"
        )

        email2_instance = core_models.Email.objects.create(
            email=email,
            is_verified=True,
            user=user2
        )

        user1.attach_email(email, is_verified=True)
        email2_instance.refresh_from_db()
        self.assertEqual(
            user1.emails.filter(email=email, is_verified=True).count(),
            1
        )
        self.assertEqual(
            email2_instance.is_verified,
            False
        )

    def test_email_attach_function_case4(self):
        """
        Attach function add verified true and others
        already has verified false.
        :return:
        """
        email = "email.test4@xyz.com"

        user1 = core_models.User.objects.create(
            first_name="Hitul"
        )
        user2 = core_models.User.objects.create(
            first_name="Hitul"
        )

        core_models.Email.objects.create(
            email=email,
            is_verified=False,
            user=user2
        )

        email_instance = user1.attach_email(email, is_verified=True)
        self.assertEqual(
            email_instance.is_verified,
            True
        )

    def test_email_attach_function_case5(self):
        """
        Test auto verified case.
        :return:
        """
        email = "email.test5@xyz.com"
        user1 = core_models.User.objects.create(
            first_name="Hitul"
        )

        email_instance = user1.attach_email(
            email,
            is_verified="auto"
        )

        self.assertEqual(
            email_instance.is_verified,
            True
        )

    def test_email_attach_function_case6(self):
        """
        Test auto verified case.
        :return:
        """
        email = "email.test6@xyz.com"
        user1 = core_models.User.objects.create(
            first_name="Hitul"
        )
        user2 = core_models.User.objects.create(
            first_name="Hello"
        )

        user2.attach_email(email=email, is_verified=True)
        email_instance = user1.attach_email(
            email,
            is_verified="auto"
        )

        self.assertEqual(
            email_instance.is_verified,
            False
        )

    def test_email_attach_function_case7(self):
        """
        Test auto verified case.
        :return:
        """
        email = "email.test7@xyz.com"
        user1 = core_models.User.objects.create(
            first_name="Hitul"
        )
        user2 = core_models.User.objects.create(
            first_name="Hello"
        )

        user2.attach_email(email=email, is_verified=False)
        email_instance = user1.attach_email(
            email,
            is_verified="auto"
        )

        self.assertEqual(
            email_instance.is_verified,
            True
        )

    # -----------------------------------------------------------------
    # Attach phone test cases
    # -----------------------------------------------------------------
    def test_phone_attach_function_case1(self):
        """
        Attach function add verified true email and no verified
        user exists.
        :return:
        """
        phone = "1"
        user = core_models.User.objects.create(
            first_name="Hitul"
        )

        user.attach_phone(phone, is_verified=True)
        self.assertEqual(
            user.phones.filter(number=phone, is_verified=True).count(),
            1
        )
        self.assertEqual(
            user.phones.filter(number=phone, is_primary=True).count(),
            1
        )

    def test_phone_attach_function_case2(self):
        """
        Attach function add verified False email and verified
        user exists.
        :return:
        """
        phone = "2"
        user = core_models.User.objects.create(
            first_name="Hitul"
        )
        user.attach_phone(phone, is_verified=False)
        self.assertEqual(
            user.phones.filter(number=phone, is_verified=False).count(),
            1
        )

    def test_phone_attach_function_case3(self):
        """
        Attach function add verified True and already other users has
        verified true email exists.
        :return:
        """
        phone = "3"

        user1 = core_models.User.objects.create(
            first_name="Hitul"
        )
        user2 = core_models.User.objects.create(
            first_name="Hitul"
        )

        phone2_instance = core_models.Phone.objects.create(
            number=phone,
            is_verified=True,
            user=user2
        )

        user1.attach_phone(phone, is_verified=True)
        phone2_instance.refresh_from_db()
        self.assertEqual(
            user1.phones.filter(number=phone, is_verified=True).count(),
            1
        )
        self.assertEqual(
            phone2_instance.is_verified,
            False
        )

    def test_phone_attach_function_case4(self):
        """
        Attach function add verified true and others
        already has verified false.
        :return:
        """
        phone = "4"

        user1 = core_models.User.objects.create(
            first_name="Hitul"
        )
        user2 = core_models.User.objects.create(
            first_name="Hitul"
        )

        core_models.Phone.objects.create(
            number=phone,
            is_verified=False,
            user=user2
        )

        phone_instance = user1.attach_phone(phone, is_verified=True)
        self.assertEqual(
            phone_instance.is_verified,
            True
        )

    # -----------------------------------------------------------------
    # Attach email test cases
    # -----------------------------------------------------------------
    def test_email_visitor_attach_function_case1(self):
        """
        Attach function add verified true email and no verified
        user exists.
        :return:
        """
        email = "email.test1@xyz.com"
        visitor = core_models.Tracker.objects.create(
            session_key=self.random_key()
        )

        visitor.attach_email(email, is_verified=True)
        self.assertEqual(
            visitor.emails.filter(email=email, is_verified=True).count(),
            1
        )
        self.assertEqual(
            visitor.emails.filter(email=email, is_primary=True).count(),
            1
        )

    def test_email_visitor_attach_function_case2(self):
        """
        Attach function add verified False email and verified
        user exists.
        :return:
        """
        email = "email.test2@xyz.com"
        visitor = core_models.Tracker.objects.create(
            session_key=self.random_key()
        )
        visitor.attach_email(email, is_verified=False)
        self.assertEqual(
            visitor.emails.filter(email=email, is_verified=False).count(),
            1
        )

    def test_email_visitor_attach_function_case3(self):
        """
        Attach function add verified True and already other users has
        verified true email exists.
        :return:
        """
        email = "email.test3@xyz.com"

        visitor1 = core_models.Tracker.objects.create(
            session_key=self.random_key()
        )
        visitor2 = core_models.Tracker.objects.create(
            session_key=self.random_key()
        )

        email2_instance = core_models.Email.objects.create(
            email=email,
            is_verified=True,
            visitor=visitor2
        )

        visitor1.attach_email(email, is_verified=True)
        email2_instance.refresh_from_db()
        self.assertEqual(
            visitor1.emails.filter(email=email, is_verified=True).count(),
            1
        )
        self.assertEqual(
            email2_instance.is_verified,
            False
        )

    def test_email_visitor_attach_function_case4(self):
        """
        Attach function add verified true and others
        already has verified false.
        :return:
        """
        email = "email.test4@xyz.com"

        visitor1 = core_models.Tracker.objects.create(
            session_key=self.random_key()
        )
        core_models.Tracker.objects.create(
            session_key=self.random_key()
        )

        core_models.Email.objects.create(
            email=email,
            is_verified=False,
            visitor=visitor1
        )

        email_instance = visitor1.attach_email(email, is_verified=True)
        self.assertEqual(
            email_instance.is_verified,
            True
        )

    # -----------------------------------------------------------------
    # Attach phone test cases
    # -----------------------------------------------------------------
    def test_phone_visitor_attach_function_case1(self):
        """
        Attach function add verified true email and no verified
        user exists.
        :return:
        """
        phone = "1"
        user = core_models.Tracker.objects.create(
            session_key=self.random_key()
        )

        user.attach_phone(phone, is_verified=True)
        self.assertEqual(
            user.phones.filter(number=phone, is_verified=True).count(),
            1
        )
        self.assertEqual(
            user.phones.filter(number=phone, is_primary=True).count(),
            1
        )

    def test_phone_visitor_attach_function_case2(self):
        """
        Attach function add verified False email and verified
        user exists.
        :return:
        """
        phone = "2"
        user = core_models.Tracker.objects.create(
            session_key=self.random_key()
        )
        user.attach_phone(phone, is_verified=False)
        self.assertEqual(
            user.phones.filter(number=phone, is_verified=False).count(),
            1
        )

    def test_phone_visitor_attach_function_case3(self):
        """
        Attach function add verified True and already other users has
        verified true email exists.
        :return:
        """
        phone = "3"

        user1 = core_models.Tracker.objects.create(
            session_key=self.random_key()
        )
        user2 = core_models.Tracker.objects.create(
            session_key=self.random_key()
        )

        phone2_instance = core_models.Phone.objects.create(
            number=phone,
            is_verified=True,
            visitor=user2
        )

        user1.attach_phone(phone, is_verified=True)
        phone2_instance.refresh_from_db()
        self.assertEqual(
            user1.phones.filter(number=phone, is_verified=True).count(),
            1
        )
        self.assertEqual(
            phone2_instance.is_verified,
            False
        )

    def test_phone_visitor_attach_function_case4(self):
        """
        Attach function add verified true and others
        already has verified false.
        :return:
        """
        phone = "4"

        visitor1 = core_models.Tracker.objects.create(
            session_key=self.random_key()
        )
        visitor2 = core_models.Tracker.objects.create(
            session_key=self.random_key()
        )

        core_models.Phone.objects.create(
            number=phone,
            is_verified=False,
            visitor=visitor2
        )

        phone_instance = visitor1.attach_phone(phone, is_verified=True)
        self.assertEqual(
            phone_instance.is_verified,
            True
        )


class IsStrongTest(SimpleTestCase):
    def setUp(self):
        pass

    def test_activity_basic_test1(self):
        data = {
            'core.forms.Person': {
                'first_name': 'Hitul',
                'email': 'hitul@cfox.com'
            }
        }
        user = core_models.User.objects.create(
            first_name='Hitul'
        )

        act = core_activity.create_activity(
            core_activity.SUCCESS, 'health', 'quotes', '',
            '', '', data1="-",
            data2="-", data=data, request_dict={
                'user': user,
                'ip_address': '10.10.10.10'
            },
            user=user, visitor=None, extra=None
        )

        self.assertEqual(
            act.is_strong(),
            True
        )

    def test_activity_basic_test2(self):
        data = {
            'core.forms.Person': {
                'first_name': 'Hitul',
            }
        }
        user = core_models.User.objects.create(
            first_name='Hitul'
        )

        act = core_activity.create_activity(
            core_activity.SUCCESS, 'health', 'quotes', '',
            '', '', data1="-",
            data2="-", data=data, request_dict={
                'user': user,
                'ip_address': '10.10.10.10'
            },
            user=user, visitor=None, extra=None
        )

        self.assertEqual(
            act.is_strong(),
            True
        )

    def test_activity_basic_test3(self):
        data = {
            'core.forms.Person': {
                'first_name': 'Hitul',
                'mobile': '9891112244'
            }
        }
        user = core_models.User.objects.create(
            first_name='Hitul'
        )

        act = core_activity.create_activity(
            core_activity.SUCCESS, 'health', 'quotes', '',
            '', '', data1="-",
            data2="-", data=data, request_dict={
                'user': user,
                'ip_address': '10.10.10.10'
            },
            user=user, visitor=None, extra=None
        )

        self.assertEqual(
            act.is_strong(),
            True
        )

    def test_activity_basic_test4(self):
        data = {
            'core.forms.Person': {
                'first_name': 'Hitul',
                'mobile': '9891112244',
                'email': 'hitul.mistry@cfox44.com'
            }
        }
        user = core_models.User.objects.create(
            first_name='Hitul'
        )

        act = core_activity.create_activity(
            core_activity.SUCCESS, 'health', 'quotes', '',
            '', '', data1="-",
            data2="-", data=data, request_dict={
                'user': user,
                'ip_address': '10.10.10.10'
            },
            user=user, visitor=None, extra=None
        )

        self.assertEqual(
            act.is_strong(),
            True
        )

    def test_activity_basic_test5(self):
        data = {
            'core.forms.Person': [
                {
                    'first_name': 'Hitul',
                    'role': 'self'
                },
                {
                    'mobile': '12345',
                    'role': 'self'
                }
            ]
        }
        user = core_models.User.objects.create(
            first_name='Hitul'
        )

        act = core_activity.create_activity(
            core_activity.SUCCESS, 'health', 'quotes', '',
            '', '', data1="-",
            data2="-", data=data, request_dict={
                'user': user,
                'ip_address': '10.10.10.10'
            },
            user=user, visitor=None, extra=None
        )

        self.assertEqual(
            act.is_strong(),
            True
        )

    def test_activity_basic_test6(self):
        data = {
            'core.forms.Person': [
                {
                    'first_name': 'Hitul',
                    'role': 'self'
                },
                {
                    'email': 'hello@ddfdsf.com',
                    'role': 'self'
                }
            ]
        }
        user = core_models.User.objects.create(
            first_name='Hitul'
        )

        act = core_activity.create_activity(
            core_activity.SUCCESS, 'health', 'quotes', '',
            '', '', data1="-",
            data2="-", data=data, request_dict={
                'user': user,
                'ip_address': '10.10.10.10'
            },
            user=user, visitor=None, extra=None
        )

        self.assertEqual(
            act.is_strong(),
            True
        )


class RequirementEngineTests(SimpleTestCase):
    def setUp(self):
        pass

    def test_requirement_reopen1(self):
        """
        Use existing requirement insted of reopening.
        :return:
        """
        # Create some activity to create requirement.
        data = {
            'core.forms.Person': {
                'first_name': 'Hitul',
                'email': 'hitul@cfox.com'
            }
        }

        user1 = core_models.User.objects.create(
            first_name='Hitul'
        )

        act1 = core_activity.create_activity(
            core_activity.SUCCESS, 'health', 'lp_', '',
            '', '', data1="-",
            data2="-", data=data, request_dict={
                'user': user1,
                'ip_address': '10.10.10.10'
            },
            user=user1, visitor=None, extra=None

        )
        req_instance1 = act1.requirement

        core_models.RequirementStateHistory.objects.create(
            requirement=req_instance1,
            current_state=req_instance1.current_state,

        )
        # Create more requirements.
        req_instance3 = core_models.Requirement.objects.create(
            kind=req_instance1.kind,
            current_state=req_instance1.current_state,
            user=user1,
            fd=act1.requirement.fd,
        )
        req_instance3.current_state = req_instance3.kind.machine.states.filter(
            name="CLOSED_unresolved"
        ).first()
        req_instance3.save()

        act2 = core_activity.create_activity(
            core_activity.SUCCESS, 'health', 'proposal', '',
            '', '', data1="-",
            data2="-", data=data, request_dict={
                'user': user1,
                'ip_address': '10.10.10.10'
            },
            user=user1, visitor=None, extra=None
        )

        self.assertNotEqual(
            act2.requirement.current_state.name,
            "FRESH_ringing"
        )
        self.assertEqual(
            act2.requirement,
            req_instance1
        )

    def test_requirement_reopen2(self):
        # Create some activity to create requirement.
        data = {
            'core.forms.Person': {
                'first_name': 'Hitul',
                'email': 'hitul@cfox.com'
            }
        }

        user1 = core_models.User.objects.create(
            first_name='Hitul'
        )

        act1 = core_activity.create_activity(
            core_activity.SUCCESS, 'health', 'lp_', '',
            '', '', data1="-",
            data2="-", data=data, request_dict={
                'user': user1,
                'ip_address': '10.10.10.10'
            },
            user=user1, visitor=None, extra=None

        )
        req_instance1 = act1.requirement

        # Create more requirements.
        req_instance2 = core_models.Requirement.objects.create(
            kind=req_instance1.kind,
            user=user1,
            current_state=req_instance1.current_state,
            fd=act1.requirement.fd,
        )

        closed_state = req_instance1.kind.machine.states.filter(
            name="CLOSED_unresolved"
        ).first()

        core_models.RequirementStateHistory.objects.create(
            state=req_instance1.current_state,
            requirement=req_instance1,
            previous_state=closed_state
        )

        req_instance1.current_state = closed_state
        req_instance1.save()

        core_models.RequirementStateHistory.objects.create(
            state=closed_state,
            requirement=req_instance2,
            previous_state=req_instance2.current_state
        )
        req_instance2.current_state = closed_state
        req_instance2.save()

        act2 = core_activity.create_activity(
            core_activity.SUCCESS, 'health', 'proposal', '',
            '', '', data1="-",
            data2="-", data=data, request_dict={
                'user': user1,
                'ip_address': '10.10.10.10'
            },
            user=user1, visitor=None, extra=None
        )

        self.assertEqual(
            act2.requirement.current_state.name,
            "FRESH_new"
        )

        self.assertEqual(
            act2.requirement.pk,
            req_instance2.pk
        )

    def test_requirement_reopen3(self):
        """
        Use existing requirement insted of reopening.
        :return:
        """
        # Create some activity to create requirement.
        data = {
            'core.forms.Person': {
                'first_name': 'Hitul',
                'email': 'hitul@cfox.com'
            }
        }

        user1 = core_models.User.objects.create(
            first_name='Hitul'
        )

        # Create new requirement.
        act1 = core_activity.create_activity(
            core_activity.SUCCESS, 'health', 'lp_', '',
            '', '', data1="-",
            data2="-", data=data, request_dict={
                'user': user1,
                'ip_address': '10.10.10.10'
            },
            user=user1, visitor=None, extra=None

        )
        req_instance1 = act1.requirement

        # Close requirement.
        req_instance1.current_state = req_instance1.kind.machine.states.filter(
            name="CLOSED_resolved"
        ).first()
        req_instance1.save()

        # Create more requirements and it will be closed.
        req_instance3 = core_models.Requirement.objects.create(
            kind=req_instance1.kind,
            current_state=req_instance1.current_state,
            user=user1,
            fd=act1.requirement.fd,
        )

        act2 = core_activity.create_activity(
            core_activity.SUCCESS, 'health', 'lp_', '',
            '', '', data1="-",
            data2="-", data=data, request_dict={
                'user': user1,
                'ip_address': '10.10.10.10'
            },
            user=user1, visitor=None, extra=None

        )

        self.assertNotEqual(
            act2.requirement.current_state.name,
            "FRESH_ringing"
        )

        self.assertNotEqual(
            act2.requirement.pk,
            req_instance3.pk
        )

        # This count should be 3 to confirm it has 3 requirements.
        # 1. Added by created activity
        # 2. Custom added.
        # 3. Create activity
        self.assertNotEqual(
            core_models.Requirement.objects.filter(user=user1).count(),
            2
        )


class FixDuplicates(SimpleTestCase):
    def setUp(self):
        core_models.Email.objects.all().delete()
        core_models.Phone.objects.all().delete()

    def _create_user(self, email=None, phone=None):
        assert email or phone
        user = core_models.User.objects.create()
        if email:
            core_models.Email.objects.create(
                email=email, is_verified=True,
                user=user
            )

        if phone:
            core_models.Phone.objects.create(
                number=phone, is_verified=True,
                user=user
            )

        return user

    def _attach_email(self, user, email, **kwargs):
        core_models.Email.objects.create(
            email=email, user=user, **kwargs
        )

    def test_duplicate_emails1(self):
        email = 'hitul1@cfox.com'
        phone = '1111'
        user = self._create_user(email=email, phone=phone)
        self._attach_email(user, email, **{'is_primary': True})

        user.fix_duplicate_emails(email)
        self.assertEqual(
            user.emails.filter(email=email).count(),
            1
        )
        self.assertNotEqual(
            user.emails.all().first(),
            None
        )
        self.assertEqual(
            user.emails.all().first().is_primary,
            True
        )

    def test_duplicate_emails2(self):
        email = 'hitul2@cfox.com'
        phone = '1112'
        user = self._create_user(email=email, phone=phone)
        self._attach_email(user, email, **{'is_verified': False})
        self._attach_email(user, 'hitul133423@cfox.com')

        user.fix_duplicate_emails(email)
        self.assertEqual(
            user.emails.filter(email=email).count(),
            1
        )
        self.assertEqual(
            user.emails.filter(email=email).first().is_verified,
            True
        )
