from django.contrib.auth.tokens import PasswordResetTokenGenerator


class PasswordResetOTPGenerator(PasswordResetTokenGenerator):
    """
    Generates a 6 cha aplha-numeric otp for a user
    NOTE: can be used multiple times, will be invalid once user is logged in.
    """

    def make_token(self, user):
        """
        Returns a token that can be used once to do a password reset
        for the given user.
        """
        return self._make_token_with_timestamp(user, int(user.last_login.strftime('%s')))[-6:].upper()

    def check_token(self, user, token):
        return token == self.make_token(user)

default_otp_generator = PasswordResetOTPGenerator()
