from django.conf.urls import url

from cf_fhurl import fhurl

from . import forms, sitemap, views

sitemaps = {
    'custom_urls': sitemap.FlatpagesSitemap,
    'view': sitemap.StaticSitemap,
    'brand_pages': sitemap.BrandPageSitemap,
    'hospital_insurers': sitemap.HospitalInsurersSitemap,
    # 'garage_insurers': GarageInsurersSitemap,
    'seocms_urls': sitemap.SeoCmsSitemap,
    'home': sitemap.HomeSitemap,
    'cfblog': sitemap.CfblogSitemap,
}


urlpatterns = [
    url(r'^sitemap\.xml$', views.sitemap, {'sitemaps': sitemaps}),
    url(r'^page-list-admin/$', view=views.admin_page_list_view,
        name='page_list_admin', ),
    url(r'^browser-not-supported/$', views.browser_not_supported,
        name='browser_not_supported'),

    url(r'^wiki-dashboard/$',
        view=views.AdminWikiView.as_view(), name='wiki_dashboard'),
    url(r'^medical-condition-dashboard/$',
        view=views.AdminMedicalConditionListView.as_view(), name='medical_condition_dashboard'),

    url(r'^reports-dashboard/$', views.reports_dashboard,
        name="reports_dashboard"),
    url(r'^review-dashboard/$', views.review_dashboard,
        name="review_dashboard"),
    url(r'^mail-manage/add/$', views.mail_add, name="mail_add"),
    url(r'^mail-manage/edit/(?P<mail_id>[\d]+)/$',
        views.mail_add, name="mail_edit"),
    url(r'^ajax/get-token/$', views.get_token, name="get_token"),
    url(r'^user-manage/$', views.UserManageView.as_view(), name="user_manage"),
    url(r'^bad-wolf/$', views.bad_wolf, name='bad_wolf'),
    url(r'^careers/$', views.careers, name='careers'),
    url(r'^career/schedule-cat/$',
        views.schedule_cat, name='schedule_cat'),
    url(r'^login/$', views.coverfox_login, name='coverfox_login'),
    fhurl(r'^ajax/user-manage/$', forms.UserForm, name="ajax_user_add",
          template="core/ajax_user_manage.html", json=True),
    fhurl(r'^ajax/user-manage/(?P<uid>[\d]+)/$', forms.UserForm,
          name="ajax_user_edit", template="core/ajax_user_manage.html", json=True),
    fhurl(r'^ajax/product-user-registration/$', forms.ProductUserRegistration,
          name='ajax_product_user_registration', template=None, json=True),
    fhurl(r'^ajax/product-user-login/$', forms.ProductUserLogin,
          name='ajax_product_user_login', template=None, json=True),

    url(r'^facebook-registration/$', views.facebook_registration,
        name='facebook_registration'),
    url(r'^google-registration/$', views.google_registration,
        name='google_registration'),
    url(r'^forgot-password/$', views.forgot_password,
        name='forgot_password'),
    fhurl(r'^intiatiate-password-reset/$', forms.IntitatePasswordReset,
          name='initiate_password_reset', template=None, json=True),
    url(r'^user/(?P<token>[\w-]+)/set-password/$',
        views.reset_password, name='reset_password'),
    fhurl(r'^user/reset-password/$', forms.ResetPassword,
          name='user_reset_password', template=None, json=True),
    fhurl(r'^requirements/$', forms.RequirementForm,
          name='requirement_form', template=None, json=True),
    url(r'^user-debug-dashboard/$', views.user_debug_dashboard, name="user-debug-dashboard"),
]
