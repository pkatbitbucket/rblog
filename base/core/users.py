import uuid
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)
from django_extensions.db.fields import UUIDField
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models import permalink
from django.utils import timezone

from customdb.thumbs import ImageWithThumbsField
from jsonfield import JSONField

import logging
import re
import os
import random
import datetime
import hashlib
from utils import log_error

logger = logging.getLogger(__name__)


Djson = None
try:
    from cfutils.knockout import djson
    Djson = djson.Djson
except ImportError:
    Djson = object


def get_mugshot_path(instance, filename):
    now = datetime.datetime.now()
    fname, ext = os.path.splitext(filename)
    try:
        import utils
    except ImportError:
        from core import utils

    new_fname = utils.slugify(fname)
    newfilename = "%s-%s.%s" % (new_fname, now.strftime("%I%M%S"), ext)
    path_to_save = "uploads/profile/images/%s" % (newfilename)
    return path_to_save


class UserManager(BaseUserManager):
    def get_user(
        self, email, phone
    ):
        """
        Handle below exceptions when using this function.
        1. User.DoesNotExist
        """
        from core.models import Email, Phone
        # Get user instance.
        phone_instance = None
        if phone:
            try:
                phone_instance = Phone.objects.get(number=phone, is_verified=True)
            except Phone.DoesNotExist:
                raise User.DoesNotExist
            except Phone.MultipleObjectsReturned:
                raise User.MultipleObjectsReturned

        email_instance = None
        if not phone_instance and email:
            try:
                email_instance = Email.objects.get(email=email, is_verified=True)
            except Email.DoesNotExist:
                raise User.DoesNotExist
            except Email.MultipleObjectsReturned:
                raise User.MultipleObjectsReturned

        if not phone_instance and not email_instance:
            raise User.DoesNotExist

        phone_instance_user = None
        if phone_instance:
            phone_instance_user = phone_instance.user

        email_instance_user = None
        if email_instance:
            email_instance_user = email_instance.user

        user = phone_instance_user or email_instance_user
        if not user:
            raise User.DoesNotExist

        return user

    def create_user(
        self, email=None, password=None, first_name="", is_verified=True, is_primary=True, **kwargs
    ):
        from core.models import Email
        """
        Creates and saves a User with the given email
        """
        user = self.model(
            first_name=first_name,
            **kwargs
        )

        if not password:
            password = hashlib.md5(
                "%s-%s" % (
                    datetime.datetime.now().strftime("%Y%m%d"),
                    str(random.random()))
            ).hexdigest()

        user.set_password(password)
        user.save(using=self._db)
        email = UserManager.normalize_email(email)
        # Check if primary email already exist or not.

        Email.objects.create(email=email, user=user, is_primary=is_primary, is_exists=True, is_verified=is_verified)
        return user

    def create_superuser(
        self, email, password, first_name, created_by=None
    ):
        """Creates and saves a superuser with the given email and password."""
        user = self.create_user(
            email=UserManager.normalize_email(email),
            password=password,
            first_name=first_name,
            created_by=created_by,
            is_verified=True
        )
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    """Custom User Model"""

    GENDER_CHOICES = (
        (1, 'Male'),
        (2, 'Female'),
    )
    ORGANIZATION_CHOICES = (
        ("", ""),
        ("COVERFOX", 'COVERFOX'),
        ("LIFENET", 'LIFENET'),
        ("ENSER", 'ENSER'),
        ("CARDEKHO", 'CARDEKHO'),
        ("GIRNARSOFT", 'GIRNARSOFT'),
    )
    USERNAME_FIELD = 'id'
    REQUIRED_FIELDS = ['first_name']

    token = UUIDField()
    # token_updated_on = models.DateTimeField(auto_now=True)
    token_updated_on = models.DateTimeField(auto_now=True)
    # If not in use, fill with md5 of email
    slug = models.SlugField(_('slug'), max_length=70)
    first_name = models.CharField(_('first name'), max_length=50, blank=True, db_index=True)
    middle_name = models.CharField(_('middle name'), max_length=50, blank=True, db_index=True)
    last_name = models.CharField(_('last name'), max_length=50, blank=True, db_index=True)
    birth_date = models.DateField(_('birth date'), null=True, blank=True)

    is_active = models.BooleanField(_('active'), default=True)
    is_staff = models.BooleanField(_('staff status'), default=False)
    is_verified = models.BooleanField(default=False)
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    code = models.CharField(_('code'), max_length=100,
                            db_index=True, blank=True)
    organization = models.CharField(
        _('organization'), max_length=100, choices=ORGANIZATION_CHOICES)
    gender = models.PositiveSmallIntegerField(
        _('gender'), choices=GENDER_CHOICES, blank=True, null=True)
    mugshot = ImageWithThumbsField(
        _('mugshot'),
        upload_to=get_mugshot_path,
        sizes=(
            ('s', 100, 100), ('t', 300, 300)
        ),
        null=True,
        blank=True
    )
    fd = models.ForeignKey('core.FormData', null=True, blank=True, related_name="user_fd")
    _customer_id = models.CharField(_('customer id'), max_length=32,
                                    null=True, editable=False, unique=True)
    extra = JSONField(_('extra'), blank=True, default={})  # For all user data
    created_on = models.DateTimeField(_('created'), auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    last_modified_on = models.DateTimeField(auto_now=True, null=True)
    last_modified_by = models.ForeignKey(
        'User', null=True, related_name="users_last_modified"
    )
    created_by = models.ForeignKey(
        'User', null=True, related_name="users_created"
    )

    objects = UserManager()

    @property
    def customer_id(self):
        if not self._customer_id:
            tracker = self.tracker_set.last()
            if tracker:
                self._customer_id = tracker.session_key
            else:
                self._customer_id = uuid.uuid4().hex
            self.save(update_fields=['_customer_id'])
        return self._customer_id

    @customer_id.setter
    def customer_id(self, value):
        from crm import drip_utils
        if self._customer_id:
            drip_utils.merge_users.delay(self.customer_id, value)
        else:
            self._customer_id = value

    def has_perm(self, perm, obj=None, company=None):
        from core.models import Employee

        if super(User, self).has_perm(perm, obj):
            return True

        # TODO: this should travese over the whole company heirarchy as the user
        # may not be employee of child companies but may have perms through a
        # parent company.

        if not company:
            return False

        try:
            emp = Employee.objects.get(company=company, user=self)
        except Employee.DoesNotExist:
            return False
        else:
            return emp.has_perm(perm, obj)

        return False

    def __unicode__(self):
        return u"%s" % self.get_full_name()

    def is_customer(self):
        if self.advisor.exists():
            return False
        else:
            return True

    def get_group_names(self):
        return [g.name for g in self.groups.all()]

    def get_full_name(self):
        return re.sub(
            r' +', ' ', " ".join(
                [self.first_name, self.middle_name, self.last_name]
            )
        )

    def get_short_name(self):
        return self.first_name

    @property
    def email(self):
        try:
            email = self.emails.get(is_primary=True)
        except self.emails.model.MultipleObjectsReturned:
            logger.info('Multiple primary emails exist for a user')
            email = self.emails.filter(is_primary=True).first()
        except self.emails.model.DoesNotExist:
            return None
        return email.email

    @property
    def full_name(self):
        if self.middle_name:
            return "%s %s %s" % (
                self.first_name, self.middle_name, self.last_name
            )
        else:
            return "%s %s" % (self.first_name, self.last_name)

    @property
    def logged_in_name(self):
        if len("%s %s %s" % (self.first_name, self.middle_name, self.last_name)) <= 16:
            return "%s %s %s" % (self.first_name, self.middle_name, self.last_name)
        elif len("%s %s" % (self.first_name, self.last_name)) <= 16:
            return "%s %s" % (self.first_name.strip(), self.last_name.strip())
        else:
            return self.first_name

    @permalink
    def get_absolute_url(self):
        return 'author_post_list', None, {'slug': self.slug}

    @property
    def permissions(self):
        # TODO: Need to deep look.
        return self.get_all_permissions()

    @property
    def primary_email(self):
        emails = self.emails.filter(is_primary=True)
        all_emails = self.emails.all()
        if emails:
            return emails.last().email
        else:
            return all_emails.last().email if all_emails else None

    @property
    def primary_phone(self):
        phones = self.phones.filter(is_primary=True)
        all_phones = self.phones.all()
        if phones:
            return phones.last().number
        else:
            return all_phones.last().number if all_phones else None

    def save_extra_data(self, data):
        if not self.extra:
            self.extra = {}
        self.extra.update(data)
        self.save()

    def save(self, *args, **kwargs):
        if self.created_by:
            self.created_by = kwargs.get("created_by", None)
            self.last_modified_by = self.created_by

        super(User, self).save(*args, **kwargs)
        if not self.slug:
            try:
                import utils
            except ImportError:
                from core import utils

            slug = utils.slugify(self.first_name)
            if User.objects.filter(slug=slug):
                slug = "%s-%s" % (slug, self.id)
            self.slug = slug
            self.save()

    def is_dealer(self):
        for employee in self.employee_instance.all():
            if employee.has_perm('is_dealer'):
                return True
        return False

    def _get_unverified_email(self, email):
        emails = self.emails.filter(email=email, is_verified=False)
        return emails, emails.count()

    def fix_duplicate_emails(self, email):
        emails = self.emails.filter(email=email)
        if emails.count() > 1:
            main_email = emails[0]
            emails = emails[1:]
            for email in emails:
                if email.is_verified:
                    main_email.is_verified = True
                elif email.is_primary:
                    main_email.is_primary = True
                elif email.is_bogus:
                    main_email.is_bogus = True
                elif not email.is_exists:
                    main_email.is_exists = False

                email.delete()

            main_email.save(
                update_fields=[
                    'is_verified', 'is_primary',
                    'is_bogus', 'is_exists'
                ]
            )

            return main_email

    def _fix_primary_email(self, email, **kwargs):
        if "is_primary" in kwargs and kwargs['is_primary']:
            self.emails.all().exlude(email, is_primary=True).update(is_primary=False)

        if not self.emails.filter(is_primary=True).count():
            self.emails.filter(email=email).update(is_primary=True)

    def attach_email(self, email, **kwargs):
        """
        :param email:
        :param kwargs:
                is_primary
                is_verified
                is_exists
                is_bogus
        :return:
        """
        from core.models import Email
        # Validate args.
        is_verified = kwargs.get("is_verified")
        is_primary = kwargs.get("is_primary")
        is_bogus = kwargs.get("is_bogus")
        is_exists = kwargs.get("is_exists")

        has_verified_key = False if is_verified is None else True
        has_primary_key = False if is_primary is None else True
        has_bogus_key = False if is_bogus is None else True
        has_exists_key = False if is_exists is None else True

        new_kwargs = {}
        if has_verified_key:
            new_kwargs["is_verified"] = is_verified

        if has_primary_key:
            new_kwargs["is_primary"] = is_primary

        if has_bogus_key:
            new_kwargs["is_bogus"] = is_bogus

        if has_exists_key:
            new_kwargs["is_exists"] = is_exists

        if not len(new_kwargs.keys()):
            raise Exception("Pass some parameter for update")

        if len(set(new_kwargs.keys()).union(set(['is_primary', 'is_bogus', 'is_exists', 'is_verified']))) > 4:
            raise Exception("Unknown field passed into kwargs. Check doc string.")

        user_emails = self.emails.filter(email=email, **new_kwargs)
        if user_emails:
            if user_emails.count() > 1:
                email = self.fix_duplicate_emails(
                    email,
                    **new_kwargs
                )

                return email

            return user_emails.first()

        if "is_verified" in kwargs and kwargs['is_verified'] and kwargs['is_verified'] is True:
            # Check with other user email is verified true.
            emails = Email.objects.filter(email=email, is_verified=True)

            # If verified true with other user then update others with verified False.
            if emails:
                emails.update(is_verified=False)
                if self.emails.filter(email=email).count():
                    self.emails.filter(email=email).update(**new_kwargs)
                    self._fix_primary_email(email, **new_kwargs)
                    return self.emails.filter(email=email).first()
                else:
                    email = Email.objects.create(
                        email=email,
                        user=self,
                        **new_kwargs
                    )
                    self._fix_primary_email(email, **new_kwargs)
                    return email
            else:
                email = Email.objects.create(
                    email=email,
                    user=self,
                    **new_kwargs
                )

            self._fix_primary_email(email, **new_kwargs)
            return email

        elif "is_verified" in kwargs and kwargs['is_verified'] is 'auto':
            # Check with other emails if they are verified true.
            emails = Email.objects.filter(email=email, is_verified=True)
            if emails:
                # Add email verified False.
                # Check if already verified email exist with current user.
                user_emails = self.emails.filter(email=email, is_verified=False)
                if user_emails:
                    if user_emails.count() > 1:
                        pass
                    else:
                        return self.emails.first()
                else:
                    return Email.objects.create(
                        email=email,
                        is_verified=False,
                        user=self
                    )
            else:
                # Add verified email.
                return Email.objects.create(
                        email=email,
                        is_verified=True,
                        user=self
                    )

        elif "is_verified" in kwargs and kwargs['is_verified'] is False:
            emails, e_count = self._get_unverified_email(email)
            if e_count and e_count > 1:
                email = self.fix_duplicate_emails(email, **new_kwargs)
                self._fix_primary_email(email, **new_kwargs)
                return email
            else:
                email = Email.objects.create(
                    email=email,
                    user=self,
                    **new_kwargs
                )
                self._fix_primary_email(email, **new_kwargs)
                return email

    def _get_unverified_phones(self, phone):
        phones = self.phones.filter(number=phone, is_verified=False)
        return phones, phones.count()

    def fix_duplicate_phones(self, phone):
        phones = self.phones.filter(number=phone)
        if phones.count() > 1:
            main_phone = phones[0]
            phones = phones[1:]
            for phone in phones:
                if phone.is_verified:
                    main_phone.is_verified = True
                elif phone.is_primary:
                    main_phone.is_primary = True
                elif phone.is_bogus:
                    main_phone.is_bogus = True
                elif not phone.is_exists:
                    main_phone.is_exists = False

                phone.delete()

            main_phone.save(
                update_fields=[
                    'is_verified', 'is_primary',
                    'is_bogus', 'is_exists'
                ]
            )

            return main_phone

    def _fix_primary_phone(self, phone, **kwargs):
        if "is_primary" in kwargs and kwargs['is_primary']:
            self.phones.all().exlude(phone, is_primary=True).update(is_primary=False)

        if not self.phones.filter(is_primary=True).count():
            self.phones.filter(number=phone).update(is_primary=True)

    def attach_phone(self, phone, **kwargs):
        """
        :param email:
        :param kwargs:
                is_primary
                is_verified
                is_exists
                is_bogus
        :return:
        """
        from core.models import Phone
        # Validate args.
        is_verified = kwargs.get("is_verified")
        is_primary = kwargs.get("is_primary")
        is_bogus = kwargs.get("is_bogus")
        is_exists = kwargs.get("is_exists")

        has_verified_key = False if is_verified is None else True
        has_primary_key = False if is_primary is None else True
        has_bogus_key = False if is_bogus is None else True
        has_exists_key = False if is_exists is None else True

        new_kwargs = {}
        if has_verified_key:
            new_kwargs["is_verified"] = is_verified

        if has_primary_key:
            new_kwargs["is_primary"] = is_primary

        if has_bogus_key:
            new_kwargs["is_bogus"] = is_bogus

        if has_exists_key:
            new_kwargs["is_exists"] = is_exists

        if not len(new_kwargs.keys()):
            raise Exception("Pass some parameter for update")

        if len(set(new_kwargs.keys()).union(set(['is_primary', 'is_bogus', 'is_exists', 'is_verified']))) > 4:
            raise Exception("Unknown field passed into kwargs. Check doc string.")

        user_phones = self.phones.filter(number=phone, **new_kwargs)
        if user_phones:
            if user_phones.count() > 1:
                email = self.fix_duplicate_phones(
                    phone,
                    **new_kwargs
                )

                return email

            return user_phones.first()

        if "is_verified" in kwargs and kwargs['is_verified']:
            # Check with other user email is verified true.
            phones = Phone.objects.filter(number=phone, is_verified=True)

            # If verified true with other user then update others with verified False.
            if phones:
                phones.update(is_verified=False)
                if self.phones.filter(number=phone).count():
                    self.phones.filter(number=phone).update(**new_kwargs)
                    self._fix_primary_phone(phone, **new_kwargs)
                    return self.emails.filter(number=phone).first()
                else:
                    email = Phone.objects.create(
                        number=phone,
                        user=self,
                        **new_kwargs
                    )
                    self._fix_primary_phone(phone, **new_kwargs)
                    return email
            else:
                phone_instance = Phone.objects.create(
                    number=phone,
                    user=self,
                    **new_kwargs
                )

            self._fix_primary_phone(phone, **new_kwargs)
            return phone_instance

        elif "is_verified" in kwargs and not kwargs['is_verified']:
            emails, e_count = self._get_unverified_phones(phone)
            if e_count and e_count > 1:
                phone = self.fix_duplicate_phones(phone, **new_kwargs)
                self._fix_primary_phone(phone, **new_kwargs)
                return phone
            else:
                phone = Phone.objects.create(
                    number=phone,
                    user=self,
                    **new_kwargs
                )
                self._fix_primary_phone(phone, **new_kwargs)
                return phone

    def update_data(self, data):
        from core.models import FormData
        if not self.fd:
            self.fd = FormData.objects.create(user=self)
            self.save(update_fields=["fd"])
        self.fd.update(data)
