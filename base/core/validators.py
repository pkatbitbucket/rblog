# coding=utf-8
import re
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _

__author__ = '¶¡rañha'


mobile_validator = RegexValidator(
    re.compile('^[7-9]\d{9}$'),
    message=_('Enter a valid integer.'),
    code='invalid',
)
