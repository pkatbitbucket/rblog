import datetime
import json
import os
import urllib
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib.auth import views as auth_views
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.sitemaps.views import sitemap as base_sitemap
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.middleware import csrf
from django.shortcuts import (get_object_or_404, redirect, render,
                              render_to_response)
from django.template import RequestContext
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.http import require_POST
from django.views.generic.list import ListView
from oauth2client.client import FlowExchangeError, flow_from_clientsecrets
from pyfb import Pyfb, PyfbException

import cf_cns
import putils
import utils
from core import models as core_models
from flatpages.models import SEOPage
from wiki.models import Insurer, MedicalCondition, WikiArticle

from . import forms as core_forms
from .models import Email, Mail, User


def get_token(request):
    email = None
    if request.user.is_authenticated():
        email = request.user.email
    return HttpResponse(json.dumps({
        'csrf': csrf.get_token(request),
        'is_authenticated': request.user.is_authenticated(),
        'email': email
    }))


@login_required
@utils.profile_type_only('AUTHOR', 'WIKI_DATA_ENTRY', 'ADMIN', 'HOSPITAL_DATA_ENTRY', 'INSURER', 'REPORT')
def coverfox_login(request):
    return render_to_response("core/admin_dashboard.html", {
    }, context_instance=RequestContext(request))


class AdminWikiView(ListView):
    model = Insurer
    template_name = "core/admin_wiki_list.html"

    @method_decorator(utils.profile_type_only('WIKI_DATA_ENTRY'))
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(AdminWikiView, self).dispatch(*args, **kwargs)


class AdminMedicalConditionListView(ListView):
    model = MedicalCondition
    template_name = "core/admin_medical_condition_list.html"

    @method_decorator(utils.profile_type_only('WIKI_DATA_ENTRY'))
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(AdminMedicalConditionListView, self).dispatch(*args, **kwargs)


def admin_page_list_view(request):
    post_dict = {}
    category = request.GET.get('category', 'PRODUCT_REVIEW')
    for insurer in Insurer.objects.all():
        post_dict[insurer] = WikiArticle.objects.filter(
            insurer=insurer, category=category)
    return render_to_response("core/admin_page_list.html", {
        'post_dict': post_dict
    }, context_instance=RequestContext(request))


@utils.profile_type_only('ADMIN')
@never_cache
def mail_add(request, mail_id=None):
    if request.POST:
        if mail_id:
            mail = get_object_or_404(Mail, pk=mail_id)
            mail_add_form = core_forms.MailForm(request.POST, instance=mail)
        else:
            mail_add_form = core_forms.MailForm(request.POST)

        if mail_add_form.is_valid():
            mail_add_form.save()
            return HttpResponseRedirect(reverse('mail_add'))
    else:
        if mail_id:
            mail = get_object_or_404(Mail, pk=mail_id)
            mail_add_form = core_forms.MailForm(instance=mail)
        else:
            mail_add_form = core_forms.MailForm()
    mail_list = Mail.objects.all()
    return render_to_response("core/mail_manage.html", {
        'form': mail_add_form,
        'mail_list': mail_list,
    }, context_instance=RequestContext(request))


def soc_home(request):
    """Home view, displays login mechanism"""
    if request.user.is_authenticated():
        # putils.kiss_identify_user(request)
        return redirect('soc_done')
    return render_to_response('core/soc/home.html', {}, RequestContext(request))


@login_required
def soc_done(request):
    """Login complete view, displays user data"""
    return render_to_response('core/soc/done.html', {'user': request.user},
                              RequestContext(request))


def soc_signup_email(request):
    return render_to_response('core/soc/email_signup.html', {}, RequestContext(request))


def soc_validation_sent(request):
    return render_to_response('core/soc/validation_sent.html', {
        'email': request.session.get('email_validation_address')
    }, RequestContext(request))


def soc_require_email(request):
    if request.method == 'POST':
        request.session['saved_email'] = request.POST.get('email')
        backend = request.session['partial_pipeline']['backend']
        return redirect('social:complete', backend=backend)
    return render_to_response('core/soc/email.html', RequestContext(request))


def irda_licence(request):
    return render_to_response("core/irda_licence.html", {
        "recommended_reads": putils.get_recommended_reads(3),
    }, context_instance=RequestContext(request))


def browser_not_supported(request):
    return render_to_response("core/browser_not_supported.html", {
    }, context_instance=RequestContext(request))


def reports_dashboard(request):
    return render_to_response("core/reports_dashboard.html", {
    }, context_instance=RequestContext(request))


def review_dashboard(request):
    return render_to_response("core/review_dashboard.html", {
    }, context_instance=RequestContext(request))


def document_view(request, document):
    return render_to_response("core/docs/%s.html" % document, {
    }, context_instance=RequestContext(request))


@require_POST
@never_cache
def google_registration(request):
    post_data = request.POST
    data = json.loads(post_data['gdata'])
    local_dob = None
    local_gender = None
    new_user = False
    fields = {'email': ''}
    if not 'emails' in data.keys():
        return HttpResponse(json.dumps({'success': False}))
    for e in data['emails']:
        if e['type'] == 'account':
            fields['email'] = e['value']
    fields['first_name'] = data['name']['givenName']
    if fields.get('email'):
        try:
            # Successful google auth
            oauth_flow = flow_from_clientsecrets(os.path.join(
                settings.CONF_ROOT, 'google_plus_client_secrets.json'), scope='')
            oauth_flow.redirect_uri = 'postmessage'
            credentials = oauth_flow.step2_exchange(
                request.POST['google_code'])

            # Possible hack attempt
            if credentials.access_token != request.POST['google_access_token']:
                return HttpResponse(json.dumps({'success': False, 'dob': local_dob, 'gender': local_gender}))

            data['credentials'] = credentials.to_json()
            data['gplus_id'] = credentials.id_token['sub']

        except FlowExchangeError, e:
            # Failed google auth
            # print "FlowExchangeError Exception ", e
            return HttpResponse(json.dumps({'success': False, 'dob': local_dob, 'gender': local_gender}))

        try:
            user = User.objects.get_user(email=fields['email'], phone='')
            new_user = False
            user.extra['google_data'] = data
            user.is_verified = True
            user.save()
        except User.DoesNotExist:
            new_user = True
            user = User.objects.create_user(**fields)
            if 'gender' in data.keys():
                user.gender = 2 if data['gender'] == 'female' else 1
            user.last_name = data['name']['familyName']

            user.extra['google_data'] = data
            user.save()
            obj = {
                'user_id': user.email,
                'to_email': user.email,
                'name': utils.clean_name(user.first_name),
                'review_url': '%s/review/start/' % (settings.SITE_URL)
            }

            if not user.is_verified:
                cf_cns.notify('VERIFIED_WELCOME_MAIL', to=(obj.get('name'), obj['to_email']), obj=obj)
                user.is_verified = True
                user.save()

        if 'tracking_data' not in user.extra.keys():
            tracking_data = {
                'device': 'mobile' if request.user_agent.is_mobile else 'desktop',
                'utm_source': request.COOKIES.get('utm_source', ''),
                'utm_medium': request.COOKIES.get('utm_medium', ''),
                'utm_campaign': request.COOKIES.get('utm_campaign', ''),
                'utm_adgroup': request.COOKIES.get('utm_adgroup', ''),
                'utm_keyword': request.COOKIES.get('utm_keyword', ''),
                'utm_content': request.COOKIES.get('utm_content', ''),
            }
            user.extra['tracking_data'] = tracking_data
            user.save()

        u = authenticate(username=user.email, password=settings.RANDOM_SEED)
        login(request, u)
        # putils.kiss_identify_user(request)
        if user.gender:
            local_gender = 'FEMALE' if int(user.gender) == 2 else 'MALE'
        if user.birth_date:
            local_dob = datetime.datetime.strftime(user.birth_date, '%d-%m-%Y')
        response = HttpResponse(json.dumps(
            {'success': True, 'new_user': new_user, 'dob': local_dob, 'gender': local_gender, 'email': user.email}))
        response.set_cookie(key='profile', value=urllib.quote(json.dumps({
            'email': user.email,
            'type': 'gplus',
            'photo': str(user.extra['google_data']['image']['url']),
            'name': user.extra['google_data'].get('displayName'),
        })))
        return response
    else:
        return HttpResponse(json.dumps({'success': False, 'dob': local_dob, 'gender': local_gender}))


@require_POST
@never_cache
def facebook_registration(request):
    post_data = request.POST
    data = json.loads(post_data['fb_data'])
    local_dob = None
    local_gender = None
    new_user = False
    fields = {'email': ''}

    # HACK for FB not giving email sometimes, initially store email as username@facebook.com
    # If you ever get email from FB, replace it with proper email-id
    if not 'email' in data.keys():
        fields['email'] = "%s@facebook.com" % data.get('username', 'username')
    else:
        fields['email'] = data['email']

    try:
        user = User.objects.get_user(email=fields['email'], phone='')
    except User.DoesNotExist:
        user = None

    fields['first_name'] = data['first_name']
    access_token = request.POST["facebook_access_token"]
    facebook = Pyfb(settings.FACEBOOK_APP_ID)
    facebook.set_access_token(access_token)

    try:
        fbuser = facebook.get_myself()
    except PyfbException, e:
        # print "PyFB Exception Hack Attempt", e
        return HttpResponse(json.dumps({'success': False, 'dob': local_dob, 'gender': local_gender}))

    if user:
        new_user = False
        # HACK for FB not giving email sometimes, initially store email as username@facebook.com
        # If you ever get email from FB, replace it with proper email-id
        if data.get('email'):
            email_obj = Email.objects.get(email=user.email, is_verified=True)
            email_obj.email = data['email']
            email_obj.save()
        user.extra['facebook_data'] = data
        user.is_verified = True
        user.save()
    else:
        new_user = True
        user = User.objects.create_user(**fields)
        if 'gender' in data.keys():
            user.gender = 2 if data['gender'] == 'female' else 1
        user.last_name = data['last_name']
        if 'birthday' in data.keys():
            user.birth_date = datetime.datetime.strptime(
                data['birthday'], '%m/%d/%Y').date()
        user.extra['facebook_data'] = data
        user.save()
        obj = {
            'user_id': user.email,
            'to_email': user.email,
            'name': utils.clean_name(user.first_name),
            'review_url': '%s/review/start/' % (settings.SITE_URL)
        }
        if not user.is_verified:
            cf_cns.notify('VERIFIED_WELCOME_MAIL', to=(obj.get('name'), obj['to_email']), obj=obj)
            user.is_verified = True
            user.save()

    if 'tracking_data' not in user.extra.keys():
        tracking_data = {
            'device': 'mobile' if request.user_agent.is_mobile else 'desktop',
            'utm_source': request.COOKIES.get('utm_source', ''),
            'utm_medium': request.COOKIES.get('utm_medium', ''),
            'utm_campaign': request.COOKIES.get('utm_campaign', ''),
            'utm_adgroup': request.COOKIES.get('utm_adgroup', ''),
            'utm_keyword': request.COOKIES.get('utm_keyword', ''),
            'utm_content': request.COOKIES.get('utm_content', ''),
        }
        user.extra['tracking_data'] = tracking_data
        user.save()

    user = authenticate(username=user.email, password=settings.RANDOM_SEED)
    login(request, user)

    if user.gender:
        local_gender = 'FEMALE' if int(user.gender) == 2 else 'MALE'
    if user.birth_date:
        local_dob = datetime.datetime.strftime(user.birth_date, '%d-%m-%Y')
    response = HttpResponse(json.dumps(
        {'success': True, 'new_user': new_user, 'dob': local_dob, 'gender': local_gender, 'email': user.email}))
    response.set_cookie(key='profile', value=urllib.quote(json.dumps({
        'email': user.email,
        'type': 'facebook',
        'photo': str('http://graph.facebook.com/%s/picture?type=small' % user.extra['facebook_data']['id']),
        'name': "%s %s" % (user.extra['facebook_data'].get('first_name'), user.extra['facebook_data'].get('last_name')),
        new_user: new_user,
    })))
    return response


@never_cache
def forgot_password(request):
    return render_to_response('core/forgot_password.html', {}, RequestContext(request))


@never_cache
def reset_password(request, token):
    if not User.objects.filter(token=token):
        headline = 'Sorry!'
        message = 'This password reset link is no longer valid. Please use <a href="/accounts/login/">reset password</a> for generating a new link.'
        subtext = 'If you still face any issue drop us a mail at <a href="mailto:info@coverfox.com">info@coverfox.com</a>.'
        return render_to_response('core/failure_generic.html', {
            'headline': headline,
            'message': message,
            'subtext': subtext
        }, RequestContext(request))

    else:
        user = User.objects.get(token=token)
        if user.token_updated_on.replace(tzinfo=None) < (datetime.datetime.today() + relativedelta(months=-1)):
            headline = 'Looong time, no see!'
            message = 'Our minions indicate that you have clicked an expired link'
            subtext = 'How about a fresh start <a href="/">here</a>!'
            return render_to_response('core/failure_generic.html', {
                'headline': headline,
                'message': message,
                'subtext': subtext
            }, RequestContext(request))
    return render_to_response('account_manager/reset_password.html', {
        'token': token,
    }, RequestContext(request))


@never_cache
def custom_logout(request, *args, **kw):
    response = auth_views.logout(request, *args, **kw)
    response.delete_cookie('profile')
    return response


@never_cache
def custom_login(request, *args, **kw):
    if request.method == "GET" and request.user.is_authenticated():
        return redirect(settings.LOGIN_REDIRECT_URL)
    response = auth_views.login(
        request, template_name='account_manager/login.html', *args, **kw)
    if request.user.is_authenticated():
        # putils.kiss_identify_user(request)
        response.set_cookie(key='profile', value=urllib.quote(json.dumps({
            'type': 'system',
            'name': request.user.get_full_name(),
            'email': request.user.email
        })))
    return response


@never_cache
def bad_wolf(request):
    raise


@never_cache
def careers(request):
    return render_to_response('core/careers.html', {}, RequestContext(request))


@never_cache
@require_POST
def schedule_cat(request):
    msg = {}
    msg.update(json.loads(request.POST['data']))
    msg['created_on'] = datetime.datetime.now().strftime("%d/%m/%Y %H:%M")
    slot = ''
    contact_data = msg['contact_data']
    if 'todaySlots' in contact_data.keys():
        for i in contact_data['todaySlots']:
            if 'slot_selected' in i.keys() and i['slot_selected'].upper() == 'YES':
                appointment_date = contact_data['todayText']
                slot = i['slot']
    if 'tommSlots' in contact_data.keys():
        for i in contact_data['tommSlots']:
            if 'slot_selected' in i.keys() and i['slot_selected'].upper() == 'YES':
                appointment_date = contact_data['tommText']
                slot = i['slot']
    if 'afterSlots' in contact_data.keys():
        for i in contact_data['afterSlots']:
            if 'slot_selected' in i.keys() and i['slot_selected'].upper() == 'YES':
                appointment_date = contact_data['afterText']
                slot = i['slot']
    if 'dayAfterSlots' in contact_data.keys():
        for i in contact_data['dayAfterSlots']:
            if 'slot_selected' in i.keys() and i['slot_selected'].upper() == 'YES':
                appointment_date = contact_data['dayAfterText']
                slot = i['slot']

    msg.update({
        'appointment_date': appointment_date,
        'mobile': msg['contact_data']['mobile'],
        'slot': slot,
        'contact_person': 'Varun',
        'contact_number': '8424011934'
    })
    msg.pop('contact_data')
    # sms_msg = 'Thank you for showing interest in being a part of Coverfox! ' \
    #           'Your apointment has been confirmed on %s between %s. We look forward to meeting you. ' \
    #           'Our address is: Coverfox, 3rd Floor, B-Wing, Krislon House, Saki Vihar Road, ' \
    #           'Opposite Marwah Center, Saki Naka, Andheri (E). ' \
    #           'Feel free to call %s from our team on %s in case of any concerns or if you have trouble finding us.' % (
    #               appointment_date,
    #               slot,
    #               'Varun',
    #               '8424011934'
    #           )

    # SEND SMS FOR APPOINTMENT CONFIRMATION TO CUSTOMER
    # thread.start_new_thread(utils.send_sms, (msg['mobile'], sms_msg))

    # SEND MAIL OF APPOINTMENT CONFIRMATION TO RECRUITERS
    # mail_list = [
    #     'varun.salian@coverfoxmail.com',
    #     'prajaktta@coverfoxmail.com',
    #     'deepika.salian@coverfox.com'
    # ]
    # set these from Notification admin

    # this will send sms and mails both.
    cf_cns.notify('APPOINTMENT_ALERT', obj={'msg': msg}, numbers=msg['mobile'], mandrill_template='newbase')
    return HttpResponse(json.dumps({'success': True}))


def sitemap(request, *args, **kwargs):
    """
    Remove duplicate urls on first come first picked basis
    Look for matching urls in SEOPage model and replace loc with redirect_url
    """
    response = base_sitemap(request, *args, **kwargs)
    urlset = response.context_data['urlset']

    import urlparse
    for url in urlset:
        try:
            loc = urlparse.urlparse(url['location']).path
            url['location'] = request.build_absolute_uri(SEOPage.objects.exclude(
                redirect_url='').get(url=loc, redirect=301, redirect_url__isnull=False).redirect_url)
        except SEOPage.DoesNotExist:
            pass

    urls = {}
    for url in urlset:
        loc = url['location']
        urls[loc] = urls.get(loc, url)
    urlset = urls.values()

    response.context_data['urlset'] = urlset
    return response


class UserManageView(ListView):
    queryset = User.objects.filter(
        Q(groups__name__in=["AUTHOR", "REPORT", "HOSPITAL_DATA_ENTRY", "TESTER", "WIKI_DATA_ENTRY", "INSURER"]) |
        Q(is_superuser=True)
    ).distinct().order_by("-is_superuser", "-is_staff", "first_name")
    context_object_name = "user_list"
    template_name = "core/user_manage.html"

    @method_decorator(utils.profile_type_only())
    def dispatch(self, *args, **kwargs):
        return super(UserManageView, self).dispatch(*args, **kwargs)


def timesince(dt, default="just now"):
    """
    Returns string representing "time since" e.g.
    3 days ago, 5 hours ago etc.
    """
    now = timezone.now()
    if not dt:
        return "Unknown time"
    diff = now - dt
    periods = (
        (diff.days / 365, "year", "years"),
        (diff.days / 30, "month", "months"),
        (diff.days / 7, "week", "weeks"),
        (diff.days, "day", "days"),
        (diff.seconds / 3600, "hour", "hours"),
        (diff.seconds / 60, "minute", "minutes"),
        (diff.seconds, "second", "seconds"),
    )
    for period, singular, plural in periods:
        if period:
            return "%d %s ago" % (period, singular if period == 1 else plural)
    return default


def req_info(req):
    html_data = []
    html_data.append("Requirement[%s]: product=%s, status=%s, next=%s, created=%s, modified=%s" % (
        req.id, req.kind.product, req.current_state, req.next_crm_task,
        timesince(req.created_date), timesince(req.modified_date)
    ))
    acts = req.activity_set.order_by("-id")
    if acts:
        html_data.append("Web activities:")
        for act in acts[:15]:
            data = "%s %s %s %s %s %s" % \
                   (
                        act.ip_address,
                        act.tracker.session_key if act.tracker else None,
                        act.tracker.user if act.tracker else None,
                        act.page_id,
                        act.activity_type,
                        timesince(act.created)
                    )
            html_data.append(data)
            if act.tracker and act.tracker.user:
                html_data.append(act.tracker.user.phones.values_list("number", "is_verified"))
        if len(acts) > 10:
            html_data.append("Skipped %s older activities." % (len(acts) - 10))
    else:
        html_data.append("Requirement has no web activity.")

    acts = req.crm_activities.order_by("-id")
    if acts:
        html_data.append("CRM activities:")
        for act in acts[:15]:
            html_data.append("%s: %s, start %s, end %s, category: %s, disposition: %s, sub dispostion: %s, number: %s" % (
                act.id, act.user.user.email, timesince(act.start_time),
                timesince(act.end_time), act.category, act.disposition, act.sub_disposition,
                act.data.get("mobile"),
            )) # , act.data
        if len(acts) > 10:
            html_data.append("Skipped %s older activities." % (len(acts) - 10))
    else:
        html_data.append("Requirement has no crm activity.")

    return html_data


def phone_info(number):
    phone_users = User.objects.filter(phones__number=number)
    html_data = []
    if phone_users:
        html_data.append("Users using %s: %s" % (number, len(phone_users)))
        for user in phone_users:
            uphones = core_models.Phone.objects.filter(user=user)
            for uphone in uphones:
                html_data.append("=" * 80)
                html_data.append("User: %s (%s), is_verified: %s, is_primary: %s, is_bogus: %s" % (
                    user.get_full_name(), user.id, uphone.is_verified, uphone.is_primary,
                    uphone.is_bogus
                ))

            html_data.append("User has %s policies." % core_models.Policy.objects.filter(user=user).count())
            html_data.append("User has %s trackers." % core_models.Tracker.objects.filter(user=user).count())
            reqs = core_models.Requirement.objects.filter(user=user)
            if reqs:
                html_data.append("Requirements: %s." % len(reqs))
                for req in reqs:
                    html_data += req_info(req)
            else:
                html_data.append("There are no requriements.")
                for tracker in user.tracker_set.all():
                    for act in tracker.activity_set.all()[:10]:
                        data = "%s %s %s %s %s %s" % \
                               (
                                    act.ip_address,
                                    act.tracker.session_key if act.tracker else None,
                                    act.tracker.user if act.tracker else None,
                                    act.page_id,
                                    act.activity_type,
                                    timesince(act.created)
                                )
                        html_data.append(data)
                        if act.tracker and act.tracker.user:
                            data = "act.tracker.user %s %s" % \
                                (
                                    act.tracker.user.phones.values_list("number", "is_verified"),
                                    act.fd.get_data() if act.fd else None
                                )
                            html_data.append(data)
                        if act.user:
                            data = "act.user %s %s" % \
                                   (
                                        act.user.phones.values_list("number", "is_verified"),
                                        act.fd.get_data() if act.fd else None
                                   )
                            html_data.append(data)
                            data = "act.tracker.phones", act.tracker.phones.values_list("number", "is_verified"), act.fd.get_data() if act.fd else None
                        html_data.append(data)
            dreqs = core_models.Requirement.objects.all().filter(user=user, current_state_id=17)
            if dreqs:
                html_data.append("Deleted requirements: %s." % len(dreqs))
                for req in dreqs:
                    html_data + req_info(req)
    else:
        html_data.append("No users for %s." % number)

    html_data.append("=" * 80)

    phone_visitors = core_models.Tracker.objects.filter(phones__number=number)
    if phone_visitors:
        html_data.append("Visitors using %s: %s" % (number, len(phone_visitors)))
        for visitor in phone_visitors:
            html_data.append("=" * 80)
            html_data.append("This visitor has this phone %s time." % core_models.Phone.objects.filter(visitor=visitor).count())
            uphone = core_models.Phone.objects.filter(visitor=visitor).first()
            html_data.append("Tracker: %s(%s), is_verified: %s, is_primary: %s, is_bogus: %s" % (
                visitor.id, visitor.session_key, uphone.is_verified, uphone.is_primary, uphone.is_bogus
            ))
            reqs = core_models.Requirement.objects.filter(visitor=visitor)
            if reqs:
                html_data.append("Requirements: %s." % len(reqs))
                for req in reqs:
                    html_data += req_info(req)
            else:
                html_data.append("There are no requriements.")
            dreqs = core_models.Requirement.objects.all().filter(visitor=visitor, current_state_id=17)
            if dreqs:
                html_data.append("Deleted requirements: %s." % len(dreqs))
                for req in dreqs:
                    html_data += req_info(req)
    else:
        html_data.append("No visitors for %s." % number)

    return html_data


def email_info(email):
    email_users = User.objects.filter(emails__email=email)
    html_data = []
    if email_users:
        html_data.append("Users using %s: %s" % (email, len(email_users)))
        for user in email_users:
            uemails = core_models.Email.objects.filter(user=user)
            for uemail in uemails:
                html_data.append("=" * 80)
                html_data.append("User: %s (%s), is_verified: %s, is_primary: %s, is_bogus: %s" % (
                    user.get_full_name(), user.id, uemail.is_verified, uemail.is_primary,
                    uemail.is_bogus
                ))

            html_data.append("User has %s policies." % core_models.Policy.objects.filter(user=user).count())
            html_data.append("User has %s trackers." % core_models.Tracker.objects.filter(user=user).count())
            reqs = core_models.Requirement.objects.filter(user=user)
            if reqs:
                html_data.append("Requirements: %s." % len(reqs))
                for req in reqs:
                    html_data += req_info(req)
            else:
                html_data.append("There are no requriements.")
                for tracker in user.tracker_set.all():
                    for act in tracker.activity_set.all()[:10]:
                        data = act.ip_address, act.tracker.session_key if act.tracker else None, act.tracker.user if act.tracker else None, act.page_id, act.activity_type, timesince(act.created)
                        html_data.append(data)
                        if act.tracker and act.tracker.user:
                            data = "act.tracker.user %s %s" % \
                                (
                                    act.tracker.user.phones.values_list("number", "is_verified"),
                                    act.fd.get_data() if act.fd else None
                                )
                            html_data.append(data)
                        if act.user:
                            data = "act.user %s %s" % \
                                (
                                    act.user.phones.values_list("number", "is_verified"),
                                    act.fd.get_data() if act.fd else None
                                )
                            html_data.append(data)

                        data = "act.tracker.phones", act.tracker.phones.values_list("number", "is_verified"), act.fd.get_data() if act.fd else None
                        html_data.append(data)
            dreqs = core_models.Requirement.objects.all().filter(user=user, current_state_id=17)
            if dreqs:
                html_data.append("Deleted requirements: %s." % len(dreqs))
                for req in dreqs:
                    html_data + req_info(req)
    else:
        html_data.append("No users for %s." % email)

    html_data.append("=" * 80)

    phone_visitors = core_models.Tracker.objects.filter(emails__email=email)
    if phone_visitors:
        html_data.append("Visitors using %s: %s" % (email, len(phone_visitors)))
        for visitor in phone_visitors:
            html_data.append("=" * 80)
            html_data.append("This visitor has this phone %s time." % core_models.Phone.objects.filter(visitor=visitor).count())
            uphone = core_models.Phone.objects.filter(visitor=visitor).first()
            html_data.append("Tracker: %s(%s), is_verified: %s, is_primary: %s, is_bogus: %s" % (
                visitor.id, visitor.session_key, uphone.is_verified, uphone.is_primary, uphone.is_bogus
            ))
            reqs = core_models.Requirement.objects.filter(visitor=visitor)
            if reqs:
                html_data.append("Requirements: %s." % len(reqs))
                for req in reqs:
                    html_data += req_info(req)
            else:
                html_data.append("There are no requriements.")
            dreqs = core_models.Requirement.objects.all().filter(visitor=visitor, current_state_id=17)
            if dreqs:
                html_data.append("Deleted requirements: %s." % len(dreqs))
                for req in dreqs:
                    html_data += req_info(req)
    else:
        html_data.append("No visitors for %s." % email)

    return html_data


def user_debug_dashboard(request):
    if not settings.DEBUG:
        raise Http404

    request_user = request.user if request.user.is_authenticated() else None
    request_visitor = request.TRACKER if request.TRACKER else None

    user_reqs = []
    visitor_reqs = []
    if request_user:
        for req in request_user.requirements.iterator():
            info = req_info(req)
            if req.fd:
                info.append("Form data")
                info.append(json.dumps(req.fd.get_data()))
            else:
                info.append("Form data not found")

            user_reqs.append(info)

    if request_visitor:
        for req in request_visitor.requirements.iterator():
            info = req_info(req)
            if req.fd:
                info.append("Form data")
                info.append(json.dumps(req.fd.get_data()))
            else:
                info.append("Form data not found")

            visitor_reqs.append(info)

    return render(request, 'user-debug-dashboard.html', {
        'request_user': request_user,
        'request_visitor': request_visitor,
        'v_reqs': visitor_reqs,
        'u_reqs': user_reqs
    })


def handle500(request):
    return render(request, "core:500.html")


def login_otp(request, nid='MYACCOUNT_OTP'):
    form = core_forms.LoginOTPForm()
    context = {'form': form, 'nid': nid}
    template = 'core/login-otp.html'
    return render(request, template, context)
