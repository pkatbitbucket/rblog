from django.contrib import admin

from coverfox.models import CompanyMembership, Company


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    pass


@admin.register(CompanyMembership)
class CompanyMembershipAdmin(admin.ModelAdmin):
    raw_id_fields = ["user"]
