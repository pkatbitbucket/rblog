from django.conf.urls import url

from . import views as mail_api_views

urlpatterns = [
    url(r'^get-sendgrid-mail/$', mail_api_views.get_sendgrid_mail, name='get_sendgrid_mail'),
]
