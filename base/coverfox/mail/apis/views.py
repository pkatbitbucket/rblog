from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from ..backends import sendgrid


@csrf_exempt
@require_http_methods(["POST"])
def get_sendgrid_mail(request):
    """
    End point where mail is pushed by SendGrid
    """
    email_data = sendgrid.parse_sendgrid_email(request)
    mail = sendgrid.save_mail(email_data)
    return JsonResponse({'success': 'True', 'mail_data': mail.mini_json()})
