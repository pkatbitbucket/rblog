import base64
import re
import dateutil.parser
import mimetypes
import datetime

# project imports
from django.core.files.base import ContentFile
import email
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from apiclient import errors
from collections import defaultdict
from django.core.mail import send_mail as django_send_mail

from celeryapps import mail_celery as celery_mail_app
from .. import models as mail_models
from django.conf import settings


class Gmail():

    def __init__(self, request_user, service):
        self.request_user = request_user
        self.service = service

    def ListHistory(self, email_id, start_history_id='1'):
        """List History of all changes to the user's mailbox.

        Args:
            service: Authorized Gmail API service instance.
            user_id: User's email address. The special value "me"
            can be used to indicate the authenticated user.
            start_history_id: Only return Histories at or after start_history_id.

        Returns:
            A list of mailbox changes that occurred after the start_history_id.
        """
        try:
            history = (self.service.users().history().list(userId=email_id,
                                                           startHistoryId=start_history_id)
                       .execute())
            changes = history['history'] if 'history' in history else []
            while 'nextPageToken' in history:
                page_token = history['nextPageToken']
                history = (self.service.users().history().list(userId='me',
                                                               startHistoryId=start_history_id,
                                                               pageToken=page_token).execute())
                changes.extend(history['history'])

            return [item['messages'][0] for item in changes]
        except errors.HttpError, error:
            print 'An error occurred: %s' % error

    def create_mail_obj(self, d):
        if isinstance(d, list):
            self.create_bulk_mail_objs(d)
            return None
        omail = mail_models.Mail.objects.filter(message_id=d['id'])
        if omail:
            gm = omail[0]
        else:
            try:
                full_message = self.get_full_message(d['id'])
                if full_message:
                    gm = GmailMessage(self.request_user,
                                      full_message)
                    gm.save()
                else:
                    gm = None
            except IndexError as e:
                print e
                gm = None
        return gm

    def create_bulk_mail_objs(self, ids_list):
        mails = []
        for item in ids_list:
            mails.append(self.create_mail_obj(item))
        return mails

    def prepare_mailbox(self):
        gmail_watch(self.request_user.id)
        ids_list = ListMessagesMatchingQuery(
            'me', self.service, "newer_than:1d label:INBOX")
        ids_list.append(ListMessagesMatchingQuery(
            'me', self.service, "newer_than:1d label:SENT"))
        mails = self.create_bulk_mail_objs(ids_list)
        print 'Sending mail that inbox is prepared'
        django_send_mail(
            'JARVIS: Your Mailbox is ready',
            ":)\n\nThanks,\nJarvis Team",
            settings.DEFAULT_FROM_EMAIL,
            [self.request_user.email]
        )
        return mails

    def save_mails_from_history_id(self, email_id, start_history_id):
        ids_list = self.ListHistory(email_id, start_history_id)
        mails = self.create_bulk_mail_objs(ids_list)
        return mails

    def get_mails(self, label_ids):
        message_snippets = ListMessagesWithLabels(
            user_id="me", service=self.service, label_ids=label_ids)
        mails = self.create_bulk_mail_objs(message_snippets)
        return mails

    def get_attachment_content(self, msg_id, att_id):
        att = self.service.users().messages().attachments().get(
            userId="me", messageId=msg_id, id=att_id).execute()
        file_data = base64.urlsafe_b64decode(att['data'].encode('UTF-8'))
        return file_data

    def get_full_message(self, msg_id):
        if msg_id:
            try:
                message = self.service.users().messages().get(userId="me", id=msg_id).execute()
                return message
            except errors.HttpError:
                return None
        else:
            return None

    def send_mail(self, email_data, fail_on_warning=False):
        if email_data["is_html"] and not email_data["is_attachment"]:
            message = CreateHTMLMessage(
                sender="me",
                email_data=email_data,
            )
        elif email_data["is_attachment"]:
            message = CreateMessageWithAttachment(
                sender="me",
                email_data=email_data,
            )
        else:
            message = CreateMessage(
                sender="me",
                email_data=email_data,
            )
        gmail_response = send_message(
            service=self.service, user_id="me", message=message)
        gm = GmailMessage(self.request_user,
                          self.get_full_message(gmail_response["id"]))
        mail = gm.save()
        mail.is_unread = False
        mail.save()
        return mail


def mark_message_read(service, msg_id):
    read_labels = CreateMsgLabels(removeLabelIds=["UNREAD"], addLabelIds=[])
    response = ModifyMessage(user_id="me", msg_id=msg_id,
                             service=service, msg_labels=read_labels)
    return response


def mark_message_unread(service, msg_id):
    unread_labels = CreateMsgLabels(removeLabelIds=[], addLabelIds=["UNREAD"])
    response = ModifyMessage(user_id="me", msg_id=msg_id,
                             service=service, msg_labels=unread_labels)
    return response


def get_header_value(message, header):
    for hdr in message["payload"]["headers"]:
        if hdr["name"] == header:
            return hdr["value"]


def get_thread(user_id, service, thread_id):
    """Get the Thread
    Args:
        service: Authorized Gmail API service instance.
        user_id: User's email address. The special value "me"
        can be used to indicate the authenticated user.
        thread_id: The ID of the Thread required.

    Returns:
        Thread with matching ID.
    """

    if not thread_id:
        return "Please give thread_id"

    try:
        thread = service.users().threads().get(userId=user_id, id=thread_id).execute()
        messages = thread['messages']
        print 'thread id: %s - number of messages in this thread: %d' % (thread['id'], len(messages))
        return thread
    except errors.HttpError, error:
        print 'An error occurred: %s' % error
        return None


def GetMessage(user_id, msg_id, service):
    """Get a Message with given ID.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      msg_id: The ID of the Message required.

    Returns:
      A Message.
    """
    try:
        message = service.users().messages().get(userId=user_id, id=msg_id).execute()
        return message
    except errors.HttpError, error:
        print "Error", error
        return None


def GetMimeMessage(user_id, msg_id, service):
    """Get a Message and use it to create a MIME Message.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      msg_id: The ID of the Message required.

    Returns:
      A MIME Message, consisting of data from Message.
    """
    try:
        message = service.users().messages().get(
            userId=user_id, id=msg_id, format='raw').execute()
        msg_str = base64.urlsafe_b64decode(message['raw'].encode('ASCII'))
        mime_msg = email.message_from_string(msg_str)
        return mime_msg
    except errors.HttpError, error:
        print "Error", error
        return None


def set_base_headers(sender, data):
    return (
        ('To', data['to']),
        ('From', sender),
        ('Subject', data['subject']),
        ('labelIds', data['labelIds']),
        ('Cc', data['cc']),
        ('Bcc', data['bcc'])
    )


def CreateMessage(sender, email_data):
    """Create a message for an email.

    Args:
        sender: Email address of the sender.
        to: Email address of the receiver.
        subject: The subject of the email message.
        message_text: The text of the email message.
        threadId = "14cb8369f527c0b2"
    Returns:
        An object containing a base64 encoded email object.
    """
    message = MIMEText(email_data['body'])
    for k, v in set_base_headers(sender, email_data):
        message[k] = v

    message['labelIds'] = email_data['labelIds']
    if email_data.get('Message-ID'):
        message['Message-ID'] = email_data['Message-ID']
    if email_data.get('In-Reply-To'):
        message['In-Reply-To'] = email_data['In-Reply-To']
    if email_data.get('References'):
        message['References'] = email_data['References']

    if email_data.get('threadId'):
        message['threadId'] = email_data['threadId']
        return {'raw': base64.urlsafe_b64encode(message.as_string()), 'threadId': email_data['threadId']}
    else:
        return {'raw': base64.urlsafe_b64encode(message.as_string())}


def CreateHTMLMessage(sender, email_data):
    """Create a message for an email.

    Args:
        sender: Email address of the sender.
        to: Email address of the receiver.
        subject: The subject of the email message.
        message_text: The text of the email message.
        threadId = "14cb8369f527c0b2"
    Returns:
        An object containing a base64 encoded email object.
    """
    message = MIMEMultipart('alternative')
    for k, v in set_base_headers(sender, email_data):
        message[k] = v

    # text = email_data['body'].encode('utf-8')
    # part1 = MIMEText(text)
    # message.attach(part1)
    html = email_data.get('body', '').encode('utf-8')
    part2 = MIMEText(html)
    part2.set_type('text/html')
    message.attach(part2)

    if email_data.get('Message-ID'):
        message['Message-ID'] = email_data['Message-ID']
    if email_data.get('In-Reply-To'):
        message['In-Reply-To'] = email_data['In-Reply-To']
    if email_data.get('References'):
        message['References'] = email_data['References']

    if email_data.get('threadId'):
        message['threadId'] = email_data['threadId']
        return {'raw': base64.urlsafe_b64encode(message.as_string()), 'threadId': email_data['threadId']}
    else:
        return {'raw': base64.urlsafe_b64encode(message.as_string())}


def CreateMessageWithAttachment(sender, email_data):
    """Create a message for an email.

    Args:
        sender: The email address of the sender.
        to: The email address of the receiver.
        subject: The subject of the email message.
        message_text: The text of the email message.
        file_dir: The directory containing the file to be attached.
        filename: The name of the file to be attached.

    Returns:
        An object containing a base64 encoded email object.
    """
    message = MIMEMultipart('')
    for k, v in set_base_headers(sender, email_data):
        message[k] = v

    # text = email_data['body'].encode('utf-8')
    # part1 = MIMEText(text)
    # message.attach(part1)

    html = email_data.get('body', '').encode('utf-8')
    part2 = MIMEText(html)
    part2.set_type('text/html')
    message.attach(part2)

    for att_id in email_data['attachments']:
        att = mail_models.Attachment.objects.get(id=att_id)
        content_type, encoding = mimetypes.guess_type(att.content.file.name)

        if content_type is None or encoding is not None:
            content_type = 'application/octet-stream'
        main_type, sub_type = content_type.split('/', 1)

        att.content.open()
        if main_type == 'text':
            msg = MIMEText(att.content.read(), _subtype=sub_type)
        elif main_type == 'image':
            msg = MIMEImage(att.content.read(), _subtype=sub_type)
        elif main_type == 'audio':
            msg = MIMEAudio(att.content.read(), _subtype=sub_type)
        else:
            msg = MIMEBase(main_type, sub_type)
            msg.set_payload(att.content.read())
        att.content.close()

        msg.add_header('Content-Disposition',
                       'attachment', filename=att.filename)
        message.attach(msg)

    if email_data.get('threadId'):
        message['threadId'] = email_data['threadId']
        return {
            'raw': base64.urlsafe_b64encode(message.as_string()),
            'threadId': email_data['threadId']
        }
    else:
        return {'raw': base64.urlsafe_b64encode(message.as_string())}


def send_message(user_id, message, service):
    """Send an email message.

    Args:
        service: Authorized Gmail API service instance.
        user_id: User's email address. The special value "me"
        can be used to indicate the authenticated user.
        message: Message to be sent.

    Returns:
        Sent Message.
    """
    try:
        message = (service.users().messages().send(
            userId=user_id, body=message).execute())
        return message
    except errors.HttpError, error:
        print "Error", error
        return None


def ListMessagesMatchingQuery(user_id, service, query):
    """List all Messages of the user's mailbox matching the query.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      query: String used to filter messages returned.
      Eg.- 'from:user@some_domain.com' for Messages from a particular sender.

    Returns:
      List of Messages that match the criteria of the query. Note that the
      returned list contains Message IDs, you must use get with the
      appropriate ID to get the details of a Message.
    """
    try:
        response = service.users().messages().list(userId=user_id, q=query).execute()
        messages = []
        if 'messages' in response:
            messages.extend(response['messages'])

        while 'nextPageToken' in response:
            page_token = response['nextPageToken']
            response = service.users().messages().list(
                userId=user_id, q=query, pageToken=page_token).execute()
            messages.extend(response['messages'])

        return messages
    except errors.HttpError, error:
        print "Error", error
        return None


def ListMessagesWithLabels(user_id, service, label_ids):
    """List all Messages of the user's mailbox with label_ids applied.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      label_ids: Only return Messages with these labelIds applied.

    Returns:
      List of Messages that have all required Labels applied. Note that the
      returned list contains Message IDs, you must use get with the
      appropriate id to get the details of a Message.
    """
    try:
        response = service.users().messages().list(
            userId=user_id, labelIds=label_ids).execute()
        messages = []
        if 'messages' in response:
            messages.extend(response['messages'])

        while 'nextPageToken' in response:
            page_token = response['nextPageToken']
            response = service.users().messages().list(
                userId=user_id, labelIds=label_ids, pageToken=page_token).execute()
            messages.extend(response['messages'])

        return messages
    except errors.HttpError, error:
        print "Error", error
        return None


def ModifyMessage(service, user_id, msg_id, msg_labels):
    """Modify the Labels on the given Message.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      msg_id: The id of the message required.
      msg_labels: The change in labels.

    Returns:
      Modified message, containing updated labelIds, id and threadId.
    """
    try:
        message = service.users().messages().modify(
            userId=user_id, id=msg_id, body=msg_labels).execute()
        return message
    except errors.HttpError, error:
        print "Error", error
        return None


def CreateMsgLabels(removeLabelIds, addLabelIds):
    """Create object to update labels.

    Returns:
    A label update object.
    """
    return {'removeLabelIds': removeLabelIds, 'addLabelIds': addLabelIds}


class GmailMessage():

    def __init__(self, request_user, message):
        cred = mail_models.Credential.objects.filter(user=request_user)
        if cred and cred[0].credentials:
            cred[0].initialize()
            self.MAIL = Gmail(request_user, cred[0].service)
        self.request_user = request_user
        self.raw = message
        self.message_id = message['id']
        self.history_id = message['historyId']
        self.snippet = message['snippet']
        self.thread, _ = mail_models.Thread.objects.get_or_create(
            thread_id=message["threadId"], user=request_user)
        self.labels = [mail_models.Label.objects.get_or_create(
            label_id=label)[0] for label in message['labelIds']]

        self.message_id = message['id']
        itemd = defaultdict(list)

        for item in message['payload']['headers']:
            itemd[item['name']].append(item['value'])

        self.headers = itemd
        self.to = self.save_email(self.headers['To'])
        self.received_on = dateutil.parser.parse(self.headers['Date'][0])
        self.from_contact = self.save_email(self.headers['From'])[0]
        self.cc = self.save_email(self.headers.get('Cc', []))
        self.bcc = self.save_email(self.headers.get('Bcc'))
        self.subject = self.headers['Subject'][0]
        self.sent_on = dateutil.parser.parse(
            itemd['Date'][0].split(",")[1].strip().split(' +')[0]
        )
        if self.headers.get('In-Reply-To'):
            self.in_reply_to = self.headers['In-Reply-To'][0]
        if self.headers.get('References'):
            self.references = self.headers['References'][0]

        self.body = ""
        self.html = ""
        self.attachments = []
        self.process_payload(message['payload'])
        self.save()

    def save_email(self, email_string):
        if not email_string:
            return []
        abks = []
        emails = email_string[0].split(',')
        eregex = re.compile(r"([a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*(@|\sat\s)"
                            "(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?(\.|\sdot\s))+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)")
        if not isinstance(emails, list):
            emails = [emails]

        for mail in emails:
            op = re.findall(eregex, mail)
            if op:
                clean_email = op[0][0]
            else:
                continue
            # Assume that after the email is extracted, whats left is the name
            name = mail.replace("<", "").replace(
                ">", "").replace(clean_email, "").strip()
            abk, _ = mail_models.AddressBook.objects.get_or_create(
                email=clean_email)
            abk.name = name.title() if name else abk.name if abk.name else clean_email.split('@')[0]
            abk.save()
            abks.append(abk)

        return abks

    def process_payload(self, payload):
        if payload['filename']:
            self.process_attachment(payload)

        if 'plain' in payload['mimeType']:
            self.process_body(payload)

        if 'html' in payload['mimeType']:
            self.process_html(payload)

        if payload.get('parts') and len(payload['parts']) > 0:
            for payload_parts in payload['parts']:
                self.process_payload(payload_parts)

    def process_body(self, payload):
        self.body = base64.urlsafe_b64decode(
            payload["body"]["data"].encode("utf-8"))

    def process_html(self, payload):
        self.html = base64.urlsafe_b64decode(
            payload["body"].get("data", '').encode("utf-8"))

    def process_attachment(self, payload):
        content = self.MAIL.get_attachment_content(
            self.message_id, payload['body']['attachmentId'])
        filename = payload['filename']
        att = mail_models.Attachment(
            user=self.request_user,
            filename=filename,
            mime_type=payload['mimeType'],
            attachment_id=payload['body']['attachmentId'],
            data=payload
        )
        att.content.save(filename, ContentFile(content), save=True)
        self.attachments.append(att)

    def save(self):
        mail = mail_models.Mail.objects.filter(message_id=self.message_id)
        if mail:
            mail = mail[0]
            return mail
        mail = mail_models.Mail(
            message_id=self.message_id,
            thread=self.thread,
            user=self.request_user,
            subject=self.subject,
            snippet=self.snippet,
            body=self.body,
            html=self.html,
            raw_message_data=self.raw,
            from_contact=self.from_contact,
            received_on=self.received_on
        )
        mail.save()
        mail.labels.add(*self.labels)
        mail.to.add(*self.to)
        mail.cc.add(*self.cc)
        mail.bcc.add(*self.bcc)
        mail.attachments.add(*self.attachments)
        return mail


def gmail_watch(user_id):
    cred = mail_models.Credential.objects.filter(user__id=user_id)
    if cred and cred[0].credentials:
        cred = cred[0]
        cred.initialize()
        service = cred.service
        req = {
            'labelIds': ['INBOX'],
            'topicName': 'projects/coverfox-insurance/topics/mail'
        }
        resp = service.users().watch(userId='me', body=req).execute()
        expiration_timestamp = resp['expiration']
        expiration_datetime = datetime.datetime.fromtimestamp(
            int(expiration_timestamp) / 1000)
        celery_mail_app.gmail_watch.apply_async(
            [user_id], eta=expiration_datetime)
        print expiration_datetime


def gmail_stop_watch(user_id):
    cred = mail_models.Credential.objects.filter(user__id=user_id)
    if cred and cred[0].credentials:
        cred[0].initialize()
        service = cred[0].service
        service.users().stop(userId='me').execute()
