from __future__ import absolute_import

import uuid

import sendgrid
from django.conf import settings
from django.db.models import Q
from datetime import datetime

from .. import models as mail_models
from core import models as core_models


class SendGrid():

    def __init__(self, request):
        self.sg = sendgrid.SendGridClient(settings.SENDGRID_API_KEY)
        self.request = request

    def get_unread(self):
        return list(mail_models.Mail.objects.filter(user=self.request.user, is_unread=True))

    def get_mails(self, label_ids):
        return list(mail_models.Mail.objects.filter(user=self.request.user, labels__label_id__in=label_ids).distinct())

    def send_mail(self, email_data):
        message = sendgrid.Mail()

        # example
        # message.add_to('example@email.com') or
        # message.add_to('Example Dude <example@email.com>') or
        # message.add_to(['Example Dude <example@email.com>', 'john@email.com']) or
        # message.add_to('example@email.com')
        # message.add_to_name('Example Dude') [Optional]
        for email in email_data['to_email']:
            message.add_to(email['email'])
            message.add_to_name('name')

        # example
        # message.set_from('example@email.com')
        # message.set_from_name('Example Dude') [Optional]
        message.set_from(email_data['from_email'])
        message.set_from_name(email_data['from_name'])

        # example
        # message.set_subject('Example')
        message.set_subject(email_data['subject'])

        # example
        # message.set_text('Body')
        message.set_text(email_data['body']['plain_text'])

        # example
        # message.set_html('<html><body>Stuff, you know?</body></html>')
        message.set_html(email_data['body']['html'])

        if 'cc' in email_data:
            # example
            # message.add_cc('example@email.com') or
            # message.add_cc(['example@email.com', 'john@email.com'])
            message.add_cc(email_data['cc'])

        if 'bcc' in email_data:
            # example
            # message.add_bcc('example@email.com') or
            # message.add_bcc(['Example Dude <example@email.com>', 'john@email.com'])
            message.add_bcc(email_data['bcc'])

        if 'reply_to' in email_data:
            # example
            # message.set_replyto('example@email.com')
            message.set_replyto(email_data['reply_to'])

        if 'headers' in email_data:
            # example
            # message.set_headers({'X-Sent-Using': 'SendGrid-API', 'X-Transport': 'web'});
            message.set_headers(email_data['headers'])

        if 'date' in email_data:
            # example
            # message.set_date('Wed, 17 Dec 2014 19:21:16 +0000')
            message.set_date(email_data['date'])

        if 'attachment' in email_data:
            # example
            # message.add_attachment('stuff.txt', './stuff.txt') or
            # message.add_attachment('stuff.txt', open('./stuff.txt', 'rb'))
            file_name = email_data['attachment']['file_name']
            file_path = email_data['attachment']['file_path']
            message.add_attachment(file_name, file_path)

        status, msg = self.sg.send(message)

        return status, msg


def parse_sendgrid_email(rdata):
    """
    Parsing mail pushed by SendGrid
    """
    # TODO: checking keys for CC, BCC, from_name, to_name and other parameters
    # received

    # Get some header information
    to_address = rdata['to']
    cc_address = rdata['cc']
    from_address = rdata['from']

    # Now, onto the body
    html = rdata.get('html')
    subject = rdata.get('subject')

    # Process the attachements, if any
    num_attachments = int(rdata.get('attachments', [0])[0])
    attachments = []
    if num_attachments > 2:
        for num in range(1, (num_attachments + 1)):
            attachment = rdata.get(('attachment{}'.format(num)))
            attachments.append(attachment.read())
            # attachment will have all the parameters expected in a Flask file
            # upload

    return {
        'to_email': to_address,
        'cc_email': cc_address,
        'to_name': 'To name',
        'from_email': from_address,
        'from_name': 'From name',
        'subject': subject,
        'attachments': attachments,
        'body': {
            'html': html,
        }
    }


def save_mail(email_data):

    # TODO change this to request.user later
    user = core_models.User.objects.get(username='meeta')

    subject_startswith = ('Re:', 'Fwd:')

    from_address, is_fa_created = mail_models.AddressBook.objects.get_or_create(
        email=email_data['from_email'],
    )
    from_address.defaults = {'name': email_data[
        'from_name'], 'email': email_data['from_email']}
    from_address.save()
    to_address, is_ta_created = mail_models.AddressBook.objects.get_or_create(
        email=email_data['to_email'],
    )
    to_address.defaults = {'name': email_data[
        'to_name'], 'email': email_data['to_email']}
    to_address.save()
    generate_new_thread = False

    # Get Mail and Thread
    if not is_fa_created and not is_ta_created:

        # Generate query
        if email_data['subject'].startswith(subject_startswith):
            subject = email_data['subject'].split(':', 1)[1].strip()
            q = (
                (Q(subject__exact=subject) | Q(subject__exact=email_data['subject'])) &
                (
                    (Q(from_contact__exact=from_address) & Q(to__exact=to_address)) |
                    (Q(from_contact__exact=to_address) & Q(to__exact=from_address))
                )
            )
        else:
            q = Q(subject__exact=email_data['subject']) & Q(
                from_contact__exact=from_address) & Q(to__exact=to_address)

        # Get Thread
        try:
            mail = mail_models.Mail.objects.filter(q).order_by('-created_on')[0]
            thread = mail.thread
        except IndexError:
            generate_new_thread = True
    else:
        generate_new_thread = True

    # Generate new Thread
    if generate_new_thread:
        thread_data = {
            'thread_id': str(uuid.uuid4()),
            'user': user,
        }
        thread = mail_models.Thread.objects.create(**thread_data)

    # Create Mail
    mail = mail_models.Mail(
        subject=email_data['subject'],
        message_id=str(uuid.uuid4()),
        thread=thread,
        user=user,
        html=email_data['body']['html'],
        from_contact=from_address,
        raw_message_data=email_data,
        received_on=datetime.now(),
    )
    mail.save(update=False)
    mail.to.add(to_address)

    return mail
