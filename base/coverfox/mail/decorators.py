from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.conf import settings

from backends import gmail as gmail_utils
from backends import sendgrid as sendgrid_utils
import models as mail_models


def mail_auth_required(function):
    def _inner(request, *args, **kwargs):
        if settings.MAIL_APP_BACKEND == 'gmail':
            cred = mail_models.Credential.objects.filter(user=request.user)
            if not cred:
                return redirect(reverse('login'))
            cred = cred[0]
            if not cred.credentials:
                return redirect(reverse('login'))
            cred.initialize()
            request.MAIL = gmail_utils.Gmail(request.user, cred.service)
        elif settings.MAIL_APP_BACKEND == 'sendgrid':
            request.MAIL = sendgrid_utils.SendGrid(request)
        else:
            raise AssertionError('Mail Backend Not Defined')
        return function(request, *args, **kwargs)
    return _inner
