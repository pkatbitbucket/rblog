from django import forms
import models as mail_models


class AttachmentForm(forms.ModelForm):

    class Meta:
        model = mail_models.Attachment
        exclude = ('attachment_id', 'data')
