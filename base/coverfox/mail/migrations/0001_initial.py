# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import cfutils.knockout.djson
import jsonfield.fields
import coverfox.mail.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AddressBook',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('email', models.EmailField(unique=True, max_length=254)),
                ('data', jsonfield.fields.JSONField()),
            ],
            options={
                'djson_exclude': ['data'],
            },
            bases=(models.Model, cfutils.knockout.djson.Djson),
        ),
        migrations.CreateModel(
            name='Attachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('attachment_id', models.TextField(null=True, blank=True)),
                ('filename', models.TextField()),
                ('content', models.FileField(
                    upload_to=coverfox.mail.models.generate_attachment_name)),
                ('mime_type', models.CharField(
                    max_length=100, null=True, blank=True)),
                ('data', jsonfield.fields.JSONField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'djson_exclude': ['data', 'user'],
            },
            bases=(models.Model, cfutils.knockout.djson.Djson),
        ),
        migrations.CreateModel(
            name='Credential',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('credentials', jsonfield.fields.JSONField()),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Label',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('label_id', models.CharField(unique=True, max_length=50)),
            ],
            options={
                'djson_exclude': ['data'],
            },
            bases=(models.Model, cfutils.knockout.djson.Djson),
        ),
        migrations.CreateModel(
            name='Mail',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('message_id', models.CharField(unique=True, max_length=200)),
                ('subject', models.TextField()),
                ('snippet', models.CharField(max_length=200, null=True)),
                ('body', models.TextField(null=True, blank=True)),
                ('html', models.TextField(null=True, blank=True)),
                ('raw_message_data', jsonfield.fields.JSONField()),
                ('is_deleted', models.BooleanField(default=False)),
                ('is_unread', models.BooleanField(default=True)),
                ('is_pinned', models.BooleanField(default=False)),
                ('is_priority', models.BooleanField(default=False)),
                ('is_draft', models.BooleanField(default=False)),
                ('received_on', models.DateTimeField()),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('data', jsonfield.fields.JSONField(blank=True)),
                ('attachments', models.ManyToManyField(to='mail.Attachment')),
                ('bcc', models.ManyToManyField(
                    related_name='bcc_emails', to='mail.AddressBook')),
                ('cc', models.ManyToManyField(
                    related_name='cc_emails', to='mail.AddressBook')),
                ('from_contact', models.ForeignKey(
                    related_name='from_emails', to='mail.AddressBook')),
                ('labels', models.ManyToManyField(to='mail.Label')),
            ],
            options={
                'djson_exclude': ['data', 'raw_message_data'],
            },
            bases=(models.Model, cfutils.knockout.djson.Djson),
        ),
        migrations.CreateModel(
            name='Thread',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('thread_id', models.CharField(unique=True, max_length=100)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('data', jsonfield.fields.JSONField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            bases=(models.Model, cfutils.knockout.djson.Djson),
        ),
        migrations.AddField(
            model_name='mail',
            name='thread',
            field=models.ForeignKey(to='mail.Thread', null=True),
        ),
        migrations.AddField(
            model_name='mail',
            name='to',
            field=models.ManyToManyField(
                related_name='to_emails', to='mail.AddressBook'),
        ),
        migrations.AddField(
            model_name='mail',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
