# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import djorm_pgfulltext.fields


class Migration(migrations.Migration):

    dependencies = [
        ('mail', '0002_auto_20151219_1757'),
    ]

    operations = [
        migrations.AddField(
            model_name='mail',
            name='mail_contacts',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mail',
            name='search_index',
            field=djorm_pgfulltext.fields.VectorField(default=b'', serialize=False, null=True, editable=False, db_index=True),
        ),
    ]
