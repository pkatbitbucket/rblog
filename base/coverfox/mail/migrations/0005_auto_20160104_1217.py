# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mail', '0004_thread_modified_on'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mail',
            name='thread',
            field=models.ForeignKey(related_name='mails', to='mail.Thread', null=True),
        ),
    ]
