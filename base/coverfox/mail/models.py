import uuid
import os
import json
import copy
import logging
import datetime
import httplib2
from collections import defaultdict
from oauth2client.client import OAuth2Credentials
from apiclient.discovery import build
from django.db import models
from jsonfield import JSONField
from django.conf import settings
from djorm_pgfulltext.models import SearchManager
from djorm_pgfulltext.fields import VectorField

from cfutils.knockout import djson
from cfutils.redis_utils import Publisher


def generate_attachment_name(instance, filename):
    _, ext = os.path.splitext(filename)
    newfilename = "%s.%s" % (uuid.uuid4(), ext)
    path = "attachments/%s/%s" % (instance.user.id, newfilename)
    return path


class Credential(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    credentials = JSONField()

    def __init__(self, *args, **kwargs):
        self._credentials = None
        self.service = False
        super(Credential, self).__init__(*args, **kwargs)

    def initialize(self):
        """ To use Credential service, call initialize first"""
        self._credentials = self._get_credentials()
        self.service = self._get_service()
        if not self.service:
            raise Exception("Cannot create gmail service")
        return self.service

    def _get_credentials(self):
        time_left = datetime.datetime.strptime(
            self.credentials['token_expiry'], "%Y-%m-%dT%H:%M:%SZ"
        ) - datetime.datetime.utcnow()

        if time_left.total_seconds() < 600:
            self.credentials = self.refresh_credentials(
                self.clean_credentials())
            self.save()

        return self.clean_credentials()

    def clean_credentials(self):
        c = copy.deepcopy(self.credentials)
        for j in ['_module', '_class', 'invalid']:
            if j in c:
                c.pop(j)
        return c

    def _get_service(self):
        """ return the service for further api calls using credentials """
        if not self._credentials:
            self._credentials = self._get_credentials()
        service = self.authenticate_credentials(self._credentials)
        return service

    def refresh_credentials(self, c):
        print "Getting new credentials......."
        credentials = OAuth2Credentials(**c)
        http_obj = httplib2.Http()
        credentials.refresh(http_obj)
        return json.loads(credentials.to_json())

    def text(self):
        return self.name + ' ' + self.email

    def authenticate_credentials(self, credentials):
        logging.basicConfig(filename='debug.log', level=logging.DEBUG)
        credentials = OAuth2Credentials(**credentials)
        http_obj = httplib2.Http()
        http = credentials.authorize(http_obj)
        gmail_service = build('gmail', 'v1', http=http)
        return gmail_service


class Label(models.Model, djson.Djson):
    label_id = models.CharField(max_length=50, unique=True)

    def __unicode__(self):
        return self.label_id

    class Meta:
        djson_exclude = ['data']


class Thread(models.Model, djson.Djson):
    thread_id = models.CharField(max_length=100, unique=True)
    created_on = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    modified_on = models.DateTimeField(auto_now=True)

    data = JSONField()

    def __unicode__(self):
        return self.thread_id


class Attachment(models.Model, djson.Djson):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    attachment_id = models.TextField(blank=True, null=True)
    filename = models.TextField()
    content = models.FileField(upload_to=generate_attachment_name)
    mime_type = models.CharField(blank=True, null=True, max_length=100)
    data = JSONField()

    def __unicode__(self):
        return self.filename

    class Meta:
        djson_exclude = ['data', 'user']


class AddressBook(models.Model, djson.Djson):
    name = models.CharField(max_length=100)
    email = models.EmailField(unique=True)
    data = JSONField()

    def __unicode__(self):
        return '"%s"<%s>' % (self.name, self.email)

    class Meta:
        djson_exclude = ['data']


class Mail(models.Model, djson.Djson):

    MAIL_TYPE_CHOICES = (
        ('gmail', 'GMAIL'),
        ('sendgrid', 'SENDGRID'),
    )

    labels = models.ManyToManyField(Label)
    message_id = models.CharField(max_length=200, unique=True)
    thread = models.ForeignKey(Thread, null=True, related_name="mails")

    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="user_mails")
    subject = models.TextField()
    snippet = models.CharField(max_length=200, null=True)
    body = models.TextField(blank=True, null=True)
    html = models.TextField(blank=True, null=True)
    to = models.ManyToManyField(AddressBook, related_name="to_emails")
    cc = models.ManyToManyField(AddressBook, related_name="cc_emails")
    bcc = models.ManyToManyField(AddressBook, related_name="bcc_emails")
    from_contact = models.ForeignKey(AddressBook, related_name="from_emails")

    attachments = models.ManyToManyField(Attachment)
    raw_message_data = JSONField()

    is_deleted = models.BooleanField(default=False)
    is_unread = models.BooleanField(default=True)
    is_pinned = models.BooleanField(default=False)
    is_priority = models.BooleanField(default=False)
    is_draft = models.BooleanField(default=False)

    received_on = models.DateTimeField()
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)

    data = JSONField(blank=True)

    mail_contacts = models.TextField(null=True, blank=True)

    search_index = VectorField()

    objects = SearchManager(
        fields=('subject', 'body', 'mail_contacts'),
        config='pg_catalog.english',  # this is default
        search_field='search_index',  # this is default
        auto_update_search_field=True
    )

    # mail_type = models.CharField(choices=MAIL_TYPE_CHOICES, max_length=20, default=None)

    class Meta:
        djson_exclude = ['data', 'raw_message_data']

    def __unicode__(self):
        return "%s" % self.subject

    def mini_json(self, detail=False):
        d = {
            'id': str(self.id),
            'message_id': self.message_id,
            'subject': self.subject,
            'snippet': self.snippet,
            'to': [t.djson() for t in self.to.all()[:3]],
            'to_count': self.to.count(),
            'cc': [t.djson() for t in self.cc.all()[:3]],
            'cc_count': self.cc.count(),
            'from_contact': self.from_contact.djson(),
            'thread': {'id': self.thread.id},
            'attachment_count': self.attachments.count(),
            'is_deleted': self.is_deleted,
            'is_unread': self.is_unread,
            'is_pinned': self.is_pinned,
            'is_priority': self.is_priority,
            'is_draft': self.is_draft,
            'labels': [label.djson() for label in self.labels.all()],
            'created_on': self.created_on.isoformat(),
            'received_on': self.received_on.isoformat()
        }
        if detail:
            d.update({
                'body': self.body,
                'html': self.html,
                'attachments': [att.djson() for att in self.attachments.all()],
                'to': [t.djson() for t in self.to.all()],
                'cc': [t.djson() for t in self.cc.all()],
            })
        return d

    def publish(self):
        Publisher.update_object(self)
        Publisher.publish_object("user_%s" % self.user.id, self)

    def save(self, update=True, *args, **kwargs):
        if self.pk:
            contact_fields = ['to', 'cc', 'bcc']
            contacts_list = []
            for field in contact_fields:
                contacts_list.extend([item[0] + ' ' + item[1] for item in getattr(self, field).values_list('name', 'email')])
            self.mail_contacts = ' '.join(contacts_list)
            self.mail_contacts += (self.from_contact.name + ' ' + self.from_contact.email)
        super(Mail, self).save(*args, **kwargs)
        if update:
            self.publish()

    def get_headers(self):
        raw = self.raw_message_data
        headers = defaultdict(list)
        for item in raw['payload']['headers']:
            headers[item['name']].append(item['value'])
        return headers
