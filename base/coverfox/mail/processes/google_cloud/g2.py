import os
import json
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()

import g1
from coverfox.mail.backends import gmail
from coverfox.mail import models as mail_models
from core import models as core_models

import base64

client = g1.create_pubsub_client()

# You can fetch multiple messages with a single API call.
batch_size = 100

subscription = 'projects/coverfox-insurance/subscriptions/jarvis'

# Create a POST body for the Pub/Sub request
body = {
    # Setting ReturnImmediately to false instructs the API to wait
    # to collect the message up to the size of MaxEvents, or until
    # the timeout.
    'returnImmediately': False,
    'maxMessages': batch_size,
}
while True:

    resp = client.projects().subscriptions().pull(
        subscription=subscription, body=body).execute()

    received_messages = resp.get('receivedMessages')
    if received_messages is not None:
        ack_ids = []
        for received_message in received_messages:
            pubsub_message = received_message.get('message')
            if pubsub_message:
                # Process messages
                resp = base64.b64decode(str(pubsub_message.get('data')))
                resp = json.loads(resp)
                email = resp['emailAddress']
                print resp
                cred = None
                try:
                    u = core_models.User.objects.get(email=email)
                    cred = mail_models.Credential.objects.get(user=u)
                except:
                    pass
                if cred:
                    try:
                        cred.initialize()
                        gmail.Gmail(u, cred.service).save_mails_from_history_id(
                            email, resp['historyId'])
                    except IndexError as e:
                        print e

                # Get the message's ack ID
                ack_ids.append(received_message.get('ackId'))

        # Create a POST body for the acknowledge request
        ack_body = {'ackIds': ack_ids}

        # Acknowledge the message.
        client.projects().subscriptions().acknowledge(
            subscription=subscription, body=ack_body).execute()
