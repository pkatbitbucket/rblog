import g1

pubsub = g1.create_pubsub_client()

topic = 'projects/coverfox-insurance/topics/mail'
policy = {
    'policy': {
        'bindings': [{
            'role': 'roles/pubsub.publisher',
            'members': ['serviceAccount:gmail-api-push@system.gserviceaccount.com'],
        }],
    }
}
resp = pubsub.projects().topics().setIamPolicy(
    resource=topic, body=policy).execute()
print(resp)
