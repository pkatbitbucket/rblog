from django.conf.urls import include, url

from . import views as mail_views

urlpatterns = [
    url(r'^$', mail_views.index, name='index'),
    url(r'^login/$', mail_views.login, name='login'),

    # REST urls for mails
    url(r'^mails/$', mail_views.get_mails, name='get_mails'),

    url(r'^send/$', mail_views.send_mail, name='send_mail'),
    url(r'^upload/$', mail_views.upload, name='upload'),
    url(r'^get-attachment/$', mail_views.get_attachment, name='get_attachment'),

    # TODO: Will be removed and made into a cron job or removed completely
    url(r'^prepare-mailbox/$', mail_views.prepare_mailbox, name='prepare_mailbox'),
    url(r'^get-initial-mails/$', mail_views.get_initial_mails, name='get_initial_mails'),
    url(r'^get-searched-mails/$', mail_views.get_searched_mails, name='get_searched_mails'),
    url(r'^apis/', include('coverfox.mail.apis.urls')),
]
