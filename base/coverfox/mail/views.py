import json

from django.shortcuts import render
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache
from django.conf import settings
from django.core.urlresolvers import reverse
from django.views.decorators.http import require_http_methods
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from oauth2client.client import credentials_from_code, FlowExchangeError

import models as mail_models
from .forms import AttachmentForm
from decorators import mail_auth_required
from celeryapps import mail_celery as celery_mail_app
from cfutils.decorators import allow_api_login_required
from .backends.sendgrid import save_mail, SendGrid


@login_required
@mail_auth_required
def index(request):
    return render(request, "mail/success.html", {})


@login_required
def login(request):
    return render(request, "mail/button.html")


@never_cache
@login_required
def registration(request):
    """
    Note: This code is used for registration purpose, do not change
    parameters here unless you exactly know what you are doing
    or google has changed its api.
    """
    try:
        credentials = credentials_from_code(
            client_id=settings.CONFIG['GOOGLE_API_CLIENT_ID'],
            client_secret=settings.CONFIG['GOOGLE_API_CLIENT_SECRET'],
            scope='https://mail.google.com/',
            code=request.GET['code'],
            redirect_uri=settings.CONFIG['GOOGLE_API_REDIRECT_URI']
        )
        auth_success = True
        credential_obj, created = mail_models.Credential.objects.get_or_create(
            user=request.user)
        credential_obj.credentials = json.loads(credentials.to_json())
        credential_obj.save()
    except FlowExchangeError, e:
        print "Google Exception", e
        auth_success = False

    print "Auth Success", auth_success
    return redirect(request.GET.get('next') or reverse('index'))


@login_required
@mail_auth_required
@require_http_methods(["GET"])
def prepare_mailbox(request):
    celery_mail_app.prepare_mailbox.delay(
        request.user.id, mail_models.Credential.objects.get(user__id=request.user.id).id)
    return JsonResponse({
        "success": True,
        "threads": [{
            'pk': t.id,
            'mails': [{
                'pk': m.id,
                'subject': m.subject,
                'snippet': m.snippet
            } for m in t.mail_set.all()],
        } for t in mail_models.Thread.objects.filter(user=request.user)]
    })


@login_required
@mail_auth_required
@require_http_methods(["POST"])
def send_mail(request):
    email_data = json.loads(request.POST['data'])
    email_data['labelIds'] = 'INBOX'
    if email_data.get('threadId'):
        thread = get_object_or_404(
            mail_models.Thread, pk=email_data.get('threadId'))
        email_data['threadId'] = thread.thread_id
    else:
        email_data['threadId'] = None

    if email_data.get('mailId'):
        mail = get_object_or_404(mail_models.Mail, pk=email_data.get('mailId'))
        # For threading, maintain the following parameters
        headers = mail.get_headers()
        email_data[
            'In-Reply-To'] = headers['In-Reply-To'][0] if headers.get('In-Reply-To') else None
        email_data['References'] = headers['References'][
            0] if headers.get('References') else None
        email_data[
            'Message-ID'] = headers['Message-ID'][0] if headers.get('Message-ID') else None

    email_data.update({
        'is_html': True,
        'is_attachment': True if email_data.get('attachments') else False,
    })

    mail = request.MAIL.send_mail(email_data)
    return JsonResponse({
        "success": True,
        "mail": mail.mini_json()
    })


@allow_api_login_required
@require_http_methods(["GET"])
def get_mails(request):
    mail = get_object_or_404(mail_models.Mail, pk=request.GET.get('id'))
    mail.is_unread = False
    mail.save()
    return JsonResponse(mail.mini_json(detail=True))


@allow_api_login_required
@require_http_methods(["GET"])
def get_attachment(request):
    # TODO Implement x-accel-redirect
    attachment = get_object_or_404(
        mail_models.Attachment, pk=request.GET.get('id'))
    return HttpResponseRedirect(attachment.url)


@login_required
@require_http_methods(["POST"])
@csrf_exempt
def upload(request):
    if request.FILES:
        content = request.FILES['content']
        filename = content.name
        post_data = {
            'user': request.user.id,
            'filename': filename,
        }
        aform = AttachmentForm(post_data, request.FILES)
        if aform.is_valid():
            attachment = aform.save()
            return JsonResponse({'success': True, 'attachment': attachment.djson()})
        else:
            return JsonResponse({'success': False, 'errors': aform.errors})
    else:
        return JsonResponse({'success': False})


@login_required
@require_http_methods(["POST"])
def send_sendgrid_mail(request):
    email_data = json.loads(request.POST['data'])
    status, msg = SendGrid.send_mail(email_data)
    mail = save_mail(email_data)
    return JsonResponse({'success': 'True', 'mail_data': mail.mini_json()})


@mail_auth_required
def get_searched_mails(request):
    query = request.GET['q']
    page_no = int(request.GET.get('page_no') or 1)
    paginator = Paginator(mail_models.Mail.objects.search(query), 50)
    try:
        results = paginator.page(page_no)
    except PageNotAnInteger:
        results = paginator.page(1)
    except EmptyPage:
        results = paginator.page(paginator.num_pages)
    return JsonResponse({
        'mails': [m.mini_json() for m in mail_models.Mail.objects.filter(
                pk__in=results.object_list
        ).order_by('-received_on')[:50]],
        'page_no': page_no,
        'num_pages': paginator.num_pages,
    })


@mail_auth_required
def get_initial_mails(request):
    page_no = int(request.GET.get('page_no') or 1)
    paginator = Paginator(mail_models.Thread.objects.filter(user=request.user).order_by('-modified_on'), 50)
    try:
        threads = paginator.page(page_no)
    except PageNotAnInteger:
        threads = paginator.page(1)
    except EmptyPage:
        threads = paginator.page(paginator.num_pages)
    mails = []
    [mails.extend(thread.mails.all()) for thread in threads]

    return JsonResponse({
        'mails': [mail.mini_json() for mail in mails],
        'page_no': page_no,
        'num_pages': paginator.num_pages,
    })
