from django.core.management.base import BaseCommand, CommandError
from statsd import StatsClient

from django.conf import settings
STATSD_HOST = getattr(settings, 'STATSD_HOST', 'localhost')
STATSD_PORT = getattr(settings, 'STATSD_PORT', 8125)

class Command(BaseCommand):
    help = 'Ping STATSD Server after each deploy'
    
    def add_arguments(self, parser):
        parser.add_argument('--test',
                action='store_true',
                dest='test',
                default=False,
                help='Send a test ping')

    def handle(self, *args, **options):
        if options['test']:
            event = 'deploy.coverfox.test'
        else:
            event = 'deploy.coverfox'
        StatsClient(host=STATSD_HOST, port=STATSD_PORT).incr(event)
        self.stdout.write('Successfully pinged deploy event to %s.' % STATSD_HOST)
