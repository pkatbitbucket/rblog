# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'\n        The name of the company. This is user visible name, there may be\n        duplicates, but we do not care.\n\n        This is not indexed.\n        ', max_length=200)),
                ('slug', models.SlugField(help_text=b'\n        This must never change. Slug will be used as company subdomain. Would\n        we have coverfox.coverfox.com someday? :-)\n        ', unique=True)),
                ('created_on', models.DateTimeField(default=datetime.datetime.now)),
            ],
        ),
        migrations.CreateModel(
            name='CompanyMembership',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default=b'', help_text=b'\n        The company specific position of the employee. Eg VP Engineering.\n\n        No validations on this.\n        ', max_length=200, blank=True)),
                ('joined_on', models.DateField(default=datetime.datetime.today, help_text=b'\n        This can get complicated eventually, a person may accept a position on\n        one date, and join on another. For now this field means the date from\n        which the employee starts getting salary.\n        ')),
                ('eid', models.CharField(default=b'', help_text=b'\n        Employee ID of the company. This is company specific. Ideally we can\n        have a database constraint of unique together (company, eid), but for\n        now we are not adding it.\n        ', max_length=200, blank=True)),
                ('left_on', models.DateField(help_text=b'\n        This too is complicated. For now this means the last date, when salary\n        stops. Full and final settlement may happen later. Ideally a person\n        should be visible, to HR etc, till FNF. We will capture FNF through\n        another field when needed.\n        ', null=True, blank=True)),
                ('company', models.ForeignKey(help_text=b'\n        The company. An employee can possibly be part of same company multiple\n        times, like I had two stints with BrowserStack. So we can not put a\n        unique together on (user, company) tuple, we must do manual validation.\n        ', to='coverfox.Company')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='company',
            name='employees',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, through='coverfox.CompanyMembership'),
        ),
    ]
