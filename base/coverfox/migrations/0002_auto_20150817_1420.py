# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coverfox', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='companymembership',
            name='left_on',
            field=models.DateField(default=None, help_text=b'\n        This too is complicated. For now this means the last date, when salary\n        stops. Full and final settlement may happen later. Ideally a person\n        should be visible, to HR etc, till FNF. We will capture FNF through\n        another field when needed.\n        ', null=True, blank=True),
        ),
    ]
