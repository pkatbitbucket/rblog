# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coverfox', '0002_auto_20150817_1420'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='companymembership',
            unique_together=set([('company', 'user', 'joined_on'), ('company', 'user', 'left_on')]),
        ),
    ]
