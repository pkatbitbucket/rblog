# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import autoslug.fields
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('coverfox', '0003_auto_20150901_1242'),
    ]

    operations = [
        migrations.RenameField(
            model_name='companymembership',
            old_name='eid',
            new_name='employee_id',
        ),
        migrations.RemoveField(
            model_name='company',
            name='created_on',
        ),
        migrations.AddField(
            model_name='company',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 8, 14, 5, 19, 727850), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='company',
            name='modified_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 8, 14, 5, 30, 73259), auto_now=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='company',
            name='slug',
            field=autoslug.fields.AutoSlugField(populate_from=b'name', editable=False),
        ),
    ]
