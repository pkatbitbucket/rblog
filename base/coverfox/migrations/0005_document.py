# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import coverfox.models
import core.models


class Migration(migrations.Migration):

    dependencies = [
        ('coverfox', '0004_auto_20150908_1405'),
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=255, blank=True)),
                ('file', models.FileField(upload_to=coverfox.models.get_document_upload_path)),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
    ]
