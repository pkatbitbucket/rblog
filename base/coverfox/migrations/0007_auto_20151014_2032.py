# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
import core.models


class Migration(migrations.Migration):

    dependencies = [
        ('coverfox', '0006_auto_20151009_1613'),
    ]

    operations = [
        migrations.CreateModel(
            name='Doodle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=255)),
                ('label', models.CharField(max_length=255, blank=True)),
                ('data', jsonfield.fields.JSONField(default={})),
            ],
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.AlterUniqueTogether(
            name='doodle',
            unique_together=set([('name', 'label')]),
        ),
    ]
