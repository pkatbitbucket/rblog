from django.conf import settings
from django.db import models

from core.models import BaseModel
from datetime import datetime
from jsonfield import JSONField

import os

User = settings.AUTH_USER_MODEL


class Company(BaseModel):
    """
    Any Company is represented by this model.

    An Company has many admins. But from employee benefit point of view we may
    have a different set of admin, compared to something else. Also employees
    have other relationships, like who they are reporting to, who their peers
    are, all things needed for HR functions, but we are not capturing them here,
    we will have separate apps for them.

    The company can be in different "states", at least if we should consider it
    active or not.
    """
    name = models.CharField(
        max_length=200, help_text="""
        The name of the company. This is user visible name, there may be
        duplicates, but we do not care.

        This is not indexed.
        """
    )
    slug = models.SlugField(
        # TODO: some slugs are not allowed (eg ones containing _, or "www" etc)
        help_text="""
        Domain name of company.
        """
    )
    employees = models.ManyToManyField(User, through='CompanyMembership')


class CompanyMembership(models.Model):
    company = models.ForeignKey(
        Company, help_text="""
        The company. An employee can possibly be part of same company multiple
        times, like I had two stints with BrowserStack. So we can not put a
        unique together on (user, company) tuple, we must do manual validation.
        """
    )
    user = models.ForeignKey(User)
    title = models.CharField(
        max_length=200, blank=True, default="", help_text="""
        The company specific position of the employee. Eg VP Engineering.

        No validations on this.
        """
    )
    joined_on = models.DateField(
        default=datetime.today, blank=False, help_text="""
        This can get complicated eventually, a person may accept a position on
        one date, and join on another. For now this field means the date from
        which the employee starts getting salary.
        """
    )
    employee_id = models.CharField(
        max_length=200, blank=True, default="", help_text="""
        Employee ID of the company. This is company specific. Ideally we can
        have a database constraint of unique together (company, eid), but for
        now we are not adding it.
        """
    )
    left_on = models.DateField(
        null=True, blank=True, default=None, help_text="""
        This too is complicated. For now this means the last date, when salary
        stops. Full and final settlement may happen later. Ideally a person
        should be visible, to HR etc, till FNF. We will capture FNF through
        another field when needed.
        """
    )

    class Meta:
        unique_together = (
            ("company", "user", "left_on", ),
            ("company", "user", "joined_on", ),
        )

    def __unicode__(self):
        return "%s(current=%s): %s" % (
            self.user, self.left_on is None, self.company
        )


def get_document_upload_path(self, filename):
    return self.get_document_upload_path(filename)


class Document(BaseModel):
    name = models.CharField(max_length=255, blank=True)
    file = models.FileField(upload_to=get_document_upload_path)

    def get_document_upload_path(self, filename):
        fname, ext = os.path.splitext(filename)
        r = lambda s: s.replace(' ', '_')
        return os.path.join(
            self._meta.app_label,
            r(str(self._meta.verbose_name_plural)),
            r(self.name + ext),
        )

    def save(self, *args, **kwargs):
        if not self.name:
            self.name = os.path.splitext(self.file.name)[0]
        return super(Document, self).save(*args, **kwargs)

    def __unicode__(self):
        return '%s' % (self.name, )


class DoodleManager(models.Manager):
    def get_queryset(self):
        return super(DoodleManager, self).get_queryset().filter(
            label='%s_%s' % (
                self.model._meta.app_label, self.model._meta.model_name,
            )
        )


class Doodle(BaseModel):
    name = models.CharField(max_length=255)
    label = models.CharField(max_length=255, blank=True)
    data = JSONField(default={})

    objects = DoodleManager()

    def save(self, *args, **kwargs):
        self.label = '%s_%s' % (self._meta.app_label, self._meta.model_name, )
        return super(Doodle, self).save(*args, **kwargs)

    class Meta:
        unique_together = (
            ("name", "label", ),
        )
