from django.core.serializers.json import DjangoJSONEncoder
from django.db.models.query import QuerySet, ValuesQuerySet

import uuid


class DjangoQuerysetJSONEncoder(DjangoJSONEncoder):
    def default(self, value):
        if isinstance(value, ValuesQuerySet):
            return [v for v in value]
        elif isinstance(value, QuerySet) and hasattr(value.model, 'natural_key') and callable(value.model.natural_key):
            natural_keys = []
            for v in value:
                natural_keys.append(', '.join(v.natural_key()))

            return natural_keys
        elif isinstance(value, uuid.UUID):
            return str(value)
        else:
            return super(DjangoQuerysetJSONEncoder, self).default(value)
