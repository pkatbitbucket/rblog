from django import forms
from django.contrib import admin
import django.db.models

from cms.common.admin import BaseAdmin

from crm import models as crm_models


class QueueAdvisorInline(admin.TabularInline):
    model = crm_models.QueueAdvisor
    raw_id_fields = ('advisor',)
    exclude = ('creator', 'editor',)
    extra = 0


class DialerQueueInline(admin.TabularInline):
    model = crm_models.DialerQueue.advisors.through
    extra = 0


class AdvisorAdmin(BaseAdmin):
    models = crm_models.Advisor
    raw_id_fields = ('user',)
    exclude = ('creator', 'editor', 'notes')
    inlines = (QueueAdvisorInline, DialerQueueInline,)
    search_fields = ('dialer_username', 'dialer_did', 'user__emails__email')


class QueueAdmin(BaseAdmin):
    models = crm_models.Queue
    fields = ('name', 'code')
    inlines = (QueueAdvisorInline,)
    search_fields = ('name', 'code')


class DispositionAdmin(BaseAdmin):
    models = crm_models.Disposition
    fields = ('verbose', 'value', 'sub_dispositions', 'serial')


class DispositionInline(admin.TabularInline):
    model = crm_models.Disposition.sub_dispositions.through
    extra = 0


class SubDispositionAdmin(BaseAdmin):
    models = crm_models.SubDisposition
    fields = ('verbose', 'value', 'is_active')
    inlines = [DispositionInline]


class ConditionAdmin(BaseAdmin):
    models = crm_models.Condition
    ordering = ('-id',)

    formfield_overrides = {
        django.db.models.TextField: {'widget': forms.widgets.TextInput(attrs={'style': 'width:500px'})}
    }


class QueueRouterAdmin(BaseAdmin):
    models = crm_models.QueueRouter
    exclude = ('params',)
    list_display = ('id', 'queue', 'score', 'queuerouter_conditions')
    list_editable = ('queue', 'score')
    list_filter = ('queue', 'score')
    ordering = ('-score',)

    def queuerouter_conditions(self, instance):
        return ', '.join([condition.name for condition in instance.conditions.all()])


class TaskCategoryAdmin(BaseAdmin):
    models = crm_models.TaskCategory


class DialerQueueAdmin(BaseAdmin):
    models = crm_models.DialerQueue


class MachineStateSubdispositionAdmin(BaseAdmin):
    models = crm_models.MachineStateSubdisposition
    list_display = ('id', 'machine', 'state', 'trigger', 'sub_disposition', 'is_active')
    list_editable = ('machine', 'state', 'trigger', 'sub_disposition', 'is_active')
    list_filter = ('machine', 'state', 'trigger', 'sub_disposition', 'is_active')
    list_per_page = 25


admin.site.register(crm_models.Advisor, AdvisorAdmin)
admin.site.register(crm_models.Queue, QueueAdmin)
admin.site.register(crm_models.Disposition, DispositionAdmin)
admin.site.register(crm_models.SubDisposition, SubDispositionAdmin)
admin.site.register(crm_models.Condition, ConditionAdmin)
admin.site.register(crm_models.QueueRouter, QueueRouterAdmin)
admin.site.register(crm_models.TaskCategory, TaskCategoryAdmin)
admin.site.register(crm_models.DialerQueue, DialerQueueAdmin)
admin.site.register(crm_models.MachineStateSubdisposition, MachineStateSubdispositionAdmin)
