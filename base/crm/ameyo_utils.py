from django.utils import timezone

from cfutils import redis_utils
from core import models as core_models
from crm import models as crm_models
from core.activity import create_crm_activity

AMEYO_COMMON_QUEUE_ID = '28'
AMEYO_COMMON_QUEUE_NAME = 'CarQualifierQueue'
AMEYO_QUEUE_MAP = {
    'ServiceQueue': '17',
    'CarQualifierQueue': '28',
    'CarClosureQueue': '36',
    'CarCommonQueue': '27',
    'CarOfflineQueue': '53',
    'BikeQualifierQueue': '30',
    'BikeCommonQueue': '29',
    'BikeClosureQueue': '44',
    'HealthQualifierQueue': '31',
    'HealthClosureQueue': '47',
    'HealthCommonQueue': '32',
    'TravelQualifierQueue': '33',
    'TravelClosureQueue': '43',
    'TravelCommonQueue': '34',
    'TermQualifierQueue': '54',
    'TestingQueue': '52',
}
PRODUCT_QUEUES = {
    'car': 'CarQualifierQueue',
    'bike': 'BikeQualifierQueue',
    'health': 'HealthQualifierQueue',
    'travel': 'TravelQualifierQueue',
    'term': 'TermQualifierQueue',
    'service': 'ServiceQueue',
}

MISSED_CALL_CAMPAIGN_DIDS = {
    '2011': {
        'campaign': 'motor-offlinecases',
        'source': 'affiliate-incoming',
    }
}
DID_MAP = {
    '5000': {'tollfree': 'SIM', 'product': 'health'},
    '2000': {'tollfree': 'SIM', 'product': 'car'},
    '2020': {'tollfree': 'SIM', 'product': 'car'},
    '3900': {'tollfree': 'SIM', 'product': 'car'},
    '2010': {'tollfree': 'SIM', 'product': 'service'},
    '1123': {'tollfree': 'BLASTER', 'product': 'car'},
    '5001': {'tollfree': '18002099920', 'product': 'health'},
    '5002': {'tollfree': '18002099930', 'product': 'car'},
    '5003': {'tollfree': '18002099940', 'product': 'bike'},
    '2001': {'tollfree': '18002099950', 'product': 'travel'},
    '2002': {'tollfree': '18002099960', 'product': 'term'},
    '2003': {'tollfree': '18002099970', 'product': 'service'},
    '2004': {'tollfree': 'waybeo', 'product': 'car'},
}
PRODUCT_DEFAULT_DIDS = {
    'car': '2020',
    'health': '5000',
    'service': '2010',
}


class IncomingCallRouter(object):
    """Return primary caller, secondary caller and common queue to route the call to.
    Args:
        mobile: mobile number of the customer
        crt_obj_id: crtObjectId of the call
        did: DID on which the call has been made
    common queue is decided from the queue that the primary caller is part of
    """

    def __init__(self, mobile, did, crt_obj_id):
        self.mobile = mobile
        self.did = did
        self.crt_obj_id = crt_obj_id
        self.customer = self._get_customer()
        self.called_user, self.product_type = self._get_called_user_and_product_type()
        redis_utils.Publisher.rclient.setex("crtObjId_%s" % self.crt_obj_id, "%s_%s" % (self.did, self.product_type.lower()), 2000)

    def get_callers(self):
        # ## Hack so that blaster campaign callbacks don't hit advisory team
        if self.did == '4800':
            pc = crm_models.Advisor.objects.get(dialer_username='test')
            return pc, pc, AMEYO_QUEUE_MAP.get('TestingQueue')

        if self.did in MISSED_CALL_CAMPAIGN_DIDS:
            rdata = MISSED_CALL_CAMPAIGN_DIDS.get(self.did)
            rdata.update({
                'mobile': self.mobile,
                'product': self.product_type,
                'category': self.did,
                'source': 'INCOMING'
            })
            adata = {
                'user': crm_models.Advisor.objects.get(dialer_username='system'),
                'category': 'INCOMING_CALL',
                'data': rdata,
            }
            activity = create_crm_activity(data=adata)
            activity.end_time = timezone.now()
            activity.save()

            pc = crm_models.Advisor.objects.get(dialer_username='test')
            return pc, pc, AMEYO_QUEUE_MAP.get('TestingQueue')
        pc, sc, dialer_queue = self._route_to_sales()
        queue_id = AMEYO_QUEUE_MAP.get(dialer_queue.name, AMEYO_COMMON_QUEUE_ID)
        return pc, sc, queue_id

    def _route_to_sales(self):
        primary_caller = self.called_user
        queue = None
        if primary_caller:
            queue = primary_caller.get_preferred_dialer_queue()
        if not queue:
            queue = crm_models.DialerQueue.objects.get(name=PRODUCT_QUEUES.get(self.product_type))
        if not queue:
            queue = crm_models.DialerQueue.objects.get(name=AMEYO_COMMON_QUEUE_NAME)
        if not primary_caller:
            req = None
            if self.customer:
                req = self.customer.requirements.last()
            primary_caller = req.owner if req else None
        primary_caller = primary_caller or queue.advisors.first()
        secondary_caller = queue.advisors.last()
        return primary_caller, secondary_caller, queue

    def _get_called_user_and_product_type(self):
        product_type = 'car'
        called_user = None
        if self.did in DID_MAP:
            product_type = DID_MAP[self.did]['product']
        else:
            called_username = redis_utils.Publisher.rclient.get('did_%s' % self.did)
            called_user = None if not called_username else crm_models.Advisor.objects.get(dialer_username=called_username)
            product_type = called_user.get_preferred_product_type() if called_user else product_type
        return called_user, product_type

    def _get_customer(self):
        phone = core_models.Phone.objects.filter(number=self.mobile).last()
        if not phone:
            return None
        return phone.user if phone.user else phone.visitor


class MissedCallHandler(object):

    def __init__(self, mobile, crt_obj_id):
        self.mobile = mobile
        self.crt_obj_id = crt_obj_id
        self.did, self.product_type = self._get_did_product_type()
        self.customer = self._get_customer()

    def handle_missed_call(self):
        if (
            self.did == '4800'
            or self.did in MISSED_CALL_CAMPAIGN_DIDS
            or self.product_type == 'service'
        ):
            return True
        self._handle_sales()

    def _handle_sales(self):
        ad_category = 'SIM'
        if self.did:
            ad_category = 'untracked'
        if self.did in DID_MAP:
            ad_category = DID_MAP[self.did]['tollfree']

        adata = {
            'user': crm_models.Advisor.objects.get(dialer_username='system'),
            'category': 'MISSED_CALL',
            'data': {
                'mobile': self.mobile,
                'product': self.product_type,
                'category': ad_category,
                'source': 'INCOMING',
                'campaign': '',
            },
        }
        activity = create_crm_activity(data=adata)
        activity.end_time = timezone.now()
        activity.save()

        req = activity.requirements.last()
        req.last_crm_activity = activity
        req.save()

    def _get_customer(self):
        phone = core_models.Phone.objects.filter(number__endswith=self.mobile).last()
        if not phone:
            return None
        return phone.user if phone.user else phone.visitor

    def _get_did_product_type(self):
        did_product_type = redis_utils.Publisher.rclient.get('crtObjId_%s' % self.crt_obj_id)
        if not did_product_type:
            product_type = 'car'
            did_product_type = '%s_%s' % (PRODUCT_DEFAULT_DIDS.get(product_type), product_type)
        did, product_type = did_product_type.split('_')
        return did, product_type
