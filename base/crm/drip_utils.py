from __future__ import absolute_import
import traceback
import json
import logging

from django.utils import timezone
from django.conf import settings

from cfutils.nudgespot import Nudge
from core import models as core_models
from crm import utils as crm_utils
from celery_app import app

drip_logger = logging.getLogger('drip')


# FIXME - should check if DND
def _track_event(customer, activity_id=None, check_ip=True):
    """
    Do not send events
        1. If customer is registered as DND
        2. If a user activity from internal ip is being sent to live account.
            activities done on behalf of the user should have check_ip=False
    """
    if settings.SEND_NUDGESPOT_EVENTS:
        if not settings.NUDGESPOT:
            return True

        if activity_id:
            try:
                activity = core_models.Activity.objects.get(id=activity_id)
            except core_models.Activity.DoesNotExist:
                # atomic rollback
                return False
            else:
                if check_ip:
                    return activity.ip_address not in settings.INTERNAL_IPS
        return True
    return False


def _get_cust_id(customer):
    """
    Nudgespot will need a unique identifier from Jarvis end.
    :type customer: User | Tracker
    """
    if isinstance(customer, core_models.User):
        return customer.customer_id
    else:
        return customer.session_key


def _get_name(customer):
    if isinstance(customer, core_models.User):
        name = customer.get_full_name()
        return name
    else:
        return ''


def _get_email(customer):
    return customer.primary_email


def _get_mobile(customer):
    return customer.primary_phone


@app.task(queue='drip')
def save_event(rdata, customer_id, activity_id, timestamp=None, check_ip=True):
    try:
        customer = core_models.User.objects.get(_customer_id=customer_id)
    except core_models.User.DoesNotExist:
        customer = core_models.Tracker.objects.get(session_key=customer_id)

    if _track_event(customer, activity_id=activity_id, check_ip=check_ip):
        activity = None
        if not rdata:
            activity = core_models.Activity.objects.get(id=activity_id)
            rdata = get_rdata(act=activity)

        if 'event' not in rdata:
            if not activity:
                activity = core_models.Activity.objects.get(id=activity_id)
            rdata['event'] = _get_event(act=activity)

        base_site = 'https://www.coverfox.com'
        data = {
            'result_page': {
                'health': {
                    'url_data': '/health-plan/results/#',
                    'url_id': 'activity_id',
                },
                'car': {
                    'url_data': '/motor/car-insurance/',
                    'url_id': 'quoteId',
                },
                'bike': {
                    'url_data': '/motor/twowheeler-insurance/',
                    'url_id': 'quoteId',
                },
                'travel': {
                    'url': rdata.get('quote_url'),
                    'url_data': '/travel-insurance/result/',
                    'url_id': 'activity_id',
                },
            },
            'buy_page': {
                'health': {
                    'url_data': '/health-plan/buy/',
                    'url_id': 'transaction_id',
                },
                'car': {
                    'url': rdata.get('transaction_url'),
                    'url_data': '/motor/fourwheeler/' + rdata['insurer'] + '/desktopForm/' if rdata.get('insurer') else None,
                    'url_id': 'transaction_id',
                },
                'bike': {
                    'url': rdata.get('transaction_url'),
                    'url_data': '/motor/twowheeler/' + rdata['insurer'] + '/desktopForm/' if rdata.get('insurer') else None,
                    'url_id': 'transaction_id',
                },
                'travel': {
                    'url': rdata.get('transaction_url'),
                    'url_data': '/travel-insurance/buy/',
                    'url_id': 'transaction_id',
                },
            },
        }
        event = rdata['event'] if not ('failed' in rdata['event']) else 'transaction_failed'
        product_type = rdata['product_type'].lower()
        event_settings = data.get(event)

        if event_settings and not event_settings.get(product_type):
            drip_logger.error('Invalid product type {} for the event {}'.format(product_type, event))
            return None

        if event_settings:
            mid_url = event_settings[product_type].get('url_data') or ''
            url_id = rdata.get(event_settings[product_type].get('url_id')) or ''
        else:
            mid_url = ''
            url_id = ''

        url = None
        if event_settings:
            url = event_settings[product_type].get('url')
            if not url and mid_url != url_id:
                url = base_site + mid_url + url_id
        url = url or rdata.get('url')

        if customer_id:
            rdata['communication'] = communication = rdata.get('communication', 'sms')
            tollfree = getattr(
                settings, product_type.upper().replace('-', '_') + '_TOLL_FREE',
                settings.GENERAL_TOLL_FREE
            )
            if _get_name(customer):
                rdata['name'] = _clean_name(_get_name(customer), communication=communication)
            query_params = {
                'utm_source': 'im_{}'.format(communication),
                'utm_medium': product_type,
                'utm_campaign': event,
                'utm_adgroup': 'nudgespot',
            }
            url = crm_utils.get_short_url(url, params=query_params) if url is not None else url
            rdata.update({
                'mobile': rdata.get('mobile') or _get_mobile(customer),
                'url': url,
                'email': rdata.get('email') or _get_email(customer),
                'tollfree': tollfree,
            })
            properties = {
                'tollfree': tollfree,
                'name': rdata.get('name'),
                'first_name': rdata.get('first_name'),
                'last_name': rdata.get('last_name'),
            }

            if not timestamp:
                properties['last_user_activity'] = timezone.now()

            rdata['timestamp'] = timestamp

            if url:
                properties['{}_url'.format(event)] = url

            try:
                nudge = Nudge(customer_id)
                nudge.update_or_create_user(properties=properties)
                drip_client = 'Nudgespot'

                payment_mode = rdata.get('payment_mode', None)
                if not payment_mode == 'CD_ACCOUNT':
                    nudge.send_event(event, rdata)
                    drip_logger.info(
                        "{} event send to {} for customer_id {} "
                        "with event_data {} and activity_id {}".format(
                            rdata['event'],
                            drip_client,
                            customer.id,
                            event,
                            activity_id
                        )
                    )
                else:
                    drip_logger.info(
                        "{} event not sent to {} for customer_id {} and "
                        "transaction_id {} because Payment Mode is {}".format(
                            str(rdata['event']) + '_blocked',
                            drip_client,
                            customer.id,
                            rdata['transaction_id'],
                            payment_mode,
                        )
                    )
            except:
                stacktrace = traceback.format_exc()
                drip_logger.error(stacktrace)


@app.task(queue='drip')
def save_bought(rdata, activity_id, user_id=None, tracker_id=None, timestamp=None):
    try:
        customer = core_models.User.objects.get(_customer_id=user_id)
    except:
        customer = core_models.Tracker.objects.get(session_key=user_id)

    if _track_event(customer, activity_id=activity_id):
        activity = None
        if not rdata:
            activity = core_models.Activity.objects.get(id=activity_id)
            rdata = get_rdata(act=activity)

        if 'event' not in rdata:
            if not activity:
                activity = core_models.Activity.objects.get(id=activity_id)
            rdata['event'] = _get_event(act=activity)

        if _get_name(customer):
            rdata['name'] = _clean_name(_get_name(customer))
        rdata['premium_paid'] = round(float(rdata['premium_paid']), 2)
        product_type = rdata['product_type'].lower()
        tollfree = getattr(
            settings, product_type.upper().replace('-', '_') + '_TOLL_FREE',
            settings.GENERAL_TOLL_FREE
        )
        rdata.update({
            'mobile': _get_mobile(customer),
            'email': _get_email(customer),
            'tollfree': tollfree,
        })
        rdata['communication'] = rdata.get('communication', 'sms')
        if rdata.get('product_title'):
            policy = '-'.join([rdata.get('insurer_title'), rdata.get('product_title')])
        else:
            policy = rdata.get('insurer_title')
        if policy:
            if len(policy) > 40 and rdata['communication'] == 'sms':
                policy = policy[:40]
            rdata['policy'] = policy.title()
        properties = {
            'tollfree': tollfree,
            'name': rdata.get('name'),
            'policy': rdata.get('policy'),
            'premium': rdata['premium_paid'],
        }

        if not timestamp:
            properties['last_user_activity'] = timezone.now()

        rdata['timestamp'] = timestamp

        try:
            nudge = Nudge(user_id)
            nudge.update_or_create_user(properties=properties)
            drip_client = 'Nudgespot'

            payment_mode = rdata.get('payment_mode', None)
            if not payment_mode == 'CD_ACCOUNT':
                nudge.send_event('bought_success', rdata)

                drip_logger.info(
                    "{} event send to {} for customer_id {} and transaction_id {}".format(
                        'bought_success',
                        drip_client,
                        customer.id,
                        rdata['transaction_id'],
                    )
                )
            else:
                drip_logger.info(
                    "{} event not sent to {} for customer_id {} and "
                    "transaction_id {} because Payment Mode is {}".format(
                        'bought_success_blocked',
                        drip_client,
                        customer.id,
                        rdata['transaction_id'],
                        payment_mode,
                    )
                )
        except:
            stacktrace = traceback.format_exc()
            drip_logger.error(stacktrace)


@app.task(queue='drip')
def welcome_customer(event, customer_id, rdata, activity_id=None):
    try:
        customer = core_models.User.objects.get(_customer_id=customer_id)
    except core_models.User.DoesNotExist:
        customer = core_models.Tracker.objects.get(session_key=customer_id)

    if _track_event(customer, activity_id=activity_id):
        label = rdata.get('label') or ''
        if label.startswith('lp_ebook'):
            product = rdata.get('product', '').split('_')[0]
            if product in ['car', 'bike']:
                rdata['event'] = 'motor_ebook'
                return save_event(rdata, customer_id, activity_id)
            elif product == 'health':
                rdata['event'] = 'health_ebook'
                return save_event(rdata, customer_id, activity_id)
        if rdata.get('name') or rdata.get('proposer_first_name'):
            rdata['name'] = _clean_name(rdata.get('name') or rdata.get('proposer_first_name'))
        product_type = rdata.get('product_type', '').lower()
        verbose_product_type = product_type
        if not product_type:
            campaign = str(rdata.get('campaign', '')).lower()
            if campaign.startswith('motor-bike'):
                product_type = 'motor-bike'
                verbose_product_type = 'two-wheeler'
            elif campaign.startswith('motor'):
                product_type = 'motor'
                verbose_product_type = 'car'
            elif campaign.startswith('health'):
                product_type = 'health'
                verbose_product_type = 'health'
            elif campaign.startswith('travel'):
                product_type = 'travel'
                verbose_product_type = 'travel'
            else:
                product_type = None
                verbose_product_type = None
        rdata['mobile'] = str(rdata['mobile'])[-10:] if rdata.get('mobile') else None
        rdata['communication'] = rdata.get('communication', 'sms')
        tollfree = getattr(
            settings, product_type.upper().replace('-', '_') + '_TOLL_FREE',
            settings.GENERAL_TOLL_FREE
        )
        if (rdata.get('mobile') and rdata['communication'] == 'sms') or \
                (rdata.get('email') and rdata['communication'] == 'email'):
            rdata.update({'product_type': product_type,
                          'tollfree': tollfree,
                          'verbose_product_type': verbose_product_type})
            properties = {
                'tollfree': tollfree,
                'name': rdata.get('name'),
                'last_user_activity': timezone.now(),
            }
            try:
                nudge = Nudge(customer_id)
                nudge.update_or_create_user(properties=properties)
                nudge.send_event(event, rdata)
                drip_client = 'Nudgespot'
                drip_logger.info(
                    "{} event send to {} for customer_id {}".format(
                        event,
                        drip_client,
                        customer.id,
                    )
                )
            except:
                stacktrace = traceback.format_exc()
                drip_logger.error(stacktrace)


@app.task(queue='drip')
def send_quote_link(customer, rdata):
    if _track_event(customer):
        quote_link = rdata['quote_link']
        event = rdata.get('event', 'fb_quote_link')
        query_params = {
            'utm_source': 'im_sms',
            'utm_medium': rdata.get('source', 'facebook'),
            'utm_campaign': rdata.get('utm_campaign', event),
            'utm_keyword': rdata.get('utm_keyword', 'nudgespot'),
            'utm_term': rdata.get('utm_term', 'quote_link'),
        }
        url = crm_utils.get_short_url(quote_link, params=query_params)
        rdata['quote_link'] = url
        rdata['mobile'] = rdata.get('mobile', _get_mobile(customer))
        try:
            nudge = Nudge(str(customer.id))
            nudge.send_event(event, rdata)
            drip_client = 'Nudgespot'
            drip_logger.info(
                "{} event send to {} for customer_id {}".format(
                    event,
                    drip_client,
                    customer.id,
                )
            )
        except:
            stacktrace = traceback.format_exc()
            drip_logger.error(stacktrace)


@app.task(queue='drip')
def agent_event(event, requirement, sms_text, created_by, sender):
    if _track_event(requirement.user):
        customer = requirement.user
        to_mobile = _get_mobile(customer)
        nudge = Nudge(str(customer.id))
        nudge.send_event(
            event,
            event_data={
                'sms_text': sms_text,
                'mobile': to_mobile,
                'email': _get_email(customer)
            }
        )
        drip_logger.info(
            "customer_busy_sms event send to Nudgespot for customer_id %s with event_data %s" % (
                str(customer.id),
                json.dumps({'sms_text': sms_text, 'mobile': to_mobile, 'email': _get_email(customer)})
            )
        )
        return True


@app.task(queue='drip')
def dispose_call(requirement_id, disposition_name, disposition, head_disposition, activity_id=None):
    from crm import models as crm_models

    requirement = core_models.Requirement.objects.get(id=requirement_id)
    product_type = requirement.kind.product
    if activity_id:
        activity = crm_models.Activity.objects.get(id=activity_id)
    else:
        activity = requirement.last_crm_activity
    agent = requirement.owner or activity.user

    if disposition_name and _track_event(requirement.user):
        customer = requirement.user or requirement.visitor

        user_id = requirement.user.customer_id if requirement.user else None
        tracker_id = requirement.visitor.session_key if requirement.visitor else None
        customer_id = user_id or tracker_id

        properties = {
            'mobile': _get_mobile(customer),
            'agent': agent.user.get_full_name(),
            'disposition': disposition_name,
            'product': product_type
        }
        try:
            nudge = Nudge(customer_id)
            nudge.update_or_create_user(
                contacts=[{'contact_value': _get_email(customer), 'contact_type': 'email'}],
                properties=properties
            )
            nudge.send_event(
                event_name='call_disposed',
                event_data={
                    'disposition_name': disposition_name,
                    'product_type': product_type,
                    'disposition': disposition,
                    'head_disposition': head_disposition,
                    'number': agent.number
                }
            )
        except:
            stacktrace = traceback.format_exc()
            drip_logger.error(stacktrace)


@app.task(queue='drip')
def merge_users(user_id, *other_user_ids):
    if not other_user_ids:
        return
    for dup_user_id in other_user_ids:
        try:
            tracker = core_models.Tracker.objects.get(session_key=dup_user_id)
        except core_models.Tracker.DoesNotExist:
            drip_logger.info("{} doesn't exist on tracker".format(dup_user_id))
        else:
            for act in tracker.activity_set.order_by('created'):
                event = _get_event(act)
                data = get_rdata(act, None)
                data['event'] = event
                if event == 'bought_success':
                    save_bought.delay(data, activity_id=act.id, user_id=user_id, timestamp=act.created)
                else:
                    save_event.delay(data, user_id, act.id, timestamp=act.created)
        finally:
            nudge = Nudge(dup_user_id)
            if nudge.exists():
                nudge.unsubscribe_all()


def get_rdata(act, req=None):
    from motor_product import models as motor_models
    from health_product import models as health_models
    from health_product.insurer import settings as health_insurer_settings
    import utils
    if req:
        product = req.kind.product
    else:
        product = act.product.split('_')[0]
    rdata = {'product_type': product}
    rdata.update({k: v for k, v in act.mid.__dict__.items() if isinstance(v, basestring)})
    rdata.update({k: v for k, v in act.__dict__.items() if isinstance(v, basestring)})
    rdata.update(act.extra)
    if product.startswith('car') or product.startswith('bike'):
        transaction = None
        quote = None
        quote_id = act.extra.get('quoteId')
        transaction_id = act.extra.get('transaction_id') or \
            utils.get_property(req, 'extra.transaction_id') or \
            utils.get_property(req, 'extra.transactionId')
        if transaction_id:
            try:
                transaction = motor_models.Transaction.objects.get(transaction_id=transaction_id)
            except motor_models.Transaction.DoesNotExist:
                pass
            if transaction:
                quote = transaction.quote
            else:
                try:
                    quote = motor_models.Quote.objects.get(quote_id=quote_id)
                except motor_models.Quote.DoesNotExist:
                    pass
            proposer_full_name = [
                utils.get_property(transaction, 'proposer.first_name'),
                utils.get_property(transaction, 'proposer.last_name')
            ]

            vehicle_data = {
                'make': utils.get_property(transaction, 'quote.vehicle.make.name'),
                'model': utils.get_property(transaction, 'quote.vehicle.model.name'),
                'variant': utils.get_property(transaction, 'quote.vehicle.variant'),
                'cc': utils.get_property(transaction, 'quote.vehicle.cc'),
                'seating_capacity': utils.get_property(transaction,
                                                       'quote.vehicle.seating_capacity'),
                'fuel_type': utils.get_property(transaction,
                                                'quote.vehicle.fuel_type'),
            }

            rdata.update({
                'name': ' '.join(filter(None, proposer_full_name)),
                'insurer': utils.get_property(transaction, 'insurer.slug'),
                'insurer_title': utils.get_property(transaction, 'insurer.title'),
                'transaction_id': transaction.transaction_id,
                'policy_number': transaction.policy_number,
                'policy_document': utils.get_property(
                    transaction,
                    'policy_document.url'
                ),
                'premium_paid': transaction.premium_paid,
                'form_details': utils.get_property(
                    transaction,
                    'raw.user_form_details'
                ),
                'vehicle': vehicle_data,
                'payment_on': transaction.payment_on,
            })
        rdata.update(utils.get_property(quote, 'raw_data') or {})
        rdata.update(utils.get_property(transaction, 'raw.user_form_details') or {})
        rdata['insurer_title'] = utils.get_property(transaction, 'insurer.title')
    elif product.startswith('health'):
        transaction = None
        transaction_id = act.extra.get('transaction_id') or \
            utils.get_property(req, 'extra.transaction_id') or \
            utils.get_property(req, 'extra.transactionId')
        if transaction_id:
            try:
                transaction = health_models.Transaction.objects.get(transaction_id=transaction_id)
            except motor_models.Transaction.DoesNotExist:
                pass
            if transaction:
                slab = transaction.slab
                name = '%s %s' % (
                    transaction.proposer_first_name, transaction.proposer_last_name
                )
                premium = (transaction.premium_paid or
                           transaction.extra.get('plan_data', {}).get('premium',
                                                                      ''))
                sum_assured = 'Rs.%s' % slab.sum_assured
                if slab.deductible:
                    sum_assured += ' (Deductible: Rs.%s)' % transaction.slab.deductible

                if transaction.policy_document:
                    rdata['policy_document'] = transaction.policy_document.url

                rdata.update({
                    'name': name,
                    'insurer': slab.health_product.insurer.title,
                    'sum_assured': sum_assured,
                    'premium': premium,
                    'transaction_link': '%s/health-plan/buy/%s' % (
                        settings.SITE_URL, transaction.transaction_id),
                })
                rdata.update(health_insurer_settings.get_product_details(slab))

    return rdata


def _get_event(act):
    from core import activity as core_activity
    page_id = act.page_id.lower()
    if page_id == 'quotes':
        event = 'result_page'
    elif page_id == 'proposal':
        if act.activity_type == core_activity.FAILED:
            event = 'proposal_failed'
        else:
            event = 'buy_page'
    elif page_id == 'payment' and act.activity_type == core_activity.SUCCESS:
        event = 'bought_success'
    elif page_id == 'payment' and act.activity_type == core_activity.FAILED:
        event = 'transaction_failed'
    else:
        event = 'welcome_customer'
    return event


def _clean_name(name, communication='sms', length=20):
    if communication == 'sms':
        names = name.split(' ')
        if len(names[0]) > 3:
            name = names[0]
        while len(name) > length:
            names = name.split(' ')
            if len(names) == 1:
                name = names[0][:length]
            else:
                name = ' '.join(names[:-1])
    return name.title()


def _get_disposition_name(requirement, product_type, disposition, head_disposition):
    name = ''
    if disposition == 'ISSUED':
        name = 'issued'
    elif product_type == 'car':
        if head_disposition == 'NO_CONVERSATION' and requirement.current_state.parent_state.name == 'QUALIFIED':
            name = 'qualified_no_conversation'
    elif product_type == 'health':
        if head_disposition == 'NO_CONVERSATION':
            name = 'no_conversation'
        elif head_disposition == 'NO_CONVERSATION' and requirement.current_state.parent_state.name == 'QUALIFIED':
            name = 'qualified_ringing'
        elif head_disposition == 'LOST_CASES':
            name = 'lost'

    if not name and head_disposition == 'DEAD' and disposition != 'DUPLICATE':
        name = 'dead'

    return name


try:
    from emailvalidation import email_validator
except:
    pass
else:
    @app.task
    def send_low_credit_mail():
        remaining_credits = email_validator.get_remaining_credits()
        if remaining_credits < 2000:
            crm_utils.send_mail.delay(
                subject='Low credits in NeverBounce',
                body='Only {} credits are remaining'.format(remaining_credits),
                to_email=['aniket@coverfoxmail.com', 'vinay@coverfoxmail.com'],
                from_email=settings.DEFAULT_FROM_EMAIL,
            )


if __name__ == '__main__':
    pass
