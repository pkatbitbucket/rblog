def is_health_requirement(requirement, **extras):
    """
    Name: Is Health Requirement
    """
    return requirement.kind.product == 'health'


def is_car_online_requirement(requirement, **extras):
    """
    Name: Is Car Online Requirement
    """
    return requirement.kind.product == 'car' and not requirement.kind.sub_kind == 'INSPECTION_REQUIRED'


def is_car_offline_requirement(requirement, **extras):
    """
    Name: Is Car Offline Requirement
    """
    return requirement.kind.product == 'car' and requirement.kind.sub_kind == 'INSPECTION_REQUIRED'


def is_bike_requirement(requirement, **extras):
    """
    Name: Is Bike Requirement
    """
    return requirement.kind.product == 'bike'


def is_travel_requirement(requirement, **extras):
    """
    Name: Is Travel Requirement
    """
    return requirement.kind.product == 'travel'


def is_home_requirement(requirement, **extras):
    """
    Name: Is Home Requirement
    """
    return requirement.kind.product == 'home'


def is_term_requirement(requirement, **extras):
    """
    Name: Is Term Requirement
    """
    return requirement.kind.product == 'term'


def is_fresh_requirement(requirement, **extras):
    """
    Name: Is Fresh Requirement
    """
    return requirement.current_state.parent_state.name == 'FRESH'


def is_fresh_ringing_requirement(requirement, **extras):
    """
    Name: Is Fresh Ringing Requirement
    """
    return requirement.current_state.name == 'FRESH_ringing'


def is_non_fresh_requirement(requirement, **extras):
    """
    Name: Requirement is not Fresh
    """
    return not requirement.current_state.parent_state.name == 'FRESH'


def is_non_system_crm_activity_count_zero(requirement, **extras):
    """
    Name: Non System CRM Activity Count is ZERO
    """
    return requirement.crm_activities.exclude(user__id=298).count() == 0


def is_ownerless_requirement(requirement, **extras):
    """
    Name: Requirement does not have any owner
    """
    return not requirement.owner


def is_cardekho_justdial_source_requirement(requirement, **extras):
    """
    Name: Requirement source is Cardekho or JustDial
    """
    source = requirement.mid.source.lower()
    allowed_sources = ['justdial', 'cardekho']
    return any(s in source for s in allowed_sources)


def is_owned_by_carexpired_qualifier(requirement, **extras):
    """
    Name: Requirement Owner is in car-expired-qualifier queue
    """
    if requirement.owner and requirement.owner.queues.filter(code='car-expired-qualifier').exists():
        return True
    else:
        return False


def is_cardekho_callcenter_requirement(requirement, **extras):
    """
    Name: Is cardekho gurgaon callcenter requirement
    """
    category = requirement.mid.category.lower()
    source = requirement.mid.source.lower()
    if not source == 'cardekho':
        return False
    if (
        category == 'cd-sales'
        or requirement.activity_set.filter(extra__contains='motor-cardekhocallcenter').exists()
    ):
        return True
    return False


def is_cardekho_source_requirement(requirement, **extras):
    """
    Name: Requirement source is Cardekho
    """
    source = requirement.mid.source.lower()
    return source == 'cardekho'


def is_car_partner_source_requirement(requirement, **extras):
    """
    Name: Requirement source is AdPartners
    """
    source = requirement.mid.source.lower()
    return source == 'adpartners'
