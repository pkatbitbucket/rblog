def is_health_qualifier_task(requirement, **extras):
    return False


def is_health_closure_task(requirement, **extras):
    if requirement.kind.product == 'health':
        return True
    else:
        return False


def is_car_offline_qualifier_task(requirement, **extras):
    """
    This will execute at a higher priority over is_car_offline_task.
    This should return False if the task isn't a Fresh or Ringing task.
    If existing owner, it should return True only if existing owner is
    in car-expired-qualifier queue.
    If not, it should return True if there have been no non-system
    crm activities performed on the requirement.
    """
    rk = requirement.kind
    if (
        not rk.product == 'car'
        or not rk.sub_kind == 'INSPECTION_REQUIRED'
        or not requirement.current_state.parent_state.name == 'FRESH'
    ):
        return False

    if (
        requirement.owner
        and requirement.owner.queues.filter(code='car-expired-qualifier').exists()
    ):
        return True
    else:
        return requirement.crm_activities.exclude(user__id=298).count() == 0
    return False


def is_car_offline_task(requirement, **extras):
    rk = requirement.kind
    if rk.product == 'car' and rk.sub_kind == 'INSPECTION_REQUIRED':
        return True
    else:
        return False


def is_bike_task(requirement, **extras):
    return requirement.kind.product == 'bike'


def is_travel_task(requirement, **extras):
    return requirement.kind.product == 'travel'


def is_home_task(requirement, **extras):
    return requirement.kind.product == 'home'


def is_term_task(requirement, **extras):
    return requirement.kind.product == 'term'


def is_car_new_task(requirement, **extras):
    rk = requirement.kind
    if not rk.product == 'car' or rk.sub_kind == 'INSPECTION_REQUIRED':
        return False
    if (
        (requirement.owner and requirement.owner.queues.filter(code='car-new').exists())
        or (requirement.current_state.name == 'FRESH_ringing')
    ):
        return True
    else:
        return False


def is_car_qualifier_task(requirement, **extras):
    rk = requirement.kind
    if not rk.product == 'car' or rk.sub_kind == 'INSPECTION_REQUIRED':
        return False
    if (
        (requirement.owner and requirement.owner.queues.filter(code='car-qualifier').exists())
        and requirement.current_state.parent_state.name == 'FRESH'
    ):
        return True
    return False


def is_car_justdial_cardekho_fresh_task(requirement, **extras):
    rk = requirement.kind
    if not rk.product == 'car' or rk.sub_kind == 'INSPECTION_REQUIRED':
        return False
    source = requirement.mid.source.lower()
    allowed_sources = ['justdial', 'cardekho']
    if (
        any(s in source for s in allowed_sources)
        and requirement.current_state.parent_state.name == 'FRESH'
    ):
        return True
    else:
        return False


def is_car_closure_task(requirement, **extras):
    rk = requirement.kind
    if (
        rk.product == 'car'
        and not rk.sub_kind == 'INSPECTION_REQUIRED'
        and not requirement.owner
        and not requirement.current_state.parent_state.name == 'FRESH'
    ):
        return True
    return False


def is_car_old_queue_task(requirement, **extras):
    rk = requirement.kind
    if not rk.product == 'car' or rk.sub_kind == 'INSPECTION_REQUIRED':
        return False
    else:
        return True


def is_car_online_fresh_task(requirement, **extras):
    rk = requirement.kind
    if not rk.product == 'car' or rk.sub_kind == 'INSPECTION_REQUIRED':
        return False
    if requirement.current_state.parent_state.name == 'FRESH':
        return True
    else:
        return False


def is_car_brand_queue_task(requirement, **extras):
    rk = requirement.kind
    if not rk.product == 'car' or rk.sub_kind == 'INSPECTION_REQUIRED':
        return False
    if requirement.owner and requirement.owner.queues.filter(code='car-brand').exists():
        return True
    return False


def is_car_search_queue_task(requirement, **extras):
    rk = requirement.kind
    if not rk.product == 'car' or rk.sub_kind == 'INSPECTION_REQUIRED':
        return False
    if requirement.owner and requirement.owner.queues.filter(code='car-search').exists():
        return True
    return False
