import re
import json
import logging
import datetime
from urlparse import urlparse

from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from django import forms
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.http.response import JsonResponse
from django.contrib.auth.models import Group, Permission

import cf_fhurl
from cfutils.forms import fhurl
from cfutils.restless import dj
from cfutils.restless import preparers
from cfutils.knockout import forms as koforms
from cfutils import model_router

from core import models as core_models
from statemachine import models as sm_models
from crm import utils as crm_utils
from crm import models as crm_models

logger = logging.getLogger(__name__)


class GroupForm(forms.ModelForm, koforms.KnockoutForm):

    class Meta:
        model = Group
        fields = ['name', 'permissions']
        field_jsmodel = {'self': 'Group', 'permissions': 'permission'}
        rest_url = '/api/group/'


class GroupResource(dj.DjangoModelResource):
    preparer = preparers.DjangoFieldsDjsonPreparer()
    form_cls = GroupForm
    model = Group

    # PUT /pk/
    def update(self, pk):
        group = get_object_or_404(Group, id=pk)
        rform = GroupForm(self.data, instance=group)
        if rform.is_valid():
            group.name = rform.cleaned_data['name']
            group.permissions.clear()
            group.permissions.add(*rform.cleaned_data['permissions'])
            group.save()
            return group
        else:
            return JsonResponse({'success': False, 'errors': rform.errors})


class PermissionForm(forms.ModelForm, koforms.KnockoutForm):

    class Meta:
        model = Permission
        fields = ['name', 'codename']
        field_jsmodel = {'self': 'Permission'}
        rest_url = '/api/permission/'

    def clean_name(self):
        d = self.cleaned_data
        if self.instance and self.instance.id:
            if Permission.objects.filter(Q(name=d['name']) & ~Q(id=self.instance.id)):
                raise forms.ValidationError(
                    "Permission with this name already exists")
        else:
            if Permission.objects.filter(name=d['name']):
                raise forms.ValidationError(
                    "Permission with this name already exists")
        return d['name']


class PermissionResource(dj.DjangoModelResource):
    preparer = preparers.DjangoFieldsDjsonPreparer()
    form_cls = PermissionForm
    model = Permission


class SubDispositionForm(forms.ModelForm, koforms.KnockoutForm):

    class Meta:
        model = crm_models.SubDisposition
        fields = ['value', 'verbose']
        field_jsmodel = {'self': 'SubDisposition'}
        rest_url = '/api/sub-disposition/'

    def clean_value(self):
        d = self.cleaned_data
        if self.instance and self.instance.id:
            if crm_models.SubDisposition.objects.filter(Q(value=d['value']) & ~Q(id=self.instance.id)):
                raise forms.ValidationError(
                    "SubDisposition with this value already exists")
        else:
            if crm_models.SubDisposition.objects.filter(value=d['value']):
                raise forms.ValidationError(
                    "SubDisposition with this value already exists")
        return d['value']


class SubDispositionResource(dj.DjangoModelResource):
    preparer = preparers.DjangoFieldsDjsonPreparer()
    form_cls = SubDispositionForm
    model = crm_models.SubDisposition


class DispositionForm(forms.ModelForm, koforms.KnockoutForm):

    class Meta:
        model = crm_models.Disposition
        fields = ['value', 'verbose', 'sub_dispositions']
        field_jsmodel = {'self': 'Disposition',
                         'sub_dispositions': 'subdisposition'}
        rest_url = '/api/disposition/'

    def clean_value(self):
        d = self.cleaned_data
        if self.instance and self.instance.id:
            if crm_models.Disposition.objects.filter(Q(value=d['value']) & ~Q(id=self.instance.id)):
                raise forms.ValidationError(
                    "Disposition with this value already exists")
        else:
            if crm_models.Disposition.objects.filter(value=d['value']):
                raise forms.ValidationError(
                    "Disposition with this value already exists")
        return d['value']


class DispositionResource(dj.DjangoModelResource):
    preparer = preparers.DjangoFieldsDjsonPreparer()
    form_cls = DispositionForm
    model = crm_models.Disposition

    # PUT /pk/
    def update(self, pk):
        disposition = get_object_or_404(crm_models.Disposition, id=pk)
        dform = DispositionForm(self.data, instance=disposition)
        if dform.is_valid():
            disposition.value = dform.cleaned_data['value']
            disposition.verbose = dform.cleaned_data['verbose']
            disposition.save()
            disposition.sub_dispositions.clear()
            disposition.sub_dispositions.add(
                *dform.cleaned_data['sub_dispositions'])
            return disposition
        else:
            return JsonResponse({'success': False, 'errors': dform.errors})


class QueueManagementForm(cf_fhurl.RequestForm):
    advisors = forms.ModelMultipleChoiceField(required=False, queryset=None, widget=forms.CheckboxSelectMultiple())

    def init(self, qid):
        self.queue = crm_models.Queue.objects.get(id=qid)
        qads = crm_models.QueueAdvisor.objects.filter(
            is_manager=False,
            queue=self.queue
        )
        self.initial = {
            'advisors': crm_models.Advisor.objects.filter(
                user__is_active=True,
                queueadvisor__in=qads
            ).distinct()
        }
        self.fields['advisors'].queryset = crm_models.Advisor.objects.all()

    def save(self):
        updated_advisors = self.cleaned_data.get('advisors')
        for adv in updated_advisors:
            qad, _ = crm_models.QueueAdvisor.objects.get_or_create(queue=self.queue, advisor=adv, is_manager=False)
        crm_models.QueueAdvisor.objects.filter(
            queue=self.queue, is_manager=False
        ).exclude(advisor=updated_advisors).delete()
        logger.info(
            'Manager {manager_id} modified queue {queue_id} to - {advisor_list}'.format(
                manager_id=crm_models.Advisor.objects.get(user=self.request.user).id,
                queue_id=self.queue.id,
                advisor_list=list(updated_advisors.values_list('id', flat=True))
            )
        )
        return {'success': True}


class AdvisorManagementForm(cf_fhurl.RequestForm):
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30, required=False)
    is_available = forms.BooleanField(label='Allow fresh leads', initial=True, required=False)
    dialer_username = forms.CharField(label='Ameyo username', max_length=30, required=True)
    dialer_did = forms.CharField(label='Ameyo did', max_length=10, required=False)
    dialer_extension = forms.CharField(label='Ameyo Extension', max_length=10, required=False)
    dialer_number = forms.CharField(label='Number', max_length=15, required=False)

    def init(self, uid):
        self.adv = crm_models.Advisor.objects.get(id=uid)
        self.initial = {
            'first_name': self.adv.user.first_name,
            'last_name': self.adv.user.last_name,
            'is_available': self.adv.is_available,
            'dialer_username': self.adv.dialer_username,
            'dialer_did': self.adv.dialer_did,
            'dialer_extension': self.adv.dialer_extension,
            'dialer_number': self.adv.number
        }

    def save(self):
        adv = self.adv
        user = adv.user
        d = self.cleaned_data.get
        user.first_name = d('first_name')
        user.last_name = d('last_name')
        adv.is_available = d('is_available')
        adv.dialer_username = d('dialer_username')
        adv.dialer_did = d('dialer_did')
        adv.dialer_extension = d('dialer_extension')
        adv.number = d('dialer_number')
        adv.save()
        user.save()
        logger.info(
            'Manager {manager_id} modified advisor {advisor_id}'.format(
                manager_id=crm_models.Advisor.objects.get(user=self.request.user).id,
                advisor_id=adv.id
            )
        )
        return {'success': True}


class LeadSearchForm(fhurl.RequestForm):
    customer_name = forms.CharField(max_length=30, required=False)
    customer_email = forms.EmailField(required=False,)
    customer_mobile = forms.CharField(max_length=12, required=False)
    last_head_disposition = forms.ModelMultipleChoiceField(required=False, queryset=None, widget=forms.CheckboxSelectMultiple)
    last_call_time_from = forms.DateTimeField(widget=forms.TextInput(attrs={"data-format": "dd/MM/yyyy hh:mm:ss"}), required=False)
    last_call_time_to = forms.DateTimeField(widget=forms.TextInput(attrs={"data-format": "dd/MM/yyyy hh:mm:ss"}), required=False)
    next_call_time_from = forms.DateTimeField(widget=forms.TextInput(attrs={"data-format": "dd/MM/yyyy hh:mm:ss"}), required=False)
    next_call_time_to = forms.DateTimeField(widget=forms.TextInput(attrs={"data-format": "dd/MM/yyyy hh:mm:ss"}), required=False)
    is_concluded_search = forms.BooleanField(label='Search in concluded', initial=False, required=False)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(LeadSearchForm, self).__init__(*args, **kwargs)
        self.fields['last_head_disposition'].queryset = crm_models.Disposition.objects.all()

    def save(self):
        up = crm_models.Advisor.objects.get(user=self.request.user)
        include_concluded = False
        search_data = dict(self.cleaned_data)
        if not search_data.get('current_state'):
            search_data['current_state'] = sm_models.State.objects.exclude(
                name__startswith='CLOSED'
            )
        if search_data.get('is_concluded_search'):
            search_data['current_state'] = sm_models.State.objects.all()
            include_concluded = True
        qs_mobile = None
        qs_email = None
        is_email_search = False
        is_mobile_search = False
        if search_data.get('customer_email'):
            qs_email = core_models.Requirement.objects.search_by_email(
                email=search_data.pop('customer_email'),
                include_concluded=include_concluded
            )
            is_email_search = True
        if search_data.get('customer_mobile'):
            qs_mobile = core_models.Requirement.objects.search_by_mobile(
                mobile=search_data.pop('customer_mobile'),
                include_concluded=include_concluded
            )
            is_mobile_search = True
        qs_leads = core_models.Requirement.objects.filter(
            id__in=crm_utils.requirement_searcher(
                search_data
            ).values_list('id', flat=True)
        )

        if is_mobile_search and qs_leads and qs_mobile:
            qs_leads = qs_leads.filter(id__in=list(qs_mobile.values_list('id', flat=True)))
        if is_email_search and qs_leads and qs_email:
            qs_leads = qs_leads.filter(id__in=list(qs_email.values_list('id', flat=True)))

        if qs_leads and not search_data.get('is_manager'):
            qs_leads = qs_leads.filter(owner=up)
        if not qs_leads.exists():
            return {'success': False, 'results': [], 'count': 0}
        else:
            qs_count = qs_leads.count()

            # The below is being done because Django doesn't support 'NULLS LAST' as a
            # param currently. Cases where there is no next_crm_task should be last
            qs_leads = qs_leads.extra(select={'null_nct': 'next_crm_task_id IS NULL'})
            qs_leads = qs_leads.order_by('null_nct', '-next_crm_task__scheduled_time').reverse()

            if search_data.get('is_manager'):
                results = qs_leads
            else:
                results = qs_leads[:10]
                logger.info(
                    'Search params and results {up_id} - {searched_json} - {results}'.format(
                        up_id=up.id,
                        searched_json=crm_utils.get_search_json(search_data),
                        results=[r.id for r in results]
                    )
                )
            return {'success': True, 'results': [l.mini_json() for l in results], 'count': qs_count}


class ManagerSearchForm(LeadSearchForm):
    created_on_from = forms.DateTimeField(widget=forms.TextInput(attrs={"data-format": "dd/MM/yyyy hh:mm:ss"}), required=False)
    created_on_to = forms.DateTimeField(widget=forms.TextInput(attrs={"data-format": "dd/MM/yyyy hh:mm:ss"}), required=False)
    id = forms.CharField(max_length=12, required=False)
    id_list = forms.CharField(max_length=10000, required=False)
    queues = forms.ModelMultipleChoiceField(required=False, queryset=crm_models.Queue.objects.all())
    owner = forms.ModelChoiceField(required=False, queryset=crm_models.Advisor.objects.all())
    current_state = forms.ModelChoiceField(required=False, queryset=None)
    parent_state = forms.ModelChoiceField(required=False, queryset=None)
    past_policy_expiry_date_from = forms.DateField(widget=forms.TextInput(attrs={"data-format": "dd/MM/yyyy"}), required=False)
    past_policy_expiry_date_to = forms.DateField(widget=forms.TextInput(attrs={"data-format": "dd/MM/yyyy"}), required=False)

    def __init__(self, *args, **kwargs):
        super(ManagerSearchForm, self).__init__(*args, **kwargs)
        up = crm_models.Advisor.objects.get(user=self.request.user)
        qad = crm_models.QueueAdvisor.objects.filter(advisor=up, is_manager=True)
        if qad:
            queues = crm_models.Queue.objects.filter(queueadvisor__in=qad)
            self.fields['queues'].queryset = queues

            qads = crm_models.QueueAdvisor.objects.filter(
                is_manager=False,
                queue__in=queues
            )
            self.fields['owner'].queryset = crm_models.Advisor.objects.filter(
                user__is_active=True,
                queueadvisor__in=qads
            ).distinct()
        self.fields['current_state'].queryset = sm_models.State.objects.all()
        self.fields['parent_state'].queryset = sm_models.ParentState.objects.exclude(
            name__in=['DUMMY', 'DUPLICATE']
        )

    def clean_id_list(self):
        d = self.cleaned_data
        if d['id_list']:
            return [int(rid) for rid in d['id_list'].split(',')]
        else:
            return []

    def save(self):
        search_data = self.cleaned_data
        search_data['is_concluded_search'] = False if search_data['current_state'] else True
        search_data['current_state'] = search_data['current_state'] or sm_models.State.objects.all()
        search_data['is_manager'] = True
        result_dict = super(ManagerSearchForm, self).save()
        result_dict['is_manager'] = True
        return result_dict


class NumberSearchForm(fhurl.RequestForm):
    customer_mobile = forms.CharField(max_length=12, widget=forms.TextInput(attrs={'class': 'span12'}))

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(NumberSearchForm, self).__init__(*args, **kwargs)

    def save(self):
        up = crm_models.Advisor.objects.get(user=self.request.user)
        search_data = dict(self.cleaned_data)
        qs_leads = core_models.Requirement.objects.search_by_mobile(search_data['customer_mobile'])
        if not qs_leads.exists():
            return {'success': False, 'results': [], 'count': 0}
        else:
            qs_count = qs_leads.count()
            results = qs_leads[:10]
            logger.info(
                'Search params and results {up_id} - {searched_json} - {results}'.format(
                    up_id=up.id,
                    searched_json=crm_utils.get_search_json(search_data),
                    results=[r.id for r in results]
                )
            )
            return {'success': True, 'results': [l.mini_json() for l in results], 'count': qs_count}


class ReassignForm(fhurl.RequestForm):
    reassign_to = forms.ModelChoiceField(queryset=None)
    rid = forms.IntegerField(widget=forms.HiddenInput)

    def map_campaign(self, requirement):
        queue = crm_models.Queue.objects.filter(name__startswith=requirement.kind.product)
        qualifier_queue = crm_models.Queue.objects.filter(name='%s-qualifier' % requirement.kind.product).last()
        if qualifier_queue:
            queue = queue.exclude(id=qualifier_queue.id)
        qs = None
        if queue:
            qad = crm_models.QueueAdvisor.objects.filter(queue=queue, is_manager=False).distinct()
            qs = crm_models.Advisor.objects.filter(queueadvisor__in=qad).distinct()
        self.fields['reassign_to'].queryset = qs

    def save(self):
        new_caller = self.cleaned_data['reassign_to']
        req = core_models.Requirement.objects.get(id=self.cleaned_data['rid'])
        crm_utils.reassign(
            req, new_caller,
            customerPhone=self.request.POST['customerPhone'],
            additionalParams=self.request.POST['additionalParams']
        )


class SendSMSForm(fhurl.RequestForm):
    PROPOSAL_FORM_SMS = """
        Hi! We noticed that you attempted a transaction at Coverfox.
        You can complete the transaction here: %(buy_link)s .
        In case you have any queries, you can contact us on %(number)s.
    """
    MANUAL_SMS_TEMPLATES = {
        "PROPOSAL_FORM_FILLED_BY_AGENT": PROPOSAL_FORM_SMS,
    }
    rid = forms.CharField(max_length=13)
    mobile = forms.CharField(max_length=10)
    sms_type = forms.ChoiceField(choices=[(k, k) for k in MANUAL_SMS_TEMPLATES])
    buy_link = forms.CharField(max_length=1000)
    sms_dict = forms.CharField()

    def init(self, rid=None):
        self.instance = get_object_or_404(core_models.Requirement, pk=rid)
        self.initial = {
            'rid': self.instance.id,
            'mobile': self.instance.mini_json().get('mobile'),
            'sms_dict': json.dumps(self.MANUAL_SMS_TEMPLATES),
        }

    def save(self):
        req = self.instance
        shortened_buy_link = crm_utils.get_short_url(
            self.cleaned_data['buy_link'], crm_utils.SMS_QUERY_URL_PARAMETERS
        )
        crm_utils.manual_send_sms(
            self.request, self.cleaned_data['mobile'],
            self.cleaned_data['sms_type'], shortened_buy_link, req
        )
        return True


class CustomerSearchForm(fhurl.RequestForm):
    customer_search = forms.CharField(
        max_length=50,
        widget=forms.TextInput(
            attrs={'class': 'span12', 'placeholder': 'Mobile/Email/Transaction Id'}
        ), required=False
    )  # for travel

    def save(self):
        response = []
        if self.cleaned_data['customer_search']:
            d = self.cleaned_data['customer_search']
            phones = core_models.Phone.objects.filter(number__endswith=d)
            emails = core_models.Email.objects.filter(email__endswith=d)
            requirements = core_models.Requirement.objects.filter(
                Q(extra__jcontains={'transaction_id': d}) | Q(user__emails__in=emails) | Q(user__phones__in=phones)
            )
            if requirements:
                for req in requirements:
                    req_json = req.mini_json()
                    # if req.policy:
                    #     if req.kind.product in ['car', 'bike', 'motor']:
                    #         transaction = req.policy.motor_transaction
                    #     elif req.kind.product.startswith('health'):
                    #         transaction = req.policy.health_transaction
                    #     elif req.kind.product.startswith('tarvel'):
                    #         transaction = req.policy.travel_transaction
                    # if transaction:
                    #     req_json['transaction'] = transaction.mini_json()
                    response.append(req_json)
            return {'success': True, 'requirements': response}
        else:
            return {'success': True, 'requirements': []}


class AdvisorDialerActivityForm(fhurl.RequestForm):
    username = forms.CharField()
    status = forms.CharField()

    def save(self):
        advisor = crm_models.Advisor.objects.get(dialer_username=self.cleaned_data['username'])
        status = self.cleaned_data['status']
        try:
            last_status = crm_models.AdvisorDialerActivity.objects.filter(advisor=advisor).latest('id').status
        except ObjectDoesNotExist:
            last_status = None
        if not last_status or not status == last_status:
            crm_models.AdvisorDialerActivity.objects.create(advisor=advisor, status=status)
        return {'success': True}


class RequirementCallForm(forms.Form):
    MAJOR_CITIES_CHOICES = [
        ("", "-----"),
        ('NEW_DELHI', 'New Delhi'),
        ('MUMBAI', 'Mumbai'),
        ('BANGLORE', 'Banglore'),
        ('CHENNAI', 'Chennai'),
        ('KOLKATTA', 'Kolkatta'),
        ('HYDERABAD', 'Hyderabad'),
        ('OTHERS', 'Others'),
    ]
    SOURCE_CHOICES = [
        ("", "-----"),
        ('RADIO', 'Radio'),
        ('RADIO_RADIO_CITY', 'Radio - Radio City'),
        ('RADIO_BIG_FM', 'Radio - Big FM'),
        ('RADIO_FEVER', 'Radio - Fever 104'),
        ('PRINT', 'Print'),
        ('PRINT_HT', 'Print - Hindustan Times'),
        ('PRINT_GGNT', 'Print - Gurgaon Times'),
        ('POSTERS', 'Posters'),
        ('OUTDOOR', 'Outdoor'),
        ('TV_AD', 'TV Advertisement'),
        ('VIDEO', 'Video'),
        ('OTHERS', 'Others'),
    ]
    PLAN_TYPE_CHOICES = [
        ('', '-----'),
        ('BASE_PLAN', 'Base Plan'),
        ('BASE_PLAN_TOP_UP', 'Base Plan + Top Up'),
        ('TOP_UP', 'Top Up'),
    ]
    YES_NO_CHOICES = [
        (True, 'Yes'),
        (False, 'No')
    ]
    HEALTH_CASE_TYPE = (
        ('', '-------------'),
        ('PORTABILITY', 'Portability Case'),
        ('RENEWAL', 'Renewal Case'),
        ('MEDICAL', 'Medical Case'),
    )

    requirement_id = forms.CharField(required=False)
    activity_id = forms.IntegerField(required=False)
    task_id = forms.IntegerField(required=False)
    first_name = forms.CharField(required=False)
    middle_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)
    email = forms.EmailField(required=False, widget=forms.TextInput(attrs={'class': 'span12'}))
    mobile = forms.CharField(max_length=100, widget=forms.HiddenInput, required=False)
    notes = forms.CharField(widget=forms.Textarea(attrs={'class': 'span12'}), required=False)
    pincode = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'span6'}))
    city = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'span20'}))
    expected_no_of_policies = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'span6'}))
    plan_type = forms.ChoiceField(choices=PLAN_TYPE_CHOICES, required=False)
    expected_premium_amount = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'span10'}))
    quote_link = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'span12'}))
    case = forms.ChoiceField(choices=HEALTH_CASE_TYPE, widget=forms.Select(attrs={'class': 'span12'}), required=False)

    is_car_offline_case = forms.BooleanField(label='Car Offline Case', initial=False, required=False)

    major_cities = forms.ChoiceField(choices=MAJOR_CITIES_CHOICES, required=False)
    past_policy_expiry_date = forms.DateField(widget=forms.TextInput(attrs={"data-format": "dd/MM/yyyy"}), required=False)
    travel_date = forms.DateField(widget=forms.TextInput(attrs={"data-format": "dd/MM/yyyy"}), required=False)
    later_time = forms.DateTimeField(required=False)
    no_of_policies_bought = forms.IntegerField(required=False, widget=forms.TextInput(attrs={'class': 'span4'}))
    is_customer_scheduled = forms.ChoiceField(choices=YES_NO_CHOICES, widget=forms.RadioSelect(), initial=False)
    reassign_to_id = forms.CharField(required=False, widget=forms.HiddenInput)

    crt_obj_id = forms.CharField(required=False, widget=forms.HiddenInput)
    call_type = forms.CharField(required=False, widget=forms.HiddenInput)
    call_start_time = forms.CharField(required=False, widget=forms.HiddenInput)
    call_end_time = forms.CharField(required=False, widget=forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(RequirementCallForm, self).__init__(*args, **kwargs)
        sd_choices = [(sd.value, sd.verbose) for sd in crm_models.SubDisposition.objects.all()]
        hd_choices = [('', '----Select----')] + [(hd.value, hd.verbose) for hd in crm_models.Disposition.objects.all()]
        self.fields['head_disposition'] = forms.ChoiceField(choices=hd_choices)
        self.fields['disposition'] = forms.ChoiceField(choices=sd_choices)

    def clean(self):
        data = self.cleaned_data

        data['crt_obj_id'] = "" if data.get('crt_obj_id') == 'undefined' else data.get('crt_obj_id')

        if data.get('call_start_time') in ['null', 'undefined'] or not data.get('call_start_time'):
            call_start_time = None
        else:
            call_start_time = int(data.get('call_start_time', 0)) / 1000
        data['call_start_time'] = datetime.datetime.fromtimestamp(int(call_start_time)) if call_start_time else None

        if data.get('call_end_time') in ['null', 'undefined'] or not data.get('call_end_time'):
            call_end_time = None
        else:
            call_end_time = int(data.get('call_end_time', 0)) / 1000
        data['call_end_time'] = datetime.datetime.fromtimestamp(int(call_end_time)) if call_end_time else None

        if data.get('quote_link'):
            quote_link_url = urlparse(data.get('quote_link'))
            if not(bool(quote_link_url.netloc) & bool(quote_link_url.path)):
                self._errors['quote_link'] = self.error_class(["Please enter a valid url"])
            else:
                q_id = re.search('[a-zA-z0-9\-]{30,}', data.get('quote_link'))
                if q_id:
                    data['quote_id'] = q_id.group(0)

        product_type = data.get('product_type')

        if not data.get('past_policy_expiry_date') and data.get('head_disposition') == 'QUALIFIED' and product_type in ['bike']:
            self._errors['past_policy_expiry_date'] = self.error_class(["Please add Policy Expiry Date"])
        elif data.get('is_customer_scheduled') == 'Y' and not data.get('later_time'):
            self._errors['later_time'] = self.error_class(["Please enter Customer Scheduled Time"])
        else:
            return data


class QueueRouterDashboardForm(cf_fhurl.RequestModelForm):
    conditions = forms.ModelMultipleChoiceField(queryset=None)

    class Meta:
        model = crm_models.QueueRouter
        fields = ['conditions']

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(QueueRouterDashboardForm, self).__init__(*args, **kwargs)
        self.fields['conditions'].queryset = crm_models.Condition.objects.all()

    def save(self):
        qrs = crm_models.QueueRouter.objects.all()
        for c in self.cleaned_data['conditions']:
            qrs = qrs.filter(conditions=c)
        qrs = qrs.order_by('-score')
        queuerouters = [(qr.queue.name, list(qr.conditions.values_list('name', flat=True)), qr.score) for qr in qrs]
        return {'success': True, 'results': queuerouters}

class LeadForm(cf_fhurl.RequestForm):
    SOURCE_CHOICES = [
        ("", "-----"),
        ('OLARK', 'Olark'),
        ('FACEBOOK', 'Facebook'),
        ('help@coverfox.com', 'help@coverfox.com'),
        ('MFC', 'MFC'),
        ('REFERRAL', 'Referral'),
    ]
    first_name = forms.CharField(max_length=30, required=False)
    last_name = forms.CharField(max_length=30, required=False)
    mobile = forms.CharField(max_length=12, required=True)
    email = forms.EmailField(max_length=30, required=False)
    product = forms.ModelChoiceField(
        queryset=None,
        required=True
    )
    assigned_to = forms.ModelChoiceField(
        required=False,
        queryset=None
    )
    source = forms.ChoiceField(choices=SOURCE_CHOICES, required=True)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(LeadForm, self).__init__(*args, **kwargs)
        self.fields['product'].queryset = core_models.RequirementKind.objects.all()
        self.fields['assigned_to'].queryset = crm_models.Advisor.objects.filter(
            queueadvisor__is_manager=False, user__is_active=True
        ).distinct()

    def save(self):
        from leads import models as leads_models
        from core import forms as core_forms
        from core import activity
        data = self.cleaned_data
        kind = data.get('product')
        data['campaign'] = kind.product
        advisor = data['assigned_to']
        data['product'] = data['product'].product
        data['assigned_to'] = self.request.user.advisor.last().id
        if not data.get('campaign'):
            data["no_campaign"] = True
            lead = leads_models.Lead.objects.create(
                data=data,
            )
            return {'success': True}
        else:
            data["no_campaign"] = False
            lead = leads_models.Lead.objects.create(
                data=data,
            )
        data['mobile'] = data['mobile'][-10:]
        data['mid'] = core_forms.RequirementForm.get_mid_obj(data).id
        adata = {
            'user': self.request.user.advisor.last(),
            'data': data
        }
        act = activity.create_crm_activity(adata)
        if act:
            act.end_time = timezone.now()
            act.save()
            req = act.requirements.last()
            rdata = {'requirement': req}
            queue = model_router.RouterUtils('crm').get_matching_router(
                router_queryset=crm_models.QueueRouter.objects.all(),
                rdata=rdata
            ).queue
            task = crm_models.Task.objects.create(
                category=crm_models.TaskCategory.objects.get(name='OUTGOING_CALL'),
                assigned_to=advisor,
                scheduled_time=timezone.now(),
                pushed_time=timezone.now(),
                from_crm_activity=act,
                queue=queue,
                is_disposed=False
            )
            req.next_crm_task = task
            req.owner = advisor
            req.save()
            req.tasks.add(task)

            # TODO: use mobile searchin.copy()g
            reqs = crm_utils.requirement_searcher(
                {'customer_mobile': data['mobile']}
            ).filter(kind=req.kind).exclude(
                current_state__name__startswith='CLOSED'
            )
            task.requirements.add(*reqs)
        else:
            logger.error("Activity not generated for lead (%s)" % lead)
            return {'success': False, 'results': None}

        return {'success': True, 'results': req.id}
