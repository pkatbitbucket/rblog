# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
import datetime
from django.conf import settings
import django.contrib.postgres.fields.hstore
import django.contrib.postgres.fields.ranges


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('last_modified_on', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
                ('category', models.CharField(max_length=10, null=True, blank=True)),
                ('time_range', django.contrib.postgres.fields.ranges.DateTimeRangeField()),
                ('data', jsonfield.fields.JSONField()),
                ('created_by', models.ForeignKey(related_name='activity_created', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AdvisorProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('last_modified_on', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
                ('shift_start_time', models.TimeField(default=datetime.time(8, 0))),
                ('shift_end_time', models.TimeField(default=datetime.time(22, 0))),
                ('did', models.CharField(max_length=15, null=True)),
                ('phone', models.CharField(max_length=10, null=True)),
                ('extension_no', models.CharField(max_length=15, null=True)),
                ('created_by', models.ForeignKey(related_name='advisorprofile_created', to=settings.AUTH_USER_MODEL, null=True)),
                ('last_modified_by', models.ForeignKey(related_name='advisorprofile_last_modified', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Communication',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('last_modified_on', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
                ('raw_text', models.TextField(null=True, verbose_name='Raw Text', blank=True)),
                ('params', django.contrib.postgres.fields.hstore.HStoreField()),
                ('created_by', models.ForeignKey(related_name='communication_created', to=settings.AUTH_USER_MODEL, null=True)),
                ('last_modified_by', models.ForeignKey(related_name='communication_last_modified', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CommunicationTemplate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('last_modified_on', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
                ('name', models.CharField(unique=True, max_length=100, verbose_name='Name')),
                ('category', models.CharField(max_length=50, verbose_name='Category', choices=[(b'SMS', b'SMS'), (b'EMAIL', b'Email')])),
                ('template', models.TextField(null=True, verbose_name='Template', blank=True)),
                ('trigger_type', models.CharField(default=b'AUTOMATED', max_length=30, verbose_name='Trigger Type', choices=[(b'AUTOMATED', b'Automated'), (b'MANUAL', b'Manual')])),
                ('created_by', models.ForeignKey(related_name='communicationtemplate_created', to=settings.AUTH_USER_MODEL, null=True)),
                ('last_modified_by', models.ForeignKey(related_name='communicationtemplate_last_modified', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CommunicationTemplateRouter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('params', jsonfield.fields.JSONField()),
                ('module', models.CharField(db_index=True, max_length=255, null=True, blank=True)),
                ('serial', models.IntegerField()),
                ('template', models.ForeignKey(to='crm.CommunicationTemplate')),
            ],
        ),
        migrations.CreateModel(
            name='Disposition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('verbose', models.CharField(unique=True, max_length=200, verbose_name='Verbose')),
                ('value', models.CharField(unique=True, max_length=100, verbose_name='Value')),
            ],
        ),
        migrations.CreateModel(
            name='Queue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('last_modified_on', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
                ('name', models.CharField(unique=True, max_length=100)),
                ('code', models.CharField(unique=True, max_length=100)),
                ('description', models.TextField()),
                ('created_by', models.ForeignKey(related_name='queue_created', to=settings.AUTH_USER_MODEL, null=True)),
                ('last_modified_by', models.ForeignKey(related_name='queue_last_modified', to=settings.AUTH_USER_MODEL, null=True)),
                ('users', models.ManyToManyField(related_name='queues', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='QueueRouter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('params', jsonfield.fields.JSONField()),
                ('module', models.CharField(db_index=True, max_length=255, null=True, blank=True)),
                ('serial', models.IntegerField()),
                ('queue', models.ForeignKey(to='auth.Group')),
            ],
        ),
        migrations.CreateModel(
            name='SubDisposition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('verbose', models.CharField(unique=True, max_length=200, verbose_name='Verbose')),
                ('value', models.CharField(unique=True, max_length=100, verbose_name='Value')),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('last_modified_on', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
                ('scheduled_time', models.DateTimeField()),
                ('pushed_time', models.DateTimeField(null=True)),
                ('extra', jsonfield.fields.JSONField()),
                ('assigned_to', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='TaskCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('queue', models.ManyToManyField(related_name='task_categories', to='auth.Group')),
            ],
        ),
        migrations.CreateModel(
            name='UserShiftException',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('last_modified_on', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
                ('date', models.DateField()),
                ('start_time', models.TimeField(default=datetime.time(8, 0))),
                ('end_time', models.TimeField(default=datetime.time(22, 0))),
                ('created_by', models.ForeignKey(related_name='usershiftexception_created', to=settings.AUTH_USER_MODEL, null=True)),
                ('last_modified_by', models.ForeignKey(related_name='usershiftexception_last_modified', to=settings.AUTH_USER_MODEL, null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='task',
            name='category',
            field=models.ForeignKey(to='crm.TaskCategory'),
        ),
        migrations.AddField(
            model_name='task',
            name='created_by',
            field=models.ForeignKey(related_name='task_created', to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='task',
            name='from_activity',
            field=models.ForeignKey(related_name='created_tasks', to='crm.Activity', null=True),
        ),
        migrations.AddField(
            model_name='task',
            name='last_modified_by',
            field=models.ForeignKey(related_name='task_last_modified', to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='disposition',
            name='sub_dispositions',
            field=models.ManyToManyField(related_name='head_dispositions', to='crm.SubDisposition'),
        ),
        migrations.AddField(
            model_name='communication',
            name='template',
            field=models.ForeignKey(blank=True, to='crm.CommunicationTemplate', null=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='disposition',
            field=models.ForeignKey(related_name='activities', to='crm.Disposition'),
        ),
        migrations.AddField(
            model_name='activity',
            name='last_modified_by',
            field=models.ForeignKey(related_name='activity_last_modified', to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='primary_task',
            field=models.ForeignKey(related_name='created_activity', to='crm.Task'),
        ),
        migrations.AddField(
            model_name='activity',
            name='sub_dispositions',
            field=models.ManyToManyField(related_name='activities', to='crm.SubDisposition'),
        ),
        migrations.AddField(
            model_name='activity',
            name='tasks_disposed',
            field=models.ManyToManyField(related_name='disposed_by_activities', to='crm.Task'),
        ),
        migrations.AlterUniqueTogether(
            name='usershiftexception',
            unique_together=set([('user', 'date')]),
        ),
    ]
