# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('statemachine', '0002_auto_20151219_2045'),
        ('crm', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='StateMachineTaskCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('machine', models.ForeignKey(to='statemachine.Machine')),
                ('state', models.ForeignKey(to='statemachine.State')),
                ('task_category', models.ForeignKey(to='crm.TaskCategory')),
            ],
        ),
        migrations.RemoveField(
            model_name='activity',
            name='created_by',
        ),
        migrations.RemoveField(
            model_name='activity',
            name='created_on',
        ),
        migrations.RemoveField(
            model_name='activity',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='activity',
            name='last_modified_by',
        ),
        migrations.RemoveField(
            model_name='activity',
            name='last_modified_on',
        ),
        migrations.AddField(
            model_name='activity',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
