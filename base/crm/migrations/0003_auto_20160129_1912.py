# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import core.models
from django.utils.timezone import utc
import django_pgjson.fields
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('statemachine', '0003_auto_20160129_1912'),
        ('core', '0039_auto_20160129_1912'),
        ('crm', '0002_auto_20151219_2045'),
    ]

    operations = [
        migrations.CreateModel(
            name='Advisor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('notes', models.TextField()),
                ('shift_start_time', models.TimeField(default=datetime.time(8, 0))),
                ('shift_end_time', models.TimeField(default=datetime.time(22, 0))),
                ('dialer_username', models.CharField(max_length=30, blank=True)),
                ('dialer_did', models.CharField(max_length=10, blank=True)),
                ('dialer_extension', models.CharField(max_length=10, blank=True)),
                ('is_available', models.BooleanField(default=True)),
                ('creator', models.ForeignKey(related_name='crm_advisor_created', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('editor', models.ForeignKey(related_name='crm_advisor_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('user', models.ForeignKey(related_name='advisor', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='AdvisorDialerActivity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(max_length=100, db_index=True)),
                ('created_on', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('advisor', models.ForeignKey(related_name='dialer_activities', to='crm.Advisor')),
            ],
        ),
        migrations.CreateModel(
            name='AdvisorShiftException',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('date', models.DateField()),
                ('start_time', models.TimeField(default=datetime.time(8, 0))),
                ('end_time', models.TimeField(default=datetime.time(22, 0))),
                ('advisor', models.ForeignKey(to='crm.Advisor')),
                ('creator', models.ForeignKey(related_name='crm_advisorshiftexception_created', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('editor', models.ForeignKey(related_name='crm_advisorshiftexception_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='DialerQueue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
                ('advisors', models.ManyToManyField(related_name='dialer_queues', to='crm.Advisor')),
            ],
        ),
        migrations.CreateModel(
            name='MachineStateSubdisposition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
                ('creator', models.ForeignKey(related_name='crm_machinestatesubdisposition_created', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('editor', models.ForeignKey(related_name='crm_machinestatesubdisposition_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('machine', models.ForeignKey(to='statemachine.Machine')),
                ('state', models.ForeignKey(to='statemachine.State')),
            ],
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='QueueAdvisor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('is_manager', models.BooleanField(default=False)),
                ('advisor', models.ForeignKey(to='crm.Advisor')),
                ('creator', models.ForeignKey(related_name='crm_queueadvisor_created', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('editor', models.ForeignKey(related_name='crm_queueadvisor_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.RemoveField(
            model_name='advisorprofile',
            name='created_by',
        ),
        migrations.RemoveField(
            model_name='advisorprofile',
            name='last_modified_by',
        ),
        migrations.RemoveField(
            model_name='communication',
            name='created_by',
        ),
        migrations.RemoveField(
            model_name='communication',
            name='last_modified_by',
        ),
        migrations.RemoveField(
            model_name='communication',
            name='template',
        ),
        migrations.RemoveField(
            model_name='communicationtemplate',
            name='created_by',
        ),
        migrations.RemoveField(
            model_name='communicationtemplate',
            name='last_modified_by',
        ),
        migrations.RemoveField(
            model_name='communicationtemplaterouter',
            name='template',
        ),
        migrations.AlterUniqueTogether(
            name='usershiftexception',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='usershiftexception',
            name='created_by',
        ),
        migrations.RemoveField(
            model_name='usershiftexception',
            name='last_modified_by',
        ),
        migrations.RemoveField(
            model_name='usershiftexception',
            name='user',
        ),
        migrations.RenameField(
            model_name='queue',
            old_name='created_on',
            new_name='created_date',
        ),
        migrations.RenameField(
            model_name='queue',
            old_name='last_modified_on',
            new_name='modified_date',
        ),
        migrations.RenameField(
            model_name='task',
            old_name='from_activity',
            new_name='from_crm_activity',
        ),
        migrations.RemoveField(
            model_name='activity',
            name='sub_dispositions',
        ),
        migrations.RemoveField(
            model_name='activity',
            name='time_range',
        ),
        migrations.RemoveField(
            model_name='queue',
            name='created_by',
        ),
        migrations.RemoveField(
            model_name='queue',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='queue',
            name='last_modified_by',
        ),
        migrations.RemoveField(
            model_name='queue',
            name='users',
        ),
        migrations.RemoveField(
            model_name='task',
            name='created_by',
        ),
        migrations.RemoveField(
            model_name='task',
            name='created_on',
        ),
        migrations.RemoveField(
            model_name='task',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='task',
            name='last_modified_by',
        ),
        migrations.RemoveField(
            model_name='task',
            name='last_modified_on',
        ),
        migrations.RemoveField(
            model_name='taskcategory',
            name='queue',
        ),
        migrations.AddField(
            model_name='activity',
            name='end_time',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='start_time',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='activity',
            name='sub_disposition',
            field=models.ForeignKey(related_name='activities', to='crm.SubDisposition', null=True),
        ),
        migrations.AddField(
            model_name='disposition',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 29, 13, 42, 24, 16338, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='disposition',
            name='creator',
            field=models.ForeignKey(related_name='crm_disposition_created', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='disposition',
            name='editor',
            field=models.ForeignKey(related_name='crm_disposition_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='disposition',
            name='modified_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 29, 13, 42, 26, 49680, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='queue',
            name='creator',
            field=models.ForeignKey(related_name='crm_queue_created', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='queue',
            name='editor',
            field=models.ForeignKey(related_name='crm_queue_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='subdisposition',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 29, 13, 42, 29, 899765, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='subdisposition',
            name='creator',
            field=models.ForeignKey(related_name='crm_subdisposition_created', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='subdisposition',
            name='editor',
            field=models.ForeignKey(related_name='crm_subdisposition_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='subdisposition',
            name='modified_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 29, 13, 42, 32, 255212, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='task',
            name='from_customer_activity',
            field=models.ForeignKey(related_name='created_tasks', to='core.Activity', null=True),
        ),
        migrations.AddField(
            model_name='task',
            name='queue',
            field=models.ForeignKey(related_name='tasks', to='crm.Queue', null=True),
        ),
        migrations.AddField(
            model_name='taskcategory',
            name='queues',
            field=models.ManyToManyField(related_name='task_categories', to='crm.Queue'),
        ),
        migrations.AlterField(
            model_name='activity',
            name='category',
            field=models.CharField(db_index=True, max_length=100, null=True, blank=True),
        ),
        migrations.RemoveField(
            model_name='activity',
            name='data',
        ),
        migrations.AddField(
            model_name='activity',
            name='data',
            field=django_pgjson.fields.JsonBField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='activity',
            name='disposition',
            field=models.ForeignKey(related_name='activities', to='crm.Disposition', null=True),
        ),
        migrations.AlterField(
            model_name='activity',
            name='primary_task',
            field=models.ForeignKey(related_name='created_activity', to='crm.Task', null=True),
        ),
        migrations.AlterField(
            model_name='activity',
            name='user',
            field=models.ForeignKey(related_name='activities', to='crm.Advisor', null=True),
        ),
        migrations.AlterField(
            model_name='disposition',
            name='value',
            field=models.CharField(unique=True, max_length=100, verbose_name='Value', db_index=True),
        ),
        migrations.AlterField(
            model_name='disposition',
            name='verbose',
            field=models.CharField(max_length=200, verbose_name='Verbose'),
        ),
        migrations.RemoveField(
            model_name='queuerouter',
            name='params',
        ),
        migrations.AddField(
            model_name='queuerouter',
            name='params',
            field=django_pgjson.fields.JsonField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='queuerouter',
            name='queue',
            field=models.ForeignKey(to='crm.Queue'),
        ),
        migrations.AlterField(
            model_name='subdisposition',
            name='value',
            field=models.CharField(unique=True, max_length=100, verbose_name='Value', db_index=True),
        ),
        migrations.AlterField(
            model_name='subdisposition',
            name='verbose',
            field=models.CharField(max_length=200, verbose_name='Verbose'),
        ),
        migrations.AlterField(
            model_name='task',
            name='assigned_to',
            field=models.ForeignKey(related_name='assigned_tasks', to='crm.Advisor', null=True),
        ),
        migrations.RemoveField(
            model_name='task',
            name='extra',
        ),
        migrations.AddField(
            model_name='task',
            name='extra',
            field=django_pgjson.fields.JsonField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='taskcategory',
            name='name',
            field=models.CharField(unique=True, max_length=100, db_index=True),
        ),
        migrations.DeleteModel(
            name='AdvisorProfile',
        ),
        migrations.DeleteModel(
            name='Communication',
        ),
        migrations.DeleteModel(
            name='CommunicationTemplate',
        ),
        migrations.DeleteModel(
            name='CommunicationTemplateRouter',
        ),
        migrations.DeleteModel(
            name='UserShiftException',
        ),
        migrations.AddField(
            model_name='queueadvisor',
            name='queue',
            field=models.ForeignKey(to='crm.Queue'),
        ),
        migrations.AddField(
            model_name='machinestatesubdisposition',
            name='sub_disposition',
            field=models.ForeignKey(to='crm.SubDisposition'),
        ),
        migrations.AddField(
            model_name='machinestatesubdisposition',
            name='trigger',
            field=models.ForeignKey(to='statemachine.Trigger'),
        ),
        migrations.AddField(
            model_name='queue',
            name='advisors',
            field=models.ManyToManyField(related_name='queues', through='crm.QueueAdvisor', to='crm.Advisor'),
        ),
        migrations.AlterUniqueTogether(
            name='machinestatesubdisposition',
            unique_together=set([('state', 'sub_disposition', 'machine')]),
        ),
        migrations.AlterUniqueTogether(
            name='advisorshiftexception',
            unique_together=set([('advisor', 'date')]),
        ),
    ]
