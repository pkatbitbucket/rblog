# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0003_auto_20160129_1912'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='disposition',
            options={'ordering': ['serial']},
        ),
        migrations.AddField(
            model_name='disposition',
            name='serial',
            field=models.IntegerField(default=1),
        ),
    ]
