# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0005_auto_20160202_1128'),
    ]

    operations = [
        migrations.AddField(
            model_name='dialerqueue',
            name='queue_id',
            field=models.CharField(max_length=20, blank=True),
        ),
    ]
