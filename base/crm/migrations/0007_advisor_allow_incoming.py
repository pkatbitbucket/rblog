# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0006_dialerqueue_queue_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='advisor',
            name='allow_incoming',
            field=models.NullBooleanField(),
        ),
    ]
