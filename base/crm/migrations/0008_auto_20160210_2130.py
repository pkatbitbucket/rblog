# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0052_auto_20160210_2005'),
        ('crm', '0007_advisor_allow_incoming'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='disposed_by_customer_activity',
            field=models.ForeignKey(related_name='disposed_tasks', to='core.Activity', null=True),
        ),
        migrations.AddField(
            model_name='task',
            name='is_disposed',
            field=models.NullBooleanField(),
        ),
    ]
