# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0008_auto_20160210_2130'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
