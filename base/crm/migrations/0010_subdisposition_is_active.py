# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0009_task_created_on'),
    ]

    operations = [
        migrations.AddField(
            model_name='subdisposition',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
