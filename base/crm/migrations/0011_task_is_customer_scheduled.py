# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0010_subdisposition_is_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='is_customer_scheduled',
            field=models.NullBooleanField(),
        ),
    ]
