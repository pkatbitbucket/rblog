# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def insert_initial_crm_conditions(apps, schema_editor):
    module_name = 'crm.dynamic_imports.queuerouter_modules'
    condition_model = apps.get_model('crm', 'Condition')
    queuerouter_model = apps.get_model('crm', 'QueueRouter')
    for qr in queuerouter_model.objects.all():
        condition, _ = condition_model.objects.get_or_create(
            name=qr.module,
            module=module_name,
            func_name=qr.module,
        )
        qr.score = qr.serial
        qr.save()
        qr.conditions.add(condition)


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0011_task_is_customer_scheduled'),
    ]

    operations = [
        migrations.CreateModel(
            name='Condition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100, db_index=True)),
                ('description', models.TextField(blank=True)),
                ('module', models.CharField(max_length=255, db_index=True)),
                ('func_name', models.CharField(max_length=100, db_index=True)),
            ],
        ),
        migrations.AddField(
            model_name='queuerouter',
            name='score',
            field=models.IntegerField(default=1),
        ),
        migrations.AlterUniqueTogether(
            name='condition',
            unique_together=set([('module', 'func_name')]),
        ),
        migrations.AddField(
            model_name='queuerouter',
            name='conditions',
            field=models.ManyToManyField(related_name='condition_for_queuerouters', to='crm.Condition', blank=True),
        ),
        migrations.RunPython(
            code=insert_initial_crm_conditions,
            reverse_code=migrations.RunPython.noop,
        ),
    ]
