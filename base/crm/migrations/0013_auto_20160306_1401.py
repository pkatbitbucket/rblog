# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0012_auto_20160303_1811'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='queuerouter',
            name='module',
        ),
        migrations.RemoveField(
            model_name='queuerouter',
            name='params',
        ),
        migrations.RemoveField(
            model_name='queuerouter',
            name='serial',
        ),
    ]
