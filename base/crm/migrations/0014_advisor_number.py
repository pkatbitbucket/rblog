# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0013_auto_20160306_1401'),
    ]

    operations = [
        migrations.AddField(
            model_name='advisor',
            name='number',
            field=models.CharField(max_length=15, null=True, blank=True),
        ),
    ]
