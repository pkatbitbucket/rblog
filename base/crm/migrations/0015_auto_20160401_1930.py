# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0014_advisor_number'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='advisor',
            options={'ordering': ['dialer_username'], 'permissions': (('view_ameyo_toolbar', 'Can see ameyo toolbar'),)},
        ),
    ]
