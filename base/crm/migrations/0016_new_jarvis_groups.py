# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.core.management.sql import emit_post_migrate_signal


def emit_post_migrate_to_avoid_breaking_continuity(schema_editor):
    """
    This is so that running migrations together doesn't break, because
    Django adds Permissions in post_migrate signal
    """
    db_alias = schema_editor.connection.alias
    try:
        emit_post_migrate_signal(2, False, 'default', db_alias)
    except TypeError:  # Django < 1.8
        emit_post_migrate_signal([], 2, False, 'default', db_alias)


def create_jarvis_groups(apps, schema_editor):

    emit_post_migrate_to_avoid_breaking_continuity(schema_editor)

    Permission = apps.get_model('auth', 'Permission')
    Group = apps.get_model('auth', 'Group')

    ameyo_access_perm = Permission.objects.get(codename='view_ameyo_toolbar')

    cd_users, _ = Group.objects.get_or_create(name='JARVIS_CARDEKHO_USERS')

    cf_users, _ = Group.objects.get_or_create(name='JARVIS_COVERFOX_USERS')
    cf_users.permissions.add(ameyo_access_perm)

    super_users, _ = Group.objects.get_or_create(name='JARVIS_SUPER_USERS')
    super_users.permissions.add(ameyo_access_perm)


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0015_auto_20160401_1930'),
    ]

    operations = [
        migrations.RunPython(create_jarvis_groups, migrations.RunPython.noop)
    ]
