# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def add_jarvis_users_to_group(apps, schema_editor):
    Group = apps.get_model('auth', 'Group')
    Advisor = apps.get_model('crm', 'Advisor')

    cf_users_group, _ = Group.objects.get_or_create(name='JARVIS_COVERFOX_USERS')
    jarvis_advisors = Advisor.objects.filter(user__is_active=True)
    for advisor in jarvis_advisors:
        u = advisor.user
        u.groups.add(cf_users_group)


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0016_new_jarvis_groups'),
    ]

    operations = [
        migrations.RunPython(add_jarvis_users_to_group, migrations.RunPython.noop)
    ]
