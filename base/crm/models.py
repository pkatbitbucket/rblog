import datetime
import logging
from django_pgjson import fields as pgjson_fields

from django.db import models
from django.db.models import Count, Q
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from django.conf import settings
from cfutils import model_router
from cfutils import redis_utils
from core import models as core_models
from statemachine import models as sm_models
from core.activity import create_crm_activity

extlogger = logging.getLogger('extensive')


class ActiveAdvisorManager(models.Manager):

    def get_queryset(self):
        return super(ActiveAdvisorManager, self).get_queryset().filter(
            user__is_active=True
        )


class DefaultAdvisorManager(models.Manager):

    def get_queryset(self):
        return super(DefaultAdvisorManager, self).get_queryset()


class Advisor(core_models.BaseAuthorModel):
    user = models.ForeignKey('core.User', null=True, related_name='advisor')
    notes = models.TextField()
    shift_start_time = models.TimeField(default=datetime.time(8, 0, 0))
    shift_end_time = models.TimeField(default=datetime.time(22, 0, 0))
    dialer_username = models.CharField(max_length=30, blank=True)
    dialer_did = models.CharField(max_length=10, blank=True)
    dialer_extension = models.CharField(max_length=10, blank=True)
    is_available = models.BooleanField(default=True)
    allow_incoming = models.NullBooleanField()
    number = models.CharField(max_length=15, null=True, blank=True)

    objects = ActiveAdvisorManager()
    default_objects = DefaultAdvisorManager()

    class Meta:
        ordering = ['dialer_username']
        permissions = (
            ("view_ameyo_toolbar", "Can see ameyo toolbar"),
        )

    def __unicode__(self):
        return '%s' % str(self.user)

    def save(self, *args, **kwargs):
        redis_utils.Publisher.rclient.set('did_%s' % self.dialer_did, self.dialer_username)
        super(Advisor, self).save(*args, **kwargs)

    def get_shift(self, date=None):
        """
        returns shift start and end time as a tuple for the date (if provided).
        If date isn't provided, it'll return the shift time for today.
        """
        if not date:
            date = datetime.datetime.now().date()
        try:
            exception_shift = AdvisorShiftException.objects.get(advisor=self,
                                                                date=date)
            return exception_shift.start_time, exception_shift.end_time
        except (ObjectDoesNotExist, MultipleObjectsReturned):
            return self.shift_start_time, self.shift_end_time

    def get_preferred_dialer_queue(self):
        queue = DialerQueue.objects.prefetch_related('advisors').annotate(
            acount=Count('advisors')
        ).filter(
            advisors=self
        ).order_by('acount').last()
        return queue

    def get_preferred_product_type(self):
        last_updated_queue = self.queues.last()
        ptype = 'car'
        if last_updated_queue:
            ptype = last_updated_queue.code.split('-')[0]
            ptype = 'bike' if 'bike' in last_updated_queue.code else ptype
            ptype = 'car' if ptype == 'motor' else ptype
        return ptype

    def get_prefix_for_pattern_based_dialing(self):
        """Return the prefix to be added to the number dialed by user"""
        did = self.dialer_did
        if not did:
            return '1'

        # TODO: What if prefixes go beyond 10?
        pri_prefix_list = [str(l) for l in range(1, 10)]
        if did in pri_prefix_list:
            return did
        else:
            return ''

    def update_available_count(self):
        expertise = self.get_preferred_product_type()
        is_busy = self.activities.filter(requirements__isnull=False, end_time__isnull=True).exists()
        if expertise in ['health', 'car'] and not self.queues.filter(code__contains='qualifier').exists():
            queue = '%s-closure' % expertise
            redis_utils.Publisher.update_advisor_available_count(
                advisor_id=self.id,
                is_busy=is_busy,
                queue=queue
            )

    def pending_task_count(self, states=None):
        query = Q(
            assigned_to=self,
            scheduled_time__lte=timezone.now(),
            next_for_requirements__isnull=False,
        )
        if states:
            query = query & Q(requirements__current_state__in=states)
        pending_task_count = Task.objects.filter(query).exclude(
            is_disposed=True
        ).distinct('id').count()
        return pending_task_count

    def assign_fresh_requirement_on_disposition(self):
        """Assign new task and/or requirement after disposition."""
        task = None
        extlogger.info("Assiging fresh requirement on disposition to advisor=%s" % (self.id))

        pending_fresh_task_count = self.pending_task_count(
            states=list(sm_models.State.objects.filter(name='FRESH_new'))
        )
        if pending_fresh_task_count > settings.PENDING_TASK_LIMIT:
            extlogger.info(
                "Skipping fresh assignment as pending_fresh_task_count=%s for advisor=%s" % (
                    pending_fresh_task_count, self.id
                )
            )
            return None

        if not self.is_available:
            extlogger.info("Skipping fresh assignment as is_available false for advisor=%s" % (self.id))
            return None

        assigned = 0
        while assigned == 0:
            unassigned_tasks = Task.objects.filter(
                queue__in=self.queues.all(),
                assigned_to__isnull=True,
                scheduled_time__lte=timezone.now(),
            ).exclude(is_disposed=True)
            if not unassigned_tasks.exists():
                extlogger.info("Skipping fresh requirement assignment as no unassigned_tasks exist for advisor=%s" % (self.id))
                return None

            unassigned_tasks = unassigned_tasks.order_by('-scheduled_time')
            for task in unassigned_tasks:
                requirements = task.requirements.all()
                if requirements:
                    break
                else:
                    extlogger.info("ERROR: Not able to get requirements for task=%s" % (task.id))

            if not task:
                extlogger.info(
                    (
                        "Skipping fresh requirement assignment as no unassigned_tasks "
                        "with requirements exist for advisor=%s"
                    ) % (self.id)
                )
                return None

            assigned = Task.objects.filter(id=task.id, assigned_to__isnull=True).update(assigned_to=self)

        task.refresh_from_db()
        extlogger.info("Task=%s selected to be pushed to advisor=%s" % (task.id, self.id))
        for requirement in requirements:
            if not requirement.owner:
                requirement.owner = self
                requirement.save()
                extlogger.info("requirement=%s has no owner, owner assigned to advisor=%s" % (requirement.id, self.id))
        task.push_task()
        extlogger.info("Task=%s has been pushed to advisor=%s" % (task.id, self.id))
        return task


class AdvisorShiftException(core_models.BaseAuthorModel):
    advisor = models.ForeignKey('Advisor')
    date = models.DateField()
    start_time = models.TimeField(default=datetime.time(8, 0, 0))
    end_time = models.TimeField(default=datetime.time(22, 0, 0))

    class Meta:
        unique_together = ('advisor', 'date')


class TaskCategory(models.Model):
    name = models.CharField(max_length=100, unique=True, db_index=True)
    queues = models.ManyToManyField('Queue', related_name='task_categories')

    def __unicode__(self):
        return self.name


class Task(models.Model):
    """
    When a task is created, it will be assigned to some queue.
    At the time of task assignment to a user, we'll look into all tasks
    in the user's queue which aren't assigned to anyone and assign it
    to the user.
    """
    category = models.ForeignKey('TaskCategory')
    from_customer_activity = models.ForeignKey('core.Activity', null=True,
                                               related_name='created_tasks')
    from_crm_activity = models.ForeignKey('Activity', null=True,
                                          related_name='created_tasks')
    disposed_by_customer_activity = models.ForeignKey(
        'core.Activity', null=True, related_name='disposed_tasks'
    )
    queue = models.ForeignKey('Queue', null=True, related_name='tasks')
    assigned_to = models.ForeignKey('Advisor', null=True, related_name='assigned_tasks')
    scheduled_time = models.DateTimeField()
    pushed_time = models.DateTimeField(null=True)
    extra = pgjson_fields.JsonField(null=True, blank=True)
    is_disposed = models.NullBooleanField()
    is_customer_scheduled = models.NullBooleanField()
    created_on = models.DateTimeField(auto_now_add=True, null=True)

    def __unicode__(self):
        return "Task - " + str(self.id)

    def set_pushed_time(self):
        if not self.pushed_time:
            self.pushed_time = timezone.now()
            self.save(update_fields=['pushed_time'])

    def push_task(self, command=None):
        for requirement in self.requirements.all():
            command = command or requirement.current_state.name
            if 'QUALIFIED' in command and not command == 'QUALIFIED_new':
                command = 'QUALIFIED'
            command = 'FRESH_new' if command == 'QUALIFIED_new' else command
            redis_utils.Publisher.publish_object(command, 'user_%s' % self.assigned_to.id, requirement)
            extlogger.info(
                "Pushing task=%s | assigned_to=%s | requirement=%s | command=%s" %
                (self.id, self.assigned_to.id, requirement.id, command)
            )
        self.set_pushed_time()


class Activity(models.Model):
    user = models.ForeignKey('Advisor', null=True, related_name='activities')
    category = models.CharField(max_length=100, null=True, blank=True, db_index=True)
    start_time = models.DateTimeField(default=timezone.now)
    end_time = models.DateTimeField(null=True)
    primary_task = models.ForeignKey('Task', related_name='created_activity',
                                     null=True)
    tasks_disposed = models.ManyToManyField(
        'Task',
        related_name='disposed_by_activities'
    )
    disposition = models.ForeignKey('Disposition', related_name='activities', null=True)
    sub_disposition = models.ForeignKey('SubDisposition',
                                        related_name='activities', null=True)
    data = pgjson_fields.JsonBField(null=True, blank=True)

    def __unicode__(self):
        return "Activity - %s" % self.id

    def create_next_task(self, scheduled_time=None, requirements=None):
        from crm import utils as crm_utils

        # TODO: Hack, should not create task if transferring call
        if self.sub_disposition.value == 'CALL_TRANSFER':
            return

        extlogger.info("Creating next task with scheduled_time=%s for activity=%s" % (
            scheduled_time.strftime('%d/%m/%Y %H:%M:%S') if scheduled_time else '',
            self.id
        ))
        if requirements:
            assert isinstance(requirements, models.query.QuerySet)

        requirements = requirements or self.requirements.all()
        requirements.update(next_crm_task=None)

        open_requirements = requirements.exclude(
            current_state__name__startswith='CLOSED'
        ).order_by('-created_date')

        if not open_requirements.exists():
            extlogger.info("No open requirements exist for activity=%s, disposing all next_crm_task to none" % self.id)
            return

        if not scheduled_time:
            if open_requirements[0].current_state.name == 'FRESH_ringing':
                scheduled_time = crm_utils.get_next_call_time_for_no_conversation(
                    requirement=open_requirements.last(),
                    crm_activity=self
                )
            elif self.sub_disposition.value in ['INSPECTION_REQUIRED', 'COLD_TRANSFER']:
                scheduled_time = timezone.now()
            else:
                scheduled_time = timezone.now() + datetime.timedelta(hours=24)

        tc = TaskCategory.objects.get(name='OUTGOING_CALL')  # TODO: Use router later

        rdata = {'requirement': open_requirements[0]}
        queue = model_router.RouterUtils('crm').get_matching_router(
            router_queryset=QueueRouter.objects.all(),
            rdata=rdata
        ).queue

        # set the default assignee of next task as owner, if owner not present then set
        # as the one who performed this activity
        user = open_requirements[0].owner or self.user

        # if the default assignee is not in the queue which can do this task, set the
        # assignee as None
        if user and not queue.advisors.filter(id=user.id):
            user = None

        task = Task.objects.create(
            category=tc,
            queue=queue,
            assigned_to=user,
            scheduled_time=scheduled_time,
            from_crm_activity=self,
            is_disposed=False
        )
        task.requirements.add(*open_requirements)
        extlogger.info("Created task=%s for all open requirements" % task.id)

        # TODO: Bulk update?
        assert open_requirements.count() == 1
        for r in open_requirements:
            r.next_crm_task = task
            r.owner = user
            r.save()
            extlogger.info("For requirement=%s owner=%s next_crm_task=%s" % (
                r.id,
                user.id if user else '',
                task.id,
            ))

    def save_call_form(self, request, call_form, requirement, up):
        """Returns a tuple of is_disposed, is_removed_from_redis"""
        from crm import utils as crm_utils
        from crm import drip_utils

        data = dict(call_form.cleaned_data)

        # set all the other requirements as DUPLICATE
        for r in self.requirements.all():
            if not r.id == requirement.id:
                r.merge(merge_into=requirement)
                r.hide()

        adata = {
            'disposition': Disposition.objects.get(value=data['head_disposition']),
            'sub_disposition': SubDisposition.objects.get(value=data['disposition']),
            'data': data,
            'end_time': timezone.now(),
        }
        if data['disposition'] == 'CALL_TRANSFER':
            mobile = requirement.mini_json().get('mobile') or ''
            redis_utils.Publisher.rclient.set(
                'calltransfer_mobile_%s' % mobile[-10:],
                str(requirement.id),
            )
            if data['reassign_to_id']:
                new_caller = Advisor.objects.get(id=data['reassign_to_id'])
                crm_utils.reassign(requirement, new_caller, customer_phone=mobile)
        extlogger.info(
            (
                "Create CRM activity for activity=%s | disposition=%s | "
                "sub_disposition=%s"
            ) % (
                self.id, data['head_disposition'], data['disposition']
            )
        )
        extlogger.info(
            "phone %s for requirement=%s exists before create_crm_activity" % (
                requirement.mini_json().get('mobile'), requirement.id
            )
        )
        self = create_crm_activity(data=adata, activity=self)

        requirement.refresh_from_db()
        extlogger.info(
            "phone %s for requirement=%s exists after create_crm_activity" % (
                requirement.mini_json().get('mobile'), requirement.id
            )
        )

        pt = self.primary_task
        pt.is_disposed = True
        pt.save()
        extlogger.info("Disposed primary_task=%s for activity=%s" % (pt.id, self.id))
        self.tasks_disposed.add(pt)

        requirement = crm_utils.update_requirement_on_disposition(requirement, data)  # TODO: move this to req engine
        extlogger.info(
            "phone %s for requirement=%s exists after requirement_update" % (
                requirement.mini_json().get('mobile'), requirement.id
            )
        )

        self.user.assign_fresh_requirement_on_disposition()
        redis_utils.Publisher.remove_object('user_%s' % self.user.id, self.primary_task)

        self.create_next_task(scheduled_time=data['later_time'])


class Disposition(core_models.BaseAuthorModel):
    verbose = models.CharField(_('Verbose'), max_length=200)
    value = models.CharField(_('Value'), max_length=100, unique=True, db_index=True)
    serial = models.IntegerField(default=1)
    sub_dispositions = models.ManyToManyField('SubDisposition',
                                              related_name='head_dispositions')

    class Meta:
        ordering = ['serial']

    def __unicode__(self):
        return self.verbose


class SubDisposition(core_models.BaseAuthorModel):
    verbose = models.CharField(_('Verbose'), max_length=200)
    value = models.CharField(_('Value'), max_length=100, unique=True, db_index=True)
    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s - %s" % (self.verbose if self.verbose else self.value, self.is_active)


class MachineStateSubdisposition(core_models.BaseAuthorModel):
    machine = models.ForeignKey('statemachine.Machine')
    state = models.ForeignKey('statemachine.State')
    trigger = models.ForeignKey('statemachine.Trigger')
    sub_disposition = models.ForeignKey('SubDisposition')
    is_active = models.BooleanField(default=True)

    class Meta:
        unique_together = (('state', 'sub_disposition', 'machine'),)

    def __unicode__(self):
        return '%s - %s - %s' % (self.machine.name, self.state.name, self.sub_disposition.verbose)


class QueueRouter(models.Model):
    queue = models.ForeignKey('Queue')
    conditions = models.ManyToManyField(
        'Condition', related_name='condition_for_queuerouters', blank=True)
    score = models.IntegerField(default=1)

    def __unicode__(self):
        return '%s - %s' % (self.score, self.queue.name)


class Condition(models.Model):
    name = models.CharField(max_length=100, db_index=True, unique=True)
    description = models.TextField(blank=True)
    module = models.CharField(max_length=255, db_index=True)
    func_name = models.CharField(max_length=100, db_index=True)

    class Meta:
        unique_together = (('module', 'func_name'),)

    def __unicode__(self):
        return self.name


class Queue(core_models.BaseAuthorModel):
    name = models.CharField(max_length=100, unique=True)
    code = models.CharField(max_length=100, unique=True)
    description = models.TextField()
    advisors = models.ManyToManyField('Advisor', through='QueueAdvisor',
                                      related_name='queues')

    def __unicode__(self):
        return '%s' % self.name


class QueueAdvisor(core_models.BaseAuthorModel):
    queue = models.ForeignKey('Queue')
    advisor = models.ForeignKey('Advisor')
    is_manager = models.BooleanField(default=False)


class StateMachineTaskCategory(models.Model):
    state = models.ForeignKey('statemachine.State')
    machine = models.ForeignKey('statemachine.Machine')
    task_category = models.ForeignKey('crm.TaskCategory')


class DialerQueue(models.Model):
    name = models.CharField(max_length=100, unique=True)
    queue_id = models.CharField(max_length=20, blank=True)
    advisors = models.ManyToManyField('Advisor', related_name='dialer_queues')

    def __unicode__(self):
        return '%s - %s' % (self.queue_id, self.name)


class AdvisorDialerActivity(models.Model):
    advisor = models.ForeignKey('Advisor', related_name='dialer_activities')
    status = models.CharField(max_length=100, db_index=True)
    created_on = models.DateTimeField(auto_now_add=True, db_index=True)

    def save(self, *args, **kwargs):
        super(AdvisorDialerActivity, self).save(*args, **kwargs)
        username = self.advisor.dialer_username
        redis_utils.Publisher.publish(
            'ameyo_%s' % username, 'advisor_changed_status',
            {'username': username, 'status': self.status}
        )
