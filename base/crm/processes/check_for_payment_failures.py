#!/usr/bin/env python
import os
import django
from django.utils import timezone
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')  # noqa
django.setup()  # noqa

from django.conf import settings
from django.core.mail import send_mail
from core import models as core_models
from crm import models as crm_models
from email_subscribers import ALERT_SUBSCRIBERS


def main():
    trigger_enter_start_time = timezone.now() - timezone.timedelta(minutes=10)
    payment_failures_state_history = core_models.RequirementStateHistory.objects.filter(
        enter_time__gt=trigger_enter_start_time,
        enter_time__lte=timezone.now(),
        state__name='QUALIFIED_paymentfailed',
    ).values_list('requirement', flat=True)
    payment_failures = core_models.Requirement.objects.filter(
        id__in=payment_failures_state_history,
        current_state__name='QUALIFIED_paymentfailed'
    ).distinct('id')
    for req in payment_failures:
        mobile = req.mini_json().get('mobile')
        pt = req.next_crm_task
        from_web_activity = req.activity_set.filter(
            activity_type='failure',
            page_id='payment'
        ).last()
        if pt:
            pt.is_disposed = True
            pt.disposed_by_customer_activity = from_web_activity
            pt.save()

        task = crm_models.Task.objects.create(
            category=crm_models.TaskCategory.objects.get(name='OUTGOING_CALL'),
            assigned_to=req.owner,
            scheduled_time=timezone.now(),
            from_customer_activity=from_web_activity,
            is_disposed=False
        )
        req.next_crm_task = task
        req.save()
        req.tasks.add(task)

        email_body = """
            Requirement Id=%s.
            Assigned To=%s.
            Phone Number=%s.
        """ % (req.id, req.owner, mobile)

        print """
            Payment Failure for Requirement=%s with %s at %s
        """ % (req.id, mobile, timezone.localtime(timezone.now()))
        send_mail(
            'ALERT - Payment Failure',
            email_body,
            settings.DEFAULT_FROM_EMAIL,
            ALERT_SUBSCRIBERS[req.kind.product]
        )


main()
