#!/usr/bin/env python
import os
import django
from django.utils import timezone
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')  # noqa
django.setup()  # noqa

from django.conf import settings
from django.core.mail import send_mail
from core import models as core_models
from crm import models as crm_models
from core.activity import create_crm_activity
from email_subscribers import ALERT_SUBSCRIBERS


def main():
    trigger_enter_start_time = timezone.now() - timezone.timedelta(minutes=20)
    trigger_enter_end_time = timezone.now() - timezone.timedelta(minutes=10)
    tr_drops = core_models.Requirement.objects.filter(
        requirementstatehistory__enter_time__gte=trigger_enter_start_time,
        requirementstatehistory__enter_time__lt=trigger_enter_end_time,
        requirementstatehistory__state__name='QUALIFIED_paymentgateway',
    ).exclude(
        current_state__name__in=[
            'QUALIFIED_proposalfailed',
            'QUALIFIED_paymentfailed',
            'QUALIFIED_transactiondrop',
            'POLICYPENDING_paymentdone',
            'CLOSED_resolved',
            'CLOSED_unresolved',
        ],
    )
    for req in tr_drops:
        mobile = req.mini_json().get('mobile')
        pt = req.next_crm_task
        adata = {
            'user': crm_models.Advisor.objects.get(dialer_username='system'),
            'category': 'TRANSACTION_CHECK',
            'start_time': timezone.now(),
            'end_time': timezone.now(),
            'sub_disposition': crm_models.SubDisposition._objects.get(value='TRANSACTION_DROP'),
            'disposition': crm_models.Disposition.objects.get(value='QUALIFIED'),
            'primary_task': pt,
            'data': {
                'mobile': req.mini_json().get('mobile'),
                'campaign': req.kind.product
            },
        }

        act = create_crm_activity(data=adata)
        req.refresh_from_db()
        if pt:
            pt.is_disposed = True
            pt.save()
            pt.disposed_by_activities.add(act)

        task = crm_models.Task.objects.create(
            category=crm_models.TaskCategory.objects.get(name='OUTGOING_CALL'),
            assigned_to=req.owner,
            scheduled_time=timezone.now(),
            from_crm_activity=act,
            is_disposed=False
        )
        req.next_crm_task = task
        req.save()
        req.tasks.add(task)

        email_body = """
            Requirement Id=%s.
            Customer Name=%s.
            Assigned To=%s.
            Phone Number=%s.
        """ % (req.id, req.mini_json().get('name'), req.owner, mobile)

        print """
            Transaction drop for Requirement=%s with %s at %s
        """ % (req.id, mobile, timezone.localtime(timezone.now()))
        send_mail(
            'ALERT - Transaction drop',
            email_body,
            settings.DEFAULT_FROM_EMAIL,
            ALERT_SUBSCRIBERS[req.kind.product]
        )


main()
