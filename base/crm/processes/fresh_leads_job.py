#!/usr/bin/env python
import os
import django
from django.utils import timezone
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()


from django.db.models import Q  # noqa
from cfutils import model_router  # noqa
from core import models as core_models  # noqa
from crm import models as crm_models  # noqa
from crm import drip_utils  # noqa
from crm.processes import data_dicts  # noqa


def main():
    # HACK!!
    crm_models.Task.objects.filter(is_disposed__isnull=True).update(is_disposed=False)

    scheduled_time = timezone.now()
    print "\n\nRunning cron at ", timezone.now()

    fresh_reqs = core_models.Requirement.objects.filter(
        next_crm_task__isnull=True,
        owner__isnull=True,
        current_state__name='FRESH_new'
    )
    tc = crm_models.TaskCategory.objects.get(name='OUTGOING_CALL')

    for r in fresh_reqs:
        r.refresh_from_db()
        if not ((r.user and r.user.phones.last())
                or (r.visitor and r.visitor.phones.last())):
            continue

        if r.next_crm_task:
            print (
                "Skipping requirement=%s as requirement got worked upon in "
                "the loop already. next_crm_task=%s"
            ) % (r.id, r.next_crm_task.id)
            continue

        # check if there are other requirements with same mobile
        u = r.user or r.visitor
        mobiles = u.phones.values_list('number', flat=True)
        if mobiles[0] in data_dicts.EXCLUDE_MOBILES.get(str(r.kind.id), []):
            print "Skipping mobile=%s from exclude mobiles list" % (mobiles[0])
            continue

        q = Q(
            Q(user__phones__number__in=mobiles) | Q(visitor__phones__number__in=mobiles)
        ) & Q(kind__product=r.kind.product) & ~Q(current_state__parent_state__name="CLOSED")

        task = None
        other_requirements = core_models.Requirement.objects.filter(q).exclude(id=r.id)
        if other_requirements.exists():
            existing_task = other_requirements.filter(next_crm_task__isnull=False).values('next_crm_task').first()
            if existing_task:
                task = crm_models.Task.objects.get(id=existing_task.get('next_crm_task'))
                task.scheduled_time = scheduled_time
                task.save()
        else:
            user_id = r.user.customer_id if r.user else None
            tracker_id = r.visitor.session_key if r.visitor else None
            customer_id = user_id or tracker_id

            rdata = r.mini_json()
            rdata.update({k: v for k, v in r.mid.__dict__.items() if isinstance(v, basestring)})

            drip_utils.welcome_customer.delay(
                event='welcome_user_request',
                customer_id=customer_id,
                rdata=rdata,
                activity_id=None
            )

        if not task:
            rdata = {'requirement': r}
            queue = model_router.RouterUtils('crm').get_matching_router(
                router_queryset=crm_models.QueueRouter.objects.all(),
                rdata=rdata
            ).queue
            task = crm_models.Task.objects.create(
                category=tc,
                queue=queue,
                scheduled_time=scheduled_time,
                from_customer_activity=r.last_web_activity,
                is_disposed=False
            )

        created_activities = task.created_activity.all()
        r.crm_activities.add(*created_activities)
        owner = None
        for other_r in other_requirements.all():
            to_add = created_activities.exclude(id__in=other_r.crm_activities.values_list('id', flat=True))
            other_r.crm_activities.add(*to_add)
            if other_r.owner:
                owner = other_r.owner

        task.requirements.add(r)
        r.next_crm_task = task
        r.owner = owner
        r.save()

        task.requirements.add(*other_requirements)
        for other_r in other_requirements.all():
            nct = other_r.next_crm_task
            if nct and not nct.id == task.id:
                nct.is_disposed = True
                nct.disposed_by_customer_activity = r.last_web_activity
                nct.save()
            other_r.next_crm_task = task
            other_r.save()

        if task.queue:
            print 'Pushed Task Id - %s in Queue %s for mobiles %s' % (task.id, task.queue.name, mobiles)
        else:
            print "ERROR: task=%s has no queue" % (task.id)


main()
