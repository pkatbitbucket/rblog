#!/usr/bin/env python
import os
import django
from django.utils import timezone
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()


from crm import models as crm_models


def main():
    print timezone.now()
    pending_tasks = crm_models.Task.objects.filter(
        scheduled_time__lte=timezone.now(),
        assigned_to__isnull=False,
        next_for_requirements__current_state__parent_state__name='QUALIFIED',
    ).exclude(is_disposed=True)
    print "Pushing Tasks: ", pending_tasks.values_list('id', flat=True)
    for pt in pending_tasks:
        pt.push_task()


main()
