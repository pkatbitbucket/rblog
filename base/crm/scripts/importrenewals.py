import os
import csv
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from django.utils import timezone  # NOQA
from crm import models as crm_models  # NOQA
from core import activity as activity_engine  # NOQA

SCHEDULED_TIME = timezone.datetime(2016, 3, 12)
non_imported_leads = []
with open('/tmp/renewals.csv', 'rb') as inf:
    reader = csv.DictReader(inf)
    for row in reader:
        try:
            mobile = row.get('Mobile') or row.get('Mobile2') or row.get('Mobile3') or row.get('Mobile4')
            product = row.get('Product', 'car')
            adata = {
                'user': crm_models.Advisor.objects.get(dialer_username='system'),
                'category': 'LEAD_CREATION',
                'data': {
                    'product': product,
                    'category': 'renewals',
                    'source': '2015renewalsleads',
                    'mobile': mobile,
                    'campaign': product,
                },
            }
            activity = activity_engine.create_crm_activity(data=adata)
            activity.end_time = activity.start_time
            activity.save()

            req = activity.requirements.last()
            req.last_crm_activity = activity
            queue, _ = crm_models.Queue.objects.get_or_create(code=product + "-renewals")
            task = crm_models.Task.objects.create(
                category=crm_models.TaskCategory.objects.get(name='OUTGOING_CALL'),
                assigned_to=None,
                queue=queue,
                scheduled_time=SCHEDULED_TIME,
                from_crm_activity=activity,
                is_disposed=False
            )

            req.next_crm_task = task
            req.tasks.add(task)
            req.save()

        except:
            print "Exception in row "
            print row
            non_imported_leads.append(row.get('Lead ID'))
            pass

print "------------------------------NON IMPORTED LEADS = > "
print non_imported_leads
