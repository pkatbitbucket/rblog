from django.utils import timezone

from celery_app import app
from core import models as core_models
from core import activity as activity_engine
from crm import models as crm_models


@app.task(queue='jarvis')
def check_for_transaction_drop(requirement_id, activity_id):
    req = core_models.Requirement.objects.get(id=requirement_id)
    exclude_states = [
        'QUALIFIED_proposalfailed',
        'QUALIFIED_paymentfailed',
        'QUALIFIED_transactiondrop',
        'POLICYPENDING_paymentdone',
        'CLOSED_resolved',
        'CLOSED_unresolved',
    ]
    current_state = req.current_state.name
    if current_state not in exclude_states:
        req = core_models.Requirement.objects.get(id=requirement_id)
        mobile = req.mini_json().get('mobile')
        pt = req.next_crm_task

        adata = {
            'user': crm_models.Advisor.objects.get(dialer_username='system'),
            'category': 'TRANSACTION_CHECK',
            'start_time': timezone.now(),
            'end_time': timezone.now(),
            'sub_disposition': crm_models.SubDisposition.objects.get(value='TRANSACTION_DROP'),
            'disposition': crm_models.Disposition.objects.get(value='QUALIFIED'),
            'primary_task': pt,
            'data': {
                'mobile': mobile,
                'campaign': req.kind.product
            },
        }
        act = activity_engine.create_crm_activity(data=adata)

        req.refresh_from_db()
        if pt:
            pt.is_disposed = True
            pt.save()
            pt.disposed_by_activities.add(act)

        task = crm_models.Task.objects.create(
            category=crm_models.TaskCategory.objects.get(name='OUTGOING_CALL'),
            assigned_to=req.owner,
            scheduled_time=timezone.now(),
            from_crm_activity=act,
            is_disposed=False
        )
        req.next_crm_task = task
        req.save(update_fields=['next_crm_task'])
        req.tasks.add(task)
