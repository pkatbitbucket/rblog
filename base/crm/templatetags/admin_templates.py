from django import template
from django.template.defaultfilters import stringfilter
from django.template import Context
from django.utils.safestring import mark_safe
from django.template.loader import get_template

register = template.Library()


@register.filter
@stringfilter
def render_admin_list(model_name):
    template = get_template('crm/snippets/admin-list.html')
    html = template.render(Context({'model_name': model_name}))
    return mark_safe(html)
