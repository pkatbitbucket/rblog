from django.template import Library

register = Library()


@register.filter
def get_type(value):
    return type(value)


@register.filter
def to_int(value):
    return int(value)
