from django.test import SimpleTestCase

import datetime

from core import models as core_models
from statemachine.models import State
from crm.models import Activity as CRMActivity, Advisor, Disposition, SubDisposition
from core.models import Activity, Requirement


class PutilsTestCase(SimpleTestCase):

    def tearDown(self):
        for t in self.tmp:
            t.delete()

    def setUp(self):
        # Send get.
        self.tmp = []
        core_models.Email.objects.all().delete()
        core_models.Phone.objects.all().delete()

        self.u1 = core_models.User.objects.create()
        self.tmp.append(self.u1)

        s1, _ = State.objects.get_or_create(name='FRESH_new')
        self.tmp.append(s1)

        s2, _ = State.objects.get_or_create(name='FRESH_ringing')
        self.tmp.append(s2)

        self.k1, _ = core_models.RequirementKind.objects.get_or_create(kind='new_policy', product='motor')
        self.tmp.append(self.k1)

        self.u2 = core_models.User.objects.create()
        self.tmp.append(self.u2)

        self.a1 = Advisor.objects.create(user=self.u2)
        self.tmp.append(self.a1)

        d, _ = Disposition.objects.get_or_create(value='QUALIFIED')
        self.tmp.append(d)

        sd, _ = SubDisposition.objects.get_or_create(value='FORM_SENT')
        self.tmp.append(sd)

    def create_test_requirement(self, user, state, kind=None):
        r1 = core_models.Requirement.objects.create(
            kind=kind or self.k1,
            user=user,
            current_state=State.objects.get(name=state)
        )
        r1.save()
        self.tmp.append(r1)
        return r1

    def create_test_crm_activity(self, requirement, end_time=None, disposition='QUALIFIED', sub_disposition='FORM_SENT'):
        ca = CRMActivity(
            user=self.a1,
            start_time=datetime.datetime.now() - datetime.timedelta(seconds=10),
            end_time=end_time,
            disposition=Disposition.objects.get(value=disposition),
            sub_disposition=SubDisposition.objects.get(value=sub_disposition)
        )
        ca.save()
        requirement.crm_activities.add(ca)
        self.tmp.append(ca)
        return ca

    def create_test_web_activity(self, requirement):
        ca = Activity(
            user=requirement.user,
            requirement=requirement,
        )
        ca.save()
        self.tmp.append(ca)
        return ca

    def test_get_best_requirement_by_state(self):
        user = self.u1
        r1 = self.create_test_requirement(user, "FRESH_ringing")
        r2 = self.create_test_requirement(user, "FRESH_new")
        r = Requirement.objects.get_best_requirement([r1, r2])
        assert r2.id == r.id

        for i in range(3):
            self.create_test_crm_activity(r1, datetime.datetime.now())
            self.create_test_crm_activity(r2, datetime.datetime.now())

        for i in range(3):
            self.create_test_crm_activity(r1, datetime.datetime.now())
            self.create_test_crm_activity(r2, datetime.datetime.now())

        r = Requirement.objects.get_best_requirement([r1, r2])
        assert r2.id == r.id

    def test_get_best_requirement_by_activities(self):
        user = self.u1
        r1 = self.create_test_requirement(user, "FRESH_ringing")
        r2 = self.create_test_requirement(user, "FRESH_ringing")

        for i in range(3):
            self.create_test_crm_activity(r1, datetime.datetime.now())

        for i in range(2):
            self.create_test_crm_activity(r2, datetime.datetime.now())

        for i in range(3):
            self.create_test_crm_activity(r1, datetime.datetime.now())

        for i in range(2):
            self.create_test_crm_activity(r2, datetime.datetime.now())

        r = Requirement.objects.get_best_requirement([r1, r2])
        assert r1.id == r.id
