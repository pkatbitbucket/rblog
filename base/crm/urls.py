from django.conf.urls import url

from cf_fhurl import fhurl as cf_fhurl
from fhurl import fhurl

from core import views as core_views
from crm import views as crm_views
from crm import forms as crm_forms

urlpatterns = [
    url(r'^$', crm_views.logged_in_router, name='logged_in_router'),
    url(r'^forgot-password/$', core_views.forgot_password, name='forgot_password'),
    # User login and registration related
    url(r'^set-rid-cookie/$', crm_views.set_rid_cookie, name='set_rid_cookie'),

    # User dashboards
    url(r'^admin/dashboard/$', crm_views.admin_dashboard, name='admin_dashboard'),
    url(r'^oauth2/token/$', crm_views.send_access_token, name='generate_access_token'),
    url(r'^affiliate-leads/$', crm_views.secret_page, name='secret_page'),  # and also a resource server!
    url(r'^just-dial/affiliate-leads/$', crm_views.just_dial_secret_page, name='just_dial_secret_page'),
    url(r'^slack-data-push/$', crm_views.slack_data_push, name='slack_data_push'),
    # blocking this URL to enable the OLD LMS Manager Dashboard
    # url(r'^manager/dashboard/$', crm_views.manager_dashboard,
    #    name='manager_dashboard'),
    url(r'^advisor/dashboard/$', crm_views.advisor_dashboard,
        name='advisor_dashboard'),

    # OLD LMS DASHBOARDS
    url(r'^ajax/save-notes/$', crm_views.ajax_save_notes),
    url(r'^manager/dashboard/$', crm_views.manager_dashboard,
        name='manager_dashboard'),

    url(r'^caller/dashboard/$', crm_views.caller_dashboard,
        name='caller_dashboard'),
    url(r'^ajax/load-initial-calls/$', crm_views.ajax_load_initial_calls,
        name='ajax_load_initial_calls'),
    url(r'^get-incoming-call-popup/$', crm_views.get_incoming_call_popup, name='get_incoming_call_popup'),
    url(r'^get-transfer-call-popup/$', crm_views.get_transfer_call_popup, name='get_transfer_call_popup'),
    url(r'^incoming-task-form/$', crm_views.incoming_task_form, name='get_call_form'),
    url(r'^view-customer-details/(?P<rid>[\w-]+)/$', crm_views.view_customer_details, name='view_customer_details'),
    fhurl(r'^ajax/manager/dashboard/create-lead/$', crm_forms.LeadForm),

    url(r'^ajax/task/(?P<tid>[\w-]+)/$', crm_views.get_task, name='get_task'),
    url(r'^requirement-form/(?P<rid>[\w-]+)/$', crm_views.requirement_form, name='requirement_form'),

    fhurl(r'^ajax/caller/dashboard/search/$', crm_forms.LeadSearchForm, json=True),
    fhurl(r'^ajax/manager/dashboard/search/$', crm_forms.ManagerSearchForm, json=True),
    fhurl(r'^ajax/search-by-number/$', crm_forms.NumberSearchForm, json=True),
    fhurl(r'^ajax/search-customer/$', crm_forms.CustomerSearchForm),
    cf_fhurl(
        r'^ajax/manage-advisors/(?P<uid>[\d-]+)/$', crm_forms.AdvisorManagementForm,
        template='ajax_generic_form.html', json=True
    ),
    cf_fhurl(
        r'^ajax/manage-queues/(?P<qid>[\d-]+)/$', crm_forms.QueueManagementForm,
        template='ajax_generic_form.html', json=True
    ),

    url(r'^ameyo/put-voicemail/$', crm_views.ameyo_put_voicemail),
    url(r'^ameyo/missed-call/$', crm_views.ameyo_missed_call),
    url(r'^ameyo/incoming-call/$', crm_views.ameyo_incoming_call),
    url(r'^ameyo/incoming-lead/$', crm_views.ameyo_incoming_lead),
    fhurl(r'^ajax/advisor-ameyo-activity/$', crm_forms.AdvisorDialerActivityForm, json=True),

    fhurl(r'^send-sms/(?P<rid>[\d-]+)/$', crm_forms.SendSMSForm, template="send_sms_form.html"),
    fhurl(r'^ajax/send-sms/(?P<rid>[\d-]+)/$', crm_forms.SendSMSForm, template="send_sms_form.html", json=True),

    # dashboards
    # Health Summery
    url(r'^manager/dashboard/health/advisor-wise/$', crm_views.manager_dashboard_health_data,
        name='manager_dashboard_health_advisor_wise_report'),
    # Health caller summery
    url(r'^caller/dashboard/health/summary/$', crm_views.caller_dashboard_health_summary,
        name='caller_dashboard_health_summary'),
    url(r'^caller/dashboard/health/qualification-team/$', crm_views.get_health_qualification_team,
        name='caller_dashboard_health_qualification_team'),
    url(r'^likely-to-buy-list/$', crm_views.likely_to_buy_details,
        name='caller_dashboard_likely_to_buy_list'),
    # Likely to buy
    url(r'^manager/dashboard/motor/likely-to-buy-details/$', crm_views.likely_to_buy_details,
        name='manager_dashboard_motor_likely_to_buy_details'),
    # autoassign dashboard
    url(r'^manager/dashboard/motor/summary/$', crm_views.autoassign_requirement_dashboard,
        name='manager_dashboard_motor_autoassign_requirement_dashboard'),
    # TODO: by vishesh
    # advisor_wise_report
    url(r'^manager/dashboard/motor/advisor-wise/$', crm_views.manager_dashboard_motor_data,
        name='manager_dashboard_motor_advisor_wise_report'),
    # customer_req_call_back_dashboard
    url(r'^manager/dashboard/motor/customer-req-callback/$', crm_views.customer_req_call_back_dashboard,
        name='manager_dashboard_motor_customer_req_callback_dashboard'),
    url(r'^caller/dashboard/motor/customer-req-callback/(?P<advisor_id>[\w-]+)/$', crm_views.customer_req_call_back_dashboard,
        name='caller_dashboard_motor_customer_req_callback_dashboard'),
    # ameyo dashboard
    url(r'^manager/dashboard/ameyo/$', crm_views.ameyo_dashboard,
        name='ameyo_dashboard_motor_dashboard'),
    # customer Dashboard
    url(r'^manager/dashboard/customers/$', crm_views.customers_dashboard,
        name='customers_dashboard'),
    # transaction details popup
    # url(r'^view-transaction-details/(?P<pid>[\w-]+)/$', crm_views.view_transaction_details,
    # name='view_transaction_details'),
    # assign requirement from customer dashboard
    url(r'^manager/dashboard/bulk-actions/$', crm_views.manager_dashboard_bulk_actions,
        name='manager_dashboard_bulk_actions'),
    # Generic Functionalities
    url(r'^file/upload/$', crm_views.upload_file, name='upload_file'),
    url(r'^numbers/$', crm_views.numbers, name='numbers'),

    url(r'^queuerouter-dashboard/$', crm_views.queuerouter_dashboard, name='queuerouter_dashboard'),
    cf_fhurl(r'^ajax/queuerouter-dashboard/$', crm_forms.QueueRouterDashboardForm, json=True),
]
