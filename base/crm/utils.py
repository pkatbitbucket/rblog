import datetime
import logging
import random
from urllib import urlencode
import re
import json
import urllib
import unidecode

import requests

from django.core.exceptions import PermissionDenied
from django.core import serializers
from django.db.models import F, Q
from django.db.models.base import ModelBase
from django.core.mail import send_mail
from django.utils import timezone
from django.template import RequestContext
from django.template.loader import render_to_string

from django.conf import settings
from core import activity as activity_engine
from celery_app import app
from core import models as core_models
from crm import models as crm_models
from account_manager import utils as account_utils
from cfutils import search_utils, redis_utils


logger = logging.getLogger(__name__)


SMS_QUERY_URL_PARAMETERS = {
    'utm_source': 'im_sms',
    'utm_medium': 'agent',
    'utm_campaign': 'agent-sms',
    'utm_adgroup': 'nudgespot',
}


def override_scheduled_calls(from_time, to_time, requirement_ids, kind_ids):
    """
    This will work on requirements which have kind_id in kind_ids list.
    Following things will happen:
        1) All requirements with next call time between from_time and to_time
        should have their tasks rescheduled to next_call_time as to_time.
        2) All requirements with requirement.id in requirement_ids should have
        the next call time set as from_time.
    """

    # 1
    q_kind = Q(kind_id__in=kind_ids)
    q1 = q_kind & Q(next_crm_task__scheduled_time__gte=from_time) & Q(next_crm_task__scheduled_time__lte=to_time)
    requirements = core_models.Requirement.objects.filter(q1)
    for r1 in requirements:
        nct = r1.next_crm_task
        if nct:
            nct.scheduled_time = to_time
            nct.save()
            logger.info(
                'Next Task Override: Next call time for requirement {rid} set to {new_time}'.format(
                    rid=r1.id,
                    new_time=nct.scheduled_time.isoformat()
                )
            )
            if nct.assigned_to:
                redis_utils.Publisher.remove_object('user_%s' % nct.assigned_to.id, nct)
        r1.save()

    # 2
    q2 = Q(id__in=requirement_ids)
    for r2 in core_models.Requirement.objects.filter(q2):
        nct = r2.next_crm_task
        if nct and not nct.is_disposed:
            nct.scheduled_time = from_time
            nct.save()
        else:
            nct = crm_models.Task.objects.create(
                category=crm_models.TaskCategory.objects.get(name='OUTGOING_CALL'),
                assigned_to=r2.owner,
                scheduled_time=from_time,
                is_disposed=False
            )
            r2.next_crm_task = nct
            r2.tasks.add(nct)
            logger.info('Next Task Override: Creating new task for requirement %s' % r2.id)

        logger.info(
            'Next Task Override: Next call time for requirement {rid} set to {new_time}'.format(
                rid=r2.id,
                new_time=nct.scheduled_time.isoformat()
            )
        )
        r2.save()


def superuser_only(function):
    def _inner(request, *args, **kwargs):
        if not request.user.is_superuser:
            raise PermissionDenied
        return function(request, *args, **kwargs)
    return _inner


def random_list_item(given_list):
    """
        this function takes a list and return any random element of that list
    """
    return given_list[random.randint(0, len(given_list) - 1)]


# TODO: Make this work, use core.notify
def manual_send_sms(request, to_mobile, sms_type, buy_link, campaigndata):
    return True


# TODO: If services fail, how do we recover, discussion required
def get_short_url(actual_url, params=None):
    """
        this function creates http://cvfx.in hosted shortened url
    """
    if not actual_url.startswith('http'):
        actual_url = 'http://' + actual_url
    if params:
        actual_url = actual_url + '?' + urlencode(params)
    post_url = 'http://cvfx.in/get-short-url/'
    response = requests.post(
        post_url,
        data={'actual_url': actual_url},
        headers={'Content-Type': 'application/x-www-form-urlencoded'}
    )

    return ("http://cvfx.in/" + str(json.loads(response.text)['shorturl_id']))


def dispose_task(form_data, request_user, ticket, task):

    activity = crm_models.Activity(
        time_range=(
            datetime.datetime.now(),
            datetime.datetime.now()
        ),
        primary_task=task,
        disposition=form_data['head_disposition'],
        data=serialize(form_data),
    )
    activity.save(request_user)
    activity.sub_dispositions.add(*form_data['sub_dispositions'])
    activity.tasks_disposed.add(task)
    ticket.activities.add(activity)

    return activity


def serialize(d):

    new_d = {}
    for k, v in d.items():
        if type(type(v)) == ModelBase:
            new_d.update({k: serializers.serialize('python', [v])})
        else:
            new_d.update({k: str(v)})

    return new_d


def slugify(str):
    str = unidecode.unidecode(str).lower()
    return re.sub(r'\W+', '-', str)


def fetch_file_from_url(url, location=None):
    if not location:
        location = '/tmp/filename'
    urllib.urlretrieve(url, location)
    return location


def ui_cleaner(data, product_type, requirement):
    if requirement.last_web_activity:
        for k, v in requirement.last_web_activity.__dict__.items():
            if not k.startswith('_'):
                data[k] = v
    priority = {
        'health': ['user', 'health', 'marketing'],
        'car': ['user', 'car', 'marketing'],
        'bike': ['user', 'bike', 'marketing'],
        'travel': ['user', 'travel', 'marketing'],
        '': ['user', 'car', 'bike', 'health', 'marketing'],
    }.get(product_type.lower(), ['user', 'health', 'car', 'bike', 'marketing', 'home', 'travel'])
    priority_keys = sorted(UI_FIELDS.keys(), key=lambda x: priority.index(x) if x in priority else len(priority))
    sdata = []
    tvars = []
    for k in priority_keys:
        values_data = []
        for i in UI_FIELDS[k]:
            tvars.append(i)
            if data.get(i):
                values_data.append([i, data[i]])
        if len(values_data):
            sdata.append([k, values_data])

    left_over = []
    for i in data.keys():
        if i not in tvars and data.get(i):
            left_over.append([i, data[i]])
    sdata.append(['Misc', left_over])
    if 'vehicle_details' in data.keys():
        for key, value in data['vehicle_details'].items():
            sdata[0][1].append([key, value])
    return sdata


def get_rdata_from_requirement(requirement):
    activities = core_models.Activity.objects.filter(
        requirement=requirement
    ).order_by('created').annotate(data=F('extra')).values('created', 'data')
    for act in activities:
        if act['data']:
            act['data'] = json.loads(act['data'])
        else:
            act['data'] = {}
    crm_activities = requirement.crm_activities.filter(
        end_time__isnull=False
    ).order_by(
        'end_time'
    ).annotate(created=F('end_time')).values('created', 'data')
    for act in crm_activities:
        act['data'] = act['data'] or {}
    activities = list(activities)
    activities.extend(list(crm_activities))
    if activities:
        activities = sorted(activities, key=lambda x: x['created'])
    sdata = {}
    for activity in activities:
        if activity.get('data'):
            sdata.update(activity.get('data'))
    if sdata and requirement.extra:
        sdata.update(requirement.extra)
    if requirement.fd:
        fd_flattened = {k.split('__')[0]: v for k, v in requirement.fd.flatten().items()}
        sdata.update(fd_flattened)
    return sdata


def update_requirement_on_disposition(requirement, data):
    user = requirement.user
    visitor = requirement.visitor
    mj = requirement.mini_json()
    if not user:
        user = account_utils.get_or_create_user(
            mobile=mj['mobile'],
            email=None
        )
        visitor.attach(user)

    if data.get('email'):
        # user.attach_email(data['email'], is_primary=True)
        pass
    user.first_name = data.get('first_name') or ''
    user.middle_name = data.get('middle_name') or ''
    user.last_name = data.get('last_name') or ''
    user.save()
    requirement.refresh_from_db()
    requirement.remark = data.get('notes')
    extra_data = {
        'pincode': data.get('pincode', ''),
        'city': data.get('city', ''),
        'expected_no_of_policies': data.get('expected_no_of_policies', ''),
        'plan_type': data.get('plan_type', ''),
        'expected_premium_amount': data.get('expected_premium_amount', ''),
        'quote_link': data.get('quote_link'),
        'case': data.get('health_case_type'),
        'past_policy_expiry_date': datetime.datetime.strftime(data['past_policy_expiry_date'], '%Y-%m-%d') if data.get('past_policy_expiry_date') else '',
    }
    requirement.update_extra(extra_data)
    requirement.save(update_fields=['extra', 'remark'])
    return requirement


@app.task
def send_mail_celery(subject, body, from_email, to_email, fail_silently=False):
    if not settings.DEBUG:
        send_mail(subject, body, from_email, to_email, fail_silently)
    else:
        send_mail(subject, body, from_email, to_email, fail_silently)
    return 'OK'


UI_FIELDS = {
    'marketing': [
        'utm_keyword',
        'ad-category',
        'utm_source',
        'landing_url',
        'url',
        'banner',
        'utm_adgroup',
        'ad-campaign',
        'network',
        'vt_device',
        'label',
        'utm_medium',
        'vt_creative',
        'utm_campaign',
        'src',
        'vt_target',
        'vt_keyword',
        'campaign',
        'gclid',
        'utm_content',
        'brand',
        'referer',
        'vt_devicemodel',
        'vt_matchtype',
        'vt_source',
        'vt_placement',
        'device',
        'vt_adposition',
    ],
    'health': [
        'proposer_first_name',
        'proposer_state',
        'proposer_gender',
        'proposer_address_landmark',
        'gender',
        'proposer_landline',
        'proposer_address2',
        'proposer_address1',
        'proposer_marital_status',
        'policy_document',
        'father_first_name',
        'members',
        'proposer_city',
        'proposer_date_of_birth',
        'proposer_pincode',
        'no_of_people_insured',
        'extra',
        'policy_token',
        'parents_pincode',
        'nominee_relationship',
        'reference_id',
        'payment_done',
        'policy_start_date',
        'payment_on',
        'proposer_last_name',
        'form_data',
        'hospital_service',
        'user_data',
        'insured_details_json',
        'slab',
        'documents-via',
        'proposer_middle_name',
        'policy_end_date',
        'plan_details',
        'premium_paid',
        'slab_id',
        'Sum_assured',
    ],
    'home': [
        'Built_area',
    ],
    'car': [
        'quote_url',
        'vehicle_engine_no',
        'policy_type',
        'make',
        'model',
        'variant',
        'vehicle_rto',
        'vehicle_reg_no',
        'vehicle_reg_date',
        'policy_claimed',
        'previousNCB',
        'pastPolicyExpiryDate',
        'past_policy_expiry_date',
        'past_policy_insurer',
        'past_policy_cover_type',
        'reg_add_pincode',
        'reg_add_city',
        'form_details',
        'nominee_age',
        'nominee_name',
        'reg_add_building_name',
        'cust_marital_status',
        'vehicle_chassis_no',
        'insurance_status',
        'add_building_name',
        'cust_gender',
        'add_city',
        'add_street_name',
        'inspection_time',
        'inspection_day',
        'policy_copy',
        'reg_add_house_no',
        'reg_add_post_office',
        'reg_add_district',
        'rc_copy',
        'add-same',
        'past_policy_end_date',
        'vehicle_model',
        'member_uuid',
        'past_policy_insurer_city',
        'add_house_no',
        'is_car_financed',
        'past_policy_number',
        'add_post_office',
        'Value_of_content',
        'expiry_month',
        'inspection_date',
        'reg_add_landmark',
        'add_district',
        'car-financier',
        'policy_expiry_month',
        'inspection_address',
        'reg_add_state',
        'reg_add_street_name',
        'add_state',
        'member_id',
        'expiry_date',
        'ownership',
        'past_policy_start_date',
        'vehicle_number',
        'company_option',
        'transaction_url',
    ],
    'travel': [
        'quote_url',
        'Travel_Insurance_for',
        'places',
        'isResident',
        'type',
        'from_date',
        'to_date',
        'adults',
        'trip_type',
        'Travelling_To',
        'travel_date',
        'nominee_relationship',
        'plan_type',
        'salutation',
        'cust_first_name',
        'cust_middle_name',
        'cust_last_name',
        'cust_gender',
        'bool_medication',
        'nominee_name',
        'partner',
        'cust_relationship_with_primary',
        'com_city',
        'plan_grade',
        'cust_passport',
        'cust_occupation',
        'plan_name',
        'insurer_slug',
        'com_address_line_1',
        'com_address_line_2',
        'com_address_line_3',
        'com_state',
        'bool_disease',
        'nominee_age',
        'sum_assured',
        'com_pincode',
        'add-same',
    ],
    'user': [
        'add_pincode',
        'pincode',
        'cust_dob',
        'father_last_name',
        'transaction_id',
        'result_url',
        'name',
        'third_party',
        'no_of_policies_bought',
        'policy_number',
        'payment_response',
        'triggered_page',
        'tracker',
        'email',
        'address',
        'add_landmark',
        'city',
        'user_name',
        'cust_pan',
        'insurer',
        'cust_occupation',
        'product_type',
    ]
}
UI_FIELDS['bike'] = UI_FIELDS['car']
MOTOR_QUOTE_FIELDS_TO_SHOW = ['registrationDate', 'make', 'model',
                              'variant', 'isNewVehicle', 'registrationNumber', 'idv', 'previousNCB']


# FIXME: Customer name search query is making exact search right now, have
# to fix this
requirement_fmap = {
    'id': ['id'],
    'id_list': ['id__in'],
    'customer_name': [
        'user__first_name',
        'user__middle_name',
        'user__last_name',
    ],
    'customer_email': [
        'user__emails__email',
        'visitor__emails__email',
    ],
    'customer_mobile': [
        'user__phones__number',
        'visitor__phones__number',
    ],
    'last_head_disposition': ['last_crm_activity__disposition'],
    'last_call_time_from': ['last_crm_activity__end_time__gte'],
    'last_call_time_to': ['last_crm_activity__end_time__lte'],
    'next_call_time_from': ['next_crm_task__scheduled_time__gte'],
    'next_call_time_to': ['next_crm_task__scheduled_time__lte'],
    'current_state': ['current_state'],
    'parent_state': ['current_state__parent_state'],
    'past_policy_expiry_date_from': ['extra__at_past_policy_expiry_date__gte'],
    'past_policy_expiry_date_to': ['extra__at_past_policy_expiry_date__lte'],
    'owner': ['owner'],
    'queues': ['next_crm_task__queue__in'],
    'created_on_to': ['created_date__lte'],
    'created_on_from': ['created_date__gte'],
}
requirement_searcher = search_utils.FilterMaker.get_searcher(fmap=requirement_fmap, model=core_models.Requirement)


def reassign(requirement, caller, customer_phone=None, additional_params=None):
    call_type = '%s-qualifier' % requirement.kind.product
    data = {
        'id': requirement.id,
        'caller_id': caller.id,
        "customerPhone": customer_phone,
        "additionalParams": additional_params,
        'call_type': call_type
    }
    redis_utils.Publisher.publish('user_%s' % caller.id, 'requirement_transfer_lead', data)
    return {'success': True}


def get_search_json(search_data):
    searched_dict = {}
    for key, value in search_data.items():
        if value:
            try:
                searched_dict.update({str(key): str(value)})
            except:
                value = 'ERROR'
                searched_dict.update({str(key): str(value)})
    return json.dumps(searched_dict) if searched_dict else ''


def get_requirement_mini_dict(requirement):
    rdata = get_rdata_from_requirement(requirement)
    mj = requirement.mini_json()
    mini_dict_map = {
        'health': ['pincode'],
        'car': ['vehicle_reg_no', 'past_policy_expiry_date'],
        'bike': ['vehicle_reg_no', 'past_policy_expiry_date'],
        'travel': ['travel_date'],
        'term': ['pincode'],
        'home': ['id'],
    }
    mini_dict = {arg: rdata.get(arg) for arg in mini_dict_map.get(requirement.kind.product)}
    mini_dict.update({
        'last_call_disposition': mj.get('last_call_disposition'),
    })
    return mini_dict


def render_requirement_form(request, requirement, is_incoming, task_id=None, activity_id=None):
    from crm import forms as crm_forms

    activity_id = activity_id or request.GET['activity']
    task_id = task_id or request.GET['task']

    mj = requirement.mini_json()
    cd_data = get_rdata_from_requirement(requirement)
    quote_link = cd_data.get('quote_link')
    show_quote_link = True if quote_link else False
    product_type = requirement.kind.product
    if cd_data.get('past_policy_expiry_date'):
        expiry_date = cd_data['past_policy_expiry_date']
    else:
        expiry_date = ''

    data_to_autofill_in_call_form = {
        'requirement_id': requirement.id,
        'activity_id': activity_id,
        'task_id': task_id,
        'first_name': mj.get('first_name'),
        'middle_name': mj.get('middle_name'),
        'last_name': mj.get('last_name'),
        'email': mj.get('email'),
        'mobile': mj.get('mobile'),
        'notes': mj.get('notes'),
        'pincode': cd_data.get('pincode', ''),
        'city': cd_data.get('city', ''),
        'expected_no_of_policies': cd_data.get('expected_no_of_policies', ''),
        'plan_type': cd_data.get('plan_type', ''),
        'expected_premium_amount': cd_data.get('expected_premium_amount', ''),
        'quote_link': quote_link,
        'case': cd_data.get('health_case_type'),
        'past_policy_expiry_date': expiry_date,
    }
    call_form = crm_forms.RequirementCallForm(initial=data_to_autofill_in_call_form)
    quote_data = {}
    if cd_data.get('quote'):
        cd_data_quote = cd_data['quote']
        if isinstance(cd_data_quote, (str, unicode)):
            try:
                cd_data_quote = json.loads(cd_data_quote)
            except:
                cd_data_quote = {}
        [quote_data.update({k: v}) for k, v in cd_data_quote.items() if k in MOTOR_QUOTE_FIELDS_TO_SHOW]
    if quote_data.get('isNewVehicle'):
        quote_data.update({'vehicle_status': 'New'}) if bool(
            quote_data.get('isNewVehicle')
        ) else quote_data.update({'vehicle_status': 'Old'})
    quote_data.pop('isNewVehicle', None)
    r_extra = requirement.extra or {}
    if r_extra.get('form_data'):
        for k, v in requirement.fd.get_data().items():
            if isinstance(v, dict):
                r_extra.update(v)
            elif isinstance(v, list):
                for item in v:
                    r_extra.update(item)

    sdata = ui_cleaner(r_extra, product_type, requirement)
    reassign_form = crm_forms.ReassignForm(request)
    reassign_form.map_campaign(requirement)

    if is_incoming:
        visible_disposition_map = {
            str(d.value): [str(sd.value) for sd in d.sub_dispositions.filter(is_active=True)]
            for d in crm_models.Disposition.objects.all()
        }
        return render_to_string("incoming_call_form.html", {
            'task': crm_models.Task.objects.get(id=task_id),
            'activity': crm_models.Activity.objects.get(id=activity_id),
            'call_form': call_form,
            'rid': requirement.id,
            'mobile': mj.get('mobile'),
            'rdata': requirement,
            'data': sdata,
            'product_type': product_type,
            'crtObjectId': request.GET['crtObjectId'],
            'incoming': request.GET['call_type'] if request.GET.get('call_type') else "incoming",
            'DISPOSITION_MAP': visible_disposition_map,
            'allow_close': False,
        }, context_instance=RequestContext(request))
    else:
        return render_to_string('call_form.html', {
            'task': crm_models.Task.objects.get(id=task_id),
            'activity': crm_models.Activity.objects.get(id=activity_id),
            'call_form': call_form,
            'rid': requirement.id,
            'mobile': mj.get('mobile'),
            'rdata': requirement,
            'data': sdata,
            'product_type': product_type,
            'show_quote_link': show_quote_link,
            'quote_url': quote_link,
            'cd_data': cd_data,
            'reassign_form': reassign_form,
            'allow_close': False,
        }, context_instance=RequestContext(request))


def get_next_call_time_for_no_conversation(requirement, crm_activity):
    CALLS_MADE_TO_NEXT_DIFF_TIME = {
        1: 1,
        2: 3,
        3: 6,
        4: 12,
        5: 24,
        6: 48,
    }
    calls_made = requirement.crm_activities.filter(disposition=crm_activity.disposition).count()
    nct_hours = CALLS_MADE_TO_NEXT_DIFF_TIME.get(calls_made, 48)
    return timezone.now() + timezone.timedelta(hours=nct_hours)
