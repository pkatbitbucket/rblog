import logging
import datetime
import time
import json
import uuid
import pytz

from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect, render, get_object_or_404
from django.template import RequestContext
from django.template.loader import render_to_string
from django.http.response import JsonResponse, HttpResponse
from django.core.urlresolvers import reverse
from django.conf import settings
from django.db.models import Count, Q
from django.db import transaction as django_transaction

from core import models as core_models
from core.activity import create_crm_activity
from cfutils import redis_utils, model_router
import ameyo_utils
import models as crm_models
import forms as crm_forms
import utils as crm_utils
from statemachine import forms as sm_forms
from cfutils.aws import s3_upload
from core import activity
from leads import models as leads_models
from statemachine import models as sm_models
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


logger = logging.getLogger('jarvis.requests')
extlogger = logging.getLogger('extensive')
ameyo_logger = logging.getLogger('ameyo')


@login_required
def logged_in_router(request):
    if request.user.is_authenticated():
        return redirect(reverse('caller_dashboard'))
    else:
        return redirect(reverse("login/"))


def reset_password(request):
    return redirect(reverse('core:reset_password'))


@login_required
def advisor_dashboard(request):
    return render(request, "crm/advisor_dashboard.html", {
        'user_role': 'advisor',  # TODO: GROT
    })


@login_required
def admin_dashboard(request):
    permission_form = crm_forms.PermissionForm()
    group_form = crm_forms.GroupForm()
    callback_form = sm_forms.CallbackForm()
    condition_form = sm_forms.ConditionForm()
    state_form = sm_forms.StateForm()
    trigger_form = sm_forms.TriggerForm()
    disposition_form = crm_forms.DispositionForm()
    statedisposition_form = sm_forms.StateDispositionForm()
    transition_form = sm_forms.TransitionForm()
    nontransition_form = sm_forms.NonTransitionForm()
    machine_form = sm_forms.MachineForm()
    subdisposition_form = crm_forms.SubDispositionForm()
    return render(request, "crm/admin_dashboard.html", {
        'permission_form': permission_form,
        'group_form': group_form,
        'callback_form': callback_form,
        'condition_form': condition_form,
        'state_form': state_form,
        'trigger_form': trigger_form,
        'disposition_form': disposition_form,
        'statedisposition_form': statedisposition_form,
        'transition_form': transition_form,
        'nontransition_form': nontransition_form,
        'machine_form': machine_form,
        'subdisposition_form': subdisposition_form,
    })


@login_required
@csrf_exempt
def upload_file(request):

    content = request.FILES['content']
    suffix = datetime.datetime.now().strftime("%d-%m-%Y-%H-%M")
    file_name = content._get_name()
    file_name = file_name.replace(' ', '_')
    new_file_name = "%s_%s" % (suffix, file_name)
    bucket_name = settings.REQUEST_BUCKET
    if settings.USE_AMAZON_S3:
        s3uploader = s3_upload.S3Upload(bucket_name)
        key_url = s3uploader.upload(new_file_name, content)
    else:
        key_url = new_file_name

    return JsonResponse({
        'success': True,
        'file': key_url,
    })


# #### VIEWS SPECIFIC TO OLD LMS BELOW THIS #### #

@csrf_exempt
def ajax_save_notes(request):
    up = crm_models.Advisor.objects.get(user=request.user)
    notes = request.POST['notes']
    up.notes = notes
    up.save()
    return HttpResponse("OK")


@login_required
def manager_dashboard(request):
    queues = advisors = []
    up = crm_models.Advisor.objects.get(user=request.user)
    qad = crm_models.QueueAdvisor.objects.filter(advisor=up, is_manager=True)
    if not qad:
        return JsonResponse({
            'success': False,
            'errors': 'You do not have permission to view this page'
        })
    queues = crm_models.Queue.objects.filter(queueadvisor__in=qad)
    qads = crm_models.QueueAdvisor.objects.filter(
        is_manager=False,
        queue__in=queues
    )
    advisors = crm_models.Advisor.objects.filter(
        user__is_active=True,
        queueadvisor__in=qads
    ).distinct()

    return render(request, "manager_dashboard.html", {
        'user_role': 'MANAGER',  # TODO: GROT
        'search_customer_form': crm_forms.ManagerSearchForm(request),
        'profile': up,
        'advisors': advisors,
        'queues': queues,
        'lead_form': crm_forms.LeadForm(request)
    })


@login_required
def view_customer_details(request, rid):
    requirement = get_object_or_404(core_models.Requirement, pk=rid)
    mj = requirement.mini_json()
    cd_data = crm_utils.get_rdata_from_requirement(requirement)
    quote_link = cd_data.get('quote_link')
    show_quote_link = True if quote_link else False
    product_type = requirement.kind.product

    data_to_autofill_in_call_form = {
        'requirement_id': requirement.id,
        'category': product_type,
        'first_name': mj.get('first_name'),
        'middle_name': mj.get('middle_name'),
        'last_name': mj.get('last_name'),
        'email': mj.get('email'),
        'mobile': mj.get('mobile'),
        'notes': mj.get('notes'),
        'pincode': cd_data.get('pincode', ''),
        'city': cd_data.get('city', ''),
        'expected_no_of_policies': cd_data.get('expected_no_of_policies', ''),
        'plan_type': cd_data.get('plan_type', ''),
        'expected_premium_amount': cd_data.get('expected_premium_amount', ''),
        'case': cd_data.get('health_case_type')
    }
    call_form = crm_forms.RequirementCallForm(initial=data_to_autofill_in_call_form)

    quote_data = {}
    if cd_data.get('quote'):
        [quote_data.update({k: v}) for k, v in cd_data.get('quote').items() if k in crm_utils.MOTOR_QUOTE_FIELDS_TO_SHOW]
    if quote_data.get('isNewVehicle'):
        quote_data.update({'vehicle_status': 'New'}) if bool(
            quote_data.get('isNewVehicle')
        ) else quote_data.update({'vehicle_status': 'Old'})
    quote_data.pop('isNewVehicle', None)
    r_extra = requirement.extra.copy()
    if r_extra.get('form_data'):
        for k, v in requirement.fd.get_data().items():
            if isinstance(v, dict):
                r_extra.update(v)
            elif isinstance(v, list):
                for item in v:
                    r_extra.update(item)

    sdata = crm_utils.ui_cleaner(r_extra, product_type, requirement)
    template_data = render_to_string('call_form.html', {
        'mobile': mj.get('mobile'),
        'rdata': requirement,
        'data': sdata,
        'exclude_data': ['later_time', 'disposition', 'head_disposition'],
        'call_form': call_form,
        'product_type': product_type,
        'show_quote_link': show_quote_link,
        'quote_url': quote_link,
        'cd_data': cd_data,
        'allow_close': True,
    }, context_instance=RequestContext(request))

    return JsonResponse({'success': True, 'message': template_data})


def manager_dashboard_health_data(request):

    advisor_map = {a.id: a for a in crm_models.Advisor.default_objects.all()}
    advisor_status_count_map = {
        a: {
            'FRESH': 0,
            'IN_PROCESS': 0,
            'RINGING_FRESH': 0,
            'VISIBLE_IN_PROCESS': 0
        }
        for a in crm_models.Advisor.default_objects.all()
    }

    qualifier_Q = Q(queues__code='health-qualifier')
    closure_Q = Q(queues__code__startswith='health') & ~Q(queues__code='health-qualifier')

    qualifier_advisors = crm_models.Advisor.objects.filter(
        qualifier_Q &
        ~Q(queueadvisor__is_manager=True) &
        Q(user__is_active=True)
    )

    closure_advisors = crm_models.Advisor.objects.filter(
        closure_Q &
        ~Q(queueadvisor__is_manager=True) &
        Q(user__is_active=True)
    )

    group_names = ['Qualifier Users', 'Closure Users']
    users_group = [qualifier_advisors, closure_advisors]

    data_dict = {}
    QUALIFIED_STATES = list(sm_models.State.objects.filter(parent_state__name='QUALIFIED'))
    FRESH_NEW_STATE = list(sm_models.State.objects.filter(name='FRESH_new'))
    FRESH_RINGING_STATE = list(sm_models.State.objects.filter(name='FRESH_ringing'))
    upcoming_tasks_map = core_models.Requirement.objects.filter(
        next_crm_task__scheduled_time__gt=datetime.datetime.now(),
        next_crm_task__scheduled_time__lte=datetime.datetime.now().replace(hour=23, minute=59, second=59),
        next_crm_task__assigned_to__isnull=False,
        owner__isnull=False,
        current_state__isnull=False,
        current_state__in=QUALIFIED_STATES
    ).exclude(
        Q(user_id__isnull=True) &
        Q(visitor_id__isnull=True)
    ).values(
        'current_state__name',
        'owner_id'
    ).annotate(cnt=Count('owner_id'))

    for row in upcoming_tasks_map:
        if row['current_state__name'].startswith('QUALIFIED'):
            advisor_status_count_map[
                advisor_map[row['owner_id']]
            ]['IN_PROCESS'] += row['cnt']

    visible_inprocess_tasks_map = core_models.Requirement.objects.filter(
        next_crm_task__scheduled_time__lte=datetime.datetime.now(),
        next_crm_task__assigned_to__isnull=False,
        owner__isnull=False,
        current_state__isnull=False,
        current_state__in=QUALIFIED_STATES + FRESH_NEW_STATE + FRESH_RINGING_STATE
    ).exclude(
        Q(user_id__isnull=True) &
        Q(visitor_id__isnull=True)
    ).values(
        'current_state__name',
        'owner_id'
    ).annotate(cnt=Count('owner_id'))

    for row in visible_inprocess_tasks_map:
        if row['current_state__name'] == 'FRESH_ringing':
            advisor_status_count_map[
                advisor_map[row['owner_id']]
            ]['RINGING_FRESH'] = row['cnt']
        elif row['current_state__name'] == 'FRESH_new':
            advisor_status_count_map[
                advisor_map[row['owner_id']]
            ]['FRESH'] = row['cnt']
        elif row['current_state__name'].startswith('QUALIFIED'):
            advisor_status_count_map[
                advisor_map[row['owner_id']]
            ]['VISIBLE_IN_PROCESS'] += row['cnt']

    for group_name, users in zip(group_names, users_group):
        data_dict.update({group_name: {
            user: advisor_status_count_map[user]
            for user in users}
        })

    total_unassigned = core_models.Requirement.objects.filter(
        Q(owner__isnull=True) &
        Q(kind__product="health") &
        Q(current_state__parent_state__name='FRESH') &
        Q(next_crm_task__isnull=False) &
        ~Q(next_crm_task__queue__code="health-renewals")
    ).exclude(
        Q(user__isnull=True) &
        Q(visitor__isnull=True)
    ).values(
        'current_state__name'
    ).annotate(Count('current_state'))
    template = "manager_dashboard_health_data.html"
    return render(request, template, {
        'health_data': data_dict,
        'last_updated': datetime.datetime.now(),
        'auto_assign_count': total_unassigned
    })


def manager_dashboard_motor_data(request):

    advisor_map = {a.id: a for a in crm_models.Advisor.default_objects.all()}
    advisor_status_count_map = {
        a: {
            'FRESH': 0,
            'IN_PROCESS': 0,
            'RINGING_FRESH': 0,
            'VISIBLE_IN_PROCESS': 0
        }
        for a in crm_models.Advisor.default_objects.all()
    }

    offline_Q = Q(queues__code='motor-offlinecases')
    bike_Q = Q(queues__code='motor-bike')
    qualifier_Q = Q(queues__code='car-qualifier')
    closure_Q = Q(queues__code='car-closure')

    offline_advisors = crm_models.Advisor.objects.filter(
        offline_Q &
        ~Q(queueadvisor__is_manager=True) &
        Q(user__is_active=True)
    )

    qualifier_advisors = crm_models.Advisor.objects.filter(
        qualifier_Q &
        ~Q(queueadvisor__is_manager=True) &
        Q(user__is_active=True)
    )

    closure_advisors = crm_models.Advisor.objects.filter(
        closure_Q &
        ~Q(queueadvisor__is_manager=True) &
        Q(user__is_active=True)
    )

    bike_advisors = crm_models.Advisor.objects.filter(
        bike_Q & ~Q(queueadvisor__is_manager=True) & Q(user__is_active=True))

    group_names = ['Offline Users', 'Qualifier Users', 'Closure Users', 'Bike Users']
    users_group = [offline_advisors, qualifier_advisors, closure_advisors, bike_advisors]

    data_dict = {}
    QUALIFIED_STATES = list(sm_models.State.objects.filter(parent_state__name='QUALIFIED'))
    FRESH_NEW_STATE = list(sm_models.State.objects.filter(name='FRESH_new'))
    FRESH_RINGING_STATE = list(sm_models.State.objects.filter(name='FRESH_ringing'))
    upcoming_tasks_map = core_models.Requirement.objects.filter(
        next_crm_task__scheduled_time__gt=datetime.datetime.now(),
        next_crm_task__scheduled_time__lte=datetime.datetime.now().replace(hour=23, minute=59, second=59),
        next_crm_task__assigned_to__isnull=False,
        owner__isnull=False,
        current_state__isnull=False,
        current_state__in=QUALIFIED_STATES
    ).exclude(
        Q(user_id__isnull=True) &
        Q(visitor_id__isnull=True)
    ).values(
        'current_state__name',
        'owner_id'
    ).annotate(cnt=Count('owner_id'))

    for row in upcoming_tasks_map:
        if row['current_state__name'].startswith('QUALIFIED'):
            advisor_status_count_map[
                advisor_map[row['owner_id']]
            ]['IN_PROCESS'] += row['cnt']

    visible_inprocess_tasks_map = core_models.Requirement.objects.filter(
        next_crm_task__scheduled_time__lte=datetime.datetime.now(),
        next_crm_task__assigned_to__isnull=False,
        owner__isnull=False,
        current_state__isnull=False,
        current_state__in=QUALIFIED_STATES + FRESH_NEW_STATE + FRESH_RINGING_STATE
    ).exclude(
        Q(user_id__isnull=True) &
        Q(visitor_id__isnull=True)
    ).values(
        'current_state__name',
        'owner_id'
    ).annotate(cnt=Count('owner_id'))

    for row in visible_inprocess_tasks_map:
        if row['current_state__name'] == 'FRESH_ringing':
            advisor_status_count_map[
                advisor_map[row['owner_id']]
            ]['RINGING_FRESH'] = row['cnt']
        elif row['current_state__name'] == 'FRESH_new':
            advisor_status_count_map[
                advisor_map[row['owner_id']]
            ]['FRESH'] = row['cnt']
        elif row['current_state__name'].startswith('QUALIFIED'):
            advisor_status_count_map[
                advisor_map[row['owner_id']]
            ]['VISIBLE_IN_PROCESS'] += row['cnt']

    for group_name, users in zip(group_names, users_group):
        data_dict.update({group_name: {
            user: advisor_status_count_map[user]
            for user in users}
        })

    template = "manager_dashboard_motor_data.html"
    return render(request, template, {'motor_data': data_dict, 'last_updated': datetime.datetime.now()})


@login_required
def caller_dashboard(request):
    up = crm_models.Advisor.objects.get(user=request.user)
    data = {
        'notes': up.notes
    }
    call_data = (
        ("requirement_QUALIFIED", "In Process"),
        ("requirement_FRESH_new", "Fresh"),
        ("requirement_FRESH_ringing", "Ringing Fresh"),
    )
    shift_timings = {'start': datetime.time.strftime(up.shift_start_time, '%H:%M'),
                     'end': datetime.time.strftime(up.shift_end_time, '%H:%M')}

    is_qualifier = up.queues.filter(code__contains='qualifier').exists()
    # ameyo activity
    last_activity = crm_models.AdvisorDialerActivity.objects.filter(advisor=up)
    if not last_activity:
        data['ameyo_data'] = ['N/A', None]
    else:
        last_activity = last_activity.latest('created_on')
        data['ameyo_data'] = [last_activity.status, int((timezone.now() - last_activity.created_on).total_seconds())]

    visible_disposition_map = {
        str(d.value): [str(sd.value) for sd in d.sub_dispositions.filter(is_active=True)]
        for d in crm_models.Disposition.objects.all()
    }
    up.update_available_count()

    return render(request, "caller_dashboard.html", {
        'is_qualifier': is_qualifier,
        'expertise': up.get_preferred_product_type(),
        'num_prefix': up.get_prefix_for_pattern_based_dialing(),
        'call_data': call_data,
        'DISPOSITION_MAP': visible_disposition_map,
        'user_data': data,
        'search_customer_form': crm_forms.LeadSearchForm(request),
        'number_search_form': crm_forms.NumberSearchForm(request),
        'shift_timings': shift_timings,
        'profile': up,
    })


def set_rid_cookie(request):
    rid = request.GET.get('rid') or ''
    response = HttpResponse('Setting / Unsetting cookie %s' % rid)
    response.set_cookie('requirement_id', rid, domain='.coverfox.com')
    return response


@login_required
def caller_dashboard_health_summary(request):
    return render(request, "caller_dashboard_health_qualification_team.html")


@login_required
def get_health_qualification_team(request):
    closure_users = crm_models.Advisor.objects.filter(
        queues__name__startswith="health",
        user__is_active=True
    ).exclude(queues__name__icontains='qualifier').distinct()
    closure_data = []

    likely_to_buy = core_models.Requirement.objects.filter(
        current_state__name='QUALIFIED_likelytobuy',
        owner__in=closure_users
    ).exclude(
        next_crm_task__queue__name__icontains='qualifier'
    ).distinct().values_list('owner_id').annotate(Count('next_crm_task_id', distinct=True))

    scheduled_calls = core_models.Requirement.objects.filter(
        tasks__scheduled_time__gte=datetime.datetime.now(),
        owner__in=closure_users
    ).exclude(
        current_state__name__startswith='CLOSED'
    ).distinct().values_list('owner_id').annotate(Count('next_crm_task_id', distinct=True))
    for advisor in closure_users:
        likely_to_buy_count = 0
        scheduled_calls_count = 0

        for element in likely_to_buy:
            if element[0] == advisor.id:
                likely_to_buy_count = element[1]

        for element in scheduled_calls:
            if element[0] == advisor.id:
                scheduled_calls_count = element[1]

        row = [advisor.user.primary_email, likely_to_buy_count, scheduled_calls_count]
        closure_data.append(row)

    return render(request, "health_qualification_team.html", {
        'success': True,
        'closure_data': closure_data,
    })


@login_required
def likely_to_buy_details(request):
    is_manager = crm_models.QueueAdvisor.objects.filter(
        advisor__user=request.user,
        is_manager=True
    ).exists()
    if is_manager:
        queues = crm_models.Queue.objects.filter(
            queueadvisor__advisor__user=request.user,
            queueadvisor__is_manager=True
        )
        callers = crm_models.Advisor.objects.filter(
            user__is_active=True,
            queueadvisor__queue__in=queues,
            queueadvisor__is_manager=False
        )
    else:
        callers = request.user.advisor.all()

    requirements_list = core_models.Requirement.objects.filter(
        Q(owner__in=callers) &
        Q(current_state__parent_state__name="QUALIFIED")
    ).exclude(Q(user_id__isnull=True) & Q(visitor_id__isnull=True)).distinct()
    paginator = Paginator(requirements_list, 10)
    page = request.GET.get('page')

    try:
        requirements = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        requirements = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        requirements = paginator.page(paginator.num_pages)

    requirement_data = []
    for req in requirements:
        ltb_activity = req.crm_activities.filter(
            sub_disposition__value__in=['FORM_SENT', 'LIKELY_TO_BUY']
        ).last()
        user = req.user or req.visitor
        requirement_data.append([
            req.id,
            req.user.full_name if req.user else '',
            user.primary_phone if user else '',
            req.owner.dialer_username if req.owner else '',
            req.next_crm_task.queue.name if req.next_crm_task and req.next_crm_task.queue else '',
            req.next_crm_task.scheduled_time if req.next_crm_task else None,
            ltb_activity.disposition.value if ltb_activity else '',
            ltb_activity.end_time.strftime("%d %b %Y %I:%M %p") if ltb_activity else '',
            req.crm_activities.filter(disposition__value='FORM_SENT').count()
        ])

    sorted(requirement_data, key=lambda x: (x[5] is None, x[5]))
    return render(request, "likely_to_buy_tab.html", {
        'likely_to_buy_list': requirement_data,
        'paginator': requirements,
        'success': True,
        'manager': is_manager,
    })


def autoassign_requirement_dashboard(request):
    exclude_motor_queues = ['motor-cardekhonew', 'motor-cardekhorenew']
    queues = crm_models.Queue.objects.filter(
        name__startswith='motor'
    ).exclude(name__in=exclude_motor_queues)

    today_start = datetime.datetime.now().replace(hour=0, minute=0, second=0)
    yestd_start = datetime.datetime.now().replace(hour=0, minute=0, second=0) - datetime.timedelta(days=1)

    total_leads = dict(core_models.Requirement.objects.filter(
        owner=None,
        next_crm_task__queue__name__startswith='motor',
        current_state__name__startswith='FRESH'
    ).exclude(
        next_crm_task__queue__name__in=['motor-cardekhonew', 'motor-cardekhorenew']
    ).values_list('next_crm_task__queue__name').annotate(Count('next_crm_task__queue__name')))

    today_leads = dict(core_models.Requirement.objects.filter(
        owner=None,
        next_crm_task__queue__name__startswith='motor',
        current_state__name__startswith='FRESH',
        created_date__gte=today_start
    ).exclude(
        next_crm_task__queue__name__in=['motor-cardekhonew', 'motor-cardekhorenew']
    ).values_list('next_crm_task__queue__name').annotate(Count('next_crm_task__queue__name')))

    yestd_leads = dict(core_models.Requirement.objects.filter(
        owner=None,
        next_crm_task__queue__name__startswith='motor',
        current_state__name__startswith='FRESH',
        created_date__gte=yestd_start
    ).exclude(
        next_crm_task__queue__name__in=['motor-cardekhonew', 'motor-cardekhorenew']
    ).values_list('next_crm_task__queue__name').annotate(Count('next_crm_task__queue__name')))

    data = [(
        queue.name,
        total_leads.get(queue.name, '0'),
        today_leads.get(queue.name, '0'),
        yestd_leads.get(queue.name, '0')
    ) for queue in queues]

    return render(request, "autoassign_requirement_dashboard.html", {
        'motor_data': data,
        'last_updated': datetime.datetime.now(),
    })


@login_required
def customer_req_call_back_dashboard(request, advisor_id=None):
    today_end = datetime.datetime.now(pytz.timezone("Asia/Calcutta")).replace(hour=23, minute=59, second=59).astimezone(pytz.utc)
    reqs = core_models.Requirement.objects.filter(
        Q(next_crm_task__scheduled_time__lte=today_end)
        & Q(Q(next_crm_task__queue__name__icontains='motor') | Q(next_crm_task__queue__name__icontains='car'))
        & Q(
            Q(last_crm_activity__data__jcontains={'is_customer_scheduled': 'True'})
            | Q(last_crm_activity__data__jcontains={'is_customer_scheduled': 'Y'})
        )
        & ~Q(next_crm_task__queue__name__contains='bike')
    )
    if advisor_id:
        reqs = reqs.filter(owner__id=advisor_id)

    qualifier_data = {advisor: [] for advisor in crm_models.Advisor.objects.filter(
        queues__name__icontains="qualifier",
        user__is_active=True,
        queueadvisor__is_manager=False
    )}
    closure_data = {advisor: [] for advisor in crm_models.Advisor.objects.filter(
        ~Q(queues__name__icontains="qualifier") &
        Q(user__is_active=True) &
        Q(Q(queues__name__icontains="motor") | Q(queues__name__icontains="car")),
        queueadvisor__is_manager=False
    )}
    for req in reqs:
        if req.owner and req.next_crm_task:
            qualifier_data.get(req.owner, []).append(req)
            closure_data.get(req.owner, []).append(req)

    [requirements.sort(
        key=lambda x: x.next_crm_task.scheduled_time, reverse=True
    ) for user, requirements in qualifier_data.items()]
    [requirements.sort(
        key=lambda x: x.next_crm_task.scheduled_time, reverse=True
    ) for user, requirements in closure_data.items()]

    return render(request, "customer_req_call_back_dashboard.html", {
        'qualifier_data': qualifier_data, 'closure_data': closure_data
    })


def ameyo_incoming_call(request):
    ameyo_logger.info("*****************INCOMING CALL REQUEST*******************")
    ameyo_logger.info(json.dumps(request.GET))

    mobile = request.GET.get('mobile')[-10:]
    crt_obj_id = request.GET.get('crt_obj_id')
    did = request.GET.get('did')

    if not mobile:
        return JsonResponse({'success': False, 'error': 'Mobile number required'})

    # HACK because idiots at ameyo can't figure this out.
    if mobile == '1123':
        did, mobile = mobile, did

    primary_caller, secondary_caller, common_queue_id = ameyo_utils.IncomingCallRouter(mobile, did, crt_obj_id).get_callers()
    agents_list = [primary_caller.dialer_username, secondary_caller.dialer_username]

    ameyo_logger.info("Returned response for %s - %s : %s - %s" % (crt_obj_id, did, agents_list, common_queue_id))

    return JsonResponse({'success': True, 'agents': agents_list, 'common_queue': common_queue_id})


def ameyo_incoming_lead(request):
    ameyo_logger.info("*****************INCOMING LEAD REQUEST*******************")
    ameyo_logger.info(json.dumps(request.GET))

    mobile = request.GET.get('mobile')[-10:]

    if not mobile:
        return JsonResponse({'success': False, 'error': 'Mobile number required'})

    adata = {
        'user': crm_models.Advisor.objects.get(dialer_username='system'),
        'category': 'INCOMING_CALL',
        'data': {
            'mobile': mobile,
            'product': 'car',
            'category': 'call-blaster-campaign',
            'source': 'INCOMING'
        },
    }
    activity = create_crm_activity(data=adata)
    req = activity.requirements.last()
    req.last_crm_activity = activity
    if req.next_crm_task:
        nct = req.next_crm_task
        nct.is_disposed = True
        nct.save()
        activity.primary_task = nct
    activity.end_time = timezone.now()
    activity.save()

    scheduled_time = timezone.now() + datetime.timedelta(minutes=5)
    queue = model_router.RouterUtils('crm').get_matching_router(
        router_queryset=crm_models.QueueRouter.objects.all(),
        rdata={'requirement': req},
    ).queue
    task = crm_models.Task.objects.create(
        category=crm_models.TaskCategory.objects.get(name='OUTGOING_CALL'),
        queue=queue,
        scheduled_time=scheduled_time,
        from_crm_activity=activity,
        is_disposed=False
    )
    req.next_crm_task = task
    req.save(update_fields=['next_crm_task'])
    req.tasks.add(task)

    return JsonResponse({'success': True})


def ameyo_missed_call(request):
    rdata = {k: v for k, v in request.GET.items()}
    mobile = rdata.get('CustomerCLI')[-10:]
    crt_obj_id = rdata.get('UniqueCRTObjectID')

    ameyo_utils.MissedCallHandler(mobile, crt_obj_id).handle_missed_call()

    ameyo_logger.info("*****************MISSED CALL REQUEST STARTS*******************")
    ameyo_logger.info(json.dumps(rdata))
    return JsonResponse({'success': True})


def ameyo_put_voicemail(request):
    response = {}
    response['POST'] = dict([(k, v) for k, v in request.POST.items()])
    response['GET'] = dict([(k, v) for k, v in request.GET.items()])

    ameyo_logger.info("*****************VOICEMAIL REQUEST STARTS*******************")
    ameyo_logger.info(json.dumps(response))
    return JsonResponse({'success': True})


@csrf_exempt
def secret_page(request):
    from core import forms as core_forms
    from motor_product import forms as motor_forms
    from health_product import forms as health_forms
    from travel_product import forms as travel_forms
    data = request.POST.dict()
    logger.error("###########affiliate Lead ###########")
    logger.error(json.dumps(data))
    try:
        accesstoken = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[-1]
        if accesstoken:
            pass
        elif data.get('client_id') and data.get('client_secret'):
            pass
        else:
            raise ValueError("Invalid parameters")
    except:
        return JsonResponse({'error': 'invalid_client'}, status=404)

    is_prod = True
    if request.IS_INTERNAL:
        is_prod = False

    if "campaign" not in data:
        data["no_campaign"] = True
        lead = leads_models.Lead.objects.create(
            data=data,
            is_production=is_prod
        )
        return HttpResponse('OK')
    else:
        data["no_campaign"] = False
        lead = leads_models.Lead.objects.create(
            data=data,
            is_production=is_prod
        )

    product = core_forms.RequirementForm.get_product_name(data['campaign'].lower())
    if product in ['bike', 'car']:
        fd = motor_forms.MotorFDForm(data=data).get_fd()
    elif product in ['health']:
        fd = health_forms.HealthFDForm(data={u'proposer_{}'.format(key): value for key, value in data.items()}).get_fd()
    elif product in ['travel']:
        fd = travel_forms.TravelFDForm(data={u'self_{}'.format(key): value for key, value in data.items()}).get_fd()
    else:
        fd = {}

    data['mobile'] = data['mobile'][-10:]

    request.MID = core_forms.RequirementForm.get_mid_obj(data)
    skey = 'phone_%s' % (data['mobile'])
    visitor, _ = core_models.Tracker.objects.get_or_create(session_key=skey)

    request.TRACKER = visitor
    act = activity.create_activity(
        activity_type=activity.SUCCESS,
        product=product,
        page_id='affiliate_leads',
        form_name='',
        form_variant='',
        form_position='',
        visitor=visitor,
        data=fd,
    )
    if act:
        act.extra = data
        act.save(update_fields=['extra'])
    else:
        logger.error("Activity not generated for lead (%s)" % lead)

    return JsonResponse({'success': True}, status=201)


@csrf_exempt
def send_access_token(request):
    accesstoken = uuid.uuid4().hex
    refresh_token = uuid.uuid4().hex
    data = request.POST.dict()
    try:
        if data.get('client_id') and data.get('client_secret'):
            return JsonResponse({
                "access_token": accesstoken, "expires_in": 86400,
                "refresh_token": refresh_token, "scope": "read",
                "token_type": "bearer"
            }, status=201)
        elif data.get('refresh_token'):
            return JsonResponse({
                "access_token": accesstoken, "expires_in": 86400,
                "refresh_token": refresh_token, "scope": "read",
                "token_type": "bearer"
            }, status=201)
        else:
            raise ValueError("Invalid parameters")
    except:
        return JsonResponse({'error': 'invalid_client'}, status=404)


@login_required
def ameyo_dashboard(request):
    motor_Q = Q(queues__name__startswith='motor')
    offline_Q = (Q(queues__name__icontains='offline') | Q(queues__name__icontains='inspection')) & motor_Q
    bike_Q = Q(queues__name__icontains='bike') & motor_Q & ~offline_Q
    qualifier_Q = Q(queues__name__icontains='qualifier') & motor_Q & ~bike_Q & ~offline_Q
    closure_Q = motor_Q & ~qualifier_Q & ~bike_Q & ~offline_Q

    # exclude(username__in=['shweta', 'baljeet'])
    offline_advisors = crm_models.Advisor.objects.filter(offline_Q & ~Q(queueadvisor__is_manager=True) & Q(user__is_active=True))

    # exclude(username__in=['shweta', 'baljeet'])
    qualifier_advisors = crm_models.Advisor.objects.filter(qualifier_Q & ~Q(queueadvisor__is_manager=True) & Q(user__is_active=True))

    # exclude(username__in=['shweta', 'baljeet'])
    closure_advisors = crm_models.Advisor.objects.filter(closure_Q & ~Q(queueadvisor__is_manager=True) & Q(user__is_active=True))

    # exclude(username__in=['shweta', 'baljeet'])
    bike_advisors = crm_models.Advisor.objects.filter(bike_Q & ~Q(queueadvisor__is_manager=True) & Q(user__is_active=True))

    group_names = ['Offline Advisor', 'Qualifier Advisor', 'Closure Advisor', 'Bike Advisor']
    advisors_group = [offline_advisors, qualifier_advisors, closure_advisors, bike_advisors]
    data = {group: {} for group in group_names}
    for i, advisors in enumerate(advisors_group):
        advisor_ameyo_data = {}
        for advisor in advisors:
            last_activity = crm_models.AdvisorDialerActivity.objects.filter(advisor=advisor)
            if not last_activity:
                advisor_ameyo_data[advisor.dialer_username] = ['N/A', None]
            else:
                last_activity = last_activity.latest('created_on')
                advisor_ameyo_data[advisor.dialer_username] = [
                    last_activity.status,
                    int((timezone.now() - last_activity.created_on).total_seconds())
                ]
        data.update({group_names[i]: advisor_ameyo_data})
    return render(request, "ameyo_dashboard.html", {
        'data': data,
        'last_updated': datetime.datetime.now()
    })


@login_required
def customers_dashboard(request):
    advisors_data = crm_models.Advisor.objects.filter(
        user__is_active=True
    ).values_list(
        'user__first_name',
        'user__last_name',
        'id'
    )
    return render(request, "customer.html", {
        'customer_search_form': crm_forms.CustomerSearchForm(request),
        'advisors_data': advisors_data
    })


@login_required
def manager_dashboard_bulk_actions(request):
    if request.POST:
        data = json.loads(request.POST['data'])
        reassign_to_advisor_id = data['reassign_to']
        req_id_list = data.get('req_id_list')
        if not req_id_list:
            return JsonResponse({"success": False, "response": "No requirements selected"})

        caller = crm_models.Advisor.objects.get(id=int(reassign_to_advisor_id))
        try:
            with django_transaction.atomic():
                if req_id_list:
                    for req_id in req_id_list:
                        requirement = core_models.Requirement.objects.get(id=int(req_id))
                        extlogger.info(
                            "user_%s reassigned requirement_%s from advisor_%s to advisor_%s" % (
                                request.user.id, req_id, requirement.owner_id, caller.id
                            )
                        )
                        requirement.owner = caller
                        if requirement.next_crm_task:
                            nct = requirement.next_crm_task
                            nct.assigned_to = caller
                            nct.is_disposed = False
                            nct.save()
                        else:
                            task = crm_models.Task.objects.create(
                                category=crm_models.TaskCategory.objects.get(name='OUTGOING_CALL'),
                                assigned_to=caller,
                                scheduled_time=timezone.now(),
                                is_disposed=False
                            )
                            requirement.next_crm_task = task
                            requirement.tasks.add(task)
                        requirement.save()
        except:
            return JsonResponse({'success': False, 'response': "An error occured"})
        return JsonResponse({'success': True})


@login_required
def ajax_load_initial_calls(request):
    up = crm_models.Advisor.objects.get(user=request.user)
    pending_tasks = crm_models.Task.objects.filter(
        scheduled_time__lte=timezone.now(),
        assigned_to=up
    ).exclude(
        is_disposed=True
    ).order_by('-scheduled_time')
    req_ids = []
    in_process_requirements = core_models.Requirement.objects.filter(
        next_crm_task__in=pending_tasks,
        current_state__parent_state__name__in=['QUALIFIED']
    ).order_by('next_crm_task__scheduled_time').distinct()
    new_requirements = in_process_requirements.filter(
        current_state__name='QUALIFIED_new'
    )
    qualified_requirements = in_process_requirements.exclude(
        current_state__name='QUALIFIED_new'
    )
    new_data = [pd.mini_json() for pd in new_requirements]
    qualified_data = [pd.mini_json() for pd in qualified_requirements]
    req_ids = list(in_process_requirements.values_list('id', flat=True))

    fresh_requirements = core_models.Requirement.objects.filter(
        next_crm_task__in=pending_tasks,
        current_state__name='FRESH_new'
    ).exclude(id__in=req_ids).distinct()
    fresh_data = [pd.mini_json() for pd in fresh_requirements]
    req_ids.extend([mj['id'] for mj in fresh_data])
    # this is so that fresh qualified requirements show up in fresh tab
    fresh_data.extend(new_data)

    ringing_fresh_requirements = core_models.Requirement.objects.filter(
        next_crm_task__in=pending_tasks,
        current_state__name='FRESH_ringing'
    ).exclude(
        id__in=req_ids
    ).order_by('next_crm_task__scheduled_time').distinct()
    ringing_fresh_data = [pd.mini_json() for pd in ringing_fresh_requirements]
    req_ids.extend([mj['id'] for mj in ringing_fresh_data])

    if len(fresh_data) == 0:
        up.assign_fresh_requirement_on_disposition()

    crm_models.Task.objects.filter(
        next_for_requirements__id__in=req_ids
    ).filter(pushed_time__isnull=True).update(pushed_time=timezone.now())

    call_data = [
        ["requirement_QUALIFIED", "Qualified", qualified_data],
        ["requirement_FRESH_new", "Fresh", fresh_data],
        ["requirement_FRESH_ringing", "Ringing Fresh", ringing_fresh_data],
    ]

    return JsonResponse(call_data, safe=False)


@login_required
def get_task(request, tid):
    """
    Gets the task with id=tid.
    Starts an activity with the appropriate category
    returns mini_dicts of those requirements
    also returns a primary requirment
    """

    up = crm_models.Advisor.objects.get(user=request.user)
    task = get_object_or_404(crm_models.Task, pk=tid)
    if task.is_disposed:
        extlogger.info("Task=%s was already disposed still appeared" % (tid))
        return JsonResponse({
            'success': False,
            'message': "Call has been disposed already."
        })

    act_data = {
        'user': up,
        'category': task.category.name,
        'primary_task': task,
    }
    activity = create_crm_activity(data=act_data)

    requirements = {r.id: crm_utils.get_requirement_mini_dict(r) for r in task.requirements.all()}

    primary_requirement = task.next_for_requirements.last() or task.requirements.last()
    primary_requirement_template_data = crm_utils.render_requirement_form(
        request=request,
        requirement=primary_requirement,
        is_incoming=False,
        task_id=task.id,
        activity_id=activity.id
    )

    requirement_mini_data = render_to_string('requirement_mini_dict.html', {
        'requirements': requirements,
    }, context_instance=RequestContext(request))
    up.update_available_count()

    return JsonResponse({
        'success': True,
        'activity_id': activity.id,
        'requirements': requirement_mini_data,
        'primary_requirement_id': primary_requirement.id,
        'primary': primary_requirement_template_data
    })


@login_required
def requirement_form(request, rid):
    """
    In the get call, it also expects the activity_id and task_id
    for which the call is being made
    """
    up = crm_models.Advisor.objects.get(user=request.user)
    requirement = get_object_or_404(core_models.Requirement, pk=rid)

    if request.POST:
        call_form = crm_forms.RequirementCallForm(request.POST, request=request)
        if call_form.is_valid():
            activity = crm_models.Activity.objects.get(
                id=int(call_form.cleaned_data['activity_id']),
            )
            activity.save_call_form(request, call_form, requirement, up)
            up.update_available_count()
            return JsonResponse({'success': True})
        else:
            return JsonResponse({'success': False, 'errors': call_form.errors})

    else:
        template_data = crm_utils.render_requirement_form(
            request=request,
            requirement=requirement,
            is_incoming=False
        )
        return JsonResponse({'success': True, 'message': template_data})


@login_required
def get_transfer_call_popup(request):
    up = crm_models.Advisor.objects.get(user=request.user)
    mobile = request.GET['mobile'][-10:]

    last_tried_agent = redis_utils.Publisher.rclient.get('customer_transfer_call_%s' % mobile)  # 'user_<up.id>'
    task_id = redis_utils.Publisher.rclient.get('customer_transfer_call_task_%s' % mobile)  # 'task_<task.id>''
    rid = redis_utils.Publisher.rclient.get('calltransfer_mobile_%s' % mobile)
    requirement = task = None

    if last_tried_agent and not last_tried_agent == 'user_%s' % up.id:
        redis_utils.Publisher.publish(
            last_tried_agent, 'remove_transfer_call_popup',
            {'caller_id': last_tried_agent.split('_')[1]}
        )

    if task_id:
        task = crm_models.Task.objects.get(id=int(task_id))
        task.assigned_to = up
        task.save()
        requirement = task.requirements.last()
    else:
        if not rid:
            time.sleep(2)
            rid = redis_utils.Publisher.rclient.get('calltransfer_mobile_%s' % mobile)

        if rid:
            requirement = core_models.Requirement.objects.get(id=int(rid))
            task = crm_models.Task.objects.create(
                category=crm_models.TaskCategory.objects.get(name='TRANSFERRED_CALL'),
                assigned_to=up,
                scheduled_time=timezone.now(),
                pushed_time=timezone.now(),
                from_crm_activity=requirement.last_crm_activity,
                is_disposed=False
            )
            requirement.next_crm_task = task
            requirement.save(update_fields=['next_crm_task'])
            requirement.tasks.add(task)
        else:
            return JsonResponse({'success': False, 'message': 'Unable to get information for this call'}, safe=False)

    redis_utils.Publisher.rclient.setex('customer_transfer_call_%s' % mobile, 'user_%s' % up.id, 600)
    redis_utils.Publisher.rclient.setex('customer_transfer_call_task_%s' % mobile, str(task.id), 600)
    popup_template = render_to_string('transfer_call_popup.html', {
        'task_id': task.id if task else None,
        'requirement': requirement.mini_json() if requirement else None,
    }, context_instance=RequestContext(request))
    return JsonResponse({'success': True, 'message': popup_template}, safe=False)


@login_required
def get_incoming_call_popup(request):
    up = crm_models.Advisor.objects.get(user=request.user)
    mobile = request.GET['mobile'][-10:]

    if request.GET.get('call_type') == 'incoming':
        # if in redis, get the task id from there
        last_tried_agent = redis_utils.Publisher.rclient.get('customer_incoming_call_%s' % mobile)  # 'user_<up.id>'
        task_id = redis_utils.Publisher.rclient.get('customer_incoming_call_task_%s' % mobile)  # 'task_<task.id>''

        if last_tried_agent and not last_tried_agent == 'user_%s' % up.id:
            redis_utils.Publisher.publish(
                last_tried_agent, 'remove_incoming_call_popup',
                {'caller_id': last_tried_agent.split('_')[1]}
            )

        if task_id:
            task = crm_models.Task.objects.get(id=int(task_id))
            task.assigned_to = up
            task.save()
            redis_utils.Publisher.rclient.setex('customer_incoming_call_%s' % mobile, 'user_%s' % up.id, 60)
            redis_utils.Publisher.rclient.setex('customer_incoming_call_task_%s' % mobile, str(task.id), 60)
            reqs = task.requirements.all()
        else:
            crt_obj_id = request.GET['crtObjectId']
            did_product_type = redis_utils.Publisher.rclient.get('crtObjId_%s' % crt_obj_id)
            if not did_product_type:
                product_type = 'car'
                did_product_type = '%s_%s' % (ameyo_utils.PRODUCT_DEFAULT_DIDS.get(product_type), product_type)
            did, product_type = did_product_type.split('_')

            if product_type == 'service':
                return JsonResponse({'success': False, 'message': 'This was a service call'}, safe=False)

            ad_category = 'SIM'
            if did in ameyo_utils.DID_MAP:
                ad_category = ameyo_utils.DID_MAP[did]['tollfree']
            adata = {
                'user': crm_models.Advisor.objects.get(dialer_username='system'),
                'category': 'INCOMING_CALL',
                'data': {
                    'product': product_type,
                    'category': ad_category,
                    'source': 'INCOMING',
                    'mobile': mobile,
                    'campaign': '',
                },
            }
            activity = create_crm_activity(data=adata)
            req = activity.requirements.last()
            req.last_crm_activity = activity

            if req.next_crm_task:
                nct = req.next_crm_task
                nct.is_disposed = True
                nct.save()
                activity.primary_task = nct
            activity.end_time = timezone.now()
            activity.save()

            task = crm_models.Task.objects.create(
                category=crm_models.TaskCategory.objects.get(name='INCOMING_CALL'),
                assigned_to=up,
                scheduled_time=timezone.now(),
                pushed_time=timezone.now(),
                from_crm_activity=activity,
                is_disposed=False
            )
            req.next_crm_task = task
            req.save()
            req.tasks.add(task)

            # TODO: use mobile searchin.copy()g
            reqs = crm_utils.requirement_searcher(
                {'customer_mobile': mobile}
            ).filter(kind=req.kind).exclude(
                current_state__name__startswith='CLOSED'
            )
            task.requirements.add(*reqs)
            redis_utils.Publisher.rclient.setex('customer_incoming_call_%s' % mobile, 'user_%s' % up.id, 60)
            redis_utils.Publisher.rclient.setex('customer_incoming_call_task_%s' % mobile, str(task.id), 60)
        requirements = [r.mini_json() for r in reqs]

        popup_template = render_to_string('incoming_call_popup.html', {
            'task_id': task.id,
            'requirements': requirements,
        }, context_instance=RequestContext(request))
        return JsonResponse({'success': True, 'message': popup_template}, safe=False)


@login_required
def incoming_task_form(request):
    tid = request.GET['task']

    up = crm_models.Advisor.objects.get(user=request.user)
    task = get_object_or_404(crm_models.Task, pk=int(tid))

    act_data = {
        'user': up,
        'category': task.category.name,
        'primary_task': task,
    }
    activity = create_crm_activity(data=act_data)
    requirements = {r.id: crm_utils.get_requirement_mini_dict(r) for r in task.requirements.all()}

    visible_disposition_map = {
        str(d.value): [str(sd.value) for sd in d.sub_dispositions.filter(is_active=True)]
        for d in crm_models.Disposition.objects.all()
    }

    template_data = render_to_string('requirement_mini_dict.html', {
        'requirements': requirements,
    }, context_instance=RequestContext(request))
    up.update_available_count()

    return render(request, "incoming.html", {
        'DISPOSITION_MAP': visible_disposition_map,
        'success': True,
        'activity_id': activity.id,
        'task': task,
        'requirements': template_data,
    })


def just_dial_secret_page(request):
    from core import forms as core_forms
    from motor_product import forms as motor_forms
    data = {k: v for k, v in request.GET.items() if v}
    is_prod = True
    if request.IS_INTERNAL:
        is_prod = False

    lead = leads_models.Lead.objects.create(
        data=data,
        is_production=is_prod
    )
    product = 'car'
    data.update({'source': 'justdial', 'category': 'affiliate'})
    fd = motor_forms.MotorFDForm(data=data).get_fd()
    request.MID = core_forms.RequirementForm.get_mid_obj(data)

    data['mobile'] = data['mobile'][-10:]

    skey = 'phone_%s' % (data['mobile'])
    visitor, _ = core_models.Tracker.objects.get_or_create(session_key=skey)

    request.TRACKER = visitor
    act = activity.create_activity(
        activity_type=activity.SUCCESS,
        product=product,
        page_id='affiliate_leads',
        form_name='',
        form_variant='',
        form_position='',
        visitor=visitor,
        data=fd,
    )

    if act:
        act.extra = data
        act.save(update_fields=['extra'])
    else:
        logger.error("Activity not generated for lead (%s)" % lead)

    return HttpResponse("RECEIVED")


@csrf_exempt
def slack_data_push(request):
    from core import forms as core_forms
    from motor_product import forms as motor_forms
    from health_product import forms as health_forms
    from travel_product import forms as travel_forms
    data = request.POST.dict()
    try:
        accesstoken = request.POST.get('token')
        if accesstoken == 'Hpdt0RgKziihqYwISdBeOd7u':
            pass
        else:
            raise ValueError("Invalid parameters")
    except:
        return HttpResponse('invalid_request')

    is_prod = True
    if request.IS_INTERNAL:
        is_prod = False

    if not data.get("text"):
        data["no_text"] = True
        lead = leads_models.Lead.objects.create(
            data=data,
            is_production=is_prod
        )
        return HttpResponse('OK')

    rdata = json.loads(data['text'])

    if "campaign" not in rdata:
        rdata["no_campaign"] = True
        lead = leads_models.Lead.objects.create(
            data=rdata,
            is_production=is_prod
        )
        return HttpResponse('OK')

    lead = leads_models.Lead.objects.create(
        data=rdata,
        is_production=is_prod
    )

    product = core_forms.RequirementForm.get_product_name(rdata['campaign'].lower())
    if product in ['bike', 'car']:
        fd = motor_forms.MotorFDForm(data=rdata).get_fd()
    elif product in ['health']:
        fd = health_forms.HealthFDForm(data={u'proposer_{}'.format(key): value for key, value in rdata.items()}).get_fd()
    elif product in ['travel']:
        fd = travel_forms.TravelFDForm(data={u'self_{}'.format(key): value for key, value in rdata.items()}).get_fd()
    else:
        fd = {}

    rdata['mobile'] = rdata['mobile'][-10:]

    request.MID = core_forms.RequirementForm.get_mid_obj(rdata)
    skey = 'phone_%s' % (rdata['mobile'])
    visitor, _ = core_models.Tracker.objects.get_or_create(session_key=skey)

    request.TRACKER = visitor
    act = activity.create_activity(
        activity_type=activity.SUCCESS,
        product=product,
        page_id='affiliate_leads',
        form_name='',
        form_variant='',
        form_position='',
        visitor=visitor,
        data=fd,
    )
    if act:
        act.extra = rdata
        act.save(update_fields=['extra'])
    else:
        logger.error("Activity not generated for lead (%s)" % lead)

    return HttpResponse('OK')


def numbers(request):
    import leads_numbers
    if not request.GET.get('secret') == 'dexter':
        return HttpResponse("Put the secret key Mr. Morgan")
    hour = request.GET.get('hour')
    minute = request.GET.get('minute')
    year = request.GET.get('year')
    day = request.GET.get('day')
    month = request.GET.get('month')
    year = request.GET.get('year')
    if hour and minute and year and day and month and year:
        end = datetime.datetime(
            int(year),
            int(month),
            int(day),
            int(hour),
            int(minute)
        )
    else:
        end = datetime.datetime.now()

    numbers = leads_numbers.countit(
        start=datetime.date.today(),
        end=end,
    )
    return render(request, "numbers.html", {
        'leadmobilescount': len(numbers['leadmobiles']),
        'phonescount': len(numbers['phones']),
        'incomingmobiles': len(numbers['incomingmobiles']),
        'nonincomingmobiles': len(numbers['nonincomingmobiles']),
        'onfloor': numbers['onfloor'],
        'overlap': numbers['overlap'],
        'mobilesinsystem': numbers['data_in_system'],
        'non_web_affiliate_requirements': numbers['non_web_affiliate_requirements'],
        'todaymobilesinsystem': numbers['todaymobilesinsystem'],
    })


@login_required
def queuerouter_dashboard(request):
    return render(request, "queuerouter_dashboard.html", {
        'qr_form': crm_forms.QueueRouterDashboardForm(request),
    })
