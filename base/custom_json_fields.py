from dateutil.relativedelta import relativedelta

from django import forms
from django.core.validators import RegexValidator
from django.utils import timezone


class CustomValidator(object):

    def __init__(self, name, message):
        self.name = name
        self.message = message


def years_from_today(years):
    return timezone.localtime(timezone.now()) + relativedelta(years=int(years))


def days_from_today(years):
    return timezone.localtime(timezone.now()) + relativedelta(days=int(years))


def validator_to_json_format(validator):
    return_data = {}
    if type(validator) is RegexValidator:
        return_data["name"] = "inverse_regex" if validator.inverse_match else "regex"
        return_data["expression"] = validator.regex.pattern
        return_data["message"] = validator.message
    elif isinstance(validator, CustomValidator):
        return_data['name'] = validator.name
        return_data['message'] = validator.message
    if return_data:
        return return_data
    return None


class JsonField(forms.Field):

    def to_jsonfield(self, **kwargs):
        return_data = {}
        return_data['required'] = self.widget.is_required
        return_data['hidden'] = self.widget.is_hidden
        return_data['placeholder'] = self.widget.attrs.get('placeholder', "")
        return_data['summaryLabel'] = self.widget.attrs.get('summaryLabel', "")
        return_data['description'] = self.widget.attrs.get('description', "")
        return_data['label'] = self.widget.attrs.get('label', return_data['placeholder'])
        return_data['title'] = self.widget.attrs.get('title', return_data['placeholder'])
        return_data['id'] = self.widget.attrs.get('id')
        return_data['customValidation'] = self.widget.attrs.get('customValidation', [])
        return_data['validators'] = []
        if self.widget.is_required:
            return_data['validators'].append({
                "name": "required",
                "message": "Oh, we can't proceed without this information. Do your bit?",
            })
        if self.widget.attrs.get("displayValueMap", None):
            return_data['displayValueMap'] = self.widget.attrs['displayValueMap']
        for validator in self.validators:
            validation_json = validator_to_json_format(validator)
            if validation_json:
                return_data['validators'].append(validation_json)
        return_data.update(kwargs)
        return return_data


class CharField(JsonField, forms.CharField):

    def to_jsonfield(self, **kwargs):
        return_data = super(self.__class__, self).to_jsonfield(**kwargs)
        return_data['type'] = self.widget.attrs.get('display_type') or "text"
        return_data['maxLength'] = self.max_length
        return_data['minLength'] = getattr(self, 'min_length', None) or 0
        if "isRefreshWithExternalValue" in self.widget.attrs:
            return_data['isRefreshWithExternalValue'] = self.widget.attrs['isRefreshWithExternalValue']
        return return_data


class IntegerField(JsonField, forms.IntegerField):

    def to_jsonfield(self, **kwargs):
        return_data = super(self.__class__, self).to_jsonfield(**kwargs)
        return_data['type'] = self.widget.attrs.get('display_type') or "numeric"
        return_data['maxLength'] = self.widget.attrs.get('max_length') or None
        return_data['minLength'] = self.widget.attrs.get('min_length') or None
        return_data['maxValue'] = getattr(self, 'max_value', None) or 0
        return_data['minValue'] = getattr(self, 'min_value', None) or 0
        return return_data


class ChoiceField(JsonField, forms.ChoiceField):

    def to_jsonfield(self, **kwargs):
        return_data = super(self.__class__, self).to_jsonfield(**kwargs)
        return_data['type'] = 'radio' if type(self.widget) == forms.RadioSelect else 'select'
        return_data['options'] = [{'key': choice[0], 'value': choice[1]} for choice in self.choices]
        if return_data['type'] == 'select':
            return_data['isSearchable'] = self.widget.attrs.get("isSearchable", True)
            return_data['isDisabled'] = self.widget.attrs.get("is_disabled")
        if self.widget.attrs.get("displayValueMap", None):
            return_data['displayValueMap'] = self.widget.attrs['displayValueMap']
        return return_data


class DateTimeField(JsonField, forms.DateTimeField):

    def to_jsonfield(self, **kwargs):
        return_data = super(self.__class__, self).to_jsonfield(**kwargs)
        return_data['minDate'] = self.widget.attrs.get('min_date')
        return_data['maxDate'] = self.widget.attrs.get('max_date')
        return_data['format'] = self.widget.attrs.get('format', 'YYYY-MM-DD')
        return_data['type'] = "calendar"
        return_data['defaultDisplayMonthDate'] = self.widget.attrs.get('defaultDisplayMonthDate')
        return return_data


class JsonForm(forms.Form):

    def form_to_json(self):
        return_data = {'children': []}
        return_data.update(getattr(self, 'json_form_data', {}))
        for label, field in self.fields.iteritems():
            keyword_arg = {}
            return_data['children'].append(field.to_jsonfield(**keyword_arg))
        return return_data
