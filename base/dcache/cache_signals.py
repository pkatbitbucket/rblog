from core.models import User
from blog.models import Post
from seo.models import SEOMetadata
import math

from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save
from dcache import cache_utils


@receiver(post_save)
def save(sender, **kwargs):
    obj = kwargs['instance']

    if type(obj) == Post:
        post = obj
        cache_post(post)

    if type(obj) == SEOMetadata:
        if type(obj.content_object) == Post:
            post = obj.content_object
            cache_post(post)

    if type(obj) == User:
        for post in obj.post_set.all():
            cache_post(post)


def cache_post(post):
    cache_utils.uncacheit(post)
    if post.status == 2:
        cache_utils.cacheit(post)
    cache_utils.regenerate_pages()
