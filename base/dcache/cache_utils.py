from core.models import User
from blog.models import Post
from seo.models import SEOMetadata
from flatpages.models import is_seo_redirect
import math

from staticgenerator import quick_delete, quick_publish


def cacheit(obj):
    quick_publish(obj)


def uncacheit(obj):
    quick_delete(obj)


def regenerate(post):
    uncacheit(post)
    cacheit(post)


def regenerate_pages():
    n = int(math.ceil(Post.objects.published().count() / 10.0))
    urls_to_cache = ['/articles/']
    urls_to_cache.extend(['/articles/page/%d/' % (i + 1) for i in range(n)])
    urls_to_cache = filter(lambda u: not is_seo_redirect(u), urls_to_cache)

    for url in urls_to_cache:
        uncacheit(url)
        cacheit(url)
    return urls_to_cache


def regenerate_all():
    total_objects = []
    for post in Post.objects.published():
        url = post.get_absolute_url()
        if is_seo_redirect(url):
            continue
        total_objects.append(url)
        uncacheit(post)
        cacheit(post)

    total_objects += regenerate_pages()
    print "Generated %d cached pages : " % len(total_objects)
    print "\n".join(total_objects)
