from django import forms
from django.contrib.auth import get_user_model

from eb.models import UserInfo

User = get_user_model()


class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('birth_date', 'gender', )


class UserInfoForm(forms.ModelForm):
    birth_date = UserForm.base_fields['birth_date']
    gender = UserForm.base_fields['gender']

    class Meta:
        model = UserInfo
        fields = ('personal_email', 'phone', )
