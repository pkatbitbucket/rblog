from django.conf import settings
from django.db.utils import IntegrityError

from core.models import User
from coverfox.models import Company

import csv
import datetime
import dateutil.parser
import os

company, _ = Company.objects.get_or_create(name='Toppr')
csv_file = 'employees_list'


def get_date(s_date):
    date_patterns = ["%d/%m/%y", "%d/%m/%Y"]
    for pattern in date_patterns:
        try:
            return datetime.datetime.strptime(s_date, pattern).date()
        except:
            pass

with open(os.path.join(settings.SETTINGS_FILE_FOLDER, '../base/eb/loaders/'+company.slug+'/'+csv_file+'.csv'), 'rt') as f:
    reader = csv.DictReader(f)
    for row in reader:
        if not row['Sr. No']:
            continue
        names = row['Name'].split(' ')
        first_name, middle_name, last_name = '', '', ''
        if names:
            names.reverse()
            first_name = names.pop()
        if names:
            names.reverse()
            last_name = names.pop()
        middle_name = ' '.join(names)

        try:
            birth_date = get_date(row['Emp. DOB'])
            assert birth_date, "Can not parse date of birth " + row['Emp. DOB']
            user = User.objects.create_user(
                email=row['Email'],
                birth_date=birth_date,
                first_name=first_name,
                middle_name=middle_name,
                last_name=last_name,
            )
        except IntegrityError:
            user = User.objects.get_user(email=row['Email'], phone='')
        except Exception as e:
            pass

        try:
            membership = company.add_employee(
                user=user,
                title=row['Designation'],
                employee_id=row['Emp. Number'],
                joined_on=dateutil.parser.parse(row['Joining Date']),
            )
        except AssertionError as e:
            print user, e.message
        else:
            print "Added", user, "to", company, membership.employee_id
