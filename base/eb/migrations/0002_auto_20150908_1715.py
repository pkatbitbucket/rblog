# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('eb', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='userinfo',
            name='personal_email',
            field=models.EmailField(null=True, max_length=255, blank=True, unique=True, verbose_name=b'e-mail address', db_index=True),
        ),
        migrations.AddField(
            model_name='userinfo',
            name='phone',
            field=models.CharField(blank=True, max_length=10, null=True, validators=[django.core.validators.RegexValidator(regex=b'\\d{10}', message=b"Phone number must be entered in the format: '9999889999'.")]),
        ),
    ]
