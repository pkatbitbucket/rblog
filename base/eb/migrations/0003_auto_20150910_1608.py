# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import core.models


class Migration(migrations.Migration):

    dependencies = [
        ('coverfox', '0005_document'),
        ('eb', '0002_auto_20150908_1715'),
    ]

    operations = [
        migrations.CreateModel(
            name='Policy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('policy_id', models.CharField(max_length=10, null=True, blank=True)),
                ('member', models.ForeignKey(to='coverfox.CompanyMembership')),
            ],
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='PolicyMaster',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='PolicyMasterDocument',
            fields=[
            ],
            options={
                'abstract': False,
                'proxy': True,
            },
            bases=('coverfox.document',),
        ),
        migrations.AddField(
            model_name='userinfo',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 10, 16, 8, 37, 889548), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userinfo',
            name='modified_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 10, 16, 8, 49, 509581), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='policymaster',
            name='policy_document',
            field=models.ForeignKey(related_name='eb_policymaster_related', blank=True, to='eb.PolicyMasterDocument', null=True),
        ),
        migrations.AddField(
            model_name='policy',
            name='policy_master',
            field=models.ForeignKey(to='eb.PolicyMaster'),
        ),
        migrations.AlterUniqueTogether(
            name='policy',
            unique_together=set([('member', 'policy_master')]),
        ),
    ]
