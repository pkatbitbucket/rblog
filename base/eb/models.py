from django.core.validators import RegexValidator
from django.db import models

from core.models import BaseModel

from coverfox.models import Company, CompanyMembership, Document
from core.models import User

from datetime import datetime
from putils import Extension


class UserInfo(BaseModel):
    user = models.OneToOneField(User, related_name='ebuserinfo')
    personal_email = models.EmailField(
        'e-mail address', max_length=255,
        null=True, blank=True, unique=True, db_index=True
    )
    phone_regex = RegexValidator(
        regex=r'\d{10}',
        message="Phone number must be entered in the format: '9999889999'."
    )
    phone = models.CharField(
        validators=[phone_regex], blank=True, null=True, max_length=10
    )


class UserExtension(Extension):
    model = User

    def get_companies(self, current=True):
        q = Company.objects.filter(companymembership__user=self)
        if current:
            q = q.filter(companymembership__left_on__isnull=True)
        return q.distinct()


class CompanyExtension(Extension):
    model = Company

    def is_employee(self, user, current=True):
        qs = CompanyMembership.objects.filter(company=self, user=user)
        if current:
            qs = qs.filter(left_on__isnull=True)
        return bool(qs.count())

    def get_employees(self, current=True):
        qs = CompanyMembership.objects.filter(company=self)
        if current:
            qs = qs.filter(left_on__isnull=True)
        qs = qs.select_related("user")
        return qs

    def add_employee(
        self, user, title, employee_id, joined_on=None, left_on=None
    ):
        assert not self.is_employee(user), "Already an employee"
        return CompanyMembership.objects.create(
            company=self,
            user=user,
            title=title,
            employee_id=employee_id,
            joined_on=joined_on or datetime.today(),
            left_on=left_on,
        )


class PolicyMasterDocumentManager(models.Manager):
    def get_queryset(self):
        return super(PolicyMasterDocumentManager, self).get_queryset().filter(
            eb_policymaster_related__isnull=False
        )


class PolicyMasterDocument(Document):
    all_objects = models.Manager()
    objects = PolicyMasterDocumentManager()

    def __unicode__(self):
        return '%s - (%s)' % (
            self.name,
            ', '.join(
                self.eb_policymaster_related.values_list(
                    'name', flat=True
                ).distinct()
            )
        )

    class Meta(Document.Meta):
        proxy = True


class PolicyMaster(BaseModel):
    policy_document = models.ForeignKey(
        PolicyMasterDocument, null=True, blank=True,
        related_name='%(app_label)s_%(class)s_related'
    )


class Policy(BaseModel):
    member = models.ForeignKey(CompanyMembership)
    policy_master = models.ForeignKey(PolicyMaster)
    policy_id = models.CharField(blank=True, null=True, max_length=10)

    class Meta:
        unique_together = ("member", "policy_master", )
