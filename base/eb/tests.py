from django.test import TransactionTestCase, TestCase

from datetime import datetime

from coverfox.models import Company, CompanyMembership
from core.models import User
from putils import Extension


class CompanyTestBase(TestCase):
    def setUp(self):
        self.cfox = Company.objects.create(
            name="Coverfox",
            slug="cfox",
        )

        self.rane = User.objects.create_user(
            email="rane@rane.com",
            username="rane",
            password="foo",
            first_name="Devendra"
        )


class CompanyTest(CompanyTestBase):
    def test_extension(self):
        self.assertIsNotNone(getattr(Company, "add_employee", None))
        self.assertIsNotNone(getattr(Company, "is_employee", None))
        self.assertIsNotNone(getattr(Company, "get_employees", None))

    def test_cant_overwrite_save(self):
        try:
            class CompanyExtension(Extension):
                model = Company

                def save(self, user, current=True):
                    pass
        except Exception:
            pass
        else:
            self.assertEqual("Exection", "not raised")

    def test_add_employee(self):
        self.assertEqual(CompanyMembership.objects.count(), 0)
        joined_on = datetime.today()
        title = "CTO"
        eid = "00002"

        cm = self.cfox.add_employee(
            self.rane, title=title, eid=eid, joined_on=joined_on
        )

        self.assertEqual(CompanyMembership.objects.count(), 1)
        self.assertEqual(cm.joined_on, joined_on)
        self.assertEqual(cm.title, title)
        self.assertEqual(cm.eid, eid)
        self.assertEqual(cm.user, self.rane)
        self.assertEqual(cm.company, self.cfox)


class CompanyTransactionTest(CompanyTestBase, TransactionTestCase):
    def test_get_employees(self):
        self.assertEqual(self.cfox.get_employees().count(), 0)
        cm = self.cfox.add_employee(self.rane, title="CTO", eid="0123")
        self.assertEqual(self.cfox.get_employees().count(), 1)
        emp0 = self.cfox.get_employees()[0]
        self.assertEqual(emp0, cm)

        self.assertNumQueries(0, lambda: emp0.user)

        cm.left_on = datetime.today()
        cm.save()
        self.assertEqual(self.cfox.get_employees().count(), 0)
