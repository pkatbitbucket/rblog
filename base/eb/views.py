from django.views.generic.edit import UpdateView

from .forms import UserInfoForm


class BaseAbstractView(object):
    pass


class UserInfoUpdateView(BaseAbstractView, UpdateView):
    form_class = UserInfoForm

    def get_object(self):
        return self.request.user.ebuserinfo

    def form_valid(self, form):
        form.instance.user.birth_date = form.cleaned_data['birth_date']
        form.instance.user.gender = form.cleaned_data['gender']
        form.instance.user.save()
        return super(self.__class__, self).form_valid(form)
