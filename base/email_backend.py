import mandrill

import smtplib
import ssl

from django.conf import settings
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.core.mail.backends.base import BaseEmailBackend
from django.core.mail.message import sanitize_address
from django.core.mail.utils import DNS_NAME
from django.template import Context, Template

from celery.contrib.methods import task_method

from celery_app import app
from core.models import Mail

DEFAULT_FROM_EMAIL = 'info@coverfox.com'
DEFAULT_FROM_NAME = 'Team Coverfox'


class CeleryEmailBackend(BaseEmailBackend):
    """
    A wrapper that manages the SMTP network connection.
    """

    def __init__(self, host=None, port=None, username=None, password=None,
                 use_tls=None, fail_silently=False, **kwargs):
        super(CeleryEmailBackend, self).__init__(fail_silently=fail_silently)
        self.host = host or settings.EMAIL_HOST
        self.port = port or settings.EMAIL_PORT
        if username is None:
            self.username = settings.EMAIL_HOST_USER
        else:
            self.username = username
        if password is None:
            self.password = settings.EMAIL_HOST_PASSWORD
        else:
            self.password = password
        if use_tls is None:
            self.use_tls = settings.EMAIL_USE_TLS
        else:
            self.use_tls = use_tls

    def send_messages(self, email_messages):
        """
        Sends one or more EmailMessage objects and returns the number of email
        messages sent.
        """
        if not email_messages:
            return

        for message in email_messages:
            self._send.apply_async([message], queue='mail')
        return len(email_messages)

    def get_parts_for_template(self, template_type, context):
        mail = Mail.objects.get(mtype=template_type, is_active=True)

        ctx = Context({'obj': context})
        subject = Template(mail.title).render(ctx)
        text = Template(mail.text).render(ctx)
        html = Template(mail.body).render(ctx)
        return subject, text, html

    def get_message_for_template(self, email_message):
        email_message['name'] = email_message['name'].title()
        email_message['from_name'] = email_message['from_name'].title()

        subject, text, html = self.get_parts_for_template(
            email_message['mail_type'], email_message)
        message = EmailMultiAlternatives(subject, text, email_message[
                                         'from_email'], [email_message['to_email']])
        message.attach_alternative(html, 'text/html')
        return message

    @app.task(bind=True, filter=task_method, default_retry_delay=10, max_retries=5, queue='mail')
    def _send(task, self, email_message):
        try:
            if isinstance(email_message, EmailMessage):
                self._send_message(email_message)
            elif type(email_message) is dict:
                if not email_message.get('from_email', None):
                    email_message['from_email'] = DEFAULT_FROM_EMAIL
                if not email_message.get('from_name', None):
                    email_message['from_name'] = DEFAULT_FROM_NAME

                if email_message.get('use_mandrill', False):
                    self._send_mandrill_message(email_message)
                else:
                    self._send_message(
                        self.get_message_for_template(email_message))
        except Exception as exception:
            raise task.retry(exc=exception)

    def _send_message(self, email_message):
        """A helper method that does the actual sending."""
        if not email_message.recipients():
            return

        from_email = sanitize_address(
            email_message.from_email, email_message.encoding)
        recipients = [sanitize_address(addr, email_message.encoding)
                      for addr in email_message.recipients()]

        try:
            """
            Ensures we have a connection to the email server. Returns whether or
            not a new connection was required (True or False).
            """
            connection = smtplib.SMTP(self.host, self.port,
                                      local_hostname=DNS_NAME.get_fqdn())
            if self.use_tls:
                connection.ehlo()
                connection.starttls()
                connection.ehlo()
            if self.username and self.password:
                connection.login(self.username, self.password)

            connection.sendmail(from_email, recipients,
                                email_message.message().as_string())

            """Closes the connection to the email server."""
            try:
                connection.quit()
            except (ssl.SSLError, smtplib.SMTPServerDisconnected):
                # This happens when calling quit() on a TLS connection
                # sometimes, or when the connection was already disconnected
                # by the server.
                connection.close()
        except smtplib.SMTPException:
            if not self.fail_silently:
                raise

    def _send_mandrill_message(self, email_message):
        try:
            """
            email_message = {
                'mail_type' : mail_type,
                'user_id' : user_id,
                'tag' : tag_for_mandill,
                'to_email' : to_email,
                'name' : name
                'from_name' : from_name, #Optional
                'from_email' : from_email, #Optional
                'template_name' : template_name, #Optional
                'attachments': [{'content': 'ZXhhbXBsZSBmaWxl',
                      'name': 'myfile.txt',
                      'type': 'text/plain'}], #Optional (list of dict)
                ...
                ...
                <any other data to pass in template>
            }
            """

            email_message['name'] = email_message['name'].title()
            email_message['from_name'] = email_message['from_name'].title()

            subject, text, html = self.get_parts_for_template(
                email_message['mail_type'], email_message)

            user_id = email_message['user_id']
            to_email = email_message['to_email']
            to_name = email_message['name']

            from_email = email_message['from_email']
            from_name = email_message['from_name']
            tag = email_message.get('tag', 'default')
            attachments = email_message.get('attachments')

            template_name = email_message.get('template_name', 'base')
            template_content = [
                {
                    'content': html,
                    'name': 'mail-body'
                },
                {
                    'content': subject,
                    'name': 'mail-title'
                }
            ]

            mandrill_client = mandrill.Mandrill(settings.MANDRILL_API_KEY)

            to = [{'email': to_email, 'name': to_name, 'type': 'to'}] + [{'email': em,
                                                                          'name': '', 'type': 'bcc'} for em in email_message.get('bcc', [])]
            message = {
                'auto_html': None,
                'auto_text': None,
                'bcc_address': None,
                'from_email': from_email,
                'from_name': from_name,
                'headers': {'Reply-To': from_email},
                'text': text,
                'important': False,
                'inline_css': None,
                'merge': True,
                'merge_vars': [{'rcpt': to_email,
                                'vars': [{'content': 'merge2 content', 'name': 'merge2'}]}],
                'metadata': {'website': 'www.coverfox.com'},
                'preserve_recipients': None,
                'recipient_metadata': [{'rcpt': to_email,
                                        'values': {'user_id': user_id}}],
                'return_path_domain': None,
                'signing_domain': None,
                'subject': subject,
                'tags': [tag],
                'to': to,
                'track_clicks': None,
                'track_opens': None,
                'tracking_domain': None,
                'url_strip_qs': None,
                'view_content_link': None
            }

            if attachments:
                message.update({'attachments': attachments})

            mandrill_client.messages.send_template(
                template_name=template_name,
                template_content=template_content,
                message=message,
                async=False,
                ip_pool='Main Pool'
            )
        except Exception, e:
            if not self.fail_silently:
                raise
