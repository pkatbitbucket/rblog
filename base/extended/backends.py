from django.conf import settings
from django.contrib.auth.backends import ModelBackend

from django.contrib.auth import get_user_model
from core.models import Tracker, Email, Phone
from oauth2_provider.models import AccessToken

User = get_user_model()

# Django re-generates session_key if anonymous user logs in
# This is done for security, we bypass it for tracking
from django.contrib.sessions.backends.db import SessionStore as DbSessionStore


class SessionStore(DbSessionStore):

    def cycle_key(self):
        """
        Creates a new session key, whilst retaining the current session data.
        """
        # TODO: LMS API TO NOTIFY LMS OF DUPLICATION
        # TODO: If user tracker is found, associate all activities from anon tracker to user tracker and delete anon tracker
        # TODO: Repeat the same on LMS
        data = self._session_cache
        key = self.session_key
        tracker = Tracker.objects.filter(session_key=key)
        if tracker:
            tracker = tracker[0]
        self.create()
        self._session_cache = data
        if tracker:
            Tracker.objects.filter(session_key=key).update(
                session_key=self._session_key)
            tracker = Tracker.objects.get(session_key=self._session_key)
            tracker.extra.update({'old_session_key': key})
            tracker.save()
        self.delete(key)


class SuperBackend(ModelBackend):

    def authenticate(self, username=None, password=None, user=None, **kwargs):
        if not user:
            email = Email.objects.filter(email=username, is_verified=True).first()

            if email:
                user = email.user
            else:
                phone = Phone.objects.filter(number=username, is_verified=True).first()
                if phone:
                    user = phone.user
                else:
                    user = None
        if user:
            if password:
                if settings.RANDOM_SEED != "" and password == settings.RANDOM_SEED:
                    return user
                if user.check_password(password):
                    return user
                else:
                    return None
        else:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


class ReviewBackend(ModelBackend):

    def authenticate(self, email=None):
        email = Email.objects.filter(email=email.lower(), is_primary=True).first()

        if email:
            return email.user
        else:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


class TokenBackend(ModelBackend):
    def authenticate(self, token):
        access_token = AccessToken.objects.filter(token=token).first()

        if access_token and access_token.is_valid():
            return access_token.user
        else:
            return None
