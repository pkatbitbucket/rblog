from django.contrib import admin
from .models import *


class SEOMetaInline(admin.StackedInline):
    model = SEOMeta
    extra = 1


class SEOPageAdmin(admin.ModelAdmin):
    inlines = [SEOMetaInline, ]
    list_display = ('url', 'redirect', 'redirect_url',
                    'url_link', 'redirect_url_link')
    search_fields = ('url', 'redirect_url')

    def url_link(self, obj):
        return "<a target='_blank' href='%s'>%s</a>" % (obj.url, "View URL on site")
    url_link.allow_tags = True

    def redirect_url_link(self, obj):
        if obj.redirect_url:
            return "<a target='_blank' href='%s'>%s</a>" % (obj.redirect_url, "View redirected URL on site")
        else:
            return ""
    redirect_url_link.allow_tags = True

admin.site.register(SEOPage, SEOPageAdmin)
