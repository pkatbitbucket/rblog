from .models import SEOPage


def get_seo_model(url):
    try:
        return SEOPage.objects.get(url=url.lower())
    except SEOPage.DoesNotExist:
        return None


def seo(request):
    url = request.path
    seo = get_seo_model(url)
    return {'seo': seo} if seo else {}
