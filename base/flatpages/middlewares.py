from django.http import Http404
from django.conf import settings
from django.shortcuts import redirect
from django.core.urlresolvers import resolve, Resolver404

from .models import SEOPage
from .views import index


def internal_redirect(request, url):
    res = resolve(url)
    return res.func(request, *res.args, **res.kwargs)


class SEORedirectMiddleware(object):

    def process_request(self, request):
        # print request.path
        page = SEOPage.objects.get_seo_model(request.path)
        if page and page.redirect:
            if page.redirect == '301' or page.redirect == '302':
                permanent = page.redirect == '301'
                return redirect(page.redirect_url, permanent=permanent)
            if page.redirect == 'INTR':
                return internal_redirect(request, page.redirect_url)


class FlatpageFallbackMiddleware(object):

    def process_response(self, request, response):
        # print "in Flatpage Middleware"
        if response.status_code != 404:
            # No need to check for a flatpage for non-404 responses.
            return response
        try:
            page = SEOPage.objects.get_seo_model(request.path)
            if page and page.redirect == 'INTR':
                url = page.redirect_url
            else:
                url = request.path
            return index(request, url)
        # Return the original response if any errors happened. Because this
        # is a middleware, we can't assume the errors will be caught elsewhere.
        except Http404:
            return response
        except Exception:
            if settings.DEBUG:
                raise
            return response
