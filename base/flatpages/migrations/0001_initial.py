# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SEOMeta',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(db_index=True, max_length=100, choices=[
                 (b'description', b'description'), (b'robots', b'robots')])),
                ('content', models.TextField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='SEOPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('url', models.CharField(unique=True, max_length=255)),
                ('title', models.CharField(max_length=255, blank=True)),
                ('redirect', models.CharField(default=b'', max_length=100, db_index=True, blank=True, choices=[
                 (b'INTR', b'INTERNAL Redirect'), (b'301', b'301 Redirect'), (b'302', b'302 Redirect')])),
                ('redirect_url', models.CharField(
                    db_index=True, max_length=255, blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='seometa',
            name='url',
            field=models.ForeignKey(
                related_name='metas', to='flatpages.SEOPage'),
        ),
        migrations.AlterUniqueTogether(
            name='seometa',
            unique_together=set([('name', 'url')]),
        ),
    ]
