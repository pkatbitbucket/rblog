from django.db import models
from django.core.validators import ValidationError

from urlparse import urlparse


def is_seo_redirect(url):
    seo = SEOPage.objects.get_seo_model(url)
    return seo and seo.redirect


class SEOPageManager(models.Manager):

    def get_seo_model(self, url):
        try:
            return SEOPage.objects.get(url=url)
        except SEOPage.DoesNotExist:
            return None


class SEOPage(models.Model):
    objects = SEOPageManager()
    redirect_choices = (
        ('INTR', 'INTERNAL Redirect'),
        ('301', '301 Redirect'),
        ('302', '302 Redirect'),
    )

    url = models.CharField(max_length=255, unique=True)
    title = models.CharField(max_length=255, blank=True)
    redirect = models.CharField(
        max_length=100, choices=redirect_choices, blank=True, default='', db_index=True)
    redirect_url = models.CharField(max_length=255, blank=True, db_index=True)

    def clean(self, *args, **kwargs):
        if not self.redirect and not self.title:
            raise ValidationError(
                "Title cannot be blank if redirect is not set.")
        if self.redirect and not self.redirect_url:
            raise ValidationError(
                "Redirect url cannot be blank if redirect is set.")
        if self.redirect_url and not self.redirect_url.startswith("/"):
            raise ValidationError("Redirect url must start with a slash.")

        super(SEOPage, self).clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.url = urlparse(self.url).path

        super(SEOPage, self).save(*args, **kwargs)


class SEOMeta(models.Model):
    name_choices = (
        ('description', 'description'),
        ('robots', 'robots'),
    )
    url = models.ForeignKey(SEOPage, related_name='metas')
    name = models.CharField(
        max_length=100, choices=name_choices, db_index=True)
    content = models.TextField(null=True, blank=True)

    class Meta:
        unique_together = ('name', 'url')
