from django.shortcuts import render, redirect
from django.http import Http404
from django.template import TemplateDoesNotExist
from django.conf import settings

from .models import SEOPage


def index(request, url=None):
    path = url if url else request.path
    if path.startswith("/"):
        path = path[1:]
    if path.endswith("/"):
        # remove leading and ending slash
        template = "flatpages:flatpages/%s.%s" % (path[:-1], "html")
        # print "flatpages...."
        try:
            breadcrumb = []
            b_url = "/"
            breadcrumb.append({'url': b_url, 'title': "Home"})
            for p in path[:-1].split("/"):
                b_url += p + "/"
                breadcrumb.append({'url': b_url, 'title': " ".join(
                    [x.capitalize() for x in p.replace("-", " ").split(" ")])})
            return render(request, template, {'breadcrumb': breadcrumb})
        except TemplateDoesNotExist:
            # print "404 error in Flatpages"
            raise Http404
    else:
        return redirect('/%s/?%s' % (path, request.GET.urlencode()), permanent=True)
