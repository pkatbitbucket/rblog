import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')

from django.conf import settings


debug = settings.DEBUG
persistent = True

db = str(os.path.abspath(os.path.join(settings.LOG_PATH, 'flower')))
logging = 'INFO'

address = '0.0.0.0'
port = 8000
