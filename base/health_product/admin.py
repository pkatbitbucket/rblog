from django.contrib import admin
from .models import Transaction

class TransactionAdmin(admin.ModelAdmin):
    search_fields = ('transaction_id', 'proposer_email', 'proposer_mobile', 'proposer_first_name', 'status', 'premium_paid')
    list_display = ('transaction_id', 'proposer_first_name', 'proposer_email', 'proposer_mobile', 'status', 'premium_paid', 'policy_document', 'created_on')
    fields = ('transaction_id', 'status', 'policy_token', 'created_on', 'payment_on', 'policy_document',
              'policy_number', 'policy_start_date', 'policy_end_date', 'premium_paid')
    readonly_fields = ('transaction_id', 'status', 'policy_token', 'created_on', 'payment_on', 'policy_document')


admin.site.register(Transaction, TransactionAdmin)
