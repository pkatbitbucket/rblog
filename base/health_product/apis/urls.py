from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^webview/$', views.webview, name='webview'),
]
