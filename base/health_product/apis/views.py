import json
import hmac
import requests
import base64
from hashlib import sha1
from datetime import datetime
from collections import OrderedDict
from django.http import HttpResponse, HttpResponseRedirect, Http404, HttpResponseNotAllowed
from django.shortcuts import redirect, render_to_response, get_object_or_404
from django.core.serializers.json import DjangoJSONEncoder
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from oauth2_provider.decorators import protected_resource
from apis.decorators import authenticate_api
from core.models import User
from account_manager.models import RelationshipManager, UserRelationshipManager, UserTransaction, UserAccount, get_health_policy, get_motor_policy, get_user_details, OfflineTransaction
from account_manager.apis.forms import UpdateUserAccount

API_AUTHENTICATION_ID = 'mLkJwTkTQYJ0EMBqrdMjC6I1FFrcvoFypcGMtHD8'
API_AUTHENTICATION_KEY = 'jfNPOWpJZEuqJ86Lap0jPrTNL8QgaxCxui8vqVXcDit3FGyWpag0O9HJEmv5M1VRdgUYsL6C2mqbB7wNuMsGVtHoK1LrrnsnixHioxhrpIM3aSkr3eILWqaYaZ0VgBSs'

def get_headers(request):
    if 'path' in request.GET:
        path = request.GET['path']
        request_time = datetime.utcnow()
        string_to_sign = "%s %s" % (path, request_time.strftime('%Y-%m-%dT%H:%M:%SZ'))
        h = hmac.HMAC(API_AUTHENTICATION_KEY, string_to_sign, sha1)
        signature = base64.b64encode(h.digest())
        headers = {
            'Authorization' : 'CVFX ' + API_AUTHENTICATION_ID + ':' + signature,
            'Date' : request_time.strftime('%Y-%m-%dT%H:%M:%SZ'),
        }
        return HttpResponse(json.dumps(headers))
    else:
        raise Http404

@csrf_exempt
@protected_resource()
def webview(request):
    if request.method == 'POST':
        return redirect('/health-plan')
    else:
        return HttpResponseNotAllowed('POST')