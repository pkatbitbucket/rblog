import functools
import hashlib
import django
import types

from django.core.cache import caches

cache = caches['health-results']

MEMOIZED_ENABLED = False
TIMEOUT = 3600 * 24 * 7
NOT_FOUND_IN_CACHE = 'NOT_FOUND_IN_CACHE'

SUPPORTED_ARG_TYPES = (
    dict,
    str,
    unicode,
    django.db.models.Model,
    list,
    float,
    int,
    long,
)

# General object cache


def str_object(obj):
    if isinstance(obj, str):
        return obj
    if isinstance(obj, unicode):
        return obj
    elif isinstance(obj, django.db.models.Model):
        return str(obj.id)
    elif isinstance(obj, dict):
        return str(sorted(obj.items()))
    elif isinstance(obj, long):
        return str(obj)
    elif isinstance(obj, float):
        return str(obj)
    elif isinstance(obj, int):
        return str(obj)
    elif isinstance(obj, list):
        return str(sorted(obj))
    else:
        try:
            return str(obj.cache_args())
        except AttributeError:
            # print "could not hash, returning None: ", obj
            raise ValueError


class memoized(object):
    # abstract class dont use.

    def __init__(self, func, prefix='', timeout=None):
        self.func = func
        self.prefix = prefix
        self.timeout = timeout
        self.name = func.__name__

    def generate_hash(self, *args, **kwargs):
        try:
            key_args = str(map(str_object, args))
            key_kwargs = str(sorted([(key, str_object(val))
                                     for key, val in kwargs.items()]))
            return hashlib.sha1((key_args + key_kwargs).encode('utf-8')).hexdigest()
        except ValueError:
            return None

    def fetch(self, key, args=(), kwargs={}, from_cache=False):
        if MEMOIZED_ENABLED and from_cache:
            result = cache.get(key, NOT_FOUND_IN_CACHE)
            if result is NOT_FOUND_IN_CACHE:
                # print "not in cache", key
                result = self.func(*args, **kwargs)
                cache.set(key, result, self.timeout)
            else:
                # print "found in cache", key
                pass
        else:
            result = self.func(*args, **kwargs)

        return result

    def __call__(self, *args, **kwargs):
        hashs = self.generate_hash(*args, **kwargs)
        if self.prefix:
            key = ".".join(
                (self.prefix, self.func.__module__, self.name, hashs))
        else:
            key = ".".join((self.func.__module__, self.name, hashs))
        return self.fetch(key, from_cache=hashs is not None, args=args, kwargs=kwargs)


class cache_memoized_func(memoized):
    pass


class cache_memoized_method(memoized):

    def __call__(self, *args, **kwargs):
        cls = args[0].__class__.__name__
        hashs = self.generate_hash(*args, **kwargs)
        if self.prefix:
            key = ".".join(
                (self.prefix, self.func.__module__, cls, self.name, hashs))
        else:
            key = ".".join((self.func.__module__, cls, self.name, hashs))
        return self.fetch(key, from_cache=hashs is not None, args=args, kwargs=kwargs)

    def __get__(self, obj, objtype):
        '''Support instance methods.'''
        return functools.partial(self.__call__, obj)


class CacheAndStaticClass(type):
    """
    This class adds staticmethod and cache_memorized_method decorator to all the functions
    for which this class is a metaclass.
    """
    def __new__(mcs, name, bases, attrs):
        for attr_name, attr_value in attrs.iteritems():
            if isinstance(attr_value, types.FunctionType):
                attrs[attr_name] = staticmethod(cache_memoized_method(attr_value))
        return super(CacheAndStaticClass, mcs).__new__(mcs, name, bases, attrs)
