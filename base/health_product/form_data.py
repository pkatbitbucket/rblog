__author__ = 'vinay'


def get_value(json_data, path=None):
    path = path.split(".")
    val = json_data
    path_len = len(path)
    i = 1
    for key in path:
        if i == path_len:
            val = val.get(key, "")
        else:
            val = val.get(key, {})

    if isinstance(val, dict) or isinstance(val, list):
        val = str('')

    if isinstance(val, int):
        val = str(val)

    return str(val.encode('utf-8'))


def create_insured_forms(proposal):
    # Get insured.
    insured_persons = proposal.get("insured", {})
    roles = insured_persons.keys()
    data = {}
    for role in roles:
        insured_json = insured_persons[role]
        person_info = {
            "fname": get_value(insured_json, "first_name.value"),
            "lname": get_value(insured_json, "last_name.value"),
            "dob": get_value(insured_json, "date_of_birth.value"),
        }
        person_info = {'insured_{}_{}'.format(role, key): value for key, value in person_info.items()}

        height_val1 = get_value(insured_json, "height.value1")
        height_val2 = get_value(insured_json, "height.value2")

        health_data = {
            "height_val1": str(height_val1),
            "height_val2": str(height_val2),
            "weight": str(get_value(insured_json, "weight.value"))
        }
        health_data = {'insured_{}_{}'.format(role, key): value for key, value in health_data.items()}
        data.update(person_info)
        data.update(health_data)
    return data


def insert_nominee_info(form_data):
    nominee = form_data.get("nominee", {})
    roles = nominee.keys()
    data = {}
    for role in roles:
        if role == 'self':
            continue
        # Create nominee person model.
        role_nominee_data = nominee[role]
        person_data = {
            "f_name": get_value(role_nominee_data, "first_name.value"),
            "l_name": get_value(role_nominee_data, "last_name.value"),
            "dob": get_value(role_nominee_data, "date_of_birth.value"),
        }
        person_data = {'nominee_{}'.format(key): value for key, value in person_data.items()}
        data.update(person_data)
    return data


def generate_form_data(transaction):
    form_data = transaction.insured_details_json
    data = {}

    if "form_data" in form_data:
        form_data = form_data["form_data"]
        # -------------------------------------------------------------
        # Proposal form.
        # -------------------------------------------------------------
        proposer = form_data.get("proposer", {})
        person_info = {
            "f_name": get_value(proposer, "personal.first_name.value"),
            "l_name": get_value(proposer, "personal.last_name.value"),
            "gender": get_value(proposer, "personal.gender.value"),
            "maritial_status": get_value(proposer, "personal.marital_status.value"),
            "dob": get_value(proposer, "personal.date_of_birth.value"),
            "occupation": get_value(proposer, "personal.occupation.value"),
            "mobile": get_value(proposer, "contact.mobile.value"),
            "email": get_value(proposer, "contact.email.value"),
            "landline": get_value(proposer, "contact.landline.display_value"),
        }
        person_info = {'{}_{}'.format("proposer", key): value for key, value in person_info.items()}
        # -------------------------------------------------------------
        # Insured form.
        # -------------------------------------------------------------
        insured_data = create_insured_forms(form_data)
        # -------------------------------------------------------------
        # Address form.
        # -------------------------------------------------------------
        address_info = {
            "address_line1": get_value(proposer, "address.address1.value"),
            "address_line2": get_value(proposer, "address.address1.value"),
            "state": get_value(proposer, "address.state.dependant_value") or get_value(proposer, "address.state.value"),
            "city": get_value(proposer, "address.state.value1") or get_value(proposer, "address.city.value"),
            "pincode": get_value(proposer, "address.pincode.value1") or get_value(proposer, "address.city.value"),
            "landmark": get_value(proposer, "address.landmark.value")
        }
        # -------------------------------------------------------------
        # Health nominee.
        # -------------------------------------------------------------
        nominee_data = insert_nominee_info(form_data) if "nominee" in form_data else {}
        data.update(person_info)
        data.update(insured_data)
        data.update(address_info)
        data.update(nominee_data)

    return data
