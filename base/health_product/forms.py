import json
from django import forms
from health_product.models import Transaction
from wiki.models import City, State, MedicalCondition, Insurer, HealthProduct, Slab
from core import forms as core_forms


class OfflineForm(forms.ModelForm):
    ATTRS = {"class": "form-control"}
    DATE_FIELDS = ('policy_start_date', 'policy_end_date', 'payment_on', 'proposer_date_of_birth')

    members_json = forms.CharField(widget=forms.HiddenInput())
    health_json = forms.CharField(widget=forms.HiddenInput(), required=False)

    insurer = forms.ModelChoiceField(
        Insurer.objects.order_by('title'),
        widget=forms.widgets.Select(ATTRS),
    )
    plan_name = forms.CharField(widget=forms.Select())
    slab_id = forms.CharField(widget=forms.Select(), label='Cover')  # filled as cover

    proposer_landline = forms.CharField(required=False, max_length=12)

    proposer_city = forms.ModelChoiceField(
        City.objects.order_by('name'),
        widget=forms.widgets.Select(ATTRS),
    )
    proposer_state = forms.ModelChoiceField(
        State.objects.order_by('name'),
        widget=forms.widgets.Select(ATTRS),
    )

    medical_conditions = forms.ModelChoiceField(
        MedicalCondition.objects.all(),
        empty_label=None, required=False,
        help_text='Select the medical conditions for any of the member.',
        widget=forms.widgets.Select({'multiple': 'true'})
    )

    # additional validations
    proposer_pincode = forms.IntegerField(max_value=999999, min_value=100000,
                                          error_messages={'max_value': 'Please enter a valid 6 digit pin code!',
                                                          'min_value': 'Please enter a valid 6 digit pin code!'})
    proposer_mobile = forms.IntegerField(min_value=6000000000, max_value=9999999999,
                                         error_messages={'max_value': 'Please enter a valid 10 digit mobile number!',
                                                         'min_value': 'Please enter a valid 10 digit mobile number!'})

    premium_paid = forms.FloatField(max_value=9999999999,
                                    error_messages={'max_value': 'Maximum value exceeded!'})
    policy_number = forms.CharField()
    policy_start_date = forms.DateField()
    policy_end_date = forms.DateField()
    payment_on = forms.DateField()
    proposer_address1 = forms.CharField(label='Address Line 1')
    proposer_address2 = forms.CharField(label='Address Line 2')
    proposer_address_landmark = forms.CharField(label='Line 3 (Landmark)')
    proposer_date_of_birth = forms.DateField()

    class Meta:
        model = Transaction
        fields = ['proposer_pincode', 'insurer', 'plan_name', 'slab_id', 'premium_paid',
                  'policy_number', 'policy_start_date', 'policy_end_date', 'payment_on',
                  'proposer_first_name', 'proposer_middle_name', 'proposer_last_name', 'proposer_address1',
                  'proposer_address2', 'proposer_address_landmark', 'proposer_state', 'proposer_city',
                  'proposer_email', 'proposer_mobile', 'proposer_landline', 'proposer_date_of_birth',
                  'proposer_marital_status', 'medical_conditions']
        # can be set
        # no_of_people_insured, status

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(OfflineForm, self).__init__(*args, **kwargs)

        self.fields['proposer_address_landmark'].widget = forms.TextInput()

        [field[1].widget.attrs.update(self.ATTRS) for field in self.fields.iteritems() if not isinstance(
            field[1], (forms.BooleanField,))]

        for field in self.DATE_FIELDS:
            self.fields[field].widget.attrs['class'] += ' dateinput'

    def prepare_members_data(self, members_data):
        members = []
        for member in members_data:
            person = member['person'].lower()
            if person == 'you':
                members.append({
                    'age': int(member['age']),
                    'age_in_months': 0,
                    'id': 'self',
                    'person': 'self',
                    'type': 'adult',
                    'verbose': 'You',
                    'gender': 1 if self.request.POST.get('proposer_gender', 'MALE') == 'MALE' else 2
                })

            elif person in ('wife', 'husband'):
                members.append({
                    'age': int(member['age']),
                    'age_in_months': 0,
                    'id': 'spouse',
                    'person': 'spouse',
                    'type': 'adult',
                    'verbose': 'Your %s' % member['person'],
                    'gender': 2 if self.request.POST.get('proposer_gender', 'MALE') == 'MALE' else 1
                })

            elif person in ('father', 'mother'):
                members.append({
                    'age': int(member['age']),
                    'age_in_months': 0,
                    'id': person,
                    'person': person,
                    'type': 'adult',
                    'verbose': member['person'],
                    'gender': 2 if person == 'mother' else 1
                })

            elif person in ('son', 'daughter'):
                age_month = age = 0
                if 'month' in member['age']:
                    age_month = int(member['age'].split(' ')[0])
                if 'year' in member['age']:
                    age = int(member['age'].split(' ')[0])
                members.append({
                    'age': age,
                    'age_in_months': age_month,
                    'id': person,
                    'person': person,
                    'type': 'parent',
                    'verbose': member['person'],
                    'gender': 2 if person == 'daughter' else 1
                })
        return members

    def clean(self):
        cleaned_data = super(OfflineForm, self).clean()

        # extra required data validations
        if 'slab_id' in cleaned_data:
            try:
                cleaned_data['slab'] = Slab.objects.get(id=cleaned_data['slab_id'])
            except Slab.DoesNotExist:
                self.add_error("slab_id", "Invalid slab selected.")

        if 'plan_name' in cleaned_data:
            try:
                cleaned_data['health_plan'] = HealthProduct.objects.get(id=cleaned_data['plan_name'])
            except HealthProduct.DoesNotExist:
                self.add_error('plan_name', "Invalid health plan selected!")

        if 'members_json' in cleaned_data:
            try:
                members_data = json.loads(cleaned_data['members_json'])
                if not isinstance(members_data, (list,)) or not members_data:
                    raise ValueError('No members data provided.')
                cleaned_data['members_data'] = self.prepare_members_data(members_data)
            except Exception as ex:
                raise forms.ValidationError("Insured members error: %s" % repr(ex))

        if 'health_json' in cleaned_data:
            try:
                health_data = json.loads(cleaned_data['health_json'])
                cleaned_data['health_data'] = health_data
            except Exception as ex:
                raise forms.ValidationError("Health conditions error: %s" % repr(ex))

        return cleaned_data


class HealthFDForm(core_forms.FDForm):
    proposer_f_name = forms.CharField(required=False)
    proposer_l_name = forms.CharField(required=False)
    proposer_gender = forms.CharField(required=False)
    proposer_maritial_status = forms.CharField(required=False)
    proposer_dob = forms.CharField(required=False)
    proposer_occupation = forms.CharField(required=False)
    proposer_mobile = forms.CharField(required=False)
    proposer_email = forms.CharField(required=False)
    proposer_landline = forms.CharField(required=False)
    address_line1 = forms.CharField(required=False)
    address_line2 = forms.CharField(required=False)
    state = forms.CharField(required=False)
    city = forms.CharField(required=False)
    pincode = forms.CharField(required=False)
    landmark = forms.CharField(required=False)

    # Insured Spouse details
    insured_spouse_fname = forms.CharField(required=False)
    insured_spouse_lname = forms.CharField(required=False)
    insured_spouse_dob = forms.CharField(required=False)
    insured_spouse_occupation = forms.CharField(required=False)

    insured_spouse_height_val1 = forms.CharField(required=False)
    insured_spouse_height_val2 = forms.CharField(required=False)
    insured_spouse_weight = forms.CharField(required=False)

    # Insured Father details
    insured_father_fname = forms.CharField(required=False)
    insured_father_lname = forms.CharField(required=False)
    insured_father_dob = forms.CharField(required=False)
    insured_father_occupation = forms.CharField(required=False)

    insured_father_height_val1 = forms.CharField(required=False)
    insured_father_height_val2 = forms.CharField(required=False)
    insured_father_weight = forms.CharField(required=False)

    # Insured Mother details.
    insured_mother_fname = forms.CharField(required=False)
    insured_mother_lname = forms.CharField(required=False)
    insured_mother_dob = forms.CharField(required=False)
    insured_mother_occupation = forms.CharField(required=False)

    insured_mother_height_val1 = forms.CharField(required=False)
    insured_mother_height_val2 = forms.CharField(required=False)
    insured_mother_weight = forms.CharField(required=False)

    # Insured Son details.
    insured_son1_height_val1 = forms.CharField(required=False)
    insured_son1_height_val2 = forms.CharField(required=False)
    insured_son1_weight = forms.CharField(required=False)

    insured_son2_fname = forms.CharField(required=False)
    insured_son2_lname = forms.CharField(required=False)
    insured_son2_dob = forms.CharField(required=False)
    insured_son2_occupation = forms.CharField(required=False)

    insured_son2_height_val1 = forms.CharField(required=False)
    insured_son2_height_val2 = forms.CharField(required=False)
    insured_son2_weight = forms.CharField(required=False)

    insured_son3_fname = forms.CharField(required=False)
    insured_son3_lname = forms.CharField(required=False)
    insured_son3_dob = forms.CharField(required=False)
    insured_son3_occupation = forms.CharField(required=False)

    insured_son3_height_val1 = forms.CharField(required=False)
    insured_son3_height_val2 = forms.CharField(required=False)
    insured_son3_weight = forms.CharField(required=False)

    insured_son4_fname = forms.CharField(required=False)
    insured_son4_lname = forms.CharField(required=False)
    insured_son4_dob = forms.DateField(required=False)
    insured_son4_occupation = forms.CharField(required=False)

    insured_son4_height_val1 = forms.CharField(required=False)
    insured_son4_height_val2 = forms.CharField(required=False)
    insured_son4_weight = forms.CharField(required=False)

    insured_son5_fname = forms.CharField(required=False)
    insured_son5_lname = forms.CharField(required=False)
    insured_son5_dob = forms.CharField(required=False)
    insured_son5_occupation = forms.CharField(required=False)

    insured_son5_height_val1 = forms.CharField(required=False)
    insured_son5_height_val2 = forms.CharField(required=False)
    insured_son5_weight = forms.CharField(required=False)

    insured_son6_fname = forms.CharField(required=False)
    insured_son6_lname = forms.CharField(required=False)
    insured_son6_dob = forms.CharField(required=False)
    insured_son6_occupation = forms.CharField(required=False)

    insured_son6_height_val1 = forms.CharField(required=False)
    insured_son6_height_val2 = forms.CharField(required=False)
    insured_son6_weight = forms.CharField(required=False)

    insured_son7_fname = forms.CharField(required=False)
    insured_son7_lname = forms.CharField(required=False)
    insured_son7_dob = forms.DateField(required=False)
    insured_son7_occupation = forms.CharField(required=False)

    insured_son7_height_val1 = forms.CharField(required=False)
    insured_son7_height_val2 = forms.CharField(required=False)
    insured_son7_weight = forms.CharField(required=False)

    # Insured Daughter details.
    insured_daughter1_fname = forms.CharField(required=False)
    insured_daughter1_lname = forms.CharField(required=False)
    insured_daughter1_dob = forms.DateField(required=False)
    insured_daughter1_occupation = forms.CharField(required=False)

    insured_daughter1_height_val1 = forms.CharField(required=False)
    insured_daughter1_height_val2 = forms.CharField(required=False)
    insured_daughter1_weight = forms.CharField(required=False)

    insured_daughter2_fname = forms.CharField(required=False)
    insured_daughter2_lname = forms.CharField(required=False)
    insured_daughter2_dob = forms.DateField(required=False)
    insured_daughter2_occupation = forms.CharField(required=False)

    insured_daughter2_height_val1 = forms.CharField(required=False)
    insured_daughter2_height_val2 = forms.CharField(required=False)
    insured_daughter2_weight = forms.CharField(required=False)

    insured_daughter3_fname = forms.CharField(required=False)
    insured_daughter3_lname = forms.CharField(required=False)
    insured_daughter3_dob = forms.DateField(required=False)
    insured_daughter3_occupation = forms.CharField(required=False)

    insured_daughter3_height_val1 = forms.CharField(required=False)
    insured_daughter3_height_val2 = forms.CharField(required=False)
    insured_daughter3_weight = forms.CharField(required=False)

    insured_daughter4_fname = forms.CharField(required=False)
    insured_daughter4_lname = forms.CharField(required=False)
    insured_daughter4_dob = forms.DateField(required=False)
    insured_daughter4_occupation = forms.CharField(required=False)

    insured_daughter4_height_val1 = forms.CharField(required=False)
    insured_daughter4_height_val2 = forms.CharField(required=False)
    insured_daughter4_weight = forms.CharField(required=False)

    insured_daughter5_fname = forms.CharField(required=False)
    insured_daughter5_lname = forms.CharField(required=False)
    insured_daughter5_dob = forms.DateField(required=False)
    insured_daughter5_occupation = forms.CharField(required=False)

    insured_daughter5_height_val1 = forms.CharField(required=False)
    insured_daughter5_height_val2 = forms.CharField(required=False)
    insured_daughter5_weight = forms.CharField(required=False)

    insured_daughter6_fname = forms.CharField(required=False)
    insured_daughter6_lname = forms.CharField(required=False)
    insured_daughter6_dob = forms.DateField(required=False)
    insured_daughter6_occupation = forms.CharField(required=False)

    insured_daughter6_height_val1 = forms.CharField(required=False)
    insured_daughter6_height_val2 = forms.CharField(required=False)
    insured_daughter6_weight = forms.CharField(required=False)

    insured_daughter7_fname = forms.CharField(required=False)
    insured_daughter7_lname = forms.CharField(required=False)
    insured_daughter7_dob = forms.DateField(required=False)
    insured_daughter7_occupation = forms.CharField(required=False)

    insured_daughter7_height_val1 = forms.CharField(required=False)
    insured_daughter7_height_val2 = forms.CharField(required=False)
    insured_daughter7_weight = forms.CharField(required=False)

    nominee_f_name = forms.CharField(required=False)
    nominee_l_name = forms.CharField(required=False)
    nominee_dob = forms.CharField(required=False)

    class Config:
        fields = [
            core_forms.Person(
                tag="proposal",
                first_name="proposer_f_name",
                last_name="proposer_l_name",
                mobile="proposer_mobile",
                email="proposer_email",
                landline="proposer_landline",
                birth_date="proposer_dob",
                gender="proposer_gender",
                maritial_status="proposer_maritial_status",
                father_name="proposer_father_name",
                occupation="proposer_occupation",
            ),
            core_forms.Person(
                tag="insured_spouse",
                first_name="insured_spouse_fname",
                last_name="insured_spouse_lname",
                birth_date="insured_spouse_dob"
            ),
            core_forms.HealthData(
                tag="insured_spouse",
                height_val1="insured_spouse_height_val1",
                height_val2="insured_spouse_height_val2",
                weight="insured_spouse_weight"
            ),

            core_forms.Person(
                tag="insured_father",
                first_name="insured_father_fname",
                last_name="insured_father_lname",
                birth_date="insured_father_dob"
            ),
            core_forms.HealthData(
                tag="insured_father",
                height_val1="insured_father_height_val1",
                height_val2="insured_father_height_val2",
                weight="insured_father_weight"
            ),

            core_forms.Person(
                tag="insured_mother",
                first_name="insured_mother_fname",
                last_name="insured_mother_lname",
                birth_data="insured_mother_dob"
            ),
            core_forms.HealthData(
                tag="insured_mother",
                height_val1="insured_mother_height_val1",
                height_val2="insured_mother_height_val2",
                weight="insured_mother_weight"
            ),

            core_forms.Person(
                tag="insured_son1",
                first_name="insured_son1_fname",
                last_name="insured_son1_lname",
                birth_date="insured_son1_dob"
            ),
            core_forms.HealthData(
                tag="insured_son1",
                height_val1="insured_son1_height_val1",
                height_val2="insured_son1_height_val2",
                weight="insured_son1_weight"
            ),

            core_forms.Person(
                tag="insured_son2",
                first_name="insured_son2_fname",
                last_name="insured_son2_lname",
                birth_date="insured_son2_dob"
            ),
            core_forms.HealthData(
                tag="insured_son2",
                height_val1="insured_son2_height_val1",
                eight_val2="insured_son2_height_val2",
                weight="insured_son2_weight"
            ),

            core_forms.Person(
                tag="insured_son3",
                first_name="insured_son3_fname",
                last_name="insured_son3_lname",
                birth_date="insured_son3_dob"
            ),
            core_forms.HealthData(
                tag="insured_son3",
                height_val1="insured_son3_height_val1",
                height_val2="insured_son3_height_val2",
                weight="insured_son3_weight"
            ),

            core_forms.Person(
                tag="insured_son4",
                first_name="insured_son4_fname",
                last_name="insured_son4_lname",
                birth_date="insured_son4_dob"
            ),
            core_forms.HealthData(
                tag="insured_son4",
                height_val1="insured_son4_height_val1",
                height_val2="insured_son4_height_val2",
                weight="insured_son4_weight"
            ),

            core_forms.Person(
                tag="insured_son5",
                first_name="insured_son5_fname",
                last_name="insured_son5_lname",
                birth_date="insured_son5_dob"
            ),
            core_forms.HealthData(
                tag="insured_son5",
                height_val1="insured_son5_height_val1",
                height_val2="insured_son5_height_val2",
                weight="insured_son5_weight"
            ),

            core_forms.Person(
                tag="insured_son6",
                first_name="insured_son6_fname",
                last_name="insured_son6_lname",
                birth_date="insured_son6_dob"
            ),
            core_forms.HealthData(
                tag="insured_son6",
                height_val1="insured_son6_height_val1",
                height_val2="insured_son6_height_val2",
                weight="insured_son6_weight"
            ),

            core_forms.Person(
                tag="insured_son7",
                first_name="insured_son7_fname",
                last_name="insured_son7_lname",
                birth_date="insured_son7_dob"
            ),
            core_forms.HealthData(
                tag="insured_son7",
                height_val1="insured_son7_height_val1",
                height_val2="insured_son7_height_val2",
                weight="insured_son7_weight"
            ),

            core_forms.Person(
                tag="insured_daughter1",
                first_name="insured_daughter1_fname",
                last_name="insured_daughter1_lname",
                birth_date="insured_daughter1_dob"
            ),
            core_forms.HealthData(
                tag="insured_daughter1",
                height_val1="insured_daughter1_height_val1",
                height_val2="insured_daughter1_height_val2",
                weight="insured_daughter1_weight"
            ),

            core_forms.Person(
                tag="insured_daughter2",
                first_name="insured_daughter2_fname",
                last_name="insured_daughter2_lname",
                birth_date="insured_daughter2_dob"
            ),
            core_forms.HealthData(
                tag="insured_daughter2",
                height_val1="insured_daughter2_height_val1",
                height_val2="insured_daughter2_height_val2",
                weight="insured_daughter2_weight"
            ),

            core_forms.Person(
                tag="insured_daughter3",
                first_name="insured_daughter3_fname",
                last_name="insured_daughter3_lname",
                birth_date="insured_daughter3_dob"
            ),
            core_forms.HealthData(
                tag="insured_daughter3",
                height_val1="insured_daughter3_height_val1",
                height_val2="insured_daughter3_height_val2",
                weight="insured_daughter3_weight"
            ),

            core_forms.Person(
                tag="insured_daughter4",
                first_name="insured_daughter4_fname",
                last_name="insured_daughter4_lname",
                birth_date="insured_daughter4_dob"
            ),
            core_forms.HealthData(
                tag="insured_daughter4",
                height_val1="insured_daughter4_height_val1",
                height_val2="insured_daughter4_height_val2",
                weight="insured_daughter4_weight"
            ),

            core_forms.Person(
                tag="insured_daughter5",
                first_name="insured_daughter5_fname",
                last_name="insured_daughter5_lname",
                birth_date="insured_daughter5_dob"
            ),
            core_forms.HealthData(
                tag="insured_daughter5",
                height_val1="insured_daughter5_height_val1",
                height_val2="insured_daughter5_height_val2",
                weight="insured_daughter5_weight"
            ),


            core_forms.Person(
                tag="insured_daughter6",
                first_name="insured_daughter6_fname",
                last_name="insured_daughter6_lname",
                birth_date="insured_daughter6_dob"
            ),
            core_forms.HealthData(
                tag="insured_daughter6",
                height_val1="insured_daughter6_height_val1",
                height_val2="insured_daughter6_height_val2",
                weight="insured_daughter6_weight"
            ),

            core_forms.Person(
                tag="insured_daughter7",
                first_name="insured_daughter7_fname",
                last_name="insured_daughter7_lname",
                birth_date="insured_daughter7_dob"
            ),
            core_forms.HealthData(
                tag="insured_daughter7",
                height_val1="insured_daughter7_height_val1",
                height_val2="insured_daughter7_height_val2",
                weight="insured_daughter7_weight"
            ),

            core_forms.Address,
            core_forms.Person(
                tag="nominee",
                first_name="nominee_f_name",
                last_name="nominee_l_name",
                birth_date="nominee_dob"
            ),
        ]


class HealthQuoteFDForm(core_forms.FDForm):
    product_type = forms.CharField(required=False)
    gender = forms.CharField(required=False)
    mobile = forms.CharField(required=False)
    email = forms.CharField(required=False)
    self_insured = forms.CharField(required=False)
    self_age = forms.CharField(required=False)
    spouse_insured = forms.CharField(required=False)
    spouse_age = forms.CharField(required=False)
    father_insured = forms.CharField(required=False)
    father_age = forms.CharField(required=False)
    mother_insured = forms.CharField(required=False)
    mother_age = forms.CharField(required=False)
    son1_insured = forms.CharField(required=False)
    son2_insured = forms.CharField(required=False)
    son3_insured = forms.CharField(required=False)
    son4_insured = forms.CharField(required=False)
    son5_insured = forms.CharField(required=False)
    son6_insured = forms.CharField(required=False)
    son7_insured = forms.CharField(required=False)
    son1_age = forms.CharField(required=False)
    son1_age = forms.CharField(required=False)
    son1_age_month = forms.CharField(required=False)
    son2_age = forms.CharField(required=False)
    son2_age_month = forms.CharField(required=False)
    son3_age = forms.CharField(required=False)
    son3_age_month = forms.CharField(required=False)
    son4_age = forms.CharField(required=False)
    son4_age_month = forms.CharField(required=False)
    son5_age = forms.CharField(required=False)
    son5_age_month = forms.CharField(required=False)
    son6_age = forms.CharField(required=False)
    son6_age_month = forms.CharField(required=False)
    son7_age = forms.CharField(required=False)
    son7_age_month = forms.CharField(required=False)
    daughter1_insured = forms.CharField(required=False)
    daughter2_insured = forms.CharField(required=False)
    daughter3_insured = forms.CharField(required=False)
    daughter4_insured = forms.CharField(required=False)
    daughter5_insured = forms.CharField(required=False)
    daughter6_insured = forms.CharField(required=False)
    daughter7_insured = forms.CharField(required=False)
    daughter1_age = forms.CharField(required=False)
    daughter1_age_month = forms.CharField(required=False)
    daughter1_age = forms.CharField(required=False)
    daughter2_age = forms.CharField(required=False)
    daughter2_age_month = forms.CharField(required=False)
    daughter3_age = forms.CharField(required=False)
    daughter3_age_month = forms.CharField(required=False)
    daughter4_age = forms.CharField(required=False)
    daughter4_age_month = forms.CharField(required=False)
    daughter5_age = forms.CharField(required=False)
    daughter5_age_month = forms.CharField(required=False)
    daughter6_age = forms.CharField(required=False)
    daughter6_age_month = forms.CharField(required=False)
    daughter7_age = forms.CharField(required=False)
    daughter7_age_month = forms.CharField(required=False)
    sum_assured = forms.CharField(required=False)
    pincode = forms.CharField(required=False)
    parents_pincode = forms.CharField(required=False)

    class Config:
        fields = [
            core_forms.Person,
            core_forms.HealthQuoteForm
        ]
