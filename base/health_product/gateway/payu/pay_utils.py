import logging
import re
import time
import datetime
import os
import random
import json
import hashlib
import hashlib
import string
from django.conf import settings
from django.utils.functional import SimpleLazyObject
from django.contrib.contenttypes.models import ContentType
from tagging.models import Tag
from django.db.models import Q
from collections import OrderedDict
import math
from django.http import HttpResponse, HttpResponseRedirect, HttpRequest

from core.models import User
import urllib
import urllib2
import hashlib
from hashlib import sha512

from health_product.gateway.payu import settings as payu_settings


KEYS = ('key', 'txnid', 'amount', 'productinfo', 'firstname', 'email',
        'udf1', 'udf2', 'udf3', 'udf4', 'udf5',  'udf6',  'udf7', 'udf8',
        'udf9',  'udf10')


def generate_hash(data):
    send_data_list = []
    for key in KEYS:
        send_data_list.append(str(data.get(key, '')))
    send_data_list.append(payu_settings.MERCHANT_SALT)
    send_data = ("|".join(send_data_list))
    hash = sha512(send_data)
    return hash.hexdigest()


def verify_hash(data):
    check_keys = ['key', 'txnid', 'amount', 'productinfo', 'firstname', 'email',
                  'udf1', 'udf2', 'udf3', 'udf4', 'udf5',  'udf6',  'udf7', 'udf8',
                  'udf9',  'udf10']
    check_keys.reverse()
    data_list = []
    data_list.append(payu_settings.MERCHANT_SALT)
    data_list.append(str(data.get('status', '')))
    for key in check_keys:
        data_list.append(str(data.get(key, '')))
    data_to_hash = ('|'.join(data_list))
    hash = sha512(data_to_hash)
    return (hash.hexdigest().lower() == data.get('hash'))
