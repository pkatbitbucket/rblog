from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^checkout/(?P<transaction_id>\w+)/$', views.checkout, name='health_plan_payu_checkout'),
    url(r'^success/$', views.success, name='health_plan_payu_success'),
    url(r'^failure/$', views.failure, name='health_plan_payu_failure'),
    url(r'^cancel/$', views.cancel, name='health_plan_payu_cancel'),
]
