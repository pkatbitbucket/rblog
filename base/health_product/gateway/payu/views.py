import datetime
import hashlib
import logging

from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.conf import settings
from django.core.urlresolvers import reverse

from django.shortcuts import redirect, render, render_to_response, get_object_or_404
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.core import serializers
from django.forms.models import modelformset_factory
from django.forms.formsets import formset_factory
from django.contrib.auth import authenticate, login
from django.middleware import csrf
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.forms.formsets import formset_factory

from collections import OrderedDict
import json
import urllib2
import urllib
from uuid import uuid4
from random import randint
from core.models import User
import utils
from health_product.models import *
from health_product.gateway.payu import settings as payu_settings
from health_product.gateway.payu import pay_utils


@never_cache
def checkout(request, transaction_id):
    transaction = Transaction.objects.get(transaction_id=transaction_id)
    processed_data = transaction.extra['processed_data']
    processed_data['key'] = payu_settings.MERCHANT_KEY
    processed_data.update({
        'surl': request.build_absolute_uri(reverse('health_plan_payu_success')),
        'furl': request.build_absolute_uri(reverse('health_plan_payu_failure')),
        'curl': request.build_absolute_uri(reverse('health_plan_payu_cancel')),
        'hash': pay_utils.generate_hash(processed_data)
    })
    data = urllib.urlencode(processed_data)
    req = urllib2.Request("%s?type=merchant_txn" %
                          (payu_settings.PAYMENT_URL), data)
    resp = urllib2.urlopen(req)
    payu_transaction_id = resp.read()
    transaction.extra['PAYU_TRANSACTION_ID'] = payu_transaction_id
    transaction.save()
    src_url = '%s_merchantCheckoutPage?txtid=%s&key=%s' % (
        payu_settings.PRODUCTION_URL, payu_transaction_id, payu_settings.MERCHANT_KEY)
    return render_to_response('payment/payu_iframe.html', {
        'transaction_id': payu_transaction_id,
        'merchant_key': payu_settings.MERCHANT_KEY,
        'src_url': src_url,
        'transaction': transaction
    }, context_instance=RequestContext(request)
    )


@csrf_exempt
def success(request):
    if request.method == 'POST':
        payment_response = {}
        transaction = Transaction.objects.get(
            reference_id=request.POST['txnid'])
        for k in request.POST.keys():
            payment_response[k] = request.POST.get(k, '')
        transaction.extra['payment_response'] = payment_response
        transaction.reference_id = 'CFOX-%s-%s' % (
            transaction.slab.health_product.insurer.id, transaction.id)
        transaction.save()
        if not pay_utils.verify_hash(request.POST):
            print "Response data for order (txnid: %s) has been tampered. Confirm payment with PayU." % request.POST.get('txnid')
            return redirect('health_plan_payu_failure')
        else:
            print "Payment for order (txnid: %s) succeeded at PayU" % request.POST.get('txnid')
            return HttpResponseRedirect('/health-plan/payment/transaction/%s/success/' % transaction.transaction_id)
    return HttpResponseRedirect('/health-plan/payment/failure/')


@csrf_exempt
def failure(request):
    if request.method == 'POST':
        payment_response = {}
        transaction = Transaction.objects.get(
            reference_id=request.POST['txnid'])
        for k in request.POST.keys():
            payment_response[k] = request.POST.get(k, '')
        transaction.extra['payment_response'] = payment_response
        transaction.save()
        return render(request, 'payment/failure.html')
    else:
        # return HttpResponse('Data Tampered')
        return render(request, 'payment/failure.html')


@csrf_exempt
def cancel(request):
    if request.method == 'POST':
        payment_response = {}
        transaction = Transaction.objects.get(
            reference_id=request.POST['txnid'])
        for k in request.POST.keys():
            payment_response[k] = request.POST.get(k, '')
        transaction.extra['payment_response'] = payment_response
        transaction.save()
        return render(request, 'payment/cancel.html')
    else:
        return HttpResponse('Transaction Cancelled')
