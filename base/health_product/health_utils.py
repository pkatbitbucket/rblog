from collections import OrderedDict
import locale
from django.conf import settings
import re
import zlib
import json


class GetSumAssured():
    segment_dict = OrderedDict({
        tuple(range(18, 31)): OrderedDict({'base_cover': 100000, 'S': 50000, 'S_DP': 50000, 'M': 100000,
                                           'M_DP': 150000, 'M_DC': 200000, 'M_DP_DC': 300000, 'SHARED': 0,
                                           'PVT': 50000, 'PVT_HIGH_END': 100000, 'METRO': 50000}),

        tuple(range(31, 41)): OrderedDict({'base_cover': 100000, 'S': 50000, 'S_DP': 100000, 'M': 100000,
                                           'M_DP': 150000, 'M_DC': 200000, 'M_DP_DC': 300000, 'SHARED': 0,
                                           'PVT': 50000, 'PVT_HIGH_END': 100000, 'METRO': 50000}),

        tuple(range(41, 46)): OrderedDict({'base_cover': 150000, 'S': 50000, 'S_DP': 100000, 'M': 150000,
                                           'M_DP': 200000, 'M_DC': 200000, 'M_DP_DC': 300000, 'SHARED': 0,
                                           'PVT': 100000, 'PVT_HIGH_END': 150000, 'METRO': 50000}),

        tuple(range(46, 51)): OrderedDict({'base_cover': 200000, 'S': 50000, 'S_DP': 150000, 'M': 150000,
                                           'M_DP': 200000, 'M_DC': 250000, 'M_DP_DC': 350000, 'SHARED': 0,
                                           'PVT': 100000, 'PVT_HIGH_END': 150000, 'METRO': 50000}),

        tuple(range(51, 56)): OrderedDict({'base_cover': 250000, 'S': 50000, 'S_DP': 150000, 'M': 200000,
                                           'M_DP': 200000, 'M_DC': 250000, 'M_DP_DC': 350000, 'SHARED': 0,
                                           'PVT': 100000, 'PVT_HIGH_END': 150000, 'METRO': 50000}),

        tuple(range(56, 61)): OrderedDict({'base_cover': 250000, 'S': 50000, 'S_DP': 200000, 'M': 200000,
                                           'M_DP': 250000, 'M_DC': 300000, 'M_DP_DC': 500000, 'SHARED': 0,
                                           'PVT': 100000, 'PVT_HIGH_END': 150000, 'METRO': 50000}),

        tuple(range(61, 66)): OrderedDict({'base_cover': 300000, 'S': 50000, 'S_DP': 200000, 'M': 200000,
                                           'M_DP': 250000, 'M_DC': 300000, 'M_DP_DC': 500000, 'SHARED': 0,
                                           'PVT': 100000, 'PVT_HIGH_END': 150000, 'METRO': 50000}),

        tuple(range(66, 71)): OrderedDict({'base_cover': 350000, 'S': 50000, 'S_DP': 200000, 'M': 250000,
                                           'M_DP': 300000, 'M_DC': 350000, 'M_DP_DC': 500000, 'SHARED': 0,
                                           'PVT': 100000, 'PVT_HIGH_END': 150000, 'METRO': 50000}),

        tuple(range(71, 120)): OrderedDict({'base_cover': 400000, 'S': 50000, 'S_DP': 250000, 'M': 250000,
                                            'M_DP': 300000, 'M_DC': 350000, 'M_DP_DC': 500000, 'SHARED': 0,
                                            'PVT': 100000, 'PVT_HIGH_END': 150000, 'METRO': 50000}),
    })

    """
    Data in the following format:
    data = {
        'sum_assured' : 200000,
        'age': '28',
        'city' : 'M_MUMBAI',
        'service_type' : 'SHARED',
        'family' : True,
        'adults' : 2,
        'kids' : 2,
    }
    """

    def __init__(self, data):
        self.sum_assured = 0
        self.filter_list = []
        self.segment = []
        self.sa_dict = {}
        self.data = data
        self.dependency = 'S'

    def get_segment(self):
        if 'age' not in self.data.keys():
            return 0

        for k, v in self.segment_dict.items():
            if int(self.data['age']) in k:
                self.segment = k
                self.sa_dict = v
                self.sum_assured = v['base_cover']
        return self.sum_assured

    def get_sa_by_city(self):
        if self.data['city']['type'] == 'METRO':
            self.filter_list.append('METRO')
        return

    def get_sa_by_family(self):
        if self.data['family']:
            self.dependency = 'M'

        if 'adults' in self.data.keys():
            if self.data['adults']:
                self.dependency = '%s_DP' % self.dependency

        if 'kids' in self.data.keys():
            if self.data['kids']:
                self.dependency = '%s_DC' % self.dependency

        self.filter_list.append(self.dependency)
        return

    def get_sa_by_service_type(self):
        if self.data['service_type']:
            self.filter_list.append(self.data['service_type'])
        return

    def get_sa(self):
        seg = self.get_segment()
        if not seg:
            return 0

        for k, v in self.data.items():
            func = getattr(self, "get_sa_by_%s" % k.lower(), None)
            if func:
                func()

        for i in self.filter_list:
            self.sum_assured += self.sa_dict[i]

        self.sum_assured = int(
            100000 * round(float(self.sum_assured) / 100000))
        return self.sum_assured

def normalize_hospital_name(name):
    name = name.lower()
    # name = re.sub(r'\([ ]*p[ ]*\)', "", name)
    name = re.sub(r'\(.*\)', "", name)
    name = name.replace(".", " ")
    name = name.replace("'s", "").replace("`s", "")
    name = name.replace("&", "")
    name = name.replace(" and", " ")
    name = name.replace("doctor ", " ")
    name = re.sub(r'^dr ', '', name)
    name = re.sub(r' dr ', '', name)
    name = name.replace("pvt", "").replace("private", "")
    name = name.replace("ltd", "").replace("limited", "")
    name = name.replace("hospital", "").replace("hosp", "")
    name = name.replace(" clinic ", "").replace(" polyclinic ", "")
    name = name.replace("nursing", "")
    name = name.replace(" home", "")
    name = name.replace("health", "")
    name = name.replace(" care", "")
    # name = name.replace("general","")
    # name = name.replace("maternity","")
    # name = name.replace(" eye","")
    # name = name.replace(" heart","")
    # name = name.replace(" dental","")
    # name = name.replace("surgery","").replace("surgical","")
    name = name.replace("center", "").replace("centre", "")
    name = name.replace("  ", " ").replace("   ", " ").replace("    ", " ")
    name = ' '.join(name.split())
    return name


def incl_range(x, y, jump, round_to=2):
    res = []
    while x <= y:
        res.append(x)
        x = round(x + jump, round_to)
    return res


def currency_format(value):
    locale.setlocale(locale.LC_ALL, settings.LC_LOCALE)
    return locale.format("%d", value, grouping=True)

def sortkeypicker(keynames):
    negate = set()
    for i, k in enumerate(keynames):
        if k[:1] == '-':
            keynames[i] = k[1:]
            negate.add(k[1:])

    def getit(adict):
        composite = [adict[k] for k in keynames]
        for i, (k, v) in enumerate(zip(keynames, composite)):
            if k in negate:
                composite[i] = -v
        return composite
    return getit


def zip_json(json_data):
    data = json.dumps(json_data)
    compressed_data = list(zlib.compress(data))
    zero_padded_ascii = lambda chr: '%03i' % ord(chr)
    compressed_data[0] = zero_padded_ascii(compressed_data[0])
    compressed_data = reduce(
        (lambda a, b: a + zero_padded_ascii(b)), compressed_data)
    return compressed_data








