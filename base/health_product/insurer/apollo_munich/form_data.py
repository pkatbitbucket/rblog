from wiki.models import Pincode, City, Slab
from dateutil.relativedelta import datetime, relativedelta
from health_product.insurer import custom_utils
from health_product.insurer.apollo_munich import preset
from collections import OrderedDict
from copy import deepcopy
import putils
from health_product.services.match_plan import MatchPlan


def get_premium_details(data, slab_id):
    discount = {
        1: 0,
        2: 7.5,
    }
    base_premiums = {}
    discounted_premiums = {}
    cpt_ids = {}

    slab = Slab.objects.get(id=slab_id)

    for i in range(1, 3):
        members = data['members']
        for m in members:
            m['age'] = m['age'] + (i - 1)
        data_to_score = putils.prepare_post_data_to_score(data)
        mp = MatchPlan(data_to_score, slab=slab, request=None)
        cpt = mp.get_cpt()
        base_premiums[i] = cpt.premium + base_premiums.get(i - 1, 0)
        discounted_premiums[i] = round(base_premiums[i] * (1 - discount[i] / 100))
        cpt_ids[i] = cpt.id
    return {
        'base_premiums': base_premiums,
        'discounted_premiums': discounted_premiums,
        'cpt_ids': cpt_ids,
    }


class Formic():

    POLICY_TERMS = [
        {'id': 1, 'name': '1 year'},
        {'id': 2, 'name': '2 years'},
    ]

    POLICY_DISCOUNTS = [
        {'id': 1, 'name': '0'},
        {'id': 2, 'name': '7.5'},
    ]

    def __init__(self, transaction, is_self_present):
        self.transaction = transaction
        self.is_self_present = is_self_present
        self.proposer_data = {
            'pincode': transaction.extra['pincode'],
            'city': City.objects.get(id=transaction.extra['city']['id']).name,
            'state': City.objects.get(id=transaction.extra['city']['id']).state.name,
            'email': ''
        }

    def add_parent_segment(self, segment_id, tag_id, details, question_type):
        try:
            saved_data = self.transaction.insured_details_json[
                'form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id,
                       'tag': tag_id, 'details': details, 'data': []}
        row = []
        if question_type == 'MultipleChoiceField':
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='MultipleChoiceField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    details=member,
                    value='No',
                    label=details.get('question_text', ''),
                    span=4,
                    css_class='bottom-margin',
                    event=', click : $parent.root_context.stage_5.any_yes_check',
                    template_option='question'
                ))
            new_segment['data'].append(row)

        elif question_type == 'DropDownField':
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='DropDownField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    options=preset.ALL_PRE_EXISTING_OPTIONS,
                    details=member,
                    label=details.get('question_text', ''),
                    span=2,
                    template_option='question',
                    required=True,
                    only_if='root_context.stage_5.check_dict.get(\'%s\')' % (member[
                        'id']),
                ))
            new_segment['data'].append(row)

        elif question_type in ['DateField', 'TextField']:
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type=question_type,
                    code='%s_question_%s' % (member['id'], tag_id),
                    details=member,
                    label=details.get('question_text', ''),
                    span=2,
                    template_option='question',
                    required=True,
                    only_if='root_context.stage_5.check_dict.get(\'%s\')' % (member[
                        'id']),
                ))
            new_segment['data'].append(row)
        return new_segment

    def stage_1(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'personal'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': {}, 'data': []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            label='First Name',
            code='first_name',
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            label='Last Name',
            code='last_name',
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Gender',
            code='gender',
            value=1 if self.transaction.extra['gender'] == 'MALE' else 2,
            options=preset.GENDER_OPTIONS
        ))
        new_segment['data'].append(row)
        row = []

        married = True if [m for m in self.transaction.extra['members'] if m['id'] == 'spouse'] else False
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Marital Status',
            code='marital_status',
            options=preset.MARITAL_STATUS_OPTIONS,
            value=1 if married or len(self.transaction.extra['members']) > 1 else None,
            template_option='static' if married else 'dynamic',
        ))
        if self.is_self_present:
            member = [m for m in self.transaction.extra['members'] if m['id'] == 'self'][0]
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                default_age=member['age'],
                min_age=18,
                max_age=100,
            ))
        else:
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                default_age=18,
                min_age=18,
                max_age=100,
            ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            label='Does your or any of the person proposed under this  policy\'s occupation or nature of job involves '
                  'working in mines, explosive units or chemical industry; handling of heavy machinery or hazardous '
                  'materials; driving of heavy motor vehicles or significant manual labor?',
            code='occupation',
            value='No',
            span=12
        ))
        new_segment['data'].append(row)
        premium_data = get_premium_details(deepcopy(self.transaction.extra), self.transaction.slab_id)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='PremiumDiscountField',
            code='policy_term',
            span=8,
            options1=self.POLICY_TERMS,
            options2=self.POLICY_DISCOUNTS,
            base_premiums=premium_data['base_premiums'],
            discounted_premiums=premium_data['discounted_premiums'],
            required=True,
            main_label='Policy term',
            dep_label='Discount',
            dep_label1='Premium',
            premium=self.transaction.extra['plan_data']['premium'],
        ))
        new_segment['data'].append(row)

        stage.append(new_segment)
        return {'template': 'stage_1', 'data': stage}

    def stage_2(self):
        stage = []
        feet_range = [{'id': feet, 'name': '%s ft' % feet} for feet in range(0, 9)]
        inch_range = [{'id': inch, 'name': '%s inches' % inch} for inch in range(0, 12)]
        inch_range[1]['name'] = '1 inch'

        for member in self.transaction.extra['members']:
            segment_id = 'insured'
            tag_id = member['id']
            template_switch = 'dynamic'
            if tag_id == 'self':
                template_switch = 'static'
            try:
                saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment': segment_id, 'tag': tag_id, 'details': member, 'data': []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                label='First Name',
                code='first_name',
                max_length=50,
                template_option=template_switch,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                label='Last Name',
                code='last_name',
                template_option=template_switch,
                max_length=50,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                template_option=template_switch,
                value=member.get('dob', ''),
                span=3,
                default_date=datetime.datetime.strftime(datetime.datetime.today() - relativedelta(years=member['age']), '%d/%m/%Y'),
                default_age=member['age'],
                min_age=18 if member['type'] == 'adult' else 0,
                max_age=100,
            ))
            new_segment['data'].append(row)

            row = []
            if member['person'].lower() in ['father', 'mother', 'self', 'spouse']:
                marital_template_option = 'static' if member['person'].lower() in ['self', 'spouse'] else 'dynamic'
                marital_value = 1 if member['person'].lower() == 'spouse' else None
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='DropDownField',
                    label='Marital Status',
                    code='marital_status',
                    span=3,
                    options=preset.MARITAL_STATUS_OPTIONS,
                    template_option=marital_template_option,
                    value=marital_value,
                ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='TwoDropDownField',
                label='Height',
                code='height',
                span=4,
                span1=6,
                span2=6,
                options1=feet_range,
                options2=inch_range
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='IntegerField',
                label='Weight (kgs)',
                code='weight',
                span=2
            ))
            new_segment['data'].append(row)
            stage.append(new_segment)

        return {'template': 'stage_2', 'data': stage}

    def stage_3(self):
        default_relation = {
            'self': '14' if self.transaction.extra['gender'] == 'MALE' else '15',
            'spouse': '15' if self.transaction.extra['gender'] == 'MALE' else '14',
            'son': '4' if self.transaction.extra['gender'] == 'MALE' else '5',
            'daughter': '4' if self.transaction.extra['gender'] == 'MALE' else '5',
            'father': '20' if self.transaction.extra['gender'] == 'MALE' else '17',
            'mother': '20' if self.transaction.extra['gender'] == 'MALE' else '17'
        }
        self_relation_probables = OrderedDict([
            ('mother', '5'),
            ('father', '4'),
            ('spouse', '14' if self.transaction.extra['gender'] == 'MALE' else '15')
        ])
        # Needs to run before the big loop on members
        for member in self.transaction.extra['members']:
            if member['person'] in self_relation_probables.keys():
                self.transaction.extra['self_nominee'] = member['id']
                self.transaction.save()
                default_relation['self'] = self_relation_probables[member['person']]

        stage = []
        segment_id = 'nominee'
        tag_id = 'self'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': {'verbose': 'Insured'}, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            code='relation',
            # label='Relationship With %s' % member['verbose'],
            label='Relationship With Proposer',
            options=preset.RELATIONSHIP_OPTIONS,
            span=3
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            code='first_name',
            label='First Name',
            max_length=50,
            span=3
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            code='last_name',
            label='Last Name',
            max_length=50,
            span=3
        ))
        new_segment['data'].append(row)
        # row = []
        # row.append(custom_utils.new_field(
        #     saved_data=saved_data,
        #     field_type='TextField',
        #     code='address',
        #     label='Nominee Address',
        #     span=6
        # ))
        # new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template': 'stage_3', 'data': stage}

    def stage_4(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'address'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': {}, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address1',
            label='Flat/House No, Street',
            span=12,
            css_class='bottom-margin',
            max_length=30
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address2',
            label='Area/Locality',
            span=12,
            css_class='bottom-margin',
            max_length=30,
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='landmark',
            label='Landmark',
            span=12,
            css_class='bottom-margin',
            max_length=30,
            required=False
        ))
        new_segment['data'].append(row)

        if self.transaction.slab.health_product.coverage_category == 'SUPER_TOPUP':
            row = []
            pincode = Pincode.objects.filter(pincode=self.transaction.extra['pincode'])[0]
            state = pincode.state
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='IntegerField',
                code='pincode',
                label='Pincode',
                span=4,
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                code='state',
                label='State',
                span=4,
                template_option='static'
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='SearchableDropDownField',
                code='city',
                label='City',
            ))
            new_segment['data'].append(row)
        else:
            row = []
            pincode = Pincode.objects.filter(pincode=self.transaction.extra['pincode'])[0]
            state = pincode.state
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='SearchableDropDownField',
                code='city',
                label='City',
                options=preset.CITY_DICT[state.id],
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                code='state',
                label='State',
                value=self.proposer_data['state'],
                span=4,
                template_option='static'
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='IntegerField',
                code='pincode',
                label='Pincode',
                value=str(self.proposer_data['pincode']),
                span=4,
                template_option='static'
            ))
            new_segment['data'].append(row)

        stage.append(new_segment)

        tag_id = 'contact'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': {}, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='email',
            label='Email',
            span=12,
            css_class='bottom-margin',
            email=True
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MobileField',
            code='mobile',
            label='Mobile',
            css_class='bottom-margin',
            span=12
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='LandlineField',
            code='landline',
            label='Landline',
            span=12,
            css_class='bottom-margin',
            required=False
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template': 'stage_4', 'data': stage}

    def stage_5(self):
        stage = []
        question_list = ['cardiac_disorder', 'lung_disorder', 'digestive_disorder',
                         'kidney_disorder', 'nervous_disorder', 'endocrine_disorder',
                         'ulcer_disorder', 'arthritis_disorder', 'dioptres_disorder',
                         'aids_disorder', 'anaemia_disorder', 'mental_disorder',
                         'breast_disorder', 'narcotic_disorder', 'regular_medication',
                         'blood_tests', 'surgery', 'fever', 'pregnant', 'diabetes'
                         ]
        # ---------- Question Unit Begins ---------
        question_code = 'cardiac_disorder'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'cardiac_disorder'
        details = {
            'conditional': 'NO',
            'question_text': 'High or low blood pressure, Chest Pain, or any other cardiac disorder?',
            'trigger_any_yes_check': True,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'lung_disorder'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'lung_disorder'
        details = {
            'conditional': 'NO',
            'question_text': 'Tuberculosis, Asthma, Bronchitis or any other lung/respiratory disorder?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'digestive_disorder'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'digestive_disorder'
        details = {
            'conditional': 'NO',
            'question_text': 'Ulcer(Stomach/Duodenal), Liver or gall bladder disorder or any other digestive tract disorder?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'kidney_disorder'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'kidney_disorder'
        details = {
            'conditional': 'NO',
            'question_text': 'Kidney Failure, Stone in kidney or urinary tract, Prostate disorder or any other kidney/urinary tract disorder?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'nervous_disorder'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'nervous_disorder'
        details = {
            'conditional': 'NO',
            'question_text': 'Stroke, Epilepsy (fits), Paralysis or any other nervous system (Brain, Spinal cord, etc) disorder?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'endocrine_disorder'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'endocrine_disorder'
        details = {
            'conditional': 'NO',
            'question_text': 'Diabetes, Impaired glucose tolerance (Pre-diabetes), Thyroid/Pituitary Disorder or any other endocrine disorder?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'ulcer_disorder'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'ulcer_disorder'
        details = {
            'conditional': 'NO',
            'question_text': 'Tumor (Swelling)-benign or malignant, any external ulcer/growth/cyst/mass anywhere in body?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'arthritis_disorder'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'arthritis_disorder'
        details = {
            'conditional': 'NO',
            'question_text': 'Arthritis, Spondylosis or any other disorder of the muscle/bone/joint?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'dioptres_disorder'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'dioptres_disorder'
        details = {
            'conditional': 'NO',
            'question_text': 'Diseases of the Ear/Nose/Throat/Teeth/ Eye (please mention Dioptres in case of refractory error)?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'aids_disorder'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'aids_disorder'
        details = {
            'conditional': 'NO',
            'question_text': 'HIV/AIDS or sexually transmitted diseases or any immune system disorder?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'anaemia_disorder'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'anaemia_disorder'
        details = {
            'conditional': 'NO',
            'question_text': 'Anaemia, Leukaemia, Lymphoma or any other blood/lymphatic system disorder?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'mental_disorder'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'mental_disorder'
        details = {
            'conditional': 'NO',
            'question_text': 'Psychiatric/Mental illnesses or Sleep disorder?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'breast_disorder'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'breast_disorder'
        details = {
            'conditional': 'NO',
            'question_text': 'Uterine Fibroid, Fibroadenoma breast or any other Gynaecological (Female reproductive system)/Breast disorder?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'narcotic_disorder'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'narcotic_disorder'
        details = {
            'conditional': 'NO',
            'question_text': 'Been addicted to alcohol, narcotics, habit forming drugs or been under detoxication therapy?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'regular_medication'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'regular_medication'
        details = {
            'conditional': 'NO',
            'question_text': 'Been under any regular medication (self/ prescribed)?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'blood_tests'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'blood_tests'
        details = {
            'conditional': 'NO',
            'question_text': 'Undertaken any lab/blood tests, imaging tests viz. scans/MRI in the last 5 years other than routine health check-up or pre-employment check-up?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'surgery'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'surgery'
        details = {
            'conditional': 'NO',
            'question_text': 'Undertaken any surgery or a surgery been advised and have surgery still pending?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'fever'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'fever'
        details = {
            'conditional': 'NO',
            'question_text': 'Suffered from any other disease/illness/accident/injury other than common cold or viral fever?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'pregnant'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'pregnant'
        details = {
            'conditional': 'NO',
            'question_text': 'Is any of the insured persons pregnant? If yes, please mention the expected date of delivery.',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'diabetes'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'diabetes'
        details = {
            'conditional': 'NO',
            'question_text': 'Any complaint of diabetes, hypertension or any complication during current or earlier pregnancy?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'illness_details'
        segment_id = 'conditional_%s' % question_code
        tag_id = 'illness_details'
        details = {
            'conditional': 'YES',
            'parent_question_code': question_list,
            'question_text': 'Name and details of Illness / Medicine / Test / Surgery / Diopter grade (for questions answered as Yes. Please specify Diagnosis, Date of Diagnosis, Treatment Details)',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'TextField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'smoke_details'
        segment_id = 'conditional_%s' % question_code
        tag_id = 'smoke_details'
        details = {
            'conditional': 'YES',
            'parent_question_code': question_list,
            'question_text': 'Does any person proposed to be insured smoke or consume gutkha/ pan masala or alcohol. If yes, please indicate the name and quantity per week:',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'TextField'))
        # ---------- Question Unit Ends -----------
        # ---------- Question Unit Begins ---------
        question_code = 'application_declined'
        segment_id = 'conditional_%s' % question_code
        tag_id = 'application_declined'
        details = {
            'conditional': 'YES',
            'parent_question_code': question_list,
            'question_text': 'Has any application for life, health, hospital daily cash or critical illness insurance ever been declined, postponed, loaded or been made subject to any special conditions by any insurance company?',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        # ---------- Question Unit Ends -----------

        return {'template': 'stage_5', 'data': stage}

    def stage_6(self):
        stage = []
        segment_id = 'previous_policy'
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}

        try:
            existing_policies_data = self.transaction.insured_details_json['form_data']['existing_policies']
        except KeyError:
            existing_policies_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': {'existing_policies_data': existing_policies_data}, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='existing_insurance',
            label='Is the proposer or any of the persons proposed, already insured under or proposed for a health '
                  'insurance policy for inpatient hospitalization with Apollo Munich Health or any other '
                  'insurance company? ',
            helptext='If yes, please indicate below the policy/Application number(s) (Please mention application number in case of pending proposal)',
            span=12,
            css_class='bottom-margin',
            template_option='question'
        ))
        new_segment['data'].append(row)

        stage.append(new_segment)
        return {'template': 'stage_6', 'data': stage}

    def stage_7(self):
        stage = []
        segment_id = 'terms'
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': {}, 'data': []}
        # row = []
        # row.append(custom_utils.new_field(
        #     saved_data=saved_data,
        #     field_type='MultipleChoiceField',
        #     code='declaration',
        #     label='Declaration warranty on behalf of all persons proposed to be insured',
        #     helptext='I hereby declare and warrant on my behalf and on behalf of all personsproposed to be insured '
        #              'that the above statements are true and complete in all respectsand that there is no other '
        #              'information which is relevant to this application for insurance that has not been disclosed '
        #              'to Apollo Munich Health Insurance Company Ltd. I agree that this proposal and the declarations '
        #              'shall be the basis of the contract between me and all persons to be insured and Apollo Munich '
        #              'Health Insurance Company Ltd. I further consent and authorise Apollo Munich Health Insurance '
        #              'Company Ltd. and/or any of its authorized representatives to seek medical information from '
        #              'any hospital/consultant that I or any person proposed to be insured has attended or may '
        #              'attend in future concerning any disease or illness or injury.',
        #     span=12,
        #     value='Yes',
        #     options=[
        #         {'id' : 'Yes', 'name' : 'Yes', 'is_correct' : True},
        #         {'id' : 'No', 'name' : 'No', 'is_correct' : False}
        #     ],
        #     css_class='bottom-margin',
        #     template_option='terms'
        # ))
        # new_segment['data'].append(row)
        # row = []
        # row.append(custom_utils.new_field(
        #     saved_data=saved_data,
        #     field_type='MultipleChoiceField',
        #     code='disclaimer',
        #     label='Disclaimer',
        #     helptext='I agree that this proposal and the declarations shall be the basis of the contract between '
        #              'me/us and Apollo Munich Health Insurance Company Ltd. I further consent and authorize Apollo '
        #              'Munich Health Insurance Co. Ltd. and/or any of its authorized representatives to seek medical '
        #              'information from any hospital/consultant that I or any person proposed to be insured has '
        #              'attended or may attend in future concerning any disease or illness or injury. '
        #              'The information provided will be the basis of any insurance policy that We may issue. '
        #              'Proposer must disclose all facts relevant to all persons proposed to be insured that may '
        #              'affect our decision to issue a policy or its terms. Non-compliance may result in the avoidance '
        #              'of the policy. I/we, have proposed for this insurance policy with Apollo Munich Health '
        #              'insurance company ltd. I/We have read & understood the full terms, conditions & '
        #              'exclusions of the policy.',
        #     span=12,
        #     value='Yes',
        #     options=[
        #         {'id' : 'Yes', 'name' : 'Yes', 'is_correct' : True},
        #         {'id' : 'No', 'name' : 'No', 'is_correct' : False}
        #     ],
        #     css_class='bottom-margin',
        #     template_option='terms'
        # ))
        # new_segment['data'].append(row)
        row = []
        t_and_c_label = ('I hereby confirm<br />'
                         '- I have read and agree to the Terms and Conditions of ')
        if self.transaction.slab.health_product.policy_wordings:
            t_and_c_label += ('the <a style="text-decoration:none;cursor:pointer;" '
                              'onclick=\'window.open("%s","","width=400, height=400")\'>policy</a> and ') % self.transaction.slab.health_product.policy_wordings.url
        t_and_c_label += ('the <a style="cursor:pointer;" class="call_overlay">website</a>;<br />'
                          '- the declarations made herewith are true and correct;<br />'
                          '- appointment of Coverfox Insurance Brokers Pvt. Ltd. as my Insurance Broker, '
                          'and authorize them to represent me to Insurance Companies/Agencies for my Insurance needs.')
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='t_and_c',
            label=t_and_c_label,
            span=12,
            options=[
                {'id': 'Yes', 'name': 'Yes', 'is_correct': True},
                {'id': 'No', 'name': 'No', 'is_correct': False}
            ],
            css_class='bottom-margin',
            template_option='terms'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template': 'stage_7', 'data': stage}

    def get_form_data_individual(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
            self.stage_6(),
            self.stage_7()
        ]
        return data

    def get_form_data_family(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
            self.stage_6(),
            self.stage_7()
        ]

        return data


def get_state_city_by_pincode(transaction, pincode):
    new_state = pincode.state.name
    new_state_id = pincode.state.id
    city_list = preset.CITY_DICT[new_state_id]

    transaction.extra['pincode'] = pincode.pincode
    transaction.extra['city']['lat'] = pincode.lat
    transaction.extra['city']['lng'] = pincode.lng
    transaction.extra['city']['name'] = pincode.city
    transaction.save()

    new_location_dict = {}
    new_location_dict['new_state'] = new_state
    new_location_dict['new_city_list'] = city_list
    return new_location_dict, transaction


def get_dynamic_form(transaction):
    transaction, plan_data, proposer_data, is_self_present = custom_utils.standard_form_data_cleanup(transaction)
    policy_type = transaction.slab.health_product.policy_type.lower()
    plan_data['medical_firewall'] = True

    obj = Formic(transaction, is_self_present)
    stages = getattr(obj, 'get_form_data_%s' % policy_type, '')()

    terms_and_conditions = "<div style='padding:20px 40px 70px 20px;'>" \
                           "<h3>Apollo Munich: Terms and Conditions</h3>" \
                           "<p>I hereby declare and warrant on my behalf and on behalf of all personsproposed to be insured " \
                           "that the above statements are true and complete in all respectsand that there is no other " \
                           "information which is relevant to this application for insurance that has not been disclosed " \
                           "to Apollo Munich Health Insurance Company Ltd. I agree that this proposal and the declarations " \
                           "shall be the basis of the contract between me and all persons to be insured and Apollo Munich " \
                           "Health Insurance Company Ltd. I further consent and authorise Apollo Munich Health Insurance " \
                           "Company Ltd. and/or any of its authorized representatives to seek medical information from " \
                           "any hospital/consultant that I or any person proposed to be insured has attended or may " \
                           "attend in future concerning any disease or illness or injury.</p>" \
                           "<p>I agree that this proposal and the declarations shall be the basis of the contract between " \
                           "me/us and Apollo Munich Health Insurance Company Ltd. I further consent and authorize Apollo " \
                           "Munich Health Insurance Co. Ltd. and/or any of its authorized representatives to seek medical " \
                           "information from any hospital/consultant that I or any person proposed to be insured has " \
                           "attended or may attend in future concerning any disease or illness or injury. " \
                           "The information provided will be the basis of any insurance policy that We may issue. " \
                           "Proposer must disclose all facts relevant to all persons proposed to be insured that may " \
                           "affect our decision to issue a policy or its terms. Non-compliance may result in the avoidance " \
                           "of the policy. I/we, have proposed for this insurance policy with Apollo Munich Health " \
                           "insurance company ltd. I/We have read & understood the full terms, conditions & " \
                           "exclusions of the policy.</p>" \
                           "<h3>Coverfox: Terms and Conditions</h3>" \
                           "<p>Coverfox Insurance Broking Pvt. (hereinafter referred to as Coverfox) Limited operates " \
                           "the website <a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> to provide " \
                           "consumers choice and an easy way to buy Insurance " \
                           "products from multiple Insurance companies. Coverfox is not an insurance company. Coverfox " \
                           "is a licensed Insurance Broking Company holding a Broking licence from the Indian " \
                           "Insurance Regulator - Insurance Regulatory and Development authority. " \
                           "<a href='/irda-licence/' target='_blank'>(View IRDA License)</a></p>" \
                           "<p>Our participating insurance providers supply some of the information available on the " \
                           "Website and therefore there may be inaccuracies in the Website Information over which " \
                           "Coverfox has limited control.</p>" \
                           "<p>Coverfox does not warrant or guarantee the: Timeliness, accuracy or completeness of " \
                           "the Website Information; or Quality of the results obtained from the use of the Website.</p>" \
                           "<p>To the maximum extent permitted by law, Coverfox has no liability in relation to or " \
                           "arising out of the Website Information and Website recommendations. You are responsible " \
                           "for the final choice of your product and you should take time to read through all " \
                           "information supplied before proceeding. If you are in any doubt regarding a product or " \
                           "its terms you should seek further advice from Coverfox or the relevant participating " \
                           "provider before choosing your product.</p>" \
                           "<p>Coverfox may pass on your personal information to the relevant participating provider " \
                           "if you apply to purchase a product through " \
                           "<a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> , however, Coverfox does " \
                           "not guarantee when or if you will actually acquire the product that you have chosen. " \
                           "Coverfox does not accept any liability arising out of circumstances where there is delay " \
                           "in you acquiring the product you have chosen.</p>" \
                           "<p>Please note that Coverfox is only collecting or assisting in collecting  the premium " \
                           "deposit on behalf of the insurer you have chosen to buy the policy. The acceptance of " \
                           "the deposit as premium and final issuance of the policy is subject to the underwriting " \
                           "norms and discretion of the Insurer whose policy you have chosen to buy on which Coverfox " \
                           "has no control. Coverfox will ensure that the amount is refunded by the insurer in case " \
                           "there is no ultimate issuance of policy.</p>" \
                           "<p><button class='btn btn-lg cf-btn btn-red hide_overlay'>Close</button></p>" \
                           "</div>"

    slate_list = [
        {'id': 'slate_1', 'name': 'Proposer', 'class': '', 'description': 'The person who is paying for the policy'},
        {'id': 'slate_2', 'name': 'Insured Members', 'class': '', 'description': 'People being insured under the policy'},
        {
            'id': 'slate_3',
            'name': 'Nominee',
            'class': '',
            'description': 'In the event of the death of an Insured Person any payment due under the Policy shall '
                           'become payable to the nominee as per the Policy terms and conditions. '
                           'The nominee must be an immediate relative of the Proposer. '
                           'Nominee for any of the persons proposed to be insured shall be the Proposer.'
        },
        {'id': 'slate_4', 'name': 'Contact Info.', 'class': '', 'description': 'Contact Information of the proposer'},
        {'id': 'slate_5', 'name': 'Medical History', 'class': '', 'description': 'Has any of the insured members suffered from following diseases?'},
        {'id': 'slate_6', 'name': 'Previous Policy', 'class': '', 'description': 'Existing insurance details'},
        {'id': 'slate_7', 'name': 'T & C', 'class': 'last', 'description': 'Mandatory terms and conditions', 'style': " 'width', '0px' "}
    ]
    data = {
        'slate_list': slate_list,
        'proposer_data': proposer_data,
        'plan_data': plan_data,
        'stages': stages,
        'last_stage': stages[-1]['template'],
        'terms_and_conditions': terms_and_conditions
    }

    return data
