import putils

def download_buy_form(transaction):

    order = []
    rows = []

    proposer_data = transaction.insured_details_json['form_data']['proposer']['personal']
    insured_data = transaction.insured_details_json['form_data']['insured']
    nominee_data = transaction.insured_details_json['form_data']['nominee']['combo']
    proposer_address = transaction.insured_details_json['form_data']['proposer']['address']
    proposer_contact = transaction.insured_details_json['form_data']['proposer']['contact']
    medical_details = transaction.insured_details_json['form_data']['medical']['combo']
    previous_policy = transaction.insured_details_json['form_data']['previous_policy']['combo']

    rows.append(['Proposer Details'])
    rows.append(['Salutation', proposer_data['salutation']['display_value']])
    rows.append(['Name', ' '.join([proposer_data['first_name']['value'], proposer_data['last_name']['value']])])
    rows.append(['Gender', proposer_data['gender']['display_value']])
    rows.append(['Marital Status', proposer_data['marital_status']['display_value']])
    rows.append(['Date of Birth', proposer_data['date_of_birth']['display_value']])
    rows.append([proposer_data['occupation']['label'], proposer_data['occupation']['value']])
    rows.append(['Policy Tenure', proposer_data['two_year_discount']['display_value']])

    rows.append([])
    rows.append(['Insured Members'])
    for member in insured_data:
        rows.append([])
        rows.append(['Member', member])
        rows.append(['Salutation', insured_data[member]['salutation']['display_value']])
        rows.append(['Name', ' '.join([insured_data[member]['first_name']['display_value'], insured_data[member]['last_name']['display_value']])])
        rows.append(['Date of Birth', insured_data[member]['date_of_birth']['display_value']])
        if member in ['spouse', 'mother', 'father']:
             rows.append(['Marital Status', insured_data[member]['marital_status']['display_value']])
        rows.append(['Height', str(insured_data[member]['height']['value1'])+' feet '+str(insured_data[member]['height']['value2'])+' inches.'])
        rows.append(['Weight (kgs)', insured_data[member]['weight']['value']])



    rows.append([])
    rows.append(['Nominee Details'])
    rows.append(['Name', ' '.join([nominee_data['first_name']['value'], nominee_data['last_name']['value']])])
    rows.append(['Relationship to You', nominee_data['relation']['display_value']])



    rows.append([])
    rows.append(['Proposer Contact Details'])
    rows.append(['Address', ', '.join([proposer_address['address1']['value'], proposer_address['address2']['value'], proposer_address['landmark']['value'], proposer_address['city']['display_value'], proposer_address['state']['value'], proposer_address['pincode']['value']])])
    rows.append(['Email', proposer_contact['email']['value']])
    rows.append(['LandLine', proposer_contact['landline']['std_code']+'-'+proposer_contact['landline']['landline']])
    rows.append(['Mobile', proposer_contact['mobile']['value']])



    rows.append([])
    rows.append(['Medical Details'])
    rows.append([medical_details['lifestyle']['label'], medical_details['lifestyle']['value']])
    rows.append([previous_policy['existing_insurance']['label'], previous_policy['existing_insurance']['value']])
    rows.append(['Doctor\'s Name', medical_details['doctor_name']['value']])
    rows.append(['Doctor\'s Contact Number', medical_details['doctor_contact']['value']])

    file_name = 'Form_Data_%s.xls' % transaction.transaction_id
    return putils.render_excel(file_name, order, rows)
