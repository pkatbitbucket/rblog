import logging
import utils

from suds.client import Client
from suds import WebFault
from wiki.models import Pincode
from dateutil.relativedelta import datetime, relativedelta
from health_product.models import Transaction
from utils import request_logger
from health_product import prod_utils
from health_product.insurer.apollo_munich import preset
from health_product.insurer import custom_utils
from django.conf import settings
from django.utils import timezone


logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.transport.http').setLevel(logging.DEBUG)

# FOR PRODUCTION
if settings.PRODUCTION:
    WEBSERVICE_URL = 'http://b2b.apollomunichinsurance.com'
    PREMIUM_WSDL = 'http://b2b.apollomunichinsurance.com/PremiumCaculatorHttpService.svc?singleWsdl'
    PROPOSAL_WSDL = 'http://b2b.apollomunichinsurance.com/ProposalCaptureHttpService.svc?singleWsdl'
    PAYMENT_WSDL = 'http://b2b.apollomunichinsurance.com/PaymentGatewayHttpService.svc?singleWsdl'
    VERIFICATION_WSDL = 'http://b2b.apollomunichinsurance.com/TransactionVerificationHttpService.svc?singleWsdl'
    MASTERS_WSDL = 'http://b2b.apollomunichinsurance.com/MastersHttpService.svc?singleWsdl'
    PRODUCT_DETAIL_WSDL = 'http://b2b.apollomunichinsurance.com/ProductDetailHttpService.svc?singleWsdl'
    AGENT_CODE = '80171360'
    PARTNER_CODE = '10665'
    USERNAME = 'coverfox'
    PASSWORD = 'amhi@1234'
else:
# FOR UAT
    WEBSERVICE_URL = 'http://b2buat.apollomunichinsurance.com'
    PREMIUM_WSDL = 'http://b2buat.apollomunichinsurance.com/PremiumCaculatorHttpService.svc?singleWsdl'
    PROPOSAL_WSDL = 'http://b2buat.apollomunichinsurance.com/ProposalCaptureHttpService.svc?singleWsdl'
    PAYMENT_WSDL = 'http://b2buat.apollomunichinsurance.com/PaymentGatewayHttpService.svc?singleWsdl'
    VERIFICATION_WSDL = 'http://b2buat.apollomunichinsurance.com/TransactionVerificationHttpService.svc?singleWsdl'

    TEMP_AGENT_CODE = '80086426'
    AGENT_CODE = '80171360'
    PARTNER_CODE = '10290'
    USERNAME = 'coverfoxtesting'
    PASSWORD = 'password'


def is_medical(transaction):
    medical_flag = False
    form_data = transaction.insured_details_json['form_data']
    insured_data = form_data['insured']
    question_list = ['medical_question_cardiac_disorder', 'medical_question_lung_disorder', 'medical_question_digestive_disorder',
                     'medical_question_kidney_disorder', 'medical_question_nervous_disorder', 'medical_question_endocrine_disorder',
                     'medical_question_ulcer_disorder', 'medical_question_arthritis_disorder', 'medical_question_dioptres_disorder',
                     'medical_question_aids_disorder', 'medical_question_anaemia_disorder', 'medical_question_mental_disorder',
                     'medical_question_breast_disorder', 'medical_question_narcotic_disorder', 'medical_question_regular_medication',
                     'medical_question_blood_tests', 'medical_question_surgery', 'medical_question_fever',
                     'medical_question_pregnant', 'medical_question_diabetes'
                     ]

    for insured, data in insured_data.items():
        insured_dob = str(data['date_of_birth']['value'])
        insured_age = prod_utils.calculate_age(insured_dob)
        if insured_age['years'] > 45:
            medical_flag = True
            break

    if not medical_flag:
        if transaction.extra['plan_data']['cover'] >= 1000000:
            medical_flag = True

    if not medical_flag:
        for data in form_data:
            if data in question_list:
                tag = data.split("medical_question_")[1]
                for question_tag in form_data[data][tag]:
                    if form_data[data][tag][question_tag]['value'] == "Yes":
                        medical_flag = True
                        break

    if not medical_flag:
        for insured in insured_data:
            height_feet = insured_data[insured]['height']['value1']
            height_inches = insured_data[insured]['height']['value2']
            height_meters = (((height_feet * 12) + height_inches) * 2.54) / 100
            weight = int(insured_data[insured]['weight']['value'])
            bmi = int(round(weight / (height_meters * height_meters)))
            if bmi >= 32:
                medical_flag = True
                break
    return medical_flag


def get_salut(transaction):
    members = transaction.extra['members']
    insured = transaction.insured_details_json['form_data']['insured']
    salut = {}
    for m in members:
        if m['gender'] == 1:
            salut[m['id']] = 'MR' if m['age'] > 18 else 'MASTER'
        else:
            try:
                married = False if insured[m['id']]['marital_status']['value'] == '2' else True
            except Exception:
                married = False
            salut[m['id']] = 'MRS' if married else 'MISS'
    return salut

def get_clients(wsdl_client, transaction):

    salut = get_salut(transaction)

    product_type = preset.PRODUCT_TYPES[transaction.slab.health_product.id]
    pincode = Pincode.objects.filter(pincode=transaction.extra['pincode'])[0]
    state = pincode.state
    policy_term = transaction.insured_details_json['form_data']['proposer']['personal']['policy_term']['value1']
    product_data = preset.UAT_PRODUCT_MASTER['%s-%s' % (transaction.slab.health_product.id, policy_term)]
    data_dict = dict([(key, val) for key, val in transaction.insured_details_json['form_data'].items()])
    if product_type == 'super_topup':
        data_dict['version'] = preset.TOPUP_VERSION_MASTER[transaction.slab.deductible]
        product_data['version'] = preset.TOPUP_VERSION_MASTER[transaction.slab.deductible]
    array_of_clients = wsdl_client.factory.create('ns2:ArrayOfClient')
    main_member = None
    for member in transaction.extra['members']:
        if member['person']=='self':
            main_member = member
            break
        if not main_member:
            main_member = member
        elif member['age'] > main_member['age']:
            main_member = member

    array_of_products = wsdl_client.factory.create('ns2:ArrayOfProduct')
    premium_response = transaction.extra.get('premium_response', {})
    product_obj = wsdl_client.factory.create('ns2:Product')
    product_obj.ClientCode = main_member['id']
    product_obj.BasePremiumAmount = premium_response.get('BasePremiumAmount', None)
    product_obj.DiscountAmount = premium_response.get('DiscountAmount', None)
    product_obj.GrossPremiumAmount = premium_response.get('GrossPremiumAmount', None)
    product_obj.ProductCode = product_data['code']
    product_obj.ProductGroup = product_data['category']
    product_obj.ProductLine = product_data['line']
    product_obj.ProductType = product_data['type']
    product_obj.ProductVersion = product_data['version']
    product_obj.TaxAmount = premium_response.get('TaxAmount', None)
    product_obj.SumAssured = transaction.slab.sum_assured
    array_of_products.Product.append(product_obj)


    client_obj = wsdl_client.factory.create('ns2:Client')
    client_obj.Age = main_member['age']
    client_obj.ClientCode = main_member['id']
    client_obj.Product = array_of_products


    #ADDRESS OBJECT
    address_obj = wsdl_client.factory.create('ns2:Address')
    address_obj.AddressLine1 = transaction.proposer_address1
    address_obj.AddressLine2 = transaction.proposer_address2
    address_obj.AddressLine3 = transaction.proposer_address_landmark
    address_obj.CountryCode = 'IN'
    address_obj.District = None
    address_obj.PinCode = transaction.proposer_pincode
    address_obj.StateCode = str(state.id).zfill(3)
    address_obj.TownCode = data_dict['proposer']['address']['city']['value']

    array_of_address = wsdl_client.factory.create('ns2:ArrayOfAddress')
    array_of_address.Address.append(address_obj)

    client_obj.Address = array_of_address

    #CONTACT OBJECT
    contact_info_obj = wsdl_client.factory.create('ns2:ContactInformation')
    contact_info_obj.ContactNumber = wsdl_client.factory.create('ns2:ArrayOfContactNumber')
    contact_info_obj.Email = transaction.proposer_email

    contact_number = wsdl_client.factory.create('ns2:ContactNumber')
    contact_number.Number = transaction.proposer_mobile
    contact_number.Type = 7
    contact_info_obj.ContactNumber.ContactNumber.append(contact_number)

    #if transaction.proposer_landline.strip():
    #    landline_number = wsdl_client.factory.create('ns2:ContactNumber')
    #    landline_number.Number = transaction.proposer_landline
    #    landline_number.Type = 7
    #    contact_info_obj.ContactNumber.ContactNumber.append(landline_number)

    client_obj.ContactInformation = contact_info_obj

    array_of_previous_insurer = wsdl_client.factory.create('ns2:ArrayOfPreviousInsurer')

    array_of_dependants = wsdl_client.factory.create('ns2:ArrayOfClient')
    for member in transaction.extra['members']:
        if member['id'] != main_member['id']:
            insured_data = data_dict['insured'][member['id']]

            dep_array_of_products = wsdl_client.factory.create('ns2:ArrayOfProduct')
            dep_product_obj = wsdl_client.factory.create('ns2:Product')
            dep_product_obj.ClientCode = member['id']
            dep_product_obj.BasePremiumAmount = '0.00'
            dep_product_obj.DiscountAmount = 0
            dep_product_obj.GrossPremiumAmount = '0.00'
            dep_product_obj.ProductCode = product_data['code']
            dep_product_obj.ProductGroup = product_data['category']
            dep_product_obj.ProductLine = product_data['line']
            dep_product_obj.ProductType = product_data['type']
            dep_product_obj.ProductVersion = product_data['version']
            dep_product_obj.TaxAmount = '0.00'
            dep_product_obj.SumAssured = 0
            dep_array_of_products.Product.append(dep_product_obj)

            dep_client_obj = wsdl_client.factory.create('ns2:Client')
            # dep_client_obj.Address = wsdl_client.factory.create('ns2:ArrayOfAddress')
            dep_client_obj.Age = member['age'] if member['age'] != 0 else 1
            dep_client_obj.BirthDate = datetime.datetime.strftime(datetime.datetime.strptime(insured_data['date_of_birth']['value'], '%d/%m/%Y'), '%Y-%m-%d')
            dep_client_obj.ClientCode = member['id']
            # dep_client_obj.ContactInformation = contact_info_obj
            dep_client_obj.FirstName = insured_data['first_name']['value']
            dep_client_obj.MiddleName = None
            dep_client_obj.LastName = insured_data['last_name']['value']
            dep_client_obj.GenderCode = member['gender']

            feet = insured_data['height']['value1']
            inches = insured_data['height']['value2']
            if not feet:
                feet = 0
            if not inches:
                inches = 0
            dep_client_obj.Height = int((int(feet) * 30.48) + (int(inches) * 2.54))
            try:
                dep_client_obj.MaritalStatusCode = insured_data['marital_status']['value']
            except KeyError:
                dep_client_obj.MaritalStatusCode = 2

            dep_client_obj.NationalityCode = 'IN'
            dep_client_obj.OccuptionCode = 'false'
            dep_client_obj.Product = dep_array_of_products

            relationship = member['person']
            if relationship == 'spouse':
                relationship = 'husband' if member['gender']==1 else 'wife'
            dep_client_obj.RelationshipCode = preset.RELATIONSHIP_MASTER[relationship]

            dep_client_obj.TitleCode = salut[member['id']]
            # dep_client_obj.TotalPremium = None
            dep_client_obj.Weight = insured_data['weight']['value']

            array_of_dependants.Client.append(dep_client_obj)

    #Filling Proposer (Main Member) Data
    insured_data = data_dict['insured'][main_member['id']]
    client_obj.BirthDate = datetime.datetime.strftime(datetime.datetime.strptime(insured_data['date_of_birth']['value'], '%d/%m/%Y'), '%Y-%m-%d')
    client_obj.ClientCode = main_member['id']
    # client_obj.ContactInformation = contact_info_obj
    client_obj.FirstName = insured_data['first_name']['value']
    client_obj.MiddleName = ''
    client_obj.LastName = insured_data['last_name']['value']
    client_obj.GenderCode = main_member['gender']
    feet = insured_data['height']['value1']
    inches = insured_data['height']['value2']
    if not feet:
        feet = 0
    if not inches:
        inches = 0
    client_obj.Height = int((int(feet) * 30.48) + (int(inches) * 2.54))
    try:
        client_obj.MaritalStatusCode = insured_data['marital_status']['value']
    except KeyError:
        client_obj.MaritalStatusCode = 2
    client_obj.NationalityCode = 'IN'
    client_obj.IDProofNumber = ''
    # client_obj.OccuptionCode = 'true' if data_dict['proposer']['personal']['occupation']['value'] == 'Yes' else 'false'
    client_obj.OccuptionCode = 'false'
    client_obj.PreviousInsurer = array_of_previous_insurer
    client_obj.Product = array_of_products

    relationship = main_member['person']
    if relationship == 'spouse':
        relationship = 'husband' if main_member['gender']==1 else 'wife'
    client_obj.RelationshipCode = preset.RELATIONSHIP_MASTER[relationship]

    client_obj.TitleCode = salut[main_member['id']]
    client_obj.Weight = insured_data['weight']['value']
    client_obj.Dependants = array_of_dependants
    array_of_clients.Client.append(client_obj)
    return array_of_clients


def get_premium_obj(premium_client, transaction):
    partner_obj = premium_client.factory.create('ns2:Partner')
    partner_obj.PartnerCode = PARTNER_CODE
    partner_obj.Password = PASSWORD
    partner_obj.UserName = USERNAME

    premium_obj = premium_client.factory.create('ns1:PremiumCalculatorRequest')
    premium_obj.Clients = get_clients(premium_client, transaction)
    premium_obj.Partner = partner_obj
    return premium_obj


def get_proposal_obj(proposal_client, transaction):
    main_member = None
    for member in transaction.extra['members']:
        if not main_member:
            main_member = member
        if member['age'] > main_member['age']:
            main_member = member
    for member in transaction.extra['members']:
        if member['person']=='self':
            main_member = member
    # main_member_data = transaction.insured_details_json['form_data']['insured'][main_member['id']]

    proposal_obj = proposal_client.factory.create('ns1:ProposalCaptureServiceRequest')
    proposal_obj.Action = 'Create'

    partner_obj = proposal_client.factory.create('ns2:Partner')
    partner_obj.PartnerCode = PARTNER_CODE
    partner_obj.Password = PASSWORD
    partner_obj.UserName = USERNAME

    prospect_obj = proposal_client.factory.create('ns2:Prospect')
    prospect_obj.BrandCode = 'ApolloMunich'

    #NOMINEE DATA
    nominee_data = transaction.insured_details_json['form_data']['nominee']['self']
    application = proposal_client.factory.create('ns2:Application')
    application.TPANameCode = 'AMHI'
    # application.Namefor80D = transaction.proposer_first_name
    # application.NomineeAddress = proposal_client.factory.create('ns2:Address')
    # application.NomineeAddress.AddressLine1 = nominee_data['address']['value']
    application.NomineeName = custom_utils.get_full_name(
        first_name=nominee_data['first_name']['value'],
        last_name=nominee_data['last_name']['value']
    )
    # application.NomineeTitleCode = None
    application.ProposerName = custom_utils.get_full_name(
        first_name=transaction.proposer_first_name,
        middle_name=transaction.proposer_middle_name,
        last_name=transaction.proposer_last_name
    )
    application.Namefor80D = application.ProposerName
    application.RelationToNomineeCode = nominee_data['relation']['value']
    # application.RuralFlag = None

    prospect_obj.Application = application
    array_of_clients = get_clients(proposal_client, transaction)
    prospect_obj.Client = array_of_clients[0]
    prospect_obj.TotalPremiumAmount = transaction.premium_paid
    # prospect_obj.PolicyType = None

    proposal_obj.Prospect = prospect_obj

    proposal_obj.Partner = partner_obj
    return proposal_obj


def get_payment_obj(payment_client, transaction):
    pg_detail_obj = payment_client.factory.create('ns2:PGDetail')
    pg_detail_obj.ProposalId = transaction.policy_token
    pg_detail_obj.ReturnUrl = '%s/health-plan/apollo_munich/transaction/%s/response/' % (settings.SITE_URL, transaction.transaction_id)
    pg_detail_obj.MerchantRefNo = transaction.reference_id
    pg_detail_obj.IPAddress = prod_utils.get_client_ip(utils.get_request())
    pg_detail_obj.UDF1 = ''
    pg_detail_obj.UDF2 = ''
    pg_detail_obj.UDF3 = ''
    pg_detail_obj.UDF4 = ''
    pg_detail_obj.UDF5 = ''

    partner_obj = payment_client.factory.create('ns2:Partner')
    partner_obj.PartnerCode = PARTNER_CODE
    partner_obj.UserName = USERNAME
    partner_obj.Password = PASSWORD

    payment_obj = payment_client.factory.create('ns1:PaymentGatewayServiceRequest')
    payment_obj.PGDetail = pg_detail_obj
    payment_obj.Partner = partner_obj
    return payment_obj


def post_transaction(transaction):
    redirect_url = '/health-plan/product/transaction/%s/failure/' % transaction.transaction_id
    is_redirect = True
    pdata = transaction.insured_details_json['form_data']['proposer']['personal']
    transaction.proposer_date_of_birth = datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y')
    transaction.proposer_gender = int(pdata['gender']['value'])
    transaction.save()

    # try:
    if True:
        try:
            premium_client = Client(PREMIUM_WSDL)
            premium_client.set_options(port = 'BasicHttpBinding_IPremimumCalculatorService')


            premium_request = premium_client.service.ComputePremium
            premium_obj = get_premium_obj(premium_client, transaction)
            resp = premium_request(premium_obj)
            request_logger.info('\nAPOLLO MUNICH TRANSACTION: %s \n PREMIUM REQUEST SENT:\n %s \n' % (transaction.transaction_id, premium_client.last_sent()))
            request_logger.info('\nAPOLLO MUNICH TRANSACTION: %s \n PREMIUM RESPONSE RECEIVED:\n %s \n' % (transaction.transaction_id, premium_client.last_received()))
            response_dict = custom_utils.suds_to_dict(resp)
            transaction.extra['premium_response'] = response_dict['Client'][0]['Product']['Product'][0]
            transaction.premium_paid = response_dict['Client'][0]['TotalPremium']
            transaction.save()

            proposal_client = Client(PROPOSAL_WSDL)
            proposal_request = proposal_client.service.ProposalCapture
            proposal_obj = get_proposal_obj(proposal_client, transaction)
            resp = proposal_request(proposal_obj)
            request_logger.info('\nAPOLLO MUNICH TRANSACTION: %s \n PROPOSAL REQUEST SENT:\n %s \n' % (transaction.transaction_id, proposal_client.last_sent()))
            request_logger.info('\nAPOLLO MUNICH TRANSACTION: %s \n PROPOSAL RESPONSE RECEIVED:\n %s \n' % (transaction.transaction_id, proposal_client.last_received()))

            transaction.policy_token = resp
            transaction.reference_id = 'CFOX-%s-%s' % (transaction.id, datetime.datetime.now().strftime("%I%M%S"))
            transaction.save()

            payment_client = Client(PAYMENT_WSDL)
            payment_client.set_options(port = 'BasicHttpBinding_IPaymentGatewayService')
            payment_request = payment_client.service.PaymentDetails
            payment_obj = get_payment_obj(payment_client, transaction)
            resp = payment_request(payment_obj)
            request_logger.info('\nAPOLLO MUNICH TRANSACTION: %s \n PAYMENT REQUEST SENT:\n %s \n' % (transaction.transaction_id, payment_client.last_sent()))
            request_logger.info('\nAPOLLO MUNICH TRANSACTION: %s \n PAYMENT RESPONSE RECEIVED:\n %s \n' % (transaction.transaction_id, payment_client.last_received()))
            response_dict = custom_utils.suds_to_dict(resp)
            request_logger.info('\nAPOLLO MUNICH TRANSACTION: %s \n PAYMENT RESPONSE DICT:\n %s \n' % (transaction.transaction_id, response_dict))
            transaction.extra['payment_obj_response'] = response_dict
            transaction.payment_request = payment_client.last_sent()
            transaction.save()
            is_redirect = True
            redirect_url = transaction.extra['payment_obj_response']['PaymentUrl']
            return is_redirect, redirect_url, '', {}
        except WebFault, error:
            request_logger.info('\nAPOLLO MUNICH TRANSACTION: %s \n PROPOSAL GENERATION ERROR:\n %s \n' % (transaction.transaction_id, error.document))
            error_details = error.document.getChild(name='Envelope').getChild(name='Body').getChild(name='Fault').getChild(name='detail')
            try:
                error_messages = error_details.getChild(name='ValidationFault').getChild(name='Details').getChildren()
            except:
                error_messages = error_details.getChild(name='ArrayOfValidationDetail').getChildren()

            error_message=''
            for m in error_messages:
                error_message = error_message+'|' + m.getChild(name='Message').text
            transaction.extra['error_msg'] = error_message;
            transaction.save();
            return is_redirect, redirect_url, '', {}
    # except Exception as e:
    else:
        return is_redirect, redirect_url, '', {}


def post_payment(payment_response, transaction=None):
    is_redirect = True
    resp = payment_response

    transaction = Transaction.objects.get(reference_id=resp['MerchantRefNo'])
    redirect_url = '/health-plan/payment/transaction/%s/failure/' % transaction.transaction_id
    transaction.extra['payment_response'] = resp
    transaction.payment_response = resp
    transaction.policy_number = resp['ProposalId']
    transaction.policy_token = resp['TransactionId']
    transaction.payment_token = resp['PaymentId']
    policy_term = transaction.insured_details_json['form_data']['proposer']['personal']['policy_term']['value1']
    transaction.policy_start_date = timezone.now()
    transaction.policy_end_date = timezone.now() + relativedelta(days=-1, years=policy_term)
    transaction.save()

    if not transaction.extra['payment_response']['Message']:
        transaction.payment_done = True
        transaction.save()
        #TRANSACTION VERIFICATION

        verification_client = Client(VERIFICATION_WSDL)
        partner_obj = verification_client.factory.create('ns1:Partner')
        partner_obj.PartnerCode = PARTNER_CODE
        partner_obj.UserName = USERNAME
        partner_obj.Password = PASSWORD
        verification_request = verification_client.service.VerifyTransaction
        ver_obj = verification_client.factory.create('ns2:TransactionVerificationRequest')
        ver_obj.Partner = partner_obj
        ver_obj.PaymentId = transaction.payment_token
        ver_obj.TransactionId = transaction.policy_token

        try:
            resp = verification_request(ver_obj)
            request_logger.info('\nAPOLLO MUNICH TRANSACTION: %s \n VERIFICATION REQUEST SENT:\n %s \n' % (transaction.transaction_id, verification_client.last_sent()))
            request_logger.info('\nAPOLLO MUNICH TRANSACTION: %s \n VERIFICATION RESPONSE RECEIVED:\n %s \n' % (transaction.transaction_id, verification_client.last_received()))

            if str(resp) == '0':
                redirect_url = '/health-plan/payment/transaction/%s/success/' % transaction.transaction_id
                transaction.payment_done = True
                transaction.payment_on = datetime.datetime.today()
                transaction.save()
        except WebFault, error:
            request_logger.info('\nAPOLLO MUNICH TRANSACTION: %s \n VERIFICATION ERROR:\n %s \n' % (transaction.transaction_id, error.document))
    else:
        transaction.extra['error_msg'] = resp['Message'];
        transaction.status = 'FAILED'
        transaction.save()
    return is_redirect, redirect_url, '', {}
