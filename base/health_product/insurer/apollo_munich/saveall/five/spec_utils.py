from health_product.insurer import custom_utils


def save_stage(transaction, stage_data):
    red_flag = []
    form_data = custom_utils.clean_stage_data(stage_data)

    def red_flag_edit(action, question, answer=None):
        if action == 'append':
            for i in red_flag:
                if i['question'] == question:
                    i['answer'] = answer
                    return
            red_flag.append({'question': question, 'answer': answer})
        elif action == 'remove':
            for i in red_flag:
                if i['question'] == question:
                    red_flag.remove(i)

    for k, v in form_data.items():
        if 'form_data' not in transaction.insured_details_json.keys():
            transaction.insured_details_json['form_data'] = {}
        if k in transaction.insured_details_json['form_data'].keys():
            transaction.insured_details_json['form_data'][k].update(**v)
        else:
            transaction.insured_details_json['form_data'][k] = v
    transaction.save()
    # ================== CHECK FOR MEDICAL ====================
    question_list = ['medical_question_cardiac_disorder', 'medical_question_lung_disorder', 'medical_question_digestive_disorder',
                     'medical_question_kidney_disorder', 'medical_question_nervous_disorder', 'medical_question_endocrine_disorder',
                     'medical_question_ulcer_disorder', 'medical_question_arthritis_disorder', 'medical_question_dioptres_disorder',
                     'medical_question_aids_disorder', 'medical_question_anaemia_disorder', 'medical_question_mental_disorder',
                     'medical_question_breast_disorder', 'medical_question_narcotic_disorder', 'medical_question_regular_medication',
                     'medical_question_blood_tests', 'medical_question_surgery', 'medical_question_fever',
                     'medical_question_pregnant', 'medical_question_diabetes'
                     ]
    insured_data = transaction.insured_details_json['form_data']['insured']
    for bigkey, bigval in form_data.items():
        if bigkey in question_list:
            for code, mdata in bigval.items():
                for k, v in mdata.items():
                    if v['value'] == 'Yes':
                        red_flag_edit('append', question=v['label'], answer=v['value'])
    occupation_obj = transaction.insured_details_json['form_data']['proposer']['personal']['occupation']
    if occupation_obj['value'] == 'Yes':
        red_flag.append({'question': occupation_obj['label'], 'answer': occupation_obj['value']})
    for insured in insured_data:
        height_feet = insured_data[insured]['height']['value1']
        height_inches = insured_data[insured]['height']['value2']
        height_meters = int(round((((height_feet * 12) + height_inches) * 2.54) / 100))
        weight = int(insured_data[insured]['weight']['value'])
        bmi = int(round(weight / (height_meters * height_meters)))
        if bmi >= 32:
            red_flag.append({'question': 'BMI higher than 32 for %s' % (insured), 'answer': str(bmi)})
    # =========================================================
    if 'red_flags' not in transaction.extra:
        transaction.extra['red_flags'] = {}
    transaction.extra['red_flags'][stage_data['template']] = {'reason': 'DISEASE', 'data': red_flag}
    transaction.save()
    return transaction
