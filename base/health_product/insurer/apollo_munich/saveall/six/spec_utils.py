from health_product.insurer import custom_utils

def save_stage(transaction, stage_data):
    current_stage = stage_data['template']
    red_flag = []
    form_data = custom_utils.clean_stage_data(stage_data)

    def red_flag_edit(action, question, answer=None):
        if action == 'append':
            for i in red_flag:
                if i['question'] == question:
                    i['answer'] = answer
                    return
            red_flag.append({'question':question, 'answer':answer})
        elif action == 'remove':
            for i in red_flag:
                if i['question'] == question:
                    red_flag.remove(i)

    for bigkey, bigval in form_data.items():
        if 'form_data' not in transaction.insured_details_json.keys():
            transaction.insured_details_json['form_data'] = {}
        if bigkey in transaction.insured_details_json['form_data'].keys():
            transaction.insured_details_json['form_data'][bigkey].update(**bigval)
        else:
            transaction.insured_details_json['form_data'][bigkey] =bigval

        if bigkey == 'previous_policy':
            for code, mdata in bigval.items():
                for k, v in mdata.items():
                    if v['value'] == 'Yes':
                        red_flag_edit('append', question=v['label'], answer=v['value'])
                    else:
                        red_flag_edit('remove', question=v['label'])

    #FOR EXISTING POLICIES SECTION
    # transaction.insured_details_json['form_data']['existing_policies'] = []
    # existing_policy_param_list = ['policy_no', 'start_date', 'end_date', 'sum_insured', 'insurer', 'claims']
    # if 'existingPolicies' in stage_data.keys():
    #     for policy in stage_data['existingPolicies']:
    #         temp_dict = {}
    #         for pkey, pval in policy.items():
    #             if pkey in existing_policy_param_list:
    #                 temp_dict[pkey] = pval
    #         transaction.insured_details_json['form_data']['existing_policies'].append(temp_dict)
    # if transaction.insured_details_json['form_data']['existing_policies']:
    #     red_flag.append({'question': 'Pre-existing policies', 'answer': 'Yes'})

    if 'red_flags' not in transaction.extra:
        transaction.extra['red_flags'] = {}
    transaction.extra['red_flags'][stage_data['template']] = {'reason':'DISEASE','data':red_flag}
    transaction.save()
    return transaction
