from wiki.models import City, Pincode
from health_product.insurer.bajaj_allianz import preset
from health_product.insurer import custom_utils
from collections import OrderedDict

class Formic():

    def __init__(self, transaction, is_self_present):
        self.transaction = transaction
        self.is_self_present = is_self_present
        self.proposer_data = {
            'pincode': transaction.extra['pincode'],
            'city': City.objects.get(id=transaction.extra['city']['id']).name,
            'state': City.objects.get(id=transaction.extra['city']['id']).state.name,
            'email': ''
        }

    def stage_1(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'personal'
        try:
            saved_data = self.transaction.insured_details_json[
                'form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id,
                       'tag': tag_id, 'details': {}, 'data': []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            label='First Name',
            code='first_name',
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            label='Last Name',
            code='last_name',
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Gender',
            code='gender',
            value=1 if self.transaction.extra['gender'] == 'MALE' else 2,
            options=preset.GENDER_OPTIONS
        ))
        new_segment['data'].append(row)
        row = []
        married = True if [m for m in self.transaction.extra[
            'members'] if m['id'] == 'spouse'] else False
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Marital Status',
            code='marital_status',
            value='Married' if married or len(
                self.transaction.extra['members']) > 1 else 'Single',
            options=preset.MARITAL_STATUS_OPTIONS,
            template_option='static' if married else 'dyanmic',
        ))
        if self.is_self_present:
            member = [m for m in self.transaction.extra[
                'members'] if m['id'] == 'self'][0]
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                span=3,
                min_age=18,
                max_age=100,
                default_age=member['age'],
            ))
        else:
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                min_age=18,
                max_age=100,
                default_age=18
            ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Occupation',
            code='occupation',
            options=preset.OCCUPATION_OPTIONS,
        ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownDependantField',
            label='Voluntary Deductible Option',
            code='deductible',
            span=6,
            options1=preset.DEDUCTIBLE_MAIN,
            options2=preset.DEDUCTIBLE_DEP,
            required=False,
            main_label='Deductible Amount in Rs',
            dep_label='Discount (percent)',
            tooltip="Voluntary deductible is the amount which shall be borne by the insured in respect of "
            "each and every hospitalization claim incurred in the policy period. The company's "
            "liability to make payment for each and every claim under the policy is in excess of the deductible."
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            label='Waiver of non-network Co-payment',
            tooltip='Non-network Co-payment is the amount which shall be borne by the insured In case of '
                    'hospitalization in a hospital other than a Network Hospital.'
                    'The non-network co-payment under this policy is 10%. The company\'s liability '
                    'in case of non-network hospitalization is in excess of this co-payment.'
                    'The co-payment waiver option is available on payment of 10% of loading on premium. '
                    'The co-payment option shall be at policy level and not at member level',
            code='copay',
            options=preset.COPAY_OPTIONS,
            value='N',
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template': 'stage_1', 'data': stage}

    def stage_2(self):
        stage = []
        feet_range = []
        for feet in range(0, 9):
            feet_range.append({'id': feet, 'name': '%s ft' % feet})
        inch_range = []
        for inch in range(0, 12):
            inch_range.append({'id': inch, 'name': '%s inches' % inch})

        for member in self.transaction.extra['members']:
            segment_id = 'insured'
            tag_id = member['id']
            template_switch = 'dynamic'
            if tag_id == 'self':
                template_switch = 'static'
            try:
                saved_data = self.transaction.insured_details_json[
                    'form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment': segment_id,
                           'tag': tag_id, 'details': member, 'data': []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                label='First Name',
                code='first_name',
                max_length=50,
                template_option=template_switch,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                label='Last Name',
                code='last_name',
                max_length=50,
                template_option=template_switch,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                template_option=template_switch,
                default_age=member['age'],
                min_age=18 if member['type'] == 'adult' else 0,
                max_age=100,
                span=3,
            ))
            new_segment['data'].append(row)
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='TwoDropDownField',
                label='Height',
                code='height',
                options1=feet_range,
                options2=inch_range
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='IntegerField',
                label='Weight',
                code='weight',
                sub_text='Kgs',
                span=2
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DropDownField',
                label='Occupation',
                code='occupation',
                options=preset.OCCUPATION_OPTIONS,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='MultipleChoiceField',
                label='Smoking Habit',
                code='%s_smoking' % member['id'],
                options=preset.SMOKING_OPTIONS,
                value='N',
                span=2
            ))
            new_segment['data'].append(row)
            # if member['person'].lower() in ['father', 'mother', 'self', 'spouse']:
            #     row = []
            #     row.append(custom_utils.new_field(
            #         saved_data=saved_data,
            #         field_type='DropDownField',
            #         label='Marital Status',
            #         code='marital_status',
            #         options=preset.MARITAL_STATUS_OPTIONS
            #     ))
            #     new_segment['data'].append(row)

            stage.append(new_segment)
        return {'template': 'stage_2', 'data': stage}

    def stage_3(self):
        stage = []

        default_relation = {
            'self': 'Spouse',
            'spouse': 'Spouse',
            'son': 'Father' if self.transaction.extra['gender'] == 'MALE' else 'Mother',
            'daughter': 'Father' if self.transaction.extra['gender'] == 'MALE' else 'Mother',
            'father': 'Son' if self.transaction.extra['gender'] == 'MALE' else 'Daughter',
            'mother': 'Son' if self.transaction.extra['gender'] == 'MALE' else 'Daughter'
        }
        self_relation_probables = OrderedDict([
            ('mother', 'Mother'),
            ('father', 'Father'),
            ('spouse', 'Spouse')
        ])
        # Needs to run before the big loop on members
        for member in self.transaction.extra['members']:
            if member['person'] in self_relation_probables.keys():
                self.transaction.extra['self_nominee'] = member['id']
                self.transaction.save()
                default_relation['self'] = self_relation_probables[
                    member['person']]

        for member in self.transaction.extra['members']:
            segment_id = 'nominee'
            tag_id = member['id']
            try:
                saved_data = self.transaction.insured_details_json[
                    'form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment': segment_id,
                           'tag': tag_id, 'details': member, 'data': []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DropDownField',
                code='relation',
                label='Relationship With %s' % member['verbose'],
                options=custom_utils.DEFAULT_NOMINEE_RELATIION_OPTIONS,
                value=default_relation[member['person']],
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                code='first_name',
                label='First Name',
                max_length=50,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                code='last_name',
                label='Last Name',
                max_length=50,
                span=3
            ))
            # row.append(custom_utils.new_field(
            #     saved_data=saved_data,
            #     field_type='DateField',
            #     code='date_of_birth',
            #     label='Date Of Birth',
            #     min_age=18,
            #     span=3
            # ))
            new_segment['data'].append(row)
            stage.append(new_segment)
        return {'template': 'stage_3', 'data': stage}

    def stage_4(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'address'
        try:
            saved_data = self.transaction.insured_details_json[
                'form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id,
                       'tag': tag_id, 'details': {}, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address1',
            label='Flat/House No, Street',
            span=12,
            css_class='bottom-margin',
            max_length=50
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address2',
            label='Area/Locality',
            span=12,
            css_class='bottom-margin',
            max_length=100,
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='landmark',
            label='Landmark',
            span=12,
            css_class='bottom-margin',
            max_length=100,
            required=False
        ))
        new_segment['data'].append(row)
        row = []
        pincode = Pincode.objects.filter(
            pincode=self.transaction.extra['pincode'])[0]
        state = pincode.state
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='SearchableDropDownField',
            code='city',
            label='City',
            options=[{'id': c.name, 'name': c.name}
                     for c in state.city_set.all()]
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='state',
            label='State',
            value=self.proposer_data['state'],
            span=4,
            template_option='static'
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='pincode',
            label='Pincode',
            value=str(self.proposer_data['pincode']),
            span=4,
            template_option='static'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)

        tag_id = 'contact'
        try:
            saved_data = self.transaction.insured_details_json[
                'form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id,
                       'tag': tag_id, 'details': {}, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='email',
            label='Email',
            span=12,
            css_class='bottom-margin',
            email=True
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MobileField',
            code='mobile',
            label='Mobile',
            css_class='bottom-margin',
            span=12
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='LandlineField',
            code='landline',
            label='Landline',
            span=12,
            css_class='bottom-margin',
            required=False
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template': 'stage_4', 'data': stage}

    def stage_5(self):
        stage = []

        segment_id = 'medical'
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json[
                'form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id,
                       'tag': tag_id, 'details': {}, 'data': []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='pregnant',
            label='Please confirm, if any of the persons to be insured is pregnant (For Females Only)',
            span=12,
            css_class='bottom-margin',
            template_option='question'
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='hospitalization',
            label='Do you or any of the family members to be covered have / had any health complaints / met '
            'with any Accident in the past 4 years and have been taking treatment / hospitization?',
            span=12,
            css_class='bottom-margin',
            template_option='question'
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='pre_existing',
            label='Has any of the persons to be insured suffer from / or investigated for any of the following? '
            'Disorder of heart, or circulatory system, chest pain, high blood pressure, stroke, asthma '
            'any respiratory conditions, cancer tumor lump of any kind, diabetes, hepatitis, disorder '
            'of urinary tract or kidneys, blood disorder, any mental or psychiatric conditions, any '
            'disease of brain or nervous system, fits (epilepsy) slipped disc, back ache, any '
            'congenital / birth defects/ urinary diseases, AIDS or positive HIV.',
            span=12,
            css_class='bottom-margin',
            template_option='question'
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='previous_proposal',
            label='Has any proposal for life, critical ilness or health related insurance on your life ever been postponed, declined or accepted on special terms?',
            span=12,
            css_class='bottom-margin',
            template_option='question'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)

        return {'template': 'stage_5', 'data': stage}

    def stage_6(self):
        stage = []
        segment_id = 'terms'
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json[
                'form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id,
                       'tag': tag_id, 'details': {}, 'data': []}
        row = []
        t_and_c_label = ('I hereby confirm<br />'
                         '- I have read and agree to the Terms and Conditions of ')
        if self.transaction.slab.health_product.policy_wordings:
            t_and_c_label += ('the <a style="text-decoration:none;cursor:pointer;" '
                              'onclick=\'window.open("%s","","width=400, height=400")\'>policy</a> and '
                              ) % self.transaction.slab.health_product.policy_wordings.url
        t_and_c_label += ('the <a style="cursor:pointer;" class="call_overlay">website</a>;<br />'
                          '- the declarations made herewith are true and correct;<br />'
                          '- appointment of Coverfox Insurance Brokers Pvt. Ltd. as my Insurance Broker, '
                          'and authorize them to represent me to Insurance Companies/Agencies for my Insurance needs.'
                          )
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='t_and_c',
            label=t_and_c_label,
            span=12,
            options=[
                {'id': 'Yes', 'name': 'Yes', 'is_correct': True},
                {'id': 'No', 'name': 'No', 'is_correct': False}
            ],
            css_class='bottom-margin',
            template_option='terms'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template': 'stage_6', 'data': stage}

    def get_form_data_family(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
            self.stage_6()
        ]
        return data

    def get_form_data_individual(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
            self.stage_6()
        ]
        return data


def get_dynamic_form(transaction):
    transaction, plan_data, proposer_data, is_self_present = custom_utils.standard_form_data_cleanup(
        transaction)

    # TOGGLES WHERE MEDICAL CASES ARE ALLOWED OR NOT
    plan_data['medical_firewall'] = False

    obj = Formic(transaction, is_self_present)
    stages = getattr(obj, 'get_form_data_%s' %
                     transaction.slab.health_product.policy_type.lower(), '')()

    terms_and_conditions = "<div style='padding:20px 40px 70px 20px;'>" \
                           "<h3>Coverfox: Terms and Conditions</h3>" \
                           "<p>Coverfox Insurance Broking Pvt. (hereinafter referred to as Coverfox) Limited operates " \
                           "the website <a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> to provide " \
                           "consumers choice and an easy way to buy Insurance " \
                           "products from multiple Insurance companies. Coverfox is not an insurance company. Coverfox " \
                           "is a licensed Insurance Broking Company holding a Broking licence from the Indian " \
                           "Insurance Regulator - Insurance Regulatory and Development authority. " \
                           "<a href='/docs/irda-licence/' target='_blank'>(View IRDA License)</a></p>" \
                           "<p>Our participating insurance providers supply some of the information available on the " \
                           "Website and therefore there may be inaccuracies in the Website Information over which " \
                           "Coverfox has limited control.</p>" \
                           "<p>Coverfox does not warrant or guarantee the: Timeliness, accuracy or completeness of " \
                           "the Website Information; or Quality of the results obtained from the use of the Website.</p>" \
                           "<p>To the maximum extent permitted by law, Coverfox has no liability in relation to or " \
                           "arising out of the Website Information and Website recommendations. You are responsible " \
                           "for the final choice of your product and you should take time to read through all " \
                           "information supplied before proceeding. If you are in any doubt regarding a product or " \
                           "its terms you should seek further advice from Coverfox or the relevant participating " \
                           "provider before choosing your product.</p>" \
                           "<p>Coverfox may pass on your personal information to the relevant participating provider " \
                           "if you apply to purchase a product through " \
                           "<a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> , however, Coverfox does " \
                           "not guarantee when or if you will actually acquire the product that you have chosen. " \
                           "Coverfox does not accept any liability arising out of circumstances where there is delay " \
                           "in you acquiring the product you have chosen.</p>" \
                           "<p>Please note that Coverfox is only collecting or assisting in collecting  the premium " \
                           "deposit on behalf of the insurer you have chosen to buy the policy. The acceptance of " \
                           "the deposit as premium and final issuance of the policy is subject to the underwriting " \
                           "norms and discretion of the Insurer whose policy you have chosen to buy on which Coverfox " \
                           "has no control. Coverfox will ensure that the amount is refunded by the insurer in case " \
                           "there is no ultimate issuance of policy.</p>" \
                           "<p><button class='btn btn-lg cf-btn btn-red hide_overlay'>Close</button></p>" \
                           "</div>"

    slate_list = [
        {'id': 'slate_1', 'name': 'Proposer', 'class': '',
            'description': 'The person who is paying for the policy'},
        {'id': 'slate_2', 'name': 'Insured Members', 'class': '',
            'description': 'People being insured under the policy'},
        {'id': 'slate_3', 'name': 'Nominee', 'class': '',
            'description': 'Nominees for the insured members'},
        {'id': 'slate_4', 'name': 'Contact Info.', 'class': '',
            'description': 'Contact Information of the proposer'},
        {'id': 'slate_5', 'name': 'Medical History', 'class': '',
            'description': 'Medical History of the people being insured'},
        {'id': 'slate_6', 'name': 'T & C', 'class': 'last',
            'description': 'Mandatory terms and conditions', 'style': " 'width', '0px' "}
    ]
    data = {
        'slate_list': slate_list,
        'proposer_data': proposer_data,
        'plan_data': plan_data,
        'stages': stages,
        'last_stage': stages[-1]['template'],
        'terms_and_conditions': terms_and_conditions
    }

    return data
