from suds.client import Client
import datetime
from dateutil.relativedelta import relativedelta
from django.conf import settings
from utils import request_logger
from health_product.insurer import custom_utils
from django.utils import timezone
import logging
logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.transport.http').setLevel(logging.DEBUG)


# TEST CREDENTIALS
#USER_ID = 'webservice@coverfox.com'
#PASSWORD  = 'newpas12'
#IMD_CODE = '10000006'
#SUB_IMD_CODE = '9906'
#CALLER_ID = 'WEBSERVICE'
#END_POINT = 'http://webservices.bajajallianz.com:8001/bjazHealthInsurance/bjazHealthInsurancePort'
#WSDL_URL = 'http://webservices.bajajallianz.com:8001/bjazHealthInsurance/bjazHealthInsurancePort?WSDL'
#PAYMENT_URL = 'http://uat.bajajallianzonline.co.in/Insurance/health/new_cc_payment.jsp'
# LOC_CHOICES = {
#    'care' : 9906,
#    'health_guard' : 9906
#}


#=============
# PRODUCTION
USER_ID = 'webservice@coverfox.com'
PASSWORD = 'coverfox'
IMD_CODE = '10038791'
SUB_IMD_CODE = '1901'
CALLER_ID = 'WEBSERVICE'
END_POINT = 'http://webservices.bajajallianz.com/bjazHealthInsurance/bjazHealthInsurancePort'
WSDL_URL = 'http://webservices.bajajallianz.com/bjazHealthInsurance/bjazHealthInsurancePort?WSDL'
PAYMENT_URL = 'http://general.bajajallianz.com/Insurance/health/new_cc_payment.jsp'
LOC_CHOICES = {
    'care': 9906,
    'health_guard': 9906
}
#=============


PRODUCT_CHOICES = {
    'care': 8416,
    'health_guard': 6001
}
PRODUCT_ID = {
    'care': 8416,
    'health_guard': 6001
}
SALUTATION_CHOICES = {
    1: 'Mr.',
    2: 'Ms.'
}
PRODUCT_DESC_CHOICES = {
    'care': 'Health Extra Care',
    'health_guard': 'Family Floater Health Guard'
}

CPTYPE_CHOICES = {
    'care': 'P',
    'health_guard': 'P'
}

RELATION_CHOICES = {
    'self': 'SELF',
    'spouse': 'SPOUSE',
    'father': 'FATHER',
    'mother': 'MOTHER'
}

RELATION_KID_CHOICES = {
    1: 'CHILD1',
    2: 'CHILD2',
    3: 'CHILD3',
    4: 'CHILD4'
}
GENDER_CHOICES = {
    1: 'M',
    2: 'F'
}
FAMILY_FLAG_CHOICES = {
    'individual': 'N',
    'floater': 'Y'
}


class Processor():

    def __init__(self, transaction):
        self.transaction = transaction
        self.product_type = ''
        self.THIS_SUM_INSURED_TYPE = ''
        if self.transaction.slab.health_product.id in [41]:
            self.product_type = 'health_guard'
            self.THIS_SUM_INSURED_TYPE = 'individual'
        elif self.transaction.slab.health_product.id in [46]:
            self.product_type = 'health_guard'
            self.THIS_SUM_INSURED_TYPE = 'floater'

    def get_processed_data_common(self):
        pRequestid_inout = ''

        client = Client(WSDL_URL)
        client.set_options(port='bjazHealthInsurancePort')
        print client

        pdata = self.transaction.insured_details_json[
            'form_data']['proposer']['personal']
        address_data = self.transaction.insured_details_json[
            'form_data']['proposer']['address']
        # contact_data = self.transaction.insured_details_json[
        #     'form_data']['proposer']['contact']

        if 'date_of_birth' in pdata.keys():
            self.transaction.proposer_date_of_birth = datetime.datetime.strptime(
                pdata['date_of_birth']['value'], '%d/%m/%Y')
        if 'gender' in pdata.keys():
            self.transaction.proposer_gender = int(pdata['gender']['value'])
        self.transaction.save()
        prop_occupation = pdata['occupation']['value']

        copay = 'N'
        deductible = ''
        deductible_discount = ''
        copay = pdata['copay']['value']

        deductible = pdata['deductible'].get('value1', '')
        deductible_discount = pdata['deductible']['dependant_value']

        pProdDtls_inout = client.factory.create('ns0:BjazWsProductDtlsIhgUser')
        pProdDtls_inout.userId = USER_ID
        pProdDtls_inout.productCode = PRODUCT_CHOICES[self.product_type]
        pProdDtls_inout.locCode = LOC_CHOICES[self.product_type]
        #pProdDtls_inout.termStartDate = datetime.datetime.strftime((datetime.datetime.today() + relativedelta(days=3)).date(), '%d-%b-%Y')
        #pProdDtls_inout.termEndDate = datetime.datetime.strftime((datetime.datetime.today() + relativedelta(days=2, years=1)).date(), '%d-%b-%Y')
        pProdDtls_inout.termStartDate = datetime.datetime.strftime(
            datetime.datetime.today().date(), '%d-%b-%Y')
        pProdDtls_inout.termEndDate = datetime.datetime.strftime(
            (datetime.datetime.today() + relativedelta(days=-1, years=1)).date(), '%d-%b-%Y')
        pProdDtls_inout.contractId = ''
        pProdDtls_inout.policyRef = ''
        pProdDtls_inout.polIssueDate = ''
        pProdDtls_inout.imdCode = IMD_CODE
        pProdDtls_inout.subImdCode = SUB_IMD_CODE
        pProdDtls_inout.spCondition = ''
        pProdDtls_inout.busType = ''
        pProdDtls_inout.ruralYN = ''
        pProdDtls_inout.productId = PRODUCT_ID[self.product_type]
        pProdDtls_inout.productDescription = PRODUCT_DESC_CHOICES[
            self.product_type]
        pProdDtls_inout.paymentMode = 'CC'
        pProdDtls_inout.familyFlag = FAMILY_FLAG_CHOICES[
            self.THIS_SUM_INSURED_TYPE]
        pProdDtls_inout.systemIp = ''
        pProdDtls_inout.returnPathUrl = '%s/health-plan/4/transaction/%s/response/' % (
            settings.SITE_URL, self.transaction.transaction_id)
        pProdDtls_inout.extraColumn1 = copay
        pProdDtls_inout.extraColumn2 = deductible
        pProdDtls_inout.extraColumn3 = deductible_discount
        pProdDtls_inout.extraColumn4 = ''
        pProdDtls_inout.extraColumn5 = ''

        # Insurer Array
        pMemDtlsList_inout = client.factory.create(
            'ns0:BjazWsMemDtlsObjUserArray')
        ass_list = []

        # Nominee Array
        pMemAssgList_inout = client.factory.create(
            'ns0:BjazWsPaAssigneeObjUserArray')
        member_list = []

        kid_counter = 1
        for code, fdata in self.transaction.insured_details_json['form_data']['insured'].items():
            ndata = self.transaction.insured_details_json[
                'form_data']['nominee'][code]
            member = [m for m in self.transaction.extra[
                'members'] if m['id'] == code][0]

            ins_occupation = fdata['occupation']['value']
            ins_weight = fdata['weight']['value']
            feet = fdata['height']['value1']
            inches = fdata['height']['value2']
            if not inches:
                inches = 0
            ins_height = ((int(feet) * 30.48) + int(inches) * 2.54)
            ins_smoking_habit = fdata['%s_smoking' % code]['value']

            memObj = client.factory.create('ns0:BjazWsMemDtlsObjUser')
            memObj.insName = custom_utils.get_full_name(first_name=fdata['first_name'][
                                                        'value'], last_name=fdata['last_name']['value'])
            memObj.dateOfBirth = datetime.datetime.strftime(datetime.datetime.strptime(
                fdata['date_of_birth']['value'], '%d/%m/%Y'), '%d-%b-%Y')
            memObj.gender = GENDER_CHOICES[member['gender']]
            #memObj.age = calculate_age(datetime.datetime.strptime(i['dateOfBirth'], '%d/%m/%Y').date())
            memObj.age = relativedelta(datetime.datetime.today(), datetime.datetime.strptime(
                fdata['date_of_birth']['value'], '%d/%m/%Y').date()).years
            if code[:3] in ['son', 'dau']:
                mem_relation = RELATION_KID_CHOICES[kid_counter]
                kid_counter += 1
            else:
                mem_relation = RELATION_CHOICES[code]
            memObj.relation = mem_relation
            memObj.occupation = ins_occupation
            memObj.monthlyIncome = '0'
            memObj.sumInsured = None
            memObj.assignee = custom_utils.get_full_name(
                first_name=ndata['first_name']['value'],
                last_name=ndata['last_name']['value']
            )
            memObj.doUSmk = ins_smoking_habit
            memObj.doUSrfAsthma = 'N'
            memObj.doUSrfBldPersur = 'N'
            memObj.doUSfrHyprTen = 'N'
            memObj.memHospitledYn = 'N'
            memObj.heightM = ''
            memObj.heightCm = ins_height
            memObj.weightKg = ins_weight
            memObj.cummBonusPer = ''
            memObj.noOfChildren = ''
            #memObj.maritalStatus = ''
            memObj.cummBonus = ''
            memObj.bodyMasIndx = ''
            memObj.doUSrfDibetes = ''
            memObj.memberNo = ''
            memObj.clmLoad = ''
            memObj.otherHltDtls = ''
            memObj.param1 = ''
            memObj.param2 = ''
            memObj.param3 = ''
            memObj.premium = ''
            memObj.param4 = ''
            memObj.param5 = ''

            member_list.append(memObj)

            assObj = client.factory.create('ns0:BjazWsPaAssigneeObjUser')
            assObj.actionCode = ''
            assObj.contractId = ''
            assObj.objectIdMem = ''
            assObj.assigneeName = ''
            assObj.dob = ''
            assObj.percentage = ''
            assObj.relationship = ''
            assObj.otherRelation = ''
            assObj.address = ''
            assObj.age = ''
            assObj.field1 = ''
            assObj.field2 = ''
            assObj.field3 = ''
            assObj.field4 = ''
            assObj.field5 = ''

            ass_list.append(assObj)

        pMemDtlsList_inout = {"BjazWsMemDtlsObjUser": member_list}
        pMemAssgList_inout = {"BjazWsPaAssigneeObjUser": ass_list}

        pCustDetails_inout = client.factory.create('ns0:BjazWsCustDtlsObjUser')
        pCustDetails_inout.cpType = CPTYPE_CHOICES[self.product_type]
        pCustDetails_inout.title = SALUTATION_CHOICES[
            int(self.transaction.proposer_gender)]
        pCustDetails_inout.firstName = self.transaction.proposer_first_name
        pCustDetails_inout.middleName = self.transaction.proposer_middle_name
        pCustDetails_inout.surname = self.transaction.proposer_last_name
        pCustDetails_inout.dateOfBirth = datetime.datetime.strftime(
            self.transaction.proposer_date_of_birth, '%d-%b-%Y')
        pCustDetails_inout.sex = GENDER_CHOICES[
            int(self.transaction.proposer_gender)]
        #pCustDetails_inout.maritalstatus = ''
        pCustDetails_inout.profession = prop_occupation
        pCustDetails_inout.empstatus = ''
        pCustDetails_inout.companyname = ''
        pCustDetails_inout.nationality = 'Indian'
        pCustDetails_inout.addLine1 = self.transaction.proposer_address1[:100]
        pCustDetails_inout.addLine2 = self.transaction.proposer_address2[:100]
        pCustDetails_inout.addLine3 = address_data['city']['value']
        pCustDetails_inout.addLine4 = address_data['state']['value']
        pCustDetails_inout.country = 'India'
        pCustDetails_inout.pincode = self.transaction.proposer_pincode
        pCustDetails_inout.email = self.transaction.proposer_email
        pCustDetails_inout.telephone1 = self.transaction.proposer_landline
        pCustDetails_inout.telephone2 = ''
        pCustDetails_inout.mobile = self.transaction.proposer_mobile
        pCustDetails_inout.fax = ''
        pCustDetails_inout.delivaryOption = ''
        pCustDetails_inout.polAddLine1 = self.transaction.proposer_address1[
            :100]
        pCustDetails_inout.polAddLine2 = self.transaction.proposer_address2[
            :100]
        pCustDetails_inout.polAddLine3 = address_data['city']['value']
        pCustDetails_inout.polAddLine4 = address_data['state']['value']
        pCustDetails_inout.polPincode = self.transaction.proposer_pincode
        pCustDetails_inout.availableTime = ''
        pCustDetails_inout.institutionName = ''
        pCustDetails_inout.existingYn = ''
        pCustDetails_inout.loggedIn = ''
        pCustDetails_inout.mobileAlerts = ''
        pCustDetails_inout.emailAlerts = ''
        pCustDetails_inout.passportno = '1383jlksh90183'
        pCustDetails_inout.status1 = ''
        pCustDetails_inout.status2 = ''
        pCustDetails_inout.status3 = ''
        pCustDetails_inout.extCol1 = self.transaction.slab.sum_assured
        pCustDetails_inout.extCol2 = ''
        pCustDetails_inout.extCol3 = ''

        pQuestionariesObj_inout = client.factory.create(
            'ns0:WeoRecStrings20User')
        pQuestionariesObj_inout.stringval1 = 0
        pQuestionariesObj_inout.stringval2 = 0
        pQuestionariesObj_inout.stringval3 = 0
        pQuestionariesObj_inout.stringval4 = 0
        pQuestionariesObj_inout.stringval5 = 0
        pQuestionariesObj_inout.stringval6 = 0
        pQuestionariesObj_inout.stringval7 = 0
        pQuestionariesObj_inout.stringval8 = 0
        pQuestionariesObj_inout.stringval9 = 0
        pQuestionariesObj_inout.stringval10 = 0
        pQuestionariesObj_inout.stringval11 = 0
        pQuestionariesObj_inout.stringval12 = 0
        pQuestionariesObj_inout.stringval13 = 0
        pQuestionariesObj_inout.stringval14 = 0
        pQuestionariesObj_inout.stringval15 = 0
        pQuestionariesObj_inout.stringval16 = 0
        pQuestionariesObj_inout.stringval17 = 0
        pQuestionariesObj_inout.stringval18 = 0
        pQuestionariesObj_inout.stringval19 = 0
        pQuestionariesObj_inout.stringval20 = 0

        pExtraObj_inout = client.factory.create('ns0:WeoRecStrings40User')
        pExtraObj_inout.stringval1 = 'COVERFOX'
        pExtraObj_inout.stringval2 = ''
        pExtraObj_inout.stringval3 = ''
        pExtraObj_inout.stringval4 = ''
        pExtraObj_inout.stringval5 = ''
        pExtraObj_inout.stringval6 = ''
        pExtraObj_inout.stringval7 = ''
        pExtraObj_inout.stringval8 = ''
        pExtraObj_inout.stringval9 = ''
        pExtraObj_inout.stringval10 = ''
        pExtraObj_inout.stringval11 = ''
        pExtraObj_inout.stringval12 = ''
        pExtraObj_inout.stringval13 = ''
        pExtraObj_inout.stringval14 = ''
        pExtraObj_inout.stringval15 = ''
        pExtraObj_inout.stringval16 = ''
        pExtraObj_inout.stringval17 = ''
        pExtraObj_inout.stringval18 = ''
        pExtraObj_inout.stringval19 = ''
        pExtraObj_inout.stringval20 = ''
        pExtraObj_inout.stringval21 = ''
        pExtraObj_inout.stringval22 = ''
        pExtraObj_inout.stringval23 = ''
        pExtraObj_inout.stringval24 = ''
        pExtraObj_inout.stringval25 = ''
        pExtraObj_inout.stringval26 = ''
        pExtraObj_inout.stringval27 = ''
        pExtraObj_inout.stringval28 = ''
        pExtraObj_inout.stringval29 = ''
        pExtraObj_inout.stringval30 = ''
        pExtraObj_inout.stringval31 = ''
        pExtraObj_inout.stringval32 = ''
        pExtraObj_inout.stringval33 = ''
        pExtraObj_inout.stringval34 = ''
        pExtraObj_inout.stringval35 = ''
        pExtraObj_inout.stringval36 = ''
        pExtraObj_inout.stringval37 = ''
        pExtraObj_inout.stringval38 = ''
        pExtraObj_inout.stringval39 = ''
        pExtraObj_inout.stringval40 = ''

        pErrorCode_out = ''

        pError_out = client.factory.create('ns0:WeoTygeErrorMessageUserArray')
        errorObj = client.factory.create('ns0:WeoTygeErrorMessageUser')
        errorObj.errNumber = 0
        errorObj.parName = ''
        errorObj.property = ''
        errorObj.errText = ''
        errorObj.parIndex = ''
        errorObj.errLevel = ''
        pError_out = {'WeoTygeErrorMessageUser': [errorObj]}

        pPremiumDtls_inout = client.factory.create(
            'ns0:BjazWsPremiumDtlsObjUser')
        # print pPremiumDtls_inout
        pPremiumDtls_inout.serviceTax = ''
        pPremiumDtls_inout.loadingPer = ''
        pPremiumDtls_inout.param1 = ''
        pPremiumDtls_inout.discPer = ''
        pPremiumDtls_inout.param2 = ''
        pPremiumDtls_inout.specialDiscount = ''
        pPremiumDtls_inout.param3 = 'Some'
        pPremiumDtls_inout.param4 = ''
        pPremiumDtls_inout.prodId = ''
        pPremiumDtls_inout.totalPremium = ''
        pPremiumDtls_inout.stampDuty = ''
        pPremiumDtls_inout.staffDisc = ''
        pPremiumDtls_inout.basicPrem = ''
        pPremiumDtls_inout.educationCess = ''

        data = {
            'pProdDtls_inout': pProdDtls_inout,
            'pCustDetails_inout': pCustDetails_inout,
            'pMemDtlsList_inout': pMemDtlsList_inout,
            'pMemAssgList_inout': pMemAssgList_inout,
            'pPremiumDtls_inout': pPremiumDtls_inout,
            'pRequestid_inout': pRequestid_inout,
            'pQuestionariesObj_inout': pQuestionariesObj_inout,
            'pExtraObj_inout': pExtraObj_inout,
            'pError_out': pError_out,
            'pErrorCode_out': pErrorCode_out
        }

        return data

    def get_premium(self):
        data = self.get_processed_data_common()
        client = Client(WSDL_URL)
        client.set_options(port='bjazHealthInsurancePort')
        print client
        resp = client.service.computePremWrapper(
            USER_ID,
            PASSWORD,
            CALLER_ID,
            data['pProdDtls_inout'],
            data['pCustDetails_inout'],
            data['pMemDtlsList_inout'],
            data['pMemAssgList_inout'],
            data['pPremiumDtls_inout'],
            data['pRequestid_inout'],
            data['pError_out'],
            data['pErrorCode_out'],
        )
        print resp
        return resp


def post_transaction(transaction):
    redirect_url = '/health-plan/product/transaction/%s/failure/' % transaction.transaction_id
    is_redirect = True
    template_name = ''
    data = {}

    obj = Processor(transaction)
    data = obj.get_processed_data_common()

    # try:
    if True:
        client = Client(WSDL_URL)
        client.set_options(port='bjazHealthInsurancePort')
        resp = client.service.computePremWrapper(
            USER_ID,
            PASSWORD,
            CALLER_ID,
            data['pProdDtls_inout'],
            data['pCustDetails_inout'],
            data['pMemDtlsList_inout'],
            data['pMemAssgList_inout'],
            data['pPremiumDtls_inout'],
            data['pRequestid_inout'],
            data['pError_out'],
            data['pErrorCode_out'],
        )
        print 'RESPONSE', resp

        request_logger.info('\nBAJAJ TRANSACTION: %s \n XML REQUEST SENT:\n %s \n' % (
            transaction.transaction_id, client.last_sent()))
        request_logger.info('\nBAJAJ TRANSACTION: %s \n XML RESPONSE RECEIVED:\n %s \n' % (
            transaction.transaction_id, client.last_received()))

        transaction.policy_request = client.last_sent()
        transaction.payment_request = client.last_sent()
        transaction.policy_response = client.last_received()
        transaction.save()

        if resp['pError_out']:
            print ">>>>>>>", resp['pError_out']['WeoTygeErrorMessageUser']
            error_list = []
            for em in resp['pError_out']['WeoTygeErrorMessageUser']:
                print "EMMMMM", em
                error_dict = {
                    'errNumber': em.errNumber,
                    'parName': em.parName,
                    'property': em.property,
                    'errText': em.errText,
                    'parIndex': em.parIndex,
                    'errLevel': em.errLevel,
                }
                error_list.append(error_dict)
            transaction.extra['premium_response'] = error_list
            transaction.save()
        else:
            transaction.premium_paid = resp['pPremiumDtls_out']['totalPremium'] or ''
            transaction.reference_id = str(resp['pRequestid_out'])
            transaction.save()

            redirect_url = 'http://general.bajajallianz.com/Insurance/health/validateRequestID.do'
            is_redirect = False
            data = {
                'requestId': transaction.reference_id,
                'username': USER_ID
            }

        return is_redirect, redirect_url, template_name, data
    # except urllib2.HTTPError as err:
    else:
        return is_redirect, redirect_url, '', {}


def post_payment(payment_response, transaction):
    """
    http://demo.coverfox.com/health-plan/4/transaction/abe2b262af4511e3bd72f23c91ae8aaa/response/?policy_no=OG-14-1901-6001-00012806&request_id=16961&p_pay_status=Y
    Final Response : {u'p_pay_status': u'Y', u'policy_no': u'OG-14-1901-6001-00012806', u'request_id': u'16961'}
    """
    redirect_url = '/health-plan/payment/transaction/%s/failure/' % transaction.transaction_id
    is_redirect = True
    print payment_response
    transaction.extra['payment_response'] = payment_response
    transaction.payment_response = payment_response
    transaction.save()

    if transaction.extra['payment_response']['p_pay_status'] == 'Y':
        transaction.policy_token = payment_response['policy_no']
        transaction.policy_number = payment_response['policy_no']
        transaction.payment_done = True
        transaction.payment_on = timezone.now()
        transaction.policy_start_date = timezone.now()
        transaction.policy_end_date = timezone.now() + relativedelta(days=-1, years=1)
        transaction.save()
        redirect_url = '/health-plan/payment/transaction/%s/success/' % transaction.transaction_id
    return is_redirect, redirect_url, '', {}
