from health_product.insurer import settings as insurer_settings
from health_product.models import *
from wiki.models import *
import datetime
from dateutil.relativedelta import *

GENDER_OPTIONS = [
    {'id': 1, 'name': 'Male'},
    {'id': 2, 'name': 'Female'}
]

TITLE_OPTIONS = [
    {'id': 'MR', 'name': 'Mr.'},
    {'id': 'MISS', 'name': 'Miss.'},
    {'id': 'MRS', 'name': 'Mrs.'},
    {'id': 'MASTER', 'name': 'Master'},
    {'id': 'BABY', 'name': 'Baby Girl'},
    {'id': 'MASTER', 'name': 'Baby Boy'},
    {'id': 'MR', 'name': 'Mr.'},
    {'id': 'MISS', 'name': 'Miss.'},
    {'id': 'MRS', 'name': 'Mrs.'}
]

RELATIONSHIP_OPTIONS = [
    {'id': '1', 'name': 'Self'},
    {'id': '2', 'name': 'Child'},
    {'id': '4', 'name': 'Father'},
    {'id': '5', 'name': 'Mother'},
    {'id': '6', 'name': 'Brother'},
    {'id': '7', 'name': 'Sister'},
    {'id': '11', 'name': 'Partner'},
    {'id': '12', 'name': 'Special Concession Adult'},
    {'id': '13', 'name': 'Special Concession Child'},
    {'id': '14', 'name': 'Wife'},
    {'id': '15', 'name': 'Husband'},
    {'id': '17', 'name': 'Daughter'},
    {'id': '18', 'name': 'Father-In-Law'},
    {'id': '19', 'name': 'Mother-In-Law'},
    {'id': '20', 'name': 'Son'}
]

MARITAL_STATUS_OPTIONS = [
    {'id': 'Single', 'name': 'Single'},
    {'id': 'Married', 'name': 'Married'}
]

OCCUPATION_OPTIONS = [
    {'id': 'Service', 'name': 'Service'},
    {'id': 'Business', 'name': 'Business'},
    {'id': 'Doctor', 'name': 'Doctor'},
    {'id': 'Professor', 'name': 'Professor'},
    {'id': 'Teacher', 'name': 'Teacher'},
    {'id': 'Student', 'name': 'Student'},
    {'id': 'Housewife', 'name': 'Housewife'},
    {'id': 'Retired', 'name': 'Retired'},
    {'id': 'Unemployed', 'name': 'Unemployed'},
    {'id': 'Other', 'name': 'Other'}
]
DEDUCTIBLE_MAIN = [
    {'id': '10000', 'name': '10000'},
    {'id': '15000', 'name': '15000'},
    {'id': '25000', 'name': '25000'},
    {'id': '50000', 'name': '50000'},
    {'id': '75000', 'name': '75000'},
    {'id': '100000', 'name': '100000'},
    {'id': '150000', 'name': '150000'},
    {'id': '200000', 'name': '200000'},
    {'id': '250000', 'name': '250000'}
]

DEDUCTIBLE_DEP = [
    {'id': '10000', 'name': '10'},
    {'id': '15000', 'name': '15'},
    {'id': '25000', 'name': '17.5'},
    {'id': '50000', 'name': '20'},
    {'id': '75000', 'name': '22.5'},
    {'id': '100000', 'name': '25'},
    {'id': '150000', 'name': '27.5'},
    {'id': '200000', 'name': '30'},
    {'id': '250000', 'name': '32.5'}
]

COPAY_OPTIONS = [
    {'id': 'Y', 'name': 'Yes'},
    {'id': 'N', 'name': 'No'}
]

SMOKING_OPTIONS = [
    {'id': 'Y', 'name': 'Yes'},
    {'id': 'N', 'name': 'No'}
]
