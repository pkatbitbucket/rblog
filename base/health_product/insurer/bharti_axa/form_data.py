from wiki.models import City, Pincode
from health_product.insurer.bharti_axa import preset
from health_product.insurer import custom_utils
from collections import OrderedDict
# Broker Portal
# User Name: 154012
# Password: f04e8e65@42


class Formic():

    def __init__(self, transaction, is_self_present):
        self.transaction = transaction
        self.is_self_present = is_self_present
        self.proposer_data = {
            'pincode': transaction.extra['pincode'],
            'city': City.objects.get(id=transaction.extra['city']['id']).name,
            'state': City.objects.get(id=transaction.extra['city']['id']).state.name,
            'email': ''
        }

    def add_parent_segment(self, segment_id, tag_id, details, question_type):
        try:
            saved_data = self.transaction.insured_details_json[
                'form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id,
                       'tag': tag_id, 'details': details, 'data': []}
        row = []
        if question_type == 'MultipleChoiceField':
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='MultipleChoiceField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    details=member,
                    value='No',
                    label=details.get('question_text', ''),
                    span=4,
                    css_class='bottom-margin',
                    event=', click : $parent.root_context.stage_5.any_yes_check',
                    template_option='question'
                ))
            new_segment['data'].append(row)

        elif question_type == 'DropDownField':
            if tag_id == 'pre_existing':
                for member in self.transaction.extra['members']:
                    row.append(custom_utils.new_field(
                        saved_data=saved_data,
                        field_type='DropDownField',
                        code='%s_question_%s' % (member['id'], tag_id),
                        options=preset.ALL_PRE_EXISTING_OPTIONS,
                        details=member,
                        label=details.get('question_text', ''),
                        span=2,
                        template_option='question',
                        required=True,
                        only_if='root_context.stage_5.check_dict.get(\'%s\')' % (member[
                            'id']),
                    ))
            new_segment['data'].append(row)
        return new_segment

    def stage_1(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'personal'
        try:
            saved_data = self.transaction.insured_details_json[
                'form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id,
                       'tag': tag_id, 'details': {}, 'data': []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            label='First Name',
            code='first_name',
            span=3,
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            label='Last Name',
            code='last_name',
            span=3,
        ))
        if self.is_self_present:
            member = [m for m in self.transaction.extra[
                'members'] if m['id'] == 'self'][0]
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                span=3,
                min_age=18,
                max_age=100,
                default_age=member['age'],
            ))
        else:
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                min_age=18,
                max_age=100,
                default_age=18,
            ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Gender',
            code='gender',
            value=1 if self.transaction.extra['gender'] == 'MALE' else 2,
            options=preset.GENDER_OPTIONS,
            span=2
        ))
        new_segment['data'].append(row)
        row = []
        married = True if [m for m in self.transaction.extra[
            'members'] if m['id'] == 'spouse'] else False
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Marital Status',
            code='marital_status',
            value='MARRIED' if married or len(
                self.transaction.extra['members']) > 1 else 'SINGLE',
            options=preset.MARITAL_STATUS_OPTIONS,
            template_option='static' if married else 'dynamic',
            span=3
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Annual Income',
            code='annual_income',
            options=preset.ANNUAL_INCOME_OPTIONS,
            span=3,
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Occupation',
            code='occupation',
            options=preset.OCCUPATION_OPTIONS,
        ))
        new_segment['data'].append(row)

        stage.append(new_segment)
        return {'template': 'stage_1', 'data': stage}

    def stage_2(self):
        stage = []
        feet_range = []
        for feet in range(0, 9):
            feet_range.append({'id': feet, 'name': '%s ft' % feet})
        inch_range = []
        for inch in range(0, 12):
            inch_range.append({'id': inch, 'name': '%s inches' % inch})

        for member in self.transaction.extra['members']:
            segment_id = 'insured'
            tag_id = member['id']
            template_switch = 'dynamic'
            if tag_id == 'self':
                template_switch = 'static'
            try:
                saved_data = self.transaction.insured_details_json[
                    'form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment': segment_id,
                           'tag': tag_id, 'details': member, 'data': []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                label='First Name',
                code='first_name',
                max_length=50,
                template_option=template_switch,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                label='Last Name',
                code='last_name',
                max_length=50,
                template_option=template_switch,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                template_option=template_switch,
                span=3,
                default_age=member['age'],
                min_age=18 if member['type'] == 'adult' else 0,
                max_age=100,
            ))
            if member['person'].lower() in ['father', 'mother', 'self', 'spouse']:
                marital_template_option = 'static' if member[
                    'person'].lower() in ['self', 'spouse'] else 'dynamic'
                marital_value = 'MARRIED' if member[
                    'person'].lower() == 'spouse' else None
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='DropDownField',
                    label='Marital Status',
                    code='marital_status',
                    span=2,
                    options=preset.MARITAL_STATUS_OPTIONS,
                    template_option=marital_template_option,
                    value=marital_value,
                ))
            new_segment['data'].append(row)

            stage.append(new_segment)
        return {'template': 'stage_2', 'data': stage}

    def stage_3(self):
        stage = []

        default_relation = {
            'self': 'Spouse',
            'spouse': 'Spouse',
            'son': 'Father' if self.transaction.extra['gender'] == 'MALE' else 'Mother',
            'daughter': 'Father' if self.transaction.extra['gender'] == 'MALE' else 'Mother',
            'father': 'Son' if self.transaction.extra['gender'] == 'MALE' else 'Daughter',
            'mother': 'Son' if self.transaction.extra['gender'] == 'MALE' else 'Daughter'
        }
        self_relation_probables = OrderedDict([
            ('mother', 'Mother'),
            ('father', 'Father'),
            ('spouse', 'Spouse')
        ])
        # Needs to run before the big loop on members
        for member in self.transaction.extra['members']:
            if member['person'] in self_relation_probables.keys():
                self.transaction.extra['self_nominee'] = member['id']
                self.transaction.save()
                default_relation['self'] = self_relation_probables[
                    member['person']]

        for member in self.transaction.extra['members']:
            segment_id = 'nominee'
            tag_id = member['id']
            try:
                saved_data = self.transaction.insured_details_json[
                    'form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment': segment_id,
                           'tag': tag_id, 'details': member, 'data': []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DropDownField',
                code='relation',
                label='Relationship With %s' % member['verbose'],
                options=custom_utils.DEFAULT_NOMINEE_RELATIION_OPTIONS,
                value=default_relation[member['person']],
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                code='first_name',
                label='First Name',
                max_length=50,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                code='last_name',
                label='Last Name',
                max_length=50,
                span=3
            ))
            new_segment['data'].append(row)
            stage.append(new_segment)
        return {'template': 'stage_3', 'data': stage}

    def stage_4(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'address'
        try:
            saved_data = self.transaction.insured_details_json[
                'form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id,
                       'tag': tag_id, 'details': {}, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address1',
            label='Flat/House No, Street',
            span=12,
            css_class='bottom-margin',
            max_length=30
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address2',
            label='Area/Locality',
            span=12,
            css_class='bottom-margin',
            max_length=30,
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='landmark',
            label='Landmark',
            span=12,
            css_class='bottom-margin',
            max_length=30,
            required=False
        ))
        new_segment['data'].append(row)
        row = []
        pincode = Pincode.objects.filter(
            pincode=self.transaction.extra['pincode'])[0]
        state = pincode.state
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='SearchableDropDownField',
            code='city',
            label='City',
            options=preset.CITY_OPTIONS[state.name]
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='state',
            value=state.name,
            label='State',
            span=4,
            template_option='static'
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='pincode',
            label='Pincode',
            value=str(self.proposer_data['pincode']),
            span=4,
            template_option='static'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)

        tag_id = 'contact'
        try:
            saved_data = self.transaction.insured_details_json[
                'form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id,
                       'tag': tag_id, 'details': {}, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='email',
            label='Email',
            span=12,
            css_class='bottom-margin',
            email=True
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MobileField',
            code='mobile',
            label='Mobile',
            css_class='bottom-margin',
            span=12
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='LandlineField',
            code='landline',
            label='Landline',
            span=12,
            css_class='bottom-margin',
            required=False
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template': 'stage_4', 'data': stage}

    def stage_5(self):
        stage = []
        #---------- Question Unit Begins -----------
        question_code = 'isIllness'
        tag_id = 'isIllness'
        segment_id = 'medical'
        details = {
            'conditional': 'NO',
            'question_text': 'Does any person, proposed to be insured, is suffering from any illness',
            'trigger_any_yes_check': True,
            'show_header': True
        }
        stage.append(self.add_parent_segment(
            segment_id, tag_id, details, 'MultipleChoiceField'))

        #---------- Question Unit Begins -----------
        question_code = 'isMedication'
        tag_id = 'isMedication'
        segment_id = 'medical'
        details = {
            'conditional': 'NO',
            'question_text': 'Does any person, proposed to be insured, under any medication?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(
            segment_id, tag_id, details, 'MultipleChoiceField'))

        #---------- Question Unit Begins -----------
        question_code = 'isHospitalized'
        tag_id = 'isHospitalized'
        segment_id = 'medical'
        details = {
            'conditional': 'NO',
            'question_text': 'Does any person, proposed to be insured, is hospitalized?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(
            segment_id, tag_id, details, 'MultipleChoiceField'))
        #---------- Question Unit Begins -----------
        question_code = 'pre_existing'
        tag_id = 'pre_existing'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': ['isHospitalized', 'isIllness', 'isMedication'],
            'question_text': 'Please specify the disease',
            'trigger_any_yes_check': False,
            'show_header': False,

        }
        stage.append(self.add_parent_segment(
            segment_id, tag_id, details, 'DropDownField'))

#        stage = []
#
#        segment_id = 'medical'
#        tag_id = 'combo'
#        try:
#            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
#        except KeyError:
#            saved_data = {}
#        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}
#
#        row = []
#        row.append(custom_utils.new_field(
#            saved_data=saved_data,
#            field_type='MultipleChoiceField',
#            code='medical_condition',
#            label='Have you suffered / are suffering from any disease, illness or medical condition?',
#            span=12,
#            css_class='bottom-margin',
#            template_option='question'
#        ))
#        new_segment['data'].append(row)
#        row = []
#        row.append(custom_utils.new_field(
#            saved_data=saved_data,
#            field_type='MultipleChoiceField',
#            code='hospitalization',
#            label='Have you been hospitalized as an in-patient in the last 4 years?',
#            span=12,
#            css_class='bottom-margin',
#            template_option='question'
#        ))
#        new_segment['data'].append(row)
#
#        stage.append(new_segment)
        return {'template': 'stage_5', 'data': stage}

    def stage_6(self):
        stage = []
        segment_id = 'terms'
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json[
                'form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id,
                       'tag': tag_id, 'details': {}, 'data': []}
        row = []
        t_and_c_label = ('I hereby confirm<br />'
                         '- I have read and agree to the Terms and Conditions of ')
        if self.transaction.slab.health_product.policy_wordings:
            t_and_c_label += ('the <a style="text-decoration:none;cursor:pointer;" '
                              'onclick=\'window.open("%s","","width=400, height=400")\'>policy</a> and '
                              ) % self.transaction.slab.health_product.policy_wordings.url
        t_and_c_label += ('the <a style="cursor:pointer;" class="call_overlay">website</a>;<br />'
                          '- the declarations made herewith are true and correct;<br />'
                          '- appointment of Coverfox Insurance Brokers Pvt. Ltd. as my Insurance Broker, '
                          'and authorize them to represent me to Insurance Companies/Agencies for my Insurance needs.'
                          )
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='t_and_c',
            label=t_and_c_label,
            span=12,
            options=[
                {'id': 'Yes', 'name': 'Yes', 'is_correct': True},
                {'id': 'No', 'name': 'No', 'is_correct': False}
            ],
            css_class='bottom-margin',
            template_option='terms'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template': 'stage_6', 'data': stage}

    def get_form_data(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
            self.stage_6()
        ]
        return data


def get_dynamic_form(transaction):
    transaction, plan_data, proposer_data, is_self_present = custom_utils.standard_form_data_cleanup(
        transaction)

    # TOGGLES WHERE MEDICAL CASES ARE ALLOWED OR NOT
    plan_data['medical_firewall'] = True

    obj = Formic(transaction, is_self_present)
    stages = obj.get_form_data()

    terms_and_conditions = "<div style='padding:20px 40px 70px 20px;'>" \
                           "<h3>Coverfox: Terms and Conditions</h3>" \
                           "<p>Coverfox Insurance Broking Pvt. (hereinafter referred to as Coverfox) Limited operates " \
                           "the website <a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> to provide " \
                           "consumers choice and an easy way to buy Insurance " \
                           "products from multiple Insurance companies. Coverfox is not an insurance company. Coverfox " \
                           "is a licensed Insurance Broking Company holding a Broking licence from the Indian " \
                           "Insurance Regulator - Insurance Regulatory and Development authority. " \
                           "<a href='/docs/irda-licence/' target='_blank'>(View IRDA License)</a></p>" \
                           "<p>Our participating insurance providers supply some of the information available on the " \
                           "Website and therefore there may be inaccuracies in the Website Information over which " \
                           "Coverfox has limited control.</p>" \
                           "<p>Coverfox does not warrant or guarantee the: Timeliness, accuracy or completeness of " \
                           "the Website Information; or Quality of the results obtained from the use of the Website.</p>" \
                           "<p>To the maximum extent permitted by law, Coverfox has no liability in relation to or " \
                           "arising out of the Website Information and Website recommendations. You are responsible " \
                           "for the final choice of your product and you should take time to read through all " \
                           "information supplied before proceeding. If you are in any doubt regarding a product or " \
                           "its terms you should seek further advice from Coverfox or the relevant participating " \
                           "provider before choosing your product.</p>" \
                           "<p>Coverfox may pass on your personal information to the relevant participating provider " \
                           "if you apply to purchase a product through " \
                           "<a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> , however, Coverfox does " \
                           "not guarantee when or if you will actually acquire the product that you have chosen. " \
                           "Coverfox does not accept any liability arising out of circumstances where there is delay " \
                           "in you acquiring the product you have chosen.</p>" \
                           "<p>Please note that Coverfox is only collecting or assisting in collecting  the premium " \
                           "deposit on behalf of the insurer you have chosen to buy the policy. The acceptance of " \
                           "the deposit as premium and final issuance of the policy is subject to the underwriting " \
                           "norms and discretion of the Insurer whose policy you have chosen to buy on which Coverfox " \
                           "has no control. Coverfox will ensure that the amount is refunded by the insurer in case " \
                           "there is no ultimate issuance of policy.</p>" \
                           "<p><button class='btn btn-lg cf-btn btn-red hide_overlay'>Close</button></p>" \
                           "</div>"

    slate_list = [
        {'id': 'slate_1', 'name': 'Proposer', 'class': '',
            'description': 'The person who is paying for the policy'},
        {'id': 'slate_2', 'name': 'Insured Members', 'class': '',
            'description': 'People being insured under the policy'},
        {'id': 'slate_3', 'name': 'Nominee', 'class': '',
            'description': 'Nominees for the insured members'},
        {'id': 'slate_4', 'name': 'Contact Info.', 'class': '',
            'description': 'Contact Information of the proposer'},
        {'id': 'slate_5', 'name': 'Medical History', 'class': '',
            'description': 'Medical History of the people being insured'},
        {'id': 'slate_6', 'name': 'T & C', 'class': 'last',
            'description': 'Mandatory terms and conditions', 'style': " 'width', '0px' "}
    ]
    data = {
        'slate_list': slate_list,
        'proposer_data': proposer_data,
        'plan_data': plan_data,
        'stages': stages,
        'last_stage': stages[-1]['template'],
        'terms_and_conditions': terms_and_conditions
    }

    return data
