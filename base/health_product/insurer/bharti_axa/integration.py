from health_product.models import Transaction
import requests
import json
import os
import settings
from django.core.files.base import File
from health_product import prod_utils
from utils import request_logger
import datetime
from dateutil.relativedelta import relativedelta
from lxml import etree
from pytz import timezone
import httplib2
import re
import xml.etree.ElementTree as ET
import mail_utils
from collections import OrderedDict
from . import preset


if settings.PRODUCTION:
    #WSDL_URL = 'https://www.smartlinkxr.bharti-axagi.co.in/cordys/WSDLGateway.wcp?service=http%3A%2F%2Fschemas.cordys.com%2Fgateway%2FProvider/serve&organization=o%3DAXA-IN%2Ccn%3Dcordys%2Ccn%3DCordysBOP4PRD%2Co%3Daxa_sgp.axa-ap.intraxa'
    WSDL_URL = 'https://www.smartlinkxr.bharti-axagi.co.in/cordys/com.eibus.web.soap.Gateway.wcp?organization=o=AXA-IN,cn=cordys,cn=CordysBOP4PRD,o=axa_sgp.axa-ap.intraxa'
    PAYMENT_URL = 'https://www.smartlinkxr.bharti-axagi.co.in/cordys/bagi/eSuite/B2C/payMe.aspx'
    HOST = 'https://www.smartlinkxr.bharti-axagi.co.in/'
    USERNAME = 'coverFoxEhl'
    PASSWORD = 'dTK++/8bSQvh3hxIfAosJEzMArbuE+qeNfp5/6u5m/HDAd8uRUansdr3NAOUdJYnDzjsD4EEBfY3AtYluEVddQ=='
    PDF_URL = HOST + '/cordys'
else:
    WSDL_URL = 'https://www.uat.bhartiaxaonline.co.in/cordys/WSDLGateway.wcp?service=http://schemas.cordys.com/gateway/Provider/serve&organization=o=B2C,cn=cordys,cn=defaultInst106,o=mydomain.com'
    PAYMENT_URL = 'https://www.uat.bhartiaxaonline.co.in/cordys/bagi/eSuite/B2C/payMe.aspx'
    HOST = 'http://www.smartlinkxruat.bharti-axagi.co.in'
    PDF_URL = HOST + '/cordys'
    USERNAME = 'coverFoxEhl'
    PASSWORD = 'AZg3Q1SktWKLz0Os/H0MlAxFfI75pjnpKjn9xrV9vimyyS7/5Ilil/ftcP5oHxC7IFYLVF0C3MAJcIznwrZvDA=='

productCodes = {
    140: 'BIH',
    142: 'BIH',
    150: 'BIH',
    153: 'BIH',
    481: 'ESC',
    486: 'ESC',
    141: 'IHS',
    151: 'IHS',
}


def get_salut(transaction):
    members = transaction.extra['members']
    insured = transaction.insured_details_json['form_data']['insured']
    salut = {}
    for m in members:
        if m['gender'] == 1:
            salut[m['id']] = 'MR'
        else:
            try:
                married = False if insured[m['id']][
                    'marital_status']['value'] == 2 else True
            except Exception:
                married = False
            salut[m['id']] = 'MRS' if married else 'MS'
    return salut


def get_proposer_salut(transaction):
    proposer_data = transaction.insured_details_json[
        'form_data']['proposer']['personal']
    salut = ''
    married = False if proposer_data['marital_status'][
        'value'] == 'MARRIED' else True
    if proposer_data['gender']['value'] == 1:
        salut = 'MR'
    else:
        salut = 'MRS' if married == True else 'MS'
    return salut


def get_xml_formated_data(transaction, QuoteNo=None, OrderNo=None):
    familyTypeCodes = {
        '1A-1C': 'SC',
        '1A-2C': 'S2C',
        '2A-0C': 'SS',
        '2A-1C': 'SSC',
        '2A-2C': 'SS2C',
    }

    salut = get_salut(transaction)
    proposer_salut = get_proposer_salut(transaction)

    date_format1 = '%a, %d %b %Y %H:%M:%S GMT'
    date_format2 = '%Y-%m-%dT%H:%M:%S.%f'

    initTime = datetime.datetime.now(timezone('GMT'))

    policyStart = datetime.datetime.today() + relativedelta(days=1)
    policyStartDate = policyStart.replace(
        hour=0, minute=0, second=0, microsecond=0)
    policyEnd = policyStart + relativedelta(years=1, days=-1)
    policyEndDate = policyEnd.replace(
        hour=23, minute=59, second=59, microsecond=0)

    plan_data = transaction.extra['plan_data']
    form_data = transaction.insured_details_json['form_data']
    if plan_data.get('category'):
        familyType = familyTypeCodes[plan_data['category']]
    else:
        familyType = 'S'

    proposer_personal = form_data['proposer']['personal']
    proposer_address = form_data['proposer']['address']
    proposer_contact = form_data['proposer']['contact']

    insured = form_data['insured']
    medical = form_data['medical']
    memberDetails = []
    primaryInsuredDetails = ''

    for k in insured:
        member = {}
        member['FirstName'] = insured[k]['first_name']['value']
        member['LastName'] = insured[k]['last_name']['value']
        member['NomineeDOB'] = datetime.datetime.strptime(
            insured[k]['date_of_birth']['value'], '%d/%m/%Y').strftime('%Y-%m-%d')
        member['IsIllness'] = 'Y' if medical['isIllness'][
            k + '_question_isIllness']['value'].lower() == 'yes' else 'N'
        member['IsHospitalized'] = 'Y' if medical['isHospitalized'][
            k + '_question_isHospitalized']['value'].lower() == 'yes' else 'N'
        member['IsMedication'] = 'Y' if medical['isMedication'][
            k + '_question_isMedication']['value'].lower() == 'yes' else 'N'
        member['ProposerRelationCode'] = k
        member['Salut'] = salut[k]
        memberDetails.append({'Member': member})

    isIllness = 'N'
    isHospitalized = 'N'
    isMedication = 'N'
    primaryInsuredDetails = {}
    primaryInsuredDetails[
        'IsPrimaryInsured'] = 'Y' if 'self' in insured else 'N'
    if primaryInsuredDetails['IsPrimaryInsured'] == 'Y':
        primaryInsuredDetails['FirstName'] = insured[
            'self']['first_name']['value']
        primaryInsuredDetails['RelationShipWithInsured'] = 'self'
        IsIllness = 'Y' if medical['isIllness'][
            'self_question_isIllness']['value'].lower() == 'yes' else 'N'
        IsHospitalized = 'Y' if medical['isHospitalized'][
            'self_question_isHospitalized']['value'].lower() == 'yes' else 'N'
        IsMedication = 'Y' if medical['isMedication'][
            'self_question_isMedication']['value'].lower() == 'yes' else 'N'
    data_dict = OrderedDict([
        ('SessionData', OrderedDict([
            ('Index', '1'),
            ('InitTime', datetime.datetime.now().strftime(date_format1)),
            ('UserName', USERNAME),
            ('Password', PASSWORD),
            ('QuoteNo', QuoteNo if QuoteNo else 'NA'),
            ('OrderNo', OrderNo if OrderNo else 'NA'),
            ('Route', 'INT'),
            ('Contract', 'EHL'),
            ('Channel', 'covfEhl'),
            ('TransactionType', 'Quote'),
            ('TransactionStatus', 'Fresh'),
            ('ID', ''),
            ('UserAgentID', ''),
            ('Source', ''),
        ])),
        ('Quote', OrderedDict([
            ('AgentNumber', ''),
            ('DealerId', ''),
            ('PolicyStartDate',
             (policyStartDate.strftime(date_format2))[:-3]),
            ('PolicyEndDate',
             (policyEndDate.strftime(date_format2))[:-3]),
            ('FamilyType', familyType),
            ('TypeOfBusiness', 'NB'),
            ('SumInsured', plan_data['cover']),
            ('ProductCode', productCodes[transaction.slab_id]),
            ('ExistingPolicy', ''),
        ])),
        ('Client', OrderedDict([
            ('CityOfResidence', proposer_address['city']['value']),
            ('ClientType', 'Individual'),
            ('SurName', proposer_personal['last_name']['value']),
            ('GivName', proposer_personal['first_name']['value']),
            ('Salut', proposer_salut),
            # had to change the datetime format....
            ('CltDOB', datetime.datetime.strptime(proposer_personal[
                'date_of_birth']['value'], '%d/%m/%Y').strftime('%Y-%m-%d')),
            ('CltSex', 'M' if proposer_personal[
                'gender']['value'] == 1 else 'F'),
            ('Marryd', 'S' if proposer_personal[
                'marital_status']['value'] == 'SINGLE' else 'M'),
            ('CltAddr01', proposer_address['address1']['value']),
            ('CltAddr02', proposer_address['address2']['value']),
            ('CltAddr03',  proposer_address['landmark']['value']),
            ('City', proposer_address['city']['value']),
            ('State', preset.STATE_OPTIONS.get(proposer_address['state']['value'], proposer_address['state']['value'])),
            ('PinCode', proposer_address['pincode']['value']),
            ('MobileNo', proposer_contact['mobile']['value']),
            ('LandLineNo', proposer_contact['landline'][
                'std_code'] + "-" + proposer_contact['landline']['landline']),
            ('Occupation',  "%04d" %
             (proposer_personal['occupation']['value'])),
            ('EmailID', proposer_contact['email']['value']),
            ('TPClientRefNo', 'CFOX-%s-%s' %
             (transaction.slab.health_product.insurer_id, transaction.id)),
            ('MonthlyIncome', '0'),
            ('AnnualIncome', proposer_personal[
                'annual_income']['value']),
            ('LoanEMI', 0),
            ('Investments', 0),
            ('Childrenfees', 0),
            ('UtilityPayments', 0),
            ('ProvisionParents', 0),
            ('ProvisionExpenses', 0),
            ('MemberDetails', memberDetails),
            ('FinancierDetails', {
                'IsFinanced': 0,
                'InstitutionName': '',
                'InstitutionCity': '',
            }),
            ('ClientExtraTag01', ''),
            ('ClientExtraTag02', ''),
            ('ClientExtraTag03', ''),
            ('ClientExtraTag04', ''),
            ('ClientExtraTag05', ''),
            ('PrimaryInsuredDetails', primaryInsuredDetails),
            ('IsIllness', isIllness),
            ('IsHospitalized', isHospitalized),
            ('IsMedication', isMedication),
        ])),
        ('Payment', {
            'PaymentMode': '',
            'PaymentType': '',
            'TxnReferenceNo': '',
            'TxnAmount': '',
            'TxnDate': '',
            'BankCode': '',
                        'InstrumentAmount': '',
        })
    ])
    return data_dict


def get_soap_request_response(session_etree):

    SessionDoc = etree.Element('SessionDoc')
    SessionDoc.append(session_etree)
    serve = etree.Element(
        'serve', xmlns='http://schemas.cordys.com/gateway/Provider')
    serve.append(SessionDoc)
    Body = etree.Element('Body')
    Body.append(serve)
    Envelope = etree.Element(
        'Envelope', xmlns='http://schemas.xmlsoap.org/soap/envelope/')
    Envelope.append(Body)
    request = etree.tostring(Envelope)
    http = httplib2.Http()
    headers = {"Content-type": "text/xml"}
    response_header, response_content = http.request(
        WSDL_URL, method='POST', body=request, headers=headers)

    return response_header, response_content


def post_transaction(transaction):

    try:
        # Quote request
        quote_data = get_xml_formated_data(transaction)
        quote_data['SessionData']['Index'] = '1'
        Session = etree.Element('Session')
        Session = prod_utils.dict_to_etree(Session, quote_data)
        request_logger.info('\nBHARTI AXA TRANSACTION: %s \n QUOTE REQUEST SENT:\n %s \n' % (
            transaction.transaction_id, ET.tostring(Session)))
        quote_resp_header, quote_resp_content = get_soap_request_response(
            Session)
        request_logger.info('\nBHARTI AXA TRANSACTION: %s \n QUOTE RESPONSE RECEIVED:\n %s \n' % (
            transaction.transaction_id, quote_resp_content))

        # Proposal Request
        quote_resp_content = re.sub(r' xmlns="(.*?)"', '', quote_resp_content)
        quote_resp_dict = prod_utils.complex_etree_to_dict(
            ET.fromstring(quote_resp_content))
        quote_data_dict = quote_resp_dict['Envelope']['Body']['serveResponse']['tuple']['old']['serve']['serve'][
            '{http://schemas.xmlsoap.org/soap/envelope/}Envelope']['{http://schemas.xmlsoap.org/soap/envelope/}Body']['processTPRequestResponse']['response']

        proposal_data = get_xml_formated_data(transaction, quote_data_dict[
                                              'QuoteNo'], quote_data_dict['OrderNo'])
        proposal_data['SessionData']['Index'] = '2'
        Session = etree.Element('Session')
        Session = prod_utils.dict_to_etree(Session, proposal_data)
        request_logger.info('\nBHARTI AXA TRANSACTION: %s \n PROPOSAL REQUEST SENT:\n %s \n' % (
            transaction.transaction_id, ET.tostring(Session)))
        proposal_resp_header, proposal_resp_content = get_soap_request_response(
            Session)
        request_logger.info('\nBHARTI AXA TRANSACTION: %s \n PROPOSAL RESPONSE RECEIVED:\n %s \n' % (
            transaction.transaction_id, proposal_resp_content))

        proposal_resp_content = re.sub(
            r' xmlns="(.*?)"', '', proposal_resp_content)
        proposal_resp_dict = prod_utils.complex_etree_to_dict(
            ET.fromstring(proposal_resp_content))

        proposal_data_dict = proposal_resp_dict['Envelope']['Body']['serveResponse']['tuple']['old']['serve']['serve'][
            '{http://schemas.xmlsoap.org/soap/envelope/}Envelope']['{http://schemas.xmlsoap.org/soap/envelope/}Body']['processTPRequestResponse']['response']

        transaction.reference_id = proposal_data_dict['OrderNo']
        transaction.policy_token = proposal_data_dict['OrderNo']

        post_data = {
            'OrderNo': proposal_data_dict['OrderNo'],
            'QuoteNo': proposal_data_dict['QuoteNo'],
            'Channel': proposal_data_dict['Session']['SessionData']['Channel'],
            'Product': 'EHL',    # always send 'EHL' for Health Products
            'Amount': transaction.extra['plan_data']['premium'] if settings.PRODUCTION else '1',
            'IsMobile': 'N',
        }
        transaction.payment_request = post_data
        transaction.premium_paid = post_data['Amount']
        transaction.save()
        is_redirect = False
        return is_redirect, PAYMENT_URL, "health_product/post_transaction_data.html", post_data
    except Exception as E:
        request_logger.info('\n === BHARTI AXA ERROR: %s === \n%s\n' % (
            transaction.transaction_id, E))
        redirect_url = '/health-plan/product/transaction/%s/failure/' % transaction.transaction_id
        return True, redirect_url, '', {}


def post_payment(payment_response, transaction=None):
    """
    success - payment & policy creation are success:
    productID=""&orderNo=""&amount=""&status=""&txnRefNo=""&ID=""

    paypass - Payment is success & policy creation is failed:
    productID=""&orderNo=""&amount=""&status=""&transactionRef=""&policyNo=""&link=""&emailId=""&ID=""

    fail - Payment is failed
    productID=""&orderNo=""&amount=""&status=""&transactionRef=""&ID=""
    """

    transaction = Transaction.objects.get(
        reference_id=payment_response['orderNo'])
    transaction.payment_response = json.dumps(payment_response)
    request_logger.info('\nBHARTI AXA TRANSACTION: %s \n PAYMENT RESPONSE RECEIVED:\n %s \n' % (
        transaction.transaction_id, payment_response))

    if payment_response['status'].lower() in ['success', 'paypass']:
        if payment_response['status'].lower() == 'paypass':
            mail_utils.health_transaction_alert(transaction, 'Generate Policy Document')
        transaction.policy_start_date = datetime.datetime.today()
        transaction.policy_end_date = datetime.datetime.today() + \
            relativedelta(days=-1, years=1)
        transaction.payment_done = True
        redirect_url = '/health-plan/payment/transaction/%s/success/' % transaction.transaction_id
        transaction.payment_done = True
        transaction.payment_on = datetime.datetime.today()
        try:
            pdf_doc = requests.get(
                PDF_URL + payment_response['link'], verify=False)

            # UPLOAD THE LOCAL FILE TO DJANGO FILE FIELD
            transaction.update_policy(pdf_doc.content, send_mail=False)
            transaction.save()
        except Exception as e:
            request_logger.info('\nBHARTI AXA INTEGRATION: %s \n EXCEPTION: \n %s \n' % (
                transaction.transaction_id, e))
    else:
        transaction.status = 'FAILED'
        redirect_url = '/health-plan/payment/transaction/%s/failure/' % transaction.transaction_id
    transaction.save()
    is_redirect = True
    return is_redirect, redirect_url, '', {}
