from health_product.insurer import custom_utils
from health_product.insurer.bharti_axa import preset

def save_stage(transaction, stage_data):
    current_stage = stage_data['template']
    red_flag = []
    form_data = custom_utils.clean_stage_data(stage_data)
    # print "FORM DATA"
    # pprint.pprint(form_data)
    for k, v in form_data.items():
        if 'form_data' not in transaction.insured_details_json.keys():
            transaction.insured_details_json['form_data'] = {}
        if k in transaction.insured_details_json['form_data'].keys():
            transaction.insured_details_json['form_data'][k].update(**v)
        else:
            transaction.insured_details_json['form_data'][k] = v
    transaction.save()
    #================== CHECK FOR MEDICAL ====================
#    for bigkey, bigval in form_data.items():
#        if bigkey.split('_question_')[0] == 'medical':
#            for code, mdata in bigval.items():
#                for k, v in mdata.items():
#                    temp = {'id':v['value'],'name':v['value']}
#                    if temp in preset.PRE_EXISTING_OPTIONS['decline']:
#                        red_flag.append({'question': v['label'], 'answer': v['value']})
#
#
    #=========================================================
    medical_dict = transaction.insured_details_json['form_data']['medical']
    main_yescheck = {}
    for key, val in medical_dict.items():
        for k, v in val.items():
            code = k.split('_question_')[0]
            yescheck = (v['value'].lower() == 'yes')
            main_yescheck[code] = (
                main_yescheck[code] | yescheck) if code in main_yescheck else yescheck

    pre_existing_dict = transaction.insured_details_json[
        'form_data']['medical_question_pre_existing']['pre_existing']

    for key, val in pre_existing_dict.items():
        code = key.split('_question_')[0]
        if(main_yescheck[code]):
            temp = {'id': val['value'], 'name': val['value']}
            if temp in preset.PRE_EXISTING_OPTIONS['decline']:
                red_flag.append(
                    {'question': val['label'], 'answer': val['value']})

    if 'red_flags' not in transaction.extra:
        transaction.extra['red_flags'] = {}
    transaction.extra['red_flags'][stage_data['template']] = {
        'reason': 'DISEASE', 'data': red_flag}
    transaction.save()
    return transaction
