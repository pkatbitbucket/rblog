from wiki.models import City, Slab
import datetime
from dateutil.relativedelta import relativedelta
import ast
from collections import OrderedDict
from health_product.insurer import custom_utils
from health_product.insurer.cigna_ttk import preset
from copy import deepcopy
from health_product.services.match_plan import MatchPlan
import health_product.queries as hqueries


def get_premium_details(data, slab_id):
    discount = {}
    for v in Formic.POLICY_DISCOUNTS:
        discount[v['id']] = float(v['name'])
    base_premiums = {}
    discounted_premiums = {}
    cpt_ids = {}

    slab = Slab.objects.get(id=slab_id)

    for i in range(1, len(discount)+1):
        members = data['members']
        for m in members:
            m['age'] = m['age'] + (i-1)

        data_to_score = hqueries.prepare_post_data_to_score(data)
        mp = MatchPlan(data_to_score, slab=slab, request=None)
        cpt = mp.get_cpt()
        base_premiums[i] = cpt.premium + base_premiums.get(i-1,0)
        discounted_premiums[i] = round(base_premiums[i] * (1 - discount[i]/100))
        cpt_ids[i] = cpt.id


    return {
            'base_premiums' : base_premiums,
            'discounted_premiums' : discounted_premiums,
            'cpt_ids' : cpt_ids,
            }

class Formic():
    POLICY_TERMS = [
            {'id' : 1, 'name' : '1 year'},
            {'id' : 2, 'name' : '2 years'},
            {'id' : 3, 'name' : '3 years'}
            ]

    POLICY_DISCOUNTS = [
            {'id' : 1, 'name' : '0'},
            {'id' : 2, 'name' : '7.5'},
            {'id' : 3, 'name' : '10'}
            ]

    def __init__(self, transaction, is_self_present):
        self.transaction = transaction
        self.is_self_present = is_self_present
        self.proposer_data = {
            'pincode': transaction.extra['pincode'],
            'city' : City.objects.get(id=transaction.extra['city']['id']).name,
            'state' : City.objects.get(id=transaction.extra['city']['id']).state.name,
            'email': ''
        }
        self.GENDER_OPTIONS = [
            {'id' : 1, 'name' : 'Male'},
            {'id' : 2, 'name' : 'Female'}
        ]
        self.MARITAL_STATUS_OPTIONS = [
            {'id' : 'MARRIED', 'name' : 'Married'},
            {'id' : 'SINGLE', 'name' : 'Single'}
        ]
        self.month_range = []
        for i in range(1, 13):
            self.month_range.append({'id' : i, 'name' : i})

    def add_conditional_segment(self, segment_id, tag_id, parent_question_code, details, question_type):
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : details, 'data' : []}
        row = []
        if question_type == 'MultipleChoiceField':
            new_segment['details']['tier'] = 1
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='MultipleChoiceField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    details=member,
                    value='No',
                    label=details.get('question_text', ''),
                    span=4,
                    required=False,
                    css_class='bottom-margin',
                    event=', click : $parent.root_context.stage_5.any_yes_check',
                    only_if='root_context.stage_5.check_dict.get(\'%s\')' % parent_question_code,
                    template_option='question'
                    ))
            new_segment['data'].append(row)
        elif question_type == 'TwoDropDownField':
            new_segment['details']['tier'] = 2
            for member in self.transaction.extra['members']:
                year_range = []
                for i in range((datetime.datetime.now() - relativedelta(years=member['age'])).year, datetime.datetime.today().year +1):
                    year_range.append({'id' : i, 'name' : i})
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='TwoDropDownField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    options1=self.month_range,
                    options2=year_range,
                    value='No',
                    label=details.get('question_text', ''),
                    span=2,
                    dependant_parent='%s_question_%s' % (member['id'], parent_question_code),
                    required=False,
                    only_if='root_context.stage_5.check_dict.get(\'%s_question_%s\')' % (member['id'], parent_question_code),
                    template_option='question'
                    ))
                new_segment['data'].append(row)
        elif question_type == 'TextField':
            new_segment['details']['tier'] = 2
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='TextField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    span=2,
                    label=details.get('question_text', ''),
                    dependant_parent='%s_question_%s' % (member['id'], parent_question_code),
                    required=False,
                    only_if='root_context.stage_5.check_dict.get(\'%s_question_%s\')' % (member['id'], parent_question_code),
                    ))
                new_segment['data'].append(row)
        return new_segment

    def add_parent_segment(self, segment_id, tag_id, details, question_type):
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': details, 'data': []}
        row = []
        if question_type == 'MultipleChoiceField':
            for member in self.transaction.extra['members']:
                kwargs = {
                    'saved_data': saved_data,
                    'field_type': 'MultipleChoiceField',
                    'code': '%s_question_%s' % (member['id'], tag_id),
                    'details': member,
                    'label': details.get('question_text', ''),
                    'value': 'No',
                    'span': 4,
                    'css_class': 'bottom-margin',
                    'event': ', click : $parent.root_context.stage_5.any_yes_check',
                    'template_option': 'question'
                }
                if details['conditional'] == 'YES':
                    kwargs['only_if'] = 'root_context.stage_5.check_dict.get(\'%s\')' % (member['id'])
                row.append(custom_utils.new_field(**kwargs))
            new_segment['data'].append(row)
        elif question_type == 'DropDownField':
            kwargs = {}
            if tag_id == 'pre_existing':
                options = self.pre_existing_options
                kwargs['event'] = ', event : {change : root_context.stage_5.any_yes_check}'
            elif tag_id == 'tobacco':
                options = [
                    {'id': 'NO', 'name': 'No'},
                    {'id': 'YES', 'name': 'Yes'}
                ]
            kwargs.update({
                'saved_data': saved_data,
                'field_type': 'DropDownField',
                'options': options,
                'label': details.get('question_text', ''),
                'span': 2,
                'css_class': 'bottom-margin',
                'template_option': 'question'
            })
            for member in self.transaction.extra['members']:
                kwargs['code'] = '%s_question_%s' % (member['id'], tag_id)
                kwargs['details'] = member
                row.append(custom_utils.new_field(**kwargs))
            new_segment['data'].append(row)
        elif question_type == 'TextField':
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='TextField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    label=details.get('question_text', ''),
                    span=2,
                    required=False,
                    ))
            new_segment['data'].append(row)

        return new_segment

    def stage_1(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'personal'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            label='First Name',
            code='first_name',
            span=3
            ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            label='Last Name',
            code='last_name',
            span=3
        ))
        if self.is_self_present:
            member = [m for m in self.transaction.extra['members'] if m['id'] == 'self'][0]
            min_date, max_date, min_age, max_age = custom_utils.get_age_values(member)
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                min_age=18,
                max_age=100,
                default_age=member['age'],
            ))
        else:
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                min_age=18,
                max_age=100,
            ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Gender',
            code='gender',
            options=preset.GENDER_MASTER,
            span=2,
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Type of Business',
            code='business_type',
            options=preset.BUSINESS_TYPE
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Marital Status',
            code='marital_status',
            options=[
                {'id': 'MARRIED', 'name': 'Married'},
                {'id': 'SINGLE', 'name': 'Single'},
                {'id': 'OTHERS', 'name': 'Others'},
                ]
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Occupation',
            code='occupation',
            options=preset.OCCUPATION_CODE
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Annual Income',
            code='annual_income',
            options=preset.ANNUAL_INCOME
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='IntegerField',
            label='Gainful Annual Income (in Rs.)',
            code='gainful_annual_income',
        ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label="Nationality",
            code="nationality",
            options=preset.NATIONALITY
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            max_length=30,
            label="Which nationality?",
            code="nationality_value",
            only_if='root_context.stage_1.is_other_nationality'
        ))
        new_segment['data'].append(row)

        if self.transaction.extra['plan_data']['premium'] > 100000:
            row = []
            row.append(custom_utils.new_field(
                saved_data = saved_data,
                field_type="PANField",
                code="pan_card_no",
                span=4,
                label="PAN Card No."
            ))
            new_segment['data'].append(row)

        premium_data = get_premium_details(deepcopy(self.transaction.extra),self.transaction.slab_id)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='PremiumDiscountField',
            code='policy_term',
            span=8,
            options1=self.POLICY_TERMS,
            options2=self.POLICY_DISCOUNTS,
            base_premiums = premium_data['base_premiums'],
            discounted_premiums = premium_data['discounted_premiums'],
            required=True,
            main_label='Policy term',
            dep_label='Discount',
            dep_label1='Premium',
            premium=self.transaction.extra['plan_data']['premium'],
        ))
        new_segment['data'].append(row)

        stage.append(new_segment)

        return {'template' : 'stage_1', 'data' : stage}


    def stage_2(self):
        stage = []
        feet_range = []
        for feet in range(0, 9):
            feet_range.append({'id' : feet, 'name' : '%s ft' % feet})
        inch_range = []
        for inch in range(0, 12):
            inch_range.append({'id' : inch, 'name' : '%s inches' % inch})

        for member in self.transaction.extra['members']:
            segment_id = 'insured'
            tag_id = member['id']
            template_switch = 'dynamic'
            if tag_id == 'self':
                template_switch = 'static'
            try:
                saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : member, \
                            'data' : []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                label='First Name',
                code='first_name',
                max_length=50,
                span=4,
                template_option=template_switch
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                label='Last Name',
                code='last_name',
                max_length=50,
                span=4,
                template_option=template_switch
            ))
            min_date, max_date, min_age, max_age = custom_utils.get_age_values(member)
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                span=4,
                template_option=template_switch,
                min_age = 18 if member['type']=='adult' else 0,
                max_age=100,
                default_age=member['age'],
            ))

            new_segment['data'].append(row)
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DropDownField',
                label='Marital Status',
                code='marital_status',
                options=[
                    {'id': 'MARRIED', 'name': 'Married'},
                    {'id': 'SINGLE', 'name': 'Single'},
                    {'id': 'OTHERS', 'name': 'Others'},
                    ],
                span=4
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='TwoDropDownField',
                label='Height',
                code='height',
                options1=feet_range,
                options2=inch_range
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='IntegerField',
                label='Weight',
                code='weight',
                sub_text='Kgs',
                max_length=3,
                span=3
            ))
            new_segment['data'].append(row)
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DropDownField',
                label='Occupation',
                code='occupation',
                options=preset.OCCUPATION_CODE
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DropDownField',
                label="Nationality",
                code="nationality",
                options=preset.NATIONALITY
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                max_length=30,
                label="Which nationality?",
                code="nationality_value",
                only_if='root_context.stage_2.has_other_nationality.get(\'%s\')' % tag_id
            ))
            new_segment['data'].append(row)
            stage.append(new_segment)
        return {'template' : 'stage_2', 'data' : stage}

    def stage_3(self):
        relation_options = [
            {'id' : 'HU', 'name' : 'Husband'},
            {'id' : 'WI', 'name' : 'Wife'},
            {'id' : 'MO', 'name' : 'Mother'},
            {'id' : 'FA', 'name' : 'Father'},
            {'id' : 'DA', 'name' : 'Daughter'},
            {'id' : 'SO', 'name' : 'Son'},
            {'id' : 'BR', 'name' : 'Brother'},
            {'id' : 'SISTER', 'name' : 'Sister'},
            {'id' : 'OTHER', 'name' : 'Other'},
        ]
        if self.transaction.extra['gender'] == 'MALE':
            relation_options.pop(0) #remove husband
        else:
            relation_options.pop(1) #remove wife

        relation_options.sort(lambda x, y: -1 if x['name'] < y['name'] else 1)

        insured_nominee_details = {'verbose': 'insured' }
        stage = []
        segment_id = 'nominee'
        tag_id = 'self'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : insured_nominee_details, 'data' : []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='TwoDropDownDependantField',
            code='relation',
            main_label='Relationship with Proposer',
            dep_label='Salutation',
            options1=relation_options,
            options2=preset.TITLES,
            span=6
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            code='first_name',
            label='First Name',
            max_length=50,
            span=3
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            code='last_name',
            label='Last Name',
            max_length=50,
            span=3
        ))
        new_segment['data'].append(row)

        stage.append(new_segment)
        return {'template' : 'stage_3', 'data' : stage}

    def stage_4(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'address'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address1',
            label='Flat/House No, Street',
            span=12,
            css_class='bottom-margin',
            max_length=50
        ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address2',
            label='Area/Locality',
            span=12,
            css_class='bottom-margin',
            max_length=100,
            ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='landmark',
            label='Landmark',
            span=12,
            css_class='bottom-margin',
            max_length=100,
            required=False
        ))
        new_segment['data'].append(row)

        row = []
        pincode = self.transaction.extra['pincode']
        location = preset.STATE_CITY_MAPPING[str(pincode)]
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='city',
            label='City',
            value=location['city'],
            span=4,
            template_option='static'
            ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='state',
            label='State',
            value=location['state'],
            span=4,
            template_option='static'
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='pincode',
            label='Pincode',
            value=str(self.proposer_data['pincode']),
            span=4,
            template_option='static'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)


        tag_id = 'contact'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='email',
            label='Email',
            span=12,
            css_class='bottom-margin',
            email=True
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MobileField',
            code='mobile',
            label='Mobile',
            css_class='bottom-margin',
            span=12
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='LandlineField',
            code='landline',
            label='Landline',
            span=12,
            css_class='bottom-margin',
            required=False
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template' : 'stage_4', 'data' : stage}

    def stage_5(self):
        stage = []

        #---------- Question Unit Begins -----------
        question_code = 'tobacco'
        segment_id = 'medical_question_%s' %question_code
        tag_id = 'tobacco'
        details = {
            'conditional': 'NO',
            'question_text': 'Do you or any of the persons proposed for Insurance, Chew Tobacco/Smoke?',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'DropDownField'))

        #---------- Question Unit Ends-------------

       #---------- Question Unit Begins -----------
        #THINGS THAT MATTER - START
        question_code = 'pre_existing'
        segment_id = 'medical_question_%s' %question_code
        tag_id = 'pre_existing'
        details = {
            'conditional': 'NO',
            'question_text': 'Have you or any of the persons proposed for insurance, ever suffered from or taken treatment, or hospitalised for or have been recommended to take investigations/medication/surgery or undergone a surgery for the following medical conditions?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        #THINGS THAT MATTER - END



        #CONDITIONAL SEGMENT
        #Conditional Question 1
        # tag_id = 'conditional_pre_existing'
        # parent_question_code = question_code
        # details = {
        #     'conditional': 'YES',
        #     'parent_question_code': parent_question_code,
        #     'question_text': 'Does any person(s) to be insured has any Pre-existing diseases?',
        #     'trigger_any_yes_check': False,
        #     'show_header': True
        # }
        # stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))


        #Conditional Question 1
        tag_id = 'conditional_blood_pressure'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'High or Low Blood Pressure / Chest Pain / Heart Disease',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #Conditional Question 2
        tag_id = 'conditional_diabetes'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Diabetes or Pre-diabetes condition',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #Conditional Question 3
        tag_id = 'conditional_arthritis'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Arthritis/Gout, Spondylosis, Joint Pain, Slip Disc, Spinal Disorder, Chronic Backache or or any other disorder of the muscle/bone/joint',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #Conditional Question 4
        tag_id = 'conditional_lung_disease'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Tuberculosis, Asthma, Bronchitis or any other lung/respiratory or ENT disorder',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #Conditional Question 5
        tag_id = 'conditional_kidney_disease'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Kidney Failure, Dialysis, Stones in kidney or urinary tract, Prostate disorder or any other kidney/ urinary tract disorder or diseases of the reproductive organs',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        #Conditional Question 6
        tag_id = 'conditional_liver_disease'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Liver Disease/Ulcers/Gall bladder disorder or any other digestive tract or gastro intestinal disorder',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #Conditional Question 7
        tag_id = 'conditional_endocrine_disorder'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Thyroid/Pituitary Disorder or any other endocrine disorder',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #Conditional Question 8
        tag_id = 'conditional_cancer_disease'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Cancer /Tumor (Swelling)-benign or malignant',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #Conditional Question 9
        tag_id = 'conditional_hiv'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'HIV / AIDS / Sexually transmitted diseases.',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #Conditional Question 10
        tag_id = 'conditional_blood_disease'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Anaemia or any other blood disorder',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #Conditional Question 11
        tag_id = 'conditional_nervous_disorder'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Dizziness, Stroke, Epilepsy, Paralysis or other brain or nervous system disorder',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #Conditional Question 12
        tag_id = 'conditional_mental_disorder'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Psychaitric, Mental Illness/Disorder',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #Conditional Question 13
        tag_id = 'conditional_females'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': '(Females) Fibroid, Cyst/ Fibroadenoma, Bleeding Disorder, Pelvic infection Or Any Other Gynaecological / Breast Disorder',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #Conditional Question 14
        tag_id = 'conditional_alcohol'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Treatment for Alcohol Abuse, narcotics, rehabilitation for overuse of drugs or detoxication therapy?',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #Conditional Question 15
        tag_id = 'conditional_external_mass'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Any external ulcer/growth/cyst/mass anywhere in the body',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #Conditional Question 16
        tag_id = 'conditional_skin'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Any type of Skin Allergies or Diseases of the Eye',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #Conditional Question 17
        tag_id = 'conditional_recent'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Any other illness/disease/injury in the past 48 months other than for childbirth,flu or for minor injuries that have completely healed',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #---------- Question Unit Begins -----------
        question_code = 'application_declined'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'application_declined'
        details = {
            'conditional': 'NO',
            'question_text': 'Has any application for life or health ever been declined, postponed, loaded or been made subject to any special conditions by CignaTTK or any insurance company?',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        #---------- Question Unit Ends-------------

        return {'template' : 'stage_5', 'data' : stage}

    def stage_6(self):
        stage = []
        segment_id = 'terms'
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='t_and_c',
            label='I have read and agree to the <a style="cursor:pointer;" class="call_overlay">'
                  'Terms and Conditions</a>. I understand that by clicking on Payment button and '
                  'making a payment, I am signing the proposal form.'
                  '<br /> And I also hereby appoint Coverfox Insurance Brokers Pvt. Ltd. as my Insurance Broker, '
                  'and authorize them to represent me to Insurance Companies/Agencies for my Insurance needs.',
            span=12,
            options=[
                {'id' : 'Yes', 'name' : 'Yes', 'is_correct' : True},
                {'id' : 'No', 'name' : 'No', 'is_correct' : False}
            ],
            css_class='bottom-margin',
            template_option='terms'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template' : 'stage_6', 'data' : stage}

    def get_form_data_family(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
            self.stage_6()
        ]
        return data

    def get_form_data_individual(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
            self.stage_6()
        ]
        return data


def get_dynamic_form(transaction):
    transaction, plan_data, proposer_data, is_self_present = custom_utils.standard_form_data_cleanup(transaction)

    #TOGGLES WHERE MEDICAL CASES ARE ALLOWED OR NOT
    plan_data['medical_firewall'] = False

    obj = Formic(transaction, is_self_present)
    stages = getattr(obj, 'get_form_data_%s' % transaction.slab.health_product.policy_type.lower(), '')()
    # proposer_data['self_nominee'] = transaction.extra['self_nominee']


    terms_and_conditions = "<div style='padding:20px 40px 70px 20px;'>" \
                           "<h3>Cigna TTK Health Insurance: Declaration & Authoraization</h3>"\
                           "<p>I/We hereby declare, on my behalf and on behalf of all persons proposed to be insured, that the above statements, answers and/ or particulars given by me are true and complete in all respects to the best of my knowledge and that I/We am/are authorized to propose on behalf of these other persons.</p>"\
                           "<p>I understand that the information provided by me will form the basis of the insurance policy, is subject to the Board approved underwriting policy of the insurance company and that the policy will come into force only after full receipt of the premium chargeable.</p>"\
                           "<p>I/We further declare that I/We will notify in writing any change occurring in the occupation or general health of the life to be insured/proposer after the proposal has been submitted but before communication of the risk acceptance by the company</p>"\
                           "<p>I/We declare and consent to the company seeking medical information from any doctor or from a hospital who at anytime has attended on the life to be insured/proposer or from any past or present employer concerning anything which affects the physical or mental health of the life to be assured/proposer and seeking information from any insurance company to which an application for insurance on the life to be assured/proposer has been made for the purpose of underwriting the proposal and/or claim settlement.</p>"\
                           "<p>I/We authorize the company to share information pertaining to my proposal including the medical records for the sole purpose of proposal underwriting and/or claims settlement and with any Government and/or Regulatory authority.</p>"\
                           "<h4>Declaration:</h4>"\
                           "<p>I hereby confirm that all premiums have been/will be paid from genuine sources and no premiums have been/will be paid out of proceeds of crime related to any of the scheduled offences listed in Prevention of Money Laundering Act, 2002. I understand that the Company has the right to call for documents to establish sources of funds. The insurance company has right to cancel the insurance contract in case I am/ have been found guilty by any competent court of law under any of the statutes, directly or indirectly governing the prevention of money laundering in India.</p>"\
                           "<h4>Disclaimer:</h4>"\
                           "<ul>"\
                           "<li>As per Regulation 3(d), IRDA (Manner of Receipt of Premium) Regulations, 2002, in case, the proposer/policyholder opts for premium payment through net banking or credit/debit card, the payment must be made only through net banking account or credit/debit card issued on the name of such proposer/policyholder.</li>"\
                           "<li>As per AML guideline KYC norm shall be carried out at the settlement stage for Claim payout /premium refund for Rs. 1 lakh and above per claim /premium refund.</li>"\
                           "<li>Company at its sole discretion may call for documents for processing the application</li>"\
                           "<li>PAN Copy/Details are mandatory where premium is Rs.1lakh and above</li>"\
                           "</ul>"\
                           "<h3>Coverfox: Terms and Conditions</h3>" \
                           "<p>Coverfox Insurance Broking Pvt. (hereinafter referred to as Coverfox) Limited operates " \
                           "the website <a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> to provide " \
                           "consumers choice and an easy way to buy Insurance " \
                           "products from multiple Insurance companies. Coverfox is not an insurance company. Coverfox " \
                           "is a licensed Insurance Broking Company holding a Broking licence from the Indian " \
                           "Insurance Regulator - Insurance Regulatory and Development authority. " \
                           "<a href='/docs/irda-licence/' target='_blank'>(View IRDA License)</a></p>" \
                           "<p>Our participating insurance providers supply some of the information available on the " \
                           "Website and therefore there may be inaccuracies in the Website Information over which " \
                           "Coverfox has limited control.</p>" \
                           "<p>Coverfox does not warrant or guarantee the: Timeliness, accuracy or completeness of " \
                           "the Website Information; or Quality of the results obtained from the use of the Website.</p>" \
                           "<p>To the maximum extent permitted by law, Coverfox has no liability in relation to or " \
                           "arising out of the Website Information and Website recommendations. You are responsible " \
                           "for the final choice of your product and you should take time to read through all " \
                           "information supplied before proceeding. If you are in any doubt regarding a product or " \
                           "its terms you should seek further advice from Coverfox or the relevant participating " \
                           "provider before choosing your product.</p>" \
                           "<p>Coverfox may pass on your personal information to the relevant participating provider " \
                           "if you apply to purchase a product through " \
                           "<a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> , however, Coverfox does " \
                           "not guarantee when or if you will actually acquire the product that you have chosen. " \
                           "Coverfox does not accept any liability arising out of circumstances where there is delay " \
                           "in you acquiring the product you have chosen.</p>" \
                           "<p>Please note that Coverfox is only collecting or assisting in collecting  the premium " \
                           "deposit on behalf of the insurer you have chosen to buy the policy. The acceptance of " \
                           "the deposit as premium and final issuance of the policy is subject to the underwriting " \
                           "norms and discretion of the Insurer whose policy you have chosen to buy on which Coverfox " \
                           "has no control. Coverfox will ensure that the amount is refunded by the insurer in case " \
                           "there is no ultimate issuance of policy.</p>" \
                           "<p><button class='btn btn-lg cf-btn btn-red hide_overlay'>Close</button></p>" \
                           "</div>"
    slate_list = [
        {'id': 'slate_1', 'name': 'Proposer', 'class' : '', 'description' : 'The person who is paying for the policy'},
        {'id': 'slate_2', 'name' : 'Insured Members', 'class' : '', 'description' : 'People being insured under the policy'},
        {'id': 'slate_3', 'name' : 'Nominee', 'class' : '', 'description' : 'Nominees for the insured members'},
        {'id': 'slate_4', 'name' : 'Contact Info.', 'class' : '', 'description' : 'Contact Information of the proposer'},
        {'id': 'slate_5', 'name' : 'Medical History', 'class' : '', 'description' : 'Medical History of the people being insured'},
        {'id': 'slate_6', 'name' : 'T & C', 'class' : 'last', 'description' : 'Mandatory terms and conditions', 'style' : " 'width', '0px' "}
    ]
    data = {
        'slate_list' : slate_list,
        'proposer_data' : proposer_data,
        'plan_data' : plan_data,
        'stages' : stages,
        'last_stage' : stages[-1]['template'],
        'terms_and_conditions' : terms_and_conditions,
        }
    return data
