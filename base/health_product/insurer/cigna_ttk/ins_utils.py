from health_product.models import *
import datetime
from openpyxl import Workbook
from django.http import HttpResponse
from integration import CAMPAIGNCD
import thread
from django.core.mail import send_mail
from collections import OrderedDict
from django.conf import settings

def download_buy_form(transaction):

    form_data = transaction.insured_details_json['form_data']
    extra_data = transaction.extra
    ppersonal = form_data['proposer']['personal']
    paddress = form_data['proposer']['address']
    pcontact = form_data['proposer']['contact']
    insured_data = form_data['insured']
    guids = transaction.extra['guids']
    stp_dict = transaction.extra['stp']
    guid_to_ppmc = transaction.extra['guid_to_ppmc']
    members_extra = {
        mem['id']: mem
        for mem in transaction.extra['members']
            }
    insured_list = []
    insured_salut = transaction.extra['insured_salut']
    for mem in insured_data:
        insured_list.append((mem, insured_data[mem]))
    nominee = form_data['nominee']['nominee'] #TODO

    header = []
    data = []
    for mem in insured_data:
        data.append([])

    def insert_policy_data(key, value):
        header.append(key)
        data[0].append(value)
        for mem_data in data[1:]:
            mem_data.append('')


    insert_policy_data('CignaProposalNo', transaction.policy_number)
    insert_policy_data('Received by ID', 'ADMINISTRATOR')
    insert_policy_data('Received By Name', 'Admin User')
    insert_policy_data('Verifier ID', '')
    insert_policy_data('Verifier Name', '')
    insert_policy_data('QC Rep ID', '')
    insert_policy_data('QC Rep Name', '')
    insert_policy_data('Call Centre ID', '')
    insert_policy_data('EMI Option', 'NO')
    insert_policy_data('Frequency', '')
    insert_policy_data('Inward Type', 'NEWBUSINESS')
    insert_policy_data('Inward Sub Type', 'PROPOSALDOCUMENT')
    insert_policy_data('Received From', 'ONLINE')
    insert_policy_data('Agent/ Broker ID', '1602702-01')
    insert_policy_data('Remarks', '')
    insert_policy_data('Policy Servicing Branch', '127')
    insert_policy_data('Policy Type', 'INDIVIDUAL' if len(insured_data) == 1 else 'FAMILYFLOATER')
    insert_policy_data('Sum Insured Option', extra_data['plan_data']['plan_option'])
    insert_policy_data('Policy Term (Year)', ppersonal['policy_term']['value1'])
    insert_policy_data('Policy Start Date', transaction.policy_start_date.strftime('%d/%m/%Y'))
    insert_policy_data('Policy End Date', transaction.policy_end_date.strftime('%d/%m/%Y'))
    insert_policy_data('Zone', extra_data['plan_data']['zone'])
    insert_policy_data('Business Type', ppersonal['business_type']['value'])
    insert_policy_data('Co-pay (per claim)', '')
    insert_policy_data('Deductible (Per policy year)', '')
    insert_policy_data('PPN', '')
    insert_policy_data('Sub Plan ID', extra_data['plan_data']['product_id'])
    insert_policy_data('Campaign Code', CAMPAIGNCD)
    insert_policy_data('AddOn1', '')
    insert_policy_data('AddOn Sum Insured Option 1', '')
    insert_policy_data('AddOn2', '')
    insert_policy_data('AddOn Sum Insured Option 2', '')
    insert_policy_data('AddOn3', '')
    insert_policy_data('AddOn Sum Insured Option 3', '')
    insert_policy_data('AddOn4', '')
    insert_policy_data('AddOn Sum Insured Option 4', '')
    insert_policy_data('AddOn5', '')
    insert_policy_data('AddOn Sum Insured Option 5', '')
    insert_policy_data('Rider1', '')
    insert_policy_data('Rider Sum Insured Option 1', '')
    insert_policy_data('Rider2', '')
    insert_policy_data('Rider Sum Insured Option 2', '')
    insert_policy_data('Rider3', '')
    insert_policy_data('Rider Sum Insured Option 3', '')
    insert_policy_data('Rider Co-pay', '')
    insert_policy_data('Rider Deductible', '')
    insert_policy_data('Quote ID', extra_data['plan_data']['quoteId'])
    insert_policy_data('Quote Amount', extra_data['plan_data']['premium'])
    insert_policy_data('Title_P', extra_data['proposer_salut'])
    insert_policy_data('DOB', ppersonal['date_of_birth']['value'])
    insert_policy_data('First Name', ppersonal['first_name']['value'])
    insert_policy_data('Middle Name', '')
    insert_policy_data('Last Name', ppersonal['last_name']['value'])
    insert_policy_data('Gender', ppersonal['gender']['display_value'].upper())
    insert_policy_data('Marital status', ppersonal['marital_status']['value'])
    insert_policy_data('Educational Qualification', '')
    insert_policy_data('Occupation', ppersonal['occupation']['value'])
    insert_policy_data('Nature of Job', '')
    insert_policy_data('Annual Gross Income (in Rs.)', ppersonal['annual_income']['value'])
    insert_policy_data('Proposer Gainful Annual Income (in Rs.)', '')
    insert_policy_data('ID Type', '')
    insert_policy_data('ID Number', '')
    insert_policy_data('Nationality', 'IND')
    insert_policy_data('Address Line 1', paddress['address1']['value'])
    insert_policy_data('Address Line 2', paddress['address2']['value'])
    insert_policy_data('Landmark', paddress['landmark']['value'])
    insert_policy_data('PropCity', paddress['city']['value'])
    insert_policy_data('District', '')
    insert_policy_data('State', paddress['state']['value'])
    insert_policy_data('Pin Code', paddress['pincode']['value'])
    insert_policy_data('Permanent Address Line 1', paddress['address1']['value'])
    insert_policy_data('Permanent Address Line 2', paddress['address2']['value'])
    insert_policy_data('Permanent Landmark', paddress['landmark']['value'])
    insert_policy_data('Permanent City', paddress['city']['value'])
    insert_policy_data('Permanent District', '')
    insert_policy_data('Permanent State', paddress['state']['value'])
    insert_policy_data('Permanent Pin Code', paddress['pincode']['value'])
    insert_policy_data('Mobile Number', pcontact['mobile']['value'])
    insert_policy_data('Residence  Number', pcontact['landline']['std_code']+pcontact['landline']['landline'])
    insert_policy_data('Office Number', '')
    insert_policy_data('Email ID1', pcontact['email']['value'])
    insert_policy_data('Receipt ID', extra_data['plan_data']['receipt_id'])
    insert_policy_data('Payment Mode', extra_data['plan_data']['payment_mode'])
    insert_policy_data('Mode of Transaction', '')
    insert_policy_data('TID', '')
    insert_policy_data('MID', '')
    insert_policy_data('Instrument/Transaction No', extra_data['plan_data']['inward_number'])
    insert_policy_data('Instrument/Transaction Date', datetime.datetime.now().strftime('%d/%m/%Y'))
    insert_policy_data('MICR Number', '')
    insert_policy_data('Instrument Pick Up Date', '')
    insert_policy_data('IFSC Code1', '')
    insert_policy_data('Instrument Amount', transaction.premium_paid)
    insert_policy_data('Clearing', '')
    insert_policy_data('Bank Name1', '')
    insert_policy_data('Branch Name', '')
    insert_policy_data('Account Number1', '')
    insert_policy_data('A/C Type', 'PROPOSAL')
    insert_policy_data('A/C Sub Type', '')
    insert_policy_data('Adjustment Amount', transaction.premium_paid)
    insert_policy_data('ECS Renewal option', 'NO')
    insert_policy_data('IFSC Code2', '')
    insert_policy_data('MICR Code', '')
    insert_policy_data('Bank Name2', '')
    insert_policy_data('Branch', '')
    insert_policy_data('City', '')
    insert_policy_data('Account Number2', '')
    insert_policy_data('Card Number', '')
    insert_policy_data('Card Expiry Date', '')
    insert_policy_data('Card Type', '')
    insert_policy_data('Name on the Card', '')
    insert_policy_data('Nominee Title', nominee['relation']['dependant_value'])
    insert_policy_data('Nominee Name', '%s %s' % (nominee['first_name']['value'], nominee['last_name']['value']))
    insert_policy_data('Relationship', nominee['relation']['value1'])
    insert_policy_data('Name', '')
    insert_policy_data('Qualification', '')
    insert_policy_data('Address', '')
    insert_policy_data('Mobile', '')
    insert_policy_data('Clinic', '')
    insert_policy_data('Email ID2', '')
    insert_policy_data('UW Required Flag - Policy Level', 'NO' if extra_data['stp']['policy'] else 'YES')
    insert_policy_data('PPMC Required Flag - Policy Level', 'YES' if 'YES' in extra_data['guid_to_ppmc'].values() else 'NO')
    insert_policy_data('Policy documents emailing option', 'YES')
    insert_policy_data('Consent ID 1', 'Consent1')
    insert_policy_data('Consent1', 'YES')
    insert_policy_data('Consent ID 2', 'Consent2')
    insert_policy_data('Consent2', 'YES')
    insert_policy_data('Consent ID 3', 'Consent3')
    insert_policy_data('Consent3', 'YES')
    insert_policy_data('Consent ID 4', 'Consent4')
    insert_policy_data('Consent4', 'YES')
    insert_policy_data('Consent ID 5', '')
    insert_policy_data('Consent5', '')

    ### Begin: INSURED_DATA ####
    header.append('Title_M')
    header.append('First Name Insured')
    header.append('Middle Name Insured')
    header.append('Last Name Insured')
    header.append('Gender Insured')
    header.append('DOB Insured')
    header.append('Relationship Insured')
    header.append('Height (cm) Insured')
    header.append('Weight (kg) Insured')
    header.append('BMI')
    header.append('Occupation Insured')
    header.append('City Insured')
    header.append('Pin Code Insured')
    header.append('Sum Insured Option Insured')
    header.append('Sum Insured Value')
    header.append('Risk Class')
    header.append('UW Required Flag - Insured Level')
    header.append('PPMC Required Flag -Insured Level')
    for i, data_row in enumerate(data):
        mem = insured_list[i][0]
        mem_data = insured_list[i][1]
        data_row.append(insured_salut[mem])#Title_M
        data_row.append(insured_data[mem]['first_name']['value'])#First Name Insured
        data_row.append('')#Middle Name Insured
        data_row.append(insured_data[mem]['last_name']['value'])#Last Name Insured
        data_row.append('MALE' if members_extra[mem]['gender'] == 1 else 'FEMALE')#Gender Insured
        data_row.append(insured_data[mem]['date_of_birth']['value'])#DOB Insured
        data_row.append(mem)#Relationship Insured
        data_row.append(insured_data[mem]['height1'])#Height (cm) Insured
        data_row.append(insured_data[mem]['weight1'])#Weight (kg) Insured
        data_row.append(insured_data[mem]['bmi'])#BMI
        data_row.append(insured_data[mem]['occupation']['value'])#Occupation Insured
        data_row.append(paddress['city']['value'])#City Insured
        data_row.append(paddress['pincode']['value'])#Pin Code Insured
        data_row.append(transaction.extra['plan_data']['plan_option'])#Sum Insured Option Insured
        data_row.append(transaction.extra['plan_data']['cover'])#Sum Insured Value
        data_row.append('')#Risk Class
        data_row.append('NO' if stp_dict[mem] else 'YES')#UW Required Flag - Insured Level
        data_row.append(guid_to_ppmc[guids[mem]])#PPMC Required Flag -Insured Level
    ### End: INSURED_DATA ####
    insert_policy_data('PPMC Test Name', '')
    insert_policy_data('Insured Gainful Annual Income (in Rs.)', ppersonal['gainful_annual_income']['value'])
    insert_policy_data('Insured Marital Status', '')
    insert_policy_data('Insured Nature Of Job', '')
    insert_policy_data('Name of illness/injury suffering from or suffered in the past1', '')
    insert_policy_data('Date of first Diagnosis', '')
    insert_policy_data('Treatment/medication received/receiving', '')
    insert_policy_data('Whether Fully Cured', '')
    insert_policy_data('Name of illness/injury suffering from or suffered in the past2', '')
    insert_policy_data('Date of first Diagnosis1', '')
    insert_policy_data('Treatment/medication received/receiving1', '')
    insert_policy_data('Whether Fully Cured1', '')
    insert_policy_data('Name of illness/injury suffering from or suffered in the past', '')
    insert_policy_data('Date of first Diagnosis2', '')
    insert_policy_data('Treatment/medication received/receiving2', '')
    insert_policy_data('Whether Fully Cured2', '')
    insert_policy_data('Policy No.1', '')
    insert_policy_data('Insured From (Date)1', '')
    insert_policy_data('Insured till (Date)1', '')
    insert_policy_data('Sum Insured1', '')
    insert_policy_data('Claim Number', '')
    insert_policy_data('Claim Amount1', '')
    insert_policy_data('Ailment1', '')
    insert_policy_data('Cumulative Bonus Earned (%)1', '')
    insert_policy_data('Cumulative Bonus Amount1', '')
    insert_policy_data('Policy No.', '')
    insert_policy_data('Insured From (Date)', '')
    insert_policy_data('Insured till (Date)', '')
    insert_policy_data('Sum Insured', '')
    insert_policy_data('Claim Number1', '')
    insert_policy_data('Claim Amount', '')
    insert_policy_data('Ailment', '')
    insert_policy_data('Cumulative Bonus Earned (%)2', '')
    insert_policy_data('Cumulative Bonus Amount2', '')
    insert_policy_data('Policy No.2', '')
    insert_policy_data('Insured From (Date)2', '')
    insert_policy_data('Insured till (Date)2', '')
    insert_policy_data('Sum Insured2', '')
    insert_policy_data('Claim Number2', '')
    insert_policy_data('Claim Amount2', '')
    insert_policy_data('Ailment2', '')
    insert_policy_data('Cumulative Bonus Earned (%)3', '')
    insert_policy_data('Cumulative Bonus Amount3', '')
    insert_policy_data('Document Category 1', '')
    insert_policy_data('Document1', '')
    insert_policy_data('Document Received Date1', '')
    insert_policy_data('Status1', '')
    insert_policy_data('Document Category 2', '')
    insert_policy_data('Document2', '')
    insert_policy_data('Document Received Date2', '')
    insert_policy_data('Status2', '')
    insert_policy_data('Document Category 3', '')
    insert_policy_data('Document3', '')
    insert_policy_data('Document Received Date3', '')
    insert_policy_data('Status3', '')
    ## Begin: INSURED QUESTIONS ##
    header.append('Question No.1')
    header.append('Answer1')
    header.append('Question No.2')
    header.append('Answer2')
    header.append('Question No.3')
    header.append('Answer3')
    header.append('Question No.4')
    header.append('Answer4')
    header.append('Question No.5')
    header.append('Answer5')
    header.append('Question No.6')
    header.append('Answer6')
    header.append('Question No.7')
    header.append('Answer7')
    header.append('Question No.8')
    header.append('Answer8')
    header.append('Question No.9')
    header.append('Answer9')
    header.append('Question No.10')
    header.append('Answer10')
    header.append('Question No.11')
    header.append('Answer11')
    header.append('Question No.12')
    header.append('Answer12')
    header.append('Question No.13')
    header.append('Answer13')
    header.append('Question No.14')
    header.append('Answer14')
    header.append('Question No.15')
    header.append('Answer15')
    header.append('Question No.16')
    header.append('Answer16')
    header.append('Question No.17')
    header.append('Answer17')
    header.append('Question No.18')
    header.append('Answer18')
    header.append('Question No.19')
    header.append('Answer19')
    header.append('Question No.20')
    header.append('Answer20')
    for i, data_row in enumerate(data):
        mem = insured_list[i][0]
        mem_data = insured_list[i][1]
        data_row.append('Lvl01_Q001')#Question No.1
        data_row.append(form_data['medical_question_tobacco']['tobacco'][mem+'_question_tobacco']['value'].upper())#Answer1
        data_row.append('Lvl01_Q002')#Question No.2
        data_row.append(form_data['medical_question_pre_existing']['pre_existing'][mem+'_question_pre_existing']['value'].upper())#Answer2
        data_row.append('Lvl01_Q003')#Question No.3
        data_row.append(form_data['medical_question_pre_existing']['conditional_blood_pressure'][mem+'_question_conditional_blood_pressure']['value'].upper())#Answer3
        data_row.append('Lvl01_Q004')#Question No.4
        data_row.append(form_data['medical_question_pre_existing']['conditional_diabetes'][mem+'_question_conditional_diabetes']['value'].upper())#Answer4
        data_row.append('Lvl01_Q005')#Question No.5
        data_row.append(form_data['medical_question_pre_existing']['conditional_arthritis'][mem+'_question_conditional_arthritis']['value'].upper())#Answer5
        data_row.append('Lvl01_Q006')#Question No.6
        data_row.append(form_data['medical_question_pre_existing']['conditional_lung_disease'][mem+'_question_conditional_lung_disease']['value'].upper())#Answer6
        data_row.append('Lvl01_Q007')#Question No.7
        data_row.append(form_data['medical_question_pre_existing']['conditional_kidney_disease'][mem+'_question_conditional_kidney_disease']['value'].upper())#Answer7
        data_row.append('Lvl01_Q008')#Question No.8
        data_row.append(form_data['medical_question_pre_existing']['conditional_liver_disease'][mem+'_question_conditional_liver_disease']['value'].upper())#Answer8
        data_row.append('Lvl01_Q009')#Question No.9
        data_row.append(form_data['medical_question_pre_existing']['conditional_endocrine_disorder'][mem+'_question_conditional_endocrine_disorder']['value'].upper())#Answer9
        data_row.append('Lvl01_Q010')#Question No.10
        data_row.append(form_data['medical_question_pre_existing']['conditional_cancer_disease'][mem+'_question_conditional_cancer_disease']['value'].upper())#Answer10
        data_row.append('Lvl01_Q011')#Question No.11
        data_row.append(form_data['medical_question_pre_existing']['conditional_hiv'][mem+'_question_conditional_hiv']['value'].upper())#Answer11
        data_row.append('Lvl01_Q012')#Question No.12
        data_row.append(form_data['medical_question_pre_existing']['conditional_blood_disease'][mem+'_question_conditional_blood_disease']['value'].upper())#Answer12
        data_row.append('Lvl01_Q013')#Question No.13
        data_row.append(form_data['medical_question_pre_existing']['conditional_nervous_disorder'][mem+'_question_conditional_nervous_disorder']['value'].upper())#Answer13
        data_row.append('Lvl01_Q014')#Question No.14
        data_row.append(form_data['medical_question_pre_existing']['conditional_mental_disorder'][mem+'_question_conditional_mental_disorder']['value'].upper())#Answer14
        data_row.append('Lvl01_Q015')#Question No.15
        data_row.append(form_data['medical_question_pre_existing']['conditional_females'][mem+'_question_conditional_females']['value'].upper())#Answer15
        data_row.append('Lvl01_Q016')#Question No.16
        data_row.append(form_data['medical_question_pre_existing']['conditional_alcohol'][mem+'_question_conditional_alcohol']['value'].upper())#Answer16
        data_row.append('Lvl01_Q017')#Question No.17
        data_row.append(form_data['medical_question_pre_existing']['conditional_external_mass'][mem+'_question_conditional_external_mass']['value'].upper())#Answer17
        data_row.append('Lvl01_Q018')#Question No.18
        data_row.append(form_data['medical_question_pre_existing']['conditional_skin'][mem+'_question_conditional_skin']['value'].upper())#Answer18
        data_row.append('Lvl01_Q019')#Question No.19
        data_row.append(form_data['medical_question_pre_existing']['conditional_recent'][mem+'_question_conditional_recent']['value'].upper())#Answer19
        data_row.append('Lvl01_Q047')#Question No.20
        data_row.append(form_data['medical_question_application_declined']['application_declined'][mem+'_question_application_declined']['value'].upper())#Answer20
    ## End: INSURED QUESTIONS ##
    insert_policy_data('Question No.21', '')
    insert_policy_data('Answer21', '')
    insert_policy_data('Question CD 22', '')
    insert_policy_data('Answer22', '')
    insert_policy_data('Question CD 23', '')
    insert_policy_data('Answer23', '')
    insert_policy_data('Question CD 24', '')
    insert_policy_data('Answer24', '')
    insert_policy_data('Question CD 25', '')
    insert_policy_data('Answer25', '')
    insert_policy_data('Question CD 26', '')
    insert_policy_data('Answer26', '')
    insert_policy_data('Question CD 27', '')
    insert_policy_data('Answer27', '')
    insert_policy_data('Question CD 28', '')
    insert_policy_data('Answer28', '')
    insert_policy_data('Question CD 29', '')
    insert_policy_data('Answer29', '')
    insert_policy_data('Question CD 30', '')
    insert_policy_data('Answer30', '')
    insert_policy_data('Question CD 31', '')
    insert_policy_data('Answer31', '')
    insert_policy_data('Question CD 32', '')
    insert_policy_data('Answer32', '')
    insert_policy_data('Question CD 33', '')
    insert_policy_data('Answer33', '')
    insert_policy_data('Question CD 34', 'Lvl01_Q999')
    insert_policy_data('Answer34', 'NO')
    insert_policy_data('Pre Existing Illness Cd1', '')
    insert_policy_data('AnswerCd1_1', '')
    insert_policy_data('Pre Existing Illness Cd2', '')
    insert_policy_data('AnswerCd2', '')
    insert_policy_data('Pre Existing Illness Cd3', '')
    insert_policy_data('AnswerCd3', '')
    insert_policy_data('Pre Existing Illness Cd4', '')
    insert_policy_data('AnswerCd4', '')
    insert_policy_data('Pre Existing Illness Cd5', '')
    insert_policy_data('AnswerCd5', '')
    insert_policy_data('Others Cd1', '')
    insert_policy_data('AnswerCd1_2', '')
    insert_policy_data('GRN Number', '')
    insert_policy_data('Date', '')
    insert_policy_data('Amount', '')
    insert_policy_data('Vernacular Tumb Impression', 'NO')
    insert_policy_data('Business Source', 'PORTAL')
    insert_policy_data('Application Received Date', datetime.datetime.now().strftime('%d/%m/%Y'))
    insert_policy_data('Policy Maturity Date', '')
    insert_policy_data('Issuing Bank', '')
    insert_policy_data('Receipt Card Type', '')
    insert_policy_data('Payout Option', '')
    insert_policy_data('Reducing Balance SI', '')

    filename = "proposal_extract_%s" % transaction.transaction_id

    wb = Workbook(write_only=True)
    ws = wb.create_sheet()
    ws.append(header)

    import StringIO
    output = StringIO.StringIO()

    for data_row in data:
        ws.append(data_row)
    wb.save(output)
    output.seek(0)
    response = HttpResponse(output.getvalue())
    response['Content-Type'] = 'application/vnd.ms-excel'
    response['Content-Disposition'] = 'attachment; filename=Cigna_Form_%s_%s.xls' % (transaction.policy_number, transaction.transaction_id)
    return response

def payment_success_alert(transaction):
    sl = transaction.slab
    msg_dict = OrderedDict([
        ('Transaction ID', transaction.transaction_id),
        ('Email', transaction.proposer_email),
        ('Plan', '%s - (Rs. %s)' % (sl.health_product.title, sl.sum_assured)),
        ('Premium Paid', transaction.premium_paid),
        ('Download Buy Form Data', '%s/health-plan/buy-form-download/%s/' % (settings.SITE_URL, transaction.transaction_id)),
        ('Download Transaction Data', '%s/health-plan/transaction/%s/download/' % (settings.SITE_URL, transaction.transaction_id)),
    ])
    msg_text = '\n'.join(['%s: %s' % (k, v) for k, v in msg_dict.items()])
    sub_text = 'ALERT: CIGNA TTK Payment Success'
    mail_list = [
            'soman@coverfox.com',
            'support@coverfox.com',
            'mahavir@coverfox.com',
            'noopur@coverfoxmail.com',
            'suhas@coverfoxmail.com',
            'jatinderjit@coverfoxmail.com',
    ]
    thread.start_new_thread(send_mail, (sub_text, msg_text, "Dev Bot <dev@cfstatic.org>", mail_list))
