from health_product.models import *
import preset
def get_insured_table(transaction):
    guids = transaction.extra['guids']
    guid_to_custid = transaction.extra['guid_to_custid']

    table = '<table align="center" style="margin-bottom:30px;border:1px solid black;border-collapse:collapse;max-width:1000px;table-layout:fixed;overflow-wrap:break-word;word-wrap:break-word;" border="1" width="1000px">'
    table = table + '<tr><th rowspan="2">Sr No.</th><th rowspan="2">Name of the Insured Person(s)</th><th rowspan="2">Relationship with Policyholder</th><th rowspan="2">Gender</th><th rowspan="2">Date of Birth</th><th rowspan="2">Pre-existing Disease/Illness/Condition</th><th rowspan="2">Customer ID</th><th rowspan="2">Sum Insured</th><th rowspan="2">Occupation</th><th colspan="2">Cumulative Bonus Earned</th></tr><tr><th>%</th><th>Amount</th></tr>'
    form_data = transaction.insured_details_json['form_data']
    insured = transaction.insured_details_json['form_data']['insured']
    counter = 1
    members_extra = {
        mem['id']: mem
        for mem in transaction.extra['members']
            }
    pre_existing = form_data['medical_question_pre_existing']['pre_existing']
    no_of_insured = len(insured)
    for i in insured:
        table = table + '<tr>'
        table = table + '<td>'+str(counter)+'</td>'
        table = table + '<td>'+' '.join([insured[i]['first_name']['value'], insured[i]['last_name']['value']])+'</td>'
        if i in ['father', 'mother', 'self', 'spouse']:
            table = table + '<td>' + i.title() + '</td>'
        else:
            table = table + '<td>'+i[:-1].title() + '</td>'
        table = table + '<td>Male</td>' if members_extra[i]['gender'] == 1 else table + '<td>Female</td>'
        table = table + '<td>'+insured[i]['date_of_birth']['display_value']+'</td>'
        table = table + '<td>'+pre_existing[i+'_question_pre_existing']['display_value']+'</td>'
        table = table + '<td>'+guid_to_custid[guids[i]]+'</td>'
        if no_of_insured > 1:
            if counter == 1:
                table = table + '<td rowspan="'+str(no_of_insured)+'">'+str(transaction.extra['plan_data']['cover'])+'</td>'
            table = table + '<td>'+insured[i]['occupation']['display_value']+'</td>'
            if counter == 1:
                table = table + '<td rowspan="'+str(no_of_insured)+'">'+'0'+'</td>'
            if counter == 1:
                table = table + '<td rowspan="'+str(no_of_insured)+'">'+'0'+'</td>'
        else:
            table = table + '<td>'+str(transaction.extra['plan_data']['cover'])+'</td>'
            table = table + '<td>'+insured[i]['occupation']['display_value']+'</td>'
            table = table + '<td>'+'0'+'</td>'
            table = table + '<td>'+'0'+'</td>'
        table = table + '</tr>'
        counter = counter + 1
    table = table + '</table>'
    return table
