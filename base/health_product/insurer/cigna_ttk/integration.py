from django.conf import settings
from django.db import connection
from django import template
from wiki.models import Pincode
from putils import FileLock
import requests
from insured_table import get_insured_table
import datetime
import json
import preset
from dateutil.relativedelta import relativedelta
import os
from path import path
from utils import request_logger
from lxml import etree
import xml.etree.ElementTree as ET
from django.core.files.base import File
import httplib2
from health_product import prod_utils
from num2words import num2words
import logging
import utils
from health_product.insurer.cigna_ttk.form_data import Formic
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


if settings.PRODUCTION:
    QUOTATION_WSDL = 'https://www.cignattkinsurance.in/systemcommon/services/ComputeQuotationListPremium?wsdl'
    PROPOSAL_PAYMENT_WSDL = 'https://www.cignattkinsurance.in/systemcommon/services/SaveProposalReceipt?wsdl'
    PAYMENT_URL = 'https://www.cignattkinsurance.in/aggregator/payment'
    PAYMENT_WSDL = 'https://www.cignattkinsurance.in/systemcommon/services/GetPaymentDetails?wsdl'
    PRODUCT_WSDL = 'https://www.cignattkinsurance.in/systemcommon/services/GetProductDetails?wsdl'
    AGENT_ID = '1602702-01'
    CHANNEL_ID = '350'
    CAMPAIGNCD = 'DEFCMP03'
else:
    QUOTATION_WSDL = "https://test.cignattkinsurance.in/systemcommon/services/ComputeQuotationListPremium?wsdl"
    PROPOSAL_PAYMENT_WSDL = "https://test.cignattkinsurance.in/systemcommon/services/SaveProposalReceipt?wsdl"
    PAYMENT_URL = "https://test.cignattkinsurance.in/aggregator/payment"
    AGENT_ID = '1600035-01'
    CHANNEL_ID = '880'
    CAMPAIGNCD = 'IPMIWEB001'

PARENT_PID = 'IPMIPRESTI'

CIGNA_FOLDER_PATH = path(__file__).parent.abspath()


def get_refguid():
    import uuid
    return uuid.uuid4().hex


def get_zone(city, state):
    if city in ['Mumbai', 'Thane', 'Navi Mumbai'] or state == 'Delhi':
        zone = 'ZONE1'
    elif city in ['Bangalore', 'Hyderabad', 'Chennai', 'Chandigarh', 'Ludhiana', 'Kolkata'] or state == 'Gujarat':
        zone = 'ZONE2'
    else:
        zone = 'ZONE3'
    return zone


def get_salut(transaction):
    members = transaction.extra['members']
    insured = transaction.insured_details_json['form_data']['insured']
    salut = {}
    for m in members:
        if m['gender'] == 1:
            salut[m['id']] = 'MR'
        else:
            try:
                married = True if insured[m['id']]['marital_status']['value'] == 'MARRIED' else False
            except Exception:
                married = False
            salut[m['id']] = 'MRS' if married else 'MISS'
    transaction.extra['insured_salut'] = salut
    transaction.save()
    return salut


def get_proposer_salut(transaction):
    proposer_data = transaction.insured_details_json['form_data']['proposer']['personal']
    salut = ''
    married = False if proposer_data['marital_status']['value'] == 'MARRIED' else True
    if proposer_data['gender']['value'] == 1:
        salut = 'MR'
    else:
        salut = 'MRS' if married == True else 'MISS'
    transaction.extra['proposer_salut'] = salut
    transaction.save()
    return salut


def get_template_dict(transaction):
    form_data = transaction.insured_details_json['form_data']
    ppersonal = form_data['proposer']['personal']
    pcontact = form_data['proposer']['contact']
    paddress = form_data['proposer']['address']
    plan_option_cd = transaction.extra['plan_option']
    premium = transaction.extra['premium']
    tax_factor = 100.0 / (100.0 + settings.SERVICE_TAX_PERCENTAGE)
    base_premium = round(premium * tax_factor)  # TODO

    tenure = transaction.insured_details_json['form_data']['proposer']['personal']['policy_term']['value1']
    discount_in_percent = 10
    for d in Formic.POLICY_DISCOUNTS:
        if d['id'] is tenure:
            discount_in_percent += float(d['name'])
    premium_before_discount = round((base_premium * 100) / (100 - discount_in_percent))

    service_tax = round(base_premium * ((settings.SERVICE_TAX_PERCENTAGE - 0.5) / 100))
    sb_cess_tax = round(base_premium * ((settings.SERVICE_TAX_PERCENTAGE - 14.0) / 100))
    insured_data = form_data['insured']
    nominee_data = form_data['nominee']['self']
    data_dict = {}
    data_dict['proposer_name'] = ' '.join([ppersonal['first_name']['value'], ppersonal['last_name']['value']])
    data_dict['proposer_cust_id'] = transaction.extra['proposer_cust_id']
    data_dict['address1'] = paddress['address1']['display_value']
    data_dict['address2'] = paddress['address2']['display_value']
    data_dict['landmark'] = paddress['landmark']['display_value']
    data_dict['city'] = paddress['city']['display_value']
    data_dict['state'] = paddress['state']['display_value']
    data_dict['pincode'] = paddress['pincode']['display_value']
    data_dict['r_number'] = '-'.join([pcontact['landline']['std_code'], pcontact['landline']['landline']])
    data_dict['m_number'] = '+91'+pcontact['mobile']['display_value']
    data_dict['proposer_email'] = pcontact['email']['display_value']
    data_dict['plan'] = preset.PLAN_OPTION_MASTER['R'+plan_option_cd[3:6]]
    data_dict['policy_number'] = transaction.extra['policy_number']
    data_dict['policy_period'] = str(ppersonal['policy_term']['value1'])
    data_dict['policy_start_date'] = transaction.policy_start_date.strftime('%d/%m/%Y')
    data_dict['policy_expiry_date'] = transaction.policy_end_date.strftime('%d/%m/%Y')
    data_dict['policy_type'] = 'INDIVIDUAL' if len(transaction.extra['members']) == 1 else 'FAMILY FLOATER'
    data_dict['zone'] = transaction.extra['zone']
    data_dict['insured_table'] = get_insured_table(transaction)
    data_dict['nominee_full_name'] = ' '.join([nominee_data['first_name']['display_value'], nominee_data['last_name']['display_value']])
    data_dict['nominee_relation'] = nominee_data['relation']['value1']
    plan_file = open('%s/%s.html' % (CIGNA_FOLDER_PATH, preset.PLAN_HTML_MASTER['R'+plan_option_cd[3:6]]), 'r')
    html = plan_file.read()
    plan_file.close()
    data_dict['plan_details'] = html
    data_dict['premium_before_discount'] = premium_before_discount
    data_dict['discount'] = premium_before_discount - base_premium
    data_dict['premim_after_discount'] = str(base_premium)
    data_dict['additional_loading'] = '-'
    data_dict['service_tax'] = str(service_tax)
    data_dict['swachh_bharat_cess'] = str(sb_cess_tax)
    data_dict['education_cess'] = '-'
    data_dict['higher_education_cess'] = '-'
    data_dict['total_premium'] = str(premium)
    data_dict['premium_in_words'] = num2words(premium, lang='en_IN').title()
    logo_image_path = '%s/Cigna.jpg' % CIGNA_FOLDER_PATH
    sign_image_path = '%s/signature1.jpg' % CIGNA_FOLDER_PATH
    data_dict['logo_image_path'] = logo_image_path
    data_dict['sign_image_path'] = sign_image_path
    return data_dict


def get_xml_formatted_data(transaction):
    members = transaction.extra['members']

    adults = len([mem for mem in members if mem['type'] == 'adult'])
    children = len(members) - adults

    pincode = Pincode.objects.filter(pincode=transaction.extra['pincode'])[0]
    city = transaction.extra['city']['name']
    state = pincode.state.name
    zoneCd = get_zone(city, state)

    cover_type = ''
    if len(members) < 2:
        cover_type = 'INDIVIDUAL'
    else:
        cover_type = 'FAMILYFLOATER'

    now = datetime.datetime.now()
    quotationDt = now.strftime('%d/%m/%Y')

    slab = transaction.slab
    hp_id = slab.health_product.id

    product_id = preset.PRODUCT_DICT[hp_id]['product_id']
    plan_option_cd = preset.PRODUCT_DICT[hp_id][slab.sum_assured]['plan_option_cd']

    form_data = transaction.insured_details_json['form_data']
    pcontact = form_data['proposer']['contact']
    ppersonal = form_data['proposer']['personal']
    member_details = []
    members_extra = {
            mem['id']: mem
            for mem in transaction.extra['members']
    }

    for mem_type in form_data['insured']:
        mem = form_data['insured'][mem_type]
        mem_extra = members_extra[mem_type]
        tobacco = form_data['medical_question_tobacco']['tobacco'][mem_type+'_question_tobacco']['value']
        member = {}
        member['genderCd'] = 'MALE' if mem_extra['gender'] == 1 else 'FEMALE'
        member['insuredTypeCd'] = 'PRIMARY'
        member['cityCd'] = city
        member['emailAddress'] = pcontact['email']['value']
        member['dob'] = mem['date_of_birth']['value']
        member['zoneCd'] = zoneCd
        member['sumInsured'] = ''
        member['smokerStatusCd'] = tobacco
        member['chewTobaccoCd'] = tobacco
        member['consumeAlcoholCd'] = ''
        if mem_type == 'spouse':
            mem_type = 'husband' if mem_extra['gender'] == 1 else 'wife'
        elif mem_type[:-1] in ['son', 'daughter']:
            mem_type = mem_type[:-1]
        member['relationCd'] = preset.RELATIONSHIP_CODES[mem_type]
        member['productPlanOptionCd'] = plan_option_cd

        member_details.append(member)

    if cover_type=='INDIVIDUAL':
        member = member_details[0]
        member['issueAge'] = '0'
        member['mobileNum'] = pcontact['mobile']['value']
        member['modalPremium'] = '0.0'
        member['extraPremium'] = '0.0'
        member['discount'] = '0.0'

    transaction.extra['product_id'] = product_id
    transaction.save()

    data_dict = {
            "channelId": CHANNEL_ID,
            "productId": product_id,
            "parentProductId": PARENT_PID,
            "parentProductVersion": "1",
            "noOfAdults": adults,
            "noOfKids": children,
            "tenure": ppersonal['policy_term']['value1'],
            "productPlanOptionCd": plan_option_cd,
            "policyType": cover_type,
            "saveFl": "YES",
            "quoteTypeCd": "PORTAL",
            "quotationDt": quotationDt,
            "agentId": AGENT_ID,
            "campaignCd": CAMPAIGNCD,
            "inwardTypeCd": "NEWBUSINESS",
            "inwardSubTypeCd": "PROPOSALDOCUMENT",
            "quotationChangeDOList": {
                "alterationType": ""
                },
            "quotationProductDOList": [
                {
                    "productId": product_id,
                    "productVersion": "1",
                    "productPlanOptionCd": plan_option_cd,
                    "basePremium": "0.0",
                    "sumInsured": "",
                    "zoneCd": zoneCd,
                    "productTypeCd": "SUBPLAN",
                    "extraPremium": "0.0",
                    "paymentFrequencyCd": "SINGLE",
                    "reducingBalanceSI": "",
                    "payoutOption": "",
                    "discount": "0.0",
                    "quotationProductBenefitDOList": {
                        "benefitId": preset.PLAN_DISCOUNTS[product_id[:4]],
                        "productId": product_id
                    },
                    "quotationProductInsuredDOList": member_details,
                },
            ]
        }

    return data_dict;

def get_xml_formatted_proposal_request(transaction, payment_response):
    #payment_response = {'response': 'PROHLT260000005|CT555578A2A465A|144416-001179|2.00|CIT|22270726|NA|INR|DIRECT|NA|NA|NA|15-05-2015 10:10:02|0300|NA|NA|AGGR|NA|123456|789654|NA|CREDITCARD|NA|Success'}

    resp = payment_response['response']
    payment_resp = resp.split('|')

    members = transaction.extra['members']

    salut = get_salut(transaction)
    proposer_salut = get_proposer_salut(transaction)

    policy_num = transaction.extra['policy_number']

    adults = len([mem for mem in members if mem['type']=='adult'])
    children = len(members) - adults

    pincode = Pincode.objects.filter(pincode=transaction.extra['pincode'])[0]
    state = pincode.state.name
    city = transaction.extra['city']['name']
    zoneCd = get_zone(city, state)

    transaction.extra['zone'] = zoneCd

    cover_type=''
    if len(members)<2:
        cover_type='INDIVIDUAL'
    else:
        cover_type='FAMILYFLOATER'
    form_data = transaction.insured_details_json['form_data']
    pcontact = form_data['proposer']['contact']
    ppersonal = form_data['proposer']['personal']
    paddress = form_data['proposer']['address']
    insured_data = form_data['insured']

    policy_term = ppersonal['policy_term']['value1']

    now = datetime.datetime.now()
    proposal_date = now.strftime('%d/%m/%Y')
    transaction.policy_start_date = now + relativedelta(days=1)
    transaction.policy_end_date = transaction.policy_start_date + relativedelta(days=-1, years=policy_term)

    slab = transaction.slab
    hp_id = slab.health_product.id

    product_id = preset.PRODUCT_DICT[hp_id]['product_id']
    plan_option_cd = preset.PRODUCT_DICT[hp_id][slab.sum_assured]['plan_option_cd']
    transaction.extra['plan_option'] = plan_option_cd
    guids = {}


    for member in form_data['insured']:
        guids[member] = get_refguid()

    proposer_guid = guids['self'] if 'self' in guids.keys() else get_refguid()

    transaction.extra['guids'] = guids
    transaction.extra['proposer_guid'] = proposer_guid

    policyProductInsuredDOList = []

    for code, fdata in form_data['insured'].items():
        insured = {}

        feet = int(fdata['height']['value1']) if fdata['height']['value1'] else 0
        inches = int(fdata['height']['value2']) if fdata['height']['value2'] else 0

        height = int(round((feet*12 + inches) * 2.54, 0))
        height_metre = height / 100.0
        bmi = int(round(float(fdata['weight']['value']) / (height_metre * height_metre), 0))
        form_data['insured'][code]['bmi'] = bmi
        form_data['insured'][code]['height1'] = height
        form_data['insured'][code]['weight1'] = fdata['weight']['value']

        insured['refGuid'] = guids[code]
        insured['zoneCd'] = zoneCd
        insured['height'] = height
        insured['weight'] = fdata['weight']['value']
        insured['bmi'] = bmi
        insured['cityCd'] = city
        insured['productPlanOptionCd'] = plan_option_cd
        insured['baseSumAssured'] = slab.sum_assured
        insured['customerId'] = ''
        insured['occupationCd'] = fdata['occupation']['value']
        insured['maritalStatusCd'] = fdata['marital_status']['value']
        insured['policyQuestionSetDOList'] = [
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q001",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q001",
                    "dataElementCd": "Lvl01_Q001",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_tobacco']['tobacco'][code+'_question_tobacco']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q002",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q002",
                    "dataElementCd": "Lvl01_Q002",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['pre_existing'][code+'_question_pre_existing']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q003",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q003",
                    "dataElementCd": "Lvl01_Q003",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['conditional_blood_pressure'][code+'_question_conditional_blood_pressure']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q004",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q004",
                    "dataElementCd": "Lvl01_Q004",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['conditional_diabetes'][code+'_question_conditional_diabetes']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q005",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q005",
                    "dataElementCd": "Lvl01_Q005",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['conditional_arthritis'][code+'_question_conditional_arthritis']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q006",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q006",
                    "dataElementCd": "Lvl01_Q006",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['conditional_lung_disease'][code+'_question_conditional_lung_disease']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q007",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q007",
                    "dataElementCd": "Lvl01_Q007",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['conditional_kidney_disease'][code+'_question_conditional_kidney_disease']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q008",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q008",
                    "dataElementCd": "Lvl01_Q008",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['conditional_liver_disease'][code+'_question_conditional_liver_disease']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q009",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q009",
                    "dataElementCd": "Lvl01_Q009",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['conditional_endocrine_disorder'][code+'_question_conditional_endocrine_disorder']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q010",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q010",
                    "dataElementCd": "Lvl01_Q010",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['conditional_cancer_disease'][code+'_question_conditional_cancer_disease']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q011",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q011",
                    "dataElementCd": "Lvl01_Q011",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['conditional_hiv'][code+'_question_conditional_hiv']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q012",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q012",
                    "dataElementCd": "Lvl01_Q012",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['conditional_blood_disease'][code+'_question_conditional_blood_disease']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q013",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q013",
                    "dataElementCd": "Lvl01_Q013",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['conditional_nervous_disorder'][code+'_question_conditional_nervous_disorder']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q014",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q014",
                    "dataElementCd": "Lvl01_Q014",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['conditional_mental_disorder'][code+'_question_conditional_mental_disorder']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q015",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q015",
                    "dataElementCd": "Lvl01_Q015",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['conditional_females'][code+'_question_conditional_females']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q016",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q016",
                    "dataElementCd": "Lvl01_Q016",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['conditional_alcohol'][code+'_question_conditional_alcohol']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q017",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q017",
                    "dataElementCd": "Lvl01_Q017",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['conditional_external_mass'][code+'_question_conditional_external_mass']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q018",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q018",
                    "dataElementCd": "Lvl01_Q018",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['conditional_skin'][code+'_question_conditional_skin']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "Lvl01_Q019",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q019",
                    "dataElementCd": "Lvl01_Q019",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_pre_existing']['conditional_recent'][code+'_question_conditional_recent']['value'].upper()
                        }
                    }
                },
            {
                "partyGuid": guids[code],
                "questionSetCd": "PCIDetailsQuestion",
                "policyQuestionDOList": {
                    "questionCd": "Lvl01_Q047",
                    "dataElementCd": "Lvl01_Q047",
                    "policyQuestionResponseDOList": {
                        "responseValue": form_data['medical_question_application_declined']['application_declined'][code+'_question_application_declined']['value'].upper()
                        }
                    }
                }
            ]

        insured['policyProductInsuredBenefitTO'] = {
            "beneftTypeCd": '',
            "benefitId": '',
            "productId": ''
        }

        policyProductInsuredDOList.append(insured)


    stp_dict = {}
    STP = True
    for member in insured_data:
        stp = True
        if form_data['medical_question_pre_existing']['pre_existing'][member+'_question_pre_existing']['value'] == 'Yes':
            stp = False
        if form_data['medical_question_application_declined']['application_declined'][member+'_question_application_declined']['value'] == 'Yes':
            stp = False
        bmi_stp = (18 <= insured_data[member]['bmi'] <= 35)
        stp = stp and bmi_stp
        STP = STP and stp
        stp_dict[member] = stp
    stp_dict['policy'] = STP
    transaction.extra['stp'] = stp_dict
    transaction.save()


    policyPartyRoleDOList = []
    for member in insured_data:
        role = {}
        role['roleCd'] = 'PRIMARY'
        role['refGuid'] = guids[member]
        role['customerId'] = ''
        policyPartyRoleDOList.append(role)

    role = {}
    role['roleCd'] = 'PROPOSER'
    role['refGuid'] = proposer_guid
    role['customerId'] = ''
    policyPartyRoleDOList.append(role)

    members_extra = {
            mem['id']: mem
            for mem in members
    }

    partyDOList = []

    for member in form_data['insured']:
        party = {}
        party['partyGuid'] = proposer_guid if member == 'self' else guids[member]
        party['titleCd'] = salut[member]
        party['firstName1'] = form_data['insured'][member]['first_name']['value']
        party['middleName1'] = ''
        party['customerId'] = ''
        party['lastName1'] = form_data['insured'][member]['last_name']['value']
        party['roleCd'] = 'PROPOSER' if member == 'self' else 'PRIMARY'
        party['birthDt'] = form_data['insured'][member]['date_of_birth']['value']
        party['genderCd'] = 'MALE' if members_extra[member]['gender']==1 else 'FEMALE'
        party['maritalStatusCd'] = form_data['insured'][member]['marital_status']['value']

        citizen = form_data['insured'][member]['nationality']['value']
        party['citizenshipCd'] = citizen
        if citizen == 'OT':
            party['otherCitizenship'] = form_data['insured'][member]['nationality_value']['value']

        party['zoneCd'] = zoneCd
        if member == 'self' and transaction.extra['premium'] > 100000 and ppersonal.has_key('pan_card_no'):
            party['partyIdentityDOList'] = {}
            party['partyIdentityDOList']['identityTypeCd'] = 'PAN'
            party['partyIdentityDOList']['identityNum'] = ppersonal['pan_card_no']['value']
        party['partyAddressDOList'] = [
            {
                "addressTypeCd": "COMMUNICATION",
                "addressLine1Lang1": paddress['address1']['value'],
                "addressLine2Lang1": paddress['address2']['value'],
                "addressLine3Lang1": paddress['landmark']['value'],
                "stateCd": paddress['state']['value'],
                "cityCd": paddress['city']['value'],
                "pinCode": paddress['pincode']['value'],
                "postalZone": zoneCd
            },
            {
                "addressTypeCd": "PERMANENT",
                "addressLine1Lang1": paddress['address1']['value'],
                "addressLine2Lang1": paddress['address2']['value'],
                "addressLine3Lang1": paddress['landmark']['value'],
                "stateCd": paddress['state']['value'],
                "cityCd": paddress['city']['value'],
                "pinCode": paddress['pincode']['value'],
                "postalZone": zoneCd
            }
        ]
        party['partyContactDOList'] = [
            {
                "contactTypeCd": "MOBILE",
                "contactNum": pcontact['mobile']['value'],
                "stdCode": ""
            },
            {
                "contactTypeCd": "RESIDENTIAL",
                "contactNum": pcontact['landline']['landline'],
                "stdCode": pcontact['landline']['std_code']
            }
        ]
        party['partyEmailDOList'] = {
            "emailTypeCd": "PERSONAL",
            "emailAddress": pcontact['email']['value']
        }
        party['partyOccupationDOList'] = {
            "occupationCd": form_data['insured'][member]['occupation']['value']
        }
        if member == 'self':
            party['partyEmploymentDOList'] = {
                "annualIncome": ppersonal['gainful_annual_income']['value'],
                "annualGrossIncome": ppersonal['annual_income']['value']
            }
        else:
            party['partyEmploymentDOList'] = {
                "annualIncome": '',
                "annualGrossIncome": ''
            }
        mem_type = member
        if mem_type == 'spouse':
            mem_type = 'husband' if members_extra[mem_type]['gender'] == 1 else 'wife'
        elif mem_type[:-1] in ['son', 'daughter']:
            mem_type = mem_type[:-1]

        party['partyRelationDOList'] = {
            "relationCd": preset.RELATIONSHIP_CODES[mem_type],
            "relatedToPartyId": proposer_guid
        }
        partyDOList.append(party)

    if 'self' not in form_data['insured']:
        party = {}
        party['partyGuid'] = proposer_guid
        party['titleCd'] = proposer_salut
        party['firstName1'] = ppersonal['first_name']['value']
        party['middleName1'] = ''
        party['lastName1'] = ppersonal['last_name']['value']
        party['roleCd'] = 'PROPOSER'
        party['customerId'] = ''
        party['birthDt'] = ppersonal['date_of_birth']['value']
        party['genderCd'] = 'MALE' if ppersonal['gender']==1 else 'FEMALE'
        party['maritalStatusCd'] = ppersonal['marital_status']['value']
        party['citizenshipCd'] = ppersonal['nationality']['value']

        citizen = ppersonal['nationality']['value']
        party['citizenshipCd'] = citizen
        if citizen == 'OT':
            party['otherCitizenship'] = ppersonal['nationality_value']['value']

        party['zoneCd'] = zoneCd
        party['partyAddressDOList'] = [
            {
                "addressTypeCd": "COMMUNICATION",
                "addressLine1Lang1": paddress['address1']['value'],
                "addressLine2Lang1": paddress['address2']['value'],
                "addressLine3Lang1": paddress['landmark']['value'],
                "stateCd": paddress['state']['value'],
                "cityCd": paddress['city']['value'],
                "pinCode": paddress['pincode']['value'],
                "postalZone": zoneCd
            },
            {
                "addressTypeCd": "PERMANENT",
                "addressLine1Lang1": paddress['address1']['value'],
                "addressLine2Lang1": paddress['address2']['value'],
                "addressLine3Lang1": paddress['landmark']['value'],
                "stateCd": paddress['state']['value'],
                "cityCd": paddress['city']['value'],
                "pinCode": paddress['pincode']['value'],
                "postalZone": zoneCd
            }
        ]
        party['partyContactDOList'] = [
            {
                "contactTypeCd": "MOBILE",
                "contactNum": pcontact['mobile']['value'],
                "stdCode": ""
            },
            {
                "contactTypeCd": "RESIDENTIAL",
                "contactNum": pcontact['landline']['landline'],
                "stdCode": pcontact['landline']['std_code']
            }
        ]
        party['partyEmailDOList'] = {
            "emailTypeCd": "PERSONAL",
            "emailAddress": pcontact['email']['value']
        }
        party['partyOccupationDOList'] = {
            "occupationCd": ppersonal['occupation']['value']
        }
        party['partyEmploymentDOList'] = {
                "annualIncome": ppersonal['gainful_annual_income']['value'],
            "annualGrossIncome": ppersonal['annual_income']['value']
        }
        party['partyRelationDOList'] = {
            "relationCd": 'SELF',
            "relatedToPartyId": proposer_guid
        }
        partyDOList.append(party)

    inward_num = transaction.payment_token
    transaction.extra['inward_number'] = inward_num
    transaction.save()

    proposal_dict = {
            "policyNum": policy_num,
            "proposalNum": policy_num,
            "baseProductId": product_id,
            "baseProductTypeCd": 'SUBPLAN',
            "baseProductFamilyCd": "HEALTHREVISED",
            "policyCommencementDt": transaction.policy_start_date.strftime('%d/%m/%Y'),
            "policyExpiryDt": transaction.policy_end_date.strftime('%d/%m/%Y'),
            "branchCd": "033",
            "productTerm": policy_term,
            "coverType": cover_type,
            "quoteId": transaction.extra['quoteId'],
            "quoteAmount": transaction.extra['premium'],
            "inwardTypeCd": "NEWBUSINESS",
            "inwardSubTypeCd": "PROPOSALDOCUMENT",
            "receivedFrom": "ONLINE",
            "baseAgentId": AGENT_ID,
            "channelId": CHANNEL_ID,
            "planId": product_id[:5],
            "intimationSourceCd": 'PORTAL',
            "uwDecisionCd": "",
            "zoneCd": zoneCd,
            "proposalEntryDt": proposal_date,
            "proposalSignedDt": proposal_date,
            "baseProductVersion": 1,
            "policyMaturityDt": transaction.policy_end_date.strftime('%d/%m/%Y'),
            "productTermUnitCd": 'YEARS',
            "proposalReceivedDt": proposal_date,
            "policyChangeDOList": {
                "alterationType": "",
                },
            "policyAdditionalFieldsDOList": {
                "businessTypeCd": ppersonal['business_type']['value'],
                "nomineeTitleCd": form_data['nominee']['self']['relation']['dependant_value'],
                "nomineeFirstName1": form_data['nominee']['self']['first_name']['value'],
                "nomineeRelationCd": form_data['nominee']['self']['relation']['value1'],
                "campaignCd": CAMPAIGNCD,
                "businessSourceCd": 'PORTAL',
                "previousPolicyNum": '',
                "previousPolicyExpiryDt": '',
                "splitPolicyNum1": '',
                "splitPolicyNum2": '',
                "changeInPolHolReason": '',
                "changeInPolHolOther": '',
                "delInsuredReason": '',
                "delInsuredOther": ''
                },
            "policyAgentDOList":{
                "agentId": AGENT_ID
                    },
            "policyPartyRoleDOList": policyPartyRoleDOList,
            "inwardDOList": {
                "payerTypeCd": "CLIENT",
                "inwardDt": proposal_date,
                "depositDt": proposal_date,
                "insurerBankCd": "DAUTSCHEBANK",
                "agentId": "",
                "receiptBranchId": "033",
                "inwardTypeCd": "NEWBUSINESS",
                "inwardSubTypeCd": "PROPOSALDOCUMENT",
                "inwardDetailsDOList": {
                    "paymentMethodCd": payment_resp[21],
                    "instrumentDt": proposal_date,
                    "inwardAmount": payment_resp[3],
                    "inwardNum": inward_num,
                    "paymentFrequencyCd": "SINGLE"
                    },
                "inwardPolicyDOList": {
                    "policyNum": payment_resp[0],
                    "acctTypeCd": "PROPOSAL",
                    "acctSubTypeCd": "NA",
                    "appAmount": payment_resp[3]
                    }
                },
            "partyDOList": partyDOList,
            "policyDocumentDOList": {
                "receivedStatusCd": 'RECEIVED',
                "customerId": ""
            },
            "policyQuestionSetDOList": {
                "questionSetCd": 'Consent1',
                "partyGuid": proposer_guid,
                "policyQuestionDOList": {
                    "questionCd": 'Consent1',
                    "dataElementCd": 'Consent1',
                    "policyQuestionResponseDOList": {
                        "responseValue": 'YES'
                    }
                }
            },
            "policyProductDOList": {
                "productId": product_id,
                "productFamilyCd": "HEALTHREVISED",
                "productTypeCd": 'SUBPLAN',
                "productTerm": ppersonal['policy_term']['value1'],
                "productTermUnitCd": "YEARS",
                "paymentFrequencyCd": "SINGLE",
                "productPlanOptionCd": plan_option_cd,
                "coverTypeCd": cover_type,
                "zoneCd": zoneCd,
                "policyProductAddOnsDOList": {
                    "productId": "",
                    "productPlanOptionCd": ""
                    },
                "policyProductDiscountDOList": {
                    "discountId": preset.PLAN_DISCOUNTS[product_id[:4]],
                    "productId": product_id
                    },
                "policyProductInsuredDOList": policyProductInsuredDOList
            }
        }
    return proposal_dict

#Generate QuoteID from https://quotes.cignattkinsurance.in/coverfox until Cigna's system is ready
def post_transaction_old(transaction):
    members = transaction.extra['members']
    is_family = (len(members) > 1)
    adults_no = len([mem for mem in members if mem['type']=='adult'])
    children_no = len(members) - adults_no

    slab = transaction.slab
    hp_id = slab.health_product.id
    product_id = preset.PRODUCT_DICT[hp_id]['product_id']
    plan_option_cd = preset.PRODUCT_DICT[hp_id][slab.sum_assured]['plan_option_cd']

    form_data = transaction.insured_details_json['form_data']
    insured_data = form_data['insured']
    ppersonal = form_data['proposer']['personal']

    pincode = Pincode.objects.filter(pincode=transaction.extra['pincode'])[0]
    city = transaction.extra['city']['name']
    state = pincode.state.name
    zone = get_zone(city, state)

    quote_data = {}
    quote_data['product_type'] = 'HEALTH'
    quote_data['policy_type'] = 'FAMILYFLOATER' if is_family else 'INDIVIDUAL'
    quote_data['deductible_si'] = ''
    quote_data['members_insured_adult'] = adults_no
    quote_data['sub_product_type'] = product_id[:5]
    quote_data['terms'] = ppersonal['policy_term']['value1']
    quote_data['sum_insured'] = plan_option_cd if is_family else '0'
    quote_data['members_insured_child'] = children_no
    for i, mem in enumerate(transaction.extra['members'], 1):
        if mem['person'] == 'spouse':
            mem_relation = 'husband' if mem['gender'] == 1 else 'wife'
        else:
            mem_relation = mem['person']

        dob = insured_data[mem['id']]['date_of_birth']['value']

        quote_data['insured_options[%i][pepole_type]' % i] = 'adult' if mem['type'] == 'adult' else 'child'
        quote_data['insured_options[%i][ordering]' % i] = '1'
        quote_data['insured_options[%i][sub_ordering]' % i] = '1'
        quote_data['insured_options[%i][relation]' % i] = preset.RELATIONSHIP_CODES[mem_relation]
        quote_data['insured_options[%i][dob]' % i] = datetime.datetime.strftime(datetime.datetime.strptime(dob, '%d/%m/%Y'), '%Y-%m-%d')
        if not is_family:
            quote_data['insured_options[%i][gender]' % i] = mem['gender'] - 1 # Cigna: 0: Male, 1: Female
        quote_data['insured_options[%i][ind_sum_deductible_si]' % i] = '0'
        quote_data['insured_options[%i][ind_sum_insured]' % i] = '0' if is_family else plan_option_cd
        quote_data['insured_options[%i][ind_location]' % i] = '%s|%s' % (city, zone)
    quote_data['optional_details[copay]'] = '0'
    quote_data['optional_details[addon]'] = '0'
    quote_data['optional_details[rider]'] = '0'
    # quote_data['op'] = '1'
    quote_data['agentId'] = AGENT_ID
    quote_data['channelId'] = CHANNEL_ID
    quote_data['campainCd'] = CAMPAIGNCD
    quote_data['quotation_dt'] = datetime.datetime.strftime(datetime.datetime.today(), '%Y-%m-%d')
    quote_data['op'] = 'Save Quote'

    QUOTE_URL = 'https://quotes.cignattkinsurance.in/coverfox/index.php?clicked_object=op'
    request_logger.info('\nCIGNA TTK TRANSACTION: %s \n QUOTE REQUEST SENT:\n %s \n' % (transaction.transaction_id, json.dumps(quote_data)))
    resp = requests.post(QUOTE_URL, quote_data)
    request_logger.info('\nCIGNA TTK TRANSACTION: %s \n QUOTE RESPONSE RECEIVED:\n %s \n' % (transaction.transaction_id, resp.text))

    resp_tree = etree.HTML(resp.text)
    root = resp_tree[0][0][1]
    quote_id = root[3].text.strip()
    base_premium = root[13][1].text.strip().replace(',','')
    service_tax = root[15][1].text.strip().replace(',','')
    premium = root[21][1].text.strip().replace(',','')

    policy_num = prod_utils.get_db_sequence('seq_cigna_policy_num')
    policy_num = "PROHLT28%07i" % policy_num
    transaction.policy_number = policy_num
    transaction.policy_token = policy_num
    return_url = '%s/health-plan/cigna-ttk/transaction/%s/response/' % (settings.SITE_URL, transaction.transaction_id)
    post_data = {
        'request' : policy_num+'|' + premium + '|123456|789654|' + return_url,
    }
    transaction.extra['quoteId'] = quote_id
    transaction.extra['base_premium'] = float(base_premium)
    transaction.extra['service_tax'] = float(service_tax)
    transaction.extra['premium'] = float(premium)
    transaction.extra['policy_number'] = policy_num
    transaction.payment_request = json.dumps(post_data)
    transaction.save()

    is_redirect = False
    return is_redirect, PAYMENT_URL, "health_product/post_transaction_data.html", post_data


def post_transaction(transaction):
    namespaces = {
            'soapenv':"http://schemas.xmlsoap.org/soap/envelope/",
            'sym':"http://syminterface.insurance.symbiosys.c2lbiz.com",
            }

    for k,v in namespaces.items():
        etree.register_namespace(k,v)

    #Quotation
    data = get_xml_formatted_data(transaction);
    listofquotationTO = prod_utils.data2xml(data, 'listofquotationTO')
    WSQuotationListIO = etree.Element('{http://syminterface.insurance.symbiosys.c2lbiz.com}WSQuotationListIO')
    WSQuotationListIO.append(listofquotationTO)
    compute = etree.Element('{http://syminterface.insurance.symbiosys.c2lbiz.com}compute')
    compute.append(WSQuotationListIO)
    Body = etree.Element('{http://schemas.xmlsoap.org/soap/envelope/}Body')
    Body.append(compute)
    Envelope = etree.Element('{http://schemas.xmlsoap.org/soap/envelope/}Envelope')
    Envelope.append(Body)
    request = etree.tostring(Envelope)
    http = httplib2.Http(disable_ssl_certificate_validation=True)
    headers = {'Content-type':'text/xml'}
    request_logger.info('\nCIGNA TTK TRANSACTION: %s \n QUOTE REQUEST SENT:\n %s \n' % (transaction.transaction_id, request))
    try:
        response_header, response_content = http.request(QUOTATION_WSDL, method='POST', body=request, headers=headers)
    except Exception as E:
        request_logger.info('\n === CIGNA TTK ERROR: %s === \n%s\n' % (transaction.transaction_id,logger.exception(E)));
        redirect_url = '/health-plan/product/transaction/%s/failure/' % transaction.transaction_id
        return True, redirect_url,'',{}
    request_logger.info('\nCIGNA TTK TRANSACTION: %s \n QUOTE RESPONSE RECEIVED:\n %s \n' % (transaction.transaction_id, response_content))

    #payment
    policy_num = prod_utils.get_db_sequence('seq_cigna_policy_num')
    policy_num = "PROHLR28%07i" % policy_num
    transaction.policy_number = policy_num
    transaction.policy_token = policy_num
    return_url = '%s/health-plan/cigna-ttk/transaction/%s/response/' % (settings.SITE_URL, transaction.transaction_id)
    Envelope = etree.XML(response_content)
    listofquotationTO = Envelope[0][0][0][1]
    for t in listofquotationTO:
        if 'totPremium' in t.tag:
            premium = t.text
            premium = str(round(float(premium)))
        if 'quoteId' in t.tag:
            quoteId = t.text
    post_data = {
        'request' : policy_num+'|' + premium + '|123456|789654|' + return_url,
    }
    transaction.extra['quoteId'] = quoteId
    transaction.extra['premium'] = float(premium)
    transaction.extra['policy_number'] = policy_num
    transaction.payment_request = json.dumps(post_data)
    transaction.save()

    is_redirect = False
    request_logger.info('\nCIGNA TTK TRANSACTION: %s \n PAYMENT REQUEST SENT:\n %s \n' % (transaction.transaction_id, post_data))
    return is_redirect, PAYMENT_URL, "health_product/post_transaction_data.html", post_data


def post_payment(payment_response, transaction):
    #payment_response = {'response': 'PROHLT260000005|CT555578A2A465A|144416-001179|2.00|CIT|22270726|NA|INR|DIRECT|NA|NA|NA|15-05-2015 10:10:02|0300|NA|NA|AGGR|NA|123456|789654|NA|CREDITCARD|NA|Success'}

    resp = payment_response['response']
    transaction.payment_response = json.dumps(resp)

    payment_resp = resp.split('|')
    response_data = {
        'PolicyNumber': payment_resp[0],
        'TxnReferenceNo': payment_resp[1],
        'BankReferenceNo': payment_resp[2],
        'TxnAmount': payment_resp[3],
        'BankId': payment_resp[4],
        'BankMerchantID': payment_resp[5],
        'TxnType': payment_resp[6],
        'CurrencyName': payment_resp[7],
        'ItemCode': payment_resp[8],
        'SecurityType': payment_resp[9],
        'SecurityID': payment_resp[10],
        'SecurityPassword': payment_resp[11],
        'TxnDate': payment_resp[12],
        'AuthStatus': payment_resp[13],
        'SettlementType': payment_resp[14],
        'AdditionalInfo1': payment_resp[15],
        'AdditionalInfo2': payment_resp[16],
        'AdditionalInfo3': payment_resp[17],
        'AdditionalInfo4': payment_resp[18],
        'AdditionalInfo5': payment_resp[19],
        'AdditionalInfo6': payment_resp[20],
        'AdditionalInfo7': payment_resp[21],
        'ErrorStatus': payment_resp[22],
        'ErrorDescription': payment_resp[23]
    }
    request_logger.info('\nCIGNA TTK TRANSACTION: %s \n PAYMENT RESPONSE RECEIVED:\n %s \n' % (transaction.transaction_id, response_data))
    transaction.extra['payment_response'] = response_data
    transaction.payment_token = response_data['TxnReferenceNo']
    transaction.premium_paid = transaction.extra['payment_response']['TxnAmount']
    transaction.payment_done = True
    transaction.payment_on = datetime.datetime.today()
    transaction.policy_number = response_data['PolicyNumber']
    transaction.extra['payment_mode'] = response_data['AdditionalInfo7']
    transaction.save()
    is_redirect = True
    redirect_url = '/health-plan/payment/transaction/%s/failure/' %transaction.transaction_id

    form_data = transaction.insured_details_json['form_data']
    insured_data = form_data['insured']



    if transaction.extra['payment_response']['AuthStatus'] == '0300':
        redirect_url = '/health-plan/payment/transaction/%s/success/' % transaction.transaction_id
        #Proposal
        proposal = get_xml_formatted_proposal_request(transaction, payment_response)
        listofPolicyTO = prod_utils.data2xml(proposal, 'listofPolicyTO')
        saveProposalReceipt = etree.Element('{http://syminterface.insurance.symbiosys.c2lbiz.com}saveProposalReceipt')
        saveProposalReceipt.append(listofPolicyTO)
        saveProposal = etree.Element('{http://syminterface.insurance.symbiosys.c2lbiz.com}saveProposal')
        saveProposal.append(saveProposalReceipt)
        Body = etree.Element('{http://schemas.xmlsoap.org/soap/envelope/}Body')
        Body.append(saveProposal)
        Envelope = etree.Element('{http://schemas.xmlsoap.org/soap/envelope/}Envelope')
        Envelope.append(Body)
        request = etree.tostring(Envelope)
        http = httplib2.Http(disable_ssl_certificate_validation=True)
        headers = {'Content-type':'text/xml'}
        request_logger.info('\nCIGNA TTK TRANSACTION: %s \n PROPOSAL REQUEST SENT:\n %s \n' % (transaction.transaction_id, request))
        response_header, response_content = http.request(PROPOSAL_PAYMENT_WSDL, method='POST', body=request, headers=headers)
        request_logger.info('\nCIGNA TTK TRANSACTION: %s \n PROPOSAL RESPONSE RECEIVED:\n %s \n' % (transaction.transaction_id, response_content))
        proposal_resp_dict = prod_utils.complex_etree_to_dict(ET.fromstring(response_content))
        proposal_data_dict = proposal_resp_dict['{http://schemas.xmlsoap.org/soap/envelope/}Envelope']['{http://schemas.xmlsoap.org/soap/envelope/}Body']['{http://syminterface.insurance.symbiosys.c2lbiz.com}saveProposalResponse']['{http://syminterface.insurance.symbiosys.c2lbiz.com}return']['{http://io.syminterface.insurance.symbiosys.c2lbiz.com/xsd}listofPolicyTO']

        #Retrieve Receipt ID from the proposal Response
        inwardDOList = proposal_data_dict['{http://transferobjects.syminterface.insurance.symbiosys.c2lbiz.com/xsd}inwardDOList']
        receiptId = inwardDOList['{http://transferobjects.syminterface.insurance.symbiosys.c2lbiz.com/xsd}receiptId']
        transaction.extra['receipt_id'] = receiptId

        request_logger.info('\nCIGNA TTK TRANSACTION: %s \n PROPOSAL RECEIPT ID:\n %s \n' % (transaction.transaction_id, receiptId))

        partyList = proposal_data_dict['{http://transferobjects.syminterface.insurance.symbiosys.c2lbiz.com/xsd}partyDOList']


        guid_to_custid = {}
        proposer_cust_id = ''
        guid_to_ppmc = {}


        if type(partyList) is list:
            for party in partyList:
                if party['{http://transferobjects.syminterface.insurance.symbiosys.c2lbiz.com/xsd}roleCd'] == 'PROPOSER':
                    cust_id = party['{http://transferobjects.syminterface.insurance.symbiosys.c2lbiz.com/xsd}customerId']
                    proposer_cust_id =  cust_id if ((type(cust_id) is not dict) or (cust_id is None)) else ''

        policyProductDOList = proposal_data_dict['{http://transferobjects.syminterface.insurance.symbiosys.c2lbiz.com/xsd}policyProductDOList']
        policyProductInsuredDOList = policyProductDOList['{http://transferobjects.syminterface.insurance.symbiosys.c2lbiz.com/xsd}policyProductInsuredDOList']

        if type(policyProductInsuredDOList) is list:
            for insured in policyProductInsuredDOList:
                party_id = insured['{http://transferobjects.syminterface.insurance.symbiosys.c2lbiz.com/xsd}refGuid']
                cust_id = insured['{http://transferobjects.syminterface.insurance.symbiosys.c2lbiz.com/xsd}customerId']
                ppmc = insured['{http://transferobjects.syminterface.insurance.symbiosys.c2lbiz.com/xsd}ppmcFl']
                if (type(cust_id) is dict) or (cust_id is None):
                    cust_id = ''
                guid_to_custid[party_id] = cust_id
                guid_to_ppmc[party_id] = ppmc
        else:
            cust_id = policyProductInsuredDOList['{http://transferobjects.syminterface.insurance.symbiosys.c2lbiz.com/xsd}customerId']
            party_id = policyProductInsuredDOList['{http://transferobjects.syminterface.insurance.symbiosys.c2lbiz.com/xsd}refGuid']
            ppmc = policyProductInsuredDOList['{http://transferobjects.syminterface.insurance.symbiosys.c2lbiz.com/xsd}ppmcFl']
            if (type(cust_id) is dict) or (cust_id is None):
                cust_id = ''
            guid_to_custid[party_id] = cust_id
            guid_to_ppmc[party_id] = ppmc



        transaction.extra['guid_to_custid'] = guid_to_custid
        transaction.extra['guid_to_ppmc'] = guid_to_ppmc
        transaction.extra['proposer_cust_id'] = proposer_cust_id
        transaction.save()


        if not transaction.extra['stp']['policy']:
            return is_redirect, redirect_url, '', {}

        attach_pdf(transaction)
    else:
        transaction.extra['error_msg'] = response_data['ErrorDescription']
    transaction.save()
    return True, redirect_url,'',{}


def attach_pdf(transaction):
    try:
        template_dict = get_template_dict(transaction)
        template_dict['receiptId'] = transaction.extra['receipt_id']
        template_dict['payment_mode'] = transaction.extra['payment_mode']
        template_dict['today'] = datetime.datetime.now().strftime('%d/%m/%Y')

        file_path = path(__file__).parent.abspath() + '/template.html'
        with open(file_path, 'r') as f:
            html = f.read()

        html_template = template.Template(html)
        context = template.Context(template_dict)
        full_html = html_template.render(context)

        if not os.path.exists(settings.POLICY_ROOT):
            os.makedirs(settings.POLICY_ROOT)

        html_file_path = '/tmp/policyTemplate_%s.html' % transaction.id
        with open(html_file_path, 'w') as f:
            f.write(full_html)

        from_path = '/tmp/policyTemplate_%s.html' % transaction.id

        pdf_content = utils.html_to_pdf(from_path)
        transaction.update_policy(pdf_content, send_mail=False)
    except Exception as E:
        transaction.extra['message'] = 'Policy was not generated'
        request_logger.info('\n === CIGNA TTK ERROR: %s === \n%s\n' % (transaction.transaction_id, logger.exception(E)));
