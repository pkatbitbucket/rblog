import json


from suds.sudsobject import asdict
from dateutil.relativedelta import relativedelta
from wiki.models import *
from importlib import import_module
import health_product.insurer.settings as insurer_settings


DEFAULT_NOMINEE_RELATIION_OPTIONS = [
    {'id' : 'Father', 'name' : 'Father'},
    {'id' : 'Mother', 'name' : 'Mother'},
    {'id' : 'Spouse', 'name' : 'Spouse'},
    {'id' : 'Son', 'name' : 'Son'},
    {'id' : 'Daughter', 'name' : 'Daughter'},
    {'id' : 'Brother', 'name' : 'Brother'},
    {'id' : 'Sister', 'name' : 'Sister'},
    {'id' : 'Nephew', 'name' : 'Nephew'},
    {'id' : 'Niece', 'name' : 'Niece'},
    {'id' : 'Uncle', 'name' : 'Uncle'},
    {'id' : 'Aunt', 'name' : 'Aunt'},
    {'id' : 'Other', 'name' : 'Other'}
]

DEFAULT_NOMINEE_RELATIION_OPTIONS.sort(lambda x, y: -1 if x['name'] < y['name'] else 1)


def recursive_asdict(d):
    """Convert Suds object into serializable format."""
    out = {}
    for k, v in asdict(d).iteritems():
        if hasattr(v, '__keylist__'):
            out[k] = recursive_asdict(v)
        elif isinstance(v, list):
            out[k] = []
            for item in v:
                if hasattr(item, '__keylist__'):
                    out[k].append(recursive_asdict(item))
                else:
                    out[k].append(item)
        else:
            out[k] = str(v)
    return out


def suds_to_dict(data):
    return recursive_asdict(data)


class CustomModel():

    def __init__(self):
        pass

    def free_field(self):
        f = {
            'label': '',
            'field_type': '',
            'code': '',
            'event': '',
            'span': 4,
            'value': None,
            'sub_text': None,
            'min_length': 0,
            'max_length': 50,
            'only_if': 'obj.code()',
            'css_class': '',
            'template_option': 'dynamic',
            'required': True
        }
        return f

    def CharField(self):
        f = self.free_field()
        f['email'] = False
        return f.copy()

    def TextField(self):
        f = self.free_field()
        f['email'] = False
        return f.copy()

    def StrictCharField(self):
        f = self.free_field()
        return f.copy()

    def NameField(self):
        f = self.free_field()
        return f.copy()

    def IntegerField(self):
        f = self.free_field()
        return f.copy()

    def MobileField(self):
        f = {
            'label': '',
            'field_type': '',
            'code': '',
            'event': '',
            'value': '',
            'span': 4,
            'only_if': 'obj.code()',
            'template_option': 'dynamic',
            'required': True
        }
        return f

    def PANField(self):
        f = {
                'label': '',
                'field_type': '',
                'code': '',
                'event': '',
                'value': '',
                'span': 4,
                'only_if': 'obj.code()',
                'template_option': 'dynamic',
                'required': True
            }
        return f

    def PANorAdhaarField(self):
        f = {
                'label': '',
                'field_type': '',
                'code': '',
                'event': '',
                'value': '',
                'span': 4,
                'only_if': 'obj.code()',
                'template_option': 'dynamic',
                'required': True
            }
        return f

    def LandlineField(self):
        f = {
            'label': '',
            'field_type': '',
            'code': '',
            'event': '',
            'std_code': '',
            'landline': '',
            'value': '',
            'span': 4,
            'only_if': 'obj.code()',
            'template_option': 'dynamic',
            'required': True
        }
        return f

    def DropDownField(self):
        f = {
            'label': '',
            'field_type': '',
            'code': '',
            'event': '',
            'span': 4,
            'value': None,
            'sub_text': None,
            'options': [],
            'only_if': 'obj.code()',
            'template_option': 'dynamic',
            'required': True
        }
        return f.copy()

    def SearchableDropDownField(self):
        f = {
            'label': '',
            'field_type': '',
            'code': '',
            'event': '',
            'span': 4,
            'value': None,
            'sub_text': None,
            'options': [],
            'only_if': 'obj.code()',
            'template_option': 'dynamic',
            'required': True
        }
        return f.copy()

    def DateTextField(self):
        today = datetime.datetime.today()
        f = {
            'label': '',
            'field_type': '',
            'code': '',
            'event': '',
            'value': '',
            'span': 4,
            'min_date': datetime.datetime.strftime(today - relativedelta(years=100), '%d/%m/%Y'),
            'max_date': datetime.datetime.strftime(today - relativedelta(years=18, days=0), '%d/%m/%Y'),
            'format': 'dd/mm/yyyy',
            'only_if': 'obj.code()',
            'template_option': 'dynamic',
            'required': True
        }
        return f.copy()

    def DateField(self):
        today = datetime.datetime.today()
        f = {
            'label': '',
            'field_type': '',
            'code': '',
            'event': '',
            'value': '',
            'span': 4,
            'format': 'dd/mm/yyyy',
            'only_if': 'obj.code()',
            'template_option': 'dynamic',
            'required': True
        }
        return f.copy()
    def TwoDropDownField(self):
        f = {
            'label': '',
            'field_type': '',
            'code': '',
            'event': '',
            'value1': None,
            'value2': None,
            'span': 4,
            'span1': 6,
            'span2': 6,
            'options1': [],
            'options2': [],
            'only_if': 'obj.code()',
            'template_option': 'dynamic',
            'required': True
        }
        return f.copy()


    def TwoDropDownDependantField(self):
        f = {
            'label': '',
            'field_type': '',
            'code': '',
            'event': '',
            'value1': None,
            'dependant_value': None,
            'span': 4,
            'span1': 6,
            'span2': 6,
            'options1': [],
            'options2': [],
            'only_if': 'obj.code()',
            'template_option': 'dynamic',
            'required': True
        }
        return f.copy()

    def TwoSearchableDDDField(self):
        f = {
            'label': '',
            'field_type': '',
            'code': '',
            'event': '',
            'value1': None,
            'dependant_value': None,
            'span': 4,
            'span1': 6,
            'span2': 6,
            'options1': [],
            'options2': [],
            'only_if': 'obj.code()',
            'template_option': 'dynamic',
            'required': True
        }
        return f.copy()

    def DropDownDependantField(self):
        f = {
            'label': '',
            'field_type': '',
            'code': '',
            'event': '',
            'value1': None,
            'dependant_value': None,
            'span': 4,
            'options1': [],
            'options2': [],
            'only_if': 'obj.code()',
            'template_option': 'dynamic',
            'required': True
        }
        return f.copy()

    def PremiumDiscountField(self):
        f = {
            'label': '',
            'field_type': '',
            'code': '',
            'event': '',
            'value1': 1,
            'dependant_value': None,
            'span': 4,
            'options1': [],
            'options2': [],
            'only_if': 'obj.code()',
            'css_class': '',
            'template_option': 'dynamic',
            'required': True
        }
        return f.copy()

    def MultipleChoiceField(self):
        f = {
            'label': '',
            'help_text': '',
            'field_type': '',
            'code': '',
            'event': '',
            'span': 4,
            'value': None,
            'sub_text': None,
            'options': [
                {'id': 'Yes', 'name': 'Yes', 'is_correct': False},
                {'id': 'No', 'name': 'No', 'is_correct': True}
            ],
            'only_if': 'obj.code()',
            'template_option': 'dynamic',
            'required': True
        }
        return f.copy()

    def MultipleAnswerField(self):
        f = {
            'label': '',
            'field_type': '',
            'code': '',
            'event': '',
            'span': 4,
            'value': None,
            'sub_text': None,
            'options': [],
            'only_if': 'obj.code()',
            'template_option': 'dynamic',
            'required': True
        }
        return f.copy()

    def MultipleSelectField(self):
        f = {
            'label': '',
            'field_type': '',
            'code': '',
            'event': '',
            'span': 4,
            'value': None,
            'sub_text': None,
            'options': [],
            'only_if': 'obj.code()',
            'template_option': 'dynamic',
            'required': True
        }
        return f.copy()

class ApplyDefault():
    def __init__(self, saved_data, f):
        self.saved_data = saved_data
        self.f = f

    def free_field(self):
        self.f['default'] = self.saved_data[self.f['code']].get('value', self.f['value']) if self.saved_data.get(self.f['code'], None) else self.f['value']
        return self.f

    def CharField(self):
        f = self.free_field()
        return f

    def TextField(self):
        f = self.free_field()
        return f

    def StrictCharField(self):
        f = self.free_field()
        return f

    def NameField(self):
        f = self.free_field()
        return f

    def IntegerField(self):
        f = self.free_field()
        return f

    def DateTextField(self):
        self.f['default'] = self.saved_data[self.f['code']].get('value', self.f['value']) if self.saved_data.get(self.f['code'], None) else self.f['value']
        return self.f

    def DateField(self):
        self.f['default'] = self.saved_data[self.f['code']].get('value', self.f['value']) if self.saved_data.get(self.f['code'], None) else self.f['value']
        return self.f

    def MobileField(self):
        self.f['default'] = self.saved_data[self.f['code']].get('value', self.f['value']) if self.saved_data.get(self.f['code'], None) else self.f['value']
        return self.f

    def PANField(self):
        self.f['default'] = self.saved_data[self.f['code']].get('value', self.f['value']) if self.saved_data.get(self.f['code'], None) else self.f['value']
        return self.f

    def PANorAdhaarField(self):
        self.f['default'] = self.saved_data[self.f['code']].get('value', self.f['value']) if self.saved_data.get(self.f['code'], None) else self.f['value']
        return self.f


    def LandlineField(self):
        self.f['default_std_code'] = self.saved_data[self.f['code']].get('std_code', self.f['std_code']) if self.saved_data.get(self.f['code'], None) else self.f['std_code']
        self.f['default_landline'] = self.saved_data[self.f['code']].get('landline', self.f['landline']) if self.saved_data.get(self.f['code'], None) else self.f['landline']
        return self.f

    def MultipleChoiceField(self):
        try:
            self.f['default'] = self.saved_data[self.f['code']].get('value', self.f['value']) if self.saved_data.get(self.f['code'], None) else self.f['value']
        except KeyError:
            self.f['default'] = self.f['value']
        return self.f

    def MultipleAnswerField(self):
        self.f['default'] = self.saved_data[self.f['code']].get('value', self.f['value']) if self.saved_data.get(self.f['code'], None) else self.f['value']
        return self.f

    def DropDownField(self):
        self.f['default'] = self.saved_data[self.f['code']].get('value', self.f['value']) if self.saved_data.get(self.f['code'], None) else self.f['value']
        return self.f

    def SearchableDropDownField(self):
        self.f['default'] = self.saved_data[self.f['code']].get('value', self.f['value']) if self.saved_data.get(self.f['code'], None) else self.f['value']
        return self.f

    def TwoDropDownField(self):
        self.f['default1'] = self.saved_data[self.f['code']].get('value1', self.f['value1']) if self.saved_data.get(self.f['code'], None) else self.f['value1']
        self.f['default2'] = self.saved_data[self.f['code']].get('value2', self.f['value2']) if self.saved_data.get(self.f['code'], None) else self.f['value2']
        return self.f

    def DropDownDependantField(self):
        self.f['default1'] = self.saved_data[self.f['code']].get('value1', self.f['value1']) if self.saved_data.get(self.f['code'], None) else self.f['value1']
        self.f['default2'] = self.saved_data[self.f['code']].get('dependant_value', self.f['dependant_value']) if self.saved_data.get(self.f['code'], None) else self.f['dependant_value']
        return self.f

    def TwoDropDownDependantField(self):
        self.f['default1'] = self.saved_data[self.f['code']].get('value1', self.f['value1']) if self.saved_data.get(self.f['code'], None) else self.f['value1']
        self.f['default2'] = self.saved_data[self.f['code']].get('dependant_value', self.f['dependant_value']) if self.saved_data.get(self.f['code'], None) else self.f['dependant_value']
        return self.f

    def TwoSearchableDDDField(self):
        self.f['default1'] = self.saved_data[self.f['code']].get('value1', self.f['value1']) if self.saved_data.get(self.f['code'], None) else self.f['value1']
        self.f['default2'] = self.saved_data[self.f['code']].get('dependant_value', self.f['dependant_value']) if self.saved_data.get(self.f['code'], None) else self.f['dependant_value']
        return self.f

    def PremiumDiscountField(self):
        self.f['default1'] = self.saved_data[self.f['code']].get('value1', self.f['value1']) if self.saved_data.get(self.f['code'], None) else self.f['value1']
        return self.f

    def MultipleSelectField(self):
        self.f['default'] = self.saved_data[self.f['code']].get('value', self.f['value']) if self.saved_data.get(self.f['code'], None) else self.f['value']
        return self.f

def new_field(**kwargs):
    # Creates a base structure for fields
    obj = CustomModel()
    f = getattr(obj, kwargs['field_type'], '')()
    f.update(kwargs)

    # Applies default values from saved data
    saved_data = kwargs.get('saved_data', {})
    appdef = ApplyDefault(saved_data, f)
    app_def_func = getattr(appdef, kwargs['field_type'], None)
    if app_def_func:
        ff = app_def_func()
    else:
        ff = f
    return ff


def clean_stage_data(stage_data):
    cleaned_data = {}
    for segment in stage_data['segments']:
        if segment['segment'] not in cleaned_data.keys():
            cleaned_data[segment['segment']] = {}
        if segment['tag'] not in cleaned_data[segment['segment']].keys():
            cleaned_data[segment['segment']][segment['tag']] = {}
        data = {}
        for row in segment['rows']:
            for element in row['elements']:
                key_list = [
                    'code',
                    'value',
                    'value1',
                    'value2',
                    'std_code',
                    'landline',
                    'display_value',
                    'display_vlaue1',
                    'display_value2',
                    'dependant_value'
                ]
                data[element['code']] = {k: (v if (v or v==0) else '') for k, v in element.items() if k in key_list}
                data[element['code']]['label'] = element['data']['label']
        cleaned_data[segment['segment']][segment['tag']].update(**data)
    return cleaned_data


def get_full_name(**kwargs):
    name_list = []
    if 'first_name' in kwargs.keys() and kwargs['first_name'].strip():
        name_list.append(kwargs['first_name'].strip().title())
    if 'middle_name' in kwargs.keys() and kwargs['middle_name'].strip():
        name_list.append(kwargs['middle_name'].strip().title())
    if 'last_name' in kwargs.keys() and kwargs['last_name'].strip():
        name_list.append(kwargs['last_name'].strip().title())
    return ' '.join(name_list)


def standard_form_data_cleanup(transaction):
    is_self_present = False
    marital_status = 'SINGLE'
    proposer_data = {
        'pincode': transaction.extra['pincode'],
        'city': City.objects.get(id=transaction.extra['city']['id']).name,
        'state': City.objects.get(id=transaction.extra['city']['id']).state.name,
        'email': '',
        'gender': 1 if transaction.extra['gender'] == 'MALE' else 2
    }
    transaction.proposer_gender = 1 if transaction.extra['gender'] == 'MALE' else 2
    for i in transaction.extra['members']:
        i['person'] = i['person'].lower()
        if i['person'] == 'you':
            i['person'] = 'self'
            i['id'] = 'self'
        i['verbose'] = i['displayName']
        if 'dob' in i.keys() and i['dob']:
            i['dob'] = i['dob'].replace('-', '/')
            i['age'] = relativedelta(datetime.datetime.today(), datetime.datetime.strptime(i['dob'], '%d/%m/%Y').date()).years

        if i['person'].lower() in ['son', 'father']:
            i['gender'] = 1

        elif i['person'].lower() in ['daughter', 'mother']:
            i['gender'] = 2

        elif i['person'].lower() == 'self':
            i['gender'] = 1 if transaction.extra['gender'] == 'MALE' else 2
            is_self_present = True
            if 'dob' in i.keys() and i['dob']:
                proposer_data['dob'] = i['dob']
                transaction.proposer_date_of_birth = datetime.datetime.strptime(i['dob'], '%d/%m/%Y').date()

        elif i['person'].lower() == 'spouse':
            i['gender'] = 2 if transaction.extra['gender'] == 'MALE' else 1

        if i['person'].lower() in ['son', 'daughter', 'spouse']:
            marital_status = 'MARRIED'
            transaction.proposer_marital_status = 'MARRIED'

    proposer_data['marital_status'] = marital_status
    slab = transaction.slab
    plan_data = {
        'insurer_id': slab.health_product.insurer.id,
        'plan_name': slab.health_product.title,
        'cover': slab.sum_assured,
        'medical_firewall': False
    }
    if slab.health_product_id in insurer_settings.INSURER_SPECIFIC_SCORE:
        hp_score = import_module('health_product.insurer.%s.score' % insurer_settings.INSURER_MAP[slab.health_product.insurer_id])
        data =  {
                'gender': transaction.extra['gender'],
                'adults': 0,
                'age': 0,
                'city': '',
                'deductible': slab.deductible,
                'family': True,
                'kid_ages': [],
                'kid_genders': [],
                'kids': 0,
                'parent_ages': [],
                'parent_genders': [],
                'parents': 0,
                'spouse_age': 0,
                'sum_assured': slab.sum_assured,
                'you_and_spouse': 0,
                'you_and_spouse_ages': [],
                'you_and_spouse_genders': [],
        }
        GENDERS = {
            1: 'MALE',
            2: 'FEMALE'
        }
        data['city']=City.objects.get(id=transaction.extra['city']['id'])
        members=transaction.extra['members']
        for m in members:
            if m['type']=='adult':
                data['adults']+=1
            elif m['type']=='kid':
                data['kids']+=1
                if m['age']==0:
                    data['kid_ages'].append(m['age_in_months']/1000.0);
                else:
                    data['kid_ages'].append(m['age']);
                data['kid_genders'].append(GENDERS[m['gender']])

            if m['person'] in ['self','spouse']:
                data['you_and_spouse']+=1
                data['you_and_spouse_ages'].append(m['age'])
                data['you_and_spouse_genders'].append(GENDERS[m['gender']])
            if m['person'] in ['father','mother']:
                data['parents']+=1
                data['parent_ages'].append(m['age'])
                data['parent_genders'].append(GENDERS[m['gender']])
            if m['person']=='spouse':
                data['spouse_age']=m['age']
            data['age']= data['age'] if data['age']>=m['age'] else m['age']
        data['age'] *= 1000

        transaction.extra['cpt_data'] = data
        transaction.extra['cpt_data']['city'] = data['city'].id
        transaction.save()

        cpt = hp_score.getCoverPremiumTable(slab, data)
    else:
        if slab.health_product.policy_type == "FAMILY":
            cpt = FamilyCoverPremiumTable.objects.get(id=transaction.extra['cpt_id'])
            plan_data['category'] = '%sA-%sC' % (cpt.adults, cpt.kids)
        else:
            cpt = CoverPremiumTable.objects.get(id=transaction.extra['cpt_id'])
            plan_data['category'] = ''
    plan_data['premium'] = cpt.premium
    transaction.extra['plan_data'] = plan_data
    transaction.save()
    return transaction, plan_data, proposer_data, is_self_present


def get_age_values(member):
    today = datetime.datetime.today()
    if member['person'].lower() in ['father', 'mother', 'self', 'spouse']:
        min_age = 18
        max_age = 120
        min_date = datetime.datetime.strftime(today - relativedelta(years=100), '%d/%m/%Y')
        max_date = datetime.datetime.strftime(today - relativedelta(years=(min_age + 1), days=-1), '%d/%m/%Y')
    else:
        min_age = 0
        max_age = 25
        min_date = datetime.datetime.strftime(today - relativedelta(years=26), '%d/%m/%Y')
        max_date = datetime.datetime.strftime(datetime.datetime.today(), '%d/%m/%Y')
    try:
        age = member['age']
        # min_date = datetime.datetime.strftime(today - relativedelta(years=(int(age) + 1), days=-1), '%d/%m/%Y')
        # max_date = datetime.datetime.strftime(today - relativedelta(years=int(age)), '%d/%m/%Y')
        min_date = datetime.datetime.strftime(today - relativedelta(years=(int(age) + 2), days=-1), '%d/%m/%Y')
        max_date = datetime.datetime.strftime(today - relativedelta(years=int(age) - 1), '%d/%m/%Y')
    except Exception as e:
        print str(e)
    return min_date, max_date, min_age, max_age
