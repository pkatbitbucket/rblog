import health_product.prod_utils as prod_utils


def download_data(t):
    big_header = None
    filename = "proposal_%s.xls" % t.transaction_id
    data_row_list, order = prod_utils.extract_transaction_data(t)

    data = {
        'data_row_list' : data_row_list,
        'order' : order,
        'filename' : filename,
        'big_header' : big_header
    }
    return data

