import json

import utils
from core import activity as core_activity
from health_product.insurer import custom_utils
from health_product import prod_utils
from leads.views import save_call_time


def save_stage(transaction, stage_data):
    if 'red_flags' not in transaction.extra:
        transaction.extra['red_flags'] = {}
    transaction.extra['red_flags'][stage_data['template']] = {
        'reason': 'DISEASE',
        'data': [],
    }

    form_data = custom_utils.clean_stage_data(stage_data)

    for k, v in form_data.items():
        if 'form_data' not in transaction.insured_details_json:
            transaction.insured_details_json['form_data'] = {}
        if k in transaction.insured_details_json['form_data']:
            transaction.insured_details_json['form_data'][k].update(**v)
        else:
            transaction.insured_details_json['form_data'][k] = v
        if 'contact' in v:
            if 'mobile' in v['contact']:
                transaction.proposer_mobile = v['contact']['mobile']['value']
            if 'email' in v['contact']:
                transaction.proposer_email = v['contact']['email']['value']
    transaction.save()

    if 'proposer' in form_data and 'contact' in form_data['proposer']:
        transaction.track_activity(
            activity_type=core_activity.VIEWED,
            page_id='proposal'
        )
        request = utils.get_request()
        request.POST = {}
        request.POST['data'] = json.dumps({
            'mobile': transaction.proposer_mobile,
            'email': transaction.proposer_email,
            'transaction_id': transaction.transaction_id,
        })
        save_call_time(request)
    return transaction
