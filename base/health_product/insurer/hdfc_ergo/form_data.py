from wiki.models import City, Pincode
from collections import OrderedDict
from health_product.insurer import custom_utils
from health_product.insurer.hdfc_ergo import preset

class Formic():
    def __init__(self, transaction, is_self_present):
        self.transaction = transaction
        self.is_self_present = is_self_present
        self.proposer_data = {
            'pincode': transaction.extra['pincode'],
            'city' : City.objects.get(id=transaction.extra['city']['id']).name,
            'state' : City.objects.get(id=transaction.extra['city']['id']).state.name,
            'email': ''
        }


    def add_parent_segment(self, segment_id, tag_id, details, question_type):
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': details, 'data': []}
        row = []
        if question_type == 'MultipleChoiceField':
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='MultipleChoiceField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    details=member,
                    label=details.get('question_text', ''),
                    value='No',
                    span=4,
                    css_class='bottom-margin',
                    event=', click : $parent.root_context.stage_5.any_yes_check',
                    template_option='question'
                ))
            new_segment['data'].append(row)
        elif question_type == 'DropDownField':
            if tag_id == 'pre_existing':
                for member in self.transaction.extra['members']:
                    row.append(custom_utils.new_field(
                        saved_data=saved_data,
                        field_type='DropDownField',
                        code='%s_question_%s' % (member['id'], tag_id),
                        options=self.pre_existing_options,
                        label=details.get('question_text', ''),
                        details=member,
                        span=2,
                        css_class='bottom-margin',
                        event=', event : {change : root_context.stage_5.any_yes_check}',
                        template_option='question'
                    ))
            new_segment['data'].append(row)
        elif question_type == 'TextField':
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='TextField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    label=details.get('question_text', ''),
                    span=2,
                    required=False,
                    ))
            new_segment['data'].append(row)

        return new_segment

    def stage_1(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'personal'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            label='First Name',
            code='first_name',
            ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            label='Last Name',
            code='last_name'
        ))
        if self.is_self_present:
            member = [m for m in self.transaction.extra['members'] if m['id'] == 'self'][0]
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                min_age=18,
                max_age=100,
                default_age=member['age'],
            ))
        else:
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                min_age=18,
                max_age=100,
                default_age=18,
            ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Gender',
            code='gender',
            options=preset.GENDER_OPTIONS,
            value=1 if self.transaction.extra['gender'] == 'MALE' else 2,
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)

        return {'template' : 'stage_1', 'data' : stage}


    def stage_2(self):
        stage = []

        for member in self.transaction.extra['members']:
            segment_id = 'insured'
            tag_id = member['id']
            template_switch = 'dynamic'
            if tag_id == 'self':
                template_switch = 'static'
            try:
                saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : member, 'data' : []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                label='First Name',
                code='first_name',
                max_length=50,
                span=3,
                template_option=template_switch
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                label='Last Name',
                code='last_name',
                max_length=50,
                span=3,
                template_option=template_switch
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                span=3,
                template_option=template_switch,
                default_age = member['age'],
                min_age = 18 if member['type']=='adult' else 0,
                max_age = 100,
            ))
            new_segment['data'].append(row)
            if member['age'] < 21:
                doc_options = []
                if member['age'] < 3:
                    doc_options = preset.DOCUMENT_TYPES[:-3]
                elif member['age'] < 10:
                    doc_options = preset.DOCUMENT_TYPES[:-2]
                elif member['age'] < 18:
                    doc_options = preset.DOCUMENT_TYPES[:-1]
                else:
                    doc_options = preset.DOCUMENT_TYPES

                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='DropDownField',
                    label='Document',
                    code='required_docs',
                    options=doc_options,

                ))
            else:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='PANorAdhaarField',
                    label='PAN/Aadhaar Card No.',
                    code='pancard_no',
                ))
            stage.append(new_segment)
        return {'template' : 'stage_2', 'data' : stage}

    def stage_3(self):
        relation_options = [
            {'id': 'B', 'name': 'Sibling'},
            {'id': 'C', 'name': 'Child'},
            {'id': 'E', 'name': 'Niece'},
            {'id': 'G', 'name': 'GrandParent'},
            {'id': 'H', 'name': 'GrandChild'},
            {'id': 'K', 'name': 'Brother-in-Law'},
            {'id': 'L', 'name': 'Sister-in-Law'},
            {'id': 'N', 'name': 'Nephew'},
            {'id': 'P', 'name': 'Parent'},
            {'id': 'S', 'name': 'Spouse'},
        ]
        topup_relation_options = relation_options
        default_relation = {
            'self': 'Spouse',
            'spouse': 'Spouse',
            'son': 'Parent',
            'daughter': 'Parent',
            'father': 'Child',
            'mother': 'Child'
        }
        self_relation_probables = OrderedDict([
            ('mother', 'Parent'),
            ('father', 'Parent'),
            ('spouse', 'Spouse')
        ])
        relations = {
            'indemnity': relation_options,
            'topup': topup_relation_options,
        }

        product_type = preset.PRODUCT_TYPES[self.transaction.slab.health_product.id]
        rel = relations[product_type]
        rel.sort(lambda x, y: -1 if x['name'] < y['name'] else 1)
        #Needs to run before the big loop on members
        for member in self.transaction.extra['members']:
            if member['person'] in self_relation_probables.keys():
                self.transaction.extra['self_nominee'] = member['id']
                self.transaction.save()
                default_relation['self'] = self_relation_probables[member['person']]
        stage = []
        for member in self.transaction.extra['members']:
            segment_id = 'nominee'
            tag_id = member['id']
            try:
                saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : member, 'data' : []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DropDownField',
                code='relation',
                label='Relationship With %s' % member['verbose'],
                options=rel,
                value=default_relation[member['person']],
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                code='first_name',
                label='First Name',
                max_length=50,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                code='last_name',
                label='Last Name',
                max_length=50,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                code='date_of_birth',
                label='Date Of Birth',
                min_age=18,
                span=3
            ))
            new_segment['data'].append(row)
            stage.append(new_segment)
        return {'template' : 'stage_3', 'data' : stage}

    def stage_4(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'address'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address1',
            label='Flat/House No, Street',
            span=12,
            css_class='bottom-margin',
            max_length=50
        ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address2',
            label='Area/Locality',
            span=12,
            css_class='bottom-margin',
            max_length=100,
            ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='landmark',
            label='Landmark',
            span=12,
            css_class='bottom-margin',
            max_length=100,
            required=False
        ))
        new_segment['data'].append(row)

        if self.transaction.slab.health_product.coverage_category == 'SUPER_TOPUP':
            row = []
            pincode = Pincode.objects.filter(pincode=self.transaction.extra['pincode'])[0]
            state = pincode.state
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                code='pincode',
                label='Pincode',
                span=4,
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                code='state',
                label='State',
                span=4,
                template_option='static'
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='SearchableDropDownField',
                code='city',
                label='City',
            ))
            new_segment['data'].append(row)
        else:
            row = []
            pincode = Pincode.objects.filter(pincode=self.transaction.extra['pincode'])[0]
            state = pincode.state
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='SearchableDropDownField',
                code='city',
                label='City',
                options=preset.CITY_DICT[state.id],
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                code='state',
                label='State',
                value=self.proposer_data['state'],
                span=4,
                template_option='static'
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                code='pincode',
                label='Pincode',
                value=str(self.proposer_data['pincode']),
                span=4,
                template_option='static'
            ))
            new_segment['data'].append(row)
        stage.append(new_segment)


        tag_id = 'contact'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='email',
            label='Email',
            span=12,
            css_class='bottom-margin',
            email=True
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MobileField',
            code='mobile',
            label='Mobile',
            css_class='bottom-margin',
            span=12
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='LandlineField',
            code='landline',
            label='Landline',
            span=12,
            css_class='bottom-margin',
            required=False
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template' : 'stage_4', 'data' : stage}

    def stage_5(self):
        stage = []
        #---------- Question Unit Begins -----------
        question_code = 'pre_existing_details'
        tag_id = 'details'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Please provide details if you have any pre-existing diseases',
            'help_text': 'If you don\'t have any pre-existing illness, then leave this field blank',
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'TextField'))
        return {'template' : 'stage_5', 'data' : stage}


    def stage_6(self):
        stage = []
        segment_id = 'terms'
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Payment Mode',
            code='payment_mode',
            options=[
                {'id': 'CC', 'name': 'Credit Card / Debit Card'},
                {'id': 'DD', 'name': 'Internet Banking'}
            ]
        ))
        new_segment['data'].append(row)
        row = []
        t_and_c_label = ('I hereby confirm<br />'
                         '- I have read and agree to the Terms and Conditions of ')
        if self.transaction.slab.health_product.policy_wordings:
            t_and_c_label += ('the <a style="text-decoration:none;cursor:pointer;" '
                              'onclick=\'window.open("%s","","width=400, height=400")\'>policy</a> and '
                             ) % self.transaction.slab.health_product.policy_wordings.url
        t_and_c_label += ('the <a style="cursor:pointer;" class="call_overlay">website</a>;<br />'
                          '- the declarations made herewith are true and correct;<br />'
                          '- appointment of Coverfox Insurance Brokers Pvt. Ltd. as my Insurance Broker, '
                          'and authorize them to represent me to Insurance Companies/Agencies for my Insurance needs.'
                         )
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='t_and_c',
            label=t_and_c_label,
            span=12,
            options=[
                {'id' : 'Yes', 'name' : 'Yes', 'is_correct' : True},
                {'id' : 'No', 'name' : 'No', 'is_correct' : False}
            ],
            css_class='bottom-margin',
            template_option='terms'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template' : 'stage_6', 'data' : stage}

    def get_form_data_family(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
            self.stage_6()
        ]
        return data

    def get_form_data_individual(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
            self.stage_6()
        ]
        return data


def get_state_city_by_pincode(transaction, pincode):
    new_state = pincode.state.name
    new_state_id = pincode.state.id
    city_list = preset.CITY_DICT[new_state_id]

    transaction.extra['pincode'] = pincode.pincode
    transaction.extra['city']['lat'] = pincode.lat
    transaction.extra['city']['lng'] = pincode.lng
    transaction.extra['city']['name'] = pincode.city
    transaction.save()

    new_location_dict = {}
    new_location_dict['new_state'] = new_state
    new_location_dict['new_city_list'] = city_list
    return new_location_dict, transaction


def get_dynamic_form(transaction):
    transaction, plan_data, proposer_data, is_self_present = custom_utils.standard_form_data_cleanup(transaction)

    #TOGGLES WHERE MEDICAL CASES ARE ALLOWED OR NOT
    plan_data['medical_firewall'] = True

    obj = Formic(transaction, is_self_present)
    stages = getattr(obj, 'get_form_data_%s' % transaction.slab.health_product.policy_type.lower(), '')()
    # proposer_data['self_nominee'] = transaction.extra['self_nominee']


    terms_and_conditions = "<div style='padding:20px 40px 70px 20px;'>" \
                           "<h3>Coverfox: Terms and Conditions</h3>" \
                           "<p>Coverfox Insurance Broking Pvt. (hereinafter referred to as Coverfox) Limited operates " \
                           "the website <a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> to provide " \
                           "consumers choice and an easy way to buy Insurance " \
                           "products from multiple Insurance companies. Coverfox is not an insurance company. Coverfox " \
                           "is a licensed Insurance Broking Company holding a Broking licence from the Indian " \
                           "Insurance Regulator - Insurance Regulatory and Development authority. " \
                           "<a href='/docs/irda-licence/' target='_blank'>(View IRDA License)</a></p>" \
                           "<p>Our participating insurance providers supply some of the information available on the " \
                           "Website and therefore there may be inaccuracies in the Website Information over which " \
                           "Coverfox has limited control.</p>" \
                           "<p>Coverfox does not warrant or guarantee the: Timeliness, accuracy or completeness of " \
                           "the Website Information; or Quality of the results obtained from the use of the Website.</p>" \
                           "<p>To the maximum extent permitted by law, Coverfox has no liability in relation to or " \
                           "arising out of the Website Information and Website recommendations. You are responsible " \
                           "for the final choice of your product and you should take time to read through all " \
                           "information supplied before proceeding. If you are in any doubt regarding a product or " \
                           "its terms you should seek further advice from Coverfox or the relevant participating " \
                           "provider before choosing your product.</p>" \
                           "<p>Coverfox may pass on your personal information to the relevant participating provider " \
                           "if you apply to purchase a product through " \
                           "<a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> , however, Coverfox does " \
                           "not guarantee when or if you will actually acquire the product that you have chosen. " \
                           "Coverfox does not accept any liability arising out of circumstances where there is delay " \
                           "in you acquiring the product you have chosen.</p>" \
                           "<p>Please note that Coverfox is only collecting or assisting in collecting  the premium " \
                           "deposit on behalf of the insurer you have chosen to buy the policy. The acceptance of " \
                           "the deposit as premium and final issuance of the policy is subject to the underwriting " \
                           "norms and discretion of the Insurer whose policy you have chosen to buy on which Coverfox " \
                           "has no control. Coverfox will ensure that the amount is refunded by the insurer in case " \
                           "there is no ultimate issuance of policy.</p>" \
                           "<p><button class='btn btn-lg cf-btn btn-red hide_overlay'>Close</button></p>" \
                           "</div>"
    slate_list = [
        {'id': 'slate_1', 'name': 'Proposer', 'class' : '', 'description' : 'The person who is paying for the policy'},
        {'id': 'slate_2', 'name' : 'Insured Members', 'class' : '', 'description' : 'People being insured under the policy'},
        {'id': 'slate_3', 'name' : 'Nominee', 'class' : '', 'description' : 'Nominees for the insured members'},
        {'id': 'slate_4', 'name' : 'Contact Info.', 'class' : '', 'description' : 'Contact Information of the proposer'},
        {'id': 'slate_5', 'name' : 'Medical History', 'class' : '', 'description' : 'Medical History of the people being insured'},
        {'id': 'slate_6', 'name' : 'T & C', 'class' : 'last', 'description' : 'Payment Mode and Mandatory terms and conditions', 'style' : " 'width', '0px' "}
    ]
    data = {
        'slate_list' : slate_list,
        'proposer_data' : proposer_data,
        'plan_data' : plan_data,
        'stages' : stages,
        'last_stage' : stages[-1]['template'],
        'terms_and_conditions' : terms_and_conditions,
        }
    return data
