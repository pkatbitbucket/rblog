from suds.client import Client
import os
import datetime
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from health_product.models import Transaction
from wiki.models import Pincode, FamilyCoverPremiumTable, CoverPremiumTable
from lxml import etree
from utils import request_logger
import settings
from health_product.insurer import custom_utils
from health_product.insurer.hdfc_ergo import preset




# AGENT_CODE = 'CVFX001'
# TOPUP_AGENT_CODE = 'PBZ0001'

CODES = {
    'indemnity': {'product': 'HSP', 'cover_type': 'Silver'},
    'topup': {'product': 'HSTOP', 'cover_type': 'PLUS'},
}

if settings.PRODUCTION:
    WSDL_URL = 'file://%s' % (os.path.join(settings.SETTINGS_FILE_FOLDER,'health_product/insurer/hdfc_ergo/webservice/proposal.wsdl'))
    POLICY_URL = 'https://hewspool.hdfcergo.com/healthonlinecp/service.asmx'
    PAYMENT_URL = 'https://netinsure.hdfcergo.com/onlineproducts/healthonline/tim.aspx'
    CODES['topup']['agent'] = 'CVFX001'
    CODES['indemnity']['agent'] = 'CVFX001'
else:
    WSDL_URL = 'http://202.191.196.210/UAT/OnlineProducts/HealthOnlineCP/service.asmx?WSDL'
    POLICY_URL = 'http://202.191.196.210/UAT/OnlineProducts/HealthOnlineCP/service.asmx'
    PAYMENT_URL = 'http://202.191.196.210/UAT/OnlineProducts/HealthOnline/Tim.aspx'
    CODES['topup']['agent'] = 'PBZ0001'
    CODES['indemnity']['agent'] = 'PBZ0001'


def etree_to_dict(t):
    d = {t.tag : map(etree_to_dict, t.iterchildren())}
    d.update(('@' + k, v) for k, v in t.attrib.iteritems())
    d['text'] = t.text
    return d


def post_transaction(transaction):
    redirect_url = '/health-plan/product/transaction/%s/failure/' % transaction.transaction_id
    is_redirect = True
    template_name = ''
    data = {}

    product_type = preset.PRODUCT_TYPES[transaction.slab.health_product.id]
    codes = CODES[product_type]
    agent_code = codes['agent']
    product_cd = codes['product']
    cover_type = codes['cover_type']

    pdata = transaction.insured_details_json['form_data']['proposer']['personal']
    address_data = transaction.insured_details_json['form_data']['proposer']['address']
    contact_data = transaction.insured_details_json['form_data']['proposer']['contact']
    tc_data = transaction.insured_details_json['form_data']['terms']['combo']
    transaction.proposer_date_of_birth = datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y')
    transaction.proposer_gender = int(pdata['gender']['value'])
    transaction.save()

    policy_type = transaction.slab.health_product.policy_type.lower()
    if policy_type == 'individual':
        cpt = CoverPremiumTable.objects.get(id=transaction.extra['cpt_id'])
        family_band = '1A-0C'
    else:
        cpt = FamilyCoverPremiumTable.objects.get(id=transaction.extra['cpt_id'])
        # family_band = '%sA-%sC' % (cpt.adults, cpt.kids)
        family_band = transaction.extra['plan_data']['category']
    if cpt.lower_age_limit in range(1, 12):
        lower_age = int(cpt.lower_age_limit)
    else:
        lower_age = int(cpt.lower_age_limit / 1000)
    if cpt.upper_age_limit in range(1,12):
        upper_age = int(cpt.upper_age_limit)
    else:
        upper_age = int(cpt.upper_age_limit / 1000)
    age_band = '%s-%s' % (lower_age, upper_age)

    if product_type == 'topup':
        plan_key = (age_band, family_band, str(transaction.slab.sum_assured), str(transaction.slab.deductible), str(cpt.payment_period))
        plan_code = preset.TOPUP_DICT[plan_key]
    else:
        plan_key = (age_band, family_band, str(transaction.slab.sum_assured), str(cpt.payment_period))
        plan_code = preset.PLAN_DICT[plan_key]

    transaction.extra['plan_key'] = plan_key
    transaction.save()

    pincode = Pincode.objects.filter(pincode=transaction.extra['pincode'])[0]
    state = pincode.state

    wsdl_client = Client(WSDL_URL)
    print wsdl_client

    InsuranceDetails = etree.Element("InsuranceDetails")

    CustDetails = etree.Element('CustDetails')
    ApplFirstName = etree.Element('ApplFirstName')
    ApplFirstName.text = transaction.proposer_first_name.lower()
    CustDetails.append(ApplFirstName)

    ApplLastName = etree.Element('ApplLastName')
    ApplLastName.text = transaction.proposer_last_name.lower()
    CustDetails.append(ApplLastName)

    ApplDOB = etree.Element('ApplDOB')
    ApplDOB.text = datetime.datetime.strftime(transaction.proposer_date_of_birth, '%d/%m/%Y')
    CustDetails.append(ApplDOB)

    ApplGender = etree.Element('ApplGender')
    ApplGender.text = preset.GENDER_CHOICES[int(transaction.proposer_gender)]
    CustDetails.append(ApplGender)

    Address1 = etree.Element('Address1')
    Address1.text = transaction.proposer_address1[:45]
    CustDetails.append(Address1)

    Address2 = etree.Element('Address2')
    Address2.text = transaction.proposer_address2[:45]
    CustDetails.append(Address2)

    Address3 = etree.Element('Address3')
    Address3.text = transaction.proposer_address_landmark[:45]
    CustDetails.append(Address3)

    hdfcState = etree.Element('State')
    hdfcState.text = preset.STATE_CHOICES[state.id]
    CustDetails.append(hdfcState)

    hdfcCity = etree.Element('City')
    hdfcCity.text = address_data['city']['value']
    CustDetails.append(hdfcCity)

    t_Pincode = etree.Element('Pincode')
    t_Pincode.text = str(address_data['pincode']['value'])
    CustDetails.append(t_Pincode)

    PhoneNo = etree.Element('PhoneNo')
    PhoneNo.text = ''
    CustDetails.append(PhoneNo)

    EmailId = etree.Element('EmailId')
    EmailId.text = transaction.proposer_email.lower()
    CustDetails.append(EmailId)

    MobileNo = etree.Element('MobileNo')
    MobileNo.text = transaction.proposer_mobile
    CustDetails.append(MobileNo)

    PlanDetails = etree.Element("PlanDetails")

    ProducerCd = etree.Element('ProducerCd')
    ProducerCd.text = agent_code
    PlanDetails.append(ProducerCd)

    ProductCd = etree.Element('ProductCd')
    ProductCd.text = product_cd
    PlanDetails.append(ProductCd)

    PlanCd = etree.Element('PlanCd')
    PlanCd.text = plan_code
    PlanDetails.append(PlanCd)

    coverType = etree.Element('coverType')
    coverType.text = cover_type
    PlanDetails.append(coverType)

    BasePremium = etree.Element('BasePremium')
    BasePremium.text = str(int(cpt._premium))
    #BasePremium.text = ''
    PlanDetails.append(BasePremium)

    ServiceTax = etree.Element('ServiceTax')
    ServiceTax.text = '14.50'
    PlanDetails.append(ServiceTax)

    TotalPremiumAmt = etree.Element('TotalPremiumAmt')
    TotalPremiumAmt.text = str(int(cpt.premium))
    #TotalPremiumAmt.text = ''
    PlanDetails.append(TotalPremiumAmt)

    SumInsured = etree.Element('SumInsured')
    SumInsured.text = str(transaction.slab.sum_assured)
    PlanDetails.append(SumInsured)

    if product_type == 'topup':
        Deductible = etree.Element('Deductible')
        Deductible.text = str(transaction.slab.deductible)
        PlanDetails.append(Deductible)

    TypeOfPlan = etree.Element('TypeOfPlan')
    TypeOfPlan.text = 'NF' if policy_type == 'individual' else 'WF'
    PlanDetails.append(TypeOfPlan)

    PolicyPeriod = etree.Element('PolicyPeriod')
    PolicyPeriod.text = str(cpt.payment_period)
    PlanDetails.append(PolicyPeriod)

    Servivalprd = etree.Element('Servivalprd')
    Servivalprd.text = ''
    PlanDetails.append(Servivalprd)

    PaymentDetails = etree.Element("PaymentDetails")

    PaymentMode = etree.Element('PaymentMode')
    PaymentMode.text = tc_data['payment_mode']['value']
    PaymentDetails.append(PaymentMode)

    # transaction.reference_id = '%sCF00%s' % (plan_code, transaction.id)
    transaction.reference_id = 'CFOX-%s-%s-%s' % (transaction.slab.health_product.insurer.id, transaction.id, datetime.datetime.now().strftime("%I%M%S"))
    transaction.save()
    PaymentReferance = etree.Element('PaymentReferance')
    PaymentReferance.text = transaction.reference_id
    PaymentDetails.append(PaymentReferance)

    CoInsurance = etree.Element('CoInsurance')
    CoInsurance.text = 'N'
    PaymentDetails.append(CoInsurance)

    Member = etree.Element("Member")

    #<InsuredDetails SrNo="1" FirstName="test" LastName="test" DOB="01/01/1980" RelationShip="I" InsuredId="1"
    # Gender="M" NomineeName="asdfasd" NomineeRelationship="S" PreExistingDisease="" />

    for (c, member) in enumerate(transaction.extra['members']):
        counter = c + 1
        if member['id'] != 'spouse':
            ins_code = member['id']
        else:
            if transaction.extra['gender'] == 'MALE':
                ins_code = 'wife'
            else:
                ins_code = 'husband'
        ins_data = transaction.insured_details_json['form_data']['insured'][member['id']]
        nominee_data = transaction.insured_details_json['form_data']['nominee'][member['id']]
        pre_existing_illness = transaction.insured_details_json['form_data']['medical_question_pre_existing_details']['details']['%s_question_details' % member['id']]['value']
        if pre_existing_illness.strip().lower() in ['none', 'nill']:
            pre_existing_illness = ''
        InsuredDetails = etree.Element(
            'InsuredDetails',
            SrNo=str(counter),
            FirstName=ins_data['first_name']['value'].lower(),
            LastName=ins_data['last_name']['value'].lower(),
            DOB=ins_data['date_of_birth']['value'],
            RelationShip=preset.RELATIONSHIP_CHOICES[ins_code],
            InsuredId='',
            Gender=preset.GENDER_CHOICES[int(member['gender'] if member['id']!='self' else transaction.proposer_gender)],
            NomineeName=custom_utils.get_full_name(first_name=nominee_data['first_name']['value'].lower(), last_name=nominee_data['last_name']['value'].lower()),
            NomineeRelationship=nominee_data['relation']['value'],
            PreExistingDisease=pre_existing_illness,
        )
        if member['age'] >= 21:
            InsuredDetails.attrib['DocumentNumber'] = ins_data['pancard_no']['value']
            InsuredDetails.attrib['DocumentType'] = ""
        else:
            InsuredDetails.attrib['DocumentNumber'] = ""
            InsuredDetails.attrib['DocumentType'] = ins_data['required_docs']['value']
        InsuredDetails.text = ''
        Member.append(InsuredDetails)

    InsuranceDetails.append(CustDetails)
    InsuranceDetails.append(PlanDetails)
    InsuranceDetails.append(PaymentDetails)
    InsuranceDetails.append(Member)

    print(etree.tostring(InsuranceDetails, pretty_print=True))
    data = etree.tostring(InsuranceDetails)

    #try:
    if True:
        resp = wsdl_client.service.CalculatePremium(data)
        request_logger.info('\nHDFC TRANSACTION: %s \n XML REQUEST SENT:\n %s \n' % (transaction.transaction_id, wsdl_client.last_sent()))
        request_logger.info('\nHDFC TRANSACTION: %s \n XML RESPONSE RECEIVED:\n %s \n' % (transaction.transaction_id, wsdl_client.last_received()))
        xml_string = str(resp)

        print ">>>>>>>>>", xml_string
        print plan_key

        root = etree.fromstring(xml_string)
        response_dict = etree_to_dict(root)
        transaction.extra['product_response'] = response_dict
        transaction.payment_request = wsdl_client.last_sent()
        transaction.save()

        res = {'status': response_dict['WsResult'][0]['WsResultSet'][0]['text'],
               'message': response_dict['WsResult'][0]['WsResultSet'][1]['text']}

        if res['status'] == '0':
            transaction.policy_token = res['message']
            transaction.premium_paid = str(int(cpt.premium))
            transaction.save()

            is_redirect = False
            redirect_url = PAYMENT_URL
            data = {
                'CustomerID': transaction.policy_token,
                'TxnAmount': str(int(cpt.premium)),
                'AdditionalInfo1': 'NB',
                'AdditionalInfo2': product_cd,
                'AdditionalInfo3': '1',
                'hdnPayMode': tc_data['payment_mode']['value'],
                'UserName': custom_utils.get_full_name(first_name=transaction.proposer_first_name.lower(),
                                               last_name=transaction.proposer_last_name.lower()),
                'UserMailId': transaction.proposer_email,
                'ProductCd': product_cd,
                'ProducerCd': agent_code,
            }

        return is_redirect, redirect_url, template_name, data
    #except urllib2.HTTPError as err:
    else:
        return is_redirect, redirect_url, '', {}



def post_payment(payment_response, transaction=None):
    redirect_url = '/health-plan/payment/failure/'
    is_redirect = True
    print payment_response

    #{u'Msg': u'Payment transcation was not completed', u'PolicyNo': u'0', u'ProposalNo': u'HI1403000046T'}
    if payment_response['ProposalNo']:
        transaction = Transaction.objects.get(policy_token=payment_response['ProposalNo'])
        redirect_url = '/health-plan/payment/transaction/%s/failure/' % transaction.transaction_id
        transaction.extra['payment_response'] = payment_response
        transaction.extra['error_msg'] = payment_response['Msg'];
        transaction.payment_response = payment_response
        transaction.save()

    if transaction.extra['payment_response']['PolicyNo'] and transaction.extra['payment_response']['PolicyNo'] != '0':
        transaction.payment_token = transaction.extra['payment_response']['ProposalNo']
        transaction.policy_number = transaction.extra['payment_response']['PolicyNo']
        transaction.payment_done = True
        transaction.payment_on = timezone.now()
        transaction.policy_start_date = timezone.now()
        transaction.policy_end_date = timezone.now() + relativedelta(days=-1, years=1)
        transaction.save()
        redirect_url = '/health-plan/payment/transaction/%s/success/' % transaction.transaction_id
    return is_redirect, redirect_url, '', {}
