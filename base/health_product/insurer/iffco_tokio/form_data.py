from wiki.models import City
from health_product.insurer.iffco_tokio import preset
from health_product.insurer import custom_utils
from collections import OrderedDict

class Formic():
    def __init__(self, transaction, is_self_present):
        self.transaction = transaction
        self.is_self_present = is_self_present
        self.proposer_data = {
            'pincode': transaction.extra['pincode'],
            'city' : City.objects.get(id=transaction.extra['city']['id']).name,
            'state' : City.objects.get(id=transaction.extra['city']['id']).state.name,
            'email': ''
        }

    def add_parent_segment(self, segment_id, tag_id, details, question_type, select_options=None):
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': details, 'data': []}
        row = []
        if question_type == 'MultipleChoiceField':
            default_options = [
                {'id': 'Yes', 'name': 'Yes', 'is_correct': False},
                {'id': 'No', 'name': 'No', 'is_correct': True}
            ]
            trigger_val = details.get('trigger_answer', 'Yes')
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='MultipleChoiceField',
                    code='%s_question_%s' % (member['id'], details['question_code']),
                    details=member,
                    value='No' if trigger_val == 'Yes' else 'Yes',
                    label=details.get('question_text', ''),
                    span=4,
                    options=select_options if select_options else default_options,
                    css_class='bottom-margin',
                    event=', click : $parent.root_context.stage_5.any_yes_check',
                    template_option='question'
                ))
            new_segment['data'].append(row)
        elif question_type == 'DropDownField':
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='DropDownField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    options=select_options,
                    details=member,
                    label=details.get('question_text', ''),
                    span=2,
                    css_class='bottom-margin',
                    event=', event : {change : root_context.stage_5.any_yes_check}',
                    template_option='question'
                ))
            new_segment['data'].append(row)
        elif question_type == 'TextField':
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='TextField',
                    label=details.get('question_text', ''),
                    code='%s_question_%s' % (member['id'], tag_id),
                    span=2,
                    required=False,
                    ))
            new_segment['data'].append(row)

        return new_segment

    def add_conditional_segment(self, segment_id, tag_id, parent_question_code, details, question_type):
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : details, 'data' : []}
        row = []
        if question_type == 'MultipleChoiceField':
            new_segment['details']['tier'] = 1
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='MultipleChoiceField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    details=member,
                    value='No',
                    label=details.get('question_text', ''),
                    span=4,
                    required=False,
                    css_class='bottom-margin',
                    event=', click : $parent.root_context.stage_5.any_yes_check',
                    only_if='root_context.stage_5.check_dict.get(\'%s\')' % parent_question_code,
                    template_option='question'
                ))
            new_segment['data'].append(row)
        elif question_type == 'TwoDropDownField':
            new_segment['details']['tier'] = 2
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='TwoDropDownField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    options1=self.month_range,
                    options2=self.year_range,
                    label=details.get('question_text', ''),
                    value='No',
                    span=2,
                    dependant_parent='%s_question_%s' % (member['id'], parent_question_code),
                    required=False,
                    only_if='root_context.stage_5.check_dict.get(\'%s_question_%s\')' % (member['id'], parent_question_code),
                    template_option='question'
                ))
            new_segment['data'].append(row)
        elif question_type == 'TextField':
            new_segment['details']['tier'] = 2
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='TextField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    span=2,
                    label=details.get('question_text', ''),
                    dependant_parent='%s_question_%s' % (member['id'], parent_question_code),
                    required=False,
                    only_if='root_context.stage_5.check_dict.get(\'%s_question_%s\')' % (member['id'], parent_question_code),
                    ))
            new_segment['data'].append(row)
        return new_segment

    def stage_1(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'personal'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='NameField',
            label='First Name',
            code='first_name',
            max_length=23
            ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='NameField',
            label='Last Name',
            code='last_name',
            max_length=23
            ))
        if self.is_self_present:
            member = [m for m in self.transaction.extra['members'] if m['id'] == 'self'][0]
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                span=3,
                min_age=18,
                max_age=100,
                default_age=member['age'],
            ))
        else:
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                min_age=18,
                max_age=100,
                default_age=18,
            ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Gender',
            code='gender',
            value=1 if self.transaction.extra['gender'] == 'MALE' else 2,
            options=preset.GENDER_OPTIONS
        ))
        married = True if [m for m in self.transaction.extra['members'] if m['id'] == 'spouse'] else False;
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Marital Status',
            code='marital_status',
            value='M' if married or len(self.transaction.extra['members']) > 1 else 'S',
            options=preset.MARITAL_STATUS_OPTIONS,
            template_option= 'static' if married else 'dynamic',
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Occupation',
            code='occupation',
            options=preset.OCCUPATION_OPTIONS,
            ))
        new_segment['data'].append(row)

        stage.append(new_segment)
        return {'template' : 'stage_1', 'data' : stage}

    def stage_2(self):
        stage = []

        for member in self.transaction.extra['members']:
            segment_id = 'insured'
            tag_id = member['id']
            template_switch = 'dynamic'
            if tag_id == 'self':
                template_switch = 'static'
            try:
                saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : member, 'data' : []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='NameField',
                label='First Name',
                code='first_name',
                max_length=23,
                template_option=template_switch,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='NameField',
                label='Last Name',
                code='last_name',
                max_length=23,
                template_option=template_switch,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                template_option=template_switch,
                span=3,
                default_age = member['age'],
                min_age = 18 if member['type']=='adult' else 0,
                max_age = 100,
            ))
            new_segment['data'].append(row)

            stage.append(new_segment)
        return {'template' : 'stage_2', 'data' : stage}


    def stage_3(self):
        stage = []

        default_relation = {
            'self': 'Spouse',
            'spouse': 'Spouse',
            'son': 'Father' if self.transaction.extra['gender'] == 'MALE' else 'Mother' ,
            'daughter': 'Father' if self.transaction.extra['gender'] == 'MALE' else 'Mother' ,
            'father': 'Child' if self.transaction.extra['gender'] == 'MALE' else 'Daughter',
            'mother': 'Child' if self.transaction.extra['gender'] == 'MALE' else 'Daughter'
        }
        self_relation_probables = OrderedDict([
            ('mother', 'Mother'),
            ('father', 'Father'),
            ('spouse', 'Spouse')
        ])
        #Needs to run before the big loop on members
        for member in self.transaction.extra['members']:
            if member['person'] in self_relation_probables.keys():
                self.transaction.extra['self_nominee'] = member['id']
                self.transaction.save()
                default_relation['self'] = self_relation_probables[member['person']]

        for member in self.transaction.extra['members']:
            segment_id = 'nominee'
            tag_id = member['id']
            try:
                saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : member, 'data' : []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DropDownField',
                code='relation',
                label='Relationship With %s' % member['verbose'],
                options=preset.NOMINEE_RELATION_OPTIONS,
                value=default_relation[member['person']],
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='NameField',
                code='first_name',
                label='First Name',
                max_length=23,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='NameField',
                code='last_name',
                label='Last Name',
                max_length=23,
                span=3
            ))
            new_segment['data'].append(row)
            stage.append(new_segment)
        return {'template' : 'stage_3', 'data' : stage}

    def stage_4(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'address'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address1',
            label='Flat/House No, Street',
            span=12,
            css_class='bottom-margin',
            max_length=30
        ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address2',
            label='Area/Locality',
            span=12,
            css_class='bottom-margin',
            max_length=30,
            ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='landmark',
            label='Landmark',
            span=12,
            css_class='bottom-margin',
            max_length=30,
            required=False
        ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='TwoSearchableDDDField',
            label='Location',
            code='state',
            span=10,
            options1=preset.STATE_CHOICES,
            options2=preset.CITY_DICT,
            required=True,
            main_label='State',
            dep_label='City',
        ))


        # pincode = Pincode.objects.filter(pincode=self.transaction.extra['pincode'])[0]
        # state = pincode.state
        # row.append(custom_utils.new_field(
        #     saved_data=saved_data,
        #     field_type='DropDownField',
        #     code='city',
        #     label='City',
        #     options=preset.CITY_DICT[state.id],
        #     ))
        # row.append(custom_utils.new_field(
        #     saved_data=saved_data,
        #     field_type='CharField',
        #     code='state',
        #     label='State : ',
        #     value=self.proposer_data['state'],
        #     span=4,
        #     template_option='static'
        # ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='pincode',
            label='Pincode',
            value=str(self.proposer_data['pincode']),
            span=2,
            template_option='static'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)


        tag_id = 'contact'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='email',
            label='Email',
            span=12,
            css_class='bottom-margin',
            max_length=50,
            email=True
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MobileField',
            code='mobile',
            label='Mobile',
            css_class='bottom-margin',
            span=12
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='LandlineField',
            code='landline',
            label='Landline',
            span=12,
            css_class='bottom-margin',
            required=False
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template' : 'stage_4', 'data' : stage}

    def stage_6(self):
        stage = []
        segment_id = 'terms'
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}
        row = []
        t_and_c_label = ('I hereby confirm<br />'
                         '- I have read and agree to the Terms and Conditions of ')
        if self.transaction.slab.health_product.policy_wordings:
            t_and_c_label += ('the <a style="text-decoration:none;cursor:pointer;" '
                              'onclick=\'window.open("%s","","width=400, height=400")\'>policy</a> and '
                             ) % self.transaction.slab.health_product.policy_wordings.url
        t_and_c_label += ('the <a style="cursor:pointer;" class="call_overlay">website</a>;<br />'
                          '- the declarations made herewith are true and correct;<br />'
                          '- appointment of Coverfox Insurance Brokers Pvt. Ltd. as my Insurance Broker, '
                          'and authorize them to represent me to Insurance Companies/Agencies for my Insurance needs.'
                         )
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='t_and_c',
            label=t_and_c_label,
            span=12,
            options=[
                {'id' : 'Yes', 'name' : 'Yes', 'is_correct' : True},
                {'id' : 'No', 'name' : 'No', 'is_correct' : False}
            ],
            css_class='bottom-margin',
            template_option='terms'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template' : 'stage_6', 'data' : stage}

    def get_form_data_individual(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
        ]
        stage = []
        #---------- Question Unit Begins -----------
        question_code = 'pre_existing'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Do you have any Pre-existing diseases?',
            'help_text': '',
            'question_code': question_code,
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        data.append({'template' : 'stage_5', 'data' : stage})
        data.append(self.stage_6())
        return data

    def get_form_data_family(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            ]
        stage = []
        #---------- Question Unit Begins -----------
        question_code = 'pre_existing'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Do you have any Pre-existing diseases?',
            'help_text': '',
            'trigger_any_yes_check': False,
            'question_code': question_code,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        #---------- Question Unit Begins -----------
        question_code = 'good_health'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Are you in good health and free from physical and mental diseases or infirmity or medical complaints?',
            'help_text': '',
            'question_code': question_code,
            'trigger_any_yes_check': True,
            'trigger_answer': 'No',
            'show_header': True
        }
        select_options = [
            {'id': 'Yes', 'name': 'Yes', 'is_correct': True},
            {'id': 'No', 'name': 'No', 'is_correct': False}
        ]
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField', select_options))


        #CONDITIONAL SEGMENT
        tag_id = 'conditional_good_health'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'If No, please give full details',
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))

        #---------- Question Unit Begins -----------
        question_code = 'complaints'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Do you have any complaints or tendency that necessitate such consultation or treatment in future?',
            'help_text': '',
            'trigger_any_yes_check': True,
            'question_code': question_code,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        #CONDITIONAL SEGMENT
        tag_id = 'conditional_complaints'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'If Yes, please give full details',
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))

        #---------- Question Unit Begins -----------
        question_code = 'additional_facts'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Are there any additional facts affecting the proposed insurance, which should be disclosed to insurers?',
            'help_text': '',
            'trigger_any_yes_check': True,
            'question_code': question_code,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        #CONDITIONAL SEGMENT
        tag_id = 'conditional_additional_facts'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'If Yes, please give full details',
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))

        #---------- Question Unit Begins -----------
        question_code = 'diseases'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Do you have sustained any disease/ illness/ accident / operation in past 4 Years?',
            'help_text': '',
            'trigger_any_yes_check': False,
            'question_code': question_code,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        #---------- Question Unit Begins -----------
        question_code = 'existing_insurance'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Are you at present or any other time in past covered under any other insurance?',
            'help_text': '',
            'trigger_any_yes_check': False,
            'question_code': question_code,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        data.append({'template' : 'stage_5', 'data' : stage})
        data.append(self.stage_6())
        return data


def get_dynamic_form(transaction):
    transaction, plan_data, proposer_data, is_self_present = custom_utils.standard_form_data_cleanup(transaction)
    #TOGGLES WHERE MEDICAL CASES ARE ALLOWED OR NOT
    plan_data['medical_firewall'] = True

    policy_type = transaction.slab.health_product.policy_type.lower()
    obj = Formic(transaction, is_self_present)
    stages = getattr(obj, 'get_form_data_%s' % policy_type, '')()

    terms_and_conditions = "<div style='padding:20px 40px 70px 20px;'>" \
                           "<h3>IFFCO-TOKIO: Terms &amp; Conditions</h3>" \
                           "<p>The above information is complete in all respects and is true to best of my " \
                           "knowledge. I/We and/or the person to be insured hereby consent you or your " \
                           "representative to seek information from any Hospital/Medical Practitioner from which " \
                           "or whom I/We and/or the person to be insured have at any time sought or shall seek " \
                           "medical attention concerning any disease, sickness, ailment or injury which affects " \
                           "my/our and/or the person to be insured's physical or mental health. I/We hereby " \
                           "authorize IFFCO Tokio to pay any claim payable to me under the Health Insurance " \
                           "policy (SKP / IMI) to the above nominee whose discharge will be considered as " \
                           "the full and final discharge on my behalf.</p>" \
                           "<p>INSURANCE ACT 1938 SECTION 41- Prohibition of Rebates. No person shall allow or " \
                           "offer to allow either directly or indirectly, as an inducement to any person to take " \
                           "out or renew or continue an insurance in respect of any kind of risk relating to lives " \
                           "or property in India, any rebate of the whole or part of the commission payable or " \
                           "any rebate of the premium shown on the policy, nor shall any person taking out or " \
                           "renewing a policy accept any rebate, except such rebate as may be allowed in accordance " \
                           "with the published prospectus or tables of the insurer. ANY PERSON MAKING FAULT IN " \
                           "COMPLYING WITH THE PROVISIONS OF THIS SECTION SHALL BE PUNISHABLE WITH FINE WHICH MAY " \
                           "EXTEND TO FIVE HUNDRED RUPEES.</p>" \
                           "<p> 	I agree to this proposal and the declaration shall be the basis of the contract " \
                           "between me and ITGI and agree to accept the policy subject to the terms & conditions " \
                           "prescribed by IFFCO Tokio General Insurance Company Ltd which have been carefully read " \
                           "and clearly understood by me.</p>" \
                           "<p> 	Policy is valid subject to correctness of information filled in online by you. " \
                           "In case of any discrepancy the policy would become void and company would not be liable.</p>" \
                           "<h3>Coverfox: Terms and Conditions</h3>" \
                           "<p>Coverfox Insurance Broking Pvt. (hereinafter referred to as Coverfox) Limited operates " \
                           "the website <a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> to provide " \
                           "consumers choice and an easy way to buy Insurance " \
                           "products from multiple Insurance companies. Coverfox is not an insurance company. Coverfox " \
                           "is a licensed Insurance Broking Company holding a Broking licence from the Indian " \
                           "Insurance Regulator - Insurance Regulatory and Development authority. " \
                           "<a href='/docs/irda-licence/' target='_blank'>(View IRDA License)</a></p>" \
                           "<p>Our participating insurance providers supply some of the information available on the " \
                           "Website and therefore there may be inaccuracies in the Website Information over which " \
                           "Coverfox has limited control.</p>" \
                           "<p>Coverfox does not warrant or guarantee the: Timeliness, accuracy or completeness of " \
                           "the Website Information; or Quality of the results obtained from the use of the Website.</p>" \
                           "<p>To the maximum extent permitted by law, Coverfox has no liability in relation to or " \
                           "arising out of the Website Information and Website recommendations. You are responsible " \
                           "for the final choice of your product and you should take time to read through all " \
                           "information supplied before proceeding. If you are in any doubt regarding a product or " \
                           "its terms you should seek further advice from Coverfox or the relevant participating " \
                           "provider before choosing your product.</p>" \
                           "<p>Coverfox may pass on your personal information to the relevant participating provider " \
                           "if you apply to purchase a product through " \
                           "<a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> , however, Coverfox does " \
                           "not guarantee when or if you will actually acquire the product that you have chosen. " \
                           "Coverfox does not accept any liability arising out of circumstances where there is delay " \
                           "in you acquiring the product you have chosen.</p>" \
                           "<p>Please note that Coverfox is only collecting or assisting in collecting  the premium " \
                           "deposit on behalf of the insurer you have chosen to buy the policy. The acceptance of " \
                           "the deposit as premium and final issuance of the policy is subject to the underwriting " \
                           "norms and discretion of the Insurer whose policy you have chosen to buy on which Coverfox " \
                           "has no control. Coverfox will ensure that the amount is refunded by the insurer in case " \
                           "there is no ultimate issuance of policy.</p>" \
                           "<p><button class='btn btn-lg cf-btn btn-red hide_overlay'>Close</button></p>" \
                           "</div>"

    slate_list = [
        {'id': 'slate_1', 'name': 'Proposer', 'class' : '', 'description' : 'The person who is paying for the policy'},
        {'id': 'slate_2', 'name' : 'Insured Members', 'class' : '', 'description' : 'People being insured under the policy'},
        {'id': 'slate_3', 'name' : 'Nominee', 'class' : '', 'description' : 'Nominees for the insured members'},
        {'id': 'slate_4', 'name' : 'Contact Info.', 'class' : '', 'description' : 'Contact Information of the proposer'},
        {'id': 'slate_5', 'name' : 'Medical History', 'class' : '', 'description' : 'Medical History of the people being insured'},
        {'id': 'slate_6', 'name' : 'T & C', 'class' : 'last', 'description' : 'Mandatory terms and conditions', 'style' : " 'width', '0px' "}
    ]
    policy_date_disclaimer = 'NOTE: The policy period will start immediately from the date this transaction gets processed'
    data = {
        'slate_list' : slate_list,
        'proposer_data' : proposer_data,
        'plan_data' : plan_data,
        'stages' : stages,
        'last_stage' : stages[-1]['template'],
        'terms_and_conditions' : terms_and_conditions,
        'policy_date_disclaimer' : policy_date_disclaimer
    }

    return data

