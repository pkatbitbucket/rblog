import putils

def download_buy_form(transaction):
    order = []
    rows = []

    form_data = transaction.insured_details_json['form_data']
    proposer_data = form_data['proposer']['personal']
    proposer_address = form_data['proposer']['address']
    proposer_contact = form_data['proposer']['contact']
    insured_data = form_data['insured']
    nominee_data = form_data['nominee']
    medical_questions = ['pre_existing', 'good_health', 'complaints', 'additional_facts', 'diseases', 'existing_insurance']

    memberList = []
    for member in insured_data:
        memberList.append(member)


    rows.append(['Proposer Details'])
    rows.append(['Name', ' '.join([proposer_data['first_name']['value'], proposer_data['last_name']['value']])])
    rows.append(['Gender', proposer_data['gender']['display_value']])
    rows.append(['Marital Status', proposer_data['marital_status']['display_value']])
    rows.append(['Date of Birth', proposer_data['date_of_birth']['display_value']])
    rows.append(['Occupation', proposer_data['occupation']['display_value']])
    rows.append([])


    rows.append(['Insured Members'])
    for member in memberList:
        rows.append(['Which Insured Member', member])
        rows.append(['Name', ' '.join([insured_data[member]['first_name']['display_value'], insured_data[member]['last_name']['display_value']])])
        rows.append(['Date of Birth', insured_data[member]['date_of_birth']['display_value']])
        rows.append([])


    rows.append(['Nominee Details'])
    for member in memberList:
        rows.append(['Nominee for which insured?', member])
        rows.append(['Name', ' '.join([nominee_data[member]['first_name']['value'], nominee_data[member]['last_name']['value']])])
        rows.append([nominee_data[member]['relation']['label'], nominee_data[member]['relation']['display_value']])
        rows.append([])



    rows.append(['Proposer Contact Details'])
    rows.append(['Address', ', '.join([proposer_address['address1']['value'], proposer_address['address2']['value'], proposer_address['landmark']['value'], proposer_address['state']['display_value2'], proposer_address['pincode']['value']])])
    rows.append(['Email', proposer_contact['email']['value']])
    rows.append(['LandLine', proposer_contact['landline']['std_code']+'-'+proposer_contact['landline']['landline']])
    rows.append(['Mobile', proposer_contact['mobile']['value']])
    rows.append([])


    rows.append(['Medical Details'])
    rows.append(['Members', '']+memberList)
    rows.append([])
    for question in medical_questions:
        row = []
        row.append(form_data['medical_question_'+question]['combo'].values()[0]['label'])
        row.append('')
        for member in memberList:
            row.append(form_data['medical_question_'+question]['combo'][member+'_question_'+question]['display_value'])
        rows.append(row)
        if question in ['good_health', 'complaints', 'additional_facts']:
            row = []
            row.append(form_data['medical_question_'+question]['conditional_'+question].values()[0]['label'])
            row.append('')
            for member in memberList:
                value = form_data['medical_question_'+question]['conditional_'+question][member+'_question_conditional_'+question]['display_value']
                row.append(value if value else 'None')
            rows.append(row)
        rows.append([])



    file_name = 'Form_Data_%s.xls' % transaction.transaction_id
    return putils.render_excel(file_name, order, rows)
