import logging
import os
import lxml.etree as et

from django.conf import settings
from suds.client import Client
from health_product.models import Transaction
from wiki.models import City, FamilyCoverPremiumTable
from dateutil.relativedelta import datetime, relativedelta
from settings import SETTINGS_FILE_FOLDER
from utils import request_logger
from health_product.insurer.iffco_tokio import preset
from health_product.insurer import custom_utils


logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.transport.http').setLevel(logging.DEBUG)

BROKER_CODE = '38000414'

WSDL_FILE_IMI = 'file://%s' % (os.path.join(SETTINGS_FILE_FOLDER,'health_product/insurer/iffco_tokio/webservice/IMI.wsdl'))
WSDL_FILE_SKP = 'file://%s' % (os.path.join(SETTINGS_FILE_FOLDER,'health_product/insurer/iffco_tokio/webservice/SKP.wsdl'))

#======================  UAT ==================================================================
# PARTNER_CODE = 'ITGIHLT022'
# BRANCH = 'COVER_FOX'
# SUB_BRANCH = 'COVER_FOX'
#
# #FOR IMI
# PREMIUM_FETCH_IMI = 'http://220.227.8.74/portaltest/services/IMIPremiumWebService'
# PROPOSAL_URL_IMI = 'http://220.227.8.74/portaltest/imireq'
#
# #FOR SKP
# PREMIUM_FETCH_SKP = 'http://220.227.8.74/portaltest/services/SKPPremiumWebService'
# PROPOSAL_URL_SKP = 'http://220.227.8.74/portaltest/skpreq'
#==============================================================================================

#======================  PRODUCTION ===========================================================
PARTNER_CODE = 'ITGIHLT013'
BRANCH = 'COVERFOX'
SUB_BRANCH = 'COVERFOX'

#IMI
PREMIUM_FETCH_IMI = 'https://www.itgionline.com/ptnrportal/services/IMIPremiumWebService'
PROPOSAL_URL_IMI = 'https://www.itgionline.com/ptnrportal/imireq'

#SKP
#PREMIUM_FETCH_MULTI_SKP = 'https://www.itgionline.com/ptnrportal/services/SKPMultiPremiumWebService'
PREMIUM_FETCH_SKP = 'https://www.itgionline.com/ptnrportal/services/SKPPremiumWebService'
PROPOSAL_URL_SKP = 'https://www.itgionline.com/ptnrportal/skpreq'
#==============================================================================================


def data2xml(d, name='data', list_name='InsuredDetails'):
    r = et.Element(name)
    return et.tostring(buildxml(r, d, list_name))


def buildxml(r, d, list_name):
    if isinstance(d, dict):
        for k, v in d.iteritems():
            s = et.SubElement(r, k)
            buildxml(s, v, list_name)
    elif isinstance(d, tuple) or isinstance(d, list):
        for v in d:
            s = et.SubElement(r, list_name)
            buildxml(s, v, list_name)
    elif isinstance(d, basestring):
        r.text = d
    else:
        r.text = str(d)
    return r


def get_transaction_from_reference_id(reference_id):
    t_id = int(reference_id.split('-')[1])
    return Transaction.objects.get(id=t_id)


class Processor():
    def __init__(self, transaction_id):
        self.transaction_id = transaction_id


    def fetch_premium_individual(self):
        transaction = Transaction.objects.get(id=self.transaction_id)

        pdata = transaction.insured_details_json['form_data']['proposer']['personal']
        address_data = transaction.insured_details_json['form_data']['proposer']['address']
        contact_data = transaction.insured_details_json['form_data']['proposer']['contact']

        if 'date_of_birth' in pdata.keys():
            transaction.proposer_date_of_birth = datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y')
        if 'gender' in pdata.keys():
            transaction.proposer_gender = int(pdata['gender']['value'])
        transaction.save()

        city = City.objects.get(id=transaction.extra['city']['id'])
        state = city.state

        client = Client(WSDL_FILE_IMI)

        imi_policy = client.factory.create('IMIPolicy')
        transaction.policy_start_date = datetime.datetime.today() + relativedelta(days=1)
        transaction.policy_end_date = transaction.policy_start_date + relativedelta(days=-1, years=1)
        transaction.save()
        imi_policy.inceptionDate = datetime.datetime.strftime(transaction.policy_start_date, '%m/%d/%Y %H:%M:%S')
        imi_policy.expiryDate = datetime.datetime.strftime(transaction.policy_end_date, '%m/%d/%Y %H:%M:%S')
        imi_policy.noOfInsured = 1
        imi_policy.partnerCode = PARTNER_CODE
        imi_policy.stateCode = transaction.proposer_state
        imi_policy.imiInsured = client.factory.create('ArrayOfErrorDetails')
        imi_policy.imiInsured.item = []
        insured_member = client.factory.create('IMIInsured')
        insured_member.age = relativedelta(datetime.datetime.today(), transaction.proposer_date_of_birth).years
        insured_member.basicSumInsured = str(transaction.slab.sum_assured)
        insured_member.coverRageType = 'NR'     #HR for critical illness else NR
        insured_member.dateOfBirth = datetime.datetime.strftime(transaction.proposer_date_of_birth, '%m/%d/%Y')
        insured_member.preExistingDisease = 'N'

        imi_policy.imiInsured.item.append(insured_member)

        svc_method = client.service.getIMIPremium
        svc_method.method.location = PREMIUM_FETCH_IMI

        res = svc_method(imi_policy)
        request_logger.info('\nIFFCO TOKIO TRANSACTION: %s \n PREMIUM REQUEST SENT:\n %s \n' % (transaction.transaction_id, client.last_sent()))
        request_logger.info('\nIFFCO TOKIO TRANSACTION: %s \n PREMIUM RESPONSE RECEIVED:\n %s \n' % (transaction.transaction_id, client.last_received()))


        response_dict = {}
        for k, v in res:
            response_dict[k] = v

        transaction.extra['premium_response'] = response_dict
        transaction.save()
        if transaction.extra['premium_response']['error']:
            return False
        else:
            return True

    def fetch_premium_family(self):
        transaction = Transaction.objects.get(id=self.transaction_id)
        pdata = transaction.insured_details_json['form_data']['proposer']['personal']
        address_data = transaction.insured_details_json['form_data']['proposer']['address']
        contact_data = transaction.insured_details_json['form_data']['proposer']['contact']

        if 'date_of_birth' in pdata.keys():
            transaction.proposer_date_of_birth = datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y')
        if 'gender' in pdata.keys():
            transaction.proposer_gender = int(pdata['gender']['value'])
        transaction.save()

        city = City.objects.get(id=transaction.extra['city']['id'])
        state = city.state

        client = Client(WSDL_FILE_SKP)

        cpt = FamilyCoverPremiumTable.objects.get(id=transaction.extra['cpt_id'])
        max_adults = cpt.adults
        max_child = cpt.kids
        skp_policy = client.factory.create('SKPPolicy')
        transaction.policy_start_date = datetime.datetime.today() + relativedelta(days=1)
        transaction.policy_end_date = datetime.datetime.today() + relativedelta(days=0, years=1)
        transaction.save()
        skp_policy.inceptionDate = datetime.datetime.strftime(transaction.policy_start_date, '%m/%d/%Y %H:%M:%S')
        skp_policy.expiryDate = datetime.datetime.strftime(transaction.policy_end_date, '%m/%d/%Y %H:%M:%S')
        skp_policy.maxAdults = max_adults
        skp_policy.maxChild = max_child
        skp_policy.planCode = preset.SLAB_PLAN_DICT[transaction.slab.id]
        skp_policy.partnerCode = PARTNER_CODE
        skp_policy.skpInsured = client.factory.create('ArrayOfErrorDetails')
        skp_policy.skpInsured.item = []


        for code, fdata in transaction.insured_details_json['form_data']['insured'].items():
            insured_member = client.factory.create('SKPInsured')
            member_age = relativedelta(datetime.datetime.today(), datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y').date()).years
            # if member_age == 0:
            #     member_age = (relativedelta(datetime.datetime.today(), datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y').date()).months / 10.0)
            insured_member.age = member_age
            insured_member.dateOfBirth = datetime.datetime.strftime(datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y'), '%m/%d/%Y')
            insured_member.preExistingDisease = 'N'
            skp_policy.skpInsured.item.append(insured_member)

        svc_method = client.service.getSKPPremium
        svc_method.method.location = PREMIUM_FETCH_SKP

        res = svc_method(skp_policy)

        request_logger.info('\nIFFCO TOKIO TRANSACTION: %s \n XML REQUEST SENT:\n %s \n' % (transaction.transaction_id, client.last_sent()))
        request_logger.info('\nIFFCO TOKIO TRANSACTION: %s \n XML RESPONSE RECEIVED:\n %s \n' % (transaction.transaction_id, client.last_received()))

        response_dict = {}
        for k, v in res:
            response_dict[k] = v

        transaction.extra['premium_response'] = response_dict
        transaction.save()
        if transaction.extra['premium_response']['error']:
            return False
        else:
            return True

    def get_processed_data_individual(self):
        transaction = Transaction.objects.get(id=self.transaction_id)
        transaction.reference_id = 'CFOX-%s-%s' % (transaction.id, datetime.datetime.now().strftime("%I%M%S"))
        transaction.save()
        city = City.objects.get(id=transaction.extra['city']['id'])
        state = city.state

        pdata = transaction.insured_details_json['form_data']['proposer']['personal']
        address_data = transaction.insured_details_json['form_data']['proposer']['address']
        contact_data = transaction.insured_details_json['form_data']['proposer']['contact']


        data = {'Policy': {
            'Product': 'IMI',
            'InceptionDate': datetime.datetime.strftime(transaction.policy_start_date, '%m/%d/%Y %H:%M:%S'),
            'ExpiryDate': datetime.datetime.strftime(transaction.policy_end_date, '%m/%d/%Y %H:%M:%S'),
            'UniqueQuoteId': transaction.reference_id,
            'FamilyDiscountPercent': transaction.extra['premium_response']['familyDiscountPercent'],
            'FamilyDiscount': transaction.extra['premium_response']['familyDiscount'],
            'GrossPremium': transaction.extra['premium_response']['grossPremiumBeforeDiscount'],
            'GrossPremiumAfterDiscLoad': transaction.extra['premium_response']['grossPremiumAfterDiscLoad'],
            'ServiceTax': transaction.extra['premium_response']['serviceTax'],
            'NetPremiumPayable': transaction.extra['premium_response']['netPremiumPayable'],
            'ExternalBranch': BRANCH,
            'ExternalSubBranch': SUB_BRANCH,
            'ExternalServiceConsumer': PARTNER_CODE
        }, 'Insued': []}

        kid_counter = 1
        counter = 0
        for code, fdata in transaction.insured_details_json['form_data']['insured'].items():
            counter += 1
            ndata = transaction.insured_details_json['form_data']['nominee'][code]
            member = [m for m in transaction.extra['members'] if m['id'] == code][0]
            insured_relation = None
            if code == 'self':
                insured_relation = 'SELF'
            elif code == 'spouse':
                insured_relation = 'SPOU'
            elif code == 'mother':
                insured_relation = 'MOTR'
            elif code == 'father':
                insured_relation = 'FATR'
            else:
                insured_relation = 'CHD%s' % kid_counter
                kid_counter += 1
            pre_existing = transaction.insured_details_json['form_data']['medical_question_pre_existing']['combo']['%s_question_pre_existing' % code]
            member_age = relativedelta(datetime.datetime.today(), datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y').date()).years
            # if member_age == 0:
            #     member_age = (relativedelta(datetime.datetime.today(), datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y').date()).months / 10.0)
            data['Insued'].append({
                'MemberId': '%s%s' % (transaction.id, counter),
                'FirstName': fdata['first_name']['value'],
                'LastName': fdata['last_name']['value'],
                'PreExistingDisease': 'Y' if pre_existing == 'Yes' else 'N',
                'Age': member_age,
                'DateOfBirth': datetime.datetime.strftime(datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y'), '%m/%d/%Y'),
                'Gender': preset.GENDER_CHOICES[member['gender']],
                'RelationtoInsured': insured_relation,
                'Nominee': custom_utils.get_full_name(first_name=ndata['first_name']['value'], last_name=ndata['last_name']['value']),
                'NomineeRelationship': preset.NOMINEE_RELATION_CHOICES[ndata['relation']['value']],
                'SumInsured': str(transaction.slab.sum_assured),
                'CriticalIllnessSumInsured': '',
                'GrossMonthlyIncome': '',
                'CumalativeBonus': '',
                'CoverageType': 'NR'
            })

        data['Contact'] = {
            'DOB': datetime.datetime.strftime(transaction.proposer_date_of_birth, '%m/%d/%Y'),
            'PassPort': '',
            'PAN': '',
            'ExternalClientNo': 'CFOX-%s-%s' % (transaction.slab.health_product.insurer.id, transaction.id),
            'Salutation': preset.SALUTATION_CHOICES[int(transaction.proposer_gender)],
            'FirstName': transaction.proposer_first_name,
            'LastName': transaction.proposer_last_name,
            'Sex': preset.GENDER_CHOICES[int(transaction.proposer_gender)],
            'AddressType': 'R',
            'PinCode': transaction.proposer_pincode,
            'State': transaction.proposer_state,
            'AddressLine1': transaction.proposer_address1,
            'AddressLine2': transaction.proposer_address2,
            'AddressLine3': '',
            'AddressLine4': '',
            'FaxNo': '',
            'Country': 'IND',
            'Occupation': pdata['occupation']['value'],
            'City': transaction.proposer_city,
            'Nationality': 'IND',
            'Married': pdata['marital_status']['value'],
            'HomePhone': '',
            'OfficePhone': '',
            'MobilePhone': transaction.proposer_mobile,
            'MailId': transaction.proposer_email
        }

        processed_data = data2xml(data, name='Request', list_name='InsuredDetails')

        send_data = {
            'RESPONSE_URL': '%s/health-plan/iffco-tokio/response/' % settings.SITE_URL,
            'PARTNER_CODE': PARTNER_CODE,
            'UNIQUE_QUOTEID': transaction.reference_id,
            'XML_DATA': processed_data
        }
        return PROPOSAL_URL_IMI, send_data

    def get_processed_data_family(self):
        transaction = Transaction.objects.get(id=self.transaction_id)
        transaction.reference_id = 'CFOX-%s-%s' % (transaction.id, datetime.datetime.now().strftime("%I%M%S"))
        transaction.save()
        city = City.objects.get(id=transaction.extra['city']['id'])
        state = city.state

        pdata = transaction.insured_details_json['form_data']['proposer']['personal']
        address_data = transaction.insured_details_json['form_data']['proposer']['address']
        contact_data = transaction.insured_details_json['form_data']['proposer']['contact']

        data = {
            'Policy': {
                'Product': 'SKP',
                'PlanCode': preset.SLAB_PLAN_DICT[transaction.slab.id],
                'InceptionDate': datetime.datetime.strftime(transaction.policy_start_date, '%m/%d/%Y %H:%M:%S'),
                'ExpiryDate': datetime.datetime.strftime(transaction.policy_end_date, '%m/%d/%Y %H:%M:%S'),
                'UniqueQuoteId': transaction.reference_id,
                'GrossPremium': transaction.extra['premium_response']['grossPremium'],
                'ServiceTax': transaction.extra['premium_response']['serviceTax'],
                'NetPremiumPayable': transaction.extra['premium_response']['netPremiumPayable'],
                'ExternalBranch': BRANCH,
                'ExternalSubBranch': SUB_BRANCH,
                'ExternalServiceConsumer': PARTNER_CODE
            },
            'ListOfSKPInsured': []
        }

        kid_counter = 1
        index = 0
        counter = 0

        mem_priority = ['self', 'spouse', 'father', 'mother', 'son', 'daughter']
        mem_codes = []
        for m in mem_priority:
            for member in transaction.extra['members']:
                if member['person'] == m:
                    mem_codes.append(member['id'])

        for code in mem_codes:
            counter += 1
            fdata = transaction.insured_details_json['form_data']['insured'][code]
            ndata = transaction.insured_details_json['form_data']['nominee'][code]
            member = [m for m in transaction.extra['members'] if m['id'] == code][0]
            is_kid = False

            mdata = transaction.insured_details_json['form_data']
            ins_pre_existing = mdata['medical_question_pre_existing']['combo']['%s_question_pre_existing' % code]['value']
            ins_good_health = mdata['medical_question_good_health']['combo']['%s_question_good_health' % code]['value']
            ins_complaints = mdata['medical_question_complaints']['combo']['%s_question_complaints' % code]['value']
            ins_additional_facts = mdata['medical_question_additional_facts']['combo']['%s_question_additional_facts' % code]['value']
            ins_diseases = mdata['medical_question_diseases']['combo']['%s_question_diseases' % code]['value']
            ins_existing_insurance = mdata['medical_question_existing_insurance']['combo']['%s_question_existing_insurance' % code]['value']

            ins_good_health_details = ''
            ins_complaints_details = ''
            ins_additional_facts_details = ''
            if ins_good_health == 'No':
                ins_good_health_details = mdata['medical_question_good_health']['conditional_good_health']['%s_question_conditional_good_health' % code]['value']
            if ins_complaints == 'Yes':
                ins_complaints_details = mdata['medical_question_complaints']['conditional_complaints']['%s_question_conditional_complaints' % code]['value']
            if ins_additional_facts == 'Yes':
                ins_additional_facts_details = mdata['medical_question_additional_facts']['conditional_additional_facts']['%s_question_conditional_additional_facts' % code]['value']


            insured_relation = None
            if code == 'self':
                insured_relation = 'SELF'
                index += 1
            elif code == 'spouse':
                insured_relation = 'SPOU'
                index += 1
            elif code == 'mother':
                insured_relation = 'MOTR'
                index += 1
            elif code == 'father':
                insured_relation = 'FATR'
                index += 1
            else:
                insured_relation = 'CHD%s' % kid_counter
                kid_counter += 1
                is_kid = True

            member_age = relativedelta(datetime.datetime.today(), datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y').date()).years
            # if member_age == 0:
            #     member_age = (relativedelta(datetime.datetime.today(), datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y').date()).months / 10.0)
            data['ListOfSKPInsured'].append({
                'MemberId': '%s%s' % (transaction.id, counter),
                'FirstName': fdata['first_name']['value'],
                'LastName': fdata['last_name']['value'],
                'Age': member_age,
                'DateOfBirth': datetime.datetime.strftime(datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y'), '%m/%d/%Y'),
                'Gender': preset.GENDER_CHOICES[member['gender']],
                'RelationtoInsured': insured_relation,
                'Nominee': custom_utils.get_full_name(first_name=ndata['first_name']['value'], last_name=ndata['last_name']['value']),
                'SumInsured': '',       #str(transaction.slab.sum_assured),
                'GrossMonthlyIncome': '',
                'Salutation': preset.SALUTATION_CHOICES[int(member['gender'])],
                'Occupation': '',
                'AdditionalFacts': 'Y' if ins_additional_facts == 'Yes' else 'N',
                'AdditionalFactsDetails': ins_additional_facts_details,
                'FutureComplaint': 'Y' if ins_complaints == 'Yes' else 'N',
                'FutureComplaintDetails': ins_complaints_details,
                'GoodHealth': 'Y' if ins_good_health == 'Yes' else 'N',
                'GoodHealthDetails': ins_good_health_details,
                'PreviousPolicyFlag': 'Y' if ins_existing_insurance == 'Yes' else 'N',
                'InsuredPrimaryFlag': 'Y' if index == 1 else 'N',
                'PastHospitalisationFlag': 'Y' if ins_diseases == 'Yes' else 'N'
            })

        data['Contact'] = {
            'DOB': datetime.datetime.strftime(transaction.proposer_date_of_birth, '%m/%d/%Y'),
            'PassPort': '',
            'PAN': '',
            'ExternalClientNo': 'CFOX-%s-%s' % (transaction.slab.health_product.insurer.id, transaction.id),
            'Salutation': preset.SALUTATION_CHOICES[int(transaction.proposer_gender)],
            'FirstName': transaction.proposer_first_name,
            'LastName': transaction.proposer_last_name,
            'Sex': preset.GENDER_CHOICES[int(transaction.proposer_gender)],
            #'AddressType': 'R',
            'PinCode': transaction.proposer_pincode,
            'City': transaction.proposer_city,
            'State': transaction.proposer_state,
            'AddressLine1': transaction.proposer_address1,
            'AddressLine2': transaction.proposer_address2,
            'AddressLine3': '',
            'AddressLine4': '',
            'FaxNo': '',
            'Country': 'IND',
            'Occupation': pdata['occupation']['value'],
            'Nationality': 'IND',
            'Married': pdata['marital_status']['value'],
            'HomePhone': '',
            'OfficePhone': '',
            'MobilePhone': transaction.proposer_mobile,
            'MailId': transaction.proposer_email,
        }

        processed_data = data2xml(data, name='Request', list_name='SKPInsured')

        send_data = {
            'RESPONSE_URL': '%s/health-plan/iffco-tokio/response/' % settings.SITE_URL,
            'PARTNER_CODE': PARTNER_CODE,
            'UNIQUE_QUOTEID': transaction.reference_id,
            'XML_DATA': processed_data
        }
        return PROPOSAL_URL_SKP, send_data


def post_transaction(transaction):
    redirect_url = '/health-plan/product/transaction/%s/failure/' % transaction.transaction_id
    is_redirect = True
    data = {}

    #try:
    if True:
        obj = Processor(transaction.id)
        policy_type = transaction.slab.health_product.policy_type.lower()
        fetch_premium = getattr(obj, 'fetch_premium_%s' % policy_type, '')()
        redirect_url, data = getattr(obj, 'get_processed_data_%s' % policy_type, '')()

        request_logger.info('\nIFFCO TOKIO TRANSACTION: %s \n PROPOSAL REQUEST SENT:\n %s \n' % (transaction.transaction_id, data))
        transaction.payment_request = data
        transaction.save()
        is_redirect = False
        template_name = 'health_product/post_transaction_data.html'
        return is_redirect, redirect_url, template_name, data
    #except urllib2.HTTPError as err:
    else:
        return is_redirect, redirect_url, '', {}


def post_payment(payment_response, transaction=None):
    """
    SUCCESS RESPONSE: {u'ITGIResponse': u'IMI|050010|1-10WTU4Z|2914.62|SUCCESSFULLY_UPDATED_IN_SIEBEL|CFOX-163-103633'}
    FAILED RESPONSE:  {u'ITGIResponse': u'SKP|545564|null|3877.55|Transaction_Declined|CFOX-8770-034418'}
    """
    redirect_url = '/health-plan/payment/failure/'
    is_redirect = True
    request_logger.info('\nIFFCO TOKIO\n PAYMENT RESPONSE RECEIVED:\n %s \n' % payment_response)

    if not 'error' in payment_response:
        resp = payment_response['ITGIResponse']
        response_list = resp.split('|')
        reference_id = response_list[-1]
        transaction = get_transaction_from_reference_id(reference_id)
        transaction.extra['payment_response'] = resp
        transaction.payment_response = resp
        transaction.save()
        if response_list[4].lower().find('success') >= 0:
            transaction.policy_token = response_list[2]
            transaction.policy_number = response_list[2]
            transaction.premium_paid = response_list[3]
            transaction.payment_done = True
            transaction.payment_on = datetime.datetime.now()
            transaction.save()
            redirect_url = '/health-plan/payment/transaction/%s/success/' % transaction.transaction_id
        else:
            transaction.status = 'FAILED'
            transaction.extra['error_msg'] = response_list[4]
            transaction.save()
            redirect_url = '/health-plan/payment/transaction/%s/failure/' % transaction.transaction_id
    else:
        # /health-plan/iffco-tokio/response/?error=Either Request or Return URL is not valid ,Quote IdCFOX-4488-015941
        resp = payment_response['error']
        reference_id = resp.split('Quote Id')[1]
        transaction = get_transaction_from_reference_id(reference_id)
        transaction.extra['error_msg'] = resp
        transaction.save()
        redirect_url = '/health-plan/payment/transaction/%s/failure/' % transaction.transaction_id
    return is_redirect, redirect_url, '', {}
