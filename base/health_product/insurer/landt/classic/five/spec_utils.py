from health_product.insurer import custom_utils

def save_stage(transaction, stage_data):
    current_stage = stage_data['template']
    red_flag = []
    code_list = ['qid_5', 'qid_6', 'qid_7', 'qid_15']
    form_data = custom_utils.clean_stage_data(stage_data)
    # print "FORM DATA"
    # pprint.pprint(form_data)
    for k, v in form_data.items():
        if 'form_data' not in transaction.insured_details_json.keys():
            transaction.insured_details_json['form_data'] = {}
        if k in transaction.insured_details_json['form_data'].keys():
            transaction.insured_details_json['form_data'][k].update(**v)
        else:
            transaction.insured_details_json['form_data'][k] = v
    transaction.save()
    #================== CHECK FOR MEDICAL ====================
    for bigkey, bigval in form_data.items():
        if bigkey.split('_')[0] == 'medical':
            check_key = bigkey.split('_question_')[1]
            for code, mdata in bigval.items():
                for k, v in mdata.items():
                    if check_key in code_list and v['value'] == 'Yes':
                        red_flag.append({'question': v['label'], 'answer': v['value']})
                    if check_key == 'pre_existing' and v['value'] not in ['NONE', '', 'No']:
                        red_flag.append({'question': v['label'], 'answer': v['value']})
    #=========================================================
    if 'red_flags' not in transaction.extra:
        transaction.extra['red_flags'] = {}
    transaction.extra['red_flags'][stage_data['template']] = {'reason':'DISEASE','data':red_flag}
    transaction.save()
    return transaction
