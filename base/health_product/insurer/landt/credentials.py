from django.conf import settings

SOURCE_ID = '8'
USER_NAME = 'coverfox'
PASSWORD = 'coverfox1234$'
PAYMENT_GATEWAY_SOURCE_ID = '630'
TPA_CODE = 'TP00000007'
INTERMEDIARY_CODE = '13C800002'
BRANCH_NAME = 'Mumbai'

#================TEST CREDENTIALS====================
# PAYMENT_URL = 'http://115.111.14.217/ltepayprocessor/hservlet'
# PROPOSAL_URL = 'http://10.70.16.97:7080/maprest/insuranceService/createProposal'
# VPN_USERNAME = 'varun'
# VPN_PASSWORD = 'v@run@347'
#====================================================
#=============PRODUCTION CREDENTIALS=================
SOURCE = 'Cover Fox'
# PAYMENT_URL = 'https://payments.ltinsurance.com/ltepayprocessor/hservlet'
# PROPOSAL_URL = 'https://pigeon.ltinsurance.com:7080/maprest/insuranceService/createProposal'
#====================================================

if settings.PRODUCTION:
    #====================PRODUCTION CREDENTIALS====================
    PAYMENT_URL = 'https://payments.ltinsurance.com/ltepayprocessor/hservlet'
    PROPOSAL_URL = 'https://pigeon.ltinsurance.com:7080/maprest/insuranceService/createProposal'
    #==============================================================
else:
    #====================TEST CREDENTIALS==========================
    PAYMENT_URL = 'http://115.111.14.217/ltepayprocessor/hservlet'
    PROPOSAL_URL = 'http://10.70.16.97:7080/maprest/insuranceService/createProposal'
    #==============================================================



