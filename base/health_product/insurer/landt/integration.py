from django.conf import settings
from wiki.models import CoverPremiumTable, FamilyCoverPremiumTable, Pincode
from collections import OrderedDict
import datetime
import requests
import json
from dateutil.relativedelta import relativedelta
from health_product.insurer import custom_utils
from utils import request_logger
from health_product.insurer.landt.credentials import BRANCH_NAME, TPA_CODE, INTERMEDIARY_CODE, \
                                                    SOURCE_ID, PAYMENT_URL, PAYMENT_GATEWAY_SOURCE_ID, \
                                                    USER_NAME, PASSWORD, PROPOSAL_URL
from health_product.insurer.landt import preset

from mail_utils import health_alert
#=====
# Below is the services link for our Super Top Up product.
#
# http://10.70.16.97:7080/maprest/insuranceService/createProposal
#
# The service is same as that for Prime and classic.
# To use the service for Super Top Up "Scheme" name to be sent as "Super Top Up".
# In case of suminsuredtype= Individual 'memDeductibleAmount' value to be set for each member.(In case of Floater this value to be default 0)
# In case of suminsuredtype= Floater floaterDeductibleAmount to be set.(In case of Individual this value to be default 0)
#
# Rest of the elements and values to be sent is same as that for prime and classic.
#=====

SCHEME_MASTER = {
    'prime' : 1,
    'classic': 2,
    'Super Top Up': 3,
}

PRODUCT_CODE_CHOICES = {
    'prime' : '6109',
    'classic' : '6108',
    'Super Top Up' : '6111',
}
PRODUCT_NAME_CHOICES = {
    'prime' : 'my:health Medisure Prime Insurance Policy',
    'classic' : 'my:health Medisure Classic Policy',
    'Super Top Up' : 'my:health Medisure Super Top Up',
}


LOCATION_MASTER = {
    'ZONE-I' : 1,
    'ZONE-II' : 2,
    'ZONE-III' : 3
}

SUM_INSURED_TYPE_CHOICES = {
    'FAMILY' : 'floater',
    'INDIVIDUAL' : 'individual'
}

BILLING_CYCLE_MASTER = {
    'MONTHLY' : 'Monthly',
    'QUARTERLY' : 'Quaterly',
    'HALF_YEARLY' : 'Half Yearly',
    'YEARLY' : 'Yearly'
}

QUESTION_MASTER = {
    'Does any person, proposed to be insured, suffer from or have been treated for any heart '
    'related ailment / blood pressure?' : 5,
    'Does any person, proposed to be insured, suffer from Diabetes/Asthma/Epilepsy?' : 6,
    'Does any person, proposed to be insured, suffer from any other disease/ailment?' : 7,
    'Is any person, proposed to be insured, receiving any treatment/medication or has in the past received '
    'treatment or undergone surgery for any medical condition/disability?' : 15,
}

GENDER_CHOICES = {
    1 : 'M',
    2 : 'F'
}

STATE_CHOICES = {
    1 : 'Andaman & Nicobar Islands',
    2 : 'Andhra Pradesh',
    3 : 'Arunachal Pradesh',
    4 : 'Assam',
    5 : 'Bihar',
    6 : 'Chandigarh',
    7 : 'Chattisgarh',
    8 : 'Dadra & Nagar Haveli',
    9 : 'Daman & Diu',
    10 : 'Delhi',
    11 : 'Goa',
    12 : 'Gujarat',
    13 : 'Haryana',
    14 : 'Himachal Pradesh',
    15 : 'Jammu & Kashmir',
    16 : 'Jharkhand',
    17 : 'Karnataka',
    18 : 'Kerala',
    19 : 'Lakshadweep',
    20 : 'Madhya Pradesh',
    21 : 'Maharashtra',
    22 : 'Manipur',
    23 : 'Meghalaya',
    24 : 'Mizoram',
    25 : 'Nagaland',
    26 : 'Orissa',
    27 : 'Pondicherry',
    28 : 'Punjab',
    29 : 'Rajasthan',
    30 : 'Sikkim',
    31 : 'Tamil Nadu',
    32 : 'Tripura',
    33 : 'Uttar Pradesh',
    34 : 'Uttarakhand',
    35 : 'West Bengal',
    36 : 'Telangana'
}

RELATIONSHIP_CHOICES = {
    'self' : 'Self',
    'spouse' : 'Spouse',
    'father' : 'Father',
    'mother' : 'Mother',
    'son' : 'Son',
    'son1' : 'Son',
    'son2' : 'Son',
    'son3' : 'Son',
    'son4' : 'Son',
    'daughter' : 'Daughter',
    'daughter1' : 'Daughter',
    'daughter2' : 'Daughter',
    'daughter3' : 'Daughter',
    'daughter4' : 'Daughter',
    #    'mil' : 'Mother In Law',
    #    'fil' : 'Father In Law',
    #    'sister' : 'Sister',
    #    'employee' : 'Employee',
    #    'brother' : 'Brother',
    #    'nephew' : 'Nephew',
    #    'niece' : 'Niece',
    #    'sil' : 'Sister In Law',
    #    'bil' : 'Brother In Law',
    #    'uncle' : 'Uncle',
    #    'aunt' : 'Aunt',
    #    'other' : 'Other',
}


SALUTATION_MASTER = {
    'Miss.' : 'Miss.',
    'CAPT.' : 'CAPT.',
    'Lt.Col.' : 'Lt.Col.',
    'Shri.' : 'Shri.',
    'Baby.' : 'Baby.',
    'Mast.' : 'Mast.',
    'Mr.' : 'Mr.',
    'MS.' : 'MS.',
    'Mrs.' : 'Mrs.',
    'Md.' : 'Md.',
    'Dr.' : 'Dr.'
}

SALUTATION_CHOICES = {
    1: 'Mr.',
    2: 'Miss.',
    3: 'Mrs.'
}

INCOME_MASTER = {
    'Between 2 - 5 Lacs' : 'Between 2 - 5 Lacs',
    'Between 5 - 10 Lacs' : 'Between 5 - 10 Lacs',
    'Between 10 - 20 Lacs' : 'Between 10 - 20 Lacs',
    '20 Lacs and above' : '20 Lacs and above'
}

INCOME_CHOICES = {
    '0_2' : 'Between 2 - 5 Lacs',
    '2_5' : 'Between 2 - 5 Lacs',
    '5_10' : 'Between 5 - 10 Lacs',
    '10_100' : 'Between 10 - 20 Lacs'
}

MARITAL_STATUS_MASTER = {
    'Married' : 'Married',
    'Single' : 'Single',
    'Divorced' : 'Divorced',
    'Widow' : 'Widow',
    'Not Mentioned' : 'Not Mentioned'
}

MARITAL_STATUS_CHOICES = {
    'SINGLE' : 'Single',
    'MARRIED' : 'Married',
}




OCCUPATION_MASTER = {
    'Professional/ Administrative/ Managerial' : 'Professional/ Administrative/ Managerial',
    'Bussiness/ Traders' : 'Bussiness/ Traders',
    'Clercial, Supervisory and related workers' : 'Clercial, Supervisory and related workers',
    'Hospitality and Support Workers' : 'Hospitality and Support Workers',
    'Production Workers, Skilled and non-Agricultural Labourers' : 'Production Workers, Skilled and non-Agricultural Labourers',
    'Farmers and Agricultural Workers' : 'Farmers and Agricultural Workers',
    'Police/ Para Militry/ Defence' : 'Police/ Para Militry/ Defence',
    'Housewife' : 'Housewife',
    'Retired Persons' : 'Retired Persons',
    'Students - School and College' : 'Students - School and College',
    'Others' : 'Others',
    'Profession' : 'Profession',
    'Business' : 'Business',
    'Clerk' : 'Clerk',
    'Student' : 'Student',
    'Agriculture' : 'Agriculture',
    'Professor' : 'Professor',
    'Doctor' : 'Doctor',
    'L&T Employee' : 'L&T Employee',
    'Self Employed' : 'Self Employed',
    'Govt. Service' : 'Govt. Service',
}

OCCUPATION_CHOICES = {
    'PVT' : 'Professional/ Administrative/ Managerial',
    'GOVT' : 'Govt. Service',
    'ARMED_FORCES' : 'Police/ Para Militry/ Defence',
    'SELF' : 'Self Employed',
    'OTHERS' : 'Others',
}

PRIME_MEMBER_TYPE_MASTER = {
    '2 Adults' : 1,
    '2 Adults and 1 Children' : 2,
    '2 Adults and 2 Children' : 3,
    'Individual' : 4,
    '1 Adult and 1 Children' : 5,
    '1 Adult and 2 Children' : 6,
    '1 Adult and 3 Children' : 7
}

PRIME_MEMBER_TYPE_CHOICES = {
    '2A-0C' : 1,
    '2A-1C' : 2,
    '2A-2C' : 3,
    '1A-0C' : 4,
    '1A-1C' : 5,
    '1A-2C' : 6,
    '1A-3C' : 7
}

PRIME_MEMBER_AGE_MASTER = {
    '3m TO 25' : 15,
    '26 TO 35' : 16,
    '36 TO 40' : 2,
    '41 TO 45' : 3,
    '46 TO 50' : 4,
    '51 TO 55' : 5,
    '56 TO 60' : 6,
    '61 TO 65' : 7,
    '66 TO 70' : 8,
    '71 TO 75' : 10,
    '76 TO 80' : 9,
    '81 TO 85' : 11,
    'ABOVE 85' : 14
}

CLASSIC_MEMBER_TYPE_MASTER = {
    '2 Adults' : 1,
    '2 Adults and 1 Children' : 2,
    '2 Adults and 2 Children' : 3,
    'Individual' : 4,
    '1 Adult and 1 Children' : 5,
    '1 Adult and 2 Children' : 6,
    '1 Adult and 3 Children' : 7,
    '1 Adult 4 Children' : 8,
    }

CLASSIC_MEMBER_TYPE_CHOICES = {
    '2A-0C' : 1,
    '2A-1C' : 2,
    '2A-2C' : 3,
    '1A-0C' : 4,
    '1A-1' : 5,
    '1A-2C' : 6,
    '1A-3C' : 7,
    '1A-4C' : 8,
    }


CLASSIC_MEMBER_AGE_MASTER = {
    'UP TO 35' : 1,
    '36 TO 40' : 2,
    '41 TO 45' : 3,
    '46 TO 50' : 4,
    '51 TO 55' : 5,
    '56 TO 60' : 6,
    '61 TO 65' : 7,
    '66 TO 70' : 8,
    '71 TO 75' : 10,
    '76 TO 80' : 9,
    'ABOVE 80' : 13,
    }

ZONE_CHOICES = {
    tuple([589, 935, 945, 952, 964, 974, 980, 990, 993, 1006, 1008, 2325, 2346, 2447, 2899, 2922, 4025, 4105, 4192,
           4404]) : 'ZONE-I',
    tuple([1330, 66, 2383, 3223, 4961, 699, 700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715,
           716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736,
           737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757,
           758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772, 773, 774, 775, 776, 777, 778,
           779, 780, 781, 782, 783, 784, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 796, 797, 798, 799,
           800, 801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820,
           821, 822, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832, 833, 834, 835, 836, 837, 838, 839, 840, 841,
           842, 843, 844, 845, 846, 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859, 860, 861, 862,
           863, 864, 865, 866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883,
           884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 898, 899, 900, 901, 902, 903, 904,
           905, 906, 907, 908, 909, 910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 920]) : 'ZONE-II',
    }


class Processor():
    def __init__(self, transaction):
        self.transaction = transaction

    def get_processed_data(self):
        policy_code = ''
        this_sum_insured_type = ''
        if self.transaction.slab.health_product.id in [33]:
            policy_code = 'prime'
            this_sum_insured_type = 'floater'
        elif self.transaction.slab.health_product.id in [23]:
            policy_code = 'prime'
            this_sum_insured_type = 'individual'
        elif self.transaction.slab.health_product.id in [25, 27, 29, 32]:
            policy_code = 'classic'
            this_sum_insured_type = 'floater'
        elif self.transaction.slab.health_product.id in [22, 26, 28, 31]:
            policy_code = 'classic'
            this_sum_insured_type = 'individual'
        elif self.transaction.slab.health_product.id in [71]:
            policy_code = 'Super Top Up'
            this_sum_insured_type = 'floater'
        elif self.transaction.slab.health_product.id in [70]:
            policy_code = 'Super Top Up'
            this_sum_insured_type = 'individual'

        form_data = self.transaction.insured_details_json['form_data']
        pdata = form_data['proposer']['personal']
        if self.transaction.proposer_gender == 2 and pdata['marital_status']['value'] == 'MARRIED':
            salutation = SALUTATION_CHOICES[3]
        else:
            salutation = SALUTATION_CHOICES[int(self.transaction.proposer_gender)]

        pincode = Pincode.objects.filter(pincode=self.transaction.extra['pincode'])[0]
        state = pincode.state

        city_zone = 'ZONE-III'
        for key, val in ZONE_CHOICES.items():
            if self.transaction.extra['city']['id'] in key:
                city_zone = val

        prop_father_name = pdata['father_name']['value']
        prop_income = pdata['annual_income']['value']
        prop_occupation = pdata['occupation']['value']
        #TODO: TEST IF WORKS
        if 'marital_status' in pdata.keys():
            self.transaction.proposer_marital_status = pdata['marital_status']['value']
        if 'date_of_birth' in pdata.keys():
            self.transaction.proposer_date_of_birth = datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y')
        if 'gender' in pdata.keys():
            self.transaction.proposer_gender = int(pdata['gender']['value'])
        self.transaction.save()

        landline_number = self.transaction.insured_details_json['form_data']['proposer']['contact']['landline']['display_value']
        self.transaction.proposer_landline = landline_number

        client_object = OrderedDict([
            ('ClientRefNo', ''),
            ('Title', salutation),
            ('FirstName', self.transaction.proposer_first_name),
            ('MiddleName', self.transaction.proposer_middle_name),
            ('LastName', self.transaction.proposer_last_name),
            ('DateOfBirth', datetime.datetime.strftime(self.transaction.proposer_date_of_birth, '%d-%m-%Y')),
            ('FatherName', prop_father_name),
            ('Gender', GENDER_CHOICES[int(self.transaction.proposer_gender)]),
            ('MaritalStatus', MARITAL_STATUS_CHOICES[self.transaction.proposer_marital_status]),
            ('PanCard', ''),
            ('Occupation', prop_occupation),
            ('AnnualIncome', prop_income),
            ('BlockNo', self.transaction.proposer_address1),
            ('FloorNo', ''),
            ('BuildingName', ''),
            ('StreetName', self.transaction.proposer_address2),
            ('Locality', ''),
            ('LandMark', self.transaction.proposer_address_landmark if self.transaction.proposer_address_landmark else 'NA'),
            ('CountryName', 'India'),
            ('StateName', STATE_CHOICES[state.id]),
            ('CityName', self.transaction.insured_details_json['form_data']['proposer']['address']['city']['value']),
            ('PinCode', self.transaction.insured_details_json['form_data']['proposer']['address']['pincode']['value']),
            ('PostOffice', ''),
            ('Tehsil', self.transaction.insured_details_json['form_data']['proposer']['address']['city']['value']),
            ('Mobile', self.transaction.proposer_mobile),
            ('StdCodeWithLandLine', landline_number),
            ('Fax', ''),
            ('CommunicationAutoMailer', 'Y'),
            ('CommunicationPhysicalLetter', 'N'),
            ('Remarks', 'NA'),
            #('PSNumber', ''),
            #('DivisionName', '')
        ])

        floater_sum_insured = str(self.transaction.slab.sum_assured) if this_sum_insured_type == 'floater' else '0'
        sum_insured = str(self.transaction.slab.sum_assured) if this_sum_insured_type == 'individual' else '0'

        sub_limits = 'Y' if int(self.transaction.slab.health_product.id) in [26, 27, 31, 32] else 'N'
        si_critical = 'Y' if int(self.transaction.slab.health_product.id) in [28, 29, 31, 32] else 'N'

        soft_copy = 'N' if self.transaction.insured_details_json['form_data']['terms']['combo']['soft_copy'] == 'No' else 'Y'

        self.transaction.no_of_people_insured = len(self.transaction.insured_details_json['form_data']['insured'].keys())
        self.transaction.policy_start_date = (datetime.datetime.today() + relativedelta(days=1)).date()
        policy_term = self.transaction.insured_details_json['form_data']['proposer']['personal']['policy_term']['value1']
        self.transaction.policy_end_date = (self.transaction.policy_start_date + relativedelta(days=-1, years=policy_term))

        proposer_object = OrderedDict([
            ('suminsuredtype', this_sum_insured_type),
            ('policyduration',self.transaction.insured_details_json['form_data']['proposer']['personal']['policy_term']['value1']),
            ('locationCode', city_zone),
            ('familysize', str(len(self.transaction.insured_details_json['form_data']['insured'].keys()))),
            #('age', relativedelta(datetime.datetime.today(), self.transaction.proposer_date_of_birth).years),
            ('floaterSumInsured', floater_sum_insured),
            ('doublesuminsured' , si_critical),
            ('roomrent', sub_limits),
            ('proposalRef', ''),    #NEW FIELD
            ('proposalRef', self.transaction.transaction_id),
            ('welcomeKitNo', ''),
            ('praWelcomeKitNo', 'N'),
            ('remark', 'NA'),
            ('paidAmount', '%0.f' % float(self.transaction.extra['payment_response']['TxnAmount'])),
            ('rollOver', 'N'),
            ('EmailID', self.transaction.proposer_email),
            ('scheme', policy_code.title()),
            ('branchName', BRANCH_NAME),
            ('tpaCode', TPA_CODE),
            ('productType', policy_code.title()),     # NEW FIELD
            # ('productType', ''),     #NEW FIELD
            ('intermediaryCode', INTERMEDIARY_CODE),
            ('proposalReceivedDate', datetime.datetime.strftime(datetime.datetime.today(), '%d-%m-%Y')),
            ('policyStartDate', datetime.datetime.strftime(self.transaction.policy_start_date, '%d-%m-%Y')),
            ('policyStartTime', '01:00 AM'),
            ('portablity', 'N'),
            # ('Suminsured', ''),
            ('portNo', '0'),
            ('billingCycle', 'Yearly'),
            ('paymentMode', 'Card'),
            ('instrumentNo', self.transaction.extra['payment_response']['TxnReferenceNo']),
            ('instrumentDate', datetime.datetime.strftime(datetime.datetime.today(), '%d-%m-%Y')),
            ('exclusion', ''),
            ('signingLocation', 'Mumbai'),
            ('signingDate', datetime.datetime.strftime(datetime.datetime.today(), '%d-%m-%Y')),
            ('eDispatch', soft_copy),
            ('floaterDeductibleAmount', ''),   # NEW FIELD
            ('noPrePost', ''),     # NEW FIELD
            ('reinstatement', ''),     # NEW FIELD
            ('extendedPrePost', ''),     # NEW FIELD
            ('hospitalCash', ''),     # NEW FIELD
            ('personAccompany', ''),     # NEW FIELD
            ('benefit', ''),     # NEW FIELD
            ('optDeductableAmt', ''),     # NEW FIELD
        ])
        insured_list = []
        ins_occupation = prop_occupation
        for code, fdata in self.transaction.insured_details_json['form_data']['insured'].items():
            ndata = self.transaction.insured_details_json['form_data']['nominee'][code]

            if code == 'self':
                ins_marital_status = pdata['marital_status']['value']
            elif code in ['spouse', 'father', 'mother']:
                ins_marital_status = 'MARRIED'
            else:
                ins_marital_status = 'SINGLE'

            feet = fdata['height']['value1']
            inches = fdata['height']['value2']
            if not feet:
                feet = 0
            if not inches:
                inches = 0
            ins_height = str(int(((int(feet) * 30.48) + int(inches) * 2.54)))

            ins_weight = fdata['weight']['value']
            ins_gender = int([member['gender'] for member in self.transaction.extra['members'] if member['id'] == code][0])
            form_data =self.transaction.insured_details_json['form_data']

            medical_answer1 = medical_answer2 = medical_answer3 = medical_answer4 = medical_pre_existing = 'Y'
            if form_data['medical_question_qid_5']['qid_5']['%s_question_qid_5' % code]['value'] == 'No':
                medical_answer1 = 'N'
            if form_data['medical_question_qid_6']['qid_6']['%s_question_qid_6' % code]['value'] == 'No':
                medical_answer2 = 'N'
            if form_data['medical_question_qid_7']['qid_7']['%s_question_qid_7' % code]['value'] == 'No':
                medical_answer3 = 'N'
            if form_data['medical_question_qid_15']['qid_15']['%s_question_qid_15' % code]['value'] == 'No':
                medical_answer4 = 'N'
            if form_data['medical_question_pre_existing']['pre_existing']['%s_question_pre_existing' % code]['value'] == 'No':
                medical_pre_existing = 'N'

            remarks1 = remarks2 = remarks3 = remarks4 = pre_existing_remarks = ''
            if medical_answer1 == 'Y':
                remarks1 = form_data['medical_question_qid_5']['qid_5_details']['%s_question_qid_5_details' % code]['value']
            if medical_answer2 == 'Y':
                remarks2 = form_data['medical_question_qid_6']['qid_6_details']['%s_question_qid_6_details' % code]['value']
            if medical_answer3 == 'Y':
                remarks3 = form_data['medical_question_qid_7']['qid_7_details']['%s_question_qid_7_details' % code]['value']
            if medical_answer4 == 'Y':
                remarks4 = form_data['medical_question_qid_15']['qid_15_details']['%s_question_qid_15_details' % code]['value']
            if medical_pre_existing == 'Y':
                # multi-select: returns an array
                pre_existing_remarks = form_data['medical_question_pre_existing']['pre_existing_details']['%s_question_pre_existing_details' % code]['value']
                pre_existing_remarks = ','.join(pre_existing_remarks)

            # temporary fix
            medical_pre_existing = pre_existing_remarks
            pre_existing_remarks = ''

            # TODO: confirm with L&T what they want
            medical_remarks = ''

            insured_list.append(
                OrderedDict([
                    #('MemRefNo', ''),   #NEW FIELD
                    ('FirstName', fdata['first_name']['value']),
                    ('Title', SALUTATION_CHOICES[ins_gender]),
                    ('MiddleName', ''),
                    ('LastName', fdata['last_name']['value']),
                    ('Gender', GENDER_CHOICES[ins_gender]),
                    ('MaritalStatus', MARITAL_STATUS_CHOICES[ins_marital_status]),
                    ('Relation', RELATIONSHIP_CHOICES[code]),
                    ('Occupation', ins_occupation),
                    ('DateOfBirth', datetime.datetime.strftime(datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y'), '%d-%m-%Y')),
                    ('Height', ins_height),
                    ('Weight', ins_weight),
                    ('Nominee', custom_utils.get_full_name(first_name=ndata['first_name']['value'], last_name=ndata['last_name']['value'])),
                    ('MemRelation', ndata['relation']['value'].lower().capitalize()),
                    ('PreExistingIllness', medical_pre_existing),
                    ('PreExistingIllnessDesc', pre_existing_remarks),
                    ('memIllness', ''),
                    ('memDateOfFirstConsultant', ''),
                    ('memTreatingDoctor', ''),
                    ('memHospitalName', ''),
                    ('memHospitalAddress', ''),
                    ('memFullyCare', ''),
                    ('memRollOver', 'N'),
                    ('memIdentityType', 'NONE'),
                    ('memIdentityValue', 'NONE'),
                    ('memPortablity', 'N'),
                    ('Suminsured', sum_insured),
                    ('PrevCB', ''),
                    ('PrevSI', ''),
                    ('memCurrentCB', ''),
                    ('memBreakin', ''),
                    ('Prevclaims', 'N'),
                    ('memSiForCritical', si_critical),
                    ('memRoomRentSubLimit', sub_limits),
                    #('strMode', ''),
                    ('ANS1', medical_answer1),
                    ('ANS2', medical_answer2),
                    ('ANS3', medical_answer3),
                    ('ANS4', medical_answer4),
                    ('queRemark1', remarks1),
                    ('queRemark2', remarks2),
                    ('queRemark3', remarks3),
                    ('queRemark4', remarks4),
                    ('Remarks', medical_remarks),
                    #('memDeductibleAmount', '0')         #NEW FIELD
                ])
            )
        if policy_code == 'Super Top Up':
            proposer_object['suminsuredtype'] = proposer_object['suminsuredtype'].capitalize()
            deductible = str(self.transaction.slab.deductible)
            for mem_dict in insured_list:
                mem_dict['memDeductibleAmount'] = deductible if this_sum_insured_type == 'individual' else '0'
            if this_sum_insured_type == 'floater':
                proposer_object['floaterDeductibleAmount'] = deductible

        data = OrderedDict([
            ('requestId', ''),
            ('vendorTxnId', self.transaction.transaction_id),   #NEW FIELD
            ('sourceSystemId', SOURCE_ID),
            ('clientObj', client_object),
            ('propObj', proposer_object),
            ('memObjList' , insured_list),
        ])

        self.transaction.save()
        return data


def post_transaction(transaction):
    policy_code = ''
    this_sum_insured_type = ''
    if transaction.slab.health_product.id in [33]:
        policy_code = 'prime'
        this_sum_insured_type = 'floater'
    elif transaction.slab.health_product.id in [23]:
        policy_code = 'prime'
        this_sum_insured_type = 'individual'
    elif transaction.slab.health_product.id in [25, 27, 29, 32]:
        policy_code = 'classic'
        this_sum_insured_type = 'floater'
    elif transaction.slab.health_product.id in [22, 26, 28, 31]:
        policy_code = 'classic'
        this_sum_insured_type = 'individual'
    elif transaction.slab.health_product.id in [71]:
        policy_code = 'Super Top Up'
        this_sum_insured_type = 'floater'
    elif transaction.slab.health_product.id in [70]:
        policy_code = 'Super Top Up'
        this_sum_insured_type = 'individual'

    transaction.reference_id = '%s11%s' % (PRODUCT_CODE_CHOICES[policy_code], transaction.id)

    is_redirect = False
    redirect_url = PAYMENT_URL
    form_data = transaction.insured_details_json['form_data']
    term = form_data['proposer']['personal']['policy_term']['value1']

    cpt_id = transaction.extra['cpt_id']
    if transaction.slab.health_product.policy_type == 'FAMILY':
        cpt = FamilyCoverPremiumTable.objects.get(id=cpt_id)
    else:
        cpt = CoverPremiumTable.objects.get(id=cpt_id)
    premium = cpt.premium

    discounted_premium = round((premium*term)/100 * (100 - preset.TERM_DISCOUNTS[term]))

    data = {
        'policyName' : PRODUCT_NAME_CHOICES[policy_code],
        'transactionAmount' : discounted_premium if settings.PRODUCTION else '1',
        'sourceChannel' : PAYMENT_GATEWAY_SOURCE_ID,
        'proposalNumber' : transaction.reference_id,
        'responseURL' : '%s/health-plan/11/transaction/%s/response/' % (settings.SITE_URL, transaction.transaction_id),
        'custContactId' : transaction.proposer_mobile,
        'productCode' : PRODUCT_CODE_CHOICES[policy_code],
        'firstName' : transaction.proposer_first_name,
        'lastName' : transaction.proposer_last_name,
        'emailId' : transaction.proposer_email,
        #'mobileNumber' : transaction.proposer_mobile,
        #'msg' : '',
    }
    request_logger.info('\nLANDT TRANSACTION: %s \n PAYMENT REQUEST SENT:\n %s \n' % (transaction.transaction_id, data))
    template_name = 'health_product/post_transaction_data.html'
    transaction.payment_request = data
    transaction.save()
    return is_redirect, redirect_url, template_name, data


def post_payment(payment_response, transaction):
    """
    http://www.coverfox.com/health-plan/2/response/?msg=LNTGENINS|539fd1479e|MCIT3266864624|213942-280390|2.00|CIT|22449557|NA|INR|ONDIRECT|NA|NA|NA|25-02-2014 20:34:41|0300|NA|630|NA|NA|NA|NA|NA|NA|NA|Success|1298140905'}
    {'AdditionalInfo1': u'630',
     'AdditionalInfo2': u'NA',
     'AdditionalInfo3': u'NA',
     'AdditionalInfo4': u'NA',
     'AdditionalInfo5': u'NA',
     'AdditionalInfo6': u'NA',
     'AdditionalInfo7': u'NA',
     'AuthStatus': u'0300',
     'BankID': u'CIT',
     'BankMerchantID': u'22449557',
     'BankReferenceNo': u'213942-280390',
     'CheckSum': u'1298140905',
     'CurrencyName': u'INR',
     'CustomerID': u'539fd1479e',
     'ErrorDescription': u'Success',
     'ErrorStatus': u'NA',
     'ItemCode': u'ONDIRECT',
     'MerchantID': u'LNTGENINS',
     'SecurityID': u'NA',
     'SecurityPassword': u'NA',
     'SecurityType': u'NA',
     'SettlementType': u'NA',
     'TxnAmount': u'2.00',
     'TxnDate': u'25-02-2014 20:34:41',
     'TxnReferenceNo': u'MCIT3266864624',
     'TxnType': u'NA'}
    """

    data = {}
    pay_res = payment_response['msg'].split('|')
    response_data = {
        'MerchantID' : pay_res[0],
        'CustomerID' : pay_res[1],
        'TxnReferenceNo' : pay_res[2],
        'BankReferenceNo' : pay_res[3],
        'TxnAmount' : pay_res[4],
        'BankID' : pay_res[5],
        'BankMerchantID' : pay_res[6],
        'TxnType' : pay_res[7],
        'CurrencyName' : pay_res[8],
        'ItemCode' : pay_res[9],
        'SecurityType' : pay_res[10],
        'SecurityID' : pay_res[11],
        'SecurityPassword' : pay_res[12],
        'TxnDate' : pay_res[13],
        'AuthStatus' : pay_res[14],
        'SettlementType' : pay_res[15],
        'AdditionalInfo1' : pay_res[16],
        'AdditionalInfo2' : pay_res[17],
        'AdditionalInfo3' : pay_res[18],
        'AdditionalInfo4' : pay_res[19],
        'AdditionalInfo5' : pay_res[20],
        'AdditionalInfo6' : pay_res[21],
        'AdditionalInfo7' : pay_res[22],
        'ErrorStatus' : pay_res[23],
        'ErrorDescription' : pay_res[24],
        'CheckSum' : pay_res[25],
    }

    request_logger.info('\nLANDT TRANSACTION: %s \n PAYMENT RESPONSE RECEIVED:\n %s \n' % (transaction.transaction_id, response_data))

    transaction.extra['payment_response'] = response_data
    transaction.payment_response = json.dumps(response_data)
    transaction.payment_token = response_data['TxnReferenceNo']
    transaction.premium_paid = str(float(transaction.extra['payment_response']['TxnAmount']))

    is_redirect = True
    redirect_url = '/health-plan/payment/transaction/%s/failure/' % transaction.transaction_id

    data = {}
    if transaction.extra['payment_response']['AuthStatus'] == '0300':
        if transaction.policy_token:
            redirect_url = '/health-plan/payment/transaction/%s/success/' % transaction.transaction_id
        else:
            transaction.payment_done = True
            transaction.payment_on = datetime.datetime.today()
            transaction.save()

            obj = Processor(transaction)
            data = obj.get_processed_data()
            transaction.extra['proposal_data'] = data

            json_data = json.dumps(data)
            request_logger.info('\nL&T TRANSACTION: %s \n POLICY REQUEST SENT:\n %s \n'
                                % (transaction.transaction_id, json_data))

            post_data = {
                'username': USER_NAME,
                'password': PASSWORD,
                'data': json_data
            }
            policy_resp = requests.get(PROPOSAL_URL, params=post_data, verify=False)

            transaction.extra['product_response'] = json.loads(policy_resp.text)
            request_logger.info('\nL&T TRANSACTION: %s \n POLICY RESPONSE RECEIVED:\n %s \n'
                                % (transaction.transaction_id, transaction.extra['product_response']))

            if not transaction.extra['product_response']['status'] == 'Success' or not transaction.extra['product_response']['proposal']['proposalRef']:
                transaction.extra['error_msg'] = transaction.extra['product_response']['errorMessage']
                msg_text = 'Transaction ID: %s\n\nTransaction UID: %s\n\n' % (transaction.id, transaction.transaction_id)
                msg_text = '%sProposal generation failed, post payment for L&T' % msg_text
                sub_text = 'ALERT: Proposal generation failed'
                health_alert(sub_text, msg_text)
            else:
                transaction.policy_token = transaction.extra['product_response']['proposal']['proposalRef']
                redirect_url = '/health-plan/payment/transaction/%s/success/' % transaction.transaction_id
    else:
        transaction.extra['error_msg'] = response_data['ErrorDescription']
    transaction.save()
    return is_redirect, redirect_url, '', {}


def redo_transaction(transaction):
    obj = Processor(transaction)
    data = obj.get_processed_data()
    transaction.extra['proposal_data'] = data
    transaction.save()

    json_data = json.dumps(data)
    post_data = {
        'username': USER_NAME,
        'password': PASSWORD,
        'data': json_data
    }
    request_logger.info('\nL&T TRANSACTION: %s \n REDO POLICY REQUEST SENT:\n %s \n'
                        % (transaction.transaction_id, json_data))
    response = requests.get(PROPOSAL_URL, params=post_data, verify=False)
    transaction.extra['product_response'] = json.loads(response.text)
    request_logger.info('\nLANDT TRANSACTION: %s \n REDO POLICY RESPONSE RECEIVED:\n %s \n'
                        % (transaction.transaction_id, transaction.extra['product_response']))

    if transaction.extra['product_response']['status'] == 'Success' or transaction.extra['product_response']['proposal']['proposalRef']:
        transaction.policy_token = transaction.extra['product_response']['proposal']['proposalRef']
    transaction.save()
    return response.text
