from wiki.models import City, Pincode, Slab
from dateutil.relativedelta import datetime, relativedelta
from health_product.insurer import custom_utils
from health_product.insurer.max_bupa import preset
from copy import deepcopy
from health_product.services.match_plan import MatchPlan
import putils


def get_premium_details(data, slab_id):
    '''
    specific for max bupa calculate discount on 2nd year premium only
    '''
    base_premiums = {}
    discounted_premiums = {}
    year_wise_premium = {}
    cpt_ids = {}
    slab = Slab.objects.get(id=slab_id)
    new_data = deepcopy(data)
    for i in range(1, 3):
        members = new_data['members']
        for m in members:
            m['age'] = m['age'] + (i-1)
        data_to_score = putils.prepare_post_data_to_score(new_data)
        mp = MatchPlan(data_to_score, slab=slab, request=None)
        cpt = mp.get_cpt()
        base_premiums[i] = cpt.premium + base_premiums.get(i-1, 0)
        year_wise_premium[i] = cpt.premium
        discounted_premiums[i] = round(year_wise_premium[i] * (1 - preset.TERM_DISCOUNTS[i]/100))
        discounted_premiums[i] = sum(discounted_premiums.values())
        # discounted_premiums[i] = round(base_premiums[i] * (1 - preset.TERM_DISCOUNTS[i]/100))
        cpt_ids[i] = cpt.id

    return {
        'base_premiums': base_premiums,
        'discounted_premiums': discounted_premiums,
        'cpt_ids': cpt_ids,
    }


class Formic():
    POLICY_TERMS = []
    POLICY_DISCOUNTS = []
    for k in preset.TERM_DISCOUNTS:
        POLICY_TERMS.append({'id': k, 'name': '%s year(s)' % k})
        POLICY_DISCOUNTS.append({'id': k, 'name': '%s' % preset.TERM_DISCOUNTS[k]})

    def __init__(self, transaction, is_self_present):
        self.transaction = transaction
        self.is_self_present = is_self_present
        self.proposer_data = {
            'pincode': transaction.extra['pincode'],
            'city': City.objects.get(id=transaction.extra['city']['id']).name,
            'state': City.objects.get(id=transaction.extra['city']['id']).state.name,
            'email': ''
        }

    def add_conditional_segment(self, segment_id, tag_id, parent_question_code, details, question_type, options=None):
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': details, 'data': []}
        row = []
        if question_type == 'MultipleChoiceField':
            new_segment['details']['tier'] = 2
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='MultipleChoiceField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    details=member,
                    value='No',
                    label=details.get('question_text', ''),
                    span=4,
                    required=False,
                    dependant_parent='%s_question_%s' % (member['id'], parent_question_code),
                    css_class='bottom-margin',
                    event=', click : $parent.root_context.stage_5.any_yes_check',
                    only_if='root_context.stage_5.check_dict.get(\'%s_question_%s\')' % (member['id'], parent_question_code),
                    template_option='question'
                    ))
            new_segment['data'].append(row)
        elif question_type == 'TwoDropDownField':
            new_segment['details']['tier'] = 2
            for member in self.transaction.extra['members']:
                year_range = []
                for i in range((datetime.datetime.now() - relativedelta(years=member['age'])).year, datetime.datetime.today().year +1):
                    year_range.append({'id' : i, 'name' : i})
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='TwoDropDownField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    options1=self.month_range,
                    options2=year_range,
                    value='No',
                    label=details.get('question_text', ''),
                    span=2,
                    dependant_parent='%s_question_%s' % (member['id'], parent_question_code),
                    required=False,
                    only_if='root_context.stage_5.check_dict.get(\'%s_question_%s\')' % (member['id'], parent_question_code),
                    template_option='question'
                    ))
            new_segment['data'].append(row)
        elif question_type in ['TextField', 'IntegerField', 'DateField']:
            new_segment['details']['tier'] = 2
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type=question_type,
                    code='%s_question_%s' % (member['id'], tag_id),
                    span=12,
                    label=details.get('question_text', ''),
                    dependant_parent='%s_question_%s' % (member['id'], parent_question_code),
                    required=False,
                    only_if='root_context.stage_5.check_dict.get(\'%s_question_%s\')' % (member['id'], parent_question_code),
                    ))
            new_segment['data'].append(row)
        elif question_type == 'MultipleSelectField':
            new_segment['details']['tier'] = 2
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type=question_type,
                    code='%s_question_%s' % (member['id'], tag_id),
                    options=options,
                    span=12,
                    label=details.get('question_text', ''),
                    dependant_parent='%s_question_%s' % (member['id'], parent_question_code),
                    required=False,
                    only_if='root_context.stage_5.check_dict.get(\'%s_question_%s\')' % (member['id'], parent_question_code),
                ))
            new_segment['data'].append(row)
        return new_segment
    def add_parent_segment(self, segment_id, tag_id, details, question_type):
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': details, 'data': []}
        row = []
        if question_type == 'MultipleChoiceField':
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='MultipleChoiceField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    details=member,
                    value='No',
                    label=details.get('question_text', ''),
                    span=4,
                    css_class='bottom-margin',
                    event=', click : $parent.root_context.stage_5.any_yes_check',
                    template_option='question'
                ))
            new_segment['data'].append(row)
        elif question_type == 'DropDownField':
            if tag_id == 'pre_existing':
                for member in self.transaction.extra['members']:
                    row.append(custom_utils.new_field(
                        saved_data=saved_data,
                        field_type='DropDownField',
                        code='%s_question_%s' % (member['id'], tag_id),
                        options=preset.pre_existing_options,
                        details=member,
                        label=details.get('question_text', ''),
                        span=2,
                        css_class='bottom-margin',
                        event=', event : {change : root_context.stage_5.any_yes_check}',
                        template_option='question'
                    ))
            new_segment['data'].append(row)
        elif question_type == 'TextField':
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='TextField',
                    label=details.get('question_text', ''),
                    code='%s_question_%s' % (member['id'], tag_id),
                    span=2,
                    required=False,
                    only_if='root_context.stage_5.check_dict.get(\'%s\')' % (member['id'])
                ))
            new_segment['data'].append(row)

        return new_segment

    def stage_1(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'personal'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': {}, 'data': []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            label='First Name',
            code='first_name',
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            label='Middle Name',
            code='middle_name',
            required=False
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            label='Last Name',
            code='last_name',
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Marital Status',
            code='marital_status',
            options=preset.MARITAL_STATUS_OPTIONS
        ))
        if self.is_self_present:
            member = [m for m in self.transaction.extra['members'] if m['id'] == 'self'][0]
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                min_age=18,
                max_age=100,
                default_age=member['age'],
            ))
        else:
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                min_age=18,
                max_age=100,
                default_age=18,
            ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Occupation',
            code='occupation',
            options=preset.OCCUPATION_OPTIONS
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            label='Nationality',
            code='nationality',
            value='Indian',
            template_option='static'
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Educational Qualification',
            code='educational_qulification',
            options=preset.EQ_OPTION
        ))
        new_segment['data'].append(row)

        row = []
        premium_data = get_premium_details(deepcopy(self.transaction.extra), self.transaction.slab_id)
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='PremiumDiscountField',
            code='policy_term',
            span=6,
            options1=self.POLICY_TERMS,
            options2=self.POLICY_DISCOUNTS,
            base_premiums=premium_data['base_premiums'],
            discounted_premiums=premium_data['discounted_premiums'],
            required=True,
            main_label='Policy term',
            dep_label='Discount',
            dep_label1='Premium',
            premium=self.transaction.extra['plan_data']['premium'],
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)

        return {'template': 'stage_1', 'data': stage}

    def stage_2(self):
        stage = []
        feet_range = []
        for feet in range(0, 9):
            feet_range.append({'id': feet, 'name': '%s ft' % feet})
        inch_range = []
        for inch in range(0, 12):
            inch_range.append({'id': inch, 'name': '%s inches' % inch})

        for member in self.transaction.extra['members']:
            segment_id = 'insured'
            tag_id = member['id']
            template_switch = 'dynamic'
            if tag_id == 'self':
                template_switch = 'static'
            try:
                saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment': segment_id, 'tag': tag_id, 'details': member, 'data': []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                label='First Name',
                code='first_name',
                max_length=50,
                template_option=template_switch,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                label='Middle Name',
                code='middle_name',
                max_length=50,
                template_option=template_switch,
                span=3,
                required=False
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                label='Last Name',
                code='last_name',
                max_length=50,
                template_option=template_switch,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                span=3,
                template_option=template_switch,
                default_age=member['age'],
                min_age=18 if member['type'] == 'adult' else 0,
                max_age=100,
            ))
            new_segment['data'].append(row)

            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DropDownField',
                label='Occupation',
                code='occupation',
                options=preset.OCCUPATION_OPTIONS,
                span=3,
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DropDownField',
                label='Educational Qualification',
                code='educational_qulification',
                options=preset.EQ_OPTION
            ))
            new_segment['data'].append(row)

            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='TwoDropDownField',
                label='Height',
                code='height',
                options1=feet_range,
                options2=inch_range
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='IntegerField',
                label='Weight',
                code='weight',
                sub_text='Kgs',
                span=2
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='IntegerField',
                label='Waistline (cm)',
                code='waistline',
                span=2
            ))
            new_segment['data'].append(row)
            stage.append(new_segment)

        return {'template': 'stage_2', 'data': stage}

    def stage_3(self):
        # Needs to run before the big loop on members
        stage = []
        segment_id = 'nominee'
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': {'verbose': 'insured'}, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            code='relation',
            label='Relationship With Proposer',
            options=preset.relation_options,
            span=3
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='nominee_first_name',
            label='First Name',
            max_length=50,
            span=3
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='nominee_last_name',
            label='Last Name',
            max_length=50,
            span=3
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DateField',
            code='date_of_birth',
            label='Date Of Birth',
            min_age=18,
            span=3
        ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='nominee_address',
            label='Address',
            max_length=150,
            span=3
        ))

        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MobileField',
            code='nominee_contact_no',
            label='Contact NO',
            css_class='bottom-margin',
            span=3
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template': 'stage_3', 'data': stage}

    def stage_4(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'address'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': {}, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address1',
            label='Flat/House No, Street',
            span=12,
            css_class='bottom-margin',
            max_length=50
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address2',
            label='Area/Locality',
            span=12,
            css_class='bottom-margin',
            max_length=100,
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='landmark',
            label='Landmark',
            span=12,
            css_class='bottom-margin',
            max_length=100,
            required=False
        ))
        new_segment['data'].append(row)
        row = []
        pincode = Pincode.objects.filter(pincode=self.transaction.extra['pincode'])[0]
        state = preset.STATE_DICT[pincode.state.name]
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='SearchableDropDownField',
            code='city',
            label='City',
            options=preset.CITY_OPTIONS[state]
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='state',
            label='State',
            value=state,
            span=4,
            template_option='static'
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='pincode',
            label='Pincode',
            value=str(self.proposer_data['pincode']),
            span=4,
            template_option='static'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)

        tag_id = 'contact'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': {}, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='email',
            label='Email',
            span=12,
            css_class='bottom-margin',
            email=True
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MobileField',
            code='mobile',
            label='Mobile',
            css_class='bottom-margin',
            span=12
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='LandlineField',
            code='landline',
            label='Landline',
            span=12,
            css_class='bottom-margin',
            required=False
            ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template': 'stage_4', 'data': stage}

    def stage_5(self):
        stage = []
        # ---------- Question Unit Begins -----------
        question_code = 'qid_1'
        tag_id = 'qid_1'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': '''
            Does any person, proposed to be insured,
            within the last 2 years consulted a doctor or healthcare professional?
            ''',
            'trigger_any_yes_check': True,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        parent_question_code = question_code
        tag_id = 'symptoms'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': '''
            Please specify as accurately as possible the symptoms or the medical condition.
            Where applicable, (e.g. right leg, left eye)?
            ''',
            'trigger_any_yes_check': False,
            'show_header': False
        }

        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))

        parent_question_code = question_code
        tag_id = 'symptoms_date'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': 'When did the symptoms start and or when was the treatment completed?',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'DateField'))

        parent_question_code = question_code
        tag_id = 'treatment'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': '''
            What treatment did you receive and when
            (please include dates of treatment and any medication prescribed)?
            ''',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))

        parent_question_code = question_code
        tag_id = 'outcome_treatment'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': '''
            What was the outcome of the treatment
            (e.g. ongoing, complete recovery, recurrent or likely to recur)?
            ''',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))

        #---------- Question Uniot Begins -----------
        question_code = 'qid_2'
        tag_id = 'qid_2'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': '''
            Does any person, proposed to be insured, have been hospital for an operation and/investigation
            (eg. Scan, x-ray, biopsy, blood test)
            ''',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        parent_question_code = question_code
        tag_id = 'symptoms'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': '''
            Please specify as accurately as possible the symptoms or the medical condition.
            Where applicable, (e.g. right leg, left eye)?
            ''',
            'trigger_any_yes_check': False,
            'show_header': False
        }

        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))

        parent_question_code = question_code
        tag_id = 'symptoms_date'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': 'When did the symptoms start and or when was the treatment completed?',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'DateField'))

        parent_question_code = question_code
        tag_id = 'treatment'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': '''
            What treatment did you receive and when
            (please include dates of treatment and any medication prescribed)?
            ''',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))

        parent_question_code = question_code
        tag_id = 'outcome_treatment'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': '''
            What was the outcome of the treatment(e.g. ongoing, complete recovery, recurrent or likely to recur)?
            ''',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))

        # ---------- Question Unit Begins -----------
        question_code = 'qid_3'
        tag_id = 'qid_3'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Does any person, proposed to be insured, take tablets, medicines or drugs on regular basis?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        parent_question_code = question_code
        tag_id = 'symptoms'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': '''
            Please specify as accurately as possible the symptoms or the medical condition.
            Where applicable, (e.g. right leg, left eye)?
            ''',
            'trigger_any_yes_check': False,
            'show_header': False
        }

        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))

        parent_question_code = question_code
        tag_id = 'symptoms_date'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': 'When did the symptoms start and or when was the treatment completed?',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'DateField'))

        parent_question_code = question_code
        tag_id = 'treatment'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': '''
            What treatment did you receive and when (please include dates of treatment and any medication prescribed)?
            ''',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))

        parent_question_code = question_code
        tag_id = 'outcome_treatment'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': '''
            What was the outcome of the treatment (e.g. ongoing, complete recovery, recurrent or likely to recur)?
            ''',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))

        # ---------- Question Unit Begins -----------
        question_code = 'qid_4'
        tag_id = 'qid_4'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': '''
            Is any person, proposed to be insured, within the last three months,
            experienced any health problems or medical conditions which have not seen a doctor for?
            ''',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))
        parent_question_code = question_code
        tag_id = 'symptoms'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': '''
            Please specify as accurately as possible the symptoms or the medical condition.
            Where applicable, (e.g. right leg, left eye)?
            ''',
            'trigger_any_yes_check': False,
            'show_header': False
        }

        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))

        parent_question_code = question_code
        tag_id = 'symptoms_date'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': 'When did the symptoms start and or when was the treatment completed?',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'DateField'))

        parent_question_code = question_code
        tag_id = 'treatment'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': '''
            What treatment did you receive and when (please include dates of treatment and any medication prescribed)?
            ''',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))

        parent_question_code = question_code
        tag_id = 'outcome_treatment'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': '''
            What was the outcome of the treatment (e.g. ongoing, complete recovery, recurrent or likely to recur)?
            ''',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))

        # ---------- Question Unit Begins -----------
        question_code = 'qid_5'
        tag_id = 'qid_5'
        segment_id = 'lifestyle_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Does any person proposed to be insured consume Alcohol',
            'trigger_any_yes_check': True,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        parent_question_code = question_code
        tag_id = 'quantity'
        segment_id = 'lifestyle_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': 'Quantity pints',
            'trigger_any_yes_check': False,
            'show_header': False
        }

        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'IntegerField'))

        parent_question_code = question_code
        tag_id = 'no_of_years'
        segment_id = 'lifestyle_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': 'No. of Years',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'IntegerField'))

        #---------- Question Unit Begins -----------
        question_code = 'qid_6'
        tag_id = 'qid_6'
        segment_id = 'lifestyle_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Does any person proposed to be insured consume Smoking',
            'trigger_any_yes_check': True,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        parent_question_code = question_code
        tag_id = 'quantity'
        segment_id = 'lifestyle_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': 'Quantity No. of sticks',
            'trigger_any_yes_check': False,
            'show_header': False
        }

        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'IntegerField'))

        parent_question_code = question_code
        tag_id = 'no_of_years'
        segment_id = 'lifestyle_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': 'No. of Years',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'IntegerField'))

        # ---------- Question Unit Begins -----------
        question_code = 'qid_7'
        tag_id = 'qid_7'
        segment_id = 'lifestyle_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': '''
            Does any person proposed to be insured consume any other substance like Tobacco/ Guthka/ Pan/ Pan Masala, etc
            ''',
            'trigger_any_yes_check': True,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        parent_question_code = question_code
        tag_id = 'quantity'
        segment_id = 'lifestyle_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': 'Quantity No. of packets',
            'trigger_any_yes_check': False,
            'show_header': False
        }

        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'IntegerField'))

        parent_question_code = question_code
        tag_id = 'no_of_years'
        segment_id = 'lifestyle_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': 'No. of Years',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'IntegerField'))

        # ---------- Question Unit Begins -----------
        question_code = 'qid_8'
        tag_id = 'qid_8'
        segment_id = 'lifestyle_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Does any person proposed to be insured consume Narcotics',
            'trigger_any_yes_check': True,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        parent_question_code = question_code
        tag_id = 'quantity'
        segment_id = 'lifestyle_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': 'Quantity',
            'trigger_any_yes_check': False,
            'show_header': False
        }

        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'IntegerField'))

        parent_question_code = question_code
        tag_id = 'no_of_years'
        segment_id = 'lifestyle_question_%s' % question_code
        details = {
            'conditional': 'YES',
            'tier': 1,
            'parent_question_code': parent_question_code,
            'question_text': 'No. of Years',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'IntegerField'))

        return {'template': 'stage_5', 'data': stage}

    def stage_6(self):
        stage = []
        segment_id = 'terms'
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': {}, 'data': []}
        row = []
        t_and_c_label = ('I hereby confirm<br />'
                         '- I have read and agree to the Terms and Conditions of ')
        if self.transaction.slab.health_product.policy_wordings:
            t_and_c_label += (
                'the <a style="text-decoration:none;cursor:pointer;" '
                'onclick=\'window.open("%s","","width=400, height=400")\'>policy</a> and '
                % self.transaction.slab.health_product.policy_wordings.url
            )
        t_and_c_label += ('the <a style="cursor:pointer;" class="call_overlay">website</a>;<br />'
                          '- the declarations made herewith are true and correct;<br />'
                          '- appointment of Coverfox Insurance Brokers Pvt. Ltd. as my Insurance Broker, '
                          'and authorize them to represent me to Insurance Companies/Agencies for my Insurance needs.')
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='t_and_c',
            label=t_and_c_label,
            span=12,
            options=[
                {'id': 'Yes', 'name': 'Yes', 'is_correct': True},
                {'id': 'No', 'name': 'No', 'is_correct': False}
            ],
            css_class='bottom-margin',
            template_option='terms'
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='soft_copy',
            label='Do you wish only soft copy?',
            options=[
                {
                    'id': 'Yes',
                    'name': '''
                    I do not want the hard copy of the policy. I am a tree-lover and will help save paper by having soft copy only
                    ''',
                    'is_correct': True
                },
                {'id': 'No', 'name': 'No', 'is_correct': True}
            ],
            span=12,
            css_class='bottom-margin',
            template_option='terms'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template': 'stage_6', 'data': stage}

    def get_form_data_classic_individual(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
            self.stage_6()
        ]
        return data

    def get_form_data_classic_family(self):
        stage2 = self.stage_2()

        for segment in stage2['data']:
            try:
                saved_data = self.transaction.insured_details_json['form_data'][segment['segment']][segment['tag']]
            except KeyError:
                saved_data = {}
            row = segment['data'][1]
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DropDownField',
                label='Marital Status',
                code='marital_status',
                options=preset.MARITAL_STATUS_OPTIONS,
                span=2,
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DropDownField',
                label='Occupation',
                code='occupation',
                options=preset.OCCUPATION_OPTIONS,
                span=3,
            ))

        data = [
            self.stage_1(),
            stage2,
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
            self.stage_6()
        ]

        return data

    def get_form_data_prime_individual(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
            self.stage_6()
        ]
        return data

    def get_form_data_prime_family(self):
        # TODO: If status married, minimum age should be > 18
        stage2 = self.stage_2()
        for segment in stage2['data']:
            try:
                saved_data = self.transaction.insured_details_json['form_data'][segment['segment']][segment['tag']]
            except KeyError:
                saved_data = {}
            row = segment['data'][1]
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DropDownField',
                label='Marital Status',
                code='marital_status',
                options=preset.MARITAL_STATUS_OPTIONS,
                span=2,
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DropDownField',
                label='Occupation',
                code='occupation',
                options=preset.OCCUPATION_OPTIONS,
                span=3,
            ))

        data = [
            self.stage_1(),
            stage2,
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
            self.stage_6()
        ]

        return data

    def get_form_data_super_topup_individual(self):
        return self.get_form_data_prime_individual()

    def get_form_data_super_topup_family(self):
        return self.get_form_data_prime_family()


def get_dynamic_form(transaction):
    transaction, plan_data, proposer_data, is_self_present = custom_utils.standard_form_data_cleanup(transaction)
    # TOGGLES WHERE MEDICAL CASES ARE ALLOWED OR NOT
    plan_data['medical_firewall'] = False

    policy_code = 'classic_individual'
    if transaction.slab.health_product.id in [33]:
        policy_code = 'prime_family'
    elif transaction.slab.health_product.id in [23]:
        policy_code = 'prime_individual'
    elif transaction.slab.health_product.id in [25, 27, 29, 32]:
        policy_code = 'classic_family'
    elif transaction.slab.health_product.id in [22, 26, 28, 31]:
        policy_code = 'classic_individual'
    elif transaction.slab.health_product.id in [68]:
        policy_code = 'super_topup_family'
    elif transaction.slab.health_product.id in [69]:
        policy_code = 'super_topup_individual'

    obj = Formic(transaction, is_self_present)
    stages = getattr(obj, 'get_form_data_%s' % policy_code.lower(), '')()

    terms_and_conditions = "<div style='padding:20px 40px 70px 20px;'>"\
                           "<h3>Max Bupa Insurance: Terms of Use</h3>"\
                           "<p>I/We hereby declare, on my behalf and on behalf of all persons proposed to be insured,"\
                           "that the above statements, answers and/or particulars given</p>"\
                           "<p>by me are true and complete in all respects to the best of my knowledge and that "\
                           "I/We am/are authorized to propose on behalf of these other persons.</p>"\
                           "<p>I understand that the information provided by me will form the basis of the insurance policy, "\
                           "is subject to the Board approved underwriting policy of the insurance company and that the policy "\
                           "will come into force only after full receipt of the premium chargeable.</p>"\
                           "<p>I/We further declare that I/We will notify in writing any change occurring in the occupation "\
                           "or general health of the life to be insured/proposer after the proposal has been submitted but "\
                           "before communication of the risk acceptance by the company.</p>"\
                           "<p>I/We declare and consent to the company seeking medical information from any doctor or "\
                           "from a hospital who at anytime has attended</p>"\
                           "<p>on the life to be insured/proposer or from any past or present employer concerning "\
                           "anything which affects the physical or mental health</p>"\
                           "<p>of the life to be assured/proposer and seeking information from any insurance company to "\
                           "which an application for insurance on the life to be assured/proposer has been made for "\
                           "the purpose of underwriting the proposal and/or claim settlement.</p>"\
                           "<p>I/We authorize the company to share information pertaining to my proposal including "\
                           "the medical records for the sole purpose of</p>"\
                           "<p>proposal underwriting and/or claims settlement and with any Government and/or "\
                           "Regulatory authority</p>"\
                           "<h3>Coverfox: Terms and Conditions</h3>" \
                           "<p>Coverfox Insurance Broking Pvt. (hereinafter referred to as Coverfox) Limited operates " \
                           "the website <a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> to provide " \
                           "consumers choice and an easy way to buy Insurance " \
                           "products from multiple Insurance companies. Coverfox is not an insurance company. Coverfox " \
                           "is a licensed Insurance Broking Company holding a Broking licence from the Indian " \
                           "Insurance Regulator - Insurance Regulatory and Development authority. " \
                           "<a href='/irda-licence/' target='_blank'>(View IRDA License)</a></p>" \
                           "<p>Our participating insurance providers supply some of the information available on the " \
                           "Website and therefore there may be inaccuracies in the Website Information over which " \
                           "Coverfox has limited control.</p>" \
                           "<p>Coverfox does not warrant or guarantee the: Timeliness, accuracy or completeness of " \
                           "the Website Information; or Quality of the results obtained from the use of the Website.</p>" \
                           "<p>To the maximum extent permitted by law, Coverfox has no liability in relation to or " \
                           "arising out of the Website Information and Website recommendations. You are responsible " \
                           "for the final choice of your product and you should take time to read through all " \
                           "information supplied before proceeding. If you are in any doubt regarding a product or " \
                           "its terms you should seek further advice from Coverfox or the relevant participating " \
                           "provider before choosing your product.</p>" \
                           "<p>Coverfox may pass on your personal information to the relevant participating provider " \
                           "if you apply to purchase a product through " \
                           "<a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> , however, Coverfox does " \
                           "not guarantee when or if you will actually acquire the product that you have chosen. " \
                           "Coverfox does not accept any liability arising out of circumstances where there is delay " \
                           "in you acquiring the product you have chosen.</p>" \
                           "<p>Please note that Coverfox is only collecting or assisting in collecting  the premium " \
                           "deposit on behalf of the insurer you have chosen to buy the policy. The acceptance of " \
                           "the deposit as premium and final issuance of the policy is subject to the underwriting " \
                           "norms and discretion of the Insurer whose policy you have chosen to buy on which Coverfox " \
                           "has no control. Coverfox will ensure that the amount is refunded by the insurer in case " \
                           "there is no ultimate issuance of policy.</p>" \
                           "<p><button class='btn btn-lg cf-btn btn-red hide_overlay'>Close</button></p>" \
                           "</div>"

    policy_date_disclaimer = 'NOTE: The policy period will start immediately from the date this transaction gets processed'

    slate_list = [
        {
            'id': 'slate_1',
            'name': 'Proposer',
            'class': '',
            'description': 'The person who is paying for the policy'
        },
        {
            'id': 'slate_2',
            'name': 'Insured Members',
            'class': '',
            'description': 'People being insured under the policy'
        },
        {
            'id': 'slate_3',
            'name': 'Nominee',
            'class': '',
            'description': 'Nominees for the insured members'
        },
        {
            'id': 'slate_4',
            'name': 'Contact Info.',
            'class': '',
            'description': 'This address will be considered as permanent address, and will also be used for correspondence.'
        },
        {
            'id': 'slate_5',
            'name': 'Medical History',
            'class': '',
            'description': 'Medical History of the people being insured'
        },
        {
            'id': 'slate_6',
            'name': 'T & C',
            'class': 'last',
            'description': 'Mandatory terms and conditions',
            'style': " 'width', '0px' "
        }
    ]
    data = {
        'slate_list': slate_list,
        'proposer_data': proposer_data,
        'plan_data': plan_data,
        'stages': stages,
        'last_stage': stages[-1]['template'],
        'terms_and_conditions': terms_and_conditions,
        'policy_date_disclaimer': policy_date_disclaimer
    }

    return data

