from suds.client import Client
from suds.xsd.doctor import ImportDoctor, Import
from django.conf import settings
from django.core.mail import send_mail
from django.utils import timezone
from dateutil.relativedelta import relativedelta
import lxml.etree as ET
import logging
from suds.sax.text import Raw
from utils import request_logger
import copy
from datetime import datetime
import urllib
import uuid
from health_product.prod_utils import dict_to_etree, list_to_etree, complex_etree_to_dict, get_db_sequence
import httplib2
import json
from collections import OrderedDict
from Crypto.Cipher import DES
from health_product.insurer.max_bupa import preset
from wiki.models import Pincode
from health_product.insurer.max_bupa.form_data import get_premium_details

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.transport.http').setLevel(logging.DEBUG)

if settings.PRODUCTION:
    # ----------------UAT CREDENTIAL------------------------------------------
    PAYMENT_URL = 'http://pay.maxbupa.com/Pages/getPaymentValues.aspx?'
    ACCESS_KEY = 'cTJL9LWhnwzkVKVE8XrlonR9G2RClP6BCgJUJvs8CLdXsZ/'\
                 'xKhB0+sSsEWZwi7A4y0VgaH+QyuVxnzhb031tHD43QhY4O/5foyouEhiV1o+feswhM/'\
                 'cpHAQ/auK4CDeF52hrSRMlrwyjj8WgE32HXsHh7mvXoGCvzOdme64AgUliXngXY66jR29Kznj4n0sCce3aVxsbofKwHwwXLH8aJzN2eXHr+'\
                 'loBZeo4cFVZCsibFMqE0hQRfA7WIncWQPEnz1wp2LOfXwp1l+lLqTUA40DOH0lX4WZlAW4J3M3Y0ggu/j4meKaRIqWWJFqQA9SB'
    PROPOSAL_URL = 'http://182.74.138.40:8091/NgcomplexModified/services/NgcomplexService?wsdl'
    PREMIUM_URL = 'http://services.maxbupa.com/GetProductPrices/Getproductprices.asmx?WSDL'
    UW_DECISION_URL = 'http://servicesone.maxbupa.com/UWDecisionService/UWDecisionService.asmx?WSDL'
    PROPOSAL_OPTION = 'CREATE_SP_NB_TPD'
    PROPOSAL_USERNAME = 'system'
    PROPOSAL_PASSWORD = '9kbXBdSqUi7LH2BSXYnhfQ=='
    GET_PLAN_ID_URL = 'http://services.maxbupa.com/GetProductPrices/Getproductprices.asmx?WSDL'
    AGENT_CODE = 'BR02120002'
    MB_SALES_BRANCH = 'Delhi - Pusa Road I'
else:
    # ----------------UAT CREDENTIAL------------------------------------------
    PAYMENT_URL = 'http://paymbhid.maxbupa.com/Pages/getPaymentValues.aspx?'
    ACCESS_KEY = 'UseQlVYMBLMRfIVQoTGm8oboCDRYTlEqK58DvKTWbYdChxJppMvbc+'\
                 'uRDRzuL7Bar9c1Bo5v2M9pgQ4a81ifAsNT7yQOmgB2yLT9YtyddFx3g5nGt85InFIz4O7YMomapmLTlz//'\
                 'uAQzpooex1jm0eBNfi1o2D+TINCsrpXiI5i7BDUwGF6rZTOYqWY0L/'\
                 '2XbNGocRkzfV6SjdM8de9yKqiIVMCgxVuiwORIQ92ex3Nwej7EK97kVqVXGaj+fviPJ8EDn7FOE7FDo/'\
                 'CaGC4YJkfdnfo7grNWzXIWKuWT6wTocPv1GxoS2rIAw4IvU4hE'
    PROPOSAL_URL = 'http://182.74.138.19:8091/NgcomplexModified/services/NgcomplexService?wsdl'
    PREMIUM_URL = 'http://services.maxbupa.com/GetProductPricesUAT/GetProductPrices.asmx?WSDL'
    UW_DECISION_URL = 'http://220.226.187.22/UWDecisionService_UAT/UWDecisionService.asmx?WSDL'
    PROPOSAL_OPTION = 'CREATE_SP_NB_TPD'
    PROPOSAL_USERNAME = 'system'
    PROPOSAL_PASSWORD = '9kbXBdSqUi7LH2BSXYnhfQ=='
    GET_PLAN_ID_URL = 'http://services.maxbupa.com/GetProductPricesUAT/GetProductPrices.asmx?WSDL'
    AGENT_CODE = 'BR01340003'
    MB_SALES_BRANCH = 'Delhi - Pusa Road I'


QUESTION_DICT = {
    'YES': 1,
    'NO': 0,
    'yes': 1,
    'no': 0,
    'Yes': 1,
    'No': 0
}

SALUTATION_CHOICES = {
    1: 'Mr.',
    2: 'Ms.',
    3: 'Mrs.'
}

GENDER_CHOICES = {
    1: 'Male',
    2: 'Female'
}

RELATIONSHIP_CHOICES = {
    'self': 'Self',
    'spouse': 'Spouse',
    'father': 'Father',
    'mother': 'Mother',
    'son': 'Son',
    'son1': 'Son',
    'son2': 'Son',
    'son3': 'Son',
    'son4': 'Son',
    'daughter': 'Daughter',
    'daughter1': 'Daughter',
    'daughter2': 'Daughter',
    'daughter3': 'Daughter',
    'daughter4': 'Daughter',
}

CARD_TYPE_DICT = {
    200000: 1,
    300000: 1,
    400000: 1,
    500000: 2,
    750000: 2,
    1000000: 2,
    1250000: 2,
    1500000: 3,
    2000000: 3,
    3000000: 3,
    5000000: 3,
    10000000: 3
}


def get_product_code(transaction):
    sum_insured = transaction.extra['plan_data']['cover']
    number = sum_insured / 100000
    if number >= 1 and number <= 9:
        return 'IH0' + str(number)
    elif number >= 100 and number <= 999:
        return 'IH' + str(number/100) + 'C'
    else:
        return 'IH' + str(number)


def get_salut(transaction):
    members = transaction.extra['members']
    insured = transaction.insured_details_json['form_data']['insured']
    salut = {}
    for m in members:
        if m['gender'] == 1:
            salut[m['id']] = 'MR'
        else:
            try:
                married = True if insured[m['id']]['marital_status']['value'] == 'MARRIED' else False
            except Exception:
                married = False
            salut[m['id']] = 'MRS' if married else 'MISS'
    transaction.extra['insured_salut'] = salut
    transaction.save()
    return salut


def get_plan_id(transaction):
    '''
    Fetch plan id with respect to sum insured and premium
    '''
    imp = Import('http://www.w3.org/2001/XMLSchema', location='http://www.w3.org/2001/XMLSchema.xsd')
    doctor = ImportDoctor(imp)
    imp.filter.add('http://microsoft.com/wsdl/types/')

    client = Client(GET_PLAN_ID_URL, doctor=doctor)
    # method = client.factory.create('GetHCPlanIDV2')

    if transaction.slab.health_product.policy_type == 'INDIVIDUAL':
        plan_type = 'IIND'
    else:
        plan_type = 'IFFT'

    # derive adult and child
    adult_count = 0
    child_count = 0
    for member in transaction.extra['members']:
        if member['type'] == 'adult':
            adult_count = adult_count + 1
        else:
            child_count = child_count + 1

    # Derive Tier
    if transaction.slab.health_product.policy_type == 'FAMILY':
        cpt = transaction.slab.family_cover_premium_table.get(id=transaction.extra['cpt_id'])
    else:
        cpt = transaction.slab.cover_premium_table.get(id=transaction.extra['cpt_id'])

    if cpt.city.all() or cpt.state.all():
        tier = 'Nat'
    else:
        tier = 'Loc'

    sProductType = get_product_code(transaction)
    sPlanType = plan_type
    iCardType = CARD_TYPE_DICT[transaction.extra['plan_data']['cover']]
    iAdultCover = adult_count
    iChildrenCover = child_count
    iPlanLimit = transaction.extra['plan_data']['cover']
    iFloaterLimit = 0  # hard code
    iDeductAmount = 0  # hard code
    iHCB = 0  # hard code
    sTier = tier  # change later

    cp = client.service.GetHCPlanIDV2
    resp = cp(sProductType, sPlanType, iCardType, iAdultCover, iChildrenCover,
              iPlanLimit, iFloaterLimit, iDeductAmount, iHCB, sTier)
    request_logger.info(
        '\nMax Bupa TRANSACTION: %s \n GetHCPlanIDV2 REQUEST SENT:\n %s \n'
        % (transaction.transaction_id, client.last_sent())
    )
    request_logger.info(
        '\nMax Bupa TRANSACTION: %s \n GetHCPlanIDV2 RESPONSE RECIEVED:\n %s \n'
        % (transaction.transaction_id, client.last_received())
    )
    return resp.diffgram.dsDataSet.Table1.PLAN_ID


def get_premium(transaction):
    '''
    Fetch premium from maxbupa webservice
    '''
    imp = Import('http://www.w3.org/2001/XMLSchema', location='http://www.w3.org/2001/XMLSchema.xsd')
    doctor = ImportDoctor(imp)
    imp.filter.add('http://microsoft.com/wsdl/types/')

    client = Client(PREMIUM_URL, doctor=doctor)
    proposer_data = transaction.insured_details_json['form_data']['proposer']
    pincode = Pincode.objects.filter(pincode=transaction.extra['pincode'])[0]
    dob_list = []
    for member, data in transaction.insured_details_json['form_data']['insured'].items():
        dob_list.append(data['date_of_birth']['value'])

    plan_id = transaction.extra['plan_id']
    duration_type = proposer_data['personal']['policy_term']['value1']
    hcb_included = 78  # No HCB
    no_of_member = len(transaction.insured_details_json['form_data']['insured'])
    dob_collection = ','.join(dob_list)
    if transaction.proposer_gender == 1:
        gender = 77
    else:
        gender = 70
    state = transaction.extra['city']['name']
    city = pincode.state.name
    effective_date = datetime.now().strftime("%d/%m/%Y")
    disc_loading = 0  # default
    member_loading = 100  # default
    access_code = ACCESS_KEY

    cp = client.service.GetPremiumForHealthCompanionV2
    resp = cp(plan_id, duration_type, hcb_included, no_of_member, dob_collection, gender,
              state, city, effective_date, disc_loading, member_loading, access_code)

    request_logger.info(
        '\nMax Bupa TRANSACTION: %s \n GetPremiumForHealthCompanionV2 REQUEST SENT:\n %s \n'
        % (transaction.transaction_id, client.last_sent())
    )
    request_logger.info(
        '\nMax Bupa TRANSACTION: %s \n GetPremiumForHealthCompanionV2 RECIEVED:\n %s \n'
        % (transaction.transaction_id, client.last_received())
    )
    return resp.diffgram[0][0][0][0][0]['Net_Premium_WithTaxAndMemberLoading'][0]


def get_height_cm(feet, inch):
    return str((int(feet) * 30) + (int(inch) * 2.54))


def get_date_format(value):
    format1 = '%d/%m/%Y'
    format2 = '%d-%b-%Y'
    return datetime.strptime(value, format1).strftime(format2)


def get_medical_quetion_data(member, transaction, medical_question_obj, add_med_q_obj):
    add_med_q_objects = []
    for key, value in transaction.insured_details_json['form_data'].items():
        if 'medical_question_qid' in key:
            question_code = int(key.split('_')[-1])
            key_question_code = 'qid_' + key.split('_')[-1]
            if value[key_question_code][member + '_question_' + key_question_code]['display_value'] == 'Yes':
                medical_question_obj[question_code - 1].text = 'YES'
                add_med_q_obj_tmp = copy.deepcopy(add_med_q_obj)
                add_med_q_obj_tmp[0].text = str(question_code)  # QUESTION
                add_med_q_obj_tmp[1].text = value['symptoms'][member + '_question_symptoms']['display_value']  # SYMPTOMS
                # SYMPTOMS_DATE
                add_med_q_obj_tmp[2].text = value['symptoms_date'][member + '_question_symptoms_date']['display_value']
                add_med_q_obj_tmp[3].text = value['treatment'][member + '_question_treatment']['display_value']  # TREATMENT
                # OUTCOME_TREATMENT
                add_med_q_obj_tmp[4].text = value['outcome_treatment'][member + '_question_outcome_treatment']['display_value']
                add_med_q_objects.append(add_med_q_obj_tmp)
    return add_med_q_objects


def uw_decision(transaction):
    plan_id = get_plan_id(transaction)
    if not transaction.extra.get('app_id'):
        transaction.extra['app_id'] = get_db_sequence('seq_maxbupa_app_id')  # get_policy_num()
        transaction.reference_id = transaction.extra['app_id']  # str(transaction.extra['app_id']).replace('827', 'CS0')
        transaction.policy_token = transaction.extra['app_id']
    transaction.extra['plan_id'] = plan_id
    transaction.save()

    properser_data = transaction.insured_details_json['form_data']['proposer']
    plan_data = transaction.extra['plan_data']

    client = Client(UW_DECISION_URL)
    method = client.factory.create('GetUWDecision')

    tree = ET.parse('health_product/insurer/max_bupa/uw_request.xml')
    root = tree.getroot()

    # build xml
    root[0].text = str(transaction.extra['app_id'])  # CUST_PIVOTAL_APP_ID
    root[1].text = properser_data['personal']['nationality']['value']  # CUST_NATIONALITY
    root[2].text = str(plan_data['cover'])     # CUST_SUM_INSURED
    root[3].text = str(int(transaction.extra['plan_id']))  # CONT_PLAN_ID
    root[12].text = properser_data['address']['city']['value']   # CITY_NAME

    insured_detail_copy = copy.deepcopy(root[14][0])
    root[14].remove(root[14][0])
    add_med_q_obj = copy.deepcopy(insured_detail_copy[8])
    insured_detail_copy.remove(insured_detail_copy[8])
    for member, details in transaction.insured_details_json['form_data']['insured'].items():
        insured_detail = copy.deepcopy(insured_detail_copy)
        member_id = str(uuid.uuid1().int >> 64)  # member_id + 1 #uuid.uuid4().hex
        height_feet = details['height']['value1']
        height_inch = details['height']['value2']
        insured_detail[0].text = member_id  # <PIV_MBR_ID> confirm and genenrate
        insured_detail[3].text = get_date_format(details['date_of_birth']['value'])  # <MBR_DOB>
        insured_detail[4].text = get_height_cm(height_feet, height_inch)  # <MBR_HEIGHT>
        insured_detail[5].text = details['weight']['value']  # <MBR_WEIGHT>
        insured_detail[6].text = 'Indian'  # <MBR_NATIONALITY>
        medical_question_obj = insured_detail[7]
        add_med_q_objects = get_medical_quetion_data(member, transaction, medical_question_obj, add_med_q_obj)
        for obj in add_med_q_objects:
            insured_detail.append(obj)
        root[14].append(insured_detail)

    str_xml = '<![CDATA[' + ET.tostring(tree) + ']]>'

    method.strXml = Raw(str_xml)
    method.AccessKey = ACCESS_KEY

    cp = client.service.GetUWDecision
    resp = cp(method.strXml, method.AccessKey)
    res = ET.fromstring(resp)
    request_logger.info(
        '\nMax Bupa TRANSACTION: %s \nUW DECISION REQUEST SENT:\n %s \n'
        % (transaction.transaction_id, client.last_sent())
    )
    request_logger.info(
        '\nMax Bupa TRANSACTION: %s \nUW DECISION RESPONSE RECIEVED:\n %s \n'
        % (transaction.transaction_id, client.last_received())
    )
    if res[2].text in ['AC', 'UW']:
        transaction.extra['uw_decision'] = res[2].text
        transaction.save()
        return True
    else:
        return False


def get_proposal_xml(transaction):
    root = ET.Element('OnlineSalesDataSet')
    form_data = transaction.insured_details_json['form_data']
    proposer_data = transaction.insured_details_json['form_data']['proposer']
    nominee_data = transaction.insured_details_json['form_data']['nominee']
    data_dict_list = []

    for member, data in transaction.insured_details_json['form_data']['insured'].items():
        height_feet = data['height']['value1']
        height_inch = data['height']['value2']
        if transaction.proposer_gender == 2 and transaction.insured_details_json['proposer']['maritalStatus'] == 'MARRIED':
            salutation = SALUTATION_CHOICES[3]
        else:
            salutation = SALUTATION_CHOICES[int(transaction.proposer_gender)]
        ins_gender = int([mem['gender'] for mem in transaction.extra['members'] if mem['id'] == member][0])
        name = data['first_name']['display_value'] + " " + data['last_name']['display_value']

        # calculate BMI
        height_meter = float(get_height_cm(height_feet, height_inch)) / 100.0
        bmi = int(round(float(data['weight']['value']) / (height_meter * height_meter), 0))

        landline_no = proposer_data['contact']['landline']['std_code'] + proposer_data['contact']['landline']['landline']

        data_dict = {
            "OnlineSalesDataTable": OrderedDict([
                ("RowCount", "1"),
                ("CCS_APPLICATION_NUMBER", transaction.extra['app_id']),
                ("CCS_CONTACT_CODE", "Null"),
                ("MB_WEB_UW_COMPLETE_DATE", ""),
                ("CCS_PROPOSER_TITLE", salutation),
                ("FIRST_NAME", proposer_data['personal']['first_name']['value']),
                ("LAST_NAME", proposer_data['personal']['last_name']['value']),
                ("MIDDLE_NAME", proposer_data['personal']['middle_name']['value']),
                ("NATIONALITY", proposer_data['personal']['nationality']['value']),
                ("MARITAL_STATUS", proposer_data['personal']['marital_status']['value']),
                ("EMAIL_ID", proposer_data['contact']['email']['value']),
                ("PAN_NUMBER", ""),  # TODO
                ("MOBILE", proposer_data['contact']['mobile']['value']),
                ("LANDLINE_NUMBER", landline_no),
                ("EDUCATIONAL_QUALIFICATION", proposer_data['personal']['educational_qulification']['value']),
                ("OCCUPATION", proposer_data['personal']['occupation']['value']),
                ("CCS_CAMPAIGN_CODE", "827"),  # hard code fixed for coverfox
                ("CHANNEL", "TPD"),  # hard code
                ("CCS_INSURED_COUNT", len(transaction.insured_details_json['form_data']['insured'])),
                ("PREMIUM_AMOUNT", transaction.premium_paid),
                ("CURRENT_PERMANENT_ADDRESS", "0"),
                ("PERMANENT_ADDRESS", "1"),
                ("CURRENT_ADDRESS", "0"),
                ("PERMANENT_ADDRESS_LINE1", transaction.proposer_address1),
                ("PERMANENT_ADDRESS_LINE2", transaction.proposer_address2),
                ("PERMANENT_ADDRESS_LINE3", ""),
                ("PERMANENT_CITY", transaction.proposer_city),
                ("CCS_PERMANENT_DISTRICT", ""),
                ("PERMANENT_STATE", transaction.proposer_state),
                ("PERMANENT_PINCODE", transaction.proposer_pincode),
                ("CURRENT_ADDRESS_LINE1", transaction.proposer_address1),
                ("CURRENT_ADDRESS_LINE2", transaction.proposer_address2),
                ("CURRENT_ADDRESS_LINE3", ""),
                ("CURRENT_CITY", transaction.proposer_city),
                ("CCS_CURRENT_DISTRICT", ""),
                ("CURRENT_STATE", transaction.proposer_state),
                ("CURRENT_PINCODE", transaction.proposer_pincode),
                ("COMMUNICATION_ADDRESS_LINE1", transaction.proposer_address1),
                ("COMMUNICATION_ADDRESS_LINE2", transaction.proposer_address2),
                ("COMMUNICATION_ADDRESS_LINE3", ""),
                ("COMMUNICATION_CITY", transaction.proposer_city),
                ("CCS_COMMUNICATION_DISTRICT", ""),
                ("COMMUNICATION_STATE", transaction.proposer_state),
                ("COMMUNICATION_PINCODE", transaction.proposer_pincode),
                ("MB_RURAL_FLAG", "Null"),
                ("MB_GROSS_ANNUAL_INCOME", ""),
                ("LEAD_ID", "Null"),
                ("PROCESS_MASTER", ""),
                ("LEAD_STAGE", ""),
                ("LEAD_STATUS", ""),
                ("TELESALES_ASSIGNED_TO", "Null"),
                ("DST_ASSIGNED_TO", "Null"),
                ("CCS_FOLLOW_UP_DATE", "Null"),
                ("CCS_LOGGED", "Null"),
                ("CCS_LOGGED_DATE", "Null"),
                ("CCS_FOLLOW_UP_TIME", "Null"),
                ("CCS_TEAM", ""),
                ("CCS_SELF_EMP", ""),
                ("MEDICAL_TEST_NEEDED", "Null"),
                ("MEDICAL_SCHEDULED_DATE", "Null"),
                ("BANK_NAME_PD", ""),
                ("BANK_BRANCH", ""),
                ("BANK_CITY_PD", ""),
                ("ACCOUNT_NUMBER", ""),
                ("BRANCH", ""),
                ("ACCOUNT_TYPE", ""),
                ("MEDICAL_SCHEDULED_TIME_TXT", ""),
                ("CCS_POL_RNWD", ""),
                ("CCS_SMS_NOTIF", ""),
                ("CCS_SAL_DESG", ""),
                ("CCS_FAMILY_PH_NAME", ""),
                ("CCS_PH_ADDRESS_LINE_1", ""),
                ("CCS_PH_ADDRESS_LINE_2", ""),
                ("CCS_PH_ADDRESS_LINE_3", ""),
                ("CCS_PH_CITY", ""),
                ("CCS_PH_STATE", ""),
                ("CCS_PH_PINCODE", "0"),
                ("CCS_EXS_INSURED_DT", ""),
                ("F1", ""),
                ("BANK_BRANCH_NAME", "Online Payment"),
                ("F2", ""),
                ("CASH", "Null"),
                ("CHEQUE", "Null"),
                ("CHEQUE_NUMBER", transaction.payment_token),
                ("AMOUNT", transaction.premium_paid),
                ("CCS_PAYMENT_DATE", transaction.payment_on),
                ("CCS_CASH_CHEQUE", ""),
                ("NOMINEE_NAME", nominee_data['combo']['nominee_first_name']['value']),
                ("NOMINEE_RELATIONSHIP", nominee_data['combo']['relation']['value']),
                ("NOMINEE_ADDRESS", nominee_data['combo']['nominee_address']['value']),
                ("NOMINEE_CONTACT_NUMBER", nominee_data['combo']['nominee_contact_no']['value']),
                ("CCS_PLAN_ID", str(transaction.extra['plan_id'])),
                ("PAYMENT_RECEIVED_FLAG", "1"),
                ("CCS_APP_FLAG", "Null"),
                ("CCS_APP_FLAG_NUMERIC", "Null"),
                ("WEB_INSURED_ID", get_db_sequence('seq_maxbupa_web_id')),
                ("INSURED_TITLE", SALUTATION_CHOICES[ins_gender]),
                ("INSURED_FIRST_NAME", data['first_name']['value']),
                ("INSURED_LAST_NAME", data['last_name']['value']),
                ("INSURED_MIDDLE_NAME", data['middle_name']['value']),
                ("INSURED_GENDER", GENDER_CHOICES[ins_gender]),
                ("INSURED_HEIGHT", get_height_cm(height_feet, height_inch)),
                ("INSURED_WEIGHT", data['weight']['value']),
                ("INSURED_DATE_OF_BIRTH", data['date_of_birth']['value']),
                ("INSURED_WAISTLINE", data['waistline']['value']),
                ("INSURED_OCCUPATION", data['occupation']['value']),
                ("INSURED_RELATIONSHIP_PROPOSER", RELATIONSHIP_CHOICES[member]),
                ("INSURED_HEIGHT_INCH", height_inch),
                ("INSURED_HEIGHT_FEET", height_feet),
                ("EDUCATIONAL_QUALIFICATION_x0020__x0028_Insured_x0029_", data['educational_qulification']['value']),
                ("SOCIAL_SECTOR", ""),
                ("CCS_DESIGNATION_OCCUPATION", ""),
                ("CHECKUP", "0"),
                ("BMI", bmi),
                ("INSURED_Q1", QUESTION_DICT[form_data['medical_question_qid_1']['qid_1']
                                             [member + '_question_qid_1']['value']]),
                ("INSURED_Q2", QUESTION_DICT[form_data['medical_question_qid_2']['qid_2']
                                             [member + '_question_qid_2']['value']]),
                ("INSURED_Q3", QUESTION_DICT[form_data['medical_question_qid_3']['qid_3']
                                             [member + '_question_qid_3']['value']]),
                ("INSURED_Q4", QUESTION_DICT[form_data['medical_question_qid_4']['qid_4']
                                             [member + '_question_qid_4']['value']]),
                ("NAME", name),
                ("MEDICAL_QUESTION", "1"),
                ("SYMPTOMS", form_data['medical_question_qid_1']['symptoms'][member + '_question_symptoms']['value']),
                ("SYMPTOM_DATE", form_data['medical_question_qid_1']['symptoms_date']
                 [member + '_question_symptoms_date']['value']),
                ("TREATMENT", form_data['medical_question_qid_1']['treatment'][member + '_question_treatment']['value']),
                ("OUTCOME_TREATMENT", form_data['medical_question_qid_1']['outcome_treatment']
                 [member + '_question_outcome_treatment']['value']),
                ("MEDICAL_QUESTION2", "2"),
                ("SYMPTOMS2", form_data['medical_question_qid_2']['symptoms'][member + '_question_symptoms']['value']),
                ("SYMPTOM_DATE2", form_data['medical_question_qid_2']['symptoms_date']
                 [member + '_question_symptoms_date']['value']),
                ("TREATMENT2", form_data['medical_question_qid_2']['treatment'][member + '_question_treatment']['value']),
                ("OUTCOME_TREATMENT2", form_data['medical_question_qid_2']['outcome_treatment']
                 [member + '_question_outcome_treatment']['value']),
                ("MEDICAL_QUESTION3", "3"),
                ("SYMPTOMS3", form_data['medical_question_qid_3']['symptoms'][member + '_question_symptoms']['value']),
                ("SYMPTOM_DATE3", form_data['medical_question_qid_3']['symptoms_date']
                 [member + '_question_symptoms_date']['value']),
                ("TREATMENT3", form_data['medical_question_qid_3']['treatment'][member + '_question_treatment']['value']),
                ("OUTCOME_TREATMENT3", form_data['medical_question_qid_2']['outcome_treatment']
                 [member + '_question_outcome_treatment']['value']),
                ("MEDICAL_QUESTION4", "4"),
                ("SYMPTOMS4", form_data['medical_question_qid_4']['symptoms'][member + '_question_symptoms']['value']),
                ("SYMPTOM_DATE4", form_data['medical_question_qid_4']['symptoms_date']
                 [member + '_question_symptoms_date']['value']),
                ("TREATMENT4", form_data['medical_question_qid_4']['treatment'][member + '_question_treatment']['value']),
                ("OUTCOME_TREATMENT4", form_data['medical_question_qid_4']['outcome_treatment']
                 [member + '_question_outcome_treatment']['value']),
                ("MB_Lifestyle_Question_HC_1", form_data['lifestyle_question_qid_5']['qid_5']
                 [member + '_question_qid_5']['value']),
                ("MB_Lifestyle_Quantity_HC_1", form_data['lifestyle_question_qid_5']['quantity']
                 [member + '_question_quantity']['value']),
                ("MB_Lifestyle_Years_HC_1", form_data['lifestyle_question_qid_5']['no_of_years']
                 [member + '_question_no_of_years']['value']),
                ("MB_Lifestyle_Remarks_HC_1", ""),
                ("MB_Lifestyle_Question_HC_2", form_data['lifestyle_question_qid_6']['qid_6']
                 [member + '_question_qid_6']['value']),
                ("MB_Lifestyle_Quantity_HC_2", form_data['lifestyle_question_qid_6']['quantity']
                 [member + '_question_quantity']['value']),
                ("MB_Lifestyle_Years_HC_2", form_data['lifestyle_question_qid_6']['no_of_years']
                 [member + '_question_no_of_years']['value']),
                ("MB_Lifestyle_Remarks_HC_2", ""),
                ("MB_Lifestyle_Question_HC_3", form_data['lifestyle_question_qid_7']['qid_7']
                 [member + '_question_qid_7']['value']),
                ("MB_Lifestyle_Quantity_HC_3", form_data['lifestyle_question_qid_7']['quantity']
                 [member + '_question_quantity']['value']),
                ("MB_Lifestyle_Years_HC_3", form_data['lifestyle_question_qid_7']['no_of_years']
                 [member + '_question_no_of_years']['value']),
                ("MB_Lifestyle_Remarks_HC_3", ""),
                ("MB_Lifestyle_Question_HC_4", form_data['lifestyle_question_qid_8']
                 ['qid_8'][member + '_question_qid_8']['value']),
                ("MB_Lifestyle_Quantity_HC_4", form_data['lifestyle_question_qid_8']['quantity']
                 [member + '_question_quantity']['value']),
                ("MB_Lifestyle_Years_HC_4", form_data['lifestyle_question_qid_8']['no_of_years']
                 [member + '_question_no_of_years']['value']),
                ("MB_Lifestyle_Remarks_HC_4", ""),
                ("AGE_PROOF", ""),
                ("RESIDENCE_PROOF", ""),
                ("ID_PROOF", ""),
                ("ID_PROOF_COMMENTS", ""),
                ("RESIDENCE_PROOF_COMMENTS", ""),
                ("AGE_PROOF_COMMENTS", ""),
                ("CLAIM_DETAILS", ""),
                ("CCS_SUM_INSURED", ""),
                ("INSURED_FROM", ""),
                ("INSURED_TO", ""),
                ("Mb_Sales_Branch", MB_SALES_BRANCH),
                ("Agent_Assigned_To", AGENT_CODE),
                ("CCS_INSURED_FLAG", "Null"),
                ("CCS_INSURED_FLAG_NUMERIC", "Null"),
                ("Policy_Term", proposer_data['personal']['policy_term']['value1']),
                ("Bill_Cycle", proposer_data['personal']['policy_term']['value1']),
                ("Tier", "0"),  # 0 As not implementing Tier top up
                ("HCB", "0"),  # 0 As not implementing Hospital cash benefit topup
                ("mb_ecs_flag", "0"),
                ("mb_cost_sharing", "NO"),
                ("mb_email_id_notification", ""),
                ("mb_nationality", "Indian"),
                ("STP_Flag", 'True' if transaction.extra['uw_decision'] == 'AC' else 'False'),
                ("Policy_Number", ""),
                ("Transaction_Number", str(transaction.extra['app_id']).replace('827', 'CS0')),
                ("Cumulative_Amount", transaction.premium_paid),
                ("LOYALTY_OPTION", ""),
                ("ELDER_CUST_CORR_CADDR1", ""),
                ("ELDER_CUST_CORR_CADDR2", ""),
                ("ELDER_CUST_CORR_CADDR3", ""),
                ("ELDER_CUST_CORR_CPIN_CODE", ""),
                ("ELDER_CUST_CORR_CSTATE_CODE", ""),
                ("ELDER_CUST_CORR_CCITY_CODE", ""),
                ("ELDER_CUST_CORR_CDIST_CODE", ""),
                ("ELDER_CUST_CORR_ADDR1", ""),
                ("ELDER_CUST_CORR_ADDR2", ""),
                ("ELDER_CUST_CORR_ADDR3", ""),
                ("ELDER_CUST_CORR_PIN_CODE", ""),
                ("ELDER_CUST_CORR_STATE_CODE", ""),
                ("ELDER_CUST_CORR_CITY_CODE", ""),
                ("ELDER_CUST_CORR_DIST_CODE", ""),
                ("ELDER_CUST_OTHER_ADDR1", ""),
                ("ELDER_CUST_OTHER_ADDR2", ""),
                ("ELDER_CUST_OTHER_ADDR3", ""),
                ("ELDER_CUST_OTHER_PIN_CODE", ""),
                ("ELDER_CUST_OTHER_STATE_CODE", ""),
                ("ELDER_CUST_OTHER_CITY_CODE", ""),
                ("ELDER_CUST_OTHER_DIST_CODE", ""),
                ("ELDER_COMM_ADDRESS_IS", "")
            ])
        }
        data_dict_list.append(data_dict)
    xml_data = list_to_etree(root, data_dict_list)
    new_root = ET.Element('OnlineSalesDataSets')
    new_root.insert(0, xml_data)
    xml_str = ET.tostring(new_root, encoding='utf8', method='xml')
    return xml_str


def post_transaction(transaction):
    if uw_decision(transaction):
        insurer_slug = transaction.slab.health_product.insurer.slug
        redirect_url = '/health-plan/product/transaction/%s/failure/' % transaction.transaction_id
        return_url = '/health-plan/%s/transaction/%s/response/' % (insurer_slug, transaction.transaction_id)
        is_redirect = False
        unq_policy_no = str(transaction.extra['app_id']).replace('827', 'CS0')
        app_id = str(transaction.extra['app_id'])
        # premium = transaction.extra['plan_data']['premium']
        # proposer_data = transaction.insured_details_json['form_data']['proposer']
        # term = proposer_data['personal']['policy_term']['value1']
        # discounted_premium = round((premium * term) / 100 * (100 - preset.TERM_DISCOUNTS[term]))

        premium = get_premium(transaction)
        data = {
            'unqPolicyNumber': unq_policy_no,
            'premiumValue': premium if settings.PRODUCTION else 1,   # transaction.extra['plan_data']['premium'],
            'otherParam': 'OnlineSales01',
            'paymentType': 'mxbpofflinewithoutemi',
            'additionalComment': app_id + "," + str(premium),  # +str(premium)
            'returnPath': settings.SITE_URL + return_url,
        }
        redirect_url = PAYMENT_URL + urllib.urlencode(data)
        template_name = 'health_product/post_transaction_data.html'
        transaction.payment_request = data
        transaction.save()
        return is_redirect, redirect_url, template_name, data


def decrypt_response(payment_response):
    '''
    Decrypt the payment response
    '''
    secret_key = '!max#bupa@'
    iv = [0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF]
    iv_cleaned = "".join([chr(each) for each in iv])
    payment_response_dict = {}
    for key, response in payment_response.items():
        des_object = DES.new(secret_key[0:8], mode=DES.MODE_CBC, IV=iv_cleaned)
        response_cleaned = response.replace(" ", "+")
        try:
            response_decoded = response_cleaned.decode('base64')
            payment_response_dict[key] = des_object.decrypt(response_decoded).replace('\x08', '').replace('\x07', '')
        except Exception:
            print "Not able to decode the response for: ", key
            payment_response_dict[key] = response_cleaned
    return payment_response_dict


def post_payment(payment_response, transaction):
    """
    Payment response formate after decoding
    {u'bankNameDesc': '',
     u'paymentGateway': 'Billdesk',
     u'paymentTypeDesc': 'mxbpofflinewithoutemi-N/A',
     u'returnMessage': 'MAXBUPA|827000000008|DHDF3958018284|812122838|00000001.00|
                        HDF|NA|01|INR|DIRECT|NA|NA|NA|12-08-2015 12:27:48|0300|NA|OnlineSales0101|
                        mxbpofflinewithoutemi01|827000000008,3673.001|NA|NA|NA|NA|NA|NA|true',
     u'tempstring': '',
     u'uniqueSalesId': ''}

    """
    error_description_dict = {
        "?": "Response Unknown",
        "0": "Transaction Successfull",
        "1": "Transaction could not be processed",
        "2": "Transaction Declined - Contact Issuing Bank",
        "3": "Transaction Declined- No reply from Bank",
        "4": "Transaction Declined - Expired Card",
        "5": "Transaction Declined - Insufficient credit",
        "6": "Transaction Declined - Bank system error",
        "7": """Payment Server Processing Error - Typically caused  by invalid input \
        data such as an invalid credit card number. Processing errors can also occur.""",
        "9": "Bank Declined Transaction (Do not contact Bank)",
        "A": "Transaction Aborted",
        "B": """Transaction Blocked - Returned when the Verification Security Level has \
        a value of '07'. If the merchant has 3-D Secure Blocking enabled, the transaction will not proceed""",
        "C": "Transaction Cancelled",
        "D": "Deferred Transaction",
        "E": "Transaction Declined - Refer to card issuer",
        "F": "3D Secure Authentication Failed",
        "I": "Card Security Code Failed",
        "L": """Shopping Transaction Locked (This indicates that there is another transaction \
        taking place using the same shopping transaction number)""",
        "N": "Cardholder is not enrolled in 3D Secure (Authentication Only)",
        "P": "Transaction is Pending",
        "R": "Retry Limits Exceeded, Transaction Not Processed",
        "S": "Duplicate OrderInfo used. (This is only  relevant for Payment Servers that enforce the uniqueness of this field)",
        "T": "Address Verification Failed",
        "U": "Card Security Code Failed",
        "V": "Address Verification and Card Security Code Failed",
    }
    request_logger.info('\nMax Bupa TRANSACTION: %s \n PAYMENT RESPONSE RECEIVED BEFORE DECODE:\n \%s \n'
                        % (transaction.transaction_id, payment_response))
    payment_response_decoded = decrypt_response(payment_response)
    data = {}
    pay_res = payment_response_decoded['returnMessage'].split('|')

    if len(pay_res) < 10:
        response_data = {
            'MerchantID': "",
            'CustomerID': pay_res[0],
            'TxnReferenceNo': pay_res[1],
            'ErrorDescription': error_description_dict[pay_res[4]],
            'ErrorStatus': "",
            'TxnAmount': str(int(pay_res[5]) / 100)
        }
        if pay_res[4] == '0':
            response_data['AuthStatus'] = '0300'
        else:
            response_data['AuthStatus'] = '0000'
    else:
        response_data = {
            'MerchantID': pay_res[0],
            'CustomerID': pay_res[1],
            'TxnReferenceNo': pay_res[2],
            'BankReferenceNo': pay_res[3],
            'TxnAmount': pay_res[4],
            'BankID': pay_res[5],
            'BankMerchantID': pay_res[6],
            'TxnType': pay_res[7],
            'CurrencyName': pay_res[8],
            'ItemCode': pay_res[9],
            'SecurityType': pay_res[10],
            'SecurityID': pay_res[11],
            'SecurityPassword': pay_res[12],
            'TxnDate': pay_res[13],
            'AuthStatus': pay_res[14],
            'SettlementType': pay_res[15],
            'AdditionalInfo1': pay_res[16],
            'AdditionalInfo2': pay_res[17],
            'AdditionalInfo3': pay_res[18],
            'AdditionalInfo4': pay_res[19],
            'AdditionalInfo5': pay_res[20],
            'AdditionalInfo6': pay_res[21],
            'AdditionalInfo7': pay_res[22],
            'ErrorStatus': pay_res[23],
            'ErrorDescription': pay_res[24],
            'CheckSum': pay_res[25],
        }
    request_logger.info('\nMax Bupa TRANSACTION: %s \n PAYMENT RESPONSE RECEIVED:\n %s \n'
                        % (transaction.transaction_id, response_data))
    transaction.extra['payment_response'] = response_data
    transaction.payment_response = json.dumps(response_data)

    is_redirect = True
    redirect_url = '/health-plan/payment/transaction/%s/failure/' % transaction.transaction_id

    data = {}
    if transaction.extra['payment_response']['AuthStatus'] == '0300':
        transaction.policy_start_date = timezone.now() + relativedelta(days=1)
        policy_term = transaction.insured_details_json['form_data']['proposer']['personal']['policy_term']['value1']
        transaction.policy_end_date = timezone.now() + relativedelta(days=-1, years=policy_term)
        transaction.payment_token = response_data['TxnReferenceNo']
        transaction.premium_paid = str(float(transaction.extra['payment_response']['TxnAmount']))
        if True:
            if transaction.extra.get('proposal_resp_status') == 'success':
                redirect_url = '/health-plan/payment/transaction/%s/success/' % transaction.transaction_id
            else:
                transaction.payment_done = True
                transaction.payment_on = datetime.today()
                transaction.save()

                # proposal request and response
                data = get_proposal_xml(transaction)
                transaction.extra['proposal_data'] = data

                root_element = ET.Element('Envelope')
                root_element.set('xmlns', 'http://schemas.xmlsoap.org/soap/envelope/')
                data_dict = {
                    'Body': {
                        'ComplexReq': OrderedDict([
                            ('Option', PROPOSAL_OPTION),
                            ('Username', PROPOSAL_USERNAME),
                            ('Password', PROPOSAL_PASSWORD),
                            ('InputFile', data),
                            ('nextSeedValue', '')
                        ])
                    }
                }
                proposal_request = dict_to_etree(root_element, data_dict)
                proposal_request[0][0].set('xmlns', 'http://ngcomplexq.ops.newgen.com/schema/Ngcomplex/request')

                for tag in proposal_request[0][0].getchildren():
                    tag.set('xmlns', "")

                headers = {'Content-Type': 'text/xml', 'charset': "utf-8"}
                proposal_req_str = ET.tostring(proposal_request)

                http = httplib2.Http()
                response_header, policy_resp = http.request(PROPOSAL_URL, method='POST', body=proposal_req_str, headers=headers)

                request_logger.info('\nMax Bupa TRANSACTION: %s \n POLICY REQUEST SENT:\n %s \n'
                                    % (transaction.transaction_id, proposal_req_str))
                response_xml = ET.fromstring(policy_resp)
                cleaned_xml = ET.HTML(response_xml[0][0][0].text)[0][0]  # to avoid xmlsyntax error
                proposal_response = complex_etree_to_dict(cleaned_xml)
                transaction.extra['product_response'] = proposal_response

                request_logger.info('\nMax Bupa TRANSACTION: %s \n POLICY RESPONSE RECEIVED:\n %s \n'
                                    % (transaction.transaction_id, transaction.extra['product_response']))
                result = transaction.extra['product_response']['getngprocedureselectrecords']['getngprocedureselectrecord']['response']['result']

                if not result['status'] == 'Success':  # or not transaction.extra['product_response']['proposal']['proposalRef']:
                    transaction.extra['error_msg'] = result['errors']['error']
                    msg_text = 'Transaction ID: %s\n\nTransaction UID: %s\n\n' % (transaction.id, transaction.transaction_id)
                    msg_text = '%sProposal generation failed, post payment for Max Bupa' % msg_text
                    sub_text = 'ALERT: Proposal generation failed'
                    if settings.PRODUCTION:
                        send_mail(sub_text, msg_text, "Dev Bot <dev@cfstatic.org>",
                                  ['jatinderjit@coverfoxmail.com', 'suhas@coverfoxmail.com', 'noopur@coverfoxmail.com'])
                else:
                    transaction.extra['proposal_resp_status'] = 'success'
                    # transaction.policy_token = transaction.extra['product_response']['proposal']['proposalRef']
                    redirect_url = '/health-plan/payment/transaction/%s/success/' % transaction.transaction_id

        else:
            msg_text = 'Transaction ID: %s\n\nTransaction UID: %s\n\n' % (transaction.id, transaction.transaction_id)
            msg_text = '%sProposal generation failed, post payment for L&T' % msg_text
            # msg_text = '%s\nError : %s' % (msg_text, str(e))
            sub_text = 'ALERT: Proposal generation failed'
            if not settings.DEBUG:
                send_mail(sub_text, msg_text, "Dev Bot <dev@cfstatic.org>",
                          ['jatinderjit@coverfoxmail.com', 'suhas@coverfoxmail.com', 'noopur@coverfoxmail.com'])
            redirect_url = '/health-plan/payment/transaction/%s/success/' % transaction.transaction_id
    else:
        transaction.extra['error_msg'] = response_data['ErrorDescription']
    transaction.save()
    return is_redirect, redirect_url, '', {}
