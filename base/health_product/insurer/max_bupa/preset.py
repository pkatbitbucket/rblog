TERM_DISCOUNTS = {
    1: 0,
    2: 12.5,
}

MARITAL_STATUS_OPTIONS = [
    {'id': 'MARRIED', 'name': 'Married'},
    {'id': 'SINGLE', 'name': 'Single'}
]

OCCUPATION_OPTIONS = [
    {'id': 'hamals', 'name': 'hamals'},
    {'id': 'handicraft artisans', 'name': 'handicraft artisans'},
    {'id': 'handloom and khadi workers', 'name': 'handloom and khadi workers'},
    {'id': 'lady tailors', 'name': 'lady tailors'},
    {'id': 'leather and tannery workers', 'name': 'leather and tannery workers'},
    {'id': 'Salaried', 'name': 'Salaried'},
    {'id': 'Self employed', 'name': 'Self employed'},
    {'id': 'Student', 'name': 'Student'},
    {'id': 'House wife', 'name': 'House wife'},
    {'id': 'Others', 'name': 'Others'},
    {'id': 'agricultural labourers', 'name': 'agricultural labourers'},
    {'id': 'bidi workers', 'name': 'bidi workers'},
    {'id': 'brick kiln workers', 'name': 'brick kiln workers'},
    {'id': 'carpenters', 'name': 'carpenters'},
    {'id': 'cobblers', 'name': 'cobblers'},
    {'id': 'construction workers', 'name': 'construction workers'},
    {'id': 'fishermen', 'name': 'fishermen'},
    {'id': 'papad makers', 'name': 'papad makers'},
    {'id': 'powerloom workers', 'name': 'powerloom workers'},
    {'id': 'physically handicapped self-employed persons', 'name': 'physically handicapped self-employed persons'},
    {'id': 'primary milk producers', 'name': 'primary milk producers'},
    {'id': 'rickshaw pullers', 'name': 'rickshaw pullers'},
    {'id': 'safai karmacharis', 'name': 'safai karmacharis'},
    {'id': 'salt growers', 'name': 'salt growers'},
    {'id': 'seri culture workers', 'name': 'seri culture workers'},
    {'id': 'sugarcane cutters', 'name': 'sugarcane cutters'},
    {'id': 'tendu leaf collectors', 'name': 'tendu leaf collectors'},
    {'id': 'toddy tappers', 'name': 'toddy tappers'},
    {'id': 'vegetable vendors', 'name': 'vegetable vendors'},
    {'id': 'washerwomen', 'name': 'washerwomen'},
    {'id': 'working women in hills', 'name': 'working women in hills'},
    {'id': 'social sector', 'name': 'social sector'},
    {'id': 'HA - Type 1', 'name': 'HA - Type 1'},
    {'id': 'HA - Type 2', 'name': 'HA - Type 2'},
    {'id': 'HA - Type 3', 'name': 'HA - Type 3'},
    {'id': 'HA - Type 4', 'name': 'HA - Type 4'},
    {'id': 'HA - Type 5', 'name': 'HA - Type 5'},
    {'id': 'HA - Type 6', 'name': 'HA - Type 6'},
    {'id': 'HA - Type 7', 'name': 'HA - Type 7'},
    {'id': 'HA - Type 8', 'name': 'HA - Type 8'},
    {'id': 'HA - Type 9', 'name': 'HA - Type 9'},
    {'id': 'HA - Type 10', 'name': 'HA - Type 10'},
    {'id': 'HA - Type 11', 'name': 'HA - Type 11'},
    {'id': 'HA - Type 12', 'name': 'HA - Type 12'}
]
ANNUAL_INCOME_OPTIONS = [
    {'id': 'Between 2 - 5 Lacs', 'name': 'Between 2 - 5 Lacs'},
    {'id': 'Between 5 - 10 Lacs', 'name': 'Between 5 - 10 Lacs'},
    {'id': 'Between 10 - 20 Lacs', 'name': 'Between 10 - 20 Lacs'},
    {'id': '20 Lacs and above', 'name': '20 Lacs and above'},
]
relation_options = [
    {'id': 'Fiancee', 'name': 'Fiancee'},
    {'id': 'Spouse', 'name': 'Spouse'},
    {'id': 'Son', 'name': 'Son'},
    {'id': 'Daughter', 'name': 'Daughter'},
    {'id': 'Father', 'name': 'Father'},
    {'id': 'Mother', 'name': 'Mother'},
    {'id': 'Brother', 'name': 'Brother'},
    {'id': 'Sister', 'name': 'Sister'},
    {'id': 'Father-in-law', 'name': 'Father-in-law'},
    {'id': 'Mother-in-law', 'name': 'Mother-in-law'},
    {'id': 'Son-in-law', 'name': 'Son-in-law'},
    {'id': 'Uncle', 'name': 'Uncle'},
    {'id': 'Aunt', 'name': 'Aunt'},
    {'id': 'Daughter-in-law', 'name': 'Daughter-in-law'},
    {'id': 'Nephew', 'name': 'Nephew'},
    {'id': 'Niece', 'name': 'Niece'},
    {'id': 'Servant', 'name': 'Servant'},
    {'id': 'Driver', 'name': 'Driver'},
    {'id': 'Brother-in-Law', 'name': 'Brother-in-Law'},
    {'id': 'Sister-in-Law', 'name': 'Sister-in-Law'},
    {'id': 'Grandfather', 'name': 'Grandfather'},
    {'id': 'Grandmother', 'name': 'Grandmother'},
    {'id': 'Grandson', 'name': 'Grandson'},
    {'id': 'Granddaughter', 'name': 'Granddaughter'},
    {'id': 'Coparcener', 'name': 'Coparcener'},
    {'id': 'Karta', 'name': 'Karta'},
    {'id': 'Members of HUF', 'name': 'Members of HUF'},
    {'id': 'Self', 'name': 'Self'}
]

relation_options.sort(lambda x, y: -1 if x['name'] < y['name'] else 1)

EQ_OPTION = [
    {'id': 'Matric', 'name': 'Matric'},
    {'id': 'Non-Matric', 'name': 'Non-Matric'},
    {'id': 'Graduate', 'name': 'Graduate'},
    {'id': 'Post-Graduate', 'name': 'Post-Graduate'},
    {'id': 'Professional Course', 'name': 'Professional Course'},
]

STATE_DICT = {
    'Andaman and Nicobar Islands': 'Andaman and Nicobar Islands',
    'Andhra Pradesh': 'Andhra Pradesh',
    'Arunachal Pradesh': 'Arunachal Pradesh',
    'Assam': 'Assam',
    'Bihar': 'Bihar',
    'Chandigarh': 'Chandigarh',
    'Chhattisgarh': 'Chhattisgarh',
    'Dadra & Nagar Haveli': 'Dadra and Nagar Haveli',
    'Daman & Diu': 'Daman and Diu',
    'Delhi': 'Delhi',
    'Goa': 'Goa',
    'Gujarat': 'Gujarat',
    'Haryana': 'Haryana',
    'Himachal Pradesh': 'Himachal Pradesh',
    'Jammu & Kashmir': 'Jammu and Kashmir',
    'Jharkhand': 'Jharkhand',
    'Karnataka': 'Karnataka',
    'Kerala': 'Kerala',
    'Lakshadweep': 'Lakshadweep',
    'Madhya Pradesh': 'Madhya Pradesh',
    'Maharashtra': 'Maharashtra',
    'Manipur': 'Manipur',
    'Meghalaya': 'Meghalaya',
    'Mizoram': 'Mizoram',
    'Nagaland': 'Nagaland',
    'Orissa': 'Orissa',
    'Pondicherry': 'Puducherry',
    'Punjab': 'Punjab',
    'Rajasthan': 'Rajasthan',
    'Sikkim': 'Sikkim',
    'Tamilnadu': 'Tamil Nadu',
    'Telangana': 'Telangana',
    'Tripura': 'Tripura',
    'Uttar Pradesh': 'Uttar Pradesh',
    'Uttaranchal': 'Uttarakhand',
    'Uttarakhand': 'Uttarakhand',
    'West Bengal': 'West Bengal'
}


CITY_OPTIONS = {
    "Andaman and Nicobar Islands": [
        {
            "id": "SOUTH ANDAMAN",
            "name": "SOUTH ANDAMAN"
        },
        {
            "id": "NICOBAR",
            "name": "NICOBAR"
        },
        {
            "id": "NORTH AND MIDDLE ANDAMAN",
            "name": "NORTH AND MIDDLE ANDAMAN"
        }
    ],
    "Andhra Pradesh": [
        {
            "id": "ANANTHAPUR",
            "name": "ANANTHAPUR"
        },
        {
            "id": "CHITTOOR",
            "name": "CHITTOOR"
        },
        {
            "id": "CUDDAPAH",
            "name": "CUDDAPAH"
        },
        {
            "id": "EAST GODAVARI",
            "name": "EAST GODAVARI"
        },
        {
            "id": "GUNTUR",
            "name": "GUNTUR"
        },
        {
            "id": "KRISHNA",
            "name": "KRISHNA"
        },
        {
            "id": "KURNOOL",
            "name": "KURNOOL"
        },
        {
            "id": "NELLORE",
            "name": "NELLORE"
        },
        {
            "id": "PRAKASAM",
            "name": "PRAKASAM"
        },
        {
            "id": "SRIKAKULAM",
            "name": "SRIKAKULAM"
        },
        {
            "id": "VISAKHAPATNAM",
            "name": "VISAKHAPATNAM"
        },
        {
            "id": "VIZIANAGARAM",
            "name": "VIZIANAGARAM"
        },
        {
            "id": "WEST GODAVARI",
            "name": "WEST GODAVARI"
        }
    ],
    "Arunachal Pradesh": [
        {
            "id": "CHANGLANG",
            "name": "CHANGLANG"
        },
        {
            "id": "DIBANG VALLEY",
            "name": "DIBANG VALLEY"
        },
        {
            "id": "EAST KAMENG",
            "name": "EAST KAMENG"
        },
        {
            "id": "EAST SIANG",
            "name": "EAST SIANG"
        },
        {
            "id": "KURUNG KUMEY",
            "name": "KURUNG KUMEY"
        },
        {
            "id": "LOHIT",
            "name": "LOHIT"
        },
        {
            "id": "LOWER DIBANG VALLEY",
            "name": "LOWER DIBANG VALLEY"
        },
        {
            "id": "LOWER SUBANSIRI",
            "name": "LOWER SUBANSIRI"
        },
        {
            "id": "PAPUM PARE",
            "name": "PAPUM PARE"
        },
        {
            "id": "TAWANG",
            "name": "TAWANG"
        },
        {
            "id": "TIRAP",
            "name": "TIRAP"
        },
        {
            "id": "UPPER SIANG",
            "name": "UPPER SIANG"
        },
        {
            "id": "UPPER SUBANSIRI",
            "name": "UPPER SUBANSIRI"
        },
        {
            "id": "WEST KAMENG",
            "name": "WEST KAMENG"
        },
        {
            "id": "WEST SIANG",
            "name": "WEST SIANG"
        },
        {
            "id": "Itanagar",
            "name": "Itanagar"
        }
    ],
    "Assam": [
        {
            "id": "DIBRUGARH",
            "name": "DIBRUGARH"
        },
        {
            "id": "GOALPARA",
            "name": "GOALPARA"
        },
        {
            "id": "GOLAGHAT",
            "name": "GOLAGHAT"
        },
        {
            "id": "HAILAKANDI",
            "name": "HAILAKANDI"
        },
        {
            "id": "JORHAT",
            "name": "JORHAT"
        },
        {
            "id": "KAMRUP",
            "name": "KAMRUP"
        },
        {
            "id": "KARBI ANGLONG",
            "name": "KARBI ANGLONG"
        },
        {
            "id": "KARIMGANJ",
            "name": "KARIMGANJ"
        },
        {
            "id": "KOKRAJHAR",
            "name": "KOKRAJHAR"
        },
        {
            "id": "LAKHIMPUR",
            "name": "LAKHIMPUR"
        },
        {
            "id": "MARIGAON",
            "name": "MARIGAON"
        },
        {
            "id": "NAGAON",
            "name": "NAGAON"
        },
        {
            "id": "NALBARI",
            "name": "NALBARI"
        },
        {
            "id": "NORTH CACHAR HILLS",
            "name": "NORTH CACHAR HILLS"
        },
        {
            "id": "SIBSAGAR",
            "name": "SIBSAGAR"
        },
        {
            "id": "SONITPUR",
            "name": "SONITPUR"
        },
        {
            "id": "TINSUKIA",
            "name": "TINSUKIA"
        },
        {
            "id": "BARPETA",
            "name": "BARPETA"
        },
        {
            "id": "BONGAIGAON",
            "name": "BONGAIGAON"
        },
        {
            "id": "CACHAR",
            "name": "CACHAR"
        },
        {
            "id": "DARRANG",
            "name": "DARRANG"
        },
        {
            "id": "DHEMAJI",
            "name": "DHEMAJI"
        },
        {
            "id": "DHUBRI",
            "name": "DHUBRI"
        },
        {
            "id": "Dispur",
            "name": "Dispur"
        }
    ],
    "Bihar": [
        {
            "id": "ARARIA",
            "name": "ARARIA"
        },
        {
            "id": "AURANGABAD(BH)",
            "name": "AURANGABAD(BH)"
        },
        {
            "id": "BANKA",
            "name": "BANKA"
        },
        {
            "id": "BEGUSARAI",
            "name": "BEGUSARAI"
        },
        {
            "id": "BHAGALPUR",
            "name": "BHAGALPUR"
        },
        {
            "id": "BHOJPUR",
            "name": "BHOJPUR"
        },
        {
            "id": "BUXAR",
            "name": "BUXAR"
        },
        {
            "id": "DARBHANGA",
            "name": "DARBHANGA"
        },
        {
            "id": "EAST CHAMPARAN",
            "name": "EAST CHAMPARAN"
        },
        {
            "id": "GAYA",
            "name": "GAYA"
        },
        {
            "id": "GOPALGANJ",
            "name": "GOPALGANJ"
        },
        {
            "id": "JAMUI",
            "name": "JAMUI"
        },
        {
            "id": "JEHANABAD",
            "name": "JEHANABAD"
        },
        {
            "id": "KAIMUR (BHABUA)",
            "name": "KAIMUR (BHABUA)"
        },
        {
            "id": "KATIHAR",
            "name": "KATIHAR"
        },
        {
            "id": "KHAGARIA",
            "name": "KHAGARIA"
        },
        {
            "id": "KISHANGANJ",
            "name": "KISHANGANJ"
        },
        {
            "id": "LAKHISARAI",
            "name": "LAKHISARAI"
        },
        {
            "id": "MADHEPURA",
            "name": "MADHEPURA"
        },
        {
            "id": "MADHUBANI",
            "name": "MADHUBANI"
        },
        {
            "id": "MUNGER",
            "name": "MUNGER"
        },
        {
            "id": "MUZAFFARPUR",
            "name": "MUZAFFARPUR"
        },
        {
            "id": "NALANDA",
            "name": "NALANDA"
        },
        {
            "id": "NAWADA",
            "name": "NAWADA"
        },
        {
            "id": "PATNA",
            "name": "PATNA"
        },
        {
            "id": "PURNIA",
            "name": "PURNIA"
        },
        {
            "id": "ROHTAS",
            "name": "ROHTAS"
        },
        {
            "id": "SAHARSA",
            "name": "SAHARSA"
        },
        {
            "id": "SAMASTIPUR",
            "name": "SAMASTIPUR"
        },
        {
            "id": "SARAN",
            "name": "SARAN"
        },
        {
            "id": "SHEIKHPURA",
            "name": "SHEIKHPURA"
        },
        {
            "id": "SHEOHAR",
            "name": "SHEOHAR"
        },
        {
            "id": "SITAMARHI",
            "name": "SITAMARHI"
        },
        {
            "id": "SIWAN",
            "name": "SIWAN"
        },
        {
            "id": "SUPAUL",
            "name": "SUPAUL"
        },
        {
            "id": "VAISHALI",
            "name": "VAISHALI"
        },
        {
            "id": "WEST CHAMPARAN",
            "name": "WEST CHAMPARAN"
        }
    ],
    "Chandigarh": [
        {
            "id": "CHANDIGARH",
            "name": "CHANDIGARH"
        }
    ],
    "Chhattisgarh": [
        {
            "id": "BASTAR",
            "name": "BASTAR"
        },
        {
            "id": "BIJAPUR",
            "name": "BIJAPUR"
        },
        {
            "id": "BILASPUR",
            "name": "BILASPUR"
        },
        {
            "id": "DANTEWADA",
            "name": "DANTEWADA"
        },
        {
            "id": "DHAMTARI",
            "name": "DHAMTARI"
        },
        {
            "id": "DURG",
            "name": "DURG"
        },
        {
            "id": "JANJGIR-CHAMPA",
            "name": "JANJGIR-CHAMPA"
        },
        {
            "id": "JASHPUR",
            "name": "JASHPUR"
        },
        {
            "id": "KANKER",
            "name": "KANKER"
        },
        {
            "id": "KAWARDHA",
            "name": "KAWARDHA"
        },
        {
            "id": "KORBA",
            "name": "KORBA"
        },
        {
            "id": "KORIYA",
            "name": "KORIYA"
        },
        {
            "id": "MAHASAMUND",
            "name": "MAHASAMUND"
        },
        {
            "id": "NARAYANPUR",
            "name": "NARAYANPUR"
        },
        {
            "id": "RAIGARH",
            "name": "RAIGARH"
        },
        {
            "id": "RAIPUR",
            "name": "RAIPUR"
        },
        {
            "id": "RAJNANDGAON",
            "name": "RAJNANDGAON"
        },
        {
            "id": "SURGUJA",
            "name": "SURGUJA"
        }
    ],
    "Dadra and Nagar Haveli": [
        {
            "id": "DADRA & NAGAR HAVELI",
            "name": "DADRA & NAGAR HAVELI"
        }
    ],
    "Daman and Diu": [
        {
            "id": "DAMAN",
            "name": "DAMAN"
        },
        {
            "id": "DIU",
            "name": "DIU"
        }
    ],
    "Delhi": [
        {
            "id": "CENTRAL DELHI",
            "name": "CENTRAL DELHI"
        },
        {
            "id": "EAST DELHI",
            "name": "EAST DELHI"
        },
        {
            "id": "NORTH DELHI",
            "name": "NORTH DELHI"
        },
        {
            "id": "NORTH WEST DELHI",
            "name": "NORTH WEST DELHI"
        },
        {
            "id": "SOUTH DELHI",
            "name": "SOUTH DELHI"
        },
        {
            "id": "SOUTH WEST DELHI",
            "name": "SOUTH WEST DELHI"
        },
        {
            "id": "WEST DELHI",
            "name": "WEST DELHI"
        },
        {
            "id": "Delhi",
            "name": "Delhi"
        }
    ],
    "Goa": [
        {
            "id": "NORTH GOA",
            "name": "NORTH GOA"
        },
        {
            "id": "SOUTH GOA",
            "name": "SOUTH GOA"
        },
        {
            "id": "Panaji",
            "name": "Panaji"
        }
    ],
    "Gujarat": [
        {
            "id": "AHMEDABAD",
            "name": "AHMEDABAD"
        },
        {
            "id": "AMRELI",
            "name": "AMRELI"
        },
        {
            "id": "ANAND",
            "name": "ANAND"
        },
        {
            "id": "BANASKANTHA",
            "name": "BANASKANTHA"
        },
        {
            "id": "BHARUCH",
            "name": "BHARUCH"
        },
        {
            "id": "BHAVNAGAR",
            "name": "BHAVNAGAR"
        },
        {
            "id": "DAHOD",
            "name": "DAHOD"
        },
        {
            "id": "GANDHI NAGAR",
            "name": "GANDHI NAGAR"
        },
        {
            "id": "JAMNAGAR",
            "name": "JAMNAGAR"
        },
        {
            "id": "JUNAGADH",
            "name": "JUNAGADH"
        },
        {
            "id": "KACHCHH",
            "name": "KACHCHH"
        },
        {
            "id": "KHEDA",
            "name": "KHEDA"
        },
        {
            "id": "MAHESANA",
            "name": "MAHESANA"
        },
        {
            "id": "NARMADA",
            "name": "NARMADA"
        },
        {
            "id": "NAVSARI",
            "name": "NAVSARI"
        },
        {
            "id": "PANCH MAHALS",
            "name": "PANCH MAHALS"
        },
        {
            "id": "PATAN",
            "name": "PATAN"
        },
        {
            "id": "PORBANDAR",
            "name": "PORBANDAR"
        },
        {
            "id": "RAJKOT",
            "name": "RAJKOT"
        },
        {
            "id": "SABARKANTHA",
            "name": "SABARKANTHA"
        },
        {
            "id": "SURAT",
            "name": "SURAT"
        },
        {
            "id": "SURENDRA NAGAR",
            "name": "SURENDRA NAGAR"
        },
        {
            "id": "THE DANGS",
            "name": "THE DANGS"
        },
        {
            "id": "VADODARA",
            "name": "VADODARA"
        },
        {
            "id": "VALSAD",
            "name": "VALSAD"
        }
    ],
    "Haryana": [
        {
            "id": "REWARI",
            "name": "REWARI"
        },
        {
            "id": "ROHTAK",
            "name": "ROHTAK"
        },
        {
            "id": "SIRSA",
            "name": "SIRSA"
        },
        {
            "id": "SONIPAT",
            "name": "SONIPAT"
        },
        {
            "id": "YAMUNA NAGAR",
            "name": "YAMUNA NAGAR"
        },
        {
            "id": "AMBALA",
            "name": "AMBALA"
        },
        {
            "id": "BHIWANI",
            "name": "BHIWANI"
        },
        {
            "id": "FARIDABAD",
            "name": "FARIDABAD"
        },
        {
            "id": "FATEHABAD",
            "name": "FATEHABAD"
        },
        {
            "id": "GURGAON",
            "name": "GURGAON"
        },
        {
            "id": "HISAR",
            "name": "HISAR"
        },
        {
            "id": "JHAJJAR",
            "name": "JHAJJAR"
        },
        {
            "id": "JIND",
            "name": "JIND"
        },
        {
            "id": "KAITHAL",
            "name": "KAITHAL"
        },
        {
            "id": "KARNAL",
            "name": "KARNAL"
        },
        {
            "id": "KURUKSHETRA",
            "name": "KURUKSHETRA"
        },
        {
            "id": "MAHENDRAGARH",
            "name": "MAHENDRAGARH"
        },
        {
            "id": "PANCHKULA",
            "name": "PANCHKULA"
        },
        {
            "id": "PANIPAT",
            "name": "PANIPAT"
        }
    ],
    "Himachal Pradesh": [
        {
            "id": "BILASPUR (HP)",
            "name": "BILASPUR (HP)"
        },
        {
            "id": "CHAMBA",
            "name": "CHAMBA"
        },
        {
            "id": "HAMIRPUR(HP)",
            "name": "HAMIRPUR(HP)"
        },
        {
            "id": "KANGRA",
            "name": "KANGRA"
        },
        {
            "id": "KINNAUR",
            "name": "KINNAUR"
        },
        {
            "id": "KULLU",
            "name": "KULLU"
        },
        {
            "id": "LAHUL & SPITI",
            "name": "LAHUL & SPITI"
        },
        {
            "id": "MANDI",
            "name": "MANDI"
        },
        {
            "id": "SHIMLA",
            "name": "SHIMLA"
        },
        {
            "id": "SIRMAUR",
            "name": "SIRMAUR"
        },
        {
            "id": "SOLAN",
            "name": "SOLAN"
        },
        {
            "id": "UNA",
            "name": "UNA"
        }
    ],
    "Jammu and Kashmir": [
        {
            "id": "ANANTHNAG",
            "name": "ANANTHNAG"
        },
        {
            "id": "BARAMULLA",
            "name": "BARAMULLA"
        },
        {
            "id": "BUDGAM",
            "name": "BUDGAM"
        },
        {
            "id": "DODA",
            "name": "DODA"
        },
        {
            "id": "JAMMU",
            "name": "JAMMU"
        },
        {
            "id": "KARGIL",
            "name": "KARGIL"
        },
        {
            "id": "KATHUA",
            "name": "KATHUA"
        },
        {
            "id": "KUPWARA",
            "name": "KUPWARA"
        },
        {
            "id": "LEH",
            "name": "LEH"
        },
        {
            "id": "POONCH",
            "name": "POONCH"
        },
        {
            "id": "PULWAMA",
            "name": "PULWAMA"
        },
        {
            "id": "RAJAURI",
            "name": "RAJAURI"
        },
        {
            "id": "SRINAGAR",
            "name": "SRINAGAR"
        },
        {
            "id": "UDHAMPUR",
            "name": "UDHAMPUR"
        }
    ],
    "Jharkhand": [
        {
            "id": "BOKARO",
            "name": "BOKARO"
        },
        {
            "id": "CHATRA",
            "name": "CHATRA"
        },
        {
            "id": "DEOGHAR",
            "name": "DEOGHAR"
        },
        {
            "id": "DHANBAD",
            "name": "DHANBAD"
        },
        {
            "id": "DUMKA",
            "name": "DUMKA"
        },
        {
            "id": "EAST SINGHBHUM",
            "name": "EAST SINGHBHUM"
        },
        {
            "id": "GARHWA",
            "name": "GARHWA"
        },
        {
            "id": "GIRIDH",
            "name": "GIRIDH"
        },
        {
            "id": "GODDA",
            "name": "GODDA"
        },
        {
            "id": "GUMLA",
            "name": "GUMLA"
        },
        {
            "id": "HAZARIBAG",
            "name": "HAZARIBAG"
        },
        {
            "id": "JAMTARA",
            "name": "JAMTARA"
        },
        {
            "id": "KODERMA",
            "name": "KODERMA"
        },
        {
            "id": "LATEHAR",
            "name": "LATEHAR"
        },
        {
            "id": "LOHARDAGA",
            "name": "LOHARDAGA"
        },
        {
            "id": "PAKUR",
            "name": "PAKUR"
        },
        {
            "id": "PALAMAU",
            "name": "PALAMAU"
        },
        {
            "id": "RAMGARH",
            "name": "RAMGARH"
        },
        {
            "id": "RANCHI",
            "name": "RANCHI"
        },
        {
            "id": "SAHIBGANJ",
            "name": "SAHIBGANJ"
        },
        {
            "id": "SERAIKELA-KHARSAWAN",
            "name": "SERAIKELA-KHARSAWAN"
        },
        {
            "id": "SIMDEGA",
            "name": "SIMDEGA"
        },
        {
            "id": "WEST SINGHBHUM",
            "name": "WEST SINGHBHUM"
        }
    ],
    "Karnataka": [
        {
            "id": "GULBARGA",
            "name": "GULBARGA"
        },
        {
            "id": "HASSAN",
            "name": "HASSAN"
        },
        {
            "id": "HAVERI",
            "name": "HAVERI"
        },
        {
            "id": "KODAGU",
            "name": "KODAGU"
        },
        {
            "id": "KOLAR",
            "name": "KOLAR"
        },
        {
            "id": "KOPPAL",
            "name": "KOPPAL"
        },
        {
            "id": "MANDYA",
            "name": "MANDYA"
        },
        {
            "id": "MYSORE",
            "name": "MYSORE"
        },
        {
            "id": "RAICHUR",
            "name": "RAICHUR"
        },
        {
            "id": "RAMANAGAR",
            "name": "RAMANAGAR"
        },
        {
            "id": "SHIMOGA",
            "name": "SHIMOGA"
        },
        {
            "id": "TUMKUR",
            "name": "TUMKUR"
        },
        {
            "id": "UDUPI",
            "name": "UDUPI"
        },
        {
            "id": "UTTARA KANNADA",
            "name": "UTTARA KANNADA"
        },
        {
            "id": "BAGALKOT",
            "name": "BAGALKOT"
        },
        {
            "id": "BENGALURU",
            "name": "BENGALURU"
        },
        {
            "id": "BANGALORE RURAL",
            "name": "BANGALORE RURAL"
        },
        {
            "id": "BELGAUM",
            "name": "BELGAUM"
        },
        {
            "id": "BELLARY",
            "name": "BELLARY"
        },
        {
            "id": "BIDAR",
            "name": "BIDAR"
        },
        {
            "id": "BIJAPUR",
            "name": "BIJAPUR"
        },
        {
            "id": "CHAMRAJNAGAR",
            "name": "CHAMRAJNAGAR"
        },
        {
            "id": "CHICKMAGALUR",
            "name": "CHICKMAGALUR"
        },
        {
            "id": "CHIKKABALLAPUR",
            "name": "CHIKKABALLAPUR"
        },
        {
            "id": "CHITRADURGA",
            "name": "CHITRADURGA"
        },
        {
            "id": "DAKSHINA KANNADA",
            "name": "DAKSHINA KANNADA"
        },
        {
            "id": "DAVANGARE",
            "name": "DAVANGARE"
        },
        {
            "id": "DHARWARD",
            "name": "DHARWARD"
        },
        {
            "id": "GADAG",
            "name": "GADAG"
        }
    ],
    "Kerala": [
        {
            "id": "ALAPPUZHA",
            "name": "ALAPPUZHA"
        },
        {
            "id": "ERNAKULAM",
            "name": "ERNAKULAM"
        },
        {
            "id": "IDUKKI",
            "name": "IDUKKI"
        },
        {
            "id": "KANNUR",
            "name": "KANNUR"
        },
        {
            "id": "KASARGOD",
            "name": "KASARGOD"
        },
        {
            "id": "KOLLAM",
            "name": "KOLLAM"
        },
        {
            "id": "KOTTAYAM",
            "name": "KOTTAYAM"
        },
        {
            "id": "KOZHIKODE",
            "name": "KOZHIKODE"
        },
        {
            "id": "MALAPPURAM",
            "name": "MALAPPURAM"
        },
        {
            "id": "PALAKKAD",
            "name": "PALAKKAD"
        },
        {
            "id": "PATHANAMTHITTA",
            "name": "PATHANAMTHITTA"
        },
        {
            "id": "THIRUVANANTHAPURAM",
            "name": "THIRUVANANTHAPURAM"
        },
        {
            "id": "THRISSUR",
            "name": "THRISSUR"
        },
        {
            "id": "WAYANAD",
            "name": "WAYANAD"
        }
    ],
    "Lakshadweep": [
        {
            "id": "LAKSHADWEEP",
            "name": "LAKSHADWEEP"
        }
    ],
    "Madhya Pradesh": [
        {
            "id": "ANUPPUR",
            "name": "ANUPPUR"
        },
        {
            "id": "ASHOK NAGAR",
            "name": "ASHOK NAGAR"
        },
        {
            "id": "BALAGHAT",
            "name": "BALAGHAT"
        },
        {
            "id": "BARWANI",
            "name": "BARWANI"
        },
        {
            "id": "BETUL",
            "name": "BETUL"
        },
        {
            "id": "BHIND",
            "name": "BHIND"
        },
        {
            "id": "BHOPAL",
            "name": "BHOPAL"
        },
        {
            "id": "BURHANPUR",
            "name": "BURHANPUR"
        },
        {
            "id": "CHHATARPUR",
            "name": "CHHATARPUR"
        },
        {
            "id": "CHHINDWARA",
            "name": "CHHINDWARA"
        },
        {
            "id": "DAMOH",
            "name": "DAMOH"
        },
        {
            "id": "DATIA",
            "name": "DATIA"
        },
        {
            "id": "DEWAS",
            "name": "DEWAS"
        },
        {
            "id": "DHAR",
            "name": "DHAR"
        },
        {
            "id": "DINDORI",
            "name": "DINDORI"
        },
        {
            "id": "GUNA",
            "name": "GUNA"
        },
        {
            "id": "GWALIOR",
            "name": "GWALIOR"
        },
        {
            "id": "HARDA",
            "name": "HARDA"
        },
        {
            "id": "HOSHANGABAD",
            "name": "HOSHANGABAD"
        },
        {
            "id": "INDORE",
            "name": "INDORE"
        },
        {
            "id": "JABALPUR",
            "name": "JABALPUR"
        },
        {
            "id": "JHABUA",
            "name": "JHABUA"
        },
        {
            "id": "KATNI",
            "name": "KATNI"
        },
        {
            "id": "KHANDWA",
            "name": "KHANDWA"
        },
        {
            "id": "KHARGONE",
            "name": "KHARGONE"
        },
        {
            "id": "MANDLA",
            "name": "MANDLA"
        },
        {
            "id": "MANDSAUR",
            "name": "MANDSAUR"
        },
        {
            "id": "MORENA",
            "name": "MORENA"
        },
        {
            "id": "NARSINGHPUR",
            "name": "NARSINGHPUR"
        },
        {
            "id": "NEEMUCH",
            "name": "NEEMUCH"
        },
        {
            "id": "PANNA",
            "name": "PANNA"
        },
        {
            "id": "RAISEN",
            "name": "RAISEN"
        },
        {
            "id": "RAJGARH",
            "name": "RAJGARH"
        },
        {
            "id": "RATLAM",
            "name": "RATLAM"
        },
        {
            "id": "REWA",
            "name": "REWA"
        },
        {
            "id": "SAGAR",
            "name": "SAGAR"
        },
        {
            "id": "SATNA",
            "name": "SATNA"
        },
        {
            "id": "SEHORE",
            "name": "SEHORE"
        },
        {
            "id": "SEONI",
            "name": "SEONI"
        },
        {
            "id": "SHAHDOL",
            "name": "SHAHDOL"
        },
        {
            "id": "SHAJAPUR",
            "name": "SHAJAPUR"
        },
        {
            "id": "SHEOPUR",
            "name": "SHEOPUR"
        },
        {
            "id": "SHIVPURI",
            "name": "SHIVPURI"
        },
        {
            "id": "SIDHI",
            "name": "SIDHI"
        },
        {
            "id": "TIKAMGARH",
            "name": "TIKAMGARH"
        },
        {
            "id": "UJJAIN",
            "name": "UJJAIN"
        },
        {
            "id": "UMARIA",
            "name": "UMARIA"
        },
        {
            "id": "VIDISHA",
            "name": "VIDISHA"
        }
    ],
    "Maharashtra": [
        {
            "id": "YAVATMAL",
            "name": "YAVATMAL"
        },
        {
            "id": "AHMED NAGAR",
            "name": "AHMED NAGAR"
        },
        {
            "id": "AKOLA",
            "name": "AKOLA"
        },
        {
            "id": "AMRAVATI",
            "name": "AMRAVATI"
        },
        {
            "id": "AURANGABAD",
            "name": "AURANGABAD"
        },
        {
            "id": "BEED",
            "name": "BEED"
        },
        {
            "id": "BHANDARA",
            "name": "BHANDARA"
        },
        {
            "id": "BULDHANA",
            "name": "BULDHANA"
        },
        {
            "id": "CHANDRAPUR",
            "name": "CHANDRAPUR"
        },
        {
            "id": "DHULE",
            "name": "DHULE"
        },
        {
            "id": "GADCHIROLI",
            "name": "GADCHIROLI"
        },
        {
            "id": "GONDIA",
            "name": "GONDIA"
        },
        {
            "id": "HINGOLI",
            "name": "HINGOLI"
        },
        {
            "id": "JALGAON",
            "name": "JALGAON"
        },
        {
            "id": "JALNA",
            "name": "JALNA"
        },
        {
            "id": "KOLHAPUR",
            "name": "KOLHAPUR"
        },
        {
            "id": "LATUR",
            "name": "LATUR"
        },
        {
            "id": "MUMBAI",
            "name": "MUMBAI"
        },
        {
            "id": "NAGPUR",
            "name": "NAGPUR"
        },
        {
            "id": "NANDED",
            "name": "NANDED"
        },
        {
            "id": "NANDURBAR",
            "name": "NANDURBAR"
        },
        {
            "id": "NASHIK",
            "name": "NASHIK"
        },
        {
            "id": "OSMANABAD",
            "name": "OSMANABAD"
        },
        {
            "id": "PARBHANI",
            "name": "PARBHANI"
        },
        {
            "id": "PUNE",
            "name": "PUNE"
        },
        {
            "id": "RAIGARH(MH)",
            "name": "RAIGARH(MH)"
        },
        {
            "id": "RATNAGIRI",
            "name": "RATNAGIRI"
        },
        {
            "id": "SANGLI",
            "name": "SANGLI"
        },
        {
            "id": "SATARA",
            "name": "SATARA"
        },
        {
            "id": "SINDHUDURG",
            "name": "SINDHUDURG"
        },
        {
            "id": "SOLAPUR",
            "name": "SOLAPUR"
        },
        {
            "id": "THANE",
            "name": "THANE"
        },
        {
            "id": "WARDHA",
            "name": "WARDHA"
        },
        {
            "id": "WASHIM",
            "name": "WASHIM"
        }
    ],
    "Manipur": [
        {
            "id": "BISHNUPUR",
            "name": "BISHNUPUR"
        },
        {
            "id": "CHANDEL",
            "name": "CHANDEL"
        },
        {
            "id": "CHURACHANDPUR",
            "name": "CHURACHANDPUR"
        },
        {
            "id": "IMPHAL EAST",
            "name": "IMPHAL EAST"
        },
        {
            "id": "IMPHAL WEST",
            "name": "IMPHAL WEST"
        },
        {
            "id": "SENAPATI",
            "name": "SENAPATI"
        },
        {
            "id": "TAMENGLONG",
            "name": "TAMENGLONG"
        },
        {
            "id": "THOUBAL",
            "name": "THOUBAL"
        },
        {
            "id": "UKHRUL",
            "name": "UKHRUL"
        }
    ],
    "Meghalaya": [
        {
            "id": "EAST GARO HILLS",
            "name": "EAST GARO HILLS"
        },
        {
            "id": "EAST KHASI HILLS",
            "name": "EAST KHASI HILLS"
        },
        {
            "id": "JAINTIA HILLS",
            "name": "JAINTIA HILLS"
        },
        {
            "id": "RI BHOI",
            "name": "RI BHOI"
        },
        {
            "id": "SOUTH GARO HILLS",
            "name": "SOUTH GARO HILLS"
        },
        {
            "id": "WEST GARO HILLS",
            "name": "WEST GARO HILLS"
        },
        {
            "id": "WEST KHASI HILLS",
            "name": "WEST KHASI HILLS"
        },
        {
            "id": "Shillong",
            "name": "Shillong"
        }
    ],
    "Mizoram": [
        {
            "id": "AIZAWL",
            "name": "AIZAWL"
        },
        {
            "id": "CHAMPHAI",
            "name": "CHAMPHAI"
        },
        {
            "id": "KOLASIB",
            "name": "KOLASIB"
        },
        {
            "id": "LAWNGTLAI",
            "name": "LAWNGTLAI"
        },
        {
            "id": "LUNGLEI",
            "name": "LUNGLEI"
        },
        {
            "id": "MAMMIT",
            "name": "MAMMIT"
        },
        {
            "id": "SAIHA",
            "name": "SAIHA"
        },
        {
            "id": "SERCHHIP",
            "name": "SERCHHIP"
        }
    ],
    "Nagaland": [
        {
            "id": "DIMAPUR",
            "name": "DIMAPUR"
        },
        {
            "id": "KIPHIRE",
            "name": "KIPHIRE"
        },
        {
            "id": "KOHIMA",
            "name": "KOHIMA"
        },
        {
            "id": "LONGLENG",
            "name": "LONGLENG"
        },
        {
            "id": "MOKOKCHUNG",
            "name": "MOKOKCHUNG"
        },
        {
            "id": "MON",
            "name": "MON"
        },
        {
            "id": "PEREN",
            "name": "PEREN"
        },
        {
            "id": "PHEK",
            "name": "PHEK"
        },
        {
            "id": "TUENSANG",
            "name": "TUENSANG"
        },
        {
            "id": "WOKHA",
            "name": "WOKHA"
        },
        {
            "id": "ZUNHEBOTTO",
            "name": "ZUNHEBOTTO"
        }
    ],
    "Orissa": [
        {
            "id": "ANGUL",
            "name": "ANGUL"
        },
        {
            "id": "BALANGIR",
            "name": "BALANGIR"
        },
        {
            "id": "BALESWAR",
            "name": "BALESWAR"
        },
        {
            "id": "BARGARH",
            "name": "BARGARH"
        },
        {
            "id": "BHADRAK",
            "name": "BHADRAK"
        },
        {
            "id": "BOUDH",
            "name": "BOUDH"
        },
        {
            "id": "CUTTACK",
            "name": "CUTTACK"
        },
        {
            "id": "DEBAGARH",
            "name": "DEBAGARH"
        },
        {
            "id": "DHENKANAL",
            "name": "DHENKANAL"
        },
        {
            "id": "GAJAPATI",
            "name": "GAJAPATI"
        },
        {
            "id": "GANJAM",
            "name": "GANJAM"
        },
        {
            "id": "JAGATSINGHAPUR",
            "name": "JAGATSINGHAPUR"
        },
        {
            "id": "JAJAPUR",
            "name": "JAJAPUR"
        },
        {
            "id": "JHARSUGUDA",
            "name": "JHARSUGUDA"
        },
        {
            "id": "KALAHANDI",
            "name": "KALAHANDI"
        },
        {
            "id": "KANDHAMAL",
            "name": "KANDHAMAL"
        },
        {
            "id": "KENDRAPARA",
            "name": "KENDRAPARA"
        },
        {
            "id": "KENDUJHAR",
            "name": "KENDUJHAR"
        },
        {
            "id": "KHORDA",
            "name": "KHORDA"
        },
        {
            "id": "KORAPUT",
            "name": "KORAPUT"
        },
        {
            "id": "MALKANGIRI",
            "name": "MALKANGIRI"
        },
        {
            "id": "MAYURBHANJ",
            "name": "MAYURBHANJ"
        },
        {
            "id": "NABARANGAPUR",
            "name": "NABARANGAPUR"
        },
        {
            "id": "NAYAGARH",
            "name": "NAYAGARH"
        },
        {
            "id": "NUAPADA",
            "name": "NUAPADA"
        },
        {
            "id": "PURI",
            "name": "PURI"
        },
        {
            "id": "RAYAGADA",
            "name": "RAYAGADA"
        },
        {
            "id": "SAMBALPUR",
            "name": "SAMBALPUR"
        },
        {
            "id": "SONAPUR",
            "name": "SONAPUR"
        },
        {
            "id": "SUNDERGARH",
            "name": "SUNDERGARH"
        },
        {
            "id": "Bhubaneshwar",
            "name": "Bhubaneshwar"
        }
    ],
    "Puducherry": [
        {
            "id": "KARAIKAL",
            "name": "KARAIKAL"
        },
        {
            "id": "MAHE",
            "name": "MAHE"
        },
        {
            "id": "PONDICHERRY",
            "name": "PONDICHERRY"
        }
    ],
    "Punjab": [
        {
            "id": "AMRITSAR",
            "name": "AMRITSAR"
        },
        {
            "id": "BARNALA",
            "name": "BARNALA"
        },
        {
            "id": "BATHINDA",
            "name": "BATHINDA"
        },
        {
            "id": "FARIDKOT",
            "name": "FARIDKOT"
        },
        {
            "id": "FATEHGARH SAHIB",
            "name": "FATEHGARH SAHIB"
        },
        {
            "id": "FIROZPUR",
            "name": "FIROZPUR"
        },
        {
            "id": "GURDASPUR",
            "name": "GURDASPUR"
        },
        {
            "id": "HOSHIARPUR",
            "name": "HOSHIARPUR"
        },
        {
            "id": "JALANDHAR",
            "name": "JALANDHAR"
        },
        {
            "id": "KAPURTHALA",
            "name": "KAPURTHALA"
        },
        {
            "id": "LUDHIANA",
            "name": "LUDHIANA"
        },
        {
            "id": "MANSA",
            "name": "MANSA"
        },
        {
            "id": "MOGA",
            "name": "MOGA"
        },
        {
            "id": "MOHALI",
            "name": "MOHALI"
        },
        {
            "id": "MUKTSAR",
            "name": "MUKTSAR"
        },
        {
            "id": "NAWANSHAHR",
            "name": "NAWANSHAHR"
        },
        {
            "id": "PATIALA",
            "name": "PATIALA"
        },
        {
            "id": "ROPAR",
            "name": "ROPAR"
        },
        {
            "id": "SANGRUR",
            "name": "SANGRUR"
        }
    ],
    "Rajasthan": [
        {
            "id": "SIROHI",
            "name": "SIROHI"
        },
        {
            "id": "TONK",
            "name": "TONK"
        },
        {
            "id": "UDAIPUR",
            "name": "UDAIPUR"
        },
        {
            "id": "AJMER",
            "name": "AJMER"
        },
        {
            "id": "ALWAR",
            "name": "ALWAR"
        },
        {
            "id": "BANSWARA",
            "name": "BANSWARA"
        },
        {
            "id": "BARAN",
            "name": "BARAN"
        },
        {
            "id": "BARMER",
            "name": "BARMER"
        },
        {
            "id": "BHARATPUR",
            "name": "BHARATPUR"
        },
        {
            "id": "BHILWARA",
            "name": "BHILWARA"
        },
        {
            "id": "BIKANER",
            "name": "BIKANER"
        },
        {
            "id": "BUNDI",
            "name": "BUNDI"
        },
        {
            "id": "CHITTORGARH",
            "name": "CHITTORGARH"
        },
        {
            "id": "CHURU",
            "name": "CHURU"
        },
        {
            "id": "DAUSA",
            "name": "DAUSA"
        },
        {
            "id": "DHOLPUR",
            "name": "DHOLPUR"
        },
        {
            "id": "DUNGARPUR",
            "name": "DUNGARPUR"
        },
        {
            "id": "GANGANAGAR",
            "name": "GANGANAGAR"
        },
        {
            "id": "HANUMANGARH",
            "name": "HANUMANGARH"
        },
        {
            "id": "JAIPUR",
            "name": "JAIPUR"
        },
        {
            "id": "JAISALMER",
            "name": "JAISALMER"
        },
        {
            "id": "JALOR",
            "name": "JALOR"
        },
        {
            "id": "JHALAWAR",
            "name": "JHALAWAR"
        },
        {
            "id": "JHUJHUNU",
            "name": "JHUJHUNU"
        },
        {
            "id": "JODHPUR",
            "name": "JODHPUR"
        },
        {
            "id": "KARAULI",
            "name": "KARAULI"
        },
        {
            "id": "KOTA",
            "name": "KOTA"
        },
        {
            "id": "NAGAUR",
            "name": "NAGAUR"
        },
        {
            "id": "PALI",
            "name": "PALI"
        },
        {
            "id": "RAJSAMAND",
            "name": "RAJSAMAND"
        },
        {
            "id": "SAWAI MADHOPUR",
            "name": "SAWAI MADHOPUR"
        },
        {
            "id": "SIKAR",
            "name": "SIKAR"
        }
    ],
    "Sikkim": [
        {
            "id": "EAST SIKKIM",
            "name": "EAST SIKKIM"
        },
        {
            "id": "NORTH SIKKIM",
            "name": "NORTH SIKKIM"
        },
        {
            "id": "SOUTH SIKKIM",
            "name": "SOUTH SIKKIM"
        },
        {
            "id": "WEST SIKKIM",
            "name": "WEST SIKKIM"
        },
        {
            "id": "Gangtok",
            "name": "Gangtok"
        }
    ],
    "Tamil Nadu": [
        {
            "id": "ARIYALUR",
            "name": "ARIYALUR"
        },
        {
            "id": "CHENNAI",
            "name": "CHENNAI"
        },
        {
            "id": "COIMBATORE",
            "name": "COIMBATORE"
        },
        {
            "id": "CUDDALORE",
            "name": "CUDDALORE"
        },
        {
            "id": "DHARMAPURI",
            "name": "DHARMAPURI"
        },
        {
            "id": "DINDIGUL",
            "name": "DINDIGUL"
        },
        {
            "id": "ERODE",
            "name": "ERODE"
        },
        {
            "id": "KANCHIPURAM",
            "name": "KANCHIPURAM"
        },
        {
            "id": "KANYAKUMARI",
            "name": "KANYAKUMARI"
        },
        {
            "id": "KARUR",
            "name": "KARUR"
        },
        {
            "id": "KRISHNAGIRI",
            "name": "KRISHNAGIRI"
        },
        {
            "id": "MADURAI",
            "name": "MADURAI"
        },
        {
            "id": "NAGAPATTINAM",
            "name": "NAGAPATTINAM"
        },
        {
            "id": "NAMAKKAL",
            "name": "NAMAKKAL"
        },
        {
            "id": "NILGIRIS",
            "name": "NILGIRIS"
        },
        {
            "id": "PERAMBALUR",
            "name": "PERAMBALUR"
        },
        {
            "id": "PUDUKKOTTAI",
            "name": "PUDUKKOTTAI"
        },
        {
            "id": "RAMANATHAPURAM",
            "name": "RAMANATHAPURAM"
        },
        {
            "id": "SALEM",
            "name": "SALEM"
        },
        {
            "id": "SIVAGANGA",
            "name": "SIVAGANGA"
        },
        {
            "id": "THANJAVUR",
            "name": "THANJAVUR"
        },
        {
            "id": "THENI",
            "name": "THENI"
        },
        {
            "id": "TIRUCHIRAPPALLI",
            "name": "TIRUCHIRAPPALLI"
        },
        {
            "id": "TIRUNELVELI",
            "name": "TIRUNELVELI"
        },
        {
            "id": "TIRUVALLUR",
            "name": "TIRUVALLUR"
        },
        {
            "id": "TIRUVANNAMALAI",
            "name": "TIRUVANNAMALAI"
        },
        {
            "id": "TIRUVARUR",
            "name": "TIRUVARUR"
        },
        {
            "id": "TUTICORIN",
            "name": "TUTICORIN"
        },
        {
            "id": "VELLORE",
            "name": "VELLORE"
        },
        {
            "id": "VILLUPURAM",
            "name": "VILLUPURAM"
        },
        {
            "id": "VIRUDHUNAGAR",
            "name": "VIRUDHUNAGAR"
        }
    ],
    "Telangana": [
        {
            "id": "ADILABAD",
            "name": "ADILABAD"
        },
        {
            "id": "HYDERABAD",
            "name": "HYDERABAD"
        },
        {
            "id": "K.V.RANGAREDDY",
            "name": "K.V.RANGAREDDY"
        },
        {
            "id": "KARIM NAGAR",
            "name": "KARIM NAGAR"
        },
        {
            "id": "KHAMMAM",
            "name": "KHAMMAM"
        },
        {
            "id": "MAHABUB NAGAR",
            "name": "MAHABUB NAGAR"
        },
        {
            "id": "MEDAK",
            "name": "MEDAK"
        },
        {
            "id": "NALGONDA",
            "name": "NALGONDA"
        },
        {
            "id": "NIZAMABAD",
            "name": "NIZAMABAD"
        },
        {
            "id": "WARANGAL",
            "name": "WARANGAL"
        }
    ],
    "Tripura": [
        {
            "id": "DHALAI",
            "name": "DHALAI"
        },
        {
            "id": "NORTH TRIPURA",
            "name": "NORTH TRIPURA"
        },
        {
            "id": "SOUTH TRIPURA",
            "name": "SOUTH TRIPURA"
        },
        {
            "id": "WEST TRIPURA",
            "name": "WEST TRIPURA"
        },
        {
            "id": "Agartala",
            "name": "Agartala"
        }
    ],
    "Uttar Pradesh": [
        {
            "id": "JHANSI",
            "name": "JHANSI"
        },
        {
            "id": "JYOTIBA PHULE NAGAR",
            "name": "JYOTIBA PHULE NAGAR"
        },
        {
            "id": "KANNAUJ",
            "name": "KANNAUJ"
        },
        {
            "id": "KANPUR DEHAT",
            "name": "KANPUR DEHAT"
        },
        {
            "id": "KANPUR NAGAR",
            "name": "KANPUR NAGAR"
        },
        {
            "id": "KAUSHAMBI",
            "name": "KAUSHAMBI"
        },
        {
            "id": "KHERI",
            "name": "KHERI"
        },
        {
            "id": "KUSHINAGAR",
            "name": "KUSHINAGAR"
        },
        {
            "id": "LALITPUR",
            "name": "LALITPUR"
        },
        {
            "id": "LUCKNOW",
            "name": "LUCKNOW"
        },
        {
            "id": "MAHARAJGANJ",
            "name": "MAHARAJGANJ"
        },
        {
            "id": "MAHOBA",
            "name": "MAHOBA"
        },
        {
            "id": "MAINPURI",
            "name": "MAINPURI"
        },
        {
            "id": "MATHURA",
            "name": "MATHURA"
        },
        {
            "id": "MAU",
            "name": "MAU"
        },
        {
            "id": "MEERUT",
            "name": "MEERUT"
        },
        {
            "id": "MIRZAPUR",
            "name": "MIRZAPUR"
        },
        {
            "id": "MORADABAD",
            "name": "MORADABAD"
        },
        {
            "id": "MUZAFFARNAGAR",
            "name": "MUZAFFARNAGAR"
        },
        {
            "id": "PILIBHIT",
            "name": "PILIBHIT"
        },
        {
            "id": "PRATAPGARH",
            "name": "PRATAPGARH"
        },
        {
            "id": "RAEBARELI",
            "name": "RAEBARELI"
        },
        {
            "id": "RAMPUR",
            "name": "RAMPUR"
        },
        {
            "id": "SAHARANPUR",
            "name": "SAHARANPUR"
        },
        {
            "id": "SANT KABIR NAGAR",
            "name": "SANT KABIR NAGAR"
        },
        {
            "id": "SANT RAVIDAS NAGAR",
            "name": "SANT RAVIDAS NAGAR"
        },
        {
            "id": "SHAHJAHANPUR",
            "name": "SHAHJAHANPUR"
        },
        {
            "id": "SHRAWASTI",
            "name": "SHRAWASTI"
        },
        {
            "id": "SIDDHARTHNAGAR",
            "name": "SIDDHARTHNAGAR"
        },
        {
            "id": "SITAPUR",
            "name": "SITAPUR"
        },
        {
            "id": "SONBHADRA",
            "name": "SONBHADRA"
        },
        {
            "id": "SULTANPUR",
            "name": "SULTANPUR"
        },
        {
            "id": "UNNAO",
            "name": "UNNAO"
        },
        {
            "id": "VARANASI",
            "name": "VARANASI"
        },
        {
            "id": "AGRA",
            "name": "AGRA"
        },
        {
            "id": "ALIGARH",
            "name": "ALIGARH"
        },
        {
            "id": "ALLAHABAD",
            "name": "ALLAHABAD"
        },
        {
            "id": "AMBEDKAR NAGAR",
            "name": "AMBEDKAR NAGAR"
        },
        {
            "id": "AURAIYA",
            "name": "AURAIYA"
        },
        {
            "id": "AZAMGARH",
            "name": "AZAMGARH"
        },
        {
            "id": "BAGPAT",
            "name": "BAGPAT"
        },
        {
            "id": "BAHRAICH",
            "name": "BAHRAICH"
        },
        {
            "id": "BALLIA",
            "name": "BALLIA"
        },
        {
            "id": "BALRAMPUR",
            "name": "BALRAMPUR"
        },
        {
            "id": "BANDA",
            "name": "BANDA"
        },
        {
            "id": "BARABANKI",
            "name": "BARABANKI"
        },
        {
            "id": "BAREILLY",
            "name": "BAREILLY"
        },
        {
            "id": "BASTI",
            "name": "BASTI"
        },
        {
            "id": "BIJNOR",
            "name": "BIJNOR"
        },
        {
            "id": "BUDAUN",
            "name": "BUDAUN"
        },
        {
            "id": "BULANDSHAHR",
            "name": "BULANDSHAHR"
        },
        {
            "id": "CHANDAULI",
            "name": "CHANDAULI"
        },
        {
            "id": "CHITRAKOOT",
            "name": "CHITRAKOOT"
        },
        {
            "id": "DEORIA",
            "name": "DEORIA"
        },
        {
            "id": "ETAH",
            "name": "ETAH"
        },
        {
            "id": "ETAWAH",
            "name": "ETAWAH"
        },
        {
            "id": "FAIZABAD",
            "name": "FAIZABAD"
        },
        {
            "id": "FARRUKHABAD",
            "name": "FARRUKHABAD"
        },
        {
            "id": "FATEHPUR",
            "name": "FATEHPUR"
        },
        {
            "id": "FIROZABAD",
            "name": "FIROZABAD"
        },
        {
            "id": "GAUTAM BUDDHA NAGAR",
            "name": "GAUTAM BUDDHA NAGAR"
        },
        {
            "id": "GHAZIABAD",
            "name": "GHAZIABAD"
        },
        {
            "id": "GHAZIPUR",
            "name": "GHAZIPUR"
        },
        {
            "id": "GONDA",
            "name": "GONDA"
        },
        {
            "id": "GORAKHPUR",
            "name": "GORAKHPUR"
        },
        {
            "id": "HAMIRPUR",
            "name": "HAMIRPUR"
        },
        {
            "id": "HARDOI",
            "name": "HARDOI"
        },
        {
            "id": "HATHRAS",
            "name": "HATHRAS"
        },
        {
            "id": "JALAUN",
            "name": "JALAUN"
        },
        {
            "id": "JAUNPUR",
            "name": "JAUNPUR"
        },
        {
            "id": "NOIDA",
            "name": "NOIDA"
        }
    ],
    "Uttarakhand": [
        {
            "id": "ALMORA",
            "name": "ALMORA"
        },
        {
            "id": "BAGESHWAR",
            "name": "BAGESHWAR"
        },
        {
            "id": "CHAMOLI",
            "name": "CHAMOLI"
        },
        {
            "id": "CHAMPAWAT",
            "name": "CHAMPAWAT"
        },
        {
            "id": "DEHRADUN",
            "name": "DEHRADUN"
        },
        {
            "id": "HARIDWAR",
            "name": "HARIDWAR"
        },
        {
            "id": "NAINITAL",
            "name": "NAINITAL"
        },
        {
            "id": "PAURI GARHWAL",
            "name": "PAURI GARHWAL"
        },
        {
            "id": "PITHORAGARH",
            "name": "PITHORAGARH"
        },
        {
            "id": "RUDRAPRAYAG",
            "name": "RUDRAPRAYAG"
        },
        {
            "id": "TEHRI GARHWAL",
            "name": "TEHRI GARHWAL"
        },
        {
            "id": "UDHAM SINGH NAGAR",
            "name": "UDHAM SINGH NAGAR"
        },
        {
            "id": "UTTARKASHI",
            "name": "UTTARKASHI"
        }
    ],
    "West Bengal": [
        {
            "id": "BANKURA",
            "name": "BANKURA"
        },
        {
            "id": "BARDHAMAN",
            "name": "BARDHAMAN"
        },
        {
            "id": "BIRBHUM",
            "name": "BIRBHUM"
        },
        {
            "id": "COOCH BEHAR",
            "name": "COOCH BEHAR"
        },
        {
            "id": "DARJILING",
            "name": "DARJILING"
        },
        {
            "id": "EAST MIDNAPORE",
            "name": "EAST MIDNAPORE"
        },
        {
            "id": "HOOGHLY",
            "name": "HOOGHLY"
        },
        {
            "id": "HOWRAH",
            "name": "HOWRAH"
        },
        {
            "id": "JALPAIGURI",
            "name": "JALPAIGURI"
        },
        {
            "id": "KOLKATA",
            "name": "KOLKATA"
        },
        {
            "id": "MALDA",
            "name": "MALDA"
        },
        {
            "id": "MURSHIDABAD",
            "name": "MURSHIDABAD"
        },
        {
            "id": "NADIA",
            "name": "NADIA"
        },
        {
            "id": "NORTH 24 PARGANAS",
            "name": "NORTH 24 PARGANAS"
        },
        {
            "id": "NORTH DINAJPUR",
            "name": "NORTH DINAJPUR"
        },
        {
            "id": "PURULIYA",
            "name": "PURULIYA"
        },
        {
            "id": "SOUTH 24 PARGANAS",
            "name": "SOUTH 24 PARGANAS"
        },
        {
            "id": "SOUTH DINAJPUR",
            "name": "SOUTH DINAJPUR"
        },
        {
            "id": "WEST MIDNAPORE",
            "name": "WEST MIDNAPORE"
        }
    ]
}
