from django.conf import settings
from wiki.models import FamilyCoverPremiumTable
import copy


class PseudoQuerySet(list):
    def filter(self, *args, **kwargs):
        return self


class PseudoCoverPremiumTables:
    SERVICE_TAX = settings.SERVICE_TAX_PERCENTAGE / 100

    def __init__(self, sl, cpts, data):
        self.data = copy.deepcopy(data)
        self.sl = sl
        self.cpts_master = cpts
        self.max_age = self.data['age']
        self.data['kid_ages'] = [age * 1000 for age in self.data['kid_ages']]
        self.data['parent_ages'] = [age * 1000 for age in self.data['parent_ages']]
        self.data['you_and_spouse_ages'] = [age * 1000 for age in self.data['you_and_spouse_ages']]

        # The oldest member will be the primary member for New India Assuarance
        self.secondary_ages = self.data['kid_ages'] + self.data['parent_ages'] + self.data['you_and_spouse_ages']
        self.primary_age = max(self.secondary_ages)
        self.secondary_ages.remove(self.primary_age)

    def get_mcpts(self, current_year_only=False):
        if current_year_only:
            return self.create_CoverPremiumTable(0)
        mcpts = PseudoQuerySet()
        for year_offset in range(0, 5000, 1000):
            cpt = self.create_CoverPremiumTable(year_offset)
            if cpt:
                mcpts.append(cpt)
        return mcpts

    def create_CoverPremiumTable(self, year_offset):
        primary_age = self.primary_age + year_offset
        # TODO: handle wrong 5th yr output if cpt is 0-4yrs
        secondary_ages = [age+year_offset for age in self.secondary_ages]
        max_age = self.max_age + year_offset
        premium = 0
        primary_cpt = self.cpts_master.filter(
            adults=1,
            lower_age_limit__lte=primary_age,
            upper_age_limit__gte=primary_age,
        )
        if len(primary_cpt) == 0:
            return None
        if len(primary_cpt) > 1:
            raise Exception("Two matching conditions found in same slab %s, product: %s"
                            % (self.sl.id, self.sl.health_product.title))
        primary_cpt = primary_cpt[0]
        premium += primary_cpt._premium

        secondary_slabs = self.cpts_master.filter(adults=-1)
        for i, sec_age in enumerate(secondary_ages):
            secondary_slab = secondary_slabs.filter(
                lower_age_limit__lte=sec_age,
                upper_age_limit__gte=sec_age,
            )
            if len(secondary_slab) == 0:
                return None
            if len(secondary_slab) > 1:
                raise Exception("Two matching conditions found in same slab %s, product: %s"
                                % (self.sl.id, self.sl.health_product.title))
            premium += secondary_slab[0]._premium

        max_age = max([primary_age]+secondary_ages)
        return FamilyCoverPremiumTable(
            payment_period=1,
            adults=self.data['adults'],
            kids=self.data['kids'],
            lower_age_limit=max_age,
            upper_age_limit=max_age,
            two_year_discount=primary_cpt.two_year_discount,
            _premium=premium
        )


def getCoverPremiumTables(sl, cpts, data):
    return PseudoCoverPremiumTables(sl, cpts, data).get_mcpts()


def getCoverPremiumTable(sl, data):
    mcpts_sl = FamilyCoverPremiumTable.objects.filter(slabs=sl, payment_period=1)
    mcpts_place_preference = mcpts_sl.filter(
        city=data['city']
    ) or mcpts_sl.filter(
        state__city=data['city']
    ) or mcpts_sl.filter(
        city__isnull=True,
        state__isnull=True
    )
    return PseudoCoverPremiumTables(sl, mcpts_place_preference, data).get_mcpts(current_year_only=True)
