from wiki.models import City, Pincode
import datetime
from health_product.insurer import custom_utils
from health_product.insurer.oriental import preset
from collections import OrderedDict


class Formic():
    def __init__(self, transaction, is_self_present):
        self.transaction = transaction
        self.is_self_present = is_self_present
        self.proposer_data = {
            'pincode': transaction.extra['pincode'],
            'city' : City.objects.get(id=transaction.extra['city']['id']).name,
            'state' : City.objects.get(id=transaction.extra['city']['id']).state.name,
            'email': ''
        }

    def add_parent_segment(self, segment_id, tag_id, details, question_type):
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': details, 'data': []}
        row = []
        if question_type == 'MultipleChoiceField':
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='MultipleChoiceField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    details=member,
                    value='No',
                    label=details.get('question_text', ''),
                    span=4,
                    css_class='bottom-margin',
                    event=', click : $parent.root_context.stage_5.any_yes_check',
                    template_option='question'
                ))
            new_segment['data'].append(row)
        elif question_type == 'DropDownField':
            if tag_id == 'pre_existing':
                for member in self.transaction.extra['members']:
                    row.append(custom_utils.new_field(
                        saved_data=saved_data,
                        field_type='DropDownField',
                        code='%s_question_%s' % (member['id'], tag_id),
                        options=preset.pre_existing_options,
                        details=member,
                        label=details.get('question_text', ''),
                        span=2,
                        css_class='bottom-margin',
                        event=', event : {change : root_context.stage_5.any_yes_check}',
                        template_option='question'
                    ))
            new_segment['data'].append(row)
        elif question_type == 'TextField':
            tag_id = 'pre_existing'
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='TextField',
                    #label=details.get('question_text', ''),
                    code='%s_question_%s' % (member['id'], tag_id),
                    span=2,
                    required=False,
                    #only_if='root_context.stage_5.check_dict.get(\'%s\')' % (member['id'])
                ))
            new_segment['data'].append(row)
        elif question_type == 'DateField':
            today = datetime.datetime.today()
            todaystr=datetime.datetime.strftime(today,'%d/%m/%Y')
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='DateField',
                    code='%s_question_%s' % (member['id'], 'suffering_since'),
                    span=2,
                    required=False,
                    max_date=todaystr,
                    #dependant_parent='%s_question_%s' % (member['id'],tag_id),
                    #only_if='root_context.stage_5.check_dict.get(\'%s_question_%s\')' % (member['id'],'pre_existing')
                ))
            new_segment['data'].append(row);
        return new_segment



    def stage_1(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'personal'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            label='First Name',
            code='first_name',
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            label='Last Name',
            code='last_name',
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Gender',
            code='gender',
            value=1 if self.transaction.extra['gender'] == 'MALE' else 2,
            options=preset.GENDER_OPTIONS
        ))
        new_segment['data'].append(row)
        row = []

        married = True if [m for m in self.transaction.extra['members'] if m['id'] == 'spouse'] else False;
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Marital Status',
            code='marital_status',
            options=preset.MARITAL_STATUS_OPTIONS,
            value= 'MARRIED' if married else None,
            template_option= 'static' if married else 'dynamic'
        ))
        if self.is_self_present:
            member = [m for m in self.transaction.extra['members'] if m['id'] == 'self'][0]
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                default_age=member['age'],
                min_age=18,
                max_age=100,
                required=False,
            ))
        else:
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                default_age=18,
                min_age=18,
                max_age=100,
            ))
        new_segment['data'].append(row)
        row = []
        stage.append(new_segment)

        return {'template' : 'stage_1', 'data' : stage}

    def stage_2(self):
        stage = []
        feet_range = []
        for feet in range(0, 9):
            feet_range.append({'id' : feet, 'name' : '%s ft' % feet})
        inch_range = []
        for inch in range(0, 12):
            inch_range.append({'id' : inch, 'name' : '%s inches' % inch})

        for member in self.transaction.extra['members']:
            segment_id = 'insured'
            tag_id = member['id']
            template_switch = 'dynamic'
            if tag_id == 'self':
                template_switch = 'static'
            try:
                saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : member, 'data' : []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                label='First Name',
                code='first_name',
                max_length=50,
                template_option=template_switch,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                label='Last Name',
                code='last_name',
                max_length=50,
                template_option=template_switch,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                span=3,
                template_option=template_switch,
                min_age=18 if member['type']=='adult' else 0,
                max_age=100,
                default_age=member['age'],
            ))
            if member['type'] != 'kid':
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='DropDownField',
                    label='Occupation',
                    code='occupn',
                    span=3,
                    options=preset.OCCUPATION_OPTIONS,
                ))
            new_segment['data'].append(row)
            stage.append(new_segment)

        return {'template' : 'stage_2', 'data' : stage}

    def stage_3(self):
        default_relation = {
            'self': 'Spouse',
            'spouse': 'Spouse',
            'son': 'Father' if self.transaction.extra['gender'] == 'MALE' else 'Mother' ,
            'daughter': 'Father' if self.transaction.extra['gender'] == 'MALE' else 'Mother' ,
            'father': 'Son' if self.transaction.extra['gender'] == 'MALE' else 'Daughter',
            'mother': 'Son' if self.transaction.extra['gender'] == 'MALE' else 'Daughter'
        }
        self_relation_probables = OrderedDict([
            ('mother', 'Mother'),
            ('father', 'Father'),
            ('spouse', 'Spouse')
        ])
        #Needs to run before the big loop on members
        for member in self.transaction.extra['members']:
            if member['person'] in self_relation_probables.keys():
                self.transaction.extra['self_nominee'] = member['id']
                self.transaction.save()
                default_relation['self'] = self_relation_probables[member['person']]

        stage = []
        for member in self.transaction.extra['members']:
            segment_id = 'nominee'
            tag_id = member['id']
            try:
                saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : member, 'data' : []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DropDownField',
                code='relation',
                label='Relationship With %s' % member['verbose'],
                options=preset.relation_options,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                code='first_name',
                label='First Name',
                max_length=50,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                code='last_name',
                label='Last Name',
                max_length=50,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                code='date_of_birth',
                label='Date Of Birth',

                min_age=18,
                span=3
            ))
            new_segment['data'].append(row)
            stage.append(new_segment)
        return {'template' : 'stage_3', 'data' : stage}

    def stage_4(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'address'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address1',
            label='Flat/House No, Street',
            span=12,
            css_class='bottom-margin',
            max_length=50
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address2',
            label='Area/Locality',
            span=12,
            css_class='bottom-margin',
            max_length=100,
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='landmark',
            label='Landmark',
            span=12,
            css_class='bottom-margin',
            max_length=100,
            required=False
        ))
        new_segment['data'].append(row)
        row = []
        pincode = Pincode.objects.filter(pincode=self.transaction.extra['pincode'])[0]
        state = pincode.state
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='SearchableDropDownField',
            code='city',
            label='City',
            options=[{'id': c.name, 'name': c.name} for c in state.city_set.all()]
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='state',
            label='State',
            value=self.proposer_data['state'],
            span=4,
            template_option='static'
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='pincode',
            label='Pincode',
            value=str(self.proposer_data['pincode']),
            span=4,
            template_option='static'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)


        tag_id = 'contact'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='email',
            label='Email',
            span=12,
            css_class='bottom-margin',
            email=True
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MobileField',
            code='mobile',
            label='Mobile',
            css_class='bottom-margin',
            span=12
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='LandlineField',
            code='landline',
            label='Landline',
            span=12,
            css_class='bottom-margin',
            required=False
            ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template' : 'stage_4', 'data' : stage}

    def stage_5(self):
        stage = []
        i=0;
        feet_range = []
        for feet in range(0, 9):
            feet_range.append({'id' : feet, 'name' : '%s ft' % feet})
        inch_range = []
        for inch in range(0, 12):
            inch_range.append({'id' : inch, 'name' : '%s inches' % inch})

        for member in self.transaction.extra['members']:
            segment_id = 'medical'
            tag_id = member['id']
            # template_switch = 'dynamic'
            # if tag_id == 'self':
            #     template_switch = 'static'
            try:
                saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : member, 'data' : []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='SearchableDropDownField',
                code='%s_question_%s' % (member['id'], "disease"),
                label="Disease",
                options=preset.pre_existing_options,
                event=', change : $parent.root_context.stage_5.any_yes_check',
                span=3,
                required=True,
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                label='Mention other disease',
                code='%s_question_%s' % (member['id'],"other_disease"),
                span=3,
                required=False,
                only_if="root_context.transaction_model().stages()[4].segments()[%s].rows()[0].elements()[0].value()=='Other disease'"%(i),
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateTextField',
                label='Suffering since',
                code='%s_question_%s' % (member['id'],"suffering_since"),
                span=3,
                required=False,
                only_if="root_context.transaction_model().stages()[4].segments()[%s].rows()[0].elements()[0].value()!='NONE' && root_context.transaction_model().stages()[4].segments()[%s].rows()[0].elements()[0].value()"%(i,i),
            ))
            new_segment['data'].append(row)
            stage.append(new_segment)
            i+=1; #THIS IS IMPORTANT..
        return {'template' : 'stage_5', 'data' : stage}

    def stage_6(self):
        stage = []
        segment_id = 'terms'
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}
        row = []
        t_and_c_label = ('I hereby confirm<br />'
                         '- I have read and agree to the Terms and Conditions of ')
        if self.transaction.slab.health_product.policy_wordings:
            t_and_c_label += ('the <a style="text-decoration:none;cursor:pointer;" '
                              'onclick=\'window.open("%s","","width=400, height=400")\'>policy</a> and '
                             ) % self.transaction.slab.health_product.policy_wordings.url
        t_and_c_label += ('the <a style="cursor:pointer;" class="call_overlay">website</a>;<br />'
                          '- appointment of Coverfox Insurance Brokers Pvt. Ltd. as my Insurance Broker, '
                          'and authorize them to represent me to Insurance Companies/Agencies for my Insurance needs;<br />'
                          '- the information given by me in this form is true '
                          'and I understand that any of these details if found untrue on correlation with my medical test or medical examination '
                          'before or after issuance of policy will affect the coverage and payments of my health insurance benefit '
                          'under this Mediclaim policy.'
                         )
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='t_and_c',
            label=t_and_c_label,
            span=12,
            options=[
                {'id' : 'Yes', 'name' : 'Yes', 'is_correct' : True},
                {'id' : 'No', 'name' : 'No', 'is_correct' : False}
            ],
            css_class='bottom-margin',
            template_option='terms',
            required=True,
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template' : 'stage_6', 'data' : stage}

    def get_form_data(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
            self.stage_6()
        ]
        return data

def get_dynamic_form(transaction):
    transaction, plan_data, proposer_data, is_self_present = custom_utils.standard_form_data_cleanup(transaction)
    #TOGGLES WHERE MEDICAL CASES ARE ALLOWED OR NOT
    plan_data['medical_firewall'] = False

    # policy_code = 'classic_individual'
    # if transaction.slab.health_product.id in [66]:
    #     policy_code = 'silver'
    # elif transaction.slab.health_product.id in [67]:
    #     policy_code = 'gold'

    obj = Formic(transaction, is_self_present)
    stages = getattr(obj, 'get_form_data','')()

    terms_and_conditions = "<div style='padding:20px 40px 70px 20px;'>" \
                           "<h3>Oriental Insurance: Terms of Use</h3>" \
                           "<p>I/we declare that the statements made by me/us in this proposal form are true and to the best of my/our knowledge and belief and I/we hereby agree that this declaration shall form the basis of the contract between me/us and The Oriental Insurance Company Ltd..</p>" \
                           "<p>I/we also declare that if any additions or alterations are carried out after the submission of this proposal form and/ or issuance of policy document, the same would be conveyed to The Oriental Insurance Company immediately.</p>" \
                           "<p>I/we hereby agree to and authorise the disclosure to the insurer or the TPA or any other person nominated by the insurer any and all Medical records and information held by any Institution/Hospital or Person from whom the insured person has obtained any medical or other treatment to the extent reasonably required by either the insurer or the TPA in connection with any claim made under this policy or the insurer's liability there under.</p>" \
                           "<p>I/ we further declare that I/ we have read the prospectus and have understood the same. I accept the policy, subject to terms, exceptions and conditions prescribed therein and further disclose that on the event of finding any thing contrary to what has been declared by me, I/ we shall be held responsible for all consequences thereof and insurance company shall incur no liability under this insurance.</p>" \
                           "<p>I/ we further declare that the Insurance Company shall not be liable to make any payment under this policy in respect of any claim if such claim be in any manner intentionally or recklessly or otherwise misrepresented or concealed or non disclosure of material facts or making false statements or submitting false bills whether by the Insured Person or Institution/ Organization on his behalf. Such action shall render this policy null and void and all benefits hereunder shall be forfeited. Company may take suitable legal action against the Insured Person/ Institution/ Organization as per Law.</p>" \
                           "<h3>Coverfox: Terms and Conditions</h3>" \
                           "<p>Coverfox Insurance Broking Pvt. (hereinafter referred to as Coverfox) Limited operates " \
                           "the website <a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> to provide " \
                           "consumers choice and an easy way to buy Insurance " \
                           "products from multiple Insurance companies. Coverfox is not an insurance company. Coverfox " \
                           "is a licensed Insurance Broking Company holding a Broking licence from the Indian " \
                           "Insurance Regulator - Insurance Regulatory and Development authority. " \
                           "<a href='/irda-licence/' target='_blank'>(View IRDA License)</a></p>" \
                           "<p>Our participating insurance providers supply some of the information available on the " \
                           "Website and therefore there may be inaccuracies in the Website Information over which " \
                           "Coverfox has limited control.</p>" \
                           "<p>Coverfox does not warrant or guarantee the: Timeliness, accuracy or completeness of " \
                           "the Website Information; or Quality of the results obtained from the use of the Website.</p>" \
                           "<p>To the maximum extent permitted by law, Coverfox has no liability in relation to or " \
                           "arising out of the Website Information and Website recommendations. You are responsible " \
                           "for the final choice of your product and you should take time to read through all " \
                           "information supplied before proceeding. If you are in any doubt regarding a product or " \
                           "its terms you should seek further advice from Coverfox or the relevant participating " \
                           "provider before choosing your product.</p>" \
                           "<p>Coverfox may pass on your personal information to the relevant participating provider " \
                           "if you apply to purchase a product through " \
                           "<a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> , however, Coverfox does " \
                           "not guarantee when or if you will actually acquire the product that you have chosen. " \
                           "Coverfox does not accept any liability arising out of circumstances where there is delay " \
                           "in you acquiring the product you have chosen.</p>" \
                           "<p>Please note that Coverfox is only collecting or assisting in collecting  the premium " \
                           "deposit on behalf of the insurer you have chosen to buy the policy. The acceptance of " \
                           "the deposit as premium and final issuance of the policy is subject to the underwriting " \
                           "norms and discretion of the Insurer whose policy you have chosen to buy on which Coverfox " \
                           "has no control. Coverfox will ensure that the amount is refunded by the insurer in case " \
                           "there is no ultimate issuance of policy.</p>" \
                           "<p><button class='btn btn-lg cf-btn btn-red hide_overlay'>Close</button></p>" \
                           "</div>"

    policy_date_disclaimer = 'NOTE: The policy will be issued and sent to you in 10 working days from the date this transaction is completed.'

    slate_list = [
        {'id': 'slate_1', 'name': 'Proposer', 'class' : '', 'description' : 'The person who is paying for the policy'},
        {'id': 'slate_2', 'name' : 'Insured Members', 'class' : '', 'description' : 'People being insured under the policy'},
        {'id': 'slate_3', 'name' : 'Nominee', 'class' : '', 'description' : 'Nominees for the insured members'},
        {'id': 'slate_4', 'name' : 'Contact Info.', 'class' : '', 'description' : 'Contact Information of the proposer'},
        {'id': 'slate_5', 'name' : 'Medical History', 'class' : '', 'description' : 'Medical History of the people being insured'},
        {'id': 'slate_6', 'name' : 'T & C', 'class' : 'last', 'description' : 'Mandatory terms and conditions', 'style' : " 'width', '0px' "}
    ]
    data = {
        'slate_list' : slate_list,
        'proposer_data' : proposer_data,
        'plan_data' : plan_data,
        'stages' : stages,
        'last_stage' : stages[-1]['template'],
        'terms_and_conditions' : terms_and_conditions,
        'policy_date_disclaimer' : policy_date_disclaimer
    }

    return data

