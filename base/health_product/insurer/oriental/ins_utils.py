


from django.http import HttpResponse
from collections import OrderedDict
from django.conf import settings
import datetime
import xlwt

from mail_utils import health_alert

class xlCell:
    def __init__(self, row=0, col=0, worksheet=None):
        self.row = row
        self.col = col
        self.ws = worksheet

    def _copy(self):
        return self.__class__(self.row, self.col, self.ws)

    def mvLeft(self, steps=1):
        if steps < 0:
            return self.mvRight(steps)
        self.col -= steps
        if self.col<0:
            self.col = 0
        return self

    def mvRight(self, steps=1):
        if steps < 0:
            return self.mvLeft(steps)
        self.col += steps
        return self

    def mvUp(self, steps=1):
        if steps < 0:
            return self.mvDown(steps)
        self.row -= steps
        if self.row<0:
            self.row = 0
        return self

    def mvDown(self, steps=1):
        if steps < 0:
            return self.mvUp(steps)
        self.row += steps
        return self

    def move(self, down=0, right=0):
        return self.mvDown(down).mvRight(right)

    def left(self, steps=1):
        return self._copy().mvLeft(steps)

    def right(self, steps=1):
        return self._copy().mvRight(steps)

    def up(self, steps=1):
        return self._copy().mvUp(steps)

    def down(self, steps=1):
        return self._copy().mvDown(steps)

    def offset(self, right=0, down=0):
        return self._copy().move(right, down)

    def nextRow(self):
        self.col = 0
        return self.mvDown()

    def write(self, value, style=None, move_right=True):
        if isinstance(value, datetime.datetime):
            value = datetime.datetime.strftime(value, '%d-%b-%Y %I:%M %p')
        if style is None:
            self.ws.write(self.row, self.col, value)
        else:
            self.ws.write(self.row, self.col, value, style)
        return self.mvRight() if move_right else self

    def write_row(self, row, style=None, move_right=True):
        for value in row:
            self.write(value)
        return self if move_right else self.mvLeft(len(row))

    def write_matrix(self, matrix):
        for row in matrix:
            self.write_row(row).nextRow()



def download_buy_form(transaction):
    sl = transaction.slab
    hp = sl.health_product
    proposer = transaction.insured_details_json['form_data']['proposer']['personal']
    contact = transaction.insured_details_json['form_data']['proposer']['contact']
    address = transaction.insured_details_json['form_data']['proposer']['address']
    insured = transaction.insured_details_json['form_data']['insured']
    nominees = transaction.insured_details_json['form_data']['nominee']
    medical = transaction.insured_details_json['form_data']['medical']

    boldFont = xlwt.Font()
    boldFont.bold = True
    bold = xlwt.XFStyle()
    bold.font = boldFont

    def dv(field):
        return field['value']
    def get_name(person):
        return '%s %s' % (dv(person['first_name']), dv(person['last_name']))
    def readable_date(dt):
        "From format 01/10/2010 to 01-Oct-2010"
        dt = datetime.datetime.strptime(dt, '%d/%m/%Y')
        return datetime.datetime.strftime(dt, '%d-%b-%Y')

    import StringIO
    output = StringIO.StringIO()
    export_wb = xlwt.Workbook()
    export_sheet = export_wb.add_sheet('Export')
    cell = xlCell(worksheet=export_sheet)

    cell.write(hp.insurer.title).nextRow()
    cell.write('Health Proposer Form').nextRow().nextRow()
    cell.write('Proposer Details', bold).nextRow()
    cell.write('Proposal Date').write(transaction.updated_on).nextRow()
    cell.write('Name of Proposer').write(get_name(proposer)).nextRow()

    cell.write('Address')
    cell.write(dv(address['address1'])+', '+dv(address['address2']), move_right=False).mvDown()
    cell.write(dv(address['landmark'])+', '+dv(address['city'])+' - '+dv(address['pincode'])+', '+dv(address['state'])).nextRow()

    cell.write('Transaction Number').write(transaction.reference_id).nextRow()
    cell.write('E-mail').write(dv(contact['email'])).nextRow()
    cell.write('Phone Number').write(dv(contact['mobile'])).nextRow()

    cell.nextRow()
    cell.write('Product Details', bold).nextRow()
    cell.write('Product name').write('Happy Family Floater').nextRow()
    cell.write('Cover Type').write('Floater').nextRow()
    cell.write('Plan Type').write('Silver' if sl.sum_assured<=500000 else 'Gold').nextRow()
    cell.write('E-mail').write(dv(contact['email'])).nextRow()

    cell.nextRow()
    cell.write_row(['Sr. No', 'Name of the insured', 'Relationship with Proposer', 'Sex (M/F)',
        'Whether dependant on the proposer Y / N', 'Date of Birth', 'Age (in completed years)', 'Occupation',
        'Medical history/ Pre - existing ailment', 'Suffering Since', 'Personal Habit', 'Basic cover', 'Add on PA cover']).nextRow()
    for i, member in enumerate(insured, 1):
        mem_details = insured[member]
        member = member.lower()

        dob = dv(mem_details['date_of_birth'])
        dob_datetime = datetime.datetime.strptime(dob, '%d/%m/%Y')
        dob = readable_date(dob)
        now = datetime.datetime.now()
        completed_age = now.year - dob_datetime.year
        if now.month < dob_datetime.month or (now.month==dob_datetime.month and now.day<dob_datetime.day):
            completed_age -= 1

        mem_occupation = dv(mem_details['occupn']) if 'occupn' in mem_details else ''

        mem_med = dv(medical[member]['%s_question_disease'%member])
        if mem_med=='Other disease':
            mem_med = dv(medical[member]['%s_question_other_disease'%member])
        mem_med_since = dv(medical[member]['%s_question_suffering_since'%member])

        if member=='self':
            gender = dv(proposer['gender'])
        elif member=='spouse':
            gender = 'Female' if dv(proposer['gender'])=='Male' else 'Male'
        elif member.find('father')!=-1 or member.find('son')!=1:
            gender = 'Male'
        elif member.find('mother')!=-1 or member.find('daughter')!=1:
            gender = 'Female'

        row = [i, get_name(mem_details), member.capitalize(), gender, '', dob, completed_age, mem_occupation,
                mem_med, mem_med_since, 'None', sl.sum_assured, '-']
        cell.write_row(row).nextRow()

    cell.nextRow()
    cell.write('Nominee details', bold).nextRow()
    cell.write_row(['Sr. No', 'Name of the insured', 'Name of the Nominee', 'Relationship with Insured']).nextRow()
    for i, member in enumerate(nominees, 1):
        nominee = nominees[member]
        row = [i, get_name(insured[member]), '%s %s'%(dv(nominee['first_name']), dv(nominee['last_name'])), dv(nominee['relation'])]
        cell.write_row(row).nextRow()

    cell.nextRow()
    cell.write('Payment Details', bold).nextRow()
    cell.write('Payment date & time').write(transaction.updated_on)
    cell.write('Transaction ref/ order id').write(transaction.reference_id)
    cell.write('Premium').write(transaction.premium_paid)
    cell.nextRow()

    cell.nextRow()
    cell.write('Broker Name').write('Coverfox Insurance Broking Pvt Ltd.')
    cell.write('Broking Code').write('LC0000000416')
    cell.nextRow()

    cell.nextRow()
    cell.write('Special Instructions, if any.').write('No').nextRow()
    cell.write('Customer Declaration').write('No')

    cell.nextRow().nextRow()
    cell.nextRow().write('I the Undersigned hereby declare that all the information given by me in this form is true.')
    cell.nextRow().write('I understand that any of these details if found untrue on correlation with my medical test or medical examination before or after issuance of policy')
    cell.nextRow().write('will affect the coverage and payments of my health insurance benefit under this Mediclaim policy.')
    export_wb.save(output)
    output.seek(0)
    response = HttpResponse(output.getvalue())
    response['Content-Type'] = 'application/vnd.ms-excel'
    response['Content-Disposition'] = 'attachment; filename=Oriental_Form_%s_%s.xls' % (transaction.proposer_first_name, transaction.transaction_id)
    return response


def transaction_successful(transaction):
    sl = transaction.slab
    msg_dict = OrderedDict([
        ('Transaction ID', transaction.transaction_id),
        ('Email', transaction.proposer_email),
        ('Plan', '%s - (Rs. %s)' % (sl.health_product.title, sl.sum_assured)),
        ('Premium Paid', transaction.premium_paid),
        ('Download Buy Form Data', '%s/health-plan/buy-form-download/%s/' % (settings.SITE_URL, transaction.transaction_id)),
        ('Download Transaction Data', '%s/health-plan/transaction/%s/download/' % (settings.SITE_URL, transaction.transaction_id)),
    ])
    msg_text = '\n'.join(['%s: %s' % (k, v) for k, v in msg_dict.items()])
    sub_text = 'ORIENTAL Payment Success'
    mail_list = [
        'soman@coverfox.com',
        'kalpesh@coverfox.com',
        'ramitha@coverfox.com',
        'ops@coverfox.com',
    ]
    health_alert(sub_text, msg_text, mail_list)
