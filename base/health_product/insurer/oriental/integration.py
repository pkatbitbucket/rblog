from health_product.insurer import settings as insurer_settings
from django.conf import settings
from collections import OrderedDict
import urllib2
import urllib
import datetime
import hashlib
import json
import ast
import requests
from health_product.insurer import custom_utils
from utils import request_logger
from health_product.insurer.oriental import tekprocess_credentials
from wiki.models import City
from dateutil import relativedelta
import base64
from Crypto.Cipher import AES
from Crypto import Random

import logging
from suds.client import Client
from suds.sax.attribute import Attribute
from suds.plugin import MessagePlugin

BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
unpad = lambda s : s[:-ord(s[len(s)-1:])]

class AESCipher:
    def __init__(self, key, iv):
        self.key = key
        self.iv = iv

    def encrypt(self, raw):
        raw = pad(raw)
        AES.key_size = 128
        cipher = AES.new(self.key, AES.MODE_CBC, self.iv)
        return base64.b64encode(cipher.encrypt(raw))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        cipher = AES.new(self.key, AES.MODE_CBC, self.iv)
        return unpad(cipher.decrypt(enc))

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.transport.http').setLevel(logging.DEBUG)


SCHEME_MASTER = {
    'prime' : 1,
    'classic': 2,
    'Super Top Up': 3,
}
PRODUCT_NAME_CHOICES = {
    'prime' : 'my:health Medisure Prime Insurance Policy',
    'classic' : 'my:health Medisure Classic Policy',
    'Super Top Up' : 'my:health Medisure Super Top Up',
}


LOCATION_MASTER = {
    'ZONE-I' : 1,
    'ZONE-II' : 2,
    'ZONE-III' : 3
}

SUM_INSURED_TYPE_CHOICES = {
    'FAMILY' : 'floater',
    'INDIVIDUAL' : 'individual'
}

BILLING_CYCLE_MASTER = {
    'MONTHLY' : 'Monthly',
    'QUARTERLY' : 'Quaterly',
    'HALF_YEARLY' : 'Half Yearly',
    'YEARLY' : 'Yearly'
}

QUESTION_MASTER = {
    'Does any person, proposed to be insured, suffer from or have been treated for any heart '
    'related ailment / blood pressure?' : 5,
    'Does any person, proposed to be insured, suffer from Diabetes/Asthma/Epilepsy?' : 6,
    'Does any person, proposed to be insured, suffer from any other disease/ailment?' : 7,
    'Is any person, proposed to be insured, receiving any treatment/medication or has in the past received '
    'treatment or undergone surgery for any medical condition/disability?' : 15,
}

GENDER_CHOICES = {
    1 : 'M',
    2 : 'F'
}

STATE_CHOICES = {
    1 : 'Andaman & Nicobar Islands',
    2 : 'Andhra Pradesh',
    3 : 'Arunachal Pradesh',
    4 : 'Assam',
    5 : 'Bihar',
    6 : 'Chandigarh',
    7 : 'Chattisgarh',
    8 : 'Dadra & Nagar Haveli',
    9 : 'Daman & Diu',
    10 : 'Delhi',
    11 : 'Goa',
    12 : 'Gujarat',
    13 : 'Haryana',
    14 : 'Himachal Pradesh',
    15 : 'Jammu & Kashmir',
    16 : 'Jharkhand',
    17 : 'Karnataka',
    18 : 'Kerala',
    19 : 'Lakshadweep',
    20 : 'Madhya Pradesh',
    21 : 'Maharashtra',
    22 : 'Manipur',
    23 : 'Meghalaya',
    24 : 'Mizoram',
    25 : 'Nagaland',
    26 : 'Orissa',
    27 : 'Pondicherry',
    28 : 'Punjab',
    29 : 'Rajasthan',
    30 : 'Sikkim',
    31 : 'Tamil Nadu',
    32 : 'Tripura',
    33 : 'Uttar Pradesh',
    34 : 'Uttarakhand',
    35 : 'West Bengal'
}

RELATIONSHIP_CHOICES = {
    'self' : 'Self',
    'spouse' : 'Spouse',
    'father' : 'Father',
    'mother' : 'Mother',
    'son' : 'Son',
    'son1' : 'Son',
    'son2' : 'Son',
    'son3' : 'Son',
    'son4' : 'Son',
    'daughter' : 'Daughter',
    'daughter1' : 'Daughter',
    'daughter2' : 'Daughter',
    'daughter3' : 'Daughter',
    'daughter4' : 'Daughter',
    #    'mil' : 'Mother In Law',
    #    'fil' : 'Father In Law',
    #    'sister' : 'Sister',
    #    'employee' : 'Employee',
    #    'brother' : 'Brother',
    #    'nephew' : 'Nephew',
    #    'niece' : 'Niece',
    #    'sil' : 'Sister In Law',
    #    'bil' : 'Brother In Law',
    #    'uncle' : 'Uncle',
    #    'aunt' : 'Aunt',
    #    'other' : 'Other',
}


SALUTATION_MASTER = {
    'Miss.' : 'Miss.',
    'CAPT.' : 'CAPT.',
    'Lt.Col.' : 'Lt.Col.',
    'Shri.' : 'Shri.',
    'Baby.' : 'Baby.',
    'Mast.' : 'Mast.',
    'Mr.' : 'Mr.',
    'MS.' : 'MS.',
    'Mrs.' : 'Mrs.',
    'Md.' : 'Md.',
    'Dr.' : 'Dr.'
}

SALUTATION_CHOICES = {
    1: 'Mr.',
    2: 'Miss.',
    3: 'Mrs.'
}

INCOME_MASTER = {
    'Between 2 - 5 Lacs' : 'Between 2 - 5 Lacs',
    'Between 5 - 10 Lacs' : 'Between 5 - 10 Lacs',
    'Between 10 - 20 Lacs' : 'Between 10 - 20 Lacs',
    '20 Lacs and above' : '20 Lacs and above'
}

INCOME_CHOICES = {
    '0_2' : 'Between 2 - 5 Lacs',
    '2_5' : 'Between 2 - 5 Lacs',
    '5_10' : 'Between 5 - 10 Lacs',
    '10_100' : 'Between 10 - 20 Lacs'
}

MARITAL_STATUS_MASTER = {
    'Married' : 'Married',
    'Single' : 'Single',
    'Divorced' : 'Divorced',
    'Widow' : 'Widow',
    'Not Mentioned' : 'Not Mentioned'
}

MARITAL_STATUS_CHOICES = {
    'SINGLE' : 'Single',
    'MARRIED' : 'Married',
}

OCCUPATION_MASTER = {
    'Professional/ Administrative/ Managerial' : 'Professional/ Administrative/ Managerial',
    'Bussiness/ Traders' : 'Bussiness/ Traders',
    'Clercial, Supervisory and related workers' : 'Clercial, Supervisory and related workers',
    'Hospitality and Support Workers' : 'Hospitality and Support Workers',
    'Production Workers, Skilled and non-Agricultural Labourers' : 'Production Workers, Skilled and non-Agricultural Labourers',
    'Farmers and Agricultural Workers' : 'Farmers and Agricultural Workers',
    'Police/ Para Militry/ Defence' : 'Police/ Para Militry/ Defence',
    'Housewife' : 'Housewife',
    'Retired Persons' : 'Retired Persons',
    'Students - School and College' : 'Students - School and College',
    'Others' : 'Others',
    'Profession' : 'Profession',
    'Business' : 'Business',
    'Clerk' : 'Clerk',
    'Student' : 'Student',
    'Agriculture' : 'Agriculture',
    'Professor' : 'Professor',
    'Doctor' : 'Doctor',
    'L&T Employee' : 'L&T Employee',
    'Self Employed' : 'Self Employed',
    'Govt. Service' : 'Govt. Service',
}

OCCUPATION_CHOICES = {
    'PVT' : 'Professional/ Administrative/ Managerial',
    'GOVT' : 'Govt. Service',
    'ARMED_FORCES' : 'Police/ Para Militry/ Defence',
    'SELF' : 'Self Employed',
    'OTHERS' : 'Others',
}

PRIME_MEMBER_TYPE_MASTER = {
    '2 Adults' : 1,
    '2 Adults and 1 Children' : 2,
    '2 Adults and 2 Children' : 3,
    'Individual' : 4,
    '1 Adult and 1 Children' : 5,
    '1 Adult and 2 Children' : 6,
    '1 Adult and 3 Children' : 7
}

PRIME_MEMBER_TYPE_CHOICES = {
    '2A-0C' : 1,
    '2A-1C' : 2,
    '2A-2C' : 3,
    '1A-0C' : 4,
    '1A-1C' : 5,
    '1A-2C' : 6,
    '1A-3C' : 7
}

PRIME_MEMBER_AGE_MASTER = {
    '3m TO 25' : 15,
    '26 TO 35' : 16,
    '36 TO 40' : 2,
    '41 TO 45' : 3,
    '46 TO 50' : 4,
    '51 TO 55' : 5,
    '56 TO 60' : 6,
    '61 TO 65' : 7,
    '66 TO 70' : 8,
    '71 TO 75' : 10,
    '76 TO 80' : 9,
    '81 TO 85' : 11,
    'ABOVE 85' : 14
}

CLASSIC_MEMBER_TYPE_MASTER = {
    '2 Adults' : 1,
    '2 Adults and 1 Children' : 2,
    '2 Adults and 2 Children' : 3,
    'Individual' : 4,
    '1 Adult and 1 Children' : 5,
    '1 Adult and 2 Children' : 6,
    '1 Adult and 3 Children' : 7,
    '1 Adult 4 Children' : 8,
    }

CLASSIC_MEMBER_TYPE_CHOICES = {
    '2A-0C' : 1,
    '2A-1C' : 2,
    '2A-2C' : 3,
    '1A-0C' : 4,
    '1A-1' : 5,
    '1A-2C' : 6,
    '1A-3C' : 7,
    '1A-4C' : 8,
    }


CLASSIC_MEMBER_AGE_MASTER = {
    'UP TO 35' : 1,
    '36 TO 40' : 2,
    '41 TO 45' : 3,
    '46 TO 50' : 4,
    '51 TO 55' : 5,
    '56 TO 60' : 6,
    '61 TO 65' : 7,
    '66 TO 70' : 8,
    '71 TO 75' : 10,
    '76 TO 80' : 9,
    'ABOVE 80' : 13,
    }

ZONE_CHOICES = {
    tuple([589, 935, 945, 952, 964, 974, 980, 990, 993, 1006, 1008, 2325, 2346, 2447, 2899, 2922, 4025, 4105, 4192,
           4404]) : 'ZONE-I',
    tuple([1330, 66, 2383, 3223, 4961, 699, 700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715,
           716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736,
           737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757,
           758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772, 773, 774, 775, 776, 777, 778,
           779, 780, 781, 782, 783, 784, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 796, 797, 798, 799,
           800, 801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820,
           821, 822, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832, 833, 834, 835, 836, 837, 838, 839, 840, 841,
           842, 843, 844, 845, 846, 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859, 860, 861, 862,
           863, 864, 865, 866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883,
           884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 898, 899, 900, 901, 902, 903, 904,
           905, 906, 907, 908, 909, 910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 920]) : 'ZONE-II',
    }


class Processor():
    def __init__(self, transaction):
        self.transaction = transaction

    def get_processed_data(self):
        policy_code = ''
        this_sum_insured_type = ''
        if self.transaction.slab.health_product.id in [66]:
            policy_code = 'silver'
        elif self.transaction.slab.health_product.id in [67]:
            policy_code = 'gold'

        if self.transaction.proposer_gender == 2 and self.transaction.insured_details_json['proposer']['maritalStatus'] == 'MARRIED':
            salutation = SALUTATION_CHOICES[3]
        else:
            salutation = SALUTATION_CHOICES[int(self.transaction.proposer_gender)]

        city_zone = 'ZONE-III'
        for key, val in ZONE_CHOICES.items():
            if self.transaction.extra['city']['id'] in key:
                city_zone = val

        pdata = self.transaction.insured_details_json['form_data']['proposer']['personal']
        prop_father_first_name = pdata['father_first_name']['value']
        prop_father_last_name = pdata['father_last_name']['value']
        prop_income = pdata['annual_income']['value']
        prop_occupation = pdata['occupation']['value']
        #TODO: TEST IF WORKS
        if 'marital_status' in pdata.keys():
            self.transaction.proposer_marital_status = pdata['marital_status']['value']
        if 'date_of_birth' in pdata.keys():
            self.transaction.proposer_date_of_birth = datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y')
        if 'gender' in pdata.keys():
            self.transaction.proposer_gender = int(pdata['gender']['value'])
        self.transaction.save()

        prop_father_full_name = custom_utils.get_full_name(first_name=prop_father_first_name, last_name=prop_father_last_name)

        city = City.objects.get(id=self.transaction.extra['city']['id'])
        state = city.state

        landline_number = self.transaction.insured_details_json['form_data']['proposer']['contact']['landline']['display_value']
        self.transaction.proposer_landline = landline_number

        client_object = OrderedDict([
            ('ClientRefNo', ''),
            ('Title', salutation),
            ('FirstName', self.transaction.proposer_first_name),
            ('MiddleName', self.transaction.proposer_middle_name),
            ('LastName', self.transaction.proposer_last_name),
            ('DateOfBirth', datetime.datetime.strftime(self.transaction.proposer_date_of_birth, '%d-%m-%Y')),
            ('FatherName', prop_father_full_name),
            ('Gender', GENDER_CHOICES[int(self.transaction.proposer_gender)]),
            ('MaritalStatus', MARITAL_STATUS_CHOICES[self.transaction.proposer_marital_status]),
            ('PanCard', ''),
            ('Occupation', prop_occupation),
            ('AnnualIncome', prop_income),
            ('BlockNo', self.transaction.proposer_address1),
            ('FloorNo', ''),
            ('BuildingName', ''),
            ('StreetName', self.transaction.proposer_address2),
            ('Locality', ''),
            ('LandMark', self.transaction.proposer_address_landmark if self.transaction.proposer_address_landmark else 'NA'),
            ('CountryName', 'India'),
            ('StateName', STATE_CHOICES[state.id]),
            ('CityName', self.transaction.insured_details_json['form_data']['proposer']['address']['city']['value']),
            ('PinCode', self.transaction.insured_details_json['form_data']['proposer']['address']['pincode']['value']),
            ('PostOffice', ''),
            ('Tehsil', self.transaction.insured_details_json['form_data']['proposer']['address']['city']['value']),
            ('Mobile', self.transaction.proposer_mobile),
            ('StdCodeWithLandLine', landline_number),
            ('Fax', ''),
            ('CommunicationAutoMailer', 'Y'),
            ('CommunicationPhysicalLetter', 'N'),
            ('Remarks', 'NA'),
            #('PSNumber', ''),
            #('DivisionName', '')
        ])

        floater_sum_insured = str(self.transaction.slab.sum_assured) if this_sum_insured_type == 'floater' else '0'
        sum_insured = str(self.transaction.slab.sum_assured) if this_sum_insured_type == 'individual' else '0'

        sub_limits = 'Y' if int(self.transaction.slab.health_product.id) in [26, 27, 31, 32] else 'N'
        si_critical = 'Y' if int(self.transaction.slab.health_product.id) in [28, 29, 31, 32] else 'N'

        soft_copy = 'N' if self.transaction.insured_details_json['form_data']['terms']['combo']['soft_copy'] == 'No' else 'Y'

        proposer_object = OrderedDict([
            ('suminsuredtype', this_sum_insured_type),
            ('policyduration', '1'),
            ('locationCode', city_zone),
            ('familysize', str(len(self.transaction.insured_details_json['form_data']['insured'].keys()))),
            #('age', relativedelta(datetime.datetime.today(), self.transaction.proposer_date_of_birth).years),
            ('floaterSumInsured', floater_sum_insured),
            ('doublesuminsured' , si_critical),
            ('roomrent', sub_limits),
            ('proposalRef', ''),    #NEW FIELD
            ('proposalRef', self.transaction.transaction_id),
            ('welcomeKitNo', ''),
            ('praWelcomeKitNo', 'N'),
            ('remark', 'NA'),
            ('paidAmount', '%s' % int(float(self.transaction.extra['plan_data']['premium']))),
            ('rollOver', 'N'),
            ('EmailID', self.transaction.proposer_email),
            ('scheme', policy_code.title()),
            ('branchName', BRANCH_NAME),
            ('tpaCode', TPA_CODE),
            ('productType', policy_code.title()),     #NEW FIELD
            # ('productType', ''),     #NEW FIELD
            ('intermediaryCode', INTERMEDIARY_CODE),
            ('proposalReceivedDate', datetime.datetime.strftime(datetime.datetime.today(), '%d-%m-%Y')),
            ('policyStartDate', datetime.datetime.strftime(datetime.datetime.today().date(), '%d-%m-%Y')),
            ('policyStartTime', '01:00 AM'),
            ('portablity', 'N'),
            # ('Suminsured', ''),
            ('portNo', '0'),
            ('billingCycle', 'Yearly'),
            ('paymentMode', 'Card'),
            ('instrumentNo', self.transaction.extra['payment_response']['TxnReferenceNo']),
            ('instrumentDate', datetime.datetime.strftime(datetime.datetime.today(), '%d-%m-%Y')),
            ('exclusion', ''),
            ('signingLocation', 'Mumbai'),
            ('signingDate', datetime.datetime.strftime(datetime.datetime.today(), '%d-%m-%Y')),
            ('eDispatch', soft_copy),
            ('floaterDeductibleAmount', ''),   #NEW FIELD
            ('noPrePost', ''),     #NEW FIELD
            ('reinstatement', ''),     #NEW FIELD
            ('extendedPrePost', ''),     #NEW FIELD
            ('hospitalCash', ''),     #NEW FIELD
            ('personAccompany', ''),     #NEW FIELD
            ('benefit', ''),     #NEW FIELD
            ('optDeductableAmt', ''),     #NEW FIELD
        ])
        insured_list = []
        ins_occupation = prop_occupation
        for code, fdata in self.transaction.insured_details_json['form_data']['insured'].items():
            ndata = self.transaction.insured_details_json['form_data']['nominee'][code]

            if code in ['self', 'spouse', 'father', 'mother']:
                ins_marital_status = 'MARRIED'
            else:
                ins_marital_status = 'SINGLE'

            feet = fdata['height']['value1']
            inches = fdata['height']['value2']
            if not feet:
                feet = 0
            if not inches:
                inches = 0

            ins_height = str(int(((int(feet) * 30.48) + int(inches) * 2.54)))

            ins_weight = fdata['weight']['value']
            ins_gender = int([member['gender'] for member in self.transaction.extra['members'] if member['id'] == code][0])
            medical_answer1 = self.transaction.insured_details_json['form_data']['medical_question_qid_5']['qid_5']['%s_question_qid_5' % code]['value']
            medical_answer2 = self.transaction.insured_details_json['form_data']['medical_question_qid_6']['qid_6']['%s_question_qid_6' % code]['value']
            medical_answer3 = self.transaction.insured_details_json['form_data']['medical_question_qid_7']['qid_7']['%s_question_qid_7' % code]['value']
            medical_answer4 = self.transaction.insured_details_json['form_data']['medical_question_qid_15']['qid_15']['%s_question_qid_15' % code]['value']
            medical_pre_existing = self.transaction.insured_details_json['form_data']['medical_question_pre_existing']['pre_existing']['%s_question_pre_existing' % code]['value']
            if (medical_answer1, medical_answer2, medical_answer3, medical_answer4, medical_pre_existing) == ('No', 'No', 'No', 'No', 'No'):
                medical_remarks = ''
            else:
                medical_remarks = self.transaction.insured_details_json['form_data']['medical_question_details']['details']['%s_question_details' % code]['value']
            insured_list.append(
                OrderedDict([
                    #('MemRefNo', ''),   #NEW FIELD
                    ('FirstName', fdata['first_name']['value']),
                    ('Title', SALUTATION_CHOICES[ins_gender]),
                    ('MiddleName', ''),
                    ('LastName', fdata['last_name']['value']),
                    ('Gender', GENDER_CHOICES[ins_gender]),
                    ('MaritalStatus', MARITAL_STATUS_CHOICES[ins_marital_status]),
                    ('Relation', RELATIONSHIP_CHOICES[code]),
                    ('Occupation', ins_occupation),
                    ('DateOfBirth', datetime.datetime.strftime(datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y'), '%d-%m-%Y')),
                    ('Height', ins_height),
                    ('Weight', ins_weight),
                    ('Nominee', custom_utils.get_full_name(first_name=ndata['first_name']['value'], last_name=ndata['last_name']['value'])),
                    ('MemRelation', ndata['relation']['value'].lower().capitalize()),
                    ('PreExistingIllness', '' if medical_pre_existing == 'NONE' else medical_pre_existing),
                    ('PreExistingIllnessDesc', '' if medical_pre_existing == 'NONE' else 'Yes'),
                    ('memIllness', ''),
                    ('memDateOfFirstConsultant', ''),
                    ('memTreatingDoctor', ''),
                    ('memHospitalName', ''),
                    ('memHospitalAddress', ''),
                    ('memFullyCare', ''),
                    ('memRollOver', 'N'),
                    ('memIdentityType', 'NONE'),
                    ('memIdentityValue', 'NONE'),
                    ('memPortablity', 'N'),
                    ('Suminsured', sum_insured),
                    ('PrevCB', ''),
                    ('PrevSI', ''),
                    ('memCurrentCB', ''),
                    ('memBreakin', ''),
                    ('Prevclaims', 'N'),
                    ('memSiForCritical', si_critical),
                    ('memRoomRentSubLimit', sub_limits),
                    #('strMode', ''),
                    ('ANS1', 'Y' if medical_answer1 == 'Yes' else 'N'),
                    ('ANS2', 'Y' if medical_answer2 == 'Yes' else 'N'),
                    ('ANS3', 'Y' if medical_answer3 == 'Yes' else 'N'),
                    ('ANS4', 'Y' if medical_answer4 == 'Yes' else 'N'),
                    ('queRemark1', ''),
                    ('queRemark2', ''),
                    ('queRemark3', ''),
                    ('queRemark4', ''),
                    ('Remarks', medical_remarks),
                    #('memDeductibleAmount', '0')         #NEW FIELD
                ])
            )
        if policy_code == 'Super Top Up':
            deductible = str(self.transaction.slab.deductible)
            for mem_dict in insured_list:
                mem_dict['memDeductibleAmount'] = deductible if this_sum_insured_type=='individual' else '0'
            if this_sum_insured_type == 'floater':
                proposer_object['floaterDeductibleAmount'] = deductible

        data = OrderedDict([
            ('requestId', ''),
            ('vendorTxnId', self.transaction.transaction_id),   #NEW FIELD
            ('sourceSystemId', SOURCE_ID),
            ('clientObj', client_object),
            ('propObj', proposer_object),
            ('memObjList' , insured_list),
        ])

        self.transaction.no_of_people_insured = len(self.transaction.insured_details_json['form_data']['insured'].keys())
        self.transaction.policy_start_date = datetime.datetime.today()
        self.transaction.policy_end_date = (datetime.datetime.today() + relativedelta.relativedelta(years=1)).date()
        self.transaction.save()

        print "DATA TO SEND", data
        #1/0

        return data


def tekprocess_send_request(payload_list):
    request_string = '|'.join(payload_list)
    hashed_string = hashlib.sha1(request_string).hexdigest()

    payload_with_sha1 = '%s|hash=%s' % (request_string, hashed_string)

    aes_cipher = AESCipher(tekprocess_credentials.encryption_key, tekprocess_credentials.iv)
    data_encrypted = aes_cipher.encrypt(payload_with_sha1)
    data_to_post = '%s|%s~' % (data_encrypted, tekprocess_credentials.merchant_code)

    client = Client(tekprocess_credentials.wsdl_url)

    get_token = client.service.getTransactionToken
    get_token.method.location = tekprocess_credentials.target_url

    tekprocess_response = get_token(data_to_post)
    return tekprocess_response



def post_transaction(transaction):
    transaction.reference_id = 'CFOX-%s-%s-%s' % (
        transaction.slab.health_product.insurer.id,
        transaction.id, datetime.datetime.now().strftime("%I%M%S")
    )

    payment_amount = '%.2f' % transaction.extra['plan_data']['premium']
    response_url = '%s/health-plan/16/transaction/%s/response/' % (settings.SITE_URL, transaction.transaction_id)

    request_params = [
        'rqst_type=T',
        'rqst_kit_vrsn=1.0.1',
        'tpsl_clnt_cd=%s' % tekprocess_credentials.merchant_code,
        'clnt_txn_ref=%s' % transaction.id,
        'clnt_rqst_meta={itc:email:%s}{email:%s}{mob:%s}{custname:%s}' % (
            transaction.proposer_email,
            transaction.proposer_email,
            transaction.proposer_mobile,
            transaction.proposer_first_name
        ),
        'rqst_amnt=%s' % payment_amount,
        'rqst_crncy=INR',
        'rtrn_url=%s' % response_url,
        's2s_url=%s' % response_url,
        'rqst_rqst_dtls=%s_%s_%s' % (tekprocess_credentials.scheme_code, payment_amount, tekprocess_credentials.commission_percent),
        'clnt_dt_tm=%s' % datetime.datetime.strftime(transaction.updated_on, '%d-%m-%Y'),
        'tpsl_bank_cd=NA'   #Previously was 470
    ]
    request_logger.info('\nORIENTAL TRANSACTION: %s \n PAYMENT REQUEST SENT:\n %s \n' % (transaction.transaction_id, request_params))
    transaction.payment_request = request_params
    tekprocess_response = tekprocess_send_request(request_params)
    if tekprocess_response.find('ERROR') >= 0:
        is_redirect = True
        redirect_url = '/health-plan/product/transaction/%s/failure/' % transaction.transaction_id
    else:
        is_redirect = False
        redirect_url = tekprocess_response
    template_name = ''
    processed_data = {}
    transaction.save()
    return is_redirect, redirect_url, template_name, processed_data


def post_payment(payment_response, transaction):
    aes_cipher = AESCipher(tekprocess_credentials.encryption_key, tekprocess_credentials.iv)
    data_encrypted = aes_cipher.decrypt(payment_response['msg'])
    split_data = data_encrypted.split('|')
    response_data = dict([(item.split('=')[0], item.split('=')[1]) for item in split_data])
    request_logger.info('\nORIENTAL TRANSACTION: %s \n PAYMENT RESPONSE RECEIVED:\n %s \n' % (transaction.transaction_id, response_data))
    transaction.extra['payment_response'] = response_data

    transaction.payment_response = json.dumps({'raw': payment_response, 'decrypted': data_encrypted})
    transaction.save()

    request_params = [
        'rqst_type=S',
        'rqst_kit_vrsn=1.0.1',
        'tpsl_clnt_cd=%s' % tekprocess_credentials.merchant_code,
        'clnt_txn_ref=%s' % response_data['clnt_txn_ref'],
        'rqst_amnt=%s' % response_data['txn_amt'],
        'tpsl_txn_id=%s' % response_data['tpsl_txn_id'],
        'clnt_dt_tm=%s' % response_data['tpsl_txn_time'].split(' ')[0]
    ]

    conf_response = tekprocess_send_request(request_params)
    transaction.extra['payment_confirmation'] = conf_response
    request_logger.info('\nORIENTAL TRANSACTION: %s \n PAYMENT CONFIRMATION RECEIVED:\n %s \n' % (transaction.transaction_id, conf_response))

    conf_split_data = conf_response.split('|')
    transaction.payment_token = response_data['tpsl_txn_id']

    is_redirect = True
    if response_data['txn_msg'].lower() == 'success':
        payment_confirmation = dict([(item.split('=')[0], item.split('=')[1]) for item in conf_split_data])
        transaction.extra['payment_confirmation'] = payment_confirmation
        redirect_url = '/health-plan/payment/transaction/%s/success/' % transaction.transaction_id
        transaction.premium_paid = str(float(response_data['txn_amt']))
        transaction.payment_done = True
        transaction.payment_on = datetime.datetime.today()
    else:
        redirect_url = '/health-plan/payment/transaction/%s/failure/' % transaction.transaction_id
        if 'txn_err_msg' in response_data:
            transaction.extra['error_msg'] = response_data['txn_err_msg']
    transaction.save()

    return is_redirect, redirect_url, '', {}
