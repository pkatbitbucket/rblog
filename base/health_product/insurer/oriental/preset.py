GENDER_OPTIONS = [
    {'id' : 1, 'name' : 'Male'},
    {'id' : 2, 'name' : 'Female'}
]
MARITAL_STATUS_OPTIONS = [
    {'id' : 'MARRIED', 'name' : 'Married'},
    {'id' : 'SINGLE', 'name' : 'Single'}
]
OCCUPATION_OPTIONS = [
    {'id' : 'Others', 'name' : 'Others'},
    {'id' : 'Business', 'name' : 'Business'},
    {'id' : 'Service', 'name' : 'Service'},
    {'id' : 'Housewife', 'name' : 'housewife'},
    {'id' : 'Retired', 'name' :'Retired'},
]
ANNUAL_INCOME_OPTIONS = [
    {'id' : 'Between 2 - 5 Lacs', 'name' : 'Between 2 - 5 Lacs'},
    {'id' : 'Between 5 - 10 Lacs', 'name' : 'Between 5 - 10 Lacs'},
    {'id' : 'Between 10 - 20 Lacs', 'name' : 'Between 10 - 20 Lacs'},
    {'id' : '20 Lacs and above', 'name' : '20 Lacs and above'},
    ]
pre_existing_options = [
    {'id' : 'NONE' , 'name' : 'None', 'is_correct' : True},
    {'id' : 'Anemia', 'name' : 'Anemia', 'is_correct' : False},
    {'id' : 'Bronchial Asthma', 'name' : 'Bronchial Asthma', 'is_correct' : False},
    {'id' : 'Cancer', 'name' : 'Cancer', 'is_correct' : False},
    {'id' : 'Cataract', 'name' : 'Cataract', 'is_correct' : False},
    {'id' : 'Cervical Spondylosis', 'name' : 'Cervical Spondylosis', 'is_correct' : False},
    {'id' : 'COPD', 'name' : 'COPD', 'is_correct' : False},
    {'id' : 'Dengue', 'name' : 'Dengue', 'is_correct' : False},
    {'id' : 'Diabetes', 'name' : 'Diabetes', 'is_correct' : False},
    {'id' : 'Dyslipidemia', 'name' : 'Dyslipidemia', 'is_correct' : False},
    {'id' : 'Epilepsy', 'name' : 'Epilepsy', 'is_correct' : False},
    {'id' : 'Fistula', 'name' : 'Fistula', 'is_correct' : False},
    {'id' : 'Heart Disease', 'name' : 'Heart Disease', 'is_correct' : False},
    {'id' : 'Hernia', 'name' : 'Hernia', 'is_correct' : False},
    {'id' : 'Hypertension', 'name' : 'Hypertension', 'is_correct' : False},
    {'id' : 'Hysterectomy', 'name' : 'Hysterectomy', 'is_correct' : False},
    {'id' : 'Ischemic Heart Disease', 'name' : 'Ischemic Heart Disease', 'is_correct' : False},
    {'id' : 'Jaundice', 'name' : 'Jaundice', 'is_correct' : False},
    {'id' : 'Kidney Stone', 'name' : 'Kidney Stone', 'is_correct' : False},
    {'id' : 'Liver function disorder', 'name' : 'Liver function disorder', 'is_correct' : False},
    {'id' : 'Myocardial Infraction', 'name' : 'Myocardial Infraction', 'is_correct' : False},
    {'id' : 'Orthopedic Fracture', 'name' : 'Orthopedic Fracture', 'is_correct' : False},
    {'id' : 'Ovarian cyst', 'name' : 'Ovarian cyst', 'is_correct' : False},
    {'id' : 'Paralysis', 'name' : 'Paralysis', 'is_correct' : False},
    {'id' : 'Piles', 'name' : 'Piles', 'is_correct' : False},
    {'id' : 'Polio', 'name' : 'Polio', 'is_correct' : False},
    {'id' : 'Rheumatoid arthritis', 'name' : 'Rheumatoid arthritis', 'is_correct' : False},
    {'id' : 'S.T.D.', 'name' : 'S.T.D.', 'is_correct' : False},
    {'id' : 'Sinus disorder', 'name' : 'Sinus disorder', 'is_correct' : False},
    {'id' : 'Thyroid disorder', 'name' : 'Thyroid disorder', 'is_correct' : False},
    {'id' : 'Tuberculosis', 'name' : 'Tuberculosis', 'is_correct' : False},
    {'id' : 'Varicose vein', 'name' : 'Varicose vein', 'is_correct' : False},
    {'id' : 'Other disease', 'name' : 'Other disease', 'is_correct' : False}
]

relation_options = [
    {'id' : 'Father', 'name' : 'Father'},
    {'id' : 'Mother', 'name' : 'Mother'},
    {'id' : 'Spouse', 'name' : 'Spouse'},
    {'id' : 'Son', 'name' : 'Son'},
    {'id' : 'Daughter', 'name' : 'Daughter'},
    {'id' : 'Mother In Law', 'name' : 'Mother In Law'},
    {'id' : 'Father In Law', 'name' : 'Father In Law'},
    {'id' : 'Sister', 'name' : 'Sister'},
    {'id' : 'Brother', 'name' : 'Brother'},
    #{'id' : 'Blood Relative', 'name' : 'Blood Relatives'},
    {'id' : 'Other', 'name' : 'Others'}
]

relation_options.sort(lambda x, y: -1 if x['name'] < y['name'] else 1)

