from wiki.models import FamilyCoverPremiumTable
from django.db.models import Q
from django.conf import settings

class PseudoQuerySet(list):
    def filter(self, *args, **kwargs):
        return self

class PseudoCoverPremiumTables:
    SERVICE_TAX = settings.SERVICE_TAX_PERCENTAGE / 100
    SECONDARY_AGES_DISCOUNT = 50
    REMAINING_AGES_DISCOUNT = 60

    def __init__(self, sl, cpts, data):
        self.data = data.copy()
        self.sl = sl

        self.max_age = self.data['age']
        self.data['kid_ages'] = [age * 1000 for age in self.data['kid_ages']]
        self.data['parent_ages'] = [age * 1000 for age in self.data['parent_ages']]
        self.data['you_and_spouse_ages'] = [age * 1000 for age in self.data['you_and_spouse_ages']]

        all_sorted_ages = sorted(self.data['kid_ages'] + self.data['parent_ages'] + self.data['you_and_spouse_ages'])
        self.primary_ages = all_sorted_ages[-2:]
        self.secondary_ages = all_sorted_ages[-3:-2]
        self.remaining_ages = all_sorted_ages[:-3]

        self.cpts_master = self.cpts_filter(cpts)

    def cpts_filter(self, cpts):
        return cpts if min(self.secondary_ages + self.primary_ages) < 60000 else PseudoQuerySet([])

    def get_mcpts(self, current_year_only = False):
        if current_year_only:
            return self.create_CoverPremiumTable(0)
        mcpts = PseudoQuerySet()
        for year_offset in range(0,5000,1000):
            cpt = self.create_CoverPremiumTable(year_offset)
            if cpt:
                mcpts.append(cpt)
        return mcpts

    def create_CoverPremiumTable(self, year_offset):
        primary_ages = map(lambda x:x+year_offset, self.primary_ages)
        secondary_ages = [x+year_offset if (x % 1000 == 0 or year_offset == 0) else year_offset for x in self.secondary_ages]
        remaining_ages = [x+year_offset if (x % 1000 == 0 or year_offset == 0) else year_offset for x in self.remaining_ages]
        premium = 0
        for primary_age in primary_ages:
            primary_cpt = self.cpts_master.filter(
                Q(adults=1)
                & Q(lower_age_limit__lte=primary_age)
                & Q(upper_age_limit__gte=primary_age)
            )
            if len(primary_cpt) == 0:
                return None
            if len(primary_cpt) > 1:
                raise Exception("Two matching conditions found in same slab %s, product: %s" % (self.sl.id, self.sl.health_product.title))
            primary_cpt = primary_cpt[0]
            premium += primary_cpt._premium

        for secondary_age in secondary_ages:
            secondary_cpt = self.cpts_master.filter(
                Q(adults=1)
                & Q(lower_age_limit__lte=secondary_age)
                & Q(upper_age_limit__gte=secondary_age)
            )
            if len(secondary_cpt) == 0:
                return None
            if len(secondary_cpt) > 1:
                raise Exception("Two matching conditions found in same slab %s, product: %s" % (self.sl.id, self.sl.health_product.title))
            secondary_cpt = secondary_cpt[0]
            premium += ((float)(100-self.SECONDARY_AGES_DISCOUNT)/100)*secondary_cpt._premium

        for remaining_age in remaining_ages:
            remaining_cpt = self.cpts_master.filter(
                Q(adults=1)
                & Q(lower_age_limit__lte=remaining_age)
                & Q(upper_age_limit__gte=remaining_age)
            )
            if len(remaining_cpt) == 0:
                return None
            if len(remaining_cpt) > 1:
                raise Exception("Two matching conditions found in same slab %s, product: %s" % (self.sl.id, self.sl.health_product.title))
            remaining_cpt = remaining_cpt[0]
            premium += ((float)(100-self.REMAINING_AGES_DISCOUNT)/100)*remaining_cpt._premium

        premium = round(premium)

        max_age = primary_ages[-1]
        return FamilyCoverPremiumTable(
                payment_period = 1,
                adults = self.data['adults'],
                kids = self.data['kids'],
                lower_age_limit = max_age,
                upper_age_limit = max_age,
                two_year_discount = primary_cpt.two_year_discount,
                _premium = premium
            )

def getCoverPremiumTables(sl, cpts, data):
    return PseudoCoverPremiumTables(sl, cpts, data).get_mcpts()


def getCoverPremiumTable(sl, data):
    cpts = FamilyCoverPremiumTable.objects.filter(slabs=sl, payment_period=1)
    mcpts_sl = FamilyCoverPremiumTable.objects.filter(
                    slabs=sl,
                    payment_period=1
            )
    mcpts_place_preference = mcpts_sl.filter(
            city = data['city']
    ) or mcpts_sl.filter(
            state__city = data['city']
    ) or mcpts_sl.filter(
            city__isnull=True,
            state__isnull=True
    )
    return PseudoCoverPremiumTables(sl, cpts, data).get_mcpts(current_year_only=True)
