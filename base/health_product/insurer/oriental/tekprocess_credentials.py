from django.conf import settings

# UAT CREDENTIALS
# iv = '9178318165MOHQMI'
# encryption_key = '3794438718HXQUVP'
# merchant_code = 'T3432'
# scheme_code = 'CIBPL'
# commission_percent = '0.0'
# wsdl_url = 'https://www.tekprocess.co.in/PaymentGateway/services/TransactionDetailsNew?wsdl'
# target_url = 'https://www.tekprocess.co.in/PaymentGateway/services/TransactionDetailsNew'
#
# sample_payload = "rqst_type=T|rqst_kit_vrsn=1.0.1|tpsl_clnt_cd=T3432|clnt_txn_ref=RC40000123456|clnt_rqst_meta={itc:email:myname@domain.com}{email:myname@domain.com}{mob:8451053257}{custname:Aniket}|rqst_amnt=2.00|rqst_crncy=INR|rtrn_url=https://www.coverfox.com/response/|s2s_url=https://www.coverfox.com/response/|rqst_rqst_dtls=CIBPL_2.0_0.0|clnt_dt_tm=19-02-2015|tpsl_bank_cd=470";
# sample_payment_response = {
#     'msg': 'WV+sKL87axE2FTWrJdYBn/dgxlqMNw+vYgLkyWvkgKWSt1TZeZgYW7amXAfZbJ1VL/o398QLEQ1Z\r\naP0Y8uKj+rSNLynP1wxdn/6BLANtCF3giJfDlNhzUAcyNLWvKH9IFOLcsI9OpkZvIg6imMAQJEXd\r\n6kkN9R9Zj750xDZyUZIfSvHKfudb7Pm5QYLQSX3WATCt6FdwwrH8OuR27c0CfyV3ueYUsZvTXnFl\r\nXJ9ChdCJeCzjz1T4ta3fyfvmKitAvA4dd19oTDlQu0ilPtPMViCYvsAYcccIy3fMr7OE7Sf1MLFt\r\n2viEh2RkzA1GcS427KuWGIEgJMC+DXLI7ZQ0OzXDCSkwvJ7TETgpYCQH2CqCsuA1ynq5akZb2WCa\r\nHf6oE4dm08njtVXPJC899+iPRMpNwZqMDQPgt9hiFxeRuO2bK7m31dy5Zlas1kYRypJv2sdJy7F1\r\nIBEnB7JF+hXE0PLpF72efoY9beY1hBndTKxUa5GYyJ5GwcheivdsnnQmcGtv6JSv01qdcSInO+df\r\nbw==',
#     'tpsl_mrct_cd': 'T3432'
# }


#PRODUCTION CREDENTIALS
if settings.PRODUCTION:
    iv = '8072212186MNRDEI'
    encryption_key = '2213468353JPWPCR'
    merchant_code = 'L4808'
else:
    iv = '1538689848QFNHWX'
    encryption_key = '5461924290GDOSUC'
    merchant_code = 'T4808'

scheme_code = 'ORIENTAL'
commission_percent = '0.0'
wsdl_url = 'https://www.tpsl-india.in/PaymentGateway/services/TransactionDetailsNew?wsdl'
target_url = 'https://www.tpsl-india.in/PaymentGateway/services/TransactionDetailsNew'