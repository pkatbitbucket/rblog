import datetime
from utils import request_logger
from suds.client import Client
from django.conf import settings
from django.utils import timezone
from dateutil.relativedelta import relativedelta
from health_product.insurer.reliance import preset
from health_product.insurer import custom_utils


TPA_CODE1 = 'TPA000001'
TPA_NAME1 = 'Medi Assist India Pvt. Ltd.'
TPA_CODE2 = 'TPA000002'
TPA_NAME2 = 'Paramount Health Services Pvt Ltd'
TPA_CODE = 'TPA000003'
TPA_NAME3 = 'E Meditek Solutions'



#=============TEST CREDENTIALS=================
#RELIANCE_USERNAME = '100001'
#RELIANCE_PASSWORD = 'admin1'
#WSDL_URL = 'http://rgiclservices.reliancegeneral.co.in/RGICL_ExternalServices/RGICLRelianceHealth_WCF.svc?wsdl'
#REDIRECTION_URL = 'http://rgiclservices.reliancegeneral.co.in/RGICLWCF_POLICY/CommonPaymentRequest.aspx'
#==============================================

#=============PRODUCTION CREDENTIALS===========
PORTAL_ID = '11BRG378'
RELIANCE_USERNAME = '11BRG378B01'
RELIANCE_PASSWORD = 'U9849g'

WSDL_URL = 'http://rgiclservices.reliancegeneral.co.in/RGICL_OnlineServices/RGICLRelianceHealth_WCF.svc?wsdl'
REDIRECTION_URL = 'http://rgiclservices.reliancegeneral.co.in/RGICL_OnlineViewPolicy/CommonPaymentRequest.aspx'
#==============================================


def calculate_premium(transaction):
    client = Client(WSDL_URL)
    #NOTE : For arrays in the factory class, do not override, use append instead
    req_rhpcd = client.factory.create('ns0:Req_RelianceHealthwisePremiumCalcDC')
    #print req_rhpcd to see the content of the class created by the factory
    req_rhpcd.PlanName = 'Standard'
    req_rhpcd.PlanSumInsured = transaction.slab.sum_assured
    req_rhpcd.NoOfInsured = len(transaction.extra['members'])
    max_db = None
    for code, fdata in transaction.insured_details_json['form_data']['insured'].items():
        if not max_db:
            max_db = datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y')
        else:
            ex_db = datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y')
            if relativedelta(max_db, ex_db).days > 0:
                max_db = ex_db
    req_rhpcd.MaxInsuredDateOfBirth = datetime.datetime.strftime(max_db, '%d-%b-%Y')
    resp_rhpcd = client.service.getRelianceHealthwisePremium(req_rhpcd)
    return resp_rhpcd


class Processor():
    def __init__(self, transaction):
        self.transaction = transaction

    def get_processed_data_healthwise_individual(self):
        client = Client(WSDL_URL)

        #For ns0:Req_LoginDetails objUserDetails
        login_details = client.factory.create('ns0:Req_LoginDetails')
        login_details.UserId = RELIANCE_USERNAME
        login_details.Password = RELIANCE_PASSWORD


        pdata = self.transaction.insured_details_json['form_data']['proposer']['personal']
        add_data = self.transaction.insured_details_json['form_data']['proposer']['address']
        contact_data = self.transaction.insured_details_json['form_data']['proposer']['contact']

        if 'date_of_birth' in pdata.keys():
            self.transaction.proposer_date_of_birth = datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y')
        if 'gender' in pdata.keys():
            self.transaction.proposer_gender = int(pdata['gender']['value'])
        self.transaction.save()

        #For ns0:Req_RelianceHealthwisePremiumCalcDC objPremiumCalDC
        cal_premium = client.factory.create('ns0:Req_RelianceHealthwisePremiumCalcDC')
        cal_premium.PlanName = 'Standard'
        cal_premium.PlanSumInsured = self.transaction.slab.sum_assured
        cal_premium.NoOfInsured = 1
        cal_premium.MaxInsuredDateOfBirth = datetime.datetime.strftime(self.transaction.proposer_date_of_birth, '%d-%b-%Y')


        #For ns0:Req_RelianceHealthwiseCoverageDC objRelianceHealthwiseCoverageDC
        data = client.factory.create('ns0:Req_RelianceHealthwiseCoverageDC')
        data.ProposerPrefix = preset.SALUTATION_CHOICES[int(self.transaction.proposer_gender)]
        data.ProposerOccupation = pdata['occupation']['value']
        data.ProposerFirstName = self.transaction.proposer_first_name
        data.ProposerMiddleName = self.transaction.proposer_middle_name
        data.ProposerLastName = self.transaction.proposer_last_name

        counter = 0
        for code, fdata in self.transaction.insured_details_json['form_data']['insured'].items():
            ndata = self.transaction.insured_details_json['form_data']['nominee'][code]
            member = [m for m in self.transaction.extra['members'] if m['id'] == code][0]
            counter += 1
            setattr(data, 'Insured%sName' % counter, custom_utils.get_full_name(
                first_name=fdata['first_name']['value'],
                middle_name='',
                last_name=fdata['last_name']['value'])
            )
            setattr(data, 'Insured%sRelationWithProposer' % counter, member['person'])
            setattr(data, 'Insured%sGender' % counter, preset.GENDER_CHOICES[member['gender']])
            setattr(data, 'Insured%sDateOfBirth' % counter, datetime.datetime.strftime(datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y'), '%d-%b-%Y'))
            setattr(data, 'Insured%sHasPreExistingIllness' % counter, 'False')
            setattr(data, 'Insured%sNominee' % counter, custom_utils.get_full_name(
                first_name=ndata['first_name']['value'],
                last_name=ndata['last_name']['value'])
            )
            setattr(data, 'Insured%sRelationWithNominee' % counter, ndata['relation']['value'].lower())
        data.PolicyStartDt = datetime.datetime.strftime((datetime.datetime.today() + relativedelta(days=3)).date(), '%d-%b-%Y')
        data.PolicyEndDt = datetime.datetime.strftime((datetime.datetime.today() + relativedelta(days=2, years=1)).date(), '%d-%b-%Y')

        data.TPAName = TPA_CODE1
        data.AgreeTermsConditions = 'True'

        #For ns0:Req_AddressDC objRelianceHealthwiseCommunicationAddrDC
        address_data = client.factory.create('ns0:Req_AddressDC')
        address_data.AddressLine1 = self.transaction.proposer_address1
        address_data.AddressLine2 = self.transaction.proposer_address2
        address_data.AddressLine3 = self.transaction.proposer_address_landmark
        address_data.State = add_data['state']['value']
        address_data.PinCode = int(self.transaction.proposer_pincode)
        address_data.City = add_data['city']['value']
        address_data.Country = 'India'
        if self.transaction.proposer_landline:
            address_data.PhoneNo = int(self.transaction.proposer_landline)
        address_data.MobileNo = int(self.transaction.proposer_mobile)
        address_data.EmailId = self.transaction.proposer_email

        return login_details, cal_premium, data, address_data


    def get_processed_data_healthwise_floater(self):
        client = Client(WSDL_URL)

        #For ns0:Req_LoginDetails objUserDetails
        login_details = client.factory.create('ns0:Req_LoginDetails')
        login_details.UserId = RELIANCE_USERNAME
        login_details.Password = RELIANCE_PASSWORD

        pdata = self.transaction.insured_details_json['form_data']['proposer']['personal']
        add_data = self.transaction.insured_details_json['form_data']['proposer']['address']
        contact_data = self.transaction.insured_details_json['form_data']['proposer']['contact']

        if 'date_of_birth' in pdata.keys():
            self.transaction.proposer_date_of_birth = datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y')
        if 'gender' in pdata.keys():
            self.transaction.proposer_gender = int(pdata['gender']['value'])
        self.transaction.save()

        #For ns0:Req_RelianceHealthwisePremiumCalcDC objPremiumCalDC
        cal_premium = client.factory.create('ns0:Req_RelianceHealthwisePremiumCalcDC')
        cal_premium.PlanName = 'Standard'
        cal_premium.PlanSumInsured = self.transaction.slab.sum_assured
        cal_premium.NoOfInsured = len(self.transaction.extra['members'])


        max_db = None
        for code, fdata in self.transaction.insured_details_json['form_data']['insured'].items():
            if not max_db:
                max_db = datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y')
            else:
                ex_db = datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y')
                if relativedelta(max_db, ex_db).days > 0:
                    max_db = ex_db

        cal_premium.MaxInsuredDateOfBirth = datetime.datetime.strftime(max_db, '%d-%b-%Y')

        #For ns0:Req_RelianceHealthwiseCoverageDC objRelianceHealthwiseCoverageDC
        data = client.factory.create('ns0:Req_RelianceHealthwiseCoverageDC')
        data.ProposerPrefix = preset.SALUTATION_CHOICES[int(self.transaction.proposer_gender)]
        data.ProposerFirstName = self.transaction.proposer_first_name
        data.ProposerOccupation = pdata['occupation']['value']
        data.ProposerMiddleName = self.transaction.proposer_middle_name
        data.ProposerLastName = self.transaction.proposer_last_name

        counter = 0
        for code, fdata in self.transaction.insured_details_json['form_data']['insured'].items():
            ndata = self.transaction.insured_details_json['form_data']['nominee'][code]
            member = [m for m in self.transaction.extra['members'] if m['id'] == code][0]
            counter += 1
            setattr(data, 'Insured%sName' % counter, custom_utils.get_full_name(
                first_name=fdata['first_name']['value'],
                middle_name='',
                last_name=fdata['last_name']['value'])
            )
            setattr(data, 'Insured%sRelationWithProposer' % counter, member['person'])
            setattr(data, 'Insured%sGender' % counter, preset.GENDER_CHOICES[member['gender']])
            setattr(data, 'Insured%sDateOfBirth' % counter, datetime.datetime.strftime(datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y'), '%d-%b-%Y'))
            setattr(data, 'Insured%sHasPreExistingIllness' % counter, 'False')
            setattr(data, 'Insured%sNominee' % counter, custom_utils.get_full_name(
                first_name=ndata['first_name']['value'],
                last_name=ndata['last_name']['value'])
            )
            setattr(data, 'Insured%sRelationWithNominee' % counter, ndata['relation']['value'].lower())

        data.PolicyStartDt = datetime.datetime.strftime((datetime.datetime.today() + relativedelta(days=3)).date(), '%d-%b-%Y')
        data.PolicyEndDt = datetime.datetime.strftime((datetime.datetime.today() + relativedelta(days=2, years=1)).date(), '%d-%b-%Y')
        data.TPAName = TPA_CODE1
        data.AgreeTermsConditions = 'True'

        #For ns0:Req_AddressDC objRelianceHealthwiseCommunicationAddrDC
        address_data = client.factory.create('ns0:Req_AddressDC')
        address_data.AddressLine1 = self.transaction.proposer_address1
        address_data.AddressLine2 = self.transaction.proposer_address2
        address_data.AddressLine3 = self.transaction.proposer_address_landmark
        address_data.State = add_data['state']['value']
        address_data.PinCode = int(self.transaction.proposer_pincode)
        address_data.City = add_data['city']['value']
        address_data.Country = 'India'
        if self.transaction.proposer_landline:
            address_data.PhoneNo = int(self.transaction.proposer_landline)
        address_data.MobileNo = int(self.transaction.proposer_mobile)
        address_data.EmailId = self.transaction.proposer_email
        return login_details, cal_premium, data, address_data



def post_transaction(transaction):
    redirect_url = '/health-plan/product/transaction/%s/failure/' % transaction.transaction_id
    is_redirect = True

    if transaction.slab.health_product.id in [35]:
        policy_code = 'healthwise_individual'
    elif transaction.slab.health_product.id in [38]:
        policy_code = 'healthwise_floater'

    obj = Processor(transaction)
    func = getattr(obj, 'get_processed_data_%s' % policy_code.lower(), '')
    login_details, cal_premium, data, address_data = func()

    # try:
    if True:
        client = Client(WSDL_URL)
        resp = client.service.setRelianceHealthwisePolicyDetails(login_details, cal_premium, data, address_data)
        print 'RESPONSE', resp
        request_logger.info('\nRELIANCE TRANSACTION: %s \n XML REQUEST SENT:\n %s \n' % (transaction.transaction_id, client.last_sent()))
        request_logger.info('\nRELIANCE TRANSACTION: %s \n XML RESPONSE RECEIVED:\n %s \n' % (transaction.transaction_id, client.last_received()))
        transaction.payment_request = client.last_sent()
        if resp.ErrorDescription:
            error = [e for e in resp.ErrorDescription]
            transaction.extra['error_msg'] = '|'.join([e for e in resp.ErrorDescription[0]])
            transaction.save()
        else:
            product_response = {
                'ClientPremiumAmount': resp.ClientPremiumAmount,
                'PaymentURL': resp.PaymentURL,
                'PremiumAmount': resp.PremiumAmount,
                'ProposalNo': resp.ProposalNo,
                'Status': resp.Status,
                'TokenNo': resp.TokenNo,
            }
            transaction.premium_paid = product_response['PremiumAmount']
            transaction.reference_id = 'CFOX-%s-%s' % (transaction.slab.health_product.insurer.id, transaction.id)
            transaction.policy_token = product_response['TokenNo']
            transaction.extra['product_response'] = product_response
            transaction.save()
            post_data = [
                product_response['ProposalNo'], RELIANCE_USERNAME,
                '%s/health-plan/10/transaction/%s/response/' % (settings.SITE_URL, transaction.transaction_id),
                product_response['TokenNo'],
                RELIANCE_PASSWORD
            ]
            data = {'message': '|'.join(post_data)}
            is_redirect = False
            redirect_url = REDIRECTION_URL
        return is_redirect, redirect_url, 'health_product/post_transaction_data.html', data
    # except urllib2.HTTPError as err:
    else:
        # print ">>>>>>>>>", err, err.read()
        return is_redirect, redirect_url, '', {}



def post_payment(payment_response, transaction):
    transaction.payment_response = payment_response
    redirect_url = '/health-plan/payment/transaction/%s/failure/' % transaction.transaction_id
    is_redirect = True
    #policy_code = transaction.slab.health_product.policy_code
    #Status|PolicyNo|ErrorCode|ErrorMessage|ScheduleURL|OtherInput
    resp = payment_response['Message'].split('|')

    transaction.extra['error_msg'] = resp[3];

    transaction.extra['payment_response'] = {
        'status' : resp[0],
        'policy_no' : resp[1],
        'insurer_policy_url' : resp[4],
        'error_code' : resp[2],
        'error_msg' : resp[3]
    }
    transaction.policy_number = transaction.extra['payment_response']['policy_no']

    if transaction.extra['payment_response']['status'] == 'SUCCESS':
        redirect_url = '/health-plan/payment/transaction/%s/success/' % transaction.transaction_id
        transaction.payment_done = True
        transaction.payment_on = timezone.now()
        transaction.policy_start_date = timezone.now() + relativedelta(days=1)
        transaction.policy_end_date = timezone.now() + relativedelta(days=-1, years=1)
    transaction.save()
    return is_redirect, redirect_url, '', {}
