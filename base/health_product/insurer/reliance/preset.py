from health_product.insurer import settings as insurer_settings
from health_product.models import *
from wiki.models import *
import datetime
from dateutil.relativedelta import *


SALUTATION_CHOICES = {
    1: 'Mr.',
    2: 'Ms.'
}

GENDER_OPTIONS = [
    {'id' : 1, 'name' : 'Male'},
    {'id' : 2, 'name' : 'Female'}
]

GENDER_CHOICES = {
    1: 'Male',
    2: 'Female'
}


MARITAL_STATUS_OPTIONS = [
    {'id' : 'SINGLE', 'name': 'Single'},
    {'id' : 'MARRIED', 'name': 'Married'}
]

OCCUPATION_OPTIONS = [
    {'id': 'Retired', 'name': 'Retired'},
    {'id': 'Executive - Senior Level', 'name': 'Executive - Senior Level'},
    {'id': 'Executive - Middle Level', 'name': 'Executive - Middle Level'},
    {'id': 'Executive Junior Level', 'name': 'Executive Junior Level'},
    {'id': 'Software Professional', 'name': 'Software Professional'},
    {'id': 'Supervisor', 'name': 'Supervisor'},
    {'id': 'Clerical', 'name': 'Clerical'},
    {'id': 'Salesperson', 'name': 'Salesperson'},
    {'id': 'Self employed Professional', 'name': 'Self employed Professional'},
    {'id': 'Shop owner', 'name': 'Shop owner'},
    {'id': 'Student', 'name': 'Student'},
    {'id': 'Not Working', 'name': 'Not Working'},
    {'id': 'Housewife', 'name': 'Housewife'},
    {'id': 'Advocate / Lawyer', 'name': 'Advocate / Lawyer'},
    {'id': 'Chartered Accountants', 'name': 'Chartered Accountants'},
    {'id': 'Labourer', 'name': 'Labourer'},
    {'id': 'Farming', 'name': 'Farming'},
    {'id': 'Service', 'name': 'Service'},
    {'id': 'Businessman/Industrialist Large Scale', 'name': 'Businessman/Industrialist Large Scale'},
    {'id': 'Businessman/Industrialist Medium Scale', 'name': 'Businessman/Industrialist Medium Scale'},
    {'id': 'Businessman/Industrialist Small Scale', 'name': 'Businessman/Industrialist Small Scale'},
    {'id': 'Reinforcing Fitter', 'name': 'Reinforcing Fitter'},
    {'id': 'Carpenter', 'name': 'Carpenter'},
    {'id': 'Steel Fixer', 'name': 'Steel Fixer'},
    {'id': 'Mason', 'name': 'Mason'},
    {'id': 'Cook', 'name': 'Cook'},
    {'id': 'Others', 'name': 'Others'}
]


# OCCUPATION_MASTER = {
#     1: 'Retired',
#     2: 'Executive - Senior Level',
#     3: 'Executive - Middle Level',
#     4: 'Executive Junior Level',
#     5: 'Software Professional',
#     6: 'Supervisor',
#     7: 'Clerical',
#     8: 'Salesperson',
#     9: 'Self employed Professional',
#     10: 'Shop owner',
#     11: 'Student',
#     12: 'Not Working',
#     13: 'Housewife',
#     14: 'Advocate / Lawyer',
#     15: 'Chartered Accountants',
#     16: 'Labourer',
#     17: 'Farming',
#     18: 'Service',
#     19: 'Businessman/Industrialist Large Scale',
#     20: 'Businessman/Industrialist Medium Scale',
#     21: 'Businessman/Industrialist Small Scale',
#     22: 'Reinforcing Fitter',
#     23: 'Carpenter',
#     24: 'Steel Fixer',
#     25: 'Mason',
#     26: 'Cook',
#     27: 'Others'
# }
# GENDER_CHOICES = {
#     1 : 'Male',
#     2 : 'Female'
# }
#
# MARITAL_STATUS_CHOICES = {
#     'SINGLE' : 'Single',
#     'MARRIED' : 'Married',
#     }


# OCCUPATION_CHOICES = {
#     'PVT' : 3,
#     'GOVT' : 7,
#     'ARMED_FORCES' : 27,
#     'SELF' : 9,
#     'OTHERS' : 27
# }
#
# RELATIONSHIP_MASTER = {
#     1: 'Father',
#     2: 'Spouse',
#     3: 'Son',
#     4: 'Daughter',
#     5: 'Brother',
#     6: 'Sister',
#     7: 'Others'
# }
RELATIONSHIP_CHOICES = {
    'self' : 1,
    'spouse' : 2,
    'son' : 3,
    'daughter' : 4,
    'father' : 1,
    'mother' : 7
}


# STATE_MASTER = {
#     1: 'Andaman And Nicobar Islands',
#     2: 'Andhra Pradesh',
#     3: 'ARUNACHAL PRADESH',
#     4: 'ASSAM',
#     5: 'BIHAR',
#     6: 'Chandigarh U.T.',
#     7: 'Chhattisgarh',
#     8: 'Dadra and Nagar Haveli',
#     9: 'Daman and Diu',
#     10: 'DELHI',
#     11: 'Goa',
#     12: 'GUJARAT',
#     13: 'HARYANA',
#     14: 'HIMACHAL PRADESH',
#     15: 'Jammu and Kashmir',
#     16: 'JHARKHAND',
#     17: 'KARNATAKA',
#     18: 'Kerala',
#     19: 'Lakshadweep U.T.',
#     20: 'MADHYA PRADESH',
#     21: 'MAHARASHTRA',
#     22: 'Manipur',
#     23: 'MIZORAM',
#     24: 'NAGALAND',
#     25: 'ODISHA',
#     26: 'Pondicherry U.T.',
#     27: 'PUNJAB',
#     28: 'RAJASTHAN',
#     29: 'SIKKIM',
#     30: 'TAMIL NADU',
#     31: 'TRIPURA',
#     32: 'UTTAR PRADESH',
#     33: 'UTTARAKHAND',
#     34: 'WEST BENGAL',
#     35: 'Unknown',
#     36: 'MEGHALAYA'
#
# }

STATE_CHOICES = {
    1: '1',
    2: '2',
    3: '3',
    4: '4',
    5: '5',
    6: '6',
    7: '7',
    8: '8',
    9: '9',
    10: '10',
    11: '11',
    12: '12',
    13: '13',
    14: '14',
    15: '15',
    16: '16',
    17: '17',
    18: '18',
    19: '19',
    20: '20',
    21: '21',
    22: '22',
    24: '23',
    25: '24',
    26: '25',
    27: '26',
    28: '27',
    29: '28',
    30: '29',
    31: '30',
    32: '31',
    33: '32',
    34: '33',
    35: '34',
    23: '36'
}


