from wiki.models import City, Pincode, Slab
import datetime
from dateutil.relativedelta import relativedelta
from health_product.insurer import custom_utils
from copy import deepcopy
from health_product.services.match_plan import MatchPlan
import health_product.queries as hqueries

def get_premium_details(data,slab_id):
    discount = {
            1 : 0,
            2 : 7.5,
            3 : 10.0,
            }
    base_premiums = {}
    discounted_premiums = {}
    cpt_ids = {}

    slab = Slab.objects.get(id = slab_id)

    data_to_score = hqueries.prepare_post_data_to_score(data)
    mp = MatchPlan(data_to_score,slab = slab, request = None)
    cpt = mp.get_cpt()
    base_premiums[1] = cpt.premium*1
    base_premiums[2] = cpt.premium*2
    base_premiums[3] = cpt.premium*3

    for k,v in base_premiums.items():
        discounted_premiums[k] = round( v * (1 - discount[k]/100) )

    cpt_ids[1] = cpt.id
    cpt_ids[2] = cpt.id
    cpt_ids[3] = cpt.id

    return {
            'base_premiums' : base_premiums,
            'discounted_premiums' : discounted_premiums,
            'cpt_ids' : cpt_ids,
            }

def get_discounted_premiums(data,slab_id):
    slab = Slab.objects.get(id = slab_id)

class Formic():
    GENDER_OPTIONS = [
        {'id' : 1, 'name' : 'Male'},
        {'id' : 2, 'name' : 'Female'}
    ]
    MARITAL_STATUS_OPTIONS = [
        {'id' : 'MARRIED', 'name' : 'Married'},
        {'id' : 'SINGLE', 'name' : 'Single'}
    ]
    POLICY_TERMS = [
            {'id':1, 'name':'1 year'},
            {'id':2, 'name':'2 years'},
            {'id':3, 'name':'3 years'}
    ]
    POLICY_DISCOUNTS = [
            {'id':1, 'name':'0'},
            {'id':2, 'name':'7.5'},
            {'id':3, 'name':'10'},
    ]
    def __init__(self, transaction, is_self_present):
        self.transaction = transaction
        self.is_self_present = is_self_present
        self.proposer_data = {
            'pincode': transaction.extra['pincode'],
            'city' : City.objects.get(id=transaction.extra['city']['id']).name,
            'state' : City.objects.get(id=transaction.extra['city']['id']).state.name,
            'email': ''
        }
        self.month_range = []
        for i in range(1, 13):
            self.month_range.append({'id' : i, 'name' : i})



    def add_conditional_segment(self, segment_id, tag_id, parent_question_code, details, question_type):
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : details, 'data' : []}
        row = []
        if question_type == 'MultipleChoiceField':
            new_segment['details']['tier'] = 1
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='MultipleChoiceField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    details=member,
                    value='No',
                    label=details.get('question_text', ''),
                    span=4,
                    required=False,
                    css_class='bottom-margin',
                    event=', click : $parent.root_context.stage_4.any_yes_check',
                    only_if='root_context.stage_4.check_dict.get(\'%s\')' % parent_question_code,
                    template_option='question'
                ))
            new_segment['data'].append(row)
        elif question_type == 'TwoDropDownField':
            new_segment['details']['tier'] = 2
            for member in self.transaction.extra['members']:
                year_range = []
                for i in range((datetime.datetime.now() - relativedelta(years=member['age'])).year, datetime.datetime.today().year +1):
                    year_range.append({'id' : i, 'name' : i})
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='TwoDropDownField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    options1=self.month_range,
                    options2=year_range,
                    value='No',
                    label=details.get('question_text', ''),
                    span=2,
                    dependant_parent='%s_question_%s' % (member['id'], parent_question_code),
                    required=False,
                    only_if='root_context.stage_4.check_dict.get(\'%s_question_%s\')' % (member['id'], parent_question_code),
                    template_option='question'
                ))
            new_segment['data'].append(row)
        elif question_type == 'TextField':
            new_segment['details']['tier'] = 2
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='TextField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    span=2,
                    label=details.get('question_text', ''),
                    dependant_parent='%s_question_%s' % (member['id'], parent_question_code),
                    required=False,
                    only_if='root_context.stage_4.check_dict.get(\'%s_question_%s\')' % (member['id'], parent_question_code),
                ))
            new_segment['data'].append(row)
        return new_segment


    def stage_1(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'personal'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            label='First Name',
            code='first_name'
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            label='Last Name',
            code='last_name'
        ))
        if self.is_self_present:
            member = [m for m in self.transaction.extra['members'] if m['id'] == 'self'][0]
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                span=3,
                min_age=18,
                max_age=100,
                default_age=member['age'],
            ))
        else:
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                min_age=18,
                max_age=100,
                default_age=18,
            ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Gender',
            code='gender',
            value=1 if self.transaction.extra['gender'] == 'MALE' else 2,
            options=self.GENDER_OPTIONS
        ))

        married = True if [m for m in self.transaction.extra['members'] if m['id'] == 'spouse'] else False;
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Marital Status',
            code='marital_status',
            options=self.MARITAL_STATUS_OPTIONS,
            value = 'MARRIED' if married or len(self.transaction.extra['members']) > 1 else None,
            template_option= 'static' if married else 'dynamic',
        ))
        new_segment['data'].append(row)

        premium_data = get_premium_details(deepcopy(self.transaction.extra),self.transaction.slab_id)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='PremiumDiscountField',
            code='policy_term',
            span=6,
            options1=self.POLICY_TERMS,
            options2=self.POLICY_DISCOUNTS,
            base_premiums = premium_data['base_premiums'],
            discounted_premiums = premium_data['discounted_premiums'],
            required=True,
            main_label='Policy term',
            dep_label='Discount',
            dep_label1='Premium',
            premium=self.transaction.extra['plan_data']['premium'],
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template': 'stage_1', 'data': stage}

    def stage_2(self):
        stage = []
        for member in self.transaction.extra['members']:
            segment_id = 'insured'
            tag_id = member['id']
            template_switch = 'dynamic'
            if tag_id == 'self':
                template_switch = 'static'
            try:
                saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : member, 'data' : []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                label='First Name',
                code='first_name',
                template_option=template_switch,
                max_length=50,
                span=3
            ))
            # row.append(custom_utils.new_field(
            #     saved_data=saved_data,
            #     field_type='CharField',
            #     label='Middle Name',
            #     code='middle_name',
            #     max_length=50,
            #     span=3,
            #     required=False
            # ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                label='Last Name',
                code='last_name',
                template_option=template_switch,
                max_length=50,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                span=3,
                template_option=template_switch,
                default_age = member['age'],
                min_age = 18 if member['type']=='adult' else 0,
                max_age = 100,
            ))
            new_segment['data'].append(row)
            stage.append(new_segment)
        return {'template': 'stage_2', 'data': stage}

    def stage_3(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'address'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address1',
            label='Flat/House No, Street',
            span=12,
            css_class='bottom-margin',
            max_length=50
        ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address2',
            label='Area/Locality',
            span=12,
            css_class='bottom-margin',
            max_length=50,
            ))
        new_segment['data'].append(row)
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='landmark',
            label='Landmark',
            span=12,
            css_class='bottom-margin',
            max_length=50,
            required=False
        ))
        new_segment['data'].append(row)
        if self.transaction.slab.health_product.coverage_category == 'SUPER_TOPUP':
            row = []
            pincode = Pincode.objects.filter(pincode=self.transaction.extra['pincode'])[0]
            state = pincode.state
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                code='pincode',
                label='Pincode',
                span=4,
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                code='state',
                label='State',
                span=4,
                template_option='static'
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='SearchableDropDownField',
                code='city',
                label='City',
            ))
            new_segment['data'].append(row)
        else:
            row = []
            pincode = Pincode.objects.filter(pincode=self.transaction.extra['pincode'])[0]
            state = pincode.state
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='SearchableDropDownField',
                code='city',
                label='City',
                options=[{'id': c.name, 'name': c.name} for c in state.city_set.all()]
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                code='state',
                label='State',
                value=self.proposer_data['state'],
                span=4,
                template_option='static'
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                code='pincode',
                label='Pincode',
                value=str(self.proposer_data['pincode']),
                span=4,
                template_option='static'
            ))
            new_segment['data'].append(row)

        stage.append(new_segment)

        tag_id = 'contact'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='email',
            label='Email',
            span=12,
            css_class='bottom-margin',
            email=True
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MobileField',
            code='mobile',
            label='Mobile',
            css_class='bottom-margin',
            span=12
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='LandlineField',
            code='landline',
            label='Landline',
            span=12,
            css_class='bottom-margin',
            required=False
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template': 'stage_3', 'data': stage}

    def stage_4(self):
        stage = []

        #---------- Question Unit Begins -----------
        #THINGS THAT MATTER - START
        question_code = 'pre_existing'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'combo'
        details = {
            'conditional': 'NO',
            'trigger_any_yes_check': True
        }
        #THINGS THAT MATTER - END

        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': details, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='%s_question_%s' % (tag_id, question_code),
            label='Does any person(s) to be insured has any Pre-existing diseases?',
            helptext='Please select "Yes" if any person(s) to be insured has Diabetes, Hypertension,'
                     'Liver Disease, Cancer, Cardiac Disease, Joint Pain, Kidney Disease, Paralysis, '
                     'Congenital Disorder, HIV/AIDS. This is vital for correct Claim dispursal.',
            span=12,
            css_class='bottom-margin',
            event=', click : $parent.root_context.stage_4.any_yes_check',
            value='No',
            template_option='question'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)


        #CONDITIONAL SEGMENT
        #Conditional Question 1
        # tag_id = 'conditional_pre_existing'
        # parent_question_code = question_code
        # details = {
        #     'conditional': 'YES',
        #     'parent_question_code': parent_question_code,
        #     'question_text': 'Does any person(s) to be insured has any Pre-existing diseases?',
        #     'trigger_any_yes_check': False,
        #     'show_header': True
        # }
        # stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))


        #Conditional Question 2
        tag_id = 'conditional_diabetes'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Diabetes',
            'trigger_any_yes_check': True,
            'show_header': True
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))


        #Conditional Question 3
        tag_id = 'conditional_diabetes_since'
        parent_question_code = 'conditional_diabetes'
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Existing Since',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TwoDropDownField'))


        #Conditional Question 4
        tag_id = 'conditional_hypertension'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Hypertension / High Blood Pressure',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))


        #Conditional Question 5
        tag_id = 'conditional_hypertension_since'
        parent_question_code = 'conditional_hypertension'
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Existing Since',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TwoDropDownField'))


        #Conditional Question 6
        tag_id = 'conditional_respiratory_disorder'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Respiratory Disorder Inclusion?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))

        #Conditional Question 7
        tag_id = 'conditional_respiratory_disorder_since'
        parent_question_code = 'conditional_respiratory_disorder'
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Existing Since',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TwoDropDownField'))


        #Conditional Question 8
        tag_id = 'conditional_hiv'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'HIV / AIDS / STD',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))


        #Conditional Question 9
        tag_id = 'conditional_hiv_since'
        parent_question_code = 'conditional_hiv'
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Existing Since',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TwoDropDownField'))


        #Conditional Question 10
        tag_id = 'conditional_liver_disease'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Liver disease?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))


        #Conditional Question 11
        tag_id = 'conditional_liver_disease_since'
        parent_question_code = 'conditional_liver_disease'
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Existing Since',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TwoDropDownField'))


        #Conditional Question 12
        tag_id = 'conditional_cancer'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Cancer / Tumor?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))


        #Conditional Question 13
        tag_id = 'conditional_cancer_since'
        parent_question_code = 'conditional_cancer'
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Existing Since',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TwoDropDownField'))


        #Conditional Question 14
        tag_id = 'conditional_cardiac'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Cardiac Disease?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))


        #Conditional Question 15
        tag_id = 'conditional_cardiac_since'
        parent_question_code = 'conditional_cardiac'
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Existing Since',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TwoDropDownField'))


        #Conditional Question 16
        tag_id = 'conditional_arthritis'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Arthritis / Joint pain?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))


        #Conditional Question 17
        tag_id = 'conditional_arthritis_since'
        parent_question_code = 'conditional_arthritis'
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Existing Since',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TwoDropDownField'))


        #Conditional Question 18
        tag_id = 'conditional_kidney'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Kidney Disease?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))


        #Conditional Question 19
        tag_id = 'conditional_kidney_since'
        parent_question_code = 'conditional_kidney'
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Existing Since',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TwoDropDownField'))


        #Conditional Question 20
        tag_id = 'conditional_paralysis'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Paralysis / Stroke?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))


        #Conditional Question 21
        tag_id = 'conditional_paralysis_since'
        parent_question_code = 'conditional_paralysis'
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Existing Since',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TwoDropDownField'))


        #Conditional Question 22
        tag_id = 'conditional_congenital'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Congenital Disorder?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))


        #Conditional Question 23
        tag_id = 'conditional_congenital_since'
        parent_question_code = 'conditional_congenital'
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Existing Since',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TwoDropDownField'))


        #Conditional Question 24
        tag_id = 'conditional_other_diseases'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Any other diseases or ailments not mentioned above?',
            'trigger_any_yes_check': True,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))


        #Conditional Question 25
        tag_id = 'conditional_other_diseases_since'
        parent_question_code = 'conditional_other_diseases'
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Existing Since',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TwoDropDownField'))


        #Conditional Question 26
        tag_id = 'conditional_other_description'
        parent_question_code = 'conditional_other_diseases'
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Other Diseases Description',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))


        #---------- Question Unit Ends -------------

        #---------- Question Unit Begins -----------
        question_code = 'hospitalization'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}

        details = {
            'trigger_any_yes_check': True,
            'conditional': 'NO'
        }
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': details, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='%s_question_%s' % (tag_id, question_code),
            label='Have any of the above mentioned person(s) to be insured been diagnosed / hospitalized for any illness / injury during the last 48 months?',
            helptext='If Yes, please select so that your records are correctly updated as it helps in quick claim dispersal.',
            span=12,
            css_class='bottom-margin',
            value='No',
            event=', click : $parent.root_context.stage_4.any_yes_check',
            template_option='question'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)

        #CONDITIONAL SEGMENT
        #Conditional Question 1
        tag_id = 'conditional_hospitalization'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Please specify',
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))
        #---------- Question Unit Ends -------------

        #---------- Question Unit Begins -----------
        question_code = 'previous_claim'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        details = {
            'trigger_any_yes_check': True,
            'conditional': 'NO'
        }
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': details, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='%s_question_%s' % (tag_id, question_code),
            label='Have any of the person(s) to be insured ever filed a claim with their current / previous insurer?',
            helptext='If Yes, please select so that your records are correctly updated as it helps in quick claim dispersal.',
            span=12,
            value='No',
            css_class='bottom-margin',
            event=', click : $parent.root_context.stage_4.any_yes_check',
            template_option='question'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)

        #CONDITIONAL SEGMENT
        #Conditional Question 1
        tag_id = 'conditional_previous_claim'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Please specify',
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))
        #---------- Question Unit Ends -------------

        #---------- Question Unit Begins -----------
        question_code = 'proposal_declined'
        segment_id = 'medical_question_%s' % question_code
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        details = {
            'trigger_any_yes_check': True,
            'conditional': 'NO'
        }
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': details, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='%s_question_%s' % (tag_id, question_code),
            label='Has any proposal for Health insurance been declined, cancelled or charged a higher premium?',
            helptext='Please select the relevant option as it helps in quick claim dispersal.',
            span=12,
            value='No',
            css_class='bottom-margin',
            event=', click : $parent.root_context.stage_4.any_yes_check',
            template_option='question'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)

        #CONDITIONAL SEGMENT
        #Conditional Question 1
        tag_id = 'conditional_proposal_declined'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Please specify',
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))




        #---------- Question Unit Ends -------------

        #---------- Question Unit Begins -----------
        question_code = 'previous_policy'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'trigger_any_yes_check': True,
            'conditional': 'NO'
        }
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': details, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='%s_question_%s' % (tag_id, question_code),
            label='Is any of the person(s) to be insured, already covered under any other health '
                  'insurance policy of Religare Health Insurance?',
            helptext='Please select "Yes" if any person(s) to be insured is already covered under any '
                     'other insurance policy of Religare Health Insurance. This can be either self-bought '
                     'or availed under group insurance coverage.',
            span=12,
            value='No',
            css_class='bottom-margin',
            event=', click : $parent.root_context.stage_4.any_yes_check',
            template_option='question'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)

        #CONDITIONAL SEGMENT
        #Conditional Question 1
        tag_id = 'conditional_previous_policy'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Please specify',
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))


        #---------- Question Unit Begins -----------
        return {'template': 'stage_4', 'data': stage}


    def stage_5(self):
        stage = []
        segment_id = 'terms'
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}
        row = []
        t_and_c_label = ('I hereby confirm<br />'
                         '- I have read and agree to the Terms and Conditions of ')
        if self.transaction.slab.health_product.policy_wordings:
            t_and_c_label += ('the <a style="text-decoration:none;cursor:pointer;" '
                              'onclick=\'window.open("%s","","width=400, height=400")\'>policy</a> and '
                             ) % self.transaction.slab.health_product.policy_wordings.url
        t_and_c_label += ('the <a style="cursor:pointer;" class="call_overlay">website</a>;<br />'
                          '- the declarations made herewith are true and correct;<br />'
                          '- appointment of Coverfox Insurance Brokers Pvt. Ltd. as my Insurance Broker, '
                          'and authorize them to represent me to Insurance Companies/Agencies for my Insurance needs.'
                         )
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='t_and_c',
            label=t_and_c_label,
            span=12,
            options=[
                {'id' : 'Yes', 'name' : 'Yes', 'is_correct' : True},
                {'id' : 'No', 'name' : 'No', 'is_correct' : False}
            ],
            css_class='bottom-margin',
            template_option='terms'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template': 'stage_5', 'data': stage}


    def get_form_data_individual(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
        ]
        return data

    def get_form_data_family(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
        ]
        return data


def get_state_city_by_pincode(transaction, pincode):
    new_state = pincode.state.name
    city_list = [{'id': c.name, 'name': c.name} for c in pincode.state.city_set.all()]

    transaction.extra['pincode'] = pincode.pincode
    transaction.extra['city']['lat'] = pincode.lat
    transaction.extra['city']['lng'] = pincode.lng
    transaction.extra['city']['name'] = pincode.city
    transaction.save()

    new_location_dict = {}
    new_location_dict['new_state'] = new_state
    new_location_dict['new_city_list'] = city_list
    return new_location_dict, transaction


def get_dynamic_form(transaction):
    transaction, plan_data, proposer_data, is_self_present = custom_utils.standard_form_data_cleanup(transaction)
    #TOGGLES WHERE MEDICAL CASES ARE ALLOWED OR NOT
    plan_data['medical_firewall'] = False
    policy_type = transaction.slab.health_product.policy_type.lower()
    obj = Formic(transaction, is_self_present)
    stages = getattr(obj, 'get_form_data_%s' % policy_type, '')()

    terms_and_conditions = "<div style='padding:20px 40px 70px 20px;'>" \
                           "<h3 style='padding-left:20px;'>Religare: Terms &amp; Conditions</h3>" \
                           "<ol style='list-style-type:lower-alpha;text-align:left;margin-top:20px;' class='ol-list'>" \
                           "<li>I have read and understood the brochure/ prospectus/ sales literature/ Terms and Conditions " \
                           "of the Policy and confirm to abide by the same.</li>" \
                           "<li>Receipt of proposal form by the Company shall not be construed as acceptance of proposal. " \
                           "Commencement of risk under the Policy shall be subject to realization of full premium and individual " \
                           "underwriting by the Company. The Company at its sole discretion reserves the right to accept or reject " \
                           "or load any proposal. Policy would start from the date as specified in the Policy Certificate.</li>" \
                           "<li>I understand that the Policy Period Start Date as specified in the Policy Certificate shall be " \
                           "from the 00:00 hours of the next day of the Proposal receipt at branch, proposed policy period " \
                           "start date as opted by me or cheque date, whichever is later.</li>" \
                           "<li>I understand that the Policy shall become void at the Company's option, in the event of any " \
                           "untrue or incorrect statement, misrepresentation, non-description or non-disclosure of any material " \
                           "fact in the proposal form/ personal statement, declaration and connected documents or any material " \
                           "information having been withheld by me or anyone acting on my behalf.</li>" \
                           "<li>I hereby declare that the lives proposed to be insured would submit to medical examinations, " \
                           "before the nominated doctors of the Company, or undergo diagnostic or other medical tests, as " \
                           "suggested by the Company for its underwriting.</li>" \
                           "<li>I consent to and authorize the Company and/ or any of its authorized representatives/ agents " \
                           "to seek medical information from any hospital/ medical practitioner or any other related entity " \
                           "that I/We have attended or may attend in future concerning any illness or injury.</li>" \
                           "<li>I consent to provide a valid age proof and identity proof at the time of claims or any other " \
                           "time when required by the Company.</li>" \
                           "<li>I authorize the Company to exchange, share or part with the information relating to myself/ " \
                           "person(s) to be insured with any external entity other than regulatory and statutory bodies, as may " \
                           "be required and I will not hold the Company or its agents liable for use/ sharing of this " \
                           "information.</li>" \
                           "<li>I / We agree and undertake to convey to the Company any change / alterations carried out in the " \
                           "risk proposed for insurance after submission of this proposal form.</li>" \
                           "<li>I/We consent to receive information from the Company through physical, electronic or " \
                           "telecommunication means from time to time.</li>" \
                           "</ol>" \
                           "<p style='padding-left:20px'>I, the undersigned hereby declare on my behalf and on behalf of each of the persons proposed to " \
                           "be insured that the above statements and particulars are true, accurate and complete and correct " \
                           "in all respects and that there is  all information which is relevant to this proposal that has " \
                           "been disclosed and not withheld from the Company.  I declare that the money used to make the " \
                           "premium payment has not been derived from any illegal activity or unaccounted funds.  I further " \
                           "declare and agree that this declaration and the answers given above shall be held to be promissory " \
                           "and shall be the basis of the contract between me/us and the Company.</p>" \
                           "<br><p style='padding-left:20px'>Premium quotes shown in this Calculator are first year premiums. Renewal premiums shall be " \
                           "communicated in the renewal notice at the time of renewal.</p> " \
                           "<h3>Coverfox: Terms and Conditions</h3>" \
                           "<p>Coverfox Insurance Broking Pvt. (hereinafter referred to as Coverfox) Limited operates " \
                           "the website <a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> to provide " \
                           "consumers choice and an easy way to buy Insurance " \
                           "products from multiple Insurance companies. Coverfox is not an insurance company. Coverfox " \
                           "is a licensed Insurance Broking Company holding a Broking licence from the Indian " \
                           "Insurance Regulator - Insurance Regulatory and Development authority. " \
                           "<a href='/irda-licence/' target='_blank'>(View IRDA License)</a></p>" \
                           "<p>Our participating insurance providers supply some of the information available on the " \
                           "Website and therefore there may be inaccuracies in the Website Information over which " \
                           "Coverfox has limited control.</p>" \
                           "<p>Coverfox does not warrant or guarantee the: Timeliness, accuracy or completeness of " \
                           "the Website Information; or Quality of the results obtained from the use of the Website.</p>" \
                           "<p>To the maximum extent permitted by law, Coverfox has no liability in relation to or " \
                           "arising out of the Website Information and Website recommendations. You are responsible " \
                           "for the final choice of your product and you should take time to read through all " \
                           "information supplied before proceeding. If you are in any doubt regarding a product or " \
                           "its terms you should seek further advice from Coverfox or the relevant participating " \
                           "provider before choosing your product.</p>" \
                           "<p>Coverfox may pass on your personal information to the relevant participating provider " \
                           "if you apply to purchase a product through " \
                           "<a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> , however, Coverfox does " \
                           "not guarantee when or if you will actually acquire the product that you have chosen. " \
                           "Coverfox does not accept any liability arising out of circumstances where there is delay " \
                           "in you acquiring the product you have chosen.</p>" \
                           "<p>Please note that Coverfox is only collecting or assisting in collecting  the premium " \
                           "deposit on behalf of the insurer you have chosen to buy the policy. The acceptance of " \
                           "the deposit as premium and final issuance of the policy is subject to the underwriting " \
                           "norms and discretion of the Insurer whose policy you have chosen to buy on which Coverfox " \
                           "has no control. Coverfox will ensure that the amount is refunded by the insurer in case " \
                           "there is no ultimate issuance of policy.</p>" \
                           "<p><button class='btn btn-lg cf-btn btn-red hide_overlay'>Close</button></p>" \
                           "</div>"

    slate_list = [
        {'id': 'slate_1', 'name': 'Proposer', 'class' : '', 'description' : 'The person who is paying for the policy'},
        {'id': 'slate_2', 'name' : 'Insured Members', 'class' : '', 'description' : 'People being insured under the policy'},
        {'id': 'slate_3', 'name' : 'Contact Info.', 'class' : '', 'description' : 'Contact Information of the proposer'},
        {'id': 'slate_4', 'name' : 'Medical History', 'class' : '', 'description' : 'Medical History of the people being insured'},
        {'id': 'slate_5', 'name' : 'T & C', 'class' : 'last', 'description' : 'Mandatory terms and conditions', 'style' : " 'width', '0px' "}
    ]
    policy_date_disclaimer = 'NOTE: The policy period will start immediately from the date this transaction gets processed'
    data = {
        'slate_list' : slate_list,
        'proposer_data' : proposer_data,
        'plan_data' : plan_data,
        'stages' : stages,
        'last_stage' : stages[-1]['template'],
        'terms_and_conditions' : terms_and_conditions,
        'policy_date_disclaimer' : policy_date_disclaimer
    }
    return data
