import putils

def download_buy_form(transaction):
    order = []
    rows = []

    form_data = transaction.insured_details_json['form_data']
    proposer_data = transaction.insured_details_json['form_data']['proposer']['personal']
    proposer_address = transaction.insured_details_json['form_data']['proposer']['address']
    proposer_contact = transaction.insured_details_json['form_data']['proposer']['contact']
    insured_data = transaction.insured_details_json['form_data']['insured']
    nominee_data = transaction.insured_details_json['form_data']['nominee']
    pre_existing = transaction.insured_details_json['form_data']['medical_question_pre_existing']
    pre_existing_diseases = pre_existing['combo']['combo_question_pre_existing']
    preExistingQuestionList = ['conditional_diabetes', 'conditional_hypertension', 'conditional_respiratory_disorder', 'conditional_hiv', 'conditional_liver_disease', 'conditional_cancer', 'conditional_cardiac', 'conditional_arthritis', 'conditional_kidney', 'conditional_paralysis', 'conditional_congenital', 'conditional_other_diseases']
    hospitalization = transaction.insured_details_json['form_data']['medical_question_hospitalization']['combo']['combo_question_hospitalization']
    conditional_hospitalization = transaction.insured_details_json['form_data']['medical_question_hospitalization']['conditional_hospitalization']
    previous_claim = transaction.insured_details_json['form_data']['medical_question_previous_claim']['combo']['combo_question_previous_claim']
    conditional_previous_claim = transaction.insured_details_json['form_data']['medical_question_previous_claim']['conditional_previous_claim']
    proposal_declined = transaction.insured_details_json['form_data']['medical_question_proposal_declined']['combo']['combo_question_proposal_declined']
    conditional_proposal_declined = transaction.insured_details_json['form_data']['medical_question_proposal_declined']['conditional_proposal_declined']
    previous_policy = transaction.insured_details_json['form_data']['medical_question_previous_policy']['combo']['combo_question_previous_policy']
    conditional_previous_policy = transaction.insured_details_json['form_data']['medical_question_previous_policy']['conditional_previous_policy']

    memberList = []
    for member in insured_data:
        memberList.append(member)

    rows.append(['Proposer Details'])
    rows.append(['Name', ' '.join([proposer_data['first_name']['value'], proposer_data['last_name']['value']])])
    rows.append(['Gender', proposer_data['gender']['display_value']])
    rows.append(['Marital Status', proposer_data['marital_status']['display_value']])
    rows.append(['Date of Birth', proposer_data['date_of_birth']['display_value']])
    rows.append(['Policy Term', str(proposer_data['policy_term']['value1'])])


    rows.append([])
    rows.append(['Insured Members'])
    for member in insured_data:
        rows.append(['Which Insured Member', member])
        rows.append(['Name', ' '.join([insured_data[member]['first_name']['display_value'], insured_data[member]['last_name']['display_value']])])
        rows.append(['Date of Birth', insured_data[member]['date_of_birth']['display_value']])
        rows.append([])



    rows.append(['Nominee Details'])
    for member in nominee_data:
        rows.append(['Nominee for which insured?', member])
        rows.append(['Name', ' '.join([nominee_data[member]['first_name']['value'], nominee_data[member]['last_name']['value']])])
        rows.append(['Date of Birth', nominee_data[member]['date_of_birth']['display_value']])
        rows.append([nominee_data[member]['relation']['label'], nominee_data[member]['relation']['display_value']])
        rows.append([])



    rows.append(['Proposer Contact Details'])
    rows.append(['Address', ', '.join([proposer_address['address1']['value'], proposer_address['address2']['value'], proposer_address['landmark']['value'], proposer_address['city']['display_value'], proposer_address['state']['value'], proposer_address['pincode']['value']])])
    rows.append(['Email', proposer_contact['email']['value']])
    rows.append(['LandLine', proposer_contact['landline']['std_code']+'-'+proposer_contact['landline']['landline']])
    rows.append(['Mobile', proposer_contact['mobile']['value']])



    rows.append([])
    rows.append(['Medical Details'])
    rows.append([pre_existing_diseases['label'], pre_existing_diseases['display_value']])
    rows.append([])
    rows.append(['Members','']+memberList)
    for dis in preExistingQuestionList:
        row = []
        row.append(pre_existing[dis].values()[0]['label'])
        row.append('')
        for member in memberList:
            row.append(pre_existing[dis][member+'_question_'+dis]['display_value'])
        rows.append(row)

        row = []
        row.append(pre_existing[dis+'_since'].values()[0]['label'])
        row.append('')
        for member in memberList:
            value1 = pre_existing[dis+'_since'][member+'_question_'+dis+'_since']['value1']
            value2 = pre_existing[dis+'_since'][member+'_question_'+dis+'_since']['value2']
            row.append(value1+' '+value2 if value1 and value2 else 'None')
        rows.append(row)
        if dis == 'conditional_other_diseases':
            row = []
            row.append(pre_existing['conditional_other_description'].values()[0]['label'])
            row.append('')
            for member in memberList:
                display_value = pre_existing['conditional_other_description'][member+'_question_conditional_other_description']['display_value']
                row.append(display_value if display_value else 'None')
            rows.append(row)
        rows.append([])

    rows.append([hospitalization['label'], hospitalization['display_value']])
    row = []
    row.append(conditional_hospitalization.values()[0]['label'])
    row.append('')
    for member in memberList:
        row.append(conditional_hospitalization[member+'_question_conditional_hospitalization']['display_value'])
    rows.append(row)

    rows.append([])
    rows.append([previous_claim['label'], previous_claim['display_value']])
    row = []
    row.append(conditional_previous_claim.values()[0]['label'])
    row.append('')
    for member in memberList:
        row.append(conditional_previous_claim[member+'_question_conditional_previous_claim']['display_value'])
    rows.append(row)


    rows.append([])
    rows.append([proposal_declined['label'], proposal_declined['display_value']])
    row = []
    row.append(conditional_proposal_declined.values()[0]['label'])
    row.append('')
    for member in memberList:
        row.append(conditional_proposal_declined[member+'_question_conditional_proposal_declined']['display_value'])
    rows.append(row)



    rows.append([])
    rows.append([previous_policy['label'], previous_policy['display_value']])
    row = []
    row.append(conditional_previous_policy.values()[0]['label'])
    row.append('')
    for member in memberList:
        row.append(conditional_previous_policy[member+'_question_conditional_previous_policy']['display_value'])
    rows.append(row)

    file_name = 'Form_Data_%s.xls' % transaction.transaction_id
    return putils.render_excel(file_name, order, rows)
