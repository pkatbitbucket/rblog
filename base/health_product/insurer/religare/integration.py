import datetime
import json
import os
import xml.etree.ElementTree as ET
import base64
import time
import logging
from collections import OrderedDict

from django.conf import settings
from wiki.models import CoverPremiumTable, FamilyCoverPremiumTable
from django.core.files.base import File
from dateutil.relativedelta import relativedelta
from settings import SETTINGS_FILE_FOLDER
from utils import request_logger

from suds.client import Client
from suds.plugin import MessagePlugin
logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.transport.http').setLevel(logging.DEBUG)

# -------- COMMON CREDENTIALS FOR PRODUCTION AND UAT -----------------------
PARENT_CODE = '20012865'
PARENT_NAME = 'Coverfox Insurance Broking Pvt Ltd'
CHILD_CODE = '20012981'
CHILD_NAME = 'COVERFOX INSURANCE BROKING PVT LTD ONLINE'
PRODUCT_CODE = {
    'care': '10001101',
    'enhance_1': '11001001',
    'enhance_2': '11001002'
}

if settings.PRODUCTION:
    # -------- PRODUCTION CREDENTIALS -----------------------
    ORIGINAL_WSDL_URL = 'https://www.religarehealthinsurance.com/relinterface/services/CreatePolicyServiceInt?wsdl'
    RELIGARE_WSDL_FILE = 'file://%s' % os.path.join(
        SETTINGS_FILE_FOLDER,
        'health_product/insurer/religare/webservice/myReligare.wsdl'
    )
    PDF_WSDL_PATH = 'file://%s' % os.path.join(
        settings.SETTINGS_FILE_FOLDER,
        'health_product/insurer/religare/webservice/PDF_fetch.wsdl'
    )
    ENDPOINT_URL = 'https://www.religarehealthinsurance.com/relinterface/services/CreatePolicyServiceInt.CreatePolicyServiceIntHttpSoap11Endpoint/'
    PAYMENT_URL = 'https://www.religarehealthinsurance.com/portalui/PortalPayment.run'
    AGENT_ID = '20012981'
    WSDL_USERNAME = 'QuotationUser'
    WSDL_PASSWORD = 'religare@123'
    PDF_USERNAME = 'SymbiosysUser'
    PDF_PASSWORD = 'Password-1'
    USERNAME = 'serviceClient'
    PASSWORD = 'weX46J'
else:
    # -------- UAT CREDENTIALS -----------------------
    ORIGINAL_WSDL_URL = 'http://59.160.230.173/cordys/WSDLGateway.wcp?service=http%3A%2F%2Fschemas.cordys.' \
                        'com%2Fdefault/calculatePremiumValuesBPMWS&organization=o%3DReligareHealth%2Ccn%3Dcordys' \
                        '%2Ccn%3DdefaultInst%2Co%3Dreligare.in&version=isv'
    RELIGARE_WSDL_FILE = 'file://%s' % os.path.join(
        SETTINGS_FILE_FOLDER,
        'health_product/insurer/religare/webservice/uat/myReligare.wsdl'
    )
    PDF_WSDL_PATH = 'file://%s' % os.path.join(
        settings.SETTINGS_FILE_FOLDER,
        'health_product/insurer/religare/webservice/uat/PDF_fetch.wsdl'
    )
    ENDPOINT_URL = 'https://rhicluat.religare.com/relinterface/services/CreatePolicyServiceInt.CreatePolicyServiceIntHttpSoap11Endpoint/'
    WSDL_USERNAME = 'QuotationUser'
    WSDL_PASSWORD = 'religare@123'
    PAYMENT_URL = 'https://rhicluat.religare.com/portalui/PortalPayment.run'
    USERNAME = 'serviceClient'
    PASSWORD = 'weX46J'
    PDF_USERNAME = 'SymbiosysUser'
    PDF_PASSWORD = 'Password-1'
    AGENT_ID = '20008025'



class _AttributePlugin(MessagePlugin):
    """
    Suds plug-in extending the method call with arbitrary attributes.
    """
    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def marshalled(self, context):
        children = context.envelope.getChildren()
        header = children[0]
        ebody = children[1]
        children.remove(header)
        ebody.prefix = "SOAP-ENV"

SUM_INSURED_DICT = {
    'care' : {
        'individual' : {
            #100000 : '024',
            #200000 : '001',
            300000 : '003',
            400000 : '005',
            500000 : '007',
            700000 : '009',
            1000000 : '011',
            1500000 : '013',
            2000000 : '015',
            2500000 : '017',
            5000000 : '019',
            6000000 : '021'
        },
        'family' : {
            #200000 : '002',
            300000 : '004',
            400000 : '006',
            500000 : '008',
            700000 : '010',
            1000000 : '012',
            1500000 : '014',
            2000000 : '016',
            2500000 : '018',
            5000000 : '020',
            6000000 : '022'
        }
    },
    'enhance_1' : {
        'individual' : {
            '100000-100000': '001',
            '1000000-1000000': '075',
            '1000000-200000': '019',
            '1000000-500000': '043',
            '1200000-300000': '027',
            '1200000-400000': '035',
            '1200000-600000': '053',
            '1400000-700000': '059',
            '1500000-300000': '029',
            '1500000-500000': '045',
            '1600000-400000': '037',
            '1600000-800000': '065',
            '1800000-600000': '055',
            '1800000-900000': '071',
            '200000-100000': '003',
            '200000-200000': '011',
            '2000000-1000000': '077',
            '2000000-400000': '039',
            '2000000-500000': '047',
            '2100000-700000': '061',
            '2400000-800000': '067',
            '2500000-500000': '049',
            '2700000-900000': '073',
            '300000-100000': '005',
            '300000-300000': '021',
            '3000000-1000000': '079',
            '400000-100000': '007',
            '400000-200000': '013',
            '400000-400000': '031',
            '500000-100000': '009',
            '500000-500000': '041',
            '600000-200000': '015',
            '600000-300000': '023',
            '600000-600000': '051',
            '700000-700000': '057',
            '800000-200000': '017',
            '800000-400000': '033',
            '800000-800000': '063',
            '900000-300000': '025',
            '900000-900000': '069'
        },

        'family' : {
            '1000000-1000000': '076',
            '100000-100000': '002',
            '1600000-400000': '038',
            '1500000-500000': '046',
            '1000000-200000': '020',
            '1500000-300000': '030',
            '1000000-500000': '044',
            '1400000-700000': '060',
            '1200000-600000': '054',
            '1200000-400000': '036',
            '1200000-300000': '028',
            '2000000-400000': '040',
            '2000000-1000000': '078',
            '200000-200000': '012',
            '200000-100000': '004',
            '1800000-900000': '072',
            '1800000-600000': '056',
            '1600000-800000': '066',
            '300000-300000': '022',
            '300000-100000': '006',
            '2700000-900000': '074',
            '2500000-500000': '050',
            '2400000-800000': '068',
            '2100000-700000': '062',
            '2000000-500000': '048',
            '3000000-1000000': '080',
            '500000-100000': '010',
            '400000-400000': '032',
            '400000-200000': '014',
            '400000-100000': '008',
            '500000-500000': '042',
            '800000-200000': '018',
            '700000-700000': '058',
            '600000-600000': '052',
            '600000-300000': '024',
            '600000-200000': '016',
            '800000-400000': '034',
            '900000-300000': '026',
            '800000-800000': '064',
            '900000-900000': '070',
            }
        },
    'enhance_2' : {
        'individual' : {
            '3000000-2000000' : '013',
            '5500000-500000' : '003',
            '4500000-500000' : '001',
            '4500000-1500000' : '011',
            '4000000-2000000' : '015',
            '5000000-1000000' : '007',
            '4000000-1000000' : '005',
            '3500000-1500000' : '009'
        },
        'family' : {
            '5500000-500000' : '004',
            '4500000-500000' : '002',
            '4500000-1500000' : '012',
            '4000000-2000000' : '016',
            '5000000-1000000' : '008',
            '4000000-1000000' : '006',
            '3500000-1500000' : '010',
            '3000000-2000000' : '014'
        }
        }
}




RELATION_CHOICES = {
    'self' : 'SELF',
    'spouse' : 'SPSE',
    'father' : 'FATH',
    'mother' : 'MOTH',
    'son' : 'SONM',
    'daughter' : 'UDTR',
}

GENDER_CHOICES = {
    1 : 'MALE',
    2 : 'FEMALE'
}

SALUTATION_CHOICES = {
    1 : 'MR',
    2 : 'MS'
}

MEDICAL_DICT = OrderedDict([
    ('medical_question_pre_existing', OrderedDict([
        ('set_code', 'yesNoExist'),
        ('qcode', 'pedYesNo'),
        ('conditional', OrderedDict([
            # ('conditional_pre_existing', OrderedDict([
            #     ('set_code', 'yesNoExist'),
            #     ('qcode', 'pedYesNo')
            # ])),
            ('conditional_diabetes', OrderedDict([
                ('set_code', 'PEDdiabetesDetails'),
                ('qcode', '205')
            ])),
            ('conditional_diabetes_since', OrderedDict([
                ('set_code', 'PEDdiabetesDetails'),
                ('qcode', 'diabetesExistingSince')
            ])),
            ('conditional_cancer', OrderedDict([
                ('set_code', 'PEDcancerDetails'),
                ('qcode', '114')
            ])),
            ('conditional_cancer_since', OrderedDict([
                ('set_code', 'PEDcancerDetails'),
                ('qcode', 'cancerExistingSince')
            ])),
            ('conditional_cardiac', OrderedDict([
                ('set_code', 'PEDcardiacDetails'),
                ('qcode', '143')
            ])),
            ('conditional_cardiac_since', OrderedDict([
                ('set_code', 'PEDcardiacDetails'),
                ('qcode', 'cardiacExistingSince')
            ])),
            ('conditional_congenital', OrderedDict([
                ('set_code', 'PEDcongenitalDetails'),
                ('qcode', '122')
            ])),
            ('conditional_congenital_since', OrderedDict([
                ('set_code', 'PEDcongenitalDetails'),
                ('qcode', 'congenitalExistingSince')
            ])),
            ('conditional_hiv', OrderedDict([
                ('set_code', 'PEDHivaidsDetails'),
                ('qcode', '147')
            ])),
            ('conditional_hiv_since', OrderedDict([
                ('set_code', 'PEDHivaidsDetails'),
                ('qcode', 'hivaidsExistingSince')
            ])),
            ('conditional_hypertension', OrderedDict([
                ('set_code', 'PEDhyperTensionDetails'),
                ('qcode', '207')
            ])),
            ('conditional_hypertension_since', OrderedDict([
                ('set_code', 'PEDhyperTensionDetails'),
                ('qcode', 'hyperTensionExistingSince')
            ])),
            ('conditional_arthritis', OrderedDict([
                ('set_code', 'PEDjointpainDetails'),
                ('qcode', '105')
            ])),
            ('conditional_arthritis_since', OrderedDict([
                ('set_code', 'PEDjointpainDetails'),
                ('qcode', 'jointpainExistingSince')
            ])),
            ('conditional_kidney', OrderedDict([
                ('set_code', 'PEDkidneyDetails'),
                ('qcode', '129')
            ])),
            ('conditional_kidney_since', OrderedDict([
                ('set_code', 'PEDkidneyDetails'),
                ('qcode', 'kidneyExistingSince')
            ])),
            ('conditional_liver_disease', OrderedDict([
                ('set_code', 'PEDliverDetails'),
                ('qcode', '232')
            ])),
            ('conditional_liver_disease_since', OrderedDict([
                ('set_code', 'PEDliverDetails'),
                ('qcode', 'liverExistingSince')
            ])),
            ('conditional_paralysis', OrderedDict([
                ('set_code', 'PEDparalysisDetails'),
                ('qcode', '164')
            ])),
            ('conditional_paralysis_since', OrderedDict([
                ('set_code', 'PEDparalysisDetails'),
                ('qcode', 'paralysisExistingSince')
            ])),
            # ('conditional_respiratory_disorder', OrderedDict([
            #     ('set_code', ''),
            #     ('qcode', '')
            # ])),
            # ('conditional_respiratory_disorder_since', OrderedDict([
            #     ('set_code', ''),
            #     ('qcode', '')
            # ])),
            ('conditional_other_diseases', OrderedDict([
                ('set_code', 'PEDotherDetails'),
                ('qcode', '210')
            ])),
            ('conditional_other_diseases_since', OrderedDict([
                ('set_code', 'PEDotherDetails'),
                ('qcode', 'otherExistingSince')
            ])),
            # ('conditional_other_description', OrderedDict([
            #     ('set_code', ''),
            #     ('qcode', '')
            # ])),
        ])),
        ])),
    ('medical_question_hospitalization', OrderedDict([
        ('set_code', 'HEDHealthHospitalized'),
        ('qcode', 'H001'),
        ('conditional', OrderedDict([
            ('conditional_hospitalization', OrderedDict([
                ('set_code', 'HEDHealthHospitalized'),
                ('qcode', 'H001')
            ]))
        ]))
    ])),
    ('medical_question_previous_claim', OrderedDict([
        ('set_code', 'HEDHealthClaim'),
        ('qcode', 'H002'),
        ('conditional', OrderedDict([
            ('conditional_previous_claim', OrderedDict([
                ('set_code', 'HEDHealthClaim'),
                ('qcode', 'H002')
            ]))
        ]))
    ])),
    ('medical_question_proposal_declined', OrderedDict([
        ('set_code', 'HEDHealthDeclined'),
        ('qcode', 'H003'),
        ('conditional', OrderedDict([
            ('conditional_proposal_declined', OrderedDict([
                ('set_code', 'HEDHealthDeclined'),
                ('qcode', 'H003')
            ]))
        ]))
    ])),
    ('medical_question_previous_policy', OrderedDict([
        ('set_code', 'HEDHealthCovered'),
        ('qcode', 'H004'),
        ('conditional', OrderedDict([
            ('conditional_previous_policy', OrderedDict([
                ('set_code', 'HEDHealthCovered'),
                ('qcode', 'H004')
            ]))
        ]))
    ]))
])

def get_product_name(transaction):
    '''
    Return product name
    '''
    if int(transaction.slab.health_product.id) in [82,83]:
        if int(transaction.slab.sum_assured) + int(transaction.slab.deductible) < 5000000:
            return 'enhance_1'
        else:
            return 'enhance_2'
    elif int(transaction.slab.health_product.id) in [5, 8, 61, 62]:
        return 'care'

def get_sum_insured(transaction):
    '''
    Return Sum Insured
    '''
    if int(transaction.slab.health_product.id) in [82, 83]:
        return str(int(transaction.slab.sum_assured)) + "-" + str(int(transaction.slab.deductible))
    elif int(transaction.slab.health_product.id) in [5, 8, 61, 62]:
        return int(transaction.slab.sum_assured)

def post_transaction(transaction):
    redirect_url = '/health-plan/product/transaction/%s/failure/' % transaction.transaction_id
    is_redirect = True
    data = {}
    policy_type = transaction.slab.health_product.policy_type.lower()
    # try:
    if True:
        slab = transaction.slab
        health_product = slab.health_product
        policy_type = health_product.policy_type.lower()
        product_name = get_product_name(transaction)
        sum_insured = get_sum_insured(transaction)
        if policy_type == 'individual':
            cpt = CoverPremiumTable.objects.get(id=transaction.extra['cpt_id'])
        else:
            cpt = FamilyCoverPremiumTable.objects.get(id=transaction.extra['cpt_id'])

        pdata = transaction.insured_details_json['form_data']['proposer']['personal']
        if 'date_of_birth' in pdata.keys():
            transaction.proposer_date_of_birth = datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y')
        if 'gender' in pdata.keys():
            transaction.proposer_gender = int(pdata['gender']['value'])
        transaction.save()

        client = Client(RELIGARE_WSDL_FILE, username=USERNAME, password=PASSWORD)
        client.set_options(port='CreatePolicyServiceIntHttpSoap11Endpoint')

        #PLAN DATA
        #################### IntPolicyDO ###################
        IntPolicyDO = client.factory.create('ns0:IntPolicyDO')
        IntPolicyDO.businessTypeCd = 'NEWBUSINESS'
        IntPolicyDO.baseProductId = PRODUCT_CODE[product_name]
        IntPolicyDO.baseAgentId = AGENT_ID
        IntPolicyDO.coverType = 'INDIVIDUAL' if policy_type == 'individual' else 'FAMILYFLOATER'
        IntPolicyDO.partyDOList = []
        IntPolicyDO.policyAdditionalFieldsDOList = []
        IntPolicyDO.quotationReferenceNum = ''
        IntPolicyDO.sumInsured = SUM_INSURED_DICT[product_name][policy_type][sum_insured]
        IntPolicyDO.term = pdata['policy_term']['value1']
        IntPolicyDO.isPremiumCalculation = 'YES'
        IntPolicyDO.uwDecisionCd = ''
        IntPolicyDO.policyNum = ''
        IntPolicyDO.proposalNum = ''
        IntPolicyDO.quotationReferenceNum = ''

        #IMPORTANT: IF THE SELECTED PLAN IS NCB SUPER
        if transaction.slab.health_product.id in [61, 62]:
            IntPolicyDO.addOns = 'CAREWITHNCB'
        #################### ~~~~~~~~~~~ ###################

        #TERMS AND CONDITIONS + NOTIFICATION SETTINGS
        #################### IntPolicyAdditionalFieldsDO ###################
        IntPolicyAdditionalFieldsDO = client.factory.create('ns0:IntPolicyAdditionalFieldsDO')
        IntPolicyAdditionalFieldsDO.fieldAgree = 'YES'
        IntPolicyAdditionalFieldsDO.fieldAlerts = 'NO'
        IntPolicyAdditionalFieldsDO.fieldTc = 'YES'
        IntPolicyDO.policyAdditionalFieldsDOList.append(IntPolicyAdditionalFieldsDO)
        #################### ~~~~~~~~~~~ ###################

        #################### partyDO == PROPOSER if roleCd == PROPOSER ###################
        proposerPartyDO = client.factory.create('ns0:IntPartyDO')
        proposerPartyDO.birthDt = datetime.datetime.strftime(transaction.proposer_date_of_birth, '%d/%m/%Y')
        proposer_bigname = transaction.proposer_first_name
        if transaction.proposer_middle_name.strip():
            proposer_bigname = '%s %s' % (transaction.proposer_first_name, transaction.proposer_middle_name)
        proposerPartyDO.firstName = proposer_bigname

        proposerPartyDO.lastName = transaction.proposer_last_name
        proposerPartyDO.genderCd = GENDER_CHOICES[int(transaction.proposer_gender)]
        proposerPartyDO.guid = 'self%s' % transaction.id
        proposerPartyDO.relationCd = 'SELF'
        proposerPartyDO.roleCd = 'PROPOSER'
        proposerPartyDO.titleCd = SALUTATION_CHOICES[int(transaction.proposer_gender)]

        proposerPartyDO.partyAddressDOList = []
        #==================Permanent address of Insured Member=====================
        addressDO1 = client.factory.create('ns0:IntPartyAddressDO')
        addressDO1.addressLine1Lang1 = transaction.proposer_address1[:100]
        addressDO1.addressLine2Lang1 = transaction.proposer_address2[:100]
        addressDO1.addressTypeCd = 'PERMANENT'
        addressDO1.areaCd = transaction.proposer_city
        addressDO1.cityCd = transaction.proposer_city
        addressDO1.pinCode = transaction.proposer_pincode
        addressDO1.stateCd = transaction.proposer_state
        proposerPartyDO.partyAddressDOList.append(addressDO1)

        #==================Communication address of Insured Member=====================
        addressDO2 = client.factory.create('ns0:IntPartyAddressDO')
        addressDO2.addressLine1Lang1 = transaction.proposer_address1[:100]
        addressDO2.addressLine2Lang1 = transaction.proposer_address2[:100]
        addressDO2.addressTypeCd = 'COMMUNICATION'
        addressDO2.areaCd = transaction.proposer_city  #TODO Check with Religare
        addressDO2.cityCd = transaction.proposer_city
        addressDO2.pinCode = transaction.proposer_pincode
        addressDO2.stateCd = transaction.proposer_state
        proposerPartyDO.partyAddressDOList.append(addressDO2)

        #==================Phone data of Insured Member (copy from Proposer) =========
        proposerPartyDO.partyContactDOList = []
        contactDO = client.factory.create('ns0:IntPartyContactDO')
        contactDO.contactNum = transaction.proposer_mobile
        contactDO.contactTypeCd = 'MOBILE'
        contactDO.stdCode = ' '
        proposerPartyDO.partyContactDOList.append(contactDO)

        #==================E-mail data of Insured Member (copy from Proposer) =========
        proposerPartyDO.partyEmailDOList = []
        #Email Object
        emailDO = client.factory.create('ns0:IntPartyEmailDO')
        emailDO.emailAddress = transaction.proposer_email
        emailDO.emailTypeCd = 'PERSONAL'
        proposerPartyDO.partyEmailDOList.append(emailDO)

        #==================Identity data of Insured Member (copy from Proposer) =========
        proposerPartyDO.partyIdentityDOList = []
        identityDO = client.factory.create('ns0:IntPartyIdentityDO')
        identityDO.identityNum = ' '
        identityDO.identityTypeCd = ' '
        proposerPartyDO.partyIdentityDOList.append(identityDO)

        #==================Medical Questions Response data of Insured Member (copy from Proposer) =========
        proposerPartyDO.partyQuestionDOList = []
        questiondo = client.factory.create('ns0:IntPartyQuestionDO')
        questiondo.questionCd = ' '
        questiondo.questionSetCd = ' '
        questiondo.response = ' '
        proposerPartyDO.partyQuestionDOList.append(questiondo)


        IntPolicyDO.partyDOList.append(proposerPartyDO)



        #INSURED MEMBER INFORMATION
        counter = 0
        mdata = transaction.insured_details_json['form_data']
        for code, fdata in transaction.insured_details_json['form_data']['insured'].items():
            counter += 1

            ins_gender = 1
            current_member = None
            for mem in transaction.extra['members']:
                if mem['id'] == code:
                    current_member = mem
                    ins_gender = int(mem['gender'])

            #################### partyDO == INSURED MEMBER if roleCd == PRIMARY ###################
            partyDO = client.factory.create('ns0:IntPartyDO')
            partyDO.birthDt = fdata['date_of_birth']['value']
            partyDO.firstName = fdata['first_name']['value']
            partyDO.lastName = fdata['last_name']['value']
            partyDO.genderCd = GENDER_CHOICES[ins_gender]
            partyDO.guid = '%s%s' % (code.lower(), transaction.id)
            partyDO.relationCd = RELATION_CHOICES[current_member['person']]
            partyDO.roleCd = 'PRIMARY'
            partyDO.titleCd = SALUTATION_CHOICES[ins_gender]

            partyDO.partyAddressDOList = []
            #==================Permanent address of Insured Member=====================
            addressDO1 = client.factory.create('ns0:IntPartyAddressDO')
            addressDO1.addressLine1Lang1 = transaction.proposer_address1[:100]
            addressDO1.addressLine2Lang1 = transaction.proposer_address2[:100]
            addressDO1.addressTypeCd = 'PERMANENT'
            addressDO1.areaCd = transaction.proposer_city
            addressDO1.cityCd = transaction.proposer_city
            addressDO1.pinCode = transaction.proposer_pincode
            addressDO1.stateCd = transaction.proposer_state
            partyDO.partyAddressDOList.append(addressDO1)

            #==================Communication address of Insured Member=====================
            addressDO2 = client.factory.create('ns0:IntPartyAddressDO')
            addressDO2.addressLine1Lang1 = transaction.proposer_address1[:100]
            addressDO2.addressLine2Lang1 = transaction.proposer_address2[:100]
            addressDO2.addressTypeCd = 'COMMUNICATION'
            addressDO2.areaCd = transaction.proposer_city
            addressDO2.cityCd = transaction.proposer_city
            addressDO2.pinCode = transaction.proposer_pincode
            addressDO2.stateCd = transaction.proposer_state
            partyDO.partyAddressDOList.append(addressDO2)

            #==================Phone data of Insured Member (copy from Proposer) =========
            partyDO.partyContactDOList = []
            contactDO = client.factory.create('ns0:IntPartyContactDO')
            contactDO.contactNum = transaction.proposer_mobile
            contactDO.contactTypeCd = 'MOBILE'
            contactDO.stdCode = ''
            partyDO.partyContactDOList.append(contactDO)

            #==================E-mail data of Insured Member (copy from Proposer) =========
            partyDO.partyEmailDOList = []
            #Email Object
            emailDO = client.factory.create('ns0:IntPartyEmailDO')
            emailDO.emailAddress = transaction.proposer_email
            emailDO.emailTypeCd = 'PERSONAL'
            partyDO.partyEmailDOList.append(emailDO)

            #==================Identity data of Insured Member (copy from Proposer) =========
            partyDO.partyIdentityDOList = []
            identityDO = client.factory.create('ns0:IntPartyIdentityDO')
            identityDO.identityNum = ' '
            identityDO.identityTypeCd = ' '
            partyDO.partyIdentityDOList.append(identityDO)

            #==================Medical Questions Response data of Insured Member (copy from Proposer) =========
            #TODO Check if this can be empty
            partyDO.partyQuestionDOList = []


            pre_existing_diseases = False
            for qname, qdata in MEDICAL_DICT.items():
                if mdata[qname]['combo']['combo_%s' % qname.split('medical_')[1]]['value'] == 'No':
                    questiondo = client.factory.create('ns0:IntPartyQuestionDO')
                    questiondo.questionCd = qdata['qcode']
                    questiondo.questionSetCd = qdata['set_code']
                    questiondo.response = 'NO'
                    partyDO.partyQuestionDOList.append(questiondo)
                else:
                    print '>>>>>> QDATA >>>>>>>>>>', qdata
                    for cqname, cqdata in qdata['conditional'].items():
                        if cqname.split('_')[-1] == 'since':
                            if mdata[qname][cqname]['%s_question_%s' % (code, cqname)].get('value1', None):
                                if qname == 'medical_question_pre_existing':
                                    pre_existing_diseases = True
                                questiondo = client.factory.create('ns0:IntPartyQuestionDO')
                                questiondo.questionCd = cqdata['qcode']
                                questiondo.questionSetCd = cqdata['set_code']
                                questiondo.response = '%s/%s' % (
                                    str(mdata[qname][cqname]['%s_question_%s' % (code, cqname)]['value1']).zfill(2),
                                    mdata[qname][cqname]['%s_question_%s' % (code, cqname)]['value2'],
                                )
                                partyDO.partyQuestionDOList.append(questiondo)
                        else:
                            if mdata[qname][cqname]['%s_question_%s' % (code, cqname)]['value'].strip():
                                if qname == 'medical_question_pre_existing' and mdata[qname][cqname]['%s_question_%s' % (code, cqname)]['value'].upper() == 'YES':
                                    pre_existing_diseases = True
                                questiondo = client.factory.create('ns0:IntPartyQuestionDO')
                                questiondo.questionCd = cqdata['qcode']
                                questiondo.questionSetCd = cqdata['set_code']
                                questiondo.response = mdata[qname][cqname]['%s_question_%s' % (code, cqname)]['value'].upper()
                                partyDO.partyQuestionDOList.append(questiondo)
                    if qname == 'medical_question_pre_existing':
                        if pre_existing_diseases:
                            questiondo = client.factory.create('ns0:IntPartyQuestionDO')
                            questiondo.questionCd = 'pedYesNo'
                            questiondo.questionSetCd = 'yesNoExist'
                            questiondo.response = 'Yes'
                            partyDO.partyQuestionDOList.append(questiondo)
                        else:
                            questiondo = client.factory.create('ns0:IntPartyQuestionDO')
                            questiondo.questionCd = 'pedYesNo'
                            questiondo.questionSetCd = 'yesNoExist'
                            questiondo.response = 'NO'
                            partyDO.partyQuestionDOList.append(questiondo)


            IntPolicyDO.partyDOList.append(partyDO)

        IntPolicyDataIO = client.factory.create('ns0:IntPolicyDataIO')
        IntPolicyDataIO.policy = IntPolicyDO

        cp = client.service.createPolicy

        cp.method.location = ENDPOINT_URL
        resp = cp(IntPolicyDataIO)

        # request_logger.info('\nRELIGARE TRANSACTION: %s \n RESPONSE DICT:\n %s \n' % (transaction.transaction_id, resp))
        # print "======================================", resp
        request_logger.info('\nRELIGARE TRANSACTION: %s \n XML REQUEST SENT:\n %s \n' % (transaction.transaction_id, client.last_sent()))
        request_logger.info('\nRELIGARE TRANSACTION: %s \n XML RESPONSE RECEIVED:\n %s \n' % (transaction.transaction_id, client.last_received()))

        response_data = {
            'proposal_number' : resp['int-policy-data-iO']['policy']['proposal-num'],
            }
        try:
            response_data['premium'] = resp['int-policy-data-iO']['policy']['premium']
            transaction.premium_paid = response_data['premium']
        except Exception as e:
            print 'ERROR : ', str(e)
            request_logger.info('\nRELIGARE TRANSACTION: %s \n NO PREMIUM IN RESPONSE ERROR:\n %s \n' % (transaction.transaction_id, str(e)))
        transaction.extra['product_response'] = response_data
        transaction.reference_id = 'CFOX-%s-%s' % (transaction.slab.health_product.insurer.id, transaction.id)
        transaction.policy_token = str(transaction.extra['product_response']['proposal_number'])
        transaction.payment_request = client.last_sent()
        transaction.save()

        is_redirect = False
        redirect_url = PAYMENT_URL
        data = {
            'proposalNum' : transaction.policy_token,
            'returnURL' : '%s/health-plan/religare/transaction/%s/response/' % (settings.SITE_URL, transaction.transaction_id)
        }
        return is_redirect, redirect_url, 'health_product/post_transaction_data.html', data
    # except Exception as e:
    else:
        # print ">>>>>>>>>", e.message
        return is_redirect, redirect_url, '', {}


def fetch_policy(transaction):
    client = Client(PDF_WSDL_PATH, username=PDF_USERNAME, password=PDF_PASSWORD)
    ltype = 'POLSCHD'
    client.options.plugins = [_AttributePlugin()]
    resp = client.service.GET_PDF(transaction.policy_number, ltype)
    request_logger.info('\nRELIGARE TRANSACTION: %s\n PDF REQUEST SENT:\n%s\n' % (transaction.transaction_id, client.last_sent()))
    #request_logger.info('\nRELIGARE TRANSACTION: %s\n PDF RESPONSE RECEIVED:\n%s\n' % (transaction.transaction_id, client.last_received()))
    xresp = ET.fromstring('<root>' + resp + '</root>')
    pdf_data = xresp.find('StreamData').text
    data = base64.b64decode(pdf_data)
    # WRITE THE PDF RESPONSE TO A LOCAL FILE
    transaction.update_policy(data, send_mail=False)


def post_payment(payment_response, transaction):
    redirect_url = '/health-plan/payment/transaction/%s/failure/' % transaction.transaction_id
    is_redirect = True
    #{u'transactionRefNum': u'403993715509148194', u'policyNumber': u'', u'uwDecision': u'DRAFT',
    # u'errorFlag': u'', u'errorMsg': u''}
    resp = payment_response

    if 'errorMsg' in resp:
        transaction.extra['error_msg'] = resp['errorMsg'];

    transaction.extra['payment_response'] = resp
    transaction.payment_response = json.dumps(resp)
    transaction.policy_number = resp['policyNumber']
    transaction.policy_start_date = datetime.datetime.today() + relativedelta(days=1)
    policy_term = transaction.insured_details_json['form_data']['proposer']['personal']['policy_term']['value1']
    transaction.policy_end_date = transaction.policy_start_date + relativedelta(days=-1, years=policy_term)
    transaction.save()

    if not transaction.extra['payment_response']['errorFlag']:
        transaction.payment_done = True
        transaction.payment_on = datetime.datetime.now()
        transaction.save()
        #PDF FETCHING
        try:
        # if True:
            time.sleep(3)
            fetch_policy(transaction)
        # else:
        #     pass
        except Exception as e:
            print 'Exception', str(e.message), e.args

        redirect_url = '/health-plan/payment/transaction/%s/success/' % transaction.transaction_id
    else:
        transaction.status = 'FAILED'
        transaction.save()
    return is_redirect, redirect_url, '', {}
