from health_product.insurer import settings as insurer_settings
from health_product.models import *
from wiki.models import *
import urllib2, urllib
import datetime
import ast
from dateutil.relativedelta import *


def save_transaction(transaction):
    red_flag = []
    if 'medicalQuestionsArray' in transaction.insured_details_json.keys():
        for i in transaction.insured_details_json['medicalQuestionsArray']:
            for item in i['questionArray']:
                if 'answer' in item.keys() and item['answer']:
                    for o in item['options']:
                        if 'is_correct' in o.keys():
                            if o['id'] == item['answer'] and not o['is_correct']:
                                if item['code'] == 't_and_c':
                                    red_flag.append({'question': 'I hereby agree to the Terms and Conditions', 'answer': o['id']})
                                # else:
                                #     red_flag.append({'question': item['content'], 'answer': o['id']})
                        elif item['answer'].strip() and item['answer'].upper() != 'NONE':
                            red_flag.append({'question': item['content'], 'answer': o['id']})
            for item in i['conditionalQuestionArray']:
                if 'answer' in item.keys() and item['answer']:
                    for o in item['options']:
                        if 'is_correct' in o.keys():
                            if o['id'] == item['answer'] and not o['is_correct']:
                                red_flag.append({'question': item['content'], 'answer': o['id']})
                        # elif item['answer'].strip():
                        #     red_flag.append({'question': item['content'], 'answer': o['id']})
    transaction.extra['red_flag'] = red_flag
    transaction.save()
    return transaction, red_flag

