INSURER_MAP = {
    1: "apollo_munich",
    2: "star_health",
    3: "icici_lombard",
    4: "bajaj_allianz",
    5: "max_bupa",
    6: "religare",
    7: "tata_aig",
    8: "new_india",
    9: "bharti_axa",
    10: "reliance",
    11: "landt",
    12: "cholamandalam",
    13: "hdfc_ergo",
    14: "iffco_tokio",
    15: "cigna_ttk",
    16: "oriental",
}

REPORT_MAP = {
    7: "tata_aig",
}

INSURER_SAVE_MAP = {
    1: "apollo_munich",
    6: "religare",
    7: "tata_aig",
    11: "landt",
}


def get_product_details(slab):
    product_id = slab.health_product.id
    insurer_id = slab.health_product.insurer.id
    insurer = ''
    product = ''

    if insurer_id == 1:
        insurer = 'Apollo Munich'
        if product_id in [3, 7, 47, 50, 52, 53]:
            product = 'Easy Health'
        elif product_id in [48, 49]:
            product = 'Optima Restore'
        elif product_id in [90, 91]:
            product = 'Optima Super'

    elif insurer_id == 2:
        insurer = 'Star'
        if product_id in [36]:
            product = 'Star Comprehensive'
        elif product_id in [37]:
            product = 'Mediclassic (Individual)'
        elif product_id in [64]:
            product = 'Senior Citizen Red Carpet'
        elif product_id in [65]:
            product = 'Family Health Optima'

    elif insurer_id == 3:
        insurer = 'ICICI Lombard'

    elif insurer_id == 4:
        insurer = 'Bajaj Allianz'
        if product_id in [41, 46]:
            product = 'Health Guard'

    elif insurer_id == 5:
        insurer = 'Max Bupa'
        if product_id in [10]:
            product = 'Health Companion - FAMILY'
        elif product_id in [9]:
            product = 'Health Companion - INDIVIDUAL'

    elif insurer_id == 6:
        insurer = 'Religare'
        if product_id in [5, 8, 61, 62]:
            product = 'Care'
        elif product_id in [82, 83]:
            product = 'Enhance'

    elif insurer_id == 7:
        insurer = 'Tata Aig'
        if product_id in [4, 13]:
            product = 'MediPrime'

    elif insurer_id == 8:
        insurer = 'new_india'

    elif insurer_id == 9:
        insurer = 'Bharti AXA'
        if product_id in [19, 21, 68, 69]:
            product = 'Smart Health Insurance'

    elif insurer_id == 10:
        insurer = 'Reliance'
        if product_id in [35, 38]:
            product = 'HealthWise'

    elif insurer_id == 11:
        insurer = 'L&T'
        if product_id in [22, 25, 26, 27, 28, 29, 31, 32]:
            product = 'Medisure Classic'
        elif product_id in [23, 33]:
            product = 'Medisure Prime'
        elif product_id in [70, 71]:
            product = 'Medisure Super Top Up'

    elif insurer_id == 12:
        insurer = 'Cholamandalam'
        if product_id in [39, 40, 42, 43, 44, 45]:
            product = 'Healthline'

    elif insurer_id == 13:
        insurer = 'HDFC Ergo'
        if product_id in [54, 55]:
            product = 'Health Suraksha'
        elif product_id in [86, 88]:
            product = 'Health Suraksha Top Up'

    elif insurer_id == 14:
        insurer = 'IFFCO TOKIO'
        if product_id in [56]:
            product = 'Individual Medishield'
        elif product_id in [57, 58]:
            product = 'Swasthya Kavach'

    elif insurer_id == 15:
        insurer = 'TTK Cigna'
        if product_id in [59, 60, 76, 77, 78, 79]:
            product = 'ProHealth'

    elif insurer_id == 16:
        insurer = 'Oriental'
        if product_id in [92, 93]:
            product = 'Happy Family Floater'
        elif product_id in [87]:
            product = 'Individual Mediclaim'

    return {'insurer_title': insurer, 'product_title': product}

# Health Product Id's
INSURER_SPECIFIC_SCORE = [
    92, 93,     # Oriental
    84, 85,     # New India Assurance
]

PLAN_TEMPLATE_MAP = {
    # TATA AIG
    # 4 : 'tata_aig/base',
    # 13 : 'tata_aig/base'
}

STAGE_VIEW_DEFAULT = {
    'generic/stage_1': 'generic/genstep-1',
    'generic/stage_2': 'generic/genstep-2',
    'generic/stage_3': '/generic/genstep-3',
    'generic/stage_4': 'generic/genstep-4',
    'generic/stage_5': 'generic/genstep-5',
    'generic/stage_6': 'generic/genstep-6'
}

STAGE_VIEW = {
    # tuple(): {
    #     'html': 'jsfile-stage_number'
    # }
    # TATA AIG
    tuple([4, 13]): {
        'generic/stage_1': 'tata_aig/stage_1-1',
        'generic/stage_2': 'generic/genstep-2',
        'generic/stage_3': 'generic/genstep-3',
        'generic/stage_4': 'generic/genstep-4',
        'generic/stage_5': 'generic/stage_5-5',
        'tata_aig/stage_6': 'tata_aig/stage_6-6',
        'tata_aig/stage_7': 'generic/genstep-7'
    },
    # L & T
    tuple([33, 23, 25, 27, 29, 32, 22, 26, 28, 31, 70, 71]): {
        'generic/stage_1': 'landt/stage_1-1',
        'generic/stage_2': 'generic/genstep-2',
        'generic/stage_3': 'generic/genstep-3',
        'generic/stage_4': 'generic/genstep-4',
        'landt/stage_5': 'landt/stage_5-5',
        'landt/stage_6': 'generic/genstep-6'
    },
    # Max Bupa
    tuple([9, 10]): {
        'generic/stage_1': 'generic/genstep-1',
        'generic/stage_2': 'generic/genstep-2',
        'generic/stage_3': 'generic/genstep-3',
        'generic/stage_4': 'generic/genstep-4',
        'max_bupa/stage_5': 'max_bupa/stage_5-5',
        'max_bupa/stage_6': 'generic/genstep-6'
    },
    # Religare
    tuple([5, 8, 61, 62, 82, 83]): {
        'generic/stage_1': 'generic/genstep-1',
        'generic/stage_2': 'generic/genstep-2',
        'religare/stage_3': 'generic/genstep-3',
        'religare/stage_4': 'religare/stage_4-4',
        'religare/stage_5': 'generic/genstep-5'
    },
    # HDFC
    tuple([54, 55, 86, 88]): {
        'generic/stage_1': 'generic/genstep-1',
        'generic/stage_2': 'generic/genstep-2',
        'generic/stage_3': 'generic/genstep-3',
        'generic/stage_4': 'generic/genstep-4',
        'generic/stage_5': 'generic/stage_5-5',
        'hdfc_ergo/stage_6': 'generic/genstep-6'
    },
    # APOLLO
    tuple([3, 7, 47, 48, 49, 50, 52, 53, 90, 91]): {
        'generic/stage_1': 'generic/genstep-1',
        'generic/stage_2': 'generic/genstep-2',
        'generic/stage_3': 'generic/genstep-3',
        'generic/stage_4': 'generic/genstep-4',
        'apollo_munich/stage_5': 'apollo_munich/stage_5-5',
        'apollo_munich/stage_6': 'apollo_munich/stage_6-6',
        'apollo_munich/stage_7': 'generic/genstep-7'
    },
    # BAJAJ ALLIANZ
    tuple([41, 46]): {
        'generic/stage_1': 'generic/genstep-1',
        'generic/stage_2': 'generic/genstep-2',
        'generic/stage_3': 'generic/genstep-3',
        'generic/stage_4': 'generic/genstep-4',
        'bajaj_allianz/stage_5': 'bajaj_allianz/stage_5-5',
        'bajaj_allianz/stage_6': 'generic/genstep-6'
    },
    # RELIANCE
    tuple([34, 35, 38]): {
        'generic/stage_1': 'generic/genstep-1',
        'generic/stage_2': 'generic/genstep-2',
        'generic/stage_3': 'generic/genstep-3',
        'generic/stage_4': 'generic/genstep-4',
        'reliance/stage_5': 'reliance/stage_5-5',
        'reliance/stage_6': 'generic/genstep-6'
    },
    # BHARTI AXA
    tuple([19, 21, 68, 69]): {
        'generic/stage_1': 'generic/genstep-1',
        'generic/stage_2': 'generic/genstep-2',
        'generic/stage_3': 'generic/genstep-3',
        'generic/stage_4': 'generic/genstep-4',
        'bharti_axa/stage_5': 'bharti_axa/stage_5-5',
        'bharti_axa/stage_6': 'generic/genstep-6'
    },
    # IFFCO TOKIO
    tuple([56, 57, 58]): {
        'generic/stage_1': 'generic/genstep-1',
        'generic/stage_2': 'generic/genstep-2',
        'generic/stage_3': '/generic/genstep-3',
        'iffco_tokio/stage_4': 'generic/genstep-4',
        'generic/stage_5': 'iffco_tokio/stage_5-5',
        'iffco_tokio/stage_6': 'generic/genstep-6'
    },
    # STAR HEALTH
    tuple([36, 37, 64, 65]): {
        'generic/stage_1': 'generic/genstep-1',
        'generic/stage_2': 'generic/genstep-2',
        'star_health/stage_3': 'star_health/stage_3-3',
        'star_health/stage_4': 'star_health/stage_4-4',
        'star_health/stage_5': 'generic/genstep-5'
    },
    # ORIENTAL
    tuple([92, 93, 87]): {
        'generic/stage_1': 'generic/genstep-1',
        'generic/stage_2': 'generic/genstep-2',
        'generic/stage_3': 'generic/genstep-3',
        'generic/stage_4': 'generic/genstep-4',
        'oriental/stage_5': 'oriental/stage_5-5',
        'oriental/stage_6': 'generic/genstep-6',
    },

    # CIGNA
    tuple([59, 60, 76, 77, 78, 79]): {
        'generic/stage_1': 'cigna_ttk/stage_1-1',
        'generic/stage_2': 'cigna_ttk/stage_2-2',
        'generic/stage_3': 'generic/genstep-3',
        'generic/stage_4': 'generic/genstep-4',
        'cigna_ttk/stage_5': 'cigna_ttk/stage_5-5',
        'cigna_ttk/stage_6': 'generic/genstep-6'

    },
}


STAGE_SAVE_MAP = {
    'stage_4': {
        tuple([36, 37, 64, 65]): 'star_health.saveall.four'
    },
    'stage_5': {
        tuple([33, 23, 25, 27, 29, 32, 22, 26, 28, 31, 70, 71]): 'landt.classic.five',
        tuple([3, 7, 47, 48, 49, 50, 52, 53, 90, 91]): 'apollo_munich.saveall.five',
        tuple([41, 46]): 'bajaj_allianz.saveall.five',
        tuple([5, 8, 61, 62, 82, 83]): 'religare.saveall.four',
        tuple([59, 60, 76, 77, 78, 79]): 'cigna_ttk.saveall.five',
        tuple([56, 57, 58]): 'iffco_tokio.saveall.five',
        tuple([19, 21, 68, 69]): 'bharti_axa.saveall.five',
        tuple([54, 55, 86, 88]): 'hdfc_ergo.saveall.five',
        tuple([34, 35, 38]): 'reliance.saveall.five',
        tuple([4, 13]): 'tata_aig.saveall.five',
        tuple([36, 37, 64]): 'star_health.saveall.four'
    },
    'stage_6': {
        tuple([3, 7, 47, 48, 49, 50, 52, 53, 90, 91]): 'apollo_munich.saveall.six',
        tuple([4, 13]): 'tata_aig.saveall.six',
    },
    'stage_7': {
        tuple([4, 13]): 'tata_aig.saveall.seven',
        }

}
