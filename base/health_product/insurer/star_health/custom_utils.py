import urllib2
import json
from django.conf import settings
from health_product.insurer.star_health.credentials import *


def is_nil(value):
     return value.upper() in ['NO', 'NONE', 'NIL', '']


def fetch_cities(pincode):
    city_url = '%s/api/policy/city/details?APIKEY=%s&SECRETKEY=%s&pincode=%s' % (
        STARHEALTH_API_URL, STARHEALTH_API_KEY, STARHEALTH_SECRET_KEY, pincode)
    try:
        req = urllib2.Request(url=city_url)
        res = urllib2.urlopen(req).read()
        res = res.replace('city_id', 'id').replace('city_name', 'name')
        city_response = json.loads(res)
        return city_response
    except urllib2.HTTPError, err:
        return {}


def fetch_areas(pincode, city_id):
    if not city_id:
        return {}
    area_url = '%s/api/policy/address/details?APIKEY=%s&SECRETKEY=%s&pincode=%s&city_id=%s' % (
        STARHEALTH_API_URL, STARHEALTH_API_KEY, STARHEALTH_SECRET_KEY, pincode, city_id)
    try:
        req = urllib2.Request(url=area_url)
        res = urllib2.urlopen(req).read()
        res = res.replace('areaID','id').replace('areaName','name')
        area_response = json.loads(res)
        if area_response['area']:
            return area_response
        else:
            return  {'error': 'No matching areas for this city.'}
    except urllib2.HTTPError, err:
        return {'error':'This Pincode is not valid for this policy.'}
