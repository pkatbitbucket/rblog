from wiki.models import City
from health_product.insurer.star_health import preset
from health_product.insurer import custom_utils
from health_product.insurer.star_health import custom_utils as star_utils


class Formic():
    def __init__(self, transaction, is_self_present, policy_code):
        self.transaction = transaction
        self.is_self_present = is_self_present
        self.policy_code = policy_code
        self.proposer_data = {
            'pincode': transaction.extra['pincode'],
            'city' : City.objects.get(id=transaction.extra['city']['id']).name,
            'state' : City.objects.get(id=transaction.extra['city']['id']).state.name,
            'email': ''
        }


    def add_parent_segment(self, segment_id, tag_id, details, question_type, select_options=None):
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': details, 'data': []}
        row = []
        if question_type == 'MultipleChoiceField':
            default_options = [
                {'id': 'Yes', 'name': 'Yes', 'is_correct': False},
                {'id': 'No', 'name': 'No', 'is_correct': True}
            ]
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='MultipleChoiceField',
                    code='%s_question_%s' % (member['id'], details['question_code']),
                    details=member,
                    label=details.get('question_text', ''),
                    span=4,
                    required=details.get('is_required', True),
                    options=select_options if select_options else default_options,
                    css_class='bottom-margin',
                    event=', click : $parent.root_context.stage_4.any_yes_check',
                    template_option='question'
                ))
            new_segment['data'].append(row)
        elif question_type == 'DropDownField':
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='DropDownField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    options=select_options,
                    details=member,
                    label=details.get('question_text', ''),
                    span=2,
                    required=details.get('is_required', True),
                    css_class='bottom-margin',
                    event=', event : {change : root_context.stage_4.any_yes_check}',
                    template_option='question'
                ))
            new_segment['data'].append(row)
        elif question_type == 'TextField':
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='TextField',
                    label=details.get('question_text', ''),
                    code='%s_question_%s' % (member['id'], details['question_code']),
                    span=2,
                    required=details.get('is_required', True)
                    ))
            new_segment['data'].append(row)

        return new_segment

    def add_conditional_segment(self, segment_id, tag_id, parent_question_code, details, question_type):
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : details, 'data' : []}
        row = []
        if question_type == 'MultipleChoiceField':
            new_segment['details']['tier'] = 1
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='MultipleChoiceField',
                code='%s_question_%s' % ('combo', tag_id),
                value='No',
                label=details.get('question_text', ''),
                span=4,
                required=False,
                css_class='bottom-margin',
                dependant_parent='%s_question_%s' % ('combo', parent_question_code),
                event=', click : $parent.root_context.stage_4.any_yes_check',
                only_if='root_context.stage_4.check_dict.get(\'%s_question_%s\')' % ('combo', parent_question_code),
                template_option='question'
            ))
            new_segment['data'].append(row)
        elif question_type == 'TextField':
            new_segment['details']['tier'] = 2
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='TextField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    span=2,
                    label=details.get('question_text', ''),
                    dependant_parent='%s_question_%s' % (member['id'], parent_question_code),
                    required=False,
                    only_if='root_context.stage_4.check_dict.get(\'%s_question_%s\')' % (member['id'], parent_question_code),
                ))
            new_segment['data'].append(row)
        return new_segment

    def stage_1(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'personal'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            label='First Name',
            code='first_name',
            max_length=23
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            label='Last Name',
            code='last_name',
            max_length=23
        ))
        if self.is_self_present:
            member = [m for m in self.transaction.extra['members'] if m['id'] == 'self'][0]
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                span=3,
                min_age=18,
                max_age=100,
                default_age=member['age'],
            ))
        else:
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                min_age=18,
                max_age=100,
                default_age=18,
            ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Gender',
            code='gender',
            value=1 if self.transaction.extra['gender'] == 'MALE' else 2,
            options=preset.GENDER_OPTIONS,
        ))
        # row.append(custom_utils.new_field(
        #     saved_data=saved_data,
        #     field_type='DropDownField',
        #     label='Marital Status',
        #     code='marital_status',
        #     value='MARRIED' if len(self.transaction.extra['members']) > 1 else 'SINGLE',
        #     options=preset.MARITAL_STATUS_OPTIONS
        # ))
        # row.append(custom_utils.new_field(
        #     saved_data=saved_data,
        #     field_type='DropDownField',
        #     label='Occupation',
        #     code='occupation',
        #     options=preset.OCCUPATION_OPTIONS['mcinew'],
        #     ))
        new_segment['data'].append(row)

        stage.append(new_segment)
        return {'template' : 'stage_1', 'data' : stage}

    def stage_2(self):
        stage = []

        for member in self.transaction.extra['members']:
            segment_id = 'insured'
            tag_id = member['id']
            template_switch = 'dynamic'
            if tag_id == 'self':
                template_switch = 'static'
            try:
                saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : member, 'data' : []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                label='First Name',
                code='first_name',
                max_length=23,
                template_option=template_switch,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='CharField',
                label='Last Name',
                code='last_name',
                max_length=23,
                template_option=template_switch,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                template_option=template_switch,
                span=3,
                default_age = member['age'],
                min_age = 18 if member['type']=='adult' else 0,
                max_age = 100,
            ))
            new_segment['data'].append(row)

            if self.policy_code in ['mcinew', 'redcarpet']:
                row = []
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='DropDownField',
                    label='Occupation',
                    code='occupation',
                    options=preset.OCCUPATION_OPTIONS[self.policy_code],
                ))
                new_segment['data'].append(row)

            stage.append(new_segment)
        return {'template' : 'stage_2', 'data' : stage}

    def stage_3(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'address'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address1',
            label='Flat/House No, Street',
            span=12,
            css_class='bottom-margin',
            max_length=30
        ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address2',
            label='Area/Locality',
            span=12,
            css_class='bottom-margin',
            max_length=30,
            ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='landmark',
            label='Landmark',
            span=12,
            css_class='bottom-margin',
            max_length=30,
            required=False
        ))
        new_segment['data'].append(row)

        row = []
        fetched_cities = star_utils.fetch_cities(self.transaction.extra['pincode'])
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='SearchableDropDownField',
            code='city',
            label='City',
            span=6,
            css_class='bottom-margin',
            options=fetched_cities['city']
        ))
        area_list = star_utils.fetch_areas(self.transaction.extra['pincode'], saved_data.get('city',{}).get('value')).get('area',[])
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='SearchableDropDownField',
            code='area',
            label='Area',
            span=6,
            css_class='bottom-margin',
            options= area_list,
        ))
        new_segment['data'].append(row)
        row = []
        # pincode = Pincode.objects.filter(pincode=self.transaction.extra['pincode'])[0]
        # state = fetched_cities['state_name']
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='state',
            label='State',
            value=self.proposer_data['state'],
            span=6,
            template_option='static'
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='IntegerField',
            code='pincode',
            label='Pincode',
            value=str(self.proposer_data['pincode']),
            span=6,
            template_option='static'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)


        tag_id = 'contact'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='email',
            label='Email',
            span=12,
            css_class='bottom-margin',
            max_length=50,
            email=True
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MobileField',
            code='mobile',
            label='Mobile',
            css_class='bottom-margin',
            span=12
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='LandlineField',
            code='landline',
            label='Landline',
            span=12,
            css_class='bottom-margin',
            required=False
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template' : 'stage_3', 'data' : stage}

    def stage_4(self):
        stage = []
        #---------- Question Unit Begins -----------
        question_code = 'pre_existing'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Do you have any pre-existing diseases or have been taking drugs/Medicines regularly during the last 1 year for any disease/illness (other than Hypertension / Diabetes/Hypothyroidism)?',
            'question_code': question_code,
            'is_required': True,
            'trigger_any_yes_check': True,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        tag_id = 'disease'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Please specify the name of the Disease/name of the Drugs you have been taking',
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))

        #---------- Question Unit Begins -----------
        question_code = 'cancer'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Have you been suffering from Cancer?',
            'help_text': '',
            'question_code': question_code,
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        #---------- Question Unit Begins -----------
        question_code = 'renal'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Have you been suffering or ever suffered from Chronic Kidney Diseases?',
            'help_text': '',
            'question_code': question_code,
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        #---------- Question Unit Begins -----------
        question_code = 'heart_diseases'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Have you been suffering or ever suffered from Heart Diseases?',
            'help_text': '',
            'question_code': question_code,
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        #---------- Question Unit Begins -----------
        question_code = 'psychiatric_disorder'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Have you been suffering or ever suffered from Psychiatric Disorders?',
            'help_text': '',
            'question_code': question_code,
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #---------- Question Unit Begins -----------

        question_code = 'social_status'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'trigger_any_yes_check': True,
            'conditional': 'NO'
        }
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': details, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='%s_question_%s' % (tag_id, question_code),
            label='Social Status',
            helptext='Please choose "Yes" in case of BPL families, Disabled Persons etc, Persons working in UnOrganized/Informal Sectors',
            span=12,
            value='No',
            css_class='bottom-margin',
            event=', click : $parent.root_context.stage_4.any_yes_check',
            template_option='question'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)




        # question_code = 'social_status'
        # tag_id = 'combo'
        # segment_id = 'medical_question_%s' % question_code
        # details = {
        #     'conditional': 'NO',
        #     'question_text': 'Social Status',
        #     'help_text': 'Please choose "Yes" in case of BPL families, Disabled Persons etc, Persons working in UnOrganized/Informal Sectors',
        #     'question_code': question_code,
        #     'trigger_any_yes_check': True,
        #     'show_header': True
        # }
        # stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        #CONDITIONAL SEGMENT
        tag_id = 'social_status_unorganized'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Please choose "Yes", if you are working in UnOrganized Sector',
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))

        #CONDITIONAL SEGMENT
        tag_id = 'social_status_bpl'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Please choose "Yes", if you live below the poverty line (BPL)',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))

        #CONDITIONAL SEGMENT
        tag_id = 'social_status_disabled'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Please choose "Yes", if you are Physically Handicaped/Disabled',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))

        #CONDITIONAL SEGMENT
        tag_id = 'social_status_informal'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Please choose "Yes", if you are working in Informal Sector',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))

        return {'template' : 'stage_4', 'data' : stage}

    def stage_4_redcarpet(self):
        stage = []
        #---------- Question Unit Begins -----------
        question_code = 'pre_existing'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Do you have any pre-existing diseases or have been taking drugs/Medicines regularly during the last 1 year for any disease/illness (other than Hypertension / Diabetes/Hypothyroidism)?',
            'question_code': question_code,
            'is_required': True,
            'trigger_any_yes_check': True,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        tag_id = 'disease'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Please specify the name of the Disease/name of the Drugs you have been taking',
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))

        #---------- Question Unit Begins -----------
        question_code = 'pre_existing_before_twelve_months'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Have you suffered from any illness prior to one year',
            'question_code': question_code,
            'is_required': True,
            'trigger_any_yes_check': True,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        tag_id = 'disease'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Please specify the name of the Disease',
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'TextField'))
        #---------- Question Unit Begins -----------
        question_code = 'cancer'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Have you been suffering from Cancer?',
            'help_text': '',
            'question_code': question_code,
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        #---------- Question Unit Begins -----------
        question_code = 'kidney'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Have you been suffering or ever suffered from Chronic Kidney Diseases?',
            'help_text': '',
            'question_code': question_code,
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        #---------- Question Unit Begins -----------
        question_code = 'brain'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Have you been suffering or ever suffered from any Brain Disease (CVA / Brain Stroke)?',
            'help_text': '',
            'question_code': question_code,
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        #---------- Question Unit Begins -----------
        question_code = 'alzheimer'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Have you been suffering or ever suffered from Alzheimer\'s disease?',
            'help_text': '',
            'question_code': question_code,
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        #---------- Question Unit Begins -----------
        question_code = 'parkinson'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Have you been suffering or ever suffered from Parkinson\'s Disease?',
            'help_text': '',
            'question_code': question_code,
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        #---------- Question Unit Begins -----------
        question_code = 'psychiatric_disorder'
        tag_id = 'combo'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Have you been suffering or ever suffered from Psychiatric Disorders?',
            'help_text': '',
            'question_code': question_code,
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        #---------- Question Unit Begins -----------

        question_code = 'social_status'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'trigger_any_yes_check': True,
            'conditional': 'NO'
        }
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': details, 'data': []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='%s_question_%s' % (tag_id, question_code),
            label='Social Status',
            helptext='Please choose "Yes" in case of BPL families, Disabled Persons etc, Persons working in UnOrganized/Informal Sectors',
            span=12,
            value='No',
            css_class='bottom-margin',
            event=', click : $parent.root_context.stage_4.any_yes_check',
            template_option='question'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)




        # question_code = 'social_status'
        # tag_id = 'combo'
        # segment_id = 'medical_question_%s' % question_code
        # details = {
        #     'conditional': 'NO',
        #     'question_text': 'Social Status',
        #     'help_text': 'Please choose "Yes" in case of BPL families, Disabled Persons etc, Persons working in UnOrganized/Informal Sectors',
        #     'question_code': question_code,
        #     'trigger_any_yes_check': True,
        #     'show_header': True
        # }
        # stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        #CONDITIONAL SEGMENT
        tag_id = 'social_status_unorganized'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Please choose "Yes", if you are working in UnOrganized Sector',
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))

        #CONDITIONAL SEGMENT
        tag_id = 'social_status_bpl'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Please choose "Yes", if you live below the poverty line (BPL)',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))

        #CONDITIONAL SEGMENT
        tag_id = 'social_status_disabled'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Please choose "Yes", if you are Physically Handicaped/Disabled',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))

        #CONDITIONAL SEGMENT
        tag_id = 'social_status_informal'
        parent_question_code = question_code
        details = {
            'conditional': 'YES',
            'parent_question_code': parent_question_code,
            'question_text': 'Please choose "Yes", if you are working in Informal Sector',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_conditional_segment(segment_id, tag_id, parent_question_code, details, 'MultipleChoiceField'))

        return {'template' : 'stage_4', 'data' : stage}


    def stage_5(self):
        stage = []
        segment_id = 'terms'
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}
        row = []
        t_and_c_label = ('I hereby confirm<br />'
                         '- I have read and agree to the Terms and Conditions of ')
        if self.transaction.slab.health_product.policy_wordings:
            t_and_c_label += ('the <a style="text-decoration:none;cursor:pointer;" '
                              'onclick=\'window.open("%s","","width=400, height=400")\'>policy</a> and '
                             ) % self.transaction.slab.health_product.policy_wordings.url
        t_and_c_label += ('the <a style="cursor:pointer;" class="call_overlay">website</a>;<br />'
                          '- the declarations made herewith are true and correct;<br />'
                          '- appointment of Coverfox Insurance Brokers Pvt. Ltd. as my Insurance Broker, '
                          'and authorize them to represent me to Insurance Companies/Agencies for my Insurance needs.'
                         )
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='t_and_c',
            label=t_and_c_label,
            span=12,
            options=[
                {'id' : 'Yes', 'name' : 'Yes', 'is_correct' : True},
                {'id' : 'No', 'name' : 'No', 'is_correct' : False}
            ],
            css_class='bottom-margin',
            template_option='terms'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template' : 'stage_5', 'data' : stage}

    def get_form_data_mcinew(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5()
            ]
        return data

    def get_form_data_comprehensive(self):
        stage_2 = self.stage_2();
        segment_id = 'accidental_coverage'
        tag_id ='accidental_coverage'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}

        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}
        row = []

        members = self.transaction.extra['members']
        options = [];
        for m in members:
            if m['type']!='kid' and m['age']<70:
                options.append({'id':m['id'],'name':m['displayName']})
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            code='accidental_coverage',
            label='This plan provides a free accidental coverage for one of the insured members. Please select a member for which you want to get accidental coverage.',
            span=6,
            options=options,
            required = True,
            css_class='bottom-margin',
        ))
        new_segment['data'].append(row)
        stage_2['data'].append(new_segment)

        data = [
            self.stage_1(),
            stage_2,
            self.stage_3(),
            self.stage_4(),
            self.stage_5()
        ]
        return data

    def get_form_data_redcarpet(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4_redcarpet(),
            self.stage_5()
        ]
        return data

    def get_form_data_fhonew(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5()
        ]
        return data


def get_dynamic_form(transaction):
    transaction, plan_data, proposer_data, is_self_present = custom_utils.standard_form_data_cleanup(transaction)
    #TOGGLES WHERE MEDICAL CASES ARE ALLOWED OR NOT
    plan_data['medical_firewall'] = True

    if transaction.slab.health_product.id in [37]:
        policy_code = 'mcinew'
    elif transaction.slab.health_product.id in [36]:
        policy_code = 'comprehensive'
    elif transaction.slab.health_product.id in [18, 65]:
        policy_code = 'fhonew'
    elif transaction.slab.health_product.id in [64]:
        policy_code = 'redcarpet'

    obj = Formic(transaction, is_self_present, policy_code)
    stages = getattr(obj, 'get_form_data_%s' % policy_code.lower(), '')()

    terms_and_conditions = "<div style='padding:20px 40px 70px 20px;'>" \
                           "<h3>Coverfox: Terms and Conditions</h3>" \
                           "<p>Coverfox Insurance Broking Pvt. (hereinafter referred to as Coverfox) Limited operates " \
                           "the website <a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> to provide " \
                           "consumers choice and an easy way to buy Insurance " \
                           "products from multiple Insurance companies. Coverfox is not an insurance company. Coverfox " \
                           "is a licensed Insurance Broking Company holding a Broking licence from the Indian " \
                           "Insurance Regulator - Insurance Regulatory and Development authority. " \
                           "<a href='/docs/irda-licence/' target='_blank'>(View IRDA License)</a></p>" \
                           "<p>Our participating insurance providers supply some of the information available on the " \
                           "Website and therefore there may be inaccuracies in the Website Information over which " \
                           "Coverfox has limited control.</p>" \
                           "<p>Coverfox does not warrant or guarantee the: Timeliness, accuracy or completeness of " \
                           "the Website Information; or Quality of the results obtained from the use of the Website.</p>" \
                           "<p>To the maximum extent permitted by law, Coverfox has no liability in relation to or " \
                           "arising out of the Website Information and Website recommendations. You are responsible " \
                           "for the final choice of your product and you should take time to read through all " \
                           "information supplied before proceeding. If you are in any doubt regarding a product or " \
                           "its terms you should seek further advice from Coverfox or the relevant participating " \
                           "provider before choosing your product.</p>" \
                           "<p>Coverfox may pass on your personal information to the relevant participating provider " \
                           "if you apply to purchase a product through " \
                           "<a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> , however, Coverfox does " \
                           "not guarantee when or if you will actually acquire the product that you have chosen. " \
                           "Coverfox does not accept any liability arising out of circumstances where there is delay " \
                           "in you acquiring the product you have chosen.</p>" \
                           "<p>Please note that Coverfox is only collecting or assisting in collecting  the premium " \
                           "deposit on behalf of the insurer you have chosen to buy the policy. The acceptance of " \
                           "the deposit as premium and final issuance of the policy is subject to the underwriting " \
                           "norms and discretion of the Insurer whose policy you have chosen to buy on which Coverfox " \
                           "has no control. Coverfox will ensure that the amount is refunded by the insurer in case " \
                           "there is no ultimate issuance of policy.</p>" \
                           "<p><button class='btn btn-lg cf-btn btn-red hide_overlay'>Close</button></p>" \
                           "</div>"

    slate_list = [
        {'id': 'slate_1', 'name': 'Proposer', 'class' : '', 'description' : 'The person who is paying for the policy'},
        {'id': 'slate_2', 'name' : 'Insured Members', 'class' : '', 'description' : 'People being insured under the policy'},
        {'id': 'slate_3', 'name' : 'Contact Info.', 'class' : '', 'description' : 'Contact Information of the proposer'},
        {'id': 'slate_4', 'name' : 'Medical History', 'class' : '', 'description' : 'Medical History of the people being insured'},
        {'id': 'slate_5', 'name' : 'T & C', 'class' : 'last', 'description' : 'Mandatory terms and conditions', 'style' : " 'width', '0px' "}
    ]
    policy_date_disclaimer = 'NOTE: The policy period will start immediately from the date this transaction gets processed'
    data = {
        'slate_list' : slate_list,
        'proposer_data' : proposer_data,
        'plan_data' : plan_data,
        'stages' : stages,
        'last_stage' : stages[-1]['template'],
        'terms_and_conditions' : terms_and_conditions,
        'policy_date_disclaimer' : policy_date_disclaimer
    }

    return data
