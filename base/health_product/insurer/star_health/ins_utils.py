import putils
from health_product.models import *
def download_buy_form(transaction):
    order = []
    rows = []

    form_data = transaction.insured_details_json['form_data']
    proposer_data = form_data['proposer']['personal']
    proposer_address = form_data['proposer']['address']
    proposer_contact = form_data['proposer']['contact']
    insured_data = form_data['insured']
    social_status = form_data['medical_question_social_status']
    medical_questions = ['pre_existing', 'cancer', 'renal', 'heart_diseases', 'psychiatric_disorder', 'drugs']
    social_questions = ['combo', 'social_status_unorganized', 'social_status_bpl', 'social_status_disabled', 'social_status_informal']


    memberList = []
    for member in insured_data:
        memberList.append(member)


    rows.append(['Proposer Details'])
    rows.append(['Name', ' '.join([proposer_data['first_name']['value'], proposer_data['last_name']['value']])])
    rows.append(['Gender', proposer_data['gender']['display_value']])
    rows.append(['Date of Birth', proposer_data['date_of_birth']['display_value']])
    rows.append([])


    rows.append(['Insured Members'])
    for member in memberList:
        rows.append(['Which Insured Member', member])
        rows.append(['Name', ' '.join([insured_data[member]['first_name']['display_value'], insured_data[member]['last_name']['display_value']])])
        rows.append(['Date of Birth', insured_data[member]['date_of_birth']['display_value']])
        rows.append([])


    rows.append(['Proposer Contact Details'])
    rows.append(['Address', ', '.join([proposer_address['address1']['value'], proposer_address['address2']['value'], proposer_address['landmark']['value'], proposer_address['area']['display_value'], proposer_address['city']['display_value'], proposer_address['state']['value'], proposer_address['pincode']['value']])])
    rows.append(['Email', proposer_contact['email']['value']])
    rows.append(['LandLine', proposer_contact['landline']['std_code']+'-'+proposer_contact['landline']['landline']])
    rows.append(['Mobile', proposer_contact['mobile']['value']])
    rows.append([])


    rows.append(['Medical Details'])
    rows.append(['Members', '']+memberList)


    for question in medical_questions:
        row = []
        row.append(form_data['medical_question_'+question]['combo'].values()[0]['label'])
        row.append('')
        for member in memberList:
            value = form_data['medical_question_'+question]['combo'][member+'_question_'+question]['display_value']
            row.append(value if value else 'None')
        rows.append(row)
        rows.append([])


    for question in social_questions:
        if question == 'combo':
            rows.append([social_status[question]['combo_question_social_status']['label'], social_status[question]['combo_question_social_status']['display_value']])
            rows.append([])
        else:
            rows.append([social_status[question]['combo_question_'+question]['label'], social_status[question]['combo_question_'+question]['display_value']])
            rows.append([])



    file_name = 'Form_Data_%s.xls' % transaction.transaction_id
    return putils.render_excel(file_name, order, rows)

