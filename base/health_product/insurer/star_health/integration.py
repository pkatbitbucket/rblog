import urllib2
import datetime
import time
import json
import os

from utils import request_logger
from django.conf import settings
from django.core.files.base import File
from health_product.insurer.star_health import preset
from health_product.insurer.star_health.credentials import STARHEALTH_API_URL, STARHEALTH_API_KEY, STARHEALTH_SECRET_KEY
from health_product.insurer import custom_utils
from dateutil.relativedelta import relativedelta
from health_product.models import Transaction

class Processor():
    def __init__(self, transaction):
        self.transaction = transaction

    def get_processed_data_mcinew(self):
        policy_type = 'mcinew'
        SUM_INSURED_CHOICES = {
            '150000' : 1,
            '200000' : 2,
            '300000' : 3,
            '400000' : 4,
            '500000' : 5,
            '1000000' : 6,
            '1500000' : 7
        }

        social_data = self.transaction.insured_details_json['form_data']['medical_question_social_status']
        social_status = 1 if social_data['combo']['combo_question_social_status']['value'] == 'Yes' else 0
        social_status_bpl = 0
        social_status_disabled = 0
        social_status_informal = 0
        social_status_unorganized = 0
        if social_status:
            social_status_bpl = 1 if social_data['social_status_bpl']['combo_question_social_status_bpl']['value'] == 'Yes' else 0
            social_status_disabled = 1 if social_data['social_status_disabled']['combo_question_social_status_disabled']['value'] else 0
            social_status_informal = 1 if social_data['social_status_informal']['combo_question_social_status_informal']['value'] else 0
            social_status_unorganized = 1 if social_data['social_status_unorganized']['combo_question_social_status_unorganized']['value'] else 0


        pdata = self.transaction.insured_details_json['form_data']['proposer']['personal']
        address_data = self.transaction.insured_details_json['form_data']['proposer']['address']
        contact_data = self.transaction.insured_details_json['form_data']['proposer']['contact']

        if 'date_of_birth' in pdata.keys():
            self.transaction.proposer_date_of_birth = datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y')
        if 'gender' in pdata.keys():
            self.transaction.proposer_gender = int(pdata['gender']['value'])
        self.transaction.save()

        data = {
            'APIKEY': STARHEALTH_API_KEY,
            'SECRETKEY': STARHEALTH_SECRET_KEY,
            'policyTypeName': 'MCINEW',
            'startOn': datetime.datetime.strftime((datetime.datetime.today() + relativedelta(days=1)).date(), '%B %d, %Y'),
            'endOn': datetime.datetime.strftime((datetime.datetime.today() + relativedelta(days=0, years=1)).date(), '%B %d, %Y'),
            'proposerName': custom_utils.get_full_name(first_name=self.transaction.proposer_first_name,
                                               last_name=self.transaction.proposer_last_name),
            'proposerEmail': self.transaction.proposer_email,
            'proposerDob': datetime.datetime.strftime(self.transaction.proposer_date_of_birth, '%B %d, %Y'),
            'proposerPhone': self.transaction.proposer_mobile,
            'proposerAddressOne': self.transaction.proposer_address1,
            'proposerAddressTwo': self.transaction.proposer_address2,
            'proposerAreaId': str(address_data['area']['value']),
            'proposerDob': datetime.datetime.strftime(datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y'), '%B %d, %Y'),
            'period' : '1',  #TODO: Fetch this from code
            'socialStatus' : social_status,
            'socialStatusBpl': social_status_bpl,
            'socialStatusDisabled': social_status_disabled,
            'socialStatusInformal': social_status_informal,
            'socialStatusUnorganized': social_status_unorganized
        }

        counter = 0
        for code, fdata in self.transaction.insured_details_json['form_data']['insured'].items():
            # ndata = self.transaction.insured_details_json['form_data']['nominee'][code]
            mdata = self.transaction.insured_details_json['form_data']
            member = [m for m in self.transaction.extra['members'] if m['id'] == code][0]

            has_pre_existing = mdata['medical_question_pre_existing']['combo']['%s_question_pre_existing'%code]['value']
            pre_existing_illness = mdata['medical_question_pre_existing']['disease']['%s_question_disease'%code]['value'] if has_pre_existing == 'Yes' else 'NIL'

            data['insureds[%s]' % counter] = {
                'name': custom_utils.get_full_name(first_name=fdata['first_name']['value'], last_name=fdata['last_name']['value']),
                'dob': datetime.datetime.strftime(datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y'), '%B %d, %Y'),
                'sex': preset.GENDER_CHOICES[member['gender']],
                'illness': pre_existing_illness,
                'occupationId': fdata['occupation']['value'],
                #TODO: preset not required here
                'sumInsuredId': SUM_INSURED_CHOICES[str(self.transaction.slab.sum_assured)],
                'relationshipId': preset.RELATIONSHIP_CHOICES[policy_type][code],
                'criticalCancer': 'true' if mdata['medical_question_cancer']['combo']['%s_question_cancer' % code]['value'] == 'Yes' else 'false',
                'criticalRenal': 'true' if mdata['medical_question_renal']['combo']['%s_question_renal' % code]['value'] == 'Yes' else 'false',
                'criticalHeart': 'true' if mdata['medical_question_heart_diseases']['combo']['%s_question_heart_diseases' % code]['value'] == 'Yes' else 'false',
                'criticalPsychiatric': 'true' if mdata['medical_question_psychiatric_disorder']['combo']['%s_question_psychiatric_disorder' % code]['value'] == 'Yes' else 'false',
                'criticalDrugs': 'true' if has_pre_existing == 'Yes' else 'false',
                'hospitalCash' : '0'
            }
            counter += 1

        processed_data = json.dumps(data)
        return processed_data

    def get_processed_data_comprehensive(self):
        policy_type = 'comprehensive'
        SUM_INSURED_CHOICES = {
            '500000' : 1,
            '750000' : 2,
            '1000000' : 3,
            '1500000' : 4,
        }

        social_data = self.transaction.insured_details_json['form_data']['medical_question_social_status']
        social_status = 1 if social_data['combo']['combo_question_social_status']['value'] == 'Yes' else 0
        social_status_bpl = 0
        social_status_disabled = 0
        social_status_informal = 0
        social_status_unorganized = 0
        if social_status:
            social_status_bpl = 1 if social_data['social_status_bpl']['combo_question_social_status_bpl']['value'] == 'Yes' else 0
            social_status_disabled = 1 if social_data['social_status_disabled']['combo_question_social_status_disabled']['value'] else 0
            social_status_informal = 1 if social_data['social_status_informal']['combo_question_social_status_informal']['value'] else 0
            social_status_unorganized = 1 if social_data['social_status_unorganized']['combo_question_social_status_unorganized']['value'] else 0

        pdata = self.transaction.insured_details_json['form_data']['proposer']['personal']
        address_data = self.transaction.insured_details_json['form_data']['proposer']['address']
        contact_data = self.transaction.insured_details_json['form_data']['proposer']['contact']
        accidental   = self.transaction.insured_details_json['form_data']['accidental_coverage']['accidental_coverage']['accidental_coverage']

        if 'date_of_birth' in pdata.keys():
            self.transaction.proposer_date_of_birth = datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y')
        if 'gender' in pdata.keys():
            self.transaction.proposer_gender = int(pdata['gender']['value'])
        self.transaction.save()

        data = {
            'APIKEY': STARHEALTH_API_KEY,
            'SECRETKEY': STARHEALTH_SECRET_KEY,
            'policyTypeName': 'COMPREHENSIVE',
            'startOn': datetime.datetime.strftime((datetime.datetime.today() + relativedelta(days=1)).date(), '%B %d, %Y'),
            'endOn': datetime.datetime.strftime((datetime.datetime.today() + relativedelta(days=0, years=1)).date(), '%B %d, %Y'),
            'sumInsuredId': SUM_INSURED_CHOICES[str(self.transaction.slab.sum_assured)],
            'schemeId': preset.SCHEME_CHOICES[self.transaction.extra['plan_data']['category']],
            'proposerName': custom_utils.get_full_name(first_name=self.transaction.proposer_first_name,
                                               last_name=self.transaction.proposer_last_name),
            'proposerEmail': self.transaction.proposer_email,
            'proposerPhone': self.transaction.proposer_mobile,
            'proposerAddressOne': self.transaction.proposer_address1,
            'proposerAddressTwo': self.transaction.proposer_address2,
            'proposerAreaId': str(address_data['area']['value']),
            'proposerDob': datetime.datetime.strftime(datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y'), '%B %d, %Y'),
            'socialStatus': social_status,
            'socialStatusBpl': social_status_bpl,
            'socialStatusDisabled': social_status_disabled,
            'socialStatusInformal': social_status_informal,
            'socialStatusUnorganized': social_status_unorganized,
        }

        counter = 0
        for code, fdata in self.transaction.insured_details_json['form_data']['insured'].items():
            # ndata = self.transaction.insured_details_json['form_data']['nominee'][code]
            mdata = self.transaction.insured_details_json['form_data']
            member = [m for m in self.transaction.extra['members'] if m['id'] == code][0]

            has_pre_existing = mdata['medical_question_pre_existing']['combo']['%s_question_pre_existing'%code]['value']
            pre_existing_illness = mdata['medical_question_pre_existing']['disease']['%s_question_disease'%code]['value'] if has_pre_existing == 'Yes' else 'NIL'

            data['insureds[%s]' % counter] = {
                'isPersonalAccidentApplicable': "true" if member['id'] == accidental['value'] else "false",
                'name': custom_utils.get_full_name(first_name=fdata['first_name']['value'], last_name=fdata['last_name']['value']),
                'dob': datetime.datetime.strftime(datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y'), '%B %d, %Y'),
                'sex': preset.GENDER_CHOICES[member['gender']],
                'illness': pre_existing_illness,
                'relationshipId': preset.RELATIONSHIP_CHOICES[policy_type][code],
                'criticalCancer': 'true' if mdata['medical_question_cancer']['combo']['%s_question_cancer' % code]['value'] == 'Yes' else 'false',
                'criticalRenal': 'true' if mdata['medical_question_renal']['combo']['%s_question_renal' % code]['value'] == 'Yes' else 'false',
                'criticalHeart': 'true' if mdata['medical_question_heart_diseases']['combo']['%s_question_heart_diseases' % code]['value'] == 'Yes' else 'false',
                'criticalPsychiatric': 'true' if mdata['medical_question_psychiatric_disorder']['combo']['%s_question_psychiatric_disorder' % code]['value'] == 'Yes' else 'false',
                'criticalDrugs': 'true' if has_pre_existing == 'Yes' else 'false',
            }
            counter += 1

        processed_data = json.dumps(data)
        return processed_data

    def get_processed_data_fhonew(self):
        policy_type = 'fhonew'
        SUM_INSURED_CHOICES = {
            '200000' : 1,
            '300000' : 2,
            '400000' : 3,
            '500000' : 4,
            '1000000' : 5,
            '1500000' : 6,
        }

        social_data = self.transaction.insured_details_json['form_data']['medical_question_social_status']
        social_status = 1 if social_data['combo']['combo_question_social_status']['value'] == 'Yes' else 0
        social_status_bpl = 0
        social_status_disabled = 0
        social_status_informal = 0
        social_status_unorganized = 0
        if social_status:
            social_status_bpl = 1 if social_data['social_status_bpl']['combo_question_social_status_bpl']['value'] == 'Yes' else 0
            social_status_disabled = 1 if social_data['social_status_disabled']['combo_question_social_status_disabled']['value'] else 0
            social_status_informal = 1 if social_data['social_status_informal']['combo_question_social_status_informal']['value'] else 0
            social_status_unorganized = 1 if social_data['social_status_unorganized']['combo_question_social_status_unorganized']['value'] else 0

        pdata = self.transaction.insured_details_json['form_data']['proposer']['personal']
        address_data = self.transaction.insured_details_json['form_data']['proposer']['address']
        contact_data = self.transaction.insured_details_json['form_data']['proposer']['contact']

        if 'date_of_birth' in pdata.keys():
            self.transaction.proposer_date_of_birth = datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y')
        if 'gender' in pdata.keys():
            self.transaction.proposer_gender = int(pdata['gender']['value'])
        self.transaction.save()


        data = {
            'APIKEY': STARHEALTH_API_KEY,
            'SECRETKEY': STARHEALTH_SECRET_KEY,
            'policyTypeName': 'FHONEW',
            'startOn': datetime.datetime.strftime((datetime.datetime.today() + relativedelta(days=1)).date(), '%B %d, %Y'),
            'endOn': datetime.datetime.strftime((datetime.datetime.today() + relativedelta(days=0, years=1)).date(), '%B %d, %Y'),
            'sumInsuredId': SUM_INSURED_CHOICES[str(self.transaction.slab.sum_assured)],
            'schemeId': preset.SCHEME_CHOICES[self.transaction.extra['plan_data']['category']],
            'proposerName': custom_utils.get_full_name(first_name=self.transaction.proposer_first_name,
                                               last_name=self.transaction.proposer_last_name),
            'proposerEmail': self.transaction.proposer_email,
            'proposerPhone': self.transaction.proposer_mobile,
            'proposerAddressOne': self.transaction.proposer_address1,
            'proposerAddressTwo': self.transaction.proposer_address2,
            'proposerAreaId': str(address_data['area']['value']),
            'proposerDob': datetime.datetime.strftime(datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y'), '%B %d, %Y'),
            'socialStatus': social_status,
            'socialStatusBpl': social_status_bpl,
            'socialStatusDisabled': social_status_disabled,
            'socialStatusInformal': social_status_informal,
            'socialStatusUnorganized': social_status_unorganized,
        }

        counter = 0
        for code, fdata in self.transaction.insured_details_json['form_data']['insured'].items():
            # ndata = self.transaction.insured_details_json['form_data']['nominee'][code]
            mdata = self.transaction.insured_details_json['form_data']
            member = [m for m in self.transaction.extra['members'] if m['id'] == code][0]

            has_pre_existing = mdata['medical_question_pre_existing']['combo']['%s_question_pre_existing'%code]['value']
            pre_existing_illness = mdata['medical_question_pre_existing']['disease']['%s_question_disease'%code]['value'] if has_pre_existing == 'Yes' else 'NIL'

            data['insureds[%s]' % counter] = {
                'name': custom_utils.get_full_name(first_name=fdata['first_name']['value'], last_name=fdata['last_name']['value']),
                'dob': datetime.datetime.strftime(datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y'), '%B %d, %Y'),
                'sex': preset.GENDER_CHOICES[member['gender']],
                'illness': pre_existing_illness,
                'relationshipId': preset.RELATIONSHIP_CHOICES[policy_type][code],
                'criticalCancer': 'true' if mdata['medical_question_cancer']['combo']['%s_question_cancer' % code]['value'] == 'Yes' else 'false',
                'criticalRenal': 'true' if mdata['medical_question_renal']['combo']['%s_question_renal' % code]['value'] == 'Yes' else 'false',
                'criticalHeart': 'true' if mdata['medical_question_heart_diseases']['combo']['%s_question_heart_diseases' % code]['value'] == 'Yes' else 'false',
                'criticalPsychiatric': 'true' if mdata['medical_question_psychiatric_disorder']['combo']['%s_question_psychiatric_disorder' % code]['value'] == 'Yes' else 'false',
                'criticalDrugs': 'true' if has_pre_existing == 'Yes' else 'false',
            }
            counter += 1

        processed_data = json.dumps(data)
        return processed_data



    def get_processed_data_redcarpet(self):
        policy_type = 'redcarpet'
        SUM_INSURED_CHOICES = {
            '100000' : 1,
            '200000' : 2,
            '300000' : 3,
            '400000' : 4,
            '500000' : 5,
            '750000' : 6,
            '1000000' : 7,
        }

        social_data = self.transaction.insured_details_json['form_data']['medical_question_social_status']
        social_status = 1 if social_data['combo']['combo_question_social_status']['value'] == 'Yes' else 0
        social_status_bpl = 0
        social_status_disabled = 0
        social_status_informal = 0
        social_status_unorganized = 0
        if social_status:
            social_status_bpl = 1 if social_data['social_status_bpl']['combo_question_social_status_bpl']['value'] == 'Yes' else 0
            social_status_disabled = 1 if social_data['social_status_disabled']['combo_question_social_status_disabled']['value'] else 0
            social_status_informal = 1 if social_data['social_status_informal']['combo_question_social_status_informal']['value'] else 0
            social_status_unorganized = 1 if social_data['social_status_unorganized']['combo_question_social_status_unorganized']['value'] else 0


        pdata = self.transaction.insured_details_json['form_data']['proposer']['personal']
        address_data = self.transaction.insured_details_json['form_data']['proposer']['address']
        contact_data = self.transaction.insured_details_json['form_data']['proposer']['contact']

        if 'date_of_birth' in pdata.keys():
            self.transaction.proposer_date_of_birth = datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y')
        if 'gender' in pdata.keys():
            self.transaction.proposer_gender = int(pdata['gender']['value'])
        self.transaction.save()

        data = {
            'APIKEY': STARHEALTH_API_KEY,
            'SECRETKEY': STARHEALTH_SECRET_KEY,
            'policyTypeName': 'REDCARPET',
            'startOn': datetime.datetime.strftime((datetime.datetime.today() + relativedelta(days=1)).date(), '%B %d, %Y'),
            'endOn': datetime.datetime.strftime((datetime.datetime.today() + relativedelta(days=0, years=1)).date(), '%B %d, %Y'),
            'proposerName': custom_utils.get_full_name(first_name=self.transaction.proposer_first_name,
                                               last_name=self.transaction.proposer_last_name),
            'proposerEmail': self.transaction.proposer_email,
            'proposerPhone': self.transaction.proposer_mobile,
            'proposerAddressOne': self.transaction.proposer_address1,
            'proposerAddressTwo': self.transaction.proposer_address2,
            'proposerAreaId': str(address_data['area']['value']),
            'proposerDob': datetime.datetime.strftime(datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y'), '%B %d, %Y'),
            'socialStatus' : social_status,
            'socialStatusBpl': social_status_bpl,
            'socialStatusDisabled': social_status_disabled,
            'socialStatusInformal': social_status_informal,
            'socialStatusUnorganized': social_status_unorganized
        }

        counter = 0
        for code, fdata in self.transaction.insured_details_json['form_data']['insured'].items():
            # ndata = self.transaction.insured_details_json['form_data']['nominee'][code]
            mdata = self.transaction.insured_details_json['form_data']
            member = [m for m in self.transaction.extra['members'] if m['id'] == code][0]

            has_pre_existing = mdata['medical_question_pre_existing']['combo']['%s_question_pre_existing'%code]['value']
            pre_existing_illness = mdata['medical_question_pre_existing']['disease']['%s_question_disease'%code]['value'] if has_pre_existing == 'Yes' else 'NIL'

            has_pre_existing_twelve = mdata['medical_question_pre_existing_before_twelve_months']['combo']['%s_question_pre_existing_before_twelve_months'%code]['value']

            illness_before_twelve = mdata['medical_question_pre_existing_before_twelve_months']['disease']['%s_question_disease'%code]['value'] if has_pre_existing_twelve == 'Yes' else 'NIL';

            data['insureds[%s]' % counter] = {
                'name': custom_utils.get_full_name(first_name=fdata['first_name']['value'], last_name=fdata['last_name']['value']),
                'dob': datetime.datetime.strftime(datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y'), '%B %d, %Y'),
                'sex': preset.GENDER_CHOICES[member['gender']],
                'illness': pre_existing_illness,
                'illnessBeforeTwelveMonths': illness_before_twelve,
                'occupationId': fdata['occupation']['value'],
                'sumInsuredId': SUM_INSURED_CHOICES[str(self.transaction.slab.sum_assured)],
                'relationshipId': preset.RELATIONSHIP_CHOICES[policy_type][code],
                'criticalCancer': 'true' if mdata['medical_question_cancer']['combo']['%s_question_cancer' % code]['value'] == 'Yes' else 'false',
                'criticalKidney': 'true' if mdata['medical_question_kidney']['combo']['%s_question_kidney' % code]['value'] == 'Yes' else 'false',
                'criticalBrain': 'true' if mdata['medical_question_brain']['combo']['%s_question_brain' % code]['value'] == 'Yes' else 'false',
                'criticalAlzheimer': 'true' if mdata['medical_question_alzheimer']['combo']['%s_question_alzheimer' % code]['value'] == 'Yes' else 'false',
                'criticalParkinson': 'true' if mdata['medical_question_parkinson']['combo']['%s_question_parkinson' % code]['value'] == 'Yes' else 'false',
                'criticalPsychiatric': 'true' if mdata['medical_question_psychiatric_disorder']['combo']['%s_question_psychiatric_disorder' % code]['value'] == 'Yes' else 'false',
                'criticalDrugs': 'true' if has_pre_existing == 'Yes' else 'false',
            }
            counter += 1

        processed_data = json.dumps(data)
        return processed_data

def post_transaction(transaction):
    is_redirect = True
    pincode_error = False
    redirect_url = '/health-plan/product/transaction/%s/failure/' % transaction.transaction_id

    if transaction.slab.health_product.id in [37]:
        policy_code = 'mcinew'
    elif transaction.slab.health_product.id in [36]:
        policy_code = 'comprehensive'
    elif transaction.slab.health_product.id in [18, 65]:
        policy_code = 'fhonew'
    elif transaction.slab.health_product.id in [64]:
        policy_code = 'redcarpet'

    if pincode_error:
        redirect_url = '/health-plan/pincode-unavailable/'
    else:
        #try:
        if True:
            obj = Processor(transaction)
            processed_data = getattr(obj, 'get_processed_data_%s' % policy_code.lower(), '')()
            print "PROCESSED DATA", processed_data
            print 'URL : %s/api/policy/proposals' % STARHEALTH_API_URL
            request_logger.info('\nSTAR TRANSACTION: %s \nPROCESSED DATA":\n %s \n' % (transaction.transaction_id, processed_data))

            req = urllib2.Request(url='%s/api/policy/proposals' % STARHEALTH_API_URL , data=processed_data, headers={'Content-Type' : 'application/json'})
            resp = urllib2.urlopen(req)

            transaction.extra['product_response'] = json.loads(resp.read())
            request_logger.info('\nSTAR TRANSACTION: %s \nPRODUCT RESPONSE: %s' % (transaction.transaction_id, transaction.extra['product_response']))
            transaction.payment_request = processed_data
            transaction.reference_id = transaction.extra['product_response']['referenceId']
            transaction.premium_paid = transaction.extra['product_response']['totalPremium']
            transaction.policy_start_date = (datetime.datetime.today() + relativedelta(days=1)).date()
            transaction.policy_end_date = (datetime.datetime.today() + relativedelta(years=1)).date()
            transaction.save()

            print "EXTRA", transaction.extra
            policy_token_url = '%s/api/policy/proposals/%s/token' % (STARHEALTH_API_URL, transaction.extra['product_response']['referenceId'])
            print "POLICY TOKEN URL:", policy_token_url
            token_data = {
                'APIKEY': STARHEALTH_API_KEY,
                'SECRETKEY': STARHEALTH_SECRET_KEY,
                'referenceId' : transaction.extra['product_response']['referenceId']
            }
            json_token_data = json.dumps(token_data)

            req = urllib2.Request(url=policy_token_url, data=json_token_data, headers={'Content-Type' : 'application/json'})
            resp = urllib2.urlopen(req)
            transaction.extra['token_response'] = json.loads(resp.read())
            transaction.policy_token = transaction.extra['token_response']['redirectToken']
            transaction.save()
            print "TOKEN RESPONSE:", transaction.extra['token_response']

            redirect_url = '%s/policy/proposals/purchase/%s' % (STARHEALTH_API_URL, transaction.extra['token_response']['redirectToken'])
            return is_redirect, redirect_url, "", {}
        #except urllib2.HTTPError as err:
        else:
            #print ">>>>>>>>>", err, err.read()
            pass
    return is_redirect, redirect_url, '', {}


def post_payment(payment_response, transaction=None):
    """
    http://www.coverfox.com/health-plan/2/response/?purchaseToken=abcd1234
    """
    redirect_url = '/health-plan/payment/failure/'
    is_redirect = True

    #REQUEST TO FIND OUT THE TRANSACTION TO ATTACH THE PURCHASE TOKEN TO
    purchase_token = payment_response['purchaseToken']
    data = {
        'APIKEY' : STARHEALTH_API_KEY,
        'SECRETKEY': STARHEALTH_SECRET_KEY,
        'purchaseToken' : purchase_token
    }
    processed_data = json.dumps(data)
    req = urllib2.Request(url='%s/api/policy/proposals/%s/purchase/response' % (STARHEALTH_API_URL, purchase_token ),
                          data=processed_data, headers={'Content-Type' : 'application/json'})
    resp_json = urllib2.urlopen(req).read()
    resp = json.loads(resp_json)

    transaction = Transaction.objects.get(reference_id= resp['referenceId'])
    transaction.payment_response = resp
    transaction.extra['error_msg']='FAILURE'
    redirect_url = '/health-plan/payment/transaction/%s/failure/' % transaction.transaction_id
    if not 'payment_response' in transaction.extra.keys():
        transaction.extra['payment_response'] = {}

    transaction.payment_token = purchase_token
    transaction.extra['payment_response']['purchase_token'] = purchase_token
    transaction.extra['payment_response']['status'] = resp['status']
    transaction.extra['payment_response']['reference_id'] = resp['referenceId']
    transaction.save()


    if transaction.extra['payment_response']['status'].lower().find('success') >= 0:
        transaction.payment_done = True
        transaction.payment_on = datetime.datetime.today()
        transaction.save()
        #REQUEST TO FETCH PDF
        data = {
            'APIKEY' : STARHEALTH_API_KEY,
            'SECRETKEY': STARHEALTH_SECRET_KEY,
            'referenceId' : transaction.extra['payment_response']['reference_id']
        }
        processed_data = json.dumps(data)
        time.sleep(3)
        pdf_req = urllib2.Request(url='%s/api/policy/proposals/%s/schedule' % (
            STARHEALTH_API_URL,
            transaction.extra['payment_response']['reference_id']
        ) , data=processed_data, headers={'Content-Type' : 'application/json'})
        pdf_response = urllib2.urlopen(pdf_req)
        pdf_resp = pdf_response.read()
        #WRITE THE PDF RESPONSE TO A LOCAL FILE
        try:
            if type(pdf_resp) == dict and 'error' in pdf_resp.keys():
                request_logger.info('\nSTAR TRANSACTION: %s \nPDF GENERATION ERROR":\n %s \n' % (transaction.transaction_id, pdf_resp))
            else:
                # UPLOAD THE LOCAL FILE TO DJANGO FILE FIELD
                transaction.update_policy(pdf_resp, send_mail=False)
        except Exception as e:
            request_logger.info('\nSTAR TRANSACTION: %s \nPDF GENERATION ERROR":\n %s \n' % (transaction.transaction_id, e.message))
            print e.message


        redirect_url = '/health-plan/payment/transaction/%s/success/' % transaction.transaction_id
    return is_redirect, redirect_url, '', {}
