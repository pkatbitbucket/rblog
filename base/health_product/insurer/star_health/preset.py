from health_product.insurer import settings as insurer_settings
from health_product.models import *
from wiki.models import *
import datetime
from dateutil.relativedelta import *
from collections import defaultdict


GENDER_OPTIONS = [
    {'id' : 1, 'name' : 'Male'},
    {'id' : 2, 'name' : 'Female'}
]

MARITAL_STATUS_OPTIONS = [
    {'id' : 'SINGLE', 'name': 'Single'},
    {'id' : 'MARRIED', 'name': 'Married'}
]

OCCUPATION_OPTIONS = {
    'mcinew':[
        {'id' : 1, 'name' : 'Business / Traders'},
        {'id' : 2, 'name' : 'Professional : Doctor'},
        {'id' : 3, 'name' : 'Professional : Engineer'},
        {'id' : 4, 'name' : 'Professional : Lawyer'},
        {'id' : 5, 'name' : 'Clerical : Supervisory and Related Worker'},
        {'id' : 6, 'name' : 'Hospitality and Support Workers'},
        {'id' : 7, 'name' : 'Production Workers, Skilled and Non-Agricultural'},
        {'id' : 8, 'name' : 'Farmers and Agricultural Workers'},
        {'id' : 9, 'name' : 'Police/Para Military/Defence'},
        {'id' : 10, 'name' : 'Housewives'},
        {'id' : 11, 'name' : 'Retired Persons'},
        {'id' : 12, 'name' : 'Students : School and College'}
        ],
    'redcarpet':[
        {'id' : 1, 'name' : 'Business'},
        {'id' : 2, 'name' : 'Public Sector'},
        {'id' : 3, 'name' : 'Private Sector'},
        {'id' : 4, 'name' : 'Student Undergraduate'},
        {'id' : 5, 'name' : 'Student Graduate'},
        {'id' : 6, 'name' : 'Student Post Graduate'},
        {'id' : 7, 'name' : 'Central Government'},
        {'id' : 8, 'name' : 'State Government'},
        {'id' : 9, 'name' : 'Professional'},
        {'id' : 10, 'name' : 'Service'},
        {'id' : 11, 'name' : 'Housewife'},
        {'id' : 12, 'name' : 'Retired'},
        {'id' : 13, 'name' : 'Pensioner'},
        {'id' : 14, 'name' : 'Rural Artisan'},
        {'id' : 15, 'name' : 'Agriculturalist'}
    ]
}

RELATIONSHIP_STAR_MASTER = {
    1: 'Self',
    2: 'Spouse',
    3: 'DependentChild',
    4: 'DependentParent',
    5: 'EMPLOYEE',
    6: 'FATHER-IN-LAW',
    7: 'MOTHER-IN-LAW',
    8: 'BROTHER',
    9: 'BROTHERINLAW',
    10: 'SISTERINLAW',
    11: 'SONINLAW1',
    12: 'SONINLAW2',
    13: 'SONINLAW3',
    14: 'DAUGHTERINLAW1',
    15: 'DAUGHTERINLAW2',
    16: 'DAUGHTERINLAW3',
    17: 'OTHERS',
    18: 'GRANDFATHER',
    19: 'GRANDMOTHER',
    20: 'AUNT',
    21: 'UNCLE',
    22: 'BLOOD_RELATIVES',
    23: 'DEPENDENTBROTHER',
    24: 'DEPENDENTSISTER'
}

GENDER_CHOICES = {
    1: 'Male',
    2: 'Female'
}

def relationship_choices_default():
    return {
        'self': 1,
        'spouse': 2,
        'dependentchild': 3,
        'son': 3,
        'son1': 3,
        'son2': 3,
        'son3': 3,
        'son4': 3,
        'daughter': 3,
        'daughter1': 3,
        'daughter2': 3,
        'daughter3': 3,
        'daughter4': 3,
        'dependentparent': 4,
        'father': 4,
        'mother': 4
    }
RELATIONSHIP_CHOICES = defaultdict(relationship_choices_default)
RELATIONSHIP_CHOICES['redcarpet'] = {
    'self': 1,
    'employee': 2,
    'spouse': 3,
    'father': 4,
    'mother': 5,
    'father-in-law': 6,
    'mother-in-law': 7,
    'brother': 8,
    'sister': 9,
    'brother-in-law': 10,
    'sister-in-law': 11,
    'son': 12,
    'son1': 12,
    'son2': 13,
    'son3': 14,
    'daughter': 13,
    'daughter1': 15,
    'daughter2': 16,
    'daughter3': 17,
    'son-in-law1': 18,
    'son-in-law2': 19,
    'daughter-in-law1': 21,
    'daughter-in-law2': 22,
    'daughter-in-law3': 23,
    'others': 24,
    'grandfather': 25,
    'grandmother': 26,
    'aunt': 27,
    'uncle': 28,
    'bloodrelatives': 29,
    'housewife': 30,
}

SCHEME_CHOICES = {
    '2A-0C': 1,
    '1A-1C': 2,
    '1A-2C': 3,
    '1A-3C': 4,
    '2A-1C': 5,
    '2A-2C': 6,
    '2A-3C': 7
}

OCCUPATION_MASTER = {
    'BUSINESS/TRADERS': 1,
    'PROFESSIONAL-DOCTOR': 2,
    'PROFESSIONAL-ENGINEER': 3,
    'PROFESSIONAL-LAWYER': 4,
    'CLERICAL/SUPERVISORYANDRELATEDWORKER': 5,
    'HOSPITALITYANDSUPPORTWORKERS': 6,
    'PRODUCTIONWORKERS,SKILLEDANDNON-AGRICULTURAL': 7,
    'FARMERSANDAGRICULTURALWORKERS': 8,
    'POLICE/PARAMILITARY/DEFENCE': 9,
    'HOUSEWIVES': 10,
    'RETIREDPERSONS': 11,
    'STUDENTS-SCHOOLANDCOLLEGE': 12
}


