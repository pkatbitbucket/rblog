from health_product.insurer import custom_utils

def save_stage(transaction, stage_data):
    current_stage = stage_data['template']
    red_flag = []
    form_data = custom_utils.clean_stage_data(stage_data)
    # print "FORM DATA"
    # pprint.pprint(form_data)
    for k, v in form_data.items():
        if 'form_data' not in transaction.insured_details_json.keys():
            transaction.insured_details_json['form_data'] = {}
        if k in transaction.insured_details_json['form_data'].keys():
            transaction.insured_details_json['form_data'][k].update(**v)
        else:
            transaction.insured_details_json['form_data'][k] = v
    transaction.save()

    #================== CHECK FOR MEDICAL ====================
    for bigkey, bigval in form_data.items():
        bigkey = bigkey.split('_question_')
        if bigkey[0] == 'medical' and bigkey[1].find('social') < 0 and bigkey[1].find('pre_existing') < 0:
            for code, mdata in bigval.items():
                for k, v in mdata.items():
                    key_red_flag = True if v['value'] == 'Yes' else False
                    if key_red_flag:
                        red_flag.append({'question': v['label'], 'answer': v['value']})

    #=========================================================
    if 'red_flags' not in transaction.extra:
        transaction.extra['red_flags'] = {}

    #red_flag = [] if transaction.slab.health_product_id == 64 else red_flag.
    transaction.extra['red_flags'][stage_data['template']]  = {'reason':'DISEASE','data':red_flag}
    transaction.save()
    return transaction
