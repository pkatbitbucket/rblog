from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^fetch-areas/$', views.fetch_areas, name='fetch_areas'),
]
