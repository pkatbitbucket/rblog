from health_product.insurer.star_health import custom_utils as star_utils
from django.http import HttpResponse, HttpResponseRedirect, Http404
import json

def fetch_areas(request):
    pincode = request.POST['pincode']
    city_id = request.POST['city']
    return HttpResponse(json.dumps(star_utils.fetch_areas(pincode, city_id)))

