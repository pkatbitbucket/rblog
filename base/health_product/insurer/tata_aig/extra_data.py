from django.template.loader import render_to_string

def get_extra_data(slab):
    footer = render_to_string('health_product/insurers/tata_aig/footer.html', {})
    return {
        'footer' : footer,
    }
