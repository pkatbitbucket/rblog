from wiki.models import City, Pincode, FamilyCoverPremiumTable, CoverPremiumTable
import math
from health_product.insurer import custom_utils
from health_product.insurer.tata_aig import preset
from collections import OrderedDict


class Formic():
    def __init__(self, transaction, is_self_present):
        self.transaction = transaction
        self.is_self_present = is_self_present
        self.proposer_data = {
            'pincode': transaction.extra['pincode'],
            'city' : City.objects.get(id=transaction.extra['city']['id']).name,
            'state' : City.objects.get(id=transaction.extra['city']['id']).state.name,
            'email': ''
        }


    def add_parent_segment(self, segment_id, tag_id, details, question_type):
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment': segment_id, 'tag': tag_id, 'details': details, 'data': []}
        row = []
        if question_type == 'MultipleChoiceField':
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='MultipleChoiceField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    details=member,
                    value='No',
                    label=details.get('question_text', ''),
                    span=4,
                    css_class='bottom-margin',
                    event=', click : $parent.root_context.stage_5.any_yes_check',
                    template_option='question'
                ))
            new_segment['data'].append(row)
        elif question_type == 'DropDownField':
            if tag_id == 'pre_existing':
                for member in self.transaction.extra['members']:
                    row.append(custom_utils.new_field(
                        saved_data=saved_data,
                        field_type='DropDownField',
                        code='%s_question_%s' % (member['id'], tag_id),
                        options=self.pre_existing_options,
                        details=member,
                        label=details.get('question_text', ''),
                        span=2,
                        css_class='bottom-margin',
                        event=', event : {change : root_context.stage_5.any_yes_check}',
                        template_option='question'
                    ))
            new_segment['data'].append(row)
        elif question_type == 'TextField':
            for member in self.transaction.extra['members']:
                row.append(custom_utils.new_field(
                    saved_data=saved_data,
                    field_type='TextField',
                    code='%s_question_%s' % (member['id'], tag_id),
                    span=2,
                    label=details.get('question_text', ''),
                    required=False,
                    only_if='root_context.stage_5.check_dict.get(\'%s\')' % (member['id'])
                ))
            new_segment['data'].append(row)

        return new_segment


    def stage_1(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'personal'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            label='First Name',
            code='first_name',
            ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='StrictCharField',
            label='Last Name',
            code='last_name',
            ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Gender',
            code='gender',
            options=preset.GENDER_OPTIONS
        ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Marital Status',
            code='marital_status',
            options=preset.MARITAL_STATUS_OPTIONS
        ))

        if self.is_self_present:
            member = [m for m in self.transaction.extra['members'] if m['id'] == 'self'][0]
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                min_age=18,
                max_age=100,
                default_age=member['age'],
            ))
        else:
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                min_age=18,
                max_age=100,
                default_age=18,
            ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Identity Proof',
            code='id_proof_type',
            options=preset.ID_PROOF_OPTIONS
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            label='Identity Proof Number',
            code='id_proof_number',
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            label='Nationality',
            code='nationality',
            options=preset.NATIONALITY_OPTIONS,
            value='Indian',
            ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Type of Organisation',
            code='type_of_organisation',
            options=preset.ORGANISATION_OPTIONS
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='DropDownField',
            label='Source of Income',
            code='source_of_income',
            options=preset.INCOME_OPTIONS
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            label='Avail 5% discount by opting for 2 year term',
            code='two_year_discount',
            value='No',
        ))
        new_segment['data'].append(row)

        stage.append(new_segment)
        return {'template' : 'stage_1', 'data' : stage}


    def stage_2(self):
        stage = []
        feet_range = []
        for feet in range(0, 9):
            feet_range.append({'id': feet, 'name': '%s ft' % feet})
        inch_range = []
        for inch in range(0, 12):
            inch_range.append({'id': inch, 'name': '%s inches' % inch})

        for member in self.transaction.extra['members']:
            segment_id = 'insured'
            tag_id = member['id']
            template_switch = 'dynamic'
            if tag_id == 'self':
                template_switch = 'static'
            try:
                saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment': segment_id, 'tag': tag_id, 'details': member, 'data': []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                label='First Name',
                code='first_name',
                max_length=50,
                template_option=template_switch,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                label='Last Name',
                code='last_name',
                template_option=template_switch,
                max_length=50,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                label='Date Of Birth',
                code='date_of_birth',
                value=member.get('dob', ''),
                span=3,
                template_option=template_switch,
                default_age = member['age'],
                min_age = 18 if member['type']=='adult' else 0,
                max_age = 100,
            ))
            new_segment['data'].append(row)

            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DropDownField',
                label='Marital Status',
                code='marital_status',
                options=preset.MARITAL_STATUS_OPTIONS,
                span=3,
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='TwoDropDownField',
                label='Height',
                code='height',
                options1=feet_range,
                options2=inch_range
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='IntegerField',
                label='Weight (kgs)',
                code='weight',
                span=3
            ))
            new_segment['data'].append(row)
            stage.append(new_segment)

        return {'template' : 'stage_2', 'data' : stage}


    def stage_3(self):
        relation_options = [
            {'id': 'Father', 'name': 'Father'},
            {'id': 'Mother', 'name': 'Mother'},
            {'id': 'Daughter', 'name': 'Daughter'},
            {'id': 'Son', 'name': 'Son'},
            {'id': 'Wife', 'name': 'Wife'},
            {'id': 'Husband', 'name': 'Husband'}
        ]
        relation_options.sort(lambda x, y: -1 if x['name'] < y['name'] else 1)
        default_relation = {
            'self': 'Wife' if self.transaction.extra['gender'] == 'MALE' else 'Husband',
            'spouse': 'Husband' if self.transaction.extra['gender'] == 'MALE' else 'Wife',
            'son': 'Father' if self.transaction.extra['gender'] == 'MALE' else 'Mother',
            'daughter': 'Father' if self.transaction.extra['gender'] == 'MALE' else 'Mother',
            'father': 'Son' if self.transaction.extra['gender'] == 'MALE' else 'Daughter',
            'mother': 'Son' if self.transaction.extra['gender'] == 'MALE' else 'Daughter'
        }
        self_relation_probables = OrderedDict([
            ('mother', 'Mother'),
            ('father', 'Father'),
            ('spouse', 'Wife' if self.transaction.extra['gender'] == 'MALE' else 'Husband')
        ])
        #Needs to run before the big loop on members
        for member in self.transaction.extra['members']:
            if member['person'].lower() in self_relation_probables.keys():
                self.transaction.extra['self_nominee'] = member['id']
                self.transaction.save()
                default_relation['self'] = self_relation_probables[member['person']]
        stage = []
        for member in self.transaction.extra['members']:
            segment_id = 'nominee'
            tag_id = member['id']
            try:
                saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
            except KeyError:
                saved_data = {}
            new_segment = {'segment': segment_id, 'tag': tag_id, 'details': member, 'data': []}
            row = []
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DropDownField',
                code='relation',
                label='Relationship With %s' % member['verbose'],
                options=relation_options,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                code='first_name',
                label='First Name',
                max_length=50,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='StrictCharField',
                code='last_name',
                label='Last Name',
                max_length=50,
                span=3
            ))
            row.append(custom_utils.new_field(
                saved_data=saved_data,
                field_type='DateField',
                code='date_of_birth',
                label='Date Of Birth',
                min_age=18,
                span=3
            ))
            new_segment['data'].append(row)
            stage.append(new_segment)
        return {'template' : 'stage_3', 'data' : stage}


    def stage_4(self):
        stage = []
        segment_id = 'proposer'
        tag_id = 'address'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address1',
            label='Flat/House No, Street',
            span=12,
            css_class='bottom-margin',
            max_length=50
        ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='address2',
            label='Area/Locality',
            span=12,
            css_class='bottom-margin',
            max_length=100,
            ))
        new_segment['data'].append(row)

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='landmark',
            label='Landmark',
            span=12,
            css_class='bottom-margin',
            max_length=100,
            required=False
        ))
        new_segment['data'].append(row)

        row = []
        pincode = Pincode.objects.filter(pincode=self.transaction.extra['pincode'])[0]
        state = pincode.state
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='SearchableDropDownField',
            code='city',
            label='City',
            options=[{'id': c.name, 'name': c.name} for c in state.city_set.all()]
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='state',
            label='State',
            value=self.proposer_data['state'],
            span=4,
            template_option='static'
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='pincode',
            label='Pincode',
            value=str(self.proposer_data['pincode']),
            span=4,
            template_option='static'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)


        tag_id = 'contact'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}

        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='CharField',
            code='email',
            label='Email',
            span=12,
            css_class='bottom-margin',
            email=True
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MobileField',
            code='mobile',
            label='Mobile',
            css_class='bottom-margin',
            span=12
        ))
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='LandlineField',
            code='landline',
            label='Landline',
            span=12,
            css_class='bottom-margin',
            required=False
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template' : 'stage_4', 'data' : stage}


    def stage_5(self):
        stage = []
        #---------- Question Unit Begins -----------
        question_code = 'pre_existing'
        tag_id = 'pre_existing'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text' : 'Have you or any other member(s) proposed to be insured the past 5 years suffered '
                              'from or are you currently suffering from any disease, illness, injury or accident '
                              'other than common cold or viral fever?',
            'trigger_any_yes_check': False,
            'show_header': True
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #---------- Question Unit Begins -----------
        question_code = 'admit'
        tag_id = 'admit'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text' : 'Have you or any other member(s) proposed to be insured admitted to a hospital '
                              'or a nursing home or had any test or imaging like CT or MRI scan or X-ray done '
                              'in the last ten years other than routine health check-up or pre-employment check-up?',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #---------- Question Unit Begins -----------
        question_code = 'admit'
        tag_id = 'medication'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text' : 'Have you or any other member(s) proposed to be insured taken any medication for '
                              'more than 2 weeks in last 5 years?',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #---------- Question Unit Begins -----------
        question_code = 'operation'
        tag_id = 'operation'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text' : 'Have you or any other member(s) proposed to be insured had any surgery in the '
                              'last ten years or has surgery ever been recommended or are you awaiting any '
                              'surgical operation?',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #---------- Question Unit Begins -----------
        question_code = 'illness'
        tag_id = 'illness'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Have you or any other member(s) proposed to be insured ever had or has a doctor '
                             'ever said that you have any of the following conditions / diseases: '
                             'High blood pressure, diabetes or sugar, any heart related ailment, TB or asthma '
                             'or breathing problem, tumor or cancer, liver or gall bladder diseases, prostrate, '
                             'kidney or stone diseases, arthritis or bone disease, blood diseases or disorders, '
                             'ulcer or stomach disorder, eye or ENT disease, dizziness or fits OR any of '
                             'the members is pregnant?',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #---------- Question Unit Begins -----------
        question_code = 'declined'
        tag_id = 'declined'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Has yours or any other member(s) proposed to be insured any application for life, '
                             'health or critical illness insurance ever been declined, postponed, loaded or '
                             'been made subject to any special conditions by any insurance company?',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))


        #---------- Question Unit Begins -----------
        question_code = 'other'
        tag_id = 'other'
        segment_id = 'medical_question_%s' % question_code
        details = {
            'conditional': 'NO',
            'question_text': 'Do you or any other member(s) proposed to be insured have any other medical '
                             'condition which is not declared above?',
            'trigger_any_yes_check': False,
            'show_header': False
        }
        stage.append(self.add_parent_segment(segment_id, tag_id, details, 'MultipleChoiceField'))

        return {'template' : 'stage_5', 'data' : stage}

    def stage_6(self):
        stage = []
        segment_id = 'previous_policy'
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}

        try:
            existing_policies_data = self.transaction.insured_details_json['form_data']['existing_policies']
        except KeyError:
            existing_policies_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {'existing_policies_data': existing_policies_data}, 'data' : []}
        row = []
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='existing_insurance',
            label='Is there an existing/previous Insurance policy that you want to consider for portability?',
            span=12,
            css_class='bottom-margin',
            event=', click : $parent.root_context.stage_6.any_yes_check',
            template_option='question'
        ))
        new_segment['data'].append(row)

        stage.append(new_segment)
        return {'template' : 'stage_6', 'data' : stage}


    # def stage_7(self):
    #     stage = []
    #     segment_id = 'co_insurance'
    #     tag_id = 'combo'
    #     try:
    #         saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
    #     except KeyError:
    #         saved_data = {}
    #
    #     try:
    #         co_insurance_data = self.transaction.insured_details_json['form_data']['co_insurance']
    #     except KeyError:
    #         co_insurance_data = {}
    #     new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {'co_insurance_data': co_insurance_data}, 'data' : []}
    #     row = []
    #     new_segment['data'].append(row)
    #
    #     stage.append(new_segment)
    #     return {'template' : 'stage_7', 'data' : stage}


    def stage_7(self):
        stage = []
        segment_id = 'terms'
        tag_id = 'combo'
        try:
            saved_data = self.transaction.insured_details_json['form_data'][segment_id][tag_id]
        except KeyError:
            saved_data = {}
        new_segment = {'segment' : segment_id, 'tag' : tag_id, 'details' : {}, 'data' : []}
        row = []
        t_and_c_label = ('I hereby confirm<br />'
                         '- I have read and agree to the Terms and Conditions of ')
        if self.transaction.slab.health_product.policy_wordings:
            t_and_c_label += ('the <a style="text-decoration:none;cursor:pointer;" '
                              'onclick=\'window.open("%s","","width=400, height=400")\'>policy</a> and '
                             ) % self.transaction.slab.health_product.policy_wordings.url
        t_and_c_label += ('the <a style="cursor:pointer;" class="call_overlay">website</a>;<br />'
                          '- the declarations made herewith are true and correct;<br />'
                          '- appointment of Coverfox Insurance Brokers Pvt. Ltd. as my Insurance Broker, '
                          'and authorize them to represent me to Insurance Companies/Agencies for my Insurance needs.'
                         )
        row.append(custom_utils.new_field(
            saved_data=saved_data,
            field_type='MultipleChoiceField',
            code='t_and_c',
            label=t_and_c_label,
            span=12,
            options=[
                {'id' : 'Yes', 'name' : 'Yes', 'is_correct' : True},
                {'id' : 'No', 'name' : 'No', 'is_correct' : False}
            ],
            css_class='bottom-margin',
            template_option='terms'
        ))
        new_segment['data'].append(row)
        stage.append(new_segment)
        return {'template' : 'stage_7', 'data' : stage}


    def get_form_data_individual(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
            self.stage_6(),
            self.stage_7()
        ]
        return data


    def get_form_data_family(self):
        data = [
            self.stage_1(),
            self.stage_2(),
            self.stage_3(),
            self.stage_4(),
            self.stage_5(),
            self.stage_6(),
            self.stage_7()
        ]
        return data


def get_dynamic_form(transaction):
    transaction, plan_data, proposer_data, is_self_present = custom_utils.standard_form_data_cleanup(transaction)
    #TOGGLES WHERE MEDICAL CASES ARE ALLOWED OR NOT
    plan_data['medical_firewall'] = True

    if transaction.slab.health_product.policy_type == "FAMILY":
        cpt = FamilyCoverPremiumTable.objects.get(id=transaction.extra['cpt_id'])
    else:
        cpt = CoverPremiumTable.objects.get(id=transaction.extra['cpt_id'])
    slab = transaction.slab

    # lal = None
    # ual = None
    # if cpt.lower_age_limit < 12:
    #     lal = cpt.lower_age_limit / 100.0
    # else:
    #     lal = cpt.lower_age_limit / 1000.0

    # if cpt.upper_age_limit:
    #     if cpt.upper_age_limit < 12:
    #         ual = cpt.upper_age_limit / 100.0
    #     else:
    #         ual = cpt.upper_age_limit / 1000.0
    # else:
    #     ual = 101

    max_age = 0
    for member in transaction.extra['members']:
        if max_age < member['age']:
            max_age = member['age']

    max_filter_age = max_age * 1000
    if slab.health_product.policy_type == "FAMILY":
        alt_cpt = FamilyCoverPremiumTable.objects.get(
            slabs=transaction.slab,
            payment_period=1,
            adults = cpt.adults,
            kids = cpt.kids,
            lower_age_limit__lte=max_filter_age,
            upper_age_limit__gte=max_filter_age
        )
    else:
        alt_cpt = CoverPremiumTable.objects.get(
            slabs=transaction.slab, payment_period=1,
            lower_age_limit__lte=max_filter_age,
            upper_age_limit__gte=max_filter_age
        )
    search_age = (max_age + 1) * 1000
    # city = City.objects.get(id=transaction.extra['city']['id'])
    if slab.health_product.policy_type == "FAMILY":
        new_cpt = FamilyCoverPremiumTable.objects.get(
            slabs=transaction.slab,
            payment_period=1,
            adults=cpt.adults,
            kids=cpt.kids,
            lower_age_limit__lte=search_age,
            upper_age_limit__gte=search_age
        )
    else:
        new_cpt = CoverPremiumTable.objects.get(
            slabs=transaction.slab, payment_period=1,
            lower_age_limit__lte=search_age,
            upper_age_limit__gte=search_age
        )
    plan_data['two_year_premium'] = int(math.floor(alt_cpt.premium * (1 - float(alt_cpt.two_year_discount) / 100) + new_cpt.premium * (1 - float(new_cpt.two_year_discount) / 100)))

    plan_data['premium'] = alt_cpt.premium
    transaction.extra['two_year_premium'] = plan_data['two_year_premium']
    transaction.save()


    obj = Formic(transaction, is_self_present)
    stages = getattr(obj, 'get_form_data_%s' % slab.health_product.policy_type.lower(), '')()

    terms_and_conditions = "<div style='padding:20px 40px 70px 20px;'>" \
                           "<h3>Tata AIG: Declaration & Warranty on behalf of all persons to be insured</h3>" \
                           "<ul>" \
                           "<li>I/ We hereby declare, on my behalf and on behalf of all persons proposed to be " \
                           "insured, that the above statements, answers and/or particulars given by me are true and " \
                           "complete in all respects to the best of my knowledge and that I/We am/ " \
                           "are authorized to propose on behalf of these other persons.</li>" \
                           "<li>I understand that the information provided by me will form the basis of insurance " \
                           "policy, is subject to the Board approved underwriting policy of the Insurance company " \
                           "and that the policy will come into force only after full receipt of the premium " \
                           "chargeable.</li>" \
                           "<li>I/ We further declare that I/We will notify in writing any change occurring in the " \
                           "occupation or general health of the life to be insured/ proposer after the proposal has " \
                           "been submitted but before communication of the risk acceptance by the company.</li>" \
                           "<li>I/We declare and consent to the company seeking medical information from any doctor " \
                           "or from hospital who at anytime has attended on the life to be insured/ proposer or " \
                           "from any past or present employer concerning anything which affects the physical and " \
                           "mental health of the life to be assured/proposer and seeking information from any " \
                           "insurance company to which an application for insurance on the life to be " \
                           "assured/ proposer has been made for the purpose of underwriting the proposal " \
                           "and/or claim settlement.</li>" \
                           "<li>I/ We authorize the company to share information pertaining to my proposal including " \
                           "the medical records for the sole purpose of proposal underwriting and/or claims " \
                           "settlement and with any Governmental and/or Regulatory Authority.</li>" \
                           "</ul>" \
                           "<strong>AML guidelines:</strong>" \
                           "<ol>" \
                           "<li>I/We hereby confirm that all premiums have been/will be paid from bonafide sources " \
                           "and no premiums have been/will be paid out of proceeds of crime related to any of the " \
                           "offence listed in preventions of Money Laundering Act, 2002.</li>" \
                           "<li>I understand that the Insurance Company has the right to call for documents to establish " \
                           "sources of funds.</li>" \
                           "<li>The insurance Company has right to cancel the insurance contract in case I am/have " \
                           "been found guilty by any competent court of law under any of the statutes, directly or " \
                           "indirectly governing the prevention of money laundering in India. </li>" \
                           "</ol>" \
                           "<ul>" \
                           "<li>I have read and understood the above mention Terms & Conditions. </li>" \
                           "</ul>" \
                           "<strong>Section 41 of Insurance Act 1938 (Prohibition of rebates):</strong>" \
                           "<ol>" \
                           "<li>No person shall allow or offer to allow either directly or indirectly as an inducement " \
                           "to any person to take out or renew or continue an insurance in respect of any kind of " \
                           "risk relating to lives or property in India, any rebate of the whole or part of the " \
                           "commission payable or any rebate of premium shown on the policy, nor shall any person " \
                           "taking out or renewing or continuing a policy accept any rebate, except such rebate as " \
                           "may be allowed in accordance with the published prospectus or tables of the insurer." \
                           "</li>" \
                           "<li>Any person making default in complying with the provision of this section " \
                           "shall be punishable with fine, which may extend to five hundred rupees.</li>" \
                           "</ol>" \
                           "<strong>64VB:</strong>" \
                           "<p>Commencement of risk cover under the policy is subject to receipt and realisation of " \
                           "payable premium by Tata AIG General Insurance Company Limited. </p>" \
                           "<strong>30 Days Right To Cancel:</strong>" \
                           "<p>As you have opted to pay by card you have been provided a 30 days period from the " \
                           "date of receipt of the policy document to review the terms and conditions of this policy. " \
                           "If you have any disagreement to any of the terms and conditions, you have the right to " \
                           "cancel the policy provided no claim has been made on the policy.</p>" \
                           "<strong>General Exclusions:</strong>" \
                           "<ol>" \
                           "<li>Any Pre-existing Condition, any complication arising from it, or</li>" \
                           "<li>Any Illness, sickness or disease, other than specified as Critical Illness, as " \
                           "mentioned in the policy schedule, or</li>" \
                           "<li>Any Critical Illness of which, the signs or symptoms first occurred prior to or " \
                           "within Ninety (90) days following the Policy Issue Date or the last Commencement Date, " \
                           "whichever is later, or</li>" \
                           "<li>Any Critical Illness resulting from a physical or mental condition which existed " \
                           "before the Policy Issue Date or the last Commencement Date which was not disclosed, or</li>" \
                           "<li>Intentionally self-inflicted Injury or illness, or sexually transmitted conditions, " \
                           "mental or nervous disorder, anxiety, stress or depression, Acquired Immune Deficiency " \
                           "Syndrome (AIDS), Human Immune-deficiency Virus (HIV) infection; suicide, or</li>" \
                           "<li>War, civil war, invasion, insurrection, revolution, act of foreign enemy, " \
                           "hostilities (whether War be declared or not), rebellion, mutiny, use of military power " \
                           "or usurpation of government or military power</li>" \
                           "</ol>" \
                           "<p>For a complete list of detailed exclusions, please refer policy wordings " \
                           "<a href='http://www.tataaiginsurance.in/taig/taig/tata_aig/about_us/Media_Centre/pdf/" \
                           "policy_wordings/MediPrime_Policy_Wordings.pdf'" \
                           " target='_blank'>(Policy Wordings)</a></p>" \
                           "<h3>Coverfox: Terms and Conditions</h3>" \
                           "<p>Coverfox Insurance Broking Pvt. (hereinafter referred to as Coverfox) Limited operates " \
                           "the website <a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> to provide " \
                           "consumers choice and an easy way to buy Insurance " \
                           "products from multiple Insurance companies. Coverfox is not an insurance company. Coverfox " \
                           "is a licensed Insurance Broking Company holding a Broking licence from the Indian " \
                           "Insurance Regulator - Insurance Regulatory and Development authority. " \
                           "<a href='/docs/irda-licence/' target='_blank'>(View IRDA License)</a></p>" \
                           "<p>Our participating insurance providers supply some of the information available on the " \
                           "Website and therefore there may be inaccuracies in the Website Information over which " \
                           "Coverfox has limited control.</p>" \
                           "<p>Coverfox does not warrant or guarantee the: Timeliness, accuracy or completeness of " \
                           "the Website Information; or Quality of the results obtained from the use of the Website.</p>" \
                           "<p>To the maximum extent permitted by law, Coverfox has no liability in relation to or " \
                           "arising out of the Website Information and Website recommendations. You are responsible " \
                           "for the final choice of your product and you should take time to read through all " \
                           "information supplied before proceeding. If you are in any doubt regarding a product or " \
                           "its terms you should seek further advice from Coverfox or the relevant participating " \
                           "provider before choosing your product.</p>" \
                           "<p>Coverfox may pass on your personal information to the relevant participating provider " \
                           "if you apply to purchase a product through " \
                           "<a href='www.coverfox.com' target='_blank'>www.coverfox.com</a> , however, Coverfox does " \
                           "not guarantee when or if you will actually acquire the product that you have chosen. " \
                           "Coverfox does not accept any liability arising out of circumstances where there is delay " \
                           "in you acquiring the product you have chosen.</p>" \
                           "<p>Please note that Coverfox is only collecting or assisting in collecting  the premium " \
                           "deposit on behalf of the insurer you have chosen to buy the policy. The acceptance of " \
                           "the deposit as premium and final issuance of the policy is subject to the underwriting " \
                           "norms and discretion of the Insurer whose policy you have chosen to buy on which Coverfox " \
                           "has no control. Coverfox will ensure that the amount is refunded by the insurer in case " \
                           "there is no ultimate issuance of policy.</p>" \
                           "<p><button class='btn btn-lg cf-btn btn-red hide_overlay'>Close</button></p>" \
                           "</div>"
    slate_list = [
        {'id': 'slate_1', 'name': 'Proposer', 'class' : '', 'description' : 'The person who is paying for the policy'},
        {'id': 'slate_2', 'name' : 'Insured Members', 'class' : '', 'description' : 'People being insured under the policy'},
        {'id': 'slate_3', 'name' : 'Nominee', 'class' : '', 'description' : 'Nominees for the insured members'},
        {'id': 'slate_4', 'name' : 'Contact Info.', 'class' : '', 'description' : 'Contact Information of the proposer'},
        {'id': 'slate_5', 'name' : 'Medical History', 'class' : '', 'description' : 'Medical History of the people being insured'},
        {'id': 'slate_6', 'name' : 'Previous Policy', 'class' : '', 'description' : 'Previous policy details', 'style' : " 'width', '0px' "},
        {'id': 'slate_7', 'name' : 'T & C', 'class' : 'last', 'description' : 'Mandatory terms and conditions', 'style' : " 'width', '0px' "}
    ]
    data = {
        'slate_list': slate_list,
        'proposer_data': proposer_data,
        'plan_data': plan_data,
        'stages': stages,
        'last_stage': stages[-1]['template'],
        'terms_and_conditions': terms_and_conditions,
    }
    return data
