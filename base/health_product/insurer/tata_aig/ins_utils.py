import putils
def download_buy_form(transaction):
    order = []
    rows = []

    proposer_data = transaction.insured_details_json['form_data']['proposer']['personal']
    proposer_address = transaction.insured_details_json['form_data']['proposer']['address']
    proposer_contact = transaction.insured_details_json['form_data']['proposer']['contact']
    insured_data = transaction.insured_details_json['form_data']['insured']
    nominee_data = transaction.insured_details_json['form_data']['nominee']
    pre_existing = transaction.insured_details_json['form_data']['medical_question_pre_existing']['pre_existing']
    admit = transaction.insured_details_json['form_data']['medical_question_admit']['admit']
    medication = transaction.insured_details_json['form_data']['medical_question_admit']['medication']
    operation = transaction.insured_details_json['form_data']['medical_question_operation']['operation']
    illness = transaction.insured_details_json['form_data']['medical_question_illness']['illness']
    declined = transaction.insured_details_json['form_data']['medical_question_declined']['declined']
    other = transaction.insured_details_json['form_data']['medical_question_other']['other']
    existing_policies = transaction.insured_details_json['form_data']['existing_policies']

    rows.append(['Proposer Details'])
    rows.append(['Name', ' '.join([proposer_data['first_name']['value'], proposer_data['last_name']['value']])])
    rows.append(['Gender', proposer_data['gender']['display_value']])
    rows.append(['Marital Status', proposer_data['marital_status']['display_value']])
    rows.append(['Date of Birth', proposer_data['date_of_birth']['display_value']])
    rows.append(['Nationality', proposer_data['nationality']['display_value']])
    rows.append(['Identity Proof', proposer_data['id_proof_type']['display_value']+', Number:'+proposer_data['id_proof_number']['display_value']])
    rows.append(['Type of Organisation', proposer_data['type_of_organisation']['display_value']])
    rows.append(['Source of Income', proposer_data['source_of_income']['display_value']])
    rows.append(['Two Year Discount', proposer_data['two_year_discount']['display_value']])

    rows.append([])
    rows.append(['Insured Members'])
    for member in insured_data:
        rows.append([])
        rows.append(['Member', member])
        rows.append(['Name', ' '.join([insured_data[member]['first_name']['display_value'], insured_data[member]['last_name']['display_value']])])
        rows.append(['Date of Birth', insured_data[member]['date_of_birth']['display_value']])
        rows.append(['Marital Status', insured_data[member]['marital_status']['display_value']])
        rows.append(['Height', str(insured_data[member]['height']['value1'])+' feet '+str(insured_data[member]['height']['value2'])+' inches.'])
        rows.append(['Weight (kgs)', insured_data[member]['weight']['value']])


    rows.append([])
    rows.append(['Nominee Details'])
    for member in nominee_data:
        rows.append([])
        rows.append(['Nominee for which member?', member])
        rows.append(['Name', ' '.join([nominee_data[member]['first_name']['value'], nominee_data[member]['last_name']['value']])])
        rows.append(['Date of Birth', nominee_data[member]['date_of_birth']['display_value']])
        rows.append([nominee_data[member]['relation']['label'], nominee_data[member]['relation']['value']])


    rows.append([])
    rows.append(['Proposer Contact Details'])
    rows.append(['Address', ', '.join([proposer_address['address1']['value'], proposer_address['address2']['value'], proposer_address['landmark']['value'], proposer_address['city']['display_value'], proposer_address['state']['value'], proposer_address['pincode']['value']])])
    rows.append(['Email', proposer_contact['email']['value']])
    rows.append(['LandLine', proposer_contact['landline']['std_code']+'-'+proposer_contact['landline']['landline']])
    rows.append(['Mobile', proposer_contact['mobile']['value']])


    rows.append([])
    rows.append(['Medical Details'])
    for item in pre_existing:
        items = item.split('_')
        member = items[0]
        rows.append([])
        rows.append(['Member', member])
        rows.append([pre_existing[item]['label'], pre_existing[item]['value']])

    for item in admit:
        items = item.split('_')
        member = items[0]
        rows.append([])
        rows.append(['Member', member])
        rows.append([admit[item]['label'], admit[item]['value']])


    for item in medication:
        items = item.split('_')
        member = items[0]
        rows.append([])
        rows.append(['Member', member])
        rows.append([medication[item]['label'], medication[item]['value']])

    for item in illness:
        items = item.split('_')
        member = items[0]
        rows.append([])
        rows.append(['Member', member])
        rows.append([illness[item]['label'], illness[item]['value']])

    for item in operation:
        items = item.split('_')
        member = items[0]
        rows.append([])
        rows.append(['Member', member])
        rows.append([operation[item]['label'], operation[item]['value']])

    for item in declined:
        items = item.split('_')
        member = items[0]
        rows.append([])
        rows.append(['Member', member])
        rows.append([declined[item]['label'], declined[item]['value']])

    for item in other:
        items = item.split('_')
        member = items[0]
        rows.append([])
        rows.append(['Member', member])
        rows.append([other[item]['label'], other[item]['value']])

    rows.append([])
    rows.append(['Previous Policy'])
    for item in existing_policies:
        for i in item:
            if i != 'insurer_options':
                rows.append([i, item[i]])

    file_name = 'Form_Data_%s.xls' % transaction.transaction_id
    return putils.render_excel(file_name, order, rows)
