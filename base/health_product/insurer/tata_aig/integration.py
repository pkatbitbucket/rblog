import datetime
from health_product.models import Transaction
from wiki.models import CoverPremiumTable, FamilyCoverPremiumTable
# from dateutil.relativedelta import *
import hashlib
from utils import request_logger
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from django.conf import settings
#UW referral
#Any medical questionnaire being answered positive
#BMI for member aged 16 yrs & above with BMI equal to or more than 37


#PPC
#Age above 50 yrs
#SI above 5L
#BMI for member aged 16 yrs & above with BMI less than 17


# FOR PRODUCTION
if settings.PRODUCTION:
    PAYMENT_URL = 'https://webpos.tataaiginsurance.in/pgprd/pgrequest.jsp'
else:
    # FOR UAT
    PAYMENT_URL = 'https://webpos.tataaiginsurance.in/pguat/pgrequest.jsp'



HASH_KEY = '67H571iF5ol7q1n8'
#Vendor code is a.k.a Partner ID
VENDOR_CODE = 'MCOVERFOX'
PRODUCER_CODE = '0019024000'


PRODUCT_CODE = {
    'individual' : 'MEDINV',
    'family' : 'MEDFAM'
}

SALUTATION_CHOICES = {
    1: 'Mr.',
    2: 'Ms.'
}
GENDER_CHOICES = {
    1 : 'M',
    2 : 'F'
}


def post_transaction(transaction):
    redirect_url = '/health-plan/product/transaction/%s/failure/' % transaction.transaction_id
    is_redirect = True
    template_name = ''
    data = {}

    policy_type = transaction.slab.health_product.policy_type.lower()
    if policy_type == 'individual':
        cpt = CoverPremiumTable.objects.get(id=transaction.extra['cpt_id'])
    else:
        cpt = FamilyCoverPremiumTable.objects.get(id=transaction.extra['cpt_id'])

    pdata = transaction.insured_details_json['form_data']['proposer']['personal']
    if 'marital_status' in pdata.keys():
        transaction.proposer_marital_status = pdata['marital_status']['value']
    if 'date_of_birth' in pdata.keys():
        transaction.proposer_date_of_birth = datetime.datetime.strptime(pdata['date_of_birth']['value'], '%d/%m/%Y')
    if 'gender' in pdata.keys():
        transaction.proposer_gender = int(pdata['gender']['value'])
    transaction.save()

    transaction.reference_id = 'CFOX-%s-%s' % (transaction.slab.health_product.insurer.id, transaction.id)
    if pdata['two_year_discount']['value'] == 'Yes':
        transaction.premium_paid = transaction.extra['two_year_premium']
    else:
        transaction.premium_paid = cpt.premium
    transaction.save()
    codestring = '%s?vendorcode=%s&referencenum=%s&amount=%s&productcode=%s&action=%s&producercode=%s%s' % (
        PAYMENT_URL,
        VENDOR_CODE,
        transaction.reference_id,
        str(int(transaction.premium_paid)),
        # '2.0',
        PRODUCT_CODE[policy_type],
        'action',
        PRODUCER_CODE,
        HASH_KEY
    )

    hash_object = hashlib.md5(codestring)
    hash = hash_object.hexdigest()

    data = {
        'vendorcode': VENDOR_CODE,
        'referencenum': transaction.reference_id,
        'amount': str(int(transaction.premium_paid)),
        # 'amount' : '2.0',
        'productcode': PRODUCT_CODE[policy_type],
        'producercode': PRODUCER_CODE,
        'action': 'action',
        'hash': hash
    }

    request_logger.info('\nTATA AIG TRANSACTION: %s \n PAYMENT REQUEST SENT:\n %s \n' % (transaction.transaction_id, data))

    redirect_url = PAYMENT_URL
    is_redirect = False
    transaction.payment_request = data
    transaction.save()
    return is_redirect, redirect_url, template_name, data



def post_payment(payment_response, transaction=None):
    redirect_url = '/health-plan/payment/failure/'
    is_redirect = True
    request_logger.info('\nTATA AIG: \n PAYMENT RESPONSE RECEIVED:\n %s \n' % (payment_response))
    pay_res = payment_response['msg'].split('|')
    transaction = Transaction.objects.get(reference_id = pay_res[0])
    redirect_url = '/health-plan/payment/transaction/%s/failure/' % transaction.transaction_id
    transaction.payment_token = pay_res[4]
    transaction.policy_token = pay_res[4]
    transaction.premium_paid = pay_res[8]
    transaction.extra['AuthStatus'] = pay_res[12]
    transaction.extra['payment_response'] = pay_res
    transaction.payment_response = payment_response
    transaction.save()
    if transaction.extra['AuthStatus'] == '0300':
        redirect_url = '/health-plan/payment/transaction/%s/success/' % (transaction.transaction_id)
        transaction.payment_done = True
        transaction.payment_on = timezone.now()
        transaction.policy_start_date = (timezone.now() + relativedelta(days=1)).date()
        transaction.policy_end_date = (timezone.now() + relativedelta(years=1)).date()
        transaction.save()

        msg_text = 'Transaction ID: %s\n\nTransaction UID: %s\n\n' % (transaction.id, transaction.transaction_id)
        msg_text = '%sTO PROCESS: Transaction for TATA AIG' % msg_text
        sub_text = 'ALERT: TATA-AIG Transaction'
        #send_mail(sub_text,msg_text, "Dev Bot <dev@cfstatic.org>",['aniket@coverfox.com', 'varun@coverfox.com', 'jack@coverfox.com'])

    return is_redirect, redirect_url, '', {}
