GENDER_OPTIONS = [
    {'id': 1, 'name': 'Male'},
    {'id': 2, 'name': 'Female'}
]
MARITAL_STATUS_OPTIONS = [
    {'id': 'MARRIED', 'name': 'Married'},
    {'id': 'SINGLE', 'name': 'Single'}
]
ID_PROOF_OPTIONS = [
    {'id': 'Pan Card', 'name': 'Pan Card'},
    {'id': 'Passport', 'name': 'Passport'},
    {'id': 'Driving License', 'name': 'Driving License'},
    {'id': 'Voters Card', 'name': 'Voter\'s Card'}
]
INCOME_OPTIONS = [
    {'id': 'Salaried', 'name': 'Salaried'},
    {'id': 'Business', 'name': 'Business'},
    {'id': 'Others', 'name': 'Others'}
]

ORGANISATION_OPTIONS = [
    {'id': 'Corporations', 'name': 'Corporations'},
    {'id': 'Governments', 'name': 'Governments'},
    {'id': 'Non Governmental Organizations', 'name': 'Non Governmental Organizations'},
    {'id': 'Society', 'name': 'Society'},
    {'id': 'Trust', 'name': 'Trust'},
    {'id': 'Partnership', 'name': 'Partnership'},
    {'id': 'International Organization', 'name': 'International Organization'},
    {'id': 'Cooperatives', 'name': 'Cooperatives'},
    {'id': 'Section 25 Company', 'name': 'Section 25 Company'}
]

NATIONALITY_OPTIONS = [
    {'id': 'Indian', 'name': 'Indian'},
    {'id': 'Other', 'name': 'Other'},
]

