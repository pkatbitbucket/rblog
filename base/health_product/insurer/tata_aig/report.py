from wiki.models import CoverPremiumTable, FamilyCoverPremiumTable
import datetime

VENDOR_CODE = 'MCOVERFOX'
PRODUCER_CODE = '0019024000'

SALUTATION_CHOICES = {
    1: 'Mr.',
    2: 'Ms.'
}

GENDER_CHOICES = {
    1: 'Male',
    2: 'Female'
}
NOMINEE_RELATION_CHOICES = {
    'Spouse' : 'Spouse',
    'Husband' : 'Spouse',
    'Wife' : 'Spouse',
    'Father' : 'Parent',
    'Mother' : 'Parent',
    'Brother' : 'Sibling',
    'Sister' : 'Sibling',
    'Son' : 'Child',
    'Daughter' : 'Child',
    'Grandfather' : 'GrandParent',
    'Grandmother' : 'GrandParent',
    'Grandson' : 'GrandChild',
    'Granddaughter' : 'GrandChild',
    'Brother-in-Law' : 'Brother-in-Law',
    'Sister-in-Law' : 'Sister-in-Law',
    'Nephew' : 'Nephew',
    'Niece' : 'Niece'
}

NOMINEE_GENDER_CHOICES = {
    'Husband' : 'Mr',
    'Wife' : 'Mrs',
    'Father' : 'Mr',
    'Mother' : 'Mrs',
    'Brother' : 'Mr',
    'Sister' : 'Ms',
    'Son' : 'Mr',
    'Daughter' : 'Ms',
    'Grandfather' : 'Mr',
    'Grandmother' : 'Mrs',
    'Grandson' : 'Mr',
    'Granddaughter' : 'Ms',
    'Brother-in-Law' : 'Mr',
    'Sister-in-Law' : 'Ms',
    'Nephew' : 'Mr',
    'Niece' : 'Ms'
}

RELATIONSHIP_CHOICES = {
    'self' : 'Self',
    'spouse' : 'Spouse',
    'father' : 'Father',
    'mother' : 'Mother',
    'son' : 'Son',
    'son1' : 'Son',
    'son2' : 'Son',
    'son3' : 'Son',
    'son4' : 'Son',
    'daughter' : 'Daughter',
    'daughter1' : 'Daughter',
    'daughter2' : 'Daughter',
    'daughter3' : 'Daughter',
    'daughter4' : 'Daughter',
}

def get_full_name(**kwargs):
    name_list = []
    if 'first_name' in kwargs.keys() and kwargs['first_name'].strip():
        name_list.append(kwargs['first_name'].strip())
    if 'middle_name' in kwargs.keys() and kwargs['middle_name'].strip():
        name_list.append(kwargs['middle_name'].strip())
    if 'last_name' in kwargs.keys() and kwargs['last_name'].strip():
        name_list.append(kwargs['last_name'].strip())
    return ' '.join(name_list)

def calculate_age(born):
    today = datetime.date.today()
    try:
        birthday = born.replace(year=today.year)
    except ValueError: # raised when birth date is February 29 and the current year is not a leap year
        birthday = born.replace(year=today.year, day=born.day-1)
    if birthday > today:
        return today.year - born.year - 1
    else:
        return today.year - born.year

def download_data(t):
    if t.slab.health_product.policy_type == "FAMILY":
        cpt = FamilyCoverPremiumTable.objects.get(id=t.extra['cpt_id'])
    else:
        cpt = CoverPremiumTable.objects.get(id=t.extra['cpt_id'])


    prop_height = ''
    prop_weight = ''
    prop_age = ''
    prop_bmi = ''
    id_proof_type = ''
    id_proof_number = ''
    source_of_income = ''
    self_gender = 1 if t.extra['gender'] == 'MALE' else 2
    spouse_gender = 2 if self_gender == 1 else 2
    for code, fdata in t.insured_details_json['form_data']['insured'].items():
        if code == 'self':
            #OCCUPATION AND MARITAL STATUS ONLY ASKED FOR FLOATER
            feet = fdata['height']['value1']
            inches = fdata['height']['value2']
            if not feet:
                feet = 0
            if not inches:
                inches = 0
            prop_height = str(int(((int(feet) * 30.48) + int(inches) * 2.54)))
            prop_weight = fdata['weight']['value']
            prop_bmi = round(float(int(prop_weight) / ((int(prop_height) / 100.0) * (int(prop_height) / 100.0))))
            prop_age = calculate_age(datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y').date())

    pdata = t.insured_details_json['form_data']['proposer']['personal']
    id_proof_type = pdata['id_proof_type']['value']
    id_proof_number = pdata['id_proof_number']['value']
    source_of_income = pdata['source_of_income']['value']
    t.proposer_marital_status = pdata['marital_status']['value']


    error_msg = ''
    big_header = [
        {'Proposer Details' : [0,0,0,28]},
        {'Proposer Nominee Details' : [0,0,29,47]}
    ]
    big_header_last_cell = 47

    data_row_list = []
    temp_tuple = []

    cdata = t.insured_details_json['form_data']['proposer']['contact']
    adata = t.insured_details_json['form_data']['proposer']['address']

    proposer_data = [
        #PROPOSER DETAILS
        t.created_on.date(),
        'Yes' if 'self' in t.insured_details_json['form_data']['insured'].keys() else 'No',
        t.reference_id,
        SALUTATION_CHOICES[int(t.proposer_gender)],
        t.proposer_first_name,
        t.proposer_middle_name,
        t.proposer_last_name,
        datetime.datetime.strftime(t.proposer_date_of_birth, '%d/%m/%Y'),
        GENDER_CHOICES[int(t.proposer_gender)],
        t.proposer_marital_status,
        cdata['landline']['display_value'],
        t.proposer_mobile,
        t.proposer_email,
        str(int(t.slab.sum_assured)),
        prop_height,
        prop_weight,
        t.proposer_address1,
        t.proposer_address2,
        t.proposer_address_landmark,
        t.proposer_state,
        t.proposer_city,
        t.proposer_pincode,
        'Indian',
        id_proof_type,
        id_proof_number,
        '',
        source_of_income,
        '',
        ''
    ]
    insured_data = []
    adult_counter = 0
    child_counter = 0
    for code, fdata in t.insured_details_json['form_data']['insured'].items():
        ndata = t.insured_details_json['form_data']['nominee'][code]
        ins_marital_status = fdata['marital_status']['value']
        feet = fdata['height']['value1']
        inches = fdata['height']['value2']
        if not feet:
            feet = 0
        if not inches:
            inches = 0
        ins_height = str(int(((int(feet) * 30.48) + int(inches) * 2.54)))
        ins_weight = fdata['weight']['value']

        ins_bmi = round(float(int(ins_weight) / ((int(ins_height) / 100.0) * (int(ins_height) / 100.0))))
        ins_age = calculate_age(datetime.datetime.strptime(fdata['date_of_birth']['value'], '%d/%m/%Y').date())

        if (code == 'self') or (code == 'spouse' and 'self' not in t.insured_details_json['form_data']['insured'].keys()):
            nominee_data = [
                GENDER_CHOICES[spouse_gender] if ndata['relation']['value'] == 'Spouse' else NOMINEE_GENDER_CHOICES[ndata['relation']['value']],
                ndata['first_name']['value'],
                '',
                ndata['last_name']['value'],
                NOMINEE_RELATION_CHOICES[ndata['relation']['value']],
                ndata['date_of_birth']['value'],
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                ''
            ]


        ins_gender = int([member['gender'] for member in t.extra['members'] if member['id'] == code][0])
        insured_data.extend([
            SALUTATION_CHOICES[ins_gender],
            fdata['first_name']['value'],
            '',
            fdata['last_name']['value'],
            fdata['date_of_birth']['value'],
            GENDER_CHOICES[ins_gender],
            ins_marital_status,
            RELATIONSHIP_CHOICES[code],
            t.slab.sum_assured,
            ins_height,
            ins_weight,
            ins_bmi
        ])

        if code in ['self', 'spouse', 'father', 'mother']:
            adult_counter += 1
            big_header.append({'Adult %s details (%s)' % (adult_counter, code) : [0, 0, big_header_last_cell + 1, big_header_last_cell + 12]})
            big_header_last_cell += 12
        else:
            child_counter += 1
            big_header.append({'Child %s details (%s)' % (child_counter, code) : [0, 0, big_header_last_cell + 1, big_header_last_cell + 12]})
            big_header_last_cell += 12

    medical_data = [
        '',
        get_full_name(first_name=fdata['first_name']['value'], last_name=fdata['last_name']['value']),
        'NA',
        'NA',
        'NA',
        'NA',
        'NA',
        'NA',
        'NA',
        'NA',
        'NA',
        'NA',
        'NA',
        'NIL',  #TODO Ask question for 'Claims lodged during the preceding 3 years',
        'No',
        'Yes'
    ]


    payment_data = [
        'Online',
        t.premium_paid,
        'NA',
        #TODO Add later
        'NA',   # 'Credit Card No',
        'NA',   # 'Expiry Date of CC',
        'NA',   # 'Card Type',
        'NA',   # 'Bank Name',
        'NA',   # 'Billing Org',
        t.reference_id,     # 'Transaction ID',
        VENDOR_CODE,    # 'Vendor Code',
        PRODUCER_CODE,  # 'Producer Code',
        t.extra['payment_response'][6] if 'payment_response' in t.extra.keys() else '',     # 'Merchant ID',
        'Success',      # 'Payment Status',
        t.payment_on,       # 'Billdesk Transaction Date & Time',
    ]

    temp_tuple.extend(proposer_data)
    temp_tuple.extend(nominee_data)
    temp_tuple.extend(insured_data)
    temp_tuple.extend(medical_data)
    temp_tuple.extend(payment_data)

    data_row_list.append(temp_tuple)

    order = [
        #Proposer Details
        'Date',
        'Is Proposer main insured',
        'Application No',
        'Title',
        'First Name',
        'Middle Name',
        'Last Name',
        'Date of Birth',
        'Gender',
        'Marital Status',
        'Contact Number',
        'Mobile Number',
        'Email Id',
        'Sum Insured (Rs)',
        'Height (cms)',
        'Weight(Kgs)',
        'Communication Address',
        'Address 2',
        'Address 3',
        'State',
        'City',
        'Pin Code',
        'Nationality',
        'Id Proof Type',
        'Id Proof Number',
        'AML KYC Details',
        'Source Of Income',
        'Pan Card',
        'Option Selected',
        #Nominee Details
        'Title',
        'First Name',
        'Middle Name',
        'Last Name',
        'Nominee Relationship with insured (Proposer)',
        'Nominee Date of birth',
        'Appointee Name (If nominee is minor)',
        'Title',
        'First Name',
        'Middle Name',
        'Last Name',
        'Relationship with Nominee',
        'Address of Appointee',
        'Communication Address 1',
        'Address 2',
        'Address 3',
        'State',
        'City',
        'Pin Code',
        ]
    for code, fdata in t.insured_details_json['form_data']['insured'].items():
        #Insured Details
        order.extend([
            'Title',
            'First Name',
            'Middle Name',
            'Last Name',
            'Date of Birth',
            'Gender',
            'Marital Status',
            'Relationship to Proposer',
            'Sum Insured (Rs)',
            'Height (cms)',
            'Weight(Kgs)',
            'BMI Result'
        ])

    order.extend([
        #MEDICAL QUESTIONS
        'Medical Declaration',
        'Insured Name',
        'Name of Pre-Existing Disease /Illness/Surgery',
        'Diagnosis Date',
        'Date Of last Consultation',
        'Treatment Inpatient/ Outpatient',
        'Doctor(s) Name',
        'Hospital(s) Name',
        'Hospital(s) Phone No. with STD code.',
        'Policyholder Name/ Policy No./Application No.',
        'Insurer',
        'Period Of Insurance',
        'Sum Insured (Rs.) & Cumulative Bonus (CB) %',
        'Claims lodged during the preceding 3 years',
        'You want us to consider these details for Portability?',
        'Co -Insurance',
        #PAYMENT DETAILS
        'Mode of Payment',
        'Premium Amount',
        'Payment Frequeny',
        'Credit Card No',
        'Expiry Date of CC',
        'Card Type',
        'Bank Name',
        'Billing Org',
        'Transaction ID',
        'Vendor Code',
        'Producer Code',
        'Merchant ID',
        'Payment Status',
        'Billdesk Transaction Date & Time'
    ])

    big_header.append({'Medical Questionare': [0, 0, big_header_last_cell + 1, big_header_last_cell + 16]})
    big_header_last_cell += 16

    big_header.append({'Payment Details' : [0, 0, big_header_last_cell + 1, big_header_last_cell + 14]})
    big_header_last_cell += 14


    filename = "proposal_%s.xls" % t.transaction_id


    data = {
        'data_row_list' : data_row_list,
        'order' : order,
        'filename' : filename,
        'big_header' : big_header
    }
    return data

