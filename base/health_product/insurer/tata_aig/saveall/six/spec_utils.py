from health_product.insurer import settings as insurer_settings
from health_product.models import *
from wiki.models import *
import urllib2, urllib
import datetime
import pprint
import ast
from dateutil.relativedelta import *
from health_product.insurer import custom_utils


def save_stage(transaction, stage_data):
    current_stage = stage_data['template']
    red_flag = []
    saved_data = stage_data['data']['data'][0]['data'][0][0]['saved_data']
    if 'existing_insurance' in saved_data.keys():
        form_data = {
            'existing_insurance': saved_data['existing_insurance']
        }
    else:
        form_data = {}
    print "======================================"
    import pprint
    pprint.pprint(form_data)


    # print "FORM DATA"
    # pprint.pprint(form_data)
    for k, v in form_data.items():
        if 'form_data' not in transaction.insured_details_json.keys():
            transaction.insured_details_json['form_data'] = {}
        if k in transaction.insured_details_json['form_data'].keys():
            transaction.insured_details_json['form_data'][k].update(**v)
        else:
            transaction.insured_details_json['form_data'][k] = v
    transaction.insured_details_json['form_data']['existing_policies'] = stage_data['existingPolicies']
    transaction.save()
    #================== CHECK FOR MEDICAL ====================
    if 'existing_insurance' in form_data.keys() and form_data['existing_insurance']['value'] == 'Yes':
        red_flag.append({'question': 'Previous Insurance', 'answer': 'Yes'})

    #=========================================================
    transaction.extra['previous_red_flag'] = red_flag
    if 'red_flags' not in transaction.extra:
        transaction.extra['red_flags'] = {}
    transaction.extra['red_flags'][stage_data['template']] = {'reason':'DISEASE','data':red_flag}
    transaction.save()
    return transaction
