from health_product.insurer import settings as insurer_settings
from health_product.models import *
from wiki.models import *
import urllib2, urllib
import datetime
import ast
from dateutil.relativedelta import *

def save_transaction(transaction):
    print ">>>>>>>>> REACHED SAVE >>>>>>>>>>>>>>>>>>>"
    red_flag = []
    for j in transaction.insured_details_json['proposer']['jokerArray']:
        if j['code'] == 'marital_status':
            transaction.proposer_marital_status = j['joker']['answer']
        elif j['code'] == 'date_of_birth':
            transaction.proposer_date_of_birth = datetime.datetime.strptime(j['joker']['dateOfBirth'], '%d/%m/%Y').date()
        elif j['code'] == 'gender':
            transaction.proposer_gender = int(j['joker']['answer'])
    transaction.no_of_people_insured = len(transaction.insured_details_json['insuredArray'])
    #pprint.pprint(transaction.insured_details_json)

    for i in transaction.insured_details_json['insuredArray']:
        ins_height = ''
        ins_weight = ''
        feet = ''
        inches = ''
        for j in i['jokerArray']:
            #OCCUPATION AND MARITAL STATUS ONLY ASKED FOR FLOATER
            if j['code'] == 'height':
                feet = j['joker']['answer1']
                inches = j['joker']['answer2']
                ht = ((int(feet) * 100) + int(inches)) / 100.00
                ins_height_cms = ((int(feet) * 30.48) + int(inches)* 2.54)
                ins_height = ins_height_cms / 100
            elif j['code'] == 'weight':
                ins_weight = int(j['joker']['value'])

        bmi = ins_weight / (ins_height * ins_height)
        age = relativedelta(datetime.datetime.today(), datetime.datetime.strptime(i['dateOfBirth'], '%d/%m/%Y').date()).years

        if (age < 16 and bmi < 17) or (age >= 16 and bmi >= 37):
            red_flag.append({
                'question': 'Height & Weight',
                'answer': '%s ft %s inches and %s kgs' % (feet, inches, ins_weight),
                'reason': 'The Body Mass Index (BMI) in not normal. It\'s determined by your height & weight with respect to your age'
            })


    for i in transaction.insured_details_json['medicalQuestionsArray']:
        for item in i['questionArray']:
            if 'answer' in item.keys() and item['answer']:
                for o in item['options']:
                    if 'is_correct' in o.keys():
                        if o['id'] == item['answer'] and not o['is_correct']:
                            if item['code'] == 't_and_c':
                                red_flag.append({'question': 'I hereby agree to the Terms and Conditions', 'answer': o['id']})
                            else:
                                red_flag.append({'question': item['content'], 'answer': o['id']})
                    elif item['answer'].strip() and item['answer'].upper() != 'NONE':
                        red_flag.append({'question': item['content'], 'answer': o['id']})
        for item in i['conditionalQuestionArray']:
            if 'answer' in item.keys() and item['answer']:
                for o in item['options']:
                    if 'is_correct' in o.keys():
                        if o['id'] == item['answer'] and not o['is_correct']:
                            red_flag.append({'question': item['content'], 'answer': o['id']})
                    elif item['answer'].strip():
                        red_flag.append({'question': item['content'], 'answer': o['id']})
    if transaction.insured_details_json['existingPolicies']:
        for i in transaction.insured_details_json['existingPolicies']:
            if 'isClaims' in i.keys() and i['isClaims'] == 'Yes':
                red_flag.append({'question': 'Have you lodged any claims in the previous 3 years', 'answer': 'Yes'})
            if 'isPortability'  in i.keys() and i['isPortability'] == 'Yes':
                red_flag.append({'question': 'Do you want us to consider an existing/previous policy for Portability?', 'answer': 'Yes'})

    if transaction.insured_details_json['co_insurance'] not in ['Yes', 'No']:
        red_flag.append({'question': 'I agree to exercise Coinsurance with Tata AIG General Insurance Company ltd. '
                                     '(Lead insurer) and Apollo Munich Health Insurance Company ltd (co-Insurer).', 'answer': 'Other'})

    print ">>>>>>>>> RED FLAG >>>>>>>>>>>>>>", red_flag
    transaction.extra['red_flag'] = red_flag
    transaction.save()
    return transaction, red_flag

