# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
import health_product.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CallAlert',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phone_number', models.CharField(max_length=15)),
                ('via', models.CharField(max_length=150, blank=True)),
                ('remarks', models.TextField(blank=True)),
                ('extra', jsonfield.fields.JSONField(default={}, verbose_name='extra', blank=True)),
                ('is_active', models.BooleanField(default=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(max_length=50, choices=[(b'DRAFT', b'DRAFT'), (b'PENDING', b'Pending'), (b'IN_PROCESS', b'In Process'), (b'COMPLETED', b'Completed'), (b'CANCELLED', b'Cancelled'), (b'FAILED', b'Failed'), (b'MANUAL COMPLETED', b'Manual Completed')])),
                ('amount', models.FloatField(null=True, blank=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('redirect_url', models.CharField(max_length=255)),
                ('extra', jsonfield.fields.JSONField(default={}, verbose_name='extra', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='PincodeMapping',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pin', models.CharField(max_length=100)),
                ('city_detail', jsonfield.fields.JSONField(default={}, verbose_name='city_details', blank=True)),
                ('area_detail', jsonfield.fields.JSONField(default={}, verbose_name='area_details', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('transaction_id', models.CharField(max_length=255, verbose_name='Transaction ID')),
                ('policy_number', models.CharField(max_length=250, blank=True)),
                ('policy_start_date', models.DateTimeField(null=True, blank=True)),
                ('policy_end_date', models.DateTimeField(null=True, blank=True)),
                ('reference_id', models.CharField(max_length=250, blank=True)),
                ('policy_token', models.CharField(max_length=250, blank=True)),
                ('payment_token', models.CharField(max_length=250, blank=True)),
                ('premium_paid', models.CharField(max_length=10, blank=True)),
                ('payment_on', models.DateTimeField(null=True, blank=True)),
                ('proposer_first_name', models.CharField(max_length=255, verbose_name='First name')),
                ('proposer_middle_name', models.CharField(max_length=255, verbose_name='First name', blank=True)),
                ('proposer_last_name', models.CharField(max_length=255, verbose_name='Last name')),
                ('proposer_address1', models.CharField(max_length=100, verbose_name='Street')),
                ('proposer_address2', models.CharField(max_length=100, verbose_name='Street', blank=True)),
                ('proposer_address_landmark', models.TextField(verbose_name='Landmark', blank=True)),
                ('proposer_city', models.CharField(max_length=100, verbose_name='City')),
                ('proposer_state', models.CharField(max_length=100, verbose_name='State')),
                ('proposer_pincode', models.CharField(max_length=10, verbose_name='Pincode')),
                ('proposer_email', models.EmailField(max_length=254, verbose_name='Email')),
                ('proposer_mobile', models.CharField(max_length=10, verbose_name='Mobile')),
                ('proposer_landline', models.CharField(max_length=12, verbose_name='Landline')),
                ('proposer_date_of_birth', models.DateField(null=True, blank=True)),
                ('proposer_gender', models.CharField(max_length=20, verbose_name='Gender', choices=[(b'MALE', b'Male'), (b'FEMALE', b'Female')])),
                ('proposer_marital_status', models.CharField(max_length=20, verbose_name='Marital Status', choices=[(b'SINGLE', b'Single'), (b'MARRIED', b'Married')])),
                ('no_of_people_insured', models.IntegerField(blank=True, null=True, choices=[(2, 2), (3, 3), (4, 4)])),
                ('status', models.CharField(max_length=50, choices=[(b'DRAFT', b'DRAFT'), (b'PENDING', b'Pending'), (b'IN_PROCESS', b'In Process'), (b'COMPLETED', b'Completed'), (b'CANCELLED', b'Cancelled'), (b'FAILED', b'Failed'), (b'MANUAL COMPLETED', b'Manual Completed')])),
                ('policy_document', models.FileField(null=True, upload_to=health_product.models.get_policy_path, blank=True)),
                ('third_party', models.CharField(max_length=100, blank=True)),
                ('payment_done', models.BooleanField(default=False)),
                ('insured_details_json', jsonfield.fields.JSONField(default={}, verbose_name='insured_details', blank=True)),
                ('extra', jsonfield.fields.JSONField(default={}, verbose_name='extra', blank=True)),
                ('mailer_flag', models.BooleanField(default=False)),
                ('policy_request', models.TextField(blank=True)),
                ('policy_response', models.TextField(blank=True)),
                ('payment_request', models.TextField(blank=True)),
                ('payment_response', models.TextField(blank=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
