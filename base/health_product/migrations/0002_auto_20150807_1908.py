# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('health_product', '0001_initial'),
        ('wiki', '0001_initial'),
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='slab',
            field=models.ForeignKey(to='wiki.Slab'),
        ),
        migrations.AddField(
            model_name='transaction',
            name='tracker',
            field=models.ForeignKey(to='core.Tracker'),
        ),
        migrations.AddField(
            model_name='order',
            name='transaction',
            field=models.ForeignKey(to='health_product.Transaction'),
        ),
        migrations.AddField(
            model_name='callalert',
            name='tracker',
            field=models.ForeignKey(to='core.Tracker'),
        ),
        migrations.AddField(
            model_name='callalert',
            name='transaction',
            field=models.ForeignKey(blank=True, to='health_product.Transaction', null=True),
        ),
    ]
