# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('health_product', '0027_insert_maxbupa_premium_table_family'),
        ('health_product', '0005_populate_policies'),
    ]

    operations = [
    ]
