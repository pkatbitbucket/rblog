# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations, connection
from django.conf import settings
import os


def create_db_sequence_for_app_id(apps, schema_editor):
    path = settings.SETTINGS_FILE_FOLDER.joinpath('health_product/insurer/max_bupa/policy_num.txt')
    if os.path.isfile(path):
        with open(path, 'r') as f:
            app_id = int(f.read())
            try:
                cursor = connection.cursor()
                cursor.execute("CREATE SEQUENCE seq_maxbupa_app_id START %s;" % (app_id))
            except Exception as e:
                print e
    else:
        print '\n\tpolicy_num.text file not found for maxbupa'
        print '\n\tsetting initial value i.e 82710000001'
        app_id = 827100000001
        try:
            cursor = connection.cursor()
            cursor.execute("CREATE SEQUENCE seq_maxbupa_app_id START %s;" % (app_id))
        except Exception as e:
            print e


def create_db_sequence_for_app_id_revoke(apps, schema_editor):
    try:
        cursor = connection.cursor()
        cursor.execute("DROP SEQUENCE seq_maxbupa_app_id")
    except Exception as e:
        print e


def create_db_sequence_for_web_id_revoke(apps, schema_editor):
    try:
        cursor = connection.cursor()
        cursor.execute("DROP SEQUENCE seq_maxbupa_web_id")
    except Exception as e:
        print e


def create_db_sequence_for_web_id(apps, schema_editor):
    path = settings.SETTINGS_FILE_FOLDER.joinpath('health_product/insurer/max_bupa/web_insured_id.txt')
    if os.path.isfile(path):
        with open(path, 'r') as f:
            web_id = int(f.read())
            try:
                cursor = connection.cursor()
                cursor.execute("CREATE SEQUENCE seq_maxbupa_web_id START %s;" % (web_id))
            except Exception as e:
                print e
    else:
        print '\n\tweb_insured_id.txt file not found for maxbupa'
        print '\n\tsetting initial value i.e 8271000001'
        web_id = 8271000001
        try:
            cursor = connection.cursor()
            cursor.execute("CREATE SEQUENCE seq_maxbupa_web_id START %s;" % (web_id))
        except Exception as e:
            print e


class Migration(migrations.Migration):

    dependencies = [
        ('health_product', '0028_merge'),
    ]

    operations = [
        migrations.RunPython(create_db_sequence_for_app_id, create_db_sequence_for_app_id_revoke),
        migrations.RunPython(create_db_sequence_for_web_id, create_db_sequence_for_web_id_revoke),
    ]
