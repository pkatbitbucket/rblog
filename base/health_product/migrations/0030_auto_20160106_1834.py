# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, connection
from django.conf import settings


def create_db_sequence(apps, schema_editor):
    path = settings.SETTINGS_FILE_FOLDER.joinpath('health_product/insurer/cigna_ttk/policy_num.txt')
    try:
        with open(path, 'r') as f:
            policy_number = int(f.read())
            with connection.cursor() as cursor:
                cursor.execute("CREATE SEQUENCE seq_cigna_policy_num START %s;" % (policy_number))
    except:
        print '\n\tpolicy_num.text file not found for cignattk, creating database sequence at %s' % 5000
        with connection.cursor() as cursor:
            cursor.execute("CREATE SEQUENCE seq_cigna_policy_num START %s;" % (5000))


def create_db_sequence_revoke(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor = connection.cursor()
        cursor.execute("DROP SEQUENCE seq_cigna_policy_num")


class Migration(migrations.Migration):

    dependencies = [
        ('health_product', '0029_create_seq_for_app_id_n_web_id'),
    ]

    operations = [
        migrations.RunPython(create_db_sequence, create_db_sequence_revoke),
    ]
