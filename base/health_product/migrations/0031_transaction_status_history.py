# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('health_product', '0030_auto_20160106_1834'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='status_history',
            field=django.contrib.postgres.fields.ArrayField(default=[], base_field=models.CharField(db_index=True, max_length=50, choices=[(b'DRAFT', b'DRAFT'), (b'PENDING', b'Pending'), (b'IN_PROCESS', b'In Process'), (b'COMPLETED', b'Completed'), (b'CANCELLED', b'Cancelled'), (b'FAILED', b'Failed'), (b'MANUAL COMPLETED', b'Manual Completed'), (b'INACTIVE', b'Inactive')]), size=None),
        ),
    ]
