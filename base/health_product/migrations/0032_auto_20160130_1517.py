# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('health_product', '0031_transaction_status_history'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='proposer_middle_name',
            field=models.CharField(max_length=255, verbose_name='Middle name', blank=True),
        ),
    ]
