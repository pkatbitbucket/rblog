# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.db import migrations
from django.conf import settings
from django.utils.functional import lazy
import putils

file_ = None


def get_file():
    global file_
    if not file_:
        file_ = putils.get_file_from_s3(
            settings.INTEGRATION_AWS_ACCESS_KEY_ID,
            settings.INTEGRATION_AWS_SECRET_ACCESS_KEY,
            settings.INTEGRATION_BUCKET_NAME,
            'health/cigna/Cigna-familyfloater-premium-new.txt'
        )
    return file_


PREMIUM_TABLE_FILE_PATH = lazy(get_file, unicode)()


def delete_cover_premium_table(apps, schema_editor):
    Slab = apps.get_model('wiki', 'Slab')
    HealthProduct = apps.get_model('wiki', 'HealthProduct')
    lines = open(str(PREMIUM_TABLE_FILE_PATH), 'r').read().split("\n")

    for line in lines:
        if not line:
            continue
        line = line.strip()

        if line.startswith('X'):
            policy_type, n = ('family', 8) if line[
                1] == 'X' else ('individual', 7)
            splitted = line.split("|", n)

            if policy_type == 'family':
                combo_str = splitted.pop(2)
                adult_str, child_str = combo_str.split("A-")
                number_of_adults = int(adult_str)
                number_of_kids = int(child_str.replace('C', ''))

            X, payment_period, gender, states, cities, two_year_discount, product_slug, sum_assureds = splitted
            # get or create new slabs
            slabs = []
            hp = HealthProduct.objects.get(slug=product_slug)
            for sum_assured in sum_assureds.split('|'):
                slab, _ = Slab.objects.get_or_create(
                    sum_assured=int(sum_assured),
                    health_product=hp
                )
                slabs.append(slab)

            for slab in slabs:
                slab.family_cover_premium_table.clear()


def insert_premiums(apps, schema_editor):
    FamilyCoverPremiumTable = apps.get_model('wiki', 'FamilyCoverPremiumTable')
    Slab = apps.get_model('wiki', 'Slab')
    State = apps.get_model('wiki', 'State')
    City = apps.get_model('wiki', 'City')
    lines = open(str(PREMIUM_TABLE_FILE_PATH), 'r').read().split("\n")
    HealthProduct = apps.get_model('wiki', 'HealthProduct')

    cpts_to_save = []
    for line in lines:
        line = line.strip()
        if not line:
            continue
        if line.startswith('X'):
            policy_type, n = ('family', 8) if line[
                1] == 'X' else ('individual', 7)
            splitted = line.split("|", n)
            print 'Policy type:', policy_type
            print splitted

            if policy_type == 'family':
                combo_str = splitted.pop(2)
                adult_str, child_str = combo_str.split("A-")
                number_of_adults = int(adult_str)
                number_of_kids = int(child_str.replace('C', ''))

            X, payment_period, gender, states, cities, two_year_discount, product_slug, sum_assureds = splitted
            payment_period = int(payment_period.strip())

            # get or create new slabs
            slabs = []
            hp = HealthProduct.objects.get(slug=product_slug)
            for sum_assured in sum_assureds.split('|'):
                slab, _ = Slab.objects.get_or_create(
                    sum_assured=int(sum_assured),
                    health_product=hp
                )
                slabs.append(slab)

            if states.strip():
                states = [State.objects.get(name__iexact=s.strip().lower())
                          for s in states.split("+")]
            if cities.strip():
                cities = [City.objects.get(name__iexact=s.strip().lower())
                          for s in cities.split("+")]
            continue

        age_factor, premium_list = line.split('|', 1)
        lage = float(age_factor.split('-')[0])
        if lage < 1:
            lage = int(lage * 100)
        else:
            lage = int(lage * 1000)
        if age_factor.split('-')[1]:
            uage = float(age_factor.split('-')[1])
            if uage < 1:
                uage = int(uage * 100)
            else:
                uage = uage * 1000
        else:
            uage = None

        if len(premium_list.split('|')) != len(slabs):
            raise Exception("Slabs not matching premium list")

        for premium, slab in zip(premium_list.split('|'), slabs):
            if not premium:
                continue
            premium = float(premium.strip())
            cpt = FamilyCoverPremiumTable(
                payment_period=payment_period,
                adults=number_of_adults,
                kids=number_of_kids,
                lower_age_limit=lage,
                upper_age_limit=uage,
                gender=gender,
            )
            cpt._premium = premium
            cpt.save()
            if states:
                cpt.state.add(*states)
            if cities:
                cpt.city.add(*cities)
            cpt.slabs.add(slab)


class Migration(migrations.Migration):

    dependencies = [
        ("health_product", '0040_cigna_premier_premium_table_indvidual'),
    ]

    operations = [
        # omit reverse_code=... if you don't want the migration to be reversible.
        migrations.RunPython(delete_cover_premium_table, reverse_code=migrations.RunPython.noop),
        migrations.RunPython(insert_premiums, delete_cover_premium_table),
    ]
