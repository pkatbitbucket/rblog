# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0079_auto_20160404_2221'),
        ('health_product', '0041_cigna_premier_premium_table_family'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='requirement',
            field=models.ForeignKey(related_name='travel_transaction', blank=True, to='core.Requirement', null=True),
        ),
    ]
