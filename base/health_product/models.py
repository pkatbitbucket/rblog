import json
import logging
import os

from core import activity as core_activity
import django
from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.contrib.postgres.fields import ArrayField
from django.core import serializers
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from jsonfield import JSONField
from core import activity as core_activity

import utils
from account_manager.signals.signals import create_user_account, policy_updated
from core.models import Policy, RequirementKind, Tracker
from wiki.models import Slab
from core import models as core_models

logger = logging.getLogger(__name__)

""" SOUTH HACKS """
if django.VERSION < (1, 7):
    try:
        from south.modelsinspector import add_introspection_rules
        add_introspection_rules([], ["^tagging\.fields\.TagField"])
        add_introspection_rules(
            [], ["^django_extensions\.db\.models\.AutoSlugField"])
        add_introspection_rules(
            [], ["^customdb\.thumbs\.ImageWithThumbsField"])
    except ImportError:
        pass

GENDER_CHOICES = (
    ('MALE', 'Male'),
    ('FEMALE', 'Female'),
)

MARITAL_STATUS_CHOICES = (
    ('SINGLE', 'Single'),
    ('MARRIED', 'Married'),
)

NO_OF_INSURED_CHOICES = (
    (2, 2),
    (3, 3),
    (4, 4),
)
TRANSACTION_STATUS_CHOICES = (
    ('DRAFT', 'DRAFT'),
    ('PENDING', 'Pending'),
    ('IN_PROCESS', 'In Process'),
    ('COMPLETED', 'Completed'),
    ('CANCELLED', 'Cancelled'),
    ('FAILED', 'Failed'),
    ('MANUAL COMPLETED', 'Manual Completed'),
    ('INACTIVE', 'Inactive'),
)


def get_policy_path(instance, filename):
    #path_to_save = "uploads/policy_documents/%s" % (filename)
    path_to_save = "policy/%s" % (filename)
    return path_to_save


class ActiveTransactionManager(models.Manager):
    def get_queryset(self):
        return (super(ActiveTransactionManager, self).get_queryset()
                .exclude(status='INACTIVE'))


class Transaction(models.Model):
    # TODO: A background process will make status CANCELLED after 2-3 days so that the
    # transaction is closed and cannot be opened again
    tracker = models.ForeignKey(Tracker)
    slab = models.ForeignKey(Slab)
    transaction_id = models.CharField(_("Transaction ID"), max_length=255, unique=True)
    policy_number = models.CharField(max_length=250, blank=True)
    policy_start_date = models.DateTimeField(blank=True, null=True)
    policy_end_date = models.DateTimeField(blank=True, null=True)
    reference_id = models.CharField(max_length=250, blank=True)
    policy_token = models.CharField(max_length=250, blank=True)
    payment_token = models.CharField(max_length=250, blank=True)
    premium_paid = models.CharField(max_length=10, blank=True)
    payment_on = models.DateTimeField(blank=True, null=True)
    proposer_first_name = models.CharField(_("First name"), max_length=255,)
    proposer_middle_name = models.CharField(
        _("Middle name"), max_length=255, blank=True)
    proposer_last_name = models.CharField(_("Last name"), max_length=255)
    proposer_address1 = models.CharField(_("Street"), max_length=100)
    proposer_address2 = models.CharField(
        _("Street"), max_length=100, blank=True)
    proposer_address_landmark = models.TextField(_("Landmark"), blank=True)
    proposer_city = models.CharField(_("City"), max_length=100)
    proposer_state = models.CharField(_("State"), max_length=100)
    proposer_pincode = models.CharField(_("Pincode"), max_length=10)
    proposer_email = models.EmailField(_("Email"))
    proposer_mobile = models.CharField(_("Mobile"), max_length=10)
    proposer_landline = models.CharField(_("Landline"), max_length=12)
    proposer_date_of_birth = models.DateField(blank=True, null=True)
    proposer_gender = models.CharField(
        _("Gender"), max_length=20, choices=GENDER_CHOICES)
    proposer_marital_status = models.CharField(
        _("Marital Status"), max_length=20, choices=MARITAL_STATUS_CHOICES)
    no_of_people_insured = models.IntegerField(
        blank=True, null=True, choices=NO_OF_INSURED_CHOICES)
    status = models.CharField(
        db_index=True, max_length=50,
        choices=TRANSACTION_STATUS_CHOICES)
    policy = models.OneToOneField('core.Policy', null=True, blank=True, related_name='health_transaction')
    policy_document = models.FileField(
        upload_to=get_policy_path, null=True, blank=True)
    third_party = models.CharField(max_length=100, blank=True)
    payment_done = models.BooleanField(default=False)
    insured_details_json = JSONField(
        _('insured_details'), blank=True, default={})
    requirement = models.ForeignKey(
        'core.Requirement', blank=True, null=True, related_name='health_transactions'
    )
    extra = JSONField(_('extra'), blank=True, default={})
    mailer_flag = models.BooleanField(default=False)
    policy_request = models.TextField(blank=True)
    policy_response = models.TextField(blank=True)
    payment_request = models.TextField(blank=True)
    payment_response = models.TextField(blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    status_history = ArrayField(status, default=[])
    objects = ActiveTransactionManager()
    all_objects = models.Manager()

    def __str__(self):
        return "%s" % (self.transaction_id)

    def update_policy(self, content, send_mail=True):
        policy_path = get_policy_path(self, u'{}.pdf'.format(self.transaction_id))
        with default_storage.open(policy_path, 'wb') as f:
            for chunk in ContentFile(content).chunks():
                f.write(chunk)
        self.policy_document = policy_path
        self.save(update_fields=['policy_document'], read_only=False)

        if not self.policy:
            try:
                self.populate_policy()
            except:
                utils.log_error(logger, msg='Unable to populate health policy')
        self.track_activity(
            activity_type=core_activity.SUCCESS,
            page_id='policy'
        )

        policy_updated.send(sender=self._meta.model, transaction=self, send_mail=send_mail)

    def track_activity(self, activity_type, page_id, request=None, requirement=None):
        try:
            act_prod = core_models.RequirementKind.HEALTH

            kwargs = {
                'activity_type': activity_type,
                'product': act_prod,
                'page_id': page_id,
                'form_name': '',
                'form_variant': '',
                'form_position': '',
                'data': self.get_form_data(),
                'visitor': self.tracker,
                'extra': {
                    'transaction_id': self.transaction_id,
                },
                'requirement': requirement
            }
            if request:
                kwargs["request_dict"] = request

            act = core_activity.create_activity(**kwargs)

            has_act_requirement = False
            if act and act.requirement:
                has_act_requirement = True

            if (not self.requirement and has_act_requirement) or (
                (self.requirement and has_act_requirement) and
                (self.requirement.pk is not act.requirement.pk)
            ):
                self.requirement = act.requirement
                self.save(update_fields=['requirement'])
        except:
            utils.log_error(logger, request, 'Unable to track motor activity')
        else:
            return act

    def populate_policy(self):
        from account_manager.models import UserTransaction
        user = UserTransaction.objects.get_for_transaction(self).user
        if not user:
            logger.error("No user found in UserTransaction for transaction_id(%s)" % self.transaction_id)
        # Generate form data.
        self.track_activity(
            core_activity.SUCCESS, 'policy'
        )

        act = core_activity.create_activity(
            activity_type=core_activity.SUCCESS,
            product=RequirementKind.HEALTH,
            page_id='policy',
            form_name='',
            form_position='',
            form_variant='',
            user=user,
            visitor=self.tracker,
        )
        start_date = utils.convert_datetime_to_timezone(self.policy_start_date)
        end_date = utils.convert_datetime_to_timezone(self.policy_end_date)
        policy = Policy.objects.create(
            name=os.path.splitext(self.policy_document.name)[0].split(os.path.sep)[-1],
            document=self.policy_document,
            app=self._meta.app_label,
            status=Policy.VALID,
            user=user,
            requirement_sold=act.requirement,
            policy_number=self.policy_number,
            premium=self.premium_paid or None,
            issue_date=start_date,
            expiry_date=end_date,
        )
        act.requirement.policy = policy
        act.requirement.save(update_fields=['policy'])
        self.policy = policy
        self.save(update_fields=['policy'], read_only=False)

    @property
    def transaction_mode(self):
        return 'online'

    @property
    def transaction_type(self):
        return 'health'

    @property
    def last_status(self):
        return self.status_history[-1] if self.status_history else None

    def save(self, read_only=True, *args, **kwargs):
        if self.last_status == "COMPLETED" and read_only:
            return
            #TODO raise ReadOnlyException()
        if self.status != self.last_status:
            self.status_history += (self.status, )

        super(Transaction, self).save(*args, **kwargs)

    def open_requirement_if_deleted(self):
        is_requirement_with_state = self.requirement and self.requirement.current_state
        if is_requirement_with_state and self.requirement.current_state.pk == 17:
            # Change state to previous state.
            self.requirement.reopen(previous_state=True)

    def mark_complete(self, status='COMPLETED', user=None):
        pre_self = self._default_manager.get(id=self.id)
        if pre_self.status in ['COMPLETED', 'MANUAL COMPLETED']:
            raise ValueError('Transaction is already marked complete')

        if status not in ['COMPLETED', 'MANUAL COMPLETED']:
            raise ValueError(u'{} is not termed as completed status'.format(status))

        user = user or AnonymousUser()
        self.status = status
        if status == 'COMPLETED':
            self.payment_on = self.payment_on or timezone.now()
        self.save()
        create_user_account.send(sender=self._meta.model, transaction=self, user=user)

    def get_form_data(self):
        from . import form_data
        from .forms import HealthFDForm
        data = form_data.generate_form_data(self)
        return HealthFDForm(data=data).get_fd()

    def mini_json(self):
        return {
            'id': self.id,
            'transaction_id': self.transaction_id,
            'payment_date': self.payment_on,
            'mobile': self.mobile,
        }

    @property
    def policy_tat(self):
        INSURER_POLICY_TAT = {
            'universalsompo': 15,
            'the-oriental-insurance-company-ltd': 30,
            'cigna-ttk': 15,    # was not on list
            'iffco-tokio': 15,
            'hdfc-ergo': 15,
            'cholamandalam-ms': 15,     # was not on list
            'landt-general-insurance': 15,      # was not on list
            'reliance-health-insurance': 15,
            'bharti-axa': 15,
            'the-new-india-assurance-co-ltd': 15,
            'tata-aig': 15,
            'religare': 15,     # was not on list
            'max-bupa': 15,     # was not on list
            'bajaj-allianz': 120,
            'icici-lombard': 15,
            'star-health-insurance': 15,        # was not on list
            'apollo-munich-insurance-company': 15,      # was not on list
        }
        return int(INSURER_POLICY_TAT.get(self.slab.health_product.insurer.slug, 24 * 60))


class Order(models.Model):
    # If a transaction fails, we can re-initiate the order with a new Order
    # model
    status = models.CharField(
        max_length=50, choices=TRANSACTION_STATUS_CHOICES)
    transaction = models.ForeignKey('Transaction')
    amount = models.FloatField(blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    redirect_url = models.CharField(max_length=255)
    extra = JSONField(_('extra'), blank=True, default={})


class PincodeMapping(models.Model):
    pin = models.CharField(max_length=100)
    city_detail = JSONField(_('city_details'), blank=True, default={})
    area_detail = JSONField(_('area_details'), blank=True, default={})

    def __str__(self):
        return "%s" % (self.pin)


class CallAlert(models.Model):
    tracker = models.ForeignKey(Tracker)
    transaction = models.ForeignKey(Transaction, blank=True, null=True)
    phone_number = models.CharField(max_length=15)
    via = models.CharField(max_length=150, blank=True)
    remarks = models.TextField(blank=True)
    extra = JSONField(_('extra'), blank=True, default={})
    is_active = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s" % (self.phone_number)
