import datetime
import json
import urlparse
from collections import defaultdict
from importlib import import_module

from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.core import serializers
from django.core.urlresolvers import reverse
from django.db import connection
from django.template import Context, loader
from lxml import etree

import cf_cns
import health_product.insurer.settings as insurer_settings
import putils
import utils
from core import activity as core_activity
from mail_utils import health_transaction_alert
from wiki import models as wiki_models


def dict_to_etree(root, request_dict):
    for key, value in request_dict.items():
        node = etree.Element(key)
        if isinstance(value, dict):
            elem = dict_to_etree(node, value)
        elif isinstance(value, list):
            # print "TAG: ", node.tag
            elem = list_to_etree(node, value)
        else:
            if value is None:
                node.text = ''
            else:
                node.text = str(value)
        root.append(node)
    return root


def list_to_etree(element, request_list):
    # print "=========================================="
    #node = etree.Element(element.tag)
    node = element
    # print "NODE TAG: ", node.tag
    for index, item in enumerate(request_list):
        if type(item) in [list, dict]:
            elem = dict_to_etree(node, item)
        else:
            elem = etree.Element(str(item))
        # print "ELE TAG: ", elem.tag
        # element.append(elem)
    return element


def complex_etree_to_dict(t):
    d = {t.tag: {} if t.attrib else None}
    children = list(t)
    if children:
        dd = defaultdict(list)
        for dc in map(complex_etree_to_dict, children):
            for k, v in dc.iteritems():
                dd[k].append(v)
        d = {t.tag: {k: v[0] if len(v) == 1 else v for k, v in dd.iteritems()}}
    if t.attrib:
        d[t.tag].update(('@' + k, v) for k, v in t.attrib.iteritems())
    if t.text:
        text = t.text.strip()
        if children or t.attrib:
            if text:
                d[t.tag]['#text'] = text
        else:
            d[t.tag] = text
    return d


def data2xml(d, name):
    r = etree.Element(name)
    return buildxml(r, d)


def buildxml(r, d):
    if isinstance(d, dict):
        for k, v in d.iteritems():
            s = etree.SubElement(r, k)
            buildxml(s, v)
    elif isinstance(d, list):
        grandparent = r.getparent()
        tag = r.tag
        grandparent.remove(r)
        for v in d:
            s = etree.SubElement(grandparent, tag)
            buildxml(s, v)
    elif isinstance(d, basestring):
        r.text = d
    else:
        r.text = str(d)
    return r

def render_question(question):
    try:
        t = loader.get_template(
            'health_product/question-%s-snippet.html' % (question.question_type))
    except:
        return
    # if question.options.all():
    #    ol = json.loads(question.options.all())
    # else:
    #    ol = []
    c = Context({
        'q': question,
        #'options':ol,
    })
    q_form = t.render(c)
    return q_form


def send_lms_transaction_event(transaction, event, **kwargs):
    request = utils.get_request()
    data = json.loads(serializers.serialize(
        "json", [transaction]))[0]['fields']

    slab = transaction.slab
    email = (kwargs.pop('email', '') or
             transaction.proposer_email or
             request.COOKIES.get('email'))
    mobile = (kwargs.pop('mobile', '') or
              transaction.proposer_mobile or
              request.COOKIES.get('mobileNo'))
    name = '%s %s' % (transaction.proposer_first_name, transaction.proposer_last_name)
    premium = (transaction.premium_paid or
               transaction.extra.get('plan_data', {}).get('premium', ''))
    campaign = ('health-topup'
                if slab.health_product.coverage_category.lower() in ['topup', 'super_topup']
                else 'Health')
    sum_assured = 'Rs.%s' % slab.sum_assured
    if slab.deductible:
        sum_assured += ' (Deductible: Rs.%s)' % transaction.slab.deductible

    if transaction.policy_document:
        data['policy_document'] = transaction.policy_document.url

    if request.user.is_authenticated():
        data['logged_in_user'] = request.user.email
    if kwargs.get('error_message'):
        data['error_response'] = kwargs.pop('error_message')

    data.update({
        'tracker_id': request.TRACKER.session_key,
        'name': name,
        'insurer': slab.health_product.insurer.title,
        'sum_assured': sum_assured,
        'premium': premium,
        'campaign': campaign,
        'transaction_link': '%s/health-plan/buy/%s' % (settings.SITE_URL, transaction.transaction_id),
        'payment_on': utils.LMSUtils.convert_datetime(transaction.payment_on),
    })
    data.update(kwargs)
    data.update(insurer_settings.get_product_details(slab))

    lms_data = {
        'event': event,
        'product_type': 'health',
        'tracker': request.TRACKER,
        'data': data,
        'email': email,
        'mobile': mobile,
        'internal': request.IS_INTERNAL,
        'request_ip': putils.get_request_ip(request)
    }

    utils.LMSUtils.send_lms_event(**lms_data)


def extract_transaction_data(transaction):
    from health_product.models import Transaction
    temp_tuple = []
    data_row_list = []
    exclusion_list = ['extra', 'insured_details_json', 'mailer_flag', 'payment_request', 'payment_response',
                      'policy_request', 'policy_response', 'tracker', 'payment_done', 'policy_document']
    for f in transaction._meta.fields:
        if f.name not in exclusion_list:
            try:
                temp_tuple.append(
                    getattr(transaction, 'get_%s_display' % f.name)())
            except AttributeError:
                temp_tuple.append(getattr(transaction, f.name))
    insured_data = ''
    temp_tuple.append(json.dumps(
        transaction.extra.get('product_response', '')))
    if 'product_response' in transaction.extra.keys():
        for k, v in transaction.extra['product_response'].items():
            if type(v) in [dict, list]:
                temp_tuple.append(json.dumps(v))
            else:
                temp_tuple.append(v)
    if 'payment_response' in transaction.extra.keys():
        for k, v in transaction.extra['payment_response'].items():
            if type(v) in [dict, list]:
                temp_tuple.append(json.dumps(v))
            else:
                temp_tuple.append(v)
    # for ins in transaction.insured_details_json['insuredArray']:
    #     for k,v in ins.items():
    #         if not k.lower().find('option') >= 0 and k != 'jokerArray':
    #             insured_data = '%s\n%s - %s' % (insured_data, k, v)
    # temp_tuple.append(insured_data)

    data_row_list.append(temp_tuple)

    a = Transaction.objects.all()[0]
    order = [f.name for f in a._meta.fields if f.name not in exclusion_list]
    order.append('Product Response')

    if 'product_response' in transaction.extra.keys():
        for k in transaction.extra['product_response'].keys():
            order.append(k)
    if 'payment_response' in transaction.extra.keys():
        for k in transaction.extra['payment_response'].keys():
            order.append(k)
    # order.append('Insured Member Data')
    return data_row_list, order


def process_payment_backend(transaction):
    from health_product.insurer import settings as insurer_settings
    from importlib import import_module
    insurer_module = insurer_settings.INSURER_MAP[
        transaction.slab.health_product.insurer.id]
    m = import_module("health_product.insurer.%s.integration" % insurer_module)

    is_redirect, redirect_url, template, data = m.post_transaction(transaction)
    return is_redirect, redirect_url, template, data


def process_payment_response(insurer_id, response, transaction=None):
    from health_product.insurer import settings as insurer_settings
    from importlib import import_module
    insurer_module = insurer_settings.INSURER_MAP[int(insurer_id)]
    m = import_module("health_product.insurer.%s.integration" % insurer_module)

    is_redirect, redirect_url, template, data = m.post_payment(
        response, transaction)
    return is_redirect, redirect_url, template, data


def process_formdata_backend(transaction):
    from health_product.insurer import settings as insurer_settings
    from importlib import import_module
    insurer_module = insurer_settings.INSURER_MAP[
        transaction.slab.health_product.insurer.id]
    m = import_module("health_product.insurer.%s.form_data" % insurer_module)
    data = m.get_dynamic_form(transaction)
    return data


def process_transaction_download(transaction):
    from health_product.insurer import settings as insurer_settings
    from importlib import import_module
    insurer_module = insurer_settings.REPORT_MAP.get(
        transaction.slab.health_product.insurer.id, 'generic')
    m = import_module("health_product.insurer.%s.report" % insurer_module)
    data = m.download_data(transaction)
    return data


def process_save_transaction(transaction):
    from health_product.insurer import settings as insurer_settings
    from importlib import import_module
    insurer_module = insurer_settings.INSURER_SAVE_MAP.get(
        transaction.slab.health_product.insurer.id, 'generic')
    m = import_module("health_product.insurer.%s.spec_utils" % insurer_module)
    data = m.save_transaction(transaction)
    return data


def save_stage(transaction, stage_data):
    from health_product.insurer import settings as insurer_settings
    from importlib import import_module

    insurer_module = 'generic'
    if stage_data['template'] in insurer_settings.STAGE_SAVE_MAP.keys():
        for k, v in insurer_settings.STAGE_SAVE_MAP[stage_data['template']].items():
            if transaction.slab.health_product.id in k:
                insurer_module = v
    m = import_module("health_product.insurer.%s.spec_utils" % insurer_module)
    transaction = m.save_stage(transaction, stage_data)

    return transaction


def transaction_successful(transaction, request):
    if transaction.status != 'COMPLETED':
        transaction.payment_done = True
        transaction.status = 'COMPLETED'
        slab = transaction.slab
        if slab.health_product_id in insurer_settings.INSURER_SPECIFIC_SCORE:
            hp_score = import_module('health_product.insurer.%s.score' % insurer_settings.INSURER_MAP[
                                     slab.health_product.insurer_id])
            data = transaction.extra['cpt_data'].copy()
            data['city'] = wiki_models.City.objects.get(id=data['city'])
            cpt = hp_score.getCoverPremiumTable(slab, data)
        else:
            if slab.health_product.policy_type == "FAMILY":
                cpt = wiki_models.FamilyCoverPremiumTable.objects.get(
                    id=transaction.extra['cpt_id'])
            else:
                cpt = wiki_models.CoverPremiumTable.objects.get(
                    id=transaction.extra['cpt_id'])
        try:
            transaction.premium_paid = float(transaction.premium_paid)
            if not transaction.premium_paid:
                transaction.premium_paid = float(cpt.premium)
        except ValueError:
            transaction.premium_paid = float(cpt.premium)
            pass
        transaction.save()

        # Sending custom signal on successful payment
        from account_manager.signals.signals import online_transaction_completed
        online_transaction_completed.send(sender=transaction.__class__, transaction=transaction, user=request.user)

        obj = {
            'user_id': transaction.transaction_id,
            'to_email': transaction.proposer_email,
            'name': utils.clean_name(transaction.proposer_first_name),
            'tag': 'transaction-success',
            'insurer': slab.health_product.insurer,
            'product': slab.health_product,
            'cover': slab.sum_assured,
            'transaction_number': 'CFOX-%s-%s' % (slab.health_product.id, transaction.id),
            'policy_document_url': transaction.policy_document.url if transaction.policy_document else '',
            'manage_policies_url': urlparse.urljoin(settings.SITE_URL, reverse('account_manager:manage_policies')),
        }
        if transaction.premium_paid:
            obj['premium'] = transaction.premium_paid
        else:
            obj['premium'] = transaction.insured_details_json['premium']
        cf_cns.notify('TRANSACTION_SUCCESS_MAIL', to=(obj['name'], obj['to_email']),
                      obj=obj, mandrill_template='newbase')

        try:
            health_transaction_alert(transaction, 'Payment success')

            ## START: SEPARATE MAIL FOR ORIENTAL ##
            if slab.health_product.insurer_id == 16:
                ins_utils = import_module('health_product.insurer.%s.ins_utils' % insurer_settings.INSURER_MAP[
                                          slab.health_product.insurer_id])
                ins_utils.transaction_successful(transaction)
            ## END: SEPARATE MAIL FOR ORIENTAL ##

        except Exception as e:
            pass
            # print str(e.message)

    return


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def calculate_age(birth):
    try:
        if type(birth) is str:
            birth = datetime.datetime.strptime(birth, '%d/%m/%Y')

        today = datetime.datetime.today()
        if today > birth:
            age = relativedelta(today, birth)
            return {'years': age.years, 'months': age.months}
        else:
            raise Exception(
                "Wow!! you must be a time traveller. Well, FUCK YOU!")
    except Exception as e:
        pass
        # print e


def get_db_sequence(sequence_name):
    cursor = connection.cursor()
    cursor.execute("SELECT nextval('%s');" % (sequence_name))
    new_num = int(cursor.fetchone()[0])
    return new_num


def check_insured_age(transaction):
    insured_data = transaction.insured_details_json['form_data']['insured']
    for tag, data in insured_data.items():
        birth_date = str(data['date_of_birth']['value'])
        age_details = calculate_age(birth_date)
        age_in_years = age_details['years']
        for person in transaction.extra['members']:
            if person['id'] == tag:
                if person['age'] != age_in_years:
                    person['age'] = age_in_years
                if person['age_in_months'] != age_details['months']:
                    person['age_in_months'] = age_details['months']
                transaction.save()
    return transaction
