import os
from django.db.models import Count, Q
from wiki.models import Hospital, Pincode, City, ContentType, MedicalCondition, MetaData
from health_product.health_utils import GetSumAssured
from health_product.decorators import cache_memoized_func
from django.db import connection
from operator import attrgetter
from mail_utils import health_alert
import StringIO
import ConfigParser


def get_city_by_hospital_extrapolation(pincode_value):
    newpin, near_by_pincodes = get_nearest_pincode(pincode_value)
    hcc_list = []
    for pobj in near_by_pincodes:
        pin = pobj.pincode
        hospitals = list(Hospital.objects.filter(pin=pin).annotate(
            insurers_count=Count('insurers')).values())
        if len(hcc_list) > 20:
            break
        else:
            hcc_list.extend(hospitals)
            continue

    pinobj = Pincode.objects.filter(pincode=pincode_value)[0]
    if hcc_list:
        # Finding best matched city
        state = pinobj.state
        cities = list(
            City.objects.filter(
                id__in=[
                    hcc['city_id']
                    for hcc in hcc_list
                    if hcc['pin'] == pincode_value
                ],
                state=state)
        )
        if not cities:
            cities = list(City.objects.filter(
                id__in=[hcc['city_id'] for hcc in hcc_list], state=state))
        if not cities:
            cities = list(City.objects.filter(
                id__in=[hcc['city_id'] for hcc in hcc_list]))
            if cities:
                health_alert('wrong pincode details', u'{}'.format(pinobj.pincode))

        if cities:
            city = max(set(cities), key=cities.count)
        else:
            city = pinobj.mapped_city if pinobj.mapped_city else pinobj.state.capital

    else:
        city = pinobj.mapped_city if pinobj.mapped_city else pinobj.state.capital

    return {
        'name': city.name,
        'id': city.id,
        'type': (
            'METRO'
            if Pincode.objects.filter(pincode=pincode_value, is_metro=True)
            else ""
        ),
        'lat': newpin.lat,
        'lng': newpin.lng,
    }


@cache_memoized_func
def get_nearest_pincode(pincode, distance=200, limit=500):
    """
    Fetch nearest *unique* pincodes within radius of specified distance (in kms)
    New pin code is returned if given pincode doesn't have lat/lng information
    """
    pin = Pincode.objects.filter(pincode=pincode)[0]

    # Hack to find nearest pincode with lat lng if given pincode has no lat
    # lng information
    if not pin.lat:
        if connection.vendor == "mysql":
            query = 'SELECT pincode, id, ABS(pincode-%s) AS nearness FROM wiki_pincode WHERE lat IS NOT NULL ORDER BY nearness ASC LIMIT 1'
        else:
            query = 'SELECT pincode, id FROM wiki_pincode WHERE lat IS NOT NULL ORDER BY ABS(pincode-%s) ASC LIMIT 1'
        nearest_pin_with_lat_lng = Pincode.objects.raw(query, [pin.pincode])
        pin = nearest_pin_with_lat_lng[0]

    ## do we need mysql now??
    if connection.vendor == "mysql":
        # convert distance to miles
        distance = distance / 1.60934
        pcodes = Pincode.objects.raw('SELECT pincode, id, lat, lng, city, region, ((ACOS(SIN(%s * PI() / 180) * SIN(lat * PI() / 180) + COS(%s * PI() / 180) * COS(lat * PI() / 180) * COS((%s - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS distance FROM wiki_pincode WHERE state_id="%s" GROUP BY pincode HAVING distance<=%s ORDER BY distance ASC LIMIT %s', [
                                     pin.lat, pin.lat, pin.lng, pin.state.id, distance, limit])
    elif connection.vendor == "postgresql":
        # convert distance to metres
        distance = distance * 1000
        pcodes = Pincode.objects.raw(
            'SELECT min(id) as id, pincode, min(distance) FROM (SELECT id, pincode, lat, lng, earth_distance(ll_to_earth(lat, lng), ll_to_earth({}, {}))  distance FROM wiki_pincode WHERE state_id={}) vt GROUP BY pincode HAVING min(distance)<={} ORDER BY min(distance) LIMIT {}'.format(pin.lat, pin.lng, pin.state.id, distance, limit))
    else:
        # convert distance to miles
        distance = distance / 1.60934
        dist_formula = "((ACOS(SIN(%s * PI() / 180) * SIN(lat * PI() / 180) + COS(%s * PI() / 180) * COS(lat * PI() / 180) * COS((%s - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515)"
        query = "SELECT pincode, id, {dist_formula} AS distance FROM wiki_pincode GROUP BY pincode, id HAVING {dist_formula} <= %s ORDER BY distance ASC LIMIT {limit}".format(
            dist_formula=dist_formula, limit=limit)
        query = "SELECT pincode, id, min(distance) FROM (SELECT id, pincode, {dist_formula} AS distance FROM wiki_pincode) vt GROUP BY pincode, id HAVING min(distance) <= %s ORDER BY min(distance) ASC LIMIT 500".format(
            dist_formula=dist_formula)
        pcodes = Pincode.objects.raw(
            query, [pin.lat, pin.lat, pin.lng, distance])

    return pin, pcodes


@cache_memoized_func
def prepare_post_data_to_score(data):

    try:
        if data['hospital_service'] == "BASIC":
            service_type = "SHARED"
        elif data['hospital_service'] == "PRIVATE_HIGH_END":
            service_type = "PVT_HIGH_END"
        else:   # if data['hospital_service'] == "PRIVATE_ANY":
            service_type = "PVT"
    except KeyError:
        service_type = 'PVT'

    spouse_age = None
    kid_ages = []
    parent_ages = []
    you_and_spouse_ages = []

    kid_genders = []
    parent_genders = []
    you_and_spouse_genders = []

    self_age = None
    self_gender = None

    for m in data['members']:
        m['person'] = 'you' if m['person'].lower() == 'self' else m['person']

    if len(data['members']) == 1:
        m = data['members'][0]
        family = False
        kids = 0
        adults = 1
        parents = 0
        you_and_spouse = 0
        is_self_insured = False
        max_age = m['age']
        self_age = m['age']
        if m['person'].lower() in ['you', 'self']:
            gender = data['gender']
            is_self_insured = True
            if m['person'] == 'you':
                self_gender = gender
        if m['person'].lower() == 'spouse':
            if data['gender'] == 'MALE':
                gender = 'FEMALE'
            else:
                gender = 'MALE'
        if m['person'].lower() in ['son', 'father']:
            gender = 'MALE'
        if m['person'].lower() in ['daughter', 'mother']:
            gender = 'FEMALE'

    else:
        family = True
        kids = 0
        adults = 0
        parents = 0
        you_and_spouse = 0
        is_self_insured = False
        max_age = 0
        for m in data['members']:
            m_person = m['person'].lower()
            age = int(m['age'])
            age_in_months = int(m.get('age_in_months', 0) or 0)

            # Find the current member's gender
            if m_person == 'you':
                m_gender = data['gender']
                self_gender = m_gender
                is_self_insured = True
            elif m_person == 'spouse':
                if data['gender'] == 'MALE':
                    m_gender = 'FEMALE'
                else:
                    m_gender = 'MALE'
            elif m_person in ['son', 'father']:
                m_gender = 'MALE'
            elif m_person in ['daughter', 'mother']:
                m_gender = 'FEMALE'

            # Treat eldest member as primary member
            if max_age < age:
                max_age = age
                gender = m_gender

            # Count and assign members in each generation their ages, genders
            if m_person in ['son', 'daughter']:
                kid_ages.append(age or age_in_months / 1000.0)
                kid_genders.append(m_gender)
                kids += 1
            elif m_person in ['father', 'mother']:
                parents += 1
                adults += 1
                parent_ages.append(age)
                parent_genders.append(m_gender)
            elif m_person in ['you', 'spouse']:
                adults += 1
                you_and_spouse += 1
                you_and_spouse_ages.append(age)
                you_and_spouse_genders.append(m_gender)
                if m_person == 'you':
                    self_age = age
                else:
                    spouse_age = age

    mdata = {
        'kids': kids,
        'adults': adults,
        'parents': parents,
        'parent_ages': parent_ages,
        'parent_genders': parent_genders,
        'you_and_spouse': you_and_spouse,
        'you_and_spouse_ages': you_and_spouse_ages,
        'you_and_spouse_genders': you_and_spouse_genders,
        'is_self_insured': is_self_insured,
        'family': family,
        'gender': gender,
        'age': max_age,
        'self_age': self_age,
        'self_gender': self_gender,
        'spouse_age': spouse_age,
        'sum_assured': data.get('sum_assured', ''),
        'deductible': data.get('deductible', ''),
        'kid_ages': kid_ages,
        'kid_genders': kid_genders,
        'service_type': service_type,
        'pincode': data['pincode'],
        'city': data.get('city'),
    }

    if not mdata.get('city'):
        mdata['city'] = get_pincode_city_details(mdata['pincode'])

    mdata['hospitals'] = []
    if not mdata.get('sum_assured'):
        mdata['sum_assured'] = GetSumAssured(mdata).get_sa()

    return mdata


def get_pincode_city_details(pincode):
    pinobj = Pincode.objects.filter(pincode=pincode)[0]
    pin_city = City.objects.filter(name=pinobj.city, state=pinobj.state).first()
    if pin_city:
        city = pin_city
    else:
        city = pinobj.mapped_city if pinobj.mapped_city else pinobj.state.capital
    return {
        'name': city.name,
        'id': city.id,
        'type': (
            'METRO'
            if pinobj.is_metro is True
            else ""
        ),
        'lat': pinobj.lat,
        'lng': pinobj.lng,
    }


@cache_memoized_func
def get_city_and_hospitals(pincode_value):
    newpin, near_by_pincodes = get_nearest_pincode(pincode_value)
    hcc_list = []
    for pobj in near_by_pincodes:
        pin = pobj.pincode
        q = Q(pin=pin)
        hospitals = list(Hospital.objects.filter(q).prefetch_related(
            'insurers').annotate(insurers_count=Count('insurers')))
        if len(hcc_list) > 20:
            break
        else:
            hcc_list.extend(hospitals)
            continue

    if hcc_list:
        hccs = [
            {
                'name': "%s" % hcc.name,
                'address': hcc.address,
                'lat': hcc.lat,
                'lng': hcc.lng,
                'id': hcc.id,
                'insurers': [ins.id for ins in hcc.insurers.all()]
            }
            for hcc in reversed(
                sorted(hcc_list, key=attrgetter('insurers_count'))
            )
        ]

        # Finding best matched city
        pinobj = Pincode.objects.filter(pincode=pincode_value)[0]
        state = pinobj.state
        cities = list(
            City.objects.filter(
                id__in=[
                    hcc.city_id
                    for hcc in hcc_list
                    if hcc.pin == pincode_value
                ],
                state=state
            )
        )
        if not cities:
            cities = list(City.objects.filter(
                id__in=[hcc.city_id for hcc in hcc_list], state=state))

        if not cities:
            cities = list(City.objects.filter(
                id__in=[hcc.city_id for hcc in hcc_list]))
            if cities:
                health_alert('wrong pincode details', u'{}'.format(pinobj.pincode))

        if cities:
            city = max(set(cities), key=cities.count)
        else:
            city = pinobj.mapped_city if pinobj.mapped_city else pinobj.state.capital

    else:
        pinobj = Pincode.objects.filter(pincode=pincode_value)[0]
        city = pinobj.mapped_city if pinobj.mapped_city else pinobj.state.capital
        hccs = []

    return {
        'hospitals': hccs,
        'city': {
            'name': city.name,
            'id': city.id,
            'type': (
                'METRO' if Pincode.objects.filter(
                    pincode=pincode_value, is_metro=True
                )
                else ""
            ),
            'lat': newpin.lat,
            'lng': newpin.lng,
        }
    }


def get_medical_conditions():
    medical_conditions = MedicalCondition.objects.all()
    data = [{'id': med.id,
             'medical_condition': med.medical_condition,
             'permission': not med.global_decline,
             'frequency': med.frequency,
             }
            for med in medical_conditions
            ]
    return data

## not used anywhere
def get_city_hospital_method(pincode):
    newpin, near_by_pincodes = get_nearest_pincode(pincode.pincode)

    hcc_list = []
    for pobj in near_by_pincodes:
        hospitals = list(Hospital.objects.filter(pin=pobj.pincode).prefetch_related('city'))
        if len(hcc_list) > 20:
            break
        else:
            hcc_list.extend(hospitals)
            continue

    cities = [h.city for h in hcc_list if h.city.state_id == pincode.state_id and h.pin == pincode.pincode]
    if not cities:
        cities = [h.city for h in hcc_list if h.city.state_id == pincode.state_id]

    if cities:
        return max(cities, key=cities.count)

@cache_memoized_func
def prepare_multiset_post_data(data):
    """ data sample: {
        u'hospital_service': u'private_any',
        u'pincode': 400053,
        u'parents_pincode': 400051,
        u'members': [
            {
                u'dob': u'01-01-2015',
                u'person': u'you',
                u'verbose': u'you'
            },
            {
                u'dob': u'04-04-2012',
                u'person': u'daughter',
                u'id': u'daughter1',
                u'verbose': u'daughter'
            },
            {
                u'dob': u'17-02-2000',
                u'person': u'son',
                u'id': u'son2',
                u'verbose': u'son'
            }
        ]
    }

    this method converts the above into the multiple sets keeping the same
    structure intact
    """
    parents = []
    family = []

    results_set = []
    for m in data['members']:
        if m['person'].lower() in ['you', 'spouse', 'son', 'daughter']:
            family.append(m)
        if m['person'].lower() in ['father', 'mother']:
            parents.append(m)

    if len(parents) > 0 and len(family) > 0:
        parents_set = data.copy()
        parents_set['members'] = parents
        parents_set['pincode'] = data.get('parents_pincode', data['pincode'])
        parents_data_to_score = prepare_post_data_to_score(parents_set)
        parents_set['metro'] = parents_data_to_score['city']['type']
        parents_set['city'] = parents_data_to_score['city']
        parents_set['sum_assured'] = parents_data_to_score['sum_assured']
        results_set.append({'type': 'parents', 'uidata': parents_set,
                            'data_to_score': parents_data_to_score})

        family_set = data.copy()
        family_set['members'] = family
        family_data_to_score = prepare_post_data_to_score(family_set)
        family_set['metro'] = family_data_to_score['city']['type']
        family_set['city'] = family_data_to_score['city']
        family_set['sum_assured'] = family_data_to_score['sum_assured']
        results_set.append(
            {
                'type': 'family',
                'uidata': family_set,
                'data_to_score': family_data_to_score
            }
        )

    combined_set = data.copy()
    combined_set['pincode'] = data['pincode'] if len(family) > 0 else data[
        'parents_pincode']
    combined_data_to_score = prepare_post_data_to_score(combined_set)
    combined_set['metro'] = combined_data_to_score['city']['type']
    combined_set['city'] = combined_data_to_score['city']
    combined_set['sum_assured'] = combined_data_to_score['sum_assured']
    results_set.append(
        {
            'type': 'combined',
            'uidata': combined_set,
            'data_to_score': combined_data_to_score
        }
    )

    return results_set


@cache_memoized_func
def update_text_to_display(slab_score):
    md_dict = dict(
        ((sl, md.content_type.model), md)
        for md in MetaData.objects.filter(
            slabs__in=slab_score.keys()
        ).distinct().select_related('content_type__model').prefetch_related('slabs')
        for sl in md.slabs.all()
    )

    for slab, slab_data in slab_score.items():
        for attr, attr_data in slab_data:
            instance = attr_data.get('model')
            if instance:
                md = md_dict.get(
                    (
                        slab,
                        instance.__class__.__name__.lower()
                    )
                )
                if md and md.text_to_display:
                    config = StringIO.StringIO()
                    config.write('[dummysection]\n')
                    config.write(md.text_to_display)
                    config.seek(0, os.SEEK_SET)
                    cp = ConfigParser.ConfigParser()
                    cp.readfp(config)
                    try:
                        attr_data['verbose'] = cp.get('dummysection', 'value')
                    except ConfigParser.NoOptionError:
                        pass
                    try:
                        attr_data['verbose'] = cp.get(
                            'dummysection', 'verbose')
                    except ConfigParser.NoOptionError:
                        pass
                    finally:
                        config.close()