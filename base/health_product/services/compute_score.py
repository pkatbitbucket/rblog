from django.db.models import Q

from health_product.decorators import CacheAndStaticClass
import health_product.health_utils as hutils
from wiki.models import ZonalRoomRent


class ComputeScore(object):
    """
    For all methods defined in this class:
    @:parameter MatchPlan object, Slab object, MatchPlan data.
    @:returns {'score': integer value,
               'verbose': 'full description of score',
               'value': 'one liner description',
               'available': True/False(boolean),
               'importance': 'LOW/MID/NA/TOP'
              }
    IMPORTANT: All methods in this class are static and decorated with cache_memorized_method by default.
    """
    __metaclass__ = CacheAndStaticClass

    def score_maternity_cover(match_plan_obj, slab, data):
        obj = slab.maternity_cover
        importance = 'LOW'
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': importance}
        if not (data['age'] and data.get('spouse_age')):
            result.update({'importance': 'NA'})
            return result
        if not obj.is_covered:
            verbose = "This plan does not cover maternity expenses. Most plans which cover maternity expenses do not " \
                      "pay more than Rs.30,000 - Rs.50,000, that too after 3-4 years"
            value = "Not Covered"
            fscore = 0
            available = False

        else:
            available = True
            # ascore : Score of waiting period
            if 49 <= obj.waiting_period:
                ascore = 1
            elif 37 <= obj.waiting_period <= 48:
                ascore = 2
            elif 25 <= obj.waiting_period <= 36:
                ascore = 3
            elif 13 <= obj.waiting_period <= 24:
                ascore = 4
            else:
                ascore = 5

            verbose = "Maternity expenses can only be claimed after %s years" % int(
                (obj.waiting_period / 12))
            value = "Covered after %s years" % int(obj.waiting_period / 12)

            if obj.limit_for_cesarean_delivery == obj.limit_for_normal_delivery:
                verbose += "The maximum you can claim as maternity expenses is Rs %s" % hutils.currency_format(
                    obj.limit_for_normal_delivery)
            else:
                if obj.limit_for_cesarean_delivery:
                    verbose += "The maximum you can claim for a cesarean delivery is Rs %s/-." % hutils.currency_format(
                        obj.limit_for_cesarean_delivery)
                if obj.limit_for_normal_delivery:
                    verbose += "The maximum you can claim for a normal delivery is Rs %s/-." % hutils.currency_format(
                        obj.limit_for_normal_delivery)

            # bscore : Score of coverage amount
            bscore = 100 * (min(obj.limit_for_cesarean_delivery /
                                match_plan_obj.CAESAREAN_DELIVERY_LIMIT, 1))
            fscore = (ascore * 20 + bscore) / 2
        if data['kids'] == 0 and data['age'] <= 35:
            importance = 'MID'
        result.update({
            'score': fscore,
            'verbose': verbose,
            'value': value,
            'available': available,
            'importance': importance
        })
        return result

    def score_life_long_renewability(match_plan_obj, slab, data):
        obj = slab.life_long_renewability
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'NA'}

        if obj.condition == 'YES':
            value = "Lifelong"
            available = True
            if data['age'] > 45:
                verbose = "Some plans can not be renewed after a certain age. At the age of %s its extremely critical " \
                          "to select a plan which provides coverage for as long as possible. Buying a new health plan " \
                          "after the age of 45 can be very costly. This plan provides you lifelong renewability" % data[
                              'age']
            else:
                verbose = "Some plans cannot be renewed after a certain age, which is a problem when you grow older. This plan can be renewed lifelong."
            result.update({'score': 100, 'value': value, 'verbose': verbose, 'available': available})
            return result

        else:
            available = False
            verbose = "This plan is renewable upto the age of %s years." % obj.age_limit
        # Score increases as you approach the age_limit linearly from 100 to 0,
        # starts when you are 30 years away from the max_age
        score = 100 * (30 - min(30, obj.age_limit - data['age'])) / float(30)
        value = "upto %s years of age" % (obj.age_limit)
        result.update({'score': score, 'verbose': verbose,
                       'value': value, 'available': available})
        return result

    def score_bonus(match_plan_obj, slab, data):
        """
        #TODO: Add flag for cummulative Vs on-base and score accordingly
        #TODO: headline on UI "Most companies increase your coverage every year when you do not claim. This is important as medical costs keep increasing year on year"
        """
        obj = slab.bonus
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'HIGH'}

        verbose = "If you do not make any claim in a year, some plans reward you with extra cover in the next year. This extra cover is called 'No Claim Bonus'."
        if not obj.is_available:
            available = False
            verbose += " This plan does not provide No Claim Bonus."
            result.update({'available': available, 'verbose': verbose,
                           'value': 'Not available', 'score': 0, 'model': obj})
            return result
        else:
            available = True

            if obj.bonus_amount.find(",") > 0:
                bonus_amount = [float(i) for i in obj.bonus_amount.split(",")]
            else:
                bonus_amount = [float(obj.bonus_amount)]

            if obj.maximum_bonus_amount:
                maximum_bonus_amount = obj.maximum_bonus_amount
            else:
                maximum_bonus_amount = sum(bonus_amount)
            verbose += " This plan offers No Claim Bonus of upto maximum of %i%% of Sum Insured." % int(
                maximum_bonus_amount)

            if obj.maximum_years:
                time_to_maximize_sa = obj.maximum_years
            else:
                if len(bonus_amount) == 1:
                    time_to_maximize_sa = obj.maximum_bonus_amount / \
                                          bonus_amount[0]
                else:
                    time_to_maximize_sa = len(bonus_amount)

            max_achievable_bonus = 200.0

            max_limit_score = 100 * \
                              float(maximum_bonus_amount) / max_achievable_bonus

            if time_to_maximize_sa < 3:
                time_to_maximize_score = 100

            if 3 <= time_to_maximize_sa <= 6:
                time_to_maximize_score = 60

            if 6 < time_to_maximize_sa:
                time_to_maximize_score = 20

            """
            Reduction on claim calculation removed for now
            reduction = obj.reduction_in_bonus_amount_after_claim

            if 0 == reduction:
                reduction_score = 100

            if 0 < reduction <= 10:
                reduction_score = 80

            if 11 <= reduction <= 20:
                reduction_score = 60

            if 21 <= reduction:
                reduction_score = 40
            """

            final_score = (max_limit_score + time_to_maximize_score) / 2

            value = "%s%%" % (bonus_amount[0])
            result.update({'available': available, 'score': final_score,
                           'verbose': verbose, 'value': value})
            return result

    def score_premium_discount(match_plan_obj, slab, data):
        # TODO: Add flag for cummulative Vs on-base and score accordingly

        obj = slab.premium_discount
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'TOP'}
        if not obj.is_available:
            available = False
            result.update({'available': available, 'verbose': "",
                           'score': 0, 'value': 'Not available'})
            return result

        # ascore : discount_value
        # bscore : time to reach maximum discount value
        available = True
        if 0 < obj.discount_amount <= 5:
            ascore = 40
        elif 5 < obj.discount_amount <= 10:
            ascore = 50
        elif 10 < obj.discount_amount <= 15:
            ascore = 80
        else:
            ascore = 100

        time_to_maximize_discount = 100 * \
                                    (obj.maximum_discount_amount / float(obj.discount_amount))

        if time_to_maximize_discount <= 2:
            bscore = 100
        elif 2 < time_to_maximize_discount <= 5:
            bscore = 60
        else:
            bscore = 20

        final_score = (ascore + bscore) / 2

        # Plan offers discount on premium
        verbose = 'This policy offers a discount on the premium as shown above, if you do not claim. However, this plan does not offer any bonus (increase in coverage)'
        value = "%s%%" % obj.discount_amount
        result.update({'score': final_score, 'verbose': verbose,
                       'value': value, 'available': available})
        return result

    def score_copay(match_plan_obj, slab, data):
        applicable_copays = match_plan_obj.calculate_copay(slab, data)
        # print "\n\n", slab.health_product.insurer.title, slab.health_product.title
        # Filter applicable_copays to pick copays with high incidence and non-zero effective_copay
        # If effective copay is zero for all, pick high incidence copay
        COPAY_INCIDENCE_ORDER = ['GENERAL_COPAY', 'AGE_COPAY', 'ENTRY_COPAY',
                                 'ROOM_COPAY', 'NONNETWORK_COPAY', 'ZONAL_COPAY', 'AGE_DEFERED_COPAY', 'NOCOPAY']
        applicable_copays = sorted(
            applicable_copays, key=lambda d: COPAY_INCIDENCE_ORDER.index(d[0]))
        # for copay_type, result in applicable_copays:
        #    print copay_type, result

        non_zero_copay = [[copay_type, result] for copay_type,
                                                   result in applicable_copays if result['effective_copay'] != 0]
        if non_zero_copay:
            copay_type, result = non_zero_copay[0]
        else:
            copay_type, result = applicable_copays[0]

        result[
            'verbose'] = 'Copay is your share (in %) on the final payable claim amount, that you have to bear. ' \
                         'Insurance companies will deduct this share and then settle the claim. Lower the copay the ' \
                         'better. ' + result['verbose']
        delta_copay_score = match_plan_obj.get_delta_copay_score(
            result['effective_copay'])
        # print "=>", copay_type, result['effective_copay'], delta_copay_score

        if data['service_type'] in ['PVT_HIGH_END', 'PVT']:
            if result['effective_copay'] == 0 or copay_type == "NOCOPAY":
                result['score'] = 100
            elif copay_type in ['AGE_DEFERED_COPAY']:
                result['score'] = 90
            elif copay_type in ['ENTRY_COPAY', 'GENERAL_COPAY', 'AGE_COPAY', 'ROOM_COPAY']:
                result['score'] = 0
            elif copay_type == "NONNETWORK_COPAY":
                result['score'] = 60 + delta_copay_score
            elif copay_type == "ZONAL_COPAY":
                result['score'] = 75 + delta_copay_score
        else:
            if result['effective_copay'] == 0 or copay_type == "NOCOPAY":
                result['score'] = 100
            elif copay_type in ['AGE_DEFERED_COPAY']:
                result['score'] = 90
            elif copay_type in ['GENERAL_COPAY', 'AGE_COPAY', 'ENTRY_COPAY']:
                result['score'] = 20 + delta_copay_score
            elif copay_type == "ROOM_COPAY":
                result['score'] = 40 + delta_copay_score
            elif copay_type == "NONNETWORK_COPAY":
                result['score'] = 60 + delta_copay_score
            elif copay_type == "ZONAL_COPAY":
                result['score'] = 75 + delta_copay_score

        # print "> Final", result['score']
        result.pop("effective_copay")
        result['available'] = ''
        result['importance'] = 'TOP'
        result['model'] = None
        return result

    def score_room_rent(match_plan_obj, slab, data):
        """
        Zonal Room rent, based on city if available
        Convert figures into availability and then score
        Room rent       Metro           Non-Metro
        1000-2000       -----           Ward
        2000-3000       Ward            Shared
        3000-5000       Shared room     Pvt. Room
        5000-10000      Pvt. Room       Pvt + 1
        >10000          Pvt + 1         Pvt + 1

        Room        Ranking
        Ward        20
        Shared Room 50
        Pvt.        85
        Pvt. + 1    95
        No Limit    100

        """
        room_rent_definition = "Room Rent Limit is a Limit applied by Insurance Cos on the the choice of Hospital " \
                               "Room you can opt for. The limit could be on the Room Charges or on the Room Category. " \
                               "The lesser than cappings the better."

        zonal_room_rents = ZonalRoomRent.objects.filter(
            Q(slabs=slab) & (Q(state=data['city'].state) | Q(city=data['city'])))
        if zonal_room_rents:
            obj = zonal_room_rents[0]
        else:
            obj = ZonalRoomRent.objects.filter(
                slabs=slab, state__isnull=True, city__isnull=True)[0]
        result = {'score': 0, 'verbose': '', 'model': obj, 'value': {
            'text': '', 'meta': ''}, 'available': True, 'importance': 'TOP'}

        if obj.conditions in match_plan_obj.ROOM_ORDER:
            if data['city'] in match_plan_obj.METRO_CITIES:
                if obj.conditions == 'CLASSD':
                    limit_value = 2000
                elif obj.conditions == 'CLASSC':
                    limit_value = 5000
                elif obj.conditions == 'CLASSB':
                    limit_value = 8000
                else:
                    limit_value = 10000
            else:
                if obj.conditions == 'CLASSD':
                    limit_value = 1000
                elif obj.conditions == 'CLASSC':
                    limit_value = 2000
                elif obj.conditions == 'CLASSB':
                    limit_value = 5000
                else:
                    limit_value = 6000

            score, affordable_room_type = match_plan_obj.get_room_rent_limit_score(
                limit_value, data)
            value = {'text': "%s" % obj.room_available, 'meta': obj.conditions}
            result.update(
                {'score': score, 'verbose': room_rent_definition, 'value': value})

        if obj.conditions == 'NO_LIMIT':
            score = 100
            available = False
            value = {'text': 'No limit on room rent', 'meta': 'NO_LIMIT'}
            result.update(
                {'score': score, 'verbose': room_rent_definition, 'value': value, 'available': available})

        if obj.conditions == 'SA_OR_VALUE_LIMIT':
            available = True
            limit_value = obj.limit_daily or (
                                                 float(obj.limit_daily_in_percentage) / 100) * slab.sum_assured
            score, affordable_room_type = match_plan_obj.get_room_rent_limit_score(
                limit_value, data)
            value = {'text': "Rs %s/- limit" % hutils.currency_format(
                limit_value), 'meta': affordable_room_type}
            verbose = "Daily room rent limit of Rs %s/-" % hutils.currency_format(
                int(limit_value))
            result.update(
                {'score': score, 'verbose': room_rent_definition, 'value': value, 'available': available})

        return result

    def score_claims(match_plan_obj, slab, data):
        obj = slab.health_product.insurer.claims_data
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'TOP'}
        if obj:
            value = "Settled in 30 days - %s%%<br/>Rejection ratio - %s%%" % (
                int(obj.claims_response_time), int(obj.rejection_ratio))
            verbose = "Settlement ratio measures what percentage of claims the company settles in 30 days. Higher the better. Rejection rate measures what percentage of claims the company declines. Lower the better."
        else:
            value = ""
            verbose = ""
        result.update({'score': 100, 'value': value, 'verbose': verbose,
                       'importance': 'TOP', 'available': True})
        return result

    def score_pre_hospitalization(match_plan_obj, slab, data):
        obj = slab.pre_post_hospitalization
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'HIGH'}

        if not obj.is_available:
            available = False
            # No pre and post hospitalization benefits are not available
            score = 0
            verbose = "Normally, companies offer pre-hospitalization expenses (diagnosis,tests etc.) incurred 30 " \
                      "days prior to hospitalization. This plan does not cover such expenses."
            value = "Not Available"

        else:
            available = True
            value = "Available"
            score = 0

            if obj.days_pre_hospitalization < 30:
                score = 40

            elif 30 <= obj.days_pre_hospitalization < 45:
                score = 60

            elif 45 <= obj.days_pre_hospitalization < 60:
                score = 80

            else:
                score = 100

            cummulative_limit = None
            if obj.cummulative_limit:
                cummulative_limit = obj.cummulative_limit
            if obj.cummulative_limit_in_percentage:
                cummulative_limit = (
                                        float(obj.cummulative_limit_in_percentage) / 100) * slab.sum_assured

            if cummulative_limit:
                verbose = "Pre (for diagnosis, tests etc.) and post hospitalization (recovery period) expenses upto" \
                          " Rs %s will be covered in this plan. Expenses incurred %s days pre and %s days post " \
                          "hospitalization will be covered in this plan" % (
                              hutils.currency_format(cummulative_limit), obj.days_pre_hospitalization,
                              obj.days_post_hospitalization)
            else:
                verbose = "Medical expenses up to %d days before admission will be covered." % obj.days_pre_hospitalization

        result.update({'available': available, 'verbose': verbose,
                       'score': score, 'value': value})
        return result

    def score_post_hospitalization(match_plan_obj, slab, data):
        obj = slab.pre_post_hospitalization
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'HIGH'}

        if not obj.is_available:
            available = False
            # No pre and post hospitalization benefits are not available
            score = 0
            verbose = "Normally, companies offer post-hospitalization expenses (recovery period) for upto 60 days. This plan does not cover such expenses."
            value = "Not Available"

        else:
            available = True
            value = "Available"
            score = 0

            if obj.days_post_hospitalization < 45:
                score = 40

            elif 45 <= obj.days_post_hospitalization < 60:
                score = 60

            elif 60 <= obj.days_post_hospitalization < 90:
                score = 80

            else:
                score = 100

            cummulative_limit = None
            if obj.cummulative_limit:
                cummulative_limit = obj.cummulative_limit
            if obj.cummulative_limit_in_percentage:
                cummulative_limit = (
                                        float(obj.cummulative_limit_in_percentage) / 100) * slab.sum_assured

            if cummulative_limit:
                verbose = "Pre (for diagnosis, tests etc.) and post hospitalization (recovery period) expenses upto " \
                          "Rs %s will be covered in this plan. Expenses incurred %s days pre and %s days post" \
                          " hospitalization will be covered in this plan" % (
                              hutils.currency_format(cummulative_limit), obj.days_pre_hospitalization,
                              obj.days_post_hospitalization)
            else:
                verbose = "Medical expenses up to %d days after discharge will be covered." % obj.days_post_hospitalization

        result.update({'available': available, 'verbose': verbose,
                       'score': score, 'value': value})
        return result

    def score_health_checkup(match_plan_obj, slab, data):
        obj = slab.health_checkup
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'MID'}

        if not obj.is_available:
            # health checkups are not available
            available = False
            score = 0
            value = "Not available"
            verbose = "Many plans provide a free health checkup after 3-4 years, provided you have not claimed. This plan does not offer free health checkups"
            result.update({'available': available, 'score': score,
                           'verbose': verbose, 'value': value})
            return result

        cfscore = 0
        if obj.claim_free_years <= 2:
            cfscore = 100

        elif 2 < obj.claim_free_years <= 4:
            cfscore = 60

        else:
            cfscore = 20

        limit = obj.limit or (
                                 float(obj.limit_in_percentage) / 100) * float(slab.sum_assured)
        limit_score = 0

        if limit <= 750:
            limit_score = 40

        elif 750 < limit <= 1500:
            limit_score = 60

        elif 1500 < limit <= 2500:
            limit_score = 80

        else:
            limit_score = 100

        value = "Available"
        available = True
        score = (cfscore * 0.7 + limit_score * 0.3)
        verbose = "This plan offers free health checkup of upto Rs %s, provided you have not claimed for %s years" % (
            hutils.currency_format(limit), obj.claim_free_years)
        result.update({'available': available, 'score': score,
                       'verbose': verbose, 'value': value})
        return result

    def score_daily_cash(match_plan_obj, slab, data):
        network = slab.network_hospital_daily_cash
        non_network = slab.non_network_hospital_daily_cash
        result = {'score': 0, 'verbose': '', 'model': network,
                  'value': '', 'available': True, 'importance': 'MID'}

        network_score = 0
        non_network_score = 0

        if not network.is_available and not non_network.is_available:
            # hospital cash is not available in network or non-network
            # hospitals
            available = False
            verbose = "For longer duration hospitalization, plans provide approximately Rs.500 - Rs.1,500 per day for" \
                      " about 7 days for small expenses. This plan does not provide daily cash expenses"
            score = 0
            value = "Not Available"
            result.update({'available': available, 'score': score,
                           'value': value, 'verbose': verbose})
            return result

        if not network.is_available and non_network.is_available:
            raise Exception(
                "Assumption that, hospital cash cannot be available for non-network if its not available for network, violated")

        if not non_network.is_available and network.is_available:
            non_network_score = 0

        if network.is_available:
            if network.limit_per_day_allowance <= 2500:
                network_score = 20

            elif 2500 < network.limit_per_day_allowance <= 5000:
                network_score = 40

            elif 5000 < network.limit_per_day_allowance <= 10000:
                network_score = 80

            else:
                network_score = 100

        if non_network.is_available:
            if non_network.limit_per_day_allowance <= 2500:
                non_network_score = 20

            elif 2500 < non_network.limit_per_day_allowance <= 5000:
                non_network_score = 40

            elif 5000 < non_network.limit_per_day_allowance <= 10000:
                non_network_score = 80

            else:
                non_network_score = 100

        score = (network_score + non_network_score) / 2

        if network.limit_per_day_allowance == non_network.limit_per_day_allowance:
            value = "Rs %s" % hutils.currency_format(
                network.limit_per_day_allowance)
            verbose = "For longer duration hospitalization, this plan provides a fixed amount per day for small expenses. Total limit for such expenses is Rs. %s" % (
                hutils.currency_format(network.limit_per_day_allowance))
        else:
            if not non_network.is_available:
                verbose = "Hospital cash of Rs %s is available in network hospitals only" % (
                    hutils.currency_format(network.limit_per_day_allowance))
            else:
                verbose = "For longer duration hospitalization, this plan provides a fixed amount per day for small " \
                          "expenses.Total limit for such expenses is Rs %s is available for network hospitals and " \
                          "Rs %s for non-network hospitals" % (
                              hutils.currency_format(network.limit_per_day_allowance),
                              hutils.currency_format(non_network.limit_per_day_allowance))
            allowance_value = network.limit_per_day_allowance or non_network.limit_per_day_allowance
            value = "Rs %s" % hutils.currency_format(allowance_value)

        available = True
        result.update({"available": available, "score": score,
                       'verbose': verbose, 'value': value})
        return result

    def score_pre_existing(match_plan_obj, slab, data):
        obj = slab.pre_existing
        if obj.waiting_period <= 12:
            score = 100
        elif 12 < obj.waiting_period <= 24:
            score = 80
        elif 24 < obj.waiting_period <= 36:
            score = 60
        elif 36 < obj.waiting_period <= 48:
            score = 40
        else:
            score = 0
        result = {'score': score, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'NA'}
        return result

    def score_critical_illness_coverage(match_plan_obj, slab, data):
        obj = slab.critical_illness_coverage
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'LOW'}

        if (not obj.is_available) or match_plan_obj.NO_CI_SLAB(slab):
            # No CI available in the plan
            available = False
            score = 0
            value = "Not available"
            verbose = "Critical illnesses like Cancer / Stroke cost more. So some plans provide an additional " \
                      "lumpsum in case of diagnosis of a critical illness. However, you get higher and cost effective" \
                      " coverage, if you buy a specific critical illness or top-up plans, available separately."
            result.update({'available': available, 'score': score,
                           'verbose': verbose, 'value': value})
            return result

        available = True
        value = "Available"

        cilimit = obj.limit or (
                                   float(obj.limit_in_percentage) / 100) * (slab.sum_assured)
        # Max ci limit can be equal to SA
        cilimit_score = cilimit / float(slab.sum_assured)

        if obj.included_in_plan:
            weight = 50
            verbose = "Critical illnesses like cancer / stroke cost more. So a lumpsum of Rs. %s is provided in case of" \
                      " diagnosis of a critical illness. This lumpsum is not additional, it's a part of your overall cover. " % (
                          hutils.currency_format(cilimit))
        else:
            if obj.benefit_type == 'REIMBURSEMENT':
                weight = 80
                verbose = "Critical illnesses like cancer/Stroke costs more. This plan/some plans provide an additional" \
                          " amount of Rs. %s for treatment of such illnesses. However, you get higher and cost effective " \
                          "coverage, if you buy a specific critical illness or top-up plans, available separately." % (
                              hutils.currency_format(cilimit))
            else:  # obj.benefit_type == 'LUMP_SUM'
                weight = 100
                verbose = "Critical illnesses like cancer/Stroke costs more. This plan/some plans provide an additional " \
                          "lump sum amount of Rs. %s in case of diagnosis of a critical illness. However, you get higher" \
                          " and cost effective coverage, if you buy a specific critical illness or top-up plans, available separately." % (
                              hutils.currency_format(cilimit))

        score = weight * cilimit_score

        result.update({'available': available, 'verbose': verbose,
                       'score': score, 'value': value})
        return result

    def score_restore_benefits(match_plan_obj, slab, data):
        obj = slab.restore_benefits
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'HIGH'}

        if not obj.is_available:
            available = False
            verbose = "Some plans reinstate your coverage in case you exhaust your cover in a policy year. This plan does not restore your cover."
            score = 0
            value = "Not available"
            result.update(
                {'available': available, 'verbose': verbose, 'score': score, 'value': value})
            return result

        restoration_percentage = obj.amount_restored_percentage or obj.amount_restored * \
                                                                   100 / float(slab.sum_assured)

        if restoration_percentage <= 25:
            score = 50

        elif 25 < restoration_percentage <= 50:
            score = 75

        else:
            score = 100

        verbose = "This plan reinstates your basic cover, in case you exhaust your cover in a policy year. Restoration works only for unrelated claims."
        value = "Upto %s%% of sum assured" % restoration_percentage
        result.update({'verbose': verbose, 'score': score, 'value': value})
        return result

    # TODO: Not so important factors
    def score_ambulance_charges(match_plan_obj, slab, data):
        obj = slab.ambulance_charges
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'MID'}
        if obj.condition == "NO":
            available = False
            value = 0
            score = 0
            verbose = "This plan doesn't cover ambulance charges. Plans that do offer ambulance expenses, do not give more than Rs.1,000 -Rs.3,000."
        else:
            available = True
            value = "Covered"

            if obj.is_ambulance_services_covered:
                score = 100
            else:
                # Third party services not available
                score = 80

            limit_nw = obj.limit_in_network_hospitals or 0
            limit_non_nw = obj.limit_in_non_network_hospitals or 0
            per_hosp_limit = max(limit_nw, limit_non_nw)

            max_limit_nw = obj.max_limit_in_network_hospitals or 0
            max_limit_non_nw = obj.max_limit_in_non_network_hospitals or 0
            max_limit = max(max_limit_nw, max_limit_non_nw)

            verbose = "You can claim"
            if per_hosp_limit:
                verbose += " Rs. %s per hospitalization" % hutils.currency_format(per_hosp_limit)
            if max_limit:
                verbose += " upto a maximum of Rs. %s" % hutils.currency_format(max_limit)
            verbose += " for ambulance charges."

        result.update({'available': available, 'value': value,
                       'verbose': verbose, 'score': score})
        return result

    def score_alternative_practices_coverage(match_plan_obj, slab, data):
        obj = slab.alternative_practices_coverage
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'MID'}

        if not (obj and obj.alternative_practices.all()):
            available = False
            value = "Not Covered"
            verbose = "Alternative practices like ayurveda / homeopathy are not covered in this plan. Plans that do" \
                      " cover non-allopathic treatment, do not give more than Rs.20,000-Rs.30,000 for such expenses."
        else:
            available = True
            vlimit = obj.limit or (
                                      float(obj.limit_in_percentage) / 100) * slab.sum_assured
            value = "Covered"
            verbose = "%s are covered upto a sum of Rs %s" % (", ".join([ap.name.title(
            ) for ap in obj.alternative_practices.all()]), hutils.currency_format(vlimit))

        result.update(
            {'available': available, 'value': value, 'verbose': verbose})
        return result

    def score_outpatient_benefits(match_plan_obj, slab, data):
        obj = slab.outpatient_benefits
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'MID'}

        verbose = "Most plans only cover expenses when you are hospitalised, not for consultations or minor treatments " \
                  "as an outpatient. Coverage for such expenses is termed as 'Outpatient benefit'."
        if obj.is_covered:
            available = True
            value = "Covered"
            verbose += " This plan covers outpatient expenses of upto Rs %s." % (
                hutils.currency_format(obj.limit))
        else:
            available = False
            value = "Not Covered"
            verbose += " This plan offers no outpatient benefits. Plans that do offer outpatient expenses, provide not" \
                       " more than Rs. 5,000 - Rs.10,000 for such expenses."

        result.update(
            {'available': available, 'value': value, 'verbose': verbose})
        return result

    def score_domiciliary_hospitalization(match_plan_obj, slab, data):
        obj = slab.domiciliary_hospitalization
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'MID'}

        verbose = "Treatment taken at home for any medical reason, if hospitalization is not possible. This is a rare " \
                  "scenario and is termed as 'Domiciliary Hospitalization'."

        if obj.is_covered:
            available = True
            value = "Covered"
            score = 100
            vlimit = obj.limit or (
                                      float(obj.limit_in_percentage) / 100) * slab.sum_assured
            verbose += " In such a scnenario, expenses of upto Rs %s are covered in this plan." % (
                hutils.currency_format(vlimit))
        else:
            available = False
            score = 0
            value = "Not Covered"
            verbose += " This plan does not cover domiciliary hospitalization expenses."

        result.update({'available': available, 'value': value,
                       'score': score, 'verbose': verbose})
        return result

    def score_organ_donor(match_plan_obj, slab, data):
        obj = slab.organ_donor
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'HIGH'}

        if obj.is_covered:
            available = True
            value = "Covered"
            # vlimit = obj.limit or (
            #     float(obj.limit_in_percentage) / 100) * slab.sum_assured
            if obj.is_surgery_covered:
                score = 100
                verbose = "In case of an organ transplant and there is a need of an organ donor, hospitalisation charges" \
                          " for the organ donor and surgical expenses for harvesting the organ are covered in this plan." \
                          " However, the cost of finding a donor is not covered."
            else:
                score = 75
                verbose = "In case of an organ transplant and there is a need of an organ donor, hospitalisation charges" \
                          " for the organ donor are covered in this plan. However, the cost of finding a donor is not covered."
        else:
            available = False
            score = 0
            value = "Not Covered"
            verbose = "In case of an organ transplant and there is a need for an organ donor, this plan does not cover the medical expenses of the organ donor."

        result.update({'available': available, 'score': score,
                       'value': value, 'verbose': verbose})
        return result

    def score_convalescence_benefit(match_plan_obj, slab, data):
        obj = slab.convalescence_benefit
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'LOW'}

        if obj.condition == 'NO':
            available = False
            score = 0
            value = "Not available"
            verbose = "This plan provides no convalescence benefits. Convalescence means the recovery period. Some plans" \
                      " offer a small amount of money for recovery, usually never more than Rs. 5,000. "
        else:
            available = True
            value = "Covered"
            score = 100
            if obj.condition == "AVAILABLE_PER_DAY":
                verbose = "Convalescence benefits which mean benefits during recovery period of Rs %s are available on " \
                          "a per day basis. These expenses are available for %s days of hospitalization" % (
                              hutils.currency_format(obj.limit), obj.minimum_days_of_hospitalization)
            if obj.condition == "AVAILABLE_LUMPSUM":
                verbose = "A lumpsum amount of Rs %s is available as convalescence benefit. Convalescence means the " \
                          "recovery period.These expenses are available for %s days of hospitalization" % (
                              hutils.currency_format(obj.limit), obj.minimum_days_of_hospitalization)

        result.update({'available': available, 'value': value,
                       'verbose': verbose, 'score': score})
        return result

    def score_day_care(match_plan_obj, slab, data):
        obj = slab.day_care
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': None, 'importance': 'HIGH'}

        if obj.is_covered:
            available = True
            value = "Covered"
            if obj.defined_list:
                score = 50
                verbose = "Health insurance covers medical expenses only for hospitalizations exceeding 24 hours. Day " \
                          "care procedures refers to medical treatment undertaken in a hospital requiring less than 24" \
                          " hours due to technological advancement. This plan covers listed day care procedures as mentioned in the policy wordings."
            else:
                score = 100
                verbose = "Health insurance covers medical expenses only for hospitalizations exceeding 24 hours. Day " \
                          "care procedures refers to medical treatment undertaken in a hospital requiring less than 24" \
                          " hours due to technological advancement. All day care procedures are covered in this plan."
        else:
            available = False
            value = "Not available"
            score = 0
            verbose = "Health insurance covers medical expenses only for hospitalizations exceeding 24 hours. Day care " \
                      "procedures refers to medical treatment undertaken in a hospital requiring less than 24 hours due" \
                      " to technological advancement. No day care procedures are not covered in this plan."

        result.update({'available': available, 'value': value,
                       'verbose': verbose, 'score': score})
        return result

    def score_dental(match_plan_obj, slab, data):
        obj = slab.dental_coverage
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'LOW'}

        if obj.condition == "NO":
            available = False
            value = "Not Covered"
            verbose = "Dental expenses are not covered in this plan. Plans that cover dental expenses, do not pay more " \
                      "than Rs.5,000/- that too usually after 3-4 years."
        else:
            available = True
            value = "Covered"
            if obj.copay:
                verbose = "Dental expenses of upto Rs %s are covered. You will however, need to pay %s%% of the valid dental claim" % (
                    hutils.currency_format(obj.limit), obj.copay)
            else:
                if obj.waiting_period:
                    verbose = "Dental expenses of upto Rs %s are covered. However, they are covered only after %s months." % (
                        hutils.currency_format(obj.limit), obj.waiting_period)
                else:
                    verbose = "Dental expenses of upto Rs %s are covered." % (
                        hutils.currency_format(obj.limit))

        result.update(
            {'available': available, 'value': value, 'verbose': verbose})
        return result

    def score_eye(match_plan_obj, slab, data):
        obj = slab.eye_coverage
        result = {'score': 0, 'verbose': '', 'model': obj,
                  'value': '', 'available': True, 'importance': 'LOW'}

        if obj.condition == "NO":
            available = False
            value = "Not Covered"
            verbose = "Eye care expenses like spectacles, contact lenses etc are not covered in this plan. Plans that " \
                      "cover eye expenses, do not pay more than Rs.5,000/- that too usually after 3-4 years."
        else:
            available = True
            value = "Covered"
            if obj.copay:
                verbose = "Eye care expenses (spectacles, contact lenses etc.) of upto Rs %s are covered. You will however, need to pay %s%% of the amount claimed" % (
                    hutils.currency_format(obj.limit), obj.copay)
            else:
                if obj.waiting_period:
                    verbose = "Eye care expenses (spectacles, contact lenses etc.) of upto Rs %s are covered. However, they are covered only after %s months." % (
                        hutils.currency_format(obj.limit), obj.waiting_period)
                else:
                    verbose = "Eye care expenses (spectacles, contact lenses etc.) of upto Rs %s are covered." % (
                        hutils.currency_format(obj.limit))

        result.update(
            {'available': available, 'value': value, 'verbose': verbose})
        return result

    def get_weighted_medical_score(match_plan_obj, slab):
        mmodel = slab.medical_score.get()
        mfactors = [mmodel.specific_waiting_period, mmodel.general_waiting_period,
                    mmodel.ped_waiting_period, mmodel.standard_exclusion]  # , mmodel.disease_condition]
        total_score = sum(mfactors) / len(mfactors)
        return round(total_score)

    def get_weighted_score(match_plan_obj, slab_score, slab):
        weight_matrix = {
            'PVT_HIGH_END': {
                'TOP': {
                    'copay': 20,
                    'room_rent': 40,
                    'premium': 0,
                    'claims': 15,
                    'pre_existing': 25
                },
                'FINANCIAL': {
                    'bonus': 100,
                    'premium_discount': 40,
                },
            },
            'PVT': {
                'TOP': {
                    'copay': 20,
                    'room_rent': 25,
                    'premium': 20,
                    'claims': 15,
                    'pre_existing': 20
                },
                'FINANCIAL': {
                    'bonus': 100,
                    'premium_discount': 40,
                },
            },
            'SHARED': {
                'TOP': {
                    'copay': 40,
                    'room_rent': 0,
                    'premium': 30,
                    'claims': 15,
                    'pre_existing': 25
                },
                'FINANCIAL': {
                    'bonus': 100,
                    'premium_discount': 40,
                },
            }
        }

        # print "PLAN : ", slab.health_product.insurer.title,
        # slab.health_product.title
        top_factor_score = {}
        maternity_score = None
        financial_factor_score = {}
        other_factor_score = {}

        # Premium factor calculation
        max_premium = max([cpt.premium for cpt in match_plan_obj.slabs_cpt.values()])
        min_premium = min([cpt.premium for cpt in match_plan_obj.slabs_cpt.values()])

        if max_premium == min_premium:
            premium_score = 50
        else:
            premium_score = 100 - \
                            (50 * ((match_plan_obj.slabs_cpt[slab].premium -
                                    min_premium) / (float(max_premium - min_premium))))
        weighted_premium_score = premium_score * \
                                 weight_matrix[match_plan_obj.data['service_type']]['TOP']['premium']

        if match_plan_obj.debug:
            slab_score.append(['premium', {
                'available': True,
                'verbose': '',
                'title': '',
                'importance': '',
                'value': 'Available',
                'score': premium_score
            }])
        top_factor_score.update({'premium': weighted_premium_score})

        for attribute, data in slab_score:
            if data['importance'] == 'NA':
                continue

            # Top factors with segment bias : 70% weightage
            if attribute == "maternity_cover":
                maternity_score = data['score'] * 100

            if attribute == "copay":
                top_factor_score.update(
                    {'copay': data['score'] * weight_matrix[match_plan_obj.data['service_type']]['TOP']['copay']})

            if attribute == "room_rent":
                top_factor_score.update({'room_rent': data[
                                                          'score'] * weight_matrix[match_plan_obj.data['service_type']]['TOP'][
                                                          'room_rent']})

            if attribute == "claims":
                top_factor_score.update(
                    {'claims': 100 * weight_matrix[match_plan_obj.data['service_type']]['TOP']['claims']})

            if attribute == "pre_existing":
                top_factor_score.update(
                    {'pre_existing': data['score'] * weight_matrix[match_plan_obj.data['service_type']]['TOP']['pre_existing']})

            # Bonus and Premium Discount will make sure that the final
            # score comes out even. For bonus=100, pd=0 score would be 100*100 + 0*10/110 = 90
            # For Bonus=0 and pd=100, 0*100 + 100*10/110 = 9
            # Final score = ( bonus + pd ) * weight
            if attribute in ["bonus"]:
                financial_factor_score.update({'bonus': data[
                                                            'score'] *
                                                        weight_matrix[match_plan_obj.data['service_type']]['FINANCIAL']['bonus']})

            if attribute in ["premium_discount"]:
                financial_factor_score.update({'discount': data[
                                                               'score'] *
                                                           weight_matrix[match_plan_obj.data['service_type']]['FINANCIAL'][
                                                               'premium_discount']})

            # ,"ambulance_charges" ,"domiciliary_hospitalization" ,"organ_donor" ,"convalescence_benefit"]:
            if attribute in ["pre_post_hospitalization", "health_checkup", "daily_cash", "critical_illness_coverage",
                             "restore_benefits"]:
                other_factor_score.update({attribute: data['score'] * 100})

        if maternity_score:
            top_factor_total_score = (
                                         sum(top_factor_score.values()) * 0.5 + maternity_score * 0.5) / 100
        else:
            top_factor_total_score = sum(top_factor_score.values()) / 100

        financial_factor_total_score = sum(financial_factor_score.values(
        )) / sum(weight_matrix[match_plan_obj.data['service_type']]['FINANCIAL'].values())

        other_factor_total_score = sum(
            other_factor_score.values()) / (len(other_factor_score) * 100)

        # total_weighted_score = (top_factor_total_score * 0.60) + (medical_score*0.10) + (financial_factor_total_score * 0.25) + (other_factor_total_score * 0.05)
        total_weighted_score = (top_factor_total_score * 0.60) + (
            financial_factor_total_score * 0.25) + (other_factor_total_score * 0.15)
        # print "TOP :", top_factor_score, "FINANCIAL :", financial_factor_score, "OTHER :", other_factor_score
        # print "TOP :", top_factor_total_score, "FINANCIAL :", financial_factor_total_score, "OTHER :", other_factor_total_score
        # print "TOTAL : ", total_weighted_score, "\n\n\n\n"
        return int(total_weighted_score)
