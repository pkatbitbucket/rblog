from django.db.models import Q
from django.core.cache import caches
from collections import defaultdict
from operator import itemgetter, attrgetter
from importlib import import_module
from health_product.insurer import settings as insurer_settings
import health_product.queries as hqueries
import health_product.health_utils as hutils
from health_product.decorators import cache_memoized_method, cache_memoized_func
from health_product.services.compute_score import ComputeScore
from wiki.models import (Slab, RoomTypeCopay, City, MedicalCondition, TermParent, Term,
                         FamilyCoverPremiumTable, CoverPremiumTable, Insurer, Hospital, ZonalCopay)

ROOM_DISPLAY = dict(RoomTypeCopay.ROOM_TYPE_CHOICES)
HOSPITAL_DISPLAY = dict(RoomTypeCopay.HOSPITAL_TYPE_CHOICES)
cache = caches['health-results']

class MatchPlan(object):
    CAESAREAN_DELIVERY_LIMIT = 30000.0
    ROOM_ORDER = ['CLASSD', 'CLASSC', 'CLASSB',
                  'CLASSA']  # from shared to Pvt. Suite
    METRO_CITIES = City.objects.filter(
        name__in=['Mumbai', 'Thane', 'Navi Mumbai', 'Pune', 'Hyderabad', 'Kolkata', 'Bangalore', 'Delhi',
                  'Gurgaon', 'Noida', 'Ghaziabad', 'Faridabad', 'Chennai', 'Ahmedabad', 'Jaipur', 'Bhopal', 'Nagpur',
                  'Indore', 'Chandigarh', 'Patna'])
    TOPUP_TYPES = ['TOPUP', 'SUPER_TOPUP']
    # insurers for which even a single medical condition means offline payment
    # apollo-munich, tata-aig, reliance, hdfc-ergo, iffco-tokio
    NSTP_INSURER_IDS = [1, 7, 10, 13, 14]

    def __init__(self, data, product_type='INDEMNITY', insurer=None,
                 slab=None, product=None, debug=False, request=None,
                 medical_conditions=None):
        """
        data will have age(required), gender(required), city(required), service_type(PVT_HIGH_END, PVT, SHARED),
        hospitals, family, adults(optional), kids(optional)
        TODO: age(string) if in years will be 28, if in months 10m (months needed in AgeLimit, CoverPremiumTable and
        FamilyCoverPremiumTable)
        e.g. data = {
            'sum_assured' : 200000,
            'city' : <City model object>,
            'hospitals' : [<hid1>, <hid2>, <hid3>]
            'service_type' : 'PVT_HIGH_END',
            'family' : True,
            'adults' : 2,

            'kids' : 2,
            'age': '28',
            'gender' : 'MALE'  # MALE/FEMALE
            'spouse_age' : '24',
            'kid_ages' : ['6','7','12'],
        }
        """
        self.request = request
        self.data = data.copy()  # Changes in data must not go up the caller of the function
        self.product = product
        self.product_type = product_type.upper()
        self.is_topup = (self.product_type in self.TOPUP_TYPES)
        self.insurer_id = getattr(insurer, 'id', None)
        self.slab = slab
        if self.slab:
            self.product_type = self.slab.health_product.coverage_category
        self.medical_conditions = MedicalCondition.objects.filter(
            id__in=medical_conditions or []
        )
        self.debug = debug
        self.data['age'] = int(self.data['age'])
        self.data['city'] = self.get_city(data['city']['id'])
        self.foreign_keys = [sl.name for sl in Slab._meta.fields if getattr(
            sl, 'rel', '') and sl.name not in ['health_product']]

        # For every slab, we store the cluster of slabs qualifying the adults, kids, state, city criteria
        # This is used to calculate the SA and premiums for next few years
        self.cpt_cluster = {}

        # Coverpremiumtables of matching slabs, with keys as the slab and value
        # as the applicable CPT
        self.premiums = []
        self.slabs_cpt = self.filter_plans(self.data)
        self.slabs = self.slabs_cpt.keys()
        if self.premiums:
            self.max_premium = max(self.premiums)
            self.min_premium = min(self.premiums)
        self.term_parents = self.get_term_parents()
        self.term_parent_lookup = self.get_term_parents_lookup()

    def cache_args(self):
        return (self.data,
                self.slab.id if self.slab else '',
                self.insurer_id,
                self.product.id if self.product else '',
                self.product_type,
                self.medical_conditions,
                self.debug,
                self.request.COOKIES.get(
                    'insurer_slug', None) if self.request else ''
                )

    @staticmethod
    @cache_memoized_func
    def get_city(city_id):
        return City.objects.get(id=city_id)

    @staticmethod
    @cache_memoized_func
    def get_term_parents():
        return TermParent.objects.filter(is_enabled=True)

    @staticmethod
    @cache_memoized_func
    def get_term_parents_lookup():
        return dict([(t.id, t.parent) for t in Term.objects.all().select_related('parent')])

    @cache_memoized_method
    def filter_plans(self, data):
        """
        Output is a dictionary with keys as slabs which pass filter criteria and value as the applicable CoverPremiumTable model object
        TODO : UI to mark products as is_completed, only search in is_completed products
        """

        fdata = data.copy()
        fdata['sum_assured'] = round(fdata['sum_assured'] / 100000.0) * 100000
        fdata['age'] = int(fdata['age']) * 1000
        if fdata['self_age']:
            fdata['self_age'] = int(fdata['self_age']) * 1000

        completed_query = Q(health_product__is_completed=True)

        if self.product_type == 'ALL':
            Q_product_type = Q()
        elif self.product_type == 'TOPUP':
            Q_product_type = Q(
                health_product__coverage_category__in=self.TOPUP_TYPES)
        else:
            Q_product_type = Q(
                health_product__coverage_category=self.product_type)

        custom_query = Q()
        if self.insurer_id:
            custom_query = custom_query & Q(
                health_product__insurer_id=self.insurer_id)
        if self.product:
            custom_query = custom_query & Q(health_product=self.product)
        if self.slab:
            custom_query = custom_query & Q(id=self.slab.id)

        if self.slab:
            Q_cover = Q()
        elif self.product_type in self.TOPUP_TYPES:
            Q_cover = Q(deductible=fdata['deductible'])
        else:
            Q_min_cover = Q(sum_assured__gte=fdata['sum_assured'])

            if fdata['sum_assured'] < 1000000:
                Q_max_cover = Q(sum_assured__lte=fdata['sum_assured'] + 50000)
            elif fdata['sum_assured'] == 1000000:
                Q_max_cover = Q(sum_assured__lte=fdata['sum_assured'] + 500000)
            else:
                Q_max_cover = Q()

            Q_cover = Q_min_cover & Q_max_cover
        Q_proposer_not_insured = Q()
        if not fdata['is_self_insured']:
            Q_proposer_not_insured = Q(health_product__is_proposer_always_insured=False)
        filtered_slabs = Slab.objects.filter(
            completed_query &
            custom_query &
            Q_product_type &
            Q_cover &
            Q_proposer_not_insured &
            (
                Q(age_limit__isnull=True) |
                (
                    Q(age_limit__minimum_entry_age__lte=fdata['age']) &
                    (
                        Q(age_limit__maximum_entry_age__gte=fdata['age']) |
                        Q(age_limit__maximum_entry_age=None)
                    )
                )
            )
        )
        filtered_slabs = filtered_slabs.select_related('health_product',
            'general', 'pre_existing', 'dental_coverage', 'family_coverage', 'network_hospital_daily_cash',
            'ambulance_charges', 'alternative_practices_coverage', 'maternity_cover', 'eye_coverage', 'day_care',
            'age_limit', 'online_availability', 'medical_required', 'standard_exclusion', 'domiciliary_hospitalization',
            'bonus', 'premium_discount', 'convalescence_benefit', 'non_network_hospital_daily_cash',
            'pre_post_hospitalization', 'copay', 'health_checkup', 'claims', 'outpatient_benefits', 'organ_donor',
            'life_long_renewability', 'critical_illness_coverage', 'restore_benefits', 'special_feature')
        if fdata['family']:
            total_members = fdata['kids'] + fdata['adults']
            filtered_slabs = filtered_slabs.filter(health_product__policy_type="FAMILY",
                                                   family_coverage__number_of_maximum_members__gte=total_members)
            if len(fdata['kid_ages']) > 0:
                fdata['min_kid_age'] = min(fdata['kid_ages'])
                fdata['max_kid_age'] = max(fdata['kid_ages'])

                fdata['min_kid_age'] = fdata['min_kid_age'] * 1000
                kids_validation = Q(
                    age_limit__minimum_entry_age__lte=fdata['min_kid_age'])
                if fdata['max_kid_age'] >= 1:
                    kids_validation = kids_validation & Q(
                        family_coverage__maximum_dependent_age__gte=fdata['max_kid_age'])
                filtered_slabs = filtered_slabs.filter(kids_validation)
            else:
                fdata['min_kid_age'] = 0
                fdata['max_kid_age'] = 0

            matching_cpts = {}
            for sl in filtered_slabs:
                hp = sl.health_product
                mcpts_sl = FamilyCoverPremiumTable.objects.filter(
                    slabs=sl,
                    payment_period=1
                )
                mcpts_place_preference = mcpts_sl.filter(
                    city=fdata['city']
                ) or mcpts_sl.filter(
                    state__city=fdata['city']
                ) or mcpts_sl.filter(
                    city__isnull=True,
                    state__isnull=True
                )
                if hp.id not in insurer_settings.INSURER_SPECIFIC_SCORE:
                    mcpts = mcpts_place_preference.filter(
                        adults=fdata['adults'],
                        kids=fdata['kids']
                    )
                else:
                    hp_score = import_module('health_product.insurer.%s.score' % insurer_settings.INSURER_MAP[
                                             hp.insurer_id])
                    mcpts = hp_score.getCoverPremiumTables(
                        sl, mcpts_place_preference, fdata)
                cpt_cluster = list((mcpts))[:]

                final_mcpts = []
                for mt in mcpts:
                    lower_age_limit = mt.lower_age_limit
                    if lower_age_limit is None:
                        lower_age_limit = float('-inf')
                    upper_age_limit = mt.upper_age_limit
                    if upper_age_limit is None:
                        upper_age_limit = float('inf')
                    if lower_age_limit <= fdata['age'] <= upper_age_limit:
                        final_mcpts.append(mt)

                if len(final_mcpts) > 0:
                    mcpt = final_mcpts[-1]
                    matching_cpts[sl] = mcpt
                    self.cpt_cluster[sl] = cpt_cluster
                    self.premiums.append(mcpt.premium)
        else:
            matching_cpts = {}
            filtered_slabs = filtered_slabs.filter(health_product__policy_type="INDIVIDUAL")
            for sl in filtered_slabs:
                mcpts_sl = CoverPremiumTable.objects.filter(
                    slabs=sl,
                    payment_period=1
                )
                mcpts_place_preference = mcpts_sl.filter(
                    city=fdata['city']
                ) or mcpts_sl.filter(
                    state__city=fdata['city']
                ) or mcpts_sl.filter(
                    city__isnull=True,
                    state__isnull=True
                )

                mcpts = mcpts_place_preference.filter(
                    gender=fdata['gender']) or mcpts_place_preference
                cpt_cluster = list((mcpts))[:]

                final_mcpts = []
                for mt in mcpts:
                    lower_age_limit = mt.lower_age_limit
                    if lower_age_limit is None:
                        lower_age_limit = float('-inf')
                    upper_age_limit = mt.upper_age_limit
                    if upper_age_limit is None:
                        upper_age_limit = float('inf')
                    if lower_age_limit <= fdata['age'] <= upper_age_limit:
                        final_mcpts.append(mt)


                if len(final_mcpts) > 0:
                    mcpt = final_mcpts[-1]
                    matching_cpts[sl] = mcpt
                    self.cpt_cluster[sl] = cpt_cluster
                    self.premiums.append(mcpt.premium)

        return matching_cpts

    @cache_memoized_method
    def get_cpt(self):
        scores_to_calculate = {
            "score_maternity_cover": 'Pregnancy cover',
            "score_life_long_renewability": 'Renewability',
            "score_bonus": "Benefits if you don't claim",
            "score_premium_discount": 'Premium Discount',
            "score_copay": 'You pay (% of bill)',
            "score_room_rent": 'Room rent',
            "score_claims": 'Claims',
            "score_pre_hospitalization": 'Expenses before Admission',
            "score_post_hospitalization": 'Expenses after Discharge',
            "score_pre_existing": 'Pre-Existing',
            "score_health_checkup": 'Health Checkup',
            "score_daily_cash": 'Daily hospitalization allowance',
            "score_critical_illness_coverage": 'Critical illness Benefit',
            "score_restore_benefits": 'Restore benefits',
            "score_ambulance_charges": 'Emergency ambulance',
            "score_alternative_practices_coverage": 'Ayurveda/Homeopathy',
            "score_outpatient_benefits": 'Regular medical expenses (OPD)',
            "score_domiciliary_hospitalization": 'Home hospitalization',
            "score_organ_donor": 'Organ donor',
            "score_convalescence_benefit": 'Recovery benefit',
            "score_day_care": "Day care treatments",
            "score_dental": 'Dental cover',
            "score_eye": 'Eye cover'
        }
        slab_score = defaultdict(list)
        for sl in self.slabs:
            for score_method in scores_to_calculate.keys():
                if score_method.startswith("score_") and callable(getattr(ComputeScore, score_method)):
                    attribute = score_method.replace("score_", "")
                    if sl.health_product.policy_type == "INDIVIDUAL" and attribute in ['family_cover_premium_table',
                                                                                       'family_coverage']:
                        continue
                    if sl.health_product.policy_type == "FAMILY" and attribute in ['cover_premium_table']:
                        continue
                    attr_data = getattr(ComputeScore, score_method)(self, sl, self.data)

                    # using set difference property to enforce output of module to be in desired format.
                    if {'available', 'verbose', 'importance', 'value', 'score', 'model'}.difference(
                            set(attr_data.keys())):
                        raise Exception("Keys not matching for %s", attribute)

                    attr_data['title'] = scores_to_calculate[
                        "score_%s" % attribute]
                    slab_score[sl].append((attribute, attr_data))

        """ Override the models text_to_display here """
        hqueries.update_text_to_display(slab_score)
        cpt = None
        for sl in slab_score.keys():
            cpt = self.slabs_cpt[sl]
        return cpt

    def sort_plans(self, slab_score_list):
        if len(slab_score_list) == 0:
            return []

        sorted_score_list = sorted(
            slab_score_list, key=itemgetter('total_score'), reverse=True)

        if self.debug:
            return sorted_score_list
        final_filtered_score = sorted(
            sorted_score_list, key=hutils.sortkeypicker(['-total_score', 'premium']))

        # If customer searched and landed on a insurer specific page then that
        # should show at the top
        insurer_special = None
        if self.request.COOKIES.get('insurer_slug'):
            try:
                insurer_special = Insurer.objects.get(
                    slug=self.request.COOKIES.get('insurer_slug').strip())
            except:
                pass

        # Mark recommended such that top 3 plans are not from the same company
        non_integrated = []
        integrated = []
        recommended = []
        insurer_special_plans = []
        insurer_list = []
        pindex = 0

        for fp in final_filtered_score:
            if insurer_special and fp['insurer_id'] == insurer_special.id:
                if pindex < 3 and fp['insurer_id'] not in insurer_list:
                    fp['recommended'] = True
                    insurer_list.append(fp['insurer_id'])
                    pindex += 1
                    recommended.append(fp)
                else:
                    insurer_special_plans.append(fp)
            # TODO: check if product is integrated from is_integrated flag
            elif fp['insurer_id'] in [1, 2, 4, 6, 9, 10, 11, 13, 14, 15, 16]:
                if pindex < 3 and fp['insurer_id'] not in insurer_list:
                    fp['recommended'] = True
                    insurer_list.append(fp['insurer_id'])
                    pindex += 1
                    recommended.append(fp)
                else:
                    integrated.append(fp)
            else:
                non_integrated.append(fp)

        final_filtered_score = insurer_special_plans + \
                               recommended + integrated + non_integrated

        return final_filtered_score

    def sort_topup_plans(self, slab_score_list):
        sorted_score_list = sorted(slab_score_list,
                                   key=lambda sl_data: (sl_data['premium'] / sl_data['sum_assured'])
                                   )
        return sorted_score_list

    @cache_memoized_method
    def calculate_score(self):
        """
        Calculate score for all methods mentioned
        All score_ methods are supposed to return the format
        {
            'score': 10,
            'value' : 'Some small representative text'
            'verbose' : 'Some text explaining the value',
            'model' : model_obj,
            'available' : True/False  #if the attribute needs to be shown or scored,
        }
        importance in ['INFO', 'TOP','MID','LOW', 'NA'] stands for
        ['Informational', 'Top', 'Medium', 'Low', 'Not Applicable']
        """

        scores_to_calculate = {
            "score_maternity_cover": 'Pregnancy cover',
            "score_life_long_renewability": 'Renewability',
            "score_bonus": "Benefits if you don't claim",
            "score_premium_discount": 'Premium Discount',
            "score_copay": 'You pay (% of bill)',
            "score_room_rent": 'Room rent',
            "score_claims": 'Claims',
            "score_pre_hospitalization": 'Expenses before Admission',
            "score_post_hospitalization": 'Expenses after Discharge',
            "score_pre_existing": 'Pre-Existing',
            "score_health_checkup": 'Health Checkup',
            "score_daily_cash": 'Daily hospitalization allowance',
            "score_critical_illness_coverage": 'Critical illness Benefit',
            "score_restore_benefits": 'Restore benefits',
            "score_ambulance_charges": 'Emergency ambulance',
            "score_alternative_practices_coverage": 'Ayurveda/Homeopathy',
            "score_outpatient_benefits": 'Regular medical expenses (OPD)',
            "score_domiciliary_hospitalization": 'Home hospitalization',
            "score_organ_donor": 'Organ donor',
            "score_convalescence_benefit": 'Recovery benefit',
            "score_day_care": "Day care treatments",
            "score_dental": 'Dental cover',
            "score_eye": 'Eye cover'
        }

        if self.is_topup:
            del scores_to_calculate['score_restore_benefits']
            del scores_to_calculate['score_eye']
            del scores_to_calculate['score_outpatient_benefits']
            del scores_to_calculate['score_critical_illness_coverage']
            del scores_to_calculate['score_dental']
            del scores_to_calculate['score_convalescence_benefit']
            del scores_to_calculate['score_maternity_cover']

        slab_score = defaultdict(list)
        for sl in self.slabs:
            for score_method in scores_to_calculate.keys():
                if score_method.startswith("score_") and callable(getattr(ComputeScore, score_method)):
                    attribute = score_method.replace("score_", "")
                    if sl.health_product.policy_type == "INDIVIDUAL" and attribute in ['family_cover_premium_table',
                                                                                       'family_coverage']:
                        continue
                    if sl.health_product.policy_type == "FAMILY" and attribute in ['cover_premium_table']:
                        continue
                    attr_data = getattr(ComputeScore, score_method)(self, sl, self.data)
                    if {'available', 'verbose', 'importance', 'value', 'score', 'model'}.difference(
                            set(attr_data.keys())):
                        raise Exception("Keys not matching for %s", attribute)

                    attr_data['title'] = scores_to_calculate[
                        "score_%s" % attribute]
                    slab_score[sl].append((attribute, attr_data))

        """ Override the models text_to_display here """
        #hqueries.update_text_to_display(slab_score)
        if slab_score:
            key = self.request.META['PATH_INFO'].split('/')[-2]
            scoring = cache.get(key)
            if scoring:
                scoring.append(slab_score)
            else:
                scoring = [slab_score]
            cache.set(key, scoring, 100)

        slab_score_list = []

        for sl in slab_score.keys():
            insurer = sl.health_product.insurer
            cpt = self.slabs_cpt[sl]

            # Jammu and Kashmir (state-id 15) have a different Tax system
            # if self.data['city'].state.id == 15:
            #     cpremium = round((cpt.premium/1.4) * 1.105)
            # else:
            #     cpremium = cpt.premium

            # Temporary: Same service tax for J&K until it's new service tax is
            # verified
            cpremium = cpt.premium

            slab_score_list.append({
                'extra': self.get_custom_fields(sl),
                'five_year_projection': self.get_five_year_projection(sl),
                'insurer_title': insurer.title,
                'insurer_id': insurer.id,
                'slab_id': sl.id,
                'policy_type': sl.health_product.policy_type,
                'product_title': sl.health_product.title,
                'product_id': sl.health_product.id,
                'total_score': ComputeScore.get_weighted_score(self, slab_score[sl], sl),
                'score': self.clean_score_for_delivery(slab_score[sl]),
                'sum_assured': sl.sum_assured,
                'deductible': sl.deductible,
                'product_type': sl.health_product.coverage_category,
                'premium': cpremium,
                'cpt_id': cpt.id,
                'medical_score': ComputeScore.get_weighted_medical_score(self, sl),
                'special_feature': self.get_special_feature(sl),
                'general': self.get_waiting_period_conditions(sl),
                'ped': {
                    'waiting_period': sl.pre_existing.waiting_period,
                },
                'max_age': self.data['age'],
                'standard_exclusions': {
                    'terms': [
                        {'name': 'War related injuries', 'id': 1},
                        {'name': 'Cosmetic surgery', 'id': 2},
                        {'name': 'Lasik eye treatments', 'id': 3},
                        {'name': 'Drugs or alcohol abuse', 'id': 4},
                        {'name': 'Administratives expenses', 'id': 5},
                        {'name': 'Registration fees', 'id': 6},
                    ]
                },
                'policy_wordings': self.get_policy_wordings_url(sl),
                'transaction_permission': self.get_transaction_permission(sl),
                'recommended': False
            })

        if self.is_topup:
            final_filtered_score = self.sort_topup_plans(slab_score_list)
        else:
            for slab_score in slab_score_list:
                if slab_score['transaction_permission'] == 'ONLINE':
                    slab_score['total_score'] += 100
                else:
                    slab_score['total_score'] -= 100
            final_filtered_score = self.sort_plans(slab_score_list)

        # Manipulate scores
        tscore = 100
        for fp in final_filtered_score:
            fp['total_score'] = tscore
            tscore -= 2

        return final_filtered_score

    def clean_score_for_delivery(self, data):
        if self.debug:
            return data
        pruned_data = []
        [(score_data.pop('score'), score_data.pop('model'), pruned_data.append((attr, score_data))) for attr, score_data in data]
        return pruned_data

    @cache_memoized_method
    def get_waiting_period_conditions(self, slab):
        cache_conditions = dict([(self.term_parent_lookup[c.term_id], c)
                                 for c in slab.general.conditions.all()])

        conditions = []
        wperiod = slab.general.waiting_period
        for parent in self.term_parents:
            wpcondition = cache_conditions.get(parent)
            if wpcondition:
                wperiod_value = wperiod
                if wpcondition.period:
                    wperiod_value = wpcondition.period
                conditions.append({
                    'pterm': {'name': parent.name, 'id': parent.id},
                    # 'term' : {'name' : c.term.name, 'id' : c.term.id},
                    'period': wperiod_value,
                    'limit': wpcondition.limit,
                    'limit_in_percentage': wpcondition.limit_in_percentage,
                    'copay': wpcondition.copay,
                })
            else:
                conditions.append({
                    'pterm': {'name': parent.name, 'id': parent.id},
                    # 'term' : {'name' : c.term.name, 'id' : c.term.id},
                    'period': wperiod,
                    'limit': None,
                    'limit_in_percentage': None,
                    'copay': None,
                })

        return {
            'waiting_period': wperiod,
            'conditions': conditions
        }

    def get_policy_wordings_url(self, slab):
        try:
            return slab.health_product.policy_wordings.url
        except:
            return '#'

    def get_transaction_permission(self, slab):
        if (self.medical_conditions.count() > 0 and
                    slab.health_product.insurer_id in self.NSTP_INSURER_IDS):
            return 'NOT_ALLOWED'
        if self.medical_conditions.filter(global_decline=True).count() > 0:
            return 'NOT_ALLOWED'
        medical_permissions = slab.health_product.medical_permissions.filter(
            medical_condition__in=self.medical_conditions,
        ).order_by('transaction_permission')
        transaction_permission = 'ONLINE'
        for mp in medical_permissions:
            if mp.transaction_permission == 'NOT_ALLOWED':
                transaction_permission = 'NOT_ALLOWED'
                break
            elif mp.transaction_permission == 'MEDICAL_OFFLINE':
                transaction_permission = 'OFFLINE'
        return transaction_permission

    def get_special_feature(self, slab):
        sfs = getattr(slab.special_feature, 'feature', '')
        if self.NO_CI_SLAB(slab):
            sfs = sfs.split('\r\n')
            for i, sf in enumerate(sfs):
                if sf.lower().find('critical illness'):
                    sfs.pop(i)
            sfs = '\n\r'.join(sfs)
        return sfs

    @staticmethod
    @cache_memoized_method
    def get_post_processing_flag(age, slab):
        """
        Returns the status of the plan in case the user wants to buy it.
        Possible cases are as follows:

        BACKEND_NOT_INTEGRATED : Insurer backend is not yet integrated
        MEDICAL_REQUIRED : Medicals are required for this slab at this age
        OFFLINE_COVER_TOO_HIGH : The cover is high and the transaction needs to be taken offline
        OFFLINE_AGE_TOO_HIGH : User age is too high and the transaction needs to be taken offline
        FILL_FORM : The user can proceed with form filling
        """

        if not slab.health_product.is_backend_integrated:
            return {'status': 'BACKEND_NOT_INTEGRATED'}

        # Online Availability Check
        obj = slab.online_availability
        hp = slab.health_product
        # If all slabs have age limit on online availability, then its a age bias
        # If one slab has a age limit on online availability, then its a slab
        # bias
        cover_bias = False
        hps = hp.slabs.filter(sum_assured__gt=0).order_by('sum_assured')
        # using generators
        online_limited_slabs = (sl for sl in hps if not sl.online_availability.is_available_online)
        try:
            online_limited_slab = online_limited_slabs.next()
            if online_limited_slab != hps[0]:
                cover_bias = True
        except StopIteration:
            pass

        if obj.is_available_online:
            #verbose = """You can buy this plan online"""
            pass
        else:
            if age <= obj.maximum_age:
                #verbose = """You can buy this plan online till the age of %s""" % (obj.maximum_age)
                pass
            else:
                if cover_bias:
                    # The plan is not available because the cover is high
                    # verbose = """You can buy this plan online if the cover
                    # value is less than %s. If you wish to buy the this plan,
                    # just <a class="contact-us" href="#">click here</a> and we
                    # will help you buy this plan""" %
                    # (int(lowest_online_slab.sum_assured))
                    return {'status': 'OFFLINE_COVER_TOO_HIGH'}
                else:
                    # The plan is not available because the age is high
                    # verbose = """This plan is not available for an online
                    # purchase. Its easy to buy this plan offline, just <a
                    # class="contact-us" href="#">click here</a> and we will
                    # help you buy this plan"""
                    return {'status': 'OFFLINE_AGE_TOO_HIGH'}

        if slab.medical_required.is_medical_required:
            if slab.medical_required.maximum_age <= age:
                return {'status': 'MEDICAL_REQUIRED'}

        return {'status': 'FILL_FORM'}

    @cache_memoized_method
    def get_five_year_projection(self, slab):
        fdata = self.data.copy()
        fdata['age'] = int(fdata['age']) * 1000
        cpt_cluster = self.cpt_cluster[slab]
        age_premium_list = []
        for cpc in sorted(cpt_cluster, key=attrgetter('lower_age_limit')):
            if not cpc.upper_age_limit:
                upper_age_limit = 100 * 1000
            else:
                upper_age_limit = cpc.upper_age_limit
            if cpc.lower_age_limit < 12 and upper_age_limit < 12:
                age_premium_list.append(
                    [range(cpc.lower_age_limit, upper_age_limit), cpc.premium])

            if cpc.lower_age_limit < 12 and upper_age_limit > 12:
                age_premium_list.append(
                    [range(cpc.lower_age_limit, 12), cpc.premium])
                age_premium_list.append(
                    [range(1000, upper_age_limit + 1000, 1000), cpc.premium])

            if cpc.lower_age_limit > 12:
                age_premium_list.append(
                    [range(cpc.lower_age_limit, upper_age_limit + 1000, 1000), cpc.premium])

        age_premium_sa = []
        for age in range(fdata['age'], fdata['age'] + 5000, 1000):
            [age_premium_sa.append([age / 1000, cpc[1], slab.sum_assured]) for cpc in age_premium_list if age in cpc[0]]
        bonus = slab.bonus
        if bonus.is_available:
            if bonus.bonus_amount.find(",") > 0:
                bonus_amount = [float(i)
                                for i in bonus.bonus_amount.split(",")]
            else:
                bonus_amount = [float(bonus.bonus_amount)]
            year = 1
            yindex = 0
            first_year_sa = age_premium_sa[0][2]
            last_year_sa = first_year_sa
            for ap in age_premium_sa:
                bonus_claimed_year = year - bonus.unclaimed_years
                if bonus_claimed_year > 0:
                    if len(bonus_amount) == 1:
                        bonus_value = first_year_sa * \
                            (bonus_amount[0] / float(100))
                    else:
                        bonus_value = first_year_sa * \
                            (bonus_amount[yindex] / float(100))
                    if bonus.maximum_years:
                        if bonus.maximum_years <= bonus_claimed_year:
                            if len(bonus_amount) == 1:
                                bonus_value = first_year_sa * \
                                    (bonus_amount[0] / float(100)
                                     ) * bonus.maximum_years
                            else:
                                bonus_value = first_year_sa * \
                                    (bonus_amount[yindex] /
                                     float(100)) * bonus.maximum_years
                    if bonus.maximum_bonus_amount:
                        if (last_year_sa + bonus_value - ap[2]) > first_year_sa * float(bonus.maximum_bonus_amount) / 100:
                            bonus_value = 0
                    ap[2] = last_year_sa + bonus_value
                    last_year_sa = ap[2]
                    yindex += 1
                year += 1

        premium_discount = slab.premium_discount
        service_tax = 0.1236
        if premium_discount.is_available:
            year = 1
            if premium_discount.maximum_discount_amount:
                premium_discount_maximum_years = float(
                    premium_discount.maximum_discount_amount) / premium_discount.discount_amount
            else:
                premium_discount_maximum_years = None
            for ap in age_premium_sa:
                premium_claimed_year = year - premium_discount.unclaimed_years
                if premium_claimed_year > 0:
                    premium_value = ap[1] * (1 - service_tax) * (
                        1 - float(premium_discount.discount_amount) / 100) * (1 + service_tax)

                    if premium_discount_maximum_years:
                        if premium_discount_maximum_years < premium_claimed_year:
                            premium_value = ap[1]

                    ap[1] = round(premium_value)
                year += 1
        return age_premium_sa

    def get_custom_fields(self, slab):
        insurer_module = insurer_settings.REPORT_MAP.get(
            slab.health_product.insurer.id)
        try:
            extra_data_module = import_module(
                "health_product.insurer.%s.extra_data" % insurer_module)
        except ImportError:
            return {}
        return extra_data_module.get_extra_data(slab)

    def get_delta_copay_score(self, actual_copay):
        if actual_copay == 0:
            efcopay = 0
        if 0 < actual_copay <= 5:
            efcopay = 20
        if 5 < actual_copay <= 10:
            efcopay = 40
        if 10 < actual_copay <= 20:
            efcopay = 65
        if 20 < actual_copay <= 30:
            efcopay = 85
        if actual_copay > 30:
            efcopay = 100
        return (100 - efcopay) / 5.0

    @cache_memoized_method
    def calculate_copay(self, slab, data):
        obj = slab.copay
        applicable_copays = []

        if not obj.is_available:
            pass

        # Copay applicable at entry age
        if obj.applicable_after_entry_age:
            effective_copay = int(
                max(obj.age_copay_network_hospital, obj.age_copay_non_network_hospital))
            if obj.applicable_after_entry_age > data['age']:
                pass
            else:
                value = "%s%%" % int(effective_copay)
                verbose = 'This plan has a %s%% copay. %s%% of the valid claim amount will need to be paid by you' % (
                    effective_copay, effective_copay)
                result = {'value': value, 'verbose': verbose,
                          'effective_copay': effective_copay}
                applicable_copays.append(["ENTRY_COPAY", result])

        # General copay, if applicable after age 0
        if obj.applicable_after_age == 0:
            effective_copay = int(
                max(obj.age_copay_network_hospital, obj.age_copay_non_network_hospital))
            value = "%s%%" % effective_copay
            verbose = 'This plan has a %s%% copay. %s%% of the valid claim amount will need to be paid by you' % (
                effective_copay, effective_copay)
            result = {'value': value, 'verbose': verbose,
                      'effective_copay': effective_copay}
            applicable_copays.append(["GENERAL_COPAY", result])

        # Age based copay
        if obj.applicable_after_age:
            effective_copay = int(
                max(obj.age_copay_network_hospital, obj.age_copay_non_network_hospital))
            if obj.applicable_after_age > data['age']:
                value = "%s%% after the age of %s" % (
                    effective_copay, obj.applicable_after_age)
                verbose = "This plan has a %s%% copay after the age of %s. Currently there is no copay applicable. Most plans have a copay for older ages." % (
                    effective_copay, obj.applicable_after_age)
                # HACK to add little penalty on copay condition
                effective_copay = 1
                result = {'verbose': verbose, 'value': value,
                          'effective_copay': effective_copay}
                applicable_copays.append(["AGE_DEFERED_COPAY", result])
            else:
                value = "%s%%" % (effective_copay)
                verbose = 'This plan has a %s%% copay. %s%% of the valid claim amount will need to be paid by you' % (
                    effective_copay, effective_copay)
                result = {'verbose': verbose, 'value': value,
                          'effective_copay': effective_copay}
                applicable_copays.append(["AGE_COPAY", result])

        # Non Network or Network Copay condition
        if obj.for_network_hospital == 0 and obj.for_non_network_hospital != 0:
            effective_copay = int(obj.for_non_network_hospital)
            value = "%s%% in non-network hospital" % (effective_copay)
            verbose = "The hospitals that are on the panel of an insurance company are called 'network hospitals'. This plan has a %s%% copay if you take treatment in a non-network hospital" % (
                effective_copay)
            result = {'value': value, 'verbose': verbose,
                      'effective_copay': effective_copay}
            applicable_copays.append(["NONNETWORK_COPAY", result])

        # RoomTypeCopay is scored separately but verbose conditions are put
        # along with copay
        room_type_copay = False

        rtcs = slab.room_type_copay.all()
        if rtcs:
            rtcs = sorted(
                rtcs, key=lambda d: self.ROOM_ORDER.index(d.room_type))
            room_type_copay = True
            if rtcs[-1].copay == rtcs[0].copay:
                value = "%s%% in %s" % (
                    int(rtcs[-1].copay), rtcs[-1].verbose_room_type)
                verbose = "This plan has %s%% copay if you choose a %s or a room more expensive than that. There will be no copay for a room category lower than %s" % (
                    int(rtcs[0].copay), rtcs[0].verbose_room_type, rtcs[0].verbose_room_type)
            else:
                value = "%s%% in %s" % (int(rtcs[0].copay), rtcs[
                    0].verbose_room_type)
                verbose = "This plan has " + ", ".join(["%s%% copay if you choose a %s" % (
                    int(rt.copay), rt.verbose_room_type) for rt in rtcs]) % rtcs[0].verbose_room_type
        else:
            room_type_copay = False

        if room_type_copay:
            effective_copay = int(sum([rtc.copay for rtc in rtcs]) / len(rtcs))
            result = {'value': value, 'verbose': verbose,
                      'effective_copay': effective_copay}
            applicable_copays.append(["ROOM_COPAY", result])

        # zonal copay
        zcp = ZonalCopay.objects.filter(Q(slabs=slab) & (
            Q(state=data['city'].state) | Q(city=data['city'])))
        if zcp:
            zcopays = [
                zc.zonal_copay_for_non_network_hospital for zc in slab.zonal_copay.all()]
            copay_verbose = ""
            if min(zcopays) == max(zcopays):
                copay_verbose = int(max(zcopays))
            else:
                copay_verbose = "%s%%-%s%%" % (int(min(zcopays)),
                                               int(max(zcopays)))
            # User resides in zone where copay is applicable
            zcp = zcp[0]

            # For zonal copay, we assume that copay is always zero, but tell
            # the user that he might have to pay if he goes outside zone
            effective_copay = int(max(
                zcp.zonal_copay_for_network_hospital, zcp.zonal_copay_for_non_network_hospital))
            places = []
            places.extend([s.name for s in zcp.state.all()])
            places.extend([c.name for c in zcp.city.all()])
            value = "%s%% if treatment outside zone"
            verbose = "If you take treatment in %s, there will be a %s copay. This is because treatment costs are higher" \
                      " in these locations and your premium has been calculated basis your current location." % (
                          ",".join(places), copay_verbose)
            result = {'verbose': verbose, 'value': value,
                      'effective_copay': effective_copay}
            applicable_copays.append(["ZONAL_COPAY", result])

        # Check if he is less than 5 years away from copay age and warn him
        obj = slab.copay
        if obj.applicable_after_age and (obj.applicable_after_age - data['age']) <= 5:
            age_copay_factor = (
                                   5 - max(obj.applicable_after_age - data['age'], 0)) / 5

            max_hosp_copay = int(
                max(obj.age_copay_network_hospital, obj.age_copay_non_network_hospital))
            effective_copay = max_hosp_copay * age_copay_factor
            verbose = "This plan has a %s%% copay after the age of %s. Currently there is no copay applicable. Most plans have a copay for older ages." % (
                max_hosp_copay, obj.applicable_after_age)
            value = "0%"
            result = {'verbose': verbose, 'value': value,
                      'effective_copay': effective_copay}
            applicable_copays.append(["AGE_COPAY", result])

        if not applicable_copays:
            applicable_copays.append([
                'NOCOPAY', {
                    'verbose': 'This plan has no co-payment.',
                    'value': "0%",
                    'effective_copay': 0
                }])

        return applicable_copays

    @cache_memoized_method
    def get_room_rent_limit_score(self, limit_value, data):
        affordable_room_type = ""
        if data['city'] in self.METRO_CITIES:
            if limit_value <= 1999:
                score = 0
                affordable_room_type = "CLASSD"

            elif 2000 <= limit_value <= 2999:
                score = 20
                affordable_room_type = "CLASSD"

            elif 3000 <= limit_value <= 4999:
                score = 50
                affordable_room_type = "CLASSC"

            elif 5000 <= limit_value <= 9999:
                score = 85
                affordable_room_type = "CLASSB"

            else:
                score = 95
                affordable_room_type = "CLASSA"

        else:
            if limit_value <= 999:
                score = 0
                affordable_room_type = "CLASSD"

            elif 1000 <= limit_value <= 1999:
                score = 20
                affordable_room_type = "CLASSD"

            elif 2000 <= limit_value <= 2999:
                score = 50
                affordable_room_type = "CLASSC"

            elif 3000 <= limit_value <= 4999:
                score = 80
                affordable_room_type = "CLASSB"

            else:
                score = 95
                affordable_room_type = "CLASSA"

        if data['service_type'] in ['PVT_HIGH_END', 'PVT']:
            if score >= 80:
                score = score
            else:
                score = 0
        else:
            if score >= 50:
                score = score
            else:
                score = 0
        return score, affordable_room_type

    def NO_CI_SLAB(self, slab):
        """No CI add-on for L&T classic plans over 65 years"""
        if slab.health_product_id in [28, 29, 31, 32]:
            return self.data['age'] > 65
        return False
