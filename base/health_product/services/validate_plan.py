from collections import defaultdict

import health_product.health_utils as hutils

from wiki.models import RoomTypeCopay, Slab, AGE_LIMITS_1

ROOM_DISPLAY = dict(RoomTypeCopay.ROOM_TYPE_CHOICES)
HOSPITAL_DISPLAY = dict(RoomTypeCopay.HOSPITAL_TYPE_CHOICES)


class ValidatePlan(object):
    """
    All methods which are to be used for tests will start with
    runtest_<parameter>, DONOT add any method which starts with runtest_
    unless its for testing parameter
    """
    pass

    def __init__(self, plan):
        self.plan = plan
        self.foreign_keys = [sl.name for sl in Slab._meta.fields if getattr(
            sl, 'rel', '') and sl.name not in ['health_product']]

    def get_age_verb(self, age, atype='months'):
        """
        type = months means the age ranges from 0,1,2...12, 1000, 2000, 3000
        type = None means age ranges from 0,1,2,3...100
        """
        if not age:
            return ""
        if atype == 'cpt':
            if age < 0.12:
                return "%s months" % int(age * 100)
            else:
                return "%s years" % int(age)
        if atype == 'months':
            if age < 12:
                return "%s months" % int(age)
            else:
                return "%s years" % int(age / 1000)
        else:
            return "%s years" % int(age)

    def model_required(self, status, obj):
        if not obj:
            status['error'].append({'model': 'Required'})
            return False
        else:
            return True

    def check_required(self, status, obj, *props, **kwargs):
        if 'error' in kwargs:
            error = kwargs['error']
        else:
            error = "Required"

        if 'verbose' in kwargs:
            verbose = kwargs['verbose']
        else:
            verbose = None

        for prop in props:
            if getattr(obj, prop) in [None, '']:
                status['error'].append({prop: error})
                return False
            else:
                if verbose:
                    status['verbose'].append(verbose % getattr(obj, prop))

        return True

    def test(self):
        """
        Returns slab wise error json for plan
        """
        status = {}
        for slb in self.plan.slabs.all():
            if slb.sum_assured == 0:  # for the default slab
                continue
            status[slb.id] = self.test_slab(slb)

        # TODO: Store/Cache this result for future use. This is getting using
        # during scoring for verbose
        return status

    def test_slab(self, slab):
        """
        This is supposed to return a dict with errors and warnings
        {
            "errors": {
                <attribute1>:{'model':<attribute level error>, '<field1_error>' : <error>, '<field2_error>' : <error>}
                <attribute2>:{'model':<attribute level error>, '<field1_error>' : <error>, '<field2_error>' : <error>}
            },
            'warnings': {
                <attribute1>:{'model':<attribute level warning>, '<field1_warning>' : <warning>, '<field2_warning>' : <warning>}
                <attribute2>:{'model':<attribute level warning>, '<field1_warning>' : <warning>, '<field2_warning>' : <warning>}
            },
        }
        """
        test_status = {"errors": {}, "warnings": {}, "verbose": {}}
        for ptest in dir(self):
            if ptest.startswith("runtest_") and callable(getattr(self, ptest)):
                status = {}
                attribute = ptest.replace("runtest_", "")
                if self.plan.policy_type == "INDIVIDUAL" and attribute in ['family_cover_premium_table', 'family_coverage']:
                    continue
                if self.plan.policy_type == "FAMILY" and attribute in ['cover_premium_table']:
                    continue
                if attribute in self.foreign_keys:
                    status = getattr(self, ptest)(
                        slab, getattr(slab, attribute))
                else:
                    status = getattr(self, ptest)(
                        slab, getattr(slab, attribute).all())
                test_status["errors"][attribute] = status['error']
                test_status["warnings"][attribute] = status['warning']
                test_status["verbose"][attribute] = status['verbose']

        return test_status

    def runtest_cover_premium_table(self, slab, cpts):
        status = {'error': [], 'warning': [], 'verbose': []}
        if self.plan.policy_type != "INDIVIDUAL":
            return status

        all_age_list = []
        for a, averb in AGE_LIMITS_1:
            if a < 12:
                all_age_list.append(a / 100.0)
            else:
                all_age_list.append(a / 1000.0)

        # If empty return error
        if not self.model_required(status, cpts):
            return status

        cpt_grp = defaultdict(list)
        for cpt in cpts:
            cities = "|".join([city.name for city in cpt.city.all()])
            states = "|".join([state.name for state in cpt.state.all()])
            if cpt.gender:
                gender = cpt.gender
            else:
                gender = ""
            payment_period = str(cpt.payment_period)
            cpt_grp[
                "-".join([payment_period, gender, states, cities])].append(cpt)

        for grp, cpts in cpt_grp.items():
            age_bands = []
            for cpt in cpts:
                lal = None
                ual = None
                if cpt.lower_age_limit < 12:
                    lal = cpt.lower_age_limit / 100.0
                else:
                    lal = cpt.lower_age_limit / 1000.0

                if cpt.upper_age_limit:
                    if cpt.upper_age_limit < 12:
                        ual = cpt.upper_age_limit / 100.0
                    else:
                        ual = cpt.upper_age_limit / 1000.0
                else:
                    ual = 101
                age_bands.append([lal, ual])

            age_bands = sorted(age_bands, key=lambda x: x[0])
            age_band_range = []

            for ll, ul in age_bands:
                if ll > ul:
                    status['error'].append({'model': "Lower Limits must be smaller than upper limits (%s, %s)" % (
                        self.get_age_verb(ll, atype="cpt"), self.get_age_verb(ul, atype="cpt"))})
                    return status

                current_band = []
                if ll < 0.12:
                    if ul < 0.12:
                        current_band = hutils.incl_range(ll, ul, 0.01)
                    else:
                        current_band = hutils.incl_range(ll, 0.11, 0.01)
                        current_band.extend(hutils.incl_range(1, ul, 1))
                else:
                    current_band = hutils.incl_range(ll, ul, 1)
                age_band_range.append(current_band)

            lowest_age = age_band_range[0][0]
            maximum_age = age_band_range[-1][-1]

            for age in all_age_list:
                if age < lowest_age or age > maximum_age:
                    continue
                matched_bands = [
                    age_band for age_band in age_band_range if age in age_band]
                if len(matched_bands) > 1:
                    status['error'].append({'model': "Overlap with bands %s" % (", ".join(
                        ["-".join([self.get_age_verb(mb[0], atype="cpt"), self.get_age_verb(mb[-1], atype="cpt")]) for mb in matched_bands]))})
                    return status
                if len(matched_bands) == 0:
                    status['error'].append(
                        {'model': "Error with age:%s, not found in the list inspite of proper range" % self.get_age_verb(age, atype="cpt")})
                    return status

        for grp, cpts in cpt_grp.items():
            payment_period, gender, states, cities = grp.split("-")
            for cpt in cpts:
                verbose_line = "%s-%s for payment period %s %s, %s : %s" % (self.get_age_verb(cpt.lower_age_limit, 'months'), self.get_age_verb(
                    cpt.upper_age_limit, 'months'), cpt.payment_period, ", ".join([s.name for s in cpt.state.all()]), ", ".join([c.name for c in cpt.city.all()]), cpt.premium)
                if cpt.two_year_discount:
                    verbose_line += " 2 year discount: %s" % cpt.two_year_discount
                status['verbose'].append(verbose_line)

        return status

    def runtest_family_cover_premium_table(self, slab, cpts):
        status = {'error': [], 'warning': [], 'verbose': []}
        if self.plan.policy_type != "FAMILY":
            return status

        all_age_list = []
        for a, averb in AGE_LIMITS_1:
            if a < 12:
                all_age_list.append(a / 100.0)
            else:
                all_age_list.append(a / 1000.0)

        # If empty return error
        if not self.model_required(status, cpts):
            return status

        cpt_grp = defaultdict(list)
        for cpt in cpts:
            cities = "|".join([city.name for city in cpt.city.all()])
            states = "|".join([state.name for state in cpt.state.all()])
            if cpt.gender:
                gender = cpt.gender
            else:
                gender = ""
            payment_period = str(cpt.payment_period)
            cpt_grp["---".join([str(cpt.adults), str(cpt.kids),
                                payment_period, gender, states, cities])].append(cpt)

        for grp, cpts in cpt_grp.items():
            age_bands = []
            for cpt in cpts:
                lal = None
                ual = None
                if cpt.lower_age_limit < 12:
                    lal = cpt.lower_age_limit / 100.0
                else:
                    lal = cpt.lower_age_limit / 1000.0

                if cpt.upper_age_limit:
                    if cpt.upper_age_limit < 12:
                        ual = cpt.upper_age_limit / 100.0
                    else:
                        ual = cpt.upper_age_limit / 1000.0
                else:
                    ual = 101
                age_bands.append([lal, ual])

            age_bands = sorted(age_bands, key=lambda x: x[0])
            age_band_range = []

            for ll, ul in age_bands:
                if ll > ul:
                    status['error'].append({'model': "Lower Limits must be smaller than upper limits (%s, %s)" % (
                        self.get_age_verb(ll, atype="cpt"), self.get_age_verb(ul, atype="cpt"))})
                    return status

                current_band = []
                if ll < 0.12:
                    if ul < 0.12:
                        current_band = hutils.incl_range(ll, ul, 0.01)
                    else:
                        current_band = hutils.incl_range(ll, 0.11, 0.01)
                        current_band.extend(hutils.incl_range(1, ul, 1))
                else:
                    current_band = hutils.incl_range(ll, ul, 1)
                age_band_range.append(current_band)

            lowest_age = age_band_range[0][0]
            maximum_age = age_band_range[-1][-1]

            for age in all_age_list:
                if age < lowest_age or age > maximum_age:
                    continue
                matched_bands = [
                    age_band for age_band in age_band_range if age in age_band]
                if len(matched_bands) > 1:
                    status['error'].append({'model': "Overlap with bands %s" % (", ".join(
                        ["-".join([self.get_age_verb(mb[0], atype="cpt"), self.get_age_verb(mb[-1], atype="cpt")]) for mb in matched_bands]))})
                    return status
                if len(matched_bands) == 0:
                    status['error'].append(
                        {'model': "Error with age:%s, not found in the list inspite of proper range" % self.get_age_verb(age, atype="cpt")})
                    return status

        for grp, cpts in cpt_grp.items():
            adults, kids, payment_period, gender, states, cities = grp.split(
                "---")
            for cpt in cpts:
                verbose_line = "%s-%s , %s-%s for payment period %s %s, %s : %s" % (str(cpt.adults), str(cpt.kids), self.get_age_verb(cpt.lower_age_limit, 'months'), self.get_age_verb(
                    cpt.upper_age_limit, 'months'), cpt.payment_period, ", ".join([s.name for s in cpt.state.all()]), ", ".join([c.name for c in cpt.city.all()]), cpt.premium)
                if cpt.two_year_discount:
                    verbose_line += " 2 year discount: %s" % cpt.two_year_discount
                status['verbose'].append(verbose_line)

        return status

    def runtest_online_availability(self, slab, obj):
        """ Required field, If is_available_online, then
        YES : Means its available online for all ages
        NO: You must specify an age above which this becomes offline
        """
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'is_available_online'):
            return status

        if not obj.is_available_online:
            if not self.check_required(
                    status, obj, 'maximum_age',
                    error='Required, need to specify age above which its only available offline',
                    verbose="Slab is only available offline for ages above %s"
            ):
                return status
        else:
            status['verbose'].append("Slab is available online for all ages")

        return status

    def runtest_medical_required(self, slab, obj):
        """ Required field, If is_medical_required, then
        YES : Means its required for age specified in maximum_age
        NO: No medicals are required for any age
        """
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'is_medical_required'):
            return status

        if not obj.is_medical_required:
            if not self.check_required(
                    status, obj, 'maximum_age',
                    error='Required, need to specify age above which medicals are required',
                    verbose="Medicals are required above the age of %s"
            ):
                return status
        else:
            status['verbose'].append("No medical checkups are required")

        return status

    def runtest_age_limit(self, slab, obj):
        """Min. age must be specified, 0 if not given (new born baby cover)
        Max age if not specified, no max age limit, else, specified limit
        """
        status = {'error': [], 'warning': [], 'verbose': []}
        if not obj:
            status['verbose'].append(
                "Slab is open for all ages, No minimum or maximum age specified")
        else:
            if not self.check_required(status, obj, 'minimum_entry_age'):
                return status
            else:
                status['verbose'].append("Minimum entry age for this slab is %s" % self.get_age_verb(
                    obj.minimum_entry_age, atype="months"))

            if obj.maximum_entry_age:
                status['verbose'].append("Slab needs person to be of not more than %s old" % self.get_age_verb(
                    obj.maximum_entry_age, atype="months"))
            else:
                status['verbose'].append(
                    "Slab is available for person of any maximum age")
        return status

    def runtest_special_feature(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not obj:
            return status
        if not self.check_required(status, obj, 'feature'):
            return status
        else:
            status['verbose'].append(obj.feature)
            return status

    def runtest_family_coverage(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}

        if self.plan.policy_type != "FAMILY":
            return status

        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'number_of_maximum_members', verbose="Maximum number of members is %s"):
            return status

        if not self.check_required(status, obj, 'maximum_dependent_age', verbose="Maximum dependent age is %s"):
            return status

        return status

    def runtest_general(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}

        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'waiting_period', verbose='General waiting period for diseases is %s months'):
            return status

        if obj.conditions.all():
            disease_condition = defaultdict(list)
            for c in obj.conditions.all():
                disease_condition[
                    "%s-%s-%s-%s" % (c.period, c.limit, c.limit_in_percentage, c.copay)].append(c.term.name)
            for cond, terms in disease_condition.items():
                period, limit, limit_in_percentage, copay = cond.split("-")
                if period != 'None':
                    status['verbose'].append(
                        "Waiting period of %s months on diseases: %s" % (period, " , ".join(terms)))
                if limit != 'None':
                    status['verbose'].append(
                        "A limit of Rs %s on diseases: %s" % (limit, " , ".join(terms)))
                if limit_in_percentage != 'None':
                    status['verbose'].append("Limit of %s%% of SA on diseases: %s" % (
                        limit_in_percentage, " , ".join(terms)))
                if copay != 'None':
                    status['verbose'].append(
                        "Copay of %s%% on diseases: %s" % (copay, " , ".join(terms)))

        return status

    def runtest_pre_existing(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}

        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'waiting_period', verbose='General waiting period for diseases is %s months'):
            return status

        if obj.conditions.all():
            disease_condition = defaultdict(list)
            for c in obj.conditions.all():
                disease_condition[
                    "%s-%s-%s-%s" % (c.period, c.limit, c.limit_in_percentage, c.copay)].append(c.term.name)
            for cond, terms in disease_condition.items():
                period, limit, limit_in_percentage, copay = cond.split("-")
                if period != 'None':
                    status['verbose'].append(
                        "PED Waiting period of %s months on diseases: %s" % (period, " , ".join(terms)))
                if limit != 'None':
                    status['verbose'].append(
                        "A limit of Rs %s on PED: %s" % (limit, " , ".join(terms)))
                if limit_in_percentage != 'None':
                    status['verbose'].append("Limit of %s%% of SA on PED: %s" % (
                        limit_in_percentage, " , ".join(terms)))
                if copay != 'None':
                    status['verbose'].append(
                        "Copay of %s%% on PED: %s" % (copay, " , ".join(terms)))

        return status

    def runtest_maternity_cover(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}

        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'is_covered'):
            return status

        if not obj.is_covered:
            status['verbose'].append("Maternity cover is not provided")

        if obj.is_covered:
            self.check_required(status, obj, 'waiting_period',
                                verbose='Cover starts only after %s years')
            self.check_required(status, obj, 'limit_for_normal_delivery',
                                verbose='Limit for normal delivery is Rs %s')
            self.check_required(status, obj, 'limit_for_cesarean_delivery',
                                verbose='Limit for cesarean delivery is Rs %s')
            if obj.delivery_limit:
                status['verbose'].append(
                    "Valid only for %s deliveries" % obj.delivery_limit)
            else:
                status['verbose'].append("No limit on number of deliveries")

        return status

    def runtest_standard_exclusion(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}

        if not self.model_required(status, obj):
            return status

        if not obj.terms.all():
            status['error'].append({'term': "Required"})
            return status

        status['verbose'].append("%s" % " , ".join(
            [t.name for t in obj.terms.all()]))

        return status

    def runtest_life_long_renewability(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}

        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'condition'):
            return status

        if obj.condition in ['YES']:
            status['verbose'].append("Lifelong renewable")
        if obj.condition in ['NO']:
            if not self.check_required(status, obj, 'age_limit', error="Required, since slab is not lifelong renewable"):
                return status
            if obj.age_limit:
                status['verbose'].append(
                    "Plan is renewable upto age %syears" % obj.age_limit)

        return status

    def runtest_ambulance_charges(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}

        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'condition'):
            return status

        if obj.condition == "NO":
            status['verbose'].append("Ambulance charges are not covered")

        if obj.condition == "EMERGENCY_ONLY":
            status['verbose'].append(
                "Ambulance charges covered for emergency cases only")

        if obj.condition == "YES":
            status['verbose'].append("Ambulance charges covered for all cases")

        if obj.condition in ["YES", "EMERGENCY_ONLY"]:
            if not self.check_required(status, obj, 'is_ambulance_services_covered',
                                       error="If not mentioned, ambulance services are covered, mark Yes",
                                       verbose="Third party ambulance services are covered - %s",
                                       ):
                return status

            if obj.limit_in_network_hospitals or obj.limit_in_non_network_hospitals:
                status['verbose'].append(
                    "Ambulance charges limited in network hositals to Rs %s" % obj.limit_in_network_hospitals)
                status['verbose'].append(
                    "Ambulance charges limited in non-network hositals to Rs %s" % obj.limit_in_non_network_hospitals)
        return status

    def runtest_alternative_practices_coverage(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not obj:
            status['verbose'].append(
                "No alternative medical practices are covered")
            return status
        if not obj.alternative_practices.all():
            status['verbose'].append(
                "No alternative medical practices are covered")
            return status

        if obj:
            if not obj.limit_in_percentage and not obj.limit:
                status['error'].append(
                    {'model': 'at least one amongst limit or percentage limit is required'})
                return status
            if obj.limit:
                status['verbose'].append("%s are covered upto Rs %s" % (
                    ", ".join([a.name for a in obj.alternative_practices.all()]), obj.limit))
            if obj.limit_in_percentage:
                status['verbose'].append("%s are covered upto %s%% of sum assured" % (", ".join(
                    [a.name for a in obj.alternative_practices.all()]), obj.limit_in_percentage))

        return status

    def runtest_domiciliary_hospitalization(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'is_covered'):
            return status

        if not obj.is_covered:
            status['verbose'].append(
                "Domiciliary hospitalization is not covered")
        else:
            if not obj.limit and not obj.limit_in_percentage:
                status['error'].append(
                    {'model': 'at least one amongst limit or percentage limit is required'})
                return status

            if obj.limit:
                if obj.limit == 0:
                    status['verbose'].append(
                        "Covered upto unspecified reasonable limits")
                else:
                    status['verbose'].append("Covered upto Rs %s" % obj.limit)

            if obj.limit_in_percentage:
                status['verbose'].append(
                    "Covered upto %s%% of sum assured" % obj.limit_in_percentage)

        return status

    def runtest_outpatient_benefits(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'is_covered'):
            return status

        if not obj.is_covered:
            status['verbose'].append("Outpatient benefits are not covered")
        else:
            if not self.check_required(status, obj, 'limit',
                                       error="Limit is required if OPD is covered",
                                       verbose="Outpatient benefits is covered upto Rs %s"
                                       ):
                return status

            if obj.number_of_consultations:
                status['verbose'].append(
                    "Number of consultations is limited to %s times" % obj.number_of_consultations)
            else:
                status['verbose'].append(
                    "There is no limit on number of consultations")

        return status

    def runtest_organ_donor(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'is_covered'):
            return status

        if not obj.is_covered:
            status['verbose'].append("Organ donors cover is not available")
        else:
            if not obj.limit and not obj.limit_in_percentage:
                status['error'].append(
                    {'limit': "Required at least the limit of the limit percentage"})
                return status
            if obj.limit:
                status['verbose'].append(
                    "Organ transplant procedure is covered upto Rs %s" % obj.limit)
            if obj.limit_in_percentage:
                status['verbose'].append(
                    "Organ transplant procedure is covered upto %s%% of sum assured" % obj.limit_in_percentage)
        return status

    def runtest_bonus(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'is_available'):
            return status

        if not obj.is_available:
            status['verbose'].append("Bonus is not available")
        else:
            if not self.check_required(status, obj, 'unclaimed_years', 'bonus_amount', 'reduction_in_bonus_amount_after_claim'):
                return status
            if not obj.maximum_bonus_amount and not obj.maximum_years:
                status['error'].append(
                    {'maximum_bonus_amount': "Either maximum bonus amount or maximum years must be entered"})
                return status

            status['verbose'].append(
                "Bonus is available after %s claim free years" % obj.unclaimed_years)
            status['verbose'].append(
                "Bonus will increase by %s%% of base sum assured every claim free year" % obj.bonus_amount)
            status['verbose'].append(
                "There will be a reduction of %s%% of base sum assured once claim is made" % obj.reduction_in_bonus_amount_after_claim)
            if obj.maximum_bonus_amount:
                status['verbose'].append(
                    "Maximum bonus is limited to %s%% of base sum assured" % obj.maximum_bonus_amount)
            if obj.maximum_years:
                status['verbose'].append(
                    "Bonus will be given for a maximum of %s years" % obj.maximum_years)

        return status

    def runtest_premium_discount(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'is_available'):
            return status

        if not obj.is_available:
            status['verbose'].append("Premium discount is not available")
        else:
            if not self.check_required(status, obj, 'unclaimed_years', 'discount_amount'):
                return status
            if not obj.maximum_discount_amount and not obj.maximum_years:
                status['error'].append(
                    {'maximum_discount_amount': "Either maximum discount amount or maximum years must be entered"})
                return status

            status['verbose'].append(
                "Discount is available after %s claim free years" % obj.unclaimed_years)
            status['verbose'].append(
                "Discount will be %s%% of premium" % obj.discount_amount)
            if obj.reduction_in_discount_amount_after_claim:
                status['verbose'].append(
                    "There will be a reduction of %s%% in discount once claim is made" % obj.reduction_in_discount_amount_after_claim)
            if obj.maximum_discount_amount:
                status['verbose'].append(
                    "Maximum discount is limited to %s%%" % obj.maximum_discount_amount)
            if obj.maximum_years:
                status['verbose'].append(
                    "Discount will be given for a maximum of %s years" % obj.maximum_years)

        return status

    def runtest_convalescence_benefit(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'condition'):
            return status

        if obj.condition == "NO":
            status['verbose'].append("Convalescence benefit is not available")
            return status
        else:
            if not self.check_required(status, obj, 'minimum_days_of_hospitalization'):
                return status

            if obj.condition == "AVAILABLE_PER_DAY":
                status['verbose'].append(
                    "Convalescence benefit is available proportional to number of days in hospital")
                if not self.check_required(status, obj, 'amount_per_day',
                                           error="Required, if Convalescence is available on a per day basis",
                                           verbose="Convalescence benefit of Rs %s will be given per day of hospitalization"
                                           ):
                    return status
            if obj.condition == "AVAILABLE_LUMPSUM":
                status['verbose'].append(
                    "Convalescence benefit is available lump-sum after hospitalization")
                if not self.check_required(status, obj, 'limit',
                                           error="Required, if Convalescence is available lump sum",
                                           verbose="Convalescence benefit of Rs %s will be given lumpsum"
                                           ):
                    return status
            status['verbose'].append(
                "Convalescence benefit will be available if hospitalization exceeds %s days" % obj.minimum_days_of_hospitalization)
            if obj.maximum_number_of_days:
                status['verbose'].append(
                    "Convalescence benefit is available for upto %s days of hospitalization" % obj.maximum_number_of_days)

        return status

    def runtest_pre_post_hospitalization(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'is_available'):
            return status

        if not obj.is_available:
            status['verbose'].append(
                "There are no pre-post hospitalization benefits")
        else:
            if not self.check_required(status, obj, 'days_pre_hospitalization', 'days_post_hospitalization'):
                return status
            else:
                status['verbose'].append(
                    "Covers expenses %s days pre-hospitalization" % obj.days_pre_hospitalization)
                status['verbose'].append(
                    "Covers expenses %s days post-hospitalization" % obj.days_post_hospitalization)
            if obj.intimation_time:
                if not self.check_required(status, obj, 'days_pre_hospitalization_with_intimation', 'days_post_hospitalization_with_intimation'):
                    return status
                else:
                    status['verbose'].append("Covers expenses %s days pre-hospitalization if intimated %s days in advance" % (
                        obj.days_pre_hospitalization, obj.intimation_time))
                    status['verbose'].append("Covers expenses %s days post-hospitalization if intimated %s days in advance" % (
                        obj.days_post_hospitalization, obj.intimation_time))

            if not obj.cummulative_limit and not obj.cummulative_limit_in_percentage:
                status['verbose'].append("Covered up to sum assured")
            if obj.cummulative_limit:
                status['verbose'].append(
                    "Covered up to Rs %s" % obj.cummulative_limit)
            if obj.cummulative_limit_in_percentage:
                status['verbose'].append(
                    "Covered up to %s%% of sum assured" % obj.cummulative_limit_in_percentage)

        return status

    def runtest_copay(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'is_available'):
            return status

        if not obj.is_available:
            status['verbose'].append(
                "There is no copay option for this product")
        else:
            if not obj.applicable_after_age:
                status['verbose'].append(
                    "There is no age based copay for this product")
            else:
                if obj.applicable_after_age == 0:
                    status['verbose'].append("A general copay of %s%% is applicable on network hospital and %s%% on non-network hospital" % (
                        obj.age_copay_network_hospital, obj.age_copay_non_network_hospital))
                else:
                    status['verbose'].append("A copay of %s%% is applicable on network hospital and %s%% on non-network hospital after the age of %s" % (
                        obj.age_copay_network_hospital, obj.age_copay_non_network_hospital, obj.applicable_after_entry_age))

        if obj.for_non_network_hospital:
            status['verbose'].append(
                "A copay of %s is available on non network hospitals" % obj.for_non_network_hospital)

        if obj.on_day_care_for_network_hospital and obj.on_day_care_for_network_hospital != 0:
            status['verbose'].append(
                "A copay of %s%% is available on network hospitals for day care procedures" % obj.on_day_care_for_network_hospital)
        if obj.on_day_care_for_non_network_hospital and obj.on_day_care_for_non_network_hospital != 0:
            status['verbose'].append(
                "A copay of %s%% is available on non-network hospitals for day care procedures" % obj.on_day_care_for_network_hospital)

        return status

    def runtest_room_type_copay(self, slab, objs):
        status = {'error': [], 'warning': [], 'verbose': []}

        if not objs:
            status['verbose'].append(
                "No room type copay applicable for this plan")
            return status

        for obj in objs:
            if not self.check_required(status, obj, 'room_type'):
                return status
            if obj.copay and not obj.applicable_for_hospital:
                status['verbose'].append("%s room is available with copay of %s%" % (
                    ROOM_DISPLAY[obj.room_type], obj.copay))
            if obj.copay and obj.applicable_for_hospital:
                status['verbose'].append("%s room is available with copay of %s% in %s" % (
                    ROOM_DISPLAY[obj.room_type], obj.copay, HOSPITAL_DISPLAY[obj.applicable_for_hospital]))
            if not obj.copay and obj.applicable_for_hospital:
                status['verbose'].append("%s room is available in %s" % (
                    ROOM_DISPLAY[obj.room_type], HOSPITAL_DISPLAY[obj.applicable_for_hospital]))
            if not obj.copay and not obj.applicable_for_hospital:
                status['verbose'].append(
                    "%s room is available" % (ROOM_DISPLAY[obj.room_type]))

        return status

    def runtest_zonal_room_rent(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        zrr_place = {}
        for zrr in slab.zonal_room_rent.all():
            for state in zrr.state.all():
                if zrr_place.get(state):
                    status['verbose'].append(
                        "Multiple room-rent values found for state: %s" % state.name)
                    return status
                zrr_place[state] = zrr
            for city in zrr.city.all():
                if zrr_place.get(city):
                    status['verbose'].append(
                        "Multiple room-rent values found for city: %s" % city.name)
                    print zrr.id
                    return status
                zrr_place[city] = zrr

        for obj in slab.zonal_room_rent.all():
            if obj.state.all():
                status['verbose'].append("States: %s" % ",".join(
                    [s.name for s in obj.state.all()]))

            if obj.city.all():
                status['verbose'].append("Cities: %s" % ",".join(
                    [c.name for c in obj.city.all()]))

            if not self.check_required(status, obj, 'conditions'):
                return status

            if obj.conditions == 'NO_LIMIT':
                status['verbose'].append("There is no limit on room rent")
                return status
            elif obj.conditions == 'SA_OR_VALUE_LIMIT':
                if not obj.limit_daily and not obj.limit_daily_in_percentage:
                    status['error'].append(
                        {'limit_daily': "Required, specify daily limit in either value or percentage of sum assured"})
                else:
                    if obj.limit_daily:
                        status['verbose'].append(
                            "Daily room-rent limit is Rs %s" % obj.limit_daily)
                    if obj.limit_daily_in_percentage:
                        status['verbose'].append(
                            "Daily room-rent limit is %s%% of sum assured" % obj.limit_daily_in_percentage)

                if not obj.limit_daily_in_icu and not obj.limit_daily_in_percentage_in_icu:
                    status['error'].append(
                        {'limit_daily_in_icu': "Required, specify daily ICU limit in either value or percentage of sum assured"})
                else:
                    if obj.limit_daily_in_icu:
                        status['verbose'].append(
                            "Daily ICU room-rent limit is Rs %s" % obj.limit_daily_in_icu)
                    if obj.limit_daily_in_percentage_in_icu:
                        status['verbose'].append(
                            "Daily ICU room-rent limit is %s%% of sum assured" % obj.limit_daily_in_percentage_in_icu)

                if obj.limit_maximum_total:
                    status['verbose'].append(
                        "Daily room-rent limit is subjected to a cummulative maximum of Rs %s" % obj.limit_maximum_total)
                else:
                    status['verbose'].append(
                        "No limit on cummulative maximum room-rent")
                if obj.limit_maximum_total_in_icu:
                    status['verbose'].append(
                        "Daily ICU room-rent limit is subjected to a maximum of Rs %s" % obj.limit_maximum_total_in_icu)
                else:
                    status['verbose'].append(
                        "No limit on cummulative maximum room-rent in ICU")
            else:
                status['verbose'].append('Only %s available = %s' % (
                    obj.room_available, obj.conditions))

            return status

    def runtest_health_checkup(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'is_available'):
            return status

        if not obj.is_available:
            status['verbose'].append("Free health checkup is not available")
            return status

        if obj.is_available:
            if obj.claim_free_years:
                status['verbose'].append(
                    "Free health checkup is available after %s claim free years" % obj.claim_free_years)
            else:
                status['verbose'].append("Free health checkup is available")

            if not obj.limit and not obj.limit_in_percentage:
                status['error'].append(
                    {'limit': "Required either limit on checkup by value or percentage of sum assured"})
                return status
            else:
                if obj.limit:
                    status['verbose'].append(
                        "Free health checkup is limited to Rs %s" % obj.limit)
                if obj.limit_in_percentage:
                    status['verbose'].append(
                        "Free health checkup is limited to %s%% of sum assured" % obj.limit_in_percentage)
        return status

    def runtest_claims(self, slab, obj):
        # This is calculated based on ClaimsData model of the Insurer, not the
        # claims models
        status = {'error': [], 'warning': [], 'verbose': []}

        claims_data = slab.health_product.insurer.claims_data
        if not claims_data:
            status['error'].append(
                {'model': "Enter claims data for the insurer %s" % slab.health_product.insurer.title})
            return status

        else:
            status['verbose'].append(
                'Rejection Ratio : %s%%' % claims_data.rejection_ratio)
            status['verbose'].append(
                'Claims Response Time : %s%%' % claims_data.claims_response_time)
            return status

    def runtest_cancellation_policy(self, slab, objs):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not len(list(objs)):
            status['error'].append("Required field")
            return status

        for obj in objs:
            if not self.check_required(status, obj, 'policy_years', 'period', 'refund_in_percentage'):
                return status
            else:
                status['verbose'].append('Premium paid for : %s years, Cancellation after : %s months, Refund : %s%%' % (
                    obj.policy_years, obj.period, obj.refund_in_percentage))

        return status

    def runtest_critical_illness_coverage(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'is_available'):
            return status

        if not obj.is_available:
            status['verbose'].append("Critical illness cover is not available")
            return status
        else:
            if not self.check_required(status, obj, 'included_in_plan'):
                return status
            else:
                if obj.included_in_plan:
                    status['verbose'].append(
                        "Critical illness cover is included under sum assured limit")
                else:
                    status['verbose'].append(
                        "Critical illness cover is over and above sum assured")

            if not obj.limit and not obj.limit_in_percentage:
                status['error'].append(
                    {'limit': "Required either limit on checkup by value or percentage of sum assured"})
                return status
            else:
                if obj.limit:
                    status['verbose'].append(
                        "Critical illness cover is Rs %s" % obj.limit)
                if obj.limit_in_percentage:
                    status['verbose'].append(
                        "Critical illness cover is %s%% of sum assured" % obj.limit_in_percentage)

        if obj.terms:
            status['verbose'].append("Critical illness cover is available for %s" % ", ".join(
                [t.name for t in obj.terms.all()]))

        return status

    def runtest_restore_benefits(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'is_available'):
            return status

        if not obj.is_available:
            status['verbose'].append("No restoration benefits available")
            return status
        else:
            if not obj.amount_restored and not obj.amount_restored_percentage:
                status['error'].append(
                    {'amount_restored': "Required the amount to be restored in value or percentage of sum assured"})
                return status
            if obj.amount_restored:
                status['verbose'].append(
                    "After claim, the sum assured will be restored to Rs %s" % obj.amount_restored)
            if obj.amount_restored_percentage:
                status['verbose'].append(
                    "After claim, the sum assured will be restored to %s%% of sum assured " % obj.amount_restored_percentage)
        return status

    def runtest_day_care(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'is_covered'):
            return status

        if not obj.is_covered:
            status['verbose'].append("Day care is not covered")
            return status
        else:
            if not self.check_required(status, obj, 'defined_list'):
                return status
            conditions = "Conditions covered:\n"

            verb_term = defaultdict(list)
            for pc in obj.procedure_covered.all():
                verb_term[pc.verbose].append(pc.term.name)

            for verbose, tlist in verb_term.items():
                conditions += "%s : %s\n" % (verbose, ",".join(tlist))
            status['verbose'].append(conditions)
        return status

    def runtest_dental_coverage(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'condition'):
            return status

        if obj.condition == "NO":
            status['verbose'].append("Dental care is not covered")
            return status
        else:
            if obj.exclusions:
                status['verbose'].append("Dental treatment is covered with following exclusions: %s" % (
                    ", ".join([e.name for e in obj.exclusions.all()])))

            if not self.check_required(status, obj, 'limit', 'waiting_period'):
                return status
            else:
                status['verbose'].append("Dental treatment is covered upto a limit of Rs %s and a waiting period of %s" % (
                    obj.limit, obj.waiting_period))

            if obj.copay:
                status['verbose'].append(
                    "A copay of %s%% is applicable for dental coverage" % obj.copay)

        return status

    def runtest_eye_coverage(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'condition'):
            return status

        if obj.condition == "NO":
            status['verbose'].append("Eye care is not covered")
            return status
        else:
            if obj.exclusions:
                status['verbose'].append("Eye treatment is covered with following exclusions: %s" % (
                    ", ".join(obj.exclusions.all())))

            if not self.check_required(status, obj, 'limit', 'waiting_period'):
                return status
            else:
                status['verbose'].append("Eye treatment is covered with following exclusions: %s" % (
                    ", ".join([e.name for e in obj.exclusions.all()])))

            if obj.copay:
                status['verbose'].append(
                    "A copay of %s%% is applicable for eye coverage" % obj.copay)

        return status

    def runtest_network_hospital_daily_cash(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'is_available'):
            return status

        if not obj.is_available:
            status['verbose'].append(
                "Hospital Cash for network hospitals is not available")
            return status

        if not self.check_required(status, obj, 'condition', 'is_icu_available'):
            return status

        if obj.condition == 'NO':
            status['verbose'].append(
                "Hospital Cash for network hospitals is not available")
            return status

        if obj.condition == 'AVAILABLE_WITH_SHARING_ONLY':
            status['verbose'].append(
                "Hospital Cash for network hospitals is available for shared rooms only")

        if obj.condition == 'YESWL':
            status['verbose'].append(
                "Hospital Cash for network hospitals is available with upper limits")

        if not self.check_required(status, obj, 'limit_per_day_allowance',
                                   verbose="Hospital cash is available upto Rs %s"
                                   ):
            return status

        if not self.check_required(status, obj, 'is_icu_available'):
            return status
        else:
            if obj.is_icu_available:
                if not self.check_required(status, obj, 'limit_per_day_allowance_icu',
                                           verbose="Hospital cash is also available for ICU upto Rs %s"
                                           ):
                    return status

        if obj.limit_cummulative_allowance:
            status['verbose'].append(
                "Hospital Cash for network hospitals is limited to a cummulative limit of Rs %s" % obj.limit_cummulative_allowance)
        if obj.from_day:
            status['verbose'].append(
                "Hospital Cash for network hospitals starts from %s day of hospitalization" % obj.from_day)
        if obj.to_day:
            status['verbose'].append(
                "Hospital Cash for network hospitals is available upto %s day of hospitalization" % obj.to_day)
        if obj.number_of_days:
            status['verbose'].append(
                "Hospital Cash for network hospitals is available for %s days of hospitalization" % obj.number_of_days)

        return status

    def runtest_non_network_hospital_daily_cash(self, slab, obj):
        status = {'error': [], 'warning': [], 'verbose': []}
        if not self.model_required(status, obj):
            return status

        if not self.check_required(status, obj, 'is_available'):
            return status

        if not obj.is_available:
            status['verbose'].append(
                "Hospital Cash for non-network hospitals is not available")
            return status

        if not self.check_required(status, obj, 'condition', 'is_icu_available'):
            return status

        if obj.condition == 'NO':
            status['verbose'].append(
                "Hospital Cash for non-network hospitals is not available")
            return status

        if obj.condition == 'AVAILABLE_WITH_SHARING_ONLY':
            status['verbose'].append(
                "Hospital Cash for non-network hospitals is available for shared rooms only")

        if obj.condition == 'YESWL':
            status['verbose'].append(
                "Hospital Cash for non-network hospitals is available with upper limits")

        if not self.check_required(status, obj, 'limit_per_day_allowance',
                                   verbose="Hospital cash is available upto Rs %s"
                                   ):
            return status

        if not self.check_required(status, obj, 'is_icu_available'):
            return status
        else:
            if obj.is_icu_available:
                if not self.check_required(status, obj, 'limit_per_day_allowance_icu',
                                           verbose="Hospital cash is also available for ICU upto Rs %s"
                                           ):
                    return status

        if obj.limit_cummulative_allowance:
            status['verbose'].append(
                "Hospital Cash for non-network hospitals is limited to a cummulative limit of Rs %s" % obj.limit_cummulative_allowance)
        if obj.from_day:
            status['verbose'].append(
                "Hospital Cash for non-network hospitals starts from %s day of hospitalization" % obj.from_day)
        if obj.to_day:
            status['verbose'].append(
                "Hospital Cash for non-network hospitals is available upto %s day of hospitalization" % obj.to_day)
        if obj.number_of_days:
            status['verbose'].append(
                "Hospital Cash for non-network hospitals is available for %s days of hospitalization" % obj.number_of_days)

        return status
