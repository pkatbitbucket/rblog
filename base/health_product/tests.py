from django.test import TestCase
from django.test import Client


class HealthTestCase(TestCase):
    fixtures = ["motor_product/fixtures/insurer.json"]

    def setUp(self):
        self.c = Client()

    def test_health_page_opens(self):
        r = self.c.get("/health-plan/")
        assert r.status_code == 200
