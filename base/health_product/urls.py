from importlib import import_module

from django.conf.urls import include, url

from health_product.insurer.settings import INSURER_MAP

from . import views

urlpatterns = [
    url(r'^(insurer/(?P<insurer_slug>[\w-]+)/)?(plan/(?P<product_type>[\w-]+)/)?((?P<results>results)/)?$',
        views.desktopv2, name='desktopv2'),

    # # Justdial ##
    url(r'^jd/(insurer/(?P<insurer_slug>[\w-]+)/)?(plan/(?P<product_type>[\w-]+)/)?((?P<results>results)/)?$',
        views.desktopv2, {'tparty': 'jd'}, name='desktopv2_jd'),

    url(r'^get-ui-data/$', views.get_ui_data, name='get_ui_data'),
    url(r'^get-medical-conditions/$', views.get_medical_conditions, name='get_medical_conditions'),
    url(r'^save-details/(?P<activity_type>[\w-]+)/$', views.save_details, name='save_details'),
    url(r'^get-details/(?P<activity_type>[\w-]+)/$', views.get_details, name='get_details'),

    url(r'^notify-premium-change', views.notify_premium_change, name='notify_premium_change'),
    url(r'^agree-premium-change', views.agree_premium_change, name='agree_premium_change'),

    url(r'^get-score/(?P<activity_id>[\w-]+)/$', views.health_product_score, name='health_product_score'),
    url(r'^get-score-for-plan/$', views.health_score_for_plan, name='health_score_for_plan'),
    # url(r'^initialize-results/$', 'health_product_initialize_results', name='health_product_initialize_results'),

    url(r'^buy/(?P<transaction_id>\w+)/?$', views.buy, name='buy'),
    url(r'^view/(?P<transaction_id>\w+)/$', views.buy, {'static_mode': True}, name='view_buy_form'),
    url(r'^buy-form-download/(?P<transaction_id>\w+)/?$', views.download_buy_form, name='download_buy_form'),
    url(r'^fetch-sum-assured/$', views.fetch_sum_assured, name='fetch_sum_assured'),
    url(r'^hospitals/by-pincode/$', views.hospitals_by_pincode, name='hospitals_by_pincode'),
    url(r'^hospitals/auto-complete/$', views.hospitals_auto_complete, name='hospitals_auto_complete'),

    # TODO: Delete older save_transaction
    # url(r'^transaction/save/$', 'save_transaction', name='save_transaction'),
    url(r'^transaction/stage/save/$', views.save_transaction_stage, name='save_stage'),
    url(r'^transaction/(?P<transaction_id>\w+)/final/$', views.transaction_final, name='transaction_final'),

    url(r'^transaction/get-data/$', views.get_transaction_data, name='health_get_transaction_data'),
    url(r'^to-medical/get-data/$', views.get_to_medical_data, name='health_get_to_medical_data'),
    url(r'^save-call-time/$', views.save_call_time, name='save_call_time'),
    url(r'^amlpl/save-number/$', views.amlpl_save_number, name='amlpl_save_number'),
    url(r'^amlpl/fetch-number/$', views.amlpl_fetch_number, name='amlpl_fetch_number'),

    url(r'^transaction/create/$', views.create_transaction, name='create_transaction'),
    url(r'^initiate/transaction/(?P<transaction_id>\w+)/$', views.initiate_transaction, name='initiate_transaction'),
    url(r'^transaction/(?P<transaction_id>\w+)/to-process/$',
        views.initiate_transaction_processing, name='initiate_transaction_processing'),

    url(r'^(?P<insurer_id>\d+)/response/$', views.payment_response, name='payment_response'),
    url(r'^(?P<insurer_slug>[\w-]+)/response/$', views.payment_response, name='payment_slug_response'),

    url(r'^(?P<insurer_id>\d+)/transaction/(?P<transaction_id>\w+)/response/$',
        views.payment_response, name='payment_transaction_response'),
    url(r'^(?P<insurer_slug>[\w-]+)/transaction/(?P<transaction_id>\w+)/response/$',
        views.payment_response, name='payment_transaction_slug_response'),

    url(r'^payment/transaction/(?P<transaction_id>\w+)/success/$', views.payment_success, name='payment_success'),
    url(r'^payment/(transaction/(?P<transaction_id>\w+)/)?failure/$', views.payment_failure, name='payment_failure'),
    # url(r'^product/failure/$', 'product_failure', name='product_failure'),
    url(r'^product/transaction/(?P<transaction_id>\w+)/failure/$',
        views.product_failure, name='product_transaction_failure'),
    url(r'^product/medical/$', views.product_medical, name='product_medical'),
    url(r'^pincode-unavailable/$', views.pincode_not_found, name='pincode_not_found'),
    url(r'^tnc/$', views.transaction_tnc, name='transaction_tnc'),

    url(r'^insurer-list/$', views.insurer_list, name='admin_insurer_list'),
    url(r'^insurer/(?P<insurer_slug>[\w-]+)/transaction-list/$',
        views.insurer_transaction_list, name='admin_insurer_transaction_list'),
    url(r'^transaction/(?P<transaction_id>\w+)/download/$',
        views.transaction_download, name='admin_transaction_download'),

    url(r'^insurer/(?P<insurer_slug>[\w-]+)/transaction/download-all/$',
        views.insurer_download_all_transactions, name='admin_insurer_transaction_download_all'),

    url(r'^act/(?P<activity_id>[\w-]+)/section/(?P<slab_id>\d+)/mail-create-transaction/$',
        views.mail_create_transaction, name='mail_create_transaction'),

    url(r'^apollo/(?P<slab_id>\d+)/start/$', views.apollo_redirect, name='apollo_redirect'),
    url(r'^call/list/$', views.call_list, name='call_list'),
    url(r'^call/(?P<call_id>\d+)/add-remarks/$', views.call_add_remarks, name='call_add_remarks'),
    url(r'^call/(?P<call_id>\d+)/remove/$', views.call_remove, name='call_remove'),
    url(r'^contact-me/$', views.contact_me, name='contact_me'),
    url(r'^download-call-data/$', views.download_call_data, name='download_call_data'),
    url(r'^download-transaction-data/(?P<status>[\w_-]+)/((?P<limit>\d+)/)?$',
        views.download_transaction_data, name='download_transaction_data'),
    url(r'^download-transaction-call-data/$',
        views.download_transaction_call_data, name='download_transaction_call_data'),
    url(r'^download-prefilled-call-data/$', views.download_amlpl_data, name='download_amlpl_data'),
    url(r'^get-city-by-pincode/$', views.get_city_by_pincode, name='get_city_by_pincode'),
    url(r'^get-processing-status/$', views.get_processing_status, name='get-processing-status/'),
    url(r'^show-results/$', views.show_results, name='show_results'),
    url(r'^process-pincode/$', views.process_pincode, name='process_pincode')
]

for insurer_id in INSURER_MAP:
    insurer = INSURER_MAP[insurer_id]
    try:
        import_module('health_product.insurer.%s.urls' % insurer)
    except ImportError:
        pass
    else:
        urlpatterns += [
            url(r'^insurer/%s/' % insurer, include('health_product.insurer.%s.urls' % insurer))
        ]
