import datetime
import json
import uuid
import logging
from collections import OrderedDict
from copy import deepcopy
from importlib import import_module

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.cache import caches
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import (Http404, HttpResponse, HttpResponseNotAllowed,
                         HttpResponseRedirect, JsonResponse)
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from django.views.decorators.cache import never_cache
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
import cf_cns

import prod_utils
import putils
import utils
from core import activity as core_activity
from health_product.models import CallAlert, Transaction
from health_product.prod_utils import calculate_age
from health_product import forms as health_forms
from insurer import settings as insurer_settings
from core.models import Activity, Tracker
from health_product.services.match_plan import MatchPlan
import health_product.queries as hqueries
import health_product.health_utils as hutils
from health_product.health_utils import GetSumAssured
from mail_utils import health_alert, health_transaction_alert
from wiki.models import City, HealthProduct, Hospital, Insurer, Slab, Pincode

cache = caches['health-results']

logger = logging.getLogger(__name__)


@xframe_options_exempt
@never_cache
def desktopv2(request, results=None, insurer_slug=None, product_type=None, tparty=None):
    if insurer_slug:
        get_object_or_404(Insurer, slug=insurer_slug)
    if product_type:
        if product_type not in [cc[0].lower().replace('_', '-') for cc in HealthProduct.COVERAGE_CATEGORY_CHOICES]:
            raise Http404('Invalid product type')
    if product_type and not results:
        return HttpResponseRedirect('/health-plan/')

    user_agent = 'mobile' if request.user_agent.is_mobile else 'desktop'
    step = '' if results else '_steps'

    template_data = {
        'tparty': tparty,
        'insurer': insurer_slug,
        'product_type': product_type
    }
    if step:
        # temporary hack until alternative to asynchronous ajax is found
        template_data['medical_conditions'] = get_medical_conditions(request).content

    template = 'health_product/%sv2%s.html' % (user_agent, step)
    response = render_to_response(
        template, {}, context_instance=RequestContext(request, template_data))
    if request.COOKIES.get('mark_product_category', '') != 'health':
        response.set_cookie('mark_product_category', 'health')

    return response


@xframe_options_exempt
@never_cache
def buy(request, transaction_id, static_mode=False):
    transaction = get_object_or_404(Transaction, transaction_id=transaction_id)

    if (not static_mode) and transaction.status == 'COMPLETED':
        headline = 'We are glad to see you back!'
        message = 'But the link to your transaction is now expired for your safety.'
        subtext = 'Do contact us in case of any concerns. You can go back to the homepage from <a href="/">here</a>!'
        return render_to_response('core/failure_generic.html', {
            'headline': headline,
            'message': message,
            'subtext': subtext,
        }, RequestContext(request))

    health_product = transaction.slab.health_product
    insurer = health_product.insurer
    module_id = insurer_settings.PLAN_TEMPLATE_MAP.get(
        transaction.slab.health_product.id, 'generic')
    stage_view_map = insurer_settings.STAGE_VIEW_DEFAULT
    for k, v in insurer_settings.STAGE_VIEW.items():
        if transaction.slab.health_product.id in k:
            stage_view_map = v
    transaction.track_activity(
        activity_type=core_activity.VIEWED,
        page_id='proposal'
    )

    return render_to_response("health_product:main_buy.html", {
        'transaction': transaction,
        'health_product': health_product,
        'insurer': insurer,
        'moduleID': module_id,
        'stage_view_map': json.dumps(stage_view_map),
        'insurer_name': insurer_settings.INSURER_MAP[insurer.id],
        'static_mode': static_mode,
    }, context_instance=RequestContext(request))

ACTIVITY_MAP = {
    'results': 'RESULTS_VISITED',
    'shortlist': 'SHORTLIST',
    'bought': 'BOUGHT'
}


@never_cache
def get_details(request, activity_type):
    if activity_type in ACTIVITY_MAP.keys():
        activity = Activity.objects.filter(
            tracker=request.TRACKER, activity_type=ACTIVITY_MAP[activity_type])
        if activity:
            activity = activity[0]
        else:
            return HttpResponse(hutils.zip_json({'shortlist': {}}))

        data = activity.extra.get('request')
        if not data:
            return HttpResponse(hutils.zip_json({'shortlist': {}}))
        else:
            if ACTIVITY_MAP[activity_type] == 'SHORTLIST':
                result_data_set = []
                for d in data['shortlist']:
                    slab_id = d['slab_id']
                    data_to_score = hqueries.prepare_post_data_to_score(d[
                                                                      'uidata'])
                    mp = MatchPlan(data_to_score, slab=Slab.objects.get(
                        id=slab_id), request=request)
                    result_data_set.append({'slab_id': slab_id, 'uidata': d[
                                           'uidata'], 'plans': mp.calculate_score()})
                return HttpResponse(hutils.zip_json({'shortlist': result_data_set}))
    else:
        raise Http404


@never_cache
def get_ui_data(request):
    activity_type = 'results'
    try:
        activity = Activity.objects.filter(
            tracker=request.TRACKER, acitity_type=ACTIVITY_MAP[activity_type])
        activity = activity[0]
        search_data = activity.extra.get('request')
    except IndexError:
        search_data = {}
    return JsonResponse(search_data)


@never_cache
def get_medical_conditions(request):
    data = hqueries.get_medical_conditions()
    return JsonResponse(data, safe=False)


@never_cache
def save_details(request, activity_type):

    # print request.TRACKER, request.POST['data']
    if activity_type in ACTIVITY_MAP:
        if request.POST.get("data"):
            json_data = json.loads(request.POST['data'])
            # Convert data into form format.
            form_data = {}
            for member in json_data.get("members", []):
                person = member.get("id", "").lower()
                if person.lower() == "you":
                    person = "self"

                form_data["%s_insured" % person] = str(member.get("selected", ""))
                form_data["%s_age" % person] = str(member.get("age", ""))
                form_data["%s_age_month" % person] = str(member.get("age_in_months", ""))

            json_data = dict(json_data, **form_data)

            activity = core_activity.create_activity(
                activity_type=core_activity.VIEWED,
                product='health',
                page_id='quotes',
                form_name='',
                form_position='',
                form_variant='',
                data=health_forms.HealthQuoteFDForm(data=json_data).get_fd()
            )

            if activity:
                if activity.requirement:
                    req = activity.requirement
                    req.update_extra({'activity_id': activity.activity_id, 'site_url': settings.SITE_URL})
                    req.save(update_fields=['extra'])

                if request.GET.get('hot'):
                    """hot means override all extra data"""
                    activity.extra = {'request': json_data}
                else:
                    activity.extra.update(json_data)

                activity.save()

                return HttpResponse(json.dumps({'activity_id': activity.activity_id}))

            return HttpResponse(json.dumps({'activity_id': ''}))
        else:
            logger.error("Health could not find data key into post request body.")

        return HttpResponse(json.dumps({'activity_id': ''}))
    else:
        raise Http404


@never_cache
def health_score_for_plan(request):
    # print request.POST
    data_to_score = hqueries.prepare_post_data_to_score(
        json.loads(request.POST['data']))
    mp = MatchPlan(data_to_score, slab=Slab.objects.get(
        id=request.POST['id']), request=request)
    sdata = mp.calculate_score()
    return HttpResponse(json.dumps(sdata))


@require_POST
@never_cache
def health_product_score(request, activity_id):
    """request.post['data'] sample : {u'city': {u'type': u'metro', u'id': 2325, u'name': u'mumbai'},
    u'hospital_service': u'private_any', u'hospitals': [343, 514, 1368, 1407, 1438, 1452, 1474, 1532],
    u'pincode': 400053, u'members': [{u'dob': u'01-01-2015', u'person': u'self', u'id': u'self', u'verbose': u'you'},
    {u'dob': u'04-04-2012', u'person': u'daughter', u'id': u'daughter1', u'verbose': u'daughter'},
    {u'dob': u'17-02-2000', u'person': u'son', u'id': u'son2', u'verbose': u'son'}]}
    prepare_post_data_to_score converts the above into the acceptable format for matchplan which is:
    data = {
        'city': City.objects.get(name="Mumbai"),
        'kids': 2,
        'adults': 2,
        'family': True,
        'gender': u'MALE',
        'age': u'28',
        'spouse_age': None,
        'sum_assured': 300000,
        'service_type': u'SHARED',
        'kid_ages': [2, 5],
        'hospitals' : [343, 514, 1368, 1407, 1438, 1452, 1474, 1532]
    } """
    # If you do not send request.POST
    #   It will take data from the older activity
    #   Helps when you are sharing the link with someone
    #   Result type will be None for this case
    #
    # If you have the data then it will take the data that you have send and write it over on the activity
    # If Result type is INIT, it will get all result sets and write current
    # post-data on activity

    json_data = json.loads(request.POST['data'])
    filters = json.loads(request.POST['filters'], '{}')
    activity = get_object_or_404(Activity, activity_id=activity_id)

    result_type = None
    if json_data:
        result_type = request.POST.get('result_type', None)
    else:
        json_data = activity.extra['request']
        result_type = 'INIT'

    if filters.get('insurer'):
        try:
            insurer = Insurer.objects.get(slug=filters['insurer'])
        except Insurer.DoesNotExist:
            insurer = None
    else:
        insurer = None

    # temporary solution until product type is posted from landing import pages
    if 'topup' in filters.get('product_type', '').lower():
        json_data['product_type'] = 'SUPER_TOPUP'

    product_type = json_data.get('product_type') or filters.get('product_type')
    if product_type not in [cc[0] for cc in HealthProduct.COVERAGE_CATEGORY_CHOICES]:
        product_type = 'INDEMNITY'

    mpargs = {
        'product_type': product_type,
        'insurer': insurer,
        'medical_conditions': json_data.get('medical_conditions', {}),
    }

    # print "POST DATA =======> ", json_data
    # print "RESULT TYPEEEEE =======> ", result_type

    result_data_set = []
    # prepare_multiset_post_data returns list of [{'type': <type-string>,
    # 'uidata': <ui-data>, 'data_to_score': <prepare_post_data_to_score>}]
    output_data = hqueries.prepare_multiset_post_data(json_data)

    if not result_type:
        for msd in output_data:
            dtype = msd['type']
            data_to_score = msd['data_to_score']
            uidata = msd['uidata']
            for rset in activity.extra.get('data', []):
                if rset['type'] == dtype:
                    # TODO comparison might not work, write custom compare
                    if rset['uidata'] != uidata:
                        mp = MatchPlan(
                            data_to_score, request=request, **mpargs)
                        result_data_set.append(
                            {'type': dtype, 'uidata': uidata, 'plans': mp.calculate_score()})
                    else:
                        result_data_set.append(
                            {'type': dtype, 'uidata': uidata, 'plans': 'NC'})
    else:
        if result_type == 'INIT':
            # The first request to load all result sets
            for msd in output_data:
                dtype = msd['type']
                data_to_score = msd['data_to_score']
                uidata = msd['uidata']
                mp = MatchPlan(data_to_score, request=request, **mpargs)
                # print "\n\n\n-----------------", msd['uidata'], "-----------------\n\n\n"
                result_data_set.append(
                    {'type': dtype, 'uidata': uidata, 'plans': mp.calculate_score()})
        else:
            for msd in output_data:
                dtype = msd['type']
                data_to_score = msd['data_to_score']
                uidata = msd['uidata']
                if dtype == result_type:
                    mp = MatchPlan(data_to_score, request=request, **mpargs)
                    result_data_set.append(
                        {'type': dtype, 'uidata': uidata, 'plans': mp.calculate_score()})

    # print "RESULT DATA SET ===============> ", [(rd['type']) for rd in result_data_set if rd['plans'] != 'NC']
    activity.extra = {
        'request': json_data,
        'data': output_data
    }
    activity.save()

    score_data_dict = {
        'score_data': result_data_set,
        'uidata': json_data
    }

    data = activity.extra
    data.update({
        'activity_id': activity.activity_id,
        'tracker_id': request.TRACKER.session_key,
    })
    if request.user.is_authenticated():
        data['logged_in_user'] = request.user.email

    form_data = {}
    for member in json_data.get("members", []):
        person = member.get("id", "").lower()
        if person.lower() == "you":
            person = "self"

        form_data["%s_insured" % person] = str(member.get("selected", ""))
        form_data["%s_age" % person] = str(member.get("age", ""))
        form_data["%s_age_month" % person] = str(member.get("age_in_months", ""))

    json_data = dict(json_data, **form_data)
    act = core_activity.create_activity(
        activity_type=core_activity.VIEWED,
        product='health',
        page_id='quotes',
        form_position='',
        form_variant='',
        form_name='',
        data=health_forms.HealthQuoteFDForm(json_data).get_fd()
    )
    act.extra = data
    act.save(update_fields=['extra'])
    return HttpResponse(hutils.zip_json(score_data_dict))


def fetch_sum_assured(request):
    if request.POST:
        data = json.loads(request.POST['data'])
        gsa = GetSumAssured(data)
        sa = gsa.get_sa()
        return HttpResponse(json.dumps({'success': True, 'sum_assured': sa}))
    else:
        return HttpResponse(json.dumps({'success': False}))


def hospitals_by_pincode(request):
    post_data = json.loads(request.POST['data'])
    data = {}
    for pincode in list(set(post_data['pincodes'])):

        data[pincode] = hqueries.get_city_and_hospitals(pincode)
    encrypted = hutils.zip_json(data)
    key = request.POST.get('activity_id', None)
    slab_scores = cache.get(key)
    if slab_scores:
        for each_score in slab_scores:
            hqueries.update_text_to_display(each_score)
    cache.delete(key)
    return HttpResponse(encrypted)


@require_POST
def get_processing_status(request):
    max_age = int(request.POST['age'])
    slab_id = request.POST['slab_id']
    slab = Slab.objects.get(id=slab_id)
    response = MatchPlan.get_post_processing_flag(max_age, slab)
    return JsonResponse(response)


def hospitals_auto_complete(request):
    if len(request.GET['term']) <= 2:
        # return HttpResponse(json.dumps({'hospitals': []}))
        encrypted = hutils.zip_json({'hospitals': []})
        return HttpResponse(json.dumps({'data': encrypted}))

    qterm = request.GET['term']
    qterm_list = qterm.split(' ')
    qterm_Q = Q()
    for t in qterm_list:
        qterm_Q = qterm_Q & (Q(name__icontains=t) | Q(city__name__icontains=t))

    if request.GET['type'] == 'city':
        pin = request.GET['pin']
        near_by_pincodes = [pobj.pincode for pobj in hqueries.get_nearest_pincode(pin)[
            1]]
        hcc_list = Hospital.objects.filter(
            Q(pin__in=near_by_pincodes) & qterm_Q)

    if request.GET['type'] == 'country':
        hcc_list = Hospital.objects.filter(qterm_Q)

    hccs = [{
        'name': hcc.name,
        'verbose': "%s, %s" % (hcc.name, hcc.city.name),
        'address': hcc.address,
        'lat': hcc.lat,
        'lng': hcc.lng,
        'id': hcc.id
    } for hcc in hcc_list]

    # return HttpResponse(json.dumps({'hospitals' : hccs}))
    encrypted = hutils.zip_json({'hospitals': hccs})
    return HttpResponse(json.dumps({'data': encrypted}))


def create_transaction(request):
    post_data = json.loads(request.POST['data'])
    slab = Slab.objects.get(id=post_data['slab_id'])

    transaction_id = uuid.uuid1().hex
    transaction = Transaction(
        tracker=request.TRACKER,
        transaction_id=transaction_id,
        status='DRAFT',
        extra=post_data,
        slab=slab,
        third_party=post_data.get('tparty', '')
    )
    transaction.save()
    return HttpResponse(json.dumps({'transaction_id': transaction_id}))


def initiate_transaction(request, transaction_id):
    transaction = Transaction.objects.get(transaction_id=transaction_id)
    transaction.extra['ip_address'] = prod_utils.get_client_ip(request)
    transaction.save()

    is_redirect, redirect_url, template_name, data = prod_utils.process_payment_backend(
        transaction)
    utils.request_logger.info("===================\n"
                              "DATA FOR PAYMENT\n"
                              "Transaction ID: %s\n"
                              "Is Redirect: %s\n"
                              "Redirect URL: %s\n"
                              "Data: %s" % (transaction.transaction_id, is_redirect, redirect_url, data))
    if is_redirect:
        return HttpResponseRedirect(redirect_url)
    else:
        return render_to_response(template_name, data, context_instance=RequestContext(request))


def get_transaction_data(request):
    """
    Format for insured_list:
    insured_list = [
        {'self' : {
            'gender': 1,
            'display_text': 'Yourself',
            'date_of_birth' : '01-01-1960',
        }},]
    """
    if request.POST:
        transaction_id = request.POST.get('transaction_id')
        transaction = Transaction.objects.get(transaction_id=transaction_id)
        # TODO add here
        data = prod_utils.process_formdata_backend(transaction)

        data['red_flags'] = {}
        if 'red_flags' in transaction.extra:
            for k, v in transaction.extra['red_flags'].items():
                data['red_flags'][k] = True if ((v['reason'] == 'AGE_LIMIT') or (
                    v['reason'] == 'MEDICAL' and v['data'])) else False

        return HttpResponse(json.dumps({'success': True, 'data': data}))
    else:
        data = {}
        return HttpResponse(json.dumps({'success': False, 'data': data}))


@never_cache
def notify_premium_change(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.POST['data'])
            transaction = Transaction.objects.get(
                transaction_id=data['transaction_id'])

            dob_list = data['date_of_birth_list']
            mem_details = data['member_details']
            age_list = {}

            for k, v in dob_list.items():
                age_list[k] = {'current_age': prod_utils.calculate_age(str(v))}

            members = transaction.extra['members']

            for m in members:
                age_list[m['id']].update(
                    {'previous_age': {'years': m['age'], 'months': m['age_in_months']}})
                m['age'] = age_list[m['id']]['current_age']['years']
                m['age_in_months'] = age_list[m['id']]['current_age']['months']

            min_age_limit = transaction.slab.age_limit.minimum_entry_age
            max_age_limit = transaction.slab.age_limit.maximum_entry_age
            medical_required = transaction.slab.medical_required.is_medical_required
            max_medical_age = transaction.slab.medical_required.maximum_age
            max_online_age = transaction.slab.online_availability.maximum_age
            is_online_available = transaction.slab.online_availability.is_available_online
            return_flag = 'PREMIUM_CHANGE'
            if transaction.slab.health_product.insurer.slug == u'the-oriental-insurance-company-ltd':
                proposer_first_name = transaction.insured_details_json['form_data']['proposer']['personal']['first_name']['value']
                proposer_last_name = transaction.insured_details_json['form_data']['proposer']['personal']['last_name']['value']
                proposer_dob = transaction.insured_details_json['form_data']['proposer']['personal']['date_of_birth']['value']
                proposer_gender = '%s' % transaction.insured_details_json['form_data']['proposer']['personal']['gender']['value']
                if {'father', 'mother'}.issubset(set(data['date_of_birth_list'].keys())) \
                   and 'self' not in data['date_of_birth_list'].keys() and \
                   transaction.slab.health_product.insurer.slug == u'the-oriental-insurance-company-ltd':
                    valid = False
                    old_bool = False
                    msg = 'please make one of your parents as proposer.'
                    for mem in mem_details:
                        if mem in ['father', 'mother']:
                            gender = '1'
                            if mem == 'mother':
                                gender = '2'
                            mem_first_name = mem_details[mem]['first_name']
                            mem_last_name = mem_details[mem]['last_name']
                            dob = mem_details[mem]['dob']
                            date_obj = datetime.datetime.strptime(dob, '%d/%m/%Y')
                            if calculate_age(date_obj)['years'] > 55:
                                old_bool = True
                                continue
                            elif proposer_dob == dob and proposer_first_name == mem_first_name and proposer_last_name == mem_last_name and gender == proposer_gender:
                                valid = True
                                break
                            if calculate_age(date_obj)['years'] <= 55:
                                msg = 'Please make your %s as proposer.' % mem
                    if not valid and old_bool:
                        return HttpResponse(json.dumps({
                            'success': True,
                            'flag': 'PROPOSER_INSURER',
                            'age_list': age_list,
                            'msg': msg

                        }))

            if transaction.slab.health_product.insurer.slug == 'hdfc-ergo':
                mem_list = []
                for m in members:
                    mem_list.append(m['id'])
                if ('father' in mem_list and 'self' in mem_list) or ('mother' in mem_list and 'self' in mem_list):
                    return HttpResponse(json.dumps({
                        'success': True,
                        'flag': 'HDFC_PARENT_WITH_PROPOSER',
                        'age_list': age_list,
                    }))

            for m in members:
                if ((min_age_limit >= 1000 and m['age'] < min_age_limit / 1000) or
                        (min_age_limit < 1000 and m['age_in_months'] < min_age_limit and m['age'] == 0)):
                    # Return MIN_AGE_LIMIT error
                    return HttpResponse(json.dumps({
                        'success': True,
                        'flag': 'MIN_AGE_LIMIT',
                        'age_limit': str(min_age_limit) + ' year(s)' if min_age_limit / 1000 > 0 else str(min_age_limit) + ' month(s)',
                        'age_list': age_list,
                    }))
                if max_age_limit >= 1000 and m['age'] > max_age_limit / 1000:
                    # Return MAX_AGE_LIMIT error
                    return HttpResponse(json.dumps({
                        'success': True,
                        'flag': 'MAX_AGE_LIMIT',
                        'age_limit': str(max_age_limit / 1000) + ' year(s)',
                        'age_list': age_list,
                    }))
                if (not is_online_available) and m['age'] > max_online_age:
                    return HttpResponse(json.dumps({
                        'success': True,
                        'flag': 'ONLINE_AGE_LIMIT',
                        'age_limit': str(max_online_age) + ' year(s)',
                        'age_list': age_list,
                    }))
                if medical_required and m['age'] > max_medical_age:
                    return_flag = 'MEDICAL_REQUIRED'
                    break

            insurer_id = transaction.slab.health_product.insurer_id
            insurer_form_data = import_module('health_product.insurer.%s.form_data' % (
                insurer_settings.INSURER_MAP[insurer_id]))

            return_data = {'success': True,
                           'flag': return_flag, 'age_list': age_list}
            try:
                get_premium_details = insurer_form_data.get_premium_details
                premium_details = get_premium_details(
                    deepcopy(transaction.extra), transaction.slab_id)
                return_data.update({
                    'discounted_premium': premium_details['discounted_premiums'],
                    'base_premium': premium_details['base_premiums'],
                    'new_cpt_id': premium_details['cpt_ids'],
                })
            except AttributeError:
                data_to_score = hqueries.prepare_post_data_to_score(
                    deepcopy(transaction.extra))
                slab = Slab.objects.get(id=transaction.slab_id)
                mp = MatchPlan(data_to_score, slab=slab, request=None)
                cpt = mp.get_cpt()
                if cpt:
                    return_data.update({
                        'discounted_premium': {1: cpt.premium},
                        'base_premium': {1: cpt.premium},
                        'new_cpt_id': {1: cpt.id},
                    })
            return HttpResponse(json.dumps(return_data))
        except Exception as e:
            # print "EXCEPTION:", e
            return HttpResponse(json.dumps({'success': False}))
    else:
        return HttpResponseNotAllowed("Not Allowed")


@never_cache
def agree_premium_change(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.POST['data'])
            transaction = Transaction.objects.get(
                transaction_id=data['transaction_id'])
            age_list = data['age_list']

            members = transaction.extra['members']
            for m in members:
                m['age'] = age_list[m['id']]['years']
                m['age_in_months'] = age_list[m['id']]['months']

            if data['new_cpt_id']:
                transaction.extra['agreed_premium_change'] = True
                transaction.extra['cpt_id'] = data['new_cpt_id']
            transaction.extra['plan_data']['premium'] = data['new_premium']
            transaction.save()
            return HttpResponse(json.dumps({'success': True}))
        except Exception as e:
            # print "Exception: ", e
            return HttpResponse(json.dumps({'success': False}))
    else:
        return HttpResponseNotAllowed("Not Allowed")


@never_cache
@csrf_exempt
def initiate_transaction_processing(request, transaction_id):
    transaction = Transaction.objects.get(transaction_id=transaction_id)

    is_redirect, redirect_url, template_name, data = prod_utils.process_payment_backend(
        transaction)

    utils.request_logger.info("===================\n"
                              "DATA FOR PAYMENT\n"
                              "Transaction ID: %s\n"
                              "Is Redirect: %s\n"
                              "Redirect URL: %s\n"
                              "Data: %s" % (transaction.transaction_id, is_redirect, redirect_url, data))

    transaction.track_activity(
        activity_type=core_activity.VIEWED,
        page_id='payment'
    )

    if is_redirect:
        return HttpResponseRedirect(redirect_url)
    else:
        if not template_name or template_name == 'health_product/post_transaction_data.html':
            template_name = 'health_product/post_transaction_data.html'
            return render_to_response(template_name, {
                'data': data,
                'redirect_url': redirect_url,
            }, context_instance=RequestContext(request))
        else:
            return render_to_response(template_name, data, context_instance=RequestContext(request))


@never_cache
def show_results(request):
    if request.method == 'POST':
        try:
            transaction_id = request.POST['transaction_id']
            transaction = Transaction.objects.get(
                transaction_id=transaction_id)
            request.POST = request.POST.copy()
            members = transaction.extra['members']
            pincode = ''
            parents_pincode = ''
            for m in members:
                if m['person'] == 'self':
                    m['person'] = 'you'
                if m['person'] in ['father', 'mother']:
                    parents_pincode = transaction.extra['pincode']
                if m['person'] in ['you']:
                    pincode = transaction.extra['pincode']

            post_data = {
                'members': transaction.extra['members'],
                'gender': transaction.extra['gender'],
                'hospital_service': transaction.extra.get('hospital_service', ''),
                'pincode': pincode,
                'parents_pincode': parents_pincode,
            }
            request.POST['data'] = json.dumps(post_data)

            request.GET = request.GET.copy()
            request.GET['hot'] = True
            return save_details(request, 'results')
        except Exception as e:
            # print "Exception: ", sys.exc_info()
            return HttpResponse(json.dumps({'success': False}))
    else:
        return HttpResponseNotAllowed("Not Allowed")


@never_cache
def payment_success(request, transaction_id):
    transaction = get_object_or_404(Transaction, transaction_id=transaction_id)
    prod_utils.transaction_successful(transaction, request)
    transaction.open_requirement_if_deleted()

    # TODO: Create a third party model here
    if transaction.third_party == 'jd':
        transaction.track_activity(
            activity_type=core_activity.SUCCESS,
            page_id='payment',
            requirement=transaction.requirement
        )
        return HttpResponseRedirect('https://justdial.com/coverfox/health-insurance/success/%s/' % transaction_id)

    transaction.track_activity(
        activity_type=core_activity.SUCCESS,
        page_id='payment',
        requirement=transaction.requirement
    )

    return render_to_response('payment/success.html', {
        'transaction': transaction,
    }, context_instance=RequestContext(request))


@never_cache
def transaction_tnc(request):
    return render_to_response('health_product/terms_and_conditions.html', {
    }, context_instance=RequestContext(request))


@never_cache
@csrf_exempt
def payment_failure(request, transaction_id=None):
    transaction = transaction_id and Transaction.objects.get(transaction_id=transaction_id)
    if transaction:
        transaction.status = 'FAILED'
        transaction.save()
        # TODO: create PAYMENT_FAILURE template
        # direct_mail.send_message('TRANSACTION_FAILURE_MAIL', obj=obj, site_url=settings.SITE_URL)

        transaction.track_activity(
            activity_type=core_activity.FAILED,
            page_id='payment'
        )
    health_transaction_alert(transaction, 'Payment failed', request)
    return render_to_response('health_product/failure.html', {}, context_instance=RequestContext(request))


@never_cache
def product_failure(request, transaction_id):
    transaction = Transaction.objects.get(transaction_id=transaction_id)
    transaction.status = 'FAILED'

    transaction.save()
    if request.user.is_authenticated():
        user_id = request.user.email
    else:
        user_id = transaction.transaction_id
    obj = {
        'user_id': user_id,
        'to_email': transaction.proposer_email,
        'user_mobile': transaction.proposer_mobile,
        'name': utils.clean_name('%s %s' % (transaction.proposer_first_name, transaction.proposer_last_name)),
        'tag': 'transaction-failed',
        'error-message': transaction.extra.get('error_msg', ''),
        'transaction_id': transaction.transaction_id,
        'insurer_title': transaction.slab.health_product.insurer.title,
        'product_title': transaction.slab.health_product.title,
        'premium': transaction.premium_paid or transaction.extra['plan_data']['premium'],
        'buy_form_link': '%s/health-plan/buy/%s' % (settings.SITE_URL, transaction.transaction_id),
    }
    cf_cns.notify('TRANSACTION_FAILURE_MAIL', to=(obj['name'], obj['to_email']),
                  obj=obj, mandrill_template='newbase')

    health_transaction_alert(transaction, 'Transaction failed', request)

    transaction.track_activity(
        activity_type=core_activity.FAILED,
        page_id='proposal',
    )

    return render_to_response('health_product/failure.html', {
    }, context_instance=RequestContext(request))


@never_cache
def pincode_not_found(request):
    return render_to_response('health_product/pincode_not_found.html', {
    }, context_instance=RequestContext(request))


@never_cache
@csrf_exempt
def payment_response(request, insurer_id=None, insurer_slug=None, transaction_id=None):
    insurer_dict = {
        'cigna-ttk': 15,
        'iffco-tokio': 14,
        'hdfc-ergo': 13,
        'cholamandalam-ms': 12,
        'landt-general-insurance': 11,
        'reliance-health-insurance': 10,
        'bharti-axa': 9,
        'the-new-india-assurance-co-ltd': 8,
        'tata-aig': 7,
        'religare': 6,
        'max-bupa': 5,
        'bajaj-allianz': 4,
        'icici-lombard': 3,
        'star-health-insurance': 2,
        'apollo-munich': 1,
        'apollo_munich': 1
    }
    response = ''
    if insurer_id:
        insurer = Insurer.objects.get(id=insurer_id)
    else:
        insurer = Insurer.objects.get(id=insurer_dict[insurer_slug])

    if insurer.id == 4:
        # EXCEPTION CHECK FOR BAJAJ ALLIANZ
        response = dict([(k, v) for k, v in request.GET.items()])
    else:
        response_dict = dict(request.GET.items() + request.POST.items())
        response = dict([(k, v) for k, v in response_dict.items()])

    try:
        if transaction_id:
            transaction = Transaction.objects.get(transaction_id=transaction_id)
        else:
            transaction = None

    except Transaction.DoesNotExist:
        error_string = ("===================\n"
                        "MISSING TRANSACTION\n"
                        "PAYMENT RESPONSE\n"
                        "Insurer ID: %s\n"
                        "Transaction ID: %s\n"
                        "Response : %s\n"
                        % (insurer.id, transaction_id, response))
        health_alert("Missing transaction: %s" % transaction_id, error_string)
        utils.request_logger.error(error_string)
        raise Http404

    is_redirect, redirect_url, template_name, data = prod_utils.process_payment_response(
            insurer.id, response, transaction)

    utils.request_logger.info("===================\n"
                              "PAYMENT RESPONSE\n"
                              "Insurer ID: %s\n"
                              "Transaction ID: %s\n"
                              "Is Redirect: %s\n"
                              "Redirect URL: %s\n"
                              "Response : %s\n"
                              "Data: %s" % (insurer.id, transaction_id, is_redirect, redirect_url, response, data))

    if is_redirect:
        return HttpResponseRedirect(redirect_url)
    else:
        return render_to_response(template_name, {
            'data': data,
            'redirect_url': redirect_url,
        }, context_instance=RequestContext(request))


@never_cache
def product_medical(request):
    return render_to_response('health_product/medicalscreen-question.html', {
    }, context_instance=RequestContext(request))


def get_to_medical_data(request):
    data = {}
    if request.POST:
        transaction_id = request.POST.get('transaction_id')
        transaction = Transaction.objects.get(transaction_id=transaction_id)

        question_list = []
        age_list = []

        for k, v in transaction.extra['red_flags'].items():
            if(v['reason'] == 'DISEASE'):
                for r in v['data']:
                    question_list.append(r)
            else:
                age_list = v['data']

        data = {
            'question_list': question_list,
            'age_list': age_list,
            'transaction_id': transaction_id,
            'extra_data': transaction.extra,
        }
        return HttpResponse(json.dumps({'success': True, 'data': data}))
    else:
        data = {}
        return HttpResponse(json.dumps({'success': False, 'data': data}))


def save_call_time(request):
    request_ip = putils.get_request_ip(request)
    msg = json.loads(request.POST['data'])
    msg['referer'] = "%s" % request.META.get('HTTP_REFERER', '')
    msg['created_on'] = datetime.datetime.now().strftime("%d/%m/%Y %H:%M")
    if msg.get('send_buy_form', '').lower() == 'y':
        msg['action'] = 'SEND BUY FORM'
    if msg.get('activity_id'):
        msg['result_url'] = "http://www.coverfox.com/health-plan/results/#%s" % msg['activity_id']
    transaction_data = {}
    if msg.get('transaction_id'):
        transaction = Transaction.objects.get(transaction_id=transaction_id)
        transaction.extra['medical_call_time'] = msg
        transaction.save()
        call_alert = CallAlert(
            tracker=request.TRACKER,
            phone_number=msg['mobile'],
            transaction=transaction,
            via=request.POST.get('source', 'default')
        )
        msg['name'] = transaction.proposer_first_name
        msg['email'] = transaction.proposer_email
    else:
        call_alert = CallAlert(
            tracker=request.TRACKER,
            phone_number=msg['mobile'],
            via=request.POST.get('source', 'default')
        )
    try:
        msg.update['email'] = "%s" % request.TRACKER.user.email
    except:
        pass

    slot_list = []
    if 'todaySlots' in msg.keys():
        for i in msg['todaySlots']:
            if 'slot_selected' in i.keys() and i['slot_selected'].upper() == 'YES':
                slot_list.append('Today(%s): %s' %
                                 (msg['todayText'], i['slot']))
    if 'tommSlots' in msg.keys():
        for i in msg['tommSlots']:
            if 'slot_selected' in i.keys() and i['slot_selected'].upper() == 'YES':
                slot_list.append('Tomorrow(%s): %s' %
                                 (msg['tommText'], i['slot']))
    if 'afterSlots' in msg.keys():
        for i in msg['afterSlots']:
            if 'slot_selected' in i.keys() and i['slot_selected'].upper() == 'YES':
                slot_list.append('Day After(%s): %s' %
                                 (msg['afterText'], i['slot']))
    call_alert.extra['slots'] = slot_list
    call_alert.remarks = '%s\nNew Call Scheduled On : %s' % (
        call_alert.remarks, datetime.datetime.now())
    call_alert.save()
    msg.update({'slots': "%s" % (', '.join(slot_list))})

    call_alert.extra.update(**msg)
    call_alert.save()

    utils.SNSUtils.send_lead_message(msg, internal=request.IS_INTERNAL, request_ip=request_ip)
    return HttpResponse(json.dumps({'success': True}))


@login_required
@utils.profile_type_only('ADMIN', 'REPORT')
@never_cache
def insurer_list(request):
    ins_dict = {}
    for i in Insurer.objects.all():
        ins_dict[i] = {
            'no_of_transactions': Transaction.objects.filter(slab__health_product__insurer=i, status='COMPLETED').count()
        }
    total_transactions = Transaction.objects.filter(status='COMPLETED').count()
    return render_to_response('health_product/insurer_list.html', {
        'ins_dict': ins_dict,
        'total_transactions': total_transactions
    }, context_instance=RequestContext(request))


@login_required
@utils.profile_type_only('ADMIN', 'REPORT')
@never_cache
def insurer_transaction_list(request, insurer_slug):
    insurer = Insurer.objects.get(slug=insurer_slug)
    trans_list = Transaction.objects.filter(
        slab__health_product__insurer=insurer, status='COMPLETED')
    return render_to_response('health_product/transaction_list.html', {
        'trans_list': trans_list,
        'insurer': insurer
    }, context_instance=RequestContext(request))


@login_required
@utils.profile_type_only('ADMIN', 'REPORT')
@never_cache
def insurer_download_all_transactions(request, insurer_slug):
    insurer = Insurer.objects.get(slug=insurer_slug)
    data_row_list = []
    order = []
    for i in Transaction.objects.filter(slab__health_product__insurer=insurer, status='COMPLETED'):
        singlet_data_row_list, order = prod_utils.extract_transaction_data(i)
        data_row_list.extend(singlet_data_row_list)
    if data_row_list:
        return putils.render_excel('%s_all_transaction.xls' % insurer_slug, order, data_row_list)
    else:
        return HttpResponse("No records found")


@login_required
@utils.profile_type_only('ADMIN', 'REPORT')
@never_cache
def transaction_download(request, transaction_id):
    transaction = Transaction.objects.get(transaction_id=transaction_id)
    # try:
    if True:
        data = prod_utils.process_transaction_download(transaction)
        if data['data_row_list']:
            return putils.render_excel(data['filename'], data['order'], data['data_row_list'], big_header=data['big_header'])
        else:
            return HttpResponse("No records found")
    # except:
    else:
        pass
    return HttpResponse("No records found")


def mail_create_transaction(request, activity_id, slab_id):
    try:
        activity = Activity.objects.get(activity_id=activity_id)
        mail_obj = activity.tracker.queue_set.all().order_by('-updated_on')[0]
        post_data = mail_obj.extra
        if 'trans_dict' in mail_obj.extra.keys():
            post_data['cpt_id'] = mail_obj.extra[
                'trans_dict'][slab_id]['cpt_id']
        post_data['slab_id'] = slab_id
        transaction_id = uuid.uuid1().hex
        transaction = Transaction(
            tracker=request.TRACKER,
            transaction_id=transaction_id,
            status='DRAFT',
            extra=post_data,
            slab=Slab.objects.get(id=slab_id)
        )
        transaction.save()
        return HttpResponseRedirect(reverse('buy', args=[transaction.transaction_id]))
    except Exception as e:
        # print str(e.message)
        return HttpResponseRedirect(reverse('results', args=[activity_id]))


def apollo_redirect(request, slab_id):
    slab = Slab.objects.get(id=slab_id)
    plan_name = slab.health_product.title
    redirect_url = 'http://www.apollomunichinsurance.com/buyonline/Buy-Online-Individual-Registration.aspx?id=10665'
    code = '16R862'
    return render_to_response('health_product/apollo_redirect.html', {
        'slab': slab,
        'redirect_url': redirect_url,
        'code': code
    }, context_instance=RequestContext(request))


@never_cache
def transaction_final(request, transaction_id):
    t = Transaction.objects.get(transaction_id=transaction_id)
    personal = t.insured_details_json['form_data']['proposer']['personal']
    address = t.insured_details_json['form_data']['proposer']['address']
    contact = t.insured_details_json['form_data']['proposer']['contact']
    t.proposer_first_name = personal['first_name'][
        'value'].strip().lower().capitalize()
    if 'middle_name' in personal.keys():
        t.proposer_middle_name = personal['middle_name'].get(
            'value', '').strip().lower().capitalize()
    else:
        t.proposer_middle_name = ''
    t.proposer_last_name = personal['last_name'][
        'value'].strip().lower().capitalize()
    t.proposer_address1 = address['address1']['value']
    t.proposer_address2 = address['address2']['value']
    t.proposer_address_landmark = address['landmark'].get('value', '')
    if 'city' in address.keys():
        t.proposer_city = address['city']['value']
        t.proposer_state = address['state']['value']
    else:
        t.proposer_city = address['state']['dependant_value']
        t.proposer_state = address['state']['value1']
    t.proposer_pincode = address['pincode']['value']
    t.proposer_email = contact['email']['value']
    t.proposer_mobile = contact['mobile']['value']
    t.proposer_landline = contact['landline'].get('display_value', '')
    t.status = 'PENDING'
    t.save()
    return HttpResponseRedirect('/health-plan/transaction/%s/to-process/' % t.transaction_id)


@never_cache
def save_transaction_stage(request):
    response_data = {}
    if request.POST:
        transaction_id = request.POST.get('transaction_id')
        transaction = Transaction.objects.get(transaction_id=transaction_id)
        stage_data = json.loads(request.POST['data'])
        transaction = prod_utils.save_stage(transaction, stage_data)
        template = stage_data['template']
        if template == 'stage_2':
            transaction = prod_utils.check_insured_age(transaction)
        red_flags = {}
        for k, v in transaction.extra['red_flags'].items():
            red_flags[k] = True if v['data'] else False

        if(request.POST.get('medical_flag') == 'true'):
            transaction.extra['red_flags'][template]['reason'] = 'AGE_LIMIT'
            transaction.extra['red_flags'][template][
                'data'] = json.loads(request.POST['age_list'])
            red_flags[template] = True

        transaction.save()

        response_data = {
            'insurer_name': transaction.slab.health_product.insurer.title,
            'success': True,
            'red_flags': red_flags,
            'redirect_url': '/health-plan/product/transaction/%s/failure/' % transaction.transaction_id
        }
    return HttpResponse(json.dumps(response_data))


@login_required
@utils.profile_type_only('ADMIN', 'REPORT')
@never_cache
def call_list(request):
    # cutoff_time = datetime.datetime.now()
    # for t in Transaction.objects.filter(status__in=['PENDING', 'FAILED'], updated_on__lte=cutoff_time).order_by('updated_on'):
    #     if not t.callalert_set.filter():
    #         call_alert = CallAlert(tracker=request.TRACKER,
    #                                transaction=t,
    #                                phone_number=t.proposer_mobile,
    #                                via='Transaction filled - %s' % t.status)
    #         call_alert.save()
    phone_list = CallAlert.objects.filter(is_active=True)
    return render_to_response('health_product/call_list.html', {
        'call_list': phone_list
    }, context_instance=RequestContext(request))


@login_required
@utils.profile_type_only('ADMIN', 'REPORT')
@never_cache
def call_add_remarks(request, call_id):
    if request.POST:
        data = json.loads(request.POST['data'])
        call_alert = CallAlert.objects.get(id=call_id)
        call_alert.remarks = data['remarks']
        call_alert.save()
    return HttpResponse(json.dumps({'success': True}))


@login_required
@utils.profile_type_only('ADMIN', 'REPORT')
def call_remove(request, call_id):
    call_alert = CallAlert.objects.get(id=call_id)
    call_alert.is_active = False
    call_alert.save()
    return HttpResponseRedirect(reverse('call_list'))


@never_cache
def contact_me(request):
    return render_to_response('health_product/contact.html', {
    }, RequestContext(request))


@csrf_exempt
def amlpl_save_number(request):
    request_ip = putils.get_request_ip(request)
    details = dict(request.POST.items() + request.GET.items())
    if details:
        request_details = {str(k): str(v) for k, v in details.items()}
        tracker_id = details['sessionID'][8:]  # yyyymmddTRACKER_ID
        tracker = Tracker.objects.get(id=tracker_id)
        tracker.extra['amlpl'] = details
        tracker.save()

        mobile = details['msisdn']
        try:
            call_alert = CallAlert.objects.get(
                phone_number=details['msisdn'], via='amlpl', tracker=tracker)
            call_alert.extra.update(details)
        except:
            details['timestamps'] = []
            call_alert = CallAlert(
                tracker=request.TRACKER,
                phone_number=details['msisdn'],
                is_active=False,
                via='amlpl',
                extra=details,
            )
        call_alert.extra['timestamps'].append(datetime.datetime.now())
        call_alert.save()

        if details['dnd'] == "no":
            lms_data = {
                'campaign': 'motor-involuntary',
                'device': 'Mobile',
                'mobile': request_details['msisdn'],
                'label': 'involuntary',
            }
            lms_data.update(dict(request_details))

            utils.SNSUtils.send_lead_message(lms_data, internal=request.IS_INTERNAL, request_ip=request_ip)

    return HttpResponse(json.dumps({'success': True if details else False}))


def amlpl_fetch_number(request):
    tracker = request.TRACKER
    try:
        return HttpResponse(tracker.extra['amlpl']['msisdn'])
    except:
        return HttpResponse('')


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def download_buy_form(request, transaction_id):
    transaction = Transaction.objects.get(transaction_id=transaction_id)
    insurer_id = transaction.slab.health_product.insurer_id
    ins_utils = import_module('health_product.insurer.%s.ins_utils' %
                              insurer_settings.INSURER_MAP[insurer_id])
    return ins_utils.download_buy_form(transaction)


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def download_call_data(request):
    from_date = datetime.datetime.strptime('1/10/2014', '%d/%m/%Y')
    call_alerts = CallAlert.objects.filter(created_on__gte=from_date)
    order = [
        'Phone',
        'Device',
        'Network',
        'Category',
        'Brand',
        'UTM Source',
        'UTM Medium',
        'UTM Campaign',
        'UTM AdGroup',
        'UTM Keyword',
        'UTM Content',
        'Via',
        'E-mail',
        'Name',
        'Remarks',
        'Requested on (Date)',
        'Requested on (Time)',
        'Slots',
        'TransactionId'
    ]
    rows = []
    for call_alert in call_alerts:
        user = call_alert.tracker.user
        if user:
            full_name = user.get_full_name()
            email = user.email
        else:
            full_name = ''
            email = ''
        row = [
            call_alert.phone_number,
            call_alert.extra.get('device', ''),
            call_alert.extra.get('network', ''),
            call_alert.extra.get('category', ''),
            call_alert.extra.get('brand', ''),
            call_alert.extra.get('utm_source', ''),
            call_alert.extra.get('utm_medium', ''),
            call_alert.extra.get('utm_campaign', ''),
            call_alert.extra.get('utm_adgroup', ''),
            call_alert.extra.get('utm_keyword', ''),
            call_alert.extra.get('utm_content', ''),
            call_alert.via,
            email,
            full_name,
            call_alert.remarks,
            call_alert.created_on.strftime('%d-%m-%Y'),
            call_alert.created_on.strftime('%H:%M'),
            ' | '.join(call_alert.extra.get('slots', '')),
        ]
        try:
            transaction_id = call_alert.transaction.transaction_id
        except:
            transaction_id = ''
        row.append(transaction_id)
        rows.append(row)

    file_name = 'call_data_%s.xls' % datetime.datetime.now().strftime("%d-%b_%H%M")
    return putils.render_excel(file_name, order, rows)


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def download_transaction_call_data(request):
    order = [
        'Phone',
        'E-mail',
        'Name',
        'Requested on (Date)',
        'Requested on (Time)',
        'TransactionId'
    ]
    rows = []
    for t in Transaction.objects.all().exclude(proposer_mobile=''):
        row = [
            t.proposer_mobile,
            t.proposer_email,
            ' '.join([t.proposer_first_name, t.proposer_last_name]),
            t.created_on.strftime('%d-%m-%Y'),
            t.created_on.strftime('%H:%M'),
            t.transaction_id
        ]
        rows.append(row)

    file_name = 'transaction_call_data_%s.xls' % datetime.datetime.now().strftime("%d-%b_%H%M")
    return putils.render_excel(file_name, order, rows)


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def download_amlpl_data(request):
    order = [
        'Phone',
        'Operator',
        'Visited Site At',
        'Voluntarily submitted no',
        'Voluntarily submitted at',
        'Label',
        'Dnd',
    ]
    CA_dict = OrderedDict()
    for amlpl in CallAlert.objects.filter(via='amlpl').order_by('-updated_on'):
        if amlpl.phone_number not in CA_dict:
            CA_dict[amlpl.phone_number] = amlpl

    callable_numbers = {}
    for ca in CallAlert.objects.filter(is_active=True, phone_number__in=CA_dict.keys()).order_by('-updated_on'):
        if ca.phone_number not in callable_numbers:
            callable_numbers[ca.phone_number] = ca

    rows = []
    for phone_number in CA_dict:
        ca = CA_dict[phone_number]
        callable_alert = callable_numbers.get(phone_number)
        row = [
            phone_number,
            ca.extra.get('operator', ''),
            datetime.datetime.strftime(ca.updated_on, '%d-%b-%Y %I:%M %p'),
            'Yes' if phone_number in callable_numbers else 'No',
            datetime.datetime.strftime(
                callable_alert.updated_on, '%d-%b-%Y %I:%M %p') if callable_alert else '',
            callable_alert.extra.get('label', '') if callable_alert else '',
            ca.extra.get('dnd', '')
        ]
        rows.append(row)

    file_name = 'Prefilled_call_data_%s.xls' % datetime.datetime.now().strftime("%d-%b_%H%M")
    return putils.render_excel(file_name, order, rows)


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def download_transaction_data(request, status, limit, insurer=None, from_date=None, to_date=None):
    filters = Q()
    if insurer:
        filters = filters & Q(insurer=insurer)
    if from_date:
        filters = filters & Q(payment_on__gte=from_date)
    if to_date:
        filters = filters & Q(payment_on__lte=to_date)
    order = [
        'Name',
        'Date of birth',
        'Phone',
        'E-mail',
        'Insurer',
        'Product',
        'Sum Assured',
        'Deductible',
        'City',
        'State',
        'Premium Paid',
        'Payment On',
        'Policy Number',
        'Policy Token',
        'Reference Id',
        'Transaction Status',
        'TransactionId',
        'Created On',
        'Updated On',
    ]
    rows = []
    transactions = Transaction.objects.filter(filters).order_by('-updated_on')
    if limit:
        transactions = transactions[:limit]
    elif status:
        if status == 'all':
            transactions = transactions.exclude(premium_paid='')
        elif status == 'success':
            transactions = transactions.filter(status='COMPLETED')
        else:
            transactions = transactions.filter(status=status.upper())

    max_insureds = 0
    for t in transactions:
        form_data = t.insured_details_json.get('form_data', {})
        sl = t.slab
        city = City.objects.get(id=t.extra['city']['id'])
        address = form_data.get('proposer', {}).get('address', {})
        row = [
            '%s %s' % (t.proposer_first_name, t.proposer_last_name),
            t.proposer_date_of_birth.strftime(
                "%d-%b-%Y") if t.proposer_date_of_birth else '',
            t.proposer_mobile,
            t.proposer_email,
            sl.health_product.insurer.title,
            sl.health_product.title,
            sl.sum_assured,
            sl.deductible,
            address.get('city', {}).get('display_value', '') or city.name,
            address.get('state', {}).get(
                'display_value', '') or city.state.name,
            t.premium_paid,
            t.payment_on,
            t.policy_number,
            t.policy_token,
            t.reference_id,
            t.status,
            t.transaction_id,
            t.created_on.strftime("%d-%b-%Y %H:%M"),
            t.updated_on.strftime("%d-%b-%Y %H:%M"),
        ]
        insureds = form_data.get('insured', {})
        n_insureds = 0
        for k in insureds:
            insured = insureds[k]
            name = '%s %s' % (insured.get('first_name', {}).get(
                'value', ''), insured.get('last_name', {}).get('value', ''))
            dob = insured.get('date_of_birth', {}).get('value', '')
            if dob:
                dob = datetime.datetime.strptime(
                    dob, '%d/%m/%Y').strftime('%d-%b-%Y')
            row.extend([name, dob])
            n_insureds += 1
        max_insureds = max(max_insureds, n_insureds)
        rows.append(row)

    for i in range(1, max_insureds + 1):
        order.extend(['Insured-%i Name' % i, 'Insured-%i Date of birth' % i])

    file_name = 'Health_transactions_%s_%s.xls' % (
        status, datetime.datetime.now().strftime("%d-%b_%H%M"))
    return putils.render_excel(file_name, order, rows)


def get_city_by_pincode(request):
    if request.method == 'POST':
        msg = json.loads(request.POST['data'])
        user_city = hqueries.get_city_by_hospital_extrapolation(
            msg['pincode'])['name'] if msg.get('pincode') else ''
        parents_city = hqueries.get_city_by_hospital_extrapolation(
            msg['parents_pincode'])['name'] if msg.get('parents_pincode') else ''

        city_data = {
            'user_city': user_city,
            'parents_city': parents_city,
        }
        return HttpResponse(json.dumps(city_data))
    else:
        return HttpResponse(json.dumps({}))


def process_pincode(request):
    transaction_id = request.POST.get('transaction_id')
    pincode = request.POST.get('pincode')
    if transaction_id and pincode:
        transaction = get_object_or_404(Transaction, transaction_id=transaction_id)
        insurer_id = transaction.slab.health_product.insurer_id
        pin = Pincode.objects.filter(pincode=pincode)[0]
        insurer_form_data = import_module('health_product.insurer.%s.form_data' % (insurer_settings.INSURER_MAP[insurer_id]))
        location_dict, transaction = insurer_form_data.get_state_city_by_pincode(transaction, pin)
        return HttpResponse(json.dumps({
            'success': True,
            'new_state': location_dict['new_state'],
            'new_city_list': location_dict['new_city_list']
        }))
    else:
        return HttpResponse(json.dumps({
            'success': False
        }))
