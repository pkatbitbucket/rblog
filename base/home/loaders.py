from django.template import TemplateDoesNotExist
from django.template.loaders import app_directories

BaseAppLoader = app_directories.Loader

get_app_template_dirs = app_directories.get_app_template_dirs
app_template_dirs = get_app_template_dirs('templates')


class AppNamespaceLoader(BaseAppLoader):

    def load_template_source(self, template_name, template_dirs=None):
        # FIXME: Remove in Django 1.9
        if ':' in template_name:
            app_name, template_name = template_name.split(':', 1)
            if not template_dirs:
                template_dirs = (d for d in app_template_dirs if d.endswith(
                    '/%s/templates' % app_name))

        return super(AppNamespaceLoader, self).load_template_source(template_name,
                                                                    template_dirs)

    def get_template(self, template_name, template_dirs=None, skip=None):
        if ':' in template_name:
            app_name, template_name = template_name.split(':', 1)
            if not template_dirs:
                template_dirs = (d for d in app_template_dirs if d.endswith(
                    '/%s/templates' % app_name))

        return super(AppNamespaceLoader, self).get_template(template_name, template_dirs, skip)
