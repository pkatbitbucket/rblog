from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index', kwargs={'webview': False}),
    url(r'^(car|travel|health|two-wheeler|term)-insurance/$', views.product_page, name='product_page', kwargs={'webview': False}),
    url(r'^(two-wheeler)-insurance/product.*', views.product_page, name='twowheeler_fallback', kwargs={'webview': False}),
    url(r'^(two-wheeler)-insurance/buyplan.*', views.index, name='twowheeler_buy', kwargs={'webview': False}),
    url(r'^(two-wheeler)-insurance/results.*', views.index, name='twowheeler_results', kwargs={'webview': False}),
    url(r'^(travel)-insurance/multi-trip', views.product_page, name='travel_multi_year', kwargs={'webview': False}),
    url(r'^(health)-insurance/super-topup', views.product_page, name='health_supertopup_page', kwargs={'webview': False}),
    url(r'^home/(?P<match>.*)$', views.index, name='spa_home'),
    url(r'^webview/$', views.index, name='webview', kwargs={'webview': True}),
    url(r'^home-mobile/$', views.index_mobile, name='index_mobile'),
    # temp shit
    url(r'^components/(?P<match>.*)$', views.components, name='components')
]
