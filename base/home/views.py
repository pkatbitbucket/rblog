import re

from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from motor_product import models as motor_models


def product_page(request, product, webview):
    template_path = "layout/base_with_widget.html"
    if request.user_agent.is_mobile:
        if product == 'travel':
            template_path = "flatpages:flatpages/travel-insurance.html"
        elif webview:
            template_path = "home:home-mobile-webview.html"
        else:
            template_path = "home:home-mobile.html"
    data_context = {'product': product}
    response = render(request, template_path, context=data_context)
    return response

# temp shit
def components(request, match):
    template_path = "components/input-components.html"
    return render(request, template_path)


def index(request, product=None, webview=False, match=None):
    path_info = request.META.get('PATH_INFO', '')
    template_path = "home:home.html"
    if request.user_agent.is_mobile:
        if webview:
            template_path = "home:home-mobile-webview.html"
        else:
            template_path = "home:home-mobile.html"
    is_product_page = False
    hide_data_for = ['two-wheeler-insurance/buyplan', 'two-wheeler-insurance/results']
    if any(word in path_info for word in hide_data_for):
        is_product_page = True
        if request.user_agent.is_mobile:
            # Fallback handling for two wheeler mobile
            uuid_regex = re.compile('[.]*([a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12})[.]*')
            uuid_search = uuid_regex.search(request.META['PATH_INFO'])
            uuid_string = uuid_search.group()
            reverse_match_for = None
            if 'results' in path_info and uuid_string:
                reverse_match_for = 'quote_share_twowheeler'
                return HttpResponseRedirect(reverse(reverse_match_for, kwargs={'quote_id': uuid_string}))
            elif 'buyplan' in path_info and uuid_string:
                reverse_match_for = 'desktopForm'
                transaction = motor_models.Transaction.objects.get(transaction_id=uuid_string)
                return HttpResponseRedirect(reverse(reverse_match_for, kwargs={
                    'transaction_id': uuid_string,
                    'insurer_slug': transaction.insurer.slug,
                    'vehicle_type': 'twowheeler',
                }))
    if product == 'term':
        title = 'Term Insurance: Compare and Buy Plans Online | Coverfox.com'
        social_media_title = title
        description = 'Term insurance plans are low cost plans that provide full protection to your family members. '\
            'Compare term insurance plans from top insurers in India.'
    else:
        title = 'Compare & Buy Top Insurance Policies Online from Coverfox.com'
        social_media_title = 'Coverfox Insurance, India - Get Cover, Get Going'
        description = 'Coverfox is a place to search, compare, buy, manage, and most importantly to understand Insurance online.'
    # Hide content of the page if bike buyplan or results
    data_context = {
        'product': product,
        'description': description,
        'title': title,
        'social_media_title': social_media_title,
        'is_product_page': is_product_page,
    }
    response = render(request, template_path, context=data_context)
    response.set_cookie('vt_home_visited', 'Yes')

    if webview:
        response.set_cookie('webview', 'true')
    return response


def index_mobile(request):
    template_path = "home:home-mobile.html"
    response = render(request, template_path)
    response.set_cookie('vt_home_visited', 'Yes')
    return response


def about(request):
    return render(request, "home:about.html")


def shipping_and_delivering(request):
    return render(request, "home:shipping-and-delivering.html")


def privacy_policy(request):
    return render(request, "home:privacy_policy.html")


def cancellation_and_refund(request):
    return render(request, "home:cancellation_and_refund.html")


def terms_and_conditions(request):
    return render(request, "home:terms_and_conditions.html")


def get_started(request):
    return render(request, "home:get_started.html")
