from django.contrib import admin

from hrms.models import ReviewClass, HRUserInfo, ReviewPeriod, Holiday
from hrms.models import ReviewSection, ResponseSection, ReviewQuestion, ReviewQuestionChoice


@admin.register(Holiday)
class HolidayAdmin(admin.ModelAdmin):
    pass


@admin.register(ReviewClass)
class ReviewClassAdmin(admin.ModelAdmin):
    verbose_name_plural = "Review Classes"


@admin.register(HRUserInfo)
class HRUserInfoAdmin(admin.ModelAdmin):
    raw_id_fields = ["reporters", "peers", "member"]


@admin.register(ReviewPeriod)
class ReviewPeriodAdmin(admin.ModelAdmin):
    pass


@admin.register(ReviewSection)
class ReviewSectionAdmin(admin.ModelAdmin):
    pass


@admin.register(ResponseSection)
class ResponseSectionAdmin(admin.ModelAdmin):
    pass


@admin.register(ReviewQuestion)
class ReviewQuestionAdmin(admin.ModelAdmin):
    pass


@admin.register(ReviewQuestionChoice)
class ReviewQuestionChoiceAdmin(admin.ModelAdmin):
    pass
