from django.db.models import Q
from core.models import Employee
from hrms.models import (
    HRUserInfo, ReviewPeriod, ReviewSection,
    ResponseSection, ReviewQuestion, ReviewQuestionChoice, ReviewResponse
)


def hr_activate_company(company):
    """
    A company may already exist, but HRMS is not yet activated on it. This
    function will activate HRMS module on it.

    How do we know if HRMS is activated for a company already or not? Company
    model has hstore attribute that captures lots of information about the
    Company, including has_hrms=True/False. If this method is called with a
    company that already has HRMS activate, we do not do anything.

    This method looks at all existing employees of a company and creates
    """


def hr_add_employee_to_company(company, user):
    """
    Add an employee to a company.
    """


def get_latest_review_period(company, status=None):
    q = Q(company=company)
    if status is not None:
        q = q & Q(status=status)
    try:
        review_period = ReviewPeriod.objects.filter(q).latest('id')
    except ReviewPeriod.DoesNotExist:
        review_period = None
    return review_period


def get_project_section(review_period):
    section = ReviewSection.objects.get(
        Q(name='Projects') &
        Q(period=review_period) &
        (~Q(dynamic=0))
    )
    return section


def get_user_projects(user, review_period):
    section = get_project_section(review_period)
    projects = ResponseSection.objects.filter(
        section=section,
        user=user,
        responder=user,
    ).order_by('order')
    return projects


def save_user_projects(user, review_period, projects):
    saved_projects = get_user_projects(user, review_period)
    no_of_saved_projects = saved_projects.count()

    # update existing projects
    if no_of_saved_projects > 0:
        for i, project in enumerate(saved_projects):
            try:
                project.text = projects[i]
            except IndexError:
                return
            project.save()
        # if no of projects to be saved exceeds no of saved projects
        projects = projects[i + 1:]

    # create new projects
    for i, project in enumerate(projects, 1):
        section = get_project_section(review_period)
        order = no_of_saved_projects + i
        project = ResponseSection.objects.create(
            section=section, user=user, responder=user,
            text=project, order=order
        )


def get_user_info(user):
    """ Get HRUserInfo instance for user """
    # TODO: if customer works in multiple companies?
    member = Employee.objects.get(user=user, left_on=None)
    user_info = HRUserInfo.objects.get(member=member)
    return user_info


def json_for_user(reviewer, review_period):
    """
    {
        self: {

        },
        peers: [
            {
                "id": 123,
                "name": "Amit Upadhyay",
                "done": true,
                "sections": [
                    {
                        "id": 33,
                        "name": "project 1",
                        "dynamic": false,
                        "questions": [
                            {
                                "id": 444,
                                "text": "Was the project delivered on time?"
                                "choices": [
                                    {
                                        "id": 21,
                                        "text": "Yes of course"
                                    }
                                ],
                                "response": 21
                            }
                        ]
                    }
                ]
            }
        ],
        bosses: [],
        reporters: [],
    }
    """
    relations = ['self', 'peers', 'reporters', 'bosses']
    other_relations = relations[1:]
    for_relation_mapping = {
        'self': 'for_self',
        'peers': 'for_peers',
        'bosses': 'for_bosses',
        'reporters': 'for_reporters',
    }

    # Prepare a dict of related members' info with their relation to the user
    member = Employee.objects.get(user=reviewer, left_on=None)
    reviewer_info = HRUserInfo.objects.get(member=member)
    all_members = {
        relation: getattr(reviewer_info, relation).all()
        for relation in other_relations
    }
    all_members['self'] = [reviewer_info]

    review_set = {}
    for relation in all_members:
        related_members_details = []
        members_info = all_members[relation]
        for_relation = for_relation_mapping[relation]
        for reviewed_info in members_info:
            reviewed = reviewed_info.member.user
            review_class = reviewed_info.review_class
            sections = get_review_sections(reviewer, reviewed, review_class,
                                           for_relation, review_period)
            review_done = all_sections_done(sections)
            member_details = {
                'id': reviewed.id,
                'name': reviewed.get_full_name(),
                'title': reviewed_info.member.title,
                'sections': sections,
                'done': review_done,
            }
            related_members_details.append(member_details)

        review_set[relation] = related_members_details

    review_set['self'] = review_set['self'][0]

    return review_set


def get_review_sections(reviewer, reviewed, review_class,
                        for_relation, review_period):
    review_section_filters = {
        'period': review_period,
    }
    review_section_filters[for_relation] = True
    review_sections = ReviewSection.objects.filter(**review_section_filters)

    all_sections = []
    for review_section in review_sections:
        if review_section.dynamic == 0:
            dynamic = False
            section_desc = (review_section, dynamic)
            all_sections.append(section_desc)
        else:
            response_sections = ResponseSection.objects.filter(
                user=reviewed,
                section=review_section
            )
            dynamic = True
            for response_section in response_sections:
                section_desc = (response_section, dynamic)
                all_sections.append(section_desc)

    review_sections_details = []
    for section_desc in all_sections:
        section = section_desc[0]
        dynamic = section_desc[1]
        section_details = get_section_details(
            reviewer, reviewed, review_class,
            section, for_relation, dynamic
        )
        review_sections_details.append(section_details)
    return review_sections_details


def get_section_details(
    reviewer, reviewed, review_class, section, for_relation, dynamic
):
    questions = get_review_questions(
        reviewer, reviewed, review_class,
        section, for_relation, dynamic
    )
    section_done = all_section_questions_done(questions)

    if dynamic:
        response_section = section
        section = response_section.section
        section_id = response_section.id
        section_name = '%s: %s' % (section.name, response_section.text)
    else:
        section_id = section.id
        section_name = section.name

    section_details = {
        'id': section_id,
        'dynamic': dynamic,
        'name': section_name,
        'questions': questions,
        'done': section_done,
    }
    return section_details


def get_review_questions(
    reviewer, reviewed, review_class, section, for_relation, dynamic
):
    if dynamic:
        response_section = section
        section = response_section.section
    else:
        response_section = None

    review_question_filters = {
        'section': section,
        'classes': review_class,
    }
    review_question_filters[for_relation] = True
    review_questions = ReviewQuestion.objects.filter(**review_question_filters)
    questions_details = [
        get_question_details(
            reviewer, reviewed, question, (
                section if not dynamic else response_section
            ), dynamic
        )
        for question in review_questions
    ]
    return questions_details


def get_question_details(reviewer, reviewed, question, section, dynamic):
    if dynamic:
        response_section = section
        section = response_section.section
    else:
        response_section = None

    choices = [
        {"id": choice.id, "text": choice.text}
        for choice in question.choices.all()
    ]

    answer = ReviewResponse.objects.filter(
        user=reviewed,
        responder=reviewer,
        response__question=question,
        response_section=response_section,
    )
    if len(answer) > 1:
        raise Exception("Integrity error for question: %s" % question)
    answer = answer[0].response.id if answer else None

    question_details = {
        "id": question.id,
        "text": question.text,
        "choices": choices,
        "answer": answer,
    }
    return question_details


def all_section_questions_done(questions):
    answered = [(question['answer'] is not None) for question in questions]
    done = all(answered)
    return done


def all_sections_done(sections):
    section_statuses = [section['done'] for section in sections]
    done = all(section_statuses)
    return done


def save_responses(reviewer, reviewed, data):
    """
    data = [
        {
            "question_id": 12,
            "section_id": 123,
            "response_id": 123,
            "dynamic": false,
        }
    ]
    """
    error=""
    for user_response in data:
        question = ReviewQuestion.objects.get(id=user_response['question_id'])
        try:
            answer = ReviewQuestionChoice.objects.get(
            id=user_response['response_id'])
        except ReviewQuestionChoice.DoesNotExist:
            error = "You have not answered all review question"
            return {'success': False, 'error':error}

        if user_response['dynamic']:
            response_section = ResponseSection.objects.get(
                id=user_response['section_id']
            )
        else:
            response_section = None

        response_kwargs = {
            'user': reviewed,
            'responder': reviewer,
            'question': question,
            'response_section': response_section,
        }
        try:
            review_response = ReviewResponse.objects.get(**response_kwargs)
            review_response.response = answer
            review_response.save()
        except ReviewResponse.DoesNotExist:
            response_kwargs['response'] = answer
            ReviewResponse.objects.create(**response_kwargs)

    return {'success': True}
