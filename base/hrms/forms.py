from django import forms
from django.contrib.auth.decorators import permission_required
from django.core.urlresolvers import reverse
from django.db import transaction
from django.forms import widgets
from django.forms.extras.widgets import SelectDateWidget
from django.http import HttpResponseRedirect

import datetime
from collections import OrderedDict

from importd import d

from core.models import User, Employee, Email
from core.fields import LabelledModelChoiceField
from hrms.models import HRUserInfo, ReviewClass, Holiday
from hrms import utils


def get_text_char_field(**args):
    return forms.CharField(
        widget=widgets.TextInput(
            attrs={"class": "form-control"},
        ),
        **args
    )


class UserForm(forms.ModelForm):
    first_name = get_text_char_field()
    last_name = get_text_char_field()
    middle_name = get_text_char_field(required=False)
    phone = get_text_char_field()
    birth_date = get_text_char_field()

    class Meta:
        model = User
        fields = ["first_name", "middle_name", "last_name", "birth_date", "phone"]


class HRUserInfoForm(forms.ModelForm):
    bio_markdown = forms.CharField(
        widget=forms.Textarea(
            attrs={"class": "form-control"}
        ),
        required=False
    )
    team = get_text_char_field()
    alias_name = get_text_char_field()
    username = forms.RegexField(
        regex=r'^((^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)|([\w]+\.[\w]+)|([\w]+))$',
        widget=widgets.TextInput(
            attrs={"class": "form-control"},
        ),
    )

    def __init__(self, *args, **kwargs):
        self.extra = kwargs.pop('extra', {})
        super(HRUserInfoForm, self).__init__(*args, **kwargs)

    def clean_username(self):
        request = self.extra.get("request")
        username = self.cleaned_data["username"]

        if self.instance.username == username:
            return username

        hr_user = HRUserInfo.objects.filter(
            username=username,
            member__company=request.company
        ).count()

        if hr_user:
            raise forms.ValidationError("Username is already taken")

        return username

    class Meta:
        model = HRUserInfo
        fields = ["username", "alias_name", "bio_markdown", "team", "profile_pic"]


@d(
    "^holidays/new/$", name="add_holiday",
    template="hrms/holidays/new.html",
    decorator=permission_required("hrms.add_holiday")
)
class AddHoliday(d.RequestForm, object):
    holiday = forms.CharField(max_length=50)
    date = forms.DateField()

    def __init__(self, *args, **kw):
        super(AddHoliday, self).__init__(*args, **kw)
        self.is_hols = True

    def save(self):
        Holiday.objects.create(
            holiday=self.cleaned_data["holiday"],
            date=self.cleaned_data["date"],
            company=self.request.company
        )
        return HttpResponseRedirect("/holidays/")


@d(
    "/member/add/",
    template="hrms/member_edit.html",
    name="member-add"
)
@d(
    "/member/<int:member_id>/edit/",
    template="hrms/member_edit.html",
    name="member-edit"
)
class MemberAdminForm(forms.Form):
    """
    Form to add or edit a member (Employee).
    A member's data is stored in 3 different models:
        1) User
        2) Employee
        3) HRUserInfo
    """

    YEAR_RANGE = range(2000, datetime.date.today().year + 1)

    email = forms.EmailField()
    employee_id = forms.CharField(label="Employee ID", max_length=200)
    first_name = forms.CharField(max_length=50)
    middle_name = forms.CharField(max_length=50, required=False)
    last_name = forms.CharField(max_length=50)
    employee_title = forms.CharField(max_length=200, required=False)
    joined_on = forms.DateField(widget=SelectDateWidget(years=YEAR_RANGE))
    left_on = forms.DateField(widget=SelectDateWidget(years=YEAR_RANGE),
                              required=False)
    profile_pic = forms.ImageField(required=False)

    def __init__(self, request):
        self.request = request
        self.company = request.company

    def init(self, member_id=None):
        request = self.request
        company = self.company

        if member_id:
            member = Employee.objects.get(id=member_id)
        else:
            member = None
        self.member = member

        hruserinfo = None
        colleagues = HRUserInfo.objects.filter(member__company=company)
        colleagues = colleagues.select_related('member__user')
        initial = {}

        if (request.method == 'GET') and member:
            hruserinfo = member.hruserinfo
            colleagues = colleagues.exclude(id=hruserinfo.id)
            initial.update({
                'email': member.user.email,
                'employee_id': member.employee_id,
                'first_name': member.user.first_name,
                'middle_name': member.user.middle_name,
                'last_name': member.user.last_name,
                'employee_title': member.title,
                'joined_on': member.joined_on,
                'left_on': member.left_on,
                'review_class': hruserinfo.review_class_id,
                'peers': [u[0] for u in hruserinfo.peers.values_list('id')],
                'bosses': [u[0] for u in hruserinfo.bosses.values_list('id')],
                'profile_pic': hruserinfo.profile_pic,
            })
        initial['company'] = company.name

        super(self.__class__, self).__init__(
            request.POST or None,
            request.FILES or None,
            initial=initial
        )

        # company is always set to logged in user's company
        self.fields['company'] = forms.CharField(required=False)
        self.fields['review_class'] = LabelledModelChoiceField(
            ReviewClass.objects.filter(company=company),
            field_label=lambda obj: obj.name,
        )
        self.fields['peers'] = forms.ModelMultipleChoiceField(
            colleagues, required=False,
        )
        self.fields['bosses'] = forms.ModelMultipleChoiceField(
            colleagues, required=False,
        )

        # Order dynamically created fields
        self.order_fields()

        # Set widget attributes (disabled, required)
        self.fields['company'].widget.attrs['readonly'] = True
        if member_id:
            self.fields['email'].widget.attrs['readonly'] = True

        for field_name, field in self.fields.items():
            if field.required:
                field.widget.attrs['required'] = True

    def clean(self):
        if self.cleaned_data['left_on'] is not None:
            if self.cleaned_data['left_on'] < self.cleaned_data['joined_on']:
                self.add_error('left_on', 'Employee cannot leave before joining')

        email = self.cleaned_data.get('email')
        # If new member is being added, check if user exists for that email
        if (not self.member) and email:
            try:
                user = User.objects.get_user(self.cleaned_data['email'], '')
                # Check company membership.
                user_company_members = Employee.objects.filter(
                    user=user,
                    company=self.request.company,
                    left_on=None
                )
                if len(user_company_members) >= 1:
                    err_msg = "User already exists for the provided email address." \
                              " He is already a member of company."
                    self.errors.clear()
                    raise forms.ValidationError(
                        err_msg
                    )
            except Email.DoesNotExist:
                pass

    @transaction.atomic
    def save(self):
        data = self.cleaned_data
        if not self.member:       # TODO
            user, _ = User.objects.get_or_create(
                email=data['email'],
                defaults={'email': data["email"]}
            )
            member = Employee.objects.create(
                company=self.company,
                user=user,
                joined_on=data['joined_on'],
                left_on=data['left_on']
            )
        else:
            user = self.member.user
            member = self.member

        user.email = data['email']
        user.first_name = data['first_name']
        user.middle_name = data['middle_name']
        user.last_name = data['last_name']
        user.save()

        member.employee_id = data['employee_id']
        member.title = data['employee_title']
        member.joined_on = data['joined_on']
        member.left_on = data['left_on']
        member.save()
        self.member = member

        try:
            hruserinfo = HRUserInfo.objects.get(member=member)
            hruserinfo.review_class = data['review_class']
            hruserinfo.user_id = user.id
            hruserinfo.company_id = member.company.id
            hruserinfo.username = utils.id_generator()
            hruserinfo.save()
        except HRUserInfo.DoesNotExist:
            hruserinfo = HRUserInfo.objects.create(
                member=member,
                review_class=data['review_class']
            )
            hruserinfo.username = utils.id_generator()
            hruserinfo.user_id = user.id
            hruserinfo.company_id = member.company.id

        hruserinfo.peers.clear()
        hruserinfo.peers.add(*data['peers'])
        hruserinfo.bosses.clear()
        hruserinfo.bosses.add(*data['bosses'])
        if data['profile_pic'] is False:
            hruserinfo.profile_pic = None
        elif data['profile_pic'] is not None:
            hruserinfo.profile_pic = data['profile_pic']
        hruserinfo.save()
        return reverse('member-edit', args=(self.member.id,))

    def order_fields(self):
        order = [
            'company', 'email', 'employee_id', 'first_name', 'middle_name',
            'last_name', 'employee_title', 'joined_on', 'left_on',
            'review_class', 'peers', 'bosses', 'profile_pic',
        ]
        fields = self.fields.copy()
        ordered_fields = OrderedDict([
            (key, fields.pop(key)) for key in order
            if key in self.fields
        ])
        ordered_fields.update(fields)
        self.fields = ordered_fields

    def get_grouped_fields(self, max_group_count=3):
        """
        Returns fields in a specific order in specified no of groups
        (for easy grouping in templates)
        """
        grouped_fields = []
        group, count = [], 0
        for field in self:
            group.append(field)
            count += 1
            if count == max_group_count:
                grouped_fields.append(group)
                group, count = [], 0
        if group:
            grouped_fields.append(group)
        return grouped_fields
