from django.conf import settings
from django.http import HttpResponseRedirect

from core import models as cmodels


class RequestCompanyMiddleware(object):
    """
    Our humanlly.com deployment handles two kinds of pages.

    1. humanlly.com/foo/bar and
    2. company_name.humanlly.com/foo/bar

    In former case request.company would be None, in later case we will check
    if company exits, if not we will redirect to humanlly.com. If company exists
    then request.company will point to right Company object.

    Further if request.user is known to be not part of some company, then we
    will redirect them to either their company, or to humanlly.com.

    If user is not logged in but on company page, we will redirect them to
    company's login page.
    """
    def process_request(self, request):
        # Get sub domain and domain from request uri.
        # assert "HTTP_HOST" in request.META  # this is a mandatory field

        request.company = None

        if "HTTP_HOST" not in request.META:
            return

        HOST = request.META["HTTP_HOST"].split(":")[0]

        if HOST.lower() in [settings.HUMANLLY_DOMAIN, "www"]:
            # Q: how to view humanlly.com pages on localhost? A: /etc/hosts
            return

        DOMAIN = HOST.split(".", 1)[0]
        request.company = cmodels.Company.objects.filter(slug=DOMAIN).first()

        if not request.company and "localhost" in HOST and settings.DEBUG:
            request.company = cmodels.Company.objects.first()
        if not request.company:
            return HttpResponseRedirect("http://%s/" % settings.HUMANLLY_DOMAIN)

        # if user is not logged in take them to login page with right next
        if (
            request.user.is_anonymous()
            and not request.path.startswith("/login")
            and not request.path.startswith("/admin")
            and not request.path.startswith("/git")
        ):
            query_param = '&'.join(["%s=%s" % (item[0], item[1]) for item in request.GET.items()])
            if query_param:
                query_param = "&" + query_param

            return HttpResponseRedirect("/login/?next=%s%s" % (request.path, query_param))
        else:
            return
        # if user is from a different company
        #   - take them to their company page if possible
        #   - if no company, take user to appropriate landing page

        if request.company.is_employee(request.user, current=True):
            return

        user_companies = request.user.get_companies(current=True)
        if not user_companies:
            return HttpResponseRedirect("http://%s" % settings.HUMANLLY_DOMAIN)

        return HttpResponseRedirect(
            "http://%s.%s" % (user_companies[0].slug, settings.HUMANLLY_DOMAIN)
        )
