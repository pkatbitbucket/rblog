# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('coverfox', '0002_auto_20150817_1420'),
    ]

    operations = [
        migrations.CreateModel(
            name='HRUserInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('member', models.ForeignKey(help_text=b'\n        Why are we ForeignKey-ing to this instead of User? Because a user can be\n        part of more than one companies over a period of time.\n\n        Why cant then we just add a ForeignKey to Company too? Because our\n        CompanyMembership is written with assumption that you could be member of\n        same company twice.\n\n        Review page should only include people with CompanyMembership.left_on ==\n        null.\n\n        It is assumed that in one review period an employee has been part of a\n        Company only once.\n        ', to='coverfox.CompanyMembership')),
                ('peers', models.ManyToManyField(help_text=b'\n            These are the "peers" of an employee. In review_cf system, the peers\n            are rated by the employee, and those guys rate this person.\n\n            We want symmetrical=True here, but that only works when User has\n            FKing to User. So we have to implement symmetrical programmatically.\n        ', to=settings.AUTH_USER_MODEL)),
                ('reporters', models.ManyToManyField(help_text=b'\n            This is the list of people who report to this user. .bosses is the\n            reverse of this, whom this guy reports to.\n        ', related_name='bosses', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ResponseSection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField(help_text=b'\n            The question that we will show to reviewers. This field may be a\n            rich field.\n        ')),
                ('order', models.IntegerField(default=0, help_text=b'This is used for ordering purpose.')),
                ('responder', models.ForeignKey(related_name='response_section_set', to=settings.AUTH_USER_MODEL, help_text=b'The person who created this response.')),
            ],
        ),
        migrations.CreateModel(
            name='ReviewClass',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(blank=True)),
                ('company', models.ForeignKey(to='coverfox.Company')),
            ],
        ),
        migrations.CreateModel(
            name='ReviewPeriod',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('period', models.CharField(help_text=b'\n            Each review is conducted over a period, some quarterly, some half\n            yearly and so on. Ideally we should have a better abstraction to\n            capture date range, but for now char field it is.\n\n            It would be hard to sort with charfield, so if we go ahead with\n            this we need an order column.\n        ', max_length=40)),
                ('is_open', models.BooleanField(default=True, help_text=b'\n            This checks if the review is open. Only one review period can be\n            open at a time.\n        ')),
                ('company', models.ForeignKey(to='coverfox.Company')),
            ],
        ),
        migrations.CreateModel(
            name='ReviewQuestion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField(help_text=b'\n            The question that we will show to reviewers. This field may be a\n            rich field.\n        ')),
                ('order', models.IntegerField(default=0, help_text=b'This is used for ordering purpose.')),
                ('weightage', models.IntegerField(default=0)),
                ('classes', models.ManyToManyField(help_text=b'\n            Not all questions may be applicable to every employee.\n        ', to='hrms.ReviewClass')),
            ],
        ),
        migrations.CreateModel(
            name='ReviewQuestionChoice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField()),
                ('order', models.IntegerField(default=0, help_text=b'This is used for ordering purpose.')),
                ('question', models.ForeignKey(to='hrms.ReviewQuestion')),
            ],
        ),
        migrations.CreateModel(
            name='ReviewResponse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.ForeignKey(help_text=b'\n            The question on which this person is being rated.\n        ', to='hrms.ReviewQuestion')),
                ('responder', models.ForeignKey(related_name='responses_set', to=settings.AUTH_USER_MODEL, help_text=b'The person who created this response.')),
                ('response', models.ForeignKey(to='hrms.ReviewQuestionChoice')),
                ('response_section', models.ForeignKey(to='hrms.ResponseSection', help_text=b'\n            This would be non null only in case of dynamic section.\n        ', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ReviewSection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Each question belongs to a section.', max_length=100)),
                ('order', models.IntegerField(default=0, help_text=b'\n            This is used for ordering purpose.\n        ')),
                ('weightage', models.IntegerField(default=0)),
                ('dynamic', models.IntegerField(default=0, help_text=b'\n            This is interesting. What happens is sometimes we have the whole\n            section repeated more than once. Say we have a bunch of questions\n            about how your projects went, but we allow employee to enter one or\n            more projects, so we ask same questions about each project.\n\n            If this field is zero, then this is not repeated. If this is non\n            zero, then this may be either upto this many times repeated, or\n            everyone has to enter at least this many. Since this is right now\n            optimized for coverfox where we are doing upto 3, so this is what we\n            are going with.\n        ')),
                ('classes', models.ManyToManyField(help_text=b'\n            Not all sections may be applicable to every employee.\n        ', to='hrms.ReviewClass')),
                ('period', models.ForeignKey(to='hrms.ReviewPeriod')),
            ],
        ),
        migrations.AddField(
            model_name='reviewquestion',
            name='section',
            field=models.ForeignKey(to='hrms.ReviewSection'),
        ),
        migrations.AddField(
            model_name='responsesection',
            name='section',
            field=models.ForeignKey(to='hrms.ReviewSection'),
        ),
        migrations.AddField(
            model_name='responsesection',
            name='user',
            field=models.ForeignKey(help_text=b'This is the guy who worked on those projects.', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='hruserinfo',
            name='review_class',
            field=models.ForeignKey(help_text=b'\n            In review_cf system, every employee belongs to a review_class, and\n            based on review_class we select different questions.\n        ', to='hrms.ReviewClass'),
        ),
    ]
