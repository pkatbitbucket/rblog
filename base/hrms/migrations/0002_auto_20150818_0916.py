# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('hrms', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviewquestion',
            name='for_peers',
            field=models.BooleanField(default=True, help_text=b'\n            If the question should be visible when filling form for peers.\n        '),
        ),
        migrations.AddField(
            model_name='reviewquestion',
            name='for_self',
            field=models.BooleanField(default=True, help_text=b'\n            If the section should be visible to self, that is when filling the\n            review for yourself.\n        '),
        ),
        migrations.AddField(
            model_name='reviewquestion',
            name='for_subordinates',
            field=models.BooleanField(default=True, help_text=b'\n            If the question should be visible when filling form for\n            subordinates.\n        '),
        ),
        migrations.AddField(
            model_name='reviewquestion',
            name='for_supervisors',
            field=models.BooleanField(default=True, help_text=b'\n            If the question should be visible when filling form for supervisor.\n        '),
        ),
        migrations.AddField(
            model_name='reviewsection',
            name='for_peers',
            field=models.BooleanField(default=True, help_text=b'\n            If the question should be visible when filling form for peers.\n        '),
        ),
        migrations.AddField(
            model_name='reviewsection',
            name='for_self',
            field=models.BooleanField(default=True, help_text=b'\n            If the section should be visible to self, that is when filling the\n            review for yourself.\n        '),
        ),
        migrations.AddField(
            model_name='reviewsection',
            name='for_subordinates',
            field=models.BooleanField(default=True, help_text=b'\n            If the question should be visible when filling form for\n            subordinates.\n        '),
        ),
        migrations.AddField(
            model_name='reviewsection',
            name='for_supervisors',
            field=models.BooleanField(default=True, help_text=b'\n            If the question should be visible when filling form for supervisor.\n        '),
        ),
        migrations.AlterField(
            model_name='hruserinfo',
            name='peers',
            field=models.ManyToManyField(help_text=b'\n            These are the "peers" of an employee. In review_cf system, the peers\n            are rated by the employee, and those guys rate this person.\n\n            We want symmetrical=True here, but that only works when User has\n            FKing to User. So we have to implement symmetrical programmatically.\n        ', to=settings.AUTH_USER_MODEL, blank=True),
        ),
        migrations.AlterField(
            model_name='hruserinfo',
            name='reporters',
            field=models.ManyToManyField(help_text=b'\n            This is the list of people who report to this user. .bosses is the\n            reverse of this, whom this guy reports to.\n        ', related_name='bosses', to=settings.AUTH_USER_MODEL, blank=True),
        ),
        migrations.AlterField(
            model_name='reviewquestion',
            name='classes',
            field=models.ManyToManyField(help_text=b'\n            Not all questions may be applicable to every employee.\n        ', to='hrms.ReviewClass', blank=True),
        ),
    ]
