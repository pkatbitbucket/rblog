# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hrms', '0002_auto_20150818_0916'),
    ]

    operations = [
        migrations.RenameField(
            model_name='reviewquestion',
            old_name='for_supervisors',
            new_name='for_bosses',
        ),
        migrations.RenameField(
            model_name='reviewquestion',
            old_name='for_subordinates',
            new_name='for_reporters',
        ),
        migrations.RenameField(
            model_name='reviewsection',
            old_name='for_supervisors',
            new_name='for_bosses',
        ),
        migrations.RenameField(
            model_name='reviewsection',
            old_name='for_subordinates',
            new_name='for_reporters',
        ),
        migrations.AlterField(
            model_name='reviewquestionchoice',
            name='question',
            field=models.ForeignKey(related_name='choices', to='hrms.ReviewQuestion'),
        ),
    ]
