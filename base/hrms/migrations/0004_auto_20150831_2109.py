# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hrms', '0003_auto_20150824_1427'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hruserinfo',
            name='peers',
            field=models.ManyToManyField(help_text=b'\n            These are the "peers" of an employee. In review_cf system, the peers\n            are rated by the employee, and those guys rate this person.\n\n            We want symmetrical=True here, but that only works when User has\n            FKing to User. So we have to implement symmetrical programmatically.\n        ', related_name='peers_rel_+', to='hrms.HRUserInfo', blank=True),
        ),
        migrations.AlterField(
            model_name='hruserinfo',
            name='reporters',
            field=models.ManyToManyField(help_text=b'\n            This is the list of people who report to this user. .bosses is the\n            reverse of this, whom this guy reports to.\n        ', related_name='reporters_rel_+', to='hrms.HRUserInfo', blank=True),
        ),
    ]
