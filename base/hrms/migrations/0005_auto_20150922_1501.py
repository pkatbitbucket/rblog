# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from cf_s3field.fields import S3ImageField

class Migration(migrations.Migration):

    dependencies = [
        ('hrms', '0004_auto_20150831_2109'),
    ]

    operations = [
        migrations.AddField(
            model_name='hruserinfo',
            name='bio_markdown',
            field=models.TextField(default=b'', help_text=b'\n        Bio markdown field contains text of user bio.\n        ', blank=True),
        ),
        migrations.AddField(
            model_name='hruserinfo',
            name='profile_pic',
            field=S3ImageField(default='', max_length=255, upload_to=b''),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='hruserinfo',
            name='reporters',
            field=models.ManyToManyField(help_text=b'\n            This is the list of people who report to this user. .bosses is the\n            reverse of this, whom this guy reports to.\n        ', related_name='bosses', to='hrms.HRUserInfo', blank=True),
        ),
        migrations.AlterField(
            model_name='reviewperiod',
            name='period',
            field=models.CharField(help_text=b'\n            Each review is conducted over a period, some quarterly, some half\n            yearly and so on. Ideally we should have a better abstraction to\n            capture date range, but for now char field it is.\n\n            It would be hard to sort with char field, so if we go ahead with\n            this we need an order column.\n        ', max_length=40),
        ),
    ]
