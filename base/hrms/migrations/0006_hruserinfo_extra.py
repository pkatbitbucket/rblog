# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import django
from django.db import models, migrations
from django.contrib.postgres.operations import HStoreExtension

class Migration(migrations.Migration):

    dependencies = [
        ('hrms', '0005_auto_20150922_1501'),
    ]

    operations = [
        HStoreExtension(),
        migrations.AddField(
            model_name='hruserinfo',
            name='extra',
            field=django.contrib.postgres.fields.hstore.HStoreField(default={}),
        ),
    ]
