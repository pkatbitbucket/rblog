# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hrms', '0007_auto_20150929_1653'),
    ]

    operations = [
        migrations.AddField(
            model_name='hruserinfo',
            name='team',
            field=models.CharField(max_length=150, blank=True),
        ),
    ]
