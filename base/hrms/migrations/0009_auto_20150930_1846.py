# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('hrms', '0008_hruserinfo_team'),
    ]

    operations = [
        migrations.AddField(
            model_name='hruserinfo',
            name='alias_name',
            field=models.CharField(max_length=35, blank=True),
        ),
    ]
