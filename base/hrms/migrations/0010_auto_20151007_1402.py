# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hrms', '0009_auto_20150930_1846'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reviewperiod',
            name='is_open',
        ),
        migrations.AddField(
            model_name='reviewperiod',
            name='status',
            field=models.CharField(default=b'CLOSED', help_text=b'\n            This checks the status of the review. Only one review period of a\n            company can be non-closed at a time.\n        ', max_length=20, choices=[(b'data-entry', b'Open for data entry'), (b'open', b'Open for reviews'), (b'closed', b'Closed')]),
        ),
    ]
