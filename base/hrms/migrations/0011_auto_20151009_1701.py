# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields.hstore
from cf_s3field.fields import S3ImageField


class Migration(migrations.Migration):

    dependencies = [
        ('hrms', '0010_auto_20151007_1402'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hruserinfo',
            name='extra',
            field=django.contrib.postgres.fields.hstore.HStoreField(default={}, blank=True),
        ),
        migrations.AlterField(
            model_name='hruserinfo',
            name='profile_pic',
            field=S3ImageField(max_length=255, null=True, upload_to=b'', blank=True),
        ),
    ]
