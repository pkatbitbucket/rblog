# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coverfox', '0007_auto_20151014_2032'),
        ('hrms', '0011_auto_20151009_1701'),
    ]

    operations = [
        migrations.CreateModel(
            name='Holiday',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('holiday', models.CharField(max_length=50)),
                ('date', models.DateField()),
                ('company', models.ForeignKey(to='coverfox.Company')),
            ],
        ),
    ]
