# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import cf_s3field.fields


def copy_username(apps, schema_editor):
    """
    Copy username from user model to hruserinfo model to make username company specific unique.
    """
    print("\n") # noqa
    print("HrUserInfo data migration started") # noqa
    hr_user_info_model = apps.get_model("hrms", "HrUserInfo")
    for hr_user in hr_user_info_model.objects.all():
        hr_user.username = hr_user.member.user.username
        print("Updated username(%s)" % hr_user.username) # noqa
        hr_user.save()


class Migration(migrations.Migration):

    dependencies = [
        ('hrms', '0013_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='hruserinfo',
            name='username',
            field=models.CharField(default='username', max_length=255, verbose_name=b'username', db_index=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='hruserinfo',
            name='profile_pic',
            field=cf_s3field.fields.S3ImageField(storage=cf_s3field.fields.S3Storage(b''), max_length=255, null=True, upload_to=b'', blank=True),
        ),
        migrations.AlterField(
            model_name='hruserinfo',
            name='reporters',
            field=models.ManyToManyField(help_text=b'\n        This is the list of people who report to this user. .bosses is the\n        reverse of this, whom this guy reports to.\n        ', related_name='bosses', to='hrms.HRUserInfo', blank=True),
        ),
        migrations.RunPython(copy_username),
    ]
