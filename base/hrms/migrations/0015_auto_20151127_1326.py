# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from hrms import utils

def modify_blank_username(apps, schema_editor):
    hr_user_info = apps.get_model("hrms", "HRUserInfo")
    hr_users = hr_user_info.objects.filter(username='')
    for hr_user in hr_users:
        username = utils.id_generator()
        hr_user.username = username
        hr_user.save()
        print("HRUser (%s) modified. Added username (%s)" % (hr_user.id, username))

class Migration(migrations.Migration):

    dependencies = [
        ('hrms', '0014_auto_20151103_1221'),
    ]

    operations = [
        migrations.RunPython(modify_blank_username),
    ]
