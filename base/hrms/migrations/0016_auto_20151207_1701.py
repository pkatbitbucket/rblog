# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hrms', '0015_auto_20151127_1326'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hruserinfo',
            name='member',
            field=models.OneToOneField(to='core.Employee', help_text=b'\n        Why are we ForeignKey-ing to this instead of User? Because a user can be\n        part of more than one companies over a period of time.\n\n        Why cant then we just add a ForeignKey to Company too? Because our\n        CompanyMembership is written with assumption that you could be member of\n        same company twice.\n\n        Review page should only include people with CompanyMembership.left_on ==\n        null.\n\n        It is assumed that in one review period an employee has been part of a\n        Company only once.\n        '),
        ),
    ]
