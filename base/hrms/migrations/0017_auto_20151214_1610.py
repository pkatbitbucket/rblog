# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hrms', '0016_auto_20151207_1701'),
    ]

    operations = [
        migrations.AlterField(
            model_name='holiday',
            name='company',
            field=models.ForeignKey(to='core.Company'),
        ),
        migrations.AlterField(
            model_name='reviewclass',
            name='company',
            field=models.ForeignKey(to='core.Company'),
        ),
        migrations.AlterField(
            model_name='reviewperiod',
            name='company',
            field=models.ForeignKey(to='core.Company'),
        ),
    ]
