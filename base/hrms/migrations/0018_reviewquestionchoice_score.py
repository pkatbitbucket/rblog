# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hrms', '0017_auto_20151214_1610'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviewquestionchoice',
            name='score',
            field=models.IntegerField(default=0, help_text=b'Score for this option'),
        ),
    ]
