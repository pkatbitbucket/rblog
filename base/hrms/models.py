"""
This app currently captures only basic of what we need to conduct reviews at
glitterbug.
"""
from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import HStoreField
from django.core.cache import cache
from django.dispatch import receiver
from django.db.models.signals import post_save

from core.models import User
from cf_s3field.fields import S3ImageField
from core.decorators import cacheable
from core.models import Company, Employee
from hrms import utils


class ReviewClass(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    company = models.ForeignKey(Company)

    def __unicode__(self):
        return "{} for {}".format(self.name, self.company)


class HRUserInfo(models.Model):
    """
    HRUserInfo model keeps track of a user's Human resource related information.

    Some of our users will have access to our HR features. This starts from
    coverfox.Company. If a user is an employee of a company, and if the company
    has activated HR module, then this row may have information about that user.

    Code written should assume that we may not have a row for those users in
    this table.

    When the HRMS module is activated, then rows would be created for existing
    employees of the company. This is managed by
    Company.activate_hrms("module"). We may have multiple modules that can be
    activated for a company, "review_cf" is one example. "review_cf" is the
    review system used by coverfox.
    We can possibly make this entire model hstore info in CompanyMembership. The
    pro and con of that is about where we want to do index and joins.


    We do not have to do many joins etc on this table. Once a user is checking
    their information, or when a manager is seeing information about the users
    they manage, would this table be queried. But since in that case the
    CompanyMembership object of the manager would already be loaded, we can
    simply do a hstore lookup in it to see who all are reporters/reportee for
    that user. Same with peers. Both lookups are actually constant time lookups.

    If we wanted to do a aggregate query, say show a page with all employees,
    and their ratings, then it becomes interesting. In that case we may end up
    doing serial scan. But if that serial scan is good or bad depends on how we
    are planning to generate that page. If we generate it by loading up all
    users in python code, and doing a .compute_score() on them one by one, then
    even with hstore it would be as bad as without.

    But if we created a SQL funciton or a query to compute the score, then it
    would matter if that SQL can do hstore lookups or not.

    And even with hstore I think we can put indexes and get the same performance
    in case we do SQL query. So where does it really matter if we do
    ManyToManyField and model like this?

    It matters a bit on developer familiarity. People know ManyToManyField in
    django. They do not know how to capture ManyToMany relationship using data
    in hstore.
    """
    member = models.OneToOneField(
        Employee, help_text="""
        Why are we ForeignKey-ing to this instead of User? Because a user can be
        part of more than one companies over a period of time.

        Why cant then we just add a ForeignKey to Company too? Because our
        CompanyMembership is written with assumption that you could be member of
        same company twice.

        Review page should only include people with CompanyMembership.left_on ==
        null.

        It is assumed that in one review period an employee has been part of a
        Company only once.
        """
    )
    reporters = models.ManyToManyField(
        'self', blank=True, related_name="bosses", symmetrical=False,
        help_text="""
        This is the list of people who report to this user. .bosses is the
        reverse of this, whom this guy reports to.
        """
    )
    peers = models.ManyToManyField(
        'self', blank=True, help_text="""
            These are the "peers" of an employee. In review_cf system, the peers
            are rated by the employee, and those guys rate this person.

            We want symmetrical=True here, but that only works when User has
            FKing to User. So we have to implement symmetrical programmatically.
        """
    )
    review_class = models.ForeignKey(
        ReviewClass, help_text="""
            In review_cf system, every employee belongs to a review_class, and
            based on review_class we select different questions.
        """
    )

    bio_markdown = models.TextField(
        blank=True, default="", help_text="""
        Bio markdown field contains text of user bio.
        """
    )

    profile_pic = S3ImageField(
        bucket=settings.HUMANLY_S3_BUCKET, max_length=255,
        key="profile_pic_{user_id}_{company_id}",
        blank=True, null=True,
    )
    alias_name = models.CharField(blank=True, max_length=35)
    team = models.CharField(blank=True, max_length=150)
    username = models.CharField(
        'username',
        max_length=255,
        blank=False,
        db_index=True
    )
    extra = HStoreField(default={}, blank=True)

    @property
    @cacheable("{company_membership_id}_{user_id}_USER_BIO_HTML")
    def bio_html(self, update_key_values=False):
        if update_key_values:
            self.company_membership_id = self.member.id
            self.user_id = self.member.user.id

        return utils.plaintext2html(self.bio_markdown)

    @property
    def profile_pic_url(self):
        if self.profile_pic:
            return self.profile_pic.url
        else:
            return settings.IMAGE_NOT_FOUND_URL

    def get_imagefield_key(self):
        return {"user_id": self.member.user.id,
                "company_id": self.member.id}

    def get_twitter_url(self):
        if "twitter_user" in self.extra:
            return "https://twitter.com/%s" % self.extra["twitter_user"]

    def get_linkedin_url(self):
        if "linkedin_url" in self.extra:
            return self.extra["linkedin_url"]

    def get_facebook_url(self):
        if "facebook_url" in self.extra:
            return self.extra["facebook_url"]

    def get_github_url(self):
        if "github_user" in self.extra:
            return "https://github.com/%s" % self.extra["github_user"]

    def __unicode__(self):
        return str(self.member.user)


class ReviewPeriod(models.Model):
    DATA_ENTRY = "data-entry"
    OPEN = "open"
    CLOSED = "closed"
    STATUS_CHOICES = (
        (DATA_ENTRY, 'Open for data entry'),
        (OPEN, 'Open for reviews'),
        (CLOSED, 'Closed'),
    )
    company = models.ForeignKey(Company)
    period = models.CharField(
        max_length=40, help_text="""
            Each review is conducted over a period, some quarterly, some half
            yearly and so on. Ideally we should have a better abstraction to
            capture date range, but for now char field it is.

            It would be hard to sort with char field, so if we go ahead with
            this we need an order column.
        """
    )
    status = models.CharField(
        max_length=20, choices=STATUS_CHOICES, default='CLOSED', help_text="""
            This checks the status of the review. Only one review period of a
            company can be non-closed at a time.
        """
    )

    def __unicode__(self):
        return "{}, status={} for {}".format(
            self.period, self.status, self.company
        )


class ReviewSection(models.Model):
    period = models.ForeignKey(ReviewPeriod)
    name = models.CharField(
        max_length=100, help_text="""Each question belongs to a section."""
    )
    classes = models.ManyToManyField(
        ReviewClass, help_text="""
            Not all sections may be applicable to every employee.
        """
    )
    order = models.IntegerField(
        default=0, help_text="""
            This is used for ordering purpose.
        """
    )
    weightage = models.IntegerField(default=0)
    dynamic = models.IntegerField(
        default=0, help_text="""
            This is interesting. What happens is sometimes we have the whole
            section repeated more than once. Say we have a bunch of questions
            about how your projects went, but we allow employee to enter one or
            more projects, so we ask same questions about each project.

            If this field is zero, then this is not repeated. If this is non
            zero, then this may be either upto this many times repeated, or
            everyone has to enter at least this many. Since this is right now
            optimized for coverfox where we are doing upto 3, so this is what we
            are going with.
        """
    )
    for_self = models.BooleanField(
        default=True, help_text="""
            If the section should be visible to self, that is when filling the
            review for yourself.
        """
    )
    for_peers = models.BooleanField(
        default=True, help_text="""
            If the question should be visible when filling form for peers.
        """
    )
    for_bosses = models.BooleanField(
        default=True, help_text="""
            If the question should be visible when filling form for supervisor.
        """
    )
    for_reporters = models.BooleanField(
        default=True, help_text="""
            If the question should be visible when filling form for
            subordinates.
        """
    )

    def __unicode__(self):
        return "{}, dynamic={} for {}".format(
            self.name, self.dynamic, self.period
        )


class ReviewQuestion(models.Model):
    section = models.ForeignKey(ReviewSection)
    text = models.TextField(
        help_text="""
            The question that we will show to reviewers. This field may be a
            rich field.
        """
    )
    order = models.IntegerField(
        default=0, help_text="""This is used for ordering purpose."""
    )
    classes = models.ManyToManyField(
        ReviewClass, blank=True, help_text="""
            Not all questions may be applicable to every employee.
        """
    )
    weightage = models.IntegerField(default=0)
    for_self = models.BooleanField(
        default=True, help_text="""
            If the section should be visible to self, that is when filling the
            review for yourself.
        """
    )
    for_peers = models.BooleanField(
        default=True, help_text="""
            If the question should be visible when filling form for peers.
        """
    )
    for_bosses = models.BooleanField(
        default=True, help_text="""
            If the question should be visible when filling form for supervisor.
        """
    )
    for_reporters = models.BooleanField(
        default=True, help_text="""
            If the question should be visible when filling form for
            subordinates.
        """
    )

    def __unicode__(self):
        return "{}: {} ({})".format(
            self.section.name, self.text, self.section.period
        )


class ReviewQuestionChoice(models.Model):
    question = models.ForeignKey(ReviewQuestion, related_name="choices")
    text = models.TextField()
    order = models.IntegerField(
        default=0, help_text="""This is used for ordering purpose."""
    )
    score = models.IntegerField(
        default=0, help_text="""Score for this option"""
    )

    def __unicode__(self):
        return "{} <= {}".format(self.text, self.question)


class ResponseSection(models.Model):
    """
    This model is only used to capture dynamic section questions. Say projects.

    Here there are two possibilities, we can say, say for amitu, there were
    three projects, that amitu entered, or rane did. And then everyone sees the
    same project and answers questions about them for amitu.

    Or we can let everyone chose projects for amitu individually, and then they
    answer the questions.

    Pro of first:
    - everyone sees same project for amitu

    Pro for second:
    - everyone can start answering questions immediately. No one has to wait
      before amitu or his seniors pick the three projects for them.

    We are going for first, but we also have a responder section that tells who
    entered this information, so it can also handle the second scenario.
    """

    user = models.ForeignKey(
        User, help_text="""This is the guy who worked on those projects."""
    )
    # TODO: This user field would be foreignkey of CompanyMembership rather than Core.User
    # in order to get company out of submitted project.
    responder = models.ForeignKey(
        User, related_name="response_section_set",
        help_text="""The person who created this response."""
    )
    section = models.ForeignKey(ReviewSection)
    text = models.TextField(
        help_text="""
            The question that we will show to reviewers. This field may be a
            rich field.
        """
    )
    order = models.IntegerField(
        default=0, help_text="""This is used for ordering purpose."""
    )

    def __unicode__(self):
        return "{}: {}".format(
            self.section.name, self.text
        )


class ReviewResponse(models.Model):
    user = models.ForeignKey(User)
    responder = models.ForeignKey(
        User, related_name="responses_set",
        help_text="""The person who created this response."""
    )
    question = models.ForeignKey(
        ReviewQuestion, help_text="""
            The question on which this person is being rated.
        """
    )
    response_section = models.ForeignKey(
        ResponseSection, null=True, help_text="""
            This would be non null only in case of dynamic section.
        """
    )
    response = models.ForeignKey(ReviewQuestionChoice)


@receiver(post_save, sender=HRUserInfo)
def handle_post_save(sender, **kwargs):
    """
    Delete cache key.
    """
    # Delete cache key.
    instance = kwargs.get("instance")
    format_json = {}
    format_json["company_membership_id"] = instance.member.id
    format_json["user_id"] = instance.member.user.id

    key = "{company_membership_id}_{user_id}_USER_BIO_HTML".format(
        **format_json
    )
    cache.delete(key)


class Holiday(models.Model):
    holiday = models.CharField(max_length=50)
    date = models.DateField(auto_now=False)
    company = models.ForeignKey(Company)

    def __unicode__(self):
        return self.holiday
