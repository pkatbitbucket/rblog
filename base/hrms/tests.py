from django.test import TestCase, Client, RequestFactory
from django.core.urlresolvers import reverse
from django.conf import settings
from django import forms

import datetime

from hrms.forms import get_text_char_field
from hrms.models import HRUserInfo, ReviewClass
from hrms.utils import plaintext2html
from core.models import User
from coverfox.models import CompanyMembership, Company
from putils import patch


def patch_test1():
    return "patch_test1"


def patch_test2():
    return "patch_test2"


class HRUserInfoFormTest(forms.ModelForm):
    bio_markdown = forms.CharField(
        widget=forms.Textarea(
            attrs={"class": "form-control"}
        ),
        required=False
    )
    team = get_text_char_field()
    alias_name = get_text_char_field()
    profile_pic = forms.CharField(required=False)
    username = get_text_char_field()

    def clean_username(self):
        request = self.extra.get("request")
        username = self.cleaned_data["username"]

        if self.instance.username == username:
            return username

        hr_user = HRUserInfo.objects.filter(
            username=username,
            member__company=request.company
        ).count()

        if hr_user:
            raise forms.ValidationError("Username is already taken")

        return username

    def __init__(self, *args, **kwargs):
        self.extra = kwargs.pop('extra', {})
        super(HRUserInfoFormTest, self).__init__(*args, **kwargs)

    class Meta:
        model = HRUserInfo
        fields = ["username", "alias_name", "bio_markdown",  "team", "profile_pic"]


class HRMSTestCase(TestCase):

    def setUp(self):
        # Delete all previouser data.
        HRUserInfo.objects.all().delete()
        CompanyMembership.objects.all().delete()
        User.objects.all().delete()

        # Insert companies.
        self.company = Company(
            name="Coverfox Insurance Pvt Ltd.",
            slug="coverfox"
        )
        self.company.save()

        self.company2 = Company(
            name="Glitterbug",
            slug="glitterbug"
        )
        self.company2.save()

        # Insert into review class.
        self.review_class = ReviewClass(
            name="review_cf",
            company=self.company
        )
        self.review_class.save()

        self.username = "hitul.mistry@coverfoxmail.com"
        self.username2 = "hitul.mistry@glitterbug.com"
        self.username3 = "xyz@coverfoxmail.com"
        password = "helloworld"
        self.first_name = "Hitul"
        self.employee_id = "123456"
        self.bio = "sample bio"

        """
        user - Company1 user
        user2 - Company2 user
        user3 - Company1 user
        """
        # Create new user.
        self.user = User.objects.create_user(
            self.username,
            password=password,
            first_name=self.first_name
        )
        self.user2 = User.objects.create_user(
            self.username2,
            password=password,
            first_name="User2"
        )
        self.user3 = User.objects.create_user(
            self.username3,
            password=password,
            first_name="User3"
        )

        self.joining_date = datetime.datetime(year=2015, month=10, day=10)

        # Insert Company memberships.
        self.company_membership = CompanyMembership(
            company=self.company,
            user=self.user,
            title="Sr. Tech Architect",
            joined_on=self.joining_date,
            employee_id=self.employee_id
        )
        self.company_membership.save()

        self.company_membership2 = CompanyMembership(
            company=self.company2,
            user=self.user2
        )
        self.company_membership2.save()

        self.company_membership3 = CompanyMembership(
            company=self.company,
            user=self.user3
        )
        self.company_membership3.save()

        # Insert HrUserInfo
        self.hr_user_info = HRUserInfo(
            username=self.username,
            member=self.company_membership,
            review_class=self.review_class,
            bio_markdown=self.bio,
            extra={
                "twitter_user": "hitul007",
                "linkedin_url": "linkedin.com",
                "github_user": "hitul007",
                "facebook_url": "facebook.com"
            }
        )
        self.hr_user_info.save()

        hr_user_info2 = HRUserInfo(
            username=self.username2,
            member=self.company_membership2,
            review_class=self.review_class,
            bio_markdown=self.bio,
            extra={
                "twitter_user": "hitul007",
                "linkedin_url": "linkedin.com",
                "github_user": "hitul007",
                "facebook_url": "facebook.com"
            }
        )
        hr_user_info2.save()

        self.hr_user_info3 = HRUserInfo(
            username=self.username3,
            member=self.company_membership3,
            review_class=self.review_class,
            bio_markdown=self.bio,
            extra={
                "twitter_user": "hitul007",
                "linkedin_url": "linkedin.com",
                "github_user": "hitul007",
                "facebook_url": "facebook.com"
            }
        )
        self.hr_user_info3.save()

        # Clients
        self.client = Client(HTTP_HOST="coverfox.%s" % settings.HUMANLLY_DOMAIN)
        self.client2 = Client(HTTP_HOST="glitterbug.%s" % settings.HUMANLLY_DOMAIN)
        self.client3 = Client(HTTP_HOST="coverfox.%s" % settings.HUMANLLY_DOMAIN)
        self.factory = RequestFactory()

        # Do login.
        self.client.post(
            "/login/",
            {
                "email": self.username,
                "password": password
            }
        )

        self.client2.post(
            "/login/",
            {
                "email": self.username2,
                "password": password
            }
        )

        self.client3.post(
            "/login/",
            {
                "email": self.username3,
                "password": password
            }
        )

    def test_employee_list(self):

        response = self.client.get(reverse("people-list"))
        # Check into response content hitul and joining date found or not.
        self.assertContains(response, self.first_name, status_code=200)


    def test_employee_detail(self):
        # Check into response of employee detail.
        response = self.client.get(
            reverse(
                "user_detail_by_username",
                args=[self.hr_user_info.username]
            )
        )

        # Check into response content if user hitul and bio exist or not.
        self.assertContains(response, self.first_name, status_code=200)
        self.assertContains(response, self.username)
        self.assertContains(response, "Oct. 10, 2015")
        self.assertContains(
            response,
            "https://twitter.com/" + self.hr_user_info.extra.get("twitter_user")
        )
        self.assertContains(response, self.hr_user_info.extra.get("linkedin_url"))
        self.assertContains(response, self.hr_user_info.extra.get("facebook_url"))
        self.assertContains(
            response,
            "https://github.com/" + self.hr_user_info.extra.get("github_user")
        )

    @patch("hrms.forms.HRUserInfoForm", HRUserInfoFormTest)
    def test_edit_employee_detail(self):
        # Check into response of employee detail.
        f_name = "Hitu"
        l_name = "Mistry"
        m_name = "N."
        phone = "9999889999"
        alias_name = "Hitul"
        team = "tech"
        bio_markdown = "This is sample bio"
        twitter_user = "hitul007"
        linkedin_url = "linkedin.com"
        github_user = "hitul007"
        facebook_url = "facebook.com"
        username = "hitul007"

        hr_user_info = HRUserInfo.objects.get(
            username=self.username,
            member__company=self.company
        )
        company_m = hr_user_info.member

        response = self.client.post(
            reverse(
                "people-detail-edit",
                args=[self.hr_user_info.username]
            ),
            {
                "first_name": f_name,
                "last_name": l_name,
                "middle_name": m_name,
                "bio_markdown": bio_markdown,
                "phone": phone,
                "alias_name": alias_name,
                "team": team,
                "twitter_user": twitter_user,
                "linkedin_url": linkedin_url,
                "facebook_url": facebook_url,
                "github_user": github_user,
                "username": username
            }
        )

        hr_user_info.refresh_from_db()
        # Check inside database for updated values.
        self.assertEqual(
            company_m.user.first_name,
            f_name,
            "After update user data request first name does not match"
        )

        self.assertEqual(
            company_m.user.last_name,
            l_name,
            "After update user data request last name does not match"
        )

        self.assertEqual(
            company_m.user.middle_name,
            m_name,
            "After update user data request middle name does not match"
        )

        # Test bio markdown
        self.assertEqual(
            hr_user_info.bio_markdown,
            bio_markdown,
            "After update user data request bio_markdown does not match"
        )

        self.assertEqual(
            hr_user_info.team,
            team,
            "After update user data request team does not match"
        )

        self.assertEqual(
            hr_user_info.alias_name,
            alias_name,
            "After update user data request alias name does not match"
        )

        self.assertEqual(
            company_m.user.phone,
            phone,
            "After update user data request phone does not match"
        )

        self.assertEqual(
            hr_user_info.extra["github_user"],
            github_user,
            "After update user data request phone does not match"
        )

        self.assertEqual(
            hr_user_info.extra["facebook_url"],
            facebook_url,
            "After update user data request phone does not match"
        )

        self.assertEqual(
            hr_user_info.extra["linkedin_url"],
            linkedin_url,
            "After update user data request phone does not match"
        )

        self.assertEqual(
            hr_user_info.extra["twitter_user"],
            twitter_user,
            "After update user data request phone does not match"
        )

        self.assertEqual(
            hr_user_info.username,
            username,
            "After update user data request username does not match"
        )

        # Try to update username to username3.
        response = self.client.post(
            reverse(
                "people-detail-edit",
                args=[username]
            ),
            {
                "first_name": f_name,
                "last_name": l_name,
                "middle_name": m_name,
                "bio_markdown": bio_markdown,
                "phone": phone,
                "alias_name": alias_name,
                "team": team,
                "twitter_user": twitter_user,
                "linkedin_url": linkedin_url,
                "facebook_url": facebook_url,
                "github_user": github_user,
                "username": self.username3
            }
        )

        self.assertContains(
            response,
            "Username is already taken",
            status_code=200
        )

    def test_middleware(self):
        response = self.client.get(
            reverse(
                "user_detail_by_username",
                args=[self.username2]
            )
        )

        # It should return 404.
        self.assertEqual(response.status_code, 404)

    def test_search(self):
        response = self.client.get(
            reverse(
                "people-search"
            ),
            {
                "q": "Hi"
            }
        )
        # Check if search result contains user.
        self.assertContains(response, self.first_name, status_code=200)

    def test_plain_text_to_html_test(self):
        # TODO: Explore \n removal.
        text1 = "hello\n\rworld"
        html = plaintext2html(text1)
        self.assertEqual(
            html,
            "<p>hello</p>\n<p>world</p>\n",
        )

        text2 = "hello&world"
        html = plaintext2html(text2)
        self.assertEqual(
            html,
            "<p>hello&amp;world</p>\n"
        )

        text3 = "hello>world"
        html = plaintext2html(text3)
        self.assertEqual(
            "<p>hello&gt;world</p>\n",
            html
        )

        text4 = "hello<>world"
        html = plaintext2html(text4)
        self.assertEqual(
            "<p>hello&lt;&gt;world</p>\n",
            html
        )

    @patch('__main__.patch_test1', patch_test2)
    def test_patch1(self):
        self.assertEqual(
            patch_test1(),
            "patch_test2"
        )
