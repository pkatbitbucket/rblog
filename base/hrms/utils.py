import mistune
import random
import string


def is_review_pending(user):
    return True


def plaintext2html(text):
    return mistune.markdown(text, escape=True, hard_wrap=True)


def id_generator(size=6, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))
