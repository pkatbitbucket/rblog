from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.views.decorators.http import require_POST
from django.http import Http404
from django.http.response import HttpResponse
from django.core.paginator import Paginator
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import permission_required
import json
import math
import re
import subprocess
import urllib

from importd import d
from hrms.models import ResponseSection, ReviewResponse
from core.models import User
from hrms import forms as hrms_forms
from hrms.models import HRUserInfo, ReviewPeriod, Holiday
from hrms import business_logic
from hrms.utils import is_review_pending
from core.models import Employee


def range_with_condition(stop, func, start=0, step=1):
    new_list = []
    for item in xrange(start, stop, step):
        if func(item):
            new_list.append(item)
        else:
            return new_list

    return new_list


def get_employees_by_page_no(users, page_no):
    context = {}
    if page_no is None:
        context["next_page"] = False
        context["previous_page"] = False
        return users, context

    items_per_page = 90
    p = Paginator(users, items_per_page)
    page = p.page(page_no)

    context["current_page"] = page_no
    # Check next and previous page exist or not.
    if page.has_next():
        context["next_page_exist"] = page.has_next()
        context["next_page"] = page.next_page_number()
        # Get count
        count_check = lambda x: True if x <= p.num_pages else False
        # Get list of paue numbers to display.
        context["next_pages"] = (range_with_condition(
            page_no + 3, count_check, start=page_no + 1
        ))
        context["next_pages"].insert(0, page_no)
    else:
        if page_no == p.num_pages:
            context["next_page_exist"] = True
            context["next_pages"] = [page_no]
            context["next_page"] = None

    if page.has_previous():
        context["previous_page_exist"] = page.has_previous()
        context["previous_page"] = page.previous_page_number()
        # Get list of previous page numbers to display.
        count_check = lambda x: True if x >= 1 else False
        range_with_condition(page_no, count_check, start=page_no, step=-1)
        context["prev_pages"] = range(page_no - 1, 0, -1)
        context["prev_pages"].reverse()

    # Render showing 'Showing 1 to 10 of 20 entries' string.
    entry = page_no * items_per_page
    out_of_count = entry
    if entry > p.count:
        out_of_count = p.count

    entries_status = "showing %s to %s of %s entries"
    context["entries_status"] = entries_status % (
        entry - items_per_page, out_of_count, p.count
    )

    context["is_people"] = True
    return page.object_list, context


@d("/")
@login_required
def index(request):
    return HttpResponseRedirect('/people/')


@d("/login/")
def user_login(request):
    redirect_url = request.GET.get("next", "/people/")

    if request.user.is_authenticated():
        return HttpResponseRedirect(redirect_url)

    template_data = {}

    if request.POST:
        user = authenticate(
            username=request.POST['email'],
            password=request.POST['password']
        )

        if user:
            login(request, user)
            return HttpResponseRedirect(redirect_url)

        template_data['email'] = request.POST['email']
        template_data['login_failed'] = True

    return "hrms/login.html", template_data


@d("/logout/", name="logout")
def user_logout(request):
    redirect_url = request.GET.get("next", "/")
    logout(request)
    return HttpResponseRedirect(redirect_url)


@d("/member/manage/", name="member-manage")
@login_required
def member_admin(request):
    company = request.company
    page = request.GET.get('p', 1)
    query = request.GET.get('q', '').strip()

    try:
        page = int(page)
        if page < 1:
            page = 1
    except ValueError:
        page = 1
    limit = 10
    start = (page - 1) * limit

    members = Employee.objects.filter(company=company)
    if query:
        members = members.filter(
            Q(employee_id__icontains=query) |
            Q(user__email__icontains=query) |
            Q(user__first_name__icontains=query) |
            Q(user__last_name__icontains=query)
        )

    def get_pagination_url(p, no_of_pages):
        if p < 1 or p > no_of_pages:
            return ''
        params = dict(request.GET.items())
        params['p'] = p
        params['q'] = query
        if p == 0:
            params.pop('p')
        return '%s?%s' % (reverse('member-manage'), urllib.urlencode(params))

    no_of_pages = math.ceil(members.count() / float(limit))
    previous_page = get_pagination_url(page - 1, no_of_pages)
    next_page = get_pagination_url(page + 1, no_of_pages)
    template_data = {
        'company': company,
        'query': query,
        'members': members[start: start + limit],
        'start': start,
        'previous_page': previous_page,
        'next_page': next_page,
    }

    return "hrms/member_manage.html", template_data


@d("/projects/", name="user-projects")
@login_required
def projects(request):
    review_period = business_logic.get_latest_review_period(
        request.company, ReviewPeriod.DATA_ENTRY
    )
    if not review_period:
        if business_logic.get_latest_review_period(
            request.company, ReviewPeriod.OPEN
        ):
            return HttpResponseRedirect('/reviews/')
        else:
            raise Http404

    no_of_projects = 3
    success = 'NA'
    if request.method == 'POST':
        post_data = [
            (int(name[8:]), val) for (name, val) in request.POST.items()
            if name.startswith('project_')
        ]
        post_data.sort(key=lambda (project_id, project): project_id)
        projects = [project_data[1] for project_data in post_data]
        business_logic.save_user_projects(
            request.user, review_period, projects
        )
        success = 'SAVED'

    member = Employee.objects.get(
        user=request.user, company=request.company, left_on=None,
    )
    hruserinfo = member.hruserinfo
    colleagues = {
        'peers': [
            hru.member.user for hru in
            hruserinfo.peers.select_related('member__user')
        ],
        'reporters': [
            hru.member.user for hru in
            hruserinfo.reporters.select_related('member__user')
        ],
        'bosses': [
            hru.member.user for hru in
            hruserinfo.bosses.select_related('member__user')
        ],
    }
    user_projects = business_logic.get_user_projects(
        request.user, review_period
    )
    user_projects = [p.text for p in user_projects]
    colleague_projects = {}
    for colleague_type, colleagues in colleagues.items():
        colleague_type_projects = colleague_projects[colleague_type] = {}
        for colleague in colleagues:
            projects = business_logic.get_user_projects(
                colleague, review_period
            )
            projects = [p.text for p in projects]
            colleague_type_projects[colleague] = projects

    template_data = {
        'projects': user_projects,
        'colleague_projects': colleague_projects,
        'success': success,
        'is_review': True,
        'no_of_projects': no_of_projects,
    }
    return "hrms/projects.html", template_data


@d("/reviews/", name="reviews")
@login_required
def reviews(request):
    review_period = business_logic.get_latest_review_period(request.company,
                                                            ReviewPeriod.OPEN)
    if not review_period:
        if business_logic.get_latest_review_period(
            request.company, ReviewPeriod.DATA_ENTRY
        ):
            return HttpResponseRedirect('/projects/')
        else:
            raise Http404
    if not is_review_pending(request.user):
        return HttpResponseRedirect("/reviews/all-done/")
    template_data = {
        'is_review': True,
    }
    return "hrms/reviews.html", template_data


@d("/review/reports/review-report/")
def pending_review_response(request):
    # TODO: Need to optimize it.
    hruser_dict = {}
    for hruser in HRUserInfo.objects.filter(member__company=request.company):
        user_id = hruser.member.user_id
        reporters = []
        peers = []
        for reporter in hruser.reporters.all():
            reporters.append(reporter.member.user_id)

        for peer in hruser.peers.all():
            peers.append(peer.member.user_id)

        hruser_dict[user_id] = [reporters, peers]

        self_review = []
        peer_review = {}
        boss_review = {}

    for review_user, responders in hruser_dict.items():
        peers = []
        bosses = []
        if not ReviewResponse.objects.filter(
            Q(user_id=review_user) & Q(responder_id=review_user)
        ):
            self_review.append(User.objects.get(id=review_user))

        for responder in responders[0]:
            if not ReviewResponse.objects.filter(
                Q(user_id=review_user) &
                Q(responder_id=responder)
            ):
                peers.append(User.objects.get(id=responder))

        peer_review[User.objects.get(id=review_user)] = peers
        for responder in responders[1]:
            if not ReviewResponse.objects.filter(
                Q(user_id=review_user) &
                Q(responder_id=responder)
            ):
                bosses.append(User.objects.get(id=responder))

        boss_review[User.objects.get(id=review_user)] = bosses

    template_data = {
        'self_review': self_review,
        'peer_review': peer_review,
        'boss_review': boss_review
    }
    return "hrms/pending_review.html", template_data


@d('/review/reports/project-report/')
def ramaining_submission(request):
    # TODO: Need to generate it for companies.
    pusers = []
    projects = ResponseSection.objects.all()
    for project in projects:
        if project.user not in pusers:
            pusers.append(project.user)
    users = HRUserInfo.objects.all().order_by('member__joined_on')
    subuser = []
    for user in users:
            if user.member.user not in pusers:
                subuser.append(user)
    context = {}
    context['subuser'] = subuser
    return "hrms/remaining_submission.html", context


@d('/reviews/ajax/get-review-data/')
@login_required
def get_review_data(request):
    review_period = business_logic.get_latest_review_period(
        request.company, ReviewPeriod.OPEN
    )
    reviewer = request.user
    review_data = business_logic.json_for_user(reviewer, review_period)
    return JsonResponse(review_data)


@d('/reviews/ajax/save-review-data/user/<int:user_id>/')
@login_required
@require_POST
def save_review_data(request, user_id):
    reviewer = request.user
    reviewed = User.objects.get(id=user_id)
    post_data = request.POST['data']
    data = json.loads(post_data)
    save_response = business_logic.save_responses(reviewer, reviewed, data)
    return JsonResponse(save_response)


@d('/people/', name="people-list")
@login_required
def people(request):
    # Check query params.
    page_no = 1

    # Check in query string if page number specified.
    if request.GET.get("page"):
        page_no = request.GET.get("page")
        if re.match(r'^[\d]+$', str(page_no)):
            page_no = int(page_no)
        else:
            raise Http404

    # Get current HRUser.
    user_company_members = Employee.objects.get(
        user=request.user,
        company=request.company,
        left_on=None
    )

    hr_logged_in_user = HRUserInfo.objects.get(
        member=user_company_members
    )

    # Get employees by page numbers.
    users = HRUserInfo.objects.filter(
        Q(member__company=request.company) & Q(member__left_on=None)
    ).order_by("member__user__first_name")
    users, context = get_employees_by_page_no(users, page_no)

    context["company"] = request.company

    # Get user employee ID.
    context["users"] = users
    context["hr_logged_in_user"] = hr_logged_in_user
    context["is_people"] = True

    return "hrms/people.html", context


@d('/people/search/', name="people-search")
@login_required
def search_employee(request):
    # Search by employeed first name, last name or last name.
    q = request.GET.get("q")

    if not q:
        return HttpResponseRedirect("/people/")

    # Get current HRUser.
    user_company_members = Employee.objects.get(
        user=request.user,
        company=request.company,
        left_on=None
    )

    hr_logged_in_user = HRUserInfo.objects.get(
        member=user_company_members
    )

    # Get user by first name and last name.
    users = HRUserInfo.objects.filter(
        Q(member__user__first_name__icontains=q) |
        Q(member__user__last_name__icontains=q)
    )

    users, context = get_employees_by_page_no(users, None)

    users, context = get_employees_by_page_no(users, None)

    context["users"] = users
    context["hr_logged_in_user"] = hr_logged_in_user
    context["is_people"] = True

    return "hrms/people.html", context


@d(
    '/<username:username>/edit/',
    name="people-detail-edit"
)
@login_required
def edit_detail(request, username):
    if request.method == "GET":
        # Get hr info for specific company.
        hr_info_instance = get_object_or_404(
            HRUserInfo,
            member__company=request.company,
            username=username,
            member__left_on=None
        )

        # Logged in user can change his detail.
        if hr_info_instance.member.user.id != request.user.id:
            raise Http404

        context = hr_info_instance.extra
        context["user_form"] = hrms_forms.UserForm(instance=request.user)
        context["hr_info_form"] = hrms_forms.HRUserInfoForm(instance=hr_info_instance)
        context["is_people"] = True

        return "hrms/edit-detail.html", context

    # Get request user.
    if request.method == "POST":
        # Get hr info for specific company.
        hr_info_instance = get_object_or_404(
            HRUserInfo,
            member__company=request.company,
            username=username,
            member__left_on=None
        )

        # Logged in user can change his detail.
        if hr_info_instance.member.user.id != request.user.id:
            raise Http404

        user_form = hrms_forms.UserForm(request.POST, instance=request.user)
        hr_info_form = hrms_forms.HRUserInfoForm(
            request.POST,
            instance=hr_info_instance, files=request.FILES,
            extra={'request': request}
        )

        context = hr_info_instance.extra
        if not user_form.is_valid():
            context = {
                "user_form": user_form,
                "hr_info_form": hr_info_form,
                "is_people": True,
            }

            return "hrms/edit-detail.html", context

        if not hr_info_form.is_valid():
            context = {
                "user_form": user_form,
                "hr_info_form": hr_info_form,
                "is_people": True,
            }

            return "hrms/edit-detail.html", context

        user_form.save()

        hr_info_form.instance.user_id = hr_info_form.instance.member.user_id
        hr_info_form.instance.company_id = hr_info_form.instance.member.id
        hr_info_form.save(commit=False)
        hr_info_form.instance.extra["facebook_url"] = request.POST.get(
            "facebook_url", ""
        )
        hr_info_form.instance.extra["github_user"] = request.POST.get(
            "github_user", ""
        )
        hr_info_form.instance.extra["linkedin_url"] = request.POST.get(
            "linkedin_url", ""
        )
        hr_info_form.instance.extra["twitter_user"] = request.POST.get(
            "twitter_user", ""
        )
        hr_info_form.save()

        context = {
            "user_form": user_form,
            "hr_info_form": hr_info_form,
            "is_people": True,
        }

        return HttpResponseRedirect(
            "/%s/" % hr_info_instance.username
        )


@d('/holidays/', name="holidays")
def holidays(request):
    return "hrms/holidays/list.html", {
        "holidays": Holiday.objects.filter(
            company=request.company
        ).order_by("date"),
        "is_hols": True,
    }


@d('/holidays/<holiday_id>/delete/')
@permission_required('hrms.delete_holiday', login_url="/")
def del_holiday(request, holiday_id):
    holiday = get_object_or_404(Holiday, id=holiday_id, company=request.company)
    if request.POST:
        holiday.delete()
        return HttpResponseRedirect('/holidays/')
    return "hrms/holidays/delete.html", {"holiday": holiday, "is_hols": True}


@d('/holidays/<holiday_id>/edit/')
@permission_required('hrms.change_holiday', login_url="/")
def edit_holiday(request, holiday_id):
    holiday = get_object_or_404(Holiday, id=holiday_id, company=request.company)
    if request.POST:
        holiday.holiday = request.POST["new_holiday"]
        holiday.date = request.POST["new_date"]
        holiday.company = request.company
        holiday.save()
        return HttpResponseRedirect("/holidays/")
    return "hrms/holidays/edit.html", {"holiday": holiday, "is_hols": True}


@d("/git/")
def git_info(request):
    git_get_commit_id = "git log --format=%H -n 1"
    p = subprocess.Popen(
        git_get_commit_id.split(" "),
        stdout=subprocess.PIPE
    )
    out, _ = p.communicate()
    return HttpResponse(out)


@login_required
def people_detail(request, username):
    if not username:
        raise Http404

    # Get hr info for specific company.
    hr_user = get_object_or_404(
        HRUserInfo,
        member__company=request.company,
        username=username
    )

    # Assign blank.
    profile_pic = hr_user.profile_pic_url

    context = hr_user.extra
    context["username"] = hr_user.username if hr_user.username else ""
    context["bio"] = hr_user.bio_html if hr_user.bio_html else ""
    context["name"] = hr_user.member.user.get_full_name()
    context["j_date"] = hr_user.member.joined_on
    context["team"] = hr_user.team
    context["profile_url"] = profile_pic
    context["email"] = hr_user.member.user.email
    context["phone"] = hr_user.member.user.phone
    context["alias_name"] = hr_user.alias_name
    context["is_people"] = True
    return "hrms/employee_detail.html", context
