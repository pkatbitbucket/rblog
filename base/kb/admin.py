from django.contrib import admin
from kb.models import WikiEntry
from kb.models import EditHistory

admin.site.register(WikiEntry)
admin.site.register(EditHistory)
