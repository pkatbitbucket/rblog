# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kb', '0002_auto_20151027_1902'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wikientry',
            name='company',
            field=models.ForeignKey(to='core.Company'),
        ),
    ]
