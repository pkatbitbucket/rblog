import json
from jsonfield import JSONField
from mptt.models import MPTTModel, TreeForeignKey

from django.core import serializers
from django.db import models
from django.conf import settings
from django.db.models.signals import post_save

from core.models import Company
import uuid


class WikiEntry(MPTTModel):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)
    title = models.CharField(max_length=200)
    content = models.TextField()
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='wiki_entries')
    created_on = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    company = models.ForeignKey(Company)

    class MPTTMeta:
        order_insertion_by = ['title']

    def custom_serialize(self, serialize_type=None):
        if serialize_type == 'history':
            data = {
                'title': self.title,
                'content': self.content,
            }
        else:
            data = json.loads(
                serializers.serialize('json', [self, ])
                )[0]['fields']
            data.pop("company", None)
            data['content'] = data['content']
            data['id'] = str(self.uuid)
        return json.dumps(data)

    def get_parent_breadcrumb(self):
        return '<-'.join([parent.title for parent in self.get_ancestors(
            ascending=True, include_self=True)
        ])

    def __unicode__(self):
        return '{0}: {1}'.format(str(self.company.name), self.get_parent_breadcrumb())


class EditHistory(models.Model):
    CREATED = 'crea'
    EDITED = 'edit'
    EDIT_TYPE = (
        (CREATED, 'Created'),
        (EDITED, 'Edited'),
        )
    entry = models.ForeignKey(WikiEntry, related_name='edit_history')
    edit_type = models.CharField(max_length=4, choices=EDIT_TYPE)
    serialized_data = JSONField(blank=True, null=True, default={})
    created_on = models.DateTimeField(auto_now_add=True)
    editor = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='my_wiki_edits')

    def custom_serialize(self):
        serialized_data = json.loads(self.serialized_data)
        return {
            'by': self.editor.get_full_name(),
            'date_time': self.created_on.strftime('%Y-%m-%d %H:%M:%S'),
            'edit_type': self.edit_type,
            'serialized_data': serialized_data,
            'show_history': False,
        }


def create_history(sender, instance, **kwargs):
    edit_type = EditHistory.EDITED if instance.edit_history.count() else EditHistory.CREATED
    EditHistory.objects.create(
        entry=instance, edit_type=edit_type,
        serialized_data=instance.custom_serialize(serialize_type='history'),
        editor=kwargs.get('editor', instance.created_by))

post_save.connect(create_history, sender=WikiEntry, dispatch_uid="create_wiki_history")
