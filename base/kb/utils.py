import json
from kb.models import WikiEntry


def serialize_wiki_to_json(instance):
    return_data = {'data': {}, 'children': []}
    return_data['data'] = json.loads(
        instance.custom_serialize()
    )
    return_data['parent_data'] = [
        {'title': parent.title, 'id': str(parent.uuid)} for parent in instance.get_ancestors()]
    return_data['history'] = [history.custom_serialize()
                              for history in instance.edit_history.all().order_by(
        '-created_on')]
    return_data['data']['parent_count'] = instance.get_ancestors().count()
    return_data['children'] = [
        {
            'title': child.title,
            'id': str(child.uuid),
        } for child in instance.get_children()
    ]
    return return_data


def get_wiki_entries(user, parent=None, **kwargs):
    return_data = {}
    wiki_data = WikiEntry.objects.filter(parent=parent)
    if kwargs.get('company', False):
        wiki_data = wiki_data.filter(company=kwargs['company'])
    if not wiki_data and parent is None:
        home_wiki = WikiEntry(
            parent=None,
            company=kwargs['company'],
            title="Home",
            created_by=user)
        home_wiki.save()
        new_data = get_wiki_entries(
            user=user,
            parent=parent,
            company=kwargs.get('company', False)
        )
        return new_data
    return_data['data'] = [
        serialize_wiki_to_json(wiki) for wiki in wiki_data
    ]
    return return_data['data'][0]
