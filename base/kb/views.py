import json
from importd import d

from kb.utils import get_wiki_entries
from kb.utils import serialize_wiki_to_json
from kb.models import WikiEntry
from django.contrib.auth.decorators import login_required


@d("/kb/", name="knowlede_base")
@d("/kb/<slug:uuid>/", name="knowlede_base_api")
@d("/kb/<slug:left_uuid>/<slug:right_uuid>/", name="knowlede_base_perma")
@login_required
def kb(request, **kwargs):
    company = request.company
    if request.method == 'GET':
        wiki_id = kwargs.get('uuid', False)
        if wiki_id:
            wiki = d.get_object_or_404(WikiEntry, uuid=wiki_id, company=company)
            return d.HttpResponse(
                json.dumps(
                    serialize_wiki_to_json(wiki)
                )
            )
        return_data = [get_wiki_entries(user=request.user, company=company)]
        if kwargs.get('left_uuid', False):
            return_data = []
            wiki_left = d.get_object_or_404(
                WikiEntry, uuid=kwargs['left_uuid'], company=company)
            return_data.append(serialize_wiki_to_json(wiki_left))
            if kwargs.get('right_uuid', False):
                wiki_right = d.get_object_or_404(
                    WikiEntry, uuid=kwargs['right_uuid'], company=company)
                return_data.append(serialize_wiki_to_json(wiki_right))

        response_data = {
            "wiki_data": json.dumps(return_data),
            "company": company,
            "is_kb": True,
        }
        return "knowledge_base/landing_page.html", response_data
    elif request.method == 'PUT':
        data = json.loads(request.body)
        flag = data.get('flag', False)
        wiki_id = kwargs.get('uuid', False)
        current_edit = WikiEntry.objects.get(uuid=wiki_id)
        if flag == 'edit_node':
            current_edit.title = data['put_data'].get(
                'title', current_edit.title
            )
            current_edit.content = data['put_data'].get(
                'content', current_edit.content
            )
            current_edit.save()
            return d.HttpResponse({'success': True})
        elif flag == 'move':
            children_data = data['move_data']
            for child in children_data[::-1]:
                try:
                    child_instance = WikiEntry.objects.get(uuid=child['id'])
                    child_instance.move_to(current_edit, position='first-child')
                    child_instance.save()
                except:
                    pass
            return d.HttpResponse({'success': True})
    elif request.method == 'POST':
        data = json.loads(request.body)
        flag = data.get('flag', False)
        if flag == 'new_node':
            parent_id = data['post_data'].get('parent_id', False)
            parent_wiki = WikiEntry.objects.get(uuid=parent_id)
            current_edit = WikiEntry(
                title=data['post_data'].get('title', ''),
                company=company, created_by=parent_wiki.created_by)
            current_edit.insert_at(parent_wiki, 'last-child', save=True)
            return d.HttpResponse(
                json.dumps(
                    serialize_wiki_to_json(
                        current_edit
                    )
                )
            )
    return d.HttpResponse({'success': False})
