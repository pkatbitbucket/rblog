# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('leads', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Lead',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', jsonfield.fields.JSONField(default={}, verbose_name='data', blank=True)),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='created')),
                ('is_production', models.BooleanField(default=False)),
            ],
        ),
    ]
