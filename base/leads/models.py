from django.db import models
from django.utils.translation import ugettext_lazy as _
from jsonfield import JSONField

# Create your models here.


class LeadEmail(models.Model):
    email = models.EmailField(
        _('e-mail address'), max_length=255, blank=False, unique=True, db_index=True)
    data = JSONField(_('data'), blank=True, default={})
    created_on = models.DateTimeField(_('created'), auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.email


class Lead(models.Model):
    data = JSONField(_('data'), blank=True, default={})
    created_on = models.DateTimeField(_('created'), auto_now_add=True)
    is_production = models.BooleanField(default=False)

    def __unicode__(self):
        return 'Lead-%s' % self.id
