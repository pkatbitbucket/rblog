__author__ = 'vinay'
from base64 import b64encode
from django.conf import settings


AFFILIATE_DETAILS = [
    # (USERNAME, TEST_KEY, LIVE_KEY, CAMPAIGN, IP LIST)
    # SET CAMPAIGN AS EMPTY STRING TO SET IT TO DEFAULT CAMPAIGN
    # SET IP LIST AS EMPTY TO ACCEPT ALL THE REQUESTS
    (
        'ValueDirect',
        'nFvooQhXMwSzsEWEATHPkdWjnbYaBVTd',
        'SMEtTrZKOAOxFcLAHzZKUUqpDYvfbJDl',
        'motor-sms',
        ['202.38.172.196']
    ),
]


if settings.PRODUCTION:
    AFFILIATE_CRED_MAP = {
        'Basic {}'.format(b64encode(auth[0] + ':' + auth[2])): [auth[3], auth[4]] for auth in AFFILIATE_DETAILS
    }
    TESTING = False
else:
    AFFILIATE_CRED_MAP = {
        'Basic {}'.format(b64encode(auth[0] + ':' + auth[1])): [auth[3], auth[4]] for auth in AFFILIATE_DETAILS
    }
    TESTING = True

AUTHORIZED_IPS = ['54.241.34.25', '50.19.99.184'] + settings.INTERNAL_IPS
FORM_REQUEST_TYPES = ['application/x-www-form-urlencoded', 'multipart/form-data']
