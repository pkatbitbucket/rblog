from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^add/$', views.add, name='lead_add'),
    url(r'^save-call-time/$', views.save_call_time, name='lead_save_call_time'),
    url(r'^save-email/$', views.save_email, name='lead_save_email'),
    # api to for third party/generic lead save
    url(r'^save-lead/$', views.save_lead, name='lead_save'),
    url(r'^graphana-event/(?P<event_type>[\w]+)/$', views.graphana_event, name='graphana_event'),
    url(r'^send-otp/$', views.send_otp, name='send_otp'),
    url(r'^verify-otp/$', views.verify_otp, name='verify_otp'),
    url(r'^notify-customer-feedback', views.notify_customer_feedback, name='notify_customer_feedback')
    # url(r'^upload-document/$', views.upload_document,name='lead_upload_document'),
]
