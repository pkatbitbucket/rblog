import datetime
import json
from urlparse import urlparse
import copy
import re
import logging
from importlib import import_module
from django.core.files.storage import default_storage
from django.conf import settings

from utils import statsd
from django.utils.crypto import get_random_string
from django.utils import timezone
from django.http.response import HttpResponse, HttpResponseNotAllowed, HttpResponseBadRequest, HttpResponseForbidden, JsonResponse
from django.http.request import MultiValueDict, QueryDict
from django.middleware.csrf import _get_new_csrf_key
from django.shortcuts import redirect
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.clickjacking import xframe_options_exempt
import cf_cns

import putils
from campaign.middlewares import get_campaign_and_network
from utils import SNSUtils, send_sms, log_error
from .models import LeadEmail
import mandrill
from .settings import AFFILIATE_CRED_MAP, AUTHORIZED_IPS, TESTING, FORM_REQUEST_TYPES

logger = logging.getLogger(__name__)


def add(request):
    if request.method == 'POST':
        request_ip = putils.get_request_ip(request)
        msg = json.loads(request.POST['data'])
        if not msg.get('device'):
            if request.user_agent.is_mobile:
                msg['device'] = 'mobile'
            else:
                msg['device'] = 'desktop'

        SNSUtils.send_lead_message(msg, internal=request.IS_INTERNAL, request_ip=request_ip)

        return HttpResponse(json.dumps({'success': True}))
    else:
        return HttpResponseNotAllowed("Not Allowed")


def save_file(rfiles):
    output = []
    for fname in rfiles.keys():

        suffix = datetime.datetime.now().strftime("%d-%m-%Y-%H-%M")
        file_name = rfiles[fname]._get_name()
        file_name = file_name.replace(' ', '_')
        new_file_name = "%s/%s_%s" % ("uploads/motor-breakin-policy",
                                      suffix, file_name)
        # the folder may not always exist
        with default_storage.open(new_file_name, 'wb') as fd:
            for chunk in rfiles[fname].chunks():
                fd.write(chunk)
        output.append({fname: default_storage.url(fd.name)})
    return output


@require_POST
def save_call_time(request):
    success = False
    try:
        current_url = request.build_absolute_uri()
        if request.resolver_match.func is save_call_time:
            current_url = request.META.get('HTTP_REFERER', current_url)

        msg = {
            'email': getattr(request.TRACKER.user, 'email', None) or request.COOKIES.get('email'),
            'mobile': request.COOKIES.get('mobileNo'),
            'campaign': 'Health',
            'agent': request.COOKIES.get('relmanager'),
            'device': 'mobile' if request.user_agent.is_mobile else 'desktop',
            'triggered_page': current_url,
            'label': 'default_label',
            'mode': request.COOKIES.get('mode'),
            'network': request.COOKIES.get('network'),
            'ad-category': request.COOKIES.get('category'),
            'brand': request.COOKIES.get('brand'),
            'gclid': request.COOKIES.get('gclid'),
            'landing_page_url': request.COOKIES.get('landing_page_url'),
            'partner': request.COOKIES.get('partner'),
            'client_id': '.'.join(str(request.COOKIES.get('_ga', '')).split('.')[-2:]),
            'visitor_id': request.TRACKER.id,
        }
        for key, val in request.COOKIES.items():
            if key.find('utm') == 0:
                msg[key] = val
            if key.find('vt_') == 0:
                msg[key] = val

        # Inbound data tracking
        if request.COOKIES.get('utm_source', '').startswith('im_'):
            for key, val in request.COOKIES.items():
                if key.startswith('utm_'):
                    msg[u'im_{}'.format(key[4:])] = val

        msg.update(json.loads(request.POST['data']))
        # *********** DO NOT DELETE ***************
        # CAMPAIGN SET FROM VIEW TO OVERRIDE ALL ELSE
        lms_capaign = msg.get('lms_campaign', '')
        if not lms_capaign:
            lms_capaign, _, _ = get_campaign_and_network(request, urlparse(current_url)[2], msg.get('product', ''))
        msg['campaign'] = lms_capaign
        # ***** CONTACT ANIKET - In case of any questions *****

        if request.FILES:
            files_saved = save_file(request.FILES)
            for f in files_saved:
                msg.update(f)

        msg['created_on'] = datetime.datetime.now().strftime("%d/%m/%Y %H:%M")
        if msg.get('send_buy_form', '').lower() == 'y':
            msg['action'] = 'SEND BUY FORM'
        if msg.get('activity_id'):
            msg['result_url'] = "http://www.coverfox.com/health-plan/results/#%s" % msg['activity_id']
        if msg.get('transaction_id'):
            raw_campaign = msg['campaign']
            dash = msg['campaign'].find('-')

            if dash > 0:
                raw_campaign = msg['campaign'][:dash]
            model = import_module('%s_product.models' % raw_campaign.lower())

            transaction = model.Transaction.objects.get(
                transaction_id=msg['transaction_id'])

            if msg['campaign'] == 'health':
                msg['name'] = transaction.proposer_first_name
                msg['email'] = transaction.proposer_email
        try:
            msg['email'] = "%s" % request.TRACKER.user.email
        except:
            pass

        slot_list = []
        if 'todaySlots' in msg.keys():
            for i in msg['todaySlots']:
                if 'slot_selected' in i.keys() and i['slot_selected'].upper() == 'YES':
                    slot_list.append('Today(%s): %s' %
                                     (msg['todayText'], i['slot']))
        if 'tommSlots' in msg.keys():
            for i in msg['tommSlots']:
                if 'slot_selected' in i.keys() and i['slot_selected'].upper() == 'YES':
                    slot_list.append('Tomorrow(%s): %s' %
                                     (msg['tommText'], i['slot']))
        if 'afterSlots' in msg.keys():
            for i in msg['afterSlots']:
                if 'slot_selected' in i.keys() and i['slot_selected'].upper() == 'YES':
                    slot_list.append('Day After(%s): %s' %
                                     (msg['afterText'], i['slot']))
        msg.update({'slots': "%s" % (', '.join(slot_list))})

        request_ip = putils.get_request_ip(request)
        request_dict = dict(request.mini_dict)
        SNSUtils.send_lead_message(msg, internal=request.IS_INTERNAL, request_ip=request_ip, request=request_dict)
    except:
        log_error(logger, request=request, msg='Unable to save the lead')
    else:
        success = True
    finally:
        return HttpResponse(json.dumps({'success': success}))


@require_POST
def save_email(request):
    msg = {
        'email': getattr(request.TRACKER.user, 'email', None) or request.COOKIES.get('email'),
        'mobile': request.COOKIES.get('mobileNo'),
        'campaign': 'Health',
        'agent': request.COOKIES.get('relmanager'),
        'device': request.COOKIES.get('device'),
        'triggered_page': request.build_absolute_uri(),
        'label': 'default_label',
        'mode': request.COOKIES.get('mode'),
        'network': request.COOKIES.get('network'),
        'ad-category': request.COOKIES.get('category'),
        'brand': request.COOKIES.get('brand'),
        'gclid': request.COOKIES.get('gclid'),
        'landing_page_url': request.COOKIES.get('landing_page_url')
    }
    for key, val in request.COOKIES.items():
        if key.find('utm') == 0:
            msg[key] = val
        if key.find('vt_') == 0:
            msg[key] = val

    # pprint.pprint(json.loads(request.POST['data']))
    msg.update(json.loads(request.POST['data']))
    # *********** DO NOT DELETE ***************
    # CAMPAIGN SET FROM VIEW TO OVERRIDE ALL ELSE
    if request.COOKIES.get('lms_campaign'):
        msg['campaign'] = request.COOKIES.get('lms_campaign')
    if msg.get('lms_campaign', ''):
        msg['campaign'] = msg['lms_campaign']
    # ***** CONTACT ANIKET - In case of any questions *****

    msg['created_on'] = datetime.datetime.now().strftime("%d/%m/%Y %H:%M")
    if msg.get('send_buy_form', '').lower() == 'y':
        msg['action'] = 'SEND BUY FORM'
    if msg.get('activity_id'):
        msg['result_url'] = "http://www.coverfox.com/health-plan/results/#%s" % msg['activity_id']
    if msg.get('email', None):
        if LeadEmail.objects.filter(email=msg['email']):
            lead = LeadEmail.objects.get(email=msg['email'])
        else:
            lead = LeadEmail(email=msg['email'])
        lead.data = msg
        lead.save()
    return HttpResponse(json.dumps({'success': True}))


@xframe_options_exempt
@csrf_exempt
@require_POST
def save_lead(request):
    api, gsp, form_api = False, False, False
    content_type = 'application/json'
    campaign = ''

    # SUBMITTED VIA API
    if request.META.get('CONTENT_TYPE', '') == 'application/json':
        # CHECK FOR AUTHENTICATION
        campaign, authorized_ip_list = AFFILIATE_CRED_MAP.get(
            request.META.get('HTTP_AUTHORIZATION', ' '), ('', ['']))
        authorized_ip_list = AUTHORIZED_IPS if authorized_ip_list == [
            ''] else authorized_ip_list
        if not TESTING and authorized_ip_list and putils.get_request_ip(request) not in authorized_ip_list:
            return HttpResponseForbidden(json.dumps({'success': False,
                                                     'message': 'Authorization failed'}),
                                         content_type=content_type)

        try:
            input_data = json.loads(request.body)
        except ValueError:
            return HttpResponseBadRequest(json.dumps({'success': False,
                                                      'message': 'JSON not found or invalid json format'}),
                                          content_type=content_type)

        else:
            required_data = ['mobile', 'product_type']
            if not all(key in input_data.keys() for key in required_data):
                return HttpResponseBadRequest(json.dumps({'success': False,
                                                          'message': 'Insufficient Data'}),
                                              content_type=content_type)

            elif not all(isinstance(value, basestring) for value in input_data.values()):
                return HttpResponseBadRequest(json.dumps({'success': False,
                                                          'message': 'Pass all the values in string format'}),
                                              content_type=content_type)

            elif len(input_data.get('mobile')) != 10:
                return HttpResponseBadRequest(json.dumps({'success': False,
                                                          'message': 'Not a valid mobile number'}),
                                              content_type=content_type)

            # elif not all(name.isalpha() for name in input_data.get('name', '').encode('utf-8').split(' ')):
            #     return HttpResponseBadRequest(json.dumps({'success': False,
            #                                               'message': 'Only letters are allowed in name'}),
            #                                   content_type=content_type)

            input_data.pop('name', '')

            try:
                int(input_data.get('mobile'))
            except (ValueError, TypeError):
                return HttpResponseBadRequest(json.dumps({'success': False,
                                                          'message': 'Not a valid mobile number'}),
                                              content_type=content_type)
            api = True

    # SUBMITTED VIA GSP MAIL
    elif any(form_type in request.META.get('CONTENT_TYPE', '') for form_type in FORM_REQUEST_TYPES):
        required_data = ['mobile', 'product_type']
        # unbounce is too fucking stupid
        if request.POST.get('data.json'):
            _post_data = QueryDict('', mutable=True)
            _post_data.update(MultiValueDict(
                json.loads(request.POST['data.json'])))
        else:
            _post_data = request.POST
        if not all(field in (request.GET.keys() + _post_data.keys()) for field in required_data):
            return HttpResponseBadRequest('Insufficient Data')

        gsp = True
        if request.GET.get('api', '') == 'true':
            form_api = True
    else:
        return HttpResponseForbidden(json.dumps({'success': False,
                                                 'message': 'Not a valid content type'}),
                                     content_type=content_type)

    if api:
        product_type = input_data.get('product_type', '').lower()
    elif gsp:
        product_type = str(request.GET.get('product_type', '')
                           or _post_data.get('product_type', '')).lower()
    if product_type == 'health':
        _campaign = 'health-search{}'.format(
            'M' if request.user_agent.is_mobile else 'D')
    elif product_type == 'motor':
        _campaign = 'motor-search{}'.format(
            'M' if request.user_agent.is_mobile else 'D')
    elif product_type == 'motor-bike':
        _campaign = 'motor-bike'
    elif product_type == 'travel':
        _campaign = 'travel'
    else:
        if api:
            return HttpResponseBadRequest(json.dumps({'success': False,
                                                      'message': 'Invalid Product Type'}),
                                          content_type=content_type)

        elif gsp:
            return HttpResponseBadRequest('Invalid Product Type')

    if api and TESTING:
        return HttpResponse(json.dumps({'success': True,
                                        'message': 'Testing Successful'}),
                            content_type=content_type)

    if not campaign:
        campaign = _post_data.get('campaign') or _campaign

    new_request = request
    params = request.GET.copy()
    post_data = request.GET.copy()
    post_data.update(
        device='mobile' if request.user_agent.is_mobile else 'desktop',
        devicemodel=request.user_agent.device,
        triggered_page=request.path,
        campaign=campaign,
    )
    if api:
        post_data.update(input_data)
    elif gsp:
        post_data.update(_post_data)
    csrf = _get_new_csrf_key()
    data = {'data': json.dumps(post_data)}
    headers = request.META.copy()
    headers['CSRF_COOKIE'] = csrf
    new_request.GET = {}
    new_request.POST = data
    new_request.META = headers
    new_request.method = 'POST'
    resp = save_call_time(new_request)
    if json.loads(resp.content)['success']:
        if api or form_api:
            return HttpResponse(json.dumps({'success': True,
                                            'message': 'Lead Created Successfully'}),
                                content_type=content_type)

        elif gsp:
            next_urls = {
                'health': '/health-plan/',
                'motor': '/motor/car-insurance/',
                'motor-bike': '/motor/twowheeler-insurance/',
                'travel': '/travel/',
            }
            params.update({
                'callback': 'true',
            })
            response = redirect(
                next_urls[product_type] + '?{}'.format(params.urlencode()))
            cookies = {'mobileNo': post_data.get('mobile'),
                       'show_custom_alert': True if product_type == 'health' else None}
            for name, value in cookies.items():
                response.set_cookie(name, value)
                if name == 'show_custom_alert':
                    response.set_cookie(name, value, expires=120)
            return response


def graphana_event(request, event_type):
    if request.method == 'POST':
        if event_type == 'standard':
            product = request.POST.get('product', None)
            action = request.POST.get('action', None)

            if not product and not action:
                return HttpResponse(json.dumps({'success': False}))

            statsd_string = "%(product)s.detailed.%(network)s.%(device)s.%(utm_campaign)s.web.%(action)s"
            statsd_string_basic = "%(product)s.web.%(action)s"

            network = request.COOKIES.get('network', None)
            device = request.COOKIES.get('device', None)

            campaign = request.COOKIES.get('utm_campaign', None)
            if not campaign:
                campaign = request.COOKIES.get('vt_campaign', 'unknown')

            if network and device and campaign:
                event_name = statsd_string % {'product': product,
                                              'network': network,
                                              'device': device,
                                              'utm_campaign': campaign,
                                              'action': action,
                                              }
                statsd.incr(event_name, 1)

            event_name = statsd_string_basic % {'product': product,
                                                'action': action,
                                                }
            statsd.incr(event_name, 1)
        else:
            post_qdict = copy.deepcopy(request.POST)
            post_parameters = {k: v[0] if len(
                v) == 1 else v for k, v in post_qdict.lists()}

            for stat_key, stat_value in post_parameters.items():
                is_timing = False

                if re.search(r'\.timing\.', stat_key):
                    is_timing = True

                try:
                    stat_value = int(stat_value)
                except:
                    stat_value = 1

                if is_timing:
                    statsd.timing(stat_key, stat_value)
                else:
                    statsd.incr(stat_key, stat_value)

    return HttpResponse(json.dumps({'success': True}))


@require_POST
def send_otp(request):
    if not request.is_ajax():
        return HttpResponseForbidden()

    otp_timeout = 15 * 60
    mobile = request.COOKIES.get('mobileNo')
    if not mobile:
        return JsonResponse({}, status=400)

    otp = get_random_string(6, allowed_chars='0123456789')

    sync, resp = cf_cns.notify('REGISTRATION_OTP_SMS', numbers=mobile,
                                obj={'otp': otp, 'validity': '15 mins'}, sync=True)
    # resp = send_sms(mobile, message.format(otp))
    try:
        success = 'success' in resp.get('sms_response', '')[0].strip().split('|')[0]
    except Exception as e:
        log_error(logger, request, e)
        success = False
    else:
        if success:
            request.session['otp'] = otp
            request.session['expires'] = datetime.datetime.now() + datetime.timedelta(seconds=otp_timeout)

    return JsonResponse({'success': success})


@require_POST
def verify_otp(request):
    now = datetime.datetime.now()
    if not request.is_ajax():
        return HttpResponseForbidden()

    try:
        otp = json.loads(request.POST['data'])['otp']
    except (KeyError, ValueError):
        return JsonResponse({}, status=400)
    else:
        invalid_time = datetime.datetime(0001, 01, 01)
        expiry = request.session.get('expires', '')
        sent_otp = request.session.get('otp', '')
        if not expiry or now > expiry or not sent_otp:
            request.session.pop('expires', invalid_time)
            request.session.pop('otp', '')
            return JsonResponse({'success': False, 'sent_otp': sent_otp})

        if sent_otp == otp:
            request.session.pop('expires', invalid_time)
            request.session.pop('otp', '')
            return JsonResponse({'success': True})

        return JsonResponse({'success': False})


@require_POST
def notify_customer_feedback(request):
    post_dict = json.loads(request.POST['data'])
    subject = post_dict['subject']
    body = post_dict['body']
    mobile_number = post_dict['mobileNo']
    email = post_dict['email']
    source = post_dict['source']
    name = post_dict['name']

    current_date = timezone.now().strftime('%Y-%m-%d %H:%M:%S')
    if source == 'contact-email':
        to = [{'email': 'help@coverfox.com',
               'name': '',
               'type': 'to'}]
        if getattr(settings, 'NOTIFICATION_EMAIL_LIST', None):
            to = []
            for e in settings.NOTIFICATION_EMAIL_LIST:
                to.append({'email': e,
                           'name': '',
                           'type': 'to'})
        try:
            send_customer_email(email, name, body, to, subject, current_date)
        except mandrill.Error, e:
            logger.error("Unable to send customer notification email due to %s" % e)
    elif source == 'contact-phone':
        email_list = ['help@coverfox.com']
        to = []
        for e in email_list:
            to.append({'email': e,
                       'name': '',
                       'type': 'to'})
        if getattr(settings, 'NOTIFICATION_EMAIL_LIST', None):
            to = []
            for e in settings.NOTIFICATION_EMAIL_LIST:
                to.append({'email': e,
                           'name': '',
                           'type': 'to'})

        subject = 'Call in 5 minutes. Call back request from website - %s' % current_date
        text = '''
        Customer Contact Number: %s \nCustomer Query: Raise Service or claim request
        ''' % mobile_number
        send_customer_email('claimrequest@coverfox.com', '', text, to, subject, current_date, async=False)
    elif source == 'claim':
        email_list = ['help@coverfox.com', 'jack@coverfox.com', 'ramitha@coverfox.com']
        to = []
        for e in email_list:
            to.append({'email': e,
                       'name': '',
                       'type': 'to'})
        if getattr(settings, 'NOTIFICATION_EMAIL_LIST', None):
            to = []
            for e in settings.NOTIFICATION_EMAIL_LIST:
                to.append({'email': e,
                           'name': '',
                           'type': 'to'})
        try:
            body = 'Contact Number: %s \n' % mobile_number + body
            send_customer_email('claimrequest@coverfox.com', 'Claims Request', body, to, subject, current_date)
        except mandrill.Error, e:
            logger.error("Unable to send customer notification email due to %s" % e)

    return JsonResponse({'success': True})


def send_customer_email(from_email, cust_name, text, send_to, subject, current_date, async=True):
    mandrill_client = mandrill.Mandrill(settings.MANDRILL_API_KEY)
    message = {
                 'from_email': from_email,
                 'from_name': cust_name,
                 'merge_language': 'mailchimp',
                 'metadata': {'website': 'www.coverfox.com'},
                 'subject': subject,
                 'tags': ['claim-form'],
                 'text': text,
                 'to': send_to,
              }
    try:
        mandrill_client.messages.send(message=message, async=async, ip_pool='Main Pool', send_at=current_date)
    except mandrill.Error, e:
        logger.error("Unable to send customer notification email due to %s" % e)
