from django.contrib import admin

from . import models as leaflet_campaign_models

admin.site.register(leaflet_campaign_models.LeafletAgent)
admin.site.register(leaflet_campaign_models.AgentReferals)
