import factory

from django.conf import settings

from leaflet_campaign import models as leaflet_campaign_models
from motor_product import factories as motor_factories

EXISTING_REGISTRATION_NUMBERS = [
    'ap01ab2333', 'ap01ad5134', 'ap01ag0451', 'ap01ah1595'
]


class LeafletAgentFactory(factory.django.DjangoModelFactory):
    agent_name = factory.Sequence(lambda n: 'Agent {0}'.format(n))

    class Meta:
        model = leaflet_campaign_models.LeafletAgent
        abstract = False


class AgentReferalsFactory(factory.django.DjangoModelFactory):
    registration_no = factory.Iterator(
        EXISTING_REGISTRATION_NUMBERS, cycle=False
    )
    agent = factory.SubFactory(LeafletAgentFactory)
    quote = factory.SubFactory(motor_factories.QuoteFactory)

    class Meta:
        model = leaflet_campaign_models.AgentReferals
        abstract = False


# TODO need to change this
class RSAPolicyFactory(factory.django.DjangoModelFactory):
    card_id = factory.Sequence(
        lambda n: "{}{}".format(
            settings.LEAFLET_CAMPAIGN_REFERRAL_CODE_REGEX,
            str(n).rjust(5, '0')[:5]
        )
    )
    registration_no = factory.Iterator(
        EXISTING_REGISTRATION_NUMBERS, cycle=False
    )

    class Meta:
        model = leaflet_campaign_models.AgentReferals
        abstract = False
