import re
from django import forms
from django.conf import settings
from core import forms as core_forms
from motor_product import models as motor_models
from leaflet_campaign import models as leaflet_campaign_models


class RSAPolicyUserForm(core_forms.FDForm):
    # leaflet details
    card_id = forms.CharField(max_length=16)

    # Vehicle Details
    registration_number = forms.CharField(max_length=25)
    vehicle_master = forms.IntegerField()
    vehicle_registration_date = forms.CharField(max_length=30)
    past_policy_end_date = forms.CharField(max_length=30, required=False)

    # User Details
    first_name = forms.CharField(max_length=50)
    middle_name = forms.CharField(max_length=50, required=False)
    last_name = forms.CharField(max_length=50, required=False)
    mobile = forms.CharField(max_length=11)
    email = forms.CharField(max_length=255)
    birth_date = forms.CharField(max_length=120)
    address_line1 = forms.CharField(max_length=200)
    address_line2 = forms.CharField(max_length=200, required=False)
    address_line3 = forms.CharField(max_length=100, required=False)
    landmark = forms.CharField(max_length=65, required=False)
    city = forms.CharField(max_length=50)
    state = forms.CharField(max_length=50)
    pincode = forms.CharField(max_length=26)

    class Config:
        fields = [
            core_forms.Person(
                tag='self',
                first_name='first_name',
                middle_name='middle_name',
                last_name='last_name',
                mobile='mobile',
                email='email',
                birth_date='birth_date',
            ),
            core_forms.Address(
                tag='self',
                address_line1='address_line1',
                landmark='landmark',
                city='city',
                state='state',
                pincode='pincode'
            ),
            core_forms.CarDetail(
                tag='self',
                registration_number='registration_number',
                vehicle_master='vehicle_master',
                vehicle_registration_date='vehicle_registration_date',
                past_policy_end_date='past_policy_end_date',
            )
        ]

    def clean_card_id(self):
        card_errors = self._errors.get('card_id', [])

        if self.cleaned_data['card_id']:
            if not re.match(
                settings.LEAFLET_CAMPAIGN_REFERRAL_CODE_REGEX,
                self.cleaned_data.get('card_id')
            ):
                card_errors.append('Invalid Referral Card ID')
            if leaflet_campaign_models.RSAPolicy.objects.filter(
                card_id=self.cleaned_data['card_id']
            ).exists():
                card_errors.append(leaflet_campaign_models.RSAPolicy.CARD_ID_ERROR)
        else:
            card_errors.append('Enter a Referral Card ID')

        if card_errors:
            self._errors['card_id'] = card_errors

        return self.cleaned_data['card_id']

    def clean_vehicle_master(self):
        vehicle_master_errors = self._errors.get('vehicle_master', [])

        if not motor_models.VehicleMaster.objects.filter(
            id=self.cleaned_data['vehicle_master']
        ).exists():
            vehicle_master_errors.append('Invalid Vehicle')

        if vehicle_master_errors:
            self._errors['vehicle_master'] = vehicle_master_errors

        return self.cleaned_data['vehicle_master']
