# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import autoslug.fields
import django_pgjson.fields


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0045_auto_20160217_1949'),
    ]

    operations = [
        migrations.CreateModel(
            name='AgentReferals',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('registration_no', models.CharField(unique=True, max_length=12)),
            ],
        ),
        migrations.CreateModel(
            name='LeafletAgent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('agent_name', models.CharField(max_length=100)),
                ('agent_slug', autoslug.fields.AutoSlugField(populate_from=b'agent_name', editable=False)),
            ],
        ),
        migrations.CreateModel(
            name='RSAPolicy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('card_id', models.CharField(max_length=16, null=True, blank=True)),
                ('registration_no', models.CharField(unique=True, max_length=12)),
                ('policy', models.FileField(null=True, upload_to=b'leaflet_campaign_rsa_pdfs/cfox', blank=True)),
                ('data', django_pgjson.fields.JsonField(default={}, verbose_name='Data')),
            ],
        ),
        migrations.AddField(
            model_name='agentreferals',
            name='agent',
            field=models.ForeignKey(to='leaflet_campaign.LeafletAgent'),
        ),
        migrations.AddField(
            model_name='agentreferals',
            name='quote',
            field=models.ForeignKey(to='motor_product.Quote'),
        ),
    ]
