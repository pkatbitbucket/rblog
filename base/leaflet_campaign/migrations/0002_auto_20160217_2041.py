# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('leaflet_campaign', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='agentreferals',
            name='created_at',
            field=models.DateTimeField(null=True, verbose_name='created', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rsapolicy',
            name='created_at',
            field=models.DateTimeField(null=True, verbose_name='created', auto_now_add=True),
            preserve_default=False,
        ),
    ]
