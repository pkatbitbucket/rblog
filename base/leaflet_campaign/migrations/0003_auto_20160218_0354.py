# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('leaflet_campaign', '0002_auto_20160217_2041'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rsapolicy',
            name='card_id',
            field=models.CharField(default=b'CFR2201F00000', unique=True, max_length=16),
        ),
        migrations.AlterField(
            model_name='rsapolicy',
            name='policy',
            field=models.FileField(null=True, upload_to=b'leaflet_campaign_rsa_pdfs/cfox'),
        ),
    ]
