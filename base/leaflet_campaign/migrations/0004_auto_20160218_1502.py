# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django_pgjson.fields


class Migration(migrations.Migration):

    dependencies = [
        ('leaflet_campaign', '0003_auto_20160218_0354'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agentreferals',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AlterField(
            model_name='leafletagent',
            name='agent_name',
            field=models.CharField(unique=True, max_length=100),
        ),
        migrations.AlterField(
            model_name='rsapolicy',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AlterField(
            model_name='rsapolicy',
            name='data',
            field=django_pgjson.fields.JsonField(default={}),
        ),
    ]
