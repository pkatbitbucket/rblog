# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django_pgjson.fields


class Migration(migrations.Migration):

    dependencies = [
        ('leaflet_campaign', '0004_auto_20160218_1502'),
    ]

    operations = [
        migrations.AddField(
            model_name='agentreferals',
            name='data',
            field=django_pgjson.fields.JsonField(default={}),
        ),
    ]
