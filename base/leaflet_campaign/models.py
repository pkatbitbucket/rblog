from django.conf import settings
from django.core import exceptions
from django.db import models

from autoslug import AutoSlugField
from django_pgjson import fields as django_pgjson_fields

from motor_product.models import Quote

from . import utils as leaflet_campaign_utils


class LeafletAgent(models.Model):
    agent_name = models.CharField(max_length=100, unique=True)
    agent_slug = AutoSlugField(populate_from='agent_name')

    def __unicode__(self):
        return "%s (%s)" % (self.agent_name, self.agent_slug)


class AgentReferals(models.Model):
    registration_no = models.CharField(max_length=12, unique=True)
    agent = models.ForeignKey(LeafletAgent)
    quote = models.ForeignKey(Quote)
    data = django_pgjson_fields.JsonField(default={})
    created_at = models.DateTimeField(auto_now_add=True, null=True)

    def __unicode__(self):
        return "%s - %s" % (self.agent, self.registration_no)

    def clean(self):
        self.registration_no = leaflet_campaign_utils.clean_registration_number(
            self.registration_no
        )


class RSAPolicy(models.Model):

    CARD_ID_ERROR = 'Looks like someone already claimed this code. Please contact the support team.'

    card_id = models.CharField(
        max_length=16, unique=True,
        default='CFR2201F00000'
    )
    registration_no = models.CharField(max_length=12, unique=True)
    policy = models.FileField(
        upload_to=settings.LEAFLET_CAMPAIGN_RSA_PDF_PATH,
        null=True
    )
    data = django_pgjson_fields.JsonField(default={})
    created_at = models.DateTimeField(auto_now_add=True, null=True)

    def clean(self):
        self.registration_no = leaflet_campaign_utils.clean_registration_number(
            self.registration_no
        )

    def set_policy(self, data={}):
        data = data or self.data
        try:
            self.policy = leaflet_campaign_utils.generate_pdf(self.data)
        except Exception, e:
            raise exceptions.ValidationError(
                {'policy': ["ErrorWhileGeneratingPDF: {}".format(e.message)]}
            )

    def policy_url(self):
        return self.policy.url

    def save(self, *args, **kwargs):
        if not self.policy:
            self.set_policy()
        super(RSAPolicy, self).save(*args, **kwargs)
