from django.conf.urls import patterns, url
from leaflet_campaign import views as leaflet_campaign_views


reg_no_regex = '([a-zA-Z]{2})([a-zA-Z0-9]{1,3})([a-zA-Z]{2})([0-9]{1,4})'

urlpatterns = patterns(
    'motor_product.dealer.views',

    url(r'^leaflet-campaign/validate/$',
        leaflet_campaign_views.validate_leaflet_card_id_and_reg_no,
        name='validate_leaflet_card_id_and_reg_no'),

    url(r'^leaflet-campaign/(?P<agent_slug>[\w-]+)/$',
        leaflet_campaign_views.AgentReferalCreateView.as_view(),
        name='create_agent_referral'),

    url(r'^ggc/$',
        leaflet_campaign_views.RSAPolicyView.as_view(),
        name='rsa_policy'),

    url(r'^(?P<registration_number>%s)/$' % reg_no_regex,
        leaflet_campaign_views.leaflet_campaign_home,
        name='leaflet_campaign_home'),
)
