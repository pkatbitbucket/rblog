import os
import re
import time

from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.conf import settings

from django.template import Context
from django.template.loader import get_template

from motor_product.motor_offline_leads import pdf_utils


def clean_registration_number(registration_number):
    return ''.join(filter(bool, re.findall('[a-z0-9]+', registration_number.lower())))


def get_rsa_policy_data(data):
    full_name = " ".join(
        "{} {} {}".format(
            data['first_name'],
            data['middle_name'],
            data['last_name']
        ).split()
    )
    return {
        'name': full_name,
        'city': data['city'] or 'NA',
        'pincode': data['pincode'] or 'NA',
        'email': data['email'] or 'NA',
        'make': data['make'] or 'NA',
        'model': data['model'] or 'NA',
        'address': data['address_line1'],
        'blood_group': "NA",
        'mobile': data['mobile'] or 'NA',
        'date_of_birth': data['birth_date'],
        'vehicle_type': data['vehicle_type'] or 'NA',
        'registration_number': data['registration_number'],
        'vehicle_registration_date': data['vehicle_registration_date'],
        'past_policy_end_date': data['past_policy_end_date'],
        'rsa_policy_number': data['card_id'],
        'product': ' ',
        'contact_number': '011-45735570'
    }


def generate_pdf(data):
    template = get_template('leaflet_campaign:rsa.html')
    data = get_rsa_policy_data(data)
    html = template.render(Context(data))
    rsa_doc_path = os.path.join(
        settings.LEAFLET_CAMPAIGN_RSA_PDF_PATH,
        str(time.time()) + str(data['rsa_policy_number']) + '.pdf'
    )
    content = pdf_utils.get_pdf_link(html, regenerate_pdf=True)

    with default_storage.open(rsa_doc_path, 'wb') as f:
        for chunk in ContentFile(content).chunks():
            f.write(chunk)

    return rsa_doc_path
