import datetime
import json
import re

from django import http
from django.conf import settings
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, redirect
from django.views.generic.edit import FormView

from . import forms as leaflet_campaign_forms
from . import models as leaflet_campaign_models
from . import utils as leaflet_campaign_utils

from motor_product import views as motor_views
from motor_product import models as motor_models
from cf_cns import notify as cf_notify

NCB_MAP = {
    '1': '0',
    '2': '20',
    '3': '25',
    '4': '35',
    '5': '45'
}


class AgentReferalCreateView(motor_views.RegistrationDetailCreateView):
    template_name = 'leaflet_campaign:create_agent_referal.html'

    def dispatch(self, request, *args, **kargs):
        get_object_or_404(
            leaflet_campaign_models.LeafletAgent,
            agent_slug=self.kwargs.get('agent_slug', 0)
        )
        referrals = leaflet_campaign_models.AgentReferals.objects.filter(
            registration_no__iexact=request.POST.get('name', '')
        )
        if referrals.exists():

            return http.JsonResponse({
                'status': 200,
                'response_code': 102,
                'message': 'This car was already referred!',
                'data': referrals.first().data,
            })

        return super(AgentReferalCreateView, self).dispatch(request, *args, **kargs)

    def post(self, request, *args, **kwargs):

        form = self.get_form()
        response = {
            'status': 200,
            'response_code': 101,
            'message': 'No data found!',
            'data': {},
        }

        if form.is_valid():
            clean_data = form.cleaned_data
            form_response = self.form_valid(form)
            response['status'] = 200
        else:
            response['data'] = self.form_invalid(form)
            response['response_code'] = 103
            response['status'] = 400

        if response['status'] == 400:
            return http.JsonResponse(response, status=response['status'])

        form_response = json.loads(form_response.content)

        vehicle_master = motor_models.VehicleMaster.objects.filter(
            id=form_response.get('data', {}).get('vehicle_master_id', 0)
        ).first()

        if vehicle_master:

            fastlane_data = form_response['data'].get(
                'fastlane', {}
            ).get('response', {}).get('result', {}).get('vehicle', {})

            reg_year = fastlane_data.get('manu_yr', 0)
            max_ncb = NCB_MAP.get(
                str(int(datetime.datetime.now().strftime('%Y')) - int(reg_year)),
                '50'
            )
            manufacturing_date = '01-{0}-{1}'.format(
                str(int(datetime.datetime.now().strftime('%m'))-1),
                str(reg_year)
            )
            fallback_regn_date = '01-{0}-{1}'.format(
                datetime.datetime.now().strftime('%m'), str(reg_year)
            )
            sanitize_reg_no = [
                clean_data['name'][:2].upper(),
                filter(lambda x: x.isdigit(), clean_data['name'][2:4]),
                '0', '0'
            ]
            post_data = {
                'addon_isDepreciationWaiver': '0',
                'isNCBCertificate': '0',
                'extra_paPassenger': '0',
                'cngKitValue': '0',
                'extra_user_dob': '',
                'isNewVehicle': '0',
                'extra_isAntiTheftFitted': '0',
                'quoteId': '',
                'idvNonElectrical': '0',
                'isClaimedLastYear': '0',
                'registrationNumber[]': sanitize_reg_no,
                'newNCB': '0',
                'manufacturingDate': manufacturing_date,
                'registrationDate': fallback_regn_date,
                'fastlane_model_changed': 'true',
                'rds_id': 'false',
                'addon_isNcbProtection': '0',
                'idvElectrical': '0',
                'expiry_error': 'false',
                'addon_is247RoadsideAssistance': '0',
                'isExpired': 'false',
                'voluntaryDeductible': '0',
                'fastlane_variant_changed': 'false',
                'formatted_reg_number': clean_data['name'],
                'fastlane_fueltype_changed': 'false',
                'addon_isInvoiceCover': '0',
                'addon_isDriveThroughProtected': '0',
                'payment_mode': 'PAYMENT_GATEWAY',
                'extra_isTPPDDiscount': '0',
                'isCNGFitted': '0',
                'isUsedVehicle': '0',
                'fastlane_success': 'true',
                'vehicleId': vehicle_master.id,
                'expirySlot': '7',
                'idv': '0',
                'discountCode': '',
                'extra_isLegalLiability': '0',
                'previousNCB': max_ncb,
                'addon_isEngineProtector': '0',
                'ncb_unknown': 'true',
                'addon_isKeyReplacement': '0',
                'extra_isMemberOfAutoAssociation': '0',
                'newPolicyStartDate': (
                    datetime.datetime.today() + datetime.timedelta(days=8)
                ).strftime('%d-%m-%Y'),
                'pastPolicyExpiryDate': (
                    datetime.datetime.today() + datetime.timedelta(days=7)
                ).strftime('%d-%m-%Y'),
            }
            request.POST = request.POST.copy()
            request.POST.pop('name')
            request.POST.update(post_data)
            return_data = motor_views.quotes(request, 'fourwheeler')
            quote_response = json.loads(return_data.content)

            if quote_response.get('success', False):
                quote_id = quote_response.get('data', {}).get('quoteId', '')

                available_prices = []
                for premium_response in quote_response['data']['premiums']:
                    available_prices.append(premium_response['final_premium'])

                response['data'] = {
                    'min': min(available_prices),
                    'max': max(available_prices),
                }

                response['message'] = 'Data Found!'
                response['status'] = 200
                response['response_code'] = 100

                leaflet_campaign_models.AgentReferals.objects.create(
                    agent=leaflet_campaign_models.LeafletAgent.objects.get(
                        agent_slug=self.kwargs.get('agent_slug')
                    ),
                    registration_no=clean_data['name'],
                    quote=motor_models.Quote.objects.get(
                        quote_id=quote_id
                    ),
                    data=response['data']
                )

        return http.JsonResponse(response)


def leaflet_campaign_home(request, registration_number):
    registration_number = leaflet_campaign_utils.clean_registration_number(
        registration_number
    )

    referral = get_object_or_404(
        leaflet_campaign_models.AgentReferals,
        registration_no__iexact=registration_number
    )

    vehicle_type = motor_views.VEHICLE_TYPE_TO_URL_TAG_MAP.get(
        referral.quote.vehicle.vehicle_type, ''
    )

    return redirect(
        reverse('quote_share_%s' % vehicle_type, kwargs={
            'quote_id': referral.quote.quote_id
        })
    )


def validate_leaflet_card_id_and_reg_no(request):
    if request.method == 'POST':
        response = {
            'status': 200,
            'messages': {},
            'is_used': False,
        }
        if request.POST.get('card_id'):
            if not re.match(
                settings.LEAFLET_CAMPAIGN_REFERRAL_CODE_REGEX,
                request.POST.get('card_id')
            ):
                return http.JsonResponse({
                    'status': 200,
                    'error': True,
                    'message': 'Invalid Referral Card ID'
                })
            if leaflet_campaign_models.RSAPolicy.objects.filter(
                card_id=request.POST.get('card_id')
            ).exists():
                response['is_used'] = True
                response['messages']['card_id'] = leaflet_campaign_models.RSAPolicy.CARD_ID_ERROR

        if request.POST.get('registration_number'):
            referral = leaflet_campaign_models.RSAPolicy.objects.filter(
                registration_no=request.POST.get('registration_number')
            ).first()

            if referral and referral.card_id:
                response['is_used'] = True
                response['messages']['registration_number'] = 'You have already claimed a Policy'
        return http.JsonResponse(response)
    else:
        return http.Http404


class RSAPolicyView(FormView):

    template_name = 'leaflet_campaign:rsa_plicy_form.html'
    form_class = leaflet_campaign_forms.RSAPolicyUserForm
    success_url = '/rsa-policy/'

    def dispatch(self, *args, **kwargs):
        if self.request.method == 'POST':

            policy = leaflet_campaign_models.RSAPolicy.objects.filter(
                registration_no=self.request.POST.get(
                    'registration_number', 0
                ),
                card_id=self.request.POST.get(
                    'card_id', 0
                )
            )

            if policy.exists():
                return http.JsonResponse({
                    'status': 200,
                    'message': 'You have already claimed this policy!',
                    'data': {'rsa_policy': policy.policy_url()},
                })

        return super(RSAPolicyView, self).dispatch(*args, **kwargs)

    def form_invalid(self, form):
        return http.JsonResponse({'success': False, 'errorItems': form.errors}, status=400)

    def post(self, form):

        form = self.get_form()
        if form.is_valid():
            data = form.cleaned_data
        else:
            return self.form_invalid(form)

        data = form.cleaned_data

        vehicle_master = motor_models.VehicleMaster.objects.get(
            id=data['vehicle_master']
        )

        data.update({
            'make': vehicle_master.make.name,
            'model': vehicle_master.model.name,
            'vehicle_type': vehicle_master.vehicle_type
        })

        rsa_policy = leaflet_campaign_models.RSAPolicy.objects.create(
            registration_no=data['registration_number'],
            card_id=data['card_id'],
            data=data,
        )

        registration_detail, created = motor_models.NewRegistrationDetail.objects.get_or_create(
            registration_number=rsa_policy.registration_no
        )
        registration_detail.vehicle_master = vehicle_master
        registration_detail.save()

        # TODO: create an activity
        # core_activity.create_activity(
        #     '',                                       # 'activity_type' will get from hitul
        #     'car',                                    # product
        #     'free_rsa_policy_form',                   # page_id
        #     '',                                       # form_name
        #     '',
        #     '',
        #     data=form.get_fd()
        # )

        response = {
            'status': 200,
            'message': 'Success!',
            'data': {'rsa_policy': rsa_policy.policy_url()},
        }
        cf_notify("RSA_NOTIFY", numbers=data['mobile'], to=data['email'], obj=data, mandrill_template='rsa-1')
        return http.JsonResponse(response, status=response['status'])
