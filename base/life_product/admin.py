from django.contrib import admin

from cms.common.admin import BaseAdmin, BaseAuthorAdmin, AbstractBaseAdmin

from .forms import PlanAdminForm
from .models import Insurer, Frame, PremiumRate, Plan, Feature, PlanFeature, RedirectRecord, PolicyWordingDocument


class PlanFeatureInline(AbstractBaseAdmin, admin.TabularInline):
    model = PlanFeature
    extra = 1


class InsurerAdmin(BaseAdmin):
    model = Insurer
    list_editable = ('active', 'claims_settled', 'logo', 'policy_wording', )
    list_display = ('__unicode__', ) + list_editable


class PlanAdmin(BaseAdmin):
    model = Plan
    form = PlanAdminForm
    inlines = (PlanFeatureInline, )
    list_display = ('__unicode__', 'calculator', )


class FrameAdmin(BaseAdmin):
    search_fields = ('plan__insurer__name', 'plan__name', 'sum_assured', 'gender', 'tobacco_user', )


class RedirectRecordAdmin(BaseAdmin):
    model = RedirectRecord
    list_display = ('__unicode__', 'activity_extra', 'created_date', )
    raw_id_fields = ('activity', )

    def activity_extra(self, instance):
        return ", ".join(map(lambda t: "%s=%s" % t, instance.activity.extra.items()))


admin.site.register(Insurer, InsurerAdmin)
admin.site.register(Frame, FrameAdmin)
admin.site.register(PremiumRate, BaseAdmin)
admin.site.register(Plan, PlanAdmin)
admin.site.register(Feature, BaseAdmin)
admin.site.register(RedirectRecord, RedirectRecordAdmin)
admin.site.register(PolicyWordingDocument, BaseAuthorAdmin)
