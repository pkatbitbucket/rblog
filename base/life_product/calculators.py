from __future__ import absolute_import

from django.conf import settings
from django.contrib.humanize.templatetags.humanize import intcomma
from django.utils import lru_cache

from psycopg2.extras import NumericRange
from life_product.models import PremiumRate, Frame

import utils


LAKH = 100000
CRORE = 100 * LAKH

FREQUENCY = utils.to_namedtuple({
    'M': 'Monthly',
    'Q': 'Quarterly',
    'H': 'Half yearly',
    'Y': 'Yearly',
})


class OutOfScope(Exception):
    pass


def _float(func):
    def wrapper(*args, **kwargs):
        return float(func(*args, **kwargs))
    return wrapper


def _round(func):
    def wrapper(*args, **kwargs):
        return int(round(func(*args, **kwargs)))
    return wrapper


class AbstractCalculator(object):
    def calculate(self):
        """Gets you the premium including ST if validations are passed"""
        pass

    def including_premium(self):
        """Gets you the premium including ST"""
        pass

    def excluding_premium(self):
        """Gets you the premium excluding ST"""
        pass

    def premium_rate(self):
        "From PremiumRate model"
        pass

    def discount(self):
        "Discount applicable, lookup table hardcoded here"
        pass

    def clean(self, cleaned_data):
        """Can add custom validations here or clean cleaned_data further"""
        pass

    class Config(dict):
        """Contains configurable insurer/plan specific values, should ideally be picked from db"""


class BaseCalculator(AbstractCalculator):
    def __formula__(self):
        return """
            formula:
            base_premium = SA/1000 * premium_rate
            total_premium = (base_premium - discount) + ((base_premium - discount)/100 * ST)
        """

    minimum_premium = None
    maximum_maturity_age = None
    maximum_entry_age = None
    maximum_continuous_term = None
    service_tax_percentage = settings.SERVICE_TAX_PERCENTAGE

    def __init__(self, plan, data={}):
        self.plan = plan
        self.premium_rates = PremiumRate.objects.filter(frame__plan=self.plan)
        assert self.premium_rates.exists(), "Premium rates absent"
        cleaned_data = {}
        data['term'] = self._get_term(maturity_age=data['maturity_age'], age=data['age'])
        for key, value in data.items():
            if hasattr(self, 'clean_%s' % (key, )):
                value = getattr(self, 'clean_%s' % (key, ))(value)
            cleaned_data[key] = value
        self.clean(cleaned_data)
        self.data = utils.to_namedtuple(data)
        self.cleaned_data = utils.to_namedtuple(cleaned_data)
        self.config = utils.to_namedtuple(self.Config(calculator=self))
        self._validate_maximum_entry_age()
        self._validate_maximum_maturity_age()
        self._validate_maximum_policy_term()

    def _get_term(self, maturity_age, age):
        term = maturity_age - age
        if self.maximum_continuous_term and term > self.maximum_continuous_term:
            term = max(term, self.maximum_maturity_age - age)
        return term

    def _validate_maximum_entry_age(self):
        if (self.maximum_entry_age and
           self.maximum_entry_age < self.cleaned_data.age):
            raise OutOfScope("maximum_entry_age exceeded, %d < %d" % (
                self.maximum_entry_age,
                self.cleaned_data.age,
            ))

    def _validate_maximum_maturity_age(self):
        if (self.maximum_maturity_age and
           self.maximum_maturity_age < self.cleaned_data.age + self.cleaned_data.term):
            raise OutOfScope("maximum_maturity_age exceeded, %d < %d" % (
                self.maximum_maturity_age,
                self.cleaned_data.age + self.cleaned_data.term,
            ))

    def _validate_maximum_policy_term(self):
        # TODO: lru_cache following query
        maximum_policy_term = max(reduce(
            lambda x, y: x+y, self.premium_rates.values_list('term', flat=True).distinct()
        ))
        if self.cleaned_data.term > maximum_policy_term:
            raise OutOfScope("policy term is greater than maximum_policy_term, %d < %d" % (
                maximum_policy_term,
                self.cleaned_data.term,
            ))

    @lru_cache.lru_cache()
    def all(self):
        """Only for debugging purpose, too expensive"""
        return {
            'including_premium': self.calculate(),
            'excluding_premium': self.excluding_premium(),
            'discount': self.discount(),
            'premium_rate': self.premium_rate(),
        }

    def calculate(self):
        return {
            'annual': {
                'label': intcomma(self.calculate_annual()),
                'value': self.calculate_annual(),
            }
        }

    @lru_cache.lru_cache()
    @_round
    def calculate_annual(self):
        including_premium = self.including_premium()
        if self.minimum_premium is not None and including_premium < self.minimum_premium:
            raise OutOfScope("minimum_premium is greater than premium calculated, %d < %d" % (
                including_premium,
                self.minimum_premium,
            ))
        else:
            return including_premium

    @lru_cache.lru_cache()
    @_float
    def including_premium(self):
        excluding_premium = self.excluding_premium()
        return excluding_premium + (excluding_premium/100 * self.service_tax_percentage)

    @lru_cache.lru_cache()
    @_float
    def excluding_premium(self):
        return self.base_premium() - self.discount()

    @lru_cache.lru_cache()
    @_float
    def base_premium(self):
        """Premium w/o ST w/o discount"""
        return (self.cleaned_data.sum_assured / 1000) * self.premium_rate()

    @lru_cache.lru_cache()
    @_float
    def discount(self):
        return 0

    @lru_cache.lru_cache()
    # From PremiumRate model
    def _premium_rate_object(self):
        try:
            filters = {
                'age': self.cleaned_data.age,
                'term__contains': [self.cleaned_data.term],
                'frame__sum_assured__contains': NumericRange(
                    int(self.cleaned_data.sum_assured), int(self.cleaned_data.sum_assured), bounds='[]'
                ),
                'frame__gender': self.cleaned_data.gender,
                'frame__tobacco_user': self.cleaned_data.tobacco_user,
            }
            return self.premium_rates.get(**filters)
        except PremiumRate.DoesNotExist:
            raise OutOfScope("Premium rate not found for %s" % (unicode(filters), ))

    @_float
    def premium_rate(self):
        return self._premium_rate_object().value

    class Config(AbstractCalculator.Config):

        def __init__(self, calculator):
            super(BaseCalculator.Config, self).__init__()
            self.calculator = calculator
            self.update({
                'buy_url': self.buy_url,
                'medical_required_condition_smoker': self.medical_required_condition_smoker,
                'medical_required_condition_non_smoker': self.medical_required_condition_non_smoker,
                'about_us': self.about_us,
                'frequency': self.frequency,
            })

        buy_url = None
        medical_required_condition_smoker = 'Medical is mandatory for all cases'
        medical_required_condition_non_smoker = medical_required_condition_smoker
        about_us = []
        frequency = []


class BajajAllianzCalculator(BaseCalculator):
    """i-Secure Plan"""

    def __formula__(self):
        return """
            formula:
            base_premium = SA/1000 * premium_rate
            discount = base_premium * (_rebate_rate / 100)
            _rebate_rate = (given in table)
            total_premium = (base_premium - discount) + ((base_premium - discount)/100 * ST)
        """

    def clean_term(self, term):
        return (
            10 if 0 < term <= 10 else
            15 if 10 < term <= 15 else
            20 if 15 < term <= 20 else
            25 if 20 < term <= 25 else
            30 if 25 < term <= 30 else
            term
        )

    @_float
    def discount(self):
        return self.base_premium() * (self._rebate_rate() / 100)

    def _rebate_rate(self):
        hsr_rate = self._hsr_rate()
        up_in_sum_assured = self.cleaned_data.sum_assured - hsr_rate['min_sa']
        return (hsr_rate['increasing'] * int(up_in_sum_assured/hsr_rate['per'])) + hsr_rate['base']

    def _hsr_rate(self):
        """High SA Rebate"""
        sa = int(self.cleaned_data.sum_assured / LAKH)
        if 20 <= sa < 25:
            rate = (
                [0.0, 0.5],
                [0.0, 1.5],
            )
            min_sa = 20
        elif 25 <= sa < 30:
            rate = (
                [2.5, 0.5],
                [7.5, 1.5],
            )
            min_sa = 25
        elif 30 <= sa < 35:
            rate = (
                [5.0, 0.5],
                [15.0, 0.5],
            )
            min_sa = 30
        elif 35 <= sa < 40:
            rate = (
                [7.5, 0.0],
                [17.5, 1.0],
            )
            min_sa = 35
        elif 40 <= sa < 45:
            rate = (
                [7.5, 0.5],
                [22.5, 0.5],
            )
            min_sa = 40
        elif 45 <= sa < 75:
            rate = (
                [10.0, 0.8],
                [25.0, 0.25],
            )
            min_sa = 45
        elif 75 <= sa < 100:
            rate = (
                [12.5, 0.0],
                [32.5, 0.10],
            )
            min_sa = 75
        elif sa >= 100:
            rate = (
                [12.5, 0.0],
                [35.0, 0.0],
            )
            min_sa = 100
        else:
            rate = (
                [0.0, 0.0],
                [0.0, 0.0],
            )
            min_sa = 0

        if self.cleaned_data.tobacco_user:
            rebate = rate[0]
        else:
            rebate = rate[1]

        return {
            'min_sa': min_sa * LAKH,
            'base': rebate[0],
            'increasing': rebate[1],
            'per': LAKH,
        }


class EdelweissTokioCalculator(BaseCalculator):
    """MyLife Plan"""

    def __formula__(self):
        return """
            formula:
            base_premium = SA/1000 * premium_rate
            discount = base_premium * (_rebate_rate / 100)
            _rebate_rate = (given in table)
            total_premium = (base_premium - discount) + ((base_premium - discount)/100 * ST)

            rules:
            premium can not be less than 2500 **before applying ST**
            maturity age is max 80
            female age  =  male - 3
            **if maturity age == max_maturity_age is reached then last column of discount table is used**
        """

    minimum_premium = 2500 * (1 + BaseCalculator.service_tax_percentage/100)
    maximum_maturity_age = 80
    maximum_continuous_term = 40

    def clean(self, cleaned_data):
        if cleaned_data['gender'] == Frame.FEMALE:
            cleaned_data['age'] = max(cleaned_data['age'] - 3, 18)
        cleaned_data['gender'] = None

        if cleaned_data['sum_assured'] < 50*LAKH:
            cleaned_data['tobacco_user'] = None

    def clean_term(self, term):
        return (
            10 if 0 < term <= 10 else
            15 if 10 < term <= 15 else
            20 if 15 < term <= 20 else
            25 if 20 < term <= 25 else
            30 if 25 < term <= 30 else
            30 if 25 < term <= 30 else
            35 if 30 < term <= 35 else
            40 if 35 < term <= 40 else
            term
        )

    @_float
    def discount(self):
        return self._rebate_rate()

    def _rebate_rate(self):
        hsr_rate = self._hsr_rate()
        up_in_sum_assured = self.cleaned_data.sum_assured - hsr_rate['min_sa']
        return (hsr_rate['increasing'] * int(up_in_sum_assured/hsr_rate['per'])) + hsr_rate['base']

    def _hsr_rate(self):
        sa = self.cleaned_data.sum_assured / LAKH
        if 25 <= sa < 50:
            rate = (165, 165, 175, 190, 205, 220, 235, 290, )
            jump = 5
            min_sa = 25
        elif 50 <= sa < 100:
            rate = (145, 150, 155, 160, 175, 185, 215, 250, )
            jump = 10
            min_sa = 50
        elif 100 <= sa < 200:
            rate = (50, 55, 60, 65, 90, 110, 125, 185, )
            jump = 10
            min_sa = 100
        elif sa >= 200:
            rate = (135, 135, 135, 135, 135, 135, 135, 135, )
            jump = 25
            min_sa = 200
        else:
            rate = (0, )*8
            jump = 0
            min_sa = 0

        term = self.cleaned_data.term
        rebate = dict(zip(
            (
                term == 10,
                term == 15,
                term == 20,
                term == 25,
                term == 30,
                term == 35,
                term == 40,
                40 < term or term + self.cleaned_data.age == self.maximum_maturity_age,  # Seems very odd I know
            ),
            rate
        ))[True]

        return {
            'min_sa': min_sa * LAKH,
            'base': 0,
            'increasing': rebate,
            'per': jump * LAKH,
        }

    class Config(BaseCalculator.Config):
        buy_url = 'http://ed.edelweisstokio.in/MyLifeplus.aspx?src=A5A007'
        about_us = [
                (
                    "Edelweiss Tokio Life Insurance is a new generation Insurance Company,"
                    " set up with a start up capital of INR 550 Crores, thereby showing our commitment"
                    " to building a long term sustainable business focused on a consumer centric approach."
                ),
                (
                    "The company is a joint venture between Edelweiss Financial Services, one of India's leading"
                    " diversified financial services companies with business straddling across Credit, Capital Markets,"
                    " Asset Management, Housing finance and Insurance and Tokio Marine Holdings Inc, one of the oldest and"
                    " the biggest Insurance companies in Japan now with presence across 39 countries around the world."
                ),
            ]
        frequency = [FREQUENCY.Y, ]


class AegonCalculator(BaseCalculator):
    """iterm2"""

    maximum_maturity_age = 75
    service_tax_percentage = 14
    maximum_continuous_term = 40

    def __formula__(self):
        return """
            formula:
            base_premium = SA/1000 * premium_rate
            total_premium = base_premium + (base_premium/100 * ST)
        """

    def clean(self, cleaned_data):
        if cleaned_data['sum_assured'] < 25*LAKH:
            cleaned_data['tobacco_user'] = None

    class Config(BaseCalculator.Config):
        buy_url = 'https://buynow.aegonlife.com/iterm?utm_source=Coverfox&utm_medium=referral'\
            '&utm_campaign=iTerm&sourcekey=iT|Referral|Coverfox|Nov15'
        medical_required_condition_non_smoker = 'Upto 25 lakhs sum assured'
        about_us = [
            (
                "Aegon Life Insurance Company Limited (formerly AEGON Religare Life Insurance"
                " Company Limited) launched its pan-India operations in July, 2008 following a multi-channel"
                " distribution strategy with a vision to help people plan their life better."
            ),
            (
                ".Aegon, an international provider of lifeinsurance, pensions and asset management and Bennett,"
                " Coleman & Company, India's leading media conglomerate, have come together to launch"
                " Aegon Life Insurance."
            ),
            (
                "The company is headquartered in Mumbai having 59 branches across"
                " 46 cities.The company has around 9600 life insurance agents serving over 4 lakh customers across India"
            ),
        ]
        frequency = [FREQUENCY.Y, ]


class BirlaSunLifeCalculator(BaseCalculator):
    """Birla Sun Life Protect Level"""

    def __formula__(self):
        return """
            formula:
            base_premium = SA/1000 * premium_rate
            total_premium = base_premium + (base_premium/100 * ST)
            premium_rate_1 = sheet matching 50*LAKH SA
            premium_rate_2 = sheet matching SA
            total_premium = (50LAKH * premium_rate_1) + ((SA - 50LAKH)*premium_rate_2)
        """

    def clean(self, cleaned_data):
        if cleaned_data['gender'] == Frame.FEMALE:
            cleaned_data['age'] = max(cleaned_data['age'] - 3, 18)
        cleaned_data['gender'] = None

    @lru_cache.lru_cache()
    @_float
    def base_premium(self):
        """Premium w/o ST w/o discount"""
        premium_rate_1 = self.premium_rate()
        if (self.cleaned_data.sum_assured - 50*LAKH) > 0:
            data_with_50_sa = self.cleaned_data.__dict__
            data_with_50_sa['sum_assured'] = 50*LAKH
            premium_rate_2 = BirlaSunLifeCalculator(data=data_with_50_sa, plan=self.plan).premium_rate()
        else:
            premium_rate_2 = premium_rate_1
        return (
            (((self.cleaned_data.sum_assured - 50*LAKH) / 1000) * premium_rate_1) +
            ((50*LAKH / 1000) * premium_rate_2)
        )

    class Config(BaseCalculator.Config):
        buy_url = 'http://insurance.birlasunlife.com/buy-term-insurance-online/Calculator/CalculatePremium?UID=8'
        about_us = [
            (
                "Birla Sun Life Insurance Company Limited (BSLI) is a joint venture between the Aditya Birla Group,"
                " a well known Indian conglomerate and Sun Life Financial Inc, one of the leading international"
                " financial services organizations from Canada."
            ),
        ]
        frequency = [FREQUENCY.Y, FREQUENCY.M, ]


class ICICIPrudentialCalculator(BaseCalculator):
    """ICICI Prudential iProtect smart life"""

    def __formula__(self):
        return """
            formula:
            base_premium = premium_rate
            total_premium = base_premium + (base_premium/100 * ST)

            rules:
            premium can not be less than 3600
            maturity age is max 75
            female age  =  male - 2
            for SA < 50Lakh, tobacco consumption doesn't matter
        """

    minimum_premium = 3600
    maximum_maturity_age = 75
    maximum_entry_age = 60

    @lru_cache.lru_cache()
    @_float
    def premium_rate(self):
        """
        Calc for 54 lakhs  - (premium for 50) + (Premium for 60 - Premium for 50)* ((54-50)/(60-50))
        """
        sa = float(self.cleaned_data.sum_assured / LAKH)

        if sa < 3:
            new_data = self.cleaned_data.__dict__
            new_data['sum_assured'] = 3*LAKH
            new_calculator = ICICIPrudentialCalculator(data=new_data, plan=self.plan)
            new_premium_rate = new_calculator.premium_rate()
            return (sa * new_premium_rate / 3.0)

        elif 3 <= sa <= 500:

            premium_rate_object = self._premium_rate_object()
            if self.cleaned_data.sum_assured - premium_rate_object.frame.sum_assured.lower:
                lower_data = self.cleaned_data.__dict__
                lower_data['sum_assured'] = premium_rate_object.frame.sum_assured.lower
                upper_data = self.cleaned_data.__dict__
                upper_data['sum_assured'] = premium_rate_object.frame.sum_assured.upper

                lower_calculator = ICICIPrudentialCalculator(data=lower_data, plan=self.plan)
                upper_calculator = ICICIPrudentialCalculator(data=upper_data, plan=self.plan)

                lower_premium_rate = lower_calculator.premium_rate()
                upper_premium_rate = upper_calculator.premium_rate()

                return lower_premium_rate + (
                    (upper_premium_rate - lower_premium_rate) *
                    (self.cleaned_data.sum_assured - premium_rate_object.frame.sum_assured.lower) /
                    (premium_rate_object.frame.sum_assured.upper - premium_rate_object.frame.sum_assured.lower)
                )
            else:
                return premium_rate_object.value

        else:
            new_data = self.cleaned_data.__dict__
            new_data['sum_assured'] = 5*CRORE
            new_calculator = ICICIPrudentialCalculator(data=new_data, plan=self.plan)
            new_premium_rate = new_calculator.premium_rate()
            return (sa/100 * new_premium_rate/5)

    @lru_cache.lru_cache()
    @_float
    def base_premium(self):
        """base premiums are given to us by icici instead of premium rates"""
        return self.premium_rate()

    @lru_cache.lru_cache()
    @_float
    def discount(self):
        return self.base_premium() * 0.01  # 1% discount

    def clean(self, cleaned_data):
        if cleaned_data['gender'] == Frame.FEMALE:
            cleaned_data['age'] = max(cleaned_data['age'] - 2, 18)
        cleaned_data['gender'] = None

        if cleaned_data['sum_assured'] < 50*LAKH:
            cleaned_data['tobacco_user'] = None

    class Config(BaseCalculator.Config):
        buy_url = 'https://onlinelifeinsurance.iciciprulife.com/buy_new/merger/'\
            'EBI_Merger/T46_EBI/T46.html?V=1453294789385&plan=term&UID=1527'
        medical_required_condition_smoker = 'Upto 50 lakhs sum assured'
        medical_required_condition_non_smoker = medical_required_condition_smoker
        about_us = [
            (
                "ICICI Prudential Life Insurance Company is a joint venture between ICICI Bank,"
                " a premier financial powerhouse,and Prudential plc, a leading international financial"
                " services group."
            ),
            (
                "ICICI Prudential began its operations in December 2000 after receiving"
                " approval from Insurance Regulatory Development Authority of India (IRDAI). ICICI"
                " Prudential Life Insurance has maintained its focus on offering a wide range of"
                " flexible products that meet the needs of the Indian customer at every step in life."
            ),
        ]
        frequency = [FREQUENCY.Y, FREQUENCY.H, FREQUENCY.Q, FREQUENCY.M, ]


class HDFCLifeCalculator(BaseCalculator):
    """HDFC Life Click2Connect"""

    def __formula__(self):
        return """
            formula:
            base_premium = SA/1000 * premium_rate
            total_premium = (base_premium - discount) + ((base_premium - discount)/100 * ST)

            rules:
            premium can not be less than 3000
            maturity age is max 75
        """

    minimum_premium = 3000
    maximum_maturity_age = 75

    @lru_cache.lru_cache()
    @_float
    def discount(self):
        return self.base_premium() * 0.055  # 5.5% discount

    class Config(BaseCalculator.Config):

        medical_required_condition_smoker = 'NA'
        medical_required_condition_non_smoker = medical_required_condition_smoker
        about_us = [
            (
                "HDFC Life is one of the leading life insurance companies in India offering"
                " a range of individual and group insurance solutions that meet various customer"
                " needs such as Protection, Pension, Savings & Investment and Health, along with Children's & Women's Plan",
            ),
            (
                "HDFC Life is a joint venture between Housing Development Finance Corporation Limited"
                " (HDFC), India's leading housing finance institution and Standard Life plc, the leading"
                " provider of financial services in the United Kingdom.",
            ),
        ]
        frequency = [FREQUENCY.Y, FREQUENCY.H, FREQUENCY.Q, FREQUENCY.M, ]

        @property
        def buy_url(self):
            return 'http://ops.hdfclife.com/ops/click2protectPlus.do'\
                '?source=A_Coverfox_C2P&mobilenumber=%(mobile)s'\
                '&email=%(email)s&sumassured=%(sum_assured)d&smoker=%(tobacco_user)s&gender=%(gender)s'\
                '&typeofcover=L&premiumterm=%(term)d&premiummode=R&billingfrequency=01&premiumpayterm=%(term)d&'\
                'premiumamount=%(premium)d&monthPayoutType=0&agentcode=00617746' % {
                    'mobile': self.calculator.cleaned_data.mobile,
                    'email': self.calculator.cleaned_data.email,
                    'sum_assured': self.calculator.cleaned_data.sum_assured,
                    'tobacco_user': 'Y' if self.calculator.data.tobacco_user else 'N',
                    'gender': 'M' if self.calculator.data.gender == 1 else 'F',
                    'term': self.calculator.cleaned_data.term,
                    'premium': self.calculator.calculate()['annual']['value'],
                }


class HDFCExtraLifeCalculator(HDFCLifeCalculator):
    class Config(HDFCLifeCalculator.Config):
        @property
        def buy_url(self):
            return 'http://ops.hdfclife.com/ops/click2protectPlus.do'\
                '?source=A_Coverfox_C2P&mobilenumber=%(mobile)s'\
                '&email=%(email)s&sumassured=%(sum_assured)d&smoker=%(tobacco_user)s&gender=%(gender)s'\
                '&typeofcover=E&premiumterm=%(term)d&premiummode=R&billingfrequency=01&premiumpayterm=%(term)d&'\
                'premiumamount=%(premium)d&monthPayoutType=0&agentcode=00617746' % {
                    'mobile': self.calculator.cleaned_data.mobile,
                    'email': self.calculator.cleaned_data.email,
                    'sum_assured': self.calculator.cleaned_data.sum_assured,
                    'tobacco_user': 'Y' if self.calculator.data.tobacco_user else 'N',
                    'gender': 'M' if self.calculator.data.gender == 1 else 'F',
                    'term': self.calculator.cleaned_data.term,
                    'premium': self.calculator.calculate()['annual']['value'],
                }


class FutureGeneraliCalculator(BaseCalculator):
    """Future Generali Flexi Online"""

    def __formula__(self):
        return """
            formula:
            base_premium = SA/1000 * premium_rate
            total_premium = base_premium + (base_premium/100 * ST)
        """

    def clean(self, cleaned_data):
        if cleaned_data['gender'] == Frame.FEMALE:
            cleaned_data['age'] = max(cleaned_data['age'] - 3, 18)
        cleaned_data['gender'] = None

    class Config(BaseCalculator.Config):
        about_us = [
            (
                "Future Generali is a joint venture between India's leading retailer Future Group, Italy"
                " based insurance major Generali and Industrial Investment Trust Ltd. (IITL)."
            ),
            (
                "The Company was incorporated in 2007 and brings together the unique qualities of the"
                " founding Companies - local experience and knowledge with global insurance expertise."
            ),
            (
                "Future Generali offers an extensive range of life insurance products, and a network that"
                " ensures we are close to you wherever you go."
            ),
        ]
        frequency = [FREQUENCY.Y, ]

        @property
        def buy_url(self):
            return 'http://buyonline.life.futuregenerali.in/flexi-online-term-insurance/Default.aspx?'\
                'sourcekey=CFO&UTM_Source=CFO&MobileNO=%(mobile)s&EmailID=%(email)s&Smoker=%(tobacco_user)d'\
                '&Gender=%(gender)d&Nationality=1&AnnualIncome=%(salary)d'\
                '&EBI_Merger/T46_EBI/T46.html?V=1453294789385&plan=term&UID=1527' % {
                    'mobile': self.calculator.cleaned_data.mobile,
                    'email': self.calculator.cleaned_data.email,
                    'tobacco_user': 1 if self.calculator.data.tobacco_user else 0,
                    'gender': self.calculator.data.gender,
                    'salary': self.calculator.cleaned_data.salary,
                }
