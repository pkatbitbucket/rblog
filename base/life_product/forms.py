from django import forms
from django.contrib.auth import get_user_model

from .models import Frame, PremiumRate
from insurer import calculators as calculator_module

import inspect

User = get_user_model()
LAKH = 100000


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('gender', )


class PremiumRateForm(forms.ModelForm):
    class Meta:
        model = PremiumRate
        fields = ('age', )


class QuoteForm(forms.Form):
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    gender = forms.TypedChoiceField(
        GENDER_CHOICES, coerce=lambda value: {'M': 1, 'F': 2}[value], required=True
    )

    tobacco_user = forms.BooleanField(required=False)
    age = PremiumRateForm.base_fields['age']
    sum_assured = forms.IntegerField(required=False, min_value=10*LAKH, max_value=1000*LAKH)  # 10lakh-10Crore
    salary = forms.IntegerField(min_value=3*LAKH)
    mobile = forms.IntegerField(required=False, min_value=7000000000, max_value=9999999999)
    email = forms.EmailField(required=False)
    maturity_age = """SET ON RUNTIME"""

    def __init__(self, *args, **kwargs):
        super(QuoteForm, self).__init__(*args, **kwargs)
        if PremiumRate.objects.exists():
            all_policy_terms = reduce(
                lambda x, y: x+y, PremiumRate.objects.values_list('term', flat=True).distinct()
            )
            self.min_policy_term, self.max_policy_term = min(all_policy_terms), max(all_policy_terms)
        else:
            self.min_policy_term, self.max_policy_term = 5, 62
        self.fields['maturity_age'] = forms.IntegerField(
            min_value=self.min_policy_term + 18, max_value=self.max_policy_term + 18,
            required=False,
        )

        self.fields['gender'].required = True

    def _clean_form(self):
        if self.errors:
            return
        else:
            return super(QuoteForm, self)._clean_form()

    def clean_mobile(self):
        return str(self.cleaned_data['mobile'])

    def clean(self):
        cd = self.cleaned_data

        # maturity_age
        cd['maturity_age'] = max(cd['age'] + self.min_policy_term, cd.get('maturity_age') or 60)

        # term
        term = cd['maturity_age'] - cd['age']
        if not (self.min_policy_term <= term <= self.max_policy_term):
            raise forms.ValidationError(
                'Maturity age allowed at the age of %d is in between %d-%d' % (
                    cd['age'],
                    cd['age'] + self.min_policy_term,
                    self.fields['maturity_age'].max_value,
                )
            )

        # sum_assured
        sum_assured = cd['sum_assured']
        if not sum_assured:
            cd['sum_assured'] = min(cd['salary'] * 20, 100*LAKH)

        return cd


def is_calculator(obj):
    return (
        inspect.isclass(obj) and
        issubclass(obj, calculator_module.base.AbstractCalculator) and
        obj is not calculator_module.base.AbstractCalculator
    )
calculators = dict(inspect.getmembers(calculator_module, is_calculator))


class PlanAdminForm(forms.ModelForm):
    CALCULATOR_CHOICES = zip(calculators.keys(), calculators.keys())

    def __init__(self, *args, **kwargs):
        super(PlanAdminForm, self).__init__(*args, **kwargs)
        if self.instance.calculator:
            try:
                self.initial['calculator'] = self.fields['calculator'].initial = self.instance.calculator.__name__
            except AttributeError:
                self.instance.calculator = None
                self.instance.save()

    calculator = forms.TypedChoiceField(
        CALCULATOR_CHOICES, coerce=lambda value: calculators[value], required=False, empty_value=None
    )
