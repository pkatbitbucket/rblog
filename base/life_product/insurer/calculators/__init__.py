from __future__ import absolute_import

from .aegon import AegonITermCalculator
from .birla import BirlaSunLifeProtectAtEaseCalculator
from .edelweiss import EdelweissTokioMyLifePlusCalculator
from .future import FutureGeneraliFlexiOnlineCalculator
from .hdfc import HDFCLifeClick2ProtectPlusLife, HDFCLifeClick2ProtectPlusExtraLifeCalculator
from .icici import ICICIPrudentialiProtectSmartLifeCalculator

__all__ = [
    'AegonITermCalculator',
    'BirlaSunLifeProtectAtEaseCalculator',
    'EdelweissTokioMyLifePlusCalculator',
    'FutureGeneraliFlexiOnlineCalculator',
    'HDFCLifeClick2ProtectPlusLife', 'HDFCLifeClick2ProtectPlusExtraLifeCalculator',
    'ICICIPrudentialiProtectSmartLifeCalculator',
]
