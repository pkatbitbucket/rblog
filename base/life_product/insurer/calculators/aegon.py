from .base import BaseCalculator, LAKH, FREQUENCY


class AegonITermCalculator(BaseCalculator):
    """iterm."""

    maximum_maturity_age = 75
    service_tax_percentage = 14
    maximum_continuous_term = 40

    def __formula__(self):
        return """
            formula:
            base_premium = SA/1000 * premium_rate
            total_premium = base_premium + (base_premium/100 * ST)
        """

    def clean(self, cleaned_data):
        if cleaned_data['sum_assured'] < 25*LAKH:
            cleaned_data['tobacco_user'] = None

    class Config(BaseCalculator.Config):
        buy_url = 'https://buynow.aegonlife.com/iterm?utm_source=Coverfox&utm_medium=referral'\
            '&utm_campaign=iTerm&sourcekey=iT|Referral|Coverfox|Nov15'
        medical_required_condition_non_smoker = 'Upto 25 lakhs sum assured'
        about_us = [
            (
                "Aegon Life Insurance Company Limited (formerly AEGON Religare Life Insurance"
                " Company Limited) launched its pan-India operations in July, 2008 following a multi-channel"
                " distribution strategy with a vision to help people plan their life better."
            ),
            (
                ".Aegon, an international provider of lifeinsurance, pensions and asset management and Bennett,"
                " Coleman & Company, India's leading media conglomerate, have come together to launch"
                " Aegon Life Insurance."
            ),
            (
                "The company is headquartered in Mumbai having 59 branches across"
                " 46 cities.The company has around 9600 life insurance agents serving over 4 lakh customers across India"
            ),
        ]
        frequency = [FREQUENCY.Y, ]
