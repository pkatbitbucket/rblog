from __future__ import absolute_import

from django.conf import settings
from django.contrib.humanize.templatetags.humanize import intcomma
from django.utils import lru_cache

from life_product.models import PremiumRate
from psycopg2.extras import NumericRange

from functools import wraps

import utils


LAKH = 100000
CRORE = 100 * LAKH

FREQUENCY = utils.to_namedtuple({
    'M': 'Monthly',
    'Q': 'Quarterly',
    'H': 'Half yearly',
    'Y': 'Yearly',
})


class OutOfScope(Exception):
    pass


def _float(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        return float(func(*args, **kwargs))
    return wrapper


def _round(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        return int(round(func(*args, **kwargs)))
    return wrapper


class AbstractCalculator(object):
    def calculate(self):
        """Gets you the premium including ST if validations are passed"""
        pass

    def including_premium(self):
        """Gets you the premium including ST"""
        pass

    def excluding_premium(self):
        """Gets you the premium excluding ST"""
        pass

    def premium_rate(self):
        "From PremiumRate model"
        pass

    def discount(self):
        "Discount applicable, lookup table hardcoded here"
        pass

    def clean(self, cleaned_data):
        """Can add custom validations here or clean cleaned_data further"""
        pass

    class Config(dict):
        """Contains configurable insurer/plan specific values, should ideally be picked from db"""


class BaseCalculator(AbstractCalculator):
    def __formula__(self):
        return """
            formula:
            base_premium = SA/1000 * premium_rate
            total_premium = (base_premium - discount) + ((base_premium - discount)/100 * ST)
        """

    minimum_premium = None
    maximum_maturity_age = None
    maximum_entry_age = None
    maximum_continuous_term = None
    service_tax_percentage = settings.SERVICE_TAX_PERCENTAGE

    def __init__(self, plan, data={}):
        self.plan = plan
        self.premium_rates = PremiumRate.objects.filter(frame__plan=self.plan)
        assert self.premium_rates.exists(), "Premium rates absent"
        cleaned_data = {}
        data['term'] = self._get_term(maturity_age=data['maturity_age'], age=data['age'])
        for key, value in data.items():
            if hasattr(self, 'clean_%s' % (key, )):
                value = getattr(self, 'clean_%s' % (key, ))(value)
            cleaned_data[key] = value
        self.clean(cleaned_data)
        self.data = utils.to_namedtuple(data)
        self.cleaned_data = utils.to_namedtuple(cleaned_data)
        self.config = utils.to_namedtuple(self.Config(calculator=self))
        self._validate_maximum_entry_age()
        self._validate_maximum_maturity_age()
        self._validate_maximum_policy_term()

    def _get_term(self, maturity_age, age):
        term = maturity_age - age
        if self.maximum_continuous_term and term > self.maximum_continuous_term:
            term = max(term, self.maximum_maturity_age - age)
        return term

    def _validate_maximum_entry_age(self):
        if (self.maximum_entry_age and
           self.maximum_entry_age < self.cleaned_data.age):
            raise OutOfScope("maximum_entry_age exceeded, %d < %d" % (
                self.maximum_entry_age,
                self.cleaned_data.age,
            ))

    def _validate_maximum_maturity_age(self):
        if (self.maximum_maturity_age and
           self.maximum_maturity_age < self.cleaned_data.age + self.cleaned_data.term):
            raise OutOfScope("maximum_maturity_age exceeded, %d < %d" % (
                self.maximum_maturity_age,
                self.cleaned_data.age + self.cleaned_data.term,
            ))

    def _validate_maximum_policy_term(self):
        # TODO: lru_cache following query
        maximum_policy_term = max(reduce(
            lambda x, y: x+y, self.premium_rates.values_list('term', flat=True).distinct()
        ))
        if self.cleaned_data.term > maximum_policy_term:
            raise OutOfScope("policy term is greater than maximum_policy_term, %d < %d" % (
                maximum_policy_term,
                self.cleaned_data.term,
            ))

    @lru_cache.lru_cache()
    def all(self):
        """Only for debugging purpose, too expensive"""
        return {
            'including_premium': self.calculate(),
            'excluding_premium': self.excluding_premium(),
            'discount': self.discount(),
            'premium_rate': self.premium_rate(),
        }

    def calculate(self):
        return {
            'annual': {
                'label': intcomma(self.calculate_annual()),
                'value': self.calculate_annual(),
            }
        }

    @lru_cache.lru_cache()
    @_round
    def calculate_annual(self):
        including_premium = self.including_premium()
        if self.minimum_premium is not None and including_premium < self.minimum_premium:
            raise OutOfScope("minimum_premium is greater than premium calculated, %d < %d" % (
                including_premium,
                self.minimum_premium,
            ))
        else:
            return including_premium

    @lru_cache.lru_cache()
    @_float
    def including_premium(self):
        excluding_premium = self.excluding_premium()
        return excluding_premium + (excluding_premium/100 * self.service_tax_percentage)

    @lru_cache.lru_cache()
    @_float
    def excluding_premium(self):
        return self.base_premium() - self.discount()

    @lru_cache.lru_cache()
    @_float
    def base_premium(self):
        """Premium w/o ST w/o discount"""
        return (self.cleaned_data.sum_assured / 1000) * self.premium_rate()

    @lru_cache.lru_cache()
    @_float
    def discount(self):
        return 0

    @lru_cache.lru_cache()
    # From PremiumRate model
    def _premium_rate_object(self):
        try:
            filters = {
                'age': self.cleaned_data.age,
                'term__contains': [self.cleaned_data.term],
                'frame__sum_assured__contains': NumericRange(
                    int(self.cleaned_data.sum_assured), int(self.cleaned_data.sum_assured), bounds='[]'
                ),
                'frame__gender': self.cleaned_data.gender,
                'frame__tobacco_user': self.cleaned_data.tobacco_user,
            }
            return self.premium_rates.get(**filters)
        except PremiumRate.DoesNotExist:
            raise OutOfScope("Premium rate not found for %s" % (unicode(filters), ))

    @_float
    def premium_rate(self):
        return self._premium_rate_object().value

    class Config(AbstractCalculator.Config):

        def __init__(self, calculator):
            super(BaseCalculator.Config, self).__init__()
            self.calculator = calculator
            self.update({
                'buy_url': self.buy_url,
                'medical_required_condition_smoker': self.medical_required_condition_smoker,
                'medical_required_condition_non_smoker': self.medical_required_condition_non_smoker,
                'about_us': self.about_us,
                'frequency': self.frequency,
            })

        buy_url = None
        medical_required_condition_smoker = 'Medical is mandatory for all cases'
        medical_required_condition_non_smoker = medical_required_condition_smoker
        about_us = []
        frequency = []
