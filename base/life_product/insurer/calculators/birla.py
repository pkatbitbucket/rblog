from life_product.models import Frame

from .base import BaseCalculator, _float, LAKH, FREQUENCY


class BirlaSunLifeProtectAtEaseCalculator(BaseCalculator):
    """Birla Sun Life Protect Level"""

    def __formula__(self):
        return """
            formula:
            base_premium = SA/1000 * premium_rate
            total_premium = base_premium + (base_premium/100 * ST)
            premium_rate_1 = sheet matching 50*LAKH SA
            premium_rate_2 = sheet matching SA
            total_premium = (50LAKH * premium_rate_1) + ((SA - 50LAKH)*premium_rate_2)
        """

    def clean(self, cleaned_data):
        if cleaned_data['gender'] == Frame.FEMALE:
            cleaned_data['age'] = max(cleaned_data['age'] - 3, 18)
        cleaned_data['gender'] = None

    @_float
    def base_premium(self):
        """Premium w/o ST w/o discount"""
        premium_rate_1 = self.premium_rate()
        if (self.cleaned_data.sum_assured - 50*LAKH) > 0:
            data_with_50_sa = self.cleaned_data.__dict__
            data_with_50_sa['sum_assured'] = 50*LAKH
            premium_rate_2 = BirlaSunLifeProtectAtEaseCalculator(data=data_with_50_sa, plan=self.plan).premium_rate()
        else:
            premium_rate_2 = premium_rate_1
        return (
            (((self.cleaned_data.sum_assured - 50*LAKH) / 1000) * premium_rate_1) +
            ((50*LAKH / 1000) * premium_rate_2)
        )

    class Config(BaseCalculator.Config):
        buy_url = 'http://insurance.birlasunlife.com/buy-term-insurance-online/Calculator/CalculatePremium?UID=8'
        about_us = [
            (
                "Birla Sun Life Insurance Company Limited (BSLI) is a joint venture between the Aditya Birla Group,"
                " a well known Indian conglomerate and Sun Life Financial Inc, one of the leading international"
                " financial services organizations from Canada."
            ),
        ]
        frequency = [FREQUENCY.Y, FREQUENCY.M, ]
