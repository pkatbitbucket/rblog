from life_product.models import Frame

from .base import BaseCalculator, _float, LAKH, FREQUENCY


class EdelweissTokioMyLifePlusCalculator(BaseCalculator):
    """MyLife Plan"""

    def __formula__(self):
        return """
            formula:
            base_premium = SA/1000 * premium_rate
            discount = base_premium * (_rebate_rate / 100)
            _rebate_rate = (given in table)
            total_premium = (base_premium - discount) + ((base_premium - discount)/100 * ST)

            rules:
            premium can not be less than 2500 **before applying ST**
            maturity age is max 80
            female age  =  male - 3
            **if maturity age == max_maturity_age is reached then last column of discount table is used**
        """

    minimum_premium = 2500 * (1 + BaseCalculator.service_tax_percentage/100)
    maximum_maturity_age = 80
    maximum_continuous_term = 40

    def clean(self, cleaned_data):
        if cleaned_data['gender'] == Frame.FEMALE:
            cleaned_data['age'] = max(cleaned_data['age'] - 3, 18)
        cleaned_data['gender'] = None

        if cleaned_data['sum_assured'] < 50*LAKH:
            cleaned_data['tobacco_user'] = None

    def clean_term(self, term):
        return (
            10 if 0 < term <= 10 else
            15 if 10 < term <= 15 else
            20 if 15 < term <= 20 else
            25 if 20 < term <= 25 else
            30 if 25 < term <= 30 else
            30 if 25 < term <= 30 else
            35 if 30 < term <= 35 else
            40 if 35 < term <= 40 else
            term
        )

    @_float
    def discount(self):
        return self._rebate_rate()

    def _rebate_rate(self):
        hsr_rate = self._hsr_rate()
        up_in_sum_assured = self.cleaned_data.sum_assured - hsr_rate['min_sa']
        return (hsr_rate['increasing'] * int(up_in_sum_assured/hsr_rate['per'])) + hsr_rate['base']

    def _hsr_rate(self):
        sa = self.cleaned_data.sum_assured / LAKH
        if 25 <= sa < 50:
            rate = (165, 165, 175, 190, 205, 220, 235, 290, )
            jump = 5
            min_sa = 25
        elif 50 <= sa < 100:
            rate = (145, 150, 155, 160, 175, 185, 215, 250, )
            jump = 10
            min_sa = 50
        elif 100 <= sa < 200:
            rate = (50, 55, 60, 65, 90, 110, 125, 185, )
            jump = 10
            min_sa = 100
        elif sa >= 200:
            rate = (135, 135, 135, 135, 135, 135, 135, 135, )
            jump = 25
            min_sa = 200
        else:
            rate = (0, )*8
            jump = 0
            min_sa = 0

        term = self.cleaned_data.term
        rebate = dict(zip(
            (
                term == 10,
                term == 15,
                term == 20,
                term == 25,
                term == 30,
                term == 35,
                term == 40,
                40 < term or term + self.cleaned_data.age == self.maximum_maturity_age,  # Seems very odd I know
            ),
            rate
        ))[True]

        return {
            'min_sa': min_sa * LAKH,
            'base': 0,
            'increasing': rebate,
            'per': jump * LAKH,
        }

    class Config(BaseCalculator.Config):
        buy_url = 'http://ed.edelweisstokio.in/MyLifeplus.aspx?src=A5A007'
        about_us = [
            (
                "Edelweiss Tokio Life Insurance is a new generation Insurance Company,"
                " set up with a start up capital of INR 550 Crores, thereby showing our commitment"
                " to building a long term sustainable business focused on a consumer centric approach."
            ),
            (
                "The company is a joint venture between Edelweiss Financial Services, one of India's leading"
                " diversified financial services companies with business straddling across Credit, Capital Markets,"
                " Asset Management, Housing finance and Insurance and Tokio Marine Holdings Inc, one of the oldest and"
                " the biggest Insurance companies in Japan now with presence across 39 countries around the world."
            ),
        ]
        frequency = [FREQUENCY.Y, ]
