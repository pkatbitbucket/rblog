from life_product.models import Frame

from .base import BaseCalculator, FREQUENCY


class FutureGeneraliFlexiOnlineCalculator(BaseCalculator):
    """Future Generali Flexi Online"""

    def __formula__(self):
        return """
            formula:
            base_premium = SA/1000 * premium_rate
            total_premium = base_premium + (base_premium/100 * ST)
        """

    def clean(self, cleaned_data):
        if cleaned_data['gender'] == Frame.FEMALE:
            cleaned_data['age'] = max(cleaned_data['age'] - 3, 18)
        cleaned_data['gender'] = None

    class Config(BaseCalculator.Config):
        about_us = [
            (
                "Future Generali is a joint venture between India's leading retailer Future Group, Italy"
                " based insurance major Generali and Industrial Investment Trust Ltd. (IITL)."
            ),
            (
                "The Company was incorporated in 2007 and brings together the unique qualities of the"
                " founding Companies - local experience and knowledge with global insurance expertise."
            ),
            (
                "Future Generali offers an extensive range of life insurance products, and a network that"
                " ensures we are close to you wherever you go."
            ),
        ]
        frequency = [FREQUENCY.Y, ]

        @property
        def buy_url(self):
            return 'http://buyonline.life.futuregenerali.in/flexi-online-term-insurance/Default.aspx?'\
                'sourcekey=CFO&UTM_Source=CFO&MobileNO=%(mobile)s&EmailID=%(email)s&Smoker=%(tobacco_user)d'\
                '&Gender=%(gender)d&Nationality=1&AnnualIncome=%(salary)d'\
                '&EBI_Merger/T46_EBI/T46.html?V=1453294789385&plan=term&UID=1527' % {
                    'mobile': self.calculator.cleaned_data.mobile,
                    'email': self.calculator.cleaned_data.email,
                    'tobacco_user': 1 if self.calculator.data.tobacco_user else 0,
                    'gender': self.calculator.data.gender,
                    'salary': self.calculator.cleaned_data.salary,
                }
