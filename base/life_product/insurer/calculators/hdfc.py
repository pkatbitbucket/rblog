from .base import BaseCalculator, _float, FREQUENCY


class HDFCLifeClick2ProtectPlusLife(BaseCalculator):
    """HDFC Life Click2Connect"""

    def __formula__(self):
        return """
            formula:
            base_premium = SA/1000 * premium_rate
            total_premium = (base_premium - discount) + ((base_premium - discount)/100 * ST)

            rules:
            premium can not be less than 3000
            maturity age is max 75
        """

    minimum_premium = 3000
    maximum_maturity_age = 75

    @_float
    def discount(self):
        return self.base_premium() * 0.055  # 5.5% discount

    class Config(BaseCalculator.Config):

        medical_required_condition_smoker = 'NA'
        medical_required_condition_non_smoker = medical_required_condition_smoker
        about_us = [
            (
                "HDFC Life is one of the leading life insurance companies in India offering"
                " a range of individual and group insurance solutions that meet various customer"
                " needs such as Protection, Pension, Savings & Investment and Health, along with Children's & Women's Plan",
            ),
            (
                "HDFC Life is a joint venture between Housing Development Finance Corporation Limited"
                " (HDFC), India's leading housing finance institution and Standard Life plc, the leading"
                " provider of financial services in the United Kingdom.",
            ),
        ]
        frequency = [FREQUENCY.Y, FREQUENCY.H, FREQUENCY.Q, FREQUENCY.M, ]

        @property
        def buy_url(self):
            return 'http://ops.hdfclife.com/ops/click2protectPlus.do'\
                '?source=A_Coverfox_C2P&mobilenumber=%(mobile)s'\
                '&email=%(email)s&sumassured=%(sum_assured)d&smoker=%(tobacco_user)s&gender=%(gender)s'\
                '&typeofcover=L&premiumterm=%(term)d&premiummode=R&billingfrequency=01&premiumpayterm=%(term)d&'\
                'premiumamount=%(premium)d&monthPayoutType=0&agentcode=00617746' % {
                    'mobile': self.calculator.cleaned_data.mobile,
                    'email': self.calculator.cleaned_data.email,
                    'sum_assured': self.calculator.cleaned_data.sum_assured,
                    'tobacco_user': 'Y' if self.calculator.data.tobacco_user else 'N',
                    'gender': 'M' if self.calculator.data.gender == 1 else 'F',
                    'term': self.calculator.cleaned_data.term,
                    'premium': self.calculator.calculate()['annual']['value'],
                }


class HDFCLifeClick2ProtectPlusExtraLifeCalculator(HDFCLifeClick2ProtectPlusLife):
    class Config(HDFCLifeClick2ProtectPlusLife.Config):
        @property
        def buy_url(self):
            return 'http://ops.hdfclife.com/ops/click2protectPlus.do'\
                '?source=A_Coverfox_C2P&mobilenumber=%(mobile)s'\
                '&email=%(email)s&sumassured=%(sum_assured)d&smoker=%(tobacco_user)s&gender=%(gender)s'\
                '&typeofcover=E&premiumterm=%(term)d&premiummode=R&billingfrequency=01&premiumpayterm=%(term)d&'\
                'premiumamount=%(premium)d&monthPayoutType=0&agentcode=00617746' % {
                    'mobile': self.calculator.cleaned_data.mobile,
                    'email': self.calculator.cleaned_data.email,
                    'sum_assured': self.calculator.cleaned_data.sum_assured,
                    'tobacco_user': 'Y' if self.calculator.data.tobacco_user else 'N',
                    'gender': 'M' if self.calculator.data.gender == 1 else 'F',
                    'term': self.calculator.cleaned_data.term,
                    'premium': self.calculator.calculate()['annual']['value'],
                }
