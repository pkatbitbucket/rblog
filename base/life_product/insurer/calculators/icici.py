from life_product.models import Frame

from .base import BaseCalculator, _float, LAKH, CRORE, FREQUENCY


class ICICIPrudentialiProtectSmartLifeCalculator(BaseCalculator):
    """ICICI Prudential iProtect smart life"""

    def __formula__(self):
        return """
            formula:
            base_premium = premium_rate
            total_premium = base_premium + (base_premium/100 * ST)

            rules:
            premium can not be less than 3600
            maturity age is max 75
            female age  =  male - 2
            for SA < 50Lakh, tobacco consumption doesn't matter
        """

    minimum_premium = 3600
    maximum_maturity_age = 75
    maximum_entry_age = 60

    @_float
    def premium_rate(self):
        """
        Calc for 54 lakhs  - (premium for 50) + (Premium for 60 - Premium for 50)* ((54-50)/(60-50))
        """
        sa = float(self.cleaned_data.sum_assured / LAKH)

        if sa < 3:
            new_data = self.cleaned_data.__dict__
            new_data['sum_assured'] = 3*LAKH
            new_calculator = ICICIPrudentialiProtectSmartLifeCalculator(data=new_data, plan=self.plan)
            new_premium_rate = new_calculator.premium_rate()
            return (sa * new_premium_rate / 3.0)

        elif 3 <= sa <= 500:

            premium_rate_object = self._premium_rate_object()
            if self.cleaned_data.sum_assured - premium_rate_object.frame.sum_assured.lower:
                lower_data = self.cleaned_data.__dict__
                lower_data['sum_assured'] = premium_rate_object.frame.sum_assured.lower
                upper_data = self.cleaned_data.__dict__
                upper_data['sum_assured'] = premium_rate_object.frame.sum_assured.upper

                lower_calculator = ICICIPrudentialiProtectSmartLifeCalculator(data=lower_data, plan=self.plan)
                upper_calculator = ICICIPrudentialiProtectSmartLifeCalculator(data=upper_data, plan=self.plan)

                lower_premium_rate = lower_calculator.premium_rate()
                upper_premium_rate = upper_calculator.premium_rate()

                return lower_premium_rate + (
                    (upper_premium_rate - lower_premium_rate) *
                    (self.cleaned_data.sum_assured - premium_rate_object.frame.sum_assured.lower) /
                    (premium_rate_object.frame.sum_assured.upper - premium_rate_object.frame.sum_assured.lower)
                )
            else:
                return premium_rate_object.value

        else:
            new_data = self.cleaned_data.__dict__
            new_data['sum_assured'] = 5*CRORE
            new_calculator = ICICIPrudentialiProtectSmartLifeCalculator(data=new_data, plan=self.plan)
            new_premium_rate = new_calculator.premium_rate()
            return (sa/100 * new_premium_rate/5)

    @_float
    def base_premium(self):
        """base premiums are given to us by icici instead of premium rates"""
        return self.premium_rate()

    @_float
    def discount(self):
        return self.base_premium() * 0.01  # 1% discount

    def clean(self, cleaned_data):
        if cleaned_data['gender'] == Frame.FEMALE:
            cleaned_data['age'] = max(cleaned_data['age'] - 2, 18)
        cleaned_data['gender'] = None

        if cleaned_data['sum_assured'] < 50*LAKH:
            cleaned_data['tobacco_user'] = None

    class Config(BaseCalculator.Config):
        buy_url = 'http://www.iciciprulife.com/public/term-plans/'\
            'iprotect-smart-term-insurance-calculator.htm?UID=1527'
        medical_required_condition_smoker = 'Upto 50 lakhs sum assured'
        medical_required_condition_non_smoker = medical_required_condition_smoker
        about_us = [
            (
                "ICICI Prudential Life Insurance Company is a joint venture between ICICI Bank,"
                " a premier financial powerhouse,and Prudential plc, a leading international financial"
                " services group."
            ),
            (
                "ICICI Prudential began its operations in December 2000 after receiving"
                " approval from Insurance Regulatory Development Authority of India (IRDAI). ICICI"
                " Prudential Life Insurance has maintained its focus on offering a wide range of"
                " flexible products that meet the needs of the Indian customer at every step in life."
            ),
        ]
        frequency = [FREQUENCY.Y, FREQUENCY.H, FREQUENCY.Q, FREQUENCY.M, ]
