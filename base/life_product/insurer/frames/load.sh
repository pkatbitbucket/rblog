#!/bin/bash
../../../manage.py import_life_frames ./edelweiss --insurer-slug='edelweiss-tokio'
../../../manage.py import_life_frames ./hdfc --insurer-slug='hdfc-life'
../../../manage.py import_life_frames ./icici --insurer-slug='icici-prudential'
../../../manage.py import_life_frames ./aegon --insurer-slug='aegon-life'
../../../manage.py import_life_frames ./future --insurer-slug='future-generali'
../../../manage.py import_life_frames ./birla --insurer-slug='birla-sun-life'
