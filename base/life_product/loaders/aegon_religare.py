import datetime

_TODAY = datetime.datetime.now().date()


class AegonReligareConfig(object):
    start_from = 0
    input_cell_names = {
        'dob': 'D15',
        'gender': 'sex',
        'premium_payment_term': 'D17',
        'policy_term': 'K13',
        'ici_rider_switch': 'i19',
        'ici_rider': 'SA_iCI',
    }

    record_cell_names = {
        'total': 'K31',
    }
    error_cells = 'B48:B52'

    def gender_choices(self):
        return ['M', 'F']

    def dob_choices(self):
        def _subtract_years(d, years):
            try:
                return d.replace(year=d.year - years)
            except ValueError:
                return d - (datetime.date(d.year - years, 1, 1) - datetime.date(d.year, 1, 1))

        return [_subtract_years(_TODAY, int(period)) for period in range(18, 66)]

    def premium_payment_term_choices(self):
        return ['Regular Pay', 'Single Pay']

    def policy_term_choices(self):
        return range(5, (75-18)+1)

    def ici_rider_switch_choices(self):
        return ['yes', 'no']

    def ici_rider_choices(self):
        return range(1000000, 5000000, 1000)

    def combinations(self):
        attrs = self.input_cell_names.keys()
        return self.get_possible_dicts([
            (attr, getattr(self, attr + '_choices')()) for attr in attrs
        ])

    def start(self):
        import xlwings as xw
        path = '/Users/arpit/Desktop/calc.xls'
        xw.Workbook(path)
        xw.Sheet('input').activate()
        xw.Range('D10').value = _TODAY

        error = xw.Range(self.error_cells)
        error_counter = success_counter = 0
        record_cells = {}
        for key, cell_name in self.record_cell_names.items():
            record_cells[key] = xw.Range(cell_name)
        # combinations = list(self.combinations())
        combinations = self.combinations()
        # len_combinations = len(combinations)
        for counter, combination in enumerate(combinations):
            if self.start_from and counter < self.start_from:
                continue
            for cell, value in combination.items():
                xw.Range(cell[1]).value = value
            if filter(bool, error.value):
                print error.value
                print combination
                print ""
                error_counter += 1
                continue
            else:
                success_counter += 1
                print map(lambda d: {d[0]: d[1].value}, record_cells.items())
                print combination
                print ""
            print "\n\n counter: ", counter+1, "\n\n"
        print 'success', success_counter, ', error', error_counter

    def get_possible_dicts(self, list_of_tuples):
        list_of_dicts = map(lambda t: {'key': (t[0], self.input_cell_names[t[0]], ), 'values': t[1]}, list_of_tuples)
        counters_max = [len(d['values']) for d in list_of_dicts]
        print "possible combinations", counters_max
        counters = [0] * len(counters_max)

        # print counters
        max = reduce(lambda x, y: x*y, [len(v['values']) for v in list_of_dicts], 1)
        for k in range(0, max):
            params = {}
            for i, data in enumerate(list_of_dicts):
                params[data['key']] = data['values'][counters[i]]

            exit = False
            for m, c in enumerate(counters):
                if c < counters_max[m] - 1:
                    counters[m] += 1
                    counters[0:m] = [0]*m
                    break
                if m == len(counters):
                    exit = True
            if exit:
                break
            yield params
