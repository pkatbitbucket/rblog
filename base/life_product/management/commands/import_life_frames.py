from django.core.management.base import BaseCommand

from csvkit.convert import convert
from psycopg2.extras import NumericRange
from ...models import Frame, Insurer, Plan, PremiumRate

import csv
import os
import re
import time


def get_all_files(path):
    all_files = []
    for root, directories, filenames in os.walk(path):
        for filename in filenames:
            all_files.append(os.path.join(root, filename))
    return all_files


class Command(BaseCommand):
    help = 'Uploads age/term matrices to Frame table'

    def add_arguments(self, parser):
        parser.add_argument('path', nargs='+', type=str)
        parser.add_argument('--insurer-slug', dest='insurer_slug', nargs='+', type=str, required=True)
        parser.add_argument('--plan-name', dest='plan_name', nargs='+', type=str, required=False)

    def handle(self, *args, **options):
        exceptions = []
        LAKH = 100000
        paths = options['path']
        insurer_slug = options['insurer_slug'][0]
        default_plan_name = (options.get('plan_name') or [None])[0]
        insurer = Insurer.objects.get(slug=insurer_slug)

        csv_files = []
        for path in paths:
            if os.path.isdir(path):
                csv_files.extend(get_all_files(path))
            else:
                csv_files.append(path)

        for csv_file in csv_files:
            filename, file_extension = os.path.splitext(csv_file)
            if not file_extension == '.csv':
                output = filename+'.csv'
                try:
                    convert(csv_file, file_extension.split('.')[-1], output=open(output, 'w'))
                except ValueError:
                    print csv_file, "Not supported"
                    os.remove(output)
                    continue
                except Exception as e:
                    print e
                    exceptions.append((e, csv, ))
                    continue
                csv_file = output

            content = csv_file.split('/')[-1].split('.')[0].split('__')

            plan_name = default_plan_name or content[-1].split('(')[1].split(')')[0]
            plan, _ = Plan.objects.get_or_create(name=plan_name, insurer=insurer)

            sum_assured_range = None
            upper_sum_assured = None
            bounds = '[)'  # lower inclusive by default, upper exclusive by default

            sum_assured_range = (
                re.search('^\((\d+)-(\d+)-(.{2})\)$', content[-2]) or
                re.search('^\((\d+)-(\d+)\)$', content[-2]) or
                re.search('^\((\d+)-\)$', content[-2])
            )
            if sum_assured_range.lastindex == 3:  # (50-100-[])
                bounds = sum_assured_range.group(3)
                upper_sum_assured = sum_assured_range.group(2)
                lower_sum_assured = sum_assured_range.group(1)
            elif sum_assured_range.lastindex == 2:  # (100-500)
                upper_sum_assured = sum_assured_range.group(2)
                lower_sum_assured = sum_assured_range.group(1)
            elif sum_assured_range.lastindex == 1:  # (500-)
                lower_sum_assured = sum_assured_range.group(1)

            sum_assured = NumericRange(int(lower_sum_assured)*LAKH, int(upper_sum_assured or 0)*LAKH or None, bounds=bounds)

            gender = tobacco_user = None
            tag_names = filter(lambda x: not(x.startswith('(') and x.endswith(')')), content)
            for tag_name in tag_names:
                tag_name = tag_name.lower()
                if tag_name == 'Male'.lower():
                    gender = Frame.MALE
                elif tag_name == 'Female'.lower():
                    gender = Frame.FEMALE
                elif tag_name == 'Male_Female'.lower():
                    gender = None
                elif tag_name == 'Tobacco_User'.lower():
                    tobacco_user = True
                elif tag_name == 'Non_Tobacco_User'.lower():
                    tobacco_user = False
                elif tag_name == 'Tobacco_Non_Tobacco_User'.lower():
                    tobacco_user = None
                else:
                    e = "No tag matched '%s'" % tag_name
                    exceptions.append((e, csv, ))
                    continue
            frame, _ = Frame.objects.get_or_create(sum_assured=sum_assured, plan=plan, gender=gender, tobacco_user=tobacco_user)

            with open(csv_file, 'rt') as f:
                reader = csv.DictReader(f)
                for row in reader:
                    for key in row.keys():
                        value = row.pop(key)
                        if value:
                            row[key.strip()] = value
                        else:
                            continue
                    if not row:
                        continue
                    else:
                        age = int(row.pop('age', row.pop('Age')))
                        for term, value in sorted(row.items(), key=lambda i: 100 if 'Upto' in i[0] else int(i[0])):
                            value = float(value)
                            if value:
                                if 'Upto age' in term or 'Upto Age' in term:
                                    row.pop(term)
                                    term = term.replace('Age', 'age')
                                    max_maturity_age = int(term.split('Upto age ')[1])
                                    max_allowed_term = max(map(int, row.keys()))
                                    term = "%s-%s" % (max_allowed_term + 1, max_maturity_age - age, )
                                terms = map(int, term.split('-'))
                                terms = terms + [terms[-1]+1]
                                terms = range(terms[0], terms[-1])
                                kwargs = {
                                    'age': age,
                                    'term': terms,
                                    'frame': frame
                                }
                                try:
                                    if PremiumRate.objects.filter(**kwargs).exists():
                                        premium_rate_object = PremiumRate.objects.get(**kwargs)
                                        if premium_rate_object.value == value:
                                            print premium_rate_object, value, "SKIPPED"
                                        else:
                                            premium_rate_object.value = value
                                            premium_rate_object.save()
                                            print premium_rate_object, value, "UPDATED"
                                            time.sleep(5)  # Seeking attention
                                    else:
                                        kwargs.update({
                                            'value': value,
                                        })
                                        for _term in terms:
                                            assert not PremiumRate.objects.filter(
                                                age=age, term__contains=[_term], frame=frame
                                            ).exists(), (
                                                "Terms calculated wrongly, value for age: %d and term: %d already present",
                                                (age, _term),
                                            )
                                        print PremiumRate.objects.create(**kwargs), value
                                except:
                                    raise
                                    # Something went wrong
                            else:
                                continue

            if not file_extension == '.csv':
                os.remove(csv_file)

        print exceptions
        # print '\n\n'.join([e[0].message + '. ' + e[1] for e in exceptions])
