# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_pgjson.fields
import core.models
import customdb.thumbs
import picklefield.fields
import autoslug.fields
import core.fields
import life_product.models
import django.contrib.postgres.fields
import django.core.validators
import django.contrib.postgres.fields.ranges


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0044_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='Feature',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=100)),
                ('slug', autoslug.fields.AutoSlugField(editable=False, populate_from=b'name', always_update=True, unique=True)),
            ],
            options={
                'alternate_key': 'slug',
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='Frame',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('sum_assured', django.contrib.postgres.fields.ranges.BigIntegerRangeField()),
                ('gender', models.PositiveSmallIntegerField(blank=True, null=True, choices=[(1, b'Male'), (2, b'Female')])),
                ('tobacco_user', models.NullBooleanField(default=False)),
            ],
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='Insurer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(unique=True, max_length=200)),
                ('slug', autoslug.fields.AutoSlugField(populate_from=b'name', unique=True, editable=False)),
                ('logo', customdb.thumbs.ImageWithThumbsField(null=True, upload_to=life_product.models.get_insurer_logo_path, blank=True)),
                ('claims_settled', core.fields.SmallIntegerRangeField(default=90, validators=[django.core.validators.MaxValueValidator(100), django.core.validators.MinValueValidator(0)])),
                ('active', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='Plan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=100)),
                ('calculator', picklefield.fields.PickledObjectField(default=None, null=True, editable=False, blank=True)),
                ('exclusions', models.TextField(blank=True)),
                ('active', models.BooleanField(default=True)),
            ],
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='PlanFeature',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(default=b'free', max_length=10, choices=[(b'free', b'Free'), (b'paid', b'Paid')])),
                ('feature', models.ForeignKey(to='life_product.Feature')),
                ('plan', models.ForeignKey(to='life_product.Plan')),
            ],
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='PremiumRate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('age', core.fields.SmallIntegerRangeField(validators=[django.core.validators.MaxValueValidator(65), django.core.validators.MinValueValidator(18)])),
                ('term', django.contrib.postgres.fields.ArrayField(base_field=core.fields.SmallIntegerRangeField(validators=[django.core.validators.MaxValueValidator(65), django.core.validators.MinValueValidator(10)]), size=None)),
                ('value', models.FloatField()),
                ('frame', models.ForeignKey(related_name='premium_rates', to='life_product.Frame')),
            ],
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='RedirectRecord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('data', django_pgjson.fields.JsonField()),
                ('activity', models.ForeignKey(blank=True, to='core.Activity', null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='PolicyWordingDocument',
            fields=[
            ],
            options={
                'abstract': False,
                'proxy': True,
            },
            bases=('core.document',),
        ),
        migrations.AddField(
            model_name='plan',
            name='features',
            field=models.ManyToManyField(to='life_product.Feature', through='life_product.PlanFeature', blank=True),
        ),
        migrations.AddField(
            model_name='plan',
            name='insurer',
            field=models.ForeignKey(related_name='plans', to='life_product.Insurer'),
        ),
        migrations.AddField(
            model_name='insurer',
            name='policy_wording',
            field=models.ForeignKey(blank=True, to='life_product.PolicyWordingDocument', null=True),
        ),
        migrations.AddField(
            model_name='frame',
            name='plan',
            field=models.ForeignKey(related_name='frames', to='life_product.Plan'),
        ),
        migrations.AlterUniqueTogether(
            name='premiumrate',
            unique_together=set([('age', 'term', 'frame')]),
        ),
        migrations.AlterIndexTogether(
            name='premiumrate',
            index_together=set([('age', 'term', 'frame')]),
        ),
        migrations.AlterUniqueTogether(
            name='planfeature',
            unique_together=set([('plan', 'feature')]),
        ),
        migrations.AlterUniqueTogether(
            name='plan',
            unique_together=set([('name', 'insurer')]),
        ),
        migrations.AlterUniqueTogether(
            name='frame',
            unique_together=set([('plan', 'sum_assured', 'gender', 'tobacco_user')]),
        ),
        migrations.AlterIndexTogether(
            name='frame',
            index_together=set([('plan', 'sum_assured', 'gender', 'tobacco_user')]),
        ),
    ]
