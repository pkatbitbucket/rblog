# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('life_product', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='insurer',
            name='claims_settled',
            field=models.FloatField(default=90),
        ),
    ]
