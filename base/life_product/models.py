from django.contrib.postgres.fields import ArrayField, BigIntegerRangeField
from django.db import models

from autoslug import AutoSlugField
from core.fields import SmallIntegerRangeField
from core.models import BaseModel, AbstractBaseModel, Activity, Document
from customdb.thumbs import ImageWithThumbsField
from django_pgjson.fields import JsonField
from picklefield.fields import PickledObjectField

from .utils import intword

import os


models.options.DEFAULT_NAMES = models.options.DEFAULT_NAMES + ('alternate_key',)


def get_insurer_logo_path(self, filename):
    return os.path.join(
        "uploads", self._meta.app_label, self._meta.model_name, self.slug, 'logo' + os.path.splitext(filename)[1]
    )


class PolicyWordingDocumentManager(models.Manager):
    def get_queryset(self):
        return super(PolicyWordingDocumentManager, self).get_queryset().filter(app='term')


class PolicyWordingDocument(Document):
    objects = PolicyWordingDocumentManager()

    def save(self, *args, **kwargs):
        self.app = self.app or 'term'
        return super(PolicyWordingDocument, self).save(*args, **kwargs)

    class Meta(Document.Meta):
        proxy = True


class Feature(BaseModel):
    name = models.CharField(max_length=100)
    slug = AutoSlugField(populate_from='name', always_update=True, unique=True)

    def natural_key(self):
        return (self.slug, )

    def __unicode__(self):
        return self.name

    class Meta:
        alternate_key = 'slug'


class InsurerManager(models.Manager):

    def get_queryset(self):
        return super(InsurerManager, self).get_queryset().exclude(active=False)


class Insurer(BaseModel):
    name = models.CharField(max_length=200, unique=True)
    slug = AutoSlugField(populate_from='name', always_update=False, unique=True, max_length=100)
    logo = ImageWithThumbsField(upload_to=get_insurer_logo_path, sizes=(('s', 300, 300), ('t', 500, 500)), null=True, blank=True)
    claims_settled = models.FloatField(default=90)
    policy_wording = models.ForeignKey(PolicyWordingDocument, null=True, blank=True)
    active = models.BooleanField(default=True)

    all_objects = models.Manager()
    objects = InsurerManager()

    def natural_key(self):
        return (self.slug, )


class PlanManager(models.Manager):

    def get_queryset(self):
        return super(PlanManager, self).get_queryset().filter(active=True, insurer__active=True)


class Plan(BaseModel):
    name = models.CharField(max_length=100)
    insurer = models.ForeignKey(Insurer, related_name='plans')
    calculator = PickledObjectField(null=True, blank=True, default=None, editable=True)
    features = models.ManyToManyField(Feature, blank=True, through='PlanFeature')
    exclusions = models.TextField(blank=True)
    active = models.BooleanField(default=True)

    all_objects = models.Manager()
    objects = PlanManager()

    def natural_key(self):
        return (self.name, self.insurer, )

    class Meta:
        unique_together = ("name", "insurer", )

    def __unicode__(self):
        return "%s - %s" % (self.insurer.__unicode__(), super(Plan, self).__unicode__())


class PlanFeature(AbstractBaseModel, models.Model):
    FREE = 'free'
    PAID = 'paid'
    TYPE_CHOICES = (
        (FREE, 'Free'),
        (PAID, 'Paid'),
    )
    plan = models.ForeignKey(Plan)
    feature = models.ForeignKey(Feature)
    type = models.CharField(max_length=10, default=FREE, choices=TYPE_CHOICES)

    class Meta:
        unique_together = ("plan", "feature", )

    def __unicode__(self):
        return "%s - %s - %s" % (self.plan.__unicode__(), self.feature.__unicode__(), self.type)


class Frame(BaseModel):
    MALE = 1
    FEMALE = 2
    GENDER_CHOICES = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    )

    plan = models.ForeignKey(Plan, related_name='frames')
    sum_assured = BigIntegerRangeField()
    gender = models.PositiveSmallIntegerField(choices=GENDER_CHOICES, null=True, blank=True)
    tobacco_user = models.NullBooleanField(default=False, null=True, blank=True)

    def get_premium_rate(self, age, term):
        return self.premium_rates.get(age=age, term__contained_by=[term], term__contains=[term])

    class Meta:
        unique_together = index_together = ('plan', 'sum_assured', 'gender', 'tobacco_user', )

    def __unicode__(self):
        return "%s - %s, %s - from %s upto %s" % (
            self.plan.__unicode__(),
            dict(self.GENDER_CHOICES).get(self.gender, 'Either Male or Female'),
            {
                False: 'Non Tobacco', True: 'Tobacco', None: 'Either Tobacco or Non Tobacco',
            }[self.tobacco_user] + ' User',
            intword(self.sum_assured.lower) or '0',
            intword(self.sum_assured.upper) or 'no-limit',
        )


class PremiumRate(BaseModel):
    age = SmallIntegerRangeField(min_value=18, max_value=65)
    term = ArrayField(base_field=SmallIntegerRangeField(min_value=10, max_value=65))
    value = models.FloatField()
    frame = models.ForeignKey(Frame, related_name='premium_rates')

    def __unicode__(self):
        return "%s - %s age - %s(years)" % (self.frame.__unicode__(), self.age, self.term)

    class Meta:
        unique_together = index_together = ('age', 'term', 'frame', )


class RedirectRecord(BaseModel):
    """Temporary model to store details of number of insurer redirects"""
    data = JsonField()
    activity = models.ForeignKey(Activity, null=True, blank=True)

    def __unicode__(self):
        return "%s - %s - Rs %s" % (
            self.data['plan']['insurer']['name'],
            self.data['plan']['name'],
            self.data['plan']['premium']['annual']['label'],
        )
