from django.conf.urls import patterns, include, url

from . import views


uuid_regex = '(%(hex)s{8}-%(hex)s{4}-%(hex)s{4}-%(hex)s{4}-%(hex)s{12})|(%(hex)s{32})' % {'hex': '[a-f0-9]'}

urlpatterns = patterns('life_product.views',
    url(r"^form/$", views.QuoteFormView.as_view(), name='form'),
    url(r"^(?P<activity_id>%s)/$" % (uuid_regex, ), views.QuoteFormView.as_view(), name='form'),
    url(r"^quotes/$", views.PlanListView.as_view(), name='quotes'),
    url(r"^quotes/(?P<activity_id>%s)/$" % (uuid_regex, ), views.PlanListView.as_view(), name='quotes'),
    url(r"^create-redirect-record/(?P<activity_id>%s)/$" % (uuid_regex, ), views.CreateRedirectRecordView.as_view(), name='create_redirect_record'),
)
