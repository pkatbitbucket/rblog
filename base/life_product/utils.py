from django.template import defaultfilters
from django.utils.translation import ungettext


intword_converters = (
    (5, lambda number: (
        ungettext('%(value)s lakh', '%(value)s lakh', number),
    )),
    (7, lambda number: (
        ungettext('%(value)s crore', '%(value)s crore', number),
    )),
)


def intword(value):
    """
    Converts a large integer to a friendly text representation.
    Inspired from django.contrib.humanize.templatetags.humanize.intword
    """
    try:
        value = int(value)
    except (TypeError, ValueError):
        return value

    if value < 100000:
        return value

    def _check_for_i18n(value, string_formatted):
        value = ('%f' % value).rstrip('0').rstrip('.')
        return string_formatted % {'value': value}

    for exponent, converters in intword_converters:
        large_number = 10 ** exponent
        if value < large_number * 100:
            new_value = value / float(large_number)
            return _check_for_i18n(new_value, *converters(new_value)) + defaultfilters.pluralize(new_value)
    return value
