from __future__ import absolute_import

from django import http
from django.conf import settings
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.utils.html import normalize_newlines, strip_tags
from django.views.generic import ListView, View
from django.views.generic.edit import FormView

from braces import views as braces_views
from copy import deepcopy
from coverfox.serializers import DjangoQuerysetJSONEncoder
from core.exceptions import NewTrackerException
from core.models import Activity
from home.views import index as home_index_view

from life_product.insurer.calculators.base import OutOfScope
from .forms import QuoteForm
from .models import Plan, Feature, RedirectRecord, PolicyWordingDocument
from . import utils as life_utils

import putils
import utils


class BaseAbstractView(braces_views.JsonRequestResponseMixin):
    activity_model = Activity
    activity_session_name = 'term_activity'
    json_encoder_class = DjangoQuerysetJSONEncoder
    default_serialized_response = {
        'errors': {},
        'data': {},
        'redirect_url': '',
    }

    def get_request_json(self):
        request_json = super(BaseAbstractView, self).get_request_json()
        if request_json:
            query_dict = getattr(self.request, self.request.method.upper())
            query_dict._mutable = True
            for key, value in request_json.items():
                if isinstance(value, list):
                    query_dict.setlist(key, value)
                else:
                    query_dict.update({key: value})
            query_dict._mutable = False
        return request_json

    @property
    def serialized_response(self):
        if not getattr(self, '_serialized_response', None):
            self._serialized_response = deepcopy(self.default_serialized_response)
        return self._serialized_response

    def get_response(self, view, method, **kwargs):
        _kwargs = deepcopy(self.kwargs)
        _kwargs.update(kwargs)
        original_method = self.request.method
        self.request.method = method.upper()
        response = view(request=self.request, **_kwargs)
        self.request.method = original_method
        return response

    def get_response_with_get(self, view, **kwargs):
        return self.get_response(view, 'GET', **kwargs)

    def get_response_with_post(self, view, **kwargs):
        return self.get_response(view, 'POST', **kwargs)

    def _cru_activity(self, activity_id=None, data=None):
        activity_id = activity_id or self.kwargs.get('activity_id')
        data_present = data is not None
        activity_id_present = activity_id is not None
        if data_present and not activity_id_present:
            # create activity
            return self.activity_model.objects.create(
                tracker=self.request.TRACKER, extra=data, activity_type='term_create_quote_activity'
            )
        elif data_present and activity_id_present:
            # set activity
            activity = self.get_activity(activity_id=activity_id)
            activity.extra = data
            activity.save()
            self.request.session.update({self.activity_session_name: activity})
            self.request.session.modified = True
            return activity
        elif not data_present and activity_id_present:
            # get activity with activity_id
            session_activity = self.request.session.get(self.activity_session_name)
            if session_activity and activity_id == str(session_activity.activity_id):
                return session_activity
            else:
                activity = get_object_or_404(self.activity_model, activity_id=activity_id)
                self.request.session.update({self.activity_session_name: activity})
                self.request.session.modified = True
                return activity
        elif not data_present and not activity_id_present:
            # get activity from session
            return self.request.session.get(self.activity_session_name)
        else:
            return None

    def get_activity(self, activity_id=None):
        return self._cru_activity(activity_id=activity_id)

    def set_activity(self, activity_id, data):
        return self._cru_activity(activity_id=activity_id, data=data)

    def create_activity(self, data):
        return self._cru_activity(data=data)

    def get(self, request, *args, **kwargs):
        superb = super(BaseAbstractView, self).get(request, *args, **kwargs)
        if self.request_json:  # Because request.is_ajax() doesn't work
            return self.render_json_response(superb.context_data)
        else:
            return superb

    def push_results_data_to_lms(self, event, activity=None, email='', mobile=''):
        request = self.request
        if activity:
            data = activity.extra
            data.update({
                'activity_id': activity.activity_id,
                'quote_url':  request.build_absolute_uri(reverse('term:quotes', args=(activity.activity_id, ))),
                'tracker_id': activity.tracker.session_key,
            })
            email = email or activity.extra.get('email')
            mobile = mobile or activity.extra.get('mobile')
        else:
            data = {}
        return utils.LMSUtils.send_lms_event(
            event=event,
            product_type='term',
            tracker=request.TRACKER,
            data=data,
            email=email or request.COOKIES.get('email'),
            mobile=mobile or request.COOKIES.get('mobileNo'),
            internal=request.IS_INTERNAL,
            request_ip=putils.get_request_ip(request)
        )


class RequireActivityMixin(object):
    def dispatch(self, *args, **kwargs):
        self.activity = self.get_activity()
        return super(RequireActivityMixin, self).dispatch(*args, **kwargs)


class RequireFormMixin(RequireActivityMixin):
    def get(self, request, *args, **kwargs):
        self.form = QuoteForm(data=self.activity.extra)
        if self.form.is_valid():
            return super(RequireFormMixin, self).get(request, *args, **kwargs)
        else:
            return http.HttpResponseRedirect(reverse('term:form'))


class QuoteFormView(BaseAbstractView, FormView):
    form_class = QuoteForm

    def get_context_data(self, **kwargs):
        context = super(QuoteFormView, self).get_context_data(**kwargs)
        return context

    def get(self, request, *args, **kwargs):
        self.push_results_data_to_lms(activity=None, event='quote_form_home_view')
        return home_index_view(request, product='term')

    def form_valid(self, form):
        data = form.data.dict()
        if 'activity_id' in self.kwargs:
            self.set_activity(activity_id=self.kwargs['activity_id'], data=data)
            return self.get_response_with_get(PlanListView.as_view())
        else:
            activity = self.create_activity(data=data)
            self.serialized_response['redirect_url'] = reverse('term:quotes', args=(activity.activity_id, ))
            return self.render_json_response(self.serialized_response)

    def form_invalid(self, form):
        self.serialized_response['errors'] = form.errors
        return self.render_json_response(self.serialized_response, status=400)


class PlanListView(RequireFormMixin, BaseAbstractView, ListView):
    model = Plan
    queryset = model.objects.filter(calculator__isnull=False)
    context_object_name = 'plan_list'
    template_name = 'life_product/plan_list.html'
    feature_alternate_key = Feature._meta.alternate_key  # 'which is 'slug' for Feature model'

    # Only these fields should be exposed in the response
    serialized_fields = (
        'name',
        'exclusions',
        'insurer__name',
        'insurer__claims_settled',
        'insurer__slug',
        'insurer__logo',
        'insurer__policy_wording__document',
    )

    # These fields shouldn't be exposed in the response
    serialized_fields += (
        'calculator',
        'pk',
    )

    def get(self, request, *args, **kwargs):
        self.push_results_data_to_lms(activity=self.activity, event='results_page_view')
        return super(PlanListView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PlanListView, self).get_context_data(**kwargs)
        self.serialized_response['data'][self.context_object_name] = context[self.context_object_name]
        self.serialized_response['data']['quote_form'] = self.form.cleaned_data
        self.serialized_response['data']['quote_form'].update(self.activity.extra)
        self.serialized_response['data']['features'] = {}
        self.serialized_response['activity_id'] = self.activity.activity_id
        for feature in Feature.objects.values('name', self.feature_alternate_key):
            self.serialized_response['data']['features'][feature.pop('slug')] = utils.to_nested_dict(feature)
        return self.serialized_response

    def get_queryset(self):
        plans_values = super(PlanListView, self).get_queryset().values(*self.serialized_fields)
        plan_list = []
        policy_wording_doc_storage = PolicyWordingDocument._meta.get_field('document').storage
        for plan in plans_values:
            plan = utils.to_nested_dict(plan)
            pk = plan.pop('pk')
            try:
                calculator = plan.pop('calculator')(data=self.form.cleaned_data, plan=pk)
            except OutOfScope as e:
                utils.term_logger.info([plan['insurer']['name'], e.message])
            except Exception as e:
                utils.term_logger.error([plan['insurer']['name'], e])
            else:
                plan['sum_assured'] = {
                    'label': life_utils.intword(calculator.cleaned_data.sum_assured),
                    'value': calculator.cleaned_data.sum_assured,
                }
                plan['term'] = calculator.cleaned_data.term
                plan['buy_url'] = calculator.config.buy_url
                if calculator.data.tobacco_user:
                    plan['medical_required_condition'] = calculator.config.medical_required_condition_smoker
                else:
                    plan['medical_required_condition'] = calculator.config.medical_required_condition_non_smoker
                plan['insurer']['policy_wording'] = (
                    policy_wording_doc_storage.url(plan['insurer']['policy_wording']['document'])
                ) if plan['insurer']['policy_wording']['document'] else None
                plan['insurer']['logo'] = policy_wording_doc_storage.url(plan['insurer']['logo'])
                plan['insurer']['about_us'] = calculator.config.about_us
                plan['frequency'] = calculator.config.frequency
                plan['exclusions'] = normalize_newlines(strip_tags(plan['exclusions'])).split('\n')
                plan['features'] = Feature.objects.filter(plan=pk).values_list(
                    self.feature_alternate_key, 'planfeature__type',
                )
                try:
                    plan['premium'] = calculator.calculate()
                except OutOfScope as e:
                    utils.term_logger.info([plan['insurer']['name'], e.message])
                except Exception as e:
                    utils.term_logger.error([plan['insurer']['name'], e])
                else:
                    plan_list.append(plan)
        return sorted(plan_list, key=lambda p: p['premium']['annual']['value'])


class CreateRedirectRecordView(RequireActivityMixin, BaseAbstractView, View):
    def post(self, request, *args, **kwargs):
        data = {
            'plan': self.request_json,
            'quote_form': self.activity.extra,
        }
        RedirectRecord.objects.create(data=data, activity=self.activity)
        return self.render_json_response({})
