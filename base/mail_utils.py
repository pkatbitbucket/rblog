from django.conf import settings
import thread
from django.core.mail import EmailMultiAlternatives
import cf_cns
from core.models import *
from blog.models import *
from django.template import Context, Template
import mandrill
from chimp_utils import *

from_mail = "info@coverfox.com"


from collections import OrderedDict, defaultdict
from django.core.mail import send_mail
from mailer import direct as direct_mail


def send_mail_template(subscriber, mail_dict):
    try:
        mandrill_client = mandrill.Mandrill(settings.MANDRILL_API_KEY)
        template_content = [
            {
                'content': mail_dict['html'],
                'name':'mail-body',
            },
            {
                'content': mail_dict['title'],
                'name':'mail-title',
            },
        ]
        message = {
            'auto_html': None,
            'auto_text': None,
            'bcc_address': None,
            'from_email': 'info@coverfox.com',
            'from_name': 'Team Coverfox',
            'headers': {'Reply-To': 'info@coverfox.com'},
            'text': mail_dict['text'],
            'important': False,
            'inline_css': None,
            'merge': True,
            'merge_vars': [{'rcpt': subscriber.email,
                            'vars': [{'content': 'merge2 content', 'name': 'merge2'}]}],
            'metadata': {'website': 'www.coverfox.com'},
            'preserve_recipients': None,
            'recipient_metadata': [{'rcpt': subscriber.email,
                                    'values': {'user_id': subscriber.id}}],
            'return_path_domain': None,
            'signing_domain': None,
            'subject': mail_dict['title'],
            'tags': ['review-signup'],
            'to': [{'email': subscriber.email,
                    'name': subscriber.name,
                    'type': 'to'}],
            'track_clicks': None,
            'track_opens': None,
            'tracking_domain': None,
            'url_strip_qs': None,
            'view_content_link': None
        }
        result = mandrill_client.messages.send_template(
            template_name='base', template_content=template_content, message=message, async=False, ip_pool='Main Pool')

    except mandrill.Error, e:
        # Mandrill errors are thrown as exceptions
        print 'A mandrill error occurred: %s - %s' % (e.__class__, e)
        mail_title = "Error occured while sending mail"
        mail_text = 'A mandrill error occurred: %s - %s' % (e.__class__, e)
        msg = EmailMultiAlternatives(mail_title, mail_text, from_mail, [
                                     'aniket@coverfox.com', 'dev@coverfox.com'])
        msg.attach_alternative(mail_html, "text/html")
        thread.start_new_thread(msg.send, ())
    return


def send_message(mtype, to, **kwargs):
    mail = Mail.objects.get(mtype=mtype, is_active=True)

    kwargs['settings'] = settings
    sf = kwargs['subscriber']
    ctx = Context(kwargs)
    mail_dict = {}
    mail_dict['title'] = Template(mail.title).render(ctx)
    mail_dict['text'] = Template(mail.text).render(ctx)
    mail_dict['html'] = Template(mail.body).render(ctx)

    thread.start_new_thread(send_mail_template, (sf, mail_dict,))
    return


def transaction_send_message(mtype, **kwargs):
    mail = Mail.objects.get(mtype=mtype, is_active=True)

    kwargs['settings'] = settings
    transaction = kwargs['transaction']
    ctx = Context(kwargs)
    mail_dict = {}
    mail_dict['title'] = Template(mail.title).render(ctx)
    mail_dict['text'] = Template(mail.text).render(ctx)
    mail_dict['html'] = Template(mail.body).render(ctx)

    thread.start_new_thread(transaction_mail_template,
                            (transaction, mail_dict,))
    return


def transaction_mail_template(transaction, mail_dict):
    try:
        mandrill_client = mandrill.Mandrill(settings.MANDRILL_API_KEY)
        template_content = [
            {
                'content': mail_dict['html'],
                'name':'mail-body',
            },
            {
                'content': mail_dict['title'],
                'name':'mail-title',
            },
        ]
        message = {
            'auto_html': None,
            'auto_text': None,
            'bcc_address': None,
            'from_email': 'info@coverfox.com',
            'from_name': 'Rupy',
            'headers': {'Reply-To': 'info@coverfox.com'},
            'text': mail_dict['text'],
            'important': False,
            'inline_css': None,
            'merge': True,
            'merge_vars': [{'rcpt': transaction.proposer_email,
                            'vars': [{'content': 'merge2 content', 'name': 'merge2'}]}],
            'metadata': {'website': 'www.coverfox.com'},
            'preserve_recipients': None,
            'recipient_metadata': [{'rcpt': transaction.proposer_email,
                                    'values': {'user_id': transaction.transaction_id}}],
            'return_path_domain': None,
            'signing_domain': None,
            'subject': mail_dict['title'],
            'tags': ['review-signup'],
            'to': [{'email': transaction.proposer_email,
                    'name': transaction.proposer_first_name,
                    'type': 'to'}],
            'track_clicks': None,
            'track_opens': None,
            'tracking_domain': None,
            'url_strip_qs': None,
            'view_content_link': None
        }
        result = mandrill_client.messages.send_template(
            template_name='base', template_content=template_content, message=message, async=False, ip_pool='Main Pool')

    except mandrill.Error, e:
        # Mandrill errors are thrown as exceptions
        print 'A mandrill error occurred: %s - %s' % (e.__class__, e)
        mail_title = "Error occured while sending mail"
        mail_text = 'A mandrill error occurred: %s - %s' % (e.__class__, e)
        msg = EmailMultiAlternatives(mail_title, mail_text, from_mail, [
                                     'aniket@coverfox.com', 'dev@coverfox.com'])
        msg.attach_alternative(mail_html, "text/html")
        # thread.start_new_thread(msg.send,())
    return


def health_alert(subject, body, mail_list=[]):
    subject = 'Health Product Alerts: %s' % subject
    #mail_list = settings.HEALTH_TEAM + mail_list  # moved to Notifications.
    #send_mail(subject, body, None, mail_list)
    cf_cns.notify('HEALTH_ALERTS', to=mail_list, subject_content=subject, html_content=body)


def health_transaction_alert(transaction, alert_type, request=None):
    mail_list = ['support@coverfox.com']
    msg_list = [
        ('Server', settings.SITE_URL),
    ]
    if transaction:
        slab = transaction.slab
        sum_assured = 'Rs.%s' % slab.sum_assured
        if slab.deductible:
            sum_assured += ' (Deductible: Rs.%s)' % slab.deductible
        msg_list.extend([
            ('Name', '%s %s' % (transaction.proposer_first_name, transaction.proposer_last_name)),
            ('Email', transaction.proposer_email),
            ('Mobile', transaction.proposer_mobile),
            ('Plan', str(slab.health_product)),
            ('Sum Assured', sum_assured),
            ('Premium', transaction.premium_paid or transaction.extra['plan_data']['premium']),
            ('Transaction ID', transaction.transaction_id),
        ])
        if transaction.status == 'COMPLETED':
            msg_list.extend([
                ('Link to Transaction',
                 '%s/health-plan/view/%s/' % (settings.SITE_URL, transaction.transaction_id)),
                ('Link to Download Transaction Data',
                 '%s/health-plan/transaction/%s/download/' % (settings.SITE_URL, transaction.transaction_id)),
            ])
            if transaction.policy_document:
                msg_list.extend([
                    ('Policy document', transaction.policy_document.url),
                ])
            mail_list.append('ops@coverfox.com')
        else:
            msg_list.extend([
                ('Error message', transaction.extra.get('error_msg', '')),
                ('Link to Transaction', '%s/health-plan/buy/%s/' % (settings.SITE_URL, transaction.transaction_id)),
            ])

        msg_list.extend([
            ('Created On', datetime.datetime.strftime(transaction.created_on, '%d-%m-%Y')),
            ('Updated On', datetime.datetime.strftime(transaction.updated_on, '%d-%m-%Y'))
        ])

    if request:
        user = request.user.email if request.user.is_authenticated() else ''
        msg_list.extend([
            ('Referer', request.META.get('HTTP_REFERER', '')),
            ('session_key', request.TRACKER.session_key),
            ('Logged-in user', user),
        ])

    msg_text = '\n'.join(['%s: %s' % (k, v) for (k, v) in msg_list])
    health_alert(subject=alert_type, body=msg_text, mail_list=mail_list)


# def send_message(mtype, to, **kwargs):
#    mail = Mail.objects.get(mtype=mtype, is_active=True)
#
#    kwargs['settings'] = settings
#    ctx = Context(kwargs)
#    mail_title = Template(mail.title).render(ctx)
#    mail_text = Template(mail.text).render(ctx)
#    mail_html = Template(mail.body).render(ctx)
#
#    msg = EmailMultiAlternatives(mail_title, mail_text, from_mail, to)
#    msg.attach_alternative(mail_html, "text/html")
#    thread.start_new_thread(msg.send,())

# ALL OPTIONS TO SEND MAIL USING MANDRILL TEMPLATE
# try:
#    mandrill_client = mandrill.Mandrill('YOUR_API_KEY')
#    template_content = [{'content': 'example content', 'name': 'example name'}]
#    message = {'attachments': [{'content': 'ZXhhbXBsZSBmaWxl',
#                      'name': 'myfile.txt',
#                      'type': 'text/plain'}],
#     'auto_html': None,
#     'auto_text': None,
#     'bcc_address': 'message.bcc_address@example.com',
#     'from_email': 'message.from_email@example.com',
#     'from_name': 'Example Name',
#     'global_merge_vars': [{'content': 'merge1 content', 'name': 'merge1'}],
#     'google_analytics_campaign': 'message.from_email@example.com',
#     'google_analytics_domains': ['example.com'],
#     'headers': {'Reply-To': 'message.reply@example.com'},
#     'html': '<p>Example HTML content</p>',
#     'images': [{'content': 'ZXhhbXBsZSBmaWxl',
#                 'name': 'IMAGECID',
#                 'type': 'image/png'}],
#     'important': False,
#     'inline_css': None,
#     'merge': True,
#     'merge_vars': [{'rcpt': 'recipient.email@example.com',
#                     'vars': [{'content': 'merge2 content', 'name': 'merge2'}]}],
#     'metadata': {'website': 'www.example.com'},
#     'preserve_recipients': None,
#     'recipient_metadata': [{'rcpt': 'recipient.email@example.com',
#                             'values': {'user_id': 123456}}],
#     'return_path_domain': None,
#     'signing_domain': None,
#     'subaccount': 'customer-123',
#     'subject': 'example subject',
#     'tags': ['password-resets'],
#     'text': 'Example text content',
#     'to': [{'email': 'recipient.email@example.com',
#             'name': 'Recipient Name',
#             'type': 'to'}],
#     'track_clicks': None,
#     'track_opens': None,
#     'tracking_domain': None,
#     'url_strip_qs': None,
#     'view_content_link': None}
#    result = mandrill_client.messages.send_template(template_name='example template_name', template_content=template_content, message=message, async=False, ip_pool='Main Pool', send_at='example send_at')
#    '''
#    [{'_id': 'abc123abc123abc123abc123abc123',
#      'email': 'recipient.email@example.com',
#      'reject_reason': 'hard-bounce',
#      'status': 'sent'}]
#    '''
#
# except mandrill.Error, e:
#    # Mandrill errors are thrown as exceptions
#    print 'A mandrill error occurred: %s - %s' % (e.__class__, e)
#    # A mandrill error occurred: <class 'mandrill.UnknownSubaccountError'> - No subaccount exists with the id 'customer-123'
#    raise
