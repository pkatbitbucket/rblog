from django.conf import settings
import thread
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from core.models import *
from blog.models import *
from django.template import Context, Template
import mandrill
from chimp_utils import *

# DEPRECATED: Use cf_cns.notify() instead.
def send_message(mtype, **kwargs):
    mail = Mail.objects.get(mtype=mtype, is_active=True)

    kwargs['settings'] = settings
    # IMPORTANT: Always use 'obj' to send main data and render over template
    obj = kwargs['obj']
    if 'name' in obj.keys():
        obj['name'] = obj['name'].title()
    ctx = Context(kwargs)
    mail_dict = {
        'title': Template(mail.title).render(ctx),
        'text': Template(mail.text).render(ctx),
        'html': Template(mail.body).render(ctx)
    }

    msg = EmailMultiAlternatives(mail_dict['title'], mail_dict[
                                 'text'], 'no-reply@coverfox.com', (obj['to_email'],))
    msg.attach_alternative(mail_dict['html'], "text/html")
    msg.send()


def send_copies(obj):
    to = []
    ins = obj.get('insurer', '')
    if getattr(ins, 'title', '').lower().find('oriental') != -1:
        to.append({'email': 'soman@coverfox.com',
                   'name': 'Soman Soni', 'type': 'bcc'})
        to.append({'email': 'sameer.navgharkar@coverfox.com',
                   'name': 'Sameer Navgharkar', 'type': 'bcc'})
    return to


def mail_template(obj, mail_dict):
    """
    obj = {
        'user_id' : user_id,
        'to_email' : to_email,
        'from_name' : from_name, #Optional
        'from_email' : from_email, #Optional
        'template_name' : template_name, #Optional
        'name' : name
        'tag' : tag_for_mandill,
        ...
        ...
        <any other data to pass in template>
    }
    mail_dict = {
        'title' : title,
        'text' : text,
        'html' : html
    }
    """
    try:
        print "OBJECT >>>>>>>>>>>", obj
        # SETTING DEFAULTS
        if 'from_email' not in obj.keys():
            obj['from_email'] = "info@coverfox.com"
        if 'from_name' not in obj.keys():
            obj['from_name'] = 'Team Coverfox'
        if 'template_name' not in obj.keys():
            obj['template_name'] = 'base'
        if not 'tag' in obj.keys():
            obj['tag'] = 'default'
        if 'name' in obj.keys():
            obj['name'] = obj['name'].title()

        mandrill_client = mandrill.Mandrill(settings.MANDRILL_API_KEY)
        template_content = [
            {
                'content': mail_dict['html'],
                'name':'mail-body'
            },
            {
                'content': mail_dict['title'],
                'name':'mail-title'
            }
        ]
        to = [{'email': obj['to_email'], 'name': obj[
            'name'].title(), 'type': 'to'}] + send_copies(obj)
        message = {
            'auto_html': None,
            'auto_text': None,
            'bcc_address': None,
            'from_email': obj['from_email'],
            'from_name': obj['from_name'],
            'headers': {'Reply-To': obj['from_email']},
            'text': mail_dict['text'],
            'important': False,
            'inline_css': None,
            'merge': True,
            'merge_vars': [{'rcpt': obj['to_email'],
                            'vars': [{'content': 'merge2 content', 'name': 'merge2'}]}],
            'metadata': {'website': 'www.coverfox.com'},
            'preserve_recipients': None,
            'recipient_metadata': [{'rcpt': obj['to_email'],
                                    'values': {'user_id': obj['user_id']}}],
            'return_path_domain': None,
            'signing_domain': None,
            'subject': mail_dict['title'],
            'tags': [obj['tag']],
            'to': to,
            'track_clicks': None,
            'track_opens': None,
            'tracking_domain': None,
            'url_strip_qs': None,
            'view_content_link': None
        }

        result = mandrill_client.messages.send_template(
            template_name=obj['template_name'],
            template_content=template_content,
            message=message,
            async=False,
            ip_pool='Main Pool'
        )
        print 'MAIL SENT', result

    # except mandrill.Error, e:
    #     Mandrill errors are thrown as exceptions
    #     print 'A mandrill error occurred: %s - %s' % (e.__class__, e)
    except Exception, e:
        # Mandrill errors are thrown as exceptions
        print 'A mandrill error occurred: %s' % (str(e))
        mail_title = "Error occured while sending mail"
        mail_text = 'A mandrill error occurred: %s' % (str(e))
        ERROR_MAIL_LIST = ['aniket@coverfoxmail.com', 'dev@coverfoxmail.com']
        msg = EmailMultiAlternatives(
            mail_title, mail_text, 'dev@cfstatic.org', ERROR_MAIL_LIST)
        msg.attach_alternative(mail_html, "text/html")
        msg.send()
    return
