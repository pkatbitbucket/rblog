# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Communication',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(blank=True, max_length=50, choices=[(b'TRANSACTION_DROP', b'Transaction Drop'), (
                    b'TRANSACTION_FILLED_DROP', b'Transaction Filled Drop'), (b'RESULT_DROP', b'Result Drop')])),
                ('next_communication_time',
                 models.DateTimeField(null=True, blank=True)),
                ('last_communication_time',
                 models.DateTimeField(null=True, blank=True)),
                ('is_processed', models.BooleanField(default=False)),
                ('extra', jsonfield.fields.JSONField(
                    default={}, verbose_name='extra')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('tracker', models.ForeignKey(
                    blank=True, to='core.Tracker', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='MailerCode',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(blank=True, max_length=50, choices=[(b'TRANSACTION_DROP', b'Transaction Drop'), (
                    b'TRANSACTION_FILLED_DROP', b'Transaction Filled Drop'), (b'RESULT_DROP', b'Result Drop')])),
                ('is_active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Queue',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(blank=True, max_length=50, choices=[(b'TRANSACTION_DROP', b'Transaction Drop'), (
                    b'TRANSACTION_FILLED_DROP', b'Transaction Filled Drop'), (b'RESULT_DROP', b'Result Drop')])),
                ('content', jsonfield.fields.JSONField(default={}, blank=True)),
                ('is_active', models.BooleanField(default=True)),
                ('status', models.CharField(max_length=50, choices=[(b'PENDING', b'Pending'), (b'SENT', b'Sent'), (
                    b'FAILED', b'Failed'), (b'CANCELLED', b'Cancelled'), (b'SKIPPED', b'Skipped')])),
                ('extra', jsonfield.fields.JSONField(
                    default={}, verbose_name='extra')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('tracker', models.ForeignKey(
                    blank=True, to='core.Tracker', null=True)),
            ],
        ),
    ]
