from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models import permalink
from django.contrib.auth.models import UserManager, AbstractBaseUser, BaseUserManager
from customdb.uuidfield import UUIDField
from django.core import serializers
from jsonfield import JSONField
from django.utils import timezone
import json
import hashlib
import os
import random
import re
import datetime

from customdb.thumbs import ImageWithThumbsField
import utils
from core.models import Tracker, Activity

######### SOUTH HACKS ########
import django
if django.VERSION < (1, 7):
    try:
        from south.modelsinspector import add_introspection_rules
        add_introspection_rules([], ["^tagging\.fields\.TagField"])
        add_introspection_rules(
            [], ["^django_extensions\.db\.models\.AutoSlugField"])
        add_introspection_rules(
            [], ["^customdb\.thumbs\.ImageWithThumbsField"])
    except ImportError:
        pass
#############################

MAILER_CODE_CHOICES = (
    ('TRANSACTION_DROP', 'Transaction Drop'),
    ('TRANSACTION_FILLED_DROP', 'Transaction Filled Drop'),
    ('RESULT_DROP', 'Result Drop'),
)


class MailerCode(models.Model):
    code = models.CharField(max_length=50, blank=True,
                            choices=MAILER_CODE_CHOICES)
    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.code

MAIL_STATUS = (
    ('PENDING', 'Pending'),
    ('SENT', 'Sent'),
    ('FAILED', 'Failed'),
    ('CANCELLED', 'Cancelled'),
    ('SKIPPED', 'Skipped')
)


class Queue(models.Model):
    tracker = models.ForeignKey(Tracker, null=True, blank=True)
    code = models.CharField(max_length=50, blank=True,
                            choices=MAILER_CODE_CHOICES)
    content = JSONField(blank=True, default={})
    is_active = models.BooleanField(default=True)
    status = models.CharField(max_length=50, choices=MAIL_STATUS)
    extra = JSONField(_('extra'), default={})
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return '%s - %s' % (self.tracker.session_key, self.code)


class Communication(models.Model):
    tracker = models.ForeignKey(Tracker, null=True, blank=True)
    code = models.CharField(max_length=50, blank=True,
                            choices=MAILER_CODE_CHOICES)
    next_communication_time = models.DateTimeField(blank=True, null=True)
    last_communication_time = models.DateTimeField(blank=True, null=True)
    is_processed = models.BooleanField(default=False)
    extra = JSONField(_('extra'), default={})
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return '%s - %s' % (self.tracker.session_key, self.code)
