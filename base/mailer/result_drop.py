from core.models import *
from blog.models import *
from health_product.models import *
from dateutil.relativedelta import *

from mailer.models import Queue, Communication


def process_data_old():
    cutoff_time = datetime.datetime.now() - relativedelta(hours=1)
    for a in Activity.objects.filter(event="RESULTS_VISITED", is_processed=False, updated_on__lte=cutoff_time):
        if Queue.objects.filter(tracker=a.tracker, code='RESULT_DROP', is_active=False):
            a.is_processed = True
            a.save()
        elif a.tracker.user:
            obj = {
                'user_id': a.tracker.user.email,
                'to_email': a.tracker.user.email,
                'name': a.tracker.user.first_name,
                'tag': 'results-drop',
                'contact_url': '%s/health-plan/contact-me/' % settings.SITE_URL,
                'results_url': '%s/health-plan/#results/%s' % (settings.SITE_URL, a.activity_id)
            }
            if Queue.objects.filter(tracker=a.tracker, code='RESULT_DROP', is_active=True):
                mail = Queue.objects.get(
                    tracker=a.tracker, code='RESULT_DROP', is_active=True)

                # Check if results view converted to a Transaction
                if a.tracker.transaction_set.all():
                    print "#################### 2"
                    last_transaction = a.tracker.transaction_set.latest(
                        'updated_on')
                    if last_transaction.updated_on > a.updated_on:
                        mail.status = 'CANCELLED'
                        mail.is_active = False
                        mail.save()
                        a.is_processed = True
                        a.save()
                if not mail.status == 'CANCELLED':
                    print "#################### 3"
                    # If another result view occured since last mail scheduling (still within cutoff time),
                    # with mail still not sent
                    activity_updated_on = a.updated_on.replace(tzinfo=None)
                    if activity_updated_on > cutoff_time:
                        mail.status = 'SKIPPED'
                        mail.is_active = False
                        mail.save()
                    else:
                        print "#################### 4"
                        # If another result view occured since last mail scheduling after cutoff time,
                        # with mail still not sent - Reset content
                        mail.content = obj
                        mail.extra = a.extra
                        mail.save()
                        a.is_processed = True
                        a.save()
            else:
                print "#################### 5"
                # Schedule a fresh mailer
                mail = Queue(tracker=a.tracker, code='RESULT_DROP',
                             content=obj, status='PENDING')
                mail.extra = a.extra
                mail.save()
                a.is_processed = True
                a.save()
    return


def process_data():
    cutoff_time = datetime.datetime.now() - relativedelta(days=5)

    for activity_type in ['RESULT_DROP', 'TRANSACTION_DROP']:
        for activity in Activity.objects.filter(event=activity_type, is_processed=False, created__gte=cutoff_time):
            comm, created = Communication.objects.get_or_create(
                tracker=activity.tracker)
            if created:
                comm.code = activity_type
                comm.next_communication_time = datetime.datetime.now()
                comm.is_processed = False
            elif comm.last_communication_time < (datetime.datetime.now() - datetime.timedelta(days=1)):
                comm.code = activity_type
                comm.next_communication_time = datetime.datetime.now()
                comm.is_processed = False
            comm.save()
            activity.is_processed = True
            activity.save()

        # for comm in Communication.objects.filter(next_communication_time__gte=datetime.datetime.now(), is_processed=False):
        #     comm.is_processed = True
        #     comm.last_communication_time = datetime.datetime.now()
        #     comm.save()
        #     #SEND MAIL HERE

    return
