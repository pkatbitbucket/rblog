from django.conf import settings
import thread
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from core.models import *
from blog.models import *
from django.template import Context, Template
from health_product.models import *
from mailer.models import *
from dateutil.relativedelta import *


def process_data():
    cutoff_time = datetime.datetime.now() - relativedelta(hours=0.5)
    # cutoff_time = datetime.datetime.now()
    user_list = {}
    for t in Transaction.objects.filter(status__in=['PENDING', 'DRAFT'], mailer_flag=False, updated_on__lte=cutoff_time).order_by('updated_on'):
        if t.status == 'PENDING':
            if 'red_flag' not in t.extra.keys() or ('red_flag' in t.extra.keys() and not t.extra['red_flag']):
                user_list[t.proposer_email] = {
                    'email': t.proposer_email,
                    'first_name': t.proposer_first_name,
                    'tracker': t.tracker,
                    'q_code': 'TRANSACTION_FILLED_DROP'
                }
        else:
            # if t.tracker.user and (t.tracker.user.email not in
            # user_list.keys()) and t.tracker.user.is_verified:
            if t.tracker.user and (t.tracker.user.email not in user_list.keys()):
                user_list[t.proposer_email] = {
                    'email': t.tracker.user.email,
                    'first_name': t.tracker.user.first_name,
                    'tracker': t.tracker,
                    'q_code': 'TRANSACTION_DROP'
                }

    import pprint
    pprint.pprint(user_list)
    for email, user in user_list.items():
        tracker = user['tracker']
        last_transaction = tracker.transaction_set.latest('updated_on')
        save_last_transaction = False
        obj = {
            'user_id': user['email'],
            'to_email': user['email'],
            'name': user['first_name'],
            'tag': 'transaction-drop',
            'transaction_url': '%s/health-plan/buy/%s' % (settings.SITE_URL, last_transaction.transaction_id)
        }

        print ">>>>>>>>>>>>>>>>>>>> LAST TRANSACTION STATUS", last_transaction.status
        if user['q_code'] == 'TRANSACTION_FILLED_DROP':
            mail = Queue(tracker=tracker, code=user[
                         'q_code'], content=obj, status='PENDING')
            mail.extra = last_transaction.extra
            mail.save()
        else:
            if last_transaction.status == 'DRAFT':
                if tracker.queue_set.filter(code=user['q_code'], is_active=True):
                    # If another transaction occurred since last mail scheduling (still within cutoff time),
                    # with mail still not sent
                    mail = tracker.queue_set.get(
                        code=user['q_code'], is_active=True)
                    transaction_updated_on = last_transaction.updated_on.replace(
                        tzinfo=None)
                    if transaction_updated_on > cutoff_time:
                        mail.status = 'SKIPPED'
                        mail.is_active = False
                        mail.save()
                        save_last_transaction = True
                    else:
                        # If another transaction occurred since last mail scheduling after cutoff time,
                        # with mail still not sent - Reset content
                        mail.content = obj
                        mail.extra = last_transaction.extra
                        mail.save()
                else:
                    # Schedule a fresh mailer
                    mail = Queue(tracker=tracker, code=user[
                                 'q_code'], content=obj, status='PENDING')
                    mail.extra = last_transaction.extra
                    mail.save()

        for t in tracker.transaction_set.all():
            if save_last_transaction and t.id == last_transaction.id:
                print "Latest transaction >>", last_transaction.id
            else:
                t.mailer_flag = True
                t.save()

    return
