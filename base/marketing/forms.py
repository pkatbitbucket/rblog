from django import forms
from core.models import Marketing
from marketing.models import Affiliates


class MarketingForm(forms.ModelForm):

    class Meta:
        model = Marketing
        fields = ('campaign', 'source', 'medium', 'category', 'brand', 'content', 'paid', 'description',)


class AffiliatesForm(forms.ModelForm):
    affiliate_name = forms.CharField(required=True,
                                     help_text='Provided affiliate name will be used in lp url as affiliate_name')
    affiliate_url = forms.CharField(required=True,
                                    help_text='Enter url provided by affiliates or url mentioned in their iframe')

    class Meta:
        model = Affiliates
        fields = ('affiliate_name', 'affiliate_url', 'page_type',)
