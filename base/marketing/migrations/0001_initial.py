# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Affiliates',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('affiliate_name', models.CharField(unique=True, max_length=100)),
                ('affiliate_url', models.CharField(max_length=225)),
                ('page_type', models.CharField(max_length=100, choices=[(b'buy', b'BUY'), (b'buy-otp', b'BUY OTP')])),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
    ]
