# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def add_initial_data(apps, schema_editor):
    Affiliates = apps.get_model('marketing', 'Affiliates')
    affiliates = {
                "icubes": "https://icubes.go2cloud.org/SL3H1?adv_sub=",
                "v-commission": "https://tracking.vcommission.com/SL5Fx?adv_sub=",
                "icubes-otp": "https://icubes.go2cloud.org/SL3Gx?adv_sub=",
                "click-zoot-otp": "https://cztrk.com/p.ashx?o=1357&e=418&t=",
                "v-commission-otp": "https://tracking.vcommission.com/SL5Fz?adv_sub=",
                "komli-otp": "https://secure.komli.com/p.ashx?o=878&e=1087&t=",
                "ad2click-otp": "https://ad2click.go2cloud.org/SLyt?adv_sub=",
                "bigtrunk-otp": "https://bigtrunk.go2cloud.org/aff_l?offer_id=7&adv_sub=",
                "netcore-otp": "https://tracking.affiliatehub.co.in/SL1FM?adv_sub="
            }
    for affiliate in affiliates:
        if affiliate.endswith('-otp'):
            page_type = 'buy-otp'
        else:
            page_type = 'buy'
        Affiliates.objects.create(affiliate_name=affiliate, affiliate_url=affiliates[affiliate], page_type=page_type)


class Migration(migrations.Migration):

    dependencies = [
        ('marketing', '0001_initial'),
    ]

    operations = [migrations.RunPython(add_initial_data, migrations.RunPython.noop),
    ]
