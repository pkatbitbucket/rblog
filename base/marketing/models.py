from django.db import models

# Create your models here.
class Affiliates(models.Model):
    page_type_choices = (('buy','BUY'),('buy-otp','BUY OTP'),)

    affiliate_name = models.CharField(max_length=100, unique=True)
    affiliate_url = models.CharField(max_length=225)
    page_type = models.CharField(max_length=100, choices=page_type_choices)
    created_on = models.DateTimeField(auto_now_add=True, null=True)
    updated_on = models.DateTimeField(auto_now=True, null=True)

    def __unicode__(self):
        return u'{}: {}'.format(self.affiliate_name, self.affiliate_url)
