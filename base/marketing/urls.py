from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.dashboard, name='dashboard'),
    url(r'^(?P<id>[\d]+)/edit/$', views.edit, name='edit'),
    url(r'^add/$', views.edit, name='add'),
    url(r'^convert-url/$', views.get_new_url, name='new_url'),
    url(r'^affiliates/$', views.affiliates, name='affiliates'),
    url(r'^(?P<id>[\d]+)/edit-affiliate/$', views.edit_affiliate, name='edit-affiliate'),
    url(r'^add-affiliate/$', views.edit_affiliate, name='add-affiliate'),
    url(r'^(?P<id>[\d]+)/delete-affiliate/$', views.delete_affiliate, name='delete-affiliate'),
]
