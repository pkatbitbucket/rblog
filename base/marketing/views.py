from urlparse import urlparse, urlunparse, parse_qsl
from urllib import urlencode

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import permission_required
from django.http import JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.views.decorators.http import require_POST
from django.db.models import Q, Sum

from core.models import Marketing, Requirement
from marketing.forms import MarketingForm, AffiliatesForm
from marketing.models import Affiliates


# Create your views here.
@permission_required('core.change_marketing')
def dashboard(request):
    search = request.GET.get('search', '')
    if search == '':
        object_list = Marketing.objects.all()
    else:
        object_list = Marketing.objects.filter(Q(id__icontains=search) |
                                               Q(campaign__icontains=search) | Q(source__icontains=search) |
                                               Q(medium__icontains=search) | Q(category__icontains=search) |
                                               Q(brand__icontains=search) | Q(content__icontains=search))

    paginator = Paginator(object_list, 10)

    page = request.GET.get('page')
    try:
        objects = paginator.page(page)
    except PageNotAnInteger:
        objects = paginator.page(1)
    except EmptyPage:
        objects = paginator.page(paginator.num_pages)

    for obj in objects:
        req = Requirement.objects.filter(mid=obj.id, current_state__name='POLICYPENDING_paymentdone', policy__isnull=False)
        policy_count = req.count()
        premium = req.aggregate(Sum("policy__premium"))['policy__premium__sum']
        obj.premium = premium
        obj.policy_count = policy_count

    return render(request, 'marketing:dashboard.html', {'objects': objects})


@permission_required('core.change_marketing', 'core.add_marketing')
def edit(request, id=0):
    url = reverse('marketing:add')
    if id == 0 and request.method == 'POST':
        form = MarketingForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return redirect(reverse('marketing:dashboard'))
    else:
        try:
            obj = Marketing.objects.get(pk=id)

        except Marketing.DoesNotExist:
            form = MarketingForm()
        else:
            if request.method == 'POST':
                form = MarketingForm(request.POST, instance=obj)
                if form.is_valid():
                    form.save(commit=True)
                    return redirect(reverse('marketing:dashboard'))
            elif request.method == 'GET':
                form = MarketingForm(instance=obj)
            else:
                form = MarketingForm()
            url = reverse('marketing:edit', kwargs={'id': id})

    for field in form.fields:
        if field != 'paid':
            form.fields[field].widget.attrs = {'class': 'form-control'}

    return render(request, 'marketing:marketing_form.html', {'form': form, 'url': url})


@require_POST
@permission_required('core.change_marketing')
def get_new_url(request):
    url = request.POST.get('url', '')
    if url == '':
        new_url = 'Please enter url'
    else:
        new_url = convert_url(url)
    return JsonResponse({'success': True, 'new_url': new_url})


def convert_url(url):
    parse_result = urlparse(url)
    query = parse_result.query
    if query:
        params = dict(parse_qsl(query))
        try:
            mid = Marketing.objects.get_mid(params)
        except Marketing.DoesNotExist:
            new_url = 'Data not found'
        else:
            parse_result = list(parse_result)
            new_params = {key: value for key, value in params.items()
                          if key not in Marketing.MID_MAPPING}
            new_params['mid'] = mid.id
            parse_result[4] = urlencode(new_params)
            new_url = urlunparse(parse_result)
    else:
        new_url = url
    return new_url


@permission_required('marketing.change_affiliates')
def affiliates(request):
    affiliates_buy = Affiliates.objects.filter(page_type='buy').order_by('id')
    affiliates_buy_otp = Affiliates.objects.filter(page_type='buy-otp').order_by('id')

    return render(request, 'marketing:affiliates_dashboard.html', {'affiliates_buy': affiliates_buy,
                  'affiliates_buy_otp': affiliates_buy_otp})


@permission_required('marketing.change_affiliates', 'marketing.add_marketing')
def edit_affiliate(request, id=0):
    url = reverse('marketing:add-affiliate')
    if id == 0 and request.method == 'POST':
        form = AffiliatesForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return redirect(reverse('marketing:affiliates'))
    else:
        try:
            obj = Affiliates.objects.get(pk=id)

        except Affiliates.DoesNotExist:
            form = AffiliatesForm()
        else:
            if request.method == 'POST':
                form = AffiliatesForm(request.POST, instance=obj)
                if form.is_valid():
                    form.save(commit=True)
                    return redirect(reverse('marketing:affiliates'))
            elif request.method == 'GET':
                form = AffiliatesForm(instance=obj)
            else:
                form = AffiliatesForm()
            url = reverse('marketing:edit-affiliate', kwargs={'id': id})

    for field in form.fields:
        form.fields[field].widget.attrs = {'class': 'form-control'}

    return render(request, 'marketing:affiliates_form.html', {'form': form, 'url': url})


@permission_required('marketing.delete_affiliates')
def delete_affiliate(request, id):
    Affiliates.objects.filter(pk=id).delete()
    return redirect(reverse('marketing:affiliates'))
