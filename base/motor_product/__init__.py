from django.apps.config import AppConfig

default_app_config = 'motor_product.MotorProductConfig'


class MotorProductConfig(AppConfig):
    name = 'motor_product'

    def ready(self):
        import signals  # NOQA

# Transaction
# TODO: Delete insured_vehicle, insured_car
# TODO: Rceipt field required?
# TODO: insurer cannot be nullable
# TODO: remove vehicle_type
# TODO: premium_proposed field (amount to payment gateway)

# Views
# TODO: refresh premium rate as soon as the proposal for request
# TODO: combine generate_form and generate_for_and_redirect
# TODO: payment response dump
# TODO: payment success grid messaging in Insurer model
