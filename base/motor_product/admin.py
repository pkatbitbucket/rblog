from django.contrib import admin, messages
from django.core.urlresolvers import reverse
from django import forms
from django.db.models import Q

from splitjson.widgets import SplitJSONWidget

from core.users import User
from core import models as core_models
from motor_product import models as motor_models

from .exceptions import NoInspectionNumber


class RTOExclusionInline(admin.TabularInline):
    model = motor_models.RTOExclusion


class ModelExclusionInline(admin.TabularInline):
    model = motor_models.ModelExclusion


class AddonExclusionAdmin(admin.ModelAdmin):
    inlines = [
        RTOExclusionInline,
        ModelExclusionInline,
    ]


class VehicleAdmin(admin.ModelAdmin):
    search_fields = ('make', 'model', 'variant',
                     'fuel_type', 'cc', 'insurer__title')
    list_display = ('make', 'model', 'variant', 'fuel_type', 'cc', 'insurer')


class VehicleMappingInline(admin.TabularInline):
    classes = ('grp-collapse grp-open',)
    model = motor_models.VehicleMapping
    extra = 0
    raw_id_fields = ('mapped_vehicle',)
    fields = ('mapped_vehicle',)


class VehicleMasterAdmin(admin.ModelAdmin):
    inlines = [VehicleMappingInline, ]
    list_display = ('__unicode__', 'make', 'model', 'variant',)
    search_fields = ('make__name', 'model__name', 'variant',)

    class Media:
        js = ('motor_product/admin/model_as_make.js',)


class InsurerAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'is_active_for_car', 'disable_cache_for_car',
                    'is_active_for_twowheeler', 'disable_cache_for_twowheeler')
    list_editable = ('is_active_for_car', 'disable_cache_for_car',
                     'is_active_for_twowheeler', 'disable_cache_for_twowheeler')


class RTOInsurerAdmin(admin.ModelAdmin):
    search_fields = ('rto_code', 'rto_name', 'rto_state',
                     'insurer__title', 'vehicle_type')
    list_display = ('rto_code', 'rto_name', 'rto_state',
                    'insurer', 'vehicle_type')


class RTOMappingInline(admin.TabularInline):
    classes = ('grp-collapse grp-open',)
    model = motor_models.RTOMapping
    extra = 0
    raw_id_fields = ('rto_insurer',)
    fields = ('rto_insurer',)


class RTOMasterAdmin(admin.ModelAdmin):
    inlines = [RTOMappingInline, ]
    list_display = ('__unicode__', 'rto_code', 'rto_name', 'rto_state',)
    search_fields = ('rto_code', 'rto_name', 'rto_state')


class TransactionAdmin(admin.ModelAdmin):
    search_fields = ('transaction_id', 'proposer__email', 'proposer__mobile', 'proposer__first_name', 'status', 'premium_paid')
    list_display = ('transaction_id', 'get_proposer_firstname', 'get_proposer_email', 'get_proposer_mobile', 'status',
                    'premium_paid', 'policy_document', 'created_on')
    fields = ('transaction_id', 'status', 'status_history', 'policy_token', 'created_on', 'payment_on', 'policy_document', 'vehicle_type',
              'policy_number', 'policy_start_date', 'policy_end_date', 'premium_paid', 'raw')
    readonly_fields = ('transaction_id', 'status', 'status_history', 'policy_token', 'created_on', 'payment_on', 'policy_document', 'vehicle_type', 'raw')

    def get_form(self, request, obj=None, **kwargs):
        form_class = super(TransactionAdmin, self).get_form(request, obj, **kwargs)
        attrs = {'class': 'special', 'size': '40'}
        if 'raw' in form_class.base_fields:
            form_class.base_fields['raw'].widget = SplitJSONWidget(attrs=attrs, debug=False)
        return form_class

    def get_proposer_firstname(self, obj):
        return obj.proposer.first_name if obj.proposer else ''

    def get_proposer_email(self, obj):
        return obj.proposer.email if obj.proposer else ''

    def get_proposer_mobile(self, obj):
        return obj.proposer.mobile if obj.proposer else ''

    get_proposer_firstname.short_description = 'First name'
    get_proposer_email.short_description = 'Email'
    get_proposer_mobile.short_description = 'Mobile'


class InspectionAdmin(admin.ModelAdmin):
    list_display = ('transaction_details', 'customer_details', 'vehicle_details',
                    'inspection_details', 'inspection_agent', 'proposal_form', 'status', 'number', )
    search_fields = ('status', 'number', 'address', )
    readonly_fields = ('raw', 'address', )
    raw_id_fields = ('transaction', 'requirement')

    def transaction_details(self, instance):
        return "%s %s " % (
            instance.transaction.insurer,
            instance.transaction.transaction_id
        )

    def customer_details(self, instance):
        return "%s (ph:%s)" % (
            instance.transaction.customer_name(),
            instance.transaction.customer_phone_number()
        )

    def vehicle_details(self, instance):
        return instance.transaction.vehicle_registration_number()

    def inspection_agent(self, instance):
        d = ""
        if instance.agent:
            d += instance.agent.user.get_full_name()
        if instance.inspection_agency:
            d += instance.inspection_agency.company.name
        return d

    def inspection_details(self, instance):
        if instance.scheduled_on and instance.schedule_slot:
            return "%s on %s-%s" % (
                instance.scheduled_on.strftime("%d/%m/%Y"),
                instance.schedule_slot.start.strftime("%I:%M %p"),
                instance.schedule_slot.end.strftime("%I:%M %p"),
            )
        else:
            return "-"

    def proposal_form(self, instance):
        return "<a href='%s' target='__blank'>%s</a>" % (
            reverse('desktopForm', args=('fourwheeler', instance.transaction.insurer.slug, instance.transaction.transaction_id,)),
            'Open'
        )
    proposal_form.allow_tags = True

    def save_model(self, request, obj, form, change):
        try:
            obj.save(user=request.user)
        except NoInspectionNumber as e:
            messages.success(request, e.message)
        return obj.transaction.proposal_form()


class DailyTimeSlotForm(forms.ModelForm):
    class Meta:
        model = motor_models.DailyTimeSlot
        exclude = []

    def clean_end(self):
        d = self.cleaned_data
        if d['start'] >= d['end']:
            raise forms.ValidationError("End time must be greater than start slot")
        return d['end']

    def clean(self):
        """
        Check if two slots are conflicting
        """
        d = self.cleaned_data

        clashing_slots = motor_models.DailyTimeSlot.objects.filter(
            Q(start__gt=d['start'], start__lte=d['end'], is_active=True) |
            Q(start__lt=d['start'], end__gt=d['end'], is_active=True)
        )
        if clashing_slots:
            clashing_slots_verbose = ", ".join([unicode(slot) for slot in clashing_slots])
            raise forms.ValidationError("Slot clashed with slots: %s" % clashing_slots_verbose)
        else:
            return d


class DailyTimeSlotAdmin(admin.ModelAdmin):
    form = DailyTimeSlotForm


class InspectionAgentForm(forms.ModelForm):
    user = forms.ModelChoiceField(queryset=User.objects.filter(groups__name='INSPECTION_AGENT'))
    weekly_off = forms.MultipleChoiceField(choices=motor_models.InspectionAgent.WEEKDAY_OPTIONS)
    is_active = forms.BooleanField(required=False)

    def __init__(self, *args, **kwargs):
        super(InspectionAgentForm, self).__init__(*args, **kwargs)
        if kwargs.get('instance'):
            self.fields['is_active'].initial = kwargs['instance'].user.is_active

    class Meta:
        model = motor_models.InspectionAgent
        exclude = []

    def clean_weekly_off(self):
        return [int(w) for w in self.cleaned_data.get('weekly_off')]

    def clean_is_active(self):
        d = self.cleaned_data
        if self.instance and self.instance.id:
            self.instance.user.is_active = d.get('is_active', False)
            self.instance.user.save()
        return d.get('is_active')


class InspectionAgentAdmin(admin.ModelAdmin):
    form = InspectionAgentForm
    list_display = ('user', 'weekly_off_verbose', 'is_active')
    search_fields = ('area', 'name')
    # list_editable = ('area',)

    def is_active(self, instance):
        return instance.user.is_active

    def weekly_off_verbose(self, instance):
        return ",".join([dict(motor_models.InspectionAgent.WEEKDAY_OPTIONS)[wo] for wo in instance.weekly_off])


class PendingInspectionAdmin(InspectionAdmin):
    fields = ('address', 'transaction', 'status', 'number', 'inspection_agency')
    readonly_fields = ('address', 'transaction')
    list_filter = ('status',)

    def get_queryset(self, request):
        qs = super(PendingInspectionAdmin, self).get_queryset(request)
        return qs.filter(schedule_slot__isnull=True, status__in=[
            motor_models.Inspection.PENDING,
            motor_models.Inspection.PAYMENT_APPROVED,
            motor_models.Inspection.DOCUMENT_APPROVED,
            motor_models.Inspection.POLICY_ISSUED,
            motor_models.Inspection.PAYMENT_REJECTED,
            motor_models.Inspection.PAYMENT_DONE
        ])


class RegistrationDetailAdmin(admin.ModelAdmin):
    search_fields = ('name', )


class MotorPremiumAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_vehicle', 'get_insurer', 'zone', 'max_idv', 'min_idv', 'premium', 'policy_type',
                    'new_ncb', 'min_age', 'max_age', 'updated_on')
    list_filter = ('policy_type', 'insurer', 'new_ncb')

    def get_vehicle(self, obj):
        vehicle = ''
        if obj.vehicle:
            vehicle = '%s %s %s %s' % (obj.vehicle.id, obj.vehicle.make.name, obj.vehicle.model.name,
                                       obj.vehicle.variant)
        return vehicle
    get_vehicle.short_description = 'Vehicle'

    def get_insurer(self, obj):
        insurer = ''
        if obj.insurer:
            insurer = obj.insurer.title
        return insurer
    get_insurer.short_description = 'Insurer'

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        actions = super(MotorPremiumAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions


class MotorModelAdmin(admin.ModelAdmin):
    ordering = ('name',)
    list_display = ("make", "name", "vehicle_type")
    list_filter = ('make', )


class MotorMakeAdmin(admin.ModelAdmin):
    ordering = ('name',)


admin.site.register(motor_models.MotorPremium, MotorPremiumAdmin)
admin.site.register(motor_models.BestPricing, admin.ModelAdmin)
admin.site.register(motor_models.DiscountCode, admin.ModelAdmin)
admin.site.register(core_models.Company, admin.ModelAdmin)
admin.site.register(motor_models.RTOMaster, RTOMasterAdmin)
admin.site.register(motor_models.RTOInsurer, RTOInsurerAdmin)
admin.site.register(motor_models.Addon, admin.ModelAdmin)
admin.site.register(motor_models.AddonExclusion, AddonExclusionAdmin)
admin.site.register(motor_models.VehicleMaster, VehicleMasterAdmin)
admin.site.register(motor_models.Vehicle, VehicleAdmin)
admin.site.register(motor_models.Make, MotorMakeAdmin)
admin.site.register(motor_models.Model, MotorModelAdmin)
admin.site.register(motor_models.Insurer, InsurerAdmin)
admin.site.register(motor_models.Garage)
admin.site.register(motor_models.Transaction, TransactionAdmin)
admin.site.register(motor_models.Inspection, InspectionAdmin)
admin.site.register(motor_models.PendingInspection, PendingInspectionAdmin)
admin.site.register(motor_models.RegistrationDetail)
admin.site.register(motor_models.InspectionAgent, InspectionAgentAdmin)
admin.site.register(motor_models.DailyTimeSlot, DailyTimeSlotAdmin)
admin.site.register(motor_models.Leave)
admin.site.register(motor_models.MotorInspectionDocument)
admin.site.register(motor_models.Dealership)
admin.site.register(motor_models.Dealer)
admin.site.register(motor_models.Quote)
admin.site.register(motor_models.InspectionAgency)
admin.site.register(motor_models.RequirementDealerMap)
admin.site.register(motor_models.Mswipe)
admin.site.register(motor_models.MotorPart)
admin.site.register(motor_models.InspectionReport)
admin.site.register(motor_models.InspectionArea)
admin.site.register(motor_models.UserVehicle)
admin.site.register(motor_models.PrivateCar)
admin.site.register(motor_models.DealerDealershipMap)
admin.site.register(motor_models.DealerInvoice)
admin.site.register(motor_models.InspectionStateChange)
admin.site.register(motor_models.RegistrationDetailStatistics)
admin.site.register(motor_models.CorporateEmployee)
