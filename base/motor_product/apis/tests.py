import hmac
import requests
from hashlib import sha1
import base64
import json

from datetime import datetime

BASE_URL = 'http://motordesktop.sandman.coverfox.com'
API_AUTHENTICATION_ID = 'vRJe46Qi1tGgOgwaG0wDukfglWJd9t2DsNJ8qyqI'
API_AUTHENTICATION_KEY = 'DokDRt1bD4tlqJdeOIBAGaDctGbDhCbCTyh63P84yhlbSMZs29uOkZ0JzppiiG4sNzjDDaIrejkc7rVavZ9JMTRWRxOFNpAt4CU5YD7dOGFwvx9GNhkx7APYTEqjdAtD'


def get_headers(path):
    request_time = datetime.utcnow()
    string_to_sign = "%s %s" % (
        path, request_time.strftime('%Y-%m-%dT%H:%M:%SZ'))
    h = hmac.HMAC(API_AUTHENTICATION_KEY, string_to_sign, sha1)
    signature = base64.b64encode(h.digest())
    headers = {
        'Authorization': 'CVFX ' + API_AUTHENTICATION_ID + ':' + signature,
        'Date': request_time.strftime('%Y-%m-%dT%H:%M:%SZ'),
    }
    return headers


def doPost(path, parameters=None):
    if parameters:
        response = requests.post(BASE_URL.rstrip(
            '/') + path, headers=get_headers(path), data=parameters)
    else:
        response = requests.post(BASE_URL.rstrip(
            '/') + path, headers=get_headers(path))

    if response.status_code == 200:
        print response.content
        jsonresponse = json.loads(response.content)
        if jsonresponse['success']:
            return True, jsonresponse
        else:
            return False, None
    else:
        return False, None


def doGet(path):
    response = requests.get(BASE_URL.rstrip(
        '/') + path, headers=get_headers(path))

    if response.status_code == 200:
        print response.content
        jsonresponse = json.loads(response.content)
        if jsonresponse['success']:
            return True, jsonresponse
        else:
            return False, None
    else:
        return False, None

EP_RTOS = '/apis/motor/%s/rtos/'
EP_MODEL_LIST = '/apis/motor/%s/vehicles/'
EP_VARIANT_LIST = '/apis/motor/%s/vehicles/%s/variants/'
EP_QUOTES = '/apis/motor/%s/quotes/'
EP_REFRESH_QUOTE = '/apis/motor/%s/quotes/%s/refresh/%s/'
EP_GENERATE_TRANSACTION = '/apis/motor/%s/confirm/%s/'
EP_SUBMIT_TRANSACTION = '/apis/motor/%s/buy/%s/%s/'


def get_rtos(vehicle_type):
    success, response = doPost(EP_RTOS % vehicle_type)
    if success:
        return response['data']['rtos']
    return None


def get_models(vehicle_type):
    success, response = doPost(EP_MODEL_LIST % vehicle_type)
    if success:
        return response['data']['models']
    return None


def get_variants(vehicle_type, model_id):
    success, response = doPost(EP_VARIANT_LIST % (vehicle_type, model_id))
    if success:
        return response['data']['variants']
    return None


def get_quote(vehicle_type, parameters):
    success, response = doPost(EP_QUOTES % vehicle_type, parameters)
    if success:
        return response['data']['quoteId']
    return None


def refresh_quote(vehicle_type, quote_id, insurer, parameters):
    success, response = doPost(EP_REFRESH_QUOTE % (
        vehicle_type, quote_id, insurer), parameters)
    if success:
        return response['data']['activityId']
    return None


def generate_form(vehicle_type, activity_id):
    success, response = doPost(EP_GENERATE_TRANSACTION %
                               (vehicle_type, activity_id))
    if success:
        return response['data']['transactionId']
    return None


def submit_form(vehicle_type, insurer, transaction_id, form_parameters):
    success, response = doPost(EP_SUBMIT_TRANSACTION % (
        vehicle_type, insurer, transaction_id), form_parameters)
    return success

quote_parameters = {
    "cngKitValue": "0",
    "extra_paPassenger": "0",
    "extra_isLegalLiability": "0",
    "extra_isAntiTheftFitted": "0",
    "extra_isMemberOfAutoAssociation": "0",
    "extra_isTPPDDiscount": "0",
    "extra_user_occupation": "0",
    "extra_user_dob": "",
    "isNewVehicle": "0",
    "idvNonElectrical": "0",
    "isClaimedLastYear": "0",
    "registrationNumber[]": ["MH", "01", "0", "0"],
    "manufacturingDate": "01-04-2012",
    "registrationDate": "01-04-2012",
    "idvElectrical": "0",
    "voluntaryDeductible": "0",
    "isCNGFitted": "0",
    "vehicleId": "1262",
    "idv": "0",
    "previousNCB": "25",
    "addon_is247RoadsideAssistance": "0",
    "addon_isInvoiceCover": "0",
    "addon_isDriveThroughProtected": "0",
    "addon_isDepreciationWaiver": "0",
    "addon_isEngineProtector": "0",
    "addon_isNcbProtection": "0",
    "addon_isKeyReplacement": "0",
    "newPolicyStartDate": "29-04-2015",
    "pastPolicyExpiryDate": "20-07-2015"
}

form_parameters = {
    "cust_first_name": "Nishant",
    "cust_last_name": "Shelar",
    "cust_gender": "Male",
    "cust_phone": "8888888888",
    "cust_dob": "01-01-1987",
    "cust_occupation": "",
    "cust_email": "nishant@coverfoxmail.com",
    "cust_pan": "",
    "cust_marital_status": "0",
    "nominee_name": "Birendra",
    "nominee_age": "53",
    "nominee_relationship": "Son",
    "vehicle_reg_no": "MH-01-DO-1017",
    "vehicle_engine_no": "ABCD1234",
    "vehicle_chassis_no": "AB123456",
    "past_policy_insurer": "1",
    "past_policy_number": "87892748312",
    "past_policy_insurer_city": "MH-01",
    "past_policy_cover_type": "1000007",
    "is_car_financed": "0",
    "car-financier": "1000",
    "add_house_no": "1204",
    "add_building_name": "Evershine Cosmic",
    "add_street_name": "Behram Baug",
    "add_landmark": "Infinity Mall",
    "add_pincode": "400067",
    "add_state": "55",
    "add_district": "",
    "add_city": "147",
    "add-same": "1"
}

vehicle_type = 'fourwheeler'
insurer = 'universal-sompo'

rtos = get_rtos(vehicle_type)

models = get_models(vehicle_type)
model_id = models[0]['id']
variants = get_variants(vehicle_type, model_id)

quote_id = get_quote(vehicle_type, quote_parameters)
activity_id = refresh_quote(vehicle_type, quote_id, insurer, quote_parameters)
transaction_id = generate_form(vehicle_type, activity_id)
success = submit_form(vehicle_type, insurer, transaction_id, form_parameters)

if success:
    payment_url = BASE_URL.rstrip(
        '/') + '/motor/%s/%s/submitForm/%s/' % (vehicle_type, insurer, transaction_id)
    print 'Payment url is:\n%s' % payment_url
else:
    print 'Some error occurred in request.'
