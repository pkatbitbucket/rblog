from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^vehicles/$', views.vehicles, name='api_make_model'),
    url(r'^vehicles/(?P<model_id>\d+)/variants/$', views.vehicles, name='api_variants'),
    url(r'^vehicle-info/$', views.vehicle_info, name='vehicle_info'),
    url(r'^rtos/$', views.rtos, name='api_rtos'),
    url(r'^quotes/$', views.quotes, name='api_quotes'),
    url(r'^quotes/(?P<quote_id>[\w-]+)/refresh/(?P<insurer_slug>[\w-]+)/$',
        views.refresh_quote, name='api_refresh_quote'),
    url(r'^confirm/(?P<activity_id>[\w-]+)/$', views.generate_form, name='api_generate_form'),
    url(r'^confirm/(?P<quote_id>[\w-]+)/(?P<insurer_slug>[\w-]+)/$',
        views.generate_form_simply, name='api_generate_form_simply'),
    url(r'^transaction/(?P<transaction_id>[\w-]+)/$', views.transaction_details, name='api_transaction_details'),
    url(r'^transaction/(?P<transaction_id>[\w-]+)/(?P<data_type>[\w-]+)/$',
        views.transaction_details, name='api_transaction_details'),
    url(r'^buy/(?P<insurer_slug>[\w-]+)/(?P<transaction_id>[\w-]+)/$', views.buy, name='api_buy'),
    url(r'^pincode-details/(?P<insurer_slug>[\w-]+)/$', views.pincode_details, name='api_pincode_details'),
    url(r'^cities/(?P<insurer_slug>[\w-]+)/$', views.cities, name='api_cities'),
    url(r'^insurer-data/(?P<insurer_slug>[\w-]+)/(?P<data_type>[\w-]+)/$',
        views.get_data_list, name='api_insurer_data'),
    url(r'^payment/(?P<transaction_id>[\w-]+)/$', views.payment, name='api_payment'),
    url(r'^quote/(?P<quote_id>[\w-]+)/$', views.QuoteDetailView.as_view(), name='api_quote_detail'),
    url(r'^active-insurers/$', views.active_insurers, name='active_insurers'),
    url(r'^vehicle-confirmation/$', views.vehicle_confirmation, name='vehicle_confirmation'),
]
