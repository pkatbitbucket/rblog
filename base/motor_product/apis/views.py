import json
import copy
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, Http404
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import never_cache
from django.views.generic.detail import DetailView
from datetime import timedelta, datetime
from motor_product import errors
from motor_product import views as pviews
from motor_product import prod_utils, parser_utils
from motor_product.insurers import insurer_utils
from motor_product.models import Transaction, Quote
from apis.decorators import authenticate_api, js_api, apilogger
from braces import views as braces_views
from oauth2_provider.decorators import protected_resource

MANADATORY_QUOTE_DATA_KEY = [
    'addon_isDepreciationWaiver',
    'addon_isAntiTheftFitted',
    'addon_is247RoadsideAssistance',
    'addon_isInvoiceCover',
    'addon_isDriveThroughProtected',
    'addon_isEngineProtector',
    'addon_isNcbProtection',
    'addon_isKeyReplacement',
    'isNewVehicle',
    'isCNGFitted',
    'extra_paPassenger',
    'extra_isLegalLiability',
    'cngKitValue',
    'idvNonElectrical',
    'registrationNumber[]',
    'manufacturingDate',
    'idvElectrical',
    'voluntaryDeductible',
    'vehicleId',
    'idv',
]

BOOLEAN_VALUE_QUOTE_DATA_KEY = [
    'addon_isDepreciationWaiver',
    'addon_isAntiTheftFitted',
    'addon_is247RoadsideAssistance',
    'addon_isInvoiceCover',
    'addon_isDriveThroughProtected',
    'addon_isEngineProtector',
    'addon_isNcbProtection',
    'addon_isKeyReplacement',
    'isNewVehicle',
    'isClaimedLastYear',
    'isCNGFitted',
    'extra_isLegalLiability',
]

BOOLEAN_VALUE = ['0', '1', 0, 1]

EXTRA_PA_PASSENGER_LIST = ['0', '10000', '50000', '100000', '200000']

VOLUNTARY_DEDUCTIBLE_FOUR_WHEELER_LIST = ['0', '2500', '5000', '7500', '15000']

VOLUNTARY_DEDUCTIBLE_TWO_WHEELER_LIST = [
    '0', '500', '750', '1000', '1500', '3000']

NCB_LIST = ['0', '20', '25', '35', '45', '50']


class APIProtectMixin(braces_views.CsrfExemptMixin):
    """
    Exempts the view from CSRF requirements.

    NOTE:
        This should be the left-most mixin of a view.
    """

    @method_decorator(authenticate_api)
    def dispatch(self, *args, **kwargs):
        return super(APIProtectMixin, self).dispatch(*args, **kwargs)


def quote_validation_and_clean(request_data, vehicle_type):
    OLD_VEHICLE_MANDATORY_DATA_KEY = [
        'isClaimedLastYear', 'registrationDate', 'previousNCB', 'pastPolicyExpiryDate']
    NEW_VEHICLE_MANDATORY_DATA_KEY = ['newPolicyStartDate']

    for key in MANADATORY_QUOTE_DATA_KEY:
        if key not in request_data.keys():
            return False, request_data  # Mandatory data missing

    for key in request_data.keys():
        if key in BOOLEAN_VALUE_QUOTE_DATA_KEY:
            if request_data[key] not in BOOLEAN_VALUE:
                return False, request_data  # Boolean value not satisfied

            request_data[key] = str(request_data[key])

    if request_data['isNewVehicle'] == '0':
        for key in OLD_VEHICLE_MANDATORY_DATA_KEY:
            if key not in request_data.keys():
                return False, request_data
    else:
        for key in NEW_VEHICLE_MANDATORY_DATA_KEY:
            if key not in request_data.keys():
                return False, request_data

    request_data['extra_paPassenger'] = str(
        request_data['extra_paPassenger']).strip()
    if request_data['extra_paPassenger'] not in EXTRA_PA_PASSENGER_LIST:
        return False, request_data

    request_data['cngKitValue'] = str(request_data['cngKitValue']).strip()
    if not request_data['cngKitValue'].isdigit():
        return False, request_data

    request_data['idvNonElectrical'] = str(
        request_data['idvNonElectrical']).strip()
    if not request_data['idvNonElectrical'].isdigit():
        return False, request_data

    request_data['idvElectrical'] = str(request_data['idvElectrical']).strip()
    if not request_data['idvElectrical'].isdigit():
        return False, request_data

    # Extra check to be added for total idv restriction

    voluntaryDeductibleList = VOLUNTARY_DEDUCTIBLE_FOUR_WHEELER_LIST
    if vehicle_type == 'twowheeler':
        voluntaryDeductibleList = VOLUNTARY_DEDUCTIBLE_TWO_WHEELER_LIST

    request_data['voluntaryDeductible'] = str(
        request_data['voluntaryDeductible']).strip()
    if request_data['voluntaryDeductible'] not in voluntaryDeductibleList:
        return False, request_data

    request_data['vehicleId'] = str(request_data['vehicleId']).strip()
    if not request_data['vehicleId'].isdigit():
        return False, request_data

    request_data['idv'] = str(request_data['idv']).strip()
    if not request_data['idv'].isdigit():
        return False, request_data

    # registrationNumber = request_data['registrationNumber[]']

    # Different date validations
    manufacture_date = datetime.strptime(
        request_data['manufacturingDate'], '%d-%m-%Y')
    now = datetime.now()

    if now <= manufacture_date:
        return False, request_data  # Manufacturing date should be before today

    if request_data['isNewVehicle'] == '1':
        policy_start_date = datetime.strptime(
            request_data['newPolicyStartDate'], '%d-%m-%Y')
        if (policy_start_date - now) > datetime.timedelta(days=45):
            return False, request_data  # Policy start date cannot exceed 45 days from today
        if (now - manufacture_date) > datetime.timedelta(days=180):
            return False, request_data  # New vehicle manufactured within 6 month is allowed
    else:
        request_data['previousNCB'] = str(request_data['previousNCB']).strip()
        if request_data['previousNCB'] not in NCB_LIST:
            return False, request_data

        registration_date = datetime.strptime(
            request_data['registrationDate'], '%d-%m-%Y')
        past_policy_expiry_date = datetime.strptime(
            request_data['pastPolicyExpiryDate'], '%d-%m-%Y')

        if (registration_date - manufacture_date) > datetime.timedelta(days=365):
            return False, request_data

        if manufacture_date >= registration_date:
            # Registartion date should be greater than manufacturing date
            return False, request_data

        if (now - registration_date) <= datetime.timedelta(days=180):
            return False, request_data  # Registartion date must be 180 days before today

        new_policy_start_date = past_policy_expiry_date + datetime.timedelta(days=1)
        if insurer_utils.get_age(new_policy_start_date, registration_date) > 15:
            return False, request_data  # 15 years of age allowed

        if past_policy_expiry_date >= now:
            # Policy not yet expired
            if (past_policy_expiry_date - now) > datetime.timedelta(days=45):
                return False, request_data  # Policy expiry date cannot exceed 45 days from today
        else:
            if (now - past_policy_expiry_date) > datetime.timedelta(days=730):
                # Policy expiry date cannot be 2 years back if policy already
                # expired
                return False, request_data
    return True, request_data


@csrf_exempt
@apilogger
@js_api
@authenticate_api
def vehicles(request, vehicle_type, model_id=None):
    return pviews.vehicles(request, vehicle_type, model_id)


@csrf_exempt
@apilogger
@js_api
@authenticate_api
def rtos(request, vehicle_type):
    return pviews.get_master_rtos(request, vehicle_type)


@csrf_exempt
@apilogger
@authenticate_api
def quotes(request, vehicle_type):
    # Parameter validations yet to be added.
    return pviews.quotes(request, vehicle_type)


@csrf_exempt
@apilogger
@authenticate_api
def refresh_quote(request, vehicle_type, quote_id, insurer_slug):
    # Parameter validations yet to be added.
    return pviews.refresh_quote(request, vehicle_type, quote_id, insurer_slug)


@csrf_exempt
@apilogger
@protected_resource()
def generate_form(request, vehicle_type, activity_id):
    return Http404


# The following view generates the proposal form
# TODO: Access of the following API(Should the user be authenticated to make a call?)
@csrf_exempt
@apilogger
@authenticate_api
def generate_form_simply(request, vehicle_type, quote_id, insurer_slug):
    return pviews.generate_form_simply(request, vehicle_type, quote_id, insurer_slug)


@csrf_exempt
@apilogger
@authenticate_api
def cities(request, vehicle_type, insurer_slug):
    return pviews.get_cities(request, vehicle_type, insurer_slug)


@csrf_exempt
@apilogger
@authenticate_api
def pincode_details(request, vehicle_type, insurer_slug):
    return pviews.pincode_details(request, vehicle_type, insurer_slug)


@never_cache
@apilogger
@authenticate_api
def get_data_list(request, vehicle_type, insurer_slug, data_type):
    data_list = prod_utils.get_data_list(vehicle_type, insurer_slug, data_type)
    if not data_list:
        return HttpResponse(json.dumps({'success': False, 'errorCode': errors.EC_INVALID_REQUEST, 'errorMessage': errors.messages[errors.EC_INVALID_REQUEST], 'errorItems': [], 'data': {}}))
    return HttpResponse(json.dumps({'success': True, 'errorCode': None, 'errorMessage': None, 'errorItems': [], 'data': {'items': data_list}}))


@csrf_exempt
@apilogger
@protected_resource()
def buy(request, vehicle_type, insurer_slug, transaction_id):
    # Parameter validations for proposal yet to be added.
    return pviews.submit_buy_form(request, vehicle_type, insurer_slug, transaction_id)


@never_cache
@apilogger
def payment(request, vehicle_type, transaction_id):
    if request.method == 'GET':
        transaction = get_object_or_404(
            Transaction, transaction_id=transaction_id)
        return pviews.submit_buy_form(request, vehicle_type, transaction.insurer.slug, transaction_id)
    raise Http404


@csrf_exempt
@apilogger
@protected_resource()
def transaction_details(request, vehicle_type, transaction_id, data_type=None):
    try:
        transaction = Transaction.objects.get(transaction_id=transaction_id)
    except ObjectDoesNotExist:
        return HttpResponse(json.dumps({'success': False, 'errorCode': errors.EC_INVALID_PARAMETERS, 'errorMessage': errors.messages[errors.EC_INVALID_PARAMETERS], 'errorItems': [{'parameter': 'transaction_id', 'errorMessage': 'Invalid transaction id.'}], 'data': {}}))

    if request.method == 'POST':
        if data_type is not None:
            raise Http404

        post_qdict = copy.deepcopy(request.POST)
        form_details = parser_utils.qdict_to_dict(post_qdict)

        if not form_details and request.body:
            form_details = json.loads(request.body)

        retval = prod_utils.save_form_partial_details(form_details,  transaction_id)
        if retval:
            return HttpResponse(json.dumps({'success': True, 'errorCode': None, 'errorMessage': None, 'errorItems': [], 'data': {}}))
        else:
            return HttpResponse(json.dumps({'success': False, 'errorCode': errors.ES_CANNOT_SAVE_DATA, 'errorMessage': errors.messages[errors.ES_CANNOT_SAVE_DATA], 'errorItems': [], 'data': {}}))

    if request.method == 'GET':
        buy_form_details = prod_utils.get_buy_form_details(transaction_id)
        if data_type == 'quote':
            transaction_data = {
                'success': True,
                'errorCode': None, 'errorMessage': None, 'errorItems': [],
                'data': {
                    'quote_parameters': buy_form_details['quote_parameters'],
                    'quote_response': buy_form_details['quote_response'],
                }
            }
        elif data_type == 'form':
            transaction_data = {
                'success': True,
                'errorCode': None, 'errorMessage': None, 'errorItems': [],
                'data': buy_form_details['user_form_details']
            }
        elif data_type == 'status':
            transaction_data = {
                'success': True,
                'errorCode': None, 'errorMessage': None, 'errorItems': [],
                'data': {
                    'insurer': transaction.insurer.slug,
                    'status': transaction.status,
                }
            }
        else:
            return HttpResponse(json.dumps({'success': False, 'errorCode': errors.EC_INVALID_PARAMETERS, 'errorMessage': errors.messages[errors.EC_INVALID_PARAMETERS], 'errorItems': [{'parameter': 'data_type', 'errorMessage': 'Invalid data type.'}], 'data': {}}))
        return HttpResponse(json.dumps(transaction_data))


class QuoteDetailView(pviews.BaseMixin, DetailView):
    model = Quote
    slug_url_kwarg = slug_field = 'quote_id'

    def get_context_data(self, object):
        context_data = super(QuoteDetailView, self).get_context_data()
        context_data[self.serialized_data_key_name] = serialized_response = copy.deepcopy(self.default_serialized_response)
        raw_data = object.raw_data
        if object.vehicle:
            raw_data.update({
                'make': object.vehicle.make.name,
                'model': object.vehicle.model.name,
                'variant': object.vehicle.variant,
            })
        serialized_response['data']['quote'] = {'raw_data': raw_data, }
        return context_data

    def get(self, *args, **kwargs):
        response = super(QuoteDetailView, self).get(*args, **kwargs)
        return self.render_json_response(response.context_data[self.serialized_data_key_name])


@csrf_exempt
@apilogger
@js_api
@authenticate_api
def vehicle_info(request, vehicle_type):
    return pviews.vehicle_info(request, vehicle_type)


def active_insurers(request, vehicle_type):
    return pviews.active_insurers(request, vehicle_type)


@csrf_exempt
@apilogger
@js_api
@authenticate_api
def vehicle_confirmation(request, vehicle_type):
    return pviews.vehicle_confirmation(request, vehicle_type)


