"""
Motor Cache utils.
We are caching motor quotes in three levels and we will lookup for cached data in similar manner as follows:

Level 1 --> QueryCache: This table creates a cache key based on the request parameters and will depend on exact
policy start and end dates etc.

Level 2 --> PremiumTableCache: This table calculates the age of vehicle (which depends on new policy start date and
registration date)  and identifies for which age range the premium is same. Based on that we create a bucket and next
time which ever vehicle lies in the same bucket range we serve the cached result.

Level 3 --> In Quote class we have defined 'put_channel' and 'get_channel' methods which quote channel based on quote
id and insurer_slug and saves the result in cache. This result can be used to show results on the front end.

"""
from datetime import datetime
from dateutil.relativedelta import relativedelta

from django.core.cache import caches
from django.conf import settings
from django.db import IntegrityError
from django.db.models import Q, F, Case, When
from django.utils import timezone

import hashlib
import json
import logging
from motor_product import models as motor_models
from motor_product.errors import INTEGRATION_ERRORS
from motor_product.insurers import insurer_utils
from utils import motor_caching, pg_advisory_lock, statsd

cache = caches['motor-results']

logger = logging.getLogger(__name__)

QUERY_CACHE_TIME = 3600 * 24 * 7

PREVIOUS_NCB_TO_NEW_NCB = {
    '0': '20',
    '20': '25',
    '25': '35',
    '35': '45',
    '45': '50',
    '50': '50',
}


class PremiumTableCache(object):
    """
    Base class for setting or getting motor premium saved in MotorPremium table.
    It's a level 2 cache for fetching motor premiums.
    Requires request_data dictionary to initialize with following keys:
    insurer_slug: slug of insurer,
    vehicleId: vehicle master id,
    registrationDate: '%d-%m-%Y'
    manufacturingDate: '%d-%m-%Y'
    quoteId: quote id
    registrationNumber[]: [u'MH', u'04', u'0', u'0']
    isNewVehicle: 0 or 1
    isExpired: 'true' or 'false'
    """

    def __init__(self, request_data):
        self.request_data = request_data
        self.insurer_slug = request_data['insurer_slug']
        self.vehicle_id = request_data['vehicleId']
        self.is_new_vehicle = request_data.get('isNewVehicle', '0')
        self.is_expired = request_data.get('isExpired', False)
        self.registration_date = request_data['registrationDate']
        self.manufacturing_date = request_data['manufacturingDate']
        self.rto = self.get_rto(request_data['registrationNumber[]'])
        self.new_ncb = self.get_ncb()
        self.policy_type = self.get_policy_type()

    def filter_params(self):
        insurer = motor_models.Insurer.objects.get(slug=self.insurer_slug)
        query_dict = {'vehicle_id': self.vehicle_id,
                      'insurer': insurer,
                      'zone': self.rto,
                      'policy_type': self.policy_type,
                      'new_ncb': self.new_ncb
                      }
        vehicle_age = self.get_vehicle_age()
        query_obj = Q(Q(**query_dict) & Q(max_age__gte=vehicle_age, min_age__lte=vehicle_age))
        return query_obj, query_dict

    def get_ncb(self):
        previous_ncb = self.request_data.get('previousNCB', '0')
        new_ncb = PREVIOUS_NCB_TO_NEW_NCB[previous_ncb]
        if self.is_expired == 'true':
            today = datetime.today()
            past_policy_expiry_date = datetime.strptime(self.request_data['pastPolicyExpiryDate'], '%d-%m-%Y')
            if (today - past_policy_expiry_date).days >= 90:
                new_ncb = '0'
        if self.request_data.get('isClaimedLastYear', '0') == '1':
            new_ncb = '0'
        return new_ncb

    def get_rto(self, reg_no):
        reg_no = '%s-%s' % (reg_no[0], reg_no[1])
        try:
            rto = motor_models.RTOMaster.objects.get(rto_code=reg_no)
        except motor_models.RTOMaster.DoesNotExist:
            rto = None
        return rto

    def get_policy_type(self):
        if self.is_new_vehicle == '1':
            policy_type = motor_models.MotorPremium.NEW_VEHICLE
        elif self.is_expired == 'true':
            policy_type = motor_models.MotorPremium.EXPIRED
        else:
            policy_type = motor_models.MotorPremium.RENEWAL
        return policy_type

    def get_premium(self):
        response = None
        caching_allowed = self.allow_motor_caching()
        if caching_allowed and getattr(settings, 'ENABLE_MOTOR_CACHING', False):
            query_obj, _ = self.filter_params()
            try:
                motor_premium = motor_models.MotorPremium.objects.get(query_obj)
            except motor_models.MotorPremium.DoesNotExist:
                pass
            except motor_models.MotorPremium.MultipleObjectsReturned:
                motor_caching.info("MultipleObjectsReturned for query %s in Motor Premium Table" % query_obj)
            except IntegrityError:
                motor_caching.info("Integrity occurred while fetching motor premium cache data for request %s" %
                                   self.request_data)
            else:
                response = motor_premium.response_data
                response['created'] = datetime.strftime(motor_premium.updated_on, '%d-%m-%Y %H:%M:%S')
        return response

    def set_premium(self, response_data):
        caching_allowed = self.allow_motor_caching()
        if caching_allowed:
            query_obj, _ = self.filter_params()
            try:
                obj = motor_models.MotorPremium.objects.get(query_obj)
                self.log_mismatch_error(response_data, obj)
            except motor_models.MotorPremium.DoesNotExist:
                self.create_premium_bucket(response_data)
            except motor_models.MotorPremium.MultipleObjectsReturned:
                motor_caching.info("MultipleObjectsReturned for query %s in Motor Premium Table" % query_obj)
            except IntegrityError:
                motor_caching.info("Integrity occurred while setting motor premium cache data for request %s" %
                                   self.request_data)

    def allow_motor_caching(self):
        caching_keys = ['idv', 'idvElectrical', 'idvNonElectrical', 'isCNGFitted', 'cngKitValue', 'voluntaryDeductible',
                        'extra_user_occupation', 'extra_isAntiTheftFitted', 'extra_isMemberOfAutoAssociation',
                        'extra_isTPPDDiscount', 'isNCBCertificate', 'extra_user_dob']
        allow_caching = True
        for cache_param in caching_keys:
            if self.request_data.get(cache_param, '0') not in ['0', '', None, False, 'false']:
                allow_caching = False
                break

        if self.request_data.get('isUsedVehicle', '0') == '1' or self.insurer_slug in ['oriental', 'universal-sompo']:
            allow_caching = False

        # Disabling caching logic for l-t if vehicle age is greater than 7 years due to discount logic incompatibility.
        if self.insurer_slug == 'l-t' and self.get_vehicle_age() >= 365 * 7:
            allow_caching = False
        return allow_caching

    def get_new_policy_start_date(self):
        try:
            new_policy_start_date = datetime.strptime(self.request_data['newPolicyStartDate'], '%d-%m-%Y')
        except KeyError:
            new_policy_start_date = None
            past_policy_expiry_date = datetime.strptime(self.request_data['pastPolicyExpiryDate'], '%d-%m-%Y')
            if self.policy_type == motor_models.MotorPremium.RENEWAL:
                new_policy_start_date = (past_policy_expiry_date + relativedelta(days=1))
            elif self.policy_type == motor_models.MotorPremium.EXPIRED:
                today = datetime.today()
                new_policy_start_date = (today + relativedelta(days=1))
        return new_policy_start_date

    def get_vehicle_age(self):
        start_date = self.get_new_policy_start_date().date()
        if self.is_new_vehicle == '1':
            end_date = self.manufacturing_date
        else:
            end_date = self.registration_date
        end_date = datetime.strptime(end_date, '%d-%m-%Y')
        if self.insurer_slug == 'l-t':
            # setting date to first of the selected month
            end_date = end_date - relativedelta(days=end_date.day-1)
        vehicle_age = (start_date - end_date.date()).days
        return vehicle_age

    def create_premium_bucket(self, response_data):
        from motor_product.prod_utils import get_flat_premium
        vehicle_age = self.get_vehicle_age()
        query_obj, query_dict = self.filter_params()
        obj = None
        new_premium = round(get_flat_premium({}, [response_data['data']])[0]['final_premium'])
        max_idv = response_data['data']['maxIDV']
        min_idv = response_data['data']['minIDV']
        try:
            premium_obj = motor_models.MotorPremium.objects.filter(premium__range=(new_premium - 2, new_premium + 2),
                                                                   max_idv=max_idv,
                                                                   min_idv=min_idv,
                                                                   **query_dict)
            premium_obj.update(
                min_age=Case(When(min_age__gt=vehicle_age, then=vehicle_age), default=F('min_age')),
                max_age=Case(When(max_age__lt=vehicle_age, then=vehicle_age), default=F('max_age')),
            )
            obj = premium_obj[0]
            motor_caching.info(
                 '''
                 Updating MOTOR_PREMIUM bucket:
                 vehicle_id --> %s
                 zone --> %s
                 insurer --> %s
                 previous_max_idv --> %s
                 new_max_idv --> %s
                 previous_min_idv --> %s
                 new_min_idv --> %s
                 previous_premium --> %s
                 new_premium --> %s
                 object_id --> %s
                 manufacturing_date --> %s
                 registration_date --> %s
                 ncb --> %s
                 vehicle_age -- > %s
                 request_data --> %s
             ''' % (obj.vehicle.id, obj.zone.rto_code, obj.insurer.slug, obj.max_idv, max_idv, obj.min_idv,
                    min_idv, obj.premium, new_premium, obj.id, self.manufacturing_date, self.registration_date,
                    self.new_ncb, self.get_vehicle_age(), self.request_data))
        except IndexError:
            create_items = query_dict.copy()
            create_items.update({'min_age': vehicle_age, 'max_age': vehicle_age})
            lock_name = ','.join(map(str, create_items.values()))
            try:
                with pg_advisory_lock(lock_name):
                    obj, created = motor_models.MotorPremium.objects.get_or_create(
                        premium__range=(new_premium - 2, new_premium + 2),
                        max_idv=max_idv,
                        min_idv=min_idv,
                        **query_dict)
                    obj.updated_on = timezone.localtime(timezone.now())
                    obj.response_data = response_data
                    obj.max_idv = max_idv
                    obj.min_idv = min_idv
                    obj.premium = new_premium
                    obj.min_age = vehicle_age
                    obj.max_age = vehicle_age
                    obj.save()
                motor_caching.info(
                         '''
                         Creating new MOTOR_PREMIUM bucket:
                         vehicle_id --> %s
                         zone --> %s
                         insurer --> %s
                         max_idv --> %s
                         min_idv --> %s
                         premium --> %s
                         object_id --> %s
                         manufacturing_date --> %s
                         registration_date --> %s
                         ncb --> %s
                         vehicle_age -- > %s
                         request_data --> %s
                         ''' % (
                             obj.vehicle.id, obj.zone.rto_code, obj.insurer.slug, max_idv, min_idv, new_premium,
                             obj.id, self.manufacturing_date, self.registration_date, self.new_ncb,
                             self.get_vehicle_age(), self.request_data,))
            except IntegrityError:
                motor_caching.info("Integrity error while creating motor-premium bucket for %s" % create_items)

        return obj

    def log_mismatch_error(self, response_data, db_premium):
        from motor_product.prod_utils import get_flat_premium
        new_max_idv = u'%s' % response_data['data']['maxIDV']
        new_min_idv = u'%s' % response_data['data']['minIDV']
        new_premium = int(round(get_flat_premium({}, [response_data['data']])[0]['final_premium']))
        if db_premium.max_idv != new_max_idv or db_premium.min_idv != new_min_idv or int(float(db_premium.premium)) \
                not in range(new_premium - 2, new_premium + 2):
            motor_caching.info(
                '''
                MOTOR PREMIUM MISMATCH:
                vehicle_id --> %s
                zone --> %s
                insurer --> %s
                previous_max_idv --> %s
                new_max_idv --> %s
                previous_min_idv --> %s
                new_min_idv --> %s
                previous_premium --> %s
                new_premium --> %s
                object_id --> %s
                manufacturing_date --> %s
                registration_date --> %s
                ncb --> %s
                request_data --> %s
                vehicle_age -- > %s
            ''' % (db_premium.vehicle.id, db_premium.zone.rto_code, db_premium.insurer.slug, db_premium.max_idv,
                   new_max_idv, db_premium.min_idv,
                   new_min_idv, db_premium.premium, new_premium, db_premium.id, self.manufacturing_date,
                   self.registration_date, self.new_ncb, self.request_data, self.get_vehicle_age()))

            statsd.incr('coverfox.motor.caching.mismatch', 1)
        else:
            statsd.incr('coverfox.motor.caching.match', 1)


def fetch_quote_premiums_from_cache(quote_id):
    """Fetch premiums from cache."""
    quote = motor_models.Quote.objects.filter(quote_id=quote_id).first()
    if not quote:
        return {'success': False, 'finished': True}

    fetch_finished = True
    premiums = []

    filters = {}
    if quote.vehicle.vehicle_type == 'Private Car':
        vehicle_type = 'Car'
        filters['is_active_for_car'] = True
    else:
        filters['is_active_for_twowheeler'] = True
        vehicle_type = 'Bike'
    insurers = motor_models.Insurer.objects.filter(**filters)
    for insurer in insurers.values_list('slug', flat=True):
        response = quote.get_channel(insurer)
        if response and response.get('isfetched') and response.get('data'):
            if 'errorCode' in response['data']:
                # Temporary hack to modify error messages.
                if response['data'].get('errorMsg', '') == INTEGRATION_ERRORS['RTO_MAPPING_NOT_FOUND']:
                    response['data'].update({'errorMsg': 'RTO not covered by insurer.'})
                elif response['data'].get('errorMsg', '') == INTEGRATION_ERRORS['INSURER_AGE_RESTRICTED']:
                    response['data'].update({'errorMsg': "%s's too old to be insured." % vehicle_type})
                else:
                    response['data'].update({'errorMsg': 'Pfffbt. Insurer server down.'})
            premiums.append(response['data'])
        else:
            fetch_finished = False

    return {
        'success': True,
        'errorCode': None,
        'errorMessage': None,
        'errorItems': [],
        'data': {
            'quoteId': quote_id,
            'premiums': premiums,
            'finished': fetch_finished,
        }
    }


class QueryCache(object):
    """
    It's a level 1 cache for fetching motor premiums.
    It creates a cache key based on request parameters i.e whatever the user inputs for viewing quotes. So it will
    give results cached quotes for exact dates only.
    """

    @staticmethod
    def get_premium(request_data, vehicle_type, insurer):
        cache_key = QueryCache.get_cache_key(request_data, vehicle_type, insurer)

    @staticmethod
    def set_premium(request_data, vehicle_type, insurer, data):
        cache_key = QueryCache.get_cache_key(request_data, vehicle_type, insurer)
        cache.set(cache_key, data, QUERY_CACHE_TIME)

    @staticmethod
    def get_cache_key(request_data, vehicle_type, insurer):
        from motor_product.prod_utils import VEHICLE_INTEGRATION_MAP
        m = VEHICLE_INTEGRATION_MAP[vehicle_type][insurer]

        ins = motor_models.Insurer.objects.get(slug=insurer)
        if vehicle_type == 'fourwheeler':
            disable_logic = ins.disable_cache_logic_car
        else:
            disable_logic = ins.disable_cache_logic_twowheeler

        if hasattr(m, 'get_cache_key') and not disable_logic:
            insurer_key = m.get_cache_key(request_data)
            if insurer_key is not None:
                return insurer_key

        caching_keys = ['isUsedVehicle', 'isNewVehicle', 'vehicleId', 'idv',
                        'idvElectrical', 'idvNonElectrical',
                        'isCNGFitted', 'cngKitValue', 'voluntaryDeductible', 'extra_user_occupation',
                        'extra_isAntiTheftFitted', 'extra_isMemberOfAutoAssociation', 'extra_isTPPDDiscount']

        hashing_dictionary = {}
        for key in caching_keys:
            if request_data.get(key, None):
                hashing_dictionary[key] = request_data[key]

        rto_info = "-".join(request_data['registrationNumber[]'][0:2])
        man_mm_yy = request_data['manufacturingDate'].split('-')[1:]

        hashing_dictionary['registrationRTO'] = rto_info

        hashing_dictionary['registrationDate'] = request_data['registrationDate']
        if request_data['isNewVehicle'] == '1':
            hashing_dictionary['manufacturingDate'] = man_mm_yy
        else:
            if request_data['isUsedVehicle'] == '1':
                if request_data['isNCBCertificate'] == '1':
                    hashing_dictionary['newNCB'] = request_data['newNCB']
                hashing_dictionary['isNCBCertificate'] = request_data[
                    'isNCBCertificate']
            else:
                if request_data['isClaimedLastYear'] == '0':
                    hashing_dictionary['previousNCB'] = request_data['previousNCB']
                hashing_dictionary['isClaimedLastYear'] = request_data[
                    'isClaimedLastYear']
                hashing_dictionary['pastPolicyExpiryDate'] = request_data[
                    'pastPolicyExpiryDate']

        hashing_dictionary['insurer'] = insurer

        customer_dob = request_data.get('extra_user_dob', None)
        if customer_dob:
            customer_dob_obj = datetime.strptime(customer_dob, '%d-%m-%Y')
            customer_age = int(insurer_utils.get_age(
                datetime.now(), customer_dob_obj))
            hashing_dictionary['extra_user_dob'] = customer_age

        # Change cache key if discount code present
        customer_discount_code = request_data.get('discountCode', None)
        if insurer_utils.get_discount_code(customer_discount_code):
            hashing_dictionary['discountCode'] = customer_discount_code

        hashing_json = json.dumps(hashing_dictionary, sort_keys=True)
        store_key = hashlib.md5(hashing_json).hexdigest()
        return store_key
