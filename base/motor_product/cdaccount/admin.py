from django.contrib import admin

from motor_product.cdaccount import models as cdaccount_models


admin.site.register(cdaccount_models.CDAccount)
admin.site.register(cdaccount_models.CDTransaction)
