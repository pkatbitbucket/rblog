from django import forms
from motor_product.cdaccount.models import CDAccount, CDTransaction


class CDTransactionForm(forms.ModelForm):
    class Meta:
        model = CDTransaction
        exclude = ('transaction', 'created_on', 'updated_on', 'balance_left')
    transaction_date = forms.DateField(input_formats=['%d-%m-%Y'])
    policy_number = forms.CharField(max_length=32)

    def __init__(self, *args, **kwargs):
        super(CDTransactionForm, self).__init__(*args, **kwargs)

        self.fields['transaction_date'].widget.attrs['class'] = "transaction_date"