# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0042_newregistrationdetail_registrationdetailstatistics'),
    ]

    operations = [
        migrations.CreateModel(
            name='BusinessCompany',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20)),
                ('slug', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='CDAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('balance', models.DecimalField(max_digits=9, decimal_places=2)),
                ('active', models.BooleanField(default=True)),
                ('insurer', models.OneToOneField(to='motor_product.Insurer')),
            ],
        ),
        migrations.CreateModel(
            name='CDTransaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('payment_mode', models.CharField(max_length=15, null=True, choices=[(b'MSWIPE', 'MSwipe'), (b'CHEQUE', 'Cheque')])),
                ('amount', models.DecimalField(max_digits=7, decimal_places=2)),
                ('transaction_type', models.CharField(default=b'CREDIT', max_length=12, choices=[(b'CREDIT', 'Credit'), (b'DEBIT', 'Debit')])),
                ('balance_left', models.DecimalField(max_digits=9, decimal_places=2)),
                ('transaction_date', models.DateField(null=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('business_type', models.ForeignKey(to='cdaccount.BusinessCompany')),
                ('cd_account', models.ForeignKey(to='cdaccount.CDAccount')),
                ('transaction', models.ForeignKey(to='motor_product.Transaction')),
            ],
        ),
    ]
