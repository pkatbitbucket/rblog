# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cdaccount', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cdtransaction',
            name='business_type',
            field=models.ForeignKey(to='core.Company', null=True),
        ),
        migrations.DeleteModel(
            name='BusinessCompany',
        ),
    ]
