# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cdaccount', '0002_auto_20160202_1300'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cdtransaction',
            name='payment_mode',
            field=models.CharField(max_length=15, null=True, choices=[(b'MSWIPE', 'MSwipe'), (b'CHEQUE', 'Cheque')]),
        ),
    ]
