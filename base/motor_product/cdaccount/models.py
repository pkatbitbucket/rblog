from django.db import models
from django.utils.translation import ugettext_lazy as _

from core import models as core_models
from motor_product import models as motor_models


class CDAccount(models.Model):
    insurer = models.OneToOneField(motor_models.Insurer)
    balance = models.DecimalField(max_digits=9, decimal_places=2)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s" % self.insurer


class CDTransaction(models.Model):
    MSWIPE = "MSWIPE"
    CHEQUE = "CHEQUE"
    CREDIT = "CREDIT"
    DEBIT = "DEBIT"
    PAYMENT_TYPES = (
        (MSWIPE, _('MSwipe')),
        (CHEQUE, _('Cheque'))
    )
    TRANSACTION_CHOICES = (
        (CREDIT, _("Credit")),
        (DEBIT, _("Debit"))
    )
    cd_account = models.ForeignKey('CDAccount')
    transaction = models.ForeignKey(motor_models.Transaction)
    payment_mode = models.CharField(max_length=15, choices=PAYMENT_TYPES, null=True)
    business_type = models.ForeignKey(core_models.Company, null=True)
    amount = models.DecimalField(max_digits=7, decimal_places=2)
    transaction_type = models.CharField(max_length=12, choices=TRANSACTION_CHOICES, default=CREDIT)
    balance_left = models.DecimalField(max_digits=9, decimal_places=2)
    transaction_date = models.DateField(null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "{}, {}".format(self.cd_account.insurer.title, self.updated_on.isoformat() if self.updated_on else '')
