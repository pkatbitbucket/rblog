from django.core.mail import send_mail
from django.template import loader
from celery_app import app
import datetime
import settings
from motor_product.cdaccount.models import (CDTransaction, CDAccount)


def get_report(mail=True, account_id=None, from_date=None, to_date=None):
    cd_accounts = CDAccount.objects.filter(active=True)
    if account_id:
        cd_accounts = cd_accounts.filter(id=account_id)
    reports = []
    for account in cd_accounts:
        report = {
            'title': account.insurer,
        }
        report['title'] = account.insurer
        report['balance'] = account.balance
        report['transactions'] = account.cdtransaction_set.all()
        if from_date and to_date:
            report['transactions'] = report['transactions'].filter(created_on__gte=from_date, created_on__lte=to_date)
        reports.append(report)
    return reports


@app.task()
def daily_report():
    date_to = datetime.datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)
    date_from = date_to - datetime.timedelta(days=1)
    reports = get_report(from_date=date_from, to_date=date_to)
    html_message = loader.render_to_string(
        'motor_product/cdaccount/daily_report.html',
        {
            'reports': reports,
        }
    )
    subject = "CD Accounts Daily Report"
    send_mail(subject, html_message, settings.DEFAULT_FROM_EMAIL, settings.CD_EMAIL_ACCOUNTS, fail_silently=True, html_message=html_message)
