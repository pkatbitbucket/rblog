from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^cdadmin/$', views.cdadmin, name='cdadmin'),
    url(r'^cd-transaction/(?P<account_id>[\w-]+)/$', views.cdtransaction, name='cd_transaction'),
    url(r'^cd-debit/(?P<transaction_id>[\w-]+)/$', views.cd_debit, name='cd_debit'),
    url(r'^viewreport/$', views.view_report, name='cd_report'),
    url(r'^viewreport/(?P<account_id>[\w-]+)/$', views.view_report, name='cd_account_report'),
]
