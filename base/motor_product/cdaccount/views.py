from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
from django.views.decorators.http import require_POST

from core import models as core_models

from motor_product import models as motor_models
from motor_product.cdaccount import models as cdaccount_models
from motor_product.cdaccount import forms as cdaccount_forms
from motor_product.cdaccount import tasks as cdaccount_tasks


import datetime
from decimal import Decimal

from django.contrib.auth.decorators import permission_required
# Create your views here.


@permission_required('cdaccount.change_cdaccount', raise_exception=False)
def cdadmin(request):
    return render(request, 'motor_product/cdadmin.html', {
        'accounts': cdaccount_models.CDAccount.objects.filter(active=True)
    })


@permission_required('cdaccount.change_cdaccount', raise_exception=False)
def cdtransaction(request, account_id):
    if request.method == 'GET':
        return render(request, 'motor_product/cdtransaction.html', {
            'form': cdaccount_forms.CDTransactionForm(
                initial={'cd_account': account_id}
            ),
            'account_id': account_id
        })

    if request.method == 'POST':
        account = get_object_or_404(cdaccount_models.CDAccount, id=account_id)
        cdform = cdaccount_forms.CDTransactionForm(request.POST)
        if cdform.is_valid():
            cd_trans = cdform.save(commit=False)
            cleaned_data = cdform.cleaned_data
            try:
                transaction = motor_models.Transaction.objects.get(
                    policy_number=cleaned_data['policy_number']
                )
            except:
                data = {
                    'success': False,
                    'errors': {'Policy Number': ["Policy number doesn't exist"]},
                }
                return JsonResponse(data)
            else:
                cd_trans.transaction = transaction
                amount = cleaned_data['amount']
                if cleaned_data['transaction_type'] == 'CREDIT':
                    account.balance += int(amount)
                else:
                    account.balance -= int(amount)

                cd_trans.balance_left = account.balance
                cd_trans.save()
                account.save()
                data = {
                    'success': True,
                    'balance': str(account.balance),
                    'account_id': account_id
                }
                return JsonResponse(data)
        else:
            data = {
                'success': False,
                'errors': cdform.errors
            }
            return JsonResponse(data)


@require_POST
def cd_debit(request, transaction_id):
    transaction = motor_models.Transaction.objects.get(transaction_id=transaction_id)
    payment_mode = request.POST.get('payment_mode', 'CD_ACCOUNT')
    transaction.quote.raw_data['payment_mode'] = payment_mode
    transaction.quote.save()

    if not request.user.groups.filter(name="CD_ACCOUNT_ADMIN").exists():
        return JsonResponse({'success': False, 'errors': ['Not authorized for CD transaction']})
    if payment_mode == 'PAYMENT_GATEWAY':
        return JsonResponse({'success': True})

    payment_type = request.POST.get('payment_type', 'MSWIPE').upper()
    payment_details = {"payment_method": payment_type, "payments_installments": []}
    first_payment = {
            'ref_no.': request.POST.get('cd_pay_1'),
            'amount': request.POST.get('amt_1')
        }
    payment_details['payments_installments'].append(first_payment)
    if request.POST.get('cd_pay_2', None):
        second_payment = {
            'ref_no': request.POST['cd_pay_2'],
            'amount': request.POST.get('amt_2', 0),
        }
        third_payment = {
            'ref_no': request.POST.get('cd_pay_3', ''),
            'amount': request.POST.get('amt_3', 0),
        }
        payment_details['payments_installments'].extend([second_payment, third_payment])

    transaction.raw['cd_payment_details'] = payment_details
    transaction.save()
    return JsonResponse({'success': True})


@permission_required('cdaccount.change_cdaccount', raise_exception=False)
def view_report(request, account_id=None):
    return render(request, "motor_product/cdaccount/daily_report.html", {
        'reports': cdaccount_tasks.get_report(mail=False, account_id=account_id)
    })
