FACTO_GRID = \
    {
        "Honda": {
            "Bajaj Allianz General Insurance": "33",
            "HDFC Ergo General Insurance": "22",
            "Iffco Tokio General Insurance": "19",
        },
        "Bajaj": {
            "Bajaj Allianz General Insurance": "28",
            "Bharti AXA General Insurance": "23",
            "HDFC Ergo General Insurance": "21",
        },
        "Hero": {
            "Bajaj Allianz General Insurance": "32",
            "Iffco Tokio General Insurance": "21",
            "Bharti AXA General Insurance": "20",
        },
        "Tvs": {
            "Bajaj Allianz General Insurance": "41",
            "Bharti AXA General Insurance": "17",
            "HDFC Ergo General Insurance": "18",
        },
        "Hero Honda": {
            "Bajaj Allianz General Insurance": "34",
            "Bharti AXA General Insurance": "24",
            "HDFC Ergo General Insurance": "7",
        },
        "Yamaha": {
            "HDFC Ergo General Insurance": "39",
            "Bajaj Allianz General Insurance": "32",
            "Universal Sompo General Insurance": "17"
        },
        "Royal Enfield": {
            "HDFC Ergo General Insurance": "29",
            "Bajaj Allianz General Insurance": "32",
            "Iffco Tokio General Insurance": "37",
        },
        "Suzuki": {
            "Iffco Tokio General Insurance": "33",
            "HDFC Ergo General Insurance": "32",
            "Bharti AXA General Insurance": "20",
        },
        "Mahindra": {
            "Bajaj Allianz General Insurance": "39",
            "Iffco Tokio General Insurance": "27",
            "Bharti AXA General Insurance": "18",
        },
        "Bajaj Ktm": {
            "Bharti AXA General Insurance": "29",
            "Bajaj Allianz General Insurance": "24",
            "HDFC Ergo General Insurance": "24",
        },
        "Piaggio": {
            "Bajaj Allianz General Insurance": "86",
            "HDFC Ergo General Insurance": "11",
            "Universal Sompo General Insurance": "3"
        },
        "Harley Davidson": {
            "HDFC Ergo General Insurance": "80",
            "Universal Sompo General Insurance": "10",
            "Bajaj Allianz General Insurance": "10",
        },
        "Lml": {
            "HDFC Ergo General Insurance": "38",
            "Bajaj Allianz General Insurance": "38",
            "Bharti AXA General Insurance": "13",
        },
        "Kinetic": {
            "Bajaj Allianz General Insurance": "66",
            "HDFC Ergo General Insurance": "16",
            "Bharti AXA General Insurance": "16",
        },
        "Triumph": {
            "HDFC Ergo General Insurance": "100",
        },
        "Kawasaki": {
            "HDFC Ergo General Insurance": "100",
        }
    }
