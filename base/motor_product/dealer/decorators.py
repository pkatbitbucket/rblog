from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from motor_product.models import DealerDealershipMap
from oauth2_provider.models import AccessToken
from django.contrib.auth import authenticate, login


def is_motor_dealer(view):
    def inner(request, *args, **kwargs):
        # TODO: Implement the functionality through request.company
        for employee in request.user.employee_instance.all():
            if employee.has_perm('is_dealer'):
                dealership = employee.company.dealership
                dealer = employee.company.dealer
                dealer_dealership_map = get_object_or_404(DealerDealershipMap, dealer=dealer, dealership=dealership)
                if dealer_dealership_map.is_approved and dealership.is_approved:
                    request.company = employee.company
                    return view(request, *args, **kwargs)
                else:
                    return HttpResponseRedirect(reverse("dealer_awaiting_approval"))
        raise Http404("Unauthorized")

    return inner


def login_from_token(view):
    def inner(request, *args, **kwargs):
        auth_token = request.META.get("HTTP_AUTHORIZATION", "None ").split(' ')
        if auth_token[0] == 'Basic':
            instance = AccessToken.objects.filter(token=auth_token[1]).first()
            if instance and not instance.is_expired():
                auth_user = authenticate(email=instance.user.email)
                login(request, auth_user)
                return view(request, *args, **kwargs)
        return view(request, *args, **kwargs)

    return inner
