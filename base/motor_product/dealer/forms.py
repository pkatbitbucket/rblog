from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator, validate_email

from core import models as core_models
from motor_product.models import Dealership
from motor_product.models import DealerDealershipMap
from motor_product.models import Dealer
from django.contrib.auth.models import Permission


alpha = RegexValidator(
    r'^[a-zA-Z ]+$', 'Only alphabets are allowed.')
numeric = RegexValidator(r'^[0-9]+$', 'Only numeric characters are allowed.')


class DealerRegistrationForm(forms.Form):
    first_name = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'First Name'}),
        max_length=50, validators=[alpha])
    middle_name = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Middle Name'}),
        max_length=50, required=False, validators=[alpha])
    last_name = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Last Name'}),
        max_length=50, required=False, validators=[alpha])
    email = forms.CharField(
        widget=forms.EmailInput(attrs={'placeholder': 'Email ID'}))
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))
    mobile = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Mobile Number'}),
        max_length=10, required=True, validators=[numeric])
    company_name = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Your Company\'s name'}),
        max_length=200, required=True)
    address = forms.CharField(
        widget=forms.Textarea(attrs={'placeholder': "Your company's address"}),
        max_length=1000, required=True)

    def __init__(self, *args, **kwargs):
        super(DealerRegistrationForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'cf-input'

    def save(self):
        data = self.cleaned_data
        user = core_models.User.objects.create_user(
            data['email'], first_name=data['first_name'],
            password=data['password'], is_verified=True)
        # TODO: Have cutom verification for dealers
        user.last_name = data['last_name']
        user.middle_name = data['middle_name']
        user.phone = data['mobile']
        user.save()
        unique_slug = '-'.join(data['company_name'].lower().split())
        existing_slugs = core_models.Company.objects.filter(slug__startswith=unique_slug)
        unique_slug += str(existing_slugs.count() - 1) if existing_slugs else ''
        # Core model instance creation
        company_instance = core_models.Company.objects.create(
            name=data['company_name'], address=data['address'], slug=unique_slug)
        employee_instance = core_models.Employee.objects.create(
            company=company_instance, user=user)
        is_dealer_perm = Permission.objects.get(codename='is_dealer')
        employee_instance.user_permissions.add(is_dealer_perm)

        # Motor specific instance creation
        dealership_instance = Dealership.objects.create(
            company=company_instance)
        dealer_instance = Dealer.objects.create(
            company=company_instance)
        DealerDealershipMap.objects.create(
            dealership=dealership_instance,
            dealer=dealer_instance)
        return user

    def clean(self, *args, **kwargs):
        super(DealerRegistrationForm, self).clean(*args, **kwargs)
        if core_models.User.objects.filter(emails__email__iexact=self.cleaned_data['email']):
            raise ValidationError("That email ID already exists")
        return self.cleaned_data


class EmployeeRegistrationForm(forms.ModelForm):
    first_name = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'First Name', 'required':'true'}),
        max_length=50, validators=[alpha])
    middle_name = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Middle Name'}),
        max_length=50, required=False, validators=[alpha])
    last_name = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Last Name', 'required':'true'}),
        max_length=50, required=False, validators=[alpha])
    email = forms.CharField(
        widget=forms.EmailInput(attrs={'placeholder': 'Email ID', 'required':'true'}),
        validators=[validate_email])
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))
    mobile = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Mobile Number'}),
        max_length=12, min_length=10, required=True, validators=[numeric])

    class Meta:
        model = core_models.Employee
        fields = ['title', 'company']

    def __init__(self, *args, **kwargs):
        super(EmployeeRegistrationForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'cf-input'
            if field_name == 'company':
                field.widget.attrs['display'] = 'none'

    def save(self):
        data = self.cleaned_data
        user = core_models.User.objects.create_user(
            data['email'], first_name=data['first_name'],
            password=data['password'], is_verified=True)
        # TODO: Have cutom verification for dealers
        user.last_name = data['last_name']
        user.middle_name = data['middle_name']
        user.phone = data['mobile']
        user.save()
        employee_instance = core_models.Employee.objects.create(
            company=data['company'], user=user)
        is_dealer_perm = Permission.objects.get(codename='is_dealer')
        employee_instance.user_permissions.add(is_dealer_perm)
        return employee_instance

    def clean(self, *args, **kwargs):
        super(EmployeeRegistrationForm, self).clean(*args, **kwargs)
        if 'email' in self.cleaned_data:
            if core_models.User.objects.filter(emails__email__iexact=self.cleaned_data['email']):
                raise ValidationError("That email ID already exists")
        return self.cleaned_data


