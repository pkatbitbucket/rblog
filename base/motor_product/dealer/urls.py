from django.conf.urls import url

from . import views as dealer_views

urlpatterns = [
    url(r'^$', dealer_views.NewVehicle.as_view(), name="dealer_dashboard"),
    url(r'^select-dealership/$', dealer_views.SelectDealershipView.as_view(), name="select-dealership"),
    url(r'^(?P<vehicle_type>[\w]+)/upload-document/(?P<transaction_id>[\w-]+)/$',
        dealer_views.DealerUploadDocument.as_view()),
    url(r'^pending-cases/$', dealer_views.DealerPendingCases.as_view(), name="dealer_pending_case"),
    url(r'^(?P<vehicle_type>[\w]+)/payment/(?P<transaction_id>[\w-]+)/$', dealer_views.DealerPayment.as_view()),
    url(r'^generate-invoice/$', dealer_views.DealerInvoiceView.as_view(), name="dealer_generate_invoice"),
    url(r'^awaiting-approval/$', dealer_views.DealerAwaitingApprovalView.as_view(), name="dealer_awaiting_approval"),
    url(r'^add-employee/$', dealer_views.DealerNewEmployee.as_view(), name="dealer_add_employee"),
    url(r'^autologin/', dealer_views.autologin, name="dealer_autologin")
]
