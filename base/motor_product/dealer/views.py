# Django imports
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.db.models import Q
# Python Imports
import json
import datetime
# Local Imports
from motor_product.dealer.decorators import is_motor_dealer as is_dealer
from motor_product.dealer.decorators import login_from_token
from motor_product import models as motor_models
from core.models import Requirement
from motor_product.dealer.forms import DealerRegistrationForm, \
    EmployeeRegistrationForm
from payment.utils import create_payment
from payment.forms import ChequePaymentForm
from core.models import Payment, Employee, Email
from core.users import User
from wiki.models import City
from core import models as core_models
from django.contrib.auth import login, authenticate
from django.http import HttpResponse


class RegisterDealer(FormView):
    template_name = "register_dealer.html"
    form_class = DealerRegistrationForm

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect('/motor/dealer/')
        return super(RegisterDealer, self).form_valid(form)


class BaseDealerView(TemplateView):

    @method_decorator(login_from_token)
    @method_decorator(login_required)
    @method_decorator(is_dealer)
    def dispatch(self, *args, **kwargs):
        if not self.request.session.get('dealer_dealership_id', False):
            available_dealerships = self.get_available_dealerships()['dealerships']
            if available_dealerships.count() == 1:
                self.request.session['dealer_dealership_id'] = available_dealerships.first().id
        return super(BaseDealerView, self).dispatch(*args, **kwargs)

    def get_available_dealerships(self, **kwargs):
        context = {}
        all_dealer_instance = motor_models.Dealer.objects.filter(
            company__in=Employee.objects.filter(user=self.request.user).values('company'))
        context['dealerships'] = motor_models.DealerDealershipMap.objects.filter(
            dealership__id__in=all_dealer_instance.values_list('dealership', flat=True))
        return context


class SelectDealershipView(BaseDealerView):
    template_name = "dealer:select_dealership.html"

    def get(self, request, *args, **kwargs):
        context = self.get_available_dealerships()
        request.session['available_dealership_count'] = context['dealerships'].count()
        if context['dealerships'].count() == 1:
            request.session['dealer_dealership_id'] = context['dealerships'][0].id
            return HttpResponseRedirect(reverse('dealer_dashboard'))
        if request.GET.get('dealership_id', False):
            dealer_dealership_instance = get_object_or_404(
                motor_models.DealerDealershipMap, id=request.GET['dealership_id'])
            request.session['dealer_dealership_id'] = dealer_dealership_instance.id
            return HttpResponseRedirect(reverse('dealer_dashboard'))
        return self.render_to_response(context)


class NewVehicle(BaseDealerView):
    template_name = "dealer:index.html"

    def get(self, request, *args, **kwargs):
        dealer_dealership_id = request.session.get('dealer_dealership_id', None)
        if dealer_dealership_id is None:
            return HttpResponseRedirect(reverse('select-dealership'))
        context = {'is_dealer_dashboard': True}
        return self.render_to_response(context)


class DealerUploadDocument(BaseDealerView):
    template_name = "dealer:pending-upload-document.html"

    def get(self, request, *args, **kwargs):
        dealer_dealership_id = request.session.get('dealer_dealership_id', None)
        if dealer_dealership_id is None:
            return HttpResponseRedirect(reverse('select-dealership'))
        transaction_id = kwargs.get('transaction_id', False)
        # Redirecting document upload to inspection form for now
        # return HttpResponseRedirect(reverse('inspection_form', kwargs={'transaction_id': transaction_id}))
        context = {'is_dealer_dashboard': True}
        context['transaction'] = get_object_or_404(
            motor_models.Transaction, transaction_id=transaction_id)
        final_premium = int(round(float(context['transaction'].raw['quote_response']['final_premium'])))
        context['quote_data'] = {
            'insurer_slug': context['transaction'].insurer.slug,
            'quote_params': context['transaction'].quote.raw_data,
            'final_premium': final_premium,
        }
        requirement = context['transaction'].requirement
        all_payments = requirement.payment.all()
        if not all_payments:
            payment_instance = create_payment(
                self.request.user, requirement=requirement,
                payment_type='cheque', amount=final_premium).chequepayment
        else:
            payment_instance = all_payments[0].chequepayment
        context['payment_data'] = json.dumps({
            "cheque_image": None if not payment_instance.cheque_image else payment_instance.cheque_image.url,
            "cheque_number": payment_instance.cheque_number,
            "bank_name": payment_instance.bank_name,
        })
        context['payment_id'] = payment_instance.uuid
        # context['inspection'] = context['transaction'].inspection_set.filter(status=Inspection.PENDING)
        email_instance, new_agent = Email.objects.get_or_create(
            email="dummyinspectionagent@coverfoxmail.com",
            is_verified=True, is_primary=True,
            defaults={
                "email": "dummyinspectionagent@coverfoxmail.com",
                "is_verified": True,
                "is_primary": True
            }
        )

        agent = None
        if new_agent:
            user = User.objects.create()
            email_instance.user = user
            agent = user
            email_instance.save()
        else:
            agent = email_instance.user

        if new_agent:
            agent.set_password("coverfox_password")
            agent.save()
        field_agent, is_new_fa = motor_models.FieldAgent.objects.get_or_create(
            company=request.company,
            user=agent)
        agency, is_new_agency = motor_models.InspectionAgency.objects.get_or_create(
            company=request.company)
        inspection_area, is_new_area = motor_models.InspectionArea.objects.get_or_create(
            name="Dummy Dealer Inspection Area",
            city=City.objects.get(name='Mumbai'),
            is_enabled=False)
        inspection_agent, is_new_ia = motor_models.InspectionAgent.objects.get_or_create(
            weekly_off=[0, 1, 2, 3, 4, 5, 6],
            user=agent)
        inspection_agent.area.add(inspection_area)
        inspection_agent.save()
        context['inspection'] = motor_models.Inspection.objects.get_or_create(
            transaction=context['transaction'],
            agent=inspection_agent,
            schedule_slot=motor_models.DailyTimeSlot.objects.first(),
            inspection_agency=agency
        )[0]
        context['inspection'].scheduled_on = datetime.date.today()
        context['inspection'].save()
        context['data'] = {}
        for x in motor_models.MotorInspectionDocument.objects.filter(
                inspection=context['inspection']):
            existing_data = context['data'].get(x.document_type, [])
            existing_data.append({
                'image_url': x.document.url,
                'id': x.id,
                'approval_status': x.approval_status,
                'progress': 100,
            })
            context['data'][x.document_type] = existing_data
        context['data'] = json.dumps(context['data'])
        return self.render_to_response(context)


class DealerPendingCases(BaseDealerView):
    template_name = "dealer:pending-proposal-list.html"

    def get(self, request, *args, **kwargs):
        dealer_dealership_id = request.session.get('dealer_dealership_id', None)
        if dealer_dealership_id is None:
            return HttpResponseRedirect(reverse('select-dealership'))
        filter_status = request.GET.get('filter', None)
        dealer_id = request.GET.get('dealer_filter', None)
        if filter_status is not None and len(filter_status) == 0:
            filter_status = None
        context = {'requirements': [], 'is_dealer_pending_cases': True}
        dealers = motor_models.DealerDealershipMap.objects.filter(dealership=dealer_dealership_id)
        if dealers.count():
            context['dealers'] = dealers
            context['selected_dealer'] = dealer_id
        if dealer_id:
            dealer_dealership_id = dealer_id
        requirements = Requirement.objects.filter(
            id__in=motor_models.RequirementDealerMap.objects.filter(
                dealer=dealer_dealership_id).values('requirement')
        )
        for requirement in requirements:
            temp_transactions = []
            for transaction in requirement.requirement_transactions.all():
                all_inspections = transaction.inspection_set.all()
                if filter_status is not None:
                    all_inspections = all_inspections.filter(status=filter_status)
                inspection = None
                inspection_approved = False
                if all_inspections:
                    inspection = all_inspections[0]
                    if inspection.status == motor_models.Inspection.DOCUMENT_APPROVED:
                        inspection_approved = True
                temp_transactions.append({
                    "vehicle_name": str(transaction.quote.vehicle),
                    "area": transaction.vehicle_registration_number()[:5],
                    "registration": transaction.vehicle_registration_number(),
                    "status": inspection.get_status_display() if inspection else "Not Scheduled",
                    "updated_on": transaction.updated_on.strftime('%Y-%m-%d %H:%M:%S'),
                    "form_link": transaction.proposal_form_link(),
                    "transaction_id": transaction.transaction_id,
                    "next_link": inspection.dealer_get_next_link() if inspection else None,
                    "inspection_approved": inspection_approved,
                })
            context['requirements'].append({
                "transactions": temp_transactions
            })
        context['filter_options'] = motor_models.Inspection.STATUS_CHOICES
        context['selected_filter'] = filter_status
        return self.render_to_response(context)


class DealerPayment(BaseDealerView):
    template_name = "dealer:dealer_payment.html"

    def generate_payment_forms(self, *args, **kwargs):
        payments = []
        for payment in kwargs['payments']:
            if str(payment.uuid) == kwargs.get('uuid', ''):
                temp_form = ChequePaymentForm(kwargs['data'], kwargs['files'])
            else:
                temp_form = ChequePaymentForm(instance=payment)
            payments.append({
                "form": temp_form,
                "uuid": payment.uuid,
            })
        return payments

    def get(self, request, *args, **kwargs):
        dealer_dealership_id = request.session.get('dealer_dealership_id', None)
        if dealer_dealership_id is None:
            return HttpResponseRedirect(reverse('select-dealership'))
        context = {}
        transaction = get_object_or_404(motor_models.Transaction, transaction_id=kwargs.get('transaction_id', 0))
        if transaction.requirement and transaction.requirement.user == self.request.user:
            requirement = transaction.requirement
        else:
            raise Http404("Unauthorized access.")
        all_payments = requirement.payment.all()
        if not all_payments:
            create_payment(
                self.request.user, requirement=requirement,
                payment_type='cheque', amount=transaction.raw['quote_response']['final_premium'])
        elif all_payments.filter(status=Payment.PENDING).count() == 0:
            return HttpResponseRedirect("/motor/dealer/pending-cases")
        context['payments'] = self.generate_payment_forms(payments=requirement.payment.all())
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        transaction = get_object_or_404(motor_models.Transaction, transaction_id=kwargs.get('transaction_id', 0))
        if transaction.requirement and transaction.requirement.user == self.request.user:
            requirement = transaction.requirement
        else:
            raise Http404("Unauthorized access.")
        is_ajax = int(request.POST.pop("is_ajax", [0])[0])
        inspection = transaction.inspection_set.filter(
            ~Q(status=motor_models.Inspection.CANCELLED
        )).order_by('created_on').last()
        form_instance = ChequePaymentForm(request.POST, request.FILES)
        if form_instance.is_valid():
            instance = form_instance.save(uuid=request.POST['uuid'])
            if is_ajax == 1:
                return JsonResponse({'cheque_image': instance.cheque_image.url})
            elif inspection:
                inspection.status = motor_models.Inspection.PAYMENT_DONE
                inspection.save()
        else:
            context = {'payments': self.generate_payment_forms(
                payments=requirement.payment.all(),
                uuid=request.POST['uuid'],
                data=request.POST,
                files=request.FILES)}
            return self.render_to_response(context)
        return HttpResponseRedirect("/motor/dealer/")


class DealerInvoiceView(BaseDealerView):
    template_name = "dealer:dealer_invoice_generation.html"

    def get(self, request, *args, **kwargs):
        dealer_dealership_id = request.session.get('dealer_dealership_id', None)
        if dealer_dealership_id is None:
            return HttpResponseRedirect(reverse('select-dealership'))
        context = super(DealerInvoiceView, self).get_context_data(**kwargs)
        dealer_dealership_instance = motor_models.DealerDealershipMap.objects.get(id=dealer_dealership_id)
        context['is_dealer_generate_invoice'] = True
        context['invoices'] = motor_models.DealerInvoice.objects.filter(
            dealership=dealer_dealership_instance.dealership)
        return self.render_to_response(context)


class DealerAwaitingApprovalView(BaseDealerView):
    template_name = "dealer:awaiting_approval.html"

    @method_decorator(login_from_token)
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return self.get(self.request, *args, **kwargs)


class DealerNewEmployee(BaseDealerView):
    template_name = "dealer:add_new_employee.html"

    def get(self, request, *args, **kwargs):
        context = {}
        employee_form = EmployeeRegistrationForm(initial={'company': self.request.user.employee_instance.last().company})
        employee_form.fields['company'].queryset = core_models.Company.objects.filter(employees=request.user)
        return self.render_to_response(context={'formset': employee_form, 'is_dealer_add_employee': True})

    @method_decorator(login_required)
    @method_decorator(is_dealer)
    def post(self, request, *args, **kwargs):
        employee_form = EmployeeRegistrationForm(request.POST)
        if employee_form.is_valid():
            employee = employee_form.save()
            employee.company = self.request.user.employee_instance.last().company
            employee.save()
            return HttpResponseRedirect("/motor/dealer/")
        else:
            return self.render_to_response(context={'formset': employee_form, 'is_dealer_add_employee': True})


def autologin(request):
    token = request.GET.get('tk')
    redirect_url = reverse('dealer_dashboard')

    if token:
        user = authenticate(token=token)
        if user:
            login(request, user)
            return HttpResponseRedirect(redirect_url)

    return HttpResponse(json.dumps({'noaction': True}))
