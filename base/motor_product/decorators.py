import binascii
import json

import putils
import utils

from django.http.request import QueryDict
from motor_product import models as motor_models
from motor_product.parser_utils import clean_body_parameters
from motor_product.insurers import insurer_utils


def crypt(view):
    def inner(request, *args, **kwargs):
        resp = view(request, *args, **kwargs)
        content = resp.content
        if request.method == "POST" and 'csrfmiddlewaretoken' in request.POST:
            key = request.POST['csrfmiddlewaretoken']
            key = key.encode('hex')
            iv, encrypted = putils.Cryptor.encrypt(content, key)
            ctext = binascii.b2a_base64(encrypted).rstrip()
            encrypted_content = "%s%s%s" % (key, ctext, iv)
            resp.content = encrypted_content
        return resp

    return inner


def amlpl(view):
    def inner(request, *args, **kwargs):
        resp = view(request, *args, **kwargs)
        if request.user_agent.is_mobile:
            amlpl = request.TRACKER.extra.get('amlpl', None)
            if amlpl:
                try:
                    resp.set_cookie('aml_mobile', amlpl['msisdn'])
                except:
                    pass
        return resp

    return inner


def is_admin(view):
    def inner(request, *args, **kwargs):
        request.META['IS_ADMIN'] = request.user.is_authenticated() and request.user.groups.filter(name='CD_ACCOUNT_ADMIN').exists()
        resp = view(request, *args, **kwargs)
        return resp

    return inner


def lms_event(event_type, methods):
    def wrap(view):
        def inner(request, *args, **kwargs):
            resp = view(request, *args, **kwargs)

            try:
                if request.method in methods:
                    post_parameters = dict([(k, v)
                                            for k, v in request.POST.items()])
                    get_parameters = dict([(k, v)
                                           for k, v in request.GET.items()])
                    cookies = dict([(k, v)
                                    for k, v in request.COOKIES.items()])

                    data = post_parameters
                    data.update(get_parameters)
                    data.update(cookies)

                    if event_type == "result_page":
                        try:
                            if kwargs.get('quote_id', None):
                                return resp

                            content = json.loads(resp.content)
                            data['quoteId'] = content['data']['quoteId']
                            customer_discount_code = post_parameters.get(
                                'discountCode', None)
                            discount_code = insurer_utils.get_discount_code(
                                customer_discount_code)
                            if discount_code:
                                lead = discount_code.get_corporate_lead()
                                if lead:
                                    data['lead_id'] = lead.id
                        except:
                            pass

                    if event_type == "buy_page":
                        data['transaction_url'] = request.build_absolute_uri()
                        try:
                            transaction_id = kwargs['transaction_id']
                            transaction = motor_models.Transaction.objects.get(
                                transaction_id=transaction_id)
                            data['transactionId'] = transaction.transaction_id
                            data['quoteId'] = transaction.quote.quote_id
                        except:
                            pass

                    product_type = 'motor'
                    vehicle_type = kwargs.get('vehicle_type', None)
                    if vehicle_type == 'twowheeler':
                        product_type = 'motor-bike'

                    utils.LMSUtils.send_lms_event(
                        event=event_type,
                        product_type=product_type,
                        tracker=request.TRACKER,
                        data=data,
                        email=request.COOKIES.get('email'),
                        mobile=request.COOKIES.get('mobileNo'),
                        internal=request.IS_INTERNAL,
                        request_ip=putils.get_request_ip(request)
                    )
            except:
                pass

            return resp
        return inner
    return wrap


def json_to_query_dict(view):
    def wrapper(request, *args, **kwargs):
        if not request.POST and request.body:
            post_params = json.loads(request.body)
            post_params = clean_body_parameters(post_params)
            query_dict = QueryDict('', mutable=True)
            query_dict.update(post_params)
            request.POST = query_dict
        resp = view(request, *args, **kwargs)
        return resp

    return wrapper
