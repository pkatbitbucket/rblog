messages = {}

EC_MISSING_PARAMETERS = 435
messages[EC_MISSING_PARAMETERS] = 'Missing required parameter(s).'

EC_INVALID_PARAMETERS = 436
messages[EC_INVALID_PARAMETERS] = 'Invalid request parameter(s).'

EC_FORBIDDEN_REQUEST = 403
messages[EC_FORBIDDEN_REQUEST] = 'Forbidden request.'

EC_INVALID_REQUEST = 404
messages[EC_INVALID_REQUEST] = 'No such details for insurer.'

ES_CANNOT_SAVE_DATA = 515
messages[ES_CANNOT_SAVE_DATA] = 'Error while saving data.'

ES_CANNOT_FETCH_DATA = 516
messages[ES_CANNOT_FETCH_DATA] = 'Error while fetching data from insurer.'

INTEGRATION_ERRORS = {
    'INSURER_EXPIRED_POLICY_RESTRICTED': 'expired policy not allowed',
    'VEHICLE_MAPPING_NOT_FOUND': 'vehicle mapping not found',
    'INSURER_NO_IDV': 'no idv present',
    'COVERFOX_MAKE_RESTRICTED': 'make restricted',
    'INSURER_AGE_RESTRICTED': 'age restricted',
    'RTO_MAPPING_NOT_FOUND': 'rto not found',
    'UNKNOWN_ERROR': 'this is really a bug',
    'COVERFOX_DISCOUNT_LIMIT_EXCEEDED': 'discount limit over',
    'INSURER_ZERO_PREMIUM': 'zero premium',
    'INSURER_USED_VEHICLE_RESTRICTED': 'vehicle not allowed',
    'PREMIUM_TIMEOUT': 'timout',
    'INSURER_NEW_VEHICLE_RESTRICTED': 'new vehicle not allowed',
    'COVERFOX_TP_RESTRICTED': 'third party restricted',
    'COVERFOX_DECLINED_MODEL': 'declined model',
    'IDV_LIMIT_EXCEEDED': 'idv limit over',
    'INSURER_MAKE_RESTRICTED': 'make restricted',
    'INSURER_ADD_ONS_RESTRICTED': 'add on restricted',
    'NCB_LIMIT_EXCEEDED': 'ncb limit over',
    'INSURER_PREMIUM_INVALID_RESPONSE': 'premium response not valid',
    'INSURER_NEW_POLICY_START_DATE_RESTRICTED': 'start date not allowed',

}

messages.update(INTEGRATION_ERRORS)





PAYMENT_RETRY = {
    'transaction-is-already-done': {
        'error_code': 1001,
        'message': 'Transaction is already done',
        'description': 'Hey {customer_first_name}, the transaction you are trying to make payment for, is already completed.<br/>If you have not received your policy document yet, please email our Customer Service Unit at <a href="mailto:support@coverfox.com">support@coverfox.com</a> or call us on the Service Toll Free <strong>18002099970</strong>.'
    },
    'redirection-failed': {
        'error_code': 1002,
        'message': '{insurer_title} services are down at the moment',
        'description': 'Hey {customer_first_name}, there seems to be some problem connecting to the {insurer_title} systems for generating your policy. We apologize for this inconvenience.<br/><br/>You could also proceed by selecting another insurer. All the information you have entered will be saved.<br/><br/>Our Sales Team has been notified about the issue and someone will call you shortly to assist you with the payment.<br/><br/>If you wish to contact us, please call us on our Toll Free Number <strong>18002099940</strong>.'
    },
    'payment-failed': {
        'error_code': 1003,
        'message': 'Payment Failed',
        'description': 'Hey {customer_first_name}, there was a problem in processing the payment.<br/><br/>You can proceed to pay again and complete the transaction. It is possible that your account was debited with Rs. {payment_amount}, in which case, please do not make the payment again.<br/><br/>Our Sales Team has been notified about the issue and someone will call you shortly to assist you with the payment.<br/><br/>If you wish to contact us, please call us on our Toll Free Number <strong>18002099940</strong>.'
    },
    'insurer-services-are-down-at-the-moment': {
        'error_code': 1004,
        'message': '{insurer_title} services are down at the moment',
        'description': 'Hey {customer_first_name}, it appears that the insurer you selected is currently unavailable. This could be temporary and you can click on the RETRY button to try again.<br/><br/>You could also proceed by selecting another insurer. All the information you have entered will be saved.<br/><br/>Our Sales Team has been notified about the issue and someone will call you shortly to assist you with the purchase.<br/><br/>If you wish to contact us, please call us on our Toll Free Number <strong>18002099940</strong>.'
    },
    'renewal-with-same-insurer-not-allowed': {
        'error_code': 1005,
        'message': 'Renewal with same insurer not allowed',
        'description': 'Hey {customer_first_name}, we are sorry. You have an existing policy from {insurer_title} and currently they do not allow to renew in such cases on Coverfox.<br/><br/>You could proceed by selecting another insurer. All the information you have entered will be saved.<br/><br/>Our Sales Team has been notified about the issue and someone will call you shortly to assist you with the purchase.<br/><br/>If you wish to contact us, please call us on our Toll Free Number <strong>18002099940</strong>.'
    }
}
