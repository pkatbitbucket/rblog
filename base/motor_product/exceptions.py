from django.core.exceptions import ValidationError


class BaseValidationError(ValidationError):
    def __init__(self, code=None, params=None):
        super(BaseValidationError, self).__init__(self.message, code, params)


class NoInspectionNumber(BaseValidationError):
    message = {
        'number': 'Can not pass inspection without inspection number'
    }

class NoMatch(Exception):
    message = 'No match found'


class ReadOnlyError(ValueError):
    message = 'Can not update transaction once it is marked complete'
