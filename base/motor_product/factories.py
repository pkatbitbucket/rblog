import random
import factory

from motor_product import models as motor_models

quote_parameters = {
    'isUsedVehicle': '0',
    'isNewVehicle': '0',
    'vehicleId': '1026',
    'registrationNumber[]': ['MH', '01', 'HH', '8888'],
    'voluntaryDeductible': '0',
    'manufacturingDate': '01-01-2012',
    'registrationDate': '01-01-2012',
    'isClaimedLastYear': '0',
    'previousNCB': '20',
    'quoteId': '',
    'idv': '0',
    'minIdv': '0',
    'maxIdv': '0',
    'idvElectrical': '0',
    'idvNonElectrical': '0',
    'cngKitValue': '0',
    'isCNGFitted': '0',
    'addon_is247RoadsideAssistance': '0',
    'addon_isDepreciationWaiver': '0',
    'addon_isNcbProtection': '0',
    'addon_isInvoiceCover': '0',
    'addon_isKeyReplacement': '0',
    'addon_isAntiTheftFitted': '0',
    'addon_isDriveThroughProtected': '0',
    'addon_isEngineProtector': '0',
    'pastPolicyExpiryDate': '08-01-2015'
}


class InsurerFactory(factory.django.DjangoModelFactory):
    title = factory.Sequence(lambda n: 'Insurer {0}'.format(n))
    slug = factory.Sequence(lambda n: 'insurer-slug-{0}'.format(n))
    body = factory.Sequence(lambda n: 'Insurer Description {0}'.format(n))
    tease = factory.Sequence(lambda n: 'Insurer Tease Text {0}'.format(n))
    status = [choice[0] for choice in motor_models.Insurer.STATUS_CHOICES]

    class Meta:
        model = motor_models.Insurer
        abstract = False


class MakeFactory(factory.django.DjangoModelFactory):
    name = factory.Sequence(lambda n: 'Make {0}'.format(n))

    class Meta:
        model = motor_models.Make
        abstract = False


FUEL_TYPES = [fuel_type[0] for fuel_type in motor_models.VehicleMaster.FUEL_CHOICES]
VEHICLE_SEGMENTS = [segment[0] for segment in motor_models.VehicleMaster.VEHICLE_SEGMENT]
VEHICLE_TYPE_CHOICES = [vehicle_type[0] for vehicle_type in motor_models.VEHICLE_TYPE_CHOICES]


class ModelFactory(factory.django.DjangoModelFactory):
    make = factory.SubFactory(MakeFactory)
    name = factory.Sequence(lambda n: 'Model {0}'.format(n))
    vehicle_type = factory.Iterator(VEHICLE_TYPE_CHOICES)

    class Meta:
        model = motor_models.Model
        abstract = False


class VehicleMasterFactory(factory.django.DjangoModelFactory):
    make = factory.SubFactory(MakeFactory)
    model = factory.SubFactory(ModelFactory)
    variant = factory.Sequence(lambda n: 'Variant {0}'.format(n))
    cc = 25 * random.randint(4, 40)
    seating_capacity = random.randint(4, 10)
    ex_showroom_price = factory.lazy_attribute(
        lambda o: 100000 * random.randint(1, 50)
    )
    fuel_type = factory.Iterator(
        FUEL_TYPES
    )
    vehicle_segment = factory.Iterator(
        VEHICLE_SEGMENTS
    )
    vehicle_type = factory.Iterator(
        VEHICLE_TYPE_CHOICES
    )

    class Meta:
        model = motor_models.VehicleMaster
        abstract = False


class VehicleFactory(factory.django.DjangoModelFactory):
    insurer = factory.SubFactory(InsurerFactory)
    vehicle_code = factory.Sequence(lambda n: 'vehicle-code-{0}'.format(n))
    name = factory.Sequence(lambda n: 'Vehicle Name {0}'.format(n))
    make = factory.Sequence(lambda n: 'Vehicle Make {0}'.format(n))
    make_code = factory.Sequence(lambda n: 'VEHICLE-MAKE-CODE-{0}'.format(n))
    model = factory.Sequence(lambda n: 'Vehicle Model {0}'.format(n))
    model_code = factory.Sequence(lambda n: 'VEHICLE-MODEL-CODE-{0}'.format(n))
    variant = factory.Sequence(lambda n: 'VEHICLE-VARIANT-{0}'.format(n))
    variant_code = factory.Sequence(lambda n: 'VEHICLE-VARIANT-CODE-{0}'.format(n))
    cc = factory.lazy_attribute(lambda o: 25 * random.randint(4, 40))
    seating_capacity = factory.lazy_attribute(lambda o: random.randint(4, 10))
    fuel_type = factory.Iterator(
        [choice[0] for choice in motor_models.Vehicle.FUEL_CHOICES]
    )
    vehicle_segment = factory.Iterator(
        [segment[0] for segment in motor_models.Vehicle.VEHICLE_SEGMENT]
    )
    vehicle_type = factory.Iterator(
        [choice[0] for choice in motor_models.VEHICLE_TYPE_CHOICES]
    )
    ex_showroom_price = factory.lazy_attribute(
        lambda o: 100000 * random.randint(1, 50)
    )
    raw_data = {}

    class Meta:
        model = motor_models.Vehicle
        abstract = False


class VehicleMappingFactory(factory.django.DjangoModelFactory):

    master_vehicle = factory.SubFactory(VehicleMasterFactory)
    mapped_vehicle = factory.SubFactory(VehicleFactory)
    insurer = factory.SubFactory(InsurerFactory)

    class Meta:
        model = motor_models.VehicleMapping
        abstract = False


class QuoteFactory(factory.django.DjangoModelFactory):
    vehicle = factory.SubFactory(VehicleMasterFactory)
    raw_data = quote_parameters

    class Meta:
        model = motor_models.Quote
        abstract = False
