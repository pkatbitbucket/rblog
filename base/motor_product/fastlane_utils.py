from fuzzywuzzy import fuzz

from motor_product import exceptions as motor_exceptions
from motor_product import models as motor_models


class FastlaneVehicleMasterSearch(object):

    def __init__(self, registration_detail):
        self._set_attributes(registration_detail)

    def _set_attributes(self, registration_detail):
        self.registration_detail = registration_detail

        vehicle_details = self.registration_detail.get_fastlane_vehicle_info()
        self.make = vehicle_details.get('fla_maker_desc', '').strip()
        self.model = vehicle_details.get('fla_model_desc', '').strip()

        variant = vehicle_details.get('fla_variant', '').strip()
        if variant and type(variant) == str:
            self.variant = vehicle_details.get('fla_variant', '').strip()
        else:
            self.variant = ''

        self.fuel_type = motor_models.NewRegistrationDetail.FUEL_TYPE_MAP.get(
            vehicle_details.get('fla_fuel_type_desc', '').strip().upper(), ''
        )

    def get_make(self):
        return motor_models.Make.objects.filter(
            name__iexact=self.make
        ).first()

    def get_exact_vehicle_master(self):
        if self.get_make() and self.fuel_type and self.model and self.variant:
            return motor_models.VehicleMaster.objects.filter(
                make=self.get_make(),
                fuel_type=self.fuel_type,
                model__name__iexact=self.model,
                variant__iexact=self.variant,
            ).order_by('-id').last()

    def get_alternative_vehicle_master(self):
        alternatives_with_score = []

        make_object = self.get_make()

        filters = {}
        if self.fuel_type:
            filters['fuel_type'] = self.fuel_type

        if make_object:
            filters['make'] = make_object

        if filters:
            vehicle_masters = motor_models.VehicleMaster.objects.filter(**filters)
        else:
            return None

        # calculate token set ratio
        for vehicle_master in vehicle_masters.iterator():
            alternatives_with_score.append(
                (
                    fuzz.token_set_ratio(
                        self.registration_detail.fastlane_search_key(),
                        vehicle_master.fastlane_key()
                    ),
                    vehicle_master
                )
            )

        if not alternatives_with_score:
            return None

        # since score is the first key of the tuple, we can simple sort it
        alternatives_with_score = sorted(alternatives_with_score, reverse=True)

        # print 'sorted alternatives_with_score', alternatives_with_score[:3]
        top_3 = alternatives_with_score[:10]
        top_ones = [a for a in alternatives_with_score if a[0] == top_3[0][0]]

        if len(top_ones) > 1:
            # top_ones = [(vm[1].quote_set.count(), vm[1]) for vm in top_ones]
            top_ones = [
                (
                    fuzz.token_sort_ratio(
                        '%s %s %s %s' % (self.make, self.fuel_type, self.model, self.variant),
                        str(vm[1])
                    ),
                    vm[1]
                )
                for vm in top_ones
            ]
            top_ones = sorted(top_ones, reverse=True)
            return top_ones[0][1]

        return alternatives_with_score[0][1]

    def vehicle_master(self):
        if 'result' not in self.registration_detail.fastlane['response']:
                return None, False

        vm = self.get_exact_vehicle_master()
        if vm:
            return vm, True

        return self.get_alternative_vehicle_master(), False
