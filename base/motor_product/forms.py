import datetime
import re

from django import forms
from django.db.models import Q
from django.db.utils import IntegrityError
from django.utils import timezone
from fuzzywuzzy import fuzz
from jsonfield.fields import JSONFormField

from motor_product import models as motor_models
from motor_product import prod_utils as motor_prod_utils
from core import forms as core_forms
from wiki import models as wiki_models

from .exceptions import NoMatch


import putils
import utils


class JSONFormFieldBase(object):
    def __init__(self, *args, **kwargs):
        self.load_kwargs = kwargs.pop('load_kwargs', {})
        super(JSONFormFieldBase, self).__init__(*args, **kwargs)


class JSONFormField(JSONFormFieldBase, JSONFormField):
    pass


class MakeForm(forms.ModelForm):
    class Meta:
        model = motor_models.Make
        fields = ('name', )


class FastLaneForm(forms.Form):
    FUEL_TYPE_MAP = {
        'PETROL': 'PETROL',
        'PETROL PETROL': 'PETROL',

        'DIESEL': 'DIESEL',
        'DIESEL DIESEL': 'DIESEL',
        'DEISEL': 'DIESEL',

        'LPG ONLY': 'INTERNAL_LPG_CNG',
        'LPG': 'INTERNAL_LPG_CNG',
        'PETLPG': 'INTERNAL_LPG_CNG',
        'PETROL/LPG': 'INTERNAL_LPG_CNG',
        'PETROL/CNG': 'INTERNAL_LPG_CNG',
        'PETCNG': 'INTERNAL_LPG_CNG',
        'RCNG/P': 'INTERNAL_LPG_CNG',
    }
    FUEL_TYPE_CHOICES = FUEL_TYPE_MAP.items()

    make = MakeForm.base_fields['name']
    model_variant = forms.CharField()
    fuel_type = forms.CharField()
    previous_insurer = forms.CharField()
    registration_date = forms.DateField(input_formats=('%d/%m/%Y', ))
    insurance_date = forms.DateField(input_formats=('%d/%m/%Y', ))
    pincode = forms.CharField(max_length=6)
    financier = forms.CharField()

    def clean_make(self):
        make = motor_models.Make.objects.filter(name__iexact=self.cleaned_data['make']).first()
        if make:
            return make
        else:
            raise forms.ValidationError("No match found for make: " + self.cleaned_data['make'])

    def clean_fuel_type(self):
        try:
            return self.FUEL_TYPE_MAP[self.cleaned_data['fuel_type'].strip().upper()]
        except KeyError:
            raise forms.ValidationError("Invalid fuel_type: " + self.cleaned_data['fuel_type'])

    def clean_previous_insurer(self):
        try:
            previous_insurer = self.cleaned_data['previous_insurer']
            return self.fuzzy_search(
                keyword=previous_insurer,
                items=[[insurer.title.upper(), insurer] for insurer in motor_models.Insurer.objects.all()],
            )
        except NoMatch:
            raise forms.ValidationError("No match found for insurer: " + previous_insurer)

    def clean_pincode(self):
        pincode = self.cleaned_data['pincode']
        try:
            self.cleaned_data['pincode_object'] = wiki_models.Pincode.objects.get(pincode=pincode)
        except wiki_models.Pincode.DoesNotExist:
            utils.motor_logger.error('Details for pincode %s does not exist' % (pincode, ))
        # previous_insurer = self.cleaned_data['previous_insurer']
        # insurers = [[insurer.title.upper(), insurer] for insurer in Insurer.objects.all()]
        # try:
        #     insurer_tuple = sorted(  # step 3: sort insurers on ratio ranking and pick the first one
        #         filter(  # step 2: remove insurers with ratio less than 70
        #             lambda i: i[2] >= 70, map(  # step 1: attach fuzz ratio to every insurer
        #                 lambda x: x + [fuzz.ratio(previous_insurer, x[0])], insurers
        #             )
        #         ), key=lambda k: k[2], reverse=True
        #     )[0]
        #     return insurer_tuple[1]
        # except IndexError:
        #     raise forms.ValidationError("No match found for insurer: " + previous_insurer)

    def fuzzy_search(self, keyword, items, minimum_ratio=70):
        try:
            insurer_tuple = sorted(  # step 3: sort items on ratio ranking and pick the first one
                filter(  # step 2: remove items with ratio less than minimum_ratio
                    lambda item: item[-1] >= minimum_ratio, map(  # step 1: attach fuzz ratio to every item
                        lambda item: item + [fuzz.ratio(keyword, item[0])], items
                    )
                ), key=lambda item: item[-1], reverse=True
            )[0]
            return insurer_tuple[1]
        except IndexError:
            raise NoMatch

    def get_vehicle_master(self):
        make, fuel_type, model_variant = self.cleaned_data['make'], self.cleaned_data['fuel_type'], self.cleaned_data['model_variant']
        INVALID_MAKES = [
            'None', "",
            'ENFIELD', 'UNSPECIFIED', 'GMC', 'HINDUSTAN',
        ]
        if make in INVALID_MAKES:
            raise NoMatch
        makes = motor_models.Make.objects.filter(name__iexact=make)
        if not makes:
            raise NoMatch
        else:
            make = makes[0]

        universe_variants = [
            (vm.model.name, vm.variant, vm.id) for vm in motor_models.VehicleMaster.objects.filter(
                make=make, fuel_type=fuel_type, vehicle_type=self.mapped_vehicle_type
            )
        ]
        if universe_variants:
            model_variant = model_variant.replace(make.name, "").upper()
            pk = sorted(universe_variants, key=lambda x: fuzz.ratio(model_variant, " ".join(x[:2]).upper()), reverse=True)[0][2]
            return motor_models.VehicleMaster.objects.get(pk=pk)
        else:
            raise NoMatch

    def clean(self):
        cd = self.cleaned_data
        cd['vehicle_master']


class InspectionSearchForm(forms.Form):
    STATUS_CHOICES = [('', '-----------')] + [sc for sc in motor_models.Inspection.STATUS_CHOICES if sc[0] != motor_models.Inspection.RESCHEDULED]
    scheduled_on = forms.DateField(input_formats=['%d-%m-%Y'], required=False)
    status = forms.ChoiceField(choices=STATUS_CHOICES, required=False)
    agent = forms.ModelChoiceField(
        queryset=motor_models.InspectionAgent.objects.filter(user__is_active=True),
        required=False
    )

    def query_scheduled_on(self, q):
        d = self.cleaned_data
        if d.get('scheduled_on'):
            return q & Q(scheduled_on=d['scheduled_on'])
        else:
            return q

    def query_status(self, q):
        d = self.cleaned_data
        if d.get('status'):
            return q & Q(status=d['status'])
        else:
            return q

    def query_agent(self, q):
        d = self.cleaned_data
        if d.get('agent'):
            return q & Q(agent=d['agent'])
        else:
            return q

    def query(self, scheduled=True):
        if scheduled:
            # For Coverfox inspection approvers
            q = Q(schedule_slot__isnull=False)
        else:
            # For Non-Coverfox inspection approvers
            q = Q(schedule_slot__isnull=True)
        for field, _ in self.fields.items():
            if hasattr(self, "query_%s" % field):
                q = getattr(self, "query_%s" % field)(q)
        return q


class MotorInspectionForm(forms.Form):
    # Number of hours before which you cannot book a slot
    IMMEDIATE_SLOT_AVAILABILITY = 2
    # number of days before which you can book for a day
    BOOKING_OPEN_SINCE = 3
    MAX_BOOKING_OPEN_SINCE = 5

    number_of_days = forms.IntegerField(required=False)
    area = forms.ModelChoiceField(
        queryset=motor_models.InspectionArea.objects.all(),
        required=False
    )
    city = forms.ModelChoiceField(queryset=wiki_models.City.objects.all(), required=False)
    date_from = forms.DateField(input_formats=['%d-%m-%Y'], required=False)
    scheduled_on = forms.DateField(input_formats=['%d-%m-%Y'], required=False)
    schedule_slot = forms.ModelChoiceField(
        motor_models.DailyTimeSlot.objects.filter(is_active=True),
        required=False
    )
    transaction = forms.CharField()
    address = forms.CharField(required=False)

    def __init__(self, request, *args, **kwargs):
        super(MotorInspectionForm, self).__init__(*args, **kwargs)
        self.request = request
        self.date_to = None
        self.agents = []
        self.holidays = []
        self.available_slots = []
        self.slot_in_available_slots = []

    def clean_transaction(self):
        transaction = motor_models.Transaction.objects.get(transaction_id=self.cleaned_data['transaction'])
        pending_inspection = transaction.inspection_set.filter(
            ~Q(status__in=[motor_models.Inspection.CANCELLED, motor_models.Inspection.RESCHEDULED])
        ).order_by('-scheduled_on')

        if pending_inspection:
            pending_inspection = pending_inspection[0]
            if pending_inspection.schedule_slot:
                raise forms.ValidationError(
                    'Inspection has been scheduled for %s at %s-%s' % (
                        pending_inspection.scheduled_on.strftime("%d/%m/%Y"),
                        pending_inspection.schedule_slot.start.strftime("%I:%M %p"),
                        pending_inspection.schedule_slot.end.strftime("%I:%M %p"),
                    )
                )
            else:
                raise forms.ValidationError("Inspection had already been scheduled")
        return transaction

    def clean_number_of_days(self):
        d = self.cleaned_data
        if not d.get('number_of_days'):
            return self.BOOKING_OPEN_SINCE
        else:
            return d['number_of_days']

    def clean_date_from(self):
        d = self.cleaned_data
        if not d.get('date_from'):
            d['date_from'] = datetime.datetime.now().date()
        self.date_to = d['date_from'] + datetime.timedelta(days=d['number_of_days'])

        self.holidays = [l.date for l in motor_models.Leave.objects.filter(
            date__gte=d['date_from'],
            date__lte=self.date_to,
            agents__isnull=True
        )]
        return d['date_from']

    def clean_scheduled_on(self):
        d = self.cleaned_data
        if d.get('scheduled_on') and d['scheduled_on'] > datetime.datetime.now().date() + datetime.timedelta(days=self.MAX_BOOKING_OPEN_SINCE):
            raise forms.ValidationError('Booking can only be scheduled %s days in advance' % self.MAX_BOOKING_OPEN_SINCE)
        else:
            return d.get('scheduled_on')

    def clean_area(self):
        d = self.cleaned_data
        self.agents = motor_models.InspectionAgent.objects.filter(area=d['area'], user__is_active=True)
        if not self.agents and self.request.GET:
            raise forms.ValidationError("""Our team will get in touch with you shortly to help you
            schedule an inspection for your vehicle. Kindly confirm the address and we will get in
            touch with you.
            """)
        return d['area']

    def clean(self):
        d = self.cleaned_data

        if self.errors:
            return d

        self.get_available_slots()
        if d.get('schedule_slot'):
            self.slot_in_available_slots = filter(
                lambda x:
                    x['slot'] == d['schedule_slot'] and
                    x['date'] == d['scheduled_on'] and
                    x['available'],
                    self.available_slots
            )
            if not self.slot_in_available_slots:
                raise forms.ValidationError('Slot is not available for booking anymore')
        return d

    def get(self):
        return {
            "available_slots": [{
                'slot': aslot['slot'].to_json(),
                'date': aslot['date'].strftime("%d-%m-%Y"),
                'available': aslot['available'],
            } for aslot in self.available_slots]
        }

    def get_available_slots(self):
        d = self.cleaned_data
        day = d['date_from']
        upper_date_limit = datetime.datetime.now().date() + datetime.timedelta(days=self.MAX_BOOKING_OPEN_SINCE)
        while day <= self.date_to and day <= upper_date_limit:
            daily_slot = {
                'date': day,
                'slots': [],
                'active_slots': False
            }
            try:
                agents_on_leave = []
                for l in motor_models.Leave.objects.filter(
                    date=day,
                    agents__isnull=False,
                    agents__area=d['area'],
                    agents__user__is_active=True
                ):
                    agents_on_leave.extend(list(l.agents.all()))
            except motor_models.Leave.DoesNotExist:
                pass

            holiday = False
            if day in self.holidays:
                holiday = True
            for slot in motor_models.DailyTimeSlot.objects.filter(is_active=True):
                slot_availability = {'slot': slot, 'date': day, 'available': True, 'agents': []}
                daily_slot['slots'].append(slot_availability)
                # slot not available if its a holiday
                if holiday:
                    slot_availability['available'] = False
                    continue

                # slot not available if its less than IMMEDIATE_SLOT_AVAILABILITY hours after now
                slot_for_day = datetime.datetime.combine(day, slot.start)
                if slot_for_day < datetime.datetime.now() + datetime.timedelta(hours=self.IMMEDIATE_SLOT_AVAILABILITY):
                    slot_availability['available'] = False
                    continue

                agents_booked = [ins.agent for ins in motor_models.Inspection.objects.filter(
                    schedule_slot=slot,
                    agent__area=d['area'],
                    agent__user__is_active=True,
                    status__in=[motor_models.Inspection.PENDING],
                    scheduled_on__range=(
                        datetime.datetime.combine(day, datetime.time.min),
                        datetime.datetime.combine(day, datetime.time.max))
                )]
                agents_on_weekly_off = motor_models.InspectionAgent.objects.filter(
                    weekly_off__contains=[slot_for_day.weekday()],
                    user__is_active=True,
                    area=d['area']
                )

                agents_not_available = set(agents_booked).union(agents_on_weekly_off).union(agents_on_leave)
                if len(agents_not_available) >= len(self.agents):
                    slot_availability['available'] = False
                    continue
                else:
                    agents_available = set(self.agents) - set(agents_not_available)
                    slot_availability['agents'] = list(set(agents_available))

                if slot_availability['available']:
                    daily_slot['active_slots'] = True

            if daily_slot['active_slots']:
                self.available_slots.extend(daily_slot['slots'])
            else:
                self.date_to += datetime.timedelta(days=1)
            day += datetime.timedelta(days=1)

    def save(self, request):
        d = self.cleaned_data
        transaction = motor_models.Transaction.objects.get(transaction_id=d['transaction'])
        if self.slot_in_available_slots and d.get('area'):
            agent = self.slot_in_available_slots[0]['agents'][0]
            inspection_agency = motor_models.InspectionAgency.objects.get(company__slug='coverfox')
            agent_city = agent.area.filter(is_enabled=True).first().city
        else:
            agent = None
            inspection_agency = None
            agent_city = None
        # If address is in POST, take it else make one from
        # transaction
        inspection_reschedule = transaction.inspection_set.filter(
            Q(status=motor_models.Inspection.RESCHEDULED)
        ).order_by('-scheduled_on')
        if inspection_reschedule:
            inspection = inspection_reschedule[0]
            inspection.address = d.get('address', '')
            inspection.agent = agent
            inspection.city = d.get('city', agent_city)
            inspection.area = d.get('area', None)
            inspection.scheduled_on = d.get('scheduled_on', None)
            inspection.schedule_slot = d.get('schedule_slot', None)
            inspection.inspection_agency = inspection_agency
            inspection.status = motor_models.Inspection.PENDING
        else:
            inspection = motor_models.Inspection(
                transaction=d['transaction'],
                address=d.get('address', ''),
                agent=agent,
                city=d.get('city', agent_city),
                area=d.get('area', None),
                scheduled_on=d.get('scheduled_on', None),
                schedule_slot=d.get('schedule_slot', None),
                inspection_agency=inspection_agency,
            )

        inspection.save(user=request.user)

        if d.get('scheduled_on'):
            return (inspection, 'Inspection has been scheduled for %s at %s-%s' % (
                inspection.scheduled_on.strftime("%d/%m/%Y"),
                inspection.schedule_slot.start.strftime("%I:%M %p"),
                inspection.schedule_slot.end.strftime("%I:%M %p"),
            ))
        else:
            return (inspection, """Our team will call you soon to schedule an inspection
            for your vehicle""")


class RegistrationDetailCreateForm(forms.ModelForm):
    transaction = forms.ModelChoiceField(
        queryset=motor_models.Transaction.objects.all(),
        to_field_name='transaction_id',
        required=False
    )
    coverfox = JSONFormField(initial={}, required=False)
    name = forms.CharField(label='Name', max_length=32)

    def is_valid(self):
        name = motor_prod_utils.clean_registration_number(self.data['name'])
        self.instance.registration_number = name
        return super(RegistrationDetailCreateForm, self).is_valid()

    def save(self):
        try:
            self.object = super(RegistrationDetailCreateForm, self).save()
        except IntegrityError:
            self.object = self._meta.model.objects.get(registration_number=self.instance.registration_number)
        transaction = self.cleaned_data.get('transaction')
        if transaction:
            self.object.transaction_set.add(self.cleaned_data.get('transaction'))

        coverfox = self.object.data.get('coverfox', {})
        coverfox.update(self.cleaned_data.get('coverfox') or {})
        self.object.data['coverfox'] = coverfox
        self.object.save()

        return self.object

    class Meta:
        model = motor_models.NewRegistrationDetail
        exclude = ['registration_number', 'data']


class InspectionReportForm(forms.ModelForm):
    part = forms.CharField(widget=forms.TextInput(attrs={'readonly': 'True'}))

    class Meta:
        model = motor_models.InspectionReport
        fields = ('part', 'status', 'inspection')


class InspectionDocumentForm(forms.ModelForm):

    class Meta:
        model = motor_models.MotorInspectionDocument
        fields = ('document_type', 'document')

    def clean(self):
        document = self.cleaned_data.get("document", None)
        if not document:
            raise forms.ValidationError('Document is not attached')


class CustomBooleanField(forms.BooleanField):
    def __init__(self, required=False, *args, **kwargs):
        super(CustomBooleanField, self).__init__(required=required, *args, **kwargs)

    def to_python(self, value):
        return '1' if super(CustomBooleanField, self).to_python(value) else '0'


class CustomDateInput(forms.widgets.DateInput):
    def __init__(self, attrs=None, *args, **kwargs):
        default_attrs = {'class': 'form-control dateinput'}
        if attrs is not None:
            default_attrs.update(attrs)
        super(CustomDateInput, self).__init__(default_attrs, *args, **kwargs)


class VehicleMasterChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return u'{} {} {} {}-CC {}'.format(obj.make.name, obj.model.name, obj.variant, str(obj.cc), obj.fuel_type)


class OfflineQuoteForm(forms.Form):
    ATTRS = {"class": "form-control"}

    policy_type = forms.ChoiceField(
        choices=(('', '---------'), ('NEW', 'New'), ('RENEW', 'Renew'), ('EXPIRED', 'Expired'), ('USED', 'Used')),
        widget=forms.widgets.Select(ATTRS)
    )

    vehicle = VehicleMasterChoiceField(
        motor_models. VehicleMaster.objects.none(),
        widget=forms.widgets.Select(ATTRS),
    )

    manufacturing_date = forms.DateField(
        label='Mfg/Reg date',
        initial=datetime.datetime.now(timezone.get_current_timezone()),
        widget=CustomDateInput(),
    )

    new_ncb = forms.ChoiceField(
        choices=((0, '0%'), (20, '20%'), (25, '25%'), (35, '35%'), (45, '45%'), (50, '50%')),
        widget=forms.widgets.Select(ATTRS),
    )

    new_policy_start_date = forms.DateField(
        initial=datetime.datetime.now(timezone.get_current_timezone()),
        widget=CustomDateInput(),
    )
    new_policy_end_date = forms.DateField(
        initial=putils.add_years(
            datetime.datetime.now(timezone.get_current_timezone()), 1
        ) - datetime.timedelta(days=1),
        widget=CustomDateInput(),
    )

    payment_mode = forms.ChoiceField(
        choices=(
            ('PAYMENT_GATEWAY', 'Payment Gateway'),
            ('CARD_SWIPE', 'Card Swipe'),
            ('CHEQUE', 'Cheque'),
            ('CHEQUE_CD_ACCOUNT', 'Cheque - CD Account'),
        ),
        widget=forms.widgets.Select(ATTRS)
    )

    voluntary_deductible = forms.IntegerField(initial=0, widget=forms.widgets.NumberInput(ATTRS))
    cng_kit_value = forms.IntegerField(initial=0, widget=forms.widgets.NumberInput(ATTRS))
    pa_passenger = forms.IntegerField(
        label="Passenger Cover",
        initial=0,
        widget=forms.widgets.NumberInput(ATTRS)
    )

    idv = forms.IntegerField(initial=0, widget=forms.widgets.NumberInput(ATTRS))
    idv_electrical = forms.IntegerField(
        initial=0,
        label='Electrical Accessories Value',
        widget=forms.widgets.NumberInput(ATTRS),
    )
    idv_non_electrical = forms.IntegerField(
        initial=0,
        label='Non-Electrical Accessories Value',
        widget=forms.widgets.NumberInput(ATTRS),
    )

    legal_liability = CustomBooleanField(label='Driver Cover')
    roadside_assistance = CustomBooleanField(label='24x7 Roadside Assistance')
    depreciation_waiver = CustomBooleanField(label='Zero Depreciation')
    engine_protector = CustomBooleanField()
    invoice_cover = CustomBooleanField()
    key_replacement = CustomBooleanField()
    ncb_protection = CustomBooleanField(label='NCB Protection')

    ncb_certificate = CustomBooleanField(label='NCB Certificate')

    anti_theft_fitted = CustomBooleanField()
    member_of_auto_association = CustomBooleanField()
    tppd_discount = CustomBooleanField(label='TPPD Discount')

    def __init__(self, product_type, *args, **kwargs):
        super(OfflineQuoteForm, self).__init__(*args, **kwargs)
        if product_type == 'car':
            self.fields['vehicle'].queryset = motor_models.VehicleMaster.objects.filter(vehicle_type='Private Car').order_by(
                'make__name', 'model__name', 'variant', 'cc', 'fuel_type')
        else:
            self.fields['vehicle'].queryset = motor_models.VehicleMaster.objects.filter(vehicle_type='Twowheeler').order_by(
                'make__name', 'model__name', 'variant', 'cc', 'fuel_type')

    def clean(self):
        quote_data = self.cleaned_data
        if quote_data.get('policy_type'):
            quote_data['new_vehicle'] = '0'
            quote_data['used_vehicle'] = '0'
            if quote_data['policy_type'] == 'NEW':
                quote_data['new_vehicle'] = '1'
                quote_data['past_policy_expiry_date'] = None
            elif quote_data['policy_type'] == 'RENEW':
                quote_data['past_policy_expiry_date'] = datetime.datetime.now().date()
            elif quote_data['policy_type'] == 'EXPIRED':
                quote_data['past_policy_expiry_date'] = datetime.datetime.now().date() - datetime.timedelta(days=1)
            else:
                quote_data['used_vehicle'] = '1'
                quote_data['past_policy_expiry_date'] = None

            if quote_data['cng_kit_value'] > 0:
                quote_data['cng_fitted'] = '1'
            else:
                quote_data['cng_fitted'] = '0'

        quote_data['drive_through_protected'] = quote_data['engine_protector']
        quote_data['registration_date'] = quote_data['manufacturing_date']


class OfflineProposalForm(forms.Form):
    ATTRS = {"class": "form-control"}

    insurer = forms.ModelChoiceField(
        motor_models.Insurer.objects.order_by('title'),
        widget=forms.widgets.Select(ATTRS),
    )

    first_name = forms.CharField(widget=forms.widgets.TextInput(ATTRS))
    last_name = forms.CharField(widget=forms.widgets.TextInput(ATTRS))
    email = forms.EmailField(widget=forms.widgets.EmailInput(ATTRS))
    phone = forms.IntegerField(
        max_value=9999999999,
        min_value=7000000000,
        widget=forms.widgets.NumberInput(ATTRS),
    )

    gender = forms.ChoiceField(
        choices=(('Male', 'Male'), ('Female', 'Female')),
        widget=forms.widgets.Select(ATTRS)
    )
    marital_status = forms.ChoiceField(
        choices=(('1', 'Married'), ('0', 'Unmarried')),
        widget=forms.widgets.Select(ATTRS)
    )
    dob = forms.DateField(
        label='Date of Birth',
        widget=CustomDateInput(),
    )
    nominee_name = forms.CharField(widget=forms.widgets.TextInput(ATTRS))
    nominee_age = forms.IntegerField(
        max_value=100,
        min_value=18,
        widget=forms.widgets.NumberInput(ATTRS),
    )
    nominee_relationship = forms.ChoiceField(
        choices=(('Son', 'Son'),
                 ('Daughter', 'Daughter'),
                 ('Mother', 'Mother'),
                 ('Father', 'Father'),
                 ('Wife', 'Wife'),
                 ('Husband', 'Husband'),
                 ('Brother', 'Brother'),
                 ('Sister', 'Sister'),
                 ('Other', 'Other')),
        widget=forms.widgets.Select(ATTRS),
    )

    chassis_no = forms.CharField(widget=forms.widgets.TextInput(ATTRS))
    engine_no = forms.CharField(widget=forms.widgets.TextInput(ATTRS))
    car_financed = forms.ChoiceField(
        choices=(('true', 'Yes'), ('false', 'No')),
        initial='false',
        widget=forms.widgets.Select(ATTRS)
    )
    car_financier = forms.CharField(
        required=False,
        widget=forms.widgets.TextInput(ATTRS)
    )

    reg_no = forms.CharField(
        label='Registration Number',
        widget=forms.widgets.TextInput(ATTRS),
        help_text='XX-12-X(XX)-1234',
    )

    premium_paid = forms.IntegerField(initial=0, widget=forms.widgets.NumberInput(ATTRS))
    payment_on = forms.DateField(
        required=False,
        widget=CustomDateInput(),
    )

    house_no = forms.CharField(widget=forms.widgets.TextInput(ATTRS))
    building_name = forms.CharField(widget=forms.widgets.TextInput(ATTRS))
    street_name = forms.CharField(widget=forms.widgets.TextInput(ATTRS))
    landmark = forms.CharField(widget=forms.widgets.TextInput(ATTRS))
    city = forms.ModelChoiceField(
        wiki_models.City.objects.order_by('name'),
        widget=forms.widgets.Select(ATTRS),
    )
    state = forms.ModelChoiceField(
        wiki_models.State.objects.order_by('name'),
        widget=forms.widgets.Select(ATTRS),
    )
    pincode = forms.IntegerField(
        min_value=100000,
        max_value=999999,
        widget=forms.widgets.NumberInput(ATTRS),
    )

    same_address = forms.BooleanField(
        required=False,
        help_text='Address mentioned in RC book is same as above address. Fill below only if it is different'
    )

    reg_house_no = forms.CharField(required=False, widget=forms.widgets.TextInput(ATTRS))
    reg_building_name = forms.CharField(required=False, widget=forms.widgets.TextInput(ATTRS))
    reg_street_name = forms.CharField(required=False, widget=forms.widgets.TextInput(ATTRS))
    reg_landmark = forms.CharField(required=False, widget=forms.widgets.TextInput(ATTRS))
    reg_city = forms.ModelChoiceField(
        wiki_models.City.objects.order_by('name'),
        required=False,
        widget=forms.widgets.Select(ATTRS),
    )
    reg_state = forms.ModelChoiceField(
        wiki_models.State.objects.order_by('name'),
        required=False,
        widget=forms.widgets.Select(ATTRS),
    )
    reg_pincode = forms.IntegerField(
        min_value=100000,
        max_value=999999,
        required=False,
        widget=forms.widgets.NumberInput(ATTRS),
    )

    def clean(self):
        super(OfflineProposalForm, self).clean()
        same_address = self.cleaned_data.get('same_address', False)
        reg_fields = ('reg_house_no',
                      'reg_building_name',
                      'reg_street_name',
                      'reg_landmark',
                      'reg_city',
                      'reg_state',
                      'reg_pincode')

        if not same_address:
            for item in reg_fields:
                if not self.cleaned_data.get(item):
                    self.add_error(item,
                                   'This field is required if RC book address is different from communication address')

        city = self.cleaned_data.get('city')
        state = self.cleaned_data.get('state')
        if city and state:
            if city.state != state:
                self.add_error('city',
                               u'{} is not in the {}'.format(city.name, state.name))

        reg_city = self.cleaned_data.get('reg_city')
        reg_state = self.cleaned_data.get('reg_state')
        if reg_city and reg_state:
            if reg_city.state != reg_state:
                self.add_error('city',
                               u'{} is not in the {}'.format(reg_city.name, reg_state.name))

        reg_no = self.cleaned_data.get('reg_no')
        if reg_no:
            self.cleaned_data['reg_no'] = reg_no.upper()

        car_financed = self.cleaned_data.get('car_financed', 'false')
        car_financier = self.cleaned_data.get('car_financier')
        if car_financed == 'true' and not car_financier:
            self.add_error('car_financier',
                           u'This field is required if car is financed')

        return self.cleaned_data

    def clean_reg_no(self):
        reg_no = self.cleaned_data.get('reg_no', '').upper().strip()
        reg_no_re = re.compile(r"^(?P<rto_code>.+)-[a-zA-Z0-9]{1,3}-[0-9]{4}$")
        match = reg_no_re.match(reg_no)
        if match:
            rto_code = match.groupdict().get('rto_code', '')
            try:
                motor_models.RTOMaster.objects.get(rto_code__iexact=rto_code)
            except motor_models.RTOMaster.DoesNotExist:
                raise forms.ValidationError(u"{} is not a valid RTO".format(rto_code))
            else:
                return reg_no

        raise forms.ValidationError('Enter a valid registration number')


class InspectionRescheduleForm(forms.Form):
    RESCHEDULE_REASONS = list(motor_models.InspectionStateChange.RESCHEDULE_REASONS)
    reason = forms.ChoiceField(choices=RESCHEDULE_REASONS, required=True)
    comment = forms.CharField(max_length=100, required=False)


class InspectionCancelForm(forms.Form):
    CANCEL_REASONS = list(motor_models.InspectionStateChange.CANCEL_REASONS)
    reason = forms.ChoiceField(choices=CANCEL_REASONS, required=True)
    comment = forms.CharField(max_length=100, required=False)


class InspectionCallBackForm(forms.ModelForm):
    # call_time = forms.DateTimeField(input_formats=['%Y/%m/%d %H:%M'])
    call_time = forms.DateTimeField(widget=forms.widgets.DateTimeInput(attrs={'class': 'callback_time'}),
                                    input_formats=['%Y/%m/%d %H:%M'])

    class Meta:
        model = motor_models.InspectionCallback
        fields = ('call_time',)


class MotorFDForm(core_forms.FDForm):
    first_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)
    mobile = forms.CharField(required=False)
    email = forms.CharField(required=False)
    gender = forms.CharField(required=False)
    maritial_status = forms.CharField(required=False)
    birth_date = forms.CharField(required=False)

    car_registration_number = forms.CharField(required=False)
    car_engine_number = forms.CharField(required=False)
    car_chassis_number = forms.CharField(required=False)
    car_loan_provider = forms.CharField(required=False)
    car_vehicle_master = forms.CharField(required=False)
    car_vehicle_registration_date = forms.CharField(required=False)
    car_past_policy_number = forms.CharField(required=False)
    car_past_policy_start_date = forms.CharField(required=False)
    car_past_policy_end_date = forms.CharField(required=False)
    car_past_policy_cover_type = forms.CharField(required=False)
    car_past_policy_insurer = forms.CharField(required=False)
    car_policy_insurer_city = forms.CharField(required=False)

    bike_registration_number = forms.CharField(required=False)
    bike_engine_number = forms.CharField(required=False)
    bike_chassis_number = forms.CharField(required=False)
    bike_loan_provider = forms.CharField(required=False)
    bike_vehicle_master = forms.CharField(required=False)
    bike_vehicle_registration_date = forms.CharField(required=False)
    bike_past_policy_number = forms.CharField(required=False)
    bike_past_policy_start_date = forms.CharField(required=False)
    bike_past_policy_end_date = forms.CharField(required=False)
    bike_past_policy_cover_type = forms.CharField(required=False)
    bike_past_policy_insurer = forms.CharField(required=False)
    bike_policy_insurer_city = forms.CharField(required=False)

    comm_address_line1 = forms.CharField(required=False)
    comm_address_line2 = forms.CharField(required=False)
    comm_address_line3 = forms.CharField(required=False)
    comm_landmark = forms.CharField(required=False)
    comm_city = forms.CharField(required=False)
    comm_state = forms.CharField(required=False)
    comm_pincode = forms.CharField(required=False)

    reg_address_line1 = forms.CharField(required=False)
    reg_address_line2 = forms.CharField(required=False)
    reg_address_line3 = forms.CharField(required=False)
    reg_landmark = forms.CharField(required=False)
    reg_city = forms.CharField(required=False)
    reg_state = forms.CharField(required=False)
    reg_pincode = forms.CharField(required=False)

    class Config:
        fields = [
            core_forms.Person,
            core_forms.CarDetail(
                tag="car",
                registration_number="car_registration_number",
                engine_number="car_engine_number",
                chassis_number="car_chassis_number",
                loan_provider="car_loan_provider",
                vehicle_master="car_vehicle_master",
                vehicle_registration_date="car_vehicle_registration_date",
                past_policy_number="car_past_policy_number",
                past_policy_start_date="car_past_policy_start_date",
                past_policy_end_date="car_past_policy_end_date",
                past_policy_cover_type="car_past_policy_cover_type",
                past_policy_insurer="car_past_policy_insurer",
                policy_insurer_city="car_policy_insurer_city"

            ),
            core_forms.BikeDetail(
                tag="bike",
                registration_number="bike_registration_number",
                engine_number="bike_engine_number",
                chassis_number="bike_chassis_number",
                loan_provider="bike_loan_provider",
                vehicle_master="bike_vehicle_master",
                vehicle_registration_date="bike_vehicle_registration_date",
                past_policy_number="bike_past_policy_number",
                past_policy_start_date="bike_past_policy_start_date",
                past_policy_end_date="bike_past_policy_end_date",
                past_policy_cover_type="bike_past_policy_cover_type",
                past_policy_insurer="bike_past_policy_insurer",
                policy_insurer_city="bike_policy_insurer_city"

            ),
            core_forms.Address(
                tag='communication',
                address_line1="comm_address_line1",
                address_line2="comm_address_line2",
                address_line3="comm_address_line3",
                pincode="comm_pincode",
                landmark="comm_landmark",
                city="comm_city",
                state="comm_state",
            ),
            core_forms.Address(
                tag='home',
                address_line1="reg_address_line1",
                address_line2="reg_address_line2",
                address_line3="reg_address_line3",
                pincode="reg_pincode",
                landmark="reg_landmark",
                city="reg_city",
                state="reg_state",
            ),
        ]


class CarQuoteFDForm(core_forms.FDForm):
    product_type = forms.CharField(required=False)
    vehicleId = forms.CharField(required=False)
    vehicle_rto = forms.CharField(required=False)
    isNewVehicle = forms.CharField(required=False)
    isCNGFitted = forms.CharField(required=False)
    mobile = forms.CharField(required=False)
    cngKitValue = forms.CharField(required=False)
    reg_number = forms.CharField(required=False)
    pastPolicyExpiryDate = forms.CharField(required=False)
    newPolicyStartDate = forms.CharField(required=False)
    isExpired = forms.CharField(required=False)
    manufacturingDates = forms.CharField(required=False)

    class Config:
        fields = [
            core_forms.Person(
                tag="self",
                mobile="mobile",
            ),
            core_forms.CarQuoteForm(
                tag="self",
                product_type="product_type",
                vehicle_master="vehicleId",
                past_policy_expiry_date="pastPolicyExpiryDate",
                new_policy_start_date="newPolicyStartDate",
                vehicle_registration_number="reg_number",
                vehicle_rto="vehicle_rto",
                is_cng_fitted="isCNGFitted",
                cng_value="cngKitValue",
                is_new_car="isNewVehicle",
                is_expired="isExpired",
                manufacturing_date="manufacturingDate"
            )
        ]


class BikeQuoteFDForm(core_forms.FDForm):
    product_type = forms.CharField(required=False)
    vehicleId = forms.CharField(required=False)
    vehicle_rto = forms.CharField(required=False)
    isNewVehicle = forms.CharField(required=False)
    isCNGFitted = forms.CharField(required=False)
    mobile = forms.CharField(required=False)
    cngKitValue = forms.CharField(required=False)
    reg_number = forms.CharField(required=False)
    pastPolicyExpiryDate = forms.CharField(required=False)
    newPolicyStartDate = forms.CharField(required=False)
    isExpired = forms.CharField(required=False)
    manufacturingDates = forms.CharField(required=False)

    class Config:
        fields = [
            core_forms.Person(
                tag="self",
                mobile="mobile",
            ),
            core_forms.BikeQuoteForm(
                tag="self",
                product_type="product_type",
                vehicle_master="vehicleId",
                mobile="mobile",
                past_policy_expiry_date="pastPolicyExpiryDate",
                new_policy_start_date="newPolicyStartDate",
                vehicle_registration_number="reg_number",
                vehicle_rto="vehicle_rto",
                is_cng_fitted="isCNGFitted",
                cng_value="cngKitValue",
                is_new_car="isNewVehicle",
                is_expired="isExpired",
                manufacturing_date="manufacturingDate"
            )
        ]


class CorporateEmployeeForm(forms.ModelForm):

    class Meta:
        model = motor_models.CorporateEmployee
        fields = ('employee_code',)
        widgets = {
            'employee_code': forms.TextInput(attrs={
                                                    'class': 'form-control',
                                                    'placeholder': "Enter your Employee code",
                                                    'required': 'required'
                                                    }),
        }

    def __init__(self, *args, **kwargs):
        self._company = kwargs.pop('company')
        super(CorporateEmployeeForm, self).__init__(*args, **kwargs)

    def clean_employee_code(self):
        d = self.cleaned_data
        employee = motor_models.CorporateEmployee.objects.filter(employee_code=d.get('employee_code'),
                                                                 company__slug=self._company,
                                                                 company__employee_discount=True).first()
        if not employee:
            self._errors["employee_code"] = self.error_class(["Incorrect Employee Id"])
        else:
            return d.get('employee_code')

    def clean(self):
        d = self.cleaned_data
        if 'employee_code' not in d:
            raise forms.ValidationError("Enter Employee Id")
        return d
