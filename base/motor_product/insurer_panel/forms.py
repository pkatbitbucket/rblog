from django import forms
from django.core.exceptions import ValidationError

def validate_file_extension(value):
		import os
		ext = os.path.splitext(value.name)[1]
		valid_extensions = ['.xlsx', '.xls']
		if not ext in valid_extensions:
			raise ValidationError(u'File not supported!')

class UploadFileForm(forms.Form):
	INSURER = (
				('bajaj_allianz', 'Bajaj Allianz'),
				('bharti_axa', 'Bharti AXA'),
				('future_generali', 'Future Generali'),
				('hdfc_ergo', 'HDFC ERGO'),
				('icici_lombard', 'ICICI Lombard'),
				('iffco_tokio', 'IFFCO TOKIO'),
				('landt', 'L&T Insurance'),
				('liberty_videocon', 'Liberty Videocon'),
				('new_india', 'New India'),
				('oriental', 'Oriental'),
				('reliance', 'Reliance'),
				('tata_aig', 'TATA AIG'),
				('universal_sompo', 'Universal Sompo'),
		)
	VEHICLE_TYPE = (
						('fourwheeler', 'Four Wheeler'),
						('twowheeler', 'Two Wheeler'),
		)
	insurer = forms.ChoiceField(choices=INSURER)
	vehicle_type = forms.ChoiceField(choices=VEHICLE_TYPE)
	file = forms.FileField(validators=[validate_file_extension])