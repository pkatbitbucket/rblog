from django.conf.urls import url

import putils

from . import views as insurer_panel_views

urlpatterns = [
    url(r'^$', insurer_panel_views.dashboard, name='dashboard'),
    url(r'^task/(?P<task_id>[\w\d\-\.]+)/status/?$', putils.task_status, name='celery-task_status'),
]
