from motor_product.models import Vehicle, Insurer, VehicleMaster, VehicleMapping
from django.forms.models import model_to_dict
from motor_product import parser_utils
import importlib
from celery_app import app
from django.db import transaction
# Utility logic

# dict to standardize fuel type
FUEL_TO_TYPE = {
  'bajaj-allianz': {
    'P': 'Petrol',
    'D': 'Diesel',
    'C': 'CNG / Petrol',
    'B': 'Electricity',
    '': 'Not Defined',
  },
  'bharti-axa': {
    'P': 'Petrol',
    'PETROL': 'Petrol',
    'D': 'Diesel',
    'DIESEL': 'Diesel',
    'C': 'External LPG / CNG',
    'G': 'Internal LPG / CNG',
    '': 'Not Defined',
  },
  'future-generali': {
    'PETROL': 'Petrol',
    'DIESEL': 'Diesel',
    'CNG/PETROL': 'CNG / Petrol',
    'CNG': 'CNG / Petrol',
    'LPG': 'LPG / Petrol',
    'ELECTRICITY': 'Electricity',
    '': 'Not Defined',
  },
  'hdfc-ergo': {
    'PETROL': 'Petrol',
    'DIESEL': 'Diesel',
    'CNG': 'CNG / Petrol',
    'LPG': 'LPG / Petrol',
    'ELECTRIC': 'Electricity',
    '': 'Not Defined',
  },
  'icici-lombard': {
    'Petrol C': 'Petrol',
    'Diesel C': 'Diesel',
    'Electric C': 'Electricity',
    '': 'Not Defined',
  },
  'iffco-tokio': {
    'P': 'Petrol',
    'PETROL': 'Petrol',
    'NA': 'Petrol',
    'D': 'Diesel',
    'DIESEL': 'Diesel',
    'C': 'External LPG / CNG',
    'G': 'Internal LPG / CNG',
    'BATTERY': 'Electricity',
    'B': 'Electricity',
    'BATERY': 'Electricity',
    'BATRY': 'Electricity',
    'E': 'Electricity',
    'ELECTRICITY': 'Electricity',
    'E': 'Electricity',
    '': 'Not Defined',
  },
  'l-t': {
    '': 'Not Defined',
  },
  'liberty-videocon': {
    'Petrol': 'Petrol',
    'Diesel': 'Diesel',
    'CNG': 'CNG / Petrol',
    'LPG': 'LPG / Petrol',
    'Electricity': 'Electricity',
    '': 'Not Defined',
  },
  'new-india': {
    'Petrol': 'Petrol',
    'Diesel': 'Diesel',
    'CNG': 'CNG / Petrol',
    'LPG': 'LPG / Petrol',
    'Battery': 'Electricity',
    '': 'Not Defined',
  },
  'oriental': {
    'PETROL': 'Petrol',
    'DIESEL': 'Diesel',
    'CNG': 'CNG / Petrol',
    'LPG': 'LPG / Petrol',
    'PETROL + LPG': 'LPG / Petrol',
    'PETROL + CNG': 'CNG / Petrol',
    'BATTERY POWERED - ELECTRICAL': 'Electricity',
    'OTHERS': 'Not Defined',
    '': 'Not Defined',
  },
  'reliance': {
    'PETROL': 'Petrol',
    'DIESEL': 'Diesel',
    'PETROL+LPG': 'LPG / Petrol',
    'PETROL+CNG': 'CNG / Petrol',
    'BATTERY OPERATED': 'Electricity',
    '': 'Not Defined',
  },
  'tata-aig': {
    'Petrol': 'Petrol',
    'Diesel': 'Diesel',
    'External CNG': 'External LPG / CNG',
    'CNG': 'CNG / Petrol',
    'LPG': 'LPG / Petrol',
    'Electricity': 'Electricity',
    '': 'Not Defined',
  },
  'universal-sompo': {
    '': 'Not Defined',
  },
}

insurer_dir_dict = {
   'bajaj_allianz': 'bajaj',
   'bharti_axa': 'bharti',
   'future_generali': 'future',
   'hdfc_ergo': 'hdfc',
   'icici_lombard': 'icici',
   'iffco_tokio': 'iffco',
   'landt': 'landt',
   'liberty_videocon': 'liberty',
   'new_india': 'newindia',
   'oriental': 'oriental',
   'reliance': 'reliance',
   'tata_aig': 'tata',
   'universal_sompo': 'universal',
}

vehicle_type_dict = {
    'fourwheeler': 'Private Car',
    'twowheeler': 'Twowheeler',
}

insurer_unique_com = {
   'bajaj_allianz_fourwheeler': ['vehicle_code', 'variant'],
   'bajaj_allianz_twowheeler': ['vehicle_code'],
   'bharti_axa_fourwheeler': ['make', 'model', 'variant', 'cc', 'seating_capacity', 'fuel_type'],
   'bharti_axa_twowheeler': ['make', 'model', 'variant', 'cc'],
   'future_generali_fourwheeler': ['vehicle_code'],
   'future_generali_twowheeler': [],
   'hdfc_ergo_fourwheeler': ['model_code'],
   'hdfc_ergo_twowheeler': ['model_code'],
   'icici_lombard_fourwheeler': ['model_code'],
   'icici_lombard_twowheeler': [],
   'iffco_tokio_fourwheeler': ['make_code'],
   'iffco_tokio_twowheeler': ['make_code'],
   'landt_fourwheeler': ['make_code', 'model_code', 'variant_code'],
   'landt_twowheeler': [],
   'liberty_videocon_fourwheeler': ['make_code', 'model_code'],
   'liberty_videocon_twowheeler': [],
   'new_india_fourwheeler': ['make', 'model', 'cc', 'seating_capacity', 'variant', 'fuel_type'],
   'new_india_twowheeler': ['make', 'model', 'cc', 'seating_capacity', 'variant', 'fuel_type'],
   'oriental_fourwheeler': ['make_code', 'model_code'],
   'oriental_twowheeler': ['make_code', 'model_code'],
   'reliance_fourwheeler': ['model_code'],
   'reliance_twowheeler': ['model_code'],
   'tata_aig_fourwheeler': ['make', 'model', 'variant', 'cc', 'seating_capacity', 'fuel_type'],
   'tata_aig_twowheeler': [],
   'universal_sompo_fourwheeler': ['make', 'model', 'cc', 'seating_capacity'],
   'universal_sompo_twowheeler': ['model_code', 'cc'],
}

def check_header(_header, _imported):
    cvs_header_params = _imported.csv_parameters
    for k, v in cvs_header_params.items():
        if k == _header[v]:
            continue
        else:
            return False
    return True

def read_file_data_to_return_vehicle_dictionary(row_gen, parameters):
    vehicle_dict_list = []
    for data in row_gen:
        if not data:
            continue

        vehicle_dict = {}
        vehicle_dict['raw_data'] = u"|".join(map(unicode, data))
        for k, v in parameters.items():
            vehicle_dict[k] = data[v]
        vehicle_dict_list.append(vehicle_dict)

    return vehicle_dict_list

def create_mappings(vehicle_with_master, _vtype, _insurer, _insurer_slug):
  mappings = []
  error = []
  info = []
  map_str_format = "Mapping %s to %s"
  for _map in vehicle_with_master:
    _master = _map[0]
    _dict = (model_to_dict(_map[1]))
    del _dict['id']
    _master_map = Vehicle.objects.filter(**_dict)
    if _master and _master_map:
      mapping = VehicleMapping()
      mapping.master_vehicle = _master[0]
      mapping.mapped_vehicle = _master_map[0]
      mapping.insurer = _map[1].insurer
      if mapping.mapped_vehicle:
        info.append(map_str_format % (mapping.master_vehicle, mapping.mapped_vehicle))
        mappings.append(mapping)
    else:
      continue
  return mappings, info

def import_vehicles(_file, _vtype, _insurer):
    response = {
        'error': [],
        'success': [],
        'info': [],
        'warning': [],
    }
    _import_path = "motor_product.insurers.%s.scripts.%s_%s" %(_insurer, insurer_dir_dict[_insurer], _vtype)
    try:
        imported  = importlib.import_module(_import_path)
    except ImportError:
        response['error'].append("We cannot import this file, please choose correct Insurer and vehicle type.")
        return response
    file_row_gen = parser_utils.parse_xlsx(_file, header=True)
    if not check_header(file_row_gen.next(), imported):
        response['error'].append("Invalid header, please fix your file or choose correct Insurer and vehicle type")
        return response
    vehicles = read_file_data_to_return_vehicle_dictionary(file_row_gen, imported.parameters)
    insurer = Insurer.objects.get(slug=imported.INSURER_SLUG)
    vehicle_objects = []
    vehicle_with_master = []
    duplicate_vehicles = []
    str_format_dup = "Duplicate found for vehicle with%s"
    str_format_new = "New record created for vehicle with%s"
    for _vehicle in vehicles:
        get_vehicles = Vehicle.objects.filter(
            vehicle_type=vehicle_type_dict[_vtype], insurer__slug=imported.INSURER_SLUG
        )
        args = {}
        for column in insurer_unique_com[_insurer+'_'+_vtype]:
            if column == 'cc':
                args[column] = parser_utils.try_parse_into_integer(_vehicle[column])
            elif column == 'fuel_type':
                args[column] = FUEL_TO_TYPE[imported.INSURER_SLUG].get(_vehicle[column], _vehicle[column])
            elif column == 'number_of_wheels':
                if _vtype == 'twowheeler':
                    args[column] = 2
                else:
                    args[column] = _vehicle[column]
            elif column == 'seating_capacity':
                if _vtype == 'twowheeler':
                    args[column] = _vehicle[column] if _vehicle[column] else 2
                else:
                    args[column] = _vehicle[column]
            else:
                args[column] = _vehicle[column]
        try:
          dup_vehicle = get_vehicles.filter(**args)
        except ValueError:
          response['error'].append("Empty value found while checking duplicate: %r" % args)
          continue
        vm = parser_utils.try_parse_into_integer(_vehicle['master'])
        del _vehicle['master']
        vm = VehicleMaster.objects.filter(pk=vm)
        if dup_vehicle:
            std_out_dup = ''
            for arg in args:
                std_out_dup = std_out_dup + (" %s: %s " % (arg.replace('_', ' '), _vehicle[arg]))
            response['warning'].append(str_format_dup % std_out_dup)
            if vm:
              for dv in dup_vehicle:
                duplicate_vehicles.append(dv.id)
            else:
              continue
        _vehicle['cc'] = parser_utils.try_parse_into_integer(_vehicle['cc'])
        try:
            _vehicle['fuel_type'] = FUEL_TO_TYPE[imported.INSURER_SLUG].get(_vehicle['fuel_type'], _vehicle['fuel_type'])
        except KeyError:
            pass
        seating_capacity = parser_utils.try_parse_into_integer(_vehicle['seating_capacity'])
        if _vtype=='twowheeler':
            _vehicle['number_of_wheels'] = 2
            _vehicle['vehicle_type'] = 'Twowheeler'
            _vehicle['seating_capacity'] = seating_capacity if seating_capacity else 2
        elif _vtype=='fourwheeler':
            _vehicle['seating_capacity'] = seating_capacity
        _vehicle['insurer'] = insurer
        vehicle = Vehicle(**_vehicle)
        if vm:
          vehicle_with_master.append([vm, vehicle])
        std_out_new = ''
        for arg in args:
            std_out_new = std_out_new + (" %s: %s " % (arg.replace('_', ' '), _vehicle[arg]))
        response['success'].append(str_format_new % std_out_new)
        vehicle_objects.append(vehicle)
    try:
        with transaction.atomic():
          Vehicle.objects.filter(pk__in=duplicate_vehicles).delete()
          Vehicle.objects.bulk_create(vehicle_objects)
          vehiclemaping_objects, map_info = create_mappings(vehicle_with_master, _vtype, _insurer, imported.INSURER_SLUG)
          response['info'] += map_info
          VehicleMapping.objects.bulk_create(vehiclemaping_objects)
    except Exception as e:
        response['error'].append("%s" % e)
        response['warning'] = []
        response['success'] = []
        response['info'] = []
    return response


@app.task(timeout=300)
def motor_data_import(_file, _vtype, _insurer):
    response = import_vehicles(_file, _vtype, _insurer)
    return response
