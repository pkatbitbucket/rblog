from __future__ import absolute_import
from django.contrib.auth.decorators import login_required, user_passes_test
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render_to_response, redirect
from django.contrib import messages
from django.template import RequestContext
from django.core.urlresolvers import reverse
from .forms import UploadFileForm
from .utils import motor_data_import
import utils
# Create your views here.

@csrf_protect
@login_required
@utils.profile_type_only('MOTOR_MASTER_ADMIN')
def dashboard(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            response = motor_data_import.delay(request.FILES['file'], request.POST['vehicle_type'], request.POST['insurer'])
            messages.info(request, response)
            return redirect(reverse('insurerPanel:dashboard'))
    else:
        form = UploadFileForm()
    return render_to_response('insurer_panel/dashboard.html', {'form': form}, context_instance=RequestContext(request))