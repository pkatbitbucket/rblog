import copy
import datetime
import json
import logging
import math
import putils
from django.conf import settings

from motor_product import logger_utils, mail_utils
from motor_product.insurers import insurer_utils
from motor_product.models import (Insurer, RTOMaster, Transaction, Vehicle,
                                  VehicleMaster, IntegrationStatistics)
from suds.client import Client
from utils import motor_logger

from . import custom_dictionaries

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)


######## TEST CREDENTIALS ########
# USER_ID = 'webservicemotor@coverfox.com'
# USER_PASSWORD  = 'password'
# IMD_CODE = '10038791'
# SUB_IMD_CODE = '1901'
# CALLER_ID = 'WEBSERVICE'
# END_POINT = 'http://webservices.bajajallianz.com:8001/WebServicePolicy/WebServicePolicyPort'
# WSDL_URL = 'http://webservices.bajajallianz.com:8001/WebServicePolicy/WebServicePolicyPort?WSDL'
# PAYMENT_URL = 'http://webservices.bajajallianz.com:8001/Insurance/motorWS/new_cc_payment.jsp'

######## PRODUCTION CREDENTIALS ########
USER_ID = 'webservicemotor@coverfox.com'
USER_PASSWORD = 'password'
IMD_CODE = '10038791'
SUB_IMD_CODE = '1901'
CALLER_ID = 'WEBSERVICE'
WSDL_URL = 'http://webservices.bajajallianz.com/WebServicePolicy/WebServicePolicyPort?WSDL'
PAYMENT_URL = 'https://general.bajajallianz.com/Insurance/motorWS/new_cc_payment.jsp'
DOWNLOAD_POLICY_WEBSERVICE = 'http://webservices.bajajallianz.com/docDownldWS/WebServiceImplService?wsdl'

INSURER_SLUG = 'bajaj-allianz'
try:
    INSURER = Insurer.objects.get(slug=INSURER_SLUG)
    INSURER_ID = INSURER.id
    INSURER_NAME = INSURER.title
except:
    INSURER_ID = 0
    INSURER_NAME = ''

suds_logger = logger_utils.getLogger(INSURER_SLUG)


class Configuration(object):

    def __init__(self, vehicle_type):

        self.VEHICLE_TYPE = vehicle_type
        if vehicle_type == 'Private Car':
            self.PRODUCT_CODE = '1801'
            self.VEHICLE_TYPE_NAME = 'Private Car'
            self.VEHICLE_TYPE_CODE = '22'
            self.VEHICLE_AGE_LIMIT = 15
            self.URL_TAG = 'fourwheeler'
            self.RESPONSE_URL = settings.SITE_URL.strip(
                "/") + '/motor/fourwheeler/insurer/bajaj-allianz/payment/response/%s/?'
        elif vehicle_type == 'Twowheeler':
            self.PRODUCT_CODE = '1802'
            self.VEHICLE_TYPE_NAME = 'Two Wheeler'
            self.VEHICLE_TYPE_CODE = '21'
            self.VEHICLE_AGE_LIMIT = 15
            self.URL_TAG = 'twowheeler'
            self.RESPONSE_URL = settings.SITE_URL.strip(
                "/") + '/motor/twowheeler/insurer/bajaj-allianz/payment/response/%s/?'


class PremiumCalculator(object):

    client = None

    def __init__(self, integrationUtils, configuration):
        self.parent = integrationUtils
        self.Configuration = configuration

    def get_client(self, name=None):
        if self.client == None:
            self.client = Client(WSDL_URL)
            self.client.set_options(port='WebServicePolicyPort')
        self.client.set_options(plugins=[suds_logger.set_name(name)])
        return self.client

    def get_premium(self, data_dictionary):
        if int(data_dictionary['vehicleAge']) >= self.Configuration.VEHICLE_AGE_LIMIT:
            motor_logger.info(
                'Vehicle age limit not valid for this insurer %s.' % INSURER_SLUG)
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_AGE_RESTRICTED}

        if data_dictionary['isUsedVehicle']:
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_USED_VEHICLE_RESTRICTED}

        if data_dictionary['isExpired'] and self.Configuration.VEHICLE_TYPE == 'Private Car':
            motor_logger.info(
                'Expired policy not allowed for this insurer %s.' % INSURER_SLUG)
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_EXPIRED_POLICY_RESTRICTED}

        parameters = copy.deepcopy(data_dictionary)
        if data_dictionary.get('req_idv', None):
            parameters['pDetariffObj_extCol10'] = 'DRIVE_ASSURE_FLAG'

        premium_calculation_dict = self._get_required_params_for_premium_calculation(
            parameters)

        raw_request = {
            'data_dictionary': data_dictionary,
        }

        resp = self.get_client(data_dictionary['quoteId']).service.calculateMotorPremiumSig(
            USER_ID,
            USER_PASSWORD,
            premium_calculation_dict['pVehicleCode'],
            premium_calculation_dict['pCity'],
            premium_calculation_dict['pWeoMotPolicyIn_inout'],
            premium_calculation_dict['accessoriesList_inout'],
            premium_calculation_dict['paddoncoverList_inout'],
            premium_calculation_dict['motExtraCover'],
            premium_calculation_dict['pQuestList_inout'],
            premium_calculation_dict['pDetariffObj_inout'],
            premium_calculation_dict['premiumDetailsOut_out'],
            premium_calculation_dict['premiumSummeryList_out'],
            premium_calculation_dict['pError_out'],
            premium_calculation_dict['pErrorCode_out'],
            premium_calculation_dict['pTransactionId_inout'],
            premium_calculation_dict['pTransactionType'],
            premium_calculation_dict['pContactNo'],
        )

        resp_dict = insurer_utils.recursive_asdict(resp)

        # TODO: Below two lines are a hack
        premium_list = resp_dict['premiumSummeryList_out']
        premium_summary_lists = premium_list['WeoMotPremiumSummaryUser']

        # idv = int(resp_dict['premiumDetailsOut_out']['totalIev'])
        idv = int(resp_dict['pWeoMotPolicyIn_inout']['vehicleIdv'])

        data_dictionary['idv'] = idv

        if data_dictionary.get('req_idv', None):
            min_idv = int(math.ceil(idv))
            max_idv = int(math.floor(1.1 * idv / 0.9) / 100) * 100

            data_dictionary['idv'] = data_dictionary['req_idv']
            del data_dictionary['req_idv']

            request_idv = float(data_dictionary['idv'])
            if request_idv < float(min_idv):
                data_dictionary['idv'] = min_idv
            elif request_idv > float(max_idv):
                data_dictionary['idv'] = max_idv

            data_dictionary['min_idv'] = min_idv
            data_dictionary['max_idv'] = max_idv
            final_response, final_details = self.get_premium(data_dictionary)['result']

            ### Bajaj hack ###
            if data_dictionary.get('isForListing', None) and len(final_response['premiumBreakup']['addonCovers']) != 0:
                for coverage_premium in resp_dict['premiumSummeryList_out']['WeoMotPremiumSummaryUser']:
                    if coverage_premium['paramRef'] == 'S13':
                        od = coverage_premium['od']
                        act = coverage_premium['act']
                        cpremium = float(od if od is not None else 0) + float(act if act is not None else 0)
                        final_response['premiumBreakup']['basicCovers'].append({
                            'id': 'odExtra',
                            'default': 1,
                            'premium': cpremium,
                            'name': 'Extra OD Premium',
                        })
                        max_premium = 0
                        max_index = 0
                        index = 0
                        for cover in final_response['premiumBreakup']['addonCovers']:
                            if cover['premium'] > max_premium:
                                max_premium = cover['premium']
                                max_index = index
                            index += 1
                        final_response['premiumBreakup']['addonCovers'][max_index]['premium'] = max_premium - cpremium

            result_data = final_response, final_details
            return {'result': result_data, 'success': True}

        if 'pWeoMotPolicyIn_prvNcb' in data_dictionary.keys():
            previous_ncb = data_dictionary['pWeoMotPolicyIn_prvNcb']
        else:
            previous_ncb = '0'

        coveragePremiumDetails = resp_dict

        service_tax = float(resp_dict['premiumDetailsOut_out'][
                            'serviceTax']) + float(resp_dict['premiumDetailsOut_out']['totalNetPremium'])
        discount = resp_dict['premiumDetailsOut_out']['spDisc']
        loading = resp_dict['premiumDetailsOut_out']['addLoadPrem']

        coveragePremiumDetails['addon_isDepreciationWaiver'] = data_dictionary[
            'pDetariffObj_extCol10']

        return self.convert_premium_response(coveragePremiumDetails, data_dictionary, service_tax, discount, loading)

    def _get_required_params_for_premium_calculation(self, data_dictionary):

        vehicle = Vehicle.objects.filter(insurer__slug=INSURER_SLUG).get(
            id=data_dictionary['VehicleId'])

        pVehicleCode = vehicle.vehicle_code
        pCity = data_dictionary['RegistrationCity']

        # Hack for bajaj
        if vehicle.fuel_type == 'CNG / Petrol':
            items = [item for item in data_dictionary[
                'paddoncoverList_WeoMotGenParamUser'] if item['paddoncoverList_paramDesc'] == 'CNG']
            for item in items:
                data_dictionary[
                    'paddoncoverList_WeoMotGenParamUser'].remove(item)
            data_dictionary['motExtraCover_cngValue'] = None

        pWeoMotPolicyIn_inout_obj = self._get_premium_pWeoMotPolicyIn_obj(
            vehicle, data_dictionary)
        accessoriesList_inout_obj = self._get_premium_accessoriesList_obj()
        paddoncoverList_inout_obj = self._get_premium_paddoncoverList_obj(
            data_dictionary)
        motExtraCover_obj = self._get_premium_motExtraCover_obj(
            data_dictionary, vehicle)
        pQuestList_inout_obj = self._get_premium_pQuestList_obj()
        pDetariffObj_inout_obj = self._get_premium_pDetariffObj_obj(
            data_dictionary)
        premiumDetailsOut_obj = self._get_premium_premiumDetailsOut_obj()
        premiumSummeryList_obj = self._get_premium_premiumSummeryList_obj()
        pError_obj = self._get_premium_pError_obj()

        pErrorCode = '0'
        pTransactionId = '0'
        pTransactionType = 'MOTOR_WEBSERVICE'
        pContactNo = '1234567890'

        premium_calculation_dict = {
            'pVehicleCode': pVehicleCode,
            'pCity': pCity,
            'pWeoMotPolicyIn_inout': pWeoMotPolicyIn_inout_obj,
            'accessoriesList_inout': accessoriesList_inout_obj,
            'paddoncoverList_inout': paddoncoverList_inout_obj,
            'motExtraCover': motExtraCover_obj,
            'pQuestList_inout': pQuestList_inout_obj,
            'pDetariffObj_inout': pDetariffObj_inout_obj,
            'premiumDetailsOut_out': premiumDetailsOut_obj,
            'premiumSummeryList_out': premiumSummeryList_obj,
            'pError_out': pError_obj,
            'pErrorCode_out': pErrorCode,
            'pTransactionId_inout': pTransactionId,
            'pTransactionType': pTransactionType,
            'pContactNo': pContactNo,
        }

        return premium_calculation_dict

    def _get_premium_pWeoMotPolicyIn_obj(self, vehicle, data_dictionary):

        current_date_time = datetime.datetime.now()
        termEndDate = (putils.add_years(current_date_time, 1) - datetime.timedelta(
            days=1)).strftime("%d-%b-%Y").upper()
        vehicle_raw_data_split = vehicle.raw_data.split('|')

        registration_date = datetime.datetime(int(data_dictionary['RegistrationYear']), int(
            data_dictionary['RegistrationMonth']), int(data_dictionary['RegistrationDay']))

        vehicle_fuel_type = ''
        if vehicle.fuel_type == 'Petrol':
            vehicle_fuel_type = 'P'
        elif vehicle.fuel_type == 'Diesel':
            vehicle_fuel_type = 'D'
        elif vehicle.fuel_type == 'CNG / Petrol':
            vehicle_fuel_type = 'C'
        elif vehicle.fuel_type == 'Electricity':
            vehicle_fuel_type = 'B'

        pWeoMotPolicyIn_inout_dict = {
            'addLoading': '0',
            'addLoadingOn': '0',
            'autoMembership': '',  # Auto membership number
            'branchCode': '1901',  # TODO: Branch from where policy is issued.
            'carryingCapacity': vehicle.seating_capacity,
            'chassisNo': 'ABCD',
            'color': '',
            'contractId': '0',
            'cubicCapacity': vehicle.cc,
            'deptCode': '18',  # TODO: Line of business code - constant
            'elecAccTotal': '0',
            'engineNo': 'ABCD',
            'fuel': vehicle_fuel_type,
            'hypo': '',
            'miscVehType': '0',  # TODO: constant
            'ncb': '',
            'nonElecAccTotal': '0',
            'partnerType': 'P',  # TODO: 'P' for Person, 'I' for Institution
            'polType': '1',  # 1-NB, 2-Bajaj renewal, 3-non bajaj renewal
            'product4digitCode': self.Configuration.PRODUCT_CODE,
            'prvClaimStatus': '0',
            'prvExpiryDate': None,
            'prvInsCompany': None,
            'prvNcb': '',
            'prvPolicyRef': None,
            'regiLocOther': data_dictionary['RegistrationCity'],
            'registrationDate': registration_date.strftime("%d-%b-%Y").upper(),
            'registrationLocation': data_dictionary['RegistrationCity'],
            'registrationNo': data_dictionary['RegistrationNumber'],
            'spDiscRate': '0',
            'termEndDate': termEndDate,
            'termStartDate': current_date_time.strftime("%d-%b-%Y").upper(),
            'tpFinType': '',
            'vehicleIdv': data_dictionary['idv'],
            'vehicleMake': vehicle.make,
            'vehicleMakeCode': vehicle.make_code,
            'vehicleModel': vehicle.model,
            'vehicleModelCode': vehicle.model_code,
            'vehicleSubtype': vehicle.variant,
            'vehicleSubtypeCode': vehicle_raw_data_split[6],
            'vehicleType': self.Configuration.VEHICLE_TYPE_NAME,  # Constant for vehicle type
            'vehicleTypeCode': self.Configuration.VEHICLE_TYPE_CODE,  # Constant for vehicle type
            'yearManf': data_dictionary['ManufacturingYear'],
            'zone': 'A' if data_dictionary['RegistrationCity'] in custom_dictionaries.ZONE_A_CITY_LIST else 'B',
        }
        insurer_utils.update_complex_dict(
            pWeoMotPolicyIn_inout_dict, data_dictionary, 'pWeoMotPolicyIn_', {})

        pWeoMotPolicyIn_inout_object = self.get_client(
        ).factory.create("ns0:WeoMotPlanDetailsUser")
        insurer_utils.convert_dict_to_obj(
            pWeoMotPolicyIn_inout_dict, pWeoMotPolicyIn_inout_object)

        return pWeoMotPolicyIn_inout_object

    def _get_premium_accessoriesList_obj(self):

        accessories_inout_dict_array = {
            'WeoMotAccessoriesUser': [
                {
                    'accModel': None,
                    'accTypeCode': None,
                    'accCategoryCode': None,
                    'accCount': None,
                    'contractId': None,
                    'accIev': None,
                    'accMake': None,
                },
            ]
        }
        accessoriesList_inout_object = self.get_client(
        ).factory.create("ns0:WeoMotAccessoriesUserArray")
        for element in accessories_inout_dict_array['WeoMotAccessoriesUser']:
            accessories_inout_object = self.get_client(
            ).factory.create("ns0:WeoMotAccessoriesUser")
            insurer_utils.convert_dict_to_obj(
                element, accessories_inout_object)
            accessoriesList_inout_object.WeoMotAccessoriesUser.append(
                accessories_inout_object)

        return accessoriesList_inout_object

    def _get_premium_paddoncoverList_obj(self, data_dictionary):

        paddoncover_inout_dict_array = {
            'WeoMotGenParamUser': [
                {
                    'paramDesc': '',
                    'paramRef': '',
                },
            ]
        }
        insurer_utils.update_complex_dict(
            paddoncover_inout_dict_array, data_dictionary, 'paddoncoverList_', {})
        paddoncoverList_inout_object = self.get_client(
        ).factory.create("ns0:WeoMotGenParamUserArray")
        for element in paddoncover_inout_dict_array['WeoMotGenParamUser']:
            paddoncover_inout_object = self.get_client(
            ).factory.create("ns0:WeoMotGenParamUser")
            insurer_utils.convert_dict_to_obj(
                element, paddoncover_inout_object)
            paddoncoverList_inout_object.WeoMotGenParamUser.append(
                paddoncover_inout_object)

        return paddoncoverList_inout_object

    def _get_premium_motExtraCover_obj(self, data_dictionary, vehicle):

        motExtraCover_dict = {
            'geogExtn': None,
            'noOfPersonsPa': None,  # With PA
            'sumInsuredPa': None,  # PA SI
            'sumInsuredTotalNamedPa': None,  # With PA, this is the total SI for PA
            'cngValue': None,  # With CNG
            'noOfEmployeesLle': None,
            'noOfPersonsLlo': None,
            'fibreGlassValue': None,
            'sideCarValue': None,
            'noOfTrailers': None,
            'totalTrailerValue': None,
            'voluntaryExcess': None,
            'covernoteNo': None,
            'covernoteDate': None,
            'subImdcode': None,
            'extraField1': None,
            'extraField2': None,
            'extraField3': None,
        }
        insurer_utils.update_complex_dict(
            motExtraCover_dict, data_dictionary, 'motExtraCover_', {})

        motExtraCover_object = self.get_client().factory.create(
            "ns0:WeoSigMotExtraCoversUser")
        insurer_utils.convert_dict_to_obj(
            motExtraCover_dict, motExtraCover_object)

        return motExtraCover_object

    def _get_premium_pQuestList_obj(self):

        pQuest_inout_dict_array = {
            'WeoBjazMotQuestionaryUser': [
                {
                    'questionRef': None,
                    'contractId': None,
                    'questionVal': None,
                },
            ]
        }

        pQuestList_inout_object = self.get_client().factory.create(
            "ns0:WeoBjazMotQuestionaryUserArray")
        for element in pQuest_inout_dict_array['WeoBjazMotQuestionaryUser']:
            pQuest_inout_object = self.get_client().factory.create(
                "ns0:WeoBjazMotQuestionaryUser")
            insurer_utils.convert_dict_to_obj(element, pQuest_inout_object)
            pQuestList_inout_object.WeoBjazMotQuestionaryUser.append(
                pQuest_inout_object)

        return pQuestList_inout_object

    def _get_premium_pDetariffObj_obj(self, data_dictionary):

        pDetariffObj_inout_dict = {
            'vehPurchaseType': None,
            'vehPurchaseDate': None,
            'monthOfMfg': None,
            'bodyType': None,
            'goodsTransType': None,
            'natureOfGoods': None,
            'otherGoodsFrequency': None,
            'permitType': None,
            'roadType': None,
            'vehDrivenBy': None,
            'driverExperience': None,
            'clmHistCode': None,
            'incurredClmExpCode': None,
            'driverQualificationCode': None,
            'tacMakeCode': None,
            'extCol1': None,
            'extCol2': None,
            'extCol3': None,
            'extCol4': None,
            'extCol5': None,
            'extCol6': None,
            'extCol7': None,
            'extCol8': None,
            'extCol9': None,
            'extCol10': None,
            'extCol11': None,
            'extCol12': None,
            'extCol13': None,
            'extCol14': None,
            'extCol15': None,
            'extCol16': None,
            'extCol17': None,
            'extCol18': None,
            'extCol19': None,
            'extCol20': None,
            'registrationAuth': None,
            'extCol21': None,
            'extCol22': None,
            'extCol23': None,
            'extCol24': None,
            'extCol25': None,
            'extCol26': None,
            'extCol27': None,
            'extCol28': None,
            'extCol29': None,
            'extCol30': None,
            'extCol31': None,
            'extCol32': None,
            'extCol33': None,
            'extCol34': None,
            'extCol35': None,
            'extCol36': None,
            'extCol37': None,
            'extCol38': None,
            'extCol39': None,
            'extCol40': None,
        }

        insurer_utils.update_complex_dict(
            pDetariffObj_inout_dict, data_dictionary, 'pDetariffObj_', {})

        pDetariffObj_inout_object = self.get_client(
        ).factory.create("ns0:WeoMotDetariffObjUser")
        insurer_utils.convert_dict_to_obj(
            pDetariffObj_inout_dict, pDetariffObj_inout_object)
        # TODO: This is a hack, request not forming properly without this
        pDetariffObj_inout_object.registrationAuth = ' '

        return pDetariffObj_inout_object

    def _get_premium_premiumDetailsOut_obj(self):

        premiumDetailsOut_dict = {
            'serviceTax': None,
            'collPremium': None,
            'totalActPremium': None,
            'netPremium': None,
            'totalIev': None,
            'addLoadPrem': None,
            'totalNetPremium': None,
            'imtOut': None,
            'totalPremium': None,
            'ncbAmt': None,
            'stampDuty': None,
            'totalOdPremium': None,
            'spDisc': None,
            'finalPremium': None,
        }

        premiumDetailsOut_out_object = self.get_client(
        ).factory.create("ns0:WeoMotPremiumDetailsUser")
        premiumDetailsOut_out_object.serviceTax = ' '
        return premiumDetailsOut_out_object

    def _get_premium_premiumSummeryList_obj(self):

        premiumSummeryList_dict_array = {
            'WeoMotPremiumSummaryUser': [
                {
                    'od': None,
                    'paramDesc': None,
                    'paramRef': None,
                    'net': None,
                    'act': None,
                    'paramType': None,
                },
            ]
        }

        premiumSummeryList_out_object = self.get_client().factory.create(
            "ns0:WeoMotPremiumSummaryUserArray")
        for element in premiumSummeryList_dict_array['WeoMotPremiumSummaryUser']:
            premiumSummery_out_object = self.get_client().factory.create(
                "ns0:WeoMotPremiumSummaryUser")
            insurer_utils.convert_dict_to_obj(
                element, premiumSummery_out_object)
            premiumSummeryList_out_object.WeoMotPremiumSummaryUser.append(
                premiumSummery_out_object)

        return premiumSummeryList_out_object

    def _get_premium_pError_obj(self):

        pErrorList_dict_array = {
            'WeoTygeErrorMessageUser': [
                {
                    'errNumber': None,
                    'parName': None,
                    'property': None,
                    'errText': None,
                    'parIndex': None,
                    'errLevel': None,
                },
            ]
        }

        pErrorList_out_object = self.get_client().factory.create(
            "ns0:WeoTygeErrorMessageUserArray")
        for element in pErrorList_dict_array['WeoTygeErrorMessageUser']:
            pError_out_object = self.get_client().factory.create("ns0:WeoTygeErrorMessageUser")
            insurer_utils.convert_dict_to_obj(element, pError_out_object)
            pErrorList_out_object.WeoTygeErrorMessageUser.append(
                pError_out_object)

        return pErrorList_out_object

    def convert_premium_response(self, coveragePremiumDetails, data_dictionary, service_tax, discount, loading):

        basic_cover_mapping = {
            'OD': {
                'id': 'basicOd',
                'default': '1',
            },
            'PA_DFT': {
                'id': 'paOwnerDriver',
                'default': '1',
            },
            'ACT': {
                'id': 'basicTp',
                'default': '1',
            },
            'PA': {
                'id': 'paPassenger',
                'default': '1',
            },
            'CNG': {
                'id': 'cngKit',
                'default': '1',
            },
            'ELECACC': {
                'id': 'idvElectrical',
                'default': '1',
            },
            'NELECACC': {
                'id': 'idvNonElectrical',
                'default': '1',
            },
            'LLO': {
                'id': 'legalLiabilityDriver',
                'default': '0',
            },
            'S13': {
                'id': 'odExtra',
                'default': '1',
            }
        }

        addon_cover_mapping = {
            'S1': {
                'id': 'is247RoadsideAssistance',
                'default': '1',
            },
            'S3': {
                'id': 'isDepreciationWaiver',
                'default': '0',
            },
            'S4': {
                'id': 'isEngineProtector',
                'default': '0',
            },
        }

        discount_mapping = {
            'NCB': {
                'id': 'ncb',
                'default': '1',
            },
            'COMMDISC': {
                'id': 'commDiscount',
                'default': '1',
            }
        }

        special_discount_mapping = {
            'VOLEX': {
                'id': 'voluntaryDeductible',
                'default': '1',
            },
        }

        basic_covers = []
        addon_covers = []
        discounts = []
        special_discounts = []

        otherloading = coveragePremiumDetails['pDetariffObj_inout']['extCol22']

        total_discount = 0
        if discount:
            total_discount = (-1) * float(discount)

        if loading:
            total_discount += float(loading)

        if otherloading:
            total_discount += float(otherloading)

        if total_discount:
            discounts.append({
                'id': 'odDiscount',
                'default': '1',
                'premium': total_discount,
            })

        basic_od_premium = 0.00

        coveragePremiumItems = coveragePremiumDetails[
            'premiumSummeryList_out']['WeoMotPremiumSummaryUser']
        for coverage_premium in coveragePremiumItems:
            cid = coverage_premium['paramRef']
            od = coverage_premium['od']
            act = coverage_premium['act']
            cpremium = float(od if od is not None else 0) + \
                float(act if act is not None else 0)
            cover = {
                'id': cid,
                'default': '1',
                'premium': round(float(cpremium), 2)
            }
            if cid in basic_cover_mapping:
                if cover['premium'] != 0.00:
                    coverage_map = basic_cover_mapping[cover['id']]
                    cover['id'] = coverage_map['id']
                    cover['default'] = coverage_map['default']
                    basic_covers.append(copy.deepcopy(cover))
                    if cover['id'] == 'basicOd':
                        basic_od_premium = float(cover['premium'])
            elif cid in addon_cover_mapping:
                coverage_map = addon_cover_mapping[cover['id']]
                cover['id'] = coverage_map['id']
                cover['default'] = coverage_map['default']
                addon_covers.append(copy.deepcopy(cover))
            elif cid in discount_mapping:
                if cover['premium'] != 0.00:
                    coverage_map = discount_mapping[cover['id']]
                    cover['id'] = coverage_map['id']
                    cover['default'] = coverage_map['default']
                    discounts.append(copy.deepcopy(cover))
            elif cid in special_discount_mapping:
                if cover['premium'] != 0.00:
                    coverage_map = special_discount_mapping[cover['id']]
                    cover['id'] = coverage_map['id']
                    cover['default'] = coverage_map['default']
                    special_discounts.append(copy.deepcopy(cover))

        vehicle = Vehicle.objects.filter(insurer__slug=INSURER_SLUG).get(
            id=data_dictionary['VehicleId'])

        result_data = {
            'insurerId': int(INSURER_ID),
            'insurerSlug': INSURER_SLUG,
            'insurerName': INSURER_NAME,
            'isInstantPolicy': True,
            'minIDV': int(float(data_dictionary['min_idv'])),
            'maxIDV': int(float(data_dictionary['max_idv'])),
            'calculatedAtIDV': int(float(data_dictionary['idv'])),
            'serviceTaxRate': 0.145,
            'premiumBreakup': {
                'serviceTax': round(float(service_tax), 2),
                'basicCovers': insurer_utils.cpr_update(basic_covers),
                'addonCovers': insurer_utils.cpr_update(addon_covers),
                'discounts': insurer_utils.cpr_update(discounts),
                'specialDiscounts': insurer_utils.cpr_update(special_discounts),
                'dependentCovers': self.parent.get_dependent_addons(None),
                'paPassengerAmounts': insurer_utils.get_pa_passenger_dict(vehicle.vehicle_type, vehicle.seating_capacity),
            },
        }, coveragePremiumDetails
        return {'result': result_data, 'success': True}



class ProposalGenerator(object):

    client = None

    def __init__(self, integrationUtils, configuration):
        self.parent = integrationUtils
        self.Configuration = configuration

    def get_client(self, name=None):
        if self.client == None:
            self.client = Client(WSDL_URL)
        self.client.set_options(plugins=[suds_logger.set_name(name)])
        return self.client

    def save_policy_details(self, data_dictionary, transaction):
        transaction.raw['policy_details'] = data_dictionary
        transaction.save()
        return True

    def get_proposal(self, transaction):

        data_dictionary = transaction.raw['policy_details']

        proposal_generation_dict = self._get_required_params_for_proposal_calculation(
            data_dictionary)
        request_id = int(data_dictionary['pTransactionId'])
        proposal_generation_dict[
            'pMotDetariff'].extCol20 = self.Configuration.RESPONSE_URL % request_id

        proposal_response = self.get_client(transaction.transaction_id).service.issuePolicy(
            USER_ID,
            USER_PASSWORD,
            proposal_generation_dict['pTransactionId'],
            proposal_generation_dict['pRcptList_inout'],
            proposal_generation_dict['pCustDetails_inout'],
            proposal_generation_dict['pWeoMotPolicyIn_inout'],
            proposal_generation_dict['accessoriesList'],
            proposal_generation_dict['paddoncoverList'],
            proposal_generation_dict['motExtraCover_inout'],
            proposal_generation_dict['premiumDetails'],
            proposal_generation_dict['premiumSummeryList'],
            proposal_generation_dict['pQuestList'],
            proposal_generation_dict['ppolicyref_out'],
            proposal_generation_dict['ppolicyissuedate_out'],
            proposal_generation_dict['ppartId_out'],
            proposal_generation_dict['pError_out'],
            proposal_generation_dict['pErrorCode_out'],
            proposal_generation_dict['ppremiumpayerid'],
            proposal_generation_dict['paymentmode'],
            proposal_generation_dict['potherdetails'],
            proposal_generation_dict['pMotDetariff'],
        )

        return proposal_response

    def _get_required_params_for_proposal_calculation(self, data_dictionary):

        pTransactionId = data_dictionary['pTransactionId']

        pRcptList_inout_obj = self._get_proposal_pRcptList_obj()  # Just send the object
        pCustDetails_inout_obj = self._get_proposal_pCustDetails_obj(
            data_dictionary)
        pWeoMotPolicyIn_inout_obj = self._get_proposal_pWeoMotPolicyIn_obj(
            data_dictionary)
        accessoriesList_obj = self._get_proposal_accessoriesList_obj()  # TODO
        paddoncoverList_obj = self._get_proposal_paddoncoverList_obj(
            data_dictionary)
        motExtraCover_inout_obj = self._get_proposal_motExtraCover_obj(
            data_dictionary)
        premiumDetails_obj = self._get_proposal_premiumDetails_obj(
            data_dictionary)  # Same as received during premium calculation
        premiumSummeryList_obj = self._get_proposal_premiumSummeryList_obj(
            data_dictionary)  # Same as received during premium calculation
        pQuestList_obj = self._get_proposal_pQuestList_obj()  # Just send the object

        ppolicyref_out = ''  # Just send the object
        ppolicyissuedate_out = 'CC'  # Payment mode, either agent float or cc
        ppartId_out = ''  # Just send the object

        pError_out_obj = self._get_proposal_pError_obj()

        pErrorCode_out = ''
        ppremiumpayerid = '0'
        paymentmode = 'CC'

        potherdetails_obj = self._get_proposal_potherdetails_obj()
        pMotDetariff_obj = self._get_proposal_pMotDetariff_obj(data_dictionary)

        proposal_generation_dict = {
            'pTransactionId': pTransactionId,
            'pRcptList_inout': pRcptList_inout_obj,
            'pCustDetails_inout': pCustDetails_inout_obj,
            'pWeoMotPolicyIn_inout': pWeoMotPolicyIn_inout_obj,
            'accessoriesList': accessoriesList_obj,
            'paddoncoverList': paddoncoverList_obj,
            'motExtraCover_inout': motExtraCover_inout_obj,
            'premiumDetails': premiumDetails_obj,
            'premiumSummeryList': premiumSummeryList_obj,
            'pQuestList': pQuestList_obj,
            'ppolicyref_out': ppolicyref_out,
            'ppolicyissuedate_out': ppolicyissuedate_out,
            'ppartId_out': ppartId_out,
            'pError_out': pError_out_obj,
            'pErrorCode_out': pErrorCode_out,
            'ppremiumpayerid': ppremiumpayerid,
            'paymentmode': paymentmode,
            'potherdetails': potherdetails_obj,
            'pMotDetariff': pMotDetariff_obj,
        }

        return proposal_generation_dict

    def _get_proposal_pRcptList_obj(self):

        pRcptList_dict_array = {
            'WeoTyacPayRowWsUser': [
                {
                    'payMode': None,
                    'receiptNo': None,
                    'payAmt': None,
                    'collectionNo': None,
                    'collectionAmt': None,
                },
            ]
        }

        pRcptList_inout_object = self.get_client().factory.create(
            "ns0:WeoTyacPayRowWsUserArray")
        for element in pRcptList_dict_array['WeoTyacPayRowWsUser']:
            pRcpt_inout_object = self.get_client().factory.create("ns0:WeoTyacPayRowWsUser")
            insurer_utils.convert_dict_to_obj(element, pRcpt_inout_object)
            pRcptList_inout_object.WeoTyacPayRowWsUser.append(
                pRcpt_inout_object)

        return pRcptList_inout_object

    def _get_proposal_pCustDetails_obj(self, data_dictionary):

        pCustDetails_dict = {
            'telephone1': None,
            'dateOfBirth': None,
            'telephone2': None,
            'status1': None,
            'institutionName': None,
            'profession': None,
            'existingYn': 'N',
            'addLine3': None,
            'surname': None,
            'addLine2': None,
            'partId': None,
            'addLine1': None,
            'password': None,
            'title': None,
            'delivaryOption': None,
            'firstName': None,
            'middleName': None,
            'mobileAlerts': None,
            'loggedIn': None,
            'partTempId': None,
            'availableTime': None,
            'pincode': None,
            'polAddLine1': None,
            'polAddLine3': None,
            'polAddLine2': None,
            'addLine5': None,
            'polAddLine5': None,
            'polPincode': None,
            'cpType': 'P',
            'email': None,
            'status3': None,
            'status2': None,
            'emailAlerts': None,
            'mobile': None,
        }
        insurer_utils.update_complex_dict(
            pCustDetails_dict, data_dictionary, 'pCustDetails_', {})

        pCustDetails_inout_object = self.get_client(
        ).factory.create('ns0:WeoB2cCustDetailsUser')
        insurer_utils.convert_dict_to_obj(
            pCustDetails_dict, pCustDetails_inout_object)

        return pCustDetails_inout_object

    def _get_proposal_pWeoMotPolicyIn_obj(self, data_dictionary):

        pWeoMotPolicyIn_inout_dict = {
            'addLoading': '0',
            'addLoadingOn': '0',
            'autoMembership': '',  # Auto membership number
            'branchCode': '1901',  # TODO: Branch from where policy is issued.
            'carryingCapacity': '',
            'chassisNo': '',
            'color': '',
            'contractId': '0',
            'cubicCapacity': '',
            'deptCode': '18',  # Line of business code - constant
            'elecAccTotal': '0',
            'engineNo': '',
            'fuel': '',
            'hypo': '',
            'miscVehType': '0',  # Constant
            'ncb': '20',
            'nonElecAccTotal': '0',
            'partnerType': 'P',  # 'P' for Person, 'I' for Institution
            'polType': '1',  # 1-NB, 2-Bajaj renewal, 3-non bajaj renewal
            'product4digitCode': self.Configuration.PRODUCT_CODE,
            'prvClaimStatus': '0',
            'prvExpiryDate': None,
            'prvInsCompany': None,
            'prvNcb': '0',
            'prvPolicyRef': None,
            'regiLocOther': '',
            'registrationDate': '',
            'registrationLocation': '',
            'registrationNo': '',
            'spDiscRate': '0',
            'termEndDate': '',
            'termStartDate': '',
            'tpFinType': '',
            'vehicleIdv': '',
            'vehicleMake': '',
            'vehicleMakeCode': '',
            'vehicleModel': '',
            'vehicleModelCode': '',
            'vehicleSubtype': '',
            'vehicleSubtypeCode': '',
            'vehicleType': self.Configuration.VEHICLE_TYPE_NAME,  # Constant for vehicle type
            'vehicleTypeCode': self.Configuration.VEHICLE_TYPE_CODE,  # Constant for vehicle type
            'yearManf': '',
            'zone': 'A',
        }
        insurer_utils.update_complex_dict(
            pWeoMotPolicyIn_inout_dict, data_dictionary, 'pWeoMotPolicyIn_', {})

        pWeoMotPolicyIn_inout_object = self.get_client(
        ).factory.create("ns0:WeoMotPlanDetailsUser")
        insurer_utils.convert_dict_to_obj(
            pWeoMotPolicyIn_inout_dict, pWeoMotPolicyIn_inout_object)

        return pWeoMotPolicyIn_inout_object

    def _get_proposal_accessoriesList_obj(self):

        accessories_inout_dict_array = {
            'WeoMotAccessoriesUser': [
                {
                    'accModel': None,
                    'accTypeCode': None,
                    'accCategoryCode': None,
                    'accCount': None,
                    'contractId': None,
                    'accIev': None,
                    'accMake': None,
                },
            ]
        }
        accessoriesList_inout_object = self.get_client(
        ).factory.create("ns0:WeoMotAccessoriesUserArray")
        for element in accessories_inout_dict_array['WeoMotAccessoriesUser']:
            accessories_inout_object = self.get_client(
            ).factory.create("ns0:WeoMotAccessoriesUser")
            insurer_utils.convert_dict_to_obj(
                element, accessories_inout_object)
            accessoriesList_inout_object.WeoMotAccessoriesUser.append(
                accessories_inout_object)

        return accessoriesList_inout_object

    def _get_proposal_paddoncoverList_obj(self, data_dictionary):

        paddoncover_inout_dict_array = {
            'WeoMotGenParamUser': [
                {
                    'paramDesc': '',
                    'paramRef': '',
                },
            ]
        }
        insurer_utils.update_complex_dict(
            paddoncover_inout_dict_array, data_dictionary, 'paddoncoverList_', {})
        paddoncoverList_inout_object = self.get_client(
        ).factory.create("ns0:WeoMotGenParamUserArray")
        for element in paddoncover_inout_dict_array['WeoMotGenParamUser']:
            paddoncover_inout_object = self.get_client(
            ).factory.create("ns0:WeoMotGenParamUser")
            insurer_utils.convert_dict_to_obj(
                element, paddoncover_inout_object)
            paddoncoverList_inout_object.WeoMotGenParamUser.append(
                paddoncover_inout_object)

        return paddoncoverList_inout_object

    def _get_proposal_motExtraCover_obj(self, data_dictionary):

        motExtraCover_dict = {
            'geogExtn': None,
            'noOfPersonsPa': '',
            'sumInsuredPa': None,  # PA SI
            'sumInsuredTotalNamedPa': '',
            'cngValue': None,  # With CNG
            'noOfEmployeesLle': None,
            'noOfPersonsLlo': None,
            'fibreGlassValue': None,
            'sideCarValue': None,
            'noOfTrailers': None,
            'totalTrailerValue': None,
            'voluntaryExcess': None,
            'covernoteNo': None,
            'covernoteDate': None,
            'subImdcode': None,
            'extraField1': None,
            'extraField2': None,
            'extraField3': None,
        }
        insurer_utils.update_complex_dict(
            motExtraCover_dict, data_dictionary, 'motExtraCover_', {})

        motExtraCover_object = self.get_client().factory.create(
            "ns0:WeoSigMotExtraCoversUser")
        insurer_utils.convert_dict_to_obj(
            motExtraCover_dict, motExtraCover_object)

        return motExtraCover_object

    def _get_proposal_premiumDetails_obj(self, data_dictionary):

        premiumDetails_dict = {
            'serviceTax': None,
            'collPremium': None,
            'totalActPremium': None,
            'netPremium': None,
            'totalIev': None,
            'addLoadPrem': None,
            'totalNetPremium': None,
            'imtOut': None,
            'totalPremium': None,
            'ncbAmt': None,
            'stampDuty': None,
            'totalOdPremium': None,
            'spDisc': None,
            'finalPremium': None,
        }
        insurer_utils.update_complex_dict(
            premiumDetails_dict, data_dictionary, 'premiumDetails_', {})

        premiumDetails_object = self.get_client().factory.create(
            "ns0:WeoMotPremiumDetailsUser")
        insurer_utils.convert_dict_to_obj(
            premiumDetails_dict, premiumDetails_object)
        return premiumDetails_object

    def _get_proposal_premiumSummeryList_obj(self, data_dictionary):

        premiumSummeryList_dict_array = {
            'WeoMotPremiumSummaryUser': [
                {
                    'od': None,
                    'paramDesc': None,
                    'paramRef': None,
                    'net': None,
                    'act': None,
                    'paramType': None,
                },
            ]
        }

        insurer_utils.update_complex_dict(
            premiumSummeryList_dict_array, data_dictionary, 'premiumSummeryList_', {})

        premiumSummeryList_out_object = self.get_client().factory.create(
            "ns0:WeoMotPremiumSummaryUserArray")
        for element in premiumSummeryList_dict_array['WeoMotPremiumSummaryUser']:
            premiumSummery_out_object = self.get_client().factory.create(
                "ns0:WeoMotPremiumSummaryUser")
            insurer_utils.convert_dict_to_obj(
                element, premiumSummery_out_object)
            premiumSummeryList_out_object.WeoMotPremiumSummaryUser.append(
                premiumSummery_out_object)

        return premiumSummeryList_out_object

    def _get_proposal_pQuestList_obj(self):

        pQuest_inout_dict_array = {
            'WeoBjazMotQuestionaryUser': [
                {
                    'questionRef': None,
                    'contractId': None,
                    'questionVal': None,
                },
            ]
        }

        pQuestList_inout_object = self.get_client().factory.create(
            "ns0:WeoBjazMotQuestionaryUserArray")
        for element in pQuest_inout_dict_array['WeoBjazMotQuestionaryUser']:
            pQuest_inout_object = self.get_client().factory.create(
                "ns0:WeoBjazMotQuestionaryUser")
            insurer_utils.convert_dict_to_obj(element, pQuest_inout_object)
            pQuestList_inout_object.WeoBjazMotQuestionaryUser.append(
                pQuest_inout_object)

        return pQuestList_inout_object

    def _get_proposal_pError_obj(self):

        pErrorList_dict_array = {
            'WeoTygeErrorMessageUser': [
                {
                    'errNumber': None,
                    'parName': None,
                    'property': None,
                    'errText': None,
                    'parIndex': None,
                    'errLevel': None,
                },
            ]
        }

        pErrorList_out_object = self.get_client().factory.create(
            "ns0:WeoTygeErrorMessageUserArray")
        for element in pErrorList_dict_array['WeoTygeErrorMessageUser']:
            pError_out_object = self.get_client().factory.create("ns0:WeoTygeErrorMessageUser")
            insurer_utils.convert_dict_to_obj(element, pError_out_object)
            pErrorList_out_object.WeoTygeErrorMessageUser.append(
                pError_out_object)

        return pErrorList_out_object

    def _get_proposal_potherdetails_obj(self):

        potherdetails_dict = {
            'covernoteNo': None,
            'cceCode': None,
            'extra1': 'NEWPG',
            'extra2': None,
            'imdcode': IMD_CODE,
            'extra3': None,
            'extra4': None,
            'extra5': None,
            'leadNo': None,
            'runnerCode': None,
        }

        potherdetails_object = self.get_client().factory.create(
            'ns0:WeoSigOtherDetailsUser')
        insurer_utils.convert_dict_to_obj(
            potherdetails_dict, potherdetails_object)
        return potherdetails_object

    def _get_proposal_pMotDetariff_obj(self, data_dictionary):

        pMotDetariff_dict = {
            'driverQualificationCode': None,
            'vehPurchaseType': None,
            'extCol25': None,
            'extCol26': None,
            'tacMakeCode': None,
            'extCol23': None,
            'extCol24': None,
            'extCol29': None,
            'extCol27': None,
            'extCol28': None,
            'extCol33': None,
            'extCol32': None,
            'extCol31': None,
            'extCol30': None,
            'registrationAuth': None,
            'extCol12': None,
            'extCol13': None,
            'extCol14': None,
            'extCol15': None,
            'extCol16': None,
            'extCol17': None,
            'extCol18': None,
            'extCol19': None,
            'otherGoodsFrequency': None,
            'extCol20': None,
            'extCol22': None,
            'incurredClmExpCode': None,
            'extCol21': None,
            'clmHistCode': None,
            'roadType': None,
            'bodyType': None,
            'monthOfMfg': None,
            'natureOfGoods': None,
            'extCol9': None,
            'extCol8': None,
            'extCol7': None,
            'extCol6': None,
            'vehPurchaseDate': None,
            'extCol5': None,
            'extCol4': None,
            'vehDrivenBy': None,
            'extCol3': None,
            'extCol2': None,
            'extCol1': None,
            'goodsTransType': None,
            'permitType': None,
            'extCol38': None,
            'extCol39': None,
            'extCol34': None,
            'extCol35': None,
            'extCol36': None,
            'extCol10': None,
            'extCol37': None,
            'extCol11': None,
            'extCol40': None,
            'driverExperience': None,
        }

        insurer_utils.update_complex_dict(
            pMotDetariff_dict, data_dictionary, 'pMotDetariff_', {})

        pMotDetariff_obj = self.get_client().factory.create("ns0:WeoMotDetariffObjUser")
        insurer_utils.convert_dict_to_obj(pMotDetariff_dict, pMotDetariff_obj)
        return pMotDetariff_obj


class IntegrationUtils(object):

    PremiumCalculator = None
    ProposalGenerator = None

    def __init__(self, vehicle_type):
        configuration = Configuration(vehicle_type)
        self.Configuration = configuration
        self.PremiumCalculator = PremiumCalculator(self, configuration)
        self.ProposalGenerator = ProposalGenerator(self, configuration)

    def convert_premium_request_data_to_required_dictionary(self, vehicle_id, request_data):

        # TODO - Should be removed once data coming correctly from front end
        if 'newPolicyStartDate' not in request_data.keys():
            request_data['newPolicyStartDate'] = (
                datetime.datetime.now() + datetime.timedelta(days=1)).strftime("%d-%m-%Y")

        if request_data['isNewVehicle'] == '1':
            is_new_vehicle = True
        else:
            is_new_vehicle = False

        # New policy start date and end date assignment
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                request_data['newPolicyStartDate'], "%d-%m-%Y")
            new_policy_end_date = putils.add_years(
                new_policy_start_date, 1) - datetime.timedelta(days=1)
        else:
            past_policy_end_date = datetime.datetime.strptime(
                request_data['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

            # This can happen for twowheeler expired flow
            if new_policy_start_date < datetime.datetime.now():
                new_policy_start_date = datetime.datetime.now() + datetime.timedelta(days=1)

            new_policy_end_date = putils.add_years(new_policy_start_date, 1) - datetime.timedelta(days=1)

        # Vehicle age calculation
        if is_new_vehicle:
            vehicle_age = 0
            request_data['registrationDate'] = datetime.datetime.now(
            ).strftime('%d-%m-%Y')
        else:
            date_of_registration = datetime.datetime.strptime(
                request_data['registrationDate'], '%d-%m-%Y')
            new_policy_start_date = new_policy_start_date.replace(
                microsecond=0)
            vehicle_age = insurer_utils.get_age(
                new_policy_start_date, date_of_registration)

        man_date_split = request_data['manufacturingDate'].split('-')
        manufacturing_year = man_date_split[2]
        manufacturing_month = man_date_split[1]

        reg_date_split = request_data['registrationDate'].split('-')
        registration_year = reg_date_split[2]
        registration_month = reg_date_split[1]
        registration_day = reg_date_split[0]

        split_registration_no = request_data.get('registrationNumber[]')
        master_rto_code = "%s-%s" % (
            split_registration_no[0], split_registration_no[1])
        rto_city_code = RTOMaster.objects.get(
            rto_code=master_rto_code).insurer_rtos.get(insurer__slug=INSURER_SLUG).rto_code

        addons = {'addon_isDepreciationWaiver': 'isDepreciationWaiver'}

        vehicle_model = VehicleMaster.objects.get(
            id=request_data['vehicleId']).model
        addon_dict = {}
        for key, value in addons.items():
            addon_dict[key] = False
            if request_data[key] == '1' and insurer_utils.is_addon_available(value, 'bajaj-allianz', vehicle_model, vehicle_age, master_rto_code):
                addon_dict[key] = True

        data_dictionary = {
            # 'isFirstRequest' : True if request_data['idv'] == '0' else False,
            'vehicleAge': vehicle_age,
            'isUsedVehicle': True if request_data['isUsedVehicle'] == '1' else False,
            'isExpired': False if is_new_vehicle else (past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now()),
            'VehicleId': vehicle_id,
            'RegistrationCity': rto_city_code,
            'RegistrationNumber': "-".join(split_registration_no),
            # 'VehicleBuyingYear' : vehicle_buying_year,
            # 'VehicleBuyingMonth' : vehicle_buying_month,
            'ManufacturingYear': manufacturing_year,
            'ManufacturingMonth': manufacturing_month,
            'RegistrationYear': registration_year,
            'RegistrationMonth': registration_month,
            'RegistrationDay': registration_day,
            'isNewCar': request_data['isNewVehicle'],
            # 'pWeoMotPolicyIn_ncb' : '50' if request_data['ncb']=='65' else request_data['ncb'],
            # 'pWeoMotPolicyIn_prvNcb' : request_data['previousNcb'],
            # 'pWeoMotPolicyIn_prvClaimStatus' : request_data['isClaimedLastYear'],
            # 'pcv_num_is_emr_asst_cvr' : request_data['is247RoadsideAssistance'],
            # 'pcv_num_is_zero_dept' : request_data['isDepreciationWaiver'],
            'pDetariffObj_extCol10': 'DRIVE_ASSURE_PACK' if addon_dict['addon_isDepreciationWaiver'] else 'DRIVE_ASSURE_FLAG',
            'pWeoMotPolicyIn_termEndDate': new_policy_end_date.strftime("%d-%b-%Y").upper(),
            'pWeoMotPolicyIn_termStartDate': new_policy_start_date.strftime("%d-%b-%Y").upper(),
        }

        previous_ncb_to_new_ncb = {
            '0': '20',
            '20': '25',
            '25': '35',
            '35': '45',
            '45': '50',
            '50': '50',
        }

        ncb = ''
        if request_data['isClaimedLastYear'] == '0':
            ncb = previous_ncb_to_new_ncb[request_data['previousNCB']]

        if not is_new_vehicle and (datetime.datetime.now() - past_policy_end_date).days > 90:
            ncb = ''

        if not is_new_vehicle:
            data_dictionary['pWeoMotPolicyIn_ncb'] = ncb
            data_dictionary['pWeoMotPolicyIn_prvNcb'] = request_data[
                'previousNCB']
            data_dictionary['pWeoMotPolicyIn_prvClaimStatus'] = request_data[
                'isClaimedLastYear']
            data_dictionary['pWeoMotPolicyIn_polType'] = '3'
            data_dictionary['pWeoMotPolicyIn_prvExpiryDate'] = past_policy_end_date.strftime(
                "%d-%b-%Y").upper()

        data_dictionary['req_idv'] = request_data['idv']
        data_dictionary['idv'] = ''

        request_data_keys_to_coverage_mapping = {
            'isCNGFitted': {
                'unacceptable': ['0'],
                'paramDesc': 'CNG',
                'paramRef': 'CNG',
                'attributes': {
                    'motExtraCover_cngValue': 'cngKitValue',
                },
            },
            'idvElectrical': {
                'unacceptable': ['0'],
                'paramDesc': 'ELECACC',
                'paramRef': 'ELECACC',
                'attributes': {
                    'pWeoMotPolicyIn_elecAccTotal': 'idvElectrical',
                },
            },
            'idvNonElectrical': {
                'unacceptable': ['0'],
                'paramDesc': 'NELECACC',
                'paramRef': 'NELECACC',
                'attributes': {
                    'pWeoMotPolicyIn_nonElecAccTotal': 'idvNonElectrical',
                },
            },
            'voluntaryDeductible': {
                'unacceptable': [None, 'None'],
                'paramDesc': 'VOLEX',
                'paramRef': 'VOLEX',
                'attributes': {
                    'motExtraCover_voluntaryExcess': 'voluntaryDeductible',
                },
            },
        }

        data_dictionary['paddoncoverList_WeoMotGenParamUser'] = []
        for key, mapping_value in request_data_keys_to_coverage_mapping.items():
            if key in request_data.keys():
                if request_data[key] not in mapping_value['unacceptable']:
                    weoMotGenParamUser = {
                        'paddoncoverList_paramDesc': mapping_value['paramDesc'],
                        'paddoncoverList_paramRef': mapping_value['paramRef'],
                    }
                    data_dictionary['paddoncoverList_WeoMotGenParamUser'].append(
                        copy.deepcopy(weoMotGenParamUser))
                    for data_dict_key, request_data_key in mapping_value['attributes'].items():
                        data_dictionary[data_dict_key] = request_data[
                            request_data_key]

        # For PA to unnamed passenger
        if int(request_data['extra_paPassenger']) > 0:
            data_dictionary['motExtraCover_noOfPersonsPa'] = Vehicle.objects.get(
                id=vehicle_id).seating_capacity
            data_dictionary['motExtraCover_sumInsuredPa'] = request_data[
                'extra_paPassenger']
            weoMotGenParamUser = {
                'paddoncoverList_paramDesc': 'PA',
                'paddoncoverList_paramRef': 'PA',
            }
            data_dictionary['paddoncoverList_WeoMotGenParamUser'].append(
                copy.deepcopy(weoMotGenParamUser))

        # For legal liability
        if request_data['extra_isLegalLiability'] == '1':
            data_dictionary['motExtraCover_noOfPersonsLlo'] = '1'
            weoMotGenParamUser = {
                'paddoncoverList_paramDesc': 'LLO',
                'paddoncoverList_paramRef': 'LLO',
            }
            data_dictionary['paddoncoverList_WeoMotGenParamUser'].append(
                copy.deepcopy(weoMotGenParamUser))
        return data_dictionary

    def get_buy_form_details(self, quote_parameters, transaction):

        split_registration_no = quote_parameters.get('registrationNumber[]')

        is_new_vehicle = True if quote_parameters[
            'isNewVehicle'] == '1' else False
        if is_new_vehicle:
            registration_number = 'NEW'
        else:
            registration_number = '-'.join(split_registration_no)

        master_rto_code = "%s-%s" % (
            split_registration_no[0], split_registration_no[1])

        data = {
            'registration_no': registration_number,
            'rto_location_info': master_rto_code,
            #'insurers_list': PAST_INSURANCE_ID_LIST,
            'insurers_list': custom_dictionaries.PAST_INSURER_ID_LIST_MASTER,
            'disallowed_insurers': custom_dictionaries.DISALLOWED_PAST_INSURERS,
            'relationship_list': custom_dictionaries.NOMINEE_RELATIONSHIP_LIST,
            'is_cities_fetch': False,
        }
        return data

    def convert_proposal_request_data_to_required_dictionary(self, vehicle, request_data, quote_parameters, quote_response, quote_custom_response):

        idv = quote_custom_response['calculatedAtIDV']

        if request_data['cust_gender'] == 'Male':
            request_data['form-insured-title'] = 'Mr.'
        else:
            if request_data['cust_marital_status'] == '0':
                request_data['form-insured-title'] = 'Ms.'
            else:
                request_data['form-insured-title'] = 'Mrs.'

        is_new_vehicle = False
        if quote_parameters['isNewVehicle'] == '1':
            is_new_vehicle = True

        if is_new_vehicle:
            request_data['vehicle_reg_no'] = 'NEW'

        registration_number = request_data['vehicle_reg_no']

        vehicle_rto = "-".join(quote_parameters['registrationNumber[]'][0:2])
        vehicle_rto = RTOMaster.objects.get(rto_code=vehicle_rto).insurer_rtos.get(
            insurer__slug=INSURER_SLUG).rto_code

        if is_new_vehicle:
            request_data['vehicle_reg_date'] = datetime.datetime.now(
            ).strftime("%d-%m-%Y")
        else:
            request_data['vehicle_reg_date'] = quote_parameters[
                'registrationDate']

        reg_date_split = request_data['vehicle_reg_date'].split('-')
        registration_year = int(reg_date_split[2])
        registration_month = int(reg_date_split[1])
        registration_day = int(reg_date_split[0])

        date_of_registration = datetime.datetime.strptime(
            request_data['vehicle_reg_date'], "%d-%m-%Y")

        man_date_split = quote_parameters['manufacturingDate'].split('-')
        manufacturing_year = man_date_split[2]
        manufacturing_month = man_date_split[1]

        current_date_time = datetime.datetime.now()
        current_date_time = current_date_time.replace(microsecond=0)

        cust_dob = datetime.datetime.strptime(request_data['cust_dob'], "%d-%m-%Y")

        vehicle_raw_data_split = vehicle.raw_data.split('|')

        vehicle_fuel_type = ''
        if vehicle.fuel_type == 'Petrol':
            vehicle_fuel_type = 'P'
        elif vehicle.fuel_type == 'Diesel':
            vehicle_fuel_type = 'D'
        elif vehicle.fuel_type == 'CNG / Petrol':
            vehicle_fuel_type = 'C'
        elif vehicle.fuel_type == 'Electricity':
            vehicle_fuel_type = 'B'

        # Assigning new_policy_start_date and new_policy_end_date
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                quote_parameters['newPolicyStartDate'], "%d-%m-%Y")
            new_policy_end_date = putils.add_years(new_policy_start_date, 1) - datetime.timedelta(days=1)
        else:
            past_policy_end_date = datetime.datetime.strptime(
                quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

            # This can happen for twowheeler expired flow
            if new_policy_start_date < datetime.datetime.now():
                new_policy_start_date = datetime.datetime.now() + datetime.timedelta(days=1)
            new_policy_end_date = putils.add_years(new_policy_start_date, 1) - datetime.timedelta(days=1)

        vehicle_age = insurer_utils.get_age(
            new_policy_start_date, date_of_registration)

        previous_ncb_to_new_ncb = {
            '0': '20',
            '20': '25',
            '25': '35',
            '35': '45',
            '45': '50',
            '50': '50',
        }
        pncb = '0'
        ncb = ''
        if not is_new_vehicle:
            pncb = quote_parameters['previousNCB']
            ncb = previous_ncb_to_new_ncb[pncb] if quote_parameters[
                'isClaimedLastYear'] == '0' else ''

            if (datetime.datetime.now() - past_policy_end_date).days > 90:
                ncb = ''

        premium_response = quote_response

        premium_details = premium_response['premiumDetailsOut_out']
        data_dictionary = {

            'pTransactionId': premium_response['pTransactionId_inout'],
            'VehicleId': vehicle.id,
            'RegistrationCity': vehicle_rto,
            'RegistrationNumber': registration_number,
            # 'VehicleBuyingYear' : vehicle_buying_year,
            # 'VehicleBuyingMonth' : vehicle_buying_month,
            'RegistrationYear': registration_year,
            'RegistrationMonth': registration_month,
            'RegistrationDay': registration_day,
            'ManufacturingYear': manufacturing_year,
            'ManufacturingMonth': manufacturing_month,
            'isNewCar': quote_parameters['isNewVehicle'],
            # 'pWeoMotPolicyIn_ncb' : '50' if vehicle_details['ncb']=='65' else vehicle_details['ncb'],
            # 'pWeoMotPolicyIn_prvNcb' : vehicle_details['previousNcb'],
            # 'pWeoMotPolicyIn_prvClaimStatus' : vehicle_details['isClaimedLastYear'],

            # Extra field to send pincode
            'motExtraCover_extraField1': request_data['add_pincode'],
            # 'pcv_num_is_emr_asst_cvr' : request_data['is247RoadsideAssistance'],
            # 'pcv_num_is_zero_dept' : request_data['isDepreciationWaiver'],

            # customer details
            'pCustDetails_dateOfBirth': cust_dob.strftime("%d-%b-%Y").upper(),
            'pCustDetails_addLine3': request_data['add_street_name'],
            'pCustDetails_surname': request_data['cust_last_name'],
            'pCustDetails_addLine2': request_data['add_building_name'] if request_data['add_building_name'] != '' else request_data['add_street_name'],
            'pCustDetails_addLine1': request_data['add_house_no'],
            'pCustDetails_title': request_data['form-insured-title'],
            'pCustDetails_firstName': request_data['cust_first_name'],
            'pCustDetails_pincode': request_data['add_pincode'],
            'pCustDetails_polAddLine1': request_data['add_house_no'],
            'pCustDetails_polAddLine3': request_data['add_street_name'],
            'pCustDetails_polAddLine2': request_data['add_building_name'],
            'pCustDetails_addLine5': request_data['add_landmark'],
            'pCustDetails_polAddLine5': request_data['add_landmark'],
            'pCustDetails_polPincode': request_data['add_pincode'],
            'pCustDetails_email': request_data['cust_email'],
            'pCustDetails_mobile': request_data['cust_phone'],

            # vehicle details
            'pWeoMotPolicyIn_hypo': '' if request_data['is_car_financed'] == '0' else request_data['car-financier'],
            'pWeoMotPolicyIn_tpFinType': '1' if request_data['is_car_financed'] == '1' else '',
            'pWeoMotPolicyIn_carryingCapacity': vehicle.seating_capacity,
            'pWeoMotPolicyIn_chassisNo': request_data['vehicle_chassis_no'],
            'pWeoMotPolicyIn_cubicCapacity': vehicle.cc,
            'pWeoMotPolicyIn_engineNo': request_data['vehicle_engine_no'],
            'pWeoMotPolicyIn_fuel': vehicle_fuel_type,
            'pWeoMotPolicyIn_regiLocOther': vehicle_rto,
            'pWeoMotPolicyIn_registrationDate': date_of_registration.strftime("%d-%b-%Y").upper(),
            'pWeoMotPolicyIn_registrationLocation': vehicle_rto,
            'pWeoMotPolicyIn_registrationNo': registration_number,
            'pWeoMotPolicyIn_termEndDate': new_policy_end_date.strftime("%d-%b-%Y").upper(),
            'pWeoMotPolicyIn_termStartDate': new_policy_start_date.strftime("%d-%b-%Y").upper(),
            'pWeoMotPolicyIn_vehicleIdv': idv,
            'pWeoMotPolicyIn_vehicleMake': vehicle.make,
            'pWeoMotPolicyIn_vehicleMakeCode': vehicle.make_code,
            'pWeoMotPolicyIn_vehicleModel': vehicle.model,
            'pWeoMotPolicyIn_vehicleModelCode': vehicle.model_code,
            'pWeoMotPolicyIn_vehicleSubtype': vehicle.variant,
            'pWeoMotPolicyIn_vehicleSubtypeCode': vehicle_raw_data_split[6],
            'pWeoMotPolicyIn_yearManf': manufacturing_year,
            'pWeoMotPolicyIn_zone': 'A' if vehicle_rto in custom_dictionaries.ZONE_A_CITY_LIST else 'B',

            # premium details
            'premiumDetails_serviceTax': premium_details['serviceTax'],
            'premiumDetails_collPremium': premium_details['collPremium'],
            'premiumDetails_totalActPremium': premium_details['totalActPremium'],
            'premiumDetails_netPremium': premium_details['netPremium'],
            'premiumDetails_totalIev': premium_details['totalIev'],
            'premiumDetails_addLoadPrem': premium_details['addLoadPrem'],
            'premiumDetails_totalNetPremium': premium_details['totalNetPremium'],
            'premiumDetails_imtOut': premium_details['imtOut'],
            'premiumDetails_totalPremium': premium_details['totalPremium'],
            'premiumDetails_ncbAmt': premium_details['ncbAmt'],
            'premiumDetails_stampDuty': premium_details['stampDuty'],
            'premiumDetails_totalOdPremium': premium_details['totalOdPremium'],
            'premiumDetails_spDisc': premium_details['spDisc'],
            'premiumDetails_finalPremium': premium_details['finalPremium'],

            # Addon details
            'pMotDetariff_extCol10': quote_response['addon_isDepreciationWaiver'],
        }

        data_dictionary['pWeoMotPolicyIn_ncb'] = ncb
        data_dictionary['pWeoMotPolicyIn_prvNcb'] = pncb
        if not is_new_vehicle:
            data_dictionary['pWeoMotPolicyIn_prvClaimStatus'] = quote_parameters[
                'isClaimedLastYear']
            data_dictionary['pWeoMotPolicyIn_polType'] = '3'
            data_dictionary['pWeoMotPolicyIn_prvExpiryDate'] = past_policy_end_date.strftime(
                "%d-%b-%Y").upper()
            past_policy_insurer_code = custom_dictionaries.REV_PASTINSURER_MAP[request_data['past_policy_insurer']]
            # data_dictionary['pWeoMotPolicyIn_prvInsCompany'] = request_data[
            #    'past_policy_insurer']
            data_dictionary['pWeoMotPolicyIn_prvInsCompany'] = past_policy_insurer_code
            data_dictionary['pWeoMotPolicyIn_prvPolicyRef'] = request_data[
                'past_policy_number']

        vehicle_details_keys_to_coverage_mapping = {
            'idvElectrical': {
                'unacceptable': ['0'],
                'paramDesc': 'ELECACC',
                'paramRef': 'ELECACC',
                'attributes': {
                    'pWeoMotPolicyIn_elecAccTotal': 'idvElectrical',
                },
            },
            'idvNonElectrical': {
                'unacceptable': ['0'],
                'paramDesc': 'NELECACC',
                'paramRef': 'NELECACC',
                'attributes': {
                    'pWeoMotPolicyIn_nonElecAccTotal': 'idvNonElectrical',
                },
            },
            'voluntaryDeductible': {
                'unacceptable': [None, 'None'],
                'paramDesc': 'VOLEX',
                'paramRef': 'VOLEX',
                'attributes': {
                    'motExtraCover_voluntaryExcess': 'voluntaryDeductible',
                },
            },
        }

        # Hack for bajaj
        if vehicle.fuel_type != 'CNG / Petrol':
            vehicle_details_keys_to_coverage_mapping['cngKitValue'] = {
                'unacceptable': [0, '0'],
                'paramDesc': 'CNG',
                'paramRef': 'CNG',
                'attributes': {
                    'motExtraCover_cngValue': 'cngKitValue',
                },
            }

        data_dictionary['paddoncoverList_WeoMotGenParamUser'] = []
        for key, mapping_value in vehicle_details_keys_to_coverage_mapping.items():
            if key in quote_parameters.keys():
                if quote_parameters[key] not in mapping_value['unacceptable']:
                    weoMotGenParamUser = {
                        'paddoncoverList_paramDesc': mapping_value['paramDesc'],
                        'paddoncoverList_paramRef': mapping_value['paramRef'],
                    }
                    data_dictionary['paddoncoverList_WeoMotGenParamUser'].append(
                        copy.deepcopy(weoMotGenParamUser))
                    for data_dict_key, vehicle_details_key in mapping_value['attributes'].items():
                        data_dictionary[data_dict_key] = quote_parameters[
                            vehicle_details_key]

        premium_summaries = premium_response[
            'premiumSummeryList_out']['WeoMotPremiumSummaryUser']
        data_dictionary['premiumSummeryList_WeoMotPremiumSummaryUser'] = []
        for premium_summary in premium_summaries:
            new_premium_summary_dict = {
                'premiumSummeryList_od': premium_summary['od'],
                'premiumSummeryList_paramDesc': premium_summary['paramDesc'],
                'premiumSummeryList_paramRef': premium_summary['paramRef'],
                'premiumSummeryList_net': premium_summary['net'],
                'premiumSummeryList_act': premium_summary['act'],
                'premiumSummeryList_paramType': premium_summary['paramType'],
            }
            data_dictionary['premiumSummeryList_WeoMotPremiumSummaryUser'].append(
                copy.deepcopy(new_premium_summary_dict))

        # For PA to unnamed passenger
        if int(quote_parameters['extra_paPassenger']) > 0:
            data_dictionary[
                'motExtraCover_noOfPersonsPa'] = vehicle.seating_capacity
            data_dictionary['motExtraCover_sumInsuredPa'] = quote_parameters[
                'extra_paPassenger']
            weoMotGenParamUser = {
                'paddoncoverList_paramDesc': 'PA',
                'paddoncoverList_paramRef': 'PA',
            }
            data_dictionary['paddoncoverList_WeoMotGenParamUser'].append(
                copy.deepcopy(weoMotGenParamUser))

        if quote_parameters['extra_isLegalLiability'] == '1':
            data_dictionary['motExtraCover_noOfPersonsLlo'] = '1'
            weoMotGenParamUser = {
                'paddoncoverList_paramDesc': 'LLO',
                'paddoncoverList_paramRef': 'LLO',
            }
            data_dictionary['paddoncoverList_WeoMotGenParamUser'].append(
                copy.deepcopy(weoMotGenParamUser))
        return data_dictionary

    def update_transaction_details(self, transaction, data):
        is_new_vehicle = data['isNewVehicle'] == '1'
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                data['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

            # This can happen for twowheeler expired flow
            if new_policy_start_date < datetime.datetime.now():
                new_policy_start_date = datetime.datetime.now() + datetime.timedelta(days=1)

        new_policy_end_date = putils.add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)

        transaction.policy_start_date = new_policy_start_date
        transaction.policy_end_date = new_policy_end_date
        transaction.save()

    def get_payment_data(self, transaction):
        proposal_response = self.ProposalGenerator.get_proposal(transaction)
        motor_logger.info(proposal_response)

        resp_dict = dict([item for item in proposal_response])

        premium_paid = transaction.raw['policy_details'][
            'premiumDetails_finalPremium']
        request_id = int(transaction.raw['policy_details']['pTransactionId'])

        if resp_dict['pErrorCode_out'] == 0.0:
            transaction.premium_paid = premium_paid
            transaction.reference_id = request_id
            transaction.save()

            payment_gateway_data = {
                'requestId': transaction.reference_id,
                'Username': USER_ID
            }
            suds_logger.set_name(transaction.transaction_id).log(
                json.dumps(payment_gateway_data))
            transaction.payment_request = payment_gateway_data
            transaction.save()
            return payment_gateway_data, PAYMENT_URL

        # Proposal failure response mail
        mail_utils.send_mail_for_proposal_failure(transaction, str(proposal_response))

        motor_logger.info(transaction.raw['WeoTygeErrorMessageUser'])
        error_list = []
        for em in transaction.raw['WeoTygeErrorMessageUser']:
            error_dict = {
                'errNumber': em.errNumber,
                'parName': em.parName,
                'property': em.property,
                'errText': em.errText,
                'parIndex': em.parIndex,
                'errLevel': em.errLevel,
            }
            error_list.append(error_dict)

        transaction.raw['premium_response'] = error_list
        transaction.save()
        return False

    def post_payment(self, payment_response, transaction):
        """
        http://demo.coverfox.com/health-plan/4/transaction/abe2b262af4511e3bd72f23c91ae8aaa/response/?policy_no=OG-14-1901-6001-00012806&request_id=16961&p_pay_status=Y
        Final Response : {u'p_pay_status': u'Y', u'policy_no': u'OG-14-1901-6001-00012806', u'request_id': u'16961'}
        """
        motor_logger.info(payment_response)
        suds_logger.set_name(transaction.transaction_id).log(json.dumps(payment_response))
        redirect_url = '/motor/%s/payment/failure/%s/' % (self.Configuration.URL_TAG, transaction.transaction_id)
        is_redirect = True
        transaction.insured_details_json['payment_response'] = payment_response
        transaction.payment_response = payment_response
        transaction.save()

        payment_status = payment_response['GET']['p_pay_status']

        if (payment_status == 'Y' or payment_status == 'PENDING') and payment_response['GET'].get('policyref', None):
            transaction.policy_number = payment_response['GET']['policyref']
            transaction.payment_done = True
            transaction.payment_on = datetime.datetime.now()
            transaction.mark_complete()
            redirect_url = '/motor/' + self.Configuration.URL_TAG + \
                '/payment/success/%s/' % transaction.transaction_id

            mail_utils.send_mail_for_admin({
                'title': 'Policy number for bajaj-allianz',
                'type': 'policy',
                'data': 'Policy number for transaction %s (CFOX-%s) is %s (email: %s, request: %s)' % (transaction.transaction_id, transaction.id, transaction.policy_number, transaction.proposer.email, transaction.reference_id)
            })
        else:
            transaction.status = 'FAILED'
            transaction.save()

        return is_redirect, redirect_url, '', {}

    def get_dependent_addons(self, parameters):
        return {
            'isDepreciationWaiver': ['isEngineProtector'],
            'isEngineProtector': ['isDepreciationWaiver'],
            'type': 'dynamic'
        }

    def get_transaction_from_id(self, transaction_id):
        transaction = Transaction.objects.filter(
            reference_id=transaction_id).latest('id')
        return transaction

    def process_expired_policy(self, transaction_id):
        if self.Configuration.VEHICLE_TYPE == 'Twowheeler':
            return True
        return False

    def refresh_transaction_before_proposal(self, transaction):
        transaction.raw['last_quote_refresh'] = 0
        transaction.save()
        return True

    def get_custom_data(self, datatype):
        datamap = {
            # 'insurers': PAST_INSURANCE_ID_LIST,
            'insurers': custom_dictionaries.PAST_INSURER_ID_LIST_MASTER,
            'relationships': custom_dictionaries.NOMINEE_RELATIONSHIP_LIST,
        }
        return datamap.get(datatype, None)

    def disable_cache_for_request(self, request_data):
        if (request_data['isNewVehicle'] != '1' and
            datetime.datetime.strptime(
                request_data['pastPolicyExpiryDate'], '%d-%m-%Y'
                ).replace(hour=23, minute=59, second=0) < datetime.datetime.now() and
                self.Configuration.VEHICLE_TYPE == 'Private Car'):
            return True
        return False
