from motor_product.models import Vehicle, Insurer, VehicleMaster, VehicleMapping
from motor_product import parser_utils

INSURER_SLUG = 'bajaj-allianz'
INSURER_MASTER_PATH = 'motor_product/insurers/bajaj_allianz/scripts/data/bajaj_fourwheeler.csv'
MASTER_PATH = 'motor_product/scripts/fourwheeler/data/master_fourwheeler.csv'

csv_parameters = {
    'cubicCapacity': 0,
    'vehicleSubtypeCode': 2,
    'vehicleSubtype': 3,
    'vehicleCode': 4,
    'fuel': 5,
    'vehicleMakeCode': 6,
    'vehicleModel': 7,
    'vehicleModelCode': 8,
    'vehicleMake': 9,    
    'carryingCapacity': 15,
    'Master': 16,
}

parameters = {
    'vehicle_code': 4,
    'make_code': 6,
    'make': 9,
    'model_code': 8,
    'model': 7,
    'variant': 3,
    'variant_code': 2,
    'fuel_type': 5,
    'cc': 0,
    'seating_capacity': 15,
    'master': 16,
}

master_parameters = {
    'make': 0,
    'model': 1,
    'variant': 2,
    'fuel_type': 3,
    'cc': 4,
    'seating_capacity': 5,
    'insurer': 6,  # Refer to master csv
}


def convert_fuel_type_literals_into_fuel_type(fuel_type_literal):
    if fuel_type_literal == 'P':
        fuel_type = 'Petrol'
    elif fuel_type_literal == 'D':
        fuel_type = 'Diesel'
    elif fuel_type_literal == 'C':
        fuel_type = 'CNG / Petrol'
    elif fuel_type_literal == 'B':
        fuel_type = 'Electricity'
    else:
        fuel_type = 'Not Defined'
    return fuel_type


def save_data():
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_MASTER_PATH, parameters)
    insurer = Insurer.objects.get(slug=INSURER_SLUG)

    vehicle_objects = []
    for _vehicle in vehicles:
        _vehicle['cc'] = parser_utils.try_parse_into_integer(_vehicle['cc'])
        _vehicle['seating_capacity'] = parser_utils.try_parse_into_integer(_vehicle[
                                                                           'seating_capacity'])
        _vehicle['fuel_type'] = convert_fuel_type_literals_into_fuel_type(_vehicle[
                                                                          'fuel_type'])
        _vehicle['insurer'] = insurer

        del _vehicle['master']

        vehicle = Vehicle(**_vehicle)
        print 'Saving vehicle %s' % vehicle.model
        vehicle_objects.append(vehicle)

    Vehicle.objects.bulk_create(vehicle_objects)


def delete_data():
    Vehicle.objects.filter(vehicle_type='Private Car',
                           insurer__slug=INSURER_SLUG).delete()


def delete_mappings():
    VehicleMapping.objects.filter(
        insurer__slug=INSURER_SLUG, master_vehicle__vehicle_type='Private Car').delete()


def create_mappings():
    insurer = Insurer.objects.get(slug=INSURER_SLUG)
    master_vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        MASTER_PATH, master_parameters)
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_MASTER_PATH, parameters)

    ivehicles = Vehicle.objects.filter(
        vehicle_type='Private Car', insurer__slug=INSURER_SLUG)

    vehicle_map = {}
    for iv in vehicles:
        _vehicles = ivehicles.filter(
            vehicle_code=iv['vehicle_code'], variant=iv['variant'])
        if _vehicles.count() > 0:
            vehicle_map[iv['master']] = _vehicles[0]
        else:
            print 'Error: No data found for vehicle %s %s %s.' % (iv['make'], iv['model'], iv['variant'])

    mappings = []
    for mv in master_vehicles:
        _master = VehicleMaster.objects.get(make__name=mv['make'], model__name=mv['model'], variant=mv[
                                            'variant'], cc=mv['cc'], fuel_type=mv['fuel_type'], seating_capacity=mv['seating_capacity'])
        mapping = VehicleMapping()
        mapping.master_vehicle = _master
        mapping.mapped_vehicle = vehicle_map.get(mv['insurer'], None)
        mapping.insurer = insurer

        if mapping.mapped_vehicle:
            print 'Mapping %s to %s' % (_master, mapping.mapped_vehicle)
            mappings.append(mapping)

    VehicleMapping.objects.bulk_create(mappings)
