import difflib

from motor_product import models as motor_models

from motor_product.scripts.rto import master_rto_data as master_dicts
from motor_product.insurers.bajaj_allianz.scripts import bajaj_rto_data as bajaj_dicts

INSURER_SLUG = 'bajaj-allianz'


def delete_data():
    motor_models.RTOInsurer.objects.filter(insurer__slug=INSURER_SLUG).delete()


def save_data():
    CITY_INFO_DICT = bajaj_dicts.RTO_STATE_CODE_TO_CITY_LIST

    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)

    rtos = []
    for state_name, city_list in CITY_INFO_DICT.items():
        mrto_state = get_insurer_master_state(state_name)
        for city_name in city_list:
            rto_insurer = motor_models.RTOInsurer()
            rto_insurer.insurer = insurer
            rto_insurer.rto_code = city_name
            rto_insurer.rto_name = city_name
            rto_insurer.rto_state = state_name
            rto_insurer.master_rto_state = mrto_state
            rto_insurer.raw = {'city_name': city_name,
                               'city_code': city_name, 'state': state_name}
            print "Saving ---- %s - %s" % (city_name, mrto_state)
            rtos.append(rto_insurer)
    motor_models.RTOInsurer.objects.bulk_create(rtos)


def get_insurer_master_state(state):
    for state_code, state_name in bajaj_dicts.STATE_LIST.items():
        if state_name == state:
            break

    if state_code not in master_dicts.STATE_NAME_TO_STATE_CODE.values():
        print 'No state found for state ' + state
    return state_code


def create_map():
    fd = open(
        'motor_product/insurers/bajaj_allianz/scripts/data/bajaj_rto_map.csv', 'r')
    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)
    insurer_rtos = motor_models.RTOInsurer.objects.filter(insurer__slug=INSURER_SLUG)

    mappings = []
    for line in fd.readlines():
        line = line.split('\n')[0]
        rto_master_code, rto_insurer_code = line.split('|')
        master_rto = motor_models.RTOMaster.objects.get(rto_code=rto_master_code)
        insurer_rto = insurer_rtos.filter(rto_code=rto_insurer_code)[0]
        print 'Mapping %s to %s' % (master_rto, insurer_rto)
        if master_rto and insurer_rto:
            mapping = motor_models.RTOMapping()
            mapping.rto_master = master_rto
            mapping.rto_insurer = insurer_rto
            mapping.insurer = insurer
            mapping.is_approved_mapping = True
            mappings.append(mapping)
    fd.close()
    motor_models.RTOMapping.objects.bulk_create(mappings)


def delete_map():
    motor_models.RTOMapping.objects.filter(insurer__slug=INSURER_SLUG).delete()


def get_insurer_state(state):
    if state == 'TG':
        return 'AP'
    elif state == 'OR':
        return 'OD'
    elif state == 'UA':
        return 'UK'
    return state


def create_mappings():
    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)
    master_rto_data = motor_models.RTOMaster.objects.all()
    insurer_rto_data = motor_models.RTOInsurer.objects.filter(insurer=insurer)

    for master_rto in master_rto_data:
        mrto_name = master_rto.rto_name
        mrto_state = get_insurer_state(master_rto.rto_state)

        rtos_in_mrtos_state = insurer_rto_data.filter(
            master_rto_state=mrto_state)

        if len(rtos_in_mrtos_state) == 0:
            print 'No RTOs found for the state ' + mrto_state

        city_list = []
        for irto in rtos_in_mrtos_state:
            city_list.append(irto.rto_name.lower())

        matches = difflib.get_close_matches(
            mrto_name.lower(), city_list, cutoff=0.85)

        for match in matches:
            matched_irto = rtos_in_mrtos_state.filter(rto_name=match)
            if matched_irto.count() > 1:
                print 'More than one RTO found for a particular match. - ' + match
            matched_irto = matched_irto[0]
            mapping = motor_models.RTOMapping()
            mapping.rto_master = master_rto
            mapping.rto_insurer = matched_irto
            mapping.insurer = insurer
            print "Matching %s with %s" % (master_rto.rto_name, matched_irto.rto_name)
            mapping.save()


def delete_mappings():
    motor_models.RTOMapping.objects.filter(insurer__slug=INSURER_SLUG).delete()
