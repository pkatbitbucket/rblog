from dateutil.relativedelta import relativedelta

import custom_json_fields as jsonfields

from django import forms
from django.core.validators import RegexValidator
from django.utils import timezone
from motor_product.insurers.reliance import custom_dictionaries

# CHOICE FIELDS
YES_NO_CHOICES = (
    ("Yes", "1"),
    ("No", "0")
)

GENDER_CHOICES = (
    ('Male', 'Male'),
    ('Female', 'Female')
)

NCB_CHOICES = (
    ("0%", "0"),
    ("20%", "20"),
    ("25%", "25"),
    ("35%", "35"),
    ("45%", "45"),
    ("50%", "50")
)

ALLOWED_PREVIOUS_NCB = {
    '1': '0',
    '2': '20',
    '3': '25',
    '4': '35',
    '5': '45',
    '6': '50'
}

TEMP_VALUES = ('TEMP', 'Temp')

DL_MAPPING = {
    '1': (
        "01C", "01S", "1S", "01C", "1C",
    ),
    '2': (
        "02C", "2C", "02S", "2S",
    ),
    '3': (
        "03C", "3C", "03S", "3S",
    ),
    '4': (
        "04C", "4C", "04S", "4S"
    ),
    '5': (
        "05C", "5C", "05S", "5S"
    ),
    '6': (
        "06C", "6C", "06S", "6S"
    ),
    '7': (
        "07C", "7C", "07S", "7S"
    ),
    '8': (
        "08C", "8C", "08S", "8S"
    ),
    '9': (
        "09C", "9C", "09S", "9S"
    ),
    '10': (
        "10C",  "10S",
    ),
    '11': (
        "11C",  "11S",
    ),
    '12': (
        "12C",  "12S",
    ),
    '13': (
        "13C",  "13S",
    )
}

# START VALIDATORS
twoCharacterAlphabet = RegexValidator(
    r'^(.{2,}.*[ ][A-Za-z\.\'\`\-]{2,}.*)$',
    "The name should contain atleast two characters."
)
startWithAlphabet = RegexValidator(
    r'^[ ]*[A-Za-z]{1}.*',
    "The name should start only with an alphabet. Like from A-Z.")
mustHaveLastName = RegexValidator(
    r'^.+[ ]{1}.+',
    "Please enter a last name.")
onlySingleSpacesAllowed = RegexValidator(
    r'^(.*[ ]{2,}.*)+$',
    "You can't have more than 1 space",
    inverse_match=True)
userNameValidation = RegexValidator(
    r'^[A-Za-z\.\'\`\-\s]+$',
    "That doesn't look like a valid name. Try again?")
numericValidation = RegexValidator(
    r'^[0-9]+$',
    'Only numeric characters are allowed.')
emailValidation = RegexValidator(
    r'^([A-Za-z0-9]+([+._-]?[A-Za-z0-9]+)*)@((?:[A-Za-z0-9-]+\.)*\w[A-Za-z0-9-]{0,66})\.([A-Za-z]{2,}(?:\.[A-Za-z]{2})?)$',
    "Please enter a valid Email Address.")
ageValidation = RegexValidator(
    r'^(1[89]|[2-9][0-9])$',
    "Nominee cannot be less than 18 years of age.")
phnoValidation = RegexValidator(
    r'^([7-9][\d]{9})$',
    "Invalid Phone number")
panCardValidation = RegexValidator(
    r'^[A-z]{5}[0-9]{4}[A-z]{1}',
    "That doesn't look like a valid PAN number. Try again?")
alphaWithSpaceValidation = RegexValidator(
    r'^[a-z\s]*$',
    "Please enter a valid name.")
pincodeValidation = RegexValidator(
    r'^((1[1-9])|([2-9][0-9]))[0-9]{4}',
    "That doesn't look like a valid pincode. Try again?")
allNinePhoneNumberValidation = RegexValidator(
    r'^[9]{10}',
    "That doesn't look like a valid mobile number. Try again?",
    inverse_match=True)
policyNumberValidation = RegexValidator(
    r'^[A-Za-z\-\d]+$',
    'Previous Policy number should contain only alphabets (A-Z) or numbers (0-9).')
noSpaceAllowedValidation = RegexValidator(
    r'^(([^ ]|[ ])+.*[ ]{1,}.*)([^ ]|[ ])+$',
    "No spaces to be allowed in between.",
    inverse_match=True
)
alphaNumericOnlyValidator = RegexValidator(
    r'^[ ]*[A-z,0-9,*]+[ ]*$',
    'Invalid characters.'
)
onlyAlphabetsValidator = RegexValidator(
    r'^[A-z ]{1,}$',
    'Invalid characters'
)


def should_have_number(field_name):
    msg = "%s should contain both alpahbets (A-Z) and numbers (0-9)" % field_name
    return RegexValidator(r'^[\w\* ]*[0-9]+[\w]*', msg)


def should_have_alphabet(field_name):
    msg = "%s should contain both alpahbets (A-Z) and numbers (0-9)" % field_name
    return RegexValidator(r'^[\w\* ]*[A-z]+[\w]*', msg)


def rto_validator(rto, msg):
    regex = r'(^%s-.*)|(^.{1,%s})$' % (rto, len(rto))
    if 'DL' in rto:
        regex = '|'.join(map(lambda x: "(^DL-%s-.*)" % x, DL_MAPPING.get(rto.split('-')[-1], ())))
        regex += '|(^.{1,%s})$' % len(rto)
    return RegexValidator(
        regex,
        msg
    )


def min_length_validator(min_length, field_name):
    msg = '%s should be atleast %s characters long.' % (field_name, min_length)
    return RegexValidator(
        r'(([^ ]*[\w\d\!\@#\$\*.,\-\+].*){1,}){%s,}' % min_length,
        msg
    )


def min_alpha_numeric_validator(min_length, field_name):
    msg = "%s should be atleast %s characters long , excluding special characters." % (field_name, min_length)
    return RegexValidator(
        r'^(.*([A-z0-9]){1}.*){%s,}' % min_length,
        msg
    )

# END VALIDATORS


class ContactDetailForm(jsonfields.JsonForm):
    name = jsonfields.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Enter your full name here', 'id': 'cust_name', 'title': 'Your Name',
                   'summaryLabel': 'Name', 'description': 'Please use the name mentioned in the RC copy of your bike.'}),
        validators=[startWithAlphabet, userNameValidation, mustHaveLastName, onlySingleSpacesAllowed], max_length=120, required=True)
    email = jsonfields.CharField(
        widget=forms.TextInput(attrs={"title": "Email Address", "placeholder": "Email Address", 'id': 'cust_email',
                                      'display_type': "email", 'summaryLabel': 'Email',
                                      'description': "Only your policy will be emailed to this address, nothing else."}),
        validators=[jsonfields.CustomValidator(name='email', message="That doesn't look like a valid email address. Try again?")],
        required=True)
    phone_number = jsonfields.CharField(
        widget=forms.TextInput(attrs={
            'placeholder': 'Mobile Number', "title": "Mobile Number",
            'display_type': "mobile", "id": "cust_phone",
            'summaryLabel': 'Mobile Number',
            'description': 'Relax. We will only send you policy related messages on this number, nothing else.'
        }), required=True,
        validators=[jsonfields.CustomValidator(name='mobile',
                                               message="That doesn't look like a valid mobile number. Try again?")])

    def __init__(self, **kwargs):
        super(ContactDetailForm, self).__init__()
        transaction = kwargs['transaction']
        if transaction and transaction.insurer.slug == 'bharti-axa':
            self.fields['phone_number'].validators.append(allNinePhoneNumberValidation)
            self.fields['name'].validators.append(twoCharacterAlphabet)
            self.fields['name'].max_length = 61

        elif transaction and transaction.insurer.slug == 'iffco-tokio':
            self.fields['name'].max_length = 61
        elif transaction and transaction.insurer.slug in ('icici-lombard', 'hdfc-ergo'):
            self.fields['name'].max_length = 101
        if transaction and transaction.insurer.slug == 'hdfc-ergo':
            self.fields['name'].validators.append(onlyAlphabetsValidator)
        self.json_form_data = {
            "id": "primary-detail-container",
            "desc": "Fill the simple form on the left and ",
            "title": "Primary Details",
            "type": "container",
            "editLabel": "Edit",
            "submitLabel": "Next",
        }


class NomineeDetailForm(jsonfields.JsonForm):
    name = jsonfields.CharField(
        max_length=50,
        widget=forms.TextInput(attrs={'placeholder': "Nominee's Name", "title": "Nominee's Name", "id": "nominee_name",
                                      'summaryLabel': "Nominee's Name",
                                      "description": "Who would you like to nominate as a beneficiary for your accidental cover ?"}),
        validators=[startWithAlphabet, userNameValidation])
    age = jsonfields.CharField(
        widget=forms.TextInput(attrs={
            'placeholder': "Nominee's Age",
            'title': "Nominee's Age",
            "display_type": "numeric",
            "id": "nominee_age",
            'summaryLabel': "Nominee's Age",
            "customValidation": [
                {"name": 'ChildNomineeAgeDependencies', "message": "Your children should be at least 18 years younger to you."},
                {"name": 'ParentNomineeAgeDependencies', "message": "Your parents should be at least 18 years older to you."}
            ],
        }),
        validators=[
            numericValidation,
            ageValidation],
        min_length=2, max_length=2)
    relationship = jsonfields.ChoiceField(
        widget=forms.Select(attrs={
            "title": "Nominee's Relationship",
            "summaryLabel": "Nominee's Relationship",
            "placeholder": "Nominee's Relationship",
            "id": "nominee_relationship",
            "isSearchable": False,
            "is_disabled": True,
            "description": "Who would you like to nominate as a beneficiary for your accidental cover."
        }),
        choices=TEMP_VALUES)

    def __init__(self, **kwargs):
        super(NomineeDetailForm, self).__init__()
        choices = []
        for relationship in sorted(kwargs.get("buy_form_details", {}).get("relationship_list", [])):
            choices.append((relationship, relationship))
            if ('Other', 'Other') in choices:
                i = choices.index(('Other', 'Other'))
                choices.pop(i)
                if kwargs.get('transaction').insurer.slug != 'reliance':
                    choices.append(('Other', 'Other'))
        self.fields['relationship'] = jsonfields.ChoiceField(
            widget=forms.Select(attrs={
                "label": "Nominee's Relationship",
                "placeholder": "Nominee's Relationship",
                "id": "nominee_relationship",
                "summaryLabel": "Nominee's Relationship",
                "isSearchable": False,
            }),
            choices=choices)
        self.json_form_data = {
            "label": "Nominee Details",
            "id": "nominee_detail_subcontainer",
            "title": "Nominee",
            "type": "sub-container",
            "dependent_validations": [],
            "isSummaryCommaSeperated": True,

        }


class BasicDetailForm(jsonfields.JsonForm):
    gender = jsonfields.ChoiceField(
        widget=forms.RadioSelect(
            attrs={
                "label": "Gender",
                "id": "cust_gender",
                "title": "Your gender",
                'summaryLabel': 'Gender',
                "customValidation": [{"name": "GenderMaritalStatusNomineeDependency"}],
            }),
        choices=GENDER_CHOICES,)
    married = jsonfields.ChoiceField(
        widget=forms.RadioSelect(
            attrs={
                "label": "Married",
                "id": "cust_marital_status",
                "title": "Are you married?",
                'summaryLabel': 'Married',
                "displayValueMap": {"1": "Yes", "0": "No"},
                "customValidation": [{"name": "GenderMaritalStatusNomineeDependency"}],
            }),
        choices=YES_NO_CHOICES)
    date_of_birth = jsonfields.DateTimeField(widget=forms.DateTimeInput(attrs={
        "label": "Date of Birth",
        "min_date": jsonfields.years_from_today(-100).strftime("%d-%m-%Y"),
        "max_date": jsonfields.years_from_today(-18).strftime("%d-%m-%Y"),
        "format": "DD-MM-YYYY",
        "id": "cust_dob",
        'summaryLabel': 'Date of Birth',
        "title": "Your date of birth",
        "defaultDisplayMonthDate": jsonfields.years_from_today(-25).strftime("%d-%m-%Y")
    }))
    pan_card = jsonfields.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'PAN Number', "label": "PAN Number", "id": "cust_pan"}),
        validators=[panCardValidation],
        min_length=10, max_length=10)

    def __init__(self, **kwargs):
        super(BasicDetailForm, self).__init__()
        self.kwargs = kwargs
        self.json_form_data = {
            "id": "basic-detail-container",
            "desc": "if any",
            "title": "Basic Details",
            "type": "container",
            "editLabel": "Edit",
            "submitLabel": "Next",
            "children": []
        }
        pan_card_insurers = ['future-generali', 'hdfc-ergo', 'icici-lombard', 'iffco-tokio', 'reliance', 'tata-aig',
                             'royal-sundaram']
        pan_card = self.fields.pop('pan_card')
        if kwargs.get('transaction', None) and kwargs.get('transaction', None).raw.get('quote_response', None) and \
                        kwargs['transaction'].insurer.slug in pan_card_insurers:
            from motor_product.prod_utils import get_flat_premium
            premium = get_flat_premium(kwargs['user_form_details'],
                                       [kwargs['transaction'].raw['quote_response']])[0]['final_premium']
            if premium > 50000:
                self.fields.update({'pan_card': pan_card})

    def form_to_json(self, **kwargs):
        return_data = super(BasicDetailForm, self).form_to_json()
        return_data['children'].append(NomineeDetailForm(**self.kwargs).form_to_json())
        return return_data


class VehicleDetailsForm(jsonfields.JsonForm):
    registration_number = jsonfields.CharField(
        widget=forms.TextInput(attrs={
            "display_type": "registration-number",
            "id": "vehicle_reg_no",
            "isRefreshWithExternalValue": False,
            "title": "Your Bike Number",
            'summaryLabel': 'Bike Number',
            "placeholder": "Bike Number"})
    )
    registration_date = jsonfields.DateTimeField(
        widget=forms.DateTimeInput(attrs={
            "placeholder": "Bike Registration Date",
            'summaryLabel': 'Registration Date',
            "title": "Bike Registration Date",
            "min_date": jsonfields.years_from_today(-10).strftime("%d-%m-%Y"),
            "max_date": jsonfields.days_from_today(0).strftime("%d-%m-%Y"),
            "format": "DD-MM-YYYY",
            "id": "registration_date",
            "defaultDisplayMonthDate": jsonfields.years_from_today(-1).strftime("%d-%m-%Y")
        }))
    engine_number = jsonfields.CharField(
        widget=forms.TextInput(attrs={
            "placeholder": "Engine Number",
            "title": "Engine Number",
            "summaryLabel": "Engine Number",
            "id": "vehicle_engine_no"}),
        validators=[noSpaceAllowedValidation, alphaNumericOnlyValidator],
        max_length=25)
    chassis_number = jsonfields.CharField(
        widget=forms.TextInput(attrs={
            "placeholder": "Chassis Number",
            "title": "Chassis Number",
            "summaryLabel": "Chassis Number",
            "id": "vehicle_chassis_no"}),
        validators=[noSpaceAllowedValidation, alphaNumericOnlyValidator],
        max_length=25)
    is_car_on_loan = jsonfields.ChoiceField(
        widget=forms.RadioSelect(attrs={
            "placeholder": "Is bike currently on loan?",
            'summaryLabel': 'Bike on Loan',
            "id": "is_car_financed",
            "displayValueMap": {"1": "Yes", "0": "No"},
            "customValidation": [{"name": "OnLoanChangeDependency"}],
        }),
        choices=YES_NO_CHOICES)
    loan_provider = jsonfields.ChoiceField(
        widget=forms.Select(attrs={
            "placeholder": "Loan Provider's Name",
            "title": "Loan Provider's Name",
            'summaryLabel': 'Loan Provider',
            "id": "financier-name",
            "isSearchable": True,
            "description": "Who did you take the bike loan from? Don't worry, we aren't snooping.",
            "displayValueMap": {"1": "Yes", "0": "No"},
        }),
        choices=YES_NO_CHOICES)

    def __init__(self, **kwargs):
        super(VehicleDetailsForm, self).__init__()
        transaction = kwargs.get('transaction', None)
        if transaction and transaction.quote.raw_data.get('fastlane_success', "0") == '1':
            self.fields.pop('registration_number')

        if kwargs['user_form_details'].get("autopopulated_car_financer", False) and kwargs['user_form_details'].get("financier-name", "") not in ["", False]:
            self.fields.pop('loan_provider')
            self.fields.pop('is_car_on_loan')
        else:
            choices = []
            if kwargs.get("buy_form_details", {}).get("financier_list"):
                for financier_id, financier_name in kwargs.get("buy_form_details", {}).get("financier_list").items():
                    choices.append((financier_name, financier_id))
            else:
                for financier_id, financier_name in custom_dictionaries.FINANCIERS.items():
                    choices.append((financier_name, financier_name))
            self.fields['loan_provider'] = jsonfields.ChoiceField(
                widget=forms.Select(attrs={
                    "placeholder": "Loan Provider's Name",
                    "label": "Loan Provider's Name",
                    "title": "Loan Provider's Name",
                    'summaryLabel': 'Loan Provider',
                    "description": "Who did you take the bike loan from? Don't worry, we aren't snooping.",
                    "id": "financier-name",
                    "isSearchable": True,
                }),
                choices=choices)

        if transaction and transaction.quote.accepted_fastlane:
            self.fields.pop('engine_number')
            self.fields.pop('chassis_number')
            self.fields.pop('registration_date')

        elif self.fields.get('engine_number', None) and transaction and transaction.insurer.slug == 'hdfc-ergo':
            self.fields['engine_number'].validators.extend([min_alpha_numeric_validator(5, 'Engine Number'),
                                                            should_have_alphabet('Engine Number'),
                                                            should_have_number('Engine Number')])
            self.fields['chassis_number'].validators.extend([min_alpha_numeric_validator(5, 'Chassis Number'),
                                                             should_have_alphabet('Chassis Number'),
                                                            should_have_number('Chassis Number')])
        elif self.fields.get('engine_number', None) and transaction and transaction.insurer.slug == 'bharti-axa':
            self.fields['engine_number'].validators.extend([min_alpha_numeric_validator(5, 'Engine Number')])
            self.fields['chassis_number'].validators.extend([min_alpha_numeric_validator(5, 'Chassis Number')])
        elif self.fields.get('engine_number', None) and transaction and transaction.insurer.slug == 'l-t':
            self.fields['engine_number'].validators.extend([min_alpha_numeric_validator(7, 'Engine Number')])
            self.fields['chassis_number'].validators.extend([min_alpha_numeric_validator(7, 'Chassis Number')])
        elif self.fields.get('engine_number', None) and transaction and transaction.insurer.slug == 'iffco-tokio':
            self.fields['engine_number'].max_length = 20
            self.fields['chassis_number'].max_length = 20
        else:
            self.fields['engine_number'].validators.extend([min_alpha_numeric_validator(1, 'Engine Number')])
            self.fields['chassis_number'].validators.extend([min_alpha_numeric_validator(1, 'Chassis Number')])
        if self.fields.get('registration_number') and transaction:
            rto_code = transaction.quote.rto if '-' in transaction.quote.rto else \
                transaction.quote.rto.replace(transaction.quote.rto_info['rto_state'], transaction.quote.rto_info['rto_state'] + '-')
            msg = 'You selected %s as your RTO. Registration number should contain that.' % rto_code if rto_code[:2] in ['DL', "GJ", "UK", "UA"] \
                else 'You selected %s as your RTO. Registration number should contain that.' % rto_code.replace('-', '')
            self.fields['registration_number'].validators = [rto_validator(rto_code, msg)]
        self.json_form_data = {
            "id": "vehicle_details_subcontainer",
            "desc": "if any",
            "title": "Vehicle Details",
            "type": "sub-container",
            "editLabel": "Edit",
            "submitLabel": "Next"
        }


class PastPolicyForm(jsonfields.JsonForm):
    past_policy_expiry_date = jsonfields.DateTimeField(
        widget=forms.DateTimeInput(attrs={
            "placeholder": "Previous Policy Expiry Date",
            "min_date": jsonfields.days_from_today(-100).strftime("%d-%m-%Y"),
            "max_date": jsonfields.days_from_today(450).strftime("%d-%m-%Y"),
            "format": "DD-MM-YYYY",
            "id": "past_policy_expiry_date",
            'summaryLabel': 'Previous Policy Expiry',
            "defaultDisplayMonthDate": jsonfields.days_from_today(0).strftime("%d-%m-%Y"),
            "customValidation": [{"name": "NCBClaimVisibility"}]
        }))
    past_policy_insurer = jsonfields.ChoiceField(
        widget=forms.Select(attrs={
            "placeholder": "Previous Policy Insurer",
            "title": "Previous Policy Insurer",
            "label": "Previous Policy Insurer",
            "summaryLabel": "Previous Policy Insurer",
            "id": "past_policy_insurer",
            "isSearchable": True}),
        choices=GENDER_CHOICES)
    past_policy_number = jsonfields.CharField(
        widget=forms.TextInput(attrs={
            "placeholder": "Previous Policy Number",
            "summaryLabel": "Previous Policy Number",
            "id": "past_policy_number"}),
        max_length=30,
        validators=[policyNumberValidation])
    claimed_last_year = jsonfields.ChoiceField(
        widget=forms.RadioSelect(attrs={
            "placeholder": "Have you claimed in the past year?",
            "id": "claimed_last_year",
            'summaryLabel': 'Claimed Last Year',
            "displayValueMap": {"1": "Yes", "0": "No"},
            "customValidation": [{"name": "NCBClaimVisibility"}]
        }),
        choices=YES_NO_CHOICES)
    previous_ncb = jsonfields.ChoiceField(
        widget=forms.Select(attrs={
            "placeholder": "NCB in Previous Policy",
            "summaryLabel": "NCB in Previous Policy",
            "title": "NCB in Previous Policy",
            "id": "previous_ncb",
            "isSearchable": False,
        }),
        choices=NCB_CHOICES)

    def __init__(self, **kwargs):
        super(PastPolicyForm, self).__init__()
        choices = []
        for insurer_id, insurer_name in kwargs.get("buy_form_details", {}).get("insurers_list", {}).items():
            if insurer_id not in kwargs.get('buy_form_details', {}).get('disallowed_insurers', {}):
                choices.append((insurer_name, insurer_id))
        if kwargs.get('transaction') and kwargs['transaction'].insurer.slug == 'reliance':
            self.fields['past_policy_number'].validators.append(min_length_validator(8, 'Previous Policy Number'))
        self.fields['past_policy_insurer'] = jsonfields.ChoiceField(
            widget=forms.Select(attrs={
                "label": "Previous Policy Insurer",
                "title": "Previous Policy Insurer",
                "placeholder": "Previous Policy Insurer",
                "summaryLabel": "Previous Policy Insurer",
                "id": "past_policy_insurer",
                "isSearchable": True,
            }),
            choices=choices)
        if kwargs['user_form_details'].get("autopopulated_past_policy_insurer", "false") in ['true', "1", 1]:
            self.fields.pop('past_policy_insurer')
        if kwargs['transaction'].insurer.slug in ['hdfc-ergo', 'iffco-tokio', 'bajaj-allianz']:
            try:
                reg_date = timezone.make_aware(kwargs['transaction'].quote.policy_registration_date(),
                                               timezone.get_default_timezone())
            except:
                reg_date = None
            if reg_date:
                today = timezone.localtime(timezone.now())
                years = relativedelta(today, reg_date).years
                TEMP_NCB_CHOICES = []
                for year, value in ALLOWED_PREVIOUS_NCB.items():
                    if int(year) <= years:
                        TEMP_NCB_CHOICES.append(("{0}%".format(value), "{0}".format(value)))
                if not TEMP_NCB_CHOICES:
                    TEMP_NCB_CHOICES.append(('0%', '0'))
                TEMP_NCB_CHOICES = sorted(TEMP_NCB_CHOICES, key=lambda tup: tup[1])
                self.fields['previous_ncb'] = jsonfields.ChoiceField(
                    widget=forms.Select(attrs={
                        "placeholder": "Previous Policy NCB",
                        "id": "previous_ncb",
                        "summaryLabel": "NCB in Previous Policy",
                        "isSearchable": False,
                    }),
                    choices=TEMP_NCB_CHOICES)
                if len(TEMP_NCB_CHOICES) == 1 and TEMP_NCB_CHOICES[0] == ("0%", "0"):
                    self.fields.pop('claimed_last_year')
                    self.fields.pop('previous_ncb')
        min_expiry_date = jsonfields.days_from_today(0).strftime("%d-%m-%Y")
        max_expiry_date = jsonfields.days_from_today(60).strftime("%d-%m-%Y")
        if kwargs['transaction'].quote.raw_data['isExpired'] == 'true':
            min_expiry_date = kwargs['transaction'].quote.raw_data.get('registrationDate', jsonfields.years_from_today(-5).strftime("%d-%m-%Y"))
            max_expiry_date = jsonfields.days_from_today(-1).strftime("%d-%m-%Y")
        self.fields['past_policy_expiry_date'].widget.attrs.update({"min_date": min_expiry_date, "max_date": max_expiry_date})
        self.json_form_data = {
            "id": "past_policy_subcontainer",
            "desc": "if any",
            "title": "Past Policy Details",
            "type": "sub-container",
            "editLabel": "Edit",
            "submitLabel": "Next"
        }


class FinalVehicleDetailForm(jsonfields.JsonForm):

    def __init__(self, **kwargs):
        super(FinalVehicleDetailForm, self).__init__()
        vehicle_form = VehicleDetailsForm(**kwargs)
        past_policy_form = PastPolicyForm(**kwargs)

        if kwargs['transaction'].quote.accepted_fastlane:
            if vehicle_form.fields.get('is_car_on_loan'):
                past_policy_form.fields['is_car_on_loan'] = vehicle_form.fields['is_car_on_loan']
                past_policy_form.fields['loan_provider'] = vehicle_form.fields['loan_provider']

        vehicle_form = vehicle_form.form_to_json()
        past_policy_form = past_policy_form.form_to_json()

        children_form = []
        if vehicle_form['children'] and not kwargs['transaction'].quote.accepted_fastlane:
            children_form.append(vehicle_form)
        if past_policy_form['children']:
            children_form.append(past_policy_form)
        self.json_form_data = {
            "label": "Vehicle Details",
            "id": "vehicle_details_container",
            "desc": "if any",
            "title": "Vehicle Details",
            "type": "container",
            "editLabel": "Edit",
            "submitLabel": "Next",
            "children": [{
                "label": "Vehicle Details",
                "id": "vehicle_details_tabbedcontainer",
                "desc": "if any",
                "title": "Vehicle Details",
                "type": "tabbed-container",
                "editLabel": "Edit",
                "submitLabel": "Next",
                "children": children_form,
            }],
        }


class AddressForm(jsonfields.JsonForm):
    pincode = jsonfields.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': "Area Pincode",
                'summaryLabel': "Area Pincode",
                'title': "Area Pincode",
                "display_type": "numeric",
                "id": "add_pincode",
                "customValidation": [{"name": "PinCodeCommAddressDependency"}],
            }),
        validators=[pincodeValidation],
        min_length=6, max_length=6)
    address_field = jsonfields.CharField(
        widget=forms.TextInput(attrs={
            "placeholder": "Postal Address (House, Building, Street)",
            "title": "Postal Address",
            "summaryLabel": "Postal Address",
            "id": "add_consoliated_field"}),
        max_length=90)
    landmark = jsonfields.CharField(
        widget=forms.TextInput(attrs={
            "placeholder": "Any landmark nearby",
            "summaryLabel": "Landmark",
            "title": "Landmark",
            "id": "add_landmark"}),
        max_length=30)
    state = jsonfields.ChoiceField(
        widget=forms.Select(attrs={
            "placeholder": "State",
            "summaryLabel": "State",
            "id": "add_state",
            "isSearchable": True,
            "customValidation": [{"name": "GetCommCitiesFromState"}],
        }),
        choices=[])
    city = jsonfields.ChoiceField(
        widget=forms.Select(attrs={
            "placeholder": "City",
            "summaryLabel": "City",
            "id": "add_city",
            "isSearchable": True,
            "is_disabled": False,
        }),
        choices=[])

    def __init__(self, **kwargs):
        super(AddressForm, self).__init__()

        if kwargs['transaction'].insurer.slug == 'bajaj-allianz':
            self.fields.pop("state")
            self.fields.pop("city")
            self.fields['pincode'].widget.attrs['customValidation'] = []
        else:
            choices = []
            state_list = kwargs.get("buy_form_details", {}).get("state_list", {})
            if type(state_list) == list:
                state_list = {e: e for e in state_list}
            for state, state_id in state_list.items():
                choices.append((state_id, state.upper()))
            self.fields['state'] = jsonfields.ChoiceField(
                widget=forms.Select(attrs={
                    "placeholder": "State",
                    "summaryLabel": "State",
                    "id": "add_state",
                    "isSearchable": True,
                    "customValidation": [{"name": "GetCommCitiesFromState"}],
                }),
                choices=choices)
            city_choices = []
            cities = kwargs.get("buy_form_details", {}).get("cities", {})
            city_options = {}
            if type(cities) == list:
                for city in cities:
                    if type(city) == dict:
                        city_options[city[city.keys()[0]]] = city[city.keys()[1]]
                    else:
                        city_options[city] = city
            for city, city_id in city_options.items():
                city_choices.append((city_id, city.upper()))
            self.fields['city'] = jsonfields.ChoiceField(
                widget=forms.Select(attrs={
                    "placeholder": "City",
                    "summaryLabel": "City",
                    "id": "add_city",
                    "isSearchable": True,
                    "is_disabled": False
                }),
                choices=city_choices)
        if kwargs['transaction'].insurer.slug == 'reliance':
            self.fields['landmark'].max_length = 35
            self.fields['address_field'].max_length = 105
        if kwargs['transaction'].insurer.slug == 'bharti-axa':
            self.fields['address_field'].max_length = 30
            self.fields['landmark'].validators.append(min_length_validator(2, 'Landmark'))
            self.fields['address_field'].validators.append(min_length_validator(2, 'Address'))
        if kwargs['transaction'].insurer.slug == 'iffco-tokio':
            self.fields['address_field'].max_length = 30
        if kwargs['transaction'].insurer.slug in ['hdfc-ergo', 'reliance']:
            self.fields['state'].widget.attrs['customValidation'].append({"name": "handleRegAddress"})

        self.json_form_data = {
            "label": "Address Details",
            "id": "communication_address_container",
            "desc": "if any",
            "title": "Communication Address",
            "type": "sub-container",
            "editLabel": "Edit",
            "submitLabel": "Next"
        }


class RegistrationAddressForm(AddressForm):

    def __init__(self, **kwargs):
        super(AddressForm, self).__init__()
        self.json_form_data = {
            "label": "Address Details",
            "id": "registration_address_container",
            "desc": "if any",
            "title": "Registration Address",
            "type": "sub-container",
            "editLabel": "Edit",
            "submitLabel": "Next"
        }
        for field, val in self.fields.items():
            val.widget.attrs['id'] = "reg_" + val.widget.attrs.get('id')
        self.fields['pincode'].widget.attrs['customValidation'] = [{"name": "PinCodeRegAddressDependency"}]
        choices = []
        state_list = kwargs.get("buy_form_details", {}).get("state_list", {})
        if type(state_list) == list:
            state_list = {e: e for e in state_list}
        for state, state_id in state_list.items():
            choices.append((state_id, state))
        self.fields['state'] = jsonfields.ChoiceField(
            widget=forms.Select(attrs={
                "placeholder": "State",
                "summaryLabel": "State",
                "id": "reg_add_state",
                "isSearchable": True,
                "is_disabled": True,
                "customValidation": [{"name": "GetRegCitiesFromState"}],
            }),
            choices=choices)
        city_choices = []
        cities = kwargs.get("buy_form_details", {}).get("cities", {})
        city_options = {}
        if type(cities) == list:
            for city in cities:
                if type(city) == dict:
                    city_options[city[city.keys()[0]]] = city[city.keys()[1]]
                else:
                    city_options[city] = city
        for city, city_id in city_options.items():
            city_choices.append((city_id, city))
        self.fields['city'] = jsonfields.ChoiceField(
            widget=forms.Select(attrs={
                "placeholder": "City",
                "summaryLabel": "City",
                "id": "reg_add_city",
                "isSearchable": True,
                "is_disabled": False
            }),
            choices=city_choices)


class FinalAddressForm(jsonfields.JsonForm):
    pass

    def __init__(self, **kwargs):
        super(FinalAddressForm, self).__init__()
        address_components = [AddressForm]
        if kwargs['transaction'].insurer.slug in ['hdfc-ergo', 'reliance']:
            address_components.append(RegistrationAddressForm)
        self.json_form_data = {
            "label": "Address",
            "id": "address_container",
            "desc": "if any",
            "title": "Address",
            "type": "container",
            "editLabel": "Edit",
            "submitLabel": "Next",
            "children": [{
                "label": "Address",
                "id": "address_tab_container",
                "desc": "if any",
                "title": "Vehicle Details",
                "type": "tabbed-container",
                "editLabel": "Edit",
                "submitLabel": "Next",
                "children": [form(**kwargs).form_to_json() for form in address_components],
            }],
        }


def generate_final_form(**kwargs):
    return_data = [form(**kwargs).form_to_json() for form in [ContactDetailForm, BasicDetailForm, FinalAddressForm,
                                                              FinalVehicleDetailForm]]
    return return_data
