import datetime
import json
import math
import re
import traceback
import xml.etree.ElementTree as ET
from collections import OrderedDict
import putils
import httplib2
from django.conf import settings
from lxml import etree

from motor_product import logger_utils, mail_utils, parser_utils
from motor_product.insurers import insurer_utils
from motor_product.models import Insurer, Transaction, Vehicle, VehicleMaster, IntegrationStatistics
from utils import motor_logger, log_error

from . import custom_dictionaries

INSURER_SLUG = 'bharti-axa'

try:
    INSURER = Insurer.objects.get(slug=INSURER_SLUG)
    INSURER_ID = INSURER.id
    INSURER_NAME = INSURER.title
except:
    INSURER_ID = 0
    INSURER_NAME = ''

suds_logger = logger_utils.getLogger(INSURER_SLUG)

VEHICLE_AGE_LIMIT = 10

USED_VEHICLE_AGE_LIMIT = 7
USED_VEHICLE_DISCOUNT_LIMIT = 50

EXPIRED_VEHICLE_AGE_LIMIT = 7
EXPIRED_VEHICLE_DISCOUNT_LIMIT = 50

SERVICE_TAX_RATE = 0.145
# ######### PRODUCTION CREDENTIALS ########
PAYMENT_URL = 'https://www.smartlinkxr.bharti-axagi.co.in/cordys/bagi/eSuite/B2C/payMe.aspx'
HOST_URL = 'https://www.smartlinkxr.bharti-axagi.co.in/'
USERNAME = 'coverFox'
PASSWORD = 'kp/Jmd8C3IKOOx9u+ujqgDiPxv6WwV+Q/yLncn2QcxikNu/3TMcWLEukiiwCToXS7Nit7pBckn4IZhWnBig4iQ=='
PREMIUM_URL = 'https://www.smartlinkxr.bharti-axagi.co.in/cordys/\
com.eibus.web.soap.Gateway.wcp?organization=o=AXA-IN,cn=cordys,cn=CordysBOP4PRD,o=axa_sgp.axa-ap.intraxa'
# ######### PRODUCTION CREDENTIALS ########

# Method to send raw request to a url, pass etree and url.


def get_soap_request_response(name, session_etree, url):
    SessionDoc = etree.Element('SessionDoc')
    SessionDoc.append(session_etree)
    serve = etree.Element(
        'serve', xmlns='http://schemas.cordys.com/gateway/Provider')
    serve.append(SessionDoc)
    Body = etree.Element('Body')
    Body.append(serve)
    Envelope = etree.Element(
        'Envelope', xmlns='http://schemas.xmlsoap.org/soap/envelope/')
    Envelope.append(Body)
    request = etree.tostring(Envelope)
    motor_logger.info('\nBHARTI REQUEST SENT:\n%s\n' % request)
    suds_logger.set_name(name).log(request)
    http = httplib2.Http(disable_ssl_certificate_validation=True)
    headers = {"Content-type": "text/xml"}
    response_header, response_content = http.request(
        url, method='POST', body=request, headers=headers)
    motor_logger.info('\nBHARTI RESPONSE:\n%s\n' % response_content)
    suds_logger.set_name(name).log(response_content)
    return response_header, response_content


class Configuration(object):

    def __init__(self, vehicle_type):
        self.VEHICLE_TYPE = vehicle_type
        if vehicle_type == 'Private Car':
            self.RISK_TYPE = 'FPV'
            self.URL_TAG = 'fourwheeler'
            self.RESPONSE_URL = settings.SITE_URL.strip(
                "/") + '/motor/fourwheeler/insurer/bharti-axa/payment/response/%s/?'
        elif vehicle_type == 'Twowheeler':
            self.RISK_TYPE = 'FTW'
            self.URL_TAG = 'twowheeler'
            self.RESPONSE_URL = settings.SITE_URL.strip(
                "/") + '/motor/twowheeler/insurer/bharti-axa/payment/response/%s/?'


class PremiumCalculator(object):

    def __init__(self, integrationUtils, configuration):
        self.parent = integrationUtils
        self.Configuration = configuration

    def get_premium(self, data_dictionary):
        if int(data_dictionary['vehicleAge']) >= 7 and data_dictionary['third_party'] == 'cardekho':
            return {'success': False, 'error_code': IntegrationStatistics.COVERFOX_TP_RESTRICTED}

        if int(data_dictionary['vehicleAge']) >= VEHICLE_AGE_LIMIT:
            motor_logger.info(
                'Vehicle age limit not valid for this insurer %s.' % INSURER_SLUG)
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_AGE_RESTRICTED}

        if data_dictionary['isUsedVehicle'] == '1' and (
           int(data_dictionary['vehicleAge']) >= USED_VEHICLE_AGE_LIMIT):
            motor_logger.info(
                'Vehicle age limit not valid for this insurer %s.' % INSURER_SLUG)
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_AGE_RESTRICTED}

        if data_dictionary['isExpired']:

            if int(data_dictionary['vehicleAge']) >= EXPIRED_VEHICLE_AGE_LIMIT:
                motor_logger.info(
                    'Vehicle age limit not valid for this insurer %s.' % INSURER_SLUG)
                return {'success': False, 'error_code': IntegrationStatistics.INSURER_EXPIRED_POLICY_RESTRICTED}

        premium_request_dict = self._get_required_params_for_premium_calculation(
            data_dictionary)
        idv, min_idv, max_idv = [int(data_dictionary[e]) for e in ('idv', 'min_idv', 'max_idv')]

        Session = etree.Element('Session')
        Session = parser_utils.dict_to_etree(Session, premium_request_dict)

        response_header, response_content = get_soap_request_response(
            data_dictionary['quoteId'], Session, PREMIUM_URL)
        response_content = re.sub(r' xmlns="(.*?)"', '', response_content)
        response_dict = parser_utils.complex_etree_to_dict(
            ET.fromstring(response_content))
        premium_response = response_dict['Envelope']['Body']['serveResponse']['tuple']['old']['serve']['serve'][
            '{http://schemas.xmlsoap.org/soap/envelope/}Envelope'][
            '{http://schemas.xmlsoap.org/soap/envelope/}Body']['processTPRequestResponse']['response']
        premium_response['addon_isDepreciationWaiver'] = data_dictionary[
            'quote_SelectedCovers']['quote_ZeroDepriciationSelected']
        premium_response['addon_is247RoadsideAssistance'] = data_dictionary[
            'quote_SelectedCovers']['quote_RoadsideAssistanceSelected']
        premium_response['addon_isInvoiceCover'] = data_dictionary[
            'quote_SelectedCovers']['quote_InvoicePriceSelected']
        premium_response['addon_isNcbProtection'] = data_dictionary[
            'quote_SelectedCovers']['quote_NoClaimBonusSameSlabSelected']
        premium_response['addon_isCostOfConsumables'] = data_dictionary[
            'quote_SelectedCovers']['quote_CosumableCoverSelected']
        premium_response['addon_isHydrostaticLock'] = data_dictionary[
            'quote_SelectedCovers']['quote_HydrostaticLockSelected']
#        premium_response['addon_isEngineProtector'] = data_dictionary[
#             'quote_SelectedCovers']['quote_EngineGearBoxProtectionSelected']
        premium_response['isUsedVehicle'] = data_dictionary['isUsedVehicle']
        premium_response['isExpired'] = data_dictionary['isExpired']
        return self.parent.convert_premium_response(premium_response, int(data_dictionary['min_idv']),
                                                    int(data_dictionary['max_idv']), int(data_dictionary['idv']),
                                                    data_dictionary['VehicleId']
                                                    )

    def _get_required_params_for_premium_calculation(self, data_dictionary):
        vehicle = Vehicle.objects.filter(insurer__slug=INSURER_SLUG).get(
            id=data_dictionary['VehicleId'])

        min_idv, idv, max_idv = self._get_vehicle_idv_from_ex_showroom_price(
            data_dictionary, vehicle)
        request_idv = float(data_dictionary['idv'])
        if request_idv < float(min_idv):
            data_dictionary['idv'] = min_idv
        elif request_idv > float(max_idv):
            data_dictionary['idv'] = max_idv

        data_dictionary.update(
            {'min_idv': int(min_idv), 'max_idv': int(max_idv)})
        return OrderedDict([
            ('SessionData', self._get_premium_session_data_dict()),
            ('Vehicle', self._get_premium_vehicle_dict(data_dictionary, vehicle)),
            ('Quote', self._get_premium_quote_dict(data_dictionary)),
            ('Client', self._get_premium_client_dict(data_dictionary)),
        ])

    def _get_vehicle_idv_from_ex_showroom_price(self, data_dictionary, vehicle):
        ex_showroom_price = float(vehicle.ex_showroom_price)
        depreciation_rate = float(custom_dictionaries.VEHICLE_AGE_TO_DEPRECIATION_MAPPING[
                                  str(int(data_dictionary['vehicleAge']))])
        idv = ex_showroom_price * ((100 - depreciation_rate) / 100)
        min_idv = 0.70 * idv
        max_idv = 1.30 * idv
        return min_idv, idv, max_idv

    def _get_premium_session_data_dict(self):
        return OrderedDict([
            ('Index', '1'),
            ('InitTime', datetime.datetime.utcnow().strftime("%a, %d %b %Y %H:%M:%S GMT")),
            ('UserName', USERNAME),
            ('Password', PASSWORD),
            ('OrderNo', 'NA'),
            ('QuoteNo', 'NA'),
            ('Route', 'INT'),
            ('Contract', 'MTR'),
            ('Channel', ''),
            ('TransactionType', 'Quote'),
            ('TransactionStatus', 'Fresh'),
            ('ID', ''),
            ('UserAgentID', ''),
            ('Source', ''),
        ])

    def _get_premium_vehicle_dict(self, data, vehicle):
        # current_date_time = datetime.now()
        registration_date = datetime.datetime(int(data['VehicleBuyingYear']), int(
            data['VehicleBuyingMonth']), int(data['VehicleBuyingDay']))
        vehicle_dict = OrderedDict([
            ('TypeOfBusiness', 'NB'),
            ('AccessoryInsured', 'Y'),
            ('AccessoryValue', '0'),
            ('BiFuelKit', OrderedDict(
                [('IsBiFuelKit', 'N'), ('BiFuelKitValue', '0'), ('ExternallyFitted', 'N'), ])),
            ('DateOfRegistration', registration_date.strftime("%Y-%m-%dT%H:%M:%S.000")),
            ('RiskType', self.Configuration.RISK_TYPE),
            ('Make', vehicle.make),
            ('Model', vehicle.model),
            ('FuelType', custom_dictionaries.FUEL_TYPE_MAPPING_LIST[vehicle.fuel_type]),
            ('Variant', vehicle.variant),
            ('IDV', data['idv']),
            ('VehicleAge', max(int(math.ceil(data['vehicleAge'])), 1)),
            ('CC', '%s%s' % (('0' if len(str(vehicle.cc)) < 3 else ''), vehicle.cc)),
            ('SeatingCapacity', vehicle.seating_capacity),
            ('PlaceOfRegistration', custom_dictionaries.RTO_CODE_TO_RTO_INFO[
             data['RegistrationCity']]['RTO_City']),
            ('VehicleExtraTag01', None),
            ('RegistrationNo', None),
            ('ExShowroomPrice', vehicle.raw_data.split('|')[5]),
        ])
        insurer_utils.update_complex_dict(vehicle_dict, data, 'vehicle_')
        if vehicle_dict['BiFuelKit']['ExternallyFitted'] == 'Y':
            vehicle_dict['FuelType'] = 'C'
        elif vehicle_dict['BiFuelKit']['IsBiFuelKit'] == 'Y':
            vehicle_dict['FuelType'] = 'G'
        return vehicle_dict

    def _get_premium_quote_dict(self, data_dictionary):
        policy_start_date = datetime.datetime.now()
        quote_dict = OrderedDict([
            ('ExistingPolicy', OrderedDict(
                [('Claims', None), ('PolicyType', None), ('EndDate', None), ('NCB', None)])),
            ('PolicyStartDate', policy_start_date.strftime("%Y-%m-%dT%H:%M:%S.000")),
            ('Deductible', '0'),
            ('PAFamilySI', '0'),
            ('AgentNumber', None),
            ('DealerId', None),
            ('Premium', OrderedDict([('Discount', None), ])),
            ('SelectedCovers', OrderedDict([
                ('CarDamageSelected', 'True'),
                ('TPLiabilitySelected', 'True'),
                ('PADriverSelected', 'True'),
                ('ZeroDepriciationSelected', 'False'),
                ('RoadsideAssistanceSelected', 'False'),
                ('InvoicePriceSelected', 'False'),
                ('PAFamilyPremiumSelected', str(
                    bool(self.Configuration.VEHICLE_TYPE == 'Private Car'))),
                ('HospitalCashSelected', 'False'),
                ('MedicalExpensesSelected', 'False'),
                ('AmbulanceChargesSelected', 'False'),
                # ('EngineGearBoxProtectionSelected', 'False'),
                ('HydrostaticLockSelected', 'False'),
                ('KeyReplacementSelected', 'False'),
                ('NoClaimBonusSameSlabSelected', 'False'),
                ('CosumableCoverSelected', 'False')
            ])),
            ('PolicyEndDate', (putils.add_years(
                policy_start_date, 1) + datetime.timedelta(
                days=-1)).strftime("%Y-%m-%dT%H:%M:%S.000")),
        ])
        insurer_utils.update_complex_dict(
            quote_dict, data_dictionary, 'quote_')
        return quote_dict

    def _get_premium_client_dict(self, data_dictionary):
        return OrderedDict([
            ('ClientType', 'Individual'),
            ('FinancierDetails', OrderedDict([('IsFinanced', '0'), ])),
            ('GivName', 'Coverfox%s' % datetime.datetime.now().strftime('%Y%m%d%H%M%S%f')),
            ('SurName', None),
            ('ClientExtraTag01', custom_dictionaries.RTO_CODE_TO_RTO_INFO[
             data_dictionary['RegistrationCity']]['State']),
            ('CityOfResidence', custom_dictionaries.RTO_CODE_TO_RTO_INFO[
             data_dictionary['RegistrationCity']]['RTO_City']),
            ('EmailID', 'premium@coverfox.com'),
            ('MobileNo', '9777777777'),
            ('RegistrationZone', custom_dictionaries.RTO_CODE_TO_RTO_INFO[
             data_dictionary['RegistrationCity']]['Zone']),
        ])


class ProposalGenerator(object):

    def __init__(self, integrationUtils, configuration):
        self.parent = integrationUtils
        self.Configuration = configuration

    def save_policy_details(self, data_dictionary, transaction):
        if transaction.quote.raw_data['isUsedVehicle'] == '1':
            return False

        proposal_request_dict = self._get_required_params_for_proposal_calculation(
            data_dictionary)
        session_data_dict = proposal_request_dict['SessionData']
        quote_dict = proposal_request_dict['Quote']
        transaction.policy_start_date = datetime.datetime.strptime(
            quote_dict['PolicyStartDate'], '%Y-%m-%dT%H:%M:%S.000')
        transaction.policy_end_date = datetime.datetime.strptime(
            quote_dict['PolicyEndDate'], '%Y-%m-%dT%H:%M:%S.000')
        transaction.reference_id = session_data_dict['OrderNo']
        transaction.raw['policy_details'] = proposal_request_dict
        transaction.save()
        proposal_response, total_premium = self.get_proposal(
            transaction)

        if proposal_response is None:
            return False

        motor_logger.info(
            "\nReceived Proposal Response for Bharti AXA, Now requesting payment gateway\n")

        transaction.premium_paid = str(total_premium)
        transaction.reference_id = proposal_response['OrderNo']
        transaction.raw['OrderNo'] = proposal_response['OrderNo']
        transaction.raw['QuoteNo'] = proposal_response['QuoteNo']
        transaction.raw['Channel'] = proposal_response['Session']['SessionData']['Channel']
        transaction.save()
        return True

    def get_proposal(self, transaction):
        proposal_request_dict = transaction.raw['policy_details']
        quote_raw_response = transaction.raw['quote_raw_response']
        addons_offered_to_available_mapping = {
            'isDepreciationWaiver': quote_raw_response['addon_isDepreciationWaiver'],
            'isInvoiceCover': quote_raw_response['addon_isInvoiceCover'],
            'is247RoadsideAssistance': quote_raw_response['addon_is247RoadsideAssistance'],
            'isNcbProtection': quote_raw_response['addon_isNcbProtection'],
            'isCostOfConsumables': quote_raw_response['addon_isCostOfConsumables'],
            'isHydrostaticLock': quote_raw_response['addon_isHydrostaticLock'],
            # 'isEngineProtector': quote_raw_response['addon_isEngineProtector'],
            'isAmbulanceCover': 'False',
            'isHospitalCover': 'False',
            'isMedicalCover': 'False',
        }

        Session = etree.Element('Session')
        Session = parser_utils.dict_to_etree(Session, proposal_request_dict)

        try:
            response_header, response_content = get_soap_request_response(
                transaction.transaction_id, Session, PREMIUM_URL)
        except:
            response_content = traceback.format_exc()
        else:
            try:
                response_content = re.sub(r' xmlns="(.*?)"', '', response_content)

                response_dict = parser_utils.complex_etree_to_dict(
                    ET.fromstring(response_content))

                proposal_response = response_dict['Envelope']['Body']['serveResponse']['tuple']['old']['serve']['serve'][
                    '{http://schemas.xmlsoap.org/soap/envelope/}Envelope'][
                    '{http://schemas.xmlsoap.org/soap/envelope/}Body']['processTPRequestResponse']['response']

                result_data = self.parent.convert_premium_response(proposal_response)
                converted_premium_response, _= result_data['result']
                total_premium = 0.00
                for cover in converted_premium_response['premiumBreakup']['basicCovers']:
                    total_premium += float(cover['premium'])
                for cover in converted_premium_response['premiumBreakup']['addonCovers']:
                    if addons_offered_to_available_mapping[cover['id']] == 'True':
                        total_premium += float(cover['premium'])
                for discount in converted_premium_response['premiumBreakup']['discounts']:
                    total_premium += float(discount['premium'])
                for discount in converted_premium_response['premiumBreakup']['specialDiscounts']:
                    total_premium += float(discount['premium'])
                # total_premium += converted_premium_response['premiumBreakup']['serviceTax']

                total_premium *= (1 + SERVICE_TAX_RATE)

            except:
                pass
            else:
                return proposal_response, math.ceil(total_premium)

        # Proposal failure response mail
        mail_utils.send_mail_for_proposal_failure(transaction, response_content)
        return None, None

    def _get_required_params_for_proposal_calculation(self, data_dictionary):
        return OrderedDict([
            ('SessionData', self._get_proposal_session_data_dict(data_dictionary)),
            ('Vehicle', self._get_proposal_vehicle_dict(data_dictionary)),
            ('Quote', self._get_proposal_quote_dict(data_dictionary)),
            ('Client', self._get_proposal_client_dict(data_dictionary)),
            ('Payment', self._get_proposal_payment_dict()),
        ])

    def _get_proposal_session_data_dict(self, data_dictionary):
        session_data_dict = OrderedDict([
            ('Index', '2'),
            ('InitTime', None),
            ('UserName', USERNAME),
            ('Password', PASSWORD),
            ('OrderNo', 'NA'),
            ('QuoteNo', 'NA'),
            ('Route', 'INT'),
            ('Contract', 'MTR'),
            ('Channel', None),
            ('TransactionType', 'Quote'),
            ('TransactionStatus', 'Fresh'),
            ('ID', None),
            ('UserAgentID', None),
            ('Source', None),
        ])
        insurer_utils.update_complex_dict(
            session_data_dict, data_dictionary, 'proposalSessionData_')
        return session_data_dict

    def _get_proposal_vehicle_dict(self, data_dictionary):
        vehicle_dict = OrderedDict([
            ('TypeOfBusiness', 'TR'),
            ('AccessoryInsured', 'Y'),
            ('AccessoryValue', '0'),
            ('BiFuelKit', OrderedDict(
                [('IsBiFuelKit', 'N'), ('BiFuelKitValue', '0'), ('ExternallyFitted', 'N'), ])),
            ('DateOfRegistration', '2011-03-01T00:00:00.000'),
            ('RiskType', self.Configuration.RISK_TYPE),
            ('Make', 'HYUNDAI'),
            ('Model', 'I20 SPORTZ'),
            ('FuelType', 'P'),
            ('Variant', '1.2'),
            ('IDV', '382000'),
            ('EngineNo', '643033'),
            ('ChasisNo', '290420'),
            ('DriveExperiance', '0'),
            ('NoOfDrivers', None),
            ('ParkingType', None),
            ('AnnualMileage', None),
            ('YoungestDriverAge', '0'),
            ('PaidDriver', 'True'),
            ('VehicleAge', '2'),
            ('CC', '1197'),
            ('SeatingCapacity', '5'),
            ('PlaceOfRegistration', 'Hyderabad'),
            ('VehicleExtraTag01', None),
            ('RegistrationNo', 'AP13AA0853'),
            ('ExShowroomPrice', '551752.00'),
        ])
        insurer_utils.update_complex_dict(
            vehicle_dict, data_dictionary, 'proposalVehicle_')
        # TODO have to revisit this
        vehicle_dict['FuelType'] = custom_dictionaries.FUEL_TYPE_MAPPING_LIST[
            vehicle_dict['FuelType']]
        if vehicle_dict['BiFuelKit']['ExternallyFitted'] == 'Y':
            vehicle_dict['FuelType'] = 'C'
        elif vehicle_dict['BiFuelKit']['IsBiFuelKit'] == 'Y':
            vehicle_dict['FuelType'] = 'G'
        return vehicle_dict

    def _get_proposal_quote_dict(self, data_dictionary):
        quote_dict = OrderedDict([
            ('ExistingPolicy', OrderedDict([
                ('Claims', None),
                ('PolicyType', None),
                ('EndDate', None),
                ('NCB', None),
                ('PolicyNo', None),
                ('InsuranceCompany', None),
            ])),
            ('PolicyStartDate', None),
            ('Deductible', '0'),
            ('PAFamilySI', '0'),
            ('AgentNumber', None),
            ('DealerId', None),
            ('Premium', OrderedDict([('Discount', None), ])),
            ('SelectedCovers', OrderedDict([
                ('CarDamageSelected', 'True'),
                ('TPLiabilitySelected', 'True'),
                ('PADriverSelected', 'True'),
                ('ZeroDepriciationSelected', 'False'),
                ('RoadsideAssistanceSelected', 'False'),
                ('InvoicePriceSelected', 'False'),
                ('PAFamilyPremiumSelected', str(
                    bool(self.Configuration.VEHICLE_TYPE == 'Private Car'))),
                ('HospitalCashSelected', 'False'),
                ('MedicalExpensesSelected', 'False'),
                ('AmbulanceChargesSelected', 'False'),
                # ('EngineGearBoxProtectionSelected', 'False'),
                ('HydrostaticLockSelected', 'False'),
                ('KeyReplacementSelected', 'False'),
                ('NoClaimBonusSameSlabSelected', 'False'),
                ('CosumableCoverSelected', 'False')
            ])),
            ('PolicyEndDate', None),
        ])
        insurer_utils.update_complex_dict(
            quote_dict, data_dictionary, 'proposalQuote_')
        return quote_dict

    def _get_proposal_client_dict(self, data_dictionary):
        client_dict = OrderedDict([
            ('ClientType', 'Individual'),
            ('CltDOB', None),
            ('FinancierDetails', OrderedDict(
                [('IsFinanced', '0'), ('InstitutionName', None), ('InstitutionCity', None), ])),
            ('Salut', None),
            ('GivName', None),
            ('ClientExtraTag01', None),
            ('CityOfResidence', None),
            ('EmailID', None),
            ('MobileNo', None),
            ('SurName', None),
            ('CltSex', None),
            ('Marryd', None),
            ('Occupation', '0007'),
            ('CltAddr01', None),
            ('CltAddr02', None),
            ('CltAddr03', None),
            ('City', None),
            ('State', None),
            ('PinCode', None),
            ('Nominee', OrderedDict(
                [('Name', None), ('Age', None), ('Relationship', None), ])),
            ('RegistrationZone', 'A'),
        ])
        insurer_utils.update_complex_dict(
            client_dict, data_dictionary, 'proposalClient_')
        return client_dict

    def _get_proposal_payment_dict(self):
        return OrderedDict([
            ('PaymentMode', None),
            ('PaymentType', None),
            ('TxnReferenceNo', None),
            ('TxnAmount', None),
            ('TxnDate', None),
            ('BankCode', None),
            ('InstrumentAmount', None),
        ])


class IntegrationUtils(object):

    def __init__(self, vehicle_type):
        configuration = Configuration(vehicle_type)
        self.Configuration = configuration
        self.PremiumCalculator = PremiumCalculator(self, configuration)
        self.ProposalGenerator = ProposalGenerator(self, configuration)

    def convert_premium_request_data_to_required_dictionary(self, vehicle_id, request_data):
        is_new_vehicle = (request_data['isNewVehicle'] == '1')
        is_expired = False
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                request_data['newPolicyStartDate'], "%d-%m-%Y")
            new_policy_end_date = putils.add_years(
                new_policy_start_date, 1) - datetime.timedelta(days=1)
        else:
            past_policy_end_date = datetime.datetime.strptime(
                request_data['pastPolicyExpiryDate'], "%d-%m-%Y")

            # Check if policy is expired
            if past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now():
                is_expired = True
                new_policy_start_date = datetime.datetime.strptime(
                    request_data['newPolicyStartDate'], "%d-%m-%Y")

                if self.Configuration.VEHICLE_TYPE == 'Twowheeler':
                    new_policy_start_date = new_policy_start_date + datetime.timedelta(days=3)
            else:
                new_policy_start_date = (
                    past_policy_end_date + datetime.timedelta(days=1)).replace(microsecond=0)

            new_policy_end_date = putils.add_years(
                new_policy_start_date, 1) - datetime.timedelta(days=1)
        date_of_registration = datetime.datetime.strptime(
            request_data['registrationDate'], '%d-%m-%Y')
        vehicle_age = insurer_utils.get_age(
            new_policy_start_date, date_of_registration)
        reg_date_split = request_data['registrationDate'].split('-')
        vehicle_buying_year = reg_date_split[2]
        vehicle_buying_month = reg_date_split[1]
        vehicle_buying_day = reg_date_split[0]

        split_registration_no = request_data.get('registrationNumber[]')
        master_rto = "%s-%s" % (split_registration_no[0],
                                split_registration_no[1])

        # addons = [
        #     'isDepreciationWaiver', 'is247RoadsideAssistance',
        #     'isInvoiceCover', 'isNcbProtection', 'isCostOfConsumables',
        #     'isHydrostaticLock', 'isEngineProtector'
        # ]
        addons = [
            'isDepreciationWaiver', 'is247RoadsideAssistance',
            'isInvoiceCover', 'isNcbProtection',
            'isCostOfConsumables', 'isHydrostaticLock',
        ]
        vehicle_model = VehicleMaster.objects.get(
            id=request_data['vehicleId']).model
        addon_dict = dict(map((lambda e: ('addon_' + e, False)), addons))
        for addon in addons:
            if request_data.get('addon_' + addon, '0') == '1' and insurer_utils.is_addon_available(addon,
                                                                                          INSURER_SLUG,
                                                                                          vehicle_model,
                                                                                          vehicle_age,
                                                                                          master_rto):
                addon_dict['addon_' + addon] = True

        is_cng_fitted = bool(int(request_data['isCNGFitted']))
        cng_kit_value = request_data['cngKitValue']
        externally_fitted = is_cng_fitted and bool(
            int(cng_kit_value))  # bool(int(x)) is True if x != 0

        is_internally_fitted = is_cng_fitted and not externally_fitted

        accessory_value = int(
            request_data['idvElectrical']) + int(request_data['idvNonElectrical'])
        data_dictionary = {
            'isUsedVehicle': request_data.get('isUsedVehicle', '0'),  # Used only for having age wise restriction
            'isExpired': is_expired,
            'vehicleAge': vehicle_age,
            'VehicleId': vehicle_id,
            'VehicleBuyingYear': vehicle_buying_year,
            'VehicleBuyingMonth': vehicle_buying_month,
            'VehicleBuyingDay': vehicle_buying_day,
            'vehicle_AccessoryValue': accessory_value,
            'vehicle_AccessoryInsured': 'N' if accessory_value == 0 else 'Y',
            'quote_Deductible': '0' if request_data['voluntaryDeductible'] == 'None' else request_data['voluntaryDeductible'],
            'quote_SelectedCovers': {
                'quote_PAFamilyPremiumSelected': str(request_data['extra_paPassenger'] != '0'),
                'quote_ZeroDepriciationSelected': str(bool(addon_dict['addon_isDepreciationWaiver'])),
                'quote_RoadsideAssistanceSelected': str(bool(addon_dict['addon_is247RoadsideAssistance'])),
                'quote_InvoicePriceSelected': str(bool(addon_dict['addon_isInvoiceCover'])),
                'quote_NoClaimBonusSameSlabSelected': str(bool(addon_dict['addon_isNcbProtection'])),
                'quote_CosumableCoverSelected': str(bool(addon_dict['addon_isCostOfConsumables'])),
                'quote_HydrostaticLockSelected': str(bool(addon_dict['addon_isHydrostaticLock'])),
                # 'quote_EngineGearBoxProtectionSelected': str(bool(addon_dict['addon_isEngineProtector'])),
            },
            'quote_PAFamilySI': request_data['extra_paPassenger'],
            'vehicle_BiFuelKit': {
                'vehicle_IsBiFuelKit': 'Y' if is_internally_fitted else 'N',
                'vehicle_BiFuelKitValue': cng_kit_value,
                'vehicle_ExternallyFitted': 'Y' if externally_fitted else 'N',
            },
            'idv': request_data['idv'],
            'RegistrationCity': "%s%s" % (split_registration_no[0], split_registration_no[1]),
            'third_party': request_data['third_party']
        }

        if not is_new_vehicle:
            pncb = request_data['previousNCB']
            previous_ncb_to_new_ncb = {
                '0': '20',
                '20': '25',
                '25': '35',
                '35': '45',
                '45': '50',
                '50': '50',
            }

            new_ncb = previous_ncb_to_new_ncb[pncb] if request_data['isClaimedLastYear'] == '0' else '0'
            if past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now() - datetime.timedelta(days=90):
                new_ncb = '0'

            data_dictionary['quote_ExistingPolicy'] = {
                'quote_Claims': request_data['isClaimedLastYear'],
                'quote_PolicyType': 'Comprehensive',
                'quote_EndDate': past_policy_end_date.strftime("%Y-%m-%dT%H:%M:%S.000"),
                'quote_NCB': new_ncb,
            }
            data_dictionary['vehicle_TypeOfBusiness'] = 'TR'
            data_dictionary['quote_PolicyStartDate'] = new_policy_start_date.strftime(
                "%Y-%m-%dT%H:%M:%S.000")
            data_dictionary['quote_PolicyEndDate'] = new_policy_end_date.strftime(
                "%Y-%m-%dT%H:%M:%S.000")

            # Set registration number as passed and set registration city too
            data_dictionary[
                'RegistrationNumber'] = "-".join(split_registration_no)

        else:
            # Set registration number = NEW and set registration city too
            data_dictionary['RegistrationNumber'] = 'NEW'
            data_dictionary['quote_PolicyStartDate'] = new_policy_start_date.strftime(
                "%Y-%m-%dT%H:%M:%S.000")
            data_dictionary['quote_PolicyEndDate'] = new_policy_end_date.strftime(
                "%Y-%m-%dT%H:%M:%S.000")

        return data_dictionary

    def get_buy_form_details(self, quote_parameters, transaction):

        split_registration_no = quote_parameters.get('registrationNumber[]')

        is_new_vehicle = True if quote_parameters[
            'isNewVehicle'] == '1' else False
        if is_new_vehicle:
            registration_number = 'NEW'
        else:
            registration_number = '-'.join(split_registration_no)

        rto_city_code = "%s%s" % (split_registration_no[
                                  0], split_registration_no[1])

        data = {
            'registration_no': registration_number,
            'rto_location_info': rto_city_code,
            # 'occupation_list': OCCUPATION_MAP,
            'state_list': custom_dictionaries.STATE_CODE_LIST,
            # 'insurers_list': PAST_INSURANCE_ID_LIST,
            'insurers_list': custom_dictionaries.PAST_INSURER_ID_LIST_MASTER,
            'disallowed_insurers': custom_dictionaries.DISALLOWED_PAST_INSURERS,
            'relationship_list': custom_dictionaries.NOMINEE_RELATIONSHIP_LIST,
            'is_cities_fetch': False,
        }
        return data

    def convert_proposal_request_data_to_required_dictionary(self, vehicle, request_data, quote_parameters,
                                                             quote_response, quote_custom_response):
        idv = quote_custom_response['calculatedAtIDV']

        if request_data['cust_gender'] == 'Male':
            request_data['form-insured-title'] = 'Mr.'
        else:
            if request_data['cust_marital_status'] == '0':
                request_data['form-insured-title'] = 'Ms.'
            else:
                request_data['form-insured-title'] = 'Mrs.'

        is_new_vehicle = (quote_parameters['isNewVehicle'] == '1')

        if is_new_vehicle:
            request_data['vehicle_reg_no'] = 'NEW'

        registration_number = request_data['vehicle_reg_no'].replace('-', '')
        date_of_registration = datetime.datetime.strptime(
            quote_parameters['registrationDate'], "%d-%m-%Y")

        pncb = '0'
        ncb = '0'
        if not is_new_vehicle:
            pncb = quote_parameters['previousNCB']
            if quote_parameters['isClaimedLastYear'] == '0':
                for ncb_key, ncb_value in custom_dictionaries.YEARWISE_NCB.items():
                    if ncb_value == pncb:
                        break

                ncb = custom_dictionaries.YEARWISE_NCB.get(ncb_key + 1, pncb)
                ncb = '50' if ncb == '65' else ncb

        current_date_time = datetime.datetime.now().replace(microsecond=0)

        # Assigning new_policy_start_date and new_policy_end_date
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                quote_parameters['newPolicyStartDate'], "%d-%m-%Y")
            new_policy_end_date = putils.add_years(
                new_policy_start_date, 1) - datetime.timedelta(days=1)
        else:

            past_policy_end_date = datetime.datetime.strptime(
                quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")

            # Check if policy is expired
            if past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now():
                new_policy_start_date = datetime.datetime.now().replace(microsecond=0)

                if self.Configuration.VEHICLE_TYPE == 'Twowheeler':
                    new_policy_start_date = new_policy_start_date + datetime.timedelta(days=3)

            else:
                new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

            new_policy_end_date = putils.add_years(
                new_policy_start_date, 1) - datetime.timedelta(days=1)

        new_policy_start_date = new_policy_start_date.replace(microsecond=0)
        vehicle_age = int(math.ceil(insurer_utils.get_age(
            new_policy_start_date, date_of_registration)))

        cust_dob = datetime.datetime.strptime(request_data['cust_dob'], "%d-%m-%Y")

        vehicle_rto = "".join(quote_parameters['registrationNumber[]'][0:2])
        rto_info = custom_dictionaries.RTO_CODE_TO_RTO_INFO[vehicle_rto]

        order_no = quote_response['OrderNo']
        quote_no = quote_response['QuoteNo']

        accessory_value = int(
            quote_parameters['idvElectrical']) + int(quote_parameters['idvNonElectrical'])

        request_data_dictionary = {
            # ADDING AN EXTRA KEY, NOT USED FOR PROPOSAL GENERATION BUT WILL BE USED FOR SAVING THE TRANSACTION
            # WHERE PREMIUM PAID IS SAVED IN SAVE METHOD OF PROPOSAL_GENERATOR CLASS
            #        session_data_dict = {
            'proposalSessionData_InitTime': current_date_time.strftime("%a, %d %b %Y %H:%M:%S GMT"),
            'proposalSessionData_OrderNo': order_no,
            'proposalSessionData_QuoteNo': quote_no,
            #        vehicle_dict = {
            'proposalVehicle_TypeOfBusiness': 'NB',
            'proposalVehicle_AccessoryInsured': 'N' if accessory_value == 0 else 'Y',
            'proposalVehicle_AccessoryValue': accessory_value,
            'proposalVehicle_DateOfRegistration': date_of_registration.strftime("%Y-%m-%dT%H:%M:%S.000"),
            'proposalVehicle_RiskType': self.Configuration.RISK_TYPE,
            'proposalVehicle_Make': vehicle.make,
            'proposalVehicle_Model': vehicle.model,
            'proposalVehicle_FuelType': vehicle.fuel_type,
            'proposalVehicle_Variant': vehicle.variant,
            'proposalVehicle_IDV': idv,
            'proposalVehicle_EngineNo': request_data['vehicle_engine_no'],
            'proposalVehicle_ChasisNo': request_data['vehicle_chassis_no'],
            'proposalVehicle_DriveExperiance': '2',  # Drive experience of customer
            'proposalVehicle_AnnualMileage': None,
            'proposalVehicle_YoungestDriverAge': '18',
            'proposalVehicle_PaidDriver': 'False',
            'proposalVehicle_VehicleAge': max(vehicle_age, 1),
            'proposalVehicle_CC': '%s%s' % (('0' if len(str(vehicle.cc)) < 3 else ''), vehicle.cc),
            'proposalVehicle_SeatingCapacity': vehicle.seating_capacity,
            'proposalVehicle_PlaceOfRegistration': rto_info['RTO_City'],
            'proposalVehicle_RegistrationNo': registration_number,
            'proposalVehicle_ExShowroomPrice': vehicle.ex_showroom_price,
            #        quote_dict = {
            'proposalQuote_PolicyStartDate': new_policy_start_date.strftime("%Y-%m-%dT%H:%M:%S.000"),
            'proposalQuote_Deductible': '0' if quote_parameters['voluntaryDeductible'] == 'None'
                                        else quote_parameters['voluntaryDeductible'],
            'proposalQuote_SelectedCovers': {
                'proposalQuote_PAFamilyPremiumSelected': str(quote_parameters['extra_paPassenger'] != '0'),
                'proposalQuote_ZeroDepriciationSelected': quote_response['addon_isDepreciationWaiver'],
                'proposalQuote_RoadsideAssistanceSelected': quote_response['addon_is247RoadsideAssistance'],
                'proposalQuote_InvoicePriceSelected': quote_response['addon_isInvoiceCover'],
                'proposalQuote_NoClaimBonusSameSlabSelected': quote_response['addon_isNcbProtection'],
                # 'proposalQuote_EngineGearBoxProtectionSelected': str(quote_response.get('addon_isEngineProtector', '0') != '0'),
                'proposalQuote_CosumableCoverSelected': str(quote_response.get('addon_isCostOfConsumables', False)),
                'proposalQuote_HydrostaticLockSelected': str(quote_response.get('addon_isHydrostaticLock', False)),
            },
            'proposalQuote_PolicyEndDate': new_policy_end_date.strftime("%Y-%m-%dT%H:%M:%S.000"),
            'proposalQuote_PAFamilySI': quote_parameters['extra_paPassenger'],
            #        client_dict = {
            'proposalClient_ClientType': 'Individual',  # 'Individual' or 'Corporate'
            'proposalClient_CltDOB': cust_dob.strftime("%Y%m%d"),  # YYYYMMDD
            'proposalClient_FinancierDetails': {
                'proposalClient_IsFinanced': '0',
                'proposalClient_InstitutionName': None,
                'proposalClient_InstitutionCity': None,
            },
            'proposalClient_Salut': request_data['form-insured-title'].split('.')[0].upper(),
            'proposalClient_GivName': request_data['cust_first_name'],
            # STATE OF REGISTRATION
            'proposalClient_ClientExtraTag01': rto_info['State'],
            # VEHICLE REGISTRATION CITY
            'proposalClient_CityOfResidence': rto_info['RTO_City'],
            'proposalClient_EmailID': request_data['cust_email'],
            'proposalClient_MobileNo': request_data['cust_phone'],
            'proposalClient_SurName': request_data['cust_last_name'],
            'proposalClient_CltSex': custom_dictionaries.GENDER_MAPPING[request_data['cust_gender']],
            # 'S' for Single and 'M' for Married
            'proposalClient_Marryd': 'S' if request_data['cust_marital_status'] == '0' else 'M',
            'proposalClient_Occupation': '0007',  # Default set to Others, request_data['cust_occupation']
            'proposalClient_CltAddr01': str(request_data['add_house_no']),
            'proposalClient_CltAddr02': '%s %s' % (str(request_data.get('add_building_name', '')), request_data['add_street_name']),
            'proposalClient_CltAddr03': str(request_data.get('add_landmark', '')),
            'proposalClient_City': str(request_data.get('add_city', '')),
            'proposalClient_State': request_data['add_state'] if request_data['add_state'] in custom_dictionaries.STATE_CODE_LIST
            else custom_dictionaries.STATE_CODE_LIST[int(request_data['add_state'])],
            'proposalClient_PinCode': request_data['add_pincode'],
            'proposalClient_Nominee': {
                'proposalClient_Name': request_data['nominee_name'],
                'proposalClient_Age': request_data['nominee_age'],
                'proposalClient_Relationship': request_data['nominee_relationship'],
            },
            # VEHICLE REGISTRATION ZONE
            'proposalClient_RegistrationZone': rto_info['Zone'],
        }

        is_cng_fitted = True if quote_parameters[
            'isCNGFitted'] == '1' else False
        cng_kit_value = quote_parameters['cngKitValue']
        is_internally_fitted = False
        externally_fitted = False
        if is_cng_fitted:
            is_internally_fitted = True if cng_kit_value == '0' else False
            externally_fitted = False if cng_kit_value == '0' else True
        request_data_dictionary['proposalVehicle_BiFuelKit'] = {
            'proposalVehicle_IsBiFuelKit': 'Y' if is_internally_fitted else 'N',
            'proposalVehicle_BiFuelKitValue': cng_kit_value,
            'proposalVehicle_ExternallyFitted': 'Y' if externally_fitted else 'N',
        }

        if request_data['is_car_financed'] == '1':
            request_data_dictionary['proposalClient_FinancierDetails'] = {
                'proposalClient_IsFinanced': '1',
                'proposalClient_InstitutionName': request_data['car-financier'],
                'proposalClient_InstitutionCity': request_data['add_district'],
            }

        if not is_new_vehicle:
            pncb = quote_parameters['previousNCB']
            previous_ncb_to_new_ncb = {
                '0': '20',
                '20': '25',
                '25': '35',
                '35': '45',
                '45': '50',
                '50': '50',
            }
            past_policy_end_date = datetime.datetime.strptime(
                quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")

            request_data_dictionary['proposalVehicle_TypeOfBusiness'] = 'TR'
            past_policy_insurer_code = custom_dictionaries.REV_PASTINSURER_MAP[request_data['past_policy_insurer']]
            request_data_dictionary['proposalQuote_ExistingPolicy'] = {
                'proposalQuote_Claims': quote_parameters['isClaimedLastYear'],
                'proposalQuote_PolicyType': 'Comprehensive',
                'proposalQuote_EndDate': past_policy_end_date.strftime("%Y-%m-%dT%H:%M:%S.000"),
                'proposalQuote_NCB': previous_ncb_to_new_ncb[pncb] if quote_parameters['isClaimedLastYear'] == '0' else '0',
                'proposalQuote_PolicyNo': request_data['past_policy_number'],
                # 'proposalQuote_InsuranceCompany': PAST_INSURANCE_ID_LIST[request_data['past_policy_insurer']],
                'proposalQuote_InsuranceCompany': custom_dictionaries.PAST_INSURANCE_ID_LIST[past_policy_insurer_code],
            }

            # Expired flow for more than 90 days
            if past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now() - datetime.timedelta(days=90):
                request_data_dictionary['proposalQuote_ExistingPolicy']['proposalQuote_NCB'] = 0
                request_data_dictionary['proposalQuote_ExistingPolicy']['proposalQuote_PolicyNo'] = 'EXPIREDPOLICY'

        return request_data_dictionary

    def update_transaction_details(self, transaction, data):
        is_new_vehicle = data['isNewVehicle'] == '1'
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                data['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data['pastPolicyExpiryDate'], "%d-%m-%Y")

            # Check if policy is expired
            if past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now():
                new_policy_start_date = datetime.datetime.now().replace(microsecond=0)

                if self.Configuration.VEHICLE_TYPE == 'Twowheeler':
                    new_policy_start_date = new_policy_start_date + datetime.timedelta(days=3)

            else:
                new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

        new_policy_end_date = putils.add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)
        new_policy_start_date = new_policy_start_date.replace(microsecond=0)

        transaction.policy_start_date = new_policy_start_date
        transaction.policy_end_date = new_policy_end_date
        transaction.save()

    def process_expired_policy(self, transaction_id):
        return True

    def get_payment_data(self, transaction):
        payment_gateway_data = {
            'OrderNo': transaction.raw['OrderNo'],
            'QuoteNo': transaction.raw['QuoteNo'],
            'Channel': transaction.raw['Channel'],
            'Product': 'MTR',
            'Amount': str(float(transaction.premium_paid)),
            'IsMobile': 'N',
        }
        transaction.payment_request = payment_gateway_data
        transaction.save()
        suds_logger.set_name(transaction.transaction_id).log(
            json.dumps(payment_gateway_data))
        return payment_gateway_data, PAYMENT_URL

    def post_payment(self, payment_response, transaction):
        motor_logger.info(payment_response)
        # orderNo == reference_id
        redirect_url = '/motor/%s/payment/failure/' % self.Configuration.URL_TAG
        is_redirect = True

        payment_response = payment_response['GET']
        status = payment_response['status']
        order_no = payment_response['orderNo']

        try:
            transaction_from_response = Transaction.objects.get(
                reference_id=order_no)
            transaction = transaction_from_response
            suds_logger.set_name(transaction.transaction_id).log(json.dumps(payment_response))
        except:
            log_error(motor_logger)

        transaction.insured_details_json['payment_response'] = payment_response
        transaction.payment_response = payment_response

        if status in ['success', 'payPass']:
            # productID=""&orderNo=""&amount=""&status=""&transactionRef=""&policyNo=""&link=""&emailId=""
            transaction.payment_done = True
            transaction.payment_on = datetime.datetime.now()
            if 'policyNo' in payment_response:
                transaction.policy_number = payment_response['policyNo']
            # transaction.insured_details_json['policy_url'] = "%s%s" % (
            #     'https://www.smartlinkxruat.bharti-axagi.co.in/cordys/', payment_response['link']
            # )
            if 'link' in payment_response:
                transaction.insured_details_json['policy_url'] = "%s%s%s" % (
                    HOST_URL, 'cordys/', payment_response['link'])
            transaction.mark_complete()
            redirect_url = '/motor/%s/payment/success/%s/' % (
                self.Configuration.URL_TAG, transaction.transaction_id)
        else:
            transaction.status = 'FAILED'
            redirect_url += '%s/' % (transaction.transaction_id)
        transaction.save()
        return is_redirect, redirect_url, '', {}

    # Method to convert bharti premium response to the required responser.
    def convert_premium_response(self, premium_response, min_idv=None, max_idv=None, idv=None, vehicle_id=None):
        cover_set = premium_response['PremiumSet']['Cover']
        car_damage_mapping = {
            'BasicOD': {'id': 'basicOd',            'default': '1', 'type': 'basic', },
            'Accessory': {'id': 'idvAccessoriers',      'default': '1', 'type': 'basic', },
            'BiFuel': {'id': 'cngKit',             'default': '1', 'type': 'basic', },
            'AntiTheft': {'id': 'isAntiTheftFitted',  'default': '0', 'type': 'special_discount', },
            'ODDeductible': {'id': 'voluntaryDeductible', 'default': '1', 'type': 'special_discount', },
            'NCB': {'id': 'ncb',                'default': '1', 'type': 'discount', },
        }
        basic_cover_mapping = {
            'ThirdPartyLiability': {'id': 'basicTp',        'default': '1', },
            'PAOwnerDriver': {'id': 'paOwnerDriver',  'default': '1', },
            'PAFamily': {'id': 'paPassenger',    'default': '1', },
        }
        addon_cover_mapping = {
            'DEPC': {'id': 'isDepreciationWaiver',    'default': '0', },
            'INPC': {'id': 'isInvoiceCover',          'default': '0', },
            'AMBC': {'id': 'isAmbulanceCover',        'default': '0', },
            'HOSP': {'id': 'isHospitalCover',         'default': '0', },
            'RSAC': {'id': 'is247RoadsideAssistance', 'default': '0', },
            'MEDI': {'id': 'isMedicalCover',          'default': '0', },
            'CONC': {'id': 'isCostOfConsumables',     'default': '0', },
            'HYLC': {'id': 'isHydrostaticLock',       'default': '0', },
            # 'EGBP': {'id': 'isEngineProtector',       'default': '0', },
            # 'KEYC': {'id': 'isKeyReplacement',        'default': '0', },
            'NCBS': {'id': 'isNcbProtection',         'default': '0', },
        }
        basic_covers, addon_covers, discounts, special_discounts = [], [], [], []
        # basic_od_premium = 0.00
        discount = 0.00

        for cover in cover_set:
            if cover['Name'] in basic_cover_mapping:
                cover_map = basic_cover_mapping[cover['Name']]
                if cover_map['id'] == 'basicTp':
                    basic_covers.append({
                        'id': 'basicTp',
                        'default': '1',
                        'premium': float(cover['Premium']) - (50 if self.Configuration.VEHICLE_TYPE == 'Private Car' else 0),
                    })
                    if self.Configuration.VEHICLE_TYPE == 'Private Car':
                        basic_covers.append({
                            'id': 'legalLiabilityDriver',
                            'default': '1',
                            'premium': 50,
                        })
                else:
                    basic_covers.append({
                        'id': cover_map['id'],
                        'default': cover_map['default'],
                        'premium': float(cover['Premium']),
                    })
            elif cover['Name'] in addon_cover_mapping:
                cover_map = addon_cover_mapping[cover['Name']]
                if premium_response.get('addon_' + cover_map['id'], 'True') == 'False':
                    continue
                addon_covers.append({
                    'id': cover_map['id'],
                    'default': cover_map['default'],
                    'premium': float(cover['Premium']),
                })
            elif cover['Name'] == 'CarDamage':
                discounted_premium = cover['Premium']
                non_discounted_premium = 0.00
                extra_details = cover['ExtraDetails']
                for detail_key, detail_value in extra_details.items():
                    if detail_key == 'BreakUp':
                        for break_up_key, break_up_value in detail_value.items():
                            car_damage_map = car_damage_mapping[break_up_key]
                            if car_damage_map['type'] == 'basic':
                                basic_covers.append({
                                    'id': car_damage_map['id'],
                                    'default': car_damage_map['default'],
                                    'premium': float(break_up_value),
                                })
                                non_discounted_premium += float(break_up_value)
                                if car_damage_map['id'] == 'basicOd':
                                    basic_od_premium = float(break_up_value)

                            elif car_damage_map['type'] == 'addon':
                                if premium_response.get('addon_' + car_damage_map['id'], 'False') == 'False':
                                    continue
                                addon_covers.append({
                                    'id': car_damage_map['id'],
                                    'default': car_damage_map['default'],
                                    'premium': float(break_up_value),
                                })
                                non_discounted_premium += float(break_up_value)
                            elif car_damage_map['type'] == 'special_discount':
                                special_discounts.append({
                                    'id': car_damage_map['id'],
                                    'default': car_damage_map['default'],
                                    'premium': -abs(float(break_up_value)),
                                })
                                non_discounted_premium += - \
                                    abs(float(break_up_value))
                            else:
                                discounts.append({
                                    'id': car_damage_map['id'],
                                    'default': car_damage_map['default'],
                                    'premium': -abs(float(break_up_value)),
                                })
                                non_discounted_premium += - \
                                    abs(float(break_up_value))
                non_discounted_premium = non_discounted_premium
                discount = float(discounted_premium) - non_discounted_premium
        discounts.append({
            'id': 'odDiscount',
            'default': '1',
            'premium': discount,
        })

        if premium_response.get('isUsedVehicle', None) == '1':
            discount_percentage = abs(float(discount) / float(basic_od_premium)) * 100
            if discount_percentage >= USED_VEHICLE_DISCOUNT_LIMIT:
                return {'success': False, 'error_code': IntegrationStatistics.COVERFOX_DISCOUNT_LIMIT_EXCEEDED}

        if premium_response.get('isExpired', None) == '1':
            discount_percentage = abs(float(discount) / float(basic_od_premium)) * 100
            if discount_percentage >= EXPIRED_VEHICLE_DISCOUNT_LIMIT:
                return {'success': False, 'error_code': IntegrationStatistics.COVERFOX_DISCOUNT_LIMIT_EXCEEDED}

        if vehicle_id:
            vehicle = Vehicle.objects.filter(
                insurer__slug=INSURER_SLUG).get(id=vehicle_id)
            paPassengerAmounts = insurer_utils.get_pa_passenger_dict(
                vehicle.vehicle_type, vehicle.seating_capacity)
        else:
            paPassengerAmounts = {}

        result_data = {
            'insurerId': int(INSURER_ID),
            'insurerSlug': INSURER_SLUG,
            'insurerName': INSURER_NAME,
            'isInstantPolicy': True,
            'minIDV': min_idv,
            'maxIDV': max_idv,
            'calculatedAtIDV': idv,
            'serviceTaxRate': SERVICE_TAX_RATE,
            'premiumBreakup': {
                'serviceTax': round(float(premium_response['PremiumSet']['ServiceTax']), 2),
                'basicCovers': insurer_utils.cpr_update(basic_covers),
                'addonCovers': insurer_utils.cpr_update(addon_covers),
                'discounts': insurer_utils.cpr_update(discounts),
                'specialDiscounts': insurer_utils.cpr_update(special_discounts),
                'paPassengerAmounts': paPassengerAmounts,
            },
        }, premium_response
        return {'success': True, 'result': result_data}

    def get_dependent_addons(self, parameters):
        return {'type': 'dynamic'}

    def get_transaction_from_id(self, transaction_id):
        transaction = Transaction.objects.get(transaction_id=transaction_id)
        return transaction

    def get_custom_data(self, datatype):

        datamap = {
            'occupations': custom_dictionaries.OCCUPATION_MAP,
            # 'insurers': PAST_INSURANCE_ID_LIST,
            'insurers': custom_dictionaries.PAST_INSURER_ID_LIST_MASTER,
            'relationships': custom_dictionaries.NOMINEE_RELATIONSHIP_LIST,
            'states': dict([(str(index), value) for index, value in enumerate(custom_dictionaries.STATE_CODE_LIST)]),
        }
        return datamap.get(datatype, None)

    def disable_cache_for_request(self, request_data):
        if (request_data['isNewVehicle'] != '1' and
            datetime.datetime.strptime(
                request_data['pastPolicyExpiryDate'], '%d-%m-%Y'
                ).replace(hour=23, minute=59, second=0) < datetime.datetime.now()):
            return True
        return False
