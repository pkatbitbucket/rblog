
import difflib

from motor_product import models as motor_models
from motor_product.insurers.bharti_axa.scripts import bharti_rto_data as bharti_dicts

INSURER_SLUG = 'bharti-axa'


def delete_data():
    motor_models.RTOInsurer.objects.filter(insurer__slug=INSURER_SLUG).delete()


def save_data():
    CITY_INFO_DICT = bharti_dicts.RTO_CODE_TO_RTO_INFO

    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)

    rtos = []
    for city_code, city_info in CITY_INFO_DICT.items():
        mrto_state = get_insurer_master_state(city_code)
        rto_insurer = motor_models.RTOInsurer()
        rto_insurer.insurer = insurer
        rto_insurer.rto_code = city_code
        rto_insurer.rto_name = city_info['RTO_City']
        rto_insurer.rto_state = city_info['State']
        rto_insurer.rto_zone = city_info['Zone']
        rto_insurer.master_rto_state = mrto_state
        rto_insurer.raw = {'city_name': city_info['RTO_City'], 'city_code': city_code, 'state': city_info['State'], 'zone': city_info['Zone']}
        print "Saving ---- %s - %s" % (city_info['RTO_City'], mrto_state)
        rtos.append(rto_insurer)
    motor_models.RTOInsurer.objects.bulk_create(rtos)


def get_insurer_master_state(rtocode):
    return rtocode[0:2]


def get_insurer_state(state):
    if state == 'TG':
        return 'TS'
    elif state == 'OR':
        return 'OD'
    elif state == 'UK':
        return 'UA'
    return state


def create_mappings():
    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)
    master_rto_data = motor_models.RTOMaster.objects.all()
    insurer_rto_data = motor_models.RTOInsurer.objects.filter(insurer=insurer)

    for master_rto in master_rto_data:

        mrto_id = master_rto.rto_code
        mrto_name = master_rto.rto_name
        mrto_state = get_insurer_state(master_rto.rto_state)

        insurer_rto_code = get_insurer_state(mrto_id.split('-')[0]) + mrto_id.split('-')[1]
        insurer_rtos = insurer_rto_data.filter(rto_code=insurer_rto_code)

        if len(insurer_rtos) > 1:
            print 'More that one direct mapping found for rto %s ... hence igonring' % mrto_id
            continue
        elif len(insurer_rtos) == 1:
            mapping = motor_models.RTOMapping()
            mapping.rto_master = master_rto
            mapping.rto_insurer = insurer_rtos[0]
            mapping.insurer = insurer
            mapping.is_approved_mapping = True  # is a 100% valid mapping
            print "Matching %s with %s" % (master_rto.rto_name, insurer_rtos[0].rto_name)
            mapping.save()
            continue
        else:
            print 'No direct RTO mapping found for rto %s' % mrto_id

        # For RTOs with no direct mapping go for the difflib mapping
        rtos_in_mrtos_state = insurer_rto_data.filter(master_rto_state=mrto_state)

        if len(rtos_in_mrtos_state) == 0:
            print 'No RTOs found for the state ' + mrto_state

        city_list = []
        for irto in rtos_in_mrtos_state:
            city_list.append(irto.rto_name)

        matches = difflib.get_close_matches(mrto_name, city_list, cutoff=0.85)

        for match in matches:
            matched_irto = rtos_in_mrtos_state.filter(rto_name=match)
            if matched_irto.count() > 1:
                print 'More than one RTO found for a particular match. - ' + match
            matched_irto = matched_irto[0]
            mapping = motor_models.RTOMapping()
            mapping.rto_master = master_rto
            mapping.rto_insurer = matched_irto
            mapping.insurer = insurer
            print "Matching %s with %s" % (master_rto.rto_name, matched_irto.rto_name)
            mapping.save()


def delete_mappings():
    motor_models.RTOMapping.objects.filter(insurer__slug=INSURER_SLUG).delete()
