import copy
import datetime
import hashlib
import logging
import json
import math
from xml.etree import ElementTree
import putils
from celery.contrib.methods import task_method
from django.conf import settings
from lxml import etree
from suds.client import Client

from celery_app import app
from motor_product import logger_utils, mail_utils, parser_utils
from motor_product.insurers import insurer_utils
from motor_product.models import Insurer, Vehicle, VehicleMaster, Transaction, IntegrationStatistics
from utils import motor_logger, log_error

from . import custom_dictionaries

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)

INSURER_SLUG = 'future-generali'
try:
    INSURER = Insurer.objects.get(slug=INSURER_SLUG)
    INSURER_ID = INSURER.id
    INSURER_NAME = INSURER.title
except:
    INSURER_ID = 0
    INSURER_NAME = ''

suds_logger = logger_utils.getLogger(INSURER_SLUG)

CONTRACT_TYPE = 'MPVC01'
MAJOR_CLASS = 'MOT'
AGENT_NAME = 'Coverfox'

# Production Credentials
PAYMENT_URL = 'https://online.futuregenerali.in/Ecom_NL/WEBAPPLN/UI/Common/WebAggPay.aspx'
SERVICE_URL = 'http://online.futuregenerali.in/wsmotornb/Service1.asmx?wsdl'
USER_ID = '22263243'
PASSWORD = 'Password@30'
AGENT_CODE = '60039501'
# BRANCH_CODE = '2J'
BRANCH_CODE = '10'

# UAT Credentials
# PAYMENT_URL = 'http://fglpg001.futuregenerali.in/Ecom_NL/WEBAPPLN/UI/Common/WebAggPay.aspx'
# SERVICE_URL = 'http://fglpg001.futuregenerali.in/wsmotornb/service1.asmx?wsdl'
# USER_ID = '10134025'
# PASSWORD = 'Password@30'
# AGENT_CODE = '60001464'
# BRANCH_CODE = '14'

VEHICLE_AGE_LIMIT = 7


def _estat(parameter, rettype=None):
    if rettype == 'YN':
        return 'Y' if parameter != '0' else 'N'
    else:
        return parameter if parameter != '0' else ''


@app.task
def send_proposal(transaction):
    intg = IntegrationUtils()
    proposal_generator = intg.ProposalGenerator

    try:
        data_dictionary = transaction.raw['policy_details']
        quote_parameters = transaction.quote.raw_data

        is_new_vehicle = (quote_parameters['isNewVehicle'] == '1')
        if is_new_vehicle:
            vehicle_age = 0
        else:
            past_policy_end_date = datetime.datetime.strptime(quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)
            date_of_registration = datetime.datetime.strptime(quote_parameters['registrationDate'], '%d-%m-%Y')
            vehicle_age = custom_dictionaries.insurer_utils.get_age(new_policy_start_date, date_of_registration)

        split_registration_no = quote_parameters.get('registrationNumber[]')
        master_rto = "%s-%s" % (split_registration_no[0], split_registration_no[1])

        addons = ['isDepreciationWaiver', 'is247RoadsideAssistance']
        vehicle_model = VehicleMaster.objects.get(id=quote_parameters['vehicleId']).model
        for addon in addons:
            if quote_parameters['addon_' + addon] == '1' and (
                custom_dictionaries.insurer_utils.is_addon_available(addon,
                                                                     INSURER_SLUG,
                                                                     vehicle_model,
                                                                     vehicle_age, master_rto)):
                quote_parameters['addon_' + addon] = '1'
            else:
                quote_parameters['addon_' + addon] = '0'

        proposal_request_data = proposal_generator._get_proposal_request(
            data_dictionary, quote_parameters, transaction)
        request_etree = custom_dictionaries.insurer_utils.convert_tuple_to_xmltree(
            proposal_request_data)
        request_xml = etree.tostring(request_etree)
        transaction.proposal_request = request_xml
        transaction.save()
        response = proposal_generator._get_client(
            transaction.transaction_id).service.CreatePolicy(request_xml)
        transaction.proposal_response = response
        transaction.save()
        parsed_result = ElementTree.fromstring(response)
        response_dict = parser_utils.complex_etree_to_dict(parsed_result)

        if response_dict['Output']['PolicyStatus'] == 'Y':
            transaction.policy_number = response_dict['Output']['PolicyNo']
            transaction.proposal_number = response_dict['Output']['ProposalNo']
            transaction.save(update_fields=['policy_number', 'proposal_number'])
            # transaction.save()

            mail_utils.send_mail_for_admin({
                'title': 'Policy number for future-generali',
                'type': 'policy',
                'data': 'Policy number for transaction %s (CFOX-%s) is %s (email: %s)' % (
                    transaction.transaction_id,
                    transaction.id,
                    transaction.policy_number,
                    transaction.proposer.email
                ),
            })
        else:
            message = 'For transaction with id %s (%s), ' \
                      'Proposal generation failed after payment for Future Generali' % (transaction.id,
                                                                                        transaction.transaction_id)
            title = 'ALERT: Proposal generation failed, failure from Future Generali server'
            mail_utils.send_mail_for_admin({
                'title': title,
                'data': message,
            })

            # Proposal failure response mail
            mail_utils.send_mail_for_proposal_failure(transaction, response)
    except:
        log_error(motor_logger,
                  msg=u"After payment error for Future Generali ({})".format(transaction.transaction_id))


class PremiumCalculator(object):

    client = None

    def __init__(self, integrationUtils):
        self.parent = integrationUtils

    def _get_client(self, name=None):
        if self.client is None:
            self.client = Client(url=SERVICE_URL)

        self.client.set_options(plugins=[suds_logger.set_name(name)])
        return self.client

    def _get_client_data(self, data):
        client = (
            'Client', (
                ('Uid', 'CFX%s' % datetime.datetime.now().strftime('%d%f')),
                ('MajorClass', MAJOR_CLASS),
                ('ContractType', CONTRACT_TYPE),
                ('NewCar', 'Y' if data['isNewVehicle'] == '1' else 'N'),
                ('EffectiveDt', data['updated_policy_start_date']),
                ('ExpiryDt', data['updated_policy_end_date']),
                ('AgentCode', AGENT_CODE),
                ('UserId', USER_ID),
                ('Password', PASSWORD),
                ('BranchCode', BRANCH_CODE),
                ('ClienType', 'A'),
                ('ClientMapTyp', 'N'),
                ('Capital', '0'),
                ('Source', 'DESC'),
            )
        )
        return client

    # No addons for now
    def _get_addons(self, data):
        addons = (
            'Addon', (
                ('CoverCode', 'PLAN1'),
                ('CoverName', 'Combined Plan'),
            )
        )
        # Key replacement, Loss of Personal Belongings, Zero Dep, RSA in PLAN1
        if data['addon_isDepreciationWaiver'] == '1' or data['addon_is247RoadsideAssistance'] == '1':
            return addons
        else:
            return None

    def _get_risk_details(self, data, vehicle):
        is_new_vehicle = (data['isNewVehicle'] == '1')
        is_claimed_last_year = (data['isClaimedLastYear'] == '1')

        if is_new_vehicle or is_claimed_last_year:
            NCBper = 0
        else:
            NCBper = custom_dictionaries.PREVIOUS_NCB_TO_NEW_NCB[data['previousNCB']]

        addons = self._get_addons(data)
        risk_details = (
            'Risk', (
                ('RTACode', data['rto_code']),
                ('RTAState', ''),
                ('RTADistrict', ''),
                ('RTACity', ''),
                ('VehCode', vehicle.vehicle_code),
                ('YearOfManf', data['manufacturingDate'].split('-')[2]),
                ('DateOfRegitration', data[
                 'registrationDate'].replace('-', '/')),
                ('IDVBasic', data['idv']),
                (
                    'Discounts', (
                        ('Antitheft', 'N'),
                        ('VoluntaryDeductable', _estat(
                            data['voluntaryDeductible'], 'YN')),
                        ('VoluntaryDeductableLimit', _estat(
                            data['voluntaryDeductible'])),
                        ('AAI', 'N'),
                        ('AAINo', ''),
                        ('AAIExpDate', ''),
                        ('RestrictedTPPD', 'N')
                    )
                ),
                (
                    'AdditionalBenefit', (
                        ('CPAReq', 'Y'),
                        ('ElecAccesValue', _estat(
                            data['idvElectrical'], 'YN')),
                        ('ElecValLimPaxIDVDays', _estat(
                            data['idvElectrical'])),
                        ('NonElecAccesValue', _estat(
                            data['idvNonElectrical'], 'YN')),
                        ('NonElecValLimPaxIDVDays', _estat(
                            data['idvNonElectrical'])),
                        ('CNGorLPG', _estat(data['isCNGFitted'], 'YN')),
                        ('InbuiltCNGKit', 'Y' if (
                            data['isCNGFitted'] != '0' and data['cngKitValue'] == '0') else 'N'),
                        ('LegLiabEmployees', 'N'),
                        ('LegLiabEmployeesLimit', ''),
                        ('UnNamedPA', _estat(data['extra_paPassenger'], 'YN')),
                        ('UNPAValLimPaxIDVDays', _estat(
                            data['extra_paPassenger'])),
                        ('CNGValLimPaxIDVDays', _estat(data['cngKitValue'])),
                        ('FibreGlassTank', 'N'),
                        ('legLiabDrivCleanEmp', _estat(
                            data['extra_isLegalLiability'], 'YN')),
                        ('legLiabDrivCleanEmpPax', _estat(
                            data['extra_isLegalLiability'])),
                    )
                ),
                ('AdditionalRemarks', data['remarks']),
                ('NCBPer', NCBper),
                ('AddonReq', 'Y' if addons else 'N'),
                addons,
            )
        )
        return risk_details

    def _get_request_data(self, data, vehicle):
        premium_request_data = (
            'Home', (
                self._get_client_data(data),
                self._get_risk_details(data, vehicle)
            )
        )
        return premium_request_data

    def get_premium(self, data_dictionary):
        if data_dictionary['isUsedVehicle'] == '1':
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_USED_VEHICLE_RESTRICTED}

        vehicle = Vehicle.objects.filter(insurer__slug=INSURER_SLUG).get(
            id=data_dictionary['vehicleId'])

        is_new_vehicle = True if data_dictionary[
            'isNewVehicle'] == '1' else False

        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                data_dictionary['newPolicyStartDate'], "%d-%m-%Y")
            reg_or_man_date = datetime.datetime.strptime(
                data_dictionary['manufacturingDate'], '%d-%m-%Y')
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data_dictionary['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)
            reg_or_man_date = datetime.datetime.strptime(
                data_dictionary['registrationDate'], '%d-%m-%Y')

        data_dictionary['updated_policy_start_date'] = new_policy_start_date.strftime('%d/%m/%Y')
        new_policy_end_date = putils.add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)
        data_dictionary['updated_policy_end_date'] = new_policy_end_date.strftime('%d/%m/%Y')

        if not is_new_vehicle and past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now():
            motor_logger.info('Expired policy not allowed for this insurer %s.' % INSURER_SLUG)
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_EXPIRED_POLICY_RESTRICTED}

        vehicle_age = custom_dictionaries.insurer_utils.get_age(
            new_policy_start_date, reg_or_man_date)

        if vehicle_age >= VEHICLE_AGE_LIMIT:
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_AGE_RESTRICTED}

        rto_code = '%s%02d' % (data_dictionary['registrationNumber[]'][
                               0], int(data_dictionary['registrationNumber[]'][1]))

        if not custom_dictionaries.RTO_CODE_TO_RTO_INFO.get(rto_code, None):
            return {'success': False, 'error_code': IntegrationStatistics.RTO_MAPPING_NOT_FOUND}

        data_dictionary['rto_code'] = rto_code

        manufacturingYear = data_dictionary['manufacturingDate'].split('-')[2]
        remarks = "Company, %s, %s," % (data_dictionary[
                                        'idvElectrical'], manufacturingYear) if data_dictionary['idvElectrical'] != '0' else ''
        remarks += "Company, %s, %s," % (data_dictionary[
                                         'idvNonElectrical'], manufacturingYear) \
            if data_dictionary['idvNonElectrical'] != '0' else ''
        data_dictionary['remarks'] = remarks

        premium_request_data = self._get_request_data(data_dictionary, vehicle)

        request_etree = custom_dictionaries.insurer_utils.convert_tuple_to_xmltree(
            premium_request_data)
        request_xml = etree.tostring(request_etree)

        response = self._get_client(
            data_dictionary['quoteId']).service.Quote(request_xml)

        parsed_result = ElementTree.fromstring(response)
        response_dict = parser_utils.complex_etree_to_dict(parsed_result)

        if data_dictionary.get('request_idv', None):
            idv = float(response_dict['Quote']['IDV']['TotalIDV'])

            if idv == 0:
                return {'success': False, 'error_code': IntegrationStatistics.INSURER_NO_IDV}

            min_idv = 0.9 * idv
            max_idv = 1.1 * idv

            data_dictionary['idv'] = float(data_dictionary['request_idv'])
            del data_dictionary['request_idv']

            request_idv = data_dictionary['idv']
            if request_idv < min_idv:
                data_dictionary['idv'] = min_idv
            elif request_idv > max_idv:
                data_dictionary['idv'] = max_idv

            data_dictionary['min_idv'] = min_idv
            data_dictionary['max_idv'] = max_idv

            return self.get_premium(data_dictionary)

        raw_covers = response_dict['Quote']['QuoteDetails']

        basic_covers_mapping = {}

        addon_covers_mapping = {}

        discounts_mapping = {
            'Discounts': {
                'id': 'extraDiscount',
                'default': '1'
            },
            'NCB': {
                'id': 'ncb',
                'default': '1'
            },
            'LOADDISC': {
                'id': 'odDiscount',
                'default': '1'
            }
        }

        totalTp = 100

        basic_covers = [
            {
                'id': 'paOwnerDriver',
                'default': '1',
                'premium': 100,  # Constant value
            }
        ]

        if data_dictionary['extra_isLegalLiability'] == '1':
            basic_covers.append({
                'id': 'legalLiabilityDriver',
                'default': '0',
                'premium': 50,  # Constant value
            })
            totalTp += 50

        paPassenger = custom_dictionaries.insurer_utils.get_pa_passenger_dict(vehicle.vehicle_type, vehicle.seating_capacity)[
            int(data_dictionary['extra_paPassenger'])]
        if paPassenger:
            basic_covers.append({
                'id': 'paPassenger',
                'default': '1',
                'premium': paPassenger,
            })
            totalTp += paPassenger

        addon_covers = []
        discounts = []
        special_discounts = []
        total_premium = 0
        service_tax = 0

        for raw_cover in raw_covers:
            if raw_cover['Description'] == 'ServTax':
                service_tax = service_tax + float(raw_cover['BOValue'])
            elif raw_cover['Description'] == 'Gross Premium':
                total_premium = total_premium + float(raw_cover['BOValue'])
            elif raw_cover['Description'] == 'IDV':
                if raw_cover['Type'] == 'TP':
                    basic_covers.append(
                        {'id': 'basicTp', 'default': '1', 'premium': float(raw_cover['BOValue'])})
                if raw_cover['Type'] == 'OD':
                    basic_covers.append(
                        {'id': 'basicOd', 'default': '1', 'premium': float(raw_cover['BOValue'])})
            elif raw_cover['Description'] == 'Electrical accessories value':
                if raw_cover['Type'] == 'OD':
                    basic_covers.append(
                        {'id': 'idvElectrical', 'default': '1', 'premium': float(raw_cover['BOValue'])})
            elif raw_cover['Description'] == 'Non Electrical accessories value':
                if raw_cover['Type'] == 'OD':
                    basic_covers.append(
                        {'id': 'idvNonElectrical', 'default': '1', 'premium': float(raw_cover['BOValue'])})
            elif raw_cover['Description'] == 'CNG/LPG':
                if raw_cover['Type'] == 'TP':
                    basic_covers.append(
                        {'id': 'cngKitTp', 'default': '1', 'premium': float(raw_cover['BOValue'])})
                if raw_cover['Type'] == 'OD':
                    basic_covers.append(
                        {'id': 'cngKit', 'default': '1', 'premium': float(raw_cover['BOValue'])})
            elif raw_cover['Description'] == 'Benefits':
                if raw_cover['Type'] == 'OD':
                    basic_covers.append(
                        {'id': 'odExtra', 'default': '1', 'premium': float(raw_cover['BOValue'])})
                if raw_cover['Type'] == 'TP':
                    basic_covers.append(
                        {'id': 'tpExtra', 'default': '1', 'premium': float(raw_cover['BOValue']) - totalTp})
            elif raw_cover['Description'] == 'MOTADON':
                if float(raw_cover['BOValue']) > 0:
                    addon_covers.append(
                        {'id': 'isDepreciationWaiver', 'default': '0', 'premium': float(raw_cover['BOValue'])})
                    addon_covers.append(
                        {'id': 'is247RoadsideAssistance', 'default': '0', 'premium': 0})
            else:
                if raw_cover['Description'] in basic_covers_mapping.keys():
                    mapping = basic_covers_mapping
                    covers = basic_covers
                elif raw_cover['Description'] in discounts_mapping.keys():
                    mapping = discounts_mapping
                    covers = discounts
                elif raw_cover['Description'] in addon_covers_mapping.keys():
                    mapping = addon_covers_mapping
                    covers = discounts
                else:
                    continue

                # temporary hack for removing --(double minus) from premium request
                # which is coming in response of future generali
                if raw_cover['BOValue'][:2] == '--':
                    raw_cover['BOValue'] = raw_cover['BOValue'][1:]

                covers.append({
                    'id': mapping[raw_cover['Description']]['id'],
                    'default': mapping[raw_cover['Description']]['default'],
                    'premium': float(raw_cover['BOValue'])
                })

        total_premium = total_premium + service_tax

        result_data = {
            'insurerId': INSURER_ID,
            'insurerSlug': INSURER_SLUG,
            'insurerName': INSURER_NAME,
            'isInstantPolicy': True,
            'minIDV': int(math.ceil(data_dictionary['min_idv'])),
            'maxIDV': int(math.floor(data_dictionary['max_idv'])),
            'calculatedAtIDV': int(math.floor(data_dictionary['idv'])),
            'serviceTaxRate': 0.145,
            'premiumBreakup': {
                'serviceTax': round(service_tax, 2),
                'basicCovers': custom_dictionaries.insurer_utils.cpr_update(basic_covers),
                'addonCovers': custom_dictionaries.insurer_utils.cpr_update(addon_covers),
                'discounts': custom_dictionaries.insurer_utils.cpr_update(discounts),
                'specialDiscounts': custom_dictionaries.insurer_utils.cpr_update(special_discounts),
                'dependentCovers': self.parent.get_dependent_addons(None),
                'paPassengerAmounts': custom_dictionaries.insurer_utils.get_pa_passenger_dict(vehicle.vehicle_type, vehicle.seating_capacity),
            },
        }, {'total_premium': total_premium}
        return {'success': True, 'result': result_data}


class ProposalGenerator(object):

    client = None

    def __init__(self, integrationUtils):
        self.parent = integrationUtils

    def _get_client(self, name=None):
        if self.client is None:
            self.client = Client(url=SERVICE_URL)

        self.client.set_options(plugins=[suds_logger.set_name(name)])
        return self.client

    def save_policy_details(self, data_dictionary, transaction):
        # Ugly fixes for future proposal
        data_dictionary['cust_first_name'] = data_dictionary['cust_first_name'].replace('-', '.')
        data_dictionary['cust_last_name'] = data_dictionary['cust_last_name'].replace('-', '.')

        transaction.raw['policy_details'] = data_dictionary
        transaction.save()
        return True

    def _get_client_data(self, data, quote_parameters, transaction):
        client = ('Client',
                  (
                      ('Uid', 'CFX%s' % datetime.datetime.now().strftime("%d%f")),
                      ('MajorClass', MAJOR_CLASS),
                      ('ContractType', CONTRACT_TYPE),
                      ('QutationNo', ''),
                      ('InwardNo', ''),
                      ('ProposalNo', ''),
                      ('PolicyNo', ''),
                      ('NewCar', 'Y' if int(
                          quote_parameters['isNewVehicle']) else 'N'),
                      ('Source', 'ECOM'),
                      ('EffectiveDt', data['inception_date']),
                      ('ExpiryDt', data['expiry_date']),
                      ('AgentCode', AGENT_CODE),
                      ('AgentName', AGENT_NAME),
                      ('UserId', USER_ID),
                      ('Password', PASSWORD),
                      ('BranchCode', BRANCH_CODE),
                      ('ClienType', 'A'),  # A - Personal, C - Corporate
                      ('ClientMapTyp', 'N'),
                      ('CustomerId', ''),
                      ('Salutation', data['cust_title']),
                      ('ClientName', data['cust_first_name']),
                      ('SurName', data['cust_last_name']),
                      ('DOB', data['cust_dob']),
                      ('Gender', data['cust_gender']),
                      ('PAN', data['cust_pan']),
                      ('AltIdType', ''),
                      ('AltIdNo', ''),
                      ('CompanyRegNo', ''),
                      ('MaritalStatus', data['cust_marital_status']),
                      ('Occupation', 'OTHR'),  # Default set to Others, data['cust_occupation'])
                      ('Nationality', 'IND'),
                      ('CorrespondenseAddr', 'P'),
                      ('Capital', '1'),
                      ('EconomicActivity', ''),
                      ('CompDateIncorporated', ''),
                      ('AddrLine1', data['add_house_no'] +
                       ' ' + data['add_building_name']),
                      ('AddrLine2', data['add_street_name'] +
                       ' ' + data['add_landmark']),
                      ('AddrLine3', ''),
                      ('Pincode', data['add_pincode']),
                      ('StateCode', data['add_state']),
                      ('StateDesc', custom_dictionaries.STATE_CODE_LIST[data['add_state']]),
                      ('City', data['add_city']),
                      ('CountryCode', 'IND'),
                      ('CountryDesc', 'INDIA'),
                      ('TelNo', ''),
                      ('MobileNo', data['cust_phone']),
                      ('FaxNo', ''),
                      ('AltTelNo', ''),
                      ('EmailAddr', data['cust_email']),
                      ('WebURL', ''),
                      ('DespAddrTyp', 'P'),
                      ('DespAddr1', data['add_house_no'] +
                       ' ' + data['add_building_name']),
                      ('DespAddr2', data['add_street_name'] +
                       ' ' + data['add_landmark']),
                      ('DespAddr3', ''),
                      ('DespPincode', data['add_pincode']),
                      ('DespStateCode', data['add_state']),
                      ('DespStateDesc', custom_dictionaries.STATE_CODE_LIST[data['add_state']]),
                      ('DespCity', data['add_city']),
                      ('DespCountryCode', 'IND'),
                      ('DespCountryDesc', 'INDIA'),
                      ('DespTelNo', ''),
                      ('DespMobNo', data['cust_phone']),
                      ('DespFaxNo', ''),
                      ('DespAltTelNo', ''),
                      ('DespEmailAddr', data['cust_email']),
                      ('TranNo', transaction.reference_id),
                      ('AuthCode', transaction.payment_token),
                  ),
                  )
        return client

    def _get_addons(self, quote_parameters):
        addons = (
            'Addon', (
                ('CoverCode', 'PLAN1'),
                ('CoverName', 'Combined Plan'),
            )
        )
        # Key replacement, Loss of Personal Belongings, Zero Dep, RSA in PLAN1
        if quote_parameters['addon_isDepreciationWaiver'] == '1' or quote_parameters['addon_is247RoadsideAssistance'] == '1':
            return addons
        else:
            return None

    def _get_discounts(self, data, quote_parameters):
        discounts = ('Discounts', (
            ('Antitheft', 'N'),
            ('HandiCap', 'N'),
            ('AAI', 'N'),
            ('AAINo', ''),
            ('AAIExpDate', ''),
            ('VintageCar', 'N'),
            ('OwnPremises', 'N'),
            ('VoluntaryDeductable', _estat(
                quote_parameters['voluntaryDeductible'], 'YN')),
            ('VoluntaryDeductableLimit', _estat(
                quote_parameters['voluntaryDeductible'])),
            ('RestrictedTPPD', 'N'),
            ('SideCar', 'N'),
        )
        )
        return discounts

    def _get_vehicle_insurance_details(self, data, quote_parameters):
        try:
            past_policy_insurer_code = custom_dictionaries.REV_PASTINSURER_MAP[data['past_policy_insurer']]
        except:
            past_policy_insurer_code = ''

        is_new_vehicle = (quote_parameters['isNewVehicle'] == '1')
        is_claimed_last_year = (quote_parameters['isClaimedLastYear'] == '1')
        NCBDeclartion = 'N'
        if int(quote_parameters['previousNCB']):
            NCBDeclartion = 'Y'

        vehicle_insurance_details = ('PreviousInsDtls', (
            ('UsedCar', (
                ('PurchaseDate', ''),
                ('InspectionRptNo', ''),
                ('InspectionDt', '')
            ),
                ('Value', 'N')
            ),
            ('RollOver', (
                ('PolicyNo', '' if is_new_vehicle else data['past_policy_number']),
                ('PreviousPolExpDt', '' if is_new_vehicle else data['past_policy_end_date']),
                # For UAT 40062891
                ('ClientCode', past_policy_insurer_code),
                ('Address1', data['add_city']),
                ('Address2', data['add_state']),
                ('Address3', ''),
                ('Address4', ''),
                ('Address5', ''),
                ('PinCode', data['add_pincode']),
                ('InspectionRptNo', ''),
                ('InspectionDt', ''),  # TODO
                ('NCBDeclartion', NCBDeclartion),  # TODO
            ),
                ('Value', 'N' if quote_parameters[
                 'isNewVehicle'] == '1' else 'Y'),
            ),
            ('NewVehicle', (
                ('InspectionRptNo', ''),  # TODO
                ('InspectionDt', ''),  # TODO
            ),
                ('Value', 'Y' if quote_parameters[
                 'isNewVehicle'] == '1' else 'N'),
            ),
        )
        )
        return vehicle_insurance_details

    def _get_additional_benefits(self, data, quote_parameters):

        additional_benefits = ('AdditionalBenefit', (
            ('AlternateAccomodation', 'N'),
            ('AltAccomodationVal', ''),
            ('ArchSurveEnggFee', 'N'),
            ('ArchSurveEnggFeeVal', ''),
            ('PowerStationDamage', 'N'),
            ('PowerStationDamageVal', ''),
            ('StorageMachinaryDamage', 'N'),
            ('StorageMachinaryDamageVal', ''),
            ('Earthquake', 'N'),
            ('EarthquakeVal', ''),
            ('ForestFire', 'N'),
            ('ForestFireVal', ''),
            ('MudguardCover', 'N'),
            ('MudguardCoverVal', ''),
            ('LeakageCover', 'N'),
            ('LeakageCoverVal', ''),
            ('LeakageContaminationCover', 'N'),
            ('LeakageContaminationCoverVal', ''),
            ('LossRent', 'N'),
            ('LossRentVal', ''),
            ('OmissionInsAddAltExt', 'N'),
            ('OmissionInsAddAltExtVal', ''),
            ('OverturningCranes', 'N'),
            ('DebrisRemoval', 'N'),
            ('DebrisRemovalVal', ''),
            ('SpontaneousCombustion', 'N'),
            ('SpontaneousCombustionVal', ''),
            ('StartupExpenses', 'N'),
            ('StartupExpensesVal', ''),
            ('SpoilMaterialDamageCov', 'N'),
            ('SpoilMaterialDamageCovVal', ''),
            ('Terrorism', 'N'),
            ('TerrorismVal', ''),
            ('TempRemovalOfStock', 'N'),
            ('TempRemovalOfStockVal', ''),
            ('PrivateorCommercialUsage', 'N'),
            ('ElecAccesValue', _estat(
                quote_parameters['idvElectrical'], 'YN')),
            ('ElecValLimPaxIDVDays', _estat(
                quote_parameters['idvElectrical'])),
            ('NonElecAccesValue', _estat(
                quote_parameters['idvNonElectrical'], 'YN')),
            ('NonElecValLimPaxIDVDays', _estat(
                quote_parameters['idvNonElectrical'])),
            ('CNGorLPG', _estat(quote_parameters['isCNGFitted'], 'YN')),
            ('InbuiltCNGKit', 'Y' if (quote_parameters[
                'isCNGFitted'] != '0' and quote_parameters['cngKitValue'] == '0') else 'N'),
            ('CNGValLimPaxIDVDays', _estat(
                quote_parameters['cngKitValue'])),
            ('Trailer', ''),
            ('TrailerTowedBy', 'N'),
            ('TrailerRegNo', ''),
            ('NoOfTrailer', ''),
            ('TrailerValLimPaxIDVDays', ''),
            ('CPAReq', 'Y'),
            ('CPA', (
                ('CPANomName', data['nominee_name']),
                ('CPANomAge', data['nominee_age']),
                ('CPANomAgeDet', 'Y'),
                ('CPANomPerc', '100'),
                ('CPARelation', custom_dictionaries.NOMINEE_RELATIONSHIP_LIST[
                    data['nominee_relationship']]),
                ('CPAAppointeeName', ''),
                ('CPAAppointeRel', ''),
            )
            ),
            ('UnNamedPA', _estat(quote_parameters[
                'extra_paPassenger'], 'YN')),
            ('UNPAValLimPaxIDVDays', _estat(
                quote_parameters['extra_paPassenger'])),
            ('NamedPAReq', 'N'),
            ('NPAName', (
                ('npaname', ''),
                ('npalimit', ''),
                ('npanomname', ''),
                ('npanomage', ''),
                ('npanomagedet', ''),
                ('nparel', ''),
                ('npaappinteename', ''),
                ('npaappinteerel', ''),
            )
            ),
            ('NPAValLimPaxIDVDays', ''),
            ('PADriverCleanCondReq', 'N'),
            ('PADriverCleanCond',
             (
                 ('PADriverClanCondNoOfPerson', ''),
                 ('PADriverClanCondLimit', '')
             )
             ),
            ('GeoArea', 'N'),
            ('GeoLocation', ''),
            ('EmbassyLoading', ''),
            ('FibreGlassTank', 'N'),
            ('DrivingTution', ''),
            ('Rallies', ''),
            ('RalliesDays', ''),
            ('legLiabDrivCondClean', 'N'),
            ('legLiabDrivCondCleanPax', ''),
            ('ReInstate', 'N'),
            ('legLiabDrivCleanEmp', _estat(
                quote_parameters['extra_isLegalLiability'], 'YN')),
            ('legLiabDrivCleanEmpPax', _estat(
                quote_parameters['extra_isLegalLiability'])),
            ('LegLiabDefencePers', ''),
            ('LegLiabDefencePersPax', ''),
            ('LegLiabEmployees', 'N'),
            ('LegLiabEmployeesLimit', '0'),
            ('LegLiabOthEmployees', 'N'),
            ('LegLiabOthEmployeesLimit', ''),
            ('LegLiabNFPayPass', 'N'),
            ('LegLiabNFPayPassPax', ''),
            ('LegLiabOppMaint', 'N'),
            ('LegLiabOppMaintPax', ''),
            ('LegLiabAmbulance', 'N'),
            ('LegLiabAmbulanceLimit', ''),
            ('LegLiabHearses', 'N'),
            ('LegLiabHearsesLimit', ''),
        )
        )
        return additional_benefits

    def _get_risk(self, data, quote_parameters, transaction):
        vehicle = Vehicle.objects.filter(
            insurer__slug=INSURER_SLUG).get(id=data['vehicleId'])
        rto_info = custom_dictionaries.RTO_CODE_TO_RTO_INFO[data['rto_code']]
        age_of_customer = int(math.floor(custom_dictionaries.insurer_utils.get_age(
            datetime.datetime.now(), datetime.datetime.strptime(data['cust_dob'], '%d/%m/%Y'))))
        vehicle_data = vehicle.raw_data.split('|')

        is_new_vehicle = (quote_parameters['isNewVehicle'] == '1')
        is_claimed_last_year = (quote_parameters['isClaimedLastYear'] == '1')

        if is_new_vehicle or is_claimed_last_year:
            ncb = '0'
        else:
            ncb = custom_dictionaries.PREVIOUS_NCB_TO_NEW_NCB[quote_parameters['previousNCB']]

        addons = self._get_addons(quote_parameters)
        risk = ('Risk',
                (
                    ('AdditionalDeductibleorExcess', ''),
                    ('VehicleClass', 'FPV'),
                    ('TypeofInsurance', custom_dictionaries.TYPE_OF_INSURANCE['Comprehensive']),
                    ('Class', ''),
                    ('TypeOfVeh', ''),
                    ('RTACode', data['rto_code']),
                    ('RTAState', data['rto_state']),
                    ('RTADistrict', rto_info['RTODistrict']),
                    ('RTACity', rto_info['RTOCity']),
                    ('VehCode', vehicle.vehicle_code),
                    ('YearOfManf', quote_parameters[
                        'manufacturingDate'].split('-')[2]),
                    ('MakeOfVeh', vehicle.make),
                    ('ModelOfVehicle', vehicle.model),
                    ('DateOfRegitration', quote_parameters[
                        'registrationDate'].replace('-', '/')),
                    ('CCofVeh', str(vehicle.cc)),
                    ('BodyType', 'SOLO'),  #  custom_dictionaries.BODY_TYPE[vehicle_data[9]]),
                    ('FuelType', custom_dictionaries.FUEL_TYPE[vehicle_data[6]]),
                    ('RegistrationNo', data[
                     'vehicle_reg_no'].replace('-', '')),
                    ('ChasisNo', data['vehicle_chassis_no']),
                    ('EngineNo', data['vehicle_engine_no']),
                    ('IDVBasic', transaction.raw[
                        'quote_response']['calculatedAtIDV']),
                    ('IDVBody', ''),
                    ('Garage', '02'),
                    ('PermitDriver', ''),
                    ('PerDayMileage', '2'),
                    ('Repair', ''),
                    ('RoadTypeCodes', '02'),
                    ('SpeedoMeterReading', '2'),
                    ('ColourofVeh', ''),
                    ('ClaimsIncurredCodes', ''),
                    ('VehPlyingCity', ''),
                    ('VehPlyingState', ''),
                    ('VehCategory', ''),
                    ('HiredorOwnedByState', 'N'),
                    ('NatureofGoods', ''),
                    self._get_additional_benefits(data, quote_parameters),
                    self._get_discounts(data, quote_parameters),
                    ('NCBPer', ncb),
                    ('AddonEnq', ''),
                    ('AddonReq', 'Y' if addons else 'N'),
                    addons,
                    ('AdditionalRemarks', data['remarks']),
                    ('PendingRemarks', ''),
                    ('IntrestedParty', 'Y' if data[
                        'is_car_financed'] == '1' else 'N'),
                    ('IntrestedPartyCltName', data[
                        'car-financier'] if data['is_car_financed'] == '1' else ''),
                    ('IntrestedPartyRel', 'HY' if data[
                        'is_car_financed'] == '1' else ''),
                    ('SpecialCondition', ''),
                    ('DriverDetails', (
                        ('Name', data['cust_first_name']),
                        ('DOB', data['cust_dob']),
                        ('Age', age_of_customer),
                        ('Gender', data['cust_gender']),
                        ('DriverExperience', '02'),
                        ('Qualification', '02'),  # Fixed for UAT
                        ('NoOfAccident', '00'),  # Fixed for UAT
                        ('ClaimHistory', '01'),  # Fixed for UAT
                    )
                    ),
                    self._get_vehicle_insurance_details(
                        data, quote_parameters),
                )
                )
        return risk

    def _get_proposal_request(self, data, quote_parameters, transaction):
        proposal_request_data = ('Home', (
            self._get_client_data(data, quote_parameters, transaction),
            self._get_risk(data, quote_parameters, transaction)
        )
        )
        return proposal_request_data


class IntegrationUtils(object):

    def __init__(self):
        self.PremiumCalculator = PremiumCalculator(self)
        self.ProposalGenerator = ProposalGenerator(self)

    def get_city_list(self, state):
        cities = []
        for k, v in custom_dictionaries.RTO_CODE_TO_RTO_INFO.items():
            k_state = custom_dictionaries.RTO_STATE_TO_ACTUAL_STATE[k[0:2]]
            if k_state == state:
                cities.append({'name': v['RTODistrict'], 'id': v['RTOCity']})
        return cities

    def convert_premium_request_data_to_required_dictionary(self, vehicle_id, request_data):
        data = copy.deepcopy(request_data)
        data['masterVehicleId'] = request_data['vehicleId']
        data['vehicleId'] = vehicle_id
        data['request_idv'] = data['idv']
        data['idv'] = ''

        is_new_vehicle = (data['isNewVehicle'] == '1')
        if is_new_vehicle:
            vehicle_age = 0
        else:
            # past_policy_end_date = datetime.strptime(data['pastPolicyExpiryDate'], "%d-%m-%Y")
            # new_policy_start_date = past_policy_end_date + timedelta(days=1)
            date_of_registration = datetime.datetime.strptime(data['registrationDate'], '%d-%m-%Y')
            # vehicle_age = insurer_utils.get_age(datetime.now(), date_of_registration)

            # Age logic differs for addons
            vehicle_age = datetime.datetime.now().year - date_of_registration.year

        split_registration_no = data.get('registrationNumber[]')
        master_rto = "%s-%s" % (split_registration_no[0], split_registration_no[1])

        addons = ['isDepreciationWaiver', 'is247RoadsideAssistance']
        vehicle_model = VehicleMaster.objects.get(id=data['masterVehicleId']).model
        for addon in addons:
            if request_data['addon_' + addon] == '1' and (
                custom_dictionaries.insurer_utils.is_addon_available(addon,
                                                                     INSURER_SLUG,
                                                                     vehicle_model,
                                                                     vehicle_age, master_rto)):
                data['addon_' + addon] = '1'
            else:
                data['addon_' + addon] = '0'
        return data

    def get_dependent_addons(self, quote_parameters):
        return {
            'isDepreciationWaiver': ['is247RoadsideAssistance'],
            'is247RoadsideAssistance': ['isDepreciationWaiver'],
            'type': 'dynamic',
        }

    def get_buy_form_details(self, quote_parameters, transaction):
        split_registration_no = quote_parameters.get('registrationNumber[]')

        is_new_vehicle = True if quote_parameters[
            'isNewVehicle'] == '1' else False
        if is_new_vehicle:
            registration_number = 'NEW'
        else:
            registration_number = '-'.join(split_registration_no)

        master_rto_code = "%s-%s" % (
            split_registration_no[0], split_registration_no[1])

        data = {
            'registration_no': registration_number,
            'rto_location_info': master_rto_code,
            # 'occupation_list': OCCUPATION_LIST,
            # 'insurers_list': PAST_INSURANCE_ID_LIST,
            'insurers_list': custom_dictionaries.PAST_INSURER_ID_LIST_MASTER,
            'disallowed_insurers': custom_dictionaries.DISALLOWED_PAST_INSURERS,
            'state_list': custom_dictionaries.STATE_CODE_LIST,
            'relationship_list': custom_dictionaries.NOMINEE_RELATIONSHIP_LIST.keys(),
            'is_cities_fetch': True,
        }
        return data

    def convert_proposal_request_data_to_required_dictionary(self, vehicle,
                                                             request_data, quote_parameters,
                                                             quote_response, quote_custom_response):
        form_details = copy.deepcopy(request_data)

        is_new_vehicle = False
        if quote_parameters['isNewVehicle'] == '1':
            is_new_vehicle = True

        form_details['cust_title'] = 'MR' if form_details['cust_gender'] == 'Male' else (
            'MS' if form_details['cust_marital_status'] == '0' else 'MRS')
        form_details['cust_gender'] = custom_dictionaries.GENDER_MAPPING[
            form_details['cust_gender']]
        form_details['cust_marital_status'] = 'M' if form_details[
            'cust_marital_status'] == '1' else 'S'
        form_details['cust_dob'] = form_details['cust_dob'].replace('-', '/')

        if is_new_vehicle:
            form_details['vehicle_reg_no'] = 'NEW'

        form_details['vehicleId'] = vehicle.id
        rto_code = '%s%02d' % (quote_parameters['registrationNumber[]'][
                               0], int(quote_parameters['registrationNumber[]'][1]))
        form_details['rto_code'] = rto_code
        form_details['rto_state'] = custom_dictionaries.RTO_STATE_TO_ACTUAL_STATE[rto_code[0:2]]

        manufacturingYear = quote_parameters['manufacturingDate'].split('-')[2]
        remarks = "Company, %s, %s," % (quote_parameters[
                                        'idvElectrical'], manufacturingYear) if quote_parameters['idvElectrical'] != '0' else ''
        remarks += "Company, %s, %s," % (quote_parameters['idvNonElectrical'], manufacturingYear) \
            if quote_parameters['idvNonElectrical'] != '0' else ''
        form_details['remarks'] = remarks

        if not is_new_vehicle:
            pyp_end_date = datetime.datetime.strptime(
                quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")
            # pyp_start_date = putils.subtract_years(pyp_end_date, 1) + timedelta(days=1)

            inception_date = pyp_end_date + datetime.timedelta(days=1)
            form_details['past_policy_end_date'] = pyp_end_date.strftime(
                '%d/%m/%Y')
        else:
            form_details['past_policy_end_date'] = ''
            inception_date = datetime.datetime.strptime(
                quote_parameters['newPolicyStartDate'], '%d-%m-%Y')

        expiry_date = putils.add_years(inception_date, 1) - datetime.timedelta(days=1)
        form_details['inception_date'] = inception_date.strftime('%d/%m/%Y')
        form_details['expiry_date'] = expiry_date.strftime('%d/%m/%Y')
        return form_details

    def update_transaction_details(self, transaction, data):
        is_new_vehicle = data['isNewVehicle'] == '1'
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                data['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)
        new_policy_end_date = putils.add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)

        transaction.policy_start_date = new_policy_start_date
        transaction.policy_end_date = new_policy_end_date
        transaction.save()

    def get_payment_data(self, transaction):
        response_url = '%s/motor/fourwheeler/insurer/%s/payment/response/%s/?' % (
            settings.SITE_URL.rstrip('/'), INSURER_SLUG, transaction.transaction_id)
        transaction.premium_paid = transaction.raw[
            'quote_raw_response']['total_premium']
        payment_gateway_data = {
            'TransactionID': 'CF%s' % datetime.datetime.now().strftime("%f%s"),
            'PaymentOption': 2,
            'ResponseURL': response_url,
            'ProposalNumber': transaction.transaction_id,
            'PremiumAmount': transaction.premium_paid,
            'UserIdentifier': USER_ID,
            'UserId': 'CVFXUser%s' % transaction.proposer.id,
            'FirstName': transaction.proposer.first_name,
            'LastName': transaction.proposer.last_name,
            'Mobile': transaction.proposer.mobile,
            'Email': transaction.proposer.email,
        }
        transaction.payment_request = payment_gateway_data
        transaction.save()

        return payment_gateway_data, PAYMENT_URL

    def post_payment(self, payment_response, transaction):
        # Sample Response
        # {
        #     "TID": "CF2674041436513565",
        #     "PGID": "CTX1504211250569289255",
        #     "Premium": "63851",
        #     "Response": "Failure",
        #     "WS_P_ID": "TN009134"
        # }
        motor_logger.info(payment_response)
        suds_logger.set_name(transaction.transaction_id).log(json.dumps(payment_response))
        redirect_url = '/motor/fourwheeler/payment/failure/%s/' % transaction.transaction_id
        is_redirect = True

        response = payment_response['GET']
        transaction.insured_details_json['payment_response'] = response
        transaction.payment_response = payment_response
        transaction.proposal_number = response['TID']
        transaction.payment_token = response['WS_P_ID']
        transaction.reference_id = response['PGID']
        transaction.premium_paid = response['Premium']
        transaction.save()

        if response['Response'] == 'Success':
            transaction.payment_done = True
            transaction.payment_on = datetime.datetime.now()
            transaction.mark_complete()
            redirect_url = '/motor/fourwheeler/payment/success/%s/' % transaction.transaction_id

            # self.ProposalGenerator.send_proposal.delay(transaction)
            send_proposal.delay(transaction)
        else:
            transaction.status = 'FAILED'
            transaction.save()

        return is_redirect, redirect_url, '', {}

    def get_custom_data(self, datatype):
        datamap = {
            'occupations': custom_dictionaries.OCCUPATION_LIST,
            # 'insurers': PAST_INSURANCE_ID_LIST,
            'insurers': custom_dictionaries.PAST_INSURER_ID_LIST_MASTER,
            'states': custom_dictionaries.STATE_CODE_LIST,
            'relationships': custom_dictionaries.NOMINEE_RELATIONSHIP_LIST.keys(),
        }
        return datamap.get(datatype, None)

    def disable_cache_for_request(self, request_data):
        if (request_data['isNewVehicle'] != '1' and
            datetime.datetime.strptime(
                request_data['pastPolicyExpiryDate'], '%d-%m-%Y'
                ).replace(hour=23, minute=59, second=0) < datetime.datetime.now()):
            return True
        return False

    def get_cache_key(self, request_data):
        if request_data['idv'] != '0':
            return None

        caching_keys = ['isUsedVehicle', 'isNewVehicle', 'vehicleId', 'idv',
                        'idvElectrical', 'idvNonElectrical',
                        'isCNGFitted', 'cngKitValue', 'voluntaryDeductible']

        hashing_dictionary = {}
        for key in caching_keys:
            if request_data.get(key, None):
                hashing_dictionary[key] = request_data[key]

        rto_info = "-".join(request_data['registrationNumber[]'][0:2])

        hashing_dictionary['registrationRTO'] = rto_info

        if not request_data['isNewVehicle'] == '1':
            # hashing_dictionary['registrationDate'] = request_data['registrationDate']
            if not request_data['isUsedVehicle'] == '1':
                if request_data['isClaimedLastYear'] == '0':
                    hashing_dictionary['previousNCB'] = request_data['previousNCB']
                hashing_dictionary['isClaimedLastYear'] = request_data[
                    'isClaimedLastYear']
                # hashing_dictionary['pastPolicyExpiryDate'] = request_data[
                #     'pastPolicyExpiryDate']
                registration_date = datetime.datetime.strptime(request_data['registrationDate'], '%d-%m-%Y')
                # past_policy_end_date = datetime.strptime(request_data['pastPolicyExpiryDate'], '%d-%m-%Y')

                year_difference = datetime.datetime.now().year - registration_date.year

                if registration_date <= putils.subtract_years(datetime.datetime.now(), year_difference):
                    hashing_dictionary['Age'] = '#FH%s' % registration_date.year
                else:
                    hashing_dictionary['Age'] = '#SH%s' % registration_date.year

        hashing_dictionary['insurer'] = INSURER_SLUG

        hashing_json = json.dumps(hashing_dictionary, sort_keys=True)
        store_key = hashlib.md5(hashing_json).hexdigest()
        return store_key

    @staticmethod
    def get_transaction_from_id(transaction_id):
        return Transaction.objects.get(transaction_id=transaction_id)


def view_proposal_xml(transaction):
    if transaction.insurer.slug != INSURER_SLUG:
        return None

    data_dictionary = transaction.raw['policy_details']
    quote_parameters = transaction.quote.raw_data
    integrationutils = IntegrationUtils()
    proposal_request_data = integrationutils.ProposalGenerator._get_proposal_request(
        data_dictionary, quote_parameters, transaction)
    request_etree = custom_dictionaries.insurer_utils.convert_tuple_to_xmltree(
        proposal_request_data)
    request_xml = etree.tostring(request_etree)
    return request_xml
