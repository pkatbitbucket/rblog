from motor_product.models import Vehicle, Insurer, VehicleMaster, VehicleMapping
from motor_product import parser_utils

INSURER_SLUG = 'future-generali'
INSURER_MASTER_PATH = 'motor_product/insurers/future_generali/scripts/data/future_fourwheeler.csv'
MASTER_PATH = 'motor_product/scripts/fourwheeler/data/master_fourwheeler.csv'

csv_parameters = {
    'Vehicle Code': 1,
    'MAKE': 2,
    'Model': 3,
    'CC': 4,
    'Fuel Code': 6,
    'Seating Capacity': 7,
    'Master': 39,
}

parameters = {
    'vehicle_code': 1,
    'make': 2,
    'model': 3,
    'fuel_type': 6,
    'cc': 4,
    'seating_capacity': 7,
    'master': 39,
}

master_parameters = {
    'make': 0,
    'model': 1,
    'variant': 2,
    'fuel_type': 3,
    'cc': 4,
    'seating_capacity': 5,
    'insurer': 8,  # Refer to master csv
}


def convert_fuel_type_literals_into_fuel_type(fuel_type_literal):
    if fuel_type_literal == 'PETROL':
        fuel_type = 'Petrol'
    elif fuel_type_literal == 'DIESEL':
        fuel_type = 'Diesel'
    elif fuel_type_literal in ['CNG/PETROL', 'CNG']:
        fuel_type = 'CNG / Petrol'
    elif fuel_type_literal == 'LPG':
        fuel_type = 'LPG / Petrol'
    elif fuel_type_literal == 'ELECTRICITY':
        fuel_type = 'Electricity'
    else:
        fuel_type = 'Not Defined'
    return fuel_type


def save_data():
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_MASTER_PATH, parameters)
    insurer = Insurer.objects.get(slug=INSURER_SLUG)

    vehicle_objects = []
    for _vehicle in vehicles:
        _vehicle['cc'] = parser_utils.try_parse_into_integer(_vehicle['cc'])
        _vehicle['seating_capacity'] = parser_utils.try_parse_into_integer(_vehicle[
                                                                           'seating_capacity'])
        _vehicle['fuel_type'] = convert_fuel_type_literals_into_fuel_type(_vehicle[
                                                                          'fuel_type'])
        _vehicle['insurer'] = insurer

        del _vehicle['master']

        vehicle = Vehicle(**_vehicle)
        print 'Saving vehicle %s' % vehicle.model
        vehicle_objects.append(vehicle)

    Vehicle.objects.bulk_create(vehicle_objects)


def delete_data():
    Vehicle.objects.filter(vehicle_type='Private Car',
                           insurer__slug=INSURER_SLUG).delete()


def delete_mappings():
    VehicleMapping.objects.filter(
        insurer__slug=INSURER_SLUG, master_vehicle__vehicle_type='Private Car').delete()


def create_mappings():
    insurer = Insurer.objects.get(slug=INSURER_SLUG)
    master_vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        MASTER_PATH, master_parameters)
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_MASTER_PATH, parameters)

    ivehicles = Vehicle.objects.filter(
        vehicle_type='Private Car', insurer__slug=INSURER_SLUG)

    vehicle_map = {}
    for iv in vehicles:
        _vehicle = ivehicles.get(vehicle_code=iv['vehicle_code'])
        vehicle_map[iv['master']] = _vehicle

    mappings = []
    for mv in master_vehicles:
        _master = VehicleMaster.objects.get(make__name=mv['make'], model__name=mv['model'],
                                            variant=mv['variant'], cc=mv['cc'],
                                            fuel_type=mv['fuel_type'],
                                            seating_capacity=mv['seating_capacity'])
        mapping = VehicleMapping()
        mapping.master_vehicle = _master
        mapping.mapped_vehicle = vehicle_map.get(mv['insurer'], None)
        mapping.insurer = insurer

        if mapping.mapped_vehicle:
            print 'Mapping %s to %s' % (_master, mapping.mapped_vehicle)
            mappings.append(mapping)

    VehicleMapping.objects.bulk_create(mappings)
