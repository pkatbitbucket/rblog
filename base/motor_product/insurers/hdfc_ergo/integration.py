import datetime
import json
import logging
import re
import math
from xml.sax.saxutils import escape
import putils
from lxml import etree
from motor_product import logger_utils, mail_utils, parser_utils
from motor_product.insurers import insurer_utils
from motor_product.models import (Insurer, RTOMaster, Transaction, Vehicle,
                                  VehicleMaster, IntegrationStatistics)
from suds.client import Client
from utils import motor_logger, log_error

from . import custom_dictionaries

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)

######################## PRODUCTION URL###########################################
PREMIUM_WSDL = 'https://hewspool.hdfcergo.com/wscalculate/service.asmx?wsdl'
BREAKIN_WSDL = None
PROPOSAL_WSDL = {
    'Private Car': 'https://hewspool.hdfcergo.com/MotorCp/service.asmx?wsdl',
    'Twowheeler': 'https://hewspool.hdfcergo.com/TWWS/service.asmx?wsdl',
}
PAYMENT_URL = {
    'Private Car': 'https://netinsure.hdfcergo.com/OnlineProducts/MotorOnline/TIM.aspx',
    'Twowheeler': 'https://netinsure.hdfcergo.com/onlineproducts/twonline/TIM.aspx',
}
##################################################################################
########################### UAT URL ##############################################
# PREMIUM_WSDL = 'http://202.191.196.210/uat/onlineproducts/wscalculate/service.asmx?wsdl'
# BREAKIN_WSDL = 'http://202.191.196.210/UAT/OnlineProducts/newmotorcp/service.asmx?wsdl'
# PROPOSAL_WSDL = {
#        'Private Car'   :   'http://202.191.196.210/uat/onlineproducts/newmotorcp/service.asmx?wsdl',
#        'Twowheeler'    :   'http://202.191.196.210/uat/onlineproducts/newtwcp/service.asmx?wsdl',
#        }
# PAYMENT_URL =   {
#        'Private Car'   :   'http://202.191.196.210/UAT/OnlineProducts/MotorOnlineBreakin/TIM.aspx',
#        'Twowheeler'    :   'http://202.191.196.210/uat/onlineproducts/twOnlinetariff/TIM.aspx',
#        }
##################################################################################
INSURER_SLUG = 'hdfc-ergo'
try:
    INSURER = Insurer.objects.get(slug=INSURER_SLUG)
    INSURER_ID = INSURER.id
    INSURER_NAME = INSURER.title
except:
    INSURER_ID = 0
    INSURER_NAME = ''

suds_logger = logger_utils.getLogger(INSURER_SLUG)


def validate_for_hdfc(input_value):
    output_value = input_value
    if len(output_value) < 5:
        return output_value
    matches = re.findall(r'((\w)\2{4,})', input_value)
    if matches:
        output_value = input_value.split(matches[0][0][:1], 1)[1]
        output_value = validate_for_hdfc(output_value)
    return output_value


class Configuration(object):
    def __init__(self, vehicle_type):
        self.VEHICLE_TYPE = vehicle_type
        self.PROPOSAL_WSDL = PROPOSAL_WSDL[vehicle_type]
        self.PAYMENT_URL = PAYMENT_URL[vehicle_type]
        if vehicle_type == 'Private Car':
            self.VEHICLE_CLASS_CODE = 45
            self.VEHICLE_AGE_LIMIT = 15
            self.AGENT_CODE = 'CVFX001'
            self.PRODUCT_CODE = 'MOT'
            self.URL_TAG = 'fourwheeler'
        elif vehicle_type == 'Twowheeler':
            self.VEHICLE_CLASS_CODE = 37
            self.VEHICLE_AGE_LIMIT = 15
            self.AGENT_CODE = 'CVFX002'
            self.PRODUCT_CODE = 'TW'
            self.URL_TAG = 'twowheeler'


class PremiumCalculator(object):
    def __init__(self, integrationUtils, configuration):
        self.Configuration = configuration
        self.parent = integrationUtils

    def get_client(self, name=None):
        if not hasattr(self, 'client'):
            setattr(self, 'client', Client(PREMIUM_WSDL))
        self.client.set_options(plugins=[suds_logger.set_name(name)])
        return self.client

    def is_quote_valid(self, data_dictionary):
        vehicle = Vehicle.objects.filter(insurer__slug=INSURER_SLUG).get(id=data_dictionary['VehicleId'])
        return not bool(re.search('PAJERO', vehicle.model))

    def get_premium(self, data_dictionary):
        if not self.is_quote_valid(data_dictionary):
            return {'success': False, 'error_code': IntegrationStatistics.COVERFOX_MAKE_RESTRICTED}

        if int(data_dictionary['vehicleAge']) >= self.Configuration.VEHICLE_AGE_LIMIT:
            motor_logger.info('Vehicle age limit not valid for this insurer %s.' % INSURER_SLUG)
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_AGE_RESTRICTED}

        if data_dictionary['isUsedVehicle']:
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_USED_VEHICLE_RESTRICTED}

        if data_dictionary['isExpired'] and self.Configuration.VEHICLE_TYPE == 'Private Car':
            motor_logger.info('Expired policy not allowed for this insurer %s.' % INSURER_SLUG)
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_EXPIRED_POLICY_RESTRICTED}
        allowed_previous_ncb = {
            '1': '0',
            '2': '20',
            '3': '25',
            '4': '35',
            '5': '45',
            '6': '50'
        }

        if self.Configuration.VEHICLE_TYPE == "Twowheeler":
            vehicle_age_min = int(math.floor(data_dictionary['vehicleAge']))
            if int(math.floor(data_dictionary['vehicleAge'])) > 6:
                allowed_previous_ncb[str(vehicle_age_min)] = '50'
            if data_dictionary['pcv_previousdiscount'] != '0':
                if data_dictionary['pcv_previousdiscount'] > allowed_previous_ncb[str(vehicle_age_min)]:
                    motor_logger.info(" NCB does not lies as per the rule.")
                    return {'success': False, 'error_code': IntegrationStatistics.NCB_LIMIT_EXCEEDED}

        pcv_calc_dict = self._get_required_params_for_premium_calculation(data_dictionary)
        pcv_calc_request = parser_utils.dict_to_xml('PCVPremiumCalc', pcv_calc_dict)

        resp = self.get_client(data_dictionary['quoteId']).service.getPremium(pcv_calc_request,
                                                                              self.Configuration.VEHICLE_CLASS_CODE)
        resp = insurer_utils.xml2dict(resp)

        coveragePremiumDetails = resp
        coveragePremiumDetails['ex_showroom_price'] = data_dictionary['ex_showroom_price']
        coveragePremiumDetails['addon_isDepreciationWaiver'] = data_dictionary['pcv_num_is_zero_dept']
        coveragePremiumDetails['addon_is247RoadsideAssistance'] = data_dictionary['pcv_num_is_emr_asst_cvr']
        coveragePremiumDetails['extra_isLegalLiability'] = data_dictionary['extra_isLegalLiability']
        coveragePremiumDetails['extra_paPassenger'] = data_dictionary['extra_paPassenger']
        coveragePremiumDetails['PlanType'] = pcv_calc_dict['txt_plan_type']
        coveragePremiumDetails['IsRTICover'] = pcv_calc_dict['num_is_rti']

        if int(coveragePremiumDetails['NUM_TOTAL_PREMIUM']) == 0:
            motor_logger.info('Zero premium error from server for %s.' % INSURER_SLUG)
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_ZERO_PREMIUM}

        return self.convert_premium_response(coveragePremiumDetails, data_dictionary)

    def _get_idv_response(self, data_dictionary, vehicle):
        cls_request_idv_dict = self._get_cls_request_idv_dict(data_dictionary, vehicle)
        idv_request = parser_utils.dict_to_xml('IDV', cls_request_idv_dict)
        idv_resp = self.get_client(data_dictionary['quoteId']).service.getIDV(idv_request)
        xml_string = str(idv_resp)
        root = etree.fromstring(xml_string)
        idv_resp_list = parser_utils.etree_to_dict(root)['IDV']
        idv_resp_dict = {}
        for item in idv_resp_list:
            for key in item.keys():
                if key is not 'text':
                    idv_resp_dict[key] = item['text']
        return map((lambda e: idv_resp_dict[e]), ['idv_amount_min', 'idv_amount', 'idv_amount_max', 'exshowroomPrice'])

    def _get_cls_request_idv_dict(self, data, vehicle):
        return {
            'policy_start_date': data['pcv_policystartdate'],
            'vehicle_class_cd': self.Configuration.VEHICLE_CLASS_CODE,
            'RTOLocationCode': data['RegistrationCity'],
            'vehiclemodelcode': vehicle.model_code,
            'manufacturer_code': vehicle.make_code,
            'purchaseregndate': '%s/%s/%s' % (
                data['RegistrationDay'], data['RegistrationMonth'], data['RegistrationYear']),
            'manufacturingyear': data['RegistrationYear'],
        }

    def _get_required_params_for_premium_calculation(self, data):
        vehicle = Vehicle.objects.filter(insurer__slug=INSURER_SLUG).get(id=data['VehicleId'])
        request_data = data.pop('original_data')  # temp measure
        min_idv, idv, max_idv, ex_showroom_price = self._get_idv_response(data, vehicle)
        data['idv'] = max(min(float(data['idv']), float(max_idv)), float(min_idv))
        data['min_idv'] = min_idv
        data['max_idv'] = max_idv
        data['ex_showroom_price'] = ex_showroom_price
        return self._get_premium_pcv_dict(data, vehicle, request_data)

    def _get_premium_pcv_dict(self, data_dictionary, vehicle, request_data):
        policy_start_date = datetime.datetime.now()
        registration_date = '%s/%s/%s' % (
            data_dictionary['RegistrationDay'], data_dictionary['RegistrationMonth'],
            data_dictionary['RegistrationYear'])
        # -----------------------------------------GetPlanTypes--------------------------------------#
        available_plans = self.get_client(data_dictionary['quoteId']).service.GetPlanTypes(
            self.Configuration.AGENT_CODE,  # AgentCode
            'NEW BUSINESS' if data_dictionary['isNewCar'] == '1' else 'ROLLOVER',  # BusinessType
            vehicle.model_code,  # VehicleModelCode
            data_dictionary['RegistrationCity'],  # RTOLocationCode
            policy_start_date.strftime("%d/%m/%Y"),  # PolicyStartDate
            registration_date,  # FirstRegistrationDate
        )
        if available_plans:
            user_selected_plans = [e.replace('addon_', '') for e in request_data.keys() if
                                   (('addon_' in e) and (request_data[e] == '1'))]
            txt_plan_type = insurer_utils.get_insurer_plans(custom_dictionaries.COVERFOX_MAP,
                                                            available_plans.split(','), user_selected_plans)
        else:
            txt_plan_type = ''
        # -----------------------------------------GetPlanTypes---------------------------------------#
        pcv_dict = {
            'agentcode': self.Configuration.AGENT_CODE,
            'basedon_IDV_ExShowRoom': '1',  # TODO: Send 2 for Exshowroom and 1 for IDV
            'typeofbusiness': 'New Business',
            'occupationtype': '0',
            'custage': 'Up to 35',  # TODO: Customer age (Up to 35, 35 to 45, More Than 45)
            'policystartdate': policy_start_date.strftime("%d/%m/%Y"),
            'policyenddate': (
                putils.add_years(policy_start_date, 1) + datetime.timedelta(days=-1)).strftime(
                "%d/%m/%Y"),
            'rtolocationcode': data_dictionary['RegistrationCity'],
            'vehiclemodelcode': vehicle.model_code,
            'manufacturer_code': vehicle.make_code,
            'purchaseregndate': registration_date,
            'exshowroomprice': data_dictionary['ex_showroom_price'],
            'Sum_Insured': data_dictionary['idv'],
            'electicalacc': '0',
            'nonelecticalacc': '0',
            'nooflldrivers': data_dictionary['extra_isLegalLiability'],
            'paiddriversi': '0',
            'noofemployees': '0',
            'unnamedsi': data_dictionary['extra_paPassenger'],
            'lpg_cngkit': '0',
            'IsPreviousClaim': '0',
            'previousdiscount': '0',
            'prepolstartdate': None,
            'prepolicyenddate': None,
            'txt_cust_type': 'I',
            'num_is_emr_asst_cvr': '0',
            'is_pa_cover_owner_driver': '1',
            'num_is_zero_dept': '0',
            'manufacturingyear': data_dictionary['ManufacturingYear'],
            'txt_plan_type': txt_plan_type,
            # 'page_calling' : 'page_calling',
            'num_is_rti': request_data['addon_isInvoiceCover'] if (txt_plan_type in ['Platinum', 'Titanium']) else '0',
        }
        if data_dictionary['isNewCar'] == '1':
            pcv_dict['purchaseregndate'] = pcv_dict['policystartdate']

        insurer_utils.update_complex_dict(pcv_dict, data_dictionary, 'pcv_')
        return pcv_dict

    ### Method to convert hdfc-ergo premium response to the required responser.
    def convert_premium_response(self, coveragePremiumDetails, data_dictionary):
        cng_kit_premium = sum(
            map((lambda e: float(coveragePremiumDetails[e])), ['NUM_LPG_CNGKIT_OD_PREM', 'NUM_LPG_CNGKIT_TP_PREM']))
        discount_and_whatnot = float(coveragePremiumDetails['NUM_OTHER_DISCOUNT_PREM'])
        basic_covers = [
            {'id': 'cngKit', 'default': '1', 'premium': round(cng_kit_premium, 2), },
            {'id': 'basicOd', 'default': '1', 'premium': float(coveragePremiumDetails['NUM_BASIC_OD_PREMIUM']), },
            {'id': 'paOwnerDriver', 'default': '1',
             'premium': float(coveragePremiumDetails['NUM_PA_COVER_OWNER_DRVR']), },
            {'id': 'paPaidDriver', 'default': '1', 'premium': float(coveragePremiumDetails['NUM_PA_PAID_DRVR_PREM']), },
            {'id': 'legalLiabilityDriver', 'default': '0',
             'premium': float(coveragePremiumDetails['NUM_LL_PAID_DRIVER']), },
            {'id': 'basicTp', 'default': '1', 'premium': float(coveragePremiumDetails['NUM_TP_RATE']), },
            {'id': 'paPassenger', 'default': '1', 'premium': float(coveragePremiumDetails['NUM_UNNAMED_PA_PREM']), },
            {'id': 'legalLiabilityEmployee', 'default': '1',
             'premium': float(coveragePremiumDetails['NUM_EMP_OF_INSRD_PREM']), },
            {'id': 'idvElectrical', 'default': '1', 'premium': float(coveragePremiumDetails['NUM_ELEC_ACC_PREM']), },
            {'id': 'idvNonElectrical', 'default': '1',
             'premium': float(coveragePremiumDetails['NUM_NON_ELEC_ACC_PREM']), },
        ]
        addon_covers = [
            {'id': 'isDepreciationWaiver', 'default': '0',
             'premium': float(coveragePremiumDetails['NUM_ZERO_DEPT_PREM']), },
            {'id': 'is247RoadsideAssistance', 'default': '0',
             'premium': float(coveragePremiumDetails['NUM_EMR_ASST_PREM']), },
            {'id': 'isNcbProtection', 'default': '0', 'premium': float(coveragePremiumDetails['NUM_NCB_PROT_PREM']), },
            {'id': 'isEngineProtector', 'default': '0',
             'premium': float(coveragePremiumDetails['NUM_ENG_GRBX_PREM']), },
            {'id': 'isInvoiceCover', 'default': '0', 'premium': float(coveragePremiumDetails['NUM_RTI_PREM']), },
            {'id': 'isConsumables', 'default': '0',
             'premium': float(coveragePremiumDetails['NUM_COST_CONSUMABLE_PREM']), },
            {'id': 'isGarageCash', 'default': '0', 'premium': float(coveragePremiumDetails['NUM_LOSS_USE_PREM']), },
        ]
        discounts = [
            {'id': 'ncb', 'default': '1', 'premium': -1 * float(coveragePremiumDetails['NUM_NCB_PREM']), },
            {'id': 'odDiscount', 'default': '1', 'premium': -1 * round(discount_and_whatnot, 2), },
        ]
        special_discounts = [
            {'id': 'isAgeDiscount', 'default': '1',
             'premium': -1 * float(coveragePremiumDetails['NUM_AGE_DISC_PREM']), },
            {'id': 'isOccupationDiscount', 'default': '1',
             'premium': -1 * float(coveragePremiumDetails['NUM_OCCUPATION_PREM']), },
        ]
        vehicle = Vehicle.objects.filter(insurer__slug=INSURER_SLUG).get(id=data_dictionary['VehicleId'])

        pa_passenger_dict = insurer_utils.get_pa_passenger_dict(vehicle.vehicle_type, vehicle.seating_capacity)
        pa_passenger_dict.pop(10000)

        result_data = {
                   'insurerId': int(INSURER_ID),
                   'insurerSlug': INSURER_SLUG,
                   'insurerName': INSURER_NAME,
                   'isInstantPolicy': True,
                   'minIDV': int(float(data_dictionary['min_idv'])),
                   'maxIDV': int(float(data_dictionary['max_idv'])),
                   'calculatedAtIDV': int(float(data_dictionary['idv'])),
                   'serviceTaxRate': 0.145,
                   'premiumBreakup': {
                       'serviceTax': round(float(coveragePremiumDetails['NUM_SERVICE_TAX']), 2),
                       'basicCovers': insurer_utils.cpr_update([e for e in basic_covers if e['premium']]),
                       'addonCovers': insurer_utils.cpr_update([e for e in addon_covers if e['premium']]),
                       'discounts': insurer_utils.cpr_update([e for e in discounts if e['premium']]),
                       'specialDiscounts': insurer_utils.cpr_update([e for e in special_discounts if e['premium']]),
                       'dependentCovers': self.parent.get_dependent_addons(parameters=None),
                       'paPassengerAmounts': pa_passenger_dict,
                   },
               }, coveragePremiumDetails
        return {'result': result_data, 'success': True}


class ProposalGenerator(object):
    def __init__(self, configuration):
        self.Configuration = configuration

    def get_client(self, name=None):
        if not hasattr(self, 'client'):
            setattr(self, 'client', Client(self.Configuration.PROPOSAL_WSDL))
        self.client.set_options(plugins=[suds_logger.set_name(name)])
        return self.client

    def get_proposal(self, transaction):
        proposal_request_dict = transaction.raw['policy_details']
        proposal_request = parser_utils.dict_to_xml('xmlmotorpolicy', proposal_request_dict)
        transaction.proposal_request = proposal_request
        transaction.save()
        if self.Configuration.VEHICLE_TYPE == 'Twowheeler':
            resp = self.get_client(transaction.transaction_id).service.GenerateTWTransNo(proposal_request)
        else:
            resp = self.get_client(transaction.transaction_id).service.xmlstring(proposal_request)

        xml_string = str(resp)
        transaction.proposal_response = xml_string
        transaction.save()
        root = etree.fromstring(xml_string)
        resp_list = parser_utils.etree_to_dict(root)['WsResult'][0]['WsResultSet']
        resp_dict = {}
        for item in resp_list:
            for key in item.keys():
                if key is not 'text':
                    resp_dict[key] = item['text']

        proposal_success = (resp_dict['WsStatus'] == '0')
        if not proposal_success:
            # Proposal failure response mail
            mail_utils.send_mail_for_proposal_failure(transaction, xml_string)

        return proposal_success, resp_dict['WsMessage']

    def _get_proposal_motor_policy_dict(self, data_dictionary):
        motor_policy_dict = {
            'AgentCode': self.Configuration.AGENT_CODE,
            'Type_of_Business': 'New Business',
            'Policy_Startdate': '04/04/2014',
            'Policy_Enddate': '03/04/2015',
            'Occupation_Type': '0',  # TODO
            'CustAge': 'Up to 35',  # TODO
            'Policy_Type': 'c',
            'Year_of_Manufacture': '01/01/2014',
            'Registration_Citycode': '10416',
            'Registration_City': 'PUNE',
            'Manufacturer_Code': '27',
            'Manufacture_Name': 'FIAT INDIA PVT. LTD.',
            'Vehicle_Modelcode': '12474',
            'Vehicle_Model': 'LINEA',
            'Purchase_Regndate': '04/04/2014',
            'Vehicle_Regno': 'NEW',
            'Engine_No': '5756756fgt',
            'Chassis_No': 'A4564546',
            'Fuel_Type': 'Petrol',
            'Vehicle_Ownedby': 'I',
            'Name_Financial_Institution': None,
            'Ex-showroom_Price': '710570',
            'Sum_Insured': '675042',
            'Electical_Acc': '0',
            'NonElectical_Acc': '0',
            'LPG-CNG_Kit': None,
            'No_of_LLdrivers': '0',
            'Paiddriver_Si': '0',
            'No_of_Employees': '0',
            'Unnamed_Si': '0',
            'NCB_ExpiringPolicy': '0',
            'NCB_RenewalPolicy': '0',
            'Total_Premium': '17953',
            'Service_Tax': '2219',
            'Total_Amoutpayable': '20172',
            'PrePolicy_Startdate': None,
            'PrePolicy_Enddate': None,
            'PreInsurerCode': None,
            'PrePolicy_Number': None,
            'IsPrevious_Claim': '0',
            'First_Name': 'gdfg',
            'Last_Name': 'dfg',
            'Date_of_Birth': '01/02/1987',
            'Gender': 'Male',
            'Email_Id': 'fardin.shaikh@hdfcergo.com',
            'Contactno_Office': None,
            'Contactno_Home': None,
            'Contactno_Mobile': '9930021183',
            'Car_Address1': 'dkshfb',
            'Car_Address2': 'jdsakl',
            'Car_Address3': 'dfsnkjl',
            'Car_Citycode': '145',
            'Car_City': 'Bongaigaon',
            'Car_Statecode': '10',
            'Car_State': 'Assam',
            'Car_Pin': '400009',
            'Corres_Address1': 'dkshfb',
            'Corres_Address2': 'jdsakl',
            'Corres_Address3': 'dfsnkjl',
            'Corres_Citycode': '145',
            'Corres_City': 'Bongaigaon',
            'Corres_Statecode': '10',
            'Corres_State': 'Assam',
            'Corres_Pin': '400009',
            'Data1': 'CC',
            'Data2': None,
            'Data3': None,
            'Data4': None,
            'Data5': None,
            'IsEmergency_Cover': '0',
            'Owner_Driver_Nominee_Name': None,
            'Owner_Driver_Nominee_Age': None,
            'Owner_Driver_Nominee_Relationship': None,
            'Owner_Driver_Appointee_Name': '  ',
            'Owner_Driver_Appointee_Relationship': '  ',
            'PAN_Card': 'SAFSS2133S',
            'IsAgeDisc': 'N',
            'is_pa_cover_owner_driver': '1',
            'IsZeroDept_Cover': '0',
            'IsZeroDept_RollOver': '0',
            'BiFuelType': None,
            'PlanType': None,
            'IsRTICover': 0,
        }
        insurer_utils.update_complex_dict(motor_policy_dict, data_dictionary, 'motorPolicy_', {})
        return motor_policy_dict

    def save_policy_details(self, data_dictionary, transaction):
        motor_policy_dict = self._get_proposal_motor_policy_dict(data_dictionary)

        start_date = datetime.datetime.strptime(data_dictionary['motorPolicy_Policy_Startdate'], '%d/%m/%Y')
        end_date = datetime.datetime.strptime(data_dictionary['motorPolicy_Policy_Enddate'], '%d/%m/%Y')

        transaction.policy_start_date = start_date
        transaction.policy_end_date = end_date
        transaction.premium_paid = data_dictionary['motorPolicy_Total_Amoutpayable']
        transaction.raw['policy_details'] = motor_policy_dict
        transaction.save()

        is_successful, transaction_ref_no = self.get_proposal(transaction)
        if is_successful:
            transaction.reference_id = transaction_ref_no
            transaction.save()
        return is_successful


class IntegrationUtils(object):
    def __init__(self, vehicle_type):
        configuration = Configuration(vehicle_type)
        self.Configuration = configuration
        self.PremiumCalculator = PremiumCalculator(self, configuration)
        self.ProposalGenerator = ProposalGenerator(configuration)

    def get_city_list(self, state):
        return custom_dictionaries.STATE_ID_TO_CITY_LIST.get(state, None)

    def convert_premium_request_data_to_required_dictionary(self, vehicle_id, request_data):
        # TODO - should be removed once data coming correctly from front end
        if 'newPolicyStartDate' not in request_data.keys():
            request_data['newPolicyStartDate'] = (datetime.datetime.now() + datetime.timedelta(days=1)).strftime(
                "%d-%m-%Y")

        if request_data['isNewVehicle'] == '1':
            is_new_vehicle = True
        else:
            is_new_vehicle = False

        # New policy start date and end date assignment
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(request_data['newPolicyStartDate'], "%d-%m-%Y")
            new_policy_end_date = putils.add_years(
                new_policy_start_date, 1) - datetime.timedelta(days=1)

        else:
            past_policy_end_date = datetime.datetime.strptime(request_data['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

            # This can happen for twowheeler expired flow
            if new_policy_start_date < datetime.datetime.now():
                new_policy_start_date = datetime.datetime.now() + datetime.timedelta(days=1)

            new_policy_end_date = putils.add_years(
                new_policy_start_date, 1) - datetime.timedelta(days=1)

            # If vehicle's registration date is after previous policy start date then set
            # registration date = previous policy start date
            registration_date = datetime.datetime.strptime(request_data['registrationDate'], '%d-%m-%Y')
            past_policy_start_date = putils.subtract_years(
                past_policy_end_date, 1) + datetime.timedelta(days=1)
            if registration_date > past_policy_start_date:
                request_data['registrationDate'] = past_policy_start_date.strftime('%d-%m-%Y')
                request_data['manufacturingDate'] = past_policy_start_date.strftime('%d-%m-%Y')

        # Vehicle age calculation
        if is_new_vehicle:
            vehicle_age = 0
        else:
            date_of_registration = datetime.datetime.strptime(request_data['registrationDate'], '%d-%m-%Y')
            new_policy_start_date = new_policy_start_date.replace(microsecond=0)
            vehicle_age = insurer_utils.get_age(new_policy_start_date, date_of_registration)

        man_date_split = request_data['manufacturingDate'].split('-')
        manufacturing_year = man_date_split[2]

        reg_date_split = request_data['registrationDate'].split('-')
        registration_year = reg_date_split[2]
        registration_month = reg_date_split[1]
        registration_day = reg_date_split[0]

        split_registration_no = request_data.get('registrationNumber[]')
        master_rto_code = "%s-%s" % (split_registration_no[0], split_registration_no[1])

        rto_city_code = RTOMaster.objects.get(rto_code=master_rto_code).insurer_rtos.get(insurer__slug=INSURER_SLUG,
                                                                                         vehicle_type=self.Configuration.VEHICLE_TYPE).rto_code

        addons = {'addon_isDepreciationWaiver': 'isDepreciationWaiver',
                  'addon_is247RoadsideAssistance': 'is247RoadsideAssistance'}

        vehicle_model = VehicleMaster.objects.get(id=request_data['vehicleId']).model

        addon_dict = {}
        for key, value in addons.items():
            addon_dict[key] = '0'
            if request_data[key] == '1' and insurer_utils.is_addon_available(value, 'hdfc-ergo', vehicle_model,
                                                                             vehicle_age, master_rto_code):
                addon_dict[key] = '1'

        customer_occupation = request_data.get('extra_user_occupation', None)
        if not customer_occupation:
            customer_occupation = '0'

        data_dictionary = {
            'vehicleAge': vehicle_age,
            'isUsedVehicle': True if request_data['isUsedVehicle'] == '1' else False,
            'isExpired': False if is_new_vehicle else (
                past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now()),
            'VehicleId': vehicle_id,
            'RegistrationCity': rto_city_code,
            'ManufacturingYear': manufacturing_year,
            'RegistrationYear': registration_year,
            'RegistrationMonth': registration_month,
            'RegistrationDay': registration_day,
            'isNewCar': request_data['isNewVehicle'],
            'pcv_electicalacc': request_data['idvElectrical'],
            'pcv_nonelecticalacc': request_data['idvNonElectrical'],
            'pcv_IsPreviousClaim': '1' if request_data['isClaimedLastYear'] == '0' else '0',
            'pcv_previousdiscount': request_data['previousNCB'] if not is_new_vehicle else '0',
            'pcv_num_is_emr_asst_cvr': addon_dict['addon_is247RoadsideAssistance'],
            'pcv_num_is_zero_dept': addon_dict['addon_isDepreciationWaiver'],
            'pcv_policystartdate': new_policy_start_date.strftime("%d/%m/%Y"),
            'pcv_policyenddate': new_policy_end_date.strftime("%d/%m/%Y"),
            'extra_paPassenger': request_data['extra_paPassenger'],
            'extra_isLegalLiability': '1' if request_data['extra_isLegalLiability'] == '1' else '0',
            'pcv_occupationtype': customer_occupation,  # Mapping to be added later
            'original_data': request_data,
        }

        data_dictionary['idv'] = request_data['idv']

        is_cng_fitted = True if request_data['isCNGFitted'] == '1' else False
        cng_kit_value = request_data['cngKitValue']

        data_dictionary['pcv_lpg_cngkit'] = cng_kit_value if is_cng_fitted else '0'

        data_dictionary['pcv_custage'] = 'Up to 35'

        if request_data.get('extra_user_dob', None):
            customer_dob = datetime.datetime.strptime(request_data['extra_user_dob'], '%d-%m-%Y')
            customer_age = insurer_utils.get_age(datetime.datetime.now(), customer_dob)

            if customer_age <= 35:
                data_dictionary['pcv_custage'] = 'Up to 35'
            if customer_age > 35 and customer_age <= 45:
                data_dictionary['pcv_custage'] = '35 to 45'
            if customer_age > 45 and customer_age <= 60:
                data_dictionary['pcv_custage'] = 'More Than 45'

        if not is_new_vehicle:
            past_policy_end_date = datetime.datetime.strptime(request_data['pastPolicyExpiryDate'], "%d-%m-%Y")
            past_policy_start_date = putils.subtract_years(
                past_policy_end_date, 1) + datetime.timedelta(days=1)

            data_dictionary['pcv_prepolstartdate'] = past_policy_start_date.strftime("%d/%m/%Y")
            data_dictionary['pcv_prepolicyenddate'] = past_policy_end_date.strftime("%d/%m/%Y")
            data_dictionary['pcv_typeofbusiness'] = 'Rollover'
        return data_dictionary

    def get_buy_form_details(self, quote_parameters, transaction):
        split_registration_no = quote_parameters.get('registrationNumber[]')
        return {
            'registration_no': 'NEW' if int(quote_parameters['isNewVehicle']) else '-'.join(split_registration_no),
            'rto_location_info': "%s-%s" % (split_registration_no[0], split_registration_no[1]),
            'occupation_list': custom_dictionaries.OCCUPATION_MAPPING,
            'insurers_list': custom_dictionaries.PAST_INSURER_ID_LIST_MASTER,
            'disallowed_insurers': custom_dictionaries.DISALLOWED_PAST_INSURERS,
            'state_list': custom_dictionaries.STATE_ID_TO_STATE_NAME,
            'relationship_list': custom_dictionaries.NOMINEE_RELATIONSHIP_MAPPING.keys(),
            'is_cities_fetch': True,
        }

    def convert_proposal_request_data_to_required_dictionary(self, vehicle, request_data, quote_parameters,
                                                             quote_response, quote_custom_response):
        # Hack for hdfc-ergo (hdfc cannot parse & (&amp;) in xml)
        request_data.update({k: request_data.get(k, '').replace('&', '') for k in [
            'add_house_no', 'add_building_name', 'add_street_name', 'add_landmark', 'reg_add_house_no',
            'reg_add_building_name', 'reg_add_street_name', 'reg_add_landmark', 'nominee_name']})

        idv = quote_custom_response['calculatedAtIDV']

        if request_data['cust_gender'] == 'Male':
            request_data['form-insured-title'] = 'Mr.'
        else:
            if request_data['cust_marital_status'] == '0':
                request_data['form-insured-title'] = 'Ms.'
            else:
                request_data['form-insured-title'] = 'Mrs.'

        is_new_vehicle = False
        if quote_parameters['isNewVehicle'] == '1':
            is_new_vehicle = True

        if is_new_vehicle:
            request_data['vehicle_reg_no'] = 'NEW'

        registration_number = request_data['vehicle_reg_no']
        vehicle_reg_no = registration_number.split('-')
        if not is_new_vehicle:
            if len(vehicle_reg_no[1]) == 1 and vehicle_reg_no[1].isdigit() and vehicle_reg_no[0] in ['GJ']:
                    vehicle_reg_no[1] = vehicle_reg_no[1].rjust(2, '0')
            if len(vehicle_reg_no[3]) < 4:
                vehicle_reg_no[3] = vehicle_reg_no[3].rjust(4, '0')
            registration_number = "-".join(vehicle_reg_no)

        # DL *C fix
        if is_new_vehicle is False and registration_number.split('-')[0].upper() == 'DL' and registration_number.split('-')[1][-1].upper() in ['C', 'S']:
            _nl = registration_number.split('-')
            _nl[1], _nl[2] = _nl[1][:-1], _nl[1][-1]+_nl[2]
            if len(_nl[1]) == 1:
                _nl[1] = '0' + _nl[1]
            registration_number = '-'.join(_nl)

        vehicle_reg_date = datetime.datetime.strptime(quote_parameters['registrationDate'], "%d-%m-%Y")

        customer_occupation = quote_parameters.get('extra_user_occupation', None)
        if not customer_occupation:
            customer_occupation = '0'  # Mapping to be added later

        enable_age_discount = False
        if quote_parameters.get('extra_user_dob', None):
            customer_dob = datetime.datetime.strptime(quote_parameters['extra_user_dob'], '%d-%m-%Y')
            enable_age_discount = True
        else:
            customer_dob = datetime.datetime.strptime(request_data['cust_dob'], "%d-%m-%Y")

        customer_age = insurer_utils.get_age(datetime.datetime.now(), customer_dob)

        age_group = 'Up to 35'
        if customer_age <= 35:
            age_group = 'Up to 35'
        if customer_age > 35 and customer_age <= 45:
            age_group = '35 to 45'
        if customer_age > 45:
            age_group = 'More Than 45'
            if customer_age > 60:
                enable_age_discount = False

        city_id = request_data['add_city']
        state_id = request_data['add_state']
        city_name = self.city_name_from_city_id(state_id, city_id)
        state_name = custom_dictionaries.STATE_ID_TO_STATE_NAME[state_id]

        not_claim = '1' if quote_parameters['isClaimedLastYear'] == '0' else '0'
        pncb, ncb = '0', '0'
        previous_ncb_to_new_ncb = {
            '0': '20',
            '20': '25',
            '25': '35',
            '35': '45',
            '45': '50',
            '50': '50',
        }
        if not is_new_vehicle:
            pncb = quote_parameters['previousNCB']
            if quote_parameters['isClaimedLastYear'] == '0':
                ncb = previous_ncb_to_new_ncb[pncb]
        vehicle_rto = "-".join(quote_parameters['registrationNumber[]'][0:2])
        insurer_rto = RTOMaster.objects.get(rto_code=vehicle_rto).insurer_rtos.get(insurer__slug=INSURER_SLUG,
                                                                                   vehicle_type=self.Configuration.VEHICLE_TYPE)
        rto_city_code = insurer_rto.rto_code
        rto_city_name = insurer_rto.rto_name
        ex_showroom_price = quote_response['ex_showroom_price']

        # Assigning new_policy_start_date and new_policy_end_date
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(quote_parameters['newPolicyStartDate'], "%d-%m-%Y")
            new_policy_end_date = putils.add_years(
                new_policy_start_date, 1) - datetime.timedelta(days=1)
            vehicle_reg_date = datetime.datetime.now().date()
        else:
            past_policy_end_date = datetime.datetime.strptime(quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")
            if (past_policy_end_date - datetime.datetime.now()).days < -90:
                ncb, pncb, not_claim = '0', '0', '0'
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

            # This can happen for twowheeler expired flow
            if new_policy_start_date < datetime.datetime.now():
                new_policy_start_date = datetime.datetime.now() + datetime.timedelta(days=1)

            new_policy_end_date = putils.add_years(
                new_policy_start_date, 1) - datetime.timedelta(days=1)
            # If vehicle's registration date is after previous policy start date then set
            # registration date = previous policy start date
            past_policy_start_date = putils.subtract_years(
                past_policy_end_date, 1) + datetime.timedelta(days=1)
            if vehicle_reg_date > past_policy_start_date:
                vehicle_reg_date = past_policy_start_date
                quote_parameters['manufacturingDate'] = vehicle_reg_date.strftime('%d-%m-%Y')

        man_date_split = quote_parameters['manufacturingDate'].split('-')
        manufacturing_year = man_date_split[2]
        manufacturing_month = man_date_split[1]
        # HOTFIX: HDFC call fails in case of 4 or more consecutive repeated numbers
        # ADDED BY SHOBHIT: 05/01/2015. Remove after HDFC fixes their validations
        request_data['vehicle_engine_no'] = validate_for_hdfc(request_data['vehicle_engine_no'])
        request_data['vehicle_chassis_no'] = validate_for_hdfc(request_data['vehicle_chassis_no'])
        request_data_dictionary = {
            'motorPolicy_Occupation_Type': customer_occupation,
            'motorPolicy_CustAge': age_group,
            'motorPolicy_IsAgeDisc': 'N' if not enable_age_discount or age_group == 'Up to 35' else 'Y',
            'motorPolicy_Policy_Startdate': new_policy_start_date.strftime("%d/%m/%Y"),
            'motorPolicy_Policy_Enddate': new_policy_end_date.strftime("%d/%m/%Y"),
            'motorPolicy_Year_of_Manufacture': "01/%s/%s" % (manufacturing_month, manufacturing_year),
            'motorPolicy_Registration_Citycode': rto_city_code,
            'motorPolicy_Registration_City': rto_city_name,
            'motorPolicy_Manufacturer_Code': vehicle.make_code,
            'motorPolicy_Manufacture_Name': vehicle.make,
            'motorPolicy_Vehicle_Modelcode': vehicle.model_code,
            'motorPolicy_Vehicle_Model': vehicle.model,
            'motorPolicy_Purchase_Regndate': vehicle_reg_date.strftime("%d/%m/%Y"),
            'motorPolicy_Vehicle_Regno': registration_number,
            'motorPolicy_Engine_No': request_data['vehicle_engine_no'],
            'motorPolicy_Chassis_No': request_data['vehicle_chassis_no'],
            'motorPolicy_Ex-showroom_Price': ex_showroom_price,
            'motorPolicy_Sum_Insured': idv,
            'motorPolicy_Electical_Acc': quote_parameters['idvElectrical'],
            'motorPolicy_NonElectical_Acc': quote_parameters['idvNonElectrical'],
            'motorPolicy_LPG-CNG_Kit': None,
            'motorPolicy_NCB_ExpiringPolicy': pncb,
            'motorPolicy_NCB_RenewalPolicy': ncb,
            'motorPolicy_Total_Premium': quote_response['NUM_NET_PREMIUM'],
            'motorPolicy_Service_Tax': quote_response['NUM_SERVICE_TAX'],
            'motorPolicy_Total_Amoutpayable': quote_response['NUM_TOTAL_PREMIUM'],
            'motorPolicy_IsPrevious_Claim': not_claim,
            'motorPolicy_First_Name': request_data['cust_first_name'],
            'motorPolicy_Last_Name': request_data['cust_last_name'],
            'motorPolicy_Date_of_Birth': customer_dob.strftime("%d/%m/%Y"),
            'motorPolicy_Gender': custom_dictionaries.GENDER_MAPPING[request_data['cust_gender']],
            'motorPolicy_Email_Id': request_data['cust_email'],
            'motorPolicy_Contactno_Mobile': request_data['cust_phone'],
            'motorPolicy_Car_Address1': request_data['add_house_no'],
            'motorPolicy_Car_Address2': request_data['add_building_name'],
            'motorPolicy_Car_Address3': request_data['add_street_name'] + ' ' + request_data['add_landmark'],
            'motorPolicy_Car_Citycode': city_id,
            'motorPolicy_Car_City': city_name,
            'motorPolicy_Car_Statecode': state_id,
            'motorPolicy_Car_State': escape(state_name),
            'motorPolicy_Car_Pin': request_data['add_pincode'],
            'motorPolicy_Corres_Address1': request_data['add_house_no'],
            'motorPolicy_Corres_Address2': request_data['add_building_name'],
            'motorPolicy_Corres_Address3': request_data['add_street_name'] + ' ' + request_data['add_landmark'],
            'motorPolicy_Corres_Citycode': city_id,
            'motorPolicy_Corres_City': city_name,
            'motorPolicy_Corres_Statecode': state_id,
            'motorPolicy_Corres_State': escape(state_name),
            'motorPolicy_Corres_Pin': request_data['add_pincode'],
            'motorPolicy_IsEmergency_Cover': quote_response['addon_is247RoadsideAssistance'],
            'motorPolicy_Owner_Driver_Nominee_Name': request_data['nominee_name'],
            'motorPolicy_Owner_Driver_Nominee_Age': request_data['nominee_age'],
            'motorPolicy_Owner_Driver_Nominee_Relationship': custom_dictionaries.NOMINEE_RELATIONSHIP_MAPPING[
                request_data['nominee_relationship']],
            'motorPolicy_Owner_Driver_Appointee_Name': None,
            'motorPolicy_Owner_Driver_Appointee_Relationship': None,
            'motorPolicy_PAN_Card': request_data['cust_pan'],
            'motorPolicy_IsZeroDept_Cover': quote_response[
                'addon_isDepreciationWaiver'] if quote_response.get('NUM_ZERO_DEPT_RATE', '0') != '0' else '0',
            'motorPolicy_IsZeroDept_RollOver': quote_response[
                'addon_isDepreciationWaiver'] if quote_response.get('NUM_ZERO_DEPT_RATE', '0') != '0' else '0',  # TODO
            'motorPolicy_No_of_LLdrivers': '1' if quote_parameters['extra_isLegalLiability'] == '1' else '0',
            'motorPolicy_Unnamed_Si': quote_parameters['extra_paPassenger'],
            'motorPolicy_BiFuelType': None,
            'motorPolicy_PlanType': quote_response['PlanType'],
            'motorPolicy_IsRTICover': quote_response['IsRTICover'],
        }

        if is_new_vehicle:
            request_data_dictionary['motorPolicy_IsPrevious_Claim'] = '0'
        if 'reg_add_state' in request_data and (request_data['reg_add_state'] != request_data['add_state'] or request_data.get('add-same', '1') == '0'):
            reg_city_id = request_data['reg_add_city']
            reg_state_id = request_data['reg_add_state']
            reg_city_name = self.city_name_from_city_id(reg_state_id, reg_city_id)
            reg_state_name = custom_dictionaries.STATE_ID_TO_STATE_NAME[reg_state_id]
            request_data_dictionary['motorPolicy_Car_Address1'] = request_data['reg_add_house_no']
            request_data_dictionary['motorPolicy_Car_Address2'] = request_data['reg_add_building_name']
            request_data_dictionary['motorPolicy_Car_Address3'] = request_data['reg_add_street_name'] + ' ' + \
                                                                  request_data['reg_add_landmark']
            request_data_dictionary['motorPolicy_Car_Citycode'] = reg_city_id
            request_data_dictionary['motorPolicy_Car_City'] = reg_city_name
            request_data_dictionary['motorPolicy_Car_Statecode'] = reg_state_id
            request_data_dictionary['motorPolicy_Car_State'] = escape(reg_state_name)
            request_data_dictionary['motorPolicy_Car_Pin'] = request_data['reg_add_pincode']

        is_cng_fitted = True if quote_parameters['isCNGFitted'] == '1' else False
        cng_kit_value = quote_parameters['cngKitValue']

        if is_cng_fitted:
            request_data_dictionary['motorPolicy_LPG-CNG_Kit'] = cng_kit_value
            request_data_dictionary['motorPolicy_BiFuelType'] = 'CNG'  # TODO

        if not is_new_vehicle:
            past_policy_end_date = datetime.datetime.strptime(quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")
            past_policy_start_date = putils.subtract_years(
                past_policy_end_date, 1) + datetime.timedelta(days=1)

            request_data['motorPolicy_IsPrevious_Claim'] = '1' if quote_parameters['isClaimedLastYear'] == '0' else '0'
            request_data_dictionary['motorPolicy_Type_of_Business'] = 'Rollover'
            request_data_dictionary['motorPolicy_Vehicle_Regno'] = registration_number
            request_data_dictionary['motorPolicy_PrePolicy_Enddate'] = past_policy_end_date.strftime("%d/%m/%Y")
            request_data_dictionary['motorPolicy_PrePolicy_Startdate'] = past_policy_start_date.strftime("%d/%m/%Y")
            past_policy_insurer_code = custom_dictionaries.REV_PASTINSURER_MAP[request_data['past_policy_insurer']]
            # request_data_dictionary['motorPolicy_PreInsurerCode'] = request_data['past_policy_insurer']
            request_data_dictionary['motorPolicy_PreInsurerCode'] = past_policy_insurer_code
            request_data_dictionary['motorPolicy_PrePolicy_Number'] = request_data['past_policy_number']
        return request_data_dictionary

    def update_transaction_details(self, transaction, data):
        is_new_vehicle = data['isNewVehicle'] == '1'
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                data['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)
            # This can happen for twowheeler expired flow
            if new_policy_start_date < datetime.datetime.now():
                new_policy_start_date = datetime.datetime.now() + datetime.timedelta(days=1)

        new_policy_end_date = putils.add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)

        transaction.policy_start_date = new_policy_start_date
        transaction.policy_end_date = new_policy_end_date
        transaction.save()

    def get_payment_data(self, transaction):
        payment_gateway_data = {
            'CustomerID': transaction.reference_id,
            'TxnAmount': str(int(transaction.premium_paid)),
            'AdditionalInfo1': 'NB',
            'AdditionalInfo2': self.Configuration.PRODUCT_CODE,
            'AdditionalInfo3': '1',
            'hdnPayMode': 'CC',
            'UserName': self.get_full_name(first_name=transaction.proposer.first_name.lower(),
                                           middle_name=transaction.proposer.middle_name.lower(),
                                           last_name=transaction.proposer.last_name.lower()),
            'UserMailId': transaction.proposer.email,
            'ProductCd': self.Configuration.PRODUCT_CODE,
            'ProducerCd': "%s-%s" % (self.Configuration.AGENT_CODE, transaction.reference_id),
        }
        transaction.payment_request = payment_gateway_data
        transaction.save()
        suds_logger.set_name(transaction.transaction_id).log(json.dumps(payment_gateway_data))
        return payment_gateway_data, self.Configuration.PAYMENT_URL

    def post_payment(self, payment_response, transaction):
        # PolicyNo=2919100000193900000&Msg=Successful&ProposalNo=MT1011000025
        motor_logger.info(payment_response)
        redirect_url = '/motor/fourwheeler/payment/failure/'
        is_redirect = True

        payment_response = payment_response['GET']
        # {u'Msg': u'Payment transcation was not completed', u'PolicyNo': u'0', u'ProposalNo': u'HI1403000046T'}
        try:
            transaction = Transaction.objects.get(reference_id=payment_response['ProposalNo'])
        except:
            log_error(motor_logger)
        else:
            suds_logger.set_name(transaction.transaction_id).log(json.dumps(payment_response))
            transaction.insured_details_json['payment_response'] = payment_response
            transaction.payment_response = payment_response
            transaction.save()

            if payment_response.get('PolicyNo', None) and payment_response['PolicyNo'] not in ['0', '']:
                transaction.proposal_number = payment_response['ProposalNo']
                transaction.policy_number = payment_response['PolicyNo']
                transaction.payment_done = True
                transaction.payment_on = datetime.datetime.now()
                transaction.mark_complete()
                redirect_url = '/motor/fourwheeler/payment/success/%s/' % transaction.transaction_id
            else:
                transaction.status = 'FAILED'
                transaction.save()
                redirect_url += '%s/' % (transaction.transaction_id)
        finally:
            return is_redirect, redirect_url, '', {}

    def city_name_from_city_id(self, state_id, city_id):
        for city_info in custom_dictionaries.STATE_ID_TO_CITY_LIST[state_id]:
            if city_info['id'] == city_id:
                return city_info['name']

    def get_full_name(self, **kwargs):
        name_list = []
        if 'first_name' in kwargs.keys() and kwargs['first_name'].strip():
            name_list.append(kwargs['first_name'].strip())
            if 'middle_name' in kwargs.keys() and kwargs['middle_name'].strip():
                name_list.append(kwargs['middle_name'].strip())
            if 'last_name' in kwargs.keys() and kwargs['last_name'].strip():
                name_list.append(kwargs['last_name'].strip())
            return ' '.join(name_list)

    def get_dependent_addons(self, parameters):
        return {
            'isGarageCash': ['isDepreciationWaiver', ],
            'isInvoiceCover': ['isDepreciationWaiver', 'isEngineProtector', 'isNcbProtection', ],
            # RTI is available only with Platinum
            'isDepreciationWaiver': [],
            'isEngineProtector': ['isDepreciationWaiver', 'isNcbProtection', ],
            'isNcbProtection': ['isDepreciationWaiver', 'isEngineProtector', ],
            'isConsumables': ['isDepreciationWaiver', 'isEngineProtector', 'isNcbProtection', 'isConsumables', ],
            'type': 'dynamic'
        }

    def get_transaction_from_id(self, transaction_id):
        return Transaction.objects.get(transaction_id=transaction_id)

    def process_expired_policy(self, transaction_id):
        if self.Configuration.VEHICLE_TYPE == 'Twowheeler':
            return True
        return False

    def get_custom_data(self, datatype):
        datamap = {
            'occupations': custom_dictionaries.OCCUPATION_MAPPING,
            # 'insurers': PAST_INSURANCE_ID_LIST,
            'insurers': custom_dictionaries.PAST_INSURER_ID_LIST_MASTER,
            'states': custom_dictionaries.STATE_ID_TO_STATE_NAME,
            'relationships': custom_dictionaries.NOMINEE_RELATIONSHIP_MAPPING.keys(),
        }
        return datamap.get(datatype, None)

    def disable_cache_for_request(self, request_data):
        if (request_data['isNewVehicle'] != '1' and
                    datetime.datetime.strptime(
                        request_data['pastPolicyExpiryDate'], '%d-%m-%Y'
                    ).replace(hour=23, minute=59, second=0) < datetime.datetime.now() and
                    self.Configuration.VEHICLE_TYPE == 'Private Car'):
            return True
        return False
