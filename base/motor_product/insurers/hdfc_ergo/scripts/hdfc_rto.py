import difflib

from motor_product import models as motor_models
from motor_product.scripts.rto import master_rto_data as master_dicts
from motor_product.insurers.hdfc_ergo.scripts import hdfc_rto_data as hdfc_dicts

INSURER_SLUG = 'hdfc-ergo'


def delete_data():
    motor_models.RTOInsurer.objects.filter(insurer__slug=INSURER_SLUG).delete()


def save_data():
    rtos = hdfc_dicts.TWOWHEELER_RTO_CITY_CODE_TO_INFO
    save_rtos(rtos, 'Twowheeler')

    rtos = hdfc_dicts.FOURWHEELER_RTO_CITY_CODE_TO_INFO
    save_rtos(rtos, 'Private Car')


def save_rtos(insurer_rtos, vehicle_type):
    insurer_hdfc = motor_models.Insurer.objects.get(slug=INSURER_SLUG)

    rtos = []
    for city_code, city_info in insurer_rtos.items():
        rto_insurer = motor_models.RTOInsurer()
        rto_insurer.insurer = insurer_hdfc
        rto_insurer.rto_code = city_code
        rto_insurer.rto_name = city_info['City']
        rto_insurer.rto_state = city_info['State']
        rto_insurer.rto_zone = city_info['IRDA_Zone']
        rto_insurer.vehicle_type = vehicle_type
        rto_insurer.master_rto_state = get_insurer_master_state(city_info['State'])
        rto_insurer.raw = city_info
        print "Saving ---- %s" % city_info
        rtos.append(rto_insurer)
    motor_models.RTOInsurer.objects.bulk_create(rtos)


def get_insurer_master_state(state):
    state = state.replace('&', 'and')
    state_dict = dict((k.lower(), v) for k, v in master_dicts.STATE_NAME_TO_STATE_CODE.items())
    state_code = state_dict.get(state.lower(), None)
    if state_code is None:
        print 'No state found for state ' + state
        state_code = ''
    return state_code


def create_map():
    fd = open('motor_product/insurers/hdfc_ergo/scripts/data/hdfc_rto_map.csv', 'r')
    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)
    insurer_rtos = motor_models.RTOInsurer.objects.filter(insurer__slug=INSURER_SLUG)

    mappings = []
    for line in fd.readlines():
        line = line.split('\n')[0]
        rto_master_code, rto_insurer_code, vehicle_type = line.split('|')
        master_rto = motor_models.RTOMaster.objects.get(rto_code=rto_master_code)
        insurer_rto = insurer_rtos.filter(vehicle_type=vehicle_type, rto_code=rto_insurer_code)[0]
        print 'Mapping %s to %s' % (master_rto, insurer_rto)
        if master_rto and insurer_rto:
            mapping = motor_models.RTOMapping()
            mapping.rto_master = master_rto
            mapping.rto_insurer = insurer_rto
            mapping.insurer = insurer
            mapping.is_approved_mapping = True
            mappings.append(mapping)
    fd.close()
    motor_models.RTOMapping.objects.bulk_create(mappings)


def delete_map():
    motor_models.RTOMapping.objects.filter(insurer__slug=INSURER_SLUG).delete()


def get_insurer_state(state):
    if state == 'TG':
        return 'AP'
    elif state == 'OD':
        return 'OR'
    elif state == 'UK':
        return 'UA'
    return state


def create_mappings():
    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)
    master_rto_data = motor_models.RTOMaster.objects.all()
    insurer_rto_data = motor_models.RTOInsurer.objects.filter(insurer=insurer)

    for master_rto in master_rto_data:
        mrto_name = master_rto.rto_name
        mrto_state = get_insurer_state(master_rto.rto_state)

        rtos_in_mrtos_state = insurer_rto_data.filter(master_rto_state=mrto_state)

        if len(rtos_in_mrtos_state) == 0:
            print 'No RTOs found for the state ' + mrto_state

        city_list = []
        for irto in rtos_in_mrtos_state:
            city_list.append(irto.rto_name.lower())

        matches = difflib.get_close_matches(mrto_name.lower(), city_list, cutoff=0.85)

        for match in matches:
            matched_irto = rtos_in_mrtos_state.filter(rto_name=match)[0]
            mapping = motor_models.RTOMapping()
            mapping.rto_master = master_rto
            mapping.rto_insurer = matched_irto
            mapping.insurer = insurer
            print "Matching %s with %s" % (master_rto.rto_name, matched_irto.rto_name)
            mapping.save()


def delete_mappings():
    motor_models.RTOMapping.objects.filter(insurer__slug=INSURER_SLUG).delete()
