from motor_product import models as motor_models
from motor_product import parser_utils

INSURER_SLUG = 'hdfc-ergo'
INSURER_MASTER_PATH = 'motor_product/insurers/hdfc_ergo/scripts/data/hdfc_twowheeler.csv'
MASTER_PATH = 'motor_product/scripts/twowheeler/data/master_twowheeler.csv'

csv_parameters = {
    'Vehicle Model Code': 0,
    'Manufacturer Code': 1,
    'Vehicle Manufacturer': 2,
    'Vehicle Model Name': 3,
    'No Of Wheels': 4,
    'Cubic Capacity': 5,
    'Seating Capacity': 7,
    'Fuel': 16,
    'Segment Type': 17,
    'Variant': 18,
    'Master': 20,
}


parameters = {
    'model_code': 0,
    'make_code': 1,
    'make': 2,
    'model': 3,
    'number_of_wheels': 4,
    'cc': 5,
    'seating_capacity': 7,
    'fuel_type': 16,
    'vehicle_segment': 17,
    'variant': 18,
    'master': 20
}

master_parameters = {
    'make': 0,
    'model': 1,
    'variant': 2,
    'cc': 3,
    'insurer': 8,  # Refer to master csv
}


def convert_fuel_type_literals_into_fuel_type(fuel_type_literal):
    if fuel_type_literal == 'PETROL':
        fuel_type = 'Petrol'
    elif fuel_type_literal == 'DIESEL':
        fuel_type = 'Diesel'
    elif fuel_type_literal == 'CNG':
        fuel_type = 'CNG / Petrol'
    elif fuel_type_literal == 'LPG':
        fuel_type = 'LPG / Petrol'
    elif fuel_type_literal == 'ELECTRIC':
        fuel_type = 'Electricity'
    else:
        fuel_type = 'Not Defined'
    return fuel_type


def save_data():
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(INSURER_MASTER_PATH, parameters)
    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)

    vehicle_objects = []
    for _vehicle in vehicles:
        _vehicle['cc'] = parser_utils.try_parse_into_integer(_vehicle['cc'])
        _vehicle['seating_capacity'] = parser_utils.try_parse_into_integer(_vehicle['seating_capacity'])
        _vehicle['fuel_type'] = convert_fuel_type_literals_into_fuel_type(_vehicle['fuel_type'])
        _vehicle['insurer'] = insurer

        _vehicle['vehicle_type'] = 'Twowheeler'

        del _vehicle['master']

        vehicle = motor_models.Vehicle(**_vehicle)
        print 'Saving vehicle %s' % vehicle.model
        vehicle_objects.append(vehicle)

    motor_models.Vehicle.objects.bulk_create(vehicle_objects)


def delete_data():
    motor_models.Vehicle.objects.filter(
        vehicle_type='Twowheeler',
        insurer__slug=INSURER_SLUG
    ).delete()


def delete_mappings():
    motor_models.VehicleMapping.objects.filter(
        insurer__slug=INSURER_SLUG,
        master_vehicle__vehicle_type='Twowheeler'
    ).delete()


def create_mappings():
    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)
    master_vehicles = parser_utils.read_file_to_return_vehicle_dictionary(MASTER_PATH, master_parameters)
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(INSURER_MASTER_PATH, parameters)

    ivehicles = motor_models.Vehicle.objects.filter(vehicle_type='Twowheeler', insurer__slug=INSURER_SLUG)

    vehicle_map = {}
    for iv in vehicles:
        _vehicle = ivehicles.get(model_code=iv['model_code'])
        vehicle_map[iv['master']] = _vehicle

    mappings = []
    for mv in master_vehicles:
        _master = motor_models.VehicleMaster.objects.get(
            make__name=mv['make'],
            model__name=mv['model'],
            variant=mv['variant'],
            cc=mv['cc']
        )
        mapping = motor_models.VehicleMapping()
        mapping.master_vehicle = _master
        mapping.mapped_vehicle = vehicle_map.get(mv['insurer'], None)
        mapping.insurer = insurer

        if mapping.mapped_vehicle:
            print 'Mapping %s to %s' % (_master, mapping.mapped_vehicle)
            mappings.append(mapping)

    motor_models.VehicleMapping.objects.bulk_create(mappings)
