import base64
import copy
import datetime
import hashlib
import logging
import math
import os
import re
from django.core.files.storage import default_storage
import putils
import requests
from django.conf import settings
from django.core.mail import EmailMessage
from lxml import etree
from celery_app import app
from celery.contrib.methods import task_method
from motor_product import logger_utils, mail_utils, parser_utils
from motor_product.insurers import insurer_utils
from motor_product.insurers.icici_lombard.techprocess import TechProcessUtils
from motor_product.models import Insurer, RTOInsurer, Vehicle, IntegrationStatistics, Transaction
from putils import render_excel, generate_pdf_from_html
from suds.client import Client
from utils import motor_logger
from . import custom_dictionaries

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)


INSURER_SLUG = 'icici-lombard'
try:
    INSURER = Insurer.objects.get(slug=INSURER_SLUG)
    INSURER_ID = INSURER.id
    INSURER_NAME = INSURER.title
except:
    INSURER_ID = 0
    INSURER_NAME = ''

suds_logger = logger_utils.getLogger(INSURER_SLUG)

ICICI_PROCESS_TEAM_MAILIDS = [
    'baljeet@coverfox.com', 'animesh@coverfox.com',
    'mohit.salviya@icicilombard.com', 'pragya.gajwe@icicilombard.com',
    'rupali.varhadi@icicilombard.com', 'shekhar.chavan@coverfox.com',
    'shah.alam@icicilombard.com'
]


class Configuration(object):

    def __init__(self, vehicle_type):
        self.APIID = 'coverfoxadm'
        self.APISECRET = 'kove7f0x@6m'

        self.PROPOSAL_USERNAME = 'biadmin'
        self.PROPOSAL_PASSWORD = 'bipwd@123'
        if settings.PRODUCTION:
            # Production Credentials
            self.DEALID = 'DL-3001/1797638'
            self.PREMIUM_URL = 'https://app6.icicilombard.com/ILESB/Motor.svc/Partners/Premium/PvtCar'
            self.PROPOSAL_WSDL = 'https://app7.icicilombard.com/BIWebService/FileUploader.asmx?wsdl'
        else:
            # UAT Credentials
            self.DEALID = 'DEAL-3001-0206164'
            self.PREMIUM_URL = 'https://cldilbizapp01.cloudapp.net/ILESB/Motor.svc/Partners/Premium/PvtCar'
            self.PROPOSAL_WSDL = 'https://cldilpfapp02.cloudapp.net/BIWebService/FileUploader.asmx?wsdl'


def _convert_tree_to_list(dictionary, tag):
    items_list = []
    for key, value in dictionary.items():
        if type(value) is dict:
            sub_list = _convert_tree_to_list(value, key)
            items_list.extend(sub_list)
        elif type(value) is str:
            if not re.match(r'^@', key):
                items_list.append({'id': tag, 'premium': value})
    return items_list


class PremiumCalculator(object):

    def __init__(self, configuration):
        self.Configuration = configuration

    def get_exshowroom_price(self, vehicle, rto):
        rto_location_id = int(rto.raw.split('|')[1])  # Refer the rto raw_data
        exrp_index = custom_dictionaries.RTO_LOCATION_ID_INORDER.index(rto_location_id)
        exshowroomprice = vehicle.raw_data.split('|')[-1470:][exrp_index]
        return exshowroomprice

    def get_premium(self, data_dictionary):
        if data_dictionary['isUsedVehicle'] == '0':
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_NEW_VEHICLE_RESTRICTED}
        vehicle = Vehicle.objects.get(id=data_dictionary['vehicleId'])

        master_rto_code = "-".join(
            data_dictionary['registrationNumber[]'][0:2])
        rto_code = custom_dictionaries.RTO_CODE_TO_ID[master_rto_code]
        rto = RTOInsurer.objects.get(
            insurer__slug=INSURER_SLUG, vehicle_type=vehicle.vehicle_type, rto_code=rto_code)

        exshowroomprice = float(self.get_exshowroom_price(vehicle, rto))
        calculated_showroom_price = int(
            float(data_dictionary['eFraction']) * exshowroomprice)

        request_xml = self._get_premium_request_data(
            data_dictionary, vehicle, rto, calculated_showroom_price)

        suds_logger.set_name(data_dictionary['quoteId']).log(request_xml)

        authentication_header = base64.b64encode(self.Configuration.APIID + '|' + hashlib.sha1(
            self.Configuration.APISECRET).hexdigest() + '|' + datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S'))
        headers = {
            'Content-Type': 'application/xml; charset=utf-8',
            'ILAuthorization': authentication_header
        }
        response = requests.post(
            self.Configuration.PREMIUM_URL, data=request_xml, headers=headers, verify=False)
        suds_logger.set_name(data_dictionary['quoteId']).log(response.content)

        response_xml = etree.fromstring(response.content)
        response_dict = parser_utils.complex_etree_to_dict(response_xml)

        response_data = response_dict['PrivateCarPremiumResponse'][
            'PremiumPrivateCarPackage']

        if data_dictionary.get('requestIdv', None):
            idv = float(response_data['IDV'])
            min_idv = math.ceil(idv * 0.94)
            max_idv = math.floor(idv * 1.2)

            data_dictionary['idv'] = float(data_dictionary['requestIdv'])
            del data_dictionary['requestIdv']

            request_idv = float(data_dictionary['idv'])
            if request_idv < float(min_idv):
                data_dictionary['idv'] = min_idv
            elif request_idv > float(max_idv):
                data_dictionary['idv'] = max_idv

            request_idv = data_dictionary['idv']

            data_dictionary['eFraction'] = (request_idv / idv)
            data_dictionary['minIdv'] = min_idv
            data_dictionary['maxIdv'] = max_idv
            return self.get_premium(data_dictionary)

        net_premium = int(response_data['PremiumSchedule'][
                          'NetPremium']['#text'])
        total_premium = int(response_data['PremiumSchedule'][
                            'TotalPremium']['#text'])
        total_od_premium = int(response_data['PremiumSchedule'][
                               'PackagePremium']['OwnDamagePremium']['@total'])
        total_liability_premium = int(response_data['PremiumSchedule'][
                                      'PackagePremium']['LiabilityPremium']['@total'])
        total_tp_premium = int(response_data['PremiumSchedule']['PackagePremium'][
                               'LiabilityPremium']['TPPremium']['@total'])
        total_pa_premium = int(response_data['PremiumSchedule']['PackagePremium'][
                               'LiabilityPremium']['PALiability']['@total'])
        service_tax = int(response_data['PremiumSchedule']['Tax']['#text'])
        total_package_premium = int(response_data['PremiumSchedule'][
                                    'PackagePremium']['@total'])

        # TODO
        antitheft_mismatch = (-1) * (total_package_premium - net_premium)

        quote_raw_data = {
            'total_package_premium': total_package_premium,
            'total_od_premium': total_od_premium,
            'total_tp_premium': total_tp_premium,
            'total_pa_premium': total_pa_premium,
            'total_liability_premium': total_liability_premium,
            'net_premium': net_premium,
            'service_tax': service_tax,
            'total_premium': total_premium,
            'exshowroomprice': exshowroomprice,
        }

        premium_breakup = response_data['PremiumSchedule']['PackagePremium']
        all_covers = _convert_tree_to_list(premium_breakup, 'breakup')

        basic_covers = []
        addon_covers = []
        discounts = []
        special_discounts = []

        basic_covers_mapping = {
            'BasicPremium': {
                'id': 'basicOd',
                'default': '1',
            },
            'TPPD': {
                'id': 'basicTp',
                'default': '1',
            },
            'CompulsoryPACoverforOwnerDriver': {
                'id': 'paOwnerDriver',
                'default': '1',
            },
            'ElectricalAccessoriesPremium': {
                'id': 'idvElectrical',
                'default': '1',
            },
            'Non-electricalAccessoriesPremium': {
                'id': 'idvNonElectrical',
                'default': '1',
            },
            'Bi-fuelKitPremium': {
                'id': 'cngKit',
                'default': '1',
            },
            'LiabilityForBi-FuelKit': {
                'id': 'cngKitTp',
                'default': '1',
            },
            'LegalLiabilitytoPaidEmployee': {
                'id': 'legalLiabilityEmployee',
                'default': '0',
            },
            'LegalLiabilityToPaidDriver': {
                'id': 'legalLiabilityDriver',
                'default': '0',
            },
            'PAforUnnamedPassenger': {
                'id': 'paPassenger',
                'default': '1',
            },
            'BreakinLoading': {
                'id': 'odExtra',
                'default': '1',
            },
            'OtherLoading': {
                'id': 'odExtra',
                'default': '1',
            }
        }

        addon_covers_mapping = {
            'FiberGlassTankPremium': {
                'id': 'repairGlassFiberPlastic',
                'default': '0',
            },
            'ZeroDepreciation': {
                'id': 'isDepreciationWaiver',
                'default': '0',
            },
            'RoadSideAssistancePremium': {
                'id': 'is247RoadsideAssistance',
                'default': '0',
            },
            'ReturnToInvoice': {
                'id': 'isInvoiceCover',
                'default': '0',
            },
            'NoClaimBonusProtection': {
                'id': 'isNcbProtection',
                'default': '0',
            },
            'Consumables': {
                'id': 'isCostOfConsumables',
                'default': '0',
            },
            'GarageCash': {
                'id': 'isGarageCash',
                'default': '0',
            },
            'GeographicExtensionPremium': {
                'id': 'isGeographicalExtension',
                'default': '0',
            },
        }

        discounts_mapping = {
            'DeductforNCB': {
                'id': 'ncb',
                'default': '1',
            },
        }

        special_discounts_mapping = {
            'VoluntaryDiscount': {
                'id': 'voluntaryDeductible',
                'default': '1',
            },
            'Anti-theftDiscount': {
                'id': 'isAntiTheftFitted',
                'default': '1',
            },
            'AutomobileMembershipDiscount': {
                'id': 'isMemberOfAutoAssociation',
                'default': '1',
            },
            'HandicappedDiscount': {
                'id': 'isHandicappedDiscount',
                'default': '1',
            },
        }

        for cover in all_covers:
            if float(cover['premium']) == 0:
                continue

            mf = 1
            if cover['id'] in basic_covers_mapping.keys():
                cv_mapping = basic_covers_mapping
                cv = basic_covers
            elif cover['id'] in addon_covers_mapping.keys():
                cv_mapping = addon_covers_mapping
                cv = addon_covers
            elif cover['id'] in discounts_mapping.keys():
                cv_mapping = discounts_mapping
                cv = discounts
                mf = -1
            elif cover['id'] in special_discounts_mapping.keys():
                cv_mapping = special_discounts_mapping
                cv = special_discounts
                mf = -1
            else:
                motor_logger.info('Cover id not found: ' + cover['id'])
                continue

            custom_cover = cv_mapping[cover['id']]
            custom_cover['premium'] = mf * float(cover['premium'])

            # Hack for Anti-theft
            if custom_cover['id'] == 'isAntiTheftFitted':
                custom_cover['premium'] = custom_cover[
                    'premium'] + antitheft_mismatch

            cv.append(copy.deepcopy(custom_cover))

        result_data = {
            'insurerId': INSURER_ID,
            'insurerSlug': INSURER_SLUG,
            'insurerName': INSURER_NAME,
            'isInstantPolicy': True,
            'NCB': 0,  # Only for used vehicle as of now
            'minIDV': int(math.ceil(data_dictionary['minIdv'])),
            'maxIDV': int(math.floor(data_dictionary['maxIdv'])),
            'calculatedAtIDV': int(math.floor(data_dictionary['idv'])),
            'serviceTaxRate': 0.145,
            'premiumBreakup': {
                'serviceTax': round(service_tax, 2),
                'basicCovers': insurer_utils.cpr_update(basic_covers),
                'addonCovers': insurer_utils.cpr_update(addon_covers),
                'discounts': insurer_utils.cpr_update(discounts),
                'specialDiscounts': insurer_utils.cpr_update(special_discounts),
                'paPassengerAmounts': insurer_utils.get_pa_passenger_dict(vehicle.vehicle_type, vehicle.seating_capacity),
            },
        }, quote_raw_data
        return {'result': result_data, 'success': True}

    def _get_premium_request_data(self, data_dictionary, vehicle, rto, exshowroomprice):
        is_new_vehicle = (data_dictionary['isNewVehicle'] == '1')

        is_used_vehicle = (data_dictionary['isUsedVehicle'] == '1')
        if is_used_vehicle:
            is_new_vehicle = False

        # For now
        is_pyp = (not is_new_vehicle) and (not is_used_vehicle)

        is_ncb_certificate = False
        ncb = data_dictionary['previousNCB'] if is_pyp else '0'
        if is_used_vehicle:
            # is_ncb_certificate = (data_dictionary['isNCBCertificate'] == '1')
            ncb = data_dictionary['newNCB'] if is_ncb_certificate else '0'

        policy_type = 'Roll Over'
        if is_new_vehicle:
            policy_type = 'New Business'

        if not is_pyp:
            new_policy_start_date = datetime.datetime.strptime(
                data_dictionary['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data_dictionary['pastPolicyExpiryDate'], "%d-%m-%Y")
            past_policy_start_date = putils.subtract_years(
                past_policy_end_date, 1) + datetime.timedelta(days=1)
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

        new_policy_end_date = putils.add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)
        registration_date = datetime.datetime.strptime(
            data_dictionary['registrationDate'], '%d-%m-%Y')
        manufacturing_year = data_dictionary['manufacturingDate'].split('-')[2]

        cngType = 'NA'
        cngKitValue = '0'
        if data_dictionary['isCNGFitted'] == '1':
            cngKitValue = data_dictionary['cngKitValue']
            if int(data_dictionary['cngKitValue']) > 0:
                cngType = 'CNG'
            else:
                cngType = 'FactoryFittedCNG'

        pa_passenger_si = str(
            int(data_dictionary['extra_paPassenger']) * vehicle.seating_capacity)

        premium_request_data = (
            'PremiumRequestPrivateCar', (
                ('CorelationId', data_dictionary['quoteId']),
                ('DealNo', self.Configuration.DEALID),
                # New Business, Roll Over, Renewal
                ('PolicyType', policy_type),
                ('CustomerType', 'Individual'),  # Individual, Corporate
                # ('IsVehicleNew', is_new_vehicle), # Deprecated
                ('VehicleMakeCode', vehicle.make_code),
                ('VehicleModelCode', vehicle.model_code),
                ('SeatingCapacity', vehicle.seating_capacity),
                ('CarryingCapacity', vehicle.seating_capacity),
                ('CubicCapacity', vehicle.cc),
                ('NoOfWheels', vehicle.number_of_wheels),
                ('ShowroomPriceDeviation', '0'),
                ('PlaceOfRegistrationCode', rto.rto_code),
                ('PlaceOfRegistration', rto.rto_name),
                ('IsInJammuAndKashmir', False),
                ('ExShowroomPrice', exshowroomprice),
                ('YearofManufacture', manufacturing_year),
                ('DateofDeliveryOrRegistration',
                 registration_date.strftime('%Y-%m-%d')),
                ('PolicyStartDate', new_policy_start_date.strftime('%Y-%m-%d')),
                ('PolicyEndDate', new_policy_end_date.strftime('%Y-%m-%d')),
                # NA, CNG, LPG, FactoryFittedCNG, FactoryFittedLPG
                ('GasKitType', cngType),
                ('SumInsuredOfLPGCNGKit', data_dictionary['cngKitValue']),
                ('ValueOfElectricalAccessories',
                 data_dictionary['idvElectrical']),
                ('ValueOfNonElectricalAccessories',
                 data_dictionary['idvNonElectrical']),
                ('NoOfDrivers', data_dictionary['extra_isLegalLiability']),
                ('NoOfEmployees', '0'),
                ('AutoAssociationMembership', (data_dictionary[
                 'extra_isMemberOfAutoAssociation'] == '1')),
                ('AutoAssociationNo', '12345678'),
                ('HasARAIApprovedAntiTheftDevice',
                 (data_dictionary['extra_isAntiTheftFitted'] == '1')),
                ('GeographicAreaExtension', ''),
                ('ZeroDepreciation', ''),
                ('RTI', False),
                ('OnRoadCharges', '0'),
                ('PreviousInsurance', is_pyp),
                ('ExpiringPolicyStartDate', past_policy_start_date.strftime(
                    '%Y-%m-%d') if is_pyp else ''),
                ('ExpiringPolicyEndDate', past_policy_end_date.strftime(
                    '%Y-%m-%d') if is_pyp else ''),
                ('ClaimInExpiringPolicy', 'Yes' if (is_pyp and data_dictionary[
                 'isClaimedLastYear'] == '1') else 'No'),  # Yes, No
                ('PreviousPolicyType', 'Comprehensive Package' if is_pyp else ''),
                ('NCBPercentInPreviousYear', ncb),
                ('DateOfPreviousVehicleSold', ''),
                ('NCBFromEarlierVehicleSold',
                 'YES' if is_ncb_certificate else 'NO'),  # YES, NO
                # ('NCBPercentFromEarlierVehicleSold', '0'), # Deprecated
                ('AmedusId', ''),
                ('TPPDLimit', '0'),
                ('BankId', ''),
                ('PACoverForUnnamedPassenger', pa_passenger_si),
                # Description: Other Discounts, Other Loadings, NA
                ('LoadingOrDiscount', '0', ('Description', 'NA')),
                ('VoluntaryDeductible', data_dictionary[
                 'voluntaryDeductible']),
            )
        )

        request_etree = insurer_utils.convert_tuple_to_xmltree(
            premium_request_data)

        request_xml = etree.tostring(request_etree)
        return request_xml


class ProposalGenerator(object):

    def __init__(self, configuration):
        self.Configuration = configuration

    def _get_cover_premium(self, cid, covers):
        premium = 0
        for cover in covers:
            if cover['id'] == cid:
                premium = abs(int(cover['premium']))
        return str(premium)

    def _get_proposal_details(self, form_details, transaction):
        quote_parameters = transaction.quote.raw_data
        quote_response = transaction.raw['quote_response']
        quote_raw_response = transaction.raw['quote_raw_response']

        # All covers
        covers = copy.deepcopy(quote_response['premiumBreakup']['basicCovers'])
        covers.extend(copy.deepcopy(
            quote_response['premiumBreakup']['addonCovers']))
        covers.extend(copy.deepcopy(
            quote_response['premiumBreakup']['discounts']))
        covers.extend(copy.deepcopy(
            quote_response['premiumBreakup']['specialDiscounts']))

        # Hack for antitheft mismatch
        antitheft_mismatch = int(quote_raw_response[
                                 'total_package_premium']) - int(quote_raw_response['net_premium'])

        is_new_vehicle = (quote_parameters['isNewVehicle'] == '1')

        is_used_vehicle = (quote_parameters['isUsedVehicle'] == '1')
        if is_used_vehicle:
            is_new_vehicle = False

        # For now
        is_pyp = (not is_new_vehicle) and (not is_used_vehicle)

        is_ncb_certificate = False
        ncb = '0' if not is_pyp else ('0' if quote_parameters[
                                      'isClaimedLastYear'] == '1' else custom_dictionaries.PREVIOUS_NCB_TO_NEW_NCB[quote_parameters['previousNCB']])
        if is_used_vehicle:
            # is_ncb_certificate = (quote_parameters['isNCBCertificate'] == '1')
            ncb = quote_parameters['newNCB'] if is_ncb_certificate else '0'

        if not is_pyp:
            new_policy_start_date = datetime.datetime.strptime(
                quote_parameters['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(
                quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")
            past_policy_start_date = putils.subtract_years(
                past_policy_end_date, 1) + datetime.timedelta(days=1)
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

        new_policy_end_date = putils.add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)
        registration_date = datetime.datetime.strptime(
            quote_parameters['registrationDate'], '%d-%m-%Y')
        manufacturing_year = quote_parameters[
            'manufacturingDate'].split('-')[2]

        vehicle = Vehicle.objects.get(
            insurer__slug=INSURER_SLUG, id=form_details['vehicleId'])
        form_details['title'] = 'Mr.' if form_details['cust_gender'] == 'Male' else (
            'Mrs.' if form_details['cust_marital_status'] == '1' else 'Ms.')

        master_rto_code = "-".join(
            quote_parameters['registrationNumber[]'][0:2])
        rto_code = custom_dictionaries.RTO_CODE_TO_ID[master_rto_code]
        rto = RTOInsurer.objects.get(
            insurer__slug=INSURER_SLUG, vehicle_type=vehicle.vehicle_type, rto_code=rto_code)

        cd_account = transaction.quote.raw_data['payment_mode'] == 'CD_ACCOUNT'
        if cd_account:
            payment_id = datetime.datetime.now().strftime('%j%H%M%S')
            payment_mode = 'D'
        else:
            payment_id = transaction.payment_token
            payment_mode = 'G'

        proposal_details = (
            ('LeadBy', 'D'),  # TODO
            ('InsCoverType', 'P'),  # P - Package Policy
            ('InsPolicyType', 'N' if is_new_vehicle else 'R'),  # N - New, R - Renew
            ('ProposalCreatedDate', datetime.datetime.now().strftime('%m/%d/%Y')),
            ('ProposalCreatedTime', datetime.datetime.now().strftime('%H:%M:%S')),
            # 'CFOX' + str(transaction.id)),
            ('TransactionID', datetime.datetime.now().strftime('CF%Y%j%H%M%S')),
            ('VISoFNumber', ''),  # TODO
            ('InsPolicyIssuingDealerCode', ''),  # TODO
            ('InsPolicyEffectiveDate', new_policy_start_date.strftime('%m/%d/%Y')),
            ('InsPolicyEffectiveTime', '00:00:00'),
            ('InsPolicyExpiryDate', new_policy_end_date.strftime('%m/%d/%Y')),
            ('ProposerType', 'I'),
            ('Salutation', form_details['title']),
            ('FirstName', form_details['cust_first_name']),
            ('MiddleName', ''),
            ('LastName', form_details['cust_last_name']),
            ('CompanyName', ''),  # Only for corporate
            ('Gender', 'M' if form_details['cust_gender'] == 'Male' else 'F'),
            ('DateOfBirth', datetime.datetime.strptime(form_details[
             'cust_dob'], '%d-%m-%Y').strftime('%m/%d/%Y')),
            ('Address1', form_details['add_house_no'] +
             ' ' + form_details['add_building_name']),
            ('Address2', form_details[
             'add_street_name'] + ' ' + form_details['add_landmark']),
            ('Address3', form_details['add_city'] + ' ' +
             custom_dictionaries.STATE_CODE_LIST[form_details['add_state']]),
            ('CityCode', ''),  # TODO
            ('StateCode', ''),  # TODO
            ('PINCode', form_details['add_pincode']),
            ('Email', form_details['cust_email']),
            ('ProposerPAN', form_details['cust_pan']),
            ('PAOwnDriverNomName', form_details['nominee_name']),
            ('PAOwnDriverNomGender', ''),
            ('PAOwnDriverNomAge', form_details['nominee_age']),
            ('PAOwnDriverNomReleation', form_details['nominee_relationship']),
            ('PAOwnDriverAppointeeName', ''),
            ('PAOwnDriverAppointeeRelation', ''),
            ('VehicleClass', 'P'),  # P - Private, C - Commercial
            ('VehicleType ', 'PCP'),
            ('VehicleSubClass', ''),  # NA
            ('CVMiscType', ''),  # NA
            ('CarrierType', ''),  # NA
            ('GVW', ''),  # NA
            ('CarryingCapacity', str(vehicle.seating_capacity)),
            ('TariffPAXRange', ''),  # NA
            ('VehicleInvoiceDate', registration_date.strftime('%m/%d/%Y')),
            ('Make', vehicle.make),
            ('ModelCode', vehicle.make_code),
            ('VariantCode', vehicle.model_code),
            ('EngineNo', form_details['vehicle_engine_no']),
            ('ChassisNo', form_details['vehicle_chassis_no']),
            ('CC', str(vehicle.cc)),
            ('NoOfTrailers', '0'),  # NA
            ('TrailerChassisNo', ''),  # NA
            ('TrailerRegistrationNo', ''),  # NA
            ('YearOfManufacture', manufacturing_year),
            ('RegistrationDate', registration_date.strftime('%m/%d/%Y')),
            ('RegistrationNo', form_details['vehicle_reg_no']),
            ('RTOCode', rto.rto_code),
            ('IsBangladeshCovered', '0'),
            ('IsBhutanCovered', '0'),
            ('IsMaldivesCovered', '0'),
            ('IsNepalCovered', '0'),
            ('IsPakistanCovered', '0'),
            ('IsSriLankaCovered', '0'),
            ('GeographicExtnPremium', '0'),
            ('GeographicExtnTPPremium', '0'),
            ('FinancerCode', form_details[
             'car-financier'] if form_details['is_car_financed'] == '1' else ''),
            ('FinancerBranch', ''),  # TODO
            ('AggrementType', 'Hypothecation' if form_details[
             'is_car_financed'] == '1' else ''),
            ('CompDeductibles', ''),
            ('ExShowRoomPrice', int(quote_raw_response['exshowroomprice'])),
            ('VehicleIDV', quote_response['calculatedAtIDV']),
            ('BodyIDV', ''),  # NA
            ('TrailerIDV', ''),  # NA
            ('TotalIDV', quote_response['calculatedAtIDV']),
            ('PremiumCalculatedBy', 'I'),  # TODO
            ('VehiclePremium', quote_raw_response['total_premium']),
            ('ODDiscPer', ''),
            ('TrailerPremium', '0'),  # NA
            ('NonElectricAccIDV', quote_parameters['idvNonElectrical']),
            ('NonElectricAccPremium', self._get_cover_premium(
                'idvNonElectrical', covers)),
            ('ElectricAccIDV', quote_parameters['idvElectrical']),
            ('ElectricAccPremium', self._get_cover_premium('idvElectrical', covers)),
            ('BifuelKitValue', quote_parameters['cngKitValue']),
            ('BifuelKitIDV', quote_parameters['cngKitValue']),
            ('BifuelTPpremium', self._get_cover_premium('cngKitTp', covers)),
            ('BifuelKitPremium', self._get_cover_premium('cngKit', covers)),
            ('IMT23Premium', ''),  # NA
            ('IMT34Premium', ''),  # NA
            ('OverturnCover', ''),  # NA
            ('IMT33Premium', ''),  # NA
            ('BasicODP', self._get_cover_premium('basicOd', covers)),
            ('VoluntaryDeductible', quote_parameters['voluntaryDeductible']),
            ('VoluntaryDisc', self._get_cover_premium(
                'voluntaryDeductible', covers)),
            ('IsAAMembership', quote_parameters[
             'extra_isMemberOfAutoAssociation']),
            ('AAMemNo', form_details['aan_number'] if quote_parameters[
             'extra_isMemberOfAutoAssociation'] == '1' else ''),
            ('AADiscAmount', self._get_cover_premium(
                'isMemberOfAutoAssociation', covers)),
            ('AAExpiryPeriod', new_policy_end_date.strftime('%Y%m') if quote_parameters[
             'extra_isMemberOfAutoAssociation'] == '1' else ''),
            ('IsAntiTheftAttached', quote_parameters[
             'extra_isAntiTheftFitted']),
            ('AntiTheftDiscAmount', int(float(self._get_cover_premium(
                'isAntiTheftFitted', covers)) - antitheft_mismatch)),  # Hack for antitheft
            ('NCBFlag', '0' if ncb == '0' else '1'),
            ('NCBPer', ncb),
            ('NCBAmount', self._get_cover_premium('ncb', covers)),
            ('IsOwnPremises', ''),  # NA
            ('ADDONIsNILDep', '0'),
            ('ADDONNILDepAmt', ''),
            ('ADDONIsAddTowing', '0'),  # NA
            ('ADDONAddTowingAmt', ''),  # NA
            ('ADDONIsEMICover', '0'),  # NA
            ('ADDONEMICoverAmt', ''),  # NA
            ('ADDONIsRTI', '0'),  # NA
            ('ADDONRTIAmt', ''),  # NA
            ('ADDONIsNCBProt', '0'),  # NA
            ('ADDONNCBProtAmt', ''),  # NA
            ('ADDONIsConsumable', '0'),  # NA
            ('ADDONConsumablesAmt', ''),  # NA
            ('ADDONIsEngineProt', '0'),  # NA
            ('ADDONEngineProtAmt', ''),  # NA
            ('ADDONIsPersonalBelonging', '0'),  # NA
            ('ADDONPersonalBelongingAmt', ''),  # NA
            ('ADDONIsCourtesyCar', '0'),
            ('ADDONCourtesyCarAmt', ''),
            ('NetODpremium', quote_raw_response['total_od_premium']),
            ('BasicTPL', self._get_cover_premium('basicTp', covers)),
            ('ExtTPPD', '0'),  # NA
            ('IMT34TP', ''),  # NA
            ('TrailerTP', '0'),  # NA
            ('TotalTP', quote_raw_response['total_tp_premium']),
            ('PACoverPremOwnerDriver', self._get_cover_premium(
                'paOwnerDriver', covers)),
            ('PACoverUnnamedDriver', '0'),
            ('PACoverPillionRider', '0'),
            ('IsPAPaidDriver', '0'),
            ('NoOfPaidDriverPA', ''),
            ('PACoverPremPaidDriver', ''),
            ('PASumInsuredPerEmployee', '0'),
            ('IsPACleaner', '0'),  # NA
            ('NoOfCleanerPA', ''),  # NA
            ('PACoverPremCleaner', ''),  # NA
            ('IsPAConductor', '0'),  # NA
            ('NoOfConductorPA', ''),  # NA
            ('PACoverPremConductor', ''),  # NA
            ('IsPAHelper', '0'),  # NA
            ('NoOfHelperPA', ''),  # NA
            ('PACoverPremHelper', ''),  # NA
            ('PANoOfPerson', str(vehicle.seating_capacity) if int(
                quote_parameters['extra_paPassenger']) > 0 else ''),
            ('PASumInsuredPerUnnamedperson',
             quote_parameters['extra_paPassenger']),
            ('PACoverPremUnnamedPerson',
             self._get_cover_premium('paPassenger', covers)),
            ('PATotalPremium', quote_raw_response['total_pa_premium']),
            ('IsLLPaidDriver', quote_parameters['extra_isLegalLiability']),
            ('NoOfPaidDriverLL', quote_parameters['extra_isLegalLiability']),
            ('LLPaidDrivPremium', self._get_cover_premium(
                'legalLiabilityDriver', covers)),
            ('IsLLCleaner', '0'),  # NA
            ('NoOfCleanerLL', ''),  # NA
            ('LLCleanerPremium', ''),  # NA
            ('IsLLConductor', '0'),  # NA
            ('NoOfConductorLL', ''),  # NA
            ('LLConductorPremium', ''),  # NA
            ('IsLLHelper', '0'),  # NA
            ('NoOfHelperLL', ''),  # NA
            ('LLHelperPremium', ''),  # NA
            ('IsLLOtherEmp', '0'),
            ('LLOtherEmpCount', ''),
            ('LLOtherEmpPremium', ''),  # NA
            ('IsLLUnnamedPass', '0'),
            ('LLUnnamedPassCount', ''),
            ('LLUnnamedPassPremium', ''),
            ('IsLLNFPP', '0'),  # NA
            ('LLNFPPCount', ''),  # NA
            ('LLNFPPPremium', ''),  # NA
            ('TotalLegalLiability', ''),
            ('NetLiabilityPremiumB', quote_raw_response[
             'total_liability_premium']),
            ('TotalPremium', quote_raw_response['net_premium']),
            ('ServiceTax', quote_raw_response['service_tax']),
            ('GrossPremium', quote_raw_response['total_premium']),
            ('IMTCode', 'IM-515683'),  # TODO
            ('PaymentID', payment_id),  # TODO
            ('ProposerPaymentMode', payment_mode),
            ('IsProposalManualApproved', '0'),
            ('PrevPolicyNo', form_details[
             'past_policy_number'] if is_pyp else ''),
            ('FirstIssingDealerCode', ''),
            ('PrevPolicyEffectiveDate', past_policy_start_date.strftime(
                '%m/%d/%Y') if is_pyp else ''),
            ('PrevPolicyExpiryDate', past_policy_end_date.strftime(
                '%m/%d/%Y') if is_pyp else ''),
            ('PrevInsurCompanyCode', form_details[
             'past_policy_insurer'] if is_pyp else ''),
            ('PrevInsurCompanyName', custom_dictionaries.PAST_INSURANCE_ID_LIST[
             form_details['past_policy_insurer']] if is_pyp else ''),
            ('PrevInsurCompanyAdd', ''),
            ('IsPrevPolCopySubmit', ''),
            ('IsNCBCertificateSubmit', ''),
            ('IsCustomerUndertakingSubmit', ''),
            ('SourceType', 'iPartner')
        )

        key_row = ''
        value_row = ''
        excel_header = []
        excel_values = []
        for entry in proposal_details:
            excel_header.append(entry[0])
            excel_values.append(entry[1])
            key_row += entry[0] + '~~'
            value_row += str(entry[1]) + '~~'

        # Extra policy parameters
        extra_policy_parameters = {
            'FinancierName': custom_dictionaries.FINANCIER_LIST[form_details['car-financier']] if form_details['is_car_financed'] == '1' else '',
            'isCNGFitted': (quote_parameters['isCNGFitted'] == '1'),
            'isAntiTheftFitted': (quote_parameters['extra_isAntiTheftFitted'] == '1'),
            'isMemberOfAutoAssociation': (quote_parameters['extra_isMemberOfAutoAssociation'] == '1'),
            'isPaPassenger': (int(quote_parameters['extra_paPassenger']) > 0),
            'isLLPaidDriver': (quote_parameters['extra_isLegalLiability'] == '1'),
            'isVoluntaryDeductible': (int(quote_parameters['voluntaryDeductible']) > 0),
            'BreakinLoading': self._get_cover_premium('odExtra', covers),
            'PackagePremium': quote_raw_response['total_package_premium'],
            'RTOName': rto.rto_name,
            'PolicyInceptionDate': new_policy_start_date.strftime('%d/%m/%Y'),
            'PolicyExpiryDate': new_policy_end_date.strftime('%d/%m/%Y'),
            'PolicyIssueDate': datetime.datetime.now().strftime('%d/%m/%Y'),
            'Model': vehicle.model,
            'IDVSum': (int(quote_response['calculatedAtIDV']) + int(quote_parameters['cngKitValue']) +
                       int(quote_parameters['idvElectrical']) + int(quote_parameters['idvNonElectrical']))
        }

        return proposal_details, extra_policy_parameters, value_row, key_row, excel_header, excel_values

    def save_policy_details(self, data_dictionary, transaction):
        transaction.raw['proposal_data_dict'] = data_dictionary
        transaction.save()
        return True


class IntegrationUtils(object):

    PremiumCalculator = None
    ProposalGenerator = None

    def __init__(self, vehicle_type):
        configuration = Configuration(vehicle_type)
        self.Configuration = configuration
        self.PremiumCalculator = PremiumCalculator(configuration)
        self.ProposalGenerator = ProposalGenerator(configuration)

    def convert_premium_request_data_to_required_dictionary(self, vehicle_id, request_data):
        data = copy.deepcopy(request_data)
        data['masterVehicleId'] = request_data['vehicleId']
        data['vehicleId'] = vehicle_id
        data['requestIdv'] = request_data['idv']
        data['idv'] = '0'
        data['eFraction'] = 1
        return data

    def get_dependent_addons(self, parameters):
        return {'type': 'dynamic'}

    def get_buy_form_details(self, quote_parameters, transaction):
        split_registration_no = quote_parameters.get('registrationNumber[]')

        is_new_vehicle = True if quote_parameters[
            'isNewVehicle'] == '1' else False
        if is_new_vehicle:
            registration_number = 'NEW---'
        else:
            registration_number = '-'.join(split_registration_no)

        master_rto_code = "%s-%s" % (
            split_registration_no[0], split_registration_no[1])

        data = {
            'registration_no': registration_number,
            'rto_location_info': master_rto_code,
            'insurers_list': custom_dictionaries.PAST_INSURANCE_ID_LIST,
            'disallowed_insurers': custom_dictionaries.DISALLOWED_PAST_INSURERS,
            'state_list': custom_dictionaries.STATE_CODE_LIST,
            'relationship_list': custom_dictionaries.NOMINEE_RELATIONSHIP_LIST,
            'financier_list': custom_dictionaries.FINANCIER_LIST,
            'is_cities_fetch': False,
        }
        return data

    def convert_proposal_request_data_to_required_dictionary(self, vehicle,
                                                             request_data, quote_parameters,
                                                             quote_response, quote_custom_response):
        form_details = copy.deepcopy(request_data)
        if quote_parameters['isNewVehicle'] == '1':
            form_details['vehicle_reg_no'] = 'NEW---'

        form_details['vehicleId'] = vehicle.id
        return form_details

    @staticmethod
    def get_transaction_from_id(transaction_id):
        return Transaction.objects.get(transaction_id=transaction_id)

    def update_transaction_details(self, transaction, data):
        is_new_vehicle = data['isNewVehicle'] == '1'
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                data['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

        new_policy_end_date = putils.add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)

        transaction.policy_start_date = new_policy_start_date
        transaction.policy_end_date = new_policy_end_date
        transaction.save()

    def get_payment_data(self, transaction):
        if transaction.quote.raw_data['payment_mode'] == 'CD_ACCOUNT':
            if not settings.PRODUCTION:
                transaction.mark_complete()
                redirect_url = settings.SITE_URL.strip(
                    "/") + '/motor/fourwheeler/payment/success/%s/' % transaction.transaction_id

                return {}, redirect_url

            data, redirect_url = self.generate_policy(transaction)

        else:
            return_url = settings.SITE_URL.strip(
                "/") + '/motor/fourwheeler/insurer/icici-lombard/payment/response/%s/' % transaction.transaction_id
            customer = transaction.proposer
            tkhandle = TechProcessUtils({'scheme_code': 'LOMBARD', 'commission_percent': '0.0'})
            success, payment_url = tkhandle.get_payment_url(
                transaction.id,
                math.ceil(float(transaction.raw['quote_raw_response']['total_premium'])),
                customer.first_name,
                customer.email,
                customer.mobile,
                return_url
            )
            data = {}
            transaction.payment_request = {'success': success, 'payment_url': payment_url}
            transaction.save()
            if not success:
                redirect_url = settings.SITE_URL.strip("/") + '/motor/fourwheeler/product/failure/'
            else:
                redirect_url = payment_url

        return data, redirect_url

    @app.task(filter=task_method)
    def generate_policy(self, transaction):
        data_dictionary = transaction.raw['proposal_data_dict']
        proposal_details, extra_policy_parameters, csv_params, csv_keys, header, values = self.ProposalGenerator._get_proposal_details(
            data_dictionary, transaction)

        client = Client(self.Configuration.PROPOSAL_WSDL)
        client.set_options(
            plugins=[suds_logger.set_name(transaction.transaction_id)])

        auth_headers = client.factory.create('HeaderAuthIn')
        auth_headers.UserID = self.Configuration.PROPOSAL_USERNAME
        auth_headers.UserPassword = self.Configuration.PROPOSAL_PASSWORD
        client.set_options(soapheaders=auth_headers)
        transaction.proposal_request = csv_params
        transaction.save(update_fields=['proposal_request'])
        response = client.service.ConsumeProposalXML(csv_params)
        transaction.proposal_response = response
        transaction.save(update_fields=['proposal_response'])
        response_xml = etree.fromstring(str(response))
        response_dict = parser_utils.complex_etree_to_dict(response_xml)
        response_dict = response_dict['outputData']

        success = (response_dict['strStatus'] == '1')
        if success:
            STATUS_URL = settings.SITE_URL.strip(
                "/") + '/motor/fourwheeler/payment/success/%s/' % (transaction.transaction_id)
            policy_details = (response_dict['strICPOLICYNO'], response_dict['strICPROPOSALNO'])
            transaction.proposal_number = policy_details[1]
            transaction.policy_number = policy_details[0]
            transaction.save(update_fields=['proposal_number', 'policy_number'])

            proposal_details_dict = {}
            for key, value in proposal_details:
                proposal_details_dict[key] = str(value)

            proposal_details_dict.update(extra_policy_parameters)
            proposal_details_dict['PolicyNo'] = transaction.policy_number

            # Assets path
            proposal_details_dict['assets'] = os.path.join(
                settings.MEDIA_ROOT, 'motor_product/motor_policy_templates/icici-lombard')

            transaction.premium_paid = proposal_details_dict['GrossPremium']
            transaction.save(update_fields=['premium_paid'])

            pdf_content = generate_pdf_from_html('motor_product/motor_policy_templates/icici-template.html',
                                                 proposal_details_dict, None)

            cd_account = transaction.quote.raw_data['payment_mode'] == 'CD_ACCOUNT'
            if cd_account:
                PAYMENT_MODE = "CD ACCOUNT"
                transaction.update_policy(pdf_content, send_mail=False)
                transaction.mark_complete()
            else:
                PAYMENT_MODE = "PAYMENT GATEWAY"
                transaction.update_policy(pdf_content)

            mail_data = 'Policy generation for transaction %s (CFOX-%s) with p.no. %s (email: %s) Policy URL: %s'
            policy_url = settings.SITE_URL.strip("/") + transaction.policy_document.url

            mail_utils.send_mail_for_admin({
                'title': 'Policy generation for icici-lombard (%s)' % PAYMENT_MODE,
                'type': 'policy',
                'data': mail_data % (transaction.transaction_id,
                                     transaction.id,
                                     transaction.policy_number,
                                     transaction.proposer.email,
                                     policy_url)
            })

            try:
                filename = os.path.join(
                    'uploads',
                    'iciciedi',
                    'CVFX%s_%s.xls' % (transaction.id, datetime.datetime.now().strftime('%d%b%Y_%H%M%S'))
                )
                index_pol_no = header.index('TransactionID')
                values[index_pol_no] = transaction.policy_number
                render_excel(filename, header, [values, ], write_to_file=True)

                proposer = transaction.proposer
                subject = 'Coverfox - ICICI Lombard policy details for %s' % transaction.policy_number
                body = '''
                Customer Name:    %s %s
                Email ID:         %s
                Policy Number:    %s
                Proposal Number:  %s
                Amount:           %s
                ''' % (proposer.first_name, proposer.last_name,
                       proposer.email, transaction.policy_number,
                       transaction.proposal_number, transaction.premium_paid)

                with default_storage.open(filename, 'rb') as f:
                    content = f.read()
                email = EmailMessage(
                    subject=subject,
                    body=body,
                    from_email="bot@coverfox.com",
                    to=ICICI_PROCESS_TEAM_MAILIDS,
                    attachments=((os.path.basename(filename),
                                  content,
                                  None),)
                )
                email.send()
            except:
                pass

        else:
            STATUS_URL = settings.SITE_URL.strip(
                "/") + '/motor/fourwheeler/product/failure/'
            # Proposal failure response mail
            mail_utils.send_mail_for_proposal_failure(transaction, response)

        return {}, STATUS_URL

    def post_payment(self, response, transaction):
        payment_response = response['POST']
        tkhandle = TechProcessUtils({'scheme_code': 'LOMBARD', 'commission_percent': '0.0'})
        payment_data = tkhandle.get_payment_parameters(payment_response)

        transaction.payment_response = payment_data
        transaction.payment_token = payment_data['tpsl_txn_id']
        transaction.save()

        is_redirect = True
        redirect_url = '/motor/fourwheeler/payment/failure/%s/' % transaction.transaction_id

        if payment_data['txn_msg'].lower() == 'success':
            redirect_url = '/motor/fourwheeler/payment/success/%s/' % transaction.transaction_id
            transaction.payment_done = True
            transaction.payment_on = datetime.datetime.now()
            transaction.premium_paid = payment_data['txn_amt']
            transaction.mark_complete()
            self.generate_policy.delay(transaction)
        else:
            transaction.status = 'FAILED'
            transaction.save()
        return is_redirect, redirect_url, '', {}
