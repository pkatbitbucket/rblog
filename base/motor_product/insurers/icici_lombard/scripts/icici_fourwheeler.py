from motor_product.models import Vehicle, Insurer, VehicleMaster, VehicleMapping
from motor_product import parser_utils

INSURER_SLUG = 'icici-lombard'
INSURER_MASTER_PATH = 'motor_product/insurers/icici_lombard/scripts/data/icici_fourwheeler.csv'
MASTER_PATH = 'motor_product/scripts/fourwheeler/data/master_fourwheeler.csv'

INSURER_PRICING_MASTER_PATH = 'motor_product/insurers/icici_lombard/scripts/data/icici_fourwheeler_srp.csv'

csv_parameters = {
    'VEHICLECLASSCODE': 0,
    'MANUFACTURER': 1,
    'VEHICLEMODELCODE': 2,
    'VEHICLEMODEL': 3,
    'NUMBEROFWHEELS': 4,
    'CUBICCAPACITY': 5,
    'SEATINGCAPACITY': 7,
    'FUELTYPE': 35,
    'CARCATEGORY': 37,
    'Master': 44,
}

srp_parameters = {
    'model_code': 5,
    'exshowroomprice': 8,
    'hub_rto': 2,
}

parameters = {
    'make_code': 0,
    'make': 1,
    'model_code': 2,
    'model': 3,
    'number_of_wheels': 4,
    'cc': 5,
    'seating_capacity': 7,
    'fuel_type': 35,
    'vehicle_segment': 37,
    'master': 44,
}

master_parameters = {
    'make': 0,
    'model': 1,
    'variant': 2,
    'fuel_type': 3,
    'cc': 4,
    'seating_capacity': 5,
    'insurer': 17,  # Refer to master csv
}

FUEL_TYPE_MAP = {
    'Petrol C': 'Petrol',
    'Diesel C': 'Diesel',
    'Electric C': 'Electricity',
}

SEGMENT_TYPE_MAP = {
    'Compact C': 'Compact Cars',
    'Mid C': 'Midsize Cars',
    'Premium C': 'High End Cars',
    'Utility C': 'Multi-utility Vehicles',
}


def get_vehicle_pricing_map():
    pricings = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_PRICING_MASTER_PATH, srp_parameters)

    rto_code_map = {}
    pricing_map = {}
    for price in pricings:
        if not pricing_map.get(price['model_code'], None):
            pricing_map[price['model_code']] = {}

        if not pricing_map[price['model_code']].get(price['hub_rto'], None):
            pricing_map[price['model_code']][
                price['hub_rto']] = price['exshowroomprice']

        rto_code_map[price['hub_rto']] = True

    rto_code_list = map(lambda x: int(x), rto_code_map.keys())
    rto_code_list.sort()
    return rto_code_list, pricing_map


def save_data():
    rto_list, price_map = get_vehicle_pricing_map()

    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_MASTER_PATH, parameters)
    insurer = Insurer.objects.get(slug=INSURER_SLUG)

    vehicle_objects = []
    for _vehicle in vehicles:
        _vehicle['cc'] = parser_utils.try_parse_into_integer(_vehicle['cc'])
        _vehicle['seating_capacity'] = parser_utils.try_parse_into_integer(_vehicle[
                                                                           'seating_capacity'])
        _vehicle['fuel_type'] = FUEL_TYPE_MAP.get(
            _vehicle['fuel_type'], 'Not Defined')
        _vehicle['vehicle_segment'] = SEGMENT_TYPE_MAP.get(
            _vehicle['vehicle_segment'], 'Not Defined')
        _vehicle['insurer'] = insurer

        for rto in rto_list:
            if not price_map.get(_vehicle['model_code'], None):
                print 'Vehicle price not available for %s %s' % (_vehicle['make'], _vehicle['model'])
                break

            price = price_map[_vehicle['model_code']][str(rto)]
            _vehicle['raw_data'] += '|' + price

        del _vehicle['master']

        vehicle = Vehicle(**_vehicle)
        print 'Saving vehicle %s' % vehicle.model
        vehicle_objects.append(vehicle)

    Vehicle.objects.bulk_create(vehicle_objects)


def delete_data():
    Vehicle.objects.filter(vehicle_type='Private Car',
                           insurer__slug=INSURER_SLUG).delete()


def delete_mappings():
    VehicleMapping.objects.filter(
        insurer__slug=INSURER_SLUG, master_vehicle__vehicle_type='Private Car').delete()


def create_mappings():
    insurer = Insurer.objects.get(slug=INSURER_SLUG)
    master_vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        MASTER_PATH, master_parameters)
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_MASTER_PATH, parameters)

    ivehicles = Vehicle.objects.filter(
        vehicle_type='Private Car', insurer__slug=INSURER_SLUG)

    vehicle_map = {}
    for iv in vehicles:
        _vehicle = ivehicles.get(model_code=iv['model_code'])
        vehicle_map[iv['master']] = _vehicle

    mappings = []
    for mv in master_vehicles:
        _master = VehicleMaster.objects.get(make__name=mv['make'], model__name=mv['model'],
                                            variant=mv['variant'], cc=mv['cc'],
                                            fuel_type=mv['fuel_type'],
                                            seating_capacity=mv['seating_capacity'])
        mapping = VehicleMapping()
        mapping.master_vehicle = _master
        mapping.mapped_vehicle = vehicle_map.get(mv['insurer'], None)
        mapping.insurer = insurer

        if mapping.mapped_vehicle:
            print 'Mapping %s to %s' % (_master, mapping.mapped_vehicle)
            mappings.append(mapping)

    VehicleMapping.objects.bulk_create(mappings)
