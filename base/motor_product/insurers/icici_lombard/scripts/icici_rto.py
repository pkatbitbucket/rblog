from motor_product.models import RTOInsurer, RTOMaster, RTOMapping, Insurer
from motor_product import parser_utils

INSURER_SLUG = 'icici-lombard'
INSURER_MASTER_RTOS_PATH = 'motor_product/insurers/icici_lombard/scripts/data/icici_rtos.csv'
MASTER_RTOS_PATH = 'motor_product/scripts/fourwheeler/data/master_fourwheeler.csv'

parameters = {
    'rto_state' : 6,
    'rto_code' : 1,
    'rto_name' : 0,
    'rto_zone' : 4,
}

def save_data():
    rtos = parser_utils.read_file_to_return_vehicle_dictionary(INSURER_MASTER_RTOS_PATH, parameters)
    insurer = Insurer.objects.get(slug = INSURER_SLUG)

    rto_objects = []
    for _rto in rtos:
        _rto['insurer'] = insurer
        _rto['vehicle_type'] = 'Private Car'
        # _rto['master_rto_state'] = '' # Not required for now
        _rto['raw'] = _rto['raw_data']
        del _rto['raw_data']

        rto = RTOInsurer(**_rto)
        print 'Saving rto %s' % rto.rto_name
        rto_objects.append(rto)

    RTOInsurer.objects.bulk_create(rto_objects)

def delete_data():
    RTOInsurer.objects.filter(vehicle_type='Private Car', insurer__slug=INSURER_SLUG).delete()

def delete_map():
    RTOMapping.objects.filter(insurer__slug=INSURER_SLUG).delete()

def create_map():
    pass