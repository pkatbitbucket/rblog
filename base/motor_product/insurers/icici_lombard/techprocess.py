import math
import base64
import hashlib
from Crypto.Cipher import AES
from datetime import datetime
from suds.client import Client
from django.conf import settings


class AESCipher(object):

    def __init__(self, key, iv):
        self.key = key
        self.iv = iv
        self.pad = lambda s: s + (16 - len(s) % 16) * chr(16 - len(s) % 16)
        self.unpad = lambda s: s[:-ord(s[len(s)-1:])]

    def encrypt(self, raw):
        raw = self.pad(raw)
        AES.key_size = 128
        cipher = AES.new(self.key, AES.MODE_CBC, self.iv)
        return base64.b64encode(cipher.encrypt(raw))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        cipher = AES.new(self.key, AES.MODE_CBC, self.iv)
        return self.unpad(cipher.decrypt(enc))


class TechProcessUtils(object):

    TARGET_URL = 'https://www.tpsl-india.in/PaymentGateway/services/TransactionDetailsNew'

    def __init__(self, configuration):
        self.scheme_code = configuration['scheme_code']
        self.commission_percent = configuration['commission_percent']

        if settings.PRODUCTION:
            # PRODUCTION CREDENTIALS #
            self.iv = '8072212186MNRDEI'
            self.encryption_key = '2213468353JPWPCR'
            self.merchant_code = 'L4808'
        else:
            # UAT CREDENTIAL #
            TechProcessUtils.TARGET_URL = 'https://www.tekprocess.co.in/PaymentGateway/services/TransactionDetailsNew'
            self.scheme_code = 'CIBPL'
            self.iv = '9178318165MOHQMI'
            self.encryption_key = '3794438718HXQUVP'
            self.merchant_code = 'T3432'

    def _send_request(self, payload):
        request_string = '|'.join(payload)
        hashed_string = hashlib.sha1(request_string).hexdigest()
        payload_with_sha1 = '%s|hash=%s' % (request_string, hashed_string)

        aes_cipher = AESCipher(self.encryption_key, self.iv)
        data_encrypted = aes_cipher.encrypt(payload_with_sha1)
        data_to_post = '%s|%s~' % (data_encrypted, self.merchant_code)

        client = Client(TechProcessUtils.TARGET_URL + '?wsdl')
        get_token = client.service.getTransactionToken
        get_token.method.location = TechProcessUtils.TARGET_URL

        response = get_token(data_to_post)
        return response

    def get_payment_url(self, identifier, amount, customer_name, email, mobile, return_url):
        payment_amount = '%.2f' % math.ceil(amount)
        request_params = [
            'rqst_type=T',
            'rqst_kit_vrsn=1.0.1',
            'tpsl_clnt_cd=%s' % self.merchant_code,
            'clnt_txn_ref=%s' % identifier,
            'clnt_rqst_meta={itc:email:%s}{email:%s}{mob:%s}{custname:%s}' % (
                email,
                email,
                mobile,
                customer_name,
            ),
            'rqst_amnt=%s' % payment_amount,
            'rqst_crncy=INR',
            'rtrn_url=%s' % return_url,
            's2s_url=%s' % return_url,
            'rqst_rqst_dtls=%s_%s_%s' % (self.scheme_code, payment_amount, self.commission_percent),
            'clnt_dt_tm=%s' % datetime.now().strftime('%d-%m-%Y'),
            'tpsl_bank_cd=NA'  # Previously was 470
        ]
        response = self._send_request(request_params)
        if response.find('ERROR') >= 0:
            return False, None
        return True, response

    def get_payment_parameters(self, payment_response):
        # SAMPLE PAYMENT RESPONSE #
        # POST: {"msg": "VxAYuLzeT117wVCGbpkmY/C1ujpH4lp4FudnwsHFaIlzwnr9XxLbqUX2cTXJRK8xIpNQJtrzkvek\r\nVHV9hyqRKcoYA7voB46jmzgPnxLHkyqMVWRWqdSYOAEBRZhUgroqVSlNTp8ebPJRIvfyzaIIg/Ym\r\nZO2wqR2/Z/Ds690+fmlY2EVAJhLsE+R4bzQ72OUBfqV5NmXfktvms/8n4L2JeIVhUPOR1X3i3fkc\r\nROInqGFU++XFYG2coEOYKhZMgZvTNlqFaWzben0jhAAQFpbDbu8pixziN11CDgePYG1jGGEJdm5p\r\npGhvYpXZu+pKv28Z6+fLKYfTnrn66QV+pg0O/6sAW0Ig3800PBa7iPotWvhOayFf6k90ZUq3ujBC\r\nOeDK7N8TEW5aQ2lu53MUZC3tiD085gstrXcajD3TwXb6pkL/yKS/OAWLZn7MApeBaHgRX0rEzsRg\r\nayES5ONXPLiNmoQqRLTOyjwYWIISmDziVlNi/6EF85SYPvT6+pDSmAx29Gt0x0X/cIUSwl+LEmbH\r\nrHhAz8qMn7QHICnFKei19K1dycjFrcEEpal/I0Ap88DPBw+IbexkI+aqxFK67JUx/A==", "tpsl_mrct_cd": "L4808"}
        # DATA: {'txn_msg': 'failure', 'tpsl_bank_cd': '820', 'txn_status': '0399', 'tpsl_txn_time': '21-05-2015 20:26:51', 'txn_amt': '1.00', 'clnt_txn_ref': '446', 'bal_amt': 'NA', 'clnt_rqst_meta': '{itc:email:amit.siddhu@coverfoxmail.com}{email:amit.siddhu@coverfoxmail.com}{mob:9958469787}{custname:AMIT}', 'tpsl_rfnd_id': 'NA', 'txn_err_msg': 'TPPGE901-Cancel by user in OTP/3DSecure capture Page', 'hash': '8884e1d80d5d2a37557228da8860577f3b1b21be', 'tpsl_txn_id': '157843223', 'rqst_token': 'dbfb0725-c766-4513-a73f-391b49167b9a'}
        aes_cipher = AESCipher(self.encryption_key, self.iv)
        data_decrypted = aes_cipher.decrypt(payment_response['msg'].replace('\\r\\n', ''))
        split_data = data_decrypted.split('|')
        response_data = dict([(item.split('=')[0].strip(), item.split('=')[1].strip()) for item in split_data])
        return response_data

    def confirm_payment(self, payment_response):
        request_params = [
            'rqst_type=S',
            'rqst_kit_vrsn=1.0.1',
            'tpsl_clnt_cd=%s' % self.merchant_code,
            'clnt_txn_ref=%s' % payment_response['clnt_txn_ref'],
            'rqst_amnt=%s' % payment_response['txn_amt'],
            'tpsl_txn_id=%s' % payment_response['tpsl_txn_id'],
            'clnt_dt_tm=%s' % payment_response['tpsl_txn_time']
        ]
        response = self._send_request(request_params)
        split_data = response.split('|')
        response_data = dict([(item.split('=')[0].strip(), item.split('=')[1].strip()) for item in split_data])
        return response_data
