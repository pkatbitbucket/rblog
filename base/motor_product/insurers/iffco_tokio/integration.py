import copy
import datetime
import json
import logging
import json
import math
from motor_product import logger_utils, parser_utils
from motor_product.insurers import insurer_utils
from motor_product.models import Insurer, RTOMaster, Transaction, Vehicle, IntegrationStatistics
from suds.client import Client
from utils import motor_logger, log_error
import putils
from . import custom_dictionaries

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)

INSURER_SLUG = 'iffco-tokio'
try:
    INSURER = Insurer.objects.get(slug=INSURER_SLUG)
    INSURER_ID = INSURER.id
    INSURER_NAME = INSURER.title
except:
    INSURER_ID = 0
    INSURER_NAME = ''

suds_logger = logger_utils.getLogger(INSURER_SLUG)

# ######## TEST CREDENTIALS ########
# IDV_WSDL = 'http://220.227.8.74/portaltest/services/IDVWebService?wsdl'
# PREMIUM_URL = 'http://220.227.8.74/portaltest/services/MotorPremiumWebService?wsdl'
# PROPOSAL_URL = 'http://220.227.8.74/portaltest/MotorServiceReq'


# ######## PRODUCTION CREDENTIALS ########
IDV_WSDL = 'https://www.itgionline.com/ptnrportal/services/IDVWebService?wsdl'
PREMIUM_URL = 'https://www.itgionline.com/ptnrportal/services/MotorPremiumWebService?wsdl'
PROPOSAL_URL = 'https://www.itgionline.com/ptnrportal/MotorServiceReq'


class Configuration(object):
    def __init__(self, vehicle_type):

        self.VEHICLE_TYPE = vehicle_type
        self.RESPONSE_URL = 'http://www.coverfox.com/motor/insurer/iffco-tokio/response/'

        # PRODUCTION CREDENTIALS
        self.PARTNER_BRANCH = 'COVERFOX'
        self.PARTNER_CODE = 'ITGIMOT020'
        self.PARTNER_SUB_BRANCH = 'COVERFOX'

        if vehicle_type == 'Private Car':
            self.POLICY_TYPE = 'PCP'
            self.VEHICLE_AGE_LIMIT = 7

            # TEST CREDENTIALS
            # self.PARTNER_BRANCH = 'COVER_FOX'
            # self.PARTNER_CODE = 'ITGIMOT025'
            # self.PARTNER_SUB_BRANCH = 'COVER_FOX'

            self.URL_TAG = 'fourwheeler'
            self.VALID_COVERAGES = ['IDV Basic', 'PA Owner / Driver', 'Legal Liability to Driver', 'TPPD',
                                    'PA to Passenger']
            self.VALID_COVERAGES_SUMINSURED_MAP = {
                'TPPD': '750000',
            }
        elif vehicle_type == 'Twowheeler':
            self.POLICY_TYPE = 'TWP'
            self.VEHICLE_AGE_LIMIT = 10

            # TEST CREDENTIALS
            # self.PARTNER_BRANCH = 'COVERFOX_TWP'
            # self.PARTNER_CODE = 'ITGIMOT026'
            # self.PARTNER_SUB_BRANCH = 'COVERFOX_TWP'

            self.URL_TAG = 'twowheeler'
            self.VALID_COVERAGES = ['IDV Basic', 'PA Owner / Driver', 'TPPD', 'PA to Passenger']
            self.VALID_COVERAGES_SUMINSURED_MAP = {
                'TPPD': '100000',
            }


class PremiumCalculator(object):
    premium_client = None
    idv_client = None

    def __init__(self, configuration):
        self.Configuration = configuration

    def get_idv_client(self, name=None):
        if self.idv_client == None:
            self.idv_client = Client(url=IDV_WSDL)

        self.idv_client.set_options(plugins=[suds_logger.set_name(name)])
        return self.idv_client

    def get_client(self, name=None):
        if self.premium_client == None:
            self.premium_client = Client(url=PREMIUM_URL)
            self.premium_client.set_options(cache=None)

        self.premium_client.set_options(plugins=[suds_logger.set_name(name)])
        return self.premium_client

    def get_premium(self, data_dictionary):
        if int(data_dictionary['vehicleAge']) >= self.Configuration.VEHICLE_AGE_LIMIT:
            motor_logger.info('Vehicle age limit not valid for this insurer %s.' % INSURER_SLUG)
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_AGE_RESTRICTED}

        if data_dictionary['isExpired']:
            motor_logger.info('Expired policy not allowed for this insurer %s.' % INSURER_SLUG)
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_EXPIRED_POLICY_RESTRICTED}

        if data_dictionary['isUsedVehicle']:
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_USED_VEHICLE_RESTRICTED}

        allowed_previous_ncb = {
            '0': '0',
            '1': '0',
            '2': '20',
            '3': '25',
            '4': '35',
            '5': '45',
            '6': '50'
        }
        if self.Configuration.VEHICLE_TYPE == "Twowheeler":
            vehicle_age_min = int(math.floor(data_dictionary['vehicleAge']))
            if int(math.floor(data_dictionary['vehicleAge'])) > 6:
                allowed_previous_ncb[str(vehicle_age_min)] = '50'
            if  data_dictionary['isNewCar'] == '0':
                if str(data_dictionary['previous_ncb']) > allowed_previous_ncb[str(vehicle_age_min)]:
                    motor_logger.info(" NCB does not lies as per the rule.")
                    return None

        policyHeader, policy, partner = self._get_required_params_for_premium_calculation(data_dictionary)
        idv = data_dictionary['idv']
        min_idv = data_dictionary['min_idv']
        max_idv = data_dictionary['max_idv']

        resp = self.get_client(data_dictionary['quoteId']).service.getMotorPremium(policyHeader, policy, partner)
        resp_dict = insurer_utils.recursive_asdict(resp)
        if 'error' in resp_dict.keys():
            resp_dict['error'] = dict(resp_dict['error'])

        service_tax = resp_dict['serviceTax']
        discount = resp_dict['discountLoadingAmt']

        resp_dict['raw_request_policy'] = policy

        return self.convert_premium_response(resp_dict, data_dictionary, service_tax, discount)

    def _get_idv_response(self, data_dictionary, vehicle):
        cls_request_idv_dict = self._get_cls_request_idv_dict(data_dictionary, vehicle)
        idv_resp = self.get_idv_client(data_dictionary['quoteId']).service.getVehicleIdv(cls_request_idv_dict)
        min_idv = idv_resp.minimumIdvAllowed
        idv = idv_resp.idv
        max_idv = idv_resp.maximumIdvAllowed
        return min_idv, idv, max_idv

    def _get_cls_request_idv_dict(self, data_dictionary, vehicle):
        registration_date = '%s/%s/%s' % (data_dictionary['RegistrationMonth'], data_dictionary['RegistrationDay'],
                                          data_dictionary['RegistrationYear'])  ## MM/DD/YYYY
        current_date_time = datetime.datetime.now()
        inception_date = current_date_time + datetime.timedelta(days=1)

        idv_dict = {
            'dateOfRegistration': registration_date,
            'inceptionDate': inception_date.strftime("%m/%d/%Y"),
            'makeCode': vehicle.make_code,
            'rtoCity': data_dictionary['RegistrationCity'],
        }

        # policy_inceptionDate is of the format '%m/%d/%Y 00:00:00', we just need '%m/%d/%Y'
        idv_dict['inceptionDate'] = data_dictionary['policy_inceptionDate'].split(' ')[0]
        return idv_dict

    def _get_required_params_for_premium_calculation(self, data_dictionary):

        vehicle = Vehicle.objects.filter(insurer__slug=INSURER_SLUG).get(id=data_dictionary['VehicleId'])

        min_idv, idv, max_idv = self._get_idv_response(data_dictionary, vehicle)
        request_idv = float(data_dictionary['idv'])
        if request_idv < float(min_idv):
            data_dictionary['idv'] = min_idv
        elif request_idv > float(max_idv):
            data_dictionary['idv'] = max_idv

        data_dictionary['min_idv'] = min_idv
        data_dictionary['max_idv'] = max_idv

        policyHeader = self._get_premium_policyHeader_dict()
        policy = self._get_premium_policy_dict(data_dictionary, vehicle)
        partner = self._get_premium_partner_dict()

        return (policyHeader, policy, partner)

    def _get_premium_policyHeader_dict(self):
        premium_policyHeader = {
            'messageId': '1234',
        }

        return premium_policyHeader

    def _get_premium_policy_dict(self, data_dictionary, vehicle):
        registration_date = '%s/%s/%s' % (
            data_dictionary['RegistrationMonth'], data_dictionary['RegistrationDay'],
            data_dictionary['RegistrationYear'])
        currentDateTime = datetime.datetime.now()

        premium_policy = {
            'contractType': self.Configuration.POLICY_TYPE,
            'expiryDate': (
                putils.add_years(currentDateTime, 1) - datetime.timedelta(days=1)).strftime(
                "%m/%d/%Y 23:59:00"),
            'inceptionDate': currentDateTime.strftime("%m/%d/%Y 00:00:00"),
            'previousPolicyEndDate': None,
            'vehicle': {
                'aaiExpiryDate': None,
                'aaiNo': None,
                'capacity': vehicle.seating_capacity,
                'engineCpacity': vehicle.cc,
                'grossVehicleWt': None,
                'itgiRiskOccupationCode': None,
                'itgiZone': data_dictionary['RegistrationZone'],
                'make': vehicle.make_code,
                'regictrationCity': data_dictionary['RegistrationCity'],
                'registrationDate': registration_date,
                'seatingCapacity': vehicle.seating_capacity,
                'type': None,
                'vehicleBody': None,
                'vehicleClass': self.Configuration.POLICY_TYPE,
                'vehicleCoverage': {
                    'item': [
                        {
                            'coverageId': '',
                            'number': None,
                            'sumInsured': '',
                        },
                    ]
                },
                'vehicleSubclass': self.Configuration.POLICY_TYPE,
                'yearOfManufacture': data_dictionary['ManufacturingYear'],
                'zcover': 'CO',
            }
        }
        insurer_utils.update_complex_dict(premium_policy, data_dictionary, 'policy_')

        # Order is important
        coverages = [{
            'coverageId': 'TPPD',
            'number': None,
            'sumInsured': '750000',
        },
            {
                'coverageId': 'Legal Liability to Driver',
                'number': None,
                'sumInsured': 'Y' if data_dictionary['extra_isLegalLiability'] == '1' else 'N',
            },
            {
                'coverageId': 'PA Owner / Driver',
                'number': None,
                'sumInsured': 'Y',
            },
            {
                'coverageId': 'PA to Passenger',
                'number': None,
                'sumInsured': data_dictionary['extra_paPassenger'],
            },
            {
                'coverageId': 'IDV Basic',
                'number': None,
                'sumInsured': int(float(data_dictionary['idv'])),
            }]

        for coverage in coverages:
            coverageId = coverage['coverageId']
            if coverageId in self.Configuration.VALID_COVERAGES:
                sumInsuredValue = self.Configuration.VALID_COVERAGES_SUMINSURED_MAP.get(coverageId, None)
                if sumInsuredValue:
                    coverage['sumInsured'] = sumInsuredValue
                premium_policy['vehicle']['vehicleCoverage']['item'].insert(0, coverage)

        return premium_policy

    def _get_premium_partner_dict(self):
        premium_partner = {
            'partnerBranch': self.Configuration.PARTNER_BRANCH,
            'partnerCode': self.Configuration.PARTNER_CODE,
            'partnerSubBranch': self.Configuration.PARTNER_SUB_BRANCH,
        }

        return premium_partner

    def convert_premium_response(self, premiumDetails, data_dictionary, service_tax, discount):
        coveragePremiumDetails = premiumDetails['coveragePremiumDetail']
        basic_cover_mapping = {
            'IDV Basic': {
                'id': 'basicOd',
                'default': '1',
            },
            'PA Owner / Driver': {
                'id': 'paOwnerDriver',
                'default': '1',
            },
            'Legal Liability to Driver': {
                'id': 'legalLiabilityDriver',
                'default': '0',
            },
            'PA to Passenger': {
                'id': 'paPassenger',
                'default': '1',
            },
            'Legal Liability to Employee': {
                'id': 'legalLiabilityEmployee',
                'default': '0',
            },
            'CNG Kit Company Fit': {
                'id': 'cngKit',
                'default': '1',
            },
            'CNG Kit': {
                'id': 'cngKit',
                'default': '1',
            },
            'Electrical Accessories': {
                'id': 'idvElectrical',
                'default': '1',
            },
            'Cost of Accessories': {
                'id': 'idvNonElectrical',
                'default': '1',
            },
        }
        addon_cover_mapping = {
        }
        discount_mapping = {
            'No Claim Bonus': {
                'id': 'ncb',
                'default': '1',
            },
        }
        special_discount_mapping = {
            'Voluntary Excess': {
                'id': 'voluntaryDeductible',
                'default': '1',
            },
            'Anti-Theft': {
                'id': 'isAntiTheftFitted',
                'default': '1',
            },
            'AAI Discount': {
                'id': 'isMemberOfAutoAssociation',
                'default': '1',
            },
        }

        basic_covers = []
        addon_covers = []
        discounts = []
        special_discounts = []
        discounts.append({
            'id': 'odDiscount',
            'default': '1',
            'premium': discount,
        })

        basic_od_premium = 0.00

        for coverage_premium in coveragePremiumDetails:
            tp = coverage_premium['tpPremium']
            od = coverage_premium['odPremium']
            cover = {
                'id': coverage_premium['coverageName'],
                'default': '1',
                'premium': (float(tp) if tp else 0.00) + (float(od) if od else 0.00),
            }
            if cover['premium'] != 0.00:
                if coverage_premium['coverageName'] in basic_cover_mapping:
                    if coverage_premium['coverageName'] == 'IDV Basic':
                        od_basic_cover = {
                            'id': 'basicOd',
                            'default': '1',
                            'premium': float(od),
                        }
                        basic_covers.append(copy.deepcopy(od_basic_cover))
                        basic_od_premium = float(od)

                        tp_basic_cover = {
                            'id': 'basicTp',
                            'default': '1',
                            'premium': float(tp),
                        }
                        basic_covers.append(copy.deepcopy(tp_basic_cover))
                    else:
                        coverage_map = basic_cover_mapping[cover['id']]
                        cover['id'] = coverage_map['id']
                        cover['default'] = coverage_map['default']
                        basic_covers.append(copy.deepcopy(cover))
                elif coverage_premium['coverageName'] in addon_cover_mapping:
                    coverage_map = addon_cover_mapping[cover['id']]
                    cover['id'] = coverage_map['id']
                    cover['default'] = coverage_map['default']
                    addon_covers.append(copy.deepcopy(cover))
                elif coverage_premium['coverageName'] in special_discount_mapping:
                    coverage_map = special_discount_mapping[cover['id']]
                    cover['id'] = coverage_map['id']
                    cover['default'] = coverage_map['default']
                    special_discounts.append(copy.deepcopy(cover))
                else:
                    coverage_map = discount_mapping[cover['id']]
                    cover['id'] = coverage_map['id']
                    cover['default'] = coverage_map['default']
                    discounts.append(copy.deepcopy(cover))

        vehicle = Vehicle.objects.filter(insurer__slug=INSURER_SLUG).get(id=data_dictionary['VehicleId'])

        result_data = {
                   'insurerId': int(INSURER_ID),
                   'insurerSlug': INSURER_SLUG,
                   'insurerName': INSURER_NAME,
                   'isInstantPolicy': True,
                   'minIDV': int(data_dictionary['min_idv']),
                   'maxIDV': int(data_dictionary['max_idv']),
                   'calculatedAtIDV': int(data_dictionary['idv']),
                   'serviceTaxRate': 0.145,
                   'premiumBreakup': {
                       'serviceTax': round(float(service_tax), 2),
                       'basicCovers': insurer_utils.cpr_update(basic_covers),
                       'addonCovers': insurer_utils.cpr_update(addon_covers),
                       'discounts': insurer_utils.cpr_update(discounts),
                       'specialDiscounts': insurer_utils.cpr_update(special_discounts),
                       'paPassengerAmounts': insurer_utils.get_pa_passenger_dict(vehicle.vehicle_type,
                                                                                 vehicle.seating_capacity),
                   },
               }, premiumDetails
        return {'result': result_data, 'success': True}


class ProposalGenerator(object):
    def __init__(self, configuration):
        self.Configuration = configuration

    proposal_contact_dictionary_mapping = {
        'Salutation': custom_dictionaries.SALUTATION_MAPPING,
        'Sex': custom_dictionaries.GENDER_MAPPING,
        'Married': custom_dictionaries.MARITAL_STATUS_MAPPING,
    }

    proposal_private_car_dictionary_mapping = {}

    proposal_policy_dictionary_mapping = {
        'PreviousPolicyInsurer': custom_dictionaries.PAST_INSURANCE_ID_LIST,
        # 'PreviousPolicyInsurer' : PAST_INSURER_ID_LIST_MASTER,
        'NomineeRelationship': custom_dictionaries.NOMINEE_RELATIONSHIP_MAPPING,
    }

    def save_policy_details(self, data_dictionary, transaction):
        proposer_dict = self._get_proposal_contact_dict(data_dictionary)

        proposer_dict['ExternalClientNo'] = transaction.proposer.id

        private_car_dict = self._get_proposal_private_car_dict(data_dictionary)

        policy_dict = self._get_proposal_policy_dict(data_dictionary)
        current_date_time = datetime.datetime.now()
        policy_dict['UniqueQuoteId'] = 'Cfox%s' % current_date_time.strftime("%Y%m%d%H%M%S")

        # Save transaction
        inception_date = datetime.datetime.strptime(policy_dict['InceptionDate'], '%m/%d/%Y 00:00:00').isoformat()
        expiry_date = datetime.datetime.strptime(policy_dict['ExpiryDate'], '%m/%d/%Y 23:59:00').isoformat()
        transaction.policy_start_date = inception_date
        transaction.policy_end_date = expiry_date
        transaction.reference_id = policy_dict['UniqueQuoteId']
        transaction.premium_paid = policy_dict['NetPremiumPayable']

        combined_dict = {
            'policy_dict': policy_dict,
            'proposer_dict': proposer_dict,
            'private_car_dict': private_car_dict,
        }

        transaction.raw['policy_details'] = combined_dict
        transaction.save()
        return True

    def _get_proposal_contact_dict(self, data_dictionary):
        contact_dict = {
            'DOB': '02/08/1982',
            'PassPort': None,
            'PAN': '34547688',
            'SiebelContactNumber': None,
            'ExternalClientNo': '',
            'ItgiClientNumber': None,
            'Salutation': 'MR',
            'FirstName': 'D',
            'LastName': 'Ram Reddy',
            'Sex': 'M',
            'AddressType': 'R',
            'PinCode': '500004',
            'State': 'AP',
            'AddressLine1': 'HNo.3456',
            'AddressLine2': 'Jangampally',
            'FaxNo': None,
            'Country': 'IND',
            'CountryOrigin': 'IND',
            'Occupation': 'FARM',
            'City': 'NZMBD',
            'Source': None,
            'Nationality': 'IND',
            'Married': 'M',
            'HomePhone': None,
            'OfficePhone': None,
            'MobilePhone': '9854754126',
            'Pager': None,
            'MailId': None,
            'TaxId': None,
            'StafFlag': None,
            'AddressLine3': None,
            'AddressLine4': None,
        }
        insurer_utils.update_complex_dict(contact_dict, data_dictionary, 'contact_',
                                          self.proposal_contact_dictionary_mapping)
        return contact_dict

    def _get_proposal_private_car_dict(self, data_dictionary):
        private_car_dict = {
            'AAINumber': None,
            'Capacity': '',
            'EngineCapacity': '',
            'GrossVehicleWeight': '0',
            'Make': '',
            'RegistrationNumber1': '',
            'RegistrationNumber2': '',
            'RegistrationNumber3': '',
            'RegistrationNumber4': '',
            'policyType': self.Configuration.POLICY_TYPE,
            'ManufacturingYear': '2011',
            'Zone': 'A',
            'RiskOccupationCode': None,
            'VehicleBody': None,
            'EngineNumber': '',
            'ChassisNumber': '',
            'SeatingCapacity': '',
            'AAIExpiryDate': None,
            'RegistrationDate': '',
            'RTOCity': '',
            'ImposedExcessPartialLoss': None,
            'ImposedExcessTotalLoss': None,
            'Zcover': 'CO',
        }
        insurer_utils.update_complex_dict(private_car_dict, data_dictionary, 'privateCar_',
                                          self.proposal_private_car_dictionary_mapping)
        return private_car_dict

    def _get_proposal_policy_dict(self, data_dictionary):
        policy_dict = {
            'Product': self.Configuration.POLICY_TYPE,
            'CreatedDate': '04/06/2011',
            'InceptionDate': '04/06/2011 00:00:00',
            'UniqueQuoteId': '',
            'ExpiryDate': '04/05/2012 23:59:00',
            'GeneralPage': None,
            'OdDiscountLoading': '',
            'OdDiscountAmt': '',
            'OdSumDisLoad': '',
            'TpSumDisLoad': '',
            'GrossPremium': '',
            'ServiceTax': '',
            'NetPremiumPayable': '',
            'TotalSumInsured': '',
            'PreviousPolicyEnddate': None,
            'PreviousPolicyStartdate': None,
            'PreviousPolicyInsurer': None,
            'PreviousPolicyNo': None,
            'ExternalBranch': self.Configuration.PARTNER_BRANCH,
            'ExternalSubBranch': self.Configuration.PARTNER_SUB_BRANCH,
            'ExternalServiceConsumer': self.Configuration.PARTNER_CODE,
            'Nominee': '',
            'NomineeRelationship': '',
        }
        insurer_utils.update_complex_dict(policy_dict, data_dictionary, 'policy_',
                                          self.proposal_policy_dictionary_mapping)
        return policy_dict

    def _get_proposal_account_dict(self, data_dictionary):
        account_dict = {
            'DOB': None,
            'PAN': None,
            'AccountNumber': None,
            'ExternalAccountId': None,
            'ClientNumber': None,
            'Name': None,
            'TaxId': None,
            'PrimaryAccountStreetAddress': None,
            'PrimaryAccountStreetAddress2': None,
            'MainPhoneNumber': None,
            'MainFaxNumber': None,
            'MailId': None,
            'EconomicActivity': None,
            'PaidCapital': None,
            'PrimaryAccountPostalCode': None,
            'PrimaryAccountState': None,
            'PrimaryAccountCity': None,
            'PrimaryAccountCountry': None,
            'Source': None,
            'Licence': None,
            'PrimaryAccountStreetAddress3': None,
            'PrimaryAccountStreetAddress4': None,
        }
        return account_dict

    def _get_proposal_vehicle_third_party_dict(self, data_dictionary):
        vehicle_third_party_dict = {
            'InterestedParty': '',
            'InterestedPartyName': '',
            'Relation': '',
        }
        return vehicle_third_party_dict


class IntegrationUtils(object):
    PremiumCalculator = None
    ProposalGenerator = None

    def __init__(self, vehicle_type):
        configuration = Configuration(vehicle_type)
        self.Configuration = configuration
        self.PremiumCalculator = PremiumCalculator(configuration)
        self.ProposalGenerator = ProposalGenerator(configuration)

    def convert_premium_request_data_to_required_dictionary(self, vehicle_id, request_data):

        # TODO - Should be removed once data coming correctly from front end
        if 'newPolicyStartDate' not in request_data.keys():
            request_data['newPolicyStartDate'] = (datetime.datetime.now() + datetime.timedelta(days=1)).strftime(
                "%d-%m-%Y")

        if request_data['isNewVehicle'] == '1':
            is_new_vehicle = True
        else:
            is_new_vehicle = False

        # New policy start date and end date assignment
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(request_data['newPolicyStartDate'], "%d-%m-%Y")
            new_policy_end_date = putils.add_years(
                new_policy_start_date, 1) - datetime.timedelta(days=1)
        else:
            past_policy_end_date = datetime.datetime.strptime(request_data['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)
            new_policy_end_date = putils.add_years(
                new_policy_start_date, 1) - datetime.timedelta(days=1)

        # Vehicle age calculation
        if is_new_vehicle:
            vehicle_age = 0
            request_data['registrationDate'] = datetime.datetime.now().strftime('%d-%m-%Y')
        else:
            date_of_registration = datetime.datetime.strptime(request_data['registrationDate'], '%d-%m-%Y')
            new_policy_start_date = new_policy_start_date.replace(microsecond=0)
            vehicle_age = insurer_utils.get_age(new_policy_start_date, date_of_registration)

        man_date_split = request_data['manufacturingDate'].split('-')
        manufacturing_year = man_date_split[2]
        manufacturing_month = man_date_split[1]

        reg_date_split = request_data['registrationDate'].split('-')
        registration_year = reg_date_split[2]
        registration_month = reg_date_split[1]
        registration_day = reg_date_split[0]

        split_registration_no = request_data.get('registrationNumber[]')
        master_rto_code = "%s-%s" % (split_registration_no[0], split_registration_no[1])
        insurer_rto = RTOMaster.objects.get(rto_code=master_rto_code).insurer_rtos.get(insurer__slug=INSURER_SLUG)
        rto_city_name = insurer_rto.rto_name
        rto_city_code = insurer_rto.rto_code
        rto_city_zone = insurer_rto.rto_zone

        data_dictionary = {
            'vehicleAge': vehicle_age,
            'isUsedVehicle': True if request_data['isUsedVehicle'] == '1' else False,
            'isExpired': False if is_new_vehicle else (
                past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now()),
            'VehicleId': vehicle_id, 'RegistrationCity': rto_city_code,
            'RegistrationZone': rto_city_zone, 'ManufacturingYear': manufacturing_year,
            'ManufacturingMonth': manufacturing_month, 'RegistrationYear': registration_year,
            'RegistrationMonth': registration_month, 'RegistrationDay': registration_day,
            'policy_vehicle': {
                'policy_vehicleCoverage': {
                    'policy_item': [],
                },
            },
            'previous_ncb': request_data['previousNCB'],
            'isNewCar': request_data['isNewVehicle'],
            'policy_inceptionDate': new_policy_start_date.strftime("%m/%d/%Y 00:00:00"),
            'policy_expiryDate': new_policy_end_date.strftime("%m/%d/%Y 23:59:00"),
            'extra_paPassenger': request_data['extra_paPassenger'],
            'extra_isLegalLiability': request_data['extra_isLegalLiability'], 'idv': request_data['idv']
        }

        cng_coverage_id = 'CNG Kit Company Fit' if request_data['cngKitValue'] == '0' else 'CNG Kit'
        previous_ncb_to_new_ncb = {
            '0': '20',
            '20': '25',
            '25': '35',
            '35': '45',
            '45': '50',
            '50': '50',
        }

        request_data_keys_to_coverage_mapping = {
            'isCNGFitted': {
                'unacceptable': ['0'],
                'coverageId': cng_coverage_id,
                'sum_insured': 'Y' if cng_coverage_id == 'CNG Kit Company Fit' else request_data['cngKitValue'],
            },
            'isNewVehicle': {
                'unacceptable': ['1'],
                'coverageId': 'No Claim Bonus',
                'sum_insured': '0' if request_data['isClaimedLastYear'] == '1' else previous_ncb_to_new_ncb[
                    request_data['previousNCB']],
            },
            'idvElectrical': {
                'unacceptable': ['0'],
                'coverageId': 'Electrical Accessories',
                'sum_insured': request_data['idvElectrical'],
            },
            'voluntaryDeductible': {
                'unacceptable': [None, 'None', '0'],
                'coverageId': 'Voluntary Excess',
                'sum_insured': request_data['voluntaryDeductible'],
            },
            'idvNonElectrical': {
                'unacceptable': ['0'],
                'coverageId': 'Cost of Accessories',
                'sum_insured': request_data['idvNonElectrical'],
            },
            'extra_isAntiTheftFitted': {
                'unacceptable': ['0'],
                'coverageId': 'Anti-Theft',
                'sum_insured': 'Y' if request_data.get('extra_isAntiTheftFitted', '0') == '1' else 'N',
            },
            'extra_isMemberOfAutoAssociation': {
                'unacceptable': ['0'],
                'coverageId': 'AAI Discount',
                'sum_insured': 'Y' if request_data.get('extra_isMemberOfAutoAssociation', '0') == '1' else 'N',
            }
        }

        for key, mapping_value in request_data_keys_to_coverage_mapping.items():
            if request_data[key] not in mapping_value['unacceptable']:
                item = {
                    'policy_coverageId': mapping_value['coverageId'],
                    'policy_number': None,
                    'policy_sumInsured': mapping_value['sum_insured'],
                }
                data_dictionary['policy_vehicle']['policy_vehicleCoverage']['policy_item'].append(item)

        # set new_policy_start_date for old vehicle
        if not is_new_vehicle:
            past_policy_end_date = datetime.datetime.strptime(request_data['pastPolicyExpiryDate'], "%d-%m-%Y")
            data_dictionary['policy_previousPolicyEndDate'] = past_policy_end_date.strftime("%m/%d/%Y 23:59:00")

        return data_dictionary

    def get_buy_form_details(self, quote_parameters, transaction):

        split_registration_no = quote_parameters.get('registrationNumber[]')

        is_new_vehicle = True if quote_parameters['isNewVehicle'] == '1' else False
        if is_new_vehicle:
            registration_number = 'NEW---0001'
        else:
            registration_number = '-'.join(split_registration_no)

        master_rto_code = "%s-%s" % (split_registration_no[0], split_registration_no[1])

        data = {
            'registration_no': registration_number,
            'rto_location_info': master_rto_code,
            # 'occupation_list': OCCUPATION_MAPPING,
            # 'insurers_list': PAST_INSURANCE_ID_LIST,
            'insurers_list': custom_dictionaries.PAST_INSURER_ID_LIST_MASTER,
            'disallowed_insurers': custom_dictionaries.DISALLOWED_PAST_INSURERS,
            'state_list': custom_dictionaries.STATE_ID_TO_STATE_NAME,
            'relationship_list': custom_dictionaries.NOMINEE_RELATIONSHIP_MAPPING.keys(),
            'is_cities_fetch': True,
        }
        return data

    def convert_proposal_request_data_to_required_dictionary(self, vehicle, request_data, quote_parameters,
                                                             quote_response, quote_custom_response):

        idv = quote_custom_response['calculatedAtIDV']

        if request_data['cust_gender'] == 'Male':
            request_data['form-insured-title'] = 'Mr.'
        else:
            if request_data['cust_marital_status'] == '0':
                request_data['form-insured-title'] = 'Ms.'
            else:
                request_data['form-insured-title'] = 'Mrs.'

        is_new_vehicle = False
        if quote_parameters['isNewVehicle'] == '1':
            is_new_vehicle = True

        if is_new_vehicle:
            request_data['vehicle_reg_no'] = 'NEW---0001'

        currentDateTime = datetime.datetime.now()
        currentDateTime = currentDateTime.replace(microsecond=0)

        cust_dob = datetime.datetime.strptime(request_data['cust_dob'], "%d-%m-%Y")

        master_rto_code = "-".join(quote_parameters['registrationNumber[]'][0:2])
        insurer_rto = RTOMaster.objects.get(rto_code=master_rto_code).insurer_rtos.get(insurer__slug=INSURER_SLUG)
        rto_city_name = insurer_rto.rto_name
        rto_city_code = insurer_rto.rto_code
        rto_zone = insurer_rto.rto_zone

        if is_new_vehicle:
            request_data['vehicle_reg_date'] = datetime.datetime.now().strftime("%d-%m-%Y")
        else:
            request_data['vehicle_reg_date'] = quote_parameters['registrationDate']

        vehicle_reg_date = datetime.datetime.strptime(request_data['vehicle_reg_date'], "%d-%m-%Y")
        reg_date_split = request_data['vehicle_reg_date'].split('-')
        vehicle_buying_year = int(reg_date_split[2])

        man_date_split = quote_parameters['manufacturingDate'].split('-')
        manufacturing_year = man_date_split[2]
        manufacturing_month = man_date_split[1]

        split_registration_number = request_data['vehicle_reg_no'].split('-')
        if len(split_registration_number[2])>=3:
            split_registration_number[1] = split_registration_number[1]+split_registration_number[2][:-2]
            split_registration_number[2] = split_registration_number[2][-2:]

        premium_details = quote_response

        # Assigning new_policy_start_date and new_policy_end_date
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(quote_parameters['newPolicyStartDate'], "%d-%m-%Y")
            new_policy_end_date = putils.add_years(
                new_policy_start_date, 1) - datetime.timedelta(days=1)
        else:
            past_policy_end_date = datetime.datetime.strptime(quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)
            new_policy_end_date = putils.add_years(
                new_policy_start_date, 1) - datetime.timedelta(days=1)

        request_data_dictionary = {

            # PROPOSAL_CONTACT_DATA_DICTIONARY
            'contact_DOB': cust_dob.strftime("%m/%d/%Y"),
            'contact_PAN': request_data['cust_pan'],
            'contact_Salutation': request_data['form-insured-title'],
            'contact_FirstName': request_data['cust_first_name'],
            'contact_LastName': request_data['cust_last_name'],
            'contact_Sex': request_data['cust_gender'],
            'contact_PinCode': request_data['add_pincode'],
            'contact_State': request_data['add_state'],
            'contact_AddressLine1': request_data['add_house_no'],
            'contact_AddressLine2': request_data['add_building_name'],
            'contact_AddressLine3': request_data['add_street_name'],
            'contact_AddressLine4': request_data['add_landmark'],
            'contact_Occupation': 'OTHR',  # Default occupation set to Others
            'contact_City': request_data['add_city'],
            'contact_Married': 'Not Known',
            'contact_MobilePhone': request_data['cust_phone'],
            'contact_MailId': request_data['cust_email'],

            # PROPOSAL_PRIVATE_CAR_DATA_DICTIONARY
            'privateCar_AAINumber': request_data['aan_number']
            if quote_parameters['extra_isMemberOfAutoAssociation'] == '1' else None,
            'privateCar_AAIExpiryDate': new_policy_end_date.strftime("%m/%d/%Y 23:59:00")
            if quote_parameters['extra_isMemberOfAutoAssociation'] == '1' else None,
            'privateCar_Capacity': vehicle.seating_capacity,
            'privateCar_EngineCapacity': vehicle.cc,
            'privateCar_Make': vehicle.make_code,
            'privateCar_RegistrationNumber1': split_registration_number[0],
            'privateCar_RegistrationNumber2': split_registration_number[1],
            'privateCar_RegistrationNumber3': split_registration_number[2],
            'privateCar_RegistrationNumber4': split_registration_number[3],
            'privateCar_ManufacturingYear': manufacturing_year,
            'privateCar_Zone': rto_zone,
            'privateCar_EngineNumber': request_data['vehicle_engine_no'],
            'privateCar_ChassisNumber': request_data['vehicle_chassis_no'],
            'privateCar_SeatingCapacity': vehicle.seating_capacity,
            'privateCar_RegistrationDate': vehicle_reg_date.strftime("%m/%d/%Y"),
            'privateCar_RTOCity': rto_city_code.strip(),

            # PROPOSAL_POLICY_DATA_DICTIONARY
            'policy_CreatedDate': currentDateTime.strftime("%m/%d/%Y"),
            'policy_InceptionDate': new_policy_start_date.strftime("%m/%d/%Y 00:00:00"),
            'policy_ExpiryDate': new_policy_end_date.strftime("%m/%d/%Y 23:59:00"),
            'policy_OdDiscountLoading': premium_details['discountLoading'],
            'policy_OdDiscountAmt': premium_details['discountLoadingAmt'],
            'policy_OdSumDisLoad': premium_details['totalODPremium'],
            'policy_TpSumDisLoad': premium_details['totalTPPremium'],
            'policy_GrossPremium': premium_details['totalPremimAfterDiscLoad'],
            'policy_ServiceTax': premium_details['serviceTax'],
            'policy_NetPremiumPayable': premium_details['premiumPayable'],
            'policy_TotalSumInsured': idv,
            'policy_Nominee': request_data['nominee_name'],
            'policy_NomineeRelationship': request_data['nominee_relationship'],
        }

        if not is_new_vehicle:
            past_policy_end_date = datetime.datetime.strptime(quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")
            past_policy_start_date = putils.subtract_years(
                past_policy_end_date, 1) + datetime.timedelta(days=1)

            request_data_dictionary['policy_PreviousPolicyEnddate'] = past_policy_end_date.strftime("%m/%d/%Y 23:59:00")
            request_data_dictionary['policy_PreviousPolicyStartdate'] = past_policy_start_date.strftime(
                "%m/%d/%Y 00:00:00")
            # request_data_dictionary['policy_PreviousPolicyInsurer'] = request_data['past_policy_insurer']
            past_policy_insurer_code = custom_dictionaries.REV_PASTINSURER_MAP[request_data['past_policy_insurer']]
            request_data_dictionary['policy_PreviousPolicyInsurer'] = past_policy_insurer_code
            request_data_dictionary['policy_PreviousPolicyNo'] = request_data['past_policy_number']

        return request_data_dictionary

    def update_transaction_details(self, transaction, data):
        is_new_vehicle = data['isNewVehicle'] == '1'
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                data['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

        new_policy_end_date = putils.add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)

        transaction.policy_start_date = new_policy_start_date
        transaction.policy_end_date = new_policy_end_date
        transaction.save()

    def get_city_list(self, state):
        return custom_dictionaries.STATE_ID_TO_CITY_LIST.get(state, None)

    def get_payment_data(self, transaction):

        response_url = self.Configuration.RESPONSE_URL
        post_xml_data = self.get_proposal_data(transaction)

        payment_gateway_data = {
            'RESPONSE_URL': response_url,
            'PARTNER_CODE': self.Configuration.PARTNER_CODE,
            'UNIQUE_QUOTEID': transaction.reference_id,
            'XML_DATA': post_xml_data,
        }
        transaction.proposal_request = post_xml_data
        transaction.payment_request = payment_gateway_data
        transaction.save()
        suds_logger.set_name(transaction.transaction_id).log(post_xml_data)
        return payment_gateway_data, PROPOSAL_URL

    def get_proposal_data(self, transaction):

        quote_parameters = transaction.quote.raw_data
        quote_response = transaction.raw['quote_raw_response']

        vehicle_xml = parser_utils.dict_to_xml('Vehicle', transaction.raw['policy_details']['private_car_dict'])
        contact_xml = parser_utils.dict_to_xml('Contact', transaction.raw['policy_details']['proposer_dict'])
        account_xml = parser_utils.dict_to_xml('Account', self.ProposalGenerator._get_proposal_account_dict({}))  # TODO
        vehicle_third_party_xml = parser_utils.dict_to_xml('VehicleThirdParty',
                                                           self.ProposalGenerator._get_proposal_vehicle_third_party_dict(
                                                               {}))  # TODO

        ### Coverages
        coverage_xml_list = ''
        coverage_premium_details = quote_response['coveragePremiumDetail']
        vehicle_coverage_list = quote_response['raw_request_policy']['vehicle']['vehicleCoverage']['item']

        request_vehicle_coverages = {}
        for coverage in vehicle_coverage_list:
            request_vehicle_coverages[coverage['coverageId']] = coverage

        total_sum_insured = 0
        coverage_codes_for_sum_insured_included_in_total_list = [
            'Electrical Accessories',
            'IDV Basic',
            'Cost of Accessories',
            'CNG Kit',
        ]

        for coverage in coverage_premium_details:
            coverage_code = coverage['coverageName']
            if coverage_code in request_vehicle_coverages.keys():
                od_premium = coverage['odPremium']
                tp_premium = coverage['tpPremium']
                coverage_dict = {
                    'Code': coverage_code,
                    'Number': None,
                    # 'SumInsured' : int(float(request_vehicle_coverages[coverage_code]['sumInsured'])),
                    # 'ODPremium' : od_premium,
                    # 'TPPremium' : tp_premium,
                }
                if od_premium in ['0.00', '-0.00', None, 'None']:
                    coverage_dict['ODPremium'] = od_premium
                else:
                    coverage_dict['ODPremium'] = format(float(od_premium), '.2f')
                if tp_premium in ['0.00', '-0.00', None, 'None']:
                    coverage_dict['TPPremium'] = tp_premium
                else:
                    coverage_dict['TPPremium'] = format(float(tp_premium), '.2f')

                if request_vehicle_coverages[coverage_code]['sumInsured'] in ['Y', 'N', None, 'None']:
                    coverage_dict['SumInsured'] = request_vehicle_coverages[coverage_code]['sumInsured']
                else:
                    coverage_dict['SumInsured'] = int(float(request_vehicle_coverages[coverage_code]['sumInsured']))
                    if coverage_dict['Code'] in coverage_codes_for_sum_insured_included_in_total_list:
                        total_sum_insured = total_sum_insured + coverage_dict['SumInsured']

                coverage_xml = parser_utils.dict_to_xml('Coverage', coverage_dict)
                if coverage_dict['SumInsured'] != 'None':
                    coverage_xml_list = coverage_xml_list + coverage_xml

        transaction.raw['policy_details']['policy_dict']['TotalSumInsured'] = total_sum_insured
        transaction.save()

        policy_dict = transaction.raw['policy_details']['policy_dict']
        policy_xml = parser_utils.dict_to_xml('Policy', policy_dict)

        post_xml_data = '<Request>'
        post_xml_data = post_xml_data + policy_xml
        post_xml_data = post_xml_data + coverage_xml_list
        post_xml_data = post_xml_data + vehicle_xml
        post_xml_data = post_xml_data + contact_xml
        post_xml_data = post_xml_data + account_xml
        post_xml_data = post_xml_data + vehicle_third_party_xml
        post_xml_data = post_xml_data + '</Request>'

        motor_logger.info(
            '\nIFFCO TRANSACTION: %s\nPROPOSAL REQUEST SENT:\n%s\n' % (transaction.transaction_id, post_xml_data))
        return post_xml_data

    def post_payment(self, payment_response, transaction):
        """
        SAMPLE RESPONSE: {u'ITGIResponse' : u'PCP|057100|1-10Y09H0|7357.12|SUCCESSFULLY_UPDATED_IN_SIEBEL|ITGIMOT025R2D2115'}
        """
        motor_logger.info(payment_response)
        redirect_url = '/motor/%s/payment/failure/' % (self.Configuration.URL_TAG)
        is_redirect = True
        resp = payment_response['GET']

        transaction_success_messages = ['SUCCESSFULLY_UPDATED_IN_SIEBEL', 'SUCCESSFULLY_SUBMITTED_IN_P400']

        if 'error' not in resp.keys():
            response_list = resp['ITGIResponse'].split('|')
            reference_id = response_list[-1]

            try:
                transaction_from_response = Transaction.objects.get(reference_id=reference_id)
                transaction = transaction_from_response
                suds_logger.set_name(transaction.transaction_id).log(json.dumps(payment_response))
            except:
                log_error(motor_logger)
            transaction.proposal_response = payment_response
            transaction.insured_details_json['payment_response'] = resp
            transaction.payment_response = resp
            transaction.save()
            if response_list[4] in transaction_success_messages:
                transaction.policy_token = response_list[2]
                transaction.policy_number = response_list[2]
                transaction.premium_paid = response_list[3]
                transaction.payment_done = True
                transaction.payment_on = datetime.datetime.now()
                transaction.mark_complete()
                redirect_url = '/motor/%s/payment/success/%s/' % (
                    self.Configuration.URL_TAG, transaction.transaction_id)
            else:
                transaction.status = 'FAILED'
                transaction.save()
                redirect_url += '%s/' % (transaction.transaction_id)
        else:
            transaction.status = 'FAILED'
            transaction.save()
            redirect_url += '%s/' % (transaction.transaction_id)
        return is_redirect, redirect_url, '', {}

    def get_dependent_addons(self, parameters):
        return {'type': 'dynamic'}

    def get_transaction_from_id(self, transaction_id):
        transaction = Transaction.objects.get(transaction_id=transaction_id)
        return transaction

    def get_custom_data(self, datatype):
        datamap = {
            'occupations': custom_dictionaries.OCCUPATION_MAPPING,
            # 'insurers' : PAST_INSURANCE_ID_LIST,
            'insurers': custom_dictionaries.PAST_INSURER_ID_LIST_MASTER,
            'states': custom_dictionaries.STATE_ID_TO_STATE_NAME,
            'relationships': custom_dictionaries.NOMINEE_RELATIONSHIP_MAPPING.keys(),
        }
        return datamap.get(datatype, None)

    def disable_cache_for_request(self, request_data):
        if (request_data['isNewVehicle'] != '1' and datetime.datetime.strptime(request_data['pastPolicyExpiryDate'],
                                                                               '%d-%m-%Y').replace(hour=23, minute=59,
                                                                                                   second=0) < datetime.datetime.now()):
            return True
        return False
