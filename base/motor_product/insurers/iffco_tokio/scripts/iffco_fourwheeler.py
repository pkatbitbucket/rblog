from motor_product.models import Vehicle, Insurer, VehicleMaster, VehicleMapping
from motor_product import parser_utils

INSURER_SLUG = 'iffco-tokio'
INSURER_MASTER_PATH = 'motor_product/insurers/iffco_tokio/scripts/data/iffco_fourwheeler.csv'
MASTER_PATH = 'motor_product/scripts/fourwheeler/data/master_fourwheeler.csv'

csv_parameters = {
    'MAKE_CODE': 0,
    'MANUFACTURE': 1,
    'MODEL': 2,
    'VARIANT': 3,
    'CC': 4,
    'SEATING_CAPACITY': 5,
    'FUEL_TYPE': 7,    
    'Master': 8,
}

parameters = {
    'make_code': 0,
    'make': 1,
    'model': 2,
    'variant': 3,
    'cc': 4,
    'seating_capacity': 5,
    'fuel_type': 7,
    'master': 8,
}

master_parameters = {
    'make': 0,
    'model': 1,
    'variant': 2,
    'fuel_type': 3,
    'cc': 4,
    'seating_capacity': 5,
    'insurer': 10,  # Refer to master csv
}


def convert_fuel_type_literals_into_fuel_type(fuel_type_literal):
    if fuel_type_literal in ['P', 'PETROL']:
        fuel_type = 'Petrol'
    elif fuel_type_literal in ['D', 'DIESEL']:
        fuel_type = 'Diesel'
    elif fuel_type_literal == 'C':
        fuel_type = 'External LPG / CNG'
    elif fuel_type_literal == 'G':
        fuel_type = 'Internal LPG / CNG'
    elif fuel_type_literal in ['BATTERY', 'B', 'BATERY', 'BATRY', 'E', 'ELECTRICITY', 'E']:
        fuel_type = 'Electricity'
    else:
        fuel_type = 'Not Defined'
    return fuel_type


def save_data():
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_MASTER_PATH, parameters)
    insurer = Insurer.objects.get(slug=INSURER_SLUG)

    vehicle_objects = []
    for _vehicle in vehicles:
        _vehicle['cc'] = parser_utils.try_parse_into_integer(_vehicle['cc'])
        _vehicle['seating_capacity'] = parser_utils.try_parse_into_integer(_vehicle[
                                                                           'seating_capacity'])
        _vehicle['fuel_type'] = convert_fuel_type_literals_into_fuel_type(_vehicle[
                                                                          'fuel_type'])
        _vehicle['insurer'] = insurer

        del _vehicle['master']

        vehicle = Vehicle(**_vehicle)
        print 'Saving vehicle %s' % vehicle.model
        vehicle_objects.append(vehicle)

    Vehicle.objects.bulk_create(vehicle_objects)


def delete_data():
    Vehicle.objects.filter(vehicle_type='Private Car',
                           insurer__slug=INSURER_SLUG).delete()


def delete_mappings():
    VehicleMapping.objects.filter(
        insurer__slug=INSURER_SLUG, master_vehicle__vehicle_type='Private Car').delete()


def create_mappings():
    insurer = Insurer.objects.get(slug=INSURER_SLUG)
    master_vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        MASTER_PATH, master_parameters)
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_MASTER_PATH, parameters)

    ivehicles = Vehicle.objects.filter(
        vehicle_type='Private Car', insurer__slug=INSURER_SLUG)

    vehicle_map = {}
    for iv in vehicles:
        _vehicle = ivehicles.filter(make_code=iv['make_code'])[0]
        vehicle_map[iv['master']] = _vehicle

    mappings = []
    for mv in master_vehicles:
        _master = VehicleMaster.objects.get(make__name=mv['make'], model__name=mv['model'],
                                            variant=mv['variant'], cc=mv['cc'],
                                            fuel_type=mv['fuel_type'],
                                            seating_capacity=mv['seating_capacity'])
        mapping = VehicleMapping()
        mapping.master_vehicle = _master
        mapping.mapped_vehicle = vehicle_map.get(mv['insurer'], None)
        mapping.insurer = insurer

        if mapping.mapped_vehicle:
            print 'Mapping %s to %s' % (_master, mapping.mapped_vehicle)
            mappings.append(mapping)

    VehicleMapping.objects.bulk_create(mappings)
