import copy
import datetime
import math
import re

from dateutil import relativedelta
from django.db.models import Q
from lxml import etree
from suds.sudsobject import asdict

from motor_product import models as motor_models


COVER_NAME_MAP = {
    'basicOd': 'Basic OD Premium',
    'paOwnerDriver': 'PA Owner-Driver',
    'paFamily': 'PA Family',
    'basicTp': 'Basic TP Premium',
    'paPassenger': 'PA Unnamed Passenger',
    'cngKit': 'CNG/LPG Kit Premium',
    'cngKitTp': 'CNG/LPG Kit TP Premium',
    'idvElectrical': 'IDV Electrical',
    'idvNonElectrical': 'IDV Non-electrical',
    'idvAccessoriers': 'Electrical and Non-electrical Accessoriers',
    'ncb': 'No Claim Bonus',
    'voluntaryDeductible': 'Voluntary Deductible',
    'isDepreciationWaiver': 'Zero Dep',
    'isAntiTheftFitted': 'Anti-theft discount',
    'isMemberOfAutoAssociation': 'Automobile Association discount',
    'isTPPDDiscount': 'TPPD Discount',
    'isInvoiceCover': 'Invoice Cover',
    'isAmbulanceCover': 'Ambulance Cover',
    'isHospitalCover': 'Hospital Cover',
    'is247RoadsideAssistance': 'Roadside Assistance',
    'isMedicalCover': 'Medical Cover',
    'legalLiabilityDriver': 'LL Driver',
    'isEngineProtector': 'Engine Protector',
    'isHydrostaticLock': 'Hydrostatic Lock Cover',
    'isCostOfConsumables': 'Consumable Cover',
    'isNcbProtection': 'NCB Protection',
    'legalLiabilityEmployee': 'LL Employee',
    'isOccupationDiscount': 'Occupation Discount',
    'isAgeDiscount': 'Age Discount',
    'odDiscount': 'OD Discount',
    'commDiscount': 'Commercial Discount',
    'extraDiscount': 'Extra Discount',
    'odExtra': 'Extra OD Premium',
    'tpExtra': 'Extra TP Premium',
    'workmenCompensation': 'Workmen Compensation',
    'repairGlassFiberPlastic': 'Repair Glass Fiber Plastic',
    'corporateEmployeeDiscount': 'Employee Discount'
}

MASTER_INSURER_LIST = {
    # '1': 'Agriculture Insurance Company',
    # '2': 'Apollo Munich',
    '3': 'Bajaj Allianz',
    '4': 'Bharti AXA',
    '5': 'Cholamandalam MS',
    '6': 'Future Generali',
    '7': 'HDFC ERGO',
    '8': 'ICICI Lombard',
    '9': 'IFFCO Tokio',
    '10': 'L&T General',
    # '11': 'Max Bupa',
    '12': 'National Insurance',
    '13': 'Raheja QBE',
    '14': 'Reliance General',
    '15': 'Royal Sundaram Alliance',
    '16': 'SBI General',
    '17': 'Shriram General',
    '18': 'Tata AIG',
    '19': 'New India Assurance',
    '20': 'Oriental Insurance',
    '21': 'United India',
    '22': 'Universal Sompo',
    '23': 'Export Credit Guarantee Corporation',
    '24': 'Liberty Videocon',
    '25': 'Magma HDI',
    # '26': 'Star Health Allied',
    # '27': 'Religare',
    '28': 'General Insurance Corporation',
    # '29': 'Gujarat State Insurance Fund',
    # '30': 'Maharashtra Govt Insurance Fund',
    # '31': 'Reinsurance Inward',
    # '32': 'HDFC Chub General Insurance Co.Ltd',
    # '33': 'Rajasthan State Insurance and Provident Fund',
    # '34': 'KERALA STATE INSURANCE',
    # '35': 'KARNATAKA GOVERNMENT INSURANCE DEPARTMENT',
}

# Use this method to set attributes of an object
# from corresponding keys in the dictionary


def convert_dict_to_obj(attrdict, obj):
    for k, v in attrdict.items():
        setattr(obj, k, v)


# The following two methods are used to update one complex
# dictionary from other. current_dict is the dictionary to be updated
# and new_dict is the dictionary from which values are to be taken
# key_prefix is the prefix to the key in new_dict and mapping_dict is
# the dictionary containing the reference to the mapping table
def update_complex_dict(current_dict, new_dict, key_prefix='', mapping_dict={}):
    for key, item in current_dict.items():
        if isinstance(item, dict):
            if (key_prefix + key) in new_dict.keys():
                update_complex_dict(current_dict[key], new_dict[
                                    key_prefix + key], key_prefix, mapping_dict)
        elif isinstance(item, list):
            if (key_prefix + key) in new_dict.keys():
                current_dict[key] = update_complex_list(
                    current_dict[key], new_dict[key_prefix + key], key_prefix, mapping_dict)

        else:
            current_dict[key] = new_dict.get(key_prefix + key, item)
            if key in mapping_dict.keys():
                if current_dict[key] is not None:
                    current_dict[key] = mapping_dict[key][current_dict[key]]


def update_complex_list(current_list, new_list, key_prefix, mapping_dict):
    temp_dict = current_list[0]
    current_list = []
    for index, item in enumerate(new_list):
        if isinstance(item, dict):
            update_complex_dict(temp_dict, item, key_prefix, mapping_dict)
            current_list.append(copy.deepcopy(temp_dict))
        elif isinstance(item, list):
            update_complex_list(current_list[index], new_list[
                                index], key_prefix, mapping_dict)
    return current_list


'''
Convert complex dict to a complex etree
Dict format ->
RiskDetails = {
    "Children" : {
        "Block" : {
            "Children" : {
                "PropRisks_RegistrationNumber" : { "Name" : "Registration Number", "Value" : "MH", "Type" : "String", },
                "PropRisks_RegistrationNumber2" : { "Name" : "Registration Number2", "Value" : "01", "Type" : "String", },
                "PropRisks_RegistrationNUmber3" : { "Name" : "Registration Number3", "Value" : "WD", "Type" : "String", },
            },
        },
    },
}
etree = convert_complex_dict_to_complex_etree("RiskDetails", RiskDetails)
Etree generated ->
<RiskDetails>
    <Block>
        <PropRisks_RegistrationNumber Type="String" Name="Registration Number" Value="MH"/>
        <PropRisks_RegistrationNumber2 Type="String" Name="Registration Number2" Value="01"/>
        <PropRisks_RegistrationNumber3 Type="String" Name="Registration Number3" Value="WD"/>
    </Block>
</RiskDetails>
'''


def convert_complex_dict_to_complex_etree(tag, complex_dict):

    elem = etree.Element(tag)
    complex_dict_keys = complex_dict.keys()

    for key in complex_dict_keys:
        if key != 'Children':
            elem.set(key, str(complex_dict[key]))
    if 'Children' in complex_dict_keys:
        children = complex_dict['Children']

        if type(children) is dict:
            for child_tag, child_dict in children.items():
                child_elem = convert_complex_dict_to_complex_etree(
                    child_tag, child_dict)
                elem.append(child_elem)
        if type(children) is list:
            for child in children:
                child_tag = child.keys()[0]
                child_dict = child[child_tag]
                child_elem = convert_complex_dict_to_complex_etree(
                    child_tag, child_dict)
                elem.append(child_elem)
    return elem


def recursive_asdict(d):
    """Convert Suds object into serializable format."""
    out = {}
    for k, v in asdict(d).iteritems():
        if hasattr(v, '__keylist__'):
            out[k] = recursive_asdict(v)
        elif isinstance(v, list):
            out[k] = []
            for item in v:
                if hasattr(item, '__keylist__'):
                    out[k].append(recursive_asdict(item))
                else:
                    out[k].append(item)
        else:
            out[k] = v
    return out


def get_age(date_new, date_old):
    age = relativedelta.relativedelta(date_new, date_old)
    float_age = age.years + float(age.months) / 12 + float(age.days) / 365.2425
    return float_age


def get_voluntary_deductible_discount_dict(vehicle_type, basic_od_premium):
    if vehicle_type == 'Private Car':
        return {
            0: 0,
            2500: max(-0.2 * basic_od_premium, -750),
            5000: max(-0.25 * basic_od_premium, -1500),
            7500: max(-0.3 * basic_od_premium, -2000),
            15000: max(-0.35 * basic_od_premium, -2500),
        }
    else:
        return {
            0: 0,
            500: max(-0.05 * basic_od_premium, -50),
            750: max(-0.1 * basic_od_premium, -75),
            1000: max(-0.15 * basic_od_premium, -125),
            1500: max(-0.20 * basic_od_premium, -200),
            3000: max(-0.25 * basic_od_premium, -250),
        }


def get_pa_passenger_dict(vehicle_type, seating_capacity, ctsc=False):
    if vehicle_type == 'Private Car':
        return {
            "type": "dynamic",
            0: 0,
            10000: 5 * int(seating_capacity),
            50000: 25 * int(seating_capacity),
            100000: 50 * int(seating_capacity),
            200000: 100 * int(seating_capacity),
        }
    else:
        if ctsc:
            return {
                "type": "dynamic",
                0: 0,
                10000: 7 * int(seating_capacity),
                50000: 35 * int(seating_capacity),
                100000: 70 * int(seating_capacity),
                200000: 140 * int(seating_capacity),
            }
        else:
            return {
                0: 0,
                10000: 14,
                50000: 70,
                100000: 140,
                200000: 280,
            }


def is_addon_available(addon_code, insurer_slug, vehicle_model, vehicle_age, rto_code):

    rto = motor_models.RTOMaster.objects.get(rto_code=rto_code)
    addon = motor_models.Addon.objects.get(code=addon_code)

    addon_exclusion_list_for_insurer = motor_models.AddonExclusion.objects.filter(
        insurer__slug=insurer_slug).filter(addon=addon)
    exclusions = addon_exclusion_list_for_insurer.filter(Q(rtos__rto=rto) | Q(
        models__model=vehicle_model) | Q(age_limit__lte=vehicle_age))
    if len(exclusions) > 0:
        return False
    else:
        return True


def add_dependent_addons(request_data, dependent_addons):
    data = copy.deepcopy(request_data)
    for addon, dependent_addon_list in dependent_addons.items():
        if data.get('addon_' + addon, None) == '1':
            data.update({'addon_' + k: '1' for k in dependent_addon_list})
    return data


def add_all_addons(request_data):
    data = copy.deepcopy(request_data)
    data.update({'addon_' + addon.code: '1' for addon in motor_models.Addon.objects.all()})
    data['extra_paPassenger'] = '0'
    data['extra_isLegalLiability'] = '1'
    return data


def get_discount_code(discount_code):
    if discount_code:
        try:
            dc = motor_models.DiscountCode.objects.get(code=discount_code)
            if not dc.is_expired():
                return dc
        except:
            pass
    return None

# ====================================XMLTODICT==================================== #


def recursive_dict(l):
    '''Leave this as it is. Or give me a call @7415838974 and say \{I_killed_batman!\}'''
    d = {}
    return dict([(k2, v2[0] if len(v2) == 1 else v2) for k2, v2 in (
        [d.setdefault(k, []).append(recursive_dict(v) if type(v) is list else v) for k, v in l], d.items())[1]])


node = lambda st: etree.fromstring(st).getroottree().getroot()

xml2tupledlist = lambda n: [
    (e.tag, e.text if e.text else xml2tupledlist(iter(e))) for e in n]

xml2dict = lambda st: recursive_dict(xml2tupledlist(node(st)))
# ====================================XMLTODICT==================================== #

norm = lambda list1, list2: math.sqrt(
    sum(map((lambda a, b: pow((a - b), 2)), list1, list2)))


def get_insurer_plans(plan_map, available_plans,
                      selected_plans,
                      order=motor_models.Addon.objects.all().order_by('id').values_list('code', flat=True)):
    selected = map((lambda e: int(e in selected_plans)), order)
    available = {k: map((lambda e: int(e in v)), order)
                 for k, v in plan_map.items() if k in available_plans}
    print "selected %s" % str(selected)
    print "available %s" % str(available)
    matched_plan = ''
    if available and sum(selected):
        matched_plan = min(map((lambda (k, v): (k, norm(v, selected))),
                               available.items()), key=(lambda (a, b): b))[0]
    return matched_plan

# ============================== convert_premium_response ==================================== #


def cpr_update(_list):  # for a non python dude, its an inplace method
    for each_dict in _list:
        each_dict.update({'premium':   round(float(each_dict['premium']), 2)})
        each_dict.update({'default': int(each_dict['default'])})

        cover_name = COVER_NAME_MAP.get(each_dict['id'], 'Extra Component')
        each_dict['name'] = cover_name
    return _list
# ============================== convert_premium_response ==================================== #


def convert_tuple_to_xmltree(data):
    element = etree.Element(data[0])

    if type(data[1]) is tuple:
        for item in data[1]:
            if item:
                child_element = convert_tuple_to_xmltree(item)
                element.append(child_element)
    elif type(data[1]) is bool:
        element.text = 'true' if data[1] else 'false'
    else:
        element.text = str(data[1])

    if len(data) > 2:
        for item in data[2:]:
            if type(item) == tuple and len(item) >= 2:
                element.set(item[0], item[1])

    return element


def convert_tuple_to_dict(data):
    if type(data[0]) is str:
        data = (data,)
    return {k: convert_tuple_to_dict(v) if type(v) is tuple else v for k, v in data}


def modify_parameters_for_used_vehicle(quote_parameters):
    if quote_parameters.get('isUsedVehicle', '0') == '1':
        quote_parameters['isNewVehicle'] = '0'
        new_policy_date = quote_parameters.get('newPolicyStartDate', None)
        if not new_policy_date:
            past_date = quote_parameters.get('pastPolicyExpiryDate', None)
            if past_date:
                past_date = datetime.datetime.strptime(past_date, '%d-%m-%Y')
                today = datetime.datetime.today()
                if past_date < today:
                    quote_parameters['newPolicyStartDate'] = (today + relativedelta.relativedelta(days=1)).strftime('%d-%m-%Y')
                else:
                    quote_parameters['newPolicyStartDate'] = (past_date + relativedelta.relativedelta(days=1)).strftime('%d-%m-%Y')

        new_policy_start_date = datetime.datetime.strptime(
            quote_parameters['newPolicyStartDate'], "%d-%m-%Y")
        past_policy_expiry_date = new_policy_start_date - datetime.timedelta(days=1)
        quote_parameters[
            'pastPolicyExpiryDate'] = past_policy_expiry_date.strftime("%d-%m-%Y")

        quote_parameters['isClaimedLastYear'] = '1' if quote_parameters.get('isNCBCertificate', '0') == '0' else '0'

        new_to_previous_ncb = {
            '0': '0',
            '20': '0',
            '25': '20',
            '35': '25',
            '45': '35',
            '50': '45',
        }
        quote_parameters['previousNCB'] = new_to_previous_ncb[
            quote_parameters.get('newNCB', '0')]
    else:
        quote_parameters['isUsedVehicle'] = '0'
        quote_parameters['isNCBCertificate'] = '0'
        quote_parameters['newNCB'] = '0'
    return quote_parameters


def modify_response_for_ncb(response, quote_parameters):
    previous_ncb_to_new_ncb = {
        '0': 20,
        '20': 25,
        '25': 35,
        '35': 45,
        '45': 50,
        '50': 50,
    }
    if response:
        if quote_parameters.get('isUsedVehicle', '0') == '1':
            ncb = int(quote_parameters['newNCB']) if quote_parameters[
                'isNCBCertificate'] == '1' else 0
        else:
            if quote_parameters['isNewVehicle'] == '1':
                ncb = 0
            else:
                ncb = 0 if quote_parameters['isClaimedLastYear'] == '1' else previous_ncb_to_new_ncb[
                    quote_parameters['previousNCB']]

        if response.get('NCB', None) is None:
            response['NCB'] = ncb
    return response


def calculate_od_premium(transaction):
    quote_parameters = transaction.quote.raw_data
    vehicle = transaction.quote.vehicle
    vehicle_type = transaction.vehicle_type

    if vehicle_type == 'Private Car':
        tp_map = {
            0: 1468,
            1: 1598,
            2: 4931,
        }
        cc_limits = [1000, 1500]
    else:
        tp_map = {
            0: 519,
            1: 538,
            2: 554,
            3: 884,
        }
        cc_limits = [75, 150, 350]

    cc = vehicle.cc
    cc_slab = 0
    for cc_limit in cc_limits:
        if cc_limit >= cc:
            break
        cc_slab += 1

    tp_premium = tp_map[cc_slab]

    if vehicle_type == 'Private Car':
        tp_premium += 100
    else:
        tp_premium += 50

    if quote_parameters['extra_isLegalLiability'] == '1' and vehicle_type == 'Private Car':
        tp_premium += 50

    if int(quote_parameters['extra_paPassenger']) > 0:
        pa_map = get_pa_passenger_dict(vehicle_type, vehicle.seating_capacity)
        tp_premium += float(pa_map[int(quote_parameters['extra_paPassenger'])])

    if quote_parameters['isCNGFitted'] == '1' and vehicle_type == 'Private Car':
        tp_premium += 60

    od_premium = math.ceil(
        (float(transaction.premium_paid) / 1.14) - tp_premium)
    return od_premium


def remove_namespace_from_dict(data):
    for key in data:
        data_store = data[key]
        key_store = re.sub(r'\{.*\}', '', key)

        del data[key]

        data[key_store] = data_store

        if type(data[key_store]) is dict:
            remove_namespace_from_dict(data[key_store])

    return data
