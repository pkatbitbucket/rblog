import datetime
import os
from dateutil import parser
from xlrd import open_workbook
from xlutils.copy import copy as xlcopy
from dateutil.relativedelta import relativedelta
from django.conf import settings

from motor_product.models import LnTPolicyResponse, Vehicle, VehicleMaster
from motor_product.insurers import insurer_utils

from . import custom_dictionaries as REF

_unpack = lambda lst: {'year': int(lst[2]), 'month': int(lst[1]), 'day': int(lst[0])}
dateit = lambda st: datetime.datetime(*[int(e) for e in st.split('T')[0].split('-')])


def reverse_lookup(dictionary, value):
    for k, v in dictionary.items():
        if value == v:
            return k
    else:
        return ''


def get_misreport_data(Tobj_list):
    misreport_data_list = []
    for Tobj in Tobj_list:
        try:
            lntpolicyresp = LnTPolicyResponse.objects.filter(TxnId=Tobj.reference_id)
            if lntpolicyresp.latest('id').Status == 'UW':
                misreport_data = []
                obj = Tobj.__dict__
                user = obj['raw']['user_form_details']

                temp_obj = lntpolicyresp
                if temp_obj:
                    ProposalNo = (lambda e: e.ProposalNo)(temp_obj.latest('id').TxnStatusDetail)
                else:
                    ProposalNo = ''

                misreport_data.append(ProposalNo)

                CFProposalNo = '5101R2D2' + str(Tobj.id)
                misreport_data.append(CFProposalNo)

                transaction_date = obj['insured_details_json'].get('payment_response', {}).get('TxnDate', '')
                misreport_data.append(transaction_date)

                transaction_id = obj['insured_details_json'].get('payment_response', {}).get('TxnReferenceNo', '')
                misreport_data.append(transaction_id)

                customer_name = user['cust_first_name'].upper() + ' ' + user['cust_last_name'].upper()
                misreport_data.append(customer_name)
                misreport_data.append(user['cust_phone'])
                misreport_data.append(Tobj.id)

                quote_parameters = Tobj.quote.raw_data
                is_new_vehicle = (quote_parameters['isNewVehicle'] == '1')

                Remarks = 'Nil Depreciation'
                is_expired = False
                if not is_new_vehicle:
                    past_policy_end_date = datetime.datetime.strptime(quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")
                    new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)
                    date_of_registration = datetime.datetime.strptime(quote_parameters['registrationDate'], "%d-%m-%Y")
                    vehicle_age = insurer_utils.get_age(new_policy_start_date, date_of_registration)

                    if past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now():
                        is_expired = True

                    if vehicle_age >= 7:
                        Remarks = 'Age>7 Yrs'

                misreport_data.append(Remarks)

                Payment_Type = 'Online Payment'
                if quote_parameters['payment_mode'] == 'CD_ACCOUNT':
                    Payment_Type = 'APD A/C'

                misreport_data.append(Payment_Type)

                Case_Type = 'Rollover'
                if is_new_vehicle:
                    Case_Type = 'New'
                if quote_parameters['isUsedVehicle'] == '1':
                    Case_Type = 'Used'
                if is_expired:
                    Case_Type = 'Expired'

                misreport_data.append(Case_Type)
                # Added premium paid for underwriting cases
                misreport_data.append(Tobj.premium_paid)

                misreport_data_list.append(misreport_data)
        except:
            pass

    return misreport_data_list


def details(Tobj):
    obj = Tobj.__dict__
    user = obj['raw']['user_form_details']
    car = obj['raw']['private_car']
    rollover = car['PastInsuranceVO']['PolicyStartDate']
    if rollover:
        ppsd = dateit(car['PastInsuranceVO']['PolicyStartDate'])
        pped = dateit(car['PastInsuranceVO']['PolicyEndDate'])
        policy_start_date = pped + datetime.timedelta(days=1)
        if pped.replace(hour=23, minute=59, second=0) < datetime.datetime.now():
            policy_start_date = datetime.datetime.now().replace(hour=0, minute=0, second=0)
    else:
        ppsd, pped = "", ""
        policy_start_date = datetime.datetime.strptime(Tobj.quote.raw_data['newPolicyStartDate'], '%d-%m-%Y')

    policy_end_date = (
        lambda e: (e + relativedelta(years=1) - datetime.timedelta(days=1)))(
        policy_start_date)
    legal_liability = bool(float(obj['raw']['quote_raw_response']['MotorLLPaidDriver']))
    if user['add-same'] == 'true' or user['add-same'] == '1':
        vehicle_reg_add = user['add_house_no'] + ' ' + user['add_building_name'] + ' ' + user['add_street_name']
        reg_city_name = user['add_district']
        reg_state = user['add_state']
        reg_pincode = user['add_pincode']
    else:
        vehicle_reg_add = user.get('reg_add_house_no', '') + ' ' + user.get('reg_add_building_name', '')
        vehicle_reg_add = vehicle_reg_add + ' ' + user.get('reg_add_street_name', '')
        # vehicle_reg_add = user['reg_add_house_no'] + ' ' + user['reg_add_building_name'] + ' ' + user[
        #    'reg_add_street_name']
        reg_city_name = user.get('reg_add_district', '')
        reg_state = user.get('reg_add_state', '')
        reg_pincode = user.get('reg_add_pincode', '')
    # --------------------------------------------------------------------------------------------------#
    # ------------------------------Logic for policy number---------------------------------------------#
    temp_obj = LnTPolicyResponse.objects.filter(TxnId='5101R2D2' + str(Tobj.id))
    if temp_obj:
        (PolicyNo, ProposalNo) = (lambda e: (e.PolicyNo, e.ProposalNo))(temp_obj.latest('id').TxnStatusDetail)
    else:
        PolicyNo, ProposalNo = '', ''
    # --------------------------------------------------------------------------------------------------#
    # ---------------------------Logic for car Financier------------------------------------------------#
    carfinancier = ''
    if user.get('is_car_financed', '0') == '1' or user.get('is_car_financed', 'false') == 'true':
        try:
            carfinancier = REF.FINANCIER_LIST[user['car-financier']]
        except KeyError:
            carfinancier = user.get('car-financier-text', '') or user.get('car-financier', '')
    # --------------------------------------------------------------------------------------------------#
    # insurer_address = car['PastInsuranceVO']['InsurerAddress']
    # rto_state = RTOInsurer.objects.filter(insurer__slug='l-t').get(rto_code=rto_code).rtomaster_set.all()[0].rto_code
    # insurer_city = RTOMaster.objects.get(rto_code=rto_name).insurer_rtos.get(insurer__slug='l-t').rto_name
    try:
        variant = Vehicle.objects.get(id=Tobj.insured_vehicle.raw['vehicle']).variant
    except Vehicle.DoesNotExist:
        variant = VehicleMaster.objects.get(id=Tobj.insured_vehicle.raw['vehicle']).variant

    try:
        past_policy_insurer = REF.PAST_INSURANCE_ID_LIST[REF.REV_PASTINSURER_MAP[str(user.get('past_policy_insurer', None))]]
    except:
        past_policy_insurer = ''
    table = [
        ("Sr No", ""),
        ("Channel", "Coverfox"),
        ("source (weblink)", "Website"),
        ("cluster taken (weblink)", "Mumbai"),
        ("Quoteno", ""),
        ("Document Type", "Policy"),
        ("Policy_Cover_Type", REF.COVER_TYPE_LIST[car['CoverTypeVORef']]),
        ("Proposal For", "Rollover"),
        ("Vehicle Type", "Private Car"),
        ("Product Code", "5101"),
        ("Product Description", "Standard Motor Product for Private Cars"),
        ("Proposal Entry Date", datetime.datetime.now().strftime('%d %b %Y')),
        ("Reqeust Received Date", datetime.datetime.now().strftime('%d %b %Y')),
        ("Type of Intermediary", "Direct"),
        ("Agent Code", ""),
        ("Agent Name", ""),
        ("Agent_Contact Number", ""),
        ("SM Code", ""),
        ("SM Name", ""),
        ("Customer_Type", reverse_lookup(REF.ENTITY_TYPE_ID, obj['raw']['contact']['EntityTypeVORef'])),
        ("Insured Title", reverse_lookup(REF.TITLE_ID, obj['raw']['contact']['TitleVORef'])),
        ("Insured First Name", user['cust_first_name']),
        ("Insured Middle Name", ''),
        ("Insured Last Name", user['cust_last_name']),
        ("Gender", user['cust_gender']),
        ("Date of Birth", dateit(obj['raw']['contact']['DateOfBirth']).strftime('%d %b %Y')),
        ("Father's Name", 'Mr. ' + user['cust_last_name']),
        ("House No", user['add_house_no']),
        ("Building Name", user['add_building_name']),
        ("Street Name", user['add_street_name']),
        ("Landmark1", user['add_landmark']),
        ("LandMark2", ""),
        ("city name", user['add_district']),
        ("State", user['add_state']),
        ("Pincode", user['add_pincode']),
        ("contactno1", ""),
        ("Mobile No", user['cust_phone']),
        ("Email1", user['cust_email']),
        ("Email2", ""),
        ("Pan No", ""),
        ("vehicle registration address", vehicle_reg_add),
        ("vlandmark1", ""),
        ("vlandmark2", ""),
        ("Reg_City Name", reg_city_name),
        ("Reg_State", reg_state),
        ("Reg_Pincode", reg_pincode),
        ("pol inception time", ""),
        ("pol inception date", policy_start_date.strftime('%d %b %Y')),
        ("pol end date", policy_end_date.strftime('%d %b %Y')),
        ("vehicle make", car['MakeDsc']),
        ("model", car['ModelDsc']),
        ("variant", variant),
        ("regno", user['vehicle_reg_no']),
        ("reg date", dateit(car['RegistrationDate']).strftime('%d %b %Y')),
        ("rto location", obj['raw']['private_car']['RTOLocationVORef']),
        ("Month & Year of Manufacture",
         '%s, %s' % (reverse_lookup(REF.MANUFACTURE_MONTH_ID, car['ManufactureMonthVORef']), car['ModelYear'])),
        ("Eng_no", user['vehicle_engine_no']),
        ("Chassis_no", user['vehicle_chassis_no']),
        ("CC/HP/GVW", car['Cc']),
        ("trailer regno (if trailer requested)", ""),
        ("seating capacity", car['SeatingCapacity']),
        ("insured declared value", car['Idv']),
        ("idv for electrical accessories", car['IDVElectrical']),
        ("idv for Non Electrical Accessoreis (other than factory fitted)", car['IDVNonElectrical']),
        ("IDV value of BI Fuel / CNG / LPG Kit", car['BIFuelKitValue']),
        ("trailer idv", ""),
        ("total idv", sum([int(car[e]) for e in ['Idv', 'IDVElectrical', 'IDVNonElectrical', 'BIFuelKitValue']])),
        ("net premium", ''),
        ("service tax", ''),
        ("total premium", float(obj['premium_paid'])),
        ("Premium for Electrical Accessories", obj['raw']['quote_raw_response']['MotorEEAccessoriesNetPremium']),
        ("premium for non electrical accessories",
         obj['raw']['quote_raw_response']['MotorNonElectricalAccessoriesNetPremium']),
        ("Depreciation Cover", {'0': 'No', '1': 'Yes'}[car['DepreciationWaiverFlagRef']]),
        ("Return to Invoice (applicable only for First Registered Owner only)", "no"),
        ("NCB Protection", {'0': 'No', '1': 'Yes'}[obj['raw']['quote_raw_response']['addon_isNcbProtection']]),
        ("Drive Through Protection for Engine", {'0': 'No', '1': 'Yes'}[car['IsDriveThroughProtectionForEngineRef']]),
        ("Key Replacement", "no"),
        ("Cash Allowance", "no"),
        ("Additional PA ot OD, SI", ''),
        ("Additional PA unnammed SI", Tobj.quote.raw_data['extra_paPassenger']),
        ("City where vehicle is used", user['add_district']),
        ("Vehicle Usage", "private"),
        ("Are you member of an automobile association?", "NO"),
        ("if so Membership No", ""),
        ("Expiry Date", ""),
        ("Association name", ""),
        ("Is the vehicle to be insured Imported?", ""),
        ("Is the vehicle specially designed for disabled?", ""),
        ("Is the vehicle fitted with Fibre Glass Fuel Tank?", ""),
        ("Is the vehicle fitted with any Anti-theft device approved by ARAI", "NO"),
        ("Are you an employee of L&T group?", "NO"),
        ("if yes emp No.", ""),
        ("Division name", ""),
        ("do you wish to restrict the damage to third party property damage to statutory limit of 6000?", ""),
        ("Do you want to opt for Geographical Extension?", ""),
        ("If yes, Please select", ""),
        ("Do you want PA cover for owner driver?", {'0': 'No', '1': 'Yes'}[car['PaCoverForOwnerDriverFlagVORef']]),
        ("Nominee Name", user['nominee_name']),
        ("Nominee Age", user['nominee_age']),
        ("Relationship", user['nominee_relationship']),
        ("Appintee Name((If Nominee is Minor))", ""),
        ("Relation with nominee", ""),
        ("Do you want unnamed PA cover?", {'0': 'No', '1': 'Yes'}[car['CoverUnnamedFlagVORef']]),
        ("If Yes No. of Unnammed passenger",
         (car['SeatingCapacity'] if (car.get('CoverUnnamedFlagVORef', '0') == '1') else '0')),
        ("Sum_Insured", reverse_lookup(REF.UNNAMED_SI_LTG_ID, car['PAUnnamedPersonSIVORef'])),
        ("Do you want named PA cover", ""),
        ("Named Passenger_1", ""),
        ("Nominee Name_1", ""),
        ("Relationship_1", ""),
        ("Sum_Insured_1", ""),
        ("Named Passenger_2", ""),
        ("Nominee Name_2", ""),
        ("Relationship2", ""),
        ("SumInsured2", ""),
        ("Named Passenger_3", ""),
        ("Nominee Name_3", ""),
        ("Relationship3", ""),
        ("SumInsured3", ""),
        ("Named Passenger_4", ""),
        ("Nominee Name_4", ""),
        ("Relationship4", ""),
        ("SumInsured4", ""),
        ("Do you want to cover legal liability for paid drivers", {True: "Yes", False: "No"}[legal_liability]),
        ("No of Paid Driver", ("1" if legal_liability else "0")),
        ("Specify Sum Insured per paid driver:", ""),
        ("Do you want to cover legal liability  for other employed", ""),
        ("voluntary excess/deductible", Tobj.quote.raw_data['voluntaryDeductible']),
        # MotorVoluntaryDeductibleNetPremium
        ("Type of Financier", ""),
        ("Financer_Name", carfinancier),
        ("Financer_Address", ""),
        ("Financer_City", ""),
        ("Previous Year NCB %", reverse_lookup(REF.NCB_PVT_CAR_ID, car['PreviousYearNCBVORef'])),
        ("Previous Year Policy or Cover Note Number", car['PastInsuranceVO']['PolicyNo']),
        ("Previous Year Policy Type", "comprehensive"),
        ("Previous Year Insurance Company Name", past_policy_insurer),
        ("Previous Year Policy Start Date", ppsd.strftime('%d %b %Y') if ppsd else ""),
        ("Previous Year Policy END Date", pped.strftime('%d %b %Y') if pped else ""),
        ("Previous Year Insurer Address", user['add_district']),
        ("Previous Year Insurer City", user['add_district']),
        ("Previous Year Insurer State", user['add_state']),
        ("Previous Year Insurer State Pncode", user['add_pincode']),
        ("Claims History in last 3 years_Year 1", ''),
        ("Claims History in last year", {'1': 'Yes', '0': 'No'}[Tobj.quote.raw_data['isClaimedLastYear']]),
        ("Payment Mode", "Online"),
        ("CMS Slip _Number/ ATOM_Transaction No/CC App. No",
         obj['insured_details_json'].get('payment_response', {}).get('TxnReferenceNo', '')),
        ("CMS_Date/Transaction Date", obj['insured_details_json'].get('payment_response', {}).get('TxnDate', '')),
        ("Premium_Amount", str(Tobj.premium_paid)),
        ("Instrumnet_or_DD_Number_or_Credit Card_Number", ""),
        ("Instrumnet_or_DD_Amount_or_Credit card_Amount", ""),
        ("Instrumnet_or_DD_Date_Credit Card_date", ""),
        ("Doc received by ops for processing", ""),
        ("contact id", ""),
        ("inward number", ""),
        ("Receiptno", ""),
        ("policy status", ""),
        ("policy no", PolicyNo),
        ("policy posted date and time", ""),
        ("proposal no", ""),
        ("request received from - team", ""),
        ("Remarks", ""),
        ("Base rate variation", ""),
        ("UW discount", ""),
        ("LeadID", ""),
        ("Policy Number", PolicyNo),
    ]
    head_keys, row_values = [], []
    for e in table:
        head_keys.append(e[0])
        row_values.append(e[1])

    filename = os.path.join(
        'uploads', 'lntedi', '%s_%s.xls' % (user['cust_first_name'], datetime.datetime.now().strftime('%d%b%Y_%H%M%S'))
    )
    return filename, head_keys, [row_values, ], ProposalNo


def offline_mis_report(Tobj_list):
    row_values_list = []
    head_keys, row_values = [], []
    for Tobj in Tobj_list:
        if Tobj.quote.raw_data['isNewVehicle'] == '1':
            continue

        obj = Tobj.__dict__
        car = obj['raw']['private_car']
        is_used_vehicle = (Tobj.quote.raw_data['isUsedVehicle'] == '1')

        is_expired = False
        if not is_used_vehicle:
            pped = parser.parse(car['PastInsuranceVO']['PolicyEndDate']).date()
            npst = parser.parse(obj['raw']['private_car']['PolicyStartDate']).date()
            is_expired = (npst - pped) > datetime.timedelta(days=1)

        temp_obj = LnTPolicyResponse.objects.filter(TxnId='5101R2D2' + str(Tobj.id))
        if temp_obj:
            PolicyNo, ProposalNo = (lambda e: (e.PolicyNo, e.ProposalNo))(temp_obj.latest('id').TxnStatusDetail)
        else:
            PolicyNo, ProposalNo = '', ''

        if is_expired or is_used_vehicle:
            table = [
                ("Policy No", PolicyNo),
                ("Proposal No", ProposalNo),
                ("Customer Name", Tobj.proposer.first_name + " " + Tobj.proposer.last_name),
                ("Issuance date", Tobj.payment_on.strftime('%d %b %Y')),
            ]
            head_keys, row_values = [], []
            for e in table:
                head_keys.append(e[0])
                row_values.append(e[1])

            row_values_list.append(row_values)
        else:
            continue

    lday = datetime.datetime.now() - datetime.timedelta(days=1)

    filename = os.path.join(settings.MEDIA_ROOT, 'uploads', 'lntedi',
                            'OFFLINE_CASES_MIS_%s_%s_%s.xls' % (lday.day, lday.month, lday.year))
    return filename, head_keys, row_values_list


def detail_list(Tobj_list):
    row_values_list = []
    for Tobj in Tobj_list:
        obj = Tobj.__dict__
        user = obj['raw']['user_form_details']
        car = obj['raw']['private_car']
        rollover = car['PastInsuranceVO']['PolicyStartDate']
        if rollover:
            ppsd = dateit(car['PastInsuranceVO']['PolicyStartDate'])
            pped = dateit(car['PastInsuranceVO']['PolicyEndDate'])
        #    policy_start_date = pped + datetime.timedelta(days=1)
        #    if pped.replace(hour=23, minute=59, second=0) < datetime.datetime.now():
        #        policy_start_date = datetime.datetime.now().replace(hour=0, minute=0, second=0)
        else:
            ppsd, pped = "", ""
        #    policy_start_date = datetime.datetime.strptime(Tobj.quote.raw_data['newPolicyStartDate'], '%d-%m-%Y')

        policy_start_date = Tobj.policy_start_date
        policy_end_date = Tobj.policy_end_date

        # policy_end_date = (
        #    lambda e: (datetime.datetime(year=e.year + 1, month=e.month, day=e.day) - datetime.timedelta(days=1)))(
        #    policy_start_date)
        legal_liability = bool(float(obj['raw']['quote_raw_response']['MotorLLPaidDriver']))
        if user['add-same'] == 'true' or user['add-same'] == '1':
            vehicle_reg_add = user['add_house_no'] + ' ' + user['add_building_name'] + ' ' + user['add_street_name']
            reg_city_name = user['add_district']
            reg_state = user['add_state']
            reg_pincode = user['add_pincode']
        else:
            vehicle_reg_add = user.get('reg_add_house_no', '') + ' ' + user.get('reg_add_building_name', '')
            vehicle_reg_add = vehicle_reg_add + ' ' + user.get('reg_add_street_name', '')
            # vehicle_reg_add = user['reg_add_house_no'] + ' ' + user['reg_add_building_name'] + ' ' + user[
            #    'reg_add_street_name']
            reg_city_name = user.get('reg_add_district', '')
            reg_state = user.get('reg_add_state', '')
            reg_pincode = user.get('reg_add_pincode', '')
        # --------------------------------------------------------------------------------------------------#
        # ------------------------------Logic for policy number---------------------------------------------#
        temp_obj = LnTPolicyResponse.objects.filter(TxnId='5101R2D2' + str(Tobj.id))
        if temp_obj:
            (PolicyNo, ProposalNo) = (lambda e: (e.PolicyNo, e.ProposalNo))(temp_obj.latest('id').TxnStatusDetail)
        else:
            PolicyNo, ProposalNo = '', ''
        # --------------------------------------------------------------------------------------------------#
        # ---------------------------Logic for car Financier------------------------------------------------#
        carfinancier = ''
        if user.get('is_car_financed', '0') == '1' or user.get('is_car_financed', '0') == 'true':
            carfinancier = REF.FINANCIER_LIST[user['car-financier']]
        # --------------------------------------------------------------------------------------------------#
        # insurer_address = car['PastInsuranceVO']['InsurerAddress']
        # rto_state = RTOInsurer.objects.filter(insurer__slug='l-t').get(rto_code=rto_code).rtomaster_set.all()[0].rto_code
        # insurer_city = RTOMaster.objects.get(rto_code=rto_name).insurer_rtos.get(insurer__slug='l-t').rto_name
        try:
            variant = Vehicle.objects.get(id=Tobj.insured_vehicle.raw['vehicle']).variant
        except Vehicle.DoesNotExist:
            variant = VehicleMaster.objects.get(id=Tobj.insured_vehicle.raw['vehicle']).variant

        table = [
            ("Sr No", ""),
            ("Channel", "Coverfox"),
            ("source (weblink)", "Website"),
            ("cluster taken (weblink)", "Mumbai"),
            ("Quoteno", ""),
            ("Document Type", "Policy"),
            ("Policy_Cover_Type", REF.COVER_TYPE_LIST[car['CoverTypeVORef']]),
            ("Proposal For", "Rollover"),
            ("Vehicle Type", "Private Car"),
            ("Product Code", "5101"),
            ("Product Description", "Standard Motor Product for Private Cars"),
            ("Proposal Entry Date", (datetime.datetime.now() - datetime.timedelta(days=1)).strftime('%d %b %Y')),
            ("Reqeust Received Date", (datetime.datetime.now() - datetime.timedelta(days=1)).strftime('%d %b %Y')),
            ("Type of Intermediary", "Direct"),
            ("Agent Code", ""),
            ("Agent Name", ""),
            ("Agent_Contact Number", ""),
            ("SM Code", ""),
            ("SM Name", ""),
            ("Customer_Type", reverse_lookup(REF.ENTITY_TYPE_ID, obj['raw']['contact']['EntityTypeVORef'])),
            ("Insured Title", reverse_lookup(REF.TITLE_ID, obj['raw']['contact']['TitleVORef'])),
            ("Insured First Name", user['cust_first_name']),
            ("Insured Middle Name", ''),
            ("Insured Last Name", user['cust_last_name']),
            ("Gender", user['cust_gender']),
            ("Date of Birth", dateit(obj['raw']['contact']['DateOfBirth']).strftime('%d %b %Y')),
            ("Father's Name", 'Mr. ' + user['cust_last_name']),
            ("House No", user['add_house_no']),
            ("Building Name", user['add_building_name']),
            ("Street Name", user['add_street_name']),
            ("Landmark1", user['add_landmark']),
            ("LandMark2", ""),
            ("city name", user['add_district']),
            ("State", user['add_state']),
            ("Pincode", user['add_pincode']),
            ("contactno1", ""),
            ("Mobile No", user['cust_phone']),
            ("Email1", user['cust_email']),
            ("Email2", ""),
            ("Pan No", ""),
            ("vehicle registration address", vehicle_reg_add),
            ("vlandmark1", ""),
            ("vlandmark2", ""),
            ("Reg_City Name", reg_city_name),
            ("Reg_State", reg_state),
            ("Reg_Pincode", reg_pincode),
            ("pol inception time", ""),
            ("pol inception date", policy_start_date.strftime('%d %b %Y')),
            ("pol end date", policy_end_date.strftime('%d %b %Y')),
            ("vehicle make", car['MakeDsc']),
            ("model", car['ModelDsc']),
            ("variant", variant),
            ("regno", user['vehicle_reg_no']),
            ("reg date", dateit(car['RegistrationDate']).strftime('%d %b %Y')),
            ("rto location", obj['raw']['private_car']['RTOLocationVORef']),
            ("Month & Year of Manufacture",
             '%s, %s' % (reverse_lookup(REF.MANUFACTURE_MONTH_ID, car['ManufactureMonthVORef']), car['ModelYear'])),
            ("Eng_no", user['vehicle_engine_no']),
            ("Chassis_no", user['vehicle_chassis_no']),
            ("CC/HP/GVW", car['Cc']),
            ("trailer regno (if trailer requested)", ""),
            ("seating capacity", car['SeatingCapacity']),
            ("insured declared value", car['Idv']),
            ("idv for electrical accessories", car['IDVElectrical']),
            ("idv for Non Electrical Accessoreis (other than factory fitted)", car['IDVNonElectrical']),
            ("IDV value of BI Fuel / CNG / LPG Kit", car['BIFuelKitValue']),
            ("trailer idv", ""),
            ("total idv", sum([int(car[e]) for e in ['Idv', 'IDVElectrical', 'IDVNonElectrical', 'BIFuelKitValue']])),
            ("net premium", ''),
            ("service tax", ''),
            ("total premium", float(obj['premium_paid'])),
            ("Premium for Electrical Accessories", obj['raw']['quote_raw_response']['MotorEEAccessoriesNetPremium']),
            ("premium for non electrical accessories",
             obj['raw']['quote_raw_response']['MotorNonElectricalAccessoriesNetPremium']),
            ("Depreciation Cover", {'0': 'No', '1': 'Yes'}[car['DepreciationWaiverFlagRef']]),
            ("Return to Invoice (applicable only for First Registered Owner only)", "no"),
            ("NCB Protection", {'0': 'No', '1': 'Yes'}[obj['raw']['quote_raw_response']['addon_isNcbProtection']]),
            ("Drive Through Protection for Engine", {'0': 'No', '1': 'Yes'}[car['IsDriveThroughProtectionForEngineRef']]),
            ("Key Replacement", "no"),
            ("Cash Allowance", "no"),
            ("Additional PA ot OD, SI", ''),
            ("Additional PA unnammed SI", Tobj.quote.raw_data['extra_paPassenger']),
            ("City where vehicle is used", user['add_district']),
            ("Vehicle Usage", "private"),
            ("Are you member of an automobile association?", "NO"),
            ("if so Membership No", ""),
            ("Expiry Date", ""),
            ("Association name", ""),
            ("Is the vehicle to be insured Imported?", ""),
            ("Is the vehicle specially designed for disabled?", ""),
            ("Is the vehicle fitted with Fibre Glass Fuel Tank?", ""),
            ("Is the vehicle fitted with any Anti-theft device approved by ARAI", "NO"),
            ("Are you an employee of L&T group?", "NO"),
            ("if yes emp No.", ""),
            ("Division name", ""),
            ("do you wish to restrict the damage to third party property damage to statutory limit of 6000?", ""),
            ("Do you want to opt for Geographical Extension?", ""),
            ("If yes, Please select", ""),
            ("Do you want PA cover for owner driver?", {'0': 'No', '1': 'Yes'}[car['PaCoverForOwnerDriverFlagVORef']]),
            ("Nominee Name", user['nominee_name']),
            ("Nominee Age", user['nominee_age']),
            ("Relationship", user['nominee_relationship']),
            ("Appintee Name((If Nominee is Minor))", ""),
            ("Relation with nominee", ""),
            ("Do you want unnamed PA cover?", {'0': 'No', '1': 'Yes'}[car['CoverUnnamedFlagVORef']]),
            ("If Yes No. of Unnammed passenger",
             (car['SeatingCapacity'] if (car.get('CoverUnnamedFlagVORef', '0') == '1') else '0')),
            ("Sum_Insured", reverse_lookup(REF.UNNAMED_SI_LTG_ID, car['PAUnnamedPersonSIVORef'])),
            ("Do you want named PA cover", ""),
            ("Named Passenger_1", ""),
            ("Nominee Name_1", ""),
            ("Relationship_1", ""),
            ("Sum_Insured_1", ""),
            ("Named Passenger_2", ""),
            ("Nominee Name_2", ""),
            ("Relationship2", ""),
            ("SumInsured2", ""),
            ("Named Passenger_3", ""),
            ("Nominee Name_3", ""),
            ("Relationship3", ""),
            ("SumInsured3", ""),
            ("Named Passenger_4", ""),
            ("Nominee Name_4", ""),
            ("Relationship4", ""),
            ("SumInsured4", ""),
            ("Do you want to cover legal liability for paid drivers", {True: "Yes", False: "No"}[legal_liability]),
            ("No of Paid Driver", ("1" if legal_liability else "0")),
            ("Specify Sum Insured per paid driver:", ""),
            ("Do you want to cover legal liability  for other employed", ""),
            ("voluntary excess/deductible", Tobj.quote.raw_data['voluntaryDeductible']),
            # MotorVoluntaryDeductibleNetPremium
            ("Type of Financier", ""),
            ("Financer_Name", carfinancier),
            ("Financer_Address", ""),
            ("Financer_City", ""),
            ("Previous Year NCB %", reverse_lookup(REF.NCB_PVT_CAR_ID, car['PreviousYearNCBVORef'])),
            ("Previous Year Policy or Cover Note Number", car['PastInsuranceVO']['PolicyNo']),
            ("Previous Year Policy Type", "comprehensive"),
            ("Previous Year Insurance Company Name",
             REF.PAST_INSURANCE_ID_LIST[REF.REV_PASTINSURER_MAP[str(user['past_policy_insurer'])]]),
            ("Previous Year Policy Start Date", ppsd.strftime('%d %b %Y') if ppsd else ""),
            ("Previous Year Policy END Date", pped.strftime('%d %b %Y') if pped else ""),
            ("Previous Year Insurer Address", user['add_district']),
            ("Previous Year Insurer City", user['add_district']),
            ("Previous Year Insurer State", user['add_state']),
            ("Previous Year Insurer State Pncode", user['add_pincode']),
            ("Claims History in last 3 years_Year 1", ''),
            ("Claims History in last year", {'1': 'Yes', '0': 'No'}[Tobj.quote.raw_data['isClaimedLastYear']]),
            ("Payment Mode", "Online"),
            ("CMS Slip _Number/ ATOM_Transaction No/CC App. No",
             obj['insured_details_json'].get('payment_response', {}).get('TxnReferenceNo', '')),
            ("CMS_Date/Transaction Date", obj['insured_details_json'].get('payment_response', {}).get('TxnDate', '')),
            ("Premium_Amount", str(Tobj.premium_paid)),
            ("Instrumnet_or_DD_Number_or_Credit Card_Number", ""),
            ("Instrumnet_or_DD_Amount_or_Credit card_Amount", ""),
            ("Instrumnet_or_DD_Date_Credit Card_date", ""),
            ("Doc received by ops for processing", ""),
            ("contact id", ""),
            ("inward number", ""),
            ("Receiptno", ""),
            ("policy status", ""),
            ("policy no", PolicyNo),
            ("policy posted date and time", ""),
            ("proposal no", ""),
            ("request received from - team", ""),
            ("Remarks", ""),
            ("Base rate variation", ""),
            ("UW discount", ""),
            ("LeadID", ""),
            ("Policy Number", PolicyNo),
        ]
        head_keys, row_values = [], []
        for e in table:
            head_keys.append(e[0])
            row_values.append(e[1])

        row_values_list.append(row_values)

    lday = datetime.datetime.now() - datetime.timedelta(days=1)

    filename = os.path.join(settings.MEDIA_ROOT, 'uploads', 'lntedi',
                            'EDI_FILE_%s_%s_%s.xls' % (lday.day, lday.month, lday.year))
    return filename, head_keys, row_values_list


def offline_closure(request_data):
    error = False
    errormsg = ''
    key_parameters = ['proposal_for', 'cust_type', 'insured_title', 'cust_first_name', 'cust_last_name',
                      'cust_gender', 'cust_dob', 'cust_father_name', 'add_house_no', 'add_building_name',
                      'add_street_name',
                      'add_landmark', 'add_district', 'add_state', 'add_pincode', 'cust_phone', 'cust_email',
                      'same_contact_reg_add',
                      'policy_incep_date', 'vehicle_make', 'vehicle_model', 'vehicle_variant', 'vehicle_reg_no',
                      'RegistrationDate',
                      'rto_location', 'manuf_month_year', 'vehicle_engine_no', 'vehicle_chassis_no', 'Cc',
                      'SeatingCapacity', 'Idv', 'IDVElectrical',
                      'IDVNonElectrical', 'BIFuelKitValue', 'total_premium', 'dep_cover', 'return_to_invoice',
                      'ncb_protection', 'drive_thru_protect',
                      'key_replace', 'cash_allow', 'nominee_name', 'nominee_age', 'nominee_relation',
                      'CoverUnnamedFlagVORef', 'sum_insured',
                      'legal_liability', 'vol_excess', 'financier', 'prev_year_ncb', 'prev_year_policy_number',
                      'prev_year_ins_comp',
                      'prev_year_pol_startdate', 'prev_year_pol_enddate', 'prev_year_ins_district',
                      'prev_year_ins_city', 'prev_year_ins_state',
                      'prev_year_ins_pincode', 'transaction_num', 'transaction_date', 'premium_amount',
                      ]

    reg_address_parameters = ['vehicle_reg_add', 'reg_city_name', 'reg_state', 'reg_pincode']

    for key in key_parameters:
        if key not in request_data or request_data[key].strip() == '':
            error = True
            errormsg = "Please fill all the data in the given form."
            break

    if not int(request_data['same_contact_reg_add']):
        for key in reg_address_parameters:
            if key not in request_data or request_data[key].strip() == '':
                error = True
                errormsg = "Please fill all the data in the given form."
                break

        if not error:
            vehicle_reg_add = request_data['vehicle_reg_add']
            reg_city_name = request_data['reg_city_name']
            reg_state = request_data['reg_state']
            reg_pincode = request_data['reg_pincode']
    else:
        vehicle_reg_add = request_data['add_house_no'] + ', ' + request_data['add_building_name'] + ', ' + request_data[
            'add_street_name'] + ', ' + request_data['add_landmark']
        reg_city_name = request_data['add_district']
        reg_state = request_data['add_state']
        reg_pincode = request_data['add_pincode']

    if error:
        return error, errormsg, '', [], []  # if error passing blank file name, header and values for excel data
    else:
        start_date = datetime.datetime.strptime(request_data['policy_incep_date'], '%m/%d/%Y')
        same_date_next_year = datetime.datetime(year=start_date.year + 1, month=start_date.month, day=start_date.day)
        policy_end_date = (same_date_next_year - datetime.timedelta(days=1)).strftime('%d/%m/%Y')
        table = [
            ("Sr No", ""),
            ("Channel", "Coverfox"),
            ("source (weblink)", "Website"),
            ("cluster taken (weblink)", "Mumbai"),
            ("Quoteno", ""),
            ("Document Type", "Policy"),
            ("Policy_Cover_Type", "Package"),
            ("Proposal For", request_data['proposal_for']),
            ("Vehicle Type", "Private Car"),
            ("Product Code", "5101"),
            ("Product Description", "Standard Motor Product for Private Cars"),
            ("Proposal Entry Date", datetime.datetime.now().strftime('%d/%m/%Y')),
            ("Reqeust Received Date", datetime.datetime.now().strftime('%d/%m/%Y')),  # '%d %b %Y'
            ("Type of Intermediary", "Direct"),
            ("Agent Code", ""),
            ("Agent Name", ""),
            ("Agent_Contact Number", ""),
            ("SM Code", ""),
            ("SM Name", ""),
            ("Customer_Type", request_data['cust_type']),
            ("Insured Title", request_data['insured_title']),
            ("Insured First Name", request_data['cust_first_name']),
            ("Insured Middle Name", request_data['cust_middle_name']),
            ("Insured Last Name", request_data['cust_last_name']),
            ("Gender", request_data['cust_gender']),
            ("Date of Birth", datetime.datetime.strptime(request_data['cust_dob'], '%m/%d/%Y').strftime('%d/%m/%Y')),
            ("Father's Name", 'Mr. ' + request_data['cust_father_name']),
            ("House No", request_data['add_house_no']),
            ("Building Name", request_data['add_building_name']),
            ("Street Name", request_data['add_street_name']),
            ("Landmark1", request_data['add_landmark']),
            ("LandMark2", ""),
            ("city name", request_data['add_district']),
            ("State", request_data['add_state']),
            ("Pincode", request_data['add_pincode']),
            ("contactno1", ""),
            ("Mobile No", request_data['cust_phone']),
            ("Email1", request_data['cust_email']),
            ("Email2", ""),
            ("Pan No", ""),
            ("vehicle registration address", vehicle_reg_add),
            ("vlandmark1", ""),
            ("vlandmark2", ""),
            ("Reg_City Name", reg_city_name),
            ("Reg_State", reg_state),
            ("Reg_Pincode", reg_pincode),
            ("pol inception time", ""),
            ("pol inception date",
             datetime.datetime.strptime(request_data['policy_incep_date'], '%m/%d/%Y').strftime('%d/%m/%Y')),
            ("pol end date", datetime.datetime.strptime(policy_end_date, '%d/%m/%Y').strftime('%d/%m/%Y')),
            # policy_end_date.strftime('%d %b %Y')),
            ("vehicle make", request_data['vehicle_make']),
            ("model", request_data['vehicle_model']),
            ("variant", request_data['vehicle_variant']),
            ("regno", request_data['vehicle_reg_no']),
            ("reg date", datetime.datetime.strptime(request_data['RegistrationDate'], '%m/%d/%Y').strftime('%d/%m/%Y')),
            ("rto location", request_data['rto_location']),
            ("Month & Year of Manufacture", request_data['manuf_month_year']),
            ("Eng_no", request_data['vehicle_engine_no']),
            ("Chassis_no", request_data['vehicle_chassis_no']),
            ("CC/HP/GVW", request_data['Cc']),
            ("trailer regno (if trailer requested)", ""),
            ("seating capacity", request_data['SeatingCapacity']),
            ("insured declared value", request_data['Idv']),
            ("idv for electrical accessories", request_data['IDVElectrical']),
            ("idv for Non Electrical Accessoreis (other than factory fitted)", request_data['IDVNonElectrical']),
            ("IDV value of BI Fuel / CNG / LPG Kit", request_data['BIFuelKitValue']),
            ("trailer idv", ""),
            ("total idv",
             str(sum([int(request_data[e]) for e in ['Idv', 'IDVElectrical', 'IDVNonElectrical', 'BIFuelKitValue']]))),
            ("net premium", ''),
            ("service tax", ''),
            ("total premium", request_data['total_premium']),
            ("Premium for Electrical Accessories", ''),
            ("premium for non electrical accessories", ''),
            ("Depreciation Cover", request_data['dep_cover']),
            ("Return to Invoice (applicable only for First Registered Owner only)", request_data['return_to_invoice']),
            ("NCB Protection", request_data['ncb_protection']),
            ("Drive Through Protection for Engine", request_data['drive_thru_protect']),
            ("Key Replacement", request_data['key_replace']),
            ("Cash Allowance", request_data['cash_allow']),
            ("Additional PA ot OD, SI", ''),
            ("Additional PA unnammed SI", ''),
            ("City where vehicle is used", ''),
            ("Vehicle Usage", "private"),
            ("Are you member of an automobile association?", ""),
            ("if so Membership No", ""),
            ("Expiry Date", ""),
            ("Association name", ""),
            ("Is the vehicle to be insured Imported?", ""),
            ("Is the vehicle specially designed for disabled?", ""),
            ("Is the vehicle fitted with Fibre Glass Fuel Tank?", ""),
            ("Is the vehicle fitted with any Anti-theft device approved by ARAI", ""),
            ("Are you an employee of L&T group?", ""),
            ("if yes emp No.", ""),
            ("Division name", ""),
            ("do you wish to restrict the damage to third party property damage to statutory limit of 6000?", ""),
            ("Do you want to opt for Geographical Extension?", ""),
            ("If yes, Please select", ""),
            ("Do you want PA cover for owner driver?", "Yes"),
            ("Nominee Name", request_data['nominee_name']),
            ("Nominee Age", request_data['nominee_age']),
            ("Relationship", request_data['nominee_relation']),
            ("Appintee Name((If Nominee is Minor))", ""),
            ("Relation with nominee", ""),
            ("Do you want unnamed PA cover?", request_data['CoverUnnamedFlagVORef']),
            ("If Yes No. of Unnammed passenger",
             (request_data['SeatingCapacity'] if (request_data.get('CoverUnnamedFlagVORef', 'No') == 'Yes') else '0')),
            ("Sum_Insured", request_data['sum_insured']),  # need to check
            ("Do you want named PA cover", ""),
            ("Named Passenger_1", ""),
            ("Nominee Name_1", ""),
            ("Relationship_1", ""),
            ("Sum_Insured_1", ""),
            ("Named Passenger_2", ""),
            ("Nominee Name_2", ""),
            ("Relationship2", ""),
            ("SumInsured2", ""),
            ("Named Passenger_3", ""),
            ("Nominee Name_3", ""),
            ("Relationship3", ""),
            ("SumInsured3", ""),
            ("Named Passenger_4", ""),
            ("Nominee Name_4", ""),
            ("Relationship4", ""),
            ("SumInsured4", ""),
            ("Do you want to cover legal liability for paid drivers", request_data['legal_liability']),
            ("No of Paid Driver", ("1" if request_data['legal_liability'] == 'Yes' else "0")),
            ("Specify Sum Insured per paid driver:", ''),
            ("Do you want to cover legal liability  for other employed", ""),
            ("voluntary excess/deductible", request_data['vol_excess']),  # MotorVoluntaryDeductibleNetPremium
            ("Type of Financier", ""),
            ("Financer_Name", request_data['financier']),
            ("Financer_Address", ""),
            ("Financer_City", ""),

            ("Previous Year NCB %", request_data['prev_year_ncb']),
            ("Previous Year Policy or Cover Note Number", request_data['prev_year_policy_number']),
            ("Previous Year Policy Type", "comprehensive"),
            ("Previous Year Insurance Company Name", request_data['prev_year_ins_comp']),
            ("Previous Year Policy Start Date",
             datetime.datetime.strptime(request_data['prev_year_pol_startdate'], '%m/%d/%Y').strftime('%d/%m/%Y')),
            ("Previous Year Policy END Date",
             datetime.datetime.strptime(request_data['prev_year_pol_enddate'], '%m/%d/%Y').strftime('%d/%m/%Y')),
            ("Previous Year Insurer Address", request_data['prev_year_ins_district']),
            ("Previous Year Insurer City", request_data['prev_year_ins_city']),
            ("Previous Year Insurer State", request_data['prev_year_ins_state']),
            ("Previous Year Insurer State Pncode", request_data['prev_year_ins_pincode']),
            ("Claims History in last 3 years_Year 1", ''),
            ("Claims History in last year", ''),
            ("Payment Mode", "Online"),
            ("CMS Slip _Number/ ATOM_Transaction No/CC App. No", request_data['transaction_num']),
            ("CMS_Date/Transaction Date",
             datetime.datetime.strptime(request_data['transaction_date'], '%m/%d/%Y').strftime('%d/%m/%Y')),
            ("Premium_Amount", request_data['premium_amount']),
            ("Instrumnet_or_DD_Number_or_Credit Card_Number", ""),
            ("Instrumnet_or_DD_Amount_or_Credit card_Amount", ""),
            ("Instrumnet_or_DD_Date_Credit Card_date", ""),
            ("Doc received by ops for processing", ""),
            ("contact id", ""),
            ("inward number", ""),
            ("Receiptno", ""),
            ("policy status", ""),
            ("policy no", ""),
            ("policy posted date and time", ""),
            ("proposal no", ""),
            ("request received from - team", ""),
            ("Remarks", ""),
            ("Base rate variation", ""),
            ("UW discount", ""),
            ("LeadID", ""),
            ("Policy Number", ''),
        ]
        head_keys, row_values = [], []
        for e in table:
            head_keys.append(e[0])
            row_values.append(e[1])

        filename = '%s_%s.xls' % (request_data['cust_first_name'], datetime.datetime.now().strftime('%d%b%Y_%H%M%S'))
        return error, errormsg, filename, head_keys, row_values
