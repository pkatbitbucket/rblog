import copy
import datetime
import hashlib
import json
import logging
import math
import os
from dateutil.relativedelta import relativedelta
from concurrent.futures import ThreadPoolExecutor, TimeoutError, as_completed
from django.conf import settings
from django.core.files.storage import default_storage
from django.core.mail import EmailMessage
from django.db.models import Q
from suds.client import Client

import putils
from celery_app import app
from motor_product import logger_utils, mail_utils
from motor_product.insurers import insurer_utils
from motor_product.insurers.landt import get_landt_user_data
from motor_product.models import (DiscountLoading, Insurer, LnTPolicyResponse,
                                  Quote, Receipt, RTOMaster, Transaction,
                                  Vehicle, VehicleMaster, IntegrationStatistics)
from utils import motor_logger, log_error

from .custom_dictionaries import *
from motor_product.models import Inspection

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)

INSURER_SLUG = 'l-t'
try:
    INSURER = Insurer.objects.get(slug=INSURER_SLUG)
    INSURER_ID = INSURER.id
    INSURER_NAME = INSURER.title
except:
    INSURER_ID = 0
    INSURER_NAME = ''


suds_logger = logger_utils.getLogger(INSURER_SLUG)

### Discount North RTO List ###
NORTH_RTO_LIST = [
    'DL-1',
    'DL-2',
    'DL-3',
    'DL-4',
    'DL-5',
    'DL-6',
    'DL-7',
    'DL-8',
    'DL-9',
    'DL-10',
    'DL-11',
    'DL-12',
    'DL-13',
    'RJ-14',
    'RJ-32',
    'UP-32',
    'UP-36',
]

NORTH_IMD_LIST = [
    '13C800007',
    '13C800015',
    '13C800016',
]

NORTH_IMD_LIST_ADDON = ['13C800007', '13C800015', '13C800016', '13C800006', ]

LANDT_RECEIVERS = ['Sanju.George@ltinsurance.com', 'd2coperations2@ltinsurance.com', 'vijay.m@ltinsurance.com']
COVERFOX_RECEIVERS = ['harshad@coverfox.com', 'mayank@coverfoxmail.com']
ANIMESH = ['animesh@coverfox.com', ]

AGENT_BRANCH_MUMBAI = '1000005'
CURRENCY_REF_INR = '70'
ASSET_TYPE_MOTOR_ID = '3'
SHOW_ROOM_LOCATION_MUMBAI = '1000031'

# Payment Gateway details
PORTAL_USER_ID = 'CoverFox01'
PORTAL_USER_NAME = 'CoverFox'
CHANNEL_TYPE_REF = '3300013'
POLICY_NAME = 'my:asset Private Car Package Policy'
PRODUCT_CODE = '5101'
PAYMENT_GATEWAY_SOURCE_ID = '630'

if settings.PRODUCTION:
    # ####### PRODUCTION CREDENTIALS ########
    # Premium Generation
    PREMIUM_WSDL = 'https://pigeon.ltinsurance.com/premiumcalculatorservice/service1.svc?wsdl'

    # Payment Gateway details
    PAYMENT_URL = 'https://payments.ltinsurance.com/ltepayprocessor/hservlet'
    SOURCE_SYSTEM_KEY = 'C0F0x_12'

    # Proposal Generation
    PROPOSAL_URL = 'https://pigeon.ltinsurance.com/Pigeon/Pigeon.svc?wsdl'
    # New proposal URL
    PROPOSAL_URL_GREEN = "https://Pigeon.ltinsurance.com/PigeonGreen/Pigeon.svc?wsdl"

else:
    ####### UAT CREDENTIALS ########
    # PREMIUM_WSDL_OLD = 'file://%s' % (os.path.join(settings.SETTINGS_FILE_FOLDER,'motor_product/insurers/landt/premium_wsdl.xml'))
    # PREMIUM_WSDL = 'http://gicgmsuat.ltinsurance.com:8080/PremiumCalculator_Upgrade/Service1.svc?wsdl'
    PREMIUM_WSDL = 'https://pigeon.ltinsurance.com/premiumcalculatorservice/service1.svc?wsdl'
    # Payment Gateway details
    PAYMENT_URL = 'http://115.111.14.217/ltepayprocessor/hservlet'
    SOURCE_SYSTEM_KEY = 'C0F0x_11'

    # PROPOSAL_URL_OLD = 'http://gicgmsuat.ltinsurance.com/pigeon/Pigeon.svc?wsdl'
    PROPOSAL_URL = 'http://gicgmsuat.ltinsurance.com:8080/PigeonWebService_Upgrade/Pigeon.svc?wsdl'



FIXED_BASE_RATE_DISCOUNT = -10
FIXED_BASE_RATE_VARIATION_FOR_ADDONS = -10
RISK_BASED_DISCOUNT_FOR_ADDONS = -10

CD_ACCOUNT_IMD_INFO = {
    'IMDCode': '13C800017',
    'AgentBranchRef': '4000002',
    'AgentContactId': '3010079259',
    'AgentAccountId': '0',
}

VEHICLE_AGE_LIMIT = 15
PREMIUM_TIMEOUT = 25

# ## This is them minimum data required for getting the premium ###
"""
mock_data_dictionary = {
    'VehicleId' : '9036',
    'RegistrationNumber' : 'AP-01-AA-3222',
    'VehicleBuyingMonth' : '04',
    'VehicleBuyingYear' : '2012',
}
"""


class PremiumCalculator:
    private_car_dictionary_mapping = {
        'UnnamedPassengersSiVORef': ADD_PA_SI_LTG_ID,
        'PreviousYearNCBVORefID_MST': NCB_PVT_CAR_ID,
        'NcbVORefID_MST': NCB_PVT_CAR_ID,
        'OwnerDriverSiVORef': ADD_PA_SI_LTG_ID,
        'PANamedPersonSIVORefID_MST': NAMED_SI_LTG_ID,
        'ManufactureMonthVORefID_MST': MANUFACTURE_MONTH_ID,
        'VehicleSegmentVORefID_MST': VEHICLE_SEGMENT_LTG_ID,
    }
    calc_premium_dictionary_mapping = {
        'AntiTheftCertifiedVORefID_MST': ANTI_THEFT_CERTIFIER_LTG_ID,
        'CoverTypeVORefID_MST': COVER_TYPE_LTG_ID,
        'FuelTypeVORef': FUEL_TYPE_ID,
        'PAUnnamedPersonSIVORefID_MST': UNNAMED_SI_LTG_ID,
        'TVehicleProposalLTGVORefID_MST': VEHICLE_PROPOSAL_LTG_ID,
        'TVehicleRegistrationTypeLTGVORefID_MST': VEHICLE_REGISTRATION_TYPE_ID,
        'TbasisOfNcbVORefID_MST': NCB_BASIS_LTG_ID,
        'VehicleMortgageTypesVORef': VEHICLE_MORTGAGE_TYPES_ID,
        'VoluntaryDeductibleIdRefID_MST': VOLUNTARY_DEDUCTIBLE_LTG_ID,
    }
    client = None

    @classmethod
    def get_client(cls, name=None):
        if cls.client == None:
            cls.client = Client(PREMIUM_WSDL)
        cls.client.set_options(plugins=[suds_logger.set_name(name)])
        return cls.client

    #### Temporary hack to send multiple requests to L&T to get addon breakup ###
    @classmethod
    def _get_lnt_futures(cls, executor, request_data):
        lnt_futures = []
        if request_data['privateCar_DepreciationWaiverFlagRef'] == '1' and request_data[
            'privateCar_NcbProtectionFlagVORef'] == '1':
            dep_waiver_request_data = copy.deepcopy(request_data)
            dep_waiver_request_data['privateCar_NcbProtectionFlagVORef'] = '0'
            lnt_futures.append(executor.submit(cls.get_premium, dep_waiver_request_data))

            ncb_protect_request_data = copy.deepcopy(request_data)
            ncb_protect_request_data['privateCar_DepreciationWaiverFlagRef'] = '0'
            lnt_futures.append(executor.submit(cls.get_premium, ncb_protect_request_data))
        else:
            lnt_futures.append(executor.submit(cls.get_premium, request_data))
        return lnt_futures

    @classmethod
    def _combine_lnt_prem_response(cls, lnt_prem_array):
        if len(lnt_prem_array) == 1:
            return lnt_prem_array[0]
        elif len(lnt_prem_array) == 2:
            resp = lnt_prem_array[0]
            resp['premiumBreakup']['addonCovers'].extend(
                lnt_prem_array[1]['premiumBreakup']['addonCovers']
            )
            return resp
        else:
            return None

    @classmethod
    def _get_premium_for_listing(cls, data_dictionary):
        data_dictionary['isForListing'] = False
        executor = ThreadPoolExecutor(max_workers=2)
        lnt_prem_array = []
        lnt_futures = cls._get_lnt_futures(executor, data_dictionary)
        try:
            for f in as_completed(lnt_futures, timeout=PREMIUM_TIMEOUT):
                try:
                    if (f.result()):
                        lnt_prem_array.append(f.result()['result'][0])
                except Exception:
                    log_error(motor_logger, msg='Unable to fetch the premium for L and T')
        except TimeoutError:
            motor_logger.info("Timeout Error for L and T\n")
            raise

        result_data = cls._combine_lnt_prem_response(lnt_prem_array), None
        if result_data[0] is None:
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_PREMIUM_INVALID_RESPONSE}
        return {'success': True, 'result': result_data}

    ### End of hack

    @classmethod
    def get_premium(cls, data_dictionary):
        if int(data_dictionary['vehicleAge']) >= 7 and data_dictionary['third_party'] == 'cardekho':
            return {'success': False, 'error_code': IntegrationStatistics.COVERFOX_TP_RESTRICTED}

        # Turn off quotes for MARUTI with age >=10
        vehicle = Vehicle.objects.get(id=int(data_dictionary['VehicleId']))
        if int(data_dictionary['vehicleAge']) >= 10 and vehicle.make == 'MARUTI':
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_AGE_RESTRICTED}

        if data_dictionary.get('isForListing', False):
            return cls._get_premium_for_listing(data_dictionary)

        if int(data_dictionary['vehicleAge']) >= VEHICLE_AGE_LIMIT:
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_AGE_RESTRICTED}

        clsRequestCalculatePremium = cls._get_required_params_for_premium_calculation(data_dictionary)

        if type(clsRequestCalculatePremium) == dict:
            if clsRequestCalculatePremium.get('error_code'):
                return clsRequestCalculatePremium

        idv = data_dictionary['privateCar_Idv']
        min_idv = data_dictionary['min_idv']
        max_idv = data_dictionary['max_idv']

        previous_ncb_ref = clsRequestCalculatePremium['PrivateCar']['PreviousYearNCBVORefID_MST']
        previous_ncb = '0'
        for k, v in NCB_PVT_CAR_ID.items():
            if v == previous_ncb_ref:
                previous_ncb = k

        selectedProduct = cls.get_client().factory.create("SelectedProduct")

        # Check if inspection is bypassed
        bypassInspection = clsRequestCalculatePremium['PrivateCar'].get('bypassInspection', False)
        if bypassInspection is not False:
                data_dictionary['bypassInspection'] = clsRequestCalculatePremium[
                    'PrivateCar'].pop('bypassInspection')

        premium_response = cls.get_client(data_dictionary['quoteId']).service.CalculatePremium(
            clsRequestCalculatePremium, selectedProduct.PrivateCar)
        resp_dict = dict([item for item in premium_response])

        if not resp_dict['StrStatus'] == 'Success':
            raise ValueError('Failure response from L&T server')

        resp_dict['addon_isDepreciationWaiver'] = data_dictionary['privateCar_DepreciationWaiverFlagRef']
        resp_dict['addon_isNcbProtection'] = data_dictionary['privateCar_NcbProtectionFlagVORef']
        resp_dict['specialDiscount'] = data_dictionary['specialDiscount']
        result_data = convert_premium_response(resp_dict, data_dictionary)
        return {'success': True, 'result': result_data}

    @classmethod
    def _get_required_params_for_premium_calculation(cls, data_dictionary):

        # vehicle = VehicleMappingTest.objects.get(mapped_vehicle_id = data_dictionary['VehicleId']).master_vehicle
        vehicle = Vehicle.objects.get(id=int(data_dictionary['VehicleId']))

        rto_location = data_dictionary['RegistrationCity']
        insurer_rto = RTOMaster.objects.get(rto_code=rto_location).insurer_rtos.get(insurer__slug=INSURER_SLUG)
        rto_id = insurer_rto.rto_code
        imd_code = RTO_ID_TO_AGENT_ID[rto_id]['IMDCode']
        dl = _get_discount(vehicle, imd_code)

        # Declined models
        if dl is None:
            return {'success': False, 'error_code': IntegrationStatistics.COVERFOX_DECLINED_MODEL}
        else:
            # LOGIC ::  If discount is given (means dl < 0) then dl is 0 for vehicle >7 & vehicle <10
            # else if loading is charged less than 10 for vehicle >10 and <15, then loading will be flat 10 %.
            if int(data_dictionary['vehicleAge']) > 7:
                if dl < 0:
                    dl = 0

                if int(data_dictionary['vehicleAge']) >= 10:
                    if dl < 10:
                        dl = 10
            else:
                if vehicle.make in ['HONDA', 'HYUNDAI', 'TOYOTA', 'RENAULT']:
                    if dl < 0:
                        dl = -10
                    elif dl == 2.5:
                        dl = 0
        if data_dictionary['corporate_employee_discount']:
            data_dictionary['specialDiscount'] = max(-10, dl-10)
            if dl < 0:
                data_dictionary['employeeDiscount'] = -10 - dl
            else:
                data_dictionary['employeeDiscount'] = -10
        else:
            data_dictionary['specialDiscount'] += float(dl)
        min_idv, idv, max_idv = cls._get_idv_response(data_dictionary, vehicle
)        # calculateIDV request failed.
        # TODO: check status field for success

        if idv == 0:
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_NO_IDV}

        request_idv = float(data_dictionary['privateCar_Idv'])
        if request_idv < float(min_idv):
            data_dictionary['privateCar_Idv'] = min_idv
        elif request_idv > float(max_idv):
            data_dictionary['privateCar_Idv'] = max_idv

        data_dictionary['min_idv'] = min_idv
        data_dictionary['max_idv'] = max_idv

        return cls._get_calculate_premium_dict(data_dictionary, vehicle)

    @classmethod
    def _get_idv_response(cls, data_dictionary, vehicle):

        clsRequestCalculateIDV = cls._get_cls_request_idv_dict(data_dictionary, vehicle)
        selectedProduct = cls.get_client().factory.create("SelectedProduct")
        idv_resp = cls.get_client(data_dictionary['quoteId']).service.CalculateIDV(clsRequestCalculateIDV,
                                                                                   selectedProduct.PrivateCar)

        idv = int(float(idv_resp.IDV))
        min_idv = int(math.ceil(0.851 * idv))
        max_idv = int(math.floor(1.149 * idv))

        # Hack for Maruti to have idv variation -35% to +35%
        if vehicle.make_code == '1000015' and vehicle.vehicle_type == 'Private Car':
            min_idv = int(math.ceil(0.651 * idv))
            max_idv = int(math.floor(1.349 * idv))

        return min_idv, idv, max_idv

    @classmethod
    def _get_cls_request_idv_dict(cls, data_dictionary, vehicle):

        split_registration_number = data_dictionary['RegistrationNumber'].split('-')
        new_policy_start_date = datetime.datetime.strptime(data_dictionary['newPolicyStartDate'], "%d-%m-%Y")

        cls_request_idv_dict = {
            'EffectiveDate': new_policy_start_date.replace(hour=0, minute=0, second=0, microsecond=0).isoformat(),
            'ManufactureVORef': vehicle.make_code,
            'ModelMonth': data_dictionary['VehicleBuyingMonth'],
            'ModelVORef': vehicle.model_code,
            'ModelYear': data_dictionary['VehicleBuyingYear'],
            'ShowRoomLocVORef': SHOW_ROOM_LOCATION_MUMBAI,
        }

        return cls_request_idv_dict

    @classmethod
    def _get_calculate_premium_dict(cls, data_dictionary, vehicle):
        rto_location = data_dictionary['RegistrationCity']
        currentDateTime = datetime.datetime.now()

        insurer_rto = RTOMaster.objects.get(rto_code=rto_location).insurer_rtos.get(insurer__slug=INSURER_SLUG)
        rto_id = insurer_rto.rto_code
        rto_zone = insurer_rto.rto_zone

        base_rate_discount, risk_based_discount = get_discounts(vehicle, rto_id)

        if data_dictionary['payment_mode'] == 'CD_ACCOUNT':
            agent_branch = CD_ACCOUNT_IMD_INFO['AgentBranchRef']
        else:
            agent_ids = RTO_ID_TO_AGENT_ID[rto_id]
            agent_branch = agent_ids['AgentBranchRef']  # 'AgentBranchRef'

        new_policy_start_date = datetime.datetime.strptime(data_dictionary['newPolicyStartDate'], "%d-%m-%Y")
        new_policy_end_date = add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)

        # # Bypass Inspection logic start
        # is_new_vehicle = (data_dictionary['isNewCar'] == '1')
        # # New policy start date and end date assignment
        # if not is_new_vehicle:
        #     past_policy_end_date = datetime.datetime.strptime(data_dictionary['policyExpiryDate'], "%d-%m-%Y")
        #     # Check if policy is expired
        #     if past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now():
        #         # BYPASS INSPECTION CHECK
        #         if past_policy_end_date.replace(
        #             hour=23, minute=59, second=0) >= (
        #                 datetime.datetime.now() - datetime.timedelta(days=56)) and data_dictionary[
        #                 'privateCar_IsClaimedLastYearRefID'] == '0' and int(data_dictionary[
        #                 'privateCar_Idv']) <= 500000:
        #                     data_dictionary['bypassInspection'] = '1'
        #                     new_policy_start_date = datetime.datetime.now().replace(hour=0, minute=0, second=0) + datetime.timedelta(days=4)
        # # End bypass inspection logic

        clsRequestCalculatePremiumDict = {
            'AdditionalDiscount': '0',
            'AgentBranch': agent_branch,
            'AntiTheftCertifiedVORefID_MST': None,
            'AntiTheftFittedFlagVORefID': '0',
            'BIFuelKitValue': '0',
            'BangladeshFlagVO': '0',
            'BaseRateVariation': base_rate_discount,
            'BhutanFlagVO': '0',
            'Commercial': None,
            'ConfinedOwnPremisesFlagVORefID': '0',
            'CoverTypeFlagVORefID': '0',
            'CoverTypeVORefID_MST': 'Package',
            'CoverUnnamedFlagVORefID': '0',
            'DiscretionaryDiscount': risk_based_discount,
            'EmployeeLNTFlagVORefID': '0',
            'FiberTankFlagVORefID': '0',
            'ForHandicappedFlagVORefID': '0',
            'FuelTypeVORef': vehicle.fuel_type,
            'GeographicalExtensionFlagVORefID': '0',
            'IDVElectrical': '0',
            'IDVNonElectrical': '0',
            'ImportedFlagVORefID': '0',
            'InspectionDate': None,
            'InspectionDoneFlagRefID': '0',
            'IsServiceTaxExemptRefID': '0',
            'LNTFinancierFlagVORefID': '0',
            'LobVORefID_MST': '4',
            'MaldivesFlagVO': '0',
            'ManufactureVORefID_MST': vehicle.make_code,
            'Misc': None,
            'ModelVORefID_MST': vehicle.model_code,
            'ModelYear': data_dictionary['VehicleBuyingYear'],
            'NepalFlagVO': '0',
            'NoOfEmployees': '0',
            'NoOfPaidDrivers': '0',
            'NoOfUnnamed': '0',
            'PAUnnamedPersonSIVORefID_MST': '0',
            'PaCoverForOwnerDriverFlagVORefID': '1',
            'PaNoOfPaidDriversID': '0',
            'PakistanFlagVO': '0',
            'PolicyEndDate': new_policy_end_date.replace(hour=0, minute=0, second=0).isoformat(),
            'PolicyStartDate': new_policy_start_date.replace(hour=0, minute=0, second=0).isoformat(),
            'RTOLocationVORef': rto_id,
            'RegistrationDate': datetime.datetime(int(data_dictionary['VehicleBuyingYear']),
                                                  int(data_dictionary['VehicleBuyingMonth']),
                                                  int(data_dictionary['VehicleBuyingDay'])).isoformat(),
            'RegularDriverFlagID': '0',
            'SrilankaFlagVO': '0',
            'TPPDRestrictedFlagVORefID': '0',
            'TVehicleProposalLTGVORefID_MST': 'New Vehicle',
            'TVehicleRegistrationTypeLTGVORefID_MST': 'New',
            'TbasisOfNcbVORefID_MST': 'NCB Declaration',
            'Trailer1Value': '0',
            'TwoWheeler': None,
            'UsedDrivingTuitionFlagVORefID': '0',
            'UsedForTowingFlagVORefID': '0',
            'VehicleLaidUpFlagRefID': '0',
            'VehicleMortgageTypesVORef': 'No',
            'VehicleValueIncludesDutyFlagVORefID': '0',
            'VoluntaryDeductibleIdRefID_MST': 'None',
            'ZoneVORef': rto_zone,
        }

        insurer_utils.update_complex_dict(clsRequestCalculatePremiumDict, data_dictionary, 'calcPremium_',
                                          cls.calc_premium_dictionary_mapping)

        if 'calcPremium_RegistrationDate' in data_dictionary:
            date_array = data_dictionary['calcPremium_RegistrationDate'].split('-')
            clsRequestCalculatePremiumDict['RegistrationDate'] = datetime.datetime(
                int(date_array[2]), int(date_array[1]), int(date_array[0])
            ).isoformat()
        if 'calcPremium_InspectionDate' in data_dictionary:
            date_array = data_dictionary['calcPremium_InspectionDate'].split('-')
            clsRequestCalculatePremiumDict['InspectionDate'] = datetime.datetime(
                int(date_array[2]), int(date_array[1]), int(date_array[0])
            ).isoformat()
        clsRequestCalculatePremiumDict['PrivateCar'] = cls._get_private_car_dict(data_dictionary, vehicle)

        return clsRequestCalculatePremiumDict

    @classmethod
    def _get_private_car_dict(cls, data_dictionary, vehicle):
        rto_location = data_dictionary['RegistrationCity']
        insurer_rto = RTOMaster.objects.get(rto_code=rto_location).insurer_rtos.get(insurer__slug=INSURER_SLUG)
        rto_id = insurer_rto.rto_code

        if int(data_dictionary['privateCar_DepreciationWaiverFlagRef']):
            base_rate_discount_addon, risk_based_discount_addon = get_discounts(vehicle, rto_id, True)
        else:
            base_rate_discount_addon, risk_based_discount_addon = 0, 0

        currentDateTime = datetime.datetime.now()
        buyingDateTime = datetime.datetime(
            int(data_dictionary['VehicleBuyingYear']),
            int(data_dictionary['VehicleBuyingMonth']),
            int(data_dictionary['VehicleBuyingDay'])
        )

        deltaYears = int((currentDateTime - buyingDateTime).days / 365.25)
        if deltaYears > 7:
            deltaYears = 7

        approximate_ncb = YEARWISE_NCB[deltaYears]
        approximate_previous_ncb = YEARWISE_NCB[0] if deltaYears - 1 < 0 else YEARWISE_NCB[deltaYears - 1]

        special_discount_case = data_dictionary['specialDiscount']

        make = Vehicle.objects.get(pk=int(data_dictionary['VehicleId'])).make
        if data_dictionary.get('bypassInspection', False) == '1':
            if make == 'MARUTI':
                base_rate_discount_addon = 10
                special_discount_case = 0
            else:
                special_discount_case = 10
                base_rate_discount_addon = 0
        elif make == 'MARUTI':
            swp = base_rate_discount_addon
            base_rate_discount_addon = special_discount_case
            special_discount_case = swp

        privateCarDict = {
            'AdditionalTowingCharge': '0',
            'AutomobileAssoMemberNumber': None,
            'BaseRateVariation': base_rate_discount_addon,
            'CashAllowanceFlagVORef': '0',
            'CoverNamedPersonFlagRefID': '0',
            'DepreciationWaiverFlagRef': '0',
            'ExpiryDate': None,
            'Idv': None,
            'IsAutomobileAssociationMemberVORefID': '0',
            'IsClaimedLastYearRefID': '0',
            'IsCoverForOwnerDriverRef': '0',
            'IsCoverForUnNamedPassengersRef': '0',
            'IsDriveThroughProtectionForEngineRef': '0',
            'IsKeyReplacementRef': '0',
            'IsRallyVehicleRefID': '0',
            'IsReturnToInvoiceRef': '0',
            'IsVintageCarRefID': '0',
            'ManufactureMonthVORefID_MST': data_dictionary['VehicleBuyingMonth'],
            'MarketMovement': special_discount_case,
            'NameOfAutomobileAssociationIdRefID_MST': None,
            'NcbProtectionFlagVORef': '0',
            'NcbVORefID_MST': approximate_ncb,
            'NoOfNamedPersons': '0',
            'NormalGeogLimitIdRefID_MST': None,
            'NrUnnamedPassengers': '0',
            'OwnerDriverSiVORef': '0',
            'PANamedPersonSIVORefID_MST': '0',
            'PaidDriverSi': '0',
            'PreviousYearNCBVORefID_MST': approximate_previous_ncb,
            'RallyDays': '0',
            'RallyStartDate': None,
            'RiskBasedDiscountLoading': risk_based_discount_addon,
            'SecondVehicleLTGICLFlagRefID': '0',
            'UnnamedPassengersSiVORef': '0',
            'VariantVORefID_MST': vehicle.variant_code,
            'VehicleSegmentVORefID_MST': vehicle.vehicle_segment,
            'bypassInspection': data_dictionary.get('bypassInspection', '0'),
        }

        insurer_utils.update_complex_dict(privateCarDict, data_dictionary, 'privateCar_',
                                          cls.private_car_dictionary_mapping)
        return privateCarDict


class ProposalGenerator():
    client = None

    @classmethod
    def get_client(cls, transaction, name=None):
        if cls.client == None:
            if transaction.raw.get('is_lnt_green', '') == '1':
                cls.client = Client(PROPOSAL_URL_GREEN)
            else:
                cls.client = Client(PROPOSAL_URL)
        cls.client.set_options(plugins=[suds_logger.set_name(name)])
        return cls.client

    proposal_contact_dictionary_mapping = {
        'AddressTypeVORef': ADDRESS_TYPE_ID,
        'BusinessActivityTypeVORef': BUSINESS_ACTIVITY_TYPE_ID,
        'ContactTypeVORef': CONTACT_TYPE_ID,
        'EntityTypeVORef': ENTITY_TYPE_ID,
        'GenderVORef': GENDER_ID,
        'LanguageVORef': LANGUAGE_ID,
        'TitleVORef': TITLE_ID,
        'AddressCategoryVORef': ADDRESS_CATEGORY_ID,
        # 'CityVORef' : CITY_ID,
        'CountryVORef': COUNTRY_ID,
        # 'PostOfficeVORef' : POST_OFFICE_ID,
        'EmailTypeVORef': EMAIL_TYPE_ID,
        'CountryRef': COUNTRY_ID,
        'IdTypeRef': CONTACT_IDENTIFIER_ID_TYPE_ID,
        'CountryDialCodeRef': COUNTRY_DIAL_CODE_ID,
        'TelephoneTypeRef': TELEPHONE_TYPE_ID,
    }

    proposal_receipt_dictionary_mapping = {
        'InstrumentType': INSTRUMENT_TYPE_LTG_ID,
    }

    proposal_private_car_dictionary_mapping = {
        'AddressCategoryVORef': ADDRESS_CATEGORY_ID,
        # 'CityVORef' : CITY_ID,
        'CountryVORef': COUNTRY_ID,
        # 'PostOfficeVORef' : POST_OFFICE_ID,
        'AddresseeTypeVORef': ADDRESSEE_TYPE_ID,
        'CollectionMethodId': COLLECTION_METHOD_ID,
        'CustomerDecisionVORef': CUSTOMER_DECISION_ID,
        'PaymentTermId': PAYMENT_TERMS_ID,
        'PolicyInsuranceTypeVORef': POLICY_INSURANCE_TYPE_ID,
        'PremiumPaymentMethodVORef': PREMIUM_PAYMENT_METHOD_ID,
        'StatusAttributeVORef': STATUS_ATTRIBUTE_ID,
        'AddDriverGenderVORef': GENDER_ID,
        'AntiTheftCertifiedVORef': ANTI_THEFT_CERTIFIER_LTG_ID,
        'BasisOfNcbVORef': NCB_BASIS_LTG_ID,
        'CoverTypeVORef': COVER_TYPE_LTG_ID,
        'FuelTypeVORef': FUEL_TYPE_ID,
        'LaidUpBenefitVORef': LU_BENEFIT_ID,
        'LaidUpPeriodVORef': LU_PERIOD_ID,
        'LockedParkingVORef': LOCKED_PARKING_ID,
        'ManufactureMonthVORef': MANUFACTURE_MONTH_ID,
        'NcbVORef': NCB_PVT_CAR_ID,
        'OwnerDriverSiVORef': ADD_PA_SI_LTG_ID,
        'PANamedPersonSIVORef': NAMED_SI_LTG_ID,
        'PAUnnamedPersonSIVORef': UNNAMED_SI_LTG_ID,
        'PreviousYearNCBVORef': NCB_PVT_CAR_ID,
        'ReasonServicetaxExempVORef': SERVICE_TAX_EXEMPTION_ID,
        'RegistrationTypeVORef': VEHICLE_REGISTRATION_TYPE_ID,
        'UnnamedPassengersSiVORef': ADD_PA_SI_LTG_ID,
        'VehicleMortgageTypesVORef': VEHICLE_MORTGAGE_TYPES_ID,
        'VehicleProposalVORef': VEHICLE_PROPOSAL_LTG_ID,
        'VehicleSegmentVORef': VEHICLE_SEGMENT_LTG_ID,
        'VoluntaryDeductibleIdRef': VOLUNTARY_DEDUCTIBLE_LTG_ID,
        # 'ZoneVORef' : RTO_ZONE_ID,
        'PolicyContactRoleVORef': CONTACT_POLICY_ROLE_ID,
        # 'ContactExtNum' : PAST_INSURANCE_ID,
    }

    proposal_transaction_dictionary_mapping = {
        'PaymentMode': INSTRUMENT_TYPE_LTG_ID,
        'ProductId': PRODUCT_ID,
    }

    @classmethod
    def get_proposal(cls, transaction):

        data = {
            'CONTACT': transaction.raw['contact'],
            'INSURED_CAR': transaction.raw['private_car'],
            'TRANSACTION': transaction.raw['policy_details'],
            'RECEIPT': transaction.receipt.raw,
        }
        proposal_data = json.dumps(data)
        transaction.proposal_request = proposal_data
        transaction.save()
        motor_logger.info(
            '\nLANDT TRANSACTION: %s\nPOLICY REQUEST SENT:\n%s\n' % (transaction.transaction_id, proposal_data))

        resp = cls.get_client(transaction, transaction.transaction_id).service.PrivateCar(data['CONTACT'], data['RECEIPT'],
                                                                             data['INSURED_CAR'], data['TRANSACTION'])

        resp_dict = dict([item for item in resp])
        insured_details_json = transaction.insured_details_json
        insured_details_json['product_response'] = resp_dict
        if transaction.status == 'COMPLETED':
            Transaction.objects.filter(id=transaction.id).update(insured_details_json=insured_details_json)
        else:
            transaction.insured_details_json = insured_details_json
            transaction.save(update_fields=['insured_details_json'])
        motor_logger.info('\nLANDT TRANSACTION: %s\nPOLICY RESPONSE RECEIVED:\n%s\n' % (
            transaction.transaction_id, insured_details_json['product_response']
        ))

        return resp_dict

    @classmethod
    def save_policy_details(cls, data_dictionary, transaction):
        proposal_number = '%sR2D2%s' % (PRODUCT_CODE, transaction.id)
        transaction.raw['is_lnt_green'] = '1'
        transaction.save()
        if data_dictionary.get('proposalPrivateCar_InspectionNumber', None) != '11111':
            # Set inspection number if used car
            if transaction.quote.raw_data['isUsedVehicle'] == '1':
                data_dictionary['proposalPrivateCar_InspectionNumber'] = 'CFX%s' % transaction.id

            # Set inspection number if expired
            elif data_dictionary.get('proposalPrivateCar_InspectionDoneFlag', '0') == '1':
                data_dictionary['proposalPrivateCar_InspectionNumber'] = 'CFX%s' % transaction.inspection_set.get(status=Inspection.DOCUMENT_APPROVED).number

        private_car_dict = cls._get_proposal_private_car_dict(data_dictionary)
        proposer_dict = cls._get_proposal_contact_dict(data_dictionary)

        if transaction.quote.raw_data['payment_mode'] == 'CD_ACCOUNT':
            private_car_dict['AgentAccountId'] = CD_ACCOUNT_IMD_INFO['AgentAccountId']
            private_car_dict['AgentBranchRef'] = CD_ACCOUNT_IMD_INFO['AgentBranchRef']
            private_car_dict['PolicyContactIVOs'][0]['policyContactIVOs']['ContactExtNum'] = CD_ACCOUNT_IMD_INFO[
                'AgentContactId']
        else:
            rto_id = private_car_dict['RTOLocationVORef']
            agent_ids = RTO_ID_TO_AGENT_ID[rto_id]
            private_car_dict['AgentAccountId'] = agent_ids['AgentAccountId']
            private_car_dict['AgentBranchRef'] = agent_ids['AgentBranchRef']
            private_car_dict['PolicyContactIVOs'][0]['policyContactIVOs']['ContactExtNum'] = agent_ids['AgentContactId']

        private_car_dict['IntermediaryReferenceCode'] = proposal_number

        transaction_dict = cls._get_proposal_trans_dict(data_dictionary)
        transaction_dict['SourceTxnId'] = proposal_number

        transaction.policy_start_date = private_car_dict['PolicyStartDate']
        transaction.policy_end_date = private_car_dict['PolicyEndDate']
        transaction.reference_id = transaction_dict['SourceTxnId']
        transaction.premium_paid = transaction_dict['Premium']
        transaction.raw['policy_details'] = transaction_dict
        transaction.raw['private_car'] = private_car_dict
        transaction.raw['contact'] = proposer_dict
        transaction.save()
        if transaction.quote.raw_data['payment_mode'] == 'CD_ACCOUNT':
            payment_details = {
                'TxnDate': datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S'),
                'TxnReferenceNo': transaction.reference_id,
                'TxnAmount': transaction.premium_paid,
            }

            receipt_dict = ProposalGenerator._get_proposal_receipt_dict(payment_details)
            receipt = Receipt(
                raw=receipt_dict,
            )
            receipt.save()
            transaction.receipt = receipt
            transaction.save()

        return True

    @classmethod
    def _get_proposal_contact_dict(cls, data_dictionary):

        contact_dict = {
            'AddressTypeVORef': 'Communication Address',
            'AddressIVO': {
                'AddressCategoryVORef': 'Urban',
                'BlockSubDivision': '',
                'BuildingName': 'Some Apartment',
                'CityName': 'Mumbai',
                'CityVORef': 'Mumbai',
                'CountryVORef': 'India',
                'DistrictName': 'Mumbai',
                'HouseNr': '5',
                'Jurisdiction': '',
                'Landmark1': 'Landmark1',
                'Landmark2': None,
                'Pob': '',
                'PoliceStation': '',
                'PostOfficeVORef': '1078658',
                'ProvinceName': 'Maharashtra',
                'Remarks': None,
                'StreetName': 'Random Street',
                'Tehsil': 'Mumbai',
                'Territory': '',
                'ZipCode': '400056',
            },
            'BusinessActivityTypeVORef': None,
            'ChannelTypeRef': CHANNEL_TYPE_REF,
            'ContactEmail': [
                {
                    'contactEmail': {
                        'Email': 'Retail.Policydocuments@ltinsurance.com',
                        'EmailTypeVORef': 'Default',
                    },
                },
            ],
            'ContactIdentifier': [
                {
                    'contactIdentifier': {
                        'CountryRef': None,
                        'IdTypeRef': None,
                        'IdValue': None,
                    },
                },
            ],
            'ContactLocation': '',
            'ContactTelephone': [
                {
                    'contactTelephone': {
                        'CountryDialCodeRef': None,
                        'TelephoneExtension': None,
                        'TelephoneNumber': None,
                        'TelephonePrefix': None,
                        'TelephoneTypeRef': None,
                    },
                },
            ],
            'ContactTypeVORef': 'Other',
            'DateOfBirth': None,
            'EntityTypeVORef': None,
            'FatherFirstName': None,
            'FatherLastName': None,
            'FirstName': None,
            'FlagEmail': '0',
            'FlagPhoneCall': '0',
            'FlagSms': '0',
            'GenderVORef': 'Male',
            'LanguageVORef': 'English',
            'MiddleName': 'Test Middle Name',
            'Name': 'Test Name',
            'NationalityRef': '94',
            'PortalUserID': PORTAL_USER_ID,
            'PortalUserName': PORTAL_USER_NAME,
            'SicDescriptionBusinessActivity': None,
            'TitleVORef': None,
        }
        insurer_utils.update_complex_dict(contact_dict, data_dictionary, 'proposalContact_',
                                          cls.proposal_contact_dictionary_mapping)

        return contact_dict

    @classmethod
    def _get_proposal_receipt_dict(cls, payment_response):

        current_date_time = datetime.datetime.now()
        transaction_date = datetime.datetime.strptime(payment_response['TxnDate'], '%d-%m-%Y %H:%M:%S')
        transaction_ref_no = payment_response['TxnReferenceNo']

        receipt_dict = {
            'Amount': payment_response['TxnAmount'],
            'AuthorizationDate': transaction_date.isoformat(),
            'AuthorizationNumber': transaction_ref_no,
            'BankBranch': None,
            'BankGuaranteeAmount': None,
            'BankGuaranteeFlagVORef': '0',
            'BankGuaranteeNr': '0',

            'BankName': None,
            'BranchVORef': AGENT_BRANCH_MUMBAI,
            'CardHolderName': 'Card Holder',  # TODO
            'CardValidDate': add_years(current_date_time, 2).replace(microsecond=0).isoformat(),
            # TODO
            'ChallanNumber': None,
            'CurrencyVORef': CURRENCY_REF_INR,
            'EntryTypeVORef': '1000012',
            'InstrumentDate': transaction_date.isoformat(),
            'InstrumentNumber': transaction_ref_no,
            'InstrumentReceivedFlagVORef': None,
            'InstrumentType': INSTRUMENT_TYPE_LTG_ID['Swipe Credit Card'],  # TODO: ASK ANITA WHAT TO SEND BY DEFAULT
            'IsLocalOrOutstation': None,
            'MicrCode': None,
            'Remarks': None,
            'SalvageFlagVORef': None,
        }

        return receipt_dict

    @classmethod
    def _get_proposal_trans_dict(cls, data_dictionary):
        trans_dict = {
            'AgentReceiptFlag': '0',
            'ContactCreatedFlag': '0',
            'ContactId': '',
            'ExternalPolicyNumber': None,
            'PaymentMode': 'Swipe Credit Card',  # TODO: SAME AS Receipt['InstrumentType']
            'PolicId': None,
            'Premium': None,
            'ProductId': 'Private Car',
            'ReceiptCreatedFlag': '0',
            'SourceSystemKey': SOURCE_SYSTEM_KEY,
            'SourceTxnId': None,  # Proposal Number to be passed here
        }
        insurer_utils.update_complex_dict(trans_dict, data_dictionary, 'proposalTransaction_',
                                          cls.proposal_transaction_dictionary_mapping)

        return trans_dict

    @classmethod
    def _get_proposal_private_car_dict(cls, data_dictionary):

        private_car_dict = {
            'AddressIVO': {
                'AddressCategoryVORef': 'Urban',
                'BlockSubDivision': '',
                'BuildingName': 'Some Apartment',
                'CityName': 'Mumbai',
                'CityVORef': 'Mumbai',
                'CountryVORef': 'India',
                'DistrictName': 'Mumbai',
                'HouseNr': '5',
                'Jurisdiction': '',
                'Landmark1': 'Landmark1',
                'Landmark2': '',
                'Pob': '',
                'PoliceStation': '',
                'PostOfficeVORef': '1078658',
                'ProvinceName': 'Maharashtra',
                'Remarks': '',
                'StreetName': 'Random Street',
                'Tehsil': 'Mumbai',
                'Territory': '',
                'ZipCode': '400056',
            },
            'AddresseeTypeVORef': 'Contact',
            'AgentAccountId': None,
            'AgentBranchRef': None,
            'AgentRepresentative': None,
            'ApplicationRecievedDate': None,
            'ApplicationRecievedTime': None,
            'AssociateCode': None,  # TODO
            'ChannelTypeRef': CHANNEL_TYPE_REF,
            'CollectionMethodId': 'Credit card',  # TODO: SAME AS Receipt['InstrumentType']
            'ContestSchemeName': None,
            'CorporateAgentAccountId': None,  # TODO
            'CustomerDecisionVORef': 'Save proposal',
            'EDispatchFlagRef': '0',
            'EmployeeNumber': None,  # TODO
            'EndorsEndDate': None,
            'EndorsStartDate': None,
            'IntermediaryAddInfoOne': None,
            'IntermediaryAddInfoTwo': None,
            'IntermediaryLocationCode': None,
            'IntermediaryPropRefNumOne': None,
            'IntermediaryReferenceCode': None,
            'InwardReferenceNumber': None,
            'LobMortgageFlag': '0',
            'MarketMovement': '0',
            'PastInsuranceVO': {
                'ContactExtNum': None,
                'InsurerAddress': None,
                'PolicyEndDate': None,
                'PolicyNo': None,
                'PolicyStartDate': None,
                'PrevCoverTypeIdRef': None,
            },
            'PaymentTermId': 'Yearly',
            'PolicyContactIVOs': [
                {
                    'policyContactIVOs': {
                        'ContactExtNum': None,
                        'PolicyContactRoleVORef': None,
                    },
                },
            ],
            'PolicyEffectiveTime': None,
            'PolicyEndDate': None,
            'PolicyInsuranceTypeVORef': '10001',
            'PolicySigningDate': None,
            'PolicySigningTime': None,
            'PolicyStartDate': None,
            'PortalUserID': PORTAL_USER_ID,
            'PortalUserName': PORTAL_USER_NAME,
            'PreInspectionRefNumber': None,
            'PremiumPaymentMethodVORef': 'Receipt',
            'ProposalApplicationDate': None,
            'ShortPremiumFlag': '0',
            'StatusAttributeVORef': None,
            'PcDepWaiverNetPremium': '0',
            'MortgageVO': {
                'ContactExtNum': None,
                'MortgageAmount': None,
                'MortgageExpirationDate': None,
                'MortgageRefNumber': None,
                'MortgageStartDate': None,
            },
            'VehicleDriverVO': [
                {
                    'vehicleDriverVOs': {
                        'DriverAge': None,
                        'DriverExtNum': None,
                        'MainDriverFlagRef': None,
                    },
                },
            ],
            'AddDriverAge': None,
            'AddDriverGenderVORef': None,
            'AddDriverName': None,
            'AdditionalDiscount': '0',
            'AdditionalTowingCharge': '0',
            'Age': None,
            'AntiTheftCertifiedVORef': None,
            'AntiTheftFittedFlagVORef': '0',
            'AssetTypeVORef': ASSET_TYPE_MOTOR_ID,
            'AutomobileAssoMemberNumber': None,
            'BIFuelKitValue': '0',
            'BangladeshFlagVO': '0',
            'BaseRateVariation': '0',
            'BaseRateVariationAddOn': '0',
            'BasisOfNcbVORef': 'NCB Declaration',
            'BreakageCoverDays': '0',  # TODO Will be calculated based on (Policy Start Date-Previous Policy End Date)
            'CashAllowanceFlagVORef': '0',
            'Cc': '1197',
            'ConfinedOwnPremisesFlagVORef': '0',
            'CoverNamedPersonFlagRef': '0',
            'CoverTypeFlagVORef': '0',
            'CoverTypeVORef': 'Package',
            'CoverUnnamedFlagVORef': '0',
            'DepreciationBenefitFlagVORef': '0',
            'DepreciationWaiverFlagRef': '0',
            'DiscountOnRenewal': '0',
            'DiscretionaryDiscount': '0',
            'EmployeeLNTFlagVORef': '0',
            'EngineNr': None,
            'ExShowRoomPrice': None,
            'ExpiryDate': None,
            'FiberTankFlagVORef': '0',
            'ForHandicappedFlagVORef': '0',
            'FuelTypeVORef': 'Petrol',
            'GeographicalExtensionFlagVORef': '0',
            'IDVElectrical': '0',
            'IDVNonElectrical': '0',
            'Idv': '244000',
            'ImportedFlagVORef': '0',
            'InspectionDate': None,
            'InspectionDoneFlag': '0',
            'InspectionDoneFlagRef': '0',
            'InspectionNumber': None,
            'InspectionTime': None,
            'IsAutomobileAssociationMemberVORef': '0',
            'IsClaimedLastYearRef': '0',
            'IsCoverForOwnerDriverRef': '0',
            'IsCoverForUnNamedPassengersRef': '0',
            'IsDriveThroughProtectionForEngineRef': '0',
            'IsKeyReplacementRef': '0',
            'IsRallyVehicleRef': '0',
            'IsReturnToInvoiceRef': '0',
            'IsServiceTaxExemptRef': '0',
            'IsVintageCarRef': '0',
            'LNTFinancierFlagVORef': '0',
            'LaidUpBenefitVORef': None,
            'LaidUpEndDate': None,
            'LaidUpPeriodVORef': None,
            'LaidUpStartDate': None,
            'LicensePlate': None,
            'LicensePlateFour': None,
            'LicensePlateOne': None,
            'LicensePlateThree': None,
            'LicensePlateTwo': None,
            'LockedParkingVORef': None,
            'MakeDsc': 'HYUNDAI',
            'MaldivesFlagVO': '0',
            'ManufactureMonthVORef': '01',
            'ManufactureVORef': '1000009',
            'MileagePerMonth': None,
            'ModelDsc': 'I10',
            'ModelVORef': '1000246',
            'ModelYear': '2010',
            'MortgageFinancierName': None,
            'MortgageFlag': '0',
            'NCBProtectionFlagVORef': '0',
            'NRUnnamedPassengers': '0',
            'NameOfAppointee': None,
            'NameOfAutomobileAssociationIdRef': None,
            'NameOfNominee': 'DHARMANDER KUMAR',
            'NcbReservingEffectiveDate': None,
            'NcbVORef': '0',
            'NepalFlagVO': '0',
            'NoOfEmployees': '0',
            'NoOfNamedPersons': '0',
            'NoOfPaidDrivers': None,
            'NoOfUnnamed': '0',
            'NormalGeogLimitIdRef': None,
            'OwnerDriverSiVORef': '0',
            'PANamedPersonSIVORef': None,
            'PAUnnamedPersonSIVORef': None,
            'PaCoverForOwnerDriverFlagVORef': None,
            'PaNoOfPaidDrivers': '0',
            'PaidDriverSi': '0',
            'PakistanFlagVO': '0',
            'PolicyExtDate': None,
            'PolicyRemarks': None,
            'PremiumExtPolicy': None,
            'PrevInsuranceFlagRef': '0',
            'PreviousYearNCBVORef': '0',
            'RTOLocationVORef': 'MH-01',
            'RallyDays': None,
            'RallyEventName': None,
            'RallyEventVenue': None,
            'RallyPromoterName': None,
            'RallyStartDate': None,
            'ReasonServicetaxExempVORef': None,
            'RegistrationDate': None,
            'RegistrationTypeVORef': 'New',
            'RegularDriverFlag': '0',
            'RelationWithNominee': None,
            'Relationship': None,
            'RiskBasedDiscountLoading': '0',
            'SeatingCapacity': '5',
            'SecondVehicleLTGICLFlagRef': '0',
            'ShassisNumber': None,
            'ShowRoomLocVORef': SHOW_ROOM_LOCATION_MUMBAI,
            'SrilankaFlagVO': '0',
            'TPPDRestrictedFlagVORef': '0',
            'Trailer1RegNO': None,
            'Trailer1Value': '0',
            'UnnamedPassengersSiVORef': '0',
            'UsedDrivingTuitionFlagVORef': '0',
            'UsedForTowingFlagVORef': '0',
            'VariantVORef': '1000453',
            'VehicleLaidUpFlagRef': '0',
            'VehicleMortgageTypesVORef': 'No',
            'VehicleProposalVORef': 'New Vehicle',
            'VehicleSegmentVORef': 'COMPACT CARS',
            'VehicleTotalAge': '3.3',
            'VehicleValueIncludesDutyFlagVORef': '0',
            'VoluntaryDeductibleIdRef': 'None',
            'ZoneVORef': 'Zone B',
            'namedPassengerPADetailsLTGVOList': [
                {
                    'AssetVehicleNamedPassengerPADetailsLTFIVO': {
                        'Name': 'None',
                        'NameOfNominee': 'None',
                        'Relationship': 'None',
                    },
                },
            ],
        }
        insurer_utils.update_complex_dict(private_car_dict, data_dictionary, 'proposalPrivateCar_',
                                          cls.proposal_private_car_dictionary_mapping)

        if private_car_dict['LicensePlateFour'] == '':
            del private_car_dict['LicensePlateFour']
            del private_car_dict['LicensePlateThree']

        return private_car_dict


def get_payment_data(transaction):
    if transaction.quote.raw_data['payment_mode'] == 'CD_ACCOUNT':
        if not settings.PRODUCTION:
            transaction.mark_complete()
            redirect_url = settings.SITE_URL.strip(
                "/") + '/motor/fourwheeler/payment/success/%s/' % transaction.transaction_id

            return {}, redirect_url

        redirect_url = '/motor/fourwheeler/product/failure/'
        policy_resp = ProposalGenerator.get_proposal(transaction)

        if policy_resp['StrStatus'] == 'Success':
            transaction.mark_complete()

            mail_utils.send_mail_for_admin({
                'title': 'Policy generation for l-t (CD ACCOUNT)',
                'type': 'policy',
                'data': 'Policy generation for transaction %s (CFOX-%s) with ref.no. %s (email: %s)' % (
                    transaction.transaction_id,
                    transaction.id,
                    transaction.reference_id,
                    transaction.proposer.email
                )
            })
            redirect_url = settings.SITE_URL.strip(
                "/") + '/motor/fourwheeler/payment/success/%s/' % transaction.transaction_id

        return {}, redirect_url

    payment_gateway_data = {
        'policyName': POLICY_NAME,
        'productCode': PRODUCT_CODE,
        'transactionAmount': transaction.premium_paid,
        # 'transactionAmount' : '1',  # NEED TO DO THIS FOR UAT
        'sourceChannel': PAYMENT_GATEWAY_SOURCE_ID,
        'proposalNumber': transaction.reference_id,
        'responseURL': '%s/motor/fourwheeler/insurer/l-t/payment/response/%s/' % (
            settings.SITE_URL.strip("/"), transaction.transaction_id
        ),
        'custContactId': transaction.proposer.mobile,
        'firstName': transaction.proposer.first_name,
        'lastName': transaction.proposer.last_name,
        'emailId': transaction.proposer.email,
        # 'mobileNumber' : transaction.proposer_mobile,
        # 'msg' : '',
    }
    transaction.payment_request = payment_gateway_data
    transaction.save()
    suds_logger.set_name(transaction.transaction_id).log(json.dumps(payment_gateway_data))
    return payment_gateway_data, PAYMENT_URL


@app.task
def generate_policy(transaction):
    success = False
    try:
        policy_resp = ProposalGenerator.get_proposal(transaction)
    except:
        log_error(motor_logger, msg='After payment error for L and T ({})'.format(transaction.transaction_id))
    else:
        transaction.proposal_response = policy_resp
        success = policy_resp['StrStatus'] == 'Success'
        if not success:
            msg_text = 'For transaction with id %s (%s)' % (transaction.id, transaction.transaction_id)
            msg_text = '%s, Proposal generation failed after payment for L & T' % msg_text
            title = 'ALERT: Proposal generation failed, failure from L & T server'
            mail_utils.send_mail_for_admin({
                'title': title,
                'data': msg_text,
            })

            # Proposal failure response mail
            mail_utils.send_mail_for_proposal_failure(transaction, str(policy_resp))

            send_automatic_email(transaction)
    finally:
        transaction.save()
        return success


def post_payment(response, transaction):
    motor_logger.info(response)

    suds_logger.set_name(transaction.transaction_id).log(json.dumps(response))

    payment_response = dict(response['GET'].items() + response['POST'].items())
    pay_res = payment_response['msg'].split('|')
    response_data = {
        'MerchantID': pay_res[0],
        'CustomerID': pay_res[1],
        'TxnReferenceNo': pay_res[2],
        'BankReferenceNo': pay_res[3],
        'TxnAmount': pay_res[4],
        'BankID': pay_res[5],
        'BankMerchantID': pay_res[6],
        'TxnType': pay_res[7],
        'CurrencyName': pay_res[8],
        'ItemCode': pay_res[9],
        'SecurityType': pay_res[10],
        'SecurityID': pay_res[11],
        'SecurityPassword': pay_res[12],
        'TxnDate': pay_res[13],
        'AuthStatus': pay_res[14],
        'SettlementType': pay_res[15],
        'AdditionalInfo1': pay_res[16],
        'AdditionalInfo2': pay_res[17],
        'AdditionalInfo3': pay_res[18],
        'AdditionalInfo4': pay_res[19],
        'AdditionalInfo5': pay_res[20],
        'AdditionalInfo6': pay_res[21],
        'AdditionalInfo7': pay_res[22],
        'ErrorStatus': pay_res[23],
        'ErrorDescription': pay_res[24],
        'CheckSum': pay_res[25],
    }
    motor_logger.info(
        '\nLANDT TRANSACTION: %s\nPAYMENT RESPONSE RECEIVED:\n %s\n' % (transaction.transaction_id, response_data))

    transaction.insured_details_json['payment_response'] = response_data
    transaction.payment_response = response_data
    transaction.save()

    is_redirect = True
    redirect_url = '/motor/fourwheeler/payment/failure/%s/' % transaction.transaction_id

    if transaction.insured_details_json['payment_response']['AuthStatus'] == '0300':

        receipt_dict = ProposalGenerator._get_proposal_receipt_dict(response_data)
        receipt = Receipt(
            raw=receipt_dict,
        )
        receipt.save()
        transaction.receipt = receipt
        transaction.payment_token = response_data['TxnReferenceNo']
        transaction.premium_paid = transaction.insured_details_json['payment_response']['TxnAmount']
        transaction.payment_done = True
        transaction.payment_on = datetime.datetime.now()
        transaction.mark_complete()

        redirect_url = '/motor/fourwheeler/payment/success/%s/' % transaction.transaction_id
        generate_policy.delay(transaction)
        return is_redirect, redirect_url, '', {}
    else:
        transaction.status = 'FAILED'
        transaction.save()
        motor_logger.info("Some error occurred in L and T response.")
    return is_redirect, redirect_url, '', {}


def get_buy_form_details(quote_parameters, transaction):
    split_registration_no = quote_parameters.get('registrationNumber[]')

    is_new_vehicle = True if quote_parameters['isNewVehicle'] == '1' else False
    if is_new_vehicle:
        registration_number = 'NEW'
    else:
        registration_number = '-'.join(split_registration_no)

    master_rto_code = "%s-%s" % (split_registration_no[0], split_registration_no[1])

    data = {
        'registration_no': registration_number,
        'rto_location_info': master_rto_code,
        # 'occupation_list': OCCUPATION_MAP,
        'cover_type_list': COVER_TYPE_LIST,
        # 'insurers_list': PAST_INSURANCE_ID_LIST,
        'insurers_list': PAST_INSURER_ID_LIST_MASTER,
        'financier_list': FINANCIER_LIST,
        'disallowed_insurers': DISALLOWED_PAST_INSURERS,
        'relationship_list': NOMINEE_RELATIONSHIP_LIST,
        'is_cities_fetch': True,
    }
    return data


def insert_policy_response(Transaction):
    transaction = {
        'TxnId': Transaction.TxnId,
        'Status': Transaction.Status,
        'TxnStatusDetail': {
            'ContactId': Transaction.TxnStatusDetail.ContactId,
            'ReceiptNo': Transaction.TxnStatusDetail.ReceiptNo,
            'PolicyNo': Transaction.TxnStatusDetail.PolicyNo,
            'ProposalNo': Transaction.TxnStatusDetail.ProposalNo,
        },
        'TxnErrorDetails': {
            'ErrorLevel': Transaction.TxnErrorDetails.ErrorLevel,
            'Details': Transaction.TxnErrorDetails.Details,
        },
    }

    policyResponse = PolicyResponse(
        raw=json.dumps(transaction)
    )
    policyResponse.save()

    response = """
        <?xml version="1.0" encoding="UTF-8"?>
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <soapenv:Body>
                <return>True</return>
        </soapenv:Body>
        </soapenv:Envelope>
    """
    return response


def get_premium_page_details():
    make_list = MAKE_LIST
    state_list = {}
    state_code_list = STATE_CODE_TO_RTO_CODES_LIST.keys()
    for state_code in state_code_list:
        state_list[state_code] = state_code

    return {
        'make_list': make_list,
        'state_list': state_list,
    }


def get_rto_city_list(state_name):
    city_list = STATE_CODE_TO_RTO_CODES_LIST[state_name]
    rto_city_list = [{'city_name': rto_code, 'city_code': rto_code} for rto_code in city_list]

    return rto_city_list


def get_vehicle_list(make_id, model_name):
    make_list = MAKE_LIST

    if make_id == '':
        return make_list

    if model_name is not '':
        return {
            'make_id': make_id,
            'variant_list': [{'vehicle_id': v.id, 'variant': v.variant} for v in
                             Vehicle.objects.filter(make_code=make_id).filter(model__istartswith=model_name)]
        }
    else:
        models = list(set([' '.join(v.model.split()) for v in Vehicle.objects.filter(make_code=make_id)]))
        model_list = model_list = [{'model_name': model} for model in models]
        return {
            'make_id': make_id,
            'model_list': model_list,
        }


# TODO This has to be rewritten
def convert_premium_request_data_to_required_dictionary(vehicle_id, request_data):
    # TODO - should be removed once data coming correctly from front end
    if 'newPolicyStartDate' not in request_data.keys():
        request_data['newPolicyStartDate'] = (datetime.datetime.now() + datetime.timedelta(days=1)).strftime("%d-%m-%Y")

    is_used_vehicle = (request_data['isUsedVehicle'] == '1')
    is_new_vehicle = (request_data['isNewVehicle'] == '1')

    if is_new_vehicle:
        man_date_split = request_data['manufacturingDate'].split('-')
        vehicle_buying_year = man_date_split[2]
        vehicle_buying_month = man_date_split[1]
        vehicle_buying_day = man_date_split[0]
    else:
        reg_date_split = request_data['registrationDate'].split('-')
        vehicle_buying_year = reg_date_split[2]
        vehicle_buying_month = reg_date_split[1]
        vehicle_buying_day = reg_date_split[0]

    # New policy start date and end date assignment
    if is_new_vehicle or is_used_vehicle:
        new_policy_start_date = datetime.datetime.strptime(request_data['newPolicyStartDate'], "%d-%m-%Y")
    else:
        past_policy_end_date = datetime.datetime.strptime(request_data['pastPolicyExpiryDate'], "%d-%m-%Y")

        # Check if policy is expired
        if past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now():
            # No ncb if policy end date before 90 days
            if past_policy_end_date.replace(
                hour=23, minute=59, second=0
            ) < datetime.datetime.now() - datetime.timedelta(days=90):
                request_data['isClaimedLastYear'] = '1'

            new_policy_start_date = datetime.datetime.strptime(
                request_data['newPolicyStartDate'], "%d-%m-%Y")
        else:
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

    # Vehicle age calculation
    if is_new_vehicle:
        vehicle_age = 0
    else:
        date_of_registration = datetime.datetime.strptime(request_data['registrationDate'], '%d-%m-%Y')
        new_policy_start_date = new_policy_start_date.replace(microsecond=0)
        vehicle_age = insurer_utils.get_age(new_policy_start_date, date_of_registration)

    split_registration_no = request_data.get('registrationNumber[]')
    master_rto_code = "%s-%s" % (split_registration_no[0], split_registration_no[1])

    addons = {'addon_isDepreciationWaiver': 'isDepreciationWaiver', 'addon_isNcbProtection': 'isNcbProtection'}

    vehicle_model = VehicleMaster.objects.get(id=request_data['vehicleId']).model

    addon_dict = {}
    for key, value in addons.items():
        addon_dict[key] = '0'
        if request_data[key] == '1' and insurer_utils.is_addon_available(value, 'l-t', vehicle_model, vehicle_age,
                                                                         master_rto_code):
            addon_dict[key] = '1'

    ### ncb calculation
    ncb = '0'
    previous_ncb = '0'
    previous_ncb_to_new_ncb = {
        '0': '20',
        '20': '25',
        '25': '35',
        '35': '45',
        '45': '50',
        '50': '50',
    }
    if not is_new_vehicle:
        if is_used_vehicle:
            previous_ncb = '0'
            ncb = request_data['newNCB'] if request_data['isNCBCertificate'] == '1' else '0'
        else:
            previous_ncb = request_data['previousNCB']
            ncb = '0' if request_data['isClaimedLastYear'] == '1' else previous_ncb_to_new_ncb[previous_ncb]

    bi_fuel_kit_value = '0'
    if request_data['isCNGFitted'] == '1':
        bi_fuel_kit_value = request_data['cngKitValue']

    data_dictionary = {
        # 'isFirstRequest' : True if request_data['idv'] == '0' else False,
        'vehicleAge': vehicle_age,
        'VehicleId': vehicle_id,
        'VehicleBuyingYear': vehicle_buying_year,
        'VehicleBuyingMonth': vehicle_buying_month,
        'VehicleBuyingDay': vehicle_buying_day,
        'isNewCar': request_data['isNewVehicle'],

        'calcPremium_TVehicleProposalLTGVORefID_MST': 'Used Vehicle' if request_data[
                                                                            'isUsedVehicle'] == '1' else 'New Vehicle',

        'privateCar_NcbVORefID_MST': ncb,
        'privateCar_PreviousYearNCBVORefID_MST': previous_ncb,
        'privateCar_IsClaimedLastYearRefID': request_data['isClaimedLastYear'],

        'calcPremium_BIFuelKitValue': bi_fuel_kit_value,
        'calcPremium_NoOfPaidDrivers': request_data['extra_isLegalLiability'],
        'privateCar_DepreciationWaiverFlagRef': addon_dict['addon_isDepreciationWaiver'],
        'privateCar_IsKeyReplacementRef': '0',  # request_data['addon_isKeyReplacement'],
        'privateCar_NcbProtectionFlagVORef': addon_dict['addon_isNcbProtection'],
        'privateCar_IsDriveThroughProtectionForEngineRef': '0',  # request_data['addon_isDriveThroughProtected'],
        'calcPremium_IDVElectrical': request_data['idvElectrical'],
        'calcPremium_IDVNonElectrical': request_data['idvNonElectrical'],
        'calcPremium_VoluntaryDeductibleIdRefID_MST': request_data['voluntaryDeductible'],
        'specialDiscount': 0,
        'payment_mode': request_data['payment_mode'],
        'third_party': request_data['third_party']
    }

    if request_data['third_party'] == 'cardekho' and vehicle_age >= 3:
        data_dictionary['privateCar_DepreciationWaiverFlagRef'] = '0'

    # For PA to unnamed passenger
    if int(request_data['extra_paPassenger']) > 0:
        vehicle = Vehicle.objects.get(id=vehicle_id)
        data_dictionary['calcPremium_CoverUnnamedFlagVORefID'] = '1'
        data_dictionary['calcPremium_NoOfUnnamed'] = vehicle.seating_capacity
        data_dictionary['calcPremium_PAUnnamedPersonSIVORefID_MST'] = request_data['extra_paPassenger']

    data_dictionary['privateCar_Idv'] = request_data['idv']

    data_dictionary['RegistrationCity'] = "%s-%s" % (split_registration_no[0], split_registration_no[1])

    if not is_new_vehicle:
        if not is_used_vehicle:
            data_dictionary['policyExpiryDate'] = past_policy_end_date.strftime('%d-%m-%Y')
        # Set registration number as passed and set registration city too
        data_dictionary['RegistrationNumber'] = "-".join(split_registration_no)
    else:
        # Set registration number = NEW and set registration city too
        data_dictionary['RegistrationNumber'] = ''

    data_dictionary['newPolicyStartDate'] = new_policy_start_date.strftime("%d-%m-%Y")

    dc = insurer_utils.get_discount_code(request_data.get('discountCode', None))
    if dc:
        discount_from_code = dc.get_discount(INSURER_SLUG, request_data)
        if discount_from_code:
            data_dictionary['specialDiscount'] = discount_from_code
    quote_obj = Quote.objects.get(quote_id=request_data['quoteId'])
    data_dictionary['corporate_employee_discount'] = quote_obj.raw_data.get('corporate_employee_discount', False)
    return data_dictionary


def _get_discount(vehicle, rto_category):
    dls_for_insurer = DiscountLoading.objects.filter(insurer__slug=INSURER_SLUG, vehicle_type=vehicle.vehicle_type)
    dls_for_insurer = dls_for_insurer.filter(Q(state=rto_category) | Q(state=None))

    discount = 0
    dls_f = []

    dls_f = dls_for_insurer.filter(make=vehicle.make_code, model=vehicle.model_code, variant=None)

    if dls_f.count() == 0:
        dls_f = dls_for_insurer.filter(make=vehicle.make_code, model=None, variant=None)

    if dls_f.count() == 0:
        dls_f = dls_for_insurer.filter(make=None, model=None, variant=None)

    dl_make = None
    dl_model = None
    dl_variant = None

    if dls_f.count() == 0:
        discount = 0
    else:
        discount = 0
        if dls_f.count() > 1:
            motor_logger.info('More than one possible discountings found.')

        discount = dls_f[0].discount
        dl_make = dls_f[0].make
        dl_model = dls_f[0].model
        dl_variant = dls_f[0].variant

    motor_logger.info(u'Discount for %s - (%s %s %s) for RTO location %s is %s' % (
        vehicle, dl_make, dl_model, dl_variant, rto_category, discount
    ))
    return discount


def get_discounts(vehicle, rto_id, for_addons=False):
    agent_ids = RTO_ID_TO_AGENT_ID[rto_id]
    imd_code = agent_ids['IMDCode']
    if for_addons:
        risk_based_discount_addon = RISK_BASED_DISCOUNT_FOR_ADDONS
        base_rate_discount_addon = FIXED_BASE_RATE_VARIATION_FOR_ADDONS
        if imd_code in NORTH_IMD_LIST:
            risk_based_discount_addon = 0

        # Hack for l-t for Maruti
        if vehicle.make_code == '1000015':
            risk_based_discount_addon = 0

        return base_rate_discount_addon, risk_based_discount_addon
    else:
        risk_based_discount = 0  # vehicle.risk_based_discount
        base_rate_discount = 0  # FIXED_BASE_RATE_DISCOUNT
        # if imd_code in NORTH_IMD_LIST:
        #    risk_based_discount = max(risk_based_discount, -20)
        #    base_rate_discount = max(base_rate_discount, -10)
        return base_rate_discount, risk_based_discount


#  Method to convert L&T premium response to the required responser.
def convert_premium_response(premium_details, data_dictionary):
    if data_dictionary['corporate_employee_discount']:
        hundred_percent_od = premium_details['MotorOdPremiumWithRoundOff'] * 100 / ( 100 + data_dictionary['specialDiscount'])
        # how to check loading
        if -10 < data_dictionary['specialDiscount'] <= 0:
            premium_details['MotorOdPremiumWithRoundOff'] = hundred_percent_od * (110 + data_dictionary['specialDiscount'] ) / 100  # loading is 10 + data_dictionary['specialDiscount']
        else:
            premium_details['MotorOdPremiumWithRoundOff'] = hundred_percent_od * (90 - data_dictionary['employeeDiscount']) / 100

    basic_cover_mapping = {
        'MotorOdPremiumWithRoundOff': {
            'id': 'basicOd',
            'default': '1',
        },
        'MotorPAOwnerDriver': {
            'id': 'paOwnerDriver',
            'default': '1',
        },
        # 'MotorLLPaidDriver' : {
        #     'id' : 'legalLiabilityDriver',
        #     'default' : '0',
        # },
        'MotorPAUnnamedPassenger': {
            'id': 'paPassenger',
            'default': '1',
        },
        'MotorLLEmployee': {
            'id': 'legalLiabilityEmployee',
            'default': '0',
        },
        'MotorBiFuelkitNetPremium': {
            'id': 'cngKit',
            'default': '1',
        },
        'MotorEEAccessoriesNetPremium': {
            'id': 'idvElectrical',
            'default': '1',
        },
        'MotorNonElectricalAccessoriesNetPremium': {
            'id': 'idvNonElectrical',
            'default': '1',
        },
        'MotorFullTPL': {
            'id': 'basicTp',
            'default': '1',
        },
    }
    # addon_cover_mapping = {
    #     'MotorAntiTheftDeviceNetPremium' : {
    #         'id' : 'isAntiTheftFitted',
    #         'default' : '0',
    #     },
    #     'PcDepWaiverNetPremium' : {
    #         'id' : 'isDepreciationWaiver',
    #         'default' : '0',
    #     },
    #     'ReturnToInvoicePremium' : {
    #         'id' : 'isInvoiceCover',
    #         'default' : '0',
    #     },
    #     'NcbProtectionPremium' : {
    #         'id' : 'isNcbProtection',
    #         'default' : '0',
    #     },
    # }
    addon_cover_mapping = {}
    discount_mapping = {
        'MotorNcbNetPremium': {
            'id': 'ncb',
            'default': '1',
        },
        'MotorDiscretionaryDiscount': {
            'id': 'odDiscount',
            'default': '1',
        },
    }

    special_discount_mapping = {
        'MotorVoluntaryDeductibleNetPremium': {
            'id': 'voluntaryDeductible',
            'default': '1',
        },
    }

    service_tax = premium_details['TotalTaxForCollAmount']
    basic_od_premium = 0.00

    basic_covers = []
    addon_covers = []
    discounts = []
    special_discounts = []

    for premium_id, premium_amount in premium_details.items():
        if premium_id in basic_cover_mapping:
            cover = {
                'id': basic_cover_mapping[premium_id]['id'],
                'default': basic_cover_mapping[premium_id]['default'],
                'premium': premium_amount,
            }
            basic_covers.append(copy.deepcopy(cover))
            if cover['id'] == 'basicOd':
                basic_od_premium = cover['premium']
        elif premium_id in addon_cover_mapping:
            cover = {
                'id': addon_cover_mapping[premium_id]['id'],
                'default': addon_cover_mapping[premium_id]['default'],
                'premium': premium_amount,
            }
            addon_covers.append(copy.deepcopy(cover))
        elif premium_id in discount_mapping:
            cover = {
                'id': discount_mapping[premium_id]['id'],
                'default': discount_mapping[premium_id]['default'],
                'premium': premium_amount,
            }
            discounts.append(copy.deepcopy(cover))
        elif premium_id in special_discount_mapping:
            cover = {
                'id': special_discount_mapping[premium_id]['id'],
                'default': special_discount_mapping[premium_id]['default'],
                'premium': premium_amount,
            }
            special_discounts.append(copy.deepcopy(cover))

    if data_dictionary['calcPremium_NoOfPaidDrivers'] == '1':
        basic_covers.append(
            {
                'id': 'legalLiabilityDriver',
                'default': '0',
                'premium': 50,
            }
        )
    if data_dictionary['corporate_employee_discount']:
        corp_additional_discount = 0.01 * hundred_percent_od * data_dictionary['employeeDiscount']
        cover = {
            'id': 'corporateEmployeeDiscount',
            'default': '1',
            'premium': round(corp_additional_discount),
            'name': 'Corporate Employee Discount'
        }
        special_discounts.append(cover)

    # TODO Temporary To calculate addon value from total #
    total_premium_without_addons = 0.0
    for basic_cover in basic_covers:
        total_premium_without_addons += float(basic_cover['premium'])
    for discount in discounts:
        total_premium_without_addons += float(discount['premium'])
    for special_discount in special_discounts:
        total_premium_without_addons += float(special_discount['premium'])
    total_basic_premium = float(premium_details['TotalBasicPremForCollAmount'])
    total_addon_premium = round(total_basic_premium - total_premium_without_addons, 2)

    if data_dictionary['privateCar_DepreciationWaiverFlagRef'] == '1' and data_dictionary['privateCar_NcbProtectionFlagVORef'] == '1':
        addon_covers.append(
            {
                'id': 'isDepreciationWaiver',
                'default': '0',
                'premium': total_addon_premium,
            }
        )
        addon_covers.append(
            {
                'id': 'isNcbProtection',
                'default': '0',
                'premium': 0,
            }
        )
    elif data_dictionary['privateCar_DepreciationWaiverFlagRef'] == '1' and data_dictionary['privateCar_NcbProtectionFlagVORef'] == '0':
        addon_covers.append(
            {
                'id': 'isDepreciationWaiver',
                'default': '0',
                'premium': total_addon_premium,
            }
        )
    elif data_dictionary['privateCar_NcbProtectionFlagVORef'] == '1' and data_dictionary['privateCar_DepreciationWaiverFlagRef'] == '0':
        addon_covers.append(
            {
                'id': 'isNcbProtection',
                'default': '0',
                'premium': total_addon_premium,
            }
        )

    # Remove zero dep for cardekho if vehicles age is greater than 3 years
    if int(data_dictionary['vehicleAge']) >= 3 and data_dictionary['third_party'] == 'cardekho':
        for data in addon_covers:
            if data['id'] == 'isDepreciationWaiver':
                addon_covers.remove(data)
                break

    ##############################################################

    vehicle = Vehicle.objects.get(id=data_dictionary['VehicleId'])
    try:
        quote_parameters = Quote.objects.get(quote_id=data_dictionary['quoteId']).raw_data
    except:
        quote_parameters = None

    final_response = {
       'insurerId': int(INSURER_ID),
       'insurerSlug': INSURER_SLUG,
       'insurerName': INSURER_NAME,
       'isInstantPolicy': True,
       'minIDV': int(data_dictionary['min_idv']),
       'maxIDV': int(data_dictionary['max_idv']),
       'calculatedAtIDV': int(data_dictionary['privateCar_Idv']),
       'serviceTaxRate': 0.145,
       'premiumBreakup': {
           'serviceTax': round(float(service_tax), 2),
           'basicCovers': insurer_utils.cpr_update(basic_covers),
           'addonCovers': insurer_utils.cpr_update(addon_covers),
           'discounts': insurer_utils.cpr_update(discounts),
           'specialDiscounts': insurer_utils.cpr_update(special_discounts),
           'dependentCovers': get_dependent_addons(quote_parameters),
           'paPassengerAmounts': insurer_utils.get_pa_passenger_dict(vehicle.vehicle_type,
                                                                     vehicle.seating_capacity),
       },
    }
    if data_dictionary.get('bypassInspection', '0') != '0':
        final_response['bypassInspection'] = '1'
    return final_response, premium_details


def convert_proposal_request_data_to_required_dictionary(vehicle, request_data, quote_parameters, quote_response,
                                                         quote_custom_response):
    special_discount_case = quote_response['specialDiscount']
    idv = quote_custom_response['calculatedAtIDV']

    if request_data['cust_gender'] == 'Male':
        request_data['form-insured-title'] = 'Mr.'
    else:
        if request_data['cust_marital_status'] == '0':
            request_data['form-insured-title'] = 'Miss'
        else:
            request_data['form-insured-title'] = 'Mrs.'

    is_new_vehicle = (quote_parameters['isNewVehicle'] == '1')

    if is_new_vehicle:
        request_data['vehicle_reg_no'] = 'NEW'

    is_used_vehicle = (quote_parameters['isUsedVehicle'] == '1')

    # for reg-date and policy-date assignment
    if is_new_vehicle:
        man_date_split = quote_parameters['manufacturingDate'].split('-')
        vehicle_buying_year = man_date_split[2]
        vehicle_buying_month = man_date_split[1]
    else:
        reg_date_split = quote_parameters['registrationDate'].split('-')
        vehicle_buying_year = reg_date_split[2]
        vehicle_buying_month = reg_date_split[1]

    is_expired = False

    if is_new_vehicle or is_used_vehicle:
        new_policy_start_date = datetime.datetime.strptime(quote_parameters['newPolicyStartDate'], "%d-%m-%Y")
        new_policy_end_date = add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)
    else:
        past_policy_end_date = datetime.datetime.strptime(quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")
        past_policy_start_date = subtract_years(
            past_policy_end_date, 1) + datetime.timedelta(days=1)
        new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)
        new_policy_end_date = add_years(past_policy_end_date, 1)

        if past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now():
            is_expired = True
            # No ncb if policy end date before 90 days
            if past_policy_end_date.replace(hour=23, minute=59,
                                            second=0) < datetime.datetime.now() - datetime.timedelta(days=90):
                quote_parameters['isClaimedLastYear'] = '1'

                # landt hack so that the policy does not go into underwriting
                past_policy_end_date = (datetime.datetime.now() - datetime.timedelta(days=85)).replace(hour=0, minute=0, second=0)

                past_policy_start_date = subtract_years(
                    past_policy_end_date, 1) + datetime.timedelta(days=1)

            new_policy_start_date = datetime.datetime.now().replace(hour=0, minute=0, second=0)

            if quote_custom_response.get('bypassInspection', '0') == '1':
                new_policy_start_date = (datetime.datetime.now() + datetime.timedelta(
                    days=4)).replace(hour=0, minute=0, second=0)

            new_policy_end_date = add_years(
                new_policy_start_date, 1) - datetime.timedelta(days=1)

    currentDateTime = datetime.datetime.now()
    currentDateTime = currentDateTime.replace(microsecond=0)

    cust_dob = datetime.datetime.strptime(request_data['cust_dob'], "%d-%m-%Y")

    rto_location = "-".join(quote_parameters['registrationNumber[]'][0:2])
    insurer_rto = RTOMaster.objects.get(rto_code=rto_location).insurer_rtos.get(insurer__slug=INSURER_SLUG)
    rto_id = insurer_rto.rto_code
    rto_zone = insurer_rto.rto_zone

    base_rate_discount, risk_based_discount = get_discounts(vehicle, rto_id)
    if int(quote_response['addon_isDepreciationWaiver']):
        base_rate_discount_addon, risk_based_discount_addon = get_discounts(vehicle, rto_id, True)
    else:
        base_rate_discount_addon, risk_based_discount_addon = 0, 0

    # for registration number assignment
    if is_new_vehicle:
        entered_registration_number = request_data['vehicle_reg_no']
        if entered_registration_number == 'NEW':
            split_rto_location = rto_location.split('-')
            split_registration_number = [split_rto_location[0], split_rto_location[1], '', '']
            license_plate = " ".join(split_rto_location)
        else:
            split_registration_number = entered_registration_number.split('-')
            license_plate = " ".join(split_registration_number)
    else:
        split_registration_number = request_data['vehicle_reg_no'].split('-')
        license_plate = " ".join(split_registration_number)

    date_of_registration = datetime.datetime.strptime(quote_parameters['registrationDate'], "%d-%m-%Y")

    new_policy_start_date = new_policy_start_date.replace(microsecond=0)
    vehicle_age = insurer_utils.get_age(new_policy_start_date, date_of_registration)

    ### ncb calculation
    ncb = '0'
    previous_ncb = '0'
    previous_ncb_to_new_ncb = {
        '0': '20',
        '20': '25',
        '25': '35',
        '35': '45',
        '45': '50',
        '50': '50',
    }
    if not is_new_vehicle:
        if is_used_vehicle:
            previous_ncb = '0'
            ncb = quote_parameters['newNCB'] if quote_parameters['isNCBCertificate'] == '1' else '0'
        else:
            previous_ncb = quote_parameters['previousNCB']
            ncb = '0' if quote_parameters['isClaimedLastYear'] == '1' else previous_ncb_to_new_ncb[previous_ncb]

    bi_fuel_kit_value = '0'
    if quote_parameters['isCNGFitted'] == '1':
        bi_fuel_kit_value = quote_parameters['cngKitValue']

    p_identifier = hashlib.md5()
    p_timestamp = datetime.datetime.now().strftime('%d%m%Y%H%M%S')
    p_identifier.update(p_timestamp)
    proposal_identifier = p_identifier.hexdigest()

    if quote_custom_response.get('bypassInspection', False) == '1':
        if vehicle.make == 'MARUTI':
            base_rate_discount = 10
            special_discount_case = 0
        else:
            special_discount_case = 10
            base_rate_discount = 0
    elif vehicle.make == 'MARUTI':
        swp = base_rate_discount
        base_rate_discount = special_discount_case
        special_discount_case = swp

    # DL *C fix
    if is_new_vehicle is False and license_plate.split(' ')[0].upper() == 'DL' and license_plate.split(' ')[1][-1].upper() == 'C':
        _nl = license_plate.split(' ')
        _nl[1], _nl[2] = _nl[1][:-1], _nl[1][-1]+_nl[2]
        license_plate = ' '.join(_nl)
        split_registration_number = _nl

    is_car_financed = 'No'
    if request_data['is_car_financed'] == '1' or request_data['is_car_financed'] == 'true':
        is_car_financed = 'Hypothecation'

    request_data_dictionary = {
        # PROPOSAL_PRIVATE_CAR_DATA_DICTIONARY
        'proposalPrivateCar_AddressIVO': {
            'proposalPrivateCar_AddressCategoryVORef': 'Urban',
            'proposalPrivateCar_BuildingName': request_data['add_building_name'],
            'proposalPrivateCar_CityName': CITY_ID_TO_CITY_NAME[request_data['add_city']],
            'proposalPrivateCar_CityVORef': request_data['add_city'],
            'proposalPrivateCar_CountryVORef': 'INDIA',
            'proposalPrivateCar_DistrictName': request_data['add_district'],
            'proposalPrivateCar_HouseNr': request_data['add_house_no'],
            'proposalPrivateCar_Landmark1': request_data['add_landmark'],
            'proposalPrivateCar_PostOfficeVORef': ZIP_CODE_TO_POST_OFFICE_MAPPING[request_data['add_pincode']][0][
                'Post_Office_Id'],  # TODO
            'proposalPrivateCar_ProvinceName': request_data['add_state'],
            'proposalPrivateCar_StreetName': request_data['add_street_name'],
            'proposalPrivateCar_Tehsil': None,
            'proposalPrivateCar_ZipCode': request_data['add_pincode'],
        },
        'proposalPrivateCar_ApplicationRecievedDate': currentDateTime.isoformat(),  # system date
        'proposalPrivateCar_ApplicationRecievedTime': "%s:%s" % (currentDateTime.hour, currentDateTime.minute),
    # system time
        'proposalPrivateCar_BIFuelKitValue': bi_fuel_kit_value,
        'proposalPrivateCar_EndorsEndDate': new_policy_end_date.replace(hour=0, minute=0, second=0).isoformat(),
        'proposalPrivateCar_EndorsStartDate': new_policy_start_date.replace(hour=0, minute=0, second=0).isoformat(),
        'proposalPrivateCar_NCBProtectionFlagVORef': quote_response['addon_isNcbProtection'],
        'proposalPrivateCar_PastInsuranceVO': {
            'proposalPrivateCar_ContactExtNum': None,
            'proposalPrivateCar_InsurerAddress': None,
            'proposalPrivateCar_PolicyEndDate': None,
            'proposalPrivateCar_PolicyNo': None,
            'proposalPrivateCar_PolicyStartDate': None,
            'proposalPrivateCar_PrevCoverTypeIdRef': None,
        },
        'proposalPrivateCar_PaymentTermId': 'Yearly',
        'proposalPrivateCar_PolicyContactIVOs': [
            {
                'proposalPrivateCar_policyContactIVOs': {
                    'proposalPrivateCar_ContactExtNum': '',  # TODO
                    'proposalPrivateCar_PolicyContactRoleVORef': 'Intermediary',  # TODO
                },
            },
        ],
        'proposalPrivateCar_PolicyEffectiveTime': '00:00',
        'proposalPrivateCar_PolicyEndDate': new_policy_end_date.replace(hour=0, minute=0, second=0).isoformat(),
        'proposalPrivateCar_PolicyInsuranceTypeVORef': 'Regular',
        'proposalPrivateCar_PolicySigningDate': currentDateTime.isoformat(),
        'proposalPrivateCar_PolicySigningTime': "%s:%s" % (currentDateTime.hour, currentDateTime.minute),
        'proposalPrivateCar_PolicyStartDate': new_policy_start_date.replace(hour=0, minute=0, second=0).isoformat(),
        'proposalPrivateCar_PreInspectionRefNumber': None,
        'proposalPrivateCar_ProposalApplicationDate': currentDateTime.isoformat(),
        'proposalPrivateCar_StatusAttributeVORef': 'Payment Received',
        'proposalPrivateCar_Age': request_data['nominee_age'],
        'proposalPrivateCar_Cc': vehicle.cc,
        'proposalPrivateCar_DepreciationBenefitFlagVORef': quote_response['addon_isDepreciationWaiver'],
        'proposalPrivateCar_DepreciationWaiverFlagRef': quote_response['addon_isDepreciationWaiver'],
        'proposalPrivateCar_EngineNr': request_data['vehicle_engine_no'],
        'proposalPrivateCar_ExpiryDate': None,
        'proposalPrivateCar_ExShowRoomPrice': vehicle.ex_showroom_price,
        'proposalPrivateCar_FuelTypeVORef': vehicle.fuel_type,
        'proposalPrivateCar_FiberTankFlagVORef': '0',
        'proposalPrivateCar_ForHandicappedFlagVORef': '0',
        'proposalPrivateCar_Idv': idv,
        'proposalPrivateCar_IDVElectrical': quote_parameters['idvElectrical'],
        'proposalPrivateCar_IDVNonElectrical': quote_parameters['idvNonElectrical'],
        'proposalPrivateCar_IsKeyReplacementRef': '0',  # quote_parameters['addon_isKeyReplacement'],
        'proposalPrivateCar_IsDriveThroughProtectionForEngineRef': '0',
        # quote_parameters['addon_isDriveThroughProtected'],
        'proposalPrivateCar_LicensePlate': license_plate,
        'proposalPrivateCar_LicensePlateFour': split_registration_number[3],
        'proposalPrivateCar_LicensePlateOne': split_registration_number[0],
        'proposalPrivateCar_LicensePlateThree': split_registration_number[2],
        'proposalPrivateCar_LicensePlateTwo': split_registration_number[1],
        'proposalPrivateCar_LockedParkingVORef': None,
        'proposalPrivateCar_MakeDsc': vehicle.make,
        'proposalPrivateCar_MarketMovement': special_discount_case,
        'proposalPrivateCar_ManufactureVORef': vehicle.make_code,
        'proposalPrivateCar_ManufactureMonthVORef': vehicle_buying_month,
        'proposalPrivateCar_ModelYear': vehicle_buying_year,
        'proposalPrivateCar_ModelDsc': vehicle.model,
        'proposalPrivateCar_ModelVORef': vehicle.model_code,
        'proposalPrivateCar_NameOfNominee': request_data['nominee_name'],
        'proposalPrivateCar_NcbVORef': ncb,
        'proposalPrivateCar_NoOfPaidDrivers': quote_parameters['extra_isLegalLiability'],
        'proposalPrivateCar_NoOfUnnamed': '0',
        'proposalPrivateCar_NormalGeogLimitIdRef': None,
        'proposalPrivateCar_PANamedPersonSIVORef': '0',
        'proposalPrivateCar_PAUnnamedPersonSIVORef': '0',
        'proposalPrivateCar_PaCoverForOwnerDriverFlagVORef': '1',
        'proposalPrivateCar_PaNoOfPaidDrivers': '0',
        'proposalPrivateCar_PaidDriverSi': '0',
        'proposalPrivateCar_PreviousYearNCBVORef': previous_ncb,
        'proposalPrivateCar_RTOLocationVORef': rto_id,
        'proposalPrivateCar_ZoneVORef': rto_zone,
        'proposalPrivateCar_RegistrationDate': date_of_registration.isoformat(),
        'proposalPrivateCar_RegistrationTypeVORef': 'New',
        'proposalPrivateCar_Relationship': request_data['nominee_relationship'],
        'proposalPrivateCar_SeatingCapacity': vehicle.seating_capacity,
        'proposalPrivateCar_ShassisNumber': request_data['vehicle_chassis_no'],
        'proposalPrivateCar_VariantVORef': vehicle.variant_code,
        'proposalPrivateCar_VehicleMortgageTypesVORef': is_car_financed,
        'proposalPrivateCar_VehicleProposalVORef': 'New Vehicle',
        'proposalPrivateCar_VehicleSegmentVORef': vehicle.vehicle_segment,
        'proposalPrivateCar_VoluntaryDeductibleIdRef': quote_parameters['voluntaryDeductible'],
        'proposalPrivateCar_VehicleTotalAge': "%.1f" % vehicle_age,
        'proposalPrivateCar_BaseRateVariation': base_rate_discount,
        'proposalPrivateCar_DiscretionaryDiscount': risk_based_discount,
        'proposalPrivateCar_BaseRateVariationAddOn': base_rate_discount_addon,
        'proposalPrivateCar_RiskBasedDiscountLoading': risk_based_discount_addon,

        # PROPOSAL_TRANSACTION_DATA_DICTIONARY
        'proposalTransaction_Premium': quote_response['TotalPremForCollAmount'],

        # PROPOSAL_CONTACT_DATA_DICTIONARY
        'proposalContact_AddressIVO': {
            'proposalContact_AddressCategoryVORef': 'Urban',
            'proposalContact_BuildingName': request_data['add_building_name'],
            'proposalContact_CityName': CITY_ID_TO_CITY_NAME[request_data['add_city']],
            'proposalContact_CityVORef': request_data['add_city'],
            'proposalContact_CountryVORef': 'INDIA',
            'proposalContact_DistrictName': request_data['add_district'],
            'proposalContact_HouseNr': request_data['add_house_no'],
            'proposalContact_Landmark1': request_data['add_landmark'],
            'proposalContact_Landmark2': '',
            'proposalContact_PostOfficeVORef': ZIP_CODE_TO_POST_OFFICE_MAPPING[request_data['add_pincode']][0][
                'Post_Office_Id'],  # TODO
            'proposalContact_ProvinceName': request_data['add_state'],
            'proposalContact_StreetName': request_data['add_street_name'],
            'proposalContact_Tehsil': None,
            'proposalContact_ZipCode': request_data['add_pincode'],
        },
        'proposalContact_ContactEmail': [
            {
                'proposalContact_contactEmail': {
                    'proposalContact_Email': request_data['cust_email'],
                    'proposalContact_EmailTypeVORef': 'Work',
                },
            },
        ],
        'proposalContact_ContactIdentifier': [
            {
                'proposalContact_contactIdentifier': {
                    'proposalContact_CountryRef': 'INDIA',
                    'proposalContact_IdTypeRef': 'Agreement/MoU Number',
                    'proposalContact_IdValue': proposal_identifier,
                },
            },
        ],
        'proposalContact_ContactTelephone': [
            {
                'proposalContact_contactTelephone': {
                    'proposalContact_CountryDialCodeRef': '91',
                    'proposalContact_TelephoneExtension': '',
                    'proposalContact_TelephoneNumber': request_data['cust_phone'],
                    'proposalContact_TelephonePrefix': '',
                    'proposalContact_TelephoneTypeRef': 'Mobile',
                },
            },
        ],
        'proposalContact_DateOfBirth': cust_dob.isoformat(),
        'proposalContact_TitleVORef': request_data['form-insured-title'],
        'proposalContact_EntityTypeVORef': 'Individual',
        'proposalContact_FatherFirstName': 'Mr.',  # Hack for father's firstname
        'proposalContact_FatherLastName': request_data['cust_last_name'],  # Hack for father's lastname
        'proposalContact_FirstName': request_data['cust_first_name'],
        'proposalContact_GenderVORef': 'Male' if request_data['form-insured-title'] == 'Mr.' else 'Female',
        'proposalContact_MiddleName': '',
        'proposalContact_Name': request_data['cust_last_name'],
    }

    if quote_parameters['third_party'] == 'cardekho' and vehicle_age >= 3:
        request_data_dictionary['proposalPrivateCar_DepreciationBenefitFlagVORef'] = '0'
        request_data_dictionary['proposalPrivateCar_DepreciationWaiverFlagRef'] = '0'

    if request_data['add-same'] == '0':
        request_data_dictionary['proposalPrivateCar_AddressIVO'] = {
            'proposalPrivateCar_AddressCategoryVORef': 'Urban',
            'proposalPrivateCar_BuildingName': request_data['reg_add_building_name'],
            'proposalPrivateCar_CityName': CITY_ID_TO_CITY_NAME[request_data['reg_add_city']],
            'proposalPrivateCar_CityVORef': request_data['reg_add_city'],
            'proposalPrivateCar_CountryVORef': 'INDIA',
            'proposalPrivateCar_DistrictName': request_data['reg_add_district'],
            'proposalPrivateCar_HouseNr': request_data['reg_add_house_no'],
            'proposalPrivateCar_Landmark1': request_data['reg_add_landmark'],
            'proposalPrivateCar_PostOfficeVORef': ZIP_CODE_TO_POST_OFFICE_MAPPING[request_data['reg_add_pincode']][0][
                'Post_Office_Id'],  # TODO
            'proposalPrivateCar_ProvinceName': request_data['reg_add_state'],
            'proposalPrivateCar_StreetName': request_data['reg_add_street_name'],
            'proposalPrivateCar_Tehsil': None,
            'proposalPrivateCar_ZipCode': request_data['reg_add_pincode'],
        }

    if quote_parameters['payment_mode'] == 'CD_ACCOUNT':
        request_data_dictionary['proposalTransaction_AgentReceiptFlag'] = '1'
        request_data_dictionary['proposalTransaction_ReceiptCreatedFlag'] = '1'

    if not is_new_vehicle:
        if is_used_vehicle:
            request_data_dictionary['proposalPrivateCar_VehicleProposalVORef'] = 'Used Vehicle'
            request_data_dictionary['proposalPrivateCar_PrevInsuranceFlagRef'] = '0'
            request_data_dictionary['proposalPrivateCar_InspectionDate'] = new_policy_start_date.replace(hour=0,
                                                                                                         minute=0,
                                                                                                         second=0).isoformat()
            request_data_dictionary['proposalPrivateCar_InspectionDoneFlag'] = '1'
            request_data_dictionary['proposalPrivateCar_InspectionDoneFlagRef'] = '1'
            request_data_dictionary['proposalPrivateCar_InspectionNumber'] = ''
            request_data_dictionary['proposalPrivateCar_InspectionTime'] = '00:00'
        else:
            past_policy_insurer_code = REV_PASTINSURER_MAP[request_data['past_policy_insurer']]
            if is_expired:
                request_data_dictionary['proposalPrivateCar_InspectionDate'] = new_policy_start_date.replace(hour=0,
                                                                                                             minute=0,
                                                                                                             second=0).isoformat()
                request_data_dictionary['proposalPrivateCar_InspectionDoneFlag'] = '1'
                request_data_dictionary['proposalPrivateCar_InspectionDoneFlagRef'] = '1'
                request_data_dictionary['proposalPrivateCar_InspectionTime'] = '00:00'
                request_data_dictionary['proposalPrivateCar_InspectionNumber'] = ''

            if quote_custom_response.get('bypassInspection', '0') == '1':
                request_data_dictionary[
                    'proposalPrivateCar_InspectionDate'] = new_policy_start_date.replace(
                        hour=0, minute=0, second=0).isoformat()
                request_data_dictionary['proposalPrivateCar_InspectionNumber'] = '11111'

            request_data_dictionary['proposalPrivateCar_PastInsuranceVO'] = {
                # 'proposalPrivateCar_ContactExtNum' : request_data['past_policy_insurer'],
                'proposalPrivateCar_ContactExtNum': past_policy_insurer_code,
                'proposalPrivateCar_InsurerAddress': request_data['past_policy_insurer_city'],
                'proposalPrivateCar_PolicyEndDate': past_policy_end_date.isoformat(),
                'proposalPrivateCar_PolicyNo': request_data['past_policy_number'],
                'proposalPrivateCar_PolicyStartDate': past_policy_start_date.isoformat(),
                'proposalPrivateCar_PrevCoverTypeIdRef': request_data['past_policy_cover_type'],
            }
            request_data_dictionary['proposalPrivateCar_VehicleProposalVORef'] = 'Roll Over'
            request_data_dictionary['proposalPrivateCar_PrevInsuranceFlagRef'] = '1'
            # request_data_dictionary['proposalPrivateCar_PreviousYearNCBVORef'] = '0'

    if request_data['is_car_financed'] == '1' or request_data['is_car_financed'] == 'true':
        request_data_dictionary['proposalPrivateCar_MortgageVO'] = {
            'proposalPrivateCar_ContactExtNum': request_data['car-financier'],
            'proposalPrivateCar_MortgageAmount': '999',
            'proposalPrivateCar_MortgageExpirationDate': add_years(
                currentDateTime, 1).replace(hour=0, minute=0, second=0).isoformat(),
            'proposalPrivateCar_MortgageRefNumber': '123456',
            'proposalPrivateCar_MortgageStartDate': (
                currentDateTime.replace(hour=0, minute=0, second=0) + datetime.timedelta(days=1)
            ).isoformat(),
        }
        request_data_dictionary['proposalPrivateCar_MortgageFinancierName'] = FINANCIER_LIST[
            request_data['car-financier']]
        request_data_dictionary['proposalPrivateCar_MortgageFlag'] = '1'

    # For PA to unnamed passenger
    if int(quote_parameters['extra_paPassenger']) > 0:
        request_data_dictionary['proposalPrivateCar_CoverUnnamedFlagVORef'] = '1'
        request_data_dictionary['proposalPrivateCar_NoOfUnnamed'] = vehicle.seating_capacity
        request_data_dictionary['proposalPrivateCar_PAUnnamedPersonSIVORef'] = quote_parameters['extra_paPassenger']

    return request_data_dictionary


def update_transaction_details(transaction, data):
        is_new_vehicle = data['isNewVehicle'] == '1'
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                data['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

            if past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now():
                new_policy_start_date = datetime.datetime.now(
                    ).replace(hour=0, minute=0, second=0)

        new_policy_end_date = add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)

        transaction.policy_start_date = new_policy_start_date
        transaction.policy_end_date = new_policy_end_date
        transaction.save()


def get_dependent_addons(parameters):
    if not parameters:
        return {
            'isNcbProtection': ['isDepreciationWaiver'],
            'type': 'dynamic'
        }

    is_new_vehicle = (parameters['isNewVehicle'] == '1')

    if is_new_vehicle:
        vehicle_age = 0
    else:
        new_policy_start_date = datetime.datetime.strptime(
            parameters['pastPolicyExpiryDate'], "%d-%m-%Y"
        ).replace(microsecond=0) + datetime.timedelta(days=1)
        date_of_registration = datetime.datetime.strptime(parameters['registrationDate'], '%d-%m-%Y')
        vehicle_age = insurer_utils.get_age(new_policy_start_date, date_of_registration)

    if vehicle_age < 3:
        return {}
    else:
        return {
            'isNcbProtection': ['isDepreciationWaiver'],
        }


def get_transaction_from_id(transaction_id):
    transaction = Transaction.objects.filter(transaction_id=transaction_id).latest('id')
    return transaction


def get_city_list(state):
    state_id = RTO_PROVINCE_NAME_TO_STATE_ID.get(state, None)

    return STATE_ID_TO_CITY_LIST.get(state_id, None)


def get_pincode_details(pincode):
    district_name = ZIP_CODE_TO_DISTRICT_NAME_MAPPING.get(pincode, None)
    province_name = DISTRICT_NAME_TO_PROVINCE_NAME_MAPPING.get(district_name, None)
    post_office_details = ZIP_CODE_TO_POST_OFFICE_MAPPING.get(pincode, None)
    if district_name is None or province_name is None or post_office_details is None:
        return None
    return {'pincode': pincode, 'district_name': district_name, 'province_name': province_name}


def resend_proposal_request(transaction_id):
    transaction = Transaction.objects.get(transaction_id=transaction_id)
    policy_resp = ProposalGenerator.get_proposal(transaction)
    return policy_resp


def get_custom_data(datatype):
    datamap = {
        'occupations': OCCUPATION_MAP,
        # 'insurers' : PAST_INSURANCE_ID_LIST,
        'insurers': PAST_INSURER_ID_LIST_MASTER,
        'financiers': FINANCIER_LIST,
        'relationships': NOMINEE_RELATIONSHIP_LIST,
    }
    return datamap.get(datatype, None)


def send_automatic_email(transaction):
    filename, head_keys, list_of_values, ProposalNo = get_landt_user_data.details(transaction)
    putils.render_excel(filename, head_keys, list_of_values, write_to_file=True)
    proposer = transaction.proposer
    # ------------------------------Logic for policy number---------------------------------------------#
    temp_obj = LnTPolicyResponse.objects.filter(TxnId=transaction.reference_id)
    PolicyNo = ''
    if temp_obj:
        PolicyNo = temp_obj.latest('id').TxnStatusDetail.PolicyNo
    # --------------------------------------------------------------------------------------------------#
    subject = 'Coverfox - EDI %s' % PolicyNo
    body = '''
    Name            :   %s %s
    Proposal Number :   %s
    Policy Number   :   %s
    ''' % (proposer.first_name, proposer.last_name, ProposalNo, PolicyNo)
    to_mail = LANDT_RECEIVERS + COVERFOX_RECEIVERS
    if transaction.status == 'FAILED':
        to_mail = COVERFOX_RECEIVERS + ANIMESH
    with default_storage.open(filename, 'rb') as f:
        content = f.read()
    email = EmailMessage(
        subject=subject,
        body=body,
        from_email="rupy@coverfox.com",
        to=to_mail,
        attachments=((os.path.basename(filename),
                      content,
                      None),)
    )
    email.send()


def process_expired_policy(transaction_id):
    return True


def subtract_years(d, years):
    return d - relativedelta(years=years)


def add_years(d, years):
    return d + relativedelta(years=years)
