from spyne.model.primitive import Unicode, Integer
from spyne.model.complex import Iterable
from spyne.model.complex import ComplexModel
from spyne.model.primitive import Integer
from spyne.model.primitive import String
from spyne.model.primitive import Boolean
from spyne.service import ServiceBase


class ErrorDetail(ComplexModel):
    __namespace__ = "cf"

    ErrorLevel = String
    Details = String

class StatusDetail(ComplexModel):
    __namespace__ = "cf"

    ContactId = String
    ReceiptNo = String
    PolicyNo = String
    ProposalNo = String

class TransactionResponse(ComplexModel):
    __namespace__ = "cf"
    _type_info = {
        'return' : Boolean
    }

class Transaction(ComplexModel):
    __namespace__ = "cf"

    TxnId = String
    Status = String
    TxnStatusDetail = StatusDetail
    TxnErrorDetails = ErrorDetail

