from motor_product import models as motor_models
from motor_product import parser_utils

INSURER_SLUG = 'l-t'
INSURER_MASTER_PATH = 'motor_product/insurers/landt/scripts/data/landt_fourwheeler.csv'
MASTER_PATH = 'motor_product/scripts/fourwheeler/data/master_fourwheeler.csv'

csv_parameters = {
    'MAKE': 0,
    'MAKE_ID': 1,
    'MODEL_ID': 2,
    'MODEL': 3,
    'VARIANT_ID': 4,
    'VARIANT': 5,
    'CC': 6,
    'SEATINGCAPACITY': 7,
    'FUELTYPE': 8,
    'VEHICLESEGMENT': 10,
    'SEGMENT_ID': 11,
    'Master': 18,
}

parameters = {
    'make': 0,
    'model': 3,
    'variant': 5,
    'model_code': 2,
    'cc': 6,
    'seating_capacity': 7,
    'fuel_type': 8,
    'ex_showroom_price': 11,
    'vehicle_segment': 10,
    # 'vehicle_code' : 13,
    'make_code': 1,
    'model_code': 2,
    'variant_code': 4,
    # 'risk_based_discount' : 22,
    'master': 18,
}

master_parameters = {
    'make': 0,
    'model': 1,
    'variant': 2,
    'fuel_type': 3,
    'cc': 4,
    'seating_capacity': 5,
    'insurer': 11,  # Refer to master csv
}


def save_data():
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_MASTER_PATH, parameters)
    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)

    vehicle_objects = []
    for _vehicle in vehicles:
        _vehicle['cc'] = parser_utils.try_parse_into_integer(_vehicle['cc'])
        _vehicle['seating_capacity'] = parser_utils.try_parse_into_integer(_vehicle[
                                                                           'seating_capacity'])
        # _vehicle['risk_based_discount'] = -abs(float(_vehicle['risk_based_discount']))
        _vehicle['insurer'] = insurer

        del _vehicle['master']

        vehicle = motor_models.Vehicle(**_vehicle)
        print 'Saving vehicle %s' % vehicle.model
        vehicle_objects.append(vehicle)

    motor_models.Vehicle.objects.bulk_create(vehicle_objects)


def delete_data():
    motor_models.Vehicle.objects.filter(
        vehicle_type='Private Car',
        insurer__slug=INSURER_SLUG
    ).delete()


def delete_mappings():
    motor_models.VehicleMapping.objects.filter(
        insurer__slug=INSURER_SLUG, master_vehicle__vehicle_type='Private Car').delete()


def create_mappings():
    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)
    master_vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        MASTER_PATH, master_parameters)
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_MASTER_PATH, parameters)

    ivehicles = motor_models.Vehicle.objects.filter(
        vehicle_type='Private Car', insurer__slug=INSURER_SLUG)

    vehicle_map = {}
    for iv in vehicles:
        _vehicle = ivehicles.get(make_code=iv['make_code'], model_code=iv[
                                 'model_code'], variant_code=iv['variant_code'])
        vehicle_map[iv['master']] = _vehicle

    mappings = []
    for mv in master_vehicles:
        _master = motor_models.VehicleMaster.objects.get(
            make__name=mv['make'],
            model__name=mv['model'],
            variant=mv['variant'],
            cc=mv['cc'],
            fuel_type=mv['fuel_type'],
            seating_capacity=mv['seating_capacity']
        )
        mapping = motor_models.VehicleMapping()
        mapping.master_vehicle = _master
        mapping.mapped_vehicle = vehicle_map.get(mv['insurer'], None)
        mapping.insurer = insurer

        if mapping.mapped_vehicle:
            print 'Mapping %s to %s' % (_master, mapping.mapped_vehicle)
            mappings.append(mapping)

    motor_models.VehicleMapping.objects.bulk_create(mappings)
