from motor_product import models as motor_models
from motor_product import parser_utils

INSURER_SLUG = 'l-t'
INSURER_MASTER_PATH = 'motor_product/insurers/landt/scripts/data/landt_fourwheeler_dl.csv'

parameters = {
    'imd_code': 0,
    'make_code': 1,
    'model_code': 2,
    'discount': 3,
}


def delete_data():
    motor_models.DiscountLoading.objects.filter(insurer__slug=INSURER_SLUG, vehicle_type='Private Car').delete()


def save_data(parameter):
    discountings = parser_utils.read_file_to_return_vehicle_dictionary(INSURER_MASTER_PATH, parameters)
    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)

    dls = []
    for _discount in discountings:
        dl = motor_models.DiscountLoading()
        dl.insurer = insurer
        dl.make = _discount['make_code']
        dl.model = _discount['model_code']

        if _discount['imd_code'] in ['All', 'all']:
            dl.state = None
        else:
            dl.state = _discount['imd_code']

        if _discount['model_code'] in ['All', 'all']:
            dl.model = None

        if _discount['discount'] in ['decline', 'Decline']:
            dl.discount = None
        else:
            dl.discount = float(_discount['discount'])

        print 'Saving discount for %s %s %s %s' % (dl.state, dl.make, dl.model, dl.discount)
        dls.append(dl)

    motor_models.DiscountLoading.objects.bulk_create(dls)
