
from motor_product import models as motor_models
from motor_product.insurers.landt.scripts import landt_rto_data as landt_dicts

INSURER_SLUG = 'l-t'


def delete_data():
    motor_models.RTOInsurer.objects.filter(insurer__slug=INSURER_SLUG).delete()


def save_data():
    RTO_INFO_DICT = landt_dicts.RTO_ID_TO_RTO_INFORMATION

    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)

    rtos = []
    for rto_code, rto_info in RTO_INFO_DICT.items():
        rto_insurer = motor_models.RTOInsurer()
        rto_insurer.insurer = insurer
        rto_insurer.rto_code = rto_code
        rto_insurer.rto_name = rto_info['RTOLocation']

        rto_state = rto_info['RTOLocation'].split('-')[0]
        rto_insurer.rto_state = rto_state
        rto_insurer.rto_zone = rto_info['ZoneId']

        rto_insurer.master_rto_state = rto_state
        rto_insurer.raw = rto_info
        print "Saving ---- %s - %s" % (rto_info, rto_state)
        rtos.append(rto_insurer)
    motor_models.RTOInsurer.objects.bulk_create(rtos)


def create_map():
    fd = open('motor_product/insurers/landt/scripts/data/landt_rto_map.csv', 'r')
    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)
    insurer_rtos = motor_models.RTOInsurer.objects.filter(insurer__slug=INSURER_SLUG)

    mappings = []
    for line in fd.readlines():
        line = line.split('\n')[0]
        rto_master_code, rto_insurer_code = line.split('|')

        try:
            master_rto = motor_models.RTOMaster.objects.get(rto_code=rto_master_code)
        except:
            continue

        insurer_rto = insurer_rtos.filter(rto_code=rto_insurer_code)[0]
        print 'Mapping %s to %s' % (master_rto, insurer_rto)
        if master_rto and insurer_rto:
            mapping = motor_models.RTOMapping()
            mapping.rto_master = master_rto
            mapping.rto_insurer = insurer_rto
            mapping.insurer = insurer
            mapping.is_approved_mapping = True
            mappings.append(mapping)
    fd.close()
    motor_models.RTOMapping.objects.bulk_create(mappings)


def delete_map():
    motor_models.RTOMapping.objects.filter(insurer__slug=INSURER_SLUG).delete()


def create_mappings():
    return


def delete_mappings():
    return
