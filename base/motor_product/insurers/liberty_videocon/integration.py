import copy
import json
import logging
import math
from xml.etree import ElementTree
from xml.sax.saxutils import escape
import datetime
import time
import json
from django.conf import settings
from django.db.models import Q
from lxml import etree
from suds.client import Client
from suds.sudsobject import Property
import putils
from celery_app import app
from motor_product import logger_utils, mail_utils, parser_utils
from motor_product.insurers import insurer_utils
from motor_product.models import DiscountLoading, Insurer, Quote, Vehicle, IntegrationStatistics, Transaction
from utils import motor_logger, log_error

from . import custom_dictionaries as insurer_data

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)

# ### UAT Credentials ###
# WSDL_URL = 'http://115.112.62.214:1761/IMDCalculatePremiumService.svc?wsdl'
# IMD_CODE = 'BSC5004162'
# IMD_NAME = 'CUSTOMER PORTAL'
# DEAL_ID = 'INTR-3151-1019147'

# PAYMENT_MERCHANT_ID = 'gtKFFx'
# PAYMENT_URL = 'http://115.112.62.214:1761/CustomerPAYU_Test/CustPayu.aspx'

# Test Card Details
# Card Number: 5123456789012346
# CVV: : 123
# Expire: 05/2017

# ### Production Credentials ###
WSDL_URL = 'http://services.libertyvideocon.com/IMDCalculatePremiumService.svc?wsdl'

IMD_CODE = 'IMD1022569'
IMD_NAME = 'Coverfox Insurance Broking Pvt Ltd'
DEAL_ID = 'INTR-3151-1152990'

PAYMENT_MERCHANT_ID = 'QcxsH3'
PAYMENT_URL = 'http://115.112.62.214:1761/CustomerPAYU/CustPayU.aspx'

FOUR_WHEELER_PRODUCT_CODE = '3151'
PARENT_OFFICE_CODE = '9100000'
DISPLAY_OFFICE_CODE = '100000'
DISPLAY_OFFICE_NAME = 'Corporate Office'
TP_SOURCE_NAME = 'COVERFOX'
PAYMENT_SOURCE = 'LVGI_PAYU'

INSURER_SLUG = 'liberty-videocon'
try:
    INSURER = Insurer.objects.get(slug=INSURER_SLUG)
    INSURER_ID = INSURER.id
    INSURER_NAME = INSURER.title
except:
    INSURER_ID = 0
    INSURER_NAME = ''

suds_logger = logger_utils.getLogger(INSURER_SLUG)

IDV_ACC_LOWER_LIMIT = 10000
IDV_ACC_UPPER_LIMIT = 25000

IDV_BIFUEL_LOWER_LIMIT = 15000
IDV_BIFUEL_UPPER_LIMIT = 50000


class CustomerCreater(object):

    client = None

    @classmethod
    def get_client(cls, name=None):
        if cls.client is None:
            cls.client = Client(WSDL_URL)
        cls.client.set_options(plugins=[suds_logger.set_name(name)])
        return cls.client

    @classmethod
    def create_mock_customer(cls):
        first_name = Property('coverfox')
        last_name = Property('coverfox')
        middle_name = Property('coverfox')
        customer_type = Property('I')
        dob = Property('01/01/1990')
        email_id = Property('premium@coverfox.com')
        mobile_no = Property('1234567890')
        salutation = Property('MR')
        tp_source = Property(TP_SOURCE_NAME)
        cust = cls.get_client().service.CreateCustomer(first_name, last_name, middle_name,
                                                       customer_type, dob, email_id, mobile_no, salutation, tp_source)
        return cust

    @classmethod
    def create_customer(cls, request_data):

        state_code = request_data['add_state']
        state_name = insurer_data.STATE_CODE_LIST[state_code]
        pincode = request_data['add_pincode']
        district_code = request_data['add_city']
        district_name = insurer_data.CITY_CODE_TO_CITY_INFO[district_code]['City']

        request_data['form-insured-title'] = 'MR' if request_data['cust_gender'] == 'Male' else (
            'MRS' if request_data['cust_marital_status'] == '1' else 'MS')

        customer_details = {
            'strCustomerType': Property('I'),
            'strSalutation': Property(request_data['form-insured-title']),
            'strFirstName': Property(request_data['cust_first_name']),
            'strLastName': Property(request_data['cust_last_name']),
            'strMiddleName': Property(''),
            'strDOB': Property(datetime.datetime.strptime(request_data['cust_dob'], '%d-%m-%Y').strftime('%d/%m/%Y')),
            'strEmailID': Property(request_data['cust_email']),
            'strPanNo': Property(request_data['cust_pan']),
            'strMobileCode': Property(''),
            'strLandlineNumber': Property(''),
            'strMobileNumber': Property(request_data['cust_phone']),
            'strAddressLine1': Property(request_data['add_house_no'] + ' ' + request_data['add_building_name']),
            'strAddressLine2': Property(request_data['add_street_name'] + ' ' + request_data['add_landmark']),
            'strAddressLine3': Property(''),
            'strArea': Property(''),
            'strCityDistrictCode': Property(district_code),
            'strCityDistrictName': Property(district_name),
            'strStateCode': Property(state_code),
            'strStateName': Property(state_name),
            'strPinCode': Property(pincode),
            'strPinCodeLocality': Property(district_name),
            'strTpSource': Property(TP_SOURCE_NAME),
        }

        cust = cls.get_client(
            request_data['transactionId']).service.CreateFullCustomer(**customer_details)
        cust_dict = insurer_utils.recursive_asdict(cust)
        return cust_dict


class PremiumCalculator(object):

    client = None

    @classmethod
    def get_client(cls, name=None):
        if cls.client is None:
            cls.client = Client(WSDL_URL)
        cls.client.set_options(plugins=[suds_logger.set_name(name)])
        return cls.client

    @classmethod
    def get_premium(cls, data_dictionary):

        idvElectrical = float(data_dictionary['idvElectrical'])
        idvNonElectrical = float(data_dictionary['idvNonElectrical'])
        cngKitValue = float(data_dictionary['cngKitValue'])

        if idvElectrical != 0 and (idvElectrical < IDV_ACC_LOWER_LIMIT or idvElectrical > IDV_ACC_UPPER_LIMIT):
            return {'success': False, 'error_code': IntegrationStatistics.IDV_LIMIT_EXCEEDED}

        if idvNonElectrical != 0 and (idvNonElectrical < IDV_ACC_LOWER_LIMIT or idvNonElectrical > IDV_ACC_UPPER_LIMIT):
            return {'success': False, 'error_code': IntegrationStatistics.IDV_LIMIT_EXCEEDED}

        if data_dictionary['isCNGFitted'] == '1':
            if cngKitValue != 0 and (cngKitValue < IDV_BIFUEL_LOWER_LIMIT or cngKitValue > IDV_BIFUEL_UPPER_LIMIT):
                return {'success': False, 'error_code': IntegrationStatistics.IDV_LIMIT_EXCEEDED}

        if (int(data_dictionary['voluntaryDeductible']) > 0 or
                data_dictionary['extra_isAntiTheftFitted'] == '1' or
                data_dictionary['extra_isMemberOfAutoAssociation'] == '1'):
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_ADD_ONS_RESTRICTED}

        premium_calculation_dict = cls._get_required_params_for_premium_calculation(
            data_dictionary)

        resp = cls.get_client(data_dictionary['quoteId']).service.CalculatePremium(
            **premium_calculation_dict)
        resp_dict = insurer_utils.recursive_asdict(resp)

        if data_dictionary.get('request_idv', None):
            idv = int(float(resp_dict['currentIDVField']))
            min_idv = math.ceil(0.9 * idv)
            max_idv = math.floor(1.1 * idv)

            data_dictionary['idv'] = float(data_dictionary['request_idv'])
            del data_dictionary['request_idv']

            request_idv = data_dictionary['idv']
            if request_idv < min_idv:
                data_dictionary['idv'] = min_idv
            elif request_idv > max_idv:
                data_dictionary['idv'] = max_idv

            data_dictionary['minIdv'] = min_idv
            data_dictionary['maxIdv'] = max_idv

            return cls.get_premium(data_dictionary)

        return cls.convert_premium_response(data_dictionary, resp_dict)

    @classmethod
    def _get_dummy_form_details_for_quote(cls, quote_parameters):
        registration_no = quote_parameters.get('registrationNumber[]')[0:2]
        registration_no.extend(['DA', '1111'])

        form_details = {
            'vehicle_reg_no': "-".join(registration_no),
            'vehicle_engine_no': "DFSDFDS343",
            'vehicle_chassis_no': "SADF3434",
            'past_policy_insurer': '8',
            'past_policy_number': 'N077157CTRLS',
            'customer_id': '4100027632',
            'customer_full_name': 'coverfox coverfox coverfox',
        }

        return form_details

    @classmethod
    def _get_required_params_for_premium_calculation(cls, data_dictionary):

        quote = Quote.objects.get(quote_id=data_dictionary['quoteId'])
        quotation_number = 'CF%s' % quote.id

        form_details = cls._get_dummy_form_details_for_quote(data_dictionary)
        root = XMLGenerator.get_root(data_dictionary, form_details)

        if root is None:
            return None

        request_etree = insurer_utils.convert_complex_dict_to_complex_etree(
            'Root', root)
        request_xml = etree.tostring(request_etree, pretty_print=True)

        premium_calculation_dict = {
            'boolIsFullQuote': False,
            'strQuotationNumber': quotation_number,
            'strRequestXML': request_xml,
            'TPSourceName': TP_SOURCE_NAME,
        }

        return premium_calculation_dict

    @classmethod
    def convert_premium_response(cls, data_dictionary, response_data):
        result_xml = response_data.get('resultXMLField', '')
        parsed_result = ElementTree.fromstring(result_xml)

        result_dict = parser_utils.complex_etree_to_dict(parsed_result)
        response_data['resultDictField'] = result_dict['ServiceResult']

        # Here premium breakup is assigned to the response_data
        result_dict_field = response_data['resultDictField']
        user_data = result_dict_field['GetUserData']
        prop_risks_col = user_data['PropRisks_Col']['Risks']

        basic_covers = []
        addon_covers = []
        discounts = []
        special_discounts = []

        basic_covers_mapping = {
            'Basic - OD': {
                'id': 'basicOd',
                'default': '1',
            },
            'Basic - TP': {
                'id': 'basicTp',
                'default': '1',
            },
            'PA Owner Driver': {
                'id': 'paOwnerDriver',
                'default': '1',
            },
            'Electrical or Electronic Accessories': {
                'id': 'idvElectrical',
                'default': '1',
            },
            'Non-Electrical Accessories': {
                'id': 'idvNonElectrical',
                'default': '1',
            },
            'CNG Kit-OD': {
                'id': 'cngKit',
                'default': '1',
            },
            'CNG Kit-TP': {
                'id': 'cngKitTp',
                'default': '1',
            },
            'Built in CNG - OD loading - OD': {
                'id': 'cngKit',
                'default': '1',
            },
            'Built in CNG-TP Loading-TP': {
                'id': 'cngKitTp',
                'default': '1',
            },
            'LL to Paid Driver IMT 28': {
                'id': 'legalLiabilityDriver',
                'default': '0',
            },
            'Personal Accident Cover-Unnamed': {
                'id': 'paPassenger',
                'default': '1',
            }
        }

        addon_covers_mapping = {}
        discount_mapping = {
            'Bonus Discount': {
                'id': 'ncb',
                'default': '1',
            },
            'Detariff Discount': {
                'id': 'odDiscount',
                'default': '1',
            }
        }

        special_discount_mapping = {
            'Voluntary Excess Discount': {
                'id': 'voluntaryDeductible',
                'default': '1',
            }
        }

        extra_covers = []

        if type(prop_risks_col) is dict:
            prop_risks_col = [prop_risks_col]

        for prop_risk in prop_risks_col:
            covers = prop_risk['PropRisks_CoverDetails_Col'][
                'Risks_CoverDetails']
            if type(covers) is dict:
                covers = [covers]

            for cover in covers:
                if cover['PropCoverDetails_LoadingDiscount_Col']:
                    extra_covers.append(cover['PropCoverDetails_LoadingDiscount_Col'][
                                        'CoverDetails_LoadingDiscount'])

                cover_id = cover['PropCoverDetails_CoverGroups']
                cover_amount = float(cover['PropCoverDetails_Premium'])

                cv_mapping = basic_covers_mapping
                cv = basic_covers

                if cover_id in addon_covers_mapping.keys():
                    cv_mapping = addon_covers_mapping
                    cv = addon_covers
                elif cover_id in discount_mapping.keys():
                    cv_mapping = discount_mapping
                    cv = discounts
                    cover_amount = cover_amount * (-1)
                elif cover_id in special_discount_mapping.keys():
                    cv_mapping = special_discount_mapping
                    cv = special_discounts
                    cover_amount = cover_amount * (-1)

                cover = {
                    'id': cv_mapping[cover_id]['id'],
                    'default': cv_mapping[cover_id]['default'],
                    'premium': cover['PropCoverDetails_Premium'],
                }
                cv.append(copy.deepcopy(cover))

        service_tax = response_data['serviceTaxField']
        total_premium = result_dict_field['TotalPremium']

        discount_loading_dict = response_data['resultDictField'][
            'GetUserData']['PropLoadingDiscount_Col']

        if discount_loading_dict:
            discount_loading_list = discount_loading_dict['LoadingDiscount']

            if type(discount_loading_list) is dict:
                discount_loading_list = [discount_loading_list]

            extra_covers.extend(discount_loading_list)

        for cover in extra_covers:
            cover_id = cover['PropLoadingDiscount_Description']
            cover_amount = float(cover['PropLoadingDiscount_CalculatedAmount'])

            cv_mapping = basic_covers_mapping
            cv = basic_covers

            if cover_id in discount_mapping.keys():
                cv_mapping = discount_mapping
                cv = discounts
                cover_amount = (-1) * cover_amount
            elif cover_id in special_discount_mapping.keys():
                cv_mapping = special_discount_mapping
                cv = special_discounts
                cover_amount = (-1) * cover_amount

            cover = {
                'id': cv_mapping[cover_id]['id'],
                'default': cv_mapping[cover_id]['default'],
                'premium': cover_amount,
            }
            cv.append(copy.deepcopy(cover))

        vehicle = Vehicle.objects.filter(insurer__slug=INSURER_SLUG).get(
            id=data_dictionary['vehicleId'])

        result_data = {
            'insurerId': int(INSURER_ID),
            'insurerSlug': INSURER_SLUG,
            'insurerName': INSURER_NAME,
            'isInstantPolicy': True,
            'minIDV': int(data_dictionary['minIdv']),
            'maxIDV': int(data_dictionary['maxIdv']),
            'calculatedAtIDV': int(data_dictionary['idv']),
            'serviceTaxRate': 0.145,
            'premiumBreakup': {
                'serviceTax': round(float(service_tax), 2),
                'basicCovers': insurer_utils.cpr_update(basic_covers),
                'addonCovers': insurer_utils.cpr_update(addon_covers),
                'discounts': insurer_utils.cpr_update(discounts),
                'specialDiscounts': insurer_utils.cpr_update(special_discounts),
                'paPassengerAmounts': insurer_utils.get_pa_passenger_dict(vehicle.vehicle_type, vehicle.seating_capacity),
            },
        }, {'total_premium': total_premium}
        return {'result': result_data, 'success': True}


class ProposalGenerator(object):

    client = None

    @classmethod
    def get_client(cls, name=None):
        if cls.client is None:
            cls.client = Client(WSDL_URL)
        cls.client.set_options(plugins=[suds_logger.set_name(name)])
        return cls.client

    @classmethod
    def save_policy_details(cls, data_dictionary, transaction):
        if transaction.quote.raw_data['isUsedVehicle'] == '1':
            return False

        data_dictionary['transactionId'] = transaction.transaction_id
        response = CustomerCreater.create_customer(data_dictionary)

        # Sample response for customer creation
        # {'PropertyChanged': None, 'errorTextField': None, 'customerIdField': 4100221780}
        customer_id = response['customerIdField']
        if not customer_id:
            # Proposal failure response mail
            mail_utils.send_mail_for_proposal_failure(transaction, str(response))
            return False

        customer_details = {
            'customer_id': customer_id,
            'customer_full_name': data_dictionary['cust_first_name'] + ' ' + data_dictionary['cust_last_name'],
        }

        transaction.raw['customer_details'] = customer_details
        transaction.raw['quotation_number'] = str(transaction.quote.id) + str(int(time.time()))
        transaction.save()

        # Now make full quote request
        quote_parameters = transaction.quote.raw_data
        quote_parameters['quoteId'] = transaction.quote.quote_id
        quote_parameters['vehicleId'] = data_dictionary['vehicleId']
        quote_parameters['idv'] = transaction.raw['quote_response']['calculatedAtIDV']
        quote_parameters['quotation_number'] = transaction.raw['quotation_number']

        data_dictionary.update(customer_details)
        proposal_dict = cls._get_required_params_for_proposal_generation(
            quote_parameters, data_dictionary)

        resp = cls.get_client(
            transaction.transaction_id).service.CalculatePremium(**proposal_dict)
        resp_dict = insurer_utils.recursive_asdict(resp)

        result_xml = resp_dict.get('resultXMLField', '')
        parsed_result = ElementTree.fromstring(result_xml)

        result_dict = parser_utils.complex_etree_to_dict(parsed_result)
        total_premium = result_dict['ServiceResult']['TotalPremium']

        transaction.proposal_number = proposal_dict['strQuotationNumber']
        transaction.premium_paid = float(total_premium)
        transaction.save()
        return True

    @classmethod
    def _get_required_params_for_proposal_generation(cls, data_dictionary, form_details):
        quotation_number = "CF" + data_dictionary['quotation_number']
        root = XMLGenerator.get_root(data_dictionary, form_details, True)

        request_etree = insurer_utils.convert_complex_dict_to_complex_etree(
            'Root', root)
        request_xml = etree.tostring(request_etree, pretty_print=True)

        proposal_generation_dict = {
            'boolIsFullQuote': True,
            'strQuotationNumber': quotation_number,
            'strRequestXML': request_xml,
            'TPSourceName': TP_SOURCE_NAME,
        }

        return proposal_generation_dict

    @classmethod
    def create_policy(cls, transaction):

        customer_id = transaction.raw['customer_details']['customer_id']
        policy_calculation_dict = {
            '_customerGcId': Property(customer_id),
            'strIMDNumber': Property(IMD_CODE),
            'OfficeCode': Property(PARENT_OFFICE_CODE),
            '_premiumAmount': Property(int(float(transaction.premium_paid))),
            '_productType': Property(FOUR_WHEELER_PRODUCT_CODE),
            '_quotationNumber': Property(transaction.proposal_number),
            '_paymentSource': Property(PAYMENT_SOURCE),
            '_transactionId': Property(transaction.reference_id),
            '_tpSource': Property(TP_SOURCE_NAME),
            '_tpEmailID': Property('motorpolicy@coverfox.com'),
            '_tpSendEmailtoCustomer': Property('true'),
        }

        resp = cls.get_client(transaction.transaction_id).service.CreatePolicy(
            **policy_calculation_dict)
        resp_dict = insurer_utils.recursive_asdict(resp)

        return resp_dict


def convert_premium_request_data_to_required_dictionary(vehicle_id, request_data):
    data_dictionary = copy.deepcopy(request_data)
    data_dictionary['masterVehicleId'] = data_dictionary['vehicleId']
    data_dictionary['vehicleId'] = vehicle_id

    data_dictionary['request_idv'] = data_dictionary['idv']
    data_dictionary['idv'] = '0'
    return data_dictionary


def get_city_list(state):
    cities = []
    for city_code, city_info in insurer_data.CITY_CODE_TO_CITY_INFO.items():
        if city_info['StateCode'] == state:
            cities.append({'id': city_code, 'name': city_info['City']})
    return cities


def get_buy_form_details(quote_parameters, transaction):
    split_registration_no = quote_parameters.get('registrationNumber[]')

    is_new_vehicle = True if quote_parameters['isNewVehicle'] == '1' else False
    if is_new_vehicle:
        split_registration_no = ['NEW', '', '', '']

    registration_number = '-'.join(split_registration_no)
    master_rto_code = "%s-%s" % (
        split_registration_no[0], split_registration_no[1])

    data = {
        'registration_no': registration_number,
        'rto_location_info': master_rto_code,
        # 'insurers_list': insurer_data.PAST_INSURANCE_ID_LIST,
        'insurers_list': insurer_utils.MASTER_INSURER_LIST,
        'disallowed_insurers': insurer_data.DISALLOWED_PAST_INSURERS,
        'relationship_list': insurer_data.NOMINEE_RELATIONSHIP_MAPPING.keys(),
        'state_list': insurer_data.STATE_CODE_LIST,
        'is_cities_fetch': True,
    }
    return data


def convert_proposal_request_data_to_required_dictionary(vehicle, request_data, quote_parameters,
                                                         quote_response, quote_custom_response):
    form_details = copy.deepcopy(request_data)

    is_new_vehicle = False

    if quote_parameters['isNewVehicle'] == '1':
        is_new_vehicle = True

    if is_new_vehicle:
        form_details['vehicle_reg_no'] = 'NEW---'

    form_details['vehicleId'] = vehicle.id
    return form_details


def get_transaction_from_id(transaction_id):
        return Transaction.objects.get(transaction_id=transaction_id)


def update_transaction_details(transaction, data):
        is_new_vehicle = data['isNewVehicle'] == '1'
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                data['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)
            # Check if policy is expired
            if past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now():
                new_policy_start_date = datetime.datetime.now(
                    ).replace(hour=0, minute=0, second=0)

        new_policy_end_date = putils.add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)

        transaction.policy_start_date = new_policy_start_date
        transaction.policy_end_date = new_policy_end_date
        transaction.save()


def get_payment_data(transaction):
    redirect_url = settings.SITE_URL.strip(
        "/") + '/motor/fourwheeler/insurer/%s/payment/response/%s/' % (INSURER_SLUG, transaction.transaction_id)
    timestamp = datetime.datetime.now().strftime('%j%H%M')
    txn_id = '%s%s' % (timestamp, transaction.id)

    transaction.reference_id = txn_id
    transaction.save()

    payment_request_data = {
        'hash': '',
        'txnid': txn_id,
        'key': PAYMENT_MERCHANT_ID,
        'amount': transaction.premium_paid,
        'firstname': transaction.proposer.first_name,
        'email': transaction.proposer.email,
        'phone': transaction.proposer.mobile,
        'lastname': transaction.proposer.last_name,
        'productinfo': 'Liverty Videocon Payment Info',
        'surl': redirect_url,
        'furl': redirect_url,
        'curl': redirect_url,
        'address1': '',
        'address2': '',
        'city': '',
        'state': '',
        'country': '',
        'zipcode': '',
        'udf1': '',
        'udf2': '',
        'udf3': '',
        'udf4': '',
        'udf5': '',
        'pg': '',
    }

    KEYS = ('key', 'txnid', 'amount', 'productinfo', 'firstname', 'email', 'udf1',
            'udf2', 'udf3', 'udf4', 'udf5',  'udf6',  'udf7', 'udf8', 'udf9',  'udf10')
    data_list = []
    for key in KEYS:
        data_list.append(str(payment_request_data.get(key, '')))
    transaction.payment_request = payment_request_data
    transaction.save()
    return payment_request_data, PAYMENT_URL


@app.task
def generate_policy(transaction):
    success = False
    try:
        policy_response = ProposalGenerator.create_policy(transaction)
        motor_logger.info(policy_response)
        # {'PropertyChanged': None, 'errorTextField': None,
        #  'policyNumberField': 201110000015100032100000, 'messageField': None}

        policy_number = policy_response['policyNumberField']
        if policy_number:
            success = True
            transaction.policy_number = str(policy_number)
            transaction.save()
        else:
            message = 'For transaction with id %s (%s), Policy creation failed after payment for Liberty Videocon' % (
                transaction.id, transaction.transaction_id)
            title = 'ALERT: Policy generation failed, failure from Liberty Videocon server'
            mail_utils.send_mail_for_admin({
                'title': title,
                'data': message,
            })

            # Proposal failure response mail
            mail_utils.send_mail_for_proposal_failure(transaction, str(policy_response))
    except:
        log_error(motor_logger, msg=u"After payment error for Liberty ({})".format(transaction.transaction_id))
    finally:
        return success


def post_payment(payment_response, transaction):
    # Sample payment response
    # {
    #     "net_amount_debit": "0.00",
    #     "txnid": "CF20151951533546",
    #     "cardCategory": "domestic",
    #     "zipcode": "",
    #     "unmappedstatus": "failed",
    #     "PG_TYPE": "HDFCPG",
    #     "addedon": "2015-07-14 15:33:59",
    #     "city": "",
    #     "field8": "failed in enrollment",
    #     "field9": " !ERROR!-GW00167-Invalid Currency Code data.Error Code: GW00167",
    #     "udf6": "",
    #     "field2": "",
    #     "field3": "",
    #     "field1": "",
    #     "field6": "",
    #     "field7": "",
    #     "field4": "",
    #     "field5": " !ERROR!-GW00167-Invalid Currency Code data.Error Code: GW00167",
    #     "productinfo": "Liverty Videocon Payment Info",
    #     "lastname": "SHELAR",
    #     "email": "nishant@coverfoxmail.com",
    #     "mode": "CC",
    #     "status": "failure",
    #     "bank_ref_num": "",
    #     "hash": "ed1e2109f2211b3e0688eacf16199e559f93e19f
    #     43679e6b2a4f9295b809750b358a7dc623ebe0f0eaa257834f4d8c853863f9d344e4b2927d53a669ce25d5b3",
    #     "firstname": "NISHANT",
    #     "state": "",
    #     "address1": "",
    #     "address2": "",
    #     "error_Message": "Bank denied transaction on the card.",
    #     "phone": "8888888888",
    #     "key": "C0Dr8m",
    #     "udf8": "",
    #     "discount": "0.00",
    #     "udf10": "",
    #     "mihpayid": "403993715512685076",
    #     "payment_source": "payu",
    #     "udf1": "",
    #     "udf3": "",
    #     "udf2": "",
    #     "udf5": "",
    #     "udf4": "",
    #     "udf7": "",
    #     "error": "E500",
    #     "udf9": "",
    #     "bankcode": "CC",
    #     "amount": "13970.00",
    #     "country": ""
    # }

    motor_logger.info(payment_response)
    suds_logger.set_name(transaction.transaction_id).log(json.dumps(payment_response))
    redirect_url = '/motor/fourwheeler/payment/failure/%s/' % transaction.transaction_id
    is_redirect = True

    payment_response = payment_response['POST']
    transaction.payment_response = payment_response
    if payment_response['status'] == 'success':
        transaction.payment_done = True
        transaction.payment_on = datetime.datetime.now()
        transaction.mark_complete()

        redirect_url = '/motor/fourwheeler/payment/success/%s/' % transaction.transaction_id
        generate_policy.delay(transaction)
    else:
        transaction.status = 'FAILED'
        transaction.save()
    return is_redirect, redirect_url, '', {}


def get_dependent_addons(quote_parameters):
    return {'type': 'dynamic'}


def get_custom_data(datatype):
    datamap = {
        # 'insurers': insurer_data.PAST_INSURANCE_ID_LIST,
        'insurers': insurer_utils.MASTER_INSURER_LIST,
        'states': insurer_data.STATE_CODE_LIST,
        'relationships': insurer_data.NOMINEE_RELATIONSHIP_MAPPING.keys(),
    }
    return datamap.get(datatype, None)


def disable_cache_for_request(request_data):
    if (request_data['isNewVehicle'] != '1' and
        datetime.datetime.strptime(
            request_data['pastPolicyExpiryDate'], '%d-%m-%Y'
            ).replace(hour=23, minute=59, second=0) < datetime.datetime.now()):
        return True
    return False


def _get_discount(vehicle, vehicle_age, rto, ncb):
    rto_split = rto.split('-')
    rto_code = "%s-%02d" % (rto_split[0], int(rto_split[1]))
    rto_category = insurer_data.RTO_CODE_TO_RTO_INFO[rto_code]['Category']
    dls_for_insurer = DiscountLoading.objects.filter(
        insurer__slug=INSURER_SLUG)

    ncbs = list(set(dls_for_insurer.values_list('ncb', flat=True)))
    ncbs.sort()

    if ncb == 0:
        ncb = None

    ncb_slab_max = None
    for ncb_slab_max in ncbs:
        if ncb_slab_max >= ncb:
            break

    ncb_q = Q(ncb=ncb_slab_max)

    age_q0 = Q(lower_age_limit__lte=vehicle_age) & Q(
        upper_age_limit__gt=vehicle_age)
    age_q1 = Q(lower_age_limit=None) & Q(upper_age_limit__gt=vehicle_age)
    age_q2 = Q(lower_age_limit__lte=vehicle_age) & Q(upper_age_limit=None)
    age_q3 = Q(lower_age_limit=None) & Q(upper_age_limit=None)
    age_q = age_q0 | age_q1 | age_q2 | age_q3

    cc_q0 = Q(lower_cc_limit__lt=vehicle.cc) & Q(
        upper_cc_limit__gte=vehicle.cc)
    cc_q1 = Q(lower_cc_limit=None) & Q(upper_cc_limit__gte=vehicle.cc)
    cc_q2 = Q(lower_cc_limit__lt=vehicle.cc) & Q(upper_cc_limit=None)
    cc_q3 = Q(lower_cc_limit=None) & Q(upper_cc_limit=None)
    cc_q = cc_q0 | cc_q1 | cc_q2 | cc_q3

    dls_after_age_cc = dls_for_insurer.filter(ncb_q & age_q & cc_q)

    dls_after_acr = dls_after_age_cc.filter(state=rto_category)
    if dls_after_acr.count() == 0:
        dls_after_acr = dls_after_age_cc.filter(state=None)

    discount = None
    dls_f = dls_after_acr.filter(
        make=vehicle.make_code, model=vehicle.model_code)

    dl_make = None
    dl_model = None
    dl_variant = None

    if dls_f.count() == 0:
        discount = None
    else:
        discount = 0
        if dls_f.count() > 1:
            motor_logger.info('More than one possible discountings found.')
        discount = dls_f[0].discount
        dl_make = vehicle.make
        dl_model = vehicle.model
        dl_variant = vehicle.variant
    motor_logger.info('Discount for %s - (%s %s %s) of age %s for RTO location %s is %s' %
                      (vehicle, dl_make, dl_model, dl_variant, vehicle_age, rto, discount))
    return discount


class XMLGenerator(object):

    @classmethod
    def get_details_dictionary(cls, quote_parameters, form_details):

        vehicle_id = quote_parameters['vehicleId']

        is_new_vehicle = (quote_parameters['isNewVehicle'] == '1')
        vehicle = Vehicle.objects.filter(
            insurer__slug=INSURER_SLUG).get(id=vehicle_id)

        split_registration_no = quote_parameters.get('registrationNumber[]')
        master_rto_code = "%s-%s" % (
            split_registration_no[0], split_registration_no[1])

        # New policy start date and end date assignment
        new_policy_start_time = '00:00'
        is_expired = False
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                quote_parameters['newPolicyStartDate'], "%d-%m-%Y")
            new_policy_end_date = putils.add_years(
                new_policy_start_date, 1) - datetime.timedelta(days=1)

            if new_policy_start_date.date() == datetime.datetime.now().date():
                new_policy_start_time = datetime.datetime.now().strftime('%H:%M')
        else:
            past_policy_end_date = datetime.datetime.strptime(
                quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")

            # Check if policy is expired
            if past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now():
                # No ncb if policy end date before 90 days
                if past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now() - datetime.timedelta(days=90):
                    quote_parameters['isClaimedLastYear'] = '1'

                new_policy_start_date = datetime.datetime.strptime(
                    quote_parameters['newPolicyStartDate'], "%d-%m-%Y")

                past_policy_end_date = new_policy_start_date - datetime.timedelta(days=1)
                is_expired = True
            else:
                new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

            past_policy_start_date = putils.subtract_years(
                past_policy_end_date, 1) + datetime.timedelta(days=1)

            new_policy_end_date = putils.add_years(
                new_policy_start_date, 1) - datetime.timedelta(days=1)

        # Vehicle age calculation
        if is_new_vehicle:
            vehicle_age = 0
        else:
            registration_date = datetime.datetime.strptime(
                quote_parameters['registrationDate'], '%d-%m-%Y')
            new_policy_start_date = new_policy_start_date.replace(
                microsecond=0)
            vehicle_age = insurer_utils.get_age(
                new_policy_start_date, registration_date)

        ncb = '0' if quote_parameters['isClaimedLastYear'] == '1' else insurer_data.PREVIOUS_NCB_TO_NEW_NCB[
            quote_parameters['previousNCB']]
        discount = 0
        loading = 0

        discount_loading = _get_discount(
            vehicle, vehicle_age, master_rto_code, int(ncb))
        if discount_loading is None:
            return None

        discount_loading = float(discount_loading)
        if discount_loading < 0:
            discount = (-1) * discount_loading
            loading = 0
        else:
            discount = 0
            loading = discount_loading

        rto_code = "%s-%02d" % (split_registration_no[0],
                                int(split_registration_no[1]))
        rto_info = insurer_data.RTO_CODE_TO_RTO_INFO[rto_code]

        data_dictionary = {
            'isNewVehicle': is_new_vehicle,
            'registration_date': datetime.datetime.strptime(quote_parameters['registrationDate'], '%d-%m-%Y').strftime('%d/%m/%Y'),
            'manufacturing_month': quote_parameters['manufacturingDate'].split('-')[1],
            'manufacturing_year': quote_parameters['manufacturingDate'].split('-')[2],
            'vehicleId': vehicle.id,
            'idv': quote_parameters['idv'],
            # if quote_parameters['addon_isDepreciationWaiver'] == '0' else
            # True,
            'depreciation_cover': False,
            # if quote_parameters['addon_isPassengerAssist'] == '0' else True,
            'passenger_assistance': False,
            # if quote_parameters['addon_is247RoadsideAssistance'] == '0' else
            # True,
            'roadside_assistance': False,
            'consumable_cover': False,
            'gap_value_cover': False,
            'gap_si': '0',
            'anti_theft': False,
            'is_aai_member': False,
            'aai_name': '',
            'aai_membership_no': '',
            # quote_parameters['voluntaryDeductible'] # Disabled
            'voluntary_excess': '0',
            'unnamed_passengers': vehicle.seating_capacity,
            'unnamed_suminsured': quote_parameters['extra_paPassenger'],
            'extra_isLegalLiability': quote_parameters['extra_isLegalLiability'],
            'plan_for_addon_covers': 'Plan A',
            'tppd_cover': '750000',  # TODO
            'loading_percent': str(loading),
            'discount_percent': str(discount),
            # 'business_source': 'DIRECT',
            'business_source': 'Intermediary',
            'business_source_type': '',
            'policy_schedule': 'One Page',
            'service_tax_exemption_category': 'No Exemption',
            'department_code': '31',  # TODO
            'proposal_date': datetime.datetime.now().strftime("%d/%m/%Y"),
            'sales_person_name': 'GauravChandra Dubey',
            'sales_person_code': 'N0267001',
            'policy_start_date': new_policy_start_date.strftime('%d/%m/%Y'),
            'policy_start_time': new_policy_start_time,
            'policy_expiry_date': new_policy_end_date.strftime('%d/%m/%Y'),
            'rto_code': rto_code,
            'rto_location': rto_info['City'],
            'rto_zone': 'Zone-%s' % rto_info['Zone'],
            'vehicle_engine_no': form_details['vehicle_engine_no'],
            'vehicle_chassis_no': form_details['vehicle_chassis_no'],
            'past_policy_type': '',
            'ncb': '0',
            'previousNCB': '0',
            'business_type': 'New Business',
            'prev_policy_required': 'False',
            'is_vehicle': 'NEW',
            'registration_no': ['NEW', '', '', ''],
            'customer_id': form_details['customer_id'],
            'customer_full_name': form_details['customer_full_name'],
            'financier_agreement': 'Hypothecation',
            'financier_code': '99',
            'financier_name': form_details.get('car-financier', ''),
        }

        if not is_new_vehicle:
            not_new_parameters = {
                'business_type': 'Roll Over',
                'prev_policy_required': 'True',
                'is_vehicle': '',
                'ncb': ncb,
                'previousNCB': quote_parameters['previousNCB'],
                'isClaimedLastYear': quote_parameters['isClaimedLastYear'],
                'past_policy_type': 'PackagePolicy',
                # 'past_policy_insurer': insurer_data.PAST_INSURANCE_ID_LIST[form_details['past_policy_insurer']],
                'past_policy_insurer': insurer_utils.MASTER_INSURER_LIST[form_details['past_policy_insurer']],
                'past_policy_number': form_details['past_policy_number'],
                'past_policy_end_date': past_policy_end_date.strftime('%d/%m/%Y'),
                'past_policy_start_date': past_policy_start_date.strftime('%d/%m/%Y'),
                'registration_no': form_details['vehicle_reg_no'].split('-'),
            }
            data_dictionary.update(not_new_parameters)

        return data_dictionary

    @classmethod
    def _get_nominee_details(cls, form_details):
        nominee_relation = insurer_data.NOMINEE_RELATIONSHIP_MAPPING.get(form_details['nominee_relationship'], 'Other')
        nominee_details = {
            'Name': "PA Owner NomineeDetails",
            'Value': "GRP57",
            'Children': [
                {
                    'PAOwnerNomineeDetails': {
                        'Type': "GroupData",
                        'Children': {
                            'NomineeFirstName': {'Name': "Nominee First Name",
                                                 'Value': form_details['nominee_name'],
                                                 'Type': "String", },
                            'NomineeMiddleName': {'Name': "Nominee Middle Name",
                                                  'Value': "",
                                                  'Type': "String", },
                            'NomineeLastName': {'Name': "Nominee Last Name", 'Value': " ", 'Type': "String", },
                            'NomineeRelationship': {'Name': "Nominee Relationship",
                                                    'Value': nominee_relation,
                                                    'Type': "String", },
                            'OtherRelationship': {'Name': "Other Relationship", 'Value': "", 'Type': "String", },
                            'IsMinor': {'Name': "Is Minor", 'Value': "False", 'Type': "String", },
                            'RepresentativeFirstName': {'Name': "Representative First Name", 'Value': "", 'Type': "String", },
                            'RepresentativeMiddleName': {'Name': "Representative Middle Name", 'Value': "", 'Type': "String", },
                            'RepresentativeLastName': {'Name': "Representative Last Name", 'Value': "", 'Type': "String", },
                            'Relationshipwithminor': {'Name': "Relationship with minor", 'Value': "", 'Type': "String", },
                            'OtherRelationshipminor': {'Name': "Other Relationship Minor", 'Value': "", 'Type': "String", },
                        },
                    },
                },
            ],
        }
        return nominee_details

    @classmethod
    def _get_pa_unnamed_details(cls, sum_insured):
        unnamed_pa_cover = {
            'Name': 'Unnamed PA Cover',
            'Value': '5',
            'Type': 'UnnamedPACoverDetails',
            'Children': {
                'UnnamedPACoverDetails': {
                    'Name': 'GroupData',
                    'Children': {
                        'SI': {'Name': "SI", 'Value': sum_insured, 'Type': "String", },
                    }
                }
            }
        }
        return unnamed_pa_cover

    @classmethod
    def _get_cng_lpg_details(cls, sumInsured):
        cng_lpg_details = {
            'Name': 'CNG',
            'Value': 'GRP169',
            'Children': {
                'BiFuelKitfittedExternallyCNG': {
                    'Type': 'GroupData',
                    'Children': {
                        'Bifueltype': {'Name': 'Bi fuel type', 'Value': 'CNG', 'Type': 'String'},
                        'ManufacturingName': {'Name': 'Manufacturing Name', 'Value': 'Manufacturer', 'Type': 'String'},
                        'ModelNumber': {'Name': 'Model Number', 'Value': 'MODELNUMBER', 'Type': 'String'},
                        'IdentificationNumber': {'Name': 'Identification Number', 'Value': 'IDNUMBER', 'Type': 'String'},
                        'SI': {'Name': 'SI', 'Value': sumInsured, 'Type': 'Double'},
                    }
                }
            }
        }
        return cng_lpg_details

    @classmethod
    def _get_coverage_details(cls, data):
        coverage_data = {
            'Name': data['name'],
            'Value': data['value'],
            'Type': data['type'],
            'Children': {
                data['coverageId']: {
                    'Type': 'GroupData',
                    'Children': {
                        'Make': {'Name': "Make", 'Value': "Make", 'Type': "String", },
                        'Model': {'Name': "Model", 'Value': "Model", 'Type': "String", },
                        'SerialNumber': {'Name': "SerialNumber", 'Value': "1", 'Type': "Double", },
                        'Description': {'Name': "Description", 'Value': data['description'], 'Type': "String", },
                        'YearofManufacture': {'Name': "Year of Manufacture",
                                              'Value': data['manufacturing_year'],
                                              'Type': "Double", },
                        'SI': {'Name': "SI", 'Value': data['sumInsured'], 'Type': "Double", },
                    },
                },
            },
        }
        return coverage_data

    @classmethod
    def _get_financier_details(cls, data_dictionary):
        financier_details = {
            'Children': {
                'FinancierDetails': {
                    'Type': 'GroupData',
                    'Children': {
                        'PropFinancierDetails_Address_Mandatary': {'Name': "Address",
                                                                   'Value': "",
                                                                   'Type': "String", },
                        'PropFinancierDetails_AgreementType': {'Name': "Hire/Lease/Hypothecation",
                                                               'Value': data_dictionary['financier_agreement'],
                                                               'Type': "String", },
                        'PropFinancierDetails_BranchName': {'Value': "", 'Type': "String", },
                        'PropFinancierDetails_City_Mandatary': {'Name': "City Name", 'Value': "", 'Type': "String", },
                        'PropFinancierDetails_CityCode': {'Name': "City Code", 'Value': "", 'Type': "String", },
                        'PropFinancierDetails_District_Mandatary': {'Name': "District Name", 'Value': "", 'Type': "String", },
                        'PropFinancierDetails_DistrictCode': {'Name': "District Code", 'Value': "", 'Type': "String", },
                        'PropFinancierDetails_FinancierCode_Mandatary': {'Name': "Financier Code From GC",
                                                                         'Value': data_dictionary['financier_code'],
                                                                         'Type': "String", },
                        'PropFinancierDetails_FinancierName': {'Name': "Full Name",
                                                               'Value': data_dictionary['financier_name'],
                                                               'Type': "String", },
                        'PropFinancierDetails_IsDataDeleted': {'Value': "False", 'Type': "String", },
                        'PropFinancierDetails_IsOldDataDeleted': {'Value': "False", 'Type': "String", },
                        'PropFinancierDetails_Pincode_Mandatary': {'Name': "Pincode", 'Value': "", 'Type': "String", },
                        'PropFinancierDetails_SrNo_Mandatary': {'Value': "1", 'Type': "String", },
                        'PropFinancierDetails_State_Mandatary': {'Name': "State Name", 'Value': "", 'Type': "String", },
                        'PropFinancierDetails_StateCode': {'Name': "State Code", 'Value': "", 'Type': "String", },
                    },
                },
            },
        }
        return financier_details

    @classmethod
    def _get_previous_policy_details(cls, data_dictionary):
        previous_policy_details = {
            'Children': {
                'PreviousPolicyDetails': {
                    'Type': 'GroupData',
                    'Children': {
                        'PropPreviousPolicyDetails_CorporateCustomerId_Mandatary': {
                            'Name': "Name",
                            'Value': data_dictionary['past_policy_insurer'],
                            'Type': "String",
                        },
                        'PropPreviousPolicyDetails_OfficeAddress': {'Name': "Address", 'Value': "", 'Type': "String", },
                        'PropPreviousPolicyDetails_PolicyNo': {'Name': "Policy/Cover Note Number",
                                                               'Value': data_dictionary['past_policy_number'],
                                                               'Type': "String", },
                        'PropPreviousPolicyDetails_PolicyEffectiveFrom': {
                            'Name': "From Date",
                            'Value': data_dictionary['past_policy_start_date'],
                            'Type': "String",
                        },
                        'PropPreviousPolicyDetails_PolicyEffectiveTo': {
                            'Name': "To Date",
                            'Value': data_dictionary['past_policy_end_date'],
                            'Type': "String",
                        },
                        'PropPreviousPolicyDetails_ClaimAmount': {
                            'Name': "Claim Amount",
                            'Value': "0",
                            'Type': "Double",
                        },
                        'PropPreviousPolicyDetails_NoOfClaims': {
                            'Name': "Number Of claims",
                            'Value': data_dictionary['isClaimedLastYear'],
                            'Type': "Double",
                        },
                        'PropPreviousPolicyDetails_PolicyYear_Mandatary': {
                            'Name': "Year",
                            'Value': "0",
                            'Type': "Double",
                        },
                        'PropPreviousPolicyDetails_ReferenceNo': {
                            'Name': "Inseption Reference Number",
                            'Value': "",
                            'Type': "String",
                        },
                        'PropPreviousPolicyDetails_InspectionDate': {
                            'Name': "Conducted Date HH:MM",
                            'Value': "",
                            'Type': "String",
                        },
                        'PropPreviousPolicyDetails_DocumentProof': {
                            'Name': "Document Proof",
                            'Value': "NCB Renewal Notice",
                            'Type': "String",
                        },
                        'PropPreviousPolicyDetails_InspectionDone': {
                            'Name': "Is Inspection Done?",
                            'Value': "False",
                            'Type': "Boolean",
                        },
                        'PropPreviousPolicyDetails_InspectionDoneByWhom': {
                            'Name': "Inspection Done By Whom",
                            'Value': "Amit",
                            'Type': "String",
                        },
                        'PropPreviousPolicyDetails_DateofLoss': {
                            'Name': "DateofLoss",
                            'Value': "",
                            'Type': "String",
                        },
                        'PropPreviousPolicyDetails_ClaimNo': {'Type': "Double", 'Value': "0", },
                        'PropPreviousPolicyDetails_ClaimPremium': {'Type': "Double", 'Value': "0", },
                        'PropPreviousPolicyDetails_ClaimSettled': {'Type': "Double", 'Value': "0", },
                        'PropPreviousPolicyDetails_ClaimsMode': {'Type': "String", 'Value': "", },
                        'PropPreviousPolicyDetails_DateofSale': {'Type': "String", 'Value': "", },
                        'PropPreviousPolicyDetails_IncurredClaimRatio': {'Type': "Double",
                                                                         'Value': "0", },
                        'PropPreviousPolicyDetails_IsDataDeleted': {'Type': "Boolean",
                                                                    'Value': "False", },
                        'PropPreviousPolicyDetails_IsOldDataDeleted': {'Type': "Boolean",
                                                                       'Value': "False", },
                        'PropPreviousPolicyDetails_NCBAbroadCheck': {'Type': "Boolean",
                                                                     'Value': "False", },
                        'PropPreviousPolicyDetails_NatureofLoss': {'Type': "String", 'Value': "", },
                        'PropPreviousPolicyDetails_OfficeCode': {'Type': "String", 'Value': "", },
                        'PropPreviousPolicyDetails_PolicyPremium': {'Type': "Double", 'Value': "0", },
                        'PropPreviousPolicyDetails_ProductCode': {'Type': "Double", 'Value': "0", },
                        'PropPreviousPolicyDetails_VehicleSold': {'Type': "Boolean", 'Value': "False", },
                    },
                },
            },
        }
        return previous_policy_details

    @classmethod
    def get_root(cls, quote_parameters, form_details, forProposal=False):
        data_dictionary = cls.get_details_dictionary(
            quote_parameters, form_details)

        if data_dictionary is None:
            return None

        vehicle_id = data_dictionary['vehicleId']
        vehicle = Vehicle.objects.filter(
            insurer__slug=INSURER_SLUG).get(id=vehicle_id)

        vehicle_raw_data = vehicle.raw_data.split('|')
        body_type_code = str(vehicle_raw_data[12]).strip()
        body_type = insurer_data.BODY_TYPE_MAP[body_type_code]
        carrying_capacity = vehicle_raw_data[8]
        ex_showroom_price = vehicle_raw_data[31]

        root = {
            'Name': 'PrivateCarPolicy',
            'Code': FOUR_WHEELER_PRODUCT_CODE,
            'Children': {
                'ProposalDetails': {
                    'Children': {
                        'RiskDetails': {
                            'Children': {
                                'Block': {
                                    'Children': {
                                        'PropRisks_RegistrationNumber': {
                                            'Name': "Registration Number",
                                            'Value': data_dictionary['registration_no'][0],
                                            'Type': "String",
                                        },
                                        'PropRisks_RegistrationNumber2': {
                                            'Name': "Registration Number2",
                                            'Value': data_dictionary['registration_no'][1],
                                            'Type': "String",
                                        },
                                        'PropRisks_RegistrationNUmber3': {
                                            'Name': "Registration Number3",
                                            'Value': data_dictionary['registration_no'][2],
                                            'Type': "String",
                                        },
                                        'PropRisks_RegistrationNumber4': {
                                            'Name': "Registration Number4",
                                            'Value': data_dictionary['registration_no'][3],
                                            'Type': "String", },
                                        'PropRisks_DateofRegistration': {
                                            'Name': "Date of Registration",
                                            'Value': data_dictionary['registration_date'],
                                            'Type': "String", },
                                        'PropRisks_AuthorityLocation_Mandatary': {
                                            'Name': "Authority Location",
                                            'Value': data_dictionary['rto_location'],
                                            'Type': "String", },
                                        'PropRisks_YearofManufacture': {
                                            'Name': "Year of Manufacture",
                                            'Value': data_dictionary['manufacturing_year'],
                                            'Type': "Double", },
                                        'PropRisks_Engineno': {
                                            'Name': "Engine No",
                                            'Value': data_dictionary['vehicle_engine_no'],
                                            'Type': "String", },
                                        'PropRisks_Chassisn': {
                                            'Name': "Chassis No",
                                            'Value': data_dictionary['vehicle_chassis_no'],
                                            'Type': "String", },
                                        'PropRisks_HPCubicCapacity_Mandatary': {
                                            'Name': "Cubic Capacity",
                                            'Value': vehicle.cc,
                                            'Type': "String", },
                                        'PropRisks_Manufacture': {
                                            'Name': "Make",
                                            'Value': escape(vehicle.make),
                                            'Type': "String", },
                                        'PropRisks_Make': {
                                            'Name': "Model",
                                            'Value': escape(vehicle.model),
                                            'Type': "String", },
                                        'PropRisks_ModelCode': {
                                            'Name': "Model Code",
                                            'Value': vehicle.model_code,
                                            'Type': "String", },
                                        'PropRisks_Zone_Mandatary': {
                                            'Name': "Rated Under",
                                            'Value': data_dictionary['rto_zone'],
                                            'Type': "String", },
                                        'PropRisks_SeatingCapacity_Mandatary': {
                                            'Name': "Seating Capacity",
                                            'Value': vehicle.seating_capacity,
                                            'Type': "String", },
                                        'PropRisks_Colorofthevehicle': {
                                            'Name': "Color of the Vehicle",
                                            'Value': "",
                                            'Type': "String", },
                                        'PropRisks_IsFleetPolicy': {
                                            'Name': "Is Fleet Policy",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_FleetpolRefNo': {
                                            'Name': "Fleet Policy Ref. Number",
                                            'Value': "",
                                            'Type': "String", },
                                        'PropRisks_IsVehicleImported': {
                                            'Name': "Is Vehicle Imported",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_FuelUsed_Mandatary': {
                                            'Name': "Fuel Type",
                                            'Value': insurer_data.FUEL_TYPE_MAP[vehicle.fuel_type],
                                            'Type': "String", },
                                        'PropRisks_IsVehicle': {
                                            'Name': "Is Vehicle",
                                            'Value': data_dictionary['is_vehicle'],
                                            'Type': "String", },
                                        'PropRisks_VehiclePrimPrkDayTime': {
                                            'Name': "Vehicle Primarily Parked Day Time",
                                            'Value': "",
                                            'Type': "String", },
                                        'PropRisks_VehiclePrimPrk': {
                                            'Name': "Vehicle Primarily Parked Night Time",
                                            'Value': "",
                                            'Type': "String", },
                                        'PropRisks_TypRoadVehiPly': {
                                            'Name': "Type Road Vehicle Ply",
                                            'Value': "",
                                            'Type': "String", },
                                        'PropRisks_VehicleDrvDateKM': {
                                            'Name': "Vehicle Driven As on Date",
                                            'Value': "0",
                                            'Type': "Double", },
                                        'PropRisks_VehicleDrvMonAvg': {
                                            'Name': "Monthly Average",
                                            'Value': "0",
                                            'Type': "Double", },
                                        'PropRisks_IDVofthevehicle': {
                                            'Name': "IDV of the Vehicle",
                                            'Value': data_dictionary['idv'],
                                            'Type': "Double", },
                                        'PropRisks_VehicleExcPvtPur': {
                                            'Name': "Private,Social,Pleasure and Professional Purpose?",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_VehicleExcCarGoods': {
                                            'Name': "Carriage of goods other than sanples or personal",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_IsVehiComm': {
                                            'Name': "Is the vechile is used for commercial purpose?",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_DrvTutionLDChk': {
                                            'Name': "Is the vechile is used for driving tution?",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_LtdOwnPremisesCheck': {
                                            'Name': "Is the vechile is limited to own premises?",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_HandicapLDCheck': {
                                            'Name': "Is the vechile is specially designed \
for use of Blind/Handicapped/Mentally Challenged Person?",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_IsTheCarCeritifiedAsVcVcccI': {
                                            'Name': "Whether the vechile is cartified \
as vintage car by Vintage And Classic Car Club of India?",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_ForeignEmbassyCheck': {
                                            'Name': "Whether the vhicle belongs to the Embassy?",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_DutyElementIdv': {
                                            'Name': "Is the Duty element is included in the IDV?",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_RallyCheck': {
                                            'Name': "Whethre the extension of Rally cover required?",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_Geographicalareaextention': {
                                            'Name': "Do you wish the Geographical Area Extension under insurance cover?",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_Bangladesh': {
                                            'Name': "Bangladesh",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_Bhutan': {
                                            'Name': "Bhutan",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_DepreciationCover': {
                                            'Name': "DepreciationCover",
                                            'Value': data_dictionary['depreciation_cover'],
                                            'Type': "Boolean", },
                                        'PropRisks_PassangerAssistanceCover': {
                                            'Name': "Passanger Assistance Cover",
                                            'Value': data_dictionary['passenger_assistance'],
                                            'Type': "Boolean", },
                                        'PropRisks_RoadSideAssistanceCover': {
                                            'Name': "RoadSide Assistance Cover",
                                            'Value': data_dictionary['roadside_assistance'],
                                            'Type': "Boolean", },
                                        'PropRisks_ConsumableCover': {
                                            'Name': "ConsumableCover",
                                            'Value': data_dictionary['consumable_cover'],
                                            'Type': "Boolean", },
                                        'PropRisks_GAPValueCover': {
                                            'Name': "GAPValue Cover",
                                            'Value': data_dictionary['gap_value_cover'],
                                            'Type': "Boolean", },
                                        'PropRisks_Nepal': {
                                            'Name': "Nepal",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_Srilanka': {
                                            'Name': "Srilanka",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_Maldives': {
                                            'Name': "Maldives",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_Pakistan': {
                                            'Name': "Pakistan",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_AntiTheftCheck': {
                                            'Name': "Is the vechile fitted with Anti Theft device which is approved by ARAI?",
                                            'Value': data_dictionary['anti_theft'],
                                            'Type': "Boolean", },
                                        'PropRisks_CertifiedbyARAI': {
                                            'Name': "Is Certified by ARAI?",
                                            'Value': "False",
                                            'Type': "Boolean", },
                                        'PropRisks_AAMembership': {
                                            'Name': "Are you a member of AA of India?",
                                            'Value': data_dictionary['is_aai_member'],
                                            'Type': "Boolean", },
                                        'PropRisks_AutoMobileAssocName': {
                                            'Name': "Name of the Association",
                                            'Value': data_dictionary['aai_name'],
                                            'Type': "String", },
                                        'PropRisks_AAOmembershipno': {
                                            'Name': "Membership Number",
                                            'Value': data_dictionary['aai_membership_no'],
                                            'Type': "String", },
                                        'PropRisks_Expirydate': {
                                            'Name': "Date of Expiry",
                                            'Value': data_dictionary['policy_expiry_date'],
                                            'Type': "String", },
                                        'PropRisks_VoluntaryExcessAmount': {
                                            'Name': "Voluntary Excess",
                                            'Value': data_dictionary['voluntary_excess'],
                                            'Type': "Double", },
                                        'PropRisks_NoOfPersonsUnnamed': {
                                            'Name': "Number of Passengers",
                                            'Value': data_dictionary['unnamed_passengers'],
                                            'Type': "Double", },
                                        'PropRisks_UnnamedPASI': {
                                            'Name': "SI Per Person",
                                            'Value': data_dictionary['unnamed_suminsured'],
                                            'Type': "Double", },
                                        'PropRisks_GAPCoverSI': {
                                            'Name': "GAPCover SI",
                                            'Value': data_dictionary['gap_si'],
                                            'Type': "Double", },
                                        'PropRisks_PlanForAddonCovers': {
                                            'Name': "Plan For Addon Covers",
                                            'Value': data_dictionary['plan_for_addon_covers'],
                                            'Type': "String", },
                                        'PropRisks_Educationalqualification': {
                                            'Name': "Driver Qualification",
                                            'Value': "",
                                            'Type': "String", },
                                        'PropRisks_Drivingexperience': {
                                            'Name': "Driver Experience",
                                            'Value': "",
                                            'Type': "String", },
                                        'PropRisks_NoOfUnmedPssngrLLCvr': {
                                            'Name': "Unnamed Passsenger",
                                            'Value': "",
                                            'Type': "String", },
                                        'PropRisks_ValidDrivingLicense': {
                                            'Name': "Does the owner holds a valid driving license?",
                                            'Value': "True",
                                            'Type': "Boolean", },
                                        'PropRisks_NoOfEmployees': {
                                            'Name': "Other Employees",
                                            'Value': "0",
                                            'Type': "Double", },
                                        'PropRisks_TPPDCover_Mandatary': {
                                            'Name': "Do you wish to restrict the legel liability towards TP Property damag?",
                                            'Value': data_dictionary['tppd_cover'],
                                            'Type': "Double", },
                                        'PropRisks_VehiclePrimDrivn': {
                                            'Name': "The vechile is primarily driven by",
                                            'Value': "",
                                            'Type': "String", },
                                        'PropRisks_OtherDrvDescrip': {
                                            'Name': "Name",
                                            'Value': "",
                                            'Type': "String", },
                                        'PropRisks_Relationship': {
                                            'Name': "Relationship",
                                            'Value': "",
                                            'Type': "String", },
                                        'PropRisks_RiskStartDate_Mandatary': {
                                            'Name': "Risk Start Date",
                                            'Value': data_dictionary['policy_start_date'],
                                            'Type': "String", },
                                        'PropRisks_Exshowroomprice_Mandatary': {
                                            'Name': "ExShowroomPrice",
                                            'Value': ex_showroom_price,
                                            'Type': "Double", },
                                        'PropRisks_Typeofbody_Mandatary': {
                                            'Name': "BodyType",
                                            'Value': body_type,
                                            'Type': "String", },
                                        'PropRisks_LiscensedCarryingCapacity_Mandatary': {
                                            'Name': "Liscensed Carrying Capacity",
                                            'Value': carrying_capacity,
                                            'Type': "String", },
                                        'PropRisks_ManufactureMonth': {
                                            'Name': "Manufacture Month",
                                            'Value': data_dictionary['manufacturing_month'],
                                            'Type': "Double", },
                                        'PropRisks_ManufactureCode': {
                                            'Name': "Manufacture Code",
                                            'Value': vehicle.make_code,
                                            'Type': "String", },
                                        'PropRisks_RTOCode': {
                                            'Name': "RTO Code",
                                            'Value': data_dictionary['rto_code'],
                                            'Type': "String", },
                                        'PropRisks_Details': {
                                            'Name': "Other Fuel Detais",
                                            'Value': "",
                                            'Type': "String", },
                                        'PropRisks_InbuiltCNGKit': {
                                            'Name': "Inbuilt CNG Kit", "Value": "False",
                                            'Type': "Boolean"},
                                        'PropRisks_NoOfPersonsNamed': {
                                            'Name': "Number of Named Persons",
                                            'Value': '0',
                                            'Type': "Double", },
                                        'PropRisks_RiskVariant': {
                                            'Name': "Cover Type",
                                            'Value': "PackagePolicy",
                                            'Type': "String", },
                                        'PropRisks_Model': {
                                            'Name': "Varient",
                                            'Value': escape(vehicle.variant),
                                            'Type': "String", },
                                        'PropRisks_SegmentType': {
                                            'Name': "Segment Type",
                                            'Value': insurer_data.SEGMENT_TYPE_MAP[vehicle.vehicle_segment],
                                            'Type': "String", },
                                        'PropRisks_Vintagecarfromdate': {
                                            'Name': "vintage car form date",
                                            'Value': "",
                                            'Type': "String", },
                                        'PropRisks_NoOfWorkmen': {
                                            'Name': "Legal Liability towards Driver",
                                            'Value': data_dictionary['extra_isLegalLiability'],
                                            'Type': "Double", },
                                        'PropRisks_DetariffODLoad': {
                                            'Name': "Detariff OD Loading",
                                            'Value': data_dictionary['loading_percent'],
                                            'Type': "Double", },
                                        'PropRisks_DetariffODDis': {
                                            'Name': "Detariff OD Discount",
                                            'Value': data_dictionary['discount_percent'],
                                            'Type': "Double", },
                                    },
                                },
                            },
                        },
                        'GeneralProposalInformation': {
                            'Children': {
                                'PropCustomerDtls_CustomerID_Mandatary': {
                                    'Name': "Customer Id",
                                    'Value': data_dictionary['customer_id'],
                                    'Type': "String", },
                                'PropCustomerDtls_CustomerName': {
                                    'Name': "Customer Full Name",
                                    'Value': data_dictionary['customer_full_name'],
                                    'Type': "String", },
                                # Individual
                                'PropCustomerDtls_CustomerType': {
                                    'Name': "Customer Type",
                                    'Value': "I",
                                    'Type': "String", },
                                'PropPolicyEffectivedate_Fromdate_Mandatary': {
                                    'Name': "Proposed Period of Insurance From Date",
                                    'Value': data_dictionary['policy_start_date'],
                                    'Type': "String", },
                                'PropPolicyEffectivedate_Fromhour_Mandatary': {
                                    'Name': "Proposed Period of Insurance From Time",
                                    'Value': data_dictionary['policy_start_time'],
                                    'Type': "String", },
                                'PropPolicyEffectivedate_Todate_Mandatary': {
                                    'Name': "Proposed Period of Insurance To Date",
                                    'Value': data_dictionary['policy_expiry_date'],
                                    'Type': "String", },
                                'PropPolicyEffectivedate_Tohour_Mandatary': {
                                    'Name': "Proposed Period of Insurance To Time",
                                    'Value': "23:59",
                                    'Type': "String", },
                                'PropIntermediaryDetails_IntermediaryCode': {
                                    'Name': "Intermediary Code",
                                    'Value': IMD_CODE,
                                    'Type': "String", },
                                'PropIntermediaryDetails_IntermediaryName': {
                                    'Name': "Intermediary Name",
                                    'Value': IMD_NAME,
                                    'Type': "String", },
                                'PropGeneralProposalInformation_BusinessType_Mandatary': {
                                    'Name': "Proposal For",
                                    'Value': data_dictionary['business_type'],
                                    'Type': "String", },
                                'PropDistributionChannel_BusinessSource_Mandatary': {
                                    'Name': "Business Source",
                                    'Value': data_dictionary['business_source'],
                                    'Type': "String", },
                                'PropDistributionChannel_BusinessSourcetype': {
                                    'Name': "Business Sourcetype",
                                    'Value': data_dictionary['business_source_type'],
                                    'Type': "String", },
                                'PropGeneralProposalInformation_PolicySchedule_Mandatary': {
                                    'Name': "Policy Schedule",
                                    'Value': data_dictionary['policy_schedule'],
                                    'Type': "String", },
                                'PropGeneralProposalInformation_OfficeCode': {
                                    'Name': "Parent Office Code",
                                    'Value': PARENT_OFFICE_CODE,
                                    'Type': "String", },
                                'PropGeneralProposalInformation_DisplayOfficeCode': {
                                    'Name': "Display Office Code",
                                    'Value': DISPLAY_OFFICE_CODE,
                                    'Type': "String", },
                                'PropGeneralProposalInformation_OfficeName': {
                                    'Name': "Office Name",
                                    'Value': DISPLAY_OFFICE_NAME,
                                    'Type': "String", },
                                'PropGeneralProposalInformation_MethodOfCalculation': {
                                    'Name': "Calculation Method",
                                    'Value': "",
                                    'Type': "String", },
                                'PropCustomerDtls_PayeeCustomerID': {
                                    'Name': "Payee Customer ID",
                                    'Value': data_dictionary['customer_id'],
                                    'Type': "String", },
                                'PropGeneralProposalInformation_ServiceTaxExemptionCategory_Mandatary': {
                                    'Name': "Service Tax Exemption Category",
                                    'Value': data_dictionary['service_tax_exemption_category'],
                                    'Type': "String", },
                                'PropGeneralProposalInformation_CovernoteGenType': {
                                    'Name': "Covernote Gen Type",
                                    'Value': "AUTO",
                                    'Type': "String", },
                                'PropGeneralProposalInformation_BranchOfficeCode': {
                                    'Name': "Branch Office Code - Policy Issuing office Code",
                                    'Value': PARENT_OFFICE_CODE,
                                    'Type': "String", },
                                'PropGeneralProposalInformation_DepartmentCode': {
                                    'Name': "Department Code",
                                    'Value': data_dictionary['department_code'],
                                    'Type': "Double", },
                                'PropGeneralProposalInformation_ProductCode': {
                                    'Name': "Product Code",
                                    'Value': FOUR_WHEELER_PRODUCT_CODE,
                                    'Type': "Double", },
                                'PropDistributionChannel_BusineeChanneltype': {
                                    'Name': "Businee Channel Type",
                                    'Value': 'BROKER',
                                    'Type': "String", },
                                'PropGeneralProposalInformation_iscovernoteused': {
                                    'Name': "Is covernote taken",
                                    'Value': "False",
                                    'Type': "Boolean", },
                                'PropGeneralProposalInformation_covernoteissuetime': {
                                    'Name': "Covernote issue time",
                                    'Value': "",
                                    'Type': "String", },
                                'PropGeneralProposalInformation_CoverNotePlace': {
                                    'Name': "Covernote place",
                                    'Value': "",
                                    'Type': "String", },
                                'PropGeneralProposalInformation_covernoteissuedate': {
                                    'Name': "Covernote issue date",
                                    'Value': "",
                                    'Type': "String", },
                                'PropGeneralProposalInformation_ManualCovernoteNo': {
                                    'Name': "Manual covernote Number",
                                    'Value': "",
                                    'Type': "String", },
                                'PropGeneralProposalInformation_ProposalDate_Mandatary': {
                                    'Name': "Proposed Date",
                                    'Value': data_dictionary['proposal_date'],
                                    'Type': "String", },
                                'PropGeneralProposalInformation_SerDealId': {
                                    'Name': "Servicing Deal ID",
                                    'Value': '',
                                    'Type': "String", },
                                'PropGeneralProposalInformation_ChannelNumber': {
                                    'Name': "Deal ID",
                                    'Value': DEAL_ID,
                                    'Type': "String", },
                                'PropSerIntermediaryDetails_SerIntermediaryCode': {
                                    'Name': "Servicing Intermediary Code",
                                    'Value': '',
                                    'Type': "String", },
                                'PropSerIntermediaryDetails_SerIntermediaryName': {
                                    'Name': "Servicing Intermediary Name",
                                    'Value': '',
                                    'Type': "String", },
                                'PropDistributionChannel_BusinessServicingChannelType': {
                                    'Name': "Business Servicing Channel Type",
                                    'Value': "",
                                    'Type': "String", },
                                'PropSPDetails_SPName': {
                                    'Name': "Sales Persons Name",
                                    'Value': data_dictionary['sales_person_name'],
                                    'Type': "String", },
                                'PropSPDetails_SPCode': {
                                    'Name': "Sales Persons Code",
                                    'Value': data_dictionary['sales_person_code'],
                                    'Type': "String", },
                                'PropPreviousPolicyDetails_PreviousPolicyType': {
                                    'Name': "Past Policy Type",
                                    'Value': data_dictionary['past_policy_type'],
                                    'Type': "String", },
                                'PropPreviousPolicyDetails_NCBPercentage': {
                                    'Name': "NCB Percentage",
                                    'Value': data_dictionary['previousNCB'],
                                    'Type': "Double", },
                                'PropGeneralProposalInformation_NoPrevInsuranceFlag': {
                                    'Name': "Previous Policy Required",
                                    'Value': data_dictionary['prev_policy_required'],
                                    'Type': "Boolean", },
                                'PropMODetails_TertiaryMOCode': {
                                    'Value': "Q_PORTAL",
                                    'Type': "String", },
                                'PropMODetails_TertiaryMOName': {
                                    'Value': "Customer Portal",
                                    'Type': "String", },
                            },
                        },
                    },
                },
                'OtherDetailsGrid': {
                    'Children': {
                    }
                },
            },
        }

        if not data_dictionary['isNewVehicle']:
            root['Children']['ProposalDetails']['Children'][
                'PreviousPolicyDetails'] = cls._get_previous_policy_details(data_dictionary)

        if quote_parameters['idvElectrical'] != '0':
            idvElectrical = {
                'coverageId': 'ElectricalAccessories',
                'name': 'Electrical Accessories',
                'value': 'GRP9',
                'type': 'ElectricalAccessoriesDtls',
                'description': 'Electrical Accessories',
                'manufacturing_year': data_dictionary['manufacturing_year'],
                'sumInsured': quote_parameters['idvElectrical'],
            }
            root['Children']['OtherDetailsGrid']['Children'][
                'ElectricalAccessories'] = cls._get_coverage_details(idvElectrical)

        if quote_parameters['idvNonElectrical'] != '0':
            idvNonElectrical = {
                'coverageId': 'NonElectricalAccessoriesDetails',
                'name': 'Non-Electrical Accessories',
                'value': 'GRP16',
                'type': 'NonElectricalAccessoriesDetails',
                'description': 'Non-Electrical Accessories',
                'manufacturing_year': data_dictionary['manufacturing_year'],
                'sumInsured': quote_parameters['idvNonElectrical'],
            }
            root['Children']['OtherDetailsGrid']['Children'][
                'NonElectricalAccessoriesDetails'] = cls._get_coverage_details(idvNonElectrical)

        if quote_parameters['extra_paPassenger'] != '0':
            root['Children']['OtherDetailsGrid']['Children'][
                'UnnamedPACover'] = cls._get_pa_unnamed_details(quote_parameters['extra_paPassenger'])

        if forProposal:
            root['Children']['OtherDetailsGrid']['Children'][
                'PAOwnerNomineeDetails'] = cls._get_nominee_details(form_details)
            if form_details['is_car_financed'] == '1':
                root['Children']['FinancierDetails'] = cls._get_financier_details(
                    data_dictionary)

        fuel_type = insurer_data.FUEL_TYPE_MAP[vehicle.fuel_type]
        if fuel_type in ['CNG', 'LPG']:
            quote_parameters['isCNGFitted'] = '1'
            quote_parameters['cngKitValue'] = '0'

        if quote_parameters['isCNGFitted'] == '1':
            if quote_parameters['cngKitValue'] != '0':
                root['Children']['OtherDetailsGrid']['Children'][
                    'BiFuelKitfittedExternallyCNG'] = cls._get_cng_lpg_details(quote_parameters['cngKitValue'])
            else:
                # Internal CNG / LPG
                root['Children']['ProposalDetails']['Children']['RiskDetails']['Children'][
                    'Block']['Children']['PropRisks_InbuiltCNGKit']['Value'] = 'True'

        return root
