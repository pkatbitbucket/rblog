from motor_product.models import VehicleMaster, Vehicle, VehicleMapping, Insurer
from motor_product import parser_utils

INSURER_SLUG = 'liberty-videocon'
INSURER_MASTER_PATH = 'motor_product/insurers/liberty_videocon/scripts/data/liberty_fourwheeler.csv'
INSURER_VEHICLE_PRICE_MASTER_PATH = 'motor_product/insurers/liberty_videocon/scripts/data/liberty_fourwheeler_srp.csv'
MASTER_PATH = 'motor_product/scripts/fourwheeler/data/master_fourwheeler.csv'

csv_parameters = {
    'MANUFACTURER': 1,
    'VEHICLEMODELCODE': 2,
    'VEHICLEMODEL': 3,
    'NUMBEROFWHEELS': 4,
    'CUBICCAPACITY': 5,
    'SEATINGCAPACITY': 7,
    'TXT_FUEL': 23,
    'TXT_SEGMENTTYPE': 24,
    'TXT_VARIANT': 25,
    'MANUFACTURERCODE': 26,
    'Master': 31,
}

parameters = {
    'make': 1,
    'model_code': 2,
    'model': 3,
    'cc': 5,
    'number_of_wheels': 4,
    'seating_capacity': 7,
    'variant': 25,
    'make_code': 26,
    'fuel_type': 23,
    'vehicle_segment': 24,
    'master': 31,
}

master_parameters = {
    'make': 0,
    'model': 1,
    'variant': 2,
    'fuel_type': 3,
    'cc': 4,
    'seating_capacity': 5,
    'insurer': 12,  # Refer to master csv
}


def delete_data():
    Vehicle.objects.filter(vehicle_type='Private Car',
                           insurer__slug=INSURER_SLUG).delete()


def delete_mappings():
    VehicleMapping.objects.filter(
        insurer__slug=INSURER_SLUG, master_vehicle__vehicle_type='Private Car').delete()

FUEL_TYPE_MAP = {
    'Petrol': 'Petrol',
    'Diesel': 'Diesel',
    'CNG': 'CNG / Petrol',
    'LPG': 'LPG / Petrol',
    'Electricity': 'Electricity',
}

SEGMENT_TYPE_MAP = {
    'COMPACT': 'Compact Cars',
    'HIGH END': 'High End Cars',
    'MID SIZE': 'Midsize Cars',
    'MUV': 'Multi-utility Vehicles',
    'SMALL': 'Small Sized Vehicles',
    'SUV': 'Sports and Utility Vehicles',
}


def _create_vehicle_prices_map():
    prices = parser_utils.read_file_to_return_vehicle_dictionary(INSURER_VEHICLE_PRICE_MASTER_PATH, {
        'ex_showroom_price': 4,
        'make_code': 2,
        'model_code': 3,
        'pg_id': 16,
    })
    mapping = {}
    for price in prices:
        if not mapping.get(price['make_code'], None):
            mapping[price['make_code']] = {}

        if not mapping[price['make_code']].get(price['model_code'], None):
            mapping[price['make_code']][price['model_code']] = {}

        if not mapping[price['make_code']][price['model_code']].get(int(price['pg_id']), None):
            mapping[price['make_code']][price['model_code']][
                int(price['pg_id'])] = price['ex_showroom_price']

    return mapping


def save_data():
    ex_showroom_price_map = _create_vehicle_prices_map()

    insurer = Insurer.objects.get(slug=INSURER_SLUG)
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_MASTER_PATH, parameters)

    vehicle_objects = []
    for _vehicle in vehicles:
        _vehicle['cc'] = parser_utils.try_parse_into_integer(_vehicle['cc'])

        fuel_type = FUEL_TYPE_MAP.get(_vehicle['fuel_type'], 'Not Defined')
        _vehicle['fuel_type'] = fuel_type

        vehicle_segment = SEGMENT_TYPE_MAP.get(
            _vehicle['vehicle_segment'], 'Not Defined')
        _vehicle['vehicle_segment'] = vehicle_segment

        seating_capacity = parser_utils.try_parse_into_integer(
            _vehicle['seating_capacity'])
        _vehicle['seating_capacity'] = seating_capacity

        _vehicle['insurer'] = insurer
        _vehicle['vehicle_type'] = 'Private Car'

        priceStr = ''
        try:
            for index in range(1, 5):
                priceStr += '|' + \
                    ex_showroom_price_map[_vehicle['make_code']][
                        _vehicle['model_code']][index]
        except:
            priceStr = ''

        _vehicle['raw_data'] += priceStr

        del _vehicle['master']

        vehicle = Vehicle(**_vehicle)
        vehicle_objects.append(vehicle)
        print 'Saving vehicle %s' % vehicle.model

    Vehicle.objects.bulk_create(vehicle_objects)


def create_mappings():
    insurer = Insurer.objects.get(slug=INSURER_SLUG)
    master_vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        MASTER_PATH, master_parameters)
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_MASTER_PATH, parameters)

    ivehicles = Vehicle.objects.filter(
        vehicle_type='Private Car', insurer__slug=INSURER_SLUG)

    vehicle_map = {}
    for iv in vehicles:
        _vehicle = ivehicles.get(
            make_code=iv['make_code'], model_code=iv['model_code'])
        vehicle_map[iv['master']] = _vehicle

    mappings = []
    for mv in master_vehicles:
        _master = VehicleMaster.objects.get(make__name=mv['make'], model__name=mv['model'],
                                            variant=mv['variant'], cc=mv['cc'],
                                            fuel_type=mv['fuel_type'],
                                            seating_capacity=mv['seating_capacity'])
        mapping = VehicleMapping()
        mapping.master_vehicle = _master
        mapping.mapped_vehicle = vehicle_map.get(mv['insurer'], None)
        mapping.insurer = insurer

        if mapping.mapped_vehicle:
            print 'Mapping %s to %s' % (_master, mapping.mapped_vehicle)
            mappings.append(mapping)

    VehicleMapping.objects.bulk_create(mappings)
