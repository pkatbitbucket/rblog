import copy
from motor_product.models import DiscountLoading, Insurer
from motor_product import parser_utils

INSURER_SLUG = 'liberty-videocon'
DISCOUNTING_PATH = 'motor_product/insurers/liberty_videocon/scripts/data/liberty_fourwheeler_dl.csv'

parameters = {
    'make': 2,
    'make_code': 3,
    'model': 4,
    'variant': 5,
    'model_code': 6,
    'rto_category': 1,
    'lower_age_limit': 8,
    'upper_age_limit': 9,
    'discount': 10,
    'non_ncb_discount': 11
}


def save_data(parameter):
    discountings = parser_utils.read_file_to_return_vehicle_dictionary(
        DISCOUNTING_PATH, parameters)
    insurer = Insurer.objects.get(slug=INSURER_SLUG)

    dlos = []
    for dl in discountings:
        dlo = DiscountLoading()
        dlo.insurer = insurer
        dlo.state = dl['rto_category']
        dlo.make = dl['make_code']
        dlo.model = dl['model_code']

        dlo.lower_age_limit = int(dl['lower_age_limit'])
        dlo.upper_age_limit = int(dl['upper_age_limit'])
        dlo.ncb = 65

        discount = float(dl['discount'])
        non_ncb_discount = float(dl['non_ncb_discount'])
        dlo.discount = discount
        print 'Saving discounting for %s %s %s %s with NCB' % (dl['make'], dl['model'], dl['variant'], dl['discount'])
        dlos.append(dlo)

        dlo_noncb = copy.deepcopy(dlo)
        dlo_noncb.discount = non_ncb_discount
        dlo_noncb.ncb = None
        print 'Saving discounting for %s %s %s %s without NCB' % (dl['make'], dl['model'], dl['variant'],
                                                                  float(dl['non_ncb_discount']))
        dlos.append(dlo_noncb)

    DiscountLoading.objects.bulk_create(dlos)


def delete_data():
    DiscountLoading.objects.filter(
        insurer__slug=INSURER_SLUG, vehicle_type='Private Car').delete()
