from . import xml_data
import math
import copy
import datetime
import json
import base64
import httplib2
import putils
from django.conf import settings
from lxml import etree
from utils import motor_logger
from motor_product import logger_utils, parser_utils
from motor_product.insurers import insurer_utils
from motor_product.models import Insurer, Vehicle, Transaction, IntegrationStatistics
from . import custom_dictionaries as cust_data
from payment_gateway.gateways.billdesk import Billdesk

if settings.PRODUCTION:
    # PRODUCTION Credentials
    USER_ID = '********'
    PASSWORD = '********'
    URL = '***********************'
else:
    # UAT Credentials
    USER_ID = 'coverfox'
    PASSWORD = 'Nia@12345'
    URL = 'https://uatapps.newindia.co.in/B2B/CoverFox'

AUTH_DATA = 'Basic ' + base64.b64encode(USER_ID + ':' + PASSWORD)
HEADER = {
    'Authorization': AUTH_DATA,
    'Content-Type': 'text/xml; charset="utf-8"'
}
USER_CODE = 'BRCOVERFOX'
ROLE_CODE = 'SUPERUSER'
STAKE_CODE = 'BROKER'
CHANNEL_CODE = 'COVERFOX'
PRODUCT_TYPE = {
    'Private Car': {
        'productName': 'PRIVATE CAR',
        'productCode': 'PC',
        'productId': '194398713122007'
    },
    'Twowheeler': {
        'productName': 'Two Wheeler',
        'productCode': 'TW',
        'productId': '194731914122007'
    }
}

VEHICLE_UPPER_AGE_LIMIT = 10

INSURER_SLUG = 'new-india'
try:
    INSURER = Insurer.objects.get(slug=INSURER_SLUG)
except:
    INSURER = None

suds_logger = logger_utils.getLogger(INSURER_SLUG)

header = {
    'Authorization': AUTH_DATA,
    'Content-Type': 'text/xml; charset="utf-8"'
}

previous_ncb_to_new_ncb = {
    '0': 20,
    '20': 25,
    '25': 35,
    '35': 45,
    '45': 50,
    '50': 50,
}

PARSER = etree.XMLParser(remove_blank_text=True)


def xml_to_dict(xml_data):
    parsed_xml_response = etree.fromstring(xml_data)
    response_dict = parser_utils.complex_etree_to_dict(parsed_xml_response)
    response_dict = insurer_utils.remove_namespace_from_dict(response_dict)
    return response_dict


def get_response(name, data_xml):
    motor_logger.info('\n====%s====\nNEW-INDIA REQUEST SENT:\n%s\n' % (name, data_xml))
    response_header, response_content = httplib2.Http().request(
        URL,
        method='POST',
        body=data_xml,
        headers=HEADER
    )
    motor_logger.info('\n====%s====\nNEW-INDIA RESPONSE:\n%s\n' % (name, response_content))
    return response_content


def remove_namespace_listdict(data):
    for key in data:
        if type(data[key]) is list:
            for index in range(len(data[key])):
                if type(data[key][index]) is dict:
                    data[key][index] = insurer_utils.remove_namespace_from_dict(data[key][index])

    return data


def get_premium_breakup_dict(data):
    premium_data_dict = {}
    for d in data:
        premium_data_dict[d['name']] = d['value']

    return premium_data_dict


def get_idv_index(age):
    index = 0
    if int(age) > 0:
        if int(age) < 9:
            index = 1 + int(age)
        else:
            index = 10
    else:
        index = 0
        if int(age * 10) >= 6:
            index = 1

    return index


class Configuration(object):

    def __init__(self, vehicle_type):
        self.VEHICLE_TYPE = vehicle_type
        self.PRODUCT_NAME = PRODUCT_TYPE[vehicle_type]['productName']
        self.PRODUCT_CODE = PRODUCT_TYPE[vehicle_type]['productCode']
        self.PRODUCT_ID = PRODUCT_TYPE[vehicle_type]['productId']


class PremiumCalculator(object):

    def __init__(self, integrationUtils, configuration):
        self.parent = integrationUtils
        self.Configuration = configuration

    def get_premium(self, data):
        if data['isUsedVehicle'] == '1':
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_USED_VEHICLE_RESTRICTED}

        if math.ceil(float(data['vehicle_age'])) >= VEHICLE_UPPER_AGE_LIMIT:
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_AGE_RESTRICTED}

        if data['isNewVehicle'] == '0':
            past_policy_end_date = datetime.datetime.strptime(data['pastPolicyExpiryDate'], "%d-%m-%Y")
            if past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now():
                motor_logger.info('Expired policy not allowed for this insurer %s.' % INSURER_SLUG)
                return {'success': False, 'error_code': IntegrationStatistics.INSURER_EXPIRED_POLICY_RESTRICTED}

        vehicle = Vehicle.objects.filter(
            insurer__slug=INSURER_SLUG).get(id=data['vehicleId'])

        idv_index = get_idv_index(data['vehicle_age'])
        vehicle_idv = vehicle.raw_data.split('|')[7 + idv_index]
        idv = int(vehicle_idv)
        if idv == 0:
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_NO_IDV}

        if int(data.get('idv', '0')) == 0:
            data['min_idv'] = int(idv * 0.95)
            data['max_idv'] = int(idv * 1.05)
            data['idv'] = data['min_idv']

        xmldata = self._get_premium_data(data, vehicle)
        resp = get_response(name=data['vehicleId'], data_xml=xmldata)
        resp = xml_to_dict(resp)
        response_dict = resp['Envelope']['Body']['calculatePremiumMasterResponseElement']

        response_dict = remove_namespace_listdict(response_dict)
        premium_data_response_dict = get_premium_breakup_dict(response_dict['properties'])

        if int(data.get('req_idv', '0')) != 0:
           min_idv = int(math.ceil(0.9 * idv))
           max_idv = int(math.floor(1.1 * idv))

           request_idv = float(data['req_idv'])

           if request_idv < float(min_idv):
               modified_idv = min_idv
           elif request_idv > float(max_idv):
               modified_idv = max_idv
           else:
               modified_idv = request_idv

           data['idv'] = modified_idv
           data['min_idv'] = min_idv
           data['max_idv'] = max_idv
           del data['req_idv']

           return self.get_premium(data)

        return self.parent.convert_premium_response(response_dict, data, vehicle, premium_data_response_dict)

    def _get_premium_data(self, data, vehicle):
        total_idv_electrical = int(data['idvElectrical']) + int(data['idvNonElectrical'])
        ex_shrm_price = vehicle.raw_data.split('|')[6]
        idv_index = get_idv_index(data['vehicle_age'])
        vehicle_idv = data['idv']
        total_idv = total_idv_electrical + int(vehicle_idv)

        zone = 'B'
        if data['rto_code'] in cust_data.ZONE_A_RTO_LIST:
            zone = 'A'

        rta_map = cust_data.RTO_ADDRESS_MAP[data['rto_code']]
        rta_addr = rta_map['RTA_NAME'] + ',' + rta_map['RTA_ADDRESS']

        if self.Configuration.PRODUCT_NAME == 'Two Wheeler':
            xml_data_tuple = (
                ("productName", self.Configuration.PRODUCT_NAME),
                ("eventDate", datetime.datetime.now().strftime('%d/%m/%Y')),
                ("polInceptiondate", data['policy_start_date']),
                ("polStartdate", data['policy_start_date']),
                ("polExpirydate", data['policy_end_date']),
                ("productCode", self.Configuration.PRODUCT_CODE),
                ("productId", self.Configuration.PRODUCT_ID),
                ("productCode", self.Configuration.PRODUCT_CODE),
                ("coverCode", 'PACKAGE'),  # Need to add logic for covercode
                ("no_unpa_value", vehicle.seating_capacity),
                ('si_individ_value', data['extra_paPassenger']),
                ('si_capital_value', str(int(vehicle.seating_capacity) * int(data['extra_paPassenger']))),
                ('ll_value', data['extra_isLegalLiability']),
                ('pa_unnamed', 'N' if data['extra_paPassenger'] == '0' else 'Y'),
                ('ll_paid_driver', 'Y' if data['extra_isLegalLiability'] == '1' else 'N'),
                ('manf_year_value', data['manufacturing_date'].split('-')[-1]),
                ('new_vehicle_value', 'Y' if data['isNewVehicle'] == '1' else 'N'),
                ('reg_value', 'NEW' if data['isNewVehicle'] == '1' else 'MH'),
                ('reg_value', '0' if data['isNewVehicle'] == '1' else '01'),
                ('reg_value', 'none' if data['isNewVehicle'] == '1' else 'AA'),
                ('reg_value', '0001' if data['isNewVehicle'] == '1' else '0001'),
                ('sale_date_value', data['registration_date']),
                ('reg_date_value', data['registration_date']),
                ('elec_access_vaue', data['idvElectrical']),
                ('bifuel_value', data['cngKitValue']),
                ('is_inbuilt_cng', 'Y' if (data['isCNGFitted'] == '1' and data['cngKitValue'] == '0') else 'N'),
                ('make_value', vehicle.make),
                ('model_value', vehicle.model),
                ('is_anti-theft', 'Y' if data['extra_isAntiTheftFitted'] == '1' else 'N'),
                ('zone_value', zone),
                ('variant_value', vehicle.variant),
                ('RTA Address', rta_addr),
                ('cc_value', vehicle.cc),
                ('seating_value', vehicle.seating_capacity),
                ('fuel_type_value', 'CNGPetrol' if data['isCNGFitted'] == '1' else vehicle.fuel_type),
                ('ex_showroom_value', ex_shrm_price),
                ('age_value', str(int(data['vehicle_age']))),
                ('elec_access_bool_value', 'N' if data['idvElectrical'] == '0' else 'Y'),
                ('nonelec_access_bool_value', 'N' if data['idvNonElectrical'] == '0' else 'Y'),
                ('nonelec_access_vaue', data['idvNonElectrical']),
                ('idv_vaue', vehicle_idv),
                ('access_idv_value', total_idv_electrical),
                ('total_idv_value', total_idv),
                ('ncb_appl_value', data['ncb_applicabe']),
                ('ncb_prev_value', data['ncb']),
                ('ncb_appl_prev_str', '% o'),
                ('past_pol_exp_value', '01/01/9999' if data[
                    'isNewVehicle'] == '1' else data['past_policy_end_date']),
                ('pol_excess_val', '100'),  # Need to make 100 for TW and 1000 for Fourwheeler
                ('voluntary_excess', '0' if data['voluntaryDeductible'] == '0' else data['voluntaryDeductible']),
            )
        else:
            xml_data_tuple = (
                ("productName", self.Configuration.PRODUCT_NAME),
                ("eventDate", datetime.datetime.now().strftime('%d/%m/%Y')),
                ("polInceptiondate", data['policy_start_date']),
                ("polStartdate", data['policy_start_date']),
                ("polExpirydate", data['policy_end_date']),
                ("term", '1'),
                ("productCode", self.Configuration.PRODUCT_CODE),
                ("productId", self.Configuration.PRODUCT_ID),
                ("productCode", self.Configuration.PRODUCT_CODE),
                ("coverCode", 'PACKAGE'),  # Need to add logic for covercode
                ("no_unpa_value", vehicle.seating_capacity),
                ('si_individ_value', data['extra_paPassenger']),
                ('si_capital_value', str(int(vehicle.seating_capacity) * int(data['extra_paPassenger']))),
                ('ll_value', data['extra_isLegalLiability']),
                ('pa_unnamed', 'N' if data['extra_paPassenger'] == '0' else 'Y'),
                ('ll_paid_driver', 'Y' if data['extra_isLegalLiability'] == '1' else 'N'),
                ('manf_year_value', data['manufacturing_date'].split('-')[-1]),
                ('new_vehicle_value', 'Y' if data['isNewVehicle'] == '1' else 'N'),
                ('reg_value', 'NEW' if data['isNewVehicle'] == '1' else 'MH'),
                ('reg_value', '0' if data['isNewVehicle'] == '1' else '01'),
                ('reg_value', 'none' if data['isNewVehicle'] == '1' else 'AA'),
                ('reg_value', '0001' if data['isNewVehicle'] == '1' else '0001'),
                ('sale_date_value', data['registration_date']),
                ('reg_date_value', data['registration_date']),
                ('elec_access_vaue', data['idvElectrical']),
                ('bifuel_value', data['cngKitValue']),
                ('is_inbuilt_cng', 'Y' if (data['isCNGFitted'] == '1' and data['cngKitValue'] == '0') else 'N'),
                ('make_value', vehicle.make),
                ('model_value', vehicle.model),
                ('is_anti-theft', 'Y' if data['extra_isAntiTheftFitted'] == '1' else 'N'),
                ('zone_value', zone),
                ('variant_value', vehicle.variant),
                ('RTA Address', rta_addr),
                ('cc_value', vehicle.cc),
                ('seating_value', vehicle.seating_capacity),
                ('fuel_type_value', 'CNGPetrol' if data['isCNGFitted'] == '1' else vehicle.fuel_type),
                ('ex_showroom_value', ex_shrm_price),
                ('age_value', str(int(data['vehicle_age']))),  # Need to calculate in months
                ('elec_access_bool_value', 'N' if data['idvElectrical'] == '0' else 'Y'),
                ('nonelec_access_bool_value', 'N' if data['idvNonElectrical'] == '0' else 'Y'),
                ('nonelec_access_vaue', data['idvNonElectrical']),
                ('idv_vaue', vehicle_idv),
                ('access_idv_value', total_idv_electrical),
                ('total_idv_value', total_idv),
                ('ncb_appl_value', data['ncb_applicabe']),
                ('past_pol_exp_value', '01/01/9999' if data[
                    'isNewVehicle'] == '1' else data['past_policy_end_date']),
                ('pol_excess_val', '1000' if vehicle.cc < 1500 else '2000'),
                ('voluntary_excess', '0' if data['voluntaryDeductible'] == '0' else data['voluntaryDeductible']),
            )

        xml_data_tuple_value = tuple([d[1] for d in xml_data_tuple])

        if self.Configuration.PRODUCT_NAME == 'Two Wheeler':
            xml_request_data = xml_data.calculatePremiumMasterXml % xml_data_tuple_value
        else:
            xml_request_data = xml_data.calculatePremiumMasterXml_PvtCar % xml_data_tuple_value

        xml_request_data = etree.tostring(etree.XML(xml_request_data, parser=PARSER))
        return xml_request_data


class ProposalGenerator(object):
    def __init__(self, integrationUtils, configuration):
        self.parent = integrationUtils
        self.Configuration = configuration

    def _get_create_policyhol_gen_response(self, data, transaction):
        form_data = transaction.raw['user_form_details']
        address = data['add_house_no'] + ',' + data['add_building_name']
        address = address + data['add_street_name'] + ',' + data['add_landmark'] + ',' + data['add_city']
        xml_data_tuple = (
            ("startDate", (datetime.datetime.now()).strftime('%d-%m-%Y 00:00:00')),
            ("address", address),
            ("firstName", data['cust_first_name']),
            ("sex", "M" if data['cust_gender'] == 'Male' else "F"),
            ("phNo3", data['cust_phone']),
            ("EMailid1", data['cust_email']),
            ("birthDate", data['cust_dob']),
            ("lastName", data['cust_last_name']),
            ("pinCode", data['add_pincode']),
            ("panNo", form_data['cust_pan']),
        )

        xml_data_tuple_value = tuple([d[1] for d in xml_data_tuple])
        xml_request = xml_data.createPolicyHol_GenElement_xml % xml_data_tuple_value
        xml_request = etree.tostring(etree.XML(xml_request, parser=PARSER))
        resp = get_response(transaction.transaction_id, xml_request)
        resp = xml_to_dict(resp)

        return resp['Envelope']['Body']['createPolicyHol_GenResponseElement']

    def _get_savequote_approveproposal_response(self, data, transaction):
        quote_parameters = transaction.quote.raw_data
        vehicle = Vehicle.objects.get(id=int(data['vehicleId']))
        registrationnum = data['vehicle_reg_no'].split('-')

        total_idv_electrical = int(quote_parameters['idvElectrical']) + int(quote_parameters['idvNonElectrical'])
        total_idv = int(data['idv']) + total_idv_electrical
        ex_shrm_price = vehicle.raw_data.split('|')[6]

        form_data = transaction.raw['user_form_details']

        zone = 'B'
        if data['rto_code'] in cust_data.ZONE_A_RTO_LIST:
            zone = 'A'

        past_policy_insurer = 'NA'
        if data.get('past_policy_insurer', None):
            past_policy_insurer = insurer_utils.MASTER_INSURER_LIST[data['past_policy_insurer']]

        rta_map = cust_data.RTO_ADDRESS_MAP[data['rto_code']]
        rta_addr = rta_map['RTA_NAME'] + ',' + rta_map['RTA_ADDRESS']

        if self.Configuration.PRODUCT_NAME == 'Two Wheeler':
            xml_data_tuple = (
                ('productName', self.Configuration.PRODUCT_NAME),
                ('policyHoldercode', transaction.raw['policy_holder_code']),
                ('eventDate', datetime.date.today().strftime('%d/%m/%Y')),
                ('polInceptiondate', data['policy_start_date']),
                ('polStartdate', data['policy_start_date']),
                ('polExpirydate', data['policy_end_date']),
                ('term', '1'),  # need to write logic to have term for 2 and 3 years
                ('productCode', self.Configuration.PRODUCT_CODE),
                ('policyHolderName', data['cust_first_name'] + ' ' + data['cust_last_name']),
                ('productId', self.Configuration.PRODUCT_ID),  # need to cahnge add from configuration
                ('productCode', self.Configuration.PRODUCT_CODE),
                ('coverCode', 'PACKAGE'),  # Need to change using login of enhancement or package
                ('no_unpa_value', vehicle.seating_capacity),
                ('si_individ_value', quote_parameters['extra_paPassenger']),
                ('si_capital_value', str(int(vehicle.seating_capacity) * int(quote_parameters['extra_paPassenger']))),
                ('ll_value', quote_parameters['extra_isLegalLiability']),
                ('pa_unnamed', 'N' if quote_parameters['extra_paPassenger'] == '0' else 'Y'),
                ('ll_paid_driver', 'Y' if quote_parameters['extra_isLegalLiability'] == '1' else 'N'),
                ('manf_year_value', quote_parameters['manufacturingDate'].split('-')[-1]),
                ('new_vehicle_value', 'Y' if quote_parameters['isNewVehicle'] == '1' else 'N'),
                ('reg_value', 'NEW' if quote_parameters['isNewVehicle'] == '1' else registrationnum[0]),
                ('reg_value', '0' if quote_parameters['isNewVehicle'] == '1' else registrationnum[1]),
                ('reg_value', 'none' if quote_parameters['isNewVehicle'] == '1' else registrationnum[2]),
                ('reg_value', '0001' if quote_parameters['isNewVehicle'] == '1' else registrationnum[3]),
                ('sale_date_value', data['registration_date']),
                ('reg_date_value', data['registration_date']),
                ('elec_access_vaue', quote_parameters['idvElectrical']),
                ('prev_pol_num_value', '0' if quote_parameters['isNewVehicle'] == '1' else data['past_policy_number']),
                ('bifuel_value', quote_parameters['cngKitValue']),
                ('is_inbuilt_cng', 'Y' if (quote_parameters['isCNGFitted'] == '1' and quote_parameters['cngKitValue'] == '0') else 'N'),
                ('eng_num_value', data['vehicle_engine_no']),
                ('chasis_num_value', data['vehicle_chassis_no']),
                ('make_value', vehicle.make),
                ('model_value', vehicle.model),
                ('is_anti-theft', 'Y' if quote_parameters['extra_isAntiTheftFitted'] == '1' else 'N'),
                ('zone_value', zone),
                ('variant_value', vehicle.variant),
                ('RTA Address', rta_addr),
                ('cc_value', vehicle.cc),
                ('seating_value', vehicle.seating_capacity),
                ('fuel_type_value', 'CNGPetrol' if quote_parameters['isCNGFitted'] == '1' else vehicle.fuel_type),
                ('ex_showroom_value', ex_shrm_price),
                ('age_value', data['vehicle_age']),
                ('elec_access_bool_value', 'N' if quote_parameters['idvElectrical'] == '0' else 'Y'),
                ('nonelec_access_bool_value', 'N' if quote_parameters['idvNonElectrical'] == '0' else 'Y'),
                ('nonelec_access_vaue', quote_parameters['idvNonElectrical']),
                ('idv_vaue', data['idv']),
                ('access_idv_value', total_idv_electrical),
                ('total_idv_value', total_idv),
                ('ncb_appl_value', data['ncb_applicabe']),
                ('ncb_prev_value', data['ncb']),
                ('ncb_appl_prev_str', '% o'),
                ('prev_pol_ins_value', past_policy_insurer),
                ('prev_ins_city_value', 'NA' if quote_parameters['isNewVehicle'] == '1' else data['add_city']),
                ('past_pol_exp_value', '01/01/9999' if quote_parameters[
                    'isNewVehicle'] == '1' else data['past_policy_end_date']),
                ('pol_excess_val', '100'),  # Need to make 100 for TW and 1000 for Fourwheeler
                ('voluntary_excess', '0' if quote_parameters[
                    'voluntaryDeductible'] == '0' else quote_parameters['voluntaryDeductible']),
                ('nominee_name_value', form_data['nominee_name']),
                ('nominee_age_value', form_data['nominee_age']),
                ('nominee_relation_value', form_data['nominee_relationship']),
                ('sex_value', "M" if data['cust_gender'] == 'Male' else "F"),
                # ('od_discount_percent_parse', '%')
            )
        else:
            xml_data_tuple = (
                ('productName', self.Configuration.PRODUCT_NAME),
                ('policyHoldercode', transaction.raw['policy_holder_code']),
                ('eventDate', datetime.date.today().strftime('%d/%m/%Y')),
                ('polInceptiondate', data['policy_start_date']),
                ('polStartdate', data['policy_start_date']),
                ('polExpirydate', data['policy_end_date']),
                ('term', '1'),  # need to write logic to have term for 2 and 3 years
                ('productCode', self.Configuration.PRODUCT_CODE),
                ('policyHolderName', data['cust_first_name'] + ' ' + data['cust_last_name']),
                ('productId', self.Configuration.PRODUCT_ID),  # need to cahnge add from configuration
                ('productCode', self.Configuration.PRODUCT_CODE),
                ('coverCode', 'PACKAGE'),  # Need to change using login of enhancement or package
                ('no_unpa_value', vehicle.seating_capacity),
                ('si_individ_value', quote_parameters['extra_paPassenger']),
                ('si_capital_value', str(int(vehicle.seating_capacity) * int(quote_parameters['extra_paPassenger']))),
                ('ll_value', quote_parameters['extra_isLegalLiability']),
                ('pa_unnamed', 'N' if quote_parameters['extra_paPassenger'] == '0' else 'Y'),
                ('ll_paid_driver', 'Y' if quote_parameters['extra_isLegalLiability'] == '1' else 'N'),
                ('manf_year_value', quote_parameters['manufacturingDate'].split('-')[-1]),
                ('new_vehicle_value', 'Y' if quote_parameters['isNewVehicle'] == '1' else 'N'),
                ('reg_value', 'NEW' if quote_parameters['isNewVehicle'] == '1' else registrationnum[0]),
                ('reg_value', '0' if quote_parameters['isNewVehicle'] == '1' else registrationnum[1]),
                ('reg_value', 'none' if quote_parameters['isNewVehicle'] == '1' else registrationnum[2]),
                ('reg_value', '0001' if quote_parameters['isNewVehicle'] == '1' else registrationnum[3]),
                ('sale_date_value', data['registration_date']),
                ('reg_date_value', data['registration_date']),
                ('elec_access_vaue', quote_parameters['idvElectrical']),
                ('prev_pol_num_value', '0' if quote_parameters['isNewVehicle'] == '1' else data['past_policy_number']),
                ('bifuel_value', quote_parameters['cngKitValue']),
                ('is_inbuilt_cng', 'Y' if (quote_parameters['isCNGFitted'] == '1' and quote_parameters['cngKitValue'] == '0') else 'N'),
                ('eng_num_value', data['vehicle_engine_no']),
                ('chasis_num_value', data['vehicle_chassis_no']),
                ('make_value', vehicle.make),
                ('model_value', vehicle.model),
                ('is_anti-theft', 'Y' if quote_parameters['extra_isAntiTheftFitted'] == '1' else 'N'),
                ('zone_value', zone),
                ('variant_value', vehicle.variant),
                ('RTA Address', rta_addr),
                ('cc_value', vehicle.cc),
                ('seating_value', vehicle.seating_capacity),
                ('fuel_type_value', 'CNGPetrol' if quote_parameters['isCNGFitted'] == '1' else vehicle.fuel_type),
                ('ex_showroom_value', ex_shrm_price),
                ('age_value', data['vehicle_age']),  # Need to find age in months
                ('elec_access_bool_value', 'N' if quote_parameters['idvElectrical'] == '0' else 'Y'),
                ('nonelec_access_bool_value', 'N' if quote_parameters['idvNonElectrical'] == '0' else 'Y'),
                ('nonelec_access_vaue', quote_parameters['idvNonElectrical']),
                ('nominee_name_value', form_data['nominee_name']),
                ('nominee_age_value', form_data['nominee_age']),
                ('nominee_relation_value', form_data['nominee_relationship']),
                ('idv_vaue', data['idv']),
                ('access_idv_value', total_idv_electrical),
                ('total_idv_value', total_idv),
                ('ncb_appl_value', data['ncb_applicabe']),
                ('prev_pol_ins_value', past_policy_insurer),
                ('prev_ins_city_value', 'NA' if quote_parameters['isNewVehicle'] == '1' else data['add_city']),
                ('past_pol_exp_value', '01/01/9999' if quote_parameters[
                    'isNewVehicle'] == '1' else data['past_policy_end_date']),
                ('pol_excess_val', '1000' if vehicle.cc < 1500 else '2000'),
                ('voluntary_excess', '0' if quote_parameters[
                    'voluntaryDeductible'] == '0' else quote_parameters['voluntaryDeductible']),
            )

        xml_data_tuple_value = tuple([d[1] for d in xml_data_tuple])

        if self.Configuration.PRODUCT_NAME == 'Two Wheeler':
            xml_request = xml_data.saveQuote_ApproveProposal_Xml % xml_data_tuple_value
        else:
            xml_request = xml_data.saveQuote_ApproveProposal_PvtCar_Xml % xml_data_tuple_value
        xml_request = etree.tostring(etree.XML(xml_request, parser=PARSER))
        transaction.proposal_request = xml_request
        transaction.save()
        resp = get_response(transaction.transaction_id, xml_request)
        transaction.proposal_response = resp
        transaction.save()
        resp = xml_to_dict(resp)

        return resp

    def _get_collectPremium_issuePol_response(self, transaction):
        proposal_resp = transaction.raw['proposal_resp']

        xml_data_tuple = (
                        ('premium', proposal_resp['netPremium']),
                        ('quoteno', proposal_resp['quoteNo']),
                        ('collectionamount', proposal_resp['netPremium']),
                        ('chequeNo', transaction.payment_token),
                        ('quoteNo', proposal_resp['quoteNo']),
                    )

        xml_data_tuple_value = tuple([d[1] for d in xml_data_tuple])
        xml_request = xml_data.collectPremium_issuePol_XML % xml_data_tuple_value
        xml_request = etree.tostring(etree.XML(xml_request, parser=PARSER))
        transaction.policy_request = xml_request
        transaction.save()
        resp = get_response(transaction.transaction_id, xml_request)
        transaction.policy_response = resp
        transaction.save()
        resp = xml_to_dict(resp)

        return resp

    def _fetch_policy_document_resp(self, transaction):
        xml_request = xml_data.fetch_policy_document_xml % transaction.raw['proposal_resp']['policyId']
        xml_request = etree.tostring(etree.XML(xml_request, parser=PARSER))
        resp = get_response(transaction.transaction_id, xml_request)
        resp = xml_to_dict(resp)

        return resp

    def save_policy_details(self, data_dictionary, transaction):
        policyhol_gen_resp = self._get_create_policyhol_gen_response(data_dictionary, transaction)

        if policyhol_gen_resp['PRetCode'] == '0' and policyhol_gen_resp.get('partyCode', None):
            transaction.raw['policy_holder_code'] = policyhol_gen_resp['partyCode']
            transaction.save()

            resp = self._get_savequote_approveproposal_response(data_dictionary, transaction)
            proposal_resp = resp['Envelope']['Body']['SaveQuote_ApproveProposalResponseElement']
            transaction.raw['proposal_resp'] = proposal_resp
            transaction.proposal_number = proposal_resp['quoteNo']
            transaction.proposal_amount = proposal_resp['netPremium']
            transaction.save()

            return True
        else:
            return False


class IntegrationUtils(object):
    def __init__(self, vehicle_type):
        self.Configuration = Configuration(vehicle_type)
        self.PremiumCalculator = PremiumCalculator(self, self.Configuration)
        self.ProposalGenerator = ProposalGenerator(self, self.Configuration)

    def convert_premium_request_data_to_required_dictionary(self, vehicle_id, request_data):
        data_dictionary = copy.deepcopy(request_data)
        data_dictionary['masterVehicleId'] = data_dictionary['vehicleId']
        data_dictionary['vehicleId'] = vehicle_id
        data_dictionary['req_idv'] = request_data['idv']
        data_dictionary['idv'] = '0'

        is_new_vehicle = (data_dictionary['isNewVehicle'] == '1')
        registration_date = datetime.datetime.strptime(data_dictionary['registrationDate'], '%d-%m-%Y')

        rto_code = '%s%02d' % (data_dictionary['registrationNumber[]'][
                               0], int(data_dictionary['registrationNumber[]'][1]))

        if is_new_vehicle:
            registration_date = datetime.datetime.now()
            data_dictionary['past_policy_end_date'] = ''
            inception_date = datetime.datetime.strptime(
                data_dictionary['newPolicyStartDate'], '%d-%m-%Y')
            data_dictionary['manufacturing_date'] = (datetime.datetime.now()).strftime('%d-%m-%Y')
            data_dictionary['ncb'] = '0'
            data_dictionary['ncb_applicabe'] = '0'
        else:
            registration_date = datetime.datetime.strptime(data_dictionary['registrationDate'], '%d-%m-%Y')
            pyp_end_date = datetime.datetime.strptime(
                data_dictionary['pastPolicyExpiryDate'], "%d-%m-%Y")

            inception_date = pyp_end_date + datetime.timedelta(days=1)
            data_dictionary['past_policy_end_date'] = pyp_end_date.strftime(
                '%d/%m/%Y')
            data_dictionary['manufacturing_date'] = data_dictionary['manufacturingDate']
            data_dictionary['ncb'] = data_dictionary['previousNCB']
            data_dictionary['ncb_applicabe'] = previous_ncb_to_new_ncb[data_dictionary['previousNCB']]

        new_policy_end_date = inception_date.replace(
            year=inception_date.year + 1) - datetime.timedelta(days=1)

        data_dictionary['policy_start_date'] = inception_date.strftime('%d/%m/%Y')
        data_dictionary['policy_end_date'] = new_policy_end_date.strftime('%d/%m/%Y')
        data_dictionary['registration_date'] = registration_date.strftime('%d/%m/%Y')

        vehicle_age = 0
        if not is_new_vehicle:
            vehicle_age = insurer_utils.get_age(inception_date, registration_date)

        data_dictionary['vehicle_age'] = vehicle_age
        data_dictionary['rto_code'] = rto_code

        return data_dictionary

    def convert_premium_response(self, data, data_dictionary, vehicle, premium_breakup_data):
        basic_covers = [
            # {'id': 'basicOd', 'default': '1', 'premium': float(premium_breakup_data['Basic OD Premium'])},
            {'id': 'basicOd', 'default': '1', 'premium': float(premium_breakup_data['Normal OD Premium'])},
            {'id': 'basicTp', 'default': '1', 'premium': float(premium_breakup_data['Basic TP Premium'])},
            {'id': 'idvElectrical', 'default': '1',
             'premium': float(premium_breakup_data['Additional Premium for Electrical fitting'])},
            {'id': 'idvNonElectrical', 'default': '1',
             'premium': float(premium_breakup_data['Additional Premium for Non-Electrical fitting'])},
            {'id': 'legalLiabilityDriver', 'default': '0',
             'premium': float(premium_breakup_data['Legal Liability Premium for Paid Driver'])},
            {'id': 'cngKit', 'default': '1',
             'premium': float(premium_breakup_data['Additional TP Premium for CNG/LPG']) +
             float(premium_breakup_data['Additional OD Premium for CNG/LPG']) +
             float(premium_breakup_data['Additional Loading for CNG/LPG Kit'])},
            {'id': 'paOwnerDriver', 'default': '1',
             'premium': float(premium_breakup_data['Compulsory PA Premium for Owner Driver'])},
            {'id': 'paPassenger', 'default': '1',
             'premium': float(premium_breakup_data['PA premium for UnNamed/Hirer/Pillion Persons'])},
        ]

        discounts = [
            {'id': 'ncb', 'default': '1', 'premium': -1 * float(premium_breakup_data['Calculated NCB Discount'])},
            {'id': 'odDiscount', 'default': '1', 'premium': -1 * float(premium_breakup_data['OD Premium Discount Amount'])},
        ]

        addon_covers = []

        special_discounts = [
            {'premium': -float(premium_breakup_data.get('Calculated Voluntary Deductible Discount', 0)),
             'id': 'voluntaryDeductible',  'default': '1', },
            {'premium': -float(premium_breakup_data.get('Calculated Discount for Anti-Theft Devices', 0)),
             'id': 'isAntiTheftFitted',    'default': '1', }
        ]

        result_data = {
            'insurerId': INSURER.id,
            'insurerSlug': INSURER_SLUG,
            'insurerName': INSURER.title,
            'isInstantPolicy': True,
            'minIDV': data_dictionary['min_idv'],
            'maxIDV': data_dictionary['max_idv'],
            'calculatedAtIDV': data_dictionary['idv'],
            'serviceTaxRate': float(float(premium_breakup_data['Service Tax Percentage']) / 100),
            'premiumBreakup': {
                'serviceTax': float(premium_breakup_data['Service Tax']),
                'basicCovers': insurer_utils.cpr_update(basic_covers),
                'addonCovers': insurer_utils.cpr_update(addon_covers),
                'discounts': insurer_utils.cpr_update(discounts),
                'specialDiscounts': insurer_utils.cpr_update(special_discounts),
                'paPassengerAmounts': insurer_utils.get_pa_passenger_dict(
                    vehicle.vehicle_type, vehicle.seating_capacity),
            },
        }, data
        return {'result': result_data, 'success': True}

    def get_buy_form_details(self, quote_parameters, transaction):
        split_registration_no = quote_parameters.get('registrationNumber[]')

        is_new_vehicle = True if quote_parameters['isNewVehicle'] == '1' else False
        if is_new_vehicle:
            registration_number = 'NEW---0001'
        else:
            registration_number = '-'.join(split_registration_no)

        master_rto_code = "%s-%s" % (split_registration_no[0], split_registration_no[1])

        data = {
            'registration_no': registration_number,
            'rto_location_info': master_rto_code,
            # 'insurers_list' : PAST_INSURANCE_ID_LIST,
            'insurers_list': insurer_utils.MASTER_INSURER_LIST,
            'relationship_list': cust_data.NOMINEE_RELATIONSHIP_LIST,
            'disallowed_insurers': cust_data.DISALLOWED_PAST_INSURERS,
            'is_cities_fetch': False,
        }
        return data

    def convert_proposal_request_data_to_required_dictionary(self, vehicle,
                                                             request_data, quote_parameters,
                                                             quote_response, quote_custom_response):
        form_data = copy.deepcopy(request_data)

        is_new_vehicle = False
        if quote_parameters['isNewVehicle'] == '1':
            is_new_vehicle = True

        if is_new_vehicle:
            request_data['vehicle_reg_no'] = 'NEW---0001'

        form_data['vehicleId'] = vehicle.id
        form_data['idv'] = quote_custom_response['calculatedAtIDV']
        rto_code = '%s%02d' % (quote_parameters['registrationNumber[]'][
                               0], int(quote_parameters['registrationNumber[]'][1]))

        registration_date = datetime.datetime.strptime(quote_parameters['registrationDate'], '%d-%m-%Y')

        if not is_new_vehicle:
            pyp_end_date = datetime.datetime.strptime(
                quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")

            inception_date = pyp_end_date + datetime.timedelta(days=1)
            form_data['past_policy_end_date'] = pyp_end_date.strftime(
                '%d/%m/%Y')
            form_data['manufacturing_date'] = quote_parameters['manufacturingDate']
            form_data['ncb'] = quote_parameters['previousNCB']
            form_data['ncb_applicabe'] = previous_ncb_to_new_ncb[quote_parameters['previousNCB']]
        else:
            registration_date = datetime.datetime.now()
            form_data['past_policy_end_date'] = ''
            inception_date = datetime.datetime.strptime(
                quote_parameters['newPolicyStartDate'], '%d-%m-%Y')

            form_data['manufacturing_date'] = (datetime.datetime.now()).strftime('%d-%m-%Y')
            form_data['ncb'] = '0'
            form_data['ncb_applicabe'] = '0'

        new_policy_end_date = inception_date.replace(
            year=inception_date.year + 1) - datetime.timedelta(days=1)

        form_data['policy_start_date'] = inception_date.strftime('%d/%m/%Y')
        form_data['policy_end_date'] = new_policy_end_date.strftime('%d/%m/%Y')
        form_data['registration_date'] = registration_date.strftime('%d/%m/%Y')

        vehicle_age = 0
        if not is_new_vehicle:
            vehicle_age = insurer_utils.get_age(inception_date, registration_date)

        form_data['vehicle_age'] = vehicle_age
        form_data['rto_code'] = rto_code
        form_data['add_city'] = cust_data.PIN_CODE_CITY_STATE_MAP[form_data['add_pincode']]['City']

        return form_data

    def update_transaction_details(self, transaction, data):
        is_new_vehicle = data['isNewVehicle'] == '1'
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                data['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

        new_policy_end_date = putils.add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)

        transaction.policy_start_date = new_policy_start_date
        transaction.policy_end_date = new_policy_end_date
        transaction.save()

    def get_payment_data(self, transaction):
        pg_in_data = {
            'transaction': transaction,
            'reference_id': transaction.proposal_number,
            'amount': transaction.proposal_amount,
            'email': transaction.proposer.email,
            'phone': transaction.proposer.mobile
        }
        pg_config = Billdesk.get_config(INSURER_SLUG)
        billdesk = Billdesk(pg_in_data, pg_config)
        payment_data, payment_url = billdesk.get_request_data()
        suds_logger.set_name(transaction.transaction_id).log(
                json.dumps({'url': payment_url, 'data': payment_data}))
        transaction.payment_request = {'url': payment_url, 'data': payment_data}
        transaction.save()
        return payment_data, payment_url

    def post_payment(self, payment_response, transaction):
        motor_logger.info(payment_response)
        suds_logger.set_name(transaction.transaction_id).log(json.dumps(payment_response))
        if transaction.vehicle_type == 'Twowheeler':
            redirect_url = '/motor/twowheeler/payment/failure/%s/' % transaction.transaction_id
        else:
            redirect_url = '/motor/fourwheeler/payment/failure/%s/' % transaction.transaction_id
        is_redirect = True

        transaction.payment_response = payment_response
        transaction.save()

        if payment_response['status'] == '0300':
            transaction.payment_done = True
            transaction.premium_paid = str(float(payment_response['payment_amount']))
            transaction.payment_token = payment_response['payment_id']
            transaction.payment_on = datetime.datetime.now()
            transaction.mark_complete()
            if transaction.vehicle_type == 'Twowheeler':
                redirect_url = '/motor/twowheeler/payment/success/%s/' % transaction.transaction_id
            else:
                redirect_url = '/motor/fourwheeler/payment/success/%s/' % transaction.transaction_id
            issue_policy_resp = self.ProposalGenerator._get_collectPremium_issuePol_response(transaction)
            issue_policy_resp_dict = issue_policy_resp['Envelope']['Body']['collectpremium_IssuepolResponseElement']
            transaction.policy_number = issue_policy_resp_dict['policyNo']
            transaction.insured_details_json['policy_url'] = issue_policy_resp_dict['documentLink']
            transaction.save(update_fields=['policy_number', 'insured_details_json'])

            # raw = transaction.raw
            # fetch_policy_doc_resp_dict = self.ProposalGenerator._fetch_policy_document_resp(transaction)
            # fetch_policy_doc_resp_dict = remove_namespace_listdict(fetch_policy_doc_resp_dict)
            # fetch_policy_doc_resp = fetch_policy_doc_resp_dict['Envelope']['Body']['fetchDocumentNameResponseElement']
            # raw['fetch_policy_doc_resp'] = fetch_policy_doc_resp
            # Transaction.objects.filter(id=transaction.id).update(raw=raw)
        else:
            transaction.status = 'FAILED'
            transaction.save()

        return is_redirect, redirect_url, '', {}

    def get_dependent_addons(self, parameters):
        return {'type': 'dynamic'}

    def get_transaction_from_id(self, _id):
        return Transaction.objects.get(transaction_id=_id)

    def get_custom_data(self, datatype):
        return None

    def disable_cache_for_request(self, request_data):
        if request_data.get('discountCode', None):
            return True
        return False
