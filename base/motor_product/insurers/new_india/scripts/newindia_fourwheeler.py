INSURER_SLUG = 'new-india'

csv_parameters = {
    'MOTOR_MAKE': 0,
    'MOTOR_MODEL': 1,
    'MOTOR_CC': 2,
    'MOTOR_CARRYING_CAPACITY': 3,
    'MOTOR_VARIANT': 4,
    'MOTOR_FUEL': 5,
    'CF ID': 18,
}

parameters = {
    'make': 0,
    'model': 1,
    'cc': 2,
    'seating_capacity': 3,
    'variant': 4,
    'fuel_type': 5,
    'master': 18,
}
