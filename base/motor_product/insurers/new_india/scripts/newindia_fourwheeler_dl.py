from motor_product import models as motor_models

INSURER_SLUG = 'new-india'
DISCOUNTING_PATH = 'motor_product/insurers/new_india/scripts/data/newindia_fourwheeler_'


def delete_data():
    motor_models.DiscountLoading.objects.filter(
        insurer__slug=INSURER_SLUG,
        vehicle_type='Private Car'
    ).delete()


def save_data(ncb):
    fd = open(DISCOUNTING_PATH + str(ncb) + '.csv')

    row = 0
    discounting_map = {}
    makes = {}
    models = {}

    col_start = 0

    for line in fd.readlines():
        if row == 0:
            col = col_start
            former_element = ''
            for element in line.split(',')[col_start:]:
                if element != '':
                    if former_element == '':
                        col_start = col
                    element = element.strip()
                    makes[col] = element
                    former_element = element
                    discounting_map[element] = {}
                elif former_element != '':
                        makes[col] = former_element
                col += 1

        if row == 1:
            col = col_start - 1
            for element in line.split(',')[col_start - 1:]:
                if col != col_start - 1:
                    element = element.strip()
                    models[col] = element
                    discounting_map[makes[col]][element] = {}
                col += 1

        if row >= 2:
            col = col_start - 1
            state = ''
            for element in line.split(',')[col_start - 1:]:
                element = element.strip()
                if col == col_start - 1:
                    state = element
                if col != col_start - 1:
                    discounting_map[makes[col]][models[col]][state] = int(element)
                col += 1

        row += 1

    fd.close()

    dls = []
    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)
    for make, make_d in discounting_map.items():
        for model, model_d in make_d.items():
            for state, discount in model_d.items():
                if (discount != 0):
                    print 'Saving ' + make + ' ' + str(model) + ' ' + state + ' ' + str(discount)
                    if model == 'ALL' or model == 'OTHERS':
                        model = None
                    dl = motor_models.DiscountLoading()
                    dl.make = make
                    dl.model = model
                    dl.state = state
                    dl.ncb = None
                    if ncb == 'ncb':
                        dl.ncb = 65
                    dl.insurer = insurer
                    dl.discount = discount
                    dls.append(dl)

                    if len(dls) == 1000:
                        motor_models.DiscountLoading.objects.bulk_create(dls)
                        dls = []

    if len(dls) > 0:
        motor_models.DiscountLoading.objects.bulk_create(dls)
