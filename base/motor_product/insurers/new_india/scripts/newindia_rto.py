from motor_product import models as motor_models
from motor_product.scripts.rto import master_rto_data as master_dicts
from motor_product.insurers.new_india.scripts import newindia_rto_data as newindia_dicts

INSURER_SLUG = 'new-india'


def delete_data():
    motor_models.RTOInsurer.objects.filter(insurer__slug=INSURER_SLUG).delete()


def save_data():
    CITY_INFO_DICT = newindia_dicts.RTO_CODE_TO_INFO

    insurer_universal = motor_models.Insurer.objects.get(slug=INSURER_SLUG)

    rtos = []
    for rto_code, rto_info in CITY_INFO_DICT.items():
        rto_insurer = motor_models.RTOInsurer()
        rto_insurer.insurer = insurer_universal
        rto_insurer.rto_code = rto_code
        rto_insurer.rto_name = rto_info['RTOCity']
        rto_insurer.rto_state = rto_info['State']
        rto_insurer.rto_zone = rto_info['Zone']
        mrto_state = get_insurer_master_state(rto_info['State'])
        rto_insurer.master_rto_state = mrto_state
        rto_insurer.raw = rto_info
        print "Saving ---- %s - %s" % (rto_info, mrto_state)
        rtos.append(rto_insurer)
    motor_models.RTOInsurer.objects.bulk_create(rtos)


def get_insurer_master_state(state):
    state_name = newindia_dicts.STATE_CODE_LIST[state]
    state_name = state_name.title().replace(' And ', ' and ')
    state_code = master_dicts.STATE_NAME_TO_STATE_CODE.get(state_name, None)
    if state_code is None:
        print 'No state found for the state code ' + state_name
    return state_code


def create_map():
    fd = open('motor_product/insurers/new_india/scripts/data/newindia_rto_map.csv', 'r')
    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)
    insurer_rtos = motor_models.RTOInsurer.objects.filter(insurer__slug=INSURER_SLUG)

    mappings = []
    for line in fd.readlines():
        line = line.split('\n')[0]
        rto_master_code, rto_insurer_code = line.split('|')
        master_rto = motor_models.RTOMaster.objects.get(rto_code=rto_master_code)
        insurer_rto = insurer_rtos.filter(rto_code=rto_insurer_code)[0]
        print 'Mapping %s to %s' % (master_rto, insurer_rto)
        if master_rto and insurer_rto:
            mapping = motor_models.RTOMapping()
            mapping.rto_master = master_rto
            mapping.rto_insurer = insurer_rto
            mapping.insurer = insurer
            mapping.is_approved_mapping = True
            mappings.append(mapping)
    fd.close()
    motor_models.RTOMapping.objects.bulk_create(mappings)


def delete_map():
    motor_models.RTOMapping.objects.filter(insurer__slug=INSURER_SLUG).delete()


def create_mappings():
    pass


def delete_mappings():
    pass
