# CALCULATE PREMIUM XML #
calculatePremiumMasterXml_PvtCar = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://iims.services/types/">
   <soapenv:Header/>
   <soapenv:Body>
      <typ:calculatePremiumMasterElement>
         <typ:userCode>BRCOVERFOX</typ:userCode>
         <typ:rolecode>SUPERUSER</typ:rolecode>
         <typ:PRetCode>0</typ:PRetCode>
         <typ:userId/>
         <typ:stakeCode>BROKER</typ:stakeCode>
         <typ:roleId/>
         <typ:userroleId/>
         <typ:branchcode/>
         <typ:PRetErr/>
         <typ:polBranchCode/>
         <typ:productName>%s</typ:productName>
         <typ:policyHoldercode>PO00129265</typ:policyHoldercode>
         <typ:eventDate>%s</typ:eventDate>
         <typ:netPremium/>
         <typ:termUnit>G</typ:termUnit>
         <typ:grossPremium/>
         <typ:policyType/>
         <typ:polInceptiondate>%s</typ:polInceptiondate>
         <typ:polStartdate>%s</typ:polStartdate>
         <typ:polExpirydate>%s</typ:polExpirydate>
         <typ:branchCode/>
         <typ:term>%s</typ:term>
         <typ:polEventEffectiveEndDate/>
         <typ:productCode>%s</typ:productCode>
         <typ:policyHolderName>POLICY BAZAAR USER</typ:policyHolderName>
         <typ:productId>%s</typ:productId>
         <typ:serviceTax/>
         <typ:status/>
         <typ:cbDetails/>
         <typ:sumInsured/>
         <typ:updateDate/>
         <typ:policyId/>
         <typ:polDetailLastUpdateDate/>
         <typ:quoteNo/>
         <typ:policyNo/>
         <typ:policyDetailid/>
         <typ:documentLink/>
         <typ:polLastUpdateDate/>
         <typ:properties>
            <typ:value>COVERFOX</typ:value>
            <typ:name>channelcode</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Fire explosion self ignition or lightning peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Burglary housebreaking or theft peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Riot and strike peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Earthquake damage peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Flood typhoon hurricane storm tempest inundation cyclone hailstorm frost peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Accidental external means peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Malicious act peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Terrorist activity peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Whilst in transit by road rail inland-waterway lift elevator or air peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Landslide rockslide peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>N</typ:value>
            <typ:name>Is it declaration type policy</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>N</typ:value>
            <typ:name>Is Service Tax Exempted</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>NO</typ:value>
            <typ:name>Co-Insurance Applicable</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>N</typ:value>
            <typ:name>Is Business Sourced from Tie Up</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Non-Dealer</typ:value>
            <typ:name>Auto Tie Up Type</typ:name>
         </typ:properties>
         <typ:risks>
            <typ:riskCode>VEHICLE</typ:riskCode>
            <typ:riskSuminsured>0</typ:riskSuminsured>
            <typ:covers>
               <typ:productCode>%s</typ:productCode>
               <typ:policyDetailid/>
               <typ:coverCode>%s</typ:coverCode>
               <typ:productId/>
               <typ:coverExpiryDate/>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>Do You want to reduce TPPD cover to the statutory limit of Rs.6000</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>200000</typ:value>
                  <typ:name>Sum Insured for PA to Owner Driver</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>Do you want to include PA cover for Named Person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Number of Named Persons</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>none</typ:value>
                  <typ:name>Names of Named person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Individual SI for Named Person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Capital SI for All Named Persons</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>No of Paid Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Individual SI for Paid Driver</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Capital SI for Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>No of unnamed Persons</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>Individual SI for unnamed Person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>Capital SI for unnamed Persons</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Number of LL to Soldiers/Sailors/Airmen</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Number Of Legal Liable Employees</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>Number of Legal Liable Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>Do you wish to include PA Cover for Paid Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>Do you want to include PA cover for unnamed person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>LL to paid drivers,cleaner employed  for opn. and/or maint. of vehicle under WCA</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>LL to Employees of Insured traveling and / or driving the Vehicle</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>LL to Soldiers/Sailors/Airmen employed as Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Sum Insured for TPPD</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>A</typ:value>
                  <typ:name>Type of Liability Coverage</typ:name>
               </typ:properties>
            </typ:covers>
            <typ:properties>
               <typ:value>Private</typ:value>
               <typ:name>Type of Private Car</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>New</typ:value>
               <typ:name>Current Ownership</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>BLACK</typ:value>
               <typ:name>Color of Vehicle</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Year of Manufacture</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>New Vehicle</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Registration No (1)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Registration No (2)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Registration No (3)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Registration No (4)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Date of Sale</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Date of Registration</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/2029</typ:value>
               <typ:name>Registration Validity Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Give Vehicle Details</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether trailer attached to the vehicle</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Number of Trailers Attached</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Total IDV of the Trailer Attached</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Value of Music System</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Value of AC/Fan</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Value of Lights</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Value of Other Fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Total Value of Extra Electrical/ Electronic fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Additional Towing Coverage Amount</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>PPN999797446</typ:value>
               <typ:name>Previous Policy Number</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Name of Association</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Membership No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Is Life Member</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Date of Membership Expiry</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Details of Vehicle Condition</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Discretion to RO</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Approval No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Approval Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Bangladesh</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Bhutan</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Nepal</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Pakistan</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Sri Lanka</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Maldives</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Value of Fibre glass fuel tanks</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Bi-fuel System Value</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>In Built Bi-fuel System fitted</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>SALOON</typ:value>
               <typ:name>Type of Body</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>879999</typ:value>
               <typ:name>(*)Engine No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>987445</typ:value>
               <typ:name>(*)Chassis No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Make</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Model</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Car in roadworthy condition and free from damage</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Vehicle Requisitioned by Government</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether vehicle is used for driving tuition</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Vehicle use is limited to own premises</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether vehicle belongs to foreign embassy or consulate</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether vehicle is certified as Vintage car by Vintage and Classic Car Club</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Vehicle designed for Blind/Handicapped/Mentally Challenged persons and endorsed by RTA</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Are you a member of Automobile Association of India</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Is the vehicle fitted with Anti-theft device</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Obsolete Vehicle</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area required</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether Vehicle belong to Embassies or imported without Custom Duty</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Vehicle Zone for Private Car</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Variant</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Name and Address of Registration Authority</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>(*)Cubic Capacity</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>(*)Seating Capacity</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Type of Fuel</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Fibre Glass Tank Fitted</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Vehicle Invoice Value</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Vehicle Age in Months</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Extra Electrical/ Electronic fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Non-Electrical/ Electronic fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Value of Non- Electrical/ Electronic fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Additional Towing Coverage Required</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Do You Hold Valid Driving License</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>License Type of Owner Driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Age of Owner Driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Owner Driver Driving License No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Owner Driver License Expiry Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>License Issuing Authority for Owner Driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Kumar</typ:value>
               <typ:name>Name of Nominee</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>25</typ:value>
               <typ:name>Age of Nominee</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>SON</typ:value>
               <typ:name>Relationship with the Insured</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>MALE</typ:value>
               <typ:name>Gender of the Nominee</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Do you Have Any other Driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Insureds declared Value (IDV)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>IDV of Accessories</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Total IDV</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>NCB Applicable Percentage</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Cholamandalam</typ:value>
               <typ:name>Name of Previous Insurer</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Delhi-North</typ:value>
               <typ:name>Address of the Previous Insurer</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value/>
               <typ:name>Date of Purchase of Vehicle by Proposer</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Vehicle New or Second hand at the time of purchase</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Vehicle Used for Private,Social,domestic,pleasure,professional purpose</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Is vehicle in good condition</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Expiry date of previous Policy</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Policy Excess (Rs)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Voluntary Excess for PC</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Imposed Excess (Rs)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Loading amount for OD</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value/>
               <typ:name>Owner Driver License Issue Date</typ:name>
            </typ:properties>
         </typ:risks>
      </typ:calculatePremiumMasterElement>
   </soapenv:Body>
</soapenv:Envelope>
"""

calculatePremiumMasterXml = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body xmlns:typ="http://iims.services/types/">
      <typ:calculatePremiumMasterElement>
         <typ:userCode>BRCOVERFOX</typ:userCode>
         <typ:rolecode>SUPERUSER</typ:rolecode>
         <typ:PRetCode>0</typ:PRetCode>
         <typ:userId/>
         <typ:roleId/>
         <typ:userroleId/>
         <typ:branchcode/>
         <typ:PRetErr/>
         <typ:polBranchCode/>
         <typ:productName>%s</typ:productName>
         <typ:policyHoldercode>PO00119103</typ:policyHoldercode>
         <typ:eventDate>%s</typ:eventDate>
         <typ:netPremium/>
         <typ:termUnit>G</typ:termUnit>
         <typ:grossPremium/>
         <typ:policyType/>
         <typ:polInceptiondate>%s</typ:polInceptiondate>
         <typ:polStartdate>%s</typ:polStartdate>
         <typ:polExpirydate>%s</typ:polExpirydate>
         <typ:branchCode/>
         <typ:term>1</typ:term>
         <typ:polEventEffectiveEndDate/>
         <typ:productCode>%s</typ:productCode>
         <typ:policyHolderName>COVERFOX</typ:policyHolderName>
         <typ:productId>%s</typ:productId>
         <typ:serviceTax/>
         <typ:status/>
         <typ:cbDetails/>
         <typ:sumInsured/>
         <typ:updateDate/>
         <typ:policyId/>
         <typ:polDetailLastUpdateDate/>
         <typ:quoteNo/>
         <typ:policyNo/>
         <typ:policyDetailid/>
         <typ:stakeCode>BROKER</typ:stakeCode>
         <typ:documentLink/>
         <typ:polLastUpdateDate/>
         <typ:properties>
            <typ:value>COVERFOX</typ:value>
            <typ:name>channelcode</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Fire explosion self ignition or lightning peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Burglary housebreaking or theft peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Riot and strike peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Earthquake damage peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Flood typhoon hurricane storm tempest inundation cyclone hailstorm frost peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Accidental external means peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Malicious act peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Terrorist activity peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Whilst in transit by road rail inland-waterway lift elevator or air peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Landslide rockslide peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>N</typ:value>
            <typ:name>Is it declaration type policy</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>N</typ:value>
            <typ:name>Is Service Tax Exempted</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>NO</typ:value>
            <typ:name>Co-Insurance Applicable</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>N</typ:value>
            <typ:name>Is Business Sourced from Tie Up</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Non-Dealer</typ:value>
            <typ:name>Auto Tie Up Type</typ:name>
         </typ:properties>
         <typ:party>
            <typ:userCode/>
            <typ:rolecode/>
            <typ:PRetCode>0</typ:PRetCode>
            <typ:userId/>
            <typ:stakeCode/>
            <typ:roleId/>
            <typ:userroleId/>
            <typ:branchcode/>
            <typ:PRetErr/>
            <typ:startDate/>
            <typ:stakeName/>
            <typ:title/>
            <typ:typeOfOrg/>
            <typ:address/>
            <typ:firstName/>
            <typ:partyCode/>
            <typ:company/>
            <typ:sex/>
            <typ:address2/>
            <typ:EMailid2/>
            <typ:partyStakeCode>FINANCIER</typ:partyStakeCode>
            <typ:partyType/>
            <typ:regNo/>
            <typ:midName/>
            <typ:city/>
            <typ:phNo3/>
            <typ:regDate/>
            <typ:contactType/>
            <typ:businessName/>
            <typ:status/>
            <typ:EMailid1/>
            <typ:clientType/>
            <typ:birthDate/>
            <typ:lastName/>
            <typ:sector/>
            <typ:country/>
            <typ:pinCode/>
            <typ:prospectId/>
            <typ:state/>
            <typ:address3/>
            <typ:phNo1/>
            <typ:businessAddress/>
            <typ:phNo2/>
            <typ:partyName/>
         </typ:party>
         <typ:risks>
            <typ:riskCode>VEHICLE</typ:riskCode>
            <typ:riskSuminsured>0</typ:riskSuminsured>
            <typ:covers>
               <typ:productCode>%s</typ:productCode>
               <typ:policyDetailId/>
               <typ:coverCode>%s</typ:coverCode>
               <typ:productId/>
               <typ:coverExpiryDate/>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>Do You want to reduce TPPD cover to the statutory limit of Rs.6000</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Sum Insured for PA to Owner Driver</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>Do you want to include PA cover for Named Person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Number of Named Persons</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>none</typ:value>
                  <typ:name>Names of Named person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Individual SI for Named Person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Capital SI for All Named Persons</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>No of Paid Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Individual SI for Paid Driver</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Capital SI for Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>No of unnamed Persons</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>Individual SI for unnamed Person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>Capital SI for unnamed Persons</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Number of LL to Soldiers/Sailors/Airmen</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Number Of Legal Liable Employees</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>Number of Legal Liable Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>Do you wish to include PA Cover for Paid Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>Do you want to include PA cover for unnamed person/hirer/pillion passangers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>LL to paid drivers,cleaner employed  for opn. and/or maint. of vehicle under WCA</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>LL to Employees of Insured traveling and / or driving the Vehicle</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>LL to Soldiers/Sailors/Airmen employed as Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Sum Insured for TPPD</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>A</typ:value>
                  <typ:name>Type of Liability Coverage</typ:name>
               </typ:properties>
            </typ:covers>
            <typ:properties>
               <typ:value>New</typ:value>
               <typ:name>Current Ownership</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>BLACK</typ:value>
               <typ:name>Color of Vehicle</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Year of Manufacture</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>New Vehicle</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Registration No (1)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Registration No (2)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Registration No (3)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Registration No (4)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Date of Sale</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Date of Registration</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/9999</typ:value>
               <typ:name>Registration Validity Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Give Vehicle Details</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether trailer attached to the vehicle</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Number of Trailers Attached</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0.00</typ:value>
               <typ:name>Total IDV of the Trailer Attached</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0.00</typ:value>
               <typ:name>Value of Music System</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0.00</typ:value>
               <typ:name>Value of AC/Fan</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0.00</typ:value>
               <typ:name>Value of Lights</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0.00</typ:value>
               <typ:name>Value of Other Fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Total Value of Extra Electrical/ Electronic fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Additional Towing Coverage Amount</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Previous Policy No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Name of Association</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Membership No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Is Life Member</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Date of Membership Expiry</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Details of Vehicle Condition</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Discretion to RO</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Approval No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/9999</typ:value>
               <typ:name>Approval Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Bangladesh</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Bhutan</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Nepal</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Pakistan</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Sri Lanka</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Maldives</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Value of Fibre glass fuel tanks</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Bi-fuel System Value</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>In Built Bi-fuel System fitted</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>METAL</typ:value>
               <typ:name>Two Wheelers Type Of Body</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>king345</typ:value>
               <typ:name>(*)Engine No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>queen122</typ:value>
               <typ:name>(*)Chassis No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Make</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Model</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Vehicle in roadworthy condition and free from damage</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Vehicle Requisitioned by Government</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether vehicle is used for driving tuition</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether vehicle belongs to foreign embassy or consulate</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether vehicle is certified as Vintage car by Vintage and Classic Car Club</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Vehicle designed for Blind/Handicapped/Mentally Challenged persons and endorsed by RTA</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Are you a member of Automobile Association of India</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Is the vehicle fitted with Anti-theft device</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Obsolete Vehicle</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area required</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether Vehicle belong to Embassies  or imported without Custom Duty</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Vehicle Zone For Two Wheeler</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Variant</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Name and Address of Registration Authority</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>(*)Cubic Capacity</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>(*)Seating Capacity</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Type of Fuel</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Fibre Glass Tank Fitted</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Vehicle Invoice Value</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Vehicle Age</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Extra Electrical/ Electronic fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Non-Electrical/ Electronic fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Value of Non- Electrical/ Electronic fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Additional Towing Coverage Required</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Do You Hold Valid Driving License</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Do you Have Any other Driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Insureds declared Value (IDV)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>IDV of Accessories</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Total IDV</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>NCB Applicable Percentage</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>NCB %sn previous policy</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value/>
               <typ:name>Is any claim pending/paid on policy which is to be renewed</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>NA</typ:value>
               <typ:name>Name of Previous Insurer</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>NA</typ:value>
               <typ:name>Address of the Previous Insurer</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value/>
               <typ:name>Date of Purchase of Vehicle by Proposer</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>New</typ:value>
               <typ:name>Vehicle New or Second hand at the time of purchase</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Vehicle Used for Private,Social,domestic,pleasure,professional purpose</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Is vehicle in good condition</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Expiry date of previous Policy</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Policy Excess (Rs)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Voluntary Excess for TW</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>STDTWWHL</typ:value>
               <typ:name>Type of Two Wheeler</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Do you want Loss of Accessories Cover</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Is side car attached with a two wheeler</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Driver Type Of Two Wheeler</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Driver Name</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Owner Driver Driving License No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Owner Driver License Issue Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Owner Driver License Expiry Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value/>
               <typ:name>License Type of Owner Driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value/>
               <typ:name>License Issuing Authority for Owner Driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value/>
               <typ:name>Name of Nominee</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Age of Nominee</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value/>
               <typ:name>Relationship with the Insured</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value/>
               <typ:name>Gender of the Nominee</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>License Number</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Issue Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Expiry Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Age of driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Date of birth</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Age of Owner Driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>M</typ:value>
               <typ:name>Sex</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Experience</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Address</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Any of the driver ever convicted or any prosecution pending</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Imposed Excess (Rs)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Loading amount for OD</typ:name>
            </typ:properties>
         </typ:risks>
      </typ:calculatePremiumMasterElement>
   </soapenv:Body>
</soapenv:Envelope>
"""

# ######################### createPolicyHol_GenElement  ##########################
createPolicyHol_GenElement_xml = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
  <soapenv:Body xmlns:ns1="http://iims.services/types/">
    <ns1:createPolicyHol_GenElement>
      <ns1:userCode>BRCOVERFOX</ns1:userCode>
      <ns1:rolecode>SUPERUSER</ns1:rolecode>
      <ns1:PRetCode>1</ns1:PRetCode>
      <ns1:userId/>
      <ns1:stakeCode>BROKER</ns1:stakeCode>
      <ns1:roleId/>
      <ns1:userroleId/>
      <ns1:branchcode/>
      <ns1:PRetErr/>
      <ns1:startDate>%s</ns1:startDate>
      <ns1:stakeName/>
      <ns1:title/>
      <ns1:typeOfOrg/>
      <ns1:address>%s</ns1:address>
      <ns1:firstName>%s</ns1:firstName>
      <ns1:partyCode/>
      <ns1:company>NIA</ns1:company>
      <ns1:sex>%s</ns1:sex>
      <ns1:address2></ns1:address2>
      <ns1:EMailid2></ns1:EMailid2>
      <ns1:partyStakeCode>POLICY-HOL</ns1:partyStakeCode>
      <ns1:partyType>I</ns1:partyType>
      <ns1:regNo/>
      <ns1:midName></ns1:midName>
      <ns1:city/>
      <ns1:phNo3>%s</ns1:phNo3>
      <ns1:regDate/>
      <ns1:contactType>Permanent</ns1:contactType>
      <ns1:businessName/>
      <ns1:status/>
      <ns1:EMailid1>%s</ns1:EMailid1>
      <ns1:clientType>NonCorporate</ns1:clientType>
      <ns1:birthDate>%s</ns1:birthDate>
      <ns1:lastName>%s</ns1:lastName>
      <ns1:sector>NA</ns1:sector>
      <ns1:country/>
      <ns1:pinCode>%s</ns1:pinCode>
      <ns1:prospectId/>
      <ns1:state/>
      <ns1:address3/>
      <ns1:phNo1></ns1:phNo1>
      <ns1:businessAddress/>
      <ns1:phNo2></ns1:phNo2>
      <ns1:partyName/>
      <ns1:panNo>%s</ns1:panNo>
    </ns1:createPolicyHol_GenElement>
  </soapenv:Body>
</soapenv:Envelope>
"""

# ########################  SaveQuote_ApproveProposal  ############################
saveQuote_ApproveProposal_PvtCar_Xml = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body xmlns:typ="http://iims.services/types/">
      <typ:SaveQuote_ApproveProposalElement>
         <typ:userCode>BRCOVERFOX</typ:userCode>
         <typ:rolecode>SUPERUSER</typ:rolecode>
         <typ:PRetCode>0</typ:PRetCode>
         <typ:userId/>
         <typ:stakeCode>BROKER</typ:stakeCode>
         <typ:roleId/>
         <typ:userroleId/>
         <typ:branchcode/>
         <typ:PRetErr/>
         <typ:polBranchCode/>
         <typ:productName>%s</typ:productName>
         <typ:policyHoldercode>%s</typ:policyHoldercode>
         <typ:eventDate>%s</typ:eventDate>
         <typ:netPremium/>
         <typ:termUnit>G</typ:termUnit>
         <typ:grossPremium/>
         <typ:policyType/>
         <typ:polInceptiondate>%s</typ:polInceptiondate>
         <typ:polStartdate>%s</typ:polStartdate>
         <typ:polExpirydate>%s</typ:polExpirydate>
         <typ:branchCode/>
         <typ:term>%s</typ:term>
         <typ:polEventEffectiveEndDate/>
         <typ:productCode>%s</typ:productCode>
         <typ:policyHolderName>%s</typ:policyHolderName>
         <typ:productId>%s</typ:productId>
         <typ:serviceTax/>
         <typ:status/>
         <typ:cbDetails/>
         <typ:sumInsured/>
         <typ:updateDate/>
         <typ:policyId/>
         <typ:polDetailLastUpdateDate/>
         <typ:quoteNo/>
         <typ:policyNo/>
         <typ:policyDetailid/>
         <typ:documentLink/>
         <typ:polLastUpdateDate/>
         <typ:properties>
            <typ:value>COVERFOX</typ:value>
            <typ:name>channelcode</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Fire explosion self ignition or lightning peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Burglary housebreaking or theft peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Riot and strike peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Earthquake damage peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Flood typhoon hurricane storm tempest inundation cyclone hailstorm frost peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Accidental external means peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Malicious act peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Terrorist activity peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Whilst in transit by road rail inland-waterway lift elevator or air peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Landslide rockslide peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>N</typ:value>
            <typ:name>Is it declaration type policy</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>N</typ:value>
            <typ:name>Is Service Tax Exempted</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>NO</typ:value>
            <typ:name>Co-Insurance Applicable</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>N</typ:value>
            <typ:name>Is Business Sourced from Tie Up</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Non-Dealer</typ:value>
            <typ:name>Auto Tie Up Type</typ:name>
         </typ:properties>
         <typ:risks>
            <typ:riskCode>VEHICLE</typ:riskCode>
            <typ:riskSuminsured>0</typ:riskSuminsured>
            <typ:covers>
               <typ:productCode>%s</typ:productCode>
               <typ:policyDetailid/>
               <typ:coverCode>%s</typ:coverCode>
               <typ:productId/>
               <typ:coverExpiryDate/>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>Do You want to reduce TPPD cover to the statutory limit of Rs.6000</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>200000</typ:value>
                  <typ:name>Sum Insured for PA to Owner Driver</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>Do you want to include PA cover for Named Person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Number of Named Persons</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>none</typ:value>
                  <typ:name>Names of Named person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Individual SI for Named Person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Capital SI for All Named Persons</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>No of Paid Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Individual SI for Paid Driver</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Capital SI for Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>No of unnamed Persons</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>Individual SI for unnamed Person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>Capital SI for unnamed Persons</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Number of LL to Soldiers/Sailors/Airmen</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Number Of Legal Liable Employees</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>Number of Legal Liable Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>Do you wish to include PA Cover for Paid Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>Do you want to include PA cover for unnamed person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>LL to paid drivers,cleaner employed  for opn. and/or maint. of vehicle under WCA</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>LL to Employees of Insured traveling and / or driving the Vehicle</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>LL to Soldiers/Sailors/Airmen employed as Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Sum Insured for TPPD</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>A</typ:value>
                  <typ:name>Type of Liability Coverage</typ:name>
               </typ:properties>
            </typ:covers>
            <typ:properties>
               <typ:value>Private</typ:value>
               <typ:name>Type of Private Car</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>New</typ:value>
               <typ:name>Current Ownership</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>BLACK</typ:value>
               <typ:name>Color of Vehicle</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Year of Manufacture</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>New Vehicle</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Registration No (1)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Registration No (2)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Registration No (3)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Registration No (4)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Date of Sale</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Date of Registration</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/2029</typ:value>
               <typ:name>Registration Validity Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Give Vehicle Details</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether trailer attached to the vehicle</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Number of Trailers Attached</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Total IDV of the Trailer Attached</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Value of Music System</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Value of AC/Fan</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Value of Lights</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Value of Other Fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Total Value of Extra Electrical/ Electronic fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Additional Towing Coverage Amount</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Previous Policy Number</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Name of Association</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Membership No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Is Life Member</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Date of Membership Expiry</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Details of Vehicle Condition</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Discretion to RO</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Approval No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Approval Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Bangladesh</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Bhutan</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Nepal</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Pakistan</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Sri Lanka</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Maldives</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Value of Fibre glass fuel tanks</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Bi-fuel System Value</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>In Built Bi-fuel System fitted</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>SALOON</typ:value>
               <typ:name>Type of Body</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>(*)Engine No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>(*)Chassis No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Make</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Model</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Car in roadworthy condition and free from damage</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Vehicle Requisitioned by Government</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether vehicle is used for driving tuition</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Vehicle use is limited to own premises</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether vehicle belongs to foreign embassy or consulate</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether vehicle is certified as Vintage car by Vintage and Classic Car Club</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Vehicle designed for Blind/Handicapped/Mentally Challenged persons and endorsed by RTA</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Are you a member of Automobile Association of India</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Is the vehicle fitted with Anti-theft device</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Obsolete Vehicle</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area required</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether Vehicle belong to Embassies or imported without Custom Duty</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Vehicle Zone for Private Car</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Variant</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Name and Address of Registration Authority</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>(*)Cubic Capacity</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>(*)Seating Capacity</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Type of Fuel</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Fibre Glass Tank Fitted</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Vehicle Invoice Value</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Vehicle Age in Months</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Extra Electrical/ Electronic fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Non-Electrical/ Electronic fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Value of Non- Electrical/ Electronic fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Additional Towing Coverage Required</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Do You Hold Valid Driving License</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>License Type of Owner Driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Age of Owner Driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Owner Driver Driving License No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Owner Driver License Expiry Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>License Issuing Authority for Owner Driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Name of Nominee</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Age of Nominee</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Relationship with the Insured</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>MALE</typ:value>
               <typ:name>Gender of the Nominee</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Do you Have Any other Driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Insureds declared Value (IDV)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>IDV of Accessories</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Total IDV</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>NCB Applicable Percentage</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Name of Previous Insurer</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Address of the Previous Insurer</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/2014</typ:value>
               <typ:name>Date of Purchase of Vehicle by Proposer</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Vehicle New or Second hand at the time of purchase</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Vehicle Used for Private,Social,domestic,pleasure,professional purpose</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Is vehicle in good condition</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Expiry date of previous Policy</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Policy Excess (Rs)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Voluntary Excess for PC</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Imposed Excess (Rs)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Loading amount for OD</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value/>
               <typ:name>Owner Driver License Issue Date</typ:name>
            </typ:properties>
         </typ:risks>
      </typ:SaveQuote_ApproveProposalElement>
   </soapenv:Body>
</soapenv:Envelope>
"""


saveQuote_ApproveProposal_Xml = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body xmlns:typ="http://iims.services/types/">
      <typ:SaveQuote_ApproveProposalElement>
         <typ:userCode>BRCOVERFOX</typ:userCode>
         <typ:rolecode>SUPERUSER</typ:rolecode>
         <typ:PRetCode>0</typ:PRetCode>
         <typ:userId/>
         <typ:roleId/>
         <typ:userroleId/>
         <typ:branchcode/>
         <typ:PRetErr/>
         <typ:polBranchCode/>
         <typ:productName>%s</typ:productName>
         <typ:policyHoldercode>%s</typ:policyHoldercode>
         <typ:eventDate>%s</typ:eventDate>
         <typ:netPremium/>
         <typ:termUnit>G</typ:termUnit>
         <typ:grossPremium/>
         <typ:policyType/>
         <typ:polInceptiondate>%s</typ:polInceptiondate>
         <typ:polStartdate>%s</typ:polStartdate>
         <typ:polExpirydate>%s</typ:polExpirydate>
         <typ:branchCode/>
         <typ:term>%s</typ:term>
         <typ:polEventEffectiveEndDate/>
         <typ:productCode>%s</typ:productCode>
         <typ:policyHolderName>%s</typ:policyHolderName>
         <typ:productId>%s</typ:productId>
         <typ:serviceTax/>
         <typ:status/>
         <typ:cbDetails/>
         <typ:sumInsured/>
         <typ:updateDate/>
         <typ:policyId/>
         <typ:polDetailLastUpdateDate/>
         <typ:quoteNo/>
         <typ:policyNo/>
         <typ:policyDetailid/>
         <typ:stakeCode>BROKER</typ:stakeCode>
         <typ:documentLink/>
         <typ:polLastUpdateDate/>
         <typ:properties>
            <typ:value>COVERFOX</typ:value>
            <typ:name>channelcode</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Fire explosion self ignition or lightning peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Burglary housebreaking or theft peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Riot and strike peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Earthquake damage peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Flood typhoon hurricane storm tempest inundation cyclone hailstorm frost peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Accidental external means peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Malicious act peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Terrorist activity peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Whilst in transit by road rail inland-waterway lift elevator or air peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Y</typ:value>
            <typ:name>Landslide rockslide peril required</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>N</typ:value>
            <typ:name>Is it declaration type policy</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>N</typ:value>
            <typ:name>Is Service Tax Exempted</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>NO</typ:value>
            <typ:name>Co-Insurance Applicable</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>N</typ:value>
            <typ:name>Is Business Sourced from Tie Up</typ:name>
         </typ:properties>
         <typ:properties>
            <typ:value>Non-Dealer</typ:value>
            <typ:name>Auto Tie Up Type</typ:name>
         </typ:properties>
         <typ:party>
            <typ:userCode/>
            <typ:rolecode/>
            <typ:PRetCode>0</typ:PRetCode>
            <typ:userId/>
            <typ:stakeCode/>
            <typ:roleId/>
            <typ:userroleId/>
            <typ:branchcode/>
            <typ:PRetErr/>
            <typ:startDate/>
            <typ:stakeName/>
            <typ:title/>
            <typ:typeOfOrg/>
            <typ:address/>
            <typ:firstName/>
            <typ:partyCode/>
            <typ:company/>
            <typ:sex/>
            <typ:address2/>
            <typ:EMailid2/>
            <typ:partyStakeCode>FINANCIER</typ:partyStakeCode>
            <typ:partyType/>
            <typ:regNo/>
            <typ:midName/>
            <typ:city/>
            <typ:phNo3/>
            <typ:regDate/>
            <typ:contactType/>
            <typ:businessName/>
            <typ:status/>
            <typ:EMailid1/>
            <typ:clientType/>
            <typ:birthDate/>
            <typ:lastName/>
            <typ:sector/>
            <typ:country/>
            <typ:pinCode/>
            <typ:prospectId/>
            <typ:state/>
            <typ:address3/>
            <typ:phNo1/>
            <typ:businessAddress/>
            <typ:phNo2/>
            <typ:partyName/>
         </typ:party>
         <typ:risks>
            <typ:riskCode>VEHICLE</typ:riskCode>
            <typ:riskSuminsured>0</typ:riskSuminsured>
            <typ:covers>
               <typ:productCode>%s</typ:productCode>
               <typ:policyDetailId/>
               <typ:coverCode>%s</typ:coverCode>
               <typ:productId/>
               <typ:coverExpiryDate/>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>Do You want to reduce TPPD cover to the statutory limit of Rs.6000</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Sum Insured for PA to Owner Driver</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>Do you want to include PA cover for Named Person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Number of Named Persons</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>none</typ:value>
                  <typ:name>Names of Named person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Individual SI for Named Person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Capital SI for All Named Persons</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>No of Paid Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Individual SI for Paid Driver</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Capital SI for Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>No of unnamed Persons</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>Individual SI for unnamed Person</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>Capital SI for unnamed Persons</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Number of LL to Soldiers/Sailors/Airmen</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Number Of Legal Liable Employees</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>Number of Legal Liable Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>Do you wish to include PA Cover for Paid Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>Do you want to include PA cover for unnamed person/hirer/pillion passangers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>%s</typ:value>
                  <typ:name>LL to paid drivers,cleaner employed  for opn. and/or maint. of vehicle under WCA</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>LL to Employees of Insured traveling and / or driving the Vehicle</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>N</typ:value>
                  <typ:name>LL to Soldiers/Sailors/Airmen employed as Drivers</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>0</typ:value>
                  <typ:name>Sum Insured for TPPD</typ:name>
               </typ:properties>
               <typ:properties>
                  <typ:value>A</typ:value>
                  <typ:name>Type of Liability Coverage</typ:name>
               </typ:properties>
            </typ:covers>
            <typ:properties>
               <typ:value>New</typ:value>
               <typ:name>Current Ownership</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>BLACK</typ:value>
               <typ:name>Color of Vehicle</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Year of Manufacture</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>New Vehicle</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Registration No (1)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Registration No (2)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Registration No (3)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Registration No (4)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Date of Sale</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Date of Registration</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/9999</typ:value>
               <typ:name>Registration Validity Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Give Vehicle Details</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether trailer attached to the vehicle</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Number of Trailers Attached</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0.00</typ:value>
               <typ:name>Total IDV of the Trailer Attached</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0.00</typ:value>
               <typ:name>Value of Music System</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0.00</typ:value>
               <typ:name>Value of AC/Fan</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0.00</typ:value>
               <typ:name>Value of Lights</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0.00</typ:value>
               <typ:name>Value of Other Fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Total Value of Extra Electrical/ Electronic fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Additional Towing Coverage Amount</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Previous Policy No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Name of Association</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Membership No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Is Life Member</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Date of Membership Expiry</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Details of Vehicle Condition</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Discretion to RO</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Approval No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/9999</typ:value>
               <typ:name>Approval Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Bangladesh</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Bhutan</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Nepal</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Pakistan</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Sri Lanka</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area to Maldives</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Value of Fibre glass fuel tanks</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Bi-fuel System Value</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>In Built Bi-fuel System fitted</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>METAL</typ:value>
               <typ:name>Two Wheelers Type Of Body</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>(*)Engine No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>(*)Chassis No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Make</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Model</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Vehicle in roadworthy condition and free from damage</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Vehicle Requisitioned by Government</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether vehicle is used for driving tuition</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether vehicle belongs to foreign embassy or consulate</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether vehicle is certified as Vintage car by Vintage and Classic Car Club</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Vehicle designed for Blind/Handicapped/Mentally Challenged persons and endorsed by RTA</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Are you a member of Automobile Association of India</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Is the vehicle fitted with Anti-theft device</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Obsolete Vehicle</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Extension of Geographical Area required</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Whether Vehicle belong to Embassies  or imported without Custom Duty</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Vehicle Zone For Two Wheeler</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Variant</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Name and Address of Registration Authority</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>(*)Cubic Capacity</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>(*)Seating Capacity</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Type of Fuel</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Fibre Glass Tank Fitted</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Vehicle Invoice Value</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Vehicle Age</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Extra Electrical/ Electronic fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Non-Electrical/ Electronic fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Value of Non- Electrical/ Electronic fittings</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Additional Towing Coverage Required</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Do You Hold Valid Driving License</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Do you Have Any other Driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Insureds declared Value (IDV)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>IDV of Accessories</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Total IDV</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>NCB Applicable Percentage</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>NCB %sn previous policy</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value/>
               <typ:name>Is any claim pending/paid on policy which is to be renewed</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Name of Previous Insurer</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Address of the Previous Insurer</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value/>
               <typ:name>Date of Purchase of Vehicle by Proposer</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>New</typ:value>
               <typ:name>Vehicle New or Second hand at the time of purchase</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Vehicle Used for Private,Social,domestic,pleasure,professional purpose</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>Y</typ:value>
               <typ:name>Is vehicle in good condition</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Expiry date of previous Policy</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Policy Excess (Rs)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Voluntary Excess for TW</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>STDTWWHL</typ:value>
               <typ:name>Type of Two Wheeler</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Do you want Loss of Accessories Cover</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Is side car attached with a two wheeler</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Driver Type Of Two Wheeler</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Driver Name</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Owner Driver Driving License No</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Owner Driver License Issue Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Owner Driver License Expiry Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value/>
               <typ:name>License Type of Owner Driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value/>
               <typ:name>License Issuing Authority for Owner Driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Name of Nominee</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Age of Nominee</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Relationship with the Insured</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value/>
               <typ:name>Gender of the Nominee</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>License Number</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Issue Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Expiry Date</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Age of driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>01/01/0001</typ:value>
               <typ:name>Date of birth</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Age of Owner Driver</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>%s</typ:value>
               <typ:name>Sex</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Experience</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>none</typ:value>
               <typ:name>Address</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>N</typ:value>
               <typ:name>Any of the driver ever convicted or any prosecution pending</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Imposed Excess (Rs)</typ:name>
            </typ:properties>
            <typ:properties>
               <typ:value>0</typ:value>
               <typ:name>Loading amount for OD</typ:name>
            </typ:properties>
         </typ:risks>
      </typ:SaveQuote_ApproveProposalElement>
   </soapenv:Body>
</soapenv:Envelope>
"""

collectPremium_issuePol_XML = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body xmlns:ns1="http://iims.services/types/">
      <ns1:collectpremium_IssuepolElement>
         <ns1:userCode>BRCOVERFOX</ns1:userCode>
         <ns1:rolecode>SUPERUSER</ns1:rolecode>
         <ns1:PRetCode>1</ns1:PRetCode>
         <ns1:userId>COVERFOX</ns1:userId>
         <ns1:stakeCode>BROKER</ns1:stakeCode>
         <ns1:roleId />
         <ns1:userroleId />
         <ns1:branchcode />
         <ns1:PRetErr />
         <ns1:sourceOfCollection>A</ns1:sourceOfCollection>
         <ns1:collectionNo />
         <ns1:receivedFrom />
         <ns1:instrumentAmt>%s</ns1:instrumentAmt>
         <ns1:collections>
            <ns1:accountCode>9100.130200</ns1:accountCode>
            <ns1:draweeBankName />
            <ns1:subCode />
            <ns1:draweeBankCode />
            <ns1:collectionMode>EPG</ns1:collectionMode>
            <ns1:debitCreditInd>D</ns1:debitCreditInd>
            <ns1:scrollNo />
            <ns1:chequeType />
            <ns1:quoteNo>%s</ns1:quoteNo>
            <ns1:collectionAmount>%s</ns1:collectionAmount>
            <ns1:chequeDate />
            <ns1:chequeNo>%s</ns1:chequeNo>
            <ns1:draweeBankBranch />
         </ns1:collections>
         <ns1:quoteNo>%s</ns1:quoteNo>
         <ns1:collectionType>A</ns1:collectionType>
         <ns1:policyNo />
         <ns1:documentLink />
      </ns1:collectpremium_IssuepolElement>
   </soapenv:Body>
</soapenv:Envelope>
"""

fetch_policy_document_xml = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body xmlns:ns1="http://iims.services/types/">
      <ns1:fetchDocumentNameElement>
         <ns1:userCode>BRCOVERFOX</ns1:userCode>
         <ns1:docs>
            <ns1:value/>
            <ns1:name/>
         </ns1:docs>
         <ns1:indexType>
            <ns1:index/>
            <ns1:type/>
         </ns1:indexType>
         <ns1:policyId>%s</ns1:policyId>
      </ns1:fetchDocumentNameElement>
   </soapenv:Body>
</soapenv:Envelope>
"""
