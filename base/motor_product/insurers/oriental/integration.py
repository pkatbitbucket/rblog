import json
import math
import copy
import datetime
import StringIO
from ftplib import FTP, parse227, parse229
import socket
from django.conf import settings
from django.db.models import Q
from django.core.mail import EmailMessage
from celery_app import app
from celery.contrib.methods import task_method
from celery.exceptions import MaxRetriesExceededError
from motor_product.models import (
    Insurer, RTOMaster, Vehicle, VehicleMaster, DiscountLoading, Transaction, IntegrationStatistics
)
from motor_product.insurers import insurer_utils
from motor_product.insurers.oriental import custom_dictionaries as insurer_data

from utils import motor_logger, log_error
from motor_product import mail_utils, logger_utils

from suds.client import Client
from payment_gateway.gateways.billdesk import Billdesk

import logging
logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)

if settings.PRODUCTION:
    # PRODUCTION SERVICE
    API_URL = 'http://223.30.244.2/MotorService/MotorService.asmx?wsdl'
else:
    # UAT SERVICE
    API_URL = 'http://223.30.244.2/MotorServiceStaging/MotorService.asmx?wsdl'

# POLICY FTP SETTINGS
POLICY_FTP_HOST = '223.30.244.2'
POLICY_FTP_PORT = '83'
FTP_USER = 'COVERFOXFTP'
FTP_PASSWORD = 'TVLUO8SI26'
POLICY_DIR = 'COVERFOX'

PREMIUM_DIFFERENCE_ALLOWED = 5
PAYMENT_MODE = '3'  # Credit/Debit Card

INSURER_SLUG = 'oriental'
try:
    INSURER = Insurer.objects.get(slug=INSURER_SLUG)
    INSURER_ID = INSURER.id
    INSURER_NAME = INSURER.title
except:
    INSURER_ID = 0
    INSURER_NAME = ''

suds_logger = logger_utils.getLogger(INSURER_SLUG)

ORIENTAL_PROCESS_TEAM_MAILIDS = [
    'soman@coverfox.com', 'harshad@coverfox.com',
    'motorpolicy@coverfox.com'
]
POLICY_GENERATION_ISSUE_MAILIDS = [
    'akash.totade@coverfoxmail.com', 'shweta@coverfox.com',
    'mayur.kalsekar@coverfox.com', 'priyanka.pundhir@coverfox.com',
    'akhil@coverfox.com', 'motorpolicy@coverfox.com'
]

RTO_ZONES_A = ['MH-01', 'MH-02', 'MH-03', 'DL-1', 'DL-2', 'DL-3', 'DL-4', 'DL-5', 'DL-6', 'DL-7', 'DL-8', 'DL-9',
               'DL-10', 'DL-11', 'DL-12', 'DL-13', 'DL-14', 'DL-15', 'DL-16', 'DL-17', 'DL-18', 'WB-01', 'WB-02', 'WB-03', 'WB-04',
               'WB-05', 'WB-06', 'WB-07', 'WB-08', 'TN-01', 'TN-02', 'TN-03', 'TN-04', 'TN-05', 'TN-06', 'TN-07', 'TN-09', 'GJ-1',
               'MH-12', 'KA-01', 'KA-02', 'KA-03', 'KA-04', 'KA-05', 'TS-09', 'TS-10', 'TS-11', 'TS-12', 'TS-13', 'AP-09', 'AP-10',
               'AP-11', 'AP-12', 'AP-13']

DEPRECIATION_FACTOR = {
    0: 0.95,
    0.5: 0.85,
    1: 0.80,
    2: 0.70,
    3: 0.60,
    4: 0.50,
    5: 0.475,
    6: 0.4512,
    7: 0.4287,
    8: 0.4073,
    9: 0.3869,
}

def get_vehicle_age(registration_date):
    today_date = datetime.datetime.today().date()
    reg_date = registration_date.date()
    delta = today_date - reg_date
    return float(delta.days) / 365


class Configuration(object):

    def __init__(self, vehicle_type):

        self.API_URL = API_URL
        self.VEHICLE_TYPE = vehicle_type
        if vehicle_type == 'Private Car':
            self.VEHICLE_AGE_LIMIT = 10
            self.PRODUCT_CODE = 'MOT-PRD-001'
            self.TARIFF_RATES_DICTIONARY = {
                0: {
                    'B': {
                        0: 3.039,
                        1: 3.191,
                        2: 3.343,
                    },
                    'A': {
                        0: 3.127,
                        1: 3.283,
                        2: 3.440,
                    }
                },
                1: {
                    'B': {
                        0: 3.191,
                        1: 3.351,
                        2: 3.510,
                    },
                    'A': {
                        0: 3.283,
                        1: 3.447,
                        2: 3.612,
                    }
                },
                2: {
                    'B': {
                        0: 3.267,
                        1: 3.430,
                        2: 3.594,
                    },
                    'A': {
                        0: 3.362,
                        1: 3.529,
                        2: 3.698,
                    }
                },
            }

            self.TP_PREMIUM_BASIC_DICTIONARY = {
                0: 1468,
                1: 1598,
                2: 4931,
            }

            self.NEW_TP_PREMIUM_BASIC_DICTIONARY = {
                0: 2055,
                1: 2237,
                2: 6164,
            }

            self.CC_LIMITS = [1000, 1500]

            self.ZERO_DEPRECIATION_RATE = {
                0: 15,
                1: 25,
                2: 35,
                3: 40,
            }

            self.ZERO_DEPRECIATION_AGE_SLABS = [0.5, 2, 5, 10]

            self.RTI_RATE = {
                0: 0.3,
                1: 0.4,
                2: 0.6,
            }

            self.RTI_AGE_SLABS = [1, 2, 3]

            self.VD_MAP = insurer_data.PRIVATE_CAR_VD_MAP

        elif vehicle_type == 'Twowheeler':
            self.VEHICLE_AGE_LIMIT = 10
            self.PRODUCT_CODE = 'MOT-PRD-002'
            self.TARIFF_RATES_DICTIONARY = {
                0: {
                    'B': {
                        0: 1.676,
                        1: 1.676,
                        2: 1.760,
                        3: 1.844,
                    },
                    'A': {
                        0: 1.708,
                        1: 1.708,
                        2: 1.793,
                        3: 1.879,
                    }
                },
                1: {
                    'B': {
                        0: 1.760,
                        1: 1.760,
                        2: 1.848,
                        3: 1.936,
                    },
                    'A': {
                        0: 1.793,
                        1: 1.793,
                        2: 1.883,
                        3: 1.973,
                    }
                },
                2: {
                    'B': {
                        0: 1.802,
                        1: 1.802,
                        2: 1.892,
                        3: 1.982,
                    },
                    'A': {
                        0: 1.836,
                        1: 1.836,
                        2: 1.928,
                        3: 2.020,
                    }
                },
            }

            self.TP_PREMIUM_BASIC_DICTIONARY = {
                0: 519,
                1: 538,
                2: 554,
                3: 884,
            }

            self.NEW_TP_PREMIUM_BASIC_DICTIONARY = {
                0: 569,
                1: 619,
                2: 693,
                3: 796,
            }

            self.CC_LIMITS = [75, 150, 350]

            self.ZERO_DEPRECIATION_RATE = {
                0: 15,
                1: 25,
                2: 35,
                3: 40,
            }

            self.ZERO_DEPRECIATION_AGE_SLABS = [0.5, 2, 5, 10]

            self.RTI_RATE = {
                0: 0.3,
                1: 0.4,
                2: 0.6,
            }

            self.RTI_AGE_SLABS = [1, 2, 3]

            self.VD_MAP = insurer_data.TWO_WHEELER_VD_MAP


class PremiumCalculator(object):

    def __init__(self, configuration):
        self.Configuration = configuration

    def _get_depreciation_factor(self, vehicle_age):
        if vehicle_age >= 0.5 and vehicle_age < 1:
            depreciation_index = 0.5
        else:
            depreciation_index = math.floor(vehicle_age)
        return DEPRECIATION_FACTOR[depreciation_index]

    def _get_exshowroom_price_and_seating_capacity(self, master_vehicle):
        vehicles = master_vehicle.sub_vehicles.filter(
            insurer__slug='bharti-axa')
        ex_showroom_price = None
        seating_capacity = None
        if len(vehicles) > 0:
            ex_showroom_price = float(vehicles[0].ex_showroom_price)
            seating_capacity = vehicles[0].seating_capacity
        else:
            motor_logger.info('======== NO VEHICLE FOUND FOR INSURER - %s - VEHICLE - %s ===========' %
                              ('bharti-axa in oriental', master_vehicle))
        return ex_showroom_price, seating_capacity

    def _get_zero_depreciation_rate(self, vehicle_age):
        age_slab = 0
        for age in self.Configuration.ZERO_DEPRECIATION_AGE_SLABS:
            if vehicle_age <= age:
                return self.Configuration.ZERO_DEPRECIATION_RATE[age_slab]
            age_slab += 1
        return None

    def _get_rti_rate(self, vehicle_age):
        age_slab = 0
        for age in self.Configuration.RTI_AGE_SLABS:
            if vehicle_age <= age:
                return self.Configuration.RTI_RATE[age_slab]
            age_slab += 1
        return None

    def get_idv(self, ex_showroom_price, vehicle_age):
        idv = ex_showroom_price * self._get_depreciation_factor(vehicle_age)
        min_idv = 0.8 * idv
        max_idv = 1.2 * idv
        return idv, min_idv, max_idv

    def get_premium(self, data_dictionary):

        if data_dictionary['isUsedVehicle'] == '1':
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_USED_VEHICLE_RESTRICTED}

        if data_dictionary['isNewVehicle'] == '0':
            past_policy_end_date = datetime.datetime.strptime(data_dictionary['pastPolicyExpiryDate'], "%d-%m-%Y")
            if past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now():
                motor_logger.info('Expired policy not allowed for this insurer %s.' % INSURER_SLUG)
                return {'success': False, 'error_code': IntegrationStatistics.INSURER_EXPIRED_POLICY_RESTRICTED}

        is_new_vehicle = True if data_dictionary[
            'isNewVehicle'] == '1' else False

        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                data_dictionary['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data_dictionary['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

        # Vehicle age calculation
        date_of_registration_or_manufacture = datetime.datetime.strptime(
            data_dictionary['registrationDate'], '%d-%m-%Y')

        if is_new_vehicle:
            vehicle_age = 0
        else:
            new_policy_start_date = new_policy_start_date.replace(
                microsecond=0)
            vehicle_age = get_vehicle_age(date_of_registration_or_manufacture)

        if vehicle_age >= self.Configuration.VEHICLE_AGE_LIMIT:
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_AGE_RESTRICTED}

        # Vehicle cc
        vehicle = Vehicle.objects.get(id=data_dictionary['vehicleId'])
        cc = vehicle.cc
        seating_capacity = vehicle.seating_capacity

        # Vehicle rto calculation
        split_registration_no = data_dictionary.get('registrationNumber[]')
        vehicle_rto = '-'.join(split_registration_no[0:2])

        tariff_rate = self._get_tariff_rate(vehicle_age, vehicle_rto, cc)

        master_vehicle = VehicleMaster.objects.get(
            id=data_dictionary['masterVehicleId'])
        ex_showroom_price, _ = self._get_exshowroom_price_and_seating_capacity(
            master_vehicle)

        if not ex_showroom_price:
            return {'success': False, 'error_code': IntegrationStatistics.VEHICLE_REQUIRED_DATA_NOT_FOUND}

        try:
            seating_capacity = int(seating_capacity)
        except:
            return {'success': False, 'error_code': IntegrationStatistics.VEHICLE_REQUIRED_DATA_NOT_FOUND}

        idv, min_idv, max_idv = self.get_idv(ex_showroom_price, vehicle_age)

        request_idv = float(data_dictionary['idv'])
        if request_idv < float(min_idv):
            data_dictionary['idv'] = min_idv
        elif request_idv > float(max_idv):
            data_dictionary['idv'] = max_idv

        data_dictionary['minIdv'] = min_idv
        data_dictionary['maxIdv'] = max_idv

        idv = float(data_dictionary['idv'])

        if self.Configuration.VEHICLE_TYPE == 'Twowheeler':
            # No CNG for twowheeler
            data_dictionary['isCNGFitted'] = '0'
            # No Non-electrical for twowheeler
            data_dictionary['idvNonElectrical'] = '0'
            # No Electrical for twowheeler
            data_dictionary['idvElectrical'] = '0'
            # No VD for TW
            data_dictionary['voluntaryDeductible'] = '0'

        returnToInvoice = 0
        rti_rate = self._get_rti_rate(vehicle_age)
        if rti_rate:
            returnToInvoice = rti_rate * \
                (idv + int(data_dictionary['idvNonElectrical'])) * 0.01

        # No invoice cover as it does not fit in current product
        returnToInvoice = 0

        basicOd = (idv * tariff_rate) / 100
        electrical = int(data_dictionary['idvElectrical']) * 0.04
        nonElectrical = int(data_dictionary['idvNonElectrical']) * tariff_rate * 0.01
        externalCngLpg = 0
        inBuiltCNG = 0

        if data_dictionary['isCNGFitted'] == '1':
            if data_dictionary['cngKitValue'] != '0':
                externalCngLpg = int(data_dictionary['cngKitValue']) * 0.04
            # else:
            #     inBuiltCNG = (basicOd + electrical + nonElectrical) * 0.05

        totalOd = (basicOd + electrical + nonElectrical +
                   externalCngLpg + inBuiltCNG)

        # Apply discount if any
        # discount = self._get_discount(vehicle, vehicle_age, vehicle_rto, newNCB)
        if self.Configuration.VEHICLE_TYPE == 'Twowheeler':
            discount = -30
        else:
            discount = -50
        special_discount = 0

        customer_discount_code = data_dictionary.get('discountCode', None)
        dc = insurer_utils.get_discount_code(customer_discount_code)
        if dc:
            discount_from_code = dc.get_discount(INSURER_SLUG, data_dictionary)
            if discount_from_code:
                special_discount = discount_from_code

        if discount + special_discount < -60:
            special_discount = -(60 + discount)

        motor_logger.info('Special discount applied for discount code %s is %s' % (
            customer_discount_code, special_discount))

        if data_dictionary['addon_isInvoiceCover'] == '1':
            odDiscount = (totalOd + returnToInvoice) * discount * 0.01
            specialOdDiscount = (totalOd + returnToInvoice) * special_discount * 0.01
        else:
            odDiscount = totalOd * discount * 0.01
            specialOdDiscount = totalOd * special_discount * 0.01

        totalOd = totalOd + odDiscount + specialOdDiscount

        zeroDepreciation = 0
        zero_depreciation_rate = self._get_zero_depreciation_rate(vehicle_age)
        if zero_depreciation_rate:
            zeroDepreciation = zero_depreciation_rate * totalOd * 0.01

        # No zero dep for TW
        if self.Configuration.VEHICLE_TYPE == 'Twowheeler':
            zeroDepreciation = 0

        voluntaryDeductibleAmount = int(data_dictionary['voluntaryDeductible'])
        voluntaryDeductible = insurer_utils.get_voluntary_deductible_discount_dict(
            vehicle.vehicle_type, (totalOd - odDiscount - specialOdDiscount))[voluntaryDeductibleAmount]

        antiTheftDiscount = 0
        if data_dictionary['extra_isAntiTheftFitted'] == '1':
            antiTheftDiscount = (-1) * \
                min((basicOd + electrical + nonElectrical + inBuiltCNG + externalCngLpg) * 2.5 * 0.01, 500)

        memberOfAutoAssociationDiscount = 0
        if data_dictionary['extra_isMemberOfAutoAssociation'] == '1':
            memberOfAutoAssociationDiscount = (-1) * \
                min((basicOd + electrical + nonElectrical + inBuiltCNG + externalCngLpg) * 5 * 0.01, 200)

        newNCB = 0
        if not is_new_vehicle and data_dictionary['isClaimedLastYear'] == '0':
            newNCB = int(insurer_data.PREVIOUS_NCB_TO_NEW_NCB[data_dictionary['previousNCB']])

        ncbDiscount = 0
        if not is_new_vehicle and data_dictionary['isClaimedLastYear'] == '0':
            if self.Configuration.VEHICLE_TYPE == 'Twowheeler':
                ncbDiscount = (-1) * newNCB * (
                    totalOd + zeroDepreciation +
                    antiTheftDiscount + memberOfAutoAssociationDiscount
                ) * 0.01
            else:
                ncbDiscount = (-1) * newNCB * (
                    totalOd + voluntaryDeductible +
                    antiTheftDiscount + memberOfAutoAssociationDiscount
                ) * 0.01
                zeroDepreciation = (1 - newNCB * 0.01) * zeroDepreciation

        cc_slab = self._get_cc_slot(cc)

        if new_policy_start_date.date() > datetime.date(2016, 3, 31):
            basicTp = self.Configuration.NEW_TP_PREMIUM_BASIC_DICTIONARY[cc_slab]
        else:
            basicTp = self.Configuration.TP_PREMIUM_BASIC_DICTIONARY[cc_slab]

        if self.Configuration.VEHICLE_TYPE == 'Twowheeler':
            if (totalOd + ncbDiscount + antiTheftDiscount + voluntaryDeductible + memberOfAutoAssociationDiscount) < 100:
                min_od = 100 - (totalOd + ncbDiscount + antiTheftDiscount + voluntaryDeductible + memberOfAutoAssociationDiscount)
                basicOd = basicOd + min_od
        else:
            if (totalOd + ncbDiscount + antiTheftDiscount + voluntaryDeductible + memberOfAutoAssociationDiscount) < 200:
                min_od = 200 - (totalOd + ncbDiscount + antiTheftDiscount + voluntaryDeductible + memberOfAutoAssociationDiscount)
                basicOd = basicOd + min_od

        paPassenger = insurer_utils.get_pa_passenger_dict(vehicle.vehicle_type, seating_capacity, ctsc=True)[
            int(data_dictionary['extra_paPassenger'])]

        addon_covers = []
        if data_dictionary['addon_isDepreciationWaiver'] == '1' and zeroDepreciation != 0:
            addon_covers.append({
                'id': 'isDepreciationWaiver',
                'default': '0',
                'premium': round(zeroDepreciation, 2),
            })

        if data_dictionary['addon_isInvoiceCover'] == '1' and returnToInvoice != 0:
            addon_covers.append({
                'id': 'isInvoiceCover',
                'default': '0',
                'premium': round(returnToInvoice, 2),
            })

        paOwnerDriver = 100
        if self.Configuration.VEHICLE_TYPE == 'Twowheeler':
            paOwnerDriver = 50

        basic_covers = [
            {
                'id': 'basicOd',
                'default': '1',
                'premium': round(basicOd, 2),
            },
            {
                'id': 'basicTp',
                'default': '1',
                'premium': round(basicTp, 2),
            },
            {
                'id': 'paOwnerDriver',
                'default': '1',
                'premium': paOwnerDriver,
            },
            # {
            #     'id': 'cngKit',
            #     'default': '1',
            #     'premium': round(inBuiltCNG, 2),
            # },
            {
                'id': 'cngKit',
                'default': '1',
                'premium': round(externalCngLpg, 2),
            },
            {
                'id': 'idvElectrical',
                'default': '1',
                'premium': round(electrical, 2),
            },
            {
                'id': 'idvNonElectrical',
                'default': '1',
                'premium': round(nonElectrical, 2),
            },
            {
                'id': 'paPassenger',
                'default': '1',
                'premium': paPassenger,
            }
        ]

        if data_dictionary['isCNGFitted'] == '1' and data_dictionary['cngKitValue'] != '0':
            basic_covers.append({
                'id': 'cngKitTp',
                'default': '1',
                'premium': 60,
            })

        if data_dictionary['extra_isLegalLiability'] == '1':
            basic_covers.append({
                'id': 'legalLiabilityDriver',
                'default': '0',
                'premium': 50,  # Constant value
            })

        discounts = [
            {
                'id': 'ncb',
                'default': '1',
                'premium': round(ncbDiscount, 2),
            },
            {'id': 'odDiscount',
                'default': '1',
                'premium': round(odDiscount, 2),
             },
        ]

        special_discounts = []

        if voluntaryDeductible != 0:
            special_discounts.append(
                {
                    'id': 'voluntaryDeductible',
                    'default': '1',
                    'premium': round(voluntaryDeductible, 2),
                }
            )

        if specialOdDiscount != 0:
            special_discounts.append({
                'id': 'extraDiscount',
                'default': '1',
                'premium': round(specialOdDiscount, 2),
            })

        if data_dictionary['extra_isAntiTheftFitted'] == '1' and antiTheftDiscount != 0:
            special_discounts.append({
                'id': 'isAntiTheftFitted',
                'default': '1',
                'premium': round(antiTheftDiscount, 2),
            })

        if data_dictionary['extra_isMemberOfAutoAssociation'] == '1' and memberOfAutoAssociationDiscount != 0:
            special_discounts.append({
                'id': 'isMemberOfAutoAssociation',
                'default': '1',
                'premium': round(memberOfAutoAssociationDiscount, 2),
            })

        total_premium = 0
        for cover in basic_covers:
            total_premium = total_premium + cover['premium']

        for discount in discounts:
            total_premium = total_premium + discount['premium']

        for cover in addon_covers:
            total_premium = total_premium + cover['premium']

        for discount in special_discounts:
            total_premium = total_premium + discount['premium']

        serviceTaxRate = 0.145
        if split_registration_no[0] == 'JK':
            serviceTaxRate = 0.126
        service_tax = total_premium * serviceTaxRate

        quote_response = {
            'insurerId': INSURER_ID,
            'insurerSlug': INSURER_SLUG,
            'insurerName': INSURER_NAME,
            'isInstantPolicy': False,
            'minIDV': int(math.ceil(float(data_dictionary['minIdv']))),
            'maxIDV': int(math.floor(float(data_dictionary['maxIdv']))),
            'calculatedAtIDV': int(math.floor(float(data_dictionary['idv']))),
            'serviceTaxRate': serviceTaxRate,
            'premiumBreakup': {
                'serviceTax': round(service_tax, 2),
                'basicCovers': insurer_utils.cpr_update(basic_covers),
                'addonCovers': insurer_utils.cpr_update(addon_covers),
                'discounts': insurer_utils.cpr_update(discounts),
                'specialDiscounts': insurer_utils.cpr_update(special_discounts),
                'paPassengerAmounts': insurer_utils.get_pa_passenger_dict(vehicle.vehicle_type, seating_capacity, ctsc=True),
            },
        }

        raw_quote = {'total_premium': round(float(total_premium) + float(service_tax), 2),
                     'service_tax': round(float(service_tax), 2), 'seating_capacity': seating_capacity}

        suds_logger.set_name(data_dictionary['quoteId']).log(json.dumps(
            {'quote_response': quote_response, 'raw_quote': raw_quote}))

        return {'result': (quote_response, raw_quote), 'success': True}

    def _get_zone(self, rto):
        rto_zone = 'B'
        if rto in RTO_ZONES_A:
            rto_zone = 'A'
        return rto_zone

    def _get_cc_slot(self, cc):
        cc_slab = 0
        for cc_limit in self.Configuration.CC_LIMITS:
            if cc_limit >= cc:
                break
            cc_slab += 1
        return cc_slab

    def _get_vehicle_age_slot(self, vehicle_age):
        if vehicle_age <= 5:
            return 0
        elif vehicle_age > 5 and vehicle_age <= 10:
            return 1
        else:
            return 2

    def _get_tariff_rate(self, vehicle_age, rto, cc):
        zone = self._get_zone(rto)
        cc_slab = self._get_cc_slot(cc)
        vehicle_age_slot = self._get_vehicle_age_slot(vehicle_age)
        rate = self.Configuration.TARIFF_RATES_DICTIONARY[
            vehicle_age_slot][zone][cc_slab]
        return rate

    # Not used for now
    def _get_discount(self, vehicle, vehicle_age, rto, ncb):
        insurer = Insurer.objects.get(id=INSURER_ID)

        rto_insurer = RTOMaster.objects.get(
            rto_code=rto).insurer_rtos.get(insurer__slug=INSURER_SLUG)

        dls_for_insurer = DiscountLoading.objects.filter(
            insurer=insurer, vehicle_type=vehicle.vehicle_type)

        ncbs = list(set(dls_for_insurer.values_list('ncb', flat=True)))
        ncbs.sort()

        if ncb == 0:
            ncb = None

        ncb_slab_max = None
        for ncb_slab_max in ncbs:
            if ncb_slab_max >= ncb:
                break

        ncb_q = Q(ncb=ncb_slab_max)

        age_q0 = Q(lower_age_limit__lt=vehicle_age) & Q(
            upper_age_limit__gte=vehicle_age)
        age_q1 = Q(lower_age_limit=None) & Q(upper_age_limit__gte=vehicle_age)
        age_q2 = Q(lower_age_limit__lt=vehicle_age) & Q(upper_age_limit=None)
        age_q3 = Q(lower_age_limit=None) & Q(upper_age_limit=None)
        age_q = age_q0 | age_q1 | age_q2 | age_q3

        cc_q0 = Q(lower_cc_limit__lt=vehicle.cc) & Q(
            upper_cc_limit__gte=vehicle.cc)
        cc_q1 = Q(lower_cc_limit=None) & Q(upper_cc_limit__gte=vehicle.cc)
        cc_q2 = Q(lower_cc_limit__lt=vehicle.cc) & Q(upper_cc_limit=None)
        cc_q3 = Q(lower_cc_limit=None) & Q(upper_cc_limit=None)
        cc_q = cc_q0 | cc_q1 | cc_q2 | cc_q3

        dls_after_age_cc = dls_for_insurer.filter(ncb_q & age_q & cc_q)

        dls_after_acr = dls_after_age_cc.filter(rtos=rto_insurer, state=None)
        if dls_after_acr.count() == 0:
            dls_after_acr = dls_after_age_cc.filter(
                state=rto_insurer.master_rto_state)

        if dls_after_acr.count() == 0:
            dls_after_acr = dls_after_age_cc.filter(state=None)

        discount = 0
        dls_f = []

        if vehicle.variant is not None:
            dls_f = dls_after_acr.filter(
                make=vehicle.make, model=vehicle.model, variant=vehicle.variant)

            if dls_f.count() == 0:
                dls_f = dls_after_acr.filter(
                    make=vehicle.make, model=vehicle.model)
                dls_f = dls_f.extra(where=["LOWER(%s) LIKE LOWER(CONCAT('%%', motor_product_discountloading.variant,'%%'))"],
                                    params=[vehicle.variant],
                                    select={
                                        'lovariant': 'LENGTH(motor_product_discountloading.variant)'},
                                    order_by=['-lovariant'])

        if dls_f.count() == 0:
            dls_f = dls_after_acr.filter(
                make=vehicle.make, model=vehicle.model, variant=None)

            if dls_f.count() == 0:
                dls_f = dls_after_acr.filter(make=vehicle.make, variant=None)
                dls_f = dls_f.extra(where=["LOWER(%s) LIKE LOWER(CONCAT('%%', motor_product_discountloading.model,'%%'))"],
                                    params=[vehicle.model],
                                    select={
                                        'lomodel': 'LENGTH(motor_product_discountloading.model)'},
                                    order_by=['-lomodel'])

        if dls_f.count() == 0:
            dls_f = dls_after_acr.filter(
                make=vehicle.make, model=None, variant=None)

            if dls_f.count() == 0:
                dls_f = dls_after_acr.filter(model=None, variant=None)
                dls_f = dls_f.extra(where=["LOWER(%s) LIKE LOWER(CONCAT('%%', motor_product_discountloading.make,'%%'))"],
                                    params=[vehicle.make],
                                    select={
                                        'lomake': 'LENGTH(motor_product_discountloading.make)'},
                                    order_by=['-lomake'])

        if dls_f.count() == 0:
            dls_f = dls_after_acr.filter(make=None, model=None, variant=None)

        dl_make = None
        dl_model = None
        dl_variant = None

        if dls_f.count() == 0:
            discount = 0
        else:
            discount = 0
            if dls_f.count() > 1:
                motor_logger.error(
                    'More than one possible discountings found.')
            discount = dls_f[0].discount
            dl_make = dls_f[0].make
            dl_model = dls_f[0].model
            dl_variant = dls_f[0].variant
        motor_logger.info('Discount for %s - (%s %s %s) of age %s for RTO location %s is %s' %
                          (vehicle, dl_make, dl_model, dl_variant, vehicle_age, rto_insurer.rto_name, discount))
        return discount


class ProposalGenerator(object):

    def __init__(self, configuration):
        self.Configuration = configuration

    def get_proposal_data(self, form_details, transaction):
        quote_parameters = transaction.quote.raw_data
        quote_response = transaction.raw['quote_response']
        # quote_raw_response = transaction.raw['quote_raw_response']

        # All covers
        covers = copy.deepcopy(quote_response['premiumBreakup']['basicCovers'])
        covers.extend(copy.deepcopy(
            quote_response['premiumBreakup']['addonCovers']))
        covers.extend(copy.deepcopy(
            quote_response['premiumBreakup']['discounts']))
        covers.extend(copy.deepcopy(
            quote_response['premiumBreakup']['specialDiscounts']))

        avail_bc = {i['id']: i['premium'] for i in copy.deepcopy(
            quote_response['premiumBreakup']['basicCovers'])}

        avail_sd = {i['id']: i['premium'] for i in copy.deepcopy(
            quote_response['premiumBreakup']['specialDiscounts'])}

        avail_addons = map(lambda i: '%s_%s' % ('addon', i['id']), copy.deepcopy(
            quote_response['premiumBreakup']['addonCovers']))

        is_new_vehicle = (quote_parameters['isNewVehicle'] == '1')

        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(quote_parameters['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")
            past_policy_start_date = past_policy_end_date.replace(year=past_policy_end_date.year-1) + datetime.timedelta(days=1)
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

        new_policy_end_date = new_policy_start_date.replace(
            year=new_policy_start_date.year + 1) - datetime.timedelta(days=1)
        registration_date = datetime.datetime.strptime(
            quote_parameters['registrationDate'], '%d-%m-%Y')
        # manufacturing_year = quote_parameters[
        #     'manufacturingDate'].split('-')[2]

        ncb = ''
        if not is_new_vehicle and quote_parameters['isClaimedLastYear'] == '0':
            ncb = quote_parameters['previousNCB']

        master_rto_code = "-".join(
            quote_parameters['registrationNumber[]'][0:2])

        rto_zone_code = '35' if master_rto_code in RTO_ZONES_A else '36'

        vehicle = Vehicle.objects.get(id=form_details['insurerVehicleId'])
        vehicle_data = vehicle.raw_data.split('|')
        vehicle_body_type = vehicle_data[5]
        vehicle_fuel_type = vehicle_data[7]

        if self.Configuration.VEHICLE_TYPE == 'Twowheeler':
            vehicle_class = 'CLASS_3'
            _discount = '30'
        else:
            vehicle_class = 'CLASS_2'
            _discount = '50'
            if vehicle.cc < 1000 and vehicle.cc >= 750:
                vehicle_class = 'CLASS_2A'
            elif vehicle.cc < 750:
                vehicle_class = 'CLASS_2B'

        if 'voluntaryDeductible' not in avail_sd:
            quote_parameters['voluntaryDeductible'] = 0

        proposal_data = (
            ('LOGIN_ID', 'CFOX1'),
            ('DLR_INV_NO', 'CFOX%s' % transaction.id),
            ('DLR_INV_DT', datetime.datetime.now().strftime('%d-%b-%Y').lower()),
            ('PRODUCT_CODE', self.Configuration.PRODUCT_CODE),
            ('POLICY_TYPE', 'MOT-PLT-001'),
            ('START_DATE', new_policy_start_date.strftime('%d-%b-%Y').lower()),
            ('END_DATE', new_policy_end_date.strftime('%d-%b-%Y').lower()),
            ('INSURED_NAME', form_details['cust_first_name'] + ' ' + form_details['cust_last_name']),
            ('ADDR_01', form_details['add_house_no'] + ' ' + form_details['add_building_name']),
            ('ADDR_02', form_details['add_street_name'] + ' ' + form_details['add_landmark']),
            ('ADDR_03', ''),
            ('CITY', form_details['add_city']),
            ('STATE', form_details['add_state']),
            ('PINCODE', form_details['add_pincode']),
            ('COUNTRY', 'IND'),
            ('EMAIL_ID', form_details['cust_email']),
            ('MOBILE_NO', form_details['cust_phone']),
            ('TEL_NO', ''),
            ('FAX_NO', ''),
            ('ROAD_TRANSPORT_YN', '0'),  # Constant for Private Cars and Twowheelers
            ('INSURED_KYC_VERIFIED', ''),
            ('MOU_ORG_MEM_ID', ''),  # NA
            ('MOU_ORG_MEM_VALI', ''),  # NA
            ('MANUF_VEHICLE_CODE', ''),  # NA
            ('VEHICLE_CODE', vehicle.model_code),
            ('VEHICLE_TYPE_CODE', 'W' if is_new_vehicle else 'P'),
            ('VEHICLE_CLASS_CODE', vehicle_class),
            ('MANUF_CODE', vehicle.make_code),
            ('VEHICLE_MODEL_CODE', vehicle.vehicle_code),
            ('TYPE_OF_BODY_CODE', vehicle_body_type),
            ('VEHICLE_COLOR', 'DMNDWHIT'),  # TODO
            ('VEHICLE_REG_NUMBER', 'NEW-1234' if is_new_vehicle else form_details['vehicle_reg_no']),
            ('FIRST_REG_DATE', datetime.datetime.today().date().strftime(
                '%d-%b-%Y').lower() if is_new_vehicle else registration_date.strftime('%d-%b-%Y').lower()),
            ('ENGINE_NUMBER', form_details['vehicle_engine_no']),
            ('CHASSIS_NUMBER', form_details['vehicle_chassis_no']),
            ('VEH_IDV', quote_response['calculatedAtIDV']),
            ('CUBIC_CAPACITY', int(vehicle.cc)),
            ('THREEWHEELER_YN', '0'),
            ('SEATING_CAPACITY', vehicle.seating_capacity),
            ('VEHICLE_GVW', ''),  # NA
            ('NO_OF_DRIVERS', '1'),
            ('FUEL_TYPE_CODE', vehicle_fuel_type),
            ('RTO_CODE', master_rto_code),
            ('ZONE_CODE', rto_zone_code),
            ('GEO_EXT_CODE', ''),
            ('VOLUNTARY_EXCESS', self.Configuration.VD_MAP.get(int(quote_parameters['voluntaryDeductible']))),
            ('MEMBER_OF_AAI', '1' if quote_parameters['extra_isMemberOfAutoAssociation'] == '1' else '0'),
            ('ANTITHEFT_DEVICE_DESC', 'ANTI' if quote_parameters['extra_isAntiTheftFitted'] == '1' else ''),
            ('NON_ELEC_ACCESS_DESC', 'NONELEC' if int(avail_bc['idvNonElectrical']) > 0 else ''),
            ('NON_ELEC_ACCESS_VALUE', quote_parameters['idvNonElectrical'] if int(avail_bc['idvNonElectrical']) > 0 else '0'),
            ('ELEC_ACCESS_DESC', 'ELEC ACC' if int(avail_bc['idvElectrical']) > 0 else ''),
            ('ELEC_ACCESS_VALUE', quote_parameters['idvElectrical'] if int(avail_bc['idvElectrical']) > 0 else '0'),
            ('SIDE_CAR_ACCESS_DESC', ''),
            ('SIDE_CARS_VALUE', ''),
            ('TRAILER_DESC', ''),  # NA
            ('TRAILER_VALUE', ''),  # NA
            ('ARTI_TRAILER_DESC', ''),  # NA
            ('ARTI_TRAILER_VALUE', ''),  # NA
            ('PREV_YR_ICR', '0' if is_new_vehicle else quote_parameters['isClaimedLastYear']),
            ('NCB_DECL_SUBMIT_YN', '1' if not is_new_vehicle and quote_parameters['isClaimedLastYear'] == '0' else '0'),
            ('LIMITED_TPPD_YN', '0'),
            ('RALLY_COVER_YN', '0'),
            ('RALLY_DAYS', ''),
            ('NIL_DEP_YN', '1' if 'addon_isDepreciationWaiver' in avail_addons else '0'),
            ('CNG_KIT_VALUE', quote_parameters['cngKitValue']),
            ('FIBRE_TANK_VALUE', '0'),
            ('ALT_CAR_BENEFIT', ''),
            ('PERS_EFF_COVER', ''),
            ('NO_OF_PA_OWNER_DRIVER', '1'),
            ('NO_OF_PA_NAMED_PERSONS', '0'),
            ('PA_NAMED_PERSONS_SI', '0'),
            ('NO_OF_PA_UNNAMED_PERSONS', vehicle.seating_capacity if int(quote_parameters['extra_paPassenger']) > 0 else 0),
            ('PA_UNNAMED_PERSONS_SI', int(quote_parameters['extra_paPassenger'])),
            ('NO_OF_PA_UNNAMED_HIRER', ''),
            ('NO_OF_LL_EMPLOYEES', '0'),
            ('NO_OF_LL_PAID_DRIVER', quote_parameters['extra_isLegalLiability']),
            ('NO_OF_LL_SOLDIERS', ''),
            ('OTH_SINGLE_FUEL_CVR', '0'),
            ('IMP_CAR_WO_CUSTOMS_CVR', '0'),
            ('DRIVING_TUITION_EXT_CVR', '0'),
            ('NO_OF_COOLIES', ''),
            ('NO_OF_CONDUCTORS', ''),
            ('NO_OF_CLEANERS', ''),
            ('TOWING_TYPE', ''),
            ('NO_OF_TRAILERS_TOWED', ''),
            ('NO_OF_NFPP_EMPL', ''),
            ('NO_OF_NFPP_OTH_THAN_EMPL', ''),
            ('DLR_PA_NOMINEE_NAME', form_details['nominee_name']),
            ('DLR_PA_NOMINEE_DOB', ''),  # TODO eg. 08-aug-1987 # NOT MAND.
            ('DLR_PA_NOMINEE_RELATION', form_details['nominee_relationship']),
            ('RETN_TO_INVOICE', '1' if 'addon_isInvoiceCover' in avail_addons else '0'),
            ('HYPO_TYPE', '2' if form_details['is_car_financed'] == '1' else ''),
            ('HYPO_COMP_NAME', form_details['car-financier'] if form_details['is_car_financed'] == '1' else ''),
            ('HYPO_COMP_ADDR_01', ''),
            ('HYPO_COMP_ADDR_02', ''),
            ('HYPO_COMP_ADDR_03', ''),
            ('HYPO_COMP_CITY', ''),
            ('HYPO_COMP_STATE', ''),
            ('HYPO_COMP_PINCODE', ''),
            ('PAYMENT_TYPE', 'OT'),
            ('NCB_PERCENTAGE', ncb),
            ('PREV_INSU_COMPANY', '' if is_new_vehicle else
                                  insurer_data.PAST_INSURANCE_ID_LIST[form_details['past_policy_insurer']]),
            ('PREV_POL_NUMBER', '' if is_new_vehicle else form_details['past_policy_number']),
            ('PREV_POL_START_DATE', '' if is_new_vehicle else past_policy_start_date.strftime('%d-%b-%Y').lower()),
            ('PREV_POL_END_DATE', '' if is_new_vehicle else past_policy_end_date.strftime('%d-%b-%Y').lower()),
            ('EXIS_POL_FM_OTHER_INSR', '0'),
            ('IP_ADDRESS', ''),  # NA
            ('MAC_ADDRESS', ''),  # NA
            ('WIN_USER_ID', ''),  # NA
            ('WIN_MACHINE_ID', ''),  # NA
            ('DISCOUNT_PERC', _discount),  # TODO
            ('FLEX_01', ''),  # NA
            ('FLEX_02', ''),  # NA
            ('FLEX_03', ''),  # NA
            ('FLEX_04', ''),  # NA
            ('FLEX_05', ''),  # NA
            ('FLEX_06', ''),  # NA
            ('FLEX_07', ''),  # NA
            ('FLEX_08', ''),  # NA
            ('FLEX_09', ''),  # NA
            ('FLEX_10', ''),  # NA
            ('TP_PREMIUM_OUT', ''),
            ('OD_PREMIUM_OUT', ''),
            ('ANNUAL_PREMIUM_OUT', ''),
            ('NCB_PERCENTAGE_OUT', ''),
            ('NCB_AMOUNT_OUT', ''),
            ('SERVICE_TAX_OUT', ''),
            ('PROPOSAL_NO_OUT', ''),
            ('POLICY_SYS_ID_OUT', ''),
            ('FLEX_01_OUT', ''),  # NA
            ('FLEX_02_OUT', ''),  # NA
            ('FLEX_03_OUT', ''),  # NA
            ('FLEX_04_OUT', ''),  # NA
            ('FLEX_05_OUT', ''),  # NA
            ('ERROR_CODE', ''),  # NA
        )

        return proposal_data

    def save_policy_details(self, data_dictionary, transaction):
        transaction.raw['policy_details'] = data_dictionary
        transaction.premium_paid = transaction.raw['quote_raw_response']['total_premium']
        transaction.save()

        proposal_data = self.get_proposal_data(data_dictionary, transaction)

        client = Client(self.Configuration.API_URL)
        client.set_options(plugins=[suds_logger.set_name(transaction.transaction_id)])
        proposal_details = client.factory.create('GetQuoteMotorETT')

        proposal_dict = dict(proposal_data)
        for key, value in proposal_details:
            proposal_details[key] = proposal_dict[key]
        transaction.proposal_request = proposal_details
        transaction.save()
        response = client.service.GetQuoteMotor(proposal_details)
        transaction.proposal_response = response
        transaction.save()
        error_response = str(response)
        if response.ANNUAL_PREMIUM != 'NULL':
            transaction.proposal_amount = response.ANNUAL_PREMIUM
            transaction.save()
            if abs(float(response.ANNUAL_PREMIUM) - transaction.premium_paid) <= PREMIUM_DIFFERENCE_ALLOWED:
                transaction.proposal_number = response.PROPOSAL_NO_OUT
                transaction.reference_id = response.POLICY_SYS_ID
                transaction.save()
                return True

            else:
                error_response = 'Premium mismatch: %s %s' % (transaction.premium_paid, str(response))
                motor_logger.info(error_response)
                log_error(motor_logger)
        transaction.status = 'FAILED'
        transaction.save()

        # Proposal failure response mail
        mail_utils.send_mail_for_proposal_failure(transaction, str(error_response))
        return False


class PolicyGenerator(object):

    def __init__(self, configuration):
        self.Configuration = configuration

    def get_policy_data(self, transaction):
        policy_data = (
            ('PROPOSAL_NO', transaction.proposal_number),
            ('MODE_OF_PAY', PAYMENT_MODE),
            ('CHEQUE_TYPE', ''),
            ('CHEQUE_NO', ''),
            ('CHEQUE_DT', ''),
            ('CHEQUE_BANK_NAME', ''),
            ('CHEQUE_BRANCH', ''),
            ('CHEQUE_AMT', transaction.premium_paid),
            ('CHEQUE_ISSUED_BY', ''),
            ('PAYINSLIP_NO', ''),
            ('PAYINSLIP_DT', ''),
            ('UNIQUE_REF_NO', transaction.payment_token),
            ('FLEX_01', ''),
            ('FLEX_02', ''),
            ('FLEX_03', ''),
            ('FLEX_04', ''),
            ('FLEX_05', ''),
            ('FLEX_06', ''),
            ('FLEX_07', ''),
            ('FLEX_08', ''),
            ('FLEX_09', ''),
            ('FLEX_10', ''),
        )
        return policy_data

    @app.task(filter=task_method, default_retry_delay=5 * 60, max_retries=3)
    def get_policy(self, transaction_id):
        pnum_success = self.get_policy_number(transaction_id)
        if pnum_success:
            self.get_policy_pdf.apply_async([transaction_id])
        else:
            self.mail_policy_issue(transaction_id)

    @app.task(filter=task_method, default_retry_delay=15 * 60, max_retries=3)
    def get_policy_pdf(self, transaction_id):
        transaction = Transaction.objects.get(transaction_id=transaction_id)

        class MyFTP(FTP):
            def __init__(self, host, port, user, password, file_dir, policy_number):
                FTP.__init__(self)
                self.host = host
                self.port = port
                self.user = user
                self.password = password
                self.file_dir = file_dir
                self.policy_number = policy_number
                self.imfile = StringIO.StringIO()

            def makepasv(self):
                if self.af == socket.AF_INET:
                    host, port = parse227(self.sendcmd('PASV'))
                else:
                    host, port = parse229(self.sendcmd('EPSV'), self.sock.getpeername())
                # We tweak this so we don't use the internal ip returned by the remote server.
                # old: `return host, port`
                return self.host, port

            def callback(self, data):
                self.imfile.write(data)

            def get_file_name(self):
                files = self.nlst()
                pnr = self.policy_number.replace('/', '_') + '_'
                return filter(lambda _file: pnr in _file, files)[0]

            def read(self):
                self.connect(self.host, self.port)
                self.login(self.user, self.password)
                self.cwd(self.file_dir)
                self.set_pasv(True)
                _file_name = self.get_file_name()
                self.retrbinary(
                    "RETR %s" % _file_name,
                    self.callback
                )
                return self.imfile.getvalue()

            def close_imf(self):
                self.imfile.close()

        policy_number = transaction.policy_number
        _file = MyFTP(
            POLICY_FTP_HOST,
            POLICY_FTP_PORT,
            FTP_USER,
            FTP_PASSWORD,
            POLICY_DIR,
            policy_number
        )
        try:
            pdf_content = _file.read()
        except Exception as e:
            error_msg = 'Policy document download failed: %s' % (transaction.id)
            motor_logger.info(error_msg)
            try:
                self.get_policy_pdf.retry(args=[transaction_id])
            except MaxRetriesExceededError:
                self.mail_policy_issue(transaction_id, pdf_issue=True)
        else:
            transaction.update_policy(pdf_content)
        finally:
            _file.close_imf()

    def get_policy_number(self, transaction_id):
        transaction = Transaction.objects.get(transaction_id=transaction_id)
        policy_data = self.get_policy_data(transaction)

        client = Client(self.Configuration.API_URL)
        client.set_options(plugins=[suds_logger.set_name(transaction.transaction_id)])
        policy_details = client.factory.create('CreatePolicyETT')

        policy_dict = dict(policy_data)
        for key, value in policy_details:
            policy_details[key] = policy_dict[key]
        transaction.policy_request = policy_details
        transaction.save()
        response = client.service.CreatePolicy(policy_details)
        transaction.policy_response = response
        transaction.save()
        error_response = str(response)
        if response.POLICY_NO_OUT:
            transaction.policy_number = response.POLICY_NO_OUT
            transaction.save()
            return True

        else:
            try:
                self.get_policy.retry(args=[transaction_id])
            except MaxRetriesExceededError:
                self.mail_policy_issue(transaction_id)
        error_response = 'Policy creation failed: %s %s' % (transaction.id, str(response))
        motor_logger.error(error_response)
        return False

    def mail_policy_issue(self, transaction_id, pdf_issue=False):
        transaction = Transaction.objects.get(transaction_id=transaction_id)
        proposal_number = transaction.proposal_number
        customer_name = '%s %s' % (transaction.proposer.first_name, transaction.proposer.last_name)
        payment_id = transaction.payment_token
        if pdf_issue:
            policy_number = transaction.policy_number
            subject = 'Oriental policy PDF not received - %s' % policy_number
            body = '''
            Transaction ID  :   %s
            Policy Number   :   %s
            Customer Name   :   %s
            Proposal Number :   %s
            Payment ID      :   %s
            ''' % (transaction_id, policy_number, customer_name, proposal_number, payment_id)
        else:
            subject = 'Oriental policy number not received - %s' % proposal_number
            body = '''
            Transaction ID  :   %s
            Customer Name   :   %s
            Proposal Number :   %s
            Payment ID      :   %s
            ''' % (transaction_id, customer_name, proposal_number, payment_id)

        to_mail = POLICY_GENERATION_ISSUE_MAILIDS
        email = EmailMessage(
            subject=subject,
            body=body,
            from_email="rupy@coverfox.com",
            to=to_mail
        )
        email.send()


class IntegrationUtils(object):

    PremiumCalculator = None
    ProposalGenerator = None

    def __init__(self, vehicle_type):
        configuration = Configuration(vehicle_type)
        self.Configuration = configuration
        self.PremiumCalculator = PremiumCalculator(configuration)
        self.ProposalGenerator = ProposalGenerator(configuration)
        self.PolicyGenerator = PolicyGenerator(configuration)
        # self.no_custom_vehicles = True

    def convert_premium_request_data_to_required_dictionary(self, vehicle_id, request_data):
        data = copy.deepcopy(request_data)
        data['masterVehicleId'] = data['vehicleId']
        data['vehicleId'] = vehicle_id
        return data

    def convert_proposal_request_data_to_required_dictionary(self, vehicle,
                                                             request_data, quote_parameters,
                                                             quote_response, quote_custom_response):
        data = copy.deepcopy(request_data)

        if quote_parameters['isNewVehicle'] == '1':
            data['vehicle_reg_no'] = 'NEW---0001'

        data['insurerVehicleId'] = vehicle.id
        return data

    def update_transaction_details(self, transaction, data):
        is_new_vehicle = data['isNewVehicle'] == '1'
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                data['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

        new_policy_end_date = new_policy_start_date.replace(
            year=new_policy_start_date.year + 1) - datetime.timedelta(days=1)

        transaction.policy_start_date = new_policy_start_date
        transaction.policy_end_date = new_policy_end_date
        transaction.save()

    def get_buy_form_details(self, quote_parameters, transaction):
        split_registration_no = quote_parameters.get('registrationNumber[]')

        is_new_vehicle = True if quote_parameters[
            'isNewVehicle'] == '1' else False
        if is_new_vehicle:
            registration_number = 'NEW---0001'
        else:
            registration_number = '-'.join(split_registration_no)

        master_rto_code = "%s-%s" % (
            split_registration_no[0], split_registration_no[1])

        data = {
            'registration_no': registration_number,
            'rto_location_info': master_rto_code,
            'insurers_list': insurer_utils.MASTER_INSURER_LIST,
            'state_list': insurer_data.STATE_CODE_LIST,
            'disallowed_insurers': insurer_data.DISALLOWED_PAST_INSURERS,
            'relationship_list': insurer_data.NOMINEE_RELATIONSHIP_LIST,
            'is_cities_fetch': True,
        }
        return data

    def get_payment_data(self, transaction):
        pg_in_data = {
            'transaction': transaction,
            'reference_id': transaction.proposal_number,
            'amount': transaction.proposal_amount,
            'email': transaction.proposer.email,
            'phone': transaction.proposer.mobile
        }
        pg_config = Billdesk.get_config(INSURER_SLUG)
        billdesk = Billdesk(pg_in_data, pg_config)
        payment_data, payment_url = billdesk.get_request_data()
        suds_logger.set_name(transaction.transaction_id).log(
                json.dumps({'url': payment_url, 'data': payment_data}))
        transaction.payment_request = {'url': payment_url, 'data': payment_data}
        transaction.save()
        return payment_data, payment_url

    def post_payment(self, payment_response, transaction):

        motor_logger.info(payment_response)
        suds_logger.set_name(transaction.transaction_id).log(json.dumps(payment_response))

        transaction.payment_response = payment_response
        transaction.save()

        is_redirect = True

        if transaction.vehicle_type == 'Twowheeler':
            redirect_url = '/motor/twowheeler/payment/failure/%s/' % transaction.transaction_id
        else:
            redirect_url = '/motor/fourwheeler/payment/failure/%s/' % transaction.transaction_id

        if payment_response['status'] == '0300':
            if transaction.vehicle_type == 'Twowheeler':
                redirect_url = '/motor/twowheeler/payment/success/%s/' % transaction.transaction_id
            else:
                redirect_url = '/motor/fourwheeler/payment/success/%s/' % transaction.transaction_id
            transaction.payment_token = payment_response['payment_id']
            transaction.premium_paid = payment_response['payment_amount'].lstrip('0')
            transaction.payment_on = datetime.datetime.now()
            transaction.payment_done = True
            transaction.mark_complete()
            self.PolicyGenerator.get_policy.delay(transaction.transaction_id)
        else:
            transaction.status = 'FAILED'
            transaction.payment_done = False
            transaction.raw['error'] = payment_response['status']
            transaction.save()
        return is_redirect, redirect_url, '', {}

    def get_dependent_addons(self, parameters):
        return {'type': 'dynamic'}

    def get_city_list(self, state):
        return insurer_data.STATE_CODE_TO_CITIES.get(state, [])

    def get_transaction_from_id(self, _id):
        return Transaction.objects.get(transaction_id=_id)

    def get_custom_data(self, datatype):
        datamap = {
            'insurers': insurer_data.PAST_INSURANCE_ID_LIST,
            'relationships': insurer_data.NOMINEE_RELATIONSHIP_LIST,
            'states': insurer_data.STATE_CODE_LIST,
        }
        return datamap.get(datatype, None)

    def disable_cache_for_request(self, request_data):
        if request_data.get('discountCode', None):
            return True
        return False
