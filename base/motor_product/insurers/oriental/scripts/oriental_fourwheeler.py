INSURER_SLUG = 'oriental'
csv_parameters = {
    'MAKE_CODE': 0,
    'MANF_DESC': 1,
    'MANF_CODE': 2,
    'MAKE_DESC': 3,
    'VEH_CODE': 4,
    'VEH_CC': 5,
    'VEH_SEAT_CAP': 6,
    'FUEL_DESC': 7,
    'Master': 8
}
parameters = {
    'vehicle_code': 0,
    'make': 1,
    'make_code': 2,
    'model': 3,
    'model_code': 4,
    'cc': 5,
    'seating_capacity': 6,
    'fuel_type': 7,
    'master': 8
}
