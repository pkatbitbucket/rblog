import copy
import math
import httplib2
import json
import datetime
from lxml import etree
from utils import motor_logger
from dateutil import relativedelta
import putils
from django.conf import settings
from motor_product.models import Insurer, Vehicle, Transaction, IntegrationStatistics
from motor_product import parser_utils, mail_utils, logger_utils
from motor_product.insurers import insurer_utils
from motor_product.insurers.reliance import custom_dictionaries as custom_data


if settings.PRODUCTION:
    # Production APIs
    COVERAGE_API = {
        'url': 'http://220.226.197.206/API/Service/CoverageDetailsForMotor',
        'request_method': 'POST',
        'headers': {'Content-Type': 'application/xml'}
    }
    PREMIUM_API = {
        'url': 'http://220.226.197.206/API/Service/PremiumCalulationForMotor',
        'request_method': 'POST',
        'headers': {'Content-Type': 'application/xml'}
    }
    PROPOSAL_API = {
        'url': 'http://220.226.197.206/API/Service/ProposalCreationForMotor',
        'request_method': 'POST',
        'headers': {'Content-Type': 'application/xml'}
    }
    POLICY_API = {
        'url': 'http://220.226.197.206/API/Service/GeneratePolicyschedule',
        'request_method': 'GET',
        'headers': {'Content-Type': 'application/pdf'}
    }
    PAYMENT_URL = 'http://220.226.197.206/PaymentIntegration/PaymentIntegration'
    # Production credentials
    SOURCE_SYSTEM_ID = 'CF000013B02'
    AUTH_TOKEN = 'COVERFOX181215'
    USER_ID = 'CF000013B02'

else:
    # UAT APIs
    COVERAGE_API = {
        'url': 'http://220.226.197.206:91/API/Service/CoverageDetailsForMotor',
        'request_method': 'POST',
        'headers': {'Content-Type': 'application/xml'}
    }
    PREMIUM_API = {
        'url': 'http://220.226.197.206:91/API/Service/PremiumCalulationForMotor',
        'request_method': 'POST',
        'headers': {'Content-Type': 'application/xml'}
    }
    PROPOSAL_API = {
        'url': 'http://220.226.197.206:91/API/Service/ProposalCreationForMotor',
        'request_method': 'POST',
        'headers': {'Content-Type': 'application/xml'}
    }
    POLICY_API = {
        'url': 'http://220.226.197.206:91/API/Service/GeneratePolicyschedule',
        'request_method': 'GET',
        'headers': {'Content-Type': 'application/pdf'}
    }
    PAYMENT_URL = 'http://220.226.197.206:91/PaymentIntegration/PaymentIntegration'
    # UAT credentials
    SOURCE_SYSTEM_ID = '100002'
    AUTH_TOKEN = 'Pass@123'
    USER_ID = '100002'

PAYMENT_TYPE = '1'
BRANCH_CODE = '9202'

PRODUCT_CODE = {
    'Private Car': '2311',
    'Twowheeler': '2312'
}

PAYMENT_URL_TAG = {
    'Private Car': 'fourwheeler',
    'Twowheeler': 'twowheeler'
}

INSURER_SLUG = 'reliance'
try:
    INSURER = Insurer.objects.get(slug=INSURER_SLUG)
except:
    INSURER = None

suds_logger = logger_utils.getLogger(INSURER_SLUG)


def get_response(name, content, api_data):
    motor_logger.info(
        '\n==========%s=========\nRELIANCE REQUEST SENT:\n%s\n' % (name, content))

    response_header, response_content = httplib2.Http().request(
        api_data['url'],
        method=api_data['request_method'],
        body=content,
        headers=api_data['headers']
    )

    parsed_xml_response = etree.fromstring(response_content)
    parsed_xml_response_str = etree.tostring(parsed_xml_response)
    response_dict = parser_utils.complex_etree_to_dict(parsed_xml_response)
    motor_logger.info('\nRELIANCE RESPONSE :\n%s\n' %
                      str(parsed_xml_response_str))
    return response_dict


class Configuration(object):

    def __init__(self, vehicle_type):
        self.VEHICLE_TYPE = vehicle_type
        self.PRODUCT_CODE = PRODUCT_CODE[vehicle_type]
        self.URL_TAG = PAYMENT_URL_TAG[vehicle_type]


class CoverageCalculator(object):

    def __init__(self, integrationUtils, configuration):
        self.parent = integrationUtils
        self.Configuration = configuration
        self.api_data = COVERAGE_API

    def get_coverage(self, data):
        xmldata = self._get_coverage_data(data)
        rxml = insurer_utils.convert_tuple_to_xmltree(xmldata)
        request_xml = etree.tostring(rxml)
        resp = get_response(name=data[
            'vehicleId'], content=request_xml, api_data=self.api_data)

        return resp

    def _get_coverage_data(self, data):
        coverage_data = ("PolicyDetails", (
                ("SourceSystemID", SOURCE_SYSTEM_ID),
                ("AuthToken", AUTH_TOKEN),
                ("UserID", USER_ID),
                ("ProductCode", self.Configuration.PRODUCT_CODE),
            ) + self._get_coverage_details(data)
        )
        return coverage_data

    def _get_coverage_details(self, data):
        is_new_vehicle = (data['isNewVehicle'] == '1')
        rto_code = '%s-%02d' % (data['registrationNumber[]'][0], int(
            data['registrationNumber[]'][1]))
        rto_zone = 'A' if rto_code in custom_data.ZONE_A_RTO_LIST else 'B'
        mfg_date = datetime.datetime.strptime(
            data['manufacturingDate'], '%d-%m-%Y')
        registration_date = datetime.datetime.strptime(
            data['registrationDate'], '%d-%m-%Y')
        vehicle = Vehicle.objects.filter(insurer=INSURER).get(
            id=data['vehicleId'])
        try:
            ex_showroom_price = vehicle.raw_data.split('|')[-1].split(
                ',')[custom_data.STATE_TO_GROUP_ID[str(
                    custom_data.RTO_CODE_TO_STATE_ID[str(rto_code)])]-1]
        except IndexError:
            motor_logger.info('ExShowroomPrice not available \
                for vehicle with id %d.' % vehicle.id)
            ex_showroom_price = 0
        fuel = custom_data.FUEL_TYPE[vehicle.fuel_type]
        ncb_applicable = 'false'
        ncb = '0'
        if is_new_vehicle:
            registration_number = rto_code + '-ZZ-0001'
            new_policy_start_date = datetime.datetime.strptime(
                data['newPolicyStartDate'], "%d-%m-%Y")
        else:
            registration_number = '-'.join(data['registrationNumber[]'])
            if data['isClaimedLastYear'] == '0':
                ncb_applicable = 'true'
                ncb = custom_data.NCB_PERCENT_TO_NCB[data['previousNCB']]
            past_policy_end_date = datetime.datetime.strptime(
                data['pastPolicyExpiryDate'], "%d-%m-%Y")
            past_policy_start_date = past_policy_end_date - \
                relativedelta.relativedelta(years=1) + \
                datetime.timedelta(days=1)
            # Expired Policy Two-Wheeler
            if (
                    past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now() and
                    self.parent.Configuration.VEHICLE_TYPE == 'Twowheeler'
            ):
                new_policy_start_date = datetime.datetime.now() + \
                                        datetime.timedelta(days=3)
            else:
                new_policy_start_date = past_policy_end_date + \
                                        datetime.timedelta(days=1)

        coverage_details = (
            ('ClientDetails', (
                ('ClientType', '0'),)),
            ('Cover', ''),
            ('Risk', (
                ('Zone', rto_zone),
                ('ManufactureYear', mfg_date.year),
                ('DateOfPurchase', registration_date.strftime('%d/%m/%Y')),
                ('Rto_RegionCode', rto_code),
                ('StateOfRegistrationID', custom_data.RTO_CODE_TO_STATE_ID[
                    rto_code]),
                ('CubicCapacity', vehicle.cc),
                ('RTOLocationID', custom_data.RTO_CODE_TO_LOCATION_ID[
                    rto_code]),
                ('VehicleModelID', vehicle.model_code),
                ('ExShowroomPrice', ex_showroom_price),
                ('VehicleMakeID', vehicle.make_code),
                ('ManufactureMonth', "%02d" % mfg_date.month),
                ('VehicleVariant', vehicle.variant))),
            ('Vehicle', (
                ('TypeOfFuel', fuel),
                ('ISNewVehicle', 'true' if is_new_vehicle else 'false'),
                ('SeatingCapacity', vehicle.seating_capacity),
                ('Registration_Number', registration_number))),
            ('NCBEligibility', (
                ('NCBEligibilityCriteria', '2'),
                ('NCBReservingLetter', ''),
                ('PreviousNCB', ncb))),
            ('PreviousInsuranceDetails', (
                ('PrevYearPolicyStartDate', past_policy_start_date.strftime(
                    '%d/%m/%Y') if not is_new_vehicle else ''),
                ('IsClaimedLastYear', 'false' if data[
                    'isClaimedLastYear'] == '0' else 'true'),
                ('PrevYearNCB', ncb),
                ('PrevYearPolicyEndDate', past_policy_end_date.strftime(
                    '%d/%m/%Y') if not is_new_vehicle else ''),
                ('IsNCBApplicable', ncb_applicable))),
            ('Policy', (
                ('productcode', self.Configuration.PRODUCT_CODE),
                ('BusinessType', '1' if is_new_vehicle else '5'),
                ('Cover_From', new_policy_start_date.strftime(
                    '%d/%m/%Y')),
                ('OtherSystemName', '1'),
                ('Branch_Code', BRANCH_CODE)
            ))
        )

        return coverage_details


class PremiumCalculator(object):

    def __init__(self, integrationUtils, configuration):
        self.parent = integrationUtils
        self.Configuration = configuration
        self.api_data = PREMIUM_API

    def get_premium(self, data):
        if data['isUsedVehicle'] == '1':
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_USED_VEHICLE_RESTRICTED}

        if data['isNewVehicle'] == '0':
            past_policy_end_date = datetime.datetime.strptime(data['pastPolicyExpiryDate'], "%d-%m-%Y")
            if (
                self.parent.Configuration.VEHICLE_TYPE == 'Private Car' and
                past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now()
            ):
                motor_logger.info('Expired policy not allowed for this insurer %s.' % INSURER_SLUG)
                return {'success': False, 'error_code': IntegrationStatistics.INSURER_EXPIRED_POLICY_RESTRICTED}

        registration_date = datetime.datetime.strptime(
                data['registrationDate'], '%d-%m-%Y')

        if data['isNewVehicle'] == '0':
            past_policy_end_date = datetime.datetime.strptime(
                data['pastPolicyExpiryDate'], "%d-%m-%Y")

            # Expired Policy
            if (
                    past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now() and
                    self.parent.Configuration.VEHICLE_TYPE == 'Twowheeler'
            ):
                policy_start_date = datetime.datetime.now() + \
                                    datetime.timedelta(days=3)
            else:
                policy_start_date = past_policy_end_date + \
                                    datetime.timedelta(days=1)

            vehicle_age = relativedelta.relativedelta(
                policy_start_date, registration_date).years

            if vehicle_age > 10:
                return {'success': False, 'error_code': IntegrationStatistics.INSURER_AGE_RESTRICTED}

        xmldata = self._get_premium_data(data)
        rxml = insurer_utils.convert_tuple_to_xmltree(xmldata)

        if data['idv'] in ['0', '0.0', 0, 0.0]:
            etree.strip_elements(rxml, "IDV")

        request_xml = etree.tostring(rxml)
        resp = get_response(name=data[
            'vehicleId'], content=request_xml, api_data=self.api_data)

        response_dict = resp['MotorPolicy']

        try:
            idv = int(float(response_dict['IDV']))
        except TypeError:
            idv = 0

        if data.get('req_idv', None):
            if response_dict['MinIDV'] and response_dict['MaxIDV']:
                min_idv = int(float(response_dict['MinIDV']))
                max_idv = int(float(response_dict['MaxIDV']))
            else:
                min_idv = int(math.ceil(0.9 * idv))
                max_idv = int(math.floor(1.1 * idv))

            request_idv = float(data['req_idv'])

            if request_idv < float(min_idv):
                modified_idv = min_idv
            elif request_idv > float(max_idv):
                modified_idv = max_idv
            else:
                modified_idv = request_idv

            data['idv'] = modified_idv
            data['min_idv'] = min_idv
            data['max_idv'] = max_idv
            del data['req_idv']

            return self.get_premium(data)

        vehicle = Vehicle.objects.filter(
            insurer__slug=INSURER_SLUG).get(id=data['vehicleId'])
        return self.parent.convert_premium_response(
            response_dict, data, vehicle)

    def _get_premium_data(self, data):
        premium_data = ("PolicyDetails", (
                ("SourceSystemID", SOURCE_SYSTEM_ID),
                ("AuthToken", AUTH_TOKEN),
                ("UserID", USER_ID),
                ("ProductCode", self.Configuration.PRODUCT_CODE),
            ) + self._get_contract_details(data)
        )
        return premium_data

    def _get_contract_details(self, data):

        is_new_vehicle = (data['isNewVehicle'] == '1')
        rto_code = '%s-%02d' % (data['registrationNumber[]'][0], int(
            data['registrationNumber[]'][1]))
        vehicle = Vehicle.objects.filter(
            insurer=INSURER).get(id=data['vehicleId'])
        registration_date = datetime.datetime.strptime(
            data['registrationDate'], '%d-%m-%Y')
        mfg_date = datetime.datetime.strptime(
            data['manufacturingDate'], '%d-%m-%Y')
        ncb_applicable = 'false'
        ncb = '0'
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                data['newPolicyStartDate'], "%d-%m-%Y")
            new_policy_end_date = new_policy_start_date + \
                relativedelta.relativedelta(years=1) - \
                datetime.timedelta(days=1)
            registration_number = rto_code + '-ZZ-0001'
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data['pastPolicyExpiryDate'], "%d-%m-%Y")
            past_policy_start_date = past_policy_end_date - \
                relativedelta.relativedelta(years=1) + \
                datetime.timedelta(days=1)

            # Expired Policy Two-Wheeler
            if (
                    past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now() and
                    self.parent.Configuration.VEHICLE_TYPE == 'Twowheeler'
            ):
                new_policy_start_date = datetime.datetime.now() + \
                                        datetime.timedelta(days=3)
            else:
                new_policy_start_date = past_policy_end_date + \
                                        datetime.timedelta(days=1)

            new_policy_end_date = new_policy_start_date + \
                relativedelta.relativedelta(years=1) - \
                datetime.timedelta(days=1)
            registration_number = '-'.join(data['registrationNumber[]'])
            if data['isClaimedLastYear'] == '0':
                ncb_applicable = 'true'
                ncb = custom_data.NCB_PERCENT_TO_NCB[data['previousNCB']]

        fuel = custom_data.FUEL_TYPE[vehicle.fuel_type]

        try:
            ex_showroom_price = vehicle.raw_data.split('|')[-1].split(',')[
                custom_data.STATE_TO_GROUP_ID[
                    custom_data.RTO_CODE_TO_STATE_ID[rto_code]]-1]
        except IndexError:
            motor_logger.info(
                'ExShowroomPrice not'
                'available for vehicle with id %d.' % vehicle.id)
            ex_showroom_price = 0

        except KeyError:
            motor_logger.info(
                'Reliance quotes are not'
                'available for %s.' % rto_code)
            return ()

        rto_zone = 'A' if rto_code in custom_data.ZONE_A_RTO_LIST else 'B'

        if not (data['idv'] in ['0', '0.0', 0, 0.0]):
            coverage_response = IntegrationUtils(self.Configuration.VEHICLE_TYPE).CoverageCalculator.get_coverage(data)
            coverage_response = coverage_response['ListCovers'][
                'LstAddonCovers']['AddonCovers']

            cov_resp = {}
            for node in coverage_response:
                cov_resp[node['CoverageName']] = node

            try:
                depreciation_rate = cov_resp['Nil Depreciation']['rate']
            except KeyError:
                depreciation_rate = 0
                data['addon_isDepreciationWaiver'] = '0'

            try:
                ncb_retention_rate = cov_resp['NCB Retention']['rate']
            except KeyError:
                ncb_retention_rate = 0
                data['addon_isNcbProtection'] = '0'

        else:
            depreciation_rate = 1.1
            ncb_retention_rate = 1150

        contract_details = (
            ('ClientDetails', (
                ('ClientType', '0'),)),
            ('Risk', (
                ('Zone', rto_zone),
                ('IDV', data['idv']),
                ('ManufactureYear', mfg_date.year),
                ('DateOfPurchase', registration_date.strftime(
                    '%d/%m/%Y')),
                ('Rto_RegionCode', rto_code),
                ('StateOfRegistrationID',
                    custom_data.RTO_CODE_TO_STATE_ID[rto_code]),
                ('CubicCapacity', vehicle.cc),
                ('RTOLocationID',
                    custom_data.RTO_CODE_TO_LOCATION_ID[
                        rto_code]),
                ('VehicleModelID', vehicle.model_code),
                ('ExShowroomPrice', ex_showroom_price),
                ('VehicleMakeID', vehicle.make_code),
                ('ManufactureMonth', "%02d" % mfg_date.month),
                ('VehicleVariant', vehicle.variant))),
            ('Vehicle', (
                ('TypeOfFuel', fuel),
                ('SeatingCapacity', vehicle.seating_capacity),
                ('Registration_date', registration_date.strftime(
                    '%d/%m/%Y')),
                ('Registration_Number', registration_number))),
            ('Cover', (
                ('PAToNamedPassenger', (
                    ('PAToNamedPassenger', (
                        ('NoOfItems', ''),
                        ('PassengerName', ''),
                        ('NomineeDOB', ''),
                        ('NomineeRelationship', ''),
                        ('PackageName', ''),
                        ('OtherRelation', ''),
                        ('IsMandatory', 'false'),
                        ('IsChecked', 'false'),
                        ('SumInsured', ''),
                        ('NomineeAddress', ''),
                        ('AppointeeName', ''),
                        ('NomineeName', ''))),
                    )),
                ('IsBasicLiability', 'true'),
                ('IsLLToPersonsEmployedInOperations_CleanerConductorCoolieCovered',
                    'false'),
                ('IsDetariffRateForOverturning', 'false'),
                ('IsCoveredForDamagedPortion', 'false'),
                ('IsLossOfAccessoriesCovered', 'false'),
                ('RegistrationCost', (
                    ('RegistrationCost', (
                        ('SumInsured', '0'),
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('IsUseOfVehiclesConfined', 'false'),
                ('IsNCBRetention', 'false' if data[
                    'addon_isNcbProtection'] == '0' else 'true'),
                ('IsBiFuelKit', 'false' if data[
                    'isCNGFitted'] == '0' else 'true'),
                ('IsLLUnderWCActForCarriageOfMoreThanSixEmpCovered',
                    'false'),
                ('AdditionalTowingCharge', '0'),
                ('IsTPPDLiabilityRestricted', 'false'),
                ('UnnamedPassengersSI', '0' if data[
                    'extra_paPassenger'] == '0' else data[
                    'extra_paPassenger']),
                ('AutomobileAssociationNo', '' if data[
                    'extra_isMemberOfAutoAssociation'] == '0'
                    else data['extra_isMemberOfAutoAssociation']),
                ('NoOfLiabilityCoveredEmployees', '0'),
                ('IsLiabilitytoCleaner', 'false'),
                ('IsFibreGlassFuelTankFitted', 'false'),
                ('NoOfUnnamedPassenegersCovered', '0' if data[
                    'extra_paPassenger'] == '0'
                    else vehicle.seating_capacity),
                ('IsVoluntaryDeductableOpted', 'false' if data[
                    'voluntaryDeductible'] == '0' else 'true'),
                ('IsBasicODCoverage', 'true'),
                ('AddOnCoverTowingCharge', '0'),
                ('IsLiabilityToEmployeeCovered', 'false'),
                ('IsLiabilityToPaidDriverCovered', 'false' if data[
                    'extra_isLegalLiability'] == '0' else 'true'),
                ('IsLegalLiabilityToNonFarePayingPassengersCovered',
                    'false'),
                ('NoOfLegalLiabilityCoveredCleaners', '0'),
                ('IsSpeciallyDesignedForHandicapped', 'false'),
                ('ElectricItems', (
                    ('ElectricalItems', (
                        ('SumInsured', '0' if data[
                            'idvElectrical'] == '0' else data[
                            'idvElectrical']),
                        ('ElectricalAccessorySlNo', ''),
                        ('MakeModel', ''),
                        ('PolicyId', ''),
                        ('ElectricalItemsID', ''),
                        ('SerialNo', ''),
                        ('Description', ''),
                        ('ElectricPremium', ''))),
                    )),
                ('InsurancePremium', (
                    ('InsurancePremium', (
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('PAToUnNamedPassenger', (
                    ('PAToUnNamedPassenger', (
                        ('SumInsured', '0' if data[
                            'extra_paPassenger'] == '0' else data[
                            'extra_paPassenger']),
                        ('IsChecked', 'false'),
                        ('NoOfItems', '' if data[
                            'extra_paPassenger'] == '0'
                            else vehicle.seating_capacity),
                        ('PolicyCoverID', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('PAToConductor', (
                    ('PAToConductor', (
                        ('SumInsured', ''),
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('PACoverToNamedPassengerSI', '0'),
                ('DrivingTuitionCoverage', (
                    ('DrivingTuitionCoverage', (
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('ISLegalLiabilityToDefenceOfficialDriverCovered',
                    'false'),
                ('ElectricalItemsTotalSI', '0' if data[
                    'idvElectrical'] == '0' else data[
                    'idvElectrical']),
                ('NFPPExcludingEmployees', (
                    ('NFPPExcludingEmployees', (
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('IsCoverageoFTyreBumps', 'false'),
                ('TrailerDetails', (
                    ('TrailerInfo', (
                        ('ChassisNumber', ''),
                        ('MakeandModel', ''),
                        ('IDV', '1'),
                        ('SerialNumber', ''),
                        ('Registration_No', ''),
                        ('ManufactureYear', ''))),
                    )),
                ('VoluntaryDeductableAmount', data[
                    'voluntaryDeductible']),
                ('PAToDriverSI', '0'),
                ('EMIprotectionCover', ''),
                ('NoOfLLUnderWCAct', '1'),
                ('SpeciallyDesignedforChallengedPerson', (
                    ('SpeciallyDesignedforChallengedPerson', (
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('IsAccidentToPassengerCovered', 'false' if data[
                    'extra_paPassenger'] == '0' else 'true'),
                ('IsTrailerAttached', 'false'),
                ('FibreGlassFuelTank', (
                    ('FibreGlassFuelTank', (
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('IsNonElectricalItemFitted', 'false' if data[
                    'idvNonElectrical'] == '0' else 'true'),
                ('IsWorkmenCompensationExcludingDriver', 'false'),
                ('NumberOfPACoveredPaidDrivers', '0'),
                ('LiabilitytoCoolie', (
                    ('LiabilitytoCoolie', (
                        ('IsChecked', 'false'),
                        ('NoOfItems', '0'),
                        ('IsMandatory', 'false'))),
                    )),
                ('IsRacingCovered', 'false'),
                ('IsAdditionalTowingCover', 'false'),
                ('IsVehicleMadeInIndia', 'true'),
                ('AutomobileAssociationName', ''),
                ('IsLegalLiabilityToCleanerCovered', 'false'),
                ('BifuelKit', (
                    ('BifuelKit', (
                        ('IsChecked', 'false'),
                        ('SumInsured', '0' if data[
                            'cngKitValue'] == '0' else data[
                            'cngKitValue']),
                        ('NoOfItems', ''),
                        ('PolicyCoverDetailsID', ''),
                        ('PolicyCoverID', ''),
                        ('PackageName', ''),
                        ('Fueltype', ''),
                        ('IsMandatory', 'false'),
                        ('ISLpgCng', 'true' if data[
                            'cngKitValue'] == '0' else 'false'))),
                    )),
                ('PAToCleanerSI', '0'),
                ('VoluntaryDeductible', (
                    ('VoluntaryDeductible', (
                        ('SumInsured', data[
                            'voluntaryDeductible']),
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('PolicyCoverID', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('IsLLToPersonsEmployedInOperations_PaidDriverCovered',
                    'false'),
                ('IsNilDepreciation', 'false' if data[
                    'addon_isDepreciationWaiver'] == '0' else 'true'),
                ('IsConfinedToOwnPremisesCovered', 'false'),
                ('UseOfVehiclesConfined', (
                    ('UseOfVehiclesConfined', (
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('IsPAToUnnamedPassengerCovered', 'false' if data[
                    'extra_paPassenger'] == '0' else 'true'),
                ('RoadTax', (
                    ('RoadTax', (
                        ('IsChecked', 'false'),
                        ('SumInsured', '0'),
                        ('NoOfItems', ''),
                        ('PolicyCoverID', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('IsImportedVehicleCover', 'false'),
                ('BasicODCoverage', (
                    ('BasicODCoverage', (
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('IsPAToDriverCovered', 'false'),
                ('NonElectricItems', (
                    ('NonElectricalItems', (
                        ('Category', ''),
                        ('NonElectricPremium', ''),
                        ('SumInsured', '' if data[
                            'idvNonElectrical'] == '0' else data[
                            'idvNonElectrical']),
                        ('NonElectricalAccessorySlNo', ''),
                        ('MakeModel', ''),
                        ('PolicyID', ''),
                        ('SerialNo', ''),
                        ('NonElectricalItemsID', ''),
                        ('Description', ''))),
                    )),
                ('NoOfLegalLiabilityCoveredCoolies', '0'),
                ('BiFuelKitSi', '0' if data[
                    'cngKitValue'] == '0' else data[
                    'cngKitValue']),
                ('IsLegalLiabilityToCoolieCovered', 'false'),
                ('IsLiabilitytoCoolie', 'false'),
                ('NoOfLLToPersonsEmployedInOperations_CleanerConductorCoolie',
                    '0'),
                ('IsLiabilityToConductor', 'false'),
                ('IsGeographicalAreaExtended', 'false'),
                ('PAToPaidCleaner', (
                    ('PAToPaidCleaner', (
                        ('SumInsured', '0'),
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('PolicyCoverID', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('IsAutomobileAssociationMember', 'false' if data[
                    'extra_isMemberOfAutoAssociation'] == '0'
                    else 'true'),
                ('IsImportedVehicle', 'false'),
                ('IsInsuredAnIndividual', 'true'),
                ('IsNFPPIncludingEmployees', 'false'),
                ('IsPAToOwnerDriverCoverd', 'true'),
                ('iNoOfLegalLiabilityCoveredPeopleOtherThanPaidDriver',
                    '0'),
                ('cAdditionalCompulsoryExcess', '0'),
                ('IsNFPPExcludingEmployees', 'false'),
                ('IsIndemnityToHirerCovered', 'false'),
                ('IsLiabilityForAccidentsExclude', 'false'),
                ('TotalCover', (
                    ('TotalCover', (
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('IsIndividualAlreadyInsured', 'false'),
                ('IsOverTurningCovered', 'false'),
                ('AutomobileAssociationExpiryDate', '' if data[
                    'extra_isMemberOfAutoAssociation'] == '0'
                    else new_policy_end_date.strftime(
                        '%d/%m/%Y')),
                ('LiabilityToPaidDriver', (
                    ('LiabilityToPaidDriver', (
                        ('PolicyCoverID', ''),
                        ('IsChecked', 'false'),
                        ('NoOfItems', '' if data[
                            'extra_isLegalLiability'] == '0'
                            else '1'),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('WorkmenCompensationExcludingDriver', (
                    ('WorkmenCompensationExcludingDriver', (
                        ('IsChecked', 'false'),
                        ('NoOfItems', '0'),
                        ('IsMandatory', 'false'))),
                    )),
                ('IsUsedForDrivingTuition', 'false'),
                ('IsLiabilityForAccidentsInclude', 'false'),
                ('IsAddOnCoverforTowing', 'false'),
                ('NoOfLegalLiabilityCoveredConductors', '0'),
                ('IsPAToNamedPassenger', 'false'),
                ('LegalLiabilitytoCleaner', ''),
                ('TPPDCover', (
                    ('TPPDCover', (
                        ('SumInsured', '0'),
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('PolicyCoverID', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('ISLegalLiabilityToConductorCovered', 'false'),
                ('AutomobileAssociationMembershipDiscount', (
                    ('AutomobileAssociationMembershipDiscount', (
                        ('IsChecked', 'false'),
                        ('NoOfItems', '' if data[
                            'extra_isMemberOfAutoAssociation'] == '0'
                            else '1'),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('NFPPIncludingEmployees', (
                    ('NFPPIncludingEmployees', (
                        ('IsChecked', 'false'),
                        ('NoOfItems', '0'),
                        ('IsMandatory', 'false'))),
                    )),
                ('PACoverToOwnerDriver', '1'),
                ('IsRoadTaxcover', 'false'),
                ('IsHandicappedDiscount', 'false'),
                ('NCBRetentiontRate', '' if data[
                    'addon_isNcbProtection'] == '0'
                    else ncb_retention_rate),
                ('NoOfAccidentToPassengerCovered', '0'),
                ('NoOfLLToPersonsEmployedInOperations_PaidDriver',
                    '0'),
                ('IsTPPDCover', 'true'),
                ('LiabilityToEmployee', (
                    ('LiabilityToEmployee', (
                        ('PolicyCoverID', ''),
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('GeographicalExtension', (
                    ('GeographicalExtension', (
                        ('IsChecked', 'false'),
                        ('IsMandatory', 'false'),
                        ('Countries', ''))),
                    )),
                ('IsPAToPaidCleanerCovered', 'false'),
                ('IsVehicleDesignedAsCV', 'false'),
                ('CompulsoryDeductible', '0'),
                ('IsPAToConductorCovered', 'false'),
                ('BasicLiability', (
                    ('BasicLiability', (
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('PAToPaidDriver', (
                    ('PAToPaidDriver', (
                        ('SumInsured', '0'),
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('PolicyCoverID', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('LiabilityToConductor', (
                    ('LiabilityToConductor', (
                        ('IsChecked', 'false'),
                        ('NoOfItems', '0'),
                        ('IsMandatory', 'false'))),
                    )),
                ('IsInsurancePremium', 'false'),
                ('IsElectricalItemFitted', 'false' if data[
                    'idvElectrical'] == '0' else 'true'),
                ('PAToConductorSI', '0'),
                ('NonElectricalItemsTotalSI', '0' if data[
                    'idvNonElectrical'] == '0' else data[
                    'idvNonElectrical']),
                ('iNumberOfLegalLiabilityCoveredPaidDrivers',
                    '0' if data['extra_isLegalLiability'] == '0'
                    else data['extra_isLegalLiability']),
                ('IsTotalCover', 'false'),
                ('NoOfLegalLiabilityCoveredNonFarePayingPassengers',
                    '0'),
                ('NilDepreciationCoverage', (
                    ('NilDepreciationCoverage', (
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('ApplicableRate', '' if data[
                            'addon_isDepreciationWaiver'] == '0'
                            else depreciation_rate),
                        ('PolicyCoverID', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('PACoverToOwner', (
                    ('PACoverToOwner', (
                        ('OtherRelation', ''),
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('NomineeAddress', ''),
                        ('NomineeDOB', ''),
                        ('NomineeRelationship', ''),
                        ('AppointeeName', ''),
                        ('PackageName', ''),
                        ('NomineeName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('IndemnityToHirer', (
                    ('IndemnityToHirer', (
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('AdditionalTowingCoverage', (
                    ('AdditionalTowingCoverage', (
                        ('SumInsured', '0'),
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('PolicyCoverID', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('NoOfLLToNFPPNotWorkmenUnderWCAct', '0'),
                ('AntiTheftDeviceDiscount', (
                    ('AntiTheftDeviceDiscount', (
                        ('IsChecked', 'false'),
                        ('NoOfItems', ''),
                        ('PackageName', ''),
                        ('IsMandatory', 'false'))),
                    )),
                ('NoOfPAtoPaidCleanerCovered', '0'),
                ('IsAntiTheftDeviceFitted', 'false' if data[
                    'extra_isAntiTheftFitted'] == '0'
                    else 'true'),
                ('IsRegistrationCover', 'false'),
                ('IsLLToNFPPNotWorkmenUnderWCAct', 'false'))),
            ('PreviousInsuranceDetails', (
                ('PrevYearPolicyStartDate',
                    past_policy_start_date.strftime('%d/%m/%Y')
                    if not is_new_vehicle
                    else None),
                ('IsClaimedLastYear', 'false' if data[
                    'isClaimedLastYear'] == '0' else 'true'),
                ('PrevYearNCB', ncb),
                ('PrevYearPolicyEndDate',
                    past_policy_end_date.strftime('%d/%m/%Y')
                    if not is_new_vehicle
                    else None),
                ('IsNCBApplicable', ncb_applicable))),
            ('NCBEligibility', (
                ('NCBEligibilityCriteria', '2'),
                ('NCBReservingLetter', ''),
                ('CurrentNCB',
                    custom_data.NCB_PERCENT_TO_CURRENT_NCB[data[
                        'previousNCB']] if ncb_applicable == 'true'
                    else '0'),
                ('PreviousNCB', ncb))),
            ('Policy', (
                ('productcode', self.Configuration.PRODUCT_CODE),
                ('BusinessType', '1' if is_new_vehicle else '5'),
                ('isMotorQuote', 'false'),
                ('Cover_From', new_policy_start_date.strftime(
                    '%d/%m/%Y')),
                ('AgentName', 'coverfox.com'),
                ('OtherSystemName', '1'),
                ('isMotorQuoteFlow', 'false'),
                ('Branch_Code', BRANCH_CODE)
            ))
        )

        return contract_details


class ProposalGenerator(object):

    def __init__(self, integrationUtils, configuration):
        self.parent = integrationUtils
        self.Configuration = configuration
        self.api_data = PROPOSAL_API

    def save_policy_details(self, data, transaction):
        quote_parameters = transaction.quote.raw_data
        quote_response = transaction.raw['quote_response']
        quote_raw_response = transaction.raw['quote_raw_response']
        _XML = self._get_proposal_xml_data(
            data, quote_parameters, quote_response, quote_raw_response)
        pxml = insurer_utils.convert_tuple_to_xmltree(_XML)
        proposal_xml = etree.tostring(pxml)
        transaction.proposal_request = proposal_xml
        transaction.save()
        raw_resp = get_response(name=transaction.transaction_id,
                                content=proposal_xml, api_data=self.api_data)

        transaction.raw['policy_details'] = raw_resp
        transaction.save()

        resp = raw_resp['MotorPolicy']

        if resp['status'] == '1':
            transaction.proposal_response = raw_resp
            transaction.proposal_number = resp['ProposalNo']
            transaction.proposal_on = datetime.datetime.now()
            transaction.proposal_amount = resp['FinalPremium']
            return True

        # Proposal failure response mail
        mail_utils.send_mail_for_proposal_failure(transaction, str(raw_resp))
        return False

    def _get_proposal_xml_data(
        self, data, quote_parameters, quote_response,
            quote_raw_response):

        proposal_data = ("PolicyDetails", (
            ("SourceSystemID", SOURCE_SYSTEM_ID),
            ("AuthToken", AUTH_TOKEN),
            ("UserID", USER_ID),
            ("ProductCode", self.Configuration.PRODUCT_CODE),
            self._get_client_details(data),
            self._get_policy_details(data, quote_parameters),
            self._get_risk_details(data, quote_parameters),
            self._get_vehicle_details(data, quote_parameters),
            self._get_cover_details(data, quote_parameters),
            self._get_previous_policy_details(data, quote_parameters),
            self._get_ncb_details(data, quote_parameters))
        )

        return proposal_data

    def _get_client_details(self, data):
        client_details = ('ClientDetails', (
            ('Salutation', 'Mr' if data['cust_gender'] == 'Male'
                else ('Ms' if data['cust_marital_status'] == '0'
                      else 'Mrs')),
            ('ForeName', data['cust_first_name']),
            ('MidName', ''),
            ('LastName', data['cust_last_name']),
            ('PhoneNo', ''),
            ('EmailID', data['cust_email']),
            ('Nationality', '1949'),
            ('MaritalStatus', custom_data.MARITAL_STATUS_ID[data[
                'cust_marital_status']]),
            ('ClientType', '0'),
            ('DOB', data['cust_dob']),
            ('Gender', data['cust_gender']),
            ('OccupationID', '26'),
            ('MobileNo', data['cust_phone']),
            ('ClientAddress', (
                ('CommunicationAddress', (
                    ('CityID',
                        custom_data.PINCODE_TO_STATE_CITY_ID[data[
                            'add_pincode']]['city_id']),
                    ('Country', '1'),
                    ('AddressType', '0'),
                    ('NearestLandmark', data['add_landmark']),
                    ('DistrictID',
                        custom_data.PINCODE_TO_STATE_CITY_ID[data[
                            'add_pincode']]['district_id']),
                    ('Address1', data['add_house_no']),
                    ('Address2', data['add_building_name']),
                    ('Address3', data['add_street_name']),
                    ('Pincode', data['add_pincode']),
                    ('StateID',
                        custom_data.PINCODE_TO_STATE_CITY_ID[data[
                            'add_pincode']]['state_id']))),
                ('PermanentAddress', (
                    ('CityID',
                        custom_data.PINCODE_TO_STATE_CITY_ID[data[
                            'add_pincode']]['city_id']),
                    ('Country', '1'),
                    ('AddressType', '0'),
                    ('NearestLandmark', data['add_landmark']),
                    ('DistrictID',
                        custom_data.PINCODE_TO_STATE_CITY_ID[data[
                            'add_pincode']]['district_id']),
                    ('Address1', data['add_house_no']),
                    ('Address2', data['add_building_name']),
                    ('Address3', data['add_street_name']),
                    ('Pincode', data['add_pincode']),
                    ('StateID',
                        custom_data.PINCODE_TO_STATE_CITY_ID[data[
                            'add_pincode']]['state_id']))),
                ('RegistrationAddress', (
                    ('CityID',
                        custom_data.PINCODE_TO_STATE_CITY_ID[data[
                            'reg_add_pincode']]['city_id']),
                    ('Country', '1'),
                    ('AddressType', '0'),
                    ('NearestLandmark', data['reg_add_landmark']),
                    ('DistrictID',
                        custom_data.PINCODE_TO_STATE_CITY_ID[data[
                            'reg_add_pincode']]['district_id']),
                    ('Address1', data['reg_add_house_no']),
                    ('Address2', data['reg_add_building_name']),
                    ('Address3', data['reg_add_street_name']),
                    ('Pincode', data['reg_add_pincode']),
                    ('StateID',
                        custom_data.PINCODE_TO_STATE_CITY_ID[data[
                            'reg_add_pincode']]['state_id'])))
                ))
            )
        )

        return client_details

    def _get_policy_details(self, data, quote_parameters):
        is_new_vehicle = (quote_parameters['isNewVehicle'] == '1')
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                quote_parameters['newPolicyStartDate'], "%d-%m-%Y")
            new_policy_end_date = new_policy_start_date + \
                relativedelta.relativedelta(years=1) - \
                datetime.timedelta(days=1)
        else:
            past_policy_end_date = datetime.datetime.strptime(
                quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")

            # Expired Policy Two-Wheeler
            if (
                    past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now() and
                    self.parent.Configuration.VEHICLE_TYPE == 'Twowheeler'
            ):
                new_policy_start_date = datetime.datetime.now() + \
                                        datetime.timedelta(days=3)
            else:
                new_policy_start_date = past_policy_end_date + \
                                        datetime.timedelta(days=1)

            new_policy_end_date = new_policy_start_date + \
                relativedelta.relativedelta(years=1) - \
                datetime.timedelta(days=1)

        policy_details = ('Policy', (
            ('productcode', self.Configuration.PRODUCT_CODE),
            ('BusinessType', '1' if is_new_vehicle else '5'),
            ('MTATypes', (
                ('GrandSubType', ''),
                ('SubType', ''),
                ('Type', ''),
                ('MTAReasonCode', ''))),
            ('isMotorQuote', 'true'),
            ('Cover_From', new_policy_start_date.strftime(
                '%d-%m-%Y')),
            ('Branch_Name', 'Direct'),
            ('Branch_Code', BRANCH_CODE),
            ('AgentCode', 'Direct'),
            ('MTAEffeciveDate', ''),
            ('MTAReason', ''),
            ('AgentName', 'coverfox.com'),
            ('Cover_To', new_policy_end_date.strftime(
                '%d-%m-%Y')),
            ('isMotorQuoteFlow', 'true'),
            ('OtherSystemName', '1'))
        )

        return policy_details

    def _get_risk_details(self, data, quote_parameters):
        rto_code = '%s-%02d' % (quote_parameters['registrationNumber[]'][0], int(
            quote_parameters['registrationNumber[]'][1]))
        mfg_date = datetime.datetime.strptime(
            quote_parameters['manufacturingDate'], '%d-%m-%Y')
        registration_date = datetime.datetime.strptime(
            quote_parameters['registrationDate'], '%d-%m-%Y')
        vehicle = Vehicle.objects.filter(insurer=INSURER).get(
            id=data['vehicleId'])
        rto_zone = 'A' if rto_code in custom_data.ZONE_A_RTO_LIST else 'B'
        ex_showroom_price = vehicle.raw_data.split('|')[-1].split(
            ',')[custom_data.STATE_TO_GROUP_ID[
                custom_data.RTO_CODE_TO_STATE_ID[rto_code]]-1]

        risk_details = ('Risk', (
            ('Rto_State_City', ''),
            ('Make_Model_Variant_CC', ''),
            ('IsRegAddressSameasPermanentAddress', 'true' if data[
                'add-same'] == '1' else 'false'),
            ('RTOLocationID',
                custom_data.RTO_CODE_TO_LOCATION_ID[rto_code]),
            ('IDV', data['idv']),
            ('TotalIDV', '0'),
            ('Colour', ''),
            ('IsPermanentAddressSameasCommAddress', 'true'),
            ('LicensedCarryingCapacity', ''),
            ('ManufactureYear', mfg_date.year),
            ('BodyIDV', '0'),
            ('SalesManagerCode', 'Direct'),
            ('EngineNo', data['vehicle_engine_no']),
            ('DateOfPurchase', registration_date.strftime(
                '%d-%m-%Y')),
            ('FinanceType', '1' if data[
                'is_car_financed'] in ['1', 1, 'true'] else ''),
            ('TrailerIDV', ''),
            ('Rto_RegionCode', rto_code),
            ('IsVehicleHypothicated', 'true' if data[
                'is_car_financed'] in ['1', 1, 'true'] else 'false'),
            ('NoOfWheels', ''),
            ('StateOfRegistrationID',
                custom_data.RTO_CODE_TO_STATE_ID[rto_code]),
            ('CubicCapacity', vehicle.cc),
            ('BodyType', ''),
            ('Zone', rto_zone),
            ('VehicleModelID', vehicle.model_code),
            ('FinancierName', custom_data.FINANCIERS[data[
                'car-financier']] if data[
                'is_car_financed'] in ['1', 1, 'true'] else ''),
            ('OtherColour', ''),
            ('PurposeOfUsage', ''),
            ('ChassisIDV', '0'),
            ('ExShowroomPrice', ex_showroom_price),
            ('VehicleMakeID', vehicle.make_code),
            ('ManufactureMonth', "%02d" % mfg_date.month),
            ('Chassis', data['vehicle_chassis_no']),
            ('FinancierCity', ''),
            ('VehicleVariant', vehicle.variant),
            ('IsRegAddressSameasCommAddress', 'true' if data[
                'add-same'] == '1' else 'false'),
            ('FinancierAddress',  'India' if data[
                'is_car_financed'] in ['1', 1, 'true'] else ''),
            ('SalesManagerName', 'Direct'),
            ('GrossVehicleWeight', ''))
        )

        return risk_details

    def _get_vehicle_details(self, data, quote_parameters):
        vehicle = Vehicle.objects.filter(insurer=INSURER).get(id=data['vehicleId'])
        is_new_vehicle = (quote_parameters['isNewVehicle'] == '1')
        registration_date = datetime.datetime.strptime(quote_parameters[
            'registrationDate'], '%d-%m-%Y')
        fuel = custom_data.FUEL_TYPE[vehicle.fuel_type]

        vehicle_details = ('Vehicle', (
            ('MiscTypeOfVehicle', ''),
            ('TypeOfFuel', fuel),
            ('ISNewVehicle', 'true' if is_new_vehicle else 'false'),
            ('RoadTypes', (
                ('RoadType', (
                    ('RoadTypeID', ''),
                    ('TypeOfRoad', ''))),
                )),
            ('SeatingCapacity', vehicle.seating_capacity),
            ('Permit', (
                ('PermitType', (
                    ('TypeOfPermit', ''),)),
                )),
            ('RegistrationNumber_New', ''),
            ('MiscTypeOfVehicleID', ''),
            ('Registration_date', registration_date.strftime(
                '%d-%m-%Y')),
            ('Registration_Number', 'NEW' if is_new_vehicle
                else data['vehicle_reg_no']))
        )

        return vehicle_details

    def _get_cover_details(self, data, quote_parameters):
        vehicle = Vehicle.objects.filter(insurer=INSURER).get(
            id=data['vehicleId'])
        quote_parameters['vehicleId'] = data['vehicleId']
        coverage_response = IntegrationUtils(self.Configuration.VEHICLE_TYPE).CoverageCalculator\
            .get_coverage(quote_parameters)
        coverage_response = coverage_response['ListCovers']['LstAddonCovers'][
            'AddonCovers']

        cov_resp = {}
        for node in coverage_response:
            cov_resp[node['CoverageName']] = node

        try:
            depreciation_rate = cov_resp['Nil Depreciation']['rate']
        except KeyError:
            depreciation_rate = 0
            quote_parameters['addon_isDepreciationWaiver'] = '0'

        try:
            ncb_retention_rate = cov_resp['NCB Retention']['rate']
        except KeyError:
            ncb_retention_rate = 0
            quote_parameters['addon_isNcbProtection'] = '0'

        try:
            nominee_dob = "01-01-"+str(
                (datetime.date.today().year - int(data['nominee_age'])))
        except ValueError:
            nominee_dob = ""

        cover_details = ('Cover', (
            ('PAToNamedPassenger', (
                ('PAToNamedPassenger', (
                    ('NoOfItems', ''),
                    ('PassengerName', ''),
                    ('NomineeDOB', ''),
                    ('NomineeRelationship', ''),
                    ('PackageName', ''),
                    ('OtherRelation', ''),
                    ('IsChecked', 'false'),
                    ('SumInsured', ''),
                    ('NomineeAddress', ''),
                    ('AppointeeName', ''),
                    ('NomineeName', ''))),
                )),
            ('IsBasicLiability', 'true'),
            ('IsLLToPersonsEmployedInOperations_CleanerConductorCoolieCovered',
                'false'),
            ('IsDetariffRateForOverturning', 'false'),
            ('IsCoveredForDamagedPortion', 'false'),
            ('IsLossOfAccessoriesCovered', 'false'),
            ('RegistrationCost', (
                ('RegistrationCost', (
                    ('SumInsured', '0'),
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''),
                    ('PackageName', ''))),
                )),
            ('IsUseOfVehiclesConfined', 'false'),
            ('IsNCBRetention', 'false' if quote_parameters[
                'addon_isNcbProtection'] == '0' else 'true'),
            ('IsBiFuelKit', 'false' if quote_parameters[
                'isCNGFitted'] == '0' else 'true'),
            ('IsLLUnderWCActForCarriageOfMoreThanSixEmpCovered',
                'false'),
            ('IsImportedVehicleCover', 'false'),
            ('IsTPPDLiabilityRestricted', 'false'),
            ('UnnamedPassengersSI',  '0' if quote_parameters[
                'extra_paPassenger'] == '0' else quote_parameters[
                'extra_paPassenger']),
            ('NoOfLiabilityCoveredEmployees', '0'),
            ('IsLiabilitytoCleaner', 'false'),
            ('IsFibreGlassFuelTankFitted', 'false'),
            ('NoOfUnnamedPassenegersCovered', '0'
                if quote_parameters['extra_paPassenger'] == '0'
                else vehicle.seating_capacity),
            ('IsVoluntaryDeductableOpted', 'false'
                if quote_parameters['voluntaryDeductible'] == '0'
                else 'true'),
            ('IsBasicODCoverage', 'true'),
            ('AddOnCoverTowingCharge', '0'),
            ('IsLiabilityToEmployeeCovered', 'false'),
            ('IsLiabilityToPaidDriverCovered', 'false'
                if quote_parameters['extra_isLegalLiability'] == '0'
                else 'true'),
            ('IsLegalLiabilityToNonFarePayingPassengersCovered',
                'false'),
            ('NoOfLegalLiabilityCoveredCleaners', '0'),
            ('IsSpeciallyDesignedForHandicapped', 'false'),
            ('ElectricItems', (
                ('ElectricalItems', (
                    ('SumInsured', '0' if quote_parameters[
                        'idvElectrical'] == '0' else quote_parameters[
                        'idvElectrical']),
                    ('ElectricalAccessorySlNo', ''),
                    ('MakeModel', ''),
                    ('PolicyId', ''),
                    ('ElectricalItemsID', ''),
                    ('SerialNo', ''),
                    ('Description', ''),
                    ('ElectricPremium', ''))),
                )),
            ('InsurancePremium', (
                ('InsurancePremium', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''),
                    ('PackageName', ''))),
                )),
            ('PAToUnNamedPassenger', (
                ('PAToUnNamedPassenger', (
                    ('SumInsured', '0' if quote_parameters[
                        'extra_paPassenger'] == '0' else quote_parameters[
                        'extra_paPassenger']),
                    ('PolicyCoverID', ''),
                    ('IsChecked', 'false'),
                    ('NoOfItems', '' if quote_parameters[
                        'extra_paPassenger'] == '0'
                        else vehicle.seating_capacity),
                    ('PackageName', ''))),
                )),
            ('LiabilitytoCleaner', (
                ('LiabilitytoCleaner', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', '0'))),
                )),
            ('PACoverToNamedPassengerSI', '0'),
            ('DrivingTuitionCoverage', (
                ('DrivingTuitionCoverage', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''),
                    ('PackageName', ''))),
                )),
            ('ISLegalLiabilityToDefenceOfficialDriverCovered', 'false'),
            ('ElectricalItemsTotalSI', '0' if quote_parameters[
                'idvElectrical'] == '0' else quote_parameters[
                'idvElectrical']),
            ('NFPPExcludingEmployees', (
                ('NFPPExcludingEmployees', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''))),
                )),
            ('IsCoverageoFTyreBumps', 'false'),
            ('TrailerDetails', (
                ('TrailerInfo', (
                    ('ChassisNumber', ''),
                    ('MakeandModel', ''),
                    ('IDV', ''),
                    ('SerialNumber', ''),
                    ('Registration_No', ''),
                    ('ManufactureYear', ''))),
                )),
            ('VoluntaryDeductableAmount', quote_parameters[
                'voluntaryDeductible']),
            ('PAToDriverSI', '0'),
            ('NoOfLLUnderWCAct', '1'),
            ('SpeciallyDesignedforChallengedPerson', (
                ('SpeciallyDesignedforChallengedPerson', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''),
                    ('PackageName', ''))),
                )),
            ('PAToConductor', (
                ('PAToConductor', (
                    ('SumInsured', ''),
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''))),
                )),
            ('IsAccidentToPassengerCovered', 'false'
                if quote_parameters['extra_paPassenger'] == '0'
                else 'true'),
            ('IsTrailerAttached', 'false'),
            ('FibreGlassFuelTank', (
                ('FibreGlassFuelTank', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''),
                    ('PackageName', ''))),
                )),
            ('IsNonElectricalItemFitted', 'false'
                if quote_parameters['idvNonElectrical'] == '0'
                else 'true'),
            ('IsWorkmenCompensationExcludingDriver', 'false'),
            ('NumberOfPACoveredPaidDrivers', '0'),
            ('LiabilitytoCoolie', (
                ('LiabilitytoCoolie', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', '0'))),
                )),
            ('IsRacingCovered', 'false'),
            ('IsAdditionalTowingCover', 'false'),
            ('IsVehicleMadeInIndia', 'true'),
            ('IsLegalLiabilityToCleanerCovered', 'false'),
            ('BifuelKit', (
                ('BifuelKit', (
                    ('IsChecked', 'false'),
                    ('SumInsured', '0' if quote_parameters[
                        'cngKitValue'] == '0' else quote_parameters[
                        'cngKitValue']),
                    ('NoOfItems', ''),
                    ('PolicyCoverDetailsID', ''),
                    ('PolicyCoverID', ''),
                    ('PackageName', ''),
                    ('Fueltype', ''),
                    ('ISLpgCng', 'true' if quote_parameters[
                        'cngKitValue'] == '0' else 'false'))),
                )),
            ('PAToCleanerSI', '0'),
            ('VoluntaryDeductible', (
                ('VoluntaryDeductible', (
                    ('PolicyCoverID', ''),
                    ('IsChecked', 'false'),
                    ('SumInsured', quote_parameters[
                        'voluntaryDeductible']),
                    ('PackageName', ''),
                    ('NoOfItems', ''))),
                )),
            ('IsLLToPersonsEmployedInOperations_PaidDriverCovered',
                'false'),
            ('IsNilDepreciation', 'false' if quote_parameters[
                'addon_isDepreciationWaiver'] == '0' else 'true'),
            ('IsConfinedToOwnPremisesCovered', 'false'),
            ('LiabilityForAccidentsExclude', (
                ('LiabilityForAccidentsExclude', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', '0'))),
                )),
            ('UseOfVehiclesConfined', (
                ('UseOfVehiclesConfined', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''),
                    ('PackageName', ''))),
                )),
            ('IsPAToUnnamedPassengerCovered', 'false'
                if quote_parameters['extra_paPassenger'] == '0'
                else 'true'),
            ('RoadTax', (
                ('RoadTax', (
                    ('SumInsured', '0'),
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''),
                    ('PackageName', ''),
                    ('PolicyCoverID', ''))),
                )),
            ('AdditionalTowingCharge', '0'),
            ('BasicODCoverage', (
                ('BasicODCoverage', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''),
                    ('PackageName', ''))),
                )),
            ('IsPAToDriverCovered', 'false'),
            ('NonElectricItems', (
                ('NonElectricalItems', (
                    ('Category', ''),
                    ('NonElectricPremium', ''),
                    ('SumInsured', '0'
                        if quote_parameters['idvNonElectrical'] == '0'
                        else quote_parameters['idvNonElectrical']),
                    ('NonElectricalAccessorySlNo', ''),
                    ('MakeModel', ''),
                    ('PolicyID', ''),
                    ('SerialNo', ''),
                    ('NonElectricalItemsID', ''),
                    ('Description', ''))),
                )),
            ('NoOfLegalLiabilityCoveredCoolies', '0'),
            ('BiFuelKitSi', '0' if quote_parameters[
                'cngKitValue'] == '0' else quote_parameters[
                'cngKitValue']),
            ('IsRegistrationCover', 'false'),
            ('IsLiabilitytoCoolie', 'false'),
            ('NoOfLLToPersonsEmployedInOperations_CleanerConductorCoolie',
                '0'),
            ('IsLiabilityToConductor', 'false'),
            ('IsGeographicalAreaExtended', 'false'),
            ('PAToPaidCleaner', (
                ('PAToPaidCleaner', (
                    ('SumInsured', '0'),
                    ('PolicyCoverID', ''),
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''),
                    ('PackageName', ''))),
                )),
            ('IsAutomobileAssociationMember', 'false'
                if quote_parameters[
                    'extra_isMemberOfAutoAssociation'] == '0'
                else 'true'),
            ('IsImportedVehicle', 'false'),
            ('IsInsuredAnIndividual', 'true'),
            ('IsNFPPIncludingEmployees', 'false'),
            ('IsPAToOwnerDriverCoverd', 'true'),
            ('iNoOfLegalLiabilityCoveredPeopleOtherThanPaidDriver',
                '0'),
            ('cAdditionalCompulsoryExcess', '0'),
            ('IsNFPPExcludingEmployees', 'false'),
            ('IsIndemnityToHirerCovered', 'false'),
            ('IsLiabilityForAccidentsExclude', 'false'),
            ('TotalCover', (
                ('TotalCover', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''),
                    ('PackageName', ''))),
                )),
            ('IsIndividualAlreadyInsured', 'false'),
            ('LiabilityForAccidentsInclude', (
                ('LiabilityForAccidentsInclude', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', '0'))),
                )),
            ('LiabilityToPaidDriver', (
                ('LiabilityToPaidDriver', (
                    ('PolicyCoverID', ''),
                    ('IsChecked', 'false'),
                    ('NoOfItems', '' if quote_parameters[
                        'extra_isLegalLiability'] == '0' else '1'),
                    ('PackageName', ''))),
                )),
            ('IsOverTurningCovered', 'false'),
            ('WorkmenCompensationExcludingDriver', (
                ('WorkmenCompensationExcludingDriver', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', '0'))),
                )),
            ('IsUsedForDrivingTuition', 'false'),
            ('IsLiabilityForAccidentsInclude', 'false'),
            ('IsAddOnCoverforTowing', 'false'),
            ('NoOfLegalLiabilityCoveredConductors', '0'),
            ('IsPAToNamedPassenger', 'false'),
            ('TPPDCover', (
                ('TPPDCover', (
                    ('PolicyCoverID', ''),
                    ('IsChecked', 'false'),
                    ('SumInsured', '0'),
                    ('PackageName', ''),
                    ('NoOfItems', ''))),
                )),
            ('ISLegalLiabilityToConductorCovered', 'false'),
            ('AutomobileAssociationMembershipDiscount', (
                ('AutomobileAssociationMembershipDiscount', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', '' if quote_parameters[
                        'extra_isMemberOfAutoAssociation'] == '0'
                        else '1'),
                    ('PackageName', ''))),
                )),
            ('NFPPIncludingEmployees', (
                ('NFPPIncludingEmployees', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', '0'))),
                )),
            ('PACoverToOwnerDriver', '1'),
            ('IsRoadTaxcover', 'false'),
            ('IsHandicappedDiscount', 'false'),
            ('NCBRetentiontRate', '' if quote_parameters[
                'addon_isNcbProtection'] == '0'
                else ncb_retention_rate),
            ('NoOfAccidentToPassengerCovered', '0'),
            ('NoOfLLToPersonsEmployedInOperations_PaidDriver',
                '0'),
            ('IsTPPDCover', 'true'),
            ('LiabilityToEmployee', (
                ('LiabilityToEmployee', (
                    ('PolicyCoverID', ''),
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''),
                    ('PackageName', ''))),
                )),
            ('GeographicalExtension', (
                ('GeographicalExtension', (
                    ('IsChecked', 'false'),
                    ('Countries', ''))),
                )),
            ('IsPAToPaidCleanerCovered', 'false'),
            ('IsVehicleDesignedAsCV', 'false'),
            ('CompulsoryDeductible', '0'),
            ('IsPAToConductorCovered', 'false'),
            ('BasicLiability', (
                ('BasicLiability', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''),
                    ('PackageName', ''))),
                )),
            ('PAToPaidDriver', (
                ('PAToPaidDriver', (
                    ('SumInsured', '0'),
                    ('PolicyCoverID', ''),
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''),
                    ('PackageName', ''))),
                )),
            ('LiabilityToConductor', (
                ('LiabilityToConductor', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', '0'))),
                )),
            ('IsInsurancePremium', 'false'),
            ('IsElectricalItemFitted', 'false' if quote_parameters[
                'idvElectrical'] == '0' else 'true'),
            ('PAToConductorSI', '0'),
            ('NonElectricalItemsTotalSI', '0' if quote_parameters[
                'idvNonElectrical'] == '0' else quote_parameters[
                'idvNonElectrical']),
            ('iNumberOfLegalLiabilityCoveredPaidDrivers', '0'
                if quote_parameters['extra_isLegalLiability'] == '0'
                else quote_parameters['extra_isLegalLiability']),
            ('IsTotalCover', 'false'),
            ('NoOfLegalLiabilityCoveredNonFarePayingPassengers',
                '0'),
            ('NilDepreciationCoverage', (
                ('NilDepreciationCoverage', (
                    ('PolicyCoverID', ''),
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''),
                    ('ApplicableRate', '' if quote_parameters[
                        'addon_isDepreciationWaiver'] == '0'
                        else depreciation_rate),
                    ('PackageName', ''))),
                )),
            ('PACoverToOwner', (
                ('PACoverToOwner', (
                    ('OtherRelation', ''),
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''),
                    ('NomineeAddress', ''),
                    ('NomineeDOB', nominee_dob),
                    ('NomineeRelationship', data['nominee_relationship']),
                    ('AppointeeName', ''),
                    ('PackageName', ''),
                    ('NomineeName', data['nominee_name']))),
                )),
            ('IndemnityToHirer', (
                ('IndemnityToHirer', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', ''))),
                )),
            ('AdditionalTowingCoverage', (
                ('AdditionalTowingCoverage', (
                    ('PolicyCoverID', ''),
                    ('IsChecked', 'false'),
                    ('SumInsured', '0'),
                    ('PackageName', ''),
                    ('NoOfItems', ''))),
                )),
            ('NoOfLLToNFPPNotWorkmenUnderWCAct', '0'),
            ('AntiTheftDeviceDiscount', (
                ('AntiTheftDeviceDiscount', (
                    ('IsChecked', 'false'),
                    ('NoOfItems', '' if quote_parameters[
                        'extra_isAntiTheftFitted'] == '0'
                        else '1'),
                    ('PackageName', ''))),
                )),
            ('NoOfPAtoPaidCleanerCovered', '0'),
            ('IsAntiTheftDeviceFitted', 'false' if quote_parameters[
                'extra_isAntiTheftFitted'] == '0' else 'true'),
            ('IsLegalLiabilityToCoolieCovered', 'false'),
            ('IsLLToNFPPNotWorkmenUnderWCAct', 'false'))
        )

        return cover_details

    def _get_previous_policy_details(self, data, quote_parameters):
        is_new_vehicle = (quote_parameters['isNewVehicle'] == '1')
        if is_new_vehicle:
            ncb_applicable = 'false'
            ncb = '0'
        else:
            past_policy_end_date = datetime.datetime.strptime(
                quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")
            past_policy_start_date = past_policy_end_date - \
                relativedelta.relativedelta(years=1) + \
                datetime.timedelta(days=1)
            ncb_applicable = 'true' if quote_parameters[
                'isClaimedLastYear'] == '0' else 'false'
            ncb = custom_data.NCB_PERCENT_TO_NCB[quote_parameters[
                'previousNCB']] if ncb_applicable == 'true' else '0'

        previous_policy_details = ('PreviousInsuranceDetails', (
            ('IsNCBEarnedAbroad', 'false'),
            ('IsTrailerNCB', 'false'),
            ('PrevYearPolicyStartDate',
                past_policy_start_date.strftime(
                    '%d-%m-%Y') if not is_new_vehicle
                else ''),
            ('PrevYearInsurerAddress', ''),
            ('IsClaimedLastYear', 'false'
                if quote_parameters[
                    'isClaimedLastYear'] == '0'
                else 'true'),
            ('PrevYearNCB', ncb),
            ('IsVehicleOfPreviousPolicySold', 'false'),
            ('Inspectionby', ''),
            ('IsInspectionDone', 'false'),
            ('PrevYearPolicyType', ''),
            ('PrevYearPolicyEndDate',
                past_policy_end_date.strftime('%d-%m-%Y')
                if not is_new_vehicle else ''),
            ('PreRateCharged', ''),
            ('PreSpecialTermsAndConditions', ''),
            ('PrevYearInsurer', '' if is_new_vehicle else
                custom_data.REV_PASTINSURER_MAP[data[
                    'past_policy_insurer']]),
            ('MTAReason', ''),
            ('InspectionID', ''),
            ('InspectorName', ''),
            ('InspectionDate', ''),
            ('IsNCBApplicable', ncb_applicable),
            ('PrevYearPolicyNo', '' if is_new_vehicle else data[
                'past_policy_number']),
            ('DocumentProof', ''),
            ('ODLoadingReason', ''),
            ('ODLoading', ''),
            ('PrevInsuranceID', ''))
        )

        return previous_policy_details

    def _get_ncb_details(self, data, quote_parameters):
        is_new_vehicle = (quote_parameters['isNewVehicle'] == '1')
        if is_new_vehicle:
            ncb_applicable = 'false'
            ncb = '0'
        else:
            ncb_applicable = 'true' if quote_parameters[
                'isClaimedLastYear'] == '0' else 'false'
            ncb = custom_data.NCB_PERCENT_TO_NCB[quote_parameters[
                'previousNCB']] if ncb_applicable == 'true' else '0'

        ncb_details = ('NCBEligibility', (
            ('NCBEligibilityCriteria', '2'
                if ncb_applicable == 'true' else ''),
            ('NCBReservingLetter', ''),
            ('PreviousNCB', ncb))
        )

        return ncb_details


class IntegrationUtils(object):

    def __init__(self, vehicle_type):
        self.Configuration = Configuration(vehicle_type)
        self.PremiumCalculator = PremiumCalculator(self, self.Configuration)
        self.ProposalGenerator = ProposalGenerator(self, self.Configuration)
        self.CoverageCalculator = CoverageCalculator(self, self.Configuration)

    def convert_premium_request_data_to_required_dictionary(
            self, vehicle_id,
            request_data):
        data_dictionary = copy.deepcopy(request_data)
        data_dictionary['masterVehicleId'] = data_dictionary['vehicleId']
        data_dictionary['vehicleId'] = vehicle_id
        data_dictionary['req_idv'] = request_data['idv']
        data_dictionary['idv'] = '0'

        is_new_vehicle = (data_dictionary['isNewVehicle'] == '1')

        if is_new_vehicle:
            data_dictionary['registrationDate'] = (datetime.datetime.now()).strftime(
                '%d-%m-%Y')
            data_dictionary['manufacturingDate'] = (datetime.datetime.now()).strftime(
                '%d-%m-%Y')

        return data_dictionary

    def get_buy_form_details(self, data, transaction):
        return {
            'registration_no': 'NEW---' if int(data['isNewVehicle']) else '-'.join(
                data['registrationNumber[]']),
            'rto_location_info': ''.join(data['registrationNumber[]'][:2]),
            'occupation_list': custom_data.OCCUPATION_MAP,
            # 'insurers_list': custom_data.PAST_POLICY_INSURER,
            'insurers_list': custom_data.PAST_INSURER_ID_LIST_MASTER,
            'disallowed_insurers': custom_data.DISALLOWED_PAST_INSURERS,
            'financier_list': custom_data.FINANCIERS,
            'state_list':   custom_data.STATE_MAP,
            'relationship_list': custom_data.NOMINEE_RELATIONSHIP_LIST,
            'is_cities_fetch': True,
        }

    def convert_proposal_request_data_to_required_dictionary(
            self, vehicle, request_data, quote_parameters, quote_response,
            quote_custom_response):

        form_data = copy.deepcopy(request_data)
        form_data['idv'] = quote_custom_response['calculatedAtIDV']

        is_new_vehicle = False
        if quote_parameters['isNewVehicle'] == '1':
            is_new_vehicle = True

        form_data['vehicleId'] = vehicle.id
        rto_code = '%s%02d' % (quote_parameters['registrationNumber[]'][
            0], int(quote_parameters['registrationNumber[]'][1]))
        form_data['rto_code'] = rto_code

        form_data['cust_title'] = 'Mr' if form_data['cust_gender'] == 'Male' else (
            'Ms' if form_data['cust_marital_status'] == '0' else 'Mrs')

        form_data['cust_dob'] = datetime.datetime.strptime(
            form_data['cust_dob'], '%d-%m-%Y').strftime('%d-%b-%Y')

        if not is_new_vehicle:
            pyp_end_date = datetime.datetime.strptime(
                quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")
            inception_date = pyp_end_date + datetime.timedelta(days=1)
            # Expired Policy Two-Wheeler
            if (pyp_end_date.replace(hour=23, minute=59, second=59) < datetime.datetime.now() and
                    self.Configuration.VEHICLE_TYPE == 'Twowheeler'):
                inception_date = datetime.datetime.now() + datetime.timedelta(days=3)
            form_data['past_policy_end_date'] = pyp_end_date.strftime(
                '%d/%m/%Y')
        else:
            form_data['past_policy_end_date'] = ''
            inception_date = datetime.datetime.strptime(
                quote_parameters['newPolicyStartDate'], '%d-%m-%Y')

        expiry_date = putils.add_years(inception_date, 1) - datetime.timedelta(days=1)
        form_data['inception_date'] = inception_date.strftime('%d-%b-%Y')
        form_data['expiry_date'] = expiry_date.strftime('%d-%b-%Y')

        if form_data['add-same'] in ['1', 1, 'true']:
            form_data['reg_add_house_no'] = form_data['add_house_no']
            form_data['reg_add_building_name'] = form_data['add_building_name']
            form_data['reg_add_street_name'] = form_data['add_street_name']
            form_data['reg_add_landmark'] = form_data['add_landmark']
            form_data['reg_add_state'] = form_data['add_state']
            form_data['reg_add_pincode'] = form_data['add_pincode']
            form_data['reg_add_city'] = form_data['add_city']

        return form_data

    def update_transaction_details(self, transaction, data):
        is_new_vehicle = data['isNewVehicle'] == '1'
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                data['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data['pastPolicyExpiryDate'], "%d-%m-%Y")

            # Expired Policy Two-Wheeler
            if (
                    past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now() and
                    self.Configuration.VEHICLE_TYPE == 'Twowheeler'
            ):
                new_policy_start_date = datetime.datetime.now() + \
                                        datetime.timedelta(days=3)
            else:
                new_policy_start_date = past_policy_end_date + \
                                        datetime.timedelta(days=1)

        new_policy_end_date = putils.add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)

        transaction.policy_start_date = new_policy_start_date
        transaction.policy_end_date = new_policy_end_date
        transaction.save()

    def get_payment_data(self, transaction):
        resp = transaction.raw['policy_details']['MotorPolicy']

        if resp['status'] == '1':
            ProposalNo = resp['ProposalNo']
            UserID = USER_ID
            ReturnURL = '%s/motor/%s/insurer/%s/payment/response/%s/' % (
                settings.SITE_URL.strip('/'), self.Configuration.URL_TAG,
                INSURER_SLUG, transaction.transaction_id)
            PaymentType = PAYMENT_TYPE
            PremiumAmount = resp['FinalPremium']
            payment_gateway_data = {
                'ProposalNo': ProposalNo,
                'UserID': UserID,
                'PaymentType': PaymentType,
                'ProposalAmount': PremiumAmount,
                'Responseurl': ReturnURL
            }
            suds_logger.set_name(transaction.transaction_id).log(
                json.dumps(payment_gateway_data))
            transaction.payment_request = payment_gateway_data
            transaction.save()
            return payment_gateway_data, PAYMENT_URL
        else:
            transaction.status = 'FAILED'
            transaction.raw['premium_response'] = resp
            transaction.save()
            return {}, ''

    def post_payment(self, payment_response, transaction):
        motor_logger.info(payment_response)
        suds_logger.set_name(transaction.transaction_id).log(json.dumps(payment_response))

        redirect_url = '/motor/%s/payment/failure/%s/' % (
            self.Configuration.URL_TAG, transaction.transaction_id)
        is_redirect = True
        transaction.insured_details_json[
            'payment_response'] = payment_response['GET']
        transaction.payment_response = payment_response['GET']
        transaction.save()

        try:
            statusID, policyNo, transactionNumber, optionalValue, gatewayName, \
                proposalNumber, transactionStatus, message = payment_response[
                    'GET']['Output'].split('|')
        except ValueError:
            statusID, opt1, opt2, optionalValue, gatewayName, proposalNumber, \
                transactionNumber, error_reliance, opt3, \
                error_billdesk = payment_response['GET']['Output'].split('|')

        if statusID == '1':
            ScheduleURL = '%s?PolicyNo=%s&ProductCode=%s' % (
                POLICY_API['url'], policyNo, self.Configuration.PRODUCT_CODE)
            transaction.insured_details_json['policy_url'] = ScheduleURL
            transaction.payment_done = True
            transaction.payment_on = datetime.datetime.now()
            transaction.policy_number = policyNo
            transaction.premium_paid = transaction.raw['policy_details'][
                'MotorPolicy']['FinalPremium']
            transaction.mark_complete()
            redirect_url = '/motor/%s/payment/success/%s/' % (
                self.Configuration.URL_TAG, transaction.transaction_id)
            mail_data = 'Policy number for transaction %s (CFOX-%s) is' \
                '%s (email: %s, customer name: %s, policy url: %s)'
            cutomer_name = transaction.proposer.first_name + ' ' + \
                transaction.proposer.last_name
            mail_utils.send_mail_for_admin({
                'title': 'Policy number for %s' % INSURER.title,
                'type': 'policy',
                'data': mail_data % (transaction.transaction_id,
                                     transaction.id,
                                     policyNo,
                                     transaction.proposer.email,
                                     cutomer_name,
                                     ScheduleURL)
            })
        elif statusID == '0':
            transaction.payment_done = False
            transaction.status = 'FAILED'
            transaction.raw['error'] = "TransactionNo: %s, ProposalNo: %s," \
                "Message: %s and %s" % (
                transactionNumber, proposalNumber, error_reliance, error_billdesk)
        else:
            transaction.status = 'FAILED'
        transaction.save()
        return is_redirect, redirect_url, '', {}

    def convert_premium_response(self, data, data_dictionary, vehicle):
        if data.get('FinalPremium', None) is None:
            motor_logger.info("RELIANCE Returend error:=> %s" %
                              data['ErrorMessages'])
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_ZERO_PREMIUM}

        for node in data['lstPricingResponse']:
            data[node['CoverageName']] = node['Premium']
        del data['lstPricingResponse']

        basic_od = (
            float(data.get('Basic OD', 0)) +
            float(data.get('InspectionCharges', 0)) +
            (-float(data.get('OD Discount', 0)))
        )

        ncb = 0.0
        if data_dictionary['isClaimedLastYear'] == '0':
            ncb = float(data.get('NCB', 0))

        serviceTaxRate = (
            float(data.get('serviceTaxRate', 14)) +
            float(data.get('EducationalCessRate', 0)) +
            float(data.get('HigherEducationalCessRate', 0))
        ) / 100

        basic_covers = [
            {'premium': basic_od,
             'id': 'basicOd',             'default': '1', },
            {'premium': float(data.get('Electrical Accessories', 0)),
             'id': 'idvElectrical',       'default': '1', },
            {'premium': float(data.get('Non Electrical Accessories', 0)),
             'id': 'idvNonElectrical',    'default': '1', },
            {'premium': float(data.get('Bifuel Kit', 0)),
             'id': 'cngKit',              'default': '1', },
            {'premium': float(data.get('Basic Liability', 0)),
             'id': 'basicTp',             'default': '1', },
            {'premium': float(data.get('PA to Owner Driver', 0)),
             'id': 'paOwnerDriver',       'default': '1', },
            {'premium': float(data.get('PA to Unnamed Passenger', 0)),
             'id': 'paPassenger',         'default': '1', },
            {'premium': float(data.get('Liability to Paid Driver', 0)),
             'id': 'legalLiabilityDriver', 'default': '0', },
        ]
        if data.get('Bifuel Kit TP', False) is not False:
            basic_covers.append({
                'premium': float(data['Bifuel Kit TP']),
                'id': 'cngKitTp',
                'default': '1'
            })
        special_discounts = [
            {'premium': float(data.get('Voluntary Deductible', 0)),
             'id': 'voluntaryDeductible',  'default': '1', },
            {'premium': float(data.get('Anti-Theft Device', 0)),
             'id': 'isAntiTheftFitted',    'default': '1', },
            {'premium': float(data.get('Automobile Association Membership', 0)),
             'id': 'isMemberOfAutoAssociation',   'default': '1', }
        ]
        # if data_dictionary['extra_isTPPDDiscount'] == '1' and data.get('TPPD', False) is not False:
        #     special_discounts.append({
        #         'premium': float(data['TPPD']),
        #         'id': 'isTPPDDiscount',
        #         'default': '1'
        #     })
        discounts = [
            {'premium': ncb,
             'id': 'ncb',   'default': '1', },
            {'premium': float(data.get('OD Discount', 0)),
             'id': 'odDiscount',   'default': '1', },
        ]
        addon_covers = []
        if data.get('Nil Depreciation', False) is not False:
            addon_covers.append({
                'premium': float(data['Nil Depreciation']),
                'id': 'isDepreciationWaiver',
                'default': '0'
            })
        if data.get('NCB Retention', False) is not False:
            addon_covers.append({
                'premium': float(data['NCB Retention']),
                'id': 'isNcbProtection',
                'default': '0'
            })
        result_data = {
            'insurerId': INSURER.id,
            'insurerSlug': INSURER_SLUG,
            'insurerName': INSURER.title,
            'isInstantPolicy': True,
            'minIDV': data_dictionary['min_idv'],
            'maxIDV': data_dictionary['max_idv'],
            'calculatedAtIDV': data_dictionary['idv'],
            'serviceTaxRate': serviceTaxRate,
            'premiumBreakup': {
                'serviceTax': round(float(data.get('ServiceTaxAmount', 0)), 2),
                'basicCovers': insurer_utils.cpr_update(basic_covers),
                'addonCovers': insurer_utils.cpr_update(addon_covers),
                'discounts': insurer_utils.cpr_update(discounts),
                'specialDiscounts': insurer_utils.cpr_update(special_discounts),
                'paPassengerAmounts': insurer_utils.get_pa_passenger_dict(
                    vehicle.vehicle_type, vehicle.seating_capacity),
            },
        }, data
        return {'result': result_data, 'success': True}

    def get_city_list(self, state):
        return custom_data.STATE_ID_TO_CITY_LIST[state]

    def get_dependent_addons(self, parameters):
        return {'type': 'dynamic'}

    def get_transaction_from_id(self, _id):
        return Transaction.objects.get(transaction_id=_id)

    def get_custom_data(self, datatype):
        datamap = {
            'occupations': custom_data.OCCUPATION_MAP,
            # 'insurers': custom_data.PAST_POLICY_INSURER,
            'insurers_list': custom_data.PAST_INSURER_ID_LIST_MASTER,
            'disallowed_insurers': custom_data.DISALLOWED_PAST_INSURERS,
            'states': custom_data.STATE_MAP,
            'relationships': custom_dictionaries.NOMINEE_RELATIONSHIP_LIST,
            'financiers': custom_data.FINANCIERS,
        }
        return datamap.get(datatype, None)

    def disable_cache_for_request(self, request_data):
        if (request_data['isNewVehicle'] != '1' and
            datetime.datetime.strptime(
                request_data['pastPolicyExpiryDate'], '%d-%m-%Y'
                ).replace(hour=23, minute=59, second=0) < datetime.datetime.now()):
            return True
        return False

    def process_expired_policy(self, transaction_id):
        if self.Configuration.VEHICLE_TYPE == 'Twowheeler':
            return True
        return False
