from motor_product.models import Vehicle, VehicleMaster, VehicleMapping, Insurer
from motor_product import parser_utils

INSURER_SLUG = 'reliance'
INSURER_MASTER_PATH = 'motor_product/insurers/reliance/scripts/data/reliance_fourwheeler.csv'
MASTER_PATH = 'motor_product/scripts/fourwheeler/data/master_fourwheeler.csv'

csv_parameters = {
    'Make_ID_PK': 0,
    'Make_Name': 1,
    'Model_ID_PK': 2,
    'Model_Name': 3,
    'Variance': 4,
    'CC': 5,
    'Seating_Capacity': 6,
    'FuelType': 7,
    'Master': 8,
}

parameters = {
    'make_code': 0,
    'make': 1,
    'model_code': 2,
    'model': 3,
    'variant': 4,
    'cc': 5,
    'seating_capacity': 6,
    'fuel_type': 7,
    'master': 8,
}

master_parameters = {
    'make': 0,
    'model': 1,
    'variant': 2,
    'fuel_type': 3,
    'cc': 4,
    'seating_capacity': 5,
    'insurer': 13,  # Refer to master csv
}

FUEL_TYPE_MAP = {
    'PETROL': 'Petrol',
    'DIESEL': 'Diesel',
    'PETROL+LPG': 'LPG / Petrol',
    'PETROL+CNG': 'CNG / Petrol',
    'BATTERY OPERATED': 'Electricity',
}


def delete_data():
    Vehicle.objects.filter(vehicle_type='Private Car',
                           insurer__slug=INSURER_SLUG).delete()


def delete_mappings():
    VehicleMapping.objects.filter(
        insurer__slug=INSURER_SLUG, master_vehicle__vehicle_type='Private Car').delete()


def save_data():
    insurer = Insurer.objects.get(slug=INSURER_SLUG)
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_MASTER_PATH, parameters)

    vehicle_objects = []
    for _vehicle in vehicles:
        _vehicle['cc'] = parser_utils.try_parse_into_integer(_vehicle['cc'])

        seating_capacity = parser_utils.try_parse_into_integer(
            _vehicle['seating_capacity'])
        _vehicle['seating_capacity'] = seating_capacity

        _vehicle['insurer'] = insurer

        _vehicle['fuel_type'] = FUEL_TYPE_MAP.get(
            _vehicle['fuel_type'], 'Not Defined')

        _vehicle['vehicle_type'] = 'Private Car'

        del _vehicle['master']

        vehicle = Vehicle(**_vehicle)
        vehicle_objects.append(vehicle)
        print 'Saving vehicle %s' % vehicle.model

    Vehicle.objects.bulk_create(vehicle_objects)


def create_mappings():
    insurer = Insurer.objects.get(slug=INSURER_SLUG)
    master_vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        MASTER_PATH, master_parameters)
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_MASTER_PATH, parameters)

    ivehicles = Vehicle.objects.filter(
        vehicle_type='Private Car', insurer__slug=INSURER_SLUG)

    vehicle_map = {}
    for iv in vehicles:
        _vehicle = ivehicles.get(model_code=iv['model_code'])
        vehicle_map[iv['master']] = _vehicle

    mappings = []
    for mv in master_vehicles:
        _master = VehicleMaster.objects.get(make__name=mv['make'], model__name=mv['model'],
                                            variant=mv['variant'], cc=mv['cc'],
                                            fuel_type=mv['fuel_type'],
                                            seating_capacity=mv['seating_capacity'])
        mapping = VehicleMapping()
        mapping.master_vehicle = _master
        mapping.mapped_vehicle = vehicle_map.get(mv['insurer'], None)
        mapping.insurer = insurer

        if mapping.mapped_vehicle:
            print 'Mapping %s to %s' % (_master, mapping.mapped_vehicle)
            mappings.append(mapping)

    VehicleMapping.objects.bulk_create(mappings)
