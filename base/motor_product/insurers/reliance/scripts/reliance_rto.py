from motor_product.models import RTOInsurer, RTOMapping, Insurer
from motor_product import parser_utils

INSURER_SLUG = 'reliance'
INSURER_RTO_PATH = 'motor_product/insurers/reliance/scripts/data/reliance_rto_data.csv'
parameters = {
    'rto_state': 0,
    'rto_name': 1,
    'rto_code': 2,
}


def delete_data():
    RTOInsurer.objects.filter(insurer__slug=INSURER_SLUG).delete()


def delete_mappings():
    RTOMapping.objects.filter(insurer__slug=INSURER_SLUG).delete()


def save_data():
    rtos = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_RTO_PATH, parameters)

    insurer = Insurer.objects.get(slug=INSURER_SLUG)

    rto_list = []
    rto_dict = {'insurer': insurer}

    for rto_info in rtos:
        del rto_info['raw_data']
        rto_dict.update(rto_info)
        rto_dict.update({'master_rto_state': rto_info['rto_code'][:2]})
        rto = RTOInsurer(**rto_dict)
        print "Saving ---- %s" % rto.rto_code
        rto_list.append(rto)

    RTOInsurer.objects.bulk_create(rto_list)


def create_map():
    pass
