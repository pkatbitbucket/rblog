INSURER_SLUG = 'reliance'

csv_parameters = {
    'Make_ID_PK': 0,
    'Make_Name': 1,
    'Model_ID_PK': 2,
    'Model_Name': 3,
    'Variance': 4,
    'CC': 5,
    'Seating_Capacity': 6,
    'FuelType': 7,
    'Master': 8,
}

parameters = {
    'make_code': 0,
    'make': 1,
    'model_code': 2,
    'model': 3,
    'variant': 4,
    'cc': 5,
    'seating_capacity': 6,
    'fuel_type': 7,
    'master': 8,
}
