import cgi
import copy
import hashlib
import datetime
from dateutil.relativedelta import relativedelta
import json
import logging
import math
import os
from xml.etree import ElementTree
import putils
from django.conf import settings
from lxml import etree
from motor_product import logger_utils, mail_utils, parser_utils
from motor_product.insurers import insurer_utils
from motor_product.models import Insurer, Vehicle, IntegrationStatistics, Transaction
from suds.client import Client
from utils import motor_logger, quote_logger, log_error

from .custom_dictionaries import *

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)

INSURER_SLUG = 'tata-aig'
try:
    INSURER = Insurer.objects.get(slug=INSURER_SLUG)
    INSURER_ID = INSURER.id
    INSURER_NAME = INSURER.title
except:
    INSURER_ID = 0
    INSURER_NAME = ''

suds_logger = logger_utils.getLogger(INSURER_SLUG)

VEHICLE_AGE_LIMIT = 10
FOUR_WHEELER_PRODUCT_CODE = '3121'
AUTHENTICATION_TOKEN = ''
HASH_KEY = 'YmMyZjE4YmYuLi4uLi4uLg=='

# ### UAT CREDENTIALS ###
# CUSTOMER_ID = '6005893283'
# PREMIUM_URL = 'file:///%s' % (os.path.join(settings.SETTINGS_FILE_FOLDER,
#                                            'motor_product/insurers/tata_aig/wsdl/uat/CustomerPortalService.wsdl'))
# PROPOSAL_URL = 'http://uatbeta.tataaiginsurance.in/AggregatorIntermediary/default.aspx'
# POLICY_URL = 'http://uatbeta.tataaiginsurance.in/GCCustomerPortalService/\
# PdfGenerator.ashx?strSource=TATA_AIG&strCampaing=EASYPOL&strMedium=EASYPOL&strHashKey=YmMyZjE4YmYuLi4uLi4uLg==&strPolicyNumber='
# CAMPAIGN = 'EASYPOL'
# INTERMEDIARY_CODE = '0019024000'

# ### PRODUCTION CREDENTIALS ###
CUSTOMER_ID = '6006368121'
# PREMIUM_URL = 'https://iip-portal.tataaiginsurance.in/BridgeService/GCCustomerPortalService?wsdl'
PREMIUM_URL = 'file:///%s' % (os.path.join(settings.SETTINGS_FILE_FOLDER,
                                           'motor_product/insurers/tata_aig/wsdl/production/GCCustomerPortalService.xml'))
PROPOSAL_URL = 'https://iip-portal.tataaiginsurance.in/AggregatorIntermediary/default.aspx'
POLICY_URL = 'https://iip-portal.tataaiginsurance.in/GCCustomerPortal/\
PdfGenerator.ashx?strSource=TATA_AIG&strCampaing=COVERFOX&strMedium=COVERFOX&strPolicyNumber='

CAMPAIGN = 'COVERFOX'
INTERMEDIARY_CODE = '0019024001'


class PremiumCalculator(object):

    def __init__(self):
        self.client = None
        self.xmlGenerator = XMLGenerator()

    def get_client(self, name=None):
        if self.client is None:
            self.client = Client(url=PREMIUM_URL)
            self.client.set_options(cache=None)
            self.client.set_options(
                port='BasicHttpBinding_ICustomerPortalService')

        self.client.set_options(plugins=[suds_logger.set_name(name)])
        return self.client

    def get_premium(self, data_dictionary):
        if math.floor(data_dictionary['vehicleAge']) >= VEHICLE_AGE_LIMIT:
            motor_logger.info(
                'Vehicle age limit not valid for this insurer %s.' % INSURER_SLUG)
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_AGE_RESTRICTED}

        if data_dictionary['isUsedVehicle'] == '1':
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_USED_VEHICLE_RESTRICTED}

        if data_dictionary['isNewVehicle'] == '0':
            past_policy_end_date = datetime.datetime.strptime(data_dictionary['pastPolicyExpiryDate'], "%d-%m-%Y")
            if past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now():
                motor_logger.info('Expired policy not allowed for this insurer %s.' % INSURER_SLUG)
                return {'success': False, 'error_code': IntegrationStatistics.INSURER_EXPIRED_POLICY_RESTRICTED}

        quote_request = self._get_quick_quote_parameters(data_dictionary)

        quote_response = self.get_client(
            data_dictionary['quoteId']).service.QuickQuote(quote_request)

        # raw_response_dict = insurer_utils.recursive_asdict(quote_response)

        response_xml = quote_response['ResponseXML']

        parsed_result = ElementTree.fromstring(response_xml)
        response_dict = parser_utils.complex_etree_to_dict(parsed_result)

        idv = int(float(response_dict['ServiceResult'][
                  'GetUserData']['PropRisks_IDVofthevehicle']))

        if float(data_dictionary['idv']) == 0:
            min_idv = int(0.9 * idv)
            max_idv = int(1.1 * idv)
            data_dictionary['min_idv'] = min_idv
            data_dictionary['max_idv'] = max_idv

            request_idv = float(data_dictionary['request_idv'])
            if request_idv < float(min_idv):
                data_dictionary['idv'] = min_idv
            elif request_idv > float(max_idv):
                data_dictionary['idv'] = max_idv
            else:
                data_dictionary['idv'] = request_idv

            # 50 Lac Idv Limit for tata-aig
            if max_idv >= 5000000:
                return {'success': False, 'error_code': IntegrationStatistics.IDV_LIMIT_EXCEEDED}

            return self.get_premium(data_dictionary)

        # Check if the total premium is non-zero (hack for tata-aig)
        total_premium = int(quote_response['TotalPremium'])
        if total_premium == 0:
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_ZERO_PREMIUM}

        service_tax = quote_response['ServiceTax']

        addon_covers_mapping = {
            'Depreciation reimbursement': {
                'id': 'isDepreciationWaiver',
                'default': '1',
            },
            'No Claim Bonus NCB Protection': {
                'id': 'isNcbProtection',
                'default': '1',
            },
            'Return to Invoice': {
                'id': 'isInvoiceCover',
                'default': '1',
            },
            'Engine Secure': {
                'id': 'isEngineProtector',
                'default': '1',
            },
            'Key Replacement': {
                'id': 'isKeyReplacement',
                'default': '1',
            },
            'Road Side Assistance': {
                'id': 'is247RoadsideAssistance',
                'default': '1',
            }
        }

        basic_covers_mapping = {
            'Own Damage': {
                'id': 'basicOd',
                'default': '1',
            },
            'Basic TP  including TPPD premium': {
                'id': 'basicTp',
                'default': '1',
            },
            'Owner Driver': {
                'id': 'paOwnerDriver',
                'default': '1',
            },
            'Electrical Accessories': {
                'id': 'idvElectrical',
                'default': '1',
            },
            'CNGKit': {
                'id': 'cngKit',
                'default': '1',
            },
            'CNG Kit TP': {
                'id': 'cngKit',
                'default': '1',
            },
            'LegalLiabilityforpaiddrivercleanerconductor': {
                'id': 'legalLiabilityDriver',
                'default': '0',
            },
            'UnnamedPAcover': {
                'id': 'paPassenger',
                'default': '1',
            },
            'Repair Glass Fiber plastic': {
                'id': 'repairGlassFiberPlastic',
                'default': '1',
            },
            'WorkMen Compensation': {
                'id': 'workmenCompensation',
                'default': '1',
            }
        }

        discounts_mapping = {
            'No Claim Bonus': {
                'id': 'ncb',
                'default': '1',
            }
        }

        special_discounts_mapping = {
            'Voluntary Excess': {
                'id': 'voluntaryDeductible',
                'default': '1',
            }
        }

        basic_covers = []
        addon_covers = []
        discounts = []
        special_discounts = []

        all_covers = []

        risks = response_dict['ServiceResult'][
            'GetUserData']['PropRisks_Col']['Risks']

        if type(risks) is dict:
            risks = [risks]

        cover_details_column = risks[0][
            'PropRisks_CoverDetails_Col']['Risks_CoverDetails']

        if type(cover_details_column) is dict:
            cover_details_column = [cover_details_column]

        for raw_cover in cover_details_column:
            cover = {
                'id': raw_cover['PropCoverDetails_CoverGroups'],
                'premium': round(float(raw_cover['PropCoverDetails_Premium']), 2),
                # 'sumInsured' : round(float(raw_cover['PropCoverDetails_SumInsured']), 2),
            }
            all_covers.append(copy.deepcopy(cover))

        if len(risks) > 1:
            for risk in risks[1:]:
                cover = {
                    'id': risk['PropRisks_VehicleSIComponent'],
                    'premium': round(float(risk['PropRisks_Premium']), 2),
                    # 'sumInsured' : round(float(risk['PropRisks_SumInsured']), 2),
                }
                all_covers.append(copy.deepcopy(cover))

        discounts_column = response_dict['ServiceResult']['GetUserData']

        if discounts_column.get('PropLoadingDiscount_Col', None):
            discount_elements = discounts_column[
                'PropLoadingDiscount_Col']['LoadingDiscount']

            if type(discount_elements) is dict:
                discount_elements = [discount_elements]

            for discount in discount_elements:
                cover = {
                    'id': discount['PropLoadingDiscount_Description'],
                    'premium': round((-1) * float(discount['PropLoadingDiscount_CalculatedAmount']), 2),
                    # 'sumInsured' : round(float(discount['PropLoadingDiscount_SumInsured']), 2),
                }
                all_covers.append(copy.deepcopy(cover))

        for cover in all_covers:
            mapping = None
            cover_list = None
            if cover['id'] in addon_covers_mapping:
                mapping = addon_covers_mapping
                cover_list = addon_covers
            elif cover['id'] in basic_covers_mapping:
                mapping = basic_covers_mapping
                cover_list = basic_covers
            elif cover['id'] in discounts_mapping:
                mapping = discounts_mapping
                cover_list = discounts
            elif cover['id'] in special_discounts_mapping:
                mapping = special_discounts_mapping
                cover_list = special_discounts
            else:
                print 'Extra cover with id: ' + cover['id']

            if mapping:
                actual_cover = mapping[cover['id']]
                actual_cover['premium'] = cover['premium']
                cover_list.append(copy.deepcopy(actual_cover))

        vehicle = Vehicle.objects.get(id=data_dictionary['vehicleId'])

        result_data = {
            'insurerId': INSURER_ID,
            'insurerSlug': INSURER_SLUG,
            'insurerName': INSURER_NAME,
            'isInstantPolicy': True,
            'minIDV': int(math.ceil(data_dictionary['min_idv'])),
            'maxIDV': int(math.floor(data_dictionary['max_idv'])),
            'calculatedAtIDV': int(math.floor(data_dictionary['idv'])),
            'serviceTaxRate': 0.145,
            'premiumBreakup': {
                'serviceTax': round(float(service_tax), 2),
                'basicCovers': insurer_utils.cpr_update(basic_covers),
                'addonCovers': insurer_utils.cpr_update(addon_covers),
                'discounts': insurer_utils.cpr_update(discounts),
                'specialDiscounts': insurer_utils.cpr_update(special_discounts),
                'dependentCovers': get_dependent_addons(),
                'paPassengerAmounts': insurer_utils.get_pa_passenger_dict(vehicle.vehicle_type, vehicle.seating_capacity),
            },
        }, {'total_premium': total_premium}
        return {'result': result_data, 'success': True}

    def _get_quick_quote_parameters(self, data_dictionary):

        form_details = {
            'id': CUSTOMER_ID,
            'name': 'RAVI  MOHNANI',
            'firstname': 'Test',
            'lastname': 'Test',
            'gender': 'MALE',
            'marital_status': 'SINGLE',
            'chassis_number': 'C34534535',
            'engine_no': 'E34535354',
            'nominee_name': 'TestNominee',
            'nominee_relationship': 'Brother',
            'nominee_age': '21',
            'appointee_name': '',
            'appointee_relationship': '',
            'age': '34',
            'licensed_year': str(datetime.datetime.now().year),
            'pincode': '0',
            'city': '',
            'city_code': '',
            'state': '',
            'state_code': '',
            'district': '',
            'district_code': '',
            'registration_pincode': '0',
            'registration_city': '',
            'registration_city_code': '',
            'registration_state': '',
            'registration_state_code': '',
            'registration_district': '',
            'registration_district_code': '',
            'registration_address_1': '',
            'registration_address_2': '',
            'registration_address_3': '',
            'business_type': 'New Business',
            'past_policy_number': 'ICI808080',
            'past_policy_insurer': 'ICICI',
            'past_policy_address': 'Insurer Address',
            'past_policy_city': '',
            'past_policy_state': '',
            'past_policy_type': 'PackageComprehensive',
            'number_of_claims': data_dictionary['isClaimedLastYear'],
            'claim_amount': '10000' if data_dictionary['isClaimedLastYear'] == '1' else '0',
            'registration_number': data_dictionary['registrationNumber[]'][0:2] + ['DA', '1111']
        }

        input_xml = cgi.escape(self.xmlGenerator._get_root(
            data_dictionary, form_details, True))

        quote_request = {
            'ProductCode': FOUR_WHEELER_PRODUCT_CODE,
            'AuthenticationToken': AUTHENTICATION_TOKEN,
            'CIAName': None,
            'Campaign': CAMPAIGN,
            'HashKey': HASH_KEY,
            'HostAddress': None,
            'Medium': CAMPAIGN,
            'ServiceConsumer': None,
            'Source': 'TATA_AIG',
            'InputXML': None,
            'IsBankDataRequired': False,
            'IsCutomerAddressRequired': False,
            'IsFinanciarDataRequired': False,
            'IsManufacturerMappingRequired': False,
            'IsModelMappingRequired': False,
            'IsRTOMappingRequired': False,
            'ModeOfOperation': None,
            'OldProposalNumber': None,
            'QuotationNumber': None,
            'QuotationVersion': None,
            'UserId': None,
            'UserRole': None,
            'VehicleClassCode': None,
        }

        quote_request['InputXML'] = str(input_xml)
        return quote_request


class ProposalGenerator(object):

    def __init__(self):
        self.xmlGenerator = XMLGenerator()

    def save_policy_details(self, data_dictionary, transaction):
        transaction.raw['policy_details'] = data_dictionary
        transaction.premium_paid = transaction.raw[
            'quote_raw_response']['total_premium']
        transaction.save()
        self.get_proposal(transaction)
        return True

    def get_proposal(self, transaction):

        data_dictionary = transaction.quote.raw_data
        fd = transaction.raw['policy_details']

        location = PINCODE_TO_LOCATION_INFO[fd['add_pincode']]
        state_code = location['state']
        state_name = STATE_LIST[state_code]

        city_code = location['city']
        city_name = CITY_CODE_TO_CITY_INFO[city_code]['name']

        district_code = location['district']
        district_name = CITY_CODE_TO_CITY_INFO[district_code]['name']

        reg_location = PINCODE_TO_LOCATION_INFO[fd['reg_add_pincode']]
        reg_state_code = reg_location['state']
        reg_state_name = STATE_LIST[reg_state_code]

        reg_city_code = reg_location['city']
        reg_city_name = CITY_CODE_TO_CITY_INFO[reg_city_code]['name']

        reg_district_code = reg_location['district']
        reg_district_name = CITY_CODE_TO_CITY_INFO[reg_district_code]['name']

        salutation = 'Mr.'
        if fd['cust_gender'] == 'Female':
            salutation = 'Ms.' if fd['cust_marital_status'] == '0' else 'Mrs.'

        if fd.get('past_policy_insurer', ''):
            past_policy_insurer_code = REV_PASTINSURER_MAP[fd['past_policy_insurer']]
        else:
            past_policy_insurer_code = ''

        # DL *C fix
        is_new_vehicle = data_dictionary['isNewVehicle'] == '1'
        if is_new_vehicle is False and fd['vehicle_reg_no'].split(
            '-')[0].upper() == 'DL' and fd['vehicle_reg_no'].split(
                '-')[1][-1].upper() == 'C':
            _nl = fd['vehicle_reg_no'].split('-')
            _nl[1], _nl[2] = _nl[1][:-1], _nl[1][-1]+_nl[2]
            if len(_nl[1]) == 1:
                _nl[1] = '0' + _nl[1]
            fd['vehicle_reg_no'] = '-'.join(_nl)

        form_details = {
            'id': '',
            'name': fd['cust_first_name'] + ' ' + fd['cust_last_name'],
            'firstname': fd['cust_first_name'],
            'lastname': fd['cust_last_name'],
            'gender': GENDER_MAPPING[fd['cust_gender']],
            'marital_status': 'SINGLE' if fd['cust_marital_status'] == '0' else 'MARRIED',
            'chassis_number': fd['vehicle_chassis_no'],
            'engine_no': fd['vehicle_engine_no'],
            'nominee_name': fd['nominee_name'],
            'nominee_relationship': RELATIONSHIP_MAPPING[fd['nominee_relationship']],
            'nominee_age': fd['nominee_age'],
            'appointee_name': '',
            'appointee_relationship': '',
            'age': '34',  # TODO
            'licensed_year': str(datetime.datetime.now().year),
            'business_type': 'New Business',
            'registration_address_1': fd['reg_add_house_no'] + ' ' + fd['reg_add_building_name'],
            'registration_address_2': fd['reg_add_street_name'] + ' ' + fd['reg_add_landmark'],
            'registration_address_3': fd['reg_add_district'] + ' ' + fd['reg_add_state'] + ' ' + fd['reg_add_pincode'],
            'state': state_name,
            'state_code': str(state_code),
            'registration_state': reg_state_name,
            'registration_state_code': str(reg_state_code),
            'district': district_name,
            'district_code': district_code,
            'registration_district': reg_district_name,
            'registration_district_code': reg_district_code,
            'dob': datetime.datetime.strptime(fd['cust_dob'], "%d-%m-%Y").strftime("%d/%m/%Y"),
            'salutation': SALUTATION_MAPPING[salutation],
            'landmark': '',
            'pan': fd['cust_pan'],
            'address1': fd['add_house_no'] + ' ' + fd['add_building_name'],
            'address2': fd['add_street_name'] + ' ' + fd['add_landmark'],
            'city': city_name,
            'city_code': city_code,
            'registration_city': reg_city_name,
            'registration_city_code': reg_city_code,
            'email': fd['cust_email'],
            'phone_no': fd['cust_phone'],
            'pincode': fd['add_pincode'],
            'registration_pincode': fd['reg_add_pincode'],
            'postoffice': city_name,
            'occupation_code': '23',  # Sending other as by default occupation
            'financier_code': '' if fd['is_car_financed'] == '0' else fd['car-financier'],
            'past_policy_number': fd.get('past_policy_number', ''),
            # 'past_policy_insurer': fd.get('past_policy_insurer', ''),
            'past_policy_insurer': past_policy_insurer_code,
            'past_policy_address': city_name,
            'past_policy_state': state_name,
            'past_policy_city': city_name,
            'past_policy_type': 'PackageComprehensive',
            'number_of_claims': '0' if data_dictionary['isClaimedLastYear'] == '0' else fd['past_policy_claims'],
            'claim_amount': '0' if data_dictionary['isClaimedLastYear'] == '0' else fd['past_policy_claims_amount'],
            'registration_number': fd['vehicle_reg_no'].split('-'),
        }

        data_dictionary['vehicleId'] = fd['vehicleId']

        rto_code = "-".join(data_dictionary['registrationNumber[]'][0:2])
        data_dictionary[
            'cityPrimarilyUsed'] = RTO_TO_CITY_PRIMARILY_USED[rto_code]

        data_dictionary['idv'] = fd['idv']

        if data_dictionary['isNewVehicle'] == '1':
            form_details['registration_number'] = data_dictionary[
                'registrationNumber[]']

        policy_request = self.xmlGenerator._get_root(
            data_dictionary, form_details, False)
        policy_request = str(policy_request).replace('\n', '')

        suds_logger.set_name(transaction.transaction_id).log(policy_request)

        processed_policy_request = policy_request.encode('hex')
        return processed_policy_request


def convert_premium_request_data_to_required_dictionary(vehicle_id, request_data):
    rquest_data = copy.deepcopy(request_data)
    rquest_data['vehicleId'] = vehicle_id
    rquest_data['request_idv'] = rquest_data['idv']
    rquest_data['idv'] = '0'

    rto_code = "-".join(rquest_data['registrationNumber[]'][0:2])
    rquest_data['cityPrimarilyUsed'] = RTO_TO_CITY_PRIMARILY_USED[rto_code]

    if rquest_data['isNewVehicle'] == '1':
        vehicle_age = 0
    else:
        date_of_registration = datetime.datetime.strptime(
            rquest_data['registrationDate'], '%d-%m-%Y')
        past_policy_end_date = datetime.datetime.strptime(
            rquest_data['pastPolicyExpiryDate'], "%d-%m-%Y")
        new_policy_start_date = past_policy_end_date.replace(
            microsecond=0) + datetime.timedelta(days=1)
        vehicle_age = insurer_utils.get_age(
            new_policy_start_date, date_of_registration)

    rquest_data['vehicleAge'] = vehicle_age
    return rquest_data


def get_buy_form_details(quote_parameters, transaction):

    split_registration_no = quote_parameters.get('registrationNumber[]')

    is_new_vehicle = True if quote_parameters['isNewVehicle'] == '1' else False
    if is_new_vehicle:
        registration_number = 'NEW'
    else:
        registration_number = '-'.join(split_registration_no)

    master_rto_code = "%s-%s" % (
        split_registration_no[0], split_registration_no[1])

    data = {
        'registration_no': registration_number,
        'rto_location_info': master_rto_code,
        # 'insurers_list': PAST_INSURANCE_ID_LIST,
        'insurers_list': PAST_INSURER_ID_LIST_MASTER,
        'disallowed_insurers': DISALLOWED_PAST_INSURERS,
        # 'occupation_list': OCCUPATION_MAP,
        'financier_list': FINANCIER_LIST,
        'relationship_list': RELATIONSHIP_MAPPING.keys(),
        'is_cities_fetch': False,
    }
    return data


def convert_proposal_request_data_to_required_dictionary(vehicle, request_data,
                                                         quote_parameters, quote_response,
                                                         quote_custom_response):
    request_data_dictionary = copy.deepcopy(request_data)

    if quote_parameters['isNewVehicle'] == '1':
        request_data['vehicle_reg_no'] = 'NEW'

    if request_data['add-same'] == '1':
        request_data_dictionary['reg_add_house_no'] = request_data['add_house_no']
        request_data_dictionary['reg_add_building_name'] = request_data['add_building_name']
        request_data_dictionary['reg_add_street_name'] = request_data['add_street_name']
        request_data_dictionary['reg_add_landmark'] = request_data['add_landmark']
        request_data_dictionary['reg_add_state'] = request_data['add_state']
        request_data_dictionary['reg_add_district'] = request_data['add_district']
        request_data_dictionary['reg_add_pincode'] = request_data['add_pincode']

    request_data_dictionary['vehicleId'] = vehicle.id
    request_data_dictionary['idv'] = quote_custom_response['calculatedAtIDV']
    return request_data_dictionary


def update_transaction_details(transaction, data):
        is_new_vehicle = data['isNewVehicle'] == '1'
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                data['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

        new_policy_end_date = putils.add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)

        transaction.policy_start_date = new_policy_start_date
        transaction.policy_end_date = new_policy_end_date
        transaction.save()


def get_transaction_from_id(transaction_id):
    return Transaction.objects.get(transaction_id=transaction_id)


def get_payment_data(transaction):

    # response_url = 'http://uat.coverfox.com/motor/insurer/tata-aig/response/%s/' % transaction.transaction_id
    response_url = '%s/motor/fourwheeler/insurer/%s/payment/response/%s/' % (
        settings.SITE_URL, INSURER_SLUG, transaction.transaction_id)
    post_xml_data = ProposalGenerator.get_proposal(transaction)

    payment_gateway_data = {
        'ReturnURL': response_url,
        'c': post_xml_data,
    }
    transaction.proposal_request = post_xml_data
    transaction.payment_request = payment_gateway_data
    transaction.save()
    return payment_gateway_data, PROPOSAL_URL


def post_payment(payment_response, transaction):
    motor_logger.info(payment_response)
    suds_logger.set_name(transaction.transaction_id).log(json.dumps(payment_response))
    hexString = payment_response['POST']['d']
    responseString = hexString.decode('hex')

    redirect_url = '/motor/fourwheeler/payment/failure/%s/' % transaction.transaction_id
    is_redirect = True

    parsed_result = ElementTree.fromstring(responseString)
    response_dict = parser_utils.complex_etree_to_dict(parsed_result)
    transaction.payment_response = payment_response
    transaction.proposal_response = payment_response
    quote_logger.info(json.dumps({
        'type': 'payment_response',
        'insurer': INSURER_SLUG,
        'data': response_dict,
    }))

    error_code = response_dict['Root']['AdditionalInfo']['ErrorCode']['@Value']
    error_text = response_dict['Root']['AdditionalInfo']['ErrorText']['@Value']
    policy_no = response_dict['Root']['AdditionalInfo']['PolicyNo']['@Value']

    if policy_no != '':
        policy_url = POLICY_URL + str(policy_no)
        transaction.insured_details_json['policy_url'] = policy_url
        transaction.insured_details_json['payment_response'] = response_dict
        transaction.policy_number = policy_no
        transaction.payment_done = True
        transaction.payment_on = datetime.datetime.now()
        transaction.mark_complete()
        redirect_url = '/motor/fourwheeler/payment/success/%s/' % transaction.transaction_id

        mail_utils.send_mail_for_admin({
            'title': 'Policy url for tata-aig',
            'type': 'policy',
            'data': 'Policy url for transaction %s (CFOX-%s) is %s' % (transaction.transaction_id,
                                                                       transaction.id,
                                                                       transaction.insured_details_json['policy_url'])
        })
    else:
        # Proposal failure response mail
        mail_utils.send_mail_for_proposal_failure(transaction, responseString)

        transaction.status = 'FAILED'
        transaction.save()

    return is_redirect, redirect_url, '', {}


def get_pincode_details(pincode):
    try:
        location = PINCODE_TO_LOCATION_INFO[pincode]
        district_name = CITY_CODE_TO_CITY_INFO[location['district']]['name']
        province_name = STATE_LIST[location['state']]
    except KeyError:
        return None
    else:
        if district_name is None or province_name is None:
            return None

    # city_code = location['city']
    # city_name = CITY_CODE_TO_CITY_INFO[city_code]['name']
    # post_office_details = [{"Post_Office_Id" : city_code, "Post_Office_Name": city_name}]
    return {'pincode': pincode, 'district_name': district_name, 'province_name': province_name}


def get_dependent_addons(parameters=None):
    return {'type': 'dynamic'}


def get_custom_data(datatype):
    datamap = {
        'occupations': OCCUPATION_MAP,
        # 'insurers': PAST_INSURANCE_ID_LIST,
        'insurers': PAST_INSURER_ID_LIST_MASTER,
        'financiers': FINANCIER_LIST,
        'relationships': RELATIONSHIP_MAPPING.keys(),
    }
    return datamap.get(datatype, None)


def disable_cache_for_request(request_data):
    if (request_data['isNewVehicle'] != '1' and
        datetime.datetime.strptime(
            request_data['pastPolicyExpiryDate'], '%d-%m-%Y'
            ).replace(hour=23, minute=59, second=0) < datetime.datetime.now()):
        return True
    return False


class XMLGenerator(object):

    def _get_general_proposal_information(self, data_dictionary, customer, proposal_info):

        distribution_channel = {
            'type': 'BROKER CORPORATE',
            'source': 'INTERMEDIARY',
            'commision': '10',
            'service_provider': '10',
        }

        intermediary = {
            'code': INTERMEDIARY_CODE,
            'name': 'Coverfox Insurance Broking Pvt Ltd',
            'type': 'Non-DM',
        }

        # TODO
        office_city = data_dictionary['cityPrimarilyUsed']
        office_code = CITY_PRIMARILY_USED_MAP[office_city]['BranchCode']
        branch_office_code = '9' + office_code

        proposal_information = {
            'business_type': proposal_info['business_type'],
            'department_code': '31',
            'branch_office_code': branch_office_code,
            'display_office_code': office_code,
            'office_code': office_code,
            'office_name': office_city,
            'method_of_calculation': 'sheetal',
            'option_for_calculation': 'Yearly',
            'sector': 'Urban',
        }

        policy_effective_date = {
            'from_date': proposal_info['policy_start_date'],
            'from_hour': '00:00',
            'to_date': proposal_info['policy_end_date'],
            'to_hour': '23:59',
        }

        application_date = datetime.datetime.now().strftime("%d/%m/%Y")  # TODO

        proposal_date = datetime.datetime.now().strftime("%d/%m/%Y")    # TODO
        tertiary_mo_code = 'WEBSERVICE'

        GeneralProposalInformation = {
            'Children': {
                'ErrorTrackingNeeded': {'Type': 'Boolean', 'Value': 'False'},
                'ProductType': {'Type': 'String', 'Value': ''},
                'PropBranchDetails_IMDBranchCode': {'Type': 'String', 'Value': ''},
                'PropBranchDetails_IMDBranchName': {'Type': 'String', 'Value': ''},
                'PropCalculation_CalculateRate': {'Type': 'Boolean', 'Value': 'False'},
                'PropCalculation_Validate': {'Type': 'Boolean', 'Value': 'False'},
                'PropCalculation_ValidateData': {'Type': 'Boolean', 'Value': 'False'},
                'PropClauseDetails_Component51': {'Type': 'String', 'Value': ''},
                'PropClauseDetails_DepartmentCode_Mandatary': {'Type': 'Double', 'Value': '0'},
                'PropClauseDetails_ProductCode_Mandatary': {'Type': 'Double', 'Value': '0'},
                'PropClauseDetails_SectionCode': {'Type': 'String', 'Value': ''},
                'PropCoinsuranceDetails_AddRow': {'Type': 'Boolean', 'Value': 'False'},
                'PropCoinsuranceDetails_CoinsuranceType': {'Type': 'String', 'Value': ''},
                'PropCoinsuranceDetails_Commissiontobepaidbytheleader': {'Type': 'Boolean', 'Value': 'False'},
                'PropCoinsuranceDetails_PolicyNooftheleader': {'Type': 'String', 'Value': ''},
                'PropCoinsuranceDetails_ReferenceNumber': {'Type': 'String', 'Value': ''},
                'PropCoinsuranceDetails_SERVTAXCOMMRCVRLEADER': {'Type': 'Boolean', 'Value': 'False'},
                'PropCoinsuranceDetails_Servicetaxtobepaidbytheleader': {'Type': 'Boolean', 'Value': 'False'},
                'PropCoinsuranceDetails_Validate': {'Type': 'String', 'Value': ''},
                'PropConditionDetails_ConditionDescription': {'Type': 'String', 'Value': ''},
                'PropConditionDetails_DepartmentCode': {'Type': 'Double', 'Value': '0'},
                'PropConditionDetails_ProductCode': {'Type': 'Double', 'Value': '0'},
                'PropConditionDetails_SectionCode': {'Type': 'String', 'Value': ''},
                'PropConditionDetails_SpecialConditionDescription': {'Type': 'String', 'Value': ''},
                'PropCoverNoteDetails_CoverNoteBookNo': {'Type': 'String', 'Value': ''},
                'PropCoverNoteDetails_CoverNoteLeafNo': {'Type': 'String', 'Value': ''},
                'PropCoverNoteDetails_IssuedOnDt': {'Type': 'String', 'Value': ''},
                'PropCoverNoteDetails_IsuedonTime': {'Type': 'String', 'Value': ''},
                'PropCoverNoteDetails_LeafNo': {'Type': 'Double', 'Value': '0'},
                'PropCoverNoteDetails_RecievedOn': {'Type': 'String', 'Value': ''},
                'PropCoverNoteDetails_Validate': {'Type': 'Boolean', 'Value': 'False'},
                'PropCustomerDtls_CustomerID_Mandatary': {'Type': 'String', 'Value': customer['id']},
                'PropCustomerDtls_CustomerName': {'Type': 'String', 'Value': customer['name']},
                'PropCustomerDtls_CustomerType': {'Type': 'String', 'Value': ''},
                'PropCustomerDtls_PayeeCustomerID': {'Type': 'String', 'Value': ''},
                'PropCustomerDtls_PayeeCustomerName': {'Type': 'String', 'Value': ''},
                'PropCustomerReferenceInfo_OldPolicyNumber': {'Type': 'String', 'Value': ''},
                'PropCustomerReferenceInfo_PolicyConversionDate': {'Type': 'String', 'Value': ''},
                'PropDistributionChannel_BranchDetails': {'Type': 'String', 'Value': ''},
                'PropDistributionChannel_BusineeChannelBrunch': {'Type': 'String', 'Value': ''},
                'PropDistributionChannel_BusineeChanneltype': {'Type': 'String', 'Value': distribution_channel['type']},
                'PropDistributionChannel_BusinessServicingChannelType': {'Type': 'String', 'Value': ''},
                'PropDistributionChannel_BusinessSource_Mandatary': {'Type': 'String', 'Value': distribution_channel['source']},
                'PropDistributionChannel_BusinessSourcetype': {'Type': 'String', 'Value': ''},
                'PropDistributionChannel_Commision': {'Type': 'Double', 'Value': distribution_channel['commision']},
                'PropDistributionChannel_EndorsementDtls': {'Type': 'String', 'Value': ''},
                'PropDistributionChannel_IntermediaryDetails': {'Type': 'String', 'Value': ''},
                'PropDistributionChannel_MODetails': {'Type': 'String', 'Value': ''},
                'PropDistributionChannel_OperationMode': {'Type': 'String', 'Value': ''},
                'PropDistributionChannel_SPDetails': {'Type': 'String', 'Value': ''},
                'PropDistributionChannel_SerIntermediaryDetails': {'Type': 'String', 'Value': ''},
                'PropDistributionChannel_ServiceProvider': {'Type': 'Double', 'Value': distribution_channel['service_provider']},
                'PropDistributionChannel_SpecialDiscount': {'Type': 'Double', 'Value': '0'},
                'PropDistributionChannel_Validate': {'Type': 'Boolean', 'Value': 'False'},
                'PropDistributionChannel_VerticalDtls': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_CancelDueToClaim': {'Type': 'Boolean', 'Value': 'False'},
                'PropEndorsementDtls_CancellationOption': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_CancellationReason': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_CertificateofVintageCar': {'Type': 'Boolean', 'Value': 'False'},
                'PropEndorsementDtls_CoverType': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_DataEntryError': {'Type': 'Boolean', 'Value': 'p'},
                'PropEndorsementDtls_DateofEndfrominsured': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_Description': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_DocForCommPrivate': {'Type': 'Boolean', 'Value': 'False'},
                'PropEndorsementDtls_DocOfRequisition': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_DocProofothersspecify': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_DocproofforNCB': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_DoubleINSOption': {'Type': 'String', 'Value': 'Automatic'},
                'PropEndorsementDtls_EndorsementTypeCode': {'Type': 'Double', 'Value': '0'},
                'PropEndorsementDtls_INSCoName': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_INSCoOffCodeAdd': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_IsSellerInsured': {'Type': 'Boolean', 'Value': 'False'},
                'PropEndorsementDtls_LaidUpFromDate': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_LaidUpToDate': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_LaidupVehicleCheck': {'Type': 'Boolean', 'Value': 'False'},
                'PropEndorsementDtls_MajorRepairRenovation': {'Type': 'Boolean', 'Value': 'False'},
                'PropEndorsementDtls_NOCFinancier': {'Type': 'Boolean', 'Value': 'False'},
                'PropEndorsementDtls_PolicyFrom': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_PolicyIssueDate': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_PolicyNo': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_PolicyTo': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_Reasonfornametransfer': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_TypeOfTransfer': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_TypeofEndorsement': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_UFormFromDate': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_UFormToDate': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_UsageinInsuredPremises': {'Type': 'Boolean', 'Value': 'False'},
                'PropEndorsementDtls_ValuationReport': {'Type': 'Boolean', 'Value': 'False'},
                'PropEndorsementDtls_VehicleLaidUptype': {'Type': 'String', 'Value': ''},
                'PropEndorsementDtls_orgcertificatesurrendered': {'Type': 'Boolean', 'Value': 'False'},
                'PropEndorsementEffectiveDate_EffectiveDate': {'Type': 'String', 'Value': ''},
                'PropEndorsementEffectiveDate_EffectiveTime': {'Type': 'String', 'Value': ''},
                'PropExclusionDetails_DepartmentCode': {'Type': 'Double', 'Value': '0'},
                'PropExclusionDetails_ProductCode': {'Type': 'Double', 'Value': '0'},
                'PropExclusionDetails_SectionCode': {'Type': 'String', 'Value': ''},
                'PropFinancierDetails_AddRow': {'Type': 'Boolean', 'Value': 'False'},
                'PropFinancierDetails_Validate': {'Type': 'Boolean', 'Value': 'False'},
                'PropGeneralNodes_ApplicationDate': {'Type': 'String', 'Value': application_date},
                'PropGeneralNodes_ApplicationNo': {'Type': 'String', 'Value': ''},
                'PropGeneralNodes_ParentCompanyName': {'Type': 'String', 'Value': ''},
                'PropGeneralNodes_RIInwardSharePercentage': {'Type': 'Double', 'Value': '0'},
                'PropGeneralProposalGroup_CoverNoteDetails': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalGroup_DistributionChannel': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalGroup_GeneralProposalInformation': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_ActiveFlag': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_ApplicationNumber': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_BankID': {'Type': 'Double', 'Value': '0'},
                'PropGeneralProposalInformation_BranchOfficeCode': {'Type': 'String',
                                                                    'Value': proposal_information['branch_office_code']},
                'PropGeneralProposalInformation_BusinessSourceInfo': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_BusinessType_Mandatary': {'Type': 'String',
                                                                          'Value': proposal_information['business_type']},
                'PropGeneralProposalInformation_CertificateNumber': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_ChannelNumber': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_CoverNotePlace': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_CovernoteGenType': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_CustomerDtls': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_CustomerReferenceInfo': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_DealId': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_DepartmentCode': {'Type': 'Double',
                                                                  'Value': proposal_information['department_code']},
                'PropGeneralProposalInformation_DepartmentName': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_DisplayOfficeCode': {'Type': 'String',
                                                                     'Value': proposal_information['display_office_code']},
                'PropGeneralProposalInformation_EmployeeName': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_EndorsementDescription': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_EndorsementEffectiveDate': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_EndorsementNo': {'Type': 'Double', 'Value': '0'},
                'PropGeneralProposalInformation_EndorsementRequestType': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_EndorsementRequestTypeCode': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_EndorsementSi': {'Type': 'Double', 'Value': '0'},
                'PropGeneralProposalInformation_EndorsementType': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_EndorsementWording': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_IndustryCode': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_InwardNo': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_IsNCBApplicable': {'Type': 'Boolean', 'Value': 'False'},
                'PropGeneralProposalInformation_ManualCovernoteNo': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_MasterPolicy': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_MethodOfCalculation': {'Type': 'String',
                                                                       'Value': proposal_information['method_of_calculation']},
                'PropGeneralProposalInformation_NCBpercentage': {'Type': 'Double', 'Value': '0'},
                'PropGeneralProposalInformation_NextB': {'Type': 'Boolean', 'Value': 'False'},
                'PropGeneralProposalInformation_NoPrevInsuranceFlag': {'Type': 'Boolean', 'Value': 'False'},
                'PropGeneralProposalInformation_NonLiablePeriod': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_NumCovernoteNo': {'Type': 'Double', 'Value': '0'},
                'PropGeneralProposalInformation_OfficeCode': {'Type': 'String', 'Value': proposal_information['office_code']},
                'PropGeneralProposalInformation_OfficeName': {'Type': 'String', 'Value': proposal_information['office_name']},
                'PropGeneralProposalInformation_OldCovernoteNo': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_OptionForCalculation': {'Type': 'String',
                                                                        'Value': proposal_information['option_for_calculation']},
                'PropGeneralProposalInformation_PolicyEffectivedate': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_PolicyNo': {'Type': 'Double', 'Value': '0'},
                'PropGeneralProposalInformation_PolicyNumberChar': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_PolicySchedule_Mandatary': {'Type': 'String', 'Value': 'Yes'},
                'PropGeneralProposalInformation_PolicyTerm': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_ProductCode': {'Type': 'Double', 'Value': FOUR_WHEELER_PRODUCT_CODE},
                'PropGeneralProposalInformation_ProductName': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_ProposalDate_Mandatary': {'Type': 'Date', 'Value': proposal_date},
                'PropGeneralProposalInformation_ProposalFormNumber': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_ProvisionalBooking': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_ProvisionalBookingReason': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_QuickId': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_QuoteNumber': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_ReInsuranceInward': {'Type': 'String', 'Value': 'No'},
                'PropGeneralProposalInformation_ReferenceNoDate': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_ReferralCode': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_RelationshipType': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_Remarks': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_RetainCancellationPremium': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_SalesTax': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_Sector_Mandatary': {'Type': 'String', 'Value': proposal_information['sector']},
                'PropGeneralProposalInformation_SerDealId': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_ServiceTaxExemptionCategory_Mandatary': {'Type': 'String',
                                                                                         'Value': 'No Exemption'},
                'PropGeneralProposalInformation_TotalSi': {'Type': 'Double', 'Value': '0'},
                'PropGeneralProposalInformation_TypeOfBusiness': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_TypeOfCalculation': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_TypeOfIndustry': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_TypeOfPolicy': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_UWLocation': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_UWLocationCode': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_ValidationFlag': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_VehicleLaidUpFrom': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_VehicleLaidUpTo': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_covernoteissuedate': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_covernoteissuetime': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_disprntoffcd': {'Type': 'String', 'Value': ''},
                'PropGeneralProposalInformation_iscovernoteused': {'Type': 'Boolean', 'Value': 'False'},
                'PropGeneralProposal_GeneralProposalGroup': {'Type': 'String', 'Value': ''},
                'PropGeneralProposal_IsCustomWorkflowCondition': {'Type': 'String', 'Value': ''},
                'PropHigherExcessDiscount_HE_Disc_RateType_In': {'Type': 'Double', 'Value': '0'},
                'PropIntermediaryDetails_IntermediaryCode': {'Type': 'String', 'Value': intermediary['code']},
                'PropIntermediaryDetails_IntermediaryName': {'Type': 'String', 'Value': intermediary['name']},
                'PropIntermediaryDetails_IntermediaryType': {'Type': 'String', 'Value': intermediary['type']},
                'PropHigherExcessDiscount_HE_Disc_RateType_In': {'Type': 'Double', 'Value': '0'},
                'PropMODetails_PrimaryMOCode': {'Type': 'String', 'Value': ''},
                'PropMODetails_PrimaryMOName': {'Type': 'String', 'Value': ''},
                'PropMODetails_SecondaryMOCode': {'Type': 'String', 'Value': ''},
                'PropMODetails_SecondaryMOName': {'Type': 'String', 'Value': ''},
                'PropMODetails_TertiaryMOCode': {'Type': 'String', 'Value': tertiary_mo_code},
                'PropMODetails_TertiaryMOName': {'Type': 'String', 'Value': ''},
                'PropMotorOtherNodes_AfterFetch': {'Type': 'String', 'Value': ''},
                'PropMotorOtherNodes_CancelationDetail': {'Type': 'String', 'Value': ''},
                'PropMotorOtherNodes_EndorsementDtls': {'Type': 'String', 'Value': ''},
                'PropMotorOtherNodes_GeneralNodes': {'Type': 'String', 'Value': ''},
                'PropMotorOtherNodes_validate_aftercalc': {'Type': 'String', 'Value': ''},
                'PropMotorOtherNodes_validate_beforecalc': {'Type': 'String', 'Value': ''},
                'PropNonLiablePeriod_NonLiableEndTime': {'Type': 'String', 'Value': ''},
                'PropNonLiablePeriod_NonLiableFromDate': {'Type': 'String', 'Value': ''},
                'PropNonLiablePeriod_NonLiableStartTime': {'Type': 'String', 'Value': ''},
                'PropNonLiablePeriod_NonLiableToDate': {'Type': 'String', 'Value': ''},
                'PropParameters_AddClause': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_AddCondition': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_AddExclusion': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_AddWarranty': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_AfterFetch': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_ClauseApplicable': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_CoInsuranceApplicability': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_CommissionApplicable': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_ConditionApplicable': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_CoverNoteApplicability': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_EQHigherExcessDiscount': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_ExclusionApplicable': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_FamilyPackageDiscount': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_FinancierApplicability': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_FloaterRiskLoading': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_GroupDiscount': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_HighClaimLoading': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_HigherExcessDiscount': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_Installment': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_LongTermPolicy': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_LowClaimDiscount': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_MinimumPremium': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_NoClaimDiscount': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_OverageLoading': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_PasPolicyApplicable': {'Type': 'String', 'Value': ''},
                'PropParameters_PastPolicyApplicable': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_PolicyCancellation': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_PopulateTransactionBegin': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_RenewalAllowed': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_SectionDiscount': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_ServiceTaxExempted': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_ShortPeriodLongPeriod': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_SpecialDiscountApplicable': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_StaffDiscount': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_StampDutyChargeable': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_TariffProduct': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_TransferFees': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_VDFDiscountAllowed': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_ValidateClause': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_ValidateCondition': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_ValidateEndorsementData': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_ValidateExclusion': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_ValidateWarranty': {'Type': 'Boolean', 'Value': 'False'},
                'PropParameters_WarrentyApplicable': {'Type': 'Boolean', 'Value': 'False'},
                'PropPolicyCoverDetails_Applicable': {'Type': 'Boolean', 'Value': 'False'},
                'PropPolicyCoverDetails_CoverGroups': {'Type': 'String', 'Value': ''},
                'PropPolicyCoverDetails_DifferentialSI': {'Type': 'Double', 'Value': '0'},
                'PropPolicyCoverDetails_EndorsementAmount': {'Type': 'Double', 'Value': '0'},
                'PropPolicyCoverDetails_Premium': {'Type': 'Double', 'Value': '0'},
                'PropPolicyCoverDetails_Rate': {'Type': 'Double', 'Value': '0'},
                'PropPolicyCoverDetails_SumInsured': {'Type': 'Double', 'Value': '0'},
                'PropPolicyEffectivedate_Fromdate_Mandatary': {'Type': 'String', 'Value': policy_effective_date['from_date']},
                'PropPolicyEffectivedate_Fromhour_Mandatary': {'Type': 'String', 'Value': policy_effective_date['from_hour']},
                'PropPolicyEffectivedate_Todate_Mandatary': {'Type': 'String', 'Value': policy_effective_date['to_date']},
                'PropPolicyEffectivedate_Tohour_Mandatary': {'Type': 'String', 'Value': policy_effective_date['to_hour']},
                'PropPremiumCalculation_BasicPremium': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_Cess': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_CustomCoverLDPremium': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_ERFAmount': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_EndorsementERFAmount': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_EndorsementPremium': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_EndorsementServiceTax': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_EndorsementStampDuty': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_EndorsementTerrorismPremium': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_GrossCoverPremium': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_NetODPremium': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_NetPremium': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_NetTPPremium': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_SP_PR_LTFactor': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_ServiceTax': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_StampDuty': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_StampDutyApplicability': {'Type': 'Boolean', 'Value': 'False'},
                'PropPremiumCalculation_TerrorismPremium': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_TotalPremium': {'Type': 'Double', 'Value': '0'},
                'PropPremiumCalculation_TransferFeeAmount': {'Type': 'Double', 'Value': '0'},
                'PropParameters_PreviousPolicyApplicable': {'Type': 'String', 'Value': ''},
                'PropPreviousPolicyDetails_AddRow': {'Type': 'Boolean', 'Value': 'False'},
                'PropPreviousPolicyDetails_CalculateClaimRatio': {'Type': 'Boolean', 'Value': 'False'},
                'PropPreviousPolicyDetails_ClaimRatio': {'Type': 'Double', 'Value': '0'},
                'PropPreviousPolicyDetails_FirstPolicyInceptionDate': {'Type': 'String', 'Value': ''},
                'PropPreviousPolicyDetails_InceptionYear': {'Type': 'Double', 'Value': '0'},
                'PropPreviousPolicyDetails_NCBPercentage': {'Type': 'Double', 'Value': '0'},
                'PropPreviousPolicyDetails_OldTariffSlab': {'Type': 'Boolean', 'Value': 'False'},
                'PropPreviousPolicyDetails_PreviousPolicyType': {'Type': 'String', 'Value': ''},
                'PropPreviousPolicyDetails_Validate': {'Type': 'Boolean', 'Value': 'False'},
                'PropProductDetails_Component17': {'Type': 'String', 'Value': ''},
                'PropProductDetails_DepartmentCode': {'Type': 'Double', 'Value': '0'},
                'PropProductDetails_ProductCode': {'Type': 'Double', 'Value': '0'},
                'PropProductDetails_ProductName': {'Type': 'String', 'Value': ''},
                'PropProductName': {'Type': 'String', 'Value': ''},
                'PropReferenceNoDate_ReferenceDate_Mandatary': {'Type': 'String', 'Value': ''},
                'PropReferenceNoDate_ReferenceNo_Mandatary': {'Type': 'Double', 'Value': '0'},
                'PropSPDetails_SPCode': {'Type': 'String', 'Value': ''},
                'PropSPDetails_SPName': {'Type': 'String', 'Value': ''},
                'PropSerIntermediaryDetails_SerIntermediaryCode': {'Type': 'String', 'Value': ''},
                'PropSerIntermediaryDetails_SerIntermediaryName': {'Type': 'String', 'Value': ''},
                'PropSerIntermediaryDetails_SerIntermediaryType': {'Type': 'String', 'Value': ''},
                'PropServiceTaxExemptionCategory_Component43': {'Type': 'String', 'Value': ''},
                'PropVerticalDtls_PrimaryVerticalCode': {'Type': 'String', 'Value': ''},
                'PropVerticalDtls_PrimaryVerticalName': {'Type': 'String', 'Value': ''},
                'PropVerticalDtls_SecondaryVerticalCode': {'Type': 'String', 'Value': ''},
                'PropVerticalDtls_SecondaryVerticalName': {'Type': 'String', 'Value': ''},
                'PropVerticalDtls_TertiaryVerticalCode': {'Type': 'String', 'Value': ''},
                'PropVerticalDtls_TertiaryVerticalName': {'Type': 'String', 'Value': ''},
                'PropWarranty_DepartmentCode': {'Type': 'Double', 'Value': '0'},
                'PropWarranty_ProductCode': {'Type': 'Double', 'Value': '0'},
                'PropWarranty_SectionCode': {'Type': 'String', 'Value': ''},
            }
        }
        return GeneralProposalInformation

    def _get_segment_type_literal(self, segment_type):
        segment_dict = {
            'High End Cars': 'High End',
            'Compact Cars': 'Compact',
            'Midsize Cars': 'Mid Size',
            'Small Sized Vehicles': 'Mini',
            'Sports and Utility Vehicles': 'MPV SUV',
            'Not Defined': '',
        }
        return segment_dict[segment_type]

    def _get_fuel_type_literal(self, fuel_type):
        fuel_dict = {
            'Petrol': 'Petrol',
            'Diesel': 'Diesel',
            'External LPG / CNG': 'External CNG',
            'LPG / Petrol': 'LPG',
            'CNG / Petrol': 'CNG',
            'Electricity': 'Electricity',
            'Not Defined': '',
        }
        return fuel_dict[fuel_type]

    def _get_risk_details(self, data_dictionary, form_details, vehicleobj, forPremium):

        registration_date = '%s/%s/%s' % tuple(
            data_dictionary['registrationDate'].split('-'))
        manufacture_month = data_dictionary['manufacturingDate'].split('-')[1]
        manufacture_year = data_dictionary['manufacturingDate'].split('-')[2]

        city_primarily_used = data_dictionary['cityPrimarilyUsed']
        ncb = '0'
        if data_dictionary['isNewVehicle'] == '0' and data_dictionary['isClaimedLastYear'] == '0':
            ncb = PREVIOUS_NCB_TO_NEW_NCB[data_dictionary['previousNCB']]

        registration_number = form_details['registration_number']
        rto_code = '%s-%02d' % (registration_number[0],
                                int(registration_number[1]))
        rto_zone = 'Zone-' + RTO_CITY_CODE_TO_INFO[rto_code]['Zone']
        authority_location = RTO_CITY_CODE_TO_INFO[rto_code]['RTOCity']

        registration_number[1] = '%02d' % int(registration_number[1])
        if len(registration_number[2]) == 3:
            registration_number[1] += registration_number[2][0]
            registration_number[2] = registration_number[2][1:]

        if data_dictionary['isNewVehicle'] == '1':
            registration_number = ['NEW', '', '', '']

        idv = data_dictionary['idv']

        data = {
            'addon_plan': 'Optional',
            'addon_choice': 'Choice 1',
            'chassis_number': form_details['chassis_number'],
            'engine_no': form_details['engine_no'],
            'authority_location': authority_location,
            'date_of_registration': registration_date,
            'date_of_purchase': registration_date,
            'state': form_details['registration_state'],
            'state_code': form_details['registration_state_code'],
            'district': form_details['registration_district'],
            'district_code': form_details['registration_district_code'],
            'city': form_details['registration_city'],
            'city_code': form_details['registration_city_code'],
            'pincode': form_details['registration_pincode'],
            'gender': form_details['gender'],
            'idv': str(idv),
            'firstname': form_details['firstname'],
            'lastname': form_details['lastname'],
            'marital_status': form_details['marital_status'],
            'no_of_person_wider_legal_liability': '1',
            'period_from': '',  # For geographical area extension
            'period_to': '',   # For geographical area extension
            'business_type': form_details['business_type'],
            'registration_address_1': form_details['registration_address_1'],
            'registration_address_2': form_details['registration_address_2'],
            'registration_address_3': form_details['registration_address_3'],
            'nominee_name': form_details['nominee_name'],
            'nominee_relationship': form_details['nominee_relationship'],
            'nominee_age': form_details['nominee_age'],
            # Still in doubt
            'vehicle_purchase_as': 'Brand new' if data_dictionary['isNewVehicle'] == '1' else 'Used',
            'main_driver': 'Self - Owner Driver',  # Other parameters: Any Other
            'age': form_details['age'],
            'licensed_year': form_details['licensed_year'],
            'manufacture_month': manufacture_month,
            'manufacture_year': manufacture_year,
            'ncb_applicable': ncb,
            'entitled_for_ncb': 'No',
            'ncb_confirmation': 'On declaration',
            'city_primarily_used': city_primarily_used,
        }

        if data_dictionary['isNewVehicle'] == '0':
            if data_dictionary['isClaimedLastYear'] == '0':
                data['entitled_for_ncb'] = 'Yes'
        else:
            data['ncb_confirmation'] = 'NCB reserving'

        if int(data['ncb_applicable']) == 0:
            data['ncb_confirmation'] = 'Yes'

        segment_type = self._get_segment_type_literal(
            vehicleobj.vehicle_segment)
        fuel_type = self._get_fuel_type_literal(vehicleobj.fuel_type)

        if segment_type == 'High End':
            data['addon_choice'] = 'Choice 2'

        vehicle = {
            'cc': str(vehicleobj.cc),
            'fuel_type': fuel_type,
            'make': str(vehicleobj.make),
            'make_code': str(vehicleobj.make_code),
            'model': str(vehicleobj.model),
            'model_code': str(vehicleobj.model_code),
            'variant': str(vehicleobj.variant),
            'seating_capacity': str(vehicleobj.seating_capacity),
            'segment_type': segment_type,
            'vehicle_type': 'Indigenous',
            'vehicle_type_code': '45',
        }

        compulsory_excess = 1000
        if int(vehicle['cc']) > 1500:
            compulsory_excess = 2000

        RiskDetails = {
            'Children': {
                'Block': {
                    'Children': {
                        'PropRisks_AAOmembershipno': {'Type': 'String', 'Value': ''},
                        'PropRisks_ABS': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_AddOnDiscnt': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_AddOnPlanSelected': {'Type': 'String', 'Value': data['addon_plan']},
                        'PropRisks_AddOnChoiceOpted': {'Type': 'String', 'Value': data['addon_choice']},
                        'PropRisks_RepairOfGlasPlastcFibNRubrGlas': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_LossOfPersonalBelongings': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_SumAssuredLossOfPersonalBelong': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_KeyReplacement': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_SumAssuredKeyReplacement': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DepreciationReimbursement': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_NoOfClaimsDepreciation': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_Age': {'Type': 'Double', 'Value': data['nominee_age']},
                        'PropRisks_AgeCoverageDetails': {'Type': 'Double', 'Value': data['nominee_age']},
                        'PropRisks_Airbag': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_AllowanceDaysAccCourtHireCar': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_AllowanceDaysAccident': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_AllowanceDaysTheftTotal': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_AllowanceDaystheft': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_Amount': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_AntiTheftCheck': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_AnyOtherTataAigProduct': {'Type': 'String', 'Value': ''},
                        'PropRisks_ApplicableDocuments': {'Type': 'String', 'Value': ''},
                        'PropRisks_AuthorityLocation': {'Type': 'String', 'Value': data['authority_location']},
                        'PropRisks_AutoMobileAssocName': {'Type': 'String', 'Value': ''},
                        'PropRisks_AutoTransmission': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_AutomobileAssociC': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_AverageMonthlySalary': {'Type': 'String', 'Value': ''},
                        'PropRisks_Bangladesh': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_Bhutan': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_BuildingSecurity': {'Type': 'String', 'Value': ''},
                        'PropRisks_CNGLPGkitValue': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_CampaignFixAmt': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_CapitalSIPerPerson': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_CertifiedVintageCarByCarClub': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_ChasisNo': {'Type': 'String', 'Value': ''},
                        'PropRisks_ChassisNumber': {'Type': 'String', 'Value': data['chassis_number']},
                        'PropRisks_ChkForGeographical': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_ChkForRacing': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_City': {'Type': 'String', 'Value': data['city']},
                        'PropRisks_CityCode': {'Type': 'String', 'Value': data['city_code']},
                        'PropRisks_CityWherePrimarilyUsed': {'Type': 'String', 'Value': data['city_primarily_used']},
                        'PropRisks_Colorofthevehicle': {'Type': 'String', 'Value': ''},
                        'PropRisks_CompForVolExc1': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_CompForVolExc2': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_CompForVolExc3': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_CompForVolExc4': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_CompulsoryExcess': {'Type': 'Double', 'Value': str(compulsory_excess)},
                        'PropRisks_CompulsoryPAwithOwnerDriver': {'Type': 'Boolean', 'Value': 'True'},
                        'PropRisks_ConsumableExpensLoading': {'Type': 'String', 'Value': ''},
                        'PropRisks_ConsumablesExpenses': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_CountryCode': {'Type': 'String', 'Value': '64'},
                        'PropRisks_CountryForRTO': {'Type': 'String', 'Value': 'INDIA'},
                        'PropRisks_CourtesyHireCar': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_CourtseyFranchiDay': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_CubicCapacity': {'Type': 'Double', 'Value': vehicle['cc']},
                        'PropRisks_CustomerLoyaltyDiscount': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_CvrPrm': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DailyAllowance': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_DateOfBirth': {'Type': 'String', 'Value': ''},
                        'PropRisks_DateofRegistration': {'Type': 'String', 'Value': data['date_of_registration']},
                        'PropRisks_Dateofpurchase': {'Type': 'String', 'Value': data['date_of_purchase']},
                        'PropRisks_DiscDeprctionreimburment': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscntEmerTrnsportHotelExp': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscntEmrgncyMedicalExp': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscntHomeProtectSublt': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscntPersonalTripEffect': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscntPrm': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_Discount': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscountConsumbleExpense': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscountCourtesyHireCar': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscountDailyAllowance': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscountEngineSecure': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscountGPA': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscountKeyReplacement': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscountLossPersnalbelong': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscountNCBProtection': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscountNewCarRplacmnt': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscountRepirGlasFiber': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscountReturnInvoice': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscountRoadSideAssis': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_DiscountTyreSecure': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_District': {'Type': 'String', 'Value': data['district']},
                        'PropRisks_DistrictCode': {'Type': 'String', 'Value': data['district_code']},
                        'PropRisks_Drivingexperience': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_Educationalqualification': {'Type': 'String', 'Value': ''},
                        'PropRisks_ElectricalAccessories': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_EmergTrnsprtAndHotelExpensIDV': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_EmergencyMedicalExpenses': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_EmployeesDiscount': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_EngineSecure': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_EngineSecureOptions': {'Type': 'String', 'Value': ''},
                        'PropRisks_Engineno': {'Type': 'String', 'Value': data['engine_no']},
                        'PropRisks_EntitledforNCB': {'Type': 'String', 'Value': data['entitled_for_ncb']},
                        'PropRisks_EventName': {'Type': 'String', 'Value': ''},
                        'PropRisks_Expirydate': {'Type': 'String', 'Value': ''},
                        'PropRisks_FirstName': {'Type': 'String', 'Value': data['firstname']},
                        'PropRisks_FleetDiscount': {'Type': 'String', 'Value': ''},
                        'PropRisks_FleetFixAmt': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_ForUWLoading': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_FranchiseDays': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_FromDateForRacing': {'Type': 'String', 'Value': ''},
                        'PropRisks_FromDateForRallies': {'Type': 'String', 'Value': ''},
                        'PropRisks_FromDateUForm': {'Type': 'String', 'Value': ''},
                        'PropRisks_FromDateVehicleLaidup': {'Type': 'String', 'Value': ''},
                        'PropRisks_FuelType': {'Type': 'String', 'Value': vehicle['fuel_type']},
                        'PropRisks_FuelUsed': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_GLMRate': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_Gender': {'Type': 'String', 'Value': data['gender']},
                        'PropRisks_Geographicalareaextention': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_GroupPersonalAccident': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_HomeAway': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_IDVofthevehicle': {'Type': 'Double', 'Value': data['idv']},
                        'PropRisks_ImposedExcessAmount': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_ImprtdVehcleWoutPaymtCustmDuty': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_InsuredHasDrivinglicense': {'Type': 'Boolean', 'Value': 'True'},
                        'PropRisks_IsCNGLPGKitinsuredinPIP': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_IsVehcleUsdForDrivngTuitions': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_IsVehicle': {'Type': 'String', 'Value': ''},
                        'PropRisks_IsVehicleUsedforblindpersons': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_IsVehicleusedlimitedtopremises': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_Issuer': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LastName': {'Type': 'String', 'Value': data['lastname']},
                        'PropRisks_LdngPrm': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LegalLiabilityToEmployees': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_LiabilityToSoldierSailorAirMan': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_Lifemember': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_LiscenseNo': {'Type': 'String', 'Value': ''},
                        'PropRisks_Liscenseissuingauthority': {'Type': 'String', 'Value': ''},
                        'PropRisks_LoadConsumbleExpense': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadDeprctionreimburment': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadEmerTrnsportHotelExp': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadEmrgencyMedicalExp': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadHomeProtectSublt': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadPersonalTripEffect': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadingCourtesyHireCar': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadingDailyAllowance': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadingDiscountOnAddOnCovers': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadingEngineSecure': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadingGPA': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadingKeyReplacement': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadingLossPersnalbelong': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadingNCBProtection': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadingNewCarRplacmnt': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadingRepirGlasFiber': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadingReturnInvoice': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadingRoadSideAssis': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LoadingTyreSecure': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_LocationCode': {'Type': 'String', 'Value': ''},
                        'PropRisks_MainDriver': {'Type': 'String', 'Value': data['main_driver']},
                        'PropRisks_Maldives': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_Manufacture': {'Type': 'String', 'Value': vehicle['make']},
                        'PropRisks_ManufacturerCode': {'Type': 'String', 'Value': vehicle['make_code']},
                        'PropRisks_MaritalStatus': {'Type': 'String', 'Value': data['marital_status']},
                        'PropRisks_Model': {'Type': 'String', 'Value': vehicle['model']},
                        'PropRisks_ModelCode': {'Type': 'String', 'Value': vehicle['model_code']},
                        'PropRisks_ModelVariant': {'Type': 'String', 'Value': vehicle['variant']},
                        'PropRisks_MonthlyMileage': {'Type': 'String', 'Value': ''},
                        'PropRisks_NCBConfirmation': {'Type': 'String', 'Value': data['ncb_confirmation']},
                        'PropRisks_NCBProtRatePop': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_NCBProtectionCover': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_NCBRatePop': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_NCPrm': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_NameOfNominee': {'Type': 'String', 'Value': data['nominee_name']},
                        'PropRisks_Nepal': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_NewCarReplacement': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_NoClaimBonusApplicable': {'Type': 'Double', 'Value': data['ncb_applicable']},
                        'PropRisks_NoOfAccidents': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_NoOfClaimFreeYearsCompleted': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_NoOfClaimsNCBProtection': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_NoOfCylinders': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_NoOfPersonWiderLegalLiability': {'Type': 'Double',
                                                                    'Value': data['no_of_person_wider_legal_liability']},
                        'PropRisks_NoOfPersonsLiabilityEmployee': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_NoOfPersonsNamed': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_NoOfPersonsPaidDriver': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_NonElectricalAccessories': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_NumberOfClaims': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_NumberOfDrivers': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_NumberofPersonsUnnamed': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_NumberofViolationsConvictions': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_ODOMETERReading': {'Type': 'String', 'Value': ''},
                        'PropRisks_Occupation': {'Type': 'String', 'Value': ''},
                        'PropRisks_OpenCvrNo': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_OriginalNCBPercentage': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_Pakistan': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_PerDayAllowance': {'Type': 'String', 'Value': ''},
                        'PropRisks_PeriodFrom': {'Type': 'String', 'Value': data['period_from']},
                        'PropRisks_PeriodTo': {'Type': 'String', 'Value': data['period_to']},
                        'PropRisks_PeriodofLaidup': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_PersnalExtndedProtctionPolPEPP': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_PersonalTripEffects': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_Pincode': {'Type': 'Double', 'Value': data['pincode']},
                        'PropRisks_PolicyAge': {'Type': 'String', 'Value': ''},
                        'PropRisks_PolicyNoEmployeeDiscount': {'Type': 'String', 'Value': ''},
                        'PropRisks_PolicyNumber': {'Type': 'String', 'Value': ''},
                        'PropRisks_PostOffice': {'Type': 'String', 'Value': ''},
                        'PropRisks_PreSourceApproval': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_PrevYearNCB': {'Type': 'String', 'Value': ''},
                        'PropRisks_PreviousAccidentHistory': {'Type': 'String', 'Value': ''},
                        'PropRisks_PromoterName': {'Type': 'String', 'Value': ''},
                        'PropRisks_ProposalType': {'Type': 'String', 'Value': data['business_type']},
                        'PropRisks_ProtectedNCB': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_RTOCode': {'Type': 'String', 'Value': rto_code},
                        'PropRisks_RallyCheck': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_RatForUWDisc': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_RateForReliabOD': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_RateForReliabTP': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_RegNoNotRequired': {'Type': 'Boolean',
                                                       'Value': 'True' if data_dictionary['isNewVehicle'] == '1' else 'False'},
                        'PropRisks_RegistrationAddressSameComm': {'Type': 'Boolean', 'Value': 'True'},
                        'PropRisks_RegistrationNoAsPerOldLogic': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_RegistrationNumber': {'Type': 'String', 'Value': registration_number[0]},
                        'PropRisks_RegistrationNumber2': {'Type': 'String', 'Value': registration_number[1]},
                        'PropRisks_RegistrationNumber3': {'Type': 'String', 'Value': registration_number[2]},
                        'PropRisks_RegistrationNumber4': {'Type': 'String', 'Value': registration_number[3]},
                        'PropRisks_Registrationaddressline1': {'Type': 'String', 'Value': data['registration_address_1']},
                        'PropRisks_Registrationaddressline2': {'Type': 'String', 'Value': data['registration_address_2']},
                        'PropRisks_Registrationaddressline3': {'Type': 'String', 'Value': data['registration_address_3']},
                        'PropRisks_Relationship': {'Type': 'String', 'Value': data['nominee_relationship']},
                        'PropRisks_RelationshipToTheInsured': {'Type': 'String', 'Value': ''},
                        'PropRisks_ReliabilityTrialCheck': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_Reliabilitytrialorracingdate': {'Type': 'String', 'Value': ''},
                        'PropRisks_Reliabilitytrialorracingtodate': {'Type': 'String', 'Value': ''},
                        'PropRisks_ReturnToInvoice': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_RiskVariant': {'Type': 'String', 'Value': 'PackageComprehensive'},
                        'PropRisks_RiskVariantSection': {'Type': 'String', 'Value': ''},
                        'PropRisks_RoadsideAssistance': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_SIForHomeAway': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_SIForPersonalTrip': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_STax': {'Type': 'String', 'Value': 'No'},
                        'PropRisks_SeatingCapacity': {'Type': 'Double', 'Value': vehicle['seating_capacity']},
                        'PropRisks_SriLanka': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_State': {'Type': 'String', 'Value': data['state']},
                        'PropRisks_StateCode': {'Type': 'String', 'Value': data['state_code']},
                        'PropRisks_SumAssuredEmergencyMedExp': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_SumInsuredPaidDriver': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_TPCoverCndtn': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_TPPDRestictDD': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_TPPDRestricted': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_TPRatChk': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_TheftTarget': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_ThreeWheelerHandicapCheck': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_ToDateForRacing': {'Type': 'String', 'Value': ''},
                        'PropRisks_ToDateForRallies': {'Type': 'String', 'Value': ''},
                        'PropRisks_ToDateUForm': {'Type': 'String', 'Value': ''},
                        'PropRisks_ToDateVehicleLaidup': {'Type': 'String', 'Value': ''},
                        'PropRisks_TotalInsuredDeclaredValue': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_TypeOfAdjustment': {'Type': 'String', 'Value': ''},
                        'PropRisks_TypeOfRoad': {'Type': 'String', 'Value': ''},
                        'PropRisks_TypeofTransmission': {'Type': 'String', 'Value': ''},
                        'PropRisks_TypeofWheelRim': {'Type': 'String', 'Value': ''},
                        'PropRisks_Typeofbody': {'Type': 'String', 'Value': vehicle['segment_type']},
                        'PropRisks_TyreSecure': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_TyreSecureOption': {'Type': 'String', 'Value': ''},
                        'PropRisks_UForm': {'Type': 'String', 'Value': ''},
                        'PropRisks_UnderwritingDiscount': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_UserEnteredPremium': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_UserName': {'Type': 'String', 'Value': ''},
                        'PropRisks_UserType': {'Type': 'String', 'Value': ''},
                        'PropRisks_Variant': {'Type': 'String', 'Value': ''},
                        'PropRisks_VehcleFitWithFibrGlasFuelTank': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_VehicleAge_Mandatary': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_VehicleInspection': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_VehicleParkingplace': {'Type': 'String', 'Value': ''},
                        'PropRisks_VehiclePurchaseAs': {'Type': 'String', 'Value': data['vehicle_purchase_as']},
                        'PropRisks_VehicleSegment': {'Type': 'String', 'Value': vehicle['segment_type']},
                        'PropRisks_VehicleType': {'Type': 'String', 'Value': vehicle['vehicle_type']},
                        'PropRisks_VehicleTypeCode': {'Type': 'String', 'Value': vehicle['vehicle_type_code']},
                        'PropRisks_VehicleUse': {'Type': 'String', 'Value': 'Social, Domestic, Pleasure &amp; Professional'},
                        'PropRisks_VoluntaryDeductibleAmount': {'Type': 'Double', 'Value': '0'},
                        'PropRisks_WiderLegalLiabilityToPaid': {'Type': 'Boolean', 'Value': 'False'},
                        'PropRisks_YearLicensed': {'Type': 'Double', 'Value': data['licensed_year']},
                        'PropRisks_Zone_Mandatary': {'Type': 'String', 'Value': rto_zone},
                        'PropRisks_ManufactureMonth': {'Type': 'Double', 'Value': data['manufacture_month']},
                        'PropRisks_ManufactureYear': {'Type': 'Double', 'Value': data['manufacture_year']},
                        'PropRisks_BasisOfRating': {'Type': 'String', 'Value': 'GLM'},
                        'PropRisks_Campaign': {'Type': 'String', 'Value': CAMPAIGN},
                        'OtherDetailsGrid': {
                            'Children': {
                            }
                        }
                    }
                }
            }
        }
        return RiskDetails

    def _get_previous_policy_details(self, policy):
        PreviousPolicyDetails = {
            'Children': {
                'PreviousPolicyDetails': {
                    'Type': 'GroupData',
                    'Children': {
                        'PropPreviousPolicyDetails_ClaimLodgedInPast': {'Type': 'Boolean', 'Value': 'False'},
                        'PropPreviousPolicyDetails_ClaimAmount': {'Type': 'Double', 'Value': policy['claim_amount']},
                        'PropPreviousPolicyDetails_ClaimNo': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_ClaimPremium': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_ClaimSettled': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_CorporateCustomerId_Mandatary': {'Type': 'String', 'Value': policy['insurer']},
                        'PropPreviousPolicyDetails_ClaimOutstanding': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_Deductibles': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_DiscountonPremiumoffered': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_IncurredClaimRatio': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_NoOfInsuredAtEnd': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_InspectionDone': {'Type': 'Boolean', 'Value': 'False'},
                        'PropPreviousPolicyDetails_NCBAbroadCheck': {'Type': 'Boolean', 'Value': 'False'},
                        'PropPreviousPolicyDetails_VehicleSold': {'Type': 'Boolean', 'Value': 'False'},
                        'PropPreviousPolicyDetails_NoOfInsuredCovered': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_NoOfClaims': {'Type': 'Double', 'Value': policy['number_of_claims']},
                        'PropPreviousPolicyDetails_PreviousInsPincode': {'Type': 'String', 'Value': policy['pincode']},
                        'PropPreviousPolicyDetails_SumInsuredOpted': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_SumInsuredPerFamily': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_AmountOfClaimsReject': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_TotalClaimsAmount': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_NoOfClaimsOutStanding': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_NoOfClaimsPaid': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_NoOfClaimsReject': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_OfficeCode': {'Type': 'String', 'Value': policy['office_code']},
                        'PropPreviousPolicyDetails_PolicyEffectiveFrom': {'Type': 'String', 'Value': policy['start_date']},
                        'PropPreviousPolicyDetails_PolicyEffectiveTo': {'Type': 'String', 'Value': policy['end_date']},
                        'PropPreviousPolicyDetails_PolicyNo': {'Type': 'String', 'Value': policy['number']},
                        'PropPreviousPolicyDetails_PolicyPremium': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_PolicyYear_Mandatary': {'Type': 'Double', 'Value': policy['year']},
                        'PropPreviousPolicyDetails_ProductCode': {'Type': 'Double', 'Value': '0'},
                        'PropPreviousPolicyDetails_WhtrPrevPolicyCpySubmitted': {'Type': 'Boolean', 'Value': 'False'},
                        'PropPreviousPolicyDetails_AddressLine2': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_OfficeAddress': {'Type': 'String', 'Value': policy['address']},
                        'PropPreviousPolicyDetails_City': {'Type': 'String', 'Value': policy['city']},
                        'PropPreviousPolicyDetails_State': {'Type': 'String', 'Value': policy['state']},
                        'PropPreviousPolicyDetails_RiskCode': {'Type': 'String', 'Value': policy['risk_code']},
                        'PropPreviousPolicyDetails_NatureofLoss': {'Type': 'String', 'Value': policy['nature_of_loss']},
                        'PropPreviousPolicyDetails_AddressLine3': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_ClaimDate': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_ClaimReconDate': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_ClaimsMode': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_Currency': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_DateofLoss': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_DateofSale': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_DocumentProof': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_DurationOfInterruption': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_FullPartiIncdtThreats': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_InspectionDate': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_InspectionDoneByWhom': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_IsDataDeleted': {'Type': 'Boolean', 'Value': 'False'},
                        'PropPreviousPolicyDetails_IsOldDataDeleted': {'Type': 'Boolean', 'Value': 'False'},
                        'PropPreviousPolicyDetails_IssuingOfficeCode': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_NameOfTPA': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_NatureOfDisease': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_PinCode': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_PolicyVariant': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_QuantumOfClaim': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_ReasonForNotRenewing': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_ReferenceNo': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_StpsTknDelInciThrtsPvntRec': {'Type': 'String', 'Value': ''},
                        'PropPreviousPolicyDetails_claimreportedorsetteled': {'Type': 'String', 'Value': ''},
                    }
                }
            }
        }
        return PreviousPolicyDetails

    def _get_financer_details(self, financier):
        FinancierDetails = {
            'Children': {
                'FinancierDetails': {
                    'Type': 'GroupData',
                    'Children': {
                            'PropFinancierDetails_AgreementType': {'Type': 'String', 'Value': 'Hire Purchase'},
                            'PropFinancierDetails_BranchName': {'Type': 'String', 'Value': ''},
                            'PropFinancierDetails_City': {'Type': 'String', 'Value': ''},
                            'PropFinancierDetails_CityCode': {'Type': 'String', 'Value': ''},
                            'PropFinancierDetails_Country': {'Type': 'String', 'Value': ''},
                            'PropFinancierDetails_District': {'Type': 'String', 'Value': ''},
                            'PropFinancierDetails_DistrictCode': {'Type': 'String', 'Value': ''},
                            'PropFinancierDetails_FinancierCode_Mandatary': {'Type': 'Double', 'Value': financier},
                            'PropFinancierDetails_FinancierName': {'Type': 'String', 'Value': FINANCIER_LIST[financier]},
                            'PropFinancierDetails_FinancierStatus': {'Type': 'String', 'Value': ''},
                            'PropFinancierDetails_FinancierType': {'Type': 'String', 'Value': ''},
                            'PropFinancierDetails_IsDataDeleted': {'Type': 'Boolean', 'Value': 'False'},
                            'PropFinancierDetails_IsOldDataDeleted': {'Type': 'Boolean', 'Value': 'False'},
                            'PropFinancierDetails_LoanAccountNo': {'Type': 'String', 'Value': ''},
                            'PropFinancierDetails_Pincode': {'Type': 'Double', 'Value': ''},
                            'PropFinancierDetails_Remarks': {'Type': 'String', 'Value': ''},
                            'PropFinancierDetails_SrNo_Mandatary': {'Type': 'Double', 'Value': '1'},
                            'PropFinancierDetails_State': {'Type': 'String', 'Value': ''},
                            'PropFinancierDetails_StateCode': {'Type': 'String', 'Value': ''},
                    }
                }
            }
        }
        return FinancierDetails

    def _get_customer_details(self, customer):

        CustomerDetails = {
            'Children': {
                'CustomerType': {'Name': 'Customer Type', 'Value': 'I', 'Type': 'String'},
                'Salutation': {'Name': 'Salutation', 'Value': customer['salutation'], 'Type': 'String'},
                'FirstName': {'Name': 'First Name', 'Value': customer['firstname'], 'Type': 'String'},
                'LastName': {'Name': 'Last Name', 'Value': customer['lastname'], 'Type': 'String'},
                'CustomerName': {'Name': 'Customer Name', 'Value': customer['name'], 'Type': 'String'},
                'DOB': {'Name': 'DOB', 'Value': customer['dob'], 'Type': 'String'},
                'SourceOfFund': {'Name': 'SourceOfFund', 'Value': 'OTHERS', 'Type': 'String'},
                'AddressLine1': {'Name': 'Address Line1', 'Value': customer['address1'], 'Type': 'String'},
                'AddressLine2': {'Name': 'Address Line2', 'Value': customer['address2'], 'Type': 'String'},
                'AddressLine3': {'Name': 'Address Line3', 'Value': '', 'Type': 'String'},
                'Landmark': {'Name': 'Landmark', 'value': customer['landmark'], 'Type': 'String'},
                'CountryID': {'Name': 'CountryID', 'Value': '64', 'Type': 'Double'},
                'CountryName': {'Name': 'CountryName', 'Value': 'INDIA', 'Type': 'String'},
                'StateCode': {'Name': 'State Code', 'Value': customer['state_code'], 'Type': 'Double'},
                'StateName': {'Name': 'State Name', 'Value': customer['state'], 'Type': 'String'},
                'CityDistrictCode': {'Name': 'District Code', 'Value': customer['district_code'], 'Type': 'Double'},
                'CityDistrictName': {'Name': 'District Name', 'Value': customer['district'], 'Type': 'String'},
                'CityId': {'Name': 'City Code', 'Value': customer['city_code'], 'Type': 'Double'},
                'CityName': {'Name': 'City Name', 'Value': customer['city'], 'Type': 'String'},
                'PinCode': {'Name': 'Pin Code', 'Value': customer['pincode'], 'Type': 'Double'},
                'POBox': {'Name': 'Post Office', 'Value': customer['postoffice'], 'Type': 'String'},
                'PinCodeLocality': {'Name': 'Pin Code Locality', 'Value': '', 'Type': 'String'},
                'Gender': {'Name': 'Gender', 'Value': customer['gender'], 'Type': 'String'},
                'LandLineNo': {'Name': 'Telephone No', 'Value': '', 'Type': 'String'},
                'MobileNo': {'Name': 'Mobile No', 'Value': customer['phone_no'], 'Type': 'String'},
                'FAXNo': {'Name': 'FAX No', 'Value': '', 'Type': 'String'},
                'Occupation': {'Name': 'Occupation Code', 'Value': customer['occupation_code'], 'Type': 'Double'},
                'EmailId': {'Name': 'Mail Id', 'Value': customer['email'], 'Type': 'String'},
                'MaritalStatus': {'Name': 'MaritalStatus', 'Value': customer['marital_status'], 'Type': 'String'},
                'IDProof': {'Name': 'IDProof', 'Value': '0', 'Type': 'Double'},
                'IDProofDetails': {'Name': 'Pan No', 'Value': customer['pan'], 'Type': 'String'},
                'PanNo': {'Name': 'Pan Number', 'Value': customer['pan'], 'Type': 'String'},
                'PermanentLocationSameAsMailLocation': {'Name': 'Permanent Location Same As Mail Location',
                                                        'Value': 'True', 'Type': 'Boolean'},
                'BankName': {'Name': 'Bank Name', 'Value': '', 'Type': 'String'},
                'BankCode': {'Name': 'Bank Code', 'Value': '0', 'Type': 'Double'},
                'BankBranchName': {'Name': 'Bank Branch Name', 'Value': '', 'Type': 'String'},
                'BankBranchCode': {'Name': 'Bank Branch Code', 'Value': '', 'Type': 'String'},
                'BankAccNumber': {'Name': 'Bank Acc Number', 'Value': '', 'Type': 'String'},
                'BankAccounttype': {'Name': 'Bank Account Type', 'Value': '1', 'Type': 'String'},
                'ISUIICEmployee': {'Name': 'UIICEmployee', 'Type': 'String', 'Value': 'N'},
            }
        }
        return CustomerDetails

    def _add_electrical(self, risk_details_dictionary, value, year):
        ElectricalAccessories = {
            'Name': 'ElectricalAccessiories',
            'Value': 'GRP289',
            'Children': {
                'ElectricalAccessiories': {
                    'Type': 'GroupData',
                    'Children': {
                        'Make': {'Name': 'Make', 'Value': 'Make', 'Type': 'String'},
                        'Model': {'Name': 'Model', 'Value': 'Model', 'Type': 'String'},
                        'Year': {'Name': 'Year', 'Value': str(year), 'Type': 'Double'},
                        'IDV': {'Name': 'IDV', 'Value': str(value), 'Type': 'Double'},
                        'TypeofAccessories': {'Name': 'TypeofAccessories', 'Value': 'Electrical', 'Type': 'String'},
                        'Description': {'Name': 'Description', 'Value': 'Electrical Accessories', 'Type': 'String'},
                    }
                }
            }
        }

        ElectricalAccessoriesDetails = {
            'Name': 'Electrical Accessories',
            'Valu': '0',
            'Children': {
                'ElectricalAccessoriesDetails': {
                    'Type': 'GroupData',
                    'Children': {
                        'SI': {'Name': 'SI', 'Value': str(value), 'Type': 'Double'},
                    }
                }
            }
        }

        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_ElectricalAccessories']['Value'] = str(value)
        risk_details_dictionary['Children']['Block']['Children']['OtherDetailsGrid'][
            'Children']['ElectricalAccessories'] = ElectricalAccessories
        risk_details_dictionary['Children']['Block']['Children']['OtherDetailsGrid'][
            'Children']['ElectricalAccessoriesDetails'] = ElectricalAccessoriesDetails

    def _add_pa_unnamed(self, risk_details_dictionary, unnamedpa_si, seating_capacity):
        UnnamedPAcoverDetails = {
            'Name': 'UnnamedPAcover',
            'Value': '0',
            'Children': {
                'UnnamedPAcoverDetails': {
                    'Type': 'GroupData',
                    'Children': {
                        'SI': {'Name': 'SI', 'Value': unnamedpa_si, 'Type': 'Double'},
                    }
                }
            }
        }
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_CapitalSIPerPerson']['Value'] = unnamedpa_si
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_NumberofPersonsUnnamed']['Value'] = seating_capacity
        risk_details_dictionary['Children']['Block']['Children']['OtherDetailsGrid'][
            'Children']['UnnamedPAcoverDetails'] = UnnamedPAcoverDetails

    def _add_legal_liability_for_paid_driver(self, risk_details_dictionary):
        LegalLiabilityforpaiddrivercleanerconductorDetails = {
            'Name': 'LegalLiabilityforpaiddrivercleanerconductor',
            'Value': '0',
            'Children': {
                'LegalLiabilityforpaiddrivercleanerconductorDetails': {
                    'Type': 'GroupData',
                    'Children': {
                        'SI': {'Name': 'SI', 'Value': '0', 'Type': 'Double'},
                    }
                }
            }
        }
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_WiderLegalLiabilityToPaid']['Value'] = 'True'
        risk_details_dictionary['Children']['Block']['Children']['OtherDetailsGrid']['Children'][
            'LegalLiabilityforpaiddrivercleanerconductorDetails'] = LegalLiabilityforpaiddrivercleanerconductorDetails

    def _add_nominee_apointee_details(self, risk_details_dictionary, nominee):
        OwnerDriverNomineeDetails = {
            'Name': 'OwnerDriverNomineeDetails',
            'Value': 'GRP292',
            'Children': {
                'OwnerDriverNomineeDetails': {
                    'Type': 'GroupData',
                    'Children': {
                        'Nominee': {'Name': 'Nominee', 'Value': nominee['name'], 'Type': 'String'},
                        'Relationship': {'Name': 'Model', 'Value': nominee['relationship'], 'Type': 'String'},
                        'Age': {'Name': 'Year', 'Value': str(nominee['age']), 'Type': 'Double'},
                    }
                }
            }
        }
        if int(nominee['age']) < 18:
            OwnerDriverNomineeDetails['Children']['OwnerDriverNomineeDetails']['Children'][
                'Nameofappointee'] = {'Name': 'Nameofappointee', 'Value': nominee['apointee'], 'Type': 'String'}
            OwnerDriverNomineeDetails['Children']['OwnerDriverNomineeDetails']['Children']['Relationshiptonominee'] = {
                'Name': 'Relationshiptonominee', 'Value': nominee['appointee_relationship'], 'Type': 'String'}

        risk_details_dictionary['Children']['Block']['Children']['OtherDetailsGrid'][
            'Children']['OwnerDriverNomineeDetails'] = OwnerDriverNomineeDetails

    def _add_non_electrical(self, risk_details_dictionary, value, year):
        NonElectricalAccessiories = {
            'Name': 'NonElectricalAccessiories',
            'Value': 'GRP288',
            'Children': {
                'NonElectricalAccessiories': {
                    'Type': 'GroupData',
                    'Children': {
                        'Make': {'Name': 'Make', 'Value': 'Make', 'Type': 'String'},
                        'Model': {'Name': 'Model', 'Value': 'Model', 'Type': 'String'},
                        'Year': {'Name': 'Year', 'Value': str(year), 'Type': 'Double'},
                        'IDV': {'Name': 'IDV', 'Value': str(value), 'Type': 'Double'},
                        'TypeofAccessories': {'Name': 'TypeofAccessories', 'Value': 'NonElectrical', 'Type': 'String'},
                        'Description': {'Name': 'Description', 'Value': 'NonElectrical Accessories', 'Type': 'String'},
                    }
                }
            }
        }
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_NonElectricalAccessories']['Value'] = str(value)
        risk_details_dictionary['Children']['Block']['Children']['OtherDetailsGrid'][
            'Children']['NonElectricalAccessiories'] = NonElectricalAccessiories

    def _add_cng_kit_value(self, risk_details_dictionary, value):
        CNGKitDetails = {
            'Name': 'CNGKit',
            'Value': '0',
            'Children': {
                'CNGKitDetails': {
                    'Type': 'GroupData',
                    'Children': {
                        'SI': {'Name': 'SI', 'Value': str(value), 'Type': 'Double'},
                    }
                }
            }
        }
        risk_details_dictionary['Children']['Block']['Children'][
            'OtherDetailsGrid']['Children']['CNGKitDetails'] = CNGKitDetails
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_CNGLPGkitValue']['Value'] = str(value)
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_FuelType']['Value'] = 'External CNG'

    def _add_road_side_assistance(self, risk_details_dictionary):
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_RoadsideAssistance']['Value'] = 'True'

    def _add_voluntary_deductable(self, risk_details_dictionary, value):
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_VoluntaryDeductibleAmount']['Value'] = str(value)

    def _add_ncb_protection(self, risk_details_dictionary):
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_NCBProtectionCover']['Value'] = 'True'
        # TODO: Value to be determined
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_NoOfClaimsNCBProtection']['Value'] = '2'

    def _add_engine_protection(self, risk_details_dictionary):
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_EngineSecure']['Value'] = 'True'
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_EngineSecureOptions']['Value'] = 'With Deductible'

    def _add_invoice_cover(self, risk_details_dictionary):
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_ReturnToInvoice']['Value'] = 'True'

    def _add_key_replacement(self, risk_details_dictionary):
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_KeyReplacement']['Value'] = 'True'
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_SumAssuredKeyReplacement']['Value'] = '25000'

    def _add_emergency_transport_and_hotel(self, risk_details_dictionary, vehicle):
        sumInsured = '5000'
        segment_type = self._get_segment_type_literal(vehicle.vehicle_segment)
        if segment_type == 'High End':
            sumInsured = '25000'

        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_RatioofAOAinAOY']['Value'] = '1:2'
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_EmergTrnsprtAndHotelExpensIDV']['Value'] = sumInsured

    def _add_loss_of_personal_belongings(self, risk_details_dictionary, vehicle):
        sumInsured = '10000'
        segment_type = self._get_segment_type_literal(vehicle.vehicle_segment)
        if segment_type == 'High End':
            sumInsured = '50000'

        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_LossOfPersonalBelongings']['Value'] = 'True'
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_SumAssuredLossOfPersonalBelong']['Value'] = sumInsured

    def _add_zero_depreciation(self, risk_details_dictionary):
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_DepreciationReimbursement']['Value'] = 'True'
        risk_details_dictionary['Children']['Block']['Children'][
            'PropRisks_NoOfClaimsDepreciation']['Value'] = '2'

    def _get_root(self, data_dictionary, form_details, forPremium):
        root = {
            'Name': 'PrivateCarInsurancePolicy',
            'Code': FOUR_WHEELER_PRODUCT_CODE,
            'Children': {
                'ProposalDetails': {
                    'Children': {
                    }
                }
            }
        }

        vehicle = Vehicle.objects.get(id=data_dictionary['vehicleId'])

        is_new_vehicle = True if data_dictionary[
            'isNewVehicle'] == '1' else False

        if 'newPolicyStartDate' not in data_dictionary.keys():
            data_dictionary['newPolicyStartDate'] = (
                datetime.datetime.now() + datetime.timedelta(days=1)).strftime("%d-%m-%Y")

        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                data_dictionary['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data_dictionary['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

            past_policy_start_date = putils.subtract_years(
                past_policy_end_date, 1) + datetime.timedelta(days=1)
            form_details['past_policy_start_date'] = past_policy_start_date.strftime(
                "%d/%m/%Y")
            form_details['past_policy_end_date'] = past_policy_end_date.strftime(
                "%d/%m/%Y")

        new_policy_end_date = putils.add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)

        proposal_info = {
            'business_type': 'New Business' if is_new_vehicle else 'Roll Over',
            'policy_start_date': new_policy_start_date.strftime("%d/%m/%Y"),
            'policy_end_date': new_policy_end_date.strftime("%d/%m/%Y"),
        }

        risk_details = self._get_risk_details(
            data_dictionary, form_details, vehicle, forPremium)

        general_information = self._get_general_proposal_information(
            data_dictionary, form_details, proposal_info)

        year = data_dictionary['registrationDate'].split('-')[2]
        NOT_LIST = ['None', '0', 0, None]

        is_cng_vehicle = True if data_dictionary[
            'isCNGFitted'] == '1' else False
        cng_kit_value = data_dictionary['cngKitValue']
        if is_cng_vehicle and cng_kit_value not in NOT_LIST:
            self._add_cng_kit_value(risk_details, cng_kit_value)

        voluntary_deductable = data_dictionary['voluntaryDeductible']
        if voluntary_deductable not in NOT_LIST:
            self._add_voluntary_deductable(risk_details, voluntary_deductable)

        idvNonElectrical = data_dictionary['idvNonElectrical']
        if idvNonElectrical not in NOT_LIST:
            self._add_non_electrical(risk_details, idvNonElectrical, year)

        idvElectrical = data_dictionary['idvElectrical']
        if idvElectrical not in NOT_LIST:
            self._add_electrical(risk_details, idvElectrical, year)

        # if data_dictionary['addon_is247RoadsideAssistance'] not in NOT_LIST:
        #     self._add_road_side_assistance(risk_details)

        # if data_dictionary['addon_isInvoiceCover'] not in NOT_LIST:
        #     self._add_invoice_cover(risk_details)

        # if data_dictionary['addon_isDriveThroughProtected'] not in NOT_LIST:
        #     self._add_engine_protection(risk_details)

        # if data_dictionary['addon_isNcbProtection'] not in NOT_LIST:
        #     self._add_ncb_protection(risk_details)

        # if data_dictionary['addon_isDepreciationWaiver'] not in NOT_LIST:
        #     self._add_zero_depreciation(risk_details)

        # if data_dictionary['addon_isKeyReplacement'] not in NOT_LIST:
        #     self._add_key_replacement(risk_details)

        if data_dictionary['extra_isLegalLiability'] not in NOT_LIST:
            self._add_legal_liability_for_paid_driver(risk_details)

        if int(data_dictionary['extra_paPassenger']) > 0:
            self._add_pa_unnamed(risk_details, data_dictionary[
                                 'extra_paPassenger'], vehicle.seating_capacity)

        root['Children']['ProposalDetails'][
            'Children']['RiskDetails'] = risk_details
        root['Children']['ProposalDetails']['Children'][
            'GeneralProposalInformation'] = general_information

        nominee = {
            'name': form_details['nominee_name'],
            'relationship': form_details['nominee_relationship'],
            'age': form_details['nominee_age'],
            'apointee': form_details['appointee_name'],
            'appointee_relationship': form_details['appointee_relationship'],
        }
        self._add_nominee_apointee_details(risk_details, nominee)

        if not is_new_vehicle:
            previous_policy_details = {
                'number': form_details['past_policy_number'],
                'insurer': form_details['past_policy_insurer'],
                'address': form_details['past_policy_address'],
                'city': form_details['past_policy_city'],
                'state': form_details['past_policy_state'],
                'pincode': '999999',  # TODO - Add proper pincode
                'year': form_details['past_policy_start_date'].split('/')[2],
                'start_date': form_details['past_policy_start_date'],
                'end_date': form_details['past_policy_end_date'],
                'office_code': '999999',
                'risk_code': '',
                'nature_of_loss': '',
                'number_of_claims': form_details['number_of_claims'],
                'claim_amount': form_details['claim_amount'],
            }

            root['Children']['ProposalDetails']['Children'][
                'PreviousPolicyDetails'] = self._get_previous_policy_details(previous_policy_details)
            root['Children']['ProposalDetails']['Children']['GeneralProposalInformation'][
                'Children']['PropPreviousPolicyDetails_Validate']['Value'] = 'True'
            root['Children']['ProposalDetails']['Children']['GeneralProposalInformation']['Children'][
                'PropPreviousPolicyDetails_PreviousPolicyType']['Value'] = form_details['past_policy_type']
            root['Children']['ProposalDetails']['Children']['GeneralProposalInformation']['Children'][
                'PropPreviousPolicyDetails_NCBPercentage']['Value'] = data_dictionary['previousNCB']

        if not forPremium:
            root['Children']['ProposalDetails']['Children']['RiskDetails']['Children'][
                'Block']['Children']['PropRisks_RegistrationAddressSameComm']['Value'] = 'False'
            if form_details['financier_code'] in FINANCIER_LIST.keys():
                root['Children']['ProposalDetails']['Children'][
                    'FinancierDetails'] = self._get_financer_details(form_details['financier_code'])
                root['Children']['ProposalDetails']['Children']['GeneralProposalInformation'][
                    'Children']['PropFinancierDetails_Validate']['Value'] = 'True'
                root['Children']['ProposalDetails']['Children']['GeneralProposalInformation'][
                    'Children']['PropParameters_FinancierApplicability']['Value'] = 'True'

            root['Children'][
                'CustomerDetails'] = self._get_customer_details(form_details)

        request_etree = insurer_utils.convert_complex_dict_to_complex_etree(
            'Root', root)
        request_xml = etree.tostring(request_etree)

        # print_request_xml = etree.tostring(request_etree, pretty_print=True)
        return request_xml


def get_cache_key(request_data):
    if request_data['idv'] != '0':
        return None

    caching_keys = ['isUsedVehicle', 'isNewVehicle', 'vehicleId', 'idv',
                    'idvElectrical', 'idvNonElectrical',
                    'isCNGFitted', 'cngKitValue', 'voluntaryDeductible']

    hashing_dictionary = {}
    for key in caching_keys:
        if request_data.get(key, None):
            hashing_dictionary[key] = request_data[key]

    rto_info = "-".join(request_data['registrationNumber[]'][0:2])

    hashing_dictionary['registrationRTO'] = rto_info

    if not request_data['isNewVehicle'] == '1':
        # hashing_dictionary['registrationDate'] = request_data['registrationDate']
        if not request_data['isUsedVehicle'] == '1':
            if request_data['isClaimedLastYear'] == '0':
                hashing_dictionary['previousNCB'] = request_data['previousNCB']
            hashing_dictionary['isClaimedLastYear'] = request_data[
                'isClaimedLastYear']
            # hashing_dictionary['pastPolicyExpiryDate'] = request_data[
            #     'pastPolicyExpiryDate']
            try:
                registration_date = datetime.datetime.strptime(
                    request_data['registrationDate'], '%d-%m-%Y')
                past_policy_end_date = datetime.datetime.strptime(
                    request_data['pastPolicyExpiryDate'], '%d-%m-%Y')
                cache_date = past_policy_end_date + relativedelta(months=3)
            except Exception as e:
                log_error(request_data, msg=str(e))
            else:
                hashing_dictionary['Age'] = math.floor(
                    insurer_utils.get_age(cache_date, registration_date))

    hashing_dictionary['insurer'] = INSURER_SLUG

    hashing_json = json.dumps(hashing_dictionary, sort_keys=True)
    store_key = hashlib.md5(hashing_json).hexdigest()
    return store_key

PremiumCalculator = PremiumCalculator()
ProposalGenerator = ProposalGenerator()
