from motor_product.models import Vehicle, Insurer, VehicleMaster, VehicleMapping
from motor_product import parser_utils

INSURER_SLUG = 'tata-aig'
INSURER_MASTER_PATH = 'motor_product/insurers/tata_aig/scripts/data/tata_fourwheeler.csv'
MASTER_PATH = 'motor_product/scripts/fourwheeler/data/master_fourwheeler.csv'

csv_parameters = {
    'MANUFACTURER': 2,
    'VEHICLEMODEL': 4,
    'CUBICCAPACITY': 7,
    'SEATINGCAPACITY': 9,
    'TXT_FUEL': 25,
    'TXT_SEGMENTTYPE': 26,
    'TXT_VARIANT': 27,
    'MANUFACTURERCODE': 28,
    'NUM_PARENT_MODEL_CODE': 54,
    'Master': 56,
}

parameters = {
    'make': 2,
    'model_code': 54,
    'model': 4,
    'cc': 7,
    'seating_capacity': 9,
    'fuel_type': 25,
    'vehicle_segment': 26,
    'variant': 27,
    'make_code': 28,
    'master': 56,
}

master_parameters = {
    'make': 0,
    'model': 1,
    'variant': 2,
    'fuel_type': 3,
    'cc': 4,
    'seating_capacity': 5,
    'insurer': 14,  # Refer to master csv
}


def convert_fuel_type_literals_into_fuel_type(fuel_type_literal):
    fuel_type_map = {
        'Petrol': 'Petrol',
        'Diesel': 'Diesel',
        'External CNG': 'External LPG / CNG',
        'CNG': 'CNG / Petrol',
        'LPG': 'LPG / Petrol',
        'Electricity': 'Electricity',
    }

    fuel_type = fuel_type_map.get(fuel_type_literal, None)
    if not fuel_type:
        fuel_type = 'Not Defined'

    return fuel_type


def convert_segment_type_literals_into_segment_type(segment_type_literal):
    segment_map = {
        'High End': 'High End Cars',
        'Compact': 'Compact Cars',
        'Mid Size': 'Midsize Cars',
        'Mini': 'Small Sized Vehicles',
        'MPV SUV': 'Sports and Utility Vehicles',
    }

    segment_type = segment_map.get(segment_type_literal, None)
    if not segment_type:
        segment_type = 'Not Defined'

    return segment_type


def save_data():
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_MASTER_PATH, parameters)
    insurer = Insurer.objects.get(slug=INSURER_SLUG)

    vehicle_objects = []
    for _vehicle in vehicles:
        _vehicle['cc'] = parser_utils.try_parse_into_integer(_vehicle['cc'])
        _vehicle['seating_capacity'] = parser_utils.try_parse_into_integer(_vehicle[
                                                                           'seating_capacity'])
        _vehicle['vehicle_segment'] = convert_segment_type_literals_into_segment_type(_vehicle[
                                                                                      'vehicle_segment'])
        _vehicle['fuel_type'] = convert_fuel_type_literals_into_fuel_type(_vehicle[
                                                                          'fuel_type'])
        _vehicle['insurer'] = insurer

        del _vehicle['master']

        vehicle = Vehicle(**_vehicle)
        print 'Saving vehicle %s' % vehicle.model
        vehicle_objects.append(vehicle)

    Vehicle.objects.bulk_create(vehicle_objects)


def delete_data():
    Vehicle.objects.filter(vehicle_type='Private Car',
                           insurer__slug=INSURER_SLUG).delete()


def delete_mappings():
    VehicleMapping.objects.filter(
        insurer__slug=INSURER_SLUG, master_vehicle__vehicle_type='Private Car').delete()


def create_mappings():
    insurer = Insurer.objects.get(slug=INSURER_SLUG)
    master_vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        MASTER_PATH, master_parameters)
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        INSURER_MASTER_PATH, parameters)

    ivehicles = Vehicle.objects.filter(
        vehicle_type='Private Car', insurer__slug=INSURER_SLUG)

    vehicle_map = {}
    for iv in vehicles:
        _vehicles = ivehicles.filter(make=iv['make'],
                                     model=iv['model'],
                                     variant=iv['variant'],
                                     cc=parser_utils.try_parse_into_integer(iv['cc']),
                                     seating_capacity=parser_utils.try_parse_into_integer(iv['seating_capacity']),
                                     fuel_type=convert_fuel_type_literals_into_fuel_type(iv['fuel_type']))
        if _vehicles.count() > 0:
            vehicle_map[iv['master']] = _vehicles[0]

    mappings = []
    for mv in master_vehicles:
        _master = VehicleMaster.objects.get(make__name=mv['make'], model__name=mv['model'],
                                            variant=mv['variant'], cc=mv['cc'],
                                            fuel_type=mv['fuel_type'],
                                            seating_capacity=mv['seating_capacity'])
        mapping = VehicleMapping()
        mapping.master_vehicle = _master
        mapping.mapped_vehicle = vehicle_map.get(mv['insurer'], None)
        mapping.insurer = insurer

        if mapping.mapped_vehicle:
            print 'Mapping %s to %s' % (_master, mapping.mapped_vehicle)
            mappings.append(mapping)

    VehicleMapping.objects.bulk_create(mappings)
