import copy
import datetime
import json
import math
import putils
from django.conf import settings
from django.db.models import Q
import putils
from motor_product import logger_utils, mail_utils
from motor_product.insurers import insurer_utils
from motor_product.models import (DiscountLoading, Insurer, RTOMaster,
                                  Transaction, Vehicle, VehicleMaster, IntegrationStatistics)
from utils import motor_logger, log_error

from . import custom_dictionaries
from .proposal import ProposalGenerator


INSURER_SLUG = 'universal-sompo'
try:
    INSURER = Insurer.objects.get(slug=INSURER_SLUG)
    INSURER_ID = INSURER.id
    INSURER_NAME = INSURER.title
except:
    INSURER_ID = 0
    INSURER_NAME = ''

suds_logger = logger_utils.getLogger(INSURER_SLUG)

PREVIOUS_NCB_TO_NEW = {
    '0': '20',
    '20': '25',
    '25': '35',
    '35': '45',
    '45': '50',
    '50': '50',
}

DEPRECIATION_FACTOR = {
    0: 0.95,
    0.5: 0.85,
    1: 0.80,
    2: 0.70,
    3: 0.60,
    4: 0.50,
    5: 0.475,
    6: 0.4512,
    7: 0.4287,
    8: 0.4073,
    9: 0.3869,
}


class Configuration(object):
    def __init__(self, vehicle_type):

        self.VEHICLE_TYPE = vehicle_type
        if vehicle_type == 'Private Car':
            self.VEHICLE_AGE_LIMIT = 10
            self.URL_TAG = 'fourwheeler'
            self.VEHICLE_CLASS = 45
            self.VEHICLE_CLASS_NAME = 'Private Car'
            self.PRODUCT_CODE = 2311

            self.TP_PREMIUM_BASIC_DICTIONARY_OLD = {
                0: 1468,
                1: 1598,
                2: 4931,
            }

            self.TP_PREMIUM_BASIC_DICTIONARY_NEW = {
                0: 2055,
                1: 2237,
                2: 6164,
            }

            self.TARIFF_RATES_DICTIONARY = {
                0: {
                    'B': {
                        0: 3.039,
                        1: 3.191,
                        2: 3.343,
                    },
                    'A': {
                        0: 3.127,
                        1: 3.283,
                        2: 3.440,
                    }
                },
                1: {
                    'B': {
                        0: 3.191,
                        1: 3.351,
                        2: 3.510,
                    },
                    'A': {
                        0: 3.283,
                        1: 3.447,
                        2: 3.612,
                    }
                },
                2: {
                    'B': {
                        0: 3.267,
                        1: 3.430,
                        2: 3.594,
                    },
                    'A': {
                        0: 3.362,
                        1: 3.529,
                        2: 3.698,
                    }
                },
            }

            self.CC_LIMITS = [1000, 1500]

            self.ZERO_DEPRECIATION_MAP = {
                1: {
                    0: 0.45,
                    1: 0.65,
                    2: 0.85,
                    3: 1.1,
                    4: 1.2,
                    # 5: 1.3,
                    # 6: 1.3,
                },
                2: {
                    0: 0.6,
                    1: 0.8,
                    2: 1.0,
                    3: 1.1,
                    4: 1.15,
                    # 5: 1.25,
                    # 6: 1.25,
                },
                3: {
                    0: 0.55,
                    1: 0.75,
                    2: 0.95,
                    3: 1.05,
                    4: 1.1,
                    # 5: 1.2,
                    # 6: 1.2,
                },
            }

            self.ENGINE_PROTECT_MAP = {
                0: {
                    # 0 to 800
                    0: 0.126,
                    1: 0.14,
                    2: 0.16,
                    3: 0.19,
                    4: 0.24,
                },
                1: {
                    # 801 to 1000
                    0: 0.119,
                    1: 0.13,
                    2: 0.15,
                    3: 0.18,
                    4: 0.23,
                },
                2: {
                    # 1001 to 1200
                    0: 0.112,
                    1: 0.12,
                    2: 0.14,
                    3: 0.17,
                    4: 0.21,
                },
                3: {
                    # 1201 to 1500
                    0: 0.098,
                    1: 0.11,
                    2: 0.12,
                    3: 0.15,
                    4: 0.19,
                },
                4: {
                    # 1501 to 2000
                    0: 0.091,
                    1: 0.1,
                    2: 0.12,
                    3: 0.14,
                    4: 0.17,
                },
                5: {
                    # 2001 and above
                    0: 0.084,
                    1: 0.09,
                    2: 0.11,
                    3: 0.13,
                    4: 0.16,
                },
            }

            self.ENGINE_PROTECT_CC_SLABS = [800, 1000, 1200, 1500, 2000]

        elif vehicle_type == 'Twowheeler':
            self.VEHICLE_AGE_LIMIT = 10
            self.URL_TAG = 'twowheeler'
            self.VEHICLE_CLASS = 37
            self.VEHICLE_CLASS_NAME = 'Two Wheeler'
            self.PRODUCT_CODE = 2312

            self.TP_PREMIUM_BASIC_DICTIONARY_OLD = {
                0: 519,
                1: 538,
                2: 554,
                3: 884,
            }
            self.TP_PREMIUM_BASIC_DICTIONARY_NEW = {
                0: 569,
                1: 619,
                2: 693,
                3: 796,
            }

            self.TARIFF_RATES_DICTIONARY = {
                0: {
                    'B': {
                        0: 1.676,
                        1: 1.676,
                        2: 1.760,
                        3: 1.844,
                    },
                    'A': {
                        0: 1.708,
                        1: 1.708,
                        2: 1.793,
                        3: 1.879,
                    }
                },
                1: {
                    'B': {
                        0: 1.760,
                        1: 1.760,
                        2: 1.848,
                        3: 1.936,
                    },
                    'A': {
                        0: 1.793,
                        1: 1.793,
                        2: 1.883,
                        3: 1.973,
                    }
                },
                2: {
                    'B': {
                        0: 1.802,
                        1: 1.802,
                        2: 1.892,
                        3: 1.982,
                    },
                    'A': {
                        0: 1.836,
                        1: 1.836,
                        2: 1.928,
                        3: 2.020,
                    }
                },
            }

            self.CC_LIMITS = [75, 150, 350]

            self.ZERO_DEPRECIATION_MAP = {
                0: {
                    0: 0.4,
                    1: 0.48,
                    2: 0.56,
                    3: 0.64,
                    4: 0.72,
                },
                1: {
                    0: 0.5,
                    1: 0.58,
                    2: 0.66,
                    3: 0.74,
                    4: 0.82,
                },
                2: {
                    0: 0.6,
                    1: 0.68,
                    2: 0.76,
                    3: 0.84,
                    4: 0.92,
                },
            }

            self.ZERO_DEPRECIATION_CC_SLABS = [145, 300]

            self.ENGINE_PROTECT_MAP = {}

            self.ENGINE_PROTECT_CC_SLABS = []


class PremiumCalculator(object):
    def __init__(self, integrationUtils, configuration):
        self.parent = integrationUtils
        self.Configuration = configuration

    def _get_depreciation_factor(self, vehicle_age):
        if 0.5 <= vehicle_age < 1:
            depreciation_index = 0.5
        else:
            depreciation_index = math.floor(vehicle_age)
        return DEPRECIATION_FACTOR[depreciation_index]

    def _get_exshowroom_price(self, master_vehicle_id):
        master_vehicle = VehicleMaster.objects.get(id=master_vehicle_id)
        vehicles = master_vehicle.sub_vehicles.filter(insurer__slug='bharti-axa')
        ex_showroom_price = None
        if len(vehicles) > 0:
            ex_showroom_price = float(vehicles[0].ex_showroom_price)
        else:
            motor_logger.info('======== NO VEHICLE FOUND FOR INSURER - %s - VEHICLE - %s ===========' % (
                'bharti-axa in universal-sompo', master_vehicle))
        return ex_showroom_price

    def get_idv(self, ex_showroom_price, vehicle_age):
        idv = ex_showroom_price * self._get_depreciation_factor(vehicle_age)
        min_idv = 0.8 * idv
        max_idv = 1.2 * idv
        return idv, min_idv, max_idv

    def get_premium(self, data_dictionary):
        # Turn of Universal for MARUTI 25th Jan
        vm = VehicleMaster.objects.get(id=data_dictionary['masterVehicleId'])
        if vm.make.name == 'Maruti':
            return {'success': False, 'error_code': IntegrationStatistics.COVERFOX_MAKE_RESTRICTED}

        if data_dictionary['isUsedVehicle'] == '1':
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_USED_VEHICLE_RESTRICTED}

        is_new_vehicle = True if data_dictionary['isNewVehicle'] == '1' else False

        if 'newPolicyStartDate' not in data_dictionary.keys():
            data_dictionary['newPolicyStartDate'] = (datetime.datetime.now() + datetime.timedelta(days=1)).strftime("%d-%m-%Y")

        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(data_dictionary['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(data_dictionary['pastPolicyExpiryDate'], "%d-%m-%Y")

            # Check if policy is expired
            if past_policy_end_date.replace(hour=23, minute=59, second=0) < datetime.datetime.now():
                # No ncb if policy end date before 90 days
                if past_policy_end_date.replace(
                        hour=23,
                        minute=59,
                        second=0) < datetime.datetime.now() - datetime.timedelta(days=90):
                    data_dictionary['isClaimedLastYear'] = '1'

                new_policy_start_date = datetime.datetime.strptime(
                    data_dictionary['newPolicyStartDate'], "%d-%m-%Y")
            else:
                new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

        # Vehicle age calculation
        date_of_registration_or_manufacture = datetime.datetime.strptime(
            data_dictionary['registrationDate'], '%d-%m-%Y'
        )

        if is_new_vehicle:
            vehicle_age = 0
            vehicle_age_proper = 0
        else:
            new_policy_start_date = new_policy_start_date.replace(microsecond=0)
            vehicle_age = _get_age(new_policy_start_date, date_of_registration_or_manufacture)
            vehicle_age_proper = insurer_utils.get_age(new_policy_start_date, date_of_registration_or_manufacture)

        if int(math.floor((vehicle_age_proper))) >= self.Configuration.VEHICLE_AGE_LIMIT:
            motor_logger.info('Vehicle age limit not valid for this insurer %s.' % INSURER_SLUG)
            return {'success': False, 'error_code': IntegrationStatistics.INSURER_AGE_RESTRICTED}

        if self.Configuration.VEHICLE_TYPE == 'Twowheeler':
            if not is_new_vehicle and past_policy_end_date.replace(
                    hour=23, minute=59, second=0) < datetime.datetime.now():
                motor_logger.info('Expired policy not allowed for this insurer %s.' % INSURER_SLUG)
                return {'success': False, 'error_code': IntegrationStatistics.INSURER_EXPIRED_POLICY_RESTRICTED}

        # Vehicle cc
        vehicle = Vehicle.objects.get(id=data_dictionary['vehicleId'])
        cc = vehicle.cc

        # Vehicle rto calculation
        split_registration_no = data_dictionary.get('registrationNumber[]')
        vehicle_rto = '-'.join(split_registration_no[0:2])

        tariff_rate = self._get_tariff_rate(vehicle_age, vehicle_rto, cc)

        # For IDV calculation (iffco-tokio idv is used)
        past_policy_expiry_date = None
        if not is_new_vehicle:
            past_policy_expiry_date = datetime.datetime.strptime(data_dictionary['pastPolicyExpiryDate'], "%d-%m-%Y")

        ex_showroom_price = vehicle.ex_showroom_price

        if not ex_showroom_price:
            master_vehicle_id = data_dictionary['masterVehicleId']
            ex_showroom_price = self._get_exshowroom_price(master_vehicle_id)

        ex_showroom_price = int(ex_showroom_price)
        idv, min_idv, max_idv = self.get_idv(ex_showroom_price, vehicle_age)

        idv = float(data_dictionary['idv'])
        if idv < min_idv:
            idv = min_idv
        elif idv > max_idv:
            idv = max_idv

        ex_showroom_price = idv / self._get_depreciation_factor(vehicle_age)
        data_dictionary['idv'] = idv
        data_dictionary['minIdv'] = min_idv
        data_dictionary['maxIdv'] = max_idv

        voluntaryDeductibleAmount = int(data_dictionary['voluntaryDeductible'])
        basicOd = (idv * tariff_rate) / 100
        voluntaryDeductible = insurer_utils.get_voluntary_deductible_discount_dict(vehicle.vehicle_type, basicOd)[
            voluntaryDeductibleAmount]
        electrical = int(data_dictionary['idvElectrical']) * 0.04
        nonElectrical = int(data_dictionary['idvNonElectrical']) * tariff_rate * 0.01
        externalCngLpg = 0
        inBuiltCNG = 0

        if data_dictionary['isCNGFitted'] == '1':
            if data_dictionary['cngKitValue'] != '0':
                externalCngLpg = int(data_dictionary['cngKitValue']) * 0.04
            else:
                inBuiltCNG = (basicOd + electrical + nonElectrical) * 0.05

        totalOd = (basicOd + electrical + nonElectrical + externalCngLpg + inBuiltCNG)

        newNCB = 0
        if not is_new_vehicle and data_dictionary['isClaimedLastYear'] == '0':
            newNCB = int(PREVIOUS_NCB_TO_NEW[data_dictionary['previousNCB']])

        # Apply discount if any
        discount = self._get_discount(vehicle, vehicle_age, vehicle_rto, newNCB)
        special_discount = 0

        # Code discount disabled for now
        # dc = insurer_utils.get_discount_code(data_dictionary.get('discountCode', None))
        # if dc:
        #     discount_from_code = dc.get_discount(INSURER_SLUG, data_dictionary)
        #     if discount_from_code:
        #         special_discount += discount_from_code

        if vehicle.vehicle_type == 'Private Car':
            # customer_occupation = data_dictionary['extra_user_occupation']
            customer_dob_str = data_dictionary.get('extra_user_dob', None)
            if customer_dob_str:
                customer_dob = datetime.datetime.strptime(customer_dob_str, '%d-%m-%Y')
                customer_age = insurer_utils.get_age(datetime.datetime.now(), customer_dob)

                if customer_age > 25:
                    special_discount -= 0  # Discount disabled for now (5)

        if discount + special_discount < -65:
            special_discount = -(65 + discount)

        otherDiscountsIdv = totalOd

        antiTheftDiscount = 0
        if data_dictionary['extra_isAntiTheftFitted'] == '1':
            antiTheftDiscount = (-1) * min(totalOd * 2.5 * 0.01, 500)

        memberOfAutoAssociationDiscountCapping = 50
        if vehicle.vehicle_type == 'Private Car':
            memberOfAutoAssociationDiscountCapping = 200

        memberOfAutoAssociationDiscount = 0
        if data_dictionary['extra_isMemberOfAutoAssociation'] == '1':
            memberOfAutoAssociationDiscount = (-1) * min(totalOd * 5 * 0.01, memberOfAutoAssociationDiscountCapping)

        odDiscountIdv = totalOd
        odDiscount = totalOd * discount * 0.01
        specialOdDiscount = totalOd * special_discount * 0.01
        totalOd = totalOd + odDiscount + specialOdDiscount

        ncbDiscount = 0
        if not is_new_vehicle and data_dictionary['isClaimedLastYear'] == '0':
            ncbDiscount = (-1) * newNCB * (
                totalOd + voluntaryDeductible + antiTheftDiscount + memberOfAutoAssociationDiscount) * 0.01

        cc_slab = self._get_cc_slot(cc)
        # for new premium after 1st april

        if new_policy_start_date.date() <= datetime.date(2016, 03, 31):
            basicTp = self.Configuration.TP_PREMIUM_BASIC_DICTIONARY_OLD[cc_slab]
        else:
            basicTp = self.Configuration.TP_PREMIUM_BASIC_DICTIONARY_NEW[cc_slab]

        # TPPD Discount (only available in twowheelers)
        tppdDiscount = 0
        if self.Configuration.VEHICLE_TYPE == 'Twowheeler' and data_dictionary.get('extra_isTPPDDiscount', '0') == '1':
            tppdDiscount = -50

        paPassenger = insurer_utils.get_pa_passenger_dict(vehicle.vehicle_type, vehicle.seating_capacity)[
            int(data_dictionary['extra_paPassenger'])]

        addon_covers = []
        addon_covers_with_extra_fields = []

        zeroDepRate, zeroDep = self._get_zero_depreciation_premium(vehicle.vehicle_type, vehicle_age_proper, cc, idv)
        if data_dictionary['addon_isDepreciationWaiver'] == '1' and zeroDep is not None:
            addon_covers.append({
                'id': 'isDepreciationWaiver',
                'default': '0',
                'premium': round(zeroDep, 2),
            })
            addon_covers_with_extra_fields.append({
                'id': 'isDepreciationWaiver',
                'default': '0',
                'rate': zeroDepRate,
                'premium': round(zeroDep, 2),
                'extra': 'isDepreciationWaiver',
                'sumInsured': idv,
            })

        # engineProtectRate, engineProtect = self._get_engine_protect_premium(vehicle_age_proper, cc, idv)
        # if data_dictionary['addon_isEngineProtector'] == '1' and engineProtect != None:
        #     addon_covers.append({
        #         'id' : 'isEngineProtector',
        #         'default' : '0',
        #         'premium' : round(engineProtect, 2),
        #     })
        #     addon_covers_with_extra_fields.append({
        #         'id' : 'isEngineProtector',
        #         'default' : '0',
        #         'rate' : engineProtectRate,
        #         'premium' : round(engineProtect, 2),
        #         'extra' : 'isEngineProtector',
        #         'sumInsured' : idv,
        #     })

        paOwnerDriver = 100
        if vehicle.vehicle_type == 'Twowheeler':
            paOwnerDriver = 50

        basic_covers = [
            {
                'id': 'basicOd',
                'default': '1',
                'premium': round(basicOd, 2),
            },
            {
                'id': 'basicTp',
                'default': '1',
                'premium': round(basicTp, 2),
            },
            {
                'id': 'paOwnerDriver',
                'default': '1',
                'premium': paOwnerDriver,  # Constant value
            },
            {
                'id': 'idvElectrical',
                'default': '1',
                'premium': round(electrical, 2),
            },
            {
                'id': 'idvNonElectrical',
                'default': '1',
                'premium': round(nonElectrical, 2),
            },
            {
                'id': 'paPassenger',
                'default': '1',
                'premium': paPassenger,
            }
        ]

        if data_dictionary['isCNGFitted'] == '1':
            basic_covers.append({
                'id': 'cngKitTp',
                'default': '1',
                'premium': 60,
            })
            if data_dictionary['cngKitValue'] == '0':
                basic_covers.append({
                    'id': 'cngKit',
                    'default': '1',
                    'premium': round(inBuiltCNG, 2),
                })
            else:
                basic_covers.append({
                    'id': 'cngKit',
                    'default': '1',
                    'premium': round(externalCngLpg, 2),
                })

        basic_covers_with_extra_fields = [
            {
                'id': 'basicOd',
                'default': '1',
                'premium': round(basicOd, 2),
                'extra': 'basicOd',
                'sumInsured': idv,
            },
            {
                'id': 'basicTp',
                'default': '1',
                'premium': round(basicTp + tppdDiscount, 2),
                'extra': 'basicTp',
                'sumInsured': idv,
            },
            {
                'id': 'paOwnerDriver',
                'default': '1',
                'premium': paOwnerDriver,  # Constant value
                'extra': 'paOwnerDriver',
                'sumInsured': idv,
            },
            {
                'id': 'idvElectrical',
                'default': '1',
                'premium': round(electrical, 2),
                'extra': 'idvElectrical',
                'sumInsured': int(data_dictionary['idvElectrical']),
            },
            {
                'id': 'idvNonElectrical',
                'default': '1',
                'premium': round(nonElectrical, 2),
                'extra': 'idvNonElectrical',
                'sumInsured': int(data_dictionary['idvNonElectrical']),
            },
            {
                'id': 'paPassenger',
                'default': '1',
                'premium': paPassenger,
                'extra': 'paPassenger',
                'sumInsured': vehicle.seating_capacity * int(data_dictionary['extra_paPassenger']),
            }
        ]

        cngKitExtra = 'cngKitTp'
        if data_dictionary['isCNGFitted'] == '1':
            if data_dictionary['cngKitValue'] == '0':
                cngKitExtra = 'cngKitInBuiltTp'
                basic_covers_with_extra_fields.append({
                    'id': 'cngKit',
                    'default': '1',
                    'premium': round(inBuiltCNG, 2),
                    'extra': 'cngKitInBuilt',
                    'sumInsured': idv,
                })
            else:
                basic_covers_with_extra_fields.append({
                    'id': 'cngKit',
                    'default': '1',
                    'premium': round(externalCngLpg, 2),
                    'extra': 'cngKit',
                    'sumInsured': int(data_dictionary['cngKitValue']),
                })

            basic_covers_with_extra_fields.append({
                'id': 'cngKit',
                'default': '1',
                'premium': 60,
                'extra': cngKitExtra,
                'sumInsured': idv,
            })

        if data_dictionary['extra_isLegalLiability'] == '1':
            basic_covers.append({
                'id': 'legalLiabilityDriver',
                'default': '0',
                'premium': 50,  # Constant value
            })
            basic_covers_with_extra_fields.append({
                'id': 'legalLiabilityDriver',
                'default': '0',
                'premium': 50,  # Constant value
                'extra': 'legalLiabilityDriver',
                'sumInsured': 200000,
            })

        discounts = [
            {
                'id': 'ncb',
                'default': '1',
                'premium': round(ncbDiscount, 2),
            },
            {'id': 'odDiscount',
             'default': '1',
             'premium': round(odDiscount, 2),
             }
        ]

        special_discounts = [
            {
                'id': 'voluntaryDeductible',
                'default': '1',
                'premium': round(voluntaryDeductible, 2),
            }
        ]

        if specialOdDiscount != 0:
            special_discounts.append({
                'id': 'extraDiscount',
                'default': '1',
                'premium': round(specialOdDiscount, 2),
            })

        discounts_with_extra_fields = [
            {
                'id': 'ncb',
                'default': '1',
                'premium': round(ncbDiscount, 2),
                'extra': 'ncb',
                'rate': newNCB * 0.01,
                'amount': '',
                'sumInsured': '',
            },
            {
                'id': 'odDiscount',
                'default': '1',
                'premium': round(odDiscount + specialOdDiscount, 2),
                'extra': 'odDiscount',
                'rate': (discount + special_discount) * 0.01,
                'amount': '',
                'sumInsured': round(odDiscountIdv, 2),
            },
            {
                'id': 'voluntaryDeductible',
                'default': '1',
                'premium': round(voluntaryDeductible, 2),
                'extra': 'voluntaryDeductible',
                'rate': '',
                'amount': voluntaryDeductibleAmount,
                'sumInsured': '',
            }
        ]

        if data_dictionary['extra_isAntiTheftFitted'] == '1':
            special_discounts.append({
                'id': 'isAntiTheftFitted',
                'default': '1',
                'premium': round(antiTheftDiscount, 2),
            })
            discounts_with_extra_fields.append({
                'id': 'extra_isAntiTheftFitted',
                'default': '1',
                'premium': round(antiTheftDiscount, 2),
                'extra': 'extra_isAntiTheftFitted',
                'rate': '',
                'amount': '',
                'sumInsured': '',
            })

        if data_dictionary['extra_isMemberOfAutoAssociation'] == '1':
            special_discounts.append({
                'id': 'isMemberOfAutoAssociation',
                'default': '1',
                'premium': round(memberOfAutoAssociationDiscount, 2),
            })
            discounts_with_extra_fields.append({
                'id': 'extra_isMemberOfAutoAssociation',
                'default': '1',
                'premium': round(memberOfAutoAssociationDiscount, 2),
                'extra': 'extra_isMemberOfAutoAssociation',
                'rate': '',
                'amount': '',
                'sumInsured': '',
            })

        if not (tppdDiscount == 0):
            special_discounts.append({
                'id': 'isTPPDDiscount',
                'default': '1',
                'premium': tppdDiscount,
            })

            discounts_with_extra_fields.append({
                'id': 'extra_isTPPDDiscount',
                'default': '1',
                'premium': '',
                'extra': 'extra_isTPPDDiscount',
                'rate': '',
                'amount': '',
                'sumInsured': '6000',
            })

        total_premium = 0
        for cover in basic_covers:
            total_premium = total_premium + cover['premium']

        for cover in addon_covers:
            total_premium = total_premium + cover['premium']

        for discount in discounts:
            total_premium = total_premium + discount['premium']

        for discount in special_discounts:
            total_premium = total_premium + discount['premium']

        serviceTaxRate = 0.145

        service_tax = total_premium * serviceTaxRate

        raw_quote = {'total_premium': total_premium, 'calculatedAtIDV': data_dictionary['idv'],
                     'service_tax': service_tax, 'basicCovers': basic_covers_with_extra_fields,
                     'addonCovers': addon_covers_with_extra_fields, 'discounts': discounts_with_extra_fields,
                     'ex_showroom_price': ex_showroom_price}

        suds_logger.set_name(data_dictionary['quoteId']).log(json.dumps(raw_quote))

        result_data = {
                   'insurerId': INSURER_ID,
                   'insurerSlug': INSURER_SLUG,
                   'insurerName': INSURER_NAME,
                   'isInstantPolicy': True,
                   'minIDV': int(math.ceil(data_dictionary['minIdv'])),
                   'maxIDV': int(math.floor(data_dictionary['maxIdv'])),
                   'calculatedAtIDV': int(math.floor(data_dictionary['idv'])),
                   'serviceTaxRate': serviceTaxRate,
                   'premiumBreakup': {
                       'serviceTax': round(service_tax, 2),
                       'basicCovers': insurer_utils.cpr_update(basic_covers),
                       'addonCovers': insurer_utils.cpr_update(addon_covers),
                       'discounts': insurer_utils.cpr_update(discounts),
                       'specialDiscounts': insurer_utils.cpr_update(special_discounts),
                       'paPassengerAmounts': insurer_utils.get_pa_passenger_dict(vehicle.vehicle_type,
                                                                                 vehicle.seating_capacity),
                   },
               }, raw_quote

        return {'result': result_data, 'success': True}

    def _get_discount(self, vehicle, vehicle_age, rto, ncb):
        insurer = Insurer.objects.get(id=INSURER_ID)

        rto_insurer = RTOMaster.objects.get(rto_code=rto).insurer_rtos.get(insurer__slug=INSURER_SLUG)

        dls_for_insurer = DiscountLoading.objects.filter(insurer=insurer, vehicle_type=vehicle.vehicle_type)

        ncbs = list(set(dls_for_insurer.values_list('ncb', flat=True)))
        ncbs.sort()

        if ncb == 0:
            ncb = None

        ncb_slab_max = None
        for ncb_slab_max in ncbs:
            if ncb_slab_max >= ncb:
                break

        ncb_q = Q(ncb=ncb_slab_max)

        age_q0 = Q(lower_age_limit__lte=vehicle_age) & Q(upper_age_limit__gt=vehicle_age)
        age_q1 = Q(lower_age_limit=None) & Q(upper_age_limit__gt=vehicle_age)
        age_q2 = Q(lower_age_limit__lte=vehicle_age) & Q(upper_age_limit=None)
        age_q3 = Q(lower_age_limit=None) & Q(upper_age_limit=None)
        age_q = age_q0 | age_q1 | age_q2 | age_q3

        cc_q0 = Q(lower_cc_limit__lte=vehicle.cc) & Q(upper_cc_limit__gte=vehicle.cc)
        cc_q1 = Q(lower_cc_limit=None) & Q(upper_cc_limit__gte=vehicle.cc)
        cc_q2 = Q(lower_cc_limit__lte=vehicle.cc) & Q(upper_cc_limit=None)
        cc_q3 = Q(lower_cc_limit=None) & Q(upper_cc_limit=None)
        cc_q = cc_q0 | cc_q1 | cc_q2 | cc_q3

        dls_after_age_cc = dls_for_insurer.filter(ncb_q & age_q & cc_q)

        dls_after_acr = dls_after_age_cc.filter(rtos=rto_insurer, state=None)
        if dls_after_acr.count() == 0:
            dls_after_acr = dls_after_age_cc.filter(state=rto_insurer.master_rto_state)

        if dls_after_acr.count() == 0:
            dls_after_acr = dls_after_age_cc.filter(state=None)

        discount = 0
        dls_f = []

        if vehicle.variant is not None:
            dls_f = dls_after_acr.filter(make=vehicle.make, model=vehicle.model, variant=vehicle.variant)

            if dls_f.count() == 0:
                dls_f = dls_after_acr.filter(make=vehicle.make, model=vehicle.model)
                dls_f = dls_f.extra(
                    where=["LOWER(%s) LIKE LOWER(CONCAT('%%', motor_product_discountloading.variant,'%%'))"],
                    params=[vehicle.variant], select={'lovariant': 'LENGTH(motor_product_discountloading.variant)'},
                    order_by=['-lovariant'])

        if dls_f.count() == 0:
            dls_f = dls_after_acr.filter(make=vehicle.make, model=vehicle.model, variant=None)

            if dls_f.count() == 0:
                dls_f = dls_after_acr.filter(make=vehicle.make, variant=None)
                dls_f = dls_f.extra(
                    where=["LOWER(%s) LIKE LOWER(CONCAT('%%', motor_product_discountloading.model,'%%'))"],
                    params=[vehicle.model], select={'lomodel': 'LENGTH(motor_product_discountloading.model)'},
                    order_by=['-lomodel'])

        if dls_f.count() == 0:
            dls_f = dls_after_acr.filter(make=vehicle.make, model__icontains=vehicle.model)

        if dls_f.count() == 0:
            dls_f = dls_after_acr.filter(make=vehicle.make, model=None, variant=None)

            if dls_f.count() == 0:
                dls_f = dls_after_acr.filter(model=None, variant=None)
                dls_f = dls_f.extra(
                    where=["LOWER(%s) LIKE LOWER(CONCAT('%%', motor_product_discountloading.make,'%%'))"],
                    params=[vehicle.make], select={'lomake': 'LENGTH(motor_product_discountloading.make)'},
                    order_by=['-lomake'])

        if dls_f.count() == 0:
            dls_f = dls_after_acr.filter(make=None, model=None, variant=None)

        dl_make = None
        dl_model = None
        dl_variant = None

        if dls_f.count() == 0:
            discount = 0
        else:
            discount = 0
            if dls_f.count() > 1:
                motor_logger.info('More than one possible discountings found.')
                dls_f = dls_f.order_by('-discount')

            discount = dls_f[0].discount
            dl_make = dls_f[0].make
            dl_model = dls_f[0].model
            dl_variant = dls_f[0].variant
            if discount is None:
                discount = 0
        motor_logger.info('Discount for %s - (%s %s %s) of age %s for RTO location %s is %s' % (
            vehicle, dl_make, dl_model, dl_variant, vehicle_age, rto_insurer.rto_name, discount))
        return discount

    def _get_zone(self, rto):
        rto_zone = RTOMaster.objects.get(rto_code=rto).insurer_rtos.get(insurer__slug=INSURER_SLUG).rto_zone
        return rto_zone

    def _get_cc_slot(self, cc):
        cc_slab = 0
        for cc_limit in self.Configuration.CC_LIMITS:
            if cc_limit >= cc:
                break
            cc_slab += 1
        return cc_slab

    def _get_vehicle_age_slot(self, vehicle_age):
        if vehicle_age <= 5:
            return 0
        elif 5 < vehicle_age <= 10:
            return 1
        else:
            return 2

    def _get_tariff_rate(self, vehicle_age, rto, cc):
        zone = self._get_zone(rto)
        cc_slab = self._get_cc_slot(cc)
        vehicle_age_slot = self._get_vehicle_age_slot(vehicle_age)
        rate = self.Configuration.TARIFF_RATES_DICTIONARY[vehicle_age_slot][zone][cc_slab]
        return rate

    def _get_zero_depreciation_premium(self, vehicle_type, vehicle_age, cc, idv):
        if vehicle_type == 'Private Car':
            vehicle_age_slab = int(math.floor(vehicle_age))
            idv_slab = int(math.ceil(float(idv) / 500000))
            zero_dep_factor = self.Configuration.ZERO_DEPRECIATION_MAP.get(idv_slab, {}).get(vehicle_age_slab, None)
            if zero_dep_factor:
                return zero_dep_factor, idv * zero_dep_factor / 100
            return zero_dep_factor, None
        else:
            vehicle_age_slab = int(math.floor(vehicle_age))
            cc_slab = 0
            for cc_limit in self.Configuration.ZERO_DEPRECIATION_CC_SLABS:
                if cc_limit > cc:
                    break
                cc_slab += 1

            zero_dep_factor = self.Configuration.ZERO_DEPRECIATION_MAP.get(cc_slab, {}).get(vehicle_age_slab, None)
            if zero_dep_factor:
                return zero_dep_factor, idv * zero_dep_factor / 100
            return zero_dep_factor, None

    def _get_engine_protect_premium(self, vehicle_age, cc, idv):
        cc_slab = 0
        for cc_limit in self.Configuration.ENGINE_PROTECT_CC_SLABS:
            if cc_limit > cc:
                break
            cc_slab += 1

        age_slab = int(math.floor(vehicle_age))
        engine_protect_factor = self.Configuration.ENGINE_PROTECT_MAP.get(cc_slab, {}).get(age_slab, None)
        if engine_protect_factor:
            return engine_protect_factor, 50 + engine_protect_factor * idv / 100
        return engine_protect_factor, None


class IntegrationUtils(object):
    PremiumCalculator = None
    ProposalGenerator = None

    def __init__(self, vehicle_type):
        configuration = Configuration(vehicle_type)
        self.Configuration = configuration
        self.PremiumCalculator = PremiumCalculator(self, configuration)
        self.ProposalGenerator = ProposalGenerator(self, configuration)

    def convert_premium_request_data_to_required_dictionary(self, vehicle_id, request_data):
        data = copy.deepcopy(request_data)
        data['masterVehicleId'] = request_data['vehicleId']
        data['vehicleId'] = vehicle_id
        return data

    def get_city_list(self, state):
        return custom_dictionaries.STATE_ID_TO_CITY_LIST.get(state, None)

    def get_buy_form_details(self, quote_parameters, transaction):
        split_registration_no = quote_parameters.get('registrationNumber[]')

        is_new_vehicle = True if quote_parameters['isNewVehicle'] == '1' else False
        if is_new_vehicle:
            registration_number = 'NEW---'
        else:
            registration_number = '-'.join(split_registration_no)

        master_rto_code = "%s-%s" % (split_registration_no[0], split_registration_no[1])

        data = {
            'registration_no': registration_number,
            'rto_location_info': master_rto_code,
            # 'insurers_list' : PAST_INSURANCE_ID_LIST,
            'insurers_list': insurer_utils.MASTER_INSURER_LIST,
            'disallowed_insurers': custom_dictionaries.DISALLOWED_PAST_INSURERS,
            'state_list': custom_dictionaries.STATE_CODE_LIST,
            'relationship_list': custom_dictionaries.NOMINEE_RELATIONSHIP_LIST,
            'is_cities_fetch': True,
        }
        return data

    def convert_proposal_request_data_to_required_dictionary(self, vehicle, request_data, quote_parameters,
                                                             quote_response, quote_custom_response):
        form_details = copy.deepcopy(request_data)

        is_new_vehicle = False
        if quote_parameters['isNewVehicle'] == '1':
            is_new_vehicle = True

        form_details['isNewVehicle'] = is_new_vehicle
        form_details['cust_gender'] = custom_dictionaries.GENDER_MAPPING[form_details['cust_gender']]

        vehicle.ex_showroom_price = int(math.ceil(quote_response['ex_showroom_price']))

        # Just replace - by / in date string
        form_details['cust_dob'] = form_details['cust_dob'].replace('-', '/')
        form_details['vehicle_reg_date'] = quote_parameters['registrationDate'].replace('-', '/')

        form_details['registration_part1'], form_details['registration_part2'], form_details['registration_part3'], \
        form_details['registration_part4'] = form_details['vehicle_reg_no'].split('-')

        if is_new_vehicle:
            form_details['vehicle_reg_no'] = 'NEW'

        master_rto_code = "-".join(quote_parameters['registrationNumber[]'][0:2])
        rto = RTOMaster.objects.get(rto_code=master_rto_code).insurer_rtos.get(insurer__slug=INSURER_SLUG)
        rto_name = rto.rto_name
        rto_code = rto.rto_code
        rto_zone = rto.rto_zone
        form_details['past_policy_insurer_city'] = rto_name

        form_details['vehicle_rto'] = master_rto_code
        form_details['vehicle_rto_name'] = rto_name + '-' + rto_code
        form_details['vehicle_rto_zone'] = rto_zone
        form_details['manufacture_year'] = quote_parameters['manufacturingDate'].split('-')[2]
        form_details['vehicle_type'] = 'NEW'  # TODO

        if self.Configuration.VEHICLE_TYPE == 'Private Car':
            vehicle.body_type = vehicle.raw_data.split('|')[
                14]  # Refer to the saving script, To be changed for new master
        else:
            vehicle.body_type = vehicle.raw_data.split('|')[12]

        form_details['vehicle'] = vehicle

        pincode = form_details['add_pincode']
        current_date = datetime.datetime.now()

        if not is_new_vehicle:
            pyp_end_date = datetime.datetime.strptime(quote_parameters['pastPolicyExpiryDate'], "%d-%m-%Y")
            pyp_start_date = putils.subtract_years(pyp_end_date, 1) + datetime.timedelta(days=1)
            inception_date = pyp_end_date + datetime.timedelta(days=1)
            form_details['past_policy_start_date'] = pyp_start_date.strftime('%d/%m/%Y')
            form_details['past_policy_end_date'] = pyp_end_date.strftime('%d/%m/%Y')
        else:
            inception_date = datetime.datetime.strptime(quote_parameters['newPolicyStartDate'], '%d-%m-%Y')

        expiry_date = putils.add_years(inception_date, 1) - datetime.timedelta(days=1)
        form_details['proposal_date'] = current_date.strftime('%d/%m/%Y')
        form_details['inception_date'] = inception_date.strftime('%d/%m/%Y')
        form_details['expiry_date'] = expiry_date.strftime('%d/%m/%Y')
        return form_details

    @staticmethod
    def get_transaction_from_id(transaction_id):
        return Transaction.objects.get(transaction_id=transaction_id)

    def update_transaction_details(self, transaction, data):
        is_new_vehicle = data['isNewVehicle'] == '1'
        if is_new_vehicle:
            new_policy_start_date = datetime.datetime.strptime(
                data['newPolicyStartDate'], "%d-%m-%Y")
        else:
            past_policy_end_date = datetime.datetime.strptime(
                data['pastPolicyExpiryDate'], "%d-%m-%Y")
            new_policy_start_date = past_policy_end_date + datetime.timedelta(days=1)

        new_policy_end_date = putils.add_years(
            new_policy_start_date, 1) - datetime.timedelta(days=1)

        transaction.policy_start_date = new_policy_start_date
        transaction.policy_end_date = new_policy_end_date
        transaction.save()

    def get_payment_data(self, transaction):
        payment_url = ProposalGenerator.PAYMENT_URL % (transaction.proposal_number, transaction.premium_paid)
        transaction.payment_request = payment_url
        transaction.save()
        return {}, payment_url

    def post_payment(self, payment_response, transaction):
        """
        SAMPLE RESPONSE:
        MerchantID|QuoteID|WAURNNo|PolicyNo|TxnAmount|AdditionalInfo0|\
        TxnDate|ErrorCode|TxnDescription|DownloadLinkURL|AdditionalInfo1|\
        AdditionalInfo2|AdditionalInfo3|AdditionalInfo4|AdditionalInfo5|\
        SOMPOGINS|QUID000059|3414909965|2311/50122343/01/B00|4842.00|NA|\
        21-01-2015 16:20:35|1001|Paymentsuccessfully|www.google.com|NA|NA|NA|NA|NA

        CHANGED RESPONSE:
        {u'MSG': u'SOMPOGINS|50000387|BICI3658301392||USGI/POSWEB/0059917/00/000|\
        |00000002.00|NA|04-02-2015 19:25:30|1001|Payment successfully.|\
        https://www.usgi.co.in/WAPDF/QUID000460.PDF|NA|NA|NA|NA|NA'}

        NEWEST CHANGED RESPONSE:
        {"MSG": "SOMPOGINS|50007897|DCMP3838376328|USGI/WEBAG/0071677/00/000|3150.00|NA|\
        07-06-2015 10:44:58|1001|Payment successfully.|\
        http://www.usgi.co.in/WAPDF/WAPDFGenerat.aspx?QuoteID=afhTMSQOqthTFAwmvWGdyA==|\
        NA|NA|NA|NA|NA"}}, "type": "payment_response", "insurer": "universal-sompo"}
        """
        motor_logger.info(payment_response)
        redirect_url = '/motor/fourwheeler/payment/failure/'
        is_redirect = True

        resp = payment_response['GET']

        response_list = resp['MSG'].split('|')

        try:
            transaction_from_response = Transaction.objects.get(proposal_number=response_list[1])
            transaction = transaction_from_response
            suds_logger.set_name(transaction.transaction_id).log(json.dumps(payment_response))
        except:
            log_error(motor_logger)

        if response_list[7] == '1001':
            transaction.insured_details_json['payment_response'] = resp
            transaction.payment_response = resp
            transaction.insured_details_json['policy_url'] = response_list[9]
            transaction.policy_number = response_list[3]
            transaction.premium_paid = str(float(response_list[4]))
            transaction.payment_done = True
            transaction.payment_on = datetime.datetime.now()
            transaction.mark_complete()
            redirect_url = '/motor/fourwheeler/payment/success/%s/' % transaction.transaction_id

            mail_utils.send_mail_for_admin({
                'title': 'Policy url for universal-sompo',
                'type': 'policy',
                'data': 'Policy url for transaction %s (CFOX-%s) is %s' % (
                    transaction.transaction_id, transaction.id, transaction.insured_details_json['policy_url'])
            })
        else:
            transaction.status = 'FAILED'
            transaction.save()
            redirect_url += '%s/' % transaction.transaction_id
        return is_redirect, redirect_url, '', {}

    def get_dependent_addons(self, parameters):
        return {'type': 'dynamic'}

    def get_custom_data(self, datatype):
        datamap = {
            # 'insurers' : PAST_INSURANCE_ID_LIST,
            'insurers': insurer_utils.MASTER_INSURER_LIST,
            'states': custom_dictionaries.STATE_CODE_LIST,
            'relationships': custom_dictionaries.NOMINEE_RELATIONSHIP_LIST,
        }
        return datamap.get(datatype, None)

    def disable_cache_for_request(self, request_data):
        if (request_data['isNewVehicle'] != '1' and datetime.datetime.strptime(
            request_data['pastPolicyExpiryDate'], '%d-%m-%Y').replace(hour=23, minute=59, second=0) <
                datetime.datetime.now() and self.Configuration.VEHICLE_TYPE == 'Twowheeler'):
            return True
        return False


def _get_age(new_date, old_date):
    age = (new_date - old_date).days / 365.25
    return age
