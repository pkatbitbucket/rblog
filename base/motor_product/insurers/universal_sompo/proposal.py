import cgi
import logging
from collections import OrderedDict
from xml.etree import ElementTree

from suds.client import Client

from motor_product import logger_utils, mail_utils, parser_utils
from utils import motor_logger, log_error

from . import custom_dictionaries

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)


INSURER_SLUG = 'universal-sompo'


suds_logger = logger_utils.getLogger(INSURER_SLUG)


class ProposalGenerator(object):
    PROPOSAL_WSDL = 'http://www.universal-sompo.net.in:800/webaggwcf/service1.svc?wsdl'
    PAYMENT_URL = 'https://www.usgi.co.in/PaymentGateway/ExtIntityCallPage.aspx?PosPolicyNo=%s&FinalPremium=%s&Src=WA&SubSrc=20000003'
    MISMATCH_MARGIN = 15

    def __init__(self, integrationUtils, configuration):
        self.parent = integrationUtils
        self.Configuration = configuration

    def get_authentication_details(self):
        AuthDetails = """
        <Authentication>
        <WACode>20000003</WACode>
        <WAAppCode>30000003</WAAppCode>
        <WAUserID>USER01</WAUserID>
        <WAUserPwd>pass@123</WAUserPwd>
        <WAType>0</WAType>
        </Authentication>
        """
        return AuthDetails

    def get_payment_details(self):
        PaymentDetails = """
        <PaymentDetails>
        <PaymentEntry>
        <PaymentId></PaymentId>
        <MICRCheque></MICRCheque>
        <InstrumentDate></InstrumentDate>
        <DraweeBankName></DraweeBankName>
        <HOUSEBANKNAME></HOUSEBANKNAME>
        <AmountPaid></AmountPaid>
        <PaymentType></PaymentType>
        <PaymentMode></PaymentMode>
        <InstrumentNo></InstrumentNo>
        <Status></Status>
        <DepositSlipNo></DepositSlipNo>
        <PayerType></PayerType>
        </PaymentEntry>
        </PaymentDetails>
        """
        return PaymentDetails

    def get_financier_details(self, form_details):
        if form_details['is_car_financed'] != '1':
            return ''

        financier = {
            'code': '',
            'name': form_details['car-financier'],
            'branch': ''  # TODO
        }
        FinancierDetails = """
        <FinancierDetails Name="Financier Details">
        <FinancierDtlGrp Type="Group" Name="Financier Dtl Group">
        <FinancierDtlGrpData Type="GroupData">
        <FinancierCode Name="Financier Code" Value="%s" />
        <AgreementType Name="Agreement Type" Value="Hypothecation" />
        <BranchName Name="Branch Name" Value="%s" />
        <FinancierName Name="Financier Name" Value="%s" />
        <SrNo Name="Sr No" Value="1" />
        </FinancierDtlGrpData>
        </FinancierDtlGrp>
        </FinancierDetails>
        """ % (financier['code'], financier['branch'], financier['name'])
        return FinancierDetails

    def get_previous_policy_details(self, form_details):
        if form_details['isNewVehicle']:
            return ''

        past_policy_insurer = custom_dictionaries.PAST_INSURANCE_ID_LIST[form_details['past_policy_insurer']]
        past_policy_insurer_city = form_details['past_policy_insurer_city']
        form_details['past_policy_premium'] = ''  # TODO
        PreviousPolicyDetails = """
        <PreviousPolicyDetails Name="Previous Policy Details">
        <PreviousPolDtlGroup Type="Group" Name="Previous Pol Dtl Group">
        <PreviousPolDtlGroupData Type="GroupData">
        <ProductCode Name="Product Code" Value="" />
        <ClaimSettled Name="Claim Settled" Value="" />
        <ClaimPremium Name="Claim Premium" Value="" />
        <ClaimAmount Name="Claim Amount" Value="" />
        <DateofLoss Name="Date Of Loss" Value="" />
        <NatureofLoss Name="Nature Of Loss" Value="" />
        <ClaimNo Name="Claim No" Value="9999999999" />
        <PolicyEffectiveTo Name="Policy Effective To" Value="%s" />
        <PolicyEffectiveFrom Name="Policy Effective From" Value="%s" />
        <PolicyPremium Name="PolicyPremium" Value="%s" />
        <PolicyNo Name="Policy No" Value="%s" />
        <PolicyYear Name="Policy Year" Value="" />
        <OfficeCode Name="Office Name" Value="" />
        <CorporateCustomerId Name="Corporate Customer Id" Value=" " />
        <InsurerName Name="InsurerName" Value="%s" />
        <InsurerAddress Name="InsurerAddress" Value="%s" />
        </PreviousPolDtlGroupData>
        </PreviousPolDtlGroup>
        <PreviousPolicyType Name="Previous Policy Type" Value="Package Policy" />
        <OfficeAddress Name="Office Address" Value="" />
        </PreviousPolicyDetails>
        """ % (
            form_details['past_policy_end_date'],
            form_details['past_policy_start_date'],
            form_details['past_policy_premium'],
            form_details['past_policy_number'],
            past_policy_insurer,
            past_policy_insurer_city,
        )
        return PreviousPolicyDetails

    def get_general_proposal_information(self, form_details):
        business_type = 'Roll Over'
        if form_details['isNewVehicle']:
            business_type = 'New Vehicle'

        GeneralProposalInformation = """
        <GeneralProposalGroup Name="General Proposal Group">
        <DistributionChannel Name="Distribution Channel">
        <BranchDetails Name="Branch Details">
        <IMDBranchName Name="IMDBranchName" Value="" />
        <IMDBranchCode Name="IMDBranch Code" Value="" />
        </BranchDetails>
        <SPDetails Name="SP Details">
        <SPName Name="SP Name" Value="" />
        <SPCode Name="SP Code" Value="" />
        </SPDetails>
        </DistributionChannel>
        <GeneralProposalInformation Name="General Proposal Information">
        <TypeOfBusiness Name="Type Of Business" Value="FROM INTERMEDIARY" />
        <ServiceTaxExemptionCategory Name="Service Tax Exemption Category" Value="No Exemption" />
        <BusinessType Name="Transaction Type" Value="%s" />
        <Sector Name="Sector" Value="Others" />
        <ProposalDate Name="Proposal Date" Value="%s" />
        <DealId Name="Deal Id" Value="" />
        <PolicyNumberChar Name="PolicyNumberChar" Value="" />
        <VehicleLaidUpFrom Name="VehicleLaidUpFrom" Value="" />
        <VehicleLaidUpTo Name="VehicleLaidUpTo" Value="" />
        <PolicyEffectiveDate Name="Policy Effective Date">
        <Fromdate Name="From Date" Value="%s" />
        <Todate Name="To Date" Value="%s" />
        <Fromhour Name="From Hour" Value="00:00" />
        <Tohour Name="To Hour" Value="23:59" />
        </PolicyEffectiveDate>
        </GeneralProposalInformation>
        </GeneralProposalGroup>
        """ % (
            business_type,
            form_details['proposal_date'],
            form_details['inception_date'],
            form_details['expiry_date'],
        )
        return GeneralProposalInformation

    def get_customer_details(self, form_details):
        address_line_1 = form_details['add_house_no'] + ' ' + form_details['add_building_name']
        address_line_2 = form_details['add_street_name'] + ' ' + form_details['add_landmark']

        CustomerDetails = """
        <Customer>
        <CustomerType>Individual</CustomerType>
        <CustomerName>%s %s</CustomerName>
        <DOB>%s</DOB>
        <Gender>%s</Gender>
        <CanBeParent>0</CanBeParent>
        <ContactTelephoneSTD>-</ContactTelephoneSTD>
        <MobileNo>%s</MobileNo>
        <Emailid>%s</Emailid>
        <PresentAddressLine1>%s</PresentAddressLine1>
        <PresentAddressLine2>%s</PresentAddressLine2>
        <PresentStateCode>%s</PresentStateCode>
        <PresentCityDistCode>%s</PresentCityDistCode>
        <PresentPinCode>%s</PresentPinCode>
        <PermanentAddressLine1>%s</PermanentAddressLine1>
        <PermanentAddressLine2>%s</PermanentAddressLine2>
        <PermanentStateCode>%s</PermanentStateCode>
        <PermanentCityDistCode>%s</PermanentCityDistCode>
        <PermanentPinCode>%s</PermanentPinCode>
        <ProductName>%s Package Policy</ProductName>
        <InstrumentNo>NULL</InstrumentNo>
        <InstrumentDate>NULL</InstrumentDate>
        <BankID>NULL</BankID>
        <PosPolicyNo>WAQuoteNo</PosPolicyNo>
        <ProductCode>%s</ProductCode>
        <WAURN>%s</WAURN>
        <NomineeName>%s</NomineeName>
        <NomineeRelation>%s</NomineeRelation>
        </Customer>
        """ % (form_details['cust_first_name'], form_details['cust_last_name'],
               form_details['cust_dob'],
               form_details['cust_gender'],
               form_details['cust_phone'],
               form_details['cust_email'],
               address_line_1,
               address_line_2,
               form_details['add_state'],
               form_details['add_city'],
               form_details['add_pincode'],
               address_line_1,
               address_line_2,
               form_details['add_state'],
               form_details['add_city'],
               form_details['add_pincode'],
               self.Configuration.VEHICLE_CLASS_NAME,
               self.Configuration.PRODUCT_CODE,
               form_details['transaction_id'],
               form_details['nominee_name'],
               form_details['nominee_relationship'],
               )
        return CustomerDetails

    def get_covers(self, form_details, quote_response, quote_parameters):
        available_covers = OrderedDict([
            ('Basic OD', 'basicOd'),
            ('ELECTRICAL ACCESSORY OD', 'idvElectrical'),
            ('NON ELECTRICAL ACCESSORY OD', 'idvNonElectrical'),
            ('CNGLPG KIT OD', 'cngKit'),
            ('BUILTIN CNGLPG KIT OD', 'cngKitInBuilt'),
            ('Basic TP', 'basicTp'),
            ('PA COVER TO OWNER DRIVER', 'paOwnerDriver'),
            ('CNGLPG KIT TP', 'cngKitTp'),
            ('BUILTIN CNGLPG KIT TP', 'cngKitInBuiltTp'),
            ('UNNAMED PA COVER TO PASSENGERS', 'paPassenger'),
            ('LEGAL LIABILITY TO PAID DRIVER', 'legalLiabilityDriver'),
        ])

        covers = quote_response['basicCovers']
        CoverDetails = ''
        for cover_name, cover_extra in available_covers.items():
            for cover in covers:
                if cover['premium'] == 0:
                    continue
                if cover['extra'] == cover_extra:
                    CoverDetails += """
                    <CoversData Type="GroupData">
                    <Premium Name="Premium" Value="" />
                    <Rate Name="Rate" Value="" />
                    <SumInsured Name="SumInsured" Value="%s" />
                    <Applicable Name="Applicable" Value="True" />
                    <CoverGroups Name="CoverGroups" Value="%s" />
                    </CoversData>
                    """ % (
                        cover['sumInsured'],
                        cover_name
                    )
                    break
        return CoverDetails

    def get_addon_covers(self, quote_response, quote_parameters):
        available_addon_covers = OrderedDict([
            ('NIL DEPRECIATION WAIVER COVER', 'isDepreciationWaiver'),
            ('HYDROSTATIC LOCK COVER', 'isEngineProtector'),
        ])

        covers = quote_response['addonCovers']
        AddonCoverDetails = ''
        for cover_name, cover_extra in available_addon_covers.items():
            for cover in covers:
                if cover['extra'] == cover_extra:
                    AddonCoverDetails += """
                    <AddonCoversData Type="GroupData">
                    <Premium Name="Premium" Value="" />
                    <Rate Name="Rate" Value="%s" />
                    <SumInsured Name="SumInsured" Value="%s" />
                    <Applicable Name="Applicable" Value="True" />
                    <AddonCoverGroups Name="AddonCoverGroups" Value="%s" />
                    </AddonCoversData>
                    """ % (
                        cover['rate'],
                        cover['sumInsured'],
                        cover_name
                    )
                    break
        return AddonCoverDetails

    def get_other_discounts(self, quote_response, quote_parameters):
        available_discounts = OrderedDict([
            # ('De-tariff discount', 'odDiscount'),
            ('Voluntary deductable', 'voluntaryDeductible'),
            ('No claim bonus ', 'ncb'),
            ('Antitheft device discount', 'extra_isAntiTheftFitted'),
            ('Automobile Association discount', 'extra_isMemberOfAutoAssociation'),
            ('TPPD Discount', 'extra_isTPPDDiscount'),
        ])

        discounts = quote_response['discounts']

        OtherDiscountsDetails = ''
        for discount_name, discount_extra in available_discounts.items():
            for discount in discounts:
                discount_amount = discount['amount']
                discount_rate = discount['rate']
                discount_sumInsured = discount['sumInsured']
                if discount['extra'] == discount_extra:
                    OtherDiscountsDetails += """
                    <OtherDiscountGroupData Type="GroupData">
                    <DiscountAmount Name="Discount Amount" Value="%s" />
                    <DiscountRate Name="Discount Rate" Value="%s" />
                    <SumInsured Name="SumInsured" Value="%s" />
                    <Rate Name="Rate" Value="" />
                    <Premium Name="Premium" Value="" />
                    <Applicable Name="Applicable" Value="True" />
                    <Description Name="Description" Value="%s" />
                    </OtherDiscountGroupData>
                    """ % (
                        discount_amount,
                        discount_rate,
                        discount_sumInsured,
                        discount_name,
                    )
                    break
        return OtherDiscountsDetails

    def get_other_loadings(self):
        OtherLoadings = """
        <OtherLoadingGroupData Type="GroupData">
        <LoadingAmount Name="Loading Amount" Value="" />
        <LoadingRate Name="Loading Rate" Value="" />
        <SumInsured Name="SumInsured" Value="" />
        <Rate Name="Rate" Value="" />
        <Premium Name="Premium" Value="" />
        <Applicable Name="Applicable" Value="True" />
        <Description Name="Description" Value="Adverse Claim Loading" />
        </OtherLoadingGroupData>
        <OtherLoadingGroupData Type="GroupData">
        <LoadingAmount Name="Loading Amount" Value="" />
        <LoadingRate Name="Loading Rate" Value="" />
        <SumInsured Name="SumInsured" Value="" />
        <Rate Name="Rate" Value="" />
        <Premium Name="Premium" Value="" />
        <Applicable Name="Applicable" Value="True" />
        <Description Name="Description" Value="Break-In Loading" />
        </OtherLoadingGroupData>
        """
        # return OtherLoadings
        return ''

    def get_detariff_loadings(self, quote_response):
        discounts = quote_response['discounts']
        for discount in discounts:
            if (discount['id'] == 'odDiscount'):
                break
        detariff_rate = discount['rate']
        if detariff_rate > 0:
            detariff_rate_str = str(detariff_rate)
            detariff_sumInsured = discount['sumInsured']
        else:
            detariff_rate_str = ''
            detariff_sumInsured = ''
        DetariffLoadings = """
        <De-tariffLoadingGroupData Type="GroupData">
        <LoadingAmount Name="Loading Amount" Value="" />
        <LoadingRate Name="Loading Rate" Value="%s" />
        <SumInsured Name="SumInsured" Value="%s" />
        <Rate Name="Rate" Value="%s" />
        <Premium Name="Premium" Value="" />
        <Applicable Name="Applicable" Value="True" />
        <Description Name="Description" Value="De-tariff Loading" />
        </De-tariffLoadingGroupData>
        """ % (
            detariff_rate_str,
            detariff_sumInsured,
            detariff_rate_str,
        )
        return DetariffLoadings

    def get_detariff_discounts(self, quote_response):
        discounts = quote_response['discounts']
        for discount in discounts:
            if (discount['id'] == 'odDiscount'):
                break
        detariff_rate = discount['rate']
        if detariff_rate < 0:
            detariff_rate_str = str((-1) * detariff_rate)
            detariff_sumInsured = discount['sumInsured']
        else:
            detariff_rate_str = ''
            detariff_sumInsured = ''

        DetariffDiscounts = """
        <De-tariffDiscountGroupData Type="GroupData">
        <DiscountAmount Name="Discount Amount" Value="" />
        <DiscountRate Name="Discount Rate" Value="%s" />
        <SumInsured Name="SumInsured" Value="%s" />
        <Rate Name="Rate" Value="%s" />
        <Premium Name="Premium" Value="" />
        <Applicable Name="Applicable" Value="True" />
        <Description Name="Description" Value="De-tariff Discount" />
        </De-tariffDiscountGroupData>
        """ % (
            detariff_rate_str,
            detariff_sumInsured,
            detariff_rate_str,
        )
        return DetariffDiscounts

    def get_risk_details(self, form_details, quote_parameters, quote_response):
        vehicle = form_details['vehicle']
        no_of_claims_on_pyp = ''
        if quote_parameters['isClaimedLastYear'] == '1':
            no_of_claims_on_pyp = '1'

        isAntiTheftFitted = 'True' if quote_parameters['extra_isAntiTheftFitted'] == '1' else 'False'

        isMemberOfAutoAssociation = 'False'
        automobileAssociationNumber = '1234'
        if quote_parameters['extra_isMemberOfAutoAssociation'] == '1':
            isMemberOfAutoAssociation = 'True'
            automobileAssociationNumber = form_details['aan_number']

        tppdLimit = ""
        if self.Configuration.VEHICLE_TYPE == 'Twowheeler' and quote_parameters.get('extra_isTPPDDiscount', '0') == '1':
            tppdLimit = "6000"

        RiskDetails = """
        <VehicleClassCode Name="VehicleClassCode" Value="%s" />
        <VehicleMakeCode Name="VehicleMakeCode" Value="%s" />
        <VehicleModelCode Name="VehicleModelCode" Value="%s" />
        <RTOLocationCode Name="RTOLocationCode" Value="%s" />
        <NoOfClaimsOnPreviousPolicy Name="No Of Claims On Previous Policy" Value="%s" />
        <RegistrationNumber Name="Registration Number" Value="%s" />
        <BodyTypeCode Name="BodyTypeCode" Value="%s" />
        <ModelStatus Name="ModelStatus" Value="" />
        <GrossVehicleWeight Name="GrossVehicleWeight" Value="0" />
        <CarryingCapacity Name="CarryingCapacity" Value="%s" />
        <VehicleType Name="VehicleType" Value="%s" />
        <PlaceOfRegistration Name="Place Of Registration" Value="%s" />
        <VehicleModel Name="VehicleModel" Value="%s" />
        <VehicleExShowroomPrice Name="VehicleExShowroomPrice" Value="%s" />
        <DateOfDeliveryOrRegistration Name="DateOfDeliveryOrRegistration" Value="%s" />
        <YearOfManufacture Name="Year Of Manufacture" Value="%s" />
        <DateOfFirstRegistration Name="DateOfFirstRegistration" Value="" />
        <RegistrationNumberSection1 Name="Regn No. Section 1" Value="%s" />
        <RegistrationNumberSection2 Name="Regn No. Section 2" Value="%s" />
        <RegistrationNumberSection3 Name="Regn No. Section 3" Value="%s" />
        <RegistrationNumberSection4 Name="Regn No. Section 4" Value="%s" />
        <EngineNumber Name="Engine Number" Value="%s" />
        <ChassisNumber Name="Chassis Number" Value="%s" />
        <BodyColour Name="Body Colour" Value="" />
        <ExtensionCountryName Name="Extension Country Name" Value="" />
        <RegistrationAuthorityName Name="Registration Authority Name" Value="%s" />
        """ % (
            self.Configuration.VEHICLE_CLASS,
            vehicle.make_code,
            vehicle.model_code,
            form_details['vehicle_rto'],
            no_of_claims_on_pyp,
            form_details['vehicle_reg_no'],
            vehicle.body_type,
            vehicle.seating_capacity,
            form_details['vehicle_type'],
            form_details['vehicle_rto_name'],
            vehicle.model.replace('&', ' '),
            str(vehicle.ex_showroom_price),
            form_details['vehicle_reg_date'],
            form_details['manufacture_year'],
            form_details['registration_part1'],
            form_details['registration_part2'],
            form_details['registration_part3'],
            form_details['registration_part4'],
            form_details['vehicle_engine_no'],
            form_details['vehicle_chassis_no'],
            form_details['vehicle_rto_name']
        )

        RiskDetails += """
        <AutomobileAssocnFlag Name="AutomobileAssocnFlag" Value="%s" />
        <AutomobileAssociationNumber Name="Automobile Association Number" Value="%s" />
        <VoluntaryExcess Name="Voluntary Access" Value="" />
        <TPPDLimit Name="TPPDLimit" Value="%s" />
        <AntiTheftDiscFlag Name="AntiTheftDiscFlag" Value="%s" />
        <HandicapDiscFlag Name="HandicapDiscFlag" Value="" />
        <NumberOfDrivers Name="NumberOfDrivers" Value="0" />
        <NumberOfEmployees Name="NumberOfEmployees" Value="" />
        <TransferOfNCB Name="TransferOfNCB" Value="" />
        <TransferOfNCBPercent Name="TransferOfNCBPercent" Value="" />
        <NCBDeclaration Name="NCBDeclaration" Value="" />
        <PreviousVehicleSaleDate Name="PreviousVehicleSaleDate" Value="" />
        <BonusOnPreviousPolicy Name="BonusOnPreviousPolicy" Value="" />
        <VehicleClass Name="VehicleClass" Value="%s" />
        <VehicleMake Name="VehicleMake" Value="%s" />
        <BodyTypeDescription Name="BodyTypeDescription" Value="" />
        <NumberOfWheels Name="NumberOfWheels" Value="%s" />
        <CubicCapacity Name="CubicCapacity" Value="%s" />
        <SeatingCapacity Name="SeatingCapacity" Value="%s" />
        <RegistrationZone Name="RegistrationZone" Value="%s" />
        """ % (
            isMemberOfAutoAssociation,
            automobileAssociationNumber,
            tppdLimit,
            isAntiTheftFitted,
            self.Configuration.VEHICLE_CLASS_NAME,
            vehicle.make.replace('&', ' '),
            vehicle.number_of_wheels,
            vehicle.cc,
            vehicle.seating_capacity,
            form_details['vehicle_rto_zone'],
        )

        RiskDetails += """
        <VehiclesDrivenBy Name="Vehicles Driven By" Value="" />
        <DriversAge Name="Drivers Age" Value="" />
        <DriversExperience Name="Drivers Experience" Value="" />
        <DriversQualification Name="Drivers Qualification" Value="" />
        <VehicleModelCluster Name="VehicleModelCluster" Value="" />
        <OpenCoverNoteFlag Name="OpenCoverNote" Value="" />
        <LegalLiability Name="LegalLiability" Value="" />
        <PaidDriver Name="PaidDriver" Value="" />
        <NCBConfirmation Name="NCBConfirmation" Value="" />
        <RegistrationDate Name="RegistrationDate" Value="%s" />
        <TPLoadingRate Name="TPLoadingRate" Value="0" />
        <ExtensionCountry Name="ExtensionCountry" Value="" />
        <VehicleAge Name="VehicleAge" Value="" />
        <LocationCode Name="LocationCode" Value="" />
        <RegistrationZoneDescription Name="RegistrationZoneDescription" Value="" />
        <NumberOfWorkmen Name="NumberOfWorkmen" Value="" />
        <VehicCd Name="VehicCd" Value="" />
        <SalesTax Name="Sales Tax" Value="" />
        <ModelOfVehicle Name="Model Of Vehicle" Value="" />
        <PopulateDetails Name="Populate details" Value="" />
        <VehicleIDV Name="VehicleInsuredDeclaredValue" Value="%s" />
        <ShowroomPriceDeviation Name="ShowroomPriceDeviation" Value="" />
        <NewVehicle Name="New Vehicle" Value="" />
        """ % (
            form_details['vehicle_reg_date'],
            int(quote_response['calculatedAtIDV']),
        )
        return RiskDetails

    def get_proposal_xml(self, data_dictionary, transaction):
        quote_response = transaction.raw['quote_raw_response']
        quote_parameters = transaction.quote.raw_data

        proposalXml = """<?xml version="1.0" encoding="utf-8" ?>
        <Root>
        %s
        %s
        <Product Name="%s Package Policy">
        <GeneralProposal Name="General Proposal">
        %s
        %s
        %s
        </GeneralProposal>
        <PremiumCalculation Name="Premium Calculation">
        <NetPremium Name="Net Premium" Value="" />
        <ServiceTax Name="Service Tax" Value="" />
        <StampDuty2 Name="Stamp Duty" Value="" />
        <TotalPremium Name="Total Premium" Value="" />
        </PremiumCalculation>
        <Risks Name="Risks">
        <Risk Type="Group" Name="Risks">
        <RisksData Type="GroupData">
        <De-tariffLoadings Name="De-tariffLoadings">
        <De-tariffLoadingGroup Type="Group" Name="De-tariff Loading Group">
        %s
        </De-tariffLoadingGroup>
        </De-tariffLoadings>
        <De-tariffDiscounts Name="De-tariffDiscounts">
        <De-tariffDiscountGroup Type="Group" Name="De-tariff Discount Group">
        %s
        </De-tariffDiscountGroup>
        </De-tariffDiscounts>
        <CoverDetails Name="CoverDetails">
        <Covers Type="Group" Name="Covers">
        %s
        </Covers>
        </CoverDetails>
        <AddonCoverDetails Name="AddonCoverDetails">
        <AddonCovers Type="Group" Name="AddonCovers">
        %s
        </AddonCovers>
        </AddonCoverDetails>
        <OtherLoadings Name="OtherLoadings">
        <OtherLoadingGroup Type="Group" Name="Other Loading Group">
        %s
        </OtherLoadingGroup>
        </OtherLoadings>
        <OtherDiscounts Name="OtherDiscounts">
        <OtherDiscountGroup Type="Group" Name="Other Discount Group">
        %s
        </OtherDiscountGroup>
        </OtherDiscounts>
        </RisksData>
        </Risk>
        %s
        </Risks>
        </Product>
        %s
        <Errors>
        <ErrorCode>0</ErrorCode>
        <ErrDescription>NULL</ErrDescription>
        </Errors>
        </Root>
        """ % (
            self.get_authentication_details(),
            self.get_customer_details(data_dictionary),
            self.Configuration.VEHICLE_CLASS_NAME,
            self.get_general_proposal_information(data_dictionary),
            self.get_financier_details(data_dictionary),
            self.get_previous_policy_details(data_dictionary),
            self.get_detariff_loadings(quote_response),
            self.get_detariff_discounts(quote_response),
            self.get_covers(data_dictionary, quote_response, quote_parameters),
            self.get_addon_covers(quote_response, quote_parameters),
            self.get_other_loadings(),
            self.get_other_discounts(quote_response, quote_parameters),
            self.get_risk_details(data_dictionary, quote_parameters, quote_response),
            self.get_payment_details(),
        )
        return proposalXml

    def save_policy_details(self, data_dictionary, transaction):
        data = {}

        # Hack for universal-sompo (universal cannot parse & (&amp;) in xml)
        data_dictionary.update({k: data_dictionary.get(k, '').replace('&', '') for k in [
            'add_house_no', 'add_building_name', 'add_street_name', 'add_landmark', 'reg_add_house_no',
            'reg_add_building_name', 'reg_add_street_name', 'reg_add_landmark', 'nominee_name']})

        for key, value in data_dictionary.items():
            if isinstance(value, basestring):
                data[key] = cgi.escape(value)
            else:
                data[key] = value

        client = Client(self.PROPOSAL_WSDL)
        client.set_options(plugins=[suds_logger.set_name(transaction.transaction_id)])

        data['transaction_id'] = transaction.transaction_id

        INPUT_XML = self.get_proposal_xml(data, transaction)
        INPUT_XML = INPUT_XML.replace('\n', '')

        response = client.service.commBRIDGEFusionMOTOR(INPUT_XML)

        try:
            parsed_result = ElementTree.fromstring(response)
        except:
            log_error(motor_logger, msg='Unable to parse proposal response')
        else:
            response_dict = parser_utils.complex_etree_to_dict(parsed_result)

            if int(response_dict['Root']['Errors']['ErrorCode']) == 0:
                policy_no = response_dict['Root']['Customer']['PosPolicyNo']
                total_premium = response_dict['Root']['Product']['PremiumCalculation']['TotalPremium']['@Value']

                quote_without_st_premium = transaction.raw['quote_raw_response']['total_premium']
                quote_service_tax = transaction.raw['quote_raw_response']['service_tax']
                quote_premium = quote_without_st_premium + quote_service_tax

                motor_logger.info(
                    "Premiums for the transaction with id: %s, Quote Premium: %s, Proposal Premium: %s" % (
                        transaction.transaction_id, quote_premium, total_premium
                    )
                )

                isPremiumMismatch = False
                total_premium = float(total_premium)
                if (quote_premium <= total_premium - self.MISMATCH_MARGIN):
                    isPremiumMismatch = True

                if not isPremiumMismatch:
                    transaction.premium_paid = total_premium
                    transaction.proposal_number = policy_no
                    transaction.save()
                    return True

        # Proposal failure response mail
        mail_utils.send_mail_for_proposal_failure(transaction, response)
        return False
