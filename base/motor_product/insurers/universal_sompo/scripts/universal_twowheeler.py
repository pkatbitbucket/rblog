from motor_product import models as motor_models
from motor_product import parser_utils

INSURER_SLUG = 'universal-sompo'
INSURER_MASTER_PATH = 'motor_product/insurers/universal_sompo/scripts/data/universal_twowheeler.csv'
MASTER_PATH = 'motor_product/scripts/twowheeler/data/master_twowheeler.csv'

csv_parameters = {
    'MANUFACTURER': 1,
    'VEHICLEMODELCODE': 2,
    'VEHICLEMODEL': 3,
    'CUBICCAPACITY': 5,
    'SEATINGCAPACITY': 7,
    'VEHICLEMANUFACTURERCODE': 25,
    'Master': 26,
    'EXSH': 27,
}

parameters = {
    'make': 1,
    'model_code': 2,
    'model': 3,
    'cc': 5,
    'seating_capacity': 7,
    'make_code': 25,
    'master': 26,
    'ex_showroom_price': 27,
}

master_parameters = {
    'make': 0,
    'model': 1,
    'variant': 2,
    'cc': 3,
    'insurer': 6,  # Refer to master csv
}


def delete_data():
    motor_models.Vehicle.objects.filter(
        vehicle_type='Twowheeler',
        insurer__slug=INSURER_SLUG
    ).delete()


def delete_mappings():
    motor_models.VehicleMapping.objects.filter(
        insurer__slug=INSURER_SLUG,
        master_vehicle__vehicle_type='Twowheeler'
    ).delete()


def save_data():
    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(INSURER_MASTER_PATH, parameters)

    vehicle_objects = []
    for _vehicle in vehicles:
        _vehicle['cc'] = parser_utils.try_parse_into_integer(_vehicle['cc'])
        _vehicle['fuel_type'] = 'Petrol'
        _vehicle['number_of_wheels'] = 2

        seating_capacity = parser_utils.try_parse_into_integer(_vehicle['seating_capacity'])
        _vehicle['seating_capacity'] = seating_capacity if seating_capacity else 2

        _vehicle['insurer'] = insurer
        _vehicle['vehicle_type'] = 'Twowheeler'

        del _vehicle['master']

        vehicle = motor_models.Vehicle(**_vehicle)
        vehicle_objects.append(vehicle)
        print 'Saving vehicle %s' % vehicle.model

    motor_models.Vehicle.objects.bulk_create(vehicle_objects)


def create_mappings():
    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)
    master_vehicles = parser_utils.read_file_to_return_vehicle_dictionary(MASTER_PATH, master_parameters)
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(INSURER_MASTER_PATH, parameters)

    ivehicles = motor_models.Vehicle.objects.filter(
        vehicle_type='Twowheeler',
        insurer__slug=INSURER_SLUG
    )

    vehicle_map = {}
    for iv in vehicles:
        _vehicle = ivehicles.filter(
            model_code=iv['model_code'],
            cc=parser_utils.try_parse_into_integer(iv['cc'])
        ).first()
        vehicle_map[iv['master']] = _vehicle

    mappings = []
    for mv in master_vehicles:
        _master = motor_models.VehicleMaster.objects.get(
            make__name=mv['make'],
            model__name=mv['model'],
            variant=mv['variant'],
            cc=mv['cc']
        )
        mapping = motor_models.VehicleMapping()
        mapping.master_vehicle = _master
        mapping.mapped_vehicle = vehicle_map.get(mv['insurer'], None)
        mapping.insurer = insurer

        if mapping.mapped_vehicle:
            print 'Mapping %s to %s' % (_master, mapping.mapped_vehicle)
            mappings.append(mapping)

    motor_models.VehicleMapping.objects.bulk_create(mappings)
