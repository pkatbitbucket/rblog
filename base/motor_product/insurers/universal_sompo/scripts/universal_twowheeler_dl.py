from motor_product import models as motor_models

INSURER_SLUG = 'universal-sompo'
DISCOUNTING_PATH = 'motor_product/insurers/universal_sompo/scripts/data/universal_twowheeler_'


def delete_data():
    motor_models.DiscountLoading.objects.filter(
        insurer__slug=INSURER_SLUG,
        vehicle_type='Twowheeler'
    ).delete()


def save_data(parameter):
    insurer = motor_models.Insurer.objects.get(slug=INSURER_SLUG)
    fd = open(DISCOUNTING_PATH + parameter + '.csv')

    dls = []
    for line in fd.readlines()[1:]:
        elements = line.split(',')

        make = elements[0]
        model = elements[1] if elements[1] not in ["", None] else None

        dl = motor_models.DiscountLoading()
        dl.make = make
        dl.model = model
        dl.insurer = insurer
        dl.lower_age_limit = 0
        dl.upper_age_limit = 5
        dl.vehicle_type = 'Twowheeler'
        dl.discount = int(elements[2])
        dls.append(dl)
        print dl.make, dl.model, dl.lower_age_limit, dl.upper_age_limit, dl.discount

        dl = motor_models.DiscountLoading()
        dl.make = make
        dl.model = model
        dl.insurer = insurer
        dl.lower_age_limit = 5
        dl.upper_age_limit = 10
        dl.vehicle_type = 'Twowheeler'
        dl.discount = int(elements[3])
        dls.append(dl)
        print dl.make, dl.model, dl.lower_age_limit, dl.upper_age_limit, dl.discount

    fd.close()

    if len(dls) > 0:
        motor_models.DiscountLoading.objects.bulk_create(dls)
