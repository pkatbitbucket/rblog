INSURER_VALIDATIONS = {
    'default': {
        'engine': {
            'minimum_length': 1
        },
        'chassis': {
            'minimum_length': 1
        }

    },

    'bharti-axa': {
        'engine': {
            'minimum_length': 5
        },
        'chassis': {
            'minimum_length': 5
        }

    },

    'hdfc-ergo': {
        'engine': {
            'minimum_length': 5
        },
        'chassis': {
            'minimum_length': 5
        }

    },

    'l-t': {
        'engine': {
            'minimum_length': 7
        },
        'chassis': {
            'minimum_length': 7
        }

    }
}


def _get_chassis_and_engine_lengths(insurer):
    return (
        INSURER_VALIDATIONS.get(insurer, INSURER_VALIDATIONS.get('default')).get('chassis').get('minimum_length'),
        INSURER_VALIDATIONS.get(insurer, INSURER_VALIDATIONS.get('default')).get('engine').get('minimum_length')
    )


def prefill_fastlane_vehicle_data(insurer, registration_detail_statistics, user_form_details):
    if not registration_detail_statistics.user_status == 'ACCEPTED':
        return user_form_details

    fastlane_vehicle_info = registration_detail_statistics.registration_detail.get_fastlane_vehicle_info()

    chassis_number, engine_number = fastlane_vehicle_info.get('chasi_no'), fastlane_vehicle_info.get('eng_no')
    chassis_length, engine_length = _get_chassis_and_engine_lengths(insurer)

    if chassis_number and engine_number and len(chassis_number) >= chassis_length and \
                    len(engine_number) >= engine_length:
        user_form_details['vehicle_chassis_no'] = chassis_number
        user_form_details['vehicle_engine_no'] = engine_number
        user_form_details['is_populated_from_fastlane'] = True
    return user_form_details
