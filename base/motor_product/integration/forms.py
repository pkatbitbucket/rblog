from django import forms
import datetime
from motor_product.models import Insurer, Transaction


INSURERS = [(i.id, str(i)) for i in Insurer.objects.filter(is_active_for_car=True)]

VEHICLE_TYPES = (
    (4, "Four Wheeler"),
    (2, "Two Wheeler")
)

INSURER_CHOICES = [(o.slug, str(o)) for o in Insurer.objects.all()]
INSURER_CHOICES.insert(0, ('all', 'All Coverfox data'))


class GetDateForm(forms.Form):
    start_date = forms.DateField(widget=forms.DateInput(attrs={'class': 'datepicker'}))
    end_date = forms.DateField(widget=forms.DateInput(attrs={'class': 'datepicker'}))
    vehicle_type = forms.ChoiceField(choices=VEHICLE_TYPES, required=True)
    no_of_records = forms.IntegerField(required=True)

    def clean_end_date(self):
        start_date = self.cleaned_data['start_date']
        end_date = self.cleaned_data['end_date']
        if end_date < start_date:
            raise forms.ValidationError("End date should greater than start date")
        if end_date > datetime.datetime.now().date():
            raise forms.ValidationError("end date should not greater than today.")
        return end_date


class FailureReasons(GetDateForm):
    insurer = forms.ChoiceField(choices=INSURER_CHOICES, required=True, label='Select Insurer')

    def __init__(self, *args, **kwargs):
        super(FailureReasons, self).__init__(*args, **kwargs)
        self.fields.pop('no_of_records')
        availble_options = self.fields['insurer']._get_choices()
        availble_options.pop(0)
        availble_options = [('all', 'All Insurers')] + availble_options
        self.fields['insurer']._set_choices(availble_options)


class TimeSlot(GetDateForm):

    def __init__(self, *args, **kwargs):
        super(TimeSlot, self).__init__(*args, **kwargs)
        self.fields.pop('no_of_records')


class DownloadDatabyID(forms.Form):
    trans_id = forms.CharField(required=True, label="Transaction ID")

    def clean_trans_id(self):
        trans_id = self.cleaned_data['trans_id']
        if not Transaction.objects.filter(transaction_id=trans_id):
            raise forms.ValidationError("Invalid transaction id")
        return trans_id


class VehilceMasterDataDownload(forms.Form):
    insurer = forms.ChoiceField(choices=INSURER_CHOICES, required=True)
    vehicle_type = forms.ChoiceField(choices=VEHICLE_TYPES, required=True)


class RTOMasterDataDownload(forms.Form):
    insurer = forms.ChoiceField(choices=INSURER_CHOICES, required=True)
