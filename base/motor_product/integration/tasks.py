import collections
import copy
import time
import datetime
from celery_app import app
from motor_product.prod_utils import VEHICLE_INTEGRATION_MAP
from motor_product.models import Insurer, VehicleMaster
from concurrent.futures import TimeoutError
from django.core.cache import cache
from motor_product.insurers import insurer_utils
from celery import group

PREMIUM_TIMEOUT = 28
PREMIUM_SOFT_TIMEOUT = 25

SIZE = 5

others_request_data = {u'addon_isDepreciationWaiver': u'0', u'isNCBCertificate': '0', u'extra_paPassenger': u'0',
                        u'cngKitValue': u'0', u'extra_user_dob': u'', u'isNewVehicle': u'0',
                        u'extra_isAntiTheftFitted': u'0', u'quoteId': u'5d5c0fd7-f553-4305-b010-5618ce3b4200',
                        u'idvNonElectrical': u'0', u'isClaimedLastYear': u'0',
                        u'registrationNumber[]': [u'MH', u'01', u'0', u'0'], u'newNCB': '0',
                        u'newPolicyStartDate': u'07-04-2016', u'manufacturingDate': u'01-03-2015',
                        u'fastlane_model_changed': u'false', u'formatted_reg_number': u'',
                        u'addon_isNcbProtection': u'0', u'registrationDate': u'01-03-2015', u'idvElectrical': u'0',
                        'third_party': None, u'expiry_error': u'false', u'addon_is247RoadsideAssistance': u'0',
                        u'voluntaryDeductible': u'0', u'fastlane_variant_changed': u'false', u'rds_id': u'false',
                        u'fastlane_fueltype_changed': u'false', u'addon_isInvoiceCover': u'0',
                        u'addon_isDriveThroughProtected': u'0', u'previous_policy_insurer_name': u'false',
                        'payment_mode': 'PAYMENT_GATEWAY', u'fastlane_success': u'false', u'isCNGFitted': u'0',
                        u'isUsedVehicle': '0', u'extra_isTPPDDiscount': u'0', u'vehicleId': u'1048',
                        u'expirySlot': u'7', u'idv': u'0', u'discountCode': u'', u'extra_isLegalLiability': u'0',
                        u'previousNCB': u'0', u'addon_isEngineProtector': u'0', u'ncb_unknown': u'true',
                        u'addon_isKeyReplacement': u'0', u'extra_isMemberOfAutoAssociation': u'0',
                        u'isExpired': u'false', u'pastPolicyExpiryDate': u'6-04-2016'}


def add_days(days):
    date_obj = datetime.datetime.today() + datetime.timedelta(days=days)
    return '%s-%s-%s' % (date_obj.strftime('%d'), date_obj.strftime('%m'), date_obj.strftime('%Y'))


others_request_data['newPolicyStartDate'] = add_days(7)
others_request_data['pastPolicyExpiryDate'] = add_days(6)

icici_request_data = copy.deepcopy(others_request_data)
icici_request_data['isUsedVehicle'] = 1


def appendfalse(cached_object, insurer):
    if cached_object:
        insurer_statuses = cached_object['status']
        last_success_full_request = cached_object['timestamp']
        insurer_statuses.append(False)
        cache.set('%s-insurer-status' % insurer, {'status': insurer_statuses, 'timestamp': last_success_full_request}, timeout=None)
    else:
        insurer_statuses = collections.deque(['Initial'] * SIZE, maxlen=SIZE)
        insurer_statuses.append(True)
        cache.set('%s-insurer-status' % insurer, {'status': insurer_statuses, 'timestamp': 'NA'}, timeout=None)


def appendtrue(cached_object, insurer):
    if cached_object:
        insurer_statuses = cached_object['status']
        insurer_statuses.append(True)
        cache.set('%s-insurer-status' % insurer, {'status': insurer_statuses, 'timestamp': time.strftime('%c')}, timeout=None)
    else:
        insurer_statuses = collections.deque(['Initial'] * SIZE, maxlen=SIZE)
        insurer_statuses.append(True)
        cache.set('%s-insurer-status' % insurer, {'status': insurer_statuses, 'timestamp': time.strftime('%c')}, timeout=None)


@app.task(time_limit=PREMIUM_TIMEOUT, soft_time_limit=PREMIUM_SOFT_TIMEOUT, expires=PREMIUM_TIMEOUT, queue='insurer_live_quote_status')
def check(vehicle_type, insurer, data_dictionary):
    cached_object = cache.get('%s-insurer-status' % insurer)
    m = VEHICLE_INTEGRATION_MAP[vehicle_type][insurer]
    try:
        result_data = m.PremiumCalculator.get_premium(data_dictionary)
    except TimeoutError:
        # Timeout error
        appendfalse(cached_object, insurer)
    except:
        # Unknown error
        appendfalse(cached_object, insurer)
    else:
        # Everything is ok
        if result_data['success']:
            # Quote response successful
            appendtrue(cached_object, insurer)
        else:
            appendfalse(cached_object, insurer)


@app.task(queue='insurer_live_quote_status')
def insurer_status():
    insurer_slugs = Insurer.objects.all().values_list('slug', flat=True)
    vehicle_type = 'fourwheeler'
    tasks = []
    for insurer in insurer_slugs:
        ins = Insurer.objects.get(slug=insurer)
        if not ins.is_active_for_car:
            insurer_statuses = collections.deque(['Disable'] * SIZE, maxlen=SIZE)
            cache.set('%s-insurer-status' % insurer, {'status': insurer_statuses, 'timestamp': 'NA'}, timeout=None)
            continue
        if insurer == 'icici-lombard':
            request_data = icici_request_data
        else:
            request_data = others_request_data

        master_vehicle_id = request_data['vehicleId']
        master_vehicle = VehicleMaster.objects.get(id=master_vehicle_id)
        vehicles = master_vehicle.sub_vehicles.filter(insurer__slug=insurer)
        if len(vehicles) > 0:
            vehicle_id = vehicles[0].id
        else:
            vehicle_id = master_vehicle_id
        processed_request_data = insurer_utils.add_all_addons(request_data)
        m = VEHICLE_INTEGRATION_MAP[vehicle_type][insurer]
        data_dictionary = m.convert_premium_request_data_to_required_dictionary(
            vehicle_id, processed_request_data)
        data_dictionary['isForListing'] = True
        data_dictionary['quoteId'] = request_data['quoteId']

        tasks.append(check.s(vehicle_type, insurer, data_dictionary))

    group(*tasks)()
