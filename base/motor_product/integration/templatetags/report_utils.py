from django import template
from motor_product.models import VehicleMaster, RTOMaster

register = template.Library()


@register.filter(name='return_success_count_for_ins')
def return_success_count_for_ins(slug, combined_data):
    if combined_data.get(slug):
        try:
            return "%s" % (combined_data[slug]['success_percentage'])
        except:
            return "%s" % (combined_data[slug])
    return 0


@register.filter(name='return_vehicle')
def return_vehicle(vehicle_id):
    obj = VehicleMaster.objects.get(id=vehicle_id)
    return "%s %s %s %s %scc" % (obj.make, obj.model, obj.variant, obj.fuel_type, obj.cc)


@register.filter(name='return_rto')
def return_rto(rto_id):
    obj = RTOMaster.objects.get(id=rto_id)
    return "%s %s %s" % (obj.rto_name, obj.rto_code, obj.rto_state)


@register.filter(name='return_time_slot')
def return_time_slot(d, name):
    return d[name]
