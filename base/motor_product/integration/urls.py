from django.conf.urls import url

from . import views

urlpatterns = [
        url(r'^$', views.index, name='motor_admin_home'),
        url(r'^summary-report/(?P<vehicle_type>[\w-]+)/$', views.summary_report, name='summary_report'),
        url(r'^summary-report/insurer/(?P<vehicle_type>[\w-]+)/(?P<insurer_slug>[\w-]+)/$', views.summary_report_insurer, name='summary_report_insurer'),
        url(r'^top-model-models/$', views.report_top_model, name='report_top_model'),
        url(r'^top-model-rto/$', views.report_top_rto, name='report_top_rto'),
        url(r'^failure-with-reasons/$', views.failure_with_reasons, name='failure_with_reasons'),
        url(r'^time-slot/$', views.time_slot, name='time_slot'),
        url(r'^top-model-models-avg/$', views.report_top_model_avg, name='report_top_model_avg'),
        url(r'^download-data/$', views.download_data, name='download_data'),
        url(r'^download-data/download-payment-request/$', views.download_payment_request, name='download_payment_request'),
        url(r'^download-data/download-payment-response/$', views.download_payment_response, name='download_payment_response'),
        url(r'^download-data/download-proposal-request/$', views.download_proposal_request, name='download_proposal_request'),
        url(r'^download-data/download-proposal-response/$', views.download_proposal_response, name='download_proposal_response'),
        url(r'^download-data/download-quote-request/$', views.download_quote_request, name='download_quote_request'),
        url(r'^download-data/download-quote-response/$', views.download_quote_response, name='download_quote_response'),
        url(r'^create-transaction-receipt/$', views.create_transaction_receipt, name="create_transaction_receipt"),
        url(r'^insurer-live-status/$', views.insurer_live_status, name="insurer_live_status"),
        ]
