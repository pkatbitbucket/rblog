import json
import utils
import datetime
import putils
from collections import OrderedDict
import cStringIO as StringIO

from django.shortcuts import render
from django.db.models import Count, Avg
from django.http import HttpResponse
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.template import Context, loader
from django.shortcuts import render_to_response
from django.core.cache import cache
from django.contrib.auth.decorators import permission_required

from xhtml2pdf import pisa

from motor_product.models import IntegrationStatistics, Insurer, Transaction, VehicleMaster, RTOMaster, Vehicle, RTOInsurer
from .forms import GetDateForm, DownloadDatabyID, VehilceMasterDataDownload, RTOMasterDataDownload, FailureReasons, TimeSlot
# Create your views here.


ALL_ERRORS = ['COVERFOX_TP_RESTRICTED', 'COVERFOX_MAKE_RESTRICTED', 'COVERFOX_DISCOUNT_LIMIT_EXCEEDED', 'COVERFOX_DECLINED_MODEL',
            'INSURER_USED_VEHICLE_RESTRICTED', 'INSURER_AGE_RESTRICTED', 'INSURER_EXPIRED_POLICY_RESTRICTED', 'INSURER_ZERO_PREMIUM',
            'INSURER_MAKE_RESTRICTED', 'INSURER_NO_IDV', 'INSURER_NEW_VEHICLE_RESTRICTED', 'INSURER_ADD_ONS_RESTRICTED',
            'VEHICLE_MAPPING_NOT_FOUND', 'RTO_MAPPING_NOT_FOUND', 'IDV_LIMIT_EXCEEDED', 'PREMIUM_TIMEOUT', 'UNKNOWN_ERROR']

ACTUAL_ERRORS = ['VEHICLE_MAPPING_NOT_FOUND', 'PREMIUM_TIMEOUT', 'UNKNOWN_ERROR', 'RTO_MAPPING_NOT_FOUND', 'IDV_LIMIT_EXCEEDED']

RESTRICTED_ERRORS = ['INSURER_EXPIRED_POLICY_RESTRICTED',
 'INSURER_NO_IDV',
 'COVERFOX_MAKE_RESTRICTED',
 'INSURER_AGE_RESTRICTED',
 'COVERFOX_DISCOUNT_LIMIT_EXCEEDED',
 'INSURER_ZERO_PREMIUM',
 'INSURER_USED_VEHICLE_RESTRICTED',
 'COVERFOX_DECLINED_MODEL',
 'COVERFOX_TP_RESTRICTED',
 'INSURER_NEW_VEHICLE_RESTRICTED',
 'INSURER_MAKE_RESTRICTED',
 'INSURER_ADD_ONS_RESTRICTED']


ALL_FLAGS = ['COVERFOX_TP_RESTRICTED', 'COVERFOX_MAKE_RESTRICTED', 'COVERFOX_DISCOUNT_LIMIT_EXCEEDED', 'COVERFOX_DECLINED_MODEL',
    'INSURER_USED_VEHICLE_RESTRICTED', 'INSURER_AGE_RESTRICTED', 'INSURER_EXPIRED_POLICY_RESTRICTED', 'INSURER_ZERO_PREMIUM',
    'INSURER_MAKE_RESTRICTED', 'INSURER_NO_IDV', 'INSURER_NEW_VEHICLE_RESTRICTED', 'INSURER_ADD_ONS_RESTRICTED',
    'VEHICLE_MAPPING_NOT_FOUND', 'RTO_MAPPING_NOT_FOUND', 'IDV_LIMIT_EXCEEDED', 'PREMIUM_TIMEOUT', 'UNKNOWN_ERROR', 'SUCCESS'
]


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def index(request):
    return render(request, 'motor_product/admin/index.html', {'top_model_form': '', 'DownloadDatabyID': DownloadDatabyID})


def detailed_data_integration_statistics(vehicle_type, today=False):
    if today:
        insurers_data = IntegrationStatistics.objects.filter(vehicle_type=vehicle_type, created_on__gte=datetime.datetime.now()-datetime.timedelta(days=1)).values('insurer__title', 'status', 'required_time', 'error_code', 'insurer__slug')
    else:
        insurers_data = IntegrationStatistics.objects.filter(vehicle_type=vehicle_type).values('insurer__title', 'status', 'required_time', 'error_code', 'insurer__slug')

    detailed_data = {}
    for i in insurers_data:
        if detailed_data.get(i['insurer__title']):
            detailed_data[i['insurer__title']]['required_time'] = detailed_data[i['insurer__title']]['required_time'] + i['required_time']
            detailed_data[i['insurer__title']]['count'] = detailed_data[i['insurer__title']]['count'] + 1
            if i['status']:
                detailed_data[i['insurer__title']]['success_count'] = detailed_data[i['insurer__title']]['success_count'] + 1
            if i['error_code'] == 'UNKNOWN_ERROR':
                detailed_data[i['insurer__title']]['unknown_errors'] = detailed_data[i['insurer__title']]['unknown_errors'] + 1
            if i['error_code'] == 'PREMIUM_TIMEOUT':
                detailed_data[i['insurer__title']]['timeout_count'] = detailed_data[i['insurer__title']]['timeout_count'] + 1
        else:
            success_count = 0
            unknown_errors = 0
            timeout_count = 0
            if i['status']:
                success_count = 1
            if i['error_code'] == 'UNKNOWN_ERROR':
                unknown_errors = 1
            if i['error_code'] == 'PREMIUM_TIMEOUT':
                timeout_count = 1
            detailed_data[i['insurer__title']] = {
                                        'success_count': success_count,
                                        'required_time': i['required_time'],
                                        'count': 1, 'unknown_errors': unknown_errors,
                                        'insurer': i['insurer__title'],
                                        'timeout_count': timeout_count,
                                        'insurer_slug': i['insurer__slug']}
    return detailed_data


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def summary_report(request, vehicle_type):
    details = detailed_data_integration_statistics(vehicle_type, today=False)
    summary = []
    for key, value in details.iteritems():
        total_valid_count = value['success_count'] + value['unknown_errors'] + value['timeout_count']
        try:
            failure_rate = 1 - (value['success_count'] / (float(total_valid_count)))
            failure_rate = round(failure_rate, 2) * 100
        except ZeroDivisionError:
            failure_rate = 'NA'

        try:
            avg = round(value['required_time']/float(value['success_count']), 2)
        except ZeroDivisionError:
            avg = 'NA'

        try:
            timeout_per = round(value['timeout_count'] / float(total_valid_count) * 100, 2)
        except ZeroDivisionError:
            timeout_per = 'NA'

        quote_percent = round((value['success_count']/float(value['count']))*100, 2)
        details[key].update({
                                'avg': avg,
                                'failure_rate': failure_rate,
                                'quote_percent': quote_percent,
                                'timeout_per': timeout_per,
                                'total_count': value['count'] - value['unknown_errors'],
                                'insurer_slug': value['insurer_slug']})
        summary.append(details[key])
    return render(request, 'motor_product/admin/summary_report.html', {'avg_statistics': summary,
                                                                         'insurers': Insurer.objects.all(),
                                                                         'vehicle_type': vehicle_type})


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def summary_report_insurer(request, vehicle_type, insurer_slug):
    all_errors_dict = {}
    actual_errors_dict = {}

    for err in ALL_FLAGS:
        all_errors_dict[err] = IntegrationStatistics.objects.filter(insurer__slug=insurer_slug, error_code=err, vehicle_type=vehicle_type).count()

        if err in ACTUAL_ERRORS:
            actual_errors_dict[err] = IntegrationStatistics.objects.filter(insurer__slug=insurer_slug, error_code=err, vehicle_type=vehicle_type).count()

    return render(request, 'motor_product/admin/summary_individual.html',
            {'all_errors_dict': all_errors_dict,
            'actual_errors_dict': actual_errors_dict,
            'insurer': Insurer.objects.get(slug=insurer_slug)})


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def report_top_model(request):
    final_dict = {}
    all_insurers = []
    odered_list_of_vehicles = OrderedDict()
    if request.POST:
        form = GetDateForm(request.POST)
        if form.is_valid():
            start_date = datetime.datetime.strptime(request.POST['start_date'], '%m/%d/%Y')
            end_date = datetime.datetime.strptime(request.POST['end_date'] + " 23:59", '%m/%d/%Y %H:%M')
            vehicle_type = int(form.cleaned_data['vehicle_type'])
            no_of_records = int(form.cleaned_data['no_of_records'])

            vehicle_type = "fourwheeler" if vehicle_type == 4 else "twowheeler"
            params = {
                'vehicle_type': vehicle_type,
                'insurer__is_active_for_car': True,
            }

            params1 = {
                'vehicle_type': vehicle_type,
                'insurer__is_active_for_twowheeler': True,
            }

            filtering_params = params if vehicle_type == "fourwheeler" else params1

            top_vehicles = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date), status=True) \
                .filter(**filtering_params) \
                .values('master_vehicle') \
                .annotate(total_count=Count('id')).order_by('-total_count')

            top_vehicles = [i['master_vehicle'] for i in top_vehicles][:no_of_records]

            statistics = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date),
                                                              master_vehicle__in=top_vehicles, **filtering_params)
            insurer_slugs = statistics.distinct().values_list('insurer__slug', flat=True)
            top_model_wise_data = []
            for insurer in insurer_slugs:
                for vm in top_vehicles:
                    d = {}
                    false_count = statistics.filter(status=False, insurer__slug=insurer, master_vehicle=vm).count()
                    true_count = statistics.filter(status=True, insurer__slug=insurer, master_vehicle=vm).count()
                    total_count = true_count + false_count
                    d['insurer__slug'] = insurer
                    d['true_count'] = true_count
                    d['false_count'] = false_count
                    d['master_vehicle'] = vm
                    d['total_count'] = total_count
                    try:
                        success_percentage = round(true_count / float((total_count)), 3) * 100
                    except:
                        success_percentage = 0
                    if success_percentage > 0:
                        d['success_percentage'] = '%s %s' % (success_percentage, '%')
                    else:
                        d['success_percentage'] = 0
                    top_model_wise_data.append(d)

            # ***********************

            if vehicle_type == "fourwheeler":
                all_insurers = Insurer.objects.filter(is_active_for_car=True).order_by('id').values_list('slug', flat=True)
            else:
                all_insurers = Insurer.objects.filter(is_active_for_twowheeler=True).order_by('id').values_list('slug', flat=True)

            data_by_vehicle_dict = {}
            for v in top_vehicles:
                data_by_vehicle_dict[v] = filter(lambda x: x['master_vehicle'] == v, top_model_wise_data)

            final_dict = {}
            for key, value in data_by_vehicle_dict.items():
                ins_dict = {i: 0 for i in all_insurers}
                for v in value:
                    ins_dict[v['insurer__slug']] = v
                final_dict[v['master_vehicle']] = ins_dict

            for tv in top_vehicles:
                total_quotes = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date), status=True)\
                        .filter(quote__vehicle__id=tv).count()
                distinct_quotes = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date), status=True)\
                        .filter(quote__vehicle__id=tv).distinct('quote_id').count()
                avg = round(total_quotes / float(distinct_quotes), 2)
                final_dict[tv]['no_of_search'] = distinct_quotes
                final_dict[tv]['no_of_results'] = total_quotes
                final_dict[tv]['avg'] = avg

            odered_list_of_vehicles = OrderedDict(final_dict)
            odered_list_of_vehicles = OrderedDict((key, odered_list_of_vehicles[key]) for key in top_vehicles)

    else:
        form = GetDateForm()
    return render(request, 'motor_product/admin/report_top_model.html', {
        'data': odered_list_of_vehicles,
        'all_insurers': all_insurers,
        'form': form})


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def report_top_rto(request):
    final_dict = {}
    all_insurers = []
    odered_list_of_rtos = OrderedDict()
    if request.POST:
        form = GetDateForm(request.POST)
        if form.is_valid():
            start_date = datetime.datetime.strptime(request.POST['start_date'], '%m/%d/%Y')
            end_date = datetime.datetime.strptime(request.POST['end_date'] + " 23:59", '%m/%d/%Y %H:%M')
            vehicle_type = int(form.cleaned_data['vehicle_type'])
            no_of_records = int(form.cleaned_data['no_of_records'])

            vehicle_type = "fourwheeler" if vehicle_type == 4 else "twowheeler"
            params = {
                'vehicle_type': vehicle_type,
                'insurer__is_active_for_car': True,
            }

            params1 = {
                'vehicle_type': vehicle_type,
                'insurer__is_active_for_twowheeler': True,
            }

            filtering_params = params if vehicle_type == "fourwheeler" else params1

            top_rtos = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date,), status=True) \
                .filter(**filtering_params) \
                .values('rto_master') \
                .annotate(total_count=Count('id')) \
                .order_by('-total_count')

            top_rtos = [i['rto_master'] for i in top_rtos][:no_of_records]

            statistics = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date),
                                                              rto_master__in=top_rtos, **filtering_params)
            insurer_slugs = statistics.distinct().values_list('insurer__slug', flat=True)
            top_rto_wise_data = []
            for insurer in insurer_slugs:
                for rto in top_rtos:
                    d = {}
                    false_count = statistics.filter(status=False, insurer__slug=insurer, rto_master=rto).count()
                    true_count = statistics.filter(status=True, insurer__slug=insurer, rto_master=rto).count()
                    total_count = true_count + false_count
                    d['insurer__slug'] = insurer
                    d['true_count'] = true_count
                    d['false_count'] = false_count
                    d['rto_master'] = rto
                    d['total_count'] = total_count
                    try:
                        success_percentage = round(true_count / float((total_count)), 3) * 100
                    except ZeroDivisionError:
                        success_percentage = 0
                    if success_percentage > 0:
                        d['success_percentage'] = '%s %s' % (success_percentage, '%')
                    else:
                        d['success_percentage'] = 0

                    top_rto_wise_data.append(d)

            if vehicle_type == "fourwheeler":
                all_insurers = Insurer.objects.filter(is_active_for_car=True).order_by('id').values_list('slug', flat=True)
            else:
                all_insurers = Insurer.objects.filter(is_active_for_twowheeler=True).order_by('id').values_list('slug', flat=True)

            data_by_rto_dict = {}

            for v in top_rtos:
                data_by_rto_dict[v] = filter(lambda x: x['rto_master'] == v, top_rto_wise_data)

            final_dict = {}
            for key, value in data_by_rto_dict.items():
                ins_dict = {i: 0 for i in all_insurers}
                for v in value:
                    ins_dict[v['insurer__slug']] = v['success_percentage']
                final_dict[v['rto_master']] = ins_dict

            odered_list_of_rtos = OrderedDict(final_dict)
            odered_list_of_rtos = OrderedDict((key, odered_list_of_rtos[key]) for key in top_rtos)

    else:
        form = GetDateForm()
    return render(request, 'motor_product/admin/report_top_rto.html', {
        'data': odered_list_of_rtos,
        'all_insurers': all_insurers,
        'form': form})

@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def failure_with_reasons(request):
    form = FailureReasons()
    all_insurers = [i.slug for i in Insurer.objects.all()]
    if request.method == 'POST':
        form = FailureReasons(request.POST)
        actual_errors_dict = {}
        if form.is_valid():
            start_date = datetime.datetime.strptime(request.POST['start_date'], '%m/%d/%Y')
            end_date = datetime.datetime.strptime(request.POST['end_date'] + " 23:59", '%m/%d/%Y %H:%M')
            insurer_slug = form.cleaned_data['insurer']
            vehicle_type = int(form.cleaned_data['vehicle_type'])
            if vehicle_type == 4:
                vehicle_type = "fourwheeler"
            else:
                vehicle_type = "twowheeler"

            if insurer_slug == "all":
                total_quotes_sent = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date), vehicle_type=vehicle_type).count()
                success_quotes = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date), status=True, vehicle_type=vehicle_type).count()
                failed_quotes = total_quotes_sent - success_quotes

                for err in ACTUAL_ERRORS:
                    actual_errors_dict[err] = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date), status=False, error_code=err, vehicle_type=vehicle_type).count()

            else:
                total_quotes_sent = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date), insurer__slug=insurer_slug, vehicle_type=vehicle_type).count()
                success_quotes = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date), insurer__slug=insurer_slug, status=True, vehicle_type=vehicle_type).count()
                failed_quotes = total_quotes_sent - success_quotes

                for err in ACTUAL_ERRORS:
                    actual_errors_dict[err] = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date), insurer__slug=insurer_slug, status=False, error_code=err, vehicle_type=vehicle_type).count()

            return render(request, 'motor_product/admin/failure_with_reasons.html',
                    {'all_insurers': all_insurers, 'output': True, 'total_quotes_sent': total_quotes_sent,
                        'success_quotes': success_quotes, 'failed_quotes': failed_quotes, 'actual_errors_dict': actual_errors_dict, 'form': form})

    return render(request, 'motor_product/admin/failure_with_reasons.html', {'all_insurers': all_insurers, 'form': form})


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def time_slot(request):
    l = []
    if request.POST:
        form = TimeSlot(request.POST)
        if form.is_valid():
            start_date = datetime.datetime.strptime(request.POST['start_date'], '%m/%d/%Y')
            end_date = datetime.datetime.strptime(request.POST['end_date'] + " 23:59", '%m/%d/%Y %H:%M')
            vehicle_type = int(form.cleaned_data['vehicle_type'])
            if vehicle_type  == 4:
                vehicle_type = "fourwheeler"
                data = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date), insurer__is_active_for_car=True, vehicle_type=vehicle_type).distinct('insurer__slug').values('insurer__slug')
                slugs = [i['insurer__slug'] for i in data]
                insurer_data = {}
                for s in slugs:
                    ins_data = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date), insurer__is_active_for_car=True, insurer__slug=s, vehicle_type=vehicle_type)
                    d = {"0-2": 0, "2-5": 0, "5-7": 0, "7-10": 0, "10-N": 0, "avg": 0, "errors": 0, "total_count": 0}
                    count = 0
                    actual_errors = 0
                    restricted_errors = 0
                    for ins in ins_data:
                        if ins.status:
                            if ins.required_time < 2:
                                d["0-2"] = d["0-2"] + 1
                            elif ins.required_time < 5:
                                d["2-5"] = d["2-5"] + 1
                            elif ins.required_time < 7:
                                d["5-7"] = d["5-7"] + 1
                            elif ins.required_time < 10:
                                d["7-10"] = d["7-10"] + 1
                            else:
                                d["10-N"] = d["10-N"] + 1
                        count += 1

                        if ins.error_code in ACTUAL_ERRORS:
                            actual_errors += 1
                        if ins.error_code in RESTRICTED_ERRORS:
                            restricted_errors = +1

                    d['failure_rate'] = round(actual_errors / float((count-restricted_errors)), 3) * 100
                    d["total_count"] = count
                    d['errors'] = actual_errors
                    insurer_data[s] = d
                l.append(insurer_data)

            else:
                vehicle_type = "twowheeler"
                data = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date), insurer__is_active_for_twowheeler=True, vehicle_type=vehicle_type).distinct('insurer__slug').values('insurer__slug')
                slugs = [i['insurer__slug'] for i in data]
                insurer_data = {}
                for s in slugs:
                    ins_data = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date), insurer__is_active_for_twowheeler=True, insurer__slug=s, vehicle_type=vehicle_type)
                    d = {"0-2": 0, "2-5": 0, "5-7": 0, "7-10": 0, "10-N": 0, "avg": 0, "errors": 0, "total_count": 0}
                    count = 0
                    actual_errors = 0
                    restricted_errors = 0
                    for ins in ins_data:
                        if ins.status:
                            if ins.required_time < 2:
                                d["0-2"] = d["0-2"] + 1
                            elif ins.required_time < 5:
                                d["2-5"] = d["2-5"] + 1
                            elif ins.required_time < 7:
                                d["5-7"] = d["5-7"] + 1
                            elif ins.required_time < 10:
                                d["7-10"] = d["7-10"] + 1
                            else:
                                d["10-N"] = d["10-N"] + 1
                        count += 1

                        if ins.error_code in ACTUAL_ERRORS:
                            actual_errors += 1
                        if ins.error_code in RESTRICTED_ERRORS:
                            restricted_errors = +1

                    d['failure_rate'] = round(actual_errors / float((count-restricted_errors)), 3) * 100
                    d["total_count"] = count
                    d['errors'] = actual_errors
                    insurer_data[s] = d
                l.append(insurer_data)

    else:
        form = TimeSlot()
    return render(request, 'motor_product/admin/time_slot.html', {"data": l, 'form': form})


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def report_top_model_avg(request):
    final_dict = {}
    all_insurers = []
    odered_list_of_vehicles = OrderedDict()
    if request.POST:
        form = GetDateForm(request.POST)
        if form.is_valid():
            start_date = datetime.datetime.strptime(request.POST['start_date'], '%m/%d/%Y')
            end_date = datetime.datetime.strptime(request.POST['end_date'] + " 23:59", '%m/%d/%Y %H:%M')
            vehicle_type = int(form.cleaned_data['vehicle_type'])
            no_of_records = int(form.cleaned_data['no_of_records'])
            odered_list_of_vehicles = OrderedDict()
            if vehicle_type == 4:
                vehicle_type = "fourwheeler"
                data = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date), status=True, insurer__is_active_for_car=True, vehicle_type=vehicle_type).values('master_vehicle', 'insurer__slug').\
                annotate(total_count=Count('id'), avg_time=Avg('required_time')).order_by('-total_count')

                top_vehicles = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date), status=True, insurer__is_active_for_car=True, vehicle_type=vehicle_type).values('master_vehicle')\
                .annotate(total_count=Count('id')).order_by('-total_count')

                top_vehicles = [i['master_vehicle']for i in top_vehicles][:no_of_records]

                all_insurers = [i.slug for i in Insurer.objects.filter(is_active_for_car=True)]

                data_by_vehicle_dict = {}
                for v in top_vehicles:
                    data_by_vehicle_dict[v] = filter(lambda x: x['master_vehicle'] == v, data)

                final_dict = {}

                for key, value in data_by_vehicle_dict.items():
                    ins_dict = {i: 0 for i in all_insurers}
                    for v in value:
                        ins_dict[v['insurer__slug']] = round(v['avg_time'] + ins_dict[v['insurer__slug']], 3)
                    final_dict[v['master_vehicle']] = ins_dict

                odered_list_of_vehicles = OrderedDict(final_dict)
                odered_list_of_vehicles = OrderedDict((key, odered_list_of_vehicles[key]) for key in top_vehicles)

                all_insurers = [i for i in Insurer.objects.filter(is_active_for_car=True).order_by('id')]

            else:
                vehicle_type = "twowheeler"
                data = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date), status=True, insurer__is_active_for_twowheeler=True, vehicle_type=vehicle_type).values('master_vehicle', 'insurer__slug').\
                annotate(total_count=Count('id'), avg_time=Avg('required_time')).order_by('-total_count')

                top_vehicles = IntegrationStatistics.objects.filter(created_on__range=(start_date, end_date), status=True, insurer__is_active_for_twowheeler=True, vehicle_type=vehicle_type).values('master_vehicle')\
                .annotate(total_count=Count('id')).order_by('-total_count')

                top_vehicles = [i['master_vehicle']for i in top_vehicles][:no_of_records]

                all_insurers = [i.slug for i in Insurer.objects.filter(is_active_for_twowheeler=True)]

                data_by_vehicle_dict = {}
                for v in top_vehicles:
                    data_by_vehicle_dict[v] = filter(lambda x: x['master_vehicle'] == v, data)

                final_dict = {}

                for key, value in data_by_vehicle_dict.items():
                    ins_dict = {i: 0 for i in all_insurers}
                    for v in value:
                        ins_dict[v['insurer__slug']] = round(v['avg_time'] + ins_dict[v['insurer__slug']], 3)
                    final_dict[v['master_vehicle']] = ins_dict

                odered_list_of_vehicles = OrderedDict(final_dict)
                odered_list_of_vehicles = OrderedDict((key, odered_list_of_vehicles[key]) for key in top_vehicles)

                all_insurers = [i for i in Insurer.objects.filter(is_active_for_twowheeler=True).order_by('id')]
    else:
        form = GetDateForm()

    return render(request, 'motor_product/admin/report_top_model_avg.html', {'data': odered_list_of_vehicles, 'all_insurers': all_insurers, 'form': form})


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def download_payment_request(request):
    try:
        trans_id = request.GET['trans_id']
        data = Transaction.objects.get(transaction_id=trans_id)
        response = HttpResponse(json.dumps(data.payment_request), content_type="application/force-download")
        response["Content-Disposition"] = "attachment; filename=%s_payment_request.txt" % trans_id
        return response
    except:
        return HttpResponse("Invalid ID or ID not provided")


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def download_payment_response(request):
    try:
        trans_id = request.GET['trans_id']
        data = Transaction.objects.get(transaction_id=trans_id)
        response = HttpResponse(json.dumps(data.payment_response), content_type="application/force-download")
        response["Content-Disposition"] = "attachment; filename=%s_payment_response.txt" % trans_id
        return response
    except:
        return HttpResponse("Invalid ID or ID not provided")


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def download_proposal_request(request):
    try:
        trans_id = request.GET['trans_id']
        data = Transaction.objects.get(transaction_id=trans_id)
        response = HttpResponse(json.dumps(data.proposal_request), content_type="application/force-download")
        response["Content-Disposition"] = "attachment; filename=%s_proposal_request.txt" % trans_id
        return response
    except:
        return HttpResponse("Invalid ID or ID not provided")


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def download_proposal_response(request):
    try:
        trans_id = request.GET['trans_id']
        data = Transaction.objects.get(transaction_id=trans_id)
        response = HttpResponse(json.dumps(data.proposal_response), content_type="application/force-download")
        response["Content-Disposition"] = "attachment; filename=%s_proposal_response.txt" % trans_id
        return response
    except:
        return HttpResponse("Invalid ID or ID not provided")


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def download_quote_response(request):
    try:
        trans_id = request.GET['trans_id']
        data = Transaction.objects.get(transaction_id=trans_id)
        response = HttpResponse(json.dumps(data.raw.get('quote_response', None)), content_type="text/plain")
        response["Content-Disposition"] = "attachment; filename=%s_quote_response.txt" % trans_id
        return response
    except:
        return HttpResponse("Invalid ID or ID not provided")


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def download_quote_request(request):
    try:
        trans_id = request.GET['trans_id']
        data = Transaction.objects.get(transaction_id=trans_id)
        response = HttpResponse(json.dumps(data.quote.raw_data), content_type="text/plain")
        response["Content-Disposition"] = "attachment; filename=%s_quote_request.txt" % trans_id
        return response
    except:
        return HttpResponse("Invalid ID or ID not provided")


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def download_data(request):
    form_policy_data = DownloadDatabyID()
    form_vehilcemaster_data_dwnld = VehilceMasterDataDownload()
    form_rtomaster_data_dwnld = RTOMasterDataDownload()
    if request.POST:
        if 'policy_data_download' in request.POST:
            form_policy_data = DownloadDatabyID(request.POST)
            if form_policy_data.is_valid():
                trans_id = request.POST['trans_id']
                trans_obj = Transaction.objects.get(transaction_id=trans_id)
                return render(request, "motor_product/admin/download_data.html", {
                        'DownloadDatabyID': form_policy_data,
                        "trans_obj": trans_obj,
                        "customer_name": "%s %s" % (trans_obj.raw['user_form_details'].get('cust_first_name', "NA"), trans_obj.raw['user_form_details'].get('cust_last_name', "NA")),
                        "customer_email": trans_obj.raw['user_form_details'].get('cust_email', "NA"),
                        "status": trans_obj.status,
                        "contact": trans_obj.raw['user_form_details'].get('cust_phone', "NA"),
                        "vehicle_no": trans_obj.raw['user_form_details'].get('vehicle_reg_no', "NA"),
                        "insurer": trans_obj.insurer,
                        'form_vehilcemaster_data_dwnld': form_vehilcemaster_data_dwnld,
                        'form_rtomaster_data_dwnld': form_rtomaster_data_dwnld,

                    }
                )
        if 'vehiclemasterdatadownload' in request.POST:
            form_vehilcemaster_data_dwnld = VehilceMasterDataDownload(request.POST)
            if form_vehilcemaster_data_dwnld.is_valid():
                insurer_slug = form_vehilcemaster_data_dwnld.cleaned_data['insurer']
                vehicle_type = form_vehilcemaster_data_dwnld.cleaned_data['vehicle_type']
                data = None
                if vehicle_type == '2':
                    if insurer_slug == 'all':
                        data = vehicle_data_in_xls(vehicle_type="twowheeler", insurer_slug=None)
                    else:
                        data = vehicle_data_in_xls(vehicle_type="twowheeler", insurer_slug=insurer_slug)
                    pass
                else:
                    if insurer_slug == 'all':
                        data = vehicle_data_in_xls(vehicle_type="fourwheeler", insurer_slug=None)
                    else:
                        data = vehicle_data_in_xls(vehicle_type="fourwheeler", insurer_slug=insurer_slug)
                if data is not None:
                    return putils.render_excel(data[0], data[1], data[2])
                else:
                    return HttpResponse("something went wrong")

        if 'rtomasterdatadownload' in request.POST:
            form_vehilcemaster_data_dwnld = RTOMasterDataDownload(request.POST)
            if form_vehilcemaster_data_dwnld.is_valid():
                insurer_slug = form_vehilcemaster_data_dwnld.cleaned_data['insurer']
                data = None
                if insurer_slug == 'all':
                    data = rto_data_in_xls(insurer_slug=None)
                else:
                    data = rto_data_in_xls(insurer_slug=insurer_slug)

                if data is not None:
                    return putils.render_excel(data[0], data[1], data[2])
                else:
                    return HttpResponse("something went wrong")

    return render(request, "motor_product/admin/download_data.html",
            {'DownloadDatabyID': form_policy_data,
            'form_vehilcemaster_data_dwnld': form_vehilcemaster_data_dwnld,
            'form_rtomaster_data_dwnld': form_rtomaster_data_dwnld})


def vehicle_data_in_xls(vehicle_type, insurer_slug=None):
    if vehicle_type == "twowheeler":
        vehicle_type = "Twowheeler"
    else:
        vehicle_type = "Private Car"

    order = ('Id', 'Make', 'Model', 'Variant', 'CC', 'Seating Capacity', 'Fuel Type')
    rows = []

    if insurer_slug is None:
        vehicles = VehicleMaster.objects.filter(vehicle_type=vehicle_type)

        for vehicle in vehicles:
            row = [
                vehicle.id,
                vehicle.make.name,
                vehicle.model.name,
                vehicle.variant,
                vehicle.cc,
                vehicle.seating_capacity,
                vehicle.fuel_type,
            ]
            rows.append(row)
    else:
        order = ('Id', 'Make', 'Model', 'Variant', 'CC', 'Insurer Slug', 'Fuel Type')
        vehicles = Vehicle.objects.filter(vehicle_type=vehicle_type, insurer__slug=insurer_slug)

        for vehicle in vehicles:
            row = [
                vehicle.id,
                vehicle.make,
                vehicle.model,
                vehicle.variant,
                vehicle.cc,
                vehicle.insurer.slug,
                vehicle.fuel_type,
            ]
            rows.append(row)

    file_name = 'Vehicle_Data_%s_%s.xls' % (
        insurer_slug.replace('-', '_') if insurer_slug else 'master', datetime.datetime.now().strftime("%d-%b_%H%M")
    )

    return file_name, order, rows


def rto_data_in_xls(insurer_slug=None):
    order = ('Id', 'rto_code', 'rto_name', 'rto_state')
    rows = []

    if insurer_slug is None:
        rtos = RTOMaster.objects.all()

        for rto in rtos:
            row = [
                rto.id,
                rto.rto_code,
                rto.rto_name,
                rto.rto_state,
            ]
            rows.append(row)
    else:
        rtos = RTOInsurer.objects.filter(insurer__slug=insurer_slug)
        order = ('Id', 'rto_code', 'rto_name', 'rto_state', 'rto_zone')
        for rto in rtos:
            row = [
                rto.id,
                rto.rto_code,
                rto.rto_name,
                rto.rto_state,
                rto.rto_zone,
            ]
            rows.append(row)

    file_name = 'RTO_Data_%s_%s.xls' % (
        insurer_slug.replace('-', '_') if insurer_slug else 'master', datetime.datetime.now().strftime("%d-%b_%H%M")
    )

    return file_name, order, rows


@never_cache
@permission_required('motor_product.can_view_ops_sales')
def create_transaction_receipt(request):

    _insurer_dict = {
        'maruti': 'Maruti',
        'tata-aig': 'Tata AIG',
        'l-t': 'L&T General',
        'bharti-axa': 'Bharati AXA',
        'bajaj-allianz': 'Bajaj Allianz',
        'universal-sompo': 'Universal Sompo',
        'oriental': 'Oriental Insurance',
        'iffco-tokio': 'Iffco Tokio',
        'future-generali': 'Future Generali',
        'liberty-videocon': 'LibertyViveocon',
        'reliance': 'Reliance General'
    }

    if request.method == 'POST':
        post_data = request.POST
        context_dict = {}

        context_dict['insurer'] = post_data.get('insurer')
        context_dict['insurer_name'] = _insurer_dict[post_data.get('insurer')]
        context_dict['issuedate'] = post_data.get('issuedate')
        context_dict['insuredname'] = post_data.get('insuredname')
        context_dict['mobile'] = post_data.get('mobile')
        context_dict['variant'] = post_data.get('variant')
        context_dict['registration'] = post_data.get('registration')
        context_dict['make'] = post_data.get('make')
        context_dict['model'] = post_data.get('model')
        context_dict['amount'] = post_data.get('amount')
        context_dict['fromdate'] = post_data.get('fromdate')
        context_dict['todate'] = post_data.get('todate')
        context_dict['emailid'] = post_data.get('emailid')
        context_dict['site_url'] = settings.SITE_URL.strip("/")

        template = loader.get_template("motor_product/covernote_pdf.html")
        context = Context(context_dict)
        html = template.render(context)
        result = StringIO.StringIO()
        pdfdata = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result, encoding='UTF-8')
        filename = '%s_%s.pdf' % (
            '_'.join(post_data.get('insuredname').split()), datetime.datetime.now().strftime('%d%b%Y_%H%M%S')
        )

        if not pdfdata.err:
            response = HttpResponse(result.getvalue(), content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename=' + filename
            return response
        return HttpResponse("Some error occurred.")

    return render_to_response("motor_product/covernote-formfields.html", {})


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def insurer_live_status(request):
    insurers = Insurer.objects.all()
    data = []
    for i in insurers:
        ins_dict = {}
        if cache.get('%s-insurer-status' % i.slug):
            ins_dict['status'] = list(cache.get('%s-insurer-status' % i.slug)['status'])[::-1]
            ins_dict['timestamp'] = cache.get('%s-insurer-status' % i.slug)['timestamp']
            ins_dict['insurer'] = i
            data.append(ins_dict)
    return render(request, 'motor_product/admin/insurer_status.html', {'data': data})
