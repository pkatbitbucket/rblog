from suds.plugin import MessagePlugin

from utils import motor_suds_logger, statsd


class LogPlugin(MessagePlugin):

    def __init__(self, insurer_slug):
        self.insurer = insurer_slug
        self.name = 'logger'

    def set_name(self, name):
        if name is not None:
            self.name = name
        return self

    def log(self, msg):
        motor_suds_logger.info('LOG ' + self.insurer +
                               ' ' + self.name + ' ' + str(msg))

    def sending(self, context):
        motor_suds_logger.info('REQUEST ' + self.insurer +
                               ' ' + self.name + ' ' + context.envelope)

    def received(self, context):
        motor_suds_logger.info(
            'RESPONSE ' + self.insurer + ' ' + self.name + ' ' + context.reply)


def getLogger(insurer_slug):
    return LogPlugin(insurer_slug)


class GrafanaStatsd(object):
    statsd_string = "%(product)s.detailed.%(network)s.%(device)s.%(utm_campaign)s.web.%(vehicle_type)s.%(action)s"
    statsd_string_basic = "%(product)s.web.%(vehicle_type)s.%(action)s"

    @classmethod
    def send_incr(cls, request, action, vehicle_type, product='motor'):
        network = request.COOKIES.get('network', None)
        device = request.COOKIES.get('device', None)

        campaign = request.COOKIES.get('utm_campaign', None)
        if not campaign:
            campaign = request.COOKIES.get('vt_campaign', 'unknown')

        if network and device and campaign:
            event_name = cls.statsd_string % {'product': product,
                                              'network': network,
                                              'device': device,
                                              'utm_campaign': campaign,
                                              'vehicle_type': vehicle_type,
                                              'action': action,
                                              }
            statsd.incr(event_name, 1)

        event_name = cls.statsd_string_basic % {'product': product,
                                                'vehicle_type': vehicle_type,
                                                'action': action,
                                                }
        statsd.incr(event_name, 1)

    @classmethod
    def get_timer(cls, action, vehicle_type, product='motor'):
        event_name = cls.statsd_string_basic % {'product': product,
                                                'vehicle_type': vehicle_type,
                                                'action': 'timing.%s' % action,
                                                }
        timer = statsd.timer(event_name).start()
        return timer
