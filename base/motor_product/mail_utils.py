import traceback
import urlparse

from django.conf import settings
from django.core.mail import EmailMessage, get_connection
from django.core.urlresolvers import reverse
from django.utils import timezone

import cf_cns
from motor_product.models import Insurer, Quote, VehicleMaster
from utils import LMSUtils, clean_name, get_property, motor_logger

MANAGEMENT_EMAIL = 'motorteam@coverfox.com'
MANAGEMENT_POLICY_EMAIL = 'motorpolicy@coverfox.com'

motor_proposal_failure = """
========================
Transaction id: %s (%s)
Insurer: %s
Vehicle Type: %s
Vehicle: %s %s %s
Customer Name: %s
Contact Number: %s
========================

%s
"""


def send_new_offline_processing_alert(transaction):
    try:
        proposer = transaction.proposer
        msg = motor_offline_msg % (proposer.first_name, proposer.last_name, proposer.email,
                                   proposer.mobile, transaction.insurer, transaction.premium_paid, transaction.transaction_id)
        motorbot.send_message(SlackUtils.MOTOR_CHANNEL, msg)
    except:
        stacktrace = traceback.format_exc()
        motor_logger.info(stacktrace)
    return True


def send_new_inspection_alert(inspection):
    try:
        proposer = inspection.transaction.proposer
        date = inspection.created_on.astimezone(
            timezone.get_current_timezone()).strftime('%d-%m-%Y')
        time = inspection.created_on.astimezone(
            timezone.get_current_timezone()).strftime('%H:%M')
        msg = motor_inspection_msg % (proposer.first_name, proposer.last_name, proposer.email, proposer.mobile,
                                      date, time, inspection.address, inspection.insurer, inspection.transaction.transaction_id)
        motorbot.send_message(SlackUtils.MOTOR_CHANNEL, msg)
    except:
        stacktrace = traceback.format_exc()
        motor_logger.info(stacktrace)
    return True


def send_transaction_fail_alert(transaction):
    try:
        if not settings.PRODUCTION:
            return True

        send_mail_for_failed_transaction(transaction)
        # Trigger LMS event
        trigger_lms_event('bought_failed', transaction)
    except:
        stacktrace = traceback.format_exc()
        motor_logger.info(stacktrace)
    return True


def send_transaction_success_alert(transaction, created=False):
    try:
        if not settings.PRODUCTION:
            return True
        if created:
            send_mail_for_successful_transaction(transaction)
        # Trigger LMS event
        trigger_lms_event('bought_success', transaction)
    except:
        stacktrace = traceback.format_exc()
        motor_logger.info(stacktrace)
    return True


def send_proposal_fail_alert(transaction):
    try:
        if not settings.PRODUCTION:
            return True
        # send_mail_for_failed_transaction(transaction)
        trigger_lms_event('proposal_failed', transaction)
    except:
        stacktrace = traceback.format_exc()
        motor_logger.info(stacktrace)
    return True


def send_message(obj, sendcopy=False):
    get_connection(fail_silently=False).send_messages([obj])
    if sendcopy:
        # obj['use_mandrill'] = False
        obj['to_email'] = MANAGEMENT_EMAIL
        get_connection(fail_silently=False).send_messages([obj])


def send_mail_for_discount_code(parameters):
    try:
        obj = {
            'user_id': parameters['emailId'],
            'name': parameters['emailId'],
            'to_email': parameters['emailId'],
            'company': parameters['company'],
            'code': parameters['code'],
        }
        cf_cns.notify('MOTOR_DISCOUNT_CODE_MAIL', to=(obj.get('name'), obj['to_email']), obj=obj)
    except:
        pass
    return True


def send_mail_for_quote(parameters):
    try:
        for quote in parameters['quotes']:
            insurer = Insurer.objects.get(slug=quote['insurer'])
            buy_url = settings.SITE_URL.strip("/") + '/motor/' + parameters[
                'vehicle_type'] + '/' + insurer.slug + '/generateForm/' + parameters['quoteId'] + '/'
            quote['insurer'] = insurer.title
            quote['buy_url'] = buy_url

        vehicle_id = parameters['quote_parameters']['vehicleId']
        vehicle = VehicleMaster.objects.get(id=vehicle_id)
        obj = {
            'user_id': parameters['emailId'],
            'name': parameters['emailId'],
            'vehicle': vehicle.make.name + ' ' + vehicle.model.name + ' ' + vehicle.variant,
            'quote_parameters': parameters['quote_parameters'],
            'quotes': parameters['quotes'],
            'quote_url': settings.SITE_URL.strip("/") + '/motor/car-insurance/' + parameters['quoteId'] + '/',
            'to_email': parameters['emailId'],
        }
        cf_cns.notify('MOTOR_QUOTES_MAIL', to=(obj.get('name'), obj['to_email']), obj=obj)
    except:
        stacktrace = traceback.format_exc()
        motor_logger.info(stacktrace)
    return True


def send_mail_for_successful_transaction(transaction):
    try:
        # Disable mailing if payment mode is CD_ACCOUNT
        if transaction.quote.raw_data.get('payment_mode', None) == 'CD_ACCOUNT':
            return

        if transaction.quote.type == Quote.OFFLINE:
            return

        user = transaction.tracker.user
        if user and user.is_authenticated():
            user_id = user.email
        else:
            user_id = transaction.transaction_id

        vehicle_type = "Car"
        if transaction.vehicle_type == "Twowheeler":
            vehicle_type = "Two Wheeler"

        obj = {
            'user_id': user_id,
            'to_email': transaction.proposer.email,
            'name': clean_name(transaction.proposer.first_name),
            'vehicle_type': vehicle_type,
            'insurer_title': transaction.insurer.title,
            'transaction_number': 'CFOX-%s' % (transaction.id),
            'policy_document_url': transaction.policy_url,
            'policy_tat': transaction.policy_tat_verbose,
            'policy_type': transaction.quote.policy_type,
            'manage_policies_url': urlparse.urljoin(settings.SITE_URL, reverse('account_manager:manage_policies')),
        }
        if transaction.premium_paid:
            obj['premium'] = transaction.premium_paid
        else:
            obj['premium'] = ''
        # send_message(obj, True)
        cf_cns.notify('MOTOR_TRANSACTION_SUCCESS_MAIL', to=(obj.get('name'), obj['to_email']),
                      obj=obj, mandrill_template='newbase')
    except:
        stacktrace = traceback.format_exc()
        motor_logger.info(stacktrace)


def send_mail_for_failed_transaction(transaction):
    try:
        # Disable mailing if payment mode is CD_ACCOUNT
        if transaction.quote.raw_data.get('payment_mode', None) == 'CD_ACCOUNT':
            return

        user = transaction.tracker.user
        if user and user.is_authenticated():
            user_id = user.email
        else:
            user_id = transaction.transaction_id

        vehicle_type = "Car"
        if transaction.vehicle_type == "Twowheeler":
            vehicle_type = "Two Wheeler"

        obj = {
            'user_id': user_id,
            'to_email': transaction.proposer.email,
            'name': clean_name(transaction.proposer.first_name),
            'vehicle_type': vehicle_type,
            'insurer_slug': transaction.insurer.slug,
            'transaction_id': transaction.transaction_id,
            'insurer_title': transaction.insurer.title,
        }
        if transaction.premium_paid:
            obj['premium'] = transaction.premium_paid
        else:
            obj['premium'] = ''
        cf_cns.notify('MOTOR_TRANSACTION_FAILURE_MAIL', to=(obj.get('name'), obj['to_email']),
                      obj=obj, mandrill_template='newbase')
    except:
        stacktrace = traceback.format_exc()
        motor_logger.info(stacktrace)


def send_mail_for_admin(parameters):
    try:
        mail_id = MANAGEMENT_EMAIL
        if parameters.get('type', None) == 'policy':
            mail_id = MANAGEMENT_POLICY_EMAIL

        obj = {
            'user_id': mail_id,
            'name': 'Motor Team',
            'to_email': mail_id,
            'title': parameters['title'],
            'data': parameters['data']
        }
        cf_cns.notify('MOTOR_ADMIN_MAIL', to=(obj.get('name'), obj['to_email']), obj=obj)
    except:
        stacktrace = traceback.format_exc()
        motor_logger.info(stacktrace)
    return True


def send_mail_for_qa(parameters):
    try:
        cf_cns.notify('QA_EMAILS', subject_content=parameters['title'],
                      html_content=parameters['data'])
    except:
        stacktrace = traceback.format_exc()
        motor_logger.info(stacktrace)
    return True


def send_mail_for_proposal_failure(transaction, extra_data):
    if not settings.PRODUCTION:
        return

    insurer = transaction.insurer
    vehicle = transaction.quote.vehicle
    form_data = transaction.raw['user_form_details']
    customer_name = form_data['cust_first_name'].upper() + ' ' + form_data['cust_last_name'].upper()

    message = motor_proposal_failure % (transaction.id,
                                        transaction.transaction_id,
                                        insurer.slug,
                                        transaction.vehicle_type,
                                        vehicle.make.name,
                                        vehicle.model.name,
                                        vehicle.variant,
                                        customer_name,
                                        form_data['cust_phone'],
                                        str(extra_data)
                                        )

    title = 'ALERT: Proposal failed (%s)' % insurer.slug
    send_mail_for_qa({
        'title': title,
        'data': message,
    })


def trigger_lms_event(event_type, transaction):

    product_type = 'motor-bike' if transaction.vehicle_type == 'Twowheeler' else 'motor'

    proposer_full_name = [get_property(transaction, 'proposer.first_name'),
                          get_property(transaction, 'proposer.last_name')]

    SUCCESS_STATUS = {
        'bought_success': True,
        'bought_failed': False,
        'proposal_failed': False,
    }

    vehicle_data = {
        'make': get_property(transaction, 'quote.vehicle.make.name'),
        'model': get_property(transaction, 'quote.vehicle.model.name'),
        'variant': get_property(transaction, 'quote.vehicle.variant'),
        'cc': get_property(transaction, 'quote.vehicle.cc'),
        'seating_capacity': get_property(transaction, 'quote.vehicle.seating_capacity'),
        'fuel_type': get_property(transaction, 'quote.vehicle.fuel_type'),
    }

    data = {
        'name': ' '.join(filter(None, proposer_full_name)),
        'insurer': get_property(transaction, 'insurer.slug'),
        'insurer_title': get_property(transaction, 'insurer.title'),
        'transaction_id': transaction.transaction_id,
        'policy_number': transaction.policy_number,
        'policy_document': get_property(transaction, 'policy_document.url'),
        'premium_paid': transaction.premium_paid,
        'form_details': get_property(transaction, 'raw.user_form_details'),
        'vehicle': vehicle_data,
        'payment_mode': get_property(transaction, 'quote.raw_data.payment_mode') or 'PAYMENT_GATEWAY',
        'error_response': get_property(transaction, 'raw.error_response'),
        'success': get_property(SUCCESS_STATUS, event_type),
        'payment_on': LMSUtils.convert_datetime(transaction.payment_on),
    }

    LMSUtils.send_lms_event(
        event=event_type,
        product_type=product_type,
        tracker=transaction.tracker,
        data=data,
        mobile=get_property(transaction, 'proposer.mobile') or get_property(transaction, "raw.user_form_details.cust_phone"),
        email=get_property(transaction, 'proposer.email') or get_property(transaction, "raw.user_form_details.cust_email")
    )


def send_low_cd_account_balance(cdaccount):
    account_name = cdaccount.insurer.title
    balance = cdaccount.balance
    message = "CD Account balance for %s is %s" % (account_name, balance)
    try:
        to_mail = settings.CD_LOW_BALANCE_MAIL
        email = EmailMessage("Low balance in CD account", message, settings.DEFAULT_FROM_EMAIL, to_mail)
        email.send()
    except:
        stacktrace = traceback.format_exc()
        motor_logger.info(stacktrace)
    return True
