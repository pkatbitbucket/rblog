from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
from motor_product.models import Insurer, DiscountLoading
import os
from django.db import transaction
from motor_product import parser_utils

# dict to standardize fuel type
FUEL_TO_TYPE = {
    'P': 'Petrol',
    'D': 'Diesel',
    'C': 'CNG / Petrol',
    'B': 'Electricity',
    'PETROL': 'Petrol',
    'CNG': 'CNG / Petrol',
    'LPG': 'LPG / Petrol',
    'DIESEL': 'Diesel',
    '': 'Not Defined'
}

# dict to map insurer with slug
INSURER_SLUG_MAP = {
    'bajaj-allianz': 'bajaj_allianz',
    'bharti-axa': 'bharti_axa',
    'future-generali': 'future_generali',
    'hdfc-ergo': 'hdfc_ergo',
    'icici-lombard': 'icici_lombard',
    'iffco-tokio': 'iffco_tokio',
    'l-t': 'landt',
    'liberty-videocon': 'liberty_videocon',
    'new-india': 'new_india',
    'oriental': 'oriental',
    'reliance': 'reliance',
    'tata-aig': 'tata_aig',
    'universal-sompo': 'universal_sompo',
}

# dict to map vehicle with type
TYPE_TO_VEHICLE = {
    'twowheeler': 'Twowheeler',
    'fourwheeler': 'Private Car'
}

# parameters for future_generali
future_generali_fourwheeler = {
    'make': 1,
    'model': 2,
    'fuel_type': 7,
    'lower_cc_limit': 6,
    'upper_cc_limit': 6,
    'lower_age_limit': [0, 2, 6],
    'upper_age_limit': [1, 5, 8],
    'discount': [3, 4, 5],
}
# parameters for universal sompo
universal_sompo_fourwheeler = {
    'make': 0,
    'model': 1,
    'lower_cc_limit': 2,
    'upper_cc_limit': 2,
    'lower_age_limit': 3,
    'upper_age_limit': 4,
    'discount': {
        'AN': 5, 'AP': 6, 'AR': 7, 'AS': 8, 'BR': 9,
        'CH': 10, 'CG': 11, 'DN': 12, 'DD': 13,
        'DL': 14, 'GA': 15, 'GJ': 16, 'HR': 17,
        'HP': 18, 'JK': 19, 'JH': 20, 'KA': 21,
        'KL': 22, 'LD': 23, 'MP': 24, 'MH': 25,
        'MN': 26, 'ML': 27, 'MZ': 28, 'NL': 29,
        'OD': 30, 'PY': 31, 'PB': 32, 'RJ': 33,
        'SK': 34, 'TN': 35, 'TR': 36, 'UP': 37,
        'UK': 38, 'WB': 39,
    }
}

# dict to dynamically resolving dictionary name without eval()
PARAMETERS_MAP = {
    'future_generali_fourwheeler': future_generali_fourwheeler,
    'universal_sompo_fourwheeler': universal_sompo_fourwheeler,
}


# Create Command class extending BaseCommand
class Command(BaseCommand):

    # Add option to take insurer slug
    option_list = BaseCommand.option_list + (
        make_option('--insurer', '-i', dest='insurer',
                    help='set insurer slug', metavar="SLUG"),
    )

    # Add option to take vehicle type
    option_list = option_list + (
        make_option('--type', '-t', dest='type',
                    help='set vehicle type [twowheeler] or [fourwheeler]', type='string'),
    )

    # Add option to take excel file path
    option_list = option_list + (
        make_option('--path', '-p', dest='path',
                    help='set excel file path', metavar="FILE"),
    )

    # Lets define help command for users
    help = 'eg. python manage.py discount_loading -i <insurer-slug> -t <vehicle-type> -p <path to excel>'

    # Lets define function to handle options
    def handle(self, **options):
        # make sure file option is present
        if options['insurer'] is None:
            raise CommandError("Option `--insurer <insurer-slug>` must be specified.")

        # find insurer by slug
        insurer = Insurer.objects.filter(slug=options['insurer'])

        # Make sure insurer with this slug exist
        if len(insurer) < 1:
            raise CommandError("Enter valid insurer slug.")

        # store insurer in object attribute insurer
        self.insurer = insurer[0]

        # Make sure type opton is present
        if options['type'] is None:
            raise CommandError("Option `--type <vehicle-type>[fourwheeler, twowheeler]` must be specified")

        # Make sure vehicle type is correct
        if not options['type'] in TYPE_TO_VEHICLE.keys():
            raise CommandError("Enter valid vehicle type, eg. fourwheeler or twowheeler")

        self.vtype = options['type']

        # Make sure excel file was specified
        if options['path'] is None:
            raise CommandError("Option `--path <excel file path>` must be specified.")

        # make sure file path resolves
        if not os.path.isfile(options['path']):
            raise CommandError("File does not exist at the specified path.")

        # store file name in object attribute _file
        self._file = options['path']

        # Check for valid excel extention
        ext = os.path.splitext(self._file)[1]
        valid_extensions = ['.xls', '.xlsx']
        if ext not in valid_extensions:
            raise CommandError("Only excel files are allowed")

        # Lets finish the task
        self.lets_do()

    # Method to perform database task
    def lets_do(self):

        # Find out duplicate and all records
        duplicate_records, all_records = self.find_duplicate_and_make_new_records()

        # perform atomic transaction
        with transaction.atomic():

            # lets delete duplicate records
            duplicate_records.delete()

            # lets create all new records
            DiscountLoading.objects.bulk_create(all_records)

    # Method to find duplicate records and make new records
    def find_duplicate_and_make_new_records(self):

        # lets initialize a list to store all new records
        all_records = []

        # filter all records with selected vehicle type and insurer
        duplicate_records = DiscountLoading.objects.filter(
            insurer=self.insurer, vehicle_type=TYPE_TO_VEHICLE[self.vtype]
        )

        # try load correct dictionary as per option provided
        try:
            parameters = PARAMETERS_MAP[INSURER_SLUG_MAP[self.insurer.slug] + '_' + self.vtype]

        # catch AttributeError if parameters dictionary not found for current insurer
        except KeyError:
            raise CommandError("No option available for insurer with slug: %s" % self.insurer.slug)

        # loop through each row in provided excel file
        for record in parser_utils.parse_xlsx(self._file):

            # check and continue if record is empty
            if not record:
                continue

            # initialize empty dictionary to store discountloading options
            discount_dict_list = []

            # loop through selected parameters
            for k, v in parameters.items():

                # check for discount parameter to create different records based on ranges
                if k == 'discount':

                    # loop through each item in discount and create different dictionary for each item
                    for state, index in v.iteritems():
                        discount_dict = {}
                        discount_dict['insurer'] = self.insurer
                        discount_dict['state'] = state.strip()
                        discount_dict[k] = parser_utils.try_parse_into_float(record[index])
                        discount_dict_list.append(discount_dict)

            # again loop through each parameter item to add remaining values in each dictionary
            for k, v in parameters.items():

                # skip keys that are already filled
                if k in ['discount']:
                    continue

                # loop through each record for current row and add remaining values
                for _dict in discount_dict_list:

                    # fix datatype as per db column type
                    if k in ['lower_cc_limit', 'upper_cc_limit', 'lower_age_limit', 'upper_age_limit']:
                        _dict[k] = parser_utils.try_parse_into_integer(record[v])

                    # stadardize fuel type
                    elif k == 'fuel_type':
                        _dict[k] = FUEL_TO_TYPE.get(record[v], record[v])

                    else:
                        _dict[k] = record[v]

            # convert all dict that belongs to current row to DiscountLoading type and append to all records list
            all_records.extend(DiscountLoading(**i) for i in discount_dict_list)

        # return list of duplicate records and all records
        return duplicate_records, all_records
