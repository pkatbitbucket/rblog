from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
from motor_product.models import Vehicle
from motor_product import parser_utils
import putils
from django.conf import settings

INSURER_SLUG = 'icici-lombard'

srp_parameters = {
    'model_code': 0,
    'exshowroomprice': 8,
    'hub_rto': 3,
}


# Create Command class extending BaseCommand
class Command(BaseCommand):

    # Add option to take insurer slug
    option_list = BaseCommand.option_list + (
        make_option('--file', '-f', dest='file',
                    help='S3 file name', metavar="FILE"),
    )

    # Lets define help command for users
    help = 'eg. python manage.py icici_srp -f <s3-file-name>'

    # Lets define function to handle options
    def handle(self, **options):
        # Make sure excel file was specified
        if options['file'] is None:
            raise CommandError("Option `--file <s3-file-name>` must be specified.")

        # store file name in object attribute _file
        self._file = options['file']

        # Lets finish the task
        self.lets_do()

    # Method to perform database task
    def lets_do(self):
        srp_file_path = putils.get_file_from_s3(
            settings.INTEGRATION_AWS_ACCESS_KEY_ID,
            settings.INTEGRATION_AWS_SECRET_ACCESS_KEY,
            settings.INTEGRATION_BUCKET_NAME, self._file
        )
        rto_list, price_map = self.get_vehicle_pricing_map(srp_file_path)
        self.update_srp(rto_list, price_map)

    def get_vehicle_pricing_map(self, srp_file_path):
        pricings = parser_utils.read_file_to_return_vehicle_dictionary(
            srp_file_path, srp_parameters, linedelimiter='\r\n')

        rto_code_map = {}
        pricing_map = {}
        for price in pricings:
            if not pricing_map.get(price['model_code'], None):
                pricing_map[price['model_code']] = {}

            if not pricing_map[price['model_code']].get(price['hub_rto'], None):
                pricing_map[price['model_code']][
                    price['hub_rto']] = price['exshowroomprice']

            rto_code_map[price['hub_rto']] = True

        rto_code_list = map(lambda x: int(x), rto_code_map.keys())
        rto_code_list.sort()
        return rto_code_list, pricing_map

    def update_srp(self, rto_list, price_map):
        vehicles = Vehicle.objects.filter(insurer__slug=INSURER_SLUG)
        for _vehicle in vehicles:
            if not price_map.get(_vehicle.model_code, None):
                print 'Vehicle price not available for %s %s' % (_vehicle.make, _vehicle.model)
                continue
            _rdl = _vehicle.raw_data.split('|')[:45]
            for rto in rto_list:
                try:
                    price = price_map[_vehicle.model_code][str(rto)]
                except KeyError:
                    price = '0'
                _rdl.append(price)
            _vehicle.raw_data = '|'.join(_rdl)
            _vehicle.save()
