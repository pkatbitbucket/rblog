from django.core.management.base import BaseCommand
from django.template.defaultfilters import title, slugify
from motor_product.models import Garage, Insurer
from wiki.models import City

import csv


class Command(BaseCommand):
    args = '<csv_path csv_path ...>'
    help = 'Loades list of motor Garages from csv file'

    def handle(self, *args, **options):

        with open(args[0], 'rbtU') as f:
            reader = csv.DictReader(f, delimiter='|')
            i = 0
#            writer = csv.writer(open('/home/coverfox/cities_unmapped.csv', 'w'))
#            city = ['City']
#            writer.writerow(city)
            for row in reader:
                for key, value in row.items():
                    row[key] = title(unicode(value, errors='ignore'))

                # try:
                #     pincode = re.search(r'(\d{6})', row['Garage Address']).group()
                #     if pincode != row['Pin Code']:
                #         row['Pin Code'] = raw_input("\nFor address: %s\n Select correct pin code '%s' or '%s'\n" % (row['Garage Address'], row['Pin Code'], pincode))
                # except AttributeError:
                #     pass

                try:
                    city_map = {
                        'nasik': 'nashik',
                        'cochin': 'kochi',
                        'ayathil': 'kollam',
                        'vishakhapatnam': 'visakhapatnam',
                        'kota': 'kota-kota',
                        'trivandrum': 'thiruvananthapuram',
                        'trivendrum': 'thiruvananthapuram',
                        'aitsar': 'jalandhar',
                        'tirupur': 'tiruppur',
                        'gaziabad': 'ghaziabad',
                        'vizag': 'visakhapatnam',
                        'trichy': 'tiruchirappalli',
                        'koyambedu': 'chennai',
                        'hubli': 'hubli-dharwad',
                        'farrukhabad': 'farrukhabad-cum-fatehgarh',
                        'manglore': 'mangalore',
                        'sonepat': 'sonipat',
                        'bereilly': 'bareilly',
                        'ferozepur': 'firozpur-cantt',
                        'hissar': 'hisar',
                        'howrah': 'haora',
                        'hooghly': 'hugli-chinsurah',
                        'ambernath': 'ambarnath',
                        'worli': 'mumbai',
                        'paschim-midnapore': 'medinipur',
                        'balasore': 'baleshwar',
                        'vijaywada': 'vijayawada',
                        'aligarah': 'aligarh',
                        'hoogly': 'hugli-chinsurah',
                        'paschim-midnapur': 'medinipur',
                        'bhubaneshwar': 'bhubaneswar',
                        'jeypore': 'jeypur',
                        'rourkela': 'raurkela',
                        'berhampur': 'brahmapur',
                        'guindy': 'chennai',
                        'govindpura': 'bhopal',
                        'sangli': 'sangli-miraj-kupwad',
                        'morbi': 'morvi',
                        'nagole': 'hyderabad',
                        'nanded': 'nanded-waghala',
                        'andaman': 'port-blair',
                        'nagarcoil': 'nagercoil',
                        'yamuna-nagar': 'yamunanagar',
                        'kankarbagh': 'patna',
                        'hagipur': 'hajipur-vaishali',
                        'kasaragode': 'kasargod',
                        'puducherry': 'pondicherry',
                        'mhow': 'indore',
                        'pondy': 'pondicherry',
                        'theni': 'theni-allinagaram',
                        'porvorim': 'altoporiorim-alto-beetim',
                    }
#                    state_map = {'chattisgarh': 'chhattisgarh'}
                    insurer_map = {
                        'l-and-t': 'l-t',
                    }
                    insurer_slug = insurer_map.get(
                        slugify(row['Company']), slugify(row['Company']))
                    insurer = Insurer.objects.get(slug=insurer_slug)
                    slug = city_map.get(
                        slugify(row['City']), slugify(row['City']))
                    city = City.objects.get(slug=slug)
                except City.DoesNotExist:
                    continue
#                    new_slug = raw_input("\nCity matching with slug %s doesn't exist, enter a valid city slug to match with '%s'\n" % (slug, slug))
#                    city_map[slug] = new_slug
#                    city = City.objects.get(slug=new_slug, state__slug=slugify(row['State']))
                except City.MultipleObjectsReturned:
                    continue
#                    state_slug = state_map.get(slugify(row['State']), slugify(row['State']))
#                    city = City.objects.get(slug=slug, state__slug=state_slug)

                kwargs = {'name': row['Garage'], 'pin': row['Pin Code']}

                try:
                    garage = Garage.objects.get(**kwargs)
                except Garage.DoesNotExist:
                    garage = Garage(**kwargs)
                    garage.city = city
                    garage.address = row['Garage Address']
                    garage.save()

                garage.insurers.add(insurer)
                i += 1
                print i, garage
