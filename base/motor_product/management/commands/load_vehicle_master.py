from django.core.management.base import BaseCommand
from motor_product.models import VehicleMaster, Make, Model
import csv


class Command(BaseCommand):
    args = '<csv path >'
    help = 'Loades list of Vehicle master from csv file'

    def handle(self, *args, **options):
        with open(args[0], 'rbtU') as f:
            reader = csv.DictReader(f, delimiter='|')
            i = 0
            for row in reader:
                try:
                    make = Make.objects.get(name=row['Make'])
                except Make.DoesNotExist:
                    print 'Make doesnot exist entering new make in DB'
                    make = Make()
                    make.name = row['Make']
                    make.save()
                try:
                    model = Model.objects.get(name=row['Model'])
                except Model.DoesNotExist:
                    print 'Model doesnot exist entering new Model in DB'
                    model = Model()
                    model.name = row['Model']
                    model.make = make
                    model.save()
                except model.MultipleObjectsReturned:
                    print 'More than one model exist in DB picking the one with same make'
                    model = Model.objects.filter(name=row['Model'], make=make)[0]

                kwargs = {'make': make, 'model': model, 'variant': row['Variant'], 'cc': row['Cc'], 'fuel_type': row['Fuel Type']}
                try:
                    vehicle = VehicleMaster.objects.get(**kwargs)
                    print 'Already exist'
                except VehicleMaster.DoesNotExist:
                    vehicle = VehicleMaster(**kwargs)
                    vehicle.seating_capacity = row['Seating Capacity']
                    if row['Vehicle Type'] == 'Twowheeler':
                        vehicle.vehicle_type = row['Vehicle Type']
                    vehicle.save()
                i += 1
                print i, vehicle





