import traceback
from django.core.management.base import BaseCommand
from optparse import make_option

from motor_product.scripts.twowheeler import master_twowheeler
from motor_product.insurers.bajaj_allianz.scripts import bajaj_twowheeler
from motor_product.insurers.universal_sompo.scripts import universal_twowheeler
from motor_product.insurers.bharti_axa.scripts import bharti_twowheeler
from motor_product.insurers.iffco_tokio.scripts import iffco_twowheeler
from motor_product.insurers.hdfc_ergo.scripts import hdfc_twowheeler

from motor_product.scripts.fourwheeler import master_fourwheeler
from motor_product.insurers.bajaj_allianz.scripts import bajaj_fourwheeler
from motor_product.insurers.bharti_axa.scripts import bharti_fourwheeler
from motor_product.insurers.iffco_tokio.scripts import iffco_fourwheeler
from motor_product.insurers.hdfc_ergo.scripts import hdfc_fourwheeler
from motor_product.insurers.landt.scripts import landt_fourwheeler
from motor_product.insurers.universal_sompo.scripts import universal_fourwheeler
from motor_product.insurers.new_india.scripts import newindia_fourwheeler
from motor_product.insurers.future_generali.scripts import future_fourwheeler
from motor_product.insurers.tata_aig.scripts import tata_fourwheeler
from motor_product.insurers.icici_lombard.scripts import icici_fourwheeler
from motor_product.insurers.liberty_videocon.scripts import liberty_fourwheeler
from motor_product.insurers.reliance.scripts import reliance_fourwheeler
from motor_product.insurers.oriental.scripts import oriental_fourwheeler

from motor_product.insurers.new_india.scripts import newindia_fourwheeler_dl
from motor_product.insurers.landt.scripts import landt_fourwheeler_dl
from motor_product.insurers.liberty_videocon.scripts import liberty_fourwheeler_dl
from motor_product.insurers.universal_sompo.scripts import universal_fourwheeler_dl
from motor_product.insurers.universal_sompo.scripts import universal_twowheeler_dl

from motor_product.scripts.rto import master_rto
from motor_product.insurers.bajaj_allianz.scripts import bajaj_rto
from motor_product.insurers.bharti_axa.scripts import bharti_rto
from motor_product.insurers.hdfc_ergo.scripts import hdfc_rto
from motor_product.insurers.iffco_tokio.scripts import iffco_rto
from motor_product.insurers.new_india.scripts import newindia_rto
from motor_product.insurers.universal_sompo.scripts import universal_rto
from motor_product.insurers.landt.scripts import landt_rto
from motor_product.insurers.icici_lombard.scripts import icici_rto
from motor_product.insurers.reliance.scripts import reliance_rto

from motor_product.scripts.garages import garages

VEHICLE_SCRIPT_MAP = {
    'twowheeler': {
        None: master_twowheeler,
        'bajaj-allianz': bajaj_twowheeler,
        'universal-sompo': universal_twowheeler,
        'bharti-axa': bharti_twowheeler,
        'iffco-tokio': iffco_twowheeler,
        'hdfc-ergo': hdfc_twowheeler,
    },
    'fourwheeler': {
        None: master_fourwheeler,
        'bajaj-allianz': bajaj_fourwheeler,
        'bharti-axa': bharti_fourwheeler,
        'iffco-tokio': iffco_fourwheeler,
        'hdfc-ergo': hdfc_fourwheeler,
        'l-t': landt_fourwheeler,
        'universal-sompo': universal_fourwheeler,
        'new-india': newindia_fourwheeler,
        'future-generali': future_fourwheeler,
        'tata-aig': tata_fourwheeler,
        'icici-lombard': icici_fourwheeler,
        'liberty-videocon': liberty_fourwheeler,
        'reliance': reliance_fourwheeler,
        'oriental': oriental_fourwheeler,
    }
}

DISCOUNTING_SCRIPT_MAP = {
    'fourwheeler': {
        'new-india': newindia_fourwheeler_dl,
        'universal-sompo': universal_fourwheeler_dl,
        'l-t': landt_fourwheeler_dl,
        'liberty-videocon': liberty_fourwheeler_dl,
    },
    'twowheeler': {
        'universal-sompo': universal_twowheeler_dl
    }
}

RTO_SCRIPT_MAP = {
    None: master_rto,
    'bajaj-allianz': bajaj_rto,
    'bharti-axa': bharti_rto,
    'hdfc-ergo': hdfc_rto,
    'iffco-tokio': iffco_rto,
    'new-india': newindia_rto,
    'universal-sompo': universal_rto,
    'l-t': landt_rto,
    'icici-lombard': icici_rto,
    'reliance': reliance_rto,
}


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--type', '-t',
                    dest='type',
                    default='twowheeler',
                    help='--type <vehicle-type>, Vehicle type twowheeler or fourwheeler.'),
        make_option('--insurer', '-i',
                    dest='insurer',
                    help='--insurer <insurer-slug>, Insurer slug.'),
    )

    def handle(self, *args, **options):
        try:
            if args[0] == 'vehicle':
                module = VEHICLE_SCRIPT_MAP[
                    options['type']][options['insurer']]

                commandarg = args[1]
                if commandarg == 'populate':
                    module.save_data()
                elif commandarg == 'map':
                    module.create_mappings()
                elif commandarg == 'delete':
                    if args[2] == 'data':
                        module.delete_data()
                    elif args[2] == 'map':
                        module.delete_mappings()
            elif args[0] == 'discounting':
                module = DISCOUNTING_SCRIPT_MAP[
                    options['type']][options['insurer']]
                commandarg = args[1]
                if commandarg == 'populate':
                    module.save_data(args[2])
                elif commandarg == 'delete':
                    module.delete_data()
            elif args[0] == 'rto':
                module = RTO_SCRIPT_MAP[options['insurer']]
                commandarg = args[1]
                if commandarg == 'populate':
                    if args[2] == 'data':
                        module.save_data()
                    elif args[2] == 'map':
                        module.create_map()
                elif commandarg == 'delete':
                    if args[2] == 'data':
                        module.delete_data()
                    elif args[2] == 'map':
                        module.delete_map()
            elif args[0] == 'garages':
                module = garages
                commandarg = args[1]
                if commandarg == 'populate':
                    garages.save_data()
                elif commandarg == 'delete':
                    garages.delete_data()
        except:
            print traceback.format_exc()
