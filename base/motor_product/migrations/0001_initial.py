# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
import django.db.models.deletion
import customdb.thumbs
import django_extensions.db.fields
import motor_product.models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Addon',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250, blank=True)),
                ('code', models.CharField(unique=True, max_length=150, blank=True)),
                ('description', models.TextField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='AddonExclusion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('age_limit', models.IntegerField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category', models.CharField(max_length=10, choices=[(b'URBAN', b'Urban'), (b'RURAL', b'Rural')])),
                ('line_1', models.CharField(max_length=100, verbose_name='Street')),
                ('line_2', models.CharField(max_length=100, verbose_name='Street', blank=True)),
                ('landmark', models.TextField(verbose_name='Landmark', blank=True)),
                ('city', models.CharField(max_length=100, verbose_name='City')),
                ('district', models.CharField(default=b'', max_length=100, verbose_name='District')),
                ('state', models.CharField(max_length=100, verbose_name='State')),
                ('country', models.CharField(max_length=100, verbose_name='Country')),
                ('pincode', models.CharField(max_length=10, verbose_name='Pincode')),
                ('raw', jsonfield.fields.JSONField(default={}, verbose_name='raw', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='BestPricing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('insurer_slug', models.CharField(default=None, max_length=255, null=True)),
                ('city', models.CharField(max_length=255)),
                ('year', models.CharField(max_length=6)),
                ('premium', models.FloatField()),
                ('idv', models.FloatField(default=0)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=10)),
                ('domains', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address_type', models.CharField(max_length=30, choices=[(b'COMMUNICATION_ADDRESS', b'Communication Address'), (b'PERMANENT_ADDRESS', b'Permanent Address'), (b'BILLING_ADDRESS', b'Billing address'), (b'RISK_ADDRESS', b'Risk address'), (b'REGISTRATION_OFFICE', b'Registered office'), (b'NEW_ADDRESS', b'New address'), (b'OTHER_OFFICES', b'Other offices')])),
                ('email', models.EmailField(max_length=254, verbose_name='Email')),
                ('mobile', models.CharField(max_length=11, verbose_name='Mobile')),
                ('landline', models.CharField(max_length=12, verbose_name='Landline')),
                ('date_of_birth', models.DateField(null=True, blank=True)),
                ('title', models.CharField(max_length=10, choices=[(b'MR.', b'Mr.'), (b'MRS.', b'Mrs.'), (b'MISS', b'Miss'), (b'DR', b'Dr'), (b'CAPTAIN', b'Captain'), (b'LT', b'Lt'), (b'MAJOR', b'Major'), (b'GENERAL', b'General'), (b'COLONEL', b'Colonel'), (b'BRIGADIER', b'Brigadier'), (b'JUDGE', b'Judge'), (b'PROF', b'Prof'), (b'SIR', b'Sir'), (b'FATHER', b'Father'), (b'MASTER', b'Master'), (b'MADAM', b'Madam')])),
                ('first_name', models.CharField(max_length=255, verbose_name='First name')),
                ('middle_name', models.CharField(max_length=255, verbose_name='First name', blank=True)),
                ('last_name', models.CharField(max_length=255, verbose_name='Last name')),
                ('gender', models.CharField(max_length=20, verbose_name='Gender', choices=[(b'MALE', b'Male'), (b'FEMALE', b'Female')])),
                ('father_first_name', models.CharField(max_length=255, verbose_name="Father's first name")),
                ('father_last_name', models.CharField(max_length=255, verbose_name="Father's last name", blank=True)),
                ('send_email', models.BooleanField(default=False)),
                ('send_sms', models.BooleanField(default=False)),
                ('phone_call', models.BooleanField(default=False)),
                ('nationality', models.CharField(max_length=50)),
                ('raw', jsonfield.fields.JSONField(default={}, verbose_name='raw', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='DiscountCode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=10)),
                ('email', models.EmailField(max_length=254, null=True, blank=True)),
                ('mobile', models.CharField(max_length=11, null=True, blank=True)),
                ('discount_type', models.IntegerField(default=1, choices=[(1, 'Special'), (2, 'Corporate')])),
                ('is_invalid', models.BooleanField(default=False)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('raw', jsonfield.fields.JSONField(default={}, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='DiscountLoading',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('make', models.CharField(max_length=250, null=True, blank=True)),
                ('model', models.CharField(max_length=250, null=True, blank=True)),
                ('variant', models.CharField(max_length=250, null=True, blank=True)),
                ('fuel_type', models.CharField(max_length=10, null=True, blank=True)),
                ('lower_age_limit', models.IntegerField(null=True, blank=True)),
                ('upper_age_limit', models.IntegerField(null=True, blank=True)),
                ('lower_cc_limit', models.IntegerField(null=True, blank=True)),
                ('upper_cc_limit', models.IntegerField(null=True, blank=True)),
                ('state', models.CharField(max_length=50, null=True, blank=True)),
                ('ncb', models.IntegerField(null=True, blank=True)),
                ('discount', models.FloatField(null=True, blank=True)),
                ('vehicle_type', models.CharField(default=b'Private Car', max_length=25, choices=[(b'Private Car', b'Private Car'), (b'Twowheeler', b'Twowheeler')])),
            ],
        ),
        migrations.CreateModel(
            name='Garage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('address', models.TextField()),
                ('pin', models.CharField(max_length=6)),
            ],
        ),
        migrations.CreateModel(
            name='Inspection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address', models.CharField(max_length=250, blank=True)),
                ('scheduled_on', models.DateTimeField()),
                ('raw', jsonfield.fields.JSONField(default={}, verbose_name='raw', blank=True)),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='created')),
                ('updated_on', models.DateTimeField(auto_now=True, verbose_name='modified')),
            ],
        ),
        migrations.CreateModel(
            name='Insurer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name='title')),
                ('slug', models.SlugField(max_length=70, verbose_name='slug')),
                ('body', models.TextField(verbose_name='body', blank=True)),
                ('tease', models.TextField(help_text='Concise text suggested. Does not appear in RSS feed.', verbose_name='tease', blank=True)),
                ('logo', customdb.thumbs.ImageWithThumbsField(null=True, upload_to=motor_product.models.get_file_path, blank=True)),
                ('status', models.IntegerField(default=1, verbose_name='status', choices=[(1, 'Draft'), (2, 'Public')])),
                ('is_active_for_car', models.BooleanField(default=False)),
                ('disable_cache_for_car', models.BooleanField(default=True)),
                ('is_active_for_twowheeler', models.BooleanField(default=False)),
                ('disable_cache_for_twowheeler', models.BooleanField(default=True)),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='created')),
                ('updated_on', models.DateTimeField(auto_now=True, verbose_name='modified')),
            ],
            options={
                'ordering': ('-created_on',),
                'get_latest_by': 'created_on',
                'verbose_name': 'insurer',
                'verbose_name_plural': 'insurers',
            },
        ),
        migrations.CreateModel(
            name='LnTErrorDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ErrorLevel', models.CharField(max_length=100, blank=True, db_column='errorlevel')),
                ('Details', models.CharField(max_length=100, blank=True, db_column='details')),
            ],
        ),
        migrations.CreateModel(
            name='LnTPolicyResponse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('TxnId', models.CharField(db_column='txnid', max_length=100, blank=True)),
                ('Status', models.CharField(db_column='status', max_length=100, blank=True)),
                ('raw', jsonfield.fields.JSONField(default={}, verbose_name='raw', blank=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('TxnErrorDetails', models.ForeignKey(blank=True, to='motor_product.LnTErrorDetail', null=True, db_column='txnerrordetails_id')),
            ],
        ),
        migrations.CreateModel(
            name='LnTStatusDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ContactId', models.CharField(max_length=100, blank=True, db_column='contactid')),
                ('ReceiptNo', models.CharField(max_length=100, blank=True, db_column='receiptno')),
                ('PolicyNo', models.CharField(max_length=100, blank=True, db_column='policyno')),
                ('ProposalNo', models.CharField(max_length=100, blank=True, db_column='proposalno')),
            ],
        ),
        migrations.CreateModel(
            name='Make',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=70, verbose_name='name')),
            ],
        ),
        migrations.CreateModel(
            name='Model',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150, verbose_name='name')),
                ('vehicle_type', models.CharField(default=b'Private Car', max_length=25, choices=[(b'Private Car', b'Private Car'), (b'Twowheeler', b'Twowheeler')])),
                ('make', models.ForeignKey(blank=True, to='motor_product.Make', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ModelExclusion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('exclusion', models.ForeignKey(related_name='models', to='motor_product.AddonExclusion')),
                ('model', models.ForeignKey(to='motor_product.Model')),
            ],
        ),
        migrations.CreateModel(
            name='PrivateCar',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('raw', jsonfield.fields.JSONField(default={}, verbose_name='raw', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Quote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quote_id', django_extensions.db.fields.UUIDField(editable=False, blank=True)),
                ('is_new', models.BooleanField(default=True)),
                ('date_of_manufacture', models.DateField(null=True, blank=True)),
                ('raw_data', jsonfield.fields.JSONField(default={}, verbose_name='request_data', blank=True)),
                ('is_processed', models.BooleanField(default=False)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Receipt',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('raw', jsonfield.fields.JSONField(default={}, verbose_name='raw', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='RTOExclusion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('exclusion', models.ForeignKey(related_name='rtos', to='motor_product.AddonExclusion')),
            ],
        ),
        migrations.CreateModel(
            name='RTOInsurer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rto_code', models.CharField(max_length=100, blank=True)),
                ('rto_name', models.CharField(max_length=100, blank=True)),
                ('rto_state', models.CharField(max_length=100, blank=True)),
                ('rto_zone', models.CharField(max_length=10, blank=True)),
                ('master_rto_state', models.CharField(max_length=100, blank=True)),
                ('raw', jsonfield.fields.JSONField(default={}, verbose_name='raw', blank=True)),
                ('vehicle_type', models.CharField(default=b'Private Car', max_length=25, choices=[(b'Private Car', b'Private Car'), (b'Twowheeler', b'Twowheeler')])),
                ('insurer', models.ForeignKey(blank=True, to='motor_product.Insurer', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='RTOMapping',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_approved_mapping', models.BooleanField(default=False)),
                ('insurer', models.ForeignKey(blank=True, to='motor_product.Insurer', null=True)),
                ('rto_insurer', models.ForeignKey(blank=True, to='motor_product.RTOInsurer', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='RTOMaster',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rto_code', models.CharField(max_length=10, blank=True)),
                ('rto_name', models.CharField(max_length=100, blank=True)),
                ('rto_state', models.CharField(max_length=100, blank=True)),
                ('raw', jsonfield.fields.JSONField(default={}, verbose_name='raw', blank=True)),
                ('insurer_rtos', models.ManyToManyField(to='motor_product.RTOInsurer', through='motor_product.RTOMapping')),
            ],
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('transaction_id', django_extensions.db.fields.UUIDField(editable=False, blank=True)),
                ('proposal_number', models.CharField(max_length=250, blank=True)),
                ('policy_number', models.CharField(max_length=250, blank=True)),
                ('policy_start_date', models.DateTimeField(null=True, blank=True)),
                ('policy_end_date', models.DateTimeField(null=True, blank=True)),
                ('reference_id', models.CharField(max_length=250, blank=True)),
                ('policy_token', models.CharField(max_length=250, blank=True)),
                ('payment_token', models.CharField(max_length=250, blank=True)),
                ('premium_paid', models.CharField(max_length=10, blank=True)),
                ('payment_on', models.DateTimeField(null=True, blank=True)),
                ('status', models.CharField(max_length=50, choices=[(b'DRAFT', b'DRAFT'), (b'PENDING', b'Pending'), (b'IN_PROCESS', b'In Process'), (b'COMPLETED', b'Completed'), (b'CANCELLED', b'Cancelled'), (b'FAILED', b'Failed'), (b'MANUAL COMPLETED', b'Manual Completed')])),
                ('policy_document', models.FileField(null=True, upload_to=motor_product.models.get_policy_path, blank=True)),
                ('payment_done', models.BooleanField(default=False)),
                ('insured_details_json', jsonfield.fields.JSONField(default={}, verbose_name='insured_details', blank=True)),
                ('raw', jsonfield.fields.JSONField(default={}, verbose_name='raw', blank=True)),
                ('mailer_flag', models.BooleanField(default=False)),
                ('policy_request', models.TextField(blank=True)),
                ('policy_response', models.TextField(blank=True)),
                ('payment_request', models.TextField(blank=True)),
                ('payment_response', models.TextField(blank=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('vehicle_type', models.CharField(default=b'Private Car', max_length=25, choices=[(b'Private Car', b'Private Car'), (b'Twowheeler', b'Twowheeler')])),
                ('insured_car', models.ForeignKey(blank=True, to='motor_product.PrivateCar', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='UserVehicle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('raw', jsonfield.fields.JSONField(default={}, verbose_name='raw', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Vehicle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vehicle_code', models.CharField(max_length=250, blank=True)),
                ('name', models.CharField(max_length=250, blank=True)),
                ('make', models.CharField(max_length=250, blank=True)),
                ('make_code', models.CharField(max_length=50, blank=True)),
                ('model', models.CharField(max_length=250, blank=True)),
                ('model_code', models.CharField(max_length=50, blank=True)),
                ('variant', models.CharField(max_length=250, blank=True)),
                ('variant_code', models.CharField(max_length=50, blank=True)),
                ('cc', models.IntegerField(null=True, blank=True)),
                ('seating_capacity', models.IntegerField(null=True, blank=True)),
                ('fuel_type', models.CharField(blank=True, max_length=50, choices=[(b'PETROL', b'Petrol'), (b'DIESEL', b'Diesel'), (b'CNG_PETROL', b'CNG / Petrol'), (b'ELECTRICITY', b'Electricity'), (b'LPG_PETROL', b'LPG / Petrol'), (b'EXTERNAL_LPG_CNG', b'External LPG / CNG'), (b'INTERNAL_LPG_CNG', b'Internal LPG / CNG')])),
                ('vehicle_segment', models.CharField(blank=True, max_length=250, choices=[(b'COMPACT_CARS', b'Compact Cars'), (b'HIGH_END_CARS', b'High End Cars'), (b'MIDSIZE_CARS', b'Midsize Cars'), (b'MULTIUTILITY_VEHICLES', b'Multi-utility Vehicles'), (b'SMALL_SIZE_VEHICLES', b'Small Sized Vehicles'), (b'SPORTS_UTILITY_VEHICLES', b'Sports and Utility Vehicles')])),
                ('number_of_wheels', models.IntegerField(null=True, blank=True)),
                ('ex_showroom_price', models.CharField(max_length=20, blank=True)),
                ('base_rate_discount', models.FloatField(null=True, blank=True)),
                ('risk_based_discount', models.FloatField(null=True, blank=True)),
                ('raw_data', models.TextField(null=True, blank=True)),
                ('processing_level', models.CharField(default=b'unprocessed', max_length=20)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('vehicle_type', models.CharField(default=b'Private Car', max_length=25, choices=[(b'Private Car', b'Private Car'), (b'Twowheeler', b'Twowheeler')])),
                ('insurer', models.ForeignKey(blank=True, to='motor_product.Insurer', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='VehicleMapping',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('insurer', models.ForeignKey(blank=True, to='motor_product.Insurer', null=True)),
                ('mapped_vehicle', models.ForeignKey(blank=True, to='motor_product.Vehicle', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='VehicleMaster',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('variant', models.CharField(max_length=250, blank=True)),
                ('cc', models.IntegerField(null=True, blank=True)),
                ('seating_capacity', models.IntegerField(null=True, blank=True)),
                ('ex_showroom_price', models.CharField(max_length=20, blank=True)),
                ('fuel_type', models.CharField(blank=True, max_length=50, choices=[(b'PETROL', b'Petrol'), (b'DIESEL', b'Diesel'), (b'ELECTRICITY', b'Electricity'), (b'INTERNAL_LPG_CNG', b'Internal LPG / CNG')])),
                ('vehicle_segment', models.CharField(blank=True, max_length=250, choices=[(b'COMPACT_CARS', b'Compact Cars'), (b'HIGH_END_CARS', b'High End Cars'), (b'MIDSIZE_CARS', b'Midsize Cars'), (b'MULTIUTILITY_VEHICLES', b'Multi-utility Vehicles'), (b'SMALL_SIZE_VEHICLES', b'Small Sized Vehicles'), (b'SPORTS_UTILITY_VEHICLES', b'Sports and Utility Vehicles')])),
                ('vehicle_type', models.CharField(default=b'Private Car', max_length=25, choices=[(b'Private Car', b'Private Car'), (b'Twowheeler', b'Twowheeler')])),
                ('make', models.ForeignKey(blank=True, to='motor_product.Make', null=True)),
                ('model', models.ForeignKey(blank=True, to='motor_product.Model', null=True)),
                ('sub_vehicles', models.ManyToManyField(to='motor_product.Vehicle', through='motor_product.VehicleMapping')),
            ],
        ),
        migrations.AddField(
            model_name='vehiclemapping',
            name='master_vehicle',
            field=models.ForeignKey(blank=True, to='motor_product.VehicleMaster', null=True),
        ),
        migrations.AddField(
            model_name='transaction',
            name='insured_vehicle',
            field=models.ForeignKey(blank=True, to='motor_product.UserVehicle', null=True),
        ),
        migrations.AddField(
            model_name='transaction',
            name='insurer',
            field=models.ForeignKey(blank=True, to='motor_product.Insurer', null=True),
        ),
        migrations.AddField(
            model_name='transaction',
            name='proposer',
            field=models.ForeignKey(blank=True, to='motor_product.Contact', null=True),
        ),
        migrations.AddField(
            model_name='transaction',
            name='quote',
            field=models.ForeignKey(to='motor_product.Quote'),
        ),
        migrations.AddField(
            model_name='transaction',
            name='receipt',
            field=models.ForeignKey(blank=True, to='motor_product.Receipt', null=True),
        ),
        migrations.AddField(
            model_name='transaction',
            name='tracker',
            field=models.ForeignKey(related_name='motor_transaction', to='core.Tracker'),
        ),
        migrations.AddField(
            model_name='rtomapping',
            name='rto_master',
            field=models.ForeignKey(blank=True, to='motor_product.RTOMaster', null=True),
        ),
        migrations.AddField(
            model_name='rtoexclusion',
            name='rto',
            field=models.ForeignKey(to='motor_product.RTOMaster'),
        ),
        migrations.AddField(
            model_name='quote',
            name='vehicle',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='motor_product.VehicleMaster', null=True),
        ),
        migrations.AddField(
            model_name='lntpolicyresponse',
            name='TxnStatusDetail',
            field=models.ForeignKey(blank=True, to='motor_product.LnTStatusDetail', null=True, db_column='txnstatusdetail_id'),
        ),
        migrations.AddField(
            model_name='inspection',
            name='insurer',
            field=models.ForeignKey(to='motor_product.Insurer', blank=True),
        ),
        migrations.AddField(
            model_name='inspection',
            name='transaction',
            field=models.ForeignKey(to='motor_product.Transaction', blank=True),
        ),
    ]
