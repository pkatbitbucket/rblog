# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0001_initial'),
        ('wiki', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='garage',
            name='city',
            field=models.ForeignKey(to='wiki.City'),
        ),
        migrations.AddField(
            model_name='garage',
            name='insurers',
            field=models.ManyToManyField(to='motor_product.Insurer', blank=True),
        ),
        migrations.AddField(
            model_name='discountloading',
            name='insurer',
            field=models.ForeignKey(to='motor_product.Insurer'),
        ),
        migrations.AddField(
            model_name='discountloading',
            name='rtos',
            field=models.ManyToManyField(to='motor_product.RTOInsurer', blank=True),
        ),
        migrations.AddField(
            model_name='discountcode',
            name='transaction',
            field=models.ForeignKey(blank=True, to='motor_product.Transaction', null=True),
        ),
        migrations.AddField(
            model_name='contact',
            name='address',
            field=models.ForeignKey(blank=True, to='motor_product.Address', null=True),
        ),
        migrations.AddField(
            model_name='bestpricing',
            name='vehicle',
            field=models.ForeignKey(to='motor_product.VehicleMaster'),
        ),
        migrations.AddField(
            model_name='addonexclusion',
            name='addon',
            field=models.ForeignKey(blank=True, to='motor_product.Addon', null=True),
        ),
        migrations.AddField(
            model_name='addonexclusion',
            name='insurer',
            field=models.ForeignKey(to='motor_product.Insurer', blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='vehiclemapping',
            unique_together=set([('master_vehicle', 'insurer')]),
        ),
        migrations.AlterUniqueTogether(
            name='garage',
            unique_together=set([('name', 'pin')]),
        ),
        migrations.AlterUniqueTogether(
            name='discountcode',
            unique_together=set([('code', 'email', 'mobile')]),
        ),
    ]
