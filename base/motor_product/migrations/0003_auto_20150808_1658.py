# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0001_initial'),
        ('motor_product', '0002_auto_20150807_1908'),
    ]

    operations = [
        migrations.CreateModel(
            name='TempGarage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('city', models.CharField(max_length=200)),
                ('address', models.TextField(null=True)),
                ('pin', models.CharField(max_length=6, null=True)),
                ('company', models.CharField(max_length=200)),
                ('make', models.CharField(default=b'Multi Brand', max_length=50, choices=[('Chevrolet', 'Chevrolet'), ('Fiat', 'Fiat'), ('Ford', 'Ford'), ('Honda', 'Honda'), ('Hyundai', 'Hyundai'), ('Mahindra', 'Mahindra'), ('Renault', 'Renault'), ('Mahindra Renault', 'Mahindra Renault'), ('Maruti', 'Maruti'), ('Nissan', 'Nissan'), ('Skoda', 'Skoda'), ('Tata', 'Tata'), ('Toyota', 'Toyota'), ('Volkswagen', 'Volkswagen'), ('BMW', 'BMW'), ('Daewoo', 'Daewoo'), ('Audi', 'Audi'), ('Mitsubishi', 'Mitsubishi'), ('Mercedes', 'Mercedes'), ('Ssangyong', 'Ssangyong'), ('Jaguar', 'Jaguar'), ('Opel', 'Opel'), ('Porsche', 'Porsche'), ('Isuzu', 'Isuzu'), ('Land Rover', 'Land Rover'), ('Accura', 'Accura'), ('Ace', 'Ace'), ('Allwyn', 'Allwyn'), ('Ampere', 'Ampere'), ('Aprilia', 'Aprilia'), ('Bajaj', 'Bajaj'), ('Bmi', 'Bmi'), ('Bsa Motors', 'Bsa Motors'), ('Cagiva', 'Cagiva'), ('Ducati', 'Ducati'), ('Eko', 'Eko'), ('Yo Bykes', 'Yo Bykes'), ('Ennes Engg Pvt Ltd', 'Ennes Engg Pvt Ltd'), ('Hyosung', 'Hyosung'), ('Global', 'Global'), ('Harley Davidson', 'Harley Davidson'), ('Confederate Motors', 'Confederate Motors'), ('Giantco', 'Giantco'), ('Crazy', 'Crazy'), ('Dsk', 'Dsk'), ('Ege', 'Ege'), ('Euro Electric Bikes', 'Euro Electric Bikes'), ('Hero', 'Hero'), ('Hero Honda', 'Hero Honda'), ('Husqvarna', 'Husqvarna'), ('Jawa Motors', 'Jawa Motors'), ('Kanda', 'Kanda'), ('Kawasaki', 'Kawasaki'), ('Kinetic', 'Kinetic'), ('Bajaj Ktm', 'Bajaj Ktm'), ('Lambretta', 'Lambretta'), ('Lml', 'Lml'), ('Pgo', 'Pgo'), ('Peugeot', 'Peugeot'), ('Monto', 'Monto'), ('Mbk', 'Mbk'), ('Moto Guzzi', 'Moto Guzzi'), ('Oreva', 'Oreva'), ('Moped', 'Moped'), ('Piaggio', 'Piaggio'), ('Polaris', 'Polaris'), ('Priya', 'Priya'), ('Rajdoot', 'Rajdoot'), ('Sooraj', 'Sooraj'), ('Power Bird', 'Power Bird'), ('Royal Enfield', 'Royal Enfield'), ('Sureja Auto ', 'Sureja Auto '), ('Scooters India', 'Scooters India'), ('Suzuki', 'Suzuki'), ('Sym', 'Sym'), ('The Moped', 'The Moped'), ('Triumph', 'Triumph'), ('Tvs', 'Tvs'), ('Tvs Suzuki', 'Tvs Suzuki'), ('Vibgyor', 'Vibgyor'), ('Xindayang China', 'Xindayang China'), ('Ultra Motors', 'Ultra Motors'), ('Xing', 'Xing'), ('Yezdi', 'Yezdi'), ('Yamaha', 'Yamaha'), ('Volvo', 'Volvo'), ('Force', 'Force'), ('Fab Motors', 'Fab Motors'), ('Hindustan Motors', 'Hindustan Motors'), ('Premier', 'Premier'), (b'Multi Brand', b'Multi Brand')])),
                ('insurers', models.ManyToManyField(to='motor_product.Insurer', null=True, blank=True)),
                ('wikicity', models.ForeignKey(to='wiki.City', null=True)),
            ],
        ),
        migrations.AddField(
            model_name='garage',
            name='make',
            field=models.CharField(default=b'Multi Brand', max_length=50, choices=[('Chevrolet', 'Chevrolet'), ('Fiat', 'Fiat'), ('Ford', 'Ford'), ('Honda', 'Honda'), ('Hyundai', 'Hyundai'), ('Mahindra', 'Mahindra'), ('Renault', 'Renault'), ('Mahindra Renault', 'Mahindra Renault'), ('Maruti', 'Maruti'), ('Nissan', 'Nissan'), ('Skoda', 'Skoda'), ('Tata', 'Tata'), ('Toyota', 'Toyota'), ('Volkswagen', 'Volkswagen'), ('BMW', 'BMW'), ('Daewoo', 'Daewoo'), ('Audi', 'Audi'), ('Mitsubishi', 'Mitsubishi'), ('Mercedes', 'Mercedes'), ('Ssangyong', 'Ssangyong'), ('Jaguar', 'Jaguar'), ('Opel', 'Opel'), ('Porsche', 'Porsche'), ('Isuzu', 'Isuzu'), ('Land Rover', 'Land Rover'), ('Accura', 'Accura'), ('Ace', 'Ace'), ('Allwyn', 'Allwyn'), ('Ampere', 'Ampere'), ('Aprilia', 'Aprilia'), ('Bajaj', 'Bajaj'), ('Bmi', 'Bmi'), ('Bsa Motors', 'Bsa Motors'), ('Cagiva', 'Cagiva'), ('Ducati', 'Ducati'), ('Eko', 'Eko'), ('Yo Bykes', 'Yo Bykes'), ('Ennes Engg Pvt Ltd', 'Ennes Engg Pvt Ltd'), ('Hyosung', 'Hyosung'), ('Global', 'Global'), ('Harley Davidson', 'Harley Davidson'), ('Confederate Motors', 'Confederate Motors'), ('Giantco', 'Giantco'), ('Crazy', 'Crazy'), ('Dsk', 'Dsk'), ('Ege', 'Ege'), ('Euro Electric Bikes', 'Euro Electric Bikes'), ('Hero', 'Hero'), ('Hero Honda', 'Hero Honda'), ('Husqvarna', 'Husqvarna'), ('Jawa Motors', 'Jawa Motors'), ('Kanda', 'Kanda'), ('Kawasaki', 'Kawasaki'), ('Kinetic', 'Kinetic'), ('Bajaj Ktm', 'Bajaj Ktm'), ('Lambretta', 'Lambretta'), ('Lml', 'Lml'), ('Pgo', 'Pgo'), ('Peugeot', 'Peugeot'), ('Monto', 'Monto'), ('Mbk', 'Mbk'), ('Moto Guzzi', 'Moto Guzzi'), ('Oreva', 'Oreva'), ('Moped', 'Moped'), ('Piaggio', 'Piaggio'), ('Polaris', 'Polaris'), ('Priya', 'Priya'), ('Rajdoot', 'Rajdoot'), ('Sooraj', 'Sooraj'), ('Power Bird', 'Power Bird'), ('Royal Enfield', 'Royal Enfield'), ('Sureja Auto ', 'Sureja Auto '), ('Scooters India', 'Scooters India'), ('Suzuki', 'Suzuki'), ('Sym', 'Sym'), ('The Moped', 'The Moped'), ('Triumph', 'Triumph'), ('Tvs', 'Tvs'), ('Tvs Suzuki', 'Tvs Suzuki'), ('Vibgyor', 'Vibgyor'), ('Xindayang China', 'Xindayang China'), ('Ultra Motors', 'Ultra Motors'), ('Xing', 'Xing'), ('Yezdi', 'Yezdi'), ('Yamaha', 'Yamaha'), ('Volvo', 'Volvo'), ('Force', 'Force'), ('Fab Motors', 'Fab Motors'), ('Hindustan Motors', 'Hindustan Motors'), ('Premier', 'Premier'), (b'Multi Brand', b'Multi Brand')]),
        ),
        migrations.AlterUniqueTogether(
            name='tempgarage',
            unique_together=set([('name', 'pin')]),
        ),
    ]
