# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.core.management import call_command
from django.db import migrations


def load_tmp_garage_data(apps, schema_editor, skip=settings.TESTING):
    if skip:
        return

    call_command('loaddata', 'motor_product', 'tempgaragedata.json')


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0003_auto_20150808_1658'),
    ]

    operations = [
        migrations.RunPython(load_tmp_garage_data, reverse_code=migrations.RunPython.noop)
    ]
