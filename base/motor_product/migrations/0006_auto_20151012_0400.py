# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0005_auto_20150903_1733'),
    ]

    operations = [
        migrations.AddField(
            model_name='inspection',
            name='status',
            field=models.CharField(default=b'PENDING', max_length=10, choices=[(b'PENDING', 'Pending'), (b'PASSED', 'Passed'), (b'FAILED', 'Failed')]),
        ),
        migrations.AlterField(
            model_name='tempgarage',
            name='make',
            field=models.CharField(default=b'Multi Brand', max_length=50, choices=[(b'Multi Brand', b'Multi Brand')]),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='status',
            field=models.CharField(max_length=50, choices=[(b'DRAFT', b'DRAFT'), (b'PENDING', b'Pending'), (b'IN_PROCESS', b'In Process'), (b'COMPLETED', b'Completed'), (b'CANCELLED', b'Cancelled'), (b'FAILED', b'Failed'), (b'MANUAL COMPLETED', b'Manual Completed'), (b'PROPOSAL FAILED', b'Proposal Failed')]),
        ),
    ]
