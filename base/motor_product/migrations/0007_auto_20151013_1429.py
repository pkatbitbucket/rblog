# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0006_auto_20151012_0400'),
    ]

    operations = [
        migrations.CreateModel(
            name='LAndTPendingInspection',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('motor_product.inspection',),
        ),
        migrations.AlterField(
            model_name='inspection',
            name='transaction',
            field=models.OneToOneField(to='motor_product.Transaction'),
        ),
    ]
