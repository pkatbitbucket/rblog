# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coverfox', '0007_auto_20151014_2032'),
        ('motor_product', '0007_auto_20151013_1429'),
    ]

    operations = [
        migrations.CreateModel(
            name='RegistrationDetail',
            fields=[
            ],
            options={
                'abstract': False,
                'proxy': True,
            },
            bases=('coverfox.doodle',),
        ),
    ]
