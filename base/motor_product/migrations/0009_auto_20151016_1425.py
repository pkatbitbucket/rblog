# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def populate_ex_show_room_prices(apps, schema_editor):
    VehicleMaster = apps.get_model('motor_product', 'VehicleMaster')
    for master in VehicleMaster.objects.filter(ex_showroom_price__isnull=False):
        if master.ex_showroom_price:
            master._ex_showroom_price = float(master.ex_showroom_price)
        else:
            master._ex_showroom_price = None


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0008_registrationdetail'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehiclemaster',
            name='_ex_showroom_price',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='vehiclemaster',
            name='cc',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='vehiclemaster',
            name='fuel_type',
            field=models.CharField(max_length=50, choices=[(b'PETROL', b'Petrol'), (b'DIESEL', b'Diesel'), (b'ELECTRICITY', b'Electricity'), (b'INTERNAL_LPG_CNG', b'Internal LPG / CNG')]),
        ),
        migrations.AlterField(
            model_name='vehiclemaster',
            name='make',
            field=models.ForeignKey(default=1, to='motor_product.Make'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='vehiclemaster',
            name='model',
            field=models.ForeignKey(default=1, to='motor_product.Model'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='vehiclemaster',
            name='seating_capacity',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='vehiclemaster',
            name='variant',
            field=models.CharField(max_length=250),
        ),
        migrations.RunPython(
            code=populate_ex_show_room_prices,
            reverse_code=migrations.RunPython.noop,
        ),
        migrations.RemoveField(
            model_name='vehiclemaster',
            name='ex_showroom_price',
        ),
        migrations.RenameField(
            model_name='vehiclemaster',
            old_name='_ex_showroom_price',
            new_name='ex_showroom_price',
        ),
    ]
