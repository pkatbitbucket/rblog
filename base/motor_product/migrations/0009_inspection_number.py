# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0008_registrationdetail'),
    ]

    operations = [
        migrations.AddField(
            model_name='inspection',
            name='number',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
    ]
