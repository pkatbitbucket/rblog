# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0009_inspection_number'),
    ]

    operations = [
        migrations.DeleteModel(
            name='LAndTPendingInspection',
        ),
        migrations.CreateModel(
            name='PendingInspection',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('motor_product.inspection',),
        ),
    ]
