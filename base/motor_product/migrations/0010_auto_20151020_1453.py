# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0009_inspection_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='rsa_policy',
            field=models.FileField(null=True, upload_to=b'motor_rsa_pdfs/cfox', blank=True),
        ),
    ]
