# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0011_merge'),
        ('motor_product', '0010_auto_20151020_1453'),
    ]

    operations = [
    ]
