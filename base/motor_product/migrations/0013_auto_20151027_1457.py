# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_auto_20151027_1424'),
        ('motor_product', '0012_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='Dealer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('company', models.OneToOneField(to='core.Company')),
            ],
        ),
        migrations.CreateModel(
            name='Dealership',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('company', models.OneToOneField(to='core.Company')),
            ],
        ),
        migrations.AddField(
            model_name='dealer',
            name='dealership',
            field=models.ManyToManyField(related_name='dealers', to='motor_product.Dealership', blank=True),
        ),
    ]
