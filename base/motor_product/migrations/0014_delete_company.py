# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0013_auto_20151027_1457'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Company',
        ),
    ]
