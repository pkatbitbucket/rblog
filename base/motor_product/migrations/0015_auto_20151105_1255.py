# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0014_delete_company'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='rsa_policy_number',
            field=models.CharField(max_length=250, null=True, blank=True),
        ),
    ]
