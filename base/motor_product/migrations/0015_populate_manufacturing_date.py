# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime
from django.db import migrations


def populate_manufacturing_date(apps, schema_editor):
    Quote = apps.get_model('motor_product', 'Quote')
    for quote in Quote.objects.filter(date_of_manufacture__isnull=True):
        try:
            quote.date_of_manufacture = datetime.strptime(quote.raw_data['manufacturingDate'], '%d-%m-%Y')
        except KeyError:
            print 'Manufacturing date is not available for quote: {}'.format(quote.quote_id)
        except:
            print 'Unable to format manufacturing date ofr quote: {}'.format(quote.quote_id)
        quote.save()


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0014_delete_company'),
    ]

    operations = [
        migrations.RunPython(populate_manufacturing_date, migrations.RunPython.noop)
    ]
