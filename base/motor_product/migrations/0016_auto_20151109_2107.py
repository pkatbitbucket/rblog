# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields
from django.conf import settings
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0011_medicaltransactionpermissions'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0013_auto_20151109_2107'),
        ('motor_product', '0015_populate_manufacturing_date'),
    ]

    operations = [
        migrations.CreateModel(
            name='DailyTimeSlot',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start', models.TimeField(verbose_name=b'start_time')),
                ('end', models.TimeField(verbose_name=b'end_time')),
                ('is_active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='FieldAgent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('agent_type', models.CharField(default=b'insp', max_length=4, choices=[(b'insp', b'Inspection')])),
                ('company', models.ForeignKey(to='core.Company')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='InspectionAgency',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('company', models.OneToOneField(to='core.Company')),
            ],
        ),
        migrations.CreateModel(
            name='InspectionAgent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('weekly_off', django.contrib.postgres.fields.ArrayField(help_text=b'\n     Weekdays on which the agent will not be working.\n     ', base_field=models.IntegerField(), size=7)),
                ('city', models.ForeignKey(related_name='agents', to='wiki.City')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Leave',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField(help_text=b'Date for which leave is granted', unique=True)),
                ('reason', models.CharField(help_text=b'In case of a public\n            holiday this is the text shown to the customer as reason for non-availability', max_length=255, blank=True)),
                ('agents', models.ManyToManyField(help_text=b'If no agent is entered, assume that all agents are on leave for that day', to='motor_product.InspectionAgent', verbose_name=b'Agents on leave', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='MotorInspectionDocument',
            fields=[
                ('document_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.Document')),
                ('approval_status', models.CharField(default=b'APPROVAL_P', max_length=10, choices=[(b'APPROVAL_P', 'Approval Pending'), (b'APPROVAL_R', 'Approval Rejected'), (b'APPROVED', 'Approved')])),
                ('document_type', models.CharField(max_length=18, choices=[(b'rc-copy', b'RC Copy'), (b'last-policy-copy', b'Last Policy Copy'), (b'engine-imprint', b'Engine Imprint'), (b'chassis-imprint', b'Chassis Imprint'), (b'front', b'Front'), (b'engine', b'Engine'), (b'dashboard', b'Dashboard'), (b'rear', b'Rear'), (b'lf-angle', b'Left Front Angle'), (b'rf-angle', b'Right Front Angle'), (b'lr-angle', b'Left Rear Angle'), (b'rr-angle', b'Right Rear Angle'), (b'odometer', b'Odometer'), (b'floor', b'Floorr'), (b'extra-accessories', b'Extra Accessories'), (b'bifuel-kit', b'Bifuel Kit')])),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='created')),
                ('updated_on', models.DateTimeField(auto_now=True, verbose_name='modified')),
            ],
            options={
                'abstract': False,
            },
            bases=('core.document',),
        ),
        migrations.RemoveField(
            model_name='inspection',
            name='created_on',
        ),
        migrations.RemoveField(
            model_name='inspection',
            name='insurer',
        ),
        migrations.RemoveField(
            model_name='inspection',
            name='updated_on',
        ),
        migrations.AddField(
            model_name='inspection',
            name='requirement',
            field=models.ForeignKey(related_name='requirement_inspections', blank=True, to='core.Requirement', null=True),
        ),
        migrations.AddField(
            model_name='inspection',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4, editable=False),
        ),
        migrations.AddField(
            model_name='rtomaster',
            name='is_active_for_inspection',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='transaction',
            name='requirement',
            field=models.ForeignKey(related_name='requirement_transactions', blank=True, to='core.Requirement', null=True),
        ),
        migrations.AlterField(
            model_name='garage',
            name='make',
            field=models.CharField(default=b'Multi Brand', max_length=50),
        ),
        migrations.AlterField(
            model_name='inspection',
            name='address',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='inspection',
            name='scheduled_on',
            field=models.DateField(blank=True),
        ),
        migrations.AlterField(
            model_name='inspection',
            name='status',
            field=models.CharField(default=b'PENDING', max_length=10, choices=[(b'PENDING', 'Pending'), (b'DONE', 'Inspection Done'), (b'APPROVED', 'Approved'), (b'REJECTED', 'Rejected'), (b'CANCELLED', 'Cancelled')]),
        ),
        migrations.AlterField(
            model_name='inspection',
            name='transaction',
            field=models.ForeignKey(to='motor_product.Transaction'),
        ),
        migrations.AddField(
            model_name='motorinspectiondocument',
            name='inspection',
            field=models.ForeignKey(related_name='inspection_documents', to='motor_product.Inspection'),
        ),
        migrations.AddField(
            model_name='inspection',
            name='agent',
            field=models.ForeignKey(related_name='inspections', blank=True, to='motor_product.InspectionAgent', null=True),
        ),
        migrations.AddField(
            model_name='inspection',
            name='inspection_agency',
            field=models.ForeignKey(to='motor_product.InspectionAgency', null=True),
        ),
        migrations.AddField(
            model_name='inspection',
            name='schedule_slot',
            field=models.ForeignKey(blank=True, to='motor_product.DailyTimeSlot', null=True),
        ),
    ]
