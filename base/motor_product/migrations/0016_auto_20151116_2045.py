# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0015_populate_manufacturing_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='status_history',
            field=django.contrib.postgres.fields.ArrayField(default=(), help_text=b'Comma separated and no spaces', base_field=models.CharField(max_length=50, choices=[(b'DRAFT', b'DRAFT'), (b'PENDING', b'Pending'), (b'IN_PROCESS', b'In Process'), (b'COMPLETED', b'Completed'), (b'CANCELLED', b'Cancelled'), (b'FAILED', b'Failed'), (b'MANUAL COMPLETED', b'Manual Completed'), (b'PROPOSAL FAILED', b'Proposal Failed')]), size=None),
        ),
        migrations.AlterField(
            model_name='garage',
            name='make',
            field=models.CharField(default=b'Multi Brand', max_length=50),
        ),
    ]
