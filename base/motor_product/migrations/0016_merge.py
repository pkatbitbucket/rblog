# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0015_auto_20151105_1255'),
        ('motor_product', '0015_populate_manufacturing_date'),
    ]

    operations = [
    ]
