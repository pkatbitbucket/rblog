# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils import timezone


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0016_auto_20151109_2107'),
    ]

    operations = [
        migrations.AddField(
            model_name='inspection',
            name='created_on',
            field=models.DateTimeField(default=timezone.now(), verbose_name='created', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='inspection',
            name='updated_on',
            field=models.DateTimeField(default=timezone.now(), verbose_name='modified', auto_now=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='motorinspectiondocument',
            name='document_type',
            field=models.CharField(max_length=18, choices=[(b'rc-copy', b'RC Copy'), (b'last-policy-copy', b'Last Policy Copy'), (b'engine-imprint', b'Engine Imprint'), (b'chassis-imprint', b'Chassis Imprint'), (b'front', b'Front'), (b'engine', b'Engine'), (b'dashboard', b'Dashboard'), (b'rear', b'Rear'), (b'lf-angle', b'Left Front Angle'), (b'rf-angle', b'Right Front Angle'), (b'lr-angle', b'Left Rear Angle'), (b'rr-angle', b'Right Rear Angle'), (b'odometer', b'Odometer'), (b'floor', b'Floor'), (b'extra-accessories', b'Extra Accessories'), (b'bifuel-kit', b'Bifuel Kit')]),
        ),
    ]
