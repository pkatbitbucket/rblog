# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0016_auto_20151116_2045'),
        ('motor_product', '0016_merge'),
    ]

    operations = [
    ]
