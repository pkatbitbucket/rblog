# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0017_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rtomaster',
            name='rto_code',
            field=models.CharField(unique=True, max_length=10),
        ),
        migrations.AlterField(
            model_name='rtomaster',
            name='rto_name',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='rtomaster',
            name='rto_state',
            field=models.CharField(max_length=100),
        ),
    ]
