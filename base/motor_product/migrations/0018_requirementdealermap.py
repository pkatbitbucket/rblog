# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0015_auto_20151113_1849'),
        ('motor_product', '0017_auto_20151110_2049'),
    ]

    operations = [
        migrations.CreateModel(
            name='RequirementDealerMap',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dealer', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('requirement', models.ForeignKey(to='core.Requirement')),
            ],
        ),
    ]
