# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0019_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='inspection',
            name='status',
            field=models.CharField(default=b'PENDING', max_length=20, choices=[(b'PENDING', 'Pending'), (b'CANCELLED', 'Document Cancelled'), (b'DOCUMENT_UPLOADED', 'Document Uploaded'), (b'DOCUMENT_APPROVED', 'Document Approved'), (b'DOCUMENT_REJECTED', 'Document Rejected'), (b'PAYMENT_DONE', 'Payment done'), (b'PAYMENT_APPROVED', 'Payment approved'), (b'POLICY_ISSUED', 'Policy Issued')]),
        ),
    ]
