# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0019_merge'),
        ('motor_product', '0017_merge'),
    ]

    operations = [
    ]
