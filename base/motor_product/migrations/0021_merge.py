# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0020_auto_20151120_1154'),
        ('motor_product', '0020_merge'),
    ]

    operations = [
    ]
