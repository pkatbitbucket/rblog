# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime

from django.db import models, migrations
from django.contrib.auth.hashers import make_password

def create_inspection_data(apps, schema_editor):
    Permission = apps.get_model('auth', 'Permission')
    User = apps.get_model('core','User')
    Group = apps.get_model('auth','Group')
    Company = apps.get_model('core','Company')
    City = apps.get_model('wiki', 'City')

    InspectionAgent = apps.get_model('motor_product', 'InspectionAgent') 
    InspectionAgency = apps.get_model('motor_product', 'InspectionAgency')
    DailyTimeSlot = apps.get_model('motor_product', 'DailyTimeSlot')
    
    agent_group, _ = Group.objects.get_or_create(name="INSPECTION_AGENT")
    approver_group, _ = Group.objects.get_or_create(name="INSPECTION_APPROVER")
    
    agent_group.permissions.add(
        Permission.objects.get_or_create(codename='change_pendinginspection')[0]
    )
    approver_group.permissions.add(
        *Permission.objects.filter(codename__in=[
            'add_inspection',
            'add_inspectionagency',
            'add_inspectionagent',
            'add_landtpendinginspection',
            'add_pendinginspection',
            'change_inspection',
            'change_inspectionagency',
            'change_inspectionagent',
            'change_landtpendinginspection',
            'change_pendinginspection',
            'delete_inspection',
            'delete_inspectionagency',
            'delete_inspectionagent',
            'delete_landtpendinginspection',
            'delete_pendinginspection',
            'add_leave',
            'change_leave',
            'delete_leave'
        ])
    )
    agents = [
            ['akram', 'Mumbai'],
            ['dhaval', 'Thane'],
            ['sachin', 'Mumbai'],
            ['younus', 'Pune'],
            ['ashpak', 'Pune'],
            ['anwar', 'Hyderabad'],
            ['imran', 'Hyderabad'],
            ['ashok', 'Bangalore'],
            ['mani', 'Bangalore'],
            ['prem', 'Delhi'],
            ['prabhakar', 'Delhi'],
            ['vijay', 'Delhi'],
            ['mainpal', 'Gurgaon'],
            ['atul', 'Noida'],
            ]

    approver, _ = User.objects.get_or_create(
            email='inspection_approver@coverfox.com',
            username='inspection_approver',
            defaults={
                'first_name':'Inspection',
                'last_name':'Approver',
                'is_staff':True,
                'password': make_password('asd'),
            })
    approver.groups.add(approver_group)

    agent_list = []
    for name, location in agents:
        u, _ = User.objects.get_or_create(
            email="inspection_%s@coverfox.com" % name,
            username='inspection_%s' % name,
            defaults={
                'first_name':name.title(),
                'last_name':"",
                'password': make_password(name[::-1]),
            })
        u.groups.add(agent_group)
        ins_agent,_ = InspectionAgent.objects.get_or_create(
                user=u,
                defaults={
                    'weekly_off':[],
                    'city':City.objects.get(name=location),
                })

        agent_list.append(u)

    for ins_company in ['Coverfox', 'AAA']:
        company, _ = Company.objects.get_or_create(
            name=ins_company,
            slug=ins_company.lower(),
            defaults= {'address':''},
        )
        ins_agency, _ = InspectionAgency.objects.get_or_create(company=company)

    slots = [
            (datetime.time(9, 0), datetime.time(12, 0)),
            (datetime.time(12, 0), datetime.time(15, 0)),
            (datetime.time(15, 0), datetime.time(18, 0)),
            ]
    for start, end in slots:
        sl = DailyTimeSlot.objects.get_or_create(
            start=start,
            end=end
            )

class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        ('core', '0017_auto_20151201_0049'),
        ('wiki', '0016_merge'),
        ('motor_product', '0023_merge'),
    ]
    
    operations = [
        migrations.RunPython(create_inspection_data, migrations.RunPython.noop)
    ]
