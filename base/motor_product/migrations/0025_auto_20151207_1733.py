# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import autoslug.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0016_merge'),
        ('motor_product', '0024_auto_20151202_1712'),
    ]

    operations = [
        migrations.CreateModel(
            name='InspectionArea',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('slug', autoslug.fields.AutoSlugField(editable=False, populate_from=b'name', always_update=True, unique=True)),
                ('is_enabled', models.BooleanField(default=True)),
                ('city', models.ForeignKey(related_name='inspection_areas', blank=True, to='wiki.City', null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='inspectionreport',
            name='inspection',
        ),
        # migrations.RemoveField(
        #     model_name='inspectionreport',
        #     name='partname',
        # ),
        migrations.RemoveField(
            model_name='inspectionagent',
            name='city',
        ),
        migrations.DeleteModel(
            name='InspectionReport',
        ),
        migrations.DeleteModel(
            name='MotorPart',
        ),
        migrations.AddField(
            model_name='inspectionagent',
            name='area',
            field=models.ManyToManyField(to='motor_product.InspectionArea'),
        ),
    ]
