# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0024_inspection_initialize_data'),
    ]

    operations = [
        migrations.AlterField(
            model_name='addonexclusion',
            name='age_limit',
            field=models.FloatField(null=True, blank=True),
        ),
    ]
