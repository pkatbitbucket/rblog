# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0024_inspection_initialize_data'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='registration_detail',
            field=models.ForeignKey(to='motor_product.RegistrationDetail', null=True),
        ),
    ]
