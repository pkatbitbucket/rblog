# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models

import re


def attaching_registration_number_to_motor_transactions(apps, schema_editor):
    Transaction = apps.get_model("motor_product", "Transaction")
    RegistrationDetail = apps.get_model("motor_product", "RegistrationDetail")
    for transaction in Transaction.objects.filter(registration_detail__isnull=True).iterator():
        registration_number = transaction.raw.get('user_form_details', {}).get('vehicle_reg_no')
        if registration_number:
            registration_number = ''.join(filter(bool, re.findall('[a-z0-9]+', registration_number.lower())))
            detail, _ = RegistrationDetail.objects.get_or_create(name=registration_number)
            transaction.registration_detail = detail
            transaction.save()
            print transaction.transaction_id, 'attached with', registration_number, ['GET', 'CREATE'][_]


def setting_fastlane_key(apps, schema_editor):
    RegistrationDetail = apps.get_model("motor_product", "RegistrationDetail")
    for registration_detail in RegistrationDetail.objects.iterator():
        if registration_detail.data:
            registration_detail.data = {'fastlane': registration_detail.data}
            registration_detail.save()
            print 'Saved', registration_detail.name


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0025_transaction_registration_detail'),
    ]

    operations = [
        migrations.RunPython(attaching_registration_number_to_motor_transactions, migrations.RunPython.noop),
        migrations.RunPython(setting_fastlane_key, migrations.RunPython.noop),
    ]
