# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0025_auto_20151208_1439'),
    ]

    operations = [
        migrations.AddField(
            model_name='dealer',
            name='account_name',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='dealer',
            name='account_number',
            field=models.CharField(max_length=30, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='dealer',
            name='commission_percentage',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='dealer',
            name='ifsc_code',
            field=models.CharField(max_length=25, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='dealer',
            name='is_pre_deducted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='dealer',
            name='payout_frequency',
            field=models.CharField(default=b'Monthly', max_length=9),
        ),
        migrations.AddField(
            model_name='dealership',
            name='account_name',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='dealership',
            name='account_number',
            field=models.CharField(max_length=30, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='dealership',
            name='commission_percentage',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='dealership',
            name='ifsc_code',
            field=models.CharField(max_length=25, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='dealership',
            name='is_pre_deducted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='dealership',
            name='payout_frequency',
            field=models.CharField(default=b'Monthly', max_length=9),
        ),
        migrations.AddField(
            model_name='requirementdealermap',
            name='commission_value',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='requirementdealermap',
            name='raw',
            field=jsonfield.fields.JSONField(default={}, verbose_name='raw', blank=True),
        ),
    ]
