# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0016_merge'),
        ('motor_product', '0025_auto_20151207_1733'),
    ]

    operations = [
        migrations.AddField(
            model_name='inspection',
            name='inspection_city',
            field=models.ForeignKey(related_name='city_inspection', blank=True, to='wiki.City', null=True),
        ),
    ]
