# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0025_auto_20151208_1439'),
    ]

    operations = [
        migrations.AddField(
            model_name='quote',
            name='type',
            field=models.CharField(default=b'online', null=True, max_length=20, verbose_name=b'quote type', choices=[(b'offline', b'Offline'), (b'online', b'Online')]),
        ),
    ]
