# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0026_auto_20151216_1354'),
    ]

    operations = [
        migrations.CreateModel(
            name='DealerDealershipMap',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('account_number', models.CharField(max_length=30, null=True, blank=True)),
                ('account_name', models.CharField(max_length=100, null=True, blank=True)),
                ('ifsc_code', models.CharField(max_length=25, null=True, blank=True)),
                ('commission_percentage', models.FloatField(default=0.0)),
                ('payout_frequency', models.CharField(default=b'Monthly', max_length=9)),
                ('is_pre_deducted', models.BooleanField(default=False)),
                ('dealer', models.ForeignKey(to='motor_product.Dealer')),
                ('dealership', models.ForeignKey(to='motor_product.Dealership')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='dealer',
            name='dealership1',
            field=models.ManyToManyField(related_name='dealers1', through='motor_product.DealerDealershipMap', to='motor_product.Dealership', blank=True),
        ),
    ]
