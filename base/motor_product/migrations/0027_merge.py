# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0026_inspection_inspection_city'),
        ('motor_product', '0024_inspection_initialize_data'),
    ]

    operations = [
    ]
