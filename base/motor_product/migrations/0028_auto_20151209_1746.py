# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import autoslug.fields


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0027_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='InspectionReport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('feedback', models.TextField(max_length=200, null=True, blank=True)),
                ('status', models.CharField(default=b'SAFE', max_length=200, choices=[(b'SAFE', 'Safe'), (b'NOT_FITTED', 'Not fitted'), (b'SCRATCH', 'Scratch'), (b'DENTED', 'Dented'), (b'BROKEN', 'Broken'), (b'RUSTED', 'Rusted'), (b'DEFORM', 'Deform')])),
                ('inspection', models.ForeignKey(to='motor_product.Inspection')),
            ],
        ),
        migrations.CreateModel(
            name='MotorPart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('part_name', models.CharField(max_length=200)),
                ('slug', autoslug.fields.AutoSlugField(populate_from=b'part_name', editable=False)),
            ],
        ),
        migrations.AddField(
            model_name='inspectionreport',
            name='part',
            field=models.ForeignKey(to='motor_product.MotorPart', null=True),
        ),
        migrations.AlterUniqueTogether(
            name='inspectionreport',
            unique_together=set([('part', 'inspection')]),
        ),
    ]
