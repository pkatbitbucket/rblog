# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def fix_dealership_data(apps, schema_editor):
    DealerDealershipMap = apps.get_model('motor_product', 'DealerDealershipMap')
    Dealer = apps.get_model('motor_product', 'Dealer')
    for dealer in Dealer.objects.all():
        for dealership in dealer.dealership.all():
            DealerDealershipMap.objects.get_or_create(dealer=dealer, dealership=dealership)


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0027_auto_20151222_1626'),
    ]

    operations = [
        migrations.RunPython(fix_dealership_data, migrations.RunPython.noop),
        migrations.RemoveField(
            model_name='dealer',
            name='dealership',
        ),
        migrations.RemoveField(
            model_name='dealer',
            name='dealership1',
        ),
        migrations.AddField(
            model_name='dealer',
            name='dealership',
            field=models.ManyToManyField(related_name='dealers', through='motor_product.DealerDealershipMap', to='motor_product.Dealership', blank=True),
        ),
    ]
