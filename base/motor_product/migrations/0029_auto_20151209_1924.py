# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0028_auto_20151209_1746'),
    ]

    operations = [
        migrations.RenameField(
            model_name='inspection',
            old_name='inspection_city',
            new_name='city',
        ),
        migrations.AddField(
            model_name='inspection',
            name='area',
            field=models.ForeignKey(related_name='area_inspection', blank=True, to='motor_product.InspectionArea', null=True),
        ),
    ]
