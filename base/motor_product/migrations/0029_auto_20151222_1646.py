# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0028_custom_dealership_through_implementation'),
    ]

    operations = [
        migrations.AddField(
            model_name='dealer',
            name='is_approved',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='dealerdealershipmap',
            name='is_approved',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='dealership',
            name='is_approved',
            field=models.BooleanField(default=False),
        ),
    ]
