# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


def fix_dealer_dealership_data(apps, schema_editor):
    RequirementDealerMap = apps.get_model('motor_product', 'RequirementDealerMap')
    DealerDealershipMap = apps.get_model('motor_product', 'DealerDealershipMap')
    Dealer = apps.get_model('motor_product', 'Dealer')
    Employee = apps.get_model('core', 'Employee')
    delete_rdm = []
    for rdm in RequirementDealerMap.objects.all():
        try:
            dealer_instance = Dealer.objects.filter(
                company__in=Employee.objects.filter(user=rdm.dealer).values('company'))[0]
        except:
            delete_rdm.append(rdm.id)
            continue
        for dealership in dealer_instance.dealership.all():
            dealer_dealership_instance, _ = DealerDealershipMap.objects.get_or_create(
                dealer=dealer_instance, dealership=dealership)
            rdm.dealer = dealer_dealership_instance.id
        rdm.save()
    RequirementDealerMap.objects.filter(id__in=delete_rdm).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0029_auto_20151222_1646'),
    ]

    operations = [
        migrations.CreateModel(
            name='DealerInvoice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('raw', jsonfield.fields.JSONField(default={}, verbose_name='raw', blank=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('cleared_on', models.DateTimeField(null=True, blank=True)),
                ('dealership', models.ForeignKey(to='motor_product.Dealership')),
            ],
        ),
        migrations.AlterField(
            model_name='dealerdealershipmap',
            name='dealership',
            field=models.ForeignKey(related_name='dealership_dealers', to='motor_product.Dealership'),
        ),
        migrations.AlterField(
            model_name='requirementdealermap',
            name='dealer',
            field=models.IntegerField(),
        ),
        migrations.RunPython(fix_dealer_dealership_data, migrations.RunPython.noop),
        migrations.AlterField(
            model_name='requirementdealermap',
            name='dealer',
            field=models.ForeignKey(to='motor_product.DealerDealershipMap'),
        ),
        migrations.AddField(
            model_name='dealerinvoice',
            name='requirements',
            field=models.ManyToManyField(to='motor_product.RequirementDealerMap', null=True, blank=True),
        ),
    ]
