# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0030_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='motorpart',
            name='is_accessory',
            field=models.BooleanField(default=False),
        ),
    ]
