# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0031_motorpart_is_accessory'),
        ('motor_product', '0027_auto_20151219_1948'),
    ]

    operations = [
    ]
