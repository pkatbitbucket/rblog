# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0025_auto_20151223_2256'),
        ('motor_product', '0031_motorpart_is_accessory'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='policy',
            field=models.OneToOneField(related_name='motor_transaction', null=True, blank=True, to='core.Policy'),
        ),
    ]
