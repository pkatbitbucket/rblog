# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def update_null_to_default(apps, schema_editor):
    Quote = apps.get_model('motor_product', 'Quote')
    Quote.objects.filter(type__isnull=True).update(type='online')


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0032_merge'),
    ]

    operations = [
        migrations.RunPython(update_null_to_default, reverse_code=migrations.RunPython.noop),
        migrations.AlterField(
            model_name='quote',
            name='type',
            field=models.CharField(default=b'online', max_length=20, verbose_name=b'quote type', choices=[(b'offline', b'Offline'), (b'online', b'Online')]),
        ),
    ]
