# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0033_populate_policies'),
        ('motor_product', '0033_auto_20151226_2243'),
    ]

    operations = [
    ]
