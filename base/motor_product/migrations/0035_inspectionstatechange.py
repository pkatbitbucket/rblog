# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('motor_product', '0034_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='InspectionStateChange',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('state', models.CharField(max_length=20, choices=[(b'PENDING', 'Pending'), (b'CANCELLED', 'Document Cancelled'), (b'DOCUMENT_UPLOADED', 'Document Uploaded'), (b'DOCUMENT_APPROVED', 'Document Approved'), (b'DOCUMENT_REJECTED', 'Document Rejected'), (b'PAYMENT_DONE', 'Payment done'), (b'PAYMENT_APPROVED', 'Payment approved'), (b'POLICY_ISSUED', 'Policy Issued')])),
                ('changed_on', models.DateTimeField(auto_now_add=True)),
                ('done_by', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('inspection', models.ForeignKey(to='motor_product.Inspection')),
            ],
        ),
    ]
