# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0034_merge'),
        ('motor_product', '0030_auto_20151224_1737'),
    ]

    operations = [
    ]
