# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0035_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dealer',
            name='payout_frequency',
            field=models.CharField(default=b'Monthly', max_length=9, choices=[(b'Weekly', b'Weekly'), (b'Bi Weekly', b'Bi Weekly'), (b'Monthly', b'Monthly')]),
        ),
        migrations.AlterField(
            model_name='dealerdealershipmap',
            name='payout_frequency',
            field=models.CharField(default=b'Monthly', max_length=9, choices=[(b'Weekly', b'Weekly'), (b'Bi Weekly', b'Bi Weekly'), (b'Monthly', b'Monthly')]),
        ),
        migrations.AlterField(
            model_name='dealership',
            name='payout_frequency',
            field=models.CharField(default=b'Monthly', max_length=9, choices=[(b'Weekly', b'Weekly'), (b'Bi Weekly', b'Bi Weekly'), (b'Monthly', b'Monthly')]),
        ),
    ]
