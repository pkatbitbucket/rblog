# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0036_auto_20160111_1654'),
        ('motor_product', '0035_inspectionstatechange'),
    ]

    operations = [
    ]
