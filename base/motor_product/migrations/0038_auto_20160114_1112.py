# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0037_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dealerinvoice',
            name='requirements',
            field=models.ManyToManyField(to='motor_product.RequirementDealerMap', blank=True),
        ),
    ]
