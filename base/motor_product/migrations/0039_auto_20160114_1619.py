# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0038_auto_20160114_1112'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='proposal_amount',
            field=models.CharField(max_length=10, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='transaction',
            name='proposal_on',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='transaction',
            name='proposal_request',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='transaction',
            name='proposal_response',
            field=models.TextField(null=True, blank=True),
        ),
    ]
