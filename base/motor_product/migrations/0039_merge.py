# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0026_attaching_registration_number_to_motor_transactions'),
        ('motor_product', '0038_auto_20160114_1112'),
    ]

    operations = [
    ]
