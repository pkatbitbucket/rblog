# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0039_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='InspectionCallback',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('call_time', models.DateTimeField(verbose_name='inspection callback')),
            ],
        ),
        migrations.AddField(
            model_name='inspectionstatechange',
            name='comment',
            field=models.TextField(max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='inspectionstatechange',
            name='reason',
            field=models.CharField(max_length=200, null=True, choices=[(b'NOT_CONTACTABLE', 'Customer not contactable'), (b'CUSTOMER_CANCELLED', 'Customer cancelled the inspection'), (b'NOT_AVAILABLE', 'Customer not available'), (b'AGENT_LATE', 'Agent reached late on site')]),
        ),
        migrations.AlterField(
            model_name='inspection',
            name='status',
            field=models.CharField(default=b'PENDING', max_length=20, choices=[(b'PENDING', 'Pending'), (b'CANCELLED', 'Cancelled'), (b'DOCUMENT_UPLOADED', 'Document Uploaded'), (b'DOCUMENT_APPROVED', 'Document Approved'), (b'DOCUMENT_REJECTED', 'Document Rejected'), (b'PAYMENT_DONE', 'Payment done'), (b'PAYMENT_APPROVED', 'Payment approved'), (b'POLICY_ISSUED', 'Policy Issued'), (b'RESCHEDULED', 'Rescheduled')]),
        ),
        migrations.AlterField(
            model_name='inspectionstatechange',
            name='state',
            field=models.CharField(max_length=20, choices=[(b'PENDING', 'Pending'), (b'CANCELLED', 'Cancelled'), (b'DOCUMENT_UPLOADED', 'Document Uploaded'), (b'DOCUMENT_APPROVED', 'Document Approved'), (b'DOCUMENT_REJECTED', 'Document Rejected'), (b'PAYMENT_DONE', 'Payment done'), (b'PAYMENT_APPROVED', 'Payment approved'), (b'POLICY_ISSUED', 'Policy Issued'), (b'RESCHEDULED', 'Rescheduled')]),
        ),
        migrations.AddField(
            model_name='inspectioncallback',
            name='inspection',
            field=models.ForeignKey(to='motor_product.Inspection'),
        ),
    ]
