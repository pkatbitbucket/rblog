# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django_pgjson.fields
import jsonfield.fields
from django.conf import settings
import django.core.validators


def create_new_registration_detail_objects(apps, schema_editor):
    RegistrationDetail = apps.get_model('motor_product', 'RegistrationDetail')
    NewRegistrationDetail = apps.get_model('motor_product', 'NewRegistrationDetail')
    registration_details = RegistrationDetail.objects.all()

    new_objects, count = [], 0

    for registration_detail in registration_details.iterator():

        if count % 1000 == 0:
            NewRegistrationDetail.objects.bulk_create(new_objects)
            new_objects, count = [], 0

        data = {}

        if 'user_input' in registration_detail.data.keys():
            data['coverfox'] = registration_detail.data.get('user_input', {})

        if 'fastlane' in registration_detail.data.keys():
            data['fastlane'] = registration_detail.data.get('fastlane', {})
            if 'fastlane' in data['fastlane'].keys():
                data['fastlane'] = data['fastlane']['fastlane']

        new_objects.append(
            NewRegistrationDetail(
                registration_number=registration_detail.name,
                data=data
            )
        )

        count += 1

    if new_objects:
        NewRegistrationDetail.objects.bulk_create(new_objects)


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0038_merge'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('motor_product', '0041_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewRegistrationDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('registration_number', models.CharField(unique=True, max_length=32, verbose_name='Registration Number', db_index=True)),
                ('data', django_pgjson.fields.JsonField(default={}, verbose_name='Data')),
                ('vehicle_master', models.ForeignKey(blank=True, to='motor_product.VehicleMaster', null=True)),
            ],
            options={
                'db_table': 'motor_product_registrationdetail',
            },
        ),
        migrations.CreateModel(
            name='RegistrationDetailStatistics',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', jsonfield.fields.JSONField(default={}, verbose_name='Data', blank=True)),
                ('ip', models.GenericIPAddressField(db_index=True)),
                ('fastlane_status', models.CharField(db_index=True, max_length=64, null=True, verbose_name='Fastlane Status', choices=[(b'SUCCESSFUL', b'Successfully Fetched'), (b'ERROR_RECORD_NOT_FOUND', b'Record not Found'), (b'ERROR_REQUEST_TIMED_OUT', b'Request Timed Out'), (b'ERROR_INVALID_RESPONSE', b'Invalid Response')])),
                ('user_status', models.CharField(default=b'UNKNOWN', max_length=64, verbose_name='User Status', db_index=True, choices=[(b'ACCEPTED', b'Accpted'), (b'CHANGED', b'Changed'), (b'UNKNOWN', b'Unknown')])),
                ('number_of_matches', models.IntegerField(db_index=True, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(4)])),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='modified')),
                ('registration_detail', models.ForeignKey(to='motor_product.NewRegistrationDetail', null=True)),
                ('tracker', models.ForeignKey(to='core.Tracker', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
                ('vehicle_master', models.ForeignKey(blank=True, to='motor_product.VehicleMaster', null=True)),
            ],
        ),
        migrations.RunPython(
            code=create_new_registration_detail_objects,
            reverse_code=migrations.RunPython.noop,
        ),
    ]
