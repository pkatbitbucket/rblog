# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0042_newregistrationdetail_registrationdetailstatistics'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registrationdetailstatistics',
            name='fastlane_status',
            field=models.CharField(db_index=True, max_length=64, null=True, verbose_name='Fastlane Status', choices=[(b'SUCCESSFUL', b'Successfully Fetched'), (b'ERROR_RECORD_NOT_FOUND', b'Record not Found'), (b'ERROR_REQUEST_TIMED_OUT', b'Request Timed Out'), (b'ERROR_INVALID_RESPONSE', b'Invalid Response'), (b'REJECTED', b'Request Rejected')]),
        ),
    ]
