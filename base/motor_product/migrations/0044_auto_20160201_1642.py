# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django_extensions.db.fields
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0043_auto_20160130_1517'),
    ]

    operations = [
        migrations.AlterField(
            model_name='inspection',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4, db_index=True, editable=False),
        ),
        migrations.AlterField(
            model_name='quote',
            name='quote_id',
            field=django_extensions.db.fields.UUIDField(unique=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='transaction_id',
            field=django_extensions.db.fields.UUIDField(unique=True, editable=False, blank=True),
        ),
    ]
