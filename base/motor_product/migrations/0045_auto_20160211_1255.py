# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0044_auto_20160201_1642'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='make',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='model',
            options={'ordering': ['name']},
        ),
    ]
