# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0044_auto_20160201_1642'),
    ]

    operations = [
        migrations.CreateModel(
            name='MotorPremium',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('response_data', jsonfield.fields.JSONField(default={}, verbose_name='request_data')),
                ('bucket', models.CharField(max_length=1024)),
                ('policy_type', models.CharField(max_length=12, choices=[(b'NEW_VEHICLE', b'New Vehicle'), (b'ROLLOVER', b'Roll Over'), (b'RENEWAL', b'Renewal'), (b'EXPIRED', b'Expired')])),
                ('updated_on', models.DateTimeField(default=django.utils.timezone.now)),
                ('max_idv', models.CharField(default=b'0', max_length=10)),
                ('min_idv', models.CharField(default=b'0', max_length=10)),
                ('premium', models.CharField(default=b'0', max_length=10)),
                ('previous_ncb', models.CharField(default=b'0', max_length=4)),
                ('insurer', models.ForeignKey(related_name='insurer_premium', to='motor_product.Insurer')),
                ('vehicle', models.ForeignKey(related_name='vehicle_premium', to='motor_product.VehicleMaster')),
                ('zone', models.ForeignKey(related_name='rto_premium', to='motor_product.RTOMaster')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='motorpremium',
            unique_together=set([('vehicle', 'insurer', 'zone', 'bucket', 'policy_type', 'previous_ncb')]),
        ),
    ]
