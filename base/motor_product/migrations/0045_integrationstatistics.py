# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0044_auto_20160201_1642'),
    ]

    operations = [
        migrations.CreateModel(
            name='IntegrationStatistics',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='created')),
                ('error_code', models.CharField(max_length=50)),
                ('status', models.BooleanField(default=False)),
                ('required_time', models.FloatField(max_length=10)),
                ('vehicle_type', models.CharField(default=b'Private Car', max_length=25, choices=[(b'Private Car', b'Private Car'), (b'Twowheeler', b'Twowheeler')])),
                ('case', models.CharField(max_length=20)),
                ('insurer', models.ForeignKey(blank=True, to='motor_product.Insurer', null=True)),
                ('master_vehicle', models.ForeignKey(blank=True, to='motor_product.VehicleMaster', null=True)),
                ('quote', models.ForeignKey(to='motor_product.Quote')),
                ('rto_master', models.ForeignKey(blank=True, to='motor_product.RTOMaster', null=True)),
            ],
        ),
    ]
