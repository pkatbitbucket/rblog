# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0046_merge'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='make',
            options={'ordering': ('name',)},
        ),
        migrations.AlterModelOptions(
            name='model',
            options={'ordering': ('name',)},
        ),
        migrations.AlterField(
            model_name='make',
            name='name',
            field=models.CharField(unique=True, max_length=70, verbose_name='name'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='status',
            field=models.CharField(max_length=50, choices=[(b'DRAFT', b'DRAFT'), (b'PENDING', b'Pending'), (b'IN_PROCESS', b'In Process'), (b'COMPLETED', b'Completed'), (b'CANCELLED', b'Cancelled'), (b'FAILED', b'Failed'), (b'MANUAL COMPLETED', b'Manual Completed'), (b'PROPOSAL FAILED', b'Proposal Failed'), (b'FORM_EXPIRED', b'Form Expired')]),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='status_history',
            field=django.contrib.postgres.fields.ArrayField(default=[], help_text=b'Comma separated and no spaces', base_field=models.CharField(max_length=50, choices=[(b'DRAFT', b'DRAFT'), (b'PENDING', b'Pending'), (b'IN_PROCESS', b'In Process'), (b'COMPLETED', b'Completed'), (b'CANCELLED', b'Cancelled'), (b'FAILED', b'Failed'), (b'MANUAL COMPLETED', b'Manual Completed'), (b'PROPOSAL FAILED', b'Proposal Failed'), (b'FORM_EXPIRED', b'Form Expired')]), size=None),
        ),
        migrations.AlterUniqueTogether(
            name='model',
            unique_together=set([('make', 'name', 'vehicle_type')]),
        ),
    ]
