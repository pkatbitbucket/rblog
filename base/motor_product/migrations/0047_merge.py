# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0045_integrationstatistics'),
        ('motor_product', '0046_merge'),
    ]

    operations = [
    ]
