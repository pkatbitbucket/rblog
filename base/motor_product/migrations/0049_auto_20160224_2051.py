# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0048_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registrationdetailstatistics',
            name='user_status',
            field=models.CharField(default=b'ACCEPTED', max_length=64, verbose_name='User Status', db_index=True, choices=[(b'ACCEPTED', b'Accpted'), (b'CHANGED', b'Changed'), (b'UNKNOWN', b'Unknown')]),
        ),
    ]
