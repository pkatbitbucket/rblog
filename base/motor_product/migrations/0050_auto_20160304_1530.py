# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0049_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='motorpremium',
            name='max_age',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='motorpremium',
            name='min_age',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterUniqueTogether(
            name='motorpremium',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='motorpremium',
            name='bucket',
        ),
    ]
