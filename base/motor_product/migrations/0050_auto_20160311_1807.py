# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0049_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='insurer',
            name='payment_gateway',
            field=models.CharField(default=b'default', max_length=32, verbose_name='Payment Gateway', choices=[(b'billdesk', b'BillDesk'), (b'techprocess', b'TechProcess'), (b'default', b'Default')]),
        ),
        migrations.AddField(
            model_name='transaction',
            name='payment_gateway',
            field=models.CharField(blank=True, max_length=32, null=True, verbose_name='Payment Gateway', choices=[(b'billdesk', b'BillDesk'), (b'techprocess', b'TechProcess'), (b'default', b'Default')]),
        ),
    ]
