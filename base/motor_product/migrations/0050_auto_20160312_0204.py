# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0049_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='motorinspectiondocument',
            name='document_type',
            field=models.CharField(max_length=18, choices=[(b'rc-copy', b'RC Copy'), (b'last-policy-copy', b'Last Policy Copy'), (b'engine-imprint', b'Engine Imprint'), (b'chassis-imprint', b'Chassis Imprint'), (b'front', b'Front'), (b'engine', b'Engine'), (b'dashboard', b'Dashboard'), (b'rear', b'Rear'), (b'lf-angle', b'Left Front Angle'), (b'rf-angle', b'Right Front Angle'), (b'lr-angle', b'Left Rear Angle'), (b'rr-angle', b'Right Rear Angle'), (b'odometer', b'Odometer'), (b'floor', b'Floor'), (b'extra-accessories', b'Extra Accessories'), (b'bifuel-kit', b'Bifuel Kit'), (b'form-29', b'Form 29'), (b'form-30', b'Form 30'), (b'inspection-report', b'Inspection Report')]),
        ),
    ]
