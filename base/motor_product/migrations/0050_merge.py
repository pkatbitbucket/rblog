# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0049_auto_20160224_2051'),
        ('motor_product', '0049_merge'),
    ]

    operations = [
    ]
