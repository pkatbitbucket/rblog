# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def activate_required_insurers(apps, schema_editor):
    Make = apps.get_model('motor_product', 'Make')

    allowed_makes_for_twowheeler = ['Aprilia', 'Bajaj', 'Bajaj Ktm', 'BMW', 'Ducati',
        'Harley Davidson', 'Hero', 'Hero Honda', 'Honda', 'Hyosung', 'Kawasaki',
        'Kinetic', 'Lml', 'Mahindra', 'Piaggio', 'Rajdoot', 'Royal Enfield',
        'Suzuki', 'Triumph', 'Tvs', 'Tvs Suzuki', 'Yamaha', 'Yezdi', 'Yo Bykes']
    for make in Make.objects.exclude(name__in=allowed_makes_for_twowheeler):
        make.is_active_for_twowheeler = False
        make.save()


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0050_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='make',
            name='is_active_for_fourwheeler',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='make',
            name='is_active_for_twowheeler',
            field=models.BooleanField(default=True),
        ),
        migrations.RunPython(
            code=activate_required_insurers,
            reverse_code=migrations.RunPython.noop,
        )
    ]
