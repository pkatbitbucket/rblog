from django.db import migrations
from django.conf import settings
import putils
from motor_product.scripts.garages import garages
from django.core.management import call_command
from django.db import transaction


def populate_garage(apps, schema_editor):
    GARAGE_MASTER_PATH = putils.get_file_from_s3(settings.INTEGRATION_AWS_ACCESS_KEY_ID,
                                                 settings.INTEGRATION_AWS_SECRET_ACCESS_KEY,
                                                 settings.INTEGRATION_BUCKET_NAME,
                                                 'motor_product/scripts/garages/data/new_garage_list.csv')
    DELIMETER = '|'
    Garage = apps.get_model('motor_product', 'Garage')
    with transaction.atomic():
        # Deleting old data from database
        Garage.objects.all().delete()
        # Deleting old data from Elastic search
        if settings.PRODUCTION:
            garages.delete_data()
        # Populate database
        call_command('load_garages', GARAGE_MASTER_PATH, 'options')
        # Elastic Search Populate
        if settings.PRODUCTION:
            garages.save_data(GARAGE_MASTER_PATH, DELIMETER)


def delete_garage(apps, schema_editor):
    GARAGE_OLD_PATH = putils.get_file_from_s3(settings.INTEGRATION_AWS_ACCESS_KEY_ID,
                                              settings.INTEGRATION_AWS_SECRET_ACCESS_KEY,
                                              settings.INTEGRATION_BUCKET_NAME,
                                              'motor_product/scripts/garages/data/garage_list_150316.csv')
    DELIMETER = '|'
    Garage = apps.get_model('motor_product', 'Garage')
    with transaction.atomic():
        # Unmigrating Database
        Garage.objects.all().delete()
        # Unmigrating Elastic Search
        if settings.PRODUCTION:
            garages.delete_data()
        # Populate database
        call_command('load_garages', GARAGE_OLD_PATH, 'options')
        # Elastic Search Populate
        if settings.PRODUCTION:
            garages.save_data(GARAGE_OLD_PATH, DELIMETER)


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0050_auto_20160312_0204'),
    ]

    operations = [
        migrations.RunPython(
            code=populate_garage,
            reverse_code=delete_garage
        ),
    ]
