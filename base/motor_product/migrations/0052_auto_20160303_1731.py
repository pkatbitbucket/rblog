# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def set_popular_makes_and_models(apps, schema_editor):
    Make = apps.get_model('motor_product', 'Make')
    Model = apps.get_model('motor_product', 'Model')

    popular_mapping = {
        'Honda': ['Activa', 'Shine', 'Unicorn', 'Dio', 'Cb Shine', 'Cb Unicorn'],
        'Bajaj': ['Pulsar 150 Dtsi', 'Discover 125', 'Discover 100', 'Avenger 220', 'Pulsar 150', 'Platina'],
        'Hero': ['Passion Pro', 'Maestro', 'Splendor Plus', 'Pleasure', 'Splendor Pro', 'Glamour'],
        'Hero Honda': ['Passion Pro', 'Splendor Plus', 'Passion Plus', 'Cbz Xtreme', 'Glamour', 'Cd Deluxe'],
        'Tvs': ['Wego', 'Apache Rtr 160', 'Jupiter', 'Scooty Pep', 'Apache Rtr 180', 'Star City'],
        'Yamaha': ['Fzs', 'Fz16', 'Ray', 'Fazer', 'Sz-R', 'R15']
    }

    Make.objects.filter(name__in=popular_mapping.keys()).update(is_popular=True)

    for make, models in popular_mapping.iteritems():
        Model.objects.filter(name__in=models).update(is_popular=True)

class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0051_auto_20160301_0241'),
    ]

    operations = [
        migrations.AddField(
            model_name='make',
            name='is_popular',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='model',
            name='is_popular',
            field=models.BooleanField(default=False),
        ),
        migrations.RunPython(
            code=set_popular_makes_and_models,
            reverse_code=migrations.RunPython.noop,
        )
    ]
