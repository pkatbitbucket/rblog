# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0071_company_employee_discount'),
        ('motor_product', '0053_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='CorporateEmployee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('employee_code', models.CharField(max_length=20)),
                ('employee_name', models.CharField(max_length=50)),
                ('company', models.ForeignKey(related_name='corporate_employee', to='core.Company')),
            ],
        ),
    ]
