from django.db import migrations
from django.conf import settings
import putils
from django.core.management import call_command
from django.db import transaction


def populate_vehiclemaster(apps, schema_editor):
    VEHICLE_MASTER_PATH = putils.get_file_from_s3(settings.INTEGRATION_AWS_ACCESS_KEY_ID,
                                                  settings.INTEGRATION_AWS_SECRET_ACCESS_KEY,
                                                  settings.INTEGRATION_BUCKET_NAME,
                                                  'motor_product/scripts/fourwheeler/data/new_master_fourwheeler.csv')

    with transaction.atomic():
        call_command('load_vehicle_master', VEHICLE_MASTER_PATH, 'options')


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0053_merge'),
    ]

    operations = [
        migrations.RunPython(
            code=populate_vehiclemaster,
            reverse_code=migrations.RunPython.noop
        ),
    ]
