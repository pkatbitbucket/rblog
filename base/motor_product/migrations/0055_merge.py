# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0054_vehiclemaster_upload'),
        ('motor_product', '0051_garage_file_upload'),
    ]

    operations = [
    ]
