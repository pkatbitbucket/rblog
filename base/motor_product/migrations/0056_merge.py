# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0055_merge'),
        ('motor_product', '0051_merge'),
    ]

    operations = [
    ]
