# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0050_auto_20160304_1530'),
        ('motor_product', '0056_merge'),
    ]

    operations = [
    ]
