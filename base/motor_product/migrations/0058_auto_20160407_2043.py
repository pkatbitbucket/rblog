# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def delete_motor_premium(apps, schema):
    MotorPremium = apps.get_model('motor_product', 'MotorPremium')
    MotorPremium.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0057_merge'),
    ]

    operations = [
        migrations.RunPython(
            code=delete_motor_premium,
            reverse_code=migrations.RunPython.noop),
        migrations.RemoveField(
            model_name='motorpremium',
            name='previous_ncb',
        ),
        migrations.AddField(
            model_name='motorpremium',
            name='new_ncb',
            field=models.CharField(default=b'', max_length=4),
        )
    ]
