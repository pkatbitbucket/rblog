# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0058_merge'),
        ('motor_product', '0058_auto_20160407_2043'),
    ]

    operations = [
    ]
