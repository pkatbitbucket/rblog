# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0059_merge'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='transaction',
            options={'permissions': ('can_view_ops_sales', 'Can view ops sales related activities')},
        ),
    ]
