import datetime
import logging
import math
import os
import copy
import random
import urlparse

import utils
import putils

from copy import deepcopy
from fuzzywuzzy import fuzz

import re
import uuid

from autoslug import AutoSlugField

from dateutil import relativedelta

from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.core import exceptions
from django.core import validators
from django.core.cache import caches
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.core.urlresolvers import reverse
from django.db import models
from django.db import transaction
from django.utils import timezone
from django.utils.functional import lazy
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import UUIDField

from django.contrib.postgres import fields as postgres_fields

from jsonfield import JSONField
from django_pgjson import fields as django_pgjson_fields

from account_manager.signals.signals import create_user_account, policy_updated
from core import models as core_models
from core import activity
from core.users import User
from coverfox import models as coverfox_models

from wiki import models as wiki_models

from customdb.thumbs import ImageWithThumbsField
from motor_product.exceptions import NoMatch
from core import activity as core_activity

from payment.models import PayoutBase

cache = caches['motor-results']
logger = logging.getLogger(__name__)
format_vehicle_type = lambda x: 'fourwheeler' if x == 'Private Car' else 'twowheeler'
inverse_format_vehicle_type = lambda x: 'Private Car' if x == 'fourwheeler' else 'Twowheeler'
QUOTE_CHANNEL_CACHE_TIME = 3600


def get_document_upload_path(self, filename):
    return self.get_document_upload_path(filename)


def get_file_path(instance, filename):
    now = datetime.datetime.now()
    fname, ext = os.path.splitext(filename)
    newfilename = "pw-%s.%s" % (now.strftime("%I%M%S"), ext)
    path_to_save = "uploads/motor/insurer/%s" % (newfilename)
    return path_to_save


def get_policy_path(instance, filename):
    path_to_save = "policy/%s" % (filename)
    return path_to_save

PAYMENT_GATEWAY_CHOICES = (
    ('billdesk', 'BillDesk'),
    ('techprocess', 'TechProcess'),
    ('default', 'Default'),
)

VEHICLE_TYPE_CHOICES = (
    ('Private Car', 'Private Car'),
    ('Twowheeler', 'Twowheeler'),
)


class Insurer(models.Model):
    """Insurer model."""
    STATUS_CHOICES = (
        (1, _('Draft')),
        (2, _('Public')),
    )
    title = models.CharField(_('title'), max_length=200)
    slug = models.SlugField(_('slug'), max_length=70)
    body = models.TextField(_('body'), blank=True)
    tease = models.TextField(_('tease'), blank=True, help_text=_(
        'Concise text suggested. Does not appear in RSS feed.'))
    logo = ImageWithThumbsField(upload_to=get_file_path, sizes=(
        ('s', 300, 300), ('t', 500, 500)), null=True, blank=True)
    status = models.IntegerField(
        _('status'), choices=STATUS_CHOICES, default=1)
    is_active_for_car = models.BooleanField(default=False)
    disable_cache_for_car = models.BooleanField(default=True)
    disable_cache_logic_car = models.BooleanField(default=True)
    is_active_for_twowheeler = models.BooleanField(default=False)
    disable_cache_for_twowheeler = models.BooleanField(default=True)
    disable_cache_logic_twowheeler = models.BooleanField(default=True)
    payment_gateway = models.CharField(
        _('Payment Gateway'),
        max_length=32,
        blank=False,
        null=False,
        choices=PAYMENT_GATEWAY_CHOICES,
        default='default')
    created_on = models.DateTimeField(_('created'), auto_now_add=True)
    updated_on = models.DateTimeField(_('modified'), auto_now=True)

    class Meta:
        verbose_name = _('insurer')
        verbose_name_plural = _('insurers')
        ordering = ('-created_on',)
        get_latest_by = 'created_on'

    def __unicode__(self):
        return u'%s' % self.title

    def is_active(self, vehicle_type):
        if vehicle_type == 'Private Car':
            return self.is_active_for_car
        elif vehicle_type == 'Twowheeler':
            return self.is_active_for_twowheeler
        else:
            return False

    def disable_cache(self, vehicle_type):
        if vehicle_type == 'Private Car':
            return self.disable_cache_for_car
        elif vehicle_type == 'Twowheeler':
            return self.disable_cache_for_twowheeler
        else:
            return True


class Make(models.Model):
    name = models.CharField(_('name'), max_length=70, unique=True)
    is_active_for_twowheeler = models.BooleanField(default=True)
    is_active_for_fourwheeler = models.BooleanField(default=True)
    is_popular = models.BooleanField(default=False)

    class Meta:
        ordering = ('name',)

    def __unicode__(self):
        return self.name


class Model(models.Model):
    make = models.ForeignKey(Make, blank=True, null=True)
    name = models.CharField(_('name'), max_length=150)
    vehicle_type = models.CharField(
        max_length=25, choices=VEHICLE_TYPE_CHOICES, default='Private Car'
    )
    is_popular = models.BooleanField(default=False)

    class Meta:
        unique_together = ('make', 'name', 'vehicle_type')
        ordering = ('name',)

    def __unicode__(self):
        return u'%s : %s' % (self.make.name, self.name)


class Vehicle(models.Model):
    FUEL_CHOICES = (
        ('PETROL', 'Petrol'),
        ('DIESEL', 'Diesel'),
        ('CNG_PETROL', 'CNG / Petrol'),
        ('ELECTRICITY', 'Electricity'),
        ('LPG_PETROL', 'LPG / Petrol'),
        ('EXTERNAL_LPG_CNG', 'External LPG / CNG'),
        ('INTERNAL_LPG_CNG', 'Internal LPG / CNG'),
    )
    VEHICLE_SEGMENT = (
        ('COMPACT_CARS', 'Compact Cars'),
        ('HIGH_END_CARS', 'High End Cars'),
        ('MIDSIZE_CARS', 'Midsize Cars'),
        ('MULTIUTILITY_VEHICLES', 'Multi-utility Vehicles'),
        ('SMALL_SIZE_VEHICLES', 'Small Sized Vehicles'),
        ('SPORTS_UTILITY_VEHICLES', 'Sports and Utility Vehicles')
    )
    insurer = models.ForeignKey(Insurer, blank=True, null=True)
    vehicle_code = models.CharField(max_length=250, blank=True)
    name = models.CharField(max_length=250, blank=True)
    make = models.CharField(max_length=250, blank=True)
    make_code = models.CharField(max_length=50, blank=True)
    model = models.CharField(max_length=250, blank=True)
    model_code = models.CharField(max_length=50, blank=True)
    variant = models.CharField(max_length=250, blank=True)
    variant_code = models.CharField(max_length=50, blank=True)
    cc = models.IntegerField(blank=True, null=True)
    seating_capacity = models.IntegerField(blank=True, null=True)
    fuel_type = models.CharField(
        max_length=50, blank=True, choices=FUEL_CHOICES)
    vehicle_segment = models.CharField(
        max_length=250, blank=True, choices=VEHICLE_SEGMENT)
    number_of_wheels = models.IntegerField(blank=True, null=True)
    ex_showroom_price = models.CharField(max_length=20, blank=True)
    base_rate_discount = models.FloatField(blank=True, null=True)
    risk_based_discount = models.FloatField(blank=True, null=True)
    raw_data = models.TextField(blank=True, null=True)
    processing_level = models.CharField(
        max_length=20, null=False, default='unprocessed')
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    vehicle_type = models.CharField(
        max_length=25, choices=VEHICLE_TYPE_CHOICES, default='Private Car')

    def __unicode__(self):
        return "%s %s %s %s %s - %s" % (self.make, self.model, self.variant, self.fuel_type, self.cc, self.insurer.title)


class VehicleMasterManager(models.Manager):

    def get_by_natural_key(self, make, model, variant, cc, fuel_type, seating_capacity):
        return self.get(make__name=make, model__name=model, variant=variant, cc=cc, fuel_type=fuel_type, seating_capacity=seating_capacity)


class VehicleMaster(models.Model):
    objects = VehicleMasterManager()

    FUEL_CHOICES = (
        ('PETROL', 'Petrol'),
        ('DIESEL', 'Diesel'),
        ('ELECTRICITY', 'Electricity'),
        ('INTERNAL_LPG_CNG', 'Internal LPG / CNG'),
    )
    VEHICLE_SEGMENT = (
        ('COMPACT_CARS', 'Compact Cars'),
        ('HIGH_END_CARS', 'High End Cars'),
        ('MIDSIZE_CARS', 'Midsize Cars'),
        ('MULTIUTILITY_VEHICLES', 'Multi-utility Vehicles'),
        ('SMALL_SIZE_VEHICLES', 'Small Sized Vehicles'),
        ('SPORTS_UTILITY_VEHICLES', 'Sports and Utility Vehicles')
    )
    make = models.ForeignKey(Make)  # maruti
    model = models.ForeignKey(Model)  # wagon r
    variant = models.CharField(max_length=250)  # LSI
    cc = models.IntegerField()
    seating_capacity = models.IntegerField()
    ex_showroom_price = models.FloatField(blank=True, null=True)
    fuel_type = models.CharField(
        max_length=50, choices=FUEL_CHOICES)
    vehicle_segment = models.CharField(
        max_length=250, blank=True, choices=VEHICLE_SEGMENT)
    sub_vehicles = models.ManyToManyField(Vehicle, through='VehicleMapping')
    vehicle_type = models.CharField(
        max_length=25, choices=VEHICLE_TYPE_CHOICES, default='Private Car')

    def __unicode__(self):
        return "%s %s %s" % (self.make.name, self.model.name, self.variant)

    def natural_key(self):
        return (
            self.make.name, self.model.name, self.variant, self.cc,
            self.fuel_type, self.seating_capacity
        )

    def fastlane_key(self):
        return " ".join(
            [
                self.make.name,
                self.model.name,
                self.variant,
                self.fuel_type
            ]
        )


class VehicleMapping(models.Model):
    master_vehicle = models.ForeignKey(VehicleMaster, blank=True, null=True)
    mapped_vehicle = models.ForeignKey(Vehicle, blank=True, null=True)
    insurer = models.ForeignKey(Insurer, blank=True, null=True)

    class Meta:
        unique_together = ('master_vehicle', 'insurer')

    def save(self, *args, **kwargs):
        self.insurer = self.mapped_vehicle.insurer
        super(VehicleMapping, self).save(*args, **kwargs)


class QuoteManager(models.Manager):

    def create_copy_quote(self, quote, **kwargs):
        return self.create(
            is_new=kwargs.get('is_new', quote.is_new),
            vehicle=kwargs.get('vehicle', quote.vehicle),
            date_of_manufacture=kwargs.get('date_of_manufacture', quote.date_of_manufacture),
            raw_data=kwargs.get('raw_data', quote.raw_data),
            is_processed=kwargs.get('is_processed', quote.is_processed),
            type=kwargs.get('type', quote.type)
        )

    def create_dummy_quote(self, **kwargs):
        today = datetime.date.today()
        data = {
            "isNewVehicle": "1",
            "isUsedVehicle": "0",
            "registrationDate": datetime.date.today().strftime("%d-%m-%Y"),
            "expiry_error": "false",
            "idv": "0",
            "previousNCB": "0",
            "newNCB": "0",
            "isNCBCertificate": "0",
            "isClaimedLastYear": "0",
            "isCNGFitted": "0",
            "cngKitValue": "0",
            "voluntaryDeductible": "0",
            "idvElectrical": "0",
            "idvNonElectrical": "0",
            "addon_isDepreciationWaiver": "0",
            "addon_isKeyReplacement": "0",
            "addon_isInvoiceCover": "0",
            "addon_isNcbProtection": "0",
            "addon_is247RoadsideAssistance": "0",
            "addon_isEngineProtector": "0",
            "addon_isDriveThroughProtected": "0",
            "extra_paPassenger": "0",
            "extra_isLegalLiability": "0",
            "extra_isAntiTheftFitted": "0",
            "extra_isMemberOfAutoAssociation": "0",
            "extra_isTPPDDiscount": "0",
            "extra_user_dob": "",
            "quoteId": "",
            "discountCode": "",
            "expirySlot": "7",
            "vehicleId": "1265",
            "registrationNumber[]": ["MH", "02", "0", "0"],
            'payment_mode': 'PAYMENT_GATEWAY',
        }

        if kwargs:
            data.update(kwargs)

        data["manufacturingDate"] = datetime.datetime.strptime(data["registrationDate"], "%d-%m-%Y").replace(
            day=1).date().strftime("%d-%m-%Y")
        data["newPolicyStartDate"] = today.strftime("%d-%m-%Y")
        pastPolicyExpiryDate = today.replace(month=today.month + 1, day=1)
        if pastPolicyExpiryDate < today:
            pastPolicyExpiryDate = pastPolicyExpiryDate.replace(year=pastPolicyExpiryDate.year + 1)
        data["pastPolicyExpiryDate"] = pastPolicyExpiryDate.strftime("%d-%m-%Y")

        return self.create(is_new=True if data['isNewVehicle'] == '1' else False,
                           vehicle=VehicleMaster.objects.get(id=data['vehicleId']),
                           raw_data=data)

    def get_queryset(self):
        return super(QuoteManager, self).get_queryset().exclude(type=self.model.OFFLINE)

    def create_quote_from_request_dict(self, request_dict):
        vehicle = VehicleMaster.objects.get(id=request_dict['vehicleId'])
        is_new = True if request_dict['isNewVehicle'] == '1' else False
        return self.create(is_new=is_new, raw_data=request_dict, vehicle=vehicle)


class Quote(models.Model):
    OFFLINE = 'offline'
    ONLINE = 'online'
    type_choices = (
        (OFFLINE, 'Offline'),
        (ONLINE, 'Online'),
    )

    quote_id = UUIDField(unique=True)
    vehicle = models.ForeignKey(
        VehicleMaster, blank=True, null=True, on_delete=models.SET_NULL)
    is_new = models.BooleanField(default=True)
    date_of_manufacture = models.DateField(blank=True, null=True)
    raw_data = JSONField(_('request_data'), blank=True, default={})
    is_processed = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)
    type = models.CharField('quote type', max_length=20, default=ONLINE, choices=type_choices)

    objects = QuoteManager()

    @property
    def is_dep(self):
        return self.raw_data.get('addon_isDepreciationWaiver', '0') == '1'

    @property
    def policy_type(self):
        if self.is_new:
            return 'new'
        if self.raw_data.get('isUsedVehicle') in [1, '1']:
            return "used"

        past_policy_expiry_date = datetime.datetime.strptime(self.raw_data['pastPolicyExpiryDate'], '%d-%m-%Y').date()
        if past_policy_expiry_date >= self.created_on.date():
            return 'renew'
        return 'expired'

    @property
    def new_policy_start_date(self):
        if self.policy_type == 'new':
            new_policy_start_date = self.raw_data['newPolicyStartDate']
            new_policy_start_date = datetime.datetime.strptime(new_policy_start_date, '%d-%m-%Y')
        elif self.policy_type == 'renew':
            past_policy_expiry_date = self.raw_data['pastPolicyExpiryDate']
            past_policy_expiry_date = datetime.datetime.strptime(past_policy_expiry_date, '%d-%m-%Y')
            new_policy_start_date = (past_policy_expiry_date + datetime.timedelta(days=1))
        else:
            new_policy_start_date = None

        return new_policy_start_date

    def get_absolute_url(self):
        if self.vehicle.vehicle_type == 'Private Car':
            return reverse("quote_share_fourwheeler", kwargs={'quote_id': self.quote_id})
        else:
            return reverse("quote_share_twowheeler", kwargs={'quote_id': self.quote_id})

    @property
    def rto(self):
        rto = None
        if self.raw_data.get('registrationNumber[]'):
            rto = '-'.join(self.raw_data['registrationNumber[]'][0:2])
        return rto

    @property
    def rto_info(self):
        info = {}
        if self.rto:
            rto_master = RTOMaster.objects.get(rto_code=self.rto)
            info['rto_code'] = rto_master.rto_code
            info['rto_name'] = rto_master.rto_name
            info['rto_state'] = rto_master.rto_state
        return info

    url = property(get_absolute_url)

    def save(self, *args, **kwargs):
        try:
            self.date_of_manufacture = datetime.datetime.strptime(self.raw_data['manufacturingDate'], '%d-%m-%Y')
        except KeyError:
            utils.log_error(logger, msg='Manufacturing date is not available for quote: {}'.format(self.quote_id))
        except:
            utils.log_error(logger, msg='Unable to format manufacturing date for quote: {}'.format(self.quote_id))
        if 'vehicleId' not in self.raw_data:
            self.raw_data['vehicleId'] = self.vehicle.id
        super(Quote, self).save(*args, **kwargs)

    def policy_expired(self):
        try:
            return datetime.datetime.strptime(self.raw_data['pastPolicyExpiryDate'], '%d-%m-%Y') < datetime.datetime.strptime(datetime.date.today().strftime("%d-%m-%Y"), '%d-%m-%Y')
        except (KeyError, ValueError, ):
            return False

    def policy_registration_date(self):
        try:
            return datetime.datetime.strptime(self.raw_data['registrationDate'], '%d-%m-%Y')
        except (KeyError, ValueError, ):
            raise

    @property
    def is_fastlane_success(self):
        if self.raw_data.get('fastlane_success', "0") in [1, '1']:
            return True
        return False

    @property
    def is_fastlane_flow(self):
        if self.raw_data.get('rds_id') not in ['', 'false', False, None]:
            return True
        return False

    @property
    def accepted_fastlane(self):
        rds_id = self.raw_data.get('rds_id')
        if rds_id and rds_id.isdigit():
            rds = RegistrationDetailStatistics.objects.filter(id=rds_id).last()
            if rds and rds.is_accepted:
                return True
        return False

    def quote_cache_key(self, insurer_slug):
        return '%s_%s' % (self.quote_id, insurer_slug)

    def put_channel(self, data, insurer_slug):
        cache_key = self.quote_cache_key(insurer_slug)
        cache.set(cache_key, data, QUOTE_CHANNEL_CACHE_TIME)

    def get_channel(self, insurer_slug):
        cache_key = self.quote_cache_key(insurer_slug)
        result = cache.get(cache_key)
        if result and result.get('data', None):
            return result
        return


class Address(models.Model):
    ADDRESS_CATEGORY_CHOICES = (
        ('URBAN', 'Urban'),
        ('RURAL', 'Rural'),
    )
    category = models.CharField(
        max_length=10, choices=ADDRESS_CATEGORY_CHOICES)
    line_1 = models.CharField(_("Street"), max_length=100)
    line_2 = models.CharField(_("Street"), max_length=100, blank=True)
    landmark = models.TextField(_("Landmark"), blank=True)
    city = models.CharField(_("City"), max_length=100)
    district = models.CharField(_("District"), max_length=100, default='')
    state = models.CharField(_("State"), max_length=100)
    country = models.CharField(_("Country"), max_length=100)
    pincode = models.CharField(_("Pincode"), max_length=10)
    raw = JSONField(_('raw'), blank=True, default={})


class Contact(models.Model):
    ADDRESS_TYPE_CHOICES = (
        ('COMMUNICATION_ADDRESS', 'Communication Address'),
        ('PERMANENT_ADDRESS', 'Permanent Address'),
        ('BILLING_ADDRESS', 'Billing address'),
        ('RISK_ADDRESS', 'Risk address'),
        ('REGISTRATION_OFFICE', 'Registered office'),
        ('NEW_ADDRESS', 'New address'),
        ('OTHER_OFFICES', 'Other offices'),
    )
    GENDER_CHOICES = (
        ('MALE', 'Male'),
        ('FEMALE', 'Female'),
    )
    TITLE_CHOICES = (
        ('MR.', 'Mr.'),
        ('MRS.', 'Mrs.'),
        ('MISS', 'Miss'),
        ('DR', 'Dr'),
        ('CAPTAIN', 'Captain'),
        ('LT', 'Lt'),
        ('MAJOR', 'Major'),
        ('GENERAL', 'General'),
        ('COLONEL', 'Colonel'),
        ('BRIGADIER', 'Brigadier'),
        ('JUDGE', 'Judge'),
        ('PROF', 'Prof'),
        ('SIR', 'Sir'),
        ('FATHER', 'Father'),
        ('MASTER', 'Master'),
        ('MADAM', 'Madam'),
    )
    address = models.ForeignKey(Address, blank=True, null=True)
    address_type = models.CharField(
        max_length=30, choices=ADDRESS_TYPE_CHOICES)
    email = models.EmailField(_("Email"))
    mobile = models.CharField(_("Mobile"), max_length=11)
    landline = models.CharField(_("Landline"), max_length=12)
    date_of_birth = models.DateField(blank=True, null=True)
    title = models.CharField(max_length=10, choices=TITLE_CHOICES)
    first_name = models.CharField(_("First name"), max_length=255,)
    middle_name = models.CharField(_("First name"), max_length=255, blank=True)
    last_name = models.CharField(_("Last name"), max_length=255)
    gender = models.CharField(
        _("Gender"), max_length=20, choices=GENDER_CHOICES)
    father_first_name = models.CharField(
        _("Father's first name"), max_length=255)
    father_last_name = models.CharField(
        _("Father's last name"), max_length=255, blank=True)
    send_email = models.BooleanField(default=False)
    send_sms = models.BooleanField(default=False)
    phone_call = models.BooleanField(default=False)
    nationality = models.CharField(max_length=50)
    raw = JSONField(_('raw'), blank=True, default={})

# ### Unwanted model ###


class PrivateCar(models.Model):
    raw = JSONField(_('raw'), blank=True, default={})

# ### Unwanted model ###


class UserVehicle(models.Model):
    raw = JSONField(_('raw'), blank=True, default={})


class Receipt(models.Model):
    raw = JSONField(_('raw'), blank=True, default={})


class RegistrationDetail(coverfox_models.Doodle):
    FUEL_TYPE_MAP = {
        'PETROL': 'PETROL',
        'PETROL PETROL': 'PETROL',

        'DIESEL': 'DIESEL',
        'DIESEL DIESEL': 'DIESEL',
        'DEISEL': 'DIESEL',

        'LPG ONLY': 'INTERNAL_LPG_CNG',
        'LPG': 'INTERNAL_LPG_CNG',
        'PETLPG': 'INTERNAL_LPG_CNG',
        'PETROL/LPG': 'INTERNAL_LPG_CNG',
        'PETROL/CNG': 'INTERNAL_LPG_CNG',
        'PETCNG': 'INTERNAL_LPG_CNG',
        'RCNG/P': 'INTERNAL_LPG_CNG',
    }

    @staticmethod
    def for_number(number):
        "why is this not on objects? because Doodle class needs objects"
        return RegistrationDetail.objects.get(
            name__iexact=number
        )

    @property
    def fastlane_raw(self):
        if "fastlane" not in self.data:
            self.data["fastlane"] = utils.fastlane(self.name)
            self.save()
        # TODO Check why is there 2 level hierarchy for fastlane key.
        if 'fastlane' in self.data['fastlane']:
            self.data['fastlane'] = self.data['fastlane']['fastlane']
            self.save()
        return self.data["fastlane"]

    @property
    def fastlane(self):
        """ this is cleaned data """
        fastlane_data = self.fastlane_raw

        if "result" not in fastlane_data.get("response", {}):
            return fastlane_data

        result = fastlane_data['response']['result']

        if type(result) == list:
            """
For one car I got two responses: AP 09 CL 3279
"result": [
    {
        "hypth": {
            "fla_fncr_name": "HDFC BANK LTD.",
            "fncr_name": "HDFC BANK LTD",
            "from_dt": "6/7/2012",
            "hp_type": "1 "
        },
        "vehicle": {
            "body_type_desc": "S    ",
            "c_city": "HYDERABAD",
            "c_pincode": "0",
            "chasi_no": "MA6BFBE2CCT021990",
            "color": "SUPER RED",
            "eng_no": "B12D110AJVZ121090055",
            "fla_fuel_type_desc": "PETROL",
            "fla_maker_desc": "CHEVROLET",
            "fla_model_desc": "BEAT",
            "fla_variant": "LT",
            "fla_vh_class_desc": "LMV",
            "fuel_type_desc": "PETROL",
            "maker_desc": "GENERAL MOTOR INDIA PVT LTD",
            "maker_model": "CHEVROLET BEAT LT BS IV",
            "manu_yr": "2012",
            "owner_sr": "0",
            "p_city": "HYDERABAD",
            "p_pincode": "0",
            "regn_dt": "6/7/2012",
            "regn_no": "AP09CL3279",
            "regn_type": "N",
            "regn_type_desc": "NEW",
            "rto_cd": "009",
            "state_cd": "TS",
            "vh_class_desc": "MOTOR CAR"
        }
    },
    {
        "hypth": {
            "fla_fncr_name": "HDFC BANK LTD.",
            "fncr_name": "HDFC BANK LTD",
            "from_dt": "6/7/2012",
            "hp_type": "1 "
        },
        "vehicle": {
            "body_type_desc": "Sedan",
            "c_city": "HYDERABAD",
            "c_pincode": "500020",
            "chasi_no": "MA6BFBE2CCT021990",
            "color": "SUPER RED",
            "eng_no": "B12D110AJVZ121090055",
            "fla_fuel_type_desc": "PETROL",
            "fla_maker_desc": "CHEVROLET",
            "fla_model_desc": "BEAT",
            "fla_variant": "LT",
            "fla_vh_class_desc": "LMV",
            "fuel_type_desc": "PETROL",
            "maker_desc": "GENERAL MOTOR INDIA PVT LTD",
            "maker_model": "CHEVROLET BEAT LT BS IV",
            "manu_yr": "2012",
            "owner_sr": "0",
            "p_city": "HYDERABAD",
            "p_pincode": "500020",
            "regn_dt": "6/7/2012",
            "regn_no": "AP09CL3279",
            "regn_type": "N",
            "regn_type_desc": "NEW",
            "rto_cd": "009",
            "state_cd": "TS",
            "vh_class_desc": "MOTOR CAR"
        }
    }
],

Second one is more accurate, body_type_desc is "Sedan" vs "S   " in first.
Not sure what is the general pattern, for now using the last value.
            """
            # we are creating a clone so we do not accidentlly save this data
            fastlane_data = deepcopy(fastlane_data)
            result = deepcopy(result[-1])
            fastlane_data['response']['result'] = result

            # NOTE: we do not call save here, else we will lose data sent by
            # fastlane, and tomorrow we may change the algo

        return fastlane_data

    @property
    def user_input(self):
        return self.data.get("user_input") or {}

    def clean(self):
        self.name = ''.join(filter(bool, re.findall('[a-z0-9]+', self.name.lower())))

    def fastlane2vehicle_master(self, raise_exception=True):
        import prod_utils
        if "result" not in self.fastlane["response"]:
            if raise_exception:
                raise NoMatch
            else:
                return None
        vehicle_details = self.fastlane["response"]["result"]['vehicle']
        make, fuel_type, model, variant = (
            vehicle_details.get('fla_maker_desc', "").strip(),
            vehicle_details.get('fla_fuel_type_desc', "").strip(),
            vehicle_details.get('fla_model_desc', "").strip(),
            vehicle_details.get('fla_variant', "").strip(),
        )
        if not make or not fuel_type or not model or not variant:
            if raise_exception:
                raise NoMatch
            else:
                return "skip"

        fastlane_key = " ".join(
            vehicle_details.get(k, "").strip().upper()
            for k in (
                "fla_maker_desc",
                "fla_model_desc",
                "fla_variant",
                "maker_model"
            )
        )

        fastlane_key += " " + " ".join(
            RegistrationDetail.FUEL_TYPE_MAP.get(
                vehicle_details.get(k, "").strip().upper(), ""
            )
            for k in (
                "fla_fuel_type_desc",
                "fuel_type_desc",
            )
        )

        return prod_utils.fastlane2vehicle_master(
            make, model, variant, fuel_type,
            fastlane_key=fastlane_key,
            raise_exception=raise_exception
        )

    def fastlane_natural_key(self, raise_exception=True):
        """ this is purely for debugging, not used anywhere """
        if "result" not in self.fastlane["response"]:
            if raise_exception:
                raise NoMatch
            else:
                return None
        vehicle_details = self.fastlane["response"]["result"]['vehicle']
        return (
            vehicle_details.get('fla_maker_desc', "").strip(),
            vehicle_details.get('fla_fuel_type_desc', "").strip(),
            vehicle_details.get('fla_model_desc', "").strip(),
            vehicle_details.get('fla_variant', "").strip(),
        )

    class Meta(coverfox_models.Doodle.Meta):
        proxy = True


class NewRegistrationDetail(models.Model):
    FUEL_TYPE_MAP = {
        'PETROL': 'PETROL',
        'PETROL PETROL': 'PETROL',

        'DIESEL': 'DIESEL',
        'DIESEL DIESEL': 'DIESEL',
        'DEISEL': 'DIESEL',

        'LPG ONLY': 'INTERNAL_LPG_CNG',
        'LPG': 'INTERNAL_LPG_CNG',
        'PETLPG': 'INTERNAL_LPG_CNG',
        'PETROL/LPG': 'INTERNAL_LPG_CNG',
        'PETROL/CNG': 'INTERNAL_LPG_CNG',
        'PETCNG': 'INTERNAL_LPG_CNG',
        'RCNG/P': 'INTERNAL_LPG_CNG',
    }
    registration_number = models.CharField(_("Registration Number"), max_length=32, unique=True, db_index=True)
    vehicle_master = models.ForeignKey(VehicleMaster, null=True, blank=True)
    data = django_pgjson_fields.JsonField(_('Data'), default={})

    SOURCES = ['coverfox', 'fastlane']

    def __unicode__(self):
        return self.registration_number + (self.vehicle_master if self.vehicle_master else '')

    def clean(self):
        import prod_utils
        self.registration_number = prod_utils.clean_registration_number(self.registration_number)

    @property
    def coverfox(self):
        return self.data.get("coverfox") or {}

    @property
    def fastlane_raw(self):
        if "fastlane" not in self.data:
            self.data["fastlane"] = utils.fastlane(self.registration_number)
            self.save()
        # TODO Check why is there 2 level hierarchy for fastlane key.
        if 'fastlane' in self.data['fastlane']:
            self.data['fastlane'] = self.data['fastlane']['fastlane']
            self.save()
        return self.data["fastlane"]

    @property
    def fastlane(self):
        """ this is cleaned data """
        fastlane_data = self.fastlane_raw

        if "result" not in fastlane_data.get("response", {}):
            return fastlane_data

        result = fastlane_data['response']['result']

        if type(result) == list:
            # we are creating a clone so we do not accidentlly save this data
            fastlane_data = deepcopy(fastlane_data)
            result = deepcopy(result[-1])
            fastlane_data['response']['result'] = result

            # NOTE: we do not call save here, else we will lose data sent by
            # fastlane, and tomorrow we may change the algo

        return fastlane_data


    def fastlane_search_key(self):
        vehicle_details = self.fastlane["response"]["result"]['vehicle']
        key = ' '.join(
            vehicle_details.get(k, '').strip().upper()
            for k in (
                'fla_maker_desc',
                'fla_model_desc',
                'fla_variant',
                'maker_model'
            )
        )
        key += ' ' + ' '.join(
            NewRegistrationDetail.FUEL_TYPE_MAP.get(
                vehicle_details.get(k, '').strip().upper(), ''
            )
            for k in (
                'fla_fuel_type_desc',
                'fuel_type_desc',
            )
        )
        return ' '.join(key.split())

    def get_matching_vehicle_master(self):
        from motor_product import fastlane_utils
        return fastlane_utils.FastlaneVehicleMasterSearch(self).vehicle_master()

    def fastlane_natural_key(self, raise_exception=True):
        """ this is purely for debugging, not used anywhere """
        if "result" not in self.fastlane["response"]:
            if raise_exception:
                raise NoMatch
            else:
                return None
        vehicle_details = self.fastlane["response"]["result"]['vehicle']
        return (
            vehicle_details.get('fla_maker_desc', "").strip(),
            vehicle_details.get('fla_fuel_type_desc', "").strip(),
            vehicle_details.get('fla_model_desc', "").strip(),
            vehicle_details.get('fla_variant', "").strip(),
        )

    def set_data(self, source, data):
        saved = False
        if self.source not in NewRegistrationDetail.SOURCES:
            raise exceptions.ValidationError({'source': _('Invalid source')})
        with transaction.atomic():
            self.refresh_from_db()
            self.data[source] = data
            saved = self.save()
        return saved

    def _get_fastlane_result(self):
        result = self.fastlane.get('response', {}).get('result', {})
        if result and type(result) == list:
            result = result[-1]
        return result

    def get_fastlane_vehicle_info(self):
        return self._get_fastlane_result().get('vehicle', {})

    def _is_financed(self, from_date):
        if from_date and (utils.years_ago(3, datetime.datetime.now().date()) - from_date.date()).days <= 0:
            return True
        return False

    def _get_mapped_value(self, text, mapped_dict):
        mapped_value = ''

        if not text:
            return mapped_value

        mapped_values = {}
        for key, value in mapped_dict.iteritems():
            score = fuzz.token_set_ratio(value.upper(), text.upper())
            if score > 90:
                mapped_values[score] = key

        if mapped_values:
            mapped_value = mapped_values[max(mapped_values.keys())]

        return mapped_value

    def get_fastlane_financier_details(self, financiers={}):
        is_financed = False
        financier_id = None
        financier_name = ''

        fla_fncr_name = self._get_fastlane_result().get('hypth', {}).get('fla_fncr_name', '')
        fncr_name = self._get_fastlane_result().get('hypth', {}).get('fncr_name', '')

        if financiers:
            mapped_values = {}
            for key, value in financiers.iteritems():
                fla_fncr_name_score = fuzz.token_set_ratio(
                    value.upper(),
                    fla_fncr_name.upper()
                )
                fncr_name_score = fuzz.token_set_ratio(
                    value.upper(),
                    fncr_name.upper()
                )
                score = max(fla_fncr_name_score, fncr_name_score)
                if score > 90:
                    mapped_values[score] = key

            if mapped_values:
                financier_id = mapped_values[max(mapped_values.keys())]
                financier_name = financiers[financier_id]
        else:
            if fncr_name:
                financier_name = fncr_name
            elif fla_fncr_name:
                financier_name = fla_fncr_name

        try:
            if financier_name:
                is_financed = self._is_financed(
                    datetime.datetime.strptime(
                        self._get_fastlane_result().get('hypth', {}).get('from_dt', ''),
                        '%d/%m/%Y'
                    )
                )
        except Exception:
            utils.fastlane_logger.info(
                'Fastlane Data: Invalid date format for %s ' % self.registration_number
            )

        return is_financed, financier_id, financier_name

    def get_fastlane_previous_insurer_master_id(self, previous_insurers={}):
        prev_insurer = self._get_mapped_value(
            self._get_fastlane_result().get('insurance', {}).get('fla_ins_name', ''),
            previous_insurers
        )
        return prev_insurer, True if prev_insurer else False

    def get_rto_master(self):
        if len(self.registration_number) > 4:
            rto_code = self.registration_number[:2] + "-" + self.registration_number[2:4]
            return RTOMaster.objects.filter(
                rto_code__iexact=rto_code
            ).first()

    class Meta:
        db_table = 'motor_product_registrationdetail'


class RegistrationDetailStatistics(models.Model):
    """
    This class stores information related to each and every request.
    It has the following fields
        -- data (this field stores the raw information submitted by the user)
        -- ip (address of the requested user)
        -- registration_detail
        -- tracker
        -- vehicle_master (associated vehicle master)
        -- fastlane status:
            --- success:
                - fetched
            - Error
                - timed out
                - no record found
                - no result
                - Undefined error
        -- user status
            - success
            - failure
    """

    FASTLANE_STATUS_CHOICES = (
        ('SUCCESSFUL', 'Successfully Fetched'),
        ('ERROR_RECORD_NOT_FOUND', 'Record not Found'),
        ('ERROR_REQUEST_TIMED_OUT', 'Request Timed Out'),
        ('ERROR_INVALID_RESPONSE', 'Invalid Response'),
        ('REJECTED', 'Request Rejected')
    )

    USER_STATUS_CHOICES = (
        ('ACCEPTED', 'Accpted'),
        ('CHANGED', 'Changed'),
        ('UNKNOWN', 'Unknown')
    )

    data = JSONField(_('Data'), blank=True, default={})
    ip = models.GenericIPAddressField(db_index=True)
    registration_detail = models.ForeignKey("motor_product.NewRegistrationDetail", null=True)
    tracker = models.ForeignKey("core.Tracker", null=True)
    vehicle_master = models.ForeignKey("motor_product.VehicleMaster", null=True, blank=True)
    user = models.ForeignKey(User, null=True)

    fastlane_status = models.CharField(
        _('Fastlane Status'),
        max_length=64,
        choices=FASTLANE_STATUS_CHOICES,
        db_index=True,
        null=True
    )

    user_status = models.CharField(
        _('User Status'),
        max_length=64,
        choices=USER_STATUS_CHOICES,
        default='ACCEPTED',
        db_index=True
    )

    number_of_matches = models.IntegerField(
        validators=[
            validators.MinValueValidator(0),
            validators.MaxValueValidator(4),
        ],
        db_index=True,
        null=True
    )

    created_at = models.DateTimeField(_('created'), auto_now_add=True)
    updated_at = models.DateTimeField(_('modified'), auto_now=True)

    def __unicode__(self):
        return "%s - %s - %s - %s" % (
            self.registration_detail, self.fastlane_status, self.user_status, self.user
        )

    @property
    def is_accepted(self):
        if self.user_status == 'ACCEPTED' and self.fastlane_status in ['SUCCESS', 'SUCCESSFUL']:
            return True
        return False

class Transaction(models.Model):
    TRANSACTION_STATUS_CHOICES = (
        ('DRAFT', 'DRAFT'),
        ('PENDING', 'Pending'),
        ('IN_PROCESS', 'In Process'),
        ('COMPLETED', 'Completed'),
        ('CANCELLED', 'Cancelled'),
        ('FAILED', 'Failed'),
        ('MANUAL COMPLETED', 'Manual Completed'),
        ('PROPOSAL FAILED', 'Proposal Failed'),
        ('FORM_EXPIRED', 'Form Expired'),
    )
    tracker = models.ForeignKey(core_models.Tracker, related_name='motor_transaction')
    quote = models.ForeignKey(Quote)
    transaction_id = UUIDField(unique=True)

    proposer = models.ForeignKey(Contact, blank=True, null=True)
    insured_car = models.ForeignKey(
        PrivateCar, blank=True, null=True)  # Unwanted field after copy
    insured_vehicle = models.ForeignKey(UserVehicle, blank=True, null=True)
    receipt = models.ForeignKey(Receipt, blank=True, null=True)
    insurer = models.ForeignKey(Insurer, blank=True, null=True)
    payment_gateway = models.CharField(
        _('Payment Gateway'),
        max_length=32,
        blank=True,
        null=True,
        choices=PAYMENT_GATEWAY_CHOICES)
    proposal_number = models.CharField(max_length=250, blank=True)
    policy = models.OneToOneField('core.Policy', null=True, blank=True, related_name='motor_transaction')
    policy_number = models.CharField(max_length=250, blank=True)
    policy_start_date = models.DateTimeField(blank=True, null=True)
    policy_end_date = models.DateTimeField(blank=True, null=True)
    reference_id = models.CharField(max_length=250, blank=True)
    policy_token = models.CharField(max_length=250, blank=True)
    payment_token = models.CharField(max_length=250, blank=True)
    premium_paid = models.CharField(max_length=10, blank=True)
    payment_on = models.DateTimeField(blank=True, null=True)
    status = models.CharField(
        max_length=50, choices=TRANSACTION_STATUS_CHOICES)
    status_history = postgres_fields.ArrayField(
        status, help_text='Comma separated and no spaces', default=[]
    )
    policy_document = models.FileField(
        upload_to=get_policy_path, null=True, blank=True)
    payment_done = models.BooleanField(default=False)
    insured_details_json = JSONField(
        _('insured_details'), blank=True, default={})
    raw = JSONField(_('raw'), blank=True, default={})
    mailer_flag = models.BooleanField(default=False)
    proposal_request = models.TextField(blank=True, null=True)
    proposal_response = models.TextField(blank=True, null=True)
    proposal_on = models.DateTimeField(blank=True, null=True)
    proposal_amount = models.CharField(max_length=10, blank=True, null=True)
    policy_request = models.TextField(blank=True)
    policy_response = models.TextField(blank=True)
    payment_request = models.TextField(blank=True)
    payment_response = models.TextField(blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    vehicle_type = models.CharField(
        max_length=25, choices=VEHICLE_TYPE_CHOICES, default='Private Car')
    rsa_policy = models.FileField(null=True, blank=True, upload_to=settings.MOTOR_RSA_PDF_PATH)
    requirement = models.ForeignKey(
        core_models.Requirement,
        blank=True,
        null=True,
        related_name="requirement_transactions"
    )
    rsa_policy_number = models.CharField(max_length=250, null=True, blank=True)
    registration_detail = models.ForeignKey(RegistrationDetail, null=True)

    class Meta:
        permissions = (
                ('can_view_ops_sales', 'Can view ops sales related  activities '),
                )

    def __unicode__(self):
        return self.transaction_id

    @property
    def policy_tat(self):

        def landt():
            try:
                registration_date = datetime.datetime.strptime(self.quote.raw_data['registrationDate'], "%d-%m-%Y")
                policy_start_date = self.policy_start_date or self.quote.new_policy_start_date
            except:
                return 24 * 60
            else:
                vehicle_age = relativedelta.relativedelta(
                    utils.convert_datetime_to_timezone(policy_start_date),
                    utils.convert_datetime_to_timezone(registration_date)
                ).years
                is_zero_dep = self.quote.raw_data['addon_isDepreciationWaiver'] == '1'
                if 7 <= vehicle_age <= 15:
                    return 1440
                elif 3 <= vehicle_age <= 5 and is_zero_dep:
                    return 1440
                else:
                    return 240

        INSURER_POLICY_TAT = {
            'reliance': 15,
            'icici-lombard': 15,
            'future-generali': 15,
            'universal-sompo': 15,
            'liberty-videocon': 15,
            'tata-aig': 15,
            'hdfc-ergo': 15,
            'iffco-tokio': 15,
            'bharti-axa': 15,
            'bajaj-allianz': 120,
            'new-india': 15,
            'oriental': 30,
            'l-t': lazy(landt, int)(),
        }

        return int(INSURER_POLICY_TAT.get(self.insurer.slug, 24 * 60))

    @property
    def policy_tat_verbose(self):
        if self.policy_tat > 60:
            policy_tat = "{} hours".format(int(math.ceil(self.policy_tat / 60)))
        else:
            policy_tat = "{} minutes".format(self.policy_tat)

        return policy_tat

    def customer_name(self):
        details = self.raw.get('user_form_details', {})
        return "%s %s" % (details.get('cust_first_name',''), details.get('cust_last_name',''), )

    @property
    def policy_url(self):
        if self.policy_document:
            return self.policy_document.url
        return self.insured_details_json.get('policy_url', '')

    @property
    def last_status(self):
        return self.status_history[-1] if self.status_history else None

    @property
    def is_complete(self):
        return self.status in ['COMPLETED', 'MANUAL COMPLETED']

    def get_form_data(self):
        from .forms import MotorFDForm
        user_form_data = self.raw.get('user_form_details', {})
        first_name = user_form_data.get('cust_first_name', None)
        last_name = user_form_data.get('cust_last_name', None)
        cust_email = user_form_data.get('cust_email', None)
        cust_phone = user_form_data.get('cust_phone', None)
        gender = user_form_data.get('cust_gender', None)
        cust_marital_status = user_form_data.get('cust_marital_status', None)
        father_fname = user_form_data.get('father_first_name', None)
        father_lname = user_form_data.get('father_last_name', None)
        dob = user_form_data.get('cust_dob', None)

        if not dob:
            day = user_form_data.get('cust_dob_dd', None)
            month = user_form_data.get('cust_dob_mm', None)
            year = user_form_data.get('cust_dob_yyyy', None)
            if day and month and year:
                dob = "%s-%s-%s" % (day, month, year)

        if dob and '-' not in dob and '/' not in dob and " " not in dob:
            day = dob[:2]
            month = dob[2:4]
            year = dob[4:]
            dob = "%s-%s-%s" % (day, month, year)

        if dob and ('-' in dob or '/' in dob):
            if " " in dob:
                dob = dob.replace(' ', '')

        occupation = user_form_data.get('cust_occupation', None)
        pan_no = user_form_data.get('cust_pan', None)

        person_form_data = {
            "first_name": first_name,
            "last_name": last_name,
            "mobile": cust_phone,
            "email": cust_email,
            "gender": gender,
            "birth_date": dob,
            "maritial_status": str(cust_marital_status),
            "father_name": "%s %s" % (father_fname, father_lname),
            "occupation": occupation,
            "pan_no": pan_no,
        }
        nominee_name = user_form_data.get('nominee_name', None)
        nominee_age = user_form_data.get('nominee_age', None)
        nominee_age = nominee_age if nominee_age else None

        if nominee_name:
            splitted_nominee_name = nominee_name.split(" ")

            nominee_form_data = {
                'first_name': ''.join(splitted_nominee_name[:-1]),
                'last_name': ''.join(splitted_nominee_name[:1]),
                'role': 'nominee'
            }

        if nominee_age:
            if isinstance(nominee_age, str):
                if re.match(r'^[0-9]{1,3}$', nominee_age.replace(" ", "")):
                    nominee_form_data['age'] = nominee_age.replace(" ", "")
            elif isinstance(nominee_age, int):
                nominee_form_data['age'] = nominee_age

        registration_number = user_form_data.get('vehicle_reg_no', None)
        vehicle_rto = user_form_data.get('vehicle_rto', None)
        engine_number = user_form_data.get('vehicle_engine_no', None)
        chassis_number = user_form_data.get('vehicle_chassis_no', None)
        vehicle_registration_dt = user_form_data.get('vehicle_reg_date', None)
        car_financier = user_form_data.get('car-financier-text', None)
        if not car_financier:
            car_financier = user_form_data.get('car-financier', None)

        # Past policy information.
        past_policy_number = user_form_data.get('past_policy_number', None)
        past_policy_start_date = user_form_data.get("past_policy_start_date", None)
        past_policy_end_date = user_form_data.get('past_policy_end_date', None)
        if not past_policy_end_date:
            day = user_form_data.get('past_policy_end_date_dd', None)
            month = user_form_data.get('past_policy_end_date_mm', None)
            year = user_form_data.get('past_policy_end_date_yyyy', None)
            if day and month and year:
                past_policy_end_date = "%s-%s-%s" % (day, month, year)

        past_policy_cover_type = user_form_data.get('past_policy_cover_type', None)
        past_policy_insurer = user_form_data.get('past_policy_insurer', '')
        past_policy_insurer = str(past_policy_insurer) if past_policy_insurer else None
        past_policy_insurer_city = user_form_data.get('past_policy_insurer_city', None)

        vehicle_master = None
        if self.quote.vehicle:
            vehicle_master = str(self.quote.vehicle.pk)

        vehicle_detail_form_data = {
            'registration_number': registration_number,
            'engine_number': engine_number,
            'chassis_number': chassis_number,
            'vehicle_master': vehicle_master,
            'vehicle_registration_date': vehicle_registration_dt,
            'vehicle_rto': vehicle_rto,
            'loan_provider': car_financier,
            'past_policy_number': past_policy_number,
            'past_policy_start_date': past_policy_start_date,
            'past_policy_end_date': past_policy_end_date,
            'past_policy_cover_type': past_policy_cover_type,
            'past_policy_insurer': past_policy_insurer,
            'past_policy_insurer_city': past_policy_insurer_city,
        }
        if self.vehicle_type == "Private Car":
            vehicle_detail_form_data = {'car_{}'.format(key): value for key, value in vehicle_detail_form_data.items()}
        else:
            vehicle_detail_form_data = {'bike_{}'.format(key): value for key, value in vehicle_detail_form_data.items()}

        building_no = user_form_data.get('add_building_name', None)
        add_house_no = user_form_data.get('add_house_no', None)
        street = user_form_data.get('add_street_name', None)
        state = user_form_data.get('add_state', None)
        pincode = user_form_data.get('add_pincode', None)

        # post_office = user_form_data.get('add_post_office', '')
        landmark = user_form_data.get('add_landmark', None)

        # district = user_form_data.get('add_district', '')
        city = user_form_data.get('add_city', None)
        if not city:
            city = user_form_data.get('loc_add_city', None)

        # Reg address
        same_addr = user_form_data.get('add-same', 'false')
        reg_address_form_data = {}
        if same_addr == 'false':
            reg_building_no = user_form_data.get('reg_add_building_name', None)
            reg_add_house_no = user_form_data.get('reg_add_house_no', None)
            reg_street = user_form_data.get('reg_add_street_name', None)
            reg_pincode = user_form_data.get('reg_add_pincode', None)
            reg_landmark = user_form_data.get('reg_add_landmark', None)
            reg_district = user_form_data.get('reg_add_district', None)
            reg_city = user_form_data.get('reg_add_city', None)
            reg_state = user_form_data.get('reg_add_state', None)

            if not reg_city:
                reg_city = user_form_data.get('loc_reg_add_city', None)

            reg_address_form_data = {
                "reg_address_line1": reg_building_no,
                "reg_address_line2": reg_add_house_no,
                "reg_address_line3": reg_street,
                "reg_pincode": reg_pincode,
                "reg_landmark": reg_landmark,
                "reg_district": reg_district,
                "reg_city": reg_city,
                "reg_state": reg_state,
            }

        address_form_data = {
            "comm_address_line1": building_no,
            "comm_address_line2": add_house_no,
            "comm_address_line3": street,
            "comm_pincode": pincode,
            "comm_landmark": landmark,
            "comm_city": city,
            "comm_state": state,
        }
        data = {}
        data.update(person_form_data)
        data.update(vehicle_detail_form_data)
        data.update(reg_address_form_data)
        data.update(address_form_data)

        # Clean data.
        cleaned_data = {}
        for key, val in data.iteritems():
            if val is not None:
                cleaned_data[key] = val

        return MotorFDForm(data=cleaned_data).get_fd()

    def mark_complete(self, status='COMPLETED', user=None):
        pre_self = self._default_manager.get(id=self.id)
        if pre_self.status in ['COMPLETED', 'MANUAL COMPLETED']:
            raise ValueError('Transaction is already marked complete')

        if status not in ['COMPLETED', 'MANUAL COMPLETED']:
            raise ValueError(u'{} is not termed as completed status'.format(status))

        user = user or AnonymousUser()
        self.status = status
        if status == 'COMPLETED':
            self.payment_on = self.payment_on or timezone.now()

        self.request = core_activity.request_to_dict()
        self.save()
        create_user_account.send(sender=self._meta.model, transaction=self, user=user)

    def update_policy(self, content, request_dict={}, send_mail=True):
        policy_path = get_policy_path(self, u'{}.pdf'.format(self.transaction_id))
        with default_storage.open(policy_path, 'wb') as f:
            for chunk in ContentFile(content).chunks():
                f.write(chunk)
        self.policy_document = policy_path
        self.save(update_fields=['policy_document'])

        if not self.policy:
            try:
                self.populate_policy(request_dict=request_dict)
            except:
                utils.log_error(logger, msg='Unable to populate motor policy')

        policy_updated.send(sender=self._meta.model, transaction=self, send_mail=send_mail)

    def populate_policy(self, request_dict=None):
        from core import models as core_models
        from account_manager.models import UserTransaction

        user = UserTransaction.objects.get_for_transaction(self).user
        if not user:
            logger.error("No user found in UserTransaction for transaction_id(%s)" % self.transaction_id)

        if self.transaction_type == 'car':
            act_prod = core_models.RequirementKind.CAR
        else:
            act_prod = core_models.RequirementKind.BIKE
        act = activity.create_activity(
            activity_type=activity.SUCCESS,
            product=act_prod,
            page_id='policy',
            form_name='',
            form_position='',
            form_variant='',
            user=user,
            request_dict=request_dict
        )

        if self.quote.policy_type in ['new', 'renew']:
            policy_start_date = self.policy_start_date or self.quote.new_policy_start_date
        else:
            policy_start_date = self.policy_start_date

        start_date = utils.convert_datetime_to_timezone(policy_start_date)
        end_date = utils.convert_datetime_to_timezone(self.policy_end_date or putils.add_years(start_date, 1) if start_date else None)
        policy = core_models.Policy.objects.create(
            name=os.path.splitext(self.policy_document.name)[0].split(os.path.sep)[-1],
            document=self.policy_document,
            app=self._meta.app_label,
            status=core_models.Policy.VALID,
            user=user,
            requirement_sold=act.requirement,
            policy_number=self.policy_number,
            premium=self.premium_paid or None,
            issue_date=start_date,
            expiry_date=end_date,
        )
        act.requirement.policy = policy
        act.requirement.save(update_fields=['policy'])
        self.policy = policy
        self.save(update_fields=['policy'])

    def open_requirement_if_deleted(self):
        is_requirement_with_state = self.requirement and self.requirement.current_state
        if is_requirement_with_state and self.requirement.current_state.pk == 17:
            # Change state to previous state.
            self.requirement.reopen(previous_state=True)

    def track_activity(self, activity_type, page_id, request=None, requirement=None):
        try:
            if self.transaction_type == 'car':
                act_prod = core_models.RequirementKind.CAR
            else:
                act_prod = core_models.RequirementKind.BIKE
            kwargs = {
                'activity_type': activity_type,
                'product': act_prod,
                'page_id': page_id,
                'form_name': '',
                'form_variant': '',
                'form_position': '',
                'data': self.get_form_data(),
                'requirement': requirement,
                'extra': {
                    'transaction_id': self.transaction_id,
                }
            }
            if request:
                kwargs["request_dict"] = request

            act = activity.create_activity(**kwargs)
            if act and act.requirement:
                # if isinstance(request, dict):
                #     request_data = request
                # else:
                #     request_data = dict(request.mini_dict)
                act.requirement.update_extra({
                    'transaction_url': urlparse.urljoin(settings.SITE_URL, self.proposal_form_link()),
                    'transactionId': self.transaction_id,
                    'quoteId': self.quote.quote_id,
                })
                # act.requirement.update_extra(request_data)
                if not self.requirement:
                    self.requirement = act.requirement
                    self.save(update_fields=['requirement'])
        except:
            utils.log_error(logger, request, 'Unable to track motor activity')
        else:
            return act

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if update_fields:
            update_fields += ['updated_on', 'created_on']

        if self.status != self.last_status:
            self.status_history += [self.status]
            if update_fields:
                update_fields += ['status_history']

        return super(Transaction, self).save(force_insert=force_insert, force_update=force_update,
                                             using=using, update_fields=update_fields)

    def customer_phone_number(self):
        return self.raw['user_form_details']['cust_phone']

    def customer_email(self):
        return "%s" % self.raw['user_form_details']['cust_email']

    def vehicle_registration_number(self):
        return self.raw['user_form_details'].get('vehicle_reg_no', '')

    def proposal_form(self):
        return "<a href='%s' target='__blank'>%s</a>" % (
            reverse('desktopForm', args=('fourwheeler', self.insurer.slug, self.transaction_id, )),
            'Open'
        )

    def proposal_form_link(self):
        return reverse('desktopForm', args=('fourwheeler', self.insurer.slug, self.transaction_id, ))

    @property
    def transaction_mode(self):
        return self.quote.type

    @property
    def od_premium(self):
        import motor_product.insurers.insurer_utils as insurer_utils
        try:
            return insurer_utils.calculate_od_premium(self) if self.premium_paid else None
        except (ValueError, KeyError):
            return None

    @property
    def ncb(self):
        import motor_product.insurers.insurer_utils as insurer_utils
        response = insurer_utils.modify_response_for_ncb({'NCB': None}, self.quote.raw_data)
        return response.get('NCB', None)

    @property
    def transaction_type(self):
        return 'car' if self.vehicle_type == 'Private Car' else 'bike'

    def mini_json(self):
        return {
            'id': self.id,
            'transaction_id': self.transaction_id,
            'payment_date': self.payment_on,
            'mobile': self.mobile
        }

    @property
    def can_populate_from_fastlane(self):
        if self.quote.raw_data.get('rds_id', "false") not in ['false', None]:
            rds = RegistrationDetailStatistics.objects.get(id=self.quote.raw_data.get('rds_id'))
            if rds.user_status == 'ACCEPTED':
                return True
        return False

    def set_cookie(self, response_obj):
        vehicle_type = format_vehicle_type(self.vehicle_type)
        max_age = 24 * 60 * 60
        expires = timezone.now() + datetime.timedelta(seconds=max_age)
        response_obj.set_cookie('%s_%s_transaction' % (vehicle_type, self.insurer.slug),
                                self.transaction_id, expires=expires)



class FieldAgent(models.Model):
    INSPECTION = "insp"
    AGENT_TYPE = (
        (INSPECTION, "Inspection"),
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    company = models.ForeignKey(core_models.Company)
    agent_type = models.CharField(
        max_length=4, choices=AGENT_TYPE, default=INSPECTION)


# ### RTO Mapping models ###
class RTOInsurer(models.Model):
    insurer = models.ForeignKey(Insurer, blank=True, null=True)
    rto_code = models.CharField(max_length=100, blank=True)
    rto_name = models.CharField(max_length=100, blank=True)
    rto_state = models.CharField(max_length=100, blank=True)
    rto_zone = models.CharField(max_length=10, blank=True)
    master_rto_state = models.CharField(max_length=100, blank=True)
    raw = JSONField(_('raw'), blank=True, default={})
    vehicle_type = models.CharField(
        max_length=25, choices=VEHICLE_TYPE_CHOICES, default='Private Car')

    def __unicode__(self):
        return u'%s - %s (%s)' % (self.insurer, self.rto_name, self.vehicle_type)

    def get_dictionary(self):
        json_dict = {
            'id': self.id,
            'name': self.rto_name,
            'state': self.master_rto_state,
        }
        return json_dict


class RTOMaster(models.Model):
    rto_code = models.CharField(max_length=10, unique=True)
    rto_name = models.CharField(max_length=100)
    rto_state = models.CharField(max_length=100)
    raw = JSONField(_('raw'), blank=True, default={})
    insurer_rtos = models.ManyToManyField(RTOInsurer, through='RTOMapping')
    # If this RTOMaster is active for inspection
    is_active_for_inspection = models.BooleanField(default=False)

    def __unicode__(self):
        return u'%s : %s' % (self.rto_code, self.rto_name)

    def get_dictionary(self):
        json_dict = {
            'id': self.rto_code,
            'name': self.rto_code + ' - ' + self.rto_name
        }
        return json_dict


class RTOMapping(models.Model):
    rto_master = models.ForeignKey(RTOMaster, blank=True, null=True)
    rto_insurer = models.ForeignKey(RTOInsurer, blank=True, null=True)
    insurer = models.ForeignKey(Insurer, blank=True, null=True)
    is_approved_mapping = models.BooleanField(default=False)

# ### Addon application models ###


class Addon(models.Model):
    name = models.CharField(max_length=250, blank=True)
    code = models.CharField(max_length=150, blank=True, unique=True)
    description = models.TextField(blank=True)

    def __unicode__(self):
        return self.name


class RTOExclusion(models.Model):
    rto = models.ForeignKey('RTOMaster')
    exclusion = models.ForeignKey('AddonExclusion', related_name='rtos')


class ModelExclusion(models.Model):
    model = models.ForeignKey('Model')
    exclusion = models.ForeignKey('AddonExclusion', related_name='models')


class AddonExclusion(models.Model):
    addon = models.ForeignKey(Addon, blank=True, null=True)
    insurer = models.ForeignKey(Insurer, blank=True)
    age_limit = models.FloatField(blank=True, null=True)

    def __unicode__(self):
        return u'%s - %s' % (self.insurer.title, self.addon.name)

# ### Created for L&T ###


class LnTPolicyResponse(models.Model):
    TxnId = models.CharField(db_column='txnid', max_length=100, blank=True)
    Status = models.CharField(db_column='status', max_length=100, blank=True)
    TxnStatusDetail = models.ForeignKey(
        'LnTStatusDetail', blank=True, null=True, db_column='txnstatusdetail_id'
    )
    TxnErrorDetails = models.ForeignKey(
        'LnTErrorDetail', blank=True, null=True, db_column='txnerrordetails_id')
    raw = JSONField(_('raw'), blank=True, default={})
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)


class LnTStatusDetail(models.Model):
    ContactId = models.CharField(
        max_length=100, blank=True, db_column='contactid'
    )
    ReceiptNo = models.CharField(
        max_length=100, blank=True, db_column='receiptno'
    )
    PolicyNo = models.CharField(
        max_length=100, blank=True, db_column='policyno'
    )
    ProposalNo = models.CharField(
        max_length=100, blank=True, db_column='proposalno'
    )


class LnTErrorDetail(models.Model):
    ErrorLevel = models.CharField(
        max_length=100, blank=True, db_column='errorlevel'
    )
    Details = models.CharField(max_length=100, blank=True, db_column='details')

# ### Discount/Loading models ###


class DiscountLoading(models.Model):
    insurer = models.ForeignKey(Insurer)
    make = models.CharField(max_length=250, blank=True, null=True)
    model = models.CharField(max_length=250, blank=True, null=True)
    variant = models.CharField(max_length=250, blank=True, null=True)
    fuel_type = models.CharField(max_length=10, blank=True, null=True)
    lower_age_limit = models.IntegerField(blank=True, null=True)
    upper_age_limit = models.IntegerField(blank=True, null=True)
    lower_cc_limit = models.IntegerField(blank=True, null=True)
    upper_cc_limit = models.IntegerField(blank=True, null=True)
    state = models.CharField(max_length=50, blank=True, null=True)
    rtos = models.ManyToManyField(RTOInsurer, blank=True)
    ncb = models.IntegerField(blank=True, null=True)
    discount = models.FloatField(blank=True, null=True)
    vehicle_type = models.CharField(
        max_length=25, choices=VEHICLE_TYPE_CHOICES, default='Private Car')

    def get_dictionary(self):
        json_dict = {
            'id': self.id,
            'make': self.make,
            'model': self.model,
            'variant': self.variant,
            'fuel_type': self.fuel_type,
            'lower_age_limit': self.lower_age_limit,
            'upper_age_limit': self.upper_age_limit,
            'lower_cc_limit': self.lower_cc_limit,
            'upper_cc_limit': self.upper_cc_limit,
            'state': self.state,
            'ncb': self.ncb,
            'rto_count': self.rtos.count(),
            'discount': self.discount,
        }
        return json_dict


class DiscountCode(models.Model):
    CODE_LENGTH = 4

    DISCOUNT_TYPE_CHOICES = (
        (1, _('Special')),
        (2, _('Corporate')),
    )

    code = models.CharField(max_length=10)
    email = models.EmailField(null=True, blank=True)
    mobile = models.CharField(max_length=11, blank=True, null=True)
    discount_type = models.IntegerField(
        choices=DISCOUNT_TYPE_CHOICES, default=1)
    is_invalid = models.BooleanField(default=False)
    transaction = models.ForeignKey(Transaction, blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    raw = JSONField(blank=True, default={})

    class Meta:
        unique_together = ('code', 'email', 'mobile')

    def save(self, *args, **kwargs):
        if not self.pk:
            # object is being created, thus no primary key field yet
            self.code = random.randint(
                int('1' * self.CODE_LENGTH), int('9' * self.CODE_LENGTH))
        super(DiscountCode, self).save(*args, **kwargs)

    def is_expired(self):
        if self.is_invalid:
            return True
        timezn = timezone.get_current_timezone()
        created_time = self.created_on.astimezone(timezn)
        created_time = created_time.replace(tzinfo=None)
        if (
            (datetime.datetime.now() > created_time + datetime.timedelta(days=1)) and
            self.discount_type != 2
        ):
            return True
        if (self.transaction and self.transaction.status == 'COMPLETED'):
            return True
        return False

    def get_discount(self, insurer, quote_parameters):
        if not self.is_expired():
            if quote_parameters and self.discount_type != 2:
                # Check for validity of quote parameters
                try:
                    quote_id = self.raw.get('quoteId', None)
                    quote = Quote.objects.get(quote_id=quote_id)
                    rto_actual = "-".join(
                        quote.raw_data['registrationNumber[]'][0:2])
                    rto_new = "-".join(
                        quote_parameters['registrationNumber[]'][0:2])
                    if not (
                        quote_parameters['vehicleId'] == quote.raw_data[
                            'vehicleId'
                        ] and rto_actual == rto_new
                    ):
                        return None
                except:
                    return None
            discount = self.raw.get(insurer, {}).get('discount', None)
            return discount
        return None

    def get_corporate_lead(self):
        if self.discount_type == 2 and self.corporateleads_set.count() > 0:
            return self.corporateleads_set.all()[0]
        return None


class BestPricing(models.Model):
    vehicle = models.ForeignKey(VehicleMaster)
    insurer_slug = models.CharField(max_length=255, null=True, default=None)
    city = models.CharField(max_length=255)
    year = models.CharField(max_length=6)
    premium = models.FloatField()
    idv = models.FloatField(default=0)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return '%s - %s - %s - %s - %s - %s' % (
            self.vehicle, self.city, self.year,
            self.insurer_slug, self.idv, self.premium
        )


def get_make_choices():
    return [(make.name, make.name) for make in Make.objects.all()] + [('Multi Brand', 'Multi Brand')]


class Garage(models.Model):
    name = models.CharField(max_length=200)
    insurers = models.ManyToManyField(Insurer, blank=True)
    city = models.ForeignKey(wiki_models.City)
    address = models.TextField()
    pin = models.CharField(max_length=6)
    make = models.CharField(max_length=50, default='Multi Brand')

    def __init__(self, *args, **kwargs):
        super(Garage, self).__init__(*args, **kwargs)
        self._meta.get_field_by_name('make')[0]._choices = get_make_choices()

    def __unicode__(self):
        return "%s, %s" % (self.name, self.city.name)

    class Meta:
        unique_together = ('name', 'pin', )


class TempGarage(models.Model):
    name = models.CharField(max_length=200)
    insurers = models.ManyToManyField(Insurer, blank=True)
    city = models.CharField(max_length=200)
    wikicity = models.ForeignKey(wiki_models.City, null=True)
    address = models.TextField(null=True)
    pin = models.CharField(max_length=6, null=True)
    company = models.CharField(max_length=200)
    make = models.CharField(
        max_length=50, choices=(('Multi Brand', 'Multi Brand'),),
        default='Multi Brand'
    )

    class Meta:
        # abstract = True
        unique_together = ('name', 'pin', )

    def __unicode__(self):
        return "%s, %s" % (self.name, self.city)


class InspectionAgency(models.Model):
    company = models.OneToOneField(core_models.Company)

    def __unicode__(self):
        return self.company.name


'''
    Both the dealership and the dealer inherit the payout base.
    The payout base contains the details of how to pay the dealer/dealership.
    The dealer commission percentage always overrides the dealership's commission
    percentage.
'''


class Dealership(PayoutBase):
    company = models.OneToOneField(core_models.Company)

    def __unicode__(self):
        return "{0}: {1} {2} {3}".format(
            self.company.name, self.commission_percentage,
            self.payout_frequency, self.is_pre_deducted)

    def get_name(self):
        return self.company.name


class Dealer(PayoutBase):
    company = models.OneToOneField(core_models.Company)
    dealership = models.ManyToManyField(
        Dealership, related_name='dealers', blank=True,
        through="DealerDealershipMap"
    )

    def __unicode__(self):
        return "{0}: {1} dealership(s)".format(self.company.name, self.dealership.count())

    def get_name(self):
        return self.company.name


class DealerDealershipMap(PayoutBase):
    dealer = models.ForeignKey(Dealer)
    dealership = models.ForeignKey(Dealership, related_name="dealership_dealers")

    def __unicode__(self):
        return "{0} < {1}".format(self.dealer.company.name, self.dealership.company.name)

    def get_name(self):
        return self.dealer.company.name

    def get_active_payout_instance(self):
        if self.is_populated():
            return self
        elif self.dealer.is_populated():
            return self.dealer
        return self.dealership

    def get_comission_percentage(self):
        if self.commission_percentage != 0.0:
            return self.commission_percentage
        elif self.dealership.commission_percentage != 0.0:
            return self.dealership.commission_percentage
        return self.dealer.commission_percentage


class InspectionAgent(models.Model):
    ACTIVE = "active"
    INACTIVE = "inactive"
    MONDAY = 0
    TUESDAY = 1
    WEDNESDAY = 2
    THURSDAY = 3
    FRIDAY = 4
    SATURDAY = 5
    SUNDAY = 6

    WEEKDAY_OPTIONS = (
        (SUNDAY, 'sunday'),
        (MONDAY, 'monday'),
        (TUESDAY, 'tuesday'),
        (WEDNESDAY, 'wednesday'),
        (THURSDAY, 'thursday'),
        (FRIDAY, 'friday'),
        (SATURDAY, 'saturday')
    )
    user = models.ForeignKey(User)
    weekly_off = postgres_fields.ArrayField(models.IntegerField(), help_text="""
     Weekdays on which the agent will not be working.
     """, size=7)
    area = models.ManyToManyField('InspectionArea')

    def __unicode__(self):
        return "%s, (%s)" % (self.user.get_full_name(), ','.join([a.name for a in self.area.all()]))


class DailyTimeSlot(models.Model):
    start = models.TimeField(verbose_name='start_time')
    end = models.TimeField(verbose_name='end_time')
    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s - %s" % (self.start.strftime("%I:%M %p"), self.end.strftime("%I:%M %p"))

    def to_json(self):
        return {
            "id": self.id,
            "start_time": self.start.strftime("%I:%M %p"),
            "end_time": self.end.strftime("%I:%M %p")
        }


class Leave(models.Model):
    """
    If the leave date is given by no agents are given
    we consider it a leave for all agents
    """
    date = models.DateField(unique=True, help_text="Date for which leave is granted")
    agents = models.ManyToManyField(InspectionAgent, blank=True,
                                    verbose_name="Agents on leave",
                                    help_text="""If no agent is entered, assume that all agents are on leave for that day""")
    reason = models.CharField(max_length=255, blank=True, help_text="""In case of a public
            holiday this is the text shown to the customer as reason for non-availability""")

    def __unicode__(self):
        return "%s - %s" % (self.date.strftime("%d-%m-%Y"), ",".join([a.user.get_full_name() for a in self.agents.all()]))


class Inspection(models.Model):
    """
    Inspection Pending
        - Document Uploaded
            - Document Approved
                - Payment done
                    - Payment approved
        - Document Rejected
    - Inspection Cancelled
    """
    PENDING = 'PENDING'
    CANCELLED = 'CANCELLED'
    DOCUMENT_UPLOADED = 'DOCUMENT_UPLOADED'
    DOCUMENT_APPROVED = 'DOCUMENT_APPROVED'
    DOCUMENT_REJECTED = 'DOCUMENT_REJECTED'
    PAYMENT_DONE = 'PAYMENT_DONE'
    PAYMENT_REJECTED = 'PAYMENT_REJECTED'
    PAYMENT_APPROVED = 'PAYMENT_APPROVED'
    POLICY_ISSUED = 'POLICY_ISSUED'
    RESCHEDULED = 'RESCHEDULED'

    STATUS_CHOICES = (
        (PENDING, _('Pending')),
        (CANCELLED, _('Cancelled')),

        (DOCUMENT_UPLOADED, _('Document Uploaded')),
        (DOCUMENT_APPROVED, _('Document Approved')),
        (DOCUMENT_REJECTED, _('Document Rejected')),

        (PAYMENT_DONE, _('Payment done')),
        (PAYMENT_APPROVED, _('Payment approved')),
        (POLICY_ISSUED, _('Policy Issued')),
        (RESCHEDULED, _('Rescheduled'))
    )
    requirement = models.ForeignKey(
        core_models.Requirement,
        blank=True,
        null=True,
        related_name="requirement_inspections"
    )
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, db_index=True)
    transaction = models.ForeignKey(Transaction)
    raw = JSONField(_('raw'), blank=True, default={})
    address = models.TextField(blank=True)
    agent = models.ForeignKey(InspectionAgent, related_name='inspections', blank=True, null=True)
    scheduled_on = models.DateField(blank=True)
    schedule_slot = models.ForeignKey(DailyTimeSlot, null=True, blank=True)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default=PENDING)

    # In case of Coverfox, the inspection agency will be Coverfox's
    # else you need to give the name of third party agency doing the inspection
    inspection_agency = models.ForeignKey('InspectionAgency', null=True)
    city = models.ForeignKey(wiki_models.City, null=True, blank=True, related_name='city_inspection')
    area = models.ForeignKey('InspectionArea', null=True, blank=True, related_name='area_inspection')
    # Reference number for third party inspections, which will be
    # passed to the insurer to make payment, Cannot be blank for
    # transaction
    number = models.CharField(max_length=20, blank=True, null=True)
    created_on = models.DateTimeField(_('created'), auto_now_add=True)
    updated_on = models.DateTimeField(_('modified'), auto_now=True)
    # motor_parts = models.ManyToManyField(MotorPart, through=InspectionReport)

    def __unicode__(self):
        return_str = u'%s - scheduled on' % (self.agent)
        if self.scheduled_on:
            return_str += self.scheduled_on.strftime('%d %b %Y')
        return return_str

    def proposal_form(self):
        return "<a href='{0}' id='{1}'>{2}</a>".format(
            reverse('desktopForm', args=('fourwheeler', self.transaction.insurer.slug, self.transaction.transaction_id, )) + '?type=agent',
            str(self.uuid), 'Open'
        )

    def proposal_form_link(self):
        return reverse('desktopForm', args=('fourwheeler', self.transaction.insurer.slug, self.transaction.transaction_id, )) + '?type=agent'

    def dealer_get_next_link(self):
        return_data = {
            'link': '/motor/dealer/fourwheeler/',
            'text': '',
            'is_disabled': False,
        }
        if self.status == self.PENDING:
            # Disabling dealer upload documents
            return_data['text'] = "Upload documents"
            return_data['link'] += "upload-document/{0}".format(self.transaction.transaction_id)
            # return_data['link'] = reverse('inspection_form', kwargs={'transaction_id': self.transaction.transaction_id})
            # return_data['text'] = "Inspection Pending"
            # return_data['is_disabled'] = True
        elif self.status == self.DOCUMENT_APPROVED:
            return_data['link'] += "payment/{0}".format(self.transaction.transaction_id)
            return_data['text'] = "Pay Now"
        elif self.status == self.PAYMENT_DONE:
            return_data['link'] = "#"
            return_data['text'] = "Please Wait"
            return_data['is_disabled'] = True
        elif self.status == self.POLICY_ISSUED:
            return_data['link'] = self.transaction.policy_url
            return_data['text'] = "Download Policy"
        return return_data

    def scheduled_datetime(self):
        return "%s %s" % (self.scheduled_on.strftime('%d/%m/%Y'), (self.schedule_slot.start.strftime("%I:%M %p")))

    def save(self, user=None, *args, **kwargs):
        if 'reason' in kwargs:
            reason = kwargs.pop('reason')
            comment = kwargs.pop('comment')
        else:
            reason = None

        try:
            original = Inspection.objects.get(pk=self.pk)

            notification_type = None
            send_notification = False
            if original.status != self.status:
                notification_type = "rescheduled"
            elif original.scheduled_on != self.scheduled_on:
                notification_type = "datetime"
            elif original.schedule_slot != self.schedule_slot:
                notification_type = "datetime"
            elif original.agent and self.agent and original.agent.user.get_full_name() != self.agent.user.get_full_name():
                notification_type = "agent"

            if notification_type:
                send_notification = True

        except Inspection.DoesNotExist:
            send_notification = True
            notification_type = "scheduled" if self.city else "callback"

        super(Inspection, self).save(*args, **kwargs)
        inspection_state = InspectionStateChange(
            inspection=self,
            state=self.status,
        )
        if user and user.id:
            inspection_state.done_by = user
        if reason:
            inspection_state.reason = reason
            inspection_state.comment = comment

        if send_notification:
            try:
                with transaction.atomic():
                    self.send_inspection_status_change_notification(
                        notification_type=notification_type
                    )
            except Exception as e:
                utils.log_error(logger, msg="Exception: %s" % e)

        inspection_state.save()

    def send_inspection_status_change_notification(self, notification_type):
        activity = core_activity.create_activity(
                activity_type=core_activity.INSPECTION_STATUS_CHANGED,
                product='car',
                page_id='inspection',
                form_position='',
                form_name='',
                form_variant='',
                data='',
                extra={'quoteId': self.transaction.quote_id},
        )

        phone = self.transaction.customer_phone_number()
        email = self.transaction.customer_email()
        try:
            customer_id = User.objects.get_user(email, phone).customer_id
        except User.DoesNotExist:
            customer_id = self.transaction.tracker.session_key

        if self.status == Inspection.PENDING:
            event = 'inspection_scheduled'
        elif self.status == Inspection.RESCHEDULED:
            event = 'inspection_scheduled'
        elif self.status == Inspection.DOCUMENT_APPROVED:
            event = 'inspection_approved'
        elif self.status == Inspection.DOCUMENT_REJECTED:
            event = 'inspection_declined'
        elif self.status == Inspection.CANCELLED:
            event = 'inspection_cancelled'
        else:
            event = None

        if notification_type != "rescheduled":
            event = "inspection_scheduled"

        idatetime = ""
        if self.scheduled_on and self.schedule_slot:
            idatetime = utils.convert_datetime_to_timezone(
                datetime.datetime.strptime(self.scheduled_datetime(), "%d/%m/%Y %I:%M %p")
            )

        details = self.transaction.raw.get('user_form_details', {})
        if event:
            rdata = {
                'event': event,
                'type': notification_type,
                'email': email,
                'mobile': phone,
                'product_type': 'car' if self.transaction.vehicle_type == 'Private Car' else 'bike',
                'registration_number': self.transaction.vehicle_registration_number(),
                'datetime': idatetime,
                'agent': self.agent.user.get_full_name() if self.agent else "",
                'address': self.address,
                'insurer': self.transaction.insurer.title,
                'first_name': details.get('cust_first_name', ''),
                'agent_contact': self.agent.user.primary_phone if self.agent else "",
                'payment_url': "%s%s" % (settings.SITE_URL, self.transaction.proposal_form_link()),
                'expected_premium': int(self.transaction.raw['quote_response']['final_premium']),
                'make': self.transaction.quote.vehicle.make.name,
                'model': self.transaction.quote.vehicle.model.name,
            }
            from crm import drip_utils
            drip_utils.save_event.delay(
                rdata=rdata,
                customer_id=customer_id,
                activity_id=activity.id,
                check_ip=False,
            )

    def get_callbacks(self):
        callbacks = self.inspectioncallback_set.filter(call_time__gt=datetime.datetime.now()).first()
        return callbacks


class MotorInspectionDocument(core_models.Document):
    """
        Created by: Shobhit Sharma 16/10/2015
        This model inherits cover.models.InspectionDocuments model.
        This model is used to store the images taken by the field agent
         on the agent application.
    """
    DOCUMENT_TYPE = (
        ("rc-copy", "RC Copy"),
        ("last-policy-copy", "Last Policy Copy"),
        ("engine-imprint", "Engine Imprint"),
        ("chassis-imprint", "Chassis Imprint"),
        ("front", "Front"),
        ("engine", "Engine"),
        ("dashboard", "Dashboard"),
        ("rear", "Rear"),
        ("lf-angle", "Left Front Angle"),
        ("rf-angle", "Right Front Angle"),
        ("lr-angle", "Left Rear Angle"),
        ("rr-angle", "Right Rear Angle"),
        ("odometer", "Odometer"),
        ("floor", "Floor"),
        ("extra-accessories", "Extra Accessories"),
        ("bifuel-kit", "Bifuel Kit"),
        ("form-29", "Form 29"),
        ("form-30", "Form 30"),
        ("inspection-report", "Inspection Report"),
    )
    APPROVAL_PENDING = "APPROVAL_P"
    APPROVAL_REJECTED = "APPROVAL_R"
    APPROVED = "APPROVED"

    STATUS_CHOICES = (
        (APPROVAL_PENDING, _('Approval Pending')),
        (APPROVAL_REJECTED, _('Approval Rejected')),
        (APPROVED, _('Approved')),
    )
    approval_status = models.CharField(
        max_length=10, choices=STATUS_CHOICES,
        default=APPROVAL_PENDING)
    document_type = models.CharField(
        max_length=18, choices=DOCUMENT_TYPE)
    inspection = models.ForeignKey(Inspection, related_name='inspection_documents')
    created_on = models.DateTimeField(_('created'), auto_now_add=True)
    updated_on = models.DateTimeField(_('modified'), auto_now=True)

    def __unicode__(self):
        return "Inspection: {0} Doc Type: {1}".format(
            str(self.inspection.uuid), str(self.get_document_type_display()))


class PendingInspection(Inspection):
    class Meta:
        proxy = True


class RequirementDealerMap(models.Model):
    requirement = models.ForeignKey(core_models.Requirement)
    dealer = models.ForeignKey(DealerDealershipMap)
    commission_value = models.FloatField(default=0.0)
    raw = JSONField(_('raw'), blank=True, default={})

    def __unicode__(self):
        return str(self.dealer.dealer.company.name) + ' -> ' + str(self.requirement)+' = '+str(self.commission_value)


class Mswipe(models.Model):
    user_id = models.CharField(max_length=20)
    password = models.CharField(max_length=20)
    serial_number = models.CharField(max_length=50, blank=True, null=True)
    assigned_agent = models.ForeignKey(User, null=True)


class MotorPart(models.Model):
    part_name = models.CharField(max_length=200)
    slug = AutoSlugField(populate_from='part_name')
    is_accessory = models.BooleanField(default=False)

    def __unicode__(self):
        return "%s" % self.part_name


class InspectionReport(models.Model):

    SAFE = 'SAFE'
    NOT_FITTED = 'NOT_FITTED'
    SCRATCH = 'SCRATCH'
    DENTED = 'DENTED'
    BROKEN = 'BROKEN'
    RUSTED = 'RUSTED'
    DEFORM = 'DEFORM'

    STATUS_CHOICES = (
        (SAFE, _("Safe")),
        (NOT_FITTED, _("Not fitted")),
        (SCRATCH, _("Scratch")),
        (DENTED, _("Dented")),
        (BROKEN, _("Broken")),
        (RUSTED, _("Rusted")),
        (DEFORM, _("Deform")),
    )
    part = models.ForeignKey(MotorPart, null=True)
    feedback = models.TextField(max_length=200, blank=True, null=True)
    status = models.CharField(max_length=200, choices=STATUS_CHOICES, default=SAFE)
    inspection = models.ForeignKey(Inspection)

    class Meta:
        unique_together = ('part', 'inspection')

    def __unicode__(self):
        return '%s, %s' % (self.inspection, self.part.part_name)


class InspectionArea(models.Model):
    name = models.CharField(max_length=100)
    slug = AutoSlugField(populate_from='name',
                         always_update=True, unique=True, null=False)
    city = models.ForeignKey(wiki_models.City, null=True, blank=True, related_name='inspection_areas')
    is_enabled = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s, %s" % (self.name, self.city.name)


class DealerInvoice(models.Model):
    raw = JSONField(_('raw'), blank=True, default={})
    dealership = models.ForeignKey(Dealership)
    requirements = models.ManyToManyField(RequirementDealerMap, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    cleared_on = models.DateTimeField(blank=True, null=True)

    def __unicode__(self):
        return "{0} {1}".format(self.dealership.company.name, str(self.raw.get('total_amount', 0)))

    def arrears(self):
        return float(self.raw.get('total_amount', 0)) - float(self.raw.get('paid_amount', 0))


class InspectionStateChange(models.Model):
    """
    Maintains the State change History of Inspection.
    """
    NOT_CONTACTABLE = 'NOT_CONTACTABLE'
    CUSTOMER_CANCELLED = 'CUSTOMER_CANCELLED'
    NOT_AVAILABLE = 'NOT_AVAILABLE'
    AGENT_LATE = 'AGENT_LATE'

    CANCEL_REASONS = (
        (NOT_CONTACTABLE, _("Customer not contactable")),
        (CUSTOMER_CANCELLED, _("Customer cancelled the inspection")),
    )
    RESCHEDULE_REASONS = (
        (NOT_AVAILABLE, _("Customer not available")),
        (AGENT_LATE, _("Agent reached late on site")),
    )
    STATUS_REASONS = CANCEL_REASONS + RESCHEDULE_REASONS
    inspection = models.ForeignKey(Inspection)
    state = models.CharField(max_length=20, choices=Inspection.STATUS_CHOICES)
    reason = models.CharField(max_length=200, null=True, choices=STATUS_REASONS)
    comment = models.TextField(max_length=200, null=True)
    changed_on = models.DateTimeField(auto_now_add=True)
    done_by = models.ForeignKey(User, null=True, blank=True)

    def __unicode__(self):
        return "%s, %s" % (self.inspection, self.state)


class InspectionCallback(models.Model):
    """
    Callback Table : Keeps track for the callbacks scheduled for inspections
    """
    inspection = models.ForeignKey('Inspection')
    call_time = models.DateTimeField(_('inspection callback'))


class IntegrationStatistics(models.Model):

    '''
    If new error is added then make changes in INTEGRATION_ERROS -> motor_product/errors.py accordingly
    '''

    COVERFOX_TP_RESTRICTED = 'COVERFOX_TP_RESTRICTED'
    COVERFOX_MAKE_RESTRICTED = 'COVERFOX_MAKE_RESTRICTED'
    COVERFOX_DISCOUNT_LIMIT_EXCEEDED = 'COVERFOX_DISCOUNT_LIMIT_EXCEEDED'
    COVERFOX_DECLINED_MODEL = 'COVERFOX_DECLINED_MODEL'

    INSURER_USED_VEHICLE_RESTRICTED = 'INSURER_USED_VEHICLE_RESTRICTED'
    INSURER_AGE_RESTRICTED = 'INSURER_AGE_RESTRICTED'
    INSURER_EXPIRED_POLICY_RESTRICTED = 'INSURER_EXPIRED_POLICY_RESTRICTED'
    INSURER_ZERO_PREMIUM = 'INSURER_ZERO_PREMIUM'
    INSURER_MAKE_RESTRICTED = 'INSURER_MAKE_RESTRICTED'
    INSURER_NO_IDV = 'INSURER_NO_IDV'
    INSURER_NEW_VEHICLE_RESTRICTED = 'INSURER_NEW_VEHICLE_RESTRICTED'
    INSURER_ADD_ONS_RESTRICTED = 'INSURER_ADD_ONS_RESTRICTED'
    INSURER_PREMIUM_INVALID_RESPONSE = "INSURER_PREMIUM_INVALID_RESPONSE"
    INSURER_NEW_POLICY_START_DATE_RESTRICTED = 'INSURER_NEW_POLICY_START_DATE_RESTRICTED'

    VEHICLE_MAPPING_NOT_FOUND = 'VEHICLE_MAPPING_NOT_FOUND'
    VEHICLE_REQUIRED_DATA_NOT_FOUND = 'VEHICLE_REQUIRED_DATA_NOT_FOUND'
    RTO_MAPPING_NOT_FOUND = 'RTO_MAPPING_NOT_FOUND'
    IDV_LIMIT_EXCEEDED = 'IDV_LIMIT_EXCEEDED'
    PREMIUM_TIMEOUT = 'PREMIUM_TIMEOUT'
    UNKNOWN_ERROR = 'UNKNOWN_ERROR'

    NEW = 'NEW'
    RENEWAL = 'RENEWAL'
    ROLLOVER = 'ROLLOOVER'
    EXPIRED = 'EXPIRED'
    USED = 'USED'

    NCB_LIMIT_EXCEEDED = 'NCB_LIMIT_EXCEEDED'
    TASK_REVOKED = 'TASK_REVOKED'

    created_on = models.DateTimeField(_('created'), auto_now_add=True)
    error_code = models.CharField(max_length=50)
    status = models.BooleanField(default=False)
    required_time = models.FloatField(max_length=10)
    insurer = models.ForeignKey(Insurer, blank=True, null=True)
    quote = models.ForeignKey(Quote)
    vehicle_type = models.CharField(max_length=25, choices=VEHICLE_TYPE_CHOICES, default='Private Car')
    rto_master = models.ForeignKey(RTOMaster, blank=True, null=True)
    master_vehicle = models.ForeignKey(VehicleMaster, blank=True, null=True)
    case = models.CharField(max_length=20)

    def __unicode__(self):
        return '%s-%s' % (self.insurer.title, self.error_code)

    def save(self, *args, **kwargs):
        request_data = self.quote.raw_data
        if request_data.get('isNewVehicle', None) == '1':
            self.case = self.NEW
        if request_data.get('isNewVehicle', None) == '0':
            self.case = self.ROLLOVER
        if request_data.get('isRenew', None) == '1':
            self.case = self.RENEWAL
        if request_data.get('isExpired', None) == '1':
            self.case = self.EXPIRED
        if request_data['isUsedVehicle'] == '1':
            self.case = self.USED
        self.required_time = round(self.required_time, 2)
        if not self.error_code:
            self.error_code = 'SUCCESS'

        super(IntegrationStatistics, self).save(*args, **kwargs)


class MotorPremiumManager(models.Manager):
    '''
    MotorPremium manager class to delete objects who's updated on less than EXPIRY_TIMER
    '''
    def get_queryset(self):
        query_set = super(MotorPremiumManager, self).get_queryset()
        expired_quotes = timezone.now() - relativedelta.relativedelta(days=MotorPremium.EXPIRY_TIMER)
        query_set.filter(updated_on__lte=expired_quotes).delete()
        return query_set


class MotorPremium(models.Model):
    """
    Vehicle premium table to store insurer's quotes response
    """
    EXPIRY_TIMER = 30

    EXPIRED = 'EXPIRED'
    RENEWAL = 'RENEWAL'
    ROLLOVER = 'ROLLOVER'
    NEW_VEHICLE = 'NEW_VEHICLE'

    POLICY_TYPE = ((NEW_VEHICLE, 'New Vehicle'),
                   (ROLLOVER, 'Roll Over'),
                   (RENEWAL, 'Renewal'),
                   (EXPIRED, 'Expired'))

    vehicle = models.ForeignKey(VehicleMaster, related_name='vehicle_premium')
    insurer = models.ForeignKey(Insurer, related_name='insurer_premium')
    response_data = JSONField(_('request_data'), default={})
    zone = models.ForeignKey(RTOMaster, related_name='rto_premium')
    policy_type = models.CharField(choices=POLICY_TYPE, max_length=12)
    updated_on = models.DateTimeField(default=timezone.now)
    max_idv = models.CharField(max_length=10, default='0')
    min_idv = models.CharField(max_length=10, default='0')
    premium = models.CharField(max_length=10, default='0')
    max_age = models.IntegerField(default=0)
    min_age = models.IntegerField(default=0)
    new_ncb = models.CharField(max_length=4, default='')

    objects = MotorPremiumManager()

    def __unicode__(self):
        if self.pk:
            return 'vehicle=%s, insurer=%s, bucket=%s-%s' % (self.vehicle.id, self.insurer.title, self.min_age,
                                                             self.max_age)
        else:
            return 'MotorPremium object'


class CorporateEmployee(models.Model):
    company = models.ForeignKey(core_models.Company, related_name='corporate_employee')
    employee_code = models.CharField(max_length=20)
    employee_name = models.CharField(max_length=50)

