import json
import copy

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import never_cache

import utils
from motor_product import parser_utils
from motor_product.models import Insurer, DiscountLoading, Vehicle, RTOInsurer


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def discountingAdmin(request, vehicle_type, insurer_slug):
    return render_to_response("motor_product/discounting-admin.html",
                              {'insurer_slug': insurer_slug}, context_instance=RequestContext(request)
                              )


def _change_discounting_parameters(discount_loading, parameters):
    make = parameters.get('make', None)
    model = parameters.get('model', None)
    variant = parameters.get('variant', None)
    state = parameters.get('state', None)
    lower_age_limit = parameters.get('lower_age_limit', None)
    upper_age_limit = parameters.get('upper_age_limit', None)
    lower_cc_limit = parameters.get('lower_cc_limit', None)
    upper_cc_limit = parameters.get('upper_cc_limit', None)
    ncb = parameters.get('ncb', None)
    discount = parameters.get('discount', 0.0)

    if (make is not None and make.strip() == ''):
        make = None
    if (model is not None and model.strip() == ''):
        model = None
    if (variant is not None and variant.strip() == ''):
        variant = None

    discount_loading.make = make
    discount_loading.model = model
    discount_loading.variant = variant
    discount_loading.state = state
    discount_loading.ncb = ncb
    discount_loading.lower_age_limit = lower_age_limit
    discount_loading.upper_age_limit = upper_age_limit
    discount_loading.lower_cc_limit = lower_cc_limit
    discount_loading.upper_cc_limit = upper_cc_limit
    discount_loading.discount = discount
    discount_loading.save()

    dl = DiscountLoading.objects.get(id=discount_loading.id)
    return dl


@never_cache
@csrf_exempt
def get_discountings(request, vehicle_type, insurer_slug):
    if request.method == 'POST':
        # post_qdict = copy.deepcopy(request.POST)
        # parameters = parser_utils.qdict_to_dict(post_qdict)

        insurer = Insurer.objects.get(slug=insurer_slug)
        dls = DiscountLoading.objects.filter(insurer=insurer)

        dl_list = [dl.get_dictionary() for dl in dls]
        return HttpResponse(json.dumps({'success': True, 'dls': dl_list}))
    return HttpResponse(json.dumps({'success': True}))


@never_cache
@csrf_exempt
def add_discounting(request, vehicle_type, insurer_slug):
    if request.method == 'POST':
        post_qdict = copy.deepcopy(request.POST)
        parameters = parser_utils.qdict_to_dict(post_qdict)

        insurer = Insurer.objects.get(slug=insurer_slug)
        dl = DiscountLoading()
        dl.insurer = insurer
        dl.save()

        dl_id = parameters.get('id', None)
        if dl_id is not None:
            existingdl = DiscountLoading.objects.get(id=dl_id)
            dl.make = existingdl.make
            dl.model = existingdl.model
            dl.variant = existingdl.variant
            dl.state = existingdl.state
            dl.lower_age_limit = existingdl.lower_age_limit
            dl.upper_age_limit = existingdl.upper_age_limit
            dl.lower_cc_limit = existingdl.lower_cc_limit
            dl.upper_cc_limit = existingdl.upper_cc_limit
            dl.ncb = existingdl.ncb
            dl.discount = existingdl.discount
            for rto in existingdl.rtos.all():
                dl.rtos.add(rto)
            dl.save()
        else:
            dl = _change_discounting_parameters(dl, parameters)
        return HttpResponse(json.dumps({'success': True, 'item': dl.get_dictionary()}))
    return HttpResponse(json.dumps({'success': True}))


@never_cache
@csrf_exempt
def modify_discounting(request, vehicle_type, insurer_slug):
    if request.method == 'POST':
        post_qdict = copy.deepcopy(request.POST)
        parameters = parser_utils.qdict_to_dict(post_qdict)

        dl_id = parameters.get('id', None)
        if dl_id is None:
            return HttpResponse(json.dumps({'success': False}))

        dl = DiscountLoading.objects.get(id=dl_id)
        dl = _change_discounting_parameters(dl, parameters)
        return HttpResponse(json.dumps({'success': True, 'item': dl.get_dictionary()}))
    return HttpResponse(json.dumps({'success': True}))


@never_cache
@csrf_exempt
def delete_discounting(request, vehicle_type, insurer_slug):
    if request.method == 'POST':
        post_qdict = copy.deepcopy(request.POST)
        parameters = parser_utils.qdict_to_dict(post_qdict)

        dl_id = parameters.get('id', None)
        if dl_id is None:
            return HttpResponse(json.dumps({'success': False}))

        dl = DiscountLoading.objects.get(id=dl_id)
        dl.delete()
        return HttpResponse(json.dumps({'success': True}))
    return HttpResponse(json.dumps({'success': True}))


@never_cache
@csrf_exempt
def get_vehicles(request, vehicle_type, insurer_slug):
    if request.method == 'POST':
        post_qdict = copy.deepcopy(request.POST)
        parameters = parser_utils.qdict_to_dict(post_qdict)

        vehicles = Vehicle.objects.filter(insurer__slug=insurer_slug)
        make = parameters.get('make', None)

        if not make:
            make_list = set()
            for vehicle in vehicles:
                make_list.add(vehicle.make)
            return HttpResponse(json.dumps({'success': True, 'data': list(make_list)}))
        else:
            model = parameters.get('model', None)

            if not model:
                model_list = set()
                for vehicle in vehicles.filter(make=make):
                    model_list.add(vehicle.model)
                return HttpResponse(json.dumps({'success': True, 'data': list(model_list)}))
            else:
                variant_list = set()
                for vehicle in vehicles.filter(make=make, model=model):
                    variant_list.add(vehicle.variant)
                return HttpResponse(json.dumps({'success': True, 'data': list(variant_list)}))
    return HttpResponse(json.dumps({'success': True}))


@never_cache
@csrf_exempt
def get_rtos(request, vehicle_type, insurer_slug):
    if request.method == 'POST':
        rtos = RTOInsurer.objects.filter(insurer__slug=insurer_slug)
        rto_list = [rto.get_dictionary() for rto in rtos]
        return HttpResponse(json.dumps({'success': True, 'data': rto_list}))


@never_cache
@csrf_exempt
def get_rtos_for_discount_loading(request, vehicle_type, insurer_slug):
    if request.method == 'POST':
        post_qdict = copy.deepcopy(request.POST)
        parameters = parser_utils.qdict_to_dict(post_qdict)

        dl_id = parameters.get('id', None)
        if dl_id is None:
            return HttpResponse(json.dumps({'success': False}))

        dl = DiscountLoading.objects.get(id=dl_id)
        rtos = dl.rtos.all()
        rto_list = [rto.id for rto in rtos]
        return HttpResponse(json.dumps({'success': True, 'data': rto_list}))
    return HttpResponse(json.dumps({'success': True}))


@never_cache
@csrf_exempt
def modify_rtos_for_discount_loading(request, vehicle_type, insurer_slug):
    if request.method == 'POST':
        post_qdict = copy.deepcopy(request.POST)
        parameters = parser_utils.qdict_to_dict(post_qdict)

        rto_list = parameters.get('rtos[]', None)
        is_delete = parameters.get('delete', 0)
        dl_id = parameters.get('id', None)
        if dl_id is None or rto_list is None:
            return HttpResponse(json.dumps({'success': False}))

        dl = DiscountLoading.objects.get(id=dl_id)
        for rto in rto_list:
            if int(is_delete):
                dl.rtos.remove(rto)
            else:
                dl.rtos.add(rto)
        return HttpResponse(json.dumps({'success': True}))
    return HttpResponse(json.dumps({'success': True}))
