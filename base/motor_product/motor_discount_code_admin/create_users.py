import re
import datetime
import os
from core.models import User, Email

MONTH_MAP = dict(map((lambda e: (e[1], e[0])), enumerate(
    ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'], start=1)))


def parse_date(_str):
    if not _str:
        return
    ddmmyy = re.match(r'([0-9]{1,2})[-/ ]([0-9]{2})[-/ ]([0-9]{2})', _str)
    ddmmYY = re.match(r'([0-9]{1,2})[-/ ]([0-9]{2})[-/ ]([0-9]{4})', _str)
    ddMMyy = re.match(r'([0-9]{1,2})[-/ ]([A-Za-z]{3})[-/ ]([0-9]{2})', _str)
    ddMMYY = re.match(r'([0-9]{1,2})[-/ ]([A-Za-z]{3})[-/ ]([0-9]{4})', _str)
    if ddmmyy:
        day, month, year = map((lambda e: int(e)), ddmmyy.groups())
        year += 1900
    elif ddmmYY:
        day, month, year = map((lambda e: int(e)), ddmmYY.groups())
    elif ddMMyy:
        day = int(ddMMyy.groups()[0])
        month = MONTH_MAP[ddMMyy.groups()[1].upper()]
        year = 1900 + int(ddMMyy.groups()[2])
    elif ddMMYY:
        day = int(ddMMYY.groups()[0])
        month = MONTH_MAP[ddMMYY.groups()[1].upper()]
        year = int(ddMMYY.groups()[2])
    else:
        print "-----------------------------------------------------------------Invalid date %s" % _str
        return
    return datetime.date(year=year, month=month, day=day)


def create_one_user(user_dictionary):
    user = None
    created = False
    email = user_dictionary.pop('email')
    username = user_dictionary.pop('username')
    try:
        email_obj, _ = Email.objects.get_or_create(email=email.lower(),
                                                   defaults={'is_primary': True, 'is_exists': True, 'is_verified': True})
        if not email_obj.user:
            user = User(**user_dictionary)
            user.save()
            email_obj.user = user
            email_obj.save()
            user.set_password(user_dictionary['password'])
            created = True
            print 'Created user ' + username
    except:
        print 'Error creating ' + username, email
        user = None

    if not created and user:
        for key, value in user_dictionary.items():
            setattr(user, key, value)
        user.set_password(user_dictionary['password'])
        try:
            user.save()
            print 'Saved user ' + username
        except:
            print 'Error saving already created user ' + username, email

    return created


def read_file_create_list(_file, company="gaadi"):
    """
    line format should be:
    Employee ID        First Name        Last Name        Mobile Number        Date of Birth        Personal Email ID        Official Email ID
    00000000000        1111111111        222222222       33333333333333         444444444           55555555555555           666666666666666
    """
    _list_of_users = []
    for line in _file.readlines()[1:]:
        user_info = map((lambda e: e.strip()), line[:-1].split('|'))
        dob = parse_date(user_info[4])
        if not dob:
            dob = parse_date("10/03/1990")
        email = user_info[5]
        if not email:
            email = user_info[6]
            if not email:
                email = "%s@temporary.com" % user_info[0]
        _list_of_users.append({
            'username': user_info[0],
            'first_name': user_info[1],
            'last_name': user_info[2],
            'email': email,
            'birth_date': dob,
            'organization': company,
            'password': dob.strftime("%d%m%Y"),
            'extra': {
                'mobile': user_info[3],
                'personal_email': user_info[5],
                'official_email': user_info[6],
                'corporate': True,
            }
        })
    return _list_of_users


def create_users(file_address="motor_product/motor_discount_code_admin/data/gaadi_employee.csv"):
    _file = open(file_address, 'r')
    company = os.path.basename(file_address).split("_")[0].upper()
    corp_users = open('/tmp/corporate_users.csv', 'w+')
    corp_users.write('"First name", "Email", "Date of Birth", "Employee id"\n')
    for u in read_file_create_list(_file, company=company):
        if create_one_user(u):
            corp_users.write('"%s","%s","%s","%s","%s"\n'
                             % (u['first_name'], u['email'], u['birth_date'], u['username'], u['password']))
    corp_users.close()
