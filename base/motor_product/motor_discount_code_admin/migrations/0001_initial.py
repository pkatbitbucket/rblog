# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CorporateLeads',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('pyp_expiry', models.DateField(blank=True)),
                ('extra', jsonfield.fields.JSONField(default={}, blank=True)),
                ('created_on', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
                ('updated_on', models.DateTimeField(
                    auto_now=True, verbose_name='modified')),
            ],
        ),
        migrations.CreateModel(
            name='CorporateUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('employee_id', models.CharField(
                    max_length=200, verbose_name='employee_id')),
                ('email', models.EmailField(max_length=254, verbose_name='Email')),
                ('mobile', models.CharField(max_length=100, verbose_name='mobile')),
                ('organization', models.CharField(
                    max_length=200, verbose_name='organization')),
                ('extra', jsonfield.fields.JSONField(
                    default={}, verbose_name='extra', blank=True)),
                ('status', models.IntegerField(default=3, verbose_name='status', choices=[
                 (1, b'Bought'), (2, b'Yet To Buy'), (3, b'Never Visited')])),
                ('created_on', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
                ('updated_on', models.DateTimeField(
                    auto_now=True, verbose_name='modified')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
