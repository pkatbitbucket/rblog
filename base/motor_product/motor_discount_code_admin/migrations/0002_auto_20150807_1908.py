# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('motor_product', '0001_initial'),
        ('motor_discount_code_admin', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='corporateleads',
            name='discount_code',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL,
                                    blank=True, to='motor_product.DiscountCode', null=True),
        ),
        migrations.AddField(
            model_name='corporateleads',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='corporateleads',
            name='vehicle',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL,
                                    blank=True, to='motor_product.VehicleMaster', null=True),
        ),
    ]
