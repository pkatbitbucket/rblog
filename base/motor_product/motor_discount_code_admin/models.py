from django.db import models
from django.utils.translation import ugettext_lazy as _
from jsonfield import JSONField
from core.models import User
from motor_product.models import VehicleMaster, DiscountCode


class CorporateUser(models.Model):
    STATUS_CHOICES = (
        (1, "Bought"),
        (2, "Yet To Buy"),
        (3, "Never Visited"),
    )
    employee_id = models.CharField(_('employee_id'), max_length=200)
    email = models.EmailField(_("Email"))
    mobile = models.CharField(_("mobile"), max_length=100)
    organization = models.CharField(_('organization'), max_length=200)
    user = models.ForeignKey(User)
    extra = JSONField(_('extra'), blank=True, default={})
    status = models.IntegerField(
        _('status'), choices=STATUS_CHOICES, default=3)
    created_on = models.DateTimeField(_('created'), auto_now_add=True)
    updated_on = models.DateTimeField(_('modified'), auto_now=True)

    def __getattr__(self, key):
        try:
            super(CorporateUser, self).__getattr__(key)
        except:
            try:
                return self.extra[key]
            except:
                raise AttributeError


class CorporateLeads(models.Model):
    user = models.ForeignKey(User)
    vehicle = models.ForeignKey(
        VehicleMaster, blank=True, null=True, on_delete=models.SET_NULL)
    pyp_expiry = models.DateField(blank=True)
    extra = JSONField(blank=True, default={})
    discount_code = models.ForeignKey(
        DiscountCode, blank=True, null=True, on_delete=models.SET_NULL)
    created_on = models.DateTimeField(_('created'), auto_now_add=True)
    updated_on = models.DateTimeField(_('modified'), auto_now=True)

    def get_dictionary(self):
        model = None
        variant = None
        vehicle_type = None
        fuel_type = self.extra.get('fuel_type', None)
        if self.vehicle:
            model = {'id': self.vehicle.model.id,
                     'name': self.vehicle.make.name + ' ' + self.vehicle.model.name}
            variant = {'id': self.vehicle.id, 'name': self.vehicle.variant}
            vehicle_type = self.vehicle.vehicle_type

        return {
            'id': self.id,
            'vehicle_type': vehicle_type,
            'model': model,
            'variant': variant,
            'fuel_type': fuel_type,
            'past_policy_expiry_date': self.pyp_expiry.strftime("%d-%m-%Y"),
            'upload_url': self.extra.get('upload', None)
        }
