# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CorporateLeads'
        db.create_table(u'motor_discount_code_admin_corporateleads', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(
                to=orm['core.User'])),
            ('vehicle', self.gf('django.db.models.fields.related.ForeignKey')(to=orm[
             'motor_product.VehicleMaster'], null=True, on_delete=models.SET_NULL, blank=True)),
            ('pyp_expiry', self.gf('django.db.models.fields.DateField')(blank=True)),
            ('extra', self.gf('jsonfield.fields.JSONField')(default={}, blank=True)),
            ('created_on', self.gf('django.db.models.fields.DateTimeField')(
                auto_now_add=True, blank=True)),
            ('updated_on', self.gf('django.db.models.fields.DateTimeField')(
                auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'motor_discount_code_admin', ['CorporateLeads'])

    def backwards(self, orm):
        # Deleting model 'CorporateLeads'
        db.delete_table(u'motor_discount_code_admin_corporateleads')

    models = {
        u'core.user': {
            'Meta': {'object_name': 'User'},
            'birth_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '100', 'blank': 'True'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'extra': ('jsonfield.fields.JSONField', [], {'default': '{}', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'gender': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_verified': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'mugshot': ('customdb.thumbs.ImageWithThumbsField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'organization': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'roles': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['core.UserRole']", 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '70'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '36', 'blank': 'True'}),
            'token_updated_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'updated_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'})
        },
        u'core.userrole': {
            'Meta': {'object_name': 'UserRole'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'default': "'CUSTOMER'", 'max_length': '50'})
        },
        u'motor_discount_code_admin.corporateleads': {
            'Meta': {'object_name': 'CorporateLeads'},
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'extra': ('jsonfield.fields.JSONField', [], {'default': '{}', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pyp_expiry': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'updated_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.User']"}),
            'vehicle': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['motor_product.VehicleMaster']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'})
        },
        u'motor_discount_code_admin.corporateuser': {
            'Meta': {'object_name': 'CorporateUser'},
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'employee_id': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'extra': ('jsonfield.fields.JSONField', [], {'default': '{}', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mobile': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'organization': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '3'}),
            'updated_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.User']"})
        },
        u'motor_product.insurer': {
            'Meta': {'ordering': "('-created_on',)", 'object_name': 'Insurer'},
            'body': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'disable_cache_for_car': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'disable_cache_for_twowheeler': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active_for_car': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_active_for_twowheeler': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'logo': ('customdb.thumbs.ImageWithThumbsField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '70'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'tease': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'updated_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'motor_product.make': {
            'Meta': {'object_name': 'Make'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'})
        },
        u'motor_product.model': {
            'Meta': {'object_name': 'Model'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'make': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['motor_product.Make']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'vehicle_type': ('django.db.models.fields.CharField', [], {'default': "'Private Car'", 'max_length': '25'})
        },
        u'motor_product.vehicle': {
            'Meta': {'object_name': 'Vehicle'},
            'base_rate_discount': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'cc': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ex_showroom_price': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'fuel_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'insurer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['motor_product.Insurer']", 'null': 'True', 'blank': 'True'}),
            'make': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'make_code': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'model_code': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'number_of_wheels': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'processing_level': ('django.db.models.fields.CharField', [], {'default': "'unprocessed'", 'max_length': '20'}),
            'raw_data': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'risk_based_discount': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'seating_capacity': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'updated_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'variant': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'variant_code': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'vehicle_code': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'vehicle_segment': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'vehicle_type': ('django.db.models.fields.CharField', [], {'default': "'Private Car'", 'max_length': '25'})
        },
        u'motor_product.vehiclemapping': {
            'Meta': {'unique_together': "(('master_vehicle', 'insurer'),)", 'object_name': 'VehicleMapping'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'insurer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['motor_product.Insurer']", 'null': 'True', 'blank': 'True'}),
            'mapped_vehicle': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['motor_product.Vehicle']", 'null': 'True', 'blank': 'True'}),
            'master_vehicle': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['motor_product.VehicleMaster']", 'null': 'True', 'blank': 'True'})
        },
        u'motor_product.vehiclemaster': {
            'Meta': {'object_name': 'VehicleMaster'},
            'cc': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'ex_showroom_price': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'fuel_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'make': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['motor_product.Make']", 'null': 'True', 'blank': 'True'}),
            'model': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['motor_product.Model']", 'null': 'True', 'blank': 'True'}),
            'seating_capacity': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sub_vehicles': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['motor_product.Vehicle']", 'through': u"orm['motor_product.VehicleMapping']", 'symmetrical': 'False'}),
            'variant': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'vehicle_segment': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'vehicle_type': ('django.db.models.fields.CharField', [], {'default': "'Private Car'", 'max_length': '25'})
        }
    }

    complete_apps = ['motor_discount_code_admin']
