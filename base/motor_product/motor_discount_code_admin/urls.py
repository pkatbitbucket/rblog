from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^user/$', views.corporate_home, name='corporate_home'),
    url(r'^login/$', views.corporate_login, name='corporate_login'),
    url(r'^logout/$', views.corporate_logout, name='corporate_logout'),
    url(r'^change-details/$', views.corporate_change_details, name='corporate_change_details'),
    url(r'^leads/$', views.corporate_leads, name='corporate_leads'),
    url(r'^leads/(?P<operation>[\w-]+)/$', views.corporate_leads, name='corporate_leads'),
    url(r'^upload-policy/$', views.corporate_policy_upload, name='corporate_policy_upload'),
    url(r'^quotes/$', views.corporate_discount_code, name='corporate_discount_code'),
    url(r'^manage/$', views.corporate_transaction_manage, name='corporate_transaction_manage'),
    url(r'^report/$', views.corporate_leads_reporting, name='corporate_report'),
    url(r'^report/(?P<report_type>[\w-]+)/$', views.corporate_leads_reporting, name='corporate_report_menu'),
    url(r'^report/(?P<report_type>[\w-]+)/(?P<view_type>[\w-]+)/$',
        views.corporate_leads_reporting, name='corporate_report_view'),
]
