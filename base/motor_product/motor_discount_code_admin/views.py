import collections
import json
import os
import copy
from django.core.files.storage import default_storage
import utils
import putils
import datetime

from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404, render
from django.views.decorators.cache import never_cache
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.core.exceptions import PermissionDenied

from motor_product.models import DiscountCode, VehicleMaster
from motor_product import parser_utils
from motor_product.insurers import insurer_utils
from motor_product.motor_discount_code_admin.models import CorporateLeads
from core.models import Email
import settings

DISCOUNT_APPLICABLE_INSURERS = ['oriental']


def logout_on_denial(view):
    def inner(request, *args, **kwargs):
        try:
            resp = view(request, *args, **kwargs)
            return resp
        except PermissionDenied:
            logout(request)
            return HttpResponseRedirect(reverse('corporate_report'))

    return inner


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def manage_discount_code(request, discount_code):
    code = get_object_or_404(DiscountCode, code=discount_code)
    if request.method == 'POST':
        post_qdict = copy.deepcopy(request.POST)
        post_parameters = parser_utils.qdict_to_dict(post_qdict)

        quoteUrl = post_parameters.get('quoteUrl', None)
        quoteId = post_parameters.get('quoteId', None)

        if not quoteUrl or not quoteId:
            return HttpResponse(json.dumps({'success': False}))

        discount_raw = {}
        discount_raw['quoteId'] = quoteId
        discount_raw['quoteUrl'] = quoteUrl

        for insurer in DISCOUNT_APPLICABLE_INSURERS:
            discount = post_parameters.get(insurer, None)
            try:
                discount = int(discount)
                if (discount > -50) and (discount < 50):
                    discount_raw[insurer] = {}
                    discount_raw[insurer]['discount'] = discount
            except:
                pass

        code.raw = discount_raw
        code.save()

        return HttpResponse(json.dumps({'success': True}))

    inputs = {}
    for insurer in DISCOUNT_APPLICABLE_INSURERS:
        inputs[insurer] = code.raw.get(insurer, {}).get('discount', 0)

    quote_url = ''
    quote_modified_url = ''
    is_linked = False
    if code.raw.get('quoteId', None):
        is_linked = True
        quote_url = code.raw.get('quoteUrl', '')

    is_expired = code.is_expired()

    if quote_url:
        parts = quote_url.split('?')
        if len(parts) < 1:
            quote_modified_url = parts[0] + '?dc=%s' % code.code
        else:
            quote_modified_url = quote_url + '&dc=%s' % code.code

    return render(request, "motor_product/motor_discount_code_admin/discount_code.html", {
        'code': code,
        'inputs': inputs,
        'quote_url': quote_url,
        'quote_modified_url': quote_modified_url,
        'is_linked': is_linked,
        'is_expired': is_expired,
    })


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def generate_discount_code(request):
    if request.method == 'POST':
        try:
            dc = DiscountCode()
            dc.save()
            return HttpResponse(json.dumps({'success': True, 'code': dc.code}))
        except:
            return HttpResponse(json.dumps({'success': False}))

    start_date = datetime.datetime.now() - datetime.timedelta(days=3)
    codes = DiscountCode.objects.filter(
        created_on__gte=start_date, discount_type=1)
    return render(request, "motor_product/motor_discount_code_admin/generate_code.html", {'codes': codes})


@login_required
def corporate_home(request):
    user = request.user
    if not user.extra.get('corporate', False):
        raise Http404

    if request.method == "GET":
        return render(request, "motor_product/motor_discount_code_admin/corporate_home.html", {
            "first_name": user.first_name if user.first_name else user.email,
            "mobile": user.extra.get('mobile', ''),
            "email": user.email,
            "username": user.email,
        })


@login_required
def corporate_leads(request, operation=None):
    user = request.user
    if request.method == "POST":
        try:
            if not operation:
                leads = []
                for lead in user.corporateleads_set.all():
                    leads.append(lead.get_dictionary())
                return HttpResponse(json.dumps({'success': True, 'data': leads}))

            elif operation == "add":
                vehicleId = request.POST['vehicleId']
                pypExpiryDate = request.POST['pastPolicyExpiryDate']
                fuel_type = json.loads(request.POST['fuelType'])

                pastPolicyExpiryDate = datetime.datetime.strptime(
                    pypExpiryDate, '%d-%m-%Y')
                vehicle = VehicleMaster.objects.get(id=vehicleId)

                lead = CorporateLeads()
                lead.vehicle = vehicle
                lead.user = user
                lead.pyp_expiry = pastPolicyExpiryDate
                lead.extra['fuel_type'] = fuel_type
                lead.save()
                return HttpResponse(json.dumps({'success': True, 'data': lead.get_dictionary()}))

            elif operation == "delete":
                leadId = request.POST['leadId']
                lead = CorporateLeads.objects.get(id=leadId)
                if lead.user == user:

                    # Check if lead is closed offline.
                    if lead.extra.get('closed_offline', False):
                        return HttpResponse(json.dumps({'success': False}))

                    # Check for completed transaction lead.
                    if (lead.discount_code and lead.discount_code.transaction and lead.discount_code.transaction.status == 'COMPLETED'):
                        return HttpResponse(json.dumps({'success': False}))

                    lead.delete()
                    return HttpResponse(json.dumps({'success': True}))
        except:
            pass
        return HttpResponse(json.dumps({'success': False}))
    return HttpResponse(json.dumps({'success': True}))


@login_required
def corporate_change_details(request):
    user = request.user
    if not user.extra.get('corporate', False):
        raise Http404

    if request.method == "POST":
        if request.POST.get('email', None):
            email_obj = Email.objects.get(email=user.email)
            email_obj.email = request.POST['email']
            email_obj.save()

        if request.POST.get('mobile', None):
            user.extra['mobile'] = request.POST['mobile']

        if request.POST.get('password', None):
            user.set_password(request.POST['password'])

        try:
            user.save()
        except:
            return HttpResponse(json.dumps({"success": False}))
        return HttpResponse(json.dumps({"success": True}))

    raise Http404


def corporate_logout(request):
    logout(request)
    return HttpResponseRedirect('/cardekho/login/')


def corporate_login(request):

    if request.method == "POST":
        logout(request)
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None and user.extra.get('corporate', False):
            if user.is_active:
                login(request, user)
                response = HttpResponse(json.dumps({'success': True, 'data': {
                                        'username': username, 'email': user.email, 'mobile': user.extra.get('mobile', None)}}))
                response.set_cookie('affiliateSource', 'cardekho')
                response.set_cookie('network', 'cardekho')
                response.set_cookie('category', 'employee_login')
                response.set_cookie('partner', 'cardekho')
                return response
            else:
                return HttpResponse(json.dumps({'success': False, 'error': 'Account disabled for the user.'}))
        else:
            return HttpResponse(json.dumps({'success': False, 'error': 'Invalid login credentials.'}))
    else:
        user = request.user
        if user.is_authenticated():
            if user.extra.get('corporate', False):
                return HttpResponseRedirect('/cardekho/user/')

            # User not a part of corporate logins
            logout(request)

        return render(request, "motor_product/motor_discount_code_admin/corporate_login.html")


@login_required
def corporate_policy_upload(request):
    if request.method == "POST":
        leadId = request.POST['leadId']
        lead = CorporateLeads.objects.get(id=leadId)
        if lead.user == request.user:
            try:
                uploaded_file = request.FILES['policy_file']
                filename = str(lead.id) + '_' + \
                    str(lead.user.email) + ".pdf"
                upload_path = os.path.join('uploads', 'corporateuser', filename)
                with default_storage.open(upload_path, 'wb') as f:
                    for each in uploaded_file.chunks():
                        f.write(each)
                upload_url = default_storage.url(upload_path)
                lead.extra['upload'] = upload_url
                lead.save()
                message = {'success': True, 'data': {
                    'url': upload_url, 'id': lead.id}}
            except:
                message = {'success': False}
            return HttpResponse(
                """<script type='text/javascript'>window.top.postMessage(%s,'*')</script>""" % (json.dumps(message))
            )
    raise Http404


@login_required
def corporate_discount_code(request):
    if request.method == "POST":
        leadId = request.POST['leadId']
        lead = CorporateLeads.objects.get(id=leadId)
        if lead.user == request.user:
            dc = lead.discount_code
            if not dc:
                dc = DiscountCode()
                dc.discount_type = 2

                # For now cardekho discount is fixed
                dc.raw = {'l-t': {'discount': -5},
                          'universal-sompo': {'discount': -5}}
                dc.save()

                lead.discount_code = dc
                lead.save()

            redirect_url = '/motor/car-insurance/#corporate'
            if lead.vehicle.vehicle_type == 'Twowheeler':
                redirect_url = '/motor/twowheeler-insurance/#corporate'

            resp = HttpResponse(json.dumps(
                {'success': True, 'data': redirect_url}))
            resp.set_cookie('discountCode', dc.code)
            return resp
        return HttpResponse(json.dumps({'success': False}))
    raise Http404


@login_required
@logout_on_denial
@utils.profile_type_only('ADMIN', 'REPORT', 'THIRDPARTY')
def corporate_leads_reporting(request, report_type=None, view_type=None):
    if not report_type and not view_type:
        return render(request, 'motor_product/motor_discount_code_admin/corporate_report.html', {'main': True})

    users = collections.OrderedDict([])
    leads = []
    for lead in CorporateLeads.objects.all():
        lstatus = 'Lead'
        transaction = None

        transaction_date = ''
        transaction_premium = 0
        transaction_od_premium = 0
        transaction_type = ''

        if lead.extra.get('closed_offline', False):
            lstatus = 'Transaction Done'
            transaction_date = lead.extra.get('transaction_date', '-')
            transaction_premium = lead.extra.get('premium_paid', 0)
            transaction_od_premium = lead.extra.get('od_premium', 0)
            transaction_type = 'Offline'

        elif lead.discount_code:
            lstatus = 'Quotation'

            dc = lead.discount_code
            if dc.transaction:
                lstatus = 'Proposal'

                if dc.transaction.status == 'COMPLETED':
                    lstatus = 'Transaction Done'
                    transaction = dc.transaction

                    transaction_date = transaction.updated_on.strftime(
                        '%d-%m-%Y')
                    transaction_premium = transaction.premium_paid
                    transaction_od_premium = insurer_utils.calculate_od_premium(
                        transaction)
                    transaction_type = 'Online'

        ld = collections.OrderedDict([
            ('pastPolicyExpiryDate', lead.pyp_expiry.strftime('%d-%m-%Y')),
            ('vehicleType', lead.vehicle.vehicle_type),
            ('employeeId', lead.user.email),
            ('name', lead.user.first_name + ' ' + lead.user.last_name),
            ('email', lead.user.email),
            ('mobile', lead.user.extra['mobile']),
            ('status', lstatus),
            ('transactionDate',  transaction_date),
            ('transactionType', transaction_type),
            ('odpremium', float(transaction_od_premium)),
            ('premium', float(transaction_premium)),
        ])

        leads.append(ld)

        ag = users.get(lead.user.email, None)
        if not ag:
            ag = collections.OrderedDict([
                ('employeeId', lead.user.email),
                ('name', lead.user.first_name + ' ' + lead.user.last_name),
                ('email', lead.user.email),
                ('mobile', lead.user.extra['mobile']),
                ('loggedIn', 'true'),
                ('car', 0),
                ('bike', 0),
                ('total', 0),
                ('quote', 0),
                ('proposal', 0),
                ('transaction', 0),
                ('total_od_premium', 0),
                ('total_premium', 0),
            ])

        if ld['vehicleType'] == 'Twowheeler':
            ag['bike'] += 1
        else:
            ag['car'] += 1

        ag['total'] = ag['bike'] + ag['car']

        if lstatus == 'Quotation':
            ag['quote'] += 1
        elif lstatus == 'Proposal':
            ag['proposal'] += 1
        elif lstatus == 'Transaction Done':
            ag['transaction'] += 1

        ag['total_od_premium'] += ld['odpremium']
        ag['total_premium'] += ld['premium']

        users[lead.user.email] = ag

    leads = sorted(leads, key=lambda x: {'Lead': 4, 'Quotation': 3, 'Proposal': 2, 'Transaction Done': 1}[x['status']])
    if report_type == "aggregate":
        other_users = Email.objects.filter(
            user__organization__in=['GAADI', 'GIRNARSOFT']).exclude(email__in=users.keys())

        for user_email in other_users:
            user = user_email.user
            ag = collections.OrderedDict([
                ('employeeId', user.username),
                ('name', user.first_name + ' ' + user.last_name),
                ('email', user.email),
                ('mobile', user.extra['mobile']),
                ('loggedIn', 'false' if (user.created_on == user.last_login) else 'true'),
                ('car', 0),
                ('bike', 0),
                ('total', 0),
                ('quote', 0),
                ('proposal', 0),
                ('transaction', 0),
                ('total_od_premium', 0),
                ('total_premium', 0),
            ])

            users[user.email] = ag

        objs = users.values()
        theaders = ['EmployeeId', 'Name', 'Email', 'Mobile', 'Login Done', 'Car', 'Bike',
                    'Total', 'Quote', 'Proposal', 'Transaction Done', 'Total OD Premium', 'Total Premium']
    elif report_type == "leads":
        objs = leads
        theaders = ['Expiry Date', 'Vehicle Type', 'EmployeeId', 'Name', 'Email', 'Mobile',
                    'Status', 'Transaction Date', 'Transaction Type', 'OD Premium', 'Premium']
    else:
        raise Http404

    rows = []
    for obj in objs:
        row = []
        for key, value in obj.items():
            row.append(value)
        rows.append(row)

    if view_type == "download":
        file_title = 'Corporate_Report_%s_' % (report_type)
        file_name = '%s_%s.xls' % (
            file_title, datetime.datetime.now().strftime("%d-%b_%H%M"))
        return putils.render_excel(file_name, theaders, rows)
    return render(request, 'motor_product/motor_discount_code_admin/corporate_report.html', {'main': False, 'objs': objs, 'theaders': theaders, 'report_type': report_type})


@utils.profile_type_only('ADMIN', 'REPORT')
def corporate_transaction_manage(request):

    if request.method == "POST":
        lead_id = request.POST['leadId']
        operation = request.POST['operation']

        lead = CorporateLeads.objects.get(id=lead_id)

        if operation == "detach":
            del lead.extra['closed_offline']
            del lead.extra['transaction_date']
            del lead.extra['premium_paid']
            del lead.extra['od_premium']
            lead.save()

            return HttpResponse(json.dumps({'success': True}))

        elif operation == "attach":
            premium_paid = request.POST['premium_paid']
            od_premium = request.POST['od_premium']
            transaction_date = request.POST['transaction_date']

            lead.extra['closed_offline'] = True
            lead.extra['premium_paid'] = float(premium_paid)
            lead.extra['od_premium'] = float(od_premium)
            lead.extra['transaction_date'] = transaction_date
            lead.save()

            return HttpResponse(json.dumps({'success': True}))

        return HttpResponse(json.dumps({'success': False}))

    query = request.GET.get('query', '')
    lead_email = Email.objects.filter(email=query).first()
    lead_objs = CorporateLeads.objects.filter(user=lead_email.user)
    leads = []
    for lead in lead_objs:
        lstatus = 'Lead'
        transaction = None

        transaction_date = ''
        transaction_premium = 0
        transaction_od_premium = 0
        transaction_type = ''

        if lead.extra.get('closed_offline', False):
            lstatus = 'Transaction Done'
            transaction_date = lead.extra.get('transaction_date', '-')
            transaction_premium = lead.extra.get('premium_paid', 0)
            transaction_od_premium = lead.extra.get('od_premium', 0)
            transaction_type = 'Offline'

        elif lead.discount_code:
            lstatus = 'Quotation'

            dc = lead.discount_code
            if dc.transaction:
                lstatus = 'Proposal'

                if dc.transaction.status == 'COMPLETED':
                    lstatus = 'Transaction Done'
                    transaction = dc.transaction

                    transaction_date = transaction.updated_on.strftime(
                        '%d-%m-%Y')
                    transaction_premium = transaction.premium_paid
                    transaction_od_premium = insurer_utils.calculate_od_premium(
                        transaction)
                    transaction_type = 'Online'

        ld = collections.OrderedDict([
            ('id', lead.id),
            ('employeeId', lead.user.email),
            ('name', lead.user.first_name + ' ' + lead.user.last_name),
            ('email', lead.user.email),
            ('mobile', lead.user.extra['mobile']),
            ('pastPolicyExpiryDate', lead.pyp_expiry.strftime('%d-%m-%Y')),
            ('vehicle', str(lead.vehicle.make.name) + ' ' +
             str(lead.vehicle.model.name) + ' ' + str(lead.vehicle.variant)),
            ('vehicleType', lead.vehicle.vehicle_type),
            ('status', lstatus),
            ('transactionDate',  transaction_date),
            ('transactionType', transaction_type),
            ('transactionId', transaction.transaction_id if transaction else ''),
            ('odpremium', float(transaction_od_premium)),
            ('premium', float(transaction_premium)),
        ])

        leads.append(ld)

    theaders = ['Id', 'EmployeeId', 'Name', 'Email', 'Mobile', 'Expiry Date', 'Vehicle', 'Vehicle Type',
                'Status', 'Transaction Date', 'Transaction Type', 'Transaction Id', 'OD Premium', 'Premium']
    return render(request, 'motor_product/motor_discount_code_admin/corporate_manage.html', {'theaders': theaders, 'leads': leads, 'query': query})
