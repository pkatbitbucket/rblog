import settings
import json
import requests
import utils
import string
import random

from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render
from django.views.decorators.cache import never_cache
from django.core.cache import cache

from core.models import User

GARAGE_CACHE_TIMEOUT = 3600 * 24 * 31


def _id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


@never_cache
def garage_list(request, list_id=None):
    is_user_admin = False
    if utils.user_has_any_group(request.user, ['REPORT', 'MOTOR_CAT', 'THIRDPARTY']):
        is_user_admin = True

    ngarages = []

    if is_user_admin and not list_id:
        new_tag = _id_generator()
        return HttpResponseRedirect("/motor/garages/%s/" % new_tag)

    if not is_user_admin and not list_id:
        raise Http404

    selected_garage_list = cache.get('garage_list_' + list_id)
    if selected_garage_list is None:
        selected_garage_list = []

    query = request.GET.get("query", None)
    if is_user_admin:
        search_url = settings.ELASTIC_SEARCH_URL.rstrip(
            "/") + "/motor/garages/_search?size=100"
        if query:
            search_url += '&q=' + query

        print search_url
        response = requests.get(search_url)
        if response.status_code == 200:
            garages = response.json().get('hits', {}).get('hits', [])

            for garage in garages:
                ngarage = garage['_source']
                ngarage['id'] = garage['_id']
                ngarage['score'] = garage['_score']
                ngarage[
                    'url'] = '/motor/garages/%s/modify/%s/' % (list_id, ngarage['id'])
                ngarage['selected'] = False
                if ngarage['id'] in selected_garage_list:
                    ngarage['selected'] = True
                    ngarage['url'] += '?add=0'
                ngarages.append(ngarage)

    search_url = settings.ELASTIC_SEARCH_URL.rstrip(
        "/") + "/motor/garages/_search"
    id_query = {'query': {'ids': {'values': selected_garage_list}}}
    response = requests.get(search_url, data=json.dumps(id_query))

    if response.status_code == 200:
        garages = response.json().get('hits', {}).get('hits', [])

        for garage in garages:
            ngarage = garage['_source']
            ngarage['id'] = garage['_id']
            ngarage['score'] = garage['_score']

            if is_user_admin:
                ngarage[
                    'url'] = '/motor/garages/%s/modify/%s/?add=0' % (list_id, ngarage['id'])
            else:
                ngarage['url'] = '#'

            ngarage['selected'] = is_user_admin
            ngarages.append(ngarage)

    garage_data = {
        'garages': ngarages,
        'query': query if query else '',
        'list_id': list_id,
        'is_user_admin': is_user_admin,
    }
    return render(request, "motor_product/motor_garages/garages.html", garage_data)


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT', 'MOTOR_CAT')
def garage_modify(request, list_id, garage_id):
    add = bool(int(request.GET.get('add', 1)))
    key_id = 'garage_list_' + list_id

    if key_id and garage_id:
        garage_list = cache.get(key_id)

        if garage_list is None:
            garage_list = []

        if add:
            if garage_id not in garage_list:
                garage_list.append(garage_id)
                cache.set(key_id, garage_list, GARAGE_CACHE_TIMEOUT)
        else:
            if garage_id in garage_list:
                garage_list.remove(garage_id)
                cache.set(key_id, garage_list, GARAGE_CACHE_TIMEOUT)

    return HttpResponse(json.dumps({"success": True}))
