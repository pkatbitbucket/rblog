from django.contrib import admin
from .models import OfflineLeads


class OfflineLeadsAdmin(admin.ModelAdmin):
    list_display = ('member_id', 'email', 'owner')

    def owner(self, obj):
        return obj.user if not obj.user else obj.user.email

    def email(self, obj):
        return obj.extra['email']

admin.site.register(OfflineLeads, OfflineLeadsAdmin)
