from motor_product.motor_offline_leads.models import OfflineLeads, OfflineLeadsSearch
from django import forms
import datetime
import django


temp_date = datetime.datetime(year=1800, month=1, day=1)


def invalid(dobj):
    return (dobj.year < 1900)


class SEARCHForm(forms.ModelForm):
    inputquery = forms.CharField(
        label="Comma seperated member_id's", widget=forms.TextInput(attrs={'class': 'query'}))

    def clean(self):
        _self = super(SEARCHForm, self).clean()
        _self['inputquery'] = _self['inputquery'].replace(' ', '')
        return _self

    class Meta:
        model = OfflineLeadsSearch
        if django.VERSION > (1, 7):
            fields = "__all__"


class BaseLeadForm(forms.ModelForm):
    name = forms.CharField(max_length=100)
    address = forms.CharField(max_length=250)
    city = forms.CharField(max_length=100)
    pincode = forms.IntegerField()
    mobile = forms.CharField(max_length=32)
    email = forms.EmailField(required=False)
    vehicle_number = forms.CharField(max_length=100)

    class Meta:
        model = OfflineLeads
        exclude = ('program',)


class BREAKDOWNForm(BaseLeadForm):
    vehicle_make = forms.CharField(required=False)
    vehicle_model = forms.CharField(max_length=100)

    def clean(self):
        _self = super(BREAKDOWNForm, self).clean()
        _self['vehicle_make'] = ' '.join(
            _self['vehicle_model'].split(' ')[0]).upper()
        _self['vehicle_model'] = _self['vehicle_model'].split(' ')[1:].upper()
        return _self


class RSAForm(BaseLeadForm):
    __VEHICLE_AGE__ = 10
    VEHICLE_AGE = tuple([(e, str(e) + ' Years')
                         for e in range(__VEHICLE_AGE__ + 1)])
    VEHICLE_TYPE = (
        ('FOUR WHEELER', 'Private Car'),
        ('TWO WHEELER', 'Two Wheeler'),
    )
    BLOOD_TYPE = (
        ('NA', 'Not Available'),
        ('A +ve', 'A +ve'),
        ('A -ve', 'A -ve'),
        ('B +ve', 'B +ve'),
        ('B -ve', 'B -ve'),
        ('AB +ve', 'AB +ve'),
        ('AB -ve', 'AB -ve'),
        ('O +ve', 'O +ve'),
        ('O -ve', 'O -ve'),
    )
    PROGRAM = (
        (1, 'Program 1'),
        (2, 'Program 2'),
    )

    program_id = forms.ChoiceField(choices=PROGRAM, initial=1)
    valid_from = forms.DateTimeField(input_formats=["%d-%m-%Y", "%d/%m/%Y"])
    valid_upto = forms.DateTimeField(required=False)
    blood_group = forms.ChoiceField(choices=BLOOD_TYPE, initial="NA")
    vehicle_make = forms.CharField(max_length=100)
    vehicle_model = forms.CharField(max_length=100)
    vehicle_age = forms.ChoiceField(choices=VEHICLE_AGE, initial=0)
    date_of_birth = forms.DateTimeField(input_formats=["%d-%m-%Y", "%d/%m/%Y"], required=False)
    #date_of_birth = forms.DateTimeField(input_formats=["%d-%m-%Y", "%d/%m/%Y"])
    date_of_sale = forms.DateTimeField(input_formats=["%d-%m-%Y", "%d/%m/%Y"], required=False)
    cover_type = forms.CharField(max_length=100, required=False)
    vehicle_type = forms.ChoiceField(
        choices=VEHICLE_TYPE, initial='FOUR WHEELER')
    number_of_services = forms.CharField(max_length=10, required=False)

    def clean(self):
        _self = super(RSAForm, self).clean()
        date_list = ['valid_from',]
        #if any(map((lambda e: invalid(_self.get(e, temp_date))), ['date_of_birth', 'valid_from'])):
        if _self.get('date_of_birth', None):
            date_list.append('date_of_birth')
        if any(map((lambda e: invalid(_self.get(e, temp_date))), date_list)):
            raise forms.ValidationError("invalid date")
        _self['date_of_sale'] = datetime.datetime.today()
        _self['number_of_services'] = '4 AUTO'
        _self['cover_type'] = 'Coverfox Auto'
        if _self['valid_from']:
            _self['valid_upto'] = (lambda v: datetime.datetime(
                year=v.year + 1, month=v.month, day=v.day) - datetime.timedelta(days=1))(_self['valid_from'])
        return _self


class COMPREHENSIVEForm(BaseLeadForm):
    MANUFACTURING_YEAR = (lambda y: tuple(
        [(e,) * 2 for e in range(y - 7, y + 1)]))(datetime.datetime.now().year)

    valid_from = forms.DateTimeField(input_formats=["%d-%m-%Y", "%d/%m/%Y"])
    valid_upto = forms.DateTimeField(required=False)
    vehicle_chassis = forms.CharField(max_length=100)
    vehicle_manufacturing_year = forms.ChoiceField(
        choices=MANUFACTURING_YEAR, initial=0)
    vehicle_engine_number = forms.CharField(max_length=100)
    vehicle_make = forms.CharField(max_length=100)
    vehicle_model = forms.CharField(max_length=100)
    vehicle_sale_date = forms.DateTimeField(
        input_formats=["%d-%m-%Y", "%d/%m/%Y"])
    first_registration_date = forms.DateTimeField(
        input_formats=["%d-%m-%Y", "%d/%m/%Y"])
    odometer_reading = forms.IntegerField()

    def clean(self):
        _self = super(COMPREHENSIVEForm, self).clean()
        if any(map((lambda e: invalid(_self.get(e, temp_date))), ['vehicle_sale_date', 'valid_from', 'first_registration_date'])):
            raise forms.ValidationError("invalid date")
        if _self['valid_from']:
            _self['valid_upto'] = _self['valid_from'] + \
                datetime.timedelta(days=180)
        return _self


class ENGINEForm(BaseLeadForm):
    MANUFACTURING_YEAR = (lambda y: tuple(
        [(e,) * 2 for e in range(y - 7, y + 1)]))(datetime.datetime.now().year)

    valid_from = forms.DateTimeField(input_formats=["%d-%m-%Y", "%d/%m/%Y"])
    valid_upto = forms.DateTimeField(required=False)
    vehicle_chassis = forms.CharField(max_length=100)
    vehicle_manufacturing_year = forms.ChoiceField(
        choices=MANUFACTURING_YEAR, initial=0)
    vehicle_engine_number = forms.CharField(max_length=100)
    vehicle_make = forms.CharField(max_length=100)
    vehicle_model = forms.CharField(max_length=100)
    vehicle_sale_date = forms.DateTimeField(
        input_formats=["%d-%m-%Y", "%d/%m/%Y"])
    first_registration_date = forms.DateTimeField(
        input_formats=["%d-%m-%Y", "%d/%m/%Y"])
    odometer_reading = forms.IntegerField()

    def clean(self):
        _self = super(ENGINEForm, self).clean()
        if any(map((lambda e: invalid(_self.get(e, temp_date))), ['vehicle_sale_date', 'valid_from', 'first_registration_date'])):
            raise forms.ValidationError("invalid date")
        if _self['valid_from']:
            _self['valid_upto'] = _self['valid_from'] + \
                datetime.timedelta(days=180)
        return _self
