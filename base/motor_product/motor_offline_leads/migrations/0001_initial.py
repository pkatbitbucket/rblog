# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
import django_extensions.db.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='OfflineLeads',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('member_id', models.CharField(
                    max_length=250, editable=False, blank=True)),
                ('channel', models.CharField(max_length=100, editable=False)),
                ('features', models.CharField(max_length=100, editable=False)),
                ('member_uuid', django_extensions.db.fields.UUIDField(
                    default=None, editable=False, blank=True)),
                ('extra', jsonfield.fields.JSONField(default={},
                                                     verbose_name='extra', editable=False, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='OfflineLeadsSearch',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('inputquery', models.TextField(blank=True)),
                ('offleads', jsonfield.fields.JSONField(default={},
                                                        verbose_name='offleads', editable=False, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ProductProgram',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('program_id', models.IntegerField()),
                ('program_name', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='offlineleads',
            name='program',
            field=models.ForeignKey(
                default=None, to='motor_offline_leads.ProductProgram', null=True),
        ),
        migrations.AddField(
            model_name='offlineleads',
            name='user',
            field=models.ForeignKey(
                editable=False, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
