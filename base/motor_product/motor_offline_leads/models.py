import settings
import os
from django.db import models
from django.db.models.query import QuerySet
from core.models import User
from motor_product.motor_offline_leads import pdf_utils
from jsonfield import JSONField
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import UUIDField
import datetime


class OfflineLeadsSearch(models.Model):
    inputquery = models.TextField(blank=True, editable=True)
    offleads = JSONField(_('offleads'), blank=True, default={}, editable=False)

    def save(self, *args, **kwargs):
        super(OfflineLeadsSearch, self).save(*args, **kwargs)
        self.offleads = dict(enumerate(OfflineLeads.objects.filter(
            member_id__in=self.inputquery.split(',')).values_list('id', flat=True)))
        super(OfflineLeadsSearch, self).save(*args, **kwargs)


class ProductProgram(models.Model):
    program_id = models.IntegerField(editable=True)
    program_name = models.CharField(max_length=100, editable=True)


class OfflineLeadsManager(models.Manager):

    def __getattr__(self, attr):
        return getattr(self.QuerySet(self.model), attr)

    class QuerySet(QuerySet):

        def filter_user(self, user):
            return self.filter(user=user)


class OfflineLeads(models.Model):
    member_id = models.CharField(max_length=250, blank=True, editable=False)
    channel = models.CharField(max_length=100, editable=False)
    features = models.CharField(max_length=100, editable=False)
    program = models.ForeignKey(
        ProductProgram, null=True, editable=True, default=None)
    member_uuid = UUIDField(default=None)
    user = models.ForeignKey(User, null=True, editable=False)
    extra = JSONField(_('extra'), blank=True, default={}, editable=False)
    objects = OfflineLeadsManager()

    is_date = staticmethod(lambda field: (
        'date' in field) or ('valid' in field))
    datetimeIt = staticmethod(lambda date: datetime.datetime(
        *[int(e) for e in date[:10].split('-')]))

    def __unicode__(self):
        return self.member_id

    def save(self, *args, **kwargs):
        super(OfflineLeads, self).save(*args, **kwargs)
        if not self.member_id.endswith(str(self.pk)):
            self.member_id = self.member_id + str(self.pk)
            super(OfflineLeads, self).save(*args, **kwargs)

    def get_pdf_link(self):
        media_location = os.path.join(
            'motor_%s_pdfs' % self.features, self.channel)
        if self.channel == 'breakdown':
            html = pdf_utils.get_breakdown_html(os.path.join(
                settings.MEDIA_ROOT, media_location), self)
        else:
            html = getattr(pdf_utils, 'get_%s_html' % self.features)(
                os.path.join(settings.MEDIA_ROOT, media_location), self)
        return pdf_utils.get_pdf_link(
            html,
            settings.MEDIA_ROOT,
            os.path.join(media_location, self.member_id + '.pdf'),
            regenerate_pdf=False
        )

    def __getattr__(self, key):
        try:
            val = self.extra.get(key, '')
            if self.is_date(key):
                val = self.datetimeIt(val).strftime('%d %b, %Y') if val else ''
            return val
        except KeyError:
            raise AttributeError

    @classmethod
    def get_excel(cls, user, head_order, selected_ids=[]):
        Filtered = OfflineLeads.objects.all()
        if not user.is_superuser:
            Filtered = Filtered.filter(user=user)
        fil_keys = ['member_id', ] + [e for e in head_order if e in set(
            (lambda p: ([p.extend(e.extra.keys()) for e in Filtered], p)[1])([]))]
        return fil_keys, [[getattr(each, k) for k in fil_keys] for each in Filtered]
