from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
import pdfkit
import os
import datetime

from utils import get_property, convert_datetime_to_timezone
from dateutil import relativedelta
from django.template import Context
from django.template.loader import get_template
from django.conf import settings

import putils


def get_pdf_link(html, static_path=None, _path=False, regenerate_pdf=False):
    if _path:
        pdf_path = os.path.join(static_path, _path)
        if os.path.exists(pdf_path) and not regenerate_pdf:
            return _path
    else:
        pdf_path = _path

    config = pdfkit.configuration(wkhtmltopdf=settings.WKHTMLTOPDF_PATH)
    return pdfkit.from_string(
        html,
        pdf_path,
        configuration=config,
        options={
            'page-size': 'A4',
            'margin-top': '0',
            'margin-right': '0',
            'margin-left': '0',
            'margin-bottom': '0',
            'zoom': '1.2',
            'encoding': "UTF-8",
        })


def get_warranty_html(media_location, OfflineObject):
    kwargs = OfflineObject.extra
    return file(os.path.join(media_location, 'template.data'), 'r').read() % (
        os.path.join(media_location, 'header.jpg'),
        OfflineObject.member_id,
        kwargs['name'],
        kwargs['vehicle_manufacturing_year'],
        kwargs['city'],
        kwargs['vehicle_chassis'],
        kwargs['pincode'],
        kwargs['vehicle_engine_number'],
        kwargs['mobile'],
        kwargs['vehicle_sale_date'].strftime('%b %d, %Y'),
        kwargs['email'],
        kwargs['odometer_reading'],
        kwargs['vehicle_make'].upper(),
        kwargs['first_registration_date'].strftime('%b %d, %Y'),
        kwargs['vehicle_model'].upper(),
        kwargs['vehicle_number'].upper(),
        kwargs['address'],
        kwargs['valid_from'].strftime('%b %d, %Y'),
        kwargs['valid_upto'].strftime('%b %d, %Y'),
        os.path.join(media_location, 'footer.jpg')
    )


def get_engine_html(media_location, kwargs):
    return get_warranty_html(media_location, kwargs)


def get_breakdown_html(media_location, OfflineObject):
    kwargs = OfflineObject.extra
    return file(os.path.join(media_location, 'template.data'), 'r').read() % (
        media_location,
        media_location,
        media_location,
        OfflineObject.member_id,
        OfflineObject.__class__.datetimeIt(
            kwargs['valid_from']).strftime("%d %b, %Y"),
        OfflineObject.__class__.datetimeIt(
            kwargs['valid_upto']).strftime("%d %b, %Y"),
        kwargs['name'],
        kwargs['address'],
        kwargs['pincode'],
        kwargs['email'],
        kwargs['mobile'],
        '',
        kwargs['vehicle_model'].upper(),
        kwargs['vehicle_number'].upper(),
        OfflineObject.member_id,
    )


def get_rsa_html(media_location, OfflineObject):
    kwargs = OfflineObject.extra
    dob = 'NA'
    try:
        dob = kwargs['date_of_birth'].strftime('%b %d, %Y')
    except:
        pass

    return file(os.path.join(media_location, 'template.data'), 'r').read() % (
        os.path.join(media_location, 'header.jpg'),
        OfflineObject.member_id,
        kwargs['name'],
        kwargs['vehicle_number'].upper(),
        kwargs['city'],
        dict([(0, "New"), (1, '1 Year')] + [(e, str(e) + ' Years')
                                            for e in range(2, int(kwargs['vehicle_age']) + 1)])[int(kwargs['vehicle_age'])],
        kwargs['pincode'],
        kwargs['vehicle_type'],
        kwargs['email'],
        # kwargs['date_of_birth'].strftime('%b %d, %Y'),
        dob,
        kwargs['vehicle_make'].upper(),
        kwargs['mobile'],
        kwargs['vehicle_model'].upper(),
        kwargs['blood_group'],
        kwargs['address'],
        kwargs['valid_from'].strftime('%b %d, %Y'),
        kwargs['valid_upto'].strftime('%b %d, %Y'),
        os.path.join(media_location, 'footer.jpg')
    )


def get_rsa_transaction(transaction):
    membership_valid_from = transaction.policy_start_date or transaction.quote.new_policy_start_date
    membership_valid_to = putils.add_years(membership_valid_from, 1) - datetime.timedelta(days=1)

    # Calculation vehicle_age
    manufacturing_date = transaction.quote.raw_data['manufacturingDate']
    manufacturing_date = datetime.datetime.strptime(manufacturing_date, "%d-%m-%Y") if manufacturing_date else None
    vehicle_age = relativedelta.relativedelta(
        convert_datetime_to_timezone(membership_valid_from),
        convert_datetime_to_timezone(manufacturing_date)
    ).years

    # Address
    address = [
        get_property(transaction, 'proposer.address.line_1'),
        get_property(transaction, 'proposer.address.line_2'),
        get_property(transaction, 'proposer.address.landmark'),
    ]
    address = [a for a in address if a is not None]

    # If numeric/empty (city)
    city = get_property(transaction, 'proposer.address.city')
    state = get_property(transaction, 'proposer.address.state')
    pincode = get_property(transaction, 'proposer.address.pincode')
    if (not city or city.isdigit()) and pincode:
        city, state = putils.get_city_state_from_pincode(pincode)

    # If new car, then registration no will be `NEW`
    registration_number = 'NEW' if transaction.quote.is_new else \
        get_property(transaction, 'raw.user_form_details.vehicle_reg_no') or 'NA'

    data_dict = {
        'name': "{} {}".format(get_property(transaction, 'proposer.first_name'), get_property(transaction, 'proposer.last_name')),
        'city': city or 'NA',
        'pincode': pincode or 'NA',
        'email': get_property(transaction, 'proposer.email') or 'NA',
        'make': get_property(transaction, 'quote.vehicle.model.make.name') or 'NA',
        'model': get_property(transaction, 'quote.vehicle.model.name') or 'NA',
        'address': address,
        'blood_group': "NA",
        'mobile': get_property(transaction, 'proposer.mobile') or 'NA',
        'date_of_birth': get_property(transaction, 'proposer.date_of_birth'),
        'vehicle_type': get_property(transaction, 'vehicle_type') or 'NA',
        'vehicle_age': vehicle_age,
        'registration_number': registration_number,
        'membership_valid_from': membership_valid_from,
        'membership_upto': membership_valid_to,
        'rsa_policy_number': get_property(transaction, 'rsa_policy_number'),
        'product': ' ',
    }

    return data_dict


def generate_pdf(transaction):
    template = get_template('motor_offline_leads:rsa.html')
    data = get_rsa_transaction(transaction)
    html = template.render(Context(data))
    rsa_doc_path = os.path.join(settings.MOTOR_RSA_PDF_PATH, str(transaction.transaction_id) + ".pdf")
    content = get_pdf_link(html, regenerate_pdf=True)
    with default_storage.open(rsa_doc_path, 'wb') as f:
        for chunk in ContentFile(content).chunks():
            f.write(chunk)
    return rsa_doc_path
