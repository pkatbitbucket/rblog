from motor_product.motor_offline_leads.models import OfflineLeads
import sys
import datetime


def ditocsv(lofdi, file_name):
    keys = max(lofdi, key=(lambda e: len(e.keys()))).keys()
    wobj = file(file_name, 'w')
    wobj.write('|'.join(keys) + '\n')
    wobj.flush()
    for each in lofdi:
        st = []
        for k in keys:
            try:
                st.append(str(each[k]))
            except KeyError:
                st.append('None')
        wobj.write('|'.join(st) + '\n')
        wobj.flush()
    wobj.close()
    del wobj


def get_csv(file_name='OfflineLeads%s.csv' % datetime.datetime.now().strftime("%d-%b-%Y")):
    ditocsv([dict(e.extra.items() + [('member_id', e.member_id)])
             for e in OfflineLeads.objects.all()], file_name)


if __name__ == '__main__':
    try:
        get_csv(sys.argv[1])
    except:
        get_csv()
