from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import Http404, HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.conf import settings
import cf_cns

from motor_product.motor_offline_leads.forms import RSAForm, COMPREHENSIVEForm, ENGINEForm, SEARCHForm
from motor_product.motor_offline_leads.models import OfflineLeads, ProductProgram, OfflineLeadsSearch

import copy
import datetime
import putils
import utils
import thread

import json
from leads.views import save_call_time
from mailer import direct as direct_mail

ROLE_MAPPING = {
    'credr': 'MOTOR_RSA_CREDR',
    'cfox': 'REPORT',
}


@login_required
def form_page(request, features, channel, member_uuid=''):
    if channel not in ROLE_MAPPING:
        raise Http404

    if not request.user.groups.filter(name=ROLE_MAPPING[channel]).exists():
        return HttpResponseForbidden("Access Denied")

    context = {'static_path': settings.MEDIA_ROOT, 'channel': channel,
               'features': features, 'errors': None, 'pdf_link': None}
    form = dict([('rsa', RSAForm), ('warranty', COMPREHENSIVEForm),
                 ('engine', ENGINEForm)])[features]
    if request.method == "GET":
        if features == 'rsa' and member_uuid:
            if '_delete' in member_uuid:
                # ignore = [each.delete() for each in OfflineLeads.objects.filter(
                #     member_uuid=member_uuid.replace('_delete', ''))]
                return HttpResponseRedirect("/motor/offline/rsa/%s/" % channel)
            else:
                objs = OfflineLeads.objects.filter(member_uuid=member_uuid)
                if objs.count():
                    # Ignore if there are more than one member_uuid
                    details = objs[0].__dict__
                    details.update(details.pop('extra'))
                    try:
                        date_of_birth = OfflineLeads.datetimeIt(
                            objs[0].extra['date_of_birth']).strftime('%d/%m/%Y')
                    except KeyError:
                        date_of_birth = ''
                        pass
                    try:
                        valid_from = OfflineLeads.datetimeIt(
                            objs[0].extra['valid_from']).strftime('%d/%m/%Y')
                    except KeyError:
                        valid_from = ''
                        pass
                    details.update(
                        {'date_of_birth': date_of_birth, 'valid_from': valid_from})
                    mem_id = objs[0].member_id
                else:
                    return HttpResponseRedirect("/motor/offline/rsa/%s" % channel)
        else:
            details, mem_id = {}, ''
        context.update({'form_': form(initial=details),
                        'errors': ' ', 'member_id': mem_id})
    else:
        form = form(request.POST)
        if form.is_valid():
            if member_uuid:
                offline_leads = OfflineLeads.objects.get(
                    member_uuid=member_uuid)
                offline_leads.extra = form.cleaned_data
            else:
                offline_leads = OfflineLeads(
                    member_id='CF%s%s201' % (
                        {'rsa': 'R', 'warranty': 'W', 'engine': 'E'}[features],
                        {'credr': 1, 'cfox': 2}[channel]
                    ),
                    channel=channel,
                    features=features,
                    user=request.user,
                    extra=form.cleaned_data
                )
            if features == 'rsa':
                offline_leads.program_id = ProductProgram.objects.get(
                    program_id=int(form.data['program_id'])).id
            offline_leads.save()
            context.update({
                'pdf_link': offline_leads.get_pdf_link(),
                'errors': None,
                'form_': form.cleaned_data,
                'member_id': offline_leads.member_id,
                'member_uuid': offline_leads.member_uuid,
            })
        else:
            context.update({'errors': form.errors, 'form_': form})
    return render_to_response("motor_product/motor_offline_leads/template-" + features + ".html", context, context_instance=RequestContext(request))


def breakdown_form(request, member_uuid=None):
    if request.method == "GET":
        if not member_uuid:
            return HttpResponseForbidden("Access Denied")
        else:
            details = OfflineLeads.objects.get(member_uuid=member_uuid)
            context = copy.deepcopy(details.extra)
            context.update({
                'member_id': details.member_id
            })
            return render(request, "campaign/motor/breakdown-summary.html", context)
    else:
        context = json.loads(request.POST['data'])
        # ------------------------------HACK--------(get this shit go away as soon as you can)---------------------------#
        current_year = datetime.datetime.now()
        next_year = (lambda v: datetime.datetime(
            year=v.year + 1, month=v.month, day=v.day) - datetime.timedelta(days=1))(current_year)
        context['form_data'].update({
            # DjangoJSONSerializer format
            'valid_from':   current_year.strftime("%Y-%m-%dT%H:%M:%S+05:30"),
            # DjangoJSONSerializer format
            'valid_upto':   next_year.strftime("%Y-%m-%dT%H:%M:%S+05:30"),
        })
        # ---------------------------------------------------------------------------------------------------------------#
        if not member_uuid:
            offline_leads = OfflineLeads(
                member_id='CFR3201',
                channel='breakdown',
                features='rsa',
                user=None if request.user.is_anonymous() else request.user,
                extra=context['form_data']
            )
            offline_leads.program_id = ProductProgram.objects.get(
                program_id=1).id
            offline_leads.save()
            context.update(context.pop('form_data'))
            context.update({'pdf_link': '', 'member_id': offline_leads.member_id,
                            'member_uuid': offline_leads.member_uuid})
            request.POST = request.POST.copy()
            request.POST['data'] = json.dumps(context)
            save_call_time(request)
            return HttpResponse(json.dumps(context))
        else:
            offline_leads = OfflineLeads.objects.get(member_uuid=member_uuid)
            offline_leads.extra.update(context['form_data'])
            offline_leads.save()
            context.update(context.pop('form_data'))
            context.update({
                'pdf_link': settings.MEDIA_URL + offline_leads.get_pdf_link(),
                'member_id': offline_leads.member_id,
                'member_uuid': member_uuid
            })
            request.POST = request.POST.copy()
            request.POST['data'] = json.dumps(context)
            save_call_time(request)

            form_data = json.loads(request.POST['data'])
            obj = {
                'name': utils.clean_name(form_data['name']),
                'member_id': form_data['member_id'],
            }

            # SEND EMAIL & SMS FOR APPOINTMENT CONFIRMATION TO CUSTOMER
            if form_data['insurance_status'] == 'valid':
                obj['health_email'] = 'cbs@coverfox.com'
                cf_cns.notify('RSA_STEP2_VALID_INS',
                              to=(form_data['name'], form_data['email']),
                              numbers=form_data['mobile'], obj=obj, mandrill_template='newbase')

            else:
                obj['health_toll_free'] = settings.HEALTH_TOLL_FREE
                cf_cns.notify('RSA_STEP2_EXPIRED_INS',
                              to=(form_data['name'], form_data['email']),
                              numbers=form_data['mobile'], obj=obj, mandrill_template='newbase')

            return HttpResponse(json.dumps({'success': True,
                                            'redirect_url': reverse('breakdown_form_with_uuid',
                                                                    kwargs={'member_uuid': member_uuid})}))


@login_required
def download(request):
    repeated_field_index = [1, 8, 8, 22, 23, 8, 4, 14, 12, 13, 13, 13, 21, 2]
    repeated_head_keys = [
        'name',
        'city',
        'city',
        'valid_from',
        'valid_upto',
        'city',
        'mobile',
        'vehicle_number',
        'vehicle_make',
        'vehicle_model',
        'vehicle_model',
        'vehicle_model',
        'vehicle_manufacturing_year',
        'email'
    ]
    user = ['name','email','user_name','mobile','date_of_birth','blood_group']
    address = ['pincode','city','address']
    vehicle = [
        'date_of_sale',
        'vehicle_age',
        'vehicle_make',
        'vehicle_model',
        'vehicle_number',
        'odometer_reading',
        'vehicle_chassis',
        'vehicle_engine_number',
        'first_registration_date',
        'vehicle_sale_date',
        'vehicle_type',
        'vehicle_manufacturing_year',
    ]
    policy = ['valid_from', 'valid_upto', 'number_of_service',
              'number_of_services', 'cover_type', 'policy_expiry_month']
    extra = ['campaign', 'label', 'device', 'triggered_page']
    head_keys, list_of_values = OfflineLeads.get_excel(
            request.user,
            user+address+vehicle+policy+extra,
            None#list(OfflineLeadsSearch.objects.get(id=search_id).offleads.values())
            )

    #  Code for embedding new fields in excel start
    head_keys = head_keys + repeated_head_keys
    for list_val in list_of_values:
        for index in repeated_field_index:
            list_val.append(list_val[index])
    #  Code for embedding new fields in excel end

    filename = "%s_%s.xls" % (request.user.username, datetime.datetime.now().strftime("%d%b%Y"))

    return putils.render_excel(filename, head_keys, list_of_values)


@login_required
def sneekpeek(request, channel):
    context = {'search': SEARCHForm(), 'query': ''}
    if request.method == 'POST':
        search = SEARCHForm(request.POST)
        if search.is_valid():
            searchobject = OfflineLeadsSearch(
                inputquery=search.cleaned_data['inputquery'])
            searchobject.save()
            context = {
                'search':   SEARCHForm(initial={'inputquery': searchobject.inputquery}),
                'offleads':   (lambda l: dict([(int(k), OfflineLeads.objects.get(id=v)) for k, v in l]))(searchobject.offleads.items()),
                'searchobject':   searchobject,
                'query':   searchobject.inputquery,
            }
        else:
            context = {'search': SEARCHForm(
                initial={'inputquery': 'Invalid search...'}), 'query': ''}
    return render_to_response(
        "motor_product/motor_offline_leads/template-sneekpeek.html",
        context,
        context_instance=RequestContext(request))
