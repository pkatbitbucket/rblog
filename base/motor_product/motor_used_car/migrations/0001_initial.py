# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import motor_product.motor_used_car.models


class Migration(migrations.Migration):

    dependencies = [
        ('motor_product', '0002_auto_20150807_1908'),
    ]

    operations = [
        migrations.CreateModel(
            name='UsedCarPolicy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=254, verbose_name='Email', blank=True)),
                ('mobile', models.CharField(max_length=11, verbose_name='Mobile', blank=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name', blank=True)),
                ('policy_number', models.CharField(max_length=255, verbose_name='Policy number')),
                ('vehicle_number', models.CharField(max_length=255, null=True, verbose_name='Vehicle number', blank=True)),
                ('premium', models.CharField(max_length=10, null=True, blank=True)),
                ('insurer', models.ForeignKey(to='motor_product.Insurer', null=True)),
                ('transaction', models.ForeignKey(to='motor_product.Transaction', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='UsedCarPolicyDocuments',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('document', models.FileField(upload_to=motor_product.motor_used_car.models.get_document_path)),
                ('document_type', models.CharField(blank=True, max_length=20, verbose_name='document_type', choices=[(b'policy', b'Current Policy'), (b'previous', b'Previous year Policy'), (b'reg_certi', b'Registration Certificate'), (b'form29', b'Form 29'), (b'form30', b'Form 30'), (b'NCB', b'NCB Transfer certificate'), (b'ins_report', b'Inspection Report'), (b'invoice', b'Invoice'), (b'photo', b'Photo'), (b'others', b'Others')])),
                ('policy', models.ForeignKey(related_name='policy_documents', to='motor_used_car.UsedCarPolicy')),
            ],
        ),
    ]
