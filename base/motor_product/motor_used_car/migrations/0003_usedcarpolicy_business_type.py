# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motor_used_car', '0002_usedcarpolicy_is_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='usedcarpolicy',
            name='business_type',
            field=models.CharField(default='alliance', max_length=20, verbose_name='business_type', choices=[(b'alliance', b'Alliance'), (b'expired', b'Expired Case'), (b'dealer', b'Dealer')]),
            preserve_default=False,
        ),
    ]
