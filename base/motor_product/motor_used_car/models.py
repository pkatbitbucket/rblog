import os

from django.db import models
from django.utils.translation import ugettext_lazy as _

from motor_product.models import Insurer, Transaction


def get_document_path(instance, filename):
    path_to_save = "media/motor/%s" % (filename)
    return path_to_save


class UsedCarPolicy(models.Model):
    BUSINESS_TYPE = (
        ('alliance', 'Alliance'),
        ('expired', 'Expired Case'),
        ('dealer', 'Dealer')
    )
    business_type = models.CharField(_("business_type"), max_length=20, choices=BUSINESS_TYPE)
    insurer = models.ForeignKey(Insurer, null=True)
    email = models.EmailField(_("Email"), blank=True)
    mobile = models.CharField(_("Mobile"), max_length=11, blank=True)
    name = models.CharField(_("Name"), max_length=255, blank=True)
    policy_number = models.CharField(_("Policy number"), max_length=255)
    vehicle_number = models.CharField(_("Vehicle number"), max_length=255, blank=True, null=True)
    premium = models.CharField(max_length=10, blank=True, null=True)
    transaction = models.ForeignKey(Transaction, null=True)
    is_active = models.BooleanField(default=False)

    def __unicode__(self):
        return "%s: %s - %s" % (self.insurer, self.name, self.policy_number)


class UsedCarPolicyDocuments(models.Model):
    DOC_TYPE_CHOICE = (
        ('policy', 'Current Policy'),
        ('previous', 'Previous year Policy'),
        ('reg_certi', 'Registration Certificate'),
        ('form29', 'Form 29'),
        ('form30', 'Form 30'),
        ('NCB', 'NCB Transfer certificate'),
        ('ins_report', 'Inspection Report'),
        ('invoice', 'Invoice'),
        ('photo', 'Photo'),
        ('others', 'Others'),
    )
    policy = models.ForeignKey(UsedCarPolicy, related_name='policy_documents')
    document = models.FileField(upload_to=get_document_path)
    document_type = models.CharField(_("document_type"), max_length=20, choices=DOC_TYPE_CHOICE, blank=True)

    def filename(self):
        return os.path.basename(self.document.name)

    def __unicode__(self):
        return '%s' % self.document.name
