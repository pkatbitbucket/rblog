from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^manage-policy/$', views.manage_policy, name='manage_policy'),
    url(r'^upload-doc/$', views.upload_document, name='upload_document'),
    url(r'^policy-documents/$', views.policy_documents, name='policy_documents'),
    url(r'^download/(?P<used_car_id>\d+)/$', views.download, name='download'),
    url(r'manage-policy/delete-doc/$', views.delete_doc, name='delete_doc'),  # (?P<insurer_slug>[\w-]+)
    url(r'logout/$', views.logout, name='logout')
]
