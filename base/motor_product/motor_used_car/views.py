import os
import urllib

from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.db.models import Q
from django.contrib.auth.decorators import login_required

import utils
from motor_product.models import Transaction, Insurer
from .models import UsedCarPolicy, UsedCarPolicyDocuments
from django.views.decorators.cache import never_cache
from django.contrib.auth import views as auth_views

doc_type_dict = {'policy': 'Current Policy',
                 'previous': 'Previous year Policy',
                 'reg_certi': 'Registration Certificate',
                 'form30': 'Form 29/30',
                 'NCB': 'NCB',
                 'ins_report': 'Inspection Report',
                 'invoice': 'Invoice',
                 'photo': 'Photo',
                 'others': 'Others'
                 }
business_type = {'alliance': 'Alliance',
                 'expired': 'Expired Case',
                 'dealer': 'Dealer'
                 }


def get_logged_in_insurer(request):
    """
    Insurer's user object must have 'motor-insurer' key in extra,
    which will be insurer's slug
    """
    insurer = None
    is_insurer = request.user.groups.filter(name='INSURER').exists()
    if is_insurer:
        insurer_slug = request.user.extra['motor-insurer']
        insurer = Insurer.objects.get(slug=insurer_slug)
    return insurer


@login_required
@utils.profile_type_only('ADMIN', 'MOTOR_CAT')
def manage_policy(request):
    try:
        policy_number = request.GET['policy_no'].strip()
    except KeyError:
        return HttpResponseRedirect(reverse('motor_used_car:policy_documents'))
    try:
        policy = UsedCarPolicy.objects.get(policy_number=policy_number)
        policy_list = policy.policy_documents.all()

    except UsedCarPolicy.DoesNotExist:
        policy = {
            'policy_number': policy_number,
        }
        policy_list = []
    template = "motor_product:used_car/manage_policy.html"
    template_data = {
        'policy': policy,
        'insurers': Insurer.objects.all(),
        'policy_type': doc_type_dict,
        'business_type': business_type,
        'policy_list': policy_list
    }
    return render_to_response(template, template_data, context_instance=RequestContext(request))


@login_required
@utils.profile_type_only('ADMIN', 'MOTOR_CAT')
def upload_document(request):
    if request.method == 'POST':
        policy_number = request.POST['policy_number'].strip()
        transaction_id = request.POST['transaction_id'].strip()
        doc_type_list = []
        get_params = {
            'policy_no': policy_number,
        }
        for key in doc_type_dict:
            if key in request.FILES:
                doc_type_list.append(key)

        used_car_policy, created = UsedCarPolicy.objects.get_or_create(policy_number=policy_number)
        used_car_policy.vehicle_number = request.POST['vehicle_number'].strip()
        used_car_policy.name = request.POST['name'].strip()
        used_car_policy.mobile = request.POST['mobile'].strip()
        used_car_policy.email = request.POST['email'].strip()
        used_car_policy.premium = request.POST['premium'].strip()
        used_car_policy.is_active = True if request.POST['active'] == '1' else False
        used_car_policy.insurer = Insurer.objects.get(id=request.POST['insurer'])
        used_car_policy.business_type = request.POST['business_type']
        manage_policy_url = reverse('motor_used_car:manage_policy')
        if transaction_id:
            try:
                used_car_policy.transaction = Transaction.objects.get(transaction_id=transaction_id)
            except Transaction.DoesNotExist:
                get_params['transaction_error'] = 'Please enter a valid transaction no'
        used_car_policy.save()
        for doc_type in doc_type_list:
            document = UsedCarPolicyDocuments(policy=used_car_policy, document=request.FILES[doc_type], document_type=doc_type)
            document.save()
        manage_policy_url = "%s?%s" % (manage_policy_url, urllib.urlencode(get_params))
        return HttpResponseRedirect(manage_policy_url)
    else:
        return HttpResponseForbidden()


@login_required
@utils.profile_type_only('ADMIN', 'MOTOR_CAT', 'INSURER')
def policy_documents(request):
    """ Returns transaction list """

    insurer = get_logged_in_insurer(request)
    if request.method == "POST":

        policy_number = request.POST['policy_number'].strip()
        customer_name = request.POST['customer_name'].strip()
        vehicle_number = request.POST['vehicle_number'].strip()
        query = Q()

        if insurer:
            query = query & Q(insurer=insurer) & Q(is_active=True)
        if policy_number:
            query = query & Q(policy_number=policy_number)
        if customer_name:
            query = query & Q(name__icontains=customer_name)
        if vehicle_number:
            query = query & Q(vehicle_number=vehicle_number)
        used_car_list = UsedCarPolicy.objects.filter(query)
        doc_dict = {}
        policy_list = []
        for used_car in used_car_list:
            policy_list = used_car.policy_documents.all()
            doc_dict[used_car] = policy_list
        template = "motor_product:used_car/refresh_upload_document.html"
        template_data = {
            'insurer': insurer,
            'doc_dict': doc_dict,
        }
    else:
        template = "motor_product:used_car/document_list.html"
        template_data = {'insurer': insurer}

    return render_to_response(template, template_data,
                              context_instance=RequestContext(request))


@login_required
@utils.profile_type_only('ADMIN', 'MOTOR_CAT')
def delete_doc(request):
    if request.POST:
        doc_id = request.POST.get('doc_id', "")
        doc = UsedCarPolicyDocuments.objects.get(id=doc_id)
        doc.document.delete()
        doc.delete()
        return HttpResponse('{"success": true}')
    else:
        return HttpResponseForbidden()


@login_required
@utils.profile_type_only('ADMIN', 'MOTOR_CAT', 'INSURER')
def download(request, used_car_id):
    used_car = UsedCarPolicy.objects.get(id=used_car_id)
    insurer = get_logged_in_insurer(request)

    # do not let an insurer download other insurer's document
    if (not insurer) or (insurer == used_car.insurer):
        policy_number = used_car.policy_number.replace('/', '-')
        zip_name = used_car.name.replace(' ', '_') + '_' + policy_number
        policy_list = used_car.policy_documents.all()
        zip_path = [x.document for x in policy_list]
        zip_file = create_zip_file(zip_path, policy_number)
        response = HttpResponse(zip_file.read(), content_type='zip')
        response['Content-Disposition'] = 'filename=%s.zip' % zip_name
        return response
    else:
        return HttpResponseForbidden()


def create_zip_file(zip_path, policy_number):
    '''
    file_list: list of file path
    '''
    from zipfile import ZipFile
    from StringIO import StringIO
    zip_file_in_memory = StringIO()
    zip_file = ZipFile(zip_file_in_memory, 'w')
    for f in zip_path:
        file_name = policy_number + "_" + f.name.split("/")[-1]
        zip_file.writestr(file_name, f.read())

    zip_file.close()
    zip_file_in_memory.seek(0)
    return zip_file_in_memory


@never_cache
def logout(request, *args, **kw):

    response = auth_views.logout(request, *args, **kw)
    response.delete_cookie('profile')
    logout_url = reverse('motor_used_car:policy_documents')
    return HttpResponseRedirect(logout_url)
