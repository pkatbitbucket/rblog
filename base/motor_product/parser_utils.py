from collections import defaultdict
from lxml import etree
from xlrd import open_workbook, XL_CELL_NUMBER
import copy
import math

from django.conf import settings


def try_parse_into_integer(value):
    try:
        val = int(value)
    except ValueError:
        val = None

    return val


def try_parse_into_negative_float(value):
    try:
        val = -float(value)
    except:
        val = None
    return val


def try_parse_into_float(value):
    try:
        val = float(value)
    except:
        val = None
    return val


def read_file_to_return_vehicle_dictionary(file_path, parameters, csvdelimiter='|', linedelimiter='\n'):
    file_data_split_by_line = open(file_path).read().split(linedelimiter)
    vehicle_dict_list = []
    for data in file_data_split_by_line[1:]:
        if not data:
            continue

        vehicle_dict = {}
        vehicle_dict['raw_data'] = data
        vehicle_data = data.split(csvdelimiter)
        for k, v in parameters.items():
            vehicle_dict[k] = vehicle_data[v]
        vehicle_dict_list.append(vehicle_dict)

    return vehicle_dict_list


def yield_mapping_dictionary(file_path):
    file_data_split_by_line = [line[:-1]
                               for line in file(file_path, 'r').xreadlines()]
    for line in file_data_split_by_line:
        if not line:
            continue

        master_dict = {}
        mapped_data = line.split('|')
        master_dict['master_car'] = mapped_data[0]
        master_dict['mapped_cars'] = {}
        for every_mapped_car in mapped_data[1:]:
            insurer, mapped_car = every_mapped_car.split('!@!')
            master_dict['mapped_cars'][insurer] = mapped_car
        yield master_dict


def qdict_to_dict(qdict):
    """Convert a Django QueryDict to a Python dict.

    Single-value fields are put in directly, and for multi-value fields, a list
    of all values is stored at the field's key.

    """
    return {k: v[0] if len(v) == 1 else v for k, v in qdict.lists()}


def dict_to_xml(element_name, request_dict):

    Root = etree.Element(element_name)

    for key, value in request_dict.items():
        node = etree.Element(key)
        if value is None:
            node.text = ''
        else:
            node.text = str(value)
        Root.append(node)
    data = etree.tostring(Root)
    return data


def etree_to_dict(t):
    d = {t.tag: map(etree_to_dict, t.iterchildren())}
    d.update(('@' + k, v) for k, v in t.attrib.iteritems())
    d['text'] = t.text
    return d


def complex_etree_to_dict(t):
    d = {t.tag: {} if t.attrib else None}
    children = list(t)
    if children:
        dd = defaultdict(list)
        for dc in map(complex_etree_to_dict, children):
            for k, v in dc.iteritems():
                dd[k].append(v)
        d = {t.tag: {k: v[0] if len(v) == 1 else v for k, v in dd.iteritems()}}
    if t.attrib:
        d[t.tag].update(('@' + k, v) for k, v in t.attrib.iteritems())
    if t.text:
        text = t.text.strip()
        if children or t.attrib:
            if text:
                d[t.tag]['#text'] = text
        else:
            d[t.tag] = text
    return d


def dict_to_etree(root, request_dict):
    for key, value in request_dict.items():
        node = etree.Element(key)
        if isinstance(value, dict):
            dict_to_etree(node, value)
        elif isinstance(value, list):
            list_to_etree(node, value)
        else:
            if value is None:
                node.text = ''
            else:
                node.text = unicode(value)
        root.append(node)
    return root


def list_to_etree(element, request_list):
    for index, item in enumerate(request_list):
        if type(item) is dict:
            elem = dict_to_etree(element, item)
        elif type(item) is list:
            elem = list_to_etree(element, item)
        element.append(elem)
    return element


def clean_body_parameters(post_parameters):
    for key, value in post_parameters.items():
        if type(value) is int:
            post_parameters[key] = str(value)
        elif type(value) is float:
            post_parameters[key] = str(value)
    return post_parameters


# Method to parse excel and retun row dictionary generator
def parse_xlsx(_file, header=False, row_dict=False, int_as_str=True):

    # check if file is an InMemoryFile
    if hasattr(_file, "read"):

        # open InMemory excel file as workbook
        workbook = open_workbook(filename=None, file_contents=_file.read())

    else:

        # open excel file from file system as workbook
        workbook = open_workbook(_file)

    # get sheets in excel workbook
    sheets = workbook.sheet_names()

    # select first sheet containg records
    active_sheet = workbook.sheet_by_name(sheets[0])

    # get number of rows in selected sheet
    num_rows = active_sheet.nrows

    # get number of column in selected sheet
    num_cols = active_sheet.ncols

    # get header row from selected sheet
    _header = [active_sheet.cell_value(0, cell) for cell in range(num_cols)]

    # loop through each row in sheet
    for row_idx in xrange(0 if header and not row_dict else 1, num_rows):

        # get all columns data in current loop row
        row_cell = [unicode(int(
            active_sheet.cell_value(row_idx, col_idx))) if active_sheet.cell(
            row_idx, col_idx).ctype == XL_CELL_NUMBER and math.ceil(
            active_sheet.cell_value(row_idx, col_idx)) == active_sheet.cell_value(
            row_idx, col_idx) and int_as_str else active_sheet.cell_value(
            row_idx, col_idx) for col_idx in range(num_cols)]

        # zip current loop row with header and convert to dictionary and yield final result
        yield dict(zip(_header, row_cell)) if row_dict else row_cell


def normalize_request_dict(request):
    request_dict = copy.deepcopy(request.POST)
    post_parameters = qdict_to_dict(request_dict)
    post_parameters.pop('csrfmiddlewaretoken', None)
    post_parameters.pop('third_party', None)
    post_parameters['third_party'] = request.META.get('THIRD_PARTY', None)
    post_parameters.pop('payment_mode', None)
    post_parameters['payment_mode'] = 'PAYMENT_GATEWAY'
    if request.META.get('IS_ADMIN', False) and settings.PRODUCTION:
        post_parameters['payment_mode'] = 'CD_ACCOUNT'
    # Hack for cardekho cd_account
    if post_parameters['third_party'] == 'cardekho' and settings.PRODUCTION:
        post_parameters['payment_mode'] = 'CD_ACCOUNT'

    return post_parameters
