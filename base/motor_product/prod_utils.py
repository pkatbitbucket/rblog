"""Production Utils."""

import copy
import hashlib
import json
import logging
import os
import re
import time

from datetime import date, datetime, timedelta
from django.core.files.storage import default_storage
import requests
from celery.exceptions import SoftTimeLimitExceeded
from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.core.cache import caches
from django.core.mail import EmailMessage
from django.db.models import Q
from django.http import JsonResponse

from account_manager.signals.signals import (activate_user_account,
                                             create_user_account)
from celery_app import app
from core import activity as core_activity
from core.models import Activity, RequirementKind
from motor_product import errors, mail_utils
from motor_product.insurers import validations as insurer_user_form_validations
from motor_product.insurers import insurer_utils
from motor_product.insurers.bajaj_allianz import integration as bajaj_allianz
from motor_product.insurers.bharti_axa import integration as bharti_axa
from motor_product.insurers.future_generali import integration as future_generali
from motor_product.insurers.hdfc_ergo import integration as hdfc_ergo
from motor_product.insurers.hdfc_ergo.custom_dictionaries import STATE_ID_TO_STATE_NAME, STATE_ID_TO_CITY_LIST
from motor_product.insurers.icici_lombard import integration as icici_lombard
from motor_product.insurers.iffco_tokio import integration as iffco_tokio
from motor_product.insurers.landt import integration as landt
from motor_product.insurers.landt import get_landt_user_data
from motor_product.insurers.liberty_videocon import integration as liberty_videocon
from motor_product.insurers.new_india import integration as new_india
from motor_product.insurers.oriental import integration as oriental
from motor_product.insurers.reliance import integration as reliance
from motor_product.insurers.tata_aig import integration as tata_aig
from motor_product.insurers.universal_sompo import integration as universal_sompo
from motor_product.logger_utils import GrafanaStatsd
from motor_product.models import (Address, Contact, Insurer, LnTErrorDetail,
                                  LnTPolicyResponse, LnTStatusDetail, Make,
                                  Model, NewRegistrationDetail, Quote,
                                  RegistrationDetailStatistics,
                                  RequirementDealerMap, Transaction,
                                  UserVehicle, VehicleMaster, get_policy_path, IntegrationStatistics, RTOMaster)
from motor_product.cache_utils import QueryCache, PremiumTableCache
from motor_product.motor_offline_leads.pdf_utils import generate_pdf
from putils import exact_email_re, render_excel, retry
from utils import log_error, motor_logger, quote_logger, pg_advisory_lock, statsd
from motor_product.insurers import base_proposal_form
from concurrent.futures import TimeoutError


logger = logging.getLogger(__name__)

format_vehicle_type = lambda x: 'fourwheeler' if x == 'Private Car' else 'twowheeler'
cache = caches['motor-results']

ACTIVITY_CREATE_QUOTE = 'MOTOR CREATE QUOTE'
ACTIVITY_REFRESH_QUOTE = 'MOTOR REFRESH QUOTE'

PREMIUM_TIMEOUT = 28
CAR_DEKHO_PREMIUM_TIMEOUT = 15
PREMIUM_SOFT_TIMEOUT = 25
PREMIUM_CACHE_TIME = 3600 * 24 * 7
PREMIUM_REFRESH_TIME = 3600
QUOTE_PREMIUM_CACHE_TIME = 3600

VEHICLE_TYPE_MAP = {
    'fourwheeler': 'Private Car',
    'twowheeler': 'Twowheeler',
}

VEHICLE_TYPE_SLUG = {
    'Private Car': 'fourwheeler',
    'Twowheeler': 'twowheeler',
}

FOURWHEELER_INSURER_INTEGRATION_MAP = {
    'bharti-axa': bharti_axa.IntegrationUtils('Private Car'),
    'iffco-tokio': iffco_tokio.IntegrationUtils('Private Car'),
    'bajaj-allianz': bajaj_allianz.IntegrationUtils('Private Car'),
    'hdfc-ergo': hdfc_ergo.IntegrationUtils('Private Car'),
    'l-t': landt,
    'universal-sompo': universal_sompo.IntegrationUtils('Private Car'),
    'new-india': new_india.IntegrationUtils('Private Car'),
    'oriental': oriental.IntegrationUtils('Private Car'),
    'liberty-videocon': liberty_videocon,
    'tata-aig': tata_aig,
    'future-generali': future_generali.IntegrationUtils(),
    'icici-lombard': icici_lombard.IntegrationUtils('Private Car'),
    'reliance': reliance.IntegrationUtils('Private Car'),
}

TWOWHEELER_INSURER_INTEGRATION_MAP = {
    'bharti-axa': bharti_axa.IntegrationUtils('Twowheeler'),
    'iffco-tokio': iffco_tokio.IntegrationUtils('Twowheeler'),
    'bajaj-allianz': bajaj_allianz.IntegrationUtils('Twowheeler'),
    'universal-sompo': universal_sompo.IntegrationUtils('Twowheeler'),
    'hdfc-ergo': hdfc_ergo.IntegrationUtils('Twowheeler'),
    'new-india': new_india.IntegrationUtils('Twowheeler'),
    'oriental': oriental.IntegrationUtils('Twowheeler'),
    'reliance': reliance.IntegrationUtils('Twowheeler'),
}

VEHICLE_INTEGRATION_MAP = {
    'twowheeler': TWOWHEELER_INSURER_INTEGRATION_MAP,
    'fourwheeler': FOURWHEELER_INSURER_INTEGRATION_MAP,
}

RTO_STATE_TO_STATE_NAME_MAPPING = {
    'AN': 'andaman_and_nicobar_islands',
    'AP': 'andhra_pradesh',
    'AR': 'arunachal_pradesh',
    'AS': 'assam',
    'BR': 'bihar',
    'CG': 'chhattisgarh',
    'CH': 'chandigarh',
    'DD': 'daman_and_diu',
    'DL': 'delhi',
    'DN': 'dadra_and_nagar_haveli',
    'GA': 'goa',
    'GJ': 'gujarat',
    'HP': 'himachal_pradesh',
    'HR': 'haryana',
    'JH': 'jharkhand',
    'JK': 'jammu_and_kashmir',
    'KA': 'karnataka',
    'KL': 'kerala',
    'LD': 'lakshadweep',
    'MH': 'maharashtra',
    'ML': 'meghalaya',
    'MN': 'manipur',
    'MP': 'madhya_pradesh',
    'MZ': 'mizoram',
    'NL': 'nagaland',
    'OD': 'orissa',
    'PB': 'punjab',
    'PY': 'pondicherry',
    'RJ': 'rajasthan',
    'TN': 'tamil_nadu',
    'TS': 'telangana',
    'TR': 'tripura',
    'UA': 'uttaranchal',
    'UK': 'uttarakhand',
    'UP': 'uttar_pradesh',
    'WB': 'west_bengal',
    'SK': 'sikkim'
}

STATE_NAME_TO_STATE_ID = {v: k for k, v in STATE_ID_TO_STATE_NAME.items()}
STATE_NAME_TO_STATE_ID.update({'CHHATTISGARH': '15'})
STATE_NAME_TO_STATE_ID.update({'ANDAMAN AND NICOBAR': '2'})


def get_vehicles(vehicle_type, model_id, fuel_type):

    if model_id is None:
        models = Model.objects.filter(
            vehicle_type=VEHICLE_TYPE_MAP[vehicle_type])
        model_list = []
        for model in models:
            model_list.append(
                {'id': model.id, 'name': model.make.name + ' ' + model.name})
        return {'success': True, 'errorCode': None, 'errorMessage': None, 'errorItems': [], 'data': {'models': model_list}}
    else:
        try:
            model = Model.objects.get(id=model_id)
        except:
            return {'success': False, 'errorCode': errors.EC_INVALID_PARAMETERS, 'errorMessage': errors.messages[errors.EC_INVALID_PARAMETERS], 'errorItems': [{'parameter': 'model_id', 'errorMessage': 'Invalid model id.'}], 'data': {}}
        if fuel_type is None:
            vehicles = VehicleMaster.objects.filter(model=model)
        else:
            vehicles = VehicleMaster.objects.filter(
                model=model, fuel_type=fuel_type)

        variant_list = []
        for vehicle in vehicles:
            vehicle_name = vehicle.variant + ' (' + str(vehicle.cc) + ' CC) '
            variant_list.append({'id': vehicle.id, 'name': vehicle_name, 'fuel_type': vehicle.fuel_type})
        return {'success': True, 'errorCode': None, 'errorMessage': None, 'errorItems': [], 'data': {'variants': variant_list}}


def premium_fetch(vehicle_type, request_data, insurer, store):
    integration_statistics = IntegrationStatistics(
        insurer=Insurer.objects.get(slug=insurer),
        quote=Quote.objects.get(quote_id=request_data['quoteId']),
        vehicle_type=vehicle_type,
        rto_master=RTOMaster.objects.get(rto_code="-".join(request_data['registrationNumber[]'][0:2])),
        master_vehicle=VehicleMaster.objects.get(id=request_data['vehicleId']),
        status=False
    )
    try:
        m = VEHICLE_INTEGRATION_MAP[vehicle_type][insurer]
        master_vehicle_id = request_data['vehicleId']
        master_vehicle = VehicleMaster.objects.get(id=master_vehicle_id)
        vehicles = master_vehicle.sub_vehicles.filter(insurer__slug=insurer)
        if len(vehicles) > 0:
            vehicle_id = vehicles[0].id
        else:
            if not hasattr(m, 'no_custom_vehicles'):
                motor_logger.info(
                    '======== NO VEHICLE MAPPING FOUND FOR INSURER - %s, VEHICLE - %s ===========' % (insurer, master_vehicle))
                integration_statistics.required_time = 0
                integration_statistics.status = False
                integration_statistics.error_code = IntegrationStatistics.VEHICLE_MAPPING_NOT_FOUND
                integration_statistics.save()
                if vehicle_type == 'twowheeler':
                    return {'insurerId': Insurer.objects.get(slug=insurer).id, 'insurerSlug': insurer, 'errorCode': 'VEHICLE_MAPPING_NOT_FOUND', 'errorMsg': errors.INTEGRATION_ERRORS['VEHICLE_MAPPING_NOT_FOUND']}
                return None
            else:
                vehicle_id = master_vehicle_id

        request_data = insurer_utils.add_all_addons(request_data)
        data_dictionary = m.convert_premium_request_data_to_required_dictionary(
            vehicle_id, request_data)
        data_dictionary['isForListing'] = True
        data_dictionary['quoteId'] = request_data['quoteId']

        start_time = time.time()
        try:
            result_data = m.PremiumCalculator.get_premium(data_dictionary)
        except TimeoutError:
            integration_statistics.required_time = time.time() - start_time
            integration_statistics.error_code = IntegrationStatistics.PREMIUM_TIMEOUT
            integration_statistics.save()
            if vehicle_type == 'twowheeler':
                return {'insurerId': Insurer.objects.get(slug=insurer).id, 'insurerSlug': insurer, 'errorCode': 'PREMIUM_TIMEOUT', 'errorMsg': errors.INTEGRATION_ERRORS['UNKNOWN_ERROR']}
        except:
            integration_statistics.required_time = time.time() - start_time
            integration_statistics.error_code = IntegrationStatistics.UNKNOWN_ERROR
            integration_statistics.save()
            log_error(motor_logger, msg="Failed to fetch premium for unknown reason.")
            if vehicle_type == 'twowheeler':
                return {'insurerId': Insurer.objects.get(slug=insurer).id, 'insurerSlug': insurer, 'errorCode': 'UNKNOWN_ERROR', 'errorMsg': errors.INTEGRATION_ERRORS['UNKNOWN_ERROR']}
        else:
            integration_statistics.required_time = time.time() - start_time
            if result_data['success']:
                resp, raw_resp = result_data['result']
                # Temporary hack to modify response for proper ncb
                resp = insurer_utils.modify_response_for_ncb(resp, request_data)
                integration_statistics.status = True
                integration_statistics.save()
                return resp
            else:
                integration_statistics.error_code = result_data['error_code']
                integration_statistics.save()
                if vehicle_type == 'twowheeler':
                    return {'insurerId': Insurer.objects.get(slug=insurer).id, 'insurerSlug': insurer, 'errorCode': result_data['error_code'], 'errorMsg': errors.INTEGRATION_ERRORS[result_data['error_code']]}

    except SoftTimeLimitExceeded:
        motor_logger.info(
            '======== TIME LIMIT EXCEEDED FOR REQUEST OF INSURER - %s ========'
            % insurer
        )
        integration_statistics.required_time = 0
        integration_statistics.error_code = IntegrationStatistics.PREMIUM_TIMEOUT
        integration_statistics.save()
        if vehicle_type == 'twowheeler':
            return {'insurerId': Insurer.objects.get(slug=insurer).id, 'insurerSlug': insurer, 'errorCode': 'PREMIUM_TIMEOUT', 'errorMsg': errors.INTEGRATION_ERRORS['PREMIUM_TIMEOUT']}

    except Exception as e:
        log_error(logger, msg=e)
        if vehicle_type == 'twowheeler':
            return {'insurerId': Insurer.objects.get(slug=insurer).id, 'insurerSlug': insurer, 'errorCode': 'UNKNOWN_ERROR', 'errorMsg': errors.INTEGRATION_ERRORS['UNKNOWN_ERROR']}
        return None


@app.task(
    time_limit=PREMIUM_TIMEOUT, soft_time_limit=PREMIUM_SOFT_TIMEOUT,
    expires=PREMIUM_TIMEOUT
)
def get_premium(vehicle_type, request_data, insurer, store=True):
    _gtimer = GrafanaStatsd.get_timer('%s.search' % insurer, vehicle_type)
    response = premium_fetch(vehicle_type, request_data, insurer, store)
    set_premium(response, vehicle_type, request_data, insurer, store=store)
    _gtimer.stop()
    return response


def set_premium(response, vehicle_type, request_data, insurer, store=True):
    quote_obj = Quote.objects.get(quote_id=request_data['quoteId'])
    cached_data = {'isfetched': True, 'data': response}

    # Creating quote channel based on quote id and insurer
    quote_obj.put_channel(cached_data, insurer)
    request_data['insurer_slug'] = insurer

    # Updating l2 cache
    if response and 'errorCode' not in response:
        fetch_obj = PremiumTableCache(request_data)
        fetch_obj.set_premium(cached_data)
        if store:
            # Updating l1 cache
            cached_data = {'data': response, 'created': datetime.now().strftime('%d-%m-%Y %H:%M:%S')}
            QueryCache.set_premium(request_data, vehicle_type, insurer, cached_data)


@app.task(rate_limit='30/m', ignore_result=True)
def get_premium_cron(vehicle_type, request_data, insurer):
    # print '-------------------------------------------'
    # print vehicle
    # print '-------------------------------------------'
    # print insurer
    # print '-------------------------------------------'
    response = premium_fetch(vehicle_type, request_data, insurer, True)
    return response


def create_quote(vehicle_type, request_data, wait_for_results=True):
    _gtimer = GrafanaStatsd.get_timer('search', vehicle_type)
    premium_request = request_data['data']
    third_party = premium_request['third_party']

    # Temporary hack for used vehicle (only for quote)
    premium_request = insurer_utils.modify_parameters_for_used_vehicle(
        premium_request)
    quote = Quote.objects.create_quote_from_request_dict(premium_request)

    quote_id = quote.quote_id
    # Added for logging quote request
    request_data['data']['quoteId'] = quote_id

    premiums = []

    intermediate_results = []

    if vehicle_type == 'fourwheeler':
        INSURERS = Insurer.objects.filter(is_active_for_car=True)
    else:
        INSURERS = Insurer.objects.filter(is_active_for_twowheeler=True)

    user = request_data['user']
    is_corporate = False
    if user.is_authenticated() and user.extra.get('corporate', False):
        is_corporate = True

    is_dealer = False
    if user.is_authenticated():
        for employee in user.employee_instance.all():
            if employee.has_perm('is_dealer'):
                is_dealer = True

    for ins in INSURERS.values_list('slug', flat=True):
        # Temporary hack to restrict dealer to just 3 insurers
        if is_dealer and ins not in ['l-t', 'bharti-axa', 'icici-lombard']:
            continue

        # Temporary hack for restricting quotes for cardekho
        cd_insurers = [
            'l-t',
            'universal-sompo',
            'bharti-axa',
            'reliance',
            'bajaj-allianz',
            'icici-lombard'
        ]
        if third_party == 'cardekho' and ins not in cd_insurers:
            continue

        if is_corporate and ins in [
            'oriental', 'iffco-tokio', 'bharti-axa', 'new-india',
            'bajaj-allianz', 'tata-aig', 'future-generali', 'reliance',
            'liberty-videocon'
        ]:
            continue
        resp = None
        fetch_result = True
        insurer = INSURERS.get(slug=ins)
        if not insurer.disable_cache(VEHICLE_TYPE_MAP[vehicle_type]):
            resp = QueryCache.get_premium(request_data['data'], vehicle_type, ins)

        # Disable cache for certain requests as per insurer
        m = VEHICLE_INTEGRATION_MAP[vehicle_type][ins]
        if (
            hasattr(m, 'disable_cache_for_request') and
            m.disable_cache_for_request(request_data['data'])
        ):
            resp = None
        if resp is None:
            try:
                data_dict = request_data['data']
                data_dict['insurer_slug'] = ins
                premium_cache_obj = PremiumTableCache(data_dict)
                resp = premium_cache_obj.get_premium()
                get_premium.apply_async([vehicle_type, data_dict, ins], queue='motor_premium_refresh')
                if resp:
                    statsd.incr('coverfox.motor.cache.served', 1)
            except Exception as e:
                motor_logger.info("Error %s occurred while serving cached data from Premium Table Cache" % e)
        if resp is not None:
            fetch_result = False
            created_date = datetime.strptime(
                resp['created'], '%d-%m-%Y %H:%M:%S')

            # Check if result expired for insurer
            insurer_expiry_date_str = cache.get(ins + '_expiry_date')
            if insurer_expiry_date_str:
                insurer_expiry_date = datetime.strptime(
                    insurer_expiry_date_str, '%d-%m-%Y %H:%M:%S')
                if insurer_expiry_date > created_date:
                    fetch_result = True

            if (
                created_date - datetime.now()
            ).total_seconds() > PREMIUM_REFRESH_TIME:
                result = get_premium.apply_async(
                    [vehicle_type, request_data['data'], ins],
                    queue='motor_premium_refresh'
                )

            if not fetch_result:
                motor_logger.info('Fetched response from cache. ' + ins)
                if wait_for_results:
                    premiums.append(resp['data'])
                else:
                    cached_data = {'isfetched': True, 'data': resp['data']}
                    quote.put_channel(cached_data, ins)

        if fetch_result:
            cached_data = {'isfetched': False}
            quote.put_channel(cached_data, ins)
            result = get_premium.apply_async(
                [vehicle_type, request_data['data'], ins], queue='motor_%s' % ins.replace('-', '_'))
            intermediate_results.append(result)

    if wait_for_results:
        for result in intermediate_results:
            try:
                premium_timeout = CAR_DEKHO_PREMIUM_TIMEOUT if third_party == 'cardekho' else PREMIUM_TIMEOUT
                result_output = result.wait(timeout=premium_timeout, interval=0.5)
                if result_output is not None and not result_output.get('errorCode'):
                    premiums.append(result_output)
            except Exception as e:
                log_error(logger, msg=e)

        premiums = get_flat_premium(premium_request, premiums)

        quote_logger.info(json.dumps({
            'type': 'quote',
            'id': quote_id,
            'data': premiums,
        }))

    _gtimer.stop()

    from . import forms
    try:
        if vehicle_type == "twowheeler":
            form_type = forms.BikeQuoteFDForm
            product = "bike"
        else:
            form_type = forms.CarQuoteFDForm
            product = "car"

        quote_form_instance = form_type(
            data=get_form_data(request_data.get("data", {}))
        )

        core_activity.create_activity(
            activity_type=core_activity.VIEWED,
            product=product,
            page_id='quotes',
            form_position='',
            form_name='',
            form_variant='',
            data=quote_form_instance.get_fd() if quote_form_instance else {},
            extra={'quoteId': quote_id}
        )

        # if activity and activity.requirement:
        #     req = activity.requirement
        #     req.update_extra(request_data)
        #     req.save(update_fields=['extra'])
    except:
        log_error(logger, msg='Unable to track quote event')

    return {
        'success': True,
        'errorCode': None,
        'errorMessage': None,
        'errorItems': [],
        'data': {
            'quoteId': quote_id,
            'premiums': premiums,
        }
    }


@app.task(ignore_result=True)
def _refresh_quote(vehicle_type, quote_id, quote_parameters, insurer, transaction_id=None, force_quote_refresh="false"):
    quote = Quote.objects.get(pk=quote_id)
    transaction = Transaction.objects.get(pk=transaction_id) if transaction_id else None

    m = VEHICLE_INTEGRATION_MAP[vehicle_type][insurer]
    if quote_parameters:
        # Temporary hack for used vehicle (only for quote)
        quote_parameters = insurer_utils.modify_parameters_for_used_vehicle(
            quote_parameters)

        quote.raw_data = quote_parameters
        quote.save()
    quote_parameters = quote.raw_data

    if (transaction and transaction.raw.get('quote_raw_response', None) and ((time.time() - transaction.raw.get('last_quote_refresh', 0)) <= PREMIUM_REFRESH_TIME)) and (force_quote_refresh != 'true'):
        quote_details = {
            'quoteId': quote.quote_id,
            'insurer': insurer,
            'quote_parameters': quote_parameters,
            'quoteResponse': transaction.raw['quote_response'],
            'quoteRawResponse': transaction.raw['quote_raw_response']
        }
        return quote_details

    master_vehicle_id = quote_parameters['vehicleId']
    master_vehicle = VehicleMaster.objects.get(id=master_vehicle_id)
    vehicles = master_vehicle.sub_vehicles.filter(insurer__slug=insurer)

    integration_statistics = IntegrationStatistics(
        insurer=Insurer.objects.get(slug=insurer),
        quote=Quote.objects.get(quote_id=quote_parameters['quoteId']),
        vehicle_type=vehicle_type,
        rto_master=RTOMaster.objects.get(rto_code="-".join(quote_parameters['registrationNumber[]'][0:2])),
        master_vehicle=VehicleMaster.objects.get(id=quote_parameters['vehicleId']),
        status=False
    )

    if len(vehicles) > 0:
        vehicle_id = vehicles[0].id
    elif hasattr(m, 'no_custom_vehicles'):
        vehicle_id = master_vehicle_id
    else:
        motor_logger.info(
            '======== NO VEHICLE MAPPING FOUND FOR INSURER - %s, VEHICLE - %s ===========' % (insurer, master_vehicle))
        integration_statistics.required_time = 0
        integration_statistics.status = False
        integration_statistics.error_code = IntegrationStatistics.VEHICLE_MAPPING_NOT_FOUND
        integration_statistics.save()
        if vehicle_type == 'twowheeler':
            return {'insurerId': Insurer.objects.get(slug=insurer).id, 'insurerSlug': insurer, 'errorCode': 'VEHICLE_MAPPING_NOT_FOUND', 'errorMsg': errors.INTEGRATION_ERRORS['VEHICLE_MAPPING_NOT_FOUND']}
        return None

    quote_parameters['quoteId'] = quote.quote_id
    get_premium.apply_async(
        [vehicle_type, quote_parameters, insurer], queue='motor_premium_refresh')

    quote_parameters = insurer_utils.add_dependent_addons(
        quote_parameters, m.get_dependent_addons(quote_parameters))
    data_dictionary = m.convert_premium_request_data_to_required_dictionary(
        vehicle_id, quote_parameters)
    data_dictionary['quoteId'] = quote.quote_id
    start_time = time.time()
    resp = None
    raw_resp = None
    quote_details = None
    try:
        result_data = m.PremiumCalculator.get_premium(data_dictionary)
    except TimeoutError:
        integration_statistics.required_time = time.time() - start_time
        integration_statistics.error_code = IntegrationStatistics.PREMIUM_TIMEOUT
        integration_statistics.save()
        if vehicle_type == 'twowheeler':
            return {'insurerId': Insurer.objects.get(slug=insurer).id, 'insurerSlug': insurer, 'errorCode': 'PREMIUM_TIMEOUT', 'errorMsg': errors.INTEGRATION_ERRORS['PREMIUM_TIMEOUT']}
    except:
        integration_statistics.required_time = time.time() - start_time
        integration_statistics.error_code = IntegrationStatistics.UNKNOWN_ERROR
        integration_statistics.save()
        log_error(motor_logger, msg="Failed to fetch premium for unknown reason.")
        if vehicle_type == 'twowheeler':
            return {'insurerId': Insurer.objects.get(slug=insurer).id, 'insurerSlug': insurer, 'errorCode': 'UNKNOWN_ERROR', 'errorMsg': errors.INTEGRATION_ERRORS['UNKNOWN_ERROR']}
    else:
        integration_statistics.required_time = time.time() - start_time
        if result_data['success']:
            resp, raw_resp = result_data['result']
            integration_statistics.status = True
        else:
            integration_statistics.error_code = result_data['error_code']
            if vehicle_type == 'twowheeler':
                return {'insurerId': Insurer.objects.get(slug=insurer).id, 'insurerSlug': insurer, 'errorCode': result_data['error_code'], 'errorMsg': errors.INTEGRATION_ERRORS[result_data['error_code']]}
    finally:
        integration_statistics.save()
    if resp:
        # Temporary hack to modify response for proper ncb
        resp = insurer_utils.modify_response_for_ncb(resp, quote_parameters)
        resp = get_flat_premium(quote_parameters, [resp])[0]
        quote_details = {'quoteId': quote.quote_id, 'insurer': insurer,
                         'quote_parameters': quote_parameters, 'quoteResponse': resp, 'quoteRawResponse': raw_resp}
        if transaction:
            lock_name = "[motor]-refresh_quote-%s" % transaction.id
            with pg_advisory_lock(lock_name):
                transaction.refresh_from_db()
                transaction.raw['quote_response'] = quote_details.get('quoteResponse')
                transaction.raw['quote_raw_response'] = quote_details.get('quoteRawResponse')
                transaction.raw['last_quote_refresh'] = time.time()
                transaction.save(update_fields=['raw'])
    return quote_details


def refresh_transaction_details(transaction):

    reverese_vehicle_map = {v: k for k, v in VEHICLE_TYPE_MAP.items()}
    vehicle_type = reverese_vehicle_map[transaction.vehicle_type]

    quote_details = _refresh_quote(
        vehicle_type, transaction.quote.id, None, transaction.insurer.slug, transaction.id, force_quote_refresh='true')
    if not quote_details:
        return False
    quote_logger.info(json.dumps({
        'type': 'transaction_refresh',
        'transactionid': transaction.transaction_id,
    }))
    return True


def refresh_quote_details(
    vehicle_type, request_data, quote_id,
    quote_parameters, insurer, force_quote_refresh="false"
):
    try:
        quote = Quote.objects.get(quote_id=quote_id)
    except Quote.DoesNotExist:
        return False, errors.EC_INVALID_PARAMETERS, [{'parameter': 'quote_id', 'errorMessage': 'Invalid quote id.'}]

    quote_details = _refresh_quote(
        vehicle_type, quote.id, quote_parameters, insurer, force_quote_refresh=force_quote_refresh
    )

    if not quote_details:
        return False, errors.ES_CANNOT_FETCH_DATA, []

    if 'quoteResponse' not in quote_details:
        return False, quote_details.get('errorCode'), []

    tracker = request_data.TRACKER
    activity = Activity(tracker=tracker, activity_type=ACTIVITY_REFRESH_QUOTE)
    activity.extra = quote_details
    activity.save()

    motor_logger.info('Confirmed Quote: ' + quote_id +
                      ' ' + json.dumps(quote_details))
    return True, {'activityId': activity.activity_id, 'quote': quote_details['quoteResponse']}, []


def generate_form(vehicle_type, request_data, activity_id, quote_parameters):
    try:
        activity = Activity.objects.get(activity_id=activity_id)
    except:
        return None

    # TODO: Add check for updated_on date
    if not (activity.activity_type == ACTIVITY_REFRESH_QUOTE):
        return None

    quote_details = activity.extra
    quote_id = quote_details.get('quoteId')
    insurer_slug = quote_details.get('insurer')
    quote_response = quote_details.get('quoteResponse')
    quote_raw_response = quote_details.get('quoteRawResponse')

    try:
        quote = Quote.objects.get(quote_id=quote_id)
        insurer = Insurer.objects.get(slug=insurer_slug)
    except:
        return None

    # if not (quote_parameters is None):
    #     quote.raw_data = quote_parameters
    #     quote.save()

    transaction = Transaction(
        tracker=activity.tracker,
        quote=quote,
        insurer=insurer,
        vehicle_type=VEHICLE_TYPE_MAP[vehicle_type],
        status='DRAFT',
    )

    if transaction.transaction_type == 'car':
        act_prod = RequirementKind.CAR
    else:
        act_prod = RequirementKind.BIKE

    act = core_activity.create_activity(
        activity_type=core_activity.VIEWED,
        product=act_prod,
        page_id='proposal',
        form_name='',
        form_position='',
        form_variant='',
        data={}
    )

    transaction.raw['quote_response'] = quote_response
    transaction.raw['quote_raw_response'] = quote_raw_response

    # quote_response_json = json.loads(quote_response)
    # quote_response_json = json.dumps(quote_response_json, parse_int=str)

    # quote_raw_response_json = json.loads(quote_raw_response)
    # quote_raw_response_json = json.dumps(quote_raw_response_json, parse_int=str)

    # form_data.sections.add(
    #     FormSection.objects.create(form='quote_response', data=quote_response_json))

    # form_data.sections.add(
    #     FormSection.objects.create(form='quote_raw_response', data=quote_raw_response_json))
    if (
        request_data.user.is_authenticated()
        and request_data.user.is_dealer()
        and request_data.session.get('dealer_dealership_id', False)
    ):
        if act:
            req_dealer_map = RequirementDealerMap.objects.create(
                requirement=act.requirement,
                dealer_id=request_data.session['dealer_dealership_id'])
            ignore_keys = ["pa_passenger", "service_tax", "basicTp", "cngKit", "paOwnerDriver", "pa_passenger"]
            comission_value = get_flat_premium(
                transaction.quote.raw_data,
                [transaction.raw['quote_response']],
                ignore_keys=ignore_keys)[0]['final_premium']
            req_dealer_map.commission_value = (comission_value * req_dealer_map.dealer.get_comission_percentage()) / 100
            req_dealer_map.save()
        else:
            logger.info("Requirement not created because no requirement created.")

    transaction.requirement = act.requirement
    transaction.save(update_fields=['raw', 'requirement'])

    # Transaction attached to discount code used
    customer_discount_code = quote.raw_data.get('discountCode', None)
    discount_code = insurer_utils.get_discount_code(customer_discount_code)
    if discount_code:
        discount_code.transaction = transaction
        discount_code.save()

    # TODO: Check the use of this
    quote.is_processed = True
    quote.save()

    quote_logger.info(json.dumps({
        'type': 'quote_transaction',
        'quoteid': quote_id,
        'transactionid': transaction.transaction_id,
    }))

    return transaction.transaction_id


def generate_form_simply(vehicle_type, request, quote_id, quote_parameters, insurer_slug, initial_data=None):
    try:
        reference_quote = Quote.objects.get(quote_id=quote_id)
    except Quote.DoesNotExist:
        return False, errors.EC_INVALID_PARAMETERS, [{'parameter': 'quote_id', 'errorMessage': 'Invalid quote id.'}]
    quote_parameters['corporate_employee_discount'] = reference_quote.raw_data.get('corporate_employee_discount')
    quote = Quote.objects.create_copy_quote(reference_quote, raw_data=quote_parameters)
    try:
        insurer = Insurer.objects.get(slug=insurer_slug)
    except Insurer.DoesNotExist:
        return None

    transaction = Transaction.objects.create(
        tracker=request.TRACKER,
        quote=quote,
        insurer=insurer,
        vehicle_type=VEHICLE_TYPE_MAP[vehicle_type],
        status='DRAFT',
    )
    transaction.track_activity(
        activity_type=core_activity.VIEWED,
        page_id='proposal',
    )

    user_form_details = {}
    new_user_form_details = {}

    if initial_data:
        user_form_details.update(initial_data)

    if quote.raw_data.get('rds_id') not in ['false', None, '']:
        rds = RegistrationDetailStatistics.objects.get(id=quote_parameters.get('rds_id'))
        form_details = insurer_user_form_validations.prefill_fastlane_vehicle_data(
            insurer, rds, copy.deepcopy(transaction.raw.get('user_form_details', {}))
        )
        # TODO: Remove this once we move to new car flow
        if transaction.vehicle_type == 'Twowheeler':
            new_user_form_details.update(form_details)
        # else:
        user_form_details.update(form_details)

    transaction.raw['user_form_details'] = user_form_details
    transaction.raw['new_user_form_details'] = new_user_form_details
    transaction.raw['previous_quote_id'] = reference_quote.quote_id
    transaction.save(update_fields=['raw'])

    # Calling quote refresh asynchronously to update and validate the quote parameters.
    if 'quote_raw_response' not in transaction.raw:
        previous_quote = Quote.objects.get(quote_id=transaction.raw['previous_quote_id'])
        cached_quote_response = previous_quote.get_channel(insurer_slug)
        if cached_quote_response:
            cached_quote_response = cached_quote_response['data']
            transaction.raw['quote_response'] = cached_quote_response
            transaction.save(update_fields=['raw'])
            # TODO: Use this task id and next time complete this task by .get instead of re-initiating the request.
            _refresh_quote.apply_async([vehicle_type, quote.id, quote_parameters, insurer_slug, transaction.id])
        else:
            _refresh_quote(vehicle_type, quote.id, quote_parameters, insurer_slug, transaction.id)
    if (
        request.user.is_authenticated()
        and request.user.is_dealer()
        and request.session.get('dealer_dealership_id', False)
        and transaction.requirement
        # TODO skipping it for now, added it in motor views desktopFormApi. Has to be handled properly
    ):
        req_dealer_map = RequirementDealerMap.objects.create(
            requirement=transaction.requirement,
            dealer_id=request.session['dealer_dealership_id'])
        ignore_keys = ["pa_passenger", "service_tax", "basicTp", "cngKit", "paOwnerDriver", "pa_passenger"]
        comission_value = get_flat_premium(
            transaction.quote.raw_data,
            [transaction.raw['quote_response']],
            ignore_keys=ignore_keys)[0]['final_premium']
        req_dealer_map.commission_value = (comission_value * req_dealer_map.dealer.get_comission_percentage()) / 100
        req_dealer_map.save()

    # Transaction attached to discount code used
    customer_discount_code = quote.raw_data.get('discountCode', None)
    discount_code = insurer_utils.get_discount_code(customer_discount_code)
    if discount_code:
        discount_code.transaction = transaction
        discount_code.save()

    # TODO: Check the use of this
    quote.is_processed = True
    quote.save()
    quote_logger.info(json.dumps({
        'type': 'quote_transaction',
        'quoteid': quote_id,
        'transactionid': transaction.transaction_id,
    }))

    return transaction.transaction_id


def get_buy_form_details(transaction_id, api_call=False):

    transaction = Transaction.objects.get(transaction_id=transaction_id)

    insurer = transaction.insurer.slug
    vehicle_type = format_vehicle_type(transaction.vehicle_type)
    m = VEHICLE_INTEGRATION_MAP[vehicle_type][insurer]

    quote_parameters = transaction.quote.raw_data
    quote_parameters = insurer_utils.add_dependent_addons(
        quote_parameters, m.get_dependent_addons(quote_parameters))

    buy_form_details = m.get_buy_form_details(quote_parameters, transaction)

    master_vehicle = transaction.quote.vehicle
    if master_vehicle is None:
        return None

    vehicle = master_vehicle.sub_vehicles.filter(insurer__slug=insurer).last()
    if vehicle:
        vehicle_make = vehicle.make
        vehicle_model = vehicle.model
        vehicle_variant = vehicle.variant
    elif hasattr(m, 'no_custom_vehicles'):
        vehicle_make = master_vehicle.make.name
        vehicle_model = master_vehicle.model.name
        vehicle_variant = master_vehicle.variant
    else:
        motor_logger.info(
            '======== NO VEHICLE MAPPING FOUND FOR INSURER - %s, VEHICLE - %s ===========' % (
                insurer, master_vehicle
            ))
        return None

    buy_form_details['make'] = vehicle_make
    buy_form_details['model'] = vehicle_model
    buy_form_details['variant'] = vehicle_variant

    buy_form_details['master_make'] = master_vehicle.make.name
    buy_form_details['master_model'] = master_vehicle.model.name
    buy_form_details['master_variant'] = master_vehicle.variant + ' (' + str(master_vehicle.cc) + ' CC) '

    buy_form_details['rto_info'] = transaction.quote.rto_info

    if vehicle_type == 'twowheeler' and api_call:
        user_form_details = transaction.raw.get('new_user_form_details', {})
    else:
        user_form_details = transaction.raw.get('user_form_details', {})

    if transaction.status in ["DRAFT", "PENDING"] and transaction.quote.accepted_fastlane:
        rd = RegistrationDetailStatistics.objects.get(id=transaction.quote.raw_data.get('rds_id')).registration_detail
        if not user_form_details.get('past_policy_insurer'):
            past_policy_insurer, autopopulated_past_policy_insurer = rd.get_fastlane_previous_insurer_master_id(
                buy_form_details.get('insurers_list', {})
            )

            user_form_details['past_policy_insurer'] = past_policy_insurer
            user_form_details['autopopulated_past_policy_insurer'] = autopopulated_past_policy_insurer

        financiers = buy_form_details.get('financier_list', {})
        is_financed, financier_id, financier_name = rd.get_fastlane_financier_details(financiers)

        # TODO: Remove when move to car new flow
        if vehicle_type == 'twowheeler' and not user_form_details.get('financier-name'):
            user_form_details['financier-name'] = financier_name
            if financiers:
                user_form_details['financier-name'] = financier_id
            user_form_details['autopopulated_car_financer'] = True

        elif not (user_form_details.get('car-financier') and user_form_details.get('car-financier-text')):
            user_form_details['car-financier-text'] = financier_name
            if financiers:
                user_form_details['car-financier'] = financier_id
                user_form_details['autopopulated_car_financer'] = True

    if vehicle_type == 'twowheeler' and api_call:
        # PastPolicyExpiryDate is not available in case of new vehicle and should not be fetched

        if quote_parameters.get('isNewVehicle') == "0":
            if 'past_policy_expiry_date' not in user_form_details:
                user_form_details['past_policy_expiry_date'] = transaction.quote.raw_data['pastPolicyExpiryDate']

        if 'previous_ncb' not in user_form_details:
            user_form_details['previous_ncb'] = transaction.quote.raw_data['previousNCB']
            user_form_details['claimed_last_year'] = "0"

        if 'registration_date' not in user_form_details:
            user_form_details['registration_date'] = transaction.quote.raw_data['registrationDate']

        if 'vehicle_reg_no' not in user_form_details:
            if transaction.quote.is_fastlane_flow:
                user_form_details['vehicle_reg_no'] = '-'.join(transaction.quote.raw_data['registrationNumber[]'])
            else:
                user_form_details['vehicle_reg_no'] = '-'.join(transaction.quote.raw_data['registrationNumber[]'][:2])

        if 'vehicle_reg_no' in user_form_details:
            vehicle_reg_no = user_form_details['vehicle_reg_no'].split('-')
            if vehicle_reg_no[0] not in ['DL', 'GJ', 'UK', 'UA', 'NEW'] and len(vehicle_reg_no[1]) == 1 and vehicle_reg_no[1].isdigit():
                vehicle_reg_no[1] = vehicle_reg_no[1].rjust(2, '0')

            if vehicle_reg_no[0] in ['DL'] and len(vehicle_reg_no) > 3 and len(vehicle_reg_no[1]) == 1 and vehicle_reg_no[2][0] in ['C', 'S']:
                vehicle_reg_no[1], vehicle_reg_no[2] = vehicle_reg_no[1] + vehicle_reg_no[2][0], vehicle_reg_no[2][1:]

            user_form_details['vehicle_reg_no'] = "-".join(vehicle_reg_no)

        selected_state = reg_selected_state = ''
        buy_form_details['state_list'] = buy_form_details.get('state_list', STATE_ID_TO_STATE_NAME)

        if user_form_details.get('add_state'):
            selected_state = user_form_details.get('add_state')
        elif transaction.quote.rto:
            rto_state = RTO_STATE_TO_STATE_NAME_MAPPING[transaction.quote.rto.split('-')[0]]
            state_list = buy_form_details['state_list']
            if transaction.insurer.slug == 'hdfc-ergo':
                state_list['15'] = 'CHHATTISGARH'
            if type(state_list) == list:
                state_list = {e: e for e in state_list}
            for state, state_id in state_list.items():
                if '_'.join(state_id.split(' ')).lower()[:6] == rto_state[:6]:
                    selected_state = reg_selected_state = state
                    break
            user_form_details['add_state'] = selected_state

        if user_form_details.get('reg_add_state'):
            reg_selected_state = user_form_details.get('reg_add_state')
            user_form_details['reg_add_state'] = reg_selected_state

        if transaction.quote.is_fastlane_flow:
            user_form_details.update({'add_state': selected_state})
            user_form_details.update({'reg_add_state': reg_selected_state})

        if selected_state:
            _, buy_form_details['cities'] = get_cities(vehicle_type, transaction.insurer.slug, selected_state)

        user_form_details['is_car_financed'] = '1' if user_form_details.get('financier-name') else '0'

        buy_form_details['form'] = base_proposal_form.generate_final_form(
            buy_form_details=buy_form_details,
            transaction=transaction,
            user_form_details=user_form_details)

    # WORKAROUND TO APPEND PA_PASSENGER AMOUNT TO CACHED DATA.
    extra_pa_passenger = quote_parameters.get('extra_paPassenger', 0)
    quote_response = get_flat_premium(
        transaction.quote.raw_data,
        [transaction.raw['quote_response']]
    )[0]

    if extra_pa_passenger not in [0, '0'] and transaction.status in ["DRAFT", "PENDING"]:
        pa_passenger_amount = quote_response['premiumBreakup']['paPassengerAmounts'][str(extra_pa_passenger)]
        for value in quote_response['premiumBreakup']['basicCovers']:
            if value['id'] == 'paPassenger':
                value['premium'] = pa_passenger_amount
                break
        else:
            quote_response['premiumBreakup']['basicCovers'].append({
                "default": 1,
                "premium": float(pa_passenger_amount),
                "id": "paPassenger",
                "name": "PA Unnamed Passenger"
            })

    return {
        'form_details': buy_form_details,
        'user_form_details': user_form_details,
        'quote_parameters': quote_parameters,
        'quote_response': quote_response
    }


def save_form_partial_details(request_data, transaction):
    try:
        lock_name = "[motor]-save_form_partial_details-%s" % transaction.id
        with pg_advisory_lock(lock_name):
            transaction.refresh_from_db()
            transaction.raw['user_form_details'] = transaction.raw.get('user_form_details', {})
            #  Updating only truthy values.. not the best approach
            transaction.raw['user_form_details'].update(dict(filter(lambda i: bool(i[1]), request_data.items())))
            transaction.save(update_fields=['raw'])
    except:
        return False
    return True


def new_save_form_partial_details(vehicle_type, request_data, insurer, transaction):
    today = datetime.now()
    try:
        lock_name = "[motor]-new_save_form_partial_details-%s" % transaction.id
        with pg_advisory_lock(lock_name):
            transaction.refresh_from_db()
            transaction.raw['new_user_form_details'] = transaction.raw.get('new_user_form_details', {})
            #  Updating only truthy values.. not the best approach
            transaction.raw['new_user_form_details'].update(dict(filter(lambda i: bool(i[1]), request_data.items())))
            expiry_date = transaction.raw['new_user_form_details'].get('past_policy_expiry_date', None)
            if expiry_date and (today - datetime.strptime(expiry_date, '%d-%m-%Y')).days >= 90:
                transaction.raw['new_user_form_details'].\
                    update({'past_policy_insurer': '19',
                            'past_policy_number': transaction.transaction_id[:8]})
            if transaction.raw['new_user_form_details'].get('is_car_financed') == '0':
                transaction.raw['new_user_form_details'].update({'financier-name': ''})
            if transaction.raw['new_user_form_details'].get('claimed_last_year') == '1':
                transaction.raw['new_user_form_details'].update({'previous_ncb': '0'})
            transaction.save(update_fields=['raw'])
    except:
        return False
    return True


def bifurcate_address(text, split=3):
    address_list = text.split(' ')
    slicer = len(address_list) / split
    if slicer == 0:
        house_no = ' '.join(address_list[0:])
    else:
        house_no = ' '.join(address_list[0:slicer])
    building_name = ' '.join(address_list[slicer: slicer*2])
    if split == 3:
        street_name = ' '.join(address_list[slicer*2:])
        district_name = ''
    else:
        street_name = ' '.join(address_list[slicer*2: slicer*3])
        district_name = ' '.join(address_list[slicer*3:])
    return house_no, building_name, street_name, district_name


def new_user_data_to_old_data(transaction):
    user_form_details = transaction.raw.get('user_form_details', {})
    for key, value in transaction.raw.get('new_user_form_details', {}).items():
        if key == 'cust_name':
            user_form_details['cust_first_name'] = ''.join(value.split(' ')[0:1])
            user_form_details['cust_last_name'] = ' '.join(value.split(' ')[1:])
        elif key == 'add_consoliated_field':
            add_house_no, add_building_name, add_street_name, district = bifurcate_address(value)
            user_form_details['add_house_no'] = add_house_no
            user_form_details['add_building_name'] = add_building_name
            user_form_details['add_street_name'] = add_street_name
            user_form_details['add_district'] = district
        elif key == 'reg_add_consoliated_field':
            add_house_no, add_building_name, add_street_name, district = bifurcate_address(value)
            user_form_details['reg_add_house_no'] = add_house_no
            user_form_details['reg_add_building_name'] = add_building_name
            user_form_details['reg_add_street_name'] = add_street_name
            user_form_details['reg_add_district'] = district
        elif key == 'financier-name':
            user_form_details['car-financier'] = value
        else:
            user_form_details[key] = value

    user_form_details['reg_add_pincode'] = user_form_details.get(
        'reg_add_pincode',
        user_form_details.get('add_pincode'))
    user_form_details['cust_pan'] = transaction.raw.get('new_user_form_details', {}).get('cust_pan', 'AAAP%s1234A' %
                                                                                         user_form_details.get(
                                                                                             'cust_last_name',
                                                                                             'A').split(' ')[-1][
                                                                                             0].upper())

    if not user_form_details.get('vehicle_reg_no'):
        user_form_details['vehicle_reg_no'] = '-'.join(transaction.quote.raw_data['registrationNumber[]'])

    if user_form_details.get('car-financier'):
        user_form_details['is_car_financed'] = '1'
    else:
        user_form_details['car-financier'] = ''
        user_form_details['is_car_financed'] = '0'

    if 'reg_add_state' in user_form_details and user_form_details.get('reg_add_state') == user_form_details.get('add_state'):
        user_form_details['add-same'] = '1'
    else:
        user_form_details['add-same'] = '0'

    if transaction.insurer.slug == 'reliance' and user_form_details.get('cust_marital_status', '0').isdigit():
        marital_status_map = {
            "0": "Unmarried",
            "1": "Married",
        }
        user_form_details['cust_marital_status'] = marital_status_map[
            str(user_form_details.get('cust_marital_status', '0'))
        ]

    transaction.raw['user_form_details'] = user_form_details
    transaction.save()
    return transaction


def _save_transaction_details(vehicle, request_data, transaction):
    # Save Address
    address = Address.objects.create(
        line_1=request_data.get('add_house_no', '') +
        ' ' + request_data.get('add_building_name', ''),
        line_2=request_data.get('add_street_name', ''),
        landmark=request_data.get('add_landmark', ''),
        city=request_data.get('add_city', ''),
        district=request_data.get('add_district', ''),
        state=request_data.get('add_state', ''),
        pincode=request_data.get('add_pincode', ''),
    )

    # Save Contact
    proposer = Contact.objects.create(
        address=address,
        email=request_data['cust_email'],
        mobile=request_data['cust_phone'],
        date_of_birth=datetime.strptime(
            request_data['cust_dob'], "%d-%m-%Y").date(),
        title=[['Mrs.', 'Ms.'][request_data['cust_marital_status'] == '0'], 'Mr.'][request_data['cust_gender'] == 'Male'],
        first_name=request_data['cust_first_name'],
        last_name=request_data['cust_last_name'],
        gender=request_data['cust_gender'],
        raw={'pan_number': request_data.get('cust_pan', '')},
        nationality='Indian',
    )

    # Save Private Car
    user_vehicle = UserVehicle.objects.create(
        raw={
            'vehicle': vehicle.id,
            'registration_number': request_data['vehicle_reg_no'],
            'engine_number': request_data['vehicle_engine_no'],
            'chassis_number': request_data['vehicle_chassis_no'],
        },
    )

    # Save Transaction Details
    transaction.proposer = proposer
    transaction.insured_vehicle = user_vehicle
    transaction.save(update_fields=['proposer', 'insured_vehicle'])


@retry(max_tries=1)
def process_save_transaction(request_data, transaction, proposal_request_data=None):
    if proposal_request_data is None:
        proposal_request_data = transaction.raw['user_form_details']
    vehicle_type = format_vehicle_type(transaction.vehicle_type)
    insurer = transaction.insurer.slug
    m = VEHICLE_INTEGRATION_MAP[vehicle_type][insurer]
    if transaction.status == 'COMPLETED':
        return None, False
    quote_parameters = transaction.quote.raw_data

    master_vehicle_id = quote_parameters['vehicleId']
    master_vehicle = VehicleMaster.objects.get(id=master_vehicle_id)
    vehicle = master_vehicle.sub_vehicles.filter(insurer__slug=insurer).last()
    if not vehicle:
        if hasattr(m, 'no_custom_vehicles'):
            vehicle = master_vehicle
        else:
            motor_logger.info(
                '======== NO VEHICLE MAPPING FOUND FOR INSURER - {0}, VEHICLE - {1} ==========='.format(
                    insurer,
                    master_vehicle
                )
            )
            return None, False

    refresh_transaction_details(transaction)
    transaction.refresh_from_db()

    quote_response = transaction.raw.get('quote_response', {})
    quote_raw_response = transaction.raw.get('quote_raw_response', {})

    transaction.status = 'DRAFT'
    transaction.save()

    _save_transaction_details(vehicle, proposal_request_data, transaction)

    quote_parameters = insurer_utils.add_dependent_addons(
        quote_parameters, m.get_dependent_addons(quote_parameters))
    request_data_dictionary = m.convert_proposal_request_data_to_required_dictionary(
        vehicle, proposal_request_data, quote_parameters, quote_raw_response, quote_response)
    is_saved = m.ProposalGenerator.save_policy_details(
        request_data_dictionary, transaction)
    if is_saved:
        m.update_transaction_details(transaction, quote_parameters)
    # Transaction attached to discount code used
    customer_discount_code = quote_parameters.get('discountCode', None)
    discount_code = insurer_utils.get_discount_code(customer_discount_code)
    if discount_code:
        discount_code.transaction = transaction
        discount_code.save()

    return transaction, is_saved


def initiate_payment_gateway(transaction_id):
    transaction = Transaction.objects.get(transaction_id=transaction_id)
    vehicle_type = format_vehicle_type(transaction.vehicle_type)
    insurer = transaction.insurer.slug
    m = VEHICLE_INTEGRATION_MAP[vehicle_type][insurer]
    return m.get_payment_data(transaction)


def process_payment_response(request, vehicle_type, insurer, response, transaction_id=None):
    m = VEHICLE_INTEGRATION_MAP[vehicle_type][insurer]

    transaction = None
    if transaction_id is not None:
        # Only for response the transaction id may differ from actual according
        # to insurer
        try:
            transaction = m.get_transaction_from_id(transaction_id)
        except:
            pass

    if transaction:
        insurer_in_transaction = transaction.insurer.slug
        if not (insurer == insurer_in_transaction):
            return None
    else:
        # Goto transaction id fallback from cookies
        transaction_id_cookie = request.COOKIES.get(u'_'.join((vehicle_type, insurer, 'transaction')), None)
        if transaction_id_cookie:
            transaction = Transaction.objects.get(
                transaction_id=transaction_id_cookie)

    return m.post_payment(response, transaction)


def process_expired_policy(transaction_id):

    transaction = Transaction.objects.get(transaction_id=transaction_id)
    vehicle_type = format_vehicle_type(transaction.vehicle_type)
    insurer = transaction.insurer.slug
    m = VEHICLE_INTEGRATION_MAP[vehicle_type][insurer]

    # Can add insurer specific code to schedule an inspection and continue to
    # buy
    if hasattr(m, 'process_expired_policy'):
        return m.process_expired_policy(transaction_id)
    else:
        can_continue_to_buy = False
    return can_continue_to_buy


def get_cities(vehicle_type, insurer, state):
    m = VEHICLE_INTEGRATION_MAP[vehicle_type][insurer]
    if hasattr(m, 'get_city_list'):
        cities = m.get_city_list(state)
        return True, cities
    elif vehicle_type == 'twowheeler':
        state_id = STATE_NAME_TO_STATE_ID.get(state, 0) or state
        cities = STATE_ID_TO_CITY_LIST.get(state_id)
        return True, cities
    return False, None


def get_pincode_details(vehicle_type, insurer, pincode):
    m = VEHICLE_INTEGRATION_MAP[vehicle_type][insurer]

    if hasattr(m, 'get_pincode_details'):
        return True, m.get_pincode_details(pincode)

    return False, None


def get_data_list(vehicle_type, insurer, datatype):
    m = VEHICLE_INTEGRATION_MAP[vehicle_type][insurer]
    data = m.get_custom_data(datatype)
    data_list = []
    if type(data) is dict:
        for key, value in data.items():
            data_list.append({'id': key, 'name': value})
    elif type(data) is list:
        for item in data:
            data_list.append({'id': item, 'name': item})
    return data_list


def process_landt_policy_response(request_data):
    try:
        TransactionData = request_data

        transaction = {
            'TxnId': TransactionData.TxnId,
            'Status': TransactionData.Status,
            'TxnStatusDetail': {
                'ContactId': TransactionData.TxnStatusDetail.ContactId,
                'ReceiptNo': TransactionData.TxnStatusDetail.ReceiptNo,
                'PolicyNo': TransactionData.TxnStatusDetail.PolicyNo,
                'ProposalNo': TransactionData.TxnStatusDetail.ProposalNo,
            },
        }
        if TransactionData.TxnErrorDetails:
            transaction['TxnErrorDetails'] = {
                'ErrorLevel': TransactionData.TxnErrorDetails.ErrorLevel,
                'Details': TransactionData.TxnErrorDetails.Details,
            }
        else:
            transaction['TxnErrorDetails'] = {
                'ErrorLevel': '',
                'Details': '',
            }

        statusDetail = LnTStatusDetail(**transaction['TxnStatusDetail'])
        statusDetail.save()
        errorDetails = LnTErrorDetail(**transaction['TxnErrorDetails'])
        errorDetails.save()
        policyResponse = LnTPolicyResponse()
        policyResponse.TxnId = transaction['TxnId']
        policyResponse.Status = transaction['Status']
        policyResponse.TxnStatusDetail = statusDetail
        policyResponse.TxnErrorDetails = errorDetails
        policyResponse.raw = json.dumps(transaction)
        policyResponse.save()

        landtTransaction = Transaction.objects.get(insurer__slug='l-t', reference_id=transaction['TxnId'])

        if transaction['Status'] == 'Success':
            if LnTPolicyResponse.objects.filter(Status='Success', TxnId=transaction['TxnId']).count() > 1:
                if landtTransaction.policy_number:
                    return True
        else:
            if LnTPolicyResponse.objects.filter(Status=transaction['Status'], TxnId=transaction['TxnId']).count() > 3:
                return True

        if str(TransactionData.TxnStatusDetail.PolicyNo):
            landtTransaction.policy_number = str(TransactionData.TxnStatusDetail.PolicyNo)
            landtTransaction.save()

        mail_utils.send_mail_for_admin({
            'title': 'Policy response for l-t',
            'type': 'policy',
            'data': 'Policy response for transaction %s (CFOX-%s): %s' % (landtTransaction.transaction_id,
                                                                          landtTransaction.id,
                                                                          json.dumps(transaction))
        })

        landt.send_automatic_email(landtTransaction)
        return True
    except:
        return False


@app.task
def generate_rsa_policy(transaction):
    if isinstance(transaction, Transaction):
        transaction.refresh_from_db()
    else:
        transaction = Transaction.objects.get(id=transaction)
    # have added an additional R at the end to avoid clashing with old RSA Policy numbers (if any)
    transaction.rsa_policy_number = '{}{}'.format('CFR2201R', str(transaction.pk))
    try:
        transaction.rsa_policy = generate_pdf(transaction)
    except:
        log_error(logger, msg="RSA Policy could not be generated.")
    else:
        transaction.save(update_fields=['rsa_policy', 'rsa_policy_number'])


@app.task
def generate_policy(transaction, request_dict):
    if not isinstance(transaction, Transaction):
        transaction = Transaction.objects.get(id=transaction)
    policy_path = get_policy_path(transaction, '{}.pdf'.format(transaction.transaction_id))
    try:
        resp = requests.get(transaction.insured_details_json['policy_url'], stream=True)
    except:
        log_error(logger, msg='Unable to reach policy download link for {}'.format(transaction.transaction_id))
    else:
        if resp.ok and resp.status_code == 200:
            if not default_storage.exists(policy_path):
                transaction.update_policy(resp.content, request_dict, send_mail=False)
                activate_user_account.send(sender=transaction._meta.model, transaction=transaction)
        else:
            log_error(logger,
                      msg='Unable to download policy document for {}'.format(transaction.transaction_id))


def get_flat_premium(premium_request, premiums, **kwargs):
    ignore_keys = kwargs.get('ignore_keys', [])
    iterate_over = ['addonCovers', 'specialDiscounts', 'discounts', 'basicCovers']
    addon_extra_list = {
        'addon_isDepreciationWaiver': {'parent': 'addonCovers', 'id': 'isDepreciationWaiver', 'included': False},
        'addon_isAntiTheftFitted': {'parent': 'discounts', 'id': 'isAntiTheftFitted', 'included': False},
        'addon_is247RoadsideAssistance': {'parent': 'addonCovers', 'id': 'is247RoadsideAssistance', 'included': False},
        'addon_isInvoiceCover': {'parent': 'addonCovers', 'id': 'isInvoiceCover', 'included': False},
        'addon_isDriveThroughProtected': {'parent': 'addonCovers', 'id': 'isDriveThroughProtected', 'included': False},
        'addon_isEngineProtector': {'parent': 'addonCovers', 'id': 'isEngineProtector', 'included': False},
        'addon_isNcbProtection': {'parent': 'addonCovers', 'id': 'isNcbProtection', 'included': False},
        'addon_isKeyReplacement': {'parent': 'addonCovers', 'id': 'isKeyReplacement', 'included': False},
        'extra_isLegalLiability': {'parent': 'basicCovers', 'id': 'legalLiabilityDriver', 'included': False},
        'extra_isAntiTheftFitted': {'parent': 'specialDiscounts', 'id': 'isAntiTheftFitted', 'included': False},
        'extra_isMemberOfAutoAssociation': {'parent': 'specialDiscounts', 'id': 'isMemberOfAutoAssociation', 'included': False},
        'addon_isCostOfConsumables': {'parent': 'addonCovers', 'id': 'isCostOfConsumables', 'included': False},
        'addon_isHydrostaticLock': {'parent': 'addonCovers', 'id': 'isHydrostaticLock', 'included': False},
    }
    id_to_addon = {
        'isDepreciationWaiver': 'addon_isDepreciationWaiver',
        'isAntiTheftFitted': 'addon_isAntiTheftFitted',
        'is247RoadsideAssistance': 'addon_is247RoadsideAssistance',
        'isInvoiceCover': 'addon_isInvoiceCover',
        'isDriveThroughProtected': 'addon_isDriveThroughProtected',
        'isEngineProtector': 'addon_isEngineProtector',
        'isNcbProtection': 'addon_isNcbProtection',
        'isKeyReplacement': 'addon_isKeyReplacement',
        'legalLiabilityDriver': 'extra_isLegalLiability',
        'isAntiTheftFitted': 'extra_isAntiTheftFitted',
        'isMemberOfAutoAssociation': 'extra_isMemberOfAutoAssociation',
        'isCostOfConsumables': 'addon_isCostOfConsumables',
        'isHydrostaticLock': 'addon_isHydrostaticLock'
    }
    enabled_addons = {}
    for key_instance, val in addon_extra_list.iteritems():
        if premium_request.get(key_instance, 0) in [1, '1']:
            enabled_addons[key_instance] = val
    for premium in premiums:
        if premium.get('errorCode'):
            continue
        insurer_enabled_addons = copy.deepcopy(enabled_addons)
        premium['final_premium'] = 0
        premium['messages'] = []
        for dep_cover, covers in premium['premiumBreakup'].get('dependentCovers', {}).iteritems():
            if dep_cover in insurer_enabled_addons:
                for cover in covers:
                    temp_addon = id_to_addon.get(cover, False)
                    if temp_addon:
                        insurer_enabled_addons[temp_addon] = addon_extra_list[temp_addon]
        # everything except basic covers
        for parent_key in iterate_over:
            for child_instance in premium['premiumBreakup'][parent_key]:
                if child_instance['id'] in ignore_keys:
                    continue
                elif child_instance['default'] in [1, '1']:
                    premium['final_premium'] += child_instance['premium']
                    try:
                        insurer_enabled_addons[id_to_addon[child_instance['id']]]['included'] = True
                    except:
                        pass
        pa_passenger_added = False
        for child_instance in premium['premiumBreakup']['basicCovers']:
            if child_instance['default'] in [1, '1'] and child_instance['id'] == 'paPassenger' and child_instance['premium'] > 0.0:
                pa_passenger_added = True
                premium['final_premium'] += child_instance['premium']
        if not pa_passenger_added:
            pa_passenger = premium['premiumBreakup']['paPassengerAmounts'].get(int(premium_request.get('extra_paPassenger', 0)), None)
            if pa_passenger is None:
                premium['messages'].append("Unable to add amount passenger amount of Rs. {0} for this insurer".format(premium_request.get('extra_paPassenger', 0)))
            else:
                premium['final_premium'] += pa_passenger
        for addon, addon_data in insurer_enabled_addons.iteritems():
            addon_not_found = True
            for iteration_instance in premium['premiumBreakup'][addon_data['parent']]:
                if iteration_instance['id'] in ignore_keys:
                    continue
                if iteration_instance['id'] == addon_data['id'] and addon_data['included'] is False:
                    premium['final_premium'] += iteration_instance['premium']
                    addon_not_found = False
            if addon_not_found:
                premium['messages'].append("Unable to add {0} for this insurer".format(addon))
        if 'service_tax' not in ignore_keys:
            tax = premium['final_premium'] * premium.get('serviceTaxRate', 0.14)
            premium['final_premium'] += tax
    return premiums


INSURER_EMAIL_DOMAINS = {
    'bharti-axa': ['bharti-axa'],
    'l-t': ['ltinsurance'],
}

INSURER_EMAIL_POLICY_PARSERS = {
    'bajaj-allianz': {
        'subject_re': re.compile(r'^Policy Copy (?P<policy_number>OG-\d{2}-\d{4}-\d{4}-\d{8})$'),
        'policy_doc': lambda details, files: [atchmt for atchmt in files if details['policy_number'] in atchmt.name][0]
    },
    'bharti-axa': {
        'subject_re': re.compile(r'^(?P<full_name>.+) - (?P<policy_number>[\w]{8}) \(Policy Documents\)$'),
        'policy_doc': lambda details, files: [atchmt for atchmt in files if atchmt.name == 'PolicySchedule.pdf'][0]
    },
    'l-t': {
        'subject_re': re.compile(r'^L&T Insurance Policy No\. (?P<policy_number>[\d]{18})$'),
        'policy_doc': lambda details, files: [atchmt for atchmt in files if 'policy schedule' in atchmt.name.lower()][0]
    },
    'hdfc-ergo': {
        'subject_re': re.compile(r'^[\w ]+ \(Policy No: (?P<policy_number>[\d]{19})\)$'),
        'policy_doc': lambda details, files: [atchmt for atchmt in files if details['policy_number'] in atchmt.name][0]
    },
    'future-generali': {
        'subject_re': re.compile(r'(?P<full_name>.+) Your MOTOR SECURE - .+ Policy documents'),
        'text_re': re.compile(r'(?:\s|\n\n)Policy No(?:\s|\n\n)(?P<policy_number>[\w]{8})(?:\s|\n\n)'),
        'policy_doc': lambda details, files: [atchmt for atchmt in files if details['policy_number'] in atchmt.name][0]
    },
    'iffco-tokio': {
        'subject_re': re.compile(r'^IFFCO Tokio Policy No:(?P<policy_number>(?:[\d]{8})|(?:[\w]-[\w]{7}))$'),
        'policy_doc': lambda details, files: [atchmt for atchmt in files if atchmt.name.lower().endswith('.pdf')][0]
    },
    'liberty-videocon': {
        'policy_doc': lambda details, files: [atchmt for atchmt in files if 'Policy_Schedule' in atchmt.name][0]
    },
}

RM_TL = ['shweta@coverfox.com']


def parse_motor_policy_email(from_address, sent_to, subject,
                             content, attachments, mail_time):
    email_domain = exact_email_re.match(from_address).groups()[-1]

    for slug, parsers in INSURER_EMAIL_POLICY_PARSERS.items():
        domains = INSURER_EMAIL_DOMAINS.get(slug, [slug.replace('-', '')])
        if any(domain in email_domain for domain in domains):
            break
    else:
        logger.error(u'Cannot identify the insurer for the email {}'.format(from_address))
        return JsonResponse({'success': True})

    policy_details = {}
    if 'subject_re' in parsers:
        match = parsers['subject_re'].match(subject)
        if match:
            policy_details.update(match.groupdict())
        else:
            email = EmailMessage(
                subject='Unknown policy email received',
                body=u'From {}\nSubject{}'.format(from_address, subject),
                to=RM_TL,
            )
            email.send()
            return JsonResponse({'success': True})

    if 'text_re' in parsers:
        match = parsers['text_re'].search(content['text'])
        if match:
            policy_details.update(match.groupdict())

    if 'html_re' in parsers:
        match = parsers['html_re'].search(content['html'])
        if match:
            policy_details.update(match.groupdict())

    customer_emails = [email for email in set(sent_to) if 'coverfox.com' not in email]
    if not policy_details and not customer_emails:
        email = EmailMessage(
            subject='Cannot parse the policy email',
            body=u'From {}\nSubject{}'.format(from_address, subject),
            to=RM_TL,
        )
        email.send()
        return JsonResponse({'success': True})

    transactions = Transaction.objects.filter(insurer__slug=slug)

    check = True

    while check:
        if 'policy_number' in policy_details:
            _transactions = transactions.filter(policy_number=policy_details['policy_number'])
            if _transactions.exists():
                transactions = _transactions
                _transactions = transactions.filter(policy_document='')
                if _transactions.exists():
                    transactions = _transactions
                    if transactions.count() == 1:
                        transaction = transactions.get()
                        break
                    else:
                        # Filter using email_id
                        pass
                else:
                    # Policy doc already exists
                    logger.info(u'Policy document already exists for the transactions {}'.format(
                        u', '.join(transactions.values_list('transaction_id', flat=True)))
                    )
                    return JsonResponse({'success': True})
            else:
                # Filter using email_id
                pass

        email_queries = [Q(proposer__email__iexact=email.lower()) for email in customer_emails]
        if email_queries:
            email_query = email_queries.pop()
            for query in email_queries:
                email_query |= query

            _transactions = transactions.filter(email_query)
            if _transactions.exists():
                transactions = _transactions
                _transactions = transactions.filter(policy_document='')
                if _transactions.exists():
                    transactions = _transactions
                    if transactions.count() == 1:
                        transaction = transactions.get()
                        break
                    else:
                        # Filter using customer name
                        pass
                else:
                    # Policy doc already exists
                    logger.info(u'Policy document already exists for the transactions {}'.format(
                        u', '.join(transactions.values_list('transaction_id', flat=True)))
                    )
                    return JsonResponse({'success': True})
            else:
                # Filter using customer name
                pass

        full_name = policy_details.get('full_name', '')
        names = full_name.split(' ')
        if len(names) == 2:
            first_name, last_name = names
            _transactions = transactions.filter(proposer__first_name__iexact=first_name.lower(),
                                                proposer__last_name__iexact=last_name.lower())
            if _transactions.exists():
                transactions = _transactions
                _transactions = transactions.filter(policy_document='')
                if _transactions.exists():
                    transactions = _transactions
                    if transactions.count() == 1:
                        transaction = transactions.get()
                        break
                    else:
                        # This will move to name filtering
                        pass
                else:
                    logger.info(u'Policy document already exists for the transactions {}'.format(
                        u', '.join(transactions.values_list('transaction_id', flat=True)))
                    )
                    return JsonResponse({'success': True})
            else:
                # Filter with name
                pass

        transactions = transactions.filter(policy_document='')
        if transactions.exists():
            _transactions = transactions.exclude(status='DRAFT')
            if _transactions.exists():
                transactions = _transactions
                _transactions = transactions.filter(status='COMPLETED')
                if _transactions.exists():
                    transactions = _transactions

                if transactions.count() == 1:
                    transaction = transactions.get()
                    break

        check = False
    else:
        email = EmailMessage(
            subject='Unable to identify the transaction for the email received',
            body=u'From {}\nSubject{}'.format(from_address, subject),
            to=RM_TL,
        )
        email.send()
        return JsonResponse({'success': False}, status=404)

    policy_doc_func = parsers['policy_doc']
    policy_doc = policy_doc_func(policy_details, attachments)

    transaction.update_policy(policy_doc.read(), send_mail=False)
    transaction.policy_number = policy_details.get('policy_number') or transaction.policy_number
    transaction.status = 'COMPLETED'
    transaction.payment_on = transaction.payment_on or mail_time
    transaction.save(update_fields=['status', 'policy_number', 'payment_on'])

    create_user_account.send(sender=Transaction, transaction=transaction, user=AnonymousUser())
    activate_user_account.send(sender=Transaction, transaction=transaction)

    return JsonResponse({'success': True})


@app.task
def lntedi_mail_scheduletask():
    if not settings.PRODUCTION:
        return

    to_mail = [
        'Noor.Shaikh@ltinsurance.com', 'Mayur.Kalsekar@coverfox.com',
        'Priyanka.Tiwari@ltinsurance.com', 'priyanka.pundhir@coverfox.com'
    ]
    cc_mail = [
        'Vishal.Sharma@ltinsurance.com', 'violet.pinto@ltinsurance.com',
        'Akhil@coverfox.com', 'bhuwan.shrivastava@ltinsurance.com'
    ]
    bcc_mail = [
        'akash.totade@coverfoxmail.com', 'amit.siddhu@coverfoxmail.com'
    ]

    excel_header = [
        'LnT Proposal #',
        'CF Proposal #',
        'Transaction Date',
        'Transaction ID',
        'Customer Name',
        'Customer Contact',
        'CFoX ID',
        'Remarks',
        'Payment Type',
        'Case Type',
        'Premium',
    ]

    date_today = date.today()
    now = datetime.now()
    lday = datetime.now() - timedelta(days=1)

    startdate = datetime(lday.year, lday.month, lday.day, 22, 0, 0)
    enddate = datetime(now.year, now.month, now.day, 14, 0, 0)
    file_to_send = 'CFOX_LNT_UW_MIS_1sthalf_%s_%s_%s.xls' % (date_today.day, date_today.month, date_today.year)

    if now.hour >= 22:
        startdate = datetime(now.year, now.month, now.day, 14, 0, 0)
        enddate = datetime(now.year, now.month, now.day, 22, 0, 0)
        file_to_send = 'CFOX_LNT_UW_MIS_2ndhalf_%s_%s_%s.xls' % (date_today.day, date_today.month, date_today.year)

    # ########  Logic to fetch edi data  ##########
    trans_obj_list = Transaction.objects.filter(insurer__slug='l-t', payment_on__range=[startdate, enddate])
    excel_data_list = get_landt_user_data.get_misreport_data(trans_obj_list)

    # ########  Logic to send edi mail if any data exist in edi  #########
    if len(excel_data_list) > 0:
        filepath_to_send = os.path.join('uploads', 'lntedi', file_to_send)
        filepath = render_excel(filepath_to_send, excel_header, excel_data_list, write_to_file=True)

        subject = 'Coverfox cases pending for underwriting approval'
        body = 'Attached is the MIS of cases pending for underwriting approval. Kindly get the same approved.'

        with default_storage.open(filepath, 'rb') as f:
            content = f.read()
        email = EmailMessage(
            subject=subject,
            body=body,
            from_email="rupy@coverfox.com",
            to=to_mail,
            cc=cc_mail,
            bcc=bcc_mail,
            attachments=((os.path.basename(filepath),
                          content,
                          None),)
        )

        # email = EmailMessage(subject, body, 'bot@coverfoxmail.com', to_mail)
        # email.attach_file(filepath)
        email.send()


@app.task
def lnt_consolidated_edimail():
    LANDT_RECEIVERS = [
        'noor.shaikh@ltinsurance.com', 'd2coperations2@ltinsurance.com',
        'vijay.m@ltinsurance.com', 'priyanka.tiwari@ltinsurance.com',
        'bhuwan.shrivastava@ltinsurance.com'
    ]
    COVERFOX_RECEIVERS = [
        'Mayur.Kalsekar@coverfox.com', 'akash.totade@coverfoxmail.com',
        'ritesh@coverfoxmail.com', 'amit.siddhu@coverfoxmail.com'
    ]

    lday = datetime.now() - timedelta(days=1)
    now = datetime.now()
    startdate = datetime(lday.year, lday.month, lday.day, 0, 0, 0)
    enddate = datetime(now.year, now.month, now.day, 0, 0, 0)

    trans_obj_list = Transaction.objects.filter(insurer__slug='l-t', payment_on__range=[startdate, enddate])

    filename, head_keys, list_of_values = get_landt_user_data.detail_list(trans_obj_list)
    filepath = render_excel(filename, head_keys, list_of_values, write_to_file=True)

    with default_storage.open(filepath, 'rb') as f:
            content = f.read()

    subject = 'EDI for the day %s-%s-%s' % (lday.year, lday.month, lday.day)
    body = 'Attached is the EDI for day %s-%s-%s. Kindly get the same approved.' % (lday.year, lday.month, lday.day)
    to_mail = LANDT_RECEIVERS + COVERFOX_RECEIVERS

    email = EmailMessage(
        subject=subject,
        body=body,
        from_email="rupy@coverfox.com",
        to=to_mail,
        attachments=((os.path.basename(filepath),
                      content,
                      None),)
    )

    email.send()


@app.task
def lnt_offline_cases_dayend_mis():
    to_mail = [
        'Noor.Shaikh@ltinsurance.com', 'Priyanka.Tiwari@ltinsurance.com'
    ]
    cc_mail = [
        'Vishal.Sharma@ltinsurance.com', 'violet.pinto@ltinsurance.com'
    ]
    bcc_mail = [
        'akash.totade@coverfoxmail.com', 'ritesh@coverfoxmail.com',
        'amit.siddhu@coverfoxmail.com'
    ]

    lday = datetime.now() - timedelta(days=1)
    now = datetime.now()
    startdate = datetime(lday.year, lday.month, lday.day, 0, 0, 0)
    enddate = datetime(now.year, now.month, now.day, 0, 0, 0)

    trans_obj_list = Transaction.objects.filter(insurer__slug='l-t', payment_on__range=[startdate, enddate])

    filename, head_keys, list_of_values = get_landt_user_data.offline_mis_report(trans_obj_list)
    filepath = render_excel(filename, head_keys, list_of_values, write_to_file=True)

    with default_storage.open(filepath, 'rb') as f:
            content = f.read()

    subject = 'Offline cases - Coverfox'
    body = 'All the attached cases needed inspection report. Kindly get the same from coverfox document dashboard.'

    email = EmailMessage(
        subject=subject,
        body=body,
        from_email="rupy@coverfox.com",
        to=to_mail,
        cc=cc_mail,
        bcc=bcc_mail,
        attachments=((os.path.basename(filepath),
                      content,
                      None),)
    )

    email.send()


def clean_registration_number(registration_number):
    return ''.join(filter(bool, re.findall('[a-z0-9]+', registration_number.lower())))


def get_default_registration_detail_statistics(request):
    import putils
    defaults = {
        'data': {},
        'ip': putils.get_request_ip(request),
        'tracker': request.TRACKER,
        'vehicle_master': None,
        'fastlane_status': 'ERROR_RECORD_NOT_FOUND',
        'number_of_matches': None
    }

    if request.user.is_authenticated():
        defaults['user'] = request.user

    if request.POST.get('name', None):
        registration_number = clean_registration_number(request.POST.get('name'))
        registration_detail, status = NewRegistrationDetail.objects.get_or_create(
            registration_number=registration_number
        )
        defaults['registration_detail'] = registration_detail

    return defaults


def create_registration_detail_statistics(request, context={}):
    params = get_default_registration_detail_statistics(request)
    params.update(context)
    return RegistrationDetailStatistics.objects.create(**params)


def test_get_matching_vehicle_master():
    from motor_product import models as motor_models

    count_skipped1 = 0
    count_skipped2 = 0
    count_failed = 0
    count_noresult = 0
    count_success = 0
    count_clash = 0
    count_key = 0
    count_unspecified = 0
    count_alt = 0

    def stats():
        print "%s \o/ a: %s u: %s, c: %s, f: %s nr: %s s1: %s, s2: %s, sk: %s" % (
            count_success, count_alt, count_unspecified, count_clash,
            count_failed, count_noresult, count_skipped1, count_skipped2,
            count_key,
        )

    for tr in motor_models.Transaction.objects.filter(
            payment_done=True
    ).order_by("-id")[:1000]:
        # stats()

        try:
            rd = motor_models.NewRegistrationDetail.objects.get(
                registration_number__iexact=tr.raw["user_form_details"][
                    "vehicle_reg_no"
                ].replace("-", "")
            )
            if not rd:
                count_noresult += 1
        except motor_models.NewRegistrationDetail.DoesNotExist:
            count_skipped1 += 1
            continue
        except KeyError:
            count_key += 1
            continue

        if "UNSPECIFIED" in (
            rd.fastlane_natural_key(raise_exception=False) or []
        ):
            count_unspecified += 1
            continue

        if "fastlane" not in rd.data:
            count_skipped2 += 1
            continue

        mv, is_exact = rd.get_matching_vehicle_master()

        if not mv:
            count_noresult += 1
            continue

        if mv.id != tr.quote.vehicle.id:
            if "result" not in rd.fastlane["response"]:
                count_noresult += 1
            else:
                if str(mv) == str(tr.quote.vehicle):
                    count_clash += 1
                else:
                    count_failed += 1

            continue

        count_success += 1

    stats()

# 284 \o/ a: 12 u: 92, c: 60, f: 86  nr: 461 s1: 0, s2: 0, sk: 5 26288356cddcbc594756d57619d6cccae1137143
# 382 \o/ a: 3  u: 92, c: 77, f: 352 nr: 89  s1: 0, s2: 0, sk: 5 cf92d383a8f87a1157136b886045a240b76a9f3d


def master_uniques():
    import putils

    d = {}  # make to fastlane list/frequency

    for make in Make.objects.all():
        d[make.name] = []
        for vm in make.vehiclemaster_set.all():
            d[make.name].append(vm.fastlane_key())

    json.dump(d, file("master_uniques.json", "w"))
    putils.d(d)


def fastlane_uniques():
    from motor_product import models as motor_models

    d = {}  # make to fastlane list/frequency

    for make in motor_models.Make.objects.all():
        d[make.name] = {}
        for vm in make.vehiclemaster_set.all():
            print vm
            for tr in motor_models.Transaction.objects.filter(quote__vehicle=vm):
                try:
                    rd = motor_models.NewRegistrationDetail.objects.get(
                        registration_number__iexact=tr.raw["user_form_details"][
                            "vehicle_reg_no"
                        ].replace("-", "")
                    )
                except (motor_models.NewRegistrationDetail.DoesNotExist, KeyError):
                    continue

                if "fastlane" not in rd.data:
                    continue

                fnk = rd.fastlane_natural_key(raise_exception=False)
                if not fnk:
                    continue
                fnk = " ".join(fnk)
                d[make.name][fnk] = d[make.name].get(fnk, 0) + 1

    json.dump(d, file("fastlane_uniques.json", "w"))


def get_form_data(request_data):
    # Get product type.
    product_type = ""

    # Check renewal process.
    # If policy expiry date is greter than current date then it is renewal case.
    # If policy expiry date is less than current date then it is expired.
    # If is_new_vehicle is there than it is new insurance for car.
    # Get past policy date datetime instance.
    product_type = ""
    try:
        past_pol_expiry_datetime_instance = datetime.strptime(
            request_data.get("pastPolicyExpiryDate", ""),
            "%d-%m-%Y"
        )
        if request_data.get("isNewVehicle", "") == "1":
            product_type = "new"
        elif past_pol_expiry_datetime_instance > datetime.now():
            product_type = "renewal"
        elif past_pol_expiry_datetime_instance < datetime.now():
            product_type = "expired"
        else:
            product_type = ""
    except:
        request_data["product_type"] = ""

    request_data["product_type"] = product_type

    vehicle_reg = request_data.get("registrationNumber[]", [])
    if not vehicle_reg:
        vehicle_reg = request_data.get("registrationNumber", [])

    vehicle_rto = ""
    if isinstance(vehicle_reg, list):
        vehicle_rto = '-'.join(vehicle_reg)

    request_data["vehicle_rto"] = vehicle_rto
    return request_data

# fastlane_uniques()
# master_uniques()
# test_get_matching_vehicle_master()


def fix_reg_number_array(reg_number):
    # Temp method to fix alphabet in Registration number
    if reg_number[1][-1].isalpha():
        reg_number[2] = reg_number[1][-1] + reg_number[2]
        reg_number[1] = reg_number[1][:-1]
        return fix_reg_number_array(reg_number)
    return reg_number
