import settings
from django.core.management import setup_environ
setup_environ(settings)

from motor_product import models as motor_models


def convert_makes():
    makes = motor_models.Make.objects.all()
    for make in makes:
        make.name = make.name.title()
        make.save()


def convert_models():
    models = motor_models.Model.objects.all()
    for model in models:
        model.name = model.name.title()
        model.save()

convert_makes()
convert_models()
