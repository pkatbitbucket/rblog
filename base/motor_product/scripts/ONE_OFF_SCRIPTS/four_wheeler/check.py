import settings
import sys
from django.core.management import setup_environ

import re
setup_environ(settings)

from motor_product import models as motor_models


uml = [(v.insurer, v.master_vehicle)
       for v in motor_models.VehicleMapping.objects.all() if v.mapped_vehicle is None]


j = lambda mas_veh: '#'.join(
    [mas_veh.make.name, mas_veh.model.name, mas_veh.variant, mas_veh.fuel_type, str(mas_veh.cc)]
)


def get_raw_master((ins, mas_veh)):
    print "Getting......"
    master = j(mas_veh)
    print '%s in %s' % (master, ins.slug)
    for each in file('motor_product/scripts/ONE_OFF_SCRIPTS/four_wheeler/mapping.csv', 'r').xreadlines():
        cars = each[:-1].split('|')
        if cars[0] == master:
            for c in cars[1:]:
                if c.startswith(ins.slug):
                    x = c.split('!@!')[1]
                    return x
    else:
        sys.exit('raw_not_found')

in_range = lambda s1, s2, rng: (abs(float(s1) - float(s2)) < rng)
mf = lambda s1, s2: not ((re.search('(p(etrol)?)', s1, flags=2)and(re.search('(d(iesel)?)', s2, flags=2)))or(
    re.search('(p(etrol)?)', s2, flags=2)and(re.search('(d(iesel)?)', s1, flags=2))))


def get_raw_vehicle((ins, mas_veh)):
    for each in motor_models.Vehicle.objects.all().filter(insurer__slug=ins.slug):
        if re.search(mas_veh.make.name, each.make, flags=2):
            if re.search(mas_veh.model.name, each.model, flags=2):
                yield each


def tpif(val):
    try:
        return float(val)
    except:
        return val

close = lambda a, b: (sorted([tpif(e) for e in list(set(a.split('|')))]) == b)


def chec(v):
    p = sorted([tpif(e) for e in list(set(get_raw_master(v).split('@!@')))])
    for each in get_raw_vehicle(v):
        if close(each.raw_data, p):
            return each
        else:
            continue
    else:
        print 'Not found', p

counter = 0
for each in uml:
    x = chec(each)
    if x:
        print 'Found', x
        p = motor_models.VehicleMapping.objects.get(insurer=each[0], master_vehicle=each[1])
        p.mapped_vehicle = x
        p.save()
    else:
        counter += 1
print counter
