import settings
from django.core.management import setup_environ
setup_environ(settings)

from motor_product.models import Vehicle


def call_it(Table):
    QSet = Table.objects.filter(insurer__slug='liberty-videocon')
    m = {'cc': 5, 'seating_capacity': 7, 'fuel_type': 23,
         'vehicle_segment': 24, 'variant': 25, 'make_code': 26, }
    for each in QSet:
        raw = each.raw_data.split('|')
        each.cc = raw[m['cc']]
        each.seating_capacity = raw[m['seating_capacity']]
        each.fuel_type = raw[m['fuel_type']]
        each.vehicle_segment = raw[m['vehicle_segment']]
        each.variant = raw[m['variant']]
        each.make_code = raw[m['make_code']]
        each.save()
        print "%s is done." % each.id

call_it(Vehicle)
