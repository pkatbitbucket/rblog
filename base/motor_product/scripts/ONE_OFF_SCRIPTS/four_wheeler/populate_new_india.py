# ---------------------------------------------STANDARD--FUNCTIONS---------------------------------- #

import re
import settings
from django.core.management import setup_environ

setup_environ(settings)

from motor_product import models as motor_models
from collections import OrderedDict as OD


and_ = lambda a, b: a and b
or_ = lambda a, b: a or b
_me = lambda p, q: lambda l=sorted([p, q], key=len): [
    (e in l[1]) for e in l[0]]
_iMax = lambda l: lambda l=l, ma=max(
    l): [i for i in range(len(l)) if (l[i] == ma)]
search = lambda p, s: re.search(p, s, flags=2)
res = lambda l: [e for e in l if e]
ml = lambda p, q: not (False in _me(p, q)())
cp = lambda s1, s2, lst: True in [
    bool(search(v, s1) and search(v, s2)) for v in lst]
substituteeach = lambda st, lst: substituteeach(
    re.sub(lst[0][0], lst[0][1], st, flags=2), lst[1:]) if len(lst) else st
in_range = lambda s1, s2, rng: (abs(float(s1) - float(s2)) < rng)
other = lambda ls, index: ls[:index] + ls[index + 1:]

# -------------------------------------------------ENDS--HERE--------------------------------------- #

# -------------------GLOBAL VARIABLES-------------------------------------- #

_COUNT_ = 0
_LOCATION_REGX_ = '([ ]%s[ ])|(^%s$)|(^%s[ ])|([ ]%s$)'

_FUEL_ = ['PETROL', 'DIESEL', 'CNG', 'LPG', 'GASOLINE', 'BIFUEL/HYBRID']
_FUEL_REGEX_ = ['(p(etrol)?)', '(d(i(esel)?)?)', '(c(ng)?)',
                '(l(pg)?)', '(g(asoline)?)', '((b(i(fuel)?)?)|(h(ybrid)?))']
_FUEL_TYPE_DICT_ = OD(map(None, _FUEL_REGEX_, _FUEL_))

_FUEL_PATTERN_ = _LOCATION_REGX_ % (
    4 * (r'((/?)(' + '|'.join(_FUEL_REGEX_) + '))+',))

_GENERAL_PATTERNS_ = ['[(),*]', _LOCATION_REGX_ % (4 * ('A[/]?C',)), '-', 'automatic([ -]transmission)?', 'manual([ -]transmission)?', '((SOLID)|(SOILD))|(non([ -]?met(al(l)?ic)?)?)', 'metal(l)?ic', '(bharat)([- ]stage)?',
                      _FUEL_PATTERN_, '[0-9][.][0-9]+[ ]?(([lL][ ])|([lL]$))?', '(?!(1000)|(800)|(000))([0-9]{3,4})([ ]?(cc)|(CC))?', 'BS([ -]?)+I', 'BS([ -]?)+II', 'BS([ -]?)+III', 'BS([ -]?)+IV', 'BS([ -]?)+V', '((standard)|(std))', '[.]', '[ ]+']

_GENERAL_REPLACEMENT_ = [' ', ' AC ', ' ', 'AT', 'MT', 'NM', 'M', 'BS',
                         ' ', ' ', ' ', 'BSI', 'BSII', 'BSIII', 'BSIV', 'BSV', 'STD', ' ', ' ']

_GENERAL_PATTERN_DICT_ = OD(
    map(None, _GENERAL_PATTERNS_, _GENERAL_REPLACEMENT_))
# -------------------ENDS HERE--------------------------------------------- #


# ------------------------------------------NORMALIZING--FUNCTIONS---------------------------------- #
caseNstrip = lambda car: car.strip().upper()
norm = lambda st: caseNstrip(substituteeach(
    st, _GENERAL_PATTERN_DICT_.items()))


filtr = lambda lst: [lst[e] for e in [3, 5, 0, 7]]
in_range = lambda s1, s2, rng: (abs(float(s1) - float(s2)) < rng)
j = lambda mv: '#'.join(
    [mv.make.name, mv.model.name, mv.variant, mv.fuel_type, str(mv.cc)])


def matched(car, master):
    mk = norm(car[0])
    md = norm(car[1])
    var = norm(car[2])
    # Since newindia slackers don't have fuel_type, which on the otherhand
    # make them customer friendly
    cc = car[3]
    if re.search(mk, master.make.name, flags=2):
        if re.search(md, master.model.name, flags=2):
            if re.search(var, master.variant, flags=2) or re.search(master.variant, var, flags=2):
                if in_range(cc, master.cc, 25):
                    return True


def find_mapped(mas_veh):
    global para
    print "Getting......"
    for each in file('motor_product/scripts/ONE_OFF_SCRIPTS/four_wheeler/newindia.csv', 'r').xreadlines():
        san_car, raw_car = each[:-1].split('|#|')
        car = filtr(san_car.split('|'))
        if matched(car, mas_veh):
            return san_car
    else:
        print 'car_not_found %s ' % j(mas_veh)


def save_vehicle(map_veh, mas_veh):
    vm = motor_models.VehicleMapping()
    vm.insurer = motor_models.Insurer.objects.get(slug='new-india')
    vm.master_vehicle = mas_veh
    vm.mapped_vehicle = map_veh
    vm.save()
    print 'Saved vehicle %s.....' % j(mas_veh)


def main():
    counter = 0
    for mas_veh in motor_models.VehicleMaster.objects.all():
        veh = find_mapped(mas_veh)
        if veh:
            save_vehicle(motor_models.Vehicle.objects.get(
                raw_data=veh, insurer__slug='new-india'), mas_veh)
        else:
            counter += 1
    print counter, 'did not mapped'

if __name__ == "__main__":
    main()
