
import settings
from django.core.management import setup_environ


setup_environ(settings)

from motor_product import models as motor_models
from motor_product import parser_utils

parameters = {
    'make': 1,
    'model_code': 2,
    'model': 3,
    'number_of_wheels': 4,
    'cc': 6,
    'seating_capacity': 8,
    'fuel_type': 24,
    'vehicle_segment': 25,
    'variant': 5,
    'variant_code': 19,
    'make_code': 27,
}


def main():

    vehicle_dict_list = parser_utils.read_file_to_return_vehicle_dictionary(
        'motor_product/scripts/ONE_OFF_SCRIPTS/four_wheeler/liberty.csv', parameters)
    insurer_liberty = motor_models.Insurer.objects.get(slug='liberty-videocon')

    for vehicle_dict in vehicle_dict_list:
        vehicle_dict['cc'] = parser_utils.try_parse_into_integer(vehicle_dict['cc'])
        vehicle_dict['seating_capacity'] = parser_utils.try_parse_into_integer(
            vehicle_dict['seating_capacity'])
        vehicle_dict['insurer'] = insurer_liberty

        vehicle = motor_models.Vehicle(**vehicle_dict)
        print 'Saving vehicle %s' % vehicle.model
        vehicle.save()


if __name__ == "__main__":
    main()
