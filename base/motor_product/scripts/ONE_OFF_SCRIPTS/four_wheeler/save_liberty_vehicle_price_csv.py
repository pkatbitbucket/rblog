import settings
from django.core.management import setup_environ

setup_environ(settings)

from motor_product import models as motor_models
from motor_product import parser_utils

parameters = {
    'make_code': 2,
    'model_code': 3,
    'ex_showroom_price': 4,
}


def main():

    vehicle_dict_list = parser_utils.read_file_to_return_vehicle_dictionary(
        'motor_product/scripts/ONE_OFF_SCRIPTS/four_wheeler/vehiclePriceLiberty.csv', parameters)

    liberty_vehicles = motor_models.Vehicle.objects.filter(insurer__id=8)

    count = 0

    for vehicle_dict in vehicle_dict_list:
        make_code = vehicle_dict['make_code']
        model_code = vehicle_dict['model_code']
        try:
            vehicle_array = motor_models.Vehicle.objects.filter(make_code=make_code).filter(model_code=model_code)
            vehicle = vehicle_array[0]

            print make_code, model_code, vehicle
            vehicle.ex_showroom_price = vehicle_dict['ex_showroom_price']
            vehicle.save()
        except:
            count = count + 1

    # assign ex showroom price = 0 for those that don't exist in the
    # spreadsheet
    for vehicle in liberty_vehicles:
        if vehicle.ex_showroom_price == '':
            print vehicle.make, vehicle.model, vehicle.variant
            vehicle.ex_showroom_price = '0'
            vehicle.save()

if __name__ == "__main__":
    main()
