import settings
from django.core.management import setup_environ

setup_environ(settings)

from motor_product import models as motor_models
from motor_product import parser_utils

parameters = {
    'make': 0,
    'model': 1,
    'fuel_type': 2,
    'risk_based_discount': 7,
    'zero_dep_discount': 8,
}


def main():

    vehicle_dict_list = parser_utils.read_file_to_return_vehicle_dictionary(
        'motor_product/scripts/ONE_OFF_SCRIPTS/four_wheeler/landt_discount_grid.csv', parameters)
    insurer_lnt = motor_models.Insurer.objects.get(slug='l-t')
    lnt_vehicle_list = motor_models.Vehicle.objects.filter(insurer=insurer_lnt)

    for vehicle_dict in vehicle_dict_list:
        vehicle_list = lnt_vehicle_list.filter(make=vehicle_dict['make']).filter(
            model=vehicle_dict['model']).filter(fuel_type=vehicle_dict['fuel_type'])
        risk_based_discount = float(vehicle_dict['risk_based_discount'])
        zero_dep_discount = float(vehicle_dict['zero_dep_discount'])
        for vehicle in vehicle_list:
            vehicle.risk_based_discount = risk_based_discount
            vehicle.raw_data = '%s|%s' % (vehicle.raw_data, zero_dep_discount)
            print 'Saving vehicle %s with discount %s, %s' % (vehicle.raw_data, risk_based_discount, zero_dep_discount)
            vehicle.save()


if __name__ == "__main__":
    main()
