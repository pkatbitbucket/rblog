
import settings
from django.core.management import setup_environ

setup_environ(settings)

from motor_product import models as motor_models
from motor_product import parser_utils


def main():
    for mapping_dict in parser_utils.yield_mapping_dictionary('motor_product/scripts/ONE_OFF_SCRIPTS/four_wheeler/mapping.csv'):
        master_car = mapping_dict['master_car'].split('#')
        # if
        # master_car[0].startswith('MARUTI')&master_car[1].startswith('ALTO'):
        vehicle_master = motor_models.VehicleMaster()
        vehicle_master.make, fla = motor_models.Make.objects.get_or_create(
            name=master_car[0])
        vehicle_master.model, fla = motor_models.Model.objects.get_or_create(
            make=vehicle_master.make, name=master_car[1])
        vehicle_master.variant = master_car[2]
        vehicle_master.fuel_type = master_car[3]
        vehicle_master.cc = int(float(master_car[4]))
        vehicle_master.save()

        for every_insurer in mapping_dict['mapped_cars'].keys():
            Car_Not_Found = False
            vehicle_mapping = motor_models.VehicleMapping()
            vehicle_mapping.master_vehicle = vehicle_master
            try:
                vehicle_mapping.mapped_vehicle = motor_models.Vehicle.objects.get(
                    raw_data=mapping_dict['mapped_cars'][every_insurer].replace('@!@', '|'))
            except motor_models.Vehicle.DoesNotExist:
                Car_Not_Found = True
                pass
            vehicle_mapping.insurer = motor_models.Insurer.objects.get(slug=every_insurer)
            vehicle_mapping.save()
            if Car_Not_Found:
                print "Not found in insurer %s for make %s model %s variant %s" % (vehicle_mapping.insurer.slug, master_car[0], master_car[1], master_car[2])

if __name__ == "__main__":
    main()
