
import settings
from django.core.management import setup_environ

setup_environ(settings)

from motor_product import models as motor_models
from motor_product import parser_utils

parameters = {
    'make': 1,
    'model_code': 2,
    'model': 3,
    'number_of_wheels': 4,
    'cc': 5,
    'seating_capacity': 7,
}


def read_file_to_get_vehicle_dictionary(file_path, parameters):
    file_data_split_by_line = [line[:-1]
                               for line in file(file_path, 'r').xreadlines()][1:]
    vehicle_dict_list = []
    for line in file_data_split_by_line:
        san, original_data = line.split('|#|')
        vehicle_dict = {}
        vehicle_dict['raw_data'] = san
        vehicle_data = original_data.split('|')
        for k, v in parameters.items():
            vehicle_dict[k] = vehicle_data[v]
        vehicle_dict['make_code'] = san.split('|')[1]
        vehicle_dict_list.append(vehicle_dict)
    return vehicle_dict_list


def main():

    vehicle_dict_list = read_file_to_get_vehicle_dictionary(
        'motor_product/scripts/ONE_OFF_SCRIPTS/four_wheeler/newindia.csv', parameters)
    insurer_sompo = motor_models.Insurer.objects.get(slug='new-india')

    for vehicle_dict in vehicle_dict_list:
        vehicle_dict['cc'] = parser_utils.try_parse_into_integer(vehicle_dict['cc'])
        vehicle_dict['seating_capacity'] = parser_utils.try_parse_into_integer(
            vehicle_dict['seating_capacity'])
        vehicle_dict['insurer'] = insurer_sompo

        vehicle = motor_models.Vehicle(**vehicle_dict)
        print 'Saving vehicle %s' % vehicle.model
        vehicle.save()


if __name__ == "__main__":
    main()
