
import settings
from django.core.management import setup_environ

setup_environ(settings)

from motor_product import models as motor_models
from motor_product import parser_utils


parameters = {
    'make': 2,
    'model_code': 3,
    'model': 4,
    'cc': 7,
    'seating_capacity': 9,
    'fuel_type': 25,
    'vehicle_segment': 26,
    'variant': 27,
    'make_code': 28,
}


def convert_fuel_type_literals_into_fuel_type(fuel_type_literal):
    if fuel_type_literal == 'Petrol':
        fuel_type = 'Petrol'
    elif fuel_type_literal == 'Diesel':
        fuel_type = 'Diesel'
    elif fuel_type_literal == 'External CNG':
        fuel_type = 'External LPG / CNG'
    elif fuel_type_literal in ['CNG', 'LPG']:
        fuel_type = 'Internal LPG / CNG'
    elif fuel_type_literal == 'Electricity':
        fuel_type = 'Electricity'
    else:
        fuel_type = 'Not Defined'

    return fuel_type


def convert_segment_type_literals_into_segment_type(segment_type_literal):
    if segment_type_literal == 'High End':
        segment_type = 'High End Cars'
    elif segment_type_literal == 'Compact':
        segment_type = 'Compact Cars'
    elif segment_type_literal == 'Mid Size':
        segment_type = 'Midsize Cars'
    elif segment_type_literal == 'Mini':
        segment_type = 'Small Sized Vehicles'
    elif segment_type_literal == 'MPV SUV':
        segment_type = 'Sports and Utility Vehicles'
    else:
        segment_type = 'Not Defined'

    return segment_type


def main():

    vehicle_dict_list = parser_utils.read_file_to_return_vehicle_dictionary(
        'motor_product/scripts/ONE_OFF_SCRIPTS/four_wheeler/tata.csv', parameters)
    insurer_tata = motor_models.Insurer.objects.get(title='TATA AIG')

    for vehicle_dict in vehicle_dict_list:
        vehicle_dict['cc'] = parser_utils.try_parse_into_integer(vehicle_dict['cc'])
        vehicle_dict['seating_capacity'] = parser_utils.try_parse_into_integer(
            vehicle_dict['seating_capacity'])
        vehicle_dict['fuel_type'] = convert_fuel_type_literals_into_fuel_type(vehicle_dict[
                                                                              'fuel_type'])
        vehicle_dict['vehicle_segment'] = convert_segment_type_literals_into_segment_type(
            vehicle_dict['vehicle_segment'])
        vehicle_dict['insurer'] = insurer_tata

        # if vehicle_dict['num_level'] == 1:
        #     vehicle_dict['model_code'] = vehicle_dict['num_parent_model_code']

        vehicle = motor_models.Vehicle(**vehicle_dict)
        print 'Saving vehicle %s' % vehicle.model
        vehicle.save()


if __name__ == "__main__":
    main()
