import sys

import settings
from django.core.management import setup_environ

setup_environ(settings)

from motor_product import models as motor_models

sluglist = ['universal-sompo', 'new-india']
insurer = motor_models.Insurer.objects.get(slug=sluglist[int(sys.argv[1])])
empty = lambda: [e for e in motor_models.VehicleMaster.objects.all(
) if not e.sub_vehicles.filter(insurer=insurer)]

for line in file('motor_product/scripts/ONE_OFF_SCRIPTS/four_wheeler/mannual_sompo.csv', 'r').readlines()[1:]:
    master, org = line[:-2].split(',|,')
    master = master.split(',')
    org = org.split(',')
    master = motor_models.VehicleMaster.objects.filter(make__name=master[0], model__name=master[
                                          1], variant=master[2], fuel_type=master[3], cc=int(master[4]))

    if master.count() == 1:
        master = master[0]
    elif master.count() == 0:
        print "No master FOund"
    else:
        print "Multiple Masters found"
        for each in master:
            print each.make.name, each.model.name, each.variant, each.fuel_type, each.cc
        raw_input('Enter To continue')
        print "which of these should be taken as Master for use"
        i = raw_input('Enter index (0 is 1) :')
        master = master[int(i)]

    org = motor_models.Vehicle.objects.filter(insurer=insurer).filter(
            make=org[0],
            model=org[1]
        )

    if org.count() == 1:
        org = org[0]
    elif org.count() == 0:
        print "No original match found"
    else:
        print "Multiple original found"
        for each in list(org):
            print each.make, each.model, each.variant
        # raw_input('Enter To continue')
        # print "which of these should be taken as original"
        # i = raw_input('Enter index (0 is 1) :')
        # org = org[int(i)]
        org = org[0]

    # print "==========================This is Mapping========================="
    # print org.make, org.model, org.variant, org.fuel_type, org.cc
    # print master.make.name, master.model.name, master.variant, master.fuel_type, master.cc
    # print "=================================================================="
    # raw_input
    vm = motor_models.VehicleMapping()
    vm.mapped_vehicle = org
    vm.master_vehicle = master
    vm.insurer = insurer
    print "Saving vehicle %s" % ('#'.join([master.make.name, master.model.name, master.variant, master.fuel_type, str(master.cc)]))
    vm.save()
