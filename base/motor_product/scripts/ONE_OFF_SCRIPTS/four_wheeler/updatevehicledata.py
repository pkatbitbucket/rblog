import sys
import settings
from django.core.management import setup_environ, execute_from_command_line

setup_environ(settings)

from django.db import transaction
from motor_product import models as motor_models

__doc__ = '''Usage: updatevehicledata [options] [fixture_address]
Options:
    -h  :   Help
    -v  :   Verbose
    -m  :   mapping related tables (VehicleMapping, VehicleMaster, Make, Model)
    -a  :   all tables
Example:
    updatevehicledata -vm motor_product/fixtures/vehiclewithmapping.json
'''


def delete_data(vf, mf, af):
    if af:
        if vf:
            print 'Deleting Vehicle......'
        motor_models.Vehicle.objects.all().delete()
    if vf:
        print 'Deleting VehicleMapping.....'
    motor_models.VehicleMapping.objects.all().delete()
    if vf:
        print 'Deleting VehiclMaster.....'
    motor_models.VehicleMaster.objects.all().delete()
    if vf:
        print 'Deleting Make and Model'
    motor_models.Make.objects.all().delete()
    motor_models.Model.objects.all().delete()


def load_data(vf, fa):
    if vf:
        print 'Loading Fixtures from %s ....' % (fa)
    execute_from_command_line(['python' 'manage.py', 'loaddata', fa])


@transaction.commit_on_success
def main():
    global __doc__
    (verbose_flag, mapping_flag, alltable_flag) = (False, False, False)
    arguments = sys.argv[1:]
    if arguments:
        if arguments[0].startswith('-'):
            if 'h' in arguments[0]:
                print __doc__
                if len(arguments[0]) == 2:
                    sys.exit()
            if 'v' in arguments[0]:
                verbose_flag = True
            if 'm' in arguments[0]:
                mapping_flag = True
            if 'a' in arguments[0]:
                alltable_flag = True
            try:
                fixture_address = arguments[1]
            except IndexError:
                fixture_address = raw_input("Pleas give the fixture address: ")
        else:
            fixture_address = arguments[0]
        sys.stdout.write('Go make some coffee dude!\n')
        delete_data(verbose_flag, mapping_flag, alltable_flag)
        load_data(verbose_flag, fixture_address)
    else:
        print __doc__

if __name__ == '__main__':
    main()
