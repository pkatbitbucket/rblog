import sys
import pprint

import settings
from django.core.management import setup_environ
setup_environ(settings)

from motor_produc import models as motor_models


def print_map(insurer_slug, logonly):
    insurer = motor_models.Insurer.objects.get(slug=insurer_slug)

    mapping_dictionary = {}

    for rto in motor_models.RTOMaster.objects.all():
        approved_map = rto.rtomapping_set.filter(
            is_approved_mapping=True).filter(insurer=insurer)

        if logonly:
            if approved_map.count() > 1:
                print 'More than one approved mapping for RTO %s - %s.' % (rto.rto_code, rto.id)
            if approved_map.count() < 1:
                print 'No RTO mapped for RTO %s - %s.' % (rto.rto_code, rto.id)

        if approved_map.count() == 1:
            mapping_dictionary[rto.rto_code] = approved_map[
                0].rto_insurer.rto_code

    if not logonly:
        pprint.pprint(mapping_dictionary)

if __name__ == "__main__":
    logonly = False

    if len(sys.argv) > 2 and sys.argv[2] == 'log':
        logonly = True

    print_map(sys.argv[1], logonly)
