import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
django.setup()

from motor_product.models import VehicleMaster, Make, Model, Quote, Transaction
from motor_product import parser_utils
from django.db import connection

MASTER_PATH = 'motor_product/scripts/fourwheeler/data/master_fourwheeler.csv'
EXISTING_MASTER_PATH = 'motor_product/scripts/ONE_OFF_SCRIPTS/existing_master.csv'

parameters = {
    'make': 0,
    'model': 1,
    'variant': 2,
    'fuel_type': 3,
    'cc': 4,
    'seating_capacity': 5,
    'master': 18,
}

eparameters = {
    'id': 0,
    'make': 1,
    'model': 3,
    'master': 9,
}


def clean_duplicate_makes():
    cursor = connection.cursor()
    makes = {}
    for make in Make.objects.all():
        if makes.get(make.name, None):
            # Duplicate make found
            print 'Duplicate make: %s' % make.name
            make_id = makes.get(make.name)
            cursor.execute("UPDATE motor_product_model SET make_id = %s WHERE make_id = %s" % (
                make_id, make.id))
            cursor.execute("UPDATE motor_product_vehiclemaster SET make_id = %s WHERE make_id = %s" % (
                make_id, make.id))
            make.delete()
        else:
            makes[make.name] = make.id


def check_duplicate_makes():
    makes = {}
    for make in Make.objects.all():
        if makes.get(make.name, None):
            makes[make.name] += 1
        else:
            makes[make.name] = 1

    for make, count in makes.items():
        if count > 1:
            print make
            raise Exception('Duplicate makes found.')


def clean_duplicate_models():
    cursor = connection.cursor()
    models = {}
    for model in Model.objects.all():
        mkey = model.make.name + ' ' + model.name
        if models.get(mkey, None):
            # Duplicate model found
            print 'Duplicate model: %s' % mkey
            model_id = models.get(mkey)
            cursor.execute("UPDATE motor_product_vehiclemaster SET model_id = %s WHERE model_id = %s" % (
                model_id, model.id))
            model.delete()
        else:
            models[mkey] = model.id


def check_duplicate_models():
    models = {}
    for model in Model.objects.filter(vehicle_type='Private Car'):
        mkey = model.make.name + ' ' + model.name
        if models.get(mkey, None):
            models[mkey] += 1
        else:
            models[mkey] = 1

    for model, count in models.items():
        if count > 1:
            print model
            raise Exception('Duplicate models found.')


def clean_duplicate_vehicles():
    cursor = connection.cursor()
    vehicles = {}
    for vehicle in VehicleMaster.objects.filter(vehicle_type='Private Car'):
        vkey = str(vehicle.make_id) + '-' + str(vehicle.model_id) + '-' + vehicle.variant + \
            '-' + str(vehicle.cc) + '-' + \
            str(vehicle.seating_capacity) + '-' + vehicle.fuel_type
        if vehicles.get(vkey, None):
            # Duplicate vehicle found
            vehicle_id = vehicles.get(vkey)
            cursor.execute("UPDATE motor_product_quote SET vehicle_id = %s WHERE vehicle_id = %s" % (
                vehicle_id, vehicle.id))
            print 'Duplicate vehicle: %s' % vkey
            print 'Removing duplicate: %s' % vehicle.id
            vehicle.delete()
        else:
            vehicles[vkey] = vehicle.id


def check_duplicate_vehicles():
    vehicles = {}
    for vehicle in VehicleMaster.objects.filter(vehicle_type='Private Car'):
        vkey = str(vehicle.make_id) + '-' + str(vehicle.model_id) + '-' + vehicle.variant + \
            '-' + str(vehicle.cc) + '-' + \
            str(vehicle.seating_capacity) + '-' + vehicle.fuel_type
        if vehicles.get(vkey, None):
            vehicles[vkey] += 1
        else:
            vehicles[vkey] = 1

    for vehicle, count in vehicles.items():
        if count > 1:
            print vehicle
            raise Exception('Duplicate vehicles found.')

vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
    MASTER_PATH, parameters)
evehicles = parser_utils.read_file_to_return_vehicle_dictionary(
    EXISTING_MASTER_PATH, eparameters)

print 'Total existing master vehicles: %s' % VehicleMaster.objects.filter(vehicle_type='Private Car').count()
print 'Total tracked: %s' % len(evehicles)

_vehicle_map = {}
for _vehicle in vehicles:
    _vehicle_map[int(_vehicle['master'])] = _vehicle

_evehicle_map = {}
_evehicle_makes = {}
_evehicle_models = {}
for _vehicle in evehicles:
    try:
        _evehicle_map[int(_vehicle['id'])] = int(_vehicle['master'])
        mvehicle = _vehicle_map[int(_vehicle['master'])]
        _evehicle_makes[mvehicle['make']] = True
        _evehicle_models[mvehicle['model']] = mvehicle['make']
    except:
        _evehicle_map[int(_vehicle['id'])] = None


def replace_makes():
    clean_duplicate_makes()

    for make in _evehicle_makes.keys():
        makeobj, created = Make.objects.get_or_create(name=make)
        if created:
            print 'Created make %s' % make


def replace_models():
    clean_duplicate_models()

    for model, make in _evehicle_models.items():
        make = Make.objects.get(name=make)
        modelobj, created = Model.objects.get_or_create(
            make=make, name=model, vehicle_type='Private Car')
        if created:
            print 'Created model %s %s' % (make, model)


def replace_vehicles(dry_run=True):
    mvehicles = VehicleMaster.objects.filter(vehicle_type='Private Car')

    for vehicle in mvehicles:
        vehicle_id = vehicle.id
        vmaster = _evehicle_map.get(vehicle_id, None)
        if vmaster:
            _vehicle = _vehicle_map[vmaster]

            make = Make.objects.get(name=_vehicle['make'])
            vehicle.make = make

            model = Model.objects.get(make=make, name=_vehicle['model'])
            vehicle.model = model

            vehicle.variant = _vehicle['variant']
            vehicle.cc = int(_vehicle['cc'])
            vehicle.fuel_type = _vehicle['fuel_type']
            vehicle.seating_capacity = int(_vehicle['seating_capacity'])
            if not dry_run:
                vehicle.save()
        else:
            print str(vehicle.id) + ',' + vehicle.make.name + ',' + vehicle.model.name +\
                ',' + vehicle.variant + ',' + str(vehicle.cc) + ',' + str(vehicle.fuel_type) +\
                ',' + str(vehicle.seating_capacity)


def check_for_empty_make_models(dry_run=True):
    for make in Make.objects.all():
        if (make.vehiclemaster_set.count() == 0):
            print 'No vehicles for %s' % make.name
        if (make.model_set.count() == 0):
            print 'No models for %s' % make.name

    for model in Model.objects.filter(vehicle_type='Private Car'):
        if (model.vehiclemaster_set.count() == 0):
            print 'No vehicles for model %s' % model.name
            if not dry_run:
                model.delete()


def entry_status():
    print 'Total Makes: %s' % Make.objects.all().count()
    print 'Total Car Models: %s' % Model.objects.filter(vehicle_type='Private Car').count()
    print 'Total Vehicles: %s' % VehicleMaster.objects.filter(vehicle_type='Private Car').count()
    print 'Total Quotes With No Vehicles: %s' % Quote.objects.filter(vehicle=None).count()
    print 'Total Transactions: %s' % Transaction.objects.all().count()


# clean_duplicate_makes()
# check_duplicate_makes()
# clean_duplicate_models()
# check_duplicate_models()
# replace_makes()
# replace_models()
# replace_vehicles()
# clean_duplicate_vehicles()
# check_duplicate_vehicles()
# check_for_empty_make_models()
entry_status()
