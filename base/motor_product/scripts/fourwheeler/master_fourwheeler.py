from motor_product import models as motor_models
from motor_product import parser_utils

MASTER_PATH = 'motor_product/scripts/fourwheeler/data/master_fourwheeler.csv'

parameters = {
    'make': 0,
    'model': 1,
    'variant': 2,
    'fuel_type': 3,
    'cc': 4,
    'seating_capacity': 5,
}


def delete_data():
    motor_models.VehicleMaster.objects.filter(vehicle_type='Private Car').delete()
    motor_models.Model.objects.filter(vehicle_type='Private Car').delete()


def save_data():
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        MASTER_PATH, parameters)

    vehicle_objects = []
    total_makes_created = 0
    total_models_created = 0
    total_vehicles_created = 0
    total_vehicles_already_present = 0

    for _vehicle in vehicles:
        make, created = motor_models.Make.objects.get_or_create(
            name=_vehicle['make'])
        if created:
            total_makes_created += 1
            print 'New make created - ' + make.name

        model, created = motor_models.Model.objects.get_or_create(
            make=make, name=_vehicle['model'], vehicle_type='Private Car')
        if created:
            total_models_created += 1
            print 'New model created - ' + model.name + ' for make ' + make.name

        _vehicle['make'] = make
        _vehicle['model'] = model
        _vehicle['cc'] = parser_utils.try_parse_into_integer(_vehicle['cc'])
        _vehicle['seating_capacity'] = int(
            _vehicle['seating_capacity'].strip())
        _vehicle['vehicle_type'] = 'Private Car'

        del _vehicle['raw_data']

        try:
            vehicle = motor_models.VehicleMaster.objects.get(**_vehicle)
            print 'Already present vehicle %s' % vehicle.model.name
            total_vehicles_already_present += 1
        except:
            vehicle = motor_models.VehicleMaster(**_vehicle)
            vehicle_objects.append(vehicle)
            print 'Saving vehicle %s' % vehicle.model.name
            total_vehicles_created += 1

    print 'Total makes created: %s' % total_makes_created
    print 'Total models created: %s' % total_models_created
    print 'Total vehicles created: %s' % total_vehicles_created
    print 'Total vehicles already present: %s' % total_vehicles_already_present
    motor_models.VehicleMaster.objects.bulk_create(vehicle_objects)


def create_mappings():
    pass


def delete_mappings():
    pass
