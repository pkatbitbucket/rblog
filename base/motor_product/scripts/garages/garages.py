import settings
import requests
import json

GARAGE_MASTER_PATH = 'motor_product/scripts/garages/data/garage_list.csv'


def save_data(_file=GARAGE_MASTER_PATH, delimeter='|'):
    fd = open(_file)
    count = 0
    for line in fd.readlines()[1:]:
        fields = line.split(delimeter)
        garage_data = {
            'name': fields[0].strip(),
            'make': fields[1].strip(),
            'city': fields[2].strip(),
            'address': fields[3].strip(),
            'state': fields[4].strip(),
            'pincode': fields[5].strip(),
            'company': fields[6].strip(),
        }
        print garage_data
        requests.put(settings.ELASTIC_SEARCH_URL.rstrip(
            "/") + "/motor/garages/" + str(count), data=json.dumps(garage_data))
        count += 1
    fd.close()


def delete_data():
    requests.delete(
        settings.ELASTIC_SEARCH_URL.rstrip("/") + "/motor/")
