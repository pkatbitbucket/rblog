import csv
from motor_product.models import TempGarage
from wiki.models import City


def populate_temp_garagelist():
    filepath = 'motor_product/scripts/garages/data/garage_list.csv'
    reader = csv.DictReader(open(filepath))

    rownumber = 1
    for row in reader:
        print "Row.....................................{}".format(rownumber)
        rownumber += 1
        print row

        data = {
            'name': row['Garage'].strip(),
            'city': row['City'].strip(),
            'address': row['Garage_Address'].strip(),
            'pin': row['Pin_Code'].strip() if len(row['Pin_Code'].strip()) == 6 else '',
            'company': row['Company'].strip(),
            'make': row['Make'].strip() if len(row['Make'].strip()) < 51 else '',
        }

        # If Garage and Pincode already exists - since both fields are `unique together`
        g = TempGarage.objects.filter(name=data['name'], pin=data['pin'])
        if g:
            continue

        # get city
        c = City.objects.filter(name=data['city'])
        if c:
            data['wikicity'] = c[0]
        else:
            pass
        
        # add
        TempGarage.objects.create(**data)