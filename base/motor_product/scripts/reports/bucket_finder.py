import os
from dateutil import relativedelta

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django

django.setup()
from django.utils import timezone
from motor_product.prod_utils import premium_fetch, get_flat_premium
from motor_product.models import Insurer
import csv

START = 0
END = 1800


def get_bucket(insurer):
    data = {u'addon_isDepreciationWaiver': u'0', u'isNCBCertificate': u'0', u'extra_paPassenger': u'0',
            u'cngKitValue': u'0', u'reg_number_bifurcated[]': [u'MH', u'02', u'0', u'0'], u'extra_user_dob': u'',
            u'reg_number': u'MH-02-BX-0378', u'isNewVehicle': u'0', u'extra_isAntiTheftFitted': u'0',
            u'quoteId': u'd71036b0-950a-4934-be11-858d4bd354c2', u'idvNonElectrical': u'0', u'isClaimedLastYear': u'0',
            u'registrationNumber[]': [u'MH', u'01', u'0', u'0'], u'newNCB': u'0', u'manufacturingDate': u'31-03-2014',
            u'fastlane_model_changed': u'false', u'rds_id': u'false', u'addon_isNcbProtection': u'0',
            'insurer_slug': u'l-t', u'registrationDate': u'31-03-2014', u'idvElectrical': u'0', u'third_party': None,
            u'expiry_error': u'false', u'addon_is247RoadsideAssistance': u'0', u'isExpired': u'false',
            u'voluntaryDeductible': u'0', u'fastlane_variant_changed': u'false',
            u'formatted_reg_number': u'MH-02-BX-0378', u'fastlane_fueltype_changed': u'false',
            u'addon_isInvoiceCover': u'0', u'addon_isDriveThroughProtected': u'0',
            u'extra_isMemberOfAutoAssociation': u'0', u'payment_mode': u'PAYMENT_GATEWAY',
            u'extra_isTPPDDiscount': u'0', u'isCNGFitted': u'0', u'isUsedVehicle': u'0', u'fastlane_success': u'false',
            u'vehicleId': u'1265', u'expirySlot': u'7', u'idv': u'0', u'discountCode': u'',
            u'extra_isLegalLiability': u'0', u'previousNCB': u'0', u'addon_isEngineProtector': u'0',
            u'ncb_unknown': u'true', u'addon_isKeyReplacement': u'0', u'previous_policy_insurer_name': u'false',
            u'newPolicyStartDate': u'11-03-2016', u'pastPolicyExpiryDate': u'10-03-2016'}
    bucket_list = []
    old_min_idv = old_max_idv = old_premium = 0
    head = START
    current_date = timezone.now()
    for i in range(START, END):
        days = current_date.day - 1
        iter_day = timezone.now() - relativedelta.relativedelta(days=i)
        data['registrationDate'] = iter_day.strftime('%d-%m-%Y')
        data['manufacturingDate'] = iter_day.strftime('%d-%m-%Y')
        first_of_month = (current_date - relativedelta.relativedelta(days=days)).strftime('%d-%m-%Y')
        if first_of_month != data['manufacturingDate']:
            current_date = timezone.now() - relativedelta.relativedelta(days=i)
            continue
        current_date = timezone.now() - relativedelta.relativedelta(days=i)
        print '****%s*****' % data['manufacturingDate']
        try:
            response = premium_fetch('fourwheeler', data, insurer, False)
        except Exception as e:
            print e
            response = None
        if response is None:
            continue
        max_idv = response['maxIDV']
        min_idv = response['minIDV']
        premium = int(round(get_flat_premium({}, [response])[0]['final_premium']))
        if i == START:
            old_premium = premium
            old_min_idv = min_idv
            old_max_idv = max_idv
            continue
        if old_max_idv != max_idv or old_min_idv != min_idv or premium not in range(old_premium-1, old_premium+1):
            bucket = '%s-%s' % (head, i+1)
            head = i+1
            bucket_list.append({'max_idv': old_max_idv, 'min_idv': old_min_idv, 'premium': old_premium, 'bucket': bucket, 'date':data['registrationDate']})
            old_max_idv = max_idv
            old_min_idv = min_idv
            old_premium = premium
    bucket = '%s-%s' % (head, i+1)
    bucket_list.append({'max_idv': old_max_idv, 'min_idv': old_min_idv, 'premium': old_premium, 'bucket': bucket, 'date': data['registrationDate']})
    try:
        with open('bucket_finder_for%s.csv' % insurer, 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['Bucket', 'MAX IDV', 'MIN IDV', 'PREMIUM', 'Date'])
            for b in bucket_list:
                writer.writerow([b['bucket'], b['max_idv'], b['min_idv'], b['premium'], b['date']])
    except:
        pass

if __name__ == '__main__':
    for i in Insurer.objects.filter(is_active_for_car=True, slug='l-t').values_list('slug', flat=True):
        get_bucket(i)
