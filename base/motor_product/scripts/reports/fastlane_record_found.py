import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()
from motor_product.models import Transaction, RegistrationDetail
import csv



record_found = []

for t in Transaction.objects.filter(status__in=['COMPLETED', 'MANUAL COMPLETED']).iterator():
    v_num = t.raw['user_form_details']['vehicle_reg_no'].replace('-', '').lower()
    rd = RegistrationDetail.objects.filter(name=v_num).first()
    if rd:
        t_engine_num = t.raw['user_form_details']['vehicle_engine_no']
        t_chasis_num = t.raw['user_form_details']['vehicle_chassis_no']
        rd_engine_num = rd.fastlane['response']['result']['vehicle']['eng_no']
        rd_chassis_num = rd.fastlane['response']['result']['vehicle']['chasi_no']
        if rd.fastlane['response']['description'] == 'Record found':
            if t_engine_num == rd_engine_num and t_chasis_num == rd_chassis_num:
                record_found.append([t.raw['user_form_details']['cust_first_name'], v_num, t.insurer, t.raw['user_form_details']['cust_phone'], t.raw['user_form_details']['cust_email']])

if record_found:
    with open('fastlane_report.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['name', 'vehicle no.', 'insurer', 'phone', 'email'])
        writer.writerows(record_found)
        print 'csv file generated'
