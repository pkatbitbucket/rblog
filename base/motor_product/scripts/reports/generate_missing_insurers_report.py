import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()
import csv
from motor_product.models import VehicleMaster, Insurer


def generate_csv():
    insurer_dict = dict(Insurer.objects.all().values_list('id', 'title'))
    vm_objs = VehicleMaster.objects.all().select_related('model', 'make').prefetch_related('sub_vehicles__insurer',
                                                                                           'sub_vehicles__vehicle')
    with open('missing_insurer_report.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        headers = ['ID', 'Make', 'Model', 'Variant', 'Fuel Type', 'Vehicle Type']
        insurers_list = insurer_dict.values()
        headers.extend(insurers_list)
        writer.writerow(headers)
        for each_obj in vm_objs.iterator():
            row = [each_obj.id, each_obj.make.name, each_obj.model.name, each_obj.variant,
                   each_obj.fuel_type, each_obj.vehicle_type]
            insurer_vehicle_details = each_obj.sub_vehicles.all().values('make', 'model', 'insurer__title', 'variant')
            insurer_vehicle_details_dict = dict()
            for insurer in insurer_vehicle_details:
                details = insurer['make'] + ', ' + insurer['model'].encode('ascii', 'ignore') + ', ' +\
                          insurer['variant'].encode('ascii', 'ignore')
                insurer_vehicle_details_dict[insurer['insurer__title']] = details
            for i in insurers_list:
                details = insurer_vehicle_details_dict.get(i, '')
                row.append(details)
            writer.writerow([unicode(r).encode("utf-8") for r in row])


if __name__ == '__main__':
    generate_csv()







