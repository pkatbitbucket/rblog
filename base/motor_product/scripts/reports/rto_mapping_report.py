import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()
import csv
from motor_product.models import RTOMaster, RTOInsurer
from importlib import import_module


def generate_csv():
    insurer_dict = dict(RTOInsurer.objects.all().values_list('insurer__id', 'insurer__title'))
    rto_objs = RTOMaster.objects.all().prefetch_related('insurer_rtos__insurer')
    unmapped_insurers_dict = {'Bharti AXA General Insurance': 'bharti_axa',
                              'Future Generali General Insurance': 'future_generali',
                              'Liberty Videocon General Insurance': 'liberty_videocon',
                              'TATA AIG General Insurance': 'tata_aig'}

    with open('rto_mapping_report.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        headers = ['ID', 'RTO_CODE', 'RTO_NAME', 'RTO_STATE']
        insurers_list = insurer_dict.values()
        insurers_list.extend(unmapped_insurers_dict.keys())
        headers.extend(insurers_list)
        writer.writerow(headers)
        for each_obj in rto_objs.iterator():
            row = [each_obj.id, each_obj.rto_code, each_obj.rto_name, each_obj.rto_state]
            insurer_details = each_obj.insurer_rtos.all().values('rto_code', 'rto_name', 'rto_state', 'insurer__title')
            insurer_vehicle_details_dict = dict()
            for insurer in insurer_details:
                details = insurer['rto_code'] + ', ' + insurer['rto_name'] + ', ' + insurer['rto_state']
                insurer_vehicle_details_dict[insurer['insurer__title']] = details
            for title, insurer in unmapped_insurers_dict.iteritems():
                details = ''
                module = import_module('motor_product.insurers.%s.custom_dictionaries' % insurer)
                try:
                    custom_rto_dict = module.RTO_CODE_TO_RTO_INFO
                except AttributeError:
                    custom_rto_dict = module.RTO_CITY_CODE_TO_INFO
                info = custom_rto_dict.get(each_obj.rto_code.replace('-', ''), custom_rto_dict.get(each_obj.rto_code,
                                                                                                   None))
                if info:
                    state = info.get('State', '')
                    if insurer == 'tata_aig':
                        state_module = module.STATE_LIST
                        state = state_module.get(info['State'], '')
                    details += each_obj.rto_code.replace('-', '') + ', ' + \
                               info.get('RTO_City', info.get('RTOCity', info.get('City'))) + ', ' + state
                insurer_vehicle_details_dict[title] = details

            for i in insurers_list:
                details = insurer_vehicle_details_dict.get(i, '')
                row.append(details)

            writer.writerow([unicode(r).encode("utf-8") for r in row])


if __name__ == '__main__':
    generate_csv()
