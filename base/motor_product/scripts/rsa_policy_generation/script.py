# setup environment
import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
django.setup()

import csv
import datetime

from django.db.models import Q

import motor_product.models as motor_product_models
import motor_product.prod_utils as motor_prod_utils


# Transactions of which RSA needs to be generated
TRANSACTION_FILE_PATH = "motor_product/scripts/rsa_policy_generation/data.csv"
TWOWHEELER_FILE_PATH = "motor_product/scripts/rsa_policy_generation/twowheeler_rsa.csv"


def generate_rsa_for_twowheeler():
    """
    Generate RSA Policy document and RSA Policy number for the corresponding transaction_ids
    in the data.csv file.
    We'll do this by setting empty value for both fields and saving which will
    regenerate the RSA PDF document and RSA policy number (this will get automatically
    trigger "post save" signal which will generate RSA PDF document and RSA Policy number)
    """

    reader = csv.DictReader(open(TWOWHEELER_FILE_PATH))

    counter = 1
    for row in reader:
        transaction_id = row.get('transaction_id', None)
        if transaction_id:
            try:
                transaction = motor_product_models.Transaction.objects.get(transaction_id=transaction_id.strip())
            except motor_product_models.Transaction.DoesNotExist:
                print "Transaction not found {}".format(transaction_id)
            else:
                transaction.rsa_policy_number = '{}{}'.format('CFR2201R', str(transaction.pk))
                motor_prod_utils.generate_rsa_policy.apply_async(queue='pdf_generator', args=[transaction])

                print "{}) RSA generated successfully ({})".format(counter, transaction_id)
                counter += 1


def regenerate_rsa_policy():
    """
    This will regenerate RSA Policy document and RSA Policy number transactions which already have RSA Policy document
    (Basically updating current RSA Policy document and RSA Policy number)
    We'll do this by setting empty value for both fields and saving, which will
    regenerate the RSA PDF document and RSA policy number (this will get automatically
    trigger "post save" signal which will generate RSA PDF document and RSA Policy number)
    """

    counter = 1
    transactions = motor_product_models.Transaction.objects.filter(rsa_policy__isnull=False).exclude(rsa_policy__exact='').order_by('created_on')
    total_count = len(transactions)
    for transaction in transactions:
        transaction.rsa_policy = ''
        transaction.rsa_policy_number = ''
        transaction.save()

        print "{}) RSA generated successfully ({}) of total {}".format(counter, transaction.transaction_id, total_count)
        counter += 1


def generate_rsa_policy():
    """
    Generate RSA policy from a specified time for Four wheeler vehicles
    """

    counter = 1
    from_date = datetime.datetime(2015, 10, 01)
    query = (
        Q(vehicle_type='Private Car') &
        (Q(rsa_policy__isnull=True) | Q(rsa_policy__exact='')) &
        (Q(status='COMPLETED') | Q(status='MANUAL COMPLETED')) &
        Q(created_on__gte=from_date)
    )

    transactions = motor_product_models.Transaction.objects.filter(query).order_by('created_on')
    total_count = len(transactions)
    for transaction in transactions:
        transaction.rsa_policy = ''
        transaction.rsa_policy_number = ''
        transaction.save()

        print "{}) RSA generated successfully ({}) of total {}".format(counter, transaction.transaction_id, total_count)
        counter += 1


# def remove_rsa_policy_two_wheeler():
#     """
#     This will set empty RSA Policy document and RSA Policy number fields for all
#     Two wheeler vehicles
#     """

#     counter = 1
#     transactions = motor_product_models.Transaction.objects.filter(vehicle_type='Twowheeler', rsa_policy__isnull=False)
#     for transaction in transactions:
#         transaction.rsa_policy = ''
#         transaction.rsa_policy_number = ''
#         transaction.save()

#         print "{}) RSA removed successfully ({})".format(counter, transaction.transaction_id)
#         counter += 1


if __name__ == '__main__':
    # print "============================= REGENERATING RSA ============================="
    # regenerate_rsa_policy()
    # print "============================= GENERATING NEW RSA ============================="
    # generate_rsa_policy()
    print "============================= GENERATING RSA FOR TWOWHEELER ============================="
    generate_rsa_for_twowheeler()
