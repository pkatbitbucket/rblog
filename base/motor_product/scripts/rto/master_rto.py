from motor_product import models as motor_models
from motor_product.scripts.rto import master_rto_data as master_dicts


def delete_data():
    motor_models.RTOMaster.objects.all().delete()


def save_data():

    RTO_INFO_DICT = master_dicts.RTO_CODE_TO_RTO_INFO

    for rto in motor_models.RTOMaster.objects.filter(rto_state='DL'):
        number = rto.rto_code.split('-')[1]
        if number in ['1L', '1C', '2C', '3C', '4C', '5C', '6C', '7C', '8C', '9C', '10C', '11C', '12C', '13C', '14C', '15C', '16C', '17C', '18C']:
            rto.delete()
        else:
            rto.rto_code = 'DL-' + str(int(number))
            rto.save()

    for rto in motor_models.RTOMaster.objects.filter(rto_state='GJ'):
        number = rto.rto_code.split('-')[1]
        rto.rto_code = 'GJ-' + str(int(number))
        rto.save()

    rtos = []
    for rto_code, rto_info in RTO_INFO_DICT.items():
        try:
            rto_master = motor_models.RTOMaster.objects.get(rto_code=rto_code)
        except:
            rto_master = motor_models.RTOMaster()
            rto_master.rto_code = rto_code
            print 'New RTO added - ' + rto_code
        rto_master.rto_name = rto_info['RTOCity']
        state_code = rto_code.split('-')[0]
        rto_master.rto_state = state_code
        rto_master.raw = rto_info
        print "Saving ---- %s, %s" % (rto_code, rto_info['RTOCity'])
        rtos.append(rto_master)

    motor_models.RTOMaster.objects.bulk_create(rtos)
