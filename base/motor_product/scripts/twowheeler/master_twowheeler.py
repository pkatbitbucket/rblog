from motor_product import models as motor_models
from motor_product import parser_utils

MASTER_PATH = 'motor_product/scripts/twowheeler/data/master_twowheeler.csv'

parameters = {
    'make': 0,
    'model': 1,
    'variant': 2,
    'cc': 3,
}


def delete_data():
    motor_models.VehicleMaster.objects.filter(vehicle_type='Twowheeler').delete()
    motor_models.Model.objects.filter(vehicle_type='Twowheeler').delete()


def save_data():
    vehicles = parser_utils.read_file_to_return_vehicle_dictionary(
        MASTER_PATH, parameters)

    vehicle_objects = []
    for _vehicle in vehicles:
        make, created = motor_models.Make.objects.get_or_create(
            name=_vehicle['make'])
        if created:
            print 'New make created - ' + make.name

        model, created = motor_models.Model.objects.get_or_create(
            make=make, name=_vehicle['model'], vehicle_type='Twowheeler')
        if created:
            print 'New model created - ' + model.name + ' for make ' + make.name

        _vehicle['make'] = make
        _vehicle['model'] = model
        _vehicle['cc'] = parser_utils.try_parse_into_integer(_vehicle['cc'])
        _vehicle['fuel_type'] = 'PETROL'
        _vehicle['vehicle_type'] = 'Twowheeler'

        del _vehicle['raw_data']
        vehicle = motor_models.VehicleMaster(**_vehicle)
        vehicle_objects.append(vehicle)
        print 'Saving vehicle %s' % vehicle.model.name

    motor_models.VehicleMaster.objects.bulk_create(vehicle_objects)


def create_mappings():
    pass


def delete_mappings():
    pass
