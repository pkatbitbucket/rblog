import logging
import datetime
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from motor_product.cdaccount.models import CDAccount, CDTransaction
from utils import log_error

from . import mail_utils as motor_mail_utils
from .exceptions import ReadOnlyError
from .models import Transaction
from core import activity as core_activity
import core.models as core_models
from decimal import Decimal

logger = logging.getLogger(__name__)


@receiver(pre_save, sender=Transaction)
def transaction_saved(sender, instance, update_fields, **kwargs):
    try:
        transaction = Transaction.objects.get(pk=instance.pk)
    except Transaction.DoesNotExist:
        pass
    else:
        if transaction.status not in ['COMPLETED', 'MANUAL COMPLETED']:
            if not transaction.status == instance.status:
                if instance.status == 'FAILED':
                    motor_mail_utils.send_transaction_fail_alert(instance)
                elif instance.status == 'COMPLETED':
                    motor_mail_utils.send_transaction_success_alert(instance, created=True)
                elif instance.status == 'PROPOSAL FAILED':
                    motor_mail_utils.send_proposal_fail_alert(instance)
        else:
            allowed_updates = [
                'policy', 'policy_document', 'policy_number', 'rsa_policy', 'updated_on',
                'rsa_policy_number', 'proposal_number', 'premium_paid', 'proposal_request',
                'proposal_response', 'policy_request', 'policy_response', 'insured_details_json',
                'payment_on'
            ]

            reverse_relations = [f.rel for f in Transaction._meta._relation_tree]
            fields = set(Transaction._meta.get_fields()).difference(reverse_relations)
            if update_fields:
                fields = [f for f in fields if f.attname in update_fields or f.name in update_fields]
            for field in fields:
                if field.attname not in allowed_updates and field.name not in allowed_updates:
                    if getattr(transaction, field.attname) != getattr(instance, field.attname):
                        # Above condition gives false positive in cases
                        # where field returns different value or different type of value than the one it is assigned to.
                        # eg: if a charfield is given a value other than a str or unicode
                        # it returns a str after successful save.
                        #
                        # We fall back to comparing the query values.
                        # This is kept as a fallback because of stupid JSONField.
                        # json.dumps() does not give a unique result everytime for complex dicts.
                        saved_query_value = field.get_prep_value(getattr(transaction, field.attname))
                        saving_query_value = field.get_prep_value(getattr(instance, field.attname))
                        if saved_query_value != saving_query_value:
                            # This is a patch and should be removed in future
                            # once status history is set for all completed transactions
                            if field.attname == 'status_history' and not saved_query_value:
                                continue
                            else:
                                raise ReadOnlyError(
                                    'Can not update {} field once transaction is marked complete.\n'.format(field.attname)
                                )

            motor_mail_utils.send_transaction_success_alert(instance, created=False)


@receiver(post_save, sender=Transaction)
def transaction_post_saved(sender, instance, **kwargs):
    from .prod_utils import generate_policy, generate_rsa_policy
    if instance.status in ['COMPLETED', 'MANUAL COMPLETED']:
        # Generate RSA policy for Private Car (Four wheelers) only
        if instance.vehicle_type == 'Private Car' and not instance.rsa_policy and not instance.rsa_policy_number:
            generate_rsa_policy.apply_async(queue='pdf_generator', args=[instance.id])
        if not instance.policy_document and instance.insured_details_json.get('policy_url'):
            if hasattr(instance, "request"):
                request_dict = instance.request
            else:
                request_dict = core_activity.request_to_dict()

            generate_policy.delay(instance.id, request_dict)

        if instance.quote.raw_data.get('payment_mode', '') == 'CD_ACCOUNT':
            try:
                cd_transaction = instance.cdtransaction_set.filter(transaction_type=CDTransaction.DEBIT).last()
                if not cd_transaction:
                    third_party = instance.quote.raw_data.get('third_party', 'coverfox')
                    company = core_models.Company.objects.filter(slug=third_party).last()
                    cd_account = CDAccount.objects.get(insurer=instance.insurer)
                    cd_account.balance -= Decimal(str(instance.premium_paid))
                    cd_transaction = CDTransaction(
                        transaction=instance,
                        cd_account=cd_account,
                        amount=instance.raw['quote_response'].get('final_premium', instance.premium_paid),
                        business_type=company if company else None,
                        transaction_type=CDTransaction.DEBIT,
                        payment_mode=instance.raw.get('cd_payment_details', {}).get('payment_method', 'CHEQUE'),
                        balance_left=cd_account.balance,
                        transaction_date=datetime.date.today()
                    )
                    if cd_account.balance < 100000:
                        motor_mail_utils.send_low_cd_account_balance(cd_account)
                    cd_transaction.balance_left = cd_account.balance
                    cd_account.save()
                    cd_transaction.save()
            except Exception, e:
                log_error(logger, msg="Couldn't insert CD_ACCOUNT transaction")
