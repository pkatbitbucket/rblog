from collections import OrderedDict
from datetime import datetime, timedelta

from motor_product.prod_utils import get_premium_cron
from cache_dictionaries import *
from celery_app import app

quote_parameters = {
    'isNewVehicle': '0',
    'vehicleId': '1026',
    'registrationNumber[]': ['MH', '01', 'HH', '8888'],
    'voluntaryDeductible': '0',
    'manufacturingDate': '01-01-2012',
    'registrationDate': '01-01-2012',
    'isClaimedLastYear': '0',
    'previousNCB': '20',
    'quoteId': '',
    'exShowroomPrice': '0',
    'idv': '0',
    'minIdv': '0',
    'maxIdv': '0',
    'idvElectrical': '0',
    'idvNonElectrical': '0',
    'cngKitValue': '0',
    'isCNGFitted': '0',
    'addon_is247RoadsideAssistance': '1',
    'addon_isDepreciationWaiver': '1',
    'addon_isNcbProtection': '1',
    'addon_isInvoiceCover': '1',
    'addon_isKeyReplacement': '0',
    'addon_isAntiTheftFitted': '0',
    'addon_isDriveThroughProtected': '0',
    'addon_isEngineProtector': '1',
    'pastPolicyExpiryDate': '08-01-2015'
}

insurers = ['l-t', 'bajaj-allianz', 'bharti-axa', 'hdfc-ergo',
            'iffco-tokio', 'universal-sompo', 'new-india']


@app.task(ignore_result=True)
def start_crawling():
    pyp_expiry_date = (datetime.datetime.now() +
                       timedelta(days=4)).strftime('%d-%m-%Y')
    quote_parameters['pastPolicyExpiryDate'] = pyp_expiry_date

    vehicles = VEHICLE_LIST
    rtos = RTO_LIST

    registrationDates = []
    year_months = OrderedDict([
        ('2014', range(1, 7)),
        ('2013', range(1, 13)),
        ('2012', range(1, 13)),
        ('2011', range(1, 13)),
        ('2010', range(1, 13)),
    ])

    for year, months in year_months.items():
        for month in months:
            registrationDates.append('01-%02d-%s' % (month, year))

    voluntaryDeductibles = ['0', '2500']

    previousNCBs = {
        '2014': ['0'],
        '2013': ['20'],
        '2012': ['20', '25'],
        '2011': ['20', '25', '35'],
        '2010': ['20', '25', '35', '45'],
    }

    # ### FOR OLD VEHICLE ###
    quote_parameters['isNewVehicle'] = '0'
    for vehicle in vehicles:
        quote_parameters['vehicleId'] = str(vehicle)
        for rto in rtos:
            quote_parameters['registrationNumber[]'] = [
                rto.split('-')[0], rto.split('-')[1], 'HH', '8888']
            for registrationDate in registrationDates:
                quote_parameters['registrationDate'] = registrationDate
                quote_parameters['manufacturingDate'] = registrationDate
                for voluntaryDeductible in voluntaryDeductibles:
                    quote_parameters[
                        'voluntaryDeductible'] = voluntaryDeductible
                    for insurer in insurers:
                        quote_parameters['isClaimedLastYear'] = '1'
                        get_premium_cron.apply_async(
                            [quote_parameters, insurer], queue='motor_premium_cron')
                        # ### FOR CLAIMED LAST YEAR ###
                        quote_parameters['isClaimedLastYear'] = '0'
                        year = registrationDate.split('-')[2]
                        for ncb in previousNCBs[year]:
                            quote_parameters['previousNCB'] = ncb
                            get_premium_cron.apply_async(
                                [quote_parameters, insurer], queue='motor_premium_cron')
