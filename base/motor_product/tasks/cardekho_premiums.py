from motor_product import models as motor_models
from motor_product.prod_utils import get_premium

from celery_app import app
import copy

PREMIUM_TIMEOUT = 27

CITIES = {
    'MH-01': 'Mumbai',
    'DL-1': 'Delhi',
    'WB-01': 'Kolkata',
    'TN-01': 'Chennai',
    'KA-01': 'Bangalore',
    'AP-09': 'Hyderabad',
    'GJ-1': 'Ahmedabad',
    'HR-26': 'Gurgaon',
    'MH-12': 'Pune',
    'CH-01': 'Chandigarh',
    'RJ-14': 'Jaipur',
    'GJ-5': 'Surat',
    'UP-32': 'Lucknow',
    'UP-77': 'Kanpur',
    'MH-31': 'Nagpur',
    'MP-09': 'Indore',
}


@app.task(ignore_result=True)
def get_pricing(quote_parameters):
    premiums = []

    INSURERS = motor_models.Insurer.objects.filter(is_active_for_car=True)

    for ins in INSURERS.values_list('slug', flat=True):
        result_output = get_premium('fourwheeler', quote_parameters, ins)

        if result_output is not None:
            idv = result_output['calculatedAtIDV']
            basic_covers = result_output['premiumBreakup']['basicCovers']
            discounts = result_output['premiumBreakup']['discounts']

            total_premium = 0
            for cover in basic_covers:
                if int(cover['default']) == 1:
                    total_premium += round(float(cover['premium']), 2)

            for discount in discounts:
                total_premium += round(float(discount['premium']), 2)

            total_premium = total_premium * 1.14

            premiums.append({'idv': idv, 'premium': round(
                total_premium, 2), 'insurer': result_output['insurerSlug']})

    for premium in premiums:
        vehicle_id = quote_parameters['vehicleId']
        v = motor_models.VehicleMaster.objects.get(id=vehicle_id)

        bprice = motor_models.BestPricing()
        bprice.vehicle = v
        bprice.city = CITIES[
            '-'.join(quote_parameters['registrationNumber[]'][0:2])]
        bprice.year = quote_parameters['registrationDate'].split('-')[2]
        bprice.insurer_slug = premium['insurer']
        bprice.idv = float(premium['idv'])
        bprice.premium = float(premium['premium'])
        bprice.save()


@app.task(ignore_result=True)
def fetch_premiums_for_vehicle(vehicle_id):

    quote_parameters = {
        'isNewVehicle': '1',
        'vehicleId': '1026',
        'registrationNumber[]': ['MH', '01', 'HH', '8888'],
        'previousNCB': '20',
        'pastPolicyExpiryDate': '08-07-2015',
        'newPolicyStartDate': '08-08-2015',
        'manufacturingDate': '01-06-2015',
        'registrationDate': '01-06-2015',
        'voluntaryDeductible': '0',
        'isClaimedLastYear': '0',
        'quoteId': '',
        'idv': '0',
        'idvElectrical': '0',
        'idvNonElectrical': '0',
        'cngKitValue': '0',
        'isCNGFitted': '0',
        'addon_is247RoadsideAssistance': '0',
        'addon_isDepreciationWaiver': '0',
        'addon_isNcbProtection': '0',
        'addon_isInvoiceCover': '0',
        'addon_isEngineProtector': '0',
        'addon_isKeyReplacement': '0',
        'extra_isAntiTheftFitted': '0',
        'extra_isMemberOfAutoAssociation': '0',
        'extra_paPassenger': '0',
        'extra_legalLiability': '0',
    }

    quote_parameters['vehicleId'] = str(vehicle_id)

    for rto in CITIES.keys():
        quote_parameters['registrationNumber[]'] = [
            rto.split('-')[0], rto.split('-')[1], 'HH', '8888']
        fqparameters = copy.deepcopy(quote_parameters)
        get_pricing.apply_async([fqparameters], queue='motor_premium_cron')


@app.task(ignore_result=True)
def start_crawling_premiums():
    for vehicle in motor_models.VehicleMaster.objects.filter(vehicle_type='Private Car'):
        fetch_premiums_for_vehicle.apply_async(
            [vehicle.id], queue='motor_premium_cron')
