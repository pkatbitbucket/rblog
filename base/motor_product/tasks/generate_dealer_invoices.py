from motor_product.models import Dealership, RequirementDealerMap, DealerInvoice
from celery_app import app
from django.db.models import Sum
from payment.models import PayoutBase
from django.utils import timezone


def generate_dealership_invoice(dealership):
    all_dealers = dealership.dealership_dealers.all()
    # All policies that have been sold but not been invoiced
    policies_sold = RequirementDealerMap.objects.filter(
        dealer__in=all_dealers, commission_value__gt=0.0,
        requirement__requirement_transactions__status='COMPLETED').exclude(
        id__in=DealerInvoice.objects.all().values_list(
            'requirements', flat=True).distinct())
    if policies_sold:
        total_amount = policies_sold.aggregate(Sum('commission_value')).get('commission_value__sum', 0)
        invoice_instance = DealerInvoice()
        invoice_instance.raw = {
            "reference_number": None,
            "total_amount": total_amount,
            "cf_cheque_no": None,
            "dealer_cheque_no": None,
            "paid_amount": 0,
        }
        invoice_instance.dealership = dealership
        invoice_instance.save()
        for requirement in policies_sold:
            invoice_instance.requirements.add(requirement)
    return dealership


@app.task
def generate_invoice_task():
    timedelta_differences = {
        PayoutBase.WEEKLY: 7,
        PayoutBase.BI_WEEKLY: 14,
        PayoutBase.MONTHLY: 30,
    }
    for dealership in Dealership.objects.all():
        create_invoice = False
        last_invoice = DealerInvoice.objects.filter(dealership=dealership).last()
        if last_invoice:
            difference = timezone.now() - last_invoice.created_date
            if difference.days >= timedelta_differences[dealership.payout_frequency]:
                create_invoice = True
        else:
            create_invoice = True
        if create_invoice:
            generate_dealership_invoice(dealership)
