import copy
from datetime import datetime, timedelta
from celery_app import app

from motor_product.prod_utils import get_premium_cron
from motor_product.tasks import insurer_cache_data
from motor_product.tasks.insurers import future_generali_cache as future_generali
from motor_product.tasks.insurers import tata_aig_cache as tata_aig

CACHE_MODULE = {
    'future-generali': future_generali,
    'tata-aig': tata_aig,
}

all_quote_parameters = {
    'isUsedVehicle': '0',
    'isNewVehicle': '0',
    'vehicleId': '1026',
    'registrationNumber[]': ['MH', '01', 'HH', '8888'],
    'voluntaryDeductible': '0',
    'manufacturingDate': '01-01-2012',
    'registration_date': '01-01-2012',
    'isClaimedLastYear': '0',
    'previousNCB': '20',
    'quoteId': '',
    'idv': '0',
    'minIdv': '0',
    'maxIdv': '0',
    'idvElectrical': '0',
    'idvNonElectrical': '0',
    'cngKitValue': '0',
    'isCNGFitted': '0',
    'addon_is247RoadsideAssistance': '0',
    'addon_isDepreciationWaiver': '0',
    'addon_isNcbProtection': '0',
    'addon_isInvoiceCover': '0',
    'addon_isKeyReplacement': '0',
    'addon_isAntiTheftFitted': '0',
    'addon_isDriveThroughProtected': '0',
    'addon_isEngineProtector': '0',
    'pastPolicyExpiryDate': '08-01-2015'
}

vehicles = insurer_cache_data.VEHICLE_LIST
rtos = insurer_cache_data.RTO_LIST
voluntary_deductibles = ['0']


@app.task(ignore_result=True)
def start_crawling(insurer):
    m = CACHE_MODULE[insurer]
    quote_parameters = copy.deepcopy(all_quote_parameters)
    pyp_expiry_date = datetime.now() + timedelta(days=4)
    quote_parameters['pastPolicyExpiryDate'] = pyp_expiry_date.strftime('%d-%m-%Y')

    if hasattr(m, 'get_registration_dates'):
        registration_dates = m.get_registration_dates(pyp_expiry_date)
    else:
        registration_dates = []
        for index in range(1, 10):
            registration_date = pyp_expiry_date.replace(year=pyp_expiry_date.year - index)
            registration_dates.append(registration_date.strftime('%d-%m-%Y'))

    previous_ncbs = {
        0: ['0'],
        1: ['20'],
        2: ['20', '25'],
        3: ['20', '25', '35'],
        4: ['20', '25', '35', '45'],
        5: ['20', '25', '35', '45', '50'],
        6: ['20', '25', '35', '45', '50'],
        7: ['20', '25', '35', '45', '50'],
        8: ['20', '25', '35', '45', '50'],
    }

    count = 0
    # ### FOR OLD VEHICLE ###
    quote_parameters['isNewVehicle'] = '0'
    for vehicle in vehicles:
        quote_parameters['vehicleId'] = str(vehicle)
        for rto in rtos:
            quote_parameters['registrationNumber[]'] = [
                rto.split('-')[0], rto.split('-')[1], 'HH', '8888']
            for registration_date in registration_dates:
                quote_parameters['registrationDate'] = registration_date
                quote_parameters['manufacturingDate'] = registration_date
                for vd in voluntary_deductibles:
                    quote_parameters['voluntaryDeductible'] = vd
                    quote_parameters['isClaimedLastYear'] = '1'
                    count += 1
                    get_premium_cron.apply_async(
                        ['fourwheeler', copy.deepcopy(quote_parameters), insurer], queue='motor_premium_cron')
                    # ### FOR CLAIMED LAST YEAR ###
                    quote_parameters['isClaimedLastYear'] = '0'
                    year = int(registration_date.split('-')[2])
                    today_year = int(datetime.now().year)
                    year_index = (today_year - year - 1)
                    for ncb in previous_ncbs[year_index]:
                        quote_parameters['previousNCB'] = ncb
                        count += 1
                        get_premium_cron.apply_async(
                            ['fourwheeler', copy.deepcopy(quote_parameters), insurer], queue='motor_premium_cron')

    print 'Total tasks launched: %s' % count
