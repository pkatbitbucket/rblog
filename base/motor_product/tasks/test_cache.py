import copy
from datetime import datetime, timedelta

from motor_product.prod_utils import get_premium
from motor_product.cache_utils import QueryCache
from celery_app import app

from django.core.cache import caches
cache = caches['motor-results']

from django.core.mail import EmailMessage

quote_parameters = {
    'isUsedVehicle': '0',
    'isNewVehicle': '0',
    'vehicleId': '1026',
    'registrationNumber[]': ['MH', '01', 'HH', '8888'],
    'voluntaryDeductible': '0',
    'manufacturingDate': '01-01-2012',
    'registrationDate': '01-01-2012',
    'isClaimedLastYear': '0',
    'previousNCB': '20',
    'quoteId': '',
    'idv': '0',
    'minIdv': '0',
    'maxIdv': '0',
    'idvElectrical': '0',
    'idvNonElectrical': '0',
    'cngKitValue': '0',
    'isCNGFitted': '0',
    'addon_is247RoadsideAssistance': '0',
    'addon_isDepreciationWaiver': '0',
    'addon_isNcbProtection': '0',
    'addon_isInvoiceCover': '0',
    'addon_isKeyReplacement': '0',
    'addon_isAntiTheftFitted': '0',
    'addon_isDriveThroughProtected': '0',
    'addon_isEngineProtector': '0',
    'pastPolicyExpiryDate': '08-01-2015'
}


def clean_result(result):
    if result is None:
        return ['-', '-']

    idv = result['calculatedAtIDV']
    basic_covers = result['premiumBreakup']['basicCovers']
    discounts = result['premiumBreakup']['discounts']

    total_premium = 0
    for cover in basic_covers:
        if int(cover['default']) == 1:
            total_premium += round(float(cover['premium']), 2)

    for discount in discounts:
        total_premium += round(float(discount['premium']), 2)

    total_premium = total_premium * 1.14

    return [idv, round(total_premium, 2)]


@app.task(ignore_result=True, queue='motor_premium_cron')
def start_test():
    pyp_expiry_date = datetime.now() + timedelta(days=4)
    quote_parameters['pastPolicyExpiryDate'] = pyp_expiry_date.strftime('%d-%m-%Y')

    from motor_product.tasks.test_cache_data import test_cases

    output = ""

    for tcase in test_cases:
        quote_parameters['isNewVehicle'] = str(tcase[1])
        quote_parameters['vehicleId'] = str(tcase[2])
        rto = tcase[3]
        quote_parameters['registrationNumber[]'] = [rto.split('-')[0], rto.split('-')[1], 'HH', '8888']
        quote_parameters['registrationDate'] = tcase[4]
        quote_parameters['manufacturingDate'] = tcase[4]
        quote_parameters['isClaimedLastYear'] = str(tcase[5])
        quote_parameters['previousNCB'] = str(tcase[6])
        quote_parameters['voluntaryDeductible'] = str(tcase[7])

        result_output = get_premium('fourwheeler', copy.deepcopy(quote_parameters), tcase[0], False)
        uncached_result = clean_result(result_output)

        store_key = QueryCache.get_cache_key(copy.deepcopy(quote_parameters), 'fourwheeler', tcase[0])
        resp = cache.get(store_key)
        cached_result = clean_result(resp['data'])

        lstr = ""
        for entry in tcase:
            lstr += str(entry) + "|"

        for entry in cached_result:
            lstr += str(entry) + "|"

        for entry in uncached_result:
            lstr += str(entry) + "|"

        output += lstr + "\n"

    email = EmailMessage('Premium Cache Test', output, 'bot@coverfoxmail.com', ['nishant@coverfoxmail.com'])
    email.send()
