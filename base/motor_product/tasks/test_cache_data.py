# new_vehicle, vehicleId, rto, registration_date, claimed_last_year, ncb, voluntary_deductible
test_cases = [
    ('tata-aig', 0, 1257, 'MH-01', '01-01-2014', 0, 0, 0),
    ('tata-aig', 0, 1257, 'MH-01', '28-12-2013', 0, 0, 0),
    ('tata-aig', 0, 1257, 'MH-01', '28-12-2012', 0, 0, 0),
    ('tata-aig', 0, 1257, 'MH-01', '28-12-2011', 0, 0, 0),
    ('tata-aig', 0, 1257, 'MH-01', '28-12-2010', 0, 0, 0),
    ('future-generali', 0, 1257, 'MH-01', '01-01-2014', 0, 0, 0),
    ('future-generali', 0, 1257, 'MH-01', '28-12-2013', 0, 0, 0),
    ('future-generali', 0, 1257, 'MH-01', '28-12-2012', 0, 0, 0),
    ('future-generali', 0, 1257, 'MH-01', '28-12-2011', 0, 0, 0),
    ('future-generali', 0, 1257, 'MH-01', '28-12-2010', 0, 0, 0),
]
