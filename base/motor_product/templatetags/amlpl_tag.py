from django import template

register = template.Library()


@register.inclusion_tag('motor_product/amlpl_template.html', takes_context=True)
def amlpl(context):
    request = context['request']
    hide_amlpl = request.user_agent.is_mobile and (
        request.COOKIES.get('aml_mobile') or request.TRACKER.extra.get('amlpl'))
    return {
        'show_amlpl': not hide_amlpl,
        'tracker_id': request.TRACKER.id
    }
