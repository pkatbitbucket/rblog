"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.db import DataError
from django.test import TestCase

from motor_product import models as motor_models


# models test
class NewRegistrationDetailTest(TestCase):

    def test_overflowing_registration_number_length(self):
        with self.assertRaises(DataError,):
            motor_models.NewRegistrationDetail.objects.create(
                registration_number='a' * 100
            )

    def test_successful_creation(self):
        registration_detail = motor_models.NewRegistrationDetail(
                registration_number='a' * 32
            )
        self.assertEqual(registration_detail.save(), None)
        registration_detail.delete()
