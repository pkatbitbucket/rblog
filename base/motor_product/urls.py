from django.conf.urls import include, url

from . import views
from .motor_discount_admin import views as discount_admin_views
from .motor_discount_code_admin import views as discount_code_admin_views
from .motor_garages import views as garage_views
from .motor_offline_leads import views as offline_lead_views

api_patterns = [
    url(r'^vehicles/$', views.ModelListView.as_view(), name='make_model_combined'),
    url(r'^vehicle-make/$', views.AvailableVehicleMakeView.as_view(), name='vehicle_make'),
    url(r'^vehicle-model/(?P<make_id>\d+)/$', views.AvailableVehicleModelView.as_view(), name='vehicle_models_by_make'),
    url(r'^vehicles/(?P<model_id>\d+)/variants/$', views.vehicles, name='vehicle_variants_with_make_and_model'),
    url(r'^quotes/email/$', views.generate_email_for_quote, name='generate_email_for_quote'),
    url(r'^quotes/$', views.quotes, name='quotes'),
    url(r'^quotes/(?P<quote_id>[\w-]+)/$', views.quote_info, name='quote_info'),
    url(r'^quotes-async/$', views.quotes_async, name='quotes_async'),
    url(r'^quotes-async/(?P<quote_id>[\w-]+)/$', views.quotes_async, name='quotes_async_with_id'),
    url(r'^quotes/(?P<quote_id>[\w-]+)/refresh/(?P<insurer_slug>[\w-]+)/$', views.refresh_quote, name='refresh_quote'),
    url(r'^confirm/(?P<quote_id>[\w-]+)/(?P<insurer_slug>[\w-]+)/$', views.generate_form_simply, name='generate_form'),
    url(r'^transaction/(?P<transaction_id>[\w-]+)/refresh/', views.refresh_transaction, name='refresh_transaction'),
    url(r'^transaction/(?P<transaction_id>[\w-]+)/refresh-quote/', views.refresh_transaction_quote, name='refresh_transaction_quote'),
    url(r'^pincode-details/(?P<insurer_slug>[\w-]+)/$', views.pincode_details, name='pincode_details'),
    url(r'^cities/(?P<insurer_slug>[\w-]+)/$', views.get_cities, name='cities'),
    url(r'^rtos/$', views.get_master_rtos, name='rtos'),
    url(r'^discount-code/$', views.generate_discount_code, name='discount_code'),
    url(r'^vehicle-info/$', views.RegistrationDetailCreateView.as_view(), name='api_vehicle_info'),
    url(r'^vehicle-confirmation/$', views.vehicle_confirmation, name='api_vehicle_confirmation'),
    url(r'^(?P<insurer_slug>[\w-]+)/desktopForm/(?P<transaction_id>[\w-]+)/$', views.desktopFormApi, name='desktop_form_api'),
]

admin_patterns = [
    url(r'^(?P<insurer_slug>[\w-]+)/$', discount_admin_views.discountingAdmin, name='discountingAdmin'),
    url(r'^(?P<insurer_slug>[\w-]+)/vehicles/$', discount_admin_views.get_vehicles, name='get_vehicles'),
    url(r'^(?P<insurer_slug>[\w-]+)/rtos/$', discount_admin_views.get_rtos, name='get_rtos'),
    url(r'^(?P<insurer_slug>[\w-]+)/discountings/rtos/$',
        discount_admin_views.get_rtos_for_discount_loading, name='get_rtos_for_discount_loading'),
    url(r'^(?P<insurer_slug>[\w-]+)/discountings/rtos/modify$',
        discount_admin_views.modify_rtos_for_discount_loading, name='modify_rtos_for_discount_loading'),
    url(r'^(?P<insurer_slug>[\w-]+)/discountings/$', discount_admin_views.get_discountings, name='get_discountings'),
    url(r'^(?P<insurer_slug>[\w-]+)/add$', discount_admin_views.add_discounting, name='add_discounting'),
    url(r'^(?P<insurer_slug>[\w-]+)/modify$', discount_admin_views.modify_discounting, name='modify_discounting'),
    url(r'^(?P<insurer_slug>[\w-]+)/delete$', discount_admin_views.delete_discounting, name='delete_discounting'),
]

garage_patterns = [
    url(r'^$', garage_views.garage_list, name='garage_list'),
    url(r'^(?P<list_id>[\w-]+)/$', garage_views.garage_list, name='garage_list'),
    url(r'^(?P<list_id>[\w-]+)/modify/(?P<garage_id>\d+)/$', garage_views.garage_modify, name='garage_modify'),
]

discount_code_patterns = [
    url(r'^$', discount_code_admin_views.generate_discount_code, name='discount_code_admin_home'),
    url(r'^(?P<discount_code>\d+)/$', discount_code_admin_views.manage_discount_code,
        name='discount_code_admin_home_with_code'),
]

urlpatterns = [
    url(r'^car-insurance/$', views.IndexTemplateView.as_view(vehicle_type='fourwheeler'), name='home_car'),
    url(r'^car-insurance/(?P<quote_id>[\w-]+)/$', views.quote_share, name='quote_share_fourwheeler'),
    url(r'^twowheeler-insurance/$', views.IndexTemplateView.as_view(vehicle_type='twowheeler'), name='home_twowheeler'),
    url(r'^twowheeler-insurance/(?P<quote_id>[\w-]+)/$', views.quote_share, name='quote_share_twowheeler'),
    url(r'^(?P<vehicle_type>[\w]+)/(?P<insurer_slug>[\w-]+)/generateForm/(?P<quote_id>[\w-]+)/$',
        views.generate_form_and_redirect, name='form_from_quote'),
    url(r'^(?P<vehicle_type>[\w]+)/(?P<insurer_slug>[\w-]+)/desktopForm/(?P<transaction_id>[\w-]+)/$',
        views.desktopForm, name='desktopForm'),
    url(r'^(?P<vehicle_type>[\w]+)/(?P<insurer_slug>[\w-]+)/submitForm/(?P<transaction_id>[\w-]+)/$',
        views.submit_buy_form),
    url(r'^(?P<vehicle_type>[\w]+)/payment/success/(?P<transaction_id>[\w-]+)/$',
        views.payment_success, name='payment_success'),
    url(r'^(?P<vehicle_type>[\w]+)/insurer/(?P<insurer_slug>[\w-]+)/response/$',
        views.payment_response, name='policy_slug_response'),
    url(r'^(?P<vehicle_type>[\w]+)/insurer/(?P<insurer_slug>[\w-]+)/payment/response/(?P<transaction_id>[\w-]+)/$',
        views.payment_response, name='payment_slug_response'),
    url(r'^(?P<vehicle_type>[\w]+)/payment/failure/$', views.payment_failure, name='payment_failure'),
    url(r'^(?P<vehicle_type>[\w]+)/payment/failure/(?P<transaction_id>[\w-]+)/$',
        views.payment_failure, name='payment_failure'),
    url(r'^(?P<vehicle_type>[\w]+)/product/failure/$', views.product_failure, name='product_failure'),

    url(r'^(?P<vehicle_type>[\w]+)/insurer/(?P<insurer_slug>[\w-]+)/process/(?P<transaction_id>[\w-]+)/$',
        views.offline_process_scheduled, name='offline_process_scheduled'),
    url(r'^submit-landing-page/$', views.get_results_from_landing_page, name='landing_page_data'),
    url(r'^(?P<vehicle_type>[\w]+)/goto-results/$', views.get_results_from_landing_page, name='landing_page_redirect'),
    url(r'^download-transaction-data/(?P<data_type>[\w]+)/$',
        views.download_transaction_details, name='transaction_data'),
    url(r'^download-landt-transaction-data/(?P<t_id>[\w-]+)/$',
        views.download_landt_transaction_details, name='download_landt_transaction_details'),
    url(r'^download-quotes-data/$', views.download_quotes_data, name='quotes_data'),
    url(r'^(?P<vehicle_type>[\w]+)/api/', include(api_patterns)),
    url(r'^insurer/iffco-tokio/response/$', views.iffco_policy_response),
    url(r'^insurer/l-t/policy-response/$', views.landt_policy_response_service),
    url(r'^admin/(?P<vehicle_type>[\w]+)/', include(admin_patterns)),
    url(r'^transaction/(?P<transaction_id>[\w-]+)/', views.get_transaction_details, name='get_transaction_details'),
    url(r'^garages/', include(garage_patterns)),
    url(r'^discount-code-admin/', include(discount_code_patterns)),
    url(r'^lnt-offline/$', views.lnt_offline_closure),
    url(r'^liberty-offline/$', views.liberty_offline_closure),
    url(r'^used-car/', include('motor_product.motor_used_car.urls', namespace='motor_used_car')),

    # Motor Inspection related urls
    url(r'^dealer/', include('motor_product.dealer.urls')),
    # url(r'^ops-admin/', include('motor_product.ops_admin.urls')),

    url(r'^download-inspection-data/$', views.download_scheduled_inspection_data, name='inspection_data'),
    url(r'^inspection-form/(?P<transaction_id>[\w-]+)/$', views.inspection_form, name='inspection_form'),
    url(r'^api/get-available-slots/$', views.get_available_slots, name='get_available_slots'),
    url(r'^schedule-inspection/$', views.InspectionCreateView.as_view(), name='schedule_inspection'),
    url(r'^agent/inspection/$', views.agent_inspection, name='agent_inspection'),
    url(r'^approve/inspection/$', views.approve_inspection, name='approve_inspection'),
    url(r'^inspection-details/(?P<inspection_id>[\w-]+)/$', views.inspection_details, name='inspection_details'),
    url(r'^inspection-approval/(?P<inspection_id>[\w-]+)/(?P<status>[\w-]+)/$',
        views.inspection_approval, name='inspection_approval'),
    url(r'^complete-inspection/(?P<transaction_id>[\w-]+)/$', views.complete_inspection, name='complete_inspection'),
    url(r'^inspection-report/(?P<inspection_id>[\w-]+)/$', views.inspection_report, name='inspection_report'),
    url(r'^add-inspection-document/(?P<inspection_id>[\w-]+)/$',
        views.add_inspection_document, name='add_inspection_document'),
    url(r'^generate-inspection-report/(?P<inspection_id>[\w-]+)/$',
        views.generate_report, name='generate_inspection_report'),
    url(r'^generate-inspection-report-pdf/(?P<inspection_id>[\w-]+)/$',
        views.generate_report_pdf, name='generate_inspection_report_pdf'),
    url(r'^cdaccount/', include('motor_product.cdaccount.urls')),
    url(r'^check-rsa-status/$', views.rsa_status, name='rsa_status'),
    url(r'^inspection-callback/(?P<inspection_id>[\w-]+)/$', views.inspection_callback, name='inspection_callback'),
    url(r'^car-insurance/overdrive', views.overdrive_callback, name='overdrive_car'),
    url(r'^populate/motor-product/$', views.populate_model_as_make, name='views.populate_model_as_make'),
    url(r'^quotes/share/$', views.share_quotes, name='share_quotes'),
    url(r'^test-component-page/$', views.test_bk_api, name='test_bk_api'),
    url(r'^customer-location-rto/$', views.location_to_rto_mapping, name='customer_location_rto'),
    url(r'^admin/', include('motor_product.integration.urls')),
]

urlpatterns += [
    url(r'offline/rsa/breakdown/(?P<member_uuid>[\w-]+)/$',
        offline_lead_views.breakdown_form, name='breakdown_form_with_uuid'),
    url(r'offline/rsa/breakdown/$', offline_lead_views.breakdown_form, name='breakdown_form'),
    url(r'offline/rsa/download/$', offline_lead_views.download, name='download'),
    # url(r'offline/rsa/download/(?P<search_id>[\w]+)/$', 'download', name='download'),
    url(r'^offline/(?P<features>[\w]+)/(?P<channel>[\w]+)/(?P<member_uuid>[\w-]*)$',
        offline_lead_views.form_page, name='form_page'),
    url(r'offline/(?P<channel>[\w]+)/$', offline_lead_views.sneekpeek, name='sneekpeek'),
]

urlpatterns += [
    url(r'^insurer-panel/', include('motor_product.insurer_panel.urls', namespace='insurerPanel')),
]
