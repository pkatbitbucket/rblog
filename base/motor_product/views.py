"""Motor Views."""

import copy
import datetime
import json
import os
import re
import traceback
import urllib
from copy import deepcopy
from fuzzywuzzy import process
from urlparse import urlparse
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
import pdfkit
from utils import ip2location
import xlwt
from braces import views as braces_views
from dateutil import relativedelta
from custom_dictionary import FACTO_GRID
from django.conf import settings
from django.contrib.auth.decorators import permission_required
from django.core.cache import caches
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.core.urlresolvers import reverse
from django.db.models import Q, Count
from django.forms import inlineformset_factory
from django.forms.forms import pretty_name
from django.http import (Http404, HttpResponse, HttpResponseBadRequest,
                         HttpResponseRedirect, JsonResponse)
from django.shortcuts import get_object_or_404, render, render_to_response
from django.template.defaultfilters import title
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from spyne.decorator import rpc
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoView
from spyne.service import ServiceBase
import mail_utils

import motor_product.forms as motor_forms
from motor_product.errors import INTEGRATION_ERRORS
import parser_utils
import prod_utils
import putils
import utils
from app_notification.views import send_message as notify_user
from brands.models import BrandPage
from core import activity as core_activity
from core.models import Company, Employee
from coverfox.serializers import DjangoQuerysetJSONEncoder
from flatpages.views import index as flatpage_view
from motor_product import errors
from motor_product.decorators import is_admin, json_to_query_dict
from motor_product.insurers import insurer_utils
from motor_product.insurers.future_generali.integration import view_proposal_xml as fg_xml
from motor_product.insurers.landt import get_landt_user_data, landt_utils
from motor_product.insurers.hdfc_ergo.custom_dictionaries import STATE_ID_TO_STATE_NAME, STATE_ID_TO_CITY_LIST
from motor_product import models as motor_models
from motor_product import cache_utils as motor_cache_utils
from prod_utils import RTO_STATE_TO_STATE_NAME_MAPPING
from marketing.models import Affiliates

from utils import fastlane_logger, LMSUtils, motor_logger, quote_logger, payment_logger, log_error, get_short_url

from wiki import models as wiki_models

from .exceptions import NoMatch
from cf_cns import notify as cf_notify
format_vehicle_type = lambda x: 'fourwheeler' if x == 'Private Car' else 'twowheeler'

cache = caches['motor-results']
default_cache = caches['default']

VEHICLE_URL_MAP = {
    'fourwheeler': 'car-insurance',
    'twowheeler': 'twowheeler-insurance',
}

VEHICLE_TYPE_TO_URL_TAG_MAP = {
    'Private Car': 'fourwheeler',
    'Twowheeler': 'twowheeler',
}



class BaseMixin(braces_views.JSONResponseMixin, braces_views.AjaxResponseMixin):
    json_encoder_class = DjangoQuerysetJSONEncoder
    vehicle_type = ''
    mapped_vehicle_type = ''
    serialized_data_key_name = 'serialized_response'
    default_serialized_response = {
        'success': True,
        'errorCode': None,
        'errorMessage': None,
        'errorItems': [],
        'data': {},
    }

    def dump_messages(self, msgs, type='INFO'):
        user_messages = []
        if isinstance(msgs, basestring):
            user_messages.append({'tag': type, 'message': msgs})
        else:
            for field, contents in msgs.items():
                for content in contents:
                    if field == '__all__':
                        user_messages.append({'tag': type, 'message': "%s" % (content)})
                    else:
                        user_messages.append({'tag': type, 'message': "%s: %s" % (pretty_name(field), content)})
        return user_messages

    @method_decorator(is_admin)
    @method_decorator(xframe_options_exempt)
    def dispatch(self, *args, **kwargs):
        self.vehicle_type = self.vehicle_type or self.kwargs.get('vehicle_type', '')
        self.mapped_vehicle_type = {
            'fourwheeler': 'Private Car',
            'twowheeler': 'Twowheeler',
        }.get(self.vehicle_type, '')
        return super(BaseMixin, self).dispatch(*args, **kwargs)

    def get_ajax(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)

    def post_ajax(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class IndexTemplateView(BaseMixin, TemplateView):

    def dispatch(self, request, *args, **kargs):
        response = super(IndexTemplateView, self).dispatch(request, *args, **kargs)
        response.delete_cookie("affiliate_name")
        return response

    def get_context_data(self, **kwargs):
        context = super(IndexTemplateView, self).get_context_data(**kwargs)
        referer = self.request.META.get('HTTP_REFERER', "")
        referer_path = urlparse(referer).path

        if referer_path == '/lp/car-insurance/buy/':
            lp_page_type = 'buy'
        elif referer_path == '/lp/car-insurance/buy-otp/':
            lp_page_type = 'buy-otp'
        else:
            lp_page_type = None

        if lp_page_type:
            try:
                affiliate_name = self.request.COOKIES.get('affiliate_name', '')
                affiliate = Affiliates.objects.get(affiliate_name=affiliate_name, page_type=lp_page_type)

            except Affiliates.DoesNotExist:
                pass
            else:
                tracker_id = self.request.TRACKER.session_key
                context['affiliate_url'] = "{}{}".format(affiliate.affiliate_url, tracker_id)

        from campaign.landing_pages import motor, bike
        if self.vehicle_type == 'fourwheeler':
            TPARTY_LIST = motor.TPARTY_LIST
            TPARTY_LOGO_MAPPING = motor.TPARTY_LOGO_MAPPING
        elif self.vehicle_type == 'twowheeler':
            TPARTY_LIST = bike.TPARTY_LIST
            TPARTY_LOGO_MAPPING = bike.TPARTY_LOGO_MAPPING
        else:
            TPARTY_LIST = []
            TPARTY_LOGO_MAPPING = {}

        tparty = self.request.GET.get('tparty')
        if tparty and tparty in TPARTY_LIST:
            context['tparty'] = tparty
            context['tparty_logo'] = TPARTY_LOGO_MAPPING.get(tparty)

        return context

    def get_template_names(self):
        self.template_name = (
            "motor_product/mobile-test.html" if self.vehicle_type == 'fourwheeler' else "motor_product/mobile.html"
        ) if self.request.user_agent.is_mobile else "motor_product/desktop.html"

        return super(IndexTemplateView, self).get_template_names()


class ModelListView(braces_views.CsrfExemptMixin, BaseMixin, ListView):
    http_method_names = ['post']
    model = motor_models.Model

    def post_ajax(self, request, *args, **kwargs):
        response = super(ModelListView, self).get_ajax(request, *args, **kwargs)
        return self.render_json_response(response.context_data[self.serialized_data_key_name])
    post = post_ajax  # Hack to allow post request

    def get_queryset(self):
        queryset = super(ModelListView, self).get_queryset()
        return queryset.filter(vehicle_type=self.mapped_vehicle_type)

    def get_context_data(self):
        context_data = super(ModelListView, self).get_context_data()
        context_data[self.serialized_data_key_name] = serialized_response = copy.deepcopy(self.default_serialized_response)
        serialized_response['data']['models'] = map(
            lambda model: {
                'id': model['id'],
                'name': model['make__name'] + ' ' + model['name'],
            }, context_data['model_list'].order_by('make__name', 'name').values('id', 'make__name', 'name')
        )
        return context_data


class RegistrationDetailCreateView(braces_views.CsrfExemptMixin, BaseMixin, CreateView):
    model = motor_models.NewRegistrationDetail
    form_class = motor_forms.RegistrationDetailCreateForm

    def form_invalid(self, form):
        return self.render_json_response({'success': False, 'errorItems': form.errors}, status=400)

    def form_valid(self, form):
        restrict_access = putils.get_request_ip(self.request) in default_cache.get('restricted_ips', [])
        source = self.kwargs.get('source', 'fastlane')
        assert source == "fastlane"
        self.object = form.save()

        statistics_info = {}

        serialized_response = deepcopy(self.default_serialized_response)
        if restrict_access:
            statistics_info.update({'fastlane_status': 'REJECTED'})
            rds = prod_utils.create_registration_detail_statistics(self.request, statistics_info)
            return self.render_json_response(serialized_response)

        serialized_response['data'][source] = self.object.fastlane
        serialized_response['data']['coverfox'] = self.object.coverfox
        try:
            vehicle_master_id = serialized_response['data'].get('vehicle_master_id')
            if not vehicle_master_id:
                vehicle_master, is_exact = self.object.get_matching_vehicle_master()
                if vehicle_master:
                    serialized_response['data']['vehicle_master_id'] = vehicle_master.id
                else:
                    raise NoMatch
            else:
                vehicle_master = motor_models.VehicleMaster.objects.get(pk=vehicle_master_id)
            if not vehicle_master.vehicle_type == motor_models.inverse_format_vehicle_type(self.kwargs.get("vehicle_type")):
                serialized_response['data'].pop('vehicle_master_id')
                raise NoMatch
            serialized_response['data']['vehicle_model_id'] = vehicle_master.model.id
            serialized_response['data']['vehicle_make'] = vehicle_master.make.name
            serialized_response['data']['vehicle_model'] = vehicle_master.model.name
            serialized_response['data']['vehicle_variant'] = vehicle_master.variant
            serialized_response['data']['vehicle_cc'] = vehicle_master.cc

            rto_master = self.object.get_rto_master()
            if rto_master:
                serialized_response['data']['rto_name'] = rto_master.rto_name
                serialized_response['data']['rto_id'] = rto_master.id

            fastlane_logger.info('Found: vehicle master found for %s: %s' % (
                self.object.registration_number, vehicle_master.model.id)
            )

            statistics_info.update(
                {
                    'fastlane_status': 'SUCCESS',
                    'vehicle_master': vehicle_master,
                    'number_of_matches': 1
                }
            )

        except KeyError as e:  # No registration details found / Invalid registration number

            fastlane_logger.info('No vehicle master found for %s: %s' % (
                self.object.registration_number, e.message)
            )

            statistics_info.update(
                {'fastlane_status': 'ERROR_INVALID_RESPONSE'}
            )

        except NoMatch as e:  # No registration details found / Invalid registration number
            natural_key = self.object.fastlane_natural_key(raise_exception=False)
            if natural_key:
                natural_key = " ".join(natural_key)
            else:
                natural_key = self.object.registration_number
            fastlane_logger.info(
                'No vehicle master found for %s %s' % (
                    self.object.registration_number, natural_key
                )
            )
        rds = prod_utils.create_registration_detail_statistics(self.request, statistics_info)
        serialized_response['data']['rds_id'] = rds.id
        return self.render_json_response(serialized_response)


def vehicle_confirmation(request, vehicle_type):
    """
        When ever a user 'accept' or 'change' the mapped object ('registration_number'
        and the 'vehicle_master_id') in RegistrationDetailStatistics, an event
        'accepted' or 'changed' will be fired. This view will capture those events.
    """

    response = {'success': False, 'message': 'Invalid Request'}

    confirmation_type = request.POST.get('confirmation_type')
    if (not request.method == 'POST') or (confirmation_type not in ['accepted', 'changed']) or (request.POST.get('rds_id') in [None, 'false']):
        return HttpResponse(json.dumps(response), status=404)

    statistics = motor_models.RegistrationDetailStatistics.objects.filter(id=request.POST.get('rds_id')).first()

    if statistics and statistics.fastlane_status in ['SUCCESS', 'SUCCESSFUL']:
        statistics.user_status = confirmation_type.upper()
        statistics.data = json.loads(request.POST.get('data', '{}'))
        statistics.save()
        response = {'success': True, 'message': confirmation_type}
    else:
        response['message'] = 'Not Found'

    return HttpResponse(json.dumps(response))


@csrf_exempt
def vehicles(request, vehicle_type, model_id=None):
    if request.method == 'POST':
        fuel_type = request.POST.get('fuel_type', None)
        if fuel_type == 'ALL':
            fuel_type = None
        response = prod_utils.get_vehicles(vehicle_type, model_id, fuel_type)
        return HttpResponse(json.dumps(response))
    return HttpResponse(json.dumps({'success': True}))


@is_admin
@json_to_query_dict
# @lms_event("result_page", ["POST"])
def quotes_async(request, vehicle_type, quote_id=None):
    invalid_response = {
        'success': False,
        'errorCode': errors.EC_INVALID_PARAMETERS,
        'errorMessage': errors.messages[
            errors.EC_INVALID_PARAMETERS
        ],
        'errorItems': [],
        'data': {}
    }
    if request.method == "POST":
        if quote_id:
            return HttpResponse(json.dumps(
                motor_cache_utils.fetch_quote_premiums_from_cache(quote_id)
            ))

        else:
            request_data = {}
            post_parameters = parser_utils.normalize_request_dict(request)
            if not motor_models.RTOMaster.objects.filter(
                rto_code="-".join(prod_utils.fix_reg_number_array(post_parameters.get('registrationNumber[]', []))[0:2])
            ).exists():
                return HttpResponse(json.dumps(invalid_response))

            request_data['data'] = post_parameters
            request_data['TRACKER'] = request.TRACKER
            request_data['user'] = request.user
            if request.COOKIES.get('employee_code', None):
                employee = motor_models.CorporateEmployee.objects.filter(employee_code=request.COOKIES.get('employee_code'),
                                                   company__slug=request.COOKIES.get('company'),
                                                   company__employee_discount=True).first()
                if employee:
                    request_data['data']['corporate_employee_discount'] = True
            try:
                quote_info = prod_utils.create_quote(vehicle_type, request_data, False)
            except Exception:
                utils.log_error(quote_logger, request=request)
                return HttpResponse(json.dumps(invalid_response))
            else:
                return HttpResponse(json.dumps(quote_info))
    return HttpResponse(json.dumps({'success': True, 'finished': True}))


@is_admin
@json_to_query_dict
# @lms_event("result_page", ["POST"])
def quotes(request, vehicle_type, quote_id=None):
    if request.method == 'POST':
        request_data = {}
        post_parameters = parser_utils.normalize_request_dict(request)
        request_data['data'] = post_parameters
        request_data['TRACKER'] = request.TRACKER
        request_data['user'] = request.user

        try:
            return HttpResponse(json.dumps(
                prod_utils.create_quote(vehicle_type, request_data)
            )
            )
        except:
            return HttpResponse(json.dumps({'success': False,
                                            'errorCode': errors.EC_INVALID_PARAMETERS,
                                            'errorMessage': errors.messages[errors.EC_INVALID_PARAMETERS],
                                            'errorItems': [],
                                            'data': {}}))

    return HttpResponse(json.dumps({'success': True}))


@csrf_exempt
@require_POST
@is_admin
@json_to_query_dict
def refresh_quote(request, vehicle_type, quote_id, insurer_slug):
    quote_parameters = parser_utils.normalize_request_dict(request)
    success, response, lerrors = prod_utils.refresh_quote_details(vehicle_type, request, quote_id, quote_parameters, insurer_slug, request.GET.get('force_quote_refresh', "false"))
    error = None
    if success is False:
        if errors.messages[response] == INTEGRATION_ERRORS['RTO_MAPPING_NOT_FOUND']:
            error = 'RTO not covered by insurer.'
        elif errors.messages[response] == INTEGRATION_ERRORS['INSURER_AGE_RESTRICTED']:
            error = "%s's too old to be insured." % vehicle_type
        else:
            error = 'Pfffbt. Insurer server down.'
    return HttpResponse(json.dumps({
        'success': success,
        'errorCode': None if success else response,
        'errorMessage': error,
        'errorItems': [] or lerrors,
        'data': response if success else {}
    }))


@require_POST
def refresh_transaction_quote(request, vehicle_type, transaction_id):
    quote_parameters = parser_utils.normalize_request_dict(request)
    transaction = get_object_or_404(motor_models.Transaction, transaction_id=transaction_id)
    quote = motor_models.Quote.objects.create_copy_quote(
        transaction.quote,
        raw_data=quote_parameters)

    transaction.quote = quote
    if 'last_quote_refresh' in transaction.raw.keys():
        transaction.raw.pop('last_quote_refresh')
    transaction.save()
    prod_utils.refresh_transaction_details(transaction)
    transaction.refresh_from_db()
    return JsonResponse({
        'success': True,
        'errorCode': None,
        'errorMessage': None,
        'errorItems': [],
        'data': {
            'quote_id': quote.quote_id,
            "quote_response": transaction.raw['quote_response'],
            "quote_parameters": transaction.quote.raw_data}
    })


@require_POST
def quote_info(request, vehicle_type, quote_id):
    quote = get_object_or_404(motor_models.Quote, quote_id=quote_id)

    data = {}
    data['quote_parameters'] = quote.raw_data
    data['rto_info'] = quote.rto_info

    if quote.vehicle:
        data['vehicle_make_id'] = quote.vehicle.make.id
        data['vehicle_make_name'] = quote.vehicle.make.name
        data['vehicle_model_id'] = quote.vehicle.model.id
        data['vehicle_model_name'] = quote.vehicle.model.name
        data['vehicle_variant'] = quote.vehicle.variant
        data['vehicle_cc'] = quote.vehicle.cc
        data['facto_grid'] = FACTO_GRID.get(quote.vehicle.make.name, {})

    return JsonResponse({
        'success': True,
        'errorCode': None,
        'errorMessage': None,
        'errorItems': [],
        'data': data
    })


@require_POST
def refresh_transaction(request, vehicle_type, transaction_id):
    if request.method == 'POST':
        try:
            transaction = motor_models.Transaction.objects.get(transaction_id=transaction_id)
            refreshed = prod_utils.refresh_transaction_details(transaction)
            if not refreshed:
                return HttpResponse(json.dumps({'success': False,
                                                'errorCode': errors.ES_CANNOT_FETCH_DATA,
                                                'errorMessage': errors.messages[errors.ES_CANNOT_FETCH_DATA],
                                                'errorItems': [],
                                                'data': {}}))
            else:
                transaction.refresh_from_db()
                quote_data = transaction.raw['quote_response']
                return HttpResponse(json.dumps({'success': True,
                                                'errorCode': None,
                                                'errorMessage': None,
                                                'errorItems': [],
                                                'data': quote_data}))
        except:
            return HttpResponse(json.dumps({'success': False,
                                            'errorCode': errors.EC_INVALID_PARAMETERS,
                                            'errorMessage': errors.messages[errors.EC_INVALID_PARAMETERS],
                                            'errorItems': [],
                                            'data': {}}))


@require_POST
@is_admin
@json_to_query_dict
def generate_form_simply(request, vehicle_type, quote_id, insurer_slug):
    quote_parameters = parser_utils.normalize_request_dict(request)
    initial_data = json.loads(quote_parameters.pop("initial_data", '{}'))

    form_details = prod_utils.generate_form_simply(
        vehicle_type, request, quote_id, quote_parameters, insurer_slug, initial_data=initial_data)
    if form_details is None:
        return HttpResponse(json.dumps({
            'success': False,
            'errorCode': errors.EC_INVALID_PARAMETERS,
            'errorMessage': errors.messages[errors.EC_INVALID_PARAMETERS],
            'errorItems': [{'parameter': 'activity_id', 'errorMessage': 'Invalid activity id.'}],
            'data': {}
        }))
    else:
        return HttpResponse(json.dumps({
            'success': True,
            'errorCode': None,
            'errorMessage': None,
            'errorItems': [],
            'data': {'transactionId': form_details}
        }))


@never_cache
def generate_form_and_redirect(request, vehicle_type, insurer_slug, quote_id):
    success, quote_details, errors = prod_utils.refresh_quote_details(
        vehicle_type, request, quote_id, None, insurer_slug)
    if not success or not quote_details.get('activityId', None):
        return HttpResponseRedirect('/motor/' + vehicle_type + '/product/failure/')

    activity_id = quote_details['activityId']
    transaction_id = prod_utils.generate_form(
        vehicle_type, request, activity_id, None)
    if transaction_id is None:
        return HttpResponseRedirect('/motor/' + vehicle_type + '/product/failure/')

    return HttpResponseRedirect('/motor/' + vehicle_type + '/' + insurer_slug + '/desktopForm/' + transaction_id + '/')


@require_POST
@json_to_query_dict
def generate_email_for_quote(request, vehicle_type):
    try:
        post_parameters = parser_utils.normalize_request_dict(request)
        quotes = json.loads(post_parameters['quotes'])
        parameters = {}
        quote_parameters = copy.deepcopy(post_parameters)
        quote_parameters.pop('quotes', None)

        if quote_parameters.get('emailId', None):
            parameters['emailId'] = quote_parameters['emailId']
            del quote_parameters['emailId']

        quote_parameters['payment_mode'] = 'PAYMENT_GATEWAY'

        # Temporary hack for used vehicle (only for quote)
        quote_parameters = insurer_utils.modify_parameters_for_used_vehicle(quote_parameters)

        parameters['quote_parameters'] = quote_parameters
        parameters['quotes'] = quotes

        vehicle = motor_models.VehicleMaster.objects.get(id=quote_parameters['vehicleId'])
        is_new = True if quote_parameters['isNewVehicle'] == '1' else False
        quote = motor_models.Quote(
            is_new=is_new,
            raw_data=quote_parameters,
            vehicle=vehicle
        )
        quote.save()

        parameters['quoteId'] = quote.quote_id
        parameters['vehicle_type'] = vehicle_type

        if not parameters.get('emailId', None):
            user = request.user
            if user and user.is_authenticated():
                parameters['emailId'] = user.email

        if not parameters.get('emailId', None):
            return HttpResponse(json.dumps({'success': False}))

        mail_utils.send_mail_for_quote(parameters)
        return HttpResponse(json.dumps({'success': True}))
    except:
        utils.log_error(motor_logger, request=request)
        return HttpResponse(json.dumps({'success': False}))


# @lms_event("buy_page", ["GET"])
@xframe_options_exempt
@never_cache
def desktopForm(request, vehicle_type, insurer_slug, transaction_id):
    transaction = get_object_or_404(motor_models.Transaction, transaction_id=transaction_id)
    insurer = transaction.insurer
    insurer_slug = transaction.insurer.slug
    vehicle_type = motor_models.format_vehicle_type(transaction.vehicle_type)

    if request.method == 'POST':
        post_qdict = copy.deepcopy(request.POST)
        form_details = parser_utils.qdict_to_dict(post_qdict)
        reset_form_val = form_details.pop('resetForm', 'false')
        reset_form = True if reset_form_val == 'true' else False
        if reset_form:
            if transaction.status in ['DRAFT', "PENDING"] and request.IS_INTERNAL:
                transaction.raw['user_form_details'] = {}
                transaction.save()
            return HttpResponseRedirect(reverse('desktopForm', args=(
                vehicle_type, insurer_slug, transaction_id)
            ))
        else:
            retval = prod_utils.save_form_partial_details(form_details, transaction)
        transaction.refresh_from_db()
        transaction.track_activity(core_activity.VIEWED, 'proposal')
        return HttpResponse(json.dumps({'success': retval}))
    else:
        # show_reset_button = False
        searched_for = transaction.quote.policy_type
        if searched_for == "renew" and transaction.quote.policy_expired() and transaction.status in ['DRAFT', "PENDING"]:
            transaction.status = "FORM_EXPIRED"
            transaction.save()
        if not transaction.is_complete:
            created_time = transaction.created_on.astimezone(
                timezone.get_current_timezone())
            created_time = created_time.replace(tzinfo=None)
            expiry_time = datetime.datetime.strptime('01-01-2015', '%d-%m-%Y')

            # quote_cache_key = transaction.quote.quote_id + '_' + insurer_slug
            # cached_quote_response = cache.get(quote_cache_key, {}).get('data')
            # if cached_quote_response:
            #     transaction.raw['quote_response'] = cached_quote_response
            #     transaction.save()
            # elif not prod_utils.refresh_transaction_details(transaction):
            #     motor_logger.error("Couldnt refresh transaction: " + transaction.transaction_id)

            if created_time < expiry_time:
                raise Http404

        form_data = prod_utils.get_buy_form_details(transaction_id, api_call=False)
        if form_data is None:
            return HttpResponseRedirect('/motor/%s/' % VEHICLE_URL_MAP[vehicle_type])
        if not (form_data.get('user_form_details') and form_data.get('user_form_details', {}).get('cust_first_name')) and transaction.status in ["DRAFT", "PENDING"] and not request.IS_INTERNAL:
            # Pre filling form data based on last transaction seen/touched if current one is empty
            last_transaction = motor_models.Transaction.objects.filter(
                transaction_id=request.session.get('last_seen_transaction_id', None),
                raw__contains='"user_form_details"').last()
            if last_transaction:
                # Fields to populate
                included_keys = [
                    'aan_number', 'add-same',
                ] + [
                    'add_building_name', 'add_extra_line', 'add_house_no',
                    'add_landmark', 'add_pincode', 'add_street_name',
                ] + [
                    'cust_dob', 'cust_email', 'cust_first_name', 'cust_gender', 'cust_last_name', 'cust_marital_status',
                    'cust_pan', 'cust_phone', 'nominee_age', 'nominee_name', 'nominee_relationship',
                ]
                # TODO: Map other fields:
                '''
                    ['add_city', 'add_district', 'add_state',
                    'loc_add_city', 'loc_add_district', 'loc_add_state',
                    'reg_add_city', 'reg_add_district', 'reg_add_state',
                    'loc_reg_add_city', 'loc_reg_add_district', 'loc_reg_add_state']
                '''
                quote = transaction.quote
                last_quote = last_transaction.quote
                if(
                    (last_quote.raw_data['registrationNumber[]'] == quote.raw_data['registrationNumber[]']) and
                    (last_quote.raw_data['registrationDate'] == quote.raw_data['registrationDate']) and
                    (last_quote.raw_data['isNewVehicle'] == quote.raw_data['isNewVehicle']) and
                    (last_quote.vehicle == quote.vehicle)
                ):
                    # Removed policy expired check from here.
                    # (last_quote.policy_expired() is quote.policy_expired()) and
                    # Fields to populate if it's not a 'totally' new search
                    included_keys += [
                        'vehicle_reg_no', 'vehicle_chassis_no', 'vehicle_engine_no', 'is_car_financed', 'isFastlaneFieldVisible'
                    ]
                    included_keys += ['past_policy_insurer', 'past_policy_number', 'car-financier-text', 'car-financier']
                else:
                    form_data['halt_at_first_stage'] = True
                previous_form_data = last_transaction.raw.get('user_form_details', {})
                if insurer_slug in ['future-generali', 'hdfc-ergo', 'iffco-tokio', 'l-t', 'reliance', 'tata-aig']:
                    included_keys += [
                        'reg_add_building_name', 'reg_add_extra_line', 'reg_add_house_no',
                        'reg_add_landmark', 'reg_add_pincode', 'reg_add_street_name',
                    ] + [
                        'loc_reg_add_building_name', 'loc_reg_add_extra_line', 'loc_reg_add_house_no',
                        'loc_reg_add_landmark', 'loc_reg_add_pincode', 'loc_reg_add_street_name',
                    ] + [
                        'loc_add_building_name', 'loc_add_extra_line', 'loc_add_house_no',
                        'loc_add_landmark', 'loc_add_pincode', 'loc_add_street_name',
                    ]
                else:
                    included_keys.remove('add-same')
                for key in set(included_keys).intersection(previous_form_data.keys()):
                    form_data['user_form_details'][key] = previous_form_data[key]
                # If selected insurer doesn't support nominee relationship selected in previous transaction then select 'other'
                # If 'other' isn't one of the options either then puke an error on frontend instead
                nominee_relationship = form_data['user_form_details'].get('nominee_relationship')
                relationship_list = form_data['form_details'].get('relationship_list')
                if (
                    relationship_list and nominee_relationship and
                    nominee_relationship not in relationship_list and
                    'Other' in relationship_list
                ):
                    form_data['user_form_details']['nominee_relationship'] = 'Other'
                if len(form_data['user_form_details'].keys()) > 0:
                    form_data['user_form_details']['is_pre_populated'] = True
                    # show_reset_button = True

        # Get latest inspection which is not cancelled (pending, approved, rejected, done)
        inspection = transaction.inspection_set.filter(
            ~Q(status=motor_models.Inspection.CANCELLED)
        ).order_by('created_on').last()
        request.session.update({'last_seen_transaction_id': transaction.transaction_id})
        request.session.modified = True
        if hasattr(transaction.insurer, 'cdaccount'):
            cd_balance = transaction.insurer.cdaccount.balance
        else:
            cd_balance = 0
        transaction.quote.raw_data['payment_mode'] = 'PAYMENT_GATEWAY'
        transaction.quote.save()
        return render(request, "motor_product/desktop-buy.html", {
            'insurer_id': insurer.id,
            'insurer_title': insurer.title,
            'insurer_slug': insurer.slug,
            'transaction_id': transaction_id,
            'data': json.dumps(form_data),
            'vehicle_type': vehicle_type,
            'inspection': inspection,
            'mode': request.COOKIES.get('mode', 'assisted'),
            'tparty': request.GET.get('tparty', None),
            'is_mobile': request.user_agent.is_mobile,
            'affiliate_source': request.COOKIES.get('affiliateSource', ''),
            'transaction': transaction,
            'cd_enabled': 1 if(cd_balance and request.user.groups.filter(name="CD_ACCOUNT_ADMIN").exists()) else 0,
            'cd_balance': cd_balance,
            'toll_free_number': settings.CAR_TOLL_FREE,
            'show_reset_button': False,  # getattr(request, 'IS_INTERNAL', False) and not transaction.is_complete,
        })


# TODO: Remove unnecessary arguments
@never_cache
def submit_buy_form(request, vehicle_type, insurer_slug, transaction_id):

    transaction = get_object_or_404(motor_models.Transaction, transaction_id=transaction_id)
    insurer_slug = transaction.insurer.slug
    vehicle_type = motor_models.format_vehicle_type(transaction.vehicle_type)

    if request.method == 'GET':
        if transaction.status != 'PENDING':
            # if vehicle_type == 'twowheeler':
            #     message = errors.PAYMENT_RETRY['transaction-is-already-done']
            #     message['description'] = message['description'].format(
            #         **{
            #             'insurer_title': transaction.insurer.title,
            #             'customer_first_name': transaction.customer_name()
            #         }
            #     )
            #     return JsonResponse(message, status=400)

            return HttpResponseRedirect('/motor/' + vehicle_type + '/product/failure/')

        if not request.user.groups.filter(name="CD_ACCOUNT_ADMIN").exists():
            transaction.quote.raw_data['payment_mode'] = 'PAYMENT_GATEWAY'
            transaction.quote.save()

        payment_data, payment_url = prod_utils.initiate_payment_gateway(transaction_id)
        response_data = {}
        response_data['data'] = payment_data
        response_data['redirect_url'] = payment_url
        template_name = 'motor_product/post_transaction_data.html'

        if payment_url == '':
            if vehicle_type == 'twowheeler':
                message = errors.PAYMENT_RETRY['redirection-failed']
                message['message'] = message['message'].format(**{'insurer_title': transaction.insurer.title})
                message['description'] = message['description'].format(
                    **{
                        'insurer_title': transaction.insurer.title,
                        'customer_first_name': transaction.customer_name()
                    }
                )
                return JsonResponse(message, status=400)
            return HttpResponseRedirect('/motor/' + vehicle_type + '/product/failure/')

        response = render(request, template_name, {
            'transaction_id': transaction_id,
            'data': response_data,
        })
        transaction.set_cookie(response)
        transaction.refresh_from_db()
        transaction.track_activity(core_activity.VIEWED, 'payment')

        return response

    if request.method == 'POST':
        # TODO: This should exclude completed transactions
        transaction = get_object_or_404(motor_models.Transaction, transaction_id=transaction_id)
        request_data = parser_utils.qdict_to_dict(request.POST)

        # TODO: request_data should be saved in Transaction model
        if not request_data and request.body:
            request_data = json.loads(request.body)
            request_data = parser_utils.clean_body_parameters(request_data)

        # Clean unicode characters
        for field, value in request_data.items():
            request_data[field] = re.sub(r'[^\x00-\x7F]+', '', value)

        if transaction.vehicle_type == 'Twowheeler' and 'desktopForm' not in request.META['HTTP_REFERER']:
            prod_utils.new_save_form_partial_details(
                vehicle_type, request_data, insurer_slug, transaction
            )
            transaction = prod_utils.new_user_data_to_old_data(transaction)
            proposal_request_data = transaction.raw['user_form_details']
        else:
            prod_utils.save_form_partial_details(request_data, transaction)
            proposal_request_data = request_data

        try:
            transaction, is_saved = prod_utils.process_save_transaction(request_data, transaction, proposal_request_data)
        except:
            is_saved = False
            # Proposal failure response mail
            stacktrace = traceback.format_exc()
            if settings.PRODUCTION:
                mail_utils.send_mail_for_proposal_failure(transaction, str(stacktrace))
            else:
                assert False, stacktrace
        if not transaction:
            return JsonResponse({
                'success': False,
                'errorCode': errors.EC_INVALID_PARAMETERS,
                'errorMessage': errors.messages[errors.EC_INVALID_PARAMETERS],
                'errorItems': [{'parameter': 'transaction_id',
                                'errorMessage': 'Invalid transaction id or transaction already completed.'}],
                'data': {}
            })

        if not is_saved:
            transaction.status = 'PROPOSAL FAILED'
            transaction.save()
            transaction.track_activity(core_activity.FAILED, 'proposal')
            return JsonResponse({
                'success': False,
                'errorCode': errors.ES_CANNOT_SAVE_DATA,
                'errorMessage': errors.messages[errors.ES_CANNOT_SAVE_DATA],
                'errorItems': [],
                'data': {}
            })

        is_new_vehicle = (transaction.quote.raw_data['isNewVehicle'] == '1')
        is_used_vehicle = (transaction.quote.raw_data['isUsedVehicle'] == '1')

        if not is_new_vehicle and not is_used_vehicle:
            is_policy_expired = False

            past_policy_end_date_str = transaction.quote.raw_data[
                'pastPolicyExpiryDate'] if transaction.quote.raw_data['isNewVehicle'] == '0' else None
            if past_policy_end_date_str:
                past_policy_end_date = datetime.datetime.strptime(
                    past_policy_end_date_str, "%d-%m-%Y")
                past_policy_end_date = past_policy_end_date.replace(
                    hour=23, minute=59, second=0, microsecond=0)
                if past_policy_end_date < datetime.datetime.now():
                    is_policy_expired = True

            if is_policy_expired:
                # Get schedule details and create an inspection visit
                # Proceed to payment if insurer permits it
                can_continue_to_buy = prod_utils.process_expired_policy(transaction_id)
                if not can_continue_to_buy:
                    return JsonResponse({
                        'success': False,
                        'errorCode': errors.EC_FORBIDDEN_REQUEST,
                        'errorMessage': errors.messages[errors.EC_FORBIDDEN_REQUEST],
                        'errorItems': [{'parameter': 'transaction_id',
                                        'errorMessage': 'Cannot buy online '
                                                        'since previous policy for transaction has already expired.'}],
                        'data': {}
                    })

        transaction.status = 'PENDING'
        transaction.save()

        return JsonResponse({
            'success': True,
            'errorCode': None,
            'errorMessage': None,
            'errorItems': [],
            'data': {}
        })
    else:

        if transaction.status != 'PENDING':
            if vehicle_type == 'twowheeler':
                message = errors.PAYMENT_RETRY['redirection-failed']
                message['message'] = message['message'].format(**{'insurer_title': transaction.insurer.title})
                message['description'] = message['description'].format(
                    **{
                        'insurer_title': transaction.insurer.title,
                        'customer_first_name': transaction.customer_name()
                    }
                )
                return JsonResponse(message, status=400)
            return HttpResponseRedirect('/motor/' + vehicle_type + '/product/failure/')

        # TODO: should be passed transaction object instead of the details
        payment_data, payment_url = prod_utils.initiate_payment_gateway(transaction_id)
        if payment_url == '':
            if vehicle_type == 'twowheeler':
                message = errors.PAYMENT_RETRY['redirection-failed']
                message['message'] = message['message'].format(**{'insurer_title': transaction.insurer.title})
                message['description'] = message['description'].format(
                    **{
                        'insurer_title': transaction.insurer.title,
                        'customer_first_name': transaction.customer_name()
                    }
                )
                return JsonResponse(message, status=400)

            return HttpResponseRedirect('/motor/' + vehicle_type + '/product/failure/')

        template_name = 'motor_product/post_transaction_data.html'
        response_data = {
            'data': payment_data,
            'redirect_url': payment_url
        }

        response = render(request, template_name, {
            'transaction_id': transaction_id,
            'data': response_data,
        })

        transaction.set_cookie(response)

        return response


def pincode_details(request, vehicle_type, insurer_slug):
    pincode = request.POST.get('pincode', '')

    if not pincode:
        return HttpResponse(json.dumps({'success': False, 'errorCode': errors.EC_MISSING_PARAMETERS, 'errorMessage': errors.messages[errors.EC_MISSING_PARAMETERS], 'errorItems': [{'parameter': 'pincode', 'errorMessage': 'Pincode not provided.'}], 'data': {}}))

    has_pincode_details, pincode_details = prod_utils.get_pincode_details(
        vehicle_type, insurer_slug, pincode)

    if not has_pincode_details:
        return HttpResponse(json.dumps({'success': False, 'errorCode': errors.EC_INVALID_REQUEST, 'errorMessage': errors.messages[errors.EC_INVALID_REQUEST], 'errorItems': [], 'data': {}}))

    if not pincode_details:
        return HttpResponse(json.dumps({'success': False, 'errorCode': errors.EC_INVALID_PARAMETERS, 'errorMessage': errors.messages[errors.EC_INVALID_PARAMETERS], 'errorItems': [{'parameter': 'pincode', 'errorMessage': 'Invalid pincode.'}], 'data': {}}))

    return HttpResponse(json.dumps({
        'success': True,
        'errorCode': None,
        'errorMessage': None,
        'errorItems': [],
        'data': pincode_details
    }
    )
    )


@csrf_exempt
def get_master_rtos(request, vehicle_type):
    if request.method == 'POST':
        rtos = motor_models.RTOMaster.objects.all().order_by('rto_code')
        rtos_list = []
        for rto in rtos:
            rtos_list.append(rto.get_dictionary())
        return HttpResponse(json.dumps({'success': True, 'errorCode': None, 'errorMessage': None, 'errorItems': [], 'data': {'rtos': rtos_list}}))
    return HttpResponse(json.dumps({'success': True}))


def get_cities(request, vehicle_type, insurer_slug):
    state = request.POST.get('state', '')
    if not state:
        return HttpResponse(json.dumps({'success': False, 'errorCode': errors.EC_MISSING_PARAMETERS, 'errorMessage': errors.messages[errors.EC_MISSING_PARAMETERS], 'errorItems': [{'parameter': 'state', 'errorMessage': 'No state provided.'}], 'data': {}}))

    has_cities, cities = prod_utils.get_cities(
        vehicle_type, insurer_slug, state)

    if not has_cities:
        return HttpResponse(json.dumps({'success': False, 'errorCode': errors.EC_INVALID_REQUEST, 'errorMessage': errors.messages[errors.EC_INVALID_REQUEST], 'errorItems': [], 'data': {}}))

    if not cities:
        return HttpResponse(json.dumps({'success': False, 'errorCode': errors.EC_INVALID_PARAMETERS, 'errorMessage': errors.messages[errors.EC_INVALID_PARAMETERS], 'errorItems': [{'parameter': 'state', 'errorMessage': 'Invalid state.'}], 'data': {}}))

    if vehicle_type == 'twowheeler':
        return JsonResponse({
            'success': True,
            'errorCode': None,
            'errorMessage': None,
            'errorItems': [],
            'data': {'cities': [{'key': city.get('name'), 'value': city.get('id')} for city in cities]}
        })

    return HttpResponse(json.dumps(
        {
            'success': True,
            'errorCode': None,
            'errorMessage': None,
            'errorItems': [],
            'data': {'cities': cities},
        }
    )
    )


def generate_discount_code(request, vehicle_type):
    if request.method == 'POST':
        discountHash = request.POST.get('discountHash', None)
        emailId = request.POST.get('emailId', None)
        companyId = request.POST.get('companyId', None)

        # if not emailId:
        #     user = request.user
        #     if user and user.is_authenticated():
        #         emailId = user.email

        # Validity check code
        if discountHash and emailId and not companyId:
            if insurer_utils.check_for_discount_code_validity(emailId, discountHash):
                return HttpResponse(json.dumps({'success': True}))
            return HttpResponse(json.dumps({'success': False}))

        # Discount code generation
        if not emailId or not companyId:
            return HttpResponse(json.dumps({'success': False}))

        try:
            company = Company.objects.get(id=companyId)
            rexp = re.compile(company.domains)
            emailDomain = emailId.split('@')[1]
            if not rexp.match(emailDomain):
                return HttpResponse(json.dumps({'success': False, 'error': 'Invalid company email.'}))
            discount_code, created = motor_models.DiscountCode.objects.get_or_create(
                email=emailId, is_invalid=False)
        except Exception:
            utils.log_error(motor_logger, request=request)
            return HttpResponse(json.dumps({'success': False}))

        parameters = {
            'emailId': emailId,
            'code': discount_code.code,
            'company': company.name,
        }
        mail_utils.send_mail_for_discount_code(parameters)
        return HttpResponse(json.dumps({'success': True}))
    return HttpResponse(json.dumps({'success': True}))


@never_cache
@csrf_exempt
def payment_success(request, vehicle_type, transaction_id):
    transaction = get_object_or_404(motor_models.Transaction, transaction_id=transaction_id, status='COMPLETED')
    address_details = get_customer_address(transaction)

    # Reopen to requirement to previous state if deleted.
    transaction.open_requirement_if_deleted()
    transaction.track_activity(
        activity_type=core_activity.SUCCESS,
        page_id='payment',
    )

    return render(request, 'motor_product/payment_success.html', {
        'transaction': transaction,
        'tat': transaction.policy_tat_verbose,
        'vehicle_type': vehicle_type,
        'address_details': address_details
    })


@require_POST
def rsa_status(request):
    referer = request.META.get('HTTP_REFERER', '')
    # TODO: Use reverse if possible
    success_regex = re.compile(r'(?P<vehicle_type>[\w]+)/payment/success/(?P<transaction_id>[\w-]+)/$')
    search = success_regex.search(referer)
    if not search:
        return HttpResponseBadRequest()

    transaction_id = search.groupdict().get('transaction_id', '')
    transaction = get_object_or_404(motor_models.Transaction, transaction_id=transaction_id, status='COMPLETED')

    if transaction.vehicle_type != 'Private Car' or transaction.rsa_policy:
        status = 'complete'
    else:
        status = 'pending'

    return JsonResponse({'status': status,
                         'policy_doc': transaction.policy_url,
                         'rsa_doc': transaction.rsa_policy.url if transaction.rsa_policy else '',
                         'policy_tat': transaction.policy_tat_verbose})


# TODO: To be deleted, not in use any more
class InspectionCreateView(BaseMixin, CreateView):
    model = motor_models.Inspection
    fields = ['transaction', 'schedule_slot']
    http_method_names = ['post']

    def get_form_class(self):
        form_class = super(InspectionCreateView, self).get_form_class()
        form_class.base_fields['transaction'].to_field_name = 'transaction_id'
        return form_class

    def get_address(self, transaction):
        user_form_details = transaction.raw['user_form_details']
        address_keys = filter(lambda k: k.startswith('add_'), user_form_details.keys())
        address = []
        for key, val in user_form_details.items():
            if key in address_keys and val:
                address.append(
                    '%s: %s' % (title(key.replace('add_', '').replace('_', ' ')), val, )
                )
        return ', '.join(address)

    def form_invalid(self, form):
        utils.log_error(motor_logger, request=self.request)
        return self.render_json_response({
            'user_messages': self.dump_messages(form.errors, 'ERROR'),
        }, status=400)

    def form_valid(self, form):
        inspection = form.save(commit=False)

        # If no address is provided, we take the same address as given in the transaction
        if not inspection.address:
            inspection.address = self.get_address(inspection.transaction)

        inspection.insurer = inspection.transaction.insurer
        inspection.address = self.get_address(inspection.transaction)
        inspection.scheduled_on = datetime.datetime.today()
        form.save()

        # TODO: Check with Arpit/Deepak if this logic is fine.
        # This will change the transaction status if someone saves inspetion model
        # which might not be desired
        inspection.transaction.status = 'PENDING'
        inspection.transaction.save()

        # Following process should be done in a much more proper way
        request_data = parser_utils.qdict_to_dict(copy.deepcopy(self.request.POST))

        # Clean unicode characters
        for field, value in request_data.items():
            request_data[field] = re.sub(r'[^\x00-\x7F]+', '', value)

        prod_utils._save_transaction_details(inspection.transaction.quote.vehicle, request_data, inspection.transaction)

        LMSUtils.send_lms_event(
            event='inspection_created',
            product_type='motor',
            tracker=self.request.TRACKER,
            data={
                'proposal_form': reverse('desktopForm', args=(
                    'fourwheeler', inspection.transaction.insurer.slug, inspection.transaction.transaction_id, )
                ),
            },
            email=self.request.COOKIES.get('email') or inspection.transaction.raw.get('user_form_details', {}).get('cust_email', ''),
            mobile=self.request.COOKIES.get('mobileNo') or inspection.transaction.raw.get('user_form_details', {}).get('cust_phone', ''),
            internal=self.request.IS_INTERNAL,
            request_ip=putils.get_request_ip(self.request)
        )
        return self.render_json_response({'success': True})


@never_cache
@csrf_exempt
def offline_process_scheduled(request, vehicle_type, insurer_slug, transaction_id):
    transaction = get_object_or_404(motor_models.Transaction, transaction_id=transaction_id)
    transaction.status = 'IN_PROCESS'
    transaction.save()

    return render(request, 'motor_product/offline.html', {})


@csrf_exempt
def payment_failure(request, vehicle_type, transaction_id=None):
    transaction = get_object_or_404(motor_models.Transaction, transaction_id=transaction_id)
    transaction.track_activity(
        activity_type=core_activity.FAILED,
        page_id='payment',
    )
    return render(request, 'motor_product/payment_failure.html', {
        'transaction': transaction,
        'vehicle_type': vehicle_type
    })


@never_cache
@csrf_exempt
def product_failure(request, vehicle_type, transaction_id=None):
    if transaction_id:
        transaction = get_object_or_404(motor_models.Transaction, transaction_id=transaction_id)
        transaction.status = 'FAILED'
        transaction.save()

    return render(request, 'motor_product/failure.html', {})


@never_cache
@csrf_exempt
def payment_response(request, vehicle_type, insurer_slug, transaction_id=None):

    response = {}
    response['POST'] = dict([(k, v) for k, v in request.POST.items()])
    response['GET'] = dict([(k, v) for k, v in request.GET.items()])

    payment_logger.info(json.dumps({
        'type': 'response',
        'insurer': insurer_slug,
        'transaction': transaction_id,
        'data': response,
    }))

    try:
        is_redirect, redirect_url, template_name, data = prod_utils.process_payment_response(
            request, vehicle_type, insurer_slug, response, transaction_id=transaction_id)
    except:
        log_error(payment_logger, msg=u'Payment Response Failed')
        if vehicle_type == 'twowheeler':
            transaction = motor_models.Transaction.objects.get(transaction_id=transaction_id)
            message = errors.PAYMENT_RETRY['payment-failed']
            message['description'] = message['description'].format(
                **{
                    'customer_first_name': transaction.customer_name(),
                    'payment_amount': transaction.premium_paid
                }
            )
            return JsonResponse(message, status=400)

        return HttpResponseRedirect('/motor/' + vehicle_type + '/product/failure/')

    if is_redirect:
        return HttpResponseRedirect(redirect_url)
    else:
        return render(request, template_name, {
            'data': data,
            'redirect_url': redirect_url,
        })


@never_cache
def quote_share(request, quote_id):
    months = ["January", "February", "March", "April", "May", "June",
              "July", "August", "September", "October", "November", "December"]
    quote = get_object_or_404(motor_models.Quote, quote_id=quote_id)

    PARAMETER_LIST = ['isNewVehicle',
                      'vehicle', 'vehicleVariant', 'vehicleId', 'fuel_type',
                      'rto_info', 'reg_date', 'reg_month', 'reg_year',
                      'man_year', 'man_month',
                      'newPolicyStartDate', 'policy_start_date', 'policy_start_month', 'policy_start_year',
                      'pastPolicyExpiryDate', 'expiry_date', 'expiry_month', 'expiry_year', 'expirySlot',
                      'idv', 'previousNCB', 'isClaimedLastYear',
                      'isCNGFitted', 'cngKitValue', 'voluntaryDeductible', 'idvElectrical', 'idvNonElectrical',
                      'quoteId', 'extra_isLegalLiability', 'extra_paPassenger',
                      'user_dob_date', 'user_dob_month', 'user_dob_year', 'user_occupation',
                      'extra_isAntiTheftFitted', 'extra_isMemberOfAutoAssociation', 'extra_isTPPDDiscount',
                      'rds_id'
                      ]

    RAW_COOKIE_LIST = ['discountCode']

    for addon in motor_models.Addon.objects.all():
        PARAMETER_LIST.append('addon_' + addon.code)

    is_new_vehicle = True if quote.raw_data['isNewVehicle'] == '1' else False

    isNewVehicle = 'true' if is_new_vehicle else 'false'

    variant_name = quote.vehicle.variant + \
        ' (' + str(quote.vehicle.cc) + ' CC) '
    vehicleId = quote.vehicle.id
    vehicle = {'id': quote.vehicle.model.id,
               'name': quote.vehicle.make.name + ' ' + quote.vehicle.model.name}
    vehicleVariant = {'id': quote.vehicle.id, 'name': variant_name, 'fuel_type': quote.vehicle.fuel_type}

    fuel_type = quote.vehicle.fuel_type
    if quote.raw_data['isCNGFitted'] == '1':
        fuel_type = 'INTERNAL_LPG_CNG'
        if int(quote.raw_data['cngKitValue']) > 0:
            fuel_type = 'EXTERNAL_LPG_CNG'

    fuel_type_map = {
        'PETROL': {
            'id': 'PETROL',
            'name': 'Petrol',
            'code': 'petrol',
        },
        'DIESEL': {
            'id': 'DIESEL',
            'name': 'Diesel',
            'code': 'diesel',
        },
        'INTERNAL_LPG_CNG': {
            'id': 'PETROL',
            'name': 'CNG/LPG Company Fitted',
            'code': 'cng_lpg_internal',
        },
        'EXTERNAL_LPG_CNG': {
            'id': 'PETROL',
            'name': 'CNG/LPG Externally fitted',
            'code': 'cng_lpg_external',
        }
    }
    fuel_type = fuel_type_map[fuel_type]

    registrationDate = None
    registrationMonth = None
    registrationYear = None
    rtoInformation = None
    manufacturingMonth = None
    manufacturingYear = None

    policyStartDate = None
    policyStartMonth = None
    policyStartYear = str(datetime.datetime.now().year)

    expiryDate = None
    expiryMonth = None
    expiryYear = None

    if not is_new_vehicle:
        registrationDate = quote.raw_data['registrationDate'].split('-')[0]
        reg_month_index = int(
            quote.raw_data['registrationDate'].split('-')[1]) - 1
        registrationMonth = months[reg_month_index]
        registrationYear = quote.raw_data['registrationDate'].split('-')[2]

        expiryDate = quote.raw_data['pastPolicyExpiryDate'].split('-')[0]
        expiry_month_index = int(
            quote.raw_data['pastPolicyExpiryDate'].split('-')[1]) - 1
        expiryMonth = months[expiry_month_index]
        expiryYear = quote.raw_data['pastPolicyExpiryDate'].split('-')[2]

    rto_info = '-'.join(quote.raw_data['registrationNumber[]'][0:2])
    rto_name = ''
    try:
        rto_master = motor_models.RTOMaster.objects.get(rto_code=rto_info)
        rto_name = rto_info + ' - ' + rto_master.rto_name
    except:
        pass
    rto_info_dict = {'id': rto_info, 'name': rto_name}
    rtoInformation = rto_info_dict

    if is_new_vehicle:
        man_month_index = int(
            quote.raw_data['manufacturingDate'].split('-')[1]) - 1
        manufacturingMonth = months[man_month_index]
        manufacturingYear = quote.raw_data['manufacturingDate'].split('-')[2]

        policyStartDate = quote.raw_data['newPolicyStartDate'].split('-')[0]
        ps_month_index = int(
            quote.raw_data['newPolicyStartDate'].split('-')[1]) - 1
        policyStartMonth = months[ps_month_index]
        policyStartYear = quote.raw_data['newPolicyStartDate'].split('-')[2]

    discountCode = request.GET.get('dc', None)
    if not discountCode:
        discountCode = quote.raw_data.get('discountCode', 'null')

    cookie_parameters = {
        'discountCode': discountCode,
    }

    customerDobDate = None
    customerDobMonth = None
    customerDobYear = None

    customerOccupation = quote.raw_data.get('extra_user_occupation', None)
    if not customerOccupation:
        customerOccupation = None

    customerDob = quote.raw_data.get('extra_user_dob', None)
    if customerDob:
        customerDobDate = customerDob.split('-')[0]
        customerDobMonth = months[int(customerDob.split('-')[1]) - 1]
        customerDobYear = customerDob.split('-')[2]

    quote_parameters = {
        'isNewVehicle': isNewVehicle,
        'vehicleId': str(vehicleId),
        'vehicle': vehicle,
        'vehicleVariant': vehicleVariant,
        'fuel_type': fuel_type,
        'reg_date': registrationDate,
        'reg_month': registrationMonth,
        'reg_year': registrationYear,
        'expiry_date': expiryDate,
        'expiry_month': expiryMonth,
        'expiry_year': expiryYear,
        'rto_info': rtoInformation,
        'man_month': manufacturingMonth,
        'man_year': manufacturingYear,
        'policy_start_date': policyStartDate,
        'policy_start_month': policyStartMonth,
        'policy_start_year': policyStartYear,
        'idv': str(quote.raw_data['idv']),
        'idvElectrical': str(quote.raw_data['idvElectrical']),
        'idvNonElectrical': str(quote.raw_data['idvNonElectrical']),
        'voluntaryDeductible': str(quote.raw_data['voluntaryDeductible']),
        'previousNCB': str(quote.raw_data['previousNCB']),
        'isClaimedLastYear': str(quote.raw_data['isClaimedLastYear']),
        'isCNGFitted': str(quote.raw_data['isCNGFitted']),
        'cngKitValue': str(quote.raw_data['cngKitValue']),
        'user_dob_date': customerDobDate,
        'user_dob_month': customerDobMonth,
        'user_dob_year': customerDobYear,
        'user_occupation': customerOccupation,
        'extra_isAntiTheftFitted': str(quote.raw_data.get('extra_isAntiTheftFitted', '0')),
        'extra_isMemberOfAutoAssociation': str(quote.raw_data.get('extra_isMemberOfAutoAssociation', '0')),
        'extra_isTPPDDiscount': str(quote.raw_data.get('extra_isTPPDDiscount', '0')),
        'fastlane_success': str(quote.raw_data.get('fastlane_success', 'false')),
        'formatted_reg_number': str(quote.raw_data.get('formatted_reg_number', None)),
        'rds_id': quote.raw_data.get('rds_id')
    }

    legalLiability = request.GET.get('ll', None)

    quote_parameters['extra_isLegalLiability'] = '0'
    try:
        if int(legalLiability) == 1:
            quote_parameters['extra_isLegalLiability'] = '1'
    except:
        pass

    paPassenger = request.GET.get('pa', None)
    try:
        paPassenger_map = {
            0: '0',
            1: '10000',
            2: '50000',
            3: '100000',
            4: '200000',
        }
        quote_parameters['extra_paPassenger'] = paPassenger_map[
            int(paPassenger)]
    except:
        quote_parameters['extra_paPassenger'] = '0'

    addon_param = request.GET.get('addons', None)
    if addon_param:
        try:
            addon_param = int(addon_param)
            addons_in_order = [
                (0, 'isDepreciationWaiver'),
                (1, 'isInvoiceCover'),
                (2, 'is247RoadsideAssistance'),
                (3, 'isEngineProtector'),
                (4, 'isNcbProtection'),
            ]

            for index, code in addons_in_order:
                addon_key = 'addon_' + code
                if (addon_param / (2 ** index)) % 2 == 1:
                    quote_parameters[addon_key] = '1'
                else:
                    quote_parameters[addon_key] = '0'
        except:
            pass

    if quote.vehicle.vehicle_type == 'Private Car':
        vehicle_tag = 'fourwheeler'
        redirect_url = '/motor/car-insurance/#results'
    else:
        vehicle_tag = 'twowheeler'
        redirect_url = '/motor/twowheeler-insurance/#results'

    response = render(request, "motor_product/share.html", {'quote': json.dumps(
        quote_parameters), 'redirect_url': redirect_url, 'vehicle_type': vehicle_tag})

    max_age = 365 * 24 * 60 * 60
    expires = datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age)

    for cookie in RAW_COOKIE_LIST:
        response.set_cookie(cookie, 'null', expires=datetime.datetime.utcnow())
        value = urllib.quote(cookie_parameters[cookie], '')
        response.set_cookie(cookie, value, expires=expires)
    # last_transaction = motor_models.Transaction.objects.filter(
    #     quote=quote,
    #     raw__contains='"user_form_details"'
    # ).order_by('updated_on').last()
    # if last_transaction:
    #     request.session.update({'last_seen_transaction_id': last_transaction.transaction_id})
    #     request.session.modified = True
    return response


@never_cache
@csrf_exempt
def get_results_from_landing_page(request, vehicle_type=None):
    response = HttpResponse(json.dumps({'success': True}))

    if request.method == 'GET':
        return response
    post_qdict = copy.deepcopy(request.POST)
    post_parameters = parser_utils.qdict_to_dict(post_qdict)

    PARAMETER_LIST = ['isNewVehicle',
                      'vehicle', 'vehicleVariant', 'vehicleId', 'fuel_type',
                      'rto_info', 'reg_date', 'reg_month', 'reg_year',
                      'man_year', 'man_month',
                      'newPolicyStartDate', 'policy_start_date', 'policy_start_month', 'policy_start_year',
                      'pastPolicyExpiryDate', 'expiry_date', 'expiry_month', 'expiry_year', 'expirySlot',
                      'idv', 'previousNCB', 'isClaimedLastYear',
                      'isCNGFitted', 'cngKitValue', 'voluntaryDeductible', 'idvElectrical', 'idvNonElectrical',
                      'quoteId', 'isUsedVehicle'
                      ]

    RAW_COOKIE_LIST = ['mobileNo']

    months = ["January", "February", "March", "April", "May", "June",
              "July", "August", "September", "October", "November", "December"]

    registrationMonth = months[datetime.datetime.now().month - 1]
    manufacturingMonth = months[datetime.datetime.now().month - 1]
    manufacturingYear = str(datetime.datetime.now().year)
    policy_expired = True if post_parameters.get('isExpired', 'false') == 'true' else False
    if policy_expired:
        expiry_date = datetime.datetime.now() - datetime.timedelta(days=1)
    else:
        expiry_date = datetime.datetime.now() + datetime.timedelta(days=7)
    expiryDate = "%02d" % (expiry_date.day)
    expiryMonth = months[expiry_date.month - 1]
    expiryYear = str(expiry_date.year)

    DEFAULT_PARAMETER_MAP = {
        'idv': '0',
        'previousNCB': None,
        'isClaimedLastYear': '0',
        'voluntaryDeductible': '0',
        'idvElectrical': '0',
        'idvNonElectrical': '0',
        'reg_month': registrationMonth,
        'reg_date': '01',
        'man_month': manufacturingMonth,
        'man_year': manufacturingYear,
        'expiry_date': expiryDate,
        'expiry_month': expiryMonth,
        'expiry_year': expiryYear,
        'expirySlot': '7',
    }

    if request.method == 'POST':
        max_age = 365 * 24 * 60 * 60
        expires = datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age)


        motor_logger.info(post_parameters)

        quote_parameters = DEFAULT_PARAMETER_MAP
        for parameter, value in post_parameters.items():
            if parameter in PARAMETER_LIST:
                try:
                    quote_parameters[parameter] = json.loads(value)
                except:
                    quote_parameters[parameter] = value

        response = HttpResponse(json.dumps(
            {'success': True, 'data': quote_parameters}))

        max_age = 365 * 24 * 60 * 60
        expires = datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age)
        if vehicle_type:
            redirect_url = '/motor/car-insurance/'
            if vehicle_type == 'twowheeler':
                redirect_url = '/motor/twowheeler-insurance/'

            tparty = request.GET.get('tparty', None)
            for key, value in request.GET.items():
                if "?" in redirect_url:
                    redirect_url += "{0}={1}&".format(key, value)
                else:
                    redirect_url += "?{0}={1}&".format(key, value)

            try:
                int(quote_parameters['vehicleVariant']['id'])
                redirect_url += '#results'
            except:
                pass

            response = render(request, "motor_product/share.html", {'quote': json.dumps(
                quote_parameters), 'redirect_url': redirect_url, 'vehicle_type': vehicle_type})
            # TODO: Make a generic solution to set campaign for tparty redirects.
            if tparty.lower() == 'cartrade':
                response.set_cookie('lms_campaign', 'motor-cartrade', expires=expires)

        for cookie in RAW_COOKIE_LIST:
            response.set_cookie(cookie, 'null', expires=datetime.datetime.utcnow())
            if post_parameters.get(cookie, None):
                value = urllib.quote(post_parameters[cookie], '')
                response.set_cookie(cookie, value, expires=expires)

        try:
            user_data = json.loads(request.POST['user_data'])
        except:
            pass
        else:
            for key, value in user_data.items():
                response.set_cookie(key, value, expires=expires)

    return response


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def download_scheduled_inspection_data(request):
    inspections = motor_models.Inspection.objects.all()
    order = [
        'Phone',
        'E-mail',
        'Name',
        'Insurer',
        'Make',
        'Model',
        'Variant',
        'Registration Number',
        'Registration Date',
        'Idv',
        'Idv Electrical',
        'Idv Non-electrical',
        'CNG Fitted',
        'CNG Kit Value',
        'Claimed Last Year',
        'Previous NCB',
        'Voluntary Deductible',
        'PYP Expiry Date',
        'Manufacturing Date',
        'Requested on (Date)',
        'Requested on (Time)',
        'Address',
        'TransactionId',
        'Transaction Link',
    ]
    rows = []
    for inspection in inspections:
        try:
            transaction = inspection.transaction
            customer = transaction.proposer
            insurer = inspection.insurer
            quote_parameters = transaction.quote.raw_data
            quote_response = transaction.raw['quote_response']
            registration_no = "-".join(
                quote_parameters['registrationNumber[]'])
            vehicle_type = VEHICLE_TYPE_TO_URL_TAG_MAP[
                transaction.vehicle_type]
            link = xlwt.Formula('HYPERLINK("' + settings.SITE_URL.strip("/") + '/motor/' + vehicle_type +
                                '/' + insurer.slug + '/desktopForm/' + transaction.transaction_id + '","transaction")')
            row = [
                customer.mobile,
                customer.email,
                customer.first_name + ' ' + customer.last_name,
                insurer.slug,
                transaction.quote.vehicle.make.name,
                transaction.quote.vehicle.model.name,
                transaction.quote.vehicle.variant,
                registration_no,
                quote_parameters['registrationDate'],
                quote_response['calculatedAtIDV'],
                quote_parameters['idvElectrical'],
                quote_parameters['idvNonElectrical'],
                quote_parameters['isCNGFitted'],
                quote_parameters['cngKitValue'],
                quote_parameters['isClaimedLastYear'],
                quote_parameters['previousNCB'],
                quote_parameters['voluntaryDeductible'],
                quote_parameters.get('pastPolicyExpiryDate', '-'),
                quote_parameters['manufacturingDate'],
                inspection.scheduled_on.astimezone(
                    timezone.get_current_timezone()).strftime('%d-%m-%Y'),
                inspection.scheduled_on.astimezone(
                    timezone.get_current_timezone()).strftime('%H:%M'),
                inspection.address,
                transaction.transaction_id,
                link,
            ]
        except:
            # Append - to error rows, the length of which is equal to the
            # length of the header row
            row = ['-'] * len(order)
        rows.append(row)

    file_name = 'Inspection_Data_%s.xls' % datetime.datetime.now().strftime("%d-%b_%H%M")
    return putils.render_excel(file_name, order, rows)


class LntProposalService(ServiceBase):

    @rpc(landt_utils.Transaction, _returns=landt_utils.TransactionResponse)
    def LntProposalRequest(ctx, Transaction):
        tr = landt_utils.TransactionResponse()
        resp = prod_utils.process_landt_policy_response(Transaction)

        tr_dict = {
            'return': resp
        }
        for k, v in tr_dict.items():
            setattr(tr, k, v)

        return tr

landt_policy_response_service = csrf_exempt(DjangoView.as_view(
    services=[LntProposalService], tns='spyne.examples.django',
    in_protocol=Soap11(validator='lxml'), out_protocol=Soap11()))


@never_cache
@csrf_exempt
def iffco_policy_response(request, transaction_id=None):
    motor_logger.info(request)

    vehicle_type = 'fourwheeler'
    response = request.GET.get('ITGIResponse', None)
    if response and response.split('|')[0] == 'TWP':
        vehicle_type = 'twowheeler'
    return payment_response(request, vehicle_type, 'iffco-tokio', transaction_id=None)


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def download_transaction_details(request, data_type):
    month = request.GET.get('month', None)
    year = request.GET.get('year', None)

    file_title = 'Transaction_Data'
    if data_type == 'offline':
        # Offline insurers hardcoded for now
        file_title = 'Offline_Data'
        query = Q(insurer__slug='new-india') & Q(status='IN_PROCESS')
        transactions = motor_models.Transaction.objects.filter(
            query).order_by('-created_on')
    elif data_type == 'success':
        file_title = 'Success_Data'
        transactions = motor_models.Transaction.objects.filter(
            Q(status='COMPLETED') | Q(status='MANUAL COMPLETED')).order_by('-created_on')
    elif data_type == 'pending':
        file_title = 'Pending_Data'
        transactions = motor_models.Transaction.objects.filter(
            status='PENDING').order_by('-created_on')
    elif data_type == 'failed':
        file_title = 'Failed_Data'
        transactions = motor_models.Transaction.objects.filter(
            status='FAILED').order_by('-created_on')
    elif data_type == 'all':
        transactions = motor_models.Transaction.objects.all().order_by('-created_on')
    else:
        return HttpResponseBadRequest("Invalid Request")

    if not month:
        month = datetime.datetime.now().month
    if not year:
        year = datetime.datetime.now().year

    start_date = datetime.datetime.strptime('01-%s-%s' % (month, year), '%d-%m-%Y')
    end_date = start_date + relativedelta.relativedelta(months=1)
    transactions = transactions.filter(
        created_on__gte=start_date, created_on__lt=end_date)

    clean_row = [("", "")]
    rows = []
    for transaction in transactions:
        try:
            customer = transaction.proposer
            insurer = transaction.insurer
            quote_parameters = transaction.quote.raw_data
            user_form_details = transaction.raw.get('user_form_details', {})
            rto_code = "-".join(quote_parameters['registrationNumber[]'][:2])
            vehicle_type_code = VEHICLE_TYPE_TO_URL_TAG_MAP[
                transaction.vehicle_type]
            premium_shown = sum([float(item['premium']) for i in ['addonCovers', 'basicCovers', 'discounts']
                                 for item in transaction.raw['quote_response']['premiumBreakup'][i]])

            row = [
                ('CFOX Id', '%s' % transaction.id),
                ('Customer Name', customer.first_name + ' ' +
                 customer.last_name if customer else '-'),
                ('Customer Mobile', customer.mobile if customer else '-'),
                ('Customer Email', customer.email if customer else '-'),
                ('Proposer First Name', user_form_details.get('cust_first_name', '')),
                ('Proposer Last Name', user_form_details.get('cust_last_name', '')),
                ('Proposer Phone', user_form_details.get('phone', '')),
                ('Proposer Email', user_form_details.get('cust_email', '')),
                ('Proposer State', user_form_details.get('add_state', '')),
                ('Proposer Pincode', user_form_details.get('add_pincode', '')),
                ('Proposer DOB', user_form_details.get('cust_dob', '')),
                ('Proposer Gender', user_form_details.get('cust_gender', '')),
                ('Insurer', insurer.slug),
                ('Make', transaction.quote.vehicle.make.name),
                ('Model', transaction.quote.vehicle.model.name),
                ('Variant', transaction.quote.vehicle.variant),
                ('Registration Number', user_form_details.get('vehicle_reg_no', '')),
                ('Registration RTO',
                 "-".join(quote_parameters['registrationNumber[]'])),
                ('RTO City', motor_models.RTOMaster.objects.get(rto_code=rto_code).rto_name),
                ('Registration Date', quote_parameters['registrationDate']),
                ('Claimed Last Year', quote_parameters['isClaimedLastYear']),
                ('Previous NCB', quote_parameters['previousNCB']),
                ('Past Policy Expiry Date',
                 quote_parameters['pastPolicyExpiryDate']),
                ('Manufacturing Date', quote_parameters['manufacturingDate']),
                ('Engine Number', transaction.raw.get(
                    'user_form_details', {}).get('vehicle_engine_no', '')),
                ('Policy Number', transaction.policy_number),
                ('Proposal Number', transaction.proposal_number),
                ('Policy Start Date', transaction.policy_start_date),
                ('Policy End Date', transaction.policy_end_date),
                ('Premium Paid', transaction.premium_paid),
                ('Premium Shown(temporary value)', premium_shown),
                ('Transaction Status', transaction.status),
                ('Payment Done', transaction.payment_done),
                ('Created On', transaction.created_on),
                ('Last Update On', transaction.updated_on),
                ('Vehicle Type', transaction.vehicle_type),
                ('Transaction ID', transaction.transaction_id),
                ('Link', "%s/motor/%s/%s/desktopForm/%s" % (settings.SITE_URL.strip("/"),
                                                            vehicle_type_code, insurer.slug, transaction.transaction_id)),
            ]
            clean_row = row
        except:
            stacktrace = traceback.format_exc()
            # motor_logger.info(stacktrace)
            row = [("", stacktrace.strip())]
        rows.append([i[1] for i in row])

    file_name = '%s_%s.xls' % (
        file_title, datetime.datetime.now().strftime("%d-%b_%H%M"))
    return putils.render_excel(file_name, [i[0] for i in clean_row], rows)


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def download_quotes_data(request):
    quotes = motor_models.Quote.objects.all()
    order = [
        'Business Type',
        'Make',
        'Model',
        'Variant',
        'Registration Number',
        'Registration Date',
        'Manufacturing Date',
        'PYP Expiry Date',
        'Claimed Last Year',
        'Previous NCB',
        'Voluntary Deductible',
        'Created On',
        'Engine Nos',
        'Chassis Nos',
    ]
    rows = []
    for quote in quotes:
        try:
            quote_parameters = quote.raw_data
            btype = 'Old'
            if quote_parameters['isNewVehicle'] == '1':
                btype = 'New'

            created_time_str = '-'
            try:
                timezn = timezone.get_current_timezone()
                created_time = quote.created_on.astimezone(timezn)
                created_time_str = created_time.strftime('%d-%m-%Y %H:%M')
            except:
                pass

            engine_nos = ''
            chassis_nos = ''
            for transaction in quote.transaction_set.all():
                user_form_details = transaction.raw.get(
                    'user_form_details', {})
                engine_nos += user_form_details.get(
                    'vehicle_engine_no', '') + ' '
                chassis_nos += user_form_details.get(
                    'vehicle_chassis_no', '') + ' '

            registration_no = "-".join(
                quote_parameters['registrationNumber[]'])
            row = [
                btype,
                quote.vehicle.make.name,
                quote.vehicle.model.name,
                quote.vehicle.variant,
                registration_no,
                quote_parameters['registrationDate'],
                quote_parameters['manufacturingDate'],
                quote_parameters['pastPolicyExpiryDate'],
                quote_parameters['isClaimedLastYear'],
                quote_parameters['previousNCB'],
                quote_parameters['voluntaryDeductible'],
                created_time_str,
                engine_nos,
                chassis_nos,
            ]
        except:
            stacktrace = traceback.format_exc()
            motor_logger.info(stacktrace)
            row = ['-'] * len(order)
        rows.append(row)

    file_name = 'Quotes_Data_%s.xls' % datetime.datetime.now().strftime("%d-%b_%H%M")
    return putils.render_excel(file_name, order, rows)


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def download_landt_transaction_details(request, t_id):
    transaction = None
    try:
        transaction = motor_models.Transaction.objects.get(id=t_id)
    except:
        pass

    if not transaction:
        try:
            transaction = motor_models.Transaction.objects.get(transaction_id=t_id)
        except:
            pass

    if not transaction or not(transaction.insurer.slug == 'l-t'):
        raise Http404

    try:
        filename, head_keys, list_of_values, proposal_number = get_landt_user_data.details(
            transaction)
        return putils.render_excel(filename, head_keys, list_of_values)
    except:
        stacktrace = traceback.format_exc()
        return HttpResponse(json.dumps({'error': stacktrace}))


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def get_transaction_details(request, transaction_id):

    transaction = None
    try:
        transaction = motor_models.Transaction.objects.get(id=transaction_id)
    except:
        pass

    if not transaction:
        try:
            transaction = motor_models.Transaction.objects.get(
                transaction_id=transaction_id)
        except:
            pass

    if not transaction:
        raise Http404

    transaction_main_details = {
        'premium_paid': transaction.premium_paid,
        'proposal_number': transaction.proposal_number,
        'reference_id': transaction.reference_id,
        'status': transaction.status,
        'policy_url': transaction.policy_document.url if transaction.policy_document else ""
    }

    transaction_details = transaction.raw
    payment_response = transaction.insured_details_json
    quote_details = transaction.quote.raw_data if transaction.quote else {}

    vehicle = transaction.quote.vehicle
    vehicle_details = {
        'master': {
            'make': vehicle.make.name,
            'model': vehicle.model.name,
            'variant': vehicle.variant,
        }
    }

    try:
        insurer_vehicle = vehicle.sub_vehicles.get(insurer=transaction.insurer)
        insurer_vehicle_details = {
            'make': insurer_vehicle.make,
            'model': insurer_vehicle.model,
            'variant': insurer_vehicle.variant,
        }
        vehicle_details['insurer'] = insurer_vehicle_details
    except:
        pass

    return HttpResponse(json.dumps({'transaction_details': transaction_details, 'quote_details': quote_details, 'payment_response': payment_response, 'vehicle_details': vehicle_details, 'other_transaction_details': transaction_main_details}), content_type='application/json')


@never_cache
@csrf_exempt
def lnt_offline_closure(request):
    if request.method == "POST":
        error, errormsg, filename, head_keys, row_values = get_landt_user_data.offline_closure(
            request.POST)
        if error:
            return render_to_response("motor_product/lnt_offline.html", {'error': error, 'errmsg': errormsg})
        else:
            return putils.render_excel(filename, head_keys, [row_values, ])
    else:
        return render_to_response("motor_product/lnt_offline.html", {})


@never_cache
@csrf_exempt
def liberty_offline_closure(request):
    if request.method == 'POST':
        post_data = request.POST
        context_dict = {}
        context_dict['proposal_for'] = post_data.get('proposal_for')
        context_dict['salutation'] = post_data.get('salutation')
        context_dict['name'] = post_data.get('name')
        context_dict['isMarried'] = post_data.get('isMarried')
        context_dict['gender'] = post_data.get('gender')
        context_dict['dob'] = list(('').join(post_data.get('dob').split('-')))
        context_dict['mobile'] = post_data.get('mobile')
        context_dict['email'] = post_data.get('email')
        context_dict['nominee'] = post_data.get('nominee')
        context_dict['age'] = post_data.get('age')
        context_dict['relation'] = post_data.get('relation')
        context_dict['contact'] = post_data.get('contact')
        context_dict['profession'] = post_data.get('profession')
        context_dict['pan'] = post_data.get('pan')
        context_dict['comm_addr_line1'] = post_data.get('comm_addr_line1')
        context_dict['comm_addr_line2'] = post_data.get('comm_addr_line2')
        context_dict['comm_addr_city'] = post_data.get('comm_addr_city')
        context_dict['comm_addr_pincode'] = post_data.get('comm_addr_pincode')
        context_dict['comm_addr_state'] = post_data.get('comm_addr_state')

        if int(post_data.get('same_contact_reg_add')):
            context_dict['reg_addr_line1'] = post_data.get('comm_addr_line1')
            context_dict['reg_addr_line2'] = post_data.get('comm_addr_line2')
            context_dict['reg_addr_city'] = post_data.get('comm_addr_city')
            context_dict['reg_addr_pincode'] = post_data.get('comm_addr_pincode')
            context_dict['reg_addr_state'] = post_data.get('comm_addr_state')
        else:
            context_dict['reg_addr_line1'] = post_data.get('reg_addr_line1')
            context_dict['reg_addr_line2'] = post_data.get('reg_addr_line2')
            context_dict['reg_addr_city'] = post_data.get('reg_addr_city')
            context_dict['reg_addr_pincode'] = post_data.get('reg_addr_pincode')
            context_dict['reg_addr_state'] = post_data.get('reg_addr_state')

        context_dict['make'] = post_data.get('make')
        context_dict['model'] = post_data.get('model')
        context_dict['variant'] = post_data.get('variant')
        context_dict['idv'] = post_data.get('idv')
        context_dict['premium'] = post_data.get('premium')
        context_dict['reg_no'] = post_data.get('reg_no')
        context_dict['reg_city'] = post_data.get('reg_city')
        context_dict['fuel_type'] = post_data.get('fuel_type')

        if post_data.get('fuel_type') not in ['Petrol', 'Diesel']:
            context_dict['non_conventional'] = '1'
        else:
            context_dict['non_conventional'] = '0'

        context_dict['cc'] = post_data.get('cc')
        context_dict['seating_capacity'] = post_data.get('seating_capacity')
        context_dict['ncb'] = post_data.get('ncb')
        context_dict['engine_no'] = post_data.get('engine_no')
        context_dict['chassis_no'] = post_data.get('chassis_no')
        context_dict['expiring_policy_no'] = post_data.get('expiring_policy_no')
        context_dict['expiring_policy_date'] = list(('').join(post_data.get('expiring_policy_date').split('-')))
        context_dict['is_claimed'] = post_data.get('is_claimed')
        if int(post_data.get('is_claimed')):
            context_dict['no_of_claim'] = post_data.get('no_of_claim')
            context_dict['claim_amount'] = post_data.get('claim_amount')
            context_dict['claim_year'] = str(datetime.date.today().year - 1) + ' - ' + str(datetime.date.today().year)
        context_dict['financier'] = post_data.get('financier')
        context_dict['financier_city'] = post_data.get('financier_city')
        context_dict['voluntary'] = post_data.get('voluntary')
        context_dict['insurer_plan_name'] = post_data.get('insurer_plan_name')
        context_dict['reg_date'] = list(('').join(post_data.get('reg_date').split('-')))
        context_dict['manufact_month'] = post_data.get('manufact_month')
        context_dict['manufact_year'] = post_data.get('manufact_year')
        context_dict['accessories'] = post_data.get('accessories')
        context_dict['naccessories'] = post_data.get('naccessories')
        context_dict['kit_value'] = post_data.get('kit_value')
        context_dict['pa_owner_driver'] = post_data.get('pa_owner_driver')
        context_dict['pa_passenger'] = post_data.get('pa_passenger')
        context_dict['pa_cover'] = post_data.get('pa_cover')
        if int(post_data.get('pa_cover')):
            context_dict['no_of_passenger'] = post_data.get('no_of_passenger')
            context_dict['pass_sum_insured'] = post_data.get('pass_sum_insured')

        context_dict['ll_driver'] = post_data.get('ll_driver')
        context_dict['anti_theft_fitted'] = post_data.get('anti_theft_fitted')
        context_dict['is_AAI'] = post_data.get('is_AAI')
        context_dict['paid_driver'] = post_data.get('paid_driver')
        context_dict['prev_insurer_name'] = post_data.get('prev_insurer_name')
        total_accessories = int(context_dict['accessories']) + int(context_dict['naccessories'])
        total_idv = total_accessories + int(context_dict['idv']) + int(context_dict['kit_value'])
        context_dict['total_idv'] = total_idv
        context_dict['policy_start_date'] = list(datetime.date.today().strftime('%d%m%Y'))
        context_dict['policy_end_date'] = list((datetime.date.today() + datetime.timedelta(days=365)).strftime('%d%m%Y'))

        expiry_date = datetime.datetime.strptime(post_data.get('expiring_policy_date'), '%d-%m-%Y')
        expiry_date = expiry_date - datetime.timedelta(days=364)
        context_dict['prev_pol_start_date'] = list(expiry_date.strftime('%d%m%Y'))

        return render_to_response('motor_product/liberty_proposal.html', context_dict)

    return render_to_response("motor_product/liberty-formfields.html", {})


@never_cache
@utils.profile_type_only('ADMIN', 'REPORT')
def fg_proposal_xml(request, transaction_id):
    transaction = None
    try:
        transaction = motor_models.Transaction.objects.get(id=transaction_id)
    except:
        pass

    if not transaction:
        try:
            transaction = motor_models.Transaction.objects.get(
                transaction_id=transaction_id)
        except:
            pass

    if not transaction:
        raise Http404

    request_data = fg_xml(transaction)
    if request_data is None:
        raise Http404

    return HttpResponse(request_data, content_type='text/xml')


class InsurerCityGaragesCount(TemplateView):
    template_name = 'motor_product/motor_garages/state_list.html'

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        insurer_slug = self.kwargs.get('insurer_slug')
        try:
            insurer = BrandPage.objects.get(
                slug=insurer_slug, _motor_insurer__isnull=False, type='motor')._motor_insurer
        except wiki_models.Insurer.DoesNotExist:
            insurer = motor_models.Insurer.objects.get(slug=insurer_slug)
        garages = motor_models.Garage.objects.filter(insurers=insurer).values(
            'city__name', 'city__state__name', 'city__slug', 'city__state__slug').annotate(Count('city')).order_by('city__name')
        states = {}
        for garage in garages:
            state = garage.pop('city__state__name')
            states[state] = states.get(state, [])
            states[state].append(garage)
        states = states.items()
        states.sort()
        context['insurer'], context['insurer_slug'], context[
            'states'] = insurer, insurer_slug, states
        return context


class CityGarageList(ListView):
    model = motor_models.Garage

    def get_queryset(self):
        queryset = super(self.__class__, self).get_queryset()
        self.city = self.kwargs.get('city')
        self.state = self.kwargs.get('state')
        self.insurer_slug = self.kwargs.get('insurer_slug')
        try:
            self.brandpage = BrandPage.objects.get(
                slug=self.insurer_slug, type='motor', _motor_insurer__isnull=False)
            self.insurer = self.brandpage._motor_insurer
        except BrandPage.DoesNotExist:
            raise Http404

        if not queryset.exists():
            raise Http404

        return queryset.filter(city__slug=self.city, city__state__slug=self.state, insurers=self.insurer)

    def get_context_data(self, *args, **kwargs):
        context = super(self.__class__, self).get_context_data(*args, **kwargs)
        context['insurer_title'] = self.insurer.title
        context['insurer_short_title'] = self.insurer.title.split(" General Insurance")[0]

        try:
            context['state'] = wiki_models.State.objects.get(slug=self.state)
            context['city'] = context['state'].city_set.get(slug=self.city)
        except (wiki_models.State.DoesNotExist, wiki_models.City.DoesNotExist):
            raise Http404
        context['brand'] = self.brandpage
        context['insurer_slug'] = self.insurer_slug
        return context


# For Cash Less Garages - INFO
def insurer_city_garages_count(request):
    if request.method == 'POST' and request.is_ajax():
        data = request.POST

        insurer_id = data.get('insurer_id')
        if not insurer_id:
            return HttpResponse(json.dumps({'message': 'Invalid request'}),
                                status=400, content_type='application/json')
        else:
            insurer = motor_models.Insurer.objects.get(id=insurer_id)

        state = data.get('state')
        if not state:
            states = list(motor_models.TempGarage.objects.filter(
                insurers=insurer
            ).values_list('wikicity__state__name', flat=True).distinct())
            states.sort()
            return HttpResponse(json.dumps({'insurer_id': insurer.id, 'states': states}),
                                content_type='application/json')

        city = data.get('city')
        if not city:
            cities = list(motor_models.TempGarage.objects.filter(
                insurers=insurer, wikicity__state__name=state
            ).values_list('wikicity__name', flat=True).distinct())
            cities.sort()
            return HttpResponse(json.dumps({'insurer_id': insurer.id, 'state': state, 'cities': cities}),
                                content_type='application/json')

        make = data.get('make')
        if not make:
            makes = list(motor_models.TempGarage.objects.filter(
                insurers=insurer, wikicity__state__name=state, wikicity__name=city
            ).values_list('make', flat=True).distinct())
            makes.sort()
            return HttpResponse(json.dumps({'insurer_id': insurer.id, 'state': state, 'city': city, 'makes': makes}),
                                content_type='application/json')

        _garages = motor_models.TempGarage.objects.filter(
            insurers=insurer, wikicity__state__name=state, wikicity__name=city, make=make
        )
        garages = [{'name': garage.name,
                    'address': garage.address,
                    'make': garage.make} for garage in _garages]
        return HttpResponse(json.dumps({'insurer_id': insurer.id, 'state': state, 'city': city, 'garages': garages}),
                            content_type='application/json')

    return flatpage_view(request)


def vehicle_info(request, vehicle_type):
    registration_number = request.POST['registration_number'].lower()
    registration_number = ''.join(
        filter(bool, re.findall('[a-z0-9]+', registration_number))
    )
    detail, should_fetch = motor_models.NewRegistrationDetail.objects.get_or_create(
        registration_number=registration_number
    )
    if should_fetch:
        detail.data = utils.fastlane(registration_number)
        detail.save()
    result = detail.data

    return HttpResponse(json.dumps({
        'success': True,
        'errorCode': None,
        'errorMessage': None,
        'errorItems': [],
        'data': result,
    }))


def inspection_form(request, transaction_id):
    transaction = get_object_or_404(motor_models.Transaction, transaction_id=transaction_id)
    message = None
    success = True

    inspection = transaction.inspection_set.filter(~Q(status__in=[motor_models.Inspection.CANCELLED,
                                                                  motor_models.Inspection.RESCHEDULED])).order_by('-scheduled_on')
    if inspection:
        success = False
        inspection = inspection[0]
        if inspection.status == motor_models.Inspection.PENDING:
            if inspection.scheduled_on:
                message = 'Inspection has been scheduled for %s at %s-%s' % (
                    inspection.scheduled_on.strftime("%d/%m/%Y"),
                    inspection.schedule_slot.start.strftime("%I:%M %p"),
                    inspection.schedule_slot.end.strftime("%I:%M %p"),
                )
            else:
                message = "Our inspection team will get in touch with you shortly."
        if inspection.status == motor_models.Inspection.PAYMENT_DONE:
            message = 'Inspection has been done and we are processing your policy request.'
        if inspection.status in [motor_models.Inspection.DOCUMENT_UPLOADED, motor_models.Inspection.DOCUMENT_APPROVED]:
            message = 'Your inspection is underway.'
        if inspection.status == motor_models.Inspection.PAYMENT_APPROVED:
            message = 'You request for policy has been approved.'
        if inspection.status in [motor_models.Inspection.DOCUMENT_REJECTED, motor_models.Inspection.PAYMENT_REJECTED]:
            message = 'You request for policy has been rejected.'
        message += " Please call %s for support." % settings.CAR_TOLL_FREE
    agent_areas = motor_models.InspectionArea.objects.filter(
        inspectionagent__isnull=False, is_enabled=True
    ).distinct()
    cities = {}
    for area in agent_areas:
        city = area.city
        if city.id not in cities:
            cities[city.id] = {'name': city.name, 'id': city.id, 'areas': [area]}
        else:
            cities[city.id]['areas'].append(area)
    return render(request, "motor_product/inspection_form.html", {
        'success': success,
        'message': message,
        'transaction': transaction,
        'agent_cities': cities,
        'address': ", ".join([
            transaction.raw['user_form_details'].get(i, '')
            for i in [
                'add_house_no',
                'add_building_name',
                'add_street_name',
                'add_landmark',
                'add_pincode'
            ]
        ])
    })


@csrf_exempt
@never_cache
def get_available_slots(request):
    if request.GET:
        inspection_form = motor_forms.MotorInspectionForm(request, request.GET)
        if inspection_form.is_valid():
            return JsonResponse({
                'success': True,
                'message': inspection_form.get()
            })
        else:
            return JsonResponse({
                'success': False,
                'errors': inspection_form.errors
            })

    if request.POST:
        inspection_form = motor_forms.MotorInspectionForm(request, request.POST)
        if inspection_form.is_valid():
            inspection, message = inspection_form.save(request)

            LMSUtils.send_lms_event(
                event='inspection_created',
                product_type='motor',
                tracker=request.TRACKER,
                data={
                    'proposal_form': reverse('desktopForm', args=(
                        'fourwheeler', inspection.transaction.insurer.slug, inspection.transaction.transaction_id,)
                    ),
                },
                email=inspection.transaction.raw.get('user_form_details', {}).get('cust_email', ''),
                mobile=inspection.transaction.raw.get('user_form_details', {}).get('cust_phone', ''),
                internal=request.IS_INTERNAL,
                request_ip=putils.get_request_ip(request)
            )
            try:
                notify_user(inspection.agent.user, {
                    "message": "New Inspection assigned to you",
                    "data": {
                        "status": inspection.status,
                        "city": inspection.city.name,
                        "scheduled_on": inspection.scheduled_datetime(),
                        "inspection_id": str(inspection.uuid),
                        "customer_name": inspection.transaction.customer_name(),
                        "customer_phone": inspection.transaction.customer_phone_number(),
                        "address": inspection.address,
                        "vehicle_number": inspection.transaction.vehicle_registration_number(),
                        "vehicle_info": str(inspection.transaction.quote.vehicle),
                    }

                })
            except Exception, e:
                # TODO: Send mail on error or notify_user inside a celery task
                motor_logger.info("GCM notification failed, not critical")

            return JsonResponse({
                'success': True,
                'message': message
            })
        else:
            return JsonResponse({
                'success': False,
                'errors': inspection_form.errors
            })


@permission_required('motor_product.change_inspection')
def inspection_details(request, inspection_id):
    if request.method == 'POST' and 'delete_image' in request.POST:
        document = get_object_or_404(motor_models.MotorInspectionDocument, id=request.POST['delete_image_id'])
        document.approval_status = motor_models.MotorInspectionDocument.APPROVAL_REJECTED
        document.save()
    inspection = get_object_or_404(motor_models.Inspection, uuid=inspection_id)
    document_filter = [
        motor_models.MotorInspectionDocument.APPROVED,
        motor_models.MotorInspectionDocument.APPROVAL_PENDING]
    documents = inspection.inspection_documents.filter(
        approval_status__in=document_filter)
    return render(request, "motor_product/inspection_documents.html", {
        'inspection': inspection,
        'documents': documents,
    })



@permission_required('motor_product.change_inspection')
def inspection_approval(request, inspection_id, status):
    inspection = motor_models.Inspection.objects.get(uuid=inspection_id)
    if status in ['cancel', 'reschedule']:
        if request.method == 'GET':
            if status == 'reschedule':
                form = motor_forms.InspectionRescheduleForm()
            else:
                form = motor_forms.InspectionCancelForm()

            return render(request, 'motor_product/inspection_cancel.html', {
                'form': form,
                'inspection': inspection,
                'status': status
            })

    status_dict = {
        'approve-document': motor_models.Inspection.DOCUMENT_APPROVED,
        'reject-document': motor_models.Inspection.DOCUMENT_REJECTED,
        'cancel': motor_models.Inspection.CANCELLED,
        'reschedule': motor_models.Inspection.RESCHEDULED,
        'approve-payment': motor_models.Inspection.PAYMENT_APPROVED,
        'reject-payment': motor_models.Inspection.PAYMENT_REJECTED,
    }
    inspection.status = status_dict[status]
    if request.method == 'POST' and 'reason' in request.POST:
        inspection.save(user=request.user, reason=request.POST.get("reason"),
                        comment=request.POST.get("comment", ""))
    else:
        inspection.save(user=request.user)
    try:
        notify_user(inspection.agent.user, {
            "message": "Your Pending Inspection has been %sed" % status_dict[status].lower(),
            'id': str(inspection.uuid)
        })
    except:
        # TODO: Send mail on error or notify_user inside a celery task
        pass

    if status == 'reschedule':
        return HttpResponseRedirect(reverse('inspection_form', args=(inspection.transaction.transaction_id, )))
    elif inspection.status == motor_models.Inspection.PAYMENT_APPROVED:
        return HttpResponseRedirect(reverse('complete_inspection', args=(inspection.transaction.transaction_id, )))
    else:
        return HttpResponseRedirect(reverse('approve_inspection'))


@never_cache
@permission_required('motor_product.change_pendinginspection', raise_exception=True)
def agent_inspection(request):
    q = Q(agent=request.user.inspectionagent_set.get(), scheduled_on=datetime.datetime.today())
    data = []

    # by default show all case which I need to attend today
    pending = q & Q(status=motor_models.Inspection.PENDING)
    data.append(['Pending Inspections', motor_models.Inspection.objects.filter(pending).order_by('scheduled_on')])

    # Get all inspection for which document has been approved, so
    # that payment can be done.
    document_approved = q & Q(status=motor_models.Inspection.DOCUMENT_APPROVED)
    data.append(['Documents Approved', motor_models.Inspection.objects.filter(document_approved).order_by('scheduled_on')])

    # Get all inspection for which documents have been uploaded
    payment_approved = q & Q(status=motor_models.Inspection.DOCUMENT_UPLOADED)
    data.append(['Documents Uploaded', motor_models.Inspection.objects.filter(payment_approved).order_by('scheduled_on')])

    # Get all inspection for which payment has been approved today
    # so that I can see success cases
    payment_approved = q & Q(status=motor_models.Inspection.PAYMENT_APPROVED)
    data.append(['Payment Approved', motor_models.Inspection.objects.filter(payment_approved).order_by('scheduled_on')])

    # Get all inspection for which policy has been issued
    # so that I can see success cases
    payment_approved = q & Q(status=motor_models.Inspection.POLICY_ISSUED)
    data.append(['Policy Issued', motor_models.Inspection.objects.filter(payment_approved).order_by('scheduled_on')])
    if request.GET.get('format', None) == 'JSON':
        json_response = []
        for i, inspection in data:
            for ins in inspection:
                inspection_details = {
                    "status": ins.status,
                    "city": str(ins.city),
                    "scheduled_on": ins.scheduled_datetime(),
                    "inspection_id": str(ins.uuid),
                    "customer_name": str(ins.transaction.customer_name()),
                    "customer_phone": ins.transaction.customer_phone_number(),
                    "address": ins.address,
                    "vehicle_number": str(ins.transaction.vehicle_registration_number()),
                    "vehicle_info": str(ins.transaction.quote.vehicle),
                }
                json_response.append(inspection_details)
        return JsonResponse({'inspections': json_response})
    return render(request, "motor_product/inspection_dashboard.html", {
        'data': data,
    })


@never_cache
@permission_required('motor_product.change_inspection')
def approve_inspection(request):
    if not request.GET:
        inspection_search_form = motor_forms.InspectionSearchForm(initial={
            'status': motor_models.Inspection.DOCUMENT_UPLOADED,
            'scheduled_on': datetime.datetime.today().strftime("%d-%m-%Y")
        })
    else:
        inspection_search_form = motor_forms.InspectionSearchForm(request.GET)
    if inspection_search_form.is_valid():
        inspection_list = motor_models.Inspection.objects.filter(inspection_search_form.query()).order_by('scheduled_on')
    else:
        inspection_list = []

    paginator = Paginator(inspection_list, 20)
    page = request.GET.get('page')
    try:
        inspections = paginator.page(page)
    except PageNotAnInteger:
        inspections = paginator.page(1)
    except EmptyPage:
        inspections = paginator.page(paginator.num_pages)

    req = request.GET.copy()
    if 'page' in req:
        req.pop('page')
    return render(request, "motor_product/inspection_dashboard.html", {
        'data': [('Result', inspections)],
        'pages': inspections,
        'inspection_search_form': inspection_search_form,
        'req': req
    })


@permission_required('motor_product.change_inspection')
def complete_inspection(request, transaction_id):
    transaction = get_object_or_404(motor_models.Transaction, transaction_id=transaction_id)
    context = {'transaction': transaction}
    if request.method == 'POST':
        if request.FILES:
            policy_document = request.FILES['policy_document']
            transaction.policy_document = policy_document
            transaction.status = 'COMPLETED'
            transaction.payment_done = True
            transaction.policy_start_date = datetime.datetime.now()
            transaction.policy_end_date = (
                datetime.datetime.now() + datetime.timedelta(days=365)
            )
            try:
                inspection = transaction.inspection_set.filter(
                    Q(status=motor_models.Inspection.PAYMENT_APPROVED)
                )[0]
            except Exception:
                raise Http404(
                    "Payment not approved for any of the Inspections on this "
                    "transaction"
                )
            else:
                inspection.status = motor_models.Inspection.POLICY_ISSUED
                inspection.save(user=request.user)
                transaction.save()
            return HttpResponseRedirect(reverse('approve_inspection'))
        else:
            context['form_error'] = "Please upload a policy document"
    return render(request, "motor_product/inspection_complete.html", context)


@csrf_exempt
@permission_required('motor_product.change_inspection')
def inspection_report(request, inspection_id):
    parts = motor_models.MotorPart.objects.all()
    inspection = motor_models.Inspection.objects.get(uuid=inspection_id)
    inspection_report = inspection.inspectionreport_set.all()
    # InspectionFormSet = formset_factory(InspectionReportForm, extra=0, can_delete=True)
    if request.method == "GET":
        if inspection_report:
            InspectionFormSet = inlineformset_factory(
                motor_models.Inspection,
                motor_models.InspectionReport,
                fields=('part', 'status'),
                extra=0
            )
            formset = InspectionFormSet(instance=inspection)
        else:
            InspectionFormSet = inlineformset_factory(
                motor_models.Inspection,
                motor_models.InspectionReport,
                fields=('part', 'status'),
                extra=parts.count()
            )
            formset = InspectionFormSet(initial=[{'part': i} for i in parts], instance=inspection)
        return render(request, "motor_product/inspection_report.html", {
            'parts_name': parts,
            'formset': formset,
        })

    if request.method == 'POST':
        InspectionFormSet = inlineformset_factory(
            motor_models.Inspection,
            motor_models.InspectionReport,
            fields=('part', 'status')
        )
        formset = InspectionFormSet(data=request.POST, instance=inspection)
        if formset.is_valid():
            formset.save()
        else:
            return HttpResponse(json.dumps({'success': False, 'errors': formset.errors}))
        return HttpResponse(json.dumps({'success': True}))


@never_cache
@permission_required('motor_product.change_inspection', raise_exception=True)
def generate_report(request, inspection_id):
    inspection = motor_models.Inspection.objects.get(uuid=inspection_id)
    rc_docs = inspection.inspection_documents.filter(document_type='rc-copy').exclude(
        approval_status=motor_models.MotorInspectionDocument.APPROVAL_REJECTED)
    if not rc_docs:
        raise Http404
    inspection_report = inspection.inspectionreport_set.filter(part__is_accessory=False)
    extra_accessories = inspection.inspectionreport_set.filter(part__is_accessory=True)
    documents = inspection.inspection_documents.exclude(
        document_type__in=['rc-copy', 'last-policy-copy']).exclude(
        approval_status=motor_models.MotorInspectionDocument.APPROVAL_REJECTED)

    prev_policy = inspection.inspection_documents.filter(document_type='last-policy-copy').exclude(
        approval_status=motor_models.MotorInspectionDocument.APPROVAL_REJECTED)
    bifuel = inspection.inspection_documents.filter(document_type='bifuel-kit').exclude(
        approval_status=motor_models.MotorInspectionDocument.APPROVAL_REJECTED)
    return render(request, "motor_product/generate_report.html", {
        'inspection': inspection,
        'inspection_parts': inspection_report,
        'documents': documents,
        'rc_docs': rc_docs,
        'prev_policy': prev_policy,
        'bifuel': bifuel,
        'extra_accessories': extra_accessories
    })


@never_cache
@permission_required('motor_product.change_pendinginspection', raise_exception=True)
def generate_report_pdf(request, inspection_id):
    inspection = motor_models.Inspection.objects.get(uuid=inspection_id)
    upload_path = os.path.join('uploads', 'motor_product', 'reports', str(inspection_id))
    policy_path = u'{}.pdf'.format(upload_path)
    filename = "%s" % str(inspection.transaction.vehicle_registration_number())
    config = pdfkit.configuration(wkhtmltopdf=settings.WKHTMLTOPDF_PATH)
    if default_storage.exists(policy_path):
        with default_storage.open(policy_path, 'rb') as f:
            content = f.read()
    else:
        content = pdfkit.from_string(
            generate_report(request, inspection_id).content,
            False,
            configuration=config,
            options={
                'page-size': 'A4',
                'encoding': 'UTF-8',
            }
        )
        with default_storage.open(policy_path, 'wb') as f:
            for chunk in ContentFile(content).chunks():
                f.write(chunk)
    response = HttpResponse(content)
    response['Content-Type'] = 'application/pdf'
    response['Content-disposition'] = 'attachment ; filename =%s.pdf' % filename
    return response


@never_cache
@permission_required('motor_product.change_pendinginspection', raise_exception=True)
def inspection_callback(request, inspection_id):
    inspection = motor_models.Inspection.objects.get(uuid=inspection_id)
    if request.method == 'GET':
        callbackform = motor_forms.InspectionCallBackForm()
        return render(request, 'motor_product/inspection_callback.html', {
            'form': callbackform,
            'inspection': inspection
        })

    if request.method == 'POST':
        callbackform = motor_forms.InspectionCallBackForm(request.POST)
        if callbackform.is_valid():
            callback = callbackform.save(commit=False)
            callback.inspection = inspection
            callback.save()
            data = {
                'success': True,
            }
        else:
            data = {
                'success': False,
                'errors': callbackform.errors
            }
        return HttpResponse(json.dumps(data))


@never_cache
@permission_required('motor_product.change_inspection', raise_exception=True)
def add_inspection_document(request, inspection_id):
    inspection = motor_models.Inspection.objects.get(uuid=inspection_id)
    if request.method == 'GET':
        form = motor_forms.InspectionDocumentForm()
        return render(request, 'motor_product/inspection_add_document.html', {
            'form': form,
            'inspection_id': inspection_id
        })
    if request.method == 'POST':
        form = motor_forms.InspectionDocumentForm(request.POST, request.FILES)
        if form.is_valid():
            new_doc = form.save(commit=False)
            new_doc.name = "%s_%s_%s" % (inspection_id, new_doc.document_type,
                                         datetime.datetime.now().strftime("%Y%m%d%H%M%S"))
            new_doc.inspection = inspection
            new_doc.save()
        else:
            return HttpResponse(json.dumps({'success': False, 'errors': form.errors}))
        return HttpResponse(json.dumps({'success': True}))


def overdrive_callback(request):
    return render(request, 'motor_product/overdrive-widget.html')

'''
These are the new APIs to be used by the new bike flow. The deprecated APIs will be removed once
the car flow is changed to this flow
'''

@never_cache
def active_insurers(request, vehicle_type):
    return_data = copy.deepcopy(BaseMixin.default_serialized_response)
    if vehicle_type == 'twowheeler':
        active_type = "is_active_for_twowheeler"
    elif vehicle_type == 'fourwheeler':
        active_type = "is_active_for_car"
    else:
        return_data['success'] = False
        return_data['errorMessage'] = "Invalid Vehicle type"
        return_data['errorItems'].append("vehicle_type is invalid")
        return_status = 400
    if return_data['success']:
        return_data['data'] = []
        for insurer in motor_models.Insurer.objects.all():
            return_data['data'].append({
                "title": insurer.title,
                "slug": insurer.slug,
                "is_active": getattr(insurer, active_type, False)
            })
        return_status = 200
    return JsonResponse(return_data, status=return_status)


class AvailableVehicleMakeView(braces_views.CsrfExemptMixin, BaseMixin, ListView):
    http_method_names = ['get']
    model = motor_models.Make

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        context = self.get_context_data()
        if self.mapped_vehicle_type:
            return_status = 200
        else:
            return_status = 400
            context['errorMessage'] = "Not a valid vehicle type. Select either twowheeler or fourwheeler"
        return JsonResponse(context, status=return_status)

    def get_queryset(self):
        queryset = super(AvailableVehicleMakeView, self).get_queryset()
        if self.kwargs.get('vehicle_type', 'twowheeler') == 'twowheeler':
            filter_kwargs = {'is_active_for_twowheeler': True}
        else:
            filter_kwargs = {'is_active_for_fourwheeler': True}
        return queryset.filter(**filter_kwargs)

    def get_context_data(self):
        context_data = super(AvailableVehicleMakeView, self).get_context_data()
        context_data[self.serialized_data_key_name] = serialized_response = copy.deepcopy(self.default_serialized_response)
        serialized_response['data'] = map(
            lambda model: {
                'id': model['id'],
                'name': model['name'],
                'is_popular': model['is_popular']
            }, context_data['make_list'].values('id', 'name', 'is_popular').distinct()
        )
        return serialized_response


class AvailableVehicleModelView(braces_views.CsrfExemptMixin, BaseMixin, ListView):
    http_method_names = ['get']
    model = motor_models.Model

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        context = self.get_context_data()
        if self.mapped_vehicle_type:
            return_status = 200
        else:
            return_status = 400
            context['errorMessage'] = "Not a valid vehicle type. Select either twowheeler or fourwheeler"
        return JsonResponse(context, status=return_status)

    def get_queryset(self):
        queryset = super(AvailableVehicleModelView, self).get_queryset()
        return queryset.filter(vehicle_type=self.mapped_vehicle_type, make__id=self.kwargs.get('make_id', 0))

    def get_context_data(self):
        context_data = super(AvailableVehicleModelView, self).get_context_data()
        context_data[self.serialized_data_key_name] = serialized_response = copy.deepcopy(self.default_serialized_response)
        serialized_response['data'] = map(
            lambda model: {
                'id': model['id'],
                'name': model['name'],
                'is_popular': model['is_popular']
            }, context_data['model_list'].values('id', 'name', 'is_popular').distinct()
        )
        return serialized_response


@xframe_options_exempt
@never_cache
def desktopFormApi(request, vehicle_type, insurer_slug, transaction_id):
    insurer = get_object_or_404(motor_models.Insurer, slug=insurer_slug)
    transaction = get_object_or_404(motor_models.Transaction, transaction_id=transaction_id)
    insurer_in_transaction = transaction.insurer.slug

    if not (insurer_slug == insurer_in_transaction):
        raise Http404

    if vehicle_type not in VEHICLE_TYPE_TO_URL_TAG_MAP.values():
        raise Http404

    if request.method == 'POST':
        post_qdict = copy.deepcopy(request.POST)
        form_details = parser_utils.qdict_to_dict(post_qdict)
        reset_form_val = form_details.pop('resetForm', 'false')
        reset_form = True if reset_form_val == 'true' else False
        if reset_form:
            if transaction.status in ['DRAFT', "PENDING"] and request.IS_INTERNAL:
                transaction.raw['new_user_form_details'] = {}
                transaction.save()
            return HttpResponseRedirect(reverse('desktopForm', args=(
                vehicle_type, insurer_slug, transaction_id)
            ))
        else:
            retval = prod_utils.new_save_form_partial_details(
                vehicle_type, form_details, insurer_slug, transaction)
        transaction.refresh_from_db()
        act = transaction.track_activity(core_activity.VIEWED, 'proposal')
        if (
                act
                and act.requirement
                and not act.requirement.requirementdealermap_set.first()
                and request.user.is_authenticated()
                and request.user.is_dealer()
                and request.session.get('dealer_dealership_id', False)
                and transaction.requirement
                # TODO: temporary fix.
        ):
            req_dealer_map = motor_models.RequirementDealerMap.objects.create(
                requirement=act.requirement,
                dealer_id=request.session['dealer_dealership_id'])
            ignore_keys = ["pa_passenger", "service_tax", "basicTp", "cngKit", "paOwnerDriver", "pa_passenger"]
            comission_value = prod_utils.get_flat_premium(
                transaction.quote.raw_data,
                [transaction.raw['quote_response']],
                ignore_keys=ignore_keys)[0]['final_premium']
            req_dealer_map.commission_value = (comission_value * req_dealer_map.dealer.get_comission_percentage()) / 100
            req_dealer_map.save()
        return HttpResponse(json.dumps({'success': retval}))
    else:
        # show_reset_button = False
        searched_for = transaction.quote.policy_type
        if searched_for == "renew" and transaction.quote.policy_expired() and transaction.status in ['DRAFT', "PENDING"]:
            transaction.status = "FORM_EXPIRED"
            transaction.save()
        if not transaction.is_complete:
            created_time = transaction.created_on.astimezone(
                timezone.get_current_timezone())
            created_time = created_time.replace(tzinfo=None)
            expiry_time = datetime.datetime.strptime('01-01-2015', '%d-%m-%Y')
            if created_time < expiry_time:
                raise Http404
        form_data = prod_utils.get_buy_form_details(transaction_id, api_call=True)
        if form_data is None:
            return HttpResponseRedirect('/motor/%s/' % VEHICLE_URL_MAP[vehicle_type])
        if not (form_data.get('user_form_details', {}).get('cust_name')) and transaction.status in ['DRAFT', "PENDING"] and not request.IS_INTERNAL:
            # Pre filling form data based on last transaction seen/touched if current one is empty

            last_transaction = motor_models.Transaction.objects.filter(
                transaction_id=request.session.get('last_seen_transaction_id')
            ).last()

            if last_transaction and last_transaction.id != transaction.id and "new_user_form_details" in last_transaction.raw:
                # Fields to populate
                included_keys = [
                    'aan_number', 'add-same',
                ] + [
                    'add_consoliated_field', 'add_landmark', 'add_pincode',
                ] + [
                    'cust_dob', 'cust_email', 'cust_name', 'cust_gender', 'cust_marital_status',
                    'cust_pan', 'cust_phone', 'nominee_age', 'nominee_name', 'nominee_relationship',
                ]
                # TODO: Map other fields:
                '''
                    ['add_city', 'add_district', 'add_state',
                    'loc_add_city', 'loc_add_district', 'loc_add_state',
                    'reg_add_city', 'reg_add_district', 'reg_add_state',
                    'loc_reg_add_city', 'loc_reg_add_district', 'loc_reg_add_state']
                '''
                quote = transaction.quote
                last_quote = last_transaction.quote
                if(
                    (last_quote.raw_data['registrationNumber[]'] == quote.raw_data['registrationNumber[]']) and
                    (last_quote.raw_data['isNewVehicle'] == quote.raw_data['isNewVehicle']) and
                    (last_quote.vehicle == quote.vehicle)
                ):
                    # Removed policy expired check from here.
                    # (last_quote.policy_expired() is quote.policy_expired()) and
                    # Fields to populate if it's not a 'totally' new search
                    included_keys += [
                        'vehicle_reg_no', 'vehicle_chassis_no', 'vehicle_engine_no', 'is_car_financed', 'isFastlaneFieldVisible', 'previous_ncb',
                    ]
                    included_keys += ['past_policy_insurer', 'past_policy_number', 'car-financier-text', 'car-financier',
                                      'past_policy_expiry_date', 'registration_date']
                previous_form_data = copy.deepcopy(last_transaction.raw.get('new_user_form_details', {}))
                if insurer_slug in ['future-generali', 'hdfc-ergo', 'iffco-tokio', 'l-t', 'reliance', 'tata-aig']:
                    included_keys += [
                        'reg_add_building_name', 'reg_add_extra_line', 'reg_add_house_no',
                        'reg_add_landmark', 'reg_add_pincode', 'reg_add_street_name',
                    ] + [
                        'loc_reg_add_building_name', 'loc_reg_add_extra_line', 'loc_reg_add_house_no',
                        'loc_reg_add_landmark', 'loc_reg_add_pincode', 'loc_reg_add_street_name',
                    ] + [
                        'loc_add_building_name', 'loc_add_extra_line', 'loc_add_house_no',
                        'loc_add_landmark', 'loc_add_pincode', 'loc_add_street_name',
                    ]
                else:
                    included_keys.remove('add-same')
                # form_data['user_form_details'] = {}
                if previous_form_data.get('vehicle_reg_no', None) \
                    and last_transaction.quote.raw_data.get('fastlane_success', '0') != '1' \
                    and last_quote.raw_data['registrationNumber[]'] == quote.raw_data['registrationNumber[]']:
                    form_data['user_form_details']['vehicle_reg_no'] = previous_form_data['vehicle_reg_no']
                for key in set(included_keys).intersection(previous_form_data.keys()):
                    if key not in form_data['user_form_details']:
                        form_data['user_form_details'][key] = previous_form_data[key]
                # If selected insurer doesn't support nominee relationship selected in previous transaction then select 'other'
                # If 'other' isn't one of the options either then puke an error on frontend instead
                nominee_relationship = form_data['user_form_details'].get('nominee_relationship')
                relationship_list = form_data['form_details'].get('relationship_list')
                form_data['user_form_details']['cust_gender'] = previous_form_data.get('cust_gender', 'Male')
                form_data['user_form_details']['cust_marital_status'] = previous_form_data.get('cust_marital_status', '1')
                if (
                    relationship_list and nominee_relationship and
                    nominee_relationship not in relationship_list and
                    'Other' in relationship_list
                ):
                    form_data['user_form_details']['nominee_relationship'] = 'Other'

                if len(form_data['user_form_details'].keys()) > 0:
                    form_data['user_form_details']['is_pre_populated'] = True
                    # show_reset_button = True
            elif last_transaction and last_transaction.id == transaction.id:
                pass
            else:
                form_data['user_form_details']['cust_gender'] = 'Male'
                form_data['user_form_details']['cust_marital_status'] = '1'

        # Setting default gender and marital status
        if 'cust_gender' not in form_data.get('user_form_details', {}):
            form_data['user_form_details']['cust_gender'] = 'Male'
        if 'cust_marital_status' not in form_data.get('user_form_details', {}):
            form_data['user_form_details']['cust_marital_status'] = '1'

        # Get latest inspection which is not cancelled (pending, approved, rejected, done)
        inspection = transaction.inspection_set.filter(
            ~Q(status=motor_models.Inspection.CANCELLED)
        ).order_by('created_on').last()
        request.session.update({'last_seen_transaction_id': transaction.transaction_id})
        request.session.modified = True
        if hasattr(transaction.insurer, 'cdaccount'):
            cd_balance = transaction.insurer.cdaccount.balance
        else:
            cd_balance = 0
        transaction.quote.raw_data['payment_mode'] = 'PAYMENT_GATEWAY'
        transaction.quote.save()
        transaction_status = {
            "complete": True if transaction.status in ['COMPLETED', 'MANUAL COMPLETED', "FAILED", "CANCELLED"] else False,
            "success": True if transaction.is_complete else False,
            "success_link": reverse(
                'payment_success',
                args=('twowheeler', transaction.transaction_id)
            ) if transaction.is_complete else '',
        }
        return JsonResponse({
            'insurer_id': insurer.id,
            'insurer_title': insurer.title,
            'insurer_slug': insurer.slug,
            'transaction_id': transaction_id,
            'data': form_data,
            'vehicle_type': vehicle_type,
            'mode': request.COOKIES.get('mode', 'assisted'),
            'tparty': request.GET.get('tparty', None),
            'is_mobile': request.user_agent.is_mobile,
            'affiliate_source': request.COOKIES.get('affiliateSource', ''),
            'cd_enabled': 1 if(cd_balance and request.user.groups.filter(name="CD_ACCOUNT_ADMIN").exists()) else 0,
            'cd_balance': cd_balance,
            'toll_free_number': settings.CAR_TOLL_FREE,
            'show_reset_button': False,
            'transaction_status': transaction_status
        })


@csrf_exempt
def populate_model_as_make(request):
    make_id = request.POST.get('make', None)
    if make_id and make_id.isdigit():
        models = motor_models.Model.objects.filter(make_id=make_id).order_by('name').values('id', 'name')
        return HttpResponse(json.dumps({'success': True, 'models': list(models)}), content_type='application/json')
    return HttpResponse(json.dumps({'success': False}))

def corporateDiscount(request, company):
    template_name = "motor_product/corporate_discount.html"

    if request.method == 'GET':
        form = motor_forms.CorporateEmployeeForm(company=company)

    if request.method == 'POST':
        form = motor_forms.CorporateEmployeeForm(request.POST, company=company)
        if form.is_valid():
            response = HttpResponseRedirect('/')
            cookie_max_age = 60*60*24*7
            response.set_cookie('employee_code', form.data.get('employee_code'), max_age=cookie_max_age)
            response.set_cookie('company', company, max_age=cookie_max_age)
            return response
    return render(request, template_name, context={'form': form})

def test_bk_api(request):
    from motor_product.insurers import base_proposal_form as proposal_form
    return JsonResponse(proposal_form.generate_final_form(asd="tets"), safe=False)

@csrf_exempt
def share_quotes(request):
    type = request.POST.get('type', 'shortlink')
    url = request.POST.get('url')
    quote_id = request.POST.get('quote_id', None)
    number = request.POST.get('number', [])
    email = request.POST.get('email', [])
    shortlink = get_short_url(url)
    obj = {
        'url': shortlink,
        'tollfree': '18002099940'
    }
    if type == 'shortlink':
        return JsonResponse({'success': True, 'shorturl': shortlink})
    if email or number:
        if quote_id:
            obj['quote'] = motor_models.Quote.objects.filter(quote_id=quote_id).last()
        try:
            cf_notify('SHARE_QUOTES', numbers=number, to=email, obj=obj)
        except Exception, e:
            return JsonResponse({'success': False, 'message':"Exception: {}".format(e)})
        return JsonResponse({'success': True, 'shorturl': shortlink})
    else:
        return HttpResponse(json.dumps({'success': False, 'message':"Please pass the required parameters"}))


def location_to_rto_mapping(request):
    ip_address = request.META['REMOTE_ADDR']
    key = ip_address+'_location'

    # Trying to get rto_map from cache
    rto_map = cache.get(key)
    if rto_map:
        return JsonResponse({"placeholder": rto_map})

    # Getting state based on ip address and fuzzy matching with RTO_STATE_TO_STATE_NAME_MAPPING
    rto_map = 'MH02BX0377'
    address = ip2location(ip_address)
    if address and address['state']:
        states = dict((y, x) for x, y in RTO_STATE_TO_STATE_NAME_MAPPING.iteritems())
        result = process.extractOne(address['state'], states.keys(), score_cutoff=70)
        if result:
            match, ratio = result
            rto_map = states[match] + '-02-BX-0222' if states[match] in ['DL', "GJ", "UK", "UA"] else states[match] + '02BX0377'
    cache.set(key, rto_map, 3600)
    return JsonResponse({"placeholder": rto_map})


def get_customer_address(transaction):
    from motor_product.prod_utils import VEHICLE_INTEGRATION_MAP
    user_details = transaction.raw.get('user_form_details', {})
    reg_city_id = user_details.get('reg_add_city', '')
    reg_state_id = user_details.get('reg_add_state', '')
    reg_district = user_details.get('reg_add_district', '')
    add_same = '1'
    if (
        'reg_add_state' in user_details and
        (
            user_details['reg_add_state'] != user_details['add_state'] or
            user_details.get('add-same', '1') == '0')
    ):
        add_same = '0'
    reg_city_name = ''
    for city_info in STATE_ID_TO_CITY_LIST.get(reg_state_id, []):
            if city_info['id'] == reg_city_id:
                reg_city_name = city_info['name']
                break
    reg_state_name = STATE_ID_TO_STATE_NAME.get(reg_state_id, '')
    add_state = user_details.get('add_state', '')
    vehicle_type = format_vehicle_type(transaction.vehicle_type)
    m = VEHICLE_INTEGRATION_MAP[vehicle_type][transaction.insurer.slug]
    buy_form_details = m.get_buy_form_details(transaction.quote.raw_data, transaction)

    if add_state in buy_form_details.get('state_list', {}):
        if isinstance(buy_form_details['state_list'], list):
            pass
        else:
            add_state = buy_form_details['state_list'][add_state]
    elif add_state in STATE_ID_TO_STATE_NAME:
        add_state = STATE_ID_TO_STATE_NAME[add_state]
    add_city = ''
    success, cities = prod_utils.get_cities(
        format_vehicle_type(transaction.vehicle_type), transaction.insurer.slug, transaction.raw['user_form_details'].get('add_state', '')
    )
    if not (success and cities):
        success, cities = prod_utils.get_cities(
        format_vehicle_type(transaction.vehicle_type), transaction.insurer.slug, add_state
    )
    if success and cities:
        for city_info in cities:
                if city_info['id'] == user_details.get('add_city', ''):
                    add_city = city_info['name']
                    break
    address_dict = {
        'add_building_name': user_details.get('add_building_name', ''),
        'add_house_no': user_details.get('add_house_no', ''),
        'add_street_name': user_details.get('add_street_name', ''),
        'add_landmark': user_details.get('add_landmark', ''),
        'add_city': add_city,
        'add_district': user_details.get('add_district', ''),
        'add_state': add_state,
        'add_same': add_same,
        'reg_add_building_name': user_details.get('reg_add_building_name', ''),
        'reg_add_house_no': user_details.get('reg_add_house_no', ''),
        'reg_add_street_name': user_details.get('reg_add_street_name', ''),
        'reg_add_landmark': user_details.get('reg_add_landmark', ''),
        'reg_add_state': reg_state_name,
        'reg_add_city': reg_city_name,
        'reg_add_district': reg_district,
    }
    return address_dict
