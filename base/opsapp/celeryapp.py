from __future__ import absolute_import
from celery import Celery

app = Celery(
    'opsapp',
)

if __name__ == '__main__':
    app.start()
