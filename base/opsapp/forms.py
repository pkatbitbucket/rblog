from django import forms
# from django.contrib.auth.models import User
from models import DeployTask


class TaskForm(forms.ModelForm):

    class Meta:
        model = DeployTask
        fields = ['name', 'command', 'host']
