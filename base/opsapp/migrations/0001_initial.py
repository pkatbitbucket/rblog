# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='DeployTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('command', models.CharField(max_length=50)),
                ('host', models.CharField(max_length=50)),
                ('no_runs', models.IntegerField(null=True, blank=True)),
                ('creater', models.ForeignKey(related_name='user_tasks', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='DeployTaskRun',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('why', models.TextField(max_length=200, null=True, blank=True)),
                ('status', models.CharField(default=b'NEW', max_length=70, choices=[(b'NEW', b'New'), (b'RUNNING', b'Running'), (b'COMPLETED', b'Completed'), (b'SUCCEDED', b'Succeded')])),
                ('created_on', models.DateTimeField(null=True, blank=True)),
                ('started_on', models.DateTimeField(null=True, blank=True)),
                ('exited_on', models.DateTimeField(null=True, blank=True)),
                ('exit_code', models.IntegerField(null=True, blank=True)),
                ('log_file', models.CharField(max_length=50, null=True, blank=True)),
                ('pid', models.IntegerField(null=True, blank=True)),
                ('task', models.ForeignKey(to='opsapp.DeployTask')),
                ('who', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
    ]
