# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('opsapp', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='deploytaskrun',
            name='created_on',
        ),
        migrations.AddField(
            model_name='deploytask',
            name='created_on',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='deploytask',
            name='host',
            field=models.CharField(max_length=50, choices=[(b'SANDMAN', b'coverfox@sandman.coverfox.com'), (b'LMS', b'LMS')]),
        ),
        migrations.AlterField(
            model_name='deploytaskrun',
            name='status',
            field=models.CharField(default=b'NEW', max_length=70, choices=[(b'new', b'New'), (b'running', b'Running'), (b'completed', b'Completed'), (b'failed', b'Failed')]),
        ),
    ]
