# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('opsapp', '0002_auto_20151013_1334'),
    ]

    operations = [
        migrations.RenameField(
            model_name='deploytask',
            old_name='creater',
            new_name='creator',
        ),
    ]
