from django.db import models
from core.models import User


class DeployTask(models.Model):
    host_choice = (
        ('SANDMAN', 'coverfox@sandman.coverfox.com'),
        ('LMS', 'LMS')
    )

    name = models.CharField(max_length=50)
    command = models.CharField(max_length=50)
    host = models.CharField(choices=host_choice, max_length=50)
    no_runs = models.IntegerField(blank=True, null=True)
    creator = models.ForeignKey(User, related_name='user_tasks')
    created_on = models.DateTimeField(blank=True, null=True)

    def __unicode__(self):
        return self.name


class DeployTaskRun(models.Model):
    NEW = "new"
    RUNNING = "running"
    COMPLETED = "completed"
    FAILED = "failed"
    status_choice = (
        (NEW, 'New'),
        (RUNNING, 'Running'),
        (COMPLETED, 'Completed'),
        (FAILED, 'Failed')
    )

    task = models.ForeignKey(DeployTask)
    who = models.ForeignKey(User, blank=True, null=True)
    why = models.TextField(max_length=200, blank=True, null=True)
    status = models.CharField(choices=status_choice, max_length=70, default='NEW')
    started_on = models.DateTimeField(blank=True, null=True)
    exited_on = models.DateTimeField(blank=True, null=True)
    exit_code = models.IntegerField(blank=True, null=True)
    log_file = models.CharField(max_length=50, blank=True, null=True)
    pid = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return unicode(self.task)
