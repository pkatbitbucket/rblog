"""
Smoke Test for sanity check. Create test by adding methods prefixed by 'test_' in StartTesting class.
Run smoke test like normal python file and pass BASE_URL (optional)
eg python SmokeTests.py https://coverfox.com/
"""
import os
import sys

import django

BASE_DIRECTORY = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(BASE_DIRECTORY)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
django.setup()

import unittest
from functools import partial
from urlparse import urljoin, urlparse, urlunparse

import redis
import requests
from celery.app import Celery
from django.conf import settings
from django.db import connections

from core.models import User

BROKER_URL = getattr(settings, 'BROKER_URL', 'redis://localhost:6381/1')
CELERY_RESULT_BACKEND = getattr(settings, 'CELERY_RESULT_BACKEND', 'redis://localhost:6381/2')

CELERY_APP = Celery(broker=BROKER_URL, backend=CELERY_RESULT_BACKEND)


class StartTesting(unittest.TestCase):
    """
    Inherit from python unittest.
    """
    def test_ssl_certificate(self):
        try:
            requests.get(SECURE_BASE_URL, verify=True)
        except requests.exceptions.SSLError:
            self.assertTrue(False, "SSL Failure!!! SSL Certificate expired")

    def test_user_login(self):
        client = requests.Session()
        user, created = User.objects.get_or_create(email="testing@coverfox.com", is_active=True)
        if created:
            user.set_password('testing')
            user.save()
        csrf_token = client.get(BASE_URL).cookies[getattr(settings, 'CSRF_COOKIE_NAME')]
        response = client.post(urljoin(BASE_URL, settings.LOGIN_URL), {'username': [user.email],
                                                                       'password': ['testing'],
                                                                       'next': [''], 'csrfmiddlewaretoken': csrf_token})
        if 'user/policy/' not in response.url:
            self.assertEqual(False, 200, 'Error in User login Api')

    def test_celery(self):
        from celery.app.control import Inspect
        result = bool(Inspect(app=CELERY_APP).registered())
        self.assertTrue(result, "Celery is down !!!")

    def test_redis(self):
        port = BROKER_URL.split(':')[-1].split('/')[0]
        host = BROKER_URL.split(':')[1].replace('/', '')
        rs = redis.StrictRedis(host=host, port=port, db=0)
        try:
            response = rs.client_list()
        except redis.ConnectionError:
            self.assertTrue(False, "Redis is not working")

    def test_db(self):
        for db in connections:
            try:
                conn = connections[db]
                conn.cursor()
            except:
                self.assertTrue(False, "%s database is not reachable" % db)


class RequestGenerator(object):
    """
    Generates a request object based on url parameters passed.
    """

    def __init__(self, info):
        self.data = info.get('data', {})
        if info.get('remove_base_url', None):
            self.url = info['path']
        else:
            self.url = urljoin(BASE_URL, info['path'])
            self.response = None
            self.data.update({'csrfmiddlewaretoken': CSRF_TOKEN})
        self.status_code = info.get('status_code', 200)
        self.method = info.get('method', 'get')
        self.params = info.get('params', {})

    def make_request(self):
        response = requests.request(self.method, self.url, params=self.params, data=self.data)
        return response.status_code

    @staticmethod
    def url_test(each_url):
            request_obj = RequestGenerator(each_url)
            response = request_obj.make_request()
            assert response == 200


if __name__ == "__main__":
    """
   Url_list format: [
                    {'url':'url_to_test<base url will be automatically added>',
                    'method':'request_type<e.g get, put ,post..> default is get',
                    'params':'additional_data<dictionary>',
                    'data':'post data<dictionary>',
                    'status_code':'expected_status_code <e.g 200, 302, 301>',
                    'remove_base_url': <bool>}
                    ]
   """

    url_list = [{'path': ''},
                {'path': 'accounts/login/'},
                {'path': 'travel-insurance/'},
                {'path': 'motor/car-insurance/'},
                {'path': 'motor/twowheeler-insurance/'},
                {'path': 'health-plan/'},
                {'path': 'home-insurance/'},
                {'path': 'about/'},
                {'path': 'articles/'},
                {'path': 'contact-us/'},
                {'path': 'careers/'},
                {'path': 'sitemap/'},
                {'path': 'faqs/'}]
    try:
        BASE_URL = sys.argv[1]
        SECURE_BASE_URL = list(urlparse(BASE_URL))
        SECURE_BASE_URL[0] = 'https'
        SECURE_BASE_URL = urlunparse(SECURE_BASE_URL)

    except IndexError:
        try:
            import local_settings
            BASE_URL = getattr(local_settings, 'SITE_URL', 'http://localhost:8000/')
            SECURE_BASE_URL = getattr(local_settings, 'SECURE_BASE_URL', 'http://localhost:8000/')
        except ImportError:
            BASE_URL = 'http://localhost:8000/'
            SECURE_BASE_URL = BASE_URL
    CLIENT = requests.Session()
    CSRF_TOKEN = CLIENT.get(BASE_URL).cookies[getattr(settings, 'CSRF_COOKIE_NAME')]
    CSRF_TOKEN = ''
    for i, url in enumerate(url_list):
        test_name = 'test_{0}'.format(url['path'])
        test_func = partial(RequestGenerator.url_test, url)
        test_func.__doc__ = test_name
        setattr(StartTesting, test_name, test_func)

    suite = unittest.TestLoader().loadTestsFromTestCase(StartTesting)
    unittest.TextTestRunner(verbosity=2).run(suite)
