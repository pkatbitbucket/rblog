from __future__ import absolute_import
from django.conf import settings
from celery_app import app
import time
from datetime import datetime
import subprocess
from .models import DeployTaskRun, DeployTask


@app.task(name='tasks.generate_log')
def generate_log(file_name, command, request):
    task_obj = DeployTask.objects.get(command=command)
    run_obj = DeployTaskRun.objects.create(task=task_obj)
    started_on = datetime.now()

    with open(settings.DEPLOY_LOG_DIR + file_name + '.txt', 'a') as fp:
        fp.write('starting_on:  %s \n' % started_on)

    log_file = open(settings.DEPLOY_LOG_DIR + file_name + '.txt', 'a')
    process = subprocess.Popen(command.split(), stdout=log_file)
    log_file.close()
    run_obj.pid = process.pid
    run_obj.status = DeployTaskRun.RUNNING
    run_obj.who = request.user
    run_obj.why = request.POST.get("why", "")
    run_obj.started_on = started_on
    run_obj.log_file = file_name
    run_obj.save()

    task_obj.no_runs += 1
    task_obj.save()

    output, error = process.communicate()  # this will block
    run_obj = DeployTaskRun.objects.filter(task=task_obj).order_by('-id')[0]
    exited_on = datetime.now()
    exit_code = process.returncode
    if exit_code == 0:
        run_obj.status = DeployTaskRun.COMPLETED
    else:
        run_obj.status = DeployTaskRun.FAILED
    run_obj.exited_on = exited_on
    run_obj.exit_code = exit_code
    run_obj.save()

    with open(settings.DEPLOY_LOG_DIR + file_name + '.txt', 'a') as fp:
        if error is not None:
            fp.write('error\n %s' % error)
        fp.write('\nexited_on:  %s \n' % exited_on)


@app.task(name='tasks.deploy_new')
def deploy_new(user):
    new_file = str(int(time.time()))
    with open(settings.DEPLOY_LOG_DIR + new_file + '.txt', 'w') as fp:
        fp.write('Deploy started by ' + user + '\n')
    return new_file
