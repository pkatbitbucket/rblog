from django.test import TestCase, Client, RequestFactory

import mock
from mock import patch

from opsapp.views import send_stats, translate_collectd_to_carbon

class OPSTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.req_data = "PUTVAL collectd.office1.curl.cfox_index.response_time "\
                        "interval=10.000 1442387417.964:0.308956"
        
        self.wrong_req_data = "PUTVAL office1/curl-cfox_index/response_time "\
                        "interval=10.000 "
        self.prefix = ""
 
    def test_send_status_request(self):
        request = self.factory.post(
            '/send-stats/?auth=coverfoxops', 
            data=self.req_data,
            content_type='text/plain'
        )

        # Append mocked function. 
        send_udp = mock.Mock(return_value=True)
        response = send_stats(request, send_udp=send_udp)
        self.assertEqual(response.status_code, 200)
        raw_response = response.content
        self.assertEqual(raw_response, "Ok")
       
    def test_send_status_multiple_data(self):
        multiple_req_data = "%s\r\n%s" % (self.req_data, self.req_data)
        request = self.factory.post(
            '/send-stats/?auth=coverfoxops', 
            data=multiple_req_data,
            content_type='text/plain'
        )

        # Append mocked function. 
        send_udp = mock.Mock(return_value=True)
        response = send_stats(request, send_udp=send_udp)
        self.assertEqual(response.status_code, 200)
        raw_response = response.content
        self.assertEqual(raw_response, "Ok")
    
    def test_wrong_req_data_send_status(self):
        request = self.factory.post(
            '/send-stats/?auth=coverfoxops', 
            data=self.wrong_req_data,
            content_type='text/plain'
        )

        # Append mocked function. 
        send_udp = mock.Mock(return_value=True)
        try:
            send_stats(request, send_udp=send_udp)
        except:
            self.assertEqual(True, True)
            return True
            
        self.assertEqual(True, False, "Test failed. Invalid data considered right.")
    
    def test_auth_failed_send_status(self):
        request = self.factory.post(
            '/send-stats/?auth=cover', 
            data=self.wrong_req_data,
            content_type='text/plain'
        )

        # Append mocked function. 
        send_udp = mock.Mock(return_value=True)
        response = send_stats(request, send_udp=send_udp)
        self.assertEqual(response.status_code, 200)
        raw_response = response.content
        self.assertEqual(
            raw_response, "Auth failed", "Invalid response. It "\
            "return Auth failed but got %s" % raw_response
        )
    
    def test_multiline_collectd_stat_format_modifier_function(self):
        carbon_data = translate_collectd_to_carbon(
            "%s\r\n%s" % (self.req_data, self.req_data),
            self.prefix
        )

        ideal_carbon = [
            "collectd.office1.curl.cfox_index.response_time 0.308956 "\
            "1442387417",
            "collectd.office1.curl.cfox_index.response_time 0.308956 "\
            "1442387417"
        ]
        
        self.assertEqual(
            isinstance(carbon_data, list), True, "Invalid"\
            " response type"
        )
        
        self.assertEqual(
            len(carbon_data), 2, "Invalid format it should "\
            "return %s but got %s" % (ideal_carbon,
                                       carbon_data)
        )
        self.assertEqual(
            carbon_data[0], ideal_carbon[0], 
            "It should return %s but got %s" % (ideal_carbon, 
                                                 carbon_data)
        )
        self.assertEqual(
            carbon_data[1], ideal_carbon[1], 
            "It should return %s but got %s" % (ideal_carbon, 
                                                 carbon_data)
        )
    
    def test_collectd_stat_format_modifier_function(self):
        carbon_data = translate_collectd_to_carbon(self.req_data, self.prefix)
        ideal_carbon = [
            "collectd.office1.curl.cfox_index.response_time 0.308956 "\
            "1442387417"
        ]
        
        self.assertEqual(
            isinstance(carbon_data, list), True, "Invalid"\
            " response type"
        )
        
        self.assertEqual(
            len(carbon_data), 1, "Invalid format it should "\
            "return %s but got %s" % (ideal_carbon,
                                       carbon_data)
        )
        self.assertEqual(
            carbon_data[0], ideal_carbon[0], 
            "It should return %s but got %s" % (ideal_carbon, 
                                                 carbon_data)
        )

    def test_collectd_stat_format_modifier_function_with_prefix(self):
        prefix = "collectd."
        req_data = "PUTVAL office1.curl.cfox_index.response_time "\
                        "interval=10.000 1442387417.964:0.308956"
                        
        carbon_data = translate_collectd_to_carbon(req_data, prefix)
        ideal_carbon = [
            "collectd.office1.curl.cfox_index.response_time 0.308956 "\
            "1442387417"
        ]
        
        self.assertEqual(
            isinstance(carbon_data, list), True, "Invalid"\
            " response type"
        )
        
        self.assertEqual(
            len(carbon_data), 1, "Invalid format it should "\
            "return %s but got %s" % (ideal_carbon,
                                       carbon_data)
        )
        self.assertEqual(
            carbon_data[0], ideal_carbon[0], 
            "It should return %s but got %s" % (ideal_carbon, 
                                                 carbon_data)
        )
        
    def test_multiline_collectd_stat_format_modifier_func_with_prefix(self):
        prefix = "collectd."
        req_data = "PUTVAL office1.curl.cfox_index.response_time "\
                        "interval=10.000 1442387417.964:0.308956"
        multiple_req_data = "%s\r\n%s" % (req_data, req_data)
        
        carbon_data = translate_collectd_to_carbon(
            "%s\r\n%s" % (req_data, req_data),
            prefix
        )

        ideal_carbon = [
            "collectd.office1.curl.cfox_index.response_time 0.308956 "\
            "1442387417",
            "collectd.office1.curl.cfox_index.response_time 0.308956 "\
            "1442387417"
        ]
        
        self.assertEqual(
            isinstance(carbon_data, list), True, "Invalid"\
            " response type"
        )
        
        self.assertEqual(
            len(carbon_data), 2, "Invalid format it should "\
            "return %s but got %s" % (ideal_carbon,
                                       carbon_data)
        )
        self.assertEqual(
            carbon_data[0], ideal_carbon[0], 
            "It should return %s but got %s" % (ideal_carbon, 
                                                 carbon_data)
        )
        self.assertEqual(
            carbon_data[1], ideal_carbon[1], 
            "It should return %s but got %s" % (ideal_carbon, 
                                                 carbon_data)
        )
