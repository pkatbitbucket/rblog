from django.shortcuts import render_to_response, render
from django.contrib.auth.decorators import login_required
# from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.conf import settings
import os
import socket
import subprocess
from datetime import datetime
# import time
from opsapp import tasks
from models import DeployTask, DeployTaskRun
from importd import d
from utils import statsd
from forms import TaskForm


@d("/deploys/<text_file:file_name>", name="show_log")
def show_log(request, file_name):
    file_path = os.path.join(settings.DEPLOY_LOG_DIR, file_name)

    # Check file exist or not. If not exist then return 404.
    if os.path.exists(file_path):
        with open(file_path, 'r') as fp:
            contents = fp.readlines()
            return render(
                request, "opsapp/logs.html", {'contents': contents},
                context_instance=RequestContext(request)
            )
    else:
        raise Http404


@d("/deploy-now/")
@login_required(login_url=settings.LOGIN_URL)
def delpoy_now(request):
    if request.method == 'GET':
        return "opsapp/deploy_now.html"
    elif request.method == 'POST':
        user = request.user.username
        file_name = tasks.deploy_new.apply([user])
        url = '/deploys/' + file_name.result + '.txt'
        return HttpResponseRedirect(url)

"""
Assume in /var/log/deploys/ we have a bunch of files, 1441886743.365041.txt
and so on.

on /deploys/ page list all such files (convert timestamp to date).

each of them would be a link, eg /deploys/1441886743.365041/, this page
will show the content of 1441886743.365041.txt.

on /deploy-now/ ask for a confirmation if method is GET. it returns
a html with a form. the form has a button "DEPLOY NOW". clicking this
submits forms with method = post and action = ".".

in /deploy-now/ if method is post create a celery task. and put
"deploy started by {{ user }}" in /var/log/1441886743.365041.txt. and
return HTTPResponseRedirect to /deploys/1441886743.365041/.


Then write a celery worker that will pick up the task and execute it
by redirecting the output to /var/log/1441886743.365041.txt.
"""


@d('/create-task/')
def create_task(request):
    if request.POST:
        task_form = TaskForm(request.POST)
        if task_form.is_valid():
            model = task_form.save(commit=False)
            model.creator = request.user
            model.no_runs = 0
            model.created_on = datetime.now()
            model.save()
            DeployTaskRun.objects.create(task=DeployTask.objects.get(name=task_form.cleaned_data['name']))
            return HttpResponseRedirect('/')
        else:
            error_fields = task_form.errors.keys()
            return render_to_response('opsapp/create_task.html', {'form': task_form, 'error': task_form.errors, 'error_fields': error_fields}, context_instance=RequestContext(request))
    else:
        task_form = TaskForm()
    return render_to_response('opsapp/create_task.html', {'form': task_form}, context_instance=RequestContext(request))


@d('/')
def tasks_index(request):
    tasks = {}
    for dt in DeployTask.objects.all():
        tasks[dt] = DeployTaskRun.objects.filter(task=dt).order_by('-id')[0]
    all_run = DeployTaskRun.objects.all()
    latest_run = all_run[len(all_run) - 10:]
    return render_to_response('opsapp/index.html', {'tasks': tasks, 'latest_run': latest_run}, context_instance=RequestContext(request))


@d('/run-task/')
def run_task(request):
    if request.POST:
        task_id = request.POST.get("task_id", "")
        task = DeployTask.objects.get(id=task_id)
        command = task.command
        user = request.user.username
        file_name = tasks.deploy_new.apply([user])
        tasks.generate_log.apply([file_name.result, command, request])
        url = '/deploys/' + file_name.result + '.txt'
        return HttpResponseRedirect(url)
    return HttpResponseRedirect("/")


@d('/view-task/<task_id>')
def view_task(request, task_id):
    task_obj = DeployTask.objects.get(id=task_id)
    runs = DeployTaskRun.objects.filter(task=task_obj)
    return render_to_response('opsapp/run_history.html', {'runs': runs, 'task': task_obj}, context_instance=RequestContext(request))
"""
Project:

- Django + Celery + Redis

Create a Task model in Django which has the following fields
- name
-  unix command,l
- latest output,
- status
- number of runs
- ask creator
- Is the task public

Create a django form to create and save a model instance. Your form should validate
- name should not be empty
- whether the unix command to run exists on the system. Write a custom validator for this.

Create a view which lists all the tasks for which the given user has permission to view.
Display a run option next to each task the user has permission to run.

Create a view which accepts a task id to run.
You will need to call a function which will run the unix command associated with the task.
Save the task results in model and display the result.
This view is synchronous and waits for the task to finish.

Celery (asynchronous) version of the task :
Create a view which executes the task asynchronously if not already running and returns the current status of the task.
You will need to write a celery task to execute the above function.
You will need to create a separate view to fetch the latest output of the task.

Create a view which lists currently running tasks. Store this list in redis cache for faster access.
Think about cache invalidation strategies.

---------
This project used to be a task. For background information, check the task  [Converted to project] Tech: web based task runner

"""


def translate_collectd_to_carbon(collectd_lines, prefix):
    # Get timestamp and graph value from request data.
    lines = []
    collectd_lines = collectd_lines.split("\r\n")
    if collectd_lines[-1] == "":
        del collectd_lines[-1]

    for line in collectd_lines:
        splitted_str = line.split(" ")
        if splitted_str[-1] == '':
            del splitted_str[-1]

        if len(splitted_str) != 4:
            raise Exception("Invalid format")

        timestamp = splitted_str[-1].split(":")[0].split(".")[0]
        value = splitted_str[-1].split(":")[-1]
        matric_key = "%s%s" % (prefix, splitted_str[1])
        lines.append("%s %s %s" % (matric_key, value, timestamp))

    return lines


def send_udp(lines):
    """
    Send UDP request to grafana
    """

    sock = socket.socket(
        socket.AF_INET,
        socket.SOCK_DGRAM
    )

    for line in lines:
        sock.sendto(line, (settings.CARBON_UDP_IP, settings.CARBON_UDP_PORT))

    return True


@d("/send-stats/", name="send_stats")
def send_stats(request, **args):
    """
    Get status from collectd and push it to carbon.

    Collectd will send below data in payload.
    PUTVAL collectd.office1.curl.cfox_index.response_time interval=10.000 1442387417.964:0.308956
    PUTVAL collectd.office1.curl.cfox_index.response_time interval=10.000 1442387417.800:0.408956

    This API will convert above data into below format and then will send UDP to Carbon.
    collectd.office1.curl.cfox_index.response_time 0.308956 1442387417
    collectd.office1.curl.cfox_index.response_time 0.408956 1442387417
    """
    auth = request.GET.get("auth", '')
    prefix = request.GET.get("prefix", "")
    # Match auth key with request key.
    if auth not in settings.SEND_STATS_KEYS:
        statsd.incr('ops.send_status_api.auth_failed')
        return HttpResponse("Auth failed")

    # Send udp data to carbon.
    try:
        if not send_udp(
            translate_collectd_to_carbon(request.body, prefix)
        ):
            statsd.incr('ops.send_status_api.wrong_data')
            return "Invalid request data"
    except Exception as e:
        statsd.incr('ops.send_status_api.Exception500')
        raise e

    statsd.incr('ops.send_status_api.success')
    return HttpResponse("Ok")


@login_required
@d("/git/")
def git_info(request):
    git_get_commit_id = "git log --format=%H -n 1"
    p = subprocess.Popen(
        git_get_commit_id.split(" "),
        stdout=subprocess.PIPE
    )
    out, _ = p.communicate()
    return HttpResponse(out)
