from __future__ import unicode_literals

import logging

from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.core.management.base import SystemCheckError
from django.db import models
from django.utils.crypto import get_random_string
from django.utils.functional import cached_property

__author__ = 'vinay'

logger = logging.getLogger(__name__)

default_app_config = 'otp.apps.OTPAppConfig'

_otp_models = {}


class OTPModelMixin(object):

    class OTPConfig(object):
        contact_type = None
        contact_field = None
        otp_field = 'verification_code'

    def __init__(self, dummy=False, contact='', tracker=None):
        self.dummy = dummy
        if self.dummy:
            if not (contact and tracker):
                raise ValueError('require contact and tracker for dummy')
            self.pk = tracker.session_key
            self._contact = contact
        else:
            self._validate_config()

    def is_anonymous(self):
        return False

    def is_authenticated(self):
        return True

    def _validate_config(self):
        if not isinstance(self, models.Model):
            raise ValueError('{} is not a django model'.format(self.__class__))

        self._meta.get_field(self.OTPConfig.contact_field)
        self._meta.get_field(self.OTPConfig.otp_field)

    @classmethod
    def _tracker_field_name(cls):
        from core import models as core_models
        for f in cls._meta.get_fields():
            if f.rel and f.rel.model is core_models.Tracker:
                return f.name

    @property
    def contact(self):
        if self.dummy:
            return self._contact
        return getattr(self, self.OTPConfig.contact_field or '', None)

    @property
    def contact_type(self):
        if self.dummy:
            return get_contact_type(self.contact)
        return getattr(self, self.OTPConfig.contact_type or '', None)

    @property
    def cache_key(self):
        return 'otp_{}_{}'.format(self.__class__, self.pk)

    @classmethod
    def get_or_create_contact(cls, tracker, contact):
        if not issubclass(cls, models.Model):
            return cls(dummy=True, contact=contact, tracker=tracker)

        field_data = {
            cls._tracker_field_name(): tracker,
            cls.OTPConfig.contact_field: contact,
        }
        instance = cls._default_manager.filter(**field_data).last()
        return instance or cls._default_manager.create(**field_data)

    @classmethod
    def get_contact(cls, tracker, contact):
        if not issubclass(cls, models.Model):
            return cls(dummy=True, contact=contact, tracker=tracker)

        contact_field = cls.OTPConfig.contact_field
        field_data = {
            cls._tracker_field_name(): tracker,
            contact_field: contact,
        }
        # Let's try our best to deal with fucked up data and race conditions
        return cls._default_manager.filter(
            **field_data
        ).order_by(contact_field).distinct(contact_field).get()

    def send_verification_code(self, nid=None, **kwargs):
        """
        Should return a bool whether the code has been successfully sent
        """
        import cf_cns
        contact_type = get_contact_type(self.contact)

        otp = self._make_verification_code(6, '0123456789')

        nid = nid or 'MYACCOUNT_OTP'
        ctx = {'validity': '15 mins', 'otp': otp}

        if contact_type == 'email':
            contact_params = {'to': self.contact, 'mandrill_template': 'newbase'}
        else:
            contact_params = {'numbers': self.contact}

        _, resp = cf_cns.notify(nid=nid, obj=ctx, sync=True, **contact_params)
        if isinstance(resp, (str, unicode)):
            # NOTIFICATION_DEBUG = True
            success = True
        else:
            if contact_type == 'mobile':
                try:
                    message = resp.get('sms_response', '')[0].strip().split('|')
                except Exception as e:
                    logger.debug(msg='Failed to send OTP', exc_info=e)
                    success = False
                else:
                    success = 'success' in message[0]
            else:
                success = True

        if success:
            cache.set(
                key=self.cache_key,
                value=otp,
                timeout=15 * 60
            )

        return success

    @staticmethod
    def _make_verification_code(length=8, allowed_chars='0123456789'):
        return get_random_string(length, allowed_chars)

    def check_verification_code(self, otp):
        """
        Return True if verified else False
        """
        cache_key = 'otp_{}'.format(self.contact)
        valid = cache.get(self.cache_key) == str(otp)
        if valid:
            cache.delete(cache_key)
        return valid


class AnonymousOTPUser(object):
    id = None
    pk = None
    _contact = None

    def __str__(self):
        return 'Anonymous OTP User'

    @property
    def contact(self):
        return self._contact

    def __eq__(self, other):
        return isinstance(other, self.__class__)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return 1  # instances always return the same hash value

    def is_anonymous(self):
        return True

    def is_authenticated(self):
        return False


def auto_discover_otp_models():
    from core import models as core_models
    for klass in OTPModelMixin.__subclasses__():
        if issubclass(klass, models.Model):
            contact_type = klass.OTPConfig.contact_type
            for f in klass._meta.get_fields():
                if f.rel and (f.rel.one_to_many or f.rel.one_to_one):
                    if f.rel.model is core_models.Tracker:
                        break
            else:
                raise SystemCheckError(
                    '{} has no foreign key/one-one relation to Tracker'.format(
                        _otp_models[contact_type]._meta.model_name,
                        contact_type
                    )
                )

            if contact_type in _otp_models:
                raise SystemCheckError(
                    '{} already has {} as contact type'.format(
                        _otp_models[contact_type]._meta.model_name,
                        contact_type
                    )
                )
            _otp_models[contact_type] = klass


def otp_login(request, otp_user):
    """
    Persist the otp model in the request and model info in tracker.
    This way a user doesn't have to reauthenticate on every request.
    """
    if otp_user is None:
        otp_user = request.otp_user

    tracker = request.TRACKER
    if otp_user.pk:
        otp_login_data = {
            'contact': otp_user.contact
        }
        tracker.update_extra({'otp_login': otp_login_data})
        tracker.save(update_fields=['extra'])

    request.otp_user = otp_user


def otp_logout(request):
    """
    Removes the authenticated user's ID from the request and flushes their
    tracker data.
    """
    request.TRACKER.flush_otp_login()
    request.otp_user = AnonymousOTPUser()


def get_contact_type(contact):
    for contact_type, model in _otp_models.items():
        meta = model._meta
        field = meta.get_field(model.OTPConfig.contact_field)
        try:
            field.clean(contact, None)
        except ValidationError:
            continue
        else:
            return contact_type
    raise ValidationError('Not a valid contact')


def get_model_for_contact(contact):
    get_contact_type(contact)
    return OTPModelMixin
