# coding=utf-8
from django.apps import AppConfig

__author__ = '¶¡rañha'


class OTPAppConfig(AppConfig):
    name = 'otp'

    def ready(self):
        self.module.auto_discover_otp_models()
