from functools import wraps

from django.contrib.auth.views import redirect_to_login
from django.utils.decorators import available_attrs

__author__ = 'vinay'


_default_nid = object()


def otp_login_required(function=None, otp_login_view=None, nid=_default_nid):
    """
    Decorator for views that checks that the user is logged in via OTP,
    redirecting to the log-in page or rendering the login-page if necessary.
    """
    from core.views import login_otp
    if otp_login_view is None:
        otp_login_view = login_otp

    def decorator(view):
        @wraps(view, assigned=available_attrs(view))
        def inner(request, *args, **kwargs):
            if request.otp_user.is_authenticated():
                return view(request, *args, **kwargs)
            if nid == _default_nid:
                return otp_login_view(request)
            return otp_login_view(request, nid=nid)
        return inner
    if function:
        return decorator(function)
    return decorator
