from __future__ import unicode_literals

from django.utils.functional import SimpleLazyObject

from otp import AnonymousOTPUser, OTPModelMixin

__author__ = 'vinay'


def _get_otp_user(request):
    tracker = request.TRACKER
    otp_user = None
    try:
        otp_login_data = tracker.extra['otp_login']
    except KeyError:
        pass
    else:
        try:
            contact = otp_login_data['contact']
        except (KeyError, LookupError):
            tracker.flush_otp_login()
        else:
            otp_user = OTPModelMixin(
                dummy=True,
                contact=contact,
                tracker=tracker
            )

    return otp_user or AnonymousOTPUser()


def get_otp_user(request):
    if not hasattr(request, '_cache_otp_user'):
        request._cache_otp_user = _get_otp_user(request)
    return request._cache_otp_user


class OTPMiddleware(object):

    def process_request(self, request):
        # Should be placed after putils.TrackMiddleware
        assert hasattr(request, 'TRACKER')
        request.otp_user = SimpleLazyObject(lambda: get_otp_user(request))
