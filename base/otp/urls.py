# coding=utf-8
from django.conf.urls import url

from otp import views

__author__ = '¶¡rañha'


urlpatterns = [
    url(r'^send/', views.send_otp, name='send'),
    url(r'^verify/', views.verify_otp, name='verify'),
    url(r'^logout/', views.otp_logout, name='logout'),
]
