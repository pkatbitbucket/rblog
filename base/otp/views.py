from __future__ import unicode_literals
import urlparse

from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME as REDIRECT_NAME
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import resolve_url
from django.utils.http import is_safe_url
from django.views.decorators.http import require_POST

from otp import otp_login as auth_login
from otp import otp_logout as auth_logout
from otp import get_model_for_contact


@require_POST
def send_otp(request):
    success = False
    status = 200
    message = []

    if not request.is_ajax():
        return JsonResponse({'success': success}, status=400)

    tracker = request.TRACKER
    try:
        contact = request.POST['contact']
        model = get_model_for_contact(contact)
    except KeyError:
        status = 400
    except ValidationError as e:
        message = e.messages
    else:
        nid = request.POST.get('nid')
        instance = model.get_or_create_contact(tracker, contact)
        success = instance.send_verification_code(nid=nid)
        if not success:
            message = ['Unable to send OTP']
    return JsonResponse({'success': success, 'msg': message}, status=status)


@require_POST
def verify_otp(request):
    success = False
    status = 200
    message = []

    tracker = request.TRACKER
    try:
        contact = request.POST['contact']
        otp = request.POST['otp']
        model = get_model_for_contact(contact)
    except KeyError:
        status = 400
    except ValidationError as e:
        message = e.messages
    else:
        try:
            instance = model.get_contact(tracker, contact)
        except (model.DoesNotExist, model.MultipleObjectsReturned):
            success = False
            message = ['Unable to verify OTP']
        else:
            success = instance.check_verification_code(otp)
            redirect_to = request.POST.get(REDIRECT_NAME,
                                           request.GET.get(REDIRECT_NAME, ''))
            if not redirect_to and request.is_ajax():
                referer = request.META.get('HTTP_REFERER')
                if referer:
                    q_dict = dict(
                        urlparse.parse_qsl(urlparse.urlparse(referer).query)
                    )
                    if REDIRECT_NAME in q_dict:
                        redirect_to = q_dict[REDIRECT_NAME]
            if success:
                # Ensure the user-originating redirection url is safe.
                if not is_safe_url(url=redirect_to, host=request.get_host()):
                    redirect_to = resolve_url(
                        urlparse.urljoin(
                            settings.SITE_URL, settings.OTP_LOGIN_REDIRECT_URL
                        )
                    )

                auth_login(request, instance)

                # redirect if the request is not ajax
                if not request.is_ajax():
                    return HttpResponseRedirect(redirect_to)

                return JsonResponse({'success': success,
                                     'redirect_to': redirect_to})

    return JsonResponse({'success': success, 'msg': message}, status=status)


def otp_logout(request):
    """
    Logs out the user and redirects to the valid next or home page.
    """
    auth_logout(request)

    next_page = None
    if REDIRECT_NAME in request.POST or REDIRECT_NAME in request.GET:
        next_page = request.POST.get(REDIRECT_NAME,
                                     request.GET.get(REDIRECT_NAME))
        # Security check -- don't allow redirection to a different host.
        if not is_safe_url(url=next_page, host=request.get_host()):
            referer = request.META.get('HTTP_REFERER', '/')
            next_page = referer

    if next_page:
        return HttpResponseRedirect(next_page)

    return HttpResponseRedirect('/')
