from django import forms
from payment.models import ChequePayment, Mswipe
from django.core.exceptions import ValidationError
from django.shortcuts import get_object_or_404


class ChequePaymentForm(forms.ModelForm):

    class Meta:
        model = ChequePayment
        fields = ['cheque_image', 'cheque_number', 'bank_name']

    def clean(self, *args, **kwargs):
        cleaned_data = super(ChequePaymentForm, self).clean(*args, **kwargs)
        # assert False, cleaned_data
        if(cleaned_data.get('cheque_image', None) is None and
            (cleaned_data.get('cheque_number', None) is None or
                len(cleaned_data.get('bank_name', '')) == 0)):
                raise ValidationError("Either upload the cheque image or enter the cheque \
                    number and bank name")
        return self.cleaned_data

    def save(self, *args, **kwargs):
        instance = get_object_or_404(ChequePayment, uuid=kwargs.get('uuid'))
        data = self.cleaned_data
        if data.get('cheque_image', False):
            instance.cheque_image = data['cheque_image']
        else:
            instance.cheque_number = data['cheque_number']
            instance.bank_name = data['bank_name']
        instance.cheque_status = ChequePayment.CHEQUE_RECIEVED
        instance.status = ChequePayment.THIRD_PARTY
        instance.save()
        return instance


class MSwipeForm(forms.ModelForm):
    class Meta:
        model = Mswipe
        fields = ['response']

    def save(self, *args, **kwargs):
        instance = get_object_or_404(Mswipe, uuid=kwargs.get('uuid'))
        data = self.cleaned_data
        instance.response = data['response']
        instance.save()
        return instance
