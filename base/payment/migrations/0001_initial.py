# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_auto_20151109_2107'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChequePayment',
            fields=[
                ('payment_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.Payment')),
                ('cheque_image', models.ImageField(null=True, upload_to=b'cheque_image', blank=True)),
                ('cheque_number', models.IntegerField(null=True, blank=True)),
                ('bank_name', models.CharField(max_length=50, null=True, blank=True)),
                ('cheque_status', models.CharField(default=b'cheque_pending', max_length=20, choices=[(b'cheque_pending', b'Cheque is yet to be retrieved'), (b'cheque_recieved', b'Cheque collected from the user'), (b'cheque_deposited', b'Cheque deposited at the bank'), (b'cheque_failed', b'Cheque failed to be cashed'), (b'cheque_cleared', b'Cheque cleared')])),
            ],
            options={
                'abstract': False,
            },
            bases=('core.payment',),
        ),
        migrations.CreateModel(
            name='Mswipe',
            fields=[
                ('payment_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.Payment')),
                ('request', jsonfield.fields.JSONField(default={})),
                ('response', jsonfield.fields.JSONField(default={})),
            ],
            options={
                'abstract': False,
            },
            bases=('core.payment',),
        ),
        migrations.CreateModel(
            name='OnlinePayment',
            fields=[
                ('payment_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.Payment')),
                ('request', jsonfield.fields.JSONField(default={})),
                ('response', jsonfield.fields.JSONField(default={})),
                ('code', models.CharField(max_length=100, blank=True)),
                ('reason', models.TextField(blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('core.payment',),
        ),
    ]
