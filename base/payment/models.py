from django.db import models
from core.models import Payment
from jsonfield import JSONField


class ChequePayment(Payment):
    CHEQUE_PENDING = "cheque_pending"
    CHEQUE_RECIEVED = "cheque_recieved"
    CHEQUE_DEPOSITED = 'cheque_deposited'
    CHEQUE_FAILED = 'cheque_failed'
    CHEQUE_CLEARED = 'cheque_cleared'
    CHEQUE_STATUS = (
        (CHEQUE_PENDING, 'Cheque is yet to be retrieved'),
        (CHEQUE_RECIEVED, 'Cheque collected from the user'),
        (CHEQUE_DEPOSITED, 'Cheque deposited at the bank'),
        (CHEQUE_FAILED, 'Cheque failed to be cashed'),
        (CHEQUE_CLEARED, 'Cheque cleared')
    )
    cheque_image = models.ImageField(
        upload_to="cheque_image", blank=True, null=True)
    cheque_number = models.IntegerField(blank=True, null=True)
    bank_name = models.CharField(max_length=50, blank=True, null=True)
    cheque_status = models.CharField(
        choices=CHEQUE_STATUS, max_length=20, default=CHEQUE_PENDING)

    def __unicode__(self):
        name = str(self.amount) + "---" + self.get_cheque_status_display()
        if self.cheque_image:
            name += str(self.cheque_image)
        elif self.cheque_number:
            name += " Cheque Number:" + str(self.cheque_number)
        return name


class OnlinePayment(Payment):
    request = JSONField(default={})
    response = JSONField(default={})
    code = models.CharField(max_length=100, blank=True)
    reason = models.TextField(blank=True)


class Mswipe(Payment):
    request = JSONField(default={})
    response = JSONField(default={})

    def __unicode__(self):
        return "Mswipe"


class PayoutBase(models.Model):
    '''
        This is the base payout model which is inherited by dealers and dealerships.
        The model stores details of how and when to pay the dealer.
    '''
    WEEKLY = "Weekly"
    BI_WEEKLY = "Bi Weekly"
    MONTHLY = "Monthly"
    PAYOUT_FREQUENCY = (
        (WEEKLY, WEEKLY),
        (BI_WEEKLY, BI_WEEKLY),
        (MONTHLY, MONTHLY),
    )
    account_number = models.CharField(max_length=30, blank=True, null=True)
    account_name = models.CharField(max_length=100, blank=True, null=True)
    ifsc_code = models.CharField(max_length=25, blank=True, null=True)
    commission_percentage = models.FloatField(default=0.0)
    payout_frequency = models.CharField(choices=PAYOUT_FREQUENCY, max_length=9, default=MONTHLY)
    is_pre_deducted = models.BooleanField(default=False)
    is_approved = models.BooleanField(default=False)

    class Meta:
        abstract = True

    def is_populated(self):
        if self.account_number is None or self.commission_percentage == 0.0:
            return False
        return True
