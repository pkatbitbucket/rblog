from payment import models


def create_payment(
        user, requirement, payment_type='online', amount=0.0):
    '''
        This method will take a user, requirement instance,
        payment_type(cheque, online, mswipe), amount and will generate a payment
        instance and bind it to the associated requirement
    '''
    if not user.inspectionagent_set.all() and not requirement.user == user:
        return {"error": "The user passed is not the same as the user of requirement"}
    if not amount:
        return {"error": "Pass the amount to be deducted"}
    elif amount <= 0:
        return {"error": "The amount to be deducted should be a positive non 0 value"}
    if payment_type == 'online':
        payment_instance = models.OnlinePayment()
    elif payment_type == 'cheque':
        payment_instance = models.ChequePayment()
    elif payment_type == 'mswipe':
        payment_instance = models.Mswipe()
    else:
        return {"error": "Invalid Payment type"}
    payment_instance.amount = amount
    payment_instance.user = user
    payment_instance.save()
    requirement.payment.add(payment_instance)
    return payment_instance


def upload_cheque(payment_instance, **kwargs):
    if not type(payment_instance) == models.ChequePayment:
        return {"error": "Passed payment is not a valid cheque payment"}
    if 'cheque_image' in kwargs:
        payment_instance.cheque_image = kwargs['cheque_image']
    else:
        if 'cheque_number' not in kwargs or 'bank_name' not in kwargs:
            return {"error": "Please pass either the 'cheque_image' or 'cheque_number' and 'bank_name'"}
        payment_instance.cheque_number = kwargs['cheque_number']
        payment_instance.bank_name = kwargs['bank_name']
    payment_instance.cheque_status = models.ChequePayment.CHEQUE_RECIEVED
    payment_instance.save()
    return payment_instance


def cheque_deposited(payment_instance):
    if not type(payment_instance) == models.ChequePayment:
        return {"error": "Passed payment is not a valid cheque payment"}
    if payment_instance.cheque_status == models.ChequePayment.CHEQUE_RECIEVED:
        payment_instance.cheque_status = models.ChequePayment.CHEQUE_DEPOSITED
    payment_instance.save()
    return payment_instance


def cheque_failed(payment_instance):
    if not type(payment_instance) == models.ChequePayment:
        return {"error": "Passed payment is not a valid cheque payment"}
    if payment_instance.cheque_status == models.ChequePayment.CHEQUE_DEPOSITED:
        payment_instance.cheque_status = models.ChequePayment.CHEQUE_FAILED
    payment_instance.save()
    return payment_instance


def cheque_cleared(payment_instance):
    if not type(payment_instance) == models.ChequePayment:
        return {"error": "Passed payment is not a valid cheque payment"}
    if payment_instance.cheque_status == models.ChequePayment.CHEQUE_DEPOSITED:
        payment_instance.cheque_status = models.ChequePayment.CHEQUE_CLEARED
    payment_instance.save()
    return payment_instance
