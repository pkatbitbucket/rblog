# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_transaction_id', models.BigIntegerField(db_index=True)),
                ('_transaction_class', models.CharField(max_length=250)),
                ('status', models.CharField(default='PENDING', max_length=20, verbose_name='status', choices=[('PENDING', 'Pending'), ('FAILED', 'Failed'), ('SUCCESS', 'Success')])),
                ('raw', jsonfield.fields.JSONField(default={}, verbose_name='raw', blank=True)),
                ('reference_id', models.CharField(max_length=250, blank=True)),
                ('payment_id', django_extensions.db.fields.UUIDField(unique=True, editable=False, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='PaymentGateway',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.SlugField(max_length=70, verbose_name='slug')),
                ('title', models.CharField(max_length=200, verbose_name='title')),
                ('body', models.TextField(verbose_name='body', blank=True)),
                ('is_active', models.BooleanField(default=False)),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='created')),
                ('updated_on', models.DateTimeField(auto_now=True, verbose_name='modified')),
            ],
        ),
        migrations.CreateModel(
            name='Request',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', jsonfield.fields.JSONField(default={}, verbose_name='data', blank=True)),
                ('type', models.CharField(default='BROWSER', max_length=20, verbose_name='request type', choices=[('BROWSER', 'Broswer'), ('S2SQUERY', 'S2SQuery')])),
                ('payment', models.ForeignKey(to='payment_gateway.Payment')),
            ],
        ),
        migrations.CreateModel(
            name='Response',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', jsonfield.fields.JSONField(default={}, verbose_name='data', blank=True)),
                ('type', models.CharField(default='BROWSER', max_length=20, verbose_name='response type', choices=[('BROWSER', 'Broswer'), ('S2S', 'S2S'), ('S2SQUERY', 'S2SQuery')])),
                ('payment', models.ForeignKey(to='payment_gateway.Payment')),
            ],
        ),
        migrations.AddField(
            model_name='payment',
            name='payment_gateway',
            field=models.ForeignKey(to='payment_gateway.PaymentGateway'),
        ),
        migrations.AlterUniqueTogether(
            name='payment',
            unique_together=set([('_transaction_id', '_transaction_class')]),
        ),
    ]
