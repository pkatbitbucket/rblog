# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payment_gateway', '0002_add_billdesk'),
    ]

    operations = [
        migrations.RenameField(
            model_name='payment',
            old_name='_transaction_class',
            new_name='_transaction_app_label',
        ),
        migrations.AddField(
            model_name='payment',
            name='_transaction_model_name',
            field=models.CharField(max_length=250),
        ),
        migrations.AlterUniqueTogether(
            name='payment',
            unique_together=set([('_transaction_id', '_transaction_app_label', '_transaction_model_name')]),
        ),
    ]
