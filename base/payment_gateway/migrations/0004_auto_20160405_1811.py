# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payment_gateway', '0003_auto_20160405_1518'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='payment',
            unique_together=set([]),
        ),
    ]
