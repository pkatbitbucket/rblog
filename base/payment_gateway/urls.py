""" URLs setting for payment gateways """
from django.conf.urls import url

urlpatterns = [
    url(r'^(?P<gateway>\w+)/response/$',
        'payment_gateway.views.response',
        name='pg-response'),

    url(r'^(?P<gateway>\w+)/s2sresponse/$',
        'payment_gateway.views.s2s_response',
        name='pg-s2s-response'),
]
