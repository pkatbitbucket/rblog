"""Payment-gateway controllers"""
from __future__ import absolute_import
import json
import traceback
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import never_cache
from payment_gateway.gateways.billdesk import Billdesk
from utils import payment_logger
from motor_product import prod_utils
from payment_gateway.models import Payment


@never_cache
@csrf_exempt
def response(request, gateway):
    request_data = request.POST.dict() or request.GET.dict()
    payment_logger.info(json.dumps({
        'type': 'response',
        'gateway': gateway,
        'medium': 'browser',
        'data': request_data
    }))

    payment_id = Billdesk.parse_response(request_data)['CustomerID']
    payment = Payment.objects.get(payment_id=payment_id)
    transaction = payment.transaction
    payment.response_set.create(data=request_data)
    insurer_slug = transaction.insurer.slug
    config = Billdesk.get_config(insurer_slug)
    billdesk = Billdesk(request_data, config)
    vehicle_type = prod_utils.VEHICLE_TYPE_SLUG[transaction.vehicle_type]
    if billdesk.validate_checksum():
        if transaction.insured_details_json.get('s2sresponse', None):
            is_redirect = transaction.insured_details_json['s2sresponse']['is_redirect']
            redirect_url = transaction.insured_details_json['s2sresponse']['redirect_url']
            template_name = transaction.insured_details_json['s2sresponse']['template_name']
            data = transaction.insured_details_json['s2sresponse']['data']

        else:
            # billdesk.get_s2s_query_response()
            response = billdesk.get_response_data()
            transaction.payment_response = response
            transaction.save()
            try:
                is_redirect, redirect_url, template_name, data = prod_utils.process_payment_response(
                    request, vehicle_type, insurer_slug, response, transaction.transaction_id)
            except:
                stacktrace = traceback.format_exc()
                payment_logger.info(stacktrace)
                return HttpResponseRedirect('/motor/' + vehicle_type + '/product/failure/')

        if is_redirect:
            return HttpResponseRedirect(redirect_url)
        else:
            return render(request, template_name, {
                'data': data,
                'redirect_url': redirect_url,
            })
    else:
        payment_logger.info(json.dumps({
            'type': 'response',
            'gateway': gateway,
            'medium': 'browser',
            'transaction': transaction.transaction_id,
            'data': request_data,
            'message': 'INVALID CHECKSUM'
        }))
        return HttpResponseRedirect('/motor/' + vehicle_type + '/product/failure/')


@never_cache
@csrf_exempt
def s2s_response(request, gateway):
    request_data = request.POST.dict() or request.GET.dict()
    payment_logger.info(json.dumps({
        'type': 'response',
        'gateway': gateway,
        'medium': 'server-to-server',
        'data': request_data,
        'body': request.body
    }))
    try:
        payment_id = Billdesk.parse_response(request_data)['CustomerID']
    except KeyError:
        return HttpResponse(json.dumps({'success': False, 'message': 'INVALID_DATA'}), status=400)
    payment = Payment.objects.get(payment_id=payment_id)
    transaction = payment.transaction
    payment.response_set.create(data=request_data, type='S2S')
    insurer_slug = transaction.insurer.slug
    config = Billdesk.get_config(insurer_slug)
    billdesk = Billdesk(request_data, config)
    vehicle_type = prod_utils.VEHICLE_TYPE_SLUG[transaction.vehicle_type]
    if billdesk.validate_checksum():
        response = billdesk.get_response_data()
        if transaction.status == 'COMPLETED':
            transaction.insured_details_json['s2sresponse'] = {'s2sresponse': response}
            transaction.save(update_fields=['insured_details_json'])
            return HttpResponse(json.dumps({
                'success': True,
                'message': 'DELAYED',
                'TxnReferenceNo': response.get('payment_id', None)
            }), status=208)
        transaction.payment_response = response
        transaction.save()
        try:
            is_redirect, redirect_url, template_name, data = prod_utils.process_payment_response(
                request, vehicle_type, insurer_slug, response, transaction.transaction_id)
            transaction.insured_details_json['s2sresponse'] = {
                'is_redirect': is_redirect,
                'redirect_url': redirect_url,
                'template_name': template_name,
                'data': data
            }
            transaction.save(update_fields=['insured_details_json'])
        except:
            stacktrace = traceback.format_exc()
            payment_logger.error(stacktrace)
        finally:
            return HttpResponse(json.dumps({
                'success': True,
                'message': 'SUCCESS',
                'TxnReferenceNo': response.get('payment_id', None)
            }))
    else:
        payment_logger.info(json.dumps({
            'type': 'response',
            'gateway': gateway,
            'medium': 'server-to-server',
            'transaction': transaction.transaction_id,
            'data': request_data,
            'message': 'INVALID CHECKSUM'
        }))
        return HttpResponse(json.dumps({'success': False, 'message': 'INVALID_CHECKSUM'}), status=401)
