from collections import OrderedDict
import os
import logging
import sys
import thread
import json
import string
import StringIO
import ConfigParser
import base64
import hashlib
import hmac
import binascii
import inspect
import importlib
import re
import pprint
from operator import attrgetter
from random import shuffle
from functools import wraps
import zlib
import time
import errno
import datetime
from django.core.files.storage import default_storage
from django.contrib.sessions.backends.base import CreateError
import pdfkit
import boto
from django.db import models
from django.apps.registry import apps
from django.conf import settings
from django.contrib.contenttypes.models import ContentType

from django.db.models import Count, Q
from django.http import HttpResponse
from django.db import connection
from django.http.cookie import http_cookies
from django.http.response import JsonResponse
from django.template import Context
from django.template.loader import get_template
from django.utils.decorators import available_attrs
import xlrd
import cookies
from xlwt import Workbook, XFStyle
from xlutils.copy import copy as xlcopy
from boto.s3.key import Key
from Crypto import Random
from Crypto.Cipher import AES
from tagging.models import Tag

from celery import states, current_app
from celery.result import AsyncResult
from celery.utils import get_full_cls_name
from celery.utils.encoding import safe_repr
from blog.models import CfblogContent, CfblogCategory
import kissmetrics
from core.models import Activity
from seo.models import SEOMetadata
from wiki.models import (
    MetaData, MedicalTransactionPermission, MedicalCondition,
    Hospital, Pincode, City, Slab
)
from health_product.decorators import cache_memoized_func
from health_product.queries import get_nearest_pincode
from wiki.models import AttributeDict

logger = logging.getLogger(__name__)

# monkey patch cookies to disable invalid cookie errors
def suppress_errors(error):
    pass

cookies._report_invalid_cookie = suppress_errors
cookies._report_invalid_attribute = suppress_errors
cookies._report_unknown_attribute = suppress_errors

from mail_utils import health_alert


email_regex = '([A-Z0-9]+(?:[+._-]?[A-Z0-9]+)*)@((?:(?:[A-Z0-9-]+\.)*\w[A-Z0-9-]{0,66})\.(?:[A-Z]{2,6}(?:\.[A-Z]{2})?))'
exact_email_regex = '^{}$'.format(email_regex)

email_re = re.compile(email_regex, re.IGNORECASE)
exact_email_re = re.compile(exact_email_regex, re.IGNORECASE)


def get_meta(slabs, content_type):
    mdata_list = [MetaData.objects.filter(
        slabs=slab, content_type=content_type) for slab in slabs]
    if len(set(mdata_list)) != len(mdata_list):
        raise Exception(
            "Error! The selected slabs do not have the same Meta Data")
    mdata = mdata_list[0]
    if mdata:
        mdata = mdata[0]
    else:
        mdata = None
    return mdata


def get_correlated_slabs(product, wiki_model_string, attr):
    # M = apps.get_model('wiki', wiki_model_string)

    slabs = Slab.objects.filter(health_product=product).exclude(**{attr: None}).values('id', attr + '__id')
    de = {}
    for s in slabs:
        key = s[attr + '__id']
        de.setdefault(key, []).append(s['id'])

    return list(set([tuple(map(lambda x: str(x), l)) for l in de.values()]))


def create_standard_response(product, wiki_model_string, slabs):
    wiki_model_string = wiki_model_string
    M = apps.get_model('wiki', wiki_model_string)
    content_type = ContentType.objects.get_for_model(M)
    mdata = get_meta(slabs, content_type)

    # to find the key given value in dict
    attr = ""
    for a1, a2 in AttributeDict.items():
        if a2 == wiki_model_string:
            attr = a1

    return {
        'success': True,
        'corr_list': get_correlated_slabs(product, wiki_model_string, attr),
        'comments': mdata.comments,
        'verbose': mdata.verbose,
        'is_completed': mdata.is_completed,
        'code': mdata.code,
    }


@cache_memoized_func
def update_text_to_display(slab_score):
    md_dict = dict(
        ((sl, md.content_type.model), md)
        for md in MetaData.objects.filter(
            slabs__in=slab_score.keys()
        ).distinct().select_related('content_type__model').prefetch_related('slabs')
        for sl in md.slabs.all()
    )

    for slab, slab_data in slab_score.items():
        for attr, attr_data in slab_data:
            instance = attr_data.get('model')
            if instance:
                md = md_dict.get(
                    (
                        slab,
                        instance.__class__.__name__.lower()
                    )
                )
                if md and md.text_to_display:
                    config = StringIO.StringIO()
                    config.write('[dummysection]\n')
                    config.write(md.text_to_display)
                    config.seek(0, os.SEEK_SET)
                    cp = ConfigParser.ConfigParser()
                    cp.readfp(config)
                    try:
                        attr_data['verbose'] = cp.get('dummysection', 'value')
                    except ConfigParser.NoOptionError:
                        pass
                    try:
                        attr_data['verbose'] = cp.get(
                            'dummysection', 'verbose')
                    except ConfigParser.NoOptionError:
                        pass
                    finally:
                        config.close()


def save_meta_form(obj, slabs, data):
    content_type = ContentType.objects.get_for_model(obj)
    update_dict = {
        'code': data.get('code', ''),
        'verbose': data.get('verbose', ''),
        'text_to_display': data.get('text_to_display', ''),
        'comments': data.get('comments', ''),
        'is_completed': data.get('is_completed', False),
    }
    mdata = get_meta(slabs, content_type)
    if mdata:
        # print "MDATA", mdata, update_dict
        MetaData.objects.filter(id=mdata.id).update(**update_dict)
    else:
        mdata = MetaData(
            content_type=content_type,
            **update_dict
        )
        mdata.save()
        mdata.slabs.add(*slabs)
    return True


def context_processor(request):
    d = {
        'categories': CfblogCategory.objects.all(),
        'currenttime': datetime.datetime.now(),
        'static_path': settings.STATIC_URL,
        'cdn_static_path': settings.STATIC_URL,
        'default_title': settings.SITE_TITLE,
        'default_description': settings.SITE_DESCRIPTION,
        'default_keywords': settings.SITE_KEYWORDS,
        'default_site_name': settings.SITE_NAME,
        'default_site_url': settings.SITE_URL,
        'default_insurer_list': [2, 11, 13, 6, 1, 14, 9, 10],
        'settings': settings
    }
    return d


@cache_memoized_func
def prepare_post_data_to_score(data):

    try:
        if data['hospital_service'] == "BASIC":
            service_type = "SHARED"
        elif data['hospital_service'] == "PRIVATE_HIGH_END":
            service_type = "PVT_HIGH_END"
        else:   # if data['hospital_service'] == "PRIVATE_ANY":
            service_type = "PVT"
    except KeyError:
        service_type = 'PVT'

    spouse_age = None
    kid_ages = []
    parent_ages = []
    you_and_spouse_ages = []

    kid_genders = []
    parent_genders = []
    you_and_spouse_genders = []

    self_age = None
    self_gender = None

    for m in data['members']:
        m['person'] = 'you' if m['person'].lower() == 'self' else m['person']

    if len(data['members']) == 1:
        m = data['members'][0]
        family = False
        kids = 0
        adults = 1
        parents = 0
        you_and_spouse = 0
        is_self_insured = False
        max_age = m['age']
        self_age = m['age']
        if m['person'].lower() in ['you', 'self']:
            gender = data['gender']
            is_self_insured = True
            if m['person'] == 'you':
                self_gender = gender
        if m['person'].lower() == 'spouse':
            if data['gender'] == 'MALE':
                gender = 'FEMALE'
            else:
                gender = 'MALE'
        if m['person'].lower() in ['son', 'father']:
            gender = 'MALE'
        if m['person'].lower() in ['daughter', 'mother']:
            gender = 'FEMALE'

    else:
        family = True
        kids = 0
        adults = 0
        parents = 0
        you_and_spouse = 0
        is_self_insured = False
        max_age = 0
        for m in data['members']:
            m_person = m['person'].lower()
            age = int(m['age'])
            age_in_months = int(m.get('age_in_months', 0) or 0)

            # Find the current member's gender
            if m_person == 'you':
                m_gender = data['gender']
                self_gender = m_gender
                is_self_insured = True
            elif m_person == 'spouse':
                if data['gender'] == 'MALE':
                    m_gender = 'FEMALE'
                else:
                    m_gender = 'MALE'
            elif m_person in ['son', 'father']:
                m_gender = 'MALE'
            elif m_person in ['daughter', 'mother']:
                m_gender = 'FEMALE'

            # Treat eldest member as primary member
            if max_age < age:
                max_age = age
                gender = m_gender

            # Count and assign members in each generation their ages, genders
            if m_person in ['son', 'daughter']:
                kid_ages.append(age or age_in_months / 1000.0)
                kid_genders.append(m_gender)
                kids += 1
            elif m_person in ['father', 'mother']:
                parents += 1
                adults += 1
                parent_ages.append(age)
                parent_genders.append(m_gender)
            elif m_person in ['you', 'spouse']:
                adults += 1
                you_and_spouse += 1
                you_and_spouse_ages.append(age)
                you_and_spouse_genders.append(m_gender)
                if m_person == 'you':
                    self_age = age
                else:
                    spouse_age = age

    mdata = {
        'kids': kids,
        'adults': adults,
        'parents': parents,
        'parent_ages': parent_ages,
        'parent_genders': parent_genders,
        'you_and_spouse': you_and_spouse,
        'you_and_spouse_ages': you_and_spouse_ages,
        'you_and_spouse_genders': you_and_spouse_genders,
        'is_self_insured': is_self_insured,
        'family': family,
        'gender': gender,
        'age': max_age,
        'self_age': self_age,
        'self_gender': self_gender,
        'spouse_age': spouse_age,
        'sum_assured': data.get('sum_assured', ''),
        'deductible': data.get('deductible', ''),
        'kid_ages': kid_ages,
        'kid_genders': kid_genders,
        'service_type': service_type,
        'pincode': data['pincode'],
        'city': data.get('city'),
    }

    if not mdata.get('city'):
        mdata['city'] = get_pincode_city_details(mdata['pincode'])

    mdata['hospitals'] = []
    if not mdata.get('sum_assured'):
        mdata['sum_assured'] = GetSumAssured(mdata).get_sa()

    return mdata


@cache_memoized_func
def prepare_multiset_post_data(data):
    """ data sample: {
        u'hospital_service': u'private_any',
        u'pincode': 400053,
        u'parents_pincode': 400051,
        u'members': [
            {
                u'dob': u'01-01-2015',
                u'person': u'you',
                u'verbose': u'you'
            },
            {
                u'dob': u'04-04-2012',
                u'person': u'daughter',
                u'id': u'daughter1',
                u'verbose': u'daughter'
            },
            {
                u'dob': u'17-02-2000',
                u'person': u'son',
                u'id': u'son2',
                u'verbose': u'son'
            }
        ]
    }

    this method converts the above into the multiple sets keeping the same
    structure intact
    """
    parents = []
    family = []

    results_set = []
    for m in data['members']:
        if m['person'].lower() in ['you', 'spouse', 'son', 'daughter']:
            family.append(m)
        if m['person'].lower() in ['father', 'mother']:
            parents.append(m)

    if len(parents) > 0 and len(family) > 0:
        parents_set = data.copy()
        parents_set['members'] = parents
        parents_set['pincode'] = data.get('parents_pincode', data['pincode'])
        parents_data_to_score = prepare_post_data_to_score(parents_set)
        parents_set['metro'] = parents_data_to_score['city']['type']
        parents_set['city'] = parents_data_to_score['city']
        parents_set['sum_assured'] = parents_data_to_score['sum_assured']
        results_set.append({'type': 'parents', 'uidata': parents_set,
                            'data_to_score': parents_data_to_score})

        family_set = data.copy()
        family_set['members'] = family
        family_data_to_score = prepare_post_data_to_score(family_set)
        family_set['metro'] = family_data_to_score['city']['type']
        family_set['city'] = family_data_to_score['city']
        family_set['sum_assured'] = family_data_to_score['sum_assured']
        results_set.append(
            {
                'type': 'family',
                'uidata': family_set,
                'data_to_score': family_data_to_score
            }
        )

    combined_set = data.copy()
    combined_set['pincode'] = data['pincode'] if len(family) > 0 else data[
        'parents_pincode']
    combined_data_to_score = prepare_post_data_to_score(combined_set)
    combined_set['metro'] = combined_data_to_score['city']['type']
    combined_set['city'] = combined_data_to_score['city']
    combined_set['sum_assured'] = combined_data_to_score['sum_assured']
    results_set.append(
        {
            'type': 'combined',
            'uidata': combined_set,
            'data_to_score': combined_data_to_score
        }
    )

    return results_set


def get_city_hospital_method(pincode):
    newpin, near_by_pincodes = get_nearest_pincode(pincode.pincode)

    hcc_list = []
    for pobj in near_by_pincodes:
        hospitals = list(Hospital.objects.filter(pin=pobj.pincode).prefetch_related('city'))
        if len(hcc_list) > 20:
            break
        else:
            hcc_list.extend(hospitals)
            continue

    cities = [h.city for h in hcc_list if h.city.state_id == pincode.state_id and h.pin == pincode.pincode]
    if not cities:
        cities = [h.city for h in hcc_list if h.city.state_id == pincode.state_id]

    if cities:
        return max(cities, key=cities.count)

@cache_memoized_func
def get_city_and_hospitals(pincode_value):
    newpin, near_by_pincodes = get_nearest_pincode(pincode_value)
    hcc_list = []
    for pobj in near_by_pincodes:
        pin = pobj.pincode
        q = Q(pin=pin)
        hospitals = list(Hospital.objects.filter(q).prefetch_related(
            'insurers').annotate(insurers_count=Count('insurers')))
        if len(hcc_list) > 20:
            break
        else:
            hcc_list.extend(hospitals)
            continue

    if hcc_list:
        hccs = [
            {
                'name': "%s" % hcc.name,
                'address': hcc.address,
                'lat': hcc.lat,
                'lng': hcc.lng,
                'id': hcc.id,
                'insurers': [ins.id for ins in hcc.insurers.all()]
            }
            for hcc in reversed(
                sorted(hcc_list, key=attrgetter('insurers_count'))
            )
        ]

        # Finding best matched city
        pinobj = Pincode.objects.filter(pincode=pincode_value)[0]
        state = pinobj.state
        cities = list(
            City.objects.filter(
                id__in=[
                    hcc.city_id
                    for hcc in hcc_list
                    if hcc.pin == pincode_value
                ],
                state=state
            )
        )
        if not cities:
            cities = list(City.objects.filter(
                id__in=[hcc.city_id for hcc in hcc_list], state=state))

        if not cities:
            cities = list(City.objects.filter(
                id__in=[hcc.city_id for hcc in hcc_list]))
            if cities:
                health_alert('wrong pincode details', u'{}'.format(pinobj.pincode))

        pin_city = City.objects.filter(name=pinobj.city, state=state).first()
        if pin_city:
            city = pin_city
        elif cities:
            city = max(set(cities), key=cities.count)
        else:
            city = pinobj.mapped_city if pinobj.mapped_city else pinobj.state.capital

    else:
        pinobj = Pincode.objects.filter(pincode=pincode_value)[0]
        city = pinobj.mapped_city if pinobj.mapped_city else pinobj.state.capital
        hccs = []

    return {
        'hospitals': hccs,
        'city': {
            'name': city.name,
            'id': city.id,
            'type': (
                'METRO' if Pincode.objects.filter(
                    pincode=pincode_value, is_metro=True
                )
                else ""
            ),
            'lat': newpin.lat,
            'lng': newpin.lng,
        }
    }


@cache_memoized_func
def get_city_by_hospital_extrapolation(pincode_value):
    newpin, near_by_pincodes = get_nearest_pincode(pincode_value)
    hcc_list = []
    for pobj in near_by_pincodes:
        pin = pobj.pincode
        hospitals = list(Hospital.objects.filter(pin=pin).annotate(
            insurers_count=Count('insurers')).values())
        if len(hcc_list) > 20:
            break
        else:
            hcc_list.extend(hospitals)
            continue

    pinobj = newpin
    if hcc_list:
        # Finding best matched city
        state = pinobj.state
        cities = list(
            City.objects.filter(
                id__in=[
                    hcc['city_id']
                    for hcc in hcc_list
                    if hcc['pin'] == pincode_value
                ],
                state=state)
        )
        if not cities:
            cities = list(City.objects.filter(
                id__in=[hcc['city_id'] for hcc in hcc_list], state=state))
        if not cities:
            cities = list(City.objects.filter(
                id__in=[hcc['city_id'] for hcc in hcc_list]))
            if cities:
                health_alert('wrong pincode details', u'{}'.format(pinobj.pincode))

        pin_city = City.objects.filter(name=pinobj.city, state=state).first()
        if pin_city:
            city = pin_city
        elif cities:
            city = max(set(cities), key=cities.count)
        else:
            city = pinobj.mapped_city if pinobj.mapped_city else pinobj.state.capital

    else:
        city = pinobj.mapped_city if pinobj.mapped_city else pinobj.state.capital

    return {
        'name': city.name,
        'id': city.id,
        'type': (
            'METRO'
            if pinobj.is_metro is True
            else ""
        ),
        'lat': newpin.lat,
        'lng': newpin.lng,
    }


def get_pincode_city_details(pincode):
    pinobj = Pincode.objects.filter(pincode=pincode)[0]
    pin_city = City.objects.filter(name=pinobj.city, state=pinobj.state).first()
    if pin_city:
        city = pin_city
    else:
        city = pinobj.mapped_city if pinobj.mapped_city else pinobj.state.capital
    return {
        'name': city.name,
        'id': city.id,
        'type': (
            'METRO'
            if pinobj.is_metro is True
            else ""
        ),
        'lat': pinobj.lat,
        'lng': pinobj.lng,
    }


class GetSumAssured():
    segment_dict = OrderedDict({
        tuple(range(18, 31)): OrderedDict({'base_cover': 100000, 'S': 50000, 'S_DP': 50000, 'M': 100000,
                                           'M_DP': 150000, 'M_DC': 200000, 'M_DP_DC': 300000, 'SHARED': 0,
                                           'PVT': 50000, 'PVT_HIGH_END': 100000, 'METRO': 50000}),

        tuple(range(31, 41)): OrderedDict({'base_cover': 100000, 'S': 50000, 'S_DP': 100000, 'M': 100000,
                                           'M_DP': 150000, 'M_DC': 200000, 'M_DP_DC': 300000, 'SHARED': 0,
                                           'PVT': 50000, 'PVT_HIGH_END': 100000, 'METRO': 50000}),

        tuple(range(41, 46)): OrderedDict({'base_cover': 150000, 'S': 50000, 'S_DP': 100000, 'M': 150000,
                                           'M_DP': 200000, 'M_DC': 200000, 'M_DP_DC': 300000, 'SHARED': 0,
                                           'PVT': 100000, 'PVT_HIGH_END': 150000, 'METRO': 50000}),

        tuple(range(46, 51)): OrderedDict({'base_cover': 200000, 'S': 50000, 'S_DP': 150000, 'M': 150000,
                                           'M_DP': 200000, 'M_DC': 250000, 'M_DP_DC': 350000, 'SHARED': 0,
                                           'PVT': 100000, 'PVT_HIGH_END': 150000, 'METRO': 50000}),

        tuple(range(51, 56)): OrderedDict({'base_cover': 250000, 'S': 50000, 'S_DP': 150000, 'M': 200000,
                                           'M_DP': 200000, 'M_DC': 250000, 'M_DP_DC': 350000, 'SHARED': 0,
                                           'PVT': 100000, 'PVT_HIGH_END': 150000, 'METRO': 50000}),

        tuple(range(56, 61)): OrderedDict({'base_cover': 250000, 'S': 50000, 'S_DP': 200000, 'M': 200000,
                                           'M_DP': 250000, 'M_DC': 300000, 'M_DP_DC': 500000, 'SHARED': 0,
                                           'PVT': 100000, 'PVT_HIGH_END': 150000, 'METRO': 50000}),

        tuple(range(61, 66)): OrderedDict({'base_cover': 300000, 'S': 50000, 'S_DP': 200000, 'M': 200000,
                                           'M_DP': 250000, 'M_DC': 300000, 'M_DP_DC': 500000, 'SHARED': 0,
                                           'PVT': 100000, 'PVT_HIGH_END': 150000, 'METRO': 50000}),

        tuple(range(66, 71)): OrderedDict({'base_cover': 350000, 'S': 50000, 'S_DP': 200000, 'M': 250000,
                                           'M_DP': 300000, 'M_DC': 350000, 'M_DP_DC': 500000, 'SHARED': 0,
                                           'PVT': 100000, 'PVT_HIGH_END': 150000, 'METRO': 50000}),

        tuple(range(71, 120)): OrderedDict({'base_cover': 400000, 'S': 50000, 'S_DP': 250000, 'M': 250000,
                                            'M_DP': 300000, 'M_DC': 350000, 'M_DP_DC': 500000, 'SHARED': 0,
                                            'PVT': 100000, 'PVT_HIGH_END': 150000, 'METRO': 50000}),
    })

    """
    Data in the following format:
    data = {
        'sum_assured' : 200000,
        'age': '28',
        'city' : 'M_MUMBAI',
        'service_type' : 'SHARED',
        'family' : True,
        'adults' : 2,
        'kids' : 2,
    }
    """

    def __init__(self, data):
        self.sum_assured = 0
        self.filter_list = []
        self.segment = []
        self.sa_dict = {}
        self.data = data
        self.dependency = 'S'

    def get_segment(self):
        if 'age' not in self.data.keys():
            return 0

        for k, v in self.segment_dict.items():
            if int(self.data['age']) in k:
                self.segment = k
                self.sa_dict = v
                self.sum_assured = v['base_cover']
        return self.sum_assured

    def get_sa_by_city(self):
        if self.data['city']['type'] == 'METRO':
            self.filter_list.append('METRO')
        return

    def get_sa_by_family(self):
        if self.data['family']:
            self.dependency = 'M'

        if 'adults' in self.data.keys():
            if self.data['adults']:
                self.dependency = '%s_DP' % self.dependency

        if 'kids' in self.data.keys():
            if self.data['kids']:
                self.dependency = '%s_DC' % self.dependency

        self.filter_list.append(self.dependency)
        return

    def get_sa_by_service_type(self):
        if self.data['service_type']:
            self.filter_list.append(self.data['service_type'])
        return

    def get_sa(self):
        seg = self.get_segment()
        if not seg:
            return 0

        for k, v in self.data.items():
            func = getattr(self, "get_sa_by_%s" % k.lower(), None)
            if func:
                func()

        for i in self.filter_list:
            self.sum_assured += self.sa_dict[i]

        self.sum_assured = int(
            100000 * round(float(self.sum_assured) / 100000))
        return self.sum_assured


def recommended_filter(n, *args):
    rlist = []
    for q_set in args:
        if q_set:
            if len(rlist) < n:
                q_set = q_set.filter(
                    ~Q(category__title='Jargon Busting'),
                    ~Q(id__in=[r.id for r in rlist])
                )
                rlist.extend(q_set)
            else:
                break

    shuffle(rlist)
    return rlist[:n]


def get_recommended_reads(n, obj=None):
    if obj:
        recommended_reads = recommended_filter(
            n,
            CfblogContent.objects.published().filter(Q(category=obj.category)),
            CfblogContent.objects.published()
        )
    else:
        recommended_reads = recommended_filter(n, CfblogContent.objects.published())
    return recommended_reads


def calculate_age(born):
    today = datetime.date.today()
    try:
        birthday = born.replace(year=today.year)
    except ValueError:
        # raised when birth date is February 29 and the current year is not a
        # leap year
        birthday = born.replace(year=today.year, day=born.day - 1)
    if birthday > today:
        return today.year - born.year - 1
    else:
        return today.year - born.year


def save_seo(obj, *args, **kwargs):
    """
    Takes a model instance for which it needs to save the seo information and
    dictionary of SEO model attributes
    """
    assert obj, "Object cannot be None"
    seo = get_seo(obj)
    if seo:
        keywords = kwargs.pop('keywords')
        seo = SEOMetadata.objects.filter(id=seo.id)
        seo.update(**kwargs)
        seo = seo[0]
        if keywords:
            seo.keywords.clear()
            seo.keywords.add(*keywords)
        seo.save()
        return seo
    else:
        keywords = kwargs.pop('keywords', None)
        seo = SEOMetadata(**kwargs)
        seo.content_object = obj
        seo.save()
        if keywords:
            [seo.keywords.add(kw) for kw in keywords]
        seo.save()
        return seo


def get_seo(obj=None):
    if not obj:
        return None
    content_type = ContentType.objects.get_for_model(obj)
    object_id = obj.id
    try:
        seodata = SEOMetadata.objects.get(
            content_type=content_type, object_id=object_id)
        return seodata
    except SEOMetadata.DoesNotExist:
        return None


def get_jargon_list(request):
    if request.user.is_superuser:
        tag_list = Tag.objects.usage_for_queryset(
            CfblogContent.objects.filter(category__title='Jargon Busting'))
    else:
        tag_list = Tag.objects.usage_for_queryset(
            CfblogContent.objects.published().filter(category__title='Jargon Busting'))
    tag_list = list(set(tag_list))
    return tag_list


def get_jargon_alpha_dict(request):
    alpha_dict = OrderedDict()
    for i in string.uppercase:
        alpha_dict[i] = []
    for t in get_jargon_list(request):
        try:
            alpha_dict[t.name[0].upper()].append(t)
        except:
            pass
    return alpha_dict


class XlTemplate(object):

    @classmethod
    def _getOutCell(cls, outSheet, colIndex, rowIndex):
        """ HACK: Extract the internal xlwt cell representation. """
        row = outSheet._Worksheet__rows.get(rowIndex)
        if not row:
            return None

        cell = row._Row__cells.get(colIndex)
        return cell

    @classmethod
    def _setOutCell(cls, outSheet, col, row, value):
        """ Change cell value without changing formatting. """
        previousCell = cls._getOutCell(outSheet, col, row)
        outSheet.write(row, col, value)

        if previousCell:
            newCell = cls._getOutCell(outSheet, col, row)
            if newCell:
                newCell.xf_idx = previousCell.xf_idx

    # For now only sheet index 0 can be updated
    @classmethod
    def process_template(cls, filename, data):
        rb = xlrd.open_workbook(filename, formatting_info=True)
        wb = xlcopy(rb)
        w_sheet = wb.get_sheet(0)
        r_sheet = rb.sheet_by_index(0)

        for row in range(0, r_sheet.nrows):
            for col in range(0, r_sheet.ncols):
                cellVal = r_sheet.cell(row, col).value.strip()
                if cellVal == "":
                    continue

                match = re.match('<.*?>', cellVal)
                if not match:
                    continue

                keyVal = cellVal.strip('<').strip('>')
                cls._setOutCell(w_sheet, col, row,
                                data.get(keyVal, '########'))
        return wb


def render_excel(
    filename, col_title_list, data_row_list, write_to_file=False,
    big_header=None
):
    import StringIO
    output = StringIO.StringIO()
    export_wb = Workbook()
    export_sheet = export_wb.add_sheet('Export')
    row_idx = 0

    col_idx = 0
    if big_header:
        # DO Something
        for i in big_header:
            for k, v in i.items():
                export_sheet.write_merge(v[0], v[1], v[2], v[3], k)
        row_idx += 1

    col_idx = 0
    for col_title in col_title_list:
        export_sheet.write(row_idx, col_idx, col_title)
        col_idx += 1
    row_idx += 1
    s = XFStyle()
    for row_item_list in data_row_list:
        col_idx = 0
        for current_value in row_item_list:
            if current_value:
                current_value_is_date = False
                if isinstance(current_value, datetime.datetime):
                    current_value = xlrd.xldate.xldate_from_datetime_tuple((current_value.year, current_value.month,
                                                                            current_value.day, current_value.hour, current_value.minute,
                                                                            current_value.second), 0)
                    current_value_is_date = True
                elif isinstance(current_value, datetime.date):
                    current_value = xlrd.xldate.xldate_from_date_tuple((current_value.year, current_value.month,
                                                                        current_value.day), 0)
                    current_value_is_date = True
                elif isinstance(current_value, datetime.time):
                    current_value = xlrd.xldate.xldate_from_time_tuple((current_value.hour, current_value.minute,
                                                                        current_value.second))
                    current_value_is_date = True
                elif isinstance(current_value, models.Model):
                    current_value = str(current_value)
                elif type(current_value) is dict:
                    current_value = json.dumps(current_value)
                elif type(current_value) is list:
                    current_value = ', '.join(current_value)
                if current_value_is_date:
                    s.num_format_str = 'M/D/YY'
                    export_sheet.write(row_idx, col_idx, current_value, s)
                else:
                    export_sheet.write(row_idx, col_idx, current_value)
            col_idx += 1
        row_idx += 1

    if write_to_file:
        with default_storage.open(filename, 'wb') as f:
            export_wb.save(f)
        return filename
    else:
        export_wb.save(output)
        output.seek(0)
        response = HttpResponse(output.getvalue())
        response['Content-Type'] = 'application/vnd.ms-excel'
        response['Content-Disposition'] = 'attachment; filename=' + \
            os.path.basename(filename)
        return response


class TrackMiddleware(object):

    def process_response(self, request, response):
        from core.models import Tracker
        tracker = request.TRACKER
        # Mismatch happens during login/logout
        must_create = request.session.session_key != tracker.session_key
        request.session._session_key = tracker.session_key
        try:
            request.session.save(must_create=must_create)
        except CreateError:
            # Universe hates us
            request.session._session_key = None
            request.session.save()
            # refresh to update fd and user foreign keys
            tracker.refresh_from_db()
            Tracker.objects.get_or_create(
                session_key=request.session.session_key,
                defaults={
                    'extra': tracker.extra,
                    'user_id': tracker.user_id,
                    'fd_id': tracker.fd_id,
                }
            )
        request.session.modified = True

        # ================ REQUIRED FOR LMS ==================
        try:
            if request.COOKIES.get('landing_page_url', '') in (
                '', 'None', None
            ):
                response.set_cookie(
                    'landing_page_url', request.build_absolute_uri()
                )

            if request.GET.get('mode', '').lower() == 'unassisted':
                response.set_cookie('mode', 'unassisted', expires=600)
            else:
                if request.COOKIES.get('mode', '') in ('', 'None', None):
                    response.set_cookie('mode', 'assisted')
        except:
            pass
        # ====================================================

        return response

    def process_request(self, request):
        from core.models import Tracker
        path = request.path_info
        if (
            path.startswith(settings.STATIC_URL or '///')
            or path.startswith(settings.MEDIA_URL or '///')
            or path.startswith('/affiliate-leads/')
        ):
            return

        session_key = request.session.session_key
        if not session_key:
            request.session.save()
            session_key = request.session.session_key
            request.session.modified = True

        if request.user.is_authenticated():
            tracker, _ = Tracker.objects.get_or_create(session_key=session_key)
            if tracker.user and tracker.user != request.user:
                # TODO (later): what will happen if one user was attached to
                # tracker but another user logged in on that tracker
                # err = "Tracker and user missmatch. This happened for Tracker(%s),"
                # err += " Tracker_user(%s), New user(%s)"
                # logger.error(
                #     err % (
                #         tracker.pk, tracker.user.pk, request.user.pk
                #     )
                # )
                pass
            if not tracker.user:
                tracker.attach(request.user)
        else:
            tracker, _ = Tracker.objects.get_or_create(session_key=session_key)

        request.TRACKER = tracker


class TrackerLogic():
    pass

    @classmethod
    def big_decisions(cls, request):
        data = {
            'from': 'BIGDECISIONS',
            'referrer_url': request.META.get('HTTP_REFERER'),
            'current_url': request.get_full_path(),
            'tracker_id': request.GET.get('tracker_id')
        }
        activity = Activity(
            tracker=request.TRACKER,
            event="REFFERER",
            extra=data
        )
        activity.save()
        return activity


def track(logic):
    def _inner(fn):
        def wrapped(request, *args, **kwargs):
            tracker_logic = TrackerLogic()
            activity = getattr(tracker_logic, logic)(request)
            return fn(request, *args, **kwargs)
        return wraps(fn)(wrapped)
    return _inner


def base64_url_decode(inp):
    padding_factor = (4 - len(inp) % 4) % 4
    inp += "=" * padding_factor
    return base64.b64decode(
        unicode(inp).translate(dict(zip(map(ord, u'-_'), u'+/')))
    )


def parse_signed_request(signed_request, secret):
    l = signed_request.split('.', 2)
    encoded_sig = l[0]
    payload = l[1]

    sig = base64_url_decode(encoded_sig)
    data = json.loads(base64_url_decode(payload))

    if data.get('algorithm').upper() != 'HMAC-SHA256':
        # print 'Unknown algorithm'
        return None
    else:
        expected_sig = hmac.new(
            secret, msg=payload, digestmod=hashlib.sha256
        ).digest()

    if sig != expected_sig:
        return None
    else:
        # print 'Valid signed request received.'
        return data


class Cryptor(object):
    '''
    Provide encryption and decryption function that works with crypto-js.
    https://code.google.com/p/crypto-js/

    Padding implemented as per RFC 2315: PKCS#7 page 21
    http://www.ietf.org/rfc/rfc2315.txt

    The key to make pycrypto work with crypto-js are:
    1. Use MODE_CFB.  For some reason, crypto-js decrypted result from MODE_CBC
       gets truncated
    2. Use Pkcs7 padding as per RFC 2315, the default padding used by CryptoJS
    3. On the JS side, make sure to wrap ciphertext with
       CryptoJS.lib.CipherParams.create()
    '''

    BLOCK_SIZE = 16

    @classmethod
    def _pad_string(cls, in_string):
        '''Pad an input string according to PKCS#7'''
        in_len = len(in_string)
        pad_size = cls.BLOCK_SIZE - (in_len % cls.BLOCK_SIZE)
        return in_string.ljust(in_len + pad_size, chr(pad_size))

    @classmethod
    def _unpad_string(cls, in_string):
        '''Remove the PKCS#7 padding from a text string'''
        in_len = len(in_string)
        pad_size = ord(in_string[-1])
        if pad_size > cls.BLOCK_SIZE:
            raise ValueError('Input is not padded or padding is corrupt')
        return in_string[:in_len - pad_size]

    @classmethod
    def generate_iv(cls, size=16):
        return Random.get_random_bytes(size)

    @classmethod
    def encrypt(cls, in_string, in_key, in_iv=None):
        '''
        Return encrypted string.
        @in_string: Simple str to be encrypted
        @key: hexified key
        @iv: hexified iv
        '''
        key = binascii.a2b_hex(in_key)

        if in_iv is None:
            iv = cls.generate_iv()
            in_iv = binascii.b2a_hex(iv)
        else:
            iv = binascii.a2b_hex(in_iv)

        aes = AES.new(key, AES.MODE_CFB, iv, segment_size=128)
        return in_iv, aes.encrypt(cls._pad_string(in_string))

    @classmethod
    def decrypt(cls, in_encrypted, in_key, in_iv):
        '''
        Return encrypted string.
        @in_encrypted: Base64 encoded
        @key: hexified key
        @iv: hexified iv
        '''
        key = binascii.a2b_hex(in_key)
        iv = binascii.a2b_hex(in_iv)
        aes = AES.new(key, AES.MODE_CFB, iv, segment_size=128)

        decrypted = aes.decrypt(binascii.a2b_base64(in_encrypted).rstrip())
        return cls._unpad_string(decrypted)

    @classmethod
    def encrypt_json_response(cls, response_dict, request):
        json_result = json.dumps(response_dict)
        key = request.POST.get('csrfmiddlewaretoken') or request.GET[
            'csrfmiddlewaretoken']
        key = key.encode('hex')
        iv, encrypted = cls.encrypt(json_result, key)
        ctext = binascii.b2a_base64(encrypted).rstrip()
        return "%s%s%s" % (key, ctext, iv)


def kiss_record_event(event, property=None):
    km = kissmetrics.KM(settings.KISS_API_KEY)
    try:
        thread.start_new_thread(km.record, (event, property))
    except Exception, e:
        pass
        # print "Error", str(e)
    return


def kiss_identify_user(request):
    km = kissmetrics.KM(settings.KISS_API_KEY)
    try:
        # km.identify(request.user.email)
        # print "Identifying Customer"
        thread.start_new_thread(km.identify, ('%s' % request.user.email,))
    except Exception, e:
        pass
        # print "Error", str(e)
    return


class SetCookieForInternalIPMiddleware(object):

    def process_request(self, request):
        request.META['REMOTE_ADDR'] = get_request_ip(request)
        if self.is_internal_ip(request):
            request.IS_INTERNAL = True
        else:
            request.IS_INTERNAL = False

    def process_response(self, request, response):
        if self.is_internal_ip(request):
            if not request.COOKIES.get('is_internal_ip', None):
                response.set_cookie(
                    'is_internal_ip', value='true', max_age=31536000)
        return response

    @staticmethod
    def is_internal_ip(request):
        request_ip = request.META['REMOTE_ADDR']
        return request_ip in settings.INTERNAL_IPS


def get_request_ip(request):
    real_ip = request.META.get('HTTP_X_REAL_IP')
    if real_ip:
        return real_ip
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class FileLockException(Exception):
    pass


class FileLock(object):
    """
    A file locking mechanism that has context-manager support so you can use it
    in a with statement. This should be relatively cross compatible as it
    doesn't rely on msvcrt or fcntl for the locking.
    """

    def __init__(self, file_name, timeout=10, delay=.05):
        """
        Prepare the file locker. Specify the file to lock and optionally the
        maximum timeout and the delay between each attempt to lock.
        """
        self.is_locked = False
        self.lockfile = os.path.join(os.getcwd(), "%s.lock" % file_name)
        self.file_name = file_name
        self.timeout = timeout
        self.delay = delay

    def acquire(self):
        """
        Acquire the lock, if possible. If the lock is in use, it check again
        every `wait` seconds. It does this until it either gets the lock or
        exceeds `timeout` number of seconds, in which case it throws an
        exception.
        """
        start_time = time.time()
        while True:
            try:
                self.fd = os.open(self.lockfile, os.O_CREAT |
                                  os.O_EXCL | os.O_RDWR)
                break
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise
                if (time.time() - start_time) >= self.timeout:
                    raise FileLockException("Timeout occured.")
                time.sleep(self.delay)
        self.is_locked = True

    def release(self):
        """
        Get rid of the lock by deleting the lockfile. When working in a `with`
        statement, this gets automatically called at the end.
        """
        if self.is_locked:
            os.close(self.fd)
            os.unlink(self.lockfile)
            self.is_locked = False

    def __enter__(self):
        """
        Activated when used in the with statement. Should automatically acquire
        a lock to be used in the with block.
        """
        if not self.is_locked:
            self.acquire()
        return self

    def __exit__(self, type, value, traceback):
        """
        Activated at the end of the with statement.
        It automatically releases the lock if it isn't locked.
        """
        if self.is_locked:
            self.release()

    def __del__(self):
        """
        Make sure that the FileLock instance doesn't leave a lockfile lying
        around.
        """
        self.release()


class ExtensionMeta(type):
    def __new__(cls, name, parents, class_scope):
        t = type.__new__(cls, name, parents, class_scope)
        if not hasattr(t, "model"):
            return t
        for key, value in class_scope.items():
            if key in ["__metaclass__", "__module__"]:
                continue
            if isinstance(value, type):
                continue
            if hasattr(t.model, key):
                raise Exception(
                    "Can not overwrite {}.{}()".format(t.model, key)
                )
            if callable(value):
                setattr(t.model, key, value)
        return t


class Extension(object):
    """
    class Extension

    Motivation
    ----------

    Most of our core models are going to be in an app 'coverfox'. We are trying
    to limit access to this app. This app would only contain very minimal model
    definition and appropriate migrations. No managers or methods.

    Now we want to have methods, its lot easier to say user.send_email() than to
    say utils.send_mail_to_user(user). So we allow "Extension"s. This class
    helps with that.

    How To
    ------

    This is how you write an extension to User model:

    class UserExtension(Extension):
        model = User
        def send_mail(self):
            pass

    This has to put in some file, lets standardize on models.py, as every app
    needs to have at least models.py to be a valid app, and this is quite
    related to models.

    Once an extension like this is defined, we will have .send_mail() method
    available on User class itself. You never have to access this class
    directly.

    All the methods defined on extension class would become available on the
    model the extension is for.

    Limitations
    -----------

    For now you can only add methods, not managers or plain attributes. Also you
    can not overwrite anything, if one extension has added .send_mail(), no
    other extension can add .send_mail(). Also you can not modify methods that
    are available on the model to begin with, eg .get_full_name() in case of
    User.
    """
    __metaclass__ = ExtensionMeta


def parse_cookie(cookie):
    if cookie == '':
        return {}
    if not isinstance(cookie, http_cookies.BaseCookie):
        try:
            # Modified this line for double quote bug
            c = cookies.Cookies.from_request(cookie, ignore_bad_cookies=True)
        except http_cookies.CookieError:
            # Invalid cookie
            return {}
    else:
        c = cookie
    cookiedict = {}
    for key in c.keys():
        cookiedict[key] = c.get(key).value
    return cookiedict

# Patching parse.cookie because parser fails when cookie has double quotes
from django.http import cookie
cookie.parse_cookie = parse_cookie
import django.http
django.http.parse_cookie = parse_cookie

from django.db.models import Expression, IntegerField

class Levenshtein(Expression):
    template = 'LEVENSHTEIN( %(expressions)s )'

    def __init__(self, f1, f2, **extra):
      super(Levenshtein, self).__init__(output_field=IntegerField())
      self.expressions = [f1, f2]
      for expression in self.expressions:
          if not hasattr(expression, 'resolve_expression'):
              raise TypeError('%r is not an Expression' % expression)
      self.extra = extra

    def resolve_expression(self, query=None, allow_joins=True, reuse=None, summarize=False):
        c = self.copy()
        c.is_summary = summarize
        for pos, expression in enumerate(self.expressions):
            c.expressions[pos] = expression.resolve_expression(query, allow_joins, reuse, summarize)
        return c

    def as_sql(self, compiler, connection):
        sql_expressions, sql_params = [], []
        for expression in self.expressions:
            sql, params = compiler.compile(expression)
            sql_expressions.append(sql)
            sql_params.extend(params)
        self.extra['expressions'] = ','.join(sql_expressions)
        return self.template % self.extra, sql_params

from django.db.models import F, Value
def find_city(pincode):
        """
        Find pincode using edit distance matching. Expects Pincode model instance
        """
        # try hospital method
        try:
            city = City.objects.filter(state=pincode.state).annotate(edit=Levenshtein(F('name'), Value(pincode.city))).filter(edit__lte=1).order_by('edit')[0]
        except IndexError:
            city = None
        return city


def patch(what, new):
    """
        Patch decorator. Similar to mock.patch.
    """
    def decorator(fn):
        def ddecorated(*args, **kw):
            module_start = what.split(".")[0]
            if module_start in ["__file__"]:
                raise Exception("__file__ not allowed")

            if module_start == "__main__":
                what_module = inspect.getmodule(fn)
                what_object = what.rsplit(".", 1)[-1]
            else:
                what_module, what_object = what.rsplit(".", 1)
                what_module = importlib.import_module(what_module)

            old = getattr(what_module, what_object)
            setattr(what_module, what_object, new)
            ret = fn(*args, **kw)
            setattr(what_module, what_object, old)
            return ret
        return ddecorated
    return decorator


def retry(max_tries, delay=1, backoff=2, exceptions=(Exception,), hook=None):
    """
    https://gist.github.com/n1ywb/2570004

    Function decorator implementing retrying logic.

    delay: Sleep this many seconds * backoff * try number after failure
    backoff: Multiply delay by this factor after each failure
    exceptions: A tuple of exception classes; default (Exception,)
    hook: A function with the signature myhook(tries_remaining, exception);
          default None

    The decorator will call the function up to max_tries times if it raises
    an exception.

    By default it catches instances of the Exception class and subclasses.
    This will recover after all but the most fatal errors. You may specify a
    custom tuple of exception classes with the 'exceptions' argument; the
    function will only be retried if it raises one of the specified
    exceptions.

    Additionally you may specify a hook function which will be called prior
    to retrying with the number of remaining tries and the exception instance;
    see given example. This is primarily intended to give the opportunity to
    log the failure. Hook is not called after failure if no retries remain.
    """
    def dec(func):
        @wraps(func, assigned=available_attrs(func))
        def f2(*args, **kwargs):
            mydelay = delay
            tries = range(max_tries)
            tries.reverse()
            for tries_remaining in tries:
                try:
                    return func(*args, **kwargs)
                except exceptions as e:
                    if tries_remaining > 0:
                        if hook is not None:
                            hook(tries_remaining, e, mydelay)
                        time.sleep(mydelay)
                        mydelay *= backoff
                    else:
                        raise
        return f2
    return dec


def subtract_years(d, years):
    """Return a date that's `years` years before the date (or datetime)
    object `d`. Return the same calendar date (month and day) in the
    destination year, if it exists, otherwise use the following day
    (thus changing February 29 to February 28).
    """
    try:
        return d.replace(year=d.year - years)
    except ValueError:
        return d - (datetime.date(d.year + years, 1, 1) - datetime.date(d.year, 1, 1))


def add_years(d, years):
    """Return a date that's `years` years after the date (or datetime)
    object `d`. Return the same calendar date (month and day) in the
    destination year, if it exists, otherwise use the following day
    (thus changing February 29 to March 1).
    """
    try:
        return d.replace(year=d.year + years)
    except ValueError:
        return d + (datetime.date(d.year + years, 1, 1) - datetime.date(d.year, 1, 1))


def is_task_successful(request, task_id):
    """Returns task execute status in JSON format."""
    return JsonResponse({'task': {
        'id': task_id,
        'executed': AsyncResult(task_id).successful(),
    }})


def task_status(request, task_id):
    """Returns task status and result in JSON format."""
    result = AsyncResult(task_id)
    state, retval = result.state, result.result
    response_data = dict(id=task_id, status=state, result=retval)
    if state in states.EXCEPTION_STATES:
        traceback = result.traceback
        response_data.update({'result': safe_repr(retval),
                              'exc': get_full_cls_name(retval.__class__),
                              'traceback': traceback})
    return JsonResponse({'task': response_data})


def registered_tasks(request):
    """View returning all defined tasks as a JSON object."""
    return JsonResponse({'regular': current_app.tasks.regular().keys(),
                         'periodic': current_app.tasks.periodic().keys()})


def clean_email_list(email_string):
    emails = email_re.finditer(email_string)
    emails_set = set()
    for email in emails:
        email = email.group()
        emails_set.add(email.lower())
    return list(emails_set)


def generate_pdf_from_html(template_path, data, pdf_path=None):
    template = get_template(template_path)
    html = template.render(Context(data))
    config = pdfkit.configuration(wkhtmltopdf=settings.WKHTMLTOPDF_PATH)
    return pdfkit.from_string(
        html,
        pdf_path,
        configuration=config,
        options={
            'page-size': 'A4',
            'margin-top': '0',
            'margin-right': '0',
            'margin-left': '0',
            'margin-bottom': '0',
            'zoom': '1.2',
            'encoding': "UTF-8",
        })


def get_city_state_from_pincode(pincode):
    """
    Args: pincode (str)

    Return: city (str), state (str)
    """
    pincode = Pincode.objects.filter(pincode=pincode)
    city = pincode[0].city if pincode else None
    state = pincode[0].state.name if pincode else None

    return city, state


class FileReader(object):
    """
    FileReader returns lines from a file. It is used for following a file. It
    will wait for timeout period if it is already at end, and check again.

    It returns position in file, till where we have read, and content of line.

    timeout is in seconds, floating number.
    """
    def __init__(self, filename, start=0, timeout=1.0, tailf=False):
        self.filename = filename
        self.timeout = timeout
        self.fd = open(filename)
        self.tailf = tailf
        self.fd.seek(start)

    def readline(self):
        while True:
            line = self.fd.readline()
            if line == "" and self.tailf:
                time.sleep(self.timeout)
                continue
            pos = self.fd.tell()
            return line, pos


class SpoolManager(object):
    """
    SpoolManager is used to keep track of how much have we read from a file.
    """
    def __init__(self, spool_file):
        self.spool_file = spool_file

    @property
    def pos(self):
        try:
            return int(open(self.spool_file, "r").read())
        except IOError:
            return 0
        # intentionally not catching ValueError in case its not int.

    def persist(self, pos):
        fd = open(self.spool_file, "w")
        fd.write(pos)


class RedshiftWriter(object):
    """
    Writes rows to redshit.
    """
    def __init__(self, table, batch_size, timeout, parser):
        self.table = table
        self.batch_size = batch_size
        self.timeout = timeout
        self.parser = parser
        self.columns = self.parser.columns
        self.buf = []
        self.last = time.time()

    def write(self, data):
        self.buf.append(data)
        if (
            len(self.lines) == self.batch_size or time.time() - self.last > self.timeout
        ):
            self.publish()
            return True

    def publish(self):
        self.last = time.time()
        for data in self.buf:
            pass


class File2X(object):
    def __init__(self, filename, spool_file, parser, timeout, tailf=False):
        self.spool = SpoolManager(spool_file)
        self.reader = FileReader(
            filename, start=self.spool.pos, timeout=timeout, tailf=tailf
        )
        self.timeout = timeout
        self.parser = parser

    def run_once(self):
        line, pos = self.reader.readline()
        parsed = self.parser.parse(line)
        committed = self.writer.write(parsed)
        if committed:
            self.spool.persist(pos)

    def run(self):
        while True:
            self.run_once()


class Kinesis2X(object):
    def __init__(self, channel, spool_file, parser, writer, timeout):
        pass


def d(obj, name=""):
    """ replacement for print for debugging something """
    from django.db import models
    print "-" * 80
    print type(obj), name
    try:
        print json.dumps(obj, indent=4, sort_keys=True)
    except TypeError:
        if isinstance(obj, models.Model):
            print repr(obj)
            pprint.PrettyPrinter(indent=4).pprint(obj.__dict__)
        else:
            pprint.PrettyPrinter(indent=4).pprint(obj)
    print "^" * 80


def get_file_from_s3(access_key, secret_key, bucket_name, file_path, local_path='/tmp/'):
    def percent_cb(complete, total):
        sys.stdout.write('Progress: %s complete of %s\n' % (complete, total))
    # connect to the bucket
    conn = boto.connect_s3(access_key, secret_key)
    bucket = conn.get_bucket(bucket_name)
    # Check if key exist
    if bucket.get_key(file_path):
        # Check if file exists locally
        if not os.path.exists(local_path + file_path):
            if not os.path.exists(local_path + '/'.join(file_path.split('/')[:-1])):
                os.makedirs(local_path + '/'.join(file_path.split('/')[:-1]))
            bucket.get_key(file_path).get_contents_to_filename(
                local_path + file_path, cb=percent_cb, num_cb=100)
        return local_path + file_path
    else:
        return False


def put_file_to_s3(access_key, secret_key, bucket_name, from_file_path, to_file_path):
    def percent_cb(complete, total):
        sys.stdout.write('Progress: %s complete of %s\n' % (complete, total))
    conn = boto.connect_s3(access_key, secret_key)
    bucket = conn.get_bucket(bucket_name)
    k = bucket.new_key(to_file_path)
    k.set_contents_from_filename(from_file_path, cb=percent_cb, num_cb=10)
    return k
