from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import login
from django.contrib.auth import authenticate
from django.db.models import Q

import random
import datetime
import time
import uuid
import re
import hashlib

from core.models import User
from rdesk.models import *
from cf_fhurl import RequestForm, RequestModelForm
from customforms.ajaxfield import AjaxField
from customdb.widgets import AdvancedFileInput


class NewQuizForm(forms.Form):
    title = forms.CharField()

    def save(self):
        quiz = Quiz(title=self.cleaned_data.get('title'))
        quiz.save()
        return quiz


class QuizForm(forms.Form):
    question = forms.CharField(widget=forms.Textarea)
    choice1 = forms.CharField(widget=forms.Textarea, required=False)
    score1 = forms.IntegerField(required=False)
    choice2 = forms.CharField(widget=forms.Textarea, required=False)
    score2 = forms.IntegerField(required=False)
    choice3 = forms.CharField(widget=forms.Textarea, required=False)
    score3 = forms.IntegerField(required=False)
    choice4 = forms.CharField(widget=forms.Textarea, required=False)
    score4 = forms.IntegerField(required=False)

    def __init__(self, *args, **kwargs):
        super(QuizForm, self).__init__(*args, **kwargs)
        self.fields['question'].widget.attrs.update(
            {'class': 'span12 expand', 'rows': 2, 'cols': 80})
        self.fields['choice1'].widget.attrs.update(
            {'class': 'span12 expand', 'rows': 2, 'cols': 80})
        self.fields['choice2'].widget.attrs.update(
            {'class': 'span12 expand', 'rows': 2, 'cols': 80})
        self.fields['choice3'].widget.attrs.update(
            {'class': 'span12 expand', 'rows': 2, 'cols': 80})
        self.fields['choice4'].widget.attrs.update(
            {'class': 'span12 expand', 'rows': 2, 'cols': 80})
