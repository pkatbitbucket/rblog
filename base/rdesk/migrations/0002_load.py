# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
from django.core.management import call_command

fixture = 'quiz.json'

def load_fixture(apps, schema_editor):
    call_command('loaddata', fixture, app_label='rdesk')


class Migration(migrations.Migration):

    dependencies = [
    	('rdesk', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_fixture)
    ]