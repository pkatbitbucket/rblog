from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.db.models import permalink
from django.contrib.auth.models import UserManager, AbstractBaseUser, BaseUserManager
from django_extensions.db.fields import UUIDField
from django.core import serializers
from jsonfield import JSONField
from django.utils import timezone
import json
import hashlib
import random
import re
import datetime
from django.template.defaultfilters import slugify

from core.models import User


class Quiz(models.Model):
    title = models.CharField(max_length=250)
    slug = models.SlugField(_('slug'), max_length=255, blank=True)
    content = JSONField(_('content'), default={})

    def __unicode__(self):
        return u"%s" % self.title

    def save(self, **kwargs):
        super(Quiz, self).save()
        if not self.slug:
            self.slug = "%s-%s" % (slugify(self.title), self.id)
        super(Quiz, self).save()
