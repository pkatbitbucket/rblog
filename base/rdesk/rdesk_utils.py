q2_score_dict = {
    tuple(range(0, 40)): {
        'title': 'THE RED-LIGHT RAGER',
        'img': 'red-light-rager.png',
        'desc': """
                        <p>
                            We would like to know if you are below the legal driving age limit.
                            If not, we would still like to know. Because you shouldn't be in
                            control of anything resembling a vehicle. Not even a tricycle with safety wheels.
                        </p>
                        <p>
                            On a parting note, there's a new NFS coming out this year.
                            Something tells us you're going to be really, really great at it.
                        </p>
                    """,
    },
    tuple(range(40, 70)): {
        'title': 'THE APE-ESCAPE CHAUFFEUR',
        'img': 'ape.png',
        'desc': """
                        <p>
                            I know an ape that has better driving skills than you.
                            Unfortunately for him, only humans are allowed to have
                            driving licences. You managed to get one. Clearly there's
                            hope for you. But, we recommend you also get a driver.
                            And a huge insurance cover for your car.
                        </p>
                    """
    },
    tuple(range(70, 90)): {
        'title': 'THE BEST 2ND-BEST DRIVER',
        'img': 'second-best.png',
        'desc': """
                         <p>
                         Almost a perfect score! Not bad at all. In fact you got so close,
                         that you kissed the rear of your boss' car.
                         And all because you stared a few seconds longer at unrecognizable road signs.
                         We suggest you retake the quiz. Until you get it right.
                         Also, remember to renew your car insurance before you hit the road next.
                         </p>
                     """
    },
    tuple(range(90, 101)): {
        'title': 'THE GOD-MODE DRIVER',
        'img': 'god-mode-driver.png',
        'desc': """
                        <p>
                            Holy rules of driving! The God of Road Signs has indeed been reincarnated.
                            We are blessed to be in your presence, oh all-knowing-one.
                            However, with all the non-believers still driving around us, we implore
                            you to always keep yourself insured. You never know when one will come your way.
                        </p>
                    """
    }
}
