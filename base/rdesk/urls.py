from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^quiz/start/$', views.quiz_landing, name='quiz_start'),
    url(r'^quiz/add/(?P<quiz_id>[\d]+)/$', views.new_quiz, name='new_quiz'),
    url(r'^manage-quiz/$', views.manage_quiz, name='quiz_manage'),
    url(r'^quiz/results/$', views.quiz_results, name='quiz_results'),
    url(r'^quiz/(?P<quiz_slug>[\w-]+)/$', views.display_quiz, name='display_quiz'),
]
