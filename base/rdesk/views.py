from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404
from django.views.decorators.cache import never_cache
from django.forms.formsets import formset_factory
from django.views.decorators.csrf import csrf_exempt

from rdesk.forms import *  # NOQA
import utils
import rdesk_utils


@utils.profile_type_only('ADMIN')
@never_cache
def manage_quiz(request):
    if request.POST:
        nq = NewQuizForm(request.POST)
        if nq.is_valid():
            quiz = nq.save()
            return HttpResponseRedirect('/rupys-desk/quiz/add/%s/' % quiz.id)
        else:
            print nq.errors
    else:
        form = NewQuizForm()
    object_list = Quiz.objects.all()
    return render_to_response('core/admin_quiz_list.html', {
        'form': form,
        'object_list': object_list,
    }, context_instance=RequestContext(request))


@utils.profile_type_only('ADMIN')
@never_cache
def new_quiz(request, quiz_id):
    quiz = get_object_or_404(Quiz, id=quiz_id)
    QuizFormset = formset_factory(QuizForm)

    if request.POST:
        formset = QuizFormset(request.POST)
        if formset.is_valid():
            # print formset.cleaned_data
            qlist = []
            for form in formset:
                d = form.cleaned_data
                if d.get('question'):
                    qd = {}
                    qd['question'] = d.get('question')
                    qd['choices'] = []
                    for i in range(1, 5):
                        if d.get('choice%s' % i, '') and d.get('score%s' % i, 0):
                            qd['choices'].append(
                                {
                                    'choice': d.get('choice%s' % i),
                                    'score': d.get('score%s' % i)
                                }
                            )
                    if qd['choices']:
                        qlist.append(qd)
            content_dict = {'data': formset.cleaned_data, 'quiz': qlist}
            quiz.content.update(**content_dict)
            quiz.save()
            return HttpResponseRedirect('/rupys-desk/manage-quiz/')
        else:
            print formset.errors
    else:
        if 'data' in quiz.content.keys():
            formset = QuizFormset(initial=quiz.content['data'])
        else:
            formset = QuizFormset()
    return render_to_response('core/admin_quiz_add.html', {
        'quiz': quiz,
        'formset': formset,
    }, context_instance=RequestContext(request))


@never_cache
def quiz_landing(request):
    quiz = get_object_or_404(Quiz, id=2)
    meta = {
        'title': quiz.title,
        'description': "The Road Side Quiz",
        'image': '%s/static/rdesk/quizy_2/img/roadside_quiz.png' % settings.SITE_URL,
        'url': '%s/rupys-desk/quiz/start/' % (settings.SITE_URL),
    }
    if request.GET.get('score', None):
        score = request.GET.get('score', 0)
        for k, v in rdesk_utils.q2_score_dict.items():
            if int(score) in k:
                score_text = v
        meta['title'] = 'I GOT %s' % score_text['title'].upper()
        meta['description'] = score_text['desc']
        meta['image'] = '%s/static/rdesk/quizy_2/img/%s' % (settings.SITE_URL, score_text['img'])
        meta['url'] = '%s/rupys-desk/quiz/start/?score=%s' % (settings.SITE_URL, score)
    return render_to_response('rdesk/quizy_new.html', {
        'meta': meta,
        'quiz': quiz,
    }, context_instance=RequestContext(request))


@never_cache
def display_quiz(request, quiz_slug):
    quiz = get_object_or_404(Quiz, slug=quiz_slug)
    if quiz.id == 1:
        meta = {
            'title': quiz.title,
            'description': "It's pop quiz time! Take a quiz to find out whether or not you've been conned into "
                           "buying an insurance that you don't really need.",
            'image': '%s/static/rdesk/img/rupy_plus_chimp.jpg' % settings.SITE_URL,
            'url': '%s/rupys-desk/quiz/%s/' % (settings.SITE_URL, quiz_slug),
        }
    else:
        meta = {
            'title': quiz.title,
            'description': "The Road Side Quiz",
            'image': '%s/static/rdesk/quizy_2/img/roadside_quiz.png' % settings.SITE_URL,
            'url': '%s/rupys-desk/quiz/start/' % (settings.SITE_URL),
        }
    return render_to_response('rdesk/quizy_%s.html' % quiz.id, {
        'quiz': quiz,
        'meta': meta,
    }, context_instance=RequestContext(request))


@never_cache
@csrf_exempt
def quiz_results(request):
    if not request.POST:
        return HttpResponseRedirect('/')
    else:
        con_percent = int(float(request.POST['score']) / float(request.POST['total_out_of']) * 100)
        quiz = get_object_or_404(Quiz, id=request.POST['quiz_number'])
        score_text = ''
        if quiz.id == 2:
            for k, v in rdesk_utils.q2_score_dict.items():
                if int(request.POST['score']) in k:
                    score_text = v
        return render_to_response('rdesk/quiz_results_%s.html' % request.POST['quiz_number'], {
            'score_text': score_text,
            'quiz': quiz,
            'score': request.POST['score'],
            'display_score': 0 if int(request.POST['score']) < 10 else int(request.POST['score']) / 10,
            'out_of': request.POST['total_out_of'],
            'con_percent': con_percent,
        }, context_instance=RequestContext(request))
