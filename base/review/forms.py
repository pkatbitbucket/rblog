from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import login
from django.contrib.auth import authenticate
from django.db.models import Q
import django

import random
import datetime
import time
import uuid
import re
import hashlib
import putils

import thread
import threading

from core.models import User, Tracker, Activity
from review.models import *
from cf_fhurl import RequestForm, RequestModelForm
from customforms.ajaxfield import AjaxField
from customdb.widgets import AdvancedFileInput
from django.core.mail import send_mail

MARITAL_STATUS_CHOICES = (
    ('S-1', 'single man'),
    ('S-2', 'single woman'),
    ('M-1', 'married man'),
    ('M-2', 'married woman'),
)

CHILDREN_CHOICES = (
    ('NONE', 'no kids'),
    ('NO_DEPENDENT', 'no dependent children'),
    ('ONE_DEPENDENT', 'one dependent child'),
    ('TWO_OR_MORE', 'two or more dependent children'),
)

PARENT_CHOICES = (
    ('FINANCIALLY_INDEPENDENT', 'financially independent'),
    ('FINANCIALLY_DEPENDENT', 'financially dependent'),
    ('NO_PARENTS', 'no more'),
)

LOCATION_CHOICES = (
    ('M-MUMBAI', 'Mumbai'),
    ('M-DELHI', 'Delhi'),
    ('M-KOLKATA', 'Kolkata'),
    ('M-CHENNAI', 'Chennai'),
    ('M-BANGALORE', 'Bangalore'),
    ('M-PUNE', 'Pune'),
    ('M-HYDERABAD', 'Hyderabad'),
    ('N-OTHER', 'a non-metro city'),
)

LOAN_STATUS_CHOICES = (
    ('0', 'nil'),
    ('10', 'less than 10 lakhs'),
    ('15', 'between 10 to 25 lakhs'),
    ('35', 'between 26 to 50 lakhs'),
    ('50', 'more than 50 lakhs'),
)

ANNUAL_INCOME_CHOICES = (
    ('0_0-0_0', 'nil'),
    ('0_0-2_5', 'less than 2.5 lakhs'),
    ('2_5-7_5', 'between 2.5 to 7.5 lakhs'),
    ('7_5-12_0', 'between 7.6 to 12 lakhs'),
    ('12_0-20_0', 'between 12.1 to 20 lakhs'),
    ('20_0-40_0', 'more than 20 lakhs'),
)


def split_name(name):
    name = ' '.join(name.split())
    if len(name.split(' ')) > 1:
        last_name = name.split(' ')[-1]
        first_name = ' '.join(name.split(' ')[:-1])
    else:
        first_name = name
        last_name = ''
    return first_name, last_name


def calculate_age(born):
    today = datetime.date.today()
    try:
        birthday = born.replace(year=today.year)
    except ValueError:  # raised when birth date is February 29 and the current year is not a leap year
        birthday = born.replace(year=today.year, day=born.day - 1)
    if birthday > today:
        return today.year - born.year - 1
    else:
        return today.year - born.year


class ReviewDataForm(forms.Form):
    name = forms.CharField(max_length=255, required=True)
    email = forms.EmailField(max_length=255, required=True)
    birth_date = forms.CharField(max_length=10, required=True)
    gender = forms.IntegerField(required=False)
    marital_status = forms.ChoiceField(
        choices=MARITAL_STATUS_CHOICES, required=True)
    children = forms.ChoiceField(choices=CHILDREN_CHOICES, required=True)
    parents = forms.ChoiceField(choices=PARENT_CHOICES, required=True)
    location = forms.ChoiceField(choices=LOCATION_CHOICES, required=True)
    #occupation_status = forms.ChoiceField(choices=OCCUPATION_STATUS_CHOICES, required=True)
    loan_status = forms.ChoiceField(choices=LOAN_STATUS_CHOICES, required=True)
    annual_income = forms.ChoiceField(
        choices=ANNUAL_INCOME_CHOICES, required=True)

    def __init__(self, *args, **kwargs):
        super(ReviewDataForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update(
            {'class': '', 'placeholder': 'name'})
        self.fields['email'].widget.attrs.update(
            {'class': '', 'placeholder': 'email'})
        self.fields['marital_status'].widget.attrs.update(
            {'class': 'form-control input-lg selectpicker'})
        self.fields['children'].widget.attrs.update(
            {'class': 'form-control input-lg selectpicker'})
        self.fields['parents'].widget.attrs.update(
            {'class': 'form-control input-lg selectpicker'})
        self.fields['location'].widget.attrs.update(
            {'class': 'form-control input-lg selectpicker'})
        self.fields['loan_status'].widget.attrs.update(
            {'class': 'form-control input-lg selectpicker'})
        self.fields['annual_income'].widget.attrs.update(
            {'class': 'form-control input-lg selectpicker'})

    def clean_birth_date(self):
        filled_date = self.cleaned_data.get('birth_date')
        try:
            bdate = datetime.datetime.strptime(filled_date, '%d/%m/%Y').date()
        except ValueError:
            raise forms.ValidationError('Please enter a valid date')
        if bdate > datetime.date.today():
            raise forms.ValidationError('Please enter a valid date')
        age = calculate_age(bdate)
        if age > 75:
            raise forms.ValidationError('There are very few insurance options for people over 75 years of age. '
                                        'You can post a mail to info@coverfox.com and we\'ll try to help you out')
        if age < 18:
            raise forms.ValidationError('We\'re sorry. You can\'t have a policy under your name if you are less than'
                                        ' 18 years of age. ')

        return filled_date

    def save(self, tracker, activity):
        d = self.cleaned_data
        extra_fields = list(set(self.fields.keys()))
        extra_data = dict([(k, d.get(k)) for k in extra_fields])
        bdate = datetime.datetime.strptime(
            d.get('birth_date'), '%d/%m/%Y').date()
        extra_data['age'] = calculate_age(bdate)
        if d.get('marital_status').split('-')[0] == 'S':
            extra_data['children'] = 'NONE'
        extra_data['gender'] = int(d.get('marital_status').split('-')[1])
        activity.extra.update(**extra_data)
        activity.save()

        full_name = self.cleaned_data['name']
        tracker.extra['first_name'], tracker.extra[
            'last_name'] = split_name(full_name)
        tracker.extra['email'] = d.get('email')
        tracker.extra['gender'] = int(d.get('marital_status').split('-')[1])
        tracker.save()

        return tracker, activity


class ReviewLoginForm(RequestForm):
    name = forms.CharField(max_length=255)
    email = forms.EmailField()

    def clean_name(self):
        d = self.cleaned_data.get
        name = d('name').strip()
        pattern = "^[a-zA-Z\s]*$"
        match = re.match(pattern, name)
        if not match:
            raise forms.ValidationError(
                "Special characters not allowed in name.")
        return name

    def save(self):
        tracker = self.request.TRACKER
        d = self.cleaned_data
        first_name, last_name = split_name(d['name'])
        extra_data = {
            'email': d['email'],
            'first_name': first_name,
            'last_name': last_name,
        }
        tracker.extra.update(**extra_data)
        tracker.save()
        return {'success': True, 'mesg': {'email': tracker.extra['email'], 'name': tracker.extra['first_name']}}


HEALTH_COVERAGE_OPTIONS = (
    ('NO_COVERAGE', 'No coverage from employer'),
    ('LESS_THAN_1_LAC', 'Less than 1 lac'),
    ('BETWEEN_1_TO_2_LAC', 'Between 1-2 Lacs'),
    ('2_AND_ABOVE', '2 Lacs and above'),
    ('NOT_SURE', 'I\'m not sure'),
)

TERM_LIFE_OPTIONS = (
    ('NO', 'No I don\'t'),
    ('LESS_THAN_50', 'Less than 50 Lacs'),
    ('50_LACS_TO_1_CRORE', 'Between 50 Lacs and 1 crore'),
    ('MORE_THAN_1_CRORE', 'More than 1 Crore'),
    ('NOT_SURE', 'I have Life insurance, but I don\'t know whether it\'s a term plan'),
)


def check_date_error(filled_date):
    error = ''
    if filled_date == 'dd/mm/yyyy' or not filled_date:
        bdate = ''
    else:
        try:
            bdate = datetime.datetime.strptime(filled_date, '%d/%m/%Y').date()
            if bdate > datetime.date.today():
                raise forms.ValidationError('Please enter a valid date')
        except ValueError:
            raise forms.ValidationError('Please enter a valid date')
    return bdate


class ExtraInfoForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(ExtraInfoForm, self).__init__(*args, **kwargs)
        self.fields['session_key'] = forms.CharField(
            max_length=100, required=True)
        self.fields['bd_spouse'] = forms.CharField(
            max_length=10, required=False)
        self.fields['bd_kid_1'] = forms.CharField(
            max_length=10, required=False)
        self.fields['bd_kid_2'] = forms.CharField(
            max_length=10, required=False)
        self.fields['bd_kid_3'] = forms.CharField(
            max_length=10, required=False)
        self.fields['bd_mom'] = forms.CharField(max_length=10, required=False)
        self.fields['bd_dad'] = forms.CharField(max_length=10, required=False)
        self.fields['health_coverage'] = forms.ChoiceField(
            widget=forms.RadioSelect(), choices=HEALTH_COVERAGE_OPTIONS)
        self.fields['term_life'] = forms.ChoiceField(
            widget=forms.RadioSelect(), choices=TERM_LIFE_OPTIONS)

    def clean_bd_spouse(self):
        filled_date = self.cleaned_data.get('bd_spouse')
        return check_date_error(filled_date)

    def clean_bd_kid_1(self):
        filled_date = self.cleaned_data.get('bd_kid_1')
        return check_date_error(filled_date)

    def clean_bd_kid_2(self):
        filled_date = self.cleaned_data.get('bd_kid_2')
        return check_date_error(filled_date)

    def clean_bd_kid_3(self):
        filled_date = self.cleaned_data.get('bd_kid_3')
        return check_date_error(filled_date)

    def clean_bd_mom(self):
        filled_date = self.cleaned_data.get('bd_mom')
        return check_date_error(filled_date)

    def clean_bd_dad(self):
        filled_date = self.cleaned_data.get('bd_dad')
        return check_date_error(filled_date)

    def save(self):
        d = self.cleaned_data
        tracker = Tracker.objects.get(session_key=d['session_key'])
        activity = tracker.activity_set.get(event='INSTANT_REVIEW')
        activity.extra.update({'review_extra_data': d})
        activity.save()
        try:
            sub_text = 'Request Initiated for product recommendation'
            msg_text = "Data : \n\nActivity ID: %s\n\n" % (activity.id)
            for k, v in activity.extra.items():
                msg_text = '%s%s : %s \n\n' % (msg_text, k, v)
            send_mail(sub_text, msg_text, "Dev Bot <dev@cfstatic.org>", [
                      'support@coverfox.com'])
        except:
            pass
        return {'success': True}


class ProductAddForm(forms.ModelForm):
    pass

    class Meta:
        model = MarketProduct
        if django.VERSION > (1, 7):
            fields = "__all__"


class FeatureAddForm(forms.ModelForm):
    pass

    class Meta:
        model = MarketProductFeature
        exclude = ('market_product',)


class ExpertReviewResponseForm(forms.Form):
    product = forms.ModelChoiceField(
        queryset=Product.objects.all(), required=True)
    market_product = forms.ModelChoiceField(
        queryset=MarketProduct.objects.all())
    cover = forms.CharField(required=True, max_length=20)
    premium = forms.CharField(required=True, max_length=20)

    def __init__(self, product_list=None, *args, **kwargs):
        super(ExpertReviewResponseForm, self).__init__(*args, **kwargs)
        if product_list:
            self.fields['product'].queryset = Product.objects.filter(
                code__in=product_list)
