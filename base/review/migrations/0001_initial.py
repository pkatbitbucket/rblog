# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
import review.models
import customdb.thumbs


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MarketProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('logo', customdb.thumbs.ImageWithThumbsField(
                    upload_to=review.models.get_file_path)),
                ('name', models.CharField(max_length=250, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='MarketProductFeature',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('content', models.TextField(blank=True)),
                ('market_product', models.ForeignKey(related_name='feature',
                                                     blank=True, to='review.MarketProduct', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=250)),
                ('name', models.CharField(max_length=250)),
                ('level1', models.CharField(max_length=250, blank=True)),
                ('level2', models.CharField(max_length=250, blank=True)),
                ('risks_covered', models.TextField(blank=True)),
                ('is_active', models.BooleanField(default=True)),
                ('content', jsonfield.fields.JSONField(
                    default={}, verbose_name='content', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Risk',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=250)),
                ('risk', models.CharField(max_length=250)),
                ('is_active', models.BooleanField(default=True)),
                ('content', jsonfield.fields.JSONField(
                    default={}, verbose_name='content', blank=True)),
                ('products', models.ManyToManyField(
                    to='review.Product', blank=True)),
            ],
        ),
    ]
