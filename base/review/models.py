from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.db.models import permalink
from django.contrib.auth.models import UserManager, AbstractBaseUser, BaseUserManager
from django_extensions.db.fields import UUIDField
from django.core import serializers
from jsonfield import JSONField
from customdb.thumbs import ImageWithThumbsField
from django.utils import timezone
import json
import hashlib
import datetime
import os
import utils
import random
import re
import datetime

from core.models import User


def get_file_path(instance, filename):
    now = datetime.datetime.now()
    fname, ext = os.path.splitext(filename)
    new_fname = utils.slugify(fname)
    newfilename = "pw-%s.%s" % (now.strftime("%I%M%S"), ext)
    path_to_save = "uploads/review/product/%s" % (newfilename)
    return path_to_save


class Product(models.Model):
    code = models.CharField(max_length=250)
    name = models.CharField(max_length=250)
    level1 = models.CharField(max_length=250, blank=True)
    level2 = models.CharField(max_length=250, blank=True)
    risks_covered = models.TextField(blank=True)
    is_active = models.BooleanField(default=True)
    content = JSONField(_('content'), blank=True, default={})

    def __unicode__(self):
        return '%s - %s' % (self.code, self.name)


class Risk(models.Model):
    code = models.CharField(max_length=250)
    risk = models.CharField(max_length=250)
    products = models.ManyToManyField(Product, blank=True)
    is_active = models.BooleanField(default=True)
    content = JSONField(_('content'), blank=True, default={})

    def __unicode__(self):
        return '%s - %s' % (self.code, self.risk)


class MarketProduct(models.Model):
    logo = ImageWithThumbsField(
        upload_to=get_file_path, sizes=(('s', 150, 225),))
    name = models.CharField(max_length=250, blank=True)

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class MarketProductFeature(models.Model):
    market_product = models.ForeignKey(
        'MarketProduct', blank=True, null=True, related_name='feature')
    content = models.TextField(blank=True)

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)
