import logging
import re
import time
import datetime
import os
import random
import json
import hashlib
import hashlib
import string
from django.conf import settings
from django.utils.functional import SimpleLazyObject
from django.contrib.contenttypes.models import ContentType
from tagging.models import Tag
from django.db.models import Q
from collections import OrderedDict
import math
from core.templatetags.manip_data import int_commator


from review.models import *
from review.forms import ReviewDataForm, LOCATION_CHOICES
from core.models import Tracker, Activity

location_dict = dict(LOCATION_CHOICES)


class GetRisk():

    def get_risk_by_gender(self, activity):
        code_list = [r.code for r in Risk.objects.filter(is_active=True)]
        return list(set(code_list))

    def get_risk_by_birth_date(self, activity):
        age = activity.extra['age']
        code_list = ['R007', 'R008', 'R009']
        # Exceptions start
        if age <= 30 and activity.extra['gender'] == 1:
            code_list.append('R001')
        if age <= 45:
            code_list.append('R003')
        # if age <= 45 and activity.extra['gender'] == 2:
        #    code_list.append('R001')
        if age <= 40 and activity.extra['gender'] == 1:
            code_list.append('R002')
        if age <= 45 and activity.extra['gender'] == 2:
            code_list.append('R002')
        # Exceptions end
        if age >= 45:
            code_list.append('R010')
        if age >= 45:
            code_list.extend([
                'R004',
                'R006',
            ])
        if age >= 50:
            code_list.append('R005')
        if age >= 55:
            code_list.append('R001')

        return list(set(code_list))

    def get_risk_by_marital_status(self, activity):
        code_list = [r.code for r in Risk.objects.filter(
            is_active=True) if r.code not in ['R002', 'R009']]
        if activity.extra['marital_status'][0] == 'M':
            code_list.append('R002')
            code_list.append('R009')
        else:
            if activity.extra['parents'] not in ['NO_PARENTS']:
                code_list.append('R009')
        return list(set(code_list))

    def get_risk_by_children(self, activity):
        code_list = [r.code for r in Risk.objects.filter(
            is_active=True) if r.code not in ['R002']]
        if activity.extra['children'] not in ['TWO_OR_MORE', 'NO_DEPENDENT']:
            code_list.append('R002')
        return list(set(code_list))

    def get_risk_by_parents(self, activity):
        code_list = [r.code for r in Risk.objects.filter(
            is_active=True) if r.code not in ['R007']]
        if activity.extra['parents'] == 'FINANCIALLY_DEPENDENT':
            code_list.append('R007')
        # EXCEPTION CONDITION
        if activity.extra['age'] <= 35 and activity.extra['parents'] == 'FINANCIALLY_INDEPENDENT':
            code_list.append('R007')

        return list(set(code_list))

    def get_risk_by_location(self, activity):
        code_list = [r.code for r in Risk.objects.filter(is_active=True)]
        return list(set(code_list))

    def get_risk_by_loan_status(self, activity):
        code_list = [r.code for r in Risk.objects.filter(
            is_active=True) if r.code not in ['R008']]
        loan_amt = (int(activity.extra['loan_status'])) * 100000
        if loan_amt:
            code_list.append('R008')
        return list(set(code_list))

    def get_risk_by_annual_income(self, activity):
        code_list = [r.code for r in Risk.objects.filter(
            is_active=True) if r.code not in ['R009']]
        ai_top_ch = activity.extra['annual_income'].split('-')[1]
        ai_top = (
            (int(ai_top_ch.split('_')[0]) * 10) + (int(ai_top_ch.split('_')[1]))) * 10000
        ai_bottom_ch = activity.extra['annual_income'].split('-')[0]
        ai_bottom = ((int(ai_bottom_ch.split('_')[
                     0]) * 10) + (int(ai_bottom_ch.split('_')[1]))) * 10000
        annual_income = ai_bottom + ((ai_top - ai_bottom) / 2)
        if annual_income:
            code_list.append('R009')
        return list(set(code_list))


def get_all_risks(activity):
    risks = []
    criteria = ReviewDataForm().fields.keys()
    obj = GetRisk()
    for c in criteria:
        try:
            if not risks:
                risks.extend(getattr(obj, 'get_risk_by_%s' %
                                     c, None)(activity))
            else:
                risks = list(set(risks).intersection(
                    set(getattr(obj, 'get_risk_by_%s' % c, None)(activity))))
        except TypeError:
            # print 'get_risk_by_%s'%c
            pass
    extra_data = {}
    extra_data['risks'] = list(set(risks))
    risk_order = ['R001', 'R003', 'R004', 'R006',
                  'R002', 'R007', 'R005', 'R010', 'R008', 'R009']
    # Handling exception
    # if 'R002' in extra_data['risks'] and 'R003' in extra_data['risks']:
    #    extra_data['risks'].remove('R003')
    extra_data['risks'] = [x for x in risk_order if x in extra_data[
        'risks']] + [x for x in extra_data['risks'] if x not in risk_order]
    extra_data['products'] = []
    for r in extra_data['risks']:
        ri = Risk.objects.get(code=r)
        extra_data['products'].extend([p.code for p in ri.products.all()])
    extra_data['products'] = list(set(extra_data['products']))
    activity.extra.update(**extra_data)
    activity.save()
    return activity


class GetCover():

    def __init__(self, activity):
        self.cover_dict = {}
        self.activity = activity

        # Initialising base cover for all products
        for p in Product.objects.filter(is_active=True):
            self.cover_dict[p.code] = {'sum': 0.0, 'level': 0}

        # Adding base price to some products
        self.cover_dict['P005']['sum'] = 200000.0
        self.cover_dict['P010']['sum'] = 750000.0
        self.cover_dict['P011']['sum'] = 500000.0

    def get_cover_by_gender(self):
        if self.activity.extra['gender'] == 2:
            self.cover_dict['P005']['sum'] += 50000
            self.cover_dict['P007']['sum'] += 50000
        return

    def get_cover_by_birth_date(self):
        age = self.activity.extra['age']
        if age > 45:
            self.cover_dict['P007']['sum'] = 500000.0
        if age <= 30:
            self.cover_dict['P001']['sum'] += 1500000
        if age > 55:
            self.cover_dict['P001']['sum'] += 150000
            self.cover_dict['P001']['level'] = 1

        return

    def get_cover_by_marital_status(self):
        if self.activity.extra['marital_status'][0] == 'S':
            self.cover_dict['P001']['sum'] += 50000

        # Exceptions
        if self.activity.extra['marital_status'][0] == 'M':
            self.cover_dict['P005']['level'] = 1
            self.cover_dict['P007']['level'] = 1
            self.cover_dict['P010']['level'] = 1

            if self.activity.extra['children'] in ['NONE', 'NO_DEPENDENT']:
                self.cover_dict['P005'][
                    'sum'] += self.cover_dict['P005']['sum'] * 0.1
                self.cover_dict['P007'][
                    'sum'] += self.cover_dict['P007']['sum'] * 0.1
            if self.activity.extra['children'] in ['ONE_DEPENDENT', 'TWO_OR_MORE']:
                self.cover_dict['P005'][
                    'sum'] += self.cover_dict['P005']['sum'] * 0.15
                self.cover_dict['P007'][
                    'sum'] += self.cover_dict['P007']['sum'] * 0.15
        return

    def get_cover_by_parents(self):
        if self.activity.extra['parents'] == 'FINANCIALLY_DEPENDENT':
            self.cover_dict['P005'][
                'sum'] += self.cover_dict['P005']['sum'] * 0.2
            self.cover_dict['P007'][
                'sum'] += self.cover_dict['P007']['sum'] * 0.1
        return

    def get_cover_by_location(self):
        if self.activity.extra['location'][0] == 'M':
            self.cover_dict['P001'][
                'sum'] += self.cover_dict['P001']['sum'] * 0.1
            self.cover_dict['P005'][
                'sum'] += self.cover_dict['P005']['sum'] * 0.1
            self.cover_dict['P007'][
                'sum'] += self.cover_dict['P007']['sum'] * 0.1
        return

    def get_cover_by_loan_status(self):
        loan_amt = int(self.activity.extra['loan_status']) * 100000
        self.cover_dict['P013']['sum'] += loan_amt
        return

    def get_cover_by_annual_income(self):
        ai_top_ch = self.activity.extra['annual_income'].split('-')[1]
        ai_top = (
            (int(ai_top_ch.split('_')[0]) * 10) + (int(ai_top_ch.split('_')[1]))) * 10000
        ai_bottom_ch = self.activity.extra['annual_income'].split('-')[0]
        ai_bottom = ((int(ai_bottom_ch.split('_')[
                     0]) * 10) + (int(ai_bottom_ch.split('_')[1]))) * 10000
        ai = (ai_top + ai_bottom) / 2
        ai = ai_bottom + ((ai_top - ai_bottom) / 2)
        if self.activity.extra['age'] > 55:
            self.cover_dict['P017']['sum'] += 3 * ai
        if self.activity.extra['age'] > 45 and self.activity.extra['age'] <= 55:
            self.cover_dict['P017']['sum'] += 4 * ai
        if self.activity.extra['age'] >= 30 and self.activity.extra['age'] <= 45:
            self.cover_dict['P017']['sum'] += 3 * ai
        if self.activity.extra['age'] < 30:
            self.cover_dict['P017']['sum'] += 2 * ai
        return

    def calculate_cover(self):
        criteria = ReviewDataForm().fields.keys()
        for c in criteria:
            func = getattr(self, "get_cover_by_%s" % c, None)
            if func:
                func()
        # Handling exceptions
        if 'R002' in self.activity.extra['risks']:
            self.cover_dict['P005'][
                'sum'] += self.cover_dict['P005']['sum'] * 0.1
            self.cover_dict['P005']['level'] = 2

        # Rounding off to nearest 250000
        for k, v in self.cover_dict.items():
            if v['sum']:
                v['sum'] = (v['sum'] + 24999) // 25000 * 25000

        if 'P009' in self.activity.extra['products']:
            self.cover_dict['P009']['sum'] = 3000

        if 'P005' in self.activity.extra['products'] and self.cover_dict['P005']['sum'] >= 700000:
            self.cover_dict['P008']['sum'] = self.cover_dict[
                'P005']['sum'] - 500000
            self.cover_dict['P005']['sum'] = 500000

        if 'P007' in self.activity.extra['products'] and self.cover_dict['P007']['sum'] >= 700000:
            self.cover_dict['P008']['sum'] = self.cover_dict[
                'P007']['sum'] - 500000
            self.cover_dict['P007']['sum'] = 500000

        # Exception to handle Income Replacement and OS Liability Term Cover
        if 'P013' in self.activity.extra['products'] and 'P017' in self.activity.extra['products']:
            self.cover_dict['P013']['level'] = 1
            self.cover_dict['P017']['level'] = 1
            if self.cover_dict['P013']['sum'] >= self.cover_dict['P017']['sum']:
                self.activity.extra['products'].remove('P017')
                self.cover_dict['P013']['sum'] = (
                    self.cover_dict['P013']['sum'] + 2499999) // 2500000 * 2500000
            else:
                self.activity.extra['products'].remove('P013')
                self.cover_dict['P017']['sum'] = (
                    self.cover_dict['P017']['sum'] + 2499999) // 2500000 * 2500000
        elif 'P013' in self.activity.extra['products']:
            # Round off to the nearest 25 lacs
            self.cover_dict['P013']['sum'] = (
                self.cover_dict['P013']['sum'] + 2499999) // 2500000 * 2500000
        elif 'P017' in self.activity.extra['products']:
            # Round off to the nearest 25 lacs
            self.cover_dict['P017']['sum'] = (
                self.cover_dict['P017']['sum'] + 2499999) // 2500000 * 2500000
        # else:
            # If income and loan are both zero, apply a term cover for 5 lacs straight
        #    self.activity.extra['products'].append('P017')
        #    self.cover_dict['P017']['level'] = 2
        #    self.cover_dict['P017']['sum'] = 500000

        if 'P015' in self.activity.extra['products'] and 'P016' in self.activity.extra['products']:
            self.cover_dict['P015']['level'] = 1
            self.cover_dict['P016']['level'] = 1
            self.activity.extra['products'].remove('P016')

        for k, v in self.cover_dict.items():
            if not v['sum'] and k not in ['P015', 'P016']:
                if k in self.activity.extra['products']:
                    self.activity.extra['products'].remove(k)
            if k not in self.activity.extra['products']:
                self.cover_dict.pop(k)

        prods = [p.code for r in self.activity.extra['risks']
                 for p in Risk.objects.get(code=r).products.all()
                 if p.code in self.activity.extra['products']]
        prods.extend([p for p in self.activity.extra[
                     'products'] if p not in prods])
        self.activity.extra['products'] = prods

        extra_data = {'cover': self.cover_dict}
        activity = Activity.objects.get(
            tracker=self.activity.tracker, event=self.activity.event)
        activity.extra.update(**self.activity.extra)
        activity.extra.update(**extra_data)
        activity.save()
        return activity


class GetRiskText():

    def __init__(self, activity):
        self.activity = activity
        self.content = OrderedDict()

    def get_content_by_r001(self):
        code = 'R001'
        risk = Risk.objects.get(code=code)
        final_text = ''

        if self.activity.extra['age'] <= 30:
            rt = risk.content['RT01']
            final_text = rt % self.activity.extra['age']
        elif self.activity.extra['age'] >= 55:
            rt = risk.content['RT02']
            final_text = rt

        self.content[code] = final_text
        return

    def get_content_by_r002(self):
        code = 'R002'
        risk = Risk.objects.get(code=code)
        if self.activity.extra['children'] == 'ONE_DEPENDENT':
            self.content[code] = risk.content['RT01']
        else:
            self.content[code] = risk.content['RT02']
        return

    def get_content_by_r003(self):
        code = 'R003'
        risk = Risk.objects.get(code=code)

        if self.activity.extra['age'] <= 40:
            rt = risk.content['RT01']
            if self.activity.extra['age'] > 30:
                atext = "The grey strands of hair may make you feel otherwise, but medically you are still young. :)"
            else:
                atext = 'You are quite young. There is nothing much to worry about on the health front. ' \
                        'But you just need to guard against "it can never happen to me" syndrome.'

            if self.activity.extra['marital_status'][0] == 'M':
                final_text = rt % (atext, ' and your family')
            else:
                final_text = rt % (atext, '')
        else:
            rt = risk.content['RT02']
            if self.activity.extra['age'] == 44:
                rt = rt % ('1 year')
            else:
                rt = rt % ('%s years' % (45 - int(self.activity.extra['age'])))
            final_text = rt

        self.content[code] = final_text
        return

    def get_content_by_r004(self):
        code = 'R004'
        risk = Risk.objects.get(code=code)

        if self.activity.extra['age'] >= 45 and self.activity.extra['age'] < 50:
            uage = 45
        else:
            uage = 50

        if self.activity.extra['age'] < 60:
            ret_text = """You haven't retired yet. Work, family, retirement... lots of things to be settled."""
        else:
            ret_text = ''

        rt = risk.content['RT01']
        final_text = rt % (uage, uage, ret_text)

        self.content[code] = final_text
        return

    def get_content_by_r005(self):
        code = 'R005'
        risk = Risk.objects.get(code=code)
        rt = risk.content['RT01']
        final_text = rt
        self.content[code] = final_text
        return

    def get_content_by_r006(self):
        code = 'R006'
        risk = Risk.objects.get(code=code)
        rt = risk.content['RT01']
        final_text = rt
        self.content[code] = final_text
        return

    def get_content_by_r007(self):
        code = 'R007'
        risk = Risk.objects.get(code=code)

        if self.activity.extra['parents'] == 'FINANCIALLY_DEPENDENT':
            self.content[code] = risk.content['RT01']
        else:
            self.content[code] = risk.content['RT02']
        return

    def get_content_by_r008(self):
        code = 'R008'
        risk = Risk.objects.get(code=code)
        rt = risk.content['RT01']
        final_text = rt
        self.content[code] = final_text
        return

    def get_content_by_r009(self):
        code = 'R009'
        risk = Risk.objects.get(code=code)

        if self.activity.extra['marital_status'][0] == 'S':
            final_text = risk.content['RT03']
        else:
            if self.activity.extra['children'] in ['ONE_DEPENDENT', 'TWO_OR_MORE']:
                rt = risk.content['RT02']
            else:
                rt = risk.content['RT01']

            if self.activity.extra['gender'] == 1:
                final_text = rt % ('wife', 'her')
            else:
                final_text = rt % ('husband', 'his')
        self.content[code] = final_text
        return

    def get_content_by_r010(self):
        code = 'R010'
        risk = Risk.objects.get(code=code)
        rt = risk.content['RT01']
        final_text = rt
        self.content[code] = final_text
        return

    def get_risk_texts(self):
        for code in self.activity.extra['risks']:
            func = getattr(self, "get_content_by_%s" % code.lower(), None)
            if func:
                func()
        return self.content


class GetProductText():

    def __init__(self, activity):
        self.activity = activity
        self.content = OrderedDict()
        risk_order = ['R001', 'R003', 'R004', 'R006',
                      'R002', 'R007', 'R005', 'R010', 'R008', 'R009']
        for r in risk_order:
            for p in Risk.objects.get(code=r).products.all():
                self.content[p.code] = {}
        for p in Product.objects.all():
            if p.code not in self.content.keys():
                self.content[p.code] = {}

    def get_content_by_p001(self):
        code = 'P001'
        prod = Product.objects.get(code=code)
        if self.activity.extra['age'] <= 30:
            self.content[code]['A'] = prod.content['PTA1']
            self.content[code]['B'] = prod.content['PTB1']
            self.content[code]['C'] = prod.content['PTC1']
            self.content[code]['D'] = prod.content[
                'PTD1'] % (self.activity.extra['age'])
        if self.activity.extra['age'] >= 55:
            self.content[code]['A'] = prod.content['PTA2']
            self.content[code]['B'] = prod.content['PTB2']
            self.content[code]['C'] = prod.content['PTC2']
            self.content[code]['D'] = prod.content['PTD2'] % int_commator(
                int(self.activity.extra['cover'][code]['sum']))
        return

    def get_content_by_p005(self):
        code = 'P005'
        prod = Product.objects.get(code=code)
        ai_top_ch = self.activity.extra['annual_income'].split('-')[1]
        ai_top = (
            (int(ai_top_ch.split('_')[0]) * 10) + (int(ai_top_ch.split('_')[1]))) * 10000
        ai_bottom_ch = self.activity.extra['annual_income'].split('-')[0]
        ai_bottom = ((int(ai_bottom_ch.split('_')[
                     0]) * 10) + (int(ai_bottom_ch.split('_')[1]))) * 10000
        annual_income = ai_bottom + ((ai_top - ai_bottom) / 2)
        if self.activity.extra['cover'][code]['level'] == 1:
            # Health with Floater cover
            self.content[code]['A'] = prod.content['PTA2']
            self.content[code]['B'] = prod.content['PTB2']
            self.content[code]['C'] = prod.content['PTC2']
            if self.activity.extra['location'][0] == 'M':
                lt = '%s, where good health-care costs are skyrocketing' % location_dict[
                    self.activity.extra['location']]
            else:
                lt = 'a non-metro city where good health-care options are limited and expensive'
            if self.activity.extra['children'] in ['ONE_DEPENDENT']:
                child = 'child '
                fact = 'four'
            elif self.activity.extra['children'] in ['TWO_OR_MORE']:
                child = 'children '
                fact = 'four'
            else:
                child = ''
                fact = 'three'
            if self.activity.extra['gender'] == 1:
                gt = 'wife'
            else:
                gt = 'husband'
            ait = "<p>Even if your employer gives you some health insurance, it's probably not enough. " \
                  "70% of Corporate India gives cover of less than 2 lacs. " \
                  "That's not going to help you with today's high medical costs and shooting medical inflation of 15% every year. </p>" \
                  "<p>So you still need a plan you own and control, to ensure you don't pay out of your pocket</p>"

            self.content[code]['D'] = prod.content[
                'PTD2'] % (fact, gt, child, lt, ait)
        elif self.activity.extra['cover'][code]['level'] == 2:
            # Health with materinity
            if self.activity.extra['location'][0] == 'M':
                if self.activity.extra['gender'] == 1:
                    lt = "In %s, delivery costs up to &#8377;75,000, more in case %s to undergo a cesarean section." % (
                        location_dict[self.activity.extra['location']], 'your spouse has')
                else:
                    lt = "In %s, delivery costs up to &#8377;75,000, more in case %s to undergo a cesarean section." % (
                        location_dict[self.activity.extra['location']], 'you have')
            else:
                lt = "In non-metro cities, delivery can cost as much as &#8377;60,000,  more in case %s to undergo a cesarean section."
            self.content[code]['A'] = prod.content['PTA1']
            self.content[code]['B'] = prod.content['PTB1']
            self.content[code]['C'] = prod.content['PTC1']
            pt = prod.content['PTD1']
            if self.activity.extra['location'][0] == 'M':
                lt = '%s, where good health-care costs are skyrocketing' % location_dict[
                    self.activity.extra['location']]
            else:
                lt = 'a non-metro city where good health-care options are limited and expensive'
            if self.activity.extra['children'] == 'ONE_DEPENDENT':
                child = 'child '
                fact = 'four'
            else:
                child = ''
                fact = 'three'
            if self.activity.extra['gender'] == 1:
                gt = 'wife'
            else:
                gt = 'husband'
            if self.activity.extra['gender'] == 1:
                relt = 'your spouse has'
            else:
                relt = 'you have'

            ait = "<p>70% of Corporate India gives cover of less than 2 lacs. " \
                  "That's not going to help you when it matters.</p>" \
                  "<p>So you still need a plan you own and control, to ensure you don't pay out of your pocket</p>"

            self.content[code]['D'] = pt % (fact, gt, child, lt, ait)
        else:
            # Health cover - Individual
            self.content[code]['A'] = prod.content['PTA2']
            self.content[code]['B'] = prod.content['PTB2']
            self.content[code]['C'] = prod.content['PTC3']
            ait = "<p>Even if your employer gives you some health insurance, it's probably not enough. " \
                  "70% of Corporate India gives cover of less than 2 lacs. " \
                  "That's not going to help you with today's high medical costs and shooting medical inflation of 15% every year. </p>" \
                  "<p>So you still need a plan you own and control, to ensure you don't pay out of your pocket</p>"
            if self.activity.extra['location'][0] == 'M':
                lt = '%s, where good health-care costs are skyrocketing' % location_dict[
                    self.activity.extra['location']]
            else:
                lt = 'a non-metro city where good health-care options are limited and expensive'
            self.content[code]['D'] = prod.content['PTD3'] % (lt, ait)

        return

    def get_content_by_p007(self):
        code = 'P007'
        prod = Product.objects.get(code=code)

        if self.activity.extra['age'] <= 58:
            atext = "<p>Even if your employer gives you some health insurance, it's probably not enough." \
                    "Research shows that 70% of Corporate India gives cover of less than 2 lacs. " \
                    "That's going to be woefully short, if the need does arise.</p>" \
                    "<p>5 Lakhs is the minimum you will need to cover peak medical risk. " \
                    "Also you need to account for an average medical inflation of 15% year on year.</p>"
        else:
            atext =  "<p>Research shows that most customers buy a cover of less than 2 lacs. " \
                     "That's going to be woefully short, if the need does arise.</p>" \
                     "<p>5 Lakhs is the minimum you will need to cover peak medical risk. " \
                     "Also you need to account for an average medical inflation of 15% year on year.</p>"

        self.content[code]['A'] = prod.content['PTA1']

        self.content[code]['B'] = prod.content[
            'PTB1'] % (self.activity.extra['age'])

        if self.activity.extra['cover'][code]['level'] == 1:
            self.content[code]['C'] = prod.content['PTC1']
        else:
            self.content[code]['C'] = prod.content['PTC2']

        if self.activity.extra['location'][0] == 'M':
            lt = '%s, where good health-care costs are skyrocketing' % location_dict[
                self.activity.extra['location']]
        else:
            lt = 'a non-metro city where good health-care options are limited and expensive'

        if self.activity.extra['children'] in ['ONE_DEPENDENT']:
            child = 'child '
            fact = 'four'
        elif self.activity.extra['children'] in ['TWO_OR_MORE']:
            child = 'children '
            fact = 'four'
        else:
            child = ''
            fact = 'three'

        if self.activity.extra['gender'] == 1:
            gt = 'wife'
        else:
            gt = 'husband'
        if self.activity.extra['cover'][code]['level'] == 1:
            self.content[code]['D'] = prod.content[
                'PTD1'] % (fact, gt, child, lt, atext)
        else:
            self.content[code]['D'] = prod.content['PTD2'] % (lt, atext)
        return

    def get_content_by_p008(self):
        code = 'P008'
        prod = Product.objects.get(code=code)
        self.content[code]['A'] = prod.content['PTA1']
        self.content[code]['B'] = prod.content['PTB1']
        self.content[code]['C'] = prod.content['PTC1']

        if self.activity.extra['marital_status'][0] == 'M':
            mtext = "Your family's"
        else:
            mtext = "Your"
        self.content[code]['D'] = prod.content['PTD1'] % (
            mtext,
            int_commator(
                int(self.activity.extra['cover'][code]['sum'] + 500000)),
            int_commator(
                int(self.activity.extra['cover'][code]['sum'] + 500000)),
            int_commator(int(self.activity.extra['cover'][code]['sum']))
        )
        return

    def get_content_by_p009(self):
        code = 'P009'
        prod = Product.objects.get(code=code)
        self.content[code]['A'] = prod.content['PTA1']
        self.content[code]['B'] = prod.content['PTB1']
        self.content[code]['C'] = prod.content['PTC1']
        self.content[code]['D'] = prod.content['PTD1']
        return

    def get_content_by_p010(self):
        code = 'P010'
        prod = Product.objects.get(code=code)
        self.content[code]['A'] = prod.content['PTA1']
        self.content[code]['B'] = prod.content['PTB1']
        self.content[code]['C'] = prod.content['PTC1']
        low_age = (self.activity.extra['age'] - 4) // 5 * 5
        high_age = (self.activity.extra['age'] + 4) // 5 * 5
        age_range = '%s - %s' % (low_age, high_age)
        if self.activity.extra['gender'] == 1:
            gt = 'wife'
        else:
            gt = 'husband'
        if self.activity.extra['cover'][code]['level'] == 1:
            self.content[code]['D'] = prod.content['PTD1'] % (age_range, gt)
        else:
            self.content[code]['D'] = prod.content['PTD2'] % (age_range)
        return

    def get_content_by_p011(self):
        code = 'P011'
        prod = Product.objects.get(code=code)
        self.content[code]['A'] = prod.content['PTA1']
        self.content[code]['B'] = prod.content['PTB1']
        self.content[code]['C'] = prod.content['PTC1']
        self.content[code]['D'] = prod.content['PTD1']
        return

    def get_content_by_p013(self):
        code = 'P013'
        prod = Product.objects.get(code=code)
        self.content[code]['A'] = prod.content['PTA1']
        self.content[code]['B'] = prod.content['PTB1']
        self.content[code]['C'] = prod.content['PTC1']

        ai_top_ch = self.activity.extra['annual_income'].split('-')[1]
        ai_top = (
            (int(ai_top_ch.split('_')[0]) * 10) + (int(ai_top_ch.split('_')[1]))) * 10000
        ai_bottom_ch = self.activity.extra['annual_income'].split('-')[0]
        ai_bottom = ((int(ai_bottom_ch.split('_')[
                     0]) * 10) + (int(ai_bottom_ch.split('_')[1]))) * 10000
        annual_income = ai_bottom + ((ai_top - ai_bottom) / 2)

        if self.activity.extra['cover'][code]['level'] == 1:
            if self.activity.extra['marital_status'][0] == 'S':
                self.content[code]['D'] = prod.content['PTD2']
            else:
                self.content[code]['D'] = prod.content['PTD3']
        else:
            self.content[code]['D'] = prod.content['PTD1']
        return

    def get_content_by_p015(self):
        code = 'P015'
        prod = Product.objects.get(code=code)
        self.content[code]['A'] = prod.content['PTA1']
        self.content[code]['B'] = prod.content['PTB1']
        self.content[code]['C'] = prod.content['PTC1']
        self.content[code]['D'] = prod.content['PTD1']
        return

    def get_content_by_p016(self):
        code = 'P016'
        prod = Product.objects.get(code=code)
        self.content[code]['A'] = prod.content['PTA1']
        self.content[code]['B'] = prod.content['PTB1']
        self.content[code]['C'] = prod.content['PTC1']
        self.content[code]['D'] = prod.content['PTD1']
        return

    def get_content_by_p017(self):
        code = 'P017'
        prod = Product.objects.get(code=code)
        self.content[code]['A'] = prod.content['PTA1']
        self.content[code]['B'] = prod.content['PTB1']
        self.content[code]['C'] = prod.content['PTC1']
        ai_top_ch = self.activity.extra['annual_income'].split('-')[1]
        ai_top = (
            (int(ai_top_ch.split('_')[0]) * 10) + (int(ai_top_ch.split('_')[1]))) * 10000
        ai_bottom_ch = self.activity.extra['annual_income'].split('-')[0]
        ai_bottom = ((int(ai_bottom_ch.split('_')[
                     0]) * 10) + (int(ai_bottom_ch.split('_')[1]))) * 10000
        annual_income = ai_bottom + ((ai_top - ai_bottom) / 2)

        if self.activity.extra['cover'][code]['level'] == 1:
            if self.activity.extra['marital_status'][0] == 'S':
                self.content[code]['D'] = prod.content['PTD2']
            else:
                self.content[code]['D'] = prod.content['PTD3']
        else:
            self.content[code]['D'] = prod.content['PTD1']

        return

    def get_product_texts(self):
        for code in self.activity.extra['products']:
            func = getattr(self, "get_content_by_%s" % code.lower(), None)
            if func:
                func()
        return self.content
