from django.conf.urls import url

from cf_fhurl import fhurl

from . import views

urlpatterns = [
    fhurl(r'^ajax/review-login/$', views.ReviewLoginForm, name='ajax_review_login', template=None, json=True),
    url(r'^info-add/$', views.add_info, name="add_info"),
    url(r'^start/$', views.review_landing, name="review_landing"),
    url(r'^data-reset/$', views.data_reset, name="data_reset"),
    url(r'^results/$', views.review_results, name="review_results"),
    url(r'^extract-data/$', views.extract_review_data, name="extract_review_data"),
    url(r'^recommendation/$', views.expert_recommendation, name="review_expert_recommendation"),
    url(r'^recommendation/(?P<session_id>[-\w]+)/$', views.expert_recommendation, name="review_expert_recommendation"),
    url(r'^product/add/$', views.add_product, name="add_recommendation_product"),
    url(r'^product/(?P<product_id>[-\d]+)/edit/$', views.add_product, name="edit_recommendation_product"),
    url(r'^product/(?P<product_id>[-\d]+)/delete/$', views.delete_product, name="delete_recommendation_product"),
    url(r'^product/(?P<product_id>[-\d]+)/feature/add/$', views.add_feature, name="add_recommendation_product_feature"),
    url(r'^product/(?P<product_id>[-\d]+)/feature/(?P<feature_id>[-\d]+)/edit/$',
        views.add_feature, name="edit_recommendation_product_feature"),
    url(r'^product/(?P<product_id>[-\d]+)/feature/(?P<feature_id>[-\d]+)/delete/$',
        views.delete_feature, name="delete_recommendation_product_feature"),

    url(r'^expert-review/user-list/$', views.expert_review_user_list, name="expert_review_user_list"),
    url(r'^response/(?P<session_id>[-\w]+)/add/$', views.add_expert_response, name="add_expert_response"),
    url(r'^response/(?P<session_key>[-\w]+)/mark-complete/$',
        views.mark_response_as_completed, name="mark_response_as_completed"),
    url(r'^get-user-data/(?P<session_id>[-\w]+)/$', views.get_user_data, name="expert_review_get_user_data"),
    url(r'^get-mail-data/(?P<session_id>[-\w]+)/$', views.get_mail_data, name="expert_review_get_mail_data"),
    url(r'^save-mail-data/$', views.save_mail_data, name="expert_review_save_mail_data"),
    url(r'^save-call-time/$', views.save_call_time, name="expert_review_save_call_time"),
    url(r'^edit-recommendation/(?P<session_id>[-\w]+)/$',
        views.edit_expert_recommendation, name="review_expert_recommendation_edit"),
    url(r'^expert-mail/(?P<session_id>[-\w]+)/$', views.expert_mail, name="review_expert_mail"),

    fhurl(r'^ajax_review_extra_data/$', views.ExtraInfoForm, name="ajax_review_extra_data", template=None, json=True),
]
