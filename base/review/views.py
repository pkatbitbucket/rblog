import datetime
import hashlib
import logging
import utils

from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.conf import settings
from django.core.urlresolvers import reverse

from django.shortcuts import redirect, render_to_response, get_object_or_404
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.core import serializers
from django.forms.models import modelformset_factory
from django.forms.formsets import formset_factory
from django.contrib.auth import authenticate, login
from django.middleware import csrf
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from collections import OrderedDict
from django.core.mail import send_mail
from putils import render_excel

from core.models import User
from review.models import *
from review.forms import *
from review.rev_utils import *

import pprint
import putils
from customdb.customquery import sqltojson, sqltodict, executesql
from xl2python import Xl2Python


@never_cache
def add_info(request):
    """
    Reaches here from: /soc-login/
    """
    cover_dict = None
    risks = []

    tracker = request.TRACKER
    if not tracker.activity_set.filter(event='INSTANT_REVIEW'):
        activity = Activity(event='INSTANT_REVIEW', tracker=tracker)
        activity.save()
    else:
        activity = tracker.activity_set.get(event='INSTANT_REVIEW')

    if request.POST:
        form = ReviewDataForm(request.POST)
        if form.is_valid():
            tracker, activity = form.save(tracker, activity)
            activity = get_all_risks(activity)
            gc = GetCover(activity)
            activity = gc.calculate_cover()
            cover_dict = activity.extra['cover']
            risks_q = Risk.objects.filter(code__in=activity.extra['risks'])
            risks = list(risks_q)
            risks.sort(key=lambda r: activity.extra['risks'].index(r.code))
            for code in activity.extra['products']:
                prod = Product.objects.get(code=code)
                if cover_dict[code]['level'] == 1:
                    cover_dict[code]['name'] = prod.level1
                elif cover_dict[code]['level'] == 2:
                    cover_dict[code]['name'] = prod.level2
                else:
                    cover_dict[code]['name'] = prod.name
            return HttpResponseRedirect('/review/results/')
        else:
            print form.errors
    else:

        criteria = ReviewDataForm().fields.keys()
        init_dict = {}
        for c in criteria:
            if c in tracker.extra.keys():
                init_dict[c] = tracker.extra[c]
            if c in activity.extra.keys():
                init_dict[c] = activity.extra[c]
        if not 'birth_date' in init_dict.keys():
            init_dict['birth_date'] = 'dd/mm/yyyy'
        if 'email' in tracker.extra.keys() and tracker.extra['email']:
            init_dict['email'] = tracker.extra['email']
        if 'first_name' in tracker.extra.keys() and tracker.extra['first_name']:
            if 'last_name' in tracker.extra.keys() and tracker.extra['last_name']:
                init_dict['name'] = "%s %s" % (
                    tracker.extra['first_name'], tracker.extra['last_name'])
            else:
                init_dict['name'] = tracker.extra['first_name']

        form = ReviewDataForm(initial=init_dict)
    return render_to_response('review/add_info.html', {
        'form': form,
        'cover_dict': cover_dict,
        'risks': risks,
        'tracker': tracker,
    }, RequestContext(request))


@login_required
@never_cache
def data_reset(request):
    u = request.user
    criteria = ReviewDataForm().fields.keys()
    for c in criteria:
        u.extra[c] = ''
    u.birth_date = None
    u.save()
    return HttpResponseRedirect('/review/info-add/')


@utils.review_tracking_required
@never_cache
def review_results(request):
    tracker = request.TRACKER
    if not tracker.activity_set.filter(event='INSTANT_REVIEW'):
        return HttpResponseRedirect('/')
    else:
        activity = tracker.activity_set.get(event='INSTANT_REVIEW')

    grt = GetRiskText(activity)
    risk_text = grt.get_risk_texts()
    gpt = GetProductText(activity)
    prod_dict = gpt.get_product_texts()
    risk_dict = OrderedDict([(r, Risk.objects.get(code=r))
                             for r in activity.extra['risks']])

    prod_text = []
    extra_data = activity.extra
    for k, v in prod_dict.items():
        if v:
            prod = Product.objects.get(code=k)
            prod_risks = ",".join(['#risk_tag_id_%s' %
                                   r.code for r in prod.risk_set.all()])
            prod_risk_names = [[r.risk, r.code] for r in prod.risk_set.all(
            ) if r.code in activity.extra['risks']]
            if extra_data['cover'][k]['level'] == 1:
                pname = prod.level1
            elif extra_data['cover'][k]['level'] == 2:
                pname = prod.level2
            else:
                pname = prod.name
            # Handling exception for term cover
            if k in ['P013', 'P017'] and extra_data['cover'][k]['level'] == 1:
                if 'R008' in activity.extra['risks']:
                    r = Risk.objects.get(code='R008')
                    if [r.risk, r.code] not in prod_risk_names:
                        prod_risk_names.append([r.risk, r.code])
                if 'R009' in activity.extra['risks']:
                    r = Risk.objects.get(code='R009')
                    if [r.risk, r.code] not in prod_risk_names:
                        prod_risk_names.append([r.risk, r.code])
            #<end>

            pd = {
                'meta': {
                    'id': 'id_%s' % (prod.code.lower()),
                    'name': pname,
                    'risks': prod_risks,
                    'risk_names': prod_risk_names,
                    'sumInsured': extra_data['cover'][prod.code]['sum'],
                },
                'features': [
                    {
                        'title': 'Know the product',
                        'description': v['A']
                    },
                    {
                        'title': 'What it covers',
                        'description': v['B']
                    },
                    {
                        'title': 'What to look for?',
                        'description': v['C']
                    },
                    {
                        'title': 'How much to buy and why?',
                        'description': v['D']
                    },
                ]
            }
            prod_text.append(pd)

    prod_text = json.dumps(prod_text)
    extra_data_form = ExtraInfoForm()

    # if request.is_IE:
    #    template_name = 'results_ie'
    # else:
    template_name = 'results'
    return render_to_response('review/%s.html' % template_name, {
        'tracker': tracker,
        'activity': activity,
        'risk_text': risk_text,
        'prod_text': prod_text,
        'risk_dict': risk_dict,
        'form': extra_data_form,
    }, RequestContext(request))


@never_cache
def review_landing(request):
    """
    Reaches here from: /soc-login/
    """
    cover_dict = None
    risks = []

    tracker = request.TRACKER
    if 'no_show' not in tracker.extra.keys():
        tracker.extra['no_show'] = False
        tracker.save()
    if not tracker.activity_set.filter(event='INSTANT_REVIEW'):
        activity = Activity(event='INSTANT_REVIEW', tracker=tracker)
        activity.save()
    else:
        activity = tracker.activity_set.get(event='INSTANT_REVIEW')

    if request.POST:
        tracker.extra['no_show'] = True
        tracker.save()
        form = ReviewDataForm(request.POST)
        if form.is_valid():
            tracker, activity = form.save(tracker, activity)
            activity = get_all_risks(activity)
            gc = GetCover(activity)
            activity = gc.calculate_cover()
            cover_dict = activity.extra['cover']
            risks_q = Risk.objects.filter(code__in=activity.extra['risks'])
            risks = list(risks_q)
            risks.sort(key=lambda r: activity.extra['risks'].index(r.code))
            for code in activity.extra['products']:
                prod = Product.objects.get(code=code)
                if cover_dict[code]['level'] == 1:
                    cover_dict[code]['name'] = prod.level1
                elif cover_dict[code]['level'] == 2:
                    cover_dict[code]['name'] = prod.level2
                else:
                    cover_dict[code]['name'] = prod.name
            return HttpResponseRedirect('/review/results/')
        else:
            print form.errors
    else:
        criteria = ReviewDataForm().fields.keys()
        init_dict = {}
        for c in criteria:
            if c in tracker.extra.keys():
                init_dict[c] = tracker.extra[c]
            if c in activity.extra.keys():
                init_dict[c] = activity.extra[c]
        if not 'birth_date' in init_dict.keys():
            init_dict['birth_date'] = 'dd/mm/yyyy'
        if 'email' in tracker.extra.keys() and tracker.extra['email']:
            init_dict['email'] = tracker.extra['email']
        if 'first_name' in tracker.extra.keys() and tracker.extra['first_name']:
            if 'last_name' in tracker.extra.keys() and tracker.extra['last_name']:
                init_dict['name'] = "%s %s" % (
                    tracker.extra['first_name'], tracker.extra['last_name'])
            else:
                init_dict['name'] = tracker.extra['first_name']

        form = ReviewDataForm(initial=init_dict)
    return render_to_response('review/review_landing.html', {
        'form': form,
        'cover_dict': cover_dict,
        'risks': risks,
        'tracker': tracker,
    }, RequestContext(request))


@login_required
@utils.profile_type_only('ADMIN', 'REPORT')
def extract_review_data(request):
    data_row_list = []
    for a in Activity.objects.filter(event='INSTANT_REVIEW'):
        review_extra_data = ''
        tracker_data = ''
        activity_data = ''
        if 'review_extra_data' in a.extra:
            for k, v in a.extra['review_extra_data'].items():
                review_extra_data = '%s%s : %s\n' % (review_extra_data, k, v)
        for k, v in a.tracker.extra.items():
            tracker_data = '%s%s : %s\n' % (tracker_data, k, v)
        for k, v in a.extra.items():
            if k != 'review_extra_data':
                activity_data = '%s%s : %s\n' % (activity_data, k, v)
        try:
            temp_tuple = [
                a.extra['name'],
                a.extra['email'],
                review_extra_data,
                tracker_data,
                activity_data,
            ]
            data_row_list.append(temp_tuple)
        except:
            pass

    order = [
        'Name',
        'Email',
        'Review Extra Data',
        'Tracker Data',
        'Activity Data',
    ]

    filename = "review_data.xls"
    if data_row_list:
        return render_excel(filename, order, data_row_list)
    else:
        return HttpResponse("No records found")


@never_cache
def expert_recommendation(request, session_id=None):
    """
    Ex. yst1d3uvnhaxfrowxsxbzpv165pus64h
    """
    if not session_id:
        tracker = request.TRACKER
    else:
        tracker = Tracker.objects.get(session_key=session_id)
    if not tracker.activity_set.filter(event='INSTANT_REVIEW'):
        return HttpResponseRedirect('/review/start/')
    else:
        activity = tracker.activity_set.get(event='INSTANT_REVIEW')

    risk_list = []
    product_dict = {}
    for r in activity.extra['risks']:
        risk = Risk.objects.get(code=r)
        product_dict[risk.code] = risk.products.filter(
            code__in=activity.extra['cover'].keys())
        risk_list.append(risk)

    return render_to_response('review/expert_recommendation.html',  {
        'tracker': tracker,
        'activity': activity,
        'risk_list': risk_list,
        'product_dict': product_dict,
        'session_id': session_id
    }, RequestContext(request))


@login_required
@utils.profile_type_only('ADMIN', 'REPORT')
@never_cache
def edit_expert_recommendation(request, session_id=None):
    """
    Ex. yst1d3uvnhaxfrowxsxbzpv165pus64h
    """
    if not session_id:
        tracker = request.TRACKER
    else:
        tracker = Tracker.objects.get(session_key=session_id)
    if not tracker.activity_set.filter(event='INSTANT_REVIEW'):
        return HttpResponseRedirect('/review/start/')
    else:
        activity = tracker.activity_set.get(event='INSTANT_REVIEW')

    risk_list = []
    product_dict = {}
    for r in activity.extra['risks']:
        risk = Risk.objects.get(code=r)
        product_dict[risk.code] = risk.products.filter(
            code__in=activity.extra['cover'].keys())
        risk_list.append(risk)

    return render_to_response('review/edit_expert_recommendation.html',  {
        'tracker': tracker,
        'activity': activity,
        'risk_list': risk_list,
        'product_dict': product_dict,
        'session_id': session_id
    }, RequestContext(request))


@login_required
@utils.profile_type_only('ADMIN', 'REPORT')
def add_product(request, product_id=None):
    if product_id:
        product = MarketProduct.objects.get(id=product_id)
    if request.POST:
        if product_id:
            form = ProductAddForm(
                request.POST, request.FILES, instance=product)
        else:
            form = ProductAddForm(request.POST, request.FILES)
        if form.is_valid():
            p = form.save()
            form = ProductAddForm()
        else:
            print form.errors
    else:
        if product_id:
            form = ProductAddForm(instance=product)
        else:
            form = ProductAddForm()
    product_list = MarketProduct.objects.all()
    return render_to_response('review/add_product.html',  {
        'form': form,
        'product_list': product_list
    }, RequestContext(request))


@login_required
@utils.profile_type_only('ADMIN', 'REPORT')
def add_feature(request, product_id, feature_id=None):
    product = MarketProduct.objects.get(id=product_id)
    if feature_id:
        feature = MarketProductFeature.objects.get(id=feature_id)
    if request.POST:
        if feature_id:
            form = FeatureAddForm(
                request.POST, request.FILES, instance=feature)
        else:
            form = FeatureAddForm(request.POST, request.FILES)
        if form.is_valid():
            feature = form.save()
            feature.market_product = product
            feature.save()

            form = FeatureAddForm()
        else:
            print form.errors
    else:
        if feature_id:
            form = FeatureAddForm(instance=feature)
        else:
            form = FeatureAddForm()
    feature_list = MarketProductFeature.objects.filter(market_product=product)
    return render_to_response('review/add_feature.html',  {
        'form': form,
        'feature_list': feature_list,
        'product': product
    }, RequestContext(request))


@login_required
@utils.profile_type_only('ADMIN', 'REPORT')
def delete_product(request, product_id):
    product = MarketProduct.objects.get(id=product_id)
    product.delete()
    return HttpResponseRedirect('/review/product/add/')


@login_required
@utils.profile_type_only('ADMIN', 'REPORT')
def delete_feature(request, product_id, feature_id):
    feature = MarketProductFeature.objects.get(id=feature_id)
    feature.delete()
    return HttpResponseRedirect('/review/product/%s/feature/add/' % product_id)


@login_required
@utils.profile_type_only('ADMIN', 'REPORT')
def expert_review_user_list(request):
    user_list = []
    for a in Activity.objects.filter(event='INSTANT_REVIEW'):
        if 'review_extra_data' in a.extra:
            user_list.append(a)
    return render_to_response('review/expert_review_user_list.html',  {
        'user_list': user_list,
    }, RequestContext(request))


@login_required
@utils.profile_type_only('ADMIN', 'REPORT')
def add_expert_response(request, session_id):
    tracker = Tracker.objects.get(session_key=session_id)
    activity = Activity.objects.filter(
        event='INSTANT_REVIEW', tracker=tracker)[0]
    if 'response_data' not in activity.extra.keys():
        activity.extra['response_data'] = []

    product_list = activity.extra['cover'].keys()
    if request.POST:
        form = ExpertReviewResponseForm(product_list, request.POST)
        if form.is_valid():
            res_data = {}
            product = form.cleaned_data['product']
            res_data['risk_code_list'] = [
                r.code for r in product.risk_set.all()]
            res_data['risk_name_list'] = [
                r.risk for r in product.risk_set.all()]

            res_data['market_product_id'] = form.cleaned_data[
                'market_product'].id
            res_data['product_code'] = product.code
            cover_dict = activity.extra['cover'][product.code]
            if cover_dict['level'] == 0:
                res_data['product_name'] = product.name
            elif cover_dict['level'] == 1:
                res_data['product_name'] = product.level1
            else:
                res_data['product_name'] = product.level2
            res_data['recommended_cover'] = cover_dict['sum']

            res_data['cover'] = form.cleaned_data['cover']
            res_data['premium'] = form.cleaned_data['premium']
            activity.extra['response_data'].append(res_data)
            activity.save()
            form = ExpertReviewResponseForm(product_list=product_list)
        else:
            print form.errors
    else:
        if 'response_data' in activity.extra.keys():
            form = ExpertReviewResponseForm(product_list=product_list)
        else:
            form = ExpertReviewResponseForm()

    added_data = activity.extra['response_data']

    return render_to_response('review/add_expert_response.html',  {
        'activity': activity,
        'form': form,
        'added_data': added_data,
        'session_key': session_id
    }, RequestContext(request))


def get_user_data(request, session_id):
    tracker = Tracker.objects.get(session_key=session_id)
    activity = Activity.objects.filter(
        event='INSTANT_REVIEW', tracker=tracker)[0]

    data = {}
    if 'first_name' in tracker.extra.keys() and tracker.extra['first_name'].strip():
        data['name'] = tracker.extra['first_name'].strip()
    else:
        data['name'] = activity.extra['name']
    data['product'] = {}
    for i in activity.extra['response_data']:
        if i['product_code'] not in data['product'].keys():
            data['product'][i['product_code']] = {
                'code': i['product_code'],
                'name': i['product_name'],
                'risk_name_list': i['risk_name_list'],
                'risk_code_list': i['risk_name_list'],
                'recommended_cover': i['recommended_cover'],
                'market_products': []
            }
        data['product'][i['product_code']]['market_products'].append({
            'id': i['market_product_id'],
            'name': MarketProduct.objects.get(id=i['market_product_id']).name,
            'url': MarketProduct.objects.get(id=i['market_product_id']).logo.url,
            'features': [f.content for f in MarketProductFeature.objects.filter(market_product__id=i['market_product_id'])],
            'cover': i['cover'],
            'premium': i['premium']
        })
    return HttpResponse(json.dumps(data))


@never_cache
def get_mail_data(request, session_id):
    tracker = Tracker.objects.get(session_key=session_id)
    activity = Activity.objects.filter(
        event='INSTANT_REVIEW', tracker=tracker)[0]
    data = activity.extra['final_mail_data']
    return HttpResponse(json.dumps(data))


@login_required
@utils.profile_type_only('ADMIN', 'REPORT')
def mark_response_as_completed(request, session_key):
    tracker = Tracker.objects.get(session_key=session_key)
    activity = Activity.objects.filter(
        event='INSTANT_REVIEW', tracker=tracker)[0]
    activity.extra['is_response_entry_completed'] = True
    activity.save()
    return HttpResponseRedirect('/review/expert-review/user-list/')


def save_mail_data(request):
    session_id = request.POST['session_id']
    tracker = Tracker.objects.get(session_key=session_id)
    activity = Activity.objects.filter(
        event='INSTANT_REVIEW', tracker=tracker)[0]
    pprint.pprint(request.POST['data'])
    activity.extra['final_mail_data'] = request.POST['data']
    activity.save()
    return HttpResponse(json.dumps({'success': True}))


def save_call_time(request):
    session_id = request.POST['session_id']
    tracker = Tracker.objects.get(session_key=session_id)
    activity = Activity.objects.filter(
        event='INSTANT_REVIEW', tracker=tracker)[0]
    activity.extra['call_time_data'] = json.loads(request.POST['data'])
    activity.save()
    msg_text = 'Name: %s\n' % activity.extra['name']
    msg_text = '%sMobile: %s\n' % (msg_text, activity.extra[
                                   'call_time_data']['mobile'])
    msg_text = '%sLink to profile: %s\n' % (
        msg_text, 'http://www.coverfox.com/review/recommendation/%s/' % tracker.session_key)
    msg_text = '%sLocation : %s\n' % (msg_text, activity.extra['location'])
    slot_list = []
    for i in activity.extra['call_time_data']['todaySlots']:
        if 'slot_selected' in i.keys() and i['slot_selected'].upper() == 'YES':
            slot_list.append('Today(%s): %s' % (
                activity.extra['call_time_data']['todayText'], i['slot']))
    for i in activity.extra['call_time_data']['tommSlots']:
        if 'slot_selected' in i.keys() and i['slot_selected'].upper() == 'YES':
            slot_list.append('Tomorrow(%s): %s' % (
                activity.extra['call_time_data']['tommText'], i['slot']))
    for i in activity.extra['call_time_data']['afterSlots']:
        if 'slot_selected' in i.keys() and i['slot_selected'].upper() == 'YES':
            slot_list.append('Day After(%s): %s' % (
                activity.extra['call_time_data']['afterText'], i['slot']))
    msg_text = '%sSlots: %s\n' % (msg_text, ', '.join(slot_list))
    sub_text = 'ALERT: Call scheduled with a customer'
    send_mail(sub_text, msg_text, "Dev Bot <dev@cfstatic.org>",
              ['support@coverfox.com'])

    return HttpResponse(json.dumps({'success': True}))


@never_cache
def expert_mail(request, session_id=None):
    """
    Ex. yst1d3uvnhaxfrowxsxbzpv165pus64h
    """
    if not session_id:
        tracker = request.TRACKER
    else:
        tracker = Tracker.objects.get(session_key=session_id)
    if not tracker.activity_set.filter(event='INSTANT_REVIEW'):
        return HttpResponseRedirect('/review/start/')
    else:
        activity = tracker.activity_set.get(event='INSTANT_REVIEW')

    return render_to_response('review/expert_mail.html',  {
        'tracker': tracker,
        'activity': activity,
        'session_id': session_id
    }, RequestContext(request))
