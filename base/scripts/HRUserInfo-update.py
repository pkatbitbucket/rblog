import export_settings  # NOQA

from hrms.models import HRUserInfo
from xlrd import open_workbook

wb = open_workbook('HRUserInfo.xlsx')

sheet = wb.sheets()[0]
for row in range(1, sheet.nrows):
    company = sheet.cell(row, 1).value
    emp_code = sheet.cell(row, 3).value.strip()

    if company == 'Glitterbug':
        emp_code = '{0}/{1}'.format(emp_code[:2], emp_code[2:])

    emp = HRUserInfo.objects.filter(member__employee_id=emp_code).first()
    if emp:
        emp.reporters.clear()
        emp.peers.clear()


for row in range(1, sheet.nrows):
    company = sheet.cell(row, 1).value
    emp_code = sheet.cell(row, 3).value.strip()

    if company == 'Glitterbug':
        emp_code = '{0}/{1}'.format(emp_code[:2], emp_code[2:])

    emp = HRUserInfo.objects.filter(member__employee_id=emp_code).first()
    if emp:
        reportee_id = sheet.cell(row, 9).value.strip()
        if reportee_id.startswith('GB'):
            reportee_id = '{0}/{1}'.format(reportee_id[:2], reportee_id[2:])
        reportee = HRUserInfo.objects.filter(member__employee_id=reportee_id).first()
        if reportee:
            reportee.reporters.add(emp)
        else:
            print '{} ,Reportee,{}, NA'.format(emp_code, reportee_id)

        for i in range(11, 16, 2):
            peer_id = sheet.cell(row, i).value.strip()
            if peer_id.startswith('GB'):
                peer_id = '{0}/{1}'.format(peer_id[:2], peer_id[2:])
            if peer_id != 'NA':
                peer = HRUserInfo.objects.filter(member__employee_id=peer_id).first()
                if peer:
                    emp.peers.add(peer)
                else:
                    print '{},Peer,{}, NA'.format(emp_code, peer_id)

    else:
        print '{}, NA'.format(emp_code)
