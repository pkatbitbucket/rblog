import sys
import time
import datetime
import os
import csv
import ast

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from customdb.customquery import sqltojson, sqltodict
from django.core.mail import send_mail
import thread
import threading
from datetime import timedelta
from django.utils.encoding import smart_str, smart_unicode

from core.models import User
from health_product.models import *
import urllib2
import urllib
from dateutil.relativedelta import *
import json


from health_product.models import *
from mailer.models import *

for t in Transaction.objects.all():
    to_save = False
    if 'members' in t.extra.keys():
        for m in t.extra['members']:
            if not 'age' in m:
                m['dob'] = m['dob'].replace('-', '/')
                m['age'] = relativedelta(datetime.datetime.today(
                ), datetime.datetime.strptime(m['dob'], '%d/%m/%Y').date()).years
                to_save = True
    if to_save:
        t.save()

for t in Activity.objects.filter(activity_type='RESULTS_VISITED'):
    to_save = False
    try:
        t.extra.keys()
    except:
        t.extra = json.loads(t.extra)
        t.save()
    if 'members' in t.extra.keys():
        for m in t.extra['members']:
            if not 'age' in m:
                m['dob'] = m['dob'].replace('-', '/')
                m['age'] = relativedelta(datetime.datetime.today(
                ), datetime.datetime.strptime(m['dob'], '%d/%m/%Y').date()).years
                to_save = True
    if to_save:
        t.save()
