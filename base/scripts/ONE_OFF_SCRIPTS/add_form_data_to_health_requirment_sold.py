import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from core import models as core_models
from wiki import models as wiki_models
from crm import models as crm_models
import statemachine.models as sm_models
from motor_product.models import Transaction as MotorTransaction
from travel_product.models import Transaction as TravelTransaction
from health_product.models import Transaction as HealthTransaction
from core import forms as core_forms


def find_matching_key_val_in_dict(pattern, d):
    x = {}
    for k, v in d.items():
        if k.startswith('medical'):
            x.update({k: v})
    return x


no_form_data_key_trids = []
exceptional_keys_map = {
    'address1': 'street',
    'address2': 'area',
}

def get_key_value_dict(d):
    x = {}
    for k, v in d.items():
        value_keys = []
        for key, value in v.items():
            if key.startswith('value'):
                value_keys.append(key)
        val = ''
        if len(value_keys) > 1:
            for item in value_keys:
                val += v[item]
        elif len(value_keys) == 1:
            val = v[value_keys[0]]
        x.update({k: val})
    return x

product_transaction_map = {
    'motor': 'motor_transaction',
    'bike': 'motor_transaction',
    'health': 'health_transaction',
    'travel': 'travel_transaction',
}

for p in Policy.objects.filter(status__in=['COMPLETED', 'MANUAL_COMPLETED'])[:100]:
    tr = getattr(p, product_transaction_map[p.requirement_sold.kind.product])
    print p.id, tr.id
    proposal_data = tr.insured_details_json.get('form_data')
    if proposal_data:
        proposal_medical_data = find_matching_key_val_in_dict('medical', proposal_data)
        for k, v in proposal_medical_data.items():
            del proposal_data[k]
        proposer_details = {}
        for k, v in proposal_data.items():
            print k
            if k == 'proposer':
                for key_type, val in v.items():
                    proposer_details.update(get_key_value_dict(val))
            if k == 'insured':
                members_data = []
                for key_type, val in v.items():
                    if key_type == 'self':
                        proposer_details.update(get_key_value_dict(val))
                    elif key_type.startswith('son'):
                        x = get_key_value_dict(val)
                        x.update({'role': 'son'})
                        members_data.append(x)
                    elif key_type.startswith('daughter'):
                        x = get_key_value_dict(val)
                        x.update({'role': 'daughter'})
                        members_data.append(x)
                    else:
                        x = get_key_value_dict(val)
                        x.update({'role': key_type})
                        members_data.append(x)

    else:
        no_form_data_key_trids.append(tr.id)
