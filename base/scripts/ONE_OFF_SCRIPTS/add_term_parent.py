import sys
import time
import datetime
import os
from optparse import OptionParser

import re

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from django.db import models
from wiki.models import *


def main():
    for t in Term.objects.all():
        t.name = t.name.lower().title()
        t.name = re.sub(' +', ' ', t.name)
        t.save()

    f = open('scripts/terms.csv').read()
    l = f.split("\n")
    l.remove('')
    for line in l:
        parent, term, is_enabled = line.split('|')
        term = re.sub(' +', ' ', term.lower().title())
        t = Term.objects.get(name=term)

        tp = TermParent.objects.filter(name=parent.strip().lower().title())
        if not tp:
            tp = TermParent(name=parent.strip().lower().title())
            tp.save()
        else:
            tp = tp[0]

        print parent, is_enabled
        if is_enabled == '1':
            tp.is_enabled = True
            tp.save()

        t.parent = tp
        t.save()

if __name__ == "__main__":
    main()
