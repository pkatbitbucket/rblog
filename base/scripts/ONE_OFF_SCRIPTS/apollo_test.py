import os
import sys
from optparse import OptionParser
import requests

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

import logging

import suds
from suds.client import Client
from wiki.models import *

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.transport.http').setLevel(logging.DEBUG)

from utils import request_logger
from health_product.models import *
from health_product.insurer import settings as insurer_settings
from importlib import import_module
from health_product.insurer import custom_utils


def try_premium(transaction_id):
    transaction = Transaction.objects.get(transaction_id=transaction_id)
    insurer_module = insurer_settings.INSURER_MAP[
        transaction.slab.health_product.insurer.id]
    m = import_module("health_product.insurer.%s.integration" % insurer_module)
    premium_client = Client(m.PREMIUM_WSDL)
    premium_request = premium_client.service.ComputePremium
    premium_obj = m.get_premium_obj(premium_client, transaction)
    resp = premium_request(premium_obj)
    response_dict = custom_utils.suds_to_dict(resp)
    print "\n\n"
    print response_dict
    transaction.extra['premium_response'] = response_dict[
        'Client'][0]['Product']['Product'][0]
    transaction.premium_paid = response_dict['Client'][0]['TotalPremium']
    transaction.save()
    return


def try_proposal(transaction_id):
    transaction = Transaction.objects.get(transaction_id=transaction_id)
    insurer_module = insurer_settings.INSURER_MAP[
        transaction.slab.health_product.insurer.id]
    m = import_module("health_product.insurer.%s.integration" % insurer_module)
    proposal_client = Client(m.PROPOSAL_WSDL)
    proposal_request = proposal_client.service.ProposalCapture
    proposal_obj = m.get_proposal_obj(proposal_client, transaction)
    # print proposal_obj
    print "\n\n"
    try:
        resp = proposal_request(proposal_obj)
        print 'RESPONSE: :', resp
    except suds.WebFault, e:
        # from IPython.frontend.terminal.embed import InteractiveShellEmbed
        # InteractiveShellEmbed()()
        print "SOME ERROR OCCURED"
        print e.message
        print e.document
    return


def try_payment(transaction_id):
    transaction = Transaction.objects.get(transaction_id=transaction_id)
    insurer_module = insurer_settings.INSURER_MAP[
        transaction.slab.health_product.insurer.id]
    m = import_module("health_product.insurer.%s.integration" % insurer_module)
    payment_client = Client(m.PAYMENT_WSDL)
    payment_request = payment_client.service.PaymentDetails
    payment_obj = m.get_payment_obj(payment_client, transaction)

    # print proposal_obj
    print "\n\n"
    try:
        resp = payment_request(payment_obj)
        response_dict = custom_utils.suds_to_dict(resp)
        print 'RESPONSE: :', response_dict
    except suds.WebFault, e:
        # from IPython.frontend.terminal.embed import InteractiveShellEmbed
        # InteractiveShellEmbed()()
        print "SOME ERROR OCCURED"
        print e.message
        print e.document
    return


def try_verification(transaction_id):
    transaction = Transaction.objects.get(transaction_id=transaction_id)
    insurer_module = insurer_settings.INSURER_MAP[
        transaction.slab.health_product.insurer.id]
    m = import_module("health_product.insurer.%s.integration" % insurer_module)

    verification_client = Client(m.VERIFICATION_WSDL)
    # verification_client.set_options(nosend=True)
    partner_obj = verification_client.factory.create('ns1:Partner')
    partner_obj.PartnerCode = m.PARTNER_CODE
    partner_obj.UserName = m.USERNAME
    partner_obj.Password = m.PASSWORD
    verification_request = verification_client.service.VerifyTransaction
    ver_obj = verification_client.factory.create(
        'ns2:TransactionVerificationRequest')
    ver_obj.Partner = partner_obj
    ver_obj.PaymentId = transaction.payment_token
    ver_obj.TransactionId = transaction.policy_token

    try:
        resp = verification_request(ver_obj)
        # from IPython.frontend.terminal.embed import InteractiveShellEmbed
        # InteractiveShellEmbed()()
        request_logger.info('\nAPOLLO MUNICH TRANSACTION: %s \n VERIFICATION REQUEST SENT:\n %s \n' % (
            transaction.transaction_id, verification_client.last_sent()))
        request_logger.info('\nAPOLLO MUNICH TRANSACTION: %s \n VERIFICATION RESPONSE RECEIVED:\n %s \n' % (
            transaction.transaction_id, verification_client.last_received()))

        if str(resp) == '0':
            transaction.payment_done = True
            transaction.payment_on = datetime.datetime.today()
    except suds.WebFault, error:
        request_logger.info('\nAPOLLO MUNICH TRANSACTION: %s \n VERIFICATION ERROR:\n %s \n' % (
            transaction.transaction_id, error.document))

    return


def main():
    usage = "usage: %prog -t <transaction id>"
    parser = OptionParser(usage)

    parser.add_option("-t", "--transaction_id", type="string",
                      dest="transaction_id", help="transction id to process")
    (options, args) = parser.parse_args()

    if not options.transaction_id:
        parser.error("Must specify transaction id you want to process")

    transaction_id = options.transaction_id.strip()
    # try_premium(transaction_id)
    # try_proposal(transaction_id)
    # try_payment(transaction_id)
    try_verification(transaction_id)


if __name__ == "__main__":
    main()
