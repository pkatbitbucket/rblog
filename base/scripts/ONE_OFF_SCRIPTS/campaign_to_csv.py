import sys
import time
import datetime
import os
from optparse import OptionParser
from dateutil.relativedelta import relativedelta

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from django.db import models
import csv
import xlwt
from wiki.models import *
import pprint
import utils
from xl2python import Xl2Python
from campaign.models import *

# FOR BRANDED
# for k in Keyword.objects.filter(ad_group__camp='COMPETITOR-1'):
#     if int(k.slug.split('-')[-1]) == 1:
#         print '%s|%s|%s|http://www.coverfox.com/outreach/%s/plan/%s/' % (
#             k.extra['campaign_name'].strip(),
#             k.extra['ad_group'].strip(),
#             k.title.strip(),
#             k.ad_group.insurer.slug,
#             k.slug
#         )

# FOR CORE
for k in Keyword.objects.filter(created__gte=(datetime.datetime.now() - relativedelta(days=1))):
    if int(k.slug.split('-')[-1]) != 1:
        print '%s|%s|%s|http://www.coverfox.com/outreach/%s/' % (
            k.extra['campaign_name'].strip(),
            k.extra['ad_group'].strip(),
            k.title.strip(),
            k.slug
        )
