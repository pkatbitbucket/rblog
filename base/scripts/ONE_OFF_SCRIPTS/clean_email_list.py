import sys
import time
import datetime
import os
from optparse import OptionParser

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from django.db import models

from django.core.exceptions import ValidationError
from django.core.validators import validate_email

import re
black_list = ['test', 'quality', 'qk', 'abc', 'adf', 'asd', 'asf', 'dfd', 'dfg', 'dgf', 'fdg', 'fgd', 'fgh', 'fgv', 'fsd', 'gde', 'gdf', 'gfd', 'gsd', 'hjk', 'pks', 'pqr', 'sdf', 'sdg', 'sds', 'xyz', 'aaa', 'bbb', 'ccc', 'ddd', 'eee', 'fff',
              'ggg', 'hhh', 'iii', 'jjj', 'kkk', 'lll', 'mmm', 'nnn', 'ooo', 'ppp', 'qqq', 'rrr', 'sss', 'ttt', 'uuu', 'vvv', 'www', 'xxx', 'yyy', 'zzz', 'as@as', 'gft', 'rtr', 'sfd', 'sfs', 'jnj', 'jkl', 'hijk', 'djk', 'sdj', 'dsad', 'lkj', 'lkh', 'fakh']
to_reject = re.compile("|".join(black_list))

f = open('/home/rane/Desktop/leads.csv', 'r').read()

for l in f.split('\n'):
    if not l:
        continue

    name, email = l.split('|')
    if to_reject.search(email.lower()):
        continue
    if to_reject.search(name.lower()):
        continue

    if len(email.split('@')[0]) < 4:
        continue

    if len(email.split('@')[1].split('.')[0]) < 4:
        continue

    if not email:
        continue
    try:
        validate_email(email)
    except ValidationError as e:
        print "Invalid: ", email
    print "%s|%s" % (name.lower().title(), email.lower())
