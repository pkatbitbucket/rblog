import sys
import time
import datetime
import os
from optparse import OptionParser

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from django.db import models
import csv
import xlwt
from wiki.models import *
import pprint
import utils
from xl2python import Xl2Python
from campaign.models import *


upload_field_map = [
    ('ad_group', 'trimmed'),
    ('headline', 'trimmed'),
    ('line1', 'trimmed'),
    ('line2', 'trimmed'),
    ('url', 'trimmed'),
]


insurer_dict = {
    2: "star",
    4: "bajaj",
    6: "religare",
    9: "bharti",
    10: "reliance",
    11: "l&t",
}


class Uploader(Xl2Python):
    pass


################## CHANGE DATA #########################
xfile = open('ad_copy.xls', 'r')
campaign_name = 'COMPETITOR'
########################################################


xlm = Uploader(upload_field_map, xfile)
data = None
if xlm.is_valid():
    data = xlm.cleaned_data
else:
    print xlm.errors

# pprint.pprint(data)
final_list = []

for k, v in insurer_dict.items():
    ins = Insurer.objects.get(id=k)
    for nodey in data:
        elem = {}
        for kk, vv in nodey.items():
            key_str = vv.replace('BRAND', v.upper()).strip()
            key_str = key_str.replace('Brand', v.title()).strip()
            key_str = key_str.replace('brand', v.title()).strip()
            if len(key_str) > 35:
                key_str = key_str.replace('india', 'www')
            elem[kk] = key_str

        # pprint.pprint(elem)
        final_list.append(elem)

for ele in final_list:
    print '%s|%s|%s|%s|%s' % (ele['ad_group'], ele['headline'], ele['line1'], ele['line2'], ele['url'])
