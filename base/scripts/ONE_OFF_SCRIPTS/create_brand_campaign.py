import sys
import time
import datetime
import os
from optparse import OptionParser

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from django.db import models
import csv
import xlwt
from wiki.models import *
import pprint
import utils
from xl2python import Xl2Python
from campaign.models import *


upload_field_map = [
    ('version', 'trimmed'),
    ('campaign_name', 'trimmed'),
    ('ad_group', 'trimmed'),
    ('keyword', 'trimmed'),
    ('headline', 'trimmed'),
    ('sub_headline', 'trimmed'),
    ('pt1', 'trimmed'),
    ('pt1_sub', 'trimmed'),
    ('pt2', 'trimmed'),
    ('pt2_sub', 'trimmed'),
    ('pt3', 'trimmed'),
    ('pt3_sub', 'trimmed'),
    ('pt4', 'trimmed'),
    ('pt4_sub', 'trimmed')
]

match_field_map = [
    ('campaign', 'trimmed'),
    ('ad_group', 'trimmed'),
    ('keyword', 'trimmed')
]


insurer_dict = {
    # 2 : "star",
    # 4 : "bajaj",
    # 6 : "religare",
    # 9 : "bharti",
    # 10 : "reliance",
    # 11 : "l & t",

    1: "apollo munich",
    13: "hdfc ergo",
    14: "iffco tokio",
}


class Uploader(Xl2Python):
    pass


################## CHANGE DATA #########################
xfile = open('gad_2.xls', 'r')
campaign_name = 'COMPETITOR-1'
########################################################


# for a in AdGroup.objects.filter(camp = campaign_name):
#     for k in a.keyword_set.all():
#         k.delete()
#     a.delete()


xlm = Uploader(upload_field_map, xfile)
data = None
if xlm.is_valid():
    data = xlm.cleaned_data
else:
    print xlm.errors


ad_list = []


match_file = open('keytemp.xls')
match_xlm = Uploader(match_field_map, match_file)
match_data = None
if match_xlm.is_valid():
    match_data = match_xlm.cleaned_data
else:
    print match_xlm.errors

final_list = []
for i in data:
    for j in match_data:
        if i['ad_group'].strip().lower() == j['ad_group'].strip().lower():
            final_elem = i.copy()
            final_elem['keyword'] = j['keyword']
            final_list.append(final_elem)

pprint.pprint(final_list)

for k, v in insurer_dict.items():
    ins = Insurer.objects.get(id=k)
    for elem in final_list:
        temp_dict = {'insurer': ins.id, 'insurer_name': ins.title}
        for kk, vv in elem.items():
            key_str = vv.replace('BRAND', v.upper()).strip()
            key_str = key_str.replace('Brand', v.title()).strip()
            key_str = key_str.replace('brand', v.title()).strip()
            temp_dict[kk] = key_str

        ad_list.append(temp_dict)

for elem in ad_list:
    pprint.pprint(elem)
    pstr = ''
    for k, v in elem.items():
        if not pstr:
            pstr = v
        else:
            pstr = '%s|%s' % (pstr, v)
    # print pstr
    ins = Insurer.objects.get(id=elem['insurer'])
    if AdGroup.objects.filter(title=elem['ad_group'].strip()):
        ad_group = AdGroup.objects.get(title=elem['ad_group'].strip())
        ad_group.camp = campaign_name
        ad_group.insurer = ins
        ad_group.extra = elem
        ad_group.save()
    else:
        ad_group = AdGroup(
            title=elem['ad_group'].strip(),
            camp=campaign_name,
            insurer=ins,
            extra=elem,
            template='campaign/marketing/landing-page-base.html'
        )
        ad_group.save()
    # keyword = Keyword(title=elem['keyword'].strip(), extra=elem.copy(), template='campaign/marketing/landing-page-marketing-brand.html')

    keyword = Keyword(title=elem['keyword'].strip(), extra=elem.copy())
    keyword.ad_group = ad_group
    keyword.save()
    key_slug = '%s-1' % utils.slugify(keyword.title)
    if Keyword.objects.filter(slug=key_slug):
        while Keyword.objects.filter(slug=key_slug):
            key_slug = '%s-%s' % ('-'.join(key_slug.split('-')
                                           [:-1]), int(key_slug.split('-')[-1]) + 1)
    keyword.slug = key_slug
    keyword.save()
