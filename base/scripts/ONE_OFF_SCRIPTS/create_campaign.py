import sys
import time
import datetime
import os
from optparse import OptionParser

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from django.db import models
import csv
import xlwt
from wiki.models import *
import pprint
import utils
from xl2python import Xl2Python
from campaign.models import *


upload_field_map = [
    'version',
    'campaign_name',
    'ad_group',
    'keyword',
    'headline',
    'sub_headline',
    'pt1',
    'pt1_sub',
    'pt2',
    'pt2_sub',
    'pt3',
    'pt3_sub',
    'pt4',
    'pt4_sub'
]


class Uploader(Xl2Python):
    pass


xfile = open('cf_brand.xls', 'r')
xlm = Uploader(upload_field_map, xfile)
data = None
if xlm.is_valid():
    data = xlm.cleaned_data
else:
    print xlm.errors

pprint.pprint(data)
for elem in data:
    if AdGroup.objects.filter(title=elem['ad_group'].strip(), camp=elem['campaign_name'].strip()):
        ad_group = AdGroup.objects.get(
            title=elem['ad_group'].strip(), camp=elem['campaign_name'])
    else:
        ad_group = AdGroup(title=elem['ad_group'].strip(), camp=elem[
                           'campaign_name'].strip(), extra=elem.copy())
        ad_group.save()
    keyword = Keyword(title=elem['keyword'].strip(), extra=elem)
    keyword.ad_group = ad_group
    keyword.save()
    key_slug = '%s-1' % utils.slugify(keyword.title)
    if Keyword.objects.filter(slug=key_slug):
        while Keyword.objects.filter(slug=key_slug):
            key_slug = '%s-%s' % ('-'.join(key_slug.split('-')
                                           [:-1]), int(key_slug.split('-')[-1]) + 1)
    keyword.slug = key_slug
    keyword.save()
