import sys
import time
import datetime
import os
from optparse import OptionParser

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from django.db import models
import csv
import xlwt
from wiki.models import *
import pprint
import utils
from xl2python import Xl2Python
from campaign.models import *


upload_field_map = [
    ('campaign', 'trimmed'),
    ('ad_group', 'trimmed'),
    ('keyword', 'trimmed'),
    ('ukey', 'trimmed'),
]

data_obj = {
    'headline': 'Buy the best health insurance plans online',
    'sub_headline': '',
    'pt1': 'Get the best rates for all top brands',
    'pt1_sub': '',
    'pt2': 'Choose your preferred hospitals',
    'pt2_sub': '',
    'pt3': 'Get 100% claims support with Coverfox',
    'pt3_sub': '',
    'pt4': 'Get the policy instantly in your inbox',
    'pt4_sub': '',
}


class Uploader(Xl2Python):
    pass


################## CHANGE DATA #########################
xfile = open('competitor_keywords.xls', 'r')
########################################################


xlm = Uploader(upload_field_map, xfile)
data = None
if xlm.is_valid():
    data = xlm.cleaned_data
else:
    print xlm.errors

for datum in data:
    campaign = utils.slugify(datum['campaign'])
    keyword = utils.slugify(datum['keyword'])
    ad_group = utils.slugify(datum['ad_group'])
    old_key = Keyword.objects.get(slug=datum['ukey'])
    if not UtmCampaign.objects.filter(slug=campaign):
        utm_camp = UtmCampaign(
            title=campaign,
            slug=campaign,
        )
        utm_camp.save()
    else:
        utm_camp = UtmCampaign.objects.get(slug=campaign)

    if not UtmAdGroup.objects.filter(slug=ad_group):
        utm_ad = UtmAdGroup(
            title=ad_group,
            slug=ad_group
        )
        utm_ad.save()
    else:
        utm_ad = UtmAdGroup.objects.get(slug=ad_group)

    if not UtmKeyword.objects.filter(slug=keyword):
        utm_keyword = UtmKeyword(
            title=datum['keyword'],
            slug=keyword,
            pattern=datum['keyword'],
            extra=old_key.extra,
        )
        utm_keyword.save()
    else:
        utm_keyword = UtmKeyword.objects.get(slug=keyword)

    if utm_ad and utm_keyword and utm_camp:
        if not UtmMapping.objects.filter(keyword=utm_keyword, ad_group=utm_ad, campaign=utm_camp):
            utm_map = UtmMapping(
                keyword=utm_keyword,
                ad_group=utm_ad,
                campaign=utm_camp
            )
            utm_map.save()
