import sys
import time
import datetime
import os
import csv
import ast

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from customdb.customquery import sqltojson, sqltodict
from django.core.mail import send_mail
import thread
import threading
from datetime import timedelta
from django.utils.encoding import smart_str, smart_unicode

from campaign.models import *

order = ['Slug',
         'Verified',
         ]

extra_keys = [
    'keyword',
    'campaign_name',
    'ad_group',
    'ia_group',
    'headline',
    'sub_headline',
    'pt1',
    'pt1_sub',
    'pt2',
    'pt2_sub',
    'pt3',
    'pt3_sub',
    'pt4',
    'pt4_sub',
    'title',
    'template'
]


row_list = []
for k in UtmKeyword.objects.all():
    pdata = []
    pdata.append(k.slug)
    pdata.append('True' if k.is_verified else 'False')
    for key in extra_keys:
        if key not in order:
            order.append(key)
        if key in k.extra.keys():
            pdata.append(str(k.extra[key]))
        else:
            pdata.append('')
    row_list.append(pdata)

print '|'.join(order)
for pdata in row_list:
    print '|'.join(pdata)
