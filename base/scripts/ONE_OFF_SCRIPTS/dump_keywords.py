import sys
import time
import datetime
import os
import csv
import ast

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from customdb.customquery import sqltojson, sqltodict
from django.core.mail import send_mail
import thread
import threading
from datetime import timedelta
from django.utils.encoding import smart_str, smart_unicode

from core.models import User
from health_product.models import *
import urllib2
import urllib
from dateutil.relativedelta import *
import json

from campaign.models import *

for m in UtmMapping.objects.all():
    print '%s|%s|%s|%s|%s' % (
        m.keyword.pattern,
        m.keyword.slug,
        m.ad_group.slug,
        m.campaign.slug,
        'http://coverfox.com/buy-health-insurance/?utm_source=google&utm_medium=cpc&utm_campaign=%s&utm_adgroup=%s&utm_term=%s' % (
            m.campaign.slug,
            m.ad_group.slug,
            m.keyword.pattern
        )
    )
