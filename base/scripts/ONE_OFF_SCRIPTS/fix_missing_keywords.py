import sys
import time
import datetime
import os
from optparse import OptionParser

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from django.db import models
import csv
import xlwt
from wiki.models import *
import pprint
import re
import utils
from xl2python import Xl2Python
from campaign.models import *


upload_field_map = [
    ('keyword', 'trimmed'),
    ('ad_group', 'trimmed'),
    ('campaign', 'trimmed'),
    ('headline', 'trimmed')
]

match_field_map = [
    ('campaign', 'trimmed'),
    ('ad_group', 'trimmed'),
    ('keyword', 'trimmed')
]


class Uploader(Xl2Python):
    pass


################## CHANGE DATA #########################
xfile = open('new_keywords.xls', 'r')
########################################################


def fix_keywords(data):
    for datum in data:
        # FOR KEYWORDS
        utm_keyword = None
        utm_ad = None
        utm_camp = None
        keyword = utils.slugify(
            re.sub(r'[ +\[\]]+', ' ', datum['keyword']).strip())
        extra_data = {'headline': datum['headline']}
        # print datum
        # print extra_data
        if not UtmKeyword.objects.filter(slug=keyword):
            utm_keyword = UtmKeyword(
                title=keyword.replace('-', ' '),
                slug=keyword,
                pattern=datum['keyword'],
                extra=extra_data
            )
            utm_keyword.save()
        else:
            utm_keyword = UtmKeyword.objects.get(slug=keyword)
            utm_keyword.pattern = datum['keyword']
            utm_keyword.save()

        # For AdGroups
        ad_group = utils.slugify(datum['ad_group'])
        if not UtmAdGroup.objects.filter(slug=ad_group):
            utm_ad = UtmAdGroup(
                title=datum['ad_group'],
                slug=ad_group
            )
            utm_ad.save()
        else:
            utm_ad = UtmAdGroup.objects.get(slug=ad_group)

        # For Campaigns
        campaign = utils.slugify(datum['campaign'])
        if not UtmCampaign.objects.filter(slug=campaign):
            utm_camp = UtmCampaign(
                title=campaign,
                slug=campaign,
                insurer=utm_ad.insurer
            )
            utm_camp.save()
        else:
            utm_camp = UtmCampaign.objects.get(slug=campaign)

        if utm_ad and utm_keyword and utm_camp:
            if not UtmMapping.objects.filter(keyword=utm_keyword, ad_group=utm_ad, campaign=utm_camp):
                utm_map = UtmMapping(
                    keyword=utm_keyword,
                    ad_group=utm_ad,
                    campaign=utm_camp
                )
                utm_map.save()
        else:
            print '#############', utm_keyword, utm_ad, utm_camp
    return


def check(data):
    print "========== CHECKING NOW ============="
    for datum in data:
        keyword = utils.slugify(
            re.sub(r'[ +\[\]]+', ' ', datum['keyword']).strip())
        if not UtmKeyword.objects.filter(slug=keyword):
            print keyword
    return


xlm = Uploader(upload_field_map, xfile)
data = None
if xlm.is_valid():
    data = xlm.cleaned_data
else:
    print xlm.errors

fix_keywords(data)
check(data)
