import os
import sys
import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from collections import defaultdict
import pprint

from wiki.models import *


class PlanText():
    """
    All methods which are to be used for tests will start with
    runtest_<parameter>, DONOT add any method which starts with runtest_
    unless its for testing parameter
    """
    pass

    def __init__(self, health_product):
        self.plan = health_product
        self.foreign_keys = [sl.name for sl in Slab._meta.fields if getattr(
            sl, 'rel', '') and sl.name not in ['health_product']]

    @staticmethod
    def get_age_verb(age, atype='months'):
        """
        type = months means the age ranges from 0,1,2...12, 1000, 2000, 3000
        type = None means age ranges from 0,1,2,3...100
        """
        if not age:
            return ""
        if atype == 'cpt':
            if age < 0.12:
                return "%s months" % int(age * 100)
            else:
                return "%s years" % int(age)
        if atype == 'months':
            if age < 12:
                return "%s months" % int(age)
            else:
                return "%s years" % int(age / 1000)
        else:
            return "%s years" % int(age)

    def generate_blog(self):
        status = {}
        for slb in self.plan.slabs.all():
            if slb.sum_assured == 0:
                continue
            attr_dict = self.generate_blog_for_slab(slb)
            for attr, desc in attr_dict.items():
                if attr not in status.keys():
                    status[attr] = {desc: [slb.id]}
                else:
                    if desc in status[attr].keys():
                        status[attr][desc].append(slb.id)
                    else:
                        status[attr][desc] = [slb.id]

        # pprint.pprint(status)
        return status

    def generate_blog_for_slab(self, slab):
        final_text = {}

        for texty in dir(self):
            if texty.startswith("runtest_") and callable(getattr(self, texty)):
                attribute = texty.replace("runtest_", "")
                if self.plan.policy_type == "INDIVIDUAL" and attribute in ['family_cover_premium_table', 'family_coverage']:
                    continue
                if self.plan.policy_type == "FAMILY" and attribute in ['cover_premium_table']:
                    continue
                if attribute in self.foreign_keys:
                    status = getattr(self, texty)(
                        slab, getattr(slab, attribute))
                else:
                    status = getattr(self, texty)(
                        slab, getattr(slab, attribute).all())
                final_text[attribute] = status
        return final_text

    @staticmethod
    def runtest_online_availability(slab, obj):
        status = []
        if not obj.is_available_online:
            verbose = "This plan is only available offline for ages above %s"
            verbose = verbose % getattr(obj, 'maximum_age')
        else:
            verbose = "This plan is available online for all ages"
        status.append(verbose)
        return tuple(status)

    @staticmethod
    def runtest_medical_required(slab, obj):
        status = []
        if obj.is_medical_required:
            verbose = "Medicals are required above the age of %s"
            verbose = verbose % getattr(obj, 'maximum_age')
        else:
            verbose = "No medical checkups are required"
        status.append(verbose)
        return tuple(status)

    def runtest_age_limit(self, slab, obj):
        status = []
        if not obj:
            status.append(
                "This plan is open for all ages, No minimum or maximum age specified")
        else:
            if obj.minimum_entry_age:
                status.append("Minimum entry age for this plan is %s" %
                              self.get_age_verb(obj.minimum_entry_age, atype="months"))

            if obj.maximum_entry_age:
                status.append("This plan needs person to be of not more than %s old" % (
                    self.get_age_verb(obj.maximum_entry_age, atype="months")))
            else:
                status.append("This plan does not have a maximum entry age.")
        return tuple(status)

    def runtest_family_coverage(self, slab, obj):
        status = []
        if self.plan.policy_type == "FAMILY":
            if obj.number_of_maximum_members:
                verbose = "Maximum number of members is %s" % obj.number_of_maximum_members
                status.append(verbose)

            if obj.maximum_dependent_age:
                verbose = "%sMaximum dependent age is %s" % (
                    verbose, obj.maximum_dependent_age)
                status.append(verbose)
        return tuple(status)

    @staticmethod
    def runtest_general(slab, obj):
        status = []
        verbose = 'General waiting period for diseases is %s month(s)' % obj.waiting_period
        status.append(verbose)

        if obj.conditions.all():
            disease_condition = defaultdict(list)
            for c in obj.conditions.all():
                disease_condition[
                    "%s-%s-%s-%s" % (c.period, c.limit, c.limit_in_percentage, c.copay)].append(c.term.name)
            for cond, terms in disease_condition.items():
                period, limit, limit_in_percentage, copay = cond.split("-")
                if period != 'None':
                    status.append("Waiting period of %s months on diseases: %s" % (
                        period, " , ".join(terms)))
                if limit != 'None':
                    status.append("A limit of Rs %s on diseases: %s" %
                                  (limit, " , ".join(terms)))
                if limit_in_percentage != 'None':
                    status.append("Limit of %s%% of SA on diseases: %s" % (
                        limit_in_percentage, " , ".join(terms)))
                if copay != 'None':
                    status.append("Copay of %s%% on diseases: %s" %
                                  (copay, " , ".join(terms)))

        return tuple(status)

    @staticmethod
    def runtest_pre_existing(slab, obj):
        status = []
        verbose = 'General waiting period for diseases is %s months' % obj.waiting_period
        status.append(verbose)

        if obj.conditions.all():
            disease_condition = defaultdict(list)
            for c in obj.conditions.all():
                disease_condition[
                    "%s-%s-%s-%s" % (c.period, c.limit, c.limit_in_percentage, c.copay)].append(c.term.name)
            for cond, terms in disease_condition.items():
                period, limit, limit_in_percentage, copay = cond.split("-")
                if period != 'None':
                    status.append("PED Waiting period of %s months on diseases: %s" % (
                        period, " , ".join(terms)))
                if limit != 'None':
                    status.append("A limit of Rs %s on PED: %s" %
                                  (limit, " , ".join(terms)))
                if limit_in_percentage != 'None':
                    status.append("Limit of %s%% of SA on PED: %s" %
                                  (limit_in_percentage, " , ".join(terms)))
                if copay != 'None':
                    status.append("Copay of %s%% on PED: %s" %
                                  (copay, " , ".join(terms)))
        return tuple(status)

    @staticmethod
    def runtest_maternity_cover(slab, obj):
        status = []
        if not obj.is_covered:
            status.append("Maternity cover is not provided")

        if obj.is_covered:
            status.append('Cover starts only after %s years' %
                          obj.waiting_period)
            status.append('Limit for normal delivery is Rs %s' %
                          obj.limit_for_normal_delivery)
            status.append('Limit for cesarean delivery is Rs %s' %
                          obj.limit_for_cesarean_delivery)
            if obj.delivery_limit:
                status.append("Valid only for %s deliveries" %
                              obj.delivery_limit)
            else:
                status.append("No limit on number of deliveries")

        return tuple(status)

    @staticmethod
    def runtest_standard_exclusion(slab, obj):
        status = []
        if obj.terms.all():
            status.append("%s" % " , ".join([t.name for t in obj.terms.all()]))
        return tuple(status)

    @staticmethod
    def runtest_life_long_renewability(slab, obj):
        status = []
        if obj.condition in ['YES']:
            status.append("This plan is Lifelong renewable")
        if obj.condition in ['NO'] and obj.age_limit:
            status.append("Plan is renewable upto age %s years" %
                          obj.age_limit)
        return tuple(status)

    @staticmethod
    def runtest_ambulance_charges(slab, obj):
        status = []

        if obj.condition == "NO":
            status.append("Ambulance charges are not covered")

        if obj.condition == "EMERGENCY_ONLY":
            status.append("Ambulance charges covered for emergency cases only")

        if obj.condition == "YES":
            status.append("Ambulance charges covered for all cases")

        if obj.condition in ["YES", "EMERGENCY_ONLY"]:
            if obj.is_ambulance_services_covered:
                status.append(
                    'Third party ambulance services are covered in this plan.')
            if obj.limit_in_network_hospitals or obj.limit_in_non_network_hospitals:
                status.append("Ambulance charges limited in network hositals to Rs %s" %
                              obj.limit_in_network_hospitals)
                status.append("Ambulance charges limited in non-network hositals to Rs %s" %
                              obj.limit_in_non_network_hospitals)
        return tuple(status)

    @staticmethod
    def runtest_alternative_practices_coverage(slab, obj):
        status = []
        if not obj:
            status.append("No alternative medical practices are covered")
        if obj:
            if not obj.alternative_practices.all():
                status.append("No alternative medical practices are covered")
            if obj.limit:
                status.append("%s are covered upto Rs %s" % (
                    ", ".join([a.name for a in obj.alternative_practices.all()]), obj.limit))
            if obj.limit_in_percentage:
                status.append("%s are covered upto %s%% of sum assured" % (", ".join(
                    [a.name for a in obj.alternative_practices.all()]), obj.limit_in_percentage))
        return tuple(status)

    @staticmethod
    def runtest_domiciliary_hospitalization(slab, obj):
        status = []
        if not obj.is_covered:
            status.append("Domiciliary hospitalization is not covered")
        else:
            if obj.limit:
                if obj.limit == 0:
                    status.append("Covered upto unspecified reasonable limits")
                else:
                    status.append("Covered upto Rs %s" % obj.limit)

            if obj.limit_in_percentage:
                status.append("Covered upto %s%% of sum assured" %
                              obj.limit_in_percentage)
        return tuple(status)

    @staticmethod
    def runtest_outpatient_benefits(slab, obj):
        status = []
        if not obj.is_covered:
            status.append("Outpatient benefits are not covered")
        else:
            if obj.limit:
                status.append(
                    'Outpatient benefits is covered upto Rs %s' % obj.limit)
            if obj.number_of_consultations:
                status.append(
                    "Number of consultations is limited to %s times" % obj.number_of_consultations)
            else:
                status.append("There is no limit on number of consultations")
        return tuple(status)

    @staticmethod
    def runtest_organ_donor(slab, obj):
        status = []
        if not obj.is_covered:
            status.append("Organ donors cover is not available")
        else:
            if obj.limit:
                status.append(
                    "Organ transplant procedure is covered upto Rs %s" % obj.limit)
            if obj.limit_in_percentage:
                status.append(
                    "Organ transplant procedure is covered upto %s%% of sum assured" % obj.limit_in_percentage)
        return tuple(status)

    @staticmethod
    def runtest_bonus(slab, obj):
        status = []
        if not obj.is_available:
            status.append("Bonus is not available for this plan")
        else:
            status.append(
                "Bonus is available after %s claim free years" % obj.unclaimed_years)
            status.append(
                "Bonus will increase by %s%% of base sum assured every claim free year" % obj.bonus_amount)
            status.append("There will be a reduction of %s%% of base sum assured once claim is made" %
                          obj.reduction_in_bonus_amount_after_claim)
            if obj.maximum_bonus_amount:
                status.append(
                    "Maximum bonus is limited to %s%% of base sum assured" % obj.maximum_bonus_amount)
            if obj.maximum_years:
                status.append(
                    "Bonus will be given for a maximum of %s years" % obj.maximum_years)
        return tuple(status)

    @staticmethod
    def runtest_premium_discount(slab, obj):
        status = []
        if not obj.is_available:
            status.append("Premium discount is not available")
        else:
            status.append(
                "Discount is available after %s claim free years" % obj.unclaimed_years)
            status.append("Discount will be %s%% of premium" %
                          obj.discount_amount)
            if obj.reduction_in_discount_amount_after_claim:
                status.append("There will be a reduction of %s%% in discount once claim is made" %
                              obj.reduction_in_discount_amount_after_claim)
            if obj.maximum_discount_amount:
                status.append("Maximum discount is limited to %s%%" %
                              obj.maximum_discount_amount)
            if obj.maximum_years:
                status.append(
                    "Discount will be given for a maximum of %s years" % obj.maximum_years)
        return tuple(status)

    @staticmethod
    def runtest_convalescence_benefit(slab, obj):
        status = []
        if obj.condition == "NO":
            status.append("Convalescence benefit is not available")
        else:
            if obj.condition == "AVAILABLE_PER_DAY":
                status.append(
                    "Convalescence benefit is available proportional to number of days in hospital")
                if obj.amount_per_day:
                    status.append(
                        'Convalescence benefit of Rs %s will be given per day of hospitalization' % obj.amount_per_day)
            if obj.condition == "AVAILABLE_LUMPSUM":
                status.append(
                    "Convalescence benefit is available lump-sum after hospitalization")
                if obj.limit:
                    status.append(
                        'Convalescence benefit of Rs %s will be given lumpsum' % obj.limit)
            status.append("Convalescence benefit will be available if hospitalization exceeds %s days" %
                          obj.minimum_days_of_hospitalization)
            if obj.maximum_number_of_days:
                status.append(
                    "Convalescence benefit is available for upto %s days of hospitalization" % obj.maximum_number_of_days)
        return tuple(status)

    @staticmethod
    def runtest_pre_post_hospitalization(slab, obj):
        status = []
        if not obj.is_available:
            status.append("There are no pre-post hospitalization benefits")
        else:
            if obj.days_pre_hospitalization:
                status.append("Covers expenses %s days pre-hospitalization" %
                              obj.days_pre_hospitalization)
            if obj.days_post_hospitalization:
                status.append(
                    "Covers expenses %s days post-hospitalization" % obj.days_post_hospitalization)
            if obj.intimation_time:
                if obj.days_pre_hospitalization_with_intimation:
                    status.append("Covers expenses %s days pre-hospitalization if intimated %s days in advance" %
                                  (obj.days_pre_hospitalization, obj.intimation_time))
                if obj.days_post_hospitalization_with_intimation:
                    status.append("Covers expenses %s days post-hospitalization if intimated %s days in advance" %
                                  (obj.days_post_hospitalization, obj.intimation_time))

            if not obj.cummulative_limit and not obj.cummulative_limit_in_percentage:
                status.append("Covered up to sum assured")
            if obj.cummulative_limit:
                status.append("Covered up to Rs %s" % obj.cummulative_limit)
            if obj.cummulative_limit_in_percentage:
                status.append("Covered up to %s%% of sum assured" %
                              obj.cummulative_limit_in_percentage)
        return tuple(status)

    @staticmethod
    def runtest_copay(slab, obj):
        status = []
        if not obj.is_available:
            status.append("There is no copay option for this product")
        else:
            if not obj.applicable_after_age:
                status.append("There is no age based copay for this product")
            else:
                if obj.applicable_after_age == 0:
                    status.append("A general copay of %s%% is applicable on network hospital and %s%% on non-network hospital" %
                                  (obj.age_copay_network_hospital, obj.age_copay_non_network_hospital))
                else:
                    status.append("A copay of %s%% is applicable on network hospital and %s%% on non-network hospital after the age of %s" %
                                  (obj.age_copay_network_hospital, obj.age_copay_non_network_hospital, obj.applicable_after_entry_age))

        if obj.for_non_network_hospital:
            status.append("A copay of %s is available on non network hospitals" %
                          obj.for_non_network_hospital)

        if obj.on_day_care_for_network_hospital and obj.on_day_care_for_network_hospital != 0:
            status.append("A copay of %s%% is available on network hospitals for day care procedures" %
                          obj.on_day_care_for_network_hospital)
        if obj.on_day_care_for_non_network_hospital and obj.on_day_care_for_non_network_hospital != 0:
            status.append("A copay of %s%% is available on non-network hospitals for day care procedures" %
                          obj.on_day_care_for_network_hospital)
        return tuple(status)

    def runtest_room_type_copay(self, slab, objs):
        status = []
        if not objs:
            status.append("No room type copay applicable for this plan")

        for obj in objs:
            if obj.room_type:
                if obj.copay and not obj.applicable_for_hospital:
                    status.append("%s room is available with copay of %s%%" % (
                        ROOM_DISPLAY[obj.room_type], obj.copay))
                if obj.copay and obj.applicable_for_hospital:
                    status.append("%s room is available with copay of %s%% in %s" % (ROOM_DISPLAY[
                                  obj.room_type], obj.copay, HOSPITAL_DISPLAY[obj.applicable_for_hospital]))
                if not obj.copay and obj.applicable_for_hospital:
                    status.append("%s room is available in %s" % (
                        ROOM_DISPLAY[obj.room_type], HOSPITAL_DISPLAY[obj.applicable_for_hospital]))
                if not obj.copay and not obj.applicable_for_hospital:
                    status.append("%s room is available" %
                                  (ROOM_DISPLAY[obj.room_type]))
        return tuple(status)

    def runtest_zonal_room_rent(self, slab, obj):
        status = []
        zrr_place = {}
        for zrr in slab.zonal_room_rent.all():
            for state in zrr.state.all():
                if zrr_place.get(state):
                    status.append(
                        "Multiple room-rent values found for state" % state.name)
                zrr_place[state] = zrr
            for city in zrr.city.all():
                if zrr_place.get(city):
                    status.append(
                        "Multiple room-rent values found for city" % city.name)
                zrr_place[city] = zrr

        for obj in slab.zonal_room_rent.all():
            if obj.state.all():
                status.append("States: %s" % ",".join(
                    [s.name for s in obj.states.all()]))

            if obj.city.all():
                status.append("Cities: %s" % ",".join(
                    [c.name for c in obj.city.all()]))

            if obj.conditions == 'NO_LIMIT':
                status.append("There is no limit on room rent")
            elif obj.conditions == 'SA_OR_VALUE_LIMIT':
                if obj.limit_daily:
                    status.append("Daily room-rent limit is Rs %s" %
                                  obj.limit_daily)
                if obj.limit_daily_in_percentage:
                    status.append(
                        "Daily room-rent limit is %s%% of sum assured" % obj.limit_daily_in_percentage)

                if obj.limit_daily_in_icu:
                    status.append("Daily ICU room-rent limit is Rs %s" %
                                  obj.limit_daily_in_icu)
                if obj.limit_daily_in_percentage_in_icu:
                    status.append("Daily ICU room-rent limit is %s%% of sum assured" %
                                  obj.limit_daily_in_percentage_in_icu)

                if obj.limit_maximum_total:
                    status.append(
                        "Daily room-rent limit is subjected to a cummulative maximum of Rs %s" % obj.limit_maximum_total)
                else:
                    status.append("No limit on cummulative maximum room-rent")
                if obj.limit_maximum_total_in_icu:
                    status.append(
                        "Daily ICU room-rent limit is subjected to a maximum of Rs %s" % obj.limit_maximum_total_in_icu)
                else:
                    status.append(
                        "No limit on cummulative maximum room-rent in ICU")
            else:
                status.append('Only %s available = %s' %
                              (obj.room_available, obj.conditions))
            return tuple(status)

    @staticmethod
    def runtest_health_checkup(slab, obj):
        status = []
        if not obj.is_available:
            status.append("Free health checkup is not available")

        if obj.is_available:
            if obj.claim_free_years:
                status.append(
                    "Free health checkup is available after %s claim free years" % obj.claim_free_years)
            else:
                status.append("Free health checkup is available")
            if obj.limit:
                status.append(
                    "Free health checkup is limited to Rs %s" % obj.limit)
            if obj.limit_in_percentage:
                status.append(
                    "Free health checkup is limited to %s%% of sum assured" % obj.limit_in_percentage)
        return tuple(status)

    @staticmethod
    def runtest_claims(slab, obj):
        status = []
        claims_data = slab.health_product.insurer.claims_data
        if claims_data:
            status.append('Rejection Ratio : %s%%' %
                          claims_data.rejection_ratio)
            status.append('Claims Response Time : %s%%' %
                          claims_data.claims_response_time)
        return tuple(status)

    @staticmethod
    def runtest_cancellation_policy(slab, objs):
        status = []
        for obj in objs:
            if obj.period and obj.policy_years and obj.refund_in_percentage:
                status.append('Premium paid for : %s years, Cancellation after : %s months, Refund : %s%%' % (
                    obj.policy_years, obj.period, obj.refund_in_percentage))
        return tuple(status)

    @staticmethod
    def runtest_critical_illness_coverage(slab, obj):
        status = []
        if not obj.is_available:
            status.append("Critical illness cover is not available")
        else:
            if obj.included_in_plan:
                status.append(
                    "Critical illness cover is included under sum assured limit")
            else:
                status.append(
                    "Critical illness cover is over and above sum assured")
            if obj.limit:
                status.append("Critical illness cover is Rs %s" % obj.limit)
            if obj.limit_in_percentage:
                status.append(
                    "Critical illness cover is %s%% of sum assured" % obj.limit_in_percentage)
        if obj.terms:
            status.append("Critical illness cover is available for %s" %
                          ", ".join([t.name for t in obj.terms.all()]))
        return tuple(status)

    @staticmethod
    def runtest_restore_benefits(slab, obj):
        status = []
        if not obj.is_available:
            status.append("No restoration benefits available")
        else:
            if obj.amount_restored:
                status.append(
                    "After claim, the sum assured will be restored to Rs %s" % obj.amount_restored)
            if obj.amount_restored_percentage:
                status.append("After claim, the sum assured will be restored to %s%% of sum assured " %
                              obj.amount_restored_percentage)
        return tuple(status)

    @staticmethod
    def runtest_day_care(slab, obj):
        status = []
        if not obj.is_covered:
            status.append("Day care is not covered")
        else:
            if obj.defined_list:
                conditions = "Conditions covered:\n"
                verb_term = defaultdict(list)
                for pc in obj.procedure_covered.all():
                    verb_term[pc.verbose].append(pc.term.name)
                for verbose, tlist in verb_term.items():
                    conditions += "%s : %s\n" % (verbose, ",".join(tlist))
                status.append(conditions)
        return tuple(status)

    @staticmethod
    def runtest_dental_coverage(slab, obj):
        status = []
        if obj.condition == "NO":
            status.append("Dental care is not covered")
        else:
            if obj.exclusions:
                status.append("Dental treatment is covered with following exclusions: %s" % (
                    ", ".join([e.name for e in obj.exclusions.all()])))
            if obj.limit and obj.waiting_period:
                status.append("Dental treatment is covered upto a limit of Rs %s and a waiting period of %s" % (
                    obj.limit, obj.waiting_period))
            if obj.copay:
                status.append(
                    "A copay of %s%% is applicable for dental coverage" % obj.copay)
        return tuple(status)

    @staticmethod
    def runtest_eye_coverage(slab, obj):
        status = []
        if obj.condition == "NO":
            status.append("Eye care is not covered")
        else:
            if obj.exclusions:
                status.append("Eye treatment is covered with following exclusions: %s" % (
                    ", ".join(obj.exclusions.all())))
            if obj.limit and obj.waiting_period:
                status.append("Eye treatment is covered with following exclusions: %s" % (
                    ", ".join([e.name for e in obj.exclusions.all()])))
            if obj.copay:
                status.append(
                    "A copay of %s%% is applicable for eye coverage" % obj.copay)
        return tuple(status)

    @staticmethod
    def runtest_network_hospital_daily_cash(slab, obj):
        status = []
        if not obj.is_available:
            status.append(
                "Hospital Cash for network hospitals is not available")

        if obj.condition == 'NO':
            status.append(
                "Hospital Cash for network hospitals is not available")

        if obj.condition == 'AVAILABLE_WITH_SHARING_ONLY':
            status.append(
                "Hospital Cash for network hospitals is available for shared rooms only")

        if obj.condition == 'YESWL':
            status.append(
                "Hospital Cash for network hospitals is available with upper limits")

        if obj.limit_per_day_allowance:
            status.append("Hospital cash is available upto Rs %s" %
                          obj.limit_per_day_allowance)

        if obj.is_icu_available:
            status.append(
                'Hospital cash is also available for ICU upto Rs %s' % obj.is_icu_available)
        if obj.limit_cummulative_allowance:
            status.append("Hospital Cash for network hospitals is limited to a cummulative limit of Rs %s" %
                          obj.limit_cummulative_allowance)
        if obj.from_day:
            status.append(
                "Hospital Cash for network hospitals starts from %s day of hospitalization" % obj.from_day)
        if obj.to_day:
            status.append(
                "Hospital Cash for network hospitals is available upto %s day of hospitalization" % obj.to_day)
        if obj.number_of_days:
            status.append(
                "Hospital Cash for network hospitals is available for %s days of hospitalization" % obj.number_of_days)
        return tuple(status)

    @staticmethod
    def runtest_non_network_hospital_daily_cash(slab, obj):
        status = []
        if not obj.is_available:
            status.append(
                "Hospital Cash for non-network hospitals is not available")

        if obj.condition == 'NO':
            status.append(
                "Hospital Cash for non-network hospitals is not available")

        if obj.condition == 'AVAILABLE_WITH_SHARING_ONLY':
            status.append(
                "Hospital Cash for non-network hospitals is available for shared rooms only")

        if obj.condition == 'YESWL':
            status.append(
                "Hospital Cash for non-network hospitals is available with upper limits")

        if obj.limit_per_day_allowance:
            status.append('Hospital cash is available upto Rs %s' %
                          obj.limit_per_day_allowance)
            if obj.is_icu_available:
                status.append(
                    'Hospital cash is also available for ICU upto Rs %s' % obj.limit_per_day_allowance)
        if obj.limit_cummulative_allowance:
            status.append("Hospital Cash for non-network hospitals is limited to a cummulative limit of Rs %s" %
                          obj.limit_cummulative_allowance)
        if obj.from_day:
            status.append(
                "Hospital Cash for non-network hospitals starts from %s day of hospitalization" % obj.from_day)
        if obj.to_day:
            status.append(
                "Hospital Cash for non-network hospitals is available upto %s day of hospitalization" % obj.to_day)
        if obj.number_of_days:
            status.append(
                "Hospital Cash for non-network hospitals is available for %s days of hospitalization" % obj.number_of_days)
        return tuple(status)


insurer_dict = {
    1: 'apollo-munich-health-insurance',
    2: 'star-health-insurance',
    3: 'icici-lombard-health-insurance',
    4: 'bajaj-allianz-health-insurance',
    5: 'max-bupa-health-insurance',
    6: 'religare-health-insurance',
    7: 'tata-aig-health-insurance',
    8: 'new-india-health-insurance',
    9: 'bharti-axa-health-insurance',
    10: 'reliance-health-insurance',
    11: 'lnt-health-insurance',
    12: 'cholamandalam-ms-health-insurance',
    13: 'hdfc-ergo-health-insurance',
    14: 'iffco-tokio-health-insurance',
    15: 'cigna-ttk-health-insurance'
}


good_to_have = [
    'general',
    'pre_existing',
    'dental_coverage',
    'family_coverage',
    # 'network_hospital_daily_cash',
    # 'ambulance_charges',
    # 'alternative_practices_coverage',
    'maternity_cover',
    # 'eye_coverage',
    # 'day_care',
    'age_limit',
    'online_availability',
    'medical_required',
    'standard_exclusion',
    # 'domiciliary_hospitalization',
    'bonus',
    'premium_discount',
    # 'convalescence_benefit',
    # 'non_network_hospital_daily_cash',
    # 'pre_post_hospitalization',
    'copay',
    'health_checkup',
    # 'claims',
    # 'outpatient_benefits',
    # 'organ_donor',
    'life_long_renewability',
    'critical_illness_coverage',
    'restore_benefits'
]

# hp = HealthProduct.objects.all()[30]
for hp in HealthProduct.objects.filter(is_completed=True, is_backend_integrated=True):
    pt = PlanText(hp)
    post_slug = '%s-%s' % (insurer_dict[hp.insurer.id],
                           hp.slug.replace('--', '-').replace('--', '-'))
    if not WikiArticle.objects.filter(slug=post_slug):
        post = WikiArticle(
            slug=post_slug,
            title='%s - Review' % hp.title,
            category='PRODUCT_REVIEW',
        )
        post.save()
        post.health_product.add(hp)
        post.insurer.add(hp.insurer)
        post.save()

    blog_dict = pt.generate_blog()

    for attribute in blog_dict.keys():
        print "============== %s ===============" % attribute
        if not ProductSection.objects.filter(product_page=post, code=attribute):
            ptext = ''
            if len(blog_dict[attribute].keys()) > 1:
                for atext, slabs in blog_dict[attribute].items():
                    if ptext:
                        ptext = '%s<br />' % ptext
                        ptext = '%s<strong>For the cover/sum assured of: %s </strong>' % (
                            ptext, ', '.join([str(s.sum_assured) for s in Slab.objects.filter(id__in=slabs)]))
                    ptext = '%s<ul>' % ptext
                    for line in list(atext):
                        ptext = '%s<li>%s</li>' % (ptext, line)
                    ptext = '%s</ul>' % ptext
            else:
                for atext, slabs in blog_dict[attribute].items():
                    ptext = '%s<ul>' % ptext
                    for line in list(atext):
                        ptext = '%s<li>%s</li>' % (ptext, line)
                    ptext = '%s</ul>' % ptext
            ptext = '<div class="fullpost">%s</div>' % ptext
            # print ptext
            product_section = ProductSection(
                product_page=post,
                code=attribute,
                body=ptext
            )
            product_section.save()
            if product_section.code in ['claims']:
                product_section.is_active = False
                product_section.save()
            if product_section.code in good_to_have:
                product_section.is_good = True
                product_section.save()
