import sys
import time
import datetime
import os
import csv

import thread
import threading
from datetime import timedelta
from django.utils.encoding import smart_str, smart_unicode

import urllib2
import urllib
import json
import ast

STARHEALTH_TEST_API_URL = 'http://sandbox.ig.nallantech.com:9000/api/policy/proposals'
STARHEALTH_TEST_API_KEY = 'fe6f6c4068bc4824b981a1f1e3f7e515'
STARHEALTH_TEST_SECRET_KEY = '2353ba6ad9514067a82fc38666cb4540'


alines = [line.strip() for line in open('scripts/city_resp')]
lines = alines[:2]

for l in lines:
    item_list = l.split('||')
    if len(item_list) == 2:
        pin = item_list[0]
        city_response = ast.literal_eval(item_list[1])

        resp_list = []
        for city_dict in city_response['city']:
            area_url = 'http://sandbox.ig.nallantech.com:9000/api/policy/address/details?APIKEY=%s&SECRETKEY=%s&pincode=%s&city_id=%s' % \
                       (STARHEALTH_TEST_API_KEY, STARHEALTH_TEST_SECRET_KEY,
                        pin, city_dict['city_id'])
            print area_url
            # try:
            if True:
                req = urllib2.Request(url=area_url)
                resp = urllib2.urlopen(req)
                response = json.loads(resp.read())
                print response
                resp_list.append(response)
            # except:
            else:
                pass
        print '%s||%s' % (str(pin), resp_list)
