import sys
import time
import datetime
import os
import csv
import ast

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from blog.models import *

campaign = Campaign.objects.get(title='BLOG')
for s in campaign.subscriber_set.filter(is_active=True):
    try:
        print "%s|%s|%s" % (s.email, s.name, datetime.datetime.strftime(s.created, '%d-%m-%Y'))
    except:
        pass
