import sys
import time
import datetime
import os
import csv

import thread
import threading
from datetime import timedelta
from django.utils.encoding import smart_str, smart_unicode

import urllib2
import urllib
import json

STARHEALTH_TEST_API_URL = 'http://sandbox.ig.nallantech.com:9000/api/policy/proposals'
STARHEALTH_TEST_API_KEY = 'fe6f6c4068bc4824b981a1f1e3f7e515'
STARHEALTH_TEST_SECRET_KEY = '2353ba6ad9514067a82fc38666cb4540'


pin_codes = [line.strip() for line in open('scripts/uniq_pin_list')]


for p in pin_codes:
    city_url = 'http://sandbox.ig.nallantech.com:9000/api/policy/city/details?APIKEY=%s&SECRETKEY=%s&pincode=%s' % (
        STARHEALTH_TEST_API_KEY, STARHEALTH_TEST_SECRET_KEY, p)
    try:
        req = urllib2.Request(url=city_url)
        resp = urllib2.urlopen(req)
        response = json.loads(resp.read())
        print '%s||%s' % (str(p), response)
    except:
        pass
