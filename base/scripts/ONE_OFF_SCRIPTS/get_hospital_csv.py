import sys
import time
import datetime
import os
from optparse import OptionParser
import settings
from django.core.management import setup_environ

setup_environ(settings)

from django.db import models
import csv
import xlwt
from wiki.models import *


def main():
    headings = ["id", "name", "address", "landmark", "district", "city.name", "state.name",
                "pin", "std_code", "phone1", "phone2", "fax1", "fax2", "email", "url", "lat", "lng"]
    insurer_heading = [ins.title for ins in Insurer.objects.all()]
    headings.extend(insurer_heading)
    print "|".join(headings)
    for h in Hospital.objects.all():
        hinsurers = [str(ins in h.insurers.all())
                     for ins in Insurer.objects.all()]
        row = [h.id, h.name, h.address, h.landmark, h.district, h.city.name, h.state.name,
               h.pin, h.std_code, h.phone1, h.phone2, h.fax1, h.fax2, h.email, h.url, h.lat, h.lng]
        crow = []
        for r in row:
            if type(r) in [unicode, str]:
                r = r.strip()
                r = r.replace("\r\n", " ")
                crow.append(r.encode('utf8'))
            else:
                crow.append(str(r))
        crow.extend(hinsurers)
        print "|".join(crow)

if __name__ == "__main__":
    main()
