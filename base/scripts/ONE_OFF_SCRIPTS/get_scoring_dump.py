import sys
import time
import datetime
import os
from optparse import OptionParser

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from django.db import models
import csv
import xlwt
from health_product.services.match_plan import MatchPlan
from health_product.services.validate_plan import ValidatePlan
from wiki.forms import TestInput


def create_sheet(wb, sname, data):
    data_label = []
    data_attr = []
    prop_attr = []
    score_data = None

    tf = TestInput(data)
    if tf.is_valid():
        kid_ages = []
        if tf.cleaned_data.get('kid_1_age'):
            kid_ages.append(tf.cleaned_data['kid_1_age'])
        if tf.cleaned_data.get('kid_2_age'):
            kid_ages.append(tf.cleaned_data['kid_2_age'])
        tf.cleaned_data['kid_ages'] = kid_ages
        print "------>", tf.cleaned_data
        tf.cleaned_data['hospitals'] = []
        mp = MatchPlan(tf.cleaned_data, None, True)
        score_data = mp.calculate_score()
        mlabel = []
        mlabel.append(['City', tf.cleaned_data.get('city').name])
        mlabel.append(
            ['Healthcare Choice', tf.cleaned_data.get('service_type')])
        mlabel.append(['Age', tf.cleaned_data.get('age')])
        mlabel.append(['Adults', tf.cleaned_data.get('adults')])
        mlabel.append(['Kids', tf.cleaned_data.get('kids'),
                       "-".join([str(k) for k in kid_ages])])
        for sd in score_data:
            sd['dscore'] = dict(sd['score'])
        data_label.extend(['Total', 'Insurer', 'Name', 'Type',
                           'SA', 'Medical Score', 'Premium'])
        #data_label.extend(['Eye', 'Outpatient Benefits', 'Daily Cash', 'Claims', 'Health Checkup', 'Maternity Cover', 'Dental', 'Ambulance Charges', 'Bonus', 'Organ Donor', 'Domiciliary Hospitalization', 'Pre Post Hospitalization', 'Life Long Renewability', 'Alternative Practices Coverage', 'Copay', 'Convalescence Benefit', 'Premium Discount', 'Critical Illness Coverage', 'Online Availability', 'Room Rent', 'Restore Benefits'])
        data_label.extend(['Premium Score', 'Maternity cover', 'Copay', 'Room Rent', 'Claims', 'Bonus', 'Premium discount',
                           'Pre-existing', 'Daily Cash', 'Health Checkup', 'Pre Post Hospitalization', 'Critical Illness Coverage', 'Restore Benefits'])

        data_attr = ['total_score', 'insurer_title', 'product_title',
                     'policy_type', 'sum_assured', 'medical_score', 'premium']
        #prop_attr = ['eye', 'outpatient_benefits', 'daily_cash', 'claims', 'health_checkup', 'maternity_cover', 'dental', 'ambulance_charges', 'bonus', 'organ_donor', 'domiciliary_hospitalization', 'pre_post_hospitalization', 'life_long_renewability', 'alternative_practices_coverage', 'copay', 'convalescence_benefit', 'premium_discount', 'critical_illness_coverage', 'online_availability', 'room_rent', 'restore_benefits']
        prop_attr = ['premium', 'maternity_cover', 'copay', 'room_rent', 'claims', 'bonus', 'premium_discount', 'pre_existing',
                     'daily_cash', 'health_checkup', 'pre_post_hospitalization', 'critical_illness_coverage', 'restore_benefits']

    ws = wb.add_sheet(sname)
    row = 0
    for m in mlabel:
        [ws.write(row, col, content)
         for content, col in zip(m, range(0, len(m)))]
        row += 1

    [ws.write(row, col, content)
     for content, col in zip(data_label, range(0, len(data_label)))]
    row += 1
    for sd in score_data:
        drow = []
        for da in data_attr:
            drow.append(sd[da])
        for pa in prop_attr:
            drow.append(sd['dscore'][pa]['score'])
        row += 1
        [ws.write(row, col, content)
         for content, col in zip(drow, range(0, len(drow)))]

#'city': 3 # Non-Metro
#'city': 2325, #Metro

individual_settings = {
    'C1A': [100000, 2325, "SHARED", 20],
    'C1B': [100000, 2325, "SHARED", 40],
    'C1C': [100000, 2325, "SHARED", 60],
    'C1D': [100000, 2325, "SHARED", 80],

    'C1E': [100000, 2325, "PVT", 20],
    'C1F': [100000, 2325, "PVT", 40],
    'C1G': [100000, 2325, "PVT", 60],
    'C1H': [100000, 2325, "PVT", 80],

    'C1I': [100000, 2325, "PVT_HIGH_END", 20],
    'C1J': [100000, 2325, "PVT_HIGH_END", 40],
    'C1K': [100000, 2325, "PVT_HIGH_END", 60],
    'C1L': [100000, 2325, "PVT_HIGH_END", 80],
}

family_settings = {
    'F1A': [100000, 2325, "SHARED", 2, 20, 18, 0, 0, 0],
    'F1B': [100000, 2325, "SHARED", 2, 40, 38, 2, 10, 10],
    'F1C': [100000, 2325, "SHARED", 2, 60, 58, 2, 10, 10],
    'F1D': [100000, 2325, "SHARED", 2, 80, 78, 2, 10, 10],

    'F1E': [100000, 2325, "PVT", 2, 20, 18, 0, 10, 10],
    'F1F': [100000, 2325, "PVT", 2, 40, 38, 2, 10, 10],
    'F1G': [100000, 2325, "PVT", 2, 60, 58, 2, 10, 10],
    'F1H': [100000, 2325, "PVT", 2, 80, 78, 2, 10, 10],

    'F1I': [100000, 2325, "PVT_HIGH_END", 2, 20, 18, 0, 0, 0],
    'F1J': [100000, 2325, "PVT_HIGH_END", 2, 40, 38, 2, 10, 10],
    'F1K': [100000, 2325, "PVT_HIGH_END", 2, 60, 58, 2, 10, 10],
    'F1L': [100000, 2325, "PVT_HIGH_END", 2, 80, 78, 2, 10, 10],
}


def main():
    for fname, rdata in family_settings.items():
        sa, city, service, adults, age, spouse_age, kids, kid_1_age, kid_2_age = rdata
        data_base = {
            'city': city,
            'kids': kids,
            'adults': adults,
            'family': 'on',
            'gender': 'MALE',
            'age': age,
            'spouse_age': spouse_age,
            'sum_assured': sa,
            'kid_2_age': kid_2_age,
            'service_type': service,
            'kid_1_age': kid_1_age
        }
        data_list = [data_base]

        d1 = data_base.copy()
        d1['sum_assured'] = 200000
        d1['city'] = 3
        data_list.append(d1)

        wb1 = xlwt.Workbook()
        index = 0
        for dobj in data_list:
            create_sheet(wb1, "%s-%s" % (fname, index), dobj)
            index += 1
        wb1.save('/tmp/family-%s.xls' % fname)

    for fname, rdata in individual_settings.items():
        sa, city, service, age = rdata
        data_base = {
            'city': city,
            'kids': 0,
            'adults': 1,
            'family': '',
            'gender': 'MALE',
            'age': age,
            'spouse_age': '',
            'sum_assured': sa,
            'kid_2_age': '',
            'service_type': service,
            'kid_1_age': ''
        }
        data_list = [data_base]

        d1 = data_base.copy()
        d1['sum_assured'] = 200000
        d1['city'] = 3
        data_list.append(d1)

        d2 = data_base.copy()
        d2['sum_assured'] = 300000
        data_list.append(d2)

        d3 = data_base.copy()
        d3['sum_assured'] = 400000
        data_list.append(d3)

        wb2 = xlwt.Workbook()
        index = 0
        for dobj in data_list:
            create_sheet(wb2, "%s-%s" % (fname, index), dobj)
            index += 1
        wb2.save('/tmp/individual-%s.xls' % fname)

if __name__ == "__main__":
    main()
