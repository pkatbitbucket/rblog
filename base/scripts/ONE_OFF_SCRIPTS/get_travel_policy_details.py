import sys
import os
import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))


import datetime
from importlib import import_module
from collections import OrderedDict
import json
import hashlib


def _subtract_years(d, years):
    """Return a date that's `years` years before the date (or datetime)
    object `d`. Return the same calendar date (month and day) in the
    destination year, if it exists, otherwise use the following day
    (thus changing February 29 to February 28).
    """
    try:
        return d.replace(year = d.year - years)
    except ValueError:
        return d - (datetime.date(d.year - years, 1, 1) - datetime.date(d.year, 1, 1))


COUNTRIES = ['', 'Afganistan', 'Albania', 'Algeria', 'Andorra', 'Angola', 'Argentina', 'Armenia', 'Aruba', 'Ascension', 'Australia', 'Austria', 'Azerbaijan Republic', 'Azores', 'Bahamas', 'Bermuda', 'Bahrain', 'Bangkok', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bhutan', 'Bolivia', 'Boputatswana', 'Bosnia and Herzegovina', 'Botswana', 'Brazil', 'Brunei Darussalam', 'Bulgaria', 'Burkina Fasso', 'Burundi', 'Cayman Islands', 'Cambodia', 'Cameroon', 'Canada', 'Canary Island', 'Cape Verde', 'Central African Republic', 'Chad', 'Chile', 'China', 'Ciskei', 'Cocos Keeling Island', 'Colombia', 'Comoros', 'Congo', 'Cook Island', 'Costa Rica', 'Croatia', 'Cuba', 'Cyprus', 'Czech Republic', 'Denmark', 'Diego Garcia', 'Djibouti', 'Dominican Republic', 'East Timor', 'Ecuador', 'Egypt', 'El Salvador Rep.', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Falkland Islands', 'Faroe Island', 'Fiji Republic', 'Finland', 'Fr.Guinea', 'Fr.Polynesia', 'France', 'France', 'Gabonese Republic', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guinea', 'Guinea Bissau', 'Guyana', 'Haiti', 'Honduras', 'Hongkong', 'Hungary', 'Ivory Coast', 'Iceland', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Israel', 'Italy', 'Jamaica', 'Japan', 'Jordan', 'Kazakhstan', 'Kenya', 'Kirghistan', 'Kiribati', 'Korea (North)', 'Korea (South)', 'Kuwait', 'Laos', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechstein', 'Lithuania', 'Luxembourg', 'MONTENEGRO', 'Macao', 'Macedonia', 'Madagascar', 'Madeira Island', 'Malawi', 'Malaysia', 'Maldives', 'Mali', 'Malta', 'Mariana Island', 'Marshal Island', 'Martinque', 'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Micronesia', 'Moldova', 'Monaco', 'Mongolia', 'Montserrat', 'Morocco', 'Mozambique', 'Myanmar', 'NICARAGUA', 'NORFOLK ISLAND', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'Netherlands Anthilles', 'New Caledonia', 'New Zealand', 'Niger', 'Nigeria', 'Niue Island', 'Norway', 'Oman', 'PUERTO RICO', 'Pakistan', 'Palau', 'Palestine', 'Panama', 'Papau New Guinea', 'Paraguay', 'Peru', 'Phillipines', 'Poland', 'Portugal', 'Qatar', 'Reunion', 'Rodrigues Island', 'Romania', 'Russian Federation', 'Rwandese Republic', 'Serbia', 'SlovakiA', 'St Kitts & Nevis', 'St Lucia (WI)', 'St Vincent & Grenadines (WI)', 'Samoa American', 'Samoa Western', 'San Marino', 'Sao Tome and Principe', 'Saudi Arabia', 'Senegal', 'Seychelles', 'Sierra Leone', 'Singapore', 'Slovak Republic', 'Slovenia', 'Soloman Island', 'Somalia Democratic Republic', 'South Africa', 'Spain', 'Srilanka', 'St.Helena', 'St.Pierre and Miquelon', 'Sudan', 'Suriname', 'Swaziland', 'Sweden', 'Switzerland', 'Syria', 'Turks & Caicos Islands', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand', 'Togolese Republic', 'Tonga', 'Transkei', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Tuvalu', 'Uganda', 'USA', 'Ukraine', 'United Arab Emirates', 'United Kingdom', 'Uruguay', 'Uzbekistan', 'Virgin Islands (British)', 'Virgin Islands (US)', 'Vanuatu', 'Vatican City State', 'Venda', 'Venezuela', 'Vietnam', 'West Indies', 'Wallis and Futina Island', 'Yemen (Saana)', 'Yogoslavia', 'Zaire', 'Zambia', 'Zimbabwe']
DISEASES = ['None', 'Cancer / Leukemia / Malignant Tumor', 'Cardiac ailments', 'COPD', 'HIV /AIDS', 'Insulin Dependent Diabetes', 'Kidney Ailment', 'Liver Disease', 'Neurological Disorder / Stroke / Paralysis', 'Thalasemia', 'Any Other']

APP_NAME = 'travel_product'

# INSURERS = os.walk(os.path.join(settings.SETTINGS_FILE_FOLDER, APP_NAME, 'insurers')).next()[1]

INSURERS = ['reliance']
INSURERS = ['religare']
INSURERS = ['bharti-axa']
INSURERS = ['hdfc-ergo']
INSURERS = ['universal-sompo']
INSURERS = ['tata-aig']
INSURERS = ['iffco-tokio']
INSURERS = ['bajaj']
INSURERS = ['apollo-munich']

QUOTE_FUNCTION = 'get_quote'

_TODAY = datetime.datetime.now().date()
_TOMORROW = _TODAY + datetime.timedelta(days=1)

DATE_FORMAT = '%d-%b-%Y'

FORM_DATA = {
##################################################
# INDIVIDUAL AND SINGLE TRIP DATA WITHOUT ASIA
    'reliance': [
        {'key': 'immigration_visa', 'values': ['no']},
        {'key': 'cover_type', 'values': ['Applicant + Spouse']},
        {'key': 'traveller_type', 'values': ['Individual']},
        {'key': 'dob', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [40, 60, 70]]},
        {'key': 'region', 'values': ['Schengen', 'World except USA & Canada', 'World including USA & Canada']},
        {'key': 'sports', 'values': ['no']},
        {'key': 'from_date', 'values': [_TODAY.strftime(DATE_FORMAT)]},
        {'key': 'to_date', 'values': [(_TODAY + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in range(6, 182, 7)]},
        {'key': 'max_trip_length', 'values': ['30']},
        {'key': 'trip_type', 'values': ['single']},
        {'key': 'ped', 'values': ['None']},
        {'key': 'indian_citizen', 'values': ['yes']},
        {'key': 'visiting_usa', 'values': ['yes', 'no']},
        {'key': 'addon_benefits', 'values': ['no']},
        {'key': 'is_oci_or_nonoci', 'values': ['yes', 'no']},
        {'key': 'oci_or_nonoci', 'values': ['OCI', 'NONOCI']},
        {'key': 'passport_issuer', 'values': ['India']},
        {'key': 'country_of_stay', 'values': ['India']},
    ],
######################################################
# INDIVIDUAL AND SINGLE TRIP ONLY ASIA
    'reliance': [
        {'key': 'immigration_visa', 'values': ['no']},
        # {'key': 'cover_type', 'values': ['Applicant + Spouse']},
        {'key': 'traveller_type', 'values': ['Individual']},
        {'key': 'dob', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [40, 60, 70]]},
        {'key': 'region', 'values': ['Asia (except Japan)']},
        {'key': 'sports', 'values': ['no']},
        {'key': 'from_date', 'values': [_TODAY.strftime(DATE_FORMAT)]},
        {'key': 'to_date', 'values': [(_TODAY + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in [3, 6, 13, 20, 29]]},
        # {'key': 'max_trip_length', 'values': ['30']},
        {'key': 'trip_type', 'values': ['single']},
        {'key': 'ped', 'values': ['None']},
        {'key': 'indian_citizen', 'values': ['yes']},
        {'key': 'visiting_usa', 'values': ['no']},
        {'key': 'addon_benefits', 'values': ['no']},
        {'key': 'is_oci_or_nonoci', 'values': ['yes', 'no']},
        {'key': 'oci_or_nonoci', 'values': ['OCI', 'NONOCI']},
        {'key': 'passport_issuer', 'values': ['India']},
        {'key': 'country_of_stay', 'values': ['India']},
    ],
######################################################
# INDIVIDUAL AND MULTIPLE TRIP
    'reliance': [
        {'key': 'immigration_visa', 'values': ['no']},
        {'key': 'cover_type', 'values': ['Applicant + Spouse']},
        {'key': 'traveller_type', 'values': ['Individual']},
        {'key': 'dob', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [40, 60]]},
        {'key': 'region', 'values': ['World except USA & Canada', 'US/Canada']},
        {'key': 'sports', 'values': ['no']},
        {'key': 'from_date', 'values': [_TODAY.strftime(DATE_FORMAT)]},
        {'key': 'to_date', 'values': [(_TODAY + datetime.timedelta(days=3)).strftime(DATE_FORMAT)]},
        {'key': 'max_trip_length', 'values': ['30', '45']},
        {'key': 'trip_type', 'values': ['multiple']},
        {'key': 'ped', 'values': ['None']},
        {'key': 'indian_citizen', 'values': ['yes']},
        {'key': 'visiting_usa', 'values': ['yes', 'no']},
        {'key': 'addon_benefits', 'values': ['no']},
        {'key': 'is_oci_or_nonoci', 'values': ['yes', 'no']},
        {'key': 'oci_or_nonoci', 'values': ['OCI', 'NONOCI']},
        {'key': 'passport_issuer', 'values': ['India']},
        {'key': 'country_of_stay', 'values': ['India']},
    ],
######################################################
# FAMILY AND SINGLE TRIP
    'reliance': [
        {'key': 'immigration_visa', 'values': ['no']},
        {'key': 'cover_type', 'values': ["Applicant + Spouse", "Applicant + Spouse + Child1", "Applicant + Spouse + Child1 + Child2", "Applicant + Child1", "Applicant + Child1 + Child2",]},
        {'key': 'traveller_type', 'values': ['Family']},
        {'key': 'dob', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [40, 60]]},
        # {'key': 'region', 'values': []},
        {'key': 'sports', 'values': ['no']},
        {'key': 'from_date', 'values': [_TODAY.strftime(DATE_FORMAT)]},
        {'key': 'to_date', 'values': [(_TODAY + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in range(6, 182, 7)]},
        {'key': 'max_trip_length', 'values': ['30']},
        {'key': 'trip_type', 'values': ['single']},
        {'key': 'ped', 'values': ['None']},
        {'key': 'indian_citizen', 'values': ['yes']},
        {'key': 'visiting_usa', 'values': ['yes', 'no']},
        # {'key': 'addon_benefits', 'values': ['no']},
        # {'key': 'is_oci_or_nonoci', 'values': ['yes', 'no']},
        # {'key': 'oci_or_nonoci', 'values': ['OCI', 'NONOCI']},
        # {'key': 'passport_issuer', 'values': []},
        # {'key': 'country_of_stay', 'values': []},
    ],
######################################################
# INDIVIDUAL AND SINGLE TRIP DATA
    'bharti-axa': [
        {'key': 'policy_type', 'values': ['I']},
        # {'key': 'policy_type', 'values': ['I', 'F', 'S']},
        {'key': 'family_type', 'values': ['SS']},
        # {'key': 'family_type', 'values': ['S2C', 'SS2C', 'SS', 'SC', 'SSC']},
        {'key': 'Travelling_To', 'values': ['1', '2', '3']},
        {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
        {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in range(6, 357, 7)]},  # duration_range = range(2, 357)
        {'key': 'max_trip_length', 'values': ['30']},  # Arbitary
        {'key': 'trip_type', 'values': ['S']},
        {'key': 'first_name', 'values': ['first']},
        {'key': 'last_name', 'values': ['last']},
        {'key': 'email_id', 'values': ['a@b.com']},
        {'key': 'mobile', 'values': ['9876543210']},
        {'key': 'DOB', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [40, 60, 70, 80, 85]]},
        {'key': 'ResAddr1', 'values': ['Add1']},
        {'key': 'ResAddr2', 'values': ['Add2']},
        {'key': 'ResAddr3', 'values': ['Add3']},
        {'key': 'state', 'values': ['Maharashtra']},
        {'key': 'city', 'values': ['Mumbai']},
        {'key': 'pincode', 'values': ['600092']},
        {'key': 'sportsperson', 'values': ['N']},
        {'key': 'dangerous_activity', 'values': ['N']},
        {'key': 'pre_existing_illness', 'values': ['N']},
    ],
######################################################
# INDIVIDUAL AND MULTI TRIP DATA
    'bharti-axa': [
        {'key': 'policy_type', 'values': ['I']},
        # {'key': 'policy_type', 'values': ['I', 'F', 'S']},
        {'key': 'family_type', 'values': ['SS']},
        # {'key': 'family_type', 'values': ['S2C', 'SS2C', 'SS', 'SC', 'SSC']},
        {'key': 'Travelling_To', 'values': ['1', '2']},
        {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
        {'key': 'to_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},  # Arbitary
        {'key': 'max_trip_length', 'values': ['30', '45', '60']},  # Arbitary
        {'key': 'trip_type', 'values': ['A']},
        {'key': 'first_name', 'values': ['first']},
        {'key': 'last_name', 'values': ['last']},
        {'key': 'email_id', 'values': ['a@b.com']},
        {'key': 'mobile', 'values': ['9876543210']},
        {'key': 'DOB', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [40, 60, 70, 80, 85]]},
        {'key': 'ResAddr1', 'values': ['Add1']},
        {'key': 'ResAddr2', 'values': ['Add2']},
        {'key': 'ResAddr3', 'values': ['Add3']},
        {'key': 'state', 'values': ['Maharashtra']},
        {'key': 'city', 'values': ['Mumbai']},
        {'key': 'pincode', 'values': ['600092']},
        {'key': 'sportsperson', 'values': ['N']},
        {'key': 'dangerous_activity', 'values': ['N']},
        {'key': 'pre_existing_illness', 'values': ['N']},
    ],
######################################################
# FAMILY AND SINGLE TRIP DATA
    'bharti-axa': [
        {'key': 'policy_type', 'values': ['F']},
        {'key': 'family_type', 'values': ['S2C', 'SS2C', 'SS', 'SC', 'SSC']},
        {'key': 'Travelling_To', 'values': ['1', '2', '3']},
        {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
        {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in range(6, 357, 7)]},  # duration_range = range(2, 357)
        {'key': 'max_trip_length', 'values': ['30']},  # Arbitary
        {'key': 'trip_type', 'values': ['S']},
        {'key': 'first_name', 'values': ['first']},
        {'key': 'last_name', 'values': ['last']},
        {'key': 'email_id', 'values': ['a@b.com']},
        {'key': 'mobile', 'values': ['9876543210']},
        {'key': 'DOB', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [40, 60, 70, 80, 85]]},
        {'key': 'ResAddr1', 'values': ['Add1']},
        {'key': 'ResAddr2', 'values': ['Add2']},
        {'key': 'ResAddr3', 'values': ['Add3']},
        {'key': 'state', 'values': ['Maharashtra']},
        {'key': 'city', 'values': ['Mumbai']},
        {'key': 'pincode', 'values': ['600092']},
        {'key': 'sportsperson', 'values': ['N']},
        {'key': 'dangerous_activity', 'values': ['N']},
        {'key': 'pre_existing_illness', 'values': ['N']},
    ],
######################################################
# SCHENGEN INDIVIDUAL AND SINGLE TRIP DATA 46972L
    'bharti-axa': [
        {'key': 'policy_type', 'values': ['I']},
        # {'key': 'policy_type', 'values': ['I', 'F', 'S']},
        {'key': 'family_type', 'values': ['SS']},
        # {'key': 'family_type', 'values': ['S2C', 'SS2C', 'SS', 'SC', 'SSC']},
        {'key': 'Travelling_To', 'values': ['SCH']},
        {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
        {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in range(6, 357, 7)]},  # duration_range = range(2, 357)
        {'key': 'max_trip_length', 'values': ['30']},  # Arbitary
        {'key': 'trip_type', 'values': ['S']},
        {'key': 'first_name', 'values': ['first']},
        {'key': 'last_name', 'values': ['last']},
        {'key': 'email_id', 'values': ['a@b.com']},
        {'key': 'mobile', 'values': ['9876543210']},
        {'key': 'DOB', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [40, 60, 70, 80, 85]]},
        {'key': 'ResAddr1', 'values': ['Add1']},
        {'key': 'ResAddr2', 'values': ['Add2']},
        {'key': 'ResAddr3', 'values': ['Add3']},
        {'key': 'state', 'values': ['Maharashtra']},
        {'key': 'city', 'values': ['Mumbai']},
        {'key': 'pincode', 'values': ['600092']},
        {'key': 'sportsperson', 'values': ['N']},
        {'key': 'dangerous_activity', 'values': ['N']},
        {'key': 'pre_existing_illness', 'values': ['N']},
    ],
######################################################
# SCHENGEN FAMILY AND SINGLE TRIP DATA 48944L
    'bharti-axa': [
        {'key': 'policy_type', 'values': ['F']},
        # {'key': 'policy_type', 'values': ['I', 'F', 'S']},
        {'key': 'family_type', 'values': ['S2C', 'SS2C', 'SS', 'SC', 'SSC']},
        {'key': 'Travelling_To', 'values': ['SCH']},
        {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
        {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in range(6, 357, 7)]},  # duration_range = range(2, 357)
        {'key': 'max_trip_length', 'values': ['30']},  # Arbitary
        {'key': 'trip_type', 'values': ['S']},
        {'key': 'first_name', 'values': ['first']},
        {'key': 'last_name', 'values': ['last']},
        {'key': 'email_id', 'values': ['a@b.com']},
        {'key': 'mobile', 'values': ['9876543210']},
        {'key': 'DOB', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [40, 60, 70, 80, 85]]},
        {'key': 'ResAddr1', 'values': ['Add1']},
        {'key': 'ResAddr2', 'values': ['Add2']},
        {'key': 'ResAddr3', 'values': ['Add3']},
        {'key': 'state', 'values': ['Maharashtra']},
        {'key': 'city', 'values': ['Mumbai']},
        {'key': 'pincode', 'values': ['600092']},
        {'key': 'sportsperson', 'values': ['N']},
        {'key': 'dangerous_activity', 'values': ['N']},
        {'key': 'pre_existing_illness', 'values': ['N']},
    ],
# INDIVIDUAL AND SINGLE TRIP DATA
    'religare': [
        {'key': 'trip_type', 'values': ['single']},
        {'key': 'no_of_travellers', 'values': ['1']},
        {'key': 'member1_age_band', 'values': ["up to 40 Years", "41 Years - 60 Years", "61  Years - 70 Years", "> 70 Years"]},
        {'key': 'plan', 'values': ['Explore - Platinum - Worldwide - Excluding US & Canada', 'Explore - Platinum - Worldwide', 'Explore - Asia', 'Explore - Europe', 'Explore - Canada', 'Explore - Gold - Worldwide', 'Explore - Africa', 'Explore - Gold - Worldwide - Excluding US & Canada']},
        {'key': 'sum_insured', 'values': ['US $ 1,00,000', 'US $ 25,000', 'US $ 3,00,000', 'US $ 5,00,000', 'US $ 50,000', 'EUR 1,00,000', 'EUR 30000']},
        {'key': 'from_date', 'values': [_TODAY.strftime(DATE_FORMAT)]},
        {'key': 'to_date', 'values': [(_TODAY + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in range(1, 365)]},
    ],
# INDIVIDUAL AND MULTI TRIP DATA
    'religare': [
        {'key': 'trip_type', 'values': ['multiple']},
        {'key': 'max_trip_length', 'values': ['45', '60']},
        {'key': 'no_of_travellers', 'values': ['1']},
        {'key': 'member1_age_band', 'values': ["up to 40 Years", "41 Years - 60 Years", "61  Years - 70 Years", "> 70 Years"]},
        {'key': 'plan', 'values': ["Explore - Gold - Worldwide - Excluding US & Canada", "Explore - Gold - Worldwide", "Explore - Platinum - Worldwide - Excluding US & Canada", "Explore - Platinum - Worldwide",]},
        {'key': 'sum_insured', 'values': ["US $ 50,000","US $ 1,00,000","US $ 3,00,000","US $ 5,00,000",]},
        {'key': 'from_date', 'values': [_TODAY.strftime(DATE_FORMAT)]},
        {'key': 'to_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
    ],
# INDIVIDUAL AND SINGLE TRIP DATA 49225L 50219L 54757L
    'bajaj': [
        {'key': 'traveller_type', 'values': ['Individual']},
        {'key': 'region', 'values': ['WORLDWIDE INCLUDING USA AND CANADA', 'WORLDWIDE EXCLUDING USA AND CANADA', 'ASIA ONLY EXCLUDING JAPAN']},
        # {'key': 'plan', 'values': ["Explore - Gold - Worldwide - Excluding US & Canada", "Explore - Gold - Worldwide", "Explore - Platinum - Worldwide - Excluding US & Canada", "Explore - Platinum - Worldwide",]},
        {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
        {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in range(1, 190)]},  # 180 is the limit
        {'key': 'dob', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [40, 60, 70]]},
    ],

# FAMILY TRIP DATA 57723L
    'bajaj': [
        {'key': 'traveller_type', 'values': ['Family']},
        {'key': 'region', 'values': ['SCHENGEN MEMBER STATES / WORLDWIDE EXCLUDING USA AND CANADA', ]},
        {'key': 'region', 'values': ['SCHENGEN MEMBER STATES / WORLDWIDE EXCLUDING USA AND CANADA', ]},
        {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
        {'key': 'to_date', 'values': [(_TODAY + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in [15, 30, 60]]},
        {'key': 'dob', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [60]]},

        {'key': 'no_of_child', 'values': ['0', '1', '2', '3', '4', ]},

        {'key': 'child1_name', 'values': ['ch1']},
        {'key': 'child2_name', 'values': ['ch1']},
        {'key': 'child3_name', 'values': ['ch1']},
        {'key': 'child4_name', 'values': ['ch1']},

        {'key': 'child1_dob', 'values': ['05-Jul-2002']},
        {'key': 'child2_dob', 'values': ['05-Jul-2002']},
        {'key': 'child3_dob', 'values': ['05-Jul-2002']},
        {'key': 'child4_dob', 'values': ['05-Jul-2002']},

        {'key': 'child1_assignee_name', 'values': ['papa']},
        {'key': 'child2_assignee_name', 'values': ['papa']},
        {'key': 'child3_assignee_name', 'values': ['papa']},
        {'key': 'child4_assignee_name', 'values': ['papa']},

        {'key': 'child1_gender', 'values': ['Male']},
        {'key': 'child2_gender', 'values': ['Male']},
        {'key': 'child3_gender', 'values': ['Male']},
        {'key': 'child4_gender', 'values': ['Male']},

        {'key': 'no_of_adult', 'values': ['0', '1', '2', '3', '4', ]},

        {'key': 'adult1_name', 'values': ['ad1']},
        {'key': 'adult2_name', 'values': ['ad1']},
        {'key': 'adult3_name', 'values': ['ad1']},
        {'key': 'adult4_name', 'values': ['ad1']},

        {'key': 'adult1_dob', 'values': ['23-Aug-1989']},
        {'key': 'adult2_dob', 'values': ['23-Aug-1989']},
        {'key': 'adult3_dob', 'values': ['23-Aug-1989']},
        {'key': 'adult4_dob', 'values': ['23-Aug-1989']},

        {'key': 'adult1_assignee_name', 'values': ['tinku']},
        {'key': 'adult2_assignee_name', 'values': ['tinku']},
        {'key': 'adult3_assignee_name', 'values': ['tinku']},
        {'key': 'adult4_assignee_name', 'values': ['tinku']},

        {'key': 'adult1_gender', 'values': ['Male']},
        {'key': 'adult2_gender', 'values': ['Male']},
        {'key': 'adult3_gender', 'values': ['Male']},
        {'key': 'adult4_gender', 'values': ['Male']},

        {'key': 'adult1_relation', 'values': ['Spouse']},
        {'key': 'adult2_relation', 'values': ['Spouse']},
        {'key': 'adult3_relation', 'values': ['Spouse']},
        {'key': 'adult4_relation', 'values': ['Spouse']},

    ],



   
#Self Multi-Trip(Starts from 58132L - 58137L (GOLD) from 58144L to 58149L (PLATINUM)
'hdfc-ergo': [
        {'key': 'geographical_coverage', 'values': ['Worldwide']},
        {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
        {'key': 'to_date', 'values':  ['16-Aug-2017']},#Arbit
        {'key': 'dob', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [40,60,70]]},
        {'key': 'policy_type', 'values': ['I']},
        {'key': 'trip_type', 'values': ['A']},
        {'key': 'max_trip_length', 'values': ['30','45']},
        {'key': 'student_traveldays', 'values': ['30']},
        {'key': 'dependent_parent', 'values': ['None']},
        {'key': 'family_type','values':['']},
        {'key': 'trip_duration','values':['']},
        
        
    ],

    #Self Single WorldWide-Including(starts from gt 58029L)
# 'hdfc-ergo': [
#         {'key': 'geographical_coverage', 'values': ['WorldWide-Excluding USA/Canada','WorldWide-Including USA/Canada']},
#         {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
#         {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in map(lambda x: x-1, [4,7,14,21,28,35,47,60,75,90,120,150,180])]},
#         {'key': 'dob', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [40,60,70]]},
#         {'key': 'policy_type', 'values': ['I']},
#         {'key': 'trip_type', 'values': ['S']},
#         {'key': 'max_trip_length', 'values': ['45']},
#         {'key': 'student_traveldays', 'values': ['30']},
#         {'key': 'dependent_parent', 'values': ['None']},
#         {'key': 'family_type','values':['']},
        
#     ],

#     #Self Single Asia (starts from gt 58107L)
# 'hdfc-ergo': [
#         {'key': 'geographical_coverage', 'values': ['Asia - Excluding Japan']},
#         {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
#         {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in [3,6,13,20,29]] },
#         {'key': 'dob', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [40,60,70]]},
#         {'key': 'policy_type', 'values': ['I']},
#         {'key': 'trip_type', 'values': ['S']},
#         {'key': 'max_trip_length', 'values': ['45']},
#         {'key': 'student_traveldays', 'values': ['30']},
#         {'key': 'dependent_parent', 'values': ['None']},
#         {'key': 'family_type','values':['']},
        
#     ],


 #Family  Trip(Starts from gt 58122L)

# 'hdfc-ergo': [
#         {'key': 'geographical_coverage', 'values': ['WorldWide-Excluding USA/Canada']},
#         {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
#         {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in map(lambda x: x-1, [15, 30, 60])]},
#         {'key': 'family_type','values': ['self+spouse', 'self+spouse+child1', 'self+spouse+child1+child2'] },
#         {'key': 'dependent_parent','values':['None']},
#         {'key': 'dob', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [60]]},
#         {'key': 'policy_type', 'values': ['F']},
#         {'key': 'max_trip_length', 'values': ['30']},
#         {'key': 'student_traveldays', 'values': ['30']},

#     ],

#  }



 #Individual  Travel-WorldWide (Starts from gt 57795L )
# 'universal-sompo': [
#          {'key': 'plan', 'values': ['Travel-Worldwide']},
#          {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
#          {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days  in range(6,36,7)+ range(46,48,7) +range(59,91,15)+range(119,181,30)]},
#          {'key':'member1_age_band', 'values':['up to 40 Years','41 Years - 60 Years']}, 
#          {'key':'member1_plan_type', 'values':['Gold','Silver','Platinum']},
#          {'key': 'max_trip_length', 'values': ['']},
#          {'key': 'no_of_travellers', 'values': ['1']},

#      ],

                   
 #Individual  Travel-WorldWide excluding US/Canada(starts from gt 57867L)
# 'universal-sompo': [
#          {'key': 'plan', 'values': ['Travel-Worldwide Excluding US & Canada']},
#          {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
#          {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days  in range(6,36,7)+ range(46,48,7) +range(59,91,15)+range(119,181,30)]},
#          {'key':'member1_age_band', 'values':['up to 40 Years','41 Years - 60 Years']}, 
#          {'key':'member1_plan_type', 'values':['Gold','Silver','Platinum']},
#          {'key': 'max_trip_length', 'values': ['']},
#          {'key': 'no_of_travellers', 'values': ['1']},

#      ],
  #Individual  Travel-Asia (starts from gt 57939L to 57963L)
# 'universal-sompo': [
#          {'key': 'plan', 'values': ['Travel-Asia']},
#          {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
#          {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days  in map(lambda x:x-1,[4,7,14,21,30,47])]},
#          {'key':'member1_age_band', 'values':['up to 40 Years','41 Years - 60 Years']}, 
#          {'key':'member1_plan_type', 'values':['Gold','Platinum']},
#          {'key': 'max_trip_length', 'values': ['']},
#          {'key': 'no_of_travellers', 'values': ['1']},

#      ],



# 'tata-aig': [
#         {'key': 'policy_type', 'values':['I'] },
#         {'key': 'trip_type', 'values': ['S']},
#         {'key': 'geographical_coverage', 'values': ['Worldwide including Americas','Worldwide excluding Americas']},
#         {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)] },
#         {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days  in range(6,36,7)+ range(46,48,7) +range(59,91,15)+range(119,181,30)]},
#         {'key':'trip_duration', 'values':['']},
#         {'key': 'max_trip_length', 'values': ['30']},
#         {'key': 'members', 'values': [1]},
#         {'key': 'DOB', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [25,45,57,65]]},
#         {'key': 'sum_assured', 'values': ['50,000 USD', '100,000 USD', '250,000 USD', '500,000 USD']},
#         {'key': 'schengen', 'values': ['Y', 'N']},
#     ] 

'iffco-tokio': [
         {'key': 'usncanada', 'values': ['N', 'Y']}, 
         {'key': 'acc_cover', 'values': ['N', 'Y']}, 
         {'key': 'plan', 'values': ['SILVER']},
         {'key': 'liability_cover', 'values': ['N', 'Y']},#same premium for Y and N
         {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
         {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days  in range(6,36,7)+ range(46,48,7) +range(59,91,15)+range(119,181,30)]},
         {'key': 'geographical_coverage', 'values': ['EXCLUDUSA', 'WORLDWIDE']},
         {'key': 'trip_duration', 'values': ['']},
         {'key': 'DOB', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [25,45,65]]},],

# INDIVIDUAL AND MULTI TRIP DATA 56267L
    'bajaj': [
        {'key': 'traveller_type', 'values': ['Business Multi-Trip']},
        {'key': 'region', 'values': ['WORLDWIDE INCLUDING USA AND CANADA']},
        {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
        {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in range(3,10,3)+ range(18,36,7)+ range(46,48,7) +range(59,91,15)+range(119,181,30)]},
        {'key': 'dob', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [61]]},
        {'key': 'no_of_child', 'values': [0]},
        {'key': 'no_of_adult', 'values': [0]},  
    ],


'apollo-munich': [
         {'key': 'trip_type', 'values': ['S']}, 
         {'key': 'policy_type', 'values': ['F']}, 
         {'key': 'family_type', 'values': ['2A','2A1C','2A2C','2A3C','2A4C']},
         {'key': 'plan', 'values': ['SILVER','BRONZE','ASIAN']},
         {'key': 'present_loc', 'values': ['Y']},
         {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
         # TODO Change the to date range accordingly afterwards
         {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days  in range(6,36,7)+ range(46,48,7) +range(59,91,15)+range(119,181,30)]},

         {'key': 'visa_type', 'values': ['N']},
         {'key': 'max_trip_length', 'values': ['30']},
         {'key': 'geographical_coverage', 'values': ['EXCLUDUSA', 'WORLDWIDE']},
         {'key': 'trip_duration', 'values': ['']},
         {'key': 'DOB', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [25,45,65]]},

     ],


#Annual Multi Trip Individual (starts from gt 57939L to 57963L)
'apollo-munich': [
         {'key': 'trip_type', 'values': ['R']}, 
         {'key': 'policy_type', 'values': ['I']}, 
         {'key': 'plan', 'values': ['SILVER']},
         {'key': 'present_loc', 'values': ['Y']},#same premium for Y and N
         {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
         # TODO Change the to date range accordingly afterwards
         {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in [29, 59]]},
         {'key': 'visa_type', 'values': ['N']},
         {'key': 'max_trip_length', 'values': ['30', '60']},
         {'key': 'geographical_coverage', 'values': ['EXCLUDUSA', 'WORLDWIDE']},
         {'key': 'trip_duration', 'values': ['']},
         {'key': 'DOB', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [25,45,65]]},

     ],

    
# Single Trip Individual (starts from gt 57939L to 57963L)
'apollo-munich': [
         {'key': 'trip_type', 'values': ['S']}, 
         {'key': 'policy_type', 'values': ['I']}, 
         {'key': 'plan', 'values': ['ASIAN']},
         {'key': 'present_loc', 'values': ['Y']}, # same premium for Y and N
         {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
         {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days  in range(6,36,7)+ range(46,48,7) +range(59,91,15)+range(119,181,30)]},
         {'key': 'visa_type', 'values': ['N']},
         {'key': 'max_trip_length', 'values': ['30']},
         {'key': 'geographical_coverage', 'values': ['ASIAPAC']},
         {'key': 'trip_duration', 'values': ['']},
         {'key': 'DOB', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [25,45,65]]},

     ],

#Annual Multi Trip Individual (starts from gt 57939L to 57963L)
'apollo-munich': [
         {'key': 'trip_type', 'values': ['R']},
         {'key': 'policy_type', 'values': ['I']},
         {'key': 'plan', 'values': ['PLATINUM', 'GOLD', 'SILVER']},
         {'key': 'present_loc', 'values': ['Y']},  # same premium for Y and N
         {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
         # TODO Change the to date range accordingly afterwards
         {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in [29, 59]]},
         {'key': 'visa_type', 'values': ['N']},
         {'key': 'max_trip_length', 'values': ['30', '60']},
         {'key': 'geographical_coverage', 'values': ['EXCLUDUSA', 'WORLDWIDE']},
         {'key': 'trip_duration', 'values': ['']},
         {'key': 'DOB', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [25,45,65]]},
     ],

    # Single Trip Individual (starts from gt 57939L to 57963L)
'apollo-munich': [
         {'key': 'trip_type', 'values': ['S']}, 
         {'key': 'policy_type', 'values': ['I']}, 
         {'key': 'plan', 'values': ['ASIAN']},   # 'PLATINUM', 'GOLD', 'SILVER', 'BRONZE'
         {'key': 'present_loc', 'values': ['Y']}, # same premium for Y and N
         {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
         {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days  in range(6,36,7)+ range(46,48,7) +range(59,91,15)+range(119,181,30)]},
         {'key': 'visa_type', 'values': ['N']},
         {'key': 'max_trip_length', 'values': ['30']},
         {'key': 'geographical_coverage', 'values': ['ASIAPAC']},  # 'ASIAPAC'
         {'key': 'trip_duration', 'values': ['']},
         {'key': 'DOB', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [25,45,65]]},

     ],

    #Annual Multi Trip Individual (starts from gt 57939L to 57963L)
'apollo-munich': [
    {'key': 'trip_type', 'values': ['R']},
    {'key': 'policy_type', 'values': ['I']},
    {'key': 'plan', 'values': ['PLATINUM', 'GOLD', 'SILVER']},
    {'key': 'present_loc', 'values': ['Y']},  # same premium for Y and N
    {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
    # TODO Change the to date range accordingly afterwards
    {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in [29, 59]]},
    {'key': 'visa_type', 'values': ['N']},
    {'key': 'max_trip_length', 'values': ['30', '60']},
    {'key': 'geographical_coverage', 'values': ['EXCLUDUSA', 'WORLDWIDE']},
    {'key': 'trip_duration', 'values': ['']},
    {'key': 'DOB', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [25,45,65]]},
     ],

    # Single Trip family (starts from gt 57939L to 57963L)
'apollo-munich': [
    {'key': 'trip_type', 'values': ['S']},
    {'key': 'policy_type', 'values': ['F']},
    {'key': 'family_type', 'values': ['2A', '2A1C', '2A2C', '2A3C', '2A4C']},
    {'key': 'plan', 'values': ['SILVER', 'BRONZE']},
    {'key': 'present_loc', 'values': ['Y']},
    {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
    # TODO Change the to date range accordingly afterwards
    {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days  in range(6,36,7)+ range(46,48,7) +range(59,91,15)+range(119,181,30)]},
    
    {'key': 'visa_type', 'values': ['N']},
    {'key': 'max_trip_length', 'values': ['30']},
    {'key': 'geographical_coverage', 'values': ['EXCLUDUSA', 'WORLDWIDE']},
    {'key': 'trip_duration', 'values': ['']},
    {'key': 'DOB', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [25,45,65]]},
    
],

#Annual Multi Trip Individual (starts from gt 57939L to 57963L)
'apollo-munich': [
    {'key': 'trip_type', 'values': ['R']},
    {'key': 'policy_type', 'values': ['I']},
    {'key': 'plan', 'values': ['PLATINUM', 'GOLD', 'SILVER', 'ASIAN']},
    {'key': 'present_loc', 'values': ['Y']},  # same premium for Y and N
    {'key': 'from_date', 'values': [_TOMORROW.strftime(DATE_FORMAT)]},
    # TODO Change the to date range accordingly afterwards
    {'key': 'to_date', 'values': [(_TOMORROW + datetime.timedelta(days=days)).strftime(DATE_FORMAT) for days in [29, 59]]},
    {'key': 'visa_type', 'values': ['N']},
    {'key': 'max_trip_length', 'values': ['30', '60']},
    {'key': 'geographical_coverage', 'values': ['EXCLUDUSA', 'WORLDWIDE', 'ASIAPAC']},
    {'key': 'trip_duration', 'values': ['']},
    {'key': 'DOB', 'values': [_subtract_years(_TODAY, int(period)).strftime(DATE_FORMAT) for period in [25,45,65]]},
],

}


def get_possible_dicts(list_of_dicts):
    counters_max = [len(d['values']) for d in list_of_dicts]
    counters = [0] * len(counters_max)

    # print counters
    max = reduce(lambda x, y: x*y, [len(v['values']) for v in list_of_dicts], 1)
    for k in range(0, max):
        params = {}
        for i, data in enumerate(list_of_dicts):
            params[data['key']] = data['values'][counters[i]]

        exit = False
        for m, c in enumerate(counters):
            if c < counters_max[m] - 1:
                counters[m] += 1 
                counters[0:m] = [0]*m
                break
            if m == len(counters):
                exit = True
        if exit: 
            break
        yield params


def post_data(insurer, form_data, quote_function):
    if (datetime.datetime.strptime(form_data['from_date'], DATE_FORMAT) < datetime.datetime.strptime(form_data['to_date'], DATE_FORMAT)) or insurer == 'bharti-axa':
        hash_key = str(form_data).encode('utf8')
        hash_value = hashlib.sha256(hash_key).hexdigest()
        if not insurer_policy_data.objects.filter(insurer=insurer, hash_form_data=hash_value):
            setattr(activity, 'search_data', form_data)
            if insurer == 'bharti-axa' or 'hdfc-ergo':
                activity.search_data['trip_duration'] = (datetime.datetime.strptime(activity.search_data['to_date'], '%d-%b-%Y') - datetime.datetime.strptime(activity.search_data['from_date'], '%d-%b-%Y')).days+1
                activity.id = 1
            try:
                plans_list, _ = quote_function(activity)
            except Exception as e:
                if e.message == 'Invalid Data':
                    print e.message, activity.search_data
                    return
                else:
                    raise
            except:
                pass
            if not len(plans_list):
                print('Invalid Form Data')  # bharti schengen plans are upto 180 days
                pass
            premiums = [plan['premium']for plan in plans_list if plan['premium']]
            if premiums:
                if not 'Err' in str(plans_list):
                    obj = insurer_policy_data.objects.create(insurer=insurer, form_data=form_data, plans_list=plans_list, hash_form_data=hash_value)
                    print obj.plans_list
                    map(lambda p: all_premiums.append(p['premium']), obj.plans_list)
                    print obj.form_data
                else:
                    print "Error Response"
                    # raise ValueError('Error response')
            else:
                print "Error in Response"
                # raise ValueError('Error response')

all_premiums = []
def main():
    counter = 1
    for insurer in INSURERS:
        possible_dicts = [ dictionary for dictionary in get_possible_dicts(FORM_DATA[insurer]) ]
        length = len(possible_dicts)
        for form_data in possible_dicts:
            print "\n\n\n\n\n counter: ", counter, " of ", length, "\n\n\n\n\n"
            counter += 1
            insurer_integration_module = import_module('{}.{}.{}.integration'.format(APP_NAME, 'insurers', insurer.replace('-', '_')))
            get_quote = getattr(insurer_integration_module, QUOTE_FUNCTION)
            post_data(insurer=insurer, form_data=form_data, quote_function=get_quote)


if __name__ == '__main__':
    app_model = import_module('{}.models'.format(APP_NAME))
    activity = app_model.Activity
    insurer_policy_data = app_model.InsurerPolicyData
    main()
    print all_premiums
    print len(set(all_premiums)), len(all_premiums)
