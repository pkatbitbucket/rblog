# setup env
import datetime
import os
import sys

import django

BASE_DIRECTORY = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..'))
sys.path.append(BASE_DIRECTORY)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

django.setup()

from django.utils import timezone

from blog import models as blog_models
from cfblog import models as cfblog_models


HEALTH_SETTINGS = {
    'template': 'layout/custom/base_right_sidebar--health.html',
    'body_key': 'left',
}

CAR_SETTINGS = {
    'template': 'layout/custom/base_right_sidebar--car.html',
    'body_key': 'left',
}

DEFAULT_SETTINGS = {
    'template': 'layout/base_full_width.html',
    'body_key': 'base_content',
}


def main():
    for category in blog_models.Category.objects.all():
        blog_posts = category.post_set.all()

        if not blog_posts:
            continue

        if category.title == 'Car Insurance':
            _settings = CAR_SETTINGS
        elif category.title == 'Health Insurance':
            _settings = HEALTH_SETTINGS
        elif 'Car Insurance' in category.title or 'Health Insurance' in category.title:
            if blog_posts:
                print u'{} should not contain any blog posts'.format(category.title)
            continue
        else:
            _settings = DEFAULT_SETTINGS

        cfblog_category, created = cfblog_models.Category.objects.get_or_create(title=category.title)

        print u'migrating posts for: {}'.format(category.title)

        if created:
            cfblog_category.description = category.description
            cfblog_category.save()

        for blog_post in blog_posts:
            settings = _settings
            auth_data = {}

            body = u'# {}'.format(blog_post.title)

            if blog_post.infographic:
                auth_data['infographic'] = blog_post.infographic.url
                body = u'{}\n![]({})'.format(body, blog_post.infographic.url)
                settings = DEFAULT_SETTINGS

            body = u'{}\n{}'.format(body, blog_post.body)

            auth_data.update({
                settings['body_key']: body,
                'meta_og_title': blog_post.title,
                'meta_og_desc': blog_post.tease,
                'meta_gplus_title': blog_post.title,
                'meta_gplus_desc': blog_post.tease,
                'meta_twitter_title': blog_post.title,
                'meta_twitter_descr': blog_post.tease,
                'meta_keywords': blog_post.keywords,
            })

            if blog_post.thumbnail:
                auth_data['thumbnail_r'] = blog_post.thumbnail.url_r
                auth_data['thumbnail_s'] = blog_post.thumbnail.url_s
                auth_data['thumbnail_t'] = blog_post.thumbnail.url_t

                auth_data['meta_og_image'] = blog_post.thumbnail.url_r
                auth_data['meta_twitter_img'] = blog_post.thumbnail.url_r
                auth_data['meta_gplus_img'] = blog_post.thumbnail.url_r

            if blog_post.pdf:
                auth_data['pdf'] = blog_post.pdf.url

            cfblog_post, created = cfblog_models.Content.objects.get_or_create(
                title=blog_post.title,
                category=cfblog_category,
                url=blog_post.get_absolute_url(),
                status=1,
                template=settings['template'],
                auth_data=auth_data,
                author=blog_post.author,
            )

            if created:
                cfblog_post.tags = blog_post.tags
                cfblog_post.publish = get_date_time(blog_post.publish)
                cfblog_post.tease = blog_post.tease
                cfblog_post.notes_todos = blog_post.notes_todos
                cfblog_post.save()


def get_date_time(dt):
    if isinstance(dt, datetime.datetime):
        if not timezone.is_aware(dt):
            dt = timezone.make_aware(dt)
    return dt


if __name__ == '__main__':
    main()
