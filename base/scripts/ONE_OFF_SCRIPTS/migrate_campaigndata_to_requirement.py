"""
Postgresql copy table data to csv.
COPY table_name TO '/tmp/c_data.csv' DELIMITER '~' CSV HEADER;
"""
import os
import uuid
import django
import json
import csv
import re
import datetime
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from django.core.exceptions import MultipleObjectsReturned
from django.db.models import Q
from django.db import connection

from core import models as core_models
from wiki import models as wiki_models
from crm import models as crm_models
import statemachine.models as sm_models
from motor_product.models import Transaction as MotorTransaction
from travel_product.models import Transaction as TravelTransaction
from health_product.models import Transaction as HealthTransaction
from core import forms as core_forms

cust_data = {}
cust_email = {}
campaign = {}
c = 0

filepath = "/tmp/"
log_file = open('/tmp/log_file.log', 'w')
cursor = connection.cursor()

DUMMY_STATE, _ = sm_models.State.objects.get_or_create(name='DUMMY')
crm_models.TaskCategory.objects.get_or_create(name='CALLBACK_LATER')

mid_mapping = {
    'im_campaign': 'campaign',
    'utm_campaign': 'campaign',

    'im_source': 'source',
    'vt_source': 'source',
    'utm_source': 'source',
    'network': 'source',

    'im_medium': 'medium',
    'utm_medium': 'medium',

    'ad-category': 'category',

    'im_content': 'content',
    'vt_creative': 'content',
    'utm_content': 'content',

    'brand': 'brand',
}
activity_param_map = {
    'gclid': 'gclid',

    'im_term': 'term',
    'utm_keyword': 'term',
    'vt_keyword': 'term',
    'keyword': 'term',
    'utm_term': 'term',

    'keyword_category': 'term_category',

    'vt_adposition': 'label',
    'label': 'label',

    'vt_matchtype': 'match_type',
    'match_type': 'match_type',

    'vt_placement': 'vt_placement',

    'vt_target': 'vt_target',

    'feed_item_id': 'feed_item',

    'location': 'city',
    'calculated_city': 'city',

    'device': 'device',

    'device_model': 'device_model',

    'referer': 'referer',

    'triggered_page': 'url',

    'landing_page_url': 'lp_url',
}


def dict2hstore(d):
    val = ''
    for k, v in d.items():
        val += '%s=>"%s", ' % (k, v)
    return val


def get_datetime(x):
    return datetime.datetime.strptime(x[:19], "%Y-%m-%d %H:%M:%S")


def get_sql_datetime(x):
    return datetime.datetime.strftime(get_datetime(x), '%m-%d-%Y %H:%M:%S')


def get_mobile_number(alt_numbers):
    # Remove speacial symbols.
    alt_numbers = alt_numbers.replace(" ", "")
    alt_numbers = alt_numbers.replace("-", "")

    # Get number.
    numbers = re.findall(r'(\+?[0-9]{6,15})', alt_numbers)
    if numbers:
        return numbers


advisor_data = {}
with open(os.path.join(filepath, "auth_user.csv"), "rb") as csvfile:
    reader = csv.reader(csvfile, delimiter='~')
    # COPY (select id, username, email, first_name, last_name, is_active from
    # auth_user) TO '/tmp/auth_user.csv' DELIMITER '~' CSV HEADER;
    for row in reader:
        if row:
            try:
                int(row[0])
                if row[1] == 'system':
                    SYSTEM_CALLER_ID = row[0]
                advisor_data[row[0]] = {
                    'username': row[1].lower(),
                    'email': row[2].lower(),
                    'first_name': row[3].title(),
                    'last_name': row[4].title(),
                    'is_active': True if row[5] == 't' else False
                }
            except:
                pass


with open(os.path.join(filepath, "core_userprofile.csv"), "rb") as csvfile:
    reader = csv.reader(csvfile, delimiter='~')
    # COPY (select user_id, role, ameyo_id, is_available, expertise,
    # extension_no, mobile, did from core_userprofile) TO '/tmp/auth_user.csv'
    # DELIMITER '~' CSV HEADER;
    for row in reader:
        if row:
            try:
                int(row[0])
                d = {
                    'role': row[1],
                    'dialer_username': row[2],
                    'is_available': True if row[3] == 't' else False,
                    'expertise': row[4],
                    'dialer_extension': row[5],
                    'phone': row[6][-10:],
                    'dialer_did': row[7],
                }
                d.update(advisor_data[row[0]])
                advisor_data[row[0]] = d.copy()
            except:
                pass

x = advisor_data.copy()
advisor_data = {}
for key, d in x.items():
    print d
    email = d['email'].strip() or d['username'] + '@crm.coverfox.com'
    u = core_models.User.objects.create()
    try:
        email_obj, _ = core_models.Email.objects.get_or_create(
            email=d['email'],
            is_primary=True,
            is_exists=True,
            is_verified=True
        )
    except MultipleObjectsReturned as e:
        email_obj = core_models.Email.objects.filter(email=d['email']).last()
        email_obj.is_primary = True
        email_obj.is_exists = True
        email_obj.is_verified = True
    if d.get('phone'):
        try:
            phone_obj, _ = core_models.Phone.objects.get_or_create(
                number=d['phone'],
                is_primary=True,
                is_exists=True,
                is_verified=True
            )
        except MultipleObjectsReturned as e:
            phone_obj = core_models.Phone.objects.filter(
                number=d['phone']).last()
            phone_obj.is_primary = True
            phone_obj.is_exists = True
            phone_obj.is_verified = True
        phone_obj.user = u
        phone_obj.save()

    email_obj.user = u
    email_obj.save()
    del d['email']
    advisorprofile = crm_models.Advisor(user=u)
    for k, v in d.items():
        if hasattr(u, k):
            setattr(u, k, v)
        if hasattr(advisorprofile, k):
            setattr(advisorprofile, k, v)
    u.save()
    advisorprofile.save()
    advisor_data[key] = u
del x


# ----------------------------------------------------------------------------
# Load core_campaign into memory
# ----------------------------------------------------------------------------
# \copy (SELECT id, name from core_campaign) TO '/tmp/core_campaign.csv' DELIMITER '~';
campaigns = {}
c = 0
with open(os.path.join(filepath, "core_campaign.csv"), "rb") as csvfile:
    reader = csv.reader(csvfile, delimiter='~')
    for row in reader:
        if c == 0:
            c += 1
            continue
        if row:
            campaigns[row[0]] = row[1]


# COPY (select * from core_campaign_callers) TO '/tmp/core_campaign_callers.csv' DELIMITER '~' CSV HEADER;
# [id, campaign_id, caller_id]
c = 0
with open(os.path.join(filepath, "core_campaign_callers.csv"), "rb") as csvfile:
    reader = csv.reader(csvfile, delimiter='~')
    for row in reader:
        if c == 0:
            c += 1
            continue
        if row:
            q = crm_models.Queue.objects.filter(name=campaigns[row[1]]).last()
            if not q:
                q = crm_models.Queue.objects.create(
                    name=campaigns[row[1]], code=campaigns[row[1]])
            crm_models.QueueAdvisor.objects.get_or_create(
                queue=q, advisor=advisor_data[row[2]].advisor.first())


# ----------------------------------------------------------------------------
# Load core_custemer data into memory
# ----------------------------------------------------------------------------
# COPY (select id, name, mobile, mobile2, mobile3, mobile4 from core_customer) TO '/tmp/core_customer.csv' DELIMITER '~' CSV HEADER;
# Health Only
# COPY (SELECT DISTINCT "core_customer"."id", "core_customer"."name",
# "core_customer"."mobile", "core_customer"."mobile2",
# "core_customer"."mobile3", "core_customer"."mobile4" FROM
# "core_customer" INNER JOIN "core_campaigndata" ON ("core_customer"."id"
# = "core_campaigndata"."customer_id") INNER JOIN "core_campaign" ON
# ("core_campaigndata"."campaign_id" = "core_campaign"."id") WHERE
# UPPER("core_campaign"."name"::text) LIKE UPPER('health%')) TO
# '/tmp/core_customer.csv' DELIMITER '~' CSV HEADER;
c = 0
with open(os.path.join(filepath, "core_customer.csv"), "rb") as csvfile:
    reader = csv.reader(csvfile, delimiter='~')
    for row in reader:
        if c == 0:
            c += 1
            continue
        if row:
            mobiles_row = [row[2], row[3], row[4], row[5]]
            mobiles = []
            for mob in mobiles_row:
                if mob.strip() and len(mob.strip()) >= 10:
                    mobiles.append(mob[-10:])
            cust_data[row[0]] = {}
            cust_data[row[0]]['mobiles'] = mobiles
            cust_data[row[0]]['name'] = row[1][:50]

# ----------------------------------------------------------------------------
# Load core_emailcustemer data into memory
# ----------------------------------------------------------------------------
# For Health Only
# COPY (SELECT DISTINCT "core_emailcustomer"."id",
# "core_emailcustomer"."name", "core_emailcustomer"."email",
# "core_emailcustomer"."alt_emails", "core_emailcustomer"."customer_id"
# FROM "core_emailcustomer" INNER JOIN "core_emailcampaigndata" ON
# ("core_emailcustomer"."id" = "core_emailcampaigndata"."customer_id")
# INNER JOIN "core_emailcampaign" ON
# ("core_emailcampaigndata"."campaign_id" = "core_emailcampaign"."id")
# WHERE UPPER("core_emailcampaign"."name"::text) LIKE UPPER('health%')) TO
# '/tmp/core_emailcustomer.csv' DELIMITER '~' CSV HEADER;
no_mobile_cust_data = {}
c = 0
with open(os.path.join(filepath, "core_emailcustomer.csv"), "rb") as csvfile:
    reader = csv.reader(csvfile, delimiter='~')
    for row in reader:
        emails = []
        if row:
            try:
                int(row[0])
                # ['id', 'name', 'email', 'alt_emails', 'customer_id']
                if row[4] and cust_data.get(row[4]):
                    emails = [row[2]]
                    if row[3]:
                        emails.append(row[3])
                        if not cust_data[row[4]]['name']:
                            cust_data[row[4]]['name'] = row[1][:50]
                    cust_data[row[4]]['emails'] = emails
                else:
                    ecustomer = {}
                    ecustomer['name'] = row[1]
                    emails = [row[2]]
                    if row[3]:
                        emails.append(row[3])
                    ecustomer['emails'] = emails
                    no_mobile_cust_data[row[0]] = ecustomer
            except:
                pass
print "Customer Data is taken into memory with name, mobiles, and emails"

MOBILE_TRACKERID_MAP = {item[0]: item[
    1] for item in core_models.Phone.objects.values_list('number', 'user__tracker')}
EMAIL_TRACKERID_MAP = {item[0]: item[
    1] for item in core_models.Email.objects.values_list('email', 'user__tracker')}
TRACKERS = {
    t.id: t for t in core_models.Tracker.objects.filter(
        Q(id__in=MOBILE_TRACKERID_MAP.values()) | Q(
            id__in=EMAIL_TRACKERID_MAP.values())
    ).distinct()
}
TRACKERS.update({None: None, '': None})
TRACKER_MAP = {k: TRACKERS[v] for k, v in MOBILE_TRACKERID_MAP.items()}
for k, v in EMAIL_TRACKERID_MAP.items():
    TRACKER_MAP.update({k: TRACKERS[v]})

customer_count = 0
for cid, cdata in cust_data.items():
    if cdata['mobiles']:
        tracker = None
        for mob in cdata['mobiles']:
            if TRACKER_MAP.get(mob):
                tracker = TRACKER_MAP[mob]
                break
        for email in cdata.get('emails', []):
            if TRACKER_MAP.get(email):
                tracker = TRACKER_MAP[email]
                break

        if not tracker:
            session_key = 'phone_%s' % cdata['mobiles'][0]
            tracker, _ = core_models.Tracker.objects.get_or_create(
                session_key=session_key
            )
        mob_count = 0
        for mobile in cdata['mobiles']:
            try:
                phone, _ = core_models.Phone.objects.get_or_create(
                    number=mobile[-10:],
                )
            except MultipleObjectsReturned as e:
                phone = core_models.Phone.objects.filter(
                    number=mobile[-10:]).last()
            phone.visitor = tracker
            if mob_count == 0:
                phone.is_primary = True
            mob_count += 1
            phone.save()
        email_count = 0
        for email in emails:
            try:
                email, _ = core_models.Email.objects.get_or_create(
                    email=email.lower(),
                )
            except MultipleObjectsReturned as e:
                email = core_models.Email.objects.filter(
                    email=email.lower()).last()
            email.visitor = tracker
            if email_count == 0:
                email.is_primary = True
                email_count += 1
            email.save()
        cdata.update({'tracker': tracker})
        customer_count += 1
        if customer_count % 100 == 0:
            print customer_count

email_customer_count = 0
for cid, cdata in no_mobile_cust_data.items():
    if cdata['emails']:
        tracker = None
        for email in cdata['emails']:
            if TRACKER_MAP.get(email):
                tracker = TRACKER_MAP[email]
                break

        if not tracker:
            session_key = 'email_%s' % cdata['emails'][0]
            tracker, _ = core_models.Tracker.objects.get_or_create(
                session_key=session_key
            )
        tracker.name = cdata['name']
        tracker.save()
        email_count = 0
        for email in emails:
            try:
                email_obj, _ = core_models.Email.objects.get_or_create(
                    email=email.lower(),
                )
            except MultipleObjectsReturned as e:
                email_obj = core_models.Email.objects.filter(
                    email=email.lower()).last()
            email_obj.visitor = tracker
            if email_count == 0:
                email_obj.is_primary = True
            email_obj.save()
        cdata.update({'tracker': tracker})
        email_customer_count += 1
        if email_customer_count % 100 == 0:
            print email_customer_count


def get_product_name(campaign):
    if campaign == 'one-health':
        return 'health'
    elif campaign.startswith('motor-bike'):
        return 'bike'
    elif campaign.startswith('motor'):
        return 'car'
    else:
        return campaign.split('-')[0]


# -------------------------------------
# Transaction list expected from product_side with row like
# ['transaction_id', 'policy']
product_transactions = {}
policies_id_map = {p.id: p for p in core_models.Policy.objects.all()}
policies_id_map.update({'': None, None: None})
motor_trs_id_map = {item[0]: item[1] for item in MotorTransaction.objects.filter(status__iendswith='COMPLETED').values_list('transaction_id', 'id')}
travel_trs_id_map = {item[0]: item[1] for item in TravelTransaction.objects.filter(status__iendswith='COMPLETED').values_list('transaction_id', 'id')}
health_trs_id_map = {item[0]: item[1] for item in HealthTransaction.objects.filter(status__iendswith='COMPLETED').values_list('transaction_id', 'id')}

health_trs = HealthTransaction.objects.filter(status__iendswith='COMPLETED').values_list('transaction_id', 'policy')
for item in health_trs:
    product_transactions[item[0]] = {
        'product': 'health',
        'transaction': health_trs_id_map[item[0]],
        'policy': item[1] if item[1] else None,
    }

motor_trs = MotorTransaction.objects.filter(status__iendswith='COMPLETED').values_list('transaction_id', 'policy')
for item in motor_trs:
    product_transactions[item[0]] = {
        'product': 'motor',
        'transaction': motor_trs_id_map[item[0]],
        'policy': item[1] if item[1] else None,
    }

travel_trs = TravelTransaction.objects.filter(status__iendswith='COMPLETED').values_list('transaction_id', 'policy')
for item in travel_trs:
    product_transactions[item[0]] = {
        'product': 'travel',
        'transaction': travel_trs_id_map[item[0]],
        'policy': item[1] if item[1] else None,
    }


# ----------------------------------------------------------------------------
# Process core_campaigndata
# ----------------------------------------------------------------------------
c = 0
campaigndatas = {}
# id~data~application_id~status~assigned_to_id~created_on~campaign_id~customer_id~last_call_time~last_call_disposition~next_call_time~is_taken~initial_data~click_call_time~past_policy_expiry_date~assigned_to_time~is_clean~is_active~health_case_type~travel_date~label
# For Health Only
# COPY (SELECT * FROM "core_campaigndata" INNER JOIN "core_campaign" ON
# ("core_campaigndata"."campaign_id" = "core_campaign"."id") WHERE
# ("core_campaigndata"."is_active" = True  AND
# UPPER("core_campaign"."name"::text) LIKE UPPER('health%')  AND NOT
# (UPPER("core_campaign"."name"::text) LIKE UPPER('health-renewal%') )))
# TO '/tmp/core_campaigndata.csv' DELIMITER '~' CSV HEADER;
with open(os.path.join(filepath, 'core_campaigndata.csv'), 'rb') as csvfile:
    csv_reader = csv.reader(csvfile, delimiter='~')
    for row in csv_reader:
        if c == 0:
            c += 1
            continue
        campaigndatas[row[0]] = row

print "All campaigndatas taken into memory"

# Campaigndataid - History mapping
# COPY (SELECT "core_campaigndata_history"."campaigndata_id",
# "core_history"."id", "core_history"."call_disposition",
# "core_history"."caller_id", "core_history"."created_on",
# "core_history"."get_call_time", "core_history"."is_customer_scheduled",
# "core_history"."data", "core_history"."changed_data",
# "core_history"."call_type" FROM "core_history" LEFT OUTER JOIN
# "core_campaigndata_history" ON ("core_history"."id" =
# "core_campaigndata_history"."history_id")) TO '/tmp/core_history.csv'
# DELIMITER '~' CSV HEADER;

# ############### For Health Only, down here ##########
# COPY (SELECT "core_campaigndata_history"."campaigndata_id",
# "core_history"."id", "core_history"."call_disposition",
# "core_history"."caller_id", "core_history"."created_on",
# "core_history"."get_call_time", "core_history"."is_customer_scheduled",
# "core_history"."data", "core_history"."changed_data",
# "core_history"."call_type" FROM "core_history" LEFT OUTER JOIN
# "core_campaigndata_history" ON ("core_history"."id" =
# "core_campaigndata_history"."history_id") INNER JOIN "core_campaigndata"
# ON ("core_campaigndata_history"."campaigndata_id" =
# "core_campaigndata"."id") INNER JOIN "core_campaign" ON
# ("core_campaigndata"."campaign_id" = "core_campaign"."id") WHERE
# UPPER("core_campaign"."name"::text) LIKE UPPER('health%')) TO
# '/tmp/core_history.csv' DELIMITER '~' CSV HEADER;
c = 0
history_data = {}
with open(os.path.join(filepath, 'core_history.csv'), 'rb') as csvfile:
    csv_reader = csv.reader(csvfile, delimiter='~')
    for row in csv_reader:
        if c == 0:
            c += 1
            continue
        if row and row[0]:
            if history_data.get(row[0]):
                history_data[row[0]].append(row)
            else:
                history_data[row[0]] = [row]

print "All history data taken into memory"


def get_val(data, key):
    x = data.get(key) or ''
    if x == 'None':
        x = ''
    return x


MID_CACHE = {}

def get_mid_obj(cd_data):
    data = {}
    for k, v in cd_data.items():
        if mid_mapping.get(k) and cd_data.get(k):
            data[mid_mapping[k]] = cd_data[k]

    mid, _ = core_models.Marketing.objects.get_or_create(
        campaign=get_val(data, 'campaign'),
        source=get_val(data, 'source'),
        medium=get_val(data, 'medium'),
        category=get_val(data, 'catgory'),
        brand=get_val(data, 'brand'),
        content=get_val(data, 'content'),
    )
    return mid.id


def get_mid_from_cache(data):
    mid_str = core_models.Marketing.mid(data)
    if MID_CACHE.get(mid_str):
        return MID_CACHE[mid_str]
    else:
        return get_mid_obj(data)


# cache mids
for cdid, row in campaigndatas.items():
    try:
        cd_idata = json.loads(row[12])
    except:
        cd_idata = json.loads(row[1])
    cd_data = json.loads(row[1])
    datas = [cd_idata, cd_data]
    for hist in history_data.get(cdid, []):
        if hist[3] == SYSTEM_CALLER_ID:
            try:
                chdata = json.loads(hist[8])
            except:
                chdata = {}
            datas.append(chdata)
    for d in datas:
        MID_CACHE[core_models.Marketing.mid(d)] = get_mid_from_cache(d)


c = 0
lms_transaction_data = {}
trs_closed_campaigndatas = {}
campaigndata_last_payment_map = {}
# COPY (select transaction_id, campaigndata_id, payment_date,
# core_transaction.assigned_to_id, type from core_transaction INNER JOIN
# "core_campaigndata" ON ("core_transaction"."campaigndata_id" =
# "core_campaigndata"."id") INNER JOIN "core_campaign" ON
# ("core_campaigndata"."campaign_id" = "core_campaign"."id") where
# (UPPER("core_campaign"."name"::text) LIKE UPPER('health%') and
# is_success = 't') order by payment_date) TO
# '/tmp/core_transction.csv' DELIMITER '~' CSV HEADER;
with open(os.path.join(filepath, 'core_transaction.csv'), 'rb') as csvfile:
    csv_reader = csv.reader(csvfile, delimiter='~')
    for row in csv_reader:
        if c == 0:
            c += 1
            continue
        if row:
            lms_transaction_data[row[0]] = {
                'campaigndata_id': row[1],
                'payment_date': row[2],
                'assigned_to': row[3],
            }
            campaigndata_last_payment_map[row[1]] = row[2]
            if campaigndatas[row[1]][3] == 'CONCLUDED':
                trs_closed_campaigndatas[row[1]] = {}

print "All tranasaction data taken into memory"


def get_form_sections(role, d, classes_list):

    fsections = []
    if isinstance(d, dict):
        d.update({'role': role})
        for cls in classes_list:
            f = getattr(core_forms, cls)(d)
            if f.is_valid() and f.cleaned_data:
                cleaned_data = {k: str(v)
                                       for k, v in f.cleaned_data.items() if v}
                if not ['role'] == cleaned_data.keys():
                    fsections.append(('core.forms.%s' %
                                     cls, dict2hstore(cleaned_data)))
    return fsections


def get_all_form_sections(data):
    if data.get('mobile'):
        data['mobile'] = data['mobile'][-10:]
    if data.get('user_data') or data.get('members'):
        user_data = data.get('user_data') or data.copy()
        data.update(user_data)
        if data.get('medical_conditions'):
            data['medical_conditions'] = [str(item) for item in wiki_models.MedicalCondition.objects.filter(
                id__in=data['medical_conditions']).values_list('slug', flat=True)]
        data.update({'city': user_data.get('user_city')})
        parents_data = {
            'pincode': user_data.get('parents_pincode'),
            'city': user_data.get('parents_city'),
        }

        for member in user_data.get('members', []):
            if member['person'].lower() == 'you':
                data.update(member)
                li = user_data['members']
                li.remove(member)
                user_data['members'] = li
                continue
            elif member['person'].lower() in ['father', 'mother']:
                member.update(parents_data)
    else:
        user_data = {}
        pass

    fsections = []
    fsections.extend(get_form_sections('self', data, [
                     'Person', 'Address', 'MedicalCondition', 'HealthProductPreference']))
    for member in user_data.get('members', []):
        fsections.extend(get_form_sections(member['person'].lower(), member, [
                         'Person', 'Address', 'MedicalCondition', 'HealthProductPreference']))
    fd = {}
    return fsections


def create_customer_activity(
    data, created_date_str, requirement, tracker, user, activity_type):
    mid = get_mid_from_cache(data)

    activity_data = {
        'requirement_id': requirement.id,
        'tracker_id': tracker.id,
        'extra': data,
        'activity_type': activity_type,
        'created': created_date_str,
        'mid_id': mid
    }
    for k, v in data.items():
        if activity_param_map.get(k) and v and not v == 'None':
            activity_data[activity_param_map[k]] = v
    return activity_data

LMS_CRM_DISPOSITION_MAP_STR = {
    'ALREADY_PURCHASED': ('ALREADY_PURCHASED',
        'Already Purchased',
        'DEAD',
        'Dead'),
    'BIKE_LEAD': ('BIKE_LEAD',
        'Bike Lead',
        'INTERESTED_IN_DIFFERENT_PRODUCT',
        'Dead - Interested in different product'),
    'BUSY': ('BUSY', 'Busy', 'NO_CONVERSATION', 'No Conversation'),
    'CALLBACK_LATER': ('CALLBACK_LATER',
        'No Conversation',
        'NO_CONVERSATION',
        'No Conversation'),
    'CALL_DROPPED': ('CALL_DROPPED',
        'Call dropped',
        'NO_CONVERSATION',
        'No Conversation'),
    'CAR_LEAD': ('CAR_LEAD',
        'Car Lead',
        'INTERESTED_IN_DIFFERENT_PRODUCT',
        'Dead - Interested in different product'),
    'CASHLESS_GARAGE_NOT_AVAILABLE': ('CASHLESS_GARAGE_NOT_AVAILABLE',
        'Cashless Garage not Available',
        'LOST_CASES',
        'Lost Cases'),
    'COMMERCIAL_VEHICLE': ('COMMERCIAL_VEHICLE',
        'Commercical Vehicle',
        'LOST_CASES',
        'Lost Cases'),
    'COMPARING_FROM_COMPETITOR': ('COMPARING_FROM_COMPETITOR',
        'Comparing from Competitor',
        'QUALIFIED',
        'Qualified'),
    'COMPARISON_SENT': ('COMPARISON_SENT',
        'Quotes sent',
        'QUALIFIED',
        'Qualified'),
    'COMPETITOR': ('COMPETITOR', 'Competitor', 'DEAD', 'Dead'),
    'CUSTOMER_NOT_RESPONDING': ('CUSTOMER_NOT_RESPONDING',
        'Customer not responding',
        'DEAD',
        'Dead'),
    'CUSTOMER_POSTPONED_BUY': ('CUSTOMER_POSTPONED_BUY',
        'Customer postpones Buy',
        'DEAD',
        'Dead'),
    'CUSTOMER_UNHAPPY_WITH_PREMIUM': ('CUSTOMER_UNHAPPY_WITH_PREMIUM',
        'Customer Unhappy with Premium',
        'LOST_CASES',
        'Lost Cases'),
    'CUSTOMER_WILL_CALLBACK': ('CALLBACK_LATER',
            'Callback Later',
            'NO_CONVERSATION',
            'No Conversation'),
    'DEAD_COMMERCIAL_VEHICLE': ('DEAD_COMMERCIAL_VEHICLE',
            'Commercial vehicle',
            'DEAD',
            'Dead'),
    'DECISION_PENDING': ('DECISION_PENDING', 'Decision Pending', 'DEAD', 'Dead'),
    'DO_NOT_CALL': ('DO_NOT_CALL', 'Do not call', 'DEAD', 'Dead'),
    'DUPLICATE': ('DUPLICATE', 'Duplicate', 'DEAD', 'Dead'),
    'ENGAGED': ('ENGAGED', 'Engaged', 'NO_CONVERSATION', 'No Conversation'),
    'FAILED_CONTACT': ('FAILED_CONTACT',
            'Failed contact',
            'NO_CONVERSATION',
            'No Conversation'),
    'FORM_SENT': ('FORM_SENT', 'Form sent', 'QUALIFIED', 'Qualified'),
    'GENERAL_BROWSING': ('GENERAL_BROWSING', 'General browsing', 'DEAD', 'Dead'),
    'HEALTH_LEAD': ('HEALTH_LEAD',
            'Health Lead',
            'INTERESTED_IN_DIFFERENT_PRODUCT',
            'Dead - Interested in different product'),
    'IDV_NOT_AVAILABLE': ('IDV_NOT_AVAILABLE',
            'IDV Not Available',
            'LOST_CASES',
            'Lost Cases'),
    'INQUIRY_ON_PRODUCT_BOUGHT': ('ALREADY_PURCHASED',
            'Already Purchased',
            'DEAD',
            'Dead'),
    'INSPECTION_PAYMENT_MADE': ('INSPECTION_PAYMENT_MADE',
            'Inspection Payment Made',
            'PAYMENT_RECEIVED',
            'UnderWriting'),
    'INSPECTION_PAYMENT_PENDING': ('INSPECTION_PAYMENT_PENDING',
            'INSPECTION_PAYMENT_PENDING',
            'PAYMENT_RECEIVED,',
            'PAYMENT_RECEIVED,'),
    'INSPECTION_REQUIRED': ('INSPECTION_REQUIRED',
            'INSPECTION_REQUIRED',
            'PAYMENT_RECEIVED,',
            'PAYMENT_RECEIVED,'),
    'INTERESTED_IN_OTHER_PRODUCTS': ('INTERESTED_IN_OTHER_PRODUCTS',
            'INTERESTED_IN_OTHER_PRODUCTS',
            'DEAD,',
            'DEAD,'),
    'INTERNAL_TESTING': ('INTERNAL_TESTING', 'Internal testing', 'DEAD', 'Dead'),
    'INVALID_INQUIRY': ('INVALID_INQUIRY', 'Invalid enquiry', 'DEAD', 'Dead'),
    'INVALID_NUMBER': ('INVALID_NUMBER', 'Invalid number', 'DEAD', 'Dead'),
    'ISSUED': ('ISSUED', 'Issued', 'PAYMENT_RECEIVED', 'Underwriting'),
    'LANGUAGE_BARRIER': ('LANGUAGE_BARRIER', 'Language barrier', 'DEAD', 'Dead'),
    'LIKELY_TO_BUY': ('LIKELY_TO_BUY', 'Likely to buy', 'QUALIFIED', 'Qualified'),
    'LOST_CUSTOMER_NOT_RESPONDING': ('CUSTOMER_NOT_RESPONDING',
            'Customer not responding',
            'DEAD',
            'Dead'),
    'LOST_CUSTOMER_WILL_GET_BACK': ('LOST_NOT_INTERESTED_ANYMORE',
            'LOST_NOT_INTERESTED_ANYMORE',
            'LOST_CASES,',
            'LOST_CASES,'),
    'LOST_NOT_INTERESTED_ANYMORE': ('LOST_NOT_INTERESTED_ANYMORE',
            'LOST_NOT_INTERESTED_ANYMORE',
            'LOST_CASES,',
            'LOST_CASES,'),
    'MEDICALLY_UNFIT': ('MEDICALLY_UNFIT', 'Medically unfit', 'DEAD', 'Dead'),
    'MOTOR_ONLINE_CASE': ('MOTOR_ONLINE_CASE',
            'MOTOR_ONLINE_CASE',
            'DEAD_TRANSFER,',
            'DEAD_TRANSFER,'),
    'NOT_ELIGIBLE_PROFILE': ('NOT_ELIGIBLE_PROFILE',
            'Not eligible profile',
            'DEAD',
            'Dead'),
    'NOT_HAPPY_WITH_INSURER_SERVICE_RENEWAL': ('NOT_HAPPY_WITH_INSURER_SERVICE_RENEWAL',
            'NOT_HAPPY_WITH_INSURER_SERVICE_RENEWAL',
            'LOST_CASES,',
            'LOST_CASES,'),
    'NOT_HAPPY_WITH_QUOTE': ('NOT_HAPPY_WITH_QUOTE',
            'Not happy with our quote',
            'QUALIFIED',
            'Qualified'),
    'NOT_INTERESTED': ('NOT_INTERESTED', 'Not interested', 'DEAD', 'Dead'),
    'NOT_WILLING_TO_PAY_ONLINE': ('NOT_WILLING_TO_PAY_ONLINE',
            'Not willing to pay online',
            'DEAD',
            'Dead'),
    'NO_CONVERSATION_CALLBACK_LATER': ('CALLBACK_LATER',
            'No Conversation',
            'NO_CONVERSATION',
            'No Conversation'),
    'OFFLINE': ('OFFLINE', 'Offline Payment', 'PAYMENT_RECEIVED', 'Underwriting'),
    'OFFLINE_PRODUCTS': ('OFFLINE_PRODUCTS',
            'OFFLINE_PRODUCTS',
            'DEAD,',
            'DEAD,'),
    'OUT_OF_COVERAGE': ('OUT_OF_COVERAGE',
            'Out of coverage',
            'NO_CONVERSATION',
            'No Conversation'),
    'PAYMENT_CF': ('PAYMENT_CF',
            'Payment on CoverFox Portal',
            'PAYMENT_RECEIVED',
            'Underwriting'),
    'PAYMENT_NCF': ('PAYMENT_NCF',
            'Payment on Insurance Company Portal',
            'PAYMENT_RECEIVED',
            'Underwriting'),
    'PAYMENT_WITH_INSURER_DIRECTLY_RENEWAL': ('PAYMENT_WITH_INSURER_DIRECTLY_RENEWAL',
            'PAYMENT_WITH_INSURER_DIRECTLY_RENEWAL',
            'LOST_CASES,',
            'LOST_CASES,'),
    'PRODUCT_NOT_AVAILABLE': ('PRODUCT_NOT_AVAILABLE',
            'PRODUCT_NOT_AVAILABLE',
            'LOST_CASES,',
            'LOST_CASES,'),
    'PURCHASED_FROM_COMPANY': ('PURCHASED_FROM_COMPANY',
            'PURCHASED_FROM_COMPANY',
            'LOST_CASES,',
            'LOST_CASES,'),
    'QUALIFIED_BUSY': ('BUSY', 'Busy', 'NO_CONVERSATION', 'No Conversation'),
    'QUALIFIED_CALLBACK_LATER': ('CALLBACK_LATER',
            'No Conversation',
            'NO_CONVERSATION',
            'No Conversation'),
    'QUALIFIED_CUSTOMER_WILL_CALLBACK': ('QUALIFIED_CUSTOMER_WILL_CALLBACK',
            'Customer will Callback Later',
            'QUALIFIED',
            'Qualified'),
    'QUALIFIED_RINGING': ('RINGING',
            'Ringing',
            'NO_CONVERSATION',
            'No Conversation'),
    'QUALIFIED_TRANSACTION_UNSUCCESSFUL': ('QUALIFIED_TRANSACTION_UNSUCCESSFUL',
            'QUALIFIED_TRANSACTION_UNSUCCESSFUL',
            'QUALIFIED,',
            'QUALIFIED,'),
    'QUERY_CALL': ('QUERY_CALL',
            'Customer called for Product Enquiry',
            'PAYMENT_RECEIVED',
            'Underwriting'),
    'RINGING': ('RINGING', 'Ringing', 'NO_CONVERSATION', 'No Conversation'),
    'SOLD_BY_AGENT': ('SOLD_BY_AGENT',
            'SOLD_BY_AGENT',
            'LOST_CASES,',
            'LOST_CASES,'),
    'SWITCHED_OFF': ('SWITCHED_OFF',
            'Switched off',
            'NO_CONVERSATION',
            'No Conversation'),
    'TERM_LEAD': ('TERM_LEAD',
            'Term Lead',
            'INTERESTED_IN_DIFFERENT_PRODUCT',
            'Dead - Interested in different product'),
    'TRAVEL_LEAD': ('TRAVEL_LEAD',
            'Travel Lead',
            'INTERESTED_IN_DIFFERENT_PRODUCT',
            'Dead - Interested in different product'),
    'UNAWARE_OF_GIVING_NUMBER': ('UNAWARE_OF_GIVING_NUMBER',
            'Unaware of giving number',
            'DEAD',
            'Dead'),
    'WANTS_ONLY_THIRD_PARTY': ('WANTS_ONLY_THIRD_PARTY',
            'Wants only third party',
            'DEAD',
            'Dead'),
    'WILL_BUY_NEXT_MONTH': ('WILL_BUY_NEXT_MONTH',
            'Will buy next month',
            'QUALIFIED',
            'Qualified'),
    'WRONG_NUMBER': ('WRONG_NUMBER', 'Wrong number', 'DEAD', 'Dead')
}

LMS_CRM_DISPOSITION_MAP = {}

for k, v in LMS_CRM_DISPOSITION_MAP_STR.items():
    d, _ = crm_models.Disposition.objects.get_or_create(
        value=v[0], defaults={'verbose': v[1]})
    sd, _ = crm_models.SubDisposition.objects.get_or_create(
        value=v[2], defaults={'verbose': v[3]})
    LMS_CRM_DISPOSITION_MAP[k] = (d.id, sd.id)


CLOSED_resolved, _ = sm_models.State.objects.get_or_create(
    name='CLOSED_resolved')

for trid in lms_transaction_data.keys():
    try:
        if not product_transactions.get(trid):
            print "Transaction id does not exist in product %s" % trid
            continue

        prod_tr = product_transactions[trid]['transaction']
        prod_policy = policies_id_map[
            product_transactions[trid].get('policy', '')]

        campaigndata_id = lms_transaction_data[trid]['campaigndata_id']
        campaigndata = campaigndatas[campaigndata_id]
        campaigndata_last_payment_map[
            campaigndata_id] = lms_transaction_data[trid]['payment_date']

        customer = cust_data[campaigndata[7]]
        tracker = customer['tracker']
        user = tracker.user if tracker else None

        requirement_kind, _ = core_models.RequirementKind.objects.get_or_create(
            kind=core_models.RequirementKind.NEW_POLICY,
            product=get_product_name(campaigns[campaigndata[6]]),
            sub_kind="",
        )

        cd_data = json.loads(campaigndata[1])
        mid = get_mid_from_cache(cd_data)

        owner_id = str(lms_transaction_data[trid][
                        'assigned_to']) or campaigndata[4]
        if prod_policy and prod_policy.requirement_sold and prod_policy.requirement_sold:
            requirement = prod_policy.requirement_sold
            requirement.mid_id = mid
            requirement.visitor = tracker
            if owner_id:
                requirement.owner = advisor_data[owner_id].advisor.last()
        else:
            requirement = core_models.Requirement.objects.create(
                user=user,
                mid_id=mid,
                kind=requirement_kind,
                policy=prod_policy,
                current_state=CLOSED_resolved,
                owner=advisor_data[owner_id].advisor.last(),
                remark=cd_data.get('notes', ''),
                extra=cd_data,
                fixed=False,
            )
            requirement.created_date = get_datetime(campaigndata[5])
            fd = core_models.FormData.objects.create(user=user, visitor=tracker)
            requirement.fd = fd
            requirement.save()
            sql_query = "insert into core_formsection (form, data, created_date, modified_date) values "
            req_fses = get_all_form_sections(cd_data)
            for i, item in enumerate(req_fses):
                sql_query += "('%s', '%s', TIMESTAMP '%s', TIMESTAMP '%s'), " % (item[0], item[1], datetime.datetime.strftime(datetime.datetime.now(), "%m-%d-%Y %H:%M:%S"), datetime.datetime.strftime(datetime.datetime.now(), "%m-%d-%Y %H:%M:%S"))
            sql_query = sql_query[:-2] + " returning id;"
            cursor.execute(sql_query)
            fs_ids = [item[0] for item in cursor.fetchall()]
            fd.sections.add(*fs_ids)

        cd_idata=json.loads(campaigndata[12])
        histories=history_data[campaigndata[0]]
        web_activities=[create_customer_activity(cd_idata, campaigndata[5], requirement, tracker, user, 'WEBSITE_VISIT')]
        advisor_activities=[]
        form_sections=get_all_form_sections(cd_idata)
        for hist in histories:
            activity_start_date=campaigndata_last_payment_map.get(
                campaigndata[0]) or campaigndata[5]
            if lms_transaction_data[trid]['payment_date'] and get_datetime(hist[4]) > get_datetime(
                lms_transaction_data[trid]['payment_date']) or get_datetime(hist[4]) < get_datetime(activity_start_date):
                continue
            if hist[3] == SYSTEM_CALLER_ID:
                d = hist[8]
                if hist[2] == 'DUPLICATE':
                    activity_type='WEBSITE_VISIT'
                elif hist[2] == 'ASSIGNED_TO_CALLER':
                    d = hist[7]
                    activity_type = 'TRANSFER'
                else:
                    activity_type=hist[2]
                try:
                    adata=json.loads(d)
                except:
                    adata={}
                web_activities.append(create_customer_activity(
                    adata, hist[4], requirement, tracker, user, activity_type))
            else:
                category='OUTGOING_CALL'
                if hist[9] == 'in':
                    category='INCOMING_CALL'
                if hist[5]:
                    start_time=row[5]
                else:
                    start_time=row[4]
                advisor_activities.append((
                    advisor_data[hist[3]].advisor.last().id,
                    get_sql_datetime(start_time),
                    get_sql_datetime(hist[4]),
                    LMS_CRM_DISPOSITION_MAP[row[2]][1],
                    LMS_CRM_DISPOSITION_MAP[row[2]][1],
                    row[8],
                    category,
                ))
        # CREATE ALL ADVISOR ACTIVITIES
        if advisor_activities:
            sql_query="insert into crm_activity (user_id, start_time, end_time, disposition_id, sub_disposition_id, data, category) values "
            for act in advisor_activities:
                sql_query += "(%s, TIMESTAMP '%s', TIMESTAMP '%s', %s, %s, '%s', %s), "
            sql_query = sql_query[:-2] + " returning id;"
            cursor.execute(sql_query)
            adv_act_objs = [item[0] for item in cursor.fetchall()]
        else:
            adv_act_objs = []
        requirement.crm_activities.add(*adv_act_objs)

        # CREATE ALL FDS
        fses_count_map = {}
        if web_activities:
            sql_query="insert into core_formdata (user_id, visitor_id, created_date, modified_date) values "
            for i, hist in enumerate(web_activities):
                fs = get_all_form_sections(hist['extra'])
                fses_count_map[i] = len(fs)
                sql_query += "(%s, %s, TIMESTAMP '%s', TIMESTAMP '%s'), " % (user.id if user else 'NULL', tracker.id if tracker else 'NULL', datetime.datetime.strftime(datetime.datetime.now(), "%m-%d-%Y %H:%M:%S"), datetime.datetime.strftime(datetime.datetime.now(), "%m-%d-%Y %H:%M:%S"))
                form_sections.extend(fs)
            sql_query = sql_query[:-2] + " returning id;"
            cursor.execute(sql_query)
            fd_ids = [item[0] for item in cursor.fetchall()]
        else:
            fd_ids = []

        # CREATING ALL FSES
        if form_sections:
            sql_query = "insert into core_formsection (form, data, created_date, modified_date) values "
            for i, item in enumerate(form_sections):
                sql_query += "('%s', '%s', TIMESTAMP '%s', TIMESTAMP '%s'), " % (item[0], item[1], datetime.datetime.strftime(datetime.datetime.now(), "%m-%d-%Y %H:%M:%S"), datetime.datetime.strftime(datetime.datetime.now(), "%m-%d-%Y %H:%M:%S"))
            sql_query = sql_query[:-2] + " returning id;"
            cursor.execute(sql_query)
            fs_ids = [item[0] for item in cursor.fetchall()]
        else:
            fs_ids = []


        fd_fs_map = {}
        count = 0
        for i in range(len(fd_ids)):
            count += fses_count_map[i]
            fd_fs_map[fd_ids[i]] = fs_ids[count:count+fses_count_map[i]]

        if fd_fs_map:
            sql_query = "insert into core_formdata_sections (formdata_id, formsection_id) values "
            for fd_id, v in fd_fs_map.items():
                for fs_id in v:
                    sql_query += "(%s, %s), " % (fd_id, fs_id)
            sql_query = sql_query[:-2] + ';'
            cursor.execute(sql_query)
        sql_query = """insert into core_activity (activity_id, mid_id, requirement_id, tracker_id, fd_id, is_processed,\
            extra, gclid, term, match_type, target, placement, label, feed_item, landing_url, url, page_id,\
            form_name, form_position, product, form_variant, activity_type, browser, os, device, device_model,\
            ip_address, state, city, isp, referer, request_id, created, updated_on) values """

        for i, adv_act in enumerate(web_activities):
            ad = web_activities[i]
            fd = fd_ids[i]
            extra = json.dumps(ad['extra'])
            extra = extra.replace("'", '\"')

            sql_query += """('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', '%s', '%s', '%s', '%s', TIMESTAMP '%s', TIMESTAMP '%s'), """% ( str(uuid.uuid4()), get_mid_from_cache(ad['extra']), requirement.id, tracker.id, fd_ids[i], 'false', extra, ad.get('gclid',''), ad.get('term', ''), ad.get('match_type', ''), ad.get('target', ''), ad.get('placement', ''), ad.get('label', ''), ad.get('feed_item', ''), ad.get('landing_page_url', ''), ad.get('url', ''), ad.get('page_id', ''), ad.get('form_name', ''), ad.get('form_position', ''), ad.get('product', ''), ad.get('form_variant', ''), ad.get('activity_type', ''), ad.get('browser', ''), ad.get('os', ''), ad.get('device', ''), ad.get('device_model', ''), ad.get('request_ip') or 'NULL', ad.get('state', ''), ad.get('city', ''), ad.get('isp', ''), ad.get('referer', ''), ad.get('request_id', ''), get_sql_datetime(ad['created']), get_sql_datetime(ad['created']))

        sql_query = sql_query[:-2] + " returning id;"
        cursor.execute(sql_query)

        campaigndata_last_payment_map[
            campaigndata_id]=lms_transaction_data[trid]['payment_date']
        if requirement.owner and 'autoassign@crm.coverfox.com' in requirement.owner.user.emails.values_list(
            'email', flat=True) and hist[3] is not 'CONCLUDED' and campaigndata[10]:
            task=crm_models.Task.objects.create(
                category=crm_models.TaskCategory.objects.get(
                    name='CALLBACK_LATER'),
                assigned_to=requirement.owner,
                scheduled_time=get_datetime(campaigndata[10])
            )
            requirement.tasks.add(task)
        print requirement.id
    except Exception as e:
        print row, e
        log_file.write(str(row))
        log_file.write('\n')
        log_file.write(str(e))
        log_file.write('\n')


print "Requirements created for campaigndata whose transaction is there in LMS"

NO_TRS_LMS_STATUS_STATE_MAP_STR={
'FRESH': 'FRESH_new',
'DUMMY_FRESH': 'FRESH_ringing',
'IN_PROCESS': ['QUALIFIED_likelytobuy', 'QUALIFIED_quotessent', 'QUALIFIED_formsent', 'QUALIFIED_paymentgateway', 'QUALIFIED_paymentfailed', 'QUALIFIED_proposalfailed', 'QUALIFIED_transactiondrop'],
'CONCLUDED': 'CLOSED_unresolved',
}

NO_TRS_LMS_STATUS_STATE_MAP={}
for k, v in NO_TRS_LMS_STATUS_STATE_MAP_STR.items():
    if not isinstance(v, list):
        x, _=sm_models.State.objects.get_or_create(name=v)
        NO_TRS_LMS_STATUS_STATE_MAP[k]=x
    else:
        NO_TRS_LMS_STATUS_STATE_MAP[k]=v


# ############### No transaction campaigndatas ###############
# 0id~1data~2application_id~3status~4assigned_to_id~5created_on~6campaign_id~7customer_id~8last_call_time~9last_call_disposition~10next_call_time~11is_taken~12initial_data~13click_call_time~14past_policy_expiry_date~15assigned_to_time~16is_clean~17is_active~18health_case_type~19travel_date~20label

for cdid, row in campaigndatas.items():
    try:
        if cdid in trs_closed_campaigndatas.keys():
            continue
        try:
            cd_idata=json.loads(row[12])
        except:
            cd_idata=json.loads(row[1])
        cd_data=json.loads(row[1])

        customer=cust_data[row[7]]
        tracker=customer['tracker']

        requirement_kind, _=core_models.RequirementKind.objects.get_or_create(
            kind=core_models.RequirementKind.NEW_POLICY,
            product=get_product_name(campaigns[row[6]]),
            sub_kind="",
        )
        if isinstance(NO_TRS_LMS_STATUS_STATE_MAP.get(
            row[3], 'IN_PROCESS'), list):
            # For requirements with DUMMY state, accurate current_state will be
            # determined in rblog by core.Activity and crm.Activity
            current_state=DUMMY_STATE
        else:
            current_state=NO_TRS_LMS_STATUS_STATE_MAP.get(row[3], DUMMY_STATE)

        owner_id=row[4]
        user=tracker.user if tracker else None
        cd_data['name'] = customer.get('name', '')
        requirement=core_models.Requirement.objects.create(
            user=user,
            mid_id=get_mid_from_cache(cd_data),
            visitor=tracker,
            kind=requirement_kind,
            current_state=current_state,
            owner=advisor_data[owner_id].advisor.last(),
            remark=cd_data.get('notes', ''),
            extra=cd_data
        )
        fd = core_models.FormData.objects.create(user=user, visitor=tracker)
        requirement.created_date = get_datetime(row[5])
        requirement.fd = fd
        requirement.save()
        sql_query = "insert into core_formsection (form, data, created_date, modified_date) values "
        cd_data['name'] = customer.get('name', '')
        req_fses = get_all_form_sections(cd_data)
        for i, item in enumerate(req_fses):
            sql_query += "('%s', '%s', TIMESTAMP '%s', TIMESTAMP '%s'), " % (item[0], item[1], datetime.datetime.strftime(datetime.datetime.now(), "%m-%d-%Y %H:%M:%S"), datetime.datetime.strftime(datetime.datetime.now(), "%m-%d-%Y %H:%M:%S"))
        sql_query = sql_query[:-2] + " returning id;"
        cursor.execute(sql_query)
        fs_ids = [item[0] for item in cursor.fetchall()]
        fd.sections.add(*fs_ids)

        histories=history_data.get(cdid, [])
        web_activities=[create_customer_activity(cd_idata, row[5], requirement, tracker, user, 'WEBSITE_VISIT')]
        advisor_activities=[]
        form_sections=get_all_form_sections(cd_idata)

        for hist in histories:
            if campaigndata_last_payment_map.get(cdid) and get_datetime(
                hist[4]) < get_datetime(campaigndata_last_payment_map[cdid]):
                continue
            elif hist[3] == SYSTEM_CALLER_ID:
                d = hist[8]
                if hist[2] == 'DUPLICATE':
                    activity_type='WEBSITE_VISIT'
                elif hist[2] == 'ASSIGNED_TO_CALLER':
                    d = hist[7]
                    activity_type = 'TRANSFER'
                else:
                    activity_type=row[2]
                try:
                    adata=json.loads(d)
                except:
                    adata={}
                web_activities.append(create_customer_activity(
                adata, hist[4], requirement, tracker, user, activity_type))
            else:
                category='OUTGOING_CALL'
                if hist[9] == 'in':
                    category='INCOMING_CALL'
                if hist[5]:
                    start_time=get_datetime(hist[5])
                else:
                    start_time=get_datetime(hist[4])
                crm_act=crm_models.Activity.objects.create(
                    user=crm_models.Advisor.objects.get(
                        user=advisor_data[hist[3]]),
                    start_time=start_time,
                    end_time=get_datetime(hist[4]),
                    disposition_id=LMS_CRM_DISPOSITION_MAP[hist[2]][0],
                    sub_disposition_id=LMS_CRM_DISPOSITION_MAP[hist[2]][1],
                    data=hist[8],
                    category=category,
                    fixed=False,
                )
                crm_act.save()
                requirement.crm_activities.add(crm_act)

        if advisor_activities:
            sql_query="insert into crm_activity (user_id, start_time, end_time, disposition_id, sub_disposition_id, data, category) values "
            for act in advisor_activities:
                sql_query += "(%s, TIMESTAMP '%s', TIMESTAMP '%s', %s, %s, '%s', %s), "
            sql_query = sql_query[:-2] + " returning id;"
            cursor.execute(sql_query)
            adv_act_objs = [item[0] for item in cursor.fetchall()]
        else:
            adv_act_objs = []

        # CREATE ALL FDS
        fses_count_map = {}
        if web_activities:
            sql_query="insert into core_formdata (user_id, visitor_id, created_date, modified_date) values "
            for i, hist in enumerate(web_activities):
                fs = get_all_form_sections(hist['extra'])
                fses_count_map[i] = len(fs)
                sql_query += "(%s, %s, TIMESTAMP '%s', TIMESTAMP '%s'), " % (user.id if user else 'NULL', tracker.id if tracker else 'NULL', datetime.datetime.strftime(datetime.datetime.now(), "%m-%d-%Y %H:%M:%S"), datetime.datetime.strftime(datetime.datetime.now(), "%m-%d-%Y %H:%M:%S"))
                form_sections.extend(fs)
            sql_query = sql_query[:-2] + " returning id;"
            cursor.execute(sql_query)
            fd_ids = [item[0] for item in cursor.fetchall()]
        else:
            fd_ids = []

        # CREATING ALL FSES
        if form_sections:
            sql_query = "insert into core_formsection (form, data, created_date, modified_date) values "
            for i, item in enumerate(form_sections):
                sql_query += "('%s', '%s', TIMESTAMP '%s', TIMESTAMP '%s'), " % (item[0], item[1], datetime.datetime.strftime(datetime.datetime.now(), "%m-%d-%Y %H:%M:%S"), datetime.datetime.strftime(datetime.datetime.now(), "%m-%d-%Y %H:%M:%S"))
            sql_query = sql_query[:-2] + " returning id;"
            cursor.execute(sql_query)
            fs_ids = [item[0] for item in cursor.fetchall()]
        else:
            fs_ids = []


        fd_fs_map = {}
        count = 0
        for i in range(len(fd_ids)):
            count += fses_count_map[i]
            fd_fs_map[fd_ids[i]] = fs_ids[count:count+fses_count_map[i]]

        if fd_fs_map:
            sql_query = "insert into core_formdata_sections (formdata_id, formsection_id) values "
            for fd_id, v in fd_fs_map.items():
                for fs_id in v:
                    sql_query += "(%s, %s), " % (fd_id, fs_id)
            sql_query = sql_query[:-2] + ';'
            cursor.execute(sql_query)

        sql_query = """insert into core_activity (activity_id, mid_id, requirement_id, tracker_id, fd_id, is_processed,\
            extra, gclid, term, match_type, target, placement, label, feed_item, landing_url, url, page_id,\
            form_name, form_position, product, form_variant, activity_type, browser, os, device, device_model,\
            ip_address, state, city, isp, referer, request_id, created, updated_on) values """

        for i, adv_act in enumerate(web_activities):
            ad = web_activities[i]
            fd = fd_ids[i]
            extra = json.dumps(ad['extra'])
            extra = extra.replace("'", '"')

            sql_query += """('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', '%s', '%s', '%s', '%s', TIMESTAMP '%s', TIMESTAMP '%s'), """% ( str(uuid.uuid4()), get_mid_from_cache(ad['extra']), requirement.id, tracker.id, fd_ids[i], 'false', extra, ad.get('gclid',''), ad.get('term', ''), ad.get('match_type', ''), ad.get('target', ''), ad.get('placement', ''), ad.get('label', ''), ad.get('feed_item', ''), ad.get('landing_page_url', ''), ad.get('url', ''), ad.get('page_id', ''), ad.get('form_name', ''), ad.get('form_position', ''), ad.get('product', ''), ad.get('form_variant', ''), ad.get('activity_type', ''), ad.get('browser', ''), ad.get('os', ''), ad.get('device', ''), ad.get('device_model', ''), ad.get('request_ip') or 'NULL', ad.get('state', ''), ad.get('city', ''), ad.get('isp', ''), ad.get('referer', ''), ad.get('request_id', ''), get_sql_datetime(ad['created']), get_sql_datetime(ad['created']))

        sql_query = sql_query[:-2] + " returning id;"
        cursor.execute(sql_query)

        if 'autoassign@crm.coverfox.com' in requirement.owner.user.emails.values_list(
            'email', flat=True) and row[3] is not 'CONCLUDED':
            task=crm_models.Task.objects.create(
                category=crm_models.TaskCategory.objects.get(
                    name='CALLBACK_LATER'),
                assigned_to=requirement.owner,
                scheduled_time=get_datetime(row[10])
            )
            requirement.tasks.add(task)
        print requirement.id
    except Exception as e:
        print row, e
        log_file.write(str(row))
        log_file.write('\n')
        log_file.write(str(e))
        log_file.write('\n')


log_file.close()
print "Done for the Day"
