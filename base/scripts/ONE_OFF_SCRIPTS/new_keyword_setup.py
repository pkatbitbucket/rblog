import sys
import time
import datetime
import os
from optparse import OptionParser

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from django.db import models
import csv
import xlwt
from wiki.models import *
import pprint
import re
import utils
from xl2python import Xl2Python
from campaign.models import *


upload_field_map = [
    ('campaign', 'trimmed'),
    ('ad_group', 'trimmed'),
    ('keyword', 'trimmed'),
    ('url', 'trimmed')
]

match_field_map = [
    ('campaign', 'trimmed'),
    ('ad_group', 'trimmed'),
    ('keyword', 'trimmed')
]


keymap = {
    'health-insurance-in-india': 'top-health-insurance-in-india-1',
    'health-insurance-products-in-india': 'compare-health-insurance-india-1',
    'health-insurance-market-in-india': 'health-insurance-marketplace-india-1',
    'health-insurance-in-india-pdf': 'top-health-insurance-in-india-1',
    'senior-citizen-health-insurance-india': 'comparison-of-senior-citizen-health-insurance-1',
    'medical-health-insurance-india': 'medical-health-insurance-1',
    'united-india-health-insurance': 'medical-health-insurance-1',
    'compare-health-insurance-in-india': 'compare-health-insurance-india-1',
    'compare-health-insurance-plans-india': 'compare-health-insurance-india-1',
    'bank-of-india-health-insurance': 'compare-health-insurance-india-1',
    'buy-health-insurance-online-india': 'buy-health-insurance-1',
    'health-insurance-premium-calculator-india': 'health-insurance-premium-calculator-2',
    'good-health-insurance-in-india': 'good-health-insurance-plan-1',
    'united-india-insurance-health-insurance': 'compare-health-insurance-1',
    'health-insurance-senior-citizens-india': 'comparison-of-senior-citizen-health-insurance-1',
    'health-insurance-review-india': 'health-insurance-reviews-and-ratings-india-1',
    'health-insurance-premium-comparison-india': 'compare-health-insurance-india-1',
    'health-insurance-comparison-chart-india': 'compare-health-insurance-india-1',
    'sbi-health-insurance-in-india': 'compare-health-insurance-india-1',
    'health-insurance-quotes-india': 'get-health-insurance-quotes-1',
    'health-insurance-reviews-india': 'health-insurance-reviews-and-ratings-india-1',
    'health-insurance-comparison-spreadsheet-india': 'compare-health-insurance-india-1',
    'health-insurance-plans-in-india-comparison': 'compare-health-insurance-india-1',
    'best-health-insurance-reviews-india': 'health-insurance-reviews-and-ratings-india-1',
    'health-insurance-penetration-in-india': 'compare-health-insurance-india-1'
}


class Uploader(Xl2Python):
    pass


################## CHANGE DATA #########################
xfile = open('keyword_list.xls', 'r')
########################################################


xlm = Uploader(upload_field_map, xfile)
data = None
if xlm.is_valid():
    data = xlm.cleaned_data
else:
    print xlm.errors

# print data

old_keyword_list = [k.slug for k in Keyword.objects.all()]
old_adgroup_list = [a.title for a in AdGroup.objects.all()]

camp_list = []

for datum in data:
    # FOR KEYWORDS
    utm_keyword = None
    utm_ad = None
    utm_camp = None
    # keyword = utils.slugify(datum['keyword'].replace('+', '').replace('[', '').replace(']', ''))
    keyword = utils.slugify(
        re.sub(r'[ +\[\]]+', ' ', datum['keyword']).strip())
    if '%s-1' % keyword in old_keyword_list:
        old_key = Keyword.objects.get(slug='%s-1' % keyword)
        if not UtmKeyword.objects.filter(slug=keyword):
            utm_keyword = UtmKeyword(
                title=old_key.title,
                slug=keyword,
                pattern=datum['keyword'],
                extra=old_key.extra,
                insurer=old_key.ad_group.insurer
            )
            utm_keyword.save()
        else:
            utm_keyword = UtmKeyword.objects.get(slug=keyword)
            utm_keyword.pattern = datum['keyword']
            utm_keyword.save()
    elif datum['ad_group'].replace('_', ' ').replace('-and-', ' & ') in old_adgroup_list:
        old_ad = AdGroup.objects.get(
            title=datum['ad_group'].replace('_', ' ').replace('-and-', ' & '))
        ad_group = utils.slugify(datum['ad_group'])
        if not UtmKeyword.objects.filter(slug=keyword):
            utm_keyword = UtmKeyword(
                title=keyword,
                slug=keyword,
                pattern=datum['keyword'],
                extra=old_ad.extra,
                insurer=old_ad.insurer
            )
            utm_keyword.save()
        else:
            utm_keyword = UtmKeyword.objects.get(slug=keyword)
            utm_keyword.pattern = datum['keyword']
            utm_keyword.save()
    else:
        if keyword in keymap.keys():
            old_key = Keyword.objects.get(slug=keymap[keyword])
            if not UtmKeyword.objects.filter(slug=keyword):
                utm_keyword = UtmKeyword(
                    title=old_key.title,
                    slug=keyword,
                    pattern=datum['keyword'],
                    extra=old_key.extra,
                    insurer=old_key.ad_group.insurer
                )
                utm_keyword.save()
            else:
                utm_keyword = UtmKeyword.objects.get(slug=keyword)
                utm_keyword.pattern = datum['keyword']
                utm_keyword.save()
        else:
            print ">>>>>>>", keyword

    # For Ad Groups
    if datum['ad_group'].replace('_', ' ').replace('-and-', ' & ') in old_adgroup_list:
        old_ad = AdGroup.objects.get(
            title=datum['ad_group'].replace('_', ' ').replace('-and-', ' & '))
        ad_group = utils.slugify(datum['ad_group'])
        if not UtmAdGroup.objects.filter(slug=ad_group):
            utm_ad = UtmAdGroup(
                title=old_ad.title,
                slug=ad_group,
                extra=old_ad.extra,
                insurer=old_ad.insurer
            )
            utm_ad.save()
        else:
            utm_ad = UtmAdGroup.objects.get(slug=ad_group)
    else:
        if not UtmAdGroup.objects.filter(slug=ad_group):
            utm_ad = UtmAdGroup(
                title=datum['ad_group'],
                slug=ad_group
            )
            utm_ad.save()
        else:
            utm_ad = UtmAdGroup.objects.get(slug=ad_group)

    # For Campaigns
    campaign = utils.slugify(datum['campaign'])
    if not UtmCampaign.objects.filter(slug=campaign):
        utm_camp = UtmCampaign(
            title=campaign,
            slug=campaign,
            insurer=utm_ad.insurer
        )
        utm_camp.save()
    else:
        utm_camp = UtmCampaign.objects.get(slug=campaign)

    if utm_ad and utm_keyword and utm_camp:
        if not UtmMapping.objects.filter(keyword=utm_keyword, ad_group=utm_ad, campaign=utm_camp):
            utm_map = UtmMapping(
                keyword=utm_keyword,
                ad_group=utm_ad,
                campaign=utm_camp
            )
            utm_map.save()
    else:
        print '#############', utm_keyword, utm_ad, utm_camp
