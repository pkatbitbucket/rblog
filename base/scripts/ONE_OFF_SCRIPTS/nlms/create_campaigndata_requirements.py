import os
import csv
import json
import django
import datetime
from django.utils import timezone
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from django.db import connection

from core import models as core_models
from crm import models as crm_models
from statemachine import models as sm_models


filepath = "/tmp/"
log_file = open('/tmp/log_file.log', 'w')
no_customer_found = open('/tmp/no_customer_found.log', 'w')
no_policy_or_no_requirement = open('/tmp/no_policy_or_no_requirement.log', 'w')
transaction_id_notfound = open('/tmp/transaction_id_notfound.log', 'w')
cursor = connection.cursor()
CLOSED_resolved, _ = sm_models.State.objects.get_or_create(name='CLOSED_resolved')
CLOSED_UNRESOLVED_ID, _ = sm_models.State.objects.get_or_create(name='CLOSED_unresolved')
QUALIFIED_LTB_ID, _ = sm_models.State.objects.get_or_create(name='QUALIFIED_likely_to_buy')
FRESH_NEW_ID, _ = sm_models.State.objects.get_or_create(name='FRESH_new')
DUMMY_FRESH_ID, _ = sm_models.State.objects.get_or_create(name='FRESH_ringing')
CALLBACK_LATER, _ = crm_models.TaskCategory.objects.get_or_create(name='CALLBACK_LATER')


RK_CACHE = {}
HEALTH_OFFLINE_RK, _ = core_models.RequirementKind.objects.get_or_create(
    product='health',
    kind=core_models.RequirementKind.NEW_POLICY,
    sub_kind='MEDICAL_REQUIRED',
)
CAR_OFFLINE_RK, _ = core_models.RequirementKind.objects.get_or_create(
    product='car',
    kind=core_models.RequirementKind.NEW_POLICY,
    sub_kind='INSPECTION_REQUIRED',
)


def cache_requirement_kind():
    products = {
        'life': '',
        'travel': '',
        'term': '',
        'health': '',
        'car': '',
        'bike': '',
        'home': '',
    }
    for prod, sub_kind in products.items():
        rk, _ = core_models.RequirementKind.objects.get_or_create(
            product=prod,
            kind=core_models.RequirementKind.NEW_POLICY,
            sub_kind=sub_kind,
        )
        RK_CACHE[prod] = rk.id


def get_rk(campaign):
    if 'offline' in campaign:
        if campaign.startswith('health'):
            return CAR_OFFLINE_RK.id
        else:
            return HEALTH_OFFLINE_RK.id
    else:
        if campaign == 'one-health':
            product = 'health'
        elif campaign.startswith('motor-bike'):
            product = 'bike'
        elif campaign.startswith('motor') or campaign.startswith('free'):
            product = 'car'
        else:
            product = campaign.split('-')[0]
        return RK_CACHE[product]


def create_requirements():
    c = 0
    campaigndatas = {}
    cursor.execute("select id, crm_advisor_id from olms.auth_user;")
    lms_user_advisor_id_map = {int(item[0]): item[1] for item in cursor.fetchall()}

    cursor.execute("select id, crm_queue_id from olms.core_campaign;")
    lms_campaign_queue_id_map = {int(item[0]): item[1] for item in cursor.fetchall()}

    cursor.execute("select id, name from olms.core_campaign;")
    lms_campaigns_id_name_map = {int(item[0]): item[1] for item in cursor.fetchall()}

    cursor.execute("SELECT id, core_tracker_id from olms.core_customer;")
    customer_tracker_map = {int(item[0]): item[1] for item in cursor.fetchall()}

    cursor.execute("SELECT id, core_user_id from olms.core_customer;")
    customer_user_map = {int(item[0]): item[1] for item in cursor.fetchall()}

    cursor.execute("SELECT id, core_requirement_id from olms.core_transaction where core_requirement_id is not null and is_success='t';")
    already_created_reqs_transactions = {int(item[0]): int(item[1]) for item in cursor.fetchall()}

    cursor.execute("SELECT campaigndata, id from core_requirement where not current_state_id is null and not current_state_id in (1, 2) and not campaigndata is null")
    open_requirements = {item[0]: item[1] for item in cursor.fetchall()}

    all_sold_today_cdids = set()

    cursor.execute("select * from olms.core_campaigndata where status!='CONCLUDED' or next_call_time>'2016-02-03 12:00:00.352509' or last_call_time>'2016-02-03 12:00:00.352509'")
    campaigndatas = {int(item[0]): list(item) for item in cursor.fetchall()}

    print "All campaigndatas taken into memory"

    c = 0
    lms_transaction_data = {}
    trs_closed_campaigndatas = []

    ccount = 0
    tcount = 0
    created_req_count = 0
    with open(os.path.join(filepath, 'core_transaction.csv'), 'rb') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter='~')
        for row in csv_reader:
            if c == 0:
                c += 1
                continue
            if row:
                lms_transaction_data[row[0]] = {
                    'cdid': row[1],
                    'payment_date': row[2],
                    'assigned_to': row[3],
                    'type': row[4],
                    'id': row[5],
                    'core_requirement_id': row[6],
                }
                ccount += 1
                if not row[6]:
                    continue
                if campaigndatas[str(row[1])][3] == 'CONCLUDED':
                    trs_closed_campaigndatas.append(row[1])
            tcount += 1

    print "All transaction data taken into memory"

    total = len(campaigndatas)
    count = 0

    processing_count = 0
    all_sold_today_cdids = list(all_sold_today_cdids)
    for cdid, campaigndata in campaigndatas.items():
        owner_id = campaigndata[4]
        cd_data = json.loads(campaigndata[1])
        count += 1
        try:
            count += 1
            if cdid in trs_closed_campaigndatas:
                continue
            if int(campaigndata[6]) in [102, 124, 125, 126]:
                continue

            user = customer_user_map.get(int(campaigndata[7]))
            tracker = customer_tracker_map.get(int(campaigndata[7]))
            if not (user and tracker):
                print 'three', "User and Tracker both not found for campaigndata %s" % cdid, campaigndata[7]
                no_customer_found.write('Campaigndata customer not found:' + str(cdid))
                no_customer_found.write('\n')
                continue
            requirement = core_models.Requirement.objects.exclude(current_state__name__in=['CLOSED_resolved', 'CLOSED_unresolved']).filter(campaigndata=cdid).last()
            if campaigndata[3] == 'CONCLUDED':
                if not open_requirements.get(int(cdid)):
                    continue
                else:
                    if requirement:
                        tasks = requirement.tasks.all()
                        requirement.next_crm_task = None
                        requirement.extra = cd_data
                        requirement.current_state = CLOSED_UNRESOLVED_ID
                        requirement.save()
                        tasks.delete()
                        processing_count += 1
                    continue
            else:
                processing_count += 1
                if open_requirements.get(int(cdid)):
                    tasks = requirement.tasks.all()
                    requirement.next_crm_task = None
                    requirement.save()
                    tasks.delete()
                    if int(owner_id) == 31:
                        assigned_to = None
                    else:
                        assigned_to = requirement.owner
                    if campaigndata[10]:
                        scheduled_time = campaigndata[10]
                        task = crm_models.Task.objects.create(
                            category=CALLBACK_LATER,
                            assigned_to=assigned_to,
                            scheduled_time=scheduled_time,
                            queue_id=lms_campaign_queue_id_map[int(campaigndata[6])]
                        )
                        requirement.tasks.add(task)
                        requirement.next_crm_task = task
                    requirement.extra = cd_data
                    requirement.save()
                else:
                    kind_id = get_rk(lms_campaigns_id_name_map[int(campaigndata[6])])
                    if campaigndata[3] == 'IN_PROCESS':
                        current_state = QUALIFIED_LTB_ID
                    elif campaigndata[3] == 'FRESH':
                        current_state = FRESH_NEW_ID
                    elif campaigndata[3] == 'DUMMY_FRESH':
                        current_state = DUMMY_FRESH_ID
                    else:
                        current_state = CLOSED_UNRESOLVED_ID
                    requirement = core_models.Requirement.objects.create(
                        user_id=user,
                        visitor_id=tracker,
                        kind_id=kind_id,
                        current_state=current_state,
                        owner_id=lms_user_advisor_id_map[int(owner_id)],
                        remark=cd_data.get('notes', ''),
                        extra=cd_data,
                        campaigndata=int(campaigndata[0]),
                        fixed=False,
                    )

                    if int(owner_id) == 31:
                        assigned_to = None
                    else:
                        assigned_to = requirement.owner
                    if campaigndata[10]:
                        scheduled_time = campaigndata[10]
                        task = crm_models.Task.objects.create(
                            category=CALLBACK_LATER,
                            assigned_to=assigned_to,
                            scheduled_time=scheduled_time,
                            queue_id=lms_campaign_queue_id_map[int(campaigndata[6])]
                        )
                        requirement.tasks.add(task)
                        requirement.next_crm_task = task
                    requirement.save()
                    created_req_count += 1
            if count % 100 == 0:
                print "progress", created_req_count, processing_count, count, total, cdid, datetime.datetime.now()
        except Exception as e:
            print "------------------------------", e
            log_file.write(str(campaigndata))
            log_file.write('\n')
            log_file.write(str(e))
            log_file.write('\n')

    print 'Requirement for campaigndatas', count

def main():
    cache_requirement_kind()
    create_requirements()

if __name__ == "__main__":
    main()
