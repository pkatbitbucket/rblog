import os
import json
import django
import datetime
from django.utils import timezone
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from django.db import connection
from core import models as core_models
from statemachine import models as sm_models


f = open('/tmp/created_campaigndatas.csv', 'w+')
cursor = connection.cursor()
CLOSED_resolved, _ = sm_models.State.objects.get_or_create(name='CLOSED_resolved')
CLOSED_UNRESOLVED_ID, _ = sm_models.State.objects.get_or_create(name='CLOSED_unresolved')
QUALIFIED_LTB_ID, _ = sm_models.State.objects.get_or_create(name='QUALIFIED_likelytobuy')
FRESH_NEW_ID, _ = sm_models.State.objects.get_or_create(name='FRESH_new')
DUMMY_FRESH_ID, _ = sm_models.State.objects.get_or_create(name='FRESH_ringing')


RK_CACHE = {}
HEALTH_OFFLINE_RK, _ = core_models.RequirementKind.objects.get_or_create(
    product='health',
    kind=core_models.RequirementKind.NEW_POLICY,
    sub_kind='MEDICAL_REQUIRED',
)
CAR_OFFLINE_RK, _ = core_models.RequirementKind.objects.get_or_create(
    product='car',
    kind=core_models.RequirementKind.NEW_POLICY,
    sub_kind='INSPECTION_REQUIRED',
)

cursor.execute("select id, name from olms.core_campaign;")
lms_campaigns_id_name_map = {int(item[0]): item[1] for item in cursor.fetchall()}

cursor.execute("select id, crm_advisor_id from olms.auth_user;")
lms_user_advisor_id_map = {int(item[0]): item[1] for item in cursor.fetchall()}


def cache_requirement_kind():
    products = {
        'life': '',
        'travel': '',
        'term': '',
        'health': '',
        'car': '',
        'bike': '',
        'home': '',
    }
    for prod, sub_kind in products.items():
        rk, _ = core_models.RequirementKind.objects.get_or_create(
            product=prod,
            kind=core_models.RequirementKind.NEW_POLICY,
            sub_kind=sub_kind,
        )
        RK_CACHE[prod] = rk.id


def get_rk(campaign):
    if 'offline' in campaign:
        if campaign.startswith('health'):
            return CAR_OFFLINE_RK.id
        else:
            return HEALTH_OFFLINE_RK.id
    else:
        if campaign == 'one-health':
            product = 'health'
        elif campaign.startswith('motor-bike'):
            product = 'bike'
        elif campaign.startswith('motor') or campaign.startswith('free'):
            product = 'car'
        else:
            product = campaign.split('-')[0]
        return RK_CACHE[product]


def dictfetchone(cursor):
    columns = (x.name for x in cursor.description)
    result = cursor.fetchone()
    if not result:
        return None

    result = dict(zip(columns, list(result)))
    return result

def main():
    # for campaign in all
    #    create requirement
    #    task would be created by fix
    #    owner = campaign.assigned_to.user
    #    extra = 
    #    created

    cursor.execute("select * from olms.core_campaigndata where next_call_time is not null and status!='CONCLUDED' order by id")

    count = 0
    while True:
        campaigndata = dictfetchone(cursor)
        if not campaigndata:
            break

        campaign_id = int(campaigndata['campaign_id'])
        if campaign_id in [102, 124, 125, 126]:
            continue

        status = campaigndata['status']
        if status == 'FRESH':
            current_state_id = FRESH_NEW_ID
        elif status == 'DUMMY_FRESH':
            current_state_id = DUMMY_FRESH_ID
        elif status == 'CONCLUDED':
            current_state_id = CLOSED_UNRESOLVED_ID
        else:
            current_state_id = QUALIFIED_LTB_ID

        cd_data = json.loads(campaigndata['data'])
        owner_id=lms_user_advisor_id_map[int(campaigndata['assigned_to_id'])]
        if int(owner_id) == 31:
            owner_id = None

        req = core_models.Requirement.objects.create(
            extra=cd_data,
            kind_id = get_rk(lms_campaigns_id_name_map[campaign_id]),
            current_state=current_state_id,
            campaigndata=int(campaigndata['id']),
            remark=cd_data.get('notes', ''),
            fixed=False,
        )
        if count % 100 == 0:
	    print count, req.id, datetime.datetime.now()


if __name__ == "__main__":
    cache_requirement_kind()
    main()
