import os
import csv
import json
import django
import datetime
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from django.db import connection

from core import models as core_models
from crm import models as crm_models
from statemachine import models as sm_models
from motor_product.models import Transaction as MotorTransaction
from travel_product.models import Transaction as TravelTransaction
from health_product.models import Transaction as HealthTransaction


filepath = "/tmp/"
log_file = open('/tmp/log_file.log', 'w')
no_customer_found = open('/tmp/no_customer_found.log', 'w')
no_policy_or_no_requirement = open('/tmp/no_policy_or_no_requirement.log', 'w')
transaction_id_notfound = open('/tmp/transaction_id_notfound.log', 'w')
cursor = connection.cursor()
CLOSED_resolved, _ = sm_models.State.objects.get_or_create(name='CLOSED_resolved')
CLOSED_UNRESOLVED_ID, _ = sm_models.State.objects.get_or_create(name='CLOSED_unresolved')
QUALIFIED_LTB_ID, _ = sm_models.State.objects.get_or_create(name='QUALIFIED_likely_to_buy')
FRESH_NEW_ID, _ = sm_models.State.objects.get_or_create(name='FRESH_new')
DUMMY_FRESH_ID, _ = sm_models.State.objects.get_or_create(name='FRESH_ringing')
CALLBACK_LATER, _ = crm_models.TaskCategory.objects.get_or_create(name='CALLBACK_LATER')


RK_CACHE = {}
HEALTH_OFFLINE_RK, _ = core_models.RequirementKind.objects.get_or_create(
    product='health',
    kind=core_models.RequirementKind.NEW_POLICY,
    sub_kind='MEDICAL_REQUIRED',
)
CAR_OFFLINE_RK, _ = core_models.RequirementKind.objects.get_or_create(
    product='car',
    kind=core_models.RequirementKind.NEW_POLICY,
    sub_kind='INSPECTION_REQUIRED',
)


def cache_requirement_kind():
    products = {
        'life': '',
        'travel': '',
        'term': '',
        'health': '',
        'car': '',
        'bike': '',
        'home': '',
    }
    for prod, sub_kind in products.items():
        rk, _ = core_models.RequirementKind.objects.get_or_create(
            product=prod,
            kind=core_models.RequirementKind.NEW_POLICY,
            sub_kind=sub_kind,
        )
        RK_CACHE[prod] = rk.id


def get_rk(campaign):
    if 'offline' in campaign:
        if campaign.startswith('health'):
            return CAR_OFFLINE_RK.id
        else:
            return HEALTH_OFFLINE_RK.id
    else:
        if campaign == 'one-health':
            product = 'health'
        elif campaign.startswith('motor-bike'):
            product = 'bike'
        elif campaign.startswith('motor') or campaign.startswith('free'):
            product = 'car'
        else:
            product = campaign.split('-')[0]
        return RK_CACHE[product]


def create_requirements():
    product_transactions = {}
    policies_id_map = {p.id: p for p in core_models.Policy.objects.all()}
    policies_id_map.update({'': None, None: None})
    motor_trs_id_map = {item[0]: item[1] for item in MotorTransaction.objects.filter(status__in=['MANUAL COMPLETED', 'COMPLETED']).values_list('transaction_id', 'id')}
    travel_trs_id_map = {item[0]: item[1] for item in TravelTransaction.objects.filter(status__in=['MANUAL COMPLETED', 'COMPLETED']).values_list('transaction_id', 'id')}
    health_trs_id_map = {item[0]: item[1] for item in HealthTransaction.objects.filter(status__in=['MANUAL COMPLETED', 'COMPLETED']).values_list('transaction_id', 'id')}

    health_trs = HealthTransaction.objects.filter(status__in=['MANUAL COMPLETED', 'COMPLETED']).values_list('transaction_id', 'policy')
    for item in health_trs:
        product_transactions[item[0]] = {
            'product': 'health',
            'transaction': health_trs_id_map[item[0]],
            'policy': item[1] if item[1] else None,
        }

    motor_trs = MotorTransaction.objects.filter(status__in=['MANUAL COMPLETED', 'COMPLETED']).values_list('transaction_id', 'policy')
    for item in motor_trs:
        product_transactions[item[0]] = {
            'product': 'motor',
            'transaction': motor_trs_id_map[item[0]],
            'policy': item[1] if item[1] else None,
        }

    travel_trs = TravelTransaction.objects.filter(status__in=['MANUAL COMPLETED', 'COMPLETED']).values_list('transaction_id', 'policy')
    for item in travel_trs:
        product_transactions[item[0]] = {
            'product': 'travel',
            'transaction': travel_trs_id_map[item[0]],
            'policy': item[1] if item[1] else None,
        }

    c = 0
    campaigndatas = {}
    cursor.execute("select id, crm_advisor_id from olms.auth_user;")
    lms_user_advisor_id_map = {int(item[0]): item[1] for item in cursor.fetchall()}

    cursor.execute("select id, name from olms.core_campaign;")
    lms_campaigns_id_name_map = {int(item[0]): item[1] for item in cursor.fetchall()}

    cursor.execute("SELECT id, core_tracker_id from olms.core_customer;")
    customer_tracker_map = {int(item[0]): item[1] for item in cursor.fetchall()}

    cursor.execute("SELECT id, core_user_id from olms.core_customer;")
    customer_user_map = {int(item[0]): item[1] for item in cursor.fetchall()}

    cursor.execute("SELECT id, core_requirement_id from olms.core_transaction where core_requirement_id is not null and is_success='t';")
    already_created_reqs_transactions = {int(item[0]): int(item[1]) for item in cursor.fetchall()}

    all_sold_today_cdids = set()

    with open(os.path.join(filepath, 'core_campaigndata.csv'), 'rb') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter='~')
        for row in csv_reader:
            if c == 0:
                c += 1
                continue
            campaigndatas[row[0]] = row

    print "All campaigndatas taken into memory"

    c = 0
    lms_transaction_data = {}
    trs_closed_campaigndatas = []
    ccount = 0
    tcount = 0
    with open(os.path.join(filepath, 'core_transaction.csv'), 'rb') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter='~')
        for row in csv_reader:
            if c == 0:
                c += 1
                continue
            if row and not int(row[5]) in already_created_reqs_transactions:
                lms_transaction_data[row[0]] = {
                    'cdid': row[1],
                    'payment_date': row[2],
                    'assigned_to': row[3],
                    'type': row[4],
                    'id': row[5],
                    'core_requirement_id': row[6],
                }
                ccount += 1
                if not row[6]:
                    continue
                if row[2] > '2016-02-03 12:00:00.352509':
                    all_sold_today_cdids.add(row[1])
                if campaigndatas[str(row[1])][3] == 'CONCLUDED':
                    trs_closed_campaigndatas.append(row[1])
            tcount += 1

    print "All transaction data taken into memory"

    total = len(lms_transaction_data)
    count = 0
    processing_count = 0
    created_req_count = 0

    for trid, value in lms_transaction_data.items():
        try:
            if not product_transactions.get(trid):
                if lms_transaction_data[trid]['type'] == 'ONLINE_PAYMENT':
                    print "Transaction id should have existed in product: %s" % trid
                    transaction_id_notfound.write(trid)
                    continue

            cdid = value['cdid']
            campaigndata = campaigndatas[cdid]
            count += 1
            if int(campaigndata[6]) == [102, 124, 125, 126]:
                continue
            if not (customer_user_map.get(int(campaigndata[7]))
                    or customer_tracker_map.get(int(campaigndata[7]))):
                print 'two', "User and Tracker both not found for campaigndata %s" % cdid
                no_customer_found.write("Transaction Id Not Found:" + str(trid))
                no_customer_found.write('\n')
                continue

            prod_policy = None
            if product_transactions.get(trid) and product_transactions[trid].get('policy'):
                prod_policy = policies_id_map[
                    product_transactions[trid]['policy']]

            tracker = customer_tracker_map[int(campaigndata[7])]
            user = customer_user_map[int(campaigndata[7])]

            cd_data = json.loads(campaigndata[1])

            owner_id = str(value['assigned_to']) or campaigndata[4]
            if prod_policy and prod_policy.requirement_sold:
                requirement = prod_policy.requirement_sold
                if not requirement.extra:
                    requirement.extra = {}
                requirement.extra.update(cd_data)
                requirement.campaigndata = int(cdid)
                if owner_id:
                    requirement.owner_id = lms_user_advisor_id_map[int(owner_id)]
            else:
                if product_transactions.get(trid):
                    no_policy_or_no_requirement.write("TRID: %s" % trid)
                    continue

            # value['payment_date'] > '2016-02-03 12:00:00.352509':
            created_req_count += 1
            requirement = core_models.Requirement.objects.create(
                user_id=user,
                tracker_id=tracker,
                kind_id=get_rk(lms_campaigns_id_name_map[int(campaigndata[6])]),
                current_state=CLOSED_resolved,
                owner_id=lms_user_advisor_id_map[int(owner_id)],
                remark=cd_data.get('notes', ''),
                extra=cd_data,
                campaigndata=int(cdid),
                fixed=False,
            )
            processing_count += 1
            requirement.created_date = campaigndata[5]
            requirement.save()
            sql_query = "update olms.core_transaction set core_requirement_id=%s where id=%s" % (requirement.id, value['id'])
            cursor.execute(sql_query)

            if count % 100 == 0:
                print "progress", created_req_count, processing_count, count, total, cdid, requirement.id, datetime.datetime.now()

        except Exception as e:
            print row, e
            log_file.write(str(row))
            log_file.write('\n')
            log_file.write(str(e))
            log_file.write('\n')
    print 'transactions are done'


def main():
    cache_requirement_kind()
    create_requirements()

if __name__ == "__main__":
    main()
