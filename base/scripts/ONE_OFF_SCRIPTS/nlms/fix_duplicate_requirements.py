import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from django.db import connection
from core.models import Requirement
from django.db.models import Count


cursor = connection.cursor()
reqs = Requirement.objects.filter(current_state__isnull=False, campaigndata__isnull=False, policy__isnull=True).exclude(current_state_id=17).values('campaigndata', 'id')

cursor.execute("select count(*) from olms.core_campaigndata where status!='CONCLUDED' and next_call_time is not null")

cdidlist = []
reqids_to_delete = []
import pdb; pdb.set_trace()


for r in reqs:
    if r['campaigndata'] in cdidlist:
        reqids_to_delete.append(r['id'])
    cdidlist.append(r['campaigndata'])

req = Requirement.objects.filter(id__in=reqids_to_delete)

import pdb; pdb.set_trace()
