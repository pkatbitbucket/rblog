import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from django.db import connection

from core import models as core_models

cursor = connection.cursor()

import sys
total_scripts = int(sys.argv[1])
current_script = int(sys.argv[2])

cursor.execute("select id, core_user_id from olms.core_customer where core_user_id is not null")
customer_userid_map = {item[0]: item[1] for item in cursor.fetchall()}

cursor.execute("select id, core_tracker_id from olms.core_customer where core_tracker_id is not null")
customer_trackerid_map = {item[0]: item[1] for item in cursor.fetchall()}


def dictfetchone(cursor):
    columns = (x.name for x in cursor.description)
    result = cursor.fetchone()
    if not result:
        return None

    result = dict(zip(columns, list(result)))
    return result


cursor.execute("select email, alt_emails, customer_id from olms.core_emailcustomer order by id desc")
count = 0
while True:
    count += 1
    ecustomer = dictfetchone(cursor)
    if not ecustomer:
        break
    customer_id = ecustomer['customer_id']
    if not customer_id:
        continue

    cid = int(customer_id)
    if cid % total_scripts != current_script:
        print current_script, cid, total_scripts
        continue

    emails_list = [ecustomer['email'].strip()]
    if ecustomer['alt_emails']:
        alt_emails = ecustomer['alt_emails']# split(',')
        if alt_emails:
            emails_list.extend(alt_emails.strip().split(","))

    for email in emails_list:
        email = email.strip()
        ev = core_models.Email.objects.filter(email=email, is_verified=True)
        if ev:
            is_verified = False
        else:
            is_verified = True

        user_id = customer_userid_map.get(customer_id)
        visitor_id = customer_trackerid_map.get(customer_id)
        if user_id:
            em = core_models.Email.objects.filter(
                email=email,
                user_id=user_id,
            ).first()
        elif visitor_id:
            em = core_models.Email.objects.filter(
                email=email,
                visitor_id=visitor_id,
            ).first()
        else:
            print(ecustomer, "no user or visitor")
            continue
        if not em:
            if user_id: owner = {"user_id": user_id}
            elif user_id: owner = {"visitor_id": visitor_id}
            em = core_models.Email.objects.create(is_verified=is_verified, email=email, **owner)
            print customer_id, ">>>>>>>", em.id
            continue

        em.is_verified = em.is_verified or is_verified
        em.save()
        if count % 100 == 0:
            print count, customer_id, ">>>>>>>", em.id
