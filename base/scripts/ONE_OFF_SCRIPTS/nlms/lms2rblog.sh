#!/bin/sh

set -e
set -x

HOST=127.0.0.1
USER=root
LMS_DATABASE_NAME=cflms

COVERFOX_HOST=127.0.0.1
COVERFOX_USER=root
COVERFOX_DATABASE=coverfox

# create schema olms in rblog db
echo "create schema olms; set search_path to olms,public;" > /tmp/cflms.sql

# dump lms db
pg_dump -h $HOST -U $USER -d $LMS_DATABASE_NAME >> /tmp/cflms.sql

grep -v "SET search_path" /tmp/cflms.sql > /tmp/cflms_2.sql

# load lms.dump into rblog.olms schema
psql -h $COVERFOX_HOST -d $COVERFOX_DATABASE -U $COVERFOX_USER --password < /tmp/cflms_2.sql
