#!/bin/sh

set -e
set -x

export PYTHONPATH=.

python scripts/ONE_OFF_SCRIPTS/nlms/trackers.py
