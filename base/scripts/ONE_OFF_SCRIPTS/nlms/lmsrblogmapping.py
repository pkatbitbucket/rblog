import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from django.db import connection

cursor = connection.cursor()


def add_foreign_keys():

    # ###### SQL ########
    # olms.User will have FK to crm.Advisor
    cursor.execute("alter table olms.auth_user add COLUMN crm_advisor_id int;")
    cursor.execute("ALTER TABLE olms.auth_user ADD CONSTRAINT crm_advisor_id_fk FOREIGN KEY (crm_advisor_id) REFERENCES public.crm_advisor;")
    print "olms.User FK created to crm.Advisor"

    # olms.Campaign will have FK to crm.Queue
    cursor.execute("alter table olms.core_campaign add COLUMN crm_queue_id int;")
    cursor.execute("ALTER TABLE olms.core_campaign ADD CONSTRAINT crm_queue_id_fk FOREIGN KEY (crm_queue_id) REFERENCES public.crm_queue;")
    print "olms.Campaign FK created to crm.Queue"

    # olms.history will have FK to core.Activity named core_activity
    cursor.execute("alter table olms.core_history add COLUMN core_activity_id int;")
    cursor.execute("ALTER TABLE olms.core_history ADD CONSTRAINT core_activity_id_fk FOREIGN KEY (core_activity_id) REFERENCES public.core_activity;")
    print "olms.History FK created to core.Activity"

    # olms.history will have FK to crm.Activity named crm_activity
    cursor.execute("alter table olms.core_history add COLUMN crm_activity_id int;")
    cursor.execute("ALTER TABLE olms.core_history ADD CONSTRAINT crm_activity_id_fk FOREIGN KEY (crm_activity_id) REFERENCES public.crm_activity;")
    print "olms.History FK created to crm.Activity"

    # olms.customer will have FK to core.Tracker
    cursor.execute("alter table olms.core_customer add COLUMN core_tracker_id int;")
    cursor.execute("ALTER TABLE olms.core_customer ADD CONSTRAINT core_tracker_id_fk FOREIGN KEY (core_tracker_id) REFERENCES public.core_tracker;")
    print "olms.Customer FK created to core.Tracker"

    # olms.emailcustomer will have FK to core.Tracker
    cursor.execute("alter table olms.core_emailcustomer add COLUMN core_tracker_id int;")
    cursor.execute("ALTER TABLE olms.core_emailcustomer ADD CONSTRAINT core_tracker_id_fk FOREIGN KEY (core_tracker_id) REFERENCES public.core_tracker;")
    print "olms.EmailCustomer FK created to core.Tracker"

    # olms.transaction will have FK to core.requirement
    cursor.execute("alter table olms.core_transaction add COLUMN core_requirement_id int;")
    cursor.execute("ALTER TABLE olms.core_transaction ADD CONSTRAINT core_requirement_id_fk FOREIGN KEY (core_requirement_id) REFERENCES public.core_requirement;")
    print "olms.Transaction FK created to core.Requirement"

    # olms.customer will have FK to core.User
    cursor.execute("alter table olms.core_customer add COLUMN core_user_id int;")
    cursor.execute("ALTER TABLE olms.core_customer ADD CONSTRAINT core_user_id_fk FOREIGN KEY (core_user_id) REFERENCES public.core_user;")
    print "olms.Customer FK created to core.User"

    # olms.emailcustomer will have FK to core.User
    cursor.execute("alter table olms.core_emailcustomer add COLUMN core_user_id int;")
    cursor.execute("ALTER TABLE olms.core_emailcustomer ADD CONSTRAINT core_user_id_fk FOREIGN KEY (core_user_id) REFERENCES public.core_user;")
    print "olms.EmailCustomer FK created to core.User"

if __name__ == '__main__':
    add_foreign_keys()
