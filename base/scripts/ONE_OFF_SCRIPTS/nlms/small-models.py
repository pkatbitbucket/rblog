import os
import django
import csv
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()


from core import models as core_models
from crm import models as crm_models
from django.core.exceptions import MultipleObjectsReturned
from django.db import connection
cust_data = {}
cust_email = {}
campaign = {}
c = 0

filepath = "/tmp/"
log_file = open('/tmp/log_file.log', 'w')
cursor = connection.cursor()


def get_product_name(campaign):
    if campaign == 'one-health':
        return 'health'
    elif campaign.startswith('motor-bike'):
        return 'bike'
    elif campaign.startswith('motor') or campaign.startswith('free-breakdown'):
        return 'car'
    else:
        return campaign.split('-')[0]


def fix_advisors():
    advisor_data = {}
    with open(os.path.join(filepath, "auth_user.csv"), "rb") as csvfile:
        reader = csv.reader(csvfile, delimiter='~')
        # COPY (select id, username, email, first_name, last_name, is_active from
        # auth_user) TO '/tmp/auth_user.csv' DELIMITER '~' CSV HEADER;
        for row in reader:
            if row:
                try:
                    int(row[0])
                    advisor_data[row[0]] = {
                        'username': row[1].lower(),
                        'email': row[2].lower(),
                        'first_name': row[3].title(),
                        'last_name': row[4].title(),
                        'is_active': True if row[5] == 't' else False
                    }
                except:
                    pass

    with open(os.path.join(filepath, "core_userprofile.csv"), "rb") as csvfile:
        reader = csv.reader(csvfile, delimiter='~')
        # COPY (select user_id, role, ameyo_id, is_available, expertise,
        # extension_no, mobile, did from core_userprofile) TO '/tmp/auth_user.csv'
        # DELIMITER '~' CSV HEADER;
        for row in reader:
            if row:
                try:
                    int(row[0])
                    d = {
                        'role': row[1],
                        'dialer_username': row[2],
                        'is_available': True if row[3] == 't' else False,
                        'expertise': row[4],
                        'dialer_extension': row[5],
                        'phone': row[6][-10:],
                        'dialer_did': row[7],
                    }
                    d.update(advisor_data[row[0]])
                    advisor_data[row[0]] = d.copy()
                except:
                    pass

    x = advisor_data.copy()
    advisor_data = {}
    for key, d in x.items():
        print d
        email = d['email'].strip() or d['username'] + '@jarvis.coverfox.com'
        print 'EMail -------', email
        try:
            email_obj, _ = core_models.Email.objects.get_or_create(
                email=email,
                defaults = {
                    'is_primary': True,
                    'is_exists': True,
                    'is_verified': True
                }
            )
        except MultipleObjectsReturned as e:
            email_obj = core_models.Email.objects.filter(email=d['email']).last()
            email_obj.is_primary = True
            email_obj.is_exists = True
            email_obj.is_verified = True
        if not email_obj.user:
            email_obj.user = core_models.User.objects.create()

        if d.get('phone'):
            try:
                phone_obj, _ = core_models.Phone.objects.get_or_create(
                    number=d['phone'],
                    defaults = {
                        'is_primary': True,
                        'is_exists': True,
                        'is_verified': True
                    }
                )
            except MultipleObjectsReturned as e:
                phone_obj = core_models.Phone.objects.filter(
                    number=d['phone']).last()
                phone_obj.is_primary = True
                phone_obj.is_exists = True
                phone_obj.is_verified = True
            phone_obj.user = email_obj.user
            phone_obj.save()

        email_obj.save()
        del d['email']
        u = email_obj.user
        advisorprofile = crm_models.Advisor(user=u)
        for k, v in d.items():
            if hasattr(u, k):
                setattr(u, k, v)
            if hasattr(advisorprofile, k):
                setattr(advisorprofile, k, v)
        u.save()
        advisorprofile.save()
        advisor_data[key] = u
        sql_query = "update olms.auth_user set crm_advisor_id=%s where id=%s" % (advisorprofile.id, key)
        cursor.execute(sql_query)
    del x
    return advisor_data


def fix_campaigns(advisor_data):
    campaigns = {}
    c = 0
    with open(os.path.join(filepath, "core_campaign.csv"), "rb") as csvfile:
        reader = csv.reader(csvfile, delimiter='~')
        for row in reader:
            if c == 0:
                c += 1
                continue
            if row:
                campaigns[row[0]] = row[1]

    c = 0
    with open(os.path.join(filepath, "core_campaign_callers.csv"), "rb") as csvfile:
        reader = csv.reader(csvfile, delimiter='~')
        for row in reader:
            if c == 0:
                c += 1
                continue
            if row:
                q = crm_models.Queue.objects.filter(name=campaigns[row[1]]).last()
                if not q:
                    q = crm_models.Queue.objects.create(
                        name=campaigns[row[1]], code=campaigns[row[1]])
                sql_query = "update olms.core_campaign set crm_queue_id=%s where id=%s" % (q.id, row[1])
                cursor.execute(sql_query)
                crm_models.QueueAdvisor.objects.get_or_create(
                    queue=q, advisor=advisor_data[row[2]].advisor.first())


def main():
    advisor_data = fix_advisors()
    fix_campaigns(advisor_data)

if __name__ == "__main__":
    main()
