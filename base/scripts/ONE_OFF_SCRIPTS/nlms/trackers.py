import os
import django
import csv
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()


from core import models as core_models
from django.core.exceptions import MultipleObjectsReturned
from django.db import connection
cust_data = {}
cust_email = {}
campaign = {}
c = 0

filepath = "/tmp/"
log_file = open('/tmp/log_file.log', 'w')
cursor = connection.cursor()

# Query
# \copy (SELECT core_customer.id, name, mobile, mobile2, mobile3, mobile4 FROM core_customer INNER JOIN core_campaigndata ON (core_customer.id = core_campaigndata.customer_id) INNER JOIN core_campaigndata_history ON (core_campaigndata.id = core_campaigndata_history.campaigndata_id) INNER JOIN core_history ON (core_campaigndata_history.history_id = core_history.id) WHERE core_history.call_disposition IN ('INVALID_NUMBER', 'INTERNAL_TESTING', 'WRONG_NUMBER', 'DO_NOT_CALL', 'UNAWARE_OF_GIVING_NUMBER'))  TO '/tmp/core_customer_to_tracker.csv' DELIMITER '~' CSV HEADER;

# Customer.objects.filter(campaigndata__history__call_disposition__in=x).query
import sys
total_scripts = int(sys.argv[1])
current_script = int(sys.argv[2])


def fix_customers():
    c = 0
    with open(os.path.join(filepath, "core_customer_to_tracker.csv"), "rb") as csvfile:
        reader = csv.reader(csvfile, delimiter='~')
        for row in reader:
            if c == 0:
                c += 1
                continue
            cid = int(row[0])
            if cid % total_scripts != current_script:
                print current_script, cid, total_scripts
                continue
            if row:
                mobiles_row = [row[2], row[3], row[4], row[5]]
                mobiles = []
                for mob in mobiles_row:
                    if mob.strip() and len(mob.strip()) >= 10:
                        mobiles.append(mob[-10:])
                cust_data[row[0]] = {}
                cust_data[row[0]]['mobiles'] = mobiles

    print "Customer Data is taken into memory with name, and emails"

    # mobile: tracker_id
    MOBILE_TRACKERID_MAP = {item[0]: item[
        1] for item in core_models.Phone.objects.values_list('number', 'visitor')}

    # tracker_id: tracker for trackers in above mapping
    TRACKERS = {
        t.id: t for t in core_models.Tracker.objects.filter(
            id__in=MOBILE_TRACKERID_MAP.values()
        ).distinct()
    }
    TRACKERS.update({None: None, '': None})
    # phone: tracker
    TRACKER_MAP = {k: TRACKERS[v] for k, v in MOBILE_TRACKERID_MAP.items()}

    customer_count = 0
    for cid, cdata in cust_data.items():
        if not cdata['mobiles']:
            continue
        tracker = None
        for mob in cdata['mobiles']:
            if TRACKER_MAP.get(mob):
                tracker = TRACKER_MAP[mob]
                break

        if not tracker:
            session_key = 'phone_%s' % cdata['mobiles'][0]
            tracker, _ = core_models.Tracker.objects.get_or_create(
                session_key=session_key
            )
        sql_query = "update olms.core_customer set core_tracker_id=%s where id=%s" % (tracker.id, cid)
        cursor.execute(sql_query)
        mob_count = 0
        for mobile in cdata['mobiles']:
            try:
                phone, _ = core_models.Phone.objects.get_or_create(
                    number=mobile[-10:], visitor=tracker, user__isnull=True
                )
            except MultipleObjectsReturned as e:
                print e, mobile[-10:]
                phone, _ = core_models.Phone.objects.filter(
                    number=mobile[-10:], visitor=tracker, user__isnull=True
                ).last()
            if mob_count == 0:
                phone.is_primary = True
                phone.save()
            mob_count += 1
        customer_count += 1
        if customer_count % 100 == 0:
            print customer_count


def main():
    fix_customers()

if __name__ == "__main__":
    main()
