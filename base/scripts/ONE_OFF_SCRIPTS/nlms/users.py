import os
import django
import csv
import datetime
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()


from core import models as core_models
from django.core.exceptions import MultipleObjectsReturned
from django.db import connection
cust_data = {}
cust_email = {}
campaign = {}
c = 0

filepath = "/tmp/"
log_file = open('/tmp/log_file.log', 'w')
cursor = connection.cursor()

# Query
"""
-- all phones
select number, user_id from core_phone;
-- missing customers
"""

def p(msg):
    print datetime.datetime.now(), msg

def get_missing_customers_and_all_phones():
    skipped_customers1 = 0
    p("get_missing_customers_and_all_phones started")
    all_phones = core_models.Phone.objects.values_list("number", "user_id")
    p("got all phones: %s" % all_phones.count())
    all_phones = dict(all_phones)
    p("got all phones dict:")

    cursor.execute("select id, name, mobile, mobile2, mobile3, mobile4 from olms.core_customer where core_user_id is null and core_tracker_id is null")
    missing_customers = []
    p("Query done")
    for row in cursor.fetchall():
        # want all customers who have even one phone missing
        has_any_missing = False
        for k in range(2, 5):
            if row[k] not in all_phones:
                has_any_missing = True
                break
        if has_any_missing:
            missing_customers.append((row[0], row[1], row[2:], ))
        else:
            skipped_customers1 += 1

    p("got all missing customers: %s" % len(missing_customers))
    print "NO OF SKPPED CUSTOMERS1: ", skipped_customers1
    return missing_customers, all_phones


# \copy (SELECT DISTINCT core_customer.id, name, mobile, mobile2, mobile3, mobile4 FROM core_customer INNER JOIN core_campaigndata ON (core_customer.id = core_campaigndata.customer_id) INNER JOIN core_campaigndata_history ON (core_campaigndata.id = core_campaigndata_history.campaigndata_id) INNER JOIN core_history ON (core_campaigndata_history.history_id = core_history.id) WHERE core_history.call_disposition NOT IN ('INVALID_NUMBER', 'INTERNAL_TESTING', 'WRONG_NUMBER', 'DO_NOT_CALL', 'UNAWARE_OF_GIVING_NUMBER'))  TO '/tmp/core_customer_to_user.csv' DELIMITER '~' CSV HEADER;

# Customer.objects.exclude(campaigndata__history__call_disposition__in=x).query

total_customers = 0
def fix_customers():
    cursor.execute("SET search_path=public")
    customer_count = 0
    skipped_customers = 0

    missing_customers, all_phones = get_missing_customers_and_all_phones()

    for customer in missing_customers:
        c_name = customer[1]
        c_id = customer[0]
        c_phones = customer[2]
        mob_count = 0
        user = None
        for mobile in c_phones:
            if mobile in all_phones:
                skipped_customers += 1
                continue
            if not mobile:
                skipped_customers += 1
                continue
            if len(mobile) < 10:
                skipped_customers += 1
                continue
            mobile = mobile[-10:]
            if mobile in all_phones:
                user = all_phones[mobile]
                break
            if not user:
                user = core_models.User.objects.create(first_name=c_name).id

            phone = core_models.Phone.objects.create(
                number=mobile, user_id=user,
                is_verified=True,
                is_exists=True
            )
            if mob_count == 0:
                phone.is_primary = True
                phone.save()
            mob_count += 1
            sql_query = "update olms.core_customer set core_user_id=%s where id=%s" % (user, c_id)
            cursor.execute(sql_query)
        customer_count += 1

        if customer_count % 100 == 0:
            print "two", customer_count, c_id, total_customers, datetime.datetime.now()
    print "NO OF SKPPED CUSTOMERS: ", skipped_customers


def main():
    fix_customers()

if __name__ == "__main__":
    main()
