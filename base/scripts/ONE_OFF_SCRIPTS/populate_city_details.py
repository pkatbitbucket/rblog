import sys
import time
import datetime
import os
import csv
import ast

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from customdb.customquery import sqltojson, sqltodict
from django.core.mail import send_mail
import thread
import threading
from datetime import timedelta
from django.utils.encoding import smart_str, smart_unicode

from core.models import User
from health_product.models import *
import urllib2
import urllib

pin_codes = [line.strip() for line in open('scripts/uniq_pin_list')]


lines = [line.strip() for line in open('scripts/city_resp')]

city_dict = {}
for l in lines:
    item_list = l.split('||')
    if len(item_list) == 2:
        city_dict[item_list[0]] = ast.literal_eval(item_list[1])


for pin in pin_codes:
    if not PincodeMapping.objects.filter(pin=pin):
        pm = PincodeMapping(pin=pin)
    else:
        pm = PincodeMapping.objects.get(pin=pin)
    if pin in city_dict.keys():
        pm.city_detail = city_dict[pin]
    pm.save()
