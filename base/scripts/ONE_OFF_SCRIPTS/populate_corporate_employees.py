import os
import sys
import csv
print sys.path

sys.path.append("../base/")
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from motor_product.models import CorporateEmployee
from core.models import Company


def populateEmployees(company_slug, csvfile):
    company = Company.objects.get(slug=company_slug)
    print company
    with open(csvfile, 'rb') as f:
        reader = csv.reader(f)
        rownum = 0
        for row in reader:
            if rownum >= 1:
                try:
                    emp, created = CorporateEmployee.objects.get_or_create(company=company,
                                                                           employee_code=row[0],
                                                                           employee_name=row[1])
                except Exception, e:
                    print e
            rownum += 1

if __name__ == '__main__':
    company_slug = sys.argv[1]
    file_name = sys.argv[2]
    populateEmployees(company_slug, file_name)
