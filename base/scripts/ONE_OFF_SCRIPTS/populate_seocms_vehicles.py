import sys
import time
import datetime
import os
import csv

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from seocms.models import *
# from cms.models import Page

for page in Page.objects.all():
    if page.category:
        if page.category.slug not in ['car-insurance', 'two-wheeler-insurance', 'health-insurance']:
            brand = page.category.slug
            modelName = page.url
            print brand + " " + modelName
            Vehicle.objects.get_or_create(
                page=page, brand=brand, modelName=modelName)
