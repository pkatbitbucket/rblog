import os
import sys
from optparse import OptionParser
import base64
import logging
import xml.etree.ElementTree as ET

from django.core.files.base import File
from suds.client import Client
from suds.plugin import MessagePlugin

# from utils import request_logger
import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from health_product.models import *

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.transport.http').setLevel(logging.DEBUG)

PARENT_CODE = '20012865'
PARENT_NAME = 'Coverfox Insurance Broking Pvt Ltd'
CHILD_CODE = '20012981'
CHILD_NAME = 'COVERFOX INSURANCE BROKING PVT LTD ONLINE'
PRODUCT_CODE = {
    'care': '10001101'
}
ORIGINAL_WSDL_URL = 'https://www.religarehealthinsurance.com/relinterface/services/CreatePolicyServiceInt?wsdl'
RELIGARE_WSDL_FILE = 'file://%s' % (os.path.join(settings.SETTINGS_FILE_FOLDER,
                                                 'health_product/insurer/religare/webservice/myReligare.wsdl'))
PDF_WSDL_PATH = 'file://%s' % (os.path.join(settings.SETTINGS_FILE_FOLDER,
                                            'health_product/insurer/religare/webservice/PDF_fetch.wsdl'))
ENDPOINT_URL = 'https://www.religarehealthinsurance.com/relinterface/services/CreatePolicyServiceInt.CreatePolicyServiceIntHttpSoap11Endpoint/'
PAYMENT_URL = 'https://www.religarehealthinsurance.com/portalui/PortalPayment.run'
AGENT_ID = '20012981'
#AGENT_ID = '20008025'
WSDL_USERNAME = 'QuotationUser'
WSDL_PASSWORD = 'religare@123'
PDF_USERNAME = 'SymbiosysUser'
PDF_PASSWORD = 'password-1'
USERNAME = 'serviceClient'
PASSWORD = 'weX46J'


class _AttributePlugin(MessagePlugin):
    """
    Suds plug-in extending the method call with arbitrary attributes.
    """

    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def marshalled(self, context):
        children = context.envelope.getChildren()
        header = children[0]
        ebody = children[1]
        children.remove(header)
        ebody.prefix = "SOAP-ENV"


def fetch_pdf(transaction_id):
    transaction = Transaction.objects.get(transaction_id=transaction_id)
    client = Client(PDF_WSDL_PATH, username=PDF_USERNAME,
                    password=PDF_PASSWORD)
    policyNo = transaction.policy_number
    ltype = 'POLSCHD'
    client.options.plugins = [_AttributePlugin()]
    resp = client.service.GET_PDF(policyNo, ltype)
    # request_logger.info('\nRELIGARE TRANSACTION: %s \n PDF REQUEST SENT:\n %s \n' % (transaction.transaction_id, client.last_sent()))
    # request_logger.info('\nRELIGARE TRANSACTION: %s \n PDF RESPONSE RECEIVED:\n %s \n' % (transaction.transaction_id, client.last_received()))
    xresp = ET.fromstring('<root>' + resp + '</root>')
    pdf_data = xresp.find('StreamData').text
    data = base64.b64decode(pdf_data)
    # WRITE THE PDF RESPONSE TO A LOCAL FILE
    filepath = os.path.join(settings.POLICY_ROOT, '%s.pdf' %
                            transaction.transaction_id)
    with open(filepath, 'wb') as f:
        f.write(data)
        f.close()
    # UPLOAD THE LOCAL FILE TO DJANGO FILE FIELD
    myfile = open(filepath)
    new_file = File(myfile)
    transaction.policy_document.save(
        '%s.pdf' % transaction.transaction_id, new_file)
    transaction.save()
    myfile.close()
    return


def fetch_pdf_by_policy_no(policy_no, transaction_id):
    client = Client(PDF_WSDL_PATH, username=PDF_USERNAME,
                    password=PDF_PASSWORD)
    policyNo = policy_no
    ltype = 'POLSCHD'
    client.options.plugins = [_AttributePlugin()]
    resp = client.service.GET_PDF(policyNo, ltype)
    # request_logger.info('\nRELIGARE TRANSACTION: %s \n PDF REQUEST SENT:\n %s \n' % (transaction_id, client.last_sent()))
    # request_logger.info('\nRELIGARE TRANSACTION: %s \n PDF RESPONSE RECEIVED:\n %s \n' % (transaction_id, client.last_received()))
    xresp = ET.fromstring('<root>' + resp + '</root>')
    pdf_data = xresp.find('StreamData').text
    data = base64.b64decode(pdf_data)
    # WRITE THE PDF RESPONSE TO A LOCAL FILE
    filepath = os.path.join(settings.POLICY_ROOT, '%s.pdf' % transaction_id)
    with open(filepath, 'wb') as f:
        f.write(data)
        f.close()
    # UPLOAD THE LOCAL FILE TO DJANGO FILE FIELD
    myfile = open(filepath)
    new_file = File(myfile)
    myfile.close()
    return


def main():
    usage = "usage: %prog -t <transaction id>"
    parser = OptionParser(usage)

    parser.add_option("-t", "--transaction_id", type="string",
                      dest="transaction_id", help="transction id to process")
    (options, args) = parser.parse_args()

    if not options.transaction_id:
        parser.error("Must specify transaction id you want to process")
    transaction_id = options.transaction_id.strip()
    fetch_pdf(transaction_id)

if __name__ == "__main__":
    main()
