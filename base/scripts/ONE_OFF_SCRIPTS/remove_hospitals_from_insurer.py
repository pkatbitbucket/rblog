import sys
import time
import datetime
import os
from optparse import OptionParser

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from django.db import models
from activity.models import *
from wiki.models import Insurer


def main():
    usage = "usage: %prog -i <insurer_id for which you want to remove all hospitals from activity map>"
    parser = OptionParser(usage)

    parser.add_option("-i", "--insurer", type="string", dest="insurer",
                      help="insurer for which hospitals need to be deleted")

    (options, args) = parser.parse_args()

    if not options.insurer:
        parser.error("Must specify insurer you want to delete hospitals of")

    insurer_id = int(options.insurer.strip())
    insurer = Insurer.objects.get(id=insurer_id)
    print "\n\n---------- Going to delete hospitals for insurer '%s'----------------\n\n" % insurer.title
    print "\n\n!!!!!!! Starting in 10 seconds from now, if you want to change your mind, please press Ctrl-C !!!!!!!!!\n\n"
    time.sleep(12)

    for hcc in HealthCareCenter.objects.filter(insurer=insurer):
        hcc.insurer.remove(insurer)
        print "Removing insurer %s from health care center record %s" % (insurer.title, hcc.id)

    print "Removing previously uploaded raw data for %s" % (insurer.title)
    Data.objects.filter(by_insurer=insurer).delete()

if __name__ == "__main__":
    main()
