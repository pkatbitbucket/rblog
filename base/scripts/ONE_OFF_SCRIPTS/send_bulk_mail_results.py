import os
import sys
import envdir
import cf_cns

envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from dateutil.relativedelta import *

from django.conf import settings
from mailer import direct as direct_mail
from mailer.models import *
import time

blacklist = [
    'akhilvsingh@gmail.com',
    'prashant.bh@gmail.com',
    'laxmi.chaurasia@gmail.com',
    'chaitraamin@yahoo.co.in',
    'krishkt723@gmail.com',
    'prashant.m@gmail.com',
    'ramitha.shetty@gmail.com',
    'sanketrathi08@gmail.com',
    'kiran@crispygam.es',
    'upesh.m@gmail.com',
    'upadhyay@gmail.com',
    'avikal_chauhan@yahoo.com',
    'rohit6699@gmail.com',
    'dinilatom328@gmail.com',
    'akhtenglikar@gmail.com',
    'amith.shetty@gmail.com',
    'sureshsvishwa@gmail.com',
    'jagat4u@gmail.com',
    'vinit.singh618@gmail.com',
    'dhamankarpk@gmail.com',
    'neha.chopde@yahoo.com',
    'parag.deo@gmail.com',
    'saurab.chavan@gmail.com',
    'nautiyal.siddharth@gmail.com',
    'nautiyal.siddharth@gmail.com',
    'rajarshc@gmail.com',
    'mishra_vinay@hotmail.com'
]

checklist = [
    'glitterbug',
    'coverfox',
    'test',
    'aaa',
    'bbb'
]
lower_cutoff_time = datetime.datetime.now() - relativedelta(days=120)
upper_cutoff_time = datetime.datetime.now() - relativedelta(days=10)


activity_list = []
for q in Queue.objects.filter(is_active=False, code='RESULT_DROP', updated_on__gte=lower_cutoff_time).order_by('-created_on'):
    a = q.tracker.activity_set.filter(
        activity_type='RESULTS_VISITED').order_by('-updated_on')[0]
    if a not in activity_list \
            and not a.tracker.queue_set.filter(updated_on__gte=upper_cutoff_time) \
            and a.tracker.user \
            and a.tracker.user.email not in blacklist \
            and not a.tracker.transaction_set.filter(status__in=['SUCCESS', 'COMPLETED']):
        add_mail = True
        for c in checklist:
            if a.tracker.user.email.find(c) >= 0:
                add_mail = False
        if add_mail:
            activity_list.append(a)

print 'TOTAL >>>>', len(activity_list)

for a in activity_list:
    print a.tracker.user.email, a.activity_id
    obj = {
        'user_id': a.tracker.user.email,
        'to_email': a.tracker.user.email,
        'name': a.tracker.user.first_name,
        'tag': 'results-drop',
        'contact_url': '%s/health-plan/contact-me/' % settings.SITE_URL,
        'results_url': '%s/health-plan/#results/%s' % (settings.SITE_URL, a.activity_id)
    }
    time.sleep(2)
    cf_cns.notify('RESULTS_DROP_MAIL2', to=(obj.get('name'), obj['to_email']), obj=obj)

