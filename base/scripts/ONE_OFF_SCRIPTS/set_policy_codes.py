import sys
import time
import datetime
import os
import csv

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

import thread
import threading
from datetime import timedelta
from django.utils.encoding import smart_str, smart_unicode

import urllib2
import urllib
import json
import uuid

from health_product.models import *

# FOR BHARTI AXA
hp = HealthProduct.objects.get(id=19)
hp.policy_code = 'smart_health_individual'
hp.save()


hp = HealthProduct.objects.get(id=21)
hp.policy_code = 'smart_health_floater'
hp.save()


# FOR RELIANCE
hp = HealthProduct.objects.get(id=35)
hp.policy_code = 'healthwise_individual'
hp.save()

hp = HealthProduct.objects.get(id=38)
hp.policy_code = 'healthwise_floater'
hp.save()


# FOR STAR HEALTH
hp = HealthProduct.objects.get(id=37)
hp.policy_code = 'mcinew'
hp.save()

hp = HealthProduct.objects.get(id=36)
hp.policy_code = 'comprehensive'
hp.save()

# FOR L and T

hp = HealthProduct.objects.get(id=33)
hp.policy_code = 'prime_family'
hp.save()

hp = HealthProduct.objects.get(id=32)
hp.policy_code = 'classic_family'
hp.save()

hp = HealthProduct.objects.get(id=32)
hp.policy_code = 'classic_individual'
hp.save()
