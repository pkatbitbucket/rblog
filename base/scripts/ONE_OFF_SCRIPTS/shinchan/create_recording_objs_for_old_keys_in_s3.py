import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')  # noqa
django.setup()  # noqa

import datetime
from django.conf import settings
import boto
from cfrecordings import models as cfr_models
from crm import models as crm_models
import logging


s3_key_parsing_logger = logging.getLogger('s3_key_parsing_logger')
existing_keys = []

s3 = boto.connect_s3(
    aws_access_key_id=settings.CFOXRECORDING_AWS_ACCESS_KEY,
    aws_secret_access_key=settings.CFOXRECORDING_AWS_SECRET_KEY
)

bucket = s3.get_bucket(settings.CFOXRECORDING_BUCKET_NAME)

for k in bucket:
    existing_keys.append(k.name)

print "Number of keys in %s bucket: %s" % (bucket.name, len(existing_keys))

advisor_ameyo_username_to_id_map = {}
for a in crm_models.Advisor.objects.all():
    advisor_ameyo_username_to_id_map[a.dialer_username] = a.id

for a in crm_models.Advisor._base_manager.all():
    if a.dialer_username not in advisor_ameyo_username_to_id_map:
        advisor_ameyo_username_to_id_map[a.dialer_username] = a.id

for kname in existing_keys:
    if kname.endswith('.mp3'):
        if cfr_models.Recording.objects.filter(filename=kname):
            pass
        else:
            print kname
            try:
                _, phone, caller, dated = kname[:-4].split('/')[-1].split("_")
                call_time = datetime.datetime.strptime(dated, "%Y-%m-%d-%H-%M-%S")

                rec = cfr_models.Recording(
                    phone=phone[-10:],
                    advisor_id=advisor_ameyo_username_to_id_map[caller],
                    call_time=call_time,
                    filename=kname
                )
                rec.save()
            except ValueError as e:
                s3_key_parsing_logger.info("Parsing error for key with name %s came with error message as ---%s---" % (kname, e))
