import sys
import os
from optparse import OptionParser

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from health_product.models import *
import urllib2

from utils import request_logger
from django.conf import settings
from django.core.files.base import File


STARHEALTH_API_URL = 'http://ig.nallantech.com'
STARHEALTH_API_KEY = '8b310f8129564357a9885eaa66043c3f'
STARHEALTH_SECRET_KEY = '6383a11e284c4e08a12e6c1f77f88c24'


def fetch_pdf(transaction_id):
    transaction = Transaction.objects.get(transaction_id=transaction_id)
    data = {
        'APIKEY': STARHEALTH_API_KEY,
        'SECRETKEY': STARHEALTH_SECRET_KEY,
        'purchaseToken': transaction.payment_token
    }
    processed_data = json.dumps(data)

    pdf_req = urllib2.Request(url='%s/api/policy/proposals/%s/schedule' % (
        STARHEALTH_API_URL,
        transaction.extra['payment_response']['reference_id']
    ), data=processed_data, headers={'Content-Type': 'application/json'})
    pdf_response = urllib2.urlopen(pdf_req)
    pdf_resp = pdf_response.read()
    print pdf_resp
    # WRITE THE PDF RESPONSE TO A LOCAL FILE
    if type(pdf_resp) == dict and 'error' in pdf_resp.keys():
        request_logger.info('\nSTAR TRANSACTION: %s \nPDF GENERATION ERROR":\n %s \n' % (
            transaction.transaction_id, pdf_resp))
    else:
        filepath = os.path.join(settings.POLICY_ROOT,
                                '%s.pdf' % transaction.transaction_id)
        with open(filepath, 'wb') as f:
            f.write(pdf_resp)
            f.close()
        # UPLOAD THE LOCAL FILE TO DJANGO FILE FIELD
        myfile = open(filepath)
        new_file = File(myfile)
        transaction.policy_document.save(
            '%s.pdf' % transaction.transaction_id, new_file)
        transaction.save()
        myfile.close()
    return


def main():
    usage = "usage: %prog -t <transaction id>"
    parser = OptionParser(usage)

    parser.add_option("-t", "--transaction_id", type="string",
                      dest="transaction_id", help="transction id to process")
    (options, args) = parser.parse_args()

    if not options.transaction_id:
        parser.error("Must specify transaction id you want to process")
    transaction_id = options.transaction_id.strip()
    fetch_pdf(transaction_id)

if __name__ == "__main__":
    main()
