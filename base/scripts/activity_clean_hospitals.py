import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

import django
django.setup()


from activity.models import *
from wiki.models import *
import re

amp1 = re.compile(re.escape('&amp;'), re.IGNORECASE)
amp2 = re.compile(re.escape('&amp'), re.IGNORECASE)


for hcc in HealthCareCenter.objects.all():
    data_objects = hcc.data_set.all()
    hcc_insurers = hcc.insurer.all()
    data_insurers = []
    for d in data_objects:
        data_insurers.append(d.by_insurer)
    delete_these_insurers = list(set(hcc_insurers) - set(data_insurers))
    for k in delete_these_insurers:
        hcc.insurer.remove(k)

hospital_ids = set([h.id for h in Hospital.objects.all()])
incr = 0
jump = 10000
total = HealthCareCenter.objects.count()

while incr + jump < total:
    hccs = HealthCareCenter.objects.all()[incr: incr+jump]
    print 'Data for deletion... '
    print type(hccs)
    hcc_ids = set([hcc.id for hcc in hccs])
    hospital_ids = hospital_ids - hcc_ids
    incr += jump

print 'Hospitals to delete', hospital_ids
Hospital.objects.filter(id__in=hospital_ids).delete()
print 'Hospital deleted..'

for hcc in HealthCareCenter.objects.all():
    print 'Processing ', hcc.id
    if hcc.duplicate:
        continue
    insurer_list = []
    duplicates = hcc.healthcarecenter_set.all()
    if duplicates:
        for d in duplicates:
            insurer_list.extend(list(d.insurer.all()))
    insurer_list.extend(list(hcc.insurer.all()))

    try:
        hos = Hospital.objects.get(id=hcc.id)
        hcc_insurer = list(hcc.insurer.all())

        hcc_insurer.extend(insurer_list)

        hos_insurer = hos.insurers.all()
        to_delete = list(set(hos_insurer) - set(hcc_insurer))
        to_add = list(set(hcc_insurer) - set(to_delete))

        for i in to_delete:
            hos.insurers.remove(i)

        for i in to_add:
            hos.insurers.add(i)

    except Hospital.DoesNotExist:
        hname = amp1.sub('&', hcc.name)
        hname = amp2.sub('&', hname)

        haddress = hcc.address1.encode('ascii', 'ignore')
        haddress = amp1.sub('&', haddress)
        haddress = amp2.sub('&', haddress)
        h_obj = Hospital(
            id=hcc.id,
            name=hname,
            address=haddress,
            landmark=hcc.landmark,
            district=hcc.district,

            city=hcc.city,
            state=hcc.city.state,
            pin=hcc.pincode,

            std_code=hcc.std_code,
            phone1=''.join([i for i in hcc.phone1 if i.isdigit()]),
            phone2=''.join([i for i in hcc.phone2 if i.isdigit()]),
            fax1=hcc.fax1,
            fax2=hcc.fax2,
            email=hcc.email,
            url=hcc.url,

            lat=hcc.lat,
            lng=hcc.lng,
        )
        h_obj.save()
        for ins in list(set(insurer_list)):
            h_obj.insurers.add(ins)

print 'Done..'
