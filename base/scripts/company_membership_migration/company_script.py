"""
1. Run this script first
This script copies all the member in CompanyMembership to Employee

2. Run dump.sh
     using bash dump.sh
This script takes the dump of  CompanyMembership
in a file company_membership_dump
and HRUserInfo in hrms_dump
3. Then run dump_delete.py script:

and deletes the all instances of CompanyMembership and HRUserInfo

4. Change field from CompanyMembership to Employee
    Replace All CompanyMembership Instances in business_logic.py and in view.py
    and import it accordingly
    (these changes already have been done)
5. Makemigrations then migrate
   Rerun the shell
6. Run the script hr_info_add.py which added all the CompanyMembership
elements as employee in HRUserInfo

7. Change request.company assignment in middileware.py from coverfox to core for Company
8. Change all the CompanyMembership.objects to Employee.objects in hrms/view.py
python manage.py dumpdata myapp.model_name > file_name


"""
import os
import export_settings  # NOQA

import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'humanlly')
django.setup()

from coverfox.models import Company, CompanyMembership
from core.models import Employee

company_list = []
for each in Company.objects.all():
    dic = {}
    dic['name'] = each.name
    dic['slug'] = each.slug
    company_list.append(dic)

from core.models import Company
for company in company_list:
    if not Company.objects.filter(name=company['name']):
        Company.objects.create(name=company['name'], slug=company['slug'])
        print " %s have been copied from coverfox.Company to Core.company" % company['name']

for member in CompanyMembership.objects.all():

    if not Employee.objects.filter(user=member.user):
        Employee.objects.create(
            user=member.user,
            joined_on=member.joined_on,
            employee_id=member.employee_id,
            left_on=member.left_on,
            company=Company.objects.get(name=member.company.name)
        )
        print "%s have been copied from coverfox.Companymembership to core.Employee" % member.user

"""
Take the dump of HRUserInfo.
Change the field from CompanyMembership to Employee.
Run the migration
then run the script hr_info_add.py
"""
"""
Procedure:
"""
