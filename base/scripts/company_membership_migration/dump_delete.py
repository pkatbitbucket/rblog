import export_settings  # NOQA
from coverfox.models import CompanyMembership
from hrms.models import HRUserInfo

for com_mem in CompanyMembership.objects.all():
    com_mem.delete()
print "deleted all Companymembership instances"

for hr_info in HRUserInfo.objects.all():
    hr_info.delete()
print "Deleted all hruserinfo instances which have companymembership as foreign key"
