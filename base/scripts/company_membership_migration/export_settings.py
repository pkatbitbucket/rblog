import os
import sys
import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import django
import humanlly  # NOQA
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'humanlly')
django.setup()
