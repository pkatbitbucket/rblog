"""
This script would not work if it founds multiple company membership for same user
"""
import export_settings  # NOQA
from core.models import Employee
from hrms.models import HRUserInfo, ReviewClass
import json

"loading the dumped data into variable hr_info"
with open('hrms_dump', 'r') as hrms_dump:
    hr_infos = json.loads(hrms_dump.read())

with open('company_membership_dump', 'r') as com_mem:
        com_mems = json.loads(com_mem.read())

open('company_membership_dump')
for hr_info in hr_infos:
    member_id = hr_info['fields']['member']
    review_class_id = hr_info['fields']['review_class']
    user_name = hr_info['fields']['username']
    for com_mem in com_mems:
        if com_mem['pk'] == member_id:
            member = com_mem['fields']['user']
    HRUserInfo.objects.create(
        member=Employee.objects.filter(user_id=member)[0],
        review_class=ReviewClass.objects.filter(id=review_class_id)[0],
        username=user_name
    )
