import sys
import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
django.setup()
from optparse import OptionParser
import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))
from django.conf import settings as main_settings

from wiki.models import *

usage = "usage: %prog [options] arg1"
parser = OptionParser(usage)

parser.add_option("-b", "--base", action="store_true",  dest="base_premium",
                  help="specifies that the premium entered is exclusive of service tax")
parser.add_option("-t", "--total", action="store_true", dest="total_premium",
                  help="specifies that the premium entered is inclusive of service tax")
parser.add_option("-r", action="store_true", dest="is_run",
                  help="if not specified only test will be run, no db entry will be made")
parser.add_option("-f", "--filename", metavar="filename", help="""
file name with cover limit content e.g.

Individual:
X|Payment period|Gender|States|Cities|Two year discount|slab_id|slab_id|slab_id|slab_id
lower_age-upper-age|premium_slab1|premium_slab2

X|1|MALE|Gujarat+Goa||7.5|2|3|4|5|6|7
0.06-17|2556|2694|2894|3144|3371|3544
18-35|3015|3410|4419|5532|6187|6558
36-|3757|4218|5586|6992|7820|8289

Family:
XX|Payment period|nA-nC|Gender|States|Cities|Two year discount|slab_id|slab_id|slab_id|slab_id
lower_age-upper-age|premium_slab1|premium_slab2

XX|1|1A-2C|||Ahmedabad+Bangalore+Mumbai+Thane+Delhi+Faridabad+Gurgaon+Noida||12|13|14
0.06-25|5880|7585|10625
26-40|6720|9840|13800
41-55|10080|14760|20010

XX|2|1A-3C|||||12|13|14
0.11-25|4900|6320|8855
26-40|5600|8200|11500
41-55|8400|12300|16675

gender = MALE / FEMALE
States = Gujarat+Goa+Maharashtra
Cities = Mumbai+Delhi+Pune

"""
                  )

(options, args) = parser.parse_args()

cpts_to_save = []
slabs = []
if not options.filename:
    parser.error("Must specify file in proper format")
elif (options.base_premium and options.total_premium) or not (options.base_premium or options.total_premium):
    parser.error(
        "Do the premium values include service tax (--total) or not (--base)?")
else:
    lines = open(options.filename, 'r').read().split("\n")

    SERVICE_TAX = main_settings.SERVICE_TAX_PERCENTAGE / 100
    if options.base_premium:
        def update_cpt_premium(cpt, premium):
            cpt._premium = premium
    else:
        def update_cpt_premium(cpt, premium):
            cpt._premium = round(premium / (1 + SERVICE_TAX), 2)

    for line in lines:
        if not line:
            continue
        line = line.strip()

        if line.startswith('X'):
            policy_type, n = ('family', 7) if line[
                1] == 'X' else ('individual', 6)
            splitted = line.split("|", n)
            print 'Policy type:', policy_type
            print splitted

            if policy_type == 'family':
                combo_str = splitted.pop(2)
                adult_str, child_str = combo_str.split("A-")
                number_of_adults = int(adult_str)
                number_of_kids = int(child_str.replace('C', ''))

            X, payment_period, gender, states, cities, two_year_discount, slab_ids = splitted
            payment_period = int(payment_period.strip())
            slabs = [Slab.objects.get(id=int(sid.strip()))
                     for sid in slab_ids.split("|")]

            if two_year_discount:
                two_year_discount = float(two_year_discount)
            else:
                two_year_discount = None
            if states.strip():
                states = [State.objects.get(name__iexact=s.strip().lower())
                          for s in states.split("+")]
            if cities.strip():
                cities = [City.objects.get(name__iexact=s.strip().lower())
                          for s in cities.split("+")]
            continue

        age_factor, premium_list = line.split('|', 1)

        lage = float(age_factor.split('-')[0])
        if lage < 1:
            lage = int(lage * 100)
        else:
            lage = int(lage * 1000)

        if age_factor.split('-')[1]:
            uage = float(age_factor.split('-')[1])
            if uage < 1:
                uage = int(uage * 100)
            else:
                uage = uage * 1000
        else:
            uage = None

        print "-------------------------------"
        print line
        print ",".join(["%s-%s" % (sl.sum_assured, sl.grade) for sl in slabs])
        print "Payment period:", payment_period
        if policy_type == 'family':
            print "Adults:", number_of_adults
            print "Kids:", number_of_kids
        print "Lower age limit:", lage
        print "Upper age limit:", int(uage / 1000) if uage else None
        print "Gender:", gender
        print "States:", states
        print "Cities:", cities
        print "2 year discount:", two_year_discount
        print "%s premiums:" % ('Base' if options.base_premium else 'Total'), premium_list
        print "-------------------------------"

        if len(premium_list.split('|')) != len(slabs):
            raise Exception("Slabs not matching premium list")

        for premium, slab in zip(premium_list.split('|'), slabs):
            if not premium:
                continue
            premium = float(premium.strip())

            if policy_type == 'family':
                cpt = FamilyCoverPremiumTable(
                    payment_period=payment_period,
                    adults=number_of_adults,
                    kids=number_of_kids,
                    lower_age_limit=lage,
                    upper_age_limit=uage,
                    gender=gender,
                    two_year_discount=two_year_discount,
                )
            else:
                cpt = CoverPremiumTable(
                    payment_period=payment_period,
                    lower_age_limit=lage,
                    upper_age_limit=uage,
                    gender=gender,
                    two_year_discount=two_year_discount,
                )
            update_cpt_premium(cpt, premium)
            cpts_to_save.append([slab, (cpt, states, cities)])

    if options.is_run:
        if policy_type == 'family':
            for slab in slabs:
                slab.family_cover_premium_table.clear()
        else:
            for slab in slabs:
                slab.cover_premium_table.clear()

        for slab, cpt in cpts_to_save:
            cpt[0].save()
            if cpt[1]:
                cpt[0].state.add(*cpt[1])
            if cpt[2]:
                cpt[0].city.add(*cpt[2])
            cpt[0].slabs.add(slab)
