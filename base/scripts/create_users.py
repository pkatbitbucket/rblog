"""
Create users from a csv file (format: first_name, last_name, email)
and send them a mail to set their passwords.
"""
import os
import sys
from argparse import ArgumentParser
import csv
import xlrd
import logging
import datetime
import envdir
from collections import defaultdict
import cf_cns

envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import django
sys.path.append('../../humanlly.com/')
import humanlly
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'humanlly')
django.setup()
# from django.core.urlresolvers import reverse
from django.core.mail import get_connection
import settings as rblog_settings
from django.conf import settings

import utils
from core.models import User, Email
from coverfox.models import Company, CompanyMembership
from hrms.models import HRUserInfo, ReviewClass

parser = ArgumentParser()
parser.add_argument('--company', '-c', required=True, dest='company_slug',
                    help='enter the company for which members are to be added')
parser.add_argument('--input-file', '-f', required=True, dest='input_file',
                    help='enter the path of input file')
args = parser.parse_args()

log_file = 'create_users'
logger = logging.getLogger(log_file)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(settings.LOG_PATH.joinpath("%s.log" % log_file))
formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

company = Company.objects.get(slug=args.company_slug)

workbook = xlrd.open_workbook(args.input_file)
sheet = workbook.sheet_by_index(0)

user_info_map = {}
reportee_map = defaultdict(lambda: [])

for nrow in range(1, sheet.nrows):
    row = sheet.row_values(nrow)
    try:
        first_name, last_name, email, joined_on, review_class, employee_id, managers = [s.strip() for s in row]
    except ValueError:
        logger.error('%s: invalid format' % ', '.join(row))
        continue

    if not email:
        logger.error('%s %s: no email provided' % (first_name, last_name))
        continue

    email = email.lower()
    joined_on = datetime.datetime.strptime(joined_on, '%d-%b-%Y').date()
    left_on = None  # currently only for current employees
    managers = [m.strip() for m in managers.lower().split(',')]
    for manager in managers:
        if manager:
            reportee_map[manager].append(email)

    try:
        user = User.objects.get_user(email=email)
        logger.info('%s: user already exists' % email)
    except (Email.DoesNotExist, User.DoesNotExist):
        user = User.objects.create_user(email, first_name=first_name, last_name=last_name,
                                        last_login=datetime.datetime.today())
        logger.info('%s: user created' % email)
        obj = {
            'use_mandrill': True,
            'user_id': user.id,
            'to_email': user.email,
            'name': utils.clean_name(user.first_name),
            'reset_url': '%s/user/%s/set-password/' % (rblog_settings.SITE_URL, user.token),
        }
        logger.debug('%s: %s' % (email, obj))
        cf_cns.notify('NON_RM_USER_ACCOUNT_CREATION', to=(user.first_name, user.email), obj=obj)

        logger.info('%s: user created and password reset mail sent' % email)

    try:
        membership = CompanyMembership.objects.get(company=company, user=user)
        logger.info('%s: Company Membership already exists' % email)
    except CompanyMembership.DoesNotExist:
        membership = CompanyMembership.objects.create(company=company, user=user, employee_id=employee_id,
                                                      left_on=left_on, joined_on=joined_on)
        logger.info('%s: Company Membership created' % email)

    try:
        hr_user_info = HRUserInfo.objects.get(member=membership)
        logger.info('%s: HRUserInfo already exists' % email)
    except HRUserInfo.DoesNotExist:
        review_class = ReviewClass.objects.get(name=review_class, company=company)
        hr_user_info = HRUserInfo.objects.create(member=membership, review_class=review_class)
        logger.info('%s: HRUserInfo created' % email)
    finally:
        user_info_map[email] = hr_user_info

for manager, reporters in reportee_map.items():
    manager_info = user_info_map[manager]
    reporters_info = [user_info_map[reporter] for reporter in reporters]
    logger.info('Adding reporters for %s: %s' % (manager_info, ', '.join([str(m) for m in reporters_info])))
    manager_info.reporters.add(*reporters_info)
