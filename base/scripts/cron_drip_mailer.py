import sys

import django
import envdir
from dateutil.relativedelta import relativedelta
from django.db.models import Q

import cf_cns
import utils
from activity.models import *
from mailer.models import *

envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))


def process_data():
    cutoff_time = datetime.datetime.now() - relativedelta(days=5)

    for activity in Activity.objects.filter(activity_type='RESULTS_VISITED', is_processed=False, updated_on__gte=cutoff_time):
        if activity.tracker.user and activity.tracker.user.email:
            if activity.tracker.user.email.split('@')[1].lower() in ['gmail.com', 'yahoo.com', 'yahoo.co.in', 'hotmail.com', 'rediffmail.com']:
                comm, created = Communication.objects.get_or_create(
                    tracker=activity.tracker)
                if created or (comm.last_communication_time and comm.last_communication_time < (django.utils.timezone.now() - datetime.timedelta(days=1))):
                    comm.code = 'RESULTS_VISITED'
                    comm.next_communication_time = datetime.datetime.now()
                    comm.is_processed = False
                    comm.extra['activity_id'] = activity.activity_id
                    comm.save()
                if activity.tracker.transaction_set.filter(Q(created_on__gte=activity.created) & ~Q(status='COMPLETED')):
                    transaction = activity.tracker.transaction_set.latest(
                        'created_on')
                    comm.code = 'TRANSACTION_DROP'
                    comm.extra['transaction_id'] = transaction.transaction_id
                    comm.save()
        activity.is_processed = True
        activity.save()
    return


def get_result_drop_object(comm):
    obj = {
        'user_id': comm.tracker.user.email,
        'to_email': comm.tracker.user.email,
        'name': comm.tracker.user.first_name,
        'tag': 'results-drop',
        'contact_url': '%s/health-plan/contact-me/' % settings.SITE_URL,
        'results_url': '%s/health-plan/#results/%s' % (settings.SITE_URL, comm.extra['activity_id'])
    }
    return obj


def get_transaction_drop_object(comm):
    obj = {
        'user_id': comm.tracker.user.email,
        'to_email': comm.tracker.user.email,
        'name': comm.tracker.user.first_name,
        'tag': 'transaction-drop',
        'transaction_url': '%s/health-plan/buy/%s' % (settings.SITE_URL, comm.extra['transaction_id'])
    }
    return obj


def send_out_mail():
    blacklist = [
        'kuchi.swathi3@gmail.com',
        'renoshiypejohn@gmail.com',
        'aniket.thakkar@gmail.com'
    ]
    mail_selector = {
        'RESULTS_VISITED': 'RESULTS_DROP_MAIL2',
        'TRANSACTION_DROP': 'TRANSACTION_DROP_MAIL',
    }
    for comm in Communication.objects.filter(next_communication_time__lte=datetime.datetime.now(), is_processed=False):
        comm.is_processed = True
        comm.last_communication_time = datetime.datetime.now()
        comm.save()
        # SEND MAIL HERE
        if comm.code == 'RESULTS_VISITED':
            obj = get_result_drop_object(comm)
        elif comm.code == 'TRANSACTION_DROP':
            obj = get_transaction_drop_object(comm)

        if obj['to_email'] not in blacklist:
            cf_cns.notify(mail_selector[comm.code], to=(utils.clean_name(obj.get('name')), obj['to_email']),
                          obj=obj, mandrill_template='newbase')
    return

process_data()
send_out_mail()
