import random
import datetime
import sys
import time
import datetime
import os

from optparse import OptionParser

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from django.db import models

from django.template import Template
from django.template import Context
from django.conf import settings

from activity.models import *
from aws import ses
import fcntl
import errno


def send_mail_to_lead():
    TEMPLATE_FOLDER = settings.SETTINGS_FILE_FOLDER.joinpath(
        "scripts/mailer_templates").abspath()
    #template_name = os.path.join(TEMPLATE_FOLDER,'mailer_1.html')
    #subject = "Warm welcome from the Coverfox CEO"
    #from_email = "Varun from Coverfox <varun@cfoutreach.in>"

    template_name = os.path.join(TEMPLATE_FOLDER, 'mailer_2.html')
    subject = "What can a fox teach you about Insurance?"
    from_email = "Shweta from Coverfox <shweta@cfoutreach.in>"

    for ld in LeadData.objects.filter(next_mail_on__lte=datetime.datetime.now(), mail_sent=False):
        if ld.mail_type == 'WELCOME':
            template_html = Template(open(template_name, 'r').read())
            if ld.lead_data.get('name'):
                first_name = ld.lead_data['name'].split(' ')[0]
            else:
                first_name = None
            rendered_html = template_html.render(
                Context({'lead': ld, 'first_name': first_name}))
            m = ses.SES(from_email)
            m.send_mail(subject, ld.email, html=rendered_html,
                        reply_to_email='info@coverfox.com')
            print "SENT for : ", ld.email
            ld.mail_sent = True
            ld.mail_sent_data.append({
                'sent_on': datetime.datetime.now().strftime('%d-%m-%Y %H:%M'),
                'mail_type': ld.mail_type,
            })
            ld.save()

if __name__ == '__main__':
    f = open('/tmp/lead_mailer_lock', 'w')
    try:
        fcntl.lockf(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
    except IOError, e:
        if e.errno == errno.EAGAIN:
            sys.stderr.write('[%s] Script already running.\n' %
                             time.strftime('%c'))
            sys.exit(-1)
        raise
    send_mail_to_lead()
