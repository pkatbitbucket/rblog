import csv
import datetime
import boto
import putils

from django.conf import settings
from oauthlib.common import generate_token
from django.core.urlresolvers import reverse
from django.contrib.auth.models import Permission
from oauth2_provider.models import Application, AccessToken
from boto.s3.key import Key

from motor_product.models import Dealer, Dealership, \
    DealerDealershipMap
from core.models import Company, \
    User, Employee


def delete_everything():
    company = Company.objects.get(slug='mfc')
    ids = []
    for e in Employee.objects.filter(company=company).values_list('user_id'):
        uid = e[0]
        ids.append(uid)

    User.objects.filter(id__in=ids).delete()
    Employee.objects.filter(company=company).delete()
    Dealership.objects.filter(company=company).delete()
    Dealer.objects.filter(company=company).delete()
    company.delete()


def create_mfc_dealer_dealership():
    # create company
    company = Company.objects.filter(slug='mfc').first()
    if not company:
        company = Company.objects.create(
            name='Mahindra First Choice',
            slug='mfc'
        )
    dealership = Dealership.objects.filter(
        company__slug='mfc',
    ).first()
    if not dealership:
        dealership = Dealership.objects.create(
            company=company,
            is_approved=True
        )
    dealer = Dealer.objects.filter(company__slug='mfc').first()
    if not dealer:
        dealer = Dealer.objects.create(
            company=company,
            is_approved=True
        )
    dealer_dealership_map = DealerDealershipMap.objects.filter(
        dealer=dealer, dealership=dealership
    ).first()
    if not dealer_dealership_map:
        dealer_dealership_map = DealerDealershipMap.objects.create(
            dealer=dealer,
            dealership=dealership,
            is_approved=True
        )


def read_path():
    path = '/tmp/mfc-login.csv'
    conn = boto.connect_s3(
        settings.CFDEV_ACCESS_KEY,
        settings.CFDEV_SECRET_ACCESS_KEY
    )
    bucket = conn.get_bucket(settings.CFDEV_BUCKET)
    key = Key(bucket)
    key.key = 'dealer/mfc-login.csv'
    key.get_contents_to_filename(path)
    return path


def write_path():
    return '/tmp/mfc-login-final.csv'


def generate_url(token):
    uri = "{}?tk={}".format(reverse('dealer_autologin'), token)
    return "{}{}".format(settings.SITE_URL, uri)


def populate_mfc_dealer_employees():
    results = []
    with open(read_path(), 'r') as f:
        reader = csv.reader(f, delimiter=",")

        # create application
        c_u = User.objects.filter(
            emails__email='aash@coverfoxmail.com'
        ).first()
        application = Application.objects.filter(
            name='mfc'
        ).first()
        if not application:
            application = Application.objects.create(name='mfc', user=c_u)

        # iterating rows in csv to feed in the data
        for row in reader:
            emp_code = row[0]
            dealer_title = row[1]
            username = row[2]

            result = [emp_code, dealer_title, username]

            # create user
            user = User.objects.create(
                first_name=username,
                extra={'organization': dealer_title}
            )
            # get company
            company = Company.objects.filter(
                slug='mfc'
            ).first()
            # create employee
            employee = Employee.objects.create(
                user=user,
                company=company,
                employee_id=emp_code
            )
            # add permissions
            employee.user_permissions.add(
                Permission.objects.get(codename='is_dealer')
            )
            # create access token
            now = datetime.datetime.now()
            access_token = AccessToken.objects.create(
                user=user,
                application=application,
                expires=(datetime.datetime(year=now.year + 1, month=now.month, day=now.day)),
                token=generate_token()
            )
            # generate url
            url = generate_url(access_token.token)
            print url
            result.append(url)
            results.append(result)

    with open(write_path(), 'wb') as f:
        writer = csv.writer(f, delimiter=',')
        for line in results:
            writer.writerow(line)

    key = putils.put_file_to_s3(
        settings.CFDEV_ACCESS_KEY,
        settings.CFDEV_SECRET_ACCESS_KEY,
        settings.CFDEV_BUCKET,
        write_path(),
        'dealer/mfc-login-final.csv'
    )
    key.set_acl('public-read')
    url = key.generate_url(expires_in=0, query_auth=False)
    print url
    return url
