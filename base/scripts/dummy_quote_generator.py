import os
import django
import datetime
import csv

os.environ["DJANGO_SETTINGS_MODULE"] = "base.settings"
django.setup()

from motor_product.models import Quote

"""
quote = Quote.objects.create_dummy_quote(VehicleId="", )
quote.quote_id
"""
f = open('scripts/Database.csv')
# file = open("newfile.txt", "w")
csv_f = csv.reader(f)
ofile = open('scripts/Database2.csv', "wb")
writer = csv.writer(ofile, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
RTO_CODE = {
    'AN': {'Andaman & Nicobar Islands': 1},
    'AP': {'Andhra Pradesh': 2},
    'AR': {'Arunachal Pradesh': 3},
    'AS': {'Assam': 4},
    'BR': {'Bihar': 5},
    'CG': {'Chhattisgarh': 6},
    'CH': {'Chandigarh': 7},
    'DD': {'Daman and Diu': 8},
    'DL': {'Delhi': 9},
    'DN': {'Dadra & Nagar Haveli': 10},
    'GA': {'Goa': 11},
    'GJ': {'Gujarat': 12},
    'HP': {'Himachal Pradesh': 13},
    'HR': {'Haryana': 14},
    'JH': {'Jharkhand': 15},
    'JK': {'Jammu & Kashmir': 16},
    'KA': {'Karnataka': 17},
    'KL': {'Kerala': 18},
    'LD': {'Lakshadweep': 19},
    'MH': {'Maharashtra': 20},
    'ML': {'Meghalaya': 21},
    'MN': {'Manipur': 22},
    'MP': {'Madhya Pradesh': 23},
    'MZ': {'Mizoram': 24},
    'NL': {'Nagaland': 25},
    'OD': {'Odisha': 26},
    'PB': {'Punjab': 27},
    'PY': {'Pondicherry': 28},
    'RJ': {'Rajasthan': 29},
    'TN': {'Tamil Nadu': 30},
    'TS': {'Telangana': 31},
    'TR': {'Tripura': 32},
    'UK': {'Uttarakhand': 33},
    'UP': {'Uttar Pradesh': 34},
    'WB': {'West Bengal': 35}
}


i = 1
for row in csv_f:
    if i != 1:
        dt = datetime.datetime.strptime(row[16], "%d/%m/%Y")
        sale_date = dt.strftime("%d-%m-%Y")
        vehicleId = row[19]
        state_name = str(row[14])[0:2]
        reg_no = row[14]
        try:
            if type(RTO_CODE[state_name]) is dict:
                rto_number = [reg_no[0:2], reg_no[2:4], "0", "0"]
        except:
            continue
        else:
            quote = Quote.objects.create_dummy_quote(
                **{
                    'registrationDate': sale_date,
                    'vehicleId': vehicleId,
                    'registrationNumber[]': rto_number,
                    "isNewVehicle": "0",
                }
            )
            col = 'https://www.coverfox.com' + quote.url
            print row
            print col
            row.append(col)
            writer.writerow(row)
    i = i+1
ofile.close()
