"""
This script will get all users with error conditions and will remove them.

# TODO items
- Talk to rathi for merging requirements.
"""
import django
import sys
import os

sys.path.append('../')
import settings  # NOQA
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from core import models as core_models

"""
Questions
- We need to athink about form data. How payment requirement will attach form data
  and will be delivered to insurer.
  Assumption - Request.user.Activities.last().requirement.fd.
"""

"""
Possible ways to fix.
- Delete all duplicate users and merge their requirements and rerun populate policy
- Mark verified = False to all duplicate user contact details
- Delete all duplicate users and script will merge users.
"""

"""
Detected cases
- User logged in and proposal form email is same.
- User logged in and proposal form email is different.
"""

"""
Duplicate users are created with page_id 'policy'.
Because code was failing just after this line.
Analysis
Car - 351
Bike - 89
travel - 8
life - 0
health - 0
"""
for activity in core_models.Activity.objects.filter(page_id="policy"):
    # All the users associated with this are
    dup_user = activity.user
    # TODO: Figure out how to merge requirements.
    dup_email = dup_user.emails.filter(is_primary=True)
    # Do action.
