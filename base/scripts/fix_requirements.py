import django
import sys
import os
import datetime

sys.path.append('../')
import settings  # NOQA
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from core import models as core_models

f = open('/tmp/requirements_fix.log', 'a')

import sys
total_scripts = int(sys.argv[1])
current_script = int(sys.argv[2])


def fix_req(requirement):
    cid = int(requirement.pk)
    if cid % total_scripts != current_script:
        print current_script, cid, total_scripts
        return
    requirement.fix()
    print "Fixed %s" % requirement.pk, datetime.datetime.now()
    f.write("Fixed Requirement - %s | %s" % (requirement.pk, datetime.datetime.now()))


reqs = core_models.Requirement.objects.filter(fixed=False, campaigndata__isnull=False)


for r in reqs:
    fix_req(r)

f.close()
