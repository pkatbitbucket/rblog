fields = [["name"], ["address1", "address2"], ["landmark", "district"], ["city", "state"], ["pincode", "std_code"], ["phone1", "phone2"], ["fax1", "fax2"], [
    "email", "url"], ["lat", "lng"], ["no_of_beds", "no_of_major_ots"], ["no_of_ambulances", "registered_blood_bank"]]

form_str = ""
for flist in fields:
    if len(flist) > 5:
        raise Exception("No more than 5 fields grouped")
    span = 12 / len(flist)
    form_str += """
    <fieldset class="row-fluid">"""
    for f in flist:
        form_str += """
        <div class="control-group span%s">
            <label class="control-label">{{form.%s.label}}</label>
            <div class="controls">
                {{form.%s|attr:"class:span12"}}
            <p class="help-block">{{form.%s.help_text}}</p>
            </div>
        </div>""" % (span, f, f, f)
    form_str += """
    </fieldset>"""

print form_str
