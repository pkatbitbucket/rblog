import django
import sys
import os
import importlib

sys.path.append('../')
import settings  # NOQA
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from core.models import Policy, FormSection, FormData  # NOQA


def get_value(json_data, path=None):
    path = path.split(".")
    val = json_data
    path_len = len(path)
    i = 1
    for key in path:
        if i == path_len:
            val = val.get(key, "")
        else:
            val = val.get(key, {})

    if isinstance(val, dict) or isinstance(val, list):
        val = str('')

    if isinstance(val, int):
        val = str(val)

    return str(val.encode('utf-8'))


def add_section(form_data, form_name, fd):
    # Clean health data.
    splitted_form_name = form_name.split('.')
    module_path = '.'.join(splitted_form_name[:-1])
    module = importlib.import_module(module_path)
    form = getattr(module, splitted_form_name[-1])

    form_instance = form(data=form_data)
    if not form_instance.is_valid():
        print(form_instance.errors)
        print(form_data)
        print(policy.pk)

    cleaned_data = form_instance.cleaned_data
    new_dict = {}
    for key, val in cleaned_data.iteritems():
        if val:
            if isinstance(val, tuple):
                val = val[0]

            new_dict[key] = str(val.encode('utf-8'))

    if len(new_dict) == 1 and "role" in new_dict:
        return

    if "role" in new_dict:
        new_dict["role"] = new_dict["role"].replace(" ", "_").lower()

    if new_dict:
        fs = FormSection.objects.create(
            form=form_name,
            data=new_dict
        )
        fd.sections.add(fs)
        fd.save()


def create_insured_forms(proposal, fd):
    if "insuredArray" in proposal:
        for person in proposal["insuredArray"]:
            person_info = {
                "first_name": person.get("firstName", ""),
                "middle_name": person.get("middleName", ""),
                "last_name": person.get("lastName", ""),
                "age": person.get("age", ""),
                "date_of_birth": person.get("dateOfBirth", ""),
                "role": "insured_%s" % person.get("code", ""),
            }

            health_data = {}
            health_data["role"] = "insured_%s" % person.get("code", "")
            for codes in person.get("jokerArray", []):
                if codes.get("code", "") == "occupation":
                    person_info["occupation"] = codes.get("joker", {}).get("answer", "")
                elif codes.get("code", "") == "height":
                    health_data["height_val1"] = codes.get("joker", {}).get("answer1", "")
                    health_data["height_val2"] = codes.get("joker", {}).get("answer2", "")
                elif codes.get("code", "") == "weight":
                    health_data["weight"] = codes.get("joker", {}).get("answer", "")

            # Filter.
            new_p_info = {}
            for key, val in person_info.iteritems():
                if isinstance(val, tuple):
                    new_p_info[key] = val[0]

            new_health_data = {}
            for key, val in health_data.iteritems():
                if isinstance(val, tuple):
                    new_health_data[key] = val[0]

            add_section(new_p_info, "core.forms.Person", fd)
            add_section(new_health_data, "core.forms.HealthData", fd)
    else:
        # Get insured.
        insured_persons = proposal.get("insured", {})
        roles = insured_persons.keys()
        for role in roles:
            insured_json = insured_persons[role]
            person_info = {
                "first_name": get_value(insured_json, "first_name.value"),
                "last_name": get_value(insured_json, "last_name.value"),
                "birth_date": get_value(insured_json, "date_of_birth.value"),
                "occupation": get_value(insured_json, "occupation.display_value"),
                "role": "insured_%s" % role
            }
            add_section(person_info, "core.forms.Person", fd)

            height_val1 = get_value(insured_json, "height.value1")
            height_val2 = get_value(insured_json, "height.value2")

            health_data = {
                "height_val1": str(height_val1),
                "height_val2": str(height_val2),
                "weight": str(get_value(insured_json, "weight.value")),
                "role": "insured_%s" % role
            }
            add_section(health_data, "core.forms.HealthData", fd)


def insert_nominee_info(form_data, fd):
    if "nominee" in form_data:
        nominee = form_data.get("nominee", {})
        roles = nominee.keys()

        for role in roles:
            # Create nominee person model.
            role_nominee_data = nominee[role]
            person_data = {
                "first_name": get_value(role_nominee_data, "first_name.value"),
                "last_name": get_value(role_nominee_data, "last_name.value"),
                "birth_date": get_value(role_nominee_data, "date_of_birth.value"),
                "role": "nominee_%s" % role
            }
            add_section(person_data, "core.forms.Person", fd)

    if "insuredArray" in form_data:
        for insured_p in form_data["insuredArray"]:
            person_data = {
                "first_name": insured_p.get("nomineeFirstName", ""),
                "last_name": insured_p.get("nomineeLastName", ""),
                "birth_date": insured_p.get("nomineeDateOfBirth", ""),
                "role": "nominee_%s" % insured_p.get("nomineeRelation", "")
            }
            add_section(person_data, "core.forms.Person", fd)


def generate_form(policy):
    try:
        policy_transaction = policy.health_transaction
    except:
        print("Transaction not found for policy-id %s" % policy.pk)

    form_data = policy_transaction.insured_details_json
    fd = FormData.objects.create()

    insured_json = policy_transaction.insured_details_json
    if "insuredArray" in insured_json:
        person_info = {}
        proposer_data_dict = form_data.get("proposer_data", {})
        person_info["first_name"] = get_value(insured_json, "proposer.firstName")
        person_info["middle_name"] = get_value(insured_json, "proposer.middleName")
        person_info["last_name"] = get_value(insured_json, "proposer.lastName")
        person_info["email"] = get_value(insured_json, "proposer.email")
        person_info["mobile"] = get_value(insured_json, "proposer.mobile")
        person_info["landline"] = get_value(insured_json, "proposer.landline")
        person_info["gender"] = proposer_data_dict.get("gender", "")
        person_info["maritial_status"] = proposer_data_dict.get("marital_status", "")
        person_info["date_of_birth"] = proposer_data_dict.get("dob", "")
        person_info["role"] = "proposal"

        add_section(person_info, "core.forms.Person", fd)

        address_info = {}
        address_info["address_line1"] = get_value(insured_json, "proposer.address1")
        address_info["address_line2"] = get_value(insured_json, "proposer.address2")
        address_info["state"] = get_value(insured_json, "proposer.state")
        address_info["city"] = get_value(insured_json, "proposer.city")
        address_info["landmark"] = get_value(insured_json, "proposer.landmark")
        address_info["pincode"] = get_value(insured_json, "proposer.pincode")
        address_info["role"] = "communication"

        add_section(address_info, "core.forms.Address", fd)

        # -------------------------------------------------------------
        # Insured form.
        # -------------------------------------------------------------
        create_insured_forms(form_data, fd)
        # -------------------------------------------------------------
        # Nominee form.
        # -------------------------------------------------------------
        insert_nominee_info(insured_json, fd)

    if "form_data" in form_data:
        form_data = form_data["form_data"]
        # -------------------------------------------------------------
        # Proposal form.
        # -------------------------------------------------------------
        proposer = form_data.get("proposer", {})
        father_first_name = get_value(proposer, "personal.father_first_name.value")
        father_last_name = get_value(proposer, "personal.father_last_name.value")
        father_name = ""
        if father_first_name and father_last_name:
            father_name = "%s.%s" % (father_first_name, father_last_name)

        if father_first_name:
            father_name = father_first_name

        person_info = {}
        person_info["first_name"] = get_value(proposer, "personal.first_name.value")
        person_info["last_name"] = get_value(proposer, "personal.last_name.value")
        person_info["gender"] = get_value(proposer, "personal.gender.value")
        person_info["maritial_status"] = get_value(proposer, "personal.marital_status.value")
        person_info["birth_date"] = get_value(proposer, "personal.date_of_birth.value")
        person_info["occupation"] = get_value(proposer, "personal.occupation.value")
        person_info["mobile"] = get_value(proposer, "contact.mobile.value")
        person_info["father_name"] = father_name
        person_info["email"] = get_value(proposer, "contact.email.value")
        person_info["landline"] = get_value(proposer, "contact.landline.display_value")
        person_info["annual_income"] = get_value(proposer, "personal.annual_income.value")
        person_info["role"] = "proposal"

        add_section(person_info, "core.forms.Person", fd)
        # -------------------------------------------------------------
        # Insured form.
        # -------------------------------------------------------------
        create_insured_forms(form_data, fd)
        # -------------------------------------------------------------
        # Address form.
        # -------------------------------------------------------------
        address_info = {}
        address_info["address_line1"] = get_value(proposer, "address.address1.value")
        address_info["address_line2"] = get_value(proposer, "address.address2.value")
        address_info["state"] = get_value(proposer, "address.state.dependant_value")
        address_info["city"] = get_value(proposer, "address.city.value")
        address_info["pincode"] = get_value(proposer, "address.pincode.value")
        address_info["landmark"] = get_value(proposer, "address.landmark.value")
        address_info["role"] = "communication"

        address_info["state"] = get_value(proposer, "address.state.dependant_value")
        if not address_info["state"]:
            address_info["state"] = get_value(proposer, "address.state.value")

        address_info["city"] = get_value(proposer, "address.state.value1")
        if not address_info["city"]:
            address_info["city"] = get_value(proposer, "address.city.value")

        address_info["pincode"] = get_value(proposer, "address.pincode.value1")
        if not address_info["pincode"]:
            address_info["pincode"] = get_value(proposer, "address.city.value")

        add_section(address_info, "core.forms.Address", fd)
        # -------------------------------------------------------------
        # Health nominee.
        # -------------------------------------------------------------
        if "nominee" in form_data:
            insert_nominee_info(form_data, fd)

    requirement = policy.requirement_sold
    old_fd = requirement.fd
    requirement.fd = fd
    requirement.save()

    if old_fd:
        old_fd.delete()

    return True


for policy in Policy.objects.filter(
    requirement_sold__kind__product='health'
).iterator():
    response = generate_form(policy)
    if response:
        print(policy.pk)
