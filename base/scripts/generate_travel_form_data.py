import django
import sys
import os
import importlib

sys.path.append('../')
import settings  # NOQA
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from core.models import Policy, FormSection, FormData  # NOQA


def split_name(name):
    splitted_name = name.split(" ")

    first_name = ' '.join(splitted_name[:-1])
    last_name = ''.join(splitted_name[-1])

    if not first_name:
        first_name = name
        last_name = ""

    return (first_name, last_name)


def add_section(form_data, form_name, fd):
    # Clean health data.
    if form_name == 'core.forms.HealthQuestions':
        if form_data.get("has_kidney_desease", "") == "no":
            form_data["has_kidney_desease"] = "False"
        elif form_data.get("has_kidney_desease", "") == "yes":
            form_data["has_kidney_desease"] = "True"

        if form_data.get("has_liver_desease", "") == "no":
            form_data["has_liver_desease"] = "False"
        elif form_data.get("has_liver_desease", "") == "yes":
            form_data["has_liver_desease"] = "True"

        if form_data.get("travel_hospitalized", "") == "no":
            form_data["travel_hospitalized"] = "False"
        elif form_data.get("travel_hospitalized", "") == "yes":
            form_data["travel_hospitalized"] = "True"

        if form_data.get("other_diseases", "") == "no":
            form_data["other_diseases"] = "False"
        elif form_data.get("other_diseases", "") == "yes":
            form_data["other_diseases"] = "True"

    splitted_form_name = form_name.split('.')
    module_path = '.'.join(splitted_form_name[:-1])
    module = importlib.import_module(module_path)
    form = getattr(module, splitted_form_name[-1])

    form_instance = form(data=form_data)
    if not form_instance.is_valid():
        print(form_instance.errors)
        print(policy.pk)

    cleaned_data = form_instance.cleaned_data
    new_dict = {}
    for key, val in cleaned_data.iteritems():
        if val:
            new_dict[key] = str(val)

    if len(new_dict) == 1 and "role" in new_dict:
        return

    if "role" in new_dict:
        new_dict["role"] = new_dict["role"].replace(" ", "_").lower()

    if new_dict:
        fs = FormSection.objects.create(
            form=form_name,
            data=new_dict
        )

        fd.sections.add(fs)
        fd.save()


def is_dynamic_rel_type(form_data):
    relations = ["self", "spouse", "child1", "child2"]
    for rel in relations:
        if "%sFName" % rel in form_data:
            return True


def insert_relation_details(form_data):
    relations = ["self", "spouse", "child1", "child2"]
    for rel in relations:
        fname_key = "%sFName" % rel
        l_name_key = "%sLName" % rel
        is_sport_person_key = "%sbool_sports" % rel
        gender_key = "%sgender" % rel
        salutation_key = "%sSalutation" % rel
        birthday_key = "%sDOB" % rel
        assignee_name_key = "%sAName" % rel
        assignee_name_rel_key = "%sANameRelation" % rel
        nominee_name_key = "%sNName" % rel
        nominee_name_rel_key = "%sNNameRelation" % rel

        if fname_key in form_data:
            person_detail = {
                "first_name": form_data.get(fname_key, ''),
                "last_name": form_data.get(l_name_key, ''),
                "is_adventurous": form_data.get('selfbool_sports', ''),
                "email": form_data.get('email', ''),
                "gender": form_data.get(gender_key, ''),
                "salutation": form_data.get(salutation_key, ''),
                "is_sports_person": form_data.get(is_sport_person_key, ''),
                "is_nri": str(form_data.get("is_nri", "")),
                "birth_date": form_data.get(birthday_key, ''),
                "role": rel
            }

            if rel == "self":
                person_detail["birth_date"] = form_data.get('birth_date', '')
                person_detail["email"] = form_data.get('email', '')

            add_section(person_detail, 'core.forms.Person', fd)

            assignee_name = form_data.get(assignee_name_key, "")
            a_fname, a_lname = split_name(assignee_name)
            assignee_role = "assignee"
            if form_data.get(assignee_name_rel_key, ""):
                assignee_role += "_%s" % form_data.get(assignee_name_rel_key, "")

            person_detail = {
                "first_name": a_fname,
                "last_name": a_lname,
                "role": assignee_role
            }
            add_section(person_detail, "core.forms.Person", fd)

            nominee_name = form_data.get(nominee_name_key, "")
            nominee_f_name, nominee_l_name = split_name(nominee_name)
            nominee_role = "nominee"
            if form_data.get(nominee_name_rel_key, ""):
                nominee_role += "_%s" % form_data.get(nominee_name_rel_key, "")

            # Add nominee.
            nominee_detail = {
                "first_name": nominee_f_name,
                "last_name": nominee_l_name,
                "role": nominee_role
            }
            add_section(nominee_detail, "core.forms.Person", fd)

            # Health details.
            if "ped" in form_data:
                ped = form_data["ped"]
                if rel in ped:
                    health_que = {
                        "has_ped": ped.get(rel, ""),
                        "role": rel
                    }
                    add_section(health_que, "core.forms.HealthQuestions", fd)


for policy in Policy.objects.filter(
    requirement_sold__kind__product='travel'
).iterator():
    try:
        form_data = policy.travel_transaction.insured_details_json
    except:
        print("Policy transaction %s not found" % policy.pk)
        continue

    if policy.requirement_sold.fd:
        # if policy.requirement_sold.fd.sections.all().count():
        #     continue
        pass

    fd = FormData.objects.create()
    form_sections = []

    if is_dynamic_rel_type(form_data):
        # if policy.pk == 17820:
        #     import ipdb; ipdb.set_trace()
        insert_relation_details(form_data)

    if "members" in form_data:
        for member in form_data['members']:
            person = {
                'first_name': member.get('cust_first_name', ''),
                'last_name': member.get('cust_last_name', ''),
                'gender': member.get('cust_gender', ''),
                'mobile': member.get('com_mobile', ''),
                'email': member.get('com_email', ''),
                'birth_date': member.get('cust_dob', ''),
                'occupation': member.get('cust_occupation', ''),
                'salutation': member.get('salutation', ''),
                'is_nri': str(form_data.get('is_nri', "")),
                'passport_no': member.get('cust_passport', ''),
                'role': member.get('cust_relationship_with_primary', '').lower().replace(" ", "_")
            }

            if member.get('cust_relationship_with_primary', '').lower() == 'self':
                form_data['mobile'] = member.get('home_mobile', '')

            add_section(person, 'core.forms.Person', fd)

            # Nominee form.
            nominee_first_name, nominee_last_name = split_name(member.get('nominee_name', ''))
            person = {
                'first_name': nominee_first_name,
                'last_name': nominee_last_name,
                'role': 'nominee_%s' % member.get('cust_relationship_with_primary', '').lower().replace(" ", "_")
            }
            add_section(person, 'core.forms.Person', fd)

            # Insert health question answers.
            health_que = {}
            if "ped" in form_data:
                relation = member.get('cust_relationship_with_primary', '').lower()
                desease = form_data["ped"]
                if relation in desease:
                    desease = desease[relation]
                    if desease in ["None", "no"]:
                        health_que['has_other_diseases'] = False
                    elif desease in ["yes"]:
                        health_que['has_other_diseases'] = True

                    health_que["other_diseases"] = member.get("disease_name", '')
                    health_que["role"] = relation
                    add_section(health_que, 'core.forms.HealthQuestions', fd)

    if "member1FName" in form_data:
        person_detail = {
            "first_name": form_data.get('member1FName', ''),
            "middle_name": form_data.get('member1LName', ''),
            "last_name": form_data.get('member1LName', ''),
            "gender": form_data.get('member1gender', ''),
            "salutation": form_data.get('member1Salutation', ''),
            "passport_number": form_data.get('member1Passport', ''),
            "birth_date": form_data.get('member1DOB', ''),
            "role": "self"
        }

        if "member1PEDotherDetailsTravel" in form_data:
            health_que = {
                "has_other_diseases": form_data.get("member1PEDotherDetailsTravel", ""),
                "has_heart_diseases": form_data.get("member1PEDHealthDiseaseTravel", ""),
                "has_stroke_paralysis": form_data.get("member1PEDparalysisDetailsTravel", ""),
                "has_cancer": form_data.get("member1PEDcancerDetailsTravel", ""),
                "has_lever_desease": form_data.get("member1PEDliverDetailsTravel", ""),
                "has_kidney_desease": form_data.get("member1PEDkidneyDetailsTravel", "")
            }

        if "NomineeName" in form_data:
            # Nominee form.
            nominee_f_name, nominee_l_name = split_name(form_data.get("NomineeName", ""))
            person_detail = {
                "first_name": nominee_f_name,
                "last_name": nominee_l_name,
                "role": "self"
            }

    # -----------------------------------------------------------------
    travel_data = {
        'places': form_data.get('places', ''),
        "from_date": form_data.get('from_date', ''),
        "to_date": form_data.get('from_date', ''),
        'role': 'self'
    }
    add_section(travel_data, 'core.forms.TravelData', fd)

    # -----------------------------------------------------------------
    # Get address info.
    if "contact" in form_data:
        contact = form_data['contact']
        address_info = {}
        if "com_address_line_1" in contact:
            address_info['address_line1'] = contact.get('com_address_line_1', '')
            address_info['address_line2'] = contact.get('com_address_line_2', '')
            address_info['address_line3'] = contact.get('com_address_line_3', '')
            address_info['pincode'] = contact.get('com_pincode', '')
            address_info['city'] = contact.get('com_city', '')
            address_info['state'] = contact.get('com_state', '')
            address_info['role'] = 'communication'

            add_section(address_info, 'core.forms.Address', fd)

        address_info = {}
        if "home_address_line_1" in contact:
            address_info['address_line1'] = contact.get('home_address_line_1', '')
            address_info['address_line2'] = contact.get('home_address_line_2', '')
            address_info['address_line3'] = contact.get('home_address_line_3', '')
            address_info['pincode'] = contact.get('home_pincode', '')
            address_info['passport_no'] = contact.get('cust_passport', '')
            address_info['city'] = contact.get('home_city', '')
            address_info['state'] = contact.get('home_state', '')
            address_info['role'] = 'home'

            add_section(address_info, 'core.forms.Address', fd)

    address_info = {}
    if "ResAddr1" in form_data:
        address_info['address_line1'] = form_data.get('ResAddr1', '')
        address_info['address_line2'] = form_data.get('ResAddr2', '')
        address_info['address_line3'] = form_data.get('ResAddr3', '')
        address_info['address_line3'] = form_data.get('ResAddr3', '')
        address_info['passport_no'] = form_data.get('cust_passport', '')
        address_info['city'] = form_data.get('city', '')
        address_info['state'] = form_data.get('state', '')
        address_info['pincode'] = form_data.get('pincode', '')
        address_info['role'] = 'communication'

        add_section(address_info, 'core.forms.Address', fd)

    requirement = policy.requirement_sold
    requirement.fd = fd
    requirement.save()
    print(policy.pk)
'''
Ask about mobile should not be inside form.
disease_name key.
'''
