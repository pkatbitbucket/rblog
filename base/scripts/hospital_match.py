import os
import sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
from optparse import OptionParser
from collections import defaultdict
import health_product.health_utils as hutils

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))


from activity.models import Data

filename = "/home/rane/Desktop/InsuranceCompanies/BhartiAxa/bharti_axa_Documents/PHS_south_and_west.csv"
fline = open(filename).read().split("\n")

fwrite = open("/tmp/matched.csv", 'w')

by_city = defaultdict(list)
for line in fline[1:]:
    if not line:
        continue
    print line
    hlist = None
    name, address, state, city, zone = line.split("|")
    if not by_city.get(city.lower().strip()):
        hlist = Data.objects.filter(city=city.lower())
        by_city[city.lower().strip()] = hlist
        if not hlist:
            raise Exception("ERROR: no match for city: %s" % city)

    name_parts = hutils.normalize_hospital_name(name).split(" ")
    matching_list = None
    print "Name parts", name_parts
    for np in name_parts:
        if len(np) == 1:
            matching_list = by_city[city.lower().strip()].filter(
                name__icontains=" ".join(name_parts))
            break
        else:
            if not matching_list:
                matching_list = by_city[
                    city.lower().strip()].filter(name__icontains=np)
            else:
                matching_list = matching_list.filter(name__icontains=np)

    for m in matching_list:
        try:
            fwrite.write("%s | %s | %s | %s | %s" %
                         (name, address, city, state, m.raw))
            fwrite.write("\n")
        except UnicodeEncodeError:
            pass
