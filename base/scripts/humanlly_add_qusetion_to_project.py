import export_settings  # NOQA
from hrms.models import ReviewSection, ReviewQuestion

company = 'Glitterbug'
project_section = ReviewSection.objects.filter(
    name="Projects", period__company__name=company
).first()

questions = ReviewQuestion.objects.filter(
    section__period__company__name=company
)

for question in questions:
    question.section = project_section
    print("Updated question (%s)" % question.text)
    question.save()
