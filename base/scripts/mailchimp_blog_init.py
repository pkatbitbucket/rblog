import sys
import time
import datetime
import os

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from customdb.customquery import sqltojson, sqltodict
from django.core.mail import send_mail
import thread
import threading
from datetime import timedelta
from django.utils.encoding import smart_str, smart_unicode


from blog.models import *


today = datetime.datetime.today()
today.isoweekday()

day_dict = {
    1: 'Mon',
    2: 'Tue',
    3: 'Wed',
    4: 'Thur',
    5: 'Fri',
    6: 'Mon',
    7: 'Tue',
}

c = Campaign.objects.get(title='BLOG')

for s in c.subscriber_set.all():
    if not BlogScribe.objects.filter(sub=s):
        bs = BlogScribe(sub=s)
        bs.save()
