import sys
import time
import datetime
import os
import hashlib
import string
import random
import thread
import threading

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from customdb.customquery import sqltojson, sqltodict
from django.conf import settings
from django.core.mail import send_mail
from datetime import timedelta
from django.utils.encoding import smart_str, smart_unicode
from utils import blog_subscribe_logger
import re

from chimp_utils import *
from blog.models import *
from core.models import *

send_error_mail = False

day_dict = {
    1: 'Mon',
    2: 'Tue',
    3: 'Wed',
    4: 'Thur',
    5: 'Fri',
    6: 'Mon',
    7: 'Tue',
}

today = datetime.datetime.today()


campaign = Campaign.objects.get(title='BLOG')
for u in User.objects.all():
    if not Subscriber.objects.filter(email=u.email):
        sub = Subscriber(
            name=u.get_full_name(),
            email=u.email,
            code=hashlib.md5("%s%s" % (datetime.datetime.now().strftime(
                '%d%m%Y%H%M%S'), str(random.random()))).hexdigest(),
        )
        sub.save()
        sub.campaign.add(campaign)
        sub.save()


for s in campaign.subscriber_set.all():
    if not BlogScribe.objects.filter(sub=s):
        bs = BlogScribe(sub=s)
        bs.save()


group = day_dict[today.isoweekday()]
grouping = 'Blog Groupings'
random_string = ''.join(random.choice(
    string.ascii_uppercase + string.digits) for x in range(6))
batch_id = hashlib.md5(today.strftime(
    "%I%M%S") + random_string).hexdigest() + os.path.splitext(random_string)[1]

batch = []

if BlogScribe.objects.filter(group=''):
    for bs in BlogScribe.objects.filter(group=''):
        bs.group = group
        bs.grouping = grouping
        bs.batch = batch_id
        bs.save()
        b_dict = {
            'email': {'email': bs.sub.email},
            'merge_vars': {
                'FNAME': bs.sub.name,
                'groupings': [
                    {
                        'name': grouping,
                        'groups': [group],
                    },
                ]
            }
        }
        batch.append(b_dict)

    try:
        res = list_batch_subscribe(settings.BLOG_SUB_LIST_ID, batch)
    except Exception, e:
        mail_text = 'A mailchimp error occurred: %s' % str(e)
        send_error_mail = True
        res = str(e)

    blog_subscribe_logger.info("====\nBatch: %s \nResponse: %s \nTIME:%s\n\n" % (
        batch_id, res, datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))

    # print res
    if res['errors']:
        mail_text = 'Error in batch response: \n%s', res
        send_error_mail = True

    if send_error_mail:
        from_mail = "info@coverfox.com"
        mail_title = "Error occured while sending mail"
        msg = EmailMultiAlternatives(mail_title, mail_text, from_mail, [
                                     'aniket@coverfox.com', 'dev@coverfox.com'])
        msg.attach_alternative(mail_html, "text/html")
        thread.start_new_thread(msg.send, ())
