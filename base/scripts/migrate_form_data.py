import sys
import re
import os
import envdir
import json
import xlsxwriter

envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import django

from django.db import transaction as db_transaction
sys.path.append('../')
import settings  # NOQA
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

# Get all data from transactions.
from core import models as core_models
from core import forms as core_forms


i = 0
dobl = []

workbook = xlsxwriter.Workbook('form_errors.xlsx')
worksheet = workbook.add_worksheet()
# Widen the first column to make the text clearer.
worksheet.set_column('A:A', 20)
worksheet.set_column('B:B', 20)
worksheet.set_column('C:C')

excel_row = 2
# for motor_transaction in motor_transaction.objects.all().iterator():
for policy in core_models.Policy.objects.all().iterator():
    # Check if form data already exists or not.
    if policy.requirement_sold.fd:
        if policy.requirement_sold.fd.sections.all().count():
            continue

    transaction = None
    if policy.requirement_sold.kind.product in ['motor', 'bike']:
        transaction = policy.motor_transaction
    elif policy.requirement_sold.kind.product == 'health':
        transaction = policy.health_transaction
    elif policy.requirement_sold.kind.product == 'travel':
        transaction = policy.travel_transaction
    elif policy.requirement_sold.kind.product == 'home':
        transaction = policy.home_transaction

    with db_transaction.atomic():
        if policy.requirement_sold.kind.product in ['motor', 'bike']:
            raw_data = transaction.raw
            user_form_data = raw_data.get('user_form_details', {})
            if not user_form_data:
                i += 1
                continue

            # Get person information.
            first_name = user_form_data.get('cust_first_name', '')
            last_name = user_form_data.get('cust_last_name', '')
            cust_email = user_form_data.get('cust_email', '')
            cust_phone = user_form_data.get('cust_phone', '')
            gender = user_form_data.get('cust_gender', '')
            cust_marital_status = user_form_data.get('cust_marital_status', '')
            father_fname = user_form_data.get('father_first_name', '')
            father_lname = user_form_data.get('father_last_name', '')
            dob = user_form_data.get('cust_dob', '')

            if not dob:
                day = user_form_data.get('cust_dob_dd', '')
                month = user_form_data.get('cust_dob_mm', '')
                year = user_form_data.get('cust_dob_yyyy', '')
                if day and month and year:
                    dob = "%s-%s-%s" % (day, month, year)

            if '-' not in dob and '/' not in dob and " " not in dob and dob:
                day = dob[:2]
                month = dob[2:4]
                year = dob[4:]
                dob = "%s-%s-%s" % (day, month, year)

            if '-' in dob or '/' in dob:
                if " " in dob:
                    dob = dob.replace(' ', '')

            occupation = user_form_data.get('cust_occupation', '')
            pan_no = user_form_data.get('cust_pan', '')

            # Create form.
            form_path = 'core.forms.Person'
            person_form = core_forms.Person(
                data={
                    "first_name": first_name,
                    "middle_name": '',
                    "last_name": last_name,
                    "mobile": cust_phone,
                    "email": cust_email,
                    "landline": '',
                    "gender": gender,
                    "birth_date": dob,
                    "maritial_status": cust_marital_status,
                    "father_name": "%s %s" % (father_fname, father_lname),
                    "occupation": occupation,
                    "pan_no": pan_no,
                    # "annual_income_upper": 0,
                    # "annual_income_bottom": 0,
                    "role": "self"
                }
            )

            if not person_form.is_valid():
                print(person_form.errors)
                worksheet.write(excel_row, 0, transaction.transaction_id)
                worksheet.write(excel_row, 1, json.dumps(user_form_data))
                worksheet.write(excel_row, 2, json.dumps(person_form.errors))
                excel_row += 1
                continue

            # TODO: Missing annual income.
            nominee_name = user_form_data.get('nominee_name', '')
            nominee_age = str(user_form_data.get('nominee_age', ''))
            nominee_relationship = user_form_data.get('nominee_relationship', '')

            splitted_nominee_name = nominee_name.split(" ")

            nominee_form_data = {
                'first_name': ''.join(splitted_nominee_name[:-1]),
                'last_name': ''.join(splitted_nominee_name[:1]),
                'role': 'nominee'
            }

            if nominee_age:
                if isinstance(nominee_age, str):
                    if re.match(r'^[0-9]{1,3}$', nominee_age.replace(" ", "")):
                        nominee_form_data['age'] = nominee_age.replace(" ", "")
                    else:
                        worksheet.write(excel_row, 0, transaction.transaction_id)
                        worksheet.write(excel_row, 1, 'Invalid nominee age')
                        worksheet.write(excel_row, 2, nominee_age)
                elif isinstance(nominee_age, int):
                    nominee_form_data['age'] = nominee_age

            nominee_form = core_forms.Person(
                data=nominee_form_data
            )

            # Vehicle RTO.
            registration_number = user_form_data.get('vehicle_reg_no', '')
            vehicle_rto = user_form_data.get('vehicle_rto', '')
            engine_number = user_form_data.get('vehicle_engine_no', '')
            chassis_number = user_form_data.get('vehicle_chassis_no', '')
            vehicle_registration_dt = user_form_data.get('vehicle_reg_date', '')
            car_financier = user_form_data.get('car-financier-text', '')
            if not car_financier:
                car_financier = user_form_data.get('car-financier', '')

            # Past policy information.
            past_policy_number = user_form_data.get('past_policy_number', '')
            past_policy_start_date = user_form_data.get("past_policy_start_date", '')
            past_policy_end_date = user_form_data.get('past_policy_end_date', '')
            if not past_policy_end_date:
                day = user_form_data.get('past_policy_end_date_dd', '')
                month = user_form_data.get('past_policy_end_date_mm', '')
                year = user_form_data.get('past_policy_end_date_yyyy', '')

            past_policy_cover_type = user_form_data.get('past_policy_cover_type', '')
            past_policy_insurer = str(user_form_data.get('past_policy_insurer', ''))
            past_policy_insurer_city = user_form_data.get('past_policy_insurer_city', '')

            vehicle_master = None
            if transaction.quote.vehicle:
                vehicle_master = str(transaction.quote.vehicle.pk)

            vehicle_detail_form_data = {
                'registration_number': registration_number,
                'engine_number': engine_number,
                'chassis_number': chassis_number,
                'vehicle_master': vehicle_master,
                'vehicle_registration_date': vehicle_registration_dt,
                'loan_provider': car_financier,
                'past_policy_number': past_policy_number,
                'past_policy_start_date': past_policy_start_date,
                'past_policy_end_date': past_policy_end_date,
                'past_policy_cover_type': past_policy_cover_type,
                'past_policy_insurer': past_policy_insurer,
                'past_policy_insurer_city': past_policy_insurer_city,
                'role': 'car'
            }

            vehicle_detail_form = None
            vehicle_form_data = None
            if transaction.vehicle_type == "Private Car":
                car_detail_form = core_forms.CarDetail(
                    data=vehicle_detail_form_data
                )

                if not car_detail_form.is_valid():
                    print(car_detail_form.errors)
                    worksheet.write(excel_row, 0, transaction.transaction_id)
                    worksheet.write(excel_row, 1, json.dumps(user_form_data))
                    worksheet.write(excel_row, 2, json.dumps(car_detail_form.errors))
                    i += 1
                    continue

                vehicle_detail_form = 'core.forms.CarDetail'
                vehicle_form_data = car_detail_form.data

            elif transaction.vehicle_type == "Twowheeler":
                bike_detail_form = core_forms.BikeDetail(
                    data=vehicle_detail_form_data
                )

                if not bike_detail_form.is_valid():
                    print(bike_detail_form.errors)
                    worksheet.write(excel_row, 0, transaction.transaction_id)
                    worksheet.write(excel_row, 1, json.dumps(bike_detail_form.data))
                    worksheet.write(excel_row, 2, json.dumps(bike_detail_form.errors))
                    excel_row += 1
                    continue

                vehicle_detail_form = 'core.forms.BikeDetail'
                vehicle_form_data = bike_detail_form.data

            # Add address.
            building_no = user_form_data.get('add_building_name', '')
            add_house_no = user_form_data.get('add_house_no')
            street = user_form_data.get('add_street_name', '')
            area = user_form_data.get('')
            state = user_form_data.get('add_state', '')
            pincode = user_form_data.get('add_pincode', '')
            city = user_form_data.get('add_city', '')

            post_office = user_form_data.get('add_post_office', '')
            landmark = user_form_data.get('add_landmark', '')

            district = user_form_data.get('add_district', '')
            city = user_form_data.get('add_city', '')

            # Reg address
            same_addr = user_form_data.get('add-same', 'false')
            reg_address_form = None
            if same_addr == 'false':
                reg_building_no = user_form_data.get('reg_add_building_name', '')
                reg_add_house_no = user_form_data.get('reg_add_house_no', '')
                reg_street = user_form_data.get('reg_add_street_name', '')
                reg_pincode = user_form_data.get('reg_add_pincode', '')
                reg_landmark = user_form_data.get('reg_add_landmark', '')
                reg_district = user_form_data.get('reg_add_district', '')
                reg_city = user_form_data.get('reg_add_city', '')
                state = user_form_data.get('reg_add_state', '')

                address_form_data = {
                    "building_no": reg_building_no,
                    "house_no": reg_add_house_no,
                    "street": reg_street,
                    "pincode": reg_pincode,
                    "landmark": reg_landmark,
                    "district": reg_district,
                    "city": reg_city,
                    "type": "reg"
                }
                # Create two copies.
                reg_address_form = core_forms.Address(
                    data=address_form_data
                )
                if not reg_address_form.is_valid():
                    print(reg_address_form.errors)
                    worksheet.write(excel_row, 0, transaction.transaction_id)
                    worksheet.write(excel_row, 1, json.dumps(reg_address_form.data))
                    worksheet.write(excel_row, 2, json.dumps(reg_address_form.errors))
                    excel_row += 1
                    continue

            address_form_data = {
                "reg_building_no": building_no,
                "reg_add_house_no": add_house_no,
                "reg_street": street,
                "reg_pincode": pincode,
                "reg_landmark": landmark,
                "reg_district": district,
                "city": city,
                "type": "main"
            }
            address_form = core_forms.Address(
                data=address_form_data
            )
            if not address_form.is_valid():
                print(address_form.errors)
                worksheet.write(excel_row, 0, transaction.transaction_id)
                worksheet.write(excel_row, 1, json.dumps(address_form.data))
                worksheet.write(excel_row, 2, json.dumps(address_form.errors))
                excel_row += 1
                continue

            form_data = core_models.FormData()
            form_data.save()
            # Prepare form data.
            person_form_instance = core_models.FormSection.objects.create(
                form='core.forms.Person',
                data=person_form.data
            )
            form_data.sections.add(person_form_instance)

            nominee_form_instance = core_models.FormSection.objects.create(
                form='core.forms.Person',
                data=nominee_form.data
            )
            form_data.sections.add(nominee_form_instance)

            if vehicle_detail_form:
                vehicle_form_instance = core_models.FormSection.objects.create(
                    form=vehicle_detail_form,
                    data=vehicle_form_data
                )
                form_data.sections.add(vehicle_form_instance)

            address_detail_instance = core_models.FormSection.objects.create(
                form='core.forms.Address',
                data=address_form.data
            )
            form_data.sections.add(address_detail_instance)

            if reg_address_form:
                reg_address_form_instance = core_models.FormSection.objects.create(
                    form='core.forms.Address',
                    data=reg_address_form.data
                )
                form_data.sections.add(reg_address_form_instance)

            form_data.user = policy.user
            form_data.save()

            requirement = policy.requirement_sold
            requirement.fd = form_data
            requirement.save()
            print(requirement.pk)
