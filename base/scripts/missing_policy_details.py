import django
import sys
import os

sys.path.append('../')
import settings  # NOQA
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

import xlsxwriter  # NOQA

from core.models import Policy  # NOQA
from account_manager import utils  # NOQA


class XlsxWriter():
    def __init__(self, filepath):
        self.workbook = xlsxwriter.Workbook(filepath)

    def __enter__(self):
        return self.workbook

    def __exit__(self, exc_type, exc_value, traceback):
        self.workbook.close()
        print("Workbook closed")


with XlsxWriter('/tmp/missing_fields.xls') as workbook:
    worksheet = workbook.add_worksheet()
    # Widen the first column to make the text clearer.
    worksheet.set_column('A:A', 20)
    worksheet.set_column('B:B', 20)
    worksheet.set_column('C:C')

    excel_row = 0
    worksheet.write(excel_row, 0, "Policy ID")
    worksheet.write(excel_row, 1, "Missing data")
    excel_row += 1

    for policy in Policy.objects.all():
        # Get policy details.
        policy_data = utils.get_policy_details(policy)
        not_found = []
        for key, value in policy_data.iteritems():
            if not value:
                not_found.append(key)

        worksheet.write(excel_row, 0, policy.pk)
        worksheet.write(excel_row, 1, ','.join(not_found))
        excel_row += 1
        print("WRITEN")
