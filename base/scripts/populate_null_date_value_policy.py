from __future__ import unicode_literals
import sys
import os
import datetime
import django


sys.path.append('../')  # NOQA
import settings  # NOQA
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')  # NOQA
django.setup()  # NOQA


from django.contrib.auth.hashers import UNUSABLE_PASSWORD_PREFIX
from django.contrib.auth.hashers import UNUSABLE_PASSWORD_SUFFIX_LENGTH
from django.utils.crypto import get_random_string
from django.contrib.contenttypes.models import ContentType

from account_manager.utils import transaction_field_mapping
from utils import convert_datetime_to_timezone

from core import models as core_models
from account_manager import models as account_manager
from motor_product import models as motor_models
from health_product import models as health_models
from travel_product import models as travel_models
from travel_product.forms import QuoteForm
from putils import add_years, clean_email_list


def populate_motor_policies():
    pol_count = 0

    def create_user(transaction):
        mapping = transaction_field_mapping(transaction)
        email = mapping.get('email', '')
        emails = clean_email_list(email)
        if emails:
            email = emails[0]
            if mapping.get('middle_name', '') is None:
                mapping['middle_name'] = ''

            try:
                return core_models.User.objects.get(email=email)
            except core_models.User.DoesNotExist:
                return core_models.User.objects.create(
                    email=mapping.get('email'),
                    username=mapping.get('email', ''),
                    password=UNUSABLE_PASSWORD_PREFIX + get_random_string(UNUSABLE_PASSWORD_SUFFIX_LENGTH),
                    first_name=mapping.get('first_name', ''),
                    middle_name=mapping.get('middle_name', ''),
                    last_name=mapping.get('last_name', ''),
                    is_active=False,
                    date_joined=datetime.datetime.now(),
                    created_on=datetime.datetime.now(),
                    updated_on=datetime.datetime.now(),
                    last_login=datetime.datetime.now(),
                )

    new_motor_requirement, _ = core_models.RequirementKind.objects.get_or_create(product='motor', kind='new_policy', sub_kind='')
    transactions = motor_models.Transaction.objects.exclude(policy_document='').filter(
        policy_start_date__isnull=True, policy_end_date__isnull=True, status__in=['COMPLETED', 'MANUAL COMPLETED']
    )
    for transaction in transactions.iterator():
        # Check if policy already exist or not.
        if transaction.policy:
            continue

        if transaction.requirement and transaction.requirement.user:
            requirement = transaction.requirement
        else:
            try:
                ut = account_manager.UserTransaction.objects.get(
                    object_id=transaction.id, content_type=ContentType.objects.get_for_model(transaction)
                )
            except account_manager.UserTransaction.DoesNotExist:
                user = create_user(transaction)
                if user is None:
                    print requirement.id, requirement.user.id
                    continue
            except account_manager.UserTransaction.MultipleObjectsReturned:
                user = account_manager.UserTransaction.objects.filter(
                    object_id=transaction.id, content_type=ContentType.objects.get_for_model(transaction)
                ).last().user
            else:
                user = ut.user
            if transaction.requirement:
                requirement = transaction.requirement
                requirement.user = user
                requirement.save()
            else:
                requirement = core_models.Requirement.objects.create(
                    kind=new_motor_requirement,
                    user=user
                )
        start_date = convert_datetime_to_timezone(transaction.policy_start_date)
        end_date = convert_datetime_to_timezone(transaction.policy_end_date)
        policy = core_models.Policy.objects.create(
            name=os.path.splitext(transaction.policy_document.name)[0].split(os.path.sep)[-1],
            document=transaction.policy_document,
            app=transaction._meta.app_label,
            status='valid',
            user=requirement.user,
            requirement_sold=requirement,
            policy_number=transaction.policy_number,
            issue_date=start_date,
            expiry_date=end_date,
        )
        transaction.policy = policy
        transaction.save()
        requirement.policy = policy
        requirement.save()
        print(policy.pk)
        pol_count += 1

    return pol_count


def populate_health_policies():
    pol_count = 0

    def create_user(transaction):
        mapping = transaction_field_mapping(transaction)
        email = mapping.get('email', '')
        emails = clean_email_list(email)
        if emails:
            if mapping.get('middle_name', '') is None:
                mapping['middle_name'] = ''

            email = emails[0]
            try:
                return core_models.User.objects.get(email=email)
            except core_models.User.DoesNotExist:
                return core_models.User.objects.create(
                    email=mapping.get('email'),
                    username=mapping.get('email', ''),
                    password=UNUSABLE_PASSWORD_PREFIX + get_random_string(UNUSABLE_PASSWORD_SUFFIX_LENGTH),
                    first_name=mapping.get('first_name', ''),
                    middle_name=mapping.get('middle_name', ''),
                    last_name=mapping.get('last_name', ''),
                    is_active=False,
                    date_joined=datetime.datetime.now(),
                    created_on=datetime.datetime.now(),
                    updated_on=datetime.datetime.now(),
                    last_login=datetime.datetime.now(),
                )

    new_health_requirement, _ = core_models.RequirementKind.objects.get_or_create(product='health', kind='new_policy', sub_kind='')
    transactions = health_models.Transaction.objects.exclude(policy_document='').filter(
        policy_start_date__isnull=True, policy_end_date__isnull=True, status__in=['COMPLETED', 'MANUAL COMPLETED']
    )
    for transaction in transactions.iterator():
        if transaction.policy:
            continue

        try:
            ut = account_manager.UserTransaction.objects.get(
                object_id=transaction.id, content_type=ContentType.objects.get_for_model(transaction)
            )
        except account_manager.UserTransaction.DoesNotExist:
            user = create_user(transaction)
            if user is None:
                continue
        except account_manager.UserTransaction.MultipleObjectsReturned:
            user = account_manager.UserTransaction.objects.filter(
                object_id=transaction.id, content_type=ContentType.objects.get_for_model(transaction)
            ).last().user
        else:
            user = ut.user
        requirement = core_models.Requirement.objects.create(
            kind=new_health_requirement,
            user=user
        )
        start_date = convert_datetime_to_timezone(transaction.policy_start_date)
        end_date = convert_datetime_to_timezone(transaction.policy_end_date)
        policy = core_models.Policy.objects.create(
            name=os.path.splitext(transaction.policy_document.name)[0].split(os.path.sep)[-1],
            document=transaction.policy_document,
            app=transaction._meta.app_label,
            status='valid',
            user=user,
            requirement_sold=requirement,
            policy_number=transaction.policy_number,
            issue_date=start_date,
            expiry_date=end_date,
        )
        transaction.policy = policy
        transaction.save()
        requirement.policy = policy
        requirement.save()
        pol_count += 1

    return pol_count


def populate_travel_policies():
    pol_count = 0
    skiped = 0

    def create_user(transaction):
        mapping = transaction_field_mapping(transaction)
        email = mapping.get('email', '')
        emails = clean_email_list(email)
        if emails:
            email = emails[0]
            if mapping.get('middle_name', '') is None:
                mapping['middle_name'] = ''

            try:
                return core_models.User.objects.get(email=email)
            except core_models.User.DoesNotExist:
                return core_models.User.objects.create(
                    email=mapping.get('email'),
                    username=mapping.get('email', ''),
                    password=UNUSABLE_PASSWORD_PREFIX + get_random_string(UNUSABLE_PASSWORD_SUFFIX_LENGTH),
                    first_name=mapping.get('first_name', ''),
                    middle_name=mapping.get('middle_name', ''),
                    last_name=mapping.get('last_name', ''),
                    is_active=False,
                    date_joined=datetime.datetime.now(),
                    created_on=datetime.datetime.now(),
                    updated_on=datetime.datetime.now(),
                    last_login=datetime.datetime.now(),
                )

    new_travel_requirement, _ = core_models.RequirementKind.objects.get_or_create(product='travel', kind='new_policy', sub_kind='')
    transactions = travel_models.Transaction.objects.exclude(policy_document='').filter(status='COMPLETED')

    def cleaned_data(activity):
        form = QuoteForm(activity.data['quote_form']['data'])
        form.is_valid()
        return form.cleaned_data

    for transaction in transactions.iterator():
        if transaction.policy:
            continue

        try:
            data = cleaned_data(transaction.activity)
        except:
            skiped += 1
            continue

        try:
            ut = account_manager.UserTransaction.objects.get(
                object_id=transaction.id, content_type=ContentType.objects.get_for_model(transaction)
            )
        except account_manager.UserTransaction.DoesNotExist:
            user = create_user(transaction)
            if user is None:
                continue
        except account_manager.UserTransaction.MultipleObjectsReturned:
            user = account_manager.UserTransaction.objects.filter(
                object_id=transaction.id, content_type=ContentType.objects.get_for_model(transaction)
            ).last().user
        else:
            user = ut.user
        requirement = core_models.Requirement.objects.create(
            kind=new_travel_requirement,
            user=user
        )

        start_date = convert_datetime_to_timezone(data.get('from_date'))
        end_date = convert_datetime_to_timezone(data.get('to_date'))
        if not end_date and data.get('type') == 'multi' and start_date:
            end_date = add_years(start_date, 1) - datetime.timedelta(days=1)

        policy = core_models.Policy.objects.create(
            name=os.path.splitext(transaction.policy_document.name)[0].split(os.path.sep)[-1],
            document=transaction.policy_document,
            app=transaction._meta.app_label,
            status='valid',
            user=user,
            requirement_sold=requirement,
            policy_number=transaction.policy_number,
            issue_date=start_date,
            expiry_date=end_date,
        )
        transaction.policy = policy
        transaction.save(read_only=False)
        requirement.policy = policy
        requirement.save()

        pol_count += 1

    return (pol_count, skiped)


print("Populating motor policies")
print("Populated %s motor policies" % populate_motor_policies())
print("Populating health policies")
print("Populated %s health policies" % populate_health_policies())
print("Populating travel policies")
c, skipped = populate_travel_policies()
print("Populated=%s Skiped=%s travel policies" % (c, skipped))
