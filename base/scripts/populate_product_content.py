import sys
import time
import datetime
import os
import csv

import envdir
envdir.open()
sys.path.extend(os.environ["PYTHONPATH"].split(os.pathsep))

from customdb.customquery import sqltojson, sqltodict
from django.core.mail import send_mail
import thread
import threading
from datetime import timedelta
from django.utils.encoding import smart_str, smart_unicode

import coverfox_com
from core.models import User
from review.models import *
from review.forms import *
from review.rev_utils import *

# For P001
content_dict = {
    'PTA1': "<p>Personal Accident Cover - Simple product. Superbly effective. Surprisingly cheap.</p>"
    "<p>This plan is sold by General Insurance companies, not Life Insurance companies.</p>",

    'PTA2': "<p>Personal Accident Cover - Simple product. Effective and cheap.</p>",

    'PTB1': "<p>Covers accidental death, disability and in some case also hospitalization for accidental injuries.</p>"
    "<p>Does not cover hospitalization expenses for illnesses.</p>",

    'PTB2': "<p>Covers broken bones (fractures), accidental death, disability and in some case also hospitalization "
    "for accidental injuries.</p>"
    "<p>Does not cover hospitalization expenses for illnesses.</p>",

    'PTC1': "<p>Basic personal accident products normally only cover permanent or temporary disability.</p>"
    "<p>Look for a plan which either covers, or also has an option to add a cover for treatment "
    "for accidental injuries."
    "</p>",

    'PTC2' : """<p>Look for a plan which covers fractures requiring hospitalization (called a broken bones benefit). </p>""",

    'PTD1': "<p>At %s an accident product is great to have. But it cannot be your only plan for health insurance.</p>"
             "<p>You need regular basic health insurance separately. You may ask - Why both? Well, for two reasons -"
             "<ul>"
             "<li>Accident plan covers only accidental injuries, not ailments</li>"
             "<li>Health Insurance plans are much much more expensive</li>"
             "</ul>"
             "</p>"
             "<p>When you split your coverage, it works out about 40%% cheaper. So a mix and match makes up for "
             "perfect protection at a low cost.</p>",

    'PTD2': "<p>You need enough cover to take care of hospitalization for fractures and small accidents, so a "
             "cover of %s would suffice.</p>",

}

prod = Product.objects.get(code='P001')
if not prod.content:
    prod.content = content_dict
else:
    prod.content.update(**content_dict)
prod.save()


# For P005
content_dict = {
    'PTA1': "<p>Commonly known as 'Mediclaim' or Health Insurance.</p>"
    "<p>The one in which you get a cashless card. The one you absolutely need to have.</p>"
    "<p>General Insurance companies sell this cover, not Life Insurance companies.</p>"
    "</p>",

    'PTA2': "<p>Commonly known as 'Mediclaim' or Health Insurance. The one in which you get a cashless card. "
            "The one you absolutely need to have.</p>"
            "<p>General Insurance companies sell this cover, not Life Insurance companies.</p>"
            "</p>",


    'PTB1': "<p>Covers all expenses when hospitalized for more than 24 hours - treatment/surgery, room charges, "
    "medicines, etc.</p>"
    "<p>Covers cost of delivery, after a few years of buying the policy.</p>",

    'PTB2': "<p>Covers all expenses when hospitalized - treatment /surgery , room charges, medicines etc.</p>",

    'PTC1': "<p>"
    "<ul>"
    "<li>Floater Cover (Covers the family under one policy)</li>"
    "<li>Maternity benefit (Covers delivery expenses up to a certain limit)</li>"
    "<li>No limit or a high limit on your hospital room charges (will help you get a private room in a "
    "good hospital)</li>"
    "</ul>"
    "</p>",

    'PTC2': "<p>"
    "<ul>"
    "<li>Floater Cover (Covers the family under one policy)</li>"
    "<li>No limit or a high limit on your hospital room charges (will help you get a private room in a "
    "good hospital)</li>"
    "<li>Can the plan be renewed for lifetime (some plans cut off the cover at age 60 or 70, when you need "
    "it the most)</li>"
    "<li>The company has a good claim settlement record</li>"
    "</ul>"
    "</p>",

    'PTC3': "<p>"
    "<ul>"
    "<li>No limit or a high limit on your hospital room charges (will help you get a private room in a "
    "good hospital)</li>"
    "<li>Can the plan be renewed for lifetime (some plans cut off the cover at age 60 or 70, "
    "when you need it the most)</li>"
    "<li>The company has a good claim settlement record</li>"
    "</ul>"
    "</p>",

    'PTD1': "<p>We considered %s factors - Medical risks at your age, cover for your %s, %s and that you "
    "live in %s.</p>"
    "<p>Also, you might need to bear maternity expenses in the future. In case of difficult pregnancies, "
    "costs could be quite high.</p>"
    "%s",

    'PTD2': "<p>We considered %s factors - Medical risks at your age, cover for your %s, %sand that you live in %s.</p>"
    "%s",

    'PTD3': "<p>We considered two main factors  - Medical risks at your age,  and that you live in %s.</p>"
    "%s",

}

prod = Product.objects.get(code='P005')
if not prod.content:
    prod.content = content_dict
else:
    prod.content.update(**content_dict)
prod.save()

# For P007
prod = Product.objects.get(code='P007')
content_dict = {
    'PTA1': "<p>Commonly known as Mediclaim or Health Insurance.</p>"
    "<p>The one in which you get a cashless card. Absolutely need to have. "
    "The high risk you face, warrants a high value cover.</p>"
    "<p>This plan is sold by General Insurance companies, not Life Insurance companies.</p>",

    'PTB1': "<p>Covers all expenses when hospitalized - treatment /surgery, room charges, medicines etc.</p>"
    "<p>At %s - the risk, and therefore the cost of medical care, is at its peak. You need to cover all "
    "the above expenses, and cover them well.</p>",

    'PTC1': "<p>"
    "<ul>"
    "<li>Floater Cover (Covers the family under one policy)</li>"
    "<li>No limit or a high limit on your hospital room charges "
    "(will help you get a private room in a good hospital)</li>"
    "<li>Can the plan be renewed for lifetime (some plans cut off the cover at age 60 or 70, "
    "when you need it the most)</li>"
    "<li>The company has a good claim settlement record</li>"
    "</ul>"
    "</p>",

    'PTC2': "<p>"
    "<ul>"
    "<li>No limit or a high limit on your hospital room charges "
    "(will help you get a private room in a good hospital)</li>"
    "<li>Can the plan be renewed for lifetime (some plans cut off the cover at age 60 or 70, when "
    "you need it the most)</li>"
    "<li>The company has a good claim settlement record</li>"
    "</ul>"
    "</p>",

    'PTD1': "<p>We considered %s factors - Medical risks at your age, cover for your %s, %sand that you live in %s.</p>"
    "%s"
    "<p>And you don't want to pay out of pocket.</p>",

    'PTD2': "<p>We considered two main factors - Medical risks at your age,  and that you live in %s.</p>"
    " %s"
    "<p>And you don't want to pay out of pocket.</p>",
}

if not prod.content:
    prod.content = content_dict
else:
    prod.content.update(**content_dict)
prod.save()

# For P008
prod = Product.objects.get(code='P008')
content_dict = {
    'PTA1': "<p>This product is an add on. It makes your health insurance costs much cheaper.</p>"
    "<p>Works exactly like your regular health insurance, with one big difference. "
    "It covers you beyond a certain limit.</p>"
    "<p>For eg. If your hospital bill is &#8377;6 Lakhs, this plan will cover for the expenses "
    "of &#8377;3 Lakhs and beyond.</p>"
    "<p>The first &#8377;3 Lakhs will be paid out of your pocket or from your base health insurance plan.</p>"
    "<p>This amount (the first 3 Lakhs) is called the deductible.</p>",

    'PTB1': "<p>Covers all expenses when hospitalized for more than 24 hours - treatment /surgery, "
    "room charges, medicines etc.</p>"
    "<p>Everything that your basic health insurance will cover, but only beyond the deductible amount "
    "mentioned.</p>",

    'PTC1': "<p>Look for plans called 'super top-up' health insurance plans.</p>"
    "<p>Plans which are not super top-up, cover the expenses if you exhaust your base plan in one single "
    "stay at the hospital.</p>"
    "<p>Super top-ups cover the expenses if you exhaust your cover in one year, not in a single stay "
    "at the hospital.</p>"
    "<p>Super top-ups therefore offer much better coverage.</p>",

    'PTD1': "<p>At this stage of your life, you face high medical risks, the cost of good health-care is skyrocketing "
    "and you need to cover it all well.</p>"
    "<p>%s overall health coverage requirement is %s.</p>"
    "<p>Buying a regular health insurance plan for %s is expensive to say the least. "
    "Top-up plans are much cheaper.</p>"
    "<p>So a top-up plan with a cover amount of %s with a base plan of &#8377;5 Lakhs - gives you 100%% "
    "protection at 40%% lesser cost.</p>"
    "</p>",
}

if not prod.content:
    prod.content = content_dict
else:
    prod.content.update(**content_dict)
prod.save()


# For P009
prod = Product.objects.get(code='P009')
content_dict = {
    'PTA1': "<p>This is to take care of recurring, relatively small value (i.e. less than &#8377;5000) costs.</p>"
    "<p>They are not strictly insurance plans, because you pay a premium equal to the benefit you get.</p>"
    "<p>But consider the fact that you get a tax break on the premium, "
    "and these plans make financial sense.</p>",

    'PTB1': "<p>This covers Outpatient expenses including cost of your annual health check up.</p>"
    "<p>Plans available in the market will cover you for OPD expenses usually up to &#8377;10,000 a year</p>",

    'PTC1': "<p>Consider buying health management plans, rather than insurance.</p>"
    "<p>They give you a discount on your annual health check up, and could include a "
    "comprehensive 'health risk profiling'.</p>"
    "<p>Advice from nutritionists and doctors help you make targeted lifestyle changes, to keep chronic "
    "conditions at bay.</p>"
    "<p>And you get discounts on check ups and small treatments. Saves a ton of money in the long run.</p>",

    'PTD1': '',
}

if not prod.content:
    prod.content = content_dict
else:
    prod.content.update(**content_dict)
prod.save()


# For P010
prod = Product.objects.get(code='P010')
content_dict = {
    'PTA1': "<p>Comes without a cashless card.</p>"
    "<p>Supplements your basic health insurance, as for something like stroke/cancer your basic plan will "
    "be found wanting and how.</p>"
    "<p>On diagnosis of any critical illness defined in the plan, the insurance company pays a "
    "fixed amount.</p>"
    "<p>If it's a disease or condition not mentioned in the plan, it's not covered. </p>",

    'PTB1': "Basic health insurance plans cover the hospital bills, as per what was actually spent.</p>"
    "<p>Critical illness plans provide a fixed amount which is not dependent on the hospital "
    "bills. Proof of diagnosis of the critical illness is enough.</p>"
    "<p>The lump sum provided compensates for heavy expenditure, recovery time, and the loss of income earning"
    " capacity that results from an "
    "instance of cancer, or stroke or heart attack.</p>",

    'PTC1': "<p>Look for 2 things -"
    "<ul>"
    "<li>How many conditions or diseases does the plan cover</li>"
    "<li>How comprehensive it is. Eg. some plans cover cancer only if its in stage IV (the final stage)</li>"
    "</ul>"
    "</p>"
    "<p>Surely that's a plan you want to avoid.</p>"
    "<p>This plan is sold by both General Insurance companies and  Life Insurance companies. "
    "However, it's safer to go with General Insurance or specific health insurance companies. "
    "Health insurance is not the expertise of Life Insurance companies </p>",

    'PTD1': "<p>We considered 2 important factors - "
    "Chances of critical illnesses between the age of %s and the value of hospitalization cover already recommended "
    "for you & your %s.</p>"
    "<p>With those factors in mind, we recommend that additional protection is needed for critical illnesses.</p>",

    'PTD2': "<p>We considered 2 important factors - "
    "Chances of critical illnesses between the age of %s and the value of hospitalization cover already recommended "
    "for you.</p>"
    "<p>With those factors in mind, we recommend that additional protection is needed for critical illnesses.</p>",
}

if not prod.content:
    prod.content = content_dict
else:
    prod.content.update(**content_dict)
prod.save()


# For P011
prod = Product.objects.get(code='P011')
content_dict = {
    'PTA1': "<p>Commonly known as Mediclaim or Senior Citizen Health Insurance.</p>"
    "<p>You'll get a cashless card for your parents to cover hospitalization expenses. "
    "Most plans have age limits after which you can't buy a health insurance plan.</p>"
    "<p>Hence the need for specific tailor made plans which allow health insurance access to older ages.</p>",

    'PTB1': "<p>Covers all expenses when hospitalized - treatment /surgery, room charges, medicines etc.</p>"
    "<p>If you pay the premium for dependent parents, you can also avail tax deduction of up "
    "to &#8377;20,000 per financial year.</p>",

    'PTC1': "<p>"
    "<ul>"
    "<li>The most important thing is to check whether the plan can be renewed for lifetime. "
    "Some plans cut off the cover at age of 70/75, and at that age, you will not find another "
    "health insurance plan for your parents</li>"
    "<li>No limit or a high limit on your hospital room charges "
    "(will help you get a private room in a good hospital)</li>"
    "<li>The company has a good claim settlement record</li>"
    "<li>List of Hospitals where you get cashless access, so you don't have to pay an advance during "
    "admission</li>"
    "</ul>"
    "</p>"
    "<p>Check that the hospital that your parents frequent, is included in the cashless network of the policy.</p>",

    'PTD1': "<p>This one is a no-brainer. Parents grow old. "
    "We hate to see them growing old, but the least we can do is give them comfort.</p>"
    "<p>To help your parents get the best quality medical care, get a cover as high as you can. "
    "Most companies won't provide a very high cover.</p>"
    "<p>Our recommended cover is the minimum you should look at. "
    "If you can get a higher cover, go for it!</p>",
}

if not prod.content:
    prod.content = content_dict
else:
    prod.content.update(**content_dict)
prod.save()


# For P013
prod = Product.objects.get(code='P013')
content_dict = {
    'PTA1': "<p>The plan covers you for a fixed number of years normally ranging between 10-35. "
    "The coverage period (number of years) is called the Term. Hence the name, Term Cover</p>"
    "<p>It's the cheapest life insurance plan you can get. "
    "And probably the only life insurance plan you need.</p>",

    'PTB1': "<p>Gives the family a fixed amount called the sum assured, in case of death. "
    "It does not give any returns, as it is not an investment product.</p>"
    "<p>Don't worry about not making any returns. Insurance plans that offer returns "
    "are extremely expensive, and their returns are usually not worth it.</p>",

    'PTC1': "<p>"
    "<ul> "
    "<li>Take a plan for the maximum possible term. It's cheaper when you buy young and gets expensive "
    "as you age. You don't want to try and buy a cover when you're old. The long term ensures, you "
    "don't run out of cover and pay lesser overall. </li>"
    "<li>Look for cost effective options; but don't settle just for the cheapest plan, "
    "without checking the company's claim settlement record</li>"
    "</ul>"
    "</p>"
    "<p>Ask yourself, if you trust that the company will be around 20 years from now, "
    "in case a claim has to be paid.</p>",


    'PTD1': "<p>This one is simple, you need to cover all outstanding loans. "
    "We suggest a cover approximately equal to your total debt.</p>"
    "<p>If you expect your loan burden to increase in the next 5 years, then err on the side of caution "
    "and take a plan which is 20% higher than current loan outstanding.</p>",


    'PTD2': "<p>You're single and the suggested cover might seem high. But look at it as a future proofing for cheap. "
    "When your responsibilities and liabilities increase, you would've already protected that much "
    "lower cost, than buying later.</p>"
    "<p>The maximum cover you can get in a Term Life plan is always a multiple of your income. "
    "Our recommendation takes into account your life-stage, and how insurance companies "
    "calculate the allowed cover.</p>",

    'PTD3': "<p>You need enough to replace at least some part of your income, "
    "and give your loved ones the financial support they need.</p>"
    "<p>And of course, you need to cover all your outstanding loans.</p>"
    "<p>The maximum cover you can get in a Term Life plan is always a multiple of your income.</p>"
    "<p>Our recommendation takes into account your life-stage, your liabilities, "
    "and how insurance companies calculate the allowed cover.</p>",


}

if not prod.content:
    prod.content = content_dict
else:
    prod.content.update(**content_dict)
prod.save()

# For P015
prod = Product.objects.get(code='P015')
content_dict = {
    'PTA1': "<p>These are annual programs which are aimed at helping you stay well.</p>"
    "<p>They are not insurance covers, but are features embedded in most of the new age insurance covers.</p>",

    'PTB1': "<p>Health management plans usually provide discounts on diagnostic tests.</p>"
    "<p>For an annual fee they would also provide unlimited access to a panel of doctors and specialists "
    "(usually covering  Cardiology, Diabetology, Orthopaedics, Gynaecology and Pediatrics).</p>"
    "<p>Such plans could also include a comprehensive 'health risk profiling'.</p>"
    "<p>These profiles help you quantify the risk of chronic diseases, and advice from nutritionists and "
    "doctors can help you make targeted lifestyle changes, to help you keep chronic conditions at bay.</p>"
    "<p>More importantly, these programs help increase overall fitness and energy levels.</p>",

    'PTC1': "<p>Check the panel of doctors and make sure that there are enough options which are suitable "
    "for where you stay.</p>"
    "<p>Also check that discounts cover not just routine tests but also high value tests.</p>",

    'PTD1': "<p>Typically plans will cost between &#8377;3000 and &#8377;5000 per annum per family.</p>"
    "<p>If a health check up is included , then the plan could cost up to &#8377;7500</p>",
}

if not prod.content:
    prod.content = content_dict
else:
    prod.content.update(**content_dict)
prod.save()

# For P016
prod = Product.objects.get(code='P016')
content_dict = {
    'PTA1': "<p>These are annual programs which are aimed at helping you stay well.</p>"
    "<p>They are not insurance covers, but are features embedded in most of the new age insurance covers.</p>",

    'PTB1': "<p>Health management plans usually provide discounts on diagnostic tests.</p>"
    "<p>For an annual fee they would also provide unlimited access to a panel of doctors and specialists "
    "(usually covering  Cardiology, Diabetology, Orthopaedics, Gynaecology and Pediatrics).</p>"
    "<p>Such plans could also include a comprehensive 'health risk profiling'.</p>"
    "<p>These profiles help you quantify the risk of chronic diseases, and advice from nutritionists and "
    "doctors can help you make targeted lifestyle changes, to help you keep chronic conditions at bay.</p>"
    "<p>More importantly, these programs help increase overall fitness and energy levels.</p>",

    'PTC1': "<p>Check the panel of doctors and make sure that there are enough options which are suitable for where "
    "you stay.</p>"
    "<p>Also check that discounts cover not just routine tests but also high value tests.</p>",

    'PTD1': "<p>Typically plans will cost between &#8377;3000 and &#8377;5000 per annum per family.</p>"
    "<p>If a health check up is included , then the plan could cost up to &#8377;7500</p>",
}

if not prod.content:
    prod.content = content_dict
else:
    prod.content.update(**content_dict)
prod.save()

# For P017
prod = Product.objects.get(code='P017')
content_dict = {
    'PTA1': "<p>The plan covers you for a fixed number of years normally ranging between 10-35. "
    "The coverage period (number of years) is called the Term. Hence the name, Term Cover</p>"
    "<p>It's the cheapest life insurance plan you can get. "
    "And probably the only life insurance plan you need.</p>",

    'PTB1': "<p>Gives the family a fixed amount called the sum assured, in case of death. "
    "It does not give any returns, as it is not an investment product.</p>"
    "<p>Don't worry about not making any returns. Insurance plans that offer returns "
    "are extremely expensive, and their returns are usually not worth it.</p>",

    'PTC1': "<p>"
    "<ul> "
    "<li>Take a plan with the maximum possible term. "
    "The premium that you pay is the same each year, for the entire term. "
    "It is cheaper when young, and gets expensive as you age. "
    "A longer term means, you can stick to relatively low premium for many years. "
    "Overall you will pay much lesser</li>"
    "<li>Look for cost effective options; but don't settle just for the cheapest plan, "
    "without checking the company's claim settlement record</li>"
    "</ul>"
    "</p>"
    "<p>Ask yourself, if you trust that the company will be around 20 years from now, "
    "in case a claim has to be paid.</p>",

    'PTD1': "<p>You need enough to replace at least some part of your income, "
    "and give your loved ones the financial support they need.</p>"
    "<p>The maximum cover you can get in a Term Life plan is always a multiple of your income.</p>"
    "<p>Our recommendation takes into account your life-stage, "
    "and how insurance companies calculate the allowed cover.</p>",

    'PTD2': "<p>You're single and the suggested cover might seem high. But look at it as a future proofing for cheap. "
    "When your responsibilities and liabilities increase, you would've already protected that much "
    "lower cost, than buying later.</p>"
    "<p>The maximum cover you can get in a Term Life plan is always a multiple of your income. "
    "Our recommendation takes into account your life-stage, and how insurance companies "
    "calculate the allowed cover.</p>",

    'PTD3': "<p>You need enough to replace at least some part of your income, "
    "and give your loved ones the financial support they need.</p>"
    "<p>And of course, you need to cover all your outstanding loans.</p>"
    "<p>The maximum cover you can get in a Term Life plan is always a multiple of your income.</p>"
    "<p>Our recommendation takes into account your life-stage, your liabilities, "
    "and how insurance companies calculate the allowed cover.</p>",

}

if not prod.content:
    prod.content = content_dict
else:
    prod.content.update(**content_dict)
prod.save()
