import sys
import time
import datetime
import os
import csv

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from customdb.customquery import sqltojson, sqltodict
from django.core.mail import send_mail
import thread
import threading
from datetime import timedelta
from django.utils.encoding import smart_str, smart_unicode

from core.models import User
from review.models import *
from review.forms import *
from review.rev_utils import *

# For R001
content_dict = {
    'RT01': "<p>Carefree at the age of %s?</p>"
    "<p>You are probably not thinking about insurance. You are at your healthiest; and illness, aches "
    "and pains are eons away."
    "It's time to buy that bike, to travel and to just LIVE.</p>"
    "<p>But it's accidents arising from that 'rush of blood' during this phase of life, that you need "
    "to guard against. "
    "A cruel twist of fate can result in loss of a limb, or an eye. It's gut wrenching, and the money "
    "that you need to get them treated is crushing.</p>",

    'RT02': "<p>The onset of the golden years. It is time to look forward to retirement, when you can relax "
    "and ease the pace of life."
    "You've probably planned a fund for the inevitable health expenses.</p>"
    "<p>But, in addition, consider this - 35% of accidents after the age of 50 are 'household accidents'."
    "They take place inside the house. The classic example being falling down stairs or slipping in "
    "the bathroom.  It's something to think about.</p>",

}

risk = Risk.objects.get(code='R001')
if not risk.content:
    risk.content = content_dict
else:
    risk.content.update(**content_dict)
risk.save()

# FOR R002
content_dict = {
    'RT01': "<p>There is nothing better than having a child. It's happiness like never before.</p>"
    "<p>You want the best for the baby and the mother. But the best involves hard cash. "
    "Given that you have a child, and could plan another bundle of joy, it's possible to cushion the cost "
    "of getting the best.</p>",

    'RT02': "<p>There is nothing better than having a child. It's happiness like never before.</p>"
    "<p>You want the best for the baby and the mother. But the best involves hard cash."
    "Given that you're married, and yet to go the family way, it's possible to cushion the cost of "
    "getting the best.</p>",
}
risk = Risk.objects.get(code='R002')
if not risk.content:
    risk.content = content_dict
else:
    risk.content.update(**content_dict)
risk.save()

# FOR R003

content_dict = {
    'RT01': "<p>%s</p>"
    "<p>You%s face some basic medical risks. The ones that involve 4-5 day hospital stays, unending tests "
    "and agony.</p>"
    "<p>You can't escape the agony, but you can cover the money.</p>",

    'RT02': "<p>40 is the new 20. At least Shahrukh, Salman and Akshay think so.</p>"
    "<p>However, medically speaking, you're %s years from entering the high risk zone.</p>"
    "<p>45 is the milestone, where the medical and insurance community categorize people as higher risk.</p>"
    "<p>Post 45, when the onset of chronic conditions is most rampant; good coverage is hard to find "
    "without undergoing a million medical tests and burning a hole in your pocket.</p>"
}

risk = Risk.objects.get(code='R003')
if not risk.content:
    risk.content = content_dict
else:
    risk.content.update(**content_dict)
risk.save()
# print risk.content


# FOR R004

content_dict = {
    'RT01': "<p>Once you hit %s, nature is unkind.</p>"
    "<p>From the small niggles to the large ailments, treatments and recovery are never cheap and quick.</p>"
    "<p>Post %s, when the onset of chronic conditions is most rampant; good coverage is hard to find without "
    "undergoing a million medical tests and burning a hole in your pocket.</p>"
    "<p>%sIt's risk at its wildest scariest best.</p>",
}

risk = Risk.objects.get(code='R004')
if not risk.content:
    risk.content = content_dict
else:
    risk.content.update(**content_dict)
risk.save()
# print risk.content


# FOR R005

content_dict = {
    'RT01': "<p>Hypertension, diabetes, arthritis - sound all too common? We bet you know five people who have one "
    "or more of these.</p>"
    "<p>These kind of chronic conditions aggravate quickly. You are forced to make lifestyle changes and "
    "constantly monitor the illness.</p>"
    "<p>That has a cost. The costs mount month after month, for years together. "
    "Can you control these expenses and plug the drain? </p>",
}

risk = Risk.objects.get(code='R005')
if not risk.content:
    risk.content = content_dict
else:
    risk.content.update(**content_dict)
risk.save()
# print risk.content

# FOR R006

content_dict = {
    'RT01': "<p>Catastrophe strikes once. The question is - who and when will it strike?</p>"
    "<p>It has the potential to take anyone down - physically, mentally and emotionally. "
    "And dare we say financially?</p>"
    "<p>The dreaded stroke, cancer, multiple sclerosis, kidney failure. Scary words. Scarier situations.</p>"
    "<p>Your basic health insurance is woefully inadequate, if life deals this hand.</p>",
}

risk = Risk.objects.get(code='R006')
if not risk.content:
    risk.content = content_dict
else:
    risk.content.update(**content_dict)
risk.save()
# print risk.content


# FOR R007
content_dict = {
    'RT01': "<p>It's one of those responsibilities, where you always feel you could do more.</p>"
    "<p>Especially when your parents are financially dependent on you, you need to plan, "
    "and plan this well.</p>"
    "<p>You want the best health-care for your parents, and the last thing you want is to fight a "
    "budget battle.</p>"
    "<p>It's an expense that will hit you sooner or later.</p>",


    'RT02': "<p>It's one of those responsibilities, where you always feel you could do more.</p>"
    "<p>It's a great thing that your parents are self-reliant, but as they get older, the hospital "
    "visits commence and the expenses pile up.</p>"
    "<p>You want the best health-care for your parents, and the last thing you want is to fight a budget "
    "battle. It's an expense that will hit you sooner or later.</p>",
}

risk = Risk.objects.get(code='R007')
if not risk.content:
    risk.content = content_dict
else:
    risk.content.update(**content_dict)
risk.save()
# print risk.content


# FOR R008
content_dict = {
    'RT01': "<p>Do you care about who pays your debts, if something were to happen to you?</p>"
    "<p>It's a cruel thought. The worst thing for loved ones is coming to terms with losing someone.</p>"
    "<p>And it gets even worse if they also have to deal with financial debt.</p>"
    "<p>There's only a small chance of such a situation arising, but it has massive impact on the family.</p>",
}

risk = Risk.objects.get(code='R008')
if not risk.content:
    risk.content = content_dict
else:
    risk.content.update(**content_dict)
risk.save()
# print risk.content


# FOR R009
content_dict = {
    'RT01': "<p>Money. Can it make up for the loss of a loved one? <b>Never</b>.</p>"
    "<p>Can it cushion the blow? <b>Maybe not</b>.</p>"
    "<p>Can it ensure that the loss is not accompanied with financial trauma? <b>Definitely, yes</b>.</p>"
    "<p>Will your %s need the cash to maintain current standard of living, pay some EMIs, and "
    "for %s retirement?</p>"
    "<p>This is the biggest risk you face. Rather, it's the biggest risk your loved ones face.</p>",

    'RT02': "<p>Money. Can it make up for the loss of a loved one? <b>Never</b>.</p>"
    "<p>Can it cushion the blow? <b>Maybe not</b>.</p>"
    "<p>Can it ensure that the loss is not accompanied with financial trauma? <b>Definitely, yes</b>.</p>"
    "<p>Will your %s need the cash to maintain current standard of living, pay some EMIs, and "
    "for %s retirement?</p>"
    "<p>Will the kids be able to work towards that 'Masters Program' without stressing on where will "
    "the money come from?</p>"
    "<p>It's the biggest risk you face. Rather, it's the biggest risk your loved ones face.</p>",

    'RT03': "<p>Money. Can it make up for the loss of a loved one? <b>Never</b>.</p>"
    "<p>Can it cushion the blow? <b>Maybe not</b>.</p>"
    "<p>Can it ensure that the loss is not accompanied with financial trauma? <b>Definitely, yes</b>.</p>"
    "<p>What about your parents' retirement fund, their health expenses, and maybe the family home that "
    "you were planning to invest some money in.</p>"
    "<p>This risk may not seem probable. But spare a thought, it's the biggest risk your loved ones face.</p>",


}

risk = Risk.objects.get(code='R009')
if not risk.content:
    risk.content = content_dict
else:
    risk.content.update(**content_dict)
risk.save()
# print risk.content


# FOR R010
content_dict = {
    'RT01': "<p>You know that you have a family history of one of more chronic conditions - but you don't quite "
    "know what to do about it?</p>"
    "<p>Take heart.</p>"
    "<p>It is possible to manage your health so that you can minimise the risk of contracting diabetes or "
    "hypertension; even if you have adverse family history.</p>"
    "<p>That's better than being fatalistic and hoping the disease will 'give you a pass'.</p>",
}

risk = Risk.objects.get(code='R010')
if not risk.content:
    risk.content = content_dict
else:
    risk.content.update(**content_dict)
risk.save()
# print risk.content
