import sys
import time
import datetime
import os
from optparse import OptionParser
import csv

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from django.db import models
from wiki.models import *

maindata = []
for p in HealthProduct.objects.all():
    print "FOR product", p.title
    slabs = p.slabs.all()
    # print "Slabs", slabs.count()
    for slab in slabs:
        row = "-".join([p.title, str(slab.sum_assured), str(slab.grade)])
        rdata = [row]
        for attr in AttributeList:
            model = attr[2]
            # print "For model", model
            data = ""
            if getattr(getattr(slab, model), 'all', None):
                d = [obj.__dict__ for obj in getattr(slab, model).all()]
                for obj in d:
                    obj.pop('_state')
                    data = data + "\n".join(["%s = %s" % (k, v)
                                             for k, v in obj.items()])
                    data += "\n-----------------------\n"
            else:
                d = getattr(slab, model)
                if d:
                    d = d.__dict__
                    d.pop('_state')
                    data = "\n".join(["%s = %s" % (k, v)
                                      for k, v in d.items()])
            rdata.append(data)
        maindata.append(rdata)

print [md[0] for md in maindata]

with open('/tmp/eggs.csv', 'wb') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter='|',
                            quotechar='@', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow([" "] + [att[1] for att in AttributeList])
    for md in maindata:
        spamwriter.writerow(md)
