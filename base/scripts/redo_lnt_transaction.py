import sys
import os
from optparse import OptionParser

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from health_product.models import *
from health_product.insurer import settings as insurer_settings
from importlib import import_module


def retry(transaction_id):
    transaction = Transaction.objects.get(transaction_id=transaction_id)
    insurer_module = insurer_settings.INSURER_MAP[
        transaction.slab.health_product.insurer.id]
    m = import_module("health_product.insurer.%s.integration" % insurer_module)
    res = m.redo_transaction(transaction)
    print res
    return


def main():
    usage = "usage: %prog -t <transaction id>"
    parser = OptionParser(usage)

    parser.add_option("-t", "--transaction_id", type="string",
                      dest="transaction_id", help="transction id to process")
    (options, args) = parser.parse_args()

    if not options.transaction_id:
        parser.error("Must specify transaction id you want to process")
    transaction_id = options.transaction_id.strip()
    retry(transaction_id)


if __name__ == "__main__":
    main()
