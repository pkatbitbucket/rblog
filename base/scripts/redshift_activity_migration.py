__author__ = 'glitterbug'
import sys  # NOQA
import traceback  # NOQA
import re  # NOQA
import os  # NOQA
import json  # NOQA
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import django  # NOQA
sys.path.append('../')  # NOQA

import settings  # NOQA
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from operator import itemgetter  # NOQA

from django.core.paginator import Paginator  # NOQA

from core import models as core_models  # NOQA
from core import activity as core_activity  # NOQA
from motor_product import models as motor_models  # NOQA

LOG_FORMAT = " %(activity_type)s %(product)s %(page_id)s "
LOG_FORMAT += "%(form_name)s %(form_position)s %(form_variant)s "
LOG_FORMAT += "%(tracker)s %(user)s %(ip_address)s %(activity_at)s "
LOG_FORMAT += "%(json)s"
LOG_FORMAT = LOG_FORMAT.replace(' ', '^^^')

INTEGER_REGEX = re.compile(r'[0-9]+')
SPOOL_FILE_DIR = '/var/spool/cfox/'


def clean_dict(json_data):
    """
    Remove json keys with None values.
    :return:
    """
    return dict(filter(itemgetter(1), json_data.items()))


class SpoolManager(object):
    def __init__(self, filename):
        if not filename:
            raise Exception("Invalid filename")

        self.completed = False
        self.spool_file = os.path.join(
            SPOOL_FILE_DIR,
            os.path.basename(filename)
        )

        if not os.path.isfile(self.spool_file):
            open(
                self.spool_file,
                "w+"
            ).close()

        self.file = open(
            self.spool_file,
            "r+"
        )

    def write_to_spool(self, data, filename):
        self.truncate_file()
        # Write into file.
        self.file.write("%s %s" % (filename, data))
        # TODO: Need to find proper way to write changes without closing a file.
        self.file.close()
        self.file = open(self.spool_file, "r+")

    def get_file_point_file(self):
        data = None
        try:
            data = self.file.read()
            self.file.seek(0)
            if not data:
                return (None, 0)

            filename, pointer = data.split()
            pointer = int(pointer)
        except:
            traceback.print_exc()
            return (None, 0)

        return filename, pointer

    def truncate_file(self):
        self.file.seek(0)
        self.file.truncate()
        self.file.close()
        self.file = open(self.spool_file, "r+")

    def __enter__(self):
        return self

    def exit(self):
        # Truncate file.
        if self.completed:
            self.truncate_file()

        self.file.close()

    def __exit__(self, exc_type, exc_value, traceback):
        self.exit()


def iterator():
    with SpoolManager('old-activity-migration-script.spool') as spool_manager:
        _, pointer = spool_manager.get_file_point_file()
        if pointer:
            p = Paginator(core_models.Activity.objects.filter(pk__gt=pointer).order_by('id'), 1000)
            # print("Resuming from activity id (%s) " % pointer)
        else:
            p = Paginator(core_models.Activity.objects.all().order_by('id'), 1000)

        total_page = p.num_pages

        with open('/tmp/migration-progress', 'w', 0) as f:
            f.write('Total pages - %s \n' % total_page)
            for page_no in p.page_range:
                yield p.page(page_no).object_list, spool_manager
                f.write('Current page - %s \n' % page_no)
                os.fsync(f)


def do_activity(acts, spool_manager, log_f):
    for act in acts:
        activity_data = {
            'state': act.state,
            'target': act.target,
            'term': act.term,
            'term_category': act.term_category,
            'url': act.url,
            'tracker_id': act.tracker_id,
            'user_id': act.user_id,
            'app': settings.APP,
            'requirement_id': act.requirement_id,

            'activity_id': act.id,
            'activity_type': act.activity_type,
            'asctime': act.created.strftime(core_activity.DATE_TIME_FORMAT),
            'browser': act.browser,
            'city': act.city,
            'data1': '',
            'data2': '',
            'device': act.device,
            'device_model': act.device_model,
            'feed_item': act.feed_item,
            'form_name': act.form_name,
            'form_position': act.form_position,
            'form_variant': act.form_variant,
            'gclid': act.gclid,
            'ip_address': act.ip_address,
            'isp': act.isp,
            'label': act.label,
            'landing_url': act.landing_url,
            'match_type': act.match_type,
            'os': act.os,
            'page_id': act.page_id,
            'placement': act.placement,
            'product': act.product,
            'request_id': '',
            'server': settings.SERVER_NAME,
        }

        req_created_dt = None
        if act.requirement:
            req_created_dt = act.requirement.created_date.strftime(core_activity.DATE_TIME_FORMAT)

        activity_data['requirement_created_at'] = req_created_dt

        session_id = None
        if act.tracker:
            session_id = act.tracker.session_key

        activity_data['session_key'] = session_id

        campaign = None
        source = None
        medium = None
        category = None
        brand = None
        content = None

        if act.mid:
            campaign = act.mid.campaign
            source = act.mid.source
            medium = act.mid.medium
            category = act.mid.category
            brand = act.mid.brand
            content = act.mid.content

        activity_data['campaign'] = campaign
        activity_data['source'] = source
        activity_data['medium'] = medium
        activity_data['category'] = category
        activity_data['brand'] = brand
        activity_data['content'] = content

        vehicle_make = None
        vehicle_model = None
        vehicle_variant = None
        vehicle_type = None
        email = None
        mobile = None
        is_internal = 'f'

        if act.fd:
            form_data = act.fd.get_data()
            activity_data['form_data'] = form_data
            data = form_data

            if form_data:
                if 'core.forms.Person' in form_data:
                    person_form = form_data['core.forms.Person']
                    if isinstance(person_form, list):
                        for form in person_form:
                            if form.get('email'):
                                email = form.get('email', 'NULL')
                            if form.get('mobile'):
                                mobile = form.get('mobile', 'NULL')
                    else:
                        email = person_form.get('email', 'NULL')
                        mobile = person_form.get('mobile', 'NULL')

                if 'core.forms.CarDetail' in data or 'core.forms.BikeDetail' in data:
                    if 'core.forms.CarDetail' in data:
                        form_data = data['core.forms.CarDetail']
                    elif 'core.forms.BikeDetail' in data:
                        form_data = data['core.forms.BikeDetail']
                    else:
                        form_data = {}

                    if form_data:
                        if isinstance(form_data, list):
                            form_data = form_data[-1]
                        elif isinstance(form_data, dict):
                            pass
                        else:
                            continue

                        vehicle_master_id = form_data.get('vehicle_master')
                        vehicle_master_instance = None

                        if vehicle_master_id and INTEGER_REGEX.match(vehicle_master_id):
                            vehicle_master_instance = motor_models.VehicleMaster.objects.filter(pk=vehicle_master_id).first()

                        if vehicle_master_instance:
                            vehicle_make = vehicle_master_instance.make.name
                            vehicle_model = vehicle_master_instance.model.name
                            vehicle_variant = vehicle_master_instance.variant
                            vehicle_type = vehicle_master_instance.vehicle_type

        if activity_data['ip_address'] in settings.INTERNAL_IPS:
            is_internal = 't'

        activity_data['is_internal'] = is_internal
        activity_data['email'] = email
        activity_data['mobile'] = mobile
        activity_data['vehicle_make'] = vehicle_make
        activity_data['vehicle_model'] = vehicle_model
        activity_data['vehicle_variant'] = vehicle_variant
        activity_data['vehicle_type'] = vehicle_type

        log_format_json = {
            'activity_type': act.activity_type if act.activity_type else '',
            'product': act.product if act.product else '',
            'page_id': act.page_id if act.page_id else '',
            'form_name': act.form_name if act.form_name else '',
            'form_variant': act.form_variant if act.form_variant else '',
            'form_position': act.form_position if act.form_position else '',
            'tracker': act.tracker_id if act.tracker_id else '',
            'user': act.user_id if act.user_id else '',
            'ip_address': act.ip_address if act.ip_address else '',
            'activity_at': act.created.strftime(core_activity.DATE_TIME_FORMAT),
            'json': json.dumps(clean_dict(activity_data))
        }

        log_str = LOG_FORMAT % log_format_json
        log_f.write(log_str + '\n')
        spool_manager.write_to_spool(act.pk, 'NULL')


with open('/logs/aws/old-activity-log/access.log', 'a') as log_f:
    for acts, spool_manager in iterator():
        do_activity(acts, spool_manager, log_f)
