__author__ = 'glitterbug'

import django  # NOQA
import sys  # NOQA
import os  # NOQA
import csv  # NOQA

sys.path.append('../')  # NOQA
import settings  # NOQA
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()  # NOQA

from django.db import transaction  # NOQA

from core import models as core_models  # NOQA


@transaction.atomic
def merge_reqs():
    with open("requirement_merge.csv", "w") as requirement_merge_csv:
        fieldnames = ['req_id', 'merged_into_req', 'merge_into_req_cur_state', 'pre_merge']
        writer = csv.DictWriter(requirement_merge_csv, fieldnames=fieldnames)
        writer.writeheader()

        for req in core_models.Requirement.objects._all().filter(current_state__pk=17, merge_into=None):
            last = req.crm_activities.last()
            assert len(r for r in last.requirements.all() if r.current_state != 17) == 1
            for r in last.requirements.all():
                if r.current_state == 17:
                    continue

                req.merge(r)
                r_cur_state = r.current_state.name if r.current_state else None
                writer.writerow({'req_id': req.pk, 'merged_into_req': r.pk, 'merge_into_req_cur_state': r_cur_state,
                                 'pre_merge': ','.join([r.pk for r in req.activity_set.all()])})

merge_reqs()
