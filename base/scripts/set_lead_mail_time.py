import random
import datetime
import sys
import time
import datetime
import os

from optparse import OptionParser

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from django.db import models
from activity.models import *


def generate_random_date(start, end):
    return start + datetime.timedelta(seconds=random.randint(0, int((end - start).total_seconds())))


def set_new_email_schedule(start, end, n=100):
    for ld in list(LeadData.objects.filter(next_mail_on__isnull=True))[:n]:
        print ">>", ld.email
        ld.next_mail_on = generate_random_date(start, end)
        ld.mail_set = False
        ld.mail_type = "WELCOME"
        ld.save()

total_to_sent = 50000
in_days = 40
starting_from = datetime.datetime.today().date()
from_time = datetime.time(8, 0)  # from 8:00am
to_time = datetime.time(17, 0)  # to 5:00pm

for day in range(0, in_days):
    for_day = starting_from + datetime.timedelta(days=day)
    per_day_approx = int(total_to_sent / (in_days + 1)) + \
        random.randint(1, 50)  # 50 mails tolerance
    print ">>", per_day_approx
    set_new_email_schedule(datetime.datetime.combine(
        for_day, from_time), datetime.datetime.combine(for_day, to_time), per_day_approx)
