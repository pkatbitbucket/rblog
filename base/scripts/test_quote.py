import django
import sys
import os

import string
import random

sys.path.append('../')
import settings  # NOQA
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from core import activity as core_activity  # NOQA
from core import models as core_models  # NOQA
from travel_product import forms as travel_forms  # NOQA
from cfblog.utils import dum_request  # NOQA

from core.models import Policy, FormSection, FormData  # NOQA

# create new user.
email = core_models.Email.objects.create(
    email="hitul.mistry221@coverfox.com"
)


def random_str():
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))

travel_quote_data = {
    "trip_type": "single", "no_of_travellers": "1",
    "member1_age_band": "61  Years - 70 Years",
    "from_date": "15-Dec-2015", "plan": "Explore - Asia",
    "sum_insured": "US $ 50,000", "indian_citizen": "yes",
    "to_date": "28-Dec-2015"}


user = core_models.User.objects.create()
email.user = user
email.save()
session_key = random_str()

visitor = core_models.Tracker.objects.create(
    session_key=session_key
)
# Create travel activity.
travel_quote_instance = travel_forms.TravelQuoteFDForm(data=travel_quote_data)
core_activity.create_activity(
    core_activity.VIEWED,
    'health',
    'quotes',
    '',
    '',
    '',
    user=user,
    visitor=visitor,
    data=travel_quote_instance.get_fd(),
    request_dict=dum_request
)

# Test data
activity = core_models.Activity.objects.filter(
    user=user
).first()

# Check form data.
fd = activity.fd
print(activity)

# Test fd assigned or not.
if fd is None:
    raise Exception("Error")

# Test all forms exist.
if not fd.sections.filter(form="core.forms.TravelQuoteForm"):
    raise Exception("Error")


if activity.requirement is None:
    raise Exception("Error")
