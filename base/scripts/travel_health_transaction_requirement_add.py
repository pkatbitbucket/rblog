import django

import os
import sys

sys.path.append('../')
import settings  # NOQA
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from django.db import transaction as django_transaction  # NOQA

from core import models as core_models  # NOQA
from travel_product import models as travel_models  # NOQA
from health_product import models as health_models  # NOQA


@django_transaction.atomic()
def migrate_existing_travel_data():
    travel_transaction = travel_models.Transaction
    req_kind = core_models.RequirementKind.objects.get(
        product='travel',
        kind=core_models.RequirementKind.NEW_POLICY, sub_kind=''
    )

    for transaction in travel_transaction.objects.exclude(
            status__in=['COMPLETED', 'MANUAL COMPLETED']
    ).iterator():
        # Get open requirement for tracker.
        tracker = None
        if transaction.activity and transaction.activity.tracker:
            tracker = transaction.activity.tracker

        if tracker:
            tracker = core_models.Tracker.objects.get(id=tracker.id)
            req = core_models.Requirement.objects.get_open_requirement(
                user=None,
                tracker=tracker,
                kind=req_kind,
            )

            if req and req.current_state and req.current_state.pk != 17:
                req = core_models.Requirement.objects.get(id=req.pk)
                travel_transaction.objects.filter(transaction_id=transaction.pk).update(
                    requirement=req
                )
                print("Assigned requirement (%s) to travel transaction (%s)" % (
                    req.pk, transaction.transaction_id
                ))


@django_transaction.atomic()
def migrate_existing_health_data():
    health_transaction = health_models.Transaction
    req_kind = core_models.RequirementKind.objects.get(
        product='health',
        kind=core_models.RequirementKind.NEW_POLICY, sub_kind=''
    )

    for transaction in health_transaction.objects.exclude(
            status__in=['COMPLETED', 'MANUAL COMPLETED']
    ).iterator():
        # Get open requirement for tracker.
        if transaction.tracker:
            tracker = core_models.Tracker.objects.get(id=transaction.tracker.id)
            req = core_models.Requirement.objects.get_open_requirement(
                user=None,
                tracker=tracker,
                kind=req_kind,
            )

            if req and req.current_state and req.current_state.pk != 17:
                req = core_models.Requirement.objects.get(id=req.pk)
                health_transaction.objects.filter(id=transaction.id).update(requirement=req)
                print("Assigned requirement (%s) to health transaction (%s)" % (
                    req.pk, transaction.transaction_id
                ))

print("---> Adding Requirement to travel transactions")
migrate_existing_travel_data()
print("---> Adding Requirement to health transactions")
migrate_existing_health_data()
