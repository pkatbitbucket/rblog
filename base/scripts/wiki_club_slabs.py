import sys
import time
import datetime
import os
from optparse import OptionParser

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from django.db import models
from wiki.models import *

usage = "usage: %prog [options] arg1"
parser = OptionParser(usage)

parser.add_option("-s", "--slabs", metavar="slabs",
                  help="comma separated slabs to club")
parser.add_option("-c", "--club_slab", metavar="club_slab",
                  help="slab with which all slabs need to be clubed")

(options, args) = parser.parse_args()

if not options.club_slab or not options.slabs:
    parser.error("Must specify slabs and club slab")

slabs = [Slab.objects.get(id=sl) for sl in options.slabs.split(",")]
club_slab = Slab.objects.get(id=options.club_slab)

for attr in AttributeList:
    model = attr[2]

    for slab in slabs:
        if getattr(getattr(club_slab, model), 'all', None):
            getattr(slab, model).add(*getattr(club_slab, model).all())
        else:
            setattr(slab, model, getattr(club_slab, model))
            slab.save()

    # Replicate MetaData
    wiki_model_string = attr[0]
    M = models.get_model('wiki', wiki_model_string)
    mdata = MetaData.objects.filter(
        slabs=club_slab, content_type=ContentType.objects.get_for_model(M))
    if mdata:
        md = mdata[0]
        for slab in slabs:
            md.slabs.add(slab)
