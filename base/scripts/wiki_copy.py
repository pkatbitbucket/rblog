import sys
import time
import datetime
import os
from optparse import OptionParser

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from django.db import models
from wiki.models import *

# python scripts/wiki_copy.py -f 'CriticalIllnessCoverage' -s 153 -c 142,143
# python scripts/wiki_copy.py -f 'StandardExclusion' -i 19 -c 140,141,142,143


def main():
    usage = "usage: %prog -f <feature name> -s <source slab id> -i <feature id> -c <comma separated slab ids to which to copy>"
    parser = OptionParser(usage)

    parser.add_option("-f", "--feature", type="string",
                      dest="feature", help="feature name to copy")
    parser.add_option("-i", "--featureid", type="string",
                      dest="fid", help="id of the feature to copy")
    parser.add_option("-s", "--slabid", type="string", dest="sid",
                      help="id of the slab from which the feature needs to be taken")
    parser.add_option("-c", "--slabs", type="string", dest="slabs",
                      help="comma separated slab-ids for which the feature needs to be copied")

    (options, args) = parser.parse_args()

    if not options.feature:
        parser.error("Must specify feature you want to copy")
    if not options.fid and not options.sid:
        parser.error("Must specify feature ID or slab ID")
    if options.fid and options.sid:
        parser.error("Must specify either the feature ID or the slab ID")
    if not options.slabs:
        parser.error(
            "Must specify slabs on which the feature needs to be copied to")

    fld = options.feature.strip()
    copy_to_slabs = [Slab.objects.get(id=int(sl.strip()))
                     for sl in options.slabs.split(",")]
    if options.fid:
        M = models.get_model('wiki', fld)
        print "Model:", fld
        mobj = M.objects.get(id=options.fid.strip())
        print "Objects found:", mobj

        if getattr(getattr(mobj, 'slabs'), 'all', None):
            print "Currently attached to slabs : ", ", ".join([str(s.id) for s in mobj.slabs.all()])

        cloned_obj = mobj.clone()
        model = ReverseAttributeDict[fld]
        for slab in copy_to_slabs:
            if getattr(getattr(slab, model), 'all', None):
                getattr(slab, model).clear()
                getattr(slab, model).add(cloned_obj)
            else:
                setattr(slab, model, cloned_obj)
                slab.save()

    if options.sid:
        slab = Slab.objects.get(id=options.sid.strip())
        print "Source Slab to copy from ", slab

        model = ReverseAttributeDict[fld]
        if getattr(getattr(slab, model), 'all', None):
            mobjs = getattr(slab, model).all()

        else:
            mobjs = [getattr(slab, model)]

        print "Objects to be cloned", mobjs
        # remove existing many_to_many relations
        model = ReverseAttributeDict[fld]
        for slab in copy_to_slabs:
            if getattr(getattr(slab, model), 'all', None):
                getattr(slab, model).clear()

        for mobj in mobjs:
            cloned_obj = mobj.clone()
            for slab in copy_to_slabs:
                if getattr(getattr(slab, model), 'all', None):
                    getattr(slab, model).add(cloned_obj)
                else:
                    setattr(slab, model, cloned_obj)
                    slab.save()

if __name__ == "__main__":
    main()
