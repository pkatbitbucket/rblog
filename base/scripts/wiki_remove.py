import sys
import time
import datetime
import os
from optparse import OptionParser

import envdir
envdir.open()
sys.path.extend(os.environ.get("PYTHONPATH", "").split(os.pathsep))

import coverfox_com

from django.db import models
from wiki.models import *

usage = "usage: %prog [options] arg1"
parser = OptionParser(usage)

parser.add_option("-z", action="store_true",
                  dest="remove_zombie", help="remove all zombies")
parser.add_option("-f", "--fields", metavar="fields",
                  help="comma separated fields to remove")
parser.add_option("-s", "--slabs", metavar="slabs",
                  help="comma separated slab ids")

(options, args) = parser.parse_args()

if options.remove_zombie:
    for attr in AttributeList:
        wiki_model_string = attr[0]
        wiki_var_string = attr[2]
        M = models.get_model('wiki', wiki_model_string)
        print "Found ", wiki_model_string, M.objects.all().count()
        for obj in M.objects.filter(slabs__isnull=True):
            obj.delete()
            print "Deleting :", obj

if options.fields:
    if not options.slabs:
        parser.error("Must specify slabs")

    slabs = [int(sl.strip()) for sl in options.slabs.split(",")]
    print "Slab_ids:", slabs
    for fd in options.fields.split(","):
        fld = fd.strip()
        M = models.get_model('wiki', fld)
        print "Model:", fld
        mobjs = M.objects.filter(slabs__in=slabs).distinct()
        print "Objects found:", len(mobjs)
        for mobj in mobjs:
            for slab_id in slabs:
                if slab_id in [sl.id for sl in mobj.slabs.all()]:
                    mobj.slabs.remove(Slab.objects.get(id=slab_id))

    print "Done"
