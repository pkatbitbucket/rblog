# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'Slab', fields ['health_product',
        # 'sum_assured', 'grade']
        db.delete_unique(
            u'wiki_slab', ['health_product_id', 'sum_assured', 'grade'])

        # Deleting model 'EyeCoverage'
        db.delete_table(u'wiki_eyecoverage')

        # Removing M2M table for field exclusions on 'EyeCoverage'
        db.delete_table('wiki_eyecoverage_exclusions')

        # Deleting model 'PrePostHospitalization'
        db.delete_table(u'wiki_preposthospitalization')

        # Deleting model 'WaitingPeriodGeneral'
        db.delete_table(u'wiki_waitingperiodgeneral')

        # Deleting model 'MetaData'
        db.delete_table(u'wiki_metadata')

        # Removing M2M table for field slabs on 'MetaData'
        db.delete_table('wiki_metadata_slabs')

        # Deleting model 'ConvalescenceBenefit'
        db.delete_table(u'wiki_convalescencebenefit')

        # Deleting model 'RestoreBenefits'
        db.delete_table(u'wiki_restorebenefits')

        # Deleting model 'StandardExclusion'
        db.delete_table(u'wiki_standardexclusion')

        # Removing M2M table for field terms on 'StandardExclusion'
        db.delete_table('wiki_standardexclusion_terms')

        # Deleting model 'Bonus'
        db.delete_table(u'wiki_bonus')

        # Deleting model 'LifeLongRenewability'
        db.delete_table(u'wiki_lifelongrenewability')

        # Deleting model 'HealthCheckup'
        db.delete_table(u'wiki_healthcheckup')

        # Deleting model 'Insurer'
        db.delete_table(u'wiki_insurer')

        # Deleting model 'Slab'
        db.delete_table(u'wiki_slab')

        # Removing M2M table for field hospital on 'Slab'
        db.delete_table('wiki_slab_hospital')

        # Deleting model 'NetworkHospitalDailyCash'
        db.delete_table(u'wiki_networkhospitaldailycash')

        # Deleting model 'MaternityCover'
        db.delete_table(u'wiki_maternitycover')

        # Deleting model 'Hospital'
        db.delete_table(u'wiki_hospital')

        # Deleting model 'State'
        db.delete_table(u'wiki_state')

        # Deleting model 'CriticalIllnessCoverage'
        db.delete_table(u'wiki_criticalillnesscoverage')

        # Removing M2M table for field terms on 'CriticalIllnessCoverage'
        db.delete_table('wiki_criticalillnesscoverage_terms')

        # Deleting model 'WaitingPeriod'
        db.delete_table(u'wiki_waitingperiod')

        # Removing M2M table for field pre_existing on 'WaitingPeriod'
        db.delete_table('wiki_waitingperiod_pre_existing')

        # Removing M2M table for field general on 'WaitingPeriod'
        db.delete_table('wiki_waitingperiod_general')

        # Deleting model 'HealthProduct'
        db.delete_table(u'wiki_healthproduct')

        # Removing M2M table for field rider_products on 'HealthProduct'
        db.delete_table('wiki_healthproduct_rider_products')

        # Deleting model 'NonNetworkHospitalDailyCash'
        db.delete_table(u'wiki_nonnetworkhospitaldailycash')

        # Deleting model 'Copay'
        db.delete_table(u'wiki_copay')

        # Deleting model 'Claims'
        db.delete_table(u'wiki_claims')

        # Deleting model 'FamilyCoverage'
        db.delete_table(u'wiki_familycoverage')

        # Deleting model 'Term'
        db.delete_table(u'wiki_term')

        # Deleting model 'AlternativeMedicalCoverage'
        db.delete_table(u'wiki_alternativemedicalcoverage')

        # Removing M2M table for field slabs on 'AlternativeMedicalCoverage'
        db.delete_table('wiki_alternativemedicalcoverage_slabs')

        # Deleting model 'RoomRent'
        db.delete_table(u'wiki_roomrent')

        # Deleting model 'DomiciliaryHospitalization'
        db.delete_table(u'wiki_domiciliaryhospitalization')

        # Removing M2M table for field exclusions on
        # 'DomiciliaryHospitalization'
        db.delete_table('wiki_domiciliaryhospitalization_exclusions')

        # Deleting model 'AmbulanceCharges'
        db.delete_table(u'wiki_ambulancecharges')

        # Deleting model 'AlternativeMedical'
        db.delete_table(u'wiki_alternativemedical')

        # Deleting model 'ProcedureCovered'
        db.delete_table(u'wiki_procedurecovered')

        # Deleting model 'City'
        db.delete_table(u'wiki_city')

        # Deleting model 'DayCare'
        db.delete_table(u'wiki_daycare')

        # Removing M2M table for field procedure_covered on 'DayCare'
        db.delete_table('wiki_daycare_procedure_covered')

        # Deleting model 'OnlineAvailability'
        db.delete_table(u'wiki_onlineavailability')

        # Deleting model 'CoverPremiumTable'
        db.delete_table(u'wiki_coverpremiumtable')

        # Removing M2M table for field slabs on 'CoverPremiumTable'
        db.delete_table('wiki_coverpremiumtable_slabs')

        # Deleting model 'AgeLimit'
        db.delete_table(u'wiki_agelimit')

        # Deleting model 'OutpatientBenefits'
        db.delete_table(u'wiki_outpatientbenefits')

        # Deleting model 'RoomTypeCopay'
        db.delete_table(u'wiki_roomtypecopay')

        # Removing M2M table for field slabs on 'RoomTypeCopay'
        db.delete_table('wiki_roomtypecopay_slabs')

        # Deleting model 'WaitingPeriodPreExisting'
        db.delete_table(u'wiki_waitingperiodpreexisting')

        # Deleting model 'CancellationPolicy'
        db.delete_table(u'wiki_cancellationpolicy')

        # Removing M2M table for field slabs on 'CancellationPolicy'
        db.delete_table('wiki_cancellationpolicy_slabs')

        # Deleting model 'FamilyCoverPremiumTable'
        db.delete_table(u'wiki_familycoverpremiumtable')

        # Removing M2M table for field slabs on 'FamilyCoverPremiumTable'
        db.delete_table('wiki_familycoverpremiumtable_slabs')

        # Deleting model 'DentalCoverage'
        db.delete_table(u'wiki_dentalcoverage')

        # Removing M2M table for field exclusions on 'DentalCoverage'
        db.delete_table('wiki_dentalcoverage_exclusions')

    def backwards(self, orm):
        # Adding model 'EyeCoverage'
        db.create_table(u'wiki_eyecoverage', (
            ('waiting_period', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('copay', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('limit', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('condition', self.gf('django.db.models.fields.CharField')(
                max_length=255, blank=True)),
        ))
        db.send_create_signal(u'wiki', ['EyeCoverage'])

        # Adding M2M table for field exclusions on 'EyeCoverage'
        db.create_table(u'wiki_eyecoverage_exclusions', (
            ('id', models.AutoField(verbose_name='ID',
                                    primary_key=True, auto_created=True)),
            ('eyecoverage', models.ForeignKey(
                orm[u'wiki.eyecoverage'], null=False)),
            ('term', models.ForeignKey(orm[u'wiki.term'], null=False))
        ))
        db.create_unique(u'wiki_eyecoverage_exclusions',
                         ['eyecoverage_id', 'term_id'])

        # Adding model 'PrePostHospitalization'
        db.create_table(u'wiki_preposthospitalization', (
            ('days_post_hospitalization', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('days_post_hospitalization_with_intimation', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('intimation_time', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('is_available', self.gf('django.db.models.fields.NullBooleanField')(
                default=False, null=True, blank=True)),
            ('days_pre_hospitalization', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('days_pre_hospitalization_with_intimation', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'wiki', ['PrePostHospitalization'])

        # Adding model 'WaitingPeriodGeneral'
        db.create_table(u'wiki_waitingperiodgeneral', (
            ('code', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('term', self.gf('django.db.models.fields.related.ForeignKey')(
                to=orm['wiki.Term'])),
            ('limit', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('verbose', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('limit_in_percentage', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('copay', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('period', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
        ))
        db.send_create_signal(u'wiki', ['WaitingPeriodGeneral'])

        # Adding model 'MetaData'
        db.create_table(u'wiki_metadata', (
            ('code', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('verbose', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(
                to=orm['contenttypes.ContentType'])),
            ('is_completed', self.gf('django.db.models.fields.NullBooleanField')(
                null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('comments', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('text_to_display', self.gf(
                'django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'wiki', ['MetaData'])

        # Adding M2M table for field slabs on 'MetaData'
        db.create_table(u'wiki_metadata_slabs', (
            ('id', models.AutoField(verbose_name='ID',
                                    primary_key=True, auto_created=True)),
            ('metadata', models.ForeignKey(orm[u'wiki.metadata'], null=False)),
            ('slab', models.ForeignKey(orm[u'wiki.slab'], null=False))
        ))
        db.create_unique(u'wiki_metadata_slabs', ['metadata_id', 'slab_id'])

        # Adding model 'ConvalescenceBenefit'
        db.create_table(u'wiki_convalescencebenefit', (
            ('minimum_days_of_hospitalization', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('limit', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('number_of_days', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('condition', self.gf('django.db.models.fields.CharField')(
                max_length=255, blank=True)),
        ))
        db.send_create_signal(u'wiki', ['ConvalescenceBenefit'])

        # Adding model 'RestoreBenefits'
        db.create_table(u'wiki_restorebenefits', (
            ('is_available', self.gf('django.db.models.fields.NullBooleanField')(
                default=False, null=True, blank=True)),
            ('amount_restored_percentage', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('amount_restored', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'wiki', ['RestoreBenefits'])

        # Adding model 'StandardExclusion'
        db.create_table(u'wiki_standardexclusion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'wiki', ['StandardExclusion'])

        # Adding M2M table for field terms on 'StandardExclusion'
        db.create_table(u'wiki_standardexclusion_terms', (
            ('id', models.AutoField(verbose_name='ID',
                                    primary_key=True, auto_created=True)),
            ('standardexclusion', models.ForeignKey(
                orm[u'wiki.standardexclusion'], null=False)),
            ('term', models.ForeignKey(orm[u'wiki.term'], null=False))
        ))
        db.create_unique(u'wiki_standardexclusion_terms', [
                         'standardexclusion_id', 'term_id'])

        # Adding model 'Bonus'
        db.create_table(u'wiki_bonus', (
            ('bonus_amount', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('maximum_bonus_amount', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('is_available', self.gf('django.db.models.fields.NullBooleanField')(
                default=False, null=True, blank=True)),
            ('maximum_years', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('unclaimed_years', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('reduction_in_bonus_amount_after_claim', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'wiki', ['Bonus'])

        # Adding model 'LifeLongRenewability'
        db.create_table(u'wiki_lifelongrenewability', (
            ('age_limit', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('condition', self.gf('django.db.models.fields.CharField')(
                max_length=255, blank=True)),
        ))
        db.send_create_signal(u'wiki', ['LifeLongRenewability'])

        # Adding model 'HealthCheckup'
        db.create_table(u'wiki_healthcheckup', (
            ('claim_free_years', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('limit', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('limit_in_percentage', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('is_available', self.gf('django.db.models.fields.NullBooleanField')(
                default=False, null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'wiki', ['HealthCheckup'])

        # Adding model 'Insurer'
        db.create_table(u'wiki_insurer', (
            ('body', self.gf('django.db.models.fields.TextField')()),
            ('status', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(
                to=orm['core.User'], null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('logo', self.gf('customdb.thumbs.ImageWithThumbsField')(
                max_length=100, null=True, blank=True)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=70)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('tease', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(
                auto_now=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(
                auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'wiki', ['Insurer'])

        # Adding model 'Slab'
        db.create_table(u'wiki_slab', (
            ('health_checkup', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.HealthCheckup'], blank=True)),
            ('bonus', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.Bonus'], blank=True)),
            ('dental_coverage', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.DentalCoverage'], blank=True)),
            ('online_availability', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.OnlineAvailability'], blank=True)),
            ('family_coverage', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.FamilyCoverage'], blank=True)),
            ('sum_assured', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('standard_exclusion', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.StandardExclusion'], blank=True)),
            ('network_hospital_daily_cash', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.NetworkHospitalDailyCash'], blank=True)),
            ('restore_benefits', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.RestoreBenefits'], blank=True)),
            ('convalescence_benefit', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.ConvalescenceBenefit'], blank=True)),
            ('non_network_hospital_daily_cash', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.NonNetworkHospitalDailyCash'], blank=True)),
            ('waiting_period_at_inception_of_ci', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('room_rent', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.RoomRent'], blank=True)),
            ('ambulance_charges', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.AmbulanceCharges'], blank=True)),
            ('maternity_cover', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.MaternityCover'], blank=True)),
            ('waiting_period', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.WaitingPeriod'], blank=True)),
            ('eye_coverage', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.EyeCoverage'], blank=True)),
            ('pre_post_hospitalization', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.PrePostHospitalization'], blank=True)),
            ('copay', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.Copay'], blank=True)),
            ('survival_period_for_ci_after_diagnosis', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('grade', self.gf('django.db.models.fields.CharField')(
                max_length=255, blank=True)),
            ('domiciliary_hospitalization', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.DomiciliaryHospitalization'], blank=True)),
            ('health_product', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', to=orm['wiki.HealthProduct'])),
            ('claims', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.Claims'], blank=True)),
            ('day_care', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.DayCare'], blank=True)),
            ('age_limit', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.AgeLimit'], blank=True)),
            ('outpatient_benefits', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.OutpatientBenefits'], blank=True)),
            ('critical_illness_coverage', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.CriticalIllnessCoverage'], blank=True)),
            ('life_long_renewability', self.gf('django.db.models.fields.related.ForeignKey')(
                related_name='slabs', null=True, to=orm['wiki.LifeLongRenewability'], blank=True)),
        ))
        db.send_create_signal(u'wiki', ['Slab'])

        # Adding M2M table for field hospital on 'Slab'
        db.create_table(u'wiki_slab_hospital', (
            ('id', models.AutoField(verbose_name='ID',
                                    primary_key=True, auto_created=True)),
            ('slab', models.ForeignKey(orm[u'wiki.slab'], null=False)),
            ('hospital', models.ForeignKey(orm[u'wiki.hospital'], null=False))
        ))
        db.create_unique(u'wiki_slab_hospital', ['slab_id', 'hospital_id'])

        # Adding unique constraint on 'Slab', fields ['health_product',
        # 'sum_assured', 'grade']
        db.create_unique(
            u'wiki_slab', ['health_product_id', 'sum_assured', 'grade'])

        # Adding model 'NetworkHospitalDailyCash'
        db.create_table(u'wiki_networkhospitaldailycash', (
            ('limit_per_day_allowance', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('from_day', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('is_available', self.gf('django.db.models.fields.NullBooleanField')(
                default=False, null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('condition', self.gf('django.db.models.fields.CharField')(
                max_length=255, blank=True)),
            ('is_icu_available', self.gf('django.db.models.fields.NullBooleanField')(
                default=False, null=True, blank=True)),
            ('limit_cummulative_allowance', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('to_day', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('number_of_days', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('limit_per_day_allowance_icu', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'wiki', ['NetworkHospitalDailyCash'])

        # Adding model 'MaternityCover'
        db.create_table(u'wiki_maternitycover', (
            ('waiting_period', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('limit_for_cesarean_delivery', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('limit_for_normal_delivery', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('is_covered', self.gf('django.db.models.fields.NullBooleanField')(
                null=True, blank=True)),
            ('delivery_limit', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'wiki', ['MaternityCover'])

        # Adding model 'Hospital'
        db.create_table(u'wiki_hospital', (
            ('city', self.gf('django.db.models.fields.related.ForeignKey')(
                to=orm['wiki.City'])),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pin', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'wiki', ['Hospital'])

        # Adding model 'State'
        db.create_table(u'wiki_state', (
            ('is_enabled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'wiki', ['State'])

        # Adding model 'CriticalIllnessCoverage'
        db.create_table(u'wiki_criticalillnesscoverage', (
            ('limit', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('limit_in_percentage', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('is_available', self.gf('django.db.models.fields.NullBooleanField')(
                null=True, blank=True)),
            ('included_in_plan', self.gf(
                'django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'wiki', ['CriticalIllnessCoverage'])

        # Adding M2M table for field terms on 'CriticalIllnessCoverage'
        db.create_table(u'wiki_criticalillnesscoverage_terms', (
            ('id', models.AutoField(verbose_name='ID',
                                    primary_key=True, auto_created=True)),
            ('criticalillnesscoverage', models.ForeignKey(
                orm[u'wiki.criticalillnesscoverage'], null=False)),
            ('term', models.ForeignKey(orm[u'wiki.term'], null=False))
        ))
        db.create_unique(u'wiki_criticalillnesscoverage_terms', [
                         'criticalillnesscoverage_id', 'term_id'])

        # Adding model 'WaitingPeriod'
        db.create_table(u'wiki_waitingperiod', (
            ('basic_period_for_pre_existing', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('portability_of_waiting_period', self.gf(
                'django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('basic_period_for_general', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('portability_of_pre_existing_waiting_period', self.gf(
                'django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'wiki', ['WaitingPeriod'])

        # Adding M2M table for field pre_existing on 'WaitingPeriod'
        db.create_table(u'wiki_waitingperiod_pre_existing', (
            ('id', models.AutoField(verbose_name='ID',
                                    primary_key=True, auto_created=True)),
            ('waitingperiod', models.ForeignKey(
                orm[u'wiki.waitingperiod'], null=False)),
            ('waitingperiodpreexisting', models.ForeignKey(
                orm[u'wiki.waitingperiodpreexisting'], null=False))
        ))
        db.create_unique(u'wiki_waitingperiod_pre_existing', [
                         'waitingperiod_id', 'waitingperiodpreexisting_id'])

        # Adding M2M table for field general on 'WaitingPeriod'
        db.create_table(u'wiki_waitingperiod_general', (
            ('id', models.AutoField(verbose_name='ID',
                                    primary_key=True, auto_created=True)),
            ('waitingperiod', models.ForeignKey(
                orm[u'wiki.waitingperiod'], null=False)),
            ('waitingperiodgeneral', models.ForeignKey(
                orm[u'wiki.waitingperiodgeneral'], null=False))
        ))
        db.create_unique(u'wiki_waitingperiod_general', [
                         'waitingperiod_id', 'waitingperiodgeneral_id'])

        # Adding model 'HealthProduct'
        db.create_table(u'wiki_healthproduct', (
            ('body', self.gf('django.db.models.fields.TextField')()),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=70)),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(
                to=orm['core.User'], null=True, blank=True)),
            ('policy_wordings', self.gf('django.db.models.fields.files.FileField')(
                max_length=100, null=True, blank=True)),
            ('notes', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('claim_form', self.gf('django.db.models.fields.files.FileField')(
                max_length=100, null=True, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(
                auto_now=True, blank=True)),
            ('proposal_form', self.gf('django.db.models.fields.files.FileField')(
                max_length=100, null=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(
                auto_now_add=True, blank=True)),
            ('coverage_category', self.gf('django.db.models.fields.CharField')(
                max_length=255, blank=True)),
            ('insurer', self.gf('django.db.models.fields.related.ForeignKey')(
                to=orm['wiki.Insurer'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('policy_type', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('brochure', self.gf('django.db.models.fields.files.FileField')(
                max_length=100, null=True, blank=True)),
            ('logo', self.gf('customdb.thumbs.ImageWithThumbsField')(
                max_length=100, null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'wiki', ['HealthProduct'])

        # Adding M2M table for field rider_products on 'HealthProduct'
        db.create_table(u'wiki_healthproduct_rider_products', (
            ('id', models.AutoField(verbose_name='ID',
                                    primary_key=True, auto_created=True)),
            ('from_healthproduct', models.ForeignKey(
                orm[u'wiki.healthproduct'], null=False)),
            ('to_healthproduct', models.ForeignKey(
                orm[u'wiki.healthproduct'], null=False))
        ))
        db.create_unique(u'wiki_healthproduct_rider_products', [
                         'from_healthproduct_id', 'to_healthproduct_id'])

        # Adding model 'NonNetworkHospitalDailyCash'
        db.create_table(u'wiki_nonnetworkhospitaldailycash', (
            ('limit_per_day_allowance', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('from_day', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('is_available', self.gf('django.db.models.fields.NullBooleanField')(
                default=False, null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('condition', self.gf('django.db.models.fields.CharField')(
                max_length=255, blank=True)),
            ('is_icu_available', self.gf('django.db.models.fields.NullBooleanField')(
                default=False, null=True, blank=True)),
            ('limit_cummulative_allowance', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('to_day', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('number_of_days', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('limit_per_day_allowance_icu', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'wiki', ['NonNetworkHospitalDailyCash'])

        # Adding model 'Copay'
        db.create_table(u'wiki_copay', (
            ('on_day_care_for_network_hospital', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('on_day_care_for_non_network_hospital', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('for_network_hospital', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('applicable_after_age', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('for_non_network_hospital', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'wiki', ['Copay'])

        # Adding model 'Claims'
        db.create_table(u'wiki_claims', (
            ('maximum_days_to_intimate', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('service_promise', self.gf('django.db.models.fields.CharField')(
                max_length=255, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'wiki', ['Claims'])

        # Adding model 'FamilyCoverage'
        db.create_table(u'wiki_familycoverage', (
            ('number_of_maximum_members', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('maximum_dependent_age', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'wiki', ['FamilyCoverage'])

        # Adding model 'Term'
        db.create_table(u'wiki_term', (
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('ib_wp_ped', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('ib_eye_coverage', self.gf(
                'django.db.models.fields.BooleanField')(default=False)),
            ('ib_wp_general', self.gf(
                'django.db.models.fields.BooleanField')(default=False)),
            ('term_type', self.gf('django.db.models.fields.CharField')(
                max_length=100, null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ib_standard_exclusions', self.gf(
                'django.db.models.fields.BooleanField')(default=False)),
            ('ib_domiciliary', self.gf(
                'django.db.models.fields.BooleanField')(default=False)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('ib_dental_coverage', self.gf(
                'django.db.models.fields.BooleanField')(default=False)),
            ('ib_day_care', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('ib_critical_illness', self.gf(
                'django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'wiki', ['Term'])

        # Adding model 'AlternativeMedicalCoverage'
        db.create_table(u'wiki_alternativemedicalcoverage', (
            ('is_covered', self.gf('django.db.models.fields.NullBooleanField')(
                null=True, blank=True)),
            ('alternative_medical', self.gf('django.db.models.fields.related.ForeignKey')(
                to=orm['wiki.AlternativeMedical'])),
            ('limit', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'wiki', ['AlternativeMedicalCoverage'])

        # Adding M2M table for field slabs on 'AlternativeMedicalCoverage'
        db.create_table(u'wiki_alternativemedicalcoverage_slabs', (
            ('id', models.AutoField(verbose_name='ID',
                                    primary_key=True, auto_created=True)),
            ('alternativemedicalcoverage', models.ForeignKey(
                orm[u'wiki.alternativemedicalcoverage'], null=False)),
            ('slab', models.ForeignKey(orm[u'wiki.slab'], null=False))
        ))
        db.create_unique(u'wiki_alternativemedicalcoverage_slabs', [
                         'alternativemedicalcoverage_id', 'slab_id'])

        # Adding model 'RoomRent'
        db.create_table(u'wiki_roomrent', (
            ('limit_daily_in_icu', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('limit_daily', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('limit_daily_in_percentage_in_icu', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('limit_maximum_total_in_icu', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('conditions', self.gf('django.db.models.fields.CharField')(
                max_length=255, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('limit_maximum_total', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('limit_daily_in_percentage', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'wiki', ['RoomRent'])

        # Adding model 'DomiciliaryHospitalization'
        db.create_table(u'wiki_domiciliaryhospitalization', (
            ('is_covered', self.gf('django.db.models.fields.NullBooleanField')(
                default=False, null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'wiki', ['DomiciliaryHospitalization'])

        # Adding M2M table for field exclusions on 'DomiciliaryHospitalization'
        db.create_table(u'wiki_domiciliaryhospitalization_exclusions', (
            ('id', models.AutoField(verbose_name='ID',
                                    primary_key=True, auto_created=True)),
            ('domiciliaryhospitalization', models.ForeignKey(
                orm[u'wiki.domiciliaryhospitalization'], null=False)),
            ('term', models.ForeignKey(orm[u'wiki.term'], null=False))
        ))
        db.create_unique(u'wiki_domiciliaryhospitalization_exclusions', [
                         'domiciliaryhospitalization_id', 'term_id'])

        # Adding model 'AmbulanceCharges'
        db.create_table(u'wiki_ambulancecharges', (
            ('is_ambulance_services_covered', self.gf(
                'django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('limit_in_non_network_hospitals', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('limit_in_network_hospitals', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('condition', self.gf('django.db.models.fields.CharField')(
                max_length=255, blank=True)),
        ))
        db.send_create_signal(u'wiki', ['AmbulanceCharges'])

        # Adding model 'AlternativeMedical'
        db.create_table(u'wiki_alternativemedical', (
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'wiki', ['AlternativeMedical'])

        # Adding model 'ProcedureCovered'
        db.create_table(u'wiki_procedurecovered', (
            ('code', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('term', self.gf('django.db.models.fields.related.ForeignKey')(
                to=orm['wiki.Term'])),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('verbose', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'wiki', ['ProcedureCovered'])

        # Adding model 'City'
        db.create_table(u'wiki_city', (
            ('is_enabled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('is_major', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('state', self.gf('django.db.models.fields.related.ForeignKey')(
                to=orm['wiki.State'])),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_approved', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'wiki', ['City'])

        # Adding model 'DayCare'
        db.create_table(u'wiki_daycare', (
            ('is_covered', self.gf('django.db.models.fields.NullBooleanField')(
                null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number_of_covered_procedures', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'wiki', ['DayCare'])

        # Adding M2M table for field procedure_covered on 'DayCare'
        db.create_table(u'wiki_daycare_procedure_covered', (
            ('id', models.AutoField(verbose_name='ID',
                                    primary_key=True, auto_created=True)),
            ('daycare', models.ForeignKey(orm[u'wiki.daycare'], null=False)),
            ('procedurecovered', models.ForeignKey(
                orm[u'wiki.procedurecovered'], null=False))
        ))
        db.create_unique(u'wiki_daycare_procedure_covered', [
                         'daycare_id', 'procedurecovered_id'])

        # Adding model 'OnlineAvailability'
        db.create_table(u'wiki_onlineavailability', (
            ('is_available_offline_only', self.gf(
                'django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('maximum_age', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
        ))
        db.send_create_signal(u'wiki', ['OnlineAvailability'])

        # Adding model 'CoverPremiumTable'
        db.create_table(u'wiki_coverpremiumtable', (
            ('premium', self.gf('django.db.models.fields.FloatField')()),
            ('location', self.gf('django.db.models.fields.CharField')(
                max_length=255, blank=True)),
            ('upper_age_limit', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('specific_location', self.gf('django.db.models.fields.CharField')(
                max_length=255, blank=True)),
            ('gender', self.gf('django.db.models.fields.CharField')(
                max_length=100, null=True, blank=True)),
            ('two_year_discount', self.gf(
                'django.db.models.fields.FloatField')(null=True, blank=True)),
            ('lower_age_limit', self.gf('django.db.models.fields.IntegerField')()),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'wiki', ['CoverPremiumTable'])

        # Adding M2M table for field slabs on 'CoverPremiumTable'
        db.create_table(u'wiki_coverpremiumtable_slabs', (
            ('id', models.AutoField(verbose_name='ID',
                                    primary_key=True, auto_created=True)),
            ('coverpremiumtable', models.ForeignKey(
                orm[u'wiki.coverpremiumtable'], null=False)),
            ('slab', models.ForeignKey(orm[u'wiki.slab'], null=False))
        ))
        db.create_unique(u'wiki_coverpremiumtable_slabs', [
                         'coverpremiumtable_id', 'slab_id'])

        # Adding model 'AgeLimit'
        db.create_table(u'wiki_agelimit', (
            ('minimum_entry_age', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('maximum_entry_age', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'wiki', ['AgeLimit'])

        # Adding model 'OutpatientBenefits'
        db.create_table(u'wiki_outpatientbenefits', (
            ('is_covered', self.gf('django.db.models.fields.NullBooleanField')(
                default=False, null=True, blank=True)),
            ('limit', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number_of_consultations', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'wiki', ['OutpatientBenefits'])

        # Adding model 'RoomTypeCopay'
        db.create_table(u'wiki_roomtypecopay', (
            ('copay', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('applicable_for_hospital', self.gf(
                'django.db.models.fields.CharField')(max_length=100)),
            ('room_type', self.gf('django.db.models.fields.CharField')(
                max_length=255, blank=True)),
            ('is_available', self.gf('django.db.models.fields.NullBooleanField')(
                default=False, null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'wiki', ['RoomTypeCopay'])

        # Adding M2M table for field slabs on 'RoomTypeCopay'
        db.create_table(u'wiki_roomtypecopay_slabs', (
            ('id', models.AutoField(verbose_name='ID',
                                    primary_key=True, auto_created=True)),
            ('roomtypecopay', models.ForeignKey(
                orm[u'wiki.roomtypecopay'], null=False)),
            ('slab', models.ForeignKey(orm[u'wiki.slab'], null=False))
        ))
        db.create_unique(u'wiki_roomtypecopay_slabs', [
                         'roomtypecopay_id', 'slab_id'])

        # Adding model 'WaitingPeriodPreExisting'
        db.create_table(u'wiki_waitingperiodpreexisting', (
            ('code', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('term', self.gf('django.db.models.fields.related.ForeignKey')(
                to=orm['wiki.Term'])),
            ('limit', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('verbose', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('limit_in_percentage', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('copay', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('period', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
        ))
        db.send_create_signal(u'wiki', ['WaitingPeriodPreExisting'])

        # Adding model 'CancellationPolicy'
        db.create_table(u'wiki_cancellationpolicy', (
            ('refund_in_percentage', self.gf(
                'django.db.models.fields.IntegerField')()),
            ('period', self.gf('django.db.models.fields.IntegerField')()),
            ('policy_years', self.gf('django.db.models.fields.IntegerField')()),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'wiki', ['CancellationPolicy'])

        # Adding M2M table for field slabs on 'CancellationPolicy'
        db.create_table(u'wiki_cancellationpolicy_slabs', (
            ('id', models.AutoField(verbose_name='ID',
                                    primary_key=True, auto_created=True)),
            ('cancellationpolicy', models.ForeignKey(
                orm[u'wiki.cancellationpolicy'], null=False)),
            ('slab', models.ForeignKey(orm[u'wiki.slab'], null=False))
        ))
        db.create_unique(u'wiki_cancellationpolicy_slabs', [
                         'cancellationpolicy_id', 'slab_id'])

        # Adding model 'FamilyCoverPremiumTable'
        db.create_table(u'wiki_familycoverpremiumtable', (
            ('premium', self.gf('django.db.models.fields.FloatField')()),
            ('adults', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('two_year_discount', self.gf(
                'django.db.models.fields.FloatField')(null=True, blank=True)),
            ('specific_location', self.gf('django.db.models.fields.CharField')(
                max_length=255, blank=True)),
            ('upper_age_limit', self.gf(
                'django.db.models.fields.IntegerField')(null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('kids', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('gender', self.gf('django.db.models.fields.CharField')(
                max_length=100, null=True, blank=True)),
            ('location', self.gf('django.db.models.fields.CharField')(
                max_length=255, blank=True)),
            ('lower_age_limit', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'wiki', ['FamilyCoverPremiumTable'])

        # Adding M2M table for field slabs on 'FamilyCoverPremiumTable'
        db.create_table(u'wiki_familycoverpremiumtable_slabs', (
            ('id', models.AutoField(verbose_name='ID',
                                    primary_key=True, auto_created=True)),
            ('familycoverpremiumtable', models.ForeignKey(
                orm[u'wiki.familycoverpremiumtable'], null=False)),
            ('slab', models.ForeignKey(orm[u'wiki.slab'], null=False))
        ))
        db.create_unique(u'wiki_familycoverpremiumtable_slabs', [
                         'familycoverpremiumtable_id', 'slab_id'])

        # Adding model 'DentalCoverage'
        db.create_table(u'wiki_dentalcoverage', (
            ('waiting_period', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('copay', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('limit', self.gf('django.db.models.fields.IntegerField')(
                null=True, blank=True)),
            ('condition', self.gf('django.db.models.fields.CharField')(
                max_length=255, blank=True)),
        ))
        db.send_create_signal(u'wiki', ['DentalCoverage'])

        # Adding M2M table for field exclusions on 'DentalCoverage'
        db.create_table(u'wiki_dentalcoverage_exclusions', (
            ('id', models.AutoField(verbose_name='ID',
                                    primary_key=True, auto_created=True)),
            ('dentalcoverage', models.ForeignKey(
                orm[u'wiki.dentalcoverage'], null=False)),
            ('term', models.ForeignKey(orm[u'wiki.term'], null=False))
        ))
        db.create_unique(u'wiki_dentalcoverage_exclusions',
                         ['dentalcoverage_id', 'term_id'])

    models = {

    }

    complete_apps = ['wiki']
