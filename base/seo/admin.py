from django.contrib import admin
from seo.models import *


class WordListAdmin(admin.ModelAdmin):
    pass
admin.site.register(WordList, WordListAdmin)


class TaxonomyAdmin(admin.ModelAdmin):
    pass
admin.site.register(Taxonomy, TaxonomyAdmin)
