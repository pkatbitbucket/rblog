from django import forms
from django.apps import apps

from cf_fhurl import RequestModelForm
import putils
from seo.models import SEOMetadata, PhraseList


class ObjectSEOForm(RequestModelForm):
    keywords = forms.CharField(widget=forms.Textarea, required=False)

    class Meta:
        model = SEOMetadata
        exclude = ('content_type', 'object_id', 'content_object', 'analysis')

    def __init__(self, *args, **kwargs):
        super(ObjectSEOForm, self).__init__(*args, **kwargs)
        if self.instance and self.instance.id:
            self.initial.update({'keywords': ",".join(
                [k.keyword for k in self.instance.keywords.all()])})

    def clean_keywords(self):
        cleaned_kw = []
        keywords = [w.strip() for w in self.cleaned_data.get(
            'keywords', '').split(",") if w]
        try:
            for kw in keywords:
                cleaned_kw.append(PhraseList.objects.get(keyword=kw))
        except Exception, e:
            raise forms.ValidationError("%s - %s" % (kw, e))
        return cleaned_kw

    def init(self, app, model, object_id):
        self.prefix = "seo"
        M = apps.get_model(app.lower(), model.title())
        obj = M.objects.get(id=object_id)
        self.obj = obj
        instance = putils.get_seo(obj)
        if instance:
            self.instance = instance

    def save(self):
        seo_data = putils.save_seo(self.obj, **self.cleaned_data)
        return {'success': True, 'redirect': True}
