# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='PhraseList',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('keyword', models.CharField(unique=True,
                                             max_length=255, verbose_name='keyword', db_index=True)),
                ('frequency', models.PositiveIntegerField(verbose_name='frequency')),
                ('group', models.ForeignKey(related_name='parent',
                                            blank=True, to='seo.PhraseList', null=True)),
                ('sub_group', models.ForeignKey(related_name='sub_parent',
                                                blank=True, to='seo.PhraseList', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='SEOMetadata',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255,
                                           verbose_name='title', blank=True)),
                ('description', models.TextField(
                    verbose_name='description', blank=True)),
                ('raw', models.TextField(verbose_name='Raw Html', blank=True)),
                ('analysis', models.TextField(
                    verbose_name='Analysis Result', blank=True)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('keywords', models.ManyToManyField(
                    to='seo.PhraseList', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Taxonomy',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('relation_type', models.CharField(max_length=10, verbose_name='relation type', choices=[
                 (b'SYNONYM', b'Synonym'), (b'SIBLING', b'Sibling')])),
            ],
        ),
        migrations.CreateModel(
            name='WordList',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('keyword', models.CharField(unique=True,
                                             max_length=255, verbose_name='keyword', db_index=True)),
            ],
        ),
        migrations.AddField(
            model_name='taxonomy',
            name='words',
            field=models.ManyToManyField(to='seo.WordList'),
        ),
    ]
