from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey


class Taxonomy(models.Model):
    RELATION_TYPE_CHOICES = (
        ('SYNONYM', 'Synonym'),
        ('SIBLING', 'Sibling'),
    )
    words = models.ManyToManyField('WordList')
    relation_type = models.CharField(
        _('relation type'), choices=RELATION_TYPE_CHOICES, max_length=10)


class WordList(models.Model):
    """Seo Word list generated through parsing google adwords"""
    keyword = models.CharField(
        _('keyword'), max_length=255, unique=True, db_index=True)

    def __unicode__(self):
        return u'%s' % self.keyword


class PhraseList(models.Model):
    """Seo Phrase list generated through google adwords"""

    keyword = models.CharField(
        _('keyword'), max_length=255, unique=True, db_index=True)
    group = models.ForeignKey(
        "self", related_name='parent', blank=True, null=True)
    sub_group = models.ForeignKey(
        "self", blank=True, null=True, related_name='sub_parent')
    frequency = models.PositiveIntegerField(_('frequency'))
    # Frequency chart :
    # 0 for group words
    # 1 for sub_group words

    def __unicode__(self):
        return u'%s' % self.keyword


class SEOMetadata(models.Model):
    title = models.CharField(_('title'), max_length=255, blank=True)
    description = models.TextField(_('description'), blank=True)
    keywords = models.ManyToManyField(PhraseList, blank=True)
    raw = models.TextField(_('Raw Html'), blank=True)
    analysis = models.TextField(_('Analysis Result'), blank=True)

    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
