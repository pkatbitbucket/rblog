from django.conf.urls import url

from . import views

urlpatterns = [
    # url for testing flat pages
    url(r'^$', views.index, name="seo_index"),
    url(r'^ajax/seo/get-keywords/$', views.get_keywords, name='get_seo_keywords'),
]
