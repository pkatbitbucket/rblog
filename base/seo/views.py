import json

from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response

from seo.models import PhraseList, WordList, Taxonomy
import utils


@utils.profile_type_only('AUTHOR')
def index(request):
    return render_to_response("seo/wordlist.html", {
        'wordlist': json.dumps([{'wordid': w.id, 'keyword': w.keyword} for w in WordList.objects.all()]),
        'phraselist': json.dumps([{'wordid': w.id, 'keyword': w.keyword, 'frequency': w.frequency} for w in PhraseList.objects.all()]),
        'taxonomylist': json.dumps([{'taxonomyid': t.id, 'relation_type': t.relation_type, 'words': [tw.id for tw in t.words.all()]} for t in Taxonomy.objects.all()]),
    },
        context_instance=RequestContext(request)
    )


@utils.profile_type_only('AUTHOR')
def get_keywords(request):
    term = request.GET.get('term')
    tags = [p.keyword for p in PhraseList.objects.filter(
        keyword__icontains=term.lower())]
    return HttpResponse(json.dumps(tags))
