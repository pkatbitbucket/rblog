# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Vehicle',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('brand', models.CharField(max_length=50)),
                ('modelName', models.CharField(max_length=50)),
                ('page', models.ForeignKey(to='cms.Page')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='vehicle',
            unique_together=set([('brand', 'modelName')]),
        ),
    ]
