from django.db import models
from cms.models import Page


class Vehicle(models.Model):
    brand = models.CharField(max_length=50)
    modelName = models.CharField(max_length=50, db_column='modelname')
    page = models.ForeignKey(Page)

    def __unicode__(self):
        return self.brand + " " + self.modelName

    class Meta:
        unique_together = ("brand", "modelName")
