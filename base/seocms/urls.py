from django.conf.urls import url
from django.views.generic.base import RedirectView

from . import views

urlpatterns = [
    url(r'^car-insurance/(?P<slug>[\w-]+)-car-insurance/cashless-garages/$',
        views.insurer_garages, name='insurer_garages'),
    url(r'^car-insurance/(?P<slug>[\w-]+)/cashless-garages/$',
        RedirectView.as_view(permanent=True, pattern_name='insurer_garages'), name='insurer_garages_old'),
    url(r'^car-insurance/(?P<brand>[\w-]+)/(?P<modelName>[\w-]+)/$',
        views.vehiclemodel, {'vertical': 'car-insurance'}, name='car_model'),
    url(r'^two-wheeler-insurance/(?P<brand>[\w-]+)/(?P<modelName>[\w-]+)/$',
        views.vehiclemodel, {'vertical': 'two-wheeler-insurance'}, name='two_wheeler_model'),
]
