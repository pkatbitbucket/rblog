from django.shortcuts import render, redirect, get_object_or_404
from django.http import Http404
from django.template import TemplateDoesNotExist
from brands.models import BrandPage
from motor_product.models import Insurer
from models import Vehicle
import os
from django.conf import settings


def vehiclemodel(request, brand, modelName, vertical):
    vehicle = get_object_or_404(Vehicle, brand=brand, modelName=modelName)
    page = vehicle.page
    root = page.category.get_root()
    if root.slug == vertical:
        template = "vehicle-insurance-model.html"
        image_path = "seocms/img/" + \
            vertical.split("-insurance")[0] + "/" + \
            brand + "/" + modelName

        #image_path = os.path.join(settings.MEDIA_ROOT, "seocms/img/car/"+brand + "/" + modelName+".png")
        context = dict()
        if os.path.isfile(os.path.join(settings.MEDIA_ROOT, image_path+".png")):
            context['img_url'] = image_path+".png"
        elif os.path.isfile(os.path.join(settings.MEDIA_ROOT, image_path+".jpg")):
            context['img_url'] = image_path+".jpg"  
        else:
            context['img_url'] = ''
        #context['seo'] = {'title': page.title}
        context['fragment'] = dict(page.fragments.values_list('name', 'content'))
        context['page'] = page
        context['page_id'] = page.url
        context['vertical'] = vertical
        #context['ancestors'] = page.category.get_ancestors(include_self=True)
        breadcrumb = []
        url = "/"
        breadcrumb.append({'url': url, 'title': "Home"})
        for a in page.category.get_ancestors(include_self=True):
            url = "/" + a.url + "/"
            breadcrumb.append(
                {'url': url, 'title': deslugify_and_capitalize(a.name)})
        breadcrumb.append({'url': url + page.url + "/",
                           'title': deslugify_and_capitalize(page.url)})
        context['breadcrumb'] = breadcrumb
        try:
            return render(request, template, context)
        except TemplateDoesNotExist:
            raise Http404

    else:
        return redirect('/%s/?%s' % (vertical + "/" + brand + "/" + modelName, request.GET.urlencode()), permanent=True)


def deslugify_and_capitalize(value):
    """Converts 'first_name' to 'First name'"""
    if not value:
        return ''
    return " ".join([x.capitalize() for x in value.replace("-", " ").split(" ")])


def insurer_garages(request, slug):
    slug = slug.lower()
    try:
        insurer = BrandPage.objects.get(slug=slug, _motor_insurer__isnull=False, type='motor')._motor_insurer
    except BrandPage.DoesNotExist:
        insurer = get_object_or_404(Insurer, slug=slug)
    breadcrumb = []
    path = request.path[1:]
    b_url = "/"
    breadcrumb.append({'url': b_url, 'title': "Home"})
    for p in path[:-1].split("/"):
        b_url += p + "/"
        breadcrumb.append({'url': b_url, 'title': " ".join(
            [x.capitalize() for x in p.replace("-", " ").split(" ")])})
    return render(request,
                  'flatpages:flatpages/car-insurance/cashless-garages.html',
                  {
                      'insurer': insurer,
                      'title': slug.replace('-', ' '),
                      'breadcrumb': breadcrumb,
                  })
