# Django settings for MYPROJECT project.
import sys
import os
from path import path
from celery.schedules import crontab
import json
from datetime import timedelta

from django.utils.translation import ugettext_lazy as _

SETTINGS_FILE_FOLDER = path(__file__).parent.abspath()
sys.path.append(SETTINGS_FILE_FOLDER.joinpath("../libs").abspath())

DEBUG = True
PRODUCTION = False
TEMPLATE_DEBUG = DEBUG
JS_BUILD_DEBUG = True
CSS_BUILD_DEBUG = True
NOTIFICATION_DEBUG = True
CACHE_REQUIREMENT_IN_REDIS = False
VERSION = 1.46
INTERNAL_IPS = [
    '127.0.0.1',
    '182.72.40.134',
    '182.74.245.218',
    '58.68.3.58',
    '124.124.12.137',
]

# CACHE_MIDDLEWARE_SECONDS = 60*60  #60 minutes
# CACHE_MIDDLEWARE_KEY_PREFIX = "rblog"
# CACHES = {
#    'default': {
#        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
#        'LOCATION': '127.0.0.1:11211',
#    }
# }
# CACHE_MIDDLEWARE_ANONYMOUS_ONLY = True
# SESSION_ENGINE = "django.contrib.sessions.backends.cache"

# Static Generator
WEB_ROOT = os.path.join(SETTINGS_FILE_FOLDER, '../static/CACHE/html')
DOWNLOADS_ROOT = os.path.join(SETTINGS_FILE_FOLDER, '../static/downloads')
POLICY_ROOT = os.path.join(SETTINGS_FILE_FOLDER, '../static/policy')
CONF_ROOT = os.path.join(SETTINGS_FILE_FOLDER, '../confs')

SERVER_NAME = "localhost"

# for httpschange user password postgres ubuntu
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')

ADMINS = (
    ("Devendra K Rane", "dev@coverfoxmail.com"),
    ("Vinay", "vinay@coverfoxmail.com"),
    ("Deepak", "deepak@coverfoxmail.com"),
    ("Sanket Rathi", "sanket@coverfoxmail.com"),
    ("Rakesh Verma", "rakesh@coverfoxmail.com"),
    ("Arpit o.O", "arpit+errorreports@coverfoxmail.com"),
    ("Amit Upadhyay", "amitu@coverfoxmail.com"),
    ("Team QA", "qa@coverfoxmail.com"),
    ("Rishabh Tariyal", "rishabh.tariyal@coverfoxmail.com"),
    ("Aash Dhariya", "aash@coverfoxmail.com"),
    ("Parag Tyagi", "parag@coverfoxmail.com"),
    ("Shobhit", "shobhit@coverfoxmail.com"),
    ("Amit Siddhu", "amit.siddhu@coverfoxmail.com"),
    ("Nripesh Agarwal", "nripesh@coverfoxmail.com"),
)

TRAVEL_ADMINS = (
    ("Aash Dhariya", "aash@coverfoxmail.com"),
    ("Arpit o.O", "arpit+travelreports@coverfoxmail.com"),
    ("Jatinderjit", "jatinderjit@coverfoxmail.com"),
    ("Suhas Ghule", "suhas@coverfoxmail.com"),
    ("Ramita", "ramitha@coverfox.com"),
    ("Jay", "jay@coverfox.com"),
    ("QA", "qa@coverfoxmail.com"),
    ("Mahesh", "mahesh@coverfox.com"),
)

MANAGERS = ADMINS
SEND_BROKEN_LINK_EMAILS = True

DATABASES = {
    'default': {
        # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or
        # 'oracle'.
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        # Or path to database file if using sqlite3.
        'NAME': 'coverfox',
        'USER': 'root',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        # Set to empty string for localhost. Not used with sqlite3.
        'HOST': '',
        # Set to empty string for default. Not used with sqlite3.
        'PORT': '',
        'TEST': {
            'NAME': 'test_coverfox',
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            # Or path to database file if using sqlite3.
            'USER': 'root',                      # Not used with sqlite3.
            'PASSWORD': '',                  # Not used with sqlite3.
            # Set to empty string for localhost. Not used with sqlite3.
            'HOST': '',
            # Set to empty string for default. Not used with sqlite3.
            'PORT': '',
        }
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.

# TIME_ZONE = 'America/Chicago'
TIME_ZONE = 'Asia/Calcutta'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'
LOCALE_PATHS = [SETTINGS_FILE_FOLDER.joinpath("../locale").abspath(), ]
LANGUAGES = [
    ('en', _('English')),
    ('hi', _('Hindi')),
    ('mr', _('Marathi')),
]

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(SETTINGS_FILE_FOLDER, '../static')

# ##### COMPRESSOR SETTINGS ########

COMPRESS_ENABLED = True
COMPRESS_OFFLINE = False
COMPRESS_YUGLIFY_BINARY = 'yuglify'  # assumes yuglify is in your path
COMPRESS_YUGLIFY_CSS_ARGUMENTS = '--terminal'
COMPRESS_YUGLIFY_JS_ARGUMENTS = '--terminal'
COMPRESS_CSS_FILTERS = ['compressor.filters.css_default.CssAbsoluteFilter',
                        'tools.compressor.filters.YUglifyCSSFilter']
COMPRESS_JS_FILTERS = ['tools.compressor.filters.YUglifyJSFilter']
COMPRESS_ROOT = MEDIA_ROOT
COMPRESS_CACHE_BACKEND = 'compress_cache'
COMPRESS_CSS_HASHING_METHOD = 'mtime'

# ##################################

STATIC_URL = '/static/'
AUTH_USER_MODEL = 'core.User'
LOGIN_REDIRECT_URL = '/user/policy'
OTP_LOGIN_REDIRECT_URL = '/user/manage-policies'
# LOGIN_URL          = '/new-landing/'
# LOGIN_REDIRECT_URL = '/review/info-add'
# LOGIN_ERROR_URL    = '/new-landing/'
# LOGOUT_URL = '/accounts/logout/'
SOCIAL_AUTH_LOGIN_ERROR_URL = '/'
SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/review/info-add/'

FACEBOOK_SOCIAL_AUTH_RAISE_EXCEPTIONS = False
SOCIAL_AUTH_RAISE_EXCEPTIONS = False
RAISE_EXCEPTIONS = False


GENERAL_TOLL_FREE = '1800 209 9920'
HEALTH_TOLL_FREE = '1800 209 9920'
HEALTH_CLAIMS_TOLL_FREE = '1800 209 9920'
CAR_TOLL_FREE = '1800 209 9930'
BIKE_TOLL_FREE = '18002099940'
TRAVEL_TOLL_FREE = '1800 209 9950'
CAR_LP_TOLL_FREE = '1800 209 9930'
CAR_CALL_ONLY = '1800 209 9930'
CAR_CLAIMS_TOLL_FREE = '1800 209 9930'
BIKE_TOLL_FREE = '1800 209 9940'
SERVICE_REQUEST_TOLL_FREE = '1800 209 9970'


GENERAL_CONTACT_MAIL = 'info@coverfox.com'
HEALTH_CONTACT_MAIL = 'info@coverfox.com'
CAR_CONTACT_MAIL = 'info@coverfox.com'

OFFICE_ADDRESS = (
    "<strong>Coverfox Insurance Broking Pvt. Ltd.</strong><br>"
    "Krislon House,<br>"
    "B-Wing, 3rd Floor,<br>"
    "Saki Vihar Road, Andheri (E)<br>"
    "Mumbai, Maharashtra - 400072<br>"
)


# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/media/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".

# Make this unique, and don't share it with anybody.
SECRET_KEY = '&l0*)nf6+^6r1(v+o5p3q1ax9g^xj9coc9xdjz&f_g*pgwhsdu'
RANDOM_SEED = '0f2bff241f41adc67e6b98d88160a381'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'home.loaders.AppNamespaceLoader',
    'cms.loaders.CMSTemplateLayoutLoader',
    'django.template.loaders.app_directories.Loader',
)

# Additional locations of static files
STATIC_PATH = os.path.join(SETTINGS_FILE_FOLDER, '../static')
STATICFILES_DIRS = (
    STATIC_PATH,
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'compressor.finders.CompressorFinder',
)

MIDDLEWARE_CLASSES = (
    # 'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'campaign.middlewares.LMSMiddleware',
    'putils.TrackMiddleware',
    'otp.middlewares.OTPMiddleware',
    # 'django.middleware.gzip.GZipMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # 'core.pipeline.RedirectOnCancelMiddleware',
    'social.apps.django_app.middleware.SocialAuthExceptionMiddleware',
    'utils.GlobalRequestMiddleware',
    'django_user_agents.middleware.UserAgentMiddleware',
    'core.middlewares.MarketingMiddleware',
    'core.middlewares.WebviewMiddleware',
    'flatpages.middlewares.SEORedirectMiddleware',
    'flatpages.middlewares.FlatpageFallbackMiddleware',
    'cms.middlewares.CMSPageFallbackMiddleware',
    'putils.SetCookieForInternalIPMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'oauth2_provider.middleware.OAuth2TokenMiddleware',
    'cfblog.Middleware',
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    SETTINGS_FILE_FOLDER.joinpath("templates"),
)

# Django messages default storage.
MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.postgres',
    'filebrowser',
    'grappelli',  # must be before django.contrib.admin
    'django.contrib.admin',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.humanize',
    'django.contrib.staticfiles',
    'social.apps.django_app.default',
    # 'raven.contrib.django.raven_compat',
    'django_shell_ipynb',
    'django_user_agents',
    'django_extensions',
    'gunicorn',
    'widget_tweaks',
    'tagging',
    'tools',
    'compressor',
    'mandrill',
    'dateutil',
    'redactor',
    'django_statsd',
    'explorer',
    'home',
    'core',
    'blog',
    'seo',
    'wiki',
    'review',
    'rdesk',
    'activity',
    'health_product',
    'travel_product',
    'life_product',
    'mailer',
    'campaign',
    'motor_product',
    'motor_product.motor_used_car',
    'motor_product.motor_offline_leads',
    'motor_product.motor_discount_code_admin',
    'motor_product.cdaccount',
    'motor_product.dealer',
    'leads',
    'flatpages',
    'brands',
    'cms',
    'cms.common',
    'django_mptt_admin',
    'apis',
    'apis.v1',
    'oauth2_provider',
    'corsheaders',
    'account_manager',
    'seocms',
    'bakar',  # also delete handler404 setting in urls file
    'fixture_magic',
    'coverfox',
    'import_export',
    'opsapp',
    'motor_product.insurer_panel',
    'cfblog',
    'payment',
    'payment_gateway',
    'app_notification',
    'cfadmin',
    'crm',
    'statemachine',
    'test_without_migrations',
    'cfrecordings',
    'motor_product.integration',
    'leaflet_campaign',
    'marketing',
    'rosetta',
    'otp',
)

INSTALLED_APPS += ("kombu.transport.django",)

AUTHENTICATION_BACKENDS = (
    'social.backends.google.GoogleOAuth2',
    'extended.backends.SuperBackend',
    'extended.backends.ReviewBackend',
    'oauth2_provider.backends.OAuth2Backend',
    'extended.backends.TokenBackend'
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    # "django.core.context_processors.tz",
    'social.apps.django_app.context_processors.backends',
    'social.apps.django_app.context_processors.login_redirect',
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request",
    "putils.context_processor",
    "flatpages.context_processors.seo",
)

# FACEBOOK SETTINGS
FACEBOOK_APP_ID = "545910808767230"
FACEBOOK_APP_SECRET = "70d63533c6c43cbf9f1e3977bbd804f2"

SOCIAL_AUTH_URL_NAMESPACE = 'social'

SOUTH_MIGRATION_MODULES = {
    'default': 'social.apps.django_app.default.south_migrations',
}

SOCIAL_AUTH_FACEBOOK_SCOPE = ['email', 'user_birthday', 'user_relationships']

SOCIAL_AUTH_FACEBOOK_KEY = "545910808767230"
SOCIAL_AUTH_FACEBOOK_SECRET = "70d63533c6c43cbf9f1e3977bbd804f2"

SOCIAL_AUTH_LINKEDIN_OAUTH2_KEY = '78e0qwtsey8d59'
SOCIAL_AUTH_LINKEDIN_OAUTH2_SECRET = 'U4va0BbPaTa2ndC8'
SOCIAL_AUTH_LINKEDIN_OAUTH2_SCOPE = ['r_fullprofile', 'r_emailaddress', ]
SOCIAL_AUTH_LINKEDIN_OAUTH2_FIELD_SELECTORS = ['positions', 'email-address', ]

# GOOGLE PLUS SETTINGS
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '526150544311-ebf82glh4td7svbq6r5dff0kp233nl9e.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = '9RpvBW6ixeSdmlH3SrFXStJk'
SOCIAL_AUTH_GOOGLE_PLUS_KEY = SOCIAL_AUTH_GOOGLE_OAUTH2_KEY
SOCIAL_AUTH_GOOGLE_PLUS_SECRET = SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET

SOCIAL_AUTH_USER_MODEL = 'core.User'

DEFAULT_MTA = 'mandrill'

MANDRILL_API_KEY = '3LCsJNj05Izr3lKNidmEtg'
MAILCHIMP_API_KEY = 'dd8c0051d49567ca95fecc098629ff56-us5'
BLOG_SUB_LIST_ID = 'a666be07eb'

# settings for sms sent via enterprise.smsgupshup.com
GUPSHUP_API_ENDPOINT = "http://enterprise.smsgupshup.com/GatewayAPI/rest"
GUPSHUP_API_USER = "2000140660"
GUPSHUP_API_PASSWORD = "trottingfox"
GUPSHUP_SMS_MASK = "CVRFOX"

KISS_API_KEY = '9549f7f08f5679b76a2443e9e9efeba8cd92321b'
KISS_GLOBAL_ID = 'c519f040-17ea-0131-5228-1231381fa34d'


URL_PATH = ''
SOCIAL_AUTH_STRATEGY = 'social.strategies.django_strategy.DjangoStrategy'
SOCIAL_AUTH_STORAGE = 'social.apps.django_app.default.models.DjangoStorage'
SOCIAL_AUTH_GOOGLE_OAUTH_SCOPE = [
    'https://www.googleapis.com/auth/userinfo.profile'
]
SOCIAL_AUTH_FORCE_EMAIL_VALIDATION = False
# SOCIAL_AUTH_EMAIL_FORM_URL = '/signup-email'
# SOCIAL_AUTH_EMAIL_FORM_HTML = 'core/soc/email_signup.html'
# SOCIAL_AUTH_EMAIL_VALIDATION_FUNCTION = 'core.mail.send_validation'
# SOCIAL_AUTH_EMAIL_VALIDATION_URL = '/soc-email-sent/'
SOCIAL_AUTH_USERNAME_FORM_URL = '/signup-username'
SOCIAL_AUTH_USERNAME_FORM_HTML = 'core/soc/username_signup.html'
SOCIAL_AUTH_USER_FIELDS = ['username', 'email']
SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL = True

SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.user.get_username',
    # 'core.pipeline.require_email',
    # 'social.pipeline.mail.mail_validation',
    'core.pipeline.create_user',
    # 'social.pipeline.user.create_user',
    'social.pipeline.social_auth.associate_user',
    # 'social.pipeline.social_auth.load_extra_data',
    # 'core.pipeline.load_extra_data',
    # 'social.pipeline.user.user_details',
)


# FILEBROWSER settings
FILEBROWSER_DIRECTORY = "uploads/articles/"
URL_FILEBROWSER_MEDIA = "/static/filebrowser/"
URL_TINYMCE = "/static/grappelli/tinymce/jscripts/tiny_mce/"

# django-tagging settings
FORCE_LOWERCASE_TAGS = True
MAX_TAG_LENGTH = 100

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.

LOG_PATH = SETTINGS_FILE_FOLDER.joinpath('../logs')
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_production_false': {
            '()': 'utils.RequireProductionFalse'
        },
        'require_production_true': {
            '()': 'utils.RequireProductionTrue'
        },
    },
    'formatters': {
        'verbose': {
            'format': (
                '%(levelname)s %(asctime)s %(module)s %(process)d '
                '%(thread)d %(message)s'
            )
        },
        'syslog': {
            'format': (
                '%(name)s [%(levelname)s]-[%(funcName)s]: %(message)s'
            )
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
            'email_backend': 'django.core.mail.backends.smtp.EmailBackend',
            'include_html': True,
        },
        'sentry': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': (
                'raven.contrib.django.raven_compat.handlers.SentryHandler'
            ),
        },
        'syslog': {
            'level': 'DEBUG',
            'filters': ['require_production_true'],
            'class': 'logging.handlers.SysLogHandler',
            'facility': 'local6',
            'address': '/dev/log',
            'formatter': 'syslog',
        },
        'file_handler': {
            'level': 'DEBUG',
            'filters': ['require_production_false'],
            'class': 'utils.CustomFileHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'celery': {
            'handlers': ['console', 'sentry', 'syslog', 'file_handler'],
            'level': 'ERROR',
            'propogate': False,
        },
        'django.security': {
            'handlers': ['mail_admins', 'console', 'sentry', 'syslog', 'file_handler'],
            'level': 'WARNING',
            'propagate': False,
        },
        'django': {
            'handlers': ['mail_admins', 'console', 'sentry', 'syslog', 'file_handler'],
            'level': 'ERROR',
            'propagate': False,
        },
        '': {
            'handlers': ['mail_admins', 'console', 'sentry', 'syslog', 'file_handler'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

SITE_NAME = "WWW.COVERFOX.COM"
SITE_URL = "https://www.coverfox.com"
SITE_TITLE = "Coverfox : Insurance without confusion"
SITE_KEYWORDS = [
    "life insurance in india",
    "health insurance in india",
    "car insurance in india",
    "mediclaim policy",
    "medical insurance",
    "what is insurance",
    "term plan in india",
    "health insurance india",
    "motor insurance"
]
SITE_DESCRIPTION = (
    "Compare, understand and learn about insurance in India. Know your health "
    "insurance, life insurance, travel insurance, car insurance. We help you "
    "choose the best insurance for you"
)

DND_MSG = _(
    "I hereby authorize Coverfox to communicate with me on the given number "
    "for my Insurance needs. I am aware that this authorization will override "
    "my registry under NDNC."
)

USE_WYSIWYG = "TINYMCE"  # Options TINYMCE, LIVEEDITOR

# SLACK SETTINGS ##
SLACK_BASE_URL = 'https://slack.com/api'
SLACK_API_TOKEN = 'xoxp-2990649255-3041351219-3099879940-cfa5b0'
SLACK_KEY = 'xoxp-2990649255-2992629782-3032510536-9bda12'

# SENTRY SETTINGS ###
RAVEN_CONFIG = {
    'dsn': None
}

# #### CELERY SETTINGS #####
BROKER_URL = 'redis://localhost:6381/1'
CELERY_RESULT_BACKEND = 'redis://localhost:6381/2'
# adding freshdesk tasks directly from libs folder for now
# i know this is a bad idea
# need to change to add it from utils
CELERY_IMPORTS = (
    'email_backend',
    'motor_product.prod_utils',
    'motor_product.tasks.best_premiums',
    'motor_product.tasks.generate_dealer_invoices',
    'motor_product.insurer_panel.utils',
    'freshdesk',
    'utils',
    'account_manager.utils',
    'account_manager.models',
    'activity.autils',
    'motor_product.insurers.landt.integration',
    'motor_product.insurers.liberty_videocon.integration',
    'motor_product.insurers.future_generali.integration',
    'motor_product.insurers.oriental.integration',
    'cf_cns',
    'crm.drip_utils'
)

CELERY_SEND_TASK_ERROR_EMAILS = False
CELERY_QUEUES = {
    'default': {
        'exchange': 'default',
        'binding_key': 'default',
    },
    'lms_events': {
        'exchange': 'lms_events',
        'binding_key': 'lms_events',
    },
    'motor_premium': {
        'exchange': 'motor_premium',
        'binding_key': 'motor_premium',
    },
    'motor_premium_refresh': {
        'exchange': 'motor_premium_refresh',
        'binding_key': 'motor_premium_refresh',
    },
    'mail': {
        'exchange': 'mail',
        'binding_key': 'mail',
    },
    'sns': {
        'exchange': 'sns',
        'binding_key': 'sns',
    },
    'motor_premium_cron': {
        'exchange': 'motor_premium_cron',
        'binding_key': 'motor_premium_cron',
    },
    'motor_bajaj_allianz': {
        'exchange': 'motor_bajaj_allianz',
        'binding_key': 'motor_bajaj_allianz',
    },
    'motor_bharti_axa': {
        'exchange': 'motor_bharti_axa',
        'binding_key': 'motor_bharti_axa',
    },
    'motor_hdfc_ergo': {
        'exchange': 'motor_hdfc_ergo',
        'binding_key': 'motor_hdfc_ergo',
    },
    'motor_iffco_tokio': {
        'exchange': 'motor_iffco_tokio',
        'binding_key': 'motor_iffco_tokio',
    },
    'motor_l_t': {
        'exchange': 'motor_l_t',
        'binding_key': 'motor_l_t',
    },
    'motor_universal_sompo': {
        'exchange': 'motor_universal_sompo',
        'binding_key': 'motor_universal_sompo',
    },
    'motor_new_india': {
        'exchange': 'motor_new_india',
        'binding_key': 'motor_new_india',
    },
    'motor_oriental': {
        'exchange': 'motor_oriental',
        'binding_key': 'motor_oriental',
    },
    'motor_tata_aig': {
        'exchange': 'motor_tata_aig',
        'binding_key': 'motor_tata_aig',
    },
    'motor_future_generali': {
        'exchange': 'motor_future_generali',
        'binding_key': 'motor_future_generali',
    },
    'motor_liberty_videocon': {
        'exchange': 'motor_liberty_videocon',
        'binding_key': 'motor_liberty_videocon',
    },
    'motor_icici_lombard': {
        'exchange': 'motor_icici_lombard',
        'binding_key': 'motor_icici_lombard',
    },
    'motor_reliance': {
        'exchange': 'motor_reliance',
        'binding_key': 'motor_reliance',
    },
    'pdf_generator': {
        'exchange': 'pdf_generator',
        'binding_key': 'pdf_generator',
    },
    'escalations': {
        'exchange': 'escalations',
        'binding_key': 'escalations',
    },
    'notifications_normal': {
        'exchange': 'notifications_normal',
        'binding_key': 'notifications_normal',
    },
    'notifications_high': {
        'exchange': 'notifications_high',
        'binding_key': 'notifications_high',
    },
    'freshdesk': {
        'exchange': 'freshdesk',
        'binding_key': 'freshdesk',
    },
    'freshdesk_requests': {
        'exchange': 'freshdesk_requests',
        'binding_key': 'freshdesk_requests',
    },
    'insurer_live_quote_status': {
        'exchange': 'insurer_live_quote_status',
        'binding_key': 'insurer_live_quote_status',
    },
    'reports': {
        'exchange': 'reports',
        'binding_key': 'reports',
    },
}

CELERY_DEFAULT_QUEUE = 'default'
CELERY_ACKS_LATE = True
CELERY_TIMEZONE = TIME_ZONE
CELERYD_HIJACK_ROOT_LOGGER = False

# ########################  SCHEDULE JOB SETTINGS  ##############################

CELERYBEAT_SCHEDULE = {
    'lntedi-mail-schedule-midnight': {
        'task': 'motor_product.prod_utils.lntedi_mail_scheduletask',
        'schedule': crontab(hour='14, 22', minute=0),
        'args': (),
    },
    'dealer-invoice-generation': {
        'task': 'motor_product.tasks.generate_dealer_invoices.generate_invoice_task',
        'schedule': crontab(hour=0, minute=0),
        'args': (),
    },
    'cdaccount-daily-report-mail': {
        'task': 'motor_product.cdaccount.tasks.daily_report',
        'schedule': crontab(hour=8, minute=0),
        'args': ()
    },
    'lnt-edimail-schedule-midnight': {
        'task': 'motor_product.prod_utils.lnt_consolidated_edimail',
        'schedule': crontab(hour=0, minute=0),
        'args': (),
    },
    'lnt-offline-cases-mis-midnight': {
        'task': 'motor_product.prod_utils.lnt_offline_cases_dayend_mis',
        'schedule': crontab(hour=0, minute=0),
        'args': (),
    },
    'insurer-up-down-report': {
        'task': 'motor_product.integration.tasks.insurer_status',
        'schedule': timedelta(seconds=60),
        'args': ()
    },
}


# CACHES FOR CEELERY ##
CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        "LOCATION": "redis://127.0.0.1:6381/0",
    },
    'health-results': {
        'BACKEND': 'django_redis.cache.RedisCache',
        "LOCATION": "redis://127.0.0.1:6381/1",
    },
    'motor-results': {
        'BACKEND': 'django_redis.cache.RedisCache',
        "LOCATION": "redis://127.0.0.1:6381/2",
    },
    'compress_cache': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://127.0.0.1:6381/10',
    },
}

# ## EMAIL SETTINGS ###
SERVER_EMAIL = 'dev@cfstatic.org'
DEFAULT_FROM_EMAIL = 'dev@cfstatic.org'
DEFAULT_FROM_NAME = "Team Coverfox"
DEFAULT_REPLY_TO = "Team Coverfox <info@coverfox.com>"
EMAIL_HOST = 'localhost'
EMAIL_HOST_PASSWORD = ''
EMAIL_HOST_USER = ''
EMAIL_PORT = '25'
EMAIL_USE_TLS = True
EMAIL_BACKEND = 'email_backend.CeleryEmailBackend'
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# ## FOR MAIL ALERTS ###
HEALTH_TEAM = [
    'jatinderjit@coverfoxmail.com',
    'suhas@coverfoxmail.com',
    'noopur@coverfoxmail.com',
    'aash@coverfoxmail.com',
    'rishabh.tariyal@coverfoxmail.com',
    'qa@coverfoxmail.com',
]

ALERT_SUBSCRIBERS = {
    'car': [],
    'bike': [],
    'health': [],
    'travel': [],
}

# ## ELASTIC SEARCH SETTINGS ###
ELASTIC_SEARCH_URL = 'http://localhost:9200'

# REDACTOR SETTINGS
REDACTOR_OPTIONS = {'lang': 'en', 'plugins': ['table']}

# ## MOTOR BUILD SETTINGS ###
try:
    with open(
        os.path.join(
            SETTINGS_FILE_FOLDER, '../static/js/motor/build/stats.json'
        )
    ) as f:
        j = json.loads(f.read())
        WEBPACK_MOBILE_ASSETS = j['assetsByChunkName']
        WEBPACK_MOBILE_ASSETS_HASH = j['hash']
except:
    pass

# ## MOTOR BUILD SETTINGS ###
try:
    with open(
        os.path.join(
            SETTINGS_FILE_FOLDER, '../static/motor_product/build/stats.json'
        )
    ) as f:
        j = json.loads(f.read())
        WEBPACK_ASSETS = j['assetsByChunkName']
        WEBPACK_ASSETS_HASH = j['hash']
except:
    pass


# ## TRAVEL BUILD SETTINGS ###
try:
    with open(
        os.path.join(
            SETTINGS_FILE_FOLDER,
            '../static/travel_product/v1/build/stats.json'
        )
    ) as f:
        j = json.loads(f.read())
        WEBPACK_TRAVEL_ASSETS = j['assetsByChunkName']
        WEBPACK_TRAVEL_ASSETS_HASH = j['hash']
except:
    pass

# ## WIDGET BUILD SETTINGS ###
try:
    with open(
        os.path.join(
            SETTINGS_FILE_FOLDER, '../static/js/build/stats.json'
        )
    ) as f:
        j = json.loads(f.read())
        WEBPACK_WIDGET_ASSETS = j['assetsByChunkName']
        WEBPACK_WIDGET_ASSETS_HASH = j['hash']
except:
    pass

# ## LMS SETTINGS ###
LMS_BOUGHT_SUCCESS_URL = 'http://localhost:8001/data-push/bought/'
LMS_EVENT_URL = 'http://localhost:8001/data-push/event/'
# REDIS/NODE PARAMETERS
# This redis instance is being used by Jarvis for caching and pushing data
# through node
JARVIS_REDIS_HOST = 'localhost'
JARVIS_REDIS_PORT = 6381

CALL_REQUIREMENT_FIX = False

SEND_NUDGESPOT_EVENTS = False
NUDGESPOT = False
VALIDATE_EMAILS = False

# ## CORS SETTINGS ###
CORS_ALLOW_HEADERS = (
    'x-requested-with',
    'content-type',
    'accept',
    'origin',
    'authorization',
    'x-csrftoken',
    'user-agent',
    'accept-encoding',
    'cfdate',
    'cfauthorization',
    'cfjsapi',
    'date',
)
CORS_ORIGIN_WHITELIST = ()
CORS_URLS_REGEX = r'.*api.*$'

SOCIAL_AUTH_FACEBOOK_EXTRA_DATA = [
    ('birthday', 'birthday'), ('first_name', 'first_name'),
]
SERVICE_TAX_PERCENTAGE = 14.5

LC_LOCALE = 'en_IN'
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'
SESSION_COOKIE_AGE = 60 * 60 * 24 * 365 * 2

# STATSD SETTINGS
STATSD_HOST = 'ops.coverfox.com'
STATSD_PORT = 8125
STATSD_PREFIX = None
STATSD_MAXUDPSIZE = 512
STATSD_CLIENT = 'django_statsd.clients.null'
STATSD_RECORD_KEYS = [
    'window.performance.timing.navigationStart',
    'window.performance.timing.domComplete',
    'window.performance.timing.domInteractive',
    'window.performance.timing.domLoading',
    'window.performance.timing.loadEventEnd',
    'window.performance.timing.responseStart',
]

# REDIS/NODE PARAMETERS
REDIS_HOST = 'localhost'
REDIS_PORT = 6381

""" FASTLANE """
FAST_LANE_URL = "https://web.fastlaneindia.com/vin/api/v1/vehicle"
FAST_LANE_BASIC_USERNAME = "I003PROD1"
FAST_LANE_BASIC_PASSWORD = "i003cf@pd2909"
""" END FASTLANE """

# Set variable when running tests
TESTING = 'test' in sys.argv

# PATHS
MOTOR_RSA_PDF_PATH = "motor_rsa_pdfs/cfox"
LEAFLET_CAMPAIGN_RSA_PDF_PATH = "leaflet_campaign_rsa_pdfs/cfox"
WKHTMLTOPDF_PATH = "/usr/local/bin/wkhtmltopdf"

LEAFLET_CAMPAIGN_REFERRAL_CODE_REGEX = r'^CFR2201[F|U]\d{5}$'

# Configuration required for Boto
BOTO_CONFIG = {
    'aws_access_key_id': 'AKIAIU54QAXYVFBND27A',
    'aws_secret_access_key': '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',
    'sns_region_name': 'ap-southeast-1',
    'sns_region_endpoint': 'sns.ap-southeast-1.amazonaws.com',
}
LMS_SNS_TOPIC = 'arn:aws:sns:ap-southeast-1:542680859958:lms-data'
LMS_DEV_SNS_TOPIC = 'arn:aws:sns:ap-southeast-1:542680859958:lms-sandman'

CFBLOG_BUCKET = 'cmsblogimages'
CFBLOG_ACCESS_KEY = 'AKIAJ5P24RYZ7Y23XV2Q'
CFBLOG_SECRET_ACCESS_KEY = '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'
WKTHMLTOPDF_PATH = '/usr/local/bin/wkhtmltopdf'

CFDEV_BUCKET = 'cfox-dev'
CFDEV_ACCESS_KEY = 'AKIAJ6VMZF3Q7T7WXAEQ'
CFDEV_SECRET_ACCESS_KEY = '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'


def explorer_check(u):
    return u.is_superuser or u.groups.filter(name="SQL_EXPLORER_USERS").exists()

EXPLORER_PERMISSION_VIEW = explorer_check
EXPLORER_PERMISSION_CHANGE = EXPLORER_PERMISSION_VIEW

CD_EMAIL_ACCOUNTS = [
    'nripesh@coverfoxmail.com',
    'girish.purohit@coverfoxmail.com',
    'baljeet@coverfox.com',
    'prajaktta@coverfoxmail.com',
    'jack@coverfox.com',
    'animesh@coverfox.com'
]
CD_LOW_BALANCE_MAIL = CD_EMAIL_ACCOUNTS

# INTEGRATION S3 Settings
INTEGRATION_BUCKET_NAME = 'insurer-data'
INTEGRATION_AWS_ACCESS_KEY_ID = 'AKIAIKHPF2RQ64M3I7QQ'
INTEGRATION_AWS_SECRET_ACCESS_KEY = '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'

MAXMIND_USERID = '104083'
MAXMIND_LICENSE_KEY = 'JkhrJqCKTEIS'
TRACE_IP = False

# PAYMENT GATEWAY SETTINGS
PAYMENT_GATEWAYS = {
    'billdesk': {
        'oriental': {
            'MERCHANT_ID': 'COVFOICL',
            'SECURITY_ID': 'covfoicl',
            'CHECKSUM_KEY': 'PzkFXW5POIk2'
        },
        'new-india': {
            'MERCHANT_ID': 'COVFNEWIND',
            'SECURITY_ID': 'covfnewind',
            'CHECKSUM_KEY': 'JrScRdNxQw5h'
        },
        'l-t': {
            'MERCHANT_ID': 'COVFLNTGEN',
            'SECURITY_ID': 'covflntgen',
            'CHECKSUM_KEY': 'GbVgNBlQc9j2'
        },
        'bharti-axa': {
            'MERCHANT_ID': 'COVFBAXGEN',
            'SECURITY_ID': 'covfbaxgen',
            'CHECKSUM_KEY': 'ZxwepHKwn0vQ'
        },
        'bajaj-allianz': {
            'MERCHANT_ID': 'COVFBAGIC',
            'SECURITY_ID': 'covfbagic',
            'CHECKSUM_KEY': 'lbtViDh33Rpv'
        },
        'religare': {
            'MERCHANT_ID': 'COVFRELIGH',
            'SECURITY_ID': 'covfreligh',
            'CHECKSUM_KEY': 'eeqNNURsrTE8'
        }
    }
}

# CALL CENTER RECORDINGS Amazon S3 settings
USE_AMAZON_S3 = False

CFOXRECORDING_AWS_ACCESS_KEY = "AKIAJYJTVXJU46VA2BRQ"
CFOXRECORDING_AWS_SECRET_KEY = "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"
CFOXRECORDING_BUCKET_NAME = 'cfoxrecording'

# NOTE: This must be last few lines of this file.
try:
    from local_settings import *  # noqa
except ImportError:
    pass

APP = "Desktop"
GCM_APIKEY = "AIzaSyC8yfcivbYt7OYRHQSHyARpAIJtcL9-YFA"
