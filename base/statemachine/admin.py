from django.contrib import admin

from cms.common.admin import BaseAdmin

from statemachine import models as sm_models


class ParentStateAdmin(BaseAdmin):
    models = sm_models.ParentState


class StateAdmin(BaseAdmin):
    models = sm_models.State


class TransitionInline(admin.StackedInline):
    model = sm_models.Transition
    filter_horizontal = ('conditions', 'unless', 'before', 'after')
    can_delete = False
    extra = 0


class NonTransitionInline(admin.StackedInline):
    model = sm_models.NonTransition
    filter_horizontal = ('conditions', 'unless', 'before', 'after')
    can_delete = False
    extra = 0


class TriggerActivityKindMapInline(admin.TabularInline):
    model = sm_models.TriggerActivityKindMap
    extra = 0


class TriggerAdmin(BaseAdmin):
    models = sm_models.Trigger
    inlines = (TransitionInline, NonTransitionInline, TriggerActivityKindMapInline,)


class CallbackAdmin(BaseAdmin):
    models = sm_models.Callback


class TransitionAdmin(BaseAdmin):
    models = sm_models.Transition


class ConditionAdmin(BaseAdmin):
    models = sm_models.Condition


class MachineAdmin(BaseAdmin):
    models = sm_models.Machine
    save_as = True


class MachineRouterAdmin(BaseAdmin):
    models = sm_models.MachineRouter


class NonTransitionAdmin(BaseAdmin):
    models = sm_models.NonTransition


class StateFormRouterAdmin(BaseAdmin):
    models = sm_models.StateFormRouter


admin.site.register(sm_models.ParentState, ParentStateAdmin)
admin.site.register(sm_models.State, StateAdmin)
admin.site.register(sm_models.Trigger, TriggerAdmin)
admin.site.register(sm_models.Callback, CallbackAdmin)
admin.site.register(sm_models.Transition, TransitionAdmin)
admin.site.register(sm_models.Condition, ConditionAdmin)
admin.site.register(sm_models.Machine, MachineAdmin)
admin.site.register(sm_models.MachineRouter, MachineRouterAdmin)
admin.site.register(sm_models.NonTransition, NonTransitionAdmin)
admin.site.register(sm_models.StateFormRouter, StateFormRouterAdmin)
