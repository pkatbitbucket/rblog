from django.conf import settings

import cf_cns
from core import models as core_models
from crm import models as crm_models
from crm import drip_utils


def _get_activity(event_data):
    return event_data.kwargs.get('activity')


def sm_check_for_transaction_drop(self, event_data):
    """
    Name: Check for Transaction Drop
    """
    from crm import tasks as crm_tasks
    act = _get_activity(event_data)
    crm_tasks.check_for_transaction_drop.apply_async(
        kwargs={'requirement_id': self.id, 'activity_id': act.id},
        countdown=600
    )


def sm_alert_managers_on_transaction_drop(self, event_data):
    """
    Name: Alert managers on transaction drop
    """
    cf_cns.notify(
        nid='JARVIS_TRANSACTION_DROP',
        to=settings.ALERT_SUBSCRIBERS[self.kind.product],
        obj={
            'requirement_id': self.id,
            'customer_name': self.mini_json().get('name'),
            'assigned_to': self.owner,
            'phone_number': self.mini_json().get('mobile')
        }
    )


def sm_alert_managers_on_payment_failure(self, event_data):
    """
    Name: Alert managers on payment failure
    """
    cf_cns.notify(
        nid='JARVIS_PAYMENT_FAILURE',
        to=settings.ALERT_SUBSCRIBERS[self.kind.product],
        obj={
            'requirement_id': self.id,
            'customer_name': self.mini_json().get('name'),
            'assigned_to': self.owner,
            'phone_number': self.mini_json().get('mobile')
        }
    )


def sm_change_requirement_kind_to_car_inspection_required(self, event_data):
    """
    Name: Change requirement kind to car inspection required
    """
    rk = core_models.RequirementKind.objects.get(
        product=core_models.RequirementKind.CAR,
        kind=core_models.RequirementKind.NEW_POLICY,
        sub_kind='INSPECTION_REQUIRED'
    )
    self.kind = rk
    self.owner = None
    self.save()


def sm_change_requirement_kind_to_health_medical_required(self, event_data):
    """
    Name: Change requirement kind to health medical required
    """
    rk = core_models.RequirementKind.objects.get(
        product=core_models.RequirementKind.HEALTH,
        kind=core_models.RequirementKind.NEW_POLICY,
        sub_kind='MEDICAL_REQUIRED'
    )
    self.kind = rk
    self.owner = None
    self.save()


def sm_change_requirement_kind_based_on_disposition(self, event_data):
    """
    Name: Change requirement kind based on Disposition
    """
    act = _get_activity(event_data)
    product = act.sub_disposition.value.split('_')[0].lower()
    rk = core_models.RequirementKind.objects.get(
        product=product,
        kind=core_models.RequirementKind.NEW_POLICY,
        sub_kind='',
    )
    self.kind = rk
    self.owner = None
    self.save()


def sm_unassign_requirement(self, event_data):
    """
    Name: Unassign requirement
    """
    self.owner = None
    self.save()


def sm_notify_nudgespot_on_payment_success(self, event_data):
    """
    Name: Notify nudgespot on payment success
    """
    act = _get_activity(event_data)
    user_id = self.user.customer_id if self.user else None
    tracker_id = self.visitor.session_key if self.visitor else None
    customer_id = user_id or tracker_id
    drip_utils.save_bought.delay(
        rdata=None,
        activity_id=act.id,
        user_id=customer_id,
        timestamp=None
    )


def sm_notify_nudgespot_on_event(self, event_data):
    """
    Name: Notify nudgespot on event
    """
    act = _get_activity(event_data)
    user_id = self.user.customer_id if self.user else None
    tracker_id = self.visitor.session_key if self.visitor else None
    customer_id = user_id or tracker_id
    drip_utils.save_event.delay(
        rdata=None,
        customer_id=customer_id,
        activity_id=act.id,
        timestamp=None
    )


def sm_notify_nudgespot_on_no_conversation_lead(self, event_data):
    """
    Name: Notify nudgespot on no conversation lead
    """
    product = self.kind.product
    parent_state = self.current_state.parent_state
    name = ''
    act = _get_activity(event_data)
    if parent_state == 'QUALIFIED':
        if product == 'car':
            name = 'qualified_no_conversation'
        elif product == 'health':
            name = 'qualified_ringing'
    else:
        name = 'no_conversation'
    drip_utils.dispose_call.delay(
        requirement_id=self.id,
        disposition_name=name,
        disposition=act.sub_disposition.value,
        head_disposition=act.disposition.value,
        activity_id=act.id,
    )


def sm_notify_nudgespot_on_closed_resolved_lead(self, event_data):
    """
    Name: Notify nudgespot on closed resolved lead
    """
    name = 'resolved'
    act = _get_activity(event_data)
    drip_utils.dispose_call.delay(
        requirement_id=self.id,
        disposition_name=name,
        disposition=act.sub_disposition.value,
        head_disposition=act.disposition.value,
        activity_id=act.id,
    )


def sm_notify_nudgespot_on_closed_unresolved_lead(self, event_data):
    """
    Name: Notify nudgespot on closed unresolved lead
    """
    act = _get_activity(event_data)
    name = ''
    if isinstance(act, crm_models.Activity):
        if act.disposition.value == 'LOST_CASES':
            name = 'lost'
        elif act.disposition.value == 'DEAD':
            name = 'dead'
    else:
        name = 'dead'
    drip_utils.dispose_call.delay(
        requirement_id=self.id,
        disposition_name=name,
        disposition=act.sub_disposition.value,
        head_disposition=act.disposition.value,
        activity_id=act.id,
    )
