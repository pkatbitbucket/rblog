from crm import models as crm_models


def _get_activity(event_data):
    return event_data.kwargs.get('activity')


def sm_is_requirement_accomplishable(self, event_data):
    return True


def sm_is_fresh_requirement(self, event_data):
    parent_state = self.current_state.parent_state
    return parent_state.name == 'FRESH'


def sm_is_freshnew_requirement(self, event_data):
    return self.current_state.name == 'FRESH_new'


def sm_is_qualifiednew_requirement(self, event_data):
    return self.current_state.name == 'QUALIFIED_new'


def sm_is_policy_issued(self, event_data):
    return False


def sm_is_crossed_no_conversation_limit(self, event_data):
    act = _get_activity(event_data)
    return self.crm_activities.filter(disposition=act.disposition).count() > 7


def sm_is_transferred_to_car_offline_queue(self, event_data):
    act = _get_activity(event_data)
    if isinstance(act, crm_models.Activity):
        return act.data.get('is_car_offline_case')
    return False
