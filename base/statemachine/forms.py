from django import forms
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.http.response import JsonResponse

from cfutils.restless import dj
from cfutils.restless import preparers
from cfutils.knockout import forms as koforms
import models as sm_models


class CallbackForm(forms.ModelForm, koforms.KnockoutForm):
    class Meta:
        model = sm_models.Callback
        fields = ['name', 'module']
        field_jsmodel = {'self': 'Callback'}
        rest_url = '/api/callback/'

    def clean_name(self):
        d = self.cleaned_data
        if self.instance and self.instance.id:
            if sm_models.Callback.objects.filter(Q(name=d['name']) & ~Q(id=self.instance.id)):
                raise forms.ValidationError("Callback with this name already exists")
        else:
            if sm_models.Callback.objects.filter(name=d['name']):
                raise forms.ValidationError("Callback with this name already exists")
        return d['name']


class CallbackResource(dj.DjangoModelResource):
    preparer = preparers.DjangoFieldsDjsonPreparer()
    form_cls = CallbackForm
    model = sm_models.Callback


class ConditionForm(forms.ModelForm, koforms.KnockoutForm):
    class Meta:
        model = sm_models.Condition
        fields = ['name', 'module', 'query']
        field_jsmodel = {'self': 'Condition'}
        rest_url = '/api/condition/'

    def clean_name(self):
        d = self.cleaned_data
        if self.instance and self.instance.id:
            if sm_models.Condition.objects.filter(Q(name=d['name']) & ~Q(id=self.instance.id)):
                raise forms.ValidationError("Condition with this name already exists")
        else:
            if sm_models.Condition.objects.filter(name=d['name']):
                raise forms.ValidationError("Condition with this name already exists")
        return d['name']


class ConditionResource(dj.DjangoModelResource):
    preparer = preparers.DjangoFieldsDjsonPreparer()
    form_cls = ConditionForm
    model = sm_models.Condition


class StateForm(forms.ModelForm, koforms.KnockoutForm):

    class Meta:
        model = sm_models.State
        fields = ['name', 'on_enter', 'on_exit', 'ignore_invalid_triggers']
        field_jsmodel = {
            'self': 'State',
            'on_enter': 'callback',
            'on_exit': 'callback',
        }
        rest_url = '/api/state/'

    def clean_name(self):
        d = self.cleaned_data
        if self.instance and self.instance.id:
            if sm_models.State.objects.filter(Q(name=d['name']) & ~Q(id=self.instance.id)):
                raise forms.ValidationError("State with this name already exists")
        else:
            if sm_models.State.objects.filter(name=d['name']):
                raise forms.ValidationError("State with this name already exists")
        return d['name']


class StateResource(dj.DjangoModelResource):
    preparer = preparers.DjangoFieldsDjsonPreparer()
    form_cls = StateForm
    model = sm_models.State

    # PUT /pk/
    def update(self, pk):
        state = get_object_or_404(sm_models.State, id=pk)
        sform = StateForm(self.data, instance=state)
        if sform.is_valid():
            state.name = sform.cleaned_data['name']
            state.save()

            state.on_enter.clear()
            state.on_enter.add(*sform.cleaned_data['on_enter'])

            state.on_exit.clear()
            state.on_exit.add(*sform.cleaned_data['on_exit'])

            return state
        else:
            return JsonResponse({'success': False, 'errors': sform.errors})


class TriggerForm(forms.ModelForm, koforms.KnockoutForm):
    class Meta:
        model = sm_models.Trigger
        fields = ['name']
        field_jsmodel = {'self': 'Trigger'}
        rest_url = '/api/trigger/'

    def clean_name(self):
        d = self.cleaned_data
        if self.instance and self.instance.id:
            if sm_models.Trigger.objects.filter(Q(name=d['name']) & ~Q(id=self.instance.id)):
                raise forms.ValidationError("Trigger with this name already exists")
        else:
            if sm_models.Trigger.objects.filter(name=d['name']):
                raise forms.ValidationError("Trigger with this name already exists")
        return d['name']


class TriggerResource(dj.DjangoModelResource):
    preparer = preparers.DjangoFieldsDjsonPreparer()
    form_cls = TriggerForm
    model = sm_models.Trigger


class StateDispositionForm(forms.ModelForm, koforms.KnockoutForm):
    class Meta:
        model = sm_models.StateDisposition
        fields = ['machine', 'state', 'disposition', 'trigger', 'is_active']
        field_jsmodel = {
            'self': 'StateDisposition',
            'machine': 'machine',
            'disposition': 'disposition',
            'state': 'state',
            'trigger': 'trigger'
        }
        rest_url = '/api/state-disposition/'


class StateDispositionResource(dj.DjangoModelResource):
    preparer = preparers.DjangoFieldsDjsonPreparer()
    form_cls = StateDispositionForm
    model = sm_models.StateDisposition


class TransitionForm(forms.ModelForm, koforms.KnockoutForm):
    class Meta:
        model = sm_models.Transition
        fields = ['source', 'destination', 'trigger', 'conditions', 'unless', 'before', 'after']
        field_jsmodel = {
            'self': 'Transition',
            'source': 'state',
            'destination': 'state',
            'trigger': 'trigger',
            'conditions': 'condition',
            'unless': 'condition',
            'before': 'callback',
            'after': 'callback'
        }
        rest_url = '/api/transition/'


class TransitionResource(dj.DjangoModelResource):
    preparer = preparers.DjangoFieldsDjsonPreparer()
    form_cls = TransitionForm
    model = sm_models.Transition

    # PUT /pk/
    def update(self, pk):
        transition = get_object_or_404(sm_models.Transition, id=pk)
        tform = TransitionForm(self.data, instance=transition)
        if tform.is_valid():
            transition.source = tform.cleaned_data['source']
            transition.destination = tform.cleaned_data['destination']
            transition.trigger = tform.cleaned_data['trigger']
            transition.save()

            transition.conditions.clear()
            transition.conditions.add(*tform.cleaned_data['conditions'])

            transition.unless.clear()
            transition.unless.add(*tform.cleaned_data['unless'])

            transition.before.clear()
            transition.before.add(*tform.cleaned_data['before'])

            transition.after.clear()
            transition.after.add(*tform.cleaned_data['after'])

            return transition
        else:
            return JsonResponse({'success': False, 'errors': tform.errors})


class NonTransitionForm(forms.ModelForm, koforms.KnockoutForm):
    class Meta:
        model = sm_models.NonTransition
        fields = ['trigger', 'conditions', 'unless', 'before', 'after']
        field_jsmodel = {
            'self': 'NonTransition',
            'trigger': 'trigger',
            'conditions': 'condition',
            'unless': 'condition',
            'before': 'callback',
            'after': 'callback'
        }
        rest_url = '/api/non-transition/'


class NonTransitionResource(dj.DjangoModelResource):
    preparer = preparers.DjangoFieldsPreparer()
    form_cls = NonTransitionForm
    model = sm_models.NonTransition

    # PUT /pk/
    def update(self, pk):
        non_transition = get_object_or_404(sm_models.NonTransition, id=pk)
        ntform = NonTransitionForm(self.data, instance=non_transition)
        if ntform.is_valid():
            non_transition.source = ntform.cleaned_data['source']
            non_transition.destination = ntform.cleaned_data['destination']
            non_transition.trigger = ntform.cleaned_data['trigger']
            non_transition.save()

            non_transition.conditions.clear()
            non_transition.conditions.add(*ntform.cleaned_data['conditions'])

            non_transition.unless.clear()
            non_transition.unless.add(*ntform.cleaned_data['unless'])

            non_transition.before.clear()
            non_transition.before.add(*ntform.cleaned_data['before'])

            non_transition.after.clear()
            non_transition.after.add(*ntform.cleaned_data['after'])

            return non_transition
        else:
            return JsonResponse({'success': False, 'errors': ntform.errors})


class MachineForm(forms.ModelForm, koforms.KnockoutForm):
    class Meta:
        model = sm_models.Machine
        fields = [
            'name', 'states', 'initial_state', 'transitions',
            'non_transitions', 'send_event', 'auto_transitions',
            'ordered_transitions', 'ignore_invalid_triggers'
        ]
        field_jsmodel = {
            'self': 'Machine',
            'states': 'state',
            'initial_state': 'state',
            'transitions': 'transition',
            'non_transitions': 'nontransition',
        }
        rest_url = '/api/machine/'


class MachineResource(dj.DjangoModelResource):
    preparer = preparers.DjangoFieldsPreparer()
    form_cls = MachineForm
    model = sm_models.Machine

    # PUT /pk/
    def update(self, pk):
        machine = get_object_or_404(sm_models.Machine, id=pk)
        mform = MachineForm(self.data, instance=machine)
        if mform.is_valid():
            machine.name = mform.cleaned_data['name']
            machine.initial_state = mform.cleaned_data['initial_state']
            machine.send_event = mform.cleaned_data['send_event']
            machine.auto_transitions = mform.cleaned_data['auto_transitions']
            machine.ordered_transitions = mform.cleaned_data['ordered_transitions']
            machine.ignore_invalid_triggers = mform.cleaned_data['ignore_invalid_triggers']
            machine.save()

            machine.states.clear()
            machine.states.add(*mform.cleaned_data['states'])

            machine.transitions.clear()
            machine.transitions.add(*mform.cleaned_data['transitions'])

            machine.non_transitions.clear()
            machine.non_transitions.add(*mform.cleaned_data['non_transitions'])

            return machine
        else:
            return JsonResponse({'success': False, 'errors': mform.errors})
