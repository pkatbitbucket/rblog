# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import cfutils.knockout.djson
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Callback',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255, db_index=True)),
                ('module', models.CharField(unique=True, max_length=255, db_index=True)),
            ],
            bases=(models.Model, cfutils.knockout.djson.Djson),
        ),
        migrations.CreateModel(
            name='Condition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100, db_index=True)),
                ('module', models.CharField(db_index=True, unique=True, max_length=255, blank=True)),
                ('query', models.CharField(db_index=True, max_length=100, blank=True)),
            ],
            bases=(models.Model, cfutils.knockout.djson.Djson),
        ),
        migrations.CreateModel(
            name='Configuration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', jsonfield.fields.JSONField()),
                ('is_active', models.BooleanField(default=False)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Machine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100, db_index=True)),
                ('send_event', models.BooleanField(default=True)),
                ('auto_transitions', models.BooleanField(default=True)),
                ('ordered_transitions', models.BooleanField(default=False)),
                ('ignore_invalid_triggers', models.BooleanField(default=False)),
            ],
            bases=(models.Model, cfutils.knockout.djson.Djson),
        ),
        migrations.CreateModel(
            name='MachineRouter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('query', jsonfield.fields.JSONField()),
                ('module', models.CharField(db_index=True, max_length=255, null=True, blank=True)),
                ('serial', models.IntegerField()),
                ('machine', models.ForeignKey(to='statemachine.Machine')),
            ],
            bases=(models.Model, cfutils.knockout.djson.Djson),
        ),
        migrations.CreateModel(
            name='NonTransition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('after', models.ManyToManyField(related_name='after_for_non_transitions', to='statemachine.Callback')),
                ('before', models.ManyToManyField(related_name='before_for_non_transitions', to='statemachine.Callback')),
                ('conditions', models.ManyToManyField(related_name='condition_for_non_transitions', to='statemachine.Condition')),
            ],
            bases=(models.Model, cfutils.knockout.djson.Djson),
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100, db_index=True)),
                ('ignore_invalid_triggers', models.BooleanField(default=False)),
            ],
            options={
                'djson_exclude': ['dispositions'],
            },
            bases=(models.Model, cfutils.knockout.djson.Djson),
        ),
        migrations.CreateModel(
            name='StateDisposition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True)),
                ('disposition', models.ForeignKey(to='crm.Disposition')),
                ('machine', models.ForeignKey(to='statemachine.Machine')),
                ('state', models.ForeignKey(to='statemachine.State')),
            ],
            bases=(models.Model, cfutils.knockout.djson.Djson),
        ),
        migrations.CreateModel(
            name='Transition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('after', models.ManyToManyField(related_name='after_for_transitions', to='statemachine.Callback', blank=True)),
                ('before', models.ManyToManyField(related_name='before_for_transitions', to='statemachine.Callback', blank=True)),
                ('conditions', models.ManyToManyField(related_name='condition_for_transitions', to='statemachine.Condition', blank=True)),
                ('destination', models.ForeignKey(related_name='destination_for_transitions', to='statemachine.State')),
                ('source', models.ForeignKey(related_name='source_for_transitions', to='statemachine.State')),
            ],
            bases=(models.Model, cfutils.knockout.djson.Djson),
        ),
        migrations.CreateModel(
            name='Trigger',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=200)),
            ],
            bases=(models.Model, cfutils.knockout.djson.Djson),
        ),
        migrations.AddField(
            model_name='transition',
            name='trigger',
            field=models.ForeignKey(to='statemachine.Trigger'),
        ),
        migrations.AddField(
            model_name='transition',
            name='unless',
            field=models.ManyToManyField(related_name='unless_for_transitions', to='statemachine.Condition', blank=True),
        ),
        migrations.AddField(
            model_name='statedisposition',
            name='trigger',
            field=models.ForeignKey(to='statemachine.Trigger'),
        ),
        migrations.AddField(
            model_name='state',
            name='dispositions',
            field=models.ManyToManyField(related_name='states', through='statemachine.StateDisposition', to='crm.Disposition'),
        ),
        migrations.AddField(
            model_name='state',
            name='on_enter',
            field=models.ManyToManyField(related_name='on_entering_states', to='statemachine.Callback', blank=True),
        ),
        migrations.AddField(
            model_name='state',
            name='on_exit',
            field=models.ManyToManyField(related_name='on_exiting_states', to='statemachine.Callback', blank=True),
        ),
        migrations.AddField(
            model_name='nontransition',
            name='trigger',
            field=models.ForeignKey(to='statemachine.Trigger'),
        ),
        migrations.AddField(
            model_name='nontransition',
            name='unless',
            field=models.ManyToManyField(related_name='unless_for_non_transitions', to='statemachine.Condition'),
        ),
        migrations.AddField(
            model_name='machine',
            name='initial_state',
            field=models.ForeignKey(related_name='initial_state_for_machines', to='statemachine.State'),
        ),
        migrations.AddField(
            model_name='machine',
            name='non_transitions',
            field=models.ManyToManyField(related_name='machines', to='statemachine.NonTransition'),
        ),
        migrations.AddField(
            model_name='machine',
            name='states',
            field=models.ManyToManyField(related_name='machines', to='statemachine.State'),
        ),
        migrations.AddField(
            model_name='machine',
            name='transitions',
            field=models.ManyToManyField(related_name='machines', to='statemachine.Transition'),
        ),
        migrations.AlterUniqueTogether(
            name='statedisposition',
            unique_together=set([('state', 'disposition', 'machine')]),
        ),
    ]
