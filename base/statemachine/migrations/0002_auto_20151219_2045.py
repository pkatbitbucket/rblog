# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('statemachine', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ParentState',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100, db_index=True)),
            ],
        ),
        migrations.CreateModel(
            name='StateFormRouter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
                ('template', models.CharField(unique=True, max_length=100)),
                ('query', jsonfield.fields.JSONField(null=True)),
                ('machine', models.ManyToManyField(to='statemachine.Machine')),
                ('state', models.ManyToManyField(to='statemachine.State')),
            ],
        ),
        migrations.AddField(
            model_name='state',
            name='parent_state',
            field=models.ForeignKey(to='statemachine.ParentState', null=True),
        ),
    ]
