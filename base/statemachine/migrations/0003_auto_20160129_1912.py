# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('statemachine', '0002_auto_20151219_2045'),
    ]

    operations = [
        migrations.AddField(
            model_name='machine',
            name='after_state_change',
            field=models.ManyToManyField(related_name='after_for_machines', to='statemachine.Callback', blank=True),
        ),
        migrations.AddField(
            model_name='machine',
            name='before_state_change',
            field=models.ManyToManyField(related_name='before_for_machines', to='statemachine.Callback', blank=True),
        ),
        migrations.AddField(
            model_name='machine',
            name='failure_states',
            field=models.ManyToManyField(related_name='failure_for_machines', to='statemachine.State', blank=True),
        ),
        migrations.AddField(
            model_name='machine',
            name='success_states',
            field=models.ManyToManyField(related_name='success_for_machines', to='statemachine.State', blank=True),
        ),
        migrations.AlterField(
            model_name='nontransition',
            name='after',
            field=models.ManyToManyField(related_name='after_for_non_transitions', to='statemachine.Callback', blank=True),
        ),
        migrations.AlterField(
            model_name='nontransition',
            name='before',
            field=models.ManyToManyField(related_name='before_for_non_transitions', to='statemachine.Callback', blank=True),
        ),
        migrations.AlterField(
            model_name='nontransition',
            name='conditions',
            field=models.ManyToManyField(related_name='condition_for_non_transitions', to='statemachine.Condition', blank=True),
        ),
        migrations.AlterField(
            model_name='nontransition',
            name='unless',
            field=models.ManyToManyField(related_name='unless_for_non_transitions', to='statemachine.Condition', blank=True),
        ),
        migrations.AlterField(
            model_name='state',
            name='parent_state',
            field=models.ForeignKey(blank=True, to='statemachine.ParentState', null=True),
        ),
        migrations.AlterField(
            model_name='stateformrouter',
            name='name',
            field=models.CharField(unique=True, max_length=100, db_index=True),
        ),
        migrations.AlterField(
            model_name='stateformrouter',
            name='template',
            field=models.CharField(unique=True, max_length=100, db_index=True),
        ),
        migrations.AlterField(
            model_name='trigger',
            name='name',
            field=models.CharField(unique=True, max_length=200, db_index=True),
        ),
    ]
