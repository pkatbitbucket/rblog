# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('statemachine', '0003_auto_20160129_1912'),
    ]

    operations = [
        migrations.AddField(
            model_name='trigger',
            name='activity_types',
            field=django.contrib.postgres.fields.ArrayField(size=None, null=True, base_field=models.CharField(max_length=200), blank=True),
        ),
    ]
