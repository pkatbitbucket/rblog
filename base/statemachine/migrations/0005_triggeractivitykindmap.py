# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def insert_initial_trigger_activity_maps(apps, schema_editor):
    trigger_model = apps.get_model('statemachine', 'Trigger')
    trigger_map_model = apps.get_model('statemachine', 'TriggerActivityKindMap')
    trigger_data = [
        ('quotes', 'viewed', 'quotes_viewed',),
        ('proposal', 'viewed', 'proposal_viewed',),
        ('proposal', 'failed', 'proposal_failed',),
        ('proposal', 'insurer_server_down', 'insurer_server_down',),
        ('payment', 'viewed', 'make_payment',),
        ('payment', 'failed', 'payment_failed',),
        ('payment', 'success', 'payment_success',),
        ('payment', 'insurer_server_down', 'insurer_server_down',),
        ('inspection', 'visited', 'inspection_viewed',),
        ('inspection', 'success', 'inspection_success',),
        ('inspection', 'failed', 'inspection_failed',),
        ('rollover_not_allowed', 'viewed', 'rollover_not_allowed',),
    ]
    for td in trigger_data:
        tr, _ = trigger_model.objects.get_or_create(name=td[2])
        trmap, _ = trigger_map_model.objects.get_or_create(
            page_id=td[0], activity_type=td[1], trigger=tr
        )


class Migration(migrations.Migration):

    dependencies = [
        ('statemachine', '0004_trigger_activity_types'),
    ]

    operations = [
        migrations.CreateModel(
            name='TriggerActivityKindMap',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('page_id', models.CharField(max_length=255, null=True, blank=True)),
                ('form_name', models.CharField(max_length=255, null=True, blank=True)),
                ('form_variant', models.CharField(max_length=255, null=True, blank=True)),
                ('form_position', models.CharField(max_length=255, null=True, blank=True)),
                ('activity_type', models.CharField(max_length=255, null=True, blank=True)),
                ('product', models.CharField(max_length=255, null=True, blank=True)),
                ('trigger', models.ForeignKey(to='statemachine.Trigger')),
            ],
        ),
        migrations.RunPython(
            code=insert_initial_trigger_activity_maps,
            reverse_code=migrations.RunPython.noop,
        )
    ]
