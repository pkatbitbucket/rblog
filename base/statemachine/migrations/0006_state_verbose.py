# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('statemachine', '0005_triggeractivitykindmap'),
    ]

    operations = [
        migrations.AddField(
            model_name='state',
            name='verbose',
            field=models.CharField(max_length=255, blank=True),
        ),
    ]
