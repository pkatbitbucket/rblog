# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('statemachine', '0006_state_verbose'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='callback',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='condition',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='machine',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='trigger',
            options={'ordering': ['name']},
        ),
    ]
