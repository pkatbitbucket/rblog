from jsonfield import JSONField

import transitions

from django.db import models
from django.db.models import Q
from django.core.serializers import serialize, deserialize
from django.contrib.postgres.fields import ArrayField
from django.apps import apps

from cfutils.knockout import djson


class StateManager(models.Manager):

    def create_from_args(self, name, on_enter=[], on_exit=[], ignore_invalid_triggers=False):
        """ Persist State information to the database.
        Args:
            name (string): The name of the state
            on_enter (list): Optional callable(s) to trigger when a
                state is entered. Can be either a string providing the name of
                a callable, or a list of strings.
            on_exit (list): Optional callable(s) to trigger when a
                state is exited. Can be either a string providing the name of a
                callable, or a list of strings.
            ignore_invalid_triggers (Boolean): Optional flag to indicate if
                unhandled/invalid triggers should raise an exception

        """
        state = State(
            name=name, ignore_invalid_triggers=ignore_invalid_triggers)
        state.save()
        on_enter = [Callback.objects.get(module=oe) for oe in on_enter]
        on_exit = [Callback.objects.get(module=oe) for oe in on_exit]
        state.on_enter.add(*on_enter)
        state.on_exit.add(*on_exit)
        return state


class ParentState(models.Model):
    name = models.CharField(max_length=100, db_index=True, unique=True)

    def __unicode__(self):
        return self.name


class State(models.Model):
    """
    Similar as the State class in Transitions library
    Fields:
        name: The name of the state
        on_enter: Optional callable(s) to trigger when a
            state is entered. Can be either a string providing the name of
            a callable, or a list of strings.
        on_exit: Optional callable(s) to trigger when a
            state is exited. Can be either a string providing the name of a
            callable, or a list of strings.
        ignore_invalid_triggers: Optional flag to indicate if
            unhandled/invalid triggers should raise an exception
    """
    name = models.CharField(max_length=100, db_index=True, unique=True)
    verbose = models.CharField(max_length=255, blank=True)
    on_enter = models.ManyToManyField(
        'Callback', related_name='on_entering_states', blank=True)
    on_exit = models.ManyToManyField(
        'Callback', related_name='on_exiting_states', blank=True)
    ignore_invalid_triggers = models.BooleanField(default=False)
    dispositions = models.ManyToManyField(
        'crm.Disposition', through='StateDisposition', related_name='states')
    parent_state = models.ForeignKey('statemachine.ParentState', db_index=True, null=True, blank=True)
    objects = StateManager()

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return "%s - %s" % (self.name, self.verbose)

    class Meta:
        djson_exclude = ['dispositions']

    def get_library_instance(self):
        state = transitions.State(name=self.name)
        state.on_enter = [oe.module for oe in self.on_enter.all()]
        state.on_exit = [oe.module for oe in self.on_exit.all()]
        return state


class Trigger(models.Model, djson.Djson):
    name = models.CharField(max_length=200, unique=True, db_index=True)
    activity_types = ArrayField(models.CharField(max_length=200), blank=True, null=True)

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return self.name


class TriggerActivityKindMapManager(models.Manager):

    def get_matching_tracker(self, data):
        """
        :return None if no match found
        if multiple are found, it'll return the latest match added
        if any of the parameters
        """
        QUERY_PARAMS = ['page_id', 'activity_type']
        q = Q()
        for qparam in QUERY_PARAMS:
            q = q & Q(**{qparam: data.get(qparam, '')})
        return self.filter(q).last()


class TriggerActivityKindMap(models.Model):
    trigger = models.ForeignKey('Trigger')
    page_id = models.CharField(max_length=255, blank=True, null=True)
    form_name = models.CharField(max_length=255, blank=True, null=True)
    form_variant = models.CharField(max_length=255, blank=True, null=True)
    form_position = models.CharField(max_length=255, blank=True, null=True)
    activity_type = models.CharField(max_length=255, blank=True, null=True)
    product = models.CharField(max_length=255, blank=True, null=True)

    objects = TriggerActivityKindMapManager()


class StateDisposition(models.Model, djson.Djson):
    machine = models.ForeignKey('Machine')
    state = models.ForeignKey('State')
    disposition = models.ForeignKey('crm.Disposition')
    is_active = models.BooleanField(default=True)
    trigger = models.ForeignKey('Trigger')

    class Meta:
        unique_together = (('state', 'disposition', 'machine'),)

    def __unicode__(self):
        return "%s - %s " % (self.state.name, self.disposition.verbose)


class Callback(models.Model, djson.Djson):
    name = models.CharField(max_length=255, db_index=True, unique=True)
    module = models.CharField(max_length=255, db_index=True, unique=True)

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return self.name


class TransitionManager(models.Manager):

    def create_from_args(self, trigger, source, destination, conditions=[], unless=[], before=[], after=[]):
        trigger = Trigger.objects.get(name=trigger)
        source = State.objects.get(name=source)
        destination = State.objects.get(name=destination)
        transition = Transition.objects.create(
            trigger=trigger, source=source, destination=destination)
        conditions = [Condition.objects.get(
            module=condition) for condition in conditions]
        unless = [Condition.objects.get(module=un) for un in unless]
        before = [Callback.objects.get(module=be) for be in before]
        after = [Callback.objects.get(module=af) for af in after]
        transition.conditions.add(*conditions)
        transition.unless.add(*unless)
        transition.before.add(*before)
        transition.after.add(*after)
        return transition


class Transition(models.Model, djson.Djson):
    """
    Similar to Transition class in Transitions library
    Fields:
        source: Source State.
        dest: Destination State.
        conditions: Condition(s) that must pass in order for
            the transition to take place. Either a string providing the
            name of a callable, or a list of callables. For the transition
            to occur, ALL callables must return True.
        unless: Condition(s) that must return False in order
            for the transition to occur. Behaves just like conditions arg
            otherwise.
        before: callbacks to trigger before the
            transition.
        after: callbacks to trigger after the transition.
    """
    source = models.ForeignKey('State', related_name='source_for_transitions')
    destination = models.ForeignKey(
        'State', related_name='destination_for_transitions')
    trigger = models.ForeignKey('Trigger')
    conditions = models.ManyToManyField(
        'Condition', related_name='condition_for_transitions', blank=True)
    unless = models.ManyToManyField(
        'Condition', related_name='unless_for_transitions', blank=True)
    before = models.ManyToManyField(
        'Callback', related_name='before_for_transitions', blank=True)
    after = models.ManyToManyField(
        'Callback', related_name='after_for_transitions', blank=True)

    objects = TransitionManager()

    def get_dict(self):
        """Return dictionary version of Transition required for creating Machine instance."""
        return {
            'trigger': self.trigger.name,
            'source': self.source.name,
            'dest': self.destination.name,
            'conditions': [co.module for co in self.conditions.all()],
            'unless': [un.module for un in self.unless.all()],
            'before': [be.module for be in self.before.all()],
            'after': [af.module for af in self.after.all()],
        }

    def __unicode__(self):
        return "%s-%s : %s -> %s" % (self.id, self.trigger, self.source.name, self.destination.name)


class Condition(models.Model, djson.Djson):
    name = models.CharField(max_length=100, db_index=True, unique=True)
    module = models.CharField(
        max_length=255, blank=True, db_index=True, unique=True)
    query = models.CharField(max_length=100, blank=True, db_index=True)

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return self.name


class Machine(models.Model, djson.Djson):
    """
    Similar to Machine class in transitions library
    Fields:
        states: A list of valid states. Each element can be either a
            string or a State instance. If string, a new generic State
            instance will be created that has the same name as the string.
        initial: The initial state of the Machine.
        transitions: An optional list of transitions. Each element
            is a dictionary of named arguments to be passed onto the
            Transition initializer.
        send_event (boolean): When True, any arguments passed to trigger
            methods will be wrapped in an EventData object, allowing
            indirect and encapsulated access to data. When False, all
            positional and keyword arguments will be passed directly to all
            callback methods.
        auto_transitions (boolean): When True (default), every state will
            automatically have an associated to_{state}() convenience
            trigger in the base model.
        ordered_transitions (boolean): Convenience argument that calls
            add_ordered_transitions() at the end of initialization if set
            to True.
        ignore_invalid_triggers: when True, any calls to trigger methods
            that are not valid for the present state (e.g., calling an
            a_to_b() trigger when the current state is c) will be silently
            ignored rather than raising an invalid transition exception.
        before_state_change: A callable called on every change state before
            the transition happened. It receives the very same args as normal
            callbacks
        after_state_change: A callable called on every change state after
            the transition happened. It receives the very same args as normal
            callbacks
    """
    name = models.CharField(max_length=100, db_index=True, unique=True)
    states = models.ManyToManyField('State', related_name='machines')
    initial_state = models.ForeignKey(
        'State', related_name='initial_state_for_machines')
    transitions = models.ManyToManyField('Transition', related_name='machines')
    non_transitions = models.ManyToManyField(
        'NonTransition', related_name='machines')
    send_event = models.BooleanField(default=True)
    auto_transitions = models.BooleanField(default=True)
    ordered_transitions = models.BooleanField(default=False)
    ignore_invalid_triggers = models.BooleanField(default=False)
    before_state_change = models.ManyToManyField(
        'Callback', related_name='before_for_machines', blank=True)
    after_state_change = models.ManyToManyField(
        'Callback', related_name='after_for_machines', blank=True)
    success_states = models.ManyToManyField('State', blank=True, related_name='success_for_machines')
    failure_states = models.ManyToManyField('State', blank=True, related_name='failure_for_machines')

    class Meta:
        ordering = ['name']

    def get_state_machine(self, model):
        """Return Machine instance as in the Transitions library."""
        machine = transitions.Machine(
            model=model,
            initial=model.current_state.name,
            states=[state.get_library_instance()
                    for state in self.states.all()],
            transitions=[tr.get_dict() for tr in self.transitions.all()],
            non_transitions=[tr.get_dict()
                             for tr in self.non_transitions.all()],
            send_event=self.send_event,
            auto_transitions=self.auto_transitions,
            ordered_transitions=self.ordered_transitions,
            ignore_invalid_triggers=self.ignore_invalid_triggers,
        )
        return machine, model

    def get_machine_instance(self):
        machine = transitions.Machine(
            states=[state.get_library_instance()
                    for state in self.states.all()],
            transitions=[tr.get_dict() for tr in self.transitions.all()],
            non_transitions=[tr.get_dict()
                             for tr in self.non_transitions.all()],
            send_event=self.send_event,
            auto_transitions=self.auto_transitions,
            ordered_transitions=self.ordered_transitions,
            ignore_invalid_triggers=self.ignore_invalid_triggers,
        )
        return machine

    def generate_graph(self, location=None):
        location = location or '/tmp/'
        fname = '%s%s.png' % (location, self.name)
        m = self.get_machine_instance()
        graph = m.get_graph()
        graph.draw(fname, prog='dot')

    def __unicode__(self):
        return self.name


class MachineRouter(models.Model, djson.Djson):
    """
    On the basis of different combination of tags, the machine will be chosen
        machine: router mapped to which of the machines
        query: query for the router
        module: can be used in place of query for complex business logic
        serial: preference of the machine router

    """
    machine = models.ForeignKey('Machine')
    query = JSONField()
    module = models.CharField(
        max_length=255, db_index=True, null=True, blank=True)
    serial = models.IntegerField()

    def __unicode__(self):
        return "%s - %s" % (self.serial, self.machine.name)


class NonTransitionManager(models.Manager):

    def create_from_args(self, trigger, conditions=[], unless=[], before=[], after=[]):
        trigger = Trigger.objects.get(name=trigger)
        non_transition = NonTransition.objects.create(trigger=trigger)
        conditions = [Condition.objects.get(
            module=condition) for condition in conditions]
        unless = [Condition.objects.get(module=un) for un in unless]
        before = [Callback.objects.get(module=be) for be in before]
        after = [Callback.objects.get(module=af) for af in after]
        non_transition.conditions.add(*conditions)
        non_transition.unless.add(*unless)
        non_transition.before.add(*before)
        non_transition.after.add(*after)
        return non_transition


class NonTransition(models.Model, djson.Djson):
    """
    Similar to NonTransition class in Transitions library
    Fields:
        conditions: Condition(s) that must pass in order for
            the transition to take place. Either a string providing the
            name of a callable, or a list of callables. For the transition
            to occur, ALL callables must return True.
        unless: Condition(s) that must return False in order
            for the transition to occur. Behaves just like conditions arg
            otherwise.
        before: callbacks to trigger before the
            transition.
        after: callbacks to trigger after the transition.
    """
    trigger = models.ForeignKey('Trigger')
    conditions = models.ManyToManyField(
        'Condition', related_name='condition_for_non_transitions', blank=True)
    unless = models.ManyToManyField(
        'Condition', related_name='unless_for_non_transitions', blank=True)
    before = models.ManyToManyField(
        'Callback', related_name='before_for_non_transitions', blank=True)
    after = models.ManyToManyField(
        'Callback', related_name='after_for_non_transitions', blank=True)

    objects = NonTransitionManager()

    def get_dict(self):
        """Return dictionary version of Transition required for creating Machine instance."""
        return {
            'trigger': self.trigger.name,
            'conditions': [co.module for co in self.conditions.all()],
            'unless': [un.module for un in self.unless.all()],
            'before': [be.module for be in self.before.all()],
            'after': [af.module for af in self.after.all()],
        }

    def __unicode__(self):
        d = self.get_dict()
        return "%s-%s - %s-%s" % (self.id, d['trigger'], d['conditions'], d['unless'])


class Configuration(models.Model):
    """
    Backup of all the state/transition/machine data can be saved here and the same backup can be
    re-enabled as and when required.
    Usage:
        Configuration.backup(app_label, models)
            This will create a serialized copy of the models (models is a list of strings) in app=app_label.
            Default values :- app_label='statemachine', models=<list of models in statemachine app'
        config_obj.restore(app_label, models)
            Restore the state as in the object config_obj. Defaults are same as 'backup' method.
    """
    data = JSONField()
    is_active = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)

    @classmethod
    def backup(cls, app_label=None, models=None):
        app_label = app_label or 'statemachine'
        if not models:
            models = ['Callback', 'State', 'Trigger', 'Condition',
                      'Transition', 'NonTransition', 'Machine',
                      'MachineRouter', 'StateDisposition']
        d = {}
        for m in models:
            objs = apps.get_model(app_label=app_label,
                                  model_name=m).objects.all()
            d[m] = serialize('json', objs)
        return Configuration.objects.create(data=d, is_active=False)

    def restore(self, app_label=None, models=None):
        app_label = app_label or 'statemachine'
        if not models:
            models = ['Callback', 'State', 'Trigger', 'Condition',
                      'Transition', 'NonTransition', 'Machine',
                      'MachineRouter', 'StateDisposition']
        for m in models:
            for d in deserialize('json', self.data.get(m)):
                d.save()


class StateFormRouter(models.Model):
    name = models.CharField(max_length=100, unique=True, db_index=True)
    template = models.CharField(max_length=100, unique=True, db_index=True)
    state = models.ManyToManyField('statemachine.State')
    machine = models.ManyToManyField('statemachine.Machine')
    query = JSONField(null=True)

    def __unicode__(self):
        return self.name
