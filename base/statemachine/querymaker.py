from models import MachineRouter


def match(self, rdata):
    param_dict = {}
    for k, v in self.query.items():
        param_dict.update({k: rdata.get(k)})
    print 'param_dict', param_dict
    print 'machinerouter query', self.query
    return self.query == param_dict


def get_matching_machine(rdata):
    machine = None
    for mr in MachineRouter.objects.order_by('serial'):
        if match(mr, rdata):
            machine = mr.machine
            break
    if not machine:
        raise ValueError("Matching machine does not exist")

    return machine
