import types
import importlib
import logging

import transitions

from django.utils import timezone

from statemachine import models as sm_models
from statemachine.exceptions import StateMachineException

logger = logging.getLogger(__name__)


class MachineUtils(object):

    def __init__(self, model_obj, machine_obj):
        self.model_obj = model_obj
        self.machine_obj = machine_obj
        self.transitions = [tr.get_dict() for tr in self.machine_obj.transitions.all()]
        self.non_transitions = [tr.get_dict() for tr in self.machine_obj.non_transitions.all()]
        self.states = [s.get_library_instance() for s in self.machine_obj.states.all()]

    def get_machine(self):
        self._add_conditions()
        self._add_callbacks()
        machine, ticket = self.machine_obj.get_state_machine(self.model_obj)
        return machine, ticket

    def _add_conditions(self):
        conditions = self._get_conditions()
        for c in conditions:
            self._add_method('Condition', c)

    def _add_callbacks(self):
        callbacks = self._get_callbacks()
        for c in callbacks:
            self._add_method('Callback', c)

    def _get_conditions(self):
        conditions = []
        for t in self.transitions:
            conditions.extend(t['conditions'])
            conditions.extend(t['unless'])
        for t in self.non_transitions:
            conditions.extend(t['conditions'])
            conditions.extend(t['unless'])
        return list(set(conditions))

    def _get_callbacks(self):
        callbacks = []
        for t in self.transitions:
            callbacks.extend(t['before'])
            callbacks.extend(t['after'])
        for t in self.non_transitions:
            callbacks.extend(t['before'])
            callbacks.extend(t['after'])
        for s in self.states:
            callbacks.extend(s.on_enter)
            callbacks.extend(s.on_exit)
        return list(set(callbacks))

    def _add_method(self, model_name, method_name):
        method = self._get_method(model_name, method_name)
        setattr(self.model_obj, method_name, types.MethodType(method, self.model_obj))

    def _get_method(self, model_name, method_name):
        i = importlib.import_module('statemachine.dynamic_imports.%s_modules' % model_name.lower())
        return getattr(i, method_name)


def state_machine(requirement, web_activity=None, crm_activity=None):
    """
    Apply appropriate statemachine to requirement. Will also update the RequirementStateHistory
    table if state changes.
    Args:
        requirement (core.Requirement): Requirement instance on which state machine
            has to be applied.
            Note: requirement.kind.machine should not be null
        Out of the below two, one has to be supplied.
        web_activity (core.Activity): Activity that just occurred for which state machine
            is being applied.
        crm_activity (crm.Activity): Activity that just occurred for which state machine
            is being applied.
    Returns:
        requirement: modified Requirement instance
        is_transition: boolean indicating whether or not the transition was
            successfully executed (True if successful, False if not).
    """
    try:
        requirement, is_transition = _state_machine(
            requirement=requirement,
            web_activity=web_activity,
            crm_activity=crm_activity
        )
        return requirement, is_transition
    except Exception:
        return requirement, False


def _state_machine(requirement, web_activity, crm_activity):
    from core import models as core_models
    from crm import models as crm_models
    machine = requirement.kind.machine

    if not machine:
        raise StateMachineException("Requirement {rid} does not have a machine configured.\n".format(
            rid=requirement.id
        ))

    if not requirement.current_state:
        raise StateMachineException("Requirement {rid} does not have a current_state.\n".format(
            rid=requirement.id
        ))

    if (crm_activity and web_activity) or not (crm_activity or web_activity):
        raise StateMachineException("Please provide one of web_activity or crm_activity for requirement {rid}".format(
            rid=requirement.id
        ))

    trigger_name = ''
    is_transition = False
    if crm_activity:
        activity = crm_activity
        trigger_name = crm_models.MachineStateSubdisposition.objects.get(
            state=requirement.current_state,
            sub_disposition=activity.sub_disposition,
            machine=machine
        ).trigger.name
        requirement.last_crm_activity = crm_activity
    elif web_activity:
        activity = web_activity
        adata = {
            'page_id': activity.page_id,
            'activity_type': activity.activity_type
        }
        trigger_map = sm_models.TriggerActivityKindMap.objects.get_matching_tracker(adata)
        trigger_name = trigger_map.trigger.name if trigger_map else ''
        requirement.last_web_activity = web_activity
    requirement.save()

    if not trigger_name:
        return requirement, is_transition

    try:
        _, requirement = MachineUtils(requirement, machine).get_machine()
        is_transition = getattr(requirement, trigger_name)(activity=activity)
    except transitions.MachineError as e:
        raise StateMachineException("Machine Error occurred for requirement {rid} - {merror}".format(
            rid=requirement.id,
            merror=str(e)
        ))

    new_state = sm_models.State.objects.get(name=requirement.state)
    if is_transition and new_state != requirement.current_state:
        old_rs = core_models.RequirementStateHistory.objects.filter(
            state=requirement.current_state, requirement=requirement
        ).last()
        if old_rs:
            old_rs.exit_time = timezone.now()
            old_rs.save()
        core_models.RequirementStateHistory.objects.create(
            previous_state=requirement.current_state,
            state=new_state,
            requirement=requirement,
            enter_time=timezone.now()
        )
        requirement.current_state = new_state
        requirement.save()
    return requirement, is_transition
