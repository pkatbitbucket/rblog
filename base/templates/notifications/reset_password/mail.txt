Hi {{obj.name}},

You are receiving this mail because you initiated a request to set up a new password on Coverfox.

You can set up a new password for your Coverfox Account, by opening the following link on your browser.

{{obj.reset_url}}

Cheers!
Team Coverfox.
