import pytest

from leaflet_campaign import factories


@pytest.fixture(scope='function')
def leaflet_agent():
    return factories.LeafletAgentFactory(
        agent_name='super agent'
    )


@pytest.fixture(scope='function')
def agent_referral(leaflet_agent):
    return factories.AgentReferalsFactory(agent=leaflet_agent)
