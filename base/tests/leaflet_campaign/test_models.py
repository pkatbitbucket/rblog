import pytest

pytestmark = pytest.mark.django_db


def test_create_agent(leaflet_agent):
    assert 'super agent' == leaflet_agent.agent_name
    assert 'super-agent' == leaflet_agent.agent_slug
