import pytest

from django.core.urlresolvers import reverse, NoReverseMatch

pytestmark = pytest.mark.django_db


# test reverse url endpoints
def test_url_create_agent_referral_with_invalid_parameters():
    """
    Test if 'create_agent_referral' url is accessible without agent_slug
    """

    # no agent_slug
    with pytest.raises(NoReverseMatch):
        reverse(
            'leaflet_campaign:create_agent_referral'
        )

    # with invalid slug
    with pytest.raises(NoReverseMatch):
        reverse(
            'leaflet_campaign:create_agent_referral',
            kwargs={'agent_slug': "abc!@#$%132"}
        )


def test_url_create_agent_referral_with_valid_agent_slug(
    leaflet_agent
):
    """
    Test if 'create_agent_referral' url is accessible with valid agent_slug
    """
    url = reverse(
        'leaflet_campaign:create_agent_referral',
        kwargs={'agent_slug': leaflet_agent.agent_slug}
    )
    assert url == '/leaflet-campaign/{0}/'.format(leaflet_agent.agent_slug)


def test_url_leaflet_campaign_home_with_invalid_parameters():
    """
    Test if 'leaflet_campaign_home' url is accessible for a non referred
    registration_number
    """
    with pytest.raises(NoReverseMatch):
        reverse(
            'leaflet_campaign:leaflet_campaign_home',
            kwargs={'registration_number': ""}
        )

    with pytest.raises(NoReverseMatch):
        reverse(
            'leaflet_campaign:leaflet_campaign_home',
            kwargs={'registration_number': "abc!@#$%132"}
        )


def test_url_leaflet_campaign_home_with_valid_parameters(agent_referral):
    """
    Test if 'leaflet_campaign_home' url is accessible for a referred
    registration_number by our agent.
    """
    reverse(
        'leaflet_campaign:leaflet_campaign_home',
        kwargs={'registration_number': agent_referral.registration_no}
    )
