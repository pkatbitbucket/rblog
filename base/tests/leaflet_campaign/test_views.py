import pytest

from django.core.urlresolvers import reverse, NoReverseMatch

pytestmark = pytest.mark.django_db


def test_get_create_agent_referral_with_invalid_parameters(client):
    """
    'create_agent_referral' url should raise NoReverseMatch without agent_slug
    """
    # without 'agent_slug'
    with pytest.raises(NoReverseMatch):
        client.get(reverse(
            'leaflet_campaign:create_agent_referral'
        ))

    # with invalid 'agent_slug'
    with pytest.raises(NoReverseMatch):
        reverse(
            'leaflet_campaign:create_agent_referral',
            kwargs={'agent_slug': "test!@#$%!slug"}
        )


def test_get_create_agent_referral_with_valid_agent_slug(
    client,
    leaflet_agent
):
    """
    'create_agent_referral' url should return response having status_code - 200
    with valid agent_slug paramter
    """
    url = reverse(
        'leaflet_campaign:create_agent_referral',
        kwargs={'agent_slug': leaflet_agent.agent_slug}
    )
    response = client.get(url)
    assert response.status_code == 200
