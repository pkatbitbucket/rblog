import pytest
import copy
import datetime
from dateutil import relativedelta
from motor_product.models import VehicleMaster, Insurer, MotorPremium, RTOMaster
from motor_product.cache_utils import PremiumTableCache
from django.conf import settings
from django.core.cache import caches

cache = caches['motor-results']

NEW_POLICY_START_DATE = datetime.datetime.today().strftime('%d-%m-%Y')
MANUFACTURING_DATE = (datetime.datetime.today() - relativedelta.relativedelta(years=1)).strftime('%d-%m-%Y')
PAST_POLICY_START_DATE = (datetime.datetime.today() - relativedelta.relativedelta(days=1)).strftime('%d-%m-%Y')
Vehicle_ID = VehicleMaster.objects.get(variant='Titanium', make__name='Ford', model__name='Figo', cc=1196).id
INSURER = Insurer.objects.get(slug='hdfc-ergo')
RTO_CODE = RTOMaster.objects.get(rto_code='MH-01')
REQUEST_DATA = {u'addon_isDepreciationWaiver': u'0', u'isNCBCertificate': u'0', u'extra_paPassenger': u'0',
                u'cngKitValue': u'0', u'extra_user_dob': u'', u'isNewVehicle': u'0', u'extra_isAntiTheftFitted': u'0',
                u'quoteId': u'11ff3781-85f7-465b-9e1d-d9cd294b7d12', u'idvNonElectrical': u'0',
                u'expiry_errorMessage': u'', u'isClaimedLastYear': u'0',
                u'registrationNumber[]': [u'MH', u'01', u'0', u'0'], u'newNCB': u'0',
                u'newPolicyStartDate': NEW_POLICY_START_DATE, u'manufacturingDate': MANUFACTURING_DATE,
                u'fastlane_model_changed': u'false', u'formatted_reg_number': u'', u'addon_isNcbProtection': u'0',
                u'registrationDate': MANUFACTURING_DATE, u'idvElectrical': u'0', 'third_party': None,
                u'expiry_error': u'false', u'addon_is247RoadsideAssistance': u'0', u'voluntaryDeductible': u'0',
                u'fastlane_variant_changed': u'false', u'rds_id': u'false', u'fastlane_fueltype_changed': u'false',
                u'addon_isInvoiceCover': u'0', u'addon_isDriveThroughProtected': u'0',
                u'previous_policy_insurer_name': u'false', 'payment_mode': 'PAYMENT_GATEWAY', u'fastlane_success': u'0',
                u'extra_user_occupation': u'', u'isCNGFitted': u'0', u'isUsedVehicle': u'false',
                u'extra_isTPPDDiscount': u'0', u'vehicleId': Vehicle_ID, u'expirySlot': u'7', u'idv': u'0',
                u'discountCode': u'', u'extra_isLegalLiability': u'0', u'previousNCB': u'0',
                u'addon_isEngineProtector': u'0', u'ncb_unknown': u'true', u'addon_isKeyReplacement': u'0',
                u'extra_isMemberOfAutoAssociation': u'0', u'isExpired': u'false',
                u'pastPolicyExpiryDate': PAST_POLICY_START_DATE}


@pytest.mark.django_db
class TestMotorCaching(object):

    def setup_cahing(self, insurer=INSURER):
        policy_type = MotorPremium.RENEWAL
        new_ncb = '20'
        response = {"data": 'Test data'}
        vehicle_age = (datetime.datetime.strptime(NEW_POLICY_START_DATE, '%d-%m-%Y') -
                       datetime.datetime.strptime(MANUFACTURING_DATE, '%d-%m-%Y')).days
        MotorPremium.objects.get_or_create(vehicle_id=Vehicle_ID,
                                           insurer=insurer,
                                           zone=RTO_CODE,
                                           policy_type=policy_type,
                                           response_data=response,
                                           max_age=vehicle_age+10,
                                           min_age=vehicle_age-10,
                                           new_ncb=new_ncb)

    def test_cache_disable_settings(self):
        self.setup_cahing()
        if not getattr(settings, 'ENABLE_MOTOR_CACHING', False):
            request_data = copy.deepcopy(REQUEST_DATA)
            request_data.update({'insurer_slug': INSURER.slug})
            fetch_premium = PremiumTableCache(request_data)
            response = fetch_premium.get_premium()
            if response:
                assert False

    def test_cache_bucket(self):
        self.setup_cahing()
        new_policy_start_date = (datetime.datetime.today() + relativedelta.relativedelta(days=5)).strftime('%d-%m-%Y')
        request_data = copy.deepcopy(REQUEST_DATA)
        request_data.update({'newPolicyStartDate': new_policy_start_date})
        if getattr(settings, 'ENABLE_MOTOR_CACHING', False):
            request_data = copy.deepcopy(REQUEST_DATA)
            request_data.update({'insurer_slug': INSURER.slug})
            fetch_premium = PremiumTableCache(request_data)
            response = fetch_premium.get_premium()
            if not response:
                assert False

    def test_cache_disable_insurer(self):
        insurer = Insurer.objects.get(slug='oriental')
        self.setup_cahing(insurer=insurer)
        if getattr(settings, 'ENABLE_MOTOR_CACHING', False):
            request_data = copy.deepcopy(REQUEST_DATA)
            request_data.update({'insurer_slug': insurer.slug})
            fetch_premium = PremiumTableCache(request_data)
            response = fetch_premium.get_premium()
            if response:
                assert False
