from motor_product.prod_utils import bifurcate_address

ADDRESS = 'House No, building, street block city state country'


class TestAddressBifurcation(object):

    def test_without_district(self):
        house, building, street, district = bifurcate_address(ADDRESS, split=3)
        if district:
            assert False

    def test_with_district(self):
        house, building, street, district = bifurcate_address(ADDRESS, split=4)
        if not district:
            assert False

    def test_address_concatenation(self):
        house, building, street, district = bifurcate_address(ADDRESS, split=3)
        if house+' '+building+' '+street != ADDRESS:
            assert False

    def test_address_component(self):
        house, building, street, district = bifurcate_address(ADDRESS, split=4)
        if not house or not building or not street or not district:
            assert False

    def test_one_word_address(self):
        address = 'House'
        house, building, street, district = bifurcate_address(address, split=4)
        if house is None or building is None or street is None or district is None:
            assert False
