from django.conf import settings
from django.contrib import admin
from django.contrib.humanize.templatetags.humanize import intcomma

from django.core import urlresolvers
from django_mptt_admin.admin import DjangoMpttAdmin

from cms.common.admin import BaseAdmin
from import_export import resources, fields

from .forms import InsurerAdminForm
from .models import Plan, Insurer, Grade, IndividualSlab, FamilySlab, StudentSlab, Place, FeatureName, PlanFeature, SlabFeature, DeclinedRisk, Transaction, Activity, Document, PolicyWordingDocument

import datetime
import uuid


class TransactionResource(resources.ModelResource):
    sum_assured = fields.Field(column_name='sum assured')
    plan = fields.Field(attribute='slab__plan__name')
    premium = fields.Field()
    transaction_id = fields.Field(column_name='transaction id')
    detail_url = fields.Field(column_name='detail url')
    insurer = fields.Field(attribute='slab__plan__insurer__name')
    payment_tried_on = fields.Field(column_name='payment tried on')
    created_on = fields.Field(column_name='created on')
    individual_or_family = fields.Field(column_name='individual/family')
    type = fields.Field(attribute='slab__plan__type', column_name='single trip/multi trip')
    first_name = fields.Field(attribute='first_name', column_name='first name')
    last_name = fields.Field(attribute='last_name', column_name='last name')

    class Meta:
        model = Transaction
        fields = ('first_name', 'last_name', 'mobile', 'email', 'status', 'type', )

        # maintain export order
        export_order = ('first_name', 'last_name', 'mobile', 'email',
                        'insurer', 'sum_assured', 'premium', 'plan', 'type', 'individual_or_family', 'status',
                        'created_on', 'payment_tried_on', 'transaction_id', 'detail_url', )

    def export(self, queryset=None):
        if queryset:
            queryset = queryset.filter(created_date__gte=datetime.datetime(year=2015, month=9, day=8))
        return super(self.__class__, self).export(queryset)

    def dehydrate_individual_or_family(self, transaction):
        try:
            assert transaction.slab.individualslab
            return 'individual'
        except:
            assert transaction.slab.familyslab
            return 'family'

    def dehydrate_payment_tried_on(self, transaction):
        return transaction.payment_on.strftime("%d-%b-%Y, %H:%M") if transaction.payment_on else ''

    def dehydrate_created_on(self, transaction):
        return transaction.created_date.strftime("%d-%b-%Y, %H:%M") if transaction.created_date else ''

    def dehydrate_sum_assured(self, transaction):
        return transaction.slab.plan.get_currency_display() + intcomma(transaction.slab.plan.sum_assured)

    def dehydrate_premium(self, transaction):
        return 'Rs ' + intcomma(int(round((transaction.slab.premium * settings.SERVICE_TAX_PERCENTAGE/100) + transaction.slab.premium)))

    def dehydrate_transaction_id(self, transaction):
        return str(uuid.UUID(str(transaction.id)))

    def dehydrate_detail_url(self, transaction):
        return settings.SITE_URL + urlresolvers.reverse(
            'admin:{0}_{1}_change'.format(transaction._meta.app_label, transaction._meta.model_name), args=(transaction.pk,)
        )


class InsurerAdmin(BaseAdmin):
    model = Insurer
    form = InsurerAdminForm
    list_display = BaseAdmin.list_display + ('no_of_transactions', 'active', )

    def no_of_transactions(self, instance):
        return Transaction.objects.filter(slab__plan__insurer=instance).count()


class PlanFeatureInline(admin.TabularInline):
    model = PlanFeature
    extra = 1


class SlabFeatureInline(admin.TabularInline):
    model = SlabFeature
    extra = 1


class PlanAdmin(BaseAdmin):
    inlines = (PlanFeatureInline, )
    search_fields = ('name', 'insurer__name', 'type', 'sum_assured',
                     'currency', 'grade__name', 'verbose_name', )
    readonly_fields = ('features_hash', )
    list_display = BaseAdmin.list_display + \
        ('no_of_transactions', 'active', 'family',
         'individual', 'verbose_name', 'policy_wording_link')
    actions = ['attach_policy_wording_document']

    def policy_wording_link(self, instance):
        if instance.policy_wording:
            return "<a href='%s'>%s</a>" % (instance.policy_wording.file.url, instance.policy_wording.name, )
        else:
            return "No attachment"
    policy_wording_link.allow_tags = True

    def no_of_transactions(self, instance):
        return Transaction.objects.filter(slab__plan=instance).count()

    def family(self, instance):
        return instance.slab_set.filter(familyslab__isnull=False).exists()
    family.boolean = True

    def individual(self, instance):
        return instance.slab_set.filter(individualslab__isnull=False).exists()
    individual.boolean = True

    def attach_policy_wording_document(self, request, queryset):
        queryset.update(policy_wording=PolicyWordingDocument.objects.filter(
            plans_with_policy_wording__in=queryset).distinct().get())
        self.message_user(
            request, "Attached policy document to %d plans" % queryset.count())


class PlanInline(admin.TabularInline):
    model = Plan
    fields = ('name', 'insurer', )


class PolicyWordingDocumentAdmin(BaseAdmin):
    inlines = (PlanInline, )

    def get_queryset(self, request):
        queryset = super(PolicyWordingDocumentAdmin,
                         self).get_queryset(request)
        queryset &= queryset.model.objects.all()
        return queryset.distinct()


class SlabAdmin(BaseAdmin):
    inlines = (SlabFeatureInline, )
    search_fields = ('plan__name', 'plan__insurer__name', 'plan__type', 'plan__sum_assured',
                     'plan__currency', 'plan__grade__name', 'duration', 'premium', 'age_min', 'age_max', )


class TransactionAbstract(object):
    model = Transaction
    readonly_fields = ('activity', 'slab', 'created_date',
                       'status_history_', 'payment_tried', 'transaction_id', 'id', )

    def status_history_(self, instance):
        return ' -> '.join(instance.status_history)


class TransactionAdmin(TransactionAbstract, BaseAdmin):
    search_fields = ('slab__plan__name', 'slab__plan__insurer__name', 'slab__plan__type', 'slab__plan__sum_assured', 'slab__plan__currency',
                     'slab__plan__verbose_name', 'status', 'created_date', 'id', 'email', 'first_name', 'last_name', 'mobile', 'reference_id')
    list_display = BaseAdmin.list_display + \
        ('status', 'created_date', 'payment_tried', 'email', )
    resource_class = TransactionResource


class TransactionInline(TransactionAbstract, admin.TabularInline):
    extra = 0
    fields = ('id', 'status', 'slab', 'created_date', 'payment_tried', )


class ActivityAdmin(BaseAdmin):
    model = Activity
    fields = ('user', 'cleaned_data', 'data', )
    readonly_fields = fields
    exclude = ('tracker', )
    verbose_name_plural = 'Activities'
    inlines = (TransactionInline, )
    search_fields = ('id', 'tracker__user__emails__email', 'tracker__user__phones__number')
    list_display = ('id', 'no_of_transactions', 'created_date', )

    def no_of_transactions(self, instance):
        return instance.travel_transactions.count()

    def user(self, instance):
        return instance.tracker.user

    def cleaned_data(self, instance):
        try:
            return instance.cleaned_data
        except:
            return {}


class PlaceAdmin(BaseAdmin, DjangoMpttAdmin):
    pass


class FeatureNameAdmin(BaseAdmin, DjangoMpttAdmin):
    pass


admin.site.register(DeclinedRisk, BaseAdmin)
admin.site.register(FamilySlab, SlabAdmin)
admin.site.register(FeatureName, FeatureNameAdmin)
admin.site.register(Grade, BaseAdmin)
admin.site.register(IndividualSlab, SlabAdmin)
admin.site.register(Insurer, InsurerAdmin)
admin.site.register(Place, PlaceAdmin)
admin.site.register(Plan, PlanAdmin)
admin.site.register(StudentSlab, SlabAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(Activity, ActivityAdmin)
admin.site.register(Document)
admin.site.register(PolicyWordingDocument, PolicyWordingDocumentAdmin)
