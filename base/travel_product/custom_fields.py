from django import forms
from django.core import validators
import copy


class BaseField(forms.Field):
    css_class = ""
    layout_type = "base"

    def __init__(self, hidden=False, disabled=False, *args, **kwargs):
        self.hidden = hidden
        self.disabled = disabled
        self.prefill_value = None

        super(BaseField, self).__init__(*args, **kwargs)

        self._layout = {}

        self.error_messages['required'] = "Field required." # Just shortening the error message.

    # Similar to django field validators, js_validators are used in front-end.
    # CAUTION: Make sure the validator method that you are using in back-end should also be present in front-end
    # otherwise front end validation wont take place. (But front end validation is optional, so feel free to screw up).
    @property
    def js_validators(self):
        js_validators = []
        for v in self.validators:
            name = type(v).__name__
            js_validator = {
                    'validator': name,
                    'message': v.message if type(v.message) == str else v.message.title(),
                    'code':v.code,
                    'limit_value': v.limit_value if hasattr(v,'limit_value') else None,
                    'regex': v.regex.pattern if hasattr(v,'regex') else None,
                }
            if js_validator not in js_validators:
                js_validators.append(js_validator)
        return js_validators

    # Similar to django field's error_messages, js_error_messages are used in front-end.
    @property
    def js_error_messages(self):
        js_error_messages = {}

        for err_code, err in self.error_messages.items():
                js_error_messages[err_code] = err if type(err) == str else err.title()
        return js_error_messages

    def make_layout(self):
        pass

    def get_layout(self):
        self.make_layout()
        layout = copy.deepcopy(self._layout)

        layout.update({
            'type': self.widget.input_type if hasattr(self.widget,'input_type') else self.layout_type,
            'title': self.label,
            'value': self.prefill_value if self.prefill_value is not None else self.initial,
            'visible': self.required, # value will not be posted if false
            'hidden': self.hidden,  # value will be posted even if its false but it wont be visible in front-end
            'disabled': self.disabled, # non-editable field
            'cssClass': self.css_class,
            'validators': self.js_validators,
            'error_messages': self.js_error_messages,
            'max_length': self.max_length if hasattr(self, "max_length") else None,
            'min_length': self.min_length if hasattr(self, "min_length") else None,
            'min_value': self.min_value if hasattr(self, "min_value") else None,
            'max_value': self.max_value if hasattr(self, "max_value") else None,
            })
        return layout

    @property
    def layout(self):
        return self.get_layout()

    def set_layout(self, key, value):
        self._layout = copy.deepcopy(self.get_layout())
        self._layout.update({key: value})


## CUSTOM FIELDS ##

class ChoiceField(forms.ChoiceField, BaseField):
    css_class = "custom-select"
    layout_type = "select"

    def __init__(self, caption="--select--", *args, **kwargs):
        self.caption = caption

        super(ChoiceField, self).__init__(*args, **kwargs)

    def make_layout(self):
        self._layout.update({
            'optionsText': 'key',
            'optionsValue': 'value',
            'options': [{'key': c[1], 'value': c[0]} for c in self.choices],
            'caption': self.caption,
            })


class RadioField(ChoiceField, BaseField):
    layout_type = 'radio'
    css_class = "inline-label"

    YES = 'yes'
    NO = 'no'
    yes_no_choices = ((YES, 'Yes'), (NO, 'No'))

    def __init__(self, *args, **kwargs):
        super(RadioField, self).__init__(
                choices=self.yes_no_choices, initial=self.NO, *args, **kwargs
                )


class CharField(forms.CharField, BaseField):
    css_class = "inline-label"

    def __init__(self, *args, **kwargs):
        super(CharField, self).__init__(*args, **kwargs)

        if self.min_length is not None:
            self.error_messages['min_length'] = "minimum length should be %s"%self.min_length
        if self.max_length is not None:
            self.error_messages['max_length'] = "maximum length should be %s"%self.max_length

    def make_layout(self):
        if self.min_length is not None:
            self.error_messages['min_length'] = "minimum length should be %s"%self.min_length
        if self.max_length is not None:
            self.error_messages['max_length'] = "maximum length should be %s"%self.max_length



class IntegerField(forms.IntegerField, BaseField):
    css_class = "inline-label"

    def __init__(self, *args, **kwargs):
        super(IntegerField, self).__init__(*args, **kwargs)

        if self.min_value is not None:
            self.error_messages['min_value'] = "value should be above %s" %self.min_value
        if self.max_value is not None:
            self.error_messages['max_value'] = "value should be below %s" %self.max_value

    def make_layout(self):
        if self.min_value is not None:
            self.error_messages['min_value'] = "value should be above %s" %self.min_value
        if self.max_value is not None:
            self.error_messages['max_value'] = "value should be below %s" %self.max_value


class EmailField(forms.EmailField, BaseField):
    css_class = "inline-label"


class DateField(forms.DateField, BaseField):
    css_class = "inline-label"
    layout_type = "date"

    def __init__(self, *args, **kwargs):
        super(DateField, self).__init__(input_formats=['%d-%m-%Y', '%Y-%m-%d'], *args, **kwargs)
        self.widget.input_type = "date"


class CheckboxField(forms.BooleanField, BaseField):
    layout_type = 'checkbox'

    def __init__(self, *args, **kwargs):
        super(CheckboxField, self).__init__(*args, **kwargs)
