from django.core import validators


NoSpaceValidator = validators.RegexValidator("^[^ ]+$", "Space Not Allowed")

CharValidator = validators.RegexValidator("^[a-zA-Z ]+$", "Special Characters And Numbers Not Allowed")

NumberValidator = validators.RegexValidator("^[0-9]+$", "Only Digits Allowed")

AlphaNumValidator = validators.RegexValidator("^[a-zA-Z0-9]+$", "Special Characters Not Allowed")

MobileValidator = validators.RegexValidator("^[7-9][0-9]{9}$", "Invalid mobile number")

PassportValidator = validators.RegexValidator("^[a-zA-Z][0-9]{7}$", "Invalid Passport Number")

HdfcAddressValidator = validators.RegexValidator("^[a-zA-Z0-9-/.:()\\; ,//]+$", "Special characters other than -/:()\; ,.// are not allowed")
