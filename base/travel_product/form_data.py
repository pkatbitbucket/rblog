__author__ = 'vinay'


def split_name(name):
    splitted_name = name.split(" ")

    first_name = ' '.join(splitted_name[:-1])
    last_name = ''.join(splitted_name[-1])

    if not first_name:
        first_name = name
        last_name = ""

    return first_name, last_name


def is_dynamic_rel_type(form_data):
    relations = ["self", "spouse", "child1", "child2"]
    for rel in relations:
        if "%sFName" % rel in form_data:
            return True


def insert_relation_details(form_data):
    relations = ["self", "spouse", "child1", "child2"]
    for rel in relations:
        fname_key = "%sFName" % rel
        l_name_key = "%sLName" % rel
        is_sport_person_key = "%sbool_sports" % rel
        gender_key = "%sgender" % rel
        salutation_key = "%sSalutation" % rel
        birthday_key = "%sDOB" % rel
        assignee_name_key = "%sAName" % rel
        assignee_name_rel_key = "%sANameRelation" % rel
        nominee_name_key = "%sNName" % rel
        nominee_name_rel_key = "%sNNameRelation" % rel

        if fname_key in form_data:
            person_detail = {
                "first_name": form_data.get(fname_key, ''),
                "last_name": form_data.get(l_name_key, ''),
                "is_adventurous": form_data.get('selfbool_sports', ''),
                "email": form_data.get('email', ''),
                "gender": form_data.get(gender_key, ''),
                "salutation": form_data.get(salutation_key, ''),
                "is_sports_person": form_data.get(is_sport_person_key, ''),
                "is_nri": str(form_data.get("is_nri", "")),
                "birth_date": form_data.get(birthday_key, ''),
                "role": rel
            }

            if rel == "self":
                person_detail["birth_date"] = form_data.get('birth_date', '')
                person_detail["email"] = form_data.get('email', '')
            form_data.update({'{}_{}'.format(rel, key) for key, value in person_detail.items()})

            assignee_name = form_data.get(assignee_name_key, "")
            a_fname, a_lname = split_name(assignee_name)
            assignee_role = "assignee"
            if form_data.get(assignee_name_rel_key, ""):
                assignee_role += "_%s" % form_data.get(assignee_name_rel_key, "")

            person_detail = {
                "first_name": a_fname,
                "last_name": a_lname,
                "role": assignee_role
            }
            form_data.update({'{}_{}'.format(assignee_role, key) for key, value in person_detail.items()})

            nominee_name = form_data.get(nominee_name_key, "")
            nominee_f_name, nominee_l_name = split_name(nominee_name)
            nominee_role = "nominee"
            if form_data.get(nominee_name_rel_key, ""):
                nominee_role += "_%s" % form_data.get(nominee_name_rel_key, "")

            # Add nominee.
            nominee_detail = {
                "first_name": nominee_f_name,
                "last_name": nominee_l_name,
                "role": nominee_role
            }
            form_data.update({'{}_{}'.format(nominee_role, key) for key, value in nominee_detail.items()})

            # Health details.
            if "ped" in form_data:
                ped = form_data["ped"]
                if rel in ped:
                    health_que = {
                        "has_ped": ped.get(rel, ""),
                        "role": rel
                    }
                    form_data.update({'{}_{}'.format(rel, key) for key, value in health_que.items()})


def generate_form_data(transaction):
    form_data = transaction.insured_details_json
    data = {}

    if is_dynamic_rel_type(form_data):
        insert_relation_details(form_data)

    if "members" in form_data:
        for member in form_data['members']:
            person = {
                'first_name': member.get('cust_first_name', ''),
                'last_name': member.get('cust_last_name', ''),
                'gender': member.get('cust_gender', ''),
                'mobile': member.get('com_mobile', ''),
                'email': member.get('com_email', ''),
                'birth_date': member.get('cust_dob', ''),
                'occupation': member.get('cust_occupation', ''),
                'salutation': member.get('salutation', ''),
                'is_nri': str(form_data.get('is_nri', "")),
                'passport_no': member.get('cust_passport', ''),
            }
            role = member.get('cust_relationship_with_primary', '').lower().replace(" ", "_")

            person = {'{}_{}'.format(role, key): value for key, value in person.items()}
            data.update(person)

            # Nominee form.
            # nominee_first_name, nominee_last_name = split_name(member.get('nominee_name', ''))
            # person = {
            #     'first_name': nominee_first_name,
            #     'last_name': nominee_last_name,
            #     'role': 'nominee_%s' % member.get('cust_relationship_with_primary', '').lower().replace(" ", "_")
            # }
            # add_section(person, 'core.forms.Person', fd)

            # Insert health question answers.
            health_que = {}
            if "ped" in form_data:
                relation = member.get('cust_relationship_with_primary', '').lower()
                desease = form_data["ped"]
                if relation in desease:
                    desease = desease[relation]
                    if desease in ["None", "no"]:
                        health_que['has_other_diseases'] = False
                    elif desease in ["yes"]:
                        health_que['has_other_diseases'] = True

                    health_que["other_diseases"] = member.get("disease_name", '')
            health_que = {'{}_{}'.format(role, key): value for key, value in health_que.items()}
            data.update(health_que)

    if "member1FName" in form_data:
        person_detail = {
            "first_name": form_data.get('member1FName', ''),
            "middle_name": form_data.get('member1LName', ''),
            "last_name": form_data.get('member1LName', ''),
            "gender": form_data.get('member1gender', ''),
            "salutation": form_data.get('member1Salutation', ''),
            "passport_number": form_data.get('member1Passport', ''),
            "birth_date": form_data.get('member1DOB', ''),
            "role": "self"
        }
        person_detail = {'self_{}'.format(key): value for key, value in person_detail.items()}
        data.update(person_detail)

        if "member1PEDotherDetailsTravel" in form_data:
            health_que = {
                "has_other_diseases": form_data.get("member1PEDotherDetailsTravel", ""),
                "has_heart_diseases": form_data.get("member1PEDHealthDiseaseTravel", ""),
                "has_stroke_paralysis": form_data.get("member1PEDparalysisDetailsTravel", ""),
                "has_cancer": form_data.get("member1PEDcancerDetailsTravel", ""),
                "has_lever_desease": form_data.get("member1PEDliverDetailsTravel", ""),
                "has_kidney_desease": form_data.get("member1PEDkidneyDetailsTravel", "")
            }
            health_que = {'self_{}'.format(key): value for key, value in health_que.items()}
            data.update(health_que)

    # -----------------------------------------------------------------
    travel_data = {
        'places': form_data.get('places', ''),
        "from_date": form_data.get('from_date', ''),
        "to_date": form_data.get('from_date', ''),
    }
    data.update(travel_data)

    # -----------------------------------------------------------------
    # Get address info.
    comm_address = {}
    home_address = {}
    if "contact" in form_data:
        contact = form_data['contact']
        address_info = {}
        if "com_address_line_1" in contact:
            address_info['address_line1'] = contact.get('com_address_line_1', '')
            address_info['address_line2'] = contact.get('com_address_line_2', '')
            address_info['address_line3'] = contact.get('com_address_line_3', '')
            address_info['pincode'] = contact.get('com_pincode', '')
            address_info['city'] = contact.get('com_city', '')
            address_info['state'] = contact.get('com_state', '')
            address_info['role'] = 'communication'

            comm_address = address_info

        address_info = {}
        if "home_address_line_1" in contact:
            address_info['address_line1'] = contact.get('home_address_line_1', '')
            address_info['address_line2'] = contact.get('home_address_line_2', '')
            address_info['address_line3'] = contact.get('home_address_line_3', '')
            address_info['pincode'] = contact.get('home_pincode', '')
            address_info['passport_no'] = contact.get('cust_passport', '')
            address_info['city'] = contact.get('home_city', '')
            address_info['state'] = contact.get('home_state', '')
            address_info['role'] = 'home'

            home_address = {'home_{}'.format(key): value for key, value in address_info.items()}

    address_info = {}
    if "ResAddr1" in form_data:
        address_info['address_line1'] = form_data.get('ResAddr1', '')
        address_info['address_line2'] = form_data.get('ResAddr2', '')
        address_info['address_line3'] = form_data.get('ResAddr3', '')
        address_info['address_line3'] = form_data.get('ResAddr3', '')
        address_info['passport_no'] = form_data.get('cust_passport', '')
        address_info['city'] = form_data.get('city', '')
        address_info['state'] = form_data.get('state', '')
        address_info['pincode'] = form_data.get('pincode', '')
        address_info['role'] = 'communication'

        comm_address = address_info

    data.update(comm_address)
    data.update(home_address)

    return data
