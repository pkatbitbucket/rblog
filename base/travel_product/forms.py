from django import forms
from django.contrib.auth import get_user_model
from django.contrib.humanize.templatetags.humanize import intcomma
from django.db.models import Q, Max
from django.db.models.fields import BLANK_CHOICE_DASH

from putils import add_years
from .models import Plan, IndividualSlab, FamilySlab, StudentSlab, Insurer, DeclinedRisk, SLAB_CHOICES
from core import forms as core_forms

import datetime
import inspect


User = get_user_model()


class InsurerForm(forms.ModelForm):

    class Meta:
        model = Insurer
        fields = ('minimum_residency', 'minimum_processing_days', 'nri', )


class PlanForm(forms.ModelForm):

    class Meta:
        model = Plan
        fields = ('type', 'places_included', 'places_excluded', 'sum_assured', )


class AbstractForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AbstractForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].required = False

    class Meta:
        exclude = ('plan', )


class IndividualSlabForm(AbstractForm):

    class Meta(AbstractForm.Meta):
        model = IndividualSlab


class FamilySlabForm(AbstractForm):

    class Meta(AbstractForm.Meta):
        model = FamilySlab


class StudentSlabForm(AbstractForm):

    class Meta(AbstractForm.Meta):
        model = StudentSlab


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        exclude = ()


class MultiAgeValueField(forms.MultiValueField):

    def clean(self, value):
        if isinstance(value, basestring):
            value = value.split(',')
        return super(self.__class__, self).clean(value)

    def compress(self, data_list):
        return filter(bool, data_list)


class SumAssuredMatrix(object):

        sum_assured_matrix = {
            'single': [
                ([25, 50], [100, 100], ),
                ([50, 100], [100, 100], ),
                ([50, 100], [100, 100], ),
                ([50, 100], [100, 200], ),
                ([50, 100], [100, 200], ),
                ([100, 200], [200, 200], ),
                ([100, 200], [200, 200], ),
            ],
            'multi': [
                200, 250
            ],
        }
        #  index, included places, excluded places
        regions = {
            'ASIA_EXCLUDING_JAPAN': (0, ('asia', ), ('japan', )),
            'AFRICA':               (1, ('africa', ), ()),
            'REST_OF_EUROPE':       (2, ('non-schengen', ), ()),
            'SCHENGEN':             (3, ('schengen', ), ()),
            'UK':                   (4, ('united-kingdom', ), ()),
            'AMERICA_CANADA':       (5, ('usa-united-states-of-america', 'canada', 'north-america', ), ()),
            'OTHER_COUNTRIES':      (6, ('worldwide', ), ()),
        }

        ages = {
            'BELOW_AND_40': 0,
            'ABOVE_40': 1,
        }
        durations = {
            "LTE_15_DAYS": 0,
            "GT_15_DAYS": 1,
        }

        def get_region(self, places):
            all_places = places.model.objects.none()
            for place in places:
                all_places |= place.get_ancestors(include_self=True)
            all_places = all_places.distinct()

            region = None
            for region_name, values in self.regions.items():
                _, included_places, excluded_places = values
                matches = set(all_places.filter(slug__in=included_places).values_list('slug', flat=True))
                if matches and matches.issubset(included_places) and not all_places.filter(slug__in=excluded_places).exists():
                    if region and region is not region_name:
                        break
                    else:
                        region = region_name
                        continue
            return region

        def get_sum_assured(self, places, duration, age, type):
            region = self.regions[self.get_region(places) or 'OTHER_COUNTRIES'][0]
            age = self.ages['ABOVE_40' if age > 40 else 'BELOW_AND_40']
            duration = self.durations['GT_15_DAYS' if duration > 15 else 'LTE_15_DAYS']
            if type == 'single':
                return self.sum_assured_matrix[type][region][age][duration]*1000
            else:
                return self.sum_assured_matrix[type][age]*1000


class QuoteForm(forms.Form):

    SUM_ASSURED_RANGES = {
        25: [None, 50],
        50: [50,  100],
        100: [100, 200],
        200: [200, 250],
        250: [250, 500],
        500: [500, None],
    }

    sum_assured_x_1000 = sorted(map(lambda s: s*1000, SUM_ASSURED_RANGES.keys()))
    SUM_ASSURED_CHOICES = zip(sum_assured_x_1000, map(lambda s: intcomma(s), sum_assured_x_1000))

    def multi_fields(field):
        return [forms.IntegerField(
            max_value=FamilySlabForm.base_fields['age_max'].max_value,
            min_value=FamilySlabForm.base_fields['age_min'].min_value) for i in range(
                FamilySlabForm.base_fields[field + '_max'].min_value,
                FamilySlabForm.base_fields[field + '_max'].max_value
            )
        ]

    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.today = datetime.date.today()
        self.fields['places'].to_field_name = 'slug'
        self.fields['places_excluded'].to_field_name = 'slug'

        self.fields['from_date'].initial = self.today + datetime.timedelta(7)
        self.fields['to_date'].initial = self.today + datetime.timedelta(14)

        optional_fields = ('sum_assured', 'slab', 'to_date', 'duration', 'children', 'adults', )
        for field in optional_fields:
            self.fields[field].required = False

        self.required_fields = ()

    places = PlanForm.base_fields['places_included']
    places_excluded = PlanForm.base_fields['places_excluded']
    from_date = forms.DateField()
    to_date = forms.DateField()
    duration = StudentSlabForm.base_fields['duration']
    sum_assured = forms.TypedChoiceField(coerce=int, required=False, choices=BLANK_CHOICE_DASH + SUM_ASSURED_CHOICES, widget=forms.Select())
    children = MultiAgeValueField(fields=multi_fields('children'))
    adults = MultiAgeValueField(fields=multi_fields('adult'))
    type = PlanForm.base_fields['type']
    slab = forms.TypedChoiceField(choices=SLAB_CHOICES, widget=forms.HiddenInput())
    resident_for = forms.FloatField(help_text='years', required=False)  # -1 for lower than x years
    declined_risks = forms.ModelMultipleChoiceField(
        required=False, queryset=DeclinedRisk.objects.filter(insurer__isnull=False).distinct(), to_field_name='slug'
    )

    def clean_from_date(self):
        from_date = self.cleaned_data['from_date']
        if from_date < self.today:
            raise forms.ValidationError("from date cannot be in the past!")

        return from_date

    def clean_to_date(self):
        to_date = self.cleaned_data['to_date']
        if to_date and to_date < datetime.date.today():
            raise forms.ValidationError("to date cannot be in the past!")
        return to_date

    def clean_type(self):
        type = self.cleaned_data['type']
        if type == 'single':
            self.required_fields += ('to_date', )
        elif type == 'multi':
            self.required_fields += ('duration', )
        return type

    def _clean_slab(self):
        cd = self.cleaned_data
        slab = cd['slab']
        if not slab:
            slab = 'family' if (len(cd['adults']) + len(cd['children'])) > 1 else 'individual'

        if slab == 'student':
            self.required_fields += ('duration', 'adults', )
        else:
            self.required_fields += ('adults', )

        return slab

    def clean(self):
        cd = self.cleaned_data
        if self.errors:
            return cd
        cd['slab'] = self._clean_slab()
        adult_age_cutoff = [1, 18][cd['slab'] == 'family']
        underaged_adults = filter(lambda a: a < adult_age_cutoff, cd['adults'])
        cd['children'] += underaged_adults
        cd['adults'] = filter(lambda a: a not in underaged_adults, cd['adults'])
        for field in set(self.required_fields):
            if hasattr(field, '__iter__'):
                if not filter(bool, [cd[f] for f in field]):
                    raise forms.ValidationError('atleast one of %s is required' % (','.join(field), ))
            elif not (field in cd and cd[field]):
                raise forms.ValidationError('%s is required' % (field, ))

        if 'to_date' in self.required_fields and cd['to_date'] < cd['from_date']:
            raise forms.ValidationError('to date cannot be before from date')

        if cd['type'] == 'multi':
            cd['to_date'] = add_years(cd['from_date'], 1) - datetime.timedelta(days=1)

        if not cd['duration']:
            cd['duration'] = (cd['to_date'] - cd['from_date']).days + 1

        cd['ages'] = cd['adults'] + cd['children']
        cd['max_age'] = max(cd['ages'])

        cd['_sum_assured'] = cd['sum_assured'] or SumAssuredMatrix().get_sum_assured(
            cd['places'], cd['duration'], cd['max_age'], cd['type']
        )
        del cd['adults']
        del cd['children']

        return cd


class InsurerRuleForm(object):
    adult_cutoff = 18
    legal_age = 18

    def __init__(self, instance, *args, **kwargs):
        self.instance = instance

    def _get_adults_and_children_count(self, ages):
        ages = list(ages)[:]
        children = filter(lambda a: a < self.adult_cutoff, ages)
        adults = filter(lambda a: a not in children, ages)
        assert adults, 'no member over %d' % (self.adult_cutoff, )
        return adults, children

    def get_annotation_rules(self):
        return {}

    def get_filter_rules(self, cd):
        if cd['slab'] == 'family':
            adults, children = self._get_adults_and_children_count(cd['ages'])
            familyslab_age_filters = Q(
                familyslab__size__gte=len(adults + children),
                familyslab__children_max=len(children),
                familyslab__adult_max=len(adults),
            )
        else:
            familyslab_age_filters = Q()

        return familyslab_age_filters


class RelianceRuleForm(InsurerRuleForm):
    adult_cutoff = 21
    _max_adult_count = None

    def _get_max_adult_count(self):
        if not self._max_adult_count:
            self._max_adult_count = self.instance.plan_set.aggregate(
                max_adult_count=Max('slab__familyslab__adult_max')
            )['max_adult_count']
        return self._max_adult_count

    def _get_adults_and_children_count(self, ages):
        adults, children = super(RelianceRuleForm, self)._get_adults_and_children_count(ages)

        # If there are already max number of adults present then do nothing
        if len(adults) < self._get_max_adult_count() and children:
            elder_spouse = min(adults)
            eldest_child = max(children)
            # If there is a children between legal age and adult cutoff then maybe it's a spouse?
            # Also age difference between spouses should be less than and equal to the legal age
            if eldest_child >= self.legal_age and (elder_spouse - eldest_child) <= self.legal_age:
                children.remove(eldest_child)
                adults.append(eldest_child)
        return adults, children

    def get_filter_rules(self, cd):
        delta = (cd['from_date'] - datetime.date.today()).days
        assert 90 >= delta, "Travel date is alot ahead than policy start date, %d < %d" % (90, delta, )
        return super(RelianceRuleForm, self).get_filter_rules(cd)


class BajajRuleForm(InsurerRuleForm):
    def get_filter_rules(self, cd):
        delta = (cd['from_date'] - datetime.date.today()).days
        assert 90 >= delta, "Travel date is alot ahead than policy start date, %d < %d" % (90, delta, )
        return super(BajajRuleForm, self).get_filter_rules(cd)


class ReligareRuleForm(InsurerRuleForm):
    def get_annotation_rules(self):
        sub_limit = (
                Q(age_min__gte=61) & ~Q(plan__name__in=['Explore - Europe', 'Explore - Platinum']),
                'Sub-limits applicable for the age group of 61 Years & above',
            ),
        return {
            'sub_limit': sub_limit
        }


class HDFCRuleForm(InsurerRuleForm):
    def get_annotation_rules(self):
        sub_limit = (
                Q(age_min__gte=61),
                'Sub-limits applicable for the age group of 61 Years & above',
            ),
        return {
            'sub_limit': sub_limit
        }


vars = locals().values()
rule_forms = ()
for var in vars:
    try:
        if InsurerRuleForm in inspect.getmro(var):
            rule_forms += ((var.__name__, var, ), )
    except AttributeError:
        pass
rule_forms = dict(rule_forms)


class InsurerAdminForm(forms.ModelForm):
    PROPOSAL_FORM_CHOICES = zip(rule_forms.keys(), rule_forms.keys())

    def __init__(self, *args, **kwargs):
        super(InsurerAdminForm, self).__init__(*args, **kwargs)
        if self.instance.rule_form:
            try:
                self.initial['rule_form'] = self.fields['rule_form'].initial = self.instance.rule_form.__name__
            except AttributeError:
                self.instance.rule_form = None
                self.instance.save()

    rule_form = forms.TypedChoiceField(
        choices=BLANK_CHOICE_DASH + PROPOSAL_FORM_CHOICES, coerce=lambda value: rule_forms[value], required=False, empty_value=None
    )


def get_travel_fd_form_fields():
    fields = []
    person_tags = [
        "self", "son1", "son2", "son3", "son4", "son5", "daughter1", "daughter2",
        "daughter3", "daughter4", "daughter5"
    ]
    # Generate person form.
    for tag in person_tags:
        person_field = core_forms.Person(
            tag=tag,
            salutation="%s_salutation" % tag,
            first_name="%s_first_name" % tag,
            last_name="%s_last_name" % tag,
            gender="%s_gender" % tag,
            mobile="%s_mobile" % tag,
            email="%s_email" % tag,
            birth_date="%s_birth_date" % tag,
            occupation="%s_occupation" % tag,
            is_nri="%s_is_nri" % tag,
            passport_no="%s_passport_no" % tag,
        )

        health_field = core_forms.HealthQuestions(
            tag=tag,
            has_other_diseases="%s_has_other_diseases" % tag,
            has_heart_diseases="%s_has_heart_diseases" % tag,
            has_stroke_paralysis="%s_has_stroke_paralysis" % tag,
            has_cancer="%s_has_cancer" % tag,
            has_lever_desease="%s_has_lever_desease" % tag,
            has_kidney_desease="%s_has_kidney_desease" % tag
        )
        fields.append(person_field)
        fields.append(health_field)

    # Generate address tag.
    add1 = core_forms.Address(
        tag="home",
        address_line1="home_address_line1",
        address_line2="home_address_line2",
        address_line3="home_address_line3",
        landmark="home_landmark",
        city="home_city",
        state="home_state",
        pincode="home_pincode"
    )

    add2 = core_forms.Address(
        tag="communication",
        address_line1="address_line1",
        address_line2="address_line2",
        address_line3="address_line3",
        landmark="landmark",
        city="city",
        state="state",
        pincode="pincode"
    )
    fields.append(add1)
    fields.append(add2)
    fields.append(core_forms.TravelData)
    return fields


class TravelFDForm(core_forms.FDForm):
    places = forms.CharField(required=False)
    from_date = forms.CharField(required=False)
    to_date = forms.CharField(required=False)

    self_salutation = forms.CharField(required=False)
    self_first_name = forms.CharField(required=False)
    self_last_name = forms.CharField(required=False)
    self_gender = forms.CharField(required=False)
    self_mobile = forms.CharField(required=False)
    self_passport_no = forms.CharField(required=False)
    self_is_nri = forms.CharField(required=False)
    self_email = forms.CharField(required=False)
    self_birth_date = forms.CharField(required=False)
    self_occupation = forms.CharField(required=False)
    self_has_other_diseases = forms.CharField(required=False)
    self_has_heart_diseases = forms.CharField(required=False)
    self_has_stroke_paralysis = forms.CharField(required=False)
    self_has_cancer = forms.CharField(required=False)
    self_has_lever_desease = forms.CharField(required=False)
    self_has_kidney_desease = forms.CharField(required=False)

    son1_salutation = forms.CharField(required=False)
    son1_first_name = forms.CharField(required=False)
    son1_last_name = forms.CharField(required=False)
    son1_gender = forms.CharField(required=False)
    son1_mobile = forms.CharField(required=False)
    son1_passport_no = forms.CharField(required=False)
    son1_is_nri = forms.CharField(required=False)
    son1_email = forms.CharField(required=False)
    son1_birth_date = forms.CharField(required=False)
    son1_occupation = forms.CharField(required=False)
    son1_has_other_diseases = forms.CharField(required=False)
    son1_has_heart_diseases = forms.CharField(required=False)
    son1_has_stroke_paralysis = forms.CharField(required=False)
    son1_has_cancer = forms.CharField(required=False)
    son1_has_lever_desease = forms.CharField(required=False)
    son1_has_kidney_desease = forms.CharField(required=False)

    son2_salutation = forms.CharField(required=False)
    son2_first_name = forms.CharField(required=False)
    son2_last_name = forms.CharField(required=False)
    son2_gender = forms.CharField(required=False)
    son2_mobile = forms.CharField(required=False)
    son2_passport_no = forms.CharField(required=False)
    son2_is_nri = forms.CharField(required=False)
    son2_email = forms.CharField(required=False)
    son2_birth_date = forms.CharField(required=False)
    son2_occupation = forms.CharField(required=False)
    son2_has_other_diseases = forms.CharField(required=False)
    son2_has_heart_diseases = forms.CharField(required=False)
    son2_has_stroke_paralysis = forms.CharField(required=False)
    son2_has_cancer = forms.CharField(required=False)
    son2_has_lever_desease = forms.CharField(required=False)
    son2_has_kidney_desease = forms.CharField(required=False)

    son3_salutation = forms.CharField(required=False)
    son3_first_name = forms.CharField(required=False)
    son3_last_name = forms.CharField(required=False)
    son3_gender = forms.CharField(required=False)
    son3_mobile = forms.CharField(required=False)
    son3_passport_no = forms.CharField(required=False)
    son3_is_nri = forms.CharField(required=False)
    son3_email = forms.CharField(required=False)
    son3_birth_date = forms.CharField(required=False)
    son3_occupation = forms.CharField(required=False)
    son3_has_other_diseases = forms.CharField(required=False)
    son3_has_heart_diseases = forms.CharField(required=False)
    son3_has_stroke_paralysis = forms.CharField(required=False)
    son3_has_cancer = forms.CharField(required=False)
    son3_has_lever_desease = forms.CharField(required=False)
    son3_has_kidney_desease = forms.CharField(required=False)

    son4_salutation = forms.CharField(required=False)
    son4_first_name = forms.CharField(required=False)
    son4_last_name = forms.CharField(required=False)
    son4_gender = forms.CharField(required=False)
    son4_mobile = forms.CharField(required=False)
    son4_passport_no = forms.CharField(required=False)
    son4_is_nri = forms.CharField(required=False)
    son4_email = forms.CharField(required=False)
    son4_birth_date = forms.CharField(required=False)
    son4_occupation = forms.CharField(required=False)
    son4_has_other_diseases = forms.CharField(required=False)
    son4_has_heart_diseases = forms.CharField(required=False)
    son4_has_stroke_paralysis = forms.CharField(required=False)
    son4_has_cancer = forms.CharField(required=False)
    son4_has_lever_desease = forms.CharField(required=False)
    son4_has_kidney_desease = forms.CharField(required=False)

    son5_salutation = forms.CharField(required=False)
    son5_first_name = forms.CharField(required=False)
    son5_last_name = forms.CharField(required=False)
    son5_gender = forms.CharField(required=False)
    son5_mobile = forms.CharField(required=False)
    son5_passport_no = forms.CharField(required=False)
    son5_is_nri = forms.CharField(required=False)
    son5_email = forms.CharField(required=False)
    son5_birth_date = forms.CharField(required=False)
    son5_occupation = forms.CharField(required=False)
    son5_has_other_diseases = forms.CharField(required=False)
    son5_has_heart_diseases = forms.CharField(required=False)
    son5_has_stroke_paralysis = forms.CharField(required=False)
    son5_has_cancer = forms.CharField(required=False)
    son5_has_lever_desease = forms.CharField(required=False)
    son5_has_kidney_desease = forms.CharField(required=False)

    daughter1_salutation = forms.CharField(required=False)
    daughter1_first_name = forms.CharField(required=False)
    daughter1_last_name = forms.CharField(required=False)
    daughter1_gender = forms.CharField(required=False)
    daughter1_mobile = forms.CharField(required=False)
    daughter1_passport_no = forms.CharField(required=False)
    daughter1_is_nri = forms.CharField(required=False)
    daughter1_email = forms.CharField(required=False)
    daughter1_birth_date = forms.CharField(required=False)
    daughter1_occupation = forms.CharField(required=False)
    daughter1_has_other_diseases = forms.CharField(required=False)
    daughter1_has_heart_diseases = forms.CharField(required=False)
    daughter1_has_stroke_paralysis = forms.CharField(required=False)
    daughter1_has_cancer = forms.CharField(required=False)
    daughter1_has_lever_desease = forms.CharField(required=False)
    daughter1_has_kidney_desease = forms.CharField(required=False)

    daughter2_salutation = forms.CharField(required=False)
    daughter2_first_name = forms.CharField(required=False)
    daughter2_last_name = forms.CharField(required=False)
    daughter2_gender = forms.CharField(required=False)
    daughter2_mobile = forms.CharField(required=False)
    daughter2_passport_no = forms.CharField(required=False)
    daughter2_is_nri = forms.CharField(required=False)
    daughter2_email = forms.CharField(required=False)
    daughter2_birth_date = forms.CharField(required=False)
    daughter2_occupation = forms.CharField(required=False)
    daughter2_has_other_diseases = forms.CharField(required=False)
    daughter2_has_heart_diseases = forms.CharField(required=False)
    daughter2_has_stroke_paralysis = forms.CharField(required=False)
    daughter2_has_cancer = forms.CharField(required=False)
    daughter2_has_lever_desease = forms.CharField(required=False)
    daughter2_has_kidney_desease = forms.CharField(required=False)

    daughter3_salutation = forms.CharField(required=False)
    daughter3_first_name = forms.CharField(required=False)
    daughter3_last_name = forms.CharField(required=False)
    daughter3_gender = forms.CharField(required=False)
    daughter3_mobile = forms.CharField(required=False)
    daughter3_passport_no = forms.CharField(required=False)
    daughter3_is_nri = forms.CharField(required=False)
    daughter3_email = forms.CharField(required=False)
    daughter3_birth_date = forms.CharField(required=False)
    daughter3_occupation = forms.CharField(required=False)
    daughter3_has_other_diseases = forms.CharField(required=False)
    daughter3_has_heart_diseases = forms.CharField(required=False)
    daughter3_has_stroke_paralysis = forms.CharField(required=False)
    daughter3_has_cancer = forms.CharField(required=False)
    daughter3_has_lever_desease = forms.CharField(required=False)
    daughter3_has_kidney_desease = forms.CharField(required=False)

    daughter4_salutation = forms.CharField(required=False)
    daughter4_first_name = forms.CharField(required=False)
    daughter4_last_name = forms.CharField(required=False)
    daughter4_gender = forms.CharField(required=False)
    daughter4_mobile = forms.CharField(required=False)
    daughter4_passport_no = forms.CharField(required=False)
    daughter4_is_nri = forms.CharField(required=False)
    daughter4_email = forms.CharField(required=False)
    daughter4_birth_date = forms.CharField(required=False)
    daughter4_occupation = forms.CharField(required=False)
    daughter4_has_other_diseases = forms.CharField(required=False)
    daughter4_has_heart_diseases = forms.CharField(required=False)
    daughter4_has_stroke_paralysis = forms.CharField(required=False)
    daughter4_has_cancer = forms.CharField(required=False)
    daughter4_has_lever_desease = forms.CharField(required=False)
    daughter4_has_kidney_desease = forms.CharField(required=False)

    daughter5_salutation = forms.CharField(required=False)
    daughter5_first_name = forms.CharField(required=False)
    daughter5_last_name = forms.CharField(required=False)
    daughter5_gender = forms.CharField(required=False)
    daughter5_mobile = forms.CharField(required=False)
    daughter5_passport_no = forms.CharField(required=False)
    daughter5_is_nri = forms.CharField(required=False)
    daughter5_email = forms.CharField(required=False)
    daughter5_birth_date = forms.CharField(required=False)
    daughter5_occupation = forms.CharField(required=False)
    daughter5_has_other_diseases = forms.CharField(required=False)
    daughter5_has_heart_diseases = forms.CharField(required=False)
    daughter5_has_stroke_paralysis = forms.CharField(required=False)
    daughter5_has_cancer = forms.CharField(required=False)
    daughter5_has_lever_desease = forms.CharField(required=False)
    daughter5_has_kidney_desease = forms.CharField(required=False)

    spouse_salutation = forms.CharField(required=False)
    spouse_first_name = forms.CharField(required=False)
    spouse_last_name = forms.CharField(required=False)
    spouse_gender = forms.CharField(required=False)
    spouse_mobile = forms.CharField(required=False)
    spouse_passport_no = forms.CharField(required=False)
    spouse_is_nri = forms.CharField(required=False)
    spouse_email = forms.CharField(required=False)
    spouse_birth_date = forms.CharField(required=False)
    spouse_occupation = forms.CharField(required=False)
    spouse_has_other_diseases = forms.CharField(required=False)
    spouse_has_heart_diseases = forms.CharField(required=False)
    spouse_has_stroke_paralysis = forms.CharField(required=False)
    spouse_has_cancer = forms.CharField(required=False)
    spouse_has_lever_desease = forms.CharField(required=False)
    spouse_has_kidney_desease = forms.CharField(required=False)

    address_line1 = forms.CharField(required=False)
    address_line2 = forms.CharField(required=False)
    address_line3 = forms.CharField(required=False)
    landmark = forms.CharField(required=False)
    city = forms.CharField(required=False)
    state = forms.CharField(required=False)
    pincode = forms.CharField(required=False)

    home_address_line1 = forms.CharField(required=False)
    home_address_line2 = forms.CharField(required=False)
    home_address_line3 = forms.CharField(required=False)
    home_landmark = forms.CharField(required=False)
    home_city = forms.CharField(required=False)
    home_state = forms.CharField(required=False)
    home_pincode = forms.CharField(required=False)

    class Config:
        fields = get_travel_fd_form_fields()


class TravelQuoteFDForm(core_forms.FDForm):
    type = forms.CharField(10)
    places = forms.CharField(100)
    isResident = forms.CharField(10)
    from_date = forms.CharField(20)
    to_date = forms.CharField(20)
    traveller_count = forms.CharField(2)
    traveller1_age = forms.CharField(3)
    traveller2_age = forms.CharField(3)
    traveller3_age = forms.CharField(3)
    traveller4_age = forms.CharField(3)
    traveller5_age = forms.CharField(3)
    traveller6_age = forms.CharField(3)
    trip_start_place_india = forms.CharField(10)
    duration = forms.CharField(10)
    mobile = forms.CharField(20)
    age = forms.CharField(3)

    class Config:
        fields = [
            core_forms.Person(
                tag="role",
                mobile='mobile'
            ),
            core_forms.TravelQuoteForm(
                tag="self",
                product_type="type",
                is_nri="isResident",
                places="places",
                trip_start_date="from_date",
                trip_end_date="to_date",
                traveller_count="traveller_count",
                traveller1_age="traveller1_age",
                traveller2_age="traveller2_age",
                traveller3_age="traveller3_age",
                traveller4_age="traveller4_age",
                traveller5_age="traveller5_age",
                traveller6_age="traveller6_age",
                trip_start_place_india="trip_start_place_india",
                longest_trip_duration="duration"
            )
        ]
