import json
import copy
import preset
from travel_product.insurers import common_preset

STATES = copy.deepcopy(common_preset.STATES)
CITIES = copy.deepcopy(common_preset.CITIES)
# COUNTRIES = copy.deepcopy(preset.COUNTRIES)


def get_form(request, transaction):
    members = transaction.plan_details['members']
    return {
        'num_insured': len(members),
        'members_list': members,
        'states_list': STATES,
        'cities_list': CITIES,
        # 'countries': COUNTRIES
    }

