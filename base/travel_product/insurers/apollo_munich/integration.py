from django.conf import settings
from travel_product.models import Transaction
from travel_product import prod_utils
from travel_product import settings as travel_settings
from django.core.files.base import File
from collections import OrderedDict
from suds.client import Client
from dateutil.relativedelta import *
from lxml import etree
from utils import travel_logger
import os
import datetime
import requests
import httplib2
import uuid

if settings.PRODUCTION:
    REQUEST_URL = "http://b2b.apollomunichinsurance.com/TravelPremiumCalculatorHttpService.svc"
    PROPOSAL_URL = "http://b2b.apollomunichinsurance.com/TravelProposalCaptureHttpService.svc"
    PAYMENT_URL = 'https://b2b.apollomunichinsurance.com/Travel-Payment-Options.aspx'
    AGENT_ID = '80171360'
    TOKEN_ID = '8D1152515E21802DA2C4C051DF0061A280171360'
    PASSWORD = '06127F59F6059EB2'
    PAYMENT_WSDL_URL = 'http://b2b.apollomunichinsurance.com/TravelPaymentGatewayHttpService.svc?wsdl'
else:
    REQUEST_URL = "http://b2buat.apollomunichinsurance.com/TravelPremiumCalculatorHttpService.svc"
    PROPOSAL_URL = "http://b2buat.apollomunichinsurance.com/TravelProposalCaptureHttpService.svc"
    PAYMENT_URL = 'https://b2buat.apollomunichinsurance.com/Travel-Payment-Options.aspx'
    AGENT_ID = '80171360'
    TOKEN_ID = 'CCF04AA0500DB61D0BA62AA9951A451320'
    PASSWORD = 'CCF04AA0500DB61D0BA62AA9951A4513'
    PAYMENT_WSDL_URL = 'http://b2b.apollomunichinsurance.com/TravelPaymentGatewayHttpService.svc?wsdl'

    
FAMILY_MEMBERS = {
    '2A': 'SELF+SPOUSE',
    '2A1C': 'SELF+SPOUSE+CHILD1',
    '2A2C': 'SELF+SPOUSE+CHILD1+CHILD2',
    '2A3C': 'SELF+SPOUSE+CHILD1+CHILD2+CHILD3',
    '2A4C': 'SELF+SPOUSE+CHILD1+CHILD2+CHILD3+CHILD4',
}

PLAN_NAMES = {
    '31001': 'Easy Travel Individual',
    '31002': 'Easy Travel Family',
    '31005': 'Easy Travel AMT',
        }

PLAN_CODES = {
    'PLATINUM': 'USD 500,000',
    'GOLD': 'USD 250,000',
    'SILVER': 'USD 100,000',
    'BRONZE': 'USD 50,000',
    'ASIAN': 'USD 25,000',
}


def pre_transaction(transaction):
    search_data = transaction.extra['search_data']
    policy_type = search_data['policy_type']
    family = search_data.get('family_type') if policy_type == "F" else ""
    insureds = [mem.strip() for mem in FAMILY_MEMBERS[family].split('+')] if policy_type == "F" else ['SELF']
    transaction.plan_details['members'] = insureds
    transaction.plan_details['plan'] = transaction.plan_details['plan_type']
    return None

    
def get_quote_dict(search_data):
    trip_type = search_data['trip_type']
    max_trip_length = search_data["max_trip_length"] if trip_type == "R" else ""
    trip_duration = max_trip_length if trip_type == "R" else search_data['trip_duration']
    from_date = search_data['from_date']

    if from_date != "":
        start_date = datetime.datetime.strptime(from_date, '%d-%b-%Y').strftime('%d-%m-%Y')
    else:
        start_date = datetime.datetime.now().strftime('%d-%m-%Y')
    
    travel_logger.info('================================\n%s get_quote_dict:\n%s' % (max_trip_length, start_date))

    to_date = search_data['to_date']

    if to_date != '':
        end_date = datetime.datetime.strptime(to_date, '%d-%b-%Y').strftime('%d-%m-%Y')
    else:
        end_date = datetime.datetime.strptime(start_date, '%d-%m-%Y') + relativedelta(days=int(trip_duration)-1).strftime('%d-%m-%Y')
    travel_logger.info('================================\n%s MUltip-plan' % (end_date))

    plan = search_data['plan']
    travel_logger.info('================================\n%s MUltip-plan' % (plan))
    multi_plan = plan + '30' if max_trip_length == "30" else plan + '60'
    travel_logger.info('================================\n%s MUltip-plan' % (multi_plan))

    message_id = str(uuid.uuid4())[:24]

    date_of_birth = search_data['DOB']
    age = relativedelta(
        datetime.datetime.today(),
        datetime.datetime.strptime(date_of_birth, travel_settings.DATE_FORMAT).date()
    ).years

    geographical_coverage = search_data['geographical_coverage']
    visa_type = search_data['visa_type']
    policy_type = search_data['policy_type']

    if (trip_type == "S") and (policy_type == "I" or policy_type == "S"):
        product_cd = "31001"
    elif trip_type == "S" and policy_type == "F":
        product_cd = "31002"
    else:
        product_cd = "31005"

    travel_logger.info('================================\n%s get_quote_dict:\n%s' % (max_trip_length, product_cd))
    family = search_data.get('family_type', '').strip() if policy_type == "F" else ""
    insureds = [mem.strip() for mem in FAMILY_MEMBERS[family].split('+')] if policy_type == "F" else ['SELF']
    travel_logger.info('================================\n%s get_quote_dict:\n%s' % (max_trip_length, product_cd))
    data_dict_quote = {
        'PremiumCal_RequestObj': {
            'RequestHeader': {
                'SourceId': AGENT_ID,
                'MessageId': message_id,
                'token': TOKEN_ID,
                'Password': PASSWORD,
            },
            'PremiumRequest':
            {
                'TripType': 'R',  # trip_type, 'S' is for oneway trip
                'RiskStartDate':  start_date,
                'RiskEndDate': end_date,
                'ProductCode': product_cd,
                'PlanCode': multi_plan if max_trip_length else plan,
                'PlanDays': trip_duration,
                'Geographical': geographical_coverage,
                'VisaType': visa_type,
                'Age': age,
                'Discount': '',
                'FamilyID': family,
                
                #  'TripType': trip_type,
                #  'RiskStartDate':   start_date,
                #  'RiskEndDate': '' if trip_type == "S" else end_date,
                #  'ProductCode': product_cd,
                #  'PlanCode': plan if trip_type=="S" else multi_plan,
                #  'PlanDays': trip_duration,
                #  'Geographical': geographical_coverage,
                #  'VisaType': visa_type,
                # 'Age': age,
                # 'Discount':'',
                # 'FamilyID': family,
            },
        },
    }
    travel_logger.info('================================\n%s DATA DICT QUOTE' % data_dict_quote)
    return data_dict_quote

    
def get_proposal_dict(transaction):
    insured_details = transaction.insured_details_json
    search_data = transaction.extra['search_data']
    trip_type = search_data['trip_type']
    max_trip_length = search_data["max_trip_length"] if trip_type == "R" else ""
    plan = search_data['plan']
    multi_plan = plan + '30' if max_trip_length == "30" else plan + '60'
    trip_duration = max_trip_length if trip_type == "R" else search_data['trip_duration']
    policy_type = search_data['policy_type']

    from_date = search_data['from_date']
    start_date = datetime.datetime.strptime(from_date, '%d-%b-%Y').strftime('%d-%m-%Y')

    travel_logger.info(
        '================================\n%s get_proposal_dict:\n%s'
        % (max_trip_length, start_date)
    )

    to_date = search_data['to_date']
    
    if search_data['trip_type'] == 'R':
        end_date = (datetime.datetime.strptime(start_date, '%d-%m-%Y') + relativedelta(days=365)).strftime('%d-%m-%Y')
    else:
        end_date = datetime.datetime.strptime(to_date, '%d-%b-%Y').strftime('%d-%m-%Y')

    travel_logger.info(
        '================================\n%s get_proposal_dict end --date:\n%s'
        % (max_trip_length, end_date)
    )

    date_of_birth = search_data['DOB']
    age = relativedelta(
        datetime.datetime.today(),
        datetime.datetime.strptime(date_of_birth, travel_settings.DATE_FORMAT).date()
    ).years
    
    family = search_data.get('family_type', '').strip() if policy_type == "F" else ""
    message_id = str(uuid.uuid4())[:24]

    member_details = []
    if (trip_type == "S") and (policy_type == "I" or policy_type == "S"):
        product_cd = "31001"
    elif trip_type == "S" and policy_type == "F":
        product_cd = "31002"
    else:
        product_cd = "31005"
    c = 1
    for mem in insured_details['members']:
        member = {}
        
        if mem['relationship'].upper() not in ['SPOUSE', 'SELF']:
            relationship = 'CHILD' + str(c)
            c = c+1
        else:
            relationship = mem['relationship']

        member['Title'] = ''  # TO-DO
        member['FirstName'] = mem['first_name']
        member['LastName'] = mem['last_name']
        member['Gender'] = mem['gender']
        member['DateOfBirth'] = mem['date_of_birth']
        member['Relation'] = relationship
        member['PassportNo'] = mem['passport_no']
        member['Nationality'] = 'INDIAN'
        member['Nominee'] = {
            'NomineeName': mem['nominee_name'],
            'RelationWithInsured': mem['nominee_relation']
        }
        member_details.append({'InsuredDetails': member})

    member_details.append({'OfficeCode': '110103'})  # '900001'
    member_details.append({'TransactionDate': datetime.datetime.now().strftime('%d-%m-%Y')})
    member_details.append({'ProductCode': product_cd})
    member_details.append({'TripType': 'R'})
    member_details.append({'VisaType': 'N'})
    member_details.append({'PlanDays':  trip_duration})
    member_details.append({'RiskStartDate': start_date})
    member_details.append({'RiskEndDate':   end_date})
    member_details.append({'InitiativeCD': '3000'})  # '4000'
    member_details.append({
        'PlanDetails': {
            'PlanCode': multi_plan if max_trip_length else plan,
            'Geographical': search_data['geographical_coverage'],
            'FamilyID': family,
            'Discount': '',
        },
        'AddressDetails': {
            'Address1': insured_details['address']['address_line_1'],
            'Address2': insured_details['address']['address_line_2'],
            'Address3': insured_details['address']['address_line_3'],
            'City': insured_details['address']['city'],
            'Pincode': insured_details['address']['pincode'],
            'ContactNo1': insured_details['members'][0]['mobile'],
            'ContactNo2': '',
            'EmailId': insured_details['members'][0]['email'],
        },
    })
    
    data_dict_proposal = {
        'ProposalRequestObj': {
            'RequestHeader': {
                'SourceId': AGENT_ID,  # '80171360',
                'MessageId': message_id,
                'token': TOKEN_ID,
                'Password': PASSWORD,
            },
            'ProposalDetails': member_details,
        },
    }
    travel_logger.info('================================\n%s DATA DICT QUOTE' % data_dict_proposal)
    return data_dict_proposal

    
def get_quote_request_response(activity):
    data = activity.search_data
    travel_logger.info('================================\n%s:\n' % (data))
    request_type = 'Quote'
    XmlData = get_quote_dict(data)
    travel_logger.info('================================\n%s APOLLO REQUEST:\n' % (XmlData))
    Session = etree.Element('PremiumRequest')
    session_etree = prod_utils.dict_to_etree(Session, XmlData)
    dict_string = etree.tostring(session_etree.getchildren()[0])
    dict_string = dict_string.replace('<', '<pol:').replace("<pol:/", '</pol:')
    request = '<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">'\
              '<Body><PremiumCalculator xmlns="http://tempuri.org/">'\
              '<PremiumRequest><![CDATA[%s]]></PremiumRequest></PremiumCalculator></Body></Envelope>' % dict_string

    travel_logger.info('================================\n%s APOLLO REQUEST:\n%s' % (request_type.upper(), request))
    http = httplib2.Http()
    headers = {"Content-type": "text/xml", 'SOAPAction': 'http://tempuri.org/ITravelPremiumCalculatorService/PremiumCalculator'}
    travel_logger.info('================================\n%s APOLLO REQUEST:\n%s' % (request_type.upper(), request))
    response_header, response_content = http.request(REQUEST_URL, method='POST', body=request, headers=headers)
    travel_logger.info('================================\n%s APOLLO RESPONSE:\n%s' % (request_type.upper(), response_content))

    return response_content

    
def get_proposal_request_response(transaction):
    request_type = 'Proposal'
    XmlData = get_proposal_dict(transaction)
    Session = etree.Element('ProspectRequest')
    session_etree = prod_utils.dict_to_etree(Session, XmlData)
    dict_string = etree.tostring(session_etree.getchildren()[0])
    dict_string = dict_string.replace('<', '<pol:').replace("<pol:/", '</pol:')
    request = '<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">'\
              '<Body><ProspectCreation xmlns="http://tempuri.org/"><ProspectRequest>'\
              '<![CDATA[%s]]></ProspectRequest></ProspectCreation></Body></Envelope>' % dict_string
    http = httplib2.Http()
    headers = {"Content-type": "text/xml", 'SOAPAction': 'http://tempuri.org/ITravelProposalCaptureService/ProspectCreation'}
    travel_logger.info('================================\n%s APOLLO REQUEST:\n%s' % (request_type.upper(), request))
    response_header, response_content = http.request(PROPOSAL_URL, method='POST', body=request, headers=headers)
    travel_logger.info('================================\n%s APOLLO RESPONSE:\n%s' % (request_type.upper(), response_content))
    return response_content


def get_quote(activity):
    response_content = get_quote_request_response(activity)
    data = activity.search_data
    plan = data['plan']
    sum_assured = PLAN_CODES[plan]
    premium = response_content[response_content.find("PREMIUM_GROSS&gt;"):response_content.find("&lt;/PREMIUM_GROSS")].replace("PREMIUM_GROSS&gt;","")
    plan_lst = []
    plandict = {}
    planname = plan
    plandict['plan_type'] = planname
    plandict['sum_assured'] = sum_assured
    plandict['premium'] = premium
    plan_lst.append(plandict)
    return plan_lst, None

    
def post_transaction(transaction):
    resp_content = get_proposal_request_response(transaction)
    search_data = transaction.extra['search_data']
    trip_type = search_data['trip_type']
    policy_type = search_data['policy_type']
    if (trip_type == "S") and (policy_type == "I" or policy_type == "S"):
        product_cd = "31001"
    elif trip_type == "S" and policy_type == "F":
        product_cd = "31002"
    else:
        product_cd = "31005"
    proposal_id = resp_content[resp_content.find('ProposalNo&gt;'):resp_content.find('&lt;/ProposalNo')].replace('ProposalNo&gt;', '')
    gross_premium = resp_content[resp_content.find("PREMIUM_GROSS&gt;"):resp_content.find("&lt;/PREMIUM_GROSS")].replace("PREMIUM_GROSS&gt;","")
    client = Client(PAYMENT_WSDL_URL)
    pg = client.factory.create('ns2:TravelPaymentGatewayServiceRequest')
    pg.TravelPGDetail.AgentId = AGENT_ID
    pg.TravelPGDetail.Mobile = transaction.insured_details_json['members'][0]['mobile']
    pg.TravelPGDetail.Email = transaction.insured_details_json['members'][0]['email']
    pg.TravelPGDetail.Password = PASSWORD
    pg.TravelPGDetail.ProductName = PLAN_NAMES[product_cd]
    pg.TravelPGDetail.ProposalId = proposal_id
    pg.TravelPGDetail.GrossPremium = gross_premium
    pg.TravelPGDetail.PayerName = (
        transaction.insured_details_json['members'][0]['first_name'] + ' ' +
        transaction.insured_details_json['members'][0]['last_name']
    )
    pg.TravelPGDetail.ProposerName = (
        transaction.insured_details_json['members'][0]['first_name'] + ' ' +
        transaction.insured_details_json['members'][0]['last_name']
    )
    pg.TravelPGDetail.IPAddress = ''
    pg.TravelPGDetail.PaymentId = ''
    pg.TravelPGDetail.ReturnUrl = (
            '%s/travel-insurance/apollo-munich/transaction/%s/response/'
            % (settings.SITE_URL, transaction.transaction_id)
        )
    pg.TravelPGDetail.MerchantRefNo = 'MER007654'
    pg.TravelPGDetail.PaymentUrl = PAYMENT_URL
    pg.TravelPGDetail.Token = TOKEN_ID
    pg.TravelPGDetail.UDF1 = ''
    pg.TravelPGDetail.UDF2 = ''
    pg.TravelPGDetail.UDF3 = ''
    pg.TravelPGDetail.UDF4 = ''
    pg.TravelPGDetail.UDF5 = ''
    
    cp = client.service.TravelPaymentDetails
    resp = cp(pg)

    travel_logger.info(
        '\nAPOLLO MUNICH TRANSACTION: %s \n PROPOSAL REQUEST SENT:\n %s \n'
        % (transaction.transaction_id, client.last_sent())
    )
    travel_logger.info(
        '\nAPOLLO MUNICH TRANSACTION: %s \n PROPOSAL RESPONSE RECEIVED:\n %s \n'
        % (transaction.transaction_id, client.last_received())
    )
    
    payment_url_new = resp.PaymentUrl
    transaction.reference_id = proposal_id
    transaction.save()

    template_name = 'health_product/post_transaction_data.html'
    is_redirect = True

    return is_redirect, payment_url_new, template_name, client.last_sent(), client.last_sent(), client.last_received()

    
def post_payment(payment_response, transaction):
    '''
    {
        u'MerchantRefNo': u'MER007654',
        u'ProposalId': u'O00160869',
        u'Amount': u'18634',
        u'PolicyIssuanceErrorDesc': u'NA',
        u'UDF1': u'',
        u'UDF3': u'',
        u'UDF2': u'',
        u'UDF5': u'',
        u'UDF4': u'',
        u'ResponseCode': u'2',
        u'PaymentId': u'10004206',
        u'TransactionId': u'0000000',
        u'Message': u'User cancelled payment',
        u'PolicyIssuanceErrorCode': u'NA'
    }
    '''
    data = {}
    proposal_id = payment_response['ProposalId']
    amount = payment_response['Amount']
    payment_id = payment_response['PaymentId']
    errorcode = payment_response['PolicyIssuanceErrorCode']
    message = payment_response['Message']
    transaction = Transaction.objects.get(reference_id=proposal_id)
    transaction.payment_response = payment_response

    travel_logger.info(
        '\nAPOLLO MUNICH TRANSACTION: %s\n PAYMENT RESPONSE RECEIVED:\n %s\n'
        % (transaction.transaction_id, payment_response)
    )

    if payment_response['PolicyIssuanceErrorDesc'] == 'Successful':
        transaction.payment_done = True
        transaction.payment_on = datetime.datetime.today()
        transaction.status = transaction.COMPLETED
        prod_utils.send_success_mail(transaction, 'payment')
        transaction.extra['error_message'] = ''
        print "successful paymen t"
    else:
        transaction.status = transaction.FAILED
        transaction.extra['error_message'] = message
        prod_utils.send_failure_mail(transaction, 'Fail')
        data = {
            'stack_trace': "",
            'error_message': message,
            'error_response': payment_response,
        }
    transaction.save()

    travel_logger.info("ALL IS WELL;;;;;; %s" % transaction.status)
    return transaction, transaction.status == transaction.COMPLETED, data
