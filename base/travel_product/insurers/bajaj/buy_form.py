import json
from travel_product.insurers import common_preset

OCCUPATIONS = common_preset.OCCUPATIONS;

STATES = common_preset.STATES;

CITIES = common_preset.CITIES;

RELATIONS = [
    {'key': 'Spouse', 'value': 'Spouse'},
    {'key': 'Son', 'value': 'Son'},
    {'key': 'Daughter', 'value': 'Daughter'},
]

SALUTATIONS = [
    {'key':'Mr.','value':'Mr'},
    {'key':'Mrs.','value':'Mrs'},
    {'key':'Ms.','value':'Miss'},
]


def get_form(request, transaction):
    members = transaction.plan_details['members']

    return {
        'num_insured': len(members),
        'members_list': members,
        'occupations_list': OCCUPATIONS,
        'relations_list': RELATIONS,
        'salutations_list': SALUTATIONS,
        'states_list': STATES,
        'cities_list': CITIES,
    }
