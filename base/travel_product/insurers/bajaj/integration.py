import traceback
from suds.client import Client
from suds.wsse import *
from utils import travel_logger
import datetime
from django.conf import settings
import json
import datetime
from dateutil.relativedelta import relativedelta
import logging
logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.transport.http').setLevel(logging.DEBUG)

# =============== LIVE CREDENTIALS ===============
WSDL_URL = 'http://webservices.bajajallianz.com/BjazTravelWebservice/BjazTravelWebservicePort?WSDL'
USERNAME = 'webservice@coverfox.com'
PASSWORD = 'coverfox.bajaj1'
IMD_CODE = '10040010'
LOCATION_CODE = '9906'
REDIRECTION_URL = ''
PAYMENT_URL = 'http://web7.bajajallianz.com/BagicOnline/travel/new_cc_payment.jsp?'\
                'requestId=%s&Username=webservice@coverfox.com'
## =============== TEST CREDENTIALS ===============
#WSDL_URL = 'http://webservices.bajajallianz.com:8001/BjazTravelWebservice/BjazTravelWebservicePort?WSDL'
#USERNAME = 'webservice@coverfox.com'
#PASSWORD = 'newpas12'
#IMD_CODE = '10038791'
#LOCATION_CODE = '1901'
#REDIRECTION_URL = ''
#PAYMENT_URL = 'http://extestapp.bajajallianz.com/BagicOnline_travel/travel/new_cc_payment.jsp?'\
#                'requestId=%s&Username=webservice@coverfox.com'

SALUTATUION_CHOICE = {
    'm': 'Mr',
    'f': 'Mrs'

}

GENDER_CHOICE = {
    'm': 'MALE',
    'f': 'FEMALE'
    }

PLANS = {
    'Individual': {
        'Travel Elite Silver': 'US $50000',
        'Travel Elite Gold': 'US $200000',
        'Travel Elite Platinum': 'US $500000',
        'Travel Care': 'US $50000',
        'Travel Secure': 'US $200000',
        'Travel Value': 'US $500000'},

    'Student': {
        'Student Elite Standard': 'US $50000',
        'Student Elite Silver': 'US $100000',
        'Student Elite Gold': 'US $200000',
        'Student Companion Standard': 'US $50000',
        'Student Companion Silver': 'US $100000',
        'Student Companion Gold': 'US $200000'},

    'Asia': {
        'Travel Asia Elite Flair': 'US $15000',
        'Travel Asia Elite Supreme': 'US $25000'},

    'Family': {
        'Travel Elite Family  (Family Floater)': 'US $50000',
        'Travel Family (Family Floater)': 'US $50000'},

    'Senior Citizen': {
        'Travel Age Elite Silver': 'US $50000',
        'Travel Age Elite Gold': 'US $200000',
        'Travel Age Elite Platinum': 'US $500000',
        'Travel Age': 'US $50000'},

    'Corporate': {
        'Corporate Elite Lite': 'US $250000',
        'Corporate Elite Plus': 'US $500000'},

    'Swadesh': {'E-Travel', 'SWADESHPLAN-A', 'SWADESHPLAN-B', 'SWADESHPLAN-C', 'SWADESHPLAN-D'}
}

REGION = {
    'Worldwide Including USA and CANADA': 'IncludingUSA',
    'Worldwide Excluding USA and CANADA': 'ExcludingUSA',
    'Asia only excluding Japan': 'ExcludingJapan'
}

ALL_PLANS = {
    'Travel Elite Silver',
    'Travel Elite Gold',
    'Travel Elite Platinum',
    'Travel Care',
    'Travel Secure',
    'Travel Value',

    'Travel Age Elite Silver',
    'Travel Age Elite Gold',
    'Travel Age Elite Platinum',
    'Travel Age',

    'Corporate Elite Lite',
    'Corporate Elite Plus',

    'Travel Asia Elite Flair',
    'Travel Asia Elite Supreme',

    'Travel Elite Family  (Family Floater)',
    'Travel Family (Family Floater)',

    'Student Elite Silver',
    'Student Elite Gold',
    'Student Elite Standard',
    'Student Companion Gold',
    'Student Companion Silver',
    'Student Companion Standard'

    'E-Travel',
    'SWADESHPLAN-A',
    'SWADESHPLAN-B',
    'SWADESHPLAN-C',
    'SWADESHPLAN-D'
}


def get_product_type(search_data):
    if 60 < calculate_age(search_data['dob']) <= 70:
        product_type = 'Senior Citizen'
    else:
        if search_data['traveller_type'] == 'Business Multi-Trip':
            product_type = 'Corporate'
        elif search_data['region'] not in ['ASIA ONLY EXCLUDING JAPAN']:
            product_type = search_data['traveller_type']
        elif search_data['region'] == 'ASIA ONLY EXCLUDING JAPAN':
            product_type = 'Asia'
        else:
            product_type = search_data['traveller_type']
    return product_type


def home_required(transaction):
    home_required = False
    search_data = transaction.extra['search_data']
    plan_type = transaction.plan_details['plan_type'].strip()
    product_type = get_product_type(search_data)
    if product_type == 'Individual':
        home_required = (plan_type in ['Silver', 'Gold', 'Platinum'])
    elif product_type == 'Family':
        home_required = True
    elif search_data['traveller_type'] == 'Business Multi-Trip':
        home_required = (plan_type in ['Plus', 'Elite'])
    return home_required


def get_quote(activity):

    search_data = activity.search_data
    product_type = get_product_type(search_data)

    plans = PLANS[product_type]
    is_family = 'Y' if search_data['traveller_type'] == 'Family' else 'N'
    # i = 1

    plans_premium = []

    for pl, sumIn in plans.iteritems():

        client = Client(WSDL_URL)
        client.set_options(port='BjazTravelWebservicePort')

        pTravelPremiumIn_inout = client.factory.create('ns0:WeoTrvPremiumInParamUser')
        pTravelPremiumIn_inout.ptravelplan = pl
        pTravelPremiumIn_inout.pareaplan = search_data['region']
        pTravelPremiumIn_inout.pdateOfBirth = search_data['dob']
        pTravelPremiumIn_inout.pfromDate = search_data['from_date']
        pTravelPremiumIn_inout.ptoDate = search_data['to_date']
        pTravelPremiumIn_inout.ploading = 0
        pTravelPremiumIn_inout.pdiscount = 0
        pTravelPremiumIn_inout.pspDiscount = 0

        pfamilyParamList_inout = client.factory.create('ns0:WeoTrvFamilyParamInUserArray')
        fam_list = []
        if is_family == 'Y':
            # Child
            if search_data['no_of_child'] > 0:
                for i in range(int(search_data['no_of_child'])):
                    index = i+1
                    famparlistObj = client.factory.create('ns0:WeoTrvFamilyParamInUser')
                    famparlistObj.pvname = search_data['child'+str(index)+'_name']
                    famparlistObj.pvage = calculate_age(search_data['child'+str(index)+'_dob'])
                    famparlistObj.pvrelation = 'Child'
                    famparlistObj.pvsex = search_data['child'+str(index)+'_gender']
                    famparlistObj.pvdob = search_data['child'+str(index)+'_dob']
                    famparlistObj.pvassignee = search_data['child'+str(index)+'_assignee_name']
                    famparlistObj.pvpartnerId = 0
                    famparlistObj.pvpassportNo = ''
                    fam_list.append(famparlistObj)

            # Adult
            if search_data['no_of_adult'] > 0:
                for i in range(int(search_data['no_of_adult'])):
                    index = i+1
                    famparlistObj = client.factory.create('ns0:WeoTrvFamilyParamInUser')
                    famparlistObj.pvname = search_data['adult'+str(index)+'_name']
                    famparlistObj.pvage = calculate_age(search_data['adult'+str(index)+'_dob'])
                    famparlistObj.pvrelation = search_data['adult'+str(index)+'_relation']
                    famparlistObj.pvsex = search_data['adult'+str(index)+'_gender']
                    famparlistObj.pvdob = search_data['adult'+str(index)+'_dob']
                    famparlistObj.pvassignee = search_data['adult'+str(index)+'_assignee_name']
                    famparlistObj.pvpartnerId = 0
                    famparlistObj.pvpassportNo = ''
                    fam_list.append(famparlistObj)
        else:
            famparlistObj = client.factory.create('ns0:WeoTrvFamilyParamInUser')
            fam_list.append(famparlistObj)

        pfamilyParamList_inout = {'WeoTrvFamilyParamInUser': fam_list}

        pTravelPremiumOut_out = client.factory.create('ns0:WeoTrvPremiumOutParamUser')
        pTravelPremiumOut_out.ptotalPremium = 0
        pTravelPremiumOut_out.pbasePremium = 0
        pTravelPremiumOut_out.pserviceTax = 0
        pTravelPremiumOut_out.ploadingAmt = 0
        pTravelPremiumOut_out.pdiscountAmt = 0
        pTravelPremiumOut_out.pspDiscountAmt = 0
        pTravelPremiumOut_out.peduCessAmt = 0
        pTravelPremiumOut_out.pfullTermPremium = 0
        pTravelPremiumOut_out.pextraCol = 0
        pTravelPremiumOut_out.pextraCol1 = 0
        pTravelPremiumOut_out.pextraCol2 = 0

        pError_out = client.factory.create('ns0:WeoTygeErrorMessageUserArray')
        errorObj = client.factory.create('ns0:WeoTygeErrorMessageUser')
        errorObj.errNumber = 0
        errorObj.parName = ''
        errorObj.property = ''
        errorObj.errText = ''
        errorObj.parIndex = ''
        errorObj.errLevel = ''
        pError_out = {'WeoTygeErrorMessageUser': [errorObj]}

        resp = client.service.bjazWebservicePremium(
            USERNAME,
            PASSWORD,
            pTravelPremiumIn_inout,
            is_family,                        # Family flag
            pfamilyParamList_inout,
            'N',
            pTravelPremiumOut_out,
            pError_out,
            0
        )

        if not resp.pTravelPremiumOut_out:
            raise Exception('Invalid Data')

        premium = {'plan_type': pl, 'sum_assured': sumIn, 'premium': resp.pTravelPremiumOut_out.ptotalPremium}
        plans_premium.append(premium)

    return plans_premium, {}


def pre_transaction(transaction):
    search_data = transaction.extra['search_data']
    product_type = get_product_type(search_data)
    plan_type = transaction.plan_details['plan_type']
    if product_type == 'Family':
        members = ['applicant']
        n_adults = int(search_data['no_of_adult'])
        n_children = int(search_data['no_of_child'])
        for i in range(1, n_adults+1):
            members.append('adult%i' % i)
        for i in range(1, n_children+1):
            members.append('child%i' % i)
    else:
        members = ['applicant']

    transaction.plan_details['product_type'] = product_type
    transaction.plan_details['plan'] = '%s - %s' % (product_type, plan_type)
    transaction.plan_details['home_required'] = home_required(transaction)
    transaction.plan_details['members'] = members
    return None


def finalize_transaction(transaction):
    insured_details = transaction.insured_details_json
    member = insured_details['members'][0]
    transaction.extra['search_data']['dob'] = datetime.datetime.strptime(member['date_of_birth'], '%d-%m-%Y').strftime('%d-%b-%Y')


def get_mapped_insured_details_json(transaction):
    mdata = {}
    insured_details = transaction.insured_details_json

    child_counter = 1
    adult_counter = 1

    for member in insured_details['members']:
        tag = ""
        relation = member['relationship']

        if relation:
            relation = relation.lower()
            if relation == 'self':
                tag = 'applicant'
            elif relation not in ['son', 'daughter']:
                tag = 'adult'+str(adult_counter)
                adult_counter += 1
            else:
                tag = 'child'+str(child_counter)
            mdata[tag+'Relation'] = member['relationship']
            mdata[tag+'AssigneeName'] = member['assignee_name']
            mdata[tag+'DOB'] = datetime.datetime.strptime(member['date_of_birth'], '%d-%m-%Y').strftime('%d-%b-%Y')
            mdata[tag+'FName'] = member['first_name']
            mdata[tag+'LName'] = member['last_name']
            mdata[tag+'MName'] = ""
            mdata[tag+'MaritalStatus'] = member.get('married', "")
            mdata[tag+'Passport'] = member['passport_no']
            mdata[tag+'Salute'] = SALUTATUION_CHOICE[member['gender']]
            mdata[tag+'gender'] = GENDER_CHOICE[member['gender']]
            mdata[tag+'bool_disease'] = member['ped']

    mdata['communication_email'] = insured_details['members'][0]['email']
    mdata['communication_mobile'] = insured_details['members'][0]['mobile']
    mdata['communication_Building'] = insured_details['address']['address_line_1']
    mdata['communication_StreetName'] = insured_details['address']['address_line_2']
    mdata['communication_city'] = insured_details['address']['city']
    mdata['communication_state'] = insured_details['address']['state']
    mdata['communicationPincode'] = insured_details['address']['pincode']
    mdata['communication_country'] = 'India'
    mdata['communication_nationality'] = 'Indian'
    return mdata


def set_travel_policy_details(transaction):

    search_data = transaction.extra['search_data']
    plan_type = transaction.plan_details['plan_type']
    product_type = transaction.plan_details['product_type']
    insured_details = get_mapped_insured_details_json(transaction)

    client = Client(WSDL_URL)
    client.set_options(port='BjazTravelWebservicePort')

    # Mandatory
    pTrvPartnerDtls_inout = client.factory.create('ns0:BjazWsCpDtlsObjUser')
    pTrvPartnerDtls_inout.userid = USERNAME
    pTrvPartnerDtls_inout.partnerType = 'P'
    pTrvPartnerDtls_inout.title = insured_details['applicantSalute']
    pTrvPartnerDtls_inout.firstname = insured_details['applicantFName']
    pTrvPartnerDtls_inout.middlename = insured_details.get('applicantMName', '')
    pTrvPartnerDtls_inout.lastname = insured_details['applicantLName']
    pTrvPartnerDtls_inout.dob = insured_details['applicantDOB']
    pTrvPartnerDtls_inout.sex = insured_details['applicantgender']
    pTrvPartnerDtls_inout.maritalstatus = insured_details['applicantMaritalStatus']
    pTrvPartnerDtls_inout.assigneeName = insured_details['applicantAssigneeName']
    pTrvPartnerDtls_inout.passportno = insured_details['applicantPassport']
    pTrvPartnerDtls_inout.building = insured_details['communication_Building']
    pTrvPartnerDtls_inout.streetname = insured_details['communication_StreetName']
    pTrvPartnerDtls_inout.pincode = insured_details['communicationPincode']
    pTrvPartnerDtls_inout.country = insured_details['communication_country']
    pTrvPartnerDtls_inout.state = insured_details['communication_state']
    pTrvPartnerDtls_inout.city = insured_details['communication_city']
    pTrvPartnerDtls_inout.email = insured_details['communication_email']
    pTrvPartnerDtls_inout.mobileNo = insured_details['communication_mobile']
    pTrvPartnerDtls_inout.nationality = insured_details['communication_nationality']
    # Optional
    pTrvPartnerDtls_inout.telephone = ''
    pTrvPartnerDtls_inout.locationcode = ''
    pTrvPartnerDtls_inout.companyname = ''
    pTrvPartnerDtls_inout.occupation = ''
    pTrvPartnerDtls_inout.fax = ''
    pTrvPartnerDtls_inout.institutionname = ''
    pTrvPartnerDtls_inout.partnerid = ''
    pTrvPartnerDtls_inout.empstatus = ''
    pTrvPartnerDtls_inout.regno = ''
    pTrvPartnerDtls_inout.requestid = ''
    pTrvPartnerDtls_inout.extCol1 = ''
    pTrvPartnerDtls_inout.extCol2 = ''
    pTrvPartnerDtls_inout.extCol3 = ''
    pTrvPartnerDtls_inout.extCol4 = ''
    pTrvPartnerDtls_inout.extCol5 = ''
    pTrvPartnerDtls_inout.extCol6 = ''
    pTrvPartnerDtls_inout.extCol7 = ''
    pTrvPartnerDtls_inout.extCol8 = ''
    pTrvPartnerDtls_inout.extCol9 = ''
    pTrvPartnerDtls_inout.extCol10 = ''

    # Mandatory
    pTrvPolDtls_inout = client.factory.create('ns0:BjazTrvPolicyDtlsObjUser')
    pTrvPolDtls_inout.userid = USERNAME
    pTrvPolDtls_inout.paymentMode = 'CC'
    pTrvPolDtls_inout.familyFlag = 'Y' if search_data['traveller_type'] == 'Family' else 'N'
    pTrvPolDtls_inout.travelplan = plan_type
    pTrvPolDtls_inout.areaplan = search_data['region']
    pTrvPolDtls_inout.dob = search_data['dob']
    pTrvPolDtls_inout.fromDate = datetime.datetime.strftime(transaction.activity.policy_start_date, "%d-%b-%Y")
    pTrvPolDtls_inout.toDate = datetime.datetime.strftime(transaction.activity.policy_end_date, "%d-%b-%Y")
    pTrvPolDtls_inout.systemip = '10.1.7.19'
    pTrvPolDtls_inout.returnpath = (
        '%s/travel-insurance/insurer/bajaj/transaction/%s/response/?'
        % (settings.SITE_URL, transaction.transaction_id)
    )
    pTrvPolDtls_inout.assigneeName = insured_details['applicantAssigneeName']
    # Optional
    pTrvPolDtls_inout.policyNo = ''
    pTrvPolDtls_inout.masterpolicyno = ''
    pTrvPolDtls_inout.productCode = ''
    pTrvPolDtls_inout.loading = ''
    pTrvPolDtls_inout.destination = ''
    pTrvPolDtls_inout.discount = ''
    pTrvPolDtls_inout.spCondition = ''
    pTrvPolDtls_inout.requestid = ''
    pTrvPolDtls_inout.finalPremium = ''
    pTrvPolDtls_inout.extraCol1 = ''
    pTrvPolDtls_inout.extraCol2 = ''
    pTrvPolDtls_inout.extraCol3 = ''
    pTrvPolDtls_inout.extraCol4 = ''
    pTrvPolDtls_inout.extraCol5 = ''

    pFamilyInfoList_inout = client.factory.create('ns0:WeoTrvFamilyParamInUserArray')

    familyObjList = []

    if(len(transaction.insured_details_json['members']) == 1):
        familyObjList.append(client.factory.create('ns0:WeoTrvFamilyParamInUser'))

    for i in range(1, len(transaction.insured_details_json['members'])):
        m = transaction.insured_details_json['members'][i]
        famObj = client.factory.create('ns0:WeoTrvFamilyParamInUser')
        relation = m['relationship']
        famObj.pvrelation = 'Child' if relation.lower() in ['son', 'daughter'] else relation
        famObj.pvname = "%s %s" % (m['first_name'], m['last_name'])
        famObj.pvage = relativedelta(datetime.datetime.today(), datetime.datetime.strptime(m['date_of_birth'], '%d-%m-%Y')).years
        famObj.pvdob = datetime.datetime.strptime(m['date_of_birth'], '%d-%m-%Y').strftime('%d-%b-%Y')
        famObj.pvassignee = m['assignee_name']
        famObj.pvpassportNo = m['passport_no']
        famObj.pvpartnerId = ''
        famObj.pvsex = GENDER_CHOICE[m['gender']]
        familyObjList.append(famObj)
    pFamilyInfoList_inout = {'WeoTrvFamilyParamInUser': familyObjList}

    pError_out = client.factory.create('ns0:WeoTygeErrorMessageUserArray')
    errorObj = client.factory.create('ns0:WeoTygeErrorMessageUser')
    errorObj.errNumber = 0
    errorObj.parName = ''
    errorObj.property = ''
    errorObj.errText = ''
    errorObj.parIndex = ''
    errorObj.errLevel = ''
    pError_out = {'WeoTygeErrorMessageUser': [errorObj]}


    proposal_response = client.service.travelRequestId(
        USERNAME,
        PASSWORD,
        pTrvPartnerDtls_inout,
        pTrvPolDtls_inout,
        pFamilyInfoList_inout,
        0,
        pError_out,
        0
    )
    proposal_request = client.last_sent().str()

    travel_logger.info(
        '\nBAJAJ TRANSACTION: %s \n PROPOSAL REQUEST SENT:\n %s \n'
        % (transaction.transaction_id, proposal_request)
    )
    travel_logger.info(
        '\nBAJAJ TRANSACTION: %s \n PROPOSAL RESPONSE RECEIVED:\n%s \n'
        % (transaction.transaction_id, proposal_response)
    )

    return proposal_request, proposal_response


def post_transaction(transaction):
    template_name = None
    is_redirect = True
    proposal_request = None
    proposal_response = None
    try:
        proposal_request, proposal_response = set_travel_policy_details(transaction)

        if(proposal_response.pErrorCode_out):
            raise Exception(proposal_response.pError_out.WeoTygeErrorMessageUser[0].errText)
        request_id = int(proposal_response.pRequestid_out)
        resp_url = '%s/travel-insurance/bajaj/transaction/%s/response/?' % (settings.SITE_URL, transaction.transaction_id)
        data = {'Message': str(request_id) + '|' + USERNAME + '|' + resp_url}
        transaction.payment_request = json.dumps(data)
        transaction.status = transaction.PENDING
        transaction.save()
        travel_logger.info('===================\BAJAJ TRANSACTION: %s\nPAYMENT REQUEST\n%s\n' % (transaction.transaction_id, data))
        redirect_url = PAYMENT_URL % (request_id)
    except Exception as e:
        travel_logger.info(
            '========\nBAJAJ TRANSACTION: %s\nTRANSACTION EXCEPTION: \n%s\n'
            % (transaction.transaction_id, traceback.format_exc())
        )
        data = {
            'stack_trace': traceback.format_exc(),
            'error_message': e.message,
            'error_response': str(proposal_response),
        }
        redirect_url = "/travel-insurance/transaction/%s/failure/" % (transaction.transaction_id)
    return is_redirect, redirect_url, template_name, data, proposal_request, proposal_response


def post_payment(payment_response, transaction):
    """
    http://demo.coverfox.com/health-plan/4/transaction/abe2b262af4511e3bd72f23c91ae8aaa/response/?policy_no=OG-14-1901-6001-00012806&request_id=16961&p_pay_status=Y
    Final Response : {u'p_pay_status': u'Y', u'policy_no': u'OG-14-1901-6001-00012806', u'request_id': u'16961'}
    """

    travel_logger.info(
        '========\nBAJAJ TRANSACTION: %s\nPAYMENT RESPONSE: \n%s\n'
        % (transaction.transaction_id, payment_response)
    )
    transaction.extra['payment_response'] = payment_response
    transaction.payment_response = payment_response

    post_data = {}
    if transaction.extra['payment_response']['p_pay_status'] == 'Y':
        transaction.policy_number = payment_response['policy_no']
        transaction.payment_done = True
        transaction.status = transaction.COMPLETED
        transaction.payment_on = datetime.datetime.today()
    else:
        transaction.status = transaction.FAILED
        post_data = {
            'stack_trace': "",
            'error_message': "",
            'error_response': payment_response,
        }
    transaction.save()
    return transaction, transaction.status == transaction.COMPLETED, post_data


def get_plans():
    data = get_processed_data_common()
    client = Client(WSDL_URL)
    client.set_options(port='BjazTravelWebservicePort')
    resp = client.service.bjazWebserviceTravelPlan(
            USERNAME,
            PASSWORD,
            0,
            0,
            data['pIntermediaryList_out'],
            data['pTravelList_out'],
            data['pError_out'],
            0
            )

    print_data(client, resp)


def print_data(client, resp):
    print "========================REQUEST (XML)========================"
    print client.last_sent()
    # print "========================RESPONSE (XML)========================"
    # print client.last_received()
    print "======================RESPONSE (OBJ)===================="
    print resp


def calculate_age(datestring):
    born = datetime.datetime.strptime(datestring, "%d-%b-%Y").date()
    today = born.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))
