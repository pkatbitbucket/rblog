import json
from travel_product.insurers import common_preset

OCCUPATIONS = common_preset.OCCUPATIONS;

STATES = common_preset.STATES;

CITIES = common_preset.CITIES;


RELATIONS = [
    {'key':"Self",'value':"Self"},
    {'key': 'Spouse', 'value': 'Spouse'},
    {'key': 'Son', 'value': 'Son'},
    {'key': 'Daughter', 'value': 'Daughter'},
]

NOMINEE_RELATIONS = [
    {'key': 'Spouse', 'value': 'Spouse'},
    {'key': 'Son', 'value': 'Dependent Son'},
    {'key': 'Daughter', 'value': 'Dependent Daughter'},
    {'key': 'Father', 'value': 'Father'},
    {'key': 'Mother', 'value': 'Mother'},
    {'key': 'Brother', 'value': 'Brother'},
    {'key': 'Sister', 'value': 'Sister'},
    {'key': 'Others', 'value': 'Others'},
]
SALUTATIONS = [
    {'key':'Mr.','value':'MR'},
    {'key':'Mrs.','value':'MRS'},
    {'key':'Ms.','value':'MS'},
]

GENDERS = [
        {'key':'Male','value':'M'},
        {'key':'Female','value':'F'},
        ]
def get_form(request, transaction):
    members = transaction.plan_details['members']
    return {
        'num_insured': len(members),
        'members_list': members,
        'occupations_list': OCCUPATIONS,
        'relations_list': RELATIONS,
        'nominee_relations_list' : NOMINEE_RELATIONS,
        'salutations_list': SALUTATIONS,
        'gender_list': GENDERS,
        'states_list': STATES,
        'cities_list': CITIES,
    }
