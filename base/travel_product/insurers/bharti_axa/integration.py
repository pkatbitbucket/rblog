import requests
import json
import os
import traceback
from django.conf import settings
from django.db.models import Q
from travel_product.models import Transaction
from travel_product import prod_utils
from travel_product import settings as travel_settings
from collections import OrderedDict
from pytz import timezone
from django.core.files.base import File
import datetime
from dateutil.relativedelta import relativedelta
import httplib2
from lxml import etree
from utils import travel_logger
from BeautifulSoup import BeautifulSoup

if settings.PRODUCTION:
    WSDL_URL = 'https://www.smartlinkxr.bharti-axagi.co.in/cordys/'\
               'WSDLGateway.wcp?service=http%3A%2F%2Fschemas.cordys.com%2Fgateway%2FProvider/' \
               'serve&organization=o%3DAXA-IN%2Ccn%3Dcordys%2Ccn%3DCordysBOP4PRD%2Co%3Daxa_sgp.axa-ap.intraxa'
    REQUEST_URL = 'https://www.smartlinkxr.bharti-axagi.co.in/cordys/'\
                  'com.eibus.web.soap.Gateway.wcp?organization=o=AXA-IN,cn=cordys,cn=CordysBOP4PRD,o=axa_sgp.axa-ap.intraxa'
    PAYMENT_URL = 'https://www.smartlinkxr.bharti-axagi.co.in/cordys/bagi/eSuite/B2C/payMe.aspx'
    HOST = 'https://www.smartlinkxr.bharti-axagi.co.in'
    PASSWORD = 'JY8VoC0Nl1HvXsZ6zl6wzoU3v485xjgfupxWx0PmCTU7iFsFcxNzhQ7jCNUtuWVRBk5j2lS0W5oydvMgUOVQ9g=='
else:
    WSDL_URL = 'http://www.smartlinkxruat.bharti-axagi.co.in/cordys/'\
               'WSDLGateway.wcp?service=http%3A%2F%2Fschemas.cordys.com%2Fgateway%2FProvider/'\
               'serve&organization=o%3DAXAIN%2Ccn%3Dcordys%2Ccn%3DCordysBOP4UAT%2Co%3Daxa_sgp.axa-ap.intraxa'
    REQUEST_URL = 'http://www.smartlinkxruat.bharti-axagi.co.in/cordys/'\
                  'com.eibus.web.soap.Gateway.wcp?organization=o=AXAIN,cn=cordys,cn=CordysBOP4UAT,o=axa_sgp.axa-ap.intraxa'
    PAYMENT_URL = 'https://www.smartlinkxruat.bharti-axagi.co.in/cordys/bagi/eSuite/B2C/payMe.aspx'
    HOST = 'http://www.smartlinkxruat.bharti-axagi.co.in'
    PASSWORD = 'AZg3Q1SktWKLz0Os/H0MlAxFfI75pjnpKjn9xrV9vimyyS7/5Ilil/ftcP5oHxC7IFYLVF0C3MAJcIznwrZvDA=='

PDF_URL = HOST+'/cordys'

FAMILY_CODES = {
    'S': 'Self',
    'SS': 'Self + Spouse',
    'SSC': 'Self + Spouse + Child1',
    'SS2C': 'Self + Spouse + Child1 + Child2',
    'SC': 'Self + Child1',
    'S2C': 'Self + Child1 + Child2',
}

SALUTATION_CHOICE = {
    'm': 'Mr.',
    'f': 'Ms.'
    }


def pre_transaction(transaction):
    search_data = transaction.extra['search_data']
    family_type = search_data['family_type']
    cover_type = search_data['policy_type']
    if cover_type == 'F':
        members = FAMILY_CODES[family_type].split('+')
    else:
        members = ['self']

    transaction.plan_details['plan'] = transaction.plan_details.pop('plan_type')
    transaction.plan_details['members'] = [mem.lower().strip() for mem in members]
    # transaction.plan_details['extra_data'] = json.loads(transaction.plan_details['extra_data'])

    activity_data = transaction.activity.cleaned_data
    transaction.data['insured_details_json'].update({
        'from_date': search_data['from_date'],
        'to_date': search_data['to_date'],
        'policy_type': search_data['policy_type'],
        'trip_duration': activity_data['duration'],
        'max_trip_length': search_data['max_trip_length'] if 'max_trip_length' in search_data else '',
        'sportsperson': search_data['sportsperson'],
        'pre_existing_illness': search_data['pre_existing_illness'],
        'dangerous_activity': search_data['dangerous_activity'],
        'Travelling_To': search_data['Travelling_To'],
        'family_type': search_data['family_type'],
        'policy_type': search_data['policy_type'],
        'trip_type': search_data['trip_type'],
    })
    return None


def get_xml_formated_data(transaction, tp_client_ref_no, plan_id=None, quote_no=None, order_no=None):
    search_data = transaction.extra['search_data']
    insured_details = transaction.insured_details_json
    from_date = search_data['from_date']
    to_date = search_data['to_date']

    date_format = '%a, %d %b %Y %H:%M:%S GMT'
    initTime = datetime.datetime.now(timezone('GMT'))
    policy_type = 'I/F' if search_data['policy_type'] in ['I', 'F'] else search_data['policy_type']
    cover_type = 'F' if search_data['policy_type'] == 'F' else 'I'
    client_type = 'Family' if cover_type == 'F' else 'Individual'
    geographical_coverage = '4' if search_data['Travelling_To'] == 'SCH' else search_data['Travelling_To']
    family_type = 'S' if search_data['policy_type'] in ['I', 'S'] else search_data['family_type']
    dep_date = datetime.datetime.strptime(from_date, '%d-%b-%Y').strftime('%Y-%m-%dT%H:%M:%S')
    trip_duration = search_data.get('trip_duration')
    max_trip_per_duration = search_data.get('max_trip_length')

    trip_type = search_data['trip_type']
    is_multitrip = (trip_type == 'A')

    if max_trip_per_duration and is_multitrip:
        dep_date = datetime.datetime.strptime(from_date, '%d-%b-%Y')
        return_date = (dep_date + relativedelta(days=365)).strftime('%Y-%m-%dT%H:%M:%S')
    else:
        return_date = datetime.datetime.strptime(to_date, '%d-%b-%Y').strftime('%Y-%m-%dT%H:%M:%S')

    if is_multitrip or search_data['policy_type'] == 'S':
        travel_type = 'NA'
    elif search_data['Travelling_To'] == 'SCH':
        travel_type = 'SCH'
    else:
        travel_type = 'NON_SCH'

    """
    'NA'if is_multitrip or data['policy_type']=='S' else 'NON_SCH' i'NA' if is_multitrip else data['travel_type']
    ## only nonschengen is available for now
    """
    dob_obj = datetime.datetime.strptime(search_data['DOB'], travel_settings.DATE_FORMAT).date()
    age = relativedelta(datetime.datetime.today(), dob_obj).years
    member_details = []

    for member in insured_details['members']:
        member_detail = {}
        member_detail['CurrentLocation'] = 'India'
        member_detail['FirstName'] = member['first_name']
        member_detail['Gender'] = member['gender']
        member_detail['HoldPassport'] = 'Y' if member['passport_no'] else 'N'
        member_detail['LastName'] = member['last_name']
        member_detail['MemberDOB'] = datetime.datetime.strptime(member['date_of_birth'], '%d-%m-%Y').strftime('%Y-%m-%d')
        member_detail['Nationality'] = 'India'
        member_detail['Age'] = relativedelta(
            datetime.datetime.today(),
            datetime.datetime.strptime(member['date_of_birth'], '%d-%m-%Y')
        ).years
        member_detail['NomineeName'] = member['nominee_name']
        member_detail['AppointeeName'] = member['appointee_name']
        member_detail['PassportNo'] = member['passport_no']
        member_detail['ProposerRelationCode'] = member['relationship']
        member_detail['AppointeeRelationCode'] = member['appointee_relation']
        member_detail['NomineeRelationCode'] = member['nominee_relation']
        member_detail['Salut'] = SALUTATION_CHOICE[member['gender']]    # TO-DO confirm with suhash
        member_details.append({'Member': member_detail})

    data_dict = {
        'SessionData': {
            'Index': 2 if quote_no else 1,
            'InitTime':  initTime.strftime(date_format),
            'UserName': 'coverfoxTrvl',
            'Password': PASSWORD,
            'OrderNo': order_no if order_no else 'NA',
            'QuoteNo': quote_no if quote_no else 'NA',
            'Route': 'INT',
            'Contract': 'STR',
            'ProductType': 'B2C',
            'Channel': 'covfxSTR',
            'TransactionType': 'Quote',
            'TransactionStatus': 'Fresh',
            'ID': '',
            'UserAgentID': '',
            'Source': '',
        },
        'Travel': {
            'TypeOfBusiness': 'NB',
            'TypeOfPolicy': policy_type,
            'TypeOfTrip': trip_type,
            'DepartureDate': datetime.datetime.strptime(from_date, '%d-%b-%Y').strftime('%Y-%m-%dT%H:%M:%S'),
            'ReturnDate': return_date,
            'TripDuration': trip_duration,
            'TypeOfCover': cover_type,
            'TypeOfTavel': travel_type,
            'MaxPerTripDuration': max_trip_per_duration,
            'GeographicalExtension': geographical_coverage,
            'FamilyType': family_type,
        },
        'Quote': {
            'PolicyStartDate': datetime.datetime.strptime(from_date, '%d-%b-%Y').strftime('%Y-%m-%dT%H:%M:%S'),
            'PolicyEndDate': return_date,
            'AgentNumber': '',
            'AgentName': '',
            'PlanSelected': plan_id if plan_id else '',
            'Stage': ''
        },
        'Client': {
            'ClientType': client_type,
            'CltDOB': datetime.datetime.strptime(search_data['DOB'], '%d-%b-%Y').strftime('%Y-%m-%d'),
            'GivName': insured_details['members'][0]['first_name'],
            'SurName': insured_details['members'][0]['last_name'],
            'EmailID': insured_details['members'][0]['email'],
            'MobileNo': insured_details['members'][0]['mobile'],
            'TPClientRefNo': tp_client_ref_no,
            'CltSex': insured_details['members'][0]['gender'],
            'Age': relativedelta(datetime.datetime.today(), datetime.datetime.strptime(member['date_of_birth'], '%d-%m-%Y')).years,
            'Salut': SALUTATION_CHOICE[insured_details['members'][0]['gender']],  # TO-DO  confrim
            'Occupation': '',
            'CltAddr01': insured_details['address']['address_line_1'],
            'CltAddr02': insured_details['address']['address_line_2'],
            'CltAddr03': insured_details['address']['address_line_3'],
            'City': insured_details['address']['city'],
            'State': insured_details['address']['state'],
            'PinCode': insured_details['address']['pincode'],
            'MemberDetails': member_details,
            'Nominee': {
                'Name': insured_details['members'][0]['nominee_name'],
                'Age': age,
                'NomineeRelationCode': insured_details['members'][0]['nominee_relation'],
                'AppointeeName': insured_details['members'][0]['appointee_name'],
                'AppointeeRelationCode': insured_details['members'][0]['appointee_relation'],
            },
            'SportsPerson': search_data['sportsperson'],
            'DangerousActivity': search_data['dangerous_activity'],
            'PreExistingIllness': search_data['pre_existing_illness'],
        }
    }
    return data_dict


def get_soap_request_response(XmlData, request_type="Proposal", transaction=None):
    Session = etree.Element('Session')
    session_etree = prod_utils.dict_to_etree(Session, XmlData)

    SessionDoc = etree.Element('SessionDoc')
    SessionDoc.append(session_etree)
    serve = etree.Element('serve', xmlns='http://schemas.cordys.com/gateway/Provider')
    serve.append(SessionDoc)
    Body = etree.Element('Body')
    Body.append(serve)
    Envelope = etree.Element('Envelope', xmlns='http://schemas.xmlsoap.org/soap/envelope/')
    Envelope.append(Body)
    proposal_request = etree.tostring(Envelope)
    http = httplib2.Http()
    headers = {"Content-type": "text/xml"}
    transaction_id = transaction.transaction_id if transaction else ''
    travel_logger.info('================================\n%s REQUEST: %s\n%s' % (request_type.upper(), transaction_id, proposal_request))
    response_header, proposal_response= http.request(REQUEST_URL, method='POST', body=proposal_request, headers=headers)
    travel_logger.info(
        '================================\n%s RESPONSE: %s\n%s'
        % (request_type.upper(), transaction_id, proposal_response)
    )
    return proposal_request, proposal_response


def get_region(places):
    from travel_product.forms import SumAssuredMatrix
    data = {
        'ASIA_EXCLUDING_JAPAN': 3,
        'AFRICA':               2,
        'REST_OF_EUROPE':       2,
        'SCHENGEN':             2,
        'UK':                   2,
        'AMERICA_CANADA':       1,
        'OTHER_COUNTRIES':      2,
    }
    return data[SumAssuredMatrix().get_region(places)]


def fill_with_dummy_data(data):
    adult_age_cutoff = [1, 18][cd['slab'] == 'family']
    cd['children'] = filter(lambda a: a < adult_age_cutoff, cd['ages'])
    cd['adults'] = filter(lambda a: a not in cd['children'], cd['ages'])

    duration = [['30'], ['45', '60']][data['duration'] > 30][data['duration'] > 45]

    ndata = {}
    ndata['dangerous_activity'] = 'N'
    ndata['email_id'] = 'suhas@coverfoxmail.com'
    ndata['sportsperson'] = 'N'
    ndata['pre_existing_illness'] = 'N'
    ndata['mobile'] = '9876543210'
    ndata['first_name'] = 'first'
    ndata['last_name'] = 'last'
    ndata['to_date'] = data['to_date'].strftime('%d-%b-%Y')
    ndata['from_date'] = data['from_date'].strftime('%d-%b-%Y')
    ndata['max_trip_length'] = duration if data['type'] == 'multiple' else ''
    ndata['trip_duration'] = (data['to_date'] - data['from_date']).days if data['type'] == 'single' else ''
    ndata['policy_type'] = data['slab'][0].upper()  # S-student, F-family,I-individual
    ndata['trip_type'] = 'S' if data['type'] == 'single' else 'A'

    ndata['Travelling_To'] = get_region(data['places'])

    childs = {
        0: '',
        1: '1C',
        2: '2C',
    }[len(data['children'])]
    adults = {
        1: 'S',
        2: 'SS',
    }[len(data['adults'])]

    ndata['family_type'] = adults+childs

    max_age = reduce(lambda x, y: x if x > y else y, data['ages'])
    today = datetime.date.today()
    dob = today.replace(day=1, month=1, year=today.year-max_age)  # TODO use dateutil.relativedelta, this one doesnt work for leap years.

    ndata['DOB'] = dob.strftime('%d-%b-%Y')
    return ndata


def get_quote(activity):
    tp_client_ref_no = 'CFOX-%s-%s' % (activity.id, datetime.datetime.now().strftime("%I%M%S"))
    data = activity.cleaned_data
    # data = fill_with_dummy_data(data) #get data in Normalized format. todo later.
    XmlData = get_xml_formated_data(data, tp_client_ref_no)

    response_header, response_content = get_soap_request_response(XmlData, 'Quote')
    soup = BeautifulSoup(response_content)
    Planset = soup.findAll('planset')
    order_no = soup.find('orderno').getText()
    quote_no = soup.find('quoteno').getText()
    extra_data = json.dumps({'order_no': order_no, 'quote_no': quote_no, 'tp_client_ref_no': tp_client_ref_no})
    plan_lst = []
    for tags in Planset:
        plandict = {}
        planname = tags.find('planname').getText()
        suminsured = tags.find('suminsured').getText()
        premium = tags.find('premium').getText()
        planid = tags.find('planid').getText()
        servicetax = tags.find('servicetax').getText()
        premiumpayable = tags.find('premiumpayable').getText()
        plandict['plan_type'] = planname
        plandict['sum_assured'] = 'USD %s' % suminsured
        plandict['base_premium'] = '%.2f' % float(premium)
        plandict['id'] = planid
        plandict['servicetax'] = servicetax
        plandict['premium'] = premiumpayable
        plandict['orderno'] = order_no
        plandict['quoteno'] = quote_no
        plan_lst.append(plandict)
    return plan_lst, extra_data


def post_transaction(transaction):
    resp_content = ""
    proposal_request = None
    proposal_response = None
    try:
        # Quote Request
        tp_client_ref_no = 'CFOX-%s-%s' % (transaction.transaction_id, datetime.datetime.now().strftime("%I%M%S"))
        XmlData = get_xml_formated_data(transaction, tp_client_ref_no)
        proposal_request, proposal_response= get_soap_request_response(XmlData, "Quote", transaction)
        soup = BeautifulSoup(proposal_response)
        order_no = soup.find('orderno').getText()
        quote_no = soup.find('quoteno').getText()
        plan_id = transaction.plan_details['id']

        if(not order_no or not quote_no):
            raise Exception("Quote request fucked up!")

        # Proposal Request
        XmlData = get_xml_formated_data(transaction, tp_client_ref_no, plan_id, quote_no, order_no)
        proposal_request, proposal_response = get_soap_request_response(XmlData, "Proposal", transaction)

        post_data = {
            'QuoteNo': quote_no,
            'OrderNo': order_no,
            'Channel': 'covfxSTR',
            'Product': 'STR',
            'Amount': transaction.plan_details['premium'],
            'IsMobile': 'N',
        }
        transaction.reference_id = order_no
        transaction.save()

        template_name = 'health_product/post_transaction_data.html'
        is_redirect = False
        redirect_url = PAYMENT_URL
    except Exception as e:
        travel_logger.info('BHARTI TRANSACTION\nEXCEPTION: %s\n%s' % (transaction.transaction_id, traceback.format_exc()))
        post_data = {
            'stack_trace': traceback.format_exc(),
            'error_message': e.message,
            'error_response': proposal_response,
        }
        redirect_url = "/travel-insurance/transaction/%s/failure/" % (transaction.transaction_id)
        is_redirect = True
        template_name = ""
    return is_redirect, redirect_url, template_name, post_data, proposal_request, proposal_response


def post_payment(payment_response, transaction):
    '''
    RESPONSE Ex:
    {u'status': u'success',
    u'emailId': u'kushalkothari@gmail.com',
    u'amount': u'810.00', u'link': u'/birt/reports/reportFiles/748272581352793.pdf',
    u'transactionRef': u'DHMP4039134720',
    u'orderNo': u'VDIH798241',
    u'policyNo': u'S9801949',
    u'ID': u'',
    u'productID':
    u'STR'}
    '''

    transaction = Transaction.objects.get(reference_id=payment_response['orderNo'])
    transaction.payment_response = json.dumps(payment_response)
    travel_logger.info(
        '\nBHARTI AXA TRANSACTION: %s\n PAYMENT RESPONSE RECEIVED:\n %s\n'
        % (transaction.transaction_id, payment_response)
    )
    post_data = {}
    if payment_response['status'].lower() in ['success', 'paypass']:
        if payment_response['status'].lower() == 'paypass':
            prod_utils.alert_mail(transaction, 'Generate Policy Document')
        else:
            # in case of paypass status, policyNo key is not present in the response
            transaction.policy_number = payment_response['policyNo']

        transaction.policy_start_date = transaction.activity.policy_start_date
        transaction.policy_end_date = transaction.activity.policy_end_date
        transaction.payment_done = True
        transaction.payment_on = datetime.datetime.today()
        transaction.extra['error_message'] = ''

        try:
            pdf_doc = requests.get(PDF_URL + payment_response['link'], verify=False)
            transaction.update_policy(pdf_doc.content, send_mail=False)
        except Exception as e:
            travel_logger.info('\nBHARTI AXA INTEGRATION: %s\n POLICY PDF EXCEPTION:\n %s\n' % (transaction.transaction_id, e))

        transaction.status = transaction.COMPLETED
    else:
        transaction.status = transaction.FAILED
        post_data = {
            'stack_trace': '',
            'error_message': payment_response['status'],
            'error_response': payment_response,
        }
    transaction.save()

    return transaction, transaction.status == transaction.COMPLETED, post_data
