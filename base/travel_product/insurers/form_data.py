GENDER_CHOICES = (
        ('m', 'Male'),
        ('f', 'Female'),
        )

MARRIED_CHOICES = (
        ('yes', 'Yes'),
        ('no', 'No'),
        )

RELATION_CHOICES = (
        ("self", "Self"),
        ("spouse", "Spouse"),
        ("son", "Son"),
        ("daughter", "Daughter"),
        )

NOMINEE_RELATION_CHOICES = (
    ('spouse', 'Spouse'),
    ('son', 'Son'),
    ('daughter', 'Daughter'),
    ('father', 'Father'),
    ('mother', 'Mother'),
    ('brother', 'Brother'),
    ('sister', 'Sister'),
    ('others', 'Others'),
)

APOLLO_NOMINEE_RELATION_CHOICES = (
    ('Husband', 'Husband'),
    ('Wife', 'Wife'),
    ('Father', 'Father'),
    ('Mother', 'Mother'),
    ('Son', 'Son'),
    ('Daughter', 'Daughter'),
    ('Sister', 'Sister'),
    ('Brother', 'Brother'),
)

OCCUPATION_CHOICES = (
    ('Retired', 'Retired'),
    ('Executive - Senior Level', 'Executive - Senior Level'),
    ('Executive - Middle Level', 'Executive - Middle Level'),
    ('Executive Junior Level', 'Executive Junior Level'),
    ('Software Professional', 'Software Professional'),
    ('Supervisor', 'Supervisor'),
    ('Clerical', 'Clerical'),
    ('Salesperson', 'Salesperson'),
    ('Self employed Professional', 'Self employed Professional'),
    ('Shop owner', 'Shop owner'),
    ('Student', 'Student'),
    ('Not Working', 'Not Working'),
    ('Housewife', 'Housewife'),
    ('Advocate / Lawyer', 'Advocate / Lawyer'),
    ('Chartered Accountants', 'Chartered Accountants'),
    ('Labourer', 'Labourer'),
    ('Farming', 'Farming'),
    ('Service', 'Service'),
    ('Businessman/Industrialist Large Scale', 'Businessman/Industrialist Large Scale'),
    ('Businessman/Industrialist Medium Scale', 'Businessman/Industrialist Medium Scale'),
    ('Businessman/Industrialist Small Scale', 'Businessman/Industrialist Small Scale'),
    ('Reinforcing Fitter', 'Reinforcing Fitter'),
    ('Carpenter', 'Carpenter'),
    ('Steel Fixer', 'Steel Fixer'),
    ('Mason', 'Mason'),
    ('Cook', 'Cook'),
    ('Others', 'Others')
)

APOLLO_MEDICAL_CHOICES = (
    ("None", "None"),
    ("High Blood Pressure", "High Blood Pressure"),
    ("Heart Condition", "Heart Condition"),
    ("Circulatory Disorder ", "Circulatory Disorder "),
    ("Haematological (blood) disorder", "Haematological (blood) disorder"),
    ("Diabetes", "Diabetes"),
    ("Disorders of the stomach / Large or Small intestine", "Disorders of the stomach / Large or Small intestine"),
    ("Hernia of any kind ", "Hernia of any kind"),
    ("Haemorrhoids", "Haemorrhoids"),
    ("Urinary Disorder", "Urinary Disorder"),
    ("Mental Condition", "Mental Condition"),
    ("Nervous Disorder", "Nervous Disorder"),
    ("Fainting episode", "Fainting episode"),
    ("Blackouts", "Blackouts"),
    ("Fits", "Fits"),
    ("Arthritis ", "Arthritis"),
    (
        "Disorders of the spinal cord or vertebral column like Slipped Disc etc",
        "Disorders of the spinal cord or vertebral column like Slipped Disc etc"
    ),
    ("paralysis of any kind", "paralysis of any kind"),
    ("Cancer of any kind", "Cancer of any kind"),
    ("Allergies", "Allergies"),
    ("Respiratory disorder", "Respiratory disorder"),
    ("Varicose Veins", "Varicose Veins"),
    (
        "Any diseases or injury requiring surgical or medical treatment",
        "Any diseases or injury requiring surgical or medical treatment"
    )
)
