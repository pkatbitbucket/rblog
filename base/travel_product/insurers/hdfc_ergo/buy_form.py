from travel_product.insurers import common_preset
from travel_product.insurers.hdfc_ergo import preset

OCCUPATIONS = common_preset.OCCUPATIONS

STATES = preset.STATE_CODES

CITIES = preset.CITY_CODES


RELATIONS = [
    {'key': "Self", 'value': "self"},
    {'key': 'Spouse', 'value': 'Spouse'},
    {'key': 'Son', 'value': 'Son'},
    {'key': 'Daughter', 'value': 'Daughter'},
    {'key': 'Father', 'value': 'Father'},
    {'key': 'Mother', 'value': 'Mother'},
]

NOMINEE_RELATIONS = [
    {'key': 'Spouse', 'value': 'Spouse'},
    {'key': 'Son', 'value': 'Son'},
    {'key': 'Daughter', 'value': 'Daughter'},
    {'key': 'Father', 'value': 'Father'},
    {'key': 'Mother', 'value': 'Mother'},
    {'key': 'Father-in-Law', 'value': 'Father-in-Law'},
    {'key': 'Mother-in-Law', 'value': 'Mother-in-Law'},
    {'key': 'Grand Daughter', 'value': 'Grand Daughter'},
    {'key': 'Grand Son', 'value': 'Grand Son'},
    {'key': 'Grand Father', 'value': 'Grand Father'},
    {'key': 'Grand Mother', 'value': 'Grand Mother'},
    {'key': 'Other', 'value': 'Other'},
]

SALUTATIONS = [
    {'key': 'Mr.', 'value': 'Mr.'},
    {'key': 'Mrs.', 'value': 'Mrs.'},
    {'key': 'Ms.', 'value': 'Ms.'},
]

GENDERS = [
        {'key':'Male','value':'M'},
        {'key':'Female','value':'F'},
        ]

PURPOSE_OF_TRAVEL = [
        {'key':'Business','value':'Business'},
        {'key':'Holidays','value':'Holiday'},
        ]
def get_form(request, transaction):
    members = transaction.plan_details['members']
    return {
        'num_insured': len(members),
        'members_list': members,
        'occupations_list': OCCUPATIONS,
        'relations_list': RELATIONS,
        'nominee_relations_list' : NOMINEE_RELATIONS,
        'salutations_list': SALUTATIONS,
        'gender_list': GENDERS,
        'states_list': STATES,
        'cities_list': CITIES,
        'purpose_of_travel_list':PURPOSE_OF_TRAVEL,
    }
