import requests
import traceback
from travel_product import settings as travel_settings
from travel_product.models import Transaction
import xlrd
import json
import os
from travel_product import prod_utils
from settings import SETTINGS_FILE_FOLDER
from collections import OrderedDict
from django.core.files.base import File
import datetime
import time
from dateutil.relativedelta import *
import httplib2
import re
from lxml import etree
from utils import travel_logger
import xml.etree.ElementTree as ET
import mail_utils
from travel_product import *
import copy
import preset
import settings


if settings.PRODUCTION:
    REQUEST_URL = 'https://hewspool.hdfcergo.com/TravelChannelPartner/webservice.asmx'
    PAYMENT_URL = 'https://netinsure.hdfcergo.com/onlineproducts/TravelOnline/tim.aspx'
    PRODUCER_CODE = 'CVFX0001'
else:
    REQUEST_URL = 'http://202.191.196.210/UAT/OnlineProducts/travelcp/WebService.asmx'
    PAYMENT_URL = 'http://202.191.196.210/UAT/OnlineProducts/TravelOnline/Tim.aspx'
    PRODUCER_CODE = 'CVFX001'

PREMIUM_SHEET_PATH = os.path.join(
    SETTINGS_FILE_FOLDER,
    'travel_product/insurers/hdfc_ergo/premium_sheet/Travel_ChannelPartner.xls'
)

PLAN_DICT = {
    'PFLY1': {'plan': 'Family Floater Silver', 'sum_assured': 'USD 50000'},
    'PPMT1': {'plan': 'Multi Trip Worldwide Gold - 30 Days', 'sum_assured': 'USD 250000'},
    'PPMT3': {'plan': 'Multi Trip Worldwide Gold - 45 Days', 'sum_assured': 'USD 250000'},
    'PPMT2': {'plan': 'Multi Trip Worldwide Platinum - 30 Days', 'sum_assured': 'USD 500000'},
    'PPMT4': {'plan': 'Multi Trip Worldwide Platinum - 45 Days', 'sum_assured': 'USD 500000'},
    'PSTEU1': {'plan': 'Single Trip Excluding USA/Canada-30,000', 'sum_assured': 'USD 30000'},
    'PSTEU2': {'plan': 'Single Trip Excluding USA/Canada-50,000', 'sum_assured': 'USD 50000'},
    'PSTEU3': {'plan': 'Single Trip Excluding USA/Canada-100,000', 'sum_assured': 'USD 100000'},
    'PSTEU4': {'plan': 'Single Trip Excluding USA/Canada-200,000', 'sum_assured': 'USD 200000'},
    'PSTEU5': {'plan': 'Single Trip Excluding USA/Canada-500,000', 'sum_assured': 'USD 500000'},
    'PSTIU1': {'plan': 'Single Trip Including USA/Canada-Bronze-30,000', 'sum_assured': 'USD 30000'},
    'PSTIU2': {'plan': 'Single Trip Including USA/Canada-Silver-50,000', 'sum_assured': 'USD 50000'},
    'PSTIU3': {'plan': 'Single Trip Including USA/Canada-Gold-100,000', 'sum_assured': 'USD 100000'},
    'PSTIU4': {'plan': 'Single Trip Including USA/Canada-Platinum-200,000', 'sum_assured': 'USD 200000'},
    'PSTIU5': {'plan': 'Single Trip Including USA/Canada-Titanium-500,000', 'sum_assured': 'USD 500000'},
    'STAEJ1': {'plan': 'Single Trip Asia Bronze Excluding Japan', 'sum_assured': 'USD 15000'},
    'STAEJ2': {'plan': 'Single Trip Asia Silver Excluding Japan', 'sum_assured': 'USD 30000'},
    # --------------------STUDENT----------------
    # 'BREUC': {'plan': 'Bronze-Excl USA/Canada', 'sum_assured': 'USD 50000'},
    # 'BRIUC': {'plan': 'Bronze-Inc USA/Canada (Worldwide)', 'sum_assured': 'USD 50000'},
    # 'BRPEUC': {'plan': 'Bronze Plus-Excl USA/Canada', 'sum_assured': 'USD 50000'},
    # 'BRPIUC': {'plan': 'Bronze Plus-Inc USA/Canada (Worldwide)', 'sum_assured': 'USD 50000'},
    'GDEUC': {'plan': 'Gold-Excl USA/Canada', 'sum_assured': 'USD 250000'},
    'GDIUC': {'plan': 'Gold-Inc USA/Canada (Worldwide)', 'sum_assured': 'USD 250000'},
    'GDPEUC': {'plan': 'Gold Plus-Excl USA/Canada', 'sum_assured': 'USD 250000'},
    'GDPIUC': {'plan': 'Gold Plus-Inc USA/Canada (Worldwide)', 'sum_assured': 'USD 250000'},
    'PPTEUC': {'plan': 'Platinum-Excl USA/Canada', 'sum_assured': 'USD 500000'},
    'PPTIUC': {'plan': 'Platinum-Inc USA/Canada (Worldwide)', 'sum_assured': 'USD 500000'},
    'PST': {'plan': 'Standard WorldWide', 'sum_assured': '0'},
    'PTPEUC': {'plan': 'Platinum Plus-Excluding USA/CAN', 'sum_assured': 'USD 500000'},
    'PTPIUC': {'plan': 'Platinum Plus-Including USA/CAN', 'sum_assured': 'USD 500000'},
    'SIEUC': {'plan': 'Silver-Excl USA/Canada', 'sum_assured': 'USD 100000'},
    'SIIUC': {'plan': 'Silver-Inc USA/Canada (Worldwide)', 'sum_assured': 'USD 100000'},
    'SIPEUC': {'plan': 'Silver Plus-Excl USA/Canada', 'sum_assured': 'USD 100000'},
    'SIPIUC': {'plan': 'Silver Plus-Inc USA/Canada (Worldwide)', 'sum_assured': 'USD 100000'},
}

FAMILY_TYPES = {
    'self+spouse': '2A',
    'self+spouse+child1': '2A1C',
    'self+spouse+child1+child2': '2A2C',
}

DEPENDENT_PARENT = {
    'Father': 'Father',
    'Mother': 'Mother',
    'Both': 'Father+Mother',
}

ADDITIONAL_CHILDREN = {
    '1': 'child3',
    '2': 'child4',
}

SALUTATION_CHOICES = {
    'ms': 'Mr',
    'mm': 'Mr',
    'fs': 'Miss',
    'fm': 'Mrs',
}

FLOATER_PLAN_CHOICES = {
    '2A': '0',
    '2A1C': '1',
    '2A2C': '2',
}

NOMINEE_RELATIONSHIPS = {
    'spouse': 'Spouse',
    'son': 'Son',
    'daughter': 'Daughter',
    'father': 'Father',
    'mother': 'Mother',
    'brother': 'Other',
    'sister': 'Other',
    'others': 'Other',
}


def pre_transaction(transaction):
    search_data = transaction.extra['search_data']
    family_type = search_data['family_type']
    dependent_parent = search_data.get('dependent_parent', '')
    additional_children = search_data.get('additional_children', '0')
    policy_type = search_data['policy_type']
    if policy_type == 'F':
        insureds = [mem.strip() for mem in family_type.split('+')]
        if dependent_parent != '' and dependent_parent != 'None' and dependent_parent != 'Both':
            insureds.append(DEPENDENT_PARENT[dependent_parent])
        if dependent_parent == 'Both':
            insureds.append('Father')
            insureds.append('Mother')
        if additional_children != '' and additional_children != '0':
            insureds.append(ADDITIONAL_CHILDREN[additional_children])
    else:
        insureds = ['self']
    transaction.plan_details['members'] = insureds
    transaction.plan_details['plan'] = transaction.plan_details['plan_type']
    family_type = search_data['family_type']

    from_date = datetime.datetime.strptime(search_data['from_date'], '%d-%b-%Y')
    to_date = datetime.datetime.strptime(search_data['to_date'], '%d-%b-%Y')
    search_data['trip_duration'] = (to_date - from_date).days + 1

    if('trip_type' in search_data and search_data['trip_type'] == 'A'):
        search_data['to_date'] = (from_date + relativedelta(days=365)).strftime('%d-%b-%Y')

    transaction.extra['insured_details_json'] = search_data

    return None


def get_xml(transaction):
    search_data = transaction.extra['search_data']
    insured_data = transaction.insured_details_json
    policy_type = search_data['policy_type']
    trip_type = search_data.get('trip_type', '')
    geographical_coverage = search_data['geographical_coverage']
    family_type = search_data.get('family_type')
    floater_plan = FAMILY_TYPES[family_type] if family_type else ''
    no_of_kids = FLOATER_PLAN_CHOICES[floater_plan] if floater_plan else '0'
    dependent_parent = search_data.get('dependent_parent', '')
    additional_children = search_data.get('additional_children', '0')
    from_date = search_data['from_date']
    to_date = search_data['to_date']
    dob = search_data['dob']
    dep_date = datetime.datetime.strptime(from_date, '%d-%b-%Y').strftime('%d/%m/%Y')

    if to_date != "":
        return_date = datetime.datetime.strptime(to_date, '%d-%b-%Y').strftime('%d/%m/%Y')
    else:
        return_date = datetime.datetime.strptime(dep_date, '%d/%m/%Y') + relativedelta(days=365).strftime('%d/%m/%Y')
    
    if search_data['trip_duration'] != '':
        trip_duration = 366 if trip_type == "A" else int(search_data['trip_duration'])
    else:
        trip_duration = ""
        
    address1 = insured_data['address']['address_line_1']
    address2 = insured_data['address']['address_line_2']
    address3 = insured_data['address']['address_line_3']
    statecd = insured_data['address']['state']
    citycd = insured_data['address']['city']
    sum_insured = transaction.plan_details['sum_assured'].replace("USD", "").strip()
    plan_cd = transaction.plan_details['plan_type']
    plan_name = plan_cd.split('-')[0] if plan_cd else ''
    total_premium = transaction.plan_details['premium']
    base_premium = transaction.plan_details['base_premium']
    service_tax = str(int(total_premium)-int(base_premium))

    for val in range(len(PLAN_DICT.values())):
        if PLAN_DICT.values()[val]['plan'] == plan_cd:
            plancd = PLAN_DICT.keys()[val]
    for val in range(len(preset.STATE_CODES)):
        if preset.STATE_CODES[val]['value'] == statecd:
            state = preset.STATE_CODES[val]['key']
    for val in range(len(preset.CITY_CODES[statecd])):
        if preset.CITY_CODES[statecd][val]['value'] == int(citycd):
            city = preset.CITY_CODES[statecd][val]['key']

    for member in insured_data['members']:
        first_name = member['first_name']
        middle_name = ''
        marital_status = 's' if member['married'] == 'no' else 'm'
        salutation = SALUTATION_CHOICES[member['gender']+marital_status]
        last_name = member['last_name']
        dob = datetime.datetime.strptime(member['date_of_birth'], '%d-%m-%Y').strftime("%d/%m/%Y")
        age = relativedelta(datetime.datetime.today(), datetime.datetime.strptime(dob, '%d/%m/%Y').date()).years
        p_visit = member['purpose_of_visit'].title()
        pre_existing_illness = member['ped']
        restriction_by_insurance = 'False' if member['restrictions'] == 'no' else 'True'
        decline_insurance = 'False' if member['medical_declined'] == 'no' else 'True'
    mobile = insured_data['members'][0]['mobile']
    email = insured_data['members'][0]['email']
    pincode = insured_data['address']['pincode']
    decline_reason = search_data.get('decline_reason', '')
    sum_insured = transaction.plan_details['sum_assured'].replace("USD", "").strip()
    plan_cd = transaction.plan_details['plan_type']
    plan_name = plan_cd.split('-')[0] if plan_cd else ''
    total_premium = transaction.plan_details['premium']
    base_premium = transaction.plan_details['base_premium']
    service_tax = str(int(total_premium)-int(base_premium))

    data_dict = {
        'InsuranceDetails': {
            'PlanDetails': {
                'ProducerCd': PRODUCER_CODE,
                'TotalSumInsured': sum_insured,
                'PlanCd': plancd,
                'DepartureDate': dep_date,
                'ArrivalDate': return_date,
                'TravelDays': int(trip_duration) if trip_duration != '' else 366,
                'BasePremiumAmt': '',
                'TotalBasePremiumAmt': '',
                'ServiceTax': '',
                'TotalPremium': total_premium,
                'purposeofvisitcd': p_visit,
                'PlacesVisitedCd': '',
                'NoOfAdults': '2' if family_type else '1',
                'NoOfKids': str(int(no_of_kids) + int(additional_children)) if additional_children != '' else no_of_kids,
                'FloaterPlan': floater_plan,
                'DependentParent': dependent_parent,
                'NoOfAdditionalKids': additional_children,
            },
            'PaymentDetails': {
                'PaymentOption': 'Credit Card',
                'PaymentReferance': '83',
            },
            'CustDetails': {
                'FirstName': insured_data['members'][0]['first_name'],
                'MiddleName':  '',
                'LastName':  insured_data['members'][0]['last_name'],
                'DOB': datetime.datetime.strptime(insured_data['members'][0]['date_of_birth'], '%d-%m-%Y').strftime("%d/%m/%Y"),
                'Gender': insured_data['members'][0]['gender'].upper(),
                'Address1': address1,
                'Address2': address2,
                'Address3': address3,
                'StateCode': statecd,
                'CityCode': citycd,
                'Pincode': pincode,
                'STDCodeResi': '',
                'TelResi': '',
                'STDCodeOff': '',
                'TelOff': '',
                'MobileNumber': mobile,
                'OverseasNo': '',
                'Email': email,
                'ExistingAliments': '',
                'PhysicianName': '',
                'PhysicianNo': '',
                'DeclineInsurance': decline_insurance,
                'DeclineReason': decline_reason,
                'RestrictionByIns': restriction_by_insurance,
                'RestrictionByInsDetails': '',
            },
            'Member': {
                'InsuredDetails':
                {
                },
            },
            },
    }

    return data_dict


def get_soap_request_response(transaction):
    XmlData = get_xml(transaction)
    data = transaction.extra['search_data']
    insured_data = transaction.insured_details_json
    policy_type = data['policy_type']
    family_type = data['family_type']
    dob = data['dob']
    Session = etree.Element('str')
    session_etree = prod_utils.dict_to_etree(Session, XmlData)
    data_file = session_etree[0].getchildren()
    insureds = [mem.strip() for mem in family_type.split('+')] if policy_type == 'F' else ['self']
    for elem in data_file:
        if 'Member' in elem.tag:
            elem_member = elem
            insured_member = elem_member.getchildren()[0]
            elem_member.remove(insured_member)
            for mem in insured_data['members']:
                tmp_insured_member = copy.deepcopy(insured_member)
                tmp_insured_member.attrib['InsFirstName'] = mem.get('first_name')
                tmp_insured_member.attrib['InsMiddleName'] = ''
                tmp_insured_member.attrib['InsLastName'] = mem.get('last_name')
                tmp_insured_member.attrib['InsGender'] = mem.get('gender').upper()
                tmp_insured_member.attrib['InsuredRelation'] = mem['relationship']
                tmp_insured_member.attrib['InsDOB'] = datetime.datetime.strptime(mem['date_of_birth'], '%d-%m-%Y').strftime("%d/%m/%Y")
                tmp_insured_member.attrib['PassportNo'] = mem.get('passport_no')
                tmp_insured_member.attrib['NomineeName'] = mem.get('nominee_name')
                tmp_insured_member.attrib['NomineeRelation'] = NOMINEE_RELATIONSHIPS[mem.get('nominee_relation')]
                elem_member.append(tmp_insured_member)
    dict_string = etree.tostring(session_etree.getchildren()[0]).replace('<', '&lt;').replace('>', '&gt;').replace('"', '&quot;')
    first_string = '<IssuePolicy xmlns = "http://tempuri.org/"><str>%s</str></IssuePolicy>' % dict_string
    tag = etree.fromstring(first_string)
    Body = etree.Element('Body')
    Body.append(tag)
    Envelope = etree.Element('Envelope', xmlns="http://schemas.xmlsoap.org/soap/envelope/")
    Envelope.append(Body)
    proposal_request = etree.tostring(Envelope)
    http = httplib2.Http()
    headers = {"Content-type": "text/xml", 'SOAPAction': "http://tempuri.org/IssuePolicy"}
    if transaction:
        request_type = 'Proposal'
        request_id = transaction.transaction_id
    else:
        request_type = 'Quote'
        request_id = activity.activity_id

    travel_logger.info(
        '=============HDFC ERGO %s REQUEST=================== \n%s\n%s'
        % (request_type, request_id, proposal_request)
    )

    response_header, proposal_response = http.request(REQUEST_URL, method='POST', body=proposal_request, headers=headers)
    travel_logger.info(
        '=============HDFC ERGO %s RESPONSE=================== \n%s\n%s'
        % (request_type, request_id, proposal_response)
    )

    return proposal_request, proposal_response

    
def get_quote(activity):
    read_excel = xlrd.open_workbook(PREMIUM_SHEET_PATH)
    travel_plan_master = read_excel.sheet_by_name('Travel PLan Master')
    travel_rates = read_excel.sheet_by_name('Travel New Rates')
    search_data = activity.search_data
    geographical_coverage = search_data['geographical_coverage']
    policy_type = search_data['policy_type']
    trip_type = 'multi' if geographical_coverage == "Worldwide" else 'single'
    max_duration_per_trip = search_data['max_trip_length']
    travelling_to_usncanada = 'Yes' if geographical_coverage == 'WorldWide-Including USA/Canada' else 'No'
    dob = search_data['dob']
    student_trip_duration = search_data['student_traveldays']
    if search_data['trip_duration'] != '':
        trip_duration = 366 if trip_type == "multi" else int(search_data['trip_duration'])
    else:
        trip_duration = int(student_trip_duration)
    age = relativedelta(datetime.datetime.today(), datetime.datetime.strptime(dob, travel_settings.DATE_FORMAT).date()).years
    if policy_type == 'S':
            if travelling_to_usncanada == 'Yes':
                plan_code = 'IUC'
            else:
                plan_code = 'EUC'
    else:
            if trip_type == "single" and geographical_coverage == "WorldWide-Excluding USA/Canada" and policy_type != 'F':
                plan_code = 'PSTEU'
            elif policy_type == 'F' and geographical_coverage == 'WorldWide-Excluding USA/Canada':
                plan_code = 'PFLY1'
            elif trip_type == 'multi' and geographical_coverage == 'Worldwide' and max_duration_per_trip == '30':
                plan_code = 'PPMT2'
                # plan_code = 'PPMT1'
            elif trip_type == 'multi' and geographical_coverage == 'Worldwide' and max_duration_per_trip == '45':
                plan_code = 'PPMT4'
                # plan_code = 'PPMT3'
            elif trip_type == 'single' and geographical_coverage == 'WorldWide-Including USA/Canada':
                plan_code = 'PSTIU'
            elif trip_type == 'single' and geographical_coverage == 'Asia - Excluding Japan':
                plan_code = 'STAEJ'
    plans = []
    for row in range(1, travel_rates.nrows):
        plandict = {}
        plancode = travel_rates.cell_value(row, 0)
        min_age = int(travel_rates.cell_value(row, 3))
        max_age = int(travel_rates.cell_value(row, 4))
        min_days = int(travel_rates.cell_value(row, 5))
        max_days = int(travel_rates.cell_value(row, 6))
        base_premium = float(travel_rates.cell_value(row, 7))
        total_premium = float(travel_rates.cell_value(row, 8))
        floater_plan = travel_rates.cell_value(row, 9).strip()

        if (plan_code in plancode) and (min_age <= age <= max_age) and (min_days <= trip_duration <= max_days):
            plancd = plancode.strip()
            family_member = FAMILY_TYPES.get(search_data['family_type'])
            if plancd == 'PFLY1':
                if floater_plan == family_member:
                    sum_assured = PLAN_DICT[plancd]['sum_assured']
                    plan_name = PLAN_DICT[plancd]['plan']
                    plandict['sum_assured'] = sum_assured
                    plandict['premium'] = round(total_premium)
                    plandict['plan_type'] = plan_name
                    plandict['base_premium'] = round(base_premium)
                    plans.append(plandict)
            else:
                sum_assured = PLAN_DICT[plancd]['sum_assured']
                plan_name = PLAN_DICT[plancd]['plan']
                plandict['sum_assured'] = sum_assured
                plandict['premium'] = round(total_premium)
                plandict['plan_type'] = plan_name
                plandict['base_premium'] = round(base_premium)
                plans.append(plandict)

    extra_data = None
    return plans, extra_data

    
def post_transaction(transaction):
    proposal_request = None
    proposal_response = None
    try:
        proposal_request, proposal_response = get_soap_request_response(transaction=transaction)
        response_status = proposal_response[proposal_response.find("WsStatus"):proposal_response.find('/WsStatus')].replace("WsStatus&gt;","").replace("&lt;","")
        proposal_no = proposal_response[proposal_response.find("WsMessage"):proposal_response.find('/WsMessage')].replace("WsMessage&gt;","").replace("&lt;","")
        
        if not proposal_no:
            raise Exception(proposal_no)

        insured_details = transaction.insured_details_json
        post_data = {
            'CustomerID': proposal_no,
            'TxnAmount':  transaction.plan_details['premium'],
            'AdditionalInfo1': 'NB',
            'AdditionalInfo2': 'RTI',
            'AdditionalInfo3': '1',
            'hdnPayMode': 'CC',
            'UserName': insured_details['members'][0]['first_name']+' '+insured_details['members'][0]['last_name'],
            'UserMailId': insured_details['members'][0]['email'],
            'ProductCd': 'RTI',
            'ProducerCd': PRODUCER_CODE,
        }
        transaction.reference_id = proposal_no
        transaction.save()

        template_name = 'health_product/post_transaction_data.html'
        redirect_url = PAYMENT_URL
        is_redirect = False
    except Exception as e:
        travel_logger.info('\n HDFC ERGO\n TRANSACTION EXCEPTION: %s\n%s\n' % (transaction.transaction_id, traceback.format_exc()))
        post_data = {
            'stack_trace': traceback.format_exc(),
            'error_message': e.message,
            'error_response': proposal_response,
        }
        redirect_url = "/travel-insurance/transaction/%s/failure/" % (transaction.transaction_id)
        is_redirect = True
        template_name = ""
    return is_redirect, redirect_url, template_name, post_data, proposal_request, proposal_response

    
def post_payment(payment_response, transaction):
    # RESPONSE: {u'Msg': u'Payment transcation was not completed', u'PolicyNo': u'0', u'ProposalNo': u'RT1509010509'}
    transaction = Transaction.objects.get(reference_id=payment_response['ProposalNo'])
    transaction.payment_response = payment_response

    travel_logger.info(
        '\nHDFC ERGO TRANSACTION: %s\n PAYMENT RESPONSE RECEIVED:\n %s\n'
        % (transaction.transaction_id, payment_response)
    )
    post_data = {}
    transaction.policy_number = payment_response['PolicyNo']
    if payment_response['Msg'] == 'Successfull':
        transaction.policy_start_date = transaction.activity.policy_start_date
        transaction.policy_end_date = transaction.activity.policy_end_date
        transaction.payment_done = True
        transaction.payment_on = datetime.datetime.today()
        transaction.status = transaction.COMPLETED
        transaction.extra['error_message'] = ''
    else:
        transaction.status = transaction.FAILED
        post_data = {
                'stack_trace': "",
                'error_message': payment_response['Msg'],
                'error_response': payment_response,
        }
    transaction.save()

    return transaction, transaction.status == transaction.COMPLETED, post_data
