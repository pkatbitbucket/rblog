import json
import copy
from travel_product.insurers import common_preset

OCCUPATIONS = common_preset.OCCUPATIONS;

STATES = copy.deepcopy(common_preset.STATES);
CITIES = copy.deepcopy(common_preset.CITIES);

change_state_old_values = [
        {'old_value':'Orissa','new_value':'Odisha'},
        {'old_value':'Tamilnadu','new_value':'Tamil Nadu'},
        {'old_value':'Pondicherry','new_value':'Pondicherry U.T.'},
        {'old_value':'Chandigarh','new_value':'Chandigarh U.T.'},
        {'old_value':'Lakshadweep','new_value':'Lakshadweep U.T.'},
        {'old_value':'Uttarakhand','new_value':'Uttaranchal'},
        ]

for key in change_state_old_values:
    for s in STATES:
        if s['value'] == key['old_value']:
            s['value'] = key['new_value']
            CITIES[key['new_value']] = CITIES[key['old_value']]
            del CITIES[key['old_value']]
            break

RELATIONS = [
    {'key': 'Spouse', 'value': 'Spouse'},
    {'key': 'Son', 'value': 'Dependent Son'},
    {'key': 'Daughter', 'value': 'Dependent Daughter'},
]

NOMINEE_RELATIONS = [
    {'key': 'Spouse', 'value': 'Spouse'},
    {'key': 'Son', 'value': 'Dependent Son'},
    {'key': 'Daughter', 'value': 'Dependent Daughter'},
    {'key': 'Father', 'value': 'Father'},
    {'key': 'Mother', 'value': 'Mother'},
    {'key': 'Brother', 'value': 'Brother'},
    {'key': 'Sister', 'value': 'Sister'},
    {'key': 'Others', 'value': 'Others'},
]
SALUTATIONS = [
    {'key':'Mr.','value':'Mr.'},
    {'key':'Mrs.','value':'Mrs.'},
    {'key':'Ms.','value':'Ms.'},
]

def get_form(request, transaction):
    members = transaction.plan_details['members']

    return {
        'num_insured': len(members),
        'members_list': members,
        'occupations_list': OCCUPATIONS,
        'relations_list': RELATIONS,
        'nominee_relations_list' : NOMINEE_RELATIONS,
        'salutations_list': SALUTATIONS,
        'states_list': STATES,
        'cities_list': CITIES,
        'declined_risks_list': list(transaction.slab.plan.insurer.declined_risks.values_list('malady',flat=True)),
    }
