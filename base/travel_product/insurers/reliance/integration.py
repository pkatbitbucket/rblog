import traceback
from django.conf import settings
from lxml import etree
import httplib2
import datetime
from dateutil.relativedelta import relativedelta
from collections import OrderedDict
import json
from utils import travel_logger
import travel_product.settings as travel_settings
import travel_product.insurers.settings as insurer_settings
from travel_product import prod_utils
import requests
import os
from django.core.files.base import File


if settings.PRODUCTION:
    # =============PRODUCTION CREDENTIALS=================
    WSDL_URL = 'http://rgiclservices.reliancegeneral.co.in/RGICL_OnlineServices/RGICLPortalTravel_WCF.svc?wsdl'
    PAYMENT_URL = 'http://rgiclservices.reliancegeneral.co.in/RGICL_OnlineViewPolicy/CommonPaymentRequest.aspx'
    URL = 'http://rgiclservices.reliancegeneral.co.in/RGICL_OnlineServices/RGICLPortalTravel_WCF.svc/RGICLPortalTravel_WCF'
    USERNAME = 'RBPO13B01'
    PASSWORD = 'l774l2'
    # ==============================================
else:
    # =============TEST CREDENTIALS=================
    WSDL_URL = 'http://rgiclservices.reliancegeneral.co.in/RGICL_ExternalServices/RGICLPortalTravel_WCF.svc?wsdl'
    PAYMENT_URL = 'http://rgiclservices.reliancegeneral.co.in/RGICLWCF_POLICY/CommonPaymentRequest.aspx'
    URL = 'http://rgiclservices.reliancegeneral.co.in/RGICL_ExternalServices/RGICLPortalTravel_WCF.svc/RGICLPortalTravel_WCF'
    USERNAME = '100001'
    PASSWORD = 'admin1'
    # ==============================================
BOOL = {
    'yes': 'True',
    'no': 'False',
    True: 'True',
    False: 'False',
    '': ''
}

SALUTATION_CHOICE = {
    'm': 'Mr',
    'f': 'Mrs',
    'M': 'Mr',
    'F': 'Mrs'
}

RELATION_CHOICES = {
    'self': 'Self',
    'spouse': 'Spouse',
    'son': 'Dependent Son',
    'daughter': 'Dependent Daughter',
}
# Sum assaured value for plans
PLANS = {
    'Asia': {
        'Standard': 'USD 25,000',
        'Silver': 'USD 30,000'
    },
    'Schengen': {
        'Basic': 'EUR 30,000',
        'Standard': 'EUR 50,000'
    },
    'Individual': {
        'Standard': 'USD 50,000',
        'Basic': '',
        'Silver': 'USD 100,000',
        'Gold': 'USD 250,000',
        'Platinum': 'USD 500,000'
    },
    'Family': {'Gold': 'USD 100,000', 'Standard': 'USD 50,000'},
    'Student': {
        'Standard': 'USD 50,000',
        'Basic': '',
        'Silver': 'USD 100,000',
        'Gold': 'USD 250,000',
        'Platinum': 'USD 500,000'},
    'Annual Multi Trip': {
        'Standard': 'USD 100,000',
        'Plus': 'USD 250,000',
        'Elite': 'USD 500,000'}
}


def get_product_type(search_data):
    if search_data['trip_type'] == 'multiple':
        product_type = 'Annual Multi Trip'
    elif search_data['traveller_type'] != 'Individual':
        product_type = search_data['traveller_type']
    elif search_data['region'] not in ['Asia (except Japan)', 'Schengen']:
        product_type = 'Individual'
    elif search_data['region'] == 'Asia (except Japan)':
        product_type = 'Asia'
    else:
        product_type = 'Schengen'
    return product_type


def home_required(transaction):
    home_required = False
    search_data = transaction.extra['search_data']
    plan_type = transaction.plan_details['plan_type'].strip()
    product_type = get_product_type(search_data)
    if product_type == 'Individual':
        home_required = (plan_type in ['Silver', 'Gold', 'Platinum'])
    elif product_type == 'Family':
        home_required = True
    elif search_data['trip_type'] == 'multiple':
        home_required = (plan_type in ['Plus', 'Elite'])
    return home_required


def create_xml_for_request(data_dict, method_tag_tuple):
    '''
    Create xml data for the request
    '''
    xmlns_path_1 = 'http://schemas.xmlsoap.org/soap/envelope/'
    Envelope = etree.Element('Envelope', xmlns=xmlns_path_1)
    Body = etree.Element('Body')
    method_tag = etree.Element(method_tag_tuple[0], xmlns=method_tag_tuple[1])
    ordered_object_tags = ['objUserDetails',
                           'objPremiumCalDC',
                           'objTravelCoverageDC',
                           'objTravelCommunicationAddrDC',
                           'objApplicant',
                           'objTravelHomeBurgalaryAddrDC',
                           'objTravelDoctorAddrDC',
                           'objTravelUniversityAddrDC',
                           'objTravelSponsorAddrDC',
                           'objTravelCompanyAddrDC',
                           'objApplicantSpouse',
                           'objApplicantChild1',
                           'objApplicantChild2',
                           'objTravelPremium']

    for each in ordered_object_tags:
        if each in data_dict:
            obj_tag = data_dict[each]
            objTravelPremium = etree.Element(each)
            xmlns_path_2 = 'http://schemas.datacontract.org/' \
                           '2004/07/IMDPortalEC.PortalEC'
            for key, val in obj_tag.items():
                sub_element = etree.Element(key, xmlns=xmlns_path_2)
                sub_element.text = val
                objTravelPremium.append(sub_element)
            method_tag.append(objTravelPremium)

    Body.append(method_tag)
    Envelope.append(Body)

    request = etree.tostring(Envelope)
    return request


def get_quote(activity):
    """
    Get the premium by the wsdl url
    """
    search_data = activity.search_data
    multi_trip = (search_data['trip_type'] == 'multiple')

    product_type = get_product_type(search_data)

    if not multi_trip and search_data['traveller_type'] == 'Individual':
        visiting_usa = BOOL[search_data['region'] == 'World including USA & Canada']
    else:
        visiting_usa = BOOL[search_data['visiting_usa']]

    is_oci_or_nonoci = (search_data['indian_citizen'] == 'no') and (search_data['is_oci_or_nonoci'] == 'yes')
    is_oci = is_oci_or_nonoci and (search_data['oci_or_nonoci'] == 'OCI')
    is_nonoci = is_oci_or_nonoci and (search_data['oci_or_nonoci'] == 'NONOCI')

    addon_benefits = BOOL[search_data['addon_benefits']] if search_data['traveller_type'] == 'Student' else ''

    if search_data['ped'] == 'None':
        ped = 'False'
    elif search_data['ped'] == 'Any Other':
        ped = search_data['ped']
    else:
        ped = 'True'

    objPremiumCalDC = OrderedDict([
        ('AddonBenefits', addon_benefits),
        ('CountryOfStay', search_data['country_of_stay'] if is_oci else ''),
        ('CoverType', search_data['cover_type'] if product_type == 'Family' else ''),
        ('DateOfBirth', search_data['dob']),
        ('IsIndianCitizen', BOOL[search_data['indian_citizen']]),
        ('IsInsuredOnImmigrantVisa', '' if multi_trip else BOOL[search_data['immigration_visa']]),
        ('IsNONOCI', BOOL[is_nonoci]),
        ('IsOCI', BOOL[is_oci]),
        ('IsOCIorNONOCI', BOOL[is_oci_or_nonoci]),
        ('JourneyEndDate', '' if multi_trip else transaction.activity.policy_end_date.strftime("%d-%b-%Y")),
        ('JourneyStDate', transaction.activity.policy_start_date.strftime("%d-%b-%Y")),
        ('MaxNoofTravelDays', search_data['max_trip_length'] if multi_trip else ''),
        ('OCINumber', search_data['country_of_stay'] if is_oci else ''),
        ('PassportIssueCountry', search_data['passport_issuer'] if is_nonoci else ''),
        ('PlanType', ''),
        ('ProductType', product_type),
        ('SportsActivityEnvolved', BOOL[search_data['sports']]),
        ('SufferingAnyPreDisease', ped),
        ('VisitingUSA', visiting_usa),
    ])

    data_dict = {"objTravelPremium": objPremiumCalDC}
    method_tag_tuple = ("getTravelPremium", "http://tempuri.org/")
    request = create_xml_for_request(data_dict,  method_tag_tuple)
    travel_logger.info('===================\nRELIANCE QUOTE REQUEST:\n%s\n' % request)
    http = httplib2.Http()
    headers = {
        "SOAPAction": "http://tempuri.org/IRGICLPortalTravel_WCF/getTravelPremium",
        "Content-Type": "text/xml"
    }
    response_header,  response_content = http.request(URL,  method='POST',  body=request,  headers=headers)
    # travel_logger.info('===================\nRELIANCE QUOTE RESPONSE:\n%s\n' % response_content)
    # transaction =None

    # parse the response content to get the premium plans
    tree = etree.fromstring(response_content)
    plans = tree[0][0][0].getchildren()[1].getchildren()[1][0].getchildren()
    plans_list = []
    for plan in plans:
        if plan.tag != 'Error':
            plan_type = plan.getchildren()[0].text.split('-')[0]
            premium = plan.getchildren()[1].text
            sum_assured_value = PLANS.get(get_product_type(search_data).strip(), '')
            if sum_assured_value:
                sum_assured_value = sum_assured_value.get(plan_type.strip(), '')
                d = {
                    'id': plan.attrib['{urn:schemas-microsoft-com:xml-diffgram-v1}id'],
                    'plan_type': plan_type,
                    'premium': premium,
                    'sum_assured': sum_assured_value
                }
                plans_list.append(d)
        else:
            error_list = plan.getchildren()
            d = {}
            d[error_list[0].tag] = error_list[0].text
            d[error_list[1].tag] = error_list[1].text
            plans_list.append(d)
    return plans_list, {}


def pre_transaction(transaction):
    product_type = get_product_type(transaction.extra['search_data'])
    plan_type = transaction.plan_details['plan_type']
    if product_type == 'Family':
        cover_type = transaction.extra['search_data']['cover_type'].split('+')
        members = [mem.strip().lower() for mem in cover_type]
    else:
        members = ['applicant']

    activity = transaction.activity
    activity_data = activity.cleaned_data

    max_age = reduce(lambda x, y: x if x > y else y, activity_data['ages'])

    transaction.extra['search_data']['dob'] = (datetime.datetime.today()-relativedelta(years=max_age)).strftime('%d-%b-%Y')
    transaction.plan_details['product_type'] = product_type
    transaction.plan_details['plan'] = '%s - %s' % (product_type, plan_type)
    transaction.plan_details['home_required'] = home_required(transaction)
    transaction.plan_details['members'] = members
    return None


def set_travel_policy_details(transaction):
    search_data = transaction.extra['search_data']
    multi_trip = (search_data['trip_type'] == 'multiple')
    product_type = transaction.plan_details['product_type']
    insured_details = transaction.insured_details_json

    if not multi_trip and search_data['traveller_type'] == 'Individual':
        visiting_usa = BOOL[search_data['region'] == 'World including USA & Canada']
    else:
        visiting_usa = BOOL[search_data['visiting_usa']]

    is_oci_or_nonoci = (search_data['indian_citizen'] == 'no') and (search_data['is_oci_or_nonoci'] == 'yes')
    is_oci = is_oci_or_nonoci and (search_data['oci_or_nonoci'] == 'OCI')
    is_nonoci = is_oci_or_nonoci and (search_data['oci_or_nonoci'] == 'NONOCI')

    if multi_trip:
        from_date = datetime.datetime.strptime(search_data['from_date'], travel_settings.DATE_FORMAT)
        to_date = from_date + relativedelta(days=-1, years=1)
        search_data['to_date'] = datetime.datetime.strftime(to_date, travel_settings.DATE_FORMAT)

    addon_benefits = BOOL[search_data['addon_benefits']] if search_data['traveller_type'] == 'Student' else ''

    if search_data['ped'] == 'None':
        ped = 'False'
    elif search_data['ped'] == 'Any Other':
        ped = search_data['ped']
    elif search_data['ped']:
        ped = 'True'
    else:
        ped = 'False'

    plan_type = transaction.plan_details['plan_type']

    self_age = 0
    spouse_age = 0

    for member in insured_details['members']:
        if member['relationship'].lower() == 'self':
            self_age = prod_utils.calculate_age(member['date_of_birth'])
        if member['relationship'].lower() == 'spouse':
            spouse_age = prod_utils.calculate_age(member['date_of_birth'])

    if spouse_age > self_age:
        date_of_birth = insured_details['members'][1]['date_of_birth']
    else:
        date_of_birth = insured_details['members'][0]['date_of_birth']

    objPremiumCalDC = OrderedDict([
        ('AddonBenefits', addon_benefits),
        ('CountryOfStay', search_data['country_of_stay'] if is_oci else ''),
        ('CoverType', search_data['cover_type'] if product_type == 'Family' else ''),
        ('DateOfBirth', datetime.datetime.strptime(date_of_birth, '%d-%m-%Y').strftime('%d-%b-%Y')),
        ('IsIndianCitizen', BOOL[search_data['indian_citizen']]),
        ('IsInsuredOnImmigrantVisa', '' if multi_trip else BOOL[search_data['immigration_visa']]),
        ('IsNONOCI', BOOL[is_nonoci]),
        ('IsOCI', BOOL[is_oci]),
        ('IsOCIorNONOCI', BOOL[is_oci_or_nonoci]),
        ('JourneyEndDate', '' if multi_trip else transaction.activity.policy_end_date.strftime('%d-%b-%Y')),
        ('JourneyStDate', transaction.activity.policy_start_date.strftime('%d-%b-%Y')),
        ('MaxNoofTravelDays', search_data['max_trip_length'] if multi_trip else ''),
        ('OCINumber', search_data['country_of_stay'] if is_oci else ''),
        ('PassportIssueCountry', search_data['passport_issuer'] if is_nonoci else ''),
        ('PlanType', transaction.plan_details['plan_type']),
        ('PremiumAmount', str(transaction.plan_details['premium'])),
        ('ProductType', product_type),
        ('SportsActivityEnvolved', BOOL[search_data['sports']]),
        ('SufferingAnyPreDisease', ped),
        ('VisitingUSA', visiting_usa),
    ])

    objTravelCoverageDC = OrderedDict([
        ("CompanyName", ""),
        ("DrFname", ""),
        ("PDuration", insured_details.get('PDuration')),
        ("PSponsorFName", ""),
        ("PTotalSem", ""),
        ("PTutionFee", ""),
        ("PUnivName", insured_details.get('PUnivName')),
        ("PlanType", transaction.plan_details['plan_type']),
        ("VisitingCountrys", ",".join([i.name for i in transaction.activity.cleaned_data['places']])),
        ("VisitingUSAC", visiting_usa)
    ])

    address_form = transaction.insured_details_json['address']
    proposer_data = [member for member in transaction.insured_details_json['members'] if member['relationship'] == 'self'][0]
    objTravelCommunicationAddrDC = OrderedDict([
        ("AddrType", "Communication"),
        ("Email", proposer_data['email']),
        ("Mobile", proposer_data['mobile']),
        ("ResAddr1", address_form['address_line_1']),
        ("ResAddr2", address_form['address_line_2']),
        ("ResAddr3", address_form['address_line_3']),
        ("ResCity", address_form['city']),
        ("ResCountry", 'India'),
        ("ResFax", ''),
        ("ResPinCode", address_form['pincode']),
        ("ResStat", address_form['state']),
        ("ResTel", '')
    ])
    objTravelHomeBurgalaryAddrDC = OrderedDict([
        ("AddrType", 'Home' if transaction.plan_details['home_required'] else ''),
        ("Email", proposer_data['email']),
        ("Mobile", proposer_data['mobile']),
        ("ResAddr1", address_form.get('home_address_line_1')),
        ("ResAddr2", address_form.get('home_address_line_2')),
        ("ResAddr3", address_form.get('home_address_line_3')),
        ("ResCity", address_form.get('home_city')),
        ("ResCountry", 'India'),
        ("ResFax", ''),
        ("ResPinCode", address_form.get('home_pincode')),
        ("ResStat", address_form.get('home_state')),
        ("ResTel", '')
    ])
    objTravelUniversityAddrDC = OrderedDict([
        ("AddrType", 'University' if product_type == 'Student' else ''),
        ("Email", insured_details.get('university_email', '')),
        ("Mobile", insured_details.get('university_mobile', '')),
        ("ResAddr1", insured_details.get('university_ResAddr1', '')),
        ("ResAddr2", insured_details.get('university_ResAddr2', '')),
        ("ResAddr3", insured_details.get('university_ResAddr3', '')),
        ("ResCity", insured_details.get('university_city', '')),
        ("ResCountry", insured_details.get('university_country', '')),
        ("ResFax", ''),
        ("ResPinCode", insured_details.get('university_pincode', '')),
        ("ResStat", insured_details.get('university_state', '')),
        ("ResTel", '')
    ])
    objUserDetails = OrderedDict([
        ("Password", PASSWORD),
        ("UserId", USERNAME)
    ])

    data_dict = {
        "objUserDetails": objUserDetails,
        "objPremiumCalDC": objPremiumCalDC,
        "objTravelCoverageDC": objTravelCoverageDC,
        "objTravelCommunicationAddrDC": objTravelCommunicationAddrDC,
        "objTravelHomeBurgalaryAddrDC": objTravelHomeBurgalaryAddrDC,
        'objTravelUniversityAddrDC': objTravelUniversityAddrDC
    }

    members = transaction.insured_details_json['members']
    tag = ""
    child_counter = 1
    for member in members:
        relation = RELATION_CHOICES[member['relationship'].lower()]
        if relation.lower() == 'self':
            tag = 'objApplicant' if(self_age >= spouse_age) else 'objApplicantSpouse'
        elif relation.lower() == 'spouse':
            tag = 'objApplicantSpouse' if(self_age >= spouse_age) else 'objApplicant'
        elif relation.lower() in ['dependent son', 'dependent daughter']:
            tag = 'objApplicantChild' + str(child_counter)
            child_counter += 1
        obj_tag = OrderedDict([
            ("DOB", datetime.datetime.strptime(member['date_of_birth'], '%d-%m-%Y').strftime('%d-%b-%Y')),
            ("FName", member['first_name']),
            ("Gender", member['gender'].upper()),
            ("IllnessDetails", ""),
            ("LName", member['last_name']),
            ("MName", member.get('middle_name', '')),
            ("NomineeFName", member['nominee_name']),
            ("NomineeRelation", member['nominee_relation']),
            ("Occupation", member['occupation']),
            ("Passport",   member['passport_no']),
            ("ProdCode", ""),
            ("Relationship", relation),
            ("Salute", SALUTATION_CHOICE[member['gender']]),
            ("SufferingSince",  ''),
            ("UnderMedication", '')
        ])
        data_dict[tag] = obj_tag
    method_tag_tuple = ("setTravelPolicyDetails", "http://tempuri.org/")
    proposal_request = create_xml_for_request(data_dict,  method_tag_tuple)

    http = httplib2.Http()
    headers = {
        "SOAPAction": "http://tempuri.org/IRGICLPortalTravel_WCF/setTravelPolicyDetails",
        "Content-Type": "text/xml"
    }
    travel_logger.info(
        '===================\nRELIANCE TRANSACTION: %s\nPROPOSAL REQUEST\n%s\n'
        % (transaction.transaction_id, proposal_request)
    )
    response_header,  proposal_response = http.request(URL,  method='POST',  body=proposal_request,  headers=headers)
    travel_logger.info(
        '===================\nRELIANCE TRANSACTION: %s\nPROPOSAL RESPONSE\n%s\n'
        % (transaction.transaction_id, proposal_response)
    )

    return proposal_request, proposal_response


def finalize_transaction(transaction):
    members = transaction.insured_details_json['members']

    cover_type = transaction.extra['search_data']['cover_type']
    for member in members:
        if(member['relationship'] == 'self'):
            dob_str = datetime.datetime.strptime(member['date_of_birth'], '%d-%m-%Y').strftime('%d-%b-%Y')
            transaction.extra['search_data']['dob'] = dob_str
            transaction.save()
        if(member['relationship'] in ['Dependent Son', 'Dependent Daughter'] and cover_type == 'Applicant + Spouse'):
            transaction.extra['search_data']['cover_type'] = 'Applicant + Child1'
            transaction.save()


def post_transaction(transaction):
    proposal_response = None
    proposal_request = None
    template_name = None
    try:
        proposal_request, proposal_response = set_travel_policy_details(transaction)
        tree = etree.fromstring(proposal_response)
        root = tree.getroottree().getroot()
        proposal_detail_list = root[0][0][0].getchildren()
        err_description = proposal_detail_list[1].getchildren()[1].getchildren()

        if(err_description):
            raise Exception(
                "%s: %s" % (
                    err_description[0].getchildren()[0].getchildren()[0].text,
                    err_description[0].getchildren()[0].getchildren()[1].text
                )
            )
        proposal_dict = {}
        for each in proposal_detail_list:
            proposal_dict[each.tag.split('}')[1]] = each.text

        resp_url = '%s/travel-insurance/reliance/transaction/%s/response/' % (settings.SITE_URL, transaction.transaction_id)
        data = {'Message': proposal_dict['ProposalNo'] + '|' + USERNAME + '|' + resp_url + '|' + proposal_dict['TokenNo']}
        transaction.payment_request = json.dumps(data)
        transaction.status = transaction.PENDING
        transaction.save()
        travel_logger.info(
            '===================\nRELIANCE TRANSACTION: %s\nPAYMENT REQUEST\n%s\n'
            % (transaction.transaction_id, data)
        )
        redirect_url = PAYMENT_URL
        template_name = None
        is_redirect = False
    except Exception as e:
        travel_logger.info(
            "\n RELIANCE TRANSACTION: %s\n EXCEPTION: \n%s\n"
            % (transaction.transaction_id, traceback.format_exc())
        )
        data = {
            'stack_trace': traceback.format_exc(),
            'error_message': e.message,
            'error_response': proposal_response,
        }
        redirect_url = "/travel-insurance/transaction/%s/failure/" % (transaction.transaction_id)
        is_redirect = True
    return is_redirect, redirect_url, template_name, data, proposal_request, proposal_response


def post_payment(payment_response, transaction):
    # Status|PolicyNo|ErrorCode|ErrorMessage|ScheduleURL|OtherInput
    travel_logger.info(
        '===================\nRELIANCE TRANSACTION: %s\nPAYMENT RESPONSE\n%s\n'
        % (transaction.transaction_id, payment_response)
    )
    transaction.payment_response = payment_response
    resp = payment_response['Message'].split('|')

    transaction.extra['payment_response'] = {
        'status': resp[0],
        'policy_no': resp[1],
        'insurer_policy_url': resp[4],
        'error_code': resp[2],
        'error_message': resp[3]
    }
    post_data = {}

    if transaction.extra['payment_response']['status'] == 'SUCCESS':
        transaction.extra['error_message'] = ''
        transaction.policy_number = transaction.extra['payment_response']['policy_no']
        transaction.payment_done = True
        transaction.payment_on = datetime.datetime.now()

        try:
            pdf_url = transaction.extra['payment_response']['insurer_policy_url']
            pdf_doc = requests.get(pdf_url, verify=False)
            transaction.update_policy(pdf_doc.content, send_mail=False)
        except Exception as e:
            travel_logger.info('\nRELIANCE INTEGRATION: %s\n POLICY PDF EXCEPTION:\n %s\n' % (transaction.transaction_id, e))

        transaction.status = transaction.COMPLETED
    else:
        transaction.extra['error_message'] = transaction.extra['payment_response']['error_message']
        transaction.status = transaction.FAILED
        post_data = {
            'stack_trace': '',
            'error_message': transaction.extra['error_message'],
            'error_response': payment_response,
        }
    transaction.save()
    return transaction, transaction.status == transaction.COMPLETED, post_data
