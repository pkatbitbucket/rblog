import json
from travel_product.insurers import common_preset

OCCUPATIONS = common_preset.OCCUPATIONS;

STATES = common_preset.STATES;

CITIES = common_preset.CITIES;

NOMINEE_RELATIONS = [
        {'key':"Spouse",'value':"SPOUSE"},
        {'key':"Son",'value':"SON"},
        {'key':"Daughter",'value':"DAUGHTER"},
]

RELATIONS = list(NOMINEE_RELATIONS);
RELATIONS.insert(0,{'key':"Self- Primary Member",'value':"SELF- PRIMARY MEMBER"})

SALUTATIONS = [
    {'key':'Mr.','value':'MR'},
    {'key':'Mrs.','value':'MS'},
    {'key':'Ms.','value':'MS'},
]

GENDER =[
        {'key':'Male','value':'MALE'},
        {'key':'Female','value':'FEMALE'},
        ]

def get_form(request, transaction):
    members = transaction.plan_details['members']
    return {
        'num_insured': len(members),
        'members_list': members,
        'occupations_list': OCCUPATIONS,
        'relations_list': RELATIONS,
        'nominee_relations_list' : NOMINEE_RELATIONS,
        'gender_list': GENDER,
        'salutations_list': SALUTATIONS,
        'states_list': STATES,
        'cities_list': CITIES,
    }
