from suds.client import Client
import traceback
import os
import xml.etree.ElementTree as ET
import base64
import time
from settings import SETTINGS_FILE_FOLDER
from django.conf import settings
from django.core.files.base import File
import logging
import travel_product.settings as travel_settings
from utils import travel_logger
from datetime import datetime, date
from dateutil.relativedelta import *
import copy
from suds.plugin import MessagePlugin
from travel_product import prod_utils

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)

if settings.PRODUCTION:
    # ============ PRODUCTION CREDENTIALS =================
    RELIGARE_WSDL_FILE = 'file://%s' % (os.path.join(SETTINGS_FILE_FOLDER,
                                                     'travel_product/insurers/religare/webservices/uat/ReligareTravel.wsdl'))
    ENDPOINT_URL = 'https://religarehealthinsurance.com/relinterface/services/'\
                   'RelSymbiosysServices.RelSymbiosysServicesHttpSoap12Endpoint/'
    USERNAME = 'serviceClient'
    PASSWORD = 'weX46J'
    agent_id = '20012981'
    PAYMENT_URL = 'https://religarehealthinsurance.com/portalui/PortalPayment.run'
    PDF_WSDL_PATH = 'http://203.160.138.164/cordys/WSDLGateway.wcp?service=http%3A%2F%2Fweb.com%2F/'\
                    'GET_PDF&organization=o%3DReligareHealth%2Ccn%3Dcordys%2Ccn%3DdevInst%2Co%3Dreligare.in&version=isv'
    PDF_USERNAME = 'SymbiosysUser'
    PDF_PASSWORD = 'Password-1'
else:
    # ============ TEST CREDENTIALS =================
    RELIGARE_WSDL_FILE = 'file://%s' % (os.path.join(SETTINGS_FILE_FOLDER,
                                                     'travel_product/insurers/religare/webservices/uat/ReligareTravel.wsdl'))
    ENDPOINT_URL = 'https://rhicluat.religare.com/relinterface/services/'\
                   'RelSymbiosysServices.RelSymbiosysServicesHttpSoap12Endpoint/'
    USERNAME = 'serviceClient'
    PASSWORD = 'weX46J'
    agent_id = '20008802'
    PREMIUM_SHEET_PATH = os.path.join(
        SETTINGS_FILE_FOLDER,
        'travel_product/insurers/religare/premium_sheet/premium_sheet_latest.xls'
    )
    PAYMENT_URL = 'https://rhicluat.religare.com/portalui/PortalPayment.run'
    PDF_WSDL_PATH = 'http://203.160.138.164/cordys/WSDLGateway.wcp?service=http%3A%2F%2Fweb.com%2F/'\
                    'GET_PDF&organization=o%3DReligareHealth%2Ccn%3Dcordys%2Ccn%3DdevInst%2Co%3Dreligare.in&version=isv'
    PDF_USERNAME = 'SymbiosysUser'
    PDF_PASSWORD = 'Password-1'


TRIP_TYPE_DICT = {
    'single': 'Single',
    'multiple': 'Multi'
}

TRIP_TYPE_DICT_PROPOSAL = {
    'single': 'SINGLE',
    'multiple': 'MULTI'
    }

SALUTE_CHOICE = {
    'm': 'MR',
    'f': 'MS'
}

GENDER_CHOICE = {
    'm': 'MALE',
    'f': 'FEMALE'
    }

YES_NO_DIC = {'yes': 'YES',
              'no': 'NO',
              'y': 'YES',
              'n': 'N',
              'YES': 'YES',
              'NO': 'NO',
              'Y': 'YES',
              'N': 'NO',
              'true': 'YES',
              'false': 'NO',
              '0': 'NO',
              'on': 'YES'}

PRODUCT_CODE_DICT_SINGLE = {
    'Explore - Asia': 40001001,
    'Explore - Africa': 40001002,
    'Explore - Canada': 40001004,
    'Explore - Gold - Worldwide - Excluding US & Canada': 40001009,
    'Explore - Gold - Worldwide': 40001011,
    'Explore - Platinum - Worldwide - Excluding US & Canada': 40001010,
    'Explore - Platinum - Worldwide': 40001012,
    'Explore - Europe': 40001015
}

PRODUCT_CODE_DICT_MULTI = {'Explore - Gold - Worldwide - Excluding US & Canada': 40001005,
                           'Explore - Gold - Worldwide': 40001007,
                           'Explore - Platinum - Worldwide - Excluding US & Canada': 40001006,
                           'Explore - Platinum - Worldwide': 40001008}

RELATION_DICT = {"MOTHER": "MOTH",
                 "UNCLE": "MUNC",
                 "NEW BORN BABY": "NBON",
                 "NEPHEW": "NEPH",
                 "NIECE": "NIEC",
                 "PATERNAL AUNT": "PANT",
                 "Paternal Uncle": "PUNC",
                 "SELF- PRIMARY MEMBER": "SELF",
                 "SISTER": "SIST",
                 "SON IN LAW": "SLAW",
                 "SON": "SONM",
                 "SPOUSE": "SPSE",
                 "KARTA": "KRTA",
                 "COPARCENER": "CPAR",
                 "DAUGHTER": "UDTR"}

ADDRESS_TYPES = ['PERMANENT', 'COMMUNICATION']

QUESTION_DICT = {
    "ped": {"name": "pedYesNoTravel", "code": "pedYesNo"},
    "liver_disease": {"name": "PEDliverDetailsTravel", "code": "128"},
    "cancer_disease": {"name": "PEDcancerDetailsTravel", "code": "114"},
    "heart_disease": {"name": "PEDHealthDiseaseTravel", "code": "143"},
    "kidney_disease": {"name": "PEDkidneyDetailsTravel", "code": "129"},
    "paralysis": {"name": "PEDparalysisDetailsTravel", "code": "164"},
    "other_disease": {"name": "PEDotherDetailsTravel", "code": "210"},
    "other_disease_name": {"name": "other_disease_desc", "code": "otherDiseasesDescription"},
}

SUM_INSURED_CODE = {40001002: {25000: '001', 50000: '002', 100000: '003'},
                    40001003: {50000: '001', 100000: '002'},
                    40001004: {50000: '001', 100000: '002'},
                    40001005: {50000: '001', 100000: '002', 300000: '003', 500000: '004'},
                    40001006: {50000: '001', 100000: '002', 300000: '003', 500000: '004'},
                    40001007: {50000: '001', 100000: '002', 300000: '003', 500000: '004'},
                    40001008: {50000: '001', 100000: '002', 300000: '003', 500000: '004'},
                    40001009: {50000: '001', 100000: '002', 300000: '003', 500000: '004'},
                    40001010: {50000: '001', 100000: '002', 300000: '003', 500000: '004'},
                    40001011: {50000: '001', 100000: '002', 300000: '003', 500000: '004'},
                    40001012: {50000: '001', 100000: '002', 300000: '003', 500000: '004'},
                    40001013: {25000: '001', 50000: '002', 100000: '003'},
                    40001014: {25000: '001', 50000: '002', 100000: '003'},
                    40001015: {30000: '001', 100000: '002'},
                    40001016: {25000: '001', 50000: '002'},
                    40001001: {25000: '001', 50000: '002', 100000: '003'}}

DISCOUNT_DICT = {1: 0, 2: 5, 3: 10, 4: 15, 5: 17.5, 6: 20}


class _AttributePlugin(MessagePlugin):
    """
    Suds plug-in extending the method call with arbitrary attributes.
    """
    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def marshalled(self, context):
        children = context.envelope.getChildren()
        header = children[0]
        ebody = children[1]
        children.remove(header)
        ebody.prefix = "SOAP-ENV"


def get_sum_insured_in_int(sum_insured):
    sum_list = sum_insured.split(' ')
    if 'US' in sum_list:
        return int(''.join(sum_list[2].split(',')))
    else:
        return int(''.join(sum_list[1].split(',')))


def calculate_age_band(born):
    today = date.today()
    age = today.year - born.year - ((today.month, today.day) < (born.month, born.day))
    if age in range(0, 41):
        return 'up to 40 Years'
    elif age in range(41, 61):
        return '41 Years - 60 Years'
    elif age in range(61, 71):
        return '61  Years - 70 Years'
    else:
        return '> 70 Years'


def get_formated_date(date, date_format=None):
    if date_format:
        date_format = date_format
    else:
        date_format = travel_settings.DATE_FORMAT
    if date:
        return datetime.strptime(date, date_format).strftime('%d/%m/%Y')
    else:
        return ''


def calcalte_term(search_data):
    '''
    Return term between two dates
    '''
    if search_data.get('max_trip_length'):
        term = int(search_data.get('max_trip_length'))
    else:
        from_date_obj = datetime.strptime(search_data['from_date'], travel_settings.DATE_FORMAT)
        to_date_obj = datetime.strptime(search_data['to_date'], travel_settings.DATE_FORMAT)
        term = (to_date_obj - from_date_obj).days + 1
        return term


def calcalte_premium(age_band, plan, sum_insured, trip_type, term):
    '''
    Return premium from the sheet
    '''
    cover_type = 'Individual'  # Hard code
    premium_list = df[(df['AgeBand'] == age_band) &
                      (df['Plan'] == plan) &
                      (df['SumInsured'] == sum_insured) &
                      (df['TripType'] == trip_type) &
                      (df['CoverType'] == cover_type) &
                      (df['Term'] == term)]

    return premium_list.Premium.tolist()[0]


def get_quote(activity):
    search_data = activity.search_data
    # calculate term
    term = calcalte_term(search_data)

    premium = 0
    for i in range(1, int(search_data['no_of_travellers'])+1):
        age_band = search_data['member'+str(i)+'_age_band']
        plan = search_data['plan']
        sum_insured = search_data['sum_insured']
        trip_type = TRIP_TYPE_DICT[search_data['trip_type']]
        try:
            premium = premium + calcalte_premium(age_band, plan, sum_insured, trip_type, term)
        except IndexError:
            raise Exception('Invalid Data')

    discount = premium * DISCOUNT_DICT[int(search_data['no_of_travellers'])] / 100
    final_premium = round(premium - discount)

    plans = []
    plan = {
        'plan_type': search_data['plan'],
        'premium': final_premium,
        'sum_assured': search_data['sum_insured']
    }

    plans.append(plan)
    return plans, None


def create_policy(transaction):
    '''
    create proposal request and return the response
    '''
    transaction_id = transaction.transaction_id
    search_data = transaction.extra['search_data']
    plan_details = transaction.plan_details
    insured_details = transaction.insured_details_json
    cover_type = 'INDIVIDUAL'  # fixed

    if search_data['trip_type'] == 'single':
        product_code = PRODUCT_CODE_DICT_SINGLE[search_data['plan']]
        term = calcalte_term(search_data)
    else:
        product_code = PRODUCT_CODE_DICT_MULTI[search_data['plan']]
        term = 366

    client = Client(RELIGARE_WSDL_FILE, username=USERNAME, password=PASSWORD)
    client.set_options(port='RelSymbiosysServicesHttpSoap11Endpoint')

    # CREATE POLICY
    # ######### IntPolicyDataIO #################
    IntPolicyDataIO = client.factory.create('ns5:IntPolicyDataIO')

    # IntPolicyDataIO.errorLists = ''
    # IntPolicyDataIO.listErrorListList = ''

    # POLICY DATA
    # ############### IntPolicyDO ##########
    IntPolicyDO = client.factory.create('ns5:IntPolicyDO')

    # IntPolicyDO.addOns = insured_details['add_on']
    IntPolicyDO.baseAgentId = agent_id
    IntPolicyDO.baseProductId = product_code
    IntPolicyDO.businessTypeCd = "NEWBUSINESS"
    IntPolicyDO.coverType = cover_type
    IntPolicyDO.isPremiumCalculation = 'YES'
    IntPolicyDO.maxTripPeriod = search_data.get('max_trip_length', '')
    IntPolicyDO.nomineeDob = ''
    IntPolicyDO.nomineeName = insured_details['members'][0].get('nominee_name', '')
    IntPolicyDO.nomineeRelationship = insured_details['members'][0].get('nominee_relationship', '')
    IntPolicyDO.occupation = insured_details['members'][0].get('occupation', '')

    # partyDOList[] = <empty>  # Append InpartyDO
    # policyAdditionalFieldsDOList[] = <empty>

    IntPolicyDO.policyCommencementDt = transaction.activity.policy_start_date.strftime("%d/%m/%Y")
    IntPolicyDO.policyMaturityDt = transaction.activity.policy_end_date.strftime("%d/%m/%Y")
    IntPolicyDO.policyNum = insured_details.get('policy_num', '')
    IntPolicyDO.premium = insured_details.get('premium', '')
    IntPolicyDO.proposalNum = insured_details.get('propsal_num')
    IntPolicyDO.quotationReferenceNum = insured_details.get('quote_ref', '')
    IntPolicyDO.sumInsured = SUM_INSURED_CODE[product_code][get_sum_insured_in_int(search_data['sum_insured'])]
    IntPolicyDO.term = term
    IntPolicyDO.travelGeographyCd = product_code
    IntPolicyDO.tripTypeCd = TRIP_TYPE_DICT_PROPOSAL[search_data['trip_type']]
    IntPolicyDO.uwDecisionCd = insured_details.get('decision_cd', '')

    # INSURED MEMBERS
    # ############### IntPartyDO ##########
    for i, member in enumerate(insured_details['members']):
        IntPartyDO = client.factory.create('ns5:IntPartyDO')
        IntPartyDO.relationCd = 'SELF'
        IntPartyDO.roleCd = 'PRIMARY'

        IntPartyDO.birthDt = get_formated_date(member['date_of_birth'], '%d-%m-%Y')
        IntPartyDO.firstName = member['first_name']
        IntPartyDO.lastName = member['last_name']
        IntPartyDO.genderCd = GENDER_CHOICE[member['gender']]
        IntPartyDO.guid = i + 1

        # partyAddressDOList[] = <empty>
        # partyContactDOList[] = <empty>
        # partyEmailDOList[] = <empty>
        # partyEmploymentDOList[] = <empty>
        # partyIdentityDOList[] = <empty>
        # partyQuestionDOList[] = <empty>

        IntPartyDO.titleCd = SALUTE_CHOICE[member['gender']]  # confirm with suhash

        # ADDRESS
        # ############### IntPartyAddressDO ##########
        for address in ADDRESS_TYPES:
            if insured_details['address']['pincode'] == '400072':
                pincode = '400076'
            else:
                pincode = insured_details['address']['pincode']
            IntPartyAddressDO = client.factory.create('ns5:IntPartyAddressDO')
            IntPartyAddressDO.addressLine1Lang1 = insured_details['address']['address_line_1']
            IntPartyAddressDO.addressLine2Lang1 = insured_details['address']['address_line_2']
            IntPartyAddressDO.addressTypeCd = address
            IntPartyAddressDO.areaCd = insured_details['address']['city']
            IntPartyAddressDO.cityCd = insured_details['address']['city']
            IntPartyAddressDO.pinCode = pincode
            IntPartyAddressDO.stateCd = insured_details['address']['state']
            IntPartyDO.partyAddressDOList.append(IntPartyAddressDO)  # added address

        # CONTACT
        # ############### IntPartyContactDO ##########
        IntPartyContactDO = client.factory.create('ns5:IntPartyContactDO')
        IntPartyContactDO.contactNum = member['mobile']
        IntPartyContactDO.contactTypeCd = "MOBILE"  # hard code
        IntPartyContactDO.stdCode = member.get('std_code', '')
        IntPartyDO.partyContactDOList.append(IntPartyContactDO)  # added contact

        # EMAIL
        # ############### IntPartyEmailDO ##########
        IntPartyEmailDO = client.factory.create('ns5:IntPartyEmailDO')
        IntPartyEmailDO.emailAddress = member['email']
        IntPartyEmailDO.emailTypeCd = "PERSONAL"  # hard code
        IntPartyDO.partyEmailDOList.append(IntPartyEmailDO)

        # OCCUPATION
        # ############### IntPartyEmploymentDO ##########
        IntPartyEmploymentDO = client.factory.create('ns5:IntPartyEmploymentDO')
        IntPartyEmploymentDO.occupationCd = member.get('occupation_cd')
        IntPartyDO.partyEmploymentDOList.append(IntPartyEmploymentDO)

        # IDENTITY
        # ############### IntPartyIdentityDO ##########
        IntPartyIdentityDO = client.factory.create('ns5:IntPartyIdentityDO')
        IntPartyIdentityDO.identityNum = member.get('passport_no', '')
        IntPartyIdentityDO.identityTypeCd = "PASSPORT"  # hard code
        IntPartyDO.partyIdentityDOList.append(IntPartyIdentityDO)

        # QUESTION
        # ############### IntPartyQuestionDO ##########
        if member['ped'] == 'yes':
            for question_set in QUESTION_DICT:
                IntPartyQuestionDO = client.factory.create('ns5:IntPartyQuestionDO')
                IntPartyQuestionDO.questionCd = QUESTION_DICT[question_set]["code"]
                IntPartyQuestionDO.questionSetCd = QUESTION_DICT[question_set]["name"]
                if question_set == 'other_disease_name':
                    IntPartyQuestionDO.response = member.get(question_set)
                    IntPartyQuestionDO.questionSetCd = QUESTION_DICT[question_set]["name"]
                    IntPartyDO.partyQuestionDOList.append(IntPartyQuestionDO)
                else:
                    IntPartyQuestionDO.response = YES_NO_DIC[member.get(question_set, 'NO')]
                    IntPartyDO.partyQuestionDOList.append(IntPartyQuestionDO)
        else:
            IntPartyQuestionDO = client.factory.create('ns5:IntPartyQuestionDO')
            IntPartyQuestionDO.questionCd = QUESTION_DICT['ped']['code']
            IntPartyQuestionDO.questionSetCd = QUESTION_DICT['ped']['name']
            IntPartyQuestionDO.response = YES_NO_DIC[member.get('ped', 'NO')]
            IntPartyDO.partyQuestionDOList.append(IntPartyQuestionDO)

        IntPartyQuestionDO = client.factory.create('ns5:IntPartyQuestionDO')
        IntPartyQuestionDO.questionCd = 'T001'
        IntPartyQuestionDO.questionSetCd = 'HEDTravelHospitalized'
        IntPartyQuestionDO.response = YES_NO_DIC[member.get('hospitalized', 'NO')]
        IntPartyDO.partyQuestionDOList.append(IntPartyQuestionDO)

        IntPartyQuestionDO = client.factory.create('ns5:IntPartyQuestionDO')
        IntPartyQuestionDO.questionCd = 'T002'
        IntPartyQuestionDO.questionSetCd = 'HEDTravelClaimPolicy'
        IntPartyQuestionDO.response = YES_NO_DIC[member.get('claimed_policy', 'NO')]
        IntPartyDO.partyQuestionDOList.append(IntPartyQuestionDO)

        IntPolicyDO.partyDOList.append(IntPartyDO)

    # ADDITIONAL FIELDSC
    # ############### IntPolicyAdditionalFieldsDO ##########

    IntPolicyAdditionalFieldsDO = client.factory.create('ns5:IntPolicyAdditionalFieldsDO')
    IntPolicyAdditionalFieldsDO.field1 = None
    IntPolicyAdditionalFieldsDO.field10 = insured_details['members'][0].get('nominee_name', '')
    IntPolicyAdditionalFieldsDO.field12 = None
    IntPolicyAdditionalFieldsDO.field17 = None
    IntPolicyAdditionalFieldsDO.field2 = None
    IntPolicyAdditionalFieldsDO.field20 = len(insured_details['members'])
    IntPolicyAdditionalFieldsDO.field3 = None
    # to-do in html and js
    IntPolicyAdditionalFieldsDO.fieldAgree = YES_NO_DIC[insured_details.get('tc_agree', 'YES')]
    IntPolicyAdditionalFieldsDO.fieldAlerts = YES_NO_DIC[insured_details.get('alert', 'YES')]
    IntPolicyAdditionalFieldsDO.fieldTc = YES_NO_DIC[insured_details.get('tc_agree', 'YES')]
    IntPolicyAdditionalFieldsDO.tripStart = YES_NO_DIC[insured_details.get('trip_start', 'YES')]

    IntPolicyDO.policyAdditionalFieldsDOList.append(IntPolicyAdditionalFieldsDO)
    IntPolicyDataIO.policy = IntPolicyDO

    # Added PROPOSER to member list
    copied_member = None
    for member in IntPolicyDataIO.policy.partyDOList:
        if member.relationCd == "SELF":
            member.firstName = insured_details['members'][0]['first_name']
            member.lastName = insured_details['members'][0]['last_name']
            member.birthDt = get_formated_date(insured_details['members'][0]['date_of_birth'], '%d-%m-%Y')
            copied_member = copy.deepcopy(member)
            copied_member.roleCd = "PROPOSER"

    if not copied_member:
        copied_member = copy.deepcopy(IntPolicyDataIO.policy.partyDOList[0])
        copied_member.birthDt = get_formated_date(insured_details['members'][0]['date_of_birth'], '%d-%m-%Y')
        copied_member.firstName = insured_details['members'][0]['first_name']
        copied_member.lastName = insured_details['members'][0]['last_name']
        copied_member.genderCd = GENDER_CHOICE[insured_details['members'][0]['gender']]
        copied_member.titleCd = SALUTE_CHOICE[insured_details['members'][0]['gender']]  # confirm with suhash
        copied_member.guid = len(insured_details['members']) + 1
        copied_member.relationCd = "SELF"
        copied_member.roleCd = 'PROPOSER'

    # Inserted the proposer on the first in the list
    IntPolicyDataIO.policy.partyDOList.insert(0, copied_member)
    # IntPolicyDataIO.policy.partyDOList.append(copied_member)

    cp = client.service.createPolicyTravel
    cp.method.location = ENDPOINT_URL
    proposal_response = cp(IntPolicyDataIO)
    proposal_request = client.last_sent()

    travel_logger.info('\nRELIGARE TRANSACTION: %s \n PROPOSAL REQUEST SENT:\n %s \n' % (transaction_id, proposal_request))
    travel_logger.info(
        '\nRELIGARE TRANSACTION: %s \n PROPOSAL RESPONSE RECEIVED:\n %s \n'
        % (transaction_id, proposal_response)
    )
    return proposal_request, proposal_response


def pre_transaction(transaction):
    search_data = transaction.extra['search_data']
    no_of_traveller = search_data.get('no_of_travellers', 1)
    members = []
    for i in range(1, int(no_of_traveller)+1):
        members.append('member' + str(i))
    transaction.plan_details['members'] = members
    return None


def post_transaction(transaction):
    template_name = None
    proposal_request = None
    proposal_response = None
    try:
        proposal_request, proposal_response = create_policy(transaction)
        resp_data = {}
        resp_data['proposalNum'] = proposal_response['int-policy-data-iO'].policy['proposal-num']
        resp_data['premium'] = proposal_response['int-policy-data-iO'].policy['premium']
        resp_data['returnURL'] = (
            '%s/travel-insurance/religare/transaction/%s/response/'
            % (settings.SITE_URL, transaction.transaction_id)
            )
        transaction.status = transaction.PENDING
        transaction.save()
        travel_logger.info(
            '===================\nRELIGARE TRANSACTION: %s\nPAYMENT REQUEST\n%s\n'
            % (transaction.transaction_id, resp_data)
        )
        redirect_url = PAYMENT_URL
        is_redirect = False
    except Exception as e:
        travel_logger.info('\nRELIGARE TRANSACTION: %s\nEXCEPTION: \n%s\n' % (transaction.transaction_id, traceback.format_exc()))
        resp_data = {
            'stack_trace': traceback.format_exc(),
            'error_message': e.message,
            'error_response': proposal_response,
        }
        is_redirect = True
        redirect_url = "/travel-insurance/transaction/%s/failure/" % (transaction.transaction_id)
    return is_redirect, redirect_url, template_name, resp_data, proposal_request, proposal_response


def post_payment(payment_response, transaction):
    # {u'transactionRefNum': u'403993715509148194', u'policyNumber': u'', u'uwDecision': u'DRAFT',
    # u'errorFlag': u'', u'errorMsg': u''}
    resp = payment_response

    if 'errorMsg' in resp:
        transaction.extra['error_msg'] = resp['errorMsg']

    transaction.extra['payment_response'] = resp
    transaction.payment_response = resp
    transaction.policy_number = resp['policyNumber']
    transaction.policy_start_date = transaction.activity.policy_start_date
    transaction.policy_end_date = transaction.activity.policy_end_date
    transaction.save()
    travel_logger.info(
        '===================\nRELIGARE TRANSACTION: %s\nPAYMENT RESPONSE\n%s\n'
        % (transaction.transaction_id, resp)
    )
    data = {}
    if not transaction.extra['payment_response']['errorFlag']:
        transaction.payment_done = True
        transaction.payment_on = datetime.now()
        # PDF FETCHING
        try:
            time.sleep(3)
            client = Client(PDF_WSDL_PATH, username=PDF_USERNAME, password=PDF_PASSWORD)
            ltype = 'POLSCHD'
            client.options.plugins = [_AttributePlugin()]
            resp = client.service.GET_PDF(transaction.policy_number, ltype)
            travel_logger.info('\nRELIGARE TRANSACTION: %s \n PDF REQUEST SENT:\n %s \n'
                               % (transaction.transaction_id, client.last_sent()))
            # travel_logger.info('\nRELIGARE TRANSACTION: %s \n PDF RESPONSE RECEIVED:\n %s \n'
            #                    % (transaction.transaction_id, client.last_received()))
            xresp = ET.fromstring('<root>' + resp + '</root>')
            pdf_data = xresp.find('StreamData').text
            data = base64.b64decode(pdf_data)

            # WRITE THE PDF RESPONSE TO A LOCAL FILE
            transaction.update_policy(data, send_mail=False)

        except Exception as e:
            travel_logger.info(
                '===================\nRELIGARE TRANSACTION: %s\nPDF EXCEPTION\n%s\n'
                % (transaction.transaction_id, e.message)
            )

        transaction.status = transaction.COMPLETED
    else:
        transaction.status = transaction.FAILED
        data = {
                'stack_trace': "",
                'error_message': payment_response['errorMsg'],
                'error_response': payment_response,
        }
    transaction.save()
    return transaction, transaction.status == transaction.COMPLETED, data
