INSURER_SLUG_MAP = {
    'bharti-axa': 'bharti_axa',
    'reliance': 'reliance',
    'hdfc-ergo': 'hdfc_ergo',
    'bajaj-allianz': 'bajaj',
    'religare': 'religare',
    'future-generali': 'future_generali',
    'star-health': 'star',
    'apollo-munich': 'apollo_munich',
    'universal-sompo': 'universal_sompo',
    'tata-aig':'tata_aig',
}
