import os
import json
import requests
import urllib2
import time
import datetime

from django.conf import settings
from django.core.files.base import File

from utils import travel_logger
from travel_product import prod_utils
from travel_product.models import Transaction

if settings.PRODUCTION:
    # ===========PRODUCTION CREDENTIAL================
    URL = 'http://ig.nallantech.com/api/policy/proposals'
    STARHEALTH_API_KEY = '8b310f8129564357a9885eaa66043c3f'
    STARHEALTH_SECRET_KEY = '6383a11e284c4e08a12e6c1f77f88c24'
    PAYMENT_URL = 'http://ig.nallantech.com/policy/proposals/purchase/'
else:
    # =============UAT CREDENTIALS=================
    URL = "http://sandbox.ig.nallantech.com:9000/api/policy/proposals"
    STARHEALTH_API_KEY = 'fe6f6c4068bc4824b981a1f1e3f7e515'
    STARHEALTH_SECRET_KEY = '2353ba6ad9514067a82fc38666cb4540'
    PAYMENT_URL = "http://sandbox.ig.nallantech.com:9000/policy/proposals/purchase/"


def get_formattedd_insured_details_json(transaction):

    insured_details = transaction.insured_details_json
    plan__details = transaction.plan_details
    data= {}
    data["assignee_name"] = insured_details["members"][0]["cust_assigneeName"],
    data["assignee_relationship_id"] =  insured_details["members"][0]["cust_assigneeRelationshipId"],
    data["critical_cancer"] =  insured_details["members"][0]["critical_cancer"],
    data["critical_drugs"] =  insured_details["members"][0]["cust_criticalDrugs"],
    data["critical_heart"] =  insured_details["members"][0]["cust_criticalHeart"],
    data["critical_psychiatric"] =  insured_details["members"][0]["cust_criticalPsychiatric"],
    data["critical_renal"] =  insured_details["members"][0]["cust_criticalRenal"],
    data["dob"] =  insured_details["members"][0]["cust_dob"],
    data["illness"] =  insured_details["members"][0]["cust_illness"],
    data["name"] =  insured_details["members"][0]["cust_name"],
    data["overseas_contact_address"] =  insured_details["members"][0]["cust_overseasContactAddress"],
    data["overseas_contact_number"] =  insured_details["members"][0]["cust_overseasContactNumber"],
    data["passport_expiry"] =  insured_details["members"][0]["cust_passportExpiry"],
    data["passport_number"] =  insured_details["members"][0]["cust_passportNumber"],
    data["sex"] =  insured_details["members"][0]["cust_sex"],
    data["visa_type"] =  insured_details["members"][0]["cust_visaType"],
    data["plan_id"] =  plan_details["plan_id"],
    data["policy_type_name"] =  plan_details["policy_type"],
    data["proposer_address_one"] =  insured_details["contact"]['com_address_line_1'],
    data["proposer_address_two"] =  insured_details["contact"]["com_address_line_2"],
    data["proposerAreaId"] =  insured_details["proposer_area_id"],
    data["proposer_dob"] =  insured_details["proposer"][data["dob"]],
    data["proposer_email"] =   insured_details["contact"]["com_email"],
    data["proposer_name"] =  insured_details["proposer"][data["name"]],
    data["proposer_phone"] =  insured_details["contact"]["com_mobile"],
    data["travelDangerousAreas"] =  insured_details["travel_dangerous_areas"],
    data["travel_end_on"] =  plan_details["to_date"],
    data["travel_purpose_id"] =  insured_details["travel_purpose_id"],
    data["travel_start_on"] =  plan_details["from_date"],
    data["travel_valid_passport"] =  insured_details["travel_valid_passport"]

    transaction.insured_details_json = data
    transaction.save()


def create_proposal(transaction):
    insured_details = transaction.insured_details_json
    from IPython.frontend.terminal.embed import InteractiveShellEmbed
    InteractiveShellEmbed()()

    data = {
    "APIKEY": STARHEALTH_API_KEY,
    "SECRETKEY": STARHEALTH_SECRET_KEY,
    "insureds[0]": {
        "assigneeName": insured_details["assignee_name"],
        "assigneeRelationshipId": insured_details["assignee_relationship_id"],
        "criticalCancer": insured_details["critical_cancer"],
        "criticalDrugs": insured_details["critical_drugs"],
        "criticalHeart": insured_details["critical_heart"],
        "criticalPsychiatric": insured_details["critical_psychiatric"],
        "criticalRenal": insured_details["critical_renal"],
        "dob": insured_details["dob"],
        "illness": insured_details["illness"],
        "name": insured_details["name"],
        "overseasContactAddress": insured_details["overseas_contact_address"],
        "overseasContactNumber": insured_details["overseas_contact_number"],
        "passportExpiry": insured_details["passport_expiry"],
        "passportNumber": insured_details["passport_number"],
        "sex": insured_details["sex"],
        "visaType": insured_details["visa_type"]
    },
    "planId": insured_details["plan_id"],
    "policyTypeName": insured_details["policy_type_name"],
    "proposerAddressOne": insured_details["proposer_address_one"],
    "proposerAddressTwo": insured_details["proposer_address_two"],
    "proposerAreaId": insured_details["proposer_area_id"],
    "proposerDob": insured_details["proposer_dob"],
    "proposerEmail": insured_details["proposer_email"],
    "proposerName": insured_details["proposer_name"],
    "proposerPhone": insured_details["proposer_phone"],
    "travelDangerousAreas": insured_details["travel_dangerous_areas"],
    "travelEndOn": insured_details["travel_end_on"],
    "travelPurposeId": insured_details["travel_purpose_id"],
    "travelStartOn": insured_details["travel_start_on"],
    "travelValidPassport": insured_details["travel_valid_passport"]
}
    data = json.dumps(data)
    travel_logger.info('===================\nProposal REQUEST\n%s\n====================================' % data)
    res = requests.post(url = URL, data = data,  headers={'Content-Type' : 'application/json'})
    travel_logger.info('===================\nProposal RESPONSE\n%s\n====================================' % str(res.json()))
    return res.json()


def create_policy_token(ref_id):
    url = URL + '/' + ref_id + '/token'
    data = {
        "APIKEY": STARHEALTH_API_KEY,
        "SECRETKEY": STARHEALTH_SECRET_KEY,
        "referenceId": ref_id
    }
    data = json.dumps(data)
    travel_logger.info('===================\nCREATE POLICY TOKEN REQUEST\n%s\n====================================' % data)
    res = requests.post(url=url, data=data, headers={'Content-Type': 'application/json'})
    travel_logger.info('===================\nCREATE POLICY TOKEN RESPONSE\n%s\n====================================' % str(res.json()))
    return res.json()


def pre_transaction(transaction):
    transaction.plan_details['plan'] = transaction.plan_details.pop('plan_type')
    transaction.plan_details['members'] = ['self']
    transaction.data['insured_details_json'] = {}


def post_transaction(transaction):
    template_data = create_proposal(transaction)

    # fetching redirect token id
    redirect_token_id = create_policy_token(template_data['referenceId'])
    template_data['redirectToken'] = redirect_token_id['redirectToken']

    # creating payment url
    payment_url = PAYMENT_URL + redirect_token_id['redirectToken']

    is_redirect = True
    # resp_url = settings.SITE_URL
    redirect_url = payment_url
    template_name = None
    return is_redirect, redirect_url, template_name, template_data


def post_payment(payment_response, transaction=None):
    purchase_token = payment_response['purchaseToken']
    data = {
        'APIKEY': STARHEALTH_API_KEY,
        'SECRETKEY': STARHEALTH_SECRET_KEY,
        'purchaseToken': purchase_token
    }
    processed_data = json.dumps(data)
    req = urllib2.Request(url='%s/api/policy/proposals/%s/purchase/response' % (URL, purchase_token),
                          data=processed_data, headers={'Content-Type': 'application/json'})
    resp_json = urllib2.urlopen(req).read()
    resp = json.loads(resp_json)

    transaction = Transaction.objects.get(reference_id=resp['referenceId'])
    transaction.payment_response = resp
    if 'payment_response' not in transaction.extra:
        transaction.extra['payment_response'] = {}

    transaction.payment_token = purchase_token
    transaction.extra['payment_response']['purchase_token'] = purchase_token
    transaction.extra['payment_response']['status'] = resp['status']
    transaction.extra['payment_response']['reference_id'] = resp['referenceId']
    transaction.save()

    if transaction.extra['payment_response']['status'].lower().find('success') >= 0:
        transaction.payment_done = True
        transaction.payment_on = datetime.datetime.today()
        data = {
            'APIKEY': STARHEALTH_API_KEY,
            'SECRETKEY': STARHEALTH_SECRET_KEY,
            'referenceId': transaction.extra['payment_response']['reference_id']
        }
        processed_data = json.dumps(data)
        time.sleep(3)
        pdf_req = urllib2.Request(url='%s/api/policy/proposals/%s/schedule' % (
            URL,
            transaction.extra['payment_response']['reference_id']
        ), data=processed_data, headers={'Content-Type': 'application/json'})
        pdf_response = urllib2.urlopen(pdf_req)
        pdf_resp = pdf_response.read()
        try:
            if type(pdf_resp) == dict and 'error' in pdf_resp.keys():
                travel_logger.info('\nSTAR TRANSACTION: %s \nPDF GENERATION ERROR":\n %s \n' % (transaction.transaction_id, pdf_resp))
            else:
                transaction.update_policy(pdf_resp, send_mail=False)
        except Exception as e:
            travel_logger.info('\nSTAR TRANSACTION: %s \nPDF GENERATION ERROR":\n %s \n' % (transaction.transaction_id, e.message))
            print e.message

        transaction.status = transaction.COMPLETED
        prod_utils.send_success_mail(transaction)

    else:
        # TODO
        # transaction.extra['error_message'] = transaction.extra['payment_response']['error_message']
        transaction.status = transaction.FAILED
        prod_utils.send_failure_mail(transaction)
    transaction.save()
    return transaction, transaction.status == transaction.COMPLETED
