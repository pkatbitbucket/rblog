
import json

def get_form(request, transaction):
    members = transaction.plan_details['members']
    return  {
        'members_list':json.dumps(members),
    }
