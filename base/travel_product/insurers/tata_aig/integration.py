import requests
import json
import os
from travel_product.models import Transaction
from settings import SETTINGS_FILE_FOLDER
from travel_product import prod_utils
from travel_product import settings as travel_settings
from collections import OrderedDict
from urllib import urlencode
from django.core.files.base import File
import datetime
from dateutil.relativedelta import *
import httplib2
import xlrd
from lxml import etree
from utils import travel_logger
import xml.etree.ElementTree as ET
from BeautifulSoup import BeautifulSoup
from hashlib import md5

WSDL_URL = ''
REQUEST_URL = 'https://purchasexml.uat.travelguard.biz/servlet/WaatsEngine.WTEngine'
PAYMENT_URL = 'https://webpos.tataaiginsurance.in/pguat/pgrequest.jsp'
HOST = ''
PDF_URL = HOST+'/cordys'
PREMIUM_SHEET_PATH = os.path.join(SETTINGS_FILE_FOLDER, 'travel_product/insurers/tata_aig/premium_sheet/tata_aig.xls')
SUM_ASSURED = {
        'GOLD': '250,000 USD',
        'PLATNM':'500,000 USD',
        'SILVER':'50,000 USD',
        'SILPLS':'100,000 USD',
        'SENPLN':'50,000 USD',
        'ANGOLD': '250,000 USD',
        'ANPLAT':'500,000 USD',
        'PLANA':'50,000 USD',
        'PLANB':'100,000 USD',
        'ULTIMT':'250,000 USD',
        }
PLAN_NAMES = {
        'GOLD': 'Gold',
        'PLATNM':'Platinum',
        'SILVER':'Silver',
        'SILPLS':'Silver Plus',
        'SENPLN':'Senior Plan',
        'ANGOLD': 'Multi-Trip Gold',
        'ANPLAT':'Multi-Trip Platinum',
        'PLANA':'Plan A',
        'PLANB':'Plan B',
        'ULTIMT':'Ultimate',
        }


def pre_transaction(transaction):
    search_data = transaction.extra['search_data']
    no_members = search_data['members']
    members = ['member%s' % (i+1) for i in range(int(no_members))]
    transaction.plan_details['plan'] = transaction.plan_details.pop('plan_type')
    transaction.plan_details['members'] = members
    transaction.insured_details_json = search_data
    return None

def get_data_dict(data):

    DOB =  data['DOB']
    date_of_birth = datetime.datetime.strptime(DOB,'%d-%b-%Y').strftime('%m/%d/%Y')
    schengen =  data['schengen']
    sum_assured = data['sum_assured']
    from_date = data['from_date']
    no_members = data['members']
    geographical_coverage = data['geographical_coverage']
    trip_duration = data['trip_duration']
    max_trip_duration = data.get('max_trip_length')
    policy_type = data['policy_type']
    trip_type = data['trip_type']
    dep_date  = datetime.datetime.strptime(from_date,'%d-%b-%Y').strftime('%m/%d/%Y %H:%M:%S AM')
    age = relativedelta(datetime.datetime.strptime(dep_date, '%m/%d/%Y %H:%M:%S AM').date(), datetime.datetime.strptime(date_of_birth, '%m/%d/%Y').date())
    if age.years == 0:
        age = age.months / 12.0
    else:
        age = age.years
    if trip_type =='A' or policy_type == 'S':
        to_date = datetime.datetime.strptime(from_date,'%d-%b-%Y') + datetime.timedelta(days= 364)
    else:
        to_date = datetime.datetime.strptime(data['to_date'],'%d-%b-%Y')
    return_date = to_date.strftime('%m/%d/%Y %H:%M:%S AM')
    insureds = ['member%s' % (i+1) for i in range(int(no_members))]
    cltaddr01 = data.get('ResAddr1')
    cltaddr02 = data.get('ResAddr2')
    cltaddr03 = data.get('ResAddr3')
    state = data.get('state')
    city = data.get('city')
    pincode = data.get('pincode')
    district_name = data.get('district_name')
    email =  data.get('email')
    mobile = data.get('mobile')
    read_excel = xlrd.open_workbook(PREMIUM_SHEET_PATH)
    travel_rates = read_excel.sheet_by_name('tata_plans')
    prod_cd_list = []
    plan_cd_list = []
    benefit_cd_list = []

    for row in range(1,travel_rates.nrows):

         plan_name = travel_rates.cell_value(row,0)
         gds_product_cd = travel_rates.cell_value(row,1)
         benefit_code = travel_rates.cell_value(row,2)
         plan_cd = travel_rates.cell_value(row,3)
         destin = travel_rates.cell_value(row,4)
         gen_purpose =  travel_rates.cell_value(row,5)
         min_age =  travel_rates.cell_value(row,7)
         max_age =  travel_rates.cell_value(row,8)
         annual_trip_duration = travel_rates.cell_value(row,9)
         annual_duration = str(int(annual_trip_duration)) if annual_trip_duration!='' else ''
         if ('annual' in plan_name.lower()) and (max_trip_duration == annual_duration) and (min_age <= age < max_age) and (trip_type=='A'):

            product_cd = '0'+str(int(gds_product_cd))
            benefit_cd = str(int(benefit_code)) if  geographical_coverage=='Asia' else '0'+str(int(benefit_code))
            plan_code = plan_cd
            destination = str(int(destin))
            general_purpose = gen_purpose
            if SUM_ASSURED[plan_code] == sum_assured:
                if  (age >= 56 and schengen =='Y') and min_age>=56:
                    prod_cd_list.append(product_cd)
                    plan_cd_list.append(plan_code)
                    benefit_cd_list.append(benefit_cd)
                elif  (age >= 56 and schengen =='N'):
                    prod_cd_list.append(product_cd)
                    plan_cd_list.append(plan_code)
                    benefit_cd_list.append(benefit_cd)
                elif age < 56:
                    prod_cd_list.append(product_cd)
                    plan_cd_list.append(plan_code)
                    benefit_cd_list.append(benefit_cd)

         elif  (trip_type!='A') and (geographical_coverage.lower() in gen_purpose.lower()) and (min_age <= age < max_age):

            product_cd = '0'+str(int(gds_product_cd))
            benefit_cd = str(int(benefit_code)) if  geographical_coverage=='Asia' else '0'+str(int(benefit_code))
            plan_code = plan_cd
            destination = str(int(destin))
            general_purpose = gen_purpose
            if SUM_ASSURED[plan_code] == sum_assured:
                if  (age >= 56 and schengen =='Y') and min_age>=56:
                    prod_cd_list.append(product_cd)
                    plan_cd_list.append(plan_code)
                    benefit_cd_list.append(benefit_cd)
                elif  (age >= 56 and schengen =='N'):
                    prod_cd_list.append(product_cd)
                    plan_cd_list.append(plan_code)
                    benefit_cd_list.append(benefit_cd)
                elif age < 56:
                    prod_cd_list.append(product_cd)
                    plan_cd_list.append(plan_code)
                    benefit_cd_list.append(benefit_cd)
    address = {}
    address['StreetLn1Nm'] = cltaddr01
    address[ 'StreetLn2Nm'] = cltaddr02
    address ['StreetLn3Nm'] = cltaddr03
    address[ 'CityNm'] = city
    address[ 'StateNm'] = state
    address[ 'ZipCd']  = pincode
    address['DistrictNm'] = district_name
    address ['CntryNm'] = 'India'
    address['HomePhoneNo'] = mobile
    address ['WorkPhoneNo'] =  mobile
    address[ 'CellPhoneNo'] = mobile
    address[ 'EmailAddr'] = email

    data_dicts = []
    for elem in range(len(prod_cd_list)):
        insuredDetails  = []

        for mem in insureds:
            member= {}
            member['FirstNm'] = data.get(mem+'FName')
            member['MiNm'] =  data.get(mem+'MName', '')
            member['LastNm']=  data.get(mem+'LName')
            member['BirthDt']=  data.get(mem+'DOB') if DOB  == "" else  date_of_birth
            member['PlanCode'] = plan_cd_list[elem]
            member['BenefitIn']  = {'BenefitCd': benefit_cd_list[elem],'NumberOfUnits':'1'}
            member['IsInsuredFlag'] = 1
            member['InsuredIdNo'] =  '16530152'
            member['Beneficiary'] = {'LastNm':'jjjj'}
            insuredDetails.append(member)

        insuredDetails.append({'Address': address})

        data_dict = {
                    'Header' : {
                        'MessageType' :'GRP1',
                        'ErrorCode' : 0,
                        'ErrorMessage' :'OK',
                        'Version' :1.0,
                        'SourceId': 'LTA',
                        'MessageId': 'LTA9120130222190952000390',
                        },
                    'Segment':{
                        'TransactionType':'NQuote',
                        'TransactionId' : 'LTA9120130222190952000390',
                        'PolicyIn': {
                            'GDSCode':'LTA',
                            'AgencyPCC':'TESTUSER',
                            'AgencyCode':'TESTUSER',
                            'IATACntryCd': 4,
                            'GDSProductCode': prod_cd_list[elem],
                            'InceptionDate': dep_date,
                            'ExpirationDate': return_date,
                            'TotalNumberOfInsureds': no_members,
                            'TransactionApplDate': datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S PM'),
                            'TransactionEffDate': dep_date,
                            'TransactionExpDate': return_date,
                            'Destination': destination,
                            'UserCd':'TESTUSER',
                            'Agent':'TESTUSER',
                            'Remark2':'TESTUSER',
                            'GeneralPurpose1': general_purpose,
                            },
                        'Insured' : insuredDetails,
                        },
                    }
        data_dicts.append(data_dict)
    return data_dicts

def get_soap_request_response(transaction=None,activity=None):

    if transaction:
        data = transaction.insured_details_json
        data_dicts  = get_data_dict(data)
    else:
        data = activity.search_data
        data_dicts  = get_data_dict(data)
    responses = []
    for XmlData in data_dicts:
        Session = etree.Element('TINS_XML_DATA')
        session_etree = prod_utils.dict_to_etree(Session,XmlData)
        session_list = []
        session_list.append(session_etree)
        request = etree.tostring(Session)
        http = httplib2.Http()
        encoded_data  = { 'MessageText' : request, 'MessageId' : '999999', 'MessageType' : 'INPN'}
        headers = {"Content-type": "application/x-www-form-urlencoded"}
        transaction_id = transaction.transaction_id if transaction else ''
        travel_logger.info('============TATA_AIG REQUEST==================== \n%s\n%s' % (transaction_id, request))
        response_header, response_content = http.request(REQUEST_URL, method='POST', body = urlencode(encoded_data), headers = headers)
        responses.append(response_content)
        travel_logger.info('===============TATA_AIG RESPONSE=================\n%s\n%s' % (transaction_id,response_content))

    return responses

def get_quote(activity):

    responses = get_soap_request_response(activity=activity)
    plans = []
    extra_data = None

    for response_content in responses:
        soup = BeautifulSoup(response_content)
        plan_codes = soup.findAll('insured')
        plan_details = soup.findAll('policyout')
        for tag in plan_codes:
            plan_cd = tag.find('plancode').getText()
            for tag in plan_details:
                plandict = {}
                product_description = tag.find('productdescription').getText()
                premium = tag.find('totalpremium').getText()
                total_tax_amt = tag.find('totaltaxamt').getText()
                plandict['plan_type'] = product_description + "( %s)" % PLAN_NAMES[plan_cd]
                plandict['sum_assured'] = SUM_ASSURED[plan_cd]
                plandict['premium'] = '%.2f' % float(premium)
                plans.append(plandict)

    return plans,extra_data

def post_transaction(transaction):

    extra_data = transaction.plan_details['extra_data']
    reference_id = 'CFOX-%s-%s' % (transaction.id, datetime.datetime.strftime(datetime.datetime.today(), '%Y%m%d'))
    transaction.reference_id = reference_id
    post_data = {
        'vendorcode': 'TCOVERFOX',
        'referencenum': reference_id,
        'amount': transaction.plan_details['premium'],
        'productcode': '020952',
        'producercode':'0010805000',
        'action':'https://webpos.tataaiginsurance.in/pguat/pgrequest.jsp',
        }
    parameter = post_data['action'] + "?" +"vendorcode="+post_data['vendorcode']+"&referencenum="+post_data['referencenum']+"&amount="+post_data['amount']+"&productcode=" +post_data['productcode']+"&action="+post_data['action']+"&producercode="+post_data['producercode']
    codestring = parameter + '67H571iF5ol7q1n8'
    hexstr = md5(codestring).hexdigest()
    transaction.save()

    template_name = 'health_product/post_transaction_data.html'
    is_redirect = True
    PAY_URL = parameter + "&" + "hash=" + hexstr

    return is_redirect, PAY_URL , template_name, post_data

def post_payment(payment_response, transaction):
    """payment_response='CFOX-165-20150626||TCOVERFOX||DHCC3872478414||HCC||2024.00||GV00004-PARes status not sucessful||0399'"""
    split_data = payment_response['msg'].split('||')
    resp_dict ={
            'reference_id': split_data[0],
            'partner_code':  split_data[1],
            'mcitnumber':  split_data[2],
            'referencenum': split_data[3],
            'amount':  split_data[4],
            'msg': split_data[5],
            'reason':  split_data[6],

            }
    transaction = Transaction.objects.get(reference_id = resp_dict['reference_id'])
    transaction.payment_response =  payment_response

    travel_logger.info('\nTATA AIG TRANSACTION: %s\n PAYMENT RESPONSE RECEIVED:\n %s\n' % (transaction.transaction_id, payment_response))

    if resp_dict['msg']=='Success':
        transaction.payment_done = True
        transaction.payment_on = datetime.datetime.today()
        transaction.status = transaction.COMPLETED
        prod_utils.send_success_mail(transaction)
        transaction.extra['error_message'] = ''
    else:
        transaction.status = transaction.FAILED
        transaction.extra['error_message'] = resp_dict['reason']
        prod_utils.send_failure_mail(transaction)
    transaction.save()

    return transaction, transaction.status == transaction.COMPLETED
