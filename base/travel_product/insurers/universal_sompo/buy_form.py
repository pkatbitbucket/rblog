import custom_dictionary
import copy

STATES = copy.deepcopy(custom_dictionary.STATE_CODES)
CITIES = copy.deepcopy(custom_dictionary.CITY_CODES)
#    COUNTRIES = custom_dictionary.COUNTRY_LIST


def get_form(request, transaction):

    members = transaction.plan_details['members']

    return {
        'num_insured': len(members),
        'members_list': members,
        'states_list': STATES,
        'cities_list': CITIES,
        # 'countries': COUNTRIES,
    }
