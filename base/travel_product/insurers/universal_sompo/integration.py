import urllib
import json
import traceback
from datetime import datetime
from dateutil.relativedelta import relativedelta
import httplib2
import HTMLParser
import requests
from lxml import etree  # import *
import xml.etree.ElementTree as ET
from django.template import Template, Context
from django.utils import html
# from utils import request_logger

from django.conf import settings
from utils import travel_logger
from travel_product import settings as travel_settings
from travel_product.insurers.universal_sompo import custom_dictionary
from travel_product.insurers.universal_sompo.policy_template import raw_template
from travel_product.models import Transaction

if settings.PRODUCTION:
    WSDL_URL = 'http://www.universal-sompo.net.in:800/WebAggWCF/Service1.svc?wsdl'
    REQUEST_URL = 'http://www.universal-sompo.net.in:800/WebAggWCF/Service1.svc'
    PAYMENT_URL = 'https://www.usgi.co.in/PaymentGateway/ExtIntityCallPage.aspx?'
    WACode = '20000003'
    WAAppCode = '30000003'
    WAUserID = 'USER01'
    WAUserPwd = 'pass@123'
    WAType = '0'
else:
    REQUEST_URL = 'http://14.141.41.53:800/WebAggWCFUAT/Service1.svc'
    PAYMENT_URL = 'https://www.usgi.co.in/PaymentGateway/ExtIntityCallPage.aspx?'
    WACode = '20000003'
    WAAppCode = '30000003'
    WAUserID = 'USER01'
    WAUserPwd = 'pass@123'
    WAType = '0'


def _calculate_term(search_data):
    '''
    Return term between two dates
    '''
    if search_data.get('max_trip_length'):
        term = int(search_data.get('max_trip_length'))

    else:
        from_date_obj = datetime.strptime(search_data['from_date'], travel_settings.DATE_FORMAT)
        to_date_obj = datetime.strptime(search_data['to_date'], travel_settings.DATE_FORMAT)
        term = (to_date_obj - from_date_obj).days + 1
    search_data['trip_duration'] = term
    if(search_data['plan'] == 'Travel-Worldwide' or search_data['plan'] == 'Travel-Worldwide Excluding US & Canada'):
        if term in range(1, 8):
            return 'Interval1'
        elif term in range(8, 15):
            return 'Interval2'
        elif term in range(15, 22):
            return 'Interval3'
        elif term in range(22, 29):
            return 'Interval4'
        elif term in range(29, 36):
            return 'Interval5'
        elif term in range(36, 48):
            return 'Interval6'
        elif term in range(48, 61):
            return 'Interval7'
        elif term in range(61, 76):
            return 'Interval8'
        elif term in range(76, 91):
            return 'Interval9'
        elif term in range(91, 121):
            return 'Interval10'
        elif term in range(121, 151):
            return 'Interval11'
        elif term in range(151, 181):
            return 'Interval12'
        else:
            return ''

    elif(search_data['plan'] == 'Travel-Asia'):
        if term in range(1, 5):
            return 'Interval1'
        elif term in range(5, 8):
            return 'Interval2'
        elif term in range(8, 15):
            return 'Interval3'
        elif term in range(15, 22):
            return 'Interval4'
        elif term in range(22, 31):
            return 'Interval5'
        elif term in range(36, 48):
            return 'Interval6'
        else:
            return ''
    elif(search_data['plan'] == 'Student-Travel' or search_data['plan'] == 'Student-Travel Excluding Us & Canada'):
        if term in range(1, 31):
            return 'Interval1'
        elif term in range(31, 61):
            return 'Interval2'
        elif term in range(61, 91):
            return 'Interval3'
        elif term in range(91, 121):
            return 'Interval4'
        elif term in range(121, 181):
            return 'Interval5'
        elif term in range(181, 241):
            return 'Interval6'
        elif term in range(241, 271):
            return 'Interval7'
        elif term in range(371, 365):
            return 'Interval8'
        else:
            return ''
    else:
        return term


def _calculate_premium(age_band, plan, plan_type, term):
    '''
    Return premium from the sheet
    '''
    premium_dict = custom_dictionary.premium_dict

    if((plan == 'Student-Travel') or (plan == 'Student-Travel Excluding Us & Canada')):
        premium_list = premium_dict[plan][plan_type][term]
    else:
        premium_list = premium_dict[plan][plan_type][age_band][term]

    return premium_list


def _get_age(age_band, plan):

    if(plan != 'Student-Travel' and plan != 'Student-Travel Excluding Us & Canada'):
            if (age_band == 'up to 40 Years'):
                age = 'Age1'
            elif(age_band == '41 Years - 60 Years'):
                age = 'Age2'
            elif(age_band == '61  Years - 70 Years'):
                age = 'Age3'
    else:
            if (age_band == '16 Years-35 Years'):
                age = 'Age1'
    return age


def get_quote(activity):
    search_data = activity.search_data
    # calculate term
    term = _calculate_term(search_data)
    premium = 0
    plans = []
    plan_display = []
    sum_assured = 0
    flag = 0
    plan_compare = ''
    for i in range(1, int(search_data['no_of_travellers']) + 1):
        plandict = {}
        age_band = search_data['member' + str(i) + '_age_band']
        plan = search_data['plan']
        age = _get_age(age_band, plan)
        plan_type = search_data['member' + str(i) + '_plan_type']

        # #To Display different Plans
        plan_display.append(str(i) + ':' + plan_type)
        if(plan_compare != '' and plan_compare != plan_type):
            flag = 1
        plan_compare = plan_type

        sum_assured = sum_assured + int(custom_dictionary.sum_assured[plan][plan_type])
        premium = premium + int(_calculate_premium(age, plan, plan_type, term))
    premium = premium + (premium * 0.14)
    if(flag == 0):
        plandict['plan_type'] = plan_compare.upper()
    else:
        plandict['plan_type'] = 'CUSTOMIZED(' + ','.join(plan_display) + ')'

    plandict['premium'] = premium
    plandict['sum_assured'] = sum_assured
    plandict['term'] = term
    plans.append(plandict)

    return plans, None


def pre_transaction(transaction):
    search_data = transaction.extra['search_data']

    no_of_traveller = search_data.get('no_of_travellers', 1)
    members = []
    for i in range(1, int(no_of_traveller) + 1):
        members.append('member' + str(i))

    transaction.plan_details['plan'] = search_data.get('plan')
    transaction.plan_details['members'] = members
    return None


def _get_product_name(plan):
    if(plan == 'Travel-Worldwide' or plan == 'Travel-Worldwide Excluding US & Canada'):
        return 'Travel WorldWide Insurance'
    elif(plan == 'Student-Travel' or plan == 'Student-Travel Excluding Us & Canada'):
        return 'Travel Student Insurance'
    elif(plan == 'Travel-Asia'):
        return 'Travel Asia Insurance'
    elif(plan == 'Annual Multi-Trip'):
        return 'Travel Annual Multi Trip Insurance'


def _get_travel_geography(plan):
    if(plan == 'Travel-Worldwide' or plan == 'Student-Travel' or plan == 'Annual Multi-Trip'):
        return 'WorldWide'
    elif(plan == 'Travel-Worldwide Excluding US & Canada' or plan == 'Student-Travel Excluding Us & Canada'):
        return 'WorldWide Excluding US/Canada'


def _calc_disease(member, search_data):
    disease = ''
    if(search_data[member + 'pedYesNoTravel'] == 'yes'):
        if(search_data[member + 'PEDliverDetailsTravel'] == 'yes'):
            disease += 'liver'
        elif(search_data[member + 'PEDcancerDetailsTravel'] == 'yes'):
            disease += 'Cancer/Tumor'
        elif(search_data[member + 'PEDHealthDiseaseTravel'] == 'yes'):
            disease += 'heart disease'
        elif(search_data[member + 'PEDkidneyDetailsTravel'] == 'yes'):
            disease += 'Kidney disease'
        elif(search_data[member + 'PEDparalysisDetailsTravel'] == 'yes'):
            disease += 'Paralysis'
            if(search_data[member + 'PEDotherDetailsTravel'] == 'yes'):
                disease += search_data[member + 'other_disease_desc']

    return disease


def _get_xml(insured_data, transaction):
    transaction_plans = transaction.plan_details
    transaction_details = transaction.extra['search_data']
    is_multitrip = (transaction_details['plan'] == 'Annual Multi-Trip')
    data_dict = {}
    data_dict['cover_type'] = 'Individual'
    data_dict['dep_date'] = datetime.strptime(transaction_details['from_date'], '%d-%b-%Y').strftime('%d/%m/%Y')
    if(is_multitrip):
        data_dict['return_date'] = (datetime.strptime(data_dict['dep_date'], '%d/%m/%Y') + relativedelta(days=365)).strftime('%d/%m/%Y')
    else:
        data_dict['return_date'] = datetime.strptime(transaction_details['to_date'], '%d-%b-%Y').strftime('%d/%m/%Y')
    proposer_data = insured_data['members'][0]
    data_dict['current_date'] = datetime.now().strftime('%d/%m/%Y')
    data_dict['trip_duration'] = '366' if is_multitrip else transaction_details['trip_duration']
    data_dict['plan_type'] = transaction.plan_details['sum_assured']
    data_dict['age'] = relativedelta(datetime.today(), datetime.strptime(proposer_data['date_of_birth'], '%d-%m-%Y').date()).years
    data_dict['customer_DOB'] = datetime.strptime(proposer_data['date_of_birth'], '%d-%m-%Y').strftime('%d/%m/%Y')
    data_dict['customer_name'] = proposer_data['first_name'] + ' ' + proposer_data['last_name']
    data_dict['gender'] = proposer_data['gender'].upper()
    data_dict['email'] = proposer_data['email']
    data_dict['mobile'] = proposer_data['mobile']
    data_dict['travel_geography'] = _get_travel_geography(transaction_details['plan'])
    data_dict['productname'] = _get_product_name(transaction_details['plan'])
    if(data_dict['productname'] == 'Travel WorldWide Insurance'):
        data_dict['max_travel_duration'] = '180'
    elif(data_dict['productname'] == 'Travel Asia Insurance'):
        data_dict['max_travel_duration'] = '47'
    elif(data_dict['productname'] == 'Travel Annual Multi Trip Insurance'):
        data_dict['max_travel_duration'] = transaction_details['trip_duration']

    mem_list = []
    destination_city = ''
    for i, member in enumerate(insured_data['members']):
        data = {}
        data['member_relationship'] = member.get('relationship', 'self')
        data['member_plan_type'] = transaction_plans['plan_type']
        data['member_fname'] = member['first_name']
        data['member_lname'] = member['last_name']
        data['member_dob'] = datetime.strptime(member['date_of_birth'], '%d-%m-%Y').strftime('%d/%m/%Y')
        data['member_gender'] = member['gender'].upper()

        data['member_passport'] = member['passport_no']
        data['member_nomineeName'] = member['nominee_name']
        data['member_nomineeRelation'] = member['nominee_relation']
        data['member_phone'] = member['mobile']
        data['memberped'] = 'Y' if member['ped'] == 'yes' else 'N'
        data['memberdisease'] = '' if data['memberped'] == 'N' else _calc_disease(member, insured_data)
        destination_city = member['destination_city']

        mem_list.append(data)

    # To Get countries name from country code
    trips = transaction.activity.get_quote_data()[1]['places']
    trip_country = []
    for country in trips:
        trip_country.append(country)

    trip_list = []
    for x in trip_country:
        data = {}
        data['arrival_country'] = x
        for x in custom_dictionary.CITY_CODES[insured_data['address']['state']]:
            if x['value'] == insured_data['address']['city']:
                data['destination_city'] = destination_city
                data['departure_city'] = x['key']
        trip_list.append(data)

    data_dict['wacode'] = WACode
    data_dict['wapcode'] = WAAppCode
    data_dict['userid'] = WAUserID
    data_dict['password'] = WAUserPwd
    data_dict['watype'] = WAType

    t = Template(raw_template)
    c = Context({
        'insured_data': insured_data['address'],
        'trans_plans': transaction_plans,
        'trans_details': transaction_details,
        'data_dict': data_dict,
        'mem_list': mem_list,
        'trip_list': trip_list
    })

    return t.render(c)


def _get_soap_request_response(transaction=None, activity=None):
    if transaction:
        data = transaction.insured_details_json
    else:
        data = activity.search_data
    XmlData = _get_xml(data, transaction)

    dict_string = html.escape(XmlData)

    first_string = '<commBRIDGEFusionTravel xmlns = "http://tempuri.org/"><strXdoc>%s</strXdoc></commBRIDGEFusionTravel>' % dict_string
    tag = etree.fromstring(first_string)
    Body = etree.Element('Body')
    Body.append(tag)
    Envelope = etree.Element('Envelope', xmlns='http://schemas.xmlsoap.org/soap/envelope/')
    Envelope.append(Body)
    proposal_request = etree.tostring(Envelope)
    http = httplib2.Http()
    headers = {'SOAPAction': "http://tempuri.org/IService1/commBRIDGEFusionTravel",
               'Content-Type': "text/xml", 'charset': "utf-8"}

    travel_logger.info('\n UNIVERSAL SOMPO TRANSACTION: %s \n PROPOSAL REQUEST SENT:\n%s\n' % (transaction.transaction_id, proposal_request))

    response_header, proposal_response = http.request(REQUEST_URL, method='POST', body=proposal_request, headers=headers)
    travel_logger.info('================================\n%s' % (proposal_response))

    return proposal_request, proposal_response


def post_transaction(transaction):
    template_name = None
    proposal_request = None
    proposal_response = None
    parameter_dict = {}
    try:
        proposal_request, proposal_response = _get_soap_request_response(transaction=transaction)
        parameters_dict = {}
        html_parser = HTMLParser.HTMLParser()
        proposal_response = html_parser.unescape(proposal_response)
        resp = ET.fromstring(proposal_response)
        root = resp[0][0][0][0]
        parameters_dict['PosPolicyNo'] = root.getchildren()[1].getchildren()[-1].text
        parameters_dict['FinalPremium'] = root.getchildren()[2].getchildren()[1].getchildren()[-1].attrib['Value']
        parameters_dict['Src'] = 'WA'
        parameters_dict['SubSrc'] = '20000003'
        travel_logger.info('=======================\nUniversal Sompo Payment Request: Transaction %s\n%s'
                           % (transaction.transaction_id, parameters_dict))
        redirect_url = PAYMENT_URL + urllib.urlencode(parameters_dict)
        transaction.reference_id = parameters_dict['PosPolicyNo']
        transaction.save()
        template_name = 'health_product/post_transaction_data.html'
        is_redirect = False

    except Exception as e:
        travel_logger.info('\nRELIGARE TRANSACTION: %s\nEXCEPTION: \n%s\n' % (transaction.transaction_id, traceback.format_exc()))
        parameter_dict = {
            'stack_trace': traceback.format_exc(),
            'error_message': e.message,
            'error_response': proposal_response,
        }
        is_redirect = True
        redirect_url = "/travel-insurance/transaction/%s/failure/" % (transaction.transaction_id)

    return is_redirect, redirect_url, template_name, parameter_dict, proposal_request, proposal_response


def post_payment(payment_response, transaction):
    # SOMPOGINS|QUID000059|3414909965|2311/50122343/01/B00|4842.00|NA|21-01-2015
    # 16:20:35|1001|Paymentsuccessfully|www.google.com|NA|NA|NA|NA|NA
    split_resp = payment_response['MSG'].split('|')
    # removing Blank strings
    split_resp = [x for x in split_resp if x]
    resp = {
        'mechant_id': split_resp[0],
        'quoteID': split_resp[1],
        'waurn_no': split_resp[2],
        'policy_no': split_resp[3],
        'txn_amount': split_resp[4],
        'additionalinfo0': split_resp[5],
        'txn_date': split_resp[6],
        'error_code': split_resp[7],
        'txn_description': split_resp[8],
        'download_link_url': split_resp[9],
        'additionalinfo1': split_resp[10],
        'additionalinfo2': split_resp[11],
        'additionalinfo3': split_resp[12],
        'additionalinfo4': split_resp[13],
        'additionalinfo5': split_resp[14]
    }
    data = {}

    transaction = Transaction.objects.get(reference_id=resp['quoteID'])
    transaction.payment_response = json.dumps(payment_response)
    transaction.policy_number = resp['quoteID']
    transaction.extra['payment_response'] = resp
    travel_logger.info('===================\nUSGIC TRANSACTION: %s\nPAYMENT RESPONSE\n%s\n' % (transaction.transaction_id, payment_response))

    if transaction.extra['payment_response']['error_code'] == '1001':
        transaction.extra['payment_response']['status'] = 'SUCCESS'
        transaction.extra['error_message'] = ''
        transaction.payment_done = True
        transaction.payment_on = datetime.now()
        try:
            pdf_url = transaction.extra['payment_response']['download_link_url']
            pdf_doc = requests.get(pdf_url, verify=False)
            transaction.update_policy(pdf_doc.content, send_mail=False)
        except Exception as e:
            travel_logger.info('\nUNIVERSAL SOMPO INTEGRATION: %s\n POLICY PDF EXCEPTION:\n %s\n' % (transaction.transaction_id, e))

        transaction.status = transaction.COMPLETED
    else:
        transaction.extra['error_message'] = resp['txn_description']
        transaction.status = transaction.FAILED
        data = {'stack_trace': "",
                'error_message': resp['txn_description'],
                'error_response': payment_response
                }

    transaction.save()
    return transaction, transaction.status == transaction.COMPLETED, data
