raw_template="""
<Root>
<Authentication>
<WACode>{{data_dict.wacode}}</WACode>
<WAAppCode>{{data_dict.wapcode}}</WAAppCode>
<WAUserID>{{data_dict.userid}}</WAUserID>
<WAUserPwd>{{data_dict.password}}</WAUserPwd>
<WAType>{{data_dict.watype}}</WAType>
</Authentication>
<Customer>
  <CustomerType>{{data_dict.cover_type}}</CustomerType>
  <CustomerName>{{data_dict.customer_name}}</CustomerName>
  <DOB>{{data_dict.customer_DOB}}</DOB>
  <Gender>{{data_dict.gender}}</Gender>
  <CanBeParent>0</CanBeParent>
  <ContactTelephoneSTD>0</ContactTelephoneSTD>
  <MobileNo>{{data_dict.mobile}}</MobileNo>
  <Emailid>{{data_dict.email}}</Emailid>
  <PresentAddressLine1>{{insured_data.address_line_1}}</PresentAddressLine1>
  <PresentAddressLine2>{{insured_data.address_line_2}}, {{insured_data.address_line_3}}</PresentAddressLine2>
  <PresentStateCode>{{insured_data.state}}</PresentStateCode>
  <PresentCityDistCode>{{insured_data.city}}</PresentCityDistCode>
  <PresentPinCode>{{insured_data.present_pincode}}</PresentPinCode>
  <PermanentAddressLine1>{{insured_data.address_line_1}}</PermanentAddressLine1>
  <PermanentAddressLine2>{{insured_data.address_line_2}}</PermanentAddressLine2>
  <PermanentStateCode>{{insured_data.state}}</PermanentStateCode>
  <PermanentCityDistCode>{{insured_data.city}}</PermanentCityDistCode>
  <PermanentPinCode>{{insured_data.pincode}}</PermanentPinCode>
   <ProductName>{{data_dict.productname}}</ProductName>
  <InstrumentNo>0</InstrumentNo>
  <InstrumentDate>0</InstrumentDate>
  <BankID>0</BankID>
  <PosPolicyNo></PosPolicyNo>
</Customer>

<Product Name = "{{data_dict.productname}}">
	<GeneralProposal Name = "General Proposal">
			<GeneralProposalGroup Name = "General Proposal Group">


			  <GeneralProposalInformation Name = "General Proposal Information">
					<TypeOfBusiness Name="Type Of Business" Value="FROM INTERMEDIARY" />
  				<ServiceTaxExemptionCategory Name="Service Tax Exemption Category" Value="No Exemption" />
 					<BusinessType Name="Transaction Type" Value="New" />
 					<Sector Name="Sector" Value="Others" />
 					<DealId Name="Deal Id" Value="" />
   				<PolicyType Name="Policy Type" Value="Individual"/>
 					<TravelStartDt Name="Travel Start Date" Value = "{{data_dict.dep_date}}"/>
 					<TravelEndDt Name="Travel End Date" Value = "{{data_dict.return_date}}"/>

 					<ProposalDate Name="Proposal Date"  Value = "{{data_dict.current_date}}" />
          <MaxTravelDuration Name="Max Travel Duration" Value="{{data_dict.max_travel_duration}}"/>
 					<TravelDuration Name="Travel Duration" Value="{{data_dict.trip_duration}}"/>
 					<TravelGeography Name="Travel  Geography" Value="{{data_dict.travel_geography}}"/>
   				<PolicyNumberChar Name="PolicyNumberChar" Value=""/>

   				<PolicyEffectiveDate Name="Policy Effective Date">
   							<Fromdate Name="From Date" Value ="{{data_dict.dep_date}}"/>
   							<Todate Name="To Date" Value ="{{data_dict.return_date}}"/>
   							<Fromhour Name="From Hour" Value ="18:04"/>
   							<Tohour Name="To Hour" Value = "23:59"/>
  				</PolicyEffectiveDate>

			</GeneralProposalInformation>

      </GeneralProposalGroup>





</GeneralProposal>
<PremiumCalculation Name="Premium Calculation">
            <NetPremium Name="Net Premium" Value="" />
            <ServiceTax Name="Service Tax" Value="" />
            <StampDuty2 Name="Stamp Duty" Value="" />
            <TotalPremium Name="Total Premium" Value="" />
 </PremiumCalculation>

<Risks Name="Risks">
    <Risk Type="Group" Name="Risks">
      <RisksData Type="GroupData">
        <InsuredDetails Name = "Insured Details">
           <InsuredDetailsGroup Type= "Group" Name ="Insured Details">
              {% for member in mem_list %}
               <InsuredDetailsGroupData Type= "GroupData" >
                  <PlanType Name= "Plan Type" Value= "{{member.member_plan_type}}"/>
                  <FirstName Name = "First Name" Value = "{{member.member_fname}}"/>
                  <LastName Name = "Last Name" Value = "{{member.member_lname}}"/>
                  <DOB Name = "Date of Birth" Value ="{{member.member_dob}}"/>
                  <Gender Name ="Gender" Value = "{{member.member_gender}}"/>
                  <Relationship Name = "Relationship" Value = "{{member.member_relationship}}"/>
                  <PassportNo Name = "Passport Number" Value = "{{member.member_passport}}"/>
                  <NomineeName Name= "NomineeName" Value = "{{member.member_nomineeName}}"/>
                  <NomineeRelationship Name ="Nominee Relationship" Value = "{{member.member_nomineeRelation}}"/>
                  <PhoneNo Name = "Phone Number" Value = ""/>
                  <PhysicianName Name = "Physician Name" Value = ""/>
                  <PED Name ="Pre-Existing Disease" Value = "{{member.memberped}}"/>
                  <PEDDeclared Name = "Pre-Existing Disease Declared" Value = "{{member.memberdisease}}"/>

              <TripDetails Name= "TripDetails">
              <TripsData Name= "Trips Data" Type= "Group">

                    {% for trip in trip_list %}
                    <TripData Type="GroupData">
                    <ArrivalCountry Name="Arrival Country" Value = "{{trip.arrival_country}}" />
                     <DepartureCity Name ="Departure City" Value = "{{trip.departure_city}}"/>
                    <DestinationCity Name ="Destination City" Value = "{{trip.destination_city}}"/>
                    </TripData>
                    {% endfor %}
              </TripsData>
              </TripDetails>

              </InsuredDetailsGroupData>

              {% endfor %}
            </InsuredDetailsGroup>
	         </InsuredDetails>
          </RisksData>
        </Risk>
 </Risks>
 </Product>




<Errors>
  <ErrorCode>0</ErrorCode>
  <ErrDescription>NULL</ErrDescription>
</Errors>
</Root>
"""
