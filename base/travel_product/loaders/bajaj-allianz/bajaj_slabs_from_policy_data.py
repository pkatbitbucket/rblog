from django.template.defaultfilters import title
import json, datetime
insurer = Insurer.objects.get(slug='bajaj-allianz')
policy_datas = InsurerPolicyData.objects.filter(insurer='bajaj').exclude(plans_list__contains='"premium":0.0,')
# Change policy_datas filter according to your Database
# policy_datas = policy_datas.filter(pk__gt=50219L, pk__lte=56267L)  # individual single trip plans
# policy_datas = policy_datas.filter(pk__gt=56267L, pk__lte=57723L)  # individual multi trip plans
# policy_datas = policy_datas.filter(pk__gt=57723L, pk__lte=57795L)  # family starts from 57723L and ends at 57795L (SCHENGEN MEMBER STATES / WORLDWIDE EXCLUDING USA AND CANADA)
# policy_datas = policy_datas.filter(pk__gt=58149L)  # family starts from 58149L (IncludingUSA, ExcludingUSA)

# policy_datas = policy_datas.filter(pk__gt=58441L)  # Business Multi-Trip starts from 58149L (IncludingUSA, ExcludingUSA)

def calculate_age(birth_date):
    today = datetime.date.today()
    try:
        birth_day = birth_date.replace(year=today.year)
    except ValueError:  # raised when birth date is February 29 and the current year is not a leap year
        birth_day = birth_date.replace(year=today.year, month=birth_date.month+1, day=1)
    if birth_day > today:
        return today.year - birth_date.year - 1
    else:
        return today.year - birth_date.year


grades = {
    'Travel Age': Grade.objects.get(slug='silver'),
    'Travel Age Elite Silver': Grade.objects.get(slug='gold'),
    'Travel Age Elite Gold': Grade.objects.get(slug='diamond'),
    'Travel Age Elite Platinum': Grade.objects.get(slug='platinum'),
    'Travel Asia Elite Flair': Grade.objects.get(slug='silver'),
    'Travel Asia Elite Supreme': Grade.objects.get(slug='gold'),
    'Travel Care': Grade.objects.get(slug='silver'),
    'Travel Elite Silver': Grade.objects.get(slug='silver'),
    'Travel Elite Gold': Grade.objects.get(slug='gold'),
    'Travel Elite Platinum': Grade.objects.get(slug='platinum'),
    'Travel Secure': Grade.objects.get(slug='silver'),
    'Travel Value': Grade.objects.get(slug='silver'),

    'Corporate Elite Lite': Grade.objects.get(slug='silver'),
    'Corporate Elite Plus': Grade.objects.get(slug='gold'),

    'Travel Family (Family Floater)': Grade.objects.get(slug='silver'),
    'Travel Elite Family  (Family Floater)': Grade.objects.get(slug='gold'),
}
places = {
    'ASIA ONLY EXCLUDING JAPAN': (Place.objects.filter(slug__in=['asia']), Place.objects.filter(slug__in=['japan'])),  # asia excluding japan
    'WORLDWIDE EXCLUDING USA AND CANADA': (Place.objects.filter(slug__in=['worldwide']), Place.objects.filter(slug__in=['usa-united-states-of-america', 'canada'])),  # world excluding usa and canada
    'WORLDWIDE INCLUDING USA AND CANADA': (Place.objects.filter(slug__in=['worldwide']), []),  # world including usa and canada

    'SCHENGEN MEMBER STATES': (Place.objects.filter(slug__in=['worldwide']), []),  # world including usa and canada
    'SCHENGEN MEMBER STATES / WORLDWIDE INCLUDING USA AND CANADA': (Place.objects.filter(slug__in=['worldwide']), []),  # world including usa and canada
    'SCHENGEN MEMBER STATES / WORLDWIDE EXCLUDING USA AND CANADA': (Place.objects.filter(slug__in=['worldwide']), Place.objects.filter(slug__in=['usa-united-states-of-america', 'canada'])),  # world including usa and canada
    'IncludingUSA': (Place.objects.filter(slug__in=['worldwide']), []),  # world including usa and canada
    'ExcludingUSA': (Place.objects.filter(slug__in=['worldwide']), Place.objects.filter(slug__in=['usa-united-states-of-america', 'canada'])),  # world excluding usa and canada
}
duration_business_multi_trip = [0,] + [4, 7, 10, 19, 26, 33, 47, 60, 75, 90, 120, 150, 180]
for data in policy_datas:
    form_data = data.form_data
    for plan_dict in data.plans_list:
        plan, _ = Plan.objects.get_or_create(**{
            'type': ['multi', 'single'][form_data['traveller_type'] in ['Individual', 'Family']],  # Toggle when required
            'name': title(plan_dict['plan_type']),
            'insurer': insurer,
            'grade': grades[plan_dict['plan_type'].strip()],
            'currency': 'USD',
            'sum_assured': int(''.join(plan_dict['sum_assured'].split(' ')[1].split(',')).replace('$', '')),
            'verbose_name': form_data['region'],
        })
        print plan

        places_included, places_excluded = places[form_data['region']]

        for place in places_included:
            plan.places_included.add(place)

        for place in places_excluded:
            plan.places_excluded.add(place)

        age = calculate_age(
            datetime.datetime.strptime(form_data['dob'], '%d-%b-%Y').date()
        )
        duration = (datetime.datetime.strptime(form_data['to_date'], '%d-%b-%Y').date() - datetime.datetime.strptime(form_data['from_date'], '%d-%b-%Y').date()).days
        duration += 1

        if form_data['traveller_type'] == 'Family':
            age_ranges = {
                0: 60,
            }
        else:
            age_ranges = {
                0: 40,
                41: 60,
                61: 70,
            }

        for age_min, age_max in age_ranges.items():
            if age >= age_min and age <= age_max:
                break

        if form_data['traveller_type'] == 'Individual' or form_data['traveller_type'] == 'Business Multi-Trip':
            if form_data['traveller_type'] == 'Business Multi-Trip':
                index = duration_business_multi_trip.index(duration)
                duration_range = range(duration_business_multi_trip[index - 1] + 1,duration_business_multi_trip[index] + 1)
            else:
                duration_range = [duration]

            inc_premium = float(plan_dict['premium'])
            exc_premium = round((inc_premium*100)/114)
            for duration in duration_range:
                slab, new = IndividualSlab.objects.get_or_create(**{
                    'plan': plan,
                    'age_min': age_min,
                    'age_max': age_max,
                    'duration': duration,
                    'premium': float(exc_premium),
                })
                if new:
                    slab.source = {
                        'form_data': form_data,
                        'plan_data': plan_dict,
                    }
                    slab.save()
            print new, duration_range[0], duration_range[-1], plan.pk,  slab  # Outside the loop?



        else:
            if plan.type == 'single':
                duration_range = range(*{
                    15: [1, 16],
                    30: [16, 31],
                    60: [31, 61],
                }[duration])
            else:
                import ipdb;ipdb.set_trace()  # there shouldnt be any multiple trip family plan

            inc_premium = float(plan_dict['premium'])
            exc_premium = round((inc_premium*100)/114)
            for duration in duration_range:
                if duration in range(2, 357): #  No plan with duration 1. wtf?
                    adult_max = int(form_data['no_of_adult']) + 1
                    children_max = int(form_data['no_of_child'])
                    slab, new = FamilySlab.objects.get_or_create(**{
                        'plan': plan,
                        'age_min': age_min,
                        'age_max': age_max,
                        'duration': duration,
                        'premium': float(exc_premium),
                        'adult_max': adult_max,
                        'children_max': children_max,
                    })
                    if new:
                        slab.source = {
                            'form_data': form_data,
                            'plan_data': plan_dict,
                        }
                        slab.save()
            print new, duration_range[0], duration_range[-1], plan_dict['plan_type'], slab  # Outside the loop?
