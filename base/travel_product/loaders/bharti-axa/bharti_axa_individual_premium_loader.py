
import csv
from travel_product.models import Insurer, Plan, Place, Slab, Grade, IndividualSlab

insurer = Insurer.objects.get_or_create(name='Bharti AXA')[0]
grade = Grade.objects.get(slug='silver')
places_included = ['asia']
places_excluded = ['japan']
sum_assured = ('USD', 50000)
age_min = 0
age_max = 40
with open('/Users/arpit/Downloads/bharti_axa_travel_plans_basic_smart_traveller_3months_to_40_years.csv', 'rt') as f:
	reader = csv.DictReader(f)
	'''Add plans first'''
	for row in reader:
		for plan_name in row.keys()[1:]:
			plan = Plan.objects.create(**{
				'name': plan_name.replace('  ', ' '),
				'type': 'single',
				'insurer': insurer,
				'grade': grade,
				'currency': sum_assured[0],
				'sum_assured': sum_assured[1],
			})
			if plan.name == 'Asia excluding Japan':
				places_included = ['asia']
				places_excluded = ['japan']
			elif plan.name == 'World-wide including USA and Canada':
				places_included = ['worldwide']
				places_excluded = []
			elif plan.name == 'World-wide excluding USA and Canada including Japan':
				places_included = ['worldwide']
				places_excluded = ['usa-united-states-of-america']
			else:
				raise Exception

			for place_included in places_included:
				plan.places_included.add(Place.objects.get(slug=place_included))
			for place_excluded in places_excluded:
				plan.places_excluded.add(Place.objects.get(slug=place_excluded))

			print plan
		break

with open('/Users/arpit/Downloads/bharti_axa_travel_plans_basic_smart_traveller_3months_to_40_years.csv', 'rt') as f:
	reader = csv.DictReader(f)
	'''Add Individual Slabs now'''
	plans = Plan.objects.filter(insurer=insurer)
	for row in reader:
		for plan_name, premium in row.items()[1:]:
			import ipdb;ipdb.set_trace()
			durations = map(lambda d: int(d), row.items()[0][1].split('-'))
			durations += durations[-1]+1
			durations = range(*durations)
			for duration in durations:
				IndividualSlab.objects.create(**{
					'plan': plans.get(name=plan_name.replace('  ', ' ')),
					'age_min': age_min,
					'age_max': age_max,
					'premium': float(premium),
					'duration': duration
				})
