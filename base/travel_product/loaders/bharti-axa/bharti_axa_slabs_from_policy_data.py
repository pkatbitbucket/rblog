from django.template.defaultfilters import title
import json, datetime
insurer, _ = Insurer.objects.get_or_create(name='Bharti AXA', slug='bharti-axa')
policy_datas = InsurerPolicyData.objects.filter(insurer=insurer.slug)
# policy_datas = policy_datas.filter(pk__gt=46972L)  # schengen plans BECAREFUL, THE PLANS SHOULD BE IN EUROS AND NOT USD
# policy_datas = policy_datas.filter(pk__gt=48944L)  # schengen plans family BECAREFUL, THE PLANS SHOULD BE IN EUROS AND NOT USD

def calculate_age(birth_date):
    today = datetime.date.today()
    try:
        birth_day = birth_date.replace(year=today.year)
    except ValueError:  # raised when birth date is February 29 and the current year is not a leap year
        birth_day = birth_date.replace(year=today.year, month=birth_date.month+1, day=1)
    if birth_day > today:
        return today.year - birth_date.year - 1
    else:
        return today.year - birth_date.year


BOOL = {
    'yes': 'True',
    'no': 'False',
    True: 'True',
    False: 'False',
    '':''
}
grades = {
    'Smart Traveller Basic Plan': Grade.objects.get(slug='silver'),
    'Smart Traveller Regular Plan': Grade.objects.get(slug='gold'),
    'Smart Traveller Essential Plan': Grade.objects.get(slug='diamond'),
    'Smart Traveller Silver Plan': Grade.objects.get(slug='platinum'),
    'Smart Traveller Gold Plan': Grade.objects.get(slug='titanium'),

    'Smart Traveller Annual Multi-trip Silver': Grade.objects.get(slug='silver'),
    'Smart Traveller Annual Multi-trip Gold': Grade.objects.get(slug='gold'),
    'Smart Traveller Annual Multi-trip Platinum': Grade.objects.get(slug='diamond'),

    'Smart Traveller Schengen Silver': Grade.objects.get(slug='silver'),
    'Smart Traveller Schengen Gold': Grade.objects.get(slug='gold'),
    'Smart Traveller Schengen Platinum': Grade.objects.get(slug='platinum'),
}
places = {
    '1': (Place.objects.filter(slug__in=['worldwide']), Place.objects.filter(slug__in=['schengen'])),  # world including usa and canada
    '2': (Place.objects.filter(slug__in=['worldwide']), Place.objects.filter(slug__in=['usa-united-states-of-america', 'canada', 'schengen'])),  # world excluding usa and canada but including japan
    '3': (Place.objects.filter(slug__in=['asia']), Place.objects.filter(slug__in=['japan'])),  # asia excluding japan
    'SCH': (Place.objects.filter(slug__in=['schengen']), []),  # schengen
}
regions = {
    '1': "world including usa and canada",
    '2': "world excluding usa and canada but including japan",
    '3': "asia excluding japan",
    'SCH': "schengen",
}
family_strength = {
    'SS': (2, 0),
    'SSC': (2, 1),
    'SS2C': (2, 2),
    'SC': (1, 1),
    'S2C': (1, 2),
}
for data in policy_datas:
    form_data = data.form_data
    for plan_dict in data.plans_list:
        plan, _ = Plan.objects.get_or_create(**{
            'type': ['multi', 'single'][form_data['trip_type'] == 'S'],
            'name': title(regions[form_data['Travelling_To']]),
            'insurer': insurer,
            'grade': grades[plan_dict['plan_type'].strip()],
            'currency': plan_dict['sum_assured'].split(' ')[0],
            'sum_assured': int(''.join(plan_dict['sum_assured'].split(' ')[1].split(','))),
        })
        places_included, places_excluded = places[form_data['Travelling_To']]

        for place in places_included:
            plan.places_included.add(place)

        for place in places_excluded:
            plan.places_excluded.add(place)

        age = calculate_age(
            datetime.datetime.strptime(form_data['DOB'], '%d-%b-%Y').date()
        )
        if plan.type == 'single':
            duration = (datetime.datetime.strptime(form_data['to_date'], '%d-%b-%Y').date() - datetime.datetime.strptime(form_data['from_date'], '%d-%b-%Y').date()).days
            duration += 1
        else:
            duration = int(form_data['max_trip_length'])
        age_ranges = {
            0: 40,
            41: 60,
            61: 70,
            71: 80,
            81: 85,
        }
        for age_min, age_max in age_ranges.items():
            if age >= age_min and age <= age_max:
                break

        if form_data['policy_type'] == 'I':
            if plan.type == 'single':
                duration_range = range(*[duration - 6, duration + 1])
            else:
                duration_range = range(1, 30+1) if duration == 30 else range(31, 45+1) if duration == 45 else range(46, 60+1)

            for duration in duration_range:
                if duration in range(2, 357): #  No plan with duration 1. wtf?
                    exc_premium = float(plan_dict['base_premium'])
                    slab, new = IndividualSlab.objects.get_or_create(**{
                        'plan': plan,
                        'age_min': age_min,
                        'age_max': age_max,
                        'duration': duration,
                        'premium': float(exc_premium),
                    })
                    if new:
                        slab.source = {
                            'form_data': form_data,
                            'plan_data': plan_dict,
                        }
                        slab.save()
            print new, duration_range[0], duration_range[-1], form_data['policy_type'], slab  # Outside the loop?



        else:
            if plan.type == 'single':
                duration_range = range(*[duration - 6, duration + 1])
            else:
                import ipdb;ipdb.set_trace()  # there shouldnt be any multiple trip family plan

            for duration in duration_range:
                if duration in range(2, 357): #  No plan with duration 1. wtf?
                    exc_premium = float(plan_dict['base_premium'])
                    adult_max = family_strength[form_data['family_type']][0]
                    children_max = family_strength[form_data['family_type']][1]
                    slab, new = FamilySlab.objects.get_or_create(**{
                        'plan': plan,
                        'age_min': age_min,
                        'age_max': age_max,
                        'duration': duration,
                        'premium': float(exc_premium),
                        'adult_max': adult_max,
                        'children_max': children_max,
                    })
                    if new:
                        slab.source = {
                            'form_data': form_data,
                            'plan_data': plan_dict,
                        }
                        slab.save()
            print new, duration_range[0], duration_range[-1], form_data['policy_type'], slab  # Outside the loop?
