countries_by_continent = {
	'EUROPE': ['Albania', 'Andorra', 'Armenia', 'Austria', 'Azerbaijan', 'Belarus', 'Belgium', 'Bosnia and Herzegovina ', 'Bulgaria', 'Croatia', 'Cyprus', 'Czech Republic', 'Denmark', 'Estonia', 'Finland', 'France', 'Georgia', 'Germany', 'Greece', 'Hungary', 'Iceland', 'Ireland', 'Italy', 'Kazakhstan', 'Kosovo', 'Latvia', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macedonia', 'Malta', 'Moldova', 'Monaco', 'Montenegro', 'Netherlands', 'Norway', 'Poland', 'Portugal', 'Romania', 'Russia', 'San Marino', 'Serbia', 'Slovakia', 'Slovenia', 'Spain', 'Sweden', 'Switzerland', 'Turkey', 'Ukraine', 'United Kingdom', 'Vatican City (Holy See)',],
	'ASIA': ['Afghanistan', 'Armenia', 'Azerbaijan', 'Bahrain', 'Bangladesh', 'Bhutan', 'Brunei', 'Cambodia', 'China', 'Cyprus', 'Georgia', 'India', 'Indonesia', 'Iran', 'Iraq', 'Israel', 'Japan', 'Jordan', 'Kazakhstan', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Lebanon', 'Malaysia', 'Maldives', 'Mongolia', 'Myanmar (Burma)', 'Nepal', 'North Korea', 'Oman', 'Pakistan', 'Palestine', 'Philippines', 'Qatar', 'Russia', 'Saudi Arabia', 'Singapore', 'South Korea', 'Sri Lanka', 'Syria', 'Taiwan', 'Tajikistan', 'Thailand', 'Timor-Leste', 'Turkey', 'Turkmenistan', 'United Arab Emirates', 'Uzbekistan', 'Vietnam', 'Yemen',],
	'AFRICA': ['Algeria','Angola','Benin','Botswana','Burkina','Burkina Faso','Burundi','Cabo Verde','Cameroon','Cape Verde','Central African Republic','Chad','Comoros','Congo, Democratic Republic of the','Congo, Republic of the',"Cote d'Ivoire",'Djibouti','Egypt','Equatorial Guinea','Eritrea','Ethiopia','Gabon','Gambia','Ghana','Guinea','Guinea-Bissau','Ivory Coast','Kenya','Lesotho','Liberia','Libya','Madagascar','Malawi','Mali','Mauritania','Mauritius','Morocco','Mozambique','Namibia','Niger','Nigeria','Rwanda','Sao Tome and Principe','Senegal','Seychelles','Sierra Leone','Somalia','South Africa','South Sudan','Sudan','Swaziland','Tanzania','Togo','Tunisia','Uganda','Zambia','Zimbabwe'],
	'NORTH AMERICA': ['Antigua and Barbuda', 'Bahamas', 'Barbados', 'Belize', 'Canada', 'Costa Rica', 'Cuba', 'Dominica', 'Dominican Republic', 'El Salvador', 'Grenada', 'Guatemala', 'Haiti', 'Honduras', 'Jamaica', 'Mexico', 'Nicaragua', 'Panama', 'St. Kitts and Nevis', 'St. Lucia', 'St. Vincent and The Grenadines', 'Trinidad and Tobago', 'USA (United States of America)',],
	'SOUTH AMERICA': ['Argentina', 'Bolivia', 'Brazil', 'Chile', 'Colombia', 'Ecuador', 'Guyana', 'Paraguay', 'Peru', 'Suriname', 'Uruguay', 'Venezuela',],
	'OCEANIA': ['Australia', 'Fiji', 'Kiribati', 'Marshall Islands', 'Micronesia', 'Nauru', 'New Zealand', 'Palau', 'Papua New Guinea', 'Samoa', 'Solomon Islands',],}

schengen_countries = ['Austria', 'Belgium', 'Czech republic', 'Denmark', 'Estonia', 'Finland', 'France', 'Germany', 'Greece', 'Hungary', 'Iceland', 'Italy', 'Latvia', 'Lithuania', 'Luxembourg', 'Malta', 'Netherlands', 'Norway', 'Poland', 'Portugal', 'Slovakia', 'Slovenia', 'Spain', 'Sweden', 'Switzerland', 'Liechtenstein',]
non_schengen_countries = ['Albania','Andorra','Armenia','Azerbaijan','Belarus','Bosnia and Herzegovina ','Bulgaria','Croatia','Cyprus','Georgia','Ireland','Kazakhstan','Kosovo','Macedonia','Moldova','Monaco','Montenegro','Romania','Russia','San Marino','Serbia','Turkey','Ukraine','United Kingdom','Vatican City (Holy See)']

worldwide, _ = Place.objects.get_or_create(name='Worldwide')
for continent, countries in countries_by_continent.items():
    continent, _ = Place.objects.get_or_create(name=continent, parent=worldwide)
    for country in countries:
        country = Place.objects.get_or_create(name=country, parent=continent)
        print country

schengen, _ = Place.objects.get_or_create(name='SCHENGEN', parent=Place.objects.get(slug='europe'))
schengen_countries = Place.objects.filter(name__in=schengen_countries)
schengen_countries.update(parent=schengen)

non_schengen, _ = Place.objects.get_or_create(name='NON SCHENGEN', parent=Place.objects.get(slug='europe'))
non_schengen_countries = Place.objects.filter(name__in=non_schengen_countries)
non_schengen_countries.update(parent=non_schengen)

Place.objects.filter(slug__endswith='-2', name__in=['Armenia', 'Azerbaijan', 'Cyprus', 'Georgia', 'Kazakhstan', 'Russia', 'Turkey']).delete()
Place.objects.get(slug='india').delete()
Place.objects.rebuild()
