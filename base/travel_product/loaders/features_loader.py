from django.conf import settings
from django.template.defaultfilters import capfirst, title

from humanize.number import intcomma
from travel_product.models import Insurer, Plan, FeatureName, PlanFeature

import csv


csv_files = []
# # bharti-axa
# csv_files += [('bharti-axa', 'single', (), 'bharti_axa_features_schengen', ['\x80 50,000', '\x80 30,000', '\x80 1,00,000'])]  # Plan not introduced yet, (they are now)
# csv_files += [('bharti-axa', 'single', (), 'bharti_axa_features_non_schengen', ['$50000', '$100000', '$200000', '$300000', '$500000'])]
# csv_files += [('bharti-axa', 'single', (), 'bharti_axa_features_non_schengen_family', ['$50000', '$100000', '$200000', '$300000', '$500000'])]
# csv_files += [('bharti-axa', 'multi', (), 'bharti_axa_features_annual', ['$100000', '$250000', '$500000'])]

# # reliance
# csv_files += [('reliance', 'single', ('World except USA & Canada', 'World including USA & Canada', ), 'reliance_features_worldwide_single_trip', ['$50000', '$100000', '$250000', '$500000'])]
# csv_files += [('reliance', 'single', ('Schengen', ), 'reliance_features_schengen_single_trip', ['\x80 50,000', '\x80 30,000'])]
# csv_files += [('reliance', 'single', ('Asia (except Japan)', ), 'reliance_features_asia_single_trip', ['$25000', '$30000'])]
# csv_files += [('reliance', 'multi', ('World except USA & Canada', 'World including USA & Canada', ), 'reliance_features_worldwide_multi_trip', ['$100000', '$250000', '$500000'])]

# # religare
# csv_files += [('religare', 'single', ('Explore - Asia', ), 'religare_features_single_and_multi_trip', ['USD-Asia-$25000, $50000, $100000'])]
# csv_files += [('religare', 'single', ('Explore - Africa', ), 'religare_features_single_and_multi_trip', ['USD-Africa-$25000, $50000, $100000'])]
# csv_files += [('religare', 'single', ('Explore - Europe', ), 'religare_features_single_trip_schengen', ['EUR-Europe-\x8030000, \x80100000'])]
# csv_files += [('religare', 'single', ('Explore - Canada', ), 'religare_features_single_and_multi_trip', ['USD-GOLDWorldwide-$50000, $100000, $300000, $500000'])]
# csv_files += [('religare', 'single', ('Explore - Gold', ), 'religare_features_single_and_multi_trip', ['USD-GOLDWorldwide-$50000, $100000, $300000, $500000'])]
# csv_files += [('religare', 'single', ('Explore - Platinum', ), 'religare_features_single_and_multi_trip', ['USD-PLATINUMDWorldwide-$50000, $100000, $300000, $500000'])]
# csv_files += [('religare', 'multi', ('Explore - Gold', ), 'religare_features_single_and_multi_trip', ['USD-GOLDWorldwide-$50000, $100000, $300000, $500000'])]
# csv_files += [('religare', 'multi', ('Explore - Platinum', ), 'religare_features_single_and_multi_trip', ['USD-PLATINUMDWorldwide-$50000, $100000, $300000, $500000'])]

# # bajaj-allianz
# csv_files += [('bajaj-allianz', 'single', ('Travel Elite Gold', 'Travel Elite Platinum', 'Travel Elite Silver', ), 'bajaj_features_travel_elite', ['$50,000', '$2,00,000', '$5,00,000'])]
# csv_files += [('bajaj-allianz', 'single', ('Travel Asia Elite Flair', 'Travel Asia Elite Supreme', ), 'bajaj_features_travel_asia_elite', ['$15,000', '$25,000'])]
# csv_files += [('bajaj-allianz', 'single', ('Travel Age', ), 'bajaj_features_travel_age', ['$50,000'])]
# csv_files += [('bajaj-allianz', 'single', ('Travel Age Elite Gold', 'Travel Age Elite Platinum', 'Travel Age Elite Silver', ), 'bajaj_features_travel_age_elite', ['$50,000', '$2,00,000', '$5,00,000'])]
# csv_files += [('bajaj-allianz', 'single', ('Travel Care', ), 'bajaj_features_travel_companion', ['$50,000'])]
# csv_files += [('bajaj-allianz', 'single', ('Travel Secure', ), 'bajaj_features_travel_companion', ['$2,00,000'])]
# csv_files += [('bajaj-allianz', 'single', ('Travel Value', ), 'bajaj_features_travel_companion', ['$5,00,000'])]
# csv_files += [('bajaj-allianz', 'multi', ('Corporate Elite Lite', ), 'bajaj_features_travel_multi_trip_corporate_elite', ['$2,50,000'])]
# csv_files += [('bajaj-allianz', 'multi', ('Corporate Elite Plus', ), 'bajaj_features_travel_multi_trip_corporate_elite', ['$5,00,000'])]
# csv_files += [('bajaj-allianz', 'single', ('Travel Family (Family Floater)', ), 'bajaj_features_family', ['$50,000'])]
# csv_files += [('bajaj-allianz', 'single', ('Travel Elite Family  (Family Floater)', ), 'bajaj_features_elite_family', ['$50,000'])]

# # hdfc-ergo
# csv_files += [('hdfc-ergo', 'single', ('Bronze', ), 'hdfc_features_single_worldwide', ['$30000', ])]
# csv_files += [('hdfc-ergo', 'single', ('Silver', 'Gold', 'Platinum', 'Titanium', ), 'hdfc_features_single_worldwide', ['$50000', '$100000', '$200000', '$500000', ])]
# csv_files += [('hdfc-ergo', 'single', ('Bronze', ), 'hdfc_features_single_asia', ['$15000', ])]
# csv_files += [('hdfc-ergo', 'single', ('Silver', ), 'hdfc_features_single_asia', ['$30000', ])]
# csv_files += [('hdfc-ergo', 'multi', ('Gold', 'Platinum', ), 'hdfc_features_multi_trip', ['$250000', '$500000', ])]
# csv_files += [('hdfc-ergo', 'single', ('Family Floater Silver', ), 'hdfc_features_family_worldwide', ['$50000', ])]

# # apollo-munich
csv_files += [('apollo-munich', 'single', ('Platinum', 'Gold', 'Silver', 'Bronze', 'Asian'), 'apollo-munich_individual_features', ['$500,000', '$250,000', '$100,000', '$50,000', '$25,000'])]


for insurer_slug, type, names, csv_file, sum_assureds in csv_files:
	insurer = Insurer.objects.get(slug=insurer_slug)
	parent_feature_name = None
	with open(settings.SETTINGS_FILE_FOLDER+'/travel_product/loaders/'+insurer_slug+'/'+csv_file+'.csv', 'rt') as f:
		reader = csv.DictReader(f)
		for row in reader:
			if not parent_feature_name or not row['Condition']:
				if not row['Condition'] and '.' in row['Plan Benefits']:
					try:
						parent_feature_name, _ = FeatureName.objects.get_or_create(name=title(row['Plan Benefits'].split('. ')[1].strip()))
						if _:
							# import ipdb;ipdb.set_trace()  # No new names needed i think
							pass
					except:
						import ipdb;ipdb.set_trace()  # Something not right
						pass
				else:
					continue
			else:
				for verbose_sum_assured in sum_assureds:
					if verbose_sum_assured in sum_assureds:
						if insurer.slug == 'religare':  # Idk man religare csv is complex
							currency, _plan_name, various_sum_assureds = verbose_sum_assured.replace('\x80', 'EUR').split('-')
							various_sum_assureds = map(int, various_sum_assureds.replace('EUR', '').replace('$', '').split(', '))
						else:
							try:
								currency, sum_assured = verbose_sum_assured.replace('\x80', 'EUR').split(' ')
							except ValueError:
								currency, sum_assured = verbose_sum_assured.replace('$', 'USD ').split(' ')

							sum_assured = int(''.join(sum_assured.split(',')))
							various_sum_assureds = [sum_assured]
						plans = Plan.objects.filter(**{
							'insurer': insurer,
							'sum_assured__in': various_sum_assureds,
							'type': type,
							'currency': currency,
						})
						plans = plans.filter(name__in=names) if names else plans
						for plan in plans:
							benefit = {
											"Loss Of Checked-In Baggage": "Baggage Loss Cover",
											"Pre-Existing Diseases": "Existing Diseases Cover",
											"Dental": "Dental Cover",
											"Accidental Death & Dismemberment Common Carrier": "Accidental Death & Disability (Public Transport)",
											"Medical Expenses": "Medical Cover",
													}.get(title(row['Plan Benefits']), title(row['Plan Benefits']))
							feature_name, _ = FeatureName.objects.get_or_create(name=benefit, parent=parent_feature_name)
							if _:
								# import ipdb;ipdb.set_trace()  # No new names needed i think
								pass
							cover = row[verbose_sum_assured].strip().replace('\x80', 'EUR').replace('EUR', u'\u20ac')

							### SANITISE
							Plan.CURRENCY_CHOICES = (
								('USD', '$'),
								('EUR', u'\u20ac'),
								('INR', 'Rs'),
							)
							try:
								old_cover = cover
								cover = cover.replace(',', '').replace(' ', '').replace('$', 'USD').replace(u'\u20ac', 'EUR')
								currency = 'USD' if 'USD' in cover else 'EUR'
								cover = dict(Plan.CURRENCY_CHOICES)[currency] + intcomma(int(cover.split(currency)[1]))
							except (IndexError, ValueError):
								print 'EXCEPTION on', old_cover
								cover = old_cover

							###
							# cover = 'Not Covered' if cover == 'NA' else cover
							deductible = row['Deductible'].strip().replace('\x80', 'EUR').replace('EUR', u'\u20ac')
							# deductible = '0' if deductible == 'NA' else deductible
							remarks = capfirst(row['Condition'].strip())
							plan_features = PlanFeature.objects.filter(**{'name': feature_name, 'plan': plan})
							if not plan_features.exists():
								try:
									if feature_name.pk != 554 and cover != 'NA':
										# import ipdb;ipdb.set_trace()
										pass
									plan_feature = PlanFeature.objects.create(**{
										'name': feature_name,
										'plan': plan,
										'cover': None if cover in ['NA', '', '-'] else cover,
										'deductible': None if deductible in ['NA', '', '-'] else deductible,
										'remarks': None if remarks in ['NA', '', '-'] else remarks,
									})
								except:
									import ipdb;ipdb.set_trace()
									pass
							else:
								try:
									plan_feature = plan_features.get(**{
										'name': feature_name,
										'plan': plan,
										'cover': None if cover in ['NA', '', '-'] else cover,
										'deductible': None if deductible in ['NA', '', '-'] else deductible,
										'remarks': None if remarks in ['NA', '', '-'] else remarks,
									})
									print 'DUPLICATED'
								except PlanFeature.DoesNotExist:
									if '/' not in deductible:  # per plan deductibles
										import ipdb;ipdb.set_trace()
									pass
									#  Same plan has different features under same featurename? familyslab and individual slab must be sharing a same plan then i guess

							print plan_feature
							if not plan_feature.cover:
								plan_feature.delete()
								print 'DELETED'
					else:
						import ipdb;ipdb.set_trace()
						pass
						#  sum_assureds list malformed
