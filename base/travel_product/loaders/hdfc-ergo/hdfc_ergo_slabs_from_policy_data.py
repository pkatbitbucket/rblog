from django.template.defaultfilters import title
import json, datetime
insurer, _ = Insurer.objects.get_or_create(name='HDFC ERGO', slug='hdfc-ergo')
policy_datas = InsurerPolicyData.objects.filter(insurer=insurer.slug)
# policy_datas = policy_datas.filter(pk__gt=58029L, pk__lte=58107L)  # worldwide including excluding individual single trip
# policy_datas = policy_datas.filter(pk__gt=58107L, pk__lte=58122L)  # Asia - Excluding Japan individual single trip
# policy_datas = policy_datas.filter(pk__gt=58122L, pk__lte=58131L)  # Family type
policy_datas = policy_datas.filter(pk__gt=58131L, pk__lte=58149L)  # Annual trip
# policy_datas = policy_datas.filter(pk__gt=48944L)  # schengen plans family BECAREFUL, THE PLANS SHOULD BE IN EUROS AND NOT USD

def calculate_age(birth_date):
    today = datetime.date.today()
    try:
        birth_day = birth_date.replace(year=today.year)
    except ValueError:  # raised when birth date is February 29 and the current year is not a leap year
        birth_day = birth_date.replace(year=today.year, month=birth_date.month+1, day=1)
    if birth_day > today:
        return today.year - birth_date.year - 1
    else:
        return today.year - birth_date.year

grades = {
    'Bronze': Grade.objects.get(slug='silver'),
    'Silver': Grade.objects.get(slug='gold'),
    'Gold': Grade.objects.get(slug='diamond'),
    'Platinum': Grade.objects.get(slug='platinum'),
    'Titanium': Grade.objects.get(slug='titanium'),
    'Family Floater Silver': Grade.objects.get(slug='silver'),
}
names = {
    '30,000': 'Bronze',
    '50,000': 'Silver',
    '100,000': 'Gold',
    '200,000': 'Platinum',
    '500,000': 'Titanium',
    'Single Trip Asia Bronze Excluding Japan': 'Bronze',
    'Single Trip Asia Silver Excluding Japan': 'Silver',
    'Multi Trip Worldwide Gold': 'Gold',
    'Multi Trip Worldwide Platinum': 'Platinum',
    'Family Floater Silver': 'Family Floater Silver',
    'Multi Trip Worldwide Gold - 30 Days': 'Gold',
    'Multi Trip Worldwide Gold - 45 Days': 'Gold',
    'Multi Trip Worldwide Platinum - 30 Days': 'Platinum',
    'Multi Trip Worldwide Platinum - 45 Days': 'Platinum',
}
places = {
    'Asia - Excluding Japan': (Place.objects.filter(slug__in=['asia']), Place.objects.filter(slug__in=['japan'])),  # asia excluding japan
    'WorldWide-Excluding USA/Canada': (Place.objects.filter(slug__in=['worldwide']), Place.objects.filter(slug__in=['usa-united-states-of-america', 'canada'])),  # world excluding usa and canada but including japan
    'WorldWide-Including USA/Canada': (Place.objects.filter(slug__in=['worldwide']), []),  # world including usa and canada
    'Worldwide': (Place.objects.filter(slug__in=['worldwide']), []),  # world including usa and canada
}
family_strength = {
    'self+spouse': (2, 0),
    'self+spouse+child1': (2, 1),
    'self+spouse+child1+child2': (2, 2),
}
individual_durations = [0, ] + [4, 7, 14, 21, 28, 35, 47, 60, 75, 90, 120, 150, 180, ]
asia_individual_durations = [0, ] + [4, 7, 14, 21, 30]
annual_durations = [0, ] + [30, 45, ]
family_durations = [0, ] + [15, 30, 60, ]
for data in policy_datas:
    form_data = data.form_data
    for plan_dict in data.plans_list:
        name = names.get(title(plan_dict['plan_type'].split('-')[-1])) or names.get(plan_dict['plan_type'])
        plan, _ = Plan.objects.get_or_create(**{
            'type': ['multi', 'single'][form_data.get('trip_type') == 'S' or form_data.get('policy_type') == 'F'],
            'name': name,
            'insurer': insurer,
            'grade': grades[name],
            'currency': plan_dict['sum_assured'].split(' ')[0],
            'sum_assured': int(''.join(plan_dict['sum_assured'].split(' ')[1].split(','))),
            'verbose_name': form_data['geographical_coverage'],
        })
        places_included, places_excluded = places[form_data['geographical_coverage']]

        for place in places_included:
            plan.places_included.add(place)

        for place in places_excluded:
            plan.places_excluded.add(place)

        age = calculate_age(
            datetime.datetime.strptime(form_data['dob'], '%d-%b-%Y').date()
        )
        if plan.type == 'single':
            duration = int(form_data['trip_duration'])
        else:
            duration = int(form_data['max_trip_length'])

        if form_data.get('policy_type') == 'F':
            age_ranges = {
                0: 60,
            }
        else:
            age_ranges = {
                0: 40,
                41: 60,
                61: 70,
            }
        for age_min, age_max in age_ranges.items():
            if age >= age_min and age <= age_max:
                break

        if form_data['policy_type'] == 'I':
            if plan.type == 'single':
                if form_data['geographical_coverage'] == 'Asia - Excluding Japan':
                    durations = asia_individual_durations
                else:
                    durations = individual_durations
            else:
                durations = annual_durations
            index = durations.index(duration)
            duration_range = range(durations[index-1] + 1, durations[index]+1)

            exc_premium = float(plan_dict['base_premium'])
            for duration in duration_range:
                if duration in range(1, 46):
                    slab, new = IndividualSlab.objects.get_or_create(**{
                        'plan': plan,
                        'age_min': age_min,
                        'age_max': age_max,
                        'duration': duration,
                        'premium': float(exc_premium),
                    })
                    if new:
                        slab.source = {
                            'form_data': form_data,
                            'plan_data': plan_dict,
                        }
                        slab.save()
                else:
                    import ipdb;ipdb.set_trace()
            print new, duration_range[0], duration_range[-1], form_data['policy_type'], slab  # Outside the loop?



        else:
            if plan.type == 'single':
                durations = family_durations
                index = durations.index(duration)
                duration_range = range(durations[index-1] + 1, durations[index]+1)
            else:
                import ipdb;ipdb.set_trace()  # there shouldnt be any multiple trip family plan

            for duration in duration_range:
                if duration in range(1, 61):
                    exc_premium = float(plan_dict['base_premium'])
                    adult_max = family_strength[form_data['family_type']][0]
                    children_max = family_strength[form_data['family_type']][1]
                    slab, new = FamilySlab.objects.get_or_create(**{
                        'plan': plan,
                        'age_min': age_min,
                        'age_max': age_max,
                        'duration': duration,
                        'premium': float(exc_premium),
                        'adult_max': adult_max,
                        'children_max': children_max,
                    })
                    if new:
                        slab.source = {
                            'form_data': form_data,
                            'plan_data': plan_dict,
                        }
                        slab.save()
                else:
                    import ipdb;ipdb.set_trace()
            print new, duration_range[0], duration_range[-1], form_data['policy_type'], slab  # Outside the loop?
