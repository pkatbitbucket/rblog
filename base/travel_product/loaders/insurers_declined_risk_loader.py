declined_risks = {
	'Cancer/Leukemia/Malignant Tumor': ('reliance', 'star-health'),

	'Cardiac ailments': ('reliance', 'star-health'),

	'Chronic Obstructive Pulmonary Disease (COPD)': ('reliance', 'star-health'),

	'HIV/AIDS': ('reliance', ),

	'Type I Diabetes Mellitus (Insulin dependent)': ('reliance', 'star-health'),

	'Kidney ailments': ('reliance', 'star-health'),

	'Liver diseases': ('reliance', 'star-health'),

	'Neurological disorders (Parkinson\'s/Alzheimer\'s disease)': ('reliance', 'star-health'),

	'Stroke/Paralysis': ('reliance', 'star-health'),

	'Thalassemia': ('reliance', ),

	'Psycho-somatic disorders (Stress related disorders)': ('star-health', ),

	'Auto-immune/Connective tissue disorders (long-term steroids)': ('star-health', ),

	'Long-term anticoagulant theray': ('star-health', ),
}

for malady, insurers in declined_risks.items():
	declined_risk, _ = DeclinedRisk.objects.get_or_create(malady=malady)
	declined_risk.insurer_set.through.objects.filter(declinedrisk=declined_risk)

	for insurer in insurers:
		declined_risk.insurer_set.add(Insurer.objects.get(slug=insurer))
