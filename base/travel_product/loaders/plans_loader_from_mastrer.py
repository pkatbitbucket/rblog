import csv


with open(settings.SETTINGS_FILE_FOLDER+'/travel_product/loaders/list_of_all_plans_master.csv', 'rt') as f:
	reader = csv.DictReader(f)
	for row in reader:
		if not row['Insurer']:
			continue
		plans = Plan.objects.filter(**{
			'insurer__name': row['Insurer'],
			'name': row['Plan name as per our database'],
			'sum_assured': row['Sum assured'],
			'currency': row['Currency'],
			'type': row['Type'],
		})
		for plan in plans:
			plan.verbose_name = plan.name if not plan.verbose_name else plan.verbose_name
			plan.name = (row['Correct Name'] or row['Plan name as per insurer(I guess) & Please update in a new adjacent column if needed']).strip()
			plan.save()
			try:
				print plan
			except Exception as e:
				# import ipdb;ipdb.set_trace()
				pass
