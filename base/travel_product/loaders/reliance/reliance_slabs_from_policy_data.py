import json, datetime
insurer = Insurer.objects.get(slug='reliance')
policy_datas = InsurerPolicyData.objects.filter(insurer=insurer.slug)

def calculate_age(birth_date):
    today = datetime.date.today()
    try:
        birth_day = birth_date.replace(year=today.year)
    except ValueError:  # raised when birth date is February 29 and the current year is not a leap year
        birth_day = birth_date.replace(year=today.year, month=birth_date.month+1, day=1)
    if birth_day > today:
        return today.year - birth_date.year - 1
    else:
        return today.year - birth_date.year


BOOL = {
    'yes': 'True',
    'no': 'False',
    True: 'True',
    False: 'False',
    '':''
}
grades = {
    'Basic': Grade.objects.get(slug='silver'),
    'Standard': Grade.objects.get(slug='gold'),
    'Silver': Grade.objects.get(slug='diamond'),
    'Gold': Grade.objects.get(slug='platinum'),
    'Platinum': Grade.objects.get(slug='titanium'),

    'Plus': Grade.objects.get(slug='diamond'),
    'Elite': Grade.objects.get(slug='platinum'),
}
places = {
    'Asia (except Japan)': (Place.objects.filter(slug__in=['asia']), Place.objects.filter(slug__in=['japan'])),
    'Schengen': (Place.objects.filter(slug__in=['schengen']), []),
    'US/Canada': (Place.objects.filter(slug__in=['usa-united-states-of-america', 'canada']), []),
    'World except USA & Canada': (Place.objects.filter(slug__in=['worldwide']), Place.objects.filter(slug__in=['usa-united-states-of-america', 'canada', 'schengen'])),
    'World including USA & Canada': (Place.objects.filter(slug__in=['worldwide']), Place.objects.filter(slug__in=['schengen'])),
}
adult_children_max = {
    "Applicant + Spouse": (2, 0),
    "Applicant + Spouse + Child1": (2, 1),
    "Applicant + Spouse + Child1 + Child2": (2, 2),
    "Applicant + Child1": (1, 1),
    "Applicant + Child1 + Child2": (1, 2),
}
for data in policy_datas:
    form_data = data.form_data
    form_data['region'] = form_data['region'] if 'region' in form_data else ['World except USA & Canada', 'World including USA & Canada'][form_data['visiting_usa'] == 'yes']
    for plan_dict in data.plans_list:
        plan, _ = Plan.objects.get_or_create(**{
            'type': ['multi', 'single'][form_data['trip_type'] == 'single'],
            'name': form_data['region'],
            'insurer': insurer,
            'grade': grades[plan_dict['plan_type'].strip()],
            'currency': plan_dict['sum_assured'].split(' ')[0],
            'sum_assured': int(''.join(plan_dict['sum_assured'].split(' ')[1].split(','))),
            'nri': True,
        })
        places_included, places_excluded = places[form_data['region']]

        for place in places_included:
            plan.places_included.add(place)

        for place in places_excluded:
            plan.places_excluded.add(place)

        age = calculate_age(
            datetime.datetime.strptime(form_data['dob'], '%d-%b-%Y').date()
        )
        if plan.type == 'single':
            duration = (datetime.datetime.strptime(form_data['to_date'], '%d-%b-%Y').date() - datetime.datetime.strptime(form_data['from_date'], '%d-%b-%Y').date()).days
            duration += 1
        else:
            duration = int(form_data['max_trip_length'])
        age_ranges = (([0, 40], []), ([41, 60], [61, 70]), )
        age_min, age_max = age_ranges[age > 40][age > 60]
        if form_data['traveller_type'] == 'Individual':
            if plan.type == 'single':
                if form_data['region'] != 'Asia (except Japan)':
                    duration_range = range(*[duration - 6, duration + 1])
                else:
                    duration_range = {
                        4: [1, 4],
                        7: [5, 7],
                        14: [8, 14],
                        21: [15, 21],
                        30: [22, 30]
                    }[duration]
                    duration_range = range(*duration_range) + [duration_range[-1] + 1]
            else:
                duration_range = range(1, 30+1) if duration == 30 else range(31, 45+1)

            inc_premium = float(plan_dict['premium'])
            exc_premium = round((inc_premium*100)/114)
            for duration in duration_range:
                slab, new = IndividualSlab.objects.get_or_create(**{
                    'plan': plan,
                    'age_min': age_min,
                    'age_max': age_max,
                    'duration': duration,
                    'premium': float(exc_premium),
                })
                if new:
                    slab.source = {
                        'form_data': form_data,
                        'plan_data': plan_dict,
                    }
                    slab.save()
            print new, duration_range[0], duration_range[-1],  slab  # Outside the loop?



        else:
            duration_range = range(*[duration - 6, duration + 1])

            inc_premium = float(plan_dict['premium'])
            exc_premium = round((inc_premium*100)/114)
            for duration in duration_range:
                slab, new = FamilySlab.objects.get_or_create(**{
                    'plan': plan,
                    'age_min': age_min,
                    'age_max': age_max,
                    'duration': duration,
                    'premium': float(exc_premium),
                    'adult_max': adult_children_max[form_data['cover_type']][0],
                    'children_max': adult_children_max[form_data['cover_type']][1],
                })
                if new:
                    slab.source = {
                        'form_data': form_data,
                        'plan_data': plan_dict,
                    }
                    slab.save()
            print new, duration_range[0], duration_range[-1],  slab  # Outside the loop?




