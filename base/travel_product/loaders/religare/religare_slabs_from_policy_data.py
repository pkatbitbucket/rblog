from travel_product.insurers.religare.integration import get_sum_insured_in_int

import json, datetime
insurer = Insurer.objects.get(slug='religare')
policy_datas = InsurerPolicyData.objects.filter(insurer=insurer.slug)
# policy_datas = policy_datas.filter(pk__gt=10248)  # single
# policy_datas = policy_datas.filter(pk__gt=46648L)  # multiple
# policy_datas = policy_datas.filter(pk__gt=47227L)  # explore europe missing plans with sum assured as 30000

"""
[{'premium': 47016.0, 'plan_type': 'Explore - Gold - Worldwide', 'sum_assured': 'US $ 5,00,000'}]
{'from_date': '23-Jul-2015', 'member1_age_band': '61  Years - 70 Years', 'no_of_travellers': '1', 
'to_date': '09-Jan-2016', 'plan': 'Explore - Gold - Worldwide', 'sum_insured': 'US $ 5,00,000', 
'trip_type': 'single'}
"""

grades = {
    'Explore - Asia': Grade.objects.get(slug='silver'),
    'Explore - Europe': Grade.objects.get(slug='silver'),
    'Explore - Canada': Grade.objects.get(slug='silver'),
    'Explore - Africa': Grade.objects.get(slug='silver'),
    'Explore - Gold - Worldwide': Grade.objects.get(slug='gold'),
    'Explore - Gold - Worldwide - Excluding US & Canada': Grade.objects.get(slug='gold'),
    'Explore - Platinum - Worldwide': Grade.objects.get(slug='platinum'),
    'Explore - Platinum - Worldwide - Excluding US & Canada': Grade.objects.get(slug='platinum'),
}

places = {
    'Explore - Asia': (Place.objects.filter(slug__in=['asia']), []),
    'Explore - Europe': (Place.objects.filter(slug__in=['europe']), []),
    'Explore - Canada': (Place.objects.filter(slug__in=['canada']), []),
    'Explore - Africa': (Place.objects.filter(slug__in=['africa']), []),
    'Explore - Gold - Worldwide': (Place.objects.filter(slug__in=['worldwide']), []),
    'Explore - Gold - Worldwide - Excluding US & Canada': (Place.objects.filter(slug__in=['worldwide']), Place.objects.filter(slug__in=['usa-united-states-of-america', 'canada'])),
    'Explore - Platinum - Worldwide': (Place.objects.filter(slug__in=['worldwide']), []),
    'Explore - Platinum - Worldwide - Excluding US & Canada': (Place.objects.filter(slug__in=['worldwide']), Place.objects.filter(slug__in=['usa-united-states-of-america', 'canada'])),
}

age_ranges = {
    "up to 40 Years": [0, 40],
    "41 Years - 60 Years": [41, 60],
    "61  Years - 70 Years": [61, 70],
    "> 70 Years": [71, 150],
}
number_of_policies = policy_datas.count()
counter = 0
for data in policy_datas:
    counter += 1
    form_data = data.form_data
    for plan_dict in data.plans_list:
        plan, _ = Plan.objects.get_or_create(**{
            'type': ['single', 'multi']['max_trip_length' in form_data],
            'name': form_data['plan'],
            'insurer': insurer,
            'grade': grades[plan_dict['plan_type'].strip()],
            'currency': ['EUR', 'USD']['US $' in plan_dict['sum_assured']],
            'sum_assured': get_sum_insured_in_int(plan_dict['sum_assured']),
            'nri': True,
        })
        places_included, places_excluded = places[plan.name]

        for place in places_included:
            plan.places_included.add(place)

        for place in places_excluded:
            plan.places_excluded.add(place)

        if plan.type == 'single':
            duration = (datetime.datetime.strptime(form_data['to_date'], '%d-%b-%Y').date() - datetime.datetime.strptime(form_data['from_date'], '%d-%b-%Y').date()).days
            duration += 1
        else:
            duration = int(form_data['max_trip_length'])
        age_min, age_max = age_ranges[form_data['member1_age_band']]
        if form_data['no_of_travellers'] == '1': # 'individual'

            if plan.type == 'single':
                duration_range = [duration]
            else:
                duration_range = range(1, 45+1) if duration == 45 else range(46, 60+1)

            inc_premium = float(plan_dict['premium'])
            exc_premium = round((inc_premium*100)/114)  # 14 tax was inclusive

            for duration in duration_range:
                slab, new = IndividualSlab.objects.get_or_create(**{
                    'plan': plan,
                    'age_min': age_min,
                    'age_max': age_max,
                    'duration': duration,
                    'premium': exc_premium,
                })

                if new:
                    slab.source = {
                        'form_data': form_data,
                        'plan_data': plan_dict,
                    }
                    slab.save()
            print new, duration, slab  # Outside the loop?
            print str(counter) + ' of ' + str(number_of_policies)
        else:
            import ipdb;ipdb.set_trace()
            #  There is no family