from django.template.defaultfilters import title
import datetime
from travel_product.models import Grade, Insurer, InsurerPolicyData, Plan, Place, IndividualSlab


insurer, _ = Insurer.objects.get_or_create(slug='universal-sompo', name='Universal Sompo')
policy_datas = InsurerPolicyData.objects.filter(insurer='universal-sompo')
number_of_policies = policy_datas.count()

grades = {'Gold': Grade.objects.get(slug='gold'),
          'Silver': Grade.objects.get(slug='silver'),
          'Platinum': Grade.objects.get(slug='platinum')}

places = {'Travel-Asia': (Place.objects.filter(slug__in=['asia']), []),  # asia excluding japa
          'Travel-Worldwide Excluding US & Canada': (Place.objects.filter(slug__in=['worldwide']), Place.objects.filter(slug__in=['usa-united-states-of-america', 'canada'])),  # world excluding usa and canada
          'Travel-Worldwide': (Place.objects.filter(slug__in=['worldwide']), [])}  # world including usa and canada

individual_worldwide_duration = [0, ] + [7, 14, 21, 28, 35, 47, 60, 75, 90, 120, 150, 180]
individual_asia_duration = [0, ] + [4, 7, 14, 21, 30, 47]
counter = 0
for data in policy_datas:
    form_data = data.form_data
    for plan_dict in data.plans_list:
        plan, _ = Plan.objects.get_or_create(**{
            'type': 'single',
            'name': title(plan_dict['plan_type']),
            'insurer': insurer,
            'grade': grades[form_data['member1_plan_type']],
            'currency': 'USD',
            'sum_assured': int(plan_dict['sum_assured']),
            'verbose_name': form_data['plan'],
        })
        places_included, places_excluded = places[form_data['plan']]

        for place in places_included:
            plan.places_included.add(place)

        for place in places_excluded:
            plan.places_excluded.add(place)

        duration = (datetime.datetime.strptime(form_data['to_date'], '%d-%b-%Y').date() - datetime.datetime.strptime(form_data['from_date'], '%d-%b-%Y').date()).days
        duration += 1
        if (form_data['member1_age_band'] == 'up to 40 Years'):
            age_min = 0
            age_max = 40
        elif(form_data['member1_age_band'] == '41 Years - 60 Years'):
            age_min = 41
            age_max = 60

        if form_data['no_of_travellers'] == '1':
            if(form_data['plan'] == 'Travel-Worldwide' or form_data['plan'] == 'Travel-Worldwide Excluding US & Canada'):
                index = individual_worldwide_duration.index(duration)
                duration_range = range(individual_worldwide_duration[index - 1] + 1, individual_worldwide_duration[index] + 1)
            elif(form_data['plan'] == 'Travel-Asia'):
                index = individual_asia_duration.index(duration)
                duration_range = range(individual_asia_duration[index - 1] + 1, individual_asia_duration[index] + 1)
            inc_premium = float(plan_dict['premium'])
            exc_premium = round((inc_premium * 100) / 114)
            for duration in duration_range:
                count = 0
                slab, new = IndividualSlab.objects.get_or_create(**{
                    'plan': plan,
                    'age_min': age_min,
                    'age_max': age_max,
                    'duration': duration,
                    'premium': float(exc_premium),
                })
                if new:
                    slab.source = {
                        'form_data': form_data,
                        'plan_data': plan_dict,
                    }
                    slab.save()
                count = count + 1
            print new, duration, plan.pk, slab
            counter = counter + 1
            print str(counter) + ' of ' + str(number_of_policies)
