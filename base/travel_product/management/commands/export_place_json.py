from django.core.management.base import BaseCommand
from django.conf import settings
from path import path
import json
import os

from travel_product.models import Place


class Command(BaseCommand):
    def handle(self, *args, **options):
        print ">> EXPORTING TRAVEL PLACES TO JSON "
        try:
            static = settings.STATIC_ROOT or os.path.join(settings.SETTINGS_FILE_FOLDER, '../static')
            # create the directory if it does not exists
            path(os.path.join(static, 'travel_product/v1/src/js/utils')).makedirs_p()

            dump = json.dumps(
                list(Place.objects.filter(display=True).distinct().values_list('slug', 'name').order_by('name')),
                indent=4,
            )
            file_path = os.path.join(static, 'travel_product/v1/src/js/utils/individual_travel_places.js')
            with open(file_path, 'wb') as fd:
                fd.write("module.exports = " + dump)
            print ">> MISSION SUCCESSFUL"
        except Exception as e:
            print "EXPORTING FUCKED UP: %s" % e
