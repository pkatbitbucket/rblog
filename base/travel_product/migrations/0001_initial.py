# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
import travel_product.models
import customdb.thumbs
import django_extensions.db.fields
import picklefield.fields
import autoslug.fields
import mptt.fields
import core.fields
import django.core.validators
import core.models

import travel_product


class Migration(migrations.Migration):

    dependencies = [
        ('core', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('id', models.UUIDField(primary_key=True, serialize=False, editable=False, blank=True, db_index=True)),
                ('data', picklefield.fields.PickledObjectField(default={}, editable=False)),
                ('tracker', models.ForeignKey(related_name='travel_activities', to='core.Tracker')),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='DeclinedRisk',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('malady', models.CharField(max_length=100)),
                ('slug', autoslug.fields.AutoSlugField(unique=True, editable=False)),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='Feature',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('cover', models.CharField(max_length=100, null=True, blank=True)),
                ('deductible', models.CharField(max_length=100, null=True, blank=True)),
                ('remarks', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='FeatureName',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('slug', autoslug.fields.AutoSlugField(unique=True, editable=False)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', default=None, blank=True, to='travel_product.FeatureName', null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='Grade',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=200)),
                ('slug', autoslug.fields.AutoSlugField(unique=True, editable=False)),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='Insurer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=200)),
                ('slug', autoslug.fields.AutoSlugField(unique=True, max_length=100, editable=False)),
                ('logo', customdb.thumbs.ImageWithThumbsField(null=True, upload_to=travel_product.models.get_insurer_logo_path, blank=True)),
                ('proposal_form', picklefield.fields.PickledObjectField(default=None, null=True, editable=False, blank=True)),
                ('nri', models.BooleanField(default=False)),
                ('minimum_residency', models.FloatField(help_text=b'years', null=True, blank=True)),
                ('minimum_processing_days', models.PositiveIntegerField(help_text=b'from date within these many days in case of any ped', null=True, blank=True)),
                ('active', models.BooleanField(default=True)),
                ('declined_risks', models.ManyToManyField(to='travel_product.DeclinedRisk', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='InsurerPolicyData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('insurer', models.CharField(max_length=255, db_index=True)),
                ('form_data', jsonfield.fields.JSONField(default={}, blank=True)),
                ('hash_form_data', models.CharField(db_index=True, unique=True, max_length=255, blank=True)),
                ('plans_list', jsonfield.fields.JSONField(default={}, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Place',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('slug', autoslug.fields.AutoSlugField(unique=True, editable=False)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('alias', models.ForeignKey(blank=True, to='travel_product.Place', null=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', default=None, blank=True, to='travel_product.Place', null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='Plan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=100)),
                ('verbose_name', models.CharField(max_length=100, null=True, blank=True)),
                ('type', models.CharField(max_length=20, choices=[(b'single', b'Single Trip'), (b'multi', b'Multi Trip / Annual')])),
                ('currency', models.CharField(default=b'USD', max_length=3, choices=[(b'USD', b'$'), (b'EUR', '\u20ac'), (b'INR', b'Rs')])),
                ('sum_assured', models.PositiveIntegerField()),
                ('features_hash', models.CharField(max_length=64)),
                ('active', models.BooleanField(default=True)),
                ('grade', models.ForeignKey(to='travel_product.Grade')),
                ('insurer', models.ForeignKey(to='travel_product.Insurer')),
                ('places_excluded', mptt.fields.TreeManyToManyField(related_name='excluded_plans', to='travel_product.Place', blank=True)),
                ('places_included', mptt.fields.TreeManyToManyField(related_name='included_plans', to='travel_product.Place')),
            ],
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='Slab',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('age_min', core.fields.SmallIntegerRangeField(default=0, verbose_name='Age from', validators=[django.core.validators.MaxValueValidator(100), django.core.validators.MinValueValidator(0)])),
                ('age_max', core.fields.SmallIntegerRangeField(default=100, verbose_name='Age to', validators=[django.core.validators.MaxValueValidator(100), django.core.validators.MinValueValidator(0)])),
                ('premium', models.FloatField()),
                ('source', picklefield.fields.PickledObjectField(default={}, editable=False)),
                ('active', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('id', models.UUIDField(primary_key=True, serialize=False, editable=False, blank=True, db_index=True)),
                ('data', picklefield.fields.PickledObjectField(default={}, editable=False)),
                ('transaction_id', models.CharField(max_length=36, null=True, blank=True)),
                ('status', models.CharField(default=b'DRAFT', max_length=20, choices=[(b'DRAFT', b'Draft'), (b'PENDING', b'Pending'), (b'COMPLETED', b'Completed'), (b'FAILED', b'Failed')])),
                ('status_history', picklefield.fields.PickledObjectField(default=(), editable=False)),
                ('email', models.EmailField(max_length=255, null=True, db_index=True)),
                ('first_name', models.CharField(max_length=50, null=True, blank=True)),
                ('last_name', models.CharField(max_length=50, null=True, blank=True)),
                ('mobile', models.CharField(max_length=50, null=True, blank=True)),
                ('payment_request', models.TextField(blank=True)),
                ('payment_response', models.TextField(blank=True)),
                ('payment_on', models.DateTimeField(null=True, blank=True)),
                ('activity', models.ForeignKey(related_name='travel_transactions', to='travel_product.Activity')),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='FamilySlab',
            fields=[
                ('slab_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='travel_product.Slab')),
                ('children_max', core.fields.SmallIntegerRangeField(default=2, verbose_name='Maximum number of children', validators=[django.core.validators.MaxValueValidator(30), django.core.validators.MinValueValidator(0)])),
                ('adult_max', core.fields.SmallIntegerRangeField(default=2, verbose_name='Maximum number of adults', validators=[django.core.validators.MaxValueValidator(30), django.core.validators.MinValueValidator(0)])),
                ('size', core.fields.SmallIntegerRangeField(default=4, blank=True, verbose_name='Total family size', validators=[django.core.validators.MaxValueValidator(60), django.core.validators.MinValueValidator(0)])),
                ('duration', core.fields.SmallIntegerRangeField(default=30, verbose_name='Trip duration', validators=[django.core.validators.MaxValueValidator(365), django.core.validators.MinValueValidator(1)])),
            ],
            options={
                'abstract': False,
            },
            bases=('travel_product.slab',),
        ),
        migrations.CreateModel(
            name='IndividualSlab',
            fields=[
                ('slab_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='travel_product.Slab')),
                ('duration', core.fields.SmallIntegerRangeField(default=30, verbose_name='Trip duration', validators=[django.core.validators.MaxValueValidator(365), django.core.validators.MinValueValidator(1)])),
            ],
            options={
                'abstract': False,
            },
            bases=('travel_product.slab',),
        ),
        migrations.CreateModel(
            name='PlanFeature',
            fields=[
                ('feature_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='travel_product.Feature')),
                ('plan', models.ForeignKey(related_name='features', to='travel_product.Plan')),
            ],
            options={
                'abstract': False,
            },
            bases=('travel_product.feature',),
        ),
        migrations.CreateModel(
            name='SlabFeature',
            fields=[
                ('feature_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='travel_product.Feature')),
            ],
            options={
                'abstract': False,
            },
            bases=('travel_product.feature',),
        ),
        migrations.CreateModel(
            name='StudentSlab',
            fields=[
                ('slab_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='travel_product.Slab')),
                ('duration', core.fields.SmallIntegerRangeField(default=365, verbose_name='Trip duration', validators=[django.core.validators.MaxValueValidator(1460), django.core.validators.MinValueValidator(1)])),
            ],
            options={
                'abstract': False,
            },
            bases=('travel_product.slab',),
        ),
        migrations.AddField(
            model_name='transaction',
            name='slab',
            field=models.ForeignKey(to='travel_product.Slab'),
        ),
        migrations.AddField(
            model_name='slab',
            name='plan',
            field=models.ForeignKey(to='travel_product.Plan'),
        ),
        migrations.AddField(
            model_name='feature',
            name='name',
            field=mptt.fields.TreeForeignKey(to='travel_product.FeatureName'),
        ),
        migrations.AddField(
            model_name='slabfeature',
            name='slab',
            field=models.ForeignKey(related_name='features', to='travel_product.Slab'),
        ),
        migrations.AlterUniqueTogether(
            name='plan',
            unique_together=set([('name', 'verbose_name', 'insurer', 'type', 'grade', 'currency', 'sum_assured')]),
        ),
    ]
