# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import travel_product.models


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='policy_document',
            field=models.FileField(null=True, upload_to=travel_product.models.get_policy_path, blank=True),
        ),
        migrations.AddField(
            model_name='transaction',
            name='reference_id',
            field=models.CharField(max_length=250, null=True, blank=True),
        ),
    ]
