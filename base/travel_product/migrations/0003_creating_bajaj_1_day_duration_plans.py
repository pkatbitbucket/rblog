# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def create_bajaj_one_day_slabs(apps, schema_editor):
    IndividualSlab = apps.get_model("travel_product", "IndividualSlab")
    two_day_slabs = IndividualSlab.objects.filter(plan__insurer__slug='bajaj-allianz', plan__type='single', duration=2)
    for slab in two_day_slabs:
        slab.id = slab.pk = None
        slab.duration = 1
        print slab.plan, 'duplicated with duration 1'
        slab.save()


class Migration(migrations.Migration):

    dependencies = [
    	('travel_product', '0002_added_reference_id_and_policy_document_field'),
    ]

    operations = [
    	migrations.RunPython(
            code=create_bajaj_one_day_slabs,
            reverse_code=migrations.RunPython.noop
        ),
    ]
