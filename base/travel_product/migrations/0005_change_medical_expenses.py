# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations

from humanize.number import intcomma


def change_medical_expenses(apps, schema_editor):
    Feature = apps.get_model("travel_product", "Feature")
    Plan = apps.get_model("travel_product", "Plan")
    Plan.CURRENCY_CHOICES = (
        ('USD', '$'),
        ('EUR', u'\u20ac'),
        ('INR', 'Rs'),
    )
    for feature in Feature.objects.filter():
        old_cover = feature.cover
        feature.cover = feature.cover.replace(',', '').replace(' ', '').replace('$', 'USD').replace(u'\u20ac', 'EUR')
        currency = 'USD' if 'USD' in feature.cover else 'EUR'
        try:
            feature.cover = dict(Plan.CURRENCY_CHOICES)[currency] + intcomma(int(feature.cover.split(currency)[1]))
            feature.save()
            print old_cover, '->', feature.cover
        except (IndexError, ValueError):
            print 'EXCEPTION on', old_cover


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0004_auto_20150807_1750'),
    ]

    operations = [
        migrations.RunPython(
            code=change_medical_expenses,
            reverse_code=migrations.RunPython.noop
        )
    ]
