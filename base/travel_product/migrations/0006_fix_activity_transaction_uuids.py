# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0005_change_medical_expenses'),
    ]

    operations = [
        # migrations.RunSQL("update travel_product_transaction set id = replace(id, '-', '');", reverse_sql=migrations.RunSQL.noop),
        # migrations.RunSQL("update travel_product_transaction set id = CONCAT(id, 'd474') where LENGTH(id)=28;", reverse_sql=migrations.RunSQL.noop),
        # migrations.RunSQL("set foreign_key_checks=0;", reverse_sql=migrations.RunSQL.noop),
        # migrations.RunSQL("update travel_product_activity set id = replace(id, '-', '');", reverse_sql=migrations.RunSQL.noop),
        # migrations.RunSQL("update travel_product_activity set id = CONCAT(id, 'd474') where LENGTH(id)=28;", reverse_sql=migrations.RunSQL.noop),
        # migrations.RunSQL("update travel_product_transaction set activity_id = replace(activity_id, '-', '');", reverse_sql=migrations.RunSQL.noop),
        # migrations.RunSQL("update travel_product_transaction set activity_id = CONCAT(activity_id, 'd474') where LENGTH(activity_id)=28;", reverse_sql=migrations.RunSQL.noop),
        # migrations.RunSQL("set foreign_key_checks=1;", reverse_sql=migrations.RunSQL.noop),
    ]
