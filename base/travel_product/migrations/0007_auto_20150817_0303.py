# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import autoslug.fields
import travel_product.models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0006_fix_activity_transaction_uuids'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activity',
            name='id',
            field=travel_product.models.UUIDField32(default=uuid.uuid4, serialize=False, primary_key=True),
        ),
        migrations.AlterField(
            model_name='declinedrisk',
            name='slug',
            field=autoslug.fields.AutoSlugField(editable=False, populate_from=b'malady', always_update=True, unique=True),
        ),
        migrations.AlterField(
            model_name='featurename',
            name='slug',
            field=autoslug.fields.AutoSlugField(editable=False, populate_from=b'name', always_update=True, unique=True),
        ),
        migrations.AlterField(
            model_name='grade',
            name='slug',
            field=autoslug.fields.AutoSlugField(populate_from=b'name', unique=True, editable=False),
        ),
        migrations.AlterField(
            model_name='insurer',
            name='slug',
            field=autoslug.fields.AutoSlugField(populate_from=b'name', unique=True, editable=False),
        ),
        migrations.AlterField(
            model_name='place',
            name='slug',
            field=autoslug.fields.AutoSlugField(populate_from=b'name', unique=True, editable=False),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='id',
            field=travel_product.models.UUIDField32(default=uuid.uuid4, serialize=False, primary_key=True),
        ),
    ]
