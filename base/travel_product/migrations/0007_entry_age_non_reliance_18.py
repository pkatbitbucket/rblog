# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def change_entry_age_to_18(apps, schema_editor):
    Slab = apps.get_model("travel_product", "Slab")
    Slab._default_manager.exclude(plan__insurer__slug='reliance').filter(age_min=0).update(age_min=18)


def change_entry_age_to_0(apps, schema_editor):
    Slab = apps.get_model("travel_product", "Slab")
    Slab._default_manager.exclude(plan__insurer__slug='reliance').filter(age_min=18).update(age_min=0)


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0006_fix_activity_transaction_uuids'),
    ]

    operations = [
        migrations.RunPython(
            code=change_entry_age_to_18,
            reverse_code=change_entry_age_to_0
        )
    ]
