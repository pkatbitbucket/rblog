# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0007_auto_20150817_0303'),
        ('travel_product', '0007_entry_age_non_reliance_18'),
    ]

    operations = [
    ]
