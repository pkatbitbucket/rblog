# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0008_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='place',
            name='display',
            field=models.BooleanField(default=True),
        ),
    ]
