# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def delete_bajaj_old_family_plans(apps, schema_editor):
    Plan = apps.get_model('travel_product', 'Plan')
    Plan.objects.filter(insurer__slug='bajaj-allianz', slab__familyslab__isnull=False).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0009_place_display'),
    ]

    operations = [
        migrations.RunPython(
            code=delete_bajaj_old_family_plans,
            reverse_code=migrations.RunPython.noop,
        )
    ]
