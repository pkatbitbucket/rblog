# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.core.management import call_command
from django.db import migrations

import os


def add_bajaj_plans_fro_fixtures(apps, schema_editor, skip=settings.TESTING):
    if skip:
        return

    BAJAJ_FEATURES_FIXTURE_PATH = os.path.join(
        settings.SETTINGS_FILE_FOLDER,
        'travel_product/fixtures/bajaj-allianz/features_bajaj.json',
    )
    call_command('loaddata', BAJAJ_FEATURES_FIXTURE_PATH, verbosity=3)

    BAJAJ_FIXTURE_PATH = os.path.join(
        settings.SETTINGS_FILE_FOLDER,
        'travel_product/fixtures/bajaj-allianz/all_important_models_fixtures.json',
    )
    call_command('loaddata', BAJAJ_FIXTURE_PATH, verbosity=3)


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0010_delete_bajaj_old_family_plans'),
    ]

    operations = [
        migrations.RunPython(
            code=add_bajaj_plans_fro_fixtures,
            reverse_code=migrations.RunPython.noop,
        )
    ]
