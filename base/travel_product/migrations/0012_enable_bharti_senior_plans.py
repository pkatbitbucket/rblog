# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def enable_disable_bharti_senior_plans(active):
    def migrate(apps, schema_editor):
        Slab = apps.get_model('travel_product', 'Slab')
        single_trip_schengen_slabs = Slab._default_manager.filter(plan__insurer__slug='bharti-axa', plan__type='single', age_min=61, age_max=70).exclude(plan__places_included__slug='schengen')
        multi_trip_slabs = Slab._default_manager.filter(plan__insurer__slug='bharti-axa', plan__type='multi', age_min=61, age_max=70)
        print ''
        print ('enabling' if active else 'disabling') + ' bharti single_trip_schengen_slabs'
        single_trip_schengen_slabs.update(active=active)
        print ('enabling' if active else 'disabling') + ' bharti multi_trip_slabs'
        multi_trip_slabs.update(active=active)
    return migrate


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0011_add_bajaj_family_plans'),
    ]

    operations = [
        migrations.RunPython(
            code=enable_disable_bharti_senior_plans(active=True),
            reverse_code=enable_disable_bharti_senior_plans(active=False),
        )
    ]
