# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def split_religare_senior_age_slabs(apps, schema_editor):
    IndividualSlab = apps.get_model('travel_product', 'IndividualSlab')
    slabs = IndividualSlab._default_manager.filter(age_min=71, age_max=150, plan__insurer__slug='religare')
    counter = 1
    for slab in slabs:
        age_max = slab.age_max
        slab.age_max = 80
        slab.save()
        slab, new = IndividualSlab.objects.get_or_create(**{
            'plan': slab.plan,
            'age_min': slab.age_max + 1,
            'age_max': age_max,
            'duration': slab.duration,
            'premium': slab.premium,
            'source': slab.source,
        })
        if new:
            print counter, 'cloning ', slab.plan.name
            counter += 1


def merge_religare_senior_age_slabs(apps, schema_editor):
    IndividualSlab = apps.get_model('travel_product', 'IndividualSlab')
    print "Deleting religare slabs over age 80"
    IndividualSlab._default_manager.filter(age_min=81, age_max=150, plan__insurer__slug='religare').delete()
    print 'updated ', IndividualSlab._default_manager.filter(age_min=71, age_max=80, plan__insurer__slug='religare').update(age_max=150)


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0012_enable_bharti_senior_plans'),
    ]

    operations = [
        migrations.RunPython(
            code=split_religare_senior_age_slabs,
            reverse_code=merge_religare_senior_age_slabs,
        )
    ]
