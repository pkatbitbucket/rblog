# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def apply_religare_sublimits(active):
    def migrate(apps, schema_editor):
        Slab = apps.get_model('travel_product', 'Slab')
        religare_over_usd_100000_senior_slabs_61_70 = Slab._default_manager.filter(plan__sum_assured__gt=100000, plan__currency='USD', plan__insurer__slug='religare', age_min=61, age_max=70).distinct()
        religare_over_usd_100000_senior_slabs_71_80 = Slab._default_manager.filter(plan__sum_assured__gt=100000, plan__currency='USD', plan__insurer__slug='religare', age_min=71, age_max=80).distinct()

        religare_over_eur_100000_senior_slabs_61_70 = Slab._default_manager.filter(plan__sum_assured__gt=100000, plan__currency='EUR', plan__insurer__slug='religare', age_min=61, age_max=70).distinct()
        religare_over_eur_100000_senior_slabs_71_80 = Slab._default_manager.filter(plan__sum_assured__gt=100000, plan__currency='EUR', plan__insurer__slug='religare', age_min=71, age_max=80).distinct()

        religare_over_usd_50000_senior_slabs_81_150 = Slab._default_manager.filter(plan__sum_assured__gt=50000, plan__currency='USD', plan__insurer__slug='religare', age_min=81, age_max=150).distinct()

        print ['applying', 'unapplying'][active], 'Religare sublimits for senior citizens'
        religare_over_eur_50000_senior_slabs_81_150 = Slab._default_manager.filter(plan__sum_assured__gt=30000, plan__currency='EUR', plan__insurer__slug='religare', age_min=81, age_max=150).distinct()
        (
            religare_over_usd_100000_senior_slabs_61_70 |
            religare_over_usd_100000_senior_slabs_71_80 |
            religare_over_eur_100000_senior_slabs_61_70 |
            religare_over_eur_100000_senior_slabs_71_80 |
            religare_over_usd_50000_senior_slabs_81_150 |
            religare_over_eur_50000_senior_slabs_81_150
        ).update(active=active)

    return migrate


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0013_split_religare_senior_age_plans'),
    ]

    operations = [
        migrations.RunPython(
            code=apply_religare_sublimits(active=False),
            reverse_code=apply_religare_sublimits(active=True)
        )
    ]
