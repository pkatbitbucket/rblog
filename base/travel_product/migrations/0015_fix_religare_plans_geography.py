# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def fix_religare_plans_geography(apps, schema_editor):
    Plan = apps.get_model('travel_product', 'Plan')
    Place = apps.get_model('travel_product', 'Place')
    places = {
        'Explore - Asia': (['asia'], []),
        'Explore - Europe': (['europe'], []),
        'Explore - Canada': (['canada'], []),
        'Explore - Africa': (['africa'], []),
        'Explore - Gold - Worldwide': (['worldwide'], []),
        'Explore - Gold - Worldwide - Excluding US & Canada': (['worldwide'], ['usa-united-states-of-america', 'canada']),
        'Explore - Platinum - Worldwide': (['worldwide'], []),
        'Explore - Platinum - Worldwide - Excluding US & Canada': (['worldwide'], ['usa-united-states-of-america', 'canada']),
    }
    for plan in Plan._default_manager.filter(insurer__slug='religare'):
        places_included, places_excluded = places[plan.verbose_name]
        places_included, places_excluded = Place._default_manager.filter(slug__in=places_included), Place._default_manager.filter(slug__in=places_excluded)
        plan.places_included.clear()
        plan.places_included.add(*places_included)
        plan.places_excluded.clear()
        plan.places_excluded.add(*places_excluded)
        print plan.verbose_name, plan.places_included.all(), plan.places_excluded.all()


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0014_applying_religare_sublimits'),
    ]

    operations = [
        migrations.RunPython(
            code=fix_religare_plans_geography,
            reverse_code=migrations.RunPython.noop,
        )
    ]
