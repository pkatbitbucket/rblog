# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def update_feature_names(apps, schema_editor):
    FeatureName = apps.get_model('travel_product', 'FeatureName')
    change_to = {
        "Loss Of Checked-In Baggage": "Baggage Loss Cover",
        "Pre-Existing Diseases": "Existing Diseases Cover",
        "Dental": "Dental Cover",
        "Accidental Death & Dismemberment Common Carrier": "Accidental Death & Disability (Public Transport)",
        "Medical Expenses": "Medical Cover",
    }
    for feature_name in FeatureName.objects.filter(name__in=change_to.keys()):
        print feature_name.name, '->', change_to[feature_name.name]
        feature_name.name = change_to[feature_name.name]
        feature_name.save()


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0015_fix_religare_plans_geography'),
    ]

    operations = [
        migrations.RunPython(
            code=update_feature_names,
            reverse_code=migrations.RunPython.noop,
        )
    ]
