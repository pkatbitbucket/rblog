# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def change_visiting_usa_value(apps, schema_editor):
    Slab = apps.get_model('travel_product', 'Slab')

    slist = Slab.objects.filter(plan__insurer__slug='reliance', plan__type='multi', plan__verbose_name='World except USA & Canada')
    for s in slist:
        s.source['form_data']['visiting_usa'] = 'no'
        s.save()


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0016_update_feature_names'),
    ]

    operations = [
            migrations.RunPython(
                code=change_visiting_usa_value,
                reverse_code=migrations.RunPython.noop,
            )
    ]
