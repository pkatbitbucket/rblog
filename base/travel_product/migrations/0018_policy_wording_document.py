# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import travel_product.models
import core.models


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0017_visiting_usa_error'),
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=255, blank=True)),
                ('file', models.FileField(upload_to=travel_product.models.get_document_upload_path)),
            ],
            options={
                'abstract': False,
            },
            bases=(core.models.AbstractBaseModel, models.Model),
        ),
        migrations.CreateModel(
            name='PolicyWordingDocument',
            fields=[
            ],
            options={
                'abstract': False,
                'proxy': True,
            },
            bases=('travel_product.document',),
        ),
        migrations.AddField(
            model_name='plan',
            name='policy_wording',
            field=models.ForeignKey(related_name='plans_with_policy_wording', blank=True, to='travel_product.PolicyWordingDocument', null=True),
        ),
    ]
