from __future__ import unicode_literals

from django.conf import settings
from django.core.management import call_command
from django.db import migrations

import os


def fix_place_typos(apps, schema_editor):
    PLACES_FIXTURE_PATH = os.path.join(
        settings.SETTINGS_FILE_FOLDER,
        'travel_product/fixtures/places.json',
    )
    call_command('loaddata', PLACES_FIXTURE_PATH, verbosity=3)


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0018_policy_wording_document'),
    ]

    operations = [
        migrations.RunPython(
            code=fix_place_typos,
            reverse_code=migrations.RunPython.noop,
        )
    ]
