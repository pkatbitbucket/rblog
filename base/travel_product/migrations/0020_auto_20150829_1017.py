# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0019_fix_place_typos'),
    ]

    operations = [
        migrations.RenameField(
            model_name='insurer',
            old_name='proposal_form',
            new_name='rule_form',
        ),
    ]
