# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, connection


def delete_bajaj_individualslabs_under_family_plans(apps, schema_editor):
    def delete_individualslab_sql(pk):
        cursor = connection.cursor()
        cursor.execute("DELETE FROM travel_product_individualslab WHERE slab_ptr_id=%s;", [pk, ])
        return cursor.rowcount

    Plan = apps.get_model('travel_product', 'Plan')
    IndividualSlab = apps.get_model('travel_product', 'IndividualSlab')
    family_plans = Plan.objects.filter(**{'name': u'Travel Elite Family  (Family Floater)', 'insurer__slug': u'bajaj-allianz'})
    individualslabs = IndividualSlab.objects.filter(plan__in=family_plans).values_list('id', flat=True)
    print "Deleting", individualslabs.count(), 'individual slabs that were added erroneously'
    for pk in IndividualSlab.objects.filter(plan__in=family_plans).values_list('id', flat=True):
        delete_individualslab_sql(pk)


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0021_auto_20150903_1737'),
    ]

    operations = [
        migrations.RunPython(
            code=delete_bajaj_individualslabs_under_family_plans,
            reverse_code=migrations.RunPython.noop,
        )
    ]
