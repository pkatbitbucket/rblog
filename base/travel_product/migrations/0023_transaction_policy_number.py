# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0022_delete_bajaj_individualslabs_under_family_plans'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='policy_number',
            field=models.CharField(max_length=250, blank=True),
        ),
    ]
