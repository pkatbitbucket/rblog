# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0025_auto_20151223_2256'),
        ('travel_product', '0023_transaction_policy_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='policy',
            field=models.OneToOneField(related_name='travel_transaction', null=True, blank=True, to='core.Policy'),
        ),
    ]
