# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
import datetime
from django.contrib.auth.hashers import UNUSABLE_PASSWORD_SUFFIX_LENGTH, UNUSABLE_PASSWORD_PREFIX

from django.db import migrations
from django.utils.crypto import get_random_string
from account_manager.utils import transaction_field_mapping
from putils import add_years, clean_email_list
from travel_product.forms import QuoteForm
from utils import convert_datetime_to_timezone


def populate_travel_policies(apps, schema_editor):
    def create_user(transaction):
        mapping = transaction_field_mapping(transaction)
        email = mapping.get('email', '')
        emails = clean_email_list(email)
        if emails:
            email = emails[0]
            try:
                return User.objects.get(email=email)
            except User.DoesNotExist:
                return User.objects.create(
                    email=mapping.get('email'),
                    username=mapping.get('email', ''),
                    password=UNUSABLE_PASSWORD_PREFIX + get_random_string(UNUSABLE_PASSWORD_SUFFIX_LENGTH),
                    first_name=mapping.get('first_name', ''),
                    middle_name=mapping.get('middle_name', ''),
                    last_name=mapping.get('last_name', ''),
                    is_active=False,
                    date_joined=datetime.datetime.now(),
                    created_on=datetime.datetime.now(),
                    updated_on=datetime.datetime.now(),
                    last_login=datetime.datetime.now(),
                )

    User = apps.get_model('core', 'User')
    Policy = apps.get_model('core', 'Policy')
    Requirement = apps.get_model('core', 'Requirement')
    RequirementKind = apps.get_model('core', 'RequirementKind')
    Transaction = apps.get_model('travel_product', 'Transaction')
    ContentType = apps.get_model('contenttypes', 'ContentType')
    UserTransaction = apps.get_model('account_manager', 'UserTransaction')
    new_travel_requirement, _ = RequirementKind.objects.get_or_create(product='travel', kind='new_policy', sub_kind='')
    transactions = Transaction.objects.exclude(policy_document='').filter(status='COMPLETED')

    def cleaned_data(activity):
        form = QuoteForm(activity.data['quote_form']['data'])
        form.is_valid()
        return form.cleaned_data

    for transaction in transactions.iterator():
        try:
            ut = UserTransaction.objects.get(
                object_id=transaction.id, content_type=ContentType.objects.get_for_model(transaction)
            )
        except UserTransaction.DoesNotExist:
            user = create_user(transaction)
            if user is None:
                continue
        except UserTransaction.MultipleObjectsReturned:
            user = UserTransaction.objects.filter(
                object_id=transaction.id, content_type=ContentType.objects.get_for_model(transaction)
            ).last().user
        else:
            user = ut.user
        requirement = Requirement.objects.create(
            kind=new_travel_requirement,
            user=user
        )
        data = cleaned_data(transaction.activity)
        start_date = convert_datetime_to_timezone(data.get('from_date'))
        end_date = convert_datetime_to_timezone(data.get('to_date'))
        if not end_date and data.get('type') == 'multi' and start_date:
            end_date = add_years(start_date, 1) - datetime.timedelta(days=1)

        if not (start_date and end_date):
            continue

        policy = Policy.objects.create(
            name=os.path.splitext(transaction.policy_document.name)[0].split(os.path.sep)[-1],
            document=transaction.policy_document,
            app=transaction._meta.app_label,
            status='valid',
            user=user,
            requirement_sold=requirement,
            policy_number=transaction.policy_number,
            issue_date=start_date,
            expiry_date=end_date,
        )
        transaction.policy = policy
        transaction.save()
        requirement.policy = policy
        requirement.save()


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0024_transaction_policy'),
        ('core', '0025_auto_20151223_2256'),
        ('account_manager', '0009_auto_20151125_1805'),
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.RunPython(code=populate_travel_policies, reverse_code=migrations.RunPython.noop),
    ]
