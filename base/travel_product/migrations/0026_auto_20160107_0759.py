# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0025_populate_policies'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='policy_end_date',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='transaction',
            name='policy_start_date',
            field=models.DateField(null=True, blank=True),
        ),
    ]
