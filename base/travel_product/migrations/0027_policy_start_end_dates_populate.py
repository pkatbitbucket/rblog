# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations

from putils import add_years
from travel_product.forms import QuoteForm

import datetime


def policy_start_date(activity):
    quote_data = activity.data['quote_form']['data']
    return QuoteForm.base_fields['from_date'].to_python(quote_data['from_date'])


def policy_end_date(activity):
    quote_data = activity.data['quote_form']['data']
    if quote_data['type'] == 'single':
        end_date = QuoteForm.base_fields['to_date'].to_python(quote_data['to_date'])
    else:
        assert quote_data['type'] == 'multi'
        end_date = add_years(policy_start_date(activity), 1) - datetime.timedelta(days=1)
    return end_date


def policy_start_end_dates_populate(apps, schema_editor):
    Transaction = apps.get_model('travel_product', 'Transaction')
    for transaction in Transaction.objects.iterator():
        try:
            start_date = policy_start_date(transaction.activity)
        except:
            print 'Unable to get start date for {}'.format(transaction.transaction_id)
        else:
            try:
                end_date = policy_start_date(transaction.activity)
            except:
                print 'Unable to get end date for {}'.format(transaction.transaction_id)
            else:
                transaction.policy_start_date = start_date
                transaction.policy_end_date = end_date
                transaction.save()


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0026_auto_20160107_0759'),
    ]

    operations = [
        migrations.RunPython(
            code=policy_start_end_dates_populate,
            reverse_code=migrations.RunPython.noop,
        )
    ]
