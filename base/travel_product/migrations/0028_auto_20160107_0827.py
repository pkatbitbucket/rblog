# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0027_policy_start_end_dates_populate'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='policy_end_date',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='policy_start_date',
            field=models.DateField(null=True, blank=True),
        ),
    ]
