# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
from django.template.defaultfilters import title
import datetime
import putils
from motor_product import parser_utils
from django.db import transaction
from django.conf import settings


def populate_insurer_policy_data(apps, schema_editor):
    # Insert data into InsurerPolicyData model
    insurer_obj_list = []
    InsurerPolicyData = apps.get_model('travel_product', 'InsurerPolicyData')
    insurance_policy_data_list = InsurerPolicyData.objects.filter(insurer='apollo-munich')

    # delete if some apollo-munich data already exist
    for insurance_policy_data in insurance_policy_data_list:
        insurance_policy_data.delete()
        
    xlsx_file_path = putils.get_file_from_s3(
        settings.INTEGRATION_AWS_ACCESS_KEY_ID,
        settings.INTEGRATION_AWS_SECRET_ACCESS_KEY,
        settings.INTEGRATION_BUCKET_NAME,
        'travel/apollo-munich/InsurerPolicyData-2016-02-09.xlsx')

    xlsx_file = open(xlsx_file_path)

    datas = parser_utils.parse_xlsx(xlsx_file, row_dict=True)
    for data in datas:
        insurer_obj_list.append(
            InsurerPolicyData(
                insurer=data['insurer'],
                plans_list=eval(data['plans_list']),
                hash_form_data=data['hash_form_data'],
                form_data=eval(data['form_data'])
                )
            )
    with transaction.atomic():
        InsurerPolicyData.objects.bulk_create(insurer_obj_list)

    
def delete_insurer_policy_data(apps, schema_editor):
    # populate_inusrer_policy_data function will create InsurerPolicyData,
    # so delete_insurer_policy_data should delete created data
    InsurerPolicyData = apps.get_model('travel_product', 'InsurerPolicyData')
    InsurerPolicyData.objects.filter(insurer='apollo-munich').delete()
    
        
def create_grades(apps, schema_editor):
    grades = [u'Gold', u'Titanium', u'Silver', u'Platinum', u'Diamond', u'Asian', u'Bronze']
    Grade = apps.get_model('travel_product', 'Grade')
    for grade in grades:
        Grade.objects.get_or_create(name=grade)

        
def calculate_age(birth_date):
    today = datetime.date.today()
    try:
        birth_day = birth_date.replace(year=today.year)
    except ValueError:  # raised when birth date is February 29 and the current year is not a leap year
        birth_day = birth_date.replace(year=today.year, month=birth_date.month + 1, day=1)
    if birth_day > today:
        return today.year - birth_date.year - 1
    else:
        return today.year - birth_date.year
        
        
def populate_plans_n_slabs(apps, schema_editor):
    Insurer = apps.get_model('travel_product', 'Insurer')
    Grade = apps.get_model('travel_product', 'Grade')
    Place = apps.get_model('travel_product', 'Place')
    Plan = apps.get_model('travel_product', 'Plan')
    FamilySlab = apps.get_model('travel_product', 'FamilySlab')
    IndividualSlab = apps.get_model('travel_product', 'IndividualSlab')
    InsurerPolicyData = apps.get_model('travel_product', 'InsurerPolicyData')

    grades = {
        'ASIAN': Grade.objects.get(slug='asian'),
        'BRONZE': Grade.objects.get(slug='bronze'),
        'SILVER': Grade.objects.get(slug='silver'),
        'GOLD': Grade.objects.get(slug='gold'),
        'PLATINUM': Grade.objects.get(slug='platinum'),
    }

    places = {
        'WORLDWIDE': (Place.objects.filter(slug__in=['worldwide']), []),  # world including usa and canada
        'EXCLUDUSA': (
            Place.objects.filter(slug__in=['worldwide']),
            Place.objects.filter(slug__in=['usa-united-states-of-america', 'canada'])
        ),  # world excluding usa and canada
        'ASIAPAC': (
            Place.objects.filter(slug__in=['asia']),
            Place.objects.filter(slug__in=['japan'])
        )  # Asian region excluding Japan
    }

    regions = {
        'WORLDWIDE': "world including usa and canada",
        'EXCLUDUSA': "world excluding usa and canada ",
        'ASIAPAC': 'Asian region excluding Japan',
    }

    family_strength = {
        '2A': (2, 0),
        '2A1C': (2, 1),
        '2A2C': (2, 2),
        '2A3C': (2, 3),
        '2A4C': (2, 4),
    }

    insurer, _ = Insurer.objects.get_or_create(name='Apollo Munich', slug='apollo-munich')
    policy_datas = InsurerPolicyData.objects.filter(insurer=insurer.slug)

    # Delete apollo plans if already exist
    Plan.objects.filter(insurer=insurer).delete()
    
    for data in policy_datas:
        form_data = data.form_data
        for plan_dict in data.plans_list:
            plan, _ = Plan.objects.get_or_create(**{
                'type': ['multi', 'single'][form_data['trip_type'] == 'S'],
                'name': title(form_data['plan']),
                'insurer': insurer,
                'grade': grades[plan_dict['plan_type'].strip()],
                'currency': plan_dict['sum_assured'].split(' ')[0],
                'sum_assured': int(''.join(plan_dict['sum_assured'].split(' ')[1].split(','))),
                'verbose_name': title(regions[form_data['geographical_coverage']])
            })
            places_included, places_excluded = places[form_data['geographical_coverage']]

            for place in places_included:
                plan.places_included.add(place)

            for place in places_excluded:
                plan.places_excluded.add(place)

            age = calculate_age(
                datetime.datetime.strptime(form_data['DOB'], '%d-%b-%Y').date()
            )
            if plan.type == 'single':
                duration = (
                    datetime.datetime.strptime(form_data['to_date'], '%d-%b-%Y').date() -
                    datetime.datetime.strptime(form_data['from_date'], '%d-%b-%Y').date()
                ).days
                duration += 1
            else:
                duration = int(form_data['max_trip_length'])
            age_ranges = {
                0: 40,
                41: 60,
                61: 70,
            }
            for age_min, age_max in age_ranges.items():
                if age >= age_min and age <= age_max:
                    break

            if form_data['policy_type'] in ['I', 'S']:
                if plan.type == 'single':
                    duration_range = range(*[duration - 6, duration + 1])
                else:
                    duration_range = [duration]
                    # duration_range = range(1, 30 + 1) if duration == 30 else range(31, 60 + 1)
                for duration in duration_range:
                    if duration in range(2, 357) and plan_dict['premium']:  # No plan with duration 1. wtf?
                        inc_premium = float(plan_dict['premium'])
                        exc_premium = round((inc_premium * 100) / 114.5)
                        slab, new = IndividualSlab.objects.get_or_create(**{
                            'plan': plan,
                            'duration': duration,
                            'age_min': age_min,
                            'age_max': age_max,
                            'premium': float(exc_premium),
                        })
                        if new:
                            slab.source = {
                                'form_data': form_data,
                                'plan_data': plan_dict,
                            }
                        slab.save()
            elif form_data['policy_type'] == 'F':
                duration_range = range(*[duration - 6, duration + 1])
                for duration in duration_range:
                    if duration in range(2, 357):  # Todo According to the changes are being done
                        inc_premium = float(plan_dict['premium'])
                        exc_premium = round((inc_premium * 100) / 114.5)
                        adult_max = family_strength[form_data['family_type']][0]
                        children_max = family_strength[form_data['family_type']][1]
                        slab, new = FamilySlab.objects.get_or_create(**{
                            'plan': plan,
                            'duration': duration,
                            'age_min': age_min,
                            'age_max': age_max,
                            'premium': float(exc_premium),
                            'adult_max': adult_max,
                            'children_max': children_max,
                        })
                        if new:
                            slab.source = {
                                'form_data': form_data,
                                'plan_data': plan_dict,
                            }
                        slab.save()

                
def delete_plans_n_slabs(apps, schema_editor):
    # should delete create plans and slabs
    Plan = apps.get_model('travel_product', 'Plan')
    Insurer = apps.get_model('travel_product', 'Insurer')
    insurer = Insurer.objects.get(slug='apollo-munich')
    Plan.objects.filter(insurer=insurer).delete()

        
class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0028_auto_20160107_0827'),
    ]

    operations = [
        migrations.RunPython(
            code=populate_insurer_policy_data,
            reverse_code=delete_insurer_policy_data
        ),
        migrations.RunPython(
            code=create_grades,
            reverse_code=migrations.RunPython.noop
        ),
        migrations.RunPython(
            code=populate_plans_n_slabs,
            reverse_code=delete_plans_n_slabs
        )
    ]
