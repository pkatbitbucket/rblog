# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0030_create_apollo_plans_n_slabs'),
    ]

    operations = [
        migrations.AddField(
            model_name='planfeature',
            name='slab',
            field=models.CharField(blank=True, max_length=20, choices=[(b'family', b'Family'), (b'individual', b'Individual'), (b'student', b'Student')]),
        ),
    ]
