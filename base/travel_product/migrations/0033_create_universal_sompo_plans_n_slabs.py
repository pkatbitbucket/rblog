from __future__ import unicode_literals

from django.db import migrations
from django.template.defaultfilters import title
import datetime
import putils
from motor_product import parser_utils
from django.db import transaction
from django.conf import settings


def populate_insurer_policy_data(apps, schema_editor):
    # Insert data into InsurerPolicyData model
    insurer_obj_list = []
    InsurerPolicyData = apps.get_model('travel_product', 'InsurerPolicyData')
    xlsx_file_path = putils.get_file_from_s3(
        settings.INTEGRATION_AWS_ACCESS_KEY_ID,
        settings.INTEGRATION_AWS_SECRET_ACCESS_KEY,
        settings.INTEGRATION_BUCKET_NAME,
        'travel/universal-sompo/InsurerPolicyData_universal-sompo-2016-02-02.xlsx')
    InsurerPolicyData.objects.filter(insurer='universal-sompo').delete()
    # xlsx_file_path = open('/home/coverfox/Downloads/InsurerPolicyData-2016-02-12.xlsx')
    datas = parser_utils.parse_xlsx(xlsx_file_path, row_dict=True)
    for data in datas:
        insurer_obj_list.append(
            InsurerPolicyData(
                insurer=data['insurer'],
                plans_list=eval(data['plans_list']),
                hash_form_data=data['hash_form_data'],
                form_data=eval(data['form_data'])
            )
        )
    with transaction.atomic():
        InsurerPolicyData.objects.bulk_create(insurer_obj_list)


def delete_insurer_policy_data(apps, schema_editor):
    # populate_inusrer_policy_data function will create InsurerPolicyData,
    # so delete_insurer_policy_data should delete created data
    InsurerPolicyData = apps.get_model('travel_product', 'InsurerPolicyData')
    datas = InsurerPolicyData.objects.filter(insurer='universal-sompo')
    for data in datas:
        data.delete()


def create_grades(apps, schema_editor):
    grades = [u'Gold', u'Silver', u'Platinum']
    Grade = apps.get_model('travel_product', 'Grade')
    for grade in grades:
        Grade.objects.get_or_create(name=grade)


def populate_plans_n_slabs(apps, schema_editor):
    Insurer = apps.get_model('travel_product', 'Insurer')
    Grade = apps.get_model('travel_product', 'Grade')
    Place = apps.get_model('travel_product', 'Place')
    Plan = apps.get_model('travel_product', 'Plan')
    IndividualSlab = apps.get_model('travel_product', 'IndividualSlab')
    InsurerPolicyData = apps.get_model('travel_product', 'InsurerPolicyData')

    grades = {'Gold': Grade.objects.get(slug='gold'),
              'Silver': Grade.objects.get(slug='silver'),
              'Platinum': Grade.objects.get(slug='platinum')}

    places = {'Travel-Asia': (Place.objects.filter(slug__in=['asia']), []),  # asia excluding japa
              'Travel-Worldwide Excluding US & Canada': (Place.objects.filter(slug__in=['worldwide']), Place.objects.filter(slug__in=['usa-united-states-of-america', 'canada'])),  # world excluding usa and canada
              'Travel-Worldwide': (Place.objects.filter(slug__in=['worldwide']), [])}  # world including usa and canada

    insurer, _ = Insurer.objects.get_or_create(slug='universal-sompo', name='Universal Sompo')
    policy_datas = InsurerPolicyData.objects.filter(insurer='universal-sompo')

    individual_worldwide_duration = [0, ] + [7, 14, 21, 28, 35, 47, 60, 75, 90, 120, 150, 180]
    individual_asia_duration = [0, ] + [4, 7, 14, 21, 30, 47]
    counter = 0
    for data in policy_datas:
        form_data = data.form_data
        for plan_dict in data.plans_list:
            plan, _ = Plan.objects.get_or_create(**{
                'type': 'single',
                'name': title(plan_dict['plan_type']),
                'insurer': insurer,
                'grade': grades[form_data['member1_plan_type']],
                'currency': 'USD',
                'sum_assured': int(plan_dict['sum_assured']),
                'verbose_name': form_data['plan'],
            })
            places_included, places_excluded = places[form_data['plan']]

            for place in places_included:
                plan.places_included.add(place)

            for place in places_excluded:
                plan.places_excluded.add(place)

            duration = (datetime.datetime.strptime(form_data['to_date'], '%d-%b-%Y').date() - datetime.datetime.strptime(form_data['from_date'], '%d-%b-%Y').date()).days
            duration += 1
            if (form_data['member1_age_band'] == 'up to 40 Years'):
                age_min = 0
                age_max = 40
            elif(form_data['member1_age_band'] == '41 Years - 60 Years'):
                age_min = 41
                age_max = 59

            if form_data['no_of_travellers'] == '1':
                if(form_data['plan'] == 'Travel-Worldwide' or form_data['plan'] == 'Travel-Worldwide Excluding US & Canada'):
                    index = individual_worldwide_duration.index(duration)
                    duration_range = range(individual_worldwide_duration[index - 1] + 1, individual_worldwide_duration[index] + 1)
                elif(form_data['plan'] == 'Travel-Asia'):
                    index = individual_asia_duration.index(duration)
                    duration_range = range(individual_asia_duration[index - 1] + 1, individual_asia_duration[index] + 1)
                inc_premium = float(plan_dict['premium'])
                exc_premium = round((inc_premium * 100) / 114)
                for duration in duration_range:
                    count = 0
                    slab, new = IndividualSlab.objects.get_or_create(**{
                        'plan': plan,
                        'age_min': age_min,
                        'age_max': age_max,
                        'duration': duration,
                        'premium': float(exc_premium),
                    })
                    if new:
                        slab.source = {
                            'form_data': form_data,
                            'plan_data': plan_dict,
                        }
                        slab.save()
                    count = count + 1
                counter = counter + 1


def delete_plans_n_slabs(apps, schema_editor):
    # should delete create plans and slabs
    Plan = apps.get_model('travel_product', 'Plan')
    Insurer = apps.get_model('travel_product', 'Insurer')
    insurer = Insurer.objects.get(slug='universal-sompo')
    Plan.objects.filter(insurer=insurer).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0032_insert_features_apollo'),
    ]

    operations = [
        migrations.RunPython(
            code=populate_insurer_policy_data,
            reverse_code=delete_insurer_policy_data
        ),
        migrations.RunPython(
            code=create_grades,
            reverse_code=migrations.RunPython.noop
        ),
        migrations.RunPython(
            code=populate_plans_n_slabs,
            reverse_code=delete_plans_n_slabs
        )
    ]
