# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0079_auto_20160404_2221'),
        ('travel_product', '0032_insert_features_apollo'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='requirement',
            field=models.ForeignKey(related_name='health_transaction', blank=True, to='core.Requirement', null=True),
        ),
    ]
