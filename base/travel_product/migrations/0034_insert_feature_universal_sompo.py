# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
from django.conf import settings
import putils
from motor_product import parser_utils
from django.template.defaultfilters import capfirst, title
from humanize.number import intcomma
from motor_product import parser_utils

import csv


def insert_features(apps, schema_editor):
    Insurer = apps.get_model('travel_product', 'Insurer')
    Plan = apps.get_model('travel_product', 'Plan')
    FeatureName = apps.get_model('travel_product', 'FeatureName')
    PlanFeature = apps.get_model('travel_product', 'PlanFeature')

    # Fetch file from s3 bucket
    individual_asia_feature_path = putils.get_file_from_s3(
        settings.INTEGRATION_AWS_ACCESS_KEY_ID,
        settings.INTEGRATION_AWS_SECRET_ACCESS_KEY,
        settings.INTEGRATION_BUCKET_NAME,
        'travel/universal-sompo/Universal-Asian.xlsx')
    individual_worldwide_feature_path = putils.get_file_from_s3(
        settings.INTEGRATION_AWS_ACCESS_KEY_ID,
        settings.INTEGRATION_AWS_SECRET_ACCESS_KEY,
        settings.INTEGRATION_BUCKET_NAME,
        'travel/universal-sompo/Universal-Worldwide.xlsx')
#    individual_asia_feature_path = ('/home/coverfox/Documents/Universal-Asian.xlsx')
#    individual_worldwide_feature_path = ('/home/coverfox/Documents/Universal-Worldwide.xlsx')
    csv_files = []
    csv_files += [(
        'universal-sompo', 'single', 'individual', ('Gold', 'Platinum'),
        individual_asia_feature_path, ['$20,000', '$25,000']
    )]
    csv_files += [(
        'universal-sompo', 'single', 'individual', ('Silver', 'Gold', 'Platinum'),
        individual_worldwide_feature_path, ['$50,000', '$200,000', '$500,000']
    )]

    for insurer_slug, type, slab, names, csv_file, sum_assureds in csv_files:
        insurer = Insurer.objects.get(slug=insurer_slug)
        parent_feature_name = None
        with open(csv_file, 'r') as f:
            reader = parser_utils.parse_xlsx(f, row_dict=True)
            for row in reader:
                if not parent_feature_name or not row['Condition']:
                    if not row['Condition'] and '.' in row['Plan Benefits']:
                        try:
                            parent_feature_name, _ = FeatureName.objects.get_or_create(
                                name=title(row['Plan Benefits'].split('. ')[1].strip())
                            )
                            if _:
                                pass
                        except:
                            pass
                    else:
                        continue
                else:
                    for verbose_sum_assured in sum_assureds:
                        if verbose_sum_assured in sum_assureds:
                            if insurer.slug == 'religare':  # Idk man religare csv is complex
                                currency, _plan_name, various_sum_assureds = verbose_sum_assured.replace('\x80', 'EUR').split('-')
                                various_sum_assureds = map(int, various_sum_assureds.replace('EUR', '').replace('$', '').split(', '))
                            else:
                                try:
                                    currency, sum_assured = verbose_sum_assured.replace('\x80', 'EUR').split(' ')
                                except ValueError:
                                    currency, sum_assured = verbose_sum_assured.replace('$', 'USD ').split(' ')
                                sum_assured = int(''.join(sum_assured.split(',')))
                                various_sum_assureds = [sum_assured]
                            plans = Plan.objects.filter(**{
                                'insurer': insurer,
                                'sum_assured__in': various_sum_assureds,
                                'type': type,
                                'currency': currency,
                            })
                            plans = plans.filter(name__in=names) if names else plans
                            for plan in plans:
                                benefit = {
                                    "Loss Of Checked-In Baggage": "Baggage Loss Cover",
                                    "Pre-Existing Diseases": "Existing Diseases Cover",
                                    "Dental": "Dental Cover",
                                    "Accidental Death & Dismemberment Common Carrier": "Accidental Death & Disability (Public Transport)",
                                    "Medical Expenses": "Medical Cover",
                                }.get(title(row['Plan Benefits']), title(row['Plan Benefits']))
                                feature_name, _ = FeatureName.objects.get_or_create(name=benefit, parent=parent_feature_name)
                                if _:
                                    pass
                                cover = row[verbose_sum_assured].strip().replace('\x80', 'EUR').replace('EUR', u'\u20ac')
                                # SANITISE
                                Plan.CURRENCY_CHOICES = (
                                    ('USD', '$'),
                                    ('EUR', u'\u20ac'),
                                    ('INR', 'Rs'),
                                )
                                try:
                                    old_cover = cover
                                    cover = cover.replace(',', '').replace(' ', '').replace('$', 'USD').replace(u'\u20ac', 'EUR')
                                    currency = 'USD' if 'USD' in cover else 'EUR'
                                    cover = dict(Plan.CURRENCY_CHOICES)[currency] + intcomma(int(cover.split(currency)[1]))
                                except (IndexError, ValueError):
                                    # print 'EXCEPTION on', old_cover
                                    cover = old_cover

                                ###
                                # cover = 'Not Covered' if cover == 'NA' else cover
                                deductible = row['Deductible'].strip().replace('\x80', 'EUR').replace('EUR', u'\u20ac')
                                # deductible = '0' if deductible == 'NA' else deductible
                                remarks = capfirst(row['Condition'].strip())
                                plan_features = PlanFeature.objects.filter(**{'name': feature_name, 'plan': plan})
                                if not plan_features.exists():
                                    try:
                                        plan_feature = PlanFeature.objects.create(**{
                                            'name': feature_name,
                                            'plan': plan,
                                            'cover': None if cover in ['NA', '', '-'] else cover,
                                            'deductible': None if deductible in ['NA', '', '-'] else deductible,
                                            'remarks': None if remarks in ['NA', '', '-'] else remarks,
                                        })
                                    except:
                                        pass
                                else:
                                    try:
                                        plan_feature = plan_features.get(**{
                                            'name': feature_name,
                                            'plan': plan,
                                            'cover': None if cover in ['NA', '', '-'] else cover,
                                            'deductible': None if deductible in ['NA', '', '-'] else deductible,
                                            'remarks': None if remarks in ['NA', '', '-'] else remarks,
                                        })
                                        # print 'DUPLICATED'
                                    except PlanFeature.DoesNotExist:
                                        '''
                                        create different plan_feature if individual and
                                        family shares same feature but with different value
                                        '''
                                        try:
                                            plan_feature = PlanFeature.objects.create(**{
                                                'name': feature_name,
                                                'plan': plan,
                                                'cover': None if cover in ['NA', '', '-'] else cover,
                                                'deductible': None if deductible in ['NA', '', '-'] else deductible,
                                                'remarks': None if remarks in ['NA', '', '-'] else remarks,
                                                'slab': slab
                                            })
                                            # print "plan_feature created with %s SLAB" % slab
                                        except:
                                            pass
                        else:
                            pass
                            # sum_assureds list malformed


def delete_features_cover_none(apps, schema_editor):
    '''
    Delete PlanFeature if cover is None
    '''
    Insurer = apps.get_model('travel_product', 'Insurer')
    PlanFeature = apps.get_model('travel_product', 'PlanFeature')
    insurer = Insurer.objects.get(slug='universal-sompo')
    PlanFeature.objects.filter(plan__insurer=insurer, cover__isnull=True).delete()


def delete_features(apps, schema_editor):
    Insurer = apps.get_model('travel_product', 'Insurer')
    PlanFeature = apps.get_model('travel_product', 'PlanFeature')
    insurer = Insurer.objects.get(slug='universal-sompo')
    PlanFeature.objects.filter(plan__insurer=insurer).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0033_create_universal_sompo_plans_n_slabs'),
    ]

    operations = [
        migrations.RunPython(
            code=insert_features,
            reverse_code=delete_features
        ),
        migrations.RunPython(
            code=delete_features_cover_none,
            reverse_code=migrations.RunPython.noop
        ),
    ]
