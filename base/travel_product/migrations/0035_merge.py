# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('travel_product', '0034_insert_feature_universal_sompo'),
        ('travel_product', '0033_transaction_requirement'),
    ]

    operations = [
    ]
