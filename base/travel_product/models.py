import datetime

from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.core.urlresolvers import reverse
from django.db import models
from django.utils import lru_cache
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q

from autoslug import AutoSlugField
from core.models import Tracker, RequirementKind, Policy, AbstractBaseModel, BaseModel, UUIDField32
from account_manager.signals.signals import policy_updated
from core import activity as core_activity
from core.exceptions import InvalidFormData, ReadOnlyException
from core.fields import SmallIntegerRangeField
from customdb.thumbs import ImageWithThumbsField
from jsonfield import JSONField
from mptt.models import MPTTModel, TreeForeignKey, TreeManyToManyField
from picklefield.fields import PickledObjectField
from putils import add_years
from utils import convert_datetime_to_timezone, log_error

import hashlib
import json
import logging
import os
import uuid
import utils

logger = logging.getLogger(__name__)


def get_insurer_logo_path(self, filename):
    return os.path.join(
        self._meta.app_label, self._meta.model_name, self.insurer.slug, 'logo' + os.path.splitext(filename)[1]
    )


def get_policy_path(instance, filename):
    path_to_save = "policy/%s" % (filename)
    return path_to_save


class DeclinedRisk(BaseModel):
    malady = models.CharField(max_length=100)
    slug = AutoSlugField(populate_from='malady', always_update=True, unique=True)

    def natural_key(self):
        return (self.slug, )

    def __unicode__(self):
        return self.malady


class InsurerManager(models.Manager):

    def get_queryset(self):
        return super(InsurerManager, self).get_queryset().exclude(active=False)


class Insurer(BaseModel):
    name = models.CharField(max_length=200)
    slug = AutoSlugField(populate_from='name', always_update=False, unique=True, max_length=100)
    logo = ImageWithThumbsField(upload_to=get_insurer_logo_path, sizes=(('s', 300, 300), ('t', 500, 500)), null=True, blank=True)
    rule_form = PickledObjectField(null=True, blank=True, default=None, editable=True)
    declined_risks = models.ManyToManyField(DeclinedRisk, blank=True)
    nri = models.BooleanField(default=False)
    minimum_residency = models.FloatField(help_text='years', null=True, blank=True)
    minimum_processing_days = models.PositiveIntegerField(help_text='from date within these many days in case of any ped', null=True, blank=True)
    active = models.BooleanField(default=True)

    all_objects = models.Manager()
    objects = InsurerManager()


    # rather create rule_forms.py and put rule forms in that
    def get_default_rule_form(self):
        from .forms import InsurerRuleForm
        return InsurerRuleForm(self)

    def get_rule_form(self):
        return self.rule_form(self) if callable(self.rule_form) else self.get_default_rule_form()

    def get_nearest_place_available(self, place, scope='included'):
        place_ancestors = place.get_ancestors(include_self=True)
        places_available = Place.objects.filter(id__in=self.plan_set
                                                .values_list('places_%s__id' % scope, flat=True)
                                                .distinct()
                                                )
        common_places = places_available & place_ancestors
        # get most specific place
        highest_level = common_places.aggregate(highest_level=models.Max('level'))['highest_level']
        return common_places.get(level=highest_level) if highest_level is not None else None

    def get_absolute_url(self):
        return reverse('travel_product:insurer_detail', args=[self.slug])

    def natural_key(self):
        return (self.slug, )


class Place(AbstractBaseModel, MPTTModel):
    name = models.CharField(max_length=64)
    slug = AutoSlugField(populate_from='name', always_update=False, unique=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', default=None)
    alias = models.ForeignKey('self', blank=True, null=True)
    display = models.BooleanField(default=True)

    def natural_key(self):
        return (self.slug, )

    @staticmethod
    def get_plans(places_included, places_excluded):
        # TODO: This method can be optimised alot, but kept simple for simplicity for now..
        plans = Plan.objects.all()
        for place in places_included:
            ancestors = place.get_ancestors(include_self=True)
            plans &= Plan.objects.filter(**{'places_included__in': ancestors})

        blacklisted_places = Plan.objects.none()
        for place in places_included:
            blacklisted_places |= place.get_ancestors(include_self=True)
            blacklisted_places |= place.get_descendants(include_self=True)
        blacklisted_places = blacklisted_places.exclude(**{'pk__in': places_excluded})

        for place in blacklisted_places:
            plans = plans.exclude(places_excluded=place)
        return plans

    def save(self, *args, **kwargs):
        superb = super(Place, self).save(*args, **kwargs)
        return superb


class Grade(BaseModel):
    name = models.CharField(max_length=200)
    slug = AutoSlugField(populate_from='name', always_update=False, unique=True)

    def natural_key(self):
        return (self.slug, )


class FeatureName(AbstractBaseModel, MPTTModel):
    name = models.CharField(max_length=64)
    slug = AutoSlugField(populate_from='name', always_update=True, unique=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', default=None)

    def natural_key(self):
        return (self.slug, )


class PlanManager(models.Manager):

    def get_queryset(self):
        return super(PlanManager, self).get_queryset().filter(active=True, insurer__active=True)


def get_document_upload_path(self, filename):
    return self.get_document_upload_path(filename)


class Document(BaseModel):
    name = models.CharField(max_length=255, blank=True)
    file = models.FileField(upload_to=get_document_upload_path)

    def get_document_upload_path(self, filename):
        fname, ext = os.path.splitext(filename)
        r = lambda s: s.replace(' ', '_')
        return os.path.join(
            self._meta.app_label, r(str(self._meta.verbose_name_plural)), r(self.name + ext),
        )

    def save(self, *args, **kwargs):
        if not self.name:
            self.name = os.path.splitext(self.file.name)[0]
        return super(Document, self).save(*args, **kwargs)

    def __unicode__(self):
        return '%s' % (self.name, )


class PolicyWordingDocumentManager(models.Manager):
    def get_queryset(self):
        return super(PolicyWordingDocumentManager, self).get_queryset().filter(plans_with_policy_wording__isnull=False)


class PolicyWordingDocument(Document):
    all_objects = models.Manager()
    objects = PolicyWordingDocumentManager()

    def __unicode__(self):
        return '%s - (%s)' % (self.name, ', '.join((self.plans_with_policy_wording.values_list('name', flat=True).distinct())))

    class Meta(Document.Meta):
        proxy = True


class Plan(BaseModel):
    TYPE_CHOICES = (
            ('single', 'Single Trip'),
            ('multi', 'Multi Trip / Annual'),
            )
    CURRENCY_CHOICES = (
            ('USD', '$'),
            ('EUR', u'\u20ac'),
            ('INR', 'Rs'),
            )

    name = models.CharField(max_length=100)
    verbose_name = models.CharField(max_length=100, blank=True, null=True)  # Worldwide w/o japan
    type = models.CharField(max_length=20, choices=TYPE_CHOICES)
    insurer = models.ForeignKey(Insurer)
    places_included = TreeManyToManyField(Place, related_name='included_plans')
    places_excluded = TreeManyToManyField(Place, related_name='excluded_plans', blank=True)
    grade = models.ForeignKey(Grade)  # Gold
    currency = models.CharField(max_length=3, choices=CURRENCY_CHOICES, default='USD')
    sum_assured = models.PositiveIntegerField()
    features_hash = models.CharField(max_length=64)
    active = models.BooleanField(default=True)
    policy_wording = models.ForeignKey(PolicyWordingDocument, null=True, blank=True, related_name='plans_with_policy_wording')

    all_objects = models.Manager()
    objects = PlanManager()

    def currency_sign(self):
        return dict(self.CURRENCY_CHOICES)[self.currency]

    def save(self, features_changed=False, *args, **kwargs):
        if self.pk and features_changed:
            hash_key = json.dumps(list(self.features.values_list('cover', 'deductible', 'name').order_by('name'))).encode('utf8')
            self.features_hash = hashlib.sha256(hash_key).hexdigest()
        return super(BaseModel, self).save(*args, **kwargs)

    def natural_key(self):
        return (self.name, self.verbose_name, self.insurer, self.type, self.grade, self.currency, self.sum_assured, )

    class Meta:
        unique_together = ("name", "verbose_name", "insurer", "type", "grade", "currency", "sum_assured", )

    def __str__(self):
        return unicode(self)

    def __unicode__(self):
        return "%s - %s - %s - %s%d" % \
                (self.insurer, self.name, dict(self.TYPE_CHOICES)[self.type], dict(self.CURRENCY_CHOICES)[self.currency], self.sum_assured)


class Feature(BaseModel):
    regex_patterns = [
            '(%(currency)s)(%(amount)s)',  # $1200
            '(%(number)s) (%(duration)s)',  # 12 hours
            '(%(currency)s)(%(amount)s) (per|for) (%(number)s) (%(duration)s)',  # $10 per 5 visits
            '(%(currency)s)(%(amount)s) per (%(duration)s) for max (%(number)s) (%(duration)s)',  # $10 per day for max 10 days
            '(%(currency)s)(%(amount)s) per (%(number)s) (%(duration)s) for max (%(currency)s)(%(amount)s)',  # $10 per 12 hours for max $100
            ]
    regex_list = map(lambda r: r % {
        'currency': '\\' + '|\\'.join(dict(Plan.CURRENCY_CHOICES).values()),
        'amount': '\d+(\.\d+)?',
        'number': '\d+',
        'duration': 'hours?|days?|visits?',
        }, regex_patterns
        )
    feature_value_field = lambda regex_list, regex_patterns: models.CharField(max_length=100, null=True, blank=True, validators=[
        # RegexValidator(
        #     regex='(^' + ('$)|(^'.join(regex_list)) + '$)',
        #     message=mark_safe('Allowed value formats are <br />') + mark_safe('<br /><br />'.join(regex_patterns))
        # )
        ])

    name = TreeForeignKey(FeatureName)
    cover = feature_value_field(regex_list, regex_patterns)
    deductible = feature_value_field(regex_list, regex_patterns)
    remarks = models.CharField(max_length=255, null=True, blank=True)

    def __unicode__(self):
        return '%s - %s - %s' % (self.name, self.cover, self.deductible, )


class SlabQuerySet(models.QuerySet):
    default_annotations = {
        ('sub_limit', None),
    }
    _annotation_rules = {}

    def generate_filter_rules(self, cleaned_data):
        filter_rules = {}
        for insurer in Insurer.objects.filter(pk__in=self.values_list('plan__insurer')):
            form = insurer.get_rule_form()
            try:
                insurer_filter_rules = form.get_filter_rules(cleaned_data)
                filter_rules.update({
                    insurer: insurer_filter_rules
                })
            except AssertionError:
                pass
        return filter_rules

    def generate_annotation_rules(self):
        if not self._annotation_rules:
            annotation_rules = {}
            for insurer in Insurer.objects.filter(pk__in=self.values_list('plan__insurer')):
                form = insurer.get_rule_form()
                insurer_annotation_rules = form.get_annotation_rules()
                for alias, when_thens in insurer_annotation_rules.items():
                    annotation_rules[alias] = annotation_rules.get(alias, {})
                    annotation_rules[alias].update({
                        insurer: when_thens
                    })
            self._annotation_rules = annotation_rules
        return self._annotation_rules

    # A desperate attempt to implement some sort of a rule engine
    def apply_annotation_rules(self):
        Case, When, Value, Q = models.Case, models.When, models.Value, models.Q
        rules = self.generate_annotation_rules().items()
        for alias, its_insurers_rules in rules:
            assert alias in dict(self.default_annotations).keys(), \
                    'alias %s missing from default annotation list' % (alias, )
            cases = []
            for insurer, when_thens in its_insurers_rules.items():
                for when, then in when_thens:
                    cases.append(
                        When(when & Q(plan__insurer=insurer), then=Value(then)),
                    )
            self = self.annotate(**{
                alias: Case(output_field=models.CharField(), *cases)
            })
        return self

    def apply_filter_rules(self, cleaned_data):
        Q = models.Q
        rules = self.generate_filter_rules(cleaned_data).items()
        filters = Q()
        for insurer, conditions in rules:
            filters |= (conditions & Q(plan__insurer=insurer))
        return self.filter(filters)


class SlabManager(models.Manager):

    def get_queryset(self):
        annotations = {}
        for alias, value in SlabQuerySet.default_annotations:
            annotations.update({alias: models.Value(value, output_field=models.CharField())})

        return SlabQuerySet(self.model, using=self._db).exclude(active=False).annotate(
            sub_limit=models.Value(None, output_field=models.CharField()),
        )

SLAB_CHOICES = (
    ('family', 'Family'),
    ('individual', 'Individual'),
    ('student', 'Student'),
)


class Slab(BaseModel):
    plan = models.ForeignKey(Plan)
    age_min = SmallIntegerRangeField(_('Age from'), min_value=0, max_value=100, default=0)
    age_max = SmallIntegerRangeField(_('Age to'), min_value=0, max_value=100, default=100)
    premium = models.FloatField()
    source = PickledObjectField(default={})
    active = models.BooleanField(default=True)

    all_objects = models.Manager()
    objects = SlabManager()

    def get_features(self, slab):
        """
        Gets slabs planfetures
        """
        plan_features = Feature.objects.filter(planfeature__plan=self.plan).filter(
            Q(planfeature__slab=slab) | Q(planfeature__slab__isnull='')
        )
        return plan_features

    def __unicode__(self):
        return "%s - (%d - %d)yrs - INR %0.2f" % (self.plan, self.age_min, self.age_max, self.premium)


class IndividualSlab(Slab):
    duration = SmallIntegerRangeField(_('Trip duration'), min_value=1, max_value=365, default=30)

    all_objects = models.Manager()
    objects = SlabManager()

    def save(self, *args, **kwargs):
        if not self.pk:
            assert (not IndividualSlab.objects.filter(
                plan=self.plan,
                age_min=self.age_max,
                age_max=self.age_max,
                duration=self.duration).exists()
            ), 'Slab already Exists'
        return super(IndividualSlab, self).save(*args, **kwargs)


class FamilySlab(Slab):
    children_max = SmallIntegerRangeField(_('Maximum number of children'), min_value=0, max_value=30, default=2)
    adult_max = SmallIntegerRangeField(_('Maximum number of adults'), min_value=0, max_value=30, default=2)
    size = SmallIntegerRangeField(_('Total family size'), min_value=0, max_value=60, default=4, blank=True)
    duration = SmallIntegerRangeField(_('Trip duration'), min_value=1, max_value=365, default=30)

    all_objects = models.Manager()
    objects = SlabManager()

    def save(self, *args, **kwargs):
        if not self.size:
            self.size = self.children_max + self.adult_max
        return super(FamilySlab, self).save(*args, **kwargs)


class StudentSlab(Slab):
    duration = SmallIntegerRangeField(_('Trip duration'), min_value=1, max_value=1460, default=365)


class PlanFeature(Feature):
    plan = models.ForeignKey(Plan, related_name='features')
    slab = models.CharField(max_length=20, choices=SLAB_CHOICES, blank=True)

    def save(self, features_saved=False, *args, **kwargs):
        super(Feature, self).save(*args, **kwargs)
        self.plan.save(features_changed=True)

    def __unicode__(self):
        return '%s - %s' % (super(PlanFeature, self).__unicode__(), 'Plan')


class SlabFeature(Feature):
    slab = models.ForeignKey(Slab, related_name='features')

    def __unicode__(self):
        return '%s - %s' % (super(SlabFeature, self).__unicode__(), 'Slab')


class Activity(BaseModel):
    id = UUIDField32(primary_key=True, default=uuid.uuid4)
    tracker = models.ForeignKey(Tracker, related_name='travel_activities')
    data = PickledObjectField(default={})

    @lru_cache.lru_cache()
    def get_quote_data(self, fail_silently=False):
        activity_quote_form = self.data.get('quote_form')
        from .forms import QuoteForm
        form = QuoteForm(activity_quote_form.get('data', {}))
        if form.is_valid():
            return form.data, form.cleaned_data
        elif fail_silently:
            return form.data, {}
        else:
            raise InvalidFormData(form.errors)

    def set_quote_data(self, activity_quote_form_data, commit=True):
        activity_quote_form_data = activity_quote_form_data.copy()
        activity_quote_form_data.pop('csrfmiddlewaretoken') if 'csrfmiddlewaretoken' in activity_quote_form_data else None
        self.data['quote_form']['data'] = activity_quote_form_data
        return self.save() if commit else None

    @property
    def policy_start_date(self):
        from .forms import QuoteForm
        quote_data = self.get_quote_data(fail_silently=True)[0]
        return QuoteForm.base_fields['from_date'].to_python(quote_data['from_date'])

    @property
    def policy_end_date(self):
        from .forms import QuoteForm
        quote_data = self.get_quote_data(fail_silently=True)[0]
        if quote_data['type'] == 'single':
            end_date = QuoteForm.base_fields['to_date'].to_python(quote_data['to_date'])
        else:
            assert quote_data['type'] == 'multi'
            end_date = add_years(self.policy_start_date, 1) - datetime.timedelta(days=1)
        return end_date

    @property
    def cleaned_data(self):
        return self.get_quote_data()[1]

    @cleaned_data.setter
    def cleaned_data(self, value):
        assert value is None  # cleaned data can only be set to None (to unset cached value), can't be directly set
        self.get_quote_data.cache_clear()

    def get_absolute_url(self):
        return reverse('travel:quote:result', args=(self.id, ))

    def save(self, *args, **kwargs):
        self.data['quote_form'] = self.data.get('quote_form', {'data': {}, })
        self.cleaned_data = None
        return super(Activity, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"%s" % (self.id)


class Transaction(BaseModel):

    DRAFT = 'DRAFT'
    PENDING = 'PENDING'
    COMPLETED = 'COMPLETED'
    FAILED = 'FAILED'
    ######################################################

    TRANSACTION_STATUS_CHOICES = (
        (DRAFT, u'Draft'),
        (PENDING, u'Pending'),
        (COMPLETED, u'Completed'),
        (FAILED, u'Failed'),
    )

    id = UUIDField32(primary_key=True, default=uuid.uuid4)
    activity = models.ForeignKey(Activity, related_name='travel_transactions')
    slab = models.ForeignKey(Slab)
    data = PickledObjectField(default={})
    transaction_id = models.CharField(max_length=36,null=True,blank=True)
    status = models.CharField(max_length=20, choices=TRANSACTION_STATUS_CHOICES, default=DRAFT)
    status_history = PickledObjectField(default=())
    email = models.EmailField(max_length=255, blank=False, db_index=True, null=True)
    first_name = models.CharField(max_length=50, blank=True, null=True)
    last_name = models.CharField(max_length=50, blank=True, null=True)
    mobile = models.CharField(max_length=50, blank=True, null=True)
    requirement = models.ForeignKey(
        'core.Requirement', blank=True, null=True, related_name='travel_transactions'
    )
    policy_number = models.CharField(max_length=250, blank=True)
    policy_start_date = models.DateField(null=True, blank=True)
    policy_end_date = models.DateField(null=True, blank=True)
    payment_request = models.TextField(blank=True)
    payment_response = models.TextField(blank=True)
    payment_on = models.DateTimeField(blank=True, null=True)
    reference_id = models.CharField(max_length=250, null=True, blank=True)
    policy = models.OneToOneField('core.Policy', null=True, blank=True, related_name='travel_transaction')
    policy_document = models.FileField(upload_to=get_policy_path, null=True, blank=True)


    @property
    def tracker(self):
        return self.activity.tracker

    @property
    def insurer_slug(self):
        return self.slab.plan.insurer.slug

    @property
    def plan_details(self):
        return self.data['plan_details']

    @property
    def extra(self):
        return self.data

    @property
    def insured_details_json(self):
        return self.data['insured_details_json']

    @property
    def last_status(self):
        return self.status_history[-1] if self.status_history else None

    def __unicode__(self):
        return u"%s" % (self.slab)

    def save(self, read_only=True, *args, **kwargs):
        if self.last_status == Transaction.COMPLETED and read_only:
            raise ReadOnlyException()
        else:
            if self.status != self.last_status:
                self.status_history += (self.status, )

            self.email = self.insured_details_json.get('email') or self.email
            self.first_name = self.insured_details_json.get('first_name') or self.first_name
            self.last_name = self.insured_details_json.get('last_name') or self.last_name
            self.mobile = self.insured_details_json.get('mobile') or self.mobile
            if not self.policy_start_date:
                self.policy_start_date = self.activity.policy_start_date
            if not self.policy_end_date:
                self.policy_end_date = self.activity.policy_end_date

            superb = super(BaseModel, self).save(*args, **kwargs)
            if self.id and not self.transaction_id:
                self.transaction_id = self.id
                superb = self.save(*args, **kwargs)
            return superb

    def mini_json(self):
        return {
            'id': self.id,
            'transaction_id': self.transaction_id,
            'payment_date': self.payment_on,
            'mobile': self.mobile
        }

    def open_requirement_if_deleted(self):
        is_requirement_with_state = self.requirement and self.requirement.current_state
        if is_requirement_with_state and self.requirement.current_state.pk == 17:
            # Change state to previous state.
            self.requirement.reopen(previous_state=True)

    def payment_done(self):
        return bool(self.payment_response)
    payment_done.boolean = True

    def payment_tried(self):
        return bool(self.payment_response)
    payment_tried.boolean = True

    @property
    def premium_paid(self):
        return round(self.slab.premium * (100 + settings.SERVICE_TAX_PERCENTAGE) / 100, 2)

    @premium_paid.setter
    def premium_paid(self, value):
        pass

    class Meta:
        ordering = ('-created_date', '-payment_on', )

    def update_policy(self, content, send_mail=True):
        policy_path = get_policy_path(self, u'{}.pdf'.format(self.transaction_id))
        with default_storage.open(policy_path, 'wb') as f:
            for chunk in ContentFile(content).chunks():
                f.write(chunk)
        self.policy_document = policy_path
        self.save(update_fields=['policy_document'], read_only=False)

        if not self.policy:
            try:
                self.populate_policy()
            except:
                log_error(logger, msg='Unable to populate travel policy')

        policy_updated.send(sender=self._meta.model, transaction=self, send_mail=send_mail)

    def populate_policy(self):
        from core import models as core_models
        from account_manager.models import UserTransaction
        user = UserTransaction.objects.get_for_transaction(self).user
        if not user:
            logger.error("No user found in UserTransaction for transaction_id(%s)" % self.transaction_id)

        # Generate form data.
        form_data = self.track_activity(
            core_activity.SUCCESS, 'policy'
        )

        act = core_activity.create_activity(
            activity_type=core_activity.SUCCESS,
            product=RequirementKind.TRAVEL,
            page_id='policy',
            form_name='',
            form_position='',
            form_variant='',
            user=user,
            visitor=self.activity.tracker,
            data=form_data
        )

        start_date = convert_datetime_to_timezone(self.policy_start_date)
        end_date = convert_datetime_to_timezone(self.policy_end_date)
        policy = Policy.objects.create(
            name=os.path.splitext(self.policy_document.name)[0].split(os.path.sep)[-1],
            document=self.policy_document,
            app=self._meta.app_label,
            status=Policy.VALID,
            user=user,
            requirement_sold=act.requirement,
            policy_number=self.policy_number,
            premium=self.premium_paid or None,
            issue_date=start_date,
            expiry_date=end_date,
        )

        if not act:
            logger.critical(
                "Exception occurred while populating policy. Got not activity response.",
                exc_info=True,
                extra={
                    "email": self.email,
                    "transaction_id": self.transaction_id,
                    "user_id": user.pk,
                    "policy_id": policy.pk
                }
            )
            return

        act.requirement.policy = policy
        act.requirement.save(update_fields=['policy'])
        self.policy = policy
        self.save(update_fields=['policy'], read_only=False)

    def get_form_data(self):
        from . import form_data
        from .forms import TravelFDForm
        data = form_data.generate_form_data(self)
        return TravelFDForm(data=data).get_fd()

    @property
    def transaction_mode(self):
        return 'online'

    @property
    def transaction_type(self):
        return 'travel'

    def proposal_form_link(self):
        return reverse('travel:transaction:buy', args=(self.pk,))

    def customer_email(self):
        return self.extra['insured_details_json']['members'][0]['email']

    def customer_mobile(self):
        return self.extra['insured_details_json']['members'][0]['mobile']

    def customer_first_name(self):
        return self.extra['insured_details_json']['members'][0]['first_name']

    def customer_last_name(self):
        return self.extra['insured_details_json']['members'][0]['last_name']

    def track_activity(self, activity_type, page_id, requirement=None, request_data={}):
        request = utils.get_request()
        try:
            data = {}
            data.update(self.data)
            data.update({
                'travel_date': self.activity.cleaned_data['from_date'].strftime('%d/%m/%Y'),
                'return_date': self.activity.cleaned_data['to_date'].strftime('%d/%m/%Y'),
                'places': ', '.join([place.name for place in self.activity.cleaned_data['places']]),
                'transaction_id': self.transaction_id,
                'premium_paid': round((self.slab.premium) * (100 + settings.SERVICE_TAX_PERCENTAGE) / 100, 2),
                'product_title': self.slab.plan.name,
                'insurer_title': self.slab.plan.insurer.name,
                'mobile': self.customer_mobile(),
                'email': self.customer_email(),
                'proposer_first_name': self.customer_first_name(),
                'proposer_middle_name': "",
                'proposer_last_name': self.customer_last_name(),
                'policy_document': request.build_absolute_uri(self.policy_document.url) if self.policy_document else None,
            })

            kwargs = {
                'activity_type': activity_type,
                'product': 'travel',
                'page_id': page_id,
                'form_name': '',
                'form_variant': '',
                'form_position': '',
                'data': self.get_form_data(),
                'requirement': requirement
            }

            act = core_activity.create_activity(**kwargs)
            if act:
                act.extra.update(data)
                act.save(update_fields=['extra'])

            has_act_requirement = False
            if act and act.requirement:
                has_act_requirement = True

            if (not self.requirement and has_act_requirement) or (
                (self.requirement and has_act_requirement) and
                (self.requirement.pk is not act.requirement.pk)
            ):
                self.requirement = act.requirement
                self.save(update_fields=['requirement'])
        except:
            utils.log_error(logger, request, 'Unable to track travel activity')
        else:
            return act

    @property
    def policy_tat(self):
        INSURER_POLICY_TAT = {
            'bharti-axa': 15,
            'bajaj-allianz': 120,
            'reliance': 15,
            'religare': 15,     # was not in sheet
            'hdfc-ergo': 15,
            'apollo-munich': 15,    # was not in sheet
        }
        return int(INSURER_POLICY_TAT.get(self.slab.plan.insurer.slug, 24 * 60))


class InsurerPolicyData(models.Model):
    insurer = models.CharField(max_length=255, db_index=True)
    form_data = JSONField(blank=True, default={})
    hash_form_data = models.CharField(blank=True, db_index=True, max_length=255, unique=True)
    plans_list = JSONField(blank=True, default={})
