import thread
import traceback
import datetime

from lxml import etree
from django.core.mail import send_mail
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db.models import Q

from account_manager.models import UserAccount
import travel_product.insurers.settings as insurer_settings
from core.models import User, Email
from travel_product.models import Transaction
from travel_product import settings as travel_settings
import utils
import putils
from utils import travel_logger

from dateutil.relativedelta import relativedelta


def trigger_lms_event(eventType, data, transaction):
    try:
        user = User.objects.get_user(email=transaction.email, phone='')
    except User.DoesNotExist:
        user = None

    try:
        request = utils.get_request()

        data.update(transaction.data.copy())

        mobile = transaction.insured_details_json['members'][0]['mobile']
        email = transaction.insured_details_json['members'][0]['email']
        first_name = transaction.insured_details_json['members'][0]['first_name']
        last_name = transaction.insured_details_json['members'][0]['last_name']

        data.update({
            'travel_date': transaction.activity.cleaned_data['from_date'].strftime('%d/%m/%Y'),
            'return_date': transaction.activity.cleaned_data['to_date'].strftime('%d/%m/%Y'),
            'places': ', '.join([place.name for place in transaction.activity.cleaned_data['places']]),
            'transaction_id': transaction.transaction_id,
            'premium_paid': round((transaction.slab.premium) * (100 + settings.SERVICE_TAX_PERCENTAGE) / 100, 2),
            'product_title': transaction.slab.plan.name,
            'insurer_title': transaction.slab.plan.insurer.name,
            'mobile': mobile,
            'email': email,
            'proposer_first_name': first_name,
            'proposer_middle_name': "",
            'proposer_last_name': last_name,
            'success_type': travel_settings.LMS_TRANSACTION_SUCCESS_FLAGS.get((transaction.slab.plan.insurer.slug, 'noped' if all(value.encode('utf-8').lower() == 'no' for value in transaction.insured_details_json.get('ped', {'dum': 'no'}).itervalues()) else 'ped'), 'delayed'),
            'customer_type': 'old' if UserAccount.objects.filter(Q(user=user) | Q(mobile=mobile)) else 'new',

            'policy_document': request.build_absolute_uri(transaction.policy_document.url) if hasattr(transaction, 'policy_document') and transaction.policy_document else None,  # TODO: REMOVE hasattr condition in future.
            'travel_admins': ', '.join(['{} <{}>'.format(name, email) for name, email in settings.TRAVEL_ADMINS]),
        })

        utils.LMSUtils.send_lms_event(
            event=eventType,
            product_type='travel',
            tracker=transaction.activity.tracker,
            data=data,
            email=email,
            mobile=mobile,
            internal=request.IS_INTERNAL,
            request_ip=putils.get_request_ip(request)
        )
    except Exception as e:
        travel_logger.info("+++++++ LMS EVENT EXCEPTION: %s +++++++++++ \n %s \n" %( transaction.transaction_id, traceback.format_exc()))


def trigger_success_event(transaction):
    request = utils.get_request()

    # Sending custom signal on successful payment
    from account_manager.signals.signals import online_transaction_completed
    online_transaction_completed.send(sender=Transaction, transaction=transaction, user=request.user)


def alert_mail(transaction, sub_text, extra_data={}):
    mail_list = dict(settings.TRAVEL_ADMINS).values()
    insurer = insurer_settings.INSURER_SLUG_MAP[transaction.insurer_slug]
    msg = {
        'server': settings.SITE_URL,
        'Insurance type': 'Travel',
        'Transaction Id': transaction.transaction_id,
        'Insurer': insurer,
        'Product': transaction.plan_details.get('plan', ''),
        'Sum assured': transaction.plan_details.get('sum_assured', ''),
        'Premium': transaction.plan_details.get('premium', ''),
        'Buy form': settings.SITE_URL + reverse('travel:transaction:buy', args=(transaction.transaction_id,)),
    }
    if transaction.status == transaction.COMPLETED:
        mail_list.append('ops@coverfox.com')
        if transaction.policy_document:
            msg['Policy'] = transaction.policy_document.url
    else:
        mail_list.append('qa@coverfoxmail.com')
        msg['Error message'] = transaction.extra.get('error_message', '')
    msg.update(extra_data)
    thread.start_new_thread(send_mail, (sub_text, "\n".join(["%s: %s" % (k, v) for k, v in msg.items()]), "Dev Bot <dev@cfstatic.org>", mail_list))


def send_success_mail(transaction, mailType, extra_data = {}):
    sub_text = 'ALERT: %s Success' %mailType
    alert_mail(transaction, sub_text, extra_data)


def send_failure_mail(transaction, mailType, extra_data = {}):
    sub_text = 'ALERT: %s Failed' %mailType
    alert_mail(transaction, sub_text, extra_data)


def dict_to_etree(root, request_dict):
    for key, value in request_dict.items():
        node = etree.Element(key)
        if isinstance(value, dict):
            elem = dict_to_etree(node, value)
        elif isinstance(value, list):
            print "TAG: ",node.tag
            elem = list_to_etree(node, value)
        else:
            if value is None:
                node.text = ''
            else:
                node.text = str(value)
        root.append(node)
    return root


def list_to_etree(element, request_list):
    node = element
    for index, item in enumerate(request_list):
        if type(item) in [list, dict]:
            elem = dict_to_etree(node, item)
        else:
            elem = etree.Element(str(item))
    return element


def calculate_age(birth):
    try:
        if type(birth) is str or type(birth) is unicode:
            birth = datetime.datetime.strptime(birth, '%d-%m-%Y')

        today = datetime.datetime.today()
        if today > birth:
            age = relativedelta(today, birth)
            return age.years + age.months/12.0 + age.days/365.0
        else:
            raise Exception(
                "Wow!! you must be a time traveller. Well, FUCK YOU!")
    except Exception as e:
        travel_logger.info("+++++++ AGE CALULATION EXCEPTION: %s ++++ \n %s \n" %(birth, traceback.format_exc()))
        pass
