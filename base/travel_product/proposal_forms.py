import sys
from django import forms
from django.forms import BaseFormSet
from django.forms.formsets import formset_factory
from travel_product import custom_fields as cf
from travel_product import custom_validators as cv
from travel_product.models import Insurer
from travel_product.insurers import form_data as fd
from travel_product.models import Transaction
import travel_product.insurers.settings as insurer_settings

from importlib import import_module
import datetime

try:
    DISEASE_CHOICES = list(Insurer.objects.get(slug='reliance').declined_risks.values_list('malady', flat=True))
    DISEASE_CHOICES.append("None")
except:
    DISEASE_CHOICES = []


class LayoutForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(LayoutForm, self).__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.set_layout('id', self.__getitem__(field_name).html_name)
            field.set_layout('prefix', self.prefix)

    def get_form_layout(self):
        form_layout = {}
        self.full_clean()
        for field_name, field in self.fields.items():
            if self._errors is not None and self._errors.get(field_name):
                field.set_layout("errorMessage", self._errors[field_name][0])
            form_layout[field_name] = field.get_layout()

        form_layout['non_field_errors'] = self.non_field_errors()
        return form_layout

    def prefill(self, data):
        for field_name, field in self.fields.items():
            html_name = self.__getitem__(field_name).html_name
            if data.get(html_name) is not None:
                field.prefill_value = data.get(html_name)

    @staticmethod
    def set_initials(forms):
        pass


class BaseMemberForm(LayoutForm):
    first_name = cf.CharField(label="First Name", min_length=2, max_length=50, validators=[cv.CharValidator])
    last_name = cf.CharField(label="Last Name", min_length=2, max_length=50, validators=[cv.CharValidator])
    gender = cf.ChoiceField(label="Gender", choices=fd.GENDER_CHOICES, caption="--")
    date_of_birth = cf.DateField(label="Date Of Birth")
    passport_no = cf.CharField(label="Passport No.", max_length=8, validators=[cv.PassportValidator])
    mobile = cf.CharField(
        required=False,
        label="10 Digit Mobile No.",
        min_length=10,
        max_length=10,
        validators=[cv.NumberValidator, cv.MobileValidator]
    )
    email = cf.EmailField(required=False, label="Email Address")
    relationship = cf.ChoiceField(label="Relationship with traveller 1", choices=fd.RELATION_CHOICES)
    married = cf.ChoiceField(label="Married", choices=fd.MARRIED_CHOICES, caption='--')

    class Meta:
        abstract = True

    def clean_date_of_birth(self):
        val = self.cleaned_data['date_of_birth']
        return datetime.datetime.strftime(val, '%d-%m-%Y')

    def clean_first_name(self):
        val = self.cleaned_data['first_name']
        new_val = " ".join(val.split())
        if new_val == '':
            raise forms.ValidationError('Invalid Value')
        return new_val

    def clean_last_name(self):
        val = self.cleaned_data['last_name']
        new_val = " ".join(val.split())
        if new_val == '':
            raise forms.ValidationError("Invalid Value")
        return new_val

    def clean_passport_no(self):
        value = self.cleaned_data['passport_no']
        return value.upper()


class ApolloMunichMemberForm(BaseMemberForm):
    disease_choices = fd.APOLLO_MEDICAL_CHOICES
    nominee_name = cf.CharField(label="Nominee Name", validators=[cv.CharValidator])
    nominee_relation = cf.ChoiceField(label="Nominee Relation", choices=fd.APOLLO_NOMINEE_RELATION_CHOICES)
    ped = cf.ChoiceField(
        required=False,
        label="Are you suffering from any of the listed diseases?",
        choices=disease_choices,
        initial="None"
    )

    def clean_ped(self):
        if 'ped' not in self.cleaned_data:
            return self.fields['ped'].initial
        return self.cleaned_data['ped']

    def clean_nominee_name(self):
        val = self.cleaned_data['nominee_name']
        new_val = " ".join(val.split())
        if new_val == '':
            raise forms.ValidationError("Invalid Value")
        return new_val


class BhartiMemberForm(BaseMemberForm):
    nominee_name = cf.CharField(label="Nominee Name", validators=[cv.CharValidator])
    nominee_relation = cf.ChoiceField(label="Nominee Relation", choices=fd.NOMINEE_RELATION_CHOICES)
    appointee_name = cf.CharField(label="Appointee Name", validators=[cv.CharValidator])
    appointee_relation = cf.ChoiceField(label="Appointee Relation", choices=fd.NOMINEE_RELATION_CHOICES)
    ped = cf.RadioField(label="Are you suffering from any pre-existing disease?")
    adventure = cf.RadioField(label="Do you engage in any kind of adventure sports?")
    sports = cf.RadioField(label="Are you a semi-professional/professoional sportsperson")

    def clean_ped(self):
        if 'ped' not in self.cleaned_data:
            return self.fields['ped'].initial
        return self.cleaned_data['ped']

    def clean_adventure(self):
        if 'adventure' not in self.cleaned_data:
            return self.fields['adventure'].initial
        return self.cleaned_data['adventure']

    def clean_sports(self):
        if 'sports' not in self.cleaned_data:
            return self.fields['sports'].initial
        return self.cleaned_data['sports']

    def clean_nominee_name(self):
        val = self.cleaned_data['nominee_name']
        new_val = " ".join(val.split())
        if new_val == '':
            raise forms.ValidationError("Invalid Value")
        return new_val


class ReligareMemberForm(BaseMemberForm):
    nominee_name = cf.CharField(label="Nominee Name", validators=[cv.CharValidator])
    ped = cf.RadioField(label="Are you suffering from any pre-existing disease?")
    liver_disease = cf.RadioField(required=False, label="Liver Disease?")
    cancer_disease = cf.RadioField(required=False, label="Cancer/Tumor?")
    heart_disease = cf.RadioField(required=False, label="Coronary Artery Heart Disease?")
    kidney_disease = cf.RadioField(required=False, label="Kidney Disease?")
    paralysis = cf.RadioField(required=False, label="Paralysis/Stroke?")
    other_disease = cf.RadioField(required=False, label="Any Other Disease?")
    other_disease_name = cf.CharField(required=False, label="Disease Name")
    claimed_policy = cf.RadioField(label="Have you ever claimed under any travel policy?")
    hospitalized = cf.RadioField(label="Have you ever been diagnosed/hospitalized during the last 48 months?")

    def clean(self):
        data = self.cleaned_data

        if data.get('ped') and data['ped'] == 'yes':
            is_any_selected = (data['liver_disease'] == 'yes')
            is_any_selected |= (data['cancer_disease'] == 'yes')
            is_any_selected |= (data['heart_disease'] == 'yes')
            is_any_selected |= (data['kidney_disease'] == 'yes')
            is_any_selected |= (data['paralysis'] == 'yes')
            is_any_selected |= (data['other_disease'] == 'yes')

            if not is_any_selected:
                raise forms.ValidationError("Please select anyone of disease list")

            if(data['other_disease'] == 'yes' and data.get('other_disease_name') is None):
                raise forms.ValidationError("Please provide Other Disease Name")

    def clean_nominee_name(self):
        val = self.cleaned_data['nominee_name']
        new_val = " ".join(val.split())
        if new_val == '':
            raise forms.ValidationError("Invalid Value")
        return new_val

    def clean_liver_disease(self):
        if 'liver_disease' not in self.cleaned_data:
            return 'no'
        return self.cleaned_data['liver_disease']

    def clean_kidney_disease(self):
        if 'kidney_disease' not in self.cleaned_data:
            return 'no'
        return self.cleaned_data['kidney_disease']

    def clean_cancer_disease(self):
        if 'cancer_disease' not in self.cleaned_data:
            return 'no'
        return self.cleaned_data['cancer_disease']

    def clean_heart_disease(self):
        if 'heart_disease' not in self.cleaned_data:
            return 'no'
        return self.cleaned_data['heart_disease']

    def clean_paralysis(self):
        if 'paralysis' not in self.cleaned_data:
            return 'no'
        return self.cleaned_data['paralysis']

    def clean_other_disease(self):
        if 'other_disease' not in self.cleaned_data:
            return 'no'
        return self.cleaned_data['other_disease']


class RelianceMemberForm(BaseMemberForm):
    disease_choices = DISEASE_CHOICES

    ped = cf.ChoiceField(
        required=False,
        label="Are you suffering from any of the listed diseases?",
        choices=[(c, c) for c in disease_choices],
    )

    nominee_name = cf.CharField(label="Nominee Name", validators=[cv.CharValidator])
    nominee_age = cf.IntegerField(label="Nominee Age", min_value=18, max_value=99)
    nominee_relation = cf.ChoiceField(label="Nominee Relationship", choices=fd.NOMINEE_RELATION_CHOICES)
    occupation = cf.ChoiceField(label="occupation", choices=fd.OCCUPATION_CHOICES, hidden=True, initial="Others")

    def __init__(self, *args, **kwargs):
        super(RelianceMemberForm, self).__init__(*args, **kwargs)

    def clean_ped(self):
        if 'ped' not in self.cleaned_data:
            return self.fields['ped'].initial
        return self.cleaned_data['ped']

    def clean_nominee_name(self):
        val = self.cleaned_data['nominee_name']
        new_val = " ".join(val.split())
        if new_val == '':
            raise forms.ValidationError("Invalid Value")
        return new_val


class HdfcErgoMemberForm(BaseMemberForm):
    purpose_of_visit_choices = (
        ('business', 'Business'),
        ('holiday', 'Holiday'),
    )

    nominee_name = cf.CharField(label="Nominee Name", validators=[cv.CharValidator])
    nominee_relation = cf.ChoiceField(label="Nominee Relationship", choices=fd.NOMINEE_RELATION_CHOICES)
    ped = cf.RadioField(label="Do you suffer from any past medical history or any current medical adversities")
    medical_declined = cf.RadioField(label="Medical Insurance declined by any insurer for you?")
    restrictions = cf.RadioField(
        label="Have any Restrictions/Special Conditions been imposed in any medical insurance taken by you?"
    )
    purpose_of_visit = cf.ChoiceField(label="Purpose Of Visit", choices=purpose_of_visit_choices, hidden=True, initial='business')

    def __init__(self, *args, **kwargs):
        super(HdfcErgoMemberForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].validators.append(cv.NoSpaceValidator)
        self.fields['last_name'].validators.append(cv.NoSpaceValidator)

    @staticmethod
    def set_initials(forms):
        if(len(forms) > 1):
            for form in forms:
                form.fields['purpose_of_visit'].initial = 'holiday'

    def clean_ped(self):
        if 'ped' not in self.cleaned_data:
            return self.fields['ped'].initial
        return self.cleaned_data['ped']

    def clean_medical_declined(self):
        if 'medical_declined' not in self.cleaned_data:
            return self.fields['medical_declined'].initial
        return self.cleaned_data['medical_declined']

    def clean_restrictions(self):
        if 'restrictions' not in self.cleaned_data:
            return self.fields['restrictions'].initial
        return self.cleaned_data['restrictions']

    def clean_nominee_name(self):
        val = self.cleaned_data['nominee_name']
        new_val = " ".join(val.split())
        if new_val == '':
            raise forms.ValidationError("Invalid Value")
        return new_val


class BajajAllianzMemberForm(BaseMemberForm):
    married_choices = (('yes', 'Yes'), ('no', 'No'))

    assignee_name = cf.CharField(label="Assignee Full Name")
    ped = cf.RadioField(label="Do you have any pre-existing disease?")

    def clean_ped(self):
        if 'ped' not in self.cleaned_data:
            return self.fields['ped'].initial
        return self.cleaned_data['ped']

    def clean_assignee_name(self):
        val = self.cleaned_data['assignee_name']
        new_val = " ".join(val.split())
        if new_val == '':
            raise forms.ValidationError("Invalid Value")
        return new_val


class UniversalSompoMemberForm(BaseMemberForm):
    first_name = cf.CharField(label="First Name", min_length=2, max_length=25, validators=[cv.CharValidator])
    last_name = cf.CharField(label="Last Name", min_length=2, max_length=24, validators=[cv.CharValidator])
    destination_city = cf.CharField(label="Destination City", max_length=50, validators=[cv.CharValidator])
    nominee_name = cf.CharField(label="Nominee Name", max_length=50, validators=[cv.CharValidator])
    email = cf.EmailField(required=False, max_length=50, label="Email Address")

    nominee_relation = cf.ChoiceField(label="Nominee Relationship", choices=fd.NOMINEE_RELATION_CHOICES)
    ped = cf.RadioField(label="Are you suffering from any pre-existing disease?")

    def clean_ped(self):
        if 'ped' not in self.cleaned_data:
            return self.fields['ped'].initial
        return self.cleaned_data['ped']

    def clean_nominee_name(self):
        val = self.cleaned_data['nominee_name']
        new_val = " ".join(val.split())
        if new_val == '':
            raise forms.ValidationError("Invalid Value")
        return new_val


class BaseAddressForm(LayoutForm):
    address_line_1 = cf.CharField(label="Address Line 1", max_length=30)
    address_line_2 = cf.CharField(label="Address Line 2", max_length=30)
    address_line_3 = cf.CharField(label="Address Line 3", max_length=30)
    pincode = cf.CharField(label="Pincode", max_length=6, min_length=6, validators=[cv.NumberValidator])
    state = cf.ChoiceField(label="State", choices=())
    city = cf.ChoiceField(label="City", choices=(), required=False)

    def __init__(self, *args, **kwargs):
        super(BaseAddressForm, self).__init__(*args, **kwargs)

        insurer = insurer_settings.INSURER_SLUG_MAP[self.insurer_slug]
        ins_module = import_module('travel_product.insurers.%s.buy_form' % insurer)

        state_choices = [(s['value'], s['key']) for s in ins_module.STATES]
        self.fields['state'].choices = state_choices

        if 'state' in self.data:
            city_choices = [(c['value'], c['key']) for c in ins_module.CITIES[self.data['state']]]
            self.fields['city'].choices = city_choices

    def clean_address_line_1(self):
        val = self.cleaned_data['address_line_1']
        new_val = " ".join(val.split())
        if self.fields['address_line_1'].required and new_val == '':
            raise forms.ValidationError("Invalid value")
        return new_val

    def clean_address_line_2(self):
        val = self.cleaned_data['address_line_2']
        new_val = " ".join(val.split())
        if self.fields['address_line_2'].required and new_val == '':
            raise forms.ValidationError("Invalid value")
        return new_val

    def clean_address_line_3(self):
        val = self.cleaned_data['address_line_3']
        new_val = " ".join(val.split())
        if self.fields['address_line_3'].required and new_val == '':
            raise forms.ValidationError("Invalid value")
        return new_val

    class Meta:
        abstract = True


class BhartiAddressForm(BaseAddressForm):
    insurer_slug = 'bharti-axa'


class ApolloMunichAddressForm(BaseAddressForm):
    insurer_slug = 'apollo-munich'


class RelianceAddressForm(BaseAddressForm):
    insurer_slug = 'reliance'
    same_address = cf.CheckboxField(required=False, label="Home address is same as Communication address", initial=True)
    home_address_line_1 = cf.CharField(label="Address Line 1", max_length=30, required=False)
    home_address_line_2 = cf.CharField(label="Address Line 2", max_length=30, required=False)
    home_address_line_3 = cf.CharField(label="Address Line 3", max_length=30, required=False)
    home_pincode = cf.CharField(label="Pincode", max_length=6, min_length=6, validators=[cv.NumberValidator], required=False)
    home_state = cf.ChoiceField(label="State", choices=(), required=False)
    home_city = cf.ChoiceField(label="City", choices=(), required=False)

    def __init__(self, *args, **kwargs):
        super(RelianceAddressForm, self).__init__(*args, **kwargs)

        insurer = insurer_settings.INSURER_SLUG_MAP[self.insurer_slug]
        ins_module = import_module('travel_product.insurers.%s.buy_form' % insurer)

        state_choices = [(s['value'], s['key']) for s in ins_module.STATES]
        self.fields['home_state'].choices = state_choices

        if 'home_state' in self.data and self.data['home_state']:
            city_choices = [(c['value'], c['key']) for c in ins_module.CITIES[self.data['home_state']]]
            self.fields['home_city'].choices = city_choices

        if 'same_address' in self.data and not self.data['same_address']:
            self.fields['home_address_line_1'].required = True
            self.fields['home_address_line_2'].required = True
            self.fields['home_address_line_3'].required = True
            self.fields['home_pincode'].required = True
            self.fields['home_city'].required = True
            self.fields['home_state'].required = True

    def clean(self):
        if 'same_address' in self.cleaned_data and self.cleaned_data['same_address']:
            self.cleaned_data['home_address_line_1'] = self.cleaned_data['address_line_1']
            self.cleaned_data['home_address_line_2'] = self.cleaned_data['address_line_2']
            self.cleaned_data['home_address_line_3'] = self.cleaned_data['address_line_3']
            self.cleaned_data['home_pincode'] = self.cleaned_data['pincode']
            self.cleaned_data['home_state'] = self.cleaned_data['state']
            self.cleaned_data['home_city'] = self.cleaned_data['city']


class BajajAllianzAddressForm(BaseAddressForm):
    insurer_slug = 'bajaj-allianz'

    def __init__(self, *args, **kwargs):
        super(BajajAllianzAddressForm, self).__init__(*args, **kwargs)
        self.fields['address_line_3'].required = False


class ReligareAddressForm(BaseAddressForm):
    insurer_slug = 'religare'

    def __init__(self, *args, **kwargs):
        super(ReligareAddressForm, self).__init__(*args, **kwargs)
        self.fields['address_line_3'].required = False


class HdfcErgoAddressForm(BaseAddressForm):
    insurer_slug = 'hdfc-ergo'

    def __init__(self, *args, **kwargs):
        super(HdfcErgoAddressForm, self).__init__(*args, **kwargs)
        self.fields['address_line_1'].validators.append(cv.HdfcAddressValidator)
        self.fields['address_line_2'].validators.append(cv.HdfcAddressValidator)
        self.fields['address_line_3'].validators.append(cv.HdfcAddressValidator)


class UniversalSompoAddressForm(BaseAddressForm):
    insurer_slug = 'universal-sompo'


this_module = sys.modules[__name__]


INSURER_SLUG_MAPPING = {
    'religare': 'Religare',
    'reliance': 'Reliance',
    'hdfc-ergo': 'HdfcErgo',
    'bharti-axa': 'Bharti',
    'bajaj-allianz': 'BajajAllianz',
    'apollo-munich': 'ApolloMunich',
    'universal-sompo': 'UniversalSompo',
}


class BaseMemberFormSet(BaseFormSet):
    def clean(self):
        # check that no two forms have same passport number
        passports = []
        for form in self.forms:
            passport = form.cleaned_data['passport_no']
            if passport in passports:
                raise forms.ValidationError("Passport Number should be unique for each traveller")
            passports.append(passport)


class MainProposalForm(object):

    member_formset = None
    address_form = None

    def __init__(self, *args, **kwargs):
        transaction = Transaction.objects.get(id=kwargs['transaction_id'])
        member_length = len(transaction.plan_details['members'])

        request = kwargs['request']

        member_form_class = getattr(this_module, "%sMemberForm" % (INSURER_SLUG_MAPPING[transaction.insurer_slug]))
        address_form_class = getattr(this_module, "%sAddressForm" % (INSURER_SLUG_MAPPING[transaction.insurer_slug]))

        MemberFormSet = formset_factory(member_form_class, extra=member_length, formset=BaseMemberFormSet)

        self.member_formset = MemberFormSet(request.POST) if request.POST else MemberFormSet()
        self.address_form = address_form_class(request.POST) if request.POST else address_form_class()

        self.member_formset.validate_max = True
        self.member_formset.validate_min = True
        self.member_formset.max_num = member_length
        self.member_formset.min_num = member_length

        forms = self.member_formset.forms

        member_form_class.set_initials(forms)

        forms[0].fields['relationship'].initial = "self"
        forms[0].fields['mobile'].required = True
        forms[0].fields['email'].required = True

        if member_length == 1:
            forms[0].fields['relationship'].hidden = True
        else:
            forms[0].fields['relationship'].disabled = True

        form_data = request.POST if request.POST else self.converted_data(transaction.insured_details_json)
        self.prefill_form(form_data)

    def prefill_form(self, form_data):
        self.address_form.prefill(form_data)

        for form in self.member_formset.forms:
            form.prefill(form_data)

    # insured_details_json format is {'members': [], 'address': {}}
    # so this following method converts that format in the way form accepts data.
    def converted_data(self, json_data):
        data = json_data
        if json_data.get('members') is not None:
            for i, member in enumerate(json_data['members']):
                for key, value in member.items():
                    data["form-%s-%s" % (i, key)] = value
            data.pop('members')
        if json_data.get('address') is not None:
            data.update(json_data.pop('address'))
        return data

    def is_valid(self):
        valid_address = self.address_form.is_valid()
        valid_members = self.member_formset.is_valid()

        return valid_address and valid_members

    def get_form_layout(self):
        return {
                'member_form_layout': [f.get_form_layout() for f in self.member_formset],
                'address_form_layout': self.address_form.get_form_layout(),
                'non_form_errors': self.member_formset.non_form_errors(),
                }
