DATE_FORMAT = '%d-%b-%Y'
LMS_TRANSACTION_FLAGS = ['immediate_coverfox', 'immediate_insurer', 'delayed_ped', 'delayed']

LMS_TRANSACTION_SUCCESS_FLAGS = {
    ('reliance', 'ped'): LMS_TRANSACTION_FLAGS[0],
    ('reliance', 'noped'): LMS_TRANSACTION_FLAGS[0],

    ('religare', 'ped'): LMS_TRANSACTION_FLAGS[2],
    ('religare', 'noped'): LMS_TRANSACTION_FLAGS[0],

    # ('bharti', 'ped'): LMS_TRANSACTION_FLAGS[2], POLICY CANT BE ISSUED
    ('bharti-axa', 'noped'): LMS_TRANSACTION_FLAGS[0],

    # ('bajaj', 'ped'): LMS_TRANSACTION_FLAGS[2], POLICY CANT BE ISSUED
    ('bajaj-allianz', 'noped'): LMS_TRANSACTION_FLAGS[0],

    # ('star', 'ped'): LMS_TRANSACTION_FLAGS[2], POLICY CANT BE ISSUED
    ('star-health', 'noped'): LMS_TRANSACTION_FLAGS[0],

    # ('hdfc-ergo', 'ped'): LMS_TRANSACTION_FLAGS[2], POLICY CANT BE ISSUED
    ('hdfc-ergo', 'noped'): LMS_TRANSACTION_FLAGS[1],

    ('tata-aig', 'ped'): LMS_TRANSACTION_FLAGS[2],
    ('tata-aig', 'noped'): LMS_TRANSACTION_FLAGS[1],

    # ('iffco-tokio', 'ped'): LMS_TRANSACTION_FLAGS[2], POLICY CANT BE ISSUED
    ('iffco-tokio', 'noped'): LMS_TRANSACTION_FLAGS[3],

    ('universal-sompo', 'ped'): LMS_TRANSACTION_FLAGS[3],
    ('universal-sompo', 'noped'): LMS_TRANSACTION_FLAGS[3],
}
