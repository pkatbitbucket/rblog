from django.db.models.signals import post_save
from django.dispatch import receiver
from core import activity

from .models import Transaction as TravelTransaction
from .prod_utils import trigger_success_event


@receiver(post_save, sender=TravelTransaction)
def transaction_saved(sender, instance, **kwargs):
    if instance.status == 'MANUAL COMPLETED':
        trigger_success_event(instance)

    if instance.status in ['COMPLETED', 'MANUAL COMPLETED']:
        activity.payment_success(instance)
