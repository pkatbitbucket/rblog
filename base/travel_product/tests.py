"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase, Client
from django.test.client import RequestFactory
from travel_factory import *
import travel_product.insurers.settings as insurer_settings
from travel_product.views import QuoteFormView, BuyView, TransactionCreateView, initiate_transaction_processing
from django.core.urlresolvers import reverse
from django.conf import settings
import local_settings
from django.utils.importlib import import_module


class TravelQuoteGetTest(TestCase):
    """
    Test case for checking get and an get_ajax call for /travel-insurance/ url
    """

    def setUp(self):
        super(TravelQuoteGetTest, self).setUp()
        self.client = Client(enforce_csrf_checks=True)
        self.response = self.client.get(reverse("travel:quote:form"))
        self.ajax_response = self.client.get(reverse("travel:quote:form"), HTTP_X_REQUESTED_WITH='XMLHttpRequest')

    def test_get_ajax(self):
        self.assertEqual(self.ajax_response.status_code, 200, 'Failed to make a GET AJAX call')

    def test_get(self):
        self.assertEqual(self.response.status_code, 200, 'Failed to make a GET call')

    def test_valid_view_call(self):
        self.assertEqual(self.response.resolver_match.func.__name__, QuoteFormView.as_view().__name__,
                         'Url is not calling the desired view')

    def test_valid_view_ajax_call(self):
        self.assertEqual(self.ajax_response.resolver_match.func.__name__, QuoteFormView.as_view().__name__,
                         'Url is not calling the desired view')


class TravelQuotePostTest(TestCase):
    """
    Test case for checking post and an post_ajax call for /travel-insurance/#/results url
    """

    def setUp(self):
        super(TravelQuotePostTest, self).setUp()
        self.client = Client()
        self.response = self.client.post(reverse("travel:quote:form"))
        travel_quote = getattr(local_settings, 'TRAVEL_QUOTE')
        self.ajax_response = self.client.post(reverse("travel:quote:form"),
                                              travel_quote, HTTP_X_REQUESTED_WITH='XMLHttpRequest')

    def test_post_ajax(self):
        self.assertEqual(self.ajax_response.status_code, 200, 'Failed to make a POST AJAX call')

    def test_post(self):
        self.assertEqual(self.response.status_code, 200, 'Failed to make a POST call')

    def test_valid_view_call(self):
        self.assertEqual(self.response.resolver_match.func.__name__, QuoteFormView.as_view().__name__,
                         'Url is not calling the desired view')

    def test_valid_view_ajax_call(self):
        self.assertEqual(self.ajax_response.resolver_match.func.__name__, QuoteFormView.as_view().__name__,
                         'Url is not calling the desired view')


class CreateTravelTransactionPostTest(TestCase):
    """
    Test case to check /buy/add/ page
    """
    def setUp(self):
        super(CreateTravelTransactionPostTest, self).setUp()
        self.factory = RequestFactory()
        self.view_func = TransactionCreateView.as_view()
        settings.SESSION_ENGINE = 'django.contrib.sessions.backends.file'
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key
        self.slab = SlabFactory.create()
        self.slab.save()
        self.slab_id = self.slab.id
        self.tracker = TrackerFactory.create()
        self.tracker.save()
        self.activity = ActivityFactory.create(tracker=self.tracker)
        self.activity.save()
        test_response = self.client.request()
        self.csrftoken = self.client.cookies['csrftoken'].value

    def test_post_api(self):

        request = self.factory.post(reverse("travel:transaction:add"),
                                    {u'slab': [self.slab_id], 'csrfmiddlewaretoken': self.csrftoken},
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        request.session = self.session
        request.session.modified = False
        request.TRACKER = self.tracker
        request.session['activity'] = self.activity
        response = self.view_func(request, {u'slab': [self.slab_id], 'csrfmiddlewaretoken': self.csrftoken},
                                  HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200, 'Failed to make a post call')

    def test_valid_view(self):
        self.client = Client(enforce_csrf_checks=True)
        response = self.client.post(reverse('travel:transaction:add'),
                                    {u'slab': [self.slab_id], 'csrfmiddlewaretoken': self.csrftoken},
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.resolver_match.func.__name__, TransactionCreateView.as_view().__name__,
                         'Url is not calling the desired view')


class TravelBuyTest(TestCase):
    """
    Test case to check the buy/<transaction_id> page
    """

    def setUp(self):
        super(TravelBuyTest, self).setUp()
        self.client = Client(enforce_csrf_checks=True)
        self.slab = SlabFactory.create()
        self.slab.save()
        self.tracker = TrackerFactory.create()
        self.tracker.save()
        self.activity = ActivityFactory.create(tracker=self.tracker)
        self.activity.save()
        self.transaction = TransactionFactory.create(activity=self.activity, slab=self.slab)
        self.transaction.save()
        self.transaction_id = self.transaction.transaction_id
        test_response = self.client.request()
        self.csrftoken = self.client.cookies['csrftoken'].value
        self.response = self.client.get(reverse("travel:transaction:buy", args=[self.transaction_id]),
                                        {'csrfmiddlewaretoken': self.csrftoken})

    def test_get_api(self):
        self.assertEqual(self.response.status_code, 200, 'Failed to make a get call')

    def test_valid_view_call(self):
        self.assertEqual(self.response.resolver_match.func.__name__, BuyView.as_view().__name__,
                         'Url is not calling the desired view')

    def test_template_used(self):
        self.assertTemplateUsed(response=self.response, template_name='travel_product:knockout-buy.html',
                                msg_prefix='Invalid template used to render response')

    def test_context_data(self):
        expected_context_data = {'transaction', 'form_details', 'insured_details', 'plan_details', 'quote_details'}
        if not expected_context_data.issubset(self.response.context_data.keys()):
            self.assertTrue(False, 'Some parameters are missing in context data')


class TravelBuyerDetailsPostTest(TestCase):
    """
    Test case to check the /final/  and to-process/ url
    """

    def setUp(self):
        super(TravelBuyerDetailsPostTest, self).setUp()
        self.client = Client(enforce_csrf_checks=True)
        test_response = self.client.request()
        self.csrftoken = self.client.cookies['csrftoken'].value
        buyer_details = getattr(local_settings, 'BUYER_DETAILS')
        self.post_data = buyer_details
        self.post_data.update({u'csrfmiddlewaretoken': self.csrftoken})
        self.slab = SlabFactory.create()
        self.slab.save()
        self.tracker = TrackerFactory.create()
        self.tracker.save()
        self.activity = ActivityFactory.create(tracker=self.tracker)
        self.activity.save()
        self.transaction = TransactionFactory.create(activity=self.activity, slab=self.slab)
        self.transaction.save()
        self.transaction_id = self.transaction.transaction_id

    def test_get_ajax(self):
        response = self.client.get(reverse("travel:initiate_transaction_processing", args=[self.transaction_id]),
                                   data=self.post_data)
        if response.status_code != 200:
            insurer = insurer_settings.INSURER_SLUG_MAP[self.transaction.insurer_slug]
            ins_module = import_module('travel_product.insurers.%s.integration' % insurer)
            is_redirect, redirect_url, template_name, data = ins_module.post_transaction(self.transaction)
            self.assertTrue(False, data.get('error_message'))
        else:
            self.assertEqual(response.status_code, 200, 'Failed to make a get call')

    def test_valid_get_view(self):
        response = self.client.get(reverse("travel:initiate_transaction_processing", args=[self.transaction_id]),
                                   data=self.post_data)
        self.assertEqual(response.resolver_match.func, initiate_transaction_processing,
                         'View is not calling the desired url')

    def test_post_ajax(self):
        response = self.client.post(reverse("travel:transaction_final", args=[self.transaction_id]),
                                    data=self.post_data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200, 'Failed to make a post ajax call')

    def test_request_method_call(self):
        response = self.client.post(reverse("travel:initiate_transaction_processing", args=[self.transaction_id]),
                                   data=self.post_data)
        self.assertEqual(response.status_code, 405, 'initiate_transaction_processing View is not checking for request method')

