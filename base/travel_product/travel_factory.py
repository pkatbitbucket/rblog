import factory
from .models import Slab, Plan, Tracker, Activity, Transaction,Place
import datetime
import random
import string
import uuid
import local_settings


class SlabFactory(factory.Factory):
    class Meta:
        model = Slab

    age_min = 20
    age_max = 150
    premium = 1530.0
    source = getattr(local_settings, 'SLAB_DATA')
    active = True
    plan = Plan.objects.filter(insurer__name__icontains=getattr(local_settings, 'INSURER_NAME'), type='single')[0]


class TrackerFactory(factory.Factory):
    class Meta:
        model = Tracker

    user = None
    extra = {}
    created = datetime.datetime.now()
    session_key = r''.join(random.choice(string.ascii_lowercase) for i in range(10))


class PlaceFactory(factory.Factory):
    class Meta:
        model = Place
    name = factory.Sequence(lambda n: 'WorldWide{0}'.format(n))
    slug = factory.Sequence(lambda n: 'worldwide{0}'.format(n))


class ActivityFactory(factory.Factory):
    class Meta:
        model = Activity

    id = uuid.uuid4()
    tracker = factory.SubFactory(TrackerFactory)
    data = {'quote_form':
                {'cleaned_data': {'max_age': 23, 'resident_for': None, 'declined_risks': [],
                                  'places': [PlaceFactory.create(), PlaceFactory.create()],
                                  '_sum_assured': 500000, 'slab': 'individual', 'places_excluded': [],
                                  'ages': [23], 'sum_assured': 500000, 'from_date': datetime.date(2015, 9, 30),
                                  'to_date': datetime.date(2015, 10, 14), 'duration': 15, 'type': u'single'},
                 'data': {u'isResident': [u'1'], u'adults': [u'23'], u'places': [u'usa-united-states-of-america'],
                          u'sum_assured': [u'500000'], u'from_date': [u'2015-09-30'], u'to_date': [u'2015-10-14'],
                          u'type': [u'single']}}}


class TransactionFactory(factory.Factory):
    class Meta:
        model = Transaction

    id = uuid.uuid4()
    activity = factory.SubFactory(Activity)
    slab = factory.SubFactory(Slab)
    data = getattr(local_settings, 'TRANSACTION_DATA')
    transaction_id = None
    status = 'PENDING'
    status_history = ()
    email = None
    first_name = 'Test'
    last_name = 'Case'
    mobile = 0000000000