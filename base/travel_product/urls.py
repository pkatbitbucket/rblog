from django.conf.urls import include, url

from . import views

uuid_regex = '(%(hex)s{8}-%(hex)s{4}-%(hex)s{4}-%(hex)s{4}-%(hex)s{12})|(%(hex)s{32})' % {'hex': '[a-f0-9]'}

quote_urlpatterns = [
    url(r"^form/$", views.QuoteFormView.as_view(), name='form'),
    url(r"^result/$", views.SlabListView.as_view(), name='result'),
    url(r"^result/(?P<activity_id>%s)/$" % (uuid_regex,), views.SlabListView.as_view(), name='result'),
]

transaction_urlpatterns = [
    url(r'^$', views.TransactionListView.as_view(), name='home'),
    url(r'^add/$', views.TransactionCreateView.as_view(), name='add'),
    url(r'^(?P<transaction_id>%s)/$' % (uuid_regex,), views.BuyFormView.as_view(), name='buy'),
]

urlpatterns = [
    url(r'^', include(quote_urlpatterns, namespace='quote')),
    url(r'^buy/', include(transaction_urlpatterns, namespace='transaction')),

    #  uat urls
    url(r'^insurer/(?P<insurer_slug>[\w-]+)/$', views.search_form, name='search_form'),
    url(r'^insurer/(?P<insurer_slug>[\w-]+)/save-details/$', views.save_details, name='save_details'),
    url(r'^response/(?P<insurer_slug>[\w-]+)/(transaction/(?P<transaction_id>%s))?/$' % (uuid_regex,),
        views.payment_response, name='payment_response'),
    # TODO: remove before production (change position of response to as above)
    url(r'^(?P<insurer_slug>[\w-]+)/(transaction/(?P<transaction_id>%s)/)?response/$' % (uuid_regex,),
        views.payment_response, name='payment_response'),
    url(r'^insurer/(?P<insurer_slug>[\w-]+)/(transaction/(?P<transaction_id>%s)/)?response/$' % (uuid_regex,),
        views.payment_response, name='payment_response'),
    url(r'^insurer/(?P<insurer_slug>[\w-]+)/response/$', views.payment_response, name='payment_response'),
    url(r'^transaction/(?P<transaction_id>%s)/final/$' % (uuid_regex,),
        views.transaction_final, name='transaction_final'),
    url(r'^transaction/(?P<transaction_id>%s)/to-process/$' % (uuid_regex,), views.initiate_transaction_processing,
        name='initiate_transaction_processing'),
    url(r'^transaction/(?P<transaction_id>%s)/qa-to-process/$' % (uuid_regex, ), views.qa_make_payment,
        name='qa_make_payment'),
    url(r'^transaction/(?P<transaction_id>%s)/success/$' % (uuid_regex,),
        views.transaction_success, name='transaction_success'),
    url(r'^transaction/(?P<transaction_id>%s)/failure/$' % (uuid_regex,),
        views.transaction_failure, name='transaction_failure'),
    url(r'^get-state-city/(?P<pincode>[\w-]+)$', views.get_state_and_city, name='get_state_and_city'),
    url(r'^get-city-list$', views.get_city_list, name='get_city_list'),
    url(r'^send-to-lms/(?P<transaction_id>%s)/$' % uuid_regex, views.send_to_lms, name='send_to_lms'),
]
