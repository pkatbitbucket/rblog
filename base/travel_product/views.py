from django.db.models.expressions import RawSQL
from django.forms import models as model_forms
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotAllowed
from django.template import RequestContext
from django.shortcuts import render_to_response, render
from django.views.decorators.csrf import csrf_exempt
from django import http
from django.conf import settings
from django.contrib.humanize.templatetags.humanize import intcomma
from django.db.models import Q, Count, Max
from django.views.generic import ListView, DetailView
from django.views.generic.edit import FormView, CreateView
from django.views.generic.detail import SingleObjectMixin

from importlib import import_module

from travel_product.models import Activity, Transaction
import travel_product.insurers.settings as insurer_settings
from utils import LMSUtils
from wiki.models import Pincode, City
from leads.views import save_call_time
import json
import putils
from django.views.decorators.http import require_GET
from django.forms.formsets import formset_factory
from travel_product import prod_utils
from travel_product.forms import TravelQuoteFDForm
from core import activity as core_activity


from copy import deepcopy
from core.exceptions import InvalidFormData, ReadOnlyException, NewTrackerException
from braces import views as braces_views
from .forms import QuoteForm
from .models import *  # NOQA
from coverfox.serializers import DjangoQuerysetJSONEncoder

import utils
import datetime


def search_form(request, insurer_slug):
    insurer = insurer_settings.INSURER_SLUG_MAP[insurer_slug]
    template_name = 'travel_product:parent_search.html'
    template_data = {
            'insurer': insurer,
            'insurer_slug': insurer_slug,
            'sub_template': '%s_search.html' % insurer
            }
    return render_to_response(template_name, template_data, context_instance=RequestContext(request))


def save_details(request, insurer_slug):
    post_data = dict(request.POST.items())
    tracker = request.TRACKER
    try:
        activity_id = post_data.pop('activity_id')
        activity = Activity.objects.get(id=activity_id)
    except KeyError:
        activity, created = Activity.objects.get_or_create(tracker=request.TRACKER)

    tracker.extra['activity'] = tracker.extra.get('activity', {})
    tracker.extra['activity']['search_data'] = post_data
    tracker.extra['activity']['insurer_slug'] = insurer_slug
    tracker.save()
    if request.is_ajax():
        return HttpResponse(json.dumps({'activity_id': activity.id}))
    else:
        return HttpResponseRedirect('/travel-insurance/results/%s/' % activity.id)


def results(request, activity_id):
    activity = Activity.objects.get(id=activity_id)
    insurer_slug = activity.tracker.extra['activity']['insurer_slug']
    insurer = insurer_settings.INSURER_SLUG_MAP[insurer_slug]
    ins_module = import_module('travel_product.insurers.%s.integration' % insurer)

    plans, extra_data = ins_module.get_quote(activity)
    template_name = 'travel_product:results.html'
    template_data = {
            'search_data': json.dumps(activity.tracker.extra['activity']['search_data']),
            'insurer_slug': insurer_slug,
            'plans': plans,
            'extra_data': extra_data,
            'sub_template': '%s_search.html' % insurer,
            'activity_id': activity_id,
            }
    return render_to_response(template_name, template_data, context_instance=RequestContext(request))


def create_transaction(request):

    post_data = dict(request.POST.items())
    insurer_slug = post_data.pop('insurer_slug')

    transaction = Transaction(
        tracker=request.TRACKER,
        status=Transaction.DRAFT,
        insurer_slug=insurer_slug,
    )
    transaction.extra['search_data'] = json.loads(post_data.pop('search_data'))
    transaction.plan_details = post_data

    insurer = insurer_settings.INSURER_SLUG_MAP[insurer_slug]
    ins_module = import_module('travel_product.insurers.%s.integration' % insurer)
    ins_module.pre_transaction(transaction)
    transaction.save()
    if request.is_ajax():
        return HttpResponse(json.dumps({'transaction_id': transaction.transaction_id}))
    else:
        return HttpResponseRedirect('/travel-insurance/buy/%s' % transaction.transaction_id)


def transaction_final(request, transaction_id):
    try:
        transaction = Transaction.objects.exclude(status=Transaction.COMPLETED).get(id=transaction_id)
    except Transaction.DoesNotExist:
        raise http.Http404("No %(verbose_name)s found matching the query" % {'verbose_name': Transaction._meta.verbose_name})

    transaction.status = Transaction.PENDING

    insurer = insurer_settings.INSURER_SLUG_MAP[transaction.insurer_slug]
    ins_module = import_module('travel_product.insurers.%s.integration' % insurer)
    finalize_transaction = getattr(ins_module,'finalize_transaction',None)
    if(finalize_transaction):
        finalize_transaction(transaction);

    transaction.save()
    if request.is_ajax():
        return HttpResponse(json.dumps({"success": True, "error": False}))
    else:
        return HttpResponseRedirect('/travel-insurance/transaction/%s/to-process/' % transaction_id)

@require_GET
def initiate_transaction_processing(request, transaction_id):
    try:
        transaction = Transaction.objects.exclude(status=Transaction.COMPLETED).get(id=transaction_id)
    except Transaction.DoesNotExist:
        raise http.Http404("No %(verbose_name)s found matching the query" % {'verbose_name': Transaction._meta.verbose_name})
    try:
        assert transaction.activity.tracker == request.TRACKER, 'Can not proceed, transaction originally created under different session'
    except:
        new_activity = transaction.activity
        new_activity.tracker = request.TRACKER
        new_activity.id = None
        new_activity.save()

        new_transaction = transaction
        new_transaction.id = None
        new_transaction.transaction_id = None
        new_transaction.activity = new_activity
        new_transaction.save()

        new_transaction.track_activity(
            core_activity.VIEWED, 'payment'
        )
        return initiate_transaction_processing(request, new_transaction.id)

    if transaction.status == Transaction.COMPLETED:
        raise ReadOnlyException()
    insurer = insurer_settings.INSURER_SLUG_MAP[transaction.insurer_slug]
    ins_module = import_module('travel_product.insurers.%s.integration' % insurer)

    is_redirect, redirect_url, template_name, data, proposal_req, proposal_resp = ins_module.post_transaction(transaction)

    if not settings.PRODUCTION:
        transaction.track_activity(
            core_activity.VIEWED, 'payment'
        )
        return render_to_response(
            'travel_product:qa.html',
            {'req': proposal_req, 'resp': proposal_resp, 'transaction_id': transaction_id},
            context_instance=RequestContext(request))

    if 'error_message' in data:
        prod_utils.trigger_lms_event("proposal_failed", data, transaction)
        prod_utils.send_failure_mail(transaction, 'Transaction', data)
        redirect_url = '/travel-insurance/transaction/%s/failure/' % transaction.transaction_id
        transaction.track_activity(
            core_activity.FAILED, 'payment'
        )
        return HttpResponseRedirect(redirect_url)
    if is_redirect:
        transaction.track_activity(
            core_activity.VIEWED, 'payment'
        )
        return HttpResponseRedirect(redirect_url)
    else:
        if not template_name or template_name == 'health_product/post_transaction_data.html':
            template_name = 'health_product/post_transaction_data.html'
            template_data = {
                'data' : data,
                'redirect_url' : redirect_url,
            }
        else:
            template_data = data

        transaction.track_activity(
            core_activity.VIEWED, 'payment'
        )
        return render_to_response(template_name, template_data, context_instance=RequestContext(request))


def qa_make_payment(request, transaction_id=None):
    transaction = Transaction.objects.get(id=transaction_id)
    insurer = insurer_settings.INSURER_SLUG_MAP[transaction.insurer_slug]
    ins_module = import_module('travel_product.insurers.%s.integration' % insurer)

    is_redirect, redirect_url, template_name, data, proposal_req, proposal_resp = ins_module.post_transaction(transaction)
    if 'error_message' in data:
        prod_utils.trigger_lms_event("proposal_failed", data, transaction)
        prod_utils.send_failure_mail(transaction, 'Transaction', data)
        redirect_url = '/travel-insurance/transaction/%s/failure/' % transaction.transaction_id
        return HttpResponseRedirect(redirect_url)
    if is_redirect:
        return HttpResponseRedirect(redirect_url)
    else:
        if not template_name or template_name == 'health_product/post_transaction_data.html':
            template_name = 'health_product/post_transaction_data.html'
            template_data = {
                'data' : data,
                'redirect_url' : redirect_url,
            }
        else:
            template_data = data
        return render_to_response(template_name, template_data, context_instance=RequestContext(request))


@csrf_exempt
def payment_response(request, insurer_slug, transaction_id=None):
    try:
        if transaction_id:
            transaction = transaction_id and Transaction.objects.exclude(status=Transaction.COMPLETED).get(id=transaction_id)
        else:
            transaction = None
    except Transaction.DoesNotExist:
        raise http.Http404("No %(verbose_name)s found matching the query" % {'verbose_name': Transaction._meta.verbose_name})
    insurer = insurer_settings.INSURER_SLUG_MAP[insurer_slug]
    ins_module = import_module('travel_product.insurers.%s.integration' % insurer)
    payment_response = dict(request.GET.items() + request.POST.items())
    transaction, success, data = ins_module.post_payment(payment_response, transaction)
    if success:
        # Send alert email to Travel Admins.
        prod_utils.send_success_mail(transaction, 'Payment')
        prod_utils.trigger_success_event(transaction)
        return HttpResponseRedirect('/travel-insurance/transaction/%s/success/' % transaction.transaction_id)
    else:
        # Send alert email to Travel Admins.
        prod_utils.send_failure_mail(transaction, 'Payment', data)
        prod_utils.trigger_lms_event("payment_failed", data, transaction)
        return HttpResponseRedirect('/travel-insurance/transaction/%s/failure/' % transaction.transaction_id)


def transaction_success(request, transaction_id):
    try:
        transaction = Transaction.objects.get(id=transaction_id)
    except Transaction.DoesNotExist:
        raise http.Http404("No %(verbose_name)s found matching the query" % {'verbose_name': Transaction._meta.verbose_name})

    transaction.open_requirement_if_deleted()
    transaction.track_activity(
        core_activity.SUCCESS, 'payment', requirement=transaction.requirement
    )
    # TODO: update template_data
    template_data = {
        'transaction': transaction,
        'insurer': transaction.slab.plan.insurer.slug,
        'plan_details': transaction.plan_details,
    }
    return render_to_response('travel_product:transaction_success.html', template_data, context_instance=RequestContext(request))


def transaction_failure(request, transaction_id):
    try:
        transaction = Transaction.objects.get(id=transaction_id)
    except Transaction.DoesNotExist:
        raise http.Http404("No %(verbose_name)s found matching the query" % {'verbose_name': Transaction._meta.verbose_name})
    insurer = insurer_settings.INSURER_SLUG_MAP[transaction.insurer_slug]

    transaction.track_activity(
        core_activity.FAILED, 'payment', requirement=transaction.requirement
    )
    # TODO: update template_data
    template_data = {
        'transaction': transaction,
        'insurer': insurer,
        'error_message': transaction.extra.get('error_message')
    }
    return render_to_response(
        'travel_product:transaction_failure.html',
        template_data,
        context_instance=RequestContext(request)
    )


def send_to_lms(request, transaction_id):
    if request.method == 'POST':
        data = json.loads(request.POST['data'])

        transaction = Transaction.objects.get(id=transaction_id)

        if transaction.status != Transaction.COMPLETED:
            transaction.extra['insured_details_json'].update(data)
            transaction.save()

        transaction.track_activity(
            core_activity.VIEWED, 'proposal'
        )
        http_response = save_call_time(request)
        return http_response
    else:
        return HttpResponseNotAllowed("Not Allowed")


def get_state_and_city(request, pincode):
    '''Return state and city as per pincode'''
    p = Pincode.objects.filter(pincode=pincode)
    if p:
        p = p[0]
        return HttpResponse(
                json.dumps({"error": False, "state": p.state.name, "city": p.city}),
            content_type="application/json"
        )
    else:
        return HttpResponse(
            json.dumps({"error": True}),
            content_type="application/json"
        )


def get_city_list(request):
    state = request.POST['state']
    try:
        city_list = City.objects.filter(state__name=state)
        cities = [{'key': c.name, 'value': c.name} for c in city_list]
        return HttpResponse(json.dumps({'error': False, 'city_list': cities}))
    except:
        return HttpResponse(json.dumps({'error': True}))


def buy(request, transaction_id):
    template_name = 'travel_product:knockout-buy.html'
    transaction = Transaction.objects.get(pk=transaction_id)
    ins_module = import_module('travel_product.proposal_forms2')
    member_form_class = getattr(ins_module, "%sIndividualMemberForm" % (
        ins_module.INSURER_SLUG_MAPPING[transaction.insurer_slug])
    )
    address_form_class_ = getattr(
        ins_module, "%sAddressForm" % (ins_module.INSURER_SLUG_MAPPING[transaction.insurer_slug])
    )

    member_form_set = formset_factory(member_form_class, extra=2)

    member_formset = member_form_set(request.POST) if request.POST else member_form_set()
    address_form = address_form_class_(request.POST)

    if member_formset.is_valid() and address_form.is_valid():
        # do something
        return HttpResponseRedirect(reverse('travel:transaction_final', args=(transaction_id, )))

    return render(
        request,
        template_name,
        {'member_formset': member_formset, 'address_form': address_form, 'transaction': transaction}
    )


error_messages = ERRORS = utils.to_namedtuple({
    'INVALID_ACTIVITY_ID': 'Invalid search data.',
    })


class BaseAbstractView(braces_views.JSONResponseMixin, braces_views.AjaxResponseMixin):
    json_encoder_class = DjangoQuerysetJSONEncoder
    activity_model = Activity
    default_serialized_response = {
        'errors': {},
        'data': {},
        'redirect_url': '',
    }
    serialized_data_key_name = 'serialized_response'

    @property
    def activity_id(self):
        return self.get_activity().id

    def get_activity(self, new=False, data={}):
        activity_id = self.kwargs.get('activity_id')
        activity = self.request.session.get('activity')
        if new:
            activity = self.activity_model.objects.create(tracker=self.request.TRACKER, data=data)
            self.request.session.update({'activity': activity})
            self.request.session.modified = True
        elif activity_id and not(activity and activity_id == str(activity.id)):
            try:
                activity = self.activity_model.objects.get(id=activity_id)
                if self.request.TRACKER == activity.tracker:
                    self.request.session.update({'activity': activity})
                    self.request.session.modified = True
                else:
                    raise NewTrackerException(self.get_activity(new=True, data=activity.data))
            except self.activity_model.DoesNotExist:
                raise
        activity = self.request.session.get('activity') or self.get_activity(new=True)
        return activity

    def get_response(self, view, method, **kwargs):
        _kwargs = deepcopy(self.kwargs)
        _kwargs.update(kwargs)
        original_method = self.request.method
        self.request.method = method.upper()
        response = view(request=self.request, **_kwargs)
        self.request.method = original_method
        return response

    def get_response_with_get(self, view, **kwargs):
        return self.get_response(view, 'GET', **kwargs)

    def get_response_with_post(self, view, **kwargs):
        return self.get_response(view, 'POST', **kwargs)

    def get_ajax(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)

    def post_ajax(self, request, *args, **kwargs):
        if hasattr(self, 'form_valid_ajax'):
            self.form_valid = self.form_valid_ajax
        if hasattr(self, 'form_invalid_ajax'):
            self.form_invalid = self.form_invalid_ajax
        return self.post(request, *args, **kwargs)

    def to_nested_dict(self, dictionary):
        def to_dict(keys, value, dictionary):
            try:
                key = keys.pop(0)
                dictionary[key] = dictionary.get(key, {}) if keys else value
                to_dict(keys, value, dictionary[key])
            except:
                pass

        new_dictionary = {}
        for key, value in dictionary.items():
            to_dict(key.split("__"), value, new_dictionary)
        return new_dictionary

    def get_serialized_quote_form(self, initial={}):
        fields = {}
        form = {
            'fields': fields,
            'initial': initial,
        }
        fields['type'] = QuoteForm.base_fields['type'].choices
        fields['sum_assured'] = QuoteForm.SUM_ASSURED_CHOICES
        declined_risks = list(QuoteForm.base_fields['declined_risks'].queryset.values('malady', 'insurer__slug', 'slug'))
        fields['declined_risks'] = {}
        for declined_risk in declined_risks:
            fields['declined_risks'][declined_risk['slug']] = fields['declined_risks'].get(declined_risk['slug'], {
                'malady': declined_risk['malady'],
                'insurers': []
            })
            fields['declined_risks'][declined_risk['slug']]['insurers'].append(declined_risk['insurer__slug'])
        return form

    def push_results_data_to_lms(self):
        activity = self.get_activity()
        request = self.request
        data = activity.data
        data.update({
            'activity_id': activity.id,
            'quote_url':  request.build_absolute_uri(activity.get_absolute_url()),
            'tracker_id': activity.tracker.session_key,
        })
        return LMSUtils.send_lms_event(
            event='result_page',
            product_type='travel',
            tracker=activity.tracker,
            data=data,
            email=request.COOKIES.get('email'),
            mobile=request.COOKIES.get('mobileNo'),
            internal=request.IS_INTERNAL,
            request_ip=putils.get_request_ip(request)
        )


class QuoteFormView(BaseAbstractView, FormView):
    form_class = QuoteForm

    def get_template_names(self):
        if self.request.user_agent.is_mobile:
            return "flatpages:flatpages/travel-insurance.html"
        else:
            return "travel_product/desktop.html"

    def initiate_activity(self, form):
        activity = self.get_activity(new=True)
        activity.set_quote_data(form.data)
        filtered_data_dict = {}
        for key, val in form.data.iteritems():
            if isinstance(val, list):
                filtered_data_dict[key] = ','.join(val)
            else:
                filtered_data_dict[key] = val

        # Prepare traveller age.
        ages = filtered_data_dict.get("adults", "")
        if "," in ages:
            ages = ages.split(",")

        if isinstance(ages, str) or isinstance(ages, unicode):
            filtered_data_dict["traveller1_age"] = ages
            filtered_data_dict["traveller_count"] = 1
        elif isinstance(ages, list):
            i = 1
            for age in ages:
                filtered_data_dict["traveller%s_age" % i] = age
                i += 1

            filtered_data_dict["traveller_count"] = len(ages)

        filtered_data_dict["trip_start_place_india"] = "true"

        travel_quote_instance = TravelQuoteFDForm(data=filtered_data_dict)
        req_activity = core_activity.create_activity(
            core_activity.VIEWED,
            'travel',
            'quotes',
            '',
            '',
            '',
            data=travel_quote_instance.get_fd()
        )

        if req_activity:
            if req_activity.requirement:
                req = req_activity.requirement
                req.extra = {'activity_id': activity.id, 'site_url': settings.SITE_URL}
                req.save(update_fields=['extra'])
        return activity

    def get_context_data(self, **kwargs):
        context_data = super(self.__class__, self).get_context_data(**kwargs)
        context_data[self.serialized_data_key_name] = serialized_response = deepcopy(self.default_serialized_response)
        serialized_response['data']['form'] = self.get_serialized_quote_form(initial=kwargs['form'].initial)
        return context_data

    def get_ajax(self, request, *args, **kwargs):
        response = super(self.__class__, self).get_ajax(request, *args, **kwargs)
        return self.render_json_response(response.context_data[self.serialized_data_key_name])

    def form_valid(self, form):
        activity = self.initiate_activity(form)
        self.success_url = activity.get_absolute_url()
        return super(self.__class__, self).form_valid(form)

    def form_invalid_ajax(self, form):
        serialized_response = deepcopy(self.default_serialized_response)
        serialized_response['data']['errors'] = form.errors
        return self.render_json_response(serialized_response, status=400)

    def form_valid_ajax(self, form):
        activity = self.initiate_activity(form)
        serialized_response = deepcopy(self.default_serialized_response)
        serialized_response['redirect_url'] = activity.get_absolute_url()
        serialized_response['data']['slab_list_view'] = json.loads(self.get_response_with_get(SlabListView.as_view()).content)
        return self.render_json_response(serialized_response, status=200)


class SlabListView(BaseAbstractView, ListView):
    model = Slab
    queryset = model.objects.all()
    ordering = 'premium'
    serialized_fields = [
            'premium',
            'premium_stx',
            'plan__name',
            'plan__insurer__name',
            'plan__insurer__slug',
            'plan__insurer__declined_risks',
            'plan__sum_assured',
            'plan__currency',
            'plan__policy_wording__file',
            'sub_limit',
            'pk',
            ]
    serialized_fields += ['plan__verbose_name'] if settings.DEBUG else []

    feature_serialized_fields = {
            'name__name',
            'cover',
            'deductible',
            'remarks',
    }

    def get_context_data(self, **kwargs):
        context_data = super(self.__class__, self).get_context_data(**kwargs)
        context_data[self.serialized_data_key_name] = serialized_response = deepcopy(self.default_serialized_response)
        serialized_response['data']['form'] = self.get_serialized_quote_form(initial=self.cleaned_data)
        return context_data

    def get(self, request, *args, **kwargs):
        try:
            # self.push_results_data_to_lms()
            return super(self.__class__, self).get(request, *args, **kwargs)
        except self.activity_model.DoesNotExist:
            return http.HttpResponseRedirect(reverse('travel:quote:form'))
        except InvalidFormData as (form_errors, ):
            return http.HttpResponseRedirect(reverse('travel:quote:form'))
        except NewTrackerException as (activity, ):
            return http.HttpResponseRedirect(activity.get_absolute_url())

    def get_ajax(self, request, *args, **kwargs):
        try:
            # Try and implement get_context_data_ajax
            serialized_response = deepcopy(self.default_serialized_response)
            response = super(self.__class__, self).get(request, *args, **kwargs)
            slab_list = response.context_data['slab_list']
            serialized_response['data']['slab_list'] = list(slab_list.values(*self.serialized_fields))
            status = 200
            serialized_response['data']['slab_list'] = map(
                lambda d: self.to_nested_dict(d), serialized_response['data']['slab_list']
            )
            serialized_response['data']['form'] = response.context_data[self.serialized_data_key_name]['data']['form']
            for slab_dict in serialized_response['data']['slab_list']:
                slab = slab_list.get(pk=slab_dict.get('pk'))
                features = list(slab.get_features(slab=self.cleaned_data['slab']).values(*self.feature_serialized_fields))
                for feature in features:
                    feature['cover'] = feature['cover'] or 'Not Covered'
                    feature['deductible'] = feature['deductible'] or '0'
                    if feature['cover'][:1] == u'\x80':
                        feature['cover'] = u"\u20AC" + feature['cover'][1:]
                    if feature['deductible'][:1] == u'\x80':
                        feature['deductible'] = u"\u20AC" + feature['deductible'][1:]
                slab_dict['features'] = map(
                    lambda d: self.to_nested_dict(d), map(lambda f: f.update({'name': f.pop('name__name')}) or f, features)
                )
                slab_dict['premium'] = intcomma(int(slab_dict['premium_stx']))
                if settings.DEBUG:
                    slab_dict['plan']['name'] = "%d - %s - %s" % (
                        slab_dict['pk'], slab_dict['plan']['name'], slab_dict['plan'].pop('verbose_name'),
                    )
            # self.push_results_data_to_lms()
        except self.activity_model.DoesNotExist:
            serialized_response['errors']['__all__'] = ERRORS.INVALID_ACTIVITY_ID
            status = 400
            serialized_response['redirect_url'] = reverse('travel:quote:form')
        except InvalidFormData as (form_errors, ):
            serialized_response['errors'] = form_errors
            status = 410
            serialized_response['redirect_url'] = reverse('travel:quote:form')
        except NewTrackerException as (activity, ):
            status = 302
            serialized_response['redirect_url'] = activity.get_absolute_url()
        return self.render_json_response(serialized_response, status=status)

    def get_initial_sum_assured_range(self, sum_assured):
        try:
            return map(lambda v: v if not v else v*1000, QuoteForm.SUM_ASSURED_RANGES[sum_assured/1000])
        except KeyError:
            return [None, None]

    # Getting all plans for all places selected
    # A plan is a valid match if it covers Europe but either Europe or Germany or Belgium is selected
    def get_queryset(self):
        try:
            activity = self.get_activity()
        except self.activity_model.DoesNotExist:
            raise
        slabs = super(self.__class__, self).get_queryset()
        form_data, cd = activity.get_quote_data()
        self.cleaned_data = cd

        # Get plans that cover all the places user entered (explains the use of `&`))
        # and don't have any those places in their excluded list either (explains the use of `|`)
        plan_ids = Place.get_plans(
            places_included=cd['places'], places_excluded=cd['places_excluded']
        ).values_list('pk', flat=True)

        resident_for = cd['resident_for']
        nri = resident_for is not None
        filters = utils.clean_dict({
            'plan__insurer__nri': nri or None,
            'plan__insurer__minimum_residency__lte': resident_for if nri else None,
            # 'plan__insurer__minimum_processing_days__lte': (
            #     cd['from_date'] - datetime.date.today()
            # ).days if cd['declined_risks'].count() else None,
            '%sslab__isnull' % (cd['slab'], ): False,
            'age_min__lte': cd['max_age'],
            'age_max__gte': cd['max_age'],
            '%sslab__duration' % (cd['slab'], ): cd['duration'],
            'plan__type': cd['type'],
        })
        slabs = (slabs
                 .filter(plan_id__in=plan_ids, **filters)
                 .annotate(plan__insurer__declined_risks=Count('plan__insurer__declined_risks'))
                 .annotate(premium_stx=RawSQL('ROUND(((premium/100)*' + str(settings.SERVICE_TAX_PERCENTAGE) + ')+premium)', ())))

        # apply insurer specific filter rules
        slabs = slabs.apply_filter_rules(cd)

        # Excluding expensive plan siblings(plans with same features[hash])
        features_hash_dict = {}
        for features_hash, insurer, pk, premium, sum_assured in slabs.values_list(
            'plan__features_hash', 'plan__insurer', 'pk', 'premium', 'plan__sum_assured'
        ):
            if (
                not features_hash_dict.get((features_hash, insurer, sum_assured)) or
                features_hash_dict.get((features_hash, insurer, sum_assured))[1] > premium
            ):
                features_hash_dict[(features_hash, insurer, sum_assured)] = (pk, premium)
        slabs = slabs.filter(pk__in=map(lambda s: s[0], features_hash_dict.values())).distinct()

        sum_assured_range = [None, None]
        if slabs:
            if not cd['sum_assured']:
                cd['sum_assured'] = max_count_sum_assured = slabs.values_list('plan__sum_assured').annotate(
                    sum_assured_count=Count('plan__sum_assured')
                ).order_by('-sum_assured_count').first()[0]
                max_count_sum_assured_by_1000 = max_count_sum_assured/1000
                sum_assured_choices = sorted(set(QuoteForm.SUM_ASSURED_RANGES.keys()))
                sum_assured_range_min, sum_assured_range_max = 0, 0
                for sum_assured_choice in sum_assured_choices:
                    sum_assured_range_min = sum_assured_choice if sum_assured_choice <= max_count_sum_assured_by_1000 else sum_assured_range_min
                    sum_assured_range_max = sum_assured_choice if not sum_assured_range_max and sum_assured_choice > max_count_sum_assured_by_1000 else sum_assured_range_max
                    if sum_assured_range_max:
                        break
                sum_assured_range = [sum_assured_range_min*1000 or None, sum_assured_range_max*1000 or None]
            else:
                sum_assured_range = self.get_initial_sum_assured_range(sum_assured=cd['sum_assured'])

        # Filter on sum assured
        slabs = slabs.filter(**utils.clean_dict({
            'plan__sum_assured__lt': sum_assured_range[1],
            'plan__sum_assured__gte': sum_assured_range[0],
        }))

        # Getting highest level of geography per insurer among selected plans
        geographic_levels_per_insurer = (Slab.objects
                                             .filter(pk__in=slabs)
                                             .values('plan__insurer__slug')
                                             .annotate(Max('plan__places_included__level'))
                                             .values_list('plan__places_included__level__max', 'plan__insurer__slug')
                                         )
        # Excluding plans that don't belong to the highest geographic level for an insurer
        # Effectively reducing result set to one or two plans per insurer
        # (or however many different plans does an insurer have per geography)
        filters = Q()
        for level, insurer in geographic_levels_per_insurer:
            filters |= Q(plan__insurer__slug=insurer, plan__places_included__level=level)
        return slabs.filter(filters).apply_annotation_rules()



from travel_product.proposal_forms import MainProposalForm

class BuyFormView(BaseAbstractView, SingleObjectMixin, FormView):
    model = Transaction
    template_name = 'travel_product:knockout-buy.html'
    form_class = MainProposalForm
    pk_url_kwarg = 'transaction_id'

    def get_form_kwargs(self):
        form_kwargs = super(FormView, self).get_form_kwargs()
        form_kwargs.update({
            'transaction_id': self.kwargs['transaction_id'],
            'request': self.request,
            })
        return form_kwargs

    def get_success_url(self):
        return reverse('travel:transaction_final', args=(self.object.transaction_id, ))

    def get_quote_details(self, transaction):
        qdata = transaction.activity.cleaned_data
        return {
            'ages': qdata['ages'],
            'travel_date': qdata['from_date'].strftime('%d/%m/%Y'),
            'plan_name': transaction.slab.plan.name,
            'sum_assured': qdata['sum_assured'],
            'plan_type': qdata['type'],
            'plan_grade': transaction.slab.plan.grade.name,
            'places': ", ".join([p.name for p in qdata['places']]),
            'is_nri': True if 'resident_for' in qdata and qdata['resident_for'] else False,
        }

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        transaction = context['object']

        insurer = insurer_settings.INSURER_SLUG_MAP[self.object.insurer_slug]
        ins_module = import_module('travel_product.insurers.%s.buy_form' % insurer)
        insurer_form_data = ins_module.get_form(self.request, transaction)

        context.update({
            'transaction': transaction,
            'insured_details': transaction.insured_details_json,
            'form_details': insurer_form_data,
            'quote_details': self.get_quote_details(transaction),
            'plan_details': transaction.plan_details,
            'management_form': self.get_form().member_formset.management_form.as_p(),
            'form_layout': self.get_form().get_form_layout(),
            'member_length': len(transaction.plan_details['members']),
        })

        return context

    def save_transaction(self, form):
        # update transaction insured_details_json here and save transaction.
        form_data = {
            'members': [f.cleaned_data for f in form.member_formset.forms],
            'address': form.address_form.cleaned_data,
        }
        self.object.extra['insured_details_json'] = form_data
        self.object.save()

        self.object.track_activity(
            core_activity.VIEWED, 'proposal'
        )

    def form_valid(self, form):
        print "==== VALID FORM ====="
        self.save_transaction(form)
        return super(BuyFormView, self).form_valid(form)

    def form_invalid(self, form):
        print "==== INVALID FORM ======"
        print form.address_form.errors
        print form.member_formset.errors
        self.save_transaction(form)
        return super(BuyFormView, self).form_invalid(form)

    def get(self, request, *args, **kwargs):
        try:
            self.object = self.get_object()
            response = super(self.__class__, self).get(request, *args, **kwargs)
            return response
        except:
            return HttpResponseRedirect(reverse("travel:quote:result", args=(self.object.activity_id, )))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()

        return super(BuyFormView, self).post(request, *args, **kwargs)


class BuyView(BaseAbstractView, DetailView):
    model = Transaction
    template_name = 'travel_product:knockout-buy.html'
    pk_url_kwarg = 'transaction_id'

    def send_data_to_lms(self, data, transaction):
        lms_data = data.copy()
        for key in ['salutations_list', 'states_list', 'relations_list', 'cities_list', 'occupations_list', 'gender_list', 'nominee_relations_list']:
            lms_data.pop(key, None)
        lms_data.update({
            'tracker_id': transaction.activity.tracker.session_key,
            'transaction_id': transaction.pk,
            'transaction_url': self.request.build_absolute_uri(),
        })
        LMSUtils.send_lms_event(
            event='buy_page',
            product_type='travel',
            tracker=transaction.activity.tracker,
            data=lms_data,
            email=self.request.COOKIES.get('email'),
            mobile=self.request.COOKIES.get('mobileNo'),
            internal=self.request.IS_INTERNAL,
            request_ip=putils.get_request_ip(self.request),
        )

    def get_quote_details(self, transaction):
        qdata = transaction.activity.cleaned_data
        return {
            'ages': qdata['ages'],
            'travel_date': qdata['from_date'].strftime('%d/%m/%Y'),
            'plan_name': transaction.slab.plan.name,
            'sum_assured': qdata['sum_assured'],
            'plan_type': qdata['type'],
            'plan_grade': transaction.slab.plan.grade.name,
            'places': ", ".join([p.name for p in qdata['places']]),
            'is_nri': True if 'resident_for' in qdata and qdata['resident_for'] else False,
        }

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        transaction = context['object']

        insurer = insurer_settings.INSURER_SLUG_MAP[self.object.insurer_slug]
        ins_module = import_module('travel_product.insurers.%s.buy_form' % insurer)
        insurer_form_data = ins_module.get_form(self.request, transaction)


        context.update({
            'transaction': transaction,
            'insured_details': transaction.insured_details_json,
            'form_details': insurer_form_data,
            'quote_details': self.get_quote_details(transaction),
            'plan_details': transaction.plan_details,
        })

        # self.send_data_to_lms(insurer_form_data, transaction)
        return context

    def get(self, request, *args, **kwargs):
        try:
            return super(self.__class__,  self).get(request, *args, **kwargs)
        except InvalidFormData:
            return HttpResponseRedirect(reverse("travel:quote:result", args=(self.object.activity_id, )))

    def post(self, request, *args, **kwargs):

        return super(self.__class__, self).post(request, *args, **kwargs)


class TransactionCreateView(BaseAbstractView, CreateView):
    model = Transaction
    fields = ['slab']

    def get_form_class(self):
        """
        Returns the form class to use in this view.
        Copied from django 1.6 version
        """
        if self.form_class:
            return self.form_class
        else:
            return model_forms.modelform_factory(self.model, fields=self.fields)

    def get_success_url(self):
        return reverse('travel:transaction:buy', args=(self.object.pk, ))

    def common_form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.activity = self.get_activity()
        self.object.slab = form.cleaned_data['slab']
        self.object.data['plan_details'] = self.object.slab.source['plan_data']
        self.object.data['plan_details']['premium'] = round(
            self.object.slab.premium +
            (self.object.slab.premium * (settings.SERVICE_TAX_PERCENTAGE/100))
        )
        self.object.data['search_data'] = self.object.slab.source['form_data']

        activity_data = self.object.activity.cleaned_data
        self.object.data['search_data']['from_date'] = datetime.datetime.strftime(activity_data['from_date'], "%d-%b-%Y")
        if self.object.slab.plan.type == 'single':
            self.object.data['search_data']['to_date'] = datetime.datetime.strftime(activity_data['to_date'], "%d-%b-%Y")
        if self.object.data['search_data'].get('trip_duration') and self.object.slab.plan.type == 'single':
            trip_duration = (activity_data['to_date'] - activity_data['from_date']).days + 1
            self.object.data['search_data']['trip_duration'] = trip_duration

        if 'insured_details_json' not in self.object.data:
            self.object.data['insured_details_json'] = {}

        self.object.data['search_data']['indian_citizen'] = 'no' if activity_data.get('resident_for') else 'yes'

        insurer = insurer_settings.INSURER_SLUG_MAP[self.object.slab.plan.insurer.slug]
        ins_module = import_module('travel_product.insurers.%s.integration' % insurer)
        ins_module.pre_transaction(self.object)

        return self.object.save()

    def form_valid(self, form):
        self.common_form_valid(form)
        return super(self.__class__, self).form_valid(form)

    def form_valid_ajax(self, form):
        self.common_form_valid(form)
        super(self.__class__, self).form_valid(form)
        serialized_response = deepcopy(self.default_serialized_response)
        serialized_response['redirect_url'] = self.get_success_url()
        return self.render_json_response(serialized_response, status=200)


class TransactionListView(BaseAbstractView, ListView):
    model = Transaction

    def get_queryset(self):
        queryset = super(self.__class__, self).get_queryset()
        return queryset.filter(activity__tracker=self.request.TRACKER)


class TransactionDetailView(BaseAbstractView, DetailView):
    model = Transaction
    pk_url_kwarg = 'transaction_id'
