from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as django_auth_views
from django.views.generic import RedirectView, TemplateView

from customforms import ajaxfield
from django_statsd.urls import urlpatterns as statsd_patterns
from motor_product.dealer.views import RegisterDealer
from motor_product.views import corporateDiscount


urlpatterns = [
    url(r'', include('home.urls')),
    url(r'^rosetta/', include('rosetta.urls')),
    url(r'^cms/', include('cms.urls', namespace='cms')),
    url(r'^honcho/', admin.site.urls),
    url(r'^nimda/', RedirectView.as_view(permanent=True, pattern_name='admin:index')),
    url(r'^insurer/hospital/', TemplateView.as_view(template_name='flatpages:hospitals.html')),
    url(r'^redactor/', include('redactor.urls')),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^ajax/AjaxField/(?P<form_name>[\w-]+)/(?P<form_meta>[\w-]+)/$', ajaxfield.widget_url),
    url(r'^rupys-desk/', include('rdesk.urls')),
    url(r'^seo/', include('seo.urls')),
    url(r'^wiki/', include('wiki.urls')),
    url(r'^activity/', include('activity.urls')),
    url(r'^health-plan/', include('health_product.urls')),
    url(r'^health-plan/payment/', include('health_product.gateway.payu.urls')),
    url(r'^cardekho/', include('motor_product.motor_discount_code_admin.urls')),
    url(r'^mailer/', include('mailer.urls')),
    url(r'^motor/', include('motor_product.urls')),
    url(r'^motor/', include('payment.urls')),
    url(r'^travel-insurance/', include('travel_product.urls', namespace='travel')),
    url(r'^term-insurance/', include('life_product.urls', namespace='term')),
    url(r'^travel/', include('travel_product.urls')),
    url(r'^apis/', include('apis.urls')),
    url(r'^api/', include('apis.urls')),
    url(r'^cfadmin/', include('cfadmin.urls')),
    url(r'^user/', include('account_manager.urls', namespace='account_manager')),
    url(r'^lp/', include('campaign.urls', namespace='campaign')),
    url(r'^leads/', include('leads.urls', namespace='leads')),
    url(r'^accounts/login/$', django_auth_views.login, {'template_name': 'account_manager/login.html'}, 'user_login'),
    url(r'^accounts/logout/$', django_auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^accounts/register-dealer/$', RegisterDealer.as_view()),
    url(r'^marketing/', include('marketing.urls', namespace='marketing')),
    url(r'^otp/', include('otp.urls', namespace='otp')),
    url(r'^corporate/(?P<company>[\w-]+)/login/$', corporateDiscount, name='corporate_login'),
]

if not settings.PRODUCTION:
    urlpatterns += [url(r'^explorer/', include('explorer.urls'))]

urlpatterns += [
    url(r'', include('bakar.urls')),
    url(r'', include('social.apps.django_app.urls', namespace='social')),
    url(r'', include('blog.urls')),
    url(r'', include('core.urls')),
    url(r'', include('campaign.flat_urls')),
    url(r'', include('wiki.flat_urls')),
    url(r'', include('brands.urls')),
    url(r'', include('seocms.urls')),
    url(r'', include('leaflet_campaign.urls', namespace='leaflet_campaign')),
    url(r'^services/timing/', include(statsd_patterns)),
    url(r'^pg/', include('payment_gateway.urls')),
    url(r'^app_notification/', include('app_notification.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# handler404 = 'core.views.handle404'
# handler500 = 'core.views.handle500'
handler500 = 'bakar.views.server_error'
handler404 = 'bakar.views.page_not_found'
handler403 = 'bakar.views.page_not_found_403'

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [url(r'^__debug__/', include(debug_toolbar.urls))]

urlpatterns += [
    url(r'', include('cfblog.urls')),
    ]
