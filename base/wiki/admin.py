from django.contrib import admin
from wiki.models import *


class StateAdmin(admin.ModelAdmin):
    pass
admin.site.register(State, StateAdmin)


class CityAdmin(admin.ModelAdmin):
    pass
admin.site.register(City, CityAdmin)


class HealthProductAdmin(admin.ModelAdmin):
    pass
admin.site.register(HealthProduct, HealthProductAdmin)


class InsurerAdmin(admin.ModelAdmin):
    pass
admin.site.register(Insurer, InsurerAdmin)


class SlabAdmin(admin.ModelAdmin):
    pass
admin.site.register(Slab, SlabAdmin)


class ProcedureCoveredAdmin(admin.ModelAdmin):
    pass
admin.site.register(ProcedureCovered, ProcedureCoveredAdmin)


class WaitingPeriodGeneralAdmin(admin.ModelAdmin):
    pass
admin.site.register(WaitingPeriodGeneral, WaitingPeriodGeneralAdmin)


class WaitingPeriodPreExistingAdmin(admin.ModelAdmin):
    pass
admin.site.register(WaitingPeriodPreExisting, WaitingPeriodPreExistingAdmin)


class AlternativeMedicalAdmin(admin.ModelAdmin):
    pass
admin.site.register(AlternativeMedical, AlternativeMedicalAdmin)


class HospitalAdmin(admin.ModelAdmin):
    pass
admin.site.register(Hospital, HospitalAdmin)


class CancellationPolicyAdmin(admin.ModelAdmin):
    pass
admin.site.register(CancellationPolicy, CancellationPolicyAdmin)


class CoverPremiumTableAdmin(admin.ModelAdmin):
    pass
admin.site.register(CoverPremiumTable, CoverPremiumTableAdmin)


class AgeLimitAdmin(admin.ModelAdmin):
    pass
admin.site.register(AgeLimit, AgeLimitAdmin)


class FamilyCoverageAdmin(admin.ModelAdmin):
    pass
admin.site.register(FamilyCoverage, FamilyCoverageAdmin)


class DayCareAdmin(admin.ModelAdmin):
    pass
admin.site.register(DayCare, DayCareAdmin)


class GeneralAdmin(admin.ModelAdmin):
    pass
admin.site.register(General, GeneralAdmin)


class PreExistingAdmin(admin.ModelAdmin):
    pass
admin.site.register(PreExisting, PreExistingAdmin)


class MaternityCoverAdmin(admin.ModelAdmin):
    pass
admin.site.register(MaternityCover, MaternityCoverAdmin)


class CriticalIllnessCoverageAdmin(admin.ModelAdmin):
    pass
admin.site.register(CriticalIllnessCoverage, CriticalIllnessCoverageAdmin)


class StandardExclusionAdmin(admin.ModelAdmin):
    pass
admin.site.register(StandardExclusion, StandardExclusionAdmin)


class LifeLongRenewabilityAdmin(admin.ModelAdmin):
    pass
admin.site.register(LifeLongRenewability, LifeLongRenewabilityAdmin)


class AmbulanceChargesAdmin(admin.ModelAdmin):
    pass
admin.site.register(AmbulanceCharges, AmbulanceChargesAdmin)


class AlternativePracticesCoverageAdmin(admin.ModelAdmin):
    pass
admin.site.register(AlternativePracticesCoverage,
                    AlternativePracticesCoverageAdmin)


class DomiciliaryHospitalizationAdmin(admin.ModelAdmin):
    pass
admin.site.register(DomiciliaryHospitalization,
                    DomiciliaryHospitalizationAdmin)


class RestoreBenefitsAdmin(admin.ModelAdmin):
    pass
admin.site.register(RestoreBenefits, RestoreBenefitsAdmin)


class OutpatientBenefitsAdmin(admin.ModelAdmin):
    pass
admin.site.register(OutpatientBenefits, OutpatientBenefitsAdmin)


class DentalCoverageAdmin(admin.ModelAdmin):
    pass
admin.site.register(DentalCoverage, DentalCoverageAdmin)


class EyeCoverageAdmin(admin.ModelAdmin):
    pass
admin.site.register(EyeCoverage, EyeCoverageAdmin)


class BonusAdmin(admin.ModelAdmin):
    pass
admin.site.register(Bonus, BonusAdmin)


class NetworkHospitalDailyCashAdmin(admin.ModelAdmin):
    pass
admin.site.register(NetworkHospitalDailyCash, NetworkHospitalDailyCashAdmin)


class NonNetworkHospitalDailyCashAdmin(admin.ModelAdmin):
    pass
admin.site.register(NonNetworkHospitalDailyCash,
                    NonNetworkHospitalDailyCashAdmin)


class ConvalescenceBenefitAdmin(admin.ModelAdmin):
    pass
admin.site.register(ConvalescenceBenefit, ConvalescenceBenefitAdmin)


class PrePostHospitalizationAdmin(admin.ModelAdmin):
    pass
admin.site.register(PrePostHospitalization, PrePostHospitalizationAdmin)


class CopayAdmin(admin.ModelAdmin):
    pass
admin.site.register(Copay, CopayAdmin)


class RoomTypeCopayAdmin(admin.ModelAdmin):
    pass
admin.site.register(RoomTypeCopay, RoomTypeCopayAdmin)


class ZonalRoomRentAdmin(admin.ModelAdmin):
    pass
admin.site.register(ZonalRoomRent, ZonalRoomRentAdmin)


class HealthCheckupAdmin(admin.ModelAdmin):
    pass
admin.site.register(HealthCheckup, HealthCheckupAdmin)


class ClaimsAdmin(admin.ModelAdmin):
    pass
admin.site.register(Claims, ClaimsAdmin)


class MetaDataAdmin(admin.ModelAdmin):
    pass
admin.site.register(MetaData, MetaDataAdmin)


class ProductMetaDataAdmin(admin.ModelAdmin):
    pass
admin.site.register(ProductMetaData, ProductMetaDataAdmin)


class TermAdmin(admin.ModelAdmin):
    pass
admin.site.register(Term, TermAdmin)


class OnlineAvailabilityAdmin(admin.ModelAdmin):
    pass
admin.site.register(OnlineAvailability, OnlineAvailabilityAdmin)


@admin.register(MedicalCondition)
class MedicalConditionAdmin(admin.ModelAdmin):
    pass


@admin.register(MedicalTransactionPermission)
class MedicalPermissionAdmin(admin.ModelAdmin):
    pass
