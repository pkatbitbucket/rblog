from django.conf.urls import url
from django.views.generic.base import RedirectView

from motor_product import views as motor_views

from . import views

urlpatterns = [
    url(r'^health-insurance/network-hospitals/$',
        views.insurer_city_hospital_count, name='hospital_insurers'),

    url(r'^health-insurance/(?P<insurer_slug>[\w-]+)-health-insurance/network-hospitals/$',
        views.insurer_hospitals, name='insurer_city_hospital_count'),

    url(r'^health-insurance/(?P<insurer_slug>[\w-]+)/network-hospitals/$', name='insurer_city_hospital_count_old',
        view=RedirectView.as_view(permanent=True, pattern_name='insurer_city_hospital_count')),

    url(r'^health-insurance/(?P<insurer_slug>[\w-]+)-health-insurance/network-hospitals/(?P<city>[\w-]+)-city-(?P<state>[\w-]+)/$',
        views.CityHospitalList.as_view(), name='insurer_hospital_list'),

    url(r'^health-insurance/(?P<insurer_slug>[\w-]+)/network-hospitals/(?P<city>[\w-]+)-city-(?P<state>[\w-]+)/$',
        RedirectView.as_view(permanent=True, pattern_name='insurer_hospital_list'), name='insurer_hospital_list_old'),

    url(r'^health-insurance/(?P<insurer_slug>[\w-]+)-health-insurance/hospitals-in-(?P<city>[\w-]+)-city-(?P<state>[\w-]+)/$',
        RedirectView.as_view(permanent=True, pattern_name='insurer_hospital_list'),
        name='insurer_location_hospital_list'),

    url(r'^health-insurance/(?P<insurer_slug>[\w-]+)/hospitals-in-(?P<city>[\w-]+)-city-(?P<state>[\w-]+)/$',
        RedirectView.as_view(permanent=True, pattern_name='insurer_hospital_list'),
        name='insurer_location_hospital_list'),

    url(r'^car-insurance/cashless-garages/$', motor_views.insurer_city_garages_count, name='insurer_city_garage_count'),
    url(r'^car-insurance/(?P<insurer_slug>[\w-]+)-car-insurance/cashless-garages/(?P<city>[\w-]+)-city-(?P<state>[\w-]+)/$',
        motor_views.CityGarageList.as_view(), name='insurer_garage_list'),

    url(r'^car-insurance/(?P<insurer_slug>[\w-]+)/cashless-garages/(?P<city>[\w-]+)-city-(?P<state>[\w-]+)/$',
        RedirectView.as_view(permanent=True, pattern_name='insurer_garage_list'), name='insurer_garage_list_old'),
]
