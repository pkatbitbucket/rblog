from collections import defaultdict

import django
from django import forms
from django.core.urlresolvers import reverse
from django.contrib.auth.models import Group
from django.apps.registry import apps
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import get_object_or_404
from django.db.models import Q

import putils
from cf_fhurl import RequestModelForm
from customdb.widgets import AdvancedFileInput
from core.models import User
from .models import (
    TermParent, Term, City, AlternativeMedical, MedicalScore, HealthProduct, ProductMetaData, AttributeDict, Slab,
    ReverseAttributeDict, FamilyCoverPremiumTable, CoverPremiumTable, CancellationPolicy, LifeLongRenewability,
    AmbulanceCharges, AlternativePracticesCoverage, MedicalRequired, AgeLimit, OnlineAvailability, SpecialFeature,
    PreExisting, FamilyCoverage, WaitingPeriodPreExisting, General, WaitingPeriodGeneral, MaternityCover,
    CriticalIllnessCoverage, DomiciliaryHospitalization, RestoreBenefits, OrganDonor, PremiumDiscount, Bonus,
    ConvalescenceBenefit, PrePostHospitalization, ZonalCopay, Copay, ZonalRoomRent, RoomTypeCopay, StandardExclusion,
    HealthCheckup, OutpatientBenefits, Claims, DayCare, ProcedureCovered, DentalCoverage, EyeCoverage,
    NetworkHospitalDailyCash, NonNetworkHospitalDailyCash, ClaimsData, Insurer, WikiArticle, ProductSection,
    MedicalCondition, MedicalTransactionPermission
)

BOOLEAN_CHOICES = (('1', 'Yes'), ('0', 'No'))


def success_response(form):
    return putils.create_standard_response(form.product, form._meta.model.__name__, form.slab_instance)


def initialize_fields(form):
    if not form.request.POST:
        content_type = ContentType.objects.get_for_model(form.Meta.model)
        md = putils.get_meta(form.slab_instance, content_type)
        if md:
            for mattr in ['comments', 'verbose', 'text_to_display', 'is_completed', 'code']:
                form.fields[mattr].initial = getattr(md, mattr, '')


class ParentTermForm(RequestModelForm):
    class Meta:
        model = TermParent
        if django.VERSION > (1, 7):
            fields = "__all__"

    def init(self, parent_term_id=None):
        if parent_term_id:
            self.instance = get_object_or_404(TermParent, pk=parent_term_id)

    def save(self):
        parent_term = super(RequestModelForm, self).save()
        return {
            'id': parent_term.id,
            'name': parent_term.name,
            'is_enabled': parent_term.is_enabled
        }


class TermForm(RequestModelForm):
    name = forms.CharField(widget=forms.Textarea, required=False)

    class Meta:
        model = Term
        if django.VERSION > (1, 7):
            fields = "__all__"

    def init(self, term_id=None):
        if term_id:
            self.instance = get_object_or_404(Term, pk=term_id)

    def save(self):
        term = super(RequestModelForm, self).save()
        return {
            'id': term.id,
            'name': term.name,
            'parent_name': term.parent.name,
            'ib_standard_exclusions': term.ib_standard_exclusions,
            'ib_critical_illness': term.ib_critical_illness,
            'ib_general': term.ib_general,
            'ib_pre_existing': term.ib_pre_existing,
            'ib_domiciliary': term.ib_domiciliary,
            'ib_dental_coverage': term.ib_dental_coverage,
            'ib_eye_coverage': term.ib_eye_coverage,
            'ib_day_care': term.ib_day_care,
        }


class TestInput(forms.Form):
    SERVICE_TYPE_CHOICES = (
        ('SHARED', 'Shared room'),
        ('PVT', 'Pvt. room'),
        ('PVT_HIGH_END', 'Pvt room in high-end')
    )
    GENDER_CHOICES = (
        ("MALE", "Male"),
        ("FEMALE", "Female"),
    )
    sum_assured = forms.IntegerField()
    city = forms.ModelChoiceField(queryset=City.objects.all())
    service_type = forms.ChoiceField(choices=SERVICE_TYPE_CHOICES)
    family = forms.BooleanField(required=False)
    adults = forms.IntegerField(required=False)
    kids = forms.IntegerField(required=False)
    age = forms.CharField()
    gender = forms.ChoiceField(choices=GENDER_CHOICES)
    spouse_age = forms.IntegerField(required=False)
    kid_1_age = forms.IntegerField(required=False)
    kid_2_age = forms.IntegerField(required=False)


class InsurerTestInput(forms.Form):
    GENDER_CHOICES = (
        ("MALE", "Male"),
        ("FEMALE", "Female"),
    )
    city = forms.ModelChoiceField(queryset=City.objects.all())
    age = forms.CharField()
    gender = forms.ChoiceField(choices=GENDER_CHOICES)

    def __init__(self, product, *args, **kwargs):
        super(InsurerTestInput, self).__init__(*args, **kwargs)
        self.fields['sum_assured'] = forms.ChoiceField(choices=list(set(
            [(sl.sum_assured, sl.sum_assured) for sl in product.slabs.filter(~Q(sum_assured=0))])), required=True)
        if product.policy_type == "FAMILY":
            self.fields['spouse_age'] = forms.IntegerField(required=False)
            self.fields['adults'] = forms.IntegerField(required=False)
            self.fields['kids'] = forms.IntegerField(required=False)


class MedicalScoreForm(RequestModelForm):
    slabs = forms.ModelMultipleChoiceField(queryset=AlternativeMedical.objects.none(
    ), widget=forms.CheckboxSelectMultiple, required=True)
    pass

    class Meta:
        model = MedicalScore
        exclude = ('health_product',)

    def init(self, product_id, obj_id=None):
        self.product = get_object_or_404(HealthProduct, pk=product_id)

        instance_id = obj_id or self.request.POST.get("instance_id", '')
        if instance_id:
            self.instance = MedicalScore.objects.get(id=instance_id)
            self.fields['slabs'].queryset = self.product.slabs.filter(Q(sum_assured__gt=0) & (
                Q(medical_score__isnull=True) | Q(id__in=self.instance.slabs.all())))
        else:
            self.fields['slabs'].queryset = self.product.slabs.filter(
                sum_assured__gt=0, medical_score__isnull=True)

        self.medical_scores = MedicalScore.objects.filter(
            health_product=self.product)

    def save(self):
        obj = super(MedicalScoreForm, self).save(commit=False)
        obj.health_product = self.product
        obj.save()
        obj.slabs.clear()
        obj.slabs.add(*self.cleaned_data['slabs'])
        return {'success': True}


class ProductMetaDataForm(RequestModelForm):
    pass

    class Meta:
        model = ProductMetaData
        exclude = ('health_product', 'content_type')

    def init(self, product_id, model):
        self.model = model
        self.product = get_object_or_404(HealthProduct, pk=product_id)
        M = apps.get_model('wiki', AttributeDict[model])
        self.content_type = ContentType.objects.get_for_model(M)
        pmd, created = ProductMetaData.objects.get_or_create(
            health_product=self.product, content_type=self.content_type)
        self.instance = pmd

    def save(self):
        super(RequestModelForm, self).save()
        # if self.instance.is_completed:
        #     MetaData.objects.filter(slabs__health_product=self.product, content_type=self.content_type).update(
        #         is_completed=True)
        return {
            # 'completed_list': [sl.id for sl in Slab.objects.filter(health_product=self.product,
            #                                                        metadata__content_type=self.content_type,
            #                                                        metadata__is_completed=True)],
            'model': self.model,
            'code': self.instance.code,
            'verbose': self.instance.verbose,
            'text_to_display': self.instance.text_to_display,
            'comments': self.instance.comments,
            'is_completed': self.instance.is_completed,
        }


class MultiFeatureForm(RequestModelForm):
    code = forms.CharField(widget=forms.Textarea, required=False)
    verbose = forms.CharField(widget=forms.Textarea, required=False)
    text_to_display = forms.CharField(widget=forms.Textarea, required=False)
    comments = forms.CharField(widget=forms.Textarea, required=False)
    is_completed = forms.BooleanField(widget=forms.Select(
        choices=((False, "No"), (True, "Yes"))), required=False)


class FeatureForm(RequestModelForm):
    code = forms.CharField(widget=forms.Textarea, required=False)
    verbose = forms.CharField(widget=forms.Textarea, required=False)
    text_to_display = forms.CharField(widget=forms.Textarea, required=False)
    comments = forms.CharField(widget=forms.Textarea, required=False)
    is_completed = forms.BooleanField(widget=forms.Select(
        choices=((False, "No"), (True, "Yes"))), required=False)

    def init(self, product_id, slab_id):
        self.product = get_object_or_404(HealthProduct, pk=product_id)
        self.slab_instance = []
        object_instance = None
        var_name = ReverseAttributeDict[self._meta.model.__name__]
        print "VARIABLE", var_name

        i = 0
        for sid in slab_id.split("-"):
            slab = Slab.objects.get(id=sid)
            if i == 0:
                object_instance = getattr(slab, var_name, None)
            if getattr(slab, var_name, None) != object_instance:
                raise Exception(
                    "Error! The selected slabs do not have the same %s" % var_name)
            self.slab_instance.append(slab)
            i += 1

        if object_instance:
            self.instance = object_instance

        initialize_fields(self)

    def save(self):
        obj = super(RequestModelForm, self).save()
        var_name = ReverseAttributeDict[self._meta.model.__name__]
        for slab in self.slab_instance:
            setattr(slab, var_name, obj)
            slab.save()

        print "Saved Object", obj, obj.id
        print "VARIABLE", var_name
        putils.save_meta_form(obj, self.slab_instance, self.cleaned_data)
        return success_response(self)


class FamilyCoverPremiumTableForm(MultiFeatureForm):
    pass

    class Meta:
        model = FamilyCoverPremiumTable
        exclude = ('slabs',)

    def init(self, product_id, slab_id, cpt_id=None):
        self.product = get_object_or_404(HealthProduct, pk=product_id)
        self.slab_instance = []
        for sid in slab_id.split("-"):
            self.slab_instance.append(Slab.objects.get(id=sid))

        instance_id = cpt_id or self.request.POST.get("instance_id", '')
        if instance_id:
            self.instance = get_object_or_404(
                FamilyCoverPremiumTable, pk=instance_id)
        initialize_fields(self)

    def save(self):
        family_cover_premium_table = super(
            FamilyCoverPremiumTableForm, self).save()
        family_cover_premium_table.slabs.add(*self.slab_instance)

        putils.save_meta_form(family_cover_premium_table,
                              self.slab_instance, self.cleaned_data)
        return success_response(self)


class CoverPremiumTableForm(MultiFeatureForm):
    pass

    class Meta:
        model = CoverPremiumTable
        exclude = ('slabs',)

    def init(self, product_id, slab_id, cpt_id=None):
        self.product = get_object_or_404(HealthProduct, pk=product_id)
        self.slab_instance = []
        for sid in slab_id.split("-"):
            self.slab_instance.append(Slab.objects.get(id=sid))

        instance_id = cpt_id or self.request.POST.get("instance_id", '')
        if instance_id:
            self.instance = get_object_or_404(
                CoverPremiumTable, pk=instance_id)
        initialize_fields(self)

    def save(self):
        cover_premium_table = super(CoverPremiumTableForm, self).save()
        cover_premium_table.slabs.add(*self.slab_instance)

        putils.save_meta_form(cover_premium_table,
                              self.slab_instance, self.cleaned_data)
        return success_response(self)


class CancellationPolicyForm(MultiFeatureForm):
    pass

    class Meta:
        model = CancellationPolicy
        exclude = ('slabs',)

    def init(self, product_id, slab_id):
        self.product = get_object_or_404(HealthProduct, pk=product_id)
        self.slab_instance = []
        for sid in slab_id.split("-"):
            self.slab_instance.append(Slab.objects.get(id=sid))
        initialize_fields(self)

    def save(self):
        cancellation_policy = super(CancellationPolicyForm, self).save()
        cancellation_policy.slabs.add(*self.slab_instance)
        putils.save_meta_form(cancellation_policy,
                              self.slab_instance, self.cleaned_data)
        return success_response(self)


class LifeLongRenewabilityForm(FeatureForm):
    pass

    class Meta:
        model = LifeLongRenewability
        exclude = ('slab',)


class AmbulanceChargesForm(FeatureForm):
    pass

    class Meta:
        model = AmbulanceCharges
        exclude = ('slab',)


class AlternativePracticesCoverageForm(FeatureForm):
    alternative_practices = forms.ModelMultipleChoiceField(
        queryset=AlternativeMedical.objects.all(), widget=forms.CheckboxSelectMultiple, required=False)
    pass

    class Meta:
        model = AlternativePracticesCoverage
        exclude = ('slabs',)


class OnlineAvailabilityForm(FeatureForm):
    pass

    class Meta:
        model = OnlineAvailability
        exclude = ('slab',)


class MedicalRequiredForm(FeatureForm):
    pass

    class Meta:
        model = MedicalRequired
        exclude = ('slab',)


class AgeLimitForm(FeatureForm):
    pass

    class Meta:
        model = AgeLimit
        exclude = ('slab',)


class SpecialFeatureForm(FeatureForm):
    pass

    class Meta:
        model = SpecialFeature
        exclude = ('slab',)


class FamilyCoverageForm(FeatureForm):
    pass

    class Meta:
        model = FamilyCoverage
        exclude = ('slab',)


class PreExistingForm(MultiFeatureForm):
    pass

    class Meta:
        model = PreExisting
        exclude = ('slab', 'conditions')

    def init(self, product_id, slab_id):
        self.num_fields = 8
        for i in range(1, self.num_fields + 1):
            # Defining fields for general list
            self.fields['terms_list_%d' % i] = forms.CharField(
                widget=forms.Textarea(), required=False)
            self.fields['terms_period_%d' %
                        i] = forms.IntegerField(required=False)
            self.fields['terms_sum_insured_limit_%d' %
                        i] = forms.IntegerField(required=False)
            self.fields['terms_sum_insured_limit_percentage_%d' %
                        i] = forms.FloatField(required=False)
            self.fields['terms_copay_%d' %
                        i] = forms.FloatField(required=False)

        self.terms_fields = []
        bound_fields = dict([(i.name, i) for i in self.visible_fields()])
        for i in range(1, self.num_fields + 1):
            self.terms_fields.append(
                [i, bound_fields['terms_list_%d' % i], bound_fields['terms_period_%d' % i], bound_fields[
                    'terms_sum_insured_limit_%d' % i], bound_fields['terms_sum_insured_limit_percentage_%d' % i],
                 bound_fields['terms_copay_%d' % i]])

        self.product = get_object_or_404(HealthProduct, pk=product_id)
        self.slab_instance = []
        object_instance = None

        i = 0
        for sid in slab_id.split("-"):
            slab = Slab.objects.get(id=sid)
            if i == 0:
                object_instance = slab.pre_existing
            if slab.pre_existing != object_instance:
                raise Exception(
                    "Error! The selected slabs do not have the same pre-existing conditions")
            self.slab_instance.append(slab)
            i += 1

        self.terms_grp = defaultdict(list)
        if object_instance:
            self.instance = object_instance
            print "Instance Object", object_instance, object_instance.id
            # only add initial values if request is NOT POST
            if not self.request.POST:
                for iwp in self.instance.conditions.all():
                    mkey = "-".join(map(lambda x: str(x) if x else '', [getattr(iwp, k) for k in [
                        'period', 'limit', 'limit_in_percentage', 'copay']]))
                    self.terms_grp[mkey].append(iwp.term.name)
                cnt = 1
                for p, dlist in self.terms_grp.items():
                    self.fields['terms_list_%d' %
                                cnt].initial = "|".join(dlist)
                    self.fields['terms_period_%d' %
                                cnt].initial = p.split("-")[0]
                    self.fields['terms_sum_insured_limit_%d' %
                                cnt].initial = p.split("-")[1]
                    self.fields['terms_sum_insured_limit_percentage_%d' %
                                cnt].initial = p.split("-")[2]
                    self.fields['terms_copay_%d' %
                                cnt].initial = p.split("-")[3]
                    cnt += 1

        # django template cannot loop with defaultdict, hence setting default
        # factory to None to make it work
        self.terms_grp.default_factory = None

        initialize_fields(self)

    def save(self):
        wp = super(PreExistingForm, self).save()
        d = self.cleaned_data.get

        wp.conditions.clear()
        WaitingPeriodPreExisting.objects.filter(
            preexisting__isnull=True).delete()

        for i in range(1, self.num_fields + 1):
            if d('terms_list_%d' % i) or d('terms_period_%d' % i) or d('terms_sum_insured_limit_%d' % i) or d(
                            'terms_sum_insured_limit_percentage_%d' % i) or d('terms_copay_%d' % i):
                terms_list = list(set([k.strip().lower() for k in d(
                    'terms_list_%d' % i).split("|") if k.strip()]))
                for term in terms_list:
                    term_obj, created = Term.objects.get_or_create(name=term)
                    term_obj.term_type = "DISEASE"
                    term_obj.save()
                    wpgen = WaitingPeriodPreExisting(
                        term=term_obj,
                        period=self.cleaned_data.get(
                            'terms_period_%d' % i, ''),
                        limit=self.cleaned_data.get(
                            'terms_sum_insured_limit_%d' % i, ''),
                        limit_in_percentage=self.cleaned_data.get(
                            'terms_sum_insured_limit_percentage_%d' % i, ''),
                        copay=self.cleaned_data.get('terms_copay_%d' % i, '')
                    )
                    wpgen.save()
                    wp.conditions.add(wpgen)

        for slab in self.slab_instance:
            slab.pre_existing = wp
            slab.save()
        putils.save_meta_form(wp, self.slab_instance, self.cleaned_data)
        return success_response(self)


class GeneralForm(MultiFeatureForm):
    pass

    class Meta:
        model = General
        exclude = ('slab', 'conditions')

    def init(self, product_id, slab_id):
        self.num_fields = 8
        for i in range(1, self.num_fields + 1):
            # Defining fields for general list
            self.fields['terms_list_%d' % i] = forms.CharField(
                widget=forms.Textarea(), required=False)
            self.fields['terms_period_%d' %
                        i] = forms.IntegerField(required=False)
            self.fields['terms_sum_insured_limit_%d' %
                        i] = forms.IntegerField(required=False)
            self.fields['terms_sum_insured_limit_percentage_%d' %
                        i] = forms.FloatField(required=False)
            self.fields['terms_copay_%d' %
                        i] = forms.FloatField(required=False)

        self.terms_fields = []
        bound_fields = dict([(i.name, i) for i in self.visible_fields()])
        for i in range(1, self.num_fields + 1):
            self.terms_fields.append(
                [i, bound_fields['terms_list_%d' % i], bound_fields['terms_period_%d' % i], bound_fields[
                    'terms_sum_insured_limit_%d' % i], bound_fields['terms_sum_insured_limit_percentage_%d' % i],
                 bound_fields['terms_copay_%d' % i]])

        self.product = get_object_or_404(HealthProduct, pk=product_id)
        self.slab_instance = []
        object_instance = None

        i = 0
        for sid in slab_id.split("-"):
            slab = Slab.objects.get(id=sid)
            if i == 0:
                object_instance = slab.general
            if slab.general != object_instance:
                raise Exception(
                    "Error! The selected slabs do not have the same general conditions")
            self.slab_instance.append(slab)
            i += 1

        self.terms_grp = defaultdict(list)
        if object_instance:
            self.instance = object_instance
            print "Instance Object", object_instance, object_instance.id

            # only add initial values if request is NOT POST
            if not self.request.POST:
                for iwp in self.instance.conditions.all():
                    mkey = "-".join(map(lambda x: str(x) if x else '', [getattr(iwp, k) for k in [
                        'period', 'limit', 'limit_in_percentage', 'copay']]))
                    self.terms_grp[mkey].append(iwp.term.name)
                cnt = 1
                for p, dlist in self.terms_grp.items():
                    self.fields['terms_list_%d' %
                                cnt].initial = "|".join(dlist)
                    self.fields['terms_period_%d' %
                                cnt].initial = p.split("-")[0]
                    self.fields['terms_sum_insured_limit_%d' %
                                cnt].initial = p.split("-")[1]
                    self.fields['terms_sum_insured_limit_percentage_%d' %
                                cnt].initial = p.split("-")[2]
                    self.fields['terms_copay_%d' %
                                cnt].initial = p.split("-")[3]
                    cnt += 1

        # django template cannot loop with defaultdict,
        # hence setting default factory to None to make it work
        self.terms_grp.default_factory = None
        initialize_fields(self)

    def save(self):
        wp = super(GeneralForm, self).save()
        d = self.cleaned_data.get

        wp.conditions.clear()
        WaitingPeriodGeneral.objects.filter(general__isnull=True).delete()

        for i in range(1, self.num_fields + 1):
            if d('terms_list_%d' % i) or d('terms_period_%d' % i) or d('terms_sum_insured_limit_%d' % i) or d(
                            'terms_sum_insured_limit_percentage_%d' % i) or d('terms_copay_%d' % i):
                terms_list = list(set([k.strip().lower() for k in d(
                    'terms_list_%d' % i).split("|") if k.strip()]))
                for term in terms_list:
                    term_obj, created = Term.objects.get_or_create(name=term)
                    term_obj.term_type = "DISEASE"
                    term_obj.save()
                    wpgen = WaitingPeriodGeneral(
                        term=term_obj,
                        period=self.cleaned_data.get(
                            'terms_period_%d' % i, ''),
                        limit=self.cleaned_data.get(
                            'terms_sum_insured_limit_%d' % i, ''),
                        limit_in_percentage=self.cleaned_data.get(
                            'terms_sum_insured_limit_percentage_%d' % i, ''),
                        copay=self.cleaned_data.get('terms_copay_%d' % i, '')
                    )
                    wpgen.save()
                    wp.conditions.add(wpgen)

        for slab in self.slab_instance:
            slab.general = wp
            slab.save()
        putils.save_meta_form(wp, self.slab_instance, self.cleaned_data)
        return success_response(self)


class MaternityCoverForm(FeatureForm):
    pass

    class Meta:
        model = MaternityCover
        exclude = ('slab',)


class CriticalIllnessCoverageForm(MultiFeatureForm):
    terms_list = forms.CharField(widget=forms.Textarea(
    ), required=False, label="Disease covered by critical illness")
    pass

    class Meta:
        model = CriticalIllnessCoverage
        exclude = ('slab', 'terms')

    def init(self, product_id, slab_id):
        self.product = get_object_or_404(HealthProduct, pk=product_id)
        self.slab_instance = []
        object_instance = None

        i = 0
        for sid in slab_id.split("-"):
            slab = Slab.objects.get(id=sid)
            if i == 0:
                object_instance = slab.critical_illness_coverage
            if slab.critical_illness_coverage != object_instance:
                raise Exception(
                    "Error! The selected slabs do not have the same critical illness coverage")
            self.slab_instance.append(slab)
            i += 1

        self.terms_grp = []
        if object_instance:
            self.instance = object_instance
            print "Instance Object", object_instance, object_instance.id

            # only add initial values if request is NOT POST
            if not self.request.POST:
                for dis in self.instance.terms.all():
                    self.terms_grp.append(dis.name)
                self.fields['terms_list'].initial = "|".join(self.terms_grp)
        initialize_fields(self)

    def save(self):
        cic = super(CriticalIllnessCoverageForm, self).save()
        d = self.cleaned_data.get
        cic.terms.clear()

        terms_list = list(set([k.strip().lower()
                               for k in d('terms_list').split("|") if k.strip()]))
        for dis in terms_list:
            dis_obj, created = Term.objects.get_or_create(name=dis)
            dis_obj.term_type = "DISEASE"
            dis_obj.save()
            cic.terms.add(dis_obj)

        for slab in self.slab_instance:
            slab.critical_illness_coverage = cic
            slab.save()
        putils.save_meta_form(cic, self.slab_instance, self.cleaned_data)
        return success_response(self)


class DomiciliaryHospitalizationForm(MultiFeatureForm):
    terms_list = forms.CharField(widget=forms.Textarea(
    ), required=False, label="Exclusions in disease, treatments etc.")
    pass

    class Meta:
        model = DomiciliaryHospitalization
        exclude = ('slab', 'exclusions')

    def init(self, product_id, slab_id):
        self.product = get_object_or_404(HealthProduct, pk=product_id)
        self.slab_instance = []
        object_instance = None

        i = 0
        for sid in slab_id.split("-"):
            slab = Slab.objects.get(id=sid)
            if i == 0:
                object_instance = slab.domiciliary_hospitalization
            if slab.domiciliary_hospitalization != object_instance:
                raise Exception(
                    "Error! The selected slabs do not have the same domiciliary hospitalization")
            self.slab_instance.append(slab)
            i += 1

        self.terms_grp = []
        if object_instance:
            self.instance = object_instance
            print "Instance Object", object_instance, object_instance.id

            # only add initial values if request is NOT POST
            if not self.request.POST:
                for dis in self.instance.exclusions.all():
                    self.terms_grp.append(dis.name)
                self.fields['terms_list'].initial = "|".join(self.terms_grp)
        initialize_fields(self)

    def save(self):
        dmh = super(DomiciliaryHospitalizationForm, self).save()
        d = self.cleaned_data.get

        dmh.exclusions.clear()

        terms_list = list(set([k.strip().lower()
                               for k in d('terms_list').split("|") if k.strip()]))
        for dis in terms_list:
            dis_obj, created = Term.objects.get_or_create(name=dis)
            dis_obj.term_type = "DISEASE"
            dis_obj.save()
            dmh.exclusions.add(dis_obj)

        for slab in self.slab_instance:
            slab.domiciliary_hospitalization = dmh
            slab.save()
        putils.save_meta_form(dmh, self.slab_instance, self.cleaned_data)
        return success_response(self)


class RestoreBenefitsForm(FeatureForm):
    pass

    class Meta:
        model = RestoreBenefits
        exclude = ('slab',)


class OrganDonorForm(FeatureForm):
    pass

    class Meta:
        model = OrganDonor
        exclude = ('slab',)


class OutpatientBenefitsForm(FeatureForm):
    pass

    class Meta:
        model = OutpatientBenefits
        exclude = ('slab',)


class BonusForm(FeatureForm):
    pass

    class Meta:
        model = Bonus
        exclude = ('slab',)


class PremiumDiscountForm(FeatureForm):
    pass

    class Meta:
        model = PremiumDiscount
        exclude = ('slab',)


class ConvalescenceBenefitForm(FeatureForm):
    pass

    class Meta:
        model = ConvalescenceBenefit
        exclude = ('slab',)


class PrePostHospitalizationForm(FeatureForm):
    pass

    class Meta:
        model = PrePostHospitalization
        exclude = ('slab',)


class CopayForm(FeatureForm):
    pass

    class Meta:
        model = Copay
        exclude = ('slab',)


class ZonalCopayForm(MultiFeatureForm):
    pass

    class Meta:
        model = ZonalCopay
        exclude = ('slabs',)

    def init(self, product_id, slab_id):
        self.product = get_object_or_404(HealthProduct, pk=product_id)
        self.slab_instance = []
        for sid in slab_id.split("-"):
            self.slab_instance.append(Slab.objects.get(id=sid))
        initialize_fields(self)

    def save(self):
        zonal_copay = super(ZonalCopayForm, self).save()
        zonal_copay.slabs.add(*self.slab_instance)
        putils.save_meta_form(
            zonal_copay, self.slab_instance, self.cleaned_data)
        return success_response(self)


class ZonalRoomRentForm(MultiFeatureForm):
    pass

    class Meta:
        model = ZonalRoomRent
        exclude = ('slabs',)

    def init(self, product_id, slab_id):
        self.product = get_object_or_404(HealthProduct, pk=product_id)
        self.slab_instance = []
        for sid in slab_id.split("-"):
            self.slab_instance.append(Slab.objects.get(id=sid))
        initialize_fields(self)

    def save(self):
        zonal_room_rent = super(ZonalRoomRentForm, self).save()
        zonal_room_rent.slabs.add(*self.slab_instance)
        putils.save_meta_form(
            zonal_room_rent, self.slab_instance, self.cleaned_data)
        return success_response(self)


class RoomTypeCopayForm(MultiFeatureForm):
    pass

    class Meta:
        model = RoomTypeCopay
        exclude = ('slabs',)

    def init(self, product_id, slab_id):
        self.product = get_object_or_404(HealthProduct, pk=product_id)
        self.slab_instance = []
        for sid in slab_id.split("-"):
            self.slab_instance.append(Slab.objects.get(id=sid))
        initialize_fields(self)

    def save(self):
        room_type_copay = super(RoomTypeCopayForm, self).save()
        room_type_copay.slabs.add(*self.slab_instance)
        putils.save_meta_form(
            room_type_copay, self.slab_instance, self.cleaned_data)
        return success_response(self)


class HealthCheckupForm(FeatureForm):
    pass

    class Meta:
        exclude = ('slab',)
        model = HealthCheckup


class StandardExclusionForm(MultiFeatureForm):
    terms_list = forms.CharField(widget=forms.Textarea(), required=False)
    pass

    class Meta:
        model = StandardExclusion
        exclude = ('slab', 'terms')

    def init(self, product_id, slab_id):
        self.product = get_object_or_404(HealthProduct, pk=product_id)
        self.slab_instance = []
        object_instance = None

        i = 0
        for sid in slab_id.split("-"):
            slab = Slab.objects.get(id=sid)
            if i == 0:
                object_instance = slab.standard_exclusion
            if slab.standard_exclusion != object_instance:
                raise Exception(
                    "Error! The selected slabs do not have the same non-standard exclusions")
            self.slab_instance.append(slab)
            i += 1

        self.terms_grp = []
        if object_instance:
            self.instance = object_instance
            print "Instance Object", object_instance, object_instance.id

            # only add initial values if request is NOT POST
            if not self.request.POST:
                for dis in self.instance.terms.all():
                    self.terms_grp.append(dis.name)
                self.fields['terms_list'].initial = "|".join(self.terms_grp)

        initialize_fields(self)

    def save(self):
        nse = super(StandardExclusionForm, self).save()
        d = self.cleaned_data.get

        nse.terms.clear()

        terms_list = list(set([k.strip().lower()
                               for k in d('terms_list').split("|") if k.strip()]))
        for terms in terms_list:
            terms_obj, created = Term.objects.get_or_create(name=terms)
            print ">>>>", terms_obj.name, created
            terms_obj.term_type = "DISEASE"
            terms_obj.save()
            nse.terms.add(terms_obj)

        for slab in self.slab_instance:
            slab.standard_exclusion = nse
            slab.save()
        putils.save_meta_form(nse, self.slab_instance, self.cleaned_data)
        return success_response(self)


class ClaimsForm(FeatureForm):
    pass

    class Meta:
        model = Claims
        exclude = ('slab',)


class DayCareForm(MultiFeatureForm):
    pass

    class Meta:
        model = DayCare
        exclude = ('slab', 'procedure_covered')

    def init(self, product_id, slab_id):
        self.num_fields = 8
        for i in range(1, self.num_fields + 1):
            self.fields['terms_list_%d' % i] = forms.CharField(
                widget=forms.Textarea(), required=False)
            self.fields['verbose_%d' % i] = forms.CharField(
                widget=forms.Textarea(), required=False)

        self.terms_fields = []
        bound_fields = dict([(i.name, i) for i in self.visible_fields()])
        for i in range(1, self.num_fields + 1):
            self.terms_fields.append(
                [i, bound_fields['terms_list_%d' % i], bound_fields['verbose_%d' % i]])

        self.product = get_object_or_404(HealthProduct, pk=product_id)
        self.slab_instance = []
        object_instance = None

        i = 0
        for sid in slab_id.split("-"):
            slab = Slab.objects.get(id=sid)
            if i == 0:
                object_instance = slab.day_care
            if slab.day_care != object_instance:
                raise Exception(
                    "Error! The selected slabs do not have the same day care")
            self.slab_instance.append(slab)
            i += 1

        self.terms_grp = defaultdict(list)
        if object_instance:
            self.instance = object_instance
            print "Instance Object", object_instance, object_instance.id

            # only add initial values if request is NOT POST
            if not self.request.POST:
                for iwp in self.instance.procedure_covered.all():
                    self.terms_grp[iwp.verbose].append(iwp.term.name)
                cnt = 1
                for p, dlist in self.terms_grp.items():
                    self.fields['terms_list_%d' %
                                cnt].initial = "|".join(dlist)
                    self.fields['verbose_%d' % cnt].initial = p
                    cnt += 1
        else:
            print "New Object"

        # django template cannot loop with defaultdict, hence setting default
        # factory to None to make it work
        self.terms_grp.default_factory = None
        initialize_fields(self)

    def save(self):
        dcare = super(DayCareForm, self).save()
        d = self.cleaned_data.get

        dcare.procedure_covered.clear()
        ProcedureCovered.objects.filter(daycare__isnull=True).delete()

        for i in range(1, self.num_fields + 1):
            if d('terms_list_%d' % i):
                terms_list = list(set([k.strip().lower() for k in d(
                    'terms_list_%d' % i).split("|") if k.strip()]))
                for proc in terms_list:
                    dcare_term, created = Term.objects.get_or_create(name=proc)
                    dcare_term.term_type = "PROCEDURE"
                    dcare_term.save()

                    pcovered = ProcedureCovered(
                        term=dcare_term, verbose=d('verbose_%d' % i, ''))
                    pcovered.save()
                    dcare.procedure_covered.add(pcovered)

        for slab in self.slab_instance:
            slab.day_care = dcare
            slab.save()
        putils.save_meta_form(dcare, self.slab_instance, self.cleaned_data)
        return success_response(self)


class DentalCoverageForm(MultiFeatureForm):
    terms_list = forms.CharField(widget=forms.Textarea(
    ), required=False, label="Exclusions in treatment")

    class Meta:
        model = DentalCoverage
        exclude = ('slab', 'exclusions')

    def init(self, product_id, slab_id):
        self.product = get_object_or_404(HealthProduct, pk=product_id)
        self.slab_instance = []
        object_instance = None

        i = 0
        for sid in slab_id.split("-"):
            slab = Slab.objects.get(id=sid)
            if i == 0:
                object_instance = slab.dental_coverage
            if slab.dental_coverage != object_instance:
                raise Exception(
                    "Error! The selected slabs do not have the same Dental Coverage")
            self.slab_instance.append(slab)
            i += 1

        self.terms_grp = []
        if object_instance:
            self.instance = object_instance
            print "Instance Object", object_instance, object_instance.id

            # only add initial values if request is NOT POST
            if not self.request.POST:
                for trt in self.instance.exclusions.all():
                    self.terms_grp.append(trt.name)
                self.fields['terms_list'].initial = "|".join(self.terms_grp)
        initialize_fields(self)

    def save(self):
        dec = super(DentalCoverageForm, self).save()
        d = self.cleaned_data.get

        dec.exclusions.clear()

        terms_list = list(set([k.strip().lower()
                               for k in d('terms_list').split("|") if k.strip()]))
        for term in terms_list:
            term_obj, created = Term.objects.get_or_create(name=term)
            term_obj.term_type = "TREATMENT"
            term_obj.save()
            dec.exclusions.add(term_obj)

        for slab in self.slab_instance:
            slab.dental_coverage = dec
            slab.save()
        putils.save_meta_form(dec, self.slab_instance, self.cleaned_data)
        return success_response(self)


class EyeCoverageForm(MultiFeatureForm):
    terms_list = forms.CharField(widget=forms.Textarea(
    ), required=False, label="Exclusions in treatment")
    pass

    class Meta:
        model = EyeCoverage
        exclude = ('slab', 'exclusions')

    def init(self, product_id, slab_id):
        self.product = get_object_or_404(HealthProduct, pk=product_id)
        self.slab_instance = []
        object_instance = None

        i = 0
        for sid in slab_id.split("-"):
            slab = Slab.objects.get(id=sid)
            if i == 0:
                object_instance = slab.eye_coverage
            if slab.eye_coverage != object_instance:
                raise Exception(
                    "Error! The selected slabs do not have the same Eye Coverage")
            self.slab_instance.append(slab)
            i += 1

        self.terms_grp = []
        if object_instance:
            self.instance = object_instance
            print "Instance Object", object_instance, object_instance.id

            # only add initial values if request is NOT POST
            if not self.request.POST:
                for trt in self.instance.exclusions.all():
                    self.terms_grp.append(trt.name)
                self.fields['terms_list'].initial = "|".join(self.terms_grp)
        initialize_fields(self)

    def save(self):
        dec = super(EyeCoverageForm, self).save()
        d = self.cleaned_data.get

        dec.exclusions.clear()

        terms_list = list(set([k.strip().lower()
                               for k in d('terms_list').split("|") if k.strip()]))
        for term in terms_list:
            term_obj, created = Term.objects.get_or_create(name=term)
            term_obj.term_type = "TREATMENT"
            term_obj.save()
            dec.exclusions.add(term_obj)

        for slab in self.slab_instance:
            slab.eye_coverage = dec
            slab.save()
        putils.save_meta_form(dec, self.slab_instance, self.cleaned_data)
        return success_response(self)


class NetworkHospitalDailyCashForm(FeatureForm):
    pass

    class Meta:
        model = NetworkHospitalDailyCash
        exclude = ('slab',)


class NonNetworkHospitalDailyCashForm(FeatureForm):
    pass

    class Meta:
        model = NonNetworkHospitalDailyCash
        exclude = ('slab',)


class ClaimsDataForm(RequestModelForm):
    pass

    class Meta:
        model = ClaimsData
        if django.VERSION > (1, 7):
            fields = "__all__"

    def init(self, insurer_id):
        self.insurer = Insurer.objects.get(id=insurer_id)
        if self.insurer.claims_data:
            self.instance = self.insurer.claims_data

    def save(self):
        cd = super(ClaimsDataForm, self).save()
        self.insurer.claims_data = cd
        self.insurer.save()
        return {'success': True}


class InsurerAddForm(RequestModelForm):
    logo = forms.ImageField(widget=AdvancedFileInput(), required=False)
    insurer_manager = forms.ModelChoiceField(
        queryset=User.objects.filter(groups__name='INSURER'))

    class Meta:
        model = Insurer
        exclude = ('author',)

    def init(self, insurer_id=None):
        if insurer_id:
            self.instance = Insurer.objects.get(id=insurer_id)

    def save(self):
        insurer = super(InsurerAddForm, self).save()
        if not insurer.author:
            insurer.author = self.request.user
        insurer.save()
        if self.request.POST['save'] == 'save':
            insurer_url = reverse('insurer_detail', kwargs={
                'slug': insurer.slug,
            })
            return {'success': True, 'redirect': insurer_url}
        if self.request.POST['save'] == 'saveedit':
            return {'success': True, 'redirect': reverse('insurer_edit', kwargs={'insurer_id': insurer.id})}

    def clean_slug(self):
        d = self.cleaned_data
        if self.instance and self.instance.id:
            isr = Insurer.objects.filter(
                Q(slug=d['slug']) & ~Q(id=self.instance.id))
        else:
            isr = Insurer.objects.filter(slug=d['slug'])

        if len(isr) > 0:
            raise forms.ValidationError("Slug invalid. Must be Unique")
        else:
            return d['slug']


class HealthProductAddForm(RequestModelForm):
    covers = forms.CharField(
        help_text='e.g. 100000, 200000-silver, 200000-gold, 300000|100000, 400000|100000-silver')

    class Meta:
        model = HealthProduct
        exclude = ('insurer', 'author', 'is_completed',
                   'is_backend_integrated')

    def init(self, insurer_id, product_id=None):
        self.insurer = Insurer.objects.get(id=insurer_id)
        if product_id:
            self.instance = HealthProduct.objects.get(id=product_id)
            cover_initial = []
            for sl in Slab.objects.filter(health_product=self.instance):
                if sl.sum_assured == 0:
                    continue
                cover_str = str(sl.sum_assured)
                if sl.deductible:
                    cover_str += '|%i' % sl.deductible
                if sl.grade != '':
                    cover_str += '-%s' % sl.grade
                cover_initial.append(cover_str)
            self.initial['covers'] = ", ".join(cover_initial)

    def save(self):
        product = super(HealthProductAddForm, self).save(commit=False)
        if not product.author:
            product.author = self.request.user
        product.insurer = self.insurer
        product.save()

        if not Slab.objects.filter(sum_assured=0, health_product=product):
            """ Create default Slab """
            Slab(sum_assured=0, health_product=product).save()

        for cover in self.cleaned_data['covers'].split(","):
            sl_kwargs = {'health_product': product, 'grade': ''}
            sl_desc = cover.split("-")

            amts = sl_desc[0].split("|")
            sl_kwargs['sum_assured'] = int(amts[0])
            if len(amts) == 2:
                sl_kwargs['deductible'] = int(amts[1])

            if len(sl_desc) == 2:
                sl_kwargs['grade'] = sl_desc[1].strip()

            if Slab.objects.filter(**sl_kwargs):
                continue
            else:
                slab = Slab(**sl_kwargs)
                slab.save()

        if self.request.POST['save'] == 'save':
            product_url = reverse('health_product_detail', kwargs={
                'insurer-slug': self.insurer.slug,
                'slug': product.slug,
            })
            return {'success': True, 'redirect': product_url}
        if self.request.POST['save'] == 'saveedit':
            return {'success': True, 'redirect': reverse('health_product_edit', kwargs={'insurer_id': self.insurer.id,
                                                                                        'product_id': product.id})}

    def clean_covers(self):
        d = self.cleaned_data
        try:
            for cover in d['covers'].split(","):
                sl_desc = cover.split("-")

                amts = sl_desc[0].split("|")
                sum_assured = int(amts[0])
                if len(amts) == 2:
                    deductible = int(amts[1])

                if len(sl_desc) == 2:
                    grade = sl_desc[1].strip()

        except Exception, e:
            raise forms.ValidationError("Error: %s" % e)
        return d['covers']

    def clean_slug(self):
        d = self.cleaned_data
        if self.instance and self.instance.id:
            product = HealthProduct.objects.filter(
                Q(slug=d['slug']) & ~Q(id=self.instance.id))
        else:
            product = HealthProduct.objects.filter(slug=d['slug'])

        if len(product) > 0:
            raise forms.ValidationError("Slug invalid. Must be Unique")
        else:
            return d['slug']


class WikiArticleAddForm(RequestModelForm):
    thumbnail = forms.ImageField(widget=AdvancedFileInput(), required=False)
    author = forms.ModelChoiceField(
        queryset=User.objects.filter(Q(is_superuser=True) | Q(groups__name='AUTHOR')).distinct()
    )

    class Meta:
        model = WikiArticle
        if django.VERSION > (1, 7):
            fields = "__all__"

    def init(self, article_slug=None):
        if article_slug:
            self.instance = WikiArticle.objects.get(slug=article_slug)

    def save(self):
        article = super(WikiArticleAddForm, self).save()
        article.save()
        if self.request.POST['save'] == 'save':
            article_url = reverse('admin_article_wiki_details', kwargs={
                'article_slug': article.slug,
            })
            return {'success': True, 'redirect': article_url}
        if self.request.POST['save'] == 'saveedit':
            return {'success': True, 'redirect': reverse('article_edit', kwargs={'article_slug': article.slug})}

    def clean_slug(self):
        d = self.cleaned_data
        if self.instance and self.instance.id:
            pst = WikiArticle.objects.filter(
                Q(slug=d['slug']) & ~Q(id=self.instance.id))
        else:
            pst = WikiArticle.objects.filter(slug=d['slug'])

        if len(pst) > 0:
            raise forms.ValidationError("Slug invalid. Must be Unique")
        else:
            return d['slug']


class SectionAddForm(RequestModelForm):
    class Meta:
        model = ProductSection
        if django.VERSION > (1, 7):
            fields = "__all__"

    def init(self, section_id=None):
        if section_id:
            self.instance = ProductSection.objects.get(id=section_id)

    def save(self):
        section = super(SectionAddForm, self).save()
        if self.instance:
            section.product_page = self.instance.product_page
        section.save()
        article = section.product_page
        if self.request.POST['save'] == 'save':
            article_url = reverse('admin_article_wiki_details', kwargs={
                'article_slug': article.slug,
            })
            return {'success': True, 'redirect': article_url}
        if self.request.POST['save'] == 'saveedit':
            return {'success': True, 'redirect': reverse('article_edit', kwargs={'article_slug': article.slug})}


class MedicalConditionAddForm(RequestModelForm):
    global_decline = forms.TypedChoiceField(
        coerce=lambda x: x == 'True',
        choices=((False, 'False'), (True, 'True')),
        widget=forms.RadioSelect
    )

    class Meta:
        model = MedicalCondition
        exclude = ()

    def init(self, medical_condition_id=None):
        if medical_condition_id:
            self.instance = MedicalCondition.objects.get(id=medical_condition_id)

    def save(self):
        super(MedicalConditionAddForm, self).save()
        return {'success': True, 'redirect': reverse('medical_condition_add')}


class MedicalPermissionAddForm(forms.ModelForm):

    class Meta:
        model = MedicalTransactionPermission
        widgets = {
            'medical_condition': forms.HiddenInput(),
            'health_product': forms.HiddenInput()
        }
        exclude = ()
