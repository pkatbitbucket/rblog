from django.core.management.base import BaseCommand, CommandError

from wiki.models import Pincode, City

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        total_pincodes = Pincode.objects.count()
        self.stdout.write("Total number of pincodes - %s\n" % total_pincodes)

        self.stdout.write("Remove currenty mappings...")
        Pincode.objects.all().update(mapped_city=None)
        self.stdout.write("DONE\n")

        pincodes = Pincode.objects.values_list('pincode', flat=True).distinct()
        for i, pinval in enumerate(pincodes):
            pincodes = Pincode.objects.filter(pincode=pinval)
            city = find_city(pincodes[0])
            pincodes.update(mapped_city=city)

            print i, pinval, city
            if i % 1000 == 0:
                self.stdout.write("%s %% complete\n" % int(i*100/len(pincodes)))
        self.stdout.write("Total number of pincodes not found - %s\n" % Pincode.objects.filter(mapped_city=None).values('pincode').distinct().count())
