from django.core.management.base import BaseCommand, CommandError

import csv
from wiki.models import Pincode, City, State
from putils import find_city

STATE_MAP = {
        'CHATTISGARH': 'chhattisgarh',
        'ODISHA': 'Orissa',
        'TAMIL NADU': 'Tamilnadu',
        'UTTARAKHAND': 'Uttaranchal',
        'ANDAMAN & NICOBAR ISLANDS': 'Andaman and Nicobar Islands',
    }

class Command(BaseCommand):
    
    def add_arguments(self, parser):
        parser.add_argument('filename', nargs='?', type=str)

    def handle(self, *args, **options):
        filename = options['filename']
        with open(filename) as f:
            reader = csv.reader(f)
            next(reader, None)
            count = 0
            for row in reader:
                if row[9] == "NULL":
                    continue
                if Pincode.objects.filter(pincode=row[1]).exists():
                    continue
                print "Adding pincode", row
                p = Pincode()
                p.pincode = int(row[1])
                p.state = State.objects.get(name__iexact=STATE_MAP.get(row[9], row[9]))
                p.region = row[5]
                p.city = row[8]
                p.locality = row[0]
                p.save()
                p.mapped_city = find_city(p)
                if p.mapped_city: p.save()
                
                count += 1

        self.stdout.write("Total number of pincodes added- %s\n" % count)
