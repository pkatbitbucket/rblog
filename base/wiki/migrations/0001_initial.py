# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import autoslug.fields
from django.conf import settings
import wiki.models
import tagging.fields
import customdb.thumbs
from django.contrib.postgres.operations import CreateExtension

class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
            CreateExtension("cube"),
            CreateExtension("earthdistance"),
            migrations.CreateModel(
            name='AgeLimit',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('minimum_entry_age', models.IntegerField(blank=True, null=True, verbose_name='Minimum Entry Age', choices=[(0, b'0 months'), (1, b'1 months'), (2, b'2 months'), (3, b'3 months'), (4, b'4 months'), (5, b'5 months'), (6, b'6 months'), (7, b'7 months'), (8, b'8 months'), (9, b'9 months'), (10, b'10 months'), (11, b'11 months'), (1000, b'1 years'), (2000, b'2 years'), (3000, b'3 years'), (4000, b'4 years'), (5000, b'5 years'), (6000, b'6 years'), (7000, b'7 years'), (8000, b'8 years'), (9000, b'9 years'), (10000, b'10 years'), (11000, b'11 years'), (12000, b'12 years'), (13000, b'13 years'), (14000, b'14 years'), (15000, b'15 years'), (16000, b'16 years'), (17000, b'17 years'), (18000, b'18 years'), (19000, b'19 years'), (20000, b'20 years'), (21000, b'21 years'), (22000, b'22 years'), (23000, b'23 years'), (24000, b'24 years'), (25000, b'25 years'), (26000, b'26 years'), (27000, b'27 years'), (28000, b'28 years'), (29000, b'29 years'), (30000, b'30 years'), (31000, b'31 years'), (32000, b'32 years'), (33000, b'33 years'), (34000, b'34 years'), (35000, b'35 years'), (36000, b'36 years'), (37000, b'37 years'), (38000, b'38 years'), (39000, b'39 years'), (40000, b'40 years'), (41000, b'41 years'), (42000, b'42 years'), (43000, b'43 years'), (
                    44000, b'44 years'), (45000, b'45 years'), (46000, b'46 years'), (47000, b'47 years'), (48000, b'48 years'), (49000, b'49 years'), (50000, b'50 years'), (51000, b'51 years'), (52000, b'52 years'), (53000, b'53 years'), (54000, b'54 years'), (55000, b'55 years'), (56000, b'56 years'), (57000, b'57 years'), (58000, b'58 years'), (59000, b'59 years'), (60000, b'60 years'), (61000, b'61 years'), (62000, b'62 years'), (63000, b'63 years'), (64000, b'64 years'), (65000, b'65 years'), (66000, b'66 years'), (67000, b'67 years'), (68000, b'68 years'), (69000, b'69 years'), (70000, b'70 years'), (71000, b'71 years'), (72000, b'72 years'), (73000, b'73 years'), (74000, b'74 years'), (75000, b'75 years'), (76000, b'76 years'), (77000, b'77 years'), (78000, b'78 years'), (79000, b'79 years'), (80000, b'80 years'), (81000, b'81 years'), (82000, b'82 years'), (83000, b'83 years'), (84000, b'84 years'), (85000, b'85 years'), (86000, b'86 years'), (87000, b'87 years'), (88000, b'88 years'), (89000, b'89 years'), (90000, b'90 years'), (91000, b'91 years'), (92000, b'92 years'), (93000, b'93 years'), (94000, b'94 years'), (95000, b'95 years'), (96000, b'96 years'), (97000, b'97 years'), (98000, b'98 years'), (99000, b'99 years'), (100000, b'100 years')])),
                ('maximum_entry_age', models.IntegerField(blank=True, null=True, verbose_name='Maximum Entry Age', choices=[(0, b'0 months'), (1, b'1 months'), (2, b'2 months'), (3, b'3 months'), (4, b'4 months'), (5, b'5 months'), (6, b'6 months'), (7, b'7 months'), (8, b'8 months'), (9, b'9 months'), (10, b'10 months'), (11, b'11 months'), (1000, b'1 years'), (2000, b'2 years'), (3000, b'3 years'), (4000, b'4 years'), (5000, b'5 years'), (6000, b'6 years'), (7000, b'7 years'), (8000, b'8 years'), (9000, b'9 years'), (10000, b'10 years'), (11000, b'11 years'), (12000, b'12 years'), (13000, b'13 years'), (14000, b'14 years'), (15000, b'15 years'), (16000, b'16 years'), (17000, b'17 years'), (18000, b'18 years'), (19000, b'19 years'), (20000, b'20 years'), (21000, b'21 years'), (22000, b'22 years'), (23000, b'23 years'), (24000, b'24 years'), (25000, b'25 years'), (26000, b'26 years'), (27000, b'27 years'), (28000, b'28 years'), (29000, b'29 years'), (30000, b'30 years'), (31000, b'31 years'), (32000, b'32 years'), (33000, b'33 years'), (34000, b'34 years'), (35000, b'35 years'), (36000, b'36 years'), (37000, b'37 years'), (38000, b'38 years'), (39000, b'39 years'), (40000, b'40 years'), (41000, b'41 years'), (42000, b'42 years'), (43000, b'43 years'), (
                    44000, b'44 years'), (45000, b'45 years'), (46000, b'46 years'), (47000, b'47 years'), (48000, b'48 years'), (49000, b'49 years'), (50000, b'50 years'), (51000, b'51 years'), (52000, b'52 years'), (53000, b'53 years'), (54000, b'54 years'), (55000, b'55 years'), (56000, b'56 years'), (57000, b'57 years'), (58000, b'58 years'), (59000, b'59 years'), (60000, b'60 years'), (61000, b'61 years'), (62000, b'62 years'), (63000, b'63 years'), (64000, b'64 years'), (65000, b'65 years'), (66000, b'66 years'), (67000, b'67 years'), (68000, b'68 years'), (69000, b'69 years'), (70000, b'70 years'), (71000, b'71 years'), (72000, b'72 years'), (73000, b'73 years'), (74000, b'74 years'), (75000, b'75 years'), (76000, b'76 years'), (77000, b'77 years'), (78000, b'78 years'), (79000, b'79 years'), (80000, b'80 years'), (81000, b'81 years'), (82000, b'82 years'), (83000, b'83 years'), (84000, b'84 years'), (85000, b'85 years'), (86000, b'86 years'), (87000, b'87 years'), (88000, b'88 years'), (89000, b'89 years'), (90000, b'90 years'), (91000, b'91 years'), (92000, b'92 years'), (93000, b'93 years'), (94000, b'94 years'), (95000, b'95 years'), (96000, b'96 years'), (97000, b'97 years'), (98000, b'98 years'), (99000, b'99 years'), (100000, b'100 years')])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AlternativeMedical',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='AlternativePracticesCoverage',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('limit_in_percentage', models.FloatField(null=True, blank=True)),
                ('limit', models.IntegerField(null=True, blank=True)),
                ('alternative_practices', models.ManyToManyField(
                    to='wiki.AlternativeMedical', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AmbulanceCharges',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('condition', models.CharField(blank=True, max_length=255, choices=[
                 (b'NO', b'No'), (b'EMERGENCY_ONLY', b'Available for emergency only'), (b'YES', b'Available for all cases')])),
                ('is_ambulance_services_covered', models.NullBooleanField(
                    verbose_name='Are 3rd party ambulance services covered', choices=[(False, b'No'), (True, b'Yes')])),
                ('limit_in_network_hospitals', models.IntegerField(
                    null=True, verbose_name='Limit on ambulance charges in network hospitals', blank=True)),
                ('limit_in_non_network_hospitals', models.IntegerField(
                    null=True, verbose_name='Limit on ambulance charges in non-network hospitals', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Bonus',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_available', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('unclaimed_years', models.IntegerField(
                    null=True, verbose_name='Unclaimed years after which bonus starts', blank=True)),
                ('bonus_amount', models.CharField(max_length=255,
                                                  verbose_name='Amount of bonus in % of SA e.g 10 or (60, 60, 10, 10, 10)', blank=True)),
                ('maximum_bonus_amount', models.FloatField(null=True,
                                                           verbose_name='Maximum bonus in % of SA e.g 50', blank=True)),
                ('maximum_years', models.IntegerField(
                    null=True, verbose_name='Maximum years after which bonus is not given', blank=True)),
                ('reduction_in_bonus_amount_after_claim', models.CharField(max_length=255,
                                                                           verbose_name='Reduction in bonus (as % of SA) after claim e.g. 20 or (60,60,10,10,10)', blank=True)),
                ('bonus_calculation_method', models.CharField(blank=True, max_length=100, choices=[
                 (b'CUMMULATIVE', b'Cummulative'), (b'ONBASE', b'On base SA')])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CancellationPolicy',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('policy_years', models.IntegerField(
                    verbose_name='Years for which premium has been paid')),
                ('period', models.IntegerField(
                    verbose_name='Upto the month you are cancelling policy')),
                ('refund_in_percentage', models.FloatField(
                    verbose_name='Refund in percentage of premium')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('is_enabled', models.BooleanField()),
                ('slug', autoslug.fields.AutoSlugField(editable=False)),
                ('is_major', models.BooleanField()),
                ('is_approved', models.BooleanField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CityAKA',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('city', models.ForeignKey(to='wiki.City')),
            ],
        ),
        migrations.CreateModel(
            name='Claims',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('service_promise', models.CharField(blank=True, max_length=255, choices=[(b'NO', b'No'), (b'TATC', b'TAT Commited'), (
                    b'TAT_COMMITED_WITH_PENALTY', b'TAT Commited with penalty payable by insurer if not adhered to')])),
                ('tat_commited', models.IntegerField(
                    null=True, verbose_name='TAT in days', blank=True)),
                ('tat_commited_penalty', models.TextField(
                    verbose_name='TAT penalty mentioned', blank=True)),
                ('intimation_for_cashless', models.IntegerField(
                    null=True, verbose_name='Intimation for cashless in hours (before hospitalization)', blank=True)),
                ('intimation_for_emergency', models.IntegerField(
                    null=True, verbose_name='Intimation of emergency in hours (after hospitalization)', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ClaimsData',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('in_business_since', models.IntegerField(
                    null=True, verbose_name='Company has been in business since year (e.g. 2007)', blank=True)),
                ('solvency_ratio', models.FloatField(null=True,
                                                     verbose_name='Solvency ratio (e.g. 1.77)', blank=True)),
                ('market_share', models.FloatField(
                    null=True, verbose_name='Market share in percentage e.g 18.23', blank=True)),
                ('growth_rate_of_market_share', models.FloatField(
                    null=True, verbose_name='Growth rate in market share e.g -13.2', blank=True)),
                ('rejection_ratio', models.FloatField(null=True,
                                                      verbose_name='Rejection ratio e.g. 6%', blank=True)),
                ('claims_response_time', models.FloatField(
                    null=True, verbose_name='Percentage of claims decision in 30 days e.g 93%', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ConvalescenceBenefit',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('condition', models.CharField(blank=True, max_length=255, choices=[
                 (b'NO', b'No'), (b'AVAILABLE_PER_DAY', b'Available for per day of hopistalization'), (b'AVAILABLE_LUMPSUM', b'Available lumpsum')])),
                ('limit', models.IntegerField(null=True,
                                              verbose_name='Maximum limit for the benefit', blank=True)),
                ('minimum_days_of_hospitalization', models.IntegerField(
                    null=True, verbose_name='Minimum days of hospitalization required to avail benefit', blank=True)),
                ('amount_per_day', models.IntegerField(
                    null=True, verbose_name='Amount paid for per day of hospitalization', blank=True)),
                ('maximum_number_of_days', models.IntegerField(
                    null=True, verbose_name='Maximum days for which amount will be paid if benefit available per day basis', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Copay',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_available', models.NullBooleanField(
                    verbose_name='Yes if ANY copay is present, Zonal or Room rent type', choices=[(False, b'No'), (True, b'Yes')])),
                ('applicable_after_age', models.IntegerField(blank=True, null=True, verbose_name='Co-pay is applicable after age, 0 if general copay', choices=[(0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10), (11, 11), (12, 12), (13, 13), (14, 14), (15, 15), (16, 16), (17, 17), (18, 18), (19, 19), (20, 20), (21, 21), (22, 22), (23, 23), (24, 24), (25, 25), (26, 26), (27, 27), (28, 28), (29, 29), (30, 30), (31, 31), (32, 32), (33, 33), (34, 34), (35, 35), (36, 36), (37, 37), (38, 38), (39, 39), (40, 40), (41, 41), (42, 42), (43, 43), (
                    44, 44), (45, 45), (46, 46), (47, 47), (48, 48), (49, 49), (50, 50), (51, 51), (52, 52), (53, 53), (54, 54), (55, 55), (56, 56), (57, 57), (58, 58), (59, 59), (60, 60), (61, 61), (62, 62), (63, 63), (64, 64), (65, 65), (66, 66), (67, 67), (68, 68), (69, 69), (70, 70), (71, 71), (72, 72), (73, 73), (74, 74), (75, 75), (76, 76), (77, 77), (78, 78), (79, 79), (80, 80), (81, 81), (82, 82), (83, 83), (84, 84), (85, 85), (86, 86), (87, 87), (88, 88), (89, 89), (90, 90), (91, 91), (92, 92), (93, 93), (94, 94), (95, 95), (96, 96), (97, 97), (98, 98), (99, 99), (100, 100)])),
                ('applicable_after_entry_age', models.IntegerField(blank=True, null=True, verbose_name='Co-pay applicable if entry age >=', choices=[(0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10), (11, 11), (12, 12), (13, 13), (14, 14), (15, 15), (16, 16), (17, 17), (18, 18), (19, 19), (20, 20), (21, 21), (22, 22), (23, 23), (24, 24), (25, 25), (26, 26), (27, 27), (28, 28), (29, 29), (30, 30), (31, 31), (32, 32), (33, 33), (34, 34), (35, 35), (36, 36), (37, 37), (38, 38), (39, 39), (40, 40), (41, 41), (42, 42), (43, 43), (44, 44), (
                    45, 45), (46, 46), (47, 47), (48, 48), (49, 49), (50, 50), (51, 51), (52, 52), (53, 53), (54, 54), (55, 55), (56, 56), (57, 57), (58, 58), (59, 59), (60, 60), (61, 61), (62, 62), (63, 63), (64, 64), (65, 65), (66, 66), (67, 67), (68, 68), (69, 69), (70, 70), (71, 71), (72, 72), (73, 73), (74, 74), (75, 75), (76, 76), (77, 77), (78, 78), (79, 79), (80, 80), (81, 81), (82, 82), (83, 83), (84, 84), (85, 85), (86, 86), (87, 87), (88, 88), (89, 89), (90, 90), (91, 91), (92, 92), (93, 93), (94, 94), (95, 95), (96, 96), (97, 97), (98, 98), (99, 99), (100, 100)])),
                ('age_copay_network_hospital', models.FloatField(
                    null=True, verbose_name='Age copay network hospital', blank=True)),
                ('age_copay_non_network_hospital', models.FloatField(
                    null=True, verbose_name='Age copay non-network hospital', blank=True)),
                ('for_network_hospital', models.FloatField(
                    null=True, verbose_name='Co-pay in network hospital (independent of age criteria)', blank=True)),
                ('for_non_network_hospital', models.FloatField(
                    null=True, verbose_name='Co-pay in non-network hospital (independent of age criteria)', blank=True)),
                ('on_day_care_for_network_hospital', models.IntegerField(
                    null=True, verbose_name='Co-pay on day care in network hospital', blank=True)),
                ('on_day_care_for_non_network_hospital', models.IntegerField(
                    null=True, verbose_name='Co-pay on day care in non-network hospital', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CoverPremiumTable',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('payment_period', models.IntegerField(default=1,
                                                       verbose_name='period', choices=[(1, 1), (2, 2)])),
                ('lower_age_limit', models.IntegerField(choices=[(0, b'0 months'), (1, b'1 months'), (2, b'2 months'), (3, b'3 months'), (4, b'4 months'), (5, b'5 months'), (6, b'6 months'), (7, b'7 months'), (8, b'8 months'), (9, b'9 months'), (10, b'10 months'), (11, b'11 months'), (1000, b'1 years'), (2000, b'2 years'), (3000, b'3 years'), (4000, b'4 years'), (5000, b'5 years'), (6000, b'6 years'), (7000, b'7 years'), (8000, b'8 years'), (9000, b'9 years'), (10000, b'10 years'), (11000, b'11 years'), (12000, b'12 years'), (13000, b'13 years'), (14000, b'14 years'), (15000, b'15 years'), (16000, b'16 years'), (17000, b'17 years'), (18000, b'18 years'), (19000, b'19 years'), (20000, b'20 years'), (21000, b'21 years'), (22000, b'22 years'), (23000, b'23 years'), (24000, b'24 years'), (25000, b'25 years'), (26000, b'26 years'), (27000, b'27 years'), (28000, b'28 years'), (29000, b'29 years'), (30000, b'30 years'), (31000, b'31 years'), (32000, b'32 years'), (33000, b'33 years'), (34000, b'34 years'), (35000, b'35 years'), (36000, b'36 years'), (37000, b'37 years'), (38000, b'38 years'), (39000, b'39 years'), (40000, b'40 years'), (41000, b'41 years'), (42000, b'42 years'), (43000, b'43 years'), (44000, b'44 years'), (
                    45000, b'45 years'), (46000, b'46 years'), (47000, b'47 years'), (48000, b'48 years'), (49000, b'49 years'), (50000, b'50 years'), (51000, b'51 years'), (52000, b'52 years'), (53000, b'53 years'), (54000, b'54 years'), (55000, b'55 years'), (56000, b'56 years'), (57000, b'57 years'), (58000, b'58 years'), (59000, b'59 years'), (60000, b'60 years'), (61000, b'61 years'), (62000, b'62 years'), (63000, b'63 years'), (64000, b'64 years'), (65000, b'65 years'), (66000, b'66 years'), (67000, b'67 years'), (68000, b'68 years'), (69000, b'69 years'), (70000, b'70 years'), (71000, b'71 years'), (72000, b'72 years'), (73000, b'73 years'), (74000, b'74 years'), (75000, b'75 years'), (76000, b'76 years'), (77000, b'77 years'), (78000, b'78 years'), (79000, b'79 years'), (80000, b'80 years'), (81000, b'81 years'), (82000, b'82 years'), (83000, b'83 years'), (84000, b'84 years'), (85000, b'85 years'), (86000, b'86 years'), (87000, b'87 years'), (88000, b'88 years'), (89000, b'89 years'), (90000, b'90 years'), (91000, b'91 years'), (92000, b'92 years'), (93000, b'93 years'), (94000, b'94 years'), (95000, b'95 years'), (96000, b'96 years'), (97000, b'97 years'), (98000, b'98 years'), (99000, b'99 years'), (100000, b'100 years')])),
                ('upper_age_limit', models.IntegerField(blank=True, null=True, choices=[(0, b'0 months'), (1, b'1 months'), (2, b'2 months'), (3, b'3 months'), (4, b'4 months'), (5, b'5 months'), (6, b'6 months'), (7, b'7 months'), (8, b'8 months'), (9, b'9 months'), (10, b'10 months'), (11, b'11 months'), (1000, b'1 years'), (2000, b'2 years'), (3000, b'3 years'), (4000, b'4 years'), (5000, b'5 years'), (6000, b'6 years'), (7000, b'7 years'), (8000, b'8 years'), (9000, b'9 years'), (10000, b'10 years'), (11000, b'11 years'), (12000, b'12 years'), (13000, b'13 years'), (14000, b'14 years'), (15000, b'15 years'), (16000, b'16 years'), (17000, b'17 years'), (18000, b'18 years'), (19000, b'19 years'), (20000, b'20 years'), (21000, b'21 years'), (22000, b'22 years'), (23000, b'23 years'), (24000, b'24 years'), (25000, b'25 years'), (26000, b'26 years'), (27000, b'27 years'), (28000, b'28 years'), (29000, b'29 years'), (30000, b'30 years'), (31000, b'31 years'), (32000, b'32 years'), (33000, b'33 years'), (34000, b'34 years'), (35000, b'35 years'), (36000, b'36 years'), (37000, b'37 years'), (38000, b'38 years'), (39000, b'39 years'), (40000, b'40 years'), (41000, b'41 years'), (42000, b'42 years'), (43000, b'43 years'), (44000, b'44 years'), (
                    45000, b'45 years'), (46000, b'46 years'), (47000, b'47 years'), (48000, b'48 years'), (49000, b'49 years'), (50000, b'50 years'), (51000, b'51 years'), (52000, b'52 years'), (53000, b'53 years'), (54000, b'54 years'), (55000, b'55 years'), (56000, b'56 years'), (57000, b'57 years'), (58000, b'58 years'), (59000, b'59 years'), (60000, b'60 years'), (61000, b'61 years'), (62000, b'62 years'), (63000, b'63 years'), (64000, b'64 years'), (65000, b'65 years'), (66000, b'66 years'), (67000, b'67 years'), (68000, b'68 years'), (69000, b'69 years'), (70000, b'70 years'), (71000, b'71 years'), (72000, b'72 years'), (73000, b'73 years'), (74000, b'74 years'), (75000, b'75 years'), (76000, b'76 years'), (77000, b'77 years'), (78000, b'78 years'), (79000, b'79 years'), (80000, b'80 years'), (81000, b'81 years'), (82000, b'82 years'), (83000, b'83 years'), (84000, b'84 years'), (85000, b'85 years'), (86000, b'86 years'), (87000, b'87 years'), (88000, b'88 years'), (89000, b'89 years'), (90000, b'90 years'), (91000, b'91 years'), (92000, b'92 years'), (93000, b'93 years'), (94000, b'94 years'), (95000, b'95 years'), (96000, b'96 years'), (97000, b'97 years'), (98000, b'98 years'), (99000, b'99 years'), (100000, b'100 years')])),
                ('gender', models.CharField(blank=True, max_length=100, null=True, verbose_name='gender', choices=[
                 (b'MALE', b'Male'), (b'FEMALE', b'Female'), (b'', b'Not Applicable')])),
                ('two_year_discount', models.FloatField(
                    null=True, verbose_name='2 year discount', blank=True)),
                ('base_premium', models.FloatField(null=True, blank=True)),
                ('service_tax', models.FloatField(null=True, blank=True)),
                ('premium', models.FloatField()),
                ('city', models.ManyToManyField(to='wiki.City', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CriticalIllnessCoverage',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_available', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('limit', models.IntegerField(null=True,
                                              verbose_name='CI cover as value (e.g. 500000)', blank=True)),
                ('limit_in_percentage', models.FloatField(
                    null=True, verbose_name='CI cover as % of sum assured (e.g. 80)', blank=True)),
                ('included_in_plan', models.NullBooleanField(verbose_name='Benefit condition', choices=[
                 (True, b'Included in SA'), (False, b'Over and above SA')])),
            ],
        ),
        migrations.CreateModel(
            name='DayCare',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_covered', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('defined_list', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('number_of_covered_procedures',
                 models.IntegerField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='DentalCoverage',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('condition', models.CharField(blank=True, max_length=255,
                                               choices=[(b'NO', b'No'), (b'YES', b'Yes')])),
                ('copay', models.FloatField(null=True, blank=True)),
                ('limit', models.IntegerField(
                    null=True, verbose_name='Maximum cover for dental treatment', blank=True)),
                ('waiting_period', models.IntegerField(null=True,
                                                       verbose_name='Waiting period in months', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='DomiciliaryHospitalization',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_covered', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('limit', models.IntegerField(
                    null=True, verbose_name='Limit', blank=True)),
                ('limit_in_percentage', models.FloatField(null=True,
                                                          verbose_name='Limit as percent of SA', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='EyeCoverage',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('condition', models.CharField(blank=True, max_length=255, choices=[
                 (b'NO', b'No'), (b'WITH_COPAY_WAITING_PERIOD', b'With Co-pay and Waiting Period'), (b'WITH_COPAY', b'With Copay')])),
                ('copay', models.FloatField(null=True, blank=True)),
                ('limit', models.IntegerField(null=True,
                                              verbose_name='Maximum cover for eye treatment', blank=True)),
                ('waiting_period', models.IntegerField(null=True,
                                                       verbose_name='Waiting period in months', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='FamilyCoverage',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('number_of_maximum_members', models.IntegerField(null=True,
                                                                  verbose_name='Maximum members for family cover', blank=True)),
                ('maximum_dependent_age', models.IntegerField(blank=True, null=True, verbose_name='Maximum dependent age for family cover', choices=[(0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10), (11, 11), (12, 12), (13, 13), (14, 14), (15, 15), (16, 16), (17, 17), (18, 18), (19, 19), (20, 20), (21, 21), (22, 22), (23, 23), (24, 24), (25, 25), (26, 26), (27, 27), (28, 28), (29, 29), (30, 30), (31, 31), (32, 32), (33, 33), (34, 34), (35, 35), (36, 36), (37, 37), (38, 38), (39, 39), (40, 40), (41, 41), (42, 42), (43, 43), (44, 44), (
                    45, 45), (46, 46), (47, 47), (48, 48), (49, 49), (50, 50), (51, 51), (52, 52), (53, 53), (54, 54), (55, 55), (56, 56), (57, 57), (58, 58), (59, 59), (60, 60), (61, 61), (62, 62), (63, 63), (64, 64), (65, 65), (66, 66), (67, 67), (68, 68), (69, 69), (70, 70), (71, 71), (72, 72), (73, 73), (74, 74), (75, 75), (76, 76), (77, 77), (78, 78), (79, 79), (80, 80), (81, 81), (82, 82), (83, 83), (84, 84), (85, 85), (86, 86), (87, 87), (88, 88), (89, 89), (90, 90), (91, 91), (92, 92), (93, 93), (94, 94), (95, 95), (96, 96), (97, 97), (98, 98), (99, 99), (100, 100)])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='FamilyCoverPremiumTable',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('payment_period', models.IntegerField(default=1,
                                                       verbose_name='period', choices=[(1, 1), (2, 2)])),
                ('adults', models.IntegerField(
                    null=True, verbose_name='Adults', blank=True)),
                ('kids', models.IntegerField(
                    null=True, verbose_name='Kids', blank=True)),
                ('lower_age_limit', models.IntegerField(choices=[(0, b'0 months'), (1, b'1 months'), (2, b'2 months'), (3, b'3 months'), (4, b'4 months'), (5, b'5 months'), (6, b'6 months'), (7, b'7 months'), (8, b'8 months'), (9, b'9 months'), (10, b'10 months'), (11, b'11 months'), (1000, b'1 years'), (2000, b'2 years'), (3000, b'3 years'), (4000, b'4 years'), (5000, b'5 years'), (6000, b'6 years'), (7000, b'7 years'), (8000, b'8 years'), (9000, b'9 years'), (10000, b'10 years'), (11000, b'11 years'), (12000, b'12 years'), (13000, b'13 years'), (14000, b'14 years'), (15000, b'15 years'), (16000, b'16 years'), (17000, b'17 years'), (18000, b'18 years'), (19000, b'19 years'), (20000, b'20 years'), (21000, b'21 years'), (22000, b'22 years'), (23000, b'23 years'), (24000, b'24 years'), (25000, b'25 years'), (26000, b'26 years'), (27000, b'27 years'), (28000, b'28 years'), (29000, b'29 years'), (30000, b'30 years'), (31000, b'31 years'), (32000, b'32 years'), (33000, b'33 years'), (34000, b'34 years'), (35000, b'35 years'), (36000, b'36 years'), (37000, b'37 years'), (38000, b'38 years'), (39000, b'39 years'), (40000, b'40 years'), (41000, b'41 years'), (42000, b'42 years'), (43000, b'43 years'), (44000, b'44 years'), (
                    45000, b'45 years'), (46000, b'46 years'), (47000, b'47 years'), (48000, b'48 years'), (49000, b'49 years'), (50000, b'50 years'), (51000, b'51 years'), (52000, b'52 years'), (53000, b'53 years'), (54000, b'54 years'), (55000, b'55 years'), (56000, b'56 years'), (57000, b'57 years'), (58000, b'58 years'), (59000, b'59 years'), (60000, b'60 years'), (61000, b'61 years'), (62000, b'62 years'), (63000, b'63 years'), (64000, b'64 years'), (65000, b'65 years'), (66000, b'66 years'), (67000, b'67 years'), (68000, b'68 years'), (69000, b'69 years'), (70000, b'70 years'), (71000, b'71 years'), (72000, b'72 years'), (73000, b'73 years'), (74000, b'74 years'), (75000, b'75 years'), (76000, b'76 years'), (77000, b'77 years'), (78000, b'78 years'), (79000, b'79 years'), (80000, b'80 years'), (81000, b'81 years'), (82000, b'82 years'), (83000, b'83 years'), (84000, b'84 years'), (85000, b'85 years'), (86000, b'86 years'), (87000, b'87 years'), (88000, b'88 years'), (89000, b'89 years'), (90000, b'90 years'), (91000, b'91 years'), (92000, b'92 years'), (93000, b'93 years'), (94000, b'94 years'), (95000, b'95 years'), (96000, b'96 years'), (97000, b'97 years'), (98000, b'98 years'), (99000, b'99 years'), (100000, b'100 years')])),
                ('upper_age_limit', models.IntegerField(blank=True, null=True, choices=[(0, b'0 months'), (1, b'1 months'), (2, b'2 months'), (3, b'3 months'), (4, b'4 months'), (5, b'5 months'), (6, b'6 months'), (7, b'7 months'), (8, b'8 months'), (9, b'9 months'), (10, b'10 months'), (11, b'11 months'), (1000, b'1 years'), (2000, b'2 years'), (3000, b'3 years'), (4000, b'4 years'), (5000, b'5 years'), (6000, b'6 years'), (7000, b'7 years'), (8000, b'8 years'), (9000, b'9 years'), (10000, b'10 years'), (11000, b'11 years'), (12000, b'12 years'), (13000, b'13 years'), (14000, b'14 years'), (15000, b'15 years'), (16000, b'16 years'), (17000, b'17 years'), (18000, b'18 years'), (19000, b'19 years'), (20000, b'20 years'), (21000, b'21 years'), (22000, b'22 years'), (23000, b'23 years'), (24000, b'24 years'), (25000, b'25 years'), (26000, b'26 years'), (27000, b'27 years'), (28000, b'28 years'), (29000, b'29 years'), (30000, b'30 years'), (31000, b'31 years'), (32000, b'32 years'), (33000, b'33 years'), (34000, b'34 years'), (35000, b'35 years'), (36000, b'36 years'), (37000, b'37 years'), (38000, b'38 years'), (39000, b'39 years'), (40000, b'40 years'), (41000, b'41 years'), (42000, b'42 years'), (43000, b'43 years'), (44000, b'44 years'), (
                    45000, b'45 years'), (46000, b'46 years'), (47000, b'47 years'), (48000, b'48 years'), (49000, b'49 years'), (50000, b'50 years'), (51000, b'51 years'), (52000, b'52 years'), (53000, b'53 years'), (54000, b'54 years'), (55000, b'55 years'), (56000, b'56 years'), (57000, b'57 years'), (58000, b'58 years'), (59000, b'59 years'), (60000, b'60 years'), (61000, b'61 years'), (62000, b'62 years'), (63000, b'63 years'), (64000, b'64 years'), (65000, b'65 years'), (66000, b'66 years'), (67000, b'67 years'), (68000, b'68 years'), (69000, b'69 years'), (70000, b'70 years'), (71000, b'71 years'), (72000, b'72 years'), (73000, b'73 years'), (74000, b'74 years'), (75000, b'75 years'), (76000, b'76 years'), (77000, b'77 years'), (78000, b'78 years'), (79000, b'79 years'), (80000, b'80 years'), (81000, b'81 years'), (82000, b'82 years'), (83000, b'83 years'), (84000, b'84 years'), (85000, b'85 years'), (86000, b'86 years'), (87000, b'87 years'), (88000, b'88 years'), (89000, b'89 years'), (90000, b'90 years'), (91000, b'91 years'), (92000, b'92 years'), (93000, b'93 years'), (94000, b'94 years'), (95000, b'95 years'), (96000, b'96 years'), (97000, b'97 years'), (98000, b'98 years'), (99000, b'99 years'), (100000, b'100 years')])),
                ('gender', models.CharField(blank=True, max_length=100, null=True, verbose_name='Gender', choices=[
                 (b'MALE', b'Male'), (b'FEMALE', b'Female'), (b'', b'Not Applicable')])),
                ('two_year_discount', models.FloatField(
                    null=True, verbose_name='2year discount', blank=True)),
                ('base_premium', models.FloatField(null=True, blank=True)),
                ('service_tax', models.FloatField(null=True, blank=True)),
                ('premium', models.FloatField()),
                ('city', models.ManyToManyField(to='wiki.City', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='General',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('waiting_period', models.IntegerField(
                    null=True, verbose_name='General waiting period except accidents (in months)', blank=True)),
                ('portability', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
            ],
        ),
        migrations.CreateModel(
            name='HealthCheckup',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_available', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('claim_free_years', models.IntegerField(
                    null=True, verbose_name='Claim free years after which free check-up is available', blank=True)),
                ('limit_in_percentage', models.FloatField(
                    null=True, verbose_name='Amount in percentage of sum assured, for check up', blank=True)),
                ('limit', models.IntegerField(null=True,
                                              verbose_name='Amount in INR for checkup', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='HealthProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name='title')),
                ('slug', models.SlugField(max_length=70, verbose_name='slug')),
                ('policy_type', models.CharField(max_length=255, verbose_name='Policy Type', choices=[
                 (b'INDIVIDUAL', b'Individual'), (b'FAMILY', b'Family')])),
                ('coverage_category', models.CharField(blank=True, max_length=255, verbose_name='Category of illness coverage', choices=[(b'CRITICAL_ILLNESS', b'Critical Illness'), (
                    b'FIXED_BENEFIT', b'Fixed Benefit'), (b'HEALTH_SAVINGS', b'Health Savings'), (b'INDEMNITY', b'Indemnity'), (b'SUPER_TOPUP', b'Super Topup'), (b'TOPUP', b'Topup')])),
                ('body', models.TextField(verbose_name='body')),
                ('claim_form', models.FileField(null=True,
                                                upload_to=wiki.models.get_file_path, blank=True)),
                ('logo', customdb.thumbs.ImageWithThumbsField(
                    null=True, upload_to=wiki.models.get_file_path, blank=True)),
                ('policy_wordings', models.FileField(null=True,
                                                     upload_to=wiki.models.get_file_path, blank=True)),
                ('brochure', models.FileField(null=True,
                                              upload_to=wiki.models.get_file_path, blank=True)),
                ('proposal_form', models.FileField(null=True,
                                                   upload_to=wiki.models.get_file_path, blank=True)),
                ('created', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
                ('modified', models.DateTimeField(
                    auto_now=True, verbose_name='modified')),
                ('notes', models.TextField(blank=True)),
                ('is_completed', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('is_backend_integrated', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('author', models.ForeignKey(blank=True,
                                             to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('-created',),
                'get_latest_by': 'created',
                'verbose_name': 'Health Product',
                'verbose_name_plural': 'Health Products',
            },
        ),
        migrations.CreateModel(
            name='Hospital',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('address', models.TextField()),
                ('pin', models.CharField(max_length=200)),
                ('district', models.CharField(max_length=200, blank=True)),
                ('landmark', models.CharField(max_length=200, null=True, blank=True)),
                ('std_code', models.CharField(max_length=10,
                                              null=True, verbose_name='STD Code', blank=True)),
                ('phone1', models.CharField(max_length=255,
                                            null=True, verbose_name='Phone', blank=True)),
                ('phone2', models.CharField(max_length=255, null=True, blank=True)),
                ('fax1', models.CharField(max_length=255, null=True, blank=True)),
                ('fax2', models.CharField(max_length=255, null=True, blank=True)),
                ('email', models.EmailField(max_length=254, blank=True)),
                ('url', models.CharField(max_length=200, blank=True)),
                ('lat', models.FloatField(null=True, blank=True)),
                ('lng', models.FloatField(null=True, blank=True)),
                ('googledata', models.TextField(blank=True)),
                ('city', models.ForeignKey(to='wiki.City')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Insurer',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name='title')),
                ('seo_title', models.CharField(default=b'',
                                               max_length=200, verbose_name='SEO title')),
                ('slug', models.SlugField(max_length=70, verbose_name='slug')),
                ('body', models.TextField(verbose_name='body')),
                ('tease', models.TextField(
                    help_text='Concise text suggested. Does not appear in RSS feed.', verbose_name='tease', blank=True)),
                ('logo', customdb.thumbs.ImageWithThumbsField(
                    null=True, upload_to=wiki.models.get_file_path, blank=True)),
                ('status', models.IntegerField(
                    default=1, verbose_name='status', choices=[(1, 'Draft'), (2, 'Public')])),
                ('created', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
                ('modified', models.DateTimeField(
                    auto_now=True, verbose_name='modified')),
                ('author', models.ForeignKey(related_name='insurer_author',
                                             blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('claims_data', models.ForeignKey(
                    blank=True, to='wiki.ClaimsData', null=True)),
                ('insurer_manager', models.ForeignKey(related_name='insurer_manager',
                                                      blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('-created',),
                'get_latest_by': 'created',
                'verbose_name': 'insurer',
                'verbose_name_plural': 'insurers',
            },
        ),
        migrations.CreateModel(
            name='LifeLongRenewability',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('condition', models.CharField(blank=True, max_length=255, choices=[(b'NO', b'Not Renewable, Maximum age limit applicable to renew'), (
                    b'AGE_LIMIT_ON_INDEMNITY_BENEFIT', b'Subject to age limit for indemnity benefit'), (b'AGE_LIMIT_ON_CRITICAL_ILLNESS', b'Subject to age limit on Critical illness'), (b'YES', b'Yes - Lifelong Renewable')])),
                ('age_limit', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MaternityCover',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_covered', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('waiting_period', models.IntegerField(null=True,
                                                       verbose_name='Waiting period(in months)', blank=True)),
                ('limit_for_normal_delivery',
                 models.IntegerField(null=True, blank=True)),
                ('limit_for_cesarean_delivery',
                 models.IntegerField(null=True, blank=True)),
                ('delivery_limit', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MedicalRequired',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_medical_required', models.NullBooleanField(
                    verbose_name='Is medical required for this slab', choices=[(False, b'No'), (True, b'Yes')])),
                ('maximum_age', models.IntegerField(blank=True, null=True, verbose_name='Age above which medicals are required', choices=[(0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10), (11, 11), (12, 12), (13, 13), (14, 14), (15, 15), (16, 16), (17, 17), (18, 18), (19, 19), (20, 20), (21, 21), (22, 22), (23, 23), (24, 24), (25, 25), (26, 26), (27, 27), (28, 28), (29, 29), (30, 30), (31, 31), (32, 32), (33, 33), (34, 34), (35, 35), (36, 36), (37, 37), (38, 38), (39, 39), (40, 40), (41, 41), (42, 42), (43, 43), (44, 44), (45, 45), (
                    46, 46), (47, 47), (48, 48), (49, 49), (50, 50), (51, 51), (52, 52), (53, 53), (54, 54), (55, 55), (56, 56), (57, 57), (58, 58), (59, 59), (60, 60), (61, 61), (62, 62), (63, 63), (64, 64), (65, 65), (66, 66), (67, 67), (68, 68), (69, 69), (70, 70), (71, 71), (72, 72), (73, 73), (74, 74), (75, 75), (76, 76), (77, 77), (78, 78), (79, 79), (80, 80), (81, 81), (82, 82), (83, 83), (84, 84), (85, 85), (86, 86), (87, 87), (88, 88), (89, 89), (90, 90), (91, 91), (92, 92), (93, 93), (94, 94), (95, 95), (96, 96), (97, 97), (98, 98), (99, 99), (100, 100)])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MedicalScore',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('general_waiting_period', models.FloatField(
                    null=True, verbose_name='Initial waiting period for diseases', blank=True)),
                ('specific_waiting_period', models.FloatField(
                    null=True, verbose_name='Two year waiting period', blank=True)),
                ('disease_condition', models.FloatField(null=True,
                                                        verbose_name='Other conditions on disease', blank=True)),
                ('ped_waiting_period', models.FloatField(null=True,
                                                         verbose_name='PED initial waiting period', blank=True)),
                ('specific_ped_period', models.FloatField(null=True,
                                                          verbose_name='Other PED waiting period', blank=True)),
                ('ped_condition', models.FloatField(null=True,
                                                    verbose_name='Other conditions on PED', blank=True)),
                ('standard_exclusion', models.FloatField(null=True,
                                                         verbose_name='Standard Exclusions', blank=True)),
                ('critical_illness_coverage', models.FloatField(
                    null=True, verbose_name='Critical Illness', blank=True)),
                ('day_care', models.FloatField(
                    null=True, verbose_name='Day Care', blank=True)),
                ('domiciliary_hospitalization',
                 models.FloatField(null=True, blank=True)),
                ('dental_coverage', models.FloatField(null=True, blank=True)),
                ('eye_coverage', models.FloatField(null=True, blank=True)),
                ('health_product', models.ForeignKey(
                    related_name='medical_score', to='wiki.HealthProduct')),
            ],
        ),
        migrations.CreateModel(
            name='MetaData',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('code', models.TextField(blank=True)),
                ('verbose', models.TextField(verbose_name='Verbose', blank=True)),
                ('text_to_display', models.TextField(
                    verbose_name='Text to display', blank=True)),
                ('comments', models.TextField(verbose_name='Comments', blank=True)),
                ('is_completed', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='NetworkHospitalDailyCash',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_available', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('condition', models.CharField(blank=True, max_length=255, choices=[
                 (b'NO', b'No'), (b'AVAILABLE_WITH_SHARING_ONLY', b'Available if you choose sharing room'), (b'YESWL', b'Yes - With Limits')])),
                ('is_icu_available', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('limit_per_day_allowance_icu', models.IntegerField(
                    null=True, verbose_name='Per day allowance for ICU', blank=True)),
                ('limit_per_day_allowance', models.IntegerField(
                    null=True, verbose_name='Per day allowance', blank=True)),
                ('limit_cummulative_allowance', models.IntegerField(
                    null=True, verbose_name='Cummulative allowance limit', blank=True)),
                ('from_day', models.IntegerField(
                    null=True, verbose_name='From the day allowance is applicable', blank=True)),
                ('to_day', models.IntegerField(
                    null=True, verbose_name='To the day allowance is applicable', blank=True)),
                ('number_of_days', models.IntegerField(
                    null=True, verbose_name='Number of days limit on allowance', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='NonNetworkHospitalDailyCash',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_available', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('condition', models.CharField(blank=True, max_length=255, choices=[
                 (b'NO', b'No'), (b'AVAILABLE_WITH_SHARING_ONLY', b'Available if you choose sharing room'), (b'YESWL', b'Yes - With Limits')])),
                ('is_icu_available', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('limit_per_day_allowance_icu', models.IntegerField(
                    null=True, verbose_name='Per day allowance for ICU', blank=True)),
                ('limit_per_day_allowance', models.IntegerField(
                    null=True, verbose_name='Per day allowance', blank=True)),
                ('limit_cummulative_allowance', models.IntegerField(
                    null=True, verbose_name='Cummulative allowance limit', blank=True)),
                ('from_day', models.IntegerField(
                    null=True, verbose_name='From the day allowance is applicable', blank=True)),
                ('to_day', models.IntegerField(
                    null=True, verbose_name='To the day allowance is applicable', blank=True)),
                ('number_of_days', models.IntegerField(
                    null=True, verbose_name='Number of days limit on allowance', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='OnlineAvailability',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_available_online', models.NullBooleanField(
                    verbose_name='Is slab available online(for all ages)', choices=[(False, b'No'), (True, b'Yes')])),
                ('maximum_age', models.IntegerField(blank=True, null=True, verbose_name='Age above which this slab is only available offline', choices=[(0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10), (11, 11), (12, 12), (13, 13), (14, 14), (15, 15), (16, 16), (17, 17), (18, 18), (19, 19), (20, 20), (21, 21), (22, 22), (23, 23), (24, 24), (25, 25), (26, 26), (27, 27), (28, 28), (29, 29), (30, 30), (31, 31), (32, 32), (33, 33), (34, 34), (35, 35), (36, 36), (37, 37), (38, 38), (39, 39), (40, 40), (41, 41), (42, 42), (43, 43), (44, 44), (
                    45, 45), (46, 46), (47, 47), (48, 48), (49, 49), (50, 50), (51, 51), (52, 52), (53, 53), (54, 54), (55, 55), (56, 56), (57, 57), (58, 58), (59, 59), (60, 60), (61, 61), (62, 62), (63, 63), (64, 64), (65, 65), (66, 66), (67, 67), (68, 68), (69, 69), (70, 70), (71, 71), (72, 72), (73, 73), (74, 74), (75, 75), (76, 76), (77, 77), (78, 78), (79, 79), (80, 80), (81, 81), (82, 82), (83, 83), (84, 84), (85, 85), (86, 86), (87, 87), (88, 88), (89, 89), (90, 90), (91, 91), (92, 92), (93, 93), (94, 94), (95, 95), (96, 96), (97, 97), (98, 98), (99, 99), (100, 100)])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='OrganDonor',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_covered', models.NullBooleanField(
                    verbose_name='Is covered', choices=[(False, b'No'), (True, b'Yes')])),
                ('limit', models.IntegerField(
                    null=True, verbose_name='Limit', blank=True)),
                ('limit_in_percentage', models.FloatField(null=True,
                                                          verbose_name='Limit in percentage of SA', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='OutpatientBenefits',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_covered', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('number_of_consultations',
                 models.IntegerField(null=True, blank=True)),
                ('limit', models.IntegerField(null=True,
                                              verbose_name='Maximum limit on OPD cover', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Pincode',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('pincode', models.IntegerField()),
                ('lat', models.FloatField(null=True, blank=True)),
                ('lng', models.FloatField(null=True, blank=True)),
                ('region', models.CharField(max_length=255)),
                ('city', models.CharField(max_length=255)),
                ('area', models.CharField(max_length=255)),
                ('locality', models.CharField(max_length=255)),
                ('is_metro', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='PreExisting',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('waiting_period', models.IntegerField(
                    null=True, verbose_name='General waiting period for pre-existing conditions (in months)', blank=True)),
                ('portability', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
            ],
        ),
        migrations.CreateModel(
            name='PremiumDiscount',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_available', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('unclaimed_years', models.IntegerField(
                    null=True, verbose_name='Unclaimed years after which premium discount starts', blank=True)),
                ('discount_amount', models.FloatField(null=True,
                                                      verbose_name='Discount in % of premium e.g 5', blank=True)),
                ('maximum_discount_amount', models.FloatField(null=True,
                                                              verbose_name='Maximum discount in % e.g 25', blank=True)),
                ('maximum_years', models.IntegerField(
                    null=True, verbose_name='Maximum years after which discount is not given', blank=True)),
                ('reduction_in_discount_amount_after_claim', models.FloatField(
                    null=True, verbose_name='Reduction in discount after claim e.g. 5%, 100% means no discount if claimed', blank=True)),
                ('discount_calculation_method', models.CharField(blank=True, max_length=100, choices=[
                 (b'CUMMULATIVE', b'Cummulative'), (b'ONBASE', b'On base SA')])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PrePostHospitalization',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_available', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('intimation_time', models.IntegerField(null=True, blank=True)),
                ('days_pre_hospitalization',
                 models.IntegerField(null=True, blank=True)),
                ('days_post_hospitalization',
                 models.IntegerField(null=True, blank=True)),
                ('days_pre_hospitalization_with_intimation',
                 models.IntegerField(null=True, blank=True)),
                ('days_post_hospitalization_with_intimation',
                 models.IntegerField(null=True, blank=True)),
                ('cummulative_limit', models.IntegerField(
                    null=True, verbose_name='Limit', blank=True)),
                ('cummulative_limit_in_percentage', models.FloatField(
                    null=True, verbose_name='Limit in percentage', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ProcedureCovered',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('verbose', models.TextField(blank=True)),
                ('code', models.TextField(blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ProductMetaData',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('code', models.TextField(blank=True)),
                ('verbose', models.TextField(verbose_name='Verbose', blank=True)),
                ('text_to_display', models.TextField(
                    verbose_name='Text to display', blank=True)),
                ('comments', models.TextField(verbose_name='Comments', blank=True)),
                ('is_completed', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('health_product', models.ForeignKey(to='wiki.HealthProduct')),
            ],
        ),
        migrations.CreateModel(
            name='ProductSection',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=250, choices=[(b'pre_text', b'Block before auto post'), (b'post_text', b'Block after auto post'), (b'cancellation_policy', b'Cancellation Policy'), (b'dental_coverage', b'Dental Coverage'), (b'room_type_copay', b'Room Type Limits & Copay'), (b'general', b'Waiting Period and Disease Limits'), (b'special_feature', b'Special Features'), (b'network_hospital_daily_cash', b'Network Hospital Daily Cash'), (b'restore_benefits', b'Restore Benefits'), (b'premium_discount', b'Premium Discount'), (b'standard_exclusion', b'Standard Exclusion'), (b'ambulance_charges', b'Ambulance Charges'), (b'maternity_cover', b'Maternity Cover'), (b'eye_coverage', b'Eye Coverage'), (b'zonal_copay', b'Zonal Copay'), (b'alternative_practices_coverage', b'Alternative Medical Coverage'), (b'online_availability', b'Online Available'), (b'zonal_room_rent', b'Room Rent'), (
                    b'day_care', b'Day Care'), (b'age_limit', b'Age Limit'), (b'family_cover_premium_table', b'Cover Premium Table'), (b'health_checkup', b'Health Checkup'), (b'bonus', b'Bonus'), (b'family_coverage', b'Family Coverage'), (b'convalescence_benefit', b'Convalescence Benefit'), (b'non_network_hospital_daily_cash', b'Non Network Hospital Daily Cash'), (b'medical_required', b'Medical Required'), (b'pre_existing', b'Pre-existing'), (b'pre_post_hospitalization', b'Pre Post Hospitalization'), (b'copay', b'Copay'), (b'cover_premium_table', b'Cover Premium Table'), (b'domiciliary_hospitalization', b'Domiciliary Hospitalization'), (b'claims', b'Claims'), (b'life_long_renewability', b'Life Long Renewability'), (b'outpatient_benefits', b'Outpatient Benefits'), (b'critical_illness_coverage', b'Critical Illness Coverage'), (b'organ_donor', b'Organ Donor')])),
                ('body', models.TextField(blank=True)),
                ('is_active', models.BooleanField(default=True)),
                ('created_on', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
                ('updated_on', models.DateTimeField(
                    auto_now=True, verbose_name='modified')),
            ],
        ),
        migrations.CreateModel(
            name='RestoreBenefits',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('is_available', models.NullBooleanField(
                    choices=[(False, b'No'), (True, b'Yes')])),
                ('amount_restored', models.IntegerField(null=True,
                                                        verbose_name='Amount restored in value', blank=True)),
                ('amount_restored_percentage', models.FloatField(
                    null=True, verbose_name='Amount restored in % of SA', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RoomTypeCopay',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('room_type', models.CharField(blank=True, max_length=255, choices=[(b'CLASSA', b'Single pvt. room without conditions'), (
                    b'CLASSB', b'Lowest cost single pvt. room + 1'), (b'CLASSC', b'Lowest cost single pvt. room'), (b'CLASSD', b'Shared Room Only')])),
                ('verbose_room_type', models.CharField(max_length=255,
                                                       verbose_name='Room as mentioned in policy', blank=True)),
                ('copay', models.FloatField(verbose_name='Copay condition')),
                ('applicable_for_hospital', models.CharField(max_length=100, verbose_name='Applicable for hospital in', choices=[
                 (b'BOTH', b'Network and non-network both'), (b'NETWORK', b'Network Hospital'), (b'NONNETWORK', b'Non-Network Hospital')])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Slab',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('sum_assured', models.PositiveIntegerField()),
                ('deductible', models.PositiveIntegerField(default=0)),
                ('grade', models.CharField(max_length=255,
                                           verbose_name='Grade of the plan if application (e.g silver, gold, premium)', blank=True)),
                ('waiting_period_at_inception_of_ci', models.IntegerField(
                    null=True, verbose_name='Waiting period after which Critical Illness is covered', blank=True)),
                ('survival_period_for_ci_after_diagnosis', models.IntegerField(
                    null=True, verbose_name='Survival period after diagnosis, only after which you get cover', blank=True)),
                ('age_limit', models.ForeignKey(related_name='slabs',
                                                blank=True, to='wiki.AgeLimit', null=True)),
                ('alternative_practices_coverage', models.ForeignKey(
                    related_name='slabs', blank=True, to='wiki.AlternativePracticesCoverage', null=True)),
                ('ambulance_charges', models.ForeignKey(related_name='slabs',
                                                        blank=True, to='wiki.AmbulanceCharges', null=True)),
                ('bonus', models.ForeignKey(related_name='slabs',
                                            blank=True, to='wiki.Bonus', null=True)),
                ('claims', models.ForeignKey(related_name='slabs',
                                             blank=True, to='wiki.Claims', null=True)),
                ('convalescence_benefit', models.ForeignKey(
                    related_name='slabs', blank=True, to='wiki.ConvalescenceBenefit', null=True)),
                ('copay', models.ForeignKey(related_name='slabs',
                                            blank=True, to='wiki.Copay', null=True)),
                ('critical_illness_coverage', models.ForeignKey(
                    related_name='slabs', blank=True, to='wiki.CriticalIllnessCoverage', null=True)),
                ('day_care', models.ForeignKey(related_name='slabs',
                                               blank=True, to='wiki.DayCare', null=True)),
                ('dental_coverage', models.ForeignKey(related_name='slabs',
                                                      blank=True, to='wiki.DentalCoverage', null=True)),
                ('domiciliary_hospitalization', models.ForeignKey(
                    related_name='slabs', blank=True, to='wiki.DomiciliaryHospitalization', null=True)),
                ('eye_coverage', models.ForeignKey(related_name='slabs',
                                                   blank=True, to='wiki.EyeCoverage', null=True)),
                ('family_coverage', models.ForeignKey(related_name='slabs',
                                                      blank=True, to='wiki.FamilyCoverage', null=True)),
                ('general', models.ForeignKey(related_name='slabs',
                                              blank=True, to='wiki.General', null=True)),
                ('health_checkup', models.ForeignKey(related_name='slabs',
                                                     blank=True, to='wiki.HealthCheckup', null=True)),
                ('health_product', models.ForeignKey(
                    related_name='slabs', to='wiki.HealthProduct')),
                ('life_long_renewability', models.ForeignKey(
                    related_name='slabs', blank=True, to='wiki.LifeLongRenewability', null=True)),
                ('maternity_cover', models.ForeignKey(related_name='slabs',
                                                      blank=True, to='wiki.MaternityCover', null=True)),
                ('medical_required', models.ForeignKey(related_name='slabs',
                                                       blank=True, to='wiki.MedicalRequired', null=True)),
                ('network_hospital_daily_cash', models.ForeignKey(
                    related_name='slabs', blank=True, to='wiki.NetworkHospitalDailyCash', null=True)),
                ('non_network_hospital_daily_cash', models.ForeignKey(
                    related_name='slabs', blank=True, to='wiki.NonNetworkHospitalDailyCash', null=True)),
                ('online_availability', models.ForeignKey(related_name='slabs',
                                                          blank=True, to='wiki.OnlineAvailability', null=True)),
                ('organ_donor', models.ForeignKey(related_name='slabs',
                                                  blank=True, to='wiki.OrganDonor', null=True)),
                ('outpatient_benefits', models.ForeignKey(related_name='slabs',
                                                          blank=True, to='wiki.OutpatientBenefits', null=True)),
                ('pre_existing', models.ForeignKey(related_name='slabs',
                                                   blank=True, to='wiki.PreExisting', null=True)),
                ('pre_post_hospitalization', models.ForeignKey(
                    related_name='slabs', blank=True, to='wiki.PrePostHospitalization', null=True)),
                ('premium_discount', models.ForeignKey(related_name='slabs',
                                                       blank=True, to='wiki.PremiumDiscount', null=True)),
                ('restore_benefits', models.ForeignKey(related_name='slabs',
                                                       blank=True, to='wiki.RestoreBenefits', null=True)),
            ],
            options={
                'ordering': ['deductible', 'sum_assured', 'grade'],
            },
        ),
        migrations.CreateModel(
            name='SpecialFeature',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('feature', models.TextField(blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='StandardExclusion',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('is_enabled', models.BooleanField()),
                ('slug', autoslug.fields.AutoSlugField(
                    unique=True, editable=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='StateAKA',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('state', models.ForeignKey(to='wiki.State')),
            ],
        ),
        migrations.CreateModel(
            name='Term',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255)),
                ('description', models.TextField(blank=True)),
                ('term_type', models.CharField(blank=True, max_length=100, null=True, verbose_name='Term Type', choices=[
                 (b'DISEASE', b'DISEASE'), (b'TREATMENT', b'TREATMENT'), (b'PROCEDURE', b'PROCEDURE'), (b'CONDITION', b'CONDITION')])),
                ('ib_standard_exclusions', models.BooleanField(default=False)),
                ('ib_critical_illness', models.BooleanField(default=False)),
                ('ib_general', models.BooleanField(default=False)),
                ('ib_pre_existing', models.BooleanField(default=False)),
                ('ib_domiciliary', models.BooleanField(default=False)),
                ('ib_dental_coverage', models.BooleanField(default=False)),
                ('ib_eye_coverage', models.BooleanField(default=False)),
                ('ib_day_care', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='TermParent',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255)),
                ('is_enabled', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='WaitingPeriodGeneral',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('period', models.IntegerField(null=True,
                                               verbose_name='Waiting period (months)', blank=True)),
                ('limit', models.IntegerField(null=True,
                                              verbose_name='Limit by value (INR)', blank=True)),
                ('limit_in_percentage', models.FloatField(null=True,
                                                          verbose_name='Limit in % of sum assured', blank=True)),
                ('copay', models.FloatField(null=True,
                                            verbose_name='Copay applicable for disease', blank=True)),
                ('verbose', models.TextField(blank=True)),
                ('code', models.TextField(blank=True)),
                ('term', models.ForeignKey(to='wiki.Term')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='WaitingPeriodPreExisting',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('period', models.IntegerField(null=True,
                                               verbose_name='Waiting period (months)', blank=True)),
                ('limit', models.IntegerField(null=True,
                                              verbose_name='Limit by value (INR)', blank=True)),
                ('limit_in_percentage', models.FloatField(null=True,
                                                          verbose_name='Limit in % of sum assured', blank=True)),
                ('copay', models.FloatField(null=True,
                                            verbose_name='Copay applicable for disease', blank=True)),
                ('verbose', models.TextField(blank=True)),
                ('code', models.TextField(blank=True)),
                ('term', models.ForeignKey(to='wiki.Term')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='WikiArticle',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name='title')),
                ('slug', models.SlugField(max_length=250, verbose_name='slug')),
                ('category', models.CharField(max_length=250, choices=[
                 (b'PRODUCT_REVIEW', b'Product Review'), (b'COMPANY_PAGE', b'Company Page')])),
                ('body', models.TextField(verbose_name='body', blank=True)),
                ('summary', models.TextField(verbose_name='summary', blank=True)),
                ('tease', models.TextField(
                    help_text='Concise text suggested. Does not appear in RSS feed.', verbose_name='tease', blank=True)),
                ('status', models.IntegerField(
                    default=1, verbose_name='status', choices=[(1, 'Draft'), (2, 'Public')])),
                ('keywords', models.TextField(
                    help_text='List of keywords, relevant for SEO', verbose_name='keywords', blank=True)),
                ('logo', customdb.thumbs.ImageWithThumbsField(
                    null=True, upload_to=wiki.models.get_file_path, blank=True)),
                ('pdf', models.FileField(null=True,
                                         upload_to=wiki.models.get_file_path, blank=True)),
                ('created', models.DateTimeField(
                    auto_now_add=True, verbose_name='created')),
                ('modified', models.DateTimeField(
                    auto_now=True, verbose_name='modified')),
                ('tags', tagging.fields.TagField(max_length=255, blank=True)),
                ('author', models.ForeignKey(related_name='article_author',
                                             blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('health_product', models.ManyToManyField(
                    to='wiki.HealthProduct', blank=True)),
                ('insurer', models.ManyToManyField(to='wiki.Insurer', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ZonalCopay',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('zonal_copay_for_network_hospital', models.FloatField(
                    null=True, verbose_name='Zonal Co-pay in network hospital', blank=True)),
                ('zonal_copay_for_non_network_hospital', models.FloatField(
                    null=True, verbose_name='Zonal Co-pay in network hospital', blank=True)),
                ('city', models.ManyToManyField(to='wiki.City', blank=True)),
                ('slabs', models.ManyToManyField(
                    related_name='zonal_copay', to='wiki.Slab')),
                ('state', models.ManyToManyField(to='wiki.State', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ZonalRoomRent',
            fields=[
                ('id', models.AutoField(verbose_name='ID',
                                        serialize=False, auto_created=True, primary_key=True)),
                ('conditions', models.CharField(blank=True, max_length=255, choices=[(b'NO_LIMIT', b'No Limit'), (b'SA_OR_VALUE_LIMIT', b'Limits on SA or value'), (
                    b'CLASSA', b'Single pvt. room without conditions'), (b'CLASSB', b'Lowest cost single pvt. room + 1'), (b'CLASSC', b'Lowest cost single pvt. room'), (b'CLASSD', b'Shared Room Only')])),
                ('room_available', models.CharField(max_length=255,
                                                    verbose_name='Room as mentioned in policy', blank=True)),
                ('limit_daily', models.IntegerField(
                    null=True, verbose_name='Daily room rent limit on sum assured in INR', blank=True)),
                ('limit_daily_in_percentage', models.FloatField(
                    null=True, verbose_name='Daily room rent limit as % of sum assured', blank=True)),
                ('limit_daily_in_icu', models.IntegerField(null=True,
                                                           verbose_name='ICU daily room rent limit in INR', blank=True)),
                ('limit_daily_in_percentage_in_icu', models.FloatField(
                    null=True, verbose_name='ICU daily room rent limit as % of sum assured', blank=True)),
                ('limit_maximum_total', models.IntegerField(
                    null=True, verbose_name='Maximum limit on total room rent benefit', blank=True)),
                ('limit_maximum_total_in_icu', models.IntegerField(
                    null=True, verbose_name='ICU maximum limit on total room rent benefit', blank=True)),
                ('city', models.ManyToManyField(to='wiki.City', blank=True)),
                ('slabs', models.ManyToManyField(
                    related_name='zonal_room_rent', to='wiki.Slab')),
                ('state', models.ManyToManyField(to='wiki.State', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='term',
            name='parent',
            field=models.ForeignKey(
                blank=True, to='wiki.TermParent', null=True),
        ),
        migrations.AddField(
            model_name='standardexclusion',
            name='terms',
            field=models.ManyToManyField(to='wiki.Term', blank=True),
        ),
        migrations.AddField(
            model_name='slab',
            name='special_feature',
            field=models.ForeignKey(
                related_name='slabs', blank=True, to='wiki.SpecialFeature', null=True),
        ),
        migrations.AddField(
            model_name='slab',
            name='standard_exclusion',
            field=models.ForeignKey(
                related_name='slabs', blank=True, to='wiki.StandardExclusion', null=True),
        ),
        migrations.AddField(
            model_name='roomtypecopay',
            name='slabs',
            field=models.ManyToManyField(
                related_name='room_type_copay', to='wiki.Slab'),
        ),
        migrations.AddField(
            model_name='productsection',
            name='product_page',
            field=models.ForeignKey(
                related_name='section_set', to='wiki.WikiArticle'),
        ),
        migrations.AddField(
            model_name='procedurecovered',
            name='term',
            field=models.ForeignKey(to='wiki.Term'),
        ),
        migrations.AddField(
            model_name='preexisting',
            name='conditions',
            field=models.ManyToManyField(
                to='wiki.WaitingPeriodPreExisting', blank=True),
        ),
        migrations.AddField(
            model_name='pincode',
            name='state',
            field=models.ForeignKey(to='wiki.State'),
        ),
        migrations.AddField(
            model_name='metadata',
            name='slabs',
            field=models.ManyToManyField(to='wiki.Slab'),
        ),
        migrations.AddField(
            model_name='medicalscore',
            name='slabs',
            field=models.ManyToManyField(
                related_name='medical_score', to='wiki.Slab'),
        ),
        migrations.AddField(
            model_name='hospital',
            name='insurers',
            field=models.ManyToManyField(to='wiki.Insurer', blank=True),
        ),
        migrations.AddField(
            model_name='hospital',
            name='state',
            field=models.ForeignKey(to='wiki.State'),
        ),
        migrations.AddField(
            model_name='healthproduct',
            name='insurer',
            field=models.ForeignKey(to='wiki.Insurer'),
        ),
        migrations.AddField(
            model_name='healthproduct',
            name='rider_products',
            field=models.ManyToManyField(to='wiki.HealthProduct', blank=True),
        ),
        migrations.AddField(
            model_name='general',
            name='conditions',
            field=models.ManyToManyField(
                to='wiki.WaitingPeriodGeneral', blank=True),
        ),
        migrations.AddField(
            model_name='familycoverpremiumtable',
            name='slabs',
            field=models.ManyToManyField(
                related_name='family_cover_premium_table', to='wiki.Slab'),
        ),
        migrations.AddField(
            model_name='familycoverpremiumtable',
            name='state',
            field=models.ManyToManyField(to='wiki.State', blank=True),
        ),
        migrations.AddField(
            model_name='eyecoverage',
            name='exclusions',
            field=models.ManyToManyField(to='wiki.Term', blank=True),
        ),
        migrations.AddField(
            model_name='domiciliaryhospitalization',
            name='exclusions',
            field=models.ManyToManyField(to='wiki.Term', blank=True),
        ),
        migrations.AddField(
            model_name='dentalcoverage',
            name='exclusions',
            field=models.ManyToManyField(to='wiki.Term', blank=True),
        ),
        migrations.AddField(
            model_name='daycare',
            name='procedure_covered',
            field=models.ManyToManyField(
                to='wiki.ProcedureCovered', blank=True),
        ),
        migrations.AddField(
            model_name='criticalillnesscoverage',
            name='terms',
            field=models.ManyToManyField(to='wiki.Term', blank=True),
        ),
        migrations.AddField(
            model_name='coverpremiumtable',
            name='slabs',
            field=models.ManyToManyField(
                related_name='cover_premium_table', to='wiki.Slab'),
        ),
        migrations.AddField(
            model_name='coverpremiumtable',
            name='state',
            field=models.ManyToManyField(to='wiki.State', blank=True),
        ),
        migrations.AddField(
            model_name='city',
            name='state',
            field=models.ForeignKey(to='wiki.State'),
        ),
        migrations.AddField(
            model_name='cancellationpolicy',
            name='slabs',
            field=models.ManyToManyField(
                related_name='cancellation_policy', to='wiki.Slab'),
        ),
        migrations.AlterUniqueTogether(
            name='slab',
            unique_together=set(
                [('health_product', 'sum_assured', 'deductible', 'grade')]),
        ),
        migrations.AlterUniqueTogether(
            name='productmetadata',
            unique_together=set([('health_product', 'content_type')]),
        ),
    ]
