# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0002_auto_20150817_0303'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pincode',
            name='area',
            field=models.CharField(max_length=255, db_index=True),
        ),
        migrations.AlterField(
            model_name='pincode',
            name='city',
            field=models.CharField(max_length=255, db_index=True),
        ),
        migrations.AlterField(
            model_name='pincode',
            name='is_metro',
            field=models.BooleanField(default=False, db_index=True),
        ),
        migrations.AlterField(
            model_name='pincode',
            name='locality',
            field=models.CharField(max_length=255, db_index=True),
        ),
        migrations.AlterField(
            model_name='pincode',
            name='pincode',
            field=models.IntegerField(db_index=True),
        ),
        migrations.AlterField(
            model_name='pincode',
            name='region',
            field=models.CharField(max_length=255, db_index=True),
        ),
    ]
