# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0003_auto_20150920_1633'),
    ]

    operations = [
        migrations.AddField(
            model_name='pincode',
            name='mapped_city',
            field=models.ForeignKey(to='wiki.City', null=True),
        ),
    ]
