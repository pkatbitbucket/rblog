# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0004_pincode_mapped_city'),
    ]

    operations = [
        migrations.AddField(
            model_name='state',
            name='capital',
            field=models.ForeignKey(related_name='+', to='wiki.City', null=True),
        ),
    ]
