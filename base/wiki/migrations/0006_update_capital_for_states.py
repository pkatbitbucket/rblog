# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def update_capital(apps, schema_editor):
    State = apps.get_model('wiki', 'State')
    City = apps.get_model('wiki', 'City')
    
    state_capitals = {
            'andaman-and-nicobar-islands': 'port-blair',
            'andhra-pradesh': 'hyderabad',
            'arunachal-pradesh': 'itanagar',
            'assam' : 'guwahati',
            'bihar' : 'patna',
            'chandigarh': 'chandigarh',
            'chhattisgarh' : 'raipur',
            'dadra-nagar-haveli' : 'silvassa',
            'daman-diu': 'daman',
            'delhi': 'delhi',
            'goa': 'panaji',
            'gujarat': 'ahmedabad',
            'haryana': 'chandigarh',
            'himachal-pradesh': 'shimla',
            'jammu-kashmir': 'jammu',
            'jharkhand' : 'ranchi',
            'karnataka' : 'bangalore',
            'kerala' : 'thiruvananthapuram',
            'lakshadweep': 'kavaratti',
            'madhya-pradesh' : 'bhopal',
            'maharashtra': 'mumbai',
            'manipur': 'imphal',
            'meghalaya': 'shillong',
            'mizoram': 'aizawl',
            'nagaland': 'kohima',
            'orissa': 'bhubaneswar',
            'pondicherry': 'pondicherry',
            'punjab': 'chandigarh',
            'rajasthan': 'jaipur',
            'sikkim': 'gangtok',
            'tamilnadu': 'chennai',
            'tripura': 'agartala',
            'uttar-pradesh': 'lucknow',
            'uttaranchal': 'dehradun',
            'west-bengal': 'kolkata',
            'telangana': 'hyderabad',
    }
    for state in State.objects.all():
        state.capital = City.objects.get(slug=state_capitals[state.slug])
        state.save()

class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0005_state_capital'),
    ]

    operations = [
        migrations.RunPython(update_capital),
    ]
