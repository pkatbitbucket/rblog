# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import F, Value
from putils import find_city, Levenshtein

def update_pincodes(apps, schema_editor):
    Pincode = apps.get_model('wiki', 'Pincode')
    City = apps.get_model('wiki', 'City')
    total_pincodes = Pincode.objects.count()
    print "\nTotal number of pincodes - %s" % total_pincodes

    pincodes = Pincode.objects.values_list('pincode', flat=True).distinct()
    for i, pinval in enumerate(pincodes):
        pincodes = Pincode.objects.filter(pincode=pinval)
        pincode = pincodes[0]
        try:
            city = City.objects.filter(state=pincode.state).annotate(edit=Levenshtein(F('name'), Value(pincode.city))).filter(edit__lte=1).order_by('edit')[0]
            pincodes.update(mapped_city=city)
        except IndexError:
            pass
        print i, pinval, city.name if city else None

    print "Total number of pincodes not found - %s" % Pincode.objects.filter(mapped_city=None).values('pincode').distinct().count()

class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0006_update_capital_for_states'),
    ]

    operations = [
        migrations.RunSQL("CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;"),
        migrations.RunPython(update_pincodes),
    ]
