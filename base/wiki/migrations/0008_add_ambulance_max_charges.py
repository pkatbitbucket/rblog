# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0007_update_pincode_city'),
    ]

    operations = [
        migrations.RenameField(
            model_name='ambulancecharges',
            old_name='limit_in_network_hospitals',
            new_name='max_limit_in_network_hospitals',
        ),
        migrations.RenameField(
            model_name='ambulancecharges',
            old_name='limit_in_non_network_hospitals',
            new_name='max_limit_in_non_network_hospitals',
        ),
        migrations.AlterField(
            model_name='ambulancecharges',
            name='max_limit_in_network_hospitals',
            field=models.IntegerField(null=True, verbose_name='Maximum limit on ambulance charges in network hospitals', blank=True),
        ),
        migrations.AlterField(
            model_name='ambulancecharges',
            name='max_limit_in_non_network_hospitals',
            field=models.IntegerField(null=True, verbose_name='Maximum limit on ambulance charges in non-network hospitals', blank=True),
        ),
        migrations.AddField(
            model_name='ambulancecharges',
            name='limit_in_network_hospitals',
            field=models.IntegerField(null=True, verbose_name='Limit on ambulance charges in network hospitals', blank=True),
        ),
        migrations.AddField(
            model_name='ambulancecharges',
            name='limit_in_non_network_hospitals',
            field=models.IntegerField(null=True, verbose_name='Limit on ambulance charges in non-network hospitals', blank=True),
        ),
    ]
