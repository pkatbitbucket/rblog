# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0008_add_ambulance_max_charges'),
    ]

    operations = [
        migrations.AddField(
            model_name='criticalillnesscoverage',
            name='benefit_type',
            field=models.CharField(blank=True, max_length=255, verbose_name='Benefit type', choices=[(b'LUMP_SUM', b'Lump sum amount'), (b'REIMBURSEMENT', b'Reimbursement of expenditure on treatment')]),
        ),
    ]
