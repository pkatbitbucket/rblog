# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0009_CI_limit_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='organdonor',
            name='is_surgery_covered',
            field=models.NullBooleanField(verbose_name='Is surgery covered', choices=[(False, b'No'), (True, b'Yes')]),
        ),
    ]
