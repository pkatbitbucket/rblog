# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def add_medical_conditions(apps, schema_editor):
    MedicalCondition = apps.get_model('wiki', 'MedicalCondition')
    medical_condition_dicts = [
        {'medical_condition': 'High / Low Blood Pressure', 'slug': 'high-low-bp', 'frequency': 'HIGH', 'global_decline': False},
        {'medical_condition': 'Diabetes Type I (Insulin Dependent)', 'slug': 'diabetes-i', 'frequency': 'HIGH', 'global_decline': True},
        {'medical_condition': 'Arthritis', 'slug': 'arthritis', 'frequency': 'HIGH', 'global_decline': False},
        {'medical_condition': 'Tuberculosis', 'slug': 'tuberculosis', 'frequency': 'HIGH', 'global_decline': False},
        {'medical_condition': 'Thyroid disorder', 'slug': 'thyroid-disorder', 'frequency': 'HIGH', 'global_decline': False},
        {'medical_condition': 'Cancer - Benign / Malignant', 'slug': 'cancer', 'frequency': 'HIGH', 'global_decline': True},
        {'medical_condition': 'HIV / AIDS', 'slug': 'aids', 'frequency': 'HIGH', 'global_decline': True},
        {'medical_condition': 'STD', 'slug': 'std', 'frequency': 'HIGH', 'global_decline': True},
        {'medical_condition': 'Anaemia', 'slug': 'anaemia', 'frequency': 'HIGH', 'global_decline': False},
        {'medical_condition': 'Stroke', 'slug': 'stroke', 'frequency': 'HIGH', 'global_decline': True},
        {'medical_condition': 'Paralysis', 'slug': 'paralysis', 'frequency': 'HIGH', 'global_decline': True},
        {'medical_condition': 'Mental Illness / Psychiatric diseases', 'slug': 'psychiatric-diseases', 'frequency': 'HIGH', 'global_decline': True},
        {'medical_condition': 'Slipped disc', 'slug': 'slipped-disc', 'frequency': 'HIGH', 'global_decline': False},
        {'medical_condition': 'Heart Diseases', 'slug': 'heart-diseases', 'frequency': 'HIGH', 'global_decline': True},
        {'medical_condition': "Parkinson's disease", 'slug': 'parkinsons-disease', 'frequency': 'HIGH', 'global_decline': True},
        {'medical_condition': "Alzheimer's disease", 'slug': 'alzheimers-disease', 'frequency': 'HIGH', 'global_decline': True},
        {'medical_condition': 'Epilepsy', 'slug': 'epilepsy', 'frequency': 'HIGH', 'global_decline': True},
        {'medical_condition': 'Congenital disorders', 'slug': 'congenital-disorders', 'frequency': 'HIGH', 'global_decline': False},
        {'medical_condition': 'Stomach or Intestine ulcers', 'slug': 'stomach-intestine-ulcers', 'frequency': 'HIGH', 'global_decline': False},
        {'medical_condition': 'Kidney Failure', 'slug': 'kidney-failure', 'frequency': 'HIGH', 'global_decline': True},
        {'medical_condition': 'Liver Failure', 'slug': 'liver-failure', 'frequency': 'HIGH', 'global_decline': True},
        {'medical_condition': 'Asthma', 'slug': 'asthma', 'frequency': 'HIGH', 'global_decline': False},
        {'medical_condition': 'Hernia', 'slug': 'hernia', 'frequency': 'HIGH', 'global_decline': False},
        {'medical_condition': 'Jaundice', 'slug': 'jaundice', 'frequency': 'HIGH', 'global_decline': False},
        {'medical_condition': 'Hepatitis', 'slug': 'hepatitis', 'frequency': 'HIGH', 'global_decline': False},
        {'medical_condition': 'Joint replacement', 'slug': 'joint-replacement', 'frequency': 'HIGH', 'global_decline': False},
        {'medical_condition': 'Multiple Sclerosis', 'slug': 'multiple-sclerosis', 'frequency': 'HIGH', 'global_decline': False},
        {'medical_condition': 'Brain & Spinal cord disorders', 'slug': 'brain-spinal-cord-disorders', 'frequency': 'HIGH', 'global_decline': False},
        {'medical_condition': 'Bronchitis (Inflammation of mucus lining of lungs)', 'slug': 'bronchitis', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Urinary tract diseases', 'slug': 'urinary-tract-diseases', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Gall bladder diseases', 'slug': 'gall-bladder-diseases', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Pituitary disorder', 'slug': 'pituitary-disorder', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Joint diseases', 'slug': 'joint-diseases', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Bone diseases', 'slug': 'bone-diseases', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Breathing disorders (Other than COPD)', 'slug': 'breathing-disorders', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Prostate disorder', 'slug': 'prostate-disorder', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Gastro-Intestinal disorder', 'slug': 'gastro-intestinal-disorder', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Endocrine disorders', 'slug': 'endocrine-disorders', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Tumor', 'slug': 'tumor', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Immune system disorder', 'slug': 'immune-system-disorder', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Blood disorders', 'slug': 'blood-disorders', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Gynaecological disorders', 'slug': 'gynaecological-disorders', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Cyst / Fibroadenoma', 'slug': 'fibroadenoma', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Alcohol abuse / Narcotics', 'slug': 'alcohol-narcotics', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'External ulcer / growth / cyst / mass', 'slug': 'external-ulcer-cyst', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Skin allergies', 'slug': 'skin-allergies', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Breast disorders', 'slug': 'breast-disorders', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Pelvic Infection', 'slug': 'pelvic-infection', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Eye diseases', 'slug': 'eye-diseases', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Chest Pain', 'slug': 'chest-pain', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Kidney Stones', 'slug': 'kidney-stones', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Transient Ischemic Attack (TIA) (Reversible Stroke)', 'slug': 'transient-ischemic-attack', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Chronic Pancreatitis (Long standing inflammation of Pancreas)', 'slug': 'chronic-pancreatitis', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': "Chron's disease (Inflammatory bowel disease, Inflammation of digestive tract)", 'slug': 'chrons-disease', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Dialysis', 'slug': 'dialysis', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Thalassemia Major (Blood disorder)', 'slug': 'thalassemia-major', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Muscle disorders', 'slug': 'muscle-disorders', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Obesity', 'slug': 'obesity', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Burns more than 10% (apart from face & neck)', 'slug': 'burns-except-face-neck', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Burns of face & neck', 'slug': 'burns-face-neck', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Varicose veins (Enlarged & twisted veins)', 'slug': 'varicose-veins', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'COPD (Chronic Obstructive Pulmonary Disease, Breathing disorder)', 'slug': 'copd', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Cysts/Fibroids of Uterus (Tumors in Uterus)', 'slug': 'uterus-fibroids-cysts', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Polio', 'slug': 'polio', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Ulcerative colitis (Inflammation & Ulcers in Colon)', 'slug': 'ulcerative-colitis', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Breast Fibroadenoma (Benign tumor in breast)', 'slug': 'breast-fibroadenoma', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Neurofibromatosis / Neurofibrosis (Genetic disorder of nervous system leading to tumor formation)', 'slug': 'neurofibromatosis-neurofibrosis', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Long-term anticoagulant therapy', 'slug': 'long-term-anticoagulant-therapy', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Connective tissue disorders requiring long-term steroids & immunosuppressants', 'slug': 'connective-tissue-disorders', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Pregnant female from second trimester up to 1 month of child birth', 'slug': 'pregnant-second-trimester-to-1-month-after-birth', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Genetic diseases', 'slug': 'genetic-diseases', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Liver Cirrhosis', 'slug': 'liver-cirrhosis', 'frequency': 'LOW', 'global_decline': True},
        {'medical_condition': 'Alcoholic Liver disease', 'slug': 'alcoholic-liver-disease', 'frequency': 'LOW', 'global_decline': True},
        {'medical_condition': 'Transplant - Liver', 'slug': 'transplant-liver', 'frequency': 'LOW', 'global_decline': True},
        {'medical_condition': 'Transplant - Kidney', 'slug': 'transplant-kidney', 'frequency': 'LOW', 'global_decline': True},
        {'medical_condition': 'Transplant - Lung', 'slug': 'transplant-lung', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Transplant - Cornea', 'slug': 'transplant-cornea', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Rheumatoid Arthritis', 'slug': 'rheumatoid-arthritis', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Ankylosing spondylitis (Arthritis of Spine)', 'slug': 'ankylosing-spondylitis', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': "Addison's disease (Insufficient functioning of glands in kidney)", 'slug': 'addisons-disease', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Endometriosis (affecting uterus)', 'slug': 'endometriosis', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': "Goodpasture's syndrome (involving kidney & lungs)", 'slug': 'goodpastures-syndrome', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Glomerulonephritis (kidney disease)', 'slug': 'glomerulonephritis', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': "Hashimoto's thyroiditis (Inflammation of thyroid)", 'slug': 'hashimotos-thyroiditis', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': "Grave's disease (Thyoid disorder)", 'slug': 'graves-disease', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Juvenile Arthritis (Arthritis in children age 16 or younger)', 'slug': 'juvenile-arthritis', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Systemic Lupus Erythematosus (SLE, Inflammation of multiple organs)', 'slug': 'systemic-lupus-erythematosus', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Sarcoidosis (Inflammation in lungs/skin)', 'slug': 'sarcoidosis', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Psoriasis (skin disease)', 'slug': 'psoriasis', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Other autoimmune diseases', 'slug': 'autoimmune-diseases', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': "Down's Syndrome", 'slug': 'downs-syndrome', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Muscular Dystrophy', 'slug': 'muscular-dystrophy', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Autism', 'slug': 'autism', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Pacemaker Implantation', 'slug': 'pacemaker-implantation', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Cerebral Palsy', 'slug': 'cerebral-palsy', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Peripheral Vascular disease', 'slug': 'peripheral-vascular-disease', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Spinal Stenosis', 'slug': 'spinal-stenosis', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Hemophilia', 'slug': 'hemophilia', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Psychological Problems', 'slug': 'psychological-problems', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Bilateral blindness', 'slug': 'bilateral-blindness', 'frequency': 'LOW', 'global_decline': False},
        {'medical_condition': 'Bilateral deafness', 'slug': 'bilateral-deafness', 'frequency': 'LOW', 'global_decline': False},
    ]
    medical_conditions = []
    for m in medical_condition_dicts:
        medical_conditions.append(
            MedicalCondition(medical_condition=m['medical_condition'],
                             slug=m['slug'],
                             frequency=m['frequency'],
                             global_decline=m['global_decline'])
        )
    MedicalCondition.objects.bulk_create(medical_conditions)


def add_transaction_permissions(apps, schema_editor):
    MedicalCondition = apps.get_model('wiki', 'MedicalCondition')
    MedicalTransactionPermission = apps.get_model('wiki', 'MedicalTransactionPermission')
    Insurer = apps.get_model('wiki', 'Insurer')

    transaction_permissions = []

    def add_insurer_permissions(insurer, not_allowed):
        medical_conditions = MedicalCondition.objects.filter(slug__in=not_allowed)
        for hp in insurer.healthproduct_set.all():
            for medical_condition in medical_conditions:
                transaction_permissions.append(
                    MedicalTransactionPermission(health_product=hp,
                                                 medical_condition=medical_condition,
                                                 transaction_permission='NOT_ALLOWED')
                )

    apollo = Insurer.objects.get(title__icontains='apollo')
    not_allowed = ['pregnant-second-trimester-to-1-month-after-birth', 'ulcerative-colitis', 'chrons-disease', 'pacemaker-implantation', 'autoimmune-diseases', 'rheumatoid-arthritis', 'multiple-sclerosis', 'ankylosing-spondylitis', 'genetic-diseases', 'diabetes-i', 'heart-diseases', 'stroke', 'cancer', 'systemic-lupus-erythematosus', 'transplant-kidney', 'liver-cirrhosis', 'epilepsy', 'psychiatric-diseases', 'paralysis']
    add_insurer_permissions(apollo, not_allowed)

    cigna = Insurer.objects.get(title__icontains='cigna')
    not_allowed = ['stroke', 'transient-ischemic-attack', 'paralysis', 'liver-cirrhosis', 'alcoholic-liver-disease', 'liver-failure', 'cancer', 'heart-diseases', 'chronic-pancreatitis', 'chrons-disease', 'dialysis', 'diabetes-i', 'arthritis', 'aids', 'transplant-liver', 'transplant-kidney', 'transplant-lung', 'transplant-cornea', 'thalassemia-major', 'hepatitis', 'tuberculosis', 'psychiatric-diseases', 'pregnant-second-trimester-to-1-month-after-birth', 'alzheimers-disease', 'ankylosing-spondylitis', 'downs-syndrome', 'multiple-sclerosis', 'muscular-dystrophy', 'autism', 'pacemaker-implantation', 'cerebral-palsy', 'copd', 'peripheral-vascular-disease', 'spinal-stenosis', 'hemophilia', 'systemic-lupus-erythematosus']
    add_insurer_permissions(cigna, not_allowed)

    lt = Insurer.objects.get(title__icontains='l&t')
    not_allowed = ['heart-diseases', 'parkinsons-disease', 'epilepsy', 'stroke', 'brain-spinal-cord-disorders', 'psychological-problems', 'kidney-failure', 'transplant-kidney', 'paralysis', 'muscle-disorders', 'systemic-lupus-erythematosus', 'hepatitis', 'liver-cirrhosis', 'burns-except-face-neck', 'burns-face-neck', 'copd', 'diabetes-i', 'congenital-disorders', 'cancer', 'bilateral-blindness', 'bilateral-deafness', 'ulcerative-colitis', 'chrons-disease', 'neurofibromatosis-neurofibrosis']
    add_insurer_permissions(lt, not_allowed)

    religare = Insurer.objects.get(title__icontains='religare')
    not_allowed = ['diabetes-i', 'heart-diseases', 'cancer', 'epilepsy', 'aids', 'stroke', 'paralysis', 'psychiatric-diseases', 'kidney-failure', 'liver-cirrhosis', 'alcoholic-liver-disease', 'liver-failure', 'rheumatoid-arthritis', 'ankylosing-spondylitis', 'ulcerative-colitis', 'chronic-pancreatitis', 'chrons-disease', 'pregnant-second-trimester-to-1-month-after-birth']
    add_insurer_permissions(religare, not_allowed)

    star = Insurer.objects.get(title__icontains='star')
    not_allowed = ['heart-diseases', 'cancer', 'kidney-failure', 'stroke', 'paralysis', 'parkinsons-disease', 'alzheimers-disease', 'psychiatric-diseases', 'liver-cirrhosis', 'alcoholic-liver-disease', 'copd', 'autoimmune-diseases', 'connective-tissue-disorders', 'long-term-anticoagulant-therapy', 'pregnant-second-trimester-to-1-month-after-birth', 'rheumatoid-arthritis', 'ankylosing-spondylitis']
    add_insurer_permissions(star, not_allowed)

    MedicalTransactionPermission.objects.bulk_create(transaction_permissions)


def reversible_func(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0010_add_organ_donor_surgery_option'),
    ]

    operations = [
        migrations.CreateModel(
            name='MedicalCondition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('medical_condition', models.CharField(max_length=255, verbose_name='medical_condition')),
                ('slug', models.CharField(max_length=255)),
                ('description', models.TextField(null=True, blank=True)),
                ('frequency', models.CharField(max_length=50, choices=[(b'HIGH', b'High'), (b'LOW', b'Low')])),
                ('global_decline', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='MedicalTransactionPermission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('transaction_permission', models.CharField(max_length=20, verbose_name='Select the permissions to be applied on the Product(s)', choices=[(b'STP', b'Allowed'), (b'MEDICAL_ONLINE', b'Medical Required (online payment allowed)'), (b'MEDICAL_OFFLINE', b'Medical Required (online payment not allowed)'), (b'NOT_ALLOWED', b'Not Allowed')])),
                ('health_product', models.ForeignKey(related_name='medical_permissions', to='wiki.HealthProduct')),
                ('medical_condition', models.ForeignKey(to='wiki.MedicalCondition')),
            ],
        ),
        migrations.RunPython(add_medical_conditions, reversible_func),
        migrations.RunPython(add_transaction_permissions, reversible_func),
    ]
