# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0011_medicaltransactionpermissions'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coverpremiumtable',
            name='premium',
            field=models.FloatField(db_column=b'premium'),
        ),
        migrations.RenameField(
            model_name='coverpremiumtable',
            old_name='premium',
            new_name='_premium',
        ),
        migrations.AlterField(
            model_name='familycoverpremiumtable',
            name='premium',
            field=models.FloatField(db_column=b'premium'),
        ),
        migrations.RenameField(
            model_name='familycoverpremiumtable',
            old_name='premium',
            new_name='_premium',
        )
    ]
