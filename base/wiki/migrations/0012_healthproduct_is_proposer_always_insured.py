# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0011_medicaltransactionpermissions'),
    ]

    operations = [
        migrations.AddField(
            model_name='healthproduct',
            name='is_proposer_always_insured',
            field=models.BooleanField(default=False, help_text='This plan expects the proposer to be one of the insured', choices=[(False, b'No'), (True, b'Yes')]),
        ),
    ]
