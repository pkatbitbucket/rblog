# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0012_healthproduct_is_proposer_always_insured'),
    ]

    operations = [
        migrations.AlterField(
            model_name='healthproduct',
            name='is_proposer_always_insured',
            field=models.BooleanField(default=False, help_text='Does this plan need the proposer to be one of the insured?', choices=[(False, b'No'), (True, b'Yes')]),
        ),
    ]
