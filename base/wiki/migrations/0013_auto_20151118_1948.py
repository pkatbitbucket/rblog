# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def cp_update_service_tax(apps, schema_editor):
    CoverPremiumTable = apps.get_model('wiki', 'CoverPremiumTable')
    for cpt in CoverPremiumTable.objects.all():
        premium = cpt._premium
        if premium is not None:
            premium = premium / 1.14
            cpt._premium = premium
            cpt.save()

def fcp_update_service_tax(apps, schema_editor):
    FamilyCoverPremiumTable = apps.get_model('wiki', 'FamilyCoverPremiumTable')
    for cpt in FamilyCoverPremiumTable.objects.all():
        premium = cpt._premium
        if premium is not None:
            premium = premium / 1.14
            cpt._premium = premium
            cpt.save()


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0012_auto_20151118_1947'),
    ]

    operations = [
        migrations.RunPython(cp_update_service_tax, migrations.RunPython.noop),
        migrations.RunPython(fcp_update_service_tax, migrations.RunPython.noop)
    ]
