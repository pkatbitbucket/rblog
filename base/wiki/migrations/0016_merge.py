# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0015_merge'),
        ('wiki', '0013_auto_20151118_1948'),
    ]

    operations = [
    ]
