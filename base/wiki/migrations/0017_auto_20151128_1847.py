# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0016_merge'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='medicaltransactionpermission',
            unique_together=set([('medical_condition', 'health_product')]),
        ),
    ]
