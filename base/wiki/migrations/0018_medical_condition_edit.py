# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def modify_medical_conditions(apps, schema_editor):
	MedicalCondition = apps.get_model('wiki', 'MedicalCondition')
	m = MedicalCondition.objects.filter(medical_condition="Stroke").first()
	if m:
		m.medical_condition = "Stroke / Paralysis"
		m.slug = 'stroke-paralysis'
		m.save()

	MedicalCondition.objects.filter(medical_condition="Paralysis").delete()
	m = MedicalCondition.objects.filter(medical_condition="High / Low Blood Pressure").first()
	if m:
		m.medical_condition = "High / Low Blood Pressure (hypertension)"
		m.slug = 'high-low-bp-ht'
		m.save()

	m = MedicalCondition.objects.filter(medical_condition="Kidney Failure").first()
	if m:
		m.medical_condition = "Kidney Failure / Liver Failure"
		m.slug = 'Kidney-liver-failure'
		m.save()

	MedicalCondition.objects.filter(medical_condition="Liver Failure").delete()
	m = MedicalCondition.objects.filter(medical_condition="Joint replacement").first()
	if m:
		m.medical_condition = "Joint replacement / diseases"
		m.slug = 'joint-replacement-diseases'
		m.save()

	MedicalCondition.objects.filter(medical_condition="Joint diseases").delete()
	m = MedicalCondition.objects.filter(medical_condition="Parkinson's disease").first()
	if m:
		m.medical_condition = "Parkinson's / Alzheimer's / Epilepsy"
		m.slug = 'parkinson-alzheimer-epilepsy'
		m.save()

	MedicalCondition.objects.filter(medical_condition="Alzheimer's disease").delete()
	MedicalCondition.objects.filter(medical_condition="Epilepsy").delete()
	m = MedicalCondition.objects.filter(medical_condition="HIV / AIDS").first()
	if m:
		m.medical_condition = "HIV / AIDS / STD"
	 	m.slug = 'hiv-aids-std'
		m.save()
	m = MedicalCondition.objects.filter(slug="diabetes-ii").first()
	if m:
		m.medical_condition = "Diabetes II"
		m.save()

	MedicalCondition.objects.filter(medical_condition="STD").delete()

	m = MedicalCondition.objects.filter(medical_condition="Dengue").first()
	if not m:
		m = MedicalCondition.objects.create(medical_condition="Dengue", slug="dengue", frequency="LOW")
	m = MedicalCondition.objects.filter(medical_condition="Fistula").first()
	if not m:
		m = MedicalCondition.objects.create(medical_condition="Fistula", slug="fistula", frequency="LOW")
	m = MedicalCondition.objects.filter(medical_condition="Hysterectomy").first()
	if not m:
		m = MedicalCondition.objects.create(medical_condition="Hysterectomy", slug="hysterectomy", frequency="LOW")
	m = MedicalCondition.objects.filter(medical_condition="Dyslipedemia").first()
	if not m:
		m = MedicalCondition.objects.create(medical_condition="Dyslipedemia", slug="dyslipedemia", frequency="LOW")



def sort_medical_conditions(apps, schema_editor):
	MedicalCondition = apps.get_model('wiki', 'MedicalCondition')
	list_in_order = ['Diabetes Type I (Insulin Dependent)',
					 'High / Low Blood Pressure (hypertension)',
					 'Heart Diseases',
					 'Asthma',
					 'Stroke / Paralysis',
					 'Tuberculosis',
					 'Thyroid disorder',
					 'Stomach or Intestine ulcers',
					 'Kidney Failure / Liver Failure',
					 'Hernia',
					 'Slipped disc',
					 'Joint replacement / diseases',
					 'HIV / AIDS / STD',
					 'Arthritis',
					 'Congenital disorders',
					 'Diabetes II',
					 'Cancer - Benign / Malignant',
					 'Hepatitis',
					 'Anaemia',
					 'Multiple Sclerosis',
					 'Jaundice',
					 'Brain & Spinal cord disorders',
					 "Parkinson's / Alzheimer's / Epilepsy",
					 'Mental Illness / Psychiatric diseases']
	for indx, val in enumerate(list_in_order):
		m = MedicalCondition.objects.filter(medical_condition=val).first()
		if m:
			m.sort_order = indx + 1
			m.save()


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0017_medicalcondition_sort_order'),
    ]

    operations = [
		migrations.RunPython(modify_medical_conditions, migrations.RunPython.noop),
		migrations.RunPython(sort_medical_conditions, migrations.RunPython.noop),
    ]
