# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0018_medical_condition_edit'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='medicalcondition',
            options={'ordering': ['sort_order', 'frequency']},
        ),
    ]
