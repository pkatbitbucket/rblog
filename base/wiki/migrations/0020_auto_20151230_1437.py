# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


"""
ids of oriental family floater gold plan - 92, silver plan - 93
1. deleting all the cover premium tables for the new products that are cloned
2. deleting 1.5, 2.5, 3.5, 4.5 L slabs from the new product that is cloned
"""
def oriental_change(apps, schema_editor):
    FamilyCoverPremiumTable = apps.get_model('wiki', 'FamilyCoverPremiumTable')
    Slab = apps.get_model('wiki', 'Slab')

    FamilyCoverPremiumTable.objects.filter(slabs__health_product_id__in=[92, 93]).delete()
    Slab.objects.filter(sum_assured__in=[150000, 250000, 350000, 450000], health_product_id=93).delete()

class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0019_auto_20151221_1552'),
    ]

    operations = [
        migrations.RunPython(oriental_change, migrations.RunPython.noop)
    ]
