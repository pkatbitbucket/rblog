# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0020_auto_20151230_1437'),
        ('wiki', '0017_auto_20151128_1847'),
    ]

    operations = [
    ]
