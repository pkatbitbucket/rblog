# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def add_pincode(apps, schema_editor):
    Pincode = apps.get_model('wiki', 'Pincode')
    State = apps.get_model('wiki', 'State')
    City = apps.get_model('wiki', 'City')
    s = State.objects.get(id=21)
    c = City.objects.filter(name='Ulhasnagar')[0]

    p = Pincode(
        pincode=421003,
        is_metro=True,
        area='Ulhasnagar',
        city='Ulhasnagar',
        lat=19.2340969,
        lng=73.157449,
        locality='Ulhasnagar Mahanagar Palika Udyan',
        region='Maharashtra',
        mapped_city=c,
        state=s
    )
    p.save()


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0021_merge'),
    ]

    operations = [
        migrations.RunPython(add_pincode, migrations.RunPython.noop),
    ]
