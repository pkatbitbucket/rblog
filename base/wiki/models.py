import datetime
import os

from django.apps import apps
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models import permalink
from django.conf import settings
from django.contrib.contenttypes.models import ContentType

from django.db.models import Manager
from autoslug import AutoSlugField
from tagging.fields import TagField

from customdb.thumbs import ImageWithThumbsField
import utils


def get_file_path(instance, filename):
    now = datetime.datetime.now()
    fname, ext = os.path.splitext(filename)
    new_fname = utils.slugify(fname)
    newfilename = "pw-%s.%s" % (now.strftime("%I%M%S"), ext)
    path_to_save = "uploads/wiki/insurer/%s" % (newfilename)
    return path_to_save

AGE_LIMITS = [(i, i) for i in range(0, 101)]
AGE_LIMITS_1 = [(i, "%d months" % i) for i in range(0, 12)]
AGE_LIMITS_1.extend([(i, "%d years" % (i / 1000))
                     for i in range(1000, 101000, 1000)])


class CloneableModel(models.Model):
    pass

    class Meta:
        abstract = True

    def clone(self):
        self.id = None
        self.save()
        return self


class LocationAbstract(models.Model):
    name = models.CharField(max_length=100)
    is_enabled = models.BooleanField()

    def __unicode__(self):
        return self.name

    class Meta:
        abstract = True


class State(LocationAbstract):
    slug = AutoSlugField(populate_from='name',
                         always_update=True, unique=True, null=False)
    capital = models.ForeignKey('City', related_name='+', null=True)


class City(LocationAbstract):
    slug = AutoSlugField(populate_from='name',
                         always_update=True, unique=False, null=False)
    state = models.ForeignKey('State')
    is_major = models.BooleanField()
    is_approved = models.BooleanField()


class CityAKA(models.Model):
    city = models.ForeignKey('City')
    name = models.CharField(max_length=100)

    def __str__(self):
        return "%s" % self.name


class StateAKA(models.Model):
    state = models.ForeignKey('State')
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Pincode(models.Model):
    """
    ';Delhi; ;Mumbai; ;Pune; ;New Delhi; ;Bangalore; ;Chennai; ;Kolkata; ;Pune; ;Gurgaon; ;Hyderabad; ;Ahmadabad; ;Ahmedabad; ;Surat; ;Jaipur; ;Thane; ;Noida; ;Chandigarh;' are metros
    """
    pincode = models.IntegerField(db_index=True)
    state = models.ForeignKey('State')
    lat = models.FloatField(null=True, blank=True)
    lng = models.FloatField(null=True, blank=True)
    region = models.CharField(max_length=255, db_index=True)
    city = models.CharField(max_length=255, db_index=True)
    area = models.CharField(max_length=255, db_index=True)
    locality = models.CharField(max_length=255, db_index=True)
    is_metro = models.BooleanField(default=False, db_index=True)
    mapped_city = models.ForeignKey(City, null=True)

class HealthProduct(models.Model):
    """ Health Insurance Products """
    POLICY_TYPE_CHOICES = (
        ('INDIVIDUAL', 'Individual'),
        ('FAMILY', 'Family'),
    )
    COVERAGE_CATEGORY_CHOICES = (
        ('CRITICAL_ILLNESS', 'Critical Illness'),
        ('FIXED_BENEFIT', 'Fixed Benefit'),
        ('HEALTH_SAVINGS', 'Health Savings'),
        ('INDEMNITY', 'Indemnity'),
        ('SUPER_TOPUP', 'Super Topup'),
        ('TOPUP', 'Topup'),
    )
    # Title, Slug, body will be used to generate arcticles
    title = models.CharField(_('title'), max_length=200)
    slug = models.SlugField(_('slug'), max_length=70)
    insurer = models.ForeignKey('Insurer')
    policy_type = models.CharField(
        _('Policy Type'), choices=POLICY_TYPE_CHOICES, max_length=255)
    coverage_category = models.CharField(
        _('Category of illness coverage'), choices=COVERAGE_CATEGORY_CHOICES, max_length=255, blank=True)
    body = models.TextField(_('body'), )
    author = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
    claim_form = models.FileField(
        upload_to=get_file_path, null=True, blank=True)
    logo = ImageWithThumbsField(upload_to=get_file_path, sizes=(
        ('s', 150, 225),), null=True, blank=True)
    policy_wordings = models.FileField(
        upload_to=get_file_path, null=True, blank=True)
    brochure = models.FileField(upload_to=get_file_path, null=True, blank=True)
    proposal_form = models.FileField(
        upload_to=get_file_path, null=True, blank=True)
    created = models.DateTimeField(_('created'), auto_now_add=True)
    modified = models.DateTimeField(_('modified'), auto_now=True)

    rider_products = models.ManyToManyField(
        'self', symmetrical=False, blank=True)
    notes = models.TextField(blank=True)
    is_proposer_always_insured = models.BooleanField(
        choices=((False, "No"), (True, "Yes")), default=False, help_text=_(
            'Does this plan need the proposer to be one of the insured?'))
    is_completed = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))
    is_backend_integrated = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))

    class Meta:
        verbose_name = _('Health Product')
        verbose_name_plural = _('Health Products')
        ordering = ('-created',)
        get_latest_by = 'created'

    def __unicode__(self):
        return u'%s - %s' % (self.insurer.title, self.title)

    def seo_title(self):
        return u'%s %s' % (self.insurer.seo_title, self.title)

    def clone(self):
        original_id = self.id
        self.id = None
        self.title = "%s - Copy" % self.title
        self.save()
        original = self.__class__.objects.get(id=original_id)

        self.rider_products.clear()

        slab_old_new_dict = {}
        for sl in original.slabs.all():
            original_slab_id = sl.id
            new_slab = sl.clone(deep_copy=True, new_health_product=self)
            self.slabs.add(new_slab)
            original_slab = Slab.objects.get(id=original_slab_id)
            slab_old_new_dict[original_slab] = new_slab

        obj_slab_dict = {}
        for attr in AttributeList:
            wiki_model_string = attr[0]
            wiki_var_string = attr[2]
            M = apps.get_model('wiki', wiki_model_string)
            for obj in M.objects.filter(slabs__health_product=original):
                obj_slab_dict[obj] = (list(obj.slabs.filter(
                    health_product=original)), wiki_model_string, wiki_var_string)

        for obj, dlist in obj_slab_dict.items():
            slabs_list, wiki_model_string, wiki_var_string = dlist
            new_obj = obj.clone()
            for osl in slabs_list:
                new_slab = slab_old_new_dict[osl]
                if getattr(getattr(osl, wiki_var_string), 'all', None):
                    getattr(new_slab, wiki_var_string).add(new_obj)
                else:
                    setattr(new_slab, wiki_var_string, new_obj)
                    new_slab.save()

        # Metadata copy Except is_completed
        meta_data_status = {}
        meta_old_new_dict = {}
        for old_slab, new_slab in slab_old_new_dict.items():
            for mdata in old_slab.metadata_set.all():
                original_meta_id = mdata.id
                if not meta_data_status.get(mdata.id):
                    new_mdata = mdata.clone()
                    new_mdata.is_completed = False
                    new_mdata.save()
                    meta_old_new_dict[original_meta_id] = new_mdata.id
                meta_data_status[mdata.id] = True

        for old_meta, new_meta in meta_old_new_dict.items():
            old_meta_obj = MetaData.objects.get(id=old_meta)
            new_meta_obj = MetaData.objects.get(id=new_meta)
            for osl in old_meta_obj.slabs.all():
                new_meta_obj.slabs.add(slab_old_new_dict[osl])

        return self

    @permalink
    def get_edit_url(self):
        return ('health_product_edit', None, {
            'health_product_id': self.id,
            'insurer_id': self.insurer.id
        })

    @permalink
    def get_absolute_url(self):
        return ('health_product_detail', None, {
            'insurer-slug': self.insurer.slug,
            'slug': self.slug
        })


class MedicalCondition(models.Model):
    FREQUENCY_CHOICES = (
        ('HIGH', 'High'),
        ('LOW', 'Low'),
    )
    medical_condition = models.CharField(_('medical_condition'), max_length=255)
    slug = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    frequency = models.CharField(max_length=50, choices=FREQUENCY_CHOICES)
    global_decline = models.BooleanField(default=False)
    sort_order = models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return self.medical_condition

    class Meta:
        ordering = ['sort_order', 'frequency']


class MedicalTransactionPermission(models.Model):
    TRANSACTION_PERMISSIONS = (
        ('STP', 'Allowed'),
        ('MEDICAL_ONLINE', 'Medical Required (online payment allowed)'),
        ('MEDICAL_OFFLINE', 'Medical Required (online payment not allowed)'),
        ('NOT_ALLOWED', 'Not Allowed'),
    )
    medical_condition = models.ForeignKey(MedicalCondition)
    transaction_permission = models.CharField(_('Select the permissions to be applied on the Product(s)'),
                                              max_length=20, choices=TRANSACTION_PERMISSIONS)
    health_product = models.ForeignKey(HealthProduct, related_name='medical_permissions')

    def __unicode__(self):
        return '%s-%s' % (self.medical_condition.medical_condition, self.transaction_permission)

    class Meta:
        unique_together = ('medical_condition', 'health_product',)


class WikiArticle(models.Model):
    """Articles related to Insurance companies, products"""
    STATUS_CHOICES = (
        (1, _('Draft')),
        (2, _('Public')),
    )
    CATEGORY_CHOICES = [
        ('PRODUCT_REVIEW', 'Product Review'),
        ('COMPANY_PAGE', 'Company Page')
    ]
    title = models.CharField(_('title'), max_length=200)
    slug = models.SlugField(_('slug'), max_length=250)
    category = models.CharField(max_length=250, choices=CATEGORY_CHOICES)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, blank=True, null=True, related_name='article_author')
    body = models.TextField(_('body'), blank=True)
    summary = models.TextField(_('summary'), blank=True)
    tease = models.TextField(_('tease'), blank=True, help_text=_(
        'Concise text suggested. Does not appear in RSS feed.'))
    status = models.IntegerField(
        _('status'), choices=STATUS_CHOICES, default=1)
    insurer = models.ManyToManyField('Insurer', blank=True)
    health_product = models.ManyToManyField('HealthProduct', blank=True)
    keywords = models.TextField(_('keywords'), blank=True, help_text=_(
        'List of keywords, relevant for SEO'))
    logo = ImageWithThumbsField(upload_to=get_file_path, sizes=(
        ('s', 300, 300), ('t', 500, 500)), null=True, blank=True)
    pdf = models.FileField(upload_to=get_file_path, null=True, blank=True)
    created = models.DateTimeField(_('created'), auto_now_add=True)
    modified = models.DateTimeField(_('modified'), auto_now=True)
    tags = TagField()

    def __unicode__(self):
        return u'%s' % self.title

    @permalink
    def get_absolute_url(self):
        return ('article_wiki_details', None, {
            'article_slug': self.slug,
        })

    def seo_title(self):
        return "%s %s - Coverfox.com" % (self.title, self.insurer.seo_title)

    def get_metacontext(self):
        import putils
        seo = putils.get_seo(self)

        keywords = settings.SITE_KEYWORDS
        if seo and getattr(seo, 'keywords', ''):
            keywords = seo.keywords.all()
        else:
            keywords = ""

        meta = {
            'title': getattr(seo, 'title', None) if seo else None,
            'canonical': self.get_absolute_url(),
            'description': getattr(seo, 'description') if seo else "",
            'keywords': keywords,
            'author': self.author,
            'modified_time': self.modified.strftime("%Y-%m-%d"),
            'published_time': self.created.strftime("%Y-%m-%d"),
            'section': self.category,
            'raw': getattr(seo, 'raw', '') if seo else "",
        }
        return meta


class InsurerManager(Manager):

    def published(self):
        return self.get_queryset().filter(status__gte=2, created__lte=datetime.datetime.now())


class Insurer(models.Model):
    """Insurer model."""
    STATUS_CHOICES = (
        (1, _('Draft')),
        (2, _('Public')),
    )
    # Title, Slug, body will be used to generate arcticles
    title = models.CharField(_('title'), max_length=200)
    seo_title = models.CharField(_('SEO title'), max_length=200, default="")
    slug = models.SlugField(_('slug'), max_length=70)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, blank=True, null=True, related_name='insurer_author')
    insurer_manager = models.ForeignKey(
        settings.AUTH_USER_MODEL, blank=True, null=True, related_name='insurer_manager')
    body = models.TextField(_('body'), )
    tease = models.TextField(_('tease'), blank=True, help_text=_(
        'Concise text suggested. Does not appear in RSS feed.'))
    logo = ImageWithThumbsField(upload_to=get_file_path, sizes=(
        ('s', 300, 300), ('t', 500, 500)), null=True, blank=True)
    status = models.IntegerField(
        _('status'), choices=STATUS_CHOICES, default=1)
    claims_data = models.ForeignKey('ClaimsData', blank=True, null=True)
    created = models.DateTimeField(_('created'), auto_now_add=True)
    modified = models.DateTimeField(_('modified'), auto_now=True)
    objects = InsurerManager()

    class Meta:
        verbose_name = _('insurer')
        verbose_name_plural = _('insurers')
        ordering = ('-created',)
        get_latest_by = 'created'

    def __unicode__(self):
        return u'%s' % self.title

    def get_metacontext(self):
        import putils
        seo = putils.get_seo(self)

        keywords = settings.SITE_KEYWORDS
        if getattr(seo, 'keywords', ''):
            keywords = seo.keywords.all()

        simage = ''
        if getattr(self, 'logo'):
            simage = self.logo.url

        meta = {
            'title': getattr(seo, 'title', self.title),
            'canonical': self.get_absolute_url(),
            'image': simage,
            'description': getattr(seo, 'description', self.body),
            'keywords': keywords,
            'author': self.author,
            'modified_time': self.modified.strftime("%Y-%m-%d"),
            'published_time': self.created.strftime("%Y-%m-%d"),
            'section': "Insurance Company",
            'raw': getattr(seo, 'raw', ''),
        }
        return meta

    @permalink
    def get_edit_url(self):
        return ('insurer_edit', None, {
            'insurer_id': self.id
        })

    @permalink
    def get_absolute_url(self):
        return ('insurer_detail', None, {
            'slug': self.slug,
        })


class ClaimsData(models.Model):
    in_business_since = models.IntegerField(
        _('Company has been in business since year (e.g. 2007)'), null=True, blank=True)
    solvency_ratio = models.FloatField(
        _('Solvency ratio (e.g. 1.77)'), null=True, blank=True)
    market_share = models.FloatField(
        _('Market share in percentage e.g 18.23'), null=True, blank=True)
    growth_rate_of_market_share = models.FloatField(
        _('Growth rate in market share e.g -13.2'), null=True, blank=True)
    rejection_ratio = models.FloatField(
        _('Rejection ratio e.g. 6%'), blank=True, null=True)
    claims_response_time = models.FloatField(
        _('Percentage of claims decision in 30 days e.g 93%'), blank=True, null=True)


class Slab(models.Model):
    health_product = models.ForeignKey('HealthProduct', related_name='slabs')
    sum_assured = models.PositiveIntegerField()
    deductible = models.PositiveIntegerField(default=0)
    grade = models.CharField(
        _('Grade of the plan if application (e.g silver, gold, premium)'), max_length=255, blank=True)
    general = models.ForeignKey(
        'General', null=True, blank=True, related_name='slabs')
    pre_existing = models.ForeignKey(
        'PreExisting', null=True, blank=True, related_name='slabs')
    dental_coverage = models.ForeignKey(
        'DentalCoverage', null=True, blank=True, related_name='slabs')
    family_coverage = models.ForeignKey(
        'FamilyCoverage', null=True, blank=True, related_name='slabs')
    network_hospital_daily_cash = models.ForeignKey(
        'NetworkHospitalDailyCash', null=True, blank=True, related_name='slabs')
    ambulance_charges = models.ForeignKey(
        'AmbulanceCharges', null=True, blank=True, related_name='slabs')
    alternative_practices_coverage = models.ForeignKey(
        'AlternativePracticesCoverage', null=True, blank=True, related_name='slabs')
    maternity_cover = models.ForeignKey(
        'MaternityCover', null=True, blank=True, related_name='slabs')
    eye_coverage = models.ForeignKey(
        'EyeCoverage', null=True, blank=True, related_name='slabs')
    day_care = models.ForeignKey(
        'DayCare', null=True, blank=True, related_name='slabs')
    age_limit = models.ForeignKey(
        'AgeLimit', null=True, blank=True, related_name='slabs')
    online_availability = models.ForeignKey(
        'OnlineAvailability', null=True, blank=True, related_name='slabs')
    medical_required = models.ForeignKey(
        'MedicalRequired', null=True, blank=True, related_name='slabs')
    standard_exclusion = models.ForeignKey(
        'StandardExclusion', null=True, blank=True, related_name='slabs')
    domiciliary_hospitalization = models.ForeignKey(
        'DomiciliaryHospitalization', null=True, blank=True, related_name='slabs')
    bonus = models.ForeignKey(
        'Bonus', null=True, blank=True, related_name='slabs')
    premium_discount = models.ForeignKey(
        'PremiumDiscount', null=True, blank=True, related_name='slabs')
    convalescence_benefit = models.ForeignKey(
        'ConvalescenceBenefit', null=True, blank=True, related_name='slabs')
    non_network_hospital_daily_cash = models.ForeignKey(
        'NonNetworkHospitalDailyCash', null=True, blank=True, related_name='slabs')
    pre_post_hospitalization = models.ForeignKey(
        'PrePostHospitalization', null=True, blank=True, related_name='slabs')
    copay = models.ForeignKey(
        'Copay', null=True, blank=True, related_name='slabs')
    health_checkup = models.ForeignKey(
        'HealthCheckup', null=True, blank=True, related_name='slabs')
    claims = models.ForeignKey(
        'Claims', null=True, blank=True, related_name='slabs')
    outpatient_benefits = models.ForeignKey(
        'OutpatientBenefits', null=True, blank=True, related_name='slabs')
    organ_donor = models.ForeignKey(
        'OrganDonor', null=True, blank=True, related_name='slabs')
    life_long_renewability = models.ForeignKey(
        'LifeLongRenewability', null=True, blank=True, related_name='slabs')
    critical_illness_coverage = models.ForeignKey(
        'CriticalIllnessCoverage', null=True, blank=True, related_name='slabs')
    restore_benefits = models.ForeignKey(
        'RestoreBenefits', null=True, blank=True, related_name='slabs')
    special_feature = models.ForeignKey(
        'SpecialFeature', null=True, blank=True, related_name='slabs')
    # Single parameters
    waiting_period_at_inception_of_ci = models.IntegerField(
        _('Waiting period after which Critical Illness is covered'), null=True, blank=True)
    survival_period_for_ci_after_diagnosis = models.IntegerField(
        _('Survival period after diagnosis, only after which you get cover'), null=True, blank=True)

    class Meta:
        unique_together = (
            ('health_product', 'sum_assured', 'deductible', 'grade'),)
        ordering = ['deductible', 'sum_assured', 'grade']

    def __unicode__(self):
        return "%s-%s (%s)" % (self.sum_assured, self.grade, self.id)

    def clone(self, deep_copy=True, new_health_product=None):
        """ If its deep copy, you must supply the new health_product
        instance, the slab needs to be attached to
        """
        original_id = self.id
        self.id = None
        if deep_copy:
            self.health_product = new_health_product
        else:
            self.grade = "%s - COPY" % (self.grade)
        self.save()
        original = self.__class__.objects.get(id=original_id)

        foreign_keys_to_clone = [
            f.name for f in self._meta.fields if f.__class__.__name__ == 'ForeignKey']
        foreign_keys_to_clone.remove('health_product')

        if not deep_copy:
            for fkey in foreign_keys_to_clone:
                obj = getattr(original, fkey, None)
                if obj:
                    setattr(self, fkey, obj)

        if not deep_copy:
            # If slab is cloned, it clones and points to the existing m2m
            # instances
            m2m_fields = ['cover_premium_table', 'family_cover_premium_table',
                          'zonal_room_rent', 'room_type_copay',
                          'zonal_copay', 'cancellation_policy']
            for m2m in m2m_fields:
                if getattr(original, m2m).all():
                    getattr(self, m2m).add(*list(getattr(original, m2m).all()))
        return self


class TermParent(models.Model):
    name = models.CharField(max_length=255, unique=True)
    is_enabled = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name


class Term(models.Model):
    TERM_TYPE_CHOICES = (
        ('DISEASE', 'DISEASE'),
        ('TREATMENT', 'TREATMENT'),
        ('PROCEDURE', 'PROCEDURE'),
        ('CONDITION', 'CONDITION'),
    )
    parent = models.ForeignKey('TermParent', null=True, blank=True)
    name = models.CharField(max_length=255, unique=True)
    description = models.TextField(blank=True)
    term_type = models.CharField(
        _('Term Type'), choices=TERM_TYPE_CHOICES, blank=True, null=True, max_length=100)

    # is_benchmark ~ ib_
    ib_standard_exclusions = models.BooleanField(default=False)
    ib_critical_illness = models.BooleanField(default=False)
    ib_general = models.BooleanField(default=False)
    ib_pre_existing = models.BooleanField(default=False)
    ib_domiciliary = models.BooleanField(default=False)
    ib_dental_coverage = models.BooleanField(default=False)
    ib_eye_coverage = models.BooleanField(default=False)
    ib_day_care = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name


class ProcedureCovered(CloneableModel):
    term = models.ForeignKey('Term')
    verbose = models.TextField(blank=True)
    code = models.TextField(blank=True)

    def __unicode__(self):
        return "%s - %s" % (self.term.name, self.verbose)


class ConditionalDiseaseModel(CloneableModel):
    period = models.IntegerField(
        _('Waiting period (months)'), null=True, blank=True)
    limit = models.IntegerField(
        _('Limit by value (INR)'), null=True, blank=True)
    limit_in_percentage = models.FloatField(
        _('Limit in % of sum assured'), null=True, blank=True)
    copay = models.FloatField(
        _('Copay applicable for disease'), null=True, blank=True)
    verbose = models.TextField(blank=True)
    code = models.TextField(blank=True)

    class Meta:
        abstract = True


class WaitingPeriodPreExisting(ConditionalDiseaseModel):
    term = models.ForeignKey('Term')

    def __unicode__(self):
        return "%s, period:%s, limit-%s, percentage limit:%s, copay:%s" % (self.term.name, self.period, self.limit, self.limit_in_percentage, self.copay)


class WaitingPeriodGeneral(ConditionalDiseaseModel):
    term = models.ForeignKey('Term')

    def __unicode__(self):
        return "%s, period:%s, limit-%s, percentage limit:%s, copay:%s" % (self.term.name, self.period, self.limit, self.limit_in_percentage, self.copay)


class AlternativeMedical(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)

    def __unicode__(self):
        return self.name


class InfraAbstract(models.Model):
    name = models.CharField(max_length=200)
    insurers = models.ManyToManyField(Insurer, blank=True)
    city = models.ForeignKey(City)
    address = models.TextField()
    pin = models.CharField(max_length=200)

    class Meta:
        abstract = True

    def __unicode__(self):
        return "%s, %s" % (self.name, self.city.name)


class Hospital(InfraAbstract):
    district = models.CharField(max_length=200, blank=True)
    landmark = models.CharField(max_length=200, blank=True, null=True)

    state = models.ForeignKey(State)

    std_code = models.CharField(
        _("STD Code"), max_length=10, blank=True, null=True)
    phone1 = models.CharField(
        _("Phone"), max_length=255, blank=True, null=True)
    phone2 = models.CharField(max_length=255, blank=True, null=True)
    fax1 = models.CharField(max_length=255, blank=True, null=True)
    fax2 = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(blank=True)
    url = models.CharField(max_length=200, blank=True)

    lat = models.FloatField(blank=True, null=True)
    lng = models.FloatField(blank=True, null=True)
    googledata = models.TextField(blank=True)


class CancellationPolicy(CloneableModel):
    "Cancellation Policy"
    slabs = models.ManyToManyField('Slab', related_name="cancellation_policy")
    # years of policy premium has been paid
    policy_years = models.IntegerField(
        _('Years for which premium has been paid'))
    period = models.IntegerField(_('Upto the month you are cancelling policy'))
    refund_in_percentage = models.FloatField(
        _('Refund in percentage of premium'))

    def __unicode__(self):
        return "%s" % self.id


class FamilyCoverPremiumTable(CloneableModel):
    GENDER_CHOICES = (
        ('MALE', 'Male'),
        ('FEMALE', 'Female'),
        ('', 'Not Applicable')
    )
    LOCATION_CHOICES = (
        ('METRO', 'Metro'),
        ('NONMETRO', 'Non-Metro'),
        ('', 'Not Applicable'),
    )
    PAYMENT_PERIOD_CHOICES = (
        (1, 1),
        (2, 2),
    )
    slabs = models.ManyToManyField(
        'Slab', related_name="family_cover_premium_table")
    payment_period = models.IntegerField(
        _('period'), choices=PAYMENT_PERIOD_CHOICES, default=1)
    adults = models.IntegerField(_('Adults'), null=True, blank=True)
    kids = models.IntegerField(_('Kids'), null=True, blank=True)
    lower_age_limit = models.IntegerField(choices=AGE_LIMITS_1)
    upper_age_limit = models.IntegerField(
        null=True, blank=True, choices=AGE_LIMITS_1)
    gender = models.CharField(
        _('Gender'), choices=GENDER_CHOICES, blank=True, null=True, max_length=100)
    state = models.ManyToManyField('State', blank=True)
    city = models.ManyToManyField('City', blank=True)
    two_year_discount = models.FloatField(
        _('2year discount'), null=True, blank=True)
    base_premium = models.FloatField(null=True, blank=True)
    service_tax = models.FloatField(null=True, blank=True)
    _premium = models.FloatField(db_column="premium")

    @property
    def premium(self):
        if self._premium == None:
            return 0
        return round(self._premium*(1+settings.SERVICE_TAX_PERCENTAGE/100))

    """
    @premium.setter
    def premium(self, value):
        self._premium = value
    """

    def __unicode__(self):
        return "%s, %s-%s, %s" % (self.payment_period, self.adults, self.kids, self.lower_age_limit)

    def clone(self):
        original_id = self.id
        self.id = None
        self.save()
        original = self.__class__.objects.get(id=original_id)
        self.city.add(*list(original.city.all()))
        self.state.add(*list(original.state.all()))
        return self


class CoverPremiumTable(CloneableModel):
    GENDER_CHOICES = (
        ('MALE', 'Male'),
        ('FEMALE', 'Female'),
        ('', 'Not Applicable')
    )
    LOCATION_CHOICES = (
        ('METRO', 'Metro'),
        ('NONMETRO', 'Non-Metro'),
        ('', 'Not Applicable'),
    )
    PAYMENT_PERIOD_CHOICES = (
        (1, 1),
        (2, 2),
    )
    slabs = models.ManyToManyField('Slab', related_name="cover_premium_table")
    payment_period = models.IntegerField(
        _('period'), choices=PAYMENT_PERIOD_CHOICES, default=1)
    lower_age_limit = models.IntegerField(choices=AGE_LIMITS_1)
    upper_age_limit = models.IntegerField(
        null=True, blank=True, choices=AGE_LIMITS_1)
    gender = models.CharField(
        _('gender'), choices=GENDER_CHOICES, blank=True, null=True, max_length=100)
    state = models.ManyToManyField('State', blank=True)
    city = models.ManyToManyField('City', blank=True)
    two_year_discount = models.FloatField(
        _('2 year discount'), null=True, blank=True)
    base_premium = models.FloatField(null=True, blank=True)
    service_tax = models.FloatField(null=True, blank=True)
    _premium = models.FloatField(db_column="premium")

    @property
    def premium(self):
        if self._premium == None:
            return 0
        return round(self._premium*(1+settings.SERVICE_TAX_PERCENTAGE/100))

    """
    @premium.setter
    def premium(self, value):
        self._premium = value
    """

    def __unicode__(self):
        return "%s, %s" % (self.payment_period, self.lower_age_limit)

    def clone(self):
        original_id = self.id
        self.id = None
        self.save()
        original = self.__class__.objects.get(id=original_id)
        self.city.add(*list(original.city.all()))
        self.state.add(*list(original.state.all()))
        return self


class AgeLimit(CloneableModel):
    """Age limits per slab"""
    minimum_entry_age = models.IntegerField(
        _('Minimum Entry Age'), null=True, blank=True, choices=AGE_LIMITS_1)  # 0 if all covered
    maximum_entry_age = models.IntegerField(
        _('Maximum Entry Age'), blank=True, null=True, choices=AGE_LIMITS_1)  # blank if no-age based criteria

    def __unicode__(self):
        return "%s - %s" % (self.minimum_entry_age, self.maximum_entry_age)


class OnlineAvailability(CloneableModel):
    """Online availability per slab"""
    is_available_online = models.NullBooleanField(
        _('Is slab available online(for all ages)'), choices=((False, "No"), (True, "Yes")))
    maximum_age = models.IntegerField(
        _('Age above which this slab is only available offline'), blank=True, null=True, choices=AGE_LIMITS)

    def __unicode__(self):
        return "%s" % (self.maximum_age)


class MedicalRequired(CloneableModel):
    """Meical required for slab after a certain age"""
    is_medical_required = models.NullBooleanField(
        _('Is medical required for this slab'), choices=((False, "No"), (True, "Yes")))
    maximum_age = models.IntegerField(
        _('Age above which medicals are required'), blank=True, null=True, choices=AGE_LIMITS)


class FamilyCoverage(CloneableModel):
    """Family Coverage features for health plan"""
    number_of_maximum_members = models.IntegerField(
        _('Maximum members for family cover'), blank=True, null=True)
    # blank if no-age based criteria
    maximum_dependent_age = models.IntegerField(
        _('Maximum dependent age for family cover'), blank=True, null=True, choices=AGE_LIMITS)


class DayCare(models.Model):
    """Day Care"""
    is_covered = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))
    defined_list = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))
    number_of_covered_procedures = models.IntegerField(blank=True, null=True)
    procedure_covered = models.ManyToManyField('ProcedureCovered', blank=True)

    def clone(self):
        original_id = self.id
        self.id = None
        self.save()
        original = self.__class__.objects.get(id=original_id)
        for pc in original.procedure_covered.all():
            self.procedure_covered.add(pc.clone())
        return self


class General(models.Model):
    """General Waiting Period"""
    waiting_period = models.IntegerField(
        _('General waiting period except accidents (in months)'), blank=True, null=True)
    conditions = models.ManyToManyField('WaitingPeriodGeneral', blank=True)
    portability = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))

    def clone(self):
        original_id = self.id
        self.id = None
        self.save()
        original = self.__class__.objects.get(id=original_id)

        for bd in original.conditions.all():
            self.conditions.add(bd.clone())

        return self


class PreExisting(models.Model):
    """Waiting Period"""
    waiting_period = models.IntegerField(
        _('General waiting period for pre-existing conditions (in months)'), null=True, blank=True)
    conditions = models.ManyToManyField('WaitingPeriodPreExisting', blank=True)
    portability = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))

    def clone(self):
        original_id = self.id
        self.id = None
        self.save()
        original = self.__class__.objects.get(id=original_id)

        for pd in original.conditions.all():
            self.conditions.add(pd.clone())

        return self


class MaternityCover(CloneableModel):
    """Maternity Cover"""
    is_covered = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))
    waiting_period = models.IntegerField(
        _('Waiting period(in months)'), null=True, blank=True)
    limit_for_normal_delivery = models.IntegerField(null=True, blank=True)
    limit_for_cesarean_delivery = models.IntegerField(null=True, blank=True)
    delivery_limit = models.IntegerField(null=True, blank=True)


class CriticalIllnessCoverage(models.Model):
    INCLUDE_IN_PLAN_CHOICES = (
        (True, "Included in SA"),
        (False, "Over and above SA"),
    )
    BENEFIT_TYPE_CHOICES = (
        ('LUMP_SUM', 'Lump sum amount'),
        ('REIMBURSEMENT', 'Reimbursement of expenditure on treatment'),
    )
    is_available = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))
    limit = models.IntegerField(
        _('CI cover as value (e.g. 500000)'), null=True, blank=True)
    limit_in_percentage = models.FloatField(
        _('CI cover as % of sum assured (e.g. 80)'), null=True, blank=True)
    included_in_plan = models.NullBooleanField(
        _("Benefit condition"), choices=INCLUDE_IN_PLAN_CHOICES, null=True, blank=True)
    benefit_type = models.CharField(
        _("Benefit type"), choices=BENEFIT_TYPE_CHOICES, max_length=255, blank=True)
    terms = models.ManyToManyField('Term', blank=True)

    def clone(self):
        original_id = self.id
        self.id = None
        self.save()
        original = self.__class__.objects.get(id=original_id)

        self.terms.add(*list(original.terms.all()))
        return self


class StandardExclusion(models.Model):
    """Standard Exclusions"""
    terms = models.ManyToManyField('Term', blank=True)

    def clone(self):
        original_id = self.id
        self.id = None
        self.save()
        original = self.__class__.objects.get(id=original_id)

        self.terms.add(*list(original.terms.all()))

        return self


class LifeLongRenewability(CloneableModel):
    """Lifelong Renewability"""
    RENEWABILITY_CHOICES = (
        ('NO', 'Not Renewable, Maximum age limit applicable to renew'),
        ('AGE_LIMIT_ON_INDEMNITY_BENEFIT',
         'Subject to age limit for indemnity benefit'),
        ('AGE_LIMIT_ON_CRITICAL_ILLNESS', 'Subject to age limit on Critical illness'),
        ('YES', 'Yes - Lifelong Renewable'),
    )
    condition = models.CharField(
        choices=RENEWABILITY_CHOICES, max_length=255, blank=True)
    age_limit = models.IntegerField(blank=True, null=True)


class AmbulanceCharges(CloneableModel):
    """Ambulance charges"""
    AMBULANCE_CHARGES_CHOICES = (
        ('NO', 'No'),
        ('EMERGENCY_ONLY', 'Available for emergency only'),
        ('YES', 'Available for all cases'),
    )
    condition = models.CharField(
        choices=AMBULANCE_CHARGES_CHOICES, max_length=255, blank=True)
    is_ambulance_services_covered = models.NullBooleanField(
        _('Are 3rd party ambulance services covered'), choices=((False, "No"), (True, "Yes")))
    # Leave blank if charges not applicable
    limit_in_network_hospitals = models.IntegerField(
        _('Limit on ambulance charges in network hospitals'), null=True, blank=True)
    limit_in_non_network_hospitals = models.IntegerField(
        _('Limit on ambulance charges in non-network hospitals'), null=True, blank=True)
    max_limit_in_network_hospitals = models.IntegerField(
        _('Maximum limit on ambulance charges in network hospitals'), null=True, blank=True)
    max_limit_in_non_network_hospitals = models.IntegerField(
        _('Maximum limit on ambulance charges in non-network hospitals'), null=True, blank=True)


class AlternativePracticesCoverage(CloneableModel):
    """Alternative Medical Coverage"""
    alternative_practices = models.ManyToManyField(
        'AlternativeMedical', blank=True)
    limit_in_percentage = models.FloatField(blank=True, null=True)
    limit = models.IntegerField(blank=True, null=True)


class OrganDonor(CloneableModel):
    """Organ Donor Coverage"""
    is_covered = models.NullBooleanField(
        _('Is covered'), choices=((False, "No"), (True, "Yes")))
    is_surgery_covered = models.NullBooleanField(
        _('Is surgery covered'), choices=((False, "No"), (True, "Yes")),
        blank=True, null=True)
    limit = models.IntegerField(_('Limit'), blank=True, null=True)
    limit_in_percentage = models.FloatField(
        _('Limit in percentage of SA'), blank=True, null=True)


class DomiciliaryHospitalization(models.Model):
    is_covered = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))
    limit = models.IntegerField(_("Limit"), blank=True, null=True)
    limit_in_percentage = models.FloatField(
        _("Limit as percent of SA"), blank=True, null=True)
    exclusions = models.ManyToManyField('Term', blank=True)

    def clone(self):
        original_id = self.id
        self.id = None
        self.save()
        original = self.__class__.objects.get(id=original_id)

        self.exclusions.add(*list(original.exclusions.all()))
        return self


class RestoreBenefits(CloneableModel):
    """Restore Benefits"""
    is_available = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))
    amount_restored = models.IntegerField(
        _('Amount restored in value'), null=True, blank=True)
    amount_restored_percentage = models.FloatField(
        _('Amount restored in % of SA'), null=True, blank=True)


class OutpatientBenefits(CloneableModel):
    """Outpatient Benefits"""
    is_covered = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))
    number_of_consultations = models.IntegerField(blank=True, null=True)
    limit = models.IntegerField(
        _('Maximum limit on OPD cover'), blank=True, null=True)


class DentalCoverage(models.Model):
    """Dental Coverage"""
    DENTAL_COVERAGE_CHOICES = (
        ('NO', 'No'),
        ('YES', 'Yes'),
    )
    condition = models.CharField(
        choices=DENTAL_COVERAGE_CHOICES, max_length=255, blank=True)
    copay = models.FloatField(null=True, blank=True)
    limit = models.IntegerField(
        _('Maximum cover for dental treatment'), blank=True, null=True)
    waiting_period = models.IntegerField(
        _('Waiting period in months'), null=True, blank=True)
    exclusions = models.ManyToManyField('Term', blank=True)

    def clone(self):
        original_id = self.id
        self.id = None
        self.save()
        original = self.__class__.objects.get(id=original_id)

        self.exclusions.add(*list(original.exclusions.all()))
        return self


class EyeCoverage(models.Model):
    """Eye Coverage"""
    EYE_COVERAGE_CHOICES = (
        ('NO', 'No'),
        ('WITH_COPAY_WAITING_PERIOD', 'With Co-pay and Waiting Period'),
        ('WITH_COPAY', 'With Copay'),
    )
    condition = models.CharField(
        choices=EYE_COVERAGE_CHOICES, max_length=255, blank=True)
    copay = models.FloatField(null=True, blank=True)
    limit = models.IntegerField(
        _('Maximum cover for eye treatment'), blank=True, null=True)
    waiting_period = models.IntegerField(
        _('Waiting period in months'), null=True, blank=True)
    exclusions = models.ManyToManyField('Term', blank=True)

    def clone(self):
        original_id = self.id
        self.id = None
        self.save()
        original = self.__class__.objects.get(id=original_id)

        self.exclusions.add(*list(original.exclusions.all()))
        return self


class Bonus(CloneableModel):
    """Bonus"""
    BONUS_CALCULATION_METHOD_CHOICES = (
        ("CUMMULATIVE", "Cummulative"),
        ("ONBASE", "On base SA")
    )
    is_available = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))
    # TODO: add field to know if its simple or compound
    unclaimed_years = models.IntegerField(
        _('Unclaimed years after which bonus starts'), null=True, blank=True)
    bonus_amount = models.CharField(
        _('Amount of bonus in % of SA e.g 10 or (60, 60, 10, 10, 10)'), blank=True, max_length=255)
    maximum_bonus_amount = models.FloatField(
        _('Maximum bonus in % of SA e.g 50'), blank=True, null=True)
    maximum_years = models.IntegerField(
        _('Maximum years after which bonus is not given'), blank=True, null=True)
    reduction_in_bonus_amount_after_claim = models.CharField(
        _('Reduction in bonus (as % of SA) after claim e.g. 20 or (60,60,10,10,10)'), blank=True, max_length=255)
    bonus_calculation_method = models.CharField(
        choices=BONUS_CALCULATION_METHOD_CHOICES, max_length=100, blank=True)


class PremiumDiscount(CloneableModel):
    """Premium Discount"""
    DISCOUNT_CALCULATION_METHOD_CHOICES = (
        ("CUMMULATIVE", "Cummulative"),
        ("ONBASE", "On base SA")
    )
    is_available = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))
    unclaimed_years = models.IntegerField(
        _('Unclaimed years after which premium discount starts'), null=True, blank=True)
    discount_amount = models.FloatField(
        _('Discount in % of premium e.g 5'), blank=True, null=True)
    maximum_discount_amount = models.FloatField(
        _('Maximum discount in % e.g 25'), blank=True, null=True)
    maximum_years = models.IntegerField(
        _('Maximum years after which discount is not given'), blank=True, null=True)
    reduction_in_discount_amount_after_claim = models.FloatField(
        _('Reduction in discount after claim e.g. 5%, 100% means no discount if claimed'), null=True, blank=True)
    discount_calculation_method = models.CharField(
        choices=DISCOUNT_CALCULATION_METHOD_CHOICES, max_length=100, blank=True)


class HospitalDailyCash(CloneableModel):
    """Hospitalization Daily Cash"""
    DAILY_CASH_ALLOWANCE_CHOICES = (
        ('NO', 'No'),
        ('AVAILABLE_WITH_SHARING_ONLY', 'Available if you choose sharing room'),
        ('YESWL', 'Yes - With Limits'),
    )
    is_available = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))
    condition = models.CharField(
        choices=DAILY_CASH_ALLOWANCE_CHOICES, max_length=255, blank=True)
    is_icu_available = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))

    limit_per_day_allowance_icu = models.IntegerField(
        _('Per day allowance for ICU'), null=True, blank=True)
    limit_per_day_allowance = models.IntegerField(
        _('Per day allowance'), null=True, blank=True)
    limit_cummulative_allowance = models.IntegerField(
        _('Cummulative allowance limit'), null=True, blank=True)

    from_day = models.IntegerField(
        _('From the day allowance is applicable'), null=True, blank=True)
    to_day = models.IntegerField(
        _('To the day allowance is applicable'), null=True, blank=True)
    number_of_days = models.IntegerField(
        _('Number of days limit on allowance'), null=True, blank=True)

    class Meta:
        abstract = True


class NetworkHospitalDailyCash(HospitalDailyCash):
    """If no different features for non-network hospitals, only fill this entry"""
    pass


class NonNetworkHospitalDailyCash(HospitalDailyCash):
    """If no different features for non-network hospitals, leave this empty"""
    pass


class ConvalescenceBenefit(CloneableModel):
    """Convalescence Benefit: When you are recuperating from hospitalization, this gives to cash for that period """
    CONVALESCENCE_BENEFIT_CHOICES = (
        ('NO', 'No'),
        ('AVAILABLE_PER_DAY', 'Available for per day of hopistalization'),
        ('AVAILABLE_LUMPSUM', 'Available lumpsum'),
    )
    condition = models.CharField(
        choices=CONVALESCENCE_BENEFIT_CHOICES, max_length=255, blank=True)
    # Amount is per day amount or lumpsum amount depending on the condition
    limit = models.IntegerField(
        _("Maximum limit for the benefit"), null=True, blank=True)
    minimum_days_of_hospitalization = models.IntegerField(
        _('Minimum days of hospitalization required to avail benefit'), null=True, blank=True)
    amount_per_day = models.IntegerField(
        _("Amount paid for per day of hospitalization"), null=True, blank=True)
    maximum_number_of_days = models.IntegerField(
        _('Maximum days for which amount will be paid if benefit available per day basis'), null=True, blank=True)


class PrePostHospitalization(CloneableModel):
    """Pre Post Hospitalization : Limited by Sum Assured unless otherwise mentioned"""
    is_available = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))
    intimation_time = models.IntegerField(null=True, blank=True)
    days_pre_hospitalization = models.IntegerField(null=True, blank=True)
    days_post_hospitalization = models.IntegerField(null=True, blank=True)
    days_pre_hospitalization_with_intimation = models.IntegerField(
        null=True, blank=True)
    days_post_hospitalization_with_intimation = models.IntegerField(
        null=True, blank=True)
    cummulative_limit = models.IntegerField(_('Limit'), null=True, blank=True)
    cummulative_limit_in_percentage = models.FloatField(
        _('Limit in percentage'), null=True, blank=True)


class Copay(CloneableModel):
    """Copay as percentage of SA"""
    is_available = models.NullBooleanField(
        _('Yes if ANY copay is present, Zonal or Room rent type'), choices=((False, "No"), (True, "Yes")))
    # Age based copay
    applicable_after_age = models.IntegerField(
        _('Co-pay is applicable after age, 0 if general copay'), null=True, blank=True, choices=AGE_LIMITS)
    applicable_after_entry_age = models.IntegerField(
        _('Co-pay applicable if entry age >='), null=True, blank=True, choices=AGE_LIMITS)
    age_copay_network_hospital = models.FloatField(
        _('Age copay network hospital'), null=True, blank=True)
    age_copay_non_network_hospital = models.FloatField(
        _('Age copay non-network hospital'), null=True, blank=True)

    # Network Hospitals & Non-Network Hospitals
    for_network_hospital = models.FloatField(
        _('Co-pay in network hospital (independent of age criteria)'), null=True, blank=True)
    for_non_network_hospital = models.FloatField(
        _('Co-pay in non-network hospital (independent of age criteria)'), null=True, blank=True)

    # on day care only
    on_day_care_for_network_hospital = models.IntegerField(
        _('Co-pay on day care in network hospital'), null=True, blank=True)
    on_day_care_for_non_network_hospital = models.IntegerField(
        _('Co-pay on day care in non-network hospital'), null=True, blank=True)


class ZonalCopay(CloneableModel):
    slabs = models.ManyToManyField('Slab', related_name='zonal_copay')
    state = models.ManyToManyField('State', blank=True)
    city = models.ManyToManyField('City', blank=True)
    zonal_copay_for_network_hospital = models.FloatField(
        _('Zonal Co-pay in network hospital'), null=True, blank=True)
    zonal_copay_for_non_network_hospital = models.FloatField(
        _('Zonal Co-pay in network hospital'), null=True, blank=True)


class RoomTypeCopay(CloneableModel):
    """Room Type Copay : Room rent limits are on particular rooms with some copay involved"""
    slabs = models.ManyToManyField('Slab', related_name='room_type_copay')
    ROOM_TYPE_CHOICES = (
        ('CLASSA', 'Single pvt. room without conditions'),
        ('CLASSB', 'Lowest cost single pvt. room + 1'),
        ('CLASSC', 'Lowest cost single pvt. room'),
        ('CLASSD', 'Shared Room Only'),
    )
    HOSPITAL_TYPE_CHOICES = (
        ("BOTH", "Network and non-network both"),
        ("NETWORK", "Network Hospital"),
        ("NONNETWORK", "Non-Network Hospital"),
    )
    room_type = models.CharField(
        choices=ROOM_TYPE_CHOICES, max_length=255, blank=True)
    verbose_room_type = models.CharField(
        _('Room as mentioned in policy'), max_length=255, blank=True)
    copay = models.FloatField(_('Copay condition'))
    applicable_for_hospital = models.CharField(
        _("Applicable for hospital in"), max_length=100, choices=HOSPITAL_TYPE_CHOICES)


class ZonalRoomRent(CloneableModel):
    """Zonal Room Rent Conditions"""
    ROOM_RENT_CHOICES = (
        ('NO_LIMIT', 'No Limit'),
        ('SA_OR_VALUE_LIMIT', 'Limits on SA or value'),
        ('CLASSA', 'Single pvt. room without conditions'),
        ('CLASSB', 'Lowest cost single pvt. room + 1'),
        ('CLASSC', 'Lowest cost single pvt. room'),
        ('CLASSD', 'Shared Room Only'),
    )
    slabs = models.ManyToManyField('Slab', related_name='zonal_room_rent')
    state = models.ManyToManyField('State', blank=True)
    city = models.ManyToManyField('City', blank=True)

    conditions = models.CharField(
        choices=ROOM_RENT_CHOICES, max_length=255, blank=True)
    room_available = models.CharField(
        _('Room as mentioned in policy'), max_length=255, blank=True)
    limit_daily = models.IntegerField(
        _('Daily room rent limit on sum assured in INR'), null=True, blank=True)
    limit_daily_in_percentage = models.FloatField(
        _('Daily room rent limit as % of sum assured'), null=True, blank=True)
    limit_daily_in_icu = models.IntegerField(
        _('ICU daily room rent limit in INR'), null=True, blank=True)
    limit_daily_in_percentage_in_icu = models.FloatField(
        _('ICU daily room rent limit as % of sum assured'), null=True, blank=True)
    limit_maximum_total = models.IntegerField(
        _('Maximum limit on total room rent benefit'), null=True, blank=True)
    limit_maximum_total_in_icu = models.IntegerField(
        _('ICU maximum limit on total room rent benefit'), null=True, blank=True)


class HealthCheckup(CloneableModel):
    """Free Health Checkup"""
    is_available = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))
    # If claim free years is zero, but is_available is True. Its available
    # EVERY year
    claim_free_years = models.IntegerField(
        _('Claim free years after which free check-up is available'), null=True, blank=True)
    limit_in_percentage = models.FloatField(
        _('Amount in percentage of sum assured, for check up'), null=True, blank=True)
    limit = models.IntegerField(
        _('Amount in INR for checkup'), null=True, blank=True)


class Claims(CloneableModel):
    """Claims Services"""
    CLAIMS_SERVICE_PROMISE_CHOICES = (
        ('NO', 'No'),
        ('TATC', 'TAT Commited'),
        ('TAT_COMMITED_WITH_PENALTY',
         'TAT Commited with penalty payable by insurer if not adhered to'),
    )
    service_promise = models.CharField(
        choices=CLAIMS_SERVICE_PROMISE_CHOICES, max_length=255, blank=True)
    tat_commited = models.IntegerField(_('TAT in days'), null=True, blank=True)
    tat_commited_penalty = models.TextField(
        _('TAT penalty mentioned'), blank=True)
    intimation_for_cashless = models.IntegerField(
        _('Intimation for cashless in hours (before hospitalization)'), null=True, blank=True)
    intimation_for_emergency = models.IntegerField(
        _('Intimation of emergency in hours (after hospitalization)'), null=True, blank=True)


class MetaData(CloneableModel):
    """Slab wise meta data for every health product """
    slabs = models.ManyToManyField('Slab')
    content_type = models.ForeignKey(ContentType)

    code = models.TextField(blank=True)
    verbose = models.TextField(_('Verbose'), blank=True)
    text_to_display = models.TextField(_('Text to display'), blank=True)
    comments = models.TextField(_('Comments'), blank=True)
    is_completed = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))


class ProductMetaData(CloneableModel):
    """Meta data at health product level"""
    health_product = models.ForeignKey('HealthProduct')
    content_type = models.ForeignKey(ContentType)

    code = models.TextField(blank=True)
    verbose = models.TextField(_('Verbose'), blank=True)
    text_to_display = models.TextField(_('Text to display'), blank=True)
    comments = models.TextField(_('Comments'), blank=True)
    is_completed = models.NullBooleanField(
        choices=((False, "No"), (True, "Yes")))

    class Meta:
        unique_together = (('health_product', 'content_type'),)


class SpecialFeature(CloneableModel):
    feature = models.TextField(blank=True)


class MedicalScore(models.Model):
    health_product = models.ForeignKey(
        'HealthProduct', related_name='medical_score')
    slabs = models.ManyToManyField('Slab', related_name='medical_score')

    general_waiting_period = models.FloatField(
        _('Initial waiting period for diseases'), blank=True, null=True)
    specific_waiting_period = models.FloatField(
        _('Two year waiting period'), blank=True, null=True)
    disease_condition = models.FloatField(
        _('Other conditions on disease'), blank=True, null=True)

    ped_waiting_period = models.FloatField(
        _('PED initial waiting period'), blank=True, null=True)
    specific_ped_period = models.FloatField(
        _('Other PED waiting period'), blank=True, null=True)
    ped_condition = models.FloatField(
        _("Other conditions on PED"), blank=True, null=True)

    standard_exclusion = models.FloatField(
        _("Standard Exclusions"), blank=True, null=True)
    critical_illness_coverage = models.FloatField(
        _("Critical Illness"), blank=True, null=True)
    day_care = models.FloatField(_("Day Care"), blank=True, null=True)
    domiciliary_hospitalization = models.FloatField(blank=True, null=True)
    dental_coverage = models.FloatField(blank=True, null=True)
    eye_coverage = models.FloatField(blank=True, null=True)

AttributeDict = dict([(sl.name, sl.rel.to.__name__) for sl in Slab._meta.fields if getattr(
    sl, 'rel', '') if sl.name not in ['health_product']])
ManyAttributeDict = {
    'cover_premium_table': 'CoverPremiumTable',
    'family_cover_premium_table': 'FamilyCoverPremiumTable',
    'room_type_copay': 'RoomTypeCopay',
    'zonal_copay': 'ZonalCopay',
    'zonal_room_rent': 'ZonalRoomRent',
    'cancellation_policy': 'CancellationPolicy',
}
AttributeDict.update(ManyAttributeDict)
ReverseAttributeDict = dict([(v, k) for k, v in AttributeDict.items()])

# To be used for ordering
AttributeList = [
    #[Model, TITLE, VARIABLE]
    ['CoverPremiumTable', 'Cover Premium Table', 'cover_premium_table'],
    ['FamilyCoverPremiumTable', 'Cover Premium Table', 'family_cover_premium_table'],
    ['OnlineAvailability', 'Online Available', 'online_availability'],
    ['MedicalRequired', 'Medical Required', 'medical_required'],
    ['AgeLimit', 'Age Limit', 'age_limit'],
    ['FamilyCoverage', 'Family Coverage', 'family_coverage'],
    ['General', 'Waiting Period and Disease Limits', 'general'],
    ['PreExisting', 'Pre-existing', 'pre_existing'],
    ['MaternityCover', 'Maternity Cover', 'maternity_cover'],
    ['StandardExclusion', 'Standard Exclusion', 'standard_exclusion'],
    ['LifeLongRenewability', 'Life Long Renewability', 'life_long_renewability'],
    ['AmbulanceCharges', 'Ambulance Charges', 'ambulance_charges'],
    ['AlternativePracticesCoverage', 'Alternative Medical Coverage',
        'alternative_practices_coverage'],
    ['DomiciliaryHospitalization', 'Domiciliary Hospitalization',
        'domiciliary_hospitalization'],
    ['OutpatientBenefits', 'Outpatient Benefits', 'outpatient_benefits'],
    ['OrganDonor', 'Organ Donor', 'organ_donor'],
    ['Bonus', 'Bonus', 'bonus'],
    ['PremiumDiscount', 'Premium Discount', 'premium_discount'],
    ['ConvalescenceBenefit', 'Convalescence Benefit', 'convalescence_benefit'],
    ['PrePostHospitalization', 'Pre Post Hospitalization', 'pre_post_hospitalization'],
    ['Copay', 'Copay', 'copay'],
    ['RoomTypeCopay', 'Room Type Limits & Copay', 'room_type_copay'],
    ['ZonalCopay', 'Zonal Copay', 'zonal_copay'],
    ['ZonalRoomRent', 'Room Rent', 'zonal_room_rent'],
    ['HealthCheckup', 'Health Checkup', 'health_checkup'],
    ['Claims', 'Claims', 'claims'],
    ['CancellationPolicy', 'Cancellation Policy', 'cancellation_policy'],
    ['CriticalIllnessCoverage', 'Critical Illness Coverage',
        'critical_illness_coverage'],
    ['RestoreBenefits', 'Restore Benefits', 'restore_benefits'],
    ['SpecialFeature', 'Special Features', 'special_feature'],
    # Version 1
    ['DayCare', 'Day Care', 'day_care'],
    ['DentalCoverage', 'Dental Coverage', 'dental_coverage'],
    ['EyeCoverage', 'Eye Coverage', 'eye_coverage'],
    ['NetworkHospitalDailyCash', 'Network Hospital Daily Cash',
        'network_hospital_daily_cash'],
    ['NonNetworkHospitalDailyCash', 'Non Network Hospital Daily Cash',
        'non_network_hospital_daily_cash'],
]
PseudoAttributeDict = dict([(k[2], k[0]) for k in AttributeList])
VerboseAttributeDict = dict([(k[2], k[1]) for k in AttributeList])
for k in AttributeDict.keys():
    if AttributeDict[k] != PseudoAttributeDict[k]:
        raise Exception("Problem! %s is not defined in AttributeList" % k)


SECTION_CODE_CHOICES = [
    ('pre_text', 'Block before auto post'),
    ('post_text', 'Block after auto post')
]
SECTION_CODE_CHOICES.extend([(code, name)
                             for code, name in VerboseAttributeDict.items()])


class ProductSection(models.Model):
    product_page = models.ForeignKey('WikiArticle', related_name='section_set')
    code = models.CharField(max_length=250, choices=SECTION_CODE_CHOICES)
    body = models.TextField(blank=True)
    is_active = models.BooleanField(default=True)
    # is_good = models.BooleanField(default=False)
    created_on = models.DateTimeField(_('created'), auto_now_add=True)
    updated_on = models.DateTimeField(_('modified'), auto_now=True)
