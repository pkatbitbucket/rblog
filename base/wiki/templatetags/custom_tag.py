from django import template
from wiki.models import MedicalTransactionPermission


register = template.Library()


@register.filter(name="get_transaction_permission")
def get_transaction_permission(md, hp):
    instance = MedicalTransactionPermission.objects.filter(medical_condition_id=md, health_product_id=hp).first()
    if instance:
        return instance.get_transaction_permission_display()
    return ""
