from django.conf.urls import url

from cf_fhurl import fhurl
from utils import profile_type_only

from . import forms, views

urlpatterns = [
    fhurl(r'^ajax/article/add/$', forms.WikiArticleAddForm, name='ajax_article_add',
          template='wiki/snippets/wiki_article_form.html', json=True),
    fhurl(r'^ajax/article/edit/(?P<article_slug>[\w-]+)/$', forms.WikiArticleAddForm,
          name='ajax_article_edit', template='wiki/snippets/wiki_article_form.html', json=True),
    url(r'^article/add/$', views.article_add, name='article_add'),
    url(r'^article/edit/(?P<article_slug>[\w-]+)/$', views.article_add, name='article_edit'),

    fhurl(r'^ajax/product-section/add/$', forms.SectionAddForm, name='ajax_product_section_add',
          template='wiki/snippets/wiki_product_section_form.html', json=True),
    fhurl(r'^ajax/product-section/edit/(?P<section_id>[\w-]+)/$', forms.SectionAddForm,
          name='ajax_product_section_edit', template='wiki/snippets/wiki_product_section_form.html', json=True),
    url(r'^product-section/add/$', views.product_section_add, name='product_section_add'),
    url(r'^product-section/edit/(?P<section_id>\d+)/$', views.product_section_add, name='product_section_edit'),

    fhurl(r'^ajax/edit/insurer-(?P<insurer_id>\d+)/$', forms.InsurerAddForm,
          decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_insurer_edit',
          template="wiki/insurer_add.html", json=True),

    url(r'^edit/insurer-(?P<insurer_id>\d+)/$', views.insurer_add, name='insurer_edit'),
    fhurl(r'^ajax/add-insurer/$', forms.InsurerAddForm, decorator=profile_type_only('WIKI_DATA_ENTRY'),
          name='ajax_insurer_add', template="wiki/insurer_add.html", json=True),
    url(r'^add-insurer/$', views.insurer_add, name='insurer_add'),

    fhurl(r'^ajax/add-term/$', forms.TermForm, decorator=profile_type_only('WIKI_DATA_ENTRY'),
          name='ajax_add_term', template="wiki/snippets/term_form.html", json=True),
    fhurl(r'^ajax/edit-term/(?P<term_id>\d+)/$', forms.TermForm,
          decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_edit_term',
          template="wiki/snippets/term_form.html", json=True),

    fhurl(r'^ajax/add-parent-term/$', forms.ParentTermForm, decorator=profile_type_only('WIKI_DATA_ENTRY'),
          name='ajax_add_parent_term', template="wiki/snippets/parent_term_form.html", json=True),
    fhurl(r'^ajax/edit-parent-term/(?P<parent_term_id>\d+)/$', forms.ParentTermForm,
          decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_edit_parent_term',
          template="wiki/snippets/parent_term_form.html", json=True),

    fhurl(r'^ajax/insurer-(?P<insurer_id>\d+)/claims-data/$', forms.ClaimsDataForm,
          decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_claims_data_form',
          template="wiki/claims_data_add.html", json=True),

    fhurl(r'^ajax/edit/insurer-(?P<insurer_id>\d+)/health-product-(?P<product_id>\d+)/$',
          forms.HealthProductAddForm, decorator=profile_type_only('WIKI_DATA_ENTRY'),
          name='ajax_health_product_edit', template="wiki/health_product_add.html", json=True),
    url(r'^edit/insurer-(?P<insurer_id>\d+)/health-product-(?P<product_id>\d+)/$',
        views.health_product_add, name='health_product_edit'),
    fhurl(r'^ajax/insurer-(?P<insurer_id>\d+)/add-health-product/$', forms.HealthProductAddForm,
          decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_health_product_add',
          template="wiki/health_product_add.html", json=True),
    url(r'^insurer-(?P<insurer_id>\d+)/add-health-product/$', views.health_product_add, name='health_product_add'),

    url(r'^ajax/insurer-(?P<insurer_id>\d+)/health-product-(?P<product_id>\d+)/mark-complete/$',
        views.health_product_mark_complete, name='health_product_mark_complete'),
    url(r'^ajax/insurer-(?P<insurer_id>\d+)/health-product-(?P<product_id>\d+)/mark-integrated/$',
        views.health_product_mark_integrated, name='health_product_mark_integrated'),

    url(r'^insurer-(?P<insurer_id>\d+)/health-product-(?P<product_id>\d+)/manage-feature/$',
        views.health_product_feature_manage, name='health_product_feature_manage'),

    url(r'^insurer-(?P<insurer_id>\d+)/health-product-(?P<product_id>\d+)/details/$',
        views.health_product_details, name='health_product_details'),
    url(r'^ajax/insurer-(?P<insurer_id>\d+)/health-product-(?P<product_id>\d+)/details/$',
        views.ajax_details_health_product, name='ajax_details_health_product'),

    url(r'^insurer-(?P<insurer_id>\d+)/health-product-(?P<product_id>\d+)/download-to-excel/$',
        views.health_product_download_to_excel, name='health_product_download_to_excel'),

    url(r'^insurer-(?P<insurer_id>\d+)/health-product-(?P<product_id>\d+)/medical-score/$',
        views.health_product_medical_score, name='health_product_medical_score'),

    url(r'^term-admin/$', views.wiki_term_admin, name='wiki_term_admin'),
    url(r'^term-admin-list/$', views.wiki_term_admin_list, name='wiki_term_admin_list'),
    url(r'^score/$', views.health_product_test_score, name='health_product_test_score'),

    url(r'^insurer/dashboard/$', views.insurer_dashboard, name='insurer_dashboard'),
    url(r'^(?P<insurer_slug>[-\w]+)/(?P<product_slug>[-\w]+)/features/$',
        views.health_product_insurer_score, name='health_product_insurer_score'),

    fhurl(r'^health-product-(?P<product_id>\d+)/cover_premium_table_form/(?P<slab_id>[\d-]+)/$',
          forms.CoverPremiumTableForm, decorator=profile_type_only('WIKI_DATA_ENTRY'),
          name='ajax_cover_premium_table_form', template="wiki/snippets/cover_premium_table_form.html", json=True),
    fhurl(r'^health-product-(?P<product_id>\d+)/cover_premium_table_form/(?P<slab_id>[\d-]+)/(?P<cpt_id>[\d-]+)/$',
          forms.CoverPremiumTableForm, decorator=profile_type_only('WIKI_DATA_ENTRY'),
          name='ajax_cover_premium_table_edit_form', template="wiki/snippets/cover_premium_table_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/family_cover_premium_table_form/(?P<slab_id>[\d-]+)/$',
          forms.FamilyCoverPremiumTableForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), json=True,
          name='ajax_family_cover_premium_table_form', template="wiki/snippets/family_cover_premium_table_form.html"),
    fhurl(r'^health-product-(?P<product_id>\d+)/family_cover_premium_table_form/(?P<slab_id>[\d-]+)/(?P<cpt_id>[\d-]+)/$',
          forms.FamilyCoverPremiumTableForm, decorator=profile_type_only('WIKI_DATA_ENTRY'),
          name='ajax_family_cover_premium_table_edit_form', json=True,
          template="wiki/snippets/family_cover_premium_table_form.html"),

    fhurl(r'^health-product-(?P<product_id>\d+)/life_long_renewability_form/(?P<slab_id>[\d-]+)/$',
          forms.LifeLongRenewabilityForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), json=True,
          name='ajax_life_long_renewability_form', template="wiki/snippets/life_long_renewability_form.html"),

    url(r'^ajax/insurer-(?P<insurer_id>\d+)/health-product-(?P<product_id>\d+)/mark-complete/$',
        views.health_product_mark_complete, name='health_product_mark_complete'),
    url(r'^ajax/insurer-(?P<insurer_id>\d+)/health-product-(?P<product_id>\d+)/mark-integrated/$',
        views.health_product_mark_integrated, name='health_product_mark_integrated'),
    fhurl(r'^health-product-(?P<product_id>\d+)/ambulance_charges_form/(?P<slab_id>[\d-]+)/$',
          forms.AmbulanceChargesForm, decorator=profile_type_only('WIKI_DATA_ENTRY'),
          name='ajax_ambulance_charges_form', template="wiki/snippets/ambulance_charges_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/alternative_practices_coverage_form/(?P<slab_id>[\d-]+)/$',
          forms.AlternativePracticesCoverageForm, decorator=profile_type_only('WIKI_DATA_ENTRY'),
          name='ajax_alternative_practices_coverage_form',
          template="wiki/snippets/alternative_practices_coverage_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/age_limit_form/(?P<slab_id>[\d-]+)/$',
          forms.AgeLimitForm, decorator=profile_type_only('WIKI_DATA_ENTRY'),
          name='ajax_age_limit_form', template="wiki/snippets/age_limit_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/special_feature_form/(?P<slab_id>[\d-]+)/$',
          forms.SpecialFeatureForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_special_feature_form',
          template="wiki/snippets/special_feature_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/online_availability_form/(?P<slab_id>[\d-]+)/$',
          forms.OnlineAvailabilityForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), json=True,
          name='ajax_online_availability_form', template="wiki/snippets/online_availability_form.html"),

    fhurl(r'^health-product-(?P<product_id>\d+)/medical_required_form/(?P<slab_id>[\d-]+)/$',
          forms.MedicalRequiredForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_medical_required_form',
          template="wiki/snippets/medical_required_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/family_coverage_form/(?P<slab_id>[\d-]+)/$',
          forms.FamilyCoverageForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_family_coverage_form',
          template="wiki/snippets/family_coverage_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/general_form/(?P<slab_id>[\d-]+)/$', forms.GeneralForm,
          decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_general_form',
          template="wiki/snippets/general_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/pre_existing_form/(?P<slab_id>[\d-]+)/$',
          forms.PreExistingForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_pre_existing_form',
          template="wiki/snippets/pre_existing_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/maternity_cover_form/(?P<slab_id>[\d-]+)/$',
          forms.MaternityCoverForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_maternity_cover_form',
          template="wiki/snippets/maternity_cover_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/domiciliary_hospitalization_form/(?P<slab_id>[\d-]+)/$',
          forms.DomiciliaryHospitalizationForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), json=True,
          name='ajax_domiciliary_hospitalization_form', template="wiki/snippets/domiciliary_hospitalization_form.html"),

    fhurl(r'^health-product-(?P<product_id>\d+)/outpatient_benefits_form/(?P<slab_id>[\d-]+)/$',
          forms.OutpatientBenefitsForm, decorator=profile_type_only('WIKI_DATA_ENTRY'),
          name='ajax_outpatient_benefits_form', template="wiki/snippets/outpatient_benefits_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/organ_donor_form/(?P<slab_id>[\d-]+)/$',
          forms.OrganDonorForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_organ_donor_form',
          template="wiki/snippets/organ_donor_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/bonus_form/(?P<slab_id>[\d-]+)/$', forms.BonusForm,
          decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_bonus_form',
          template="wiki/snippets/bonus_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/premium_discount_form/(?P<slab_id>[\d-]+)/$',
          forms.PremiumDiscountForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_premium_discount_form',
          template="wiki/snippets/premium_discount_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/convalescence_benefit_form/(?P<slab_id>[\d-]+)/$',
          forms.ConvalescenceBenefitForm, decorator=profile_type_only('WIKI_DATA_ENTRY'),
          name='ajax_convalescence_benefit_form', template="wiki/snippets/convalescence_benefit_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/pre_post_hospitalization_form/(?P<slab_id>[\d-]+)/$',
          forms.PrePostHospitalizationForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), json=True,
          name='ajax_pre_post_hospitalization_form', template="wiki/snippets/pre_post_hospitalization_form.html"),

    fhurl(r'^health-product-(?P<product_id>\d+)/copay_form/(?P<slab_id>[\d-]+)/$', forms.CopayForm,
          decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_copay_form',
          template="wiki/snippets/copay_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/room_type_copay_form/(?P<slab_id>[\d-]+)/$',
          forms.RoomTypeCopayForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_room_type_copay_form',
          template="wiki/snippets/room_type_copay_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/zonal_copay_form/(?P<slab_id>[\d-]+)/$',
          forms.ZonalCopayForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_zonal_copay_form',
          template="wiki/snippets/zonal_copay_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/zonal_room_rent_form/(?P<slab_id>[\d-]+)/$',
          forms.ZonalRoomRentForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_zonal_room_rent_form',
          template="wiki/snippets/zonal_room_rent_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/health_checkup_form/(?P<slab_id>[\d-]+)/$',
          forms.HealthCheckupForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_health_checkup_form',
          template="wiki/snippets/health_checkup_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/standard_exclusion_form/(?P<slab_id>[\d-]+)/$',
          forms.StandardExclusionForm, decorator=profile_type_only('WIKI_DATA_ENTRY'),
          name='ajax_standard_exclusion_form', template="wiki/snippets/standard_exclusion_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/claims_form/(?P<slab_id>[\d-]+)/$',
          forms.ClaimsForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_claims_form',
          template="wiki/snippets/claims_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/day_care_form/(?P<slab_id>[\d-]+)/$',
          forms.DayCareForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_day_care_form',
          template="wiki/snippets/day_care_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/dental_coverage_form/(?P<slab_id>[\d-]+)/$',
          forms.DentalCoverageForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_dental_coverage_form',
          template="wiki/snippets/dental_coverage_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/eye_coverage_form/(?P<slab_id>[\d-]+)/$',
          forms.EyeCoverageForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_eye_coverage_form',
          template="wiki/snippets/eye_coverage_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/network_hospital_daily_cash_form/(?P<slab_id>[\d-]+)/$',
          forms.NetworkHospitalDailyCashForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), json=True,
          name='ajax_network_hospital_daily_cash_form', template="wiki/snippets/network_hospital_daily_cash_form.html"),

    fhurl(r'^health-product-(?P<product_id>\d+)/non_network_hospital_daily_cash_form/(?P<slab_id>[\d-]+)/$',
          forms.NonNetworkHospitalDailyCashForm, decorator=profile_type_only('WIKI_DATA_ENTRY'),
          name='ajax_non_network_hospital_daily_cash_form', json=True,
          template="wiki/snippets/non_network_hospital_daily_cash_form.html"),

    fhurl(r'^health-product-(?P<product_id>\d+)/cancellation_policy_form/(?P<slab_id>[\d-]+)/$',
          forms.CancellationPolicyForm, decorator=profile_type_only('WIKI_DATA_ENTRY'),
          name='ajax_cancellation_policy_form', template="wiki/snippets/cancellation_policy_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/critical_illness_coverage_form/(?P<slab_id>[\d-]+)/$',
          forms.CriticalIllnessCoverageForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), json=True,
          name='ajax_critical_illness_coverage_form', template="wiki/snippets/critical_illness_coverage_form.html"),

    fhurl(r'^health-product-(?P<product_id>\d+)/restore_benefits_form/(?P<slab_id>[\d-]+)/$',
          forms.RestoreBenefitsForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_restore_benefits_form',
          template="wiki/snippets/restore_benefits_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/medical_score_form/$', forms.MedicalScoreForm,
          decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_save_score',
          template="wiki/snippets/medical_score_form.html", json=True),

    fhurl(r'^health-product-(?P<product_id>\d+)/(?P<obj_id>\d+)/medical_score_form/$',
          forms.MedicalScoreForm, decorator=profile_type_only('WIKI_DATA_ENTRY'), name='ajax_save_score',
          template="wiki/snippets/medical_score_form.html", json=True),

    fhurl(r'^ajax/save-product-meta/(?P<product_id>\d+)/(?P<model>[\w-]+)/$', forms.ProductMetaDataForm,
          decorator=profile_type_only('WIKI_DATA_ENTRY'), name='wiki_save_product_meta', json=True),

    url(r'^insurance-company/$', profile_type_only('WIKI_DATA_ENTRY')(views.InsurerListView.as_view()),
        name='insurer_index'),

    url(r'^ajax/remove/medical-score/(?P<product_id>\d+)/(?P<pid>\d+)/$',
        views.wiki_ajax_medical_score_remove, name='wiki_ajax_medical_score_remove'),

    url(r'^ajax/remove/(?P<product_id>\d+)/(?P<model>[\w-]+)/(?P<pid>\d+)/$',
        views.wiki_ajax_remove, name='wiki_ajax_remove'),
    url(r'^ajax/health-product-(?P<product_id>\d+)/save-notes/$',
        views.wiki_ajax_save_notes, name='wiki_ajax_save_notes'),

    url(r'^ajax/detach/(?P<product_id>[\d-]+)/(?P<medical_condition_id>\d+)/$',
        views.wiki_ajax_detach_medical_condition, name='wiki_ajax_detach_medical_condition'),
    url(r'^ajax/detach/(?P<product_id>\d+)/(?P<model>[\w-]+)/(?P<slab_id>[\d-]+)/$',
        views.wiki_ajax_detach, name='wiki_ajax_detach'),
    url(r'^ajax/detach/copy/(?P<product_id>\d+)/(?P<model>[\w-]+)/(?P<slab_id>[\d-]+)/$',
        views.wiki_ajax_detach, {'copy': True}, name='wiki_ajax_detach_copy'),

    url(r'^ajax/save-meta/(?P<product_id>\d+)/(?P<model>[\w-]+)/(?P<slab_id>[\d-]+)/$',
        views.wiki_save_meta, name='wiki_save_meta'),

    url(r'^copy/insurer/(?P<insurer_id>[-\w]+)/(?P<product_id>[-\w]+)/$',
        views.health_product_copy, name='health_product_copy'),

    url(r'^$', views.wiki_index, name='wiki_index'),
    url(r'^add-medical-condition/$', views.medical_condition_add, name='medical_condition_add'),
    url(r'^(?P<article_slug>[\w-]+)/$', views.admin_article_wiki_details, name='admin_article_wiki_details'),

    url(r'^insurance-company/(?P<slug>[-\w]+)/$',
        profile_type_only('WIKI_DATA_ENTRY')(views.InsurerDetailView.as_view()), name='insurer_detail'),
    url(r'^slug-check/(?P<app>[\w]+)/(?P<model>[\w]+)/$', views.slug_check, name='slug_check'),

    fhurl(r'^ajax/add-medical-condition/$', profile_type_only('WIKI_DATA_ENTRY')(forms.MedicalConditionAddForm),
          name='ajax_medical_condition_add', template="wiki/medical_condition_add.html", json=True),
    url(r'^edit/medical-condition-(?P<medical_condition_id>\d+)/$', views.medical_condition_add,
        name='medical_condition_edit'),
    fhurl(r'^ajax/edit/medical-condition-(?P<medical_condition_id>\d+)/$',
          profile_type_only('WIKI_DATA_ENTRY')(forms.MedicalConditionAddForm), name='ajax_medical_condition_edit',
          template="wiki/medical_condition_add.html", json=True),
    url(
        r'^insurer/(?P<insurer_id>[-\w]+)/manage-medical-conditions-form/$',
        views.manage_medical_conditions, name="manage_medical_conditions"
    ),
    url(
        r'^insurer/(?P<insurer_id>[-\w]+)/manage-medical-conditions-form/edit-medical-condition/$',
        views.edit_medical_condition, name='edit_medical_condition'
    )
]
