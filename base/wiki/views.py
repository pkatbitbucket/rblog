import csv
import json
from collections import defaultdict

from django.conf import settings
from django.shortcuts import render, render_to_response, get_object_or_404
from django.template import RequestContext
from django.http.response import Http404, HttpResponse, HttpResponseRedirect
from django.core.exceptions import PermissionDenied
from django.views.generic import ListView, DetailView
from django.views.decorators.cache import never_cache
from django.apps.registry import apps
from django.core.urlresolvers import reverse
from django.contrib.contenttypes.models import ContentType
from django.http import JsonResponse
from braces import views as braces_views

from health_product.services.match_plan import MatchPlan
from health_product.services.validate_plan import ValidatePlan
import utils
from flatpages.views import index as flatpage_view
from brands.models import BrandPage
from seo.forms import ObjectSEOForm
from seo.models import PhraseList, Taxonomy
import putils
from .models import (
    TermParent, Term, City, MedicalScore, HealthProduct, AttributeDict, Slab, Insurer, WikiArticle, ProductSection,
    MedicalCondition, MetaData, AttributeList, ManyAttributeDict, Hospital, State, MedicalTransactionPermission
)
from .forms import (
    WikiArticleAddForm, InsurerTestInput, TestInput, InsurerAddForm, ClaimsDataForm, HealthProductAddForm,
    MedicalConditionAddForm, SectionAddForm, MedicalPermissionAddForm
)


@utils.profile_type_only('AUTHOR')
@never_cache
def article_add(request, article_slug=None):
    phrase_list = [[p.id, p.keyword.lower(), p.frequency]
                   for p in PhraseList.objects.all().order_by('-frequency')]
    word_list = [[t.id, t.relation_type, [tw.keyword.lower() for tw in t.words.all()]]
                 for t in Taxonomy.objects.all()]
    if article_slug:
        article = get_object_or_404(WikiArticle, slug=article_slug)
        article_add_form = WikiArticleAddForm(request, instance=article)
        seo_form = ObjectSEOForm(
            request, prefix="seo", instance=putils.get_seo(article))
    else:
        article_add_form = WikiArticleAddForm(request)
        seo_form = None

    if article_slug:
        attribute_list = ProductSection.objects.filter(product_page=article)
    else:
        attribute_list = []
    return render_to_response("blog/wiki_article_add.html", {
        'form': article_add_form,
        'seo_form': seo_form,
        'phrase_list': json.dumps(phrase_list),
        'word_list': json.dumps(word_list),
        'attribute_list': attribute_list
    }, context_instance=RequestContext(request))


@utils.profile_type_only('AUTHOR')
@never_cache
def product_section_add(request, section_id=None):
    if section_id:
        section = get_object_or_404(ProductSection, id=section_id)
        section_add_form = SectionAddForm(request, instance=section)
    else:
        section_add_form = SectionAddForm(request)
    return render_to_response("blog/wiki_section_add.html", {
        'form': section_add_form,
    }, context_instance=RequestContext(request))


@utils.profile_type_only('ADMIN', 'AUTHOR')
@never_cache
def admin_article_wiki_details(request, article_slug):
    article = get_object_or_404(WikiArticle, slug=article_slug)
    if article.category == 'PRODUCT_REVIEW':
        important_features = [
            'age_limit',
            'zonal_room_rent',
            'pre_existing',
            'general',
            'copay',
            'family_coverage',
            'bonus',
            'maternity_cover'
        ]
        good_to_have = [
            'critical_illness_coverage',
            'restore_benefits',
            'health_checkup',
            'network_hospital_daily_cash',
            'dental_coverage',
            'convalescence_benefit'
        ]
        imp_list = [p for p in ProductSection.objects.filter(
            product_page=article) if p.code in important_features and p.body]
        good_list = [p for p in ProductSection.objects.filter(
            product_page=article) if p.code in good_to_have and p.body]
        for p in good_list:
            print p.code, p.body

        return render_to_response("blog/wiki_article_details.html", {
            'object': article,
            'imp_list': imp_list,
            'good_list': good_list
        }, context_instance=RequestContext(request))
    else:
        return render_to_response("blog/wiki_company_page.html", {
            'object': article,
        }, context_instance=RequestContext(request))


@never_cache
def article_wiki_details(request, article_slug):
    article = get_object_or_404(WikiArticle, slug=article_slug, status=2)
    meta = article.get_metacontext()

    try:
        product = article.health_product.all()[0]
    except IndexError:
        product = None

    if article.category == 'PRODUCT_REVIEW':
        if product:
            if not meta['title']:
                meta['title'] = "%s %s Policy - Coverfox.com" % (
                    product.insurer.seo_title, product.title)
            if not meta['description']:
                meta['description'] = "Planning to buy health insurance plans online? Get detailed review & affordable premium rates for %s policy only on Coverfox.com" % (
                    product.insurer.seo_title + " " + product.title, )

        important_features = [
            'age_limit',
            'zonal_room_rent',
            'pre_existing',
            'general',
            'copay',
            'family_coverage',
            'bonus',
            'maternity_cover'
        ]
        good_to_have = [
            'critical_illness_coverage',
            'restore_benefits',
            'health_checkup',
            'network_hospital_daily_cash',
            'dental_coverage',
            'convalescence_benefit'
        ]
        imp_list = [p for p in ProductSection.objects.filter(
            product_page=article) if p.code in important_features]
        good_list = [p for p in ProductSection.objects.filter(
            product_page=article) if p.code in good_to_have]

        return render_to_response("blog/wiki_article_details.html", {
            'object': article,
            'imp_list': imp_list,
            'good_list': good_list,
            'meta': meta,
        }, context_instance=RequestContext(request))
    else:
        return render_to_response("blog/wiki_company_page.html", {
            'object': article,
            'meta': meta,
        }, context_instance=RequestContext(request))


@utils.profile_type_only('WIKI_DATA_ENTRY')
def wiki_ajax_save_notes(request, product_id):
    product = get_object_or_404(HealthProduct, pk=product_id)
    notes = request.POST['notes']
    product.notes = notes
    product.save()
    return HttpResponse(json.dumps({'success': True}))


@utils.profile_type_only('WIKI_DATA_ENTRY')
def health_product_mark_complete(request, insurer_id, product_id):
    insurer = get_object_or_404(Insurer, pk=insurer_id)
    product = get_object_or_404(HealthProduct, pk=product_id)
    if request.POST['status'] == 'COMPLETED':
        product.is_completed = True
        product.save()
    else:
        product.is_completed = False
        product.save()
    return HttpResponse(json.dumps({'status': product.is_completed, 'insurer_id': insurer.id, 'product_id': product.id}))


@utils.profile_type_only('WIKI_DATA_ENTRY')
def health_product_mark_integrated(request, insurer_id, product_id):
    insurer = get_object_or_404(Insurer, pk=insurer_id)
    product = get_object_or_404(HealthProduct, pk=product_id)
    if request.POST['status'] == 'INTEGRATED':
        product.is_backend_integrated = True
        product.save()
    else:
        product.is_backend_integrated = False
        product.save()
    return HttpResponse(json.dumps({'status': product.is_backend_integrated, 'insurer_id': insurer.id, 'product_id': product.id}))


@utils.profile_type_only('WIKI_DATA_ENTRY')
def health_product_copy(request, insurer_id, product_id):
    insurer = get_object_or_404(Insurer, pk=insurer_id)
    product = get_object_or_404(HealthProduct, pk=product_id)
    product.clone()
    return HttpResponseRedirect(reverse('wiki_dashboard', args=[]))


@utils.profile_type_only('WIKI_DATA_ENTRY')
def wiki_ajax_remove(request, product_id, model, pid):
    product = get_object_or_404(HealthProduct, pk=product_id)
    M = apps.get_model('wiki', AttributeDict[model])
    obj = M.objects.get(id=pid)
    obj.delete()
    return HttpResponse(json.dumps({
        'success': True,
        'deleted': True,
        'corr_list': putils.get_correlated_slabs(product, AttributeDict[model], model)
    }))


@utils.profile_type_only('WIKI_DATA_ENTRY')
def wiki_ajax_medical_score_remove(request, product_id, pid):
    product = get_object_or_404(HealthProduct, pk=product_id)
    obj = MedicalScore.objects.get(id=pid)
    obj.delete()
    return HttpResponse(json.dumps({
        'success': True,
        'deleted': True,
    }))


@utils.profile_type_only('WIKI_DATA_ENTRY')
def wiki_save_meta(request, product_id, model, slab_id):
    product = get_object_or_404(HealthProduct, pk=product_id)
    slabs = [Slab.objects.get(id=sl) for sl in slab_id.split("-")]
    M = apps.get_model('wiki', AttributeDict[model])
    putils.save_meta_form(M, slabs, request.POST)
    return HttpResponse(json.dumps({
        'success': True,
        'response': putils.create_standard_response(product, AttributeDict[model], slabs)
    }))


@utils.profile_type_only('WIKI_DATA_ENTRY')
def wiki_ajax_detach(request, product_id, model, slab_id, copy=False):
    M = apps.get_model('wiki', AttributeDict[model])
    slabs = []
    for sid in slab_id.split("-"):
        slabs.append(get_object_or_404(Slab, pk=sid))
    product = get_object_or_404(HealthProduct, pk=product_id)

    # Select the first slab and attach all others to his clone
    obj = M.objects.filter(slabs=slabs[0])

    for o in obj:
        for slab in slabs:
            o.slabs.remove(slab)
        if copy:
            newo = o.clone()
            # if model is manytomany Vs foreign_key
            for slab in slabs:
                if getattr(getattr(slab, model), 'all', None):
                    getattr(slab, model).add(newo)
                else:
                    setattr(slab, model, newo)
                    slab.save()
    # Remove zombie conditions
    M.objects.filter(slabs__isnull=True).delete()

    # Also clone associated MetaData
    mdata = MetaData.objects.filter(
        slabs=slabs[0], content_type=ContentType.objects.get_for_model(M))
    if mdata:
        md = mdata[0]
        for slab in slabs:
            md.slabs.remove(slab)
            newmd = md.clone()
            newmd.slabs.add(slab)

    return HttpResponse(json.dumps({
        'success': True,
        'detached': True,
        'corr_list': putils.get_correlated_slabs(product, AttributeDict[model], model)
    }))


@utils.profile_type_only('WIKI_DATA_ENTRY')
def wiki_term_admin_list(request):
    parent_terms = TermParent.objects.all()
    response = render_to_response("wiki/wiki_term_admin_list.html", {
        'terms': Term.objects.order_by('-id'),
        'parent_terms': parent_terms
    },
        context_instance=RequestContext(request)
    )
    response['Cache-Control'] = 'no-cache, no-store'
    return response


@utils.profile_type_only('WIKI_DATA_ENTRY')
def wiki_term_admin(request):
    if request.POST:
        if request.POST.get('action', '').lower() == 'delete':
            t = Term.objects.get(id=request.POST['id'])
            t.criticalillnesscoverage_set.clear()
            t.dentalcoverage_set.clear()
            t.eyecoverage_set.clear()
            t.domiciliaryhospitalization_set.clear()
            t.standardexclusion_set.clear()
            t.waitingperiodgeneral_set.all().delete()
            t.waitingperiodpreexisting_set.all().delete()
            t.delete()
            return HttpResponse(json.dumps({'success': True, 'id': request.POST['id']}))

        data = {}
        for k in request.POST.keys():
            if request.POST[k] == 'false':
                data[k] = False
                continue
            if request.POST[k] == 'true':
                data[k] = True
                continue
            data[k] = request.POST[k]

        data.pop('csrfmiddlewaretoken')
        try:
            if not data.get('id'):
                t = Term(name=data['name'].strip().replace("\n", ""))
                t.save()
                data['id'] = t.id
            data['name'] = data['name'].strip().replace("\n", "")
            Term.objects.filter(id=data['id']).update(**data)
        except Exception, e:
            return HttpResponse(json.dumps({'success': False, 'error': "Error %s" % e}))

        return HttpResponse(json.dumps({'success': True, 'id': data['id']}))
    else:
        response = render_to_response("wiki/wiki_term_admin.html", {
            'terms': Term.objects.order_by('-id')
        },
            context_instance=RequestContext(request)
        )
        return response


@utils.profile_type_only('WIKI_DATA_ENTRY')
def wiki_index(request):
    return render_to_response("wiki/wiki_index.html", {
    },
        context_instance=RequestContext(request)
    )


class InsurerListView(ListView):
    paginate_by = getattr(settings, 'WIKI_PAGESIZE', 10)
    template_name = "wiki/insurer_list.html"

    def get_context_data(self, **kwargs):
        context = super(InsurerListView, self).get_context_data(**kwargs)
        context.setdefault('meta', {})
        context['meta']['title'] = 'Coverfox | Wiki'
        return context

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Insurer.objects.all()
        else:
            return Insurer.objects.published()


class InsurerDetailView(DetailView):
    model = Insurer
    slug = 'slug'
    template = "wiki/insurer_detail.html"

    def get_context_data(self, **kwargs):
        context = super(InsurerDetailView, self).get_context_data(**kwargs)
        context.setdefault('meta', {})
        context['meta'].update(context['object'].get_metacontext())
        return context


@utils.profile_type_only('WIKI_DATA_ENTRY')
@never_cache
def health_product_medical_score(request, insurer_id, product_id):
    insurer = get_object_or_404(Insurer, pk=insurer_id)
    product = get_object_or_404(HealthProduct, pk=product_id)

    term_related_models = ['CriticalIllnessCoverage',
                           'StandardExclusion', 'DayCare', 'General', 'PreExisting']
    attribute_list = [att for att in AttributeList if att[
        0] in term_related_models]

    slab_id_corr_dict = {}
    for wiki_model_string, verbose, attr in attribute_list:
        slab_id_corr_dict[attr] = putils.get_correlated_slabs(
            product, wiki_model_string, attr)

    slab_corr_dict = defaultdict(list)
    for attr, slab_grps in slab_id_corr_dict.items():
        print attr, slab_grps
        for slab_ids in slab_grps:
            slab_corr_dict[attr].append(
                [Slab.objects.get(id=sid) for sid in slab_ids])

    response = render_to_response("wiki/health_plan_medical_score.html", {
        'insurer': insurer,
        'product': product,
        'attribute_list': attribute_list,
        'slab_corr_dict': slab_corr_dict,
        'slab_id_corr_dict': json.dumps(slab_id_corr_dict),
        'terms_list': json.dumps([d.name.lower() for d in Term.objects.all().order_by('name')]),
        'standard_exclusion_list': json.dumps([d.name.lower() for d in Term.objects.filter(ib_standard_exclusions=True).order_by('name')]),
        'critical_illness_coverage_list': json.dumps([d.name.lower() for d in Term.objects.filter(ib_critical_illness=True).order_by('name')]),
        'general_list': json.dumps([d.name.lower() for d in Term.objects.filter(ib_general=True).order_by('name')]),
        'pre_existing_list': json.dumps([d.name.lower() for d in Term.objects.filter(ib_pre_existing=True).order_by('name')]),
        'day_care_list': json.dumps([d.name.lower() for d in Term.objects.filter(ib_day_care=True).order_by('name')]),
    }, context_instance=RequestContext(request))
    return response


@utils.profile_type_only('WIKI_DATA_ENTRY')
@never_cache
def ajax_details_health_product(request, insurer_id, product_id):
    insurer = get_object_or_404(Insurer, pk=insurer_id)
    product = get_object_or_404(HealthProduct, pk=product_id)
    tp = ValidatePlan(product)
    status = tp.test()
    return HttpResponse(json.dumps(status))


@utils.profile_type_only('INSURER')
@never_cache
def insurer_dashboard(request):
    insurer_list = request.user.insurer_manager.all()
    return render_to_response("wiki/insurer_dashboard.html", {
        'insurer_list': insurer_list,
    }, context_instance=RequestContext(request))


@utils.profile_type_only('INSURER')
@never_cache
def health_product_insurer_score(request, insurer_slug, product_slug):
    insurer = get_object_or_404(Insurer, slug=insurer_slug)
    product = get_object_or_404(HealthProduct, slug=product_slug)
    if product.insurer.insurer_manager != request.user:
        raise PermissionDenied
    if request.POST:
        tf = InsurerTestInput(product, request.POST)
    else:
        tf = InsurerTestInput(product)
    score_data = None
    if tf.is_valid():
        print tf.cleaned_data
        tf.cleaned_data['service_type'] = 'SHARED'
        if product.policy_type == 'FAMILY':
            tf.cleaned_data['family'] = True
        else:
            tf.cleaned_data['family'] = False
        if not tf.cleaned_data.get('kids'):
            tf.cleaned_data['kids'] = 0
        if not tf.cleaned_data.get('adults'):
            tf.cleaned_data['adults'] = 0

        tf.cleaned_data['sum_assured'] = int(tf.cleaned_data['sum_assured'])
        mp = MatchPlan(tf.cleaned_data, product)
        score_data = mp.calculate_score()
    else:
        print "FORM ERROR", tf.errors

    return render_to_response("wiki/health_plan_insurer_score.html", {
        'form': tf,
        'score_data': score_data
    }, context_instance=RequestContext(request))


@utils.profile_type_only('WIKI_DATA_ENTRY')
@never_cache
def health_product_test_score(request):
    data_label = []
    data_attr = []
    prop_attr = []
    score_data = None

    print request.POST
    if request.POST:
        tf = TestInput(request.POST)
        if tf.is_valid():
            kid_ages = []
            if tf.cleaned_data.get('kid_1_age'):
                kid_ages.append(tf.cleaned_data['kid_1_age'])
            if tf.cleaned_data.get('kid_2_age'):
                kid_ages.append(tf.cleaned_data['kid_2_age'])
            tf.cleaned_data['kid_ages'] = kid_ages
            print "------>", tf.cleaned_data
            tf.cleaned_data['hospitals'] = []
            mp = MatchPlan(tf.cleaned_data, None, True)
            score_data = mp.calculate_score()
            mlabel = []
            mlabel.append(['City', tf.cleaned_data.get('city')])
            mlabel.append(
                ['Healthcare Choice', tf.cleaned_data.get('service_type')])
            mlabel.append(['Age', tf.cleaned_data.get('age')])
            mlabel.append(['Adults', tf.cleaned_data.get('adults')])
            mlabel.append(['Kids', tf.cleaned_data.get('kids')])
            for sd in score_data:
                sd['dscore'] = dict(sd['score'])
            data_label.extend(['Total', 'Insurer', 'Name',
                               'Type', 'SA', 'Medical Score', 'Premium'])
            #data_label.extend(['Eye', 'Outpatient Benefits', 'Daily Cash', 'Claims', 'Health Checkup', 'Maternity Cover', 'Dental', 'Ambulance Charges', 'Bonus', 'Organ Donor', 'Domiciliary Hospitalization', 'Pre Post Hospitalization', 'Life Long Renewability', 'Alternative Practices Coverage', 'Copay', 'Convalescence Benefit', 'Premium Discount', 'Critical Illness Coverage', 'Online Availability', 'Room Rent', 'Restore Benefits'])
            data_label.extend(['Premium Score', 'Maternity cover', 'Copay', 'Room Rent', 'Claims', 'Bonus', 'Premium discount',
                               'Pre-existing', 'Daily Cash', 'Health Checkup', 'Pre Post Hospitalization', 'Critical Illness Coverage', 'Restore Benefits'])

            data_attr = ['total_score', 'insurer_title', 'product_title',
                         'policy_type', 'sum_assured', 'medical_score', 'premium']
            #prop_attr = ['eye', 'outpatient_benefits', 'daily_cash', 'claims', 'health_checkup', 'maternity_cover', 'dental', 'ambulance_charges', 'bonus', 'organ_donor', 'domiciliary_hospitalization', 'pre_post_hospitalization', 'life_long_renewability', 'alternative_practices_coverage', 'copay', 'convalescence_benefit', 'premium_discount', 'critical_illness_coverage', 'online_availability', 'room_rent', 'restore_benefits']
            prop_attr = ['premium', 'maternity_cover', 'copay', 'room_rent', 'claims', 'bonus', 'premium_discount', 'pre_existing',
                         'daily_cash', 'health_checkup', 'pre_post_hospitalization', 'critical_illness_coverage', 'restore_benefits']

    else:
        tf = TestInput()

    if request.GET.get('csv'):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="output.csv"'
        writer = csv.writer(response)
        for m in mlabel:
            writer.writerow(m)

        writer.writerow(data_label)
        for sd in score_data:
            row = []
            for da in data_attr:
                row.append(sd[da])
            for pa in prop_attr:
                row.append(sd['dscore'][pa]['score'])
            writer.writerow(row)

        return response

    return render_to_response("wiki/health_plan_score.html", {
        'form': tf,
        'score_data': score_data,
        'data_label': data_label,
        'data_attr': data_attr,
        'prop_attr': prop_attr,
    }, context_instance=RequestContext(request))


@utils.profile_type_only('WIKI_DATA_ENTRY')
@never_cache
def health_product_details(request, insurer_id, product_id):
    insurer = get_object_or_404(Insurer, pk=insurer_id)
    product = get_object_or_404(HealthProduct, pk=product_id)

    if product.policy_type == "INDIVIDUAL":
        attribute_list = [att for att in AttributeList if att[
            0] not in ['FamilyCoverage', 'FamilyCoverPremiumTable']]
    else:
        attribute_list = [att for att in AttributeList if att[
            0] not in ['CoverPremiumTable']]

    return render_to_response("wiki/health_plan_details.html", {
        'insurer': insurer,
        'product': product,
        'attribute_list': attribute_list,
    }, context_instance=RequestContext(request))


@utils.profile_type_only('WIKI_DATA_ENTRY')
@never_cache
def health_product_download_to_excel(request, insurer_id, product_id):
    insurer = get_object_or_404(Insurer, pk=insurer_id)
    product = get_object_or_404(HealthProduct, pk=product_id)

    if product.policy_type == "INDIVIDUAL":
        attribute_list = [att for att in AttributeList if att[
            0] not in ['FamilyCoverage', 'FamilyCoverPremiumTable']]
    else:
        attribute_list = [att for att in AttributeList if att[
            0] not in ['CoverPremiumTable']]

    #a = Ind.objects.all()[0]
    #order = [f.name for f in a._meta.fields]
    order = ['']
    for slab in product.slabs.all():
        if slab.sum_assured:
            if slab.grade:
                order.append('%s- %s/-' %
                             (slab.grade.title(), slab.sum_assured))
            else:
                order.append('%s/-' % slab.sum_assured)

    data_row_list = []
    tp = ValidatePlan(product)
    status = tp.test()
    for att in attribute_list:
        temp_tuple = []
        temp_tuple.append(att[1])
        for slab in product.slabs.all():
            if slab.sum_assured:
                tup_text = []
                for k, v in status[slab.id].items():
                    if k == 'verbose':
                        for i in v.get(att[2], []):
                            tup_text.append("- %s" % i)
                temp_tuple.append("\n".join(tup_text))
        data_row_list.append(temp_tuple)

    filename = "Insurer_%s_product_%s_data.xls" % (insurer_id, product_id)
    if data_row_list:
        return putils.render_excel(filename, order, data_row_list)
    else:
        return HttpResponse("No records found")


@utils.profile_type_only('WIKI_DATA_ENTRY')
@never_cache
def health_product_feature_manage(request, insurer_id, product_id):
    insurer = get_object_or_404(Insurer, pk=insurer_id)
    product = get_object_or_404(HealthProduct, pk=product_id)

    slab_corr_dict = {}
    for attr, wiki_model_string in AttributeDict.items():
        slab_corr_dict[attr] = putils.get_correlated_slabs(
            product, wiki_model_string, attr)

    if product.policy_type == "INDIVIDUAL":
        attribute_list = [att for att in AttributeList if att[
            0] not in ['FamilyCoverage', 'FamilyCoverPremiumTable']]
    else:
        attribute_list = [att for att in AttributeList if att[
            0] not in ['CoverPremiumTable']]

    return render_to_response("wiki/health_plan_feature_manage.html", {
        'insurer': insurer,
        'product': product,
        'slabs' : product.slabs.values('id', 'sum_assured', 'grade', 'deductible'),
        'many_attribute_dict': ManyAttributeDict,
        'attribute_list': attribute_list,
        'slab_corr_dict': json.dumps(slab_corr_dict),
        'terms_list': json.dumps([d.name for d in Term.objects.none()]),
    }, context_instance=RequestContext(request))



@utils.profile_type_only('WIKI_DATA_ENTRY')
@never_cache
def insurer_add(request, insurer_id=None):
    phrase_list = [[p.id, p.keyword.lower(), p.frequency]
                   for p in PhraseList.objects.all().order_by('-frequency')]
    word_list = [[t.id, t.relation_type, [tw.keyword.lower() for tw in t.words.all()]]
                 for t in Taxonomy.objects.all()]
    if insurer_id:
        insurer = get_object_or_404(Insurer, pk=insurer_id)
        insurer_add_form = InsurerAddForm(request, instance=insurer)
        claims_instance = insurer.claims_data
        if claims_instance:
            claims_data_form = ClaimsDataForm(
                request, instance=claims_instance)
        else:
            claims_data_form = ClaimsDataForm(request)
        claims_data_form.init(insurer.id)
        seo_form = ObjectSEOForm(
            request, prefix="seo", instance=putils.get_seo(insurer))
    else:
        insurer_add_form = InsurerAddForm(request)
        seo_form = None
        claims_data_form = None
    return render_to_response("wiki/insurer_add.html", {
        'form': insurer_add_form,
        'seo_form': seo_form,
        'claims_data_form': claims_data_form,
        'phrase_list': json.dumps(phrase_list),
        'word_list': json.dumps(word_list),
    }, context_instance=RequestContext(request))


@utils.profile_type_only('WIKI_DATA_ENTRY')
@never_cache
def health_product_add(request, insurer_id, product_id=None):
    insurer = get_object_or_404(Insurer, pk=insurer_id)
    phrase_list = [[p.id, p.keyword.lower(), p.frequency]
                   for p in PhraseList.objects.all().order_by('-frequency')]
    word_list = [[t.id, t.relation_type, [tw.keyword.lower() for tw in t.words.all()]]
                 for t in Taxonomy.objects.all()]
    if product_id:
        product = get_object_or_404(HealthProduct, pk=product_id)
        product_add_form = HealthProductAddForm(request, instance=product)
        product_add_form.init(insurer_id, product_id)
        seo_form = ObjectSEOForm(
            request, prefix="seo", instance=putils.get_seo(product))
    else:
        product_add_form = HealthProductAddForm(request)
        product_add_form.init(insurer_id)
        seo_form = None
    return render_to_response("wiki/health_product_add.html", {
        'form': product_add_form,
        'seo_form': seo_form,
        'phrase_list': json.dumps(phrase_list),
        'word_list': json.dumps(word_list),
    }, context_instance=RequestContext(request))


def insurer_city_hospital_count(request):
    if request.method == 'POST' and request.is_ajax():
        data = request.POST

        insurer_id = data.get('insurer_id')
        if not insurer_id:
            return HttpResponse(json.dumps({'message': 'Invalid request'}),
                                status=400, content_type='application/json')
        else:
            insurer = get_object_or_404(Insurer, id=insurer_id)

        state = data.get('state')
        if not state:
            states = list(Hospital.objects.filter(insurers=insurer).values_list('state__name', flat=True).distinct())
            states.sort()
            return HttpResponse(json.dumps({'insurer_id': insurer.id, 'states': states}),
                                content_type='application/json')

        city = data.get('city')
        if not city:
            cities = list(Hospital.objects.filter(insurers=insurer,
                                                  state__name=state).values_list('city__name', flat=True).distinct())
            cities.sort()
            return HttpResponse(json.dumps({'insurer_id': insurer.id, 'state': state, 'cities': cities}),
                                content_type='application/json')

        _hospitals = Hospital.objects.filter(insurers=insurer,
                                             state__name=state,
                                             city__name=city)
        hospitals = [{'name': hospital.name,
                      'address': hospital.address,
                      'phone': hospital.phone1} for hospital in _hospitals]
        return HttpResponse(json.dumps({'insurer_id': insurer.id, 'state': state, 'city': city, 'hospitals': hospitals}),
                            content_type='application/json')

    return flatpage_view(request)


network_hospital_content = {
    'bajajallianz': "Here is an exhaustive list of Bajaj Allianz network hospitals that offer cashless service. "
                    "If you have health insurance from Bajaj Allianz, "
                    "this is probably one of the most important lists you can keep handy.",
    'iffcotokio': "IFFCO TOKIO network hospitals list that offer cashless services are enlisted here. "
                  "This extensive list will help a great deal in case of a medical emergency.",
    'hdfcergo': "Enlisted below are HDFC ERGO network hospitals list, "
                "where you can avail cashless benefits and services for everyone holding HDFC ERGO health policy.",
    'tataaig': "Tata AIG network hospitals list have been enlisted here for your reference. "
               "We would suggest you to keep this list handy for easy access during a medical emergency.",
    'apollomunich': "Cashless benefit can be availed at all the Apollo Munich network hospitals enlisted below. "
                    "If you have health insurance from Apollo Munich, "
                    "this is probably one of the most important lists you can keep handy.",
    'maxbupa': "If you have health insurance from Max Bupa, you might want to add this page in your favorites. "
               "The following is the list of all the Max Bupa network hospitals. "
               "This extensive list covers all network hospitals that offer cashless benefits, "
               "affiliated with Max Bupa.",
    'l-and-t': "L & T is a big player in the insurance industry and needless to say has a huge list of "
               "network hospitals that offer the cashless benefit. "
               "The following is the list of L & T Health Insurance network hospitals.",
    'new-india-assurance': "Now that you have health insurance from New India Assurance, "
                           "the next thing to do is zero down the network hospitals that will be convenient "
                           "for you during an emergency. "
                           "Below is the list of New India Assurance network hospitals for your reference.",
    'star-health-and-allied-insurance': "Handling medical emergencies becomes a little easier if you know which "
                                        "hospitals offer cashless hospitalization. "
                                        "The following is the list of Star Health network hospitals. "
                                        "Keep this list handy, just in case.",
    'cholamandalam-ms': "Along with getting cashless benefit in your health insurance, "
                        "it is equally important to be aware of what hospitals are included in the network. "
                        "The following is a list of Cholamandalam network hospitals for your reference.",
    'reliance': "After purchasing health insurance, "
                "the next thing to do is make yourself aware about the network hospitals that will be convenient "
                "for you during an emergency to avail the cashless benefit. "
                "Below is the list of Reliance network hospitals.",
    'religare': "Cashless hospitalization can be availed at all the Religare network hospitals, "
                "which are enlisted here. "
                "Everyone who have health insurance from Religare ought to keep this exhaustive list handy.",
    'oriental': "Enlisted below is a list of Oriental network hospitals, "
                "where you can avail cashless hospitalization for everyone who has an Oriental health insurance policy."
}


def insurer_hospitals(request, insurer_slug):
    slug = insurer_slug.lower()
    try:
        insurer = BrandPage.objects.get(slug=insurer_slug, _insurer__isnull=False, type='health')._insurer
    except BrandPage.DoesNotExist:
        insurer = get_object_or_404(Insurer, slug=slug)

    popular_cities = {
        'delhi': 'delhi',
        'bangalore': 'karnataka',
        'mumbai': 'maharashtra',
        'chennai': 'tamilnadu',
        'kolkata': 'west-bengal',
        'hyderabad': 'andhra-pradesh',
        'ernakulam': 'kerala',
        'pune': 'maharashtra'
    }
    return render(request,
                  'wiki/network_hospitals.html',
                  {
                      'insurer': insurer,
                      'insurer_slug': insurer_slug,
                      'content': network_hospital_content.get(insurer_slug, '') or '',
                      'popular_cities': popular_cities,
                      'breadcrumb': [
                          {'url': '/', 'title': 'Home'},
                          {'url': '/health-insurance/', 'title': 'Health Insurance'},
                          {'url': u'{}{}{}'.format('/health-insurance/', insurer_slug, '/'), 'title': insurer},
                          {'url': u'{}{}{}'.format('/health-insurance/', insurer_slug, '/network-hospitals/'), 'title': 'Network Hospitals'}]
                  })


class CityHospitalList(braces_views.JSONResponseMixin, braces_views.AjaxResponseMixin, ListView):
    model = Hospital

    def get_ajax(self, request, *args, **kwargs):
        context = {}
        response = super(self.__class__, self).get_ajax(request, *args, **kwargs)
        assert response.status_code is 200, {context['message']: 'Some error occoured'}
        hospital_list = response.context_data['hospital_list']
        context['hospital_list'] = [{'name': hospital.name,
                                     'address': hospital.address,
                                     'phone': hospital.phone1} for hospital in hospital_list]
        return self.render_json_response(context)

    def get_queryset(self):
        queryset = super(self.__class__, self).get_queryset()
        self.city = self.kwargs.get('city')
        self.state = self.kwargs.get('state')
        insurer = self.kwargs.get('insurer_slug')
        try:
            self.brandpage = BrandPage.objects.get(
                slug=insurer, type='health', _insurer__isnull=False)
            self.insurer = self.brandpage._insurer
        except BrandPage.DoesNotExist:
            raise Http404

        if not queryset.exists():
            raise Http404

        return queryset.filter(city__slug=self.city, state__slug=self.state, insurers=self.insurer)

    def get_context_data(self, *args, **kwargs):
        context = super(self.__class__, self).get_context_data(*args, **kwargs)
        context['insurer'] = self.insurer
        context['insurer_slug'] = self.kwargs.get('insurer_slug')
        try:
            context['state'] = State.objects.get(slug=self.state)
            context['city'] = context['state'].city_set.get(slug=self.city)
        except (State.DoesNotExist, City.DoesNotExist):
            raise Http404

        context['brand'] = self.brandpage
        context['canonical'] = True
        if self.city in ["delhi", "bangalore", "mumbai", "chennai", "kolkata", "hyderabad", "ernakulam", "pune"]:
            context['canonical'] = False

        return context


@utils.profile_type_only('WIKI_DATA_ENTRY')
@never_cache
def medical_condition_add(request, medical_condition_id=None):
    if medical_condition_id:
        medical_condition = get_object_or_404(MedicalCondition, pk=medical_condition_id)
        medical_condition_add_form = MedicalConditionAddForm(request, instance=medical_condition)
        medical_condition_add_form.init(medical_condition_id)
    else:
        medical_condition_add_form = MedicalConditionAddForm(request)
        medical_condition_add_form.init()
    return render_to_response("wiki/medical_condition_add.html", {
        'form': medical_condition_add_form,
    }, context_instance=RequestContext(request))


@utils.profile_type_only('WIKI_DATA_ENTRY')
def manage_medical_conditions(request, insurer_id):
    m_objects = MedicalCondition.objects.all()
    healthproducts = HealthProduct.objects.filter(insurer_id=insurer_id)
    form = MedicalPermissionAddForm()
    return render(request, 'wiki/manage_medical_conditions.html', {
        'healthproducts': healthproducts,
        'm_objects': m_objects,
        'form': form,
    })


@utils.profile_type_only('WIKI_DATA_ENTRY')
@never_cache
def edit_medical_condition(request, insurer_id):
    initial = {
        'medical_condition': MedicalCondition.objects.get(id=request.REQUEST['medical_condition']),
        'health_product': HealthProduct.objects.get(id=request.REQUEST['health_product']),
    }
    instance = MedicalTransactionPermission.objects.filter(
        medical_condition_id=initial['medical_condition'],
        health_product_id=initial['health_product']
    ).first()
    if request.POST:
        if instance:
            form = MedicalPermissionAddForm(request.POST, instance=instance)
        else:
            form = MedicalPermissionAddForm(request.POST)
        if form.is_valid():
            instance = form.save()
            return JsonResponse({
                "key": '%s-%s' % (request.POST['medical_condition'], request.POST['health_product']),
                "value": instance.get_transaction_permission_display(),
                "success": True
            })
        else:
            return JsonResponse({'success': False, 'errors': form.errors})
    else:
        if instance:
            initial.update({
                'transaction_permission': instance.transaction_permission
            })
        form = MedicalPermissionAddForm(initial=initial)
        return JsonResponse({
            'success': True,
            'form': form.as_p()
        })


def wiki_ajax_detach_medical_condition(request, product_id, medical_condition_id):
    health_products = []
    for pid in product_id.split("-"):
        health_products.append(get_object_or_404(HealthProduct, pk=pid))
    medical_condition = get_object_or_404(MedicalCondition, pk=medical_condition_id)
    insurer = health_products[0].insurer
    # Select the first product and attach all others to its clone
    obj = MedicalTransactionPermission.objects.filter(health_products=health_products[0], medical_condition=medical_condition)

    for o in obj:
        for health_product in health_products:
            o.health_products.remove(health_product)
    # Remove zombie conditions
    MedicalTransactionPermission.objects.filter(health_products__isnull=True).delete()

    return HttpResponse(json.dumps({
        'success': True,
        'detached': True,
        'corr_list': putils.get_correlated_products(medical_condition, insurer)
    }))

@utils.profile_type_only('WIKI_DATA_ENTRY')
@never_cache
def slug_check(request, app, model):
    M = apps.get_model(app.lower(), model.title())
    objs = M.objects.filter(slug=request.POST['slug'])
    if objs:
        return HttpResponse(json.dumps({'success': False}))
    else:
        return HttpResponse(json.dumps({'success': True}))

