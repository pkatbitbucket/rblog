set -e
set -x
export NPATH=`pwd`/node_modules
# node ${NPATH}/postcss-cli/bin/postcss -c static/pcss/conf.json -d static/css/build static/pcss/*.css
find static/pcss/*.css | grep -v "_" | xargs node ${NPATH}/postcss-cli/bin/postcss -c static/pcss/conf.json -d static/css