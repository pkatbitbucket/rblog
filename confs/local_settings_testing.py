import os

from path import path

LOCAL_SETTINGS_FILE_FOLDER = path(__file__).parent.abspath()

DATABASES = {
    'default': {
        # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or
        # 'oracle'.
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        # Or path to database file if using sqlite3.
        'NAME': 'coverfox_5thmarch',
        # Not used with sqlite3.
        'USER': 'postgres',
        # Not used with sqlite3.
        'PASSWORD': '',
        # Set to empty string for localhost. Not used with sqlite3.
        'HOST': '',
        # Set to empty string for default. Not used with sqlite3.
        'PORT': '',
    }
}

ALLOWED_HOSTS = (
    'localhost',
    '{{BRANCH}}.{{HOST}}',
)

ADMINS = (
    ('{{USER_NAME}}', '{{USER_EMAIL}}'),
)
MANAGERS = ADMINS

DEBUG = True

BROKER_URL = 'redis://127.0.0.1:6379/4'
CELERY_RESULT_BACKEND = 'redis://127.0.0.1:6379/6'

CACHES = {
    'compress_cache': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://127.0.0.1:6379/10',
    },
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        "LOCATION": "redis://127.0.0.1:6379/0",
    },
    'health-results': {
        'BACKEND': 'django_redis.cache.RedisCache',
        "LOCATION": "redis://127.0.0.1:6379/1",
    },
    'motor-results': {
        'BACKEND': 'django_redis.cache.RedisCache',
        "LOCATION": "redis://127.0.0.1:6379/7",
    }
}
COMPRESS_ENABLED = True

MEDIA_URL = "https://www.coverfox.com/static/"

COMPRESS_ROOT = os.path.join(LOCAL_SETTINGS_FILE_FOLDER, '../static')
COMPRESS_CACHE_BACKEND = 'compress_cache'
COMPRESS_CSS_HASHING_METHOD = 'mtime'
COMPRESS_CSS_FILTERS = ['compressor.filters.cssmin.CSSMinFilter', 'compressor.filters.css_default.CssAbsoluteFilter']
COMPRESS_JS_FILTERS = ['compressor.filters.jsmin.JSMinFilter']

JS_BUILD_DEBUG = False
CSS_BUILD_DEBUG = False
NOTIFICATION_DEBUG = False

SITE_URL = "http://{{BRANCH}}.{{HOST}}"

# ## LMS SETTINGS ## #
LMS_BOUGHT_SUCCESS_URL = 'http:// lms-dev.coverfox.com/data-push/bought/'
LMS_EVENT_URL = 'http://lms-dev.coverfox.com/data-push/event/'
