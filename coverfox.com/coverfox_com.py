from __future__ import absolute_import

from importd import d, debug, env, s
d.openenv()

from path import path
import os
import json
import sys
import djcelery

STATIC_PATH = d.dotslash('../static')
SETTINGS_FILE_FOLDER = path(__file__).parent.abspath()


def get_extra_settings_params():
    setting_params = {}
    # ## MOTOR BUILD SETTINGS ###
    try:
        with open(
            os.path.join(
                SETTINGS_FILE_FOLDER, '../static/js/motor/build/stats.json'
            )
        ) as f:
            j = json.loads(f.read())
            setting_params["WEBPACK_MOBILE_ASSETS"] = j['assetsByChunkName'],
            setting_params["WEBPACK_MOBILE_ASSETS_HASH"] = j['hash']
    except:
        pass

    try:
        with open(
            d.dotslash(
                '../static/travel_product/v1/build/stats.json'
            )
        ) as f:
            j = json.loads(f.read())
            setting_params["WEBPACK_TRAVEL_ASSETS"] = j['assetsByChunkName'],
            setting_params["WEBPACK_TRAVEL_ASSETS_HASH"] = j['hash']
    except:
        pass

    # ## MOTOR BUILD SETTINGS ###
    try:
        with open(
            os.path.join(
                SETTINGS_FILE_FOLDER, '../static/motor_product/build/stats.json'
            )
        ) as f:
            j = json.loads(f.read())
            setting_params["WEBPACK_ASSETS"] = j['assetsByChunkName']
            setting_params["WEBPACK_ASSETS_HASH"] = j['hash']
    except:
        pass

    try:
        with open(
            d.dotslash(
                '../static/travel_product/v1/build/stats.json'
            )
        ) as f:
            j = json.loads(f.read())
            setting_params["WEBPACK_TRAVEL_ASSETS"] = j['assetsByChunkName'],
            setting_params["WEBPACK_TRAVEaL_ASSETS_HASH"] = j['hash']
    except:
        pass

    return setting_params


# #### CELERY SETTINGS #####
djcelery.setup_loader()

d(
    # Django settings.
    DEBUG=debug(True, prod=False),
    DATABASES={
        'default': {
            # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or
            # 'oracle'.
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            # Or path to database file if using sqlite3.
            'NAME': env('DB_NAME', 'coverfox'),
            'USER': env('DB_USER', 'root'),                      # Not used with sqlite3.
            'PASSWORD': env('DB_PASSWORD', 'root'),                  # Not used with sqlite3.
            # Set to empty string for localhost. Not used with sqlite3.
            'HOST': env('DB_HOST', '127.0.0.1'),
            # Set to empty string for default. Not used with sqlite3.
            'PORT': env('DB_PORT', ''),
        }
    },
    # Hosts/domain names that are valid for this site; required if DEBUG is False
    # See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
    ALLOWED_HOSTS=[],
    # Local time zone for this installation. Choices can be found here:
    # http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
    # although not all choices may be available on all operating systems.
    # If running in a Windows environment this must be set to the same as your
    # system time zone.

    # TIME_ZONE = 'America/Chicago'
    TIME_ZONE='Asia/Calcutta',
    # Language code for this installation. All choices can be found here:
    # http://www.i18nguy.com/unicode/language-identifiers.html
    LANGUAGE_CODE='en-us',
    SITE_ID=1,
    # If you set this to False, Django will make some optimizations so as not
    # to load the internationalization machinery.
    USE_I18N=True,

    # If you set this to False, Django will not format dates, numbers and
    # calendars according to the current locale.
    USE_L10N=True,

    # If you set this to False, Django will not use timezone-aware datetimes.
    USE_TZ=True,

    # Absolute path to the directory that holds media.
    # Example: "/home/media/media.lawrence.com/"
    MEDIA_ROOT=d.dotslash('../static'),
    STATIC_URL='/static/',
    AUTH_USER_MODEL='core.User',
    LOGIN_REDIRECT_URL='/user/profile',
    ADMINS=(
        ("Devendra K Rane", "dev@coverfoxmail.com"),
        ("Aniket Thakkar", "aniket@coverfoxmail.com"),
        ("Vinay", "vinay@coverfoxmail.com"),
        ("Deepak", "deepak@coverfoxmail.com"),
        ("Jatinderjit", "jatinderjit@coverfoxmail.com"),
        ("Sanket Rathi", "sanket@coverfoxmail.com"),
        ("Rakesh Verma", "rakesh@coverfoxmail.com"),
        ("Nishant Shelar", "nishant@coverfoxmail.com"),
        ("Arpit o.O", "arpit+errorreports@coverfoxmail.com"),
        ("Amit Upadhyay", "amitu@coverfoxmail.com"),
        ("Team QA", "qa@coverfoxmail.com"),
    ),
    # URL that handles the media served from MEDIA_ROOT. Make sure to use a
    # trailing slash if there is a path component (optional in other cases).
    # Examples: "http://media.lawrence.com", "http://example.com/media/"
    MEDIA_URL='/media/',

    # URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
    # trailing slash.
    # Examples: "http://foo.com/media/", "/media/".
    MIDDLEWARE_CLASSES=(
        # 'debug_toolbar.middleware.DebugToolbarMiddleware',
        'django.middleware.cache.UpdateCacheMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'campaign.middlewares.LMSMiddleware',
        'putils.TrackMiddleware',
        # 'django.middleware.gzip.GZipMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        # 'django.middleware.cache.FetchFromCacheMiddleware',
        # 'core.pipeline.RedirectOnCancelMiddleware',
        'social.apps.django_app.middleware.SocialAuthExceptionMiddleware',
        'utils.GlobalRequestMiddleware',
        'django_user_agents.middleware.UserAgentMiddleware',
        'flatpages.middlewares.SEORedirectMiddleware',
        'flatpages.middlewares.FlatpageFallbackMiddleware',
        'cms.middlewares.CMSPageFallbackMiddleware',
        'putils.SetCookieForInternalIPMiddleware',
        'corsheaders.middleware.CorsMiddleware',
        'oauth2_provider.middleware.OAuth2TokenMiddleware',
        'cfcms.middleware.CmsPageMiddleware',
    ),

    ROOT_URLCONF='urls',

    TEMPLATE_DIRS=(
        d.dotslash("../base/templates"),
    ),

    INSTALLED_APPS=(
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'filebrowser',
        'grappelli',  # must be before django.contrib.admin
        'django.contrib.admin',
        'django.contrib.messages',
        'django.contrib.sites',
        'django.contrib.sitemaps',
        'django.contrib.humanize',
        'django.contrib.staticfiles',
        'social.apps.django_app.default',
        # 'raven.contrib.django.raven_compat',
        'django_user_agents',
        'gunicorn',
        'widget_tweaks',
        'tagging',
        'tools',
        'compressor',
        'mandrill',
        'dateutil',
        'djcelery',
        'redactor',
        'django_statsd',
        'home',
        'core',
        'blog',
        'seo',
        'wiki',
        'review',
        'rdesk',
        'activity',
        'health_product',
        'travel_product',
        'mailer',
        'campaign',
        'motor_product',
        'motor_product.motor_used_car',
        'motor_product.motor_offline_leads',
        'motor_product.motor_discount_code_admin',
        'leads',
        'flatpages',
        'brands',
        'cms',
        'cms.common',
        'django_mptt_admin',
        'apis',
        'oauth2_provider',
        'corsheaders',
        'account_manager',
        'seocms',
        'bakar',  # also delete handler404 setting in urls file
        'knowledge_center',
        'fixture_magic',
        'coverfox',
        'cfcms',
        'import_export',
        'motor_product.insurer_panel',
        'kombu.transport.django',
    ),

    AUTHENTICATION_BACKENDS=(
        'social.backends.google.GoogleOAuth2',
        'extended.backends.SuperBackend',
        'extended.backends.ReviewBackend',
        'django.contrib.auth.backends.ModelBackend',
        'oauth2_provider.backends.OAuth2Backend',
    ),

    TEMPLATE_CONTEXT_PROCESSORS=(
        "django.contrib.auth.context_processors.auth",
        "django.core.context_processors.debug",
        "django.core.context_processors.i18n",
        "django.core.context_processors.media",
        "django.core.context_processors.static",
        # "django.core.context_processors.tz",
        'social.apps.django_app.context_processors.backends',
        'social.apps.django_app.context_processors.login_redirect',
        "django.contrib.messages.context_processors.messages",
        "django.core.context_processors.request",
        "putils.context_processor",
        "flatpages.context_processors.seo",
    ),
    LOGGING={
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse'
            }
        },
        'formatters': {
            'verbose': {
                'format': (
                    '%(levelname)s %(asctime)s %(module)s %(process)d '
                    '%(thread)d %(message)s'
                )
            },
        },
        'handlers': {
            'null': {
                'level': 'DEBUG',
                'class': 'django.utils.log.NullHandler',
            },
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'verbose'
            },
            'mail_admins': {
                'level': 'ERROR',
                'filters': ['require_debug_false'],
                'class': 'django.utils.log.AdminEmailHandler',
                'include_html': True,
            },
            'sentry': {
                'level': 'ERROR',
                'filters': ['require_debug_false'],
                'class': (
                    'raven.contrib.django.raven_compat.handlers.SentryHandler'
                ),
            },
        },
        'loggers': {
            'core': {
                'handlers': ['mail_admins', 'console', 'sentry'],
                'level': 'ERROR',
                'propagate': True,
            },
            'django.request': {
                'handlers': ['mail_admins', 'console', 'sentry'],
                'level': 'ERROR',
                'propagate': True,
            },
            '': {
                'handlers': ['mail_admins', 'console', 'sentry'],
                'level': 'ERROR',
                'propagate': True,
            },
        }
    },
    # List of callables that know how to import templates from various sources.
    TEMPLATE_LOADERS=(
        'django.template.loaders.filesystem.Loader',
        'home.loaders.AppNamespaceLoader',
        'cms.loaders.CMSTemplateLayoutLoader',
        # 'django.template.loaders.app_directories.Loader',

    ),
    STATICFILES_DIRS=(
        STATIC_PATH,
    ),

    STATICFILES_FINDERS=(
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
        'django.contrib.staticfiles.finders.FileSystemFinder',
        'compressor.finders.CompressorFinder',
    ),

    # ## EMAIL SETTINGS ###
    SERVER_EMAIL='dev@cfstatic.org',
    DEFAULT_FROM_EMAIL='dev@cfstatic.org',
    EMAIL_HOST='localhost',
    EMAIL_HOST_PASSWORD='',
    EMAIL_HOST_USER='',
    EMAIL_PORT='25',
    EMAIL_USE_TLS=True,
    EMAIL_BACKEND='email_backend.CeleryEmailBackend',
    # EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

    # Coverfox settings
    # Make this unique, and don't share it with anybody.
    SECRET_KEY='&l0*)nf6+^6r1(v+o5p3q1ax9g^xj9coc9xdjz&f_g*pgwhsdu',
    PROD=env("IS_PROD", False),
    PRODUCTION=env("PRODUCTION", False),
    TESTING='test' in sys.argv,
    TEMPLATE_DEBUG=s("DEBUG"),
    JS_BUILD_DEBUG=True,
    CSS_BUILD_DEBUG=True,
    VERSION=1.46,
    INTERNAL_IPS=("127.0.0.1", "localhost"),
    INTERNAL_IP_LIST=[
        '127.0.0.1',
        '110.5.73.106',  # Glitterbug IP
        '49.248.29.218',
        '49.248.29.219',
        '49.248.29.220',
        '49.248.29.221',
        '49.248.29.222',
        '182.72.40.134',
        '183.87.94.22',
        '210.89.40.228 ',
        '210.89.40.227',
    ],
    # Static Generator
    WEB_ROOT=d.dotslash('../static/CACHE/html'),
    DOWNLOADS_ROOT=d.dotslash('../static/downloads'),
    POLICY_ROOT=d.dotslash('../static/policy'),
    CONF_ROOT=d.dotslash('../confs'),
    SERVER_NAME="localhost",
    # for httpschange user password postgres ubuntu
    SECURE_PROXY_SSL_HEADER=('HTTP_X_FORWARDED_PROTOCOL', 'https'),
    TRAVEL_ADMINS=(
        ("Arpit o.O", "arpit+travelreports@coverfoxmail.com"),
        ("Jatinderjit", "jatinderjit@coverfoxmail.com"),
        ("Suhas Ghule", "suhas@coverfoxmail.com"),
        ("Ramita", "ramitha@coverfox.com"),
        ("Jay", "jay@coverfox.com"),
        ("QA", "qa@coverfoxmail.com"),
        ("Mahesh", "mahesh@coverfox.com"),
    ),
    MANAGERS=s("ADMINS"),
    SEND_BROKEN_LINK_EMAILS=True,

    GENERAL_TOLL_FREE='1800 209 9920',
    HEALTH_TOLL_FREE='1800 209 9920',
    HEALTH_CLAIMS_TOLL_FREE='1800 209 9920',
    CAR_TOLL_FREE='1800 209 9930',
    TRAVEL_TOLL_FREE='1800 209 9950',
    CAR_LP_TOLL_FREE='1800 209 9930',
    CAR_CALL_ONLY='1800 209 9930',
    CAR_CLAIMS_TOLL_FREE='1800 209 9930',
    BIKE_TOLL_FREE='1800 209 9940',
    SERVICE_REQUEST_TOLL_FREE='1800 209 9970',


    GENERAL_CONTACT_MAIL='info@coverfox.com',
    HEALTH_CONTACT_MAIL='info@coverfox.com',
    CAR_CONTACT_MAIL='info@coverfox.com',

    OFFICE_ADDRESS=(
        "<strong>Coverfox Insurance Broking Pvt. Ltd.</strong><br>"
        "Krislon House,<br>"
        "B-Wing, 3rd Floor,<br>"
        "Saki Vihar Road, Andheri (E)<br>"
        "Mumbai, Maharashtra - 400072<br>"
    ),
    # ## FOR MAIL ALERTS ###
    HEALTH_TEAM=[
        'jatinderjit@coverfoxmail.com',
        'suhas@coverfoxmail.com',
        'noopur@coverfoxmail.com',
        'qa@coverfoxmail.com',
    ],

    # ## LMS SETTINGS ###
    LMS_BOUGHT_SUCCESS_URL='http://localhost:8001/data-push/bought/',
    LMS_EVENT_URL='http://localhost:8001/data-push/event/',
    LMS_SNS_TOPIC='arn:aws:sns:ap-southeast-1:542680859958:lms-data',
    LMS_DEV_SNS_TOPIC='arn:aws:sns:ap-southeast-1:542680859958:lms-sandman',
    SITE_NAME="WWW.COVERFOX.COM",
    SITE_URL="https://www.coverfox.com",
    SITE_TITLE="Coverfox : Insurance without confusion",
    SITE_KEYWORDS=[
        "life insurance in india",
        "health insurance in india",
        "car insurance in india",
        "mediclaim policy",
        "medical insurance",
        "what is insurance",
        "term plan in india",
        "health insurance india",
        "motor insurance"
    ],
    SITE_DESCRIPTION=(
        "Compare, understand and learn about insurance in India. Know your health "
        "insurance, life insurance, travel insurance, car insurance. We help you "
        "choose the best insurance for you"
    ),

    DND_MSG=(
        "I hereby authorize Coverfox to communicate with me on the given number "
        "for my Insurance needs. I am aware that this authorization will override "
        "my registry under NDNC."
    ),

    # Third party library settings
    # ##### COMPRESSOR SETTINGS ########

    COMPRESS_ENABLED=debug(False, prod=True),
    COMPRESS_OFFLINE=False,
    COMPRESS_YUGLIFY_BINARY='yuglify',  # assumes yuglify is in your path
    COMPRESS_YUGLIFY_CSS_ARGUMENTS='--terminal',
    COMPRESS_YUGLIFY_JS_ARGUMENTS='--terminal',
    COMPRESS_CSS_FILTERS=['compressor.filters.css_default.CssAbsoluteFilter',
                            'tools.compressor.filters.YUglifyCSSFilter'],
    COMPRESS_JS_FILTERS=['tools.compressor.filters.YUglifyJSFilter'],
    COMPRESS_ROOT=s('MEDIA_ROOT'),
    COMPRESS_CACHE_BACKEND='compress_cache',
    COMPRESS_CSS_HASHING_METHOD='mtime',

    # ##################################

    # LOGIN_URL          = '/new-landing/'
    # LOGIN_REDIRECT_URL = '/review/info-add'
    # LOGIN_ERROR_URL    = '/new-landing/'
    # LOGOUT_URL = '/accounts/logout/'
    SOCIAL_AUTH_LOGIN_ERROR_URL='/',
    SOCIAL_AUTH_LOGIN_REDIRECT_URL='/review/info-add/',

    FACEBOOK_SOCIAL_AUTH_RAISE_EXCEPTIONS=False,
    SOCIAL_AUTH_RAISE_EXCEPTIONS=False,
    RAISE_EXCEPTIONS=False,

    RANDOM_SEED='0f2bff241f41adc67e6b98d88160a381',

    # Additional locations of static files
    STATIC_PATH=STATIC_PATH,

    # Django messages default storage.
    MESSAGE_STORAGE='django.contrib.messages.storage.session.SessionStorage',

    # FACEBOOK SETTINGS
    FACEBOOK_APP_ID="545910808767230",
    FACEBOOK_APP_SECRET="70d63533c6c43cbf9f1e3977bbd804f2",

    SOCIAL_AUTH_URL_NAMESPACE='social',

    SOUTH_MIGRATION_MODULES={
        'default': 'social.apps.django_app.default.south_migrations',
    },

    SOCIAL_AUTH_FACEBOOK_SCOPE=['email', 'user_birthday', 'user_relationships'],

    SOCIAL_AUTH_FACEBOOK_KEY="545910808767230",
    SOCIAL_AUTH_FACEBOOK_SECRET="70d63533c6c43cbf9f1e3977bbd804f2",

    SOCIAL_AUTH_LINKEDIN_OAUTH2_KEY='78e0qwtsey8d59',
    SOCIAL_AUTH_LINKEDIN_OAUTH2_SECRET='U4va0BbPaTa2ndC8',
    SOCIAL_AUTH_LINKEDIN_OAUTH2_SCOPE=['r_fullprofile', 'r_emailaddress', ],
    SOCIAL_AUTH_LINKEDIN_OAUTH2_FIELD_SELECTORS=['positions', 'email-address', ],

    # GOOGLE PLUS SETTINGS
    SOCIAL_AUTH_GOOGLE_OAUTH2_KEY='526150544311-ebf82glh4td7svbq6r5dff0kp233nl9e.apps.googleusercontent.com',
    SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET='9RpvBW6ixeSdmlH3SrFXStJk',
    SOCIAL_AUTH_GOOGLE_PLUS_KEY=s("SOCIAL_AUTH_GOOGLE_OAUTH2_KEY"),
    SOCIAL_AUTH_GOOGLE_PLUS_SECRET=s("SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET"),

    SOCIAL_AUTH_USER_MODEL='core.User',

    MANDRILL_API_KEY='3LCsJNj05Izr3lKNidmEtg',
    MAILCHIMP_API_KEY='dd8c0051d49567ca95fecc098629ff56-us5',
    BLOG_SUB_LIST_ID='a666be07eb',

    KISS_API_KEY='9549f7f08f5679b76a2443e9e9efeba8cd92321b',
    KISS_GLOBAL_ID='c519f040-17ea-0131-5228-1231381fa34d',

    ## FASTLANE ##
    FAST_LANE_URL = "https://web.fastlaneindia.com/vin/api/v1/vehicle",
    FAST_LANE_BASIC_USERNAME = "I003PROD1",
    FAST_LANE_BASIC_PASSWORD = "i003cf@pd2909",

    # PATHS
    MOTOR_RSA_PDF_PATH = "motor_rsa_pdfs/cfox",
    WKHTMLTOPDF_PATH = "/usr/local/bin/wkhtmltopdf",

    URL_PATH='',
    SOCIAL_AUTH_STRATEGY='social.strategies.django_strategy.DjangoStrategy',
    SOCIAL_AUTH_STORAGE='social.apps.django_app.default.models.DjangoStorage',
    SOCIAL_AUTH_GOOGLE_OAUTH_SCOPE=[
        'https://www.googleapis.com/auth/userinfo.profile'
    ],
    SOCIAL_AUTH_FORCE_EMAIL_VALIDATION=False,
    # SOCIAL_AUTH_EMAIL_FORM_URL = '/signup-email'
    # SOCIAL_AUTH_EMAIL_FORM_HTML = 'core/soc/email_signup.html'
    # SOCIAL_AUTH_EMAIL_VALIDATION_FUNCTION = 'core.mail.send_validation'
    # SOCIAL_AUTH_EMAIL_VALIDATION_URL = '/soc-email-sent/'
    SOCIAL_AUTH_USERNAME_FORM_URL='/signup-username',
    SOCIAL_AUTH_USERNAME_FORM_HTML='core/soc/username_signup.html',
    SOCIAL_AUTH_USER_FIELDS=['username', 'email'],
    SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL=True,

    SOCIAL_AUTH_PIPELINE=(
        'social.pipeline.social_auth.social_details',
        'social.pipeline.social_auth.social_uid',
        'social.pipeline.social_auth.auth_allowed',
        'social.pipeline.social_auth.social_user',
        'social.pipeline.user.get_username',
        # 'core.pipeline.require_email',
        # 'social.pipeline.mail.mail_validation',
        'core.pipeline.create_user',
        # 'social.pipeline.user.create_user',
        'social.pipeline.social_auth.associate_user',
        # 'social.pipeline.social_auth.load_extra_data',
        # 'core.pipeline.load_extra_data',
        # 'social.pipeline.user.user_details',
    ),

    # FILEBROWSER settings
    FILEBROWSER_DIRECTORY="uploads/articles/",
    URL_FILEBROWSER_MEDIA="/static/filebrowser/",
    URL_TINYMCE="/static/grappelli/tinymce/jscripts/tiny_mce/",

    # django-tagging settings
    FORCE_LOWERCASE_TAGS=True,
    MAX_TAG_LENGTH=100,

    # A sample logging configuration. The only tangible logging
    # performed by this configuration is to send an email to
    # the site admins on every HTTP 500 error when DEBUG=False.
    # See http://docs.djangoproject.com/en/dev/topics/logging for
    # more details on how to customize your logging configuration.

    LOG_PATH=d.dotslash('../logs'),

    USE_WYSIWYG="TINYMCE",  # Options TINYMCE, LIVEEDITOR

    # SLACK SETTINGS ##
    SLACK_BASE_URL='https://slack.com/api',
    SLACK_API_TOKEN='xoxp-2990649255-3041351219-3099879940-cfa5b0',
    SLACK_KEY='xoxp-2990649255-2992629782-3032510536-9bda12',

    # SENTRY SETTINGS ###
    RAVEN_CONFIG={
        'dsn': None
    },

    BROKER_URL='redis://localhost:6381/1',
    CELERY_RESULT_BACKEND='redis://localhost:6381/2',
    # adding freshdesk tasks directly from libs folder for now
    # i know this is a bad idea
    # need to change to add it from utils
    CELERY_IMPORTS=('email_backend', 'motor_product.prod_utils',
                    'motor_product.tasks.best_premiums', 'freshdesk',
                    'motor_product.insurer_panel.utils'),
    CELERYBEAT_SCHEDULER='djcelery.schedulers.DatabaseScheduler',

    CELERY_QUEUES={
        'default': {
            'exchange': 'default',
            'binding_key': 'default',
        },
        'motor_premium': {
            'exchange': 'motor_premium',
            'binding_key': 'motor_premium',
        },
        'motor_premium_refresh': {
            'exchange': 'motor_premium_refresh',
            'binding_key': 'motor_premium_refresh',
        },
        'mail': {
            'exchange': 'mail',
            'binding_key': 'mail',
        },
        'sns': {
            'exchange': 'sns',
            'binding_key': 'sns',
        },
        'motor_premium_cron': {
            'exchange': 'motor_premium_cron',
            'binding_key': 'motor_premium_cron',
        },
        'motor_bajaj_allianz': {
            'exchange': 'motor_bajaj_allianz',
            'binding_key': 'motor_bajaj_allianz',
        },
        'motor_bharti_axa': {
            'exchange': 'motor_bharti_axa',
            'binding_key': 'motor_bharti_axa',
        },
        'motor_hdfc_ergo': {
            'exchange': 'motor_hdfc_ergo',
            'binding_key': 'motor_hdfc_ergo',
        },
        'motor_iffco_tokio': {
            'exchange': 'motor_iffco_tokio',
            'binding_key': 'motor_iffco_tokio',
        },
        'motor_l_t': {
            'exchange': 'motor_l_t',
            'binding_key': 'motor_l_t',
        },
        'motor_universal_sompo': {
            'exchange': 'motor_universal_sompo',
            'binding_key': 'motor_universal_sompo',
        },
        'motor_new_india': {
            'exchange': 'motor_new_india',
            'binding_key': 'motor_new_india',
        },
        'motor_oriental': {
            'exchange': 'motor_oriental',
            'binding_key': 'motor_oriental',
        },
        'motor_tata_aig': {
            'exchange': 'motor_tata_aig',
            'binding_key': 'motor_tata_aig',
        },
        'motor_future_generali': {
            'exchange': 'motor_future_generali',
            'binding_key': 'motor_future_generali',
        },
        'motor_liberty_videocon': {
            'exchange': 'motor_liberty_videocon',
            'binding_key': 'motor_liberty_videocon',
        },
        'motor_icici_lombard': {
            'exchange': 'motor_icici_lombard',
            'binding_key': 'motor_icici_lombard',
        },
        'motor_reliance': {
            'exchange': 'motor_reliance',
            'binding_key': 'motor_reliance',
        },
        'motor_data_import': {
            'exchange': 'motor_data_import',
            'binding_key': 'motor_data_import',
        }
    },

    CELERY_DEFAULT_QUEUE='default',

    # CACHES FOR CELERY
    CACHES={
        'compress_cache': {
            'BACKEND': 'django_redis.cache.RedisCache',
            'LOCATION': 'redis://127.0.0.1:6381/10',
        },
        'default': {
            'BACKEND': 'django_redis.cache.RedisCache',
            "LOCATION": "redis://127.0.0.1:6381/0",
        },
        'health-results': {
            'BACKEND': 'django_redis.cache.RedisCache',
            "LOCATION": "redis://127.0.0.1:6381/1",
        },
        'motor-results': {
            'BACKEND': 'django_redis.cache.RedisCache',
            "LOCATION": "redis://127.0.0.1:6381/2",
        }
    },

    # ELASTIC SEARCH SETTINGS
    ELASTIC_SEARCH_URL="http://localhost:9200",

    # REDACTOR SETTINGS
    REDACTOR_OPTIONS={'lang': 'en', 'plugins': ['table']},

    # ## CORS SETTINGS ###
    CORS_ALLOW_HEADERS=(
        'x-requested-with',
        'content-type',
        'accept',
        'origin',
        'authorization',
        'x-csrftoken',
        'user-agent',
        'accept-encoding',
        'cfdate',
        'cfauthorization',
        'cfjsapi',
        'date',
    ),
    CORS_ORIGIN_WHITELIST=(),
    CORS_URLS_REGEX=r'^/apis/.*$',

    SOCIAL_AUTH_FACEBOOK_EXTRA_DATA=[
        ('birthday', 'birthday'), ('first_name', 'first_name'),
    ],
    SERVICE_TAX_PERCENTAGE=14.5,

    LC_LOCALE='en_IN',
    SESSION_SERIALIZER='django.contrib.sessions.serializers.PickleSerializer',

    # CFBLOG SETTINGS
    CFBLOG_BUCKET='cmsblogimages',
    CFBLOG_ACCESS_KEY='AKIAJ5P24RYZ7Y23XV2Q',
    CFBLOG_SECRET_ACCESS_KEY='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',
    # STATSD SETTINGS
    STATSD_HOST='ops.coverfox.com',
    STATSD_PORT=8125,
    STATSD_PREFIX=None,
    STATSD_MAXUDPSIZE=512,
    STATSD_CLIENT='django_statsd.clients.null',
    STATSD_RECORD_KEYS=[
        'window.performance.timing.navigationStart',
        'window.performance.timing.domComplete',
        'window.performance.timing.domInteractive',
        'window.performance.timing.domLoading',
        'window.performance.timing.loadEventEnd',
        'window.performance.timing.responseStart',
    ],
    # Configuration required for Boto
    BOTO_CONFIG = {
        'aws_access_key_id': 'AKIAIU54QAXYVFBND27A',
        'aws_secret_access_key': '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',
        'sns_region_name': 'ap-southeast-1',
        'sns_region_endpoint': 'sns.ap-southeast-1.amazonaws.com',
    },
    SETTINGS_FILE_FOLDER=SETTINGS_FILE_FOLDER,
    **get_extra_settings_params()
)

if __name__ == "__main__":
    d.main()
