import os
from fabric.api import (abort, env, local, parallel, prefix, prompt, roles,
                         run, sudo, cd, task, warn_only)
from fabric.contrib.files import exists

from contextlib import contextmanager as _contextmanager

OPENVPN_RSA_DIR = "/etc/openvpn/easy-rsa/"
OPENVPN_KEYS = os.path.join(OPENVPN_RSA_DIR, "keys")
OPENVPN_USER_STUFF = os.path.join(OPENVPN_RSA_DIR, "users")

OPENSSL_SUBJECT = "/C=IN/ST=Maharashtra/L=Maharashtra/O=Coverfox/OU=Engineering/CN=%s/emailAddress=%s/name=%s"
env.roledefs = {
    'vpn': ['ubuntu@dream.coverfox.com']
}

@_contextmanager
def sourcevars():
    cmd = "source %s" % os.path.join(OPENVPN_RSA_DIR, "vars")
    with prefix(cmd):
        yield

def generate_stuff(user):
    dirname = "%s/%s" % (OPENVPN_USER_STUFF, user)
    email = user+"@coverfoxmail.com"
    subject = OPENSSL_SUBJECT % (user, email, user)

    try:
        if not exists("%s/user.crt" % dirname):
            run("mkdir -p %s" % dirname)
            run("openssl genrsa -out %s/priv.key 2048" % dirname)
            run("openssl req -new -sha256 -key %s/priv.key -out %s/user.csr -subj %s" %
                (dirname, dirname, subject)
            )

            with cd(OPENVPN_RSA_DIR):
                with sourcevars():
                    sudo("openssl ca -in %s/user.csr -out %s/user.crt  -keyfile keys/ca.key -config openssl-1.0.0.cnf" %
                        (dirname, dirname)
                    )

        #phew! we are done. lets zip everythin in tmp
        run("rm -rf /tmp/%s.zip" % user)
        run("zip -j -r /tmp/%s.zip %s %s/cfsingapore_openvpn.ovpn %s/ta.key %s/ca.crt" %
                (user, dirname, OPENVPN_USER_STUFF, OPENVPN_KEYS, OPENVPN_KEYS)
        )
        run("sendemail -f ubuntu@vpn.coverfox.com -t %s -m 'Your VPN Access (Keep Safe)' -u 'Your VPN Access to dream.coverfox.com' -a /tmp/%s.zip" % (email, user))

    finally:
        run("rm -rf /tmp/%s.zip" % user)

@roles('vpn')
def create_vpn_access(username):
    generate_stuff(username)

@roles('vpn')
def revoke_vpn_access(username):
    with cd(OPENVPN_KEYS):
        userid = sudo("grep /CN=%s/ index.txt |cut -f 4" % username).strip()
        if not exists("%s.crt" % username):
            sudo("cp %s.pem %s.crt" % (userid, username))
    with cd(OPENVPN_RSA_DIR):
        with sourcevars():
            with warn_only():
                sudo("./revoke-full %s" % username)
        sudo("grep /CN=%s/ keys/index.txt" % username)
