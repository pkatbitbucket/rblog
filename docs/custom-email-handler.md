# Django Custom Email Handler


### Email Handler
```
from django.utils.log import AdminEmailHandler
from django.conf import settings

class CustomEmailHandler(AdminEmailHandler):
   def __init__(self, include_html=False, 
   						  email_backend=None, 
   						  admins=None):
   		super(AdminEmailHandler,self).__init__(include_html=include_html, 
   				  							email_backend=email_backend)
   		self.admins = admins
   		
   def send_mail(self, subject, message, *args, **kwargs):
   		connection = self.connection()
   		admins = self.admins or settings.ADMINS
   		html_message = kwargs.get('html_message', None)
   		fail_silently = True
 
 		if not admins:
 			return
 			
 		mail = EmailMultiAlternatives(
 				'%s%s' % (settings.EMAIL_SUBJECT_PREFIX, subject),
 				message, 				
 				settings.SERVER_EMAIL, [a[1] for a in settings.ADMINS], 				connection=connection)
 		
		if html_message:
			mail.attach_alternative(html_message, 'text/html')
		mail.send(fail_silently=fail_silently)
	
```

### Settings
```
class NoLogFilter(logging.Filter):
	def __init__(self, name):
	 	super(NoLogFilter, self).__init__()
	 	self.name = name
	 	
    def filter(self, record):
        return record.name == name
   


LMS_ADMINS = (('Gullu', 'gulshan@coverfoxmail.com'),
				 ('Rathi', 'sanket@coverfoxmail.com'))
				
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
    	'require_debug_false': {
    		 '()': 'django.utils.log.RequireDebugFalse'
    	},
    	'no_requirment_log': {
    		  '()': NoLogFilter,
    		  'name': 'core.requirement',
    	},
    }
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
        'lms_admins': {
            'level': 'ERROR',
            'class': 'core.email_handler.CustomEmailHandler',
            'admins': LMS_ADMINS,
        },
        'mail_admins': {
        		'level': 'ERROR',
        		'filters': ['require_debug_false', 'no_requirement_log'],
        		'class': 'django.utils.log.AdminEmailHandler',
        		'include_html': True,
        },
    },

    'loggers': {
        'core.requirement': {
            'handlers': ['lms_admins' ],
            'level': 'ERROR',
            'propagate': False, 
            # if propogate true, you need to use no_requirement_log
            # filter, otherwise you don't (best guess)
        },
        '': {
        	 'handlers': ['mail_admins'],
        	 'level': 'ERROR',
        }
    },
}
```