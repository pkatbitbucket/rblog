from django.conf.urls import *
from django.contrib.auth.views import login, logout
from django.conf import settings

from core.sitemap import BlogSitemap, StaticSitemap

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

sitemaps = {
    'blog' : BlogSitemap,
    'view' : StaticSitemap
}

urlpatterns = patterns('',

    (r'^grappelli/(?P<path>.*)$', 'django.views.static.serve',
        {
            'document_root': settings.SETTINGS_FILE_FOLDER.joinpath('../static/grappelli/'),
            'show_indexes': False
        }
    ),
    (r'^grappelli/', include('grappelli.urls')),

    (r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
    # Static serving for css images and stuff, for media (pics/videos), make different static serve
    (r'^nimda/', admin.site.urls),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve',
        {
            'document_root': settings.SETTINGS_FILE_FOLDER.joinpath('../static'),
            'show_indexes': False
        }
    ),
    url(r'^ajax/AjaxField/(?P<form_name>[\w-]+)/(?P<form_meta>[\w-]+)/$', 'customforms.ajaxfield.widget_url'),
    (r'^articles/', include('blog.urls')),
    (r'^seo/', include('seo.urls')),
    (r'^wiki/', include('wiki.urls')),
    (r'', include('core.urls')),

    (r'^accounts/login/$',  login),
    (r'^accounts/logout/$', logout),

    (r'^facebook/', include('django_facebook.urls')),
    (r'^accounts/', include('django_facebook.auth_urls')),
)
