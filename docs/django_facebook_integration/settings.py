# Django settings for MYPROJECT project.
import sys
import os

from path import path

SETTINGS_FILE_FOLDER = path(__file__).parent.abspath()
sys.path.append(SETTINGS_FILE_FOLDER.joinpath("../libs").abspath())

DEBUG = True
TEMPLATE_DEBUG = DEBUG
INTERNAL_IPS = ("127.0.0.1", "localhost")

COMPRESS_ENABLED=False

#CACHE_MIDDLEWARE_SECONDS = 60*60  #60 minutes
#CACHE_MIDDLEWARE_KEY_PREFIX = "rblog"
#CACHES = {
#    'default': {
#        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
#        'LOCATION': '127.0.0.1:11211',
#    }
#}
#CACHE_MIDDLEWARE_ANONYMOUS_ONLY = True
#SESSION_ENGINE = "django.contrib.sessions.backends.cache"

#Static Generator
WEB_ROOT = os.path.join(SETTINGS_FILE_FOLDER,'../static/CACHE/html')
SERVER_NAME = "localhost"

ADMINS = (
    ("Devendra K Rane", "ranedk@gmail.com"),
    ("Aniket Thakkar", "aniket.thakkar@gmail.com"),
)
MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'cfblog',                      # Or path to database file if using sqlite3.
        'USER': 'root',                      # Not used with sqlite3.
        'PASSWORD': 'asd',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.

#TIME_ZONE = 'America/Chicago'
TIME_ZONE = 'Asia/Calcutta'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(SETTINGS_FILE_FOLDER,'../static')
COMPRESS_ROOT = MEDIA_ROOT
STATIC_PATH = '/static'
STATIC_URL = '/static/'

AUTH_USER_MODEL = 'core.User'
LOGIN_REDIRECT_URL = '/login/'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/static/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".

# Make this unique, and don't share it with anybody.
SECRET_KEY = '&l0*)nf6+^6r1(v+o5p3q1ax9g^xj9coc9xdjz&f_g*pgwhsdu'
RANDOM_SEED = '0f2bff241f41adc67e6b98d88160a381'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

STATICFILES_FINDERS = (
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
        'django.contrib.staticfiles.finders.FileSystemFinder',
        'compressor.finders.CompressorFinder',
    )

MIDDLEWARE_CLASSES = (
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
   SETTINGS_FILE_FOLDER.joinpath("templates"),
)


TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'filebrowser',
    'grappelli', #must be before django.contrib.admin
    'django.contrib.admin',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.humanize',

    'gunicorn',
    'south',
    'widget_tweaks',
    'django.contrib.markup',
    'comments',
    #'django_extensions',
    'tagging',
    'tools',
    'compressor',
    'django_facebook',

    'core',
    'blog',
    'seo',
    'wiki',
)

AUTHENTICATION_BACKENDS = (
    'extended.backends.SuperBackend',
    'django_facebook.auth_backends.FacebookBackend',
    'django.contrib.auth.backends.ModelBackend',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    #"django.core.context_processors.media",
    #"django.core.context_processors.static",
    #"django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    'django_facebook.context_processors.facebook',
    "django.core.context_processors.request",
    "putils.context_processor",
)

#FACEBOOK SETTINGS
FACEBOOK_APP_ID = "545910808767230"
FACEBOOK_APP_SECRET = "70d63533c6c43cbf9f1e3977bbd804f2"



#FILEBROWSER settings
FILEBROWSER_DIRECTORY = "uploads/articles/"
URL_FILEBROWSER_MEDIA = "/static/filebrowser/"
URL_TINYMCE = "/static/grappelli/tinymce/jscripts/tiny_mce/"

#django-tagging settings
FORCE_LOWERCASE_TAGS = True
MAX_TAG_LENGTH = 100

# python -m smtpd -n -c DebuggingServer localhost:1025
DEFAULT_FROM_EMAIL = "varun@coverfox.in"
EMAIL_HOST = "smtp.gmail.com"
EMAIL_HOST_PASSWORD = '123costarica'
EMAIL_HOST_USER = 'varun@coverfox.in'
EMAIL_PORT = '587'
EMAIL_USE_TLS = True

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level':'DEBUG',
            'class':'django.utils.log.NullHandler',
        },
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        # I always add this handler to facilitate separating loggings
        'log_file':{
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(SETTINGS_FILE_FOLDER,'../logs/django.log'),
            'maxBytes': '16777216', # 16megabytes
            'formatter': 'verbose'
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        }
    },
    'loggers': {
        'django': {
            'handlers': ['mail_admins', 'log_file', 'console'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

SITE_NAME = "WWW.COVERFOX.COM"
SITE_URL = "http://www.coverfox.com"
SITE_TITLE = "Coverfox : Insurance without confusion"
SITE_KEYWORDS = ["life insurance in india", "health insurance in india", "car insurance in india", "mediclaim policy", "medical insurance","what is insurance","term plan in india","health insurance india", "motor insurance"]
SITE_DESCRIPTION = "Compare, understand and learn about insurance in India. Know your health insurance, life insurance, travel insurance, car insurance. We help you choose the best insurance for you"

USE_WYSIWYG = "TINYMCE" # Options TINYMCE, LIVEEDITOR

##### local_settings.py ######
#DEBUG = False
#ALLOWED_HOSTS = ["coverfox.com", "www.coverfox.com","testserver"]

#STATIC_PATH = "http://cfstatic.org/static"
#COMPRESS_URL = "http://cfstatic.org/static/"
#MEDIA_URL = "http://cfstatic.org/static/"
#CACHES = {
#        'default': {
#            'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
#            }
#        }
#SERVER_NAME = "www.coverfox.com"
#COMPRESS_ENABLED=True

try:
    from local_settings import *
except ImportError:
    pass
