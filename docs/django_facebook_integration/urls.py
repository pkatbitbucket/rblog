from django.conf.urls import patterns, url, include

from fhurl import fhurl
from core.models import User
from core.forms import *
from core.views import *

urlpatterns = patterns('blog.views',
    url(r'^$', 'index', name='index'),
)

urlpatterns += patterns('core.views',
    url(r'^fb/$', 'fb', name='fb'),

    url(r'^author-dashboard/$',
        view=AdminPostListView.as_view(),
        name='author_dashboard',
    ),

    url(r'^articles/(?P<pk>[\d]+)/$',
        view=AdminPostDetailView.as_view(),
        name='post_detail_admin'
    ),

    url(r'^ajax/get-token/$', 'get_token', name="get_token"),
    url(r'^user-manage/$', ListView.as_view(
            queryset=User.objects.all(),
            context_object_name="user_list",
            template_name = "core/user_manage.html"
        ),
        name="user_manage"
    ),
    url(r'^login/$', 'login', name='login'),
    fhurl(r'^ajax/user-manage/$', UserForm, name="ajax_user_add", template="core/ajax_user_manage.html", json=True),
    fhurl(r'^ajax/user-manage/(?P<uid>\d{1,2})/$', UserForm, name="ajax_user_edit", template="core/ajax_user_manage.html", json=True),
)
