import datetime
import hashlib
import logging

from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.conf import settings
from django.core.urlresolvers import reverse

from django.shortcuts import render_to_response, get_object_or_404
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.core import serializers
from django.forms.models import modelformset_factory
from django.forms.formsets import formset_factory
from django.contrib.auth import authenticate, login
from django.utils import simplejson
from django.middleware import csrf
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache

from core.forms import *
from core.models import *
from blog.models import Post
import putils
from customdb.customquery import sqltojson, sqltodict, executesql
from xl2python import Xl2Python

def fb(request):
    return render_to_response("core/fb.html",{
        },
        context_instance=RequestContext(request)
    )

def get_token(request):
    return HttpResponse(csrf.get_token(request))

@login_required
def login(request):
    if request.user.role == "AUTHOR":
        return HttpResponseRedirect(reverse('author_dashboard'))
    else:
        return HttpResponseRedirect(reverse('index'))

class AdminPostListView(ListView):
    model = Post
    paginate_by = getattr(settings,'BLOG_PAGESIZE', 30)
    template_name = "core/admin_post_list.html"

    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(AdminPostListView, self).dispatch(*args, **kwargs)

class AdminPostDetailView(DetailView):
    model = Post
    template = "blog/post_detail.html"

    @method_decorator(login_required)
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(AdminPostDetailView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AdminPostDetailView, self).get_context_data(**kwargs)
        context['recommended_reads'] = putils.get_recommended_reads(4, self.object.category)
        context.setdefault('meta', {})
        context['meta'].update(context['object'].get_metacontext())
        return context
