.. coverfox documentation master file, created by
   sphinx-quickstart on Thu Sep 24 15:39:36 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

coverfox's documentation
========================

Contents:

.. toctree::
   :maxdepth: 2

   style
   putils


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

