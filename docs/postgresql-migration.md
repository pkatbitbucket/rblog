## MySQL to Postgresql Migration Quirks

* Add a database named postgres to local_settings.py
* Run syncdb on the postgresql database
* Need to fix migration **wiki:0033**. Fix and run migration on postgresql database. To fix the migration, just comment the code corresponding to `add_col`;
* Run sqlflush on the postgresql database to truncate all the tables.
	
	```
	$ python manage.py sqlflush  --database=postgres |python manage.py dbshell --database=postgres
	```

* Data integrity changes required before dumping fixtures:
	1. Trunctate table blog_feedback;
	2. In table blog_posts_related_posts, delete rows which link blog posts to itself. (`from_post_id == to_post_id`)
	
* Dump fixtures from the mysql database. Load fixtures using django into **postgresql** database.
	
	```
    $ python manage.py dumpdata > fixtures.json
    $ python manage.py loaddata --database=postgres fixtures.json
	```
	
## Code TODO

* Make adding a blog post in relation to itself a NOOP. (is it required?)
* Change raw query to calculate nearest pincodes to a pincode.
* Change JSONField to not return 'json' type.


## Django GIS

* Connect to postgres and enable postgis extension on the database. 

	```
	CREATE EXTENSION postgis;
	```


