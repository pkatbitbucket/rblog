from __future__ import print_function, unicode_literals

import datetime
import os
import re
import sys
from contextlib import contextmanager as _contextmanager
from itertools import chain

from django.utils.functional import lazy
from django.utils.text import slugify
from fabric.api import cd as _cd
from fabric.api import settings as fab_settings
from fabric.api import (abort, env, local, parallel, prefix, prompt, roles,
                        run, sudo)
from fabric.colors import blue, green, magenta, red, yellow
from fabric.contrib import django
from fabric.contrib.files import exists, upload_template
from fabric.operations import put
from path import path as p
from pyfiglet import Figlet

django.settings_module('settings')
from django.conf import settings  # noqa isort:skip

LOCAL_REPO = p(__file__).parent.abspath()

INIT = False
WWW_DIR = "/home/www"
LOG_PATH = ''
BRANCH = 'master'
PREFIX = ''
VIRTUAL_ENV = "envproj"
REPO = "git@github.com:Coverfox/rblog.git"
STATIC_URL = ""

REPO_DIR = 'rblog'
DEPLOY_DIR = lazy(lambda: os.path.join(WWW_DIR, str(REPO_DIR)), str)()
BASE_DIR = lazy(lambda: os.path.join(str(DEPLOY_DIR), 'base'), str)()
VENV = lazy(lambda: os.path.join(str(DEPLOY_DIR), VIRTUAL_ENV), str)()

CELERY_QUEUES = lazy(
    lambda: [
        str(p(f).stripext().stripext())
        for f in os.listdir(os.path.join(LOCAL_REPO, 'confs/supervisor_confs/'))
        if f.startswith('celery_') and p(LOCAL_REPO).joinpath(
            'confs/supervisor_confs/{}'.format(f)).isfile()
    ],
    list
)()

PROD_CELERY = [q for q in CELERY_QUEUES
               if q not in ['celery_testing', 'celery_pre_master']]

log_folders = [
    'supervisord',
    'transaction_logs',
    'celery',
    'lms_requests',
    'account_manager',
    'error_mails',
    'quote_logs'
]

figlet = Figlet(font='slant')

env.forward_agent = True
env.roledefs = {
    'prod_main': ['ubuntu@aws.coverfox.com:333'],
    'prod_celery': ['ubuntu@54.169.225.207'],
    'sandman': ['coverfox@sandman.coverfox.com'],
    'uat': ['ubuntu@uat.coverfox.com'],
    'pre_master': ['ubuntu@54.169.217.43'],
    'local': ['{}@localhost'.format(env.local_user)],
}
env.roledefs['prod'] = list(set(
    host
    for host in chain(env.roledefs['prod_celery'], env.roledefs['prod_main'])
))


def _continue():
    if env.host_string in env.exclude_hosts:
        return False
    return True


def _upload_template(filename, destination, use_jinja=True, use_sudo=True,
                     template_dir=LOCAL_REPO, context=None):
    if not _continue():
        return

    if context is None:
        context = {}

    context.update({
        'LOG_PATH': LOG_PATH,
        'DEPLOY_DIR': DEPLOY_DIR,
        'VENV': VENV,
        'BRANCH': BRANCH,
        'PREFIX': PREFIX,
        'ENV_USER': env.user,
    })

    return upload_template(filename=filename, destination=destination,
                           context=context, use_jinja=use_jinja,
                           use_sudo=use_sudo, template_dir=template_dir)


def _run_patch(*args, **kwargs):
    if _continue():
        return run(*args, **kwargs)

_run = _run_patch


def _sudo_patch(*args, **kwargs):
    if _continue():
        return sudo(*args, **kwargs)

_sudo = _sudo_patch


def _branch_check():
    if BRANCH != slugify(BRANCH):
        abort(red("""\
        CANNOT DEPLOY THIS BRANCH.
        RENAME THIS BRANCH USING THE FOLLOWING COMMAND.

        git branch -m {} {}
        """.format(BRANCH, slugify(BRANCH))))


def _user_not_configured():
    abort(red(
        "CONFIGURE YOUR NAME AND COVERFOX EMAIL ID IN GIT USING\n{}".format(
            magenta("git config -e")
        )
    ))


def _git_user_check():
    try:
        user_name = local('git config user.name', capture=True)
        user_email = local('git config user.email', capture=True)
    except:
        _user_not_configured()
    else:
        if user_name and user_email and '@coverfoxmail.com' in user_email:
            return user_name, user_email
        else:
            _user_not_configured()


@parallel
def setup_local_settings():
    print(yellow('\t>>>> COPYING LOCAL SETTINGS <<<<<<<'))
    user_name, user_email = _git_user_check()
    _upload_template(
        filename='confs/local_settings_testing.py',
        destination='{}'.format(p(str(BASE_DIR)).joinpath('local_settings.py')),
        context={
            'USER_NAME': user_name,
            'USER_EMAIL': user_email,
            'HOST': env.host,
        }
    )


def prod():
    global WWW_DIR, REPO_DIR, BRANCH, PREFIX
    WWW_DIR = "/home/www"
    REPO_DIR = "rblog"
    BRANCH = 'master'
    PREFIX = 'cvfx'


def pre_master():
    global WWW_DIR, REPO_DIR, BRANCH, PREFIX
    WWW_DIR = "/home/www"
    REPO_DIR = "rblog"
    BRANCH = 'pre-master'
    PREFIX = ''


def sandman():
    global WWW_DIR, REPO_DIR, BRANCH, PREFIX, REPO
    WWW_DIR = "/home/www/sandman.coverfox.com"
    BRANCH = local("git branch | sed -n '/\* /s///p'", capture=True)
    REPO = local("git config remote.origin.url", capture=True)
    REPO_DIR = BRANCH
    PREFIX = '{}_'.format(BRANCH)


def uat():
    global WWW_DIR, REPO_DIR, BRANCH, PREFIX, REPO
    WWW_DIR = "/home/www/uat.coverfox.com"
    BRANCH = local("git branch | sed -n '/\* /s///p'", capture=True)
    REPO = local("git config remote.origin.url", capture=True)
    REPO_DIR = BRANCH
    PREFIX = '{}_'.format(BRANCH)


@_contextmanager
def _virtualenv(venv):
    cmd = "source %s" % os.path.join(venv, "bin/activate")
    with prefix(cmd):
        yield


def get_django_settings(*settings_):
    with _cd(BASE_DIR):
        with _virtualenv(VIRTUAL_ENV):
            sys.path.append('.')
            for setting in settings_:
                print('{}: {}'.format(setting, getattr(settings, setting)))


@parallel
def setup_log_folder():
    print(yellow('\t>>>> CREATING LOG FOLDERS <<<<<<<"'))
    with _cd(BASE_DIR):
        _sudo('mkdir -p {}'.format(LOG_PATH))
        _sudo('chown -R {0}:{0} {1}'.format(env.user, LOG_PATH))
        with _cd(LOG_PATH):
            for d in log_folders:
                _run('mkdir -p {}'.format(d))


@parallel
def setup_supervisord_conf():
    _upload_template(
        filename='confs/supervisord.conf',
        destination='/etc/supervisor/supervisord.conf'
    )


@parallel
def setup_supervisor_conf(*file_names):
    for file_name in file_names:
        if file_name.endswith('.template'):
            file_name = file_name[:len(file_name) - len('.template')]
        if not file_name.endswith('.conf'):
            file_name = '{}.conf'.format(file_name)
        remote_file_name = '{}{}'.format(
            PREFIX, file_name.replace('_testing', '')
        )
        _upload_template(
            filename='confs/supervisor_confs/{}.template'.format(file_name),
            destination='/etc/supervisor/conf.d/{}'.format(remote_file_name)
        )


def setup_gunicorn_confs():
    if env.host_string in env.roledefs['prod_main']:
        setup_supervisor_conf('gunicorn_coverfox', 'gunicorn_humanlly')
    elif env.host_string in env.roledefs['pre_master']:
        setup_supervisor_conf('gunicorn_pre_master', 'gunicorn_jarvis')
    else:
        setup_supervisor_conf('gunicorn_testing')


def setup_monitoring_confs():
    if env.host_string in env.roledefs['prod_main']:
        setup_supervisor_conf('monitoring')


@parallel
def setup_celery_conf(*file_names):
    setup_supervisor_conf(*file_names)


def setup_flower_conf():
    setup_supervisor_conf('flower')


@parallel
def clean_confs():
    _sudo('rm -fr /etc/supervisor/conf.d/{}*.conf'.format(PREFIX))


@parallel
def setup_confs(testing=False):
    print(yellow('\t>>>> COPYING SUPERVISOR CONFS <<<<<<<"'))
    setup_supervisord_conf()
    clean_confs()

    if testing:
        setup_celery_conf('celery_testing')
    else:
        if env.host_string in env.roledefs['pre_master']:
            setup_celery_conf('celery_pre_master')
        elif env.host_string in env.roledefs['prod_celery']:
            setup_celery_conf(*PROD_CELERY)

    with fab_settings(exclude_hosts=env.roledefs['prod_celery']):
        setup_gunicorn_confs()
        setup_monitoring_confs()
    if not testing:
        with fab_settings(exclude_hosts=env.roledefs['prod_main']):
            setup_flower_conf()


def generate_supervisor_groups():
    code = """
from StringIO import StringIO
from ConfigParser import RawConfigParser
from collections import defaultdict
from glob import glob
import sys


programs = []
conf_files = ['/etc/supervisor/supervisord.conf']
c = RawConfigParser()
with open('/etc/supervisor/supervisord.conf') as f:
    c.readfp(f)

if 'include' in c.sections():
    for path_ in c.get('include', 'files').split(' '):
        for conf in glob(path_):
            with open(conf) as f:
                c.readfp(f)
            conf_files.append(conf)

for section in c.sections():
    if section.startswith('program:'):
        programs.append(section[len('program:'):])

groups = defaultdict(list)
for prog in programs:
    if '_' in prog:
        group, prog_name = prog.split('_', 1)
        groups[group].append(prog)

c = RawConfigParser()
with open('/etc/supervisor/supervisord.conf') as f:
    c.readfp(f)

for group, progs in groups.items():
    if len(progs) > 1:
        if 'group:{}'.format(group) not in c.sections():
            c.add_section('group:{}'.format(group))
        c.set('group:{}'.format(group), 'programs', ','.join(progs))

o = StringIO()
c.write(o)
sys.stdout.write(o.getvalue())
o.close()
"""
    import StringIO
    with _virtualenv(VENV):
        sup_content = _run('python -c "{}"'.format(code), quiet=True)
    f = StringIO.StringIO(sup_content)
    put(f, '/etc/supervisor/supervisord.conf', use_sudo=True)


@parallel
def setup_git_repo(testing=False):
    global INIT
    if not exists(WWW_DIR):
        _sudo('mkdir -p {}'.format(WWW_DIR))
        _sudo('chown -R {0}:{0} {1}'.format(env.user, WWW_DIR))

    with _cd(WWW_DIR):
        if exists(REPO_DIR):
            with _cd(REPO_DIR):
                print(
                    yellow("\t>>>> PULLING {} <<<<<<<".format(BRANCH.title()))
                )
                _run("git pull origin %s" % BRANCH)
        else:
            print(yellow("\t>>>> CLONING REPO <<<<<<<"))
            with fab_settings(warn_only=True):
                if testing:
                    _run("git clone {0} --single-branch "
                         "--depth 1 -b {1} {1}".format(REPO, BRANCH))
                else:
                    _run("git clone {}".format(REPO))
                    with _cd(REPO_DIR):
                        _run("git checkout {}".format(BRANCH))
                INIT = True


@parallel
def setup_env(testing):
    global LOG_PATH, STATIC_URL
    print(yellow('\t>>>> SETTING UP VIRTUAL ENVIRONMENT <<<<<<<"'))
    with _cd(DEPLOY_DIR):
        if not exists(VIRTUAL_ENV):
            _run("virtualenv %s --no-site-packages" % VIRTUAL_ENV)
        with _virtualenv(VENV):
            if testing:
                _run("pip install -U pip")
                _run("pip install -r requirements.txt "
                     "--extra-index-url http://pypi.coverfox.com/ "
                     "--trusted-host pypi.coverfox.com")
            else:
                _run("pip install -r requirements.txt")

    settings_ = _run(
        'cd {} && source ../{}/bin/activate && fab get_django_settings:LOG_PATH,STATIC_URL'.format(
            BASE_DIR, VIRTUAL_ENV
        )
    )
    LOG_PATH = re.search(r'LOG_PATH: (.+)[\r\n]', settings_).groups()[0].strip()
    STATIC_URL = re.search(r'STATIC_URL: (.+)[\r\n]', settings_).groups()[0].strip()


@parallel
def setup_packages():
    packages = [
        'python-pip',
        'python-dev',
        'git',
        'libfreetype6-dev',
        'libxft-dev',
        'libpq-dev',
        'libgraphviz-dev',
        'libxml2',
        'libblas-dev',
        'liblapack-dev',
        'libatlas-base-dev',
        'gfortran',
        'libxml2-dev',
        'libxslt1-dev',
        'libopenblas-dev',
        'lib32ncurses5-dev',
        'supervisor',
        'nodejs'
    ]
    if INIT:
        _run('curl -sL https://deb.nodesource.com/setup_5.x > /tmp/nodejs.sh')
        _sudo('bash /tmp/nodejs.sh')

    print(yellow('\t>>>> INSTALLING SYSTEM PACKAGES <<<<<<<"'))
    with fab_settings(warn_only=True):
        _sudo('apt-get -y install %s' % ' '.join(packages))
    if INIT:
        with fab_settings(warn_only=True):
            _sudo('ln -s /usr/bin/nodejs /usr/bin/node')
            _sudo('npm install webpack -g')
            _sudo('npm install grunt-cli -g')


@parallel
def clean_pyc():
    print(yellow('\t>>>> CLEANING *.pyc FILES <<<<<<<"'))
    with _cd(BASE_DIR):
        with _virtualenv(VENV):
            _run("python manage.py clean_pyc -p ..")


@parallel
def setup_db():
    print(yellow('\t>>>> MIGRATING <<<<<<<"'))
    with _cd(os.path.join(BASE_DIR)):
        with _virtualenv(VENV):
            _run("python manage.py migrate --fake-initial --noinput")


def setup_js(testing=False):
    print(yellow("\t>>>> RUNNING MOTOR WEBPACK <<<<<<<"))
    with _cd(DEPLOY_DIR):
        _run("npm install")

    with _cd(os.path.join(str(DEPLOY_DIR), "static/motor_product")):
        _run("../../node_modules/gulp-cli/bin/gulp.js build")

    with _cd(os.path.join(str(DEPLOY_DIR), "static/js")):
        cdn_script = '' if testing else "STATIC_URL={0} ".format(STATIC_URL)
        _run(cdn_script + "node ../../node_modules/webpack/bin/webpack.js -p")

    with _cd(os.path.join(str(DEPLOY_DIR), "static/js/motor")):
        _run("node ../../../node_modules/webpack/bin/webpack.js -p")

    print(yellow("\t>>>> RUNNING TRAVEL WEBPACK <<<<<<<"))
    with _cd(os.path.join(str(DEPLOY_DIR), "static/travel_product/v1")):
        _run("npm install")
        _run("webpack -p")

    # run grunt for health
    print(yellow("\t>>>> RUNNING GRUNT <<<<<<<"))
    with _cd(os.path.join(str(DEPLOY_DIR), "static/health_product")):
        _run("npm install")
        with _cd("desktopv2"):
            _run("ln -sf ../node_modules")
            _run("grunt")
        with _cd("desktopv2_steps"):
            _run("ln -sf ../node_modules")
            _run("grunt")
        with _cd("mobilev2"):
            _run("ln -sf ../node_modules")
            _run("grunt")
        with _cd("mobilev2_steps"):
            _run("ln -sf ../node_modules")
            _run("grunt")
        with _cd("js"):
            _run("ln -sf ../node_modules")
            _run("grunt")


@parallel
def restart_supervisor(testing=False):
    generate_supervisor_groups()
    _sudo('supervisorctl update')
    if testing:
        _sudo("supervisorctl restart {0}:".format(BRANCH))
    if env.host_string in env.roledefs['prod_main']:
        _sudo('supervisorctl restart gunicorn')
    elif env.host_string in env.roledefs['prod_celery']:
        _sudo('supervisorctl restart motor:')
        _sudo('supervisorctl restart celery:')


def _confirm():
    response = prompt("""
        THIS WILL DEPLOY {} TO {}.
        ARE YOU SURE??? SAY YES IN CAPS ===>
    """.format(
        magenta(BRANCH), magenta(', '.join(env.hosts or [env.host_string])))
    )
    if response != "YES":
        abort(red("ABORTING DEPLOYMENT"))

    local("clear")
    print(blue(figlet.renderText("HOLD YOUR BREATH")))


@parallel
def pre_deploy(quick=False, migrate=True, webpack=True,
               supervisor=True, testing=False):
    setup_git_repo(testing=testing)
    if testing and not quick:
        setup_local_settings()
    setup_packages()
    setup_env(testing=testing)
    if not quick:
        setup_log_folder()
        clean_pyc()
    if migrate:
        with fab_settings(exclude_hosts=env.roledefs['prod_celery']):
            setup_db()
    if webpack:
        with fab_settings(exclude_hosts=env.roledefs['prod_celery']):
            setup_js(testing)
    if supervisor:
        setup_confs(testing=testing)


@parallel
def post_deploy():
    print(yellow("\t>>>> CLEANUP >>>>>>>>>>>>"))
    with fab_settings(exclude_hosts=env.roledefs['prod_celery']):
        with _cd(os.path.join(DEPLOY_DIR, "static/motor_product")):
            _run("gulp clean:motor")

        with _cd(BASE_DIR):
            with _virtualenv(VENV):
                _run("python manage.py deploy_ping")
                # clear health cache
                # _run("python manage.py cleancache health-results")

    with _cd(BASE_DIR):
        with _virtualenv(VENV):
            _run("python manage.py export_place_json")
    print(blue(figlet.renderText("DID SHIT HIT THE ROOF?")))


@parallel
def deploy(*args):
    pre_deploy(
        quick='q' in args,
        migrate='m' in args,
        webpack='w' in args,
        supervisor='s' in args,
        testing='t' in args
    )
    restart_supervisor(testing='t' in args)


@roles('prod_main')
def deploy_production(*args):
    prod()
    remote_diff()
    args += ('m', 'w', 's')
    if 'q' not in args:
        _confirm()
    print("run this command to deploy\n{}".format(
        green("fab deploy_production_parallel")
    ))


@roles('prod')
@parallel
def deploy_production_parallel(*args):
    prod()
    args += ('m', 'w', 's')
    deploy(*args)
    post_deploy()


@roles('pre_master')
def deploy_pre_master():
    pre_master()
    _branch_check()
    _git_user_check()
    _confirm()
    deploy('s', 'm', 'w')


@roles('pre_master')
def deploy_pre_master_quick(*args):
    args += ('q',)
    pre_master()
    deploy(*args)


def _deploy_testing(*args):
    args += ('t',)
    deploy(*args)


@roles('sandman')
def deploy_sandman():
    sandman()
    _branch_check()
    _git_user_check()
    _confirm()
    _deploy_testing('s', 'm', 'w')


@roles('sandman')
def deploy_sandman_quick(*args):
    args += ('q',)
    sandman()
    _deploy_testing(*args)


@roles('uat')
def deploy_uat():
    uat()
    _branch_check()
    _git_user_check()
    _confirm()
    _deploy_testing('s', 'm', 'w')


@roles('uat')
def deploy_uat_quick(*args):
    args += ('q',)
    uat()
    _deploy_testing(*args)


@parallel
def get_remote_version():
    if not exists(DEPLOY_DIR):
        # deploying for the first time on remote host
        return 'master', None

    with _cd(DEPLOY_DIR):
        branch = _run("git rev-parse --abbrev-ref HEAD", quiet=True).strip()
        version = _run("git rev-parse HEAD", quiet=True).strip()
        print(
            "{}: {} is currently at {}:{}".format(
                datetime.datetime.now(), env.host_string, branch, version
            )
        )
        return branch, version


def remote_diff():
    assert local(
        "git rev-parse --abbrev-ref HEAD", capture=True
    ).strip() == BRANCH, "you are not on {}".format(BRANCH)
    local("git pull origin {}".format(BRANCH))
    local_version = local("git rev-parse HEAD", capture=True).strip()
    _, remote_version = get_remote_version()
    if remote_version and local_version != remote_version:
        if sys.platform == "darwin":
            default_open = "open"
        elif sys.platform.startswith('linux'):
            default_open = "xdg-open"
        elif sys.platform == 'win32':
            default_open = "start"
        else:
            default_open = None
        diff_url = (
            "https://github.com/Coverfox/rblog/compare/{}...{}\#diff".format(
                remote_version, local_version
            )
        )

        if default_open:
            local("{} {}".format(default_open, diff_url))
        else:
            local('echo open the following link yourself\n{}'.format(diff_url))
