from __future__ import absolute_import

from importd import d, debug, e, env
from path import path

import commands
import envdir
import sys

envdir.open()  # opens and applies varaibles in ./envdir folder
sys.path.extend(env("PYTHONPATH", "../base:../libs").split(":"))

PROD = env("IS_PROD", False)

if PROD:
    REDIS_URL = env("REDIS_URL", "redis://localhost:6379/")
    CACHE_BACKEND = 'django_redis.cache.RedisCache'
    CACHE_LOCATION = REDIS_URL + '6'
else:
    CACHE_BACKEND = 'django.core.cache.backends.db.DatabaseCache'
    CACHE_LOCATION = 'cache_table'

HUMANLLY_PATH = path(__file__).parent.abspath()
sys.path.append(d.dotslash("../base"))
sys.path.append(d.dotslash("../libs"))


d(
    DEFAULT_MTA='',
    DEFAULT_FROM_NAME='',
    JARVIS_REDIS_HOST='',
    JARVIS_REDIS_PORT=6381,
    NUDGESPOT='',
    # Django Stuff
    DEBUG=not PROD,
    INSTALLED_APPS=[
        "django.contrib.contenttypes",
        "django.contrib.auth",
        "django.contrib.sessions",
        "django.contrib.staticfiles",
        "django.contrib.sites",
        "django.contrib.sitemaps",
        "django.contrib.postgres",
        "django.contrib.humanize",
        # "django.contrib.flatpages",

        # "django_hstore",
        "django.contrib.admin",

        "waffle",
        "widget_tweaks",

        # debug stuff
        "debug:devserver",
        "debug:debug_toolbar",
        "django_statsd",

        "core",
        "coverfox",
        "kb",
        "eb",
        "hrms",
        "crm",
        "health_product",
        "motor_product",
        "travel_product",
        "wiki",
        "account_manager",
        "statemachine",
    ],
    ADMINS=(
        ("Amit Upadhyay", "amitu@coverfoxmail.com "),
        ("Hitul Mistry", "hitul.mistry@coverfoxmail.com")
    ),
    SITE_ID=1,
    STATIC_URL="/static/",
    STATICFILES_DIRS=(d.dotslash('../static'),),
    LOG_PATH=d.dotslash("../logs"),
    LOGIN_URL="/login/",
    TEMPLATE_CONTEXT_PROCESSORS=(
        "django.contrib.auth.context_processors.auth",
        "debug:django.core.context_processors.debug",
        "importd.esettings",
        "hrms.cp",
        "django.core.context_processors.i18n",
        "django.core.context_processors.media",
        "django.core.context_processors.request",
        "django.core.context_processors.static",
        "django.core.context_processors.tz",
        "django.contrib.messages.context_processors.messages",
        'django.core.context_processors.request',
    ),
    MIDDLEWARE_CLASSES=[
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
        'debug:debug_toolbar.middleware.DebugToolbarMiddleware',
        'debug:devserver.middleware.DevServerMiddleware',
        'django_statsd.middleware.GraphiteRequestTimingMiddleware',
        'django_statsd.middleware.GraphiteMiddleware',
        'hrms.middleware.RequestCompanyMiddleware',
    ],
    STATICFILES_FINDERS=(
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
        'django.contrib.staticfiles.finders.FileSystemFinder',
    ),
    AUTHENTICATION_BACKENDS=(
        'social.backends.google.GoogleOAuth2',
        'extended.backends.SuperBackend',
        'extended.backends.ReviewBackend',
        'oauth2_provider.backends.OAuth2Backend',
    ),
    USE_TZ=True,
    TIME_ZONE="Asia/Kolkata",
    CACHES={
        'default': {
            'BACKEND': CACHE_BACKEND,
            'LOCATION': CACHE_LOCATION,
            'TIMEOUT': 24 * 60 * 60,
        },
        'compress_cache': {
            'BACKEND': CACHE_BACKEND,
            'LOCATION': CACHE_LOCATION + '10',
        },
        'health-results': {
            'BACKEND': CACHE_BACKEND,
            "LOCATION": CACHE_LOCATION + '1',
        },
        'motor-results': {
            'BACKEND': CACHE_BACKEND,
            "LOCATION": CACHE_LOCATION + '2',
        }
    },
    ALLOWED_HOSTS = ['.humanlly.com', '127.0.0.1'],
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': env('DB_NAME', 'coverfox'),
            'USER': env('DB_USER', 'root'),
            'PASSWORD': env('DB_PASSWORD', ''),
            'HOST': env('DB_HOST', ''),
            'PORT': env('DB_POST', ''),
        }
    },

    TEMPLATE_DIRS = [d.dotslash("../templates")],
    AUTH_USER_MODEL = 'core.User',

    # coverfox stuff
    SETTINGS_FILE_FOLDER=HUMANLLY_PATH,
    MOTOR_RSA_PDF_PATH='motor_rsa_pdfs/cfox',
    PRODUCTION=False,
    SITE_URL="https://www.coverfox.com",
    INTERNAL_IPS=[
        '127.0.0.1',
        '110.5.73.106',  # Glitterbug IP
        '49.248.29.218',
        '49.248.29.219',
        '49.248.29.220',
        '49.248.29.221',
        '49.248.29.222',
        '182.72.40.134',
        '183.87.94.22',
        '210.89.40.228 ',
        '210.89.40.227',
        '124.124.12.137',
        '58.68.3.58',
    ],

    # humanlly.com stuff
    TESTING = "test" in sys.argv,
    COMMIT_ID = env("COMMIT_ID", ""),
    HUMANLY_S3_BUCKET="humanlly",
    AWS_ACCESS_KEY="AKIAIQKRBPRLABM755MQ",
    AWS_SECRET_KEY="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",
    RANDOM_SEED='0f2bff241f41adc67e6b98d88160a381',
    HUMANLLY_DOMAIN="humanlly.com",
    IMAGE_NOT_FOUND_URL="/static/img/humanlly/no-image-found.jpg",

    # statsd
    STATSD_HOST = 'localhost',
    STATSD_PORT = 8125,
    STATSD_PREFIX = 'humanlly',
    STATSD_MAXUDPSIZE = 512,
    STATSD_CLIENT = debug(
        'django_statsd.clients.toolbar', prod='django_statsd.clients.normal'
    ),
    STATSD_MODEL_SIGNALS = True,
    STATSD_CELERY_SIGNALS = True,
    SURL_REGEX={'username': '^((^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)|([\w]+\.[\w]+)|([\w]+))$'},

    debug=dict(
        # DEVSERVER stuff
        DEVSERVER_TRUNCATE_SQL=False,
        DEVSERVER_AUTO_PROFILE=True,
        DEVSERVER_MODULES=[
            'devserver.modules.sql.SQLRealTimeModule',
            'devserver.modules.sql.SQLSummaryModule',

            'devserver.modules.profile.ProfileSummaryModule',
            'devserver.modules.ajax.AjaxDumpModule',
            'devserver.modules.profile.MemoryUseModule',
            'devserver.modules.cache.CacheSummaryModule',
            'devserver.modules.profile.LineProfilerModule',
        ],

        # django-debug-toolbar stuff
        DEBUG_TOOLBAR_PANELS=(
            'debug_toolbar.panels.version.VersionDebugPanel',
            'debug_toolbar.panels.timer.TimerDebugPanel',
            'debug_toolbar.panels.headers.HeaderDebugPanel',
            'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
            'debug_toolbar.panels.template.TemplateDebugPanel',
            'debug_toolbar.panels.sql.SQLDebugPanel',
            'debug_toolbar.panels.signals.SignalDebugPanel',
            'django_statsd.panel.StatsdPanel',
            # these two panels are there in doc, but dont actually exist in code
            # 'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
            # 'debug_toolbar.panels.logger.LoggingPanel',
        ),
        STATSD_PATCHES = [
            'django_statsd.patches.db',
            # 'django_statsd.patches.cache',
        ],
    )
)

from hrms.views import people_detail

d.add_view("/<username:username>/", people_detail, name="user_detail_by_username")

if __name__ == "__main__":
    d.main()
