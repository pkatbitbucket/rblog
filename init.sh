#!/bin/bash


#grunt custom:-ajax/script,-ajax/jsonp,-css,-deprecated,-dimensions,-effects,-event,-offset,-wrap,-core/ready,-exports/global,-sizzle
sudo locale-gen en_IN
sudo bash -c "echo 'Asia/Kolkata' > /etc/timezone"
sudo dpkg-reconfigure -f noninteractive tzdata
sudo reboot

#Attaching volumes
sudo fdisk -l

sudo mkfs -t ext4 /dev/xvdc
sudo mkfs -t ext4 /dev/xvdd

sudo mkdir /ebs
sudo mkdir /ssd

sudo mount /dev/xvdc /ssd
sudo mount /dev/xvdd /ebs

##### SYSTEM ######
export CURRENT_DIR=`pwd`

sudo apt-get update;
sudo apt-get upgrade;
sudo apt-get install mysql-client mysql-server libmysqlclient-dev nginx libevent-dev libpq-dev libncurses5-dev memcached redis-server
sudo apt-get install zsh git-core libzmq1 libzmq-dev make automake bison build-essential python-dev python-mysqldb python-virtualenv openssl libssl-dev libevent-dev libxml2-dev libxslt1-dev python-dev

sudo apt-get install libjpeg8 libjpeg62-dev libfreetype6 libfreetype6-dev

##### Mysql Setup ####
#change config file as per project
# make changes to app armor so that mysql has permission to read write on
# non standard directories, make sure that it takes proper log permissions
# as log file, as data file and binary log file
sudo vim /etc/apparmor.d/usr.sbin.mysqld
sudo invoke-rc.d apparmor restart
#sudo chown mysql:mysql -R /data/mysql => change perms for all data and log files
sudo mysql_install_db  #install base tables and logs to start with
#restart and hope it works

#We should fix PIL path libraries by: x86_64-linux-gnu OR i386-linux-gnu
sudo ln -s /usr/lib/x86_64-linux-gnu/libjpeg.so /usr/lib
sudo ln -s /usr/lib/x86_64-linux-gnu/libfreetype.so /usr/lib
sudo ln -s /usr/lib/x86_64-linux-gnu/libz.so /usr/lib

#If you want to install webserver with nginx
#sed "s#<static_content_path>#$CURRENT_DIR#g" conf/template_nginx.conf > conf/ignore_nginx.conf
#sudo ln -s -t /etc/nginx/sites-enabled/ $CURRENT_DIR/conf/ignore_static_nginx.conf
#sudo /etc/init.d/nginx restart

#If you want to install system wide oh-my-zsh
#curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh
#chsh -s /bin/zsh
#If this doesn't work, then check $which zsh = /usr/bin/zsh has an entry in /etc/shells
#if yes do sudo -s and as root run chsh -s /usr/bin/zsh <username>

####### Crontab ######
# With normal user crontab:
@reboot /home/coverfox/projects/rblog/envproj/bin/supervisord -c /home/coverfox/projects/rblog/confs/supervisord.conf
# With root user crontab:
@reboot /usr/bin/redis-server /home/coverfox/projects/rblog/confs/redis.conf

##################

virtualenv envproj
source envproj/bin/activate

export PREFIX=$CURRENT_DIR/envproj/

pip install -r requirements.txt

cd $CURRENT_DIR
mkdir -p logs/project logs/supervisord logs/transaction_logs logs/lms_requests
touch /Users/arpit/Documents/workplace/glitterbug/rblog/logs/django.log
cd $CURRENT_DIR

wget http://nodejs.org/dist/v0.10.26/node-v0.10.26.tar.gz
tar -xvzf node-v0.10.26.tar.gz
cd node-v0.10.26
./configure
make
sudo make install
sudo npm install -g grunt-cli

#From inside the app where you want to put node_modules
sudo npm install grunt --save-dev
sudo npm install grunt-contrib-concat --save-dev
sudo npm install grunt-contrib-copy --save-dev
sudo npm install grunt-contrib-cssmin --save-dev
sudo npm install grunt-contrib-uglify --save-dev
sudo npm install requirejs --save-dev
sudo npm -g install yuglify
