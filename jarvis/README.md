# README #

# Install dependencies and virtual environment #
sudo apt-get update;
sudo apt-get upgrade;
sudo apt-get install libpq-dev nginx libevent-dev libpq-dev
sudo apt-get install postgresql postgresql-contrib
sudo apt-get install zsh libzmq1 libzmq-dev make automake bison build-essential python-dev python-mysqldb python-virtualenv openssl libssl-dev libevent-dev
sudo apt-get install graphviz libgraphviz-dev pkg-config

# To be able to use potgres Hstorefield
sudo apt-get install postgis postgresql-9.3-postgis-scripts

virtualenv envproj
source envproj/bin/activate

pip install -r requirements.txt

npm install
webpack --config webpack.config.js --watch --display-error-details


- Basic rules and philosophy
    - Where ever you need to serialize a model, use djson (inherit from Djson, use djson_exclude and override djson)
    - Use mini_json to store mini-serialized version of instance on redis
    - Do not use json.dumps or json.loads, postgres Jsonfield should handle this automatically
    - Use __unicode__ everywhere instead of __str__

    - Always Import MODULE not a function or class following below mentioned rules(examples):
        - WRONG - from core.models import * OR from core.models import User, Person
        - RIGHT - from core import models as core_models

        - WRONG - from cfutils.forms.fhurl import fhurl, RequestForm
        - RIGHT - from cfutils.forms import fhurl(and use as fhurl.fhurl or fhurl.RequestForm)


- TODOS
    - Document create_ticket api (required params)
    - For all configs which definitely need some base fields, for e.g. ticket priority needs "Normal", create a method "get_normal_priority"

    - Discuss this properly, Before push, check if objects assigned_to to has changed, if yes, remove and push again, if not, only update
    - Use redis pipeline with transaction=True, when updating, removing queues in redis in a single go
    - Maintain ticket_to_user and user_to_ticket mapping in redis, node and django, read/write from redis.
    - Create a Django app, which listens to node (through redis) and sets mini_json and publishes is back to redis on a channel given by redis, after redis has data from the channel, it unsubscribes from the channel

    - Create Ameyo as a component

    - All component registrations to be done after the require statements
    - Where ever we are using fhurl, please use fhurl form which responds to get, post requests with json
    - Make ticket-detail.html template into template. Make a component called ticket-action and one which only only displays ticket-detail
    - For dashboard, get the dashboard settings from backend from a settings model

