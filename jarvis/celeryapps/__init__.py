from __future__ import absolute_import

# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from .auto_task_assignment import app as auto_task_assignment  # flake8: noqa
from .report_generator import app as report_generator  # flake8: noqa
