from __future__ import absolute_import
import os

from celery import Celery
from celery.task.schedules import crontab
from celery.decorators import periodic_task

from django.db.models import Q

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
import django
django.setup()
from django.conf import settings

from alfred import models as alfred_models

app = Celery('auto_task_assignment')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@periodic_task(run_every=crontab(hour="*", minute="*/10", day_of_week="*"))
def set_tasks():
    tickets_without_tasks = alfred_models.Ticket.objects.filter(
        ~Q(tasks__disposed_by_activities__isnull=True),
        ~Q(current_state__name__in=['Close resolved', 'Close unresolved'])
    )
    for ot in tickets_without_tasks:
        ot.add_task()
