from __future__ import absolute_import
import os

from celery import Celery, task


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
from django.conf import settings
from coverfox.mail import models as mail_models
from core import models as core_models

app = Celery('mail_celery')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@task
def gmail_watch(user_id):
    from coverfox.mail.backends import gmail
    gmail.gmail_watch(user_id)


@task
def prepare_mailbox(user_id, cred_id):
    from coverfox.mail.backends import gmail
    mod = gmail.Gmail(core_models.User.objects.get(
        id=user_id), mail_models.Credential.objects.get(id=cred_id).initialize())
    mod.prepare_mailbox()
