from __future__ import absolute_import
import os

from celery import Celery
from celery.task.schedules import crontab
from celery.decorators import periodic_task

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
from django.conf import settings
from django.core.mail import send_mail
from alfred import models as alfred_models
from alfred import report_utils as alfred_reports

app = Celery('report_generator')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@periodic_task(run_every=crontab(hour="*/24", minute="0", day_of_week="*"))
def send_ticket_report_mail():

    ticket_query_set = alfred_models.Ticket.objects.all()

    report_url = settings.SITE_URL + \
        alfred_reports.export_list_to_csv(
            alfred_reports.generate_report(ticket_query_set))

    email_subject = settings.EMAIL_SUBJECT_PREFIX + "Tickets Report"

    email_body = """Below is the link for Ticket Reports:\n
        %s
    """ % (report_url)

    send_mail(email_subject, email_body, settings.DEFAULT_FROM_EMAIL, [
        'gulshan@coverfoxmail.com',
        'aman@coverfoxmail.com',
        'akhil@coverfox.com',
        'shweta@coverfox.com',
        'dhinakaran@coverfoxmail.com'])
