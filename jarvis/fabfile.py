import os

from fabric.api import run, local, cd, task, env, prefix
from fabric.operations import prompt
from contextlib import contextmanager as _contextmanager

BITBUCKET_REPO = "git@bitbucket.org:glitterbug/rblog.git"
DEPLOYMENT_CONFIG = {
    'prod': {
        'PRODUCTION_DIR': '/home/www/rblog/',
        'env': {
            'user': 'ubuntu',
            'host_string': 'alfred.coverfox.com',
            'forward_agent': True,
        },
    },
    'dev': {
        'PRODUCTION_DIR': '/home/www/alfred-dev.coverfox.com/',
        'env': {
            'user': 'ubuntu',
            'host_string': 'alfred-dev.coverfox.com',
            'forward_agent': True,
        },
    },
}


def _set_environment(env_config):
    env.user = env_config.get('user')
    env.host_string = env_config.get('host_string')
    env.forward_agent = env_config.get('forward_agent')


@_contextmanager
def virtualenv(venv):
    cmd = "source %s" % os.path.join(venv, "bin/activate")
    with prefix(cmd):
        yield


def pip_install(package):
    local("sudo pip install %s" % package)


def _deploy(config, branch="master"):
    deploy_dir = config.get('PRODUCTION_DIR')
    try:
        from pyfiglet import Figlet
    except ImportError:
        print "Please install pyfiglet: pip install pyfiglet"
    else:
        local("clear")
        f = Figlet(font='slant')
        print f.renderText("HOLD YOUR BREATH")
        with cd(deploy_dir):
            run("git pull origin %s" % branch)
        setup(deploy_dir)
        print f.renderText("DID SHIT HIT THE ROOF?")


def setup(deploy_dir):
    venv = os.path.join(deploy_dir, "envproj")
    with cd(os.path.join(deploy_dir)):
        with virtualenv(venv):
            run("pip install -r requirements.txt")

    with cd(os.path.join(deploy_dir, "jarvis")):
        with virtualenv(venv):
            run("python manage.py migrate")

    # run webpack
    print "\t>>>> RUNNING WEBPACK <<<<<<<"
    with cd(os.path.join(deploy_dir, "jarvis")):
        run("npm install")
        run("webpack -p --progress")

    # restart services
    print "\t>>>> RESTARTING SERVICES <<<<<<<"
    with cd(os.path.join(deploy_dir, "jarvis")):
        with virtualenv(venv):
            run("supervisorctl -c confs/supervisord.conf restart all")


def _deployment_prompts(deploy_location):
    response = prompt("""
        THIS WILL DEPLOY TO ALFRED %s.
        ARE YOU SURE??? SAY YES IN CAPS ===>
    """ % deploy_location)

    return response


@task
def deploy_production():
    config = DEPLOYMENT_CONFIG.get('prod')
    _set_environment(config.get('env'))
    response = _deployment_prompts('PRODUCTION')
    if response == "YES":
        _deploy(config)


@task
def deploy_dev():
    config = DEPLOYMENT_CONFIG.get('dev')
    _set_environment(config.get('env'))
    response = _deployment_prompts('DEV')
    if response == "YES":
        _deploy(config)
