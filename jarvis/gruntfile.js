module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            development: {
                options: {
                    paths: ["css"]
                },
                files: {
                    "base/core/static/core/material/css/bootstrap-app.css": "base/core/static/core/material/less/app.less",
                },
                cleancss: true
            },
            /*production: {
                options: {
                    paths: ["css"],
                    cleancss: true,
                },
                files: {
                    "css/app.min.css": "less/app.less",
                }
            },*/
        },
        /*csssplit: {
            your_target: {
                src: ['css/bootstrap-app.css'],
                dest: 'css/app.min.css',
                options: {
                    maxSelectors: 4095,
                    suffix: '.'
                }
            },
        },*/
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'base/core/static/core/material/css/bootstrap-app.min.css': ['base/core/static/core/material/css/bootstrap-app.css']
                }
            }
        },
        watch: {
            styles: {
                files: ['less/**/*.less'], // which files to watch
                tasks: ['less'],
                options: {
                    nospawn: true
                }
            }
        }
    });

    // Load the plugin that provides the "less" task.
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-csssplit');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    // Default task(s).
    grunt.registerTask('default', ['less', 'cssmin']);

};
