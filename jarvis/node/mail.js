var request = require('request');
var qs = require('querystring');
var utils = require('./utils');
var s = require('./services');

var MAIL_SERVICE_URL = 'http://localhost:8000';
var ALLOW_API_LOGIN_KEY = '9988bcb5543739b7734d277f8f8fbdfc';

var MailModel = function() {
    var self = this;
    self.prefix = "mail";

    self.on_push = function(STATE, channel, cdata) {
        sockets = STATE.channel_to_socket.getElementsForEntity(channel);
        sockets.forEach(function(socket, index){
            s.db.get('mail_' + cdata.data, function(err, reply){
                s.logger.info(channel, cdata.command);
                socket.emit('update', {'channel': channel, 'msg': {'command': cdata.command, 'data': JSON.parse(reply)}});
            });
        });
    };

    self.on_subscription = function(mail_id, channel, socket) {
        s.db.get(mail_id, function(err, reply){
            data = JSON.parse(reply);
            s.logger.info("Redis auto mail push on", channel, " message:", data.id);
            socket.emit('update', {'channel': channel, 'msg' : {'command': 'mail_push', 'data': data}});
        });
    };

    self.on_disconnection = function(socket) {
    };

    self.on_message = function(STATE, socket, cdata) {
        switch(cdata.command) {
            case "get-mail":
                //TODO: pass userid too, check if mail belongs to user in the view
                console.log("Getting mail for", cdata.id);
                var get_url = MAIL_SERVICE_URL + "/mail/mails/?" + qs.stringify({
                    id: cdata.id,
                    key: ALLOW_API_LOGIN_KEY
                });
                request(get_url, function (error, response, body) {
                    var data = JSON.parse(body);
                    s.logger.info("Call for mail on url: ", get_url);
                    if (!error && response.statusCode == 200) {
                        socket.emit('update', {
                            'channel': "user_" + STATE.socket_to_user[socket.id],
                            'msg': {'command': "mail_push", 'data': data}
                        });
                    } else {
                        s.logger.info("Error while retriving url from mail server: ", get_url);
                    }
            });
            break;
        }
    };
};

var Mail = new MailModel();
module.exports = Mail;
