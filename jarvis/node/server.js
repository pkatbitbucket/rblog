var s = require('./services');
var utils = require('./utils');
var Ticket = require("./ticket");
var Mail = require("./mail");

/**************************************************************
channel             command         app_method      data
user_<user_id>      ticket_push     on_push         <ticket_id>

user_<user_id>      mail_push       on_push         <mail_id>
***************************************************************/

// Add apps here
s.apps.push(Ticket);
s.apps.push(Mail);

var STATE = {
    socket_to_user: {},
    user_to_socket: {},
    channel_to_socket: new utils.EntityElementMap(),
    socket_to_channel: new utils.EntityElementMap(),
};

/**** websocket-redis code ****/
s.subscriber.on("message", function(channel, message) {
    console.log("MESSAGE", channel, message);
    s.logger.info("Redis push on", channel, " message:", message);
    var cdata = JSON.parse(message);

    var sockets = [];

    app_found = false;

    var commandArgs = cdata.command.split('_');
    var appName = commandArgs[0];
    var command = commandArgs[1];
    for(var i=0; i<s.apps.length; i++) {
        app = s.apps[i];
        if(appName == app.prefix){
            app_found = true;
            app['on_'+command](STATE, channel, cdata);
            break;
        }
    }

    if(!app_found){
        sockets = STATE.channel_to_socket.getElementsForEntity(channel);
        sockets.forEach(function(socket, index){
            socket.emit('update', {'channel': channel, 'msg' : {'command': cdata.command, 'data': JSON.parse(cdata.data)}});
        });
    }

});
/**** websocket-redis code ****/

/**** websocket code ****/

function subscribe_to_channel(socket, channel) {
    STATE.channel_to_socket.addElementToEntity(channel, socket);
    STATE.socket_to_channel.addElementToEntity(socket.id, channel);
    s.subscriber.subscribe(channel);
    s.logger.info("subscribed to channel", channel);
}

s.io.sockets.on('connection', function(socket) {

    // subscribing to channels
    socket.on('subscribe_to_channels', function(msg) {
        var userId = msg.userId.toString();
        var socketId = socket.id.toString();
        STATE.socket_to_user[socketId] = userId;
        STATE.user_to_socket[userId] = socket;

        var channels = msg.channels;
        channels.forEach(function(channel, index) {
            subscribe_to_channel(socket, channel);
        });

        channels.forEach(function(channel, index) {
            s.db.zrange(channel, 0, -1, function(err, objects){
                objects.forEach(function(obj, index){
                    for(var i=0; i<s.apps.length; i++) {
                        app = s.apps[i];
                        if(obj.startsWith(app.prefix)){
                            app.on_subscription(obj, channel, socket);
                        }
                    }
                });
            });
        });
    });

    //Message from front-end, unhandled
    socket.on('message', function(cdata) {
        s.logger.info("Message ", cdata);
        console.log("Message", cdata);
        for(var i=0; i<s.apps.length; i++) {
            app = s.apps[i];
            if(cdata.app.startsWith(app.prefix)) {
                app.on_message(STATE, socket, cdata);
            }
        }
    });

    //Disconnection
    socket.on('disconnect', function() {
        var socketId = this.id;
        var userId = STATE.socket_to_user[socketId];

        var channels = STATE.socket_to_channel.getElementsForEntity(socketId);
        channels.forEach(function(channel, index){
            STATE.channel_to_socket.removeElementFromEntity(channel, socket);
            if(STATE.channel_to_socket.isEmptyElementList(channel)){
                s.subscriber.unsubscribe(channel);
                s.logger.info('Unsubscribed from channel ', channel);
            }
        });

        delete(STATE.socket_to_user[socketId]);
        delete(STATE.user_to_socket[userId]);
    });

    //Error handling unhandled
    socket.on('error', function (err) {
        s.logger.info(err.stack);
    });
});

/**** websocket code ****/
