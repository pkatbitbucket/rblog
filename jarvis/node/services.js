var winston = require('winston');
var http = require("http").createServer();
const redis = require('redis');
var utils = require('./utils');

const APP_HOST = "0.0.0.0";
const APP_PORT = 8081;

const REDIS_HOST = 'localhost';
const REDIS_PORT = 6379;

winston.add(winston.transports.File, {
    filename: 'logs/nodejs.log',
    timestamp: true,
    json: false,
    formatter: utils.formatter
});

http.listen(APP_PORT, APP_HOST);
var io = require('socket.io').listen(http);
winston.info("Server listening on host %s:%s", APP_HOST, APP_PORT)

const rclient = redis.createClient(REDIS_PORT, REDIS_HOST);
const subscriber = redis.createClient(REDIS_PORT, REDIS_HOST);

var apps = [];

module.exports = {
    'logger': winston,
    'db': rclient,
    'subscriber': subscriber,
    'io': io,
    'apps': apps,
};
