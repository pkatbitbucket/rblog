var utils = require('./utils');
var s = require('./services');

var TicketModel = function() {
    var self = this;
    self.prefix = "ticket";

    //TODO: Implement onError
    self.on_push = function(STATE, channel, cdata) {
        sockets = STATE.channel_to_socket.getElementsForEntity(channel);
        s.db.get('ticket_' + cdata.data, function(err, reply){
            sockets.forEach(function(socket, index){
                socket.emit('update', {'channel': channel, 'msg': {'command': cdata.command, 'data': JSON.parse(reply)}});
            });
        });
    }

    self.on_remove = function(STATE, channel, cdata) {
        sockets = STATE.channel_to_socket.getElementsForEntity(channel)
        sockets.forEach(function(socket, index){
            socket.emit('update', {'channel': channel, 'msg': {'command': cdata.command, 'data': {'id': cdata.data}}});
        });
    }

    self.on_subscription = function(ticket_id, channel, socket) {
        tid = ticket_id.split('_')[1]
        s.db.get(ticket_id, function(err, reply){
            s.logger.info("Redis auto ticket push on", channel, " message:", reply);
            socket.emit('update', {'channel': channel, 'msg' : {'command': 'ticket_push', 'data': JSON.parse(reply)}})
        });
    }

    self.on_message = function(STATE, socket, cdata) {
        /*switch(cdata.command) {
            case "get-ticket":
                console.log("Getting ticket for", cdata.id);
            break;
        }*/
    }
}

var Ticket = new TicketModel();
module.exports = Ticket;
