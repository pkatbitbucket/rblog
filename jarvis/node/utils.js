if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
    };
}
if (!String.prototype.endsWith) {
  String.prototype.endsWith = function(searchString, position) {
      var subjectString = this.toString();
      if (position === undefined || position > subjectString.length) {
        position = subjectString.length;
      }
      position -= searchString.length;
      var lastIndex = subjectString.indexOf(searchString, position);
      return lastIndex !== -1 && lastIndex === position;
  };
}

var defaultDict = function(elem) {
    this.get = function (key) {
        if (this.hasOwnProperty(key)) {
            return this[key];
        } else {
            return elem();
        }
    }
}

var EntityElementMap = function(){
    var self = this;
    self.map = new defaultDict(Array);
    self.getElementsForEntity = function(entity){
        return self.map.get(entity);
    };

    self.addElementToEntity= function(entity, element){
        var elements = self.map.get(entity);
        if (elements.indexOf(element) == -1){
            elements.push(element);
        }
        self.map[entity] = elements;
    };
    self.removeElementFromEntity = function(entity, element){
        var elements = self.map.get(entity);
        var index = elements.indexOf(element);
        if (index > -1){
            elements.splice(index, 1);
        }
        self.map[entity] = elements;
    };
    self.isEmptyElementList = function(entity){
        var l = self.getElementsForEntity(entity);
        if(l.length == 0){
            return true;
        }
        else{
            return false;
        }
    };
}

function formatter(args) {
    var dateTimeComponents = new Date()
    dateTimeComponents = dateTimeComponents.toLocaleDateString() + ' ' + dateTimeComponents.toLocaleTimeString();
    var logMessage = dateTimeComponents + ' - ' + args.level + ': ' + args.message;
    return logMessage;
}

module.exports = {
    'EntityElementMap': EntityElementMap,
    'formatter': formatter,
}
