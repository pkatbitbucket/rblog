import django
import os
import datetime
import string
import random
import urllib
from django.conf import settings

import leads.models as leads_models
import core.models as core_models
import alfred.models as alfred_models
from cfutils.aws import s3_upload

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
django.setup()

motor_csv = open('/tmp/health_transactions_2015-10-30-161542_modified.csv')

motor_headings = ['type', 'id', 'first_name', 'middle_name', 'last_name', 'email', 'mobile', 'address_line1',
                  'address_line2', 'address_landmark', 'city', 'state', 'pincode', 'policy_number', 'policy_status',
                  'policy_start_date', 'policy_end_date', 'policy_document', 'premium_paid', 'transaction_id', 'insurer_title',
                  'insurer_slug', 'policy_name', 'policy_covers', 'vehicle_model', 'vehicle_make', 'vehicle_variant',
                  'vehicle_fuel_type', 'vehicle_rto', 'vehicle_registration_number', 'vehicle_registation_date',
                  'vehicle_engine_number', 'payment_on', 'transaction_created', 'transaction_updated']
health_headings = ['type', 'id', 'first_name', 'middle_name', 'last_name', 'email', 'mobile', 'address_line1',
                   'address_line2', 'address_landmark', 'city', 'state', 'pincode', 'policy_number', 'policy_status',
                   'policy_start_date', 'policy_end_date', 'policy_document', 'premium_paid', 'transaction_id', 'insurer_title',
                   'insurer_slug', 'policy_name', 'policy_covers', 'payment_on', 'transaction_created', 'transaction_updated']

for line in motor_csv:
    line = line.split('$')
    if line[14].upper() in ['COMPLETED', 'MANUAL COMPLETED']:
        data = {}
        for index, item in enumerate(line):
            data[motor_headings[index]] = item

        email = core_models.Email.objects.filter(address__iexact=data['email'])
        phone = core_models.Phone.objects.filter(number__iexact=data['mobile'])
        if email:
            email = email.latest('id')
            person = email.person
            if phone:
                phone = phone.latest('id')
                phone.person = person
                phone.save(None)
            else:
                phone = core_models.Phone(number=data['mobile'])
        elif phone:
            phone = phone.latest('id')
            person = phone.person
            email = core_models.Email(address=data['email'])
        else:
            email = core_models.Email(address=data['email'])
            phone = core_models.Phone(number=data['mobile'])
            person = core_models.Person()
        email.is_primary = True
        email.is_verified = True
        phone.is_primary = True
        phone.is_verified = True

        person.first_name = data['first_name']
        person.middle_name = data['middle_name']
        person.last_name = data['last_name']
        person.save(None)
        email.person = person
        phone.person = person
        email.save(None)
        phone.save(None)
        address_keys = ['address_line1', 'address_line2',
                        'address_landmark', 'city', 'state', 'pincode']
        address = ""
        for k in address_keys:
            address += data[k]
        address = core_models.Address(
            address=address,
            person=person,
            category=core_models.Address.CORRESPONDENCE
        )
        address.save(None)

        tdata = data.copy()
        del tdata['policy_number']
        del tdata['policy_status']
        del tdata['policy_start_date']
        del tdata['policy_end_date']
        del tdata['policy_document']
        if data['policy_number']:
            policy = alfred_models.Policy.objects.filter(
                policy_number__endswith=data['policy_number'])
        else:
            policy = None
        if policy:
            policy = policy[0]
            policy.data = policy.data or {}
        else:
            policy = alfred_models.Policy()
            policy.data = {}
        policy.data.update(data)
        policy.policy_number = data['policy_number']
        policy.status = alfred_models.Policy.ISSUED
        if data['policy_start_date'] and not data['policy_start_date'] == 'None':
            policy_start_date = datetime.datetime.strptime(
                data['policy_start_date'][:19], "%Y-%m-%d %H:%M:%S")
        else:
            policy_start_date = None
        if data['policy_end_date'] and not data['policy_end_date'] == 'None':
            policy_end_date = datetime.datetime.strptime(
                data['policy_end_date'][:19], "%Y-%m-%d %H:%M:%S")
        else:
            policy_end_date = None
        policy.policy_start_date = policy_start_date
        policy.policy_end_date = policy_end_date
        if data['premium_paid']:
            policy.premium_paid = float(data['premium_paid'])
        policy.save()
        policy.contacts.add(person)

        choices = string.ascii_lowercase + string.digits
        rand_string = ''.join(random.choice(choices) for i in range(16))
        doc = alfred_models.Document(
            category=alfred_models.DocumentCategory.objects.get(
                name='Policy Copy')
        )
        file_name_to_save_to = "policy_" + \
            str(policy.id) + data['transaction_id'] + "_" + rand_string
        doc.key = file_name_to_save_to
        doc.policy = policy
        doc.save()

        if not settings.DEBUG:
            if data['policy_document']:
                urllib.urlretrieve(
                    data['policy_document'], "/tmp/" + file_name_to_save_to)
            content = open("/tmp/" + file_name_to_save_to)
            s3uploader = s3_upload.S3Upload(settings.REQUEST)
            key_url = s3uploader.upload(file_name_to_save_to, content)

        tr = leads_models.Transaction.objects.filter(
            transaction_id=data['transaction_id'])
        if tr:
            tr = tr[0]
            tr.extra = tr.extra or {}
        else:
            tr = leads_models.Transaction(
                transaction_id=data['transaction_id'], extra={})
        tr.is_successful = True
        tr.payment_date = None
        if data['premium_paid']:
            tr.amount_paid = data['premium_paid']
        tr.extra.update(tdata)
        tr.save(None)
        tr.policies.add(policy)

motor_csv.close()
