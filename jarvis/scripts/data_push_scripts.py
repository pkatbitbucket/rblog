"""
This file will eventually be removed.
Currently used to push initial alfred data to the db.
For any questions, ask Rathi-9920130681.
"""

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()

from statemachine import models as state_models
from core import models as core_models

##########################################################################
# Callbacks
callbacks = [
    {'name': 'Send ticket opening email', 'module': 'send_ticket_opening_mail'},
    {'name': 'Update ticket with details accordingly',
        'module': 'update_ticket_with_details'},
    {'name': 'Notify customer on state update',
        'module': 'notify_customer_on_state_update'},
    {'name': 'Send no conversation notification',
        'module': 'send_no_conversation_notification'},
]
for cb_args in callbacks:
    callback, created = state_models.Callback.objects.get_or_create(**cb_args)
print "Callbacks created"
##########################################################################

##########################################################################
# States
states_args = [
    {'name': '*'},
    {'name': 'Close resolved', 'on_enter': [
        'notify_customer_on_state_update']},
    {'name': 'Close unresolved'},
    {'name': 'New', 'on_enter': ['notify_customer_on_state_update']},
    {'name': 'Insurer intimated'},
    {'name': 'Documents pending', 'on_enter': [
        'notify_customer_on_state_update']},
    {'name': 'Documents received'},
    {'name': 'Documents uploaded'},
    {'name': 'Insurer decision pending'},
    {'name': 'Request accepted'},
    {'name': 'Request rejected'},
    {'name': 'Appointment scheduled'},
    {'name': 'Appointment accepted'},
    {'name': 'Payment pending'},
    {'name': 'Policy pending'},
    {'name': 'QC pending'},
    {'name': 'Policy approved'},
    {'name': 'Policy uploaded'},
    {'name': 'Policy dispatch pending'},
    {'name': 'Policy dispatched', 'on_enter': [
        'notify_customer_on_state_update']},
    {'name': 'Health card pending'},
    {'name': 'Health card received'},
    {'name': 'Health card uploaded'},
]

states = []
for state_args in states_args:
    state = state_models.State.objects.create_from_args(**state_args)
    states.append(state)
print "States created"
claim_flow_args = [
    {'name': '*'},
    {'name': 'Close resolved'},
    {'name': 'Close unresolved'},
    {'name': 'New'},
    {'name': 'Insurer intimated'},
    {'name': 'Documents pending'},
    {'name': 'Documents received'},
    {'name': 'Documents uploaded'},
    {'name': 'Insurer decision pending'},
    {'name': 'Request accepted'},
    {'name': 'Request rejected'},
]
cancel_flow_args = [
    {'name': '*'},
    {'name': 'Close resolved'},
    {'name': 'Close unresolved'},
    {'name': 'New'},
    {'name': 'Documents pending'},
    {'name': 'Documents received'},
    {'name': 'Documents uploaded'},
    {'name': 'Insurer decision pending'},
    {'name': 'Request accepted'},
    {'name': 'Request rejected'},
]
refund_flow_args = [
    {'name': '*'},
    {'name': 'Close resolved'},
    {'name': 'Close unresolved'},
    {'name': 'New'},
    {'name': 'Insurer decision pending'},
    {'name': 'Request accepted'},
]
endorsement_flow_args = [
    {'name': '*'},
    {'name': 'Close resolved'},
    {'name': 'Close unresolved'},
    {'name': 'New'},
    {'name': 'Documents pending'},
    {'name': 'Documents received'},
    {'name': 'Documents uploaded'},
    {'name': 'Insurer decision pending'},
    {'name': 'Request accepted'},
    {'name': 'Request rejected'},
]
offline_flow_args = [
    {'name': '*'},
    {'name': 'Close resolved'},
    {'name': 'Close unresolved'},
    {'name': 'New'},
    {'name': 'Documents pending'},
    {'name': 'Documents received'},
    {'name': 'Documents uploaded'},
    {'name': 'Insurer decision pending'},
    {'name': 'Request rejected'},
    {'name': 'Appointment scheduled'},
    {'name': 'Appointment accepted'},
    {'name': 'Payment pending'},
]
approved_flow_args = [
    {'name': '*'},
    {'name': 'Close resolved'},
    {'name': 'Close unresolved'},
    {'name': 'New'},
    {'name': 'Policy pending'},
    {'name': 'QC pending'},
    {'name': 'Policy approved'},
    {'name': 'Policy uploaded'},
    {'name': 'Policy dispatch pending'},
    {'name': 'Policy dispatched'},
]
approved_health_flow_args = [
    {'name': '*'},
    {'name': 'Close resolved'},
    {'name': 'Close unresolved'},
    {'name': 'New'},
    {'name': 'Policy pending'},
    {'name': 'QC pending'},
    {'name': 'Policy approved'},
    {'name': 'Policy uploaded'},
    {'name': 'Policy dispatch pending'},
    {'name': 'Policy dispatched'},
    {'name': 'Health card pending'},
    {'name': 'Health card received'},
    {'name': 'Health card uploaded'},
]
fresh_flow_args = [
    {'name': '*'},
    {'name': 'Close resolved'},
    {'name': 'Close unresolved'},
    {'name': 'New'},
]
inquiry_flow_args = [
    {'name': '*'},
    {'name': 'Close resolved'},
    {'name': 'Close unresolved'},
    {'name': 'New'},
]

states_for_claim = [state_models.State.objects.get(
    name=elem['name']) for elem in claim_flow_args]
states_for_cancel = [state_models.State.objects.get(
    name=elem['name']) for elem in cancel_flow_args]
states_for_refund = [state_models.State.objects.get(
    name=elem['name']) for elem in refund_flow_args]
states_for_endorsement = [state_models.State.objects.get(
    name=elem['name']) for elem in endorsement_flow_args]
states_for_offline = [state_models.State.objects.get(
    name=elem['name']) for elem in offline_flow_args]
states_for_approved = [state_models.State.objects.get(
    name=elem['name']) for elem in approved_flow_args]
states_for_approved_health = [state_models.State.objects.get(
    name=elem['name']) for elem in approved_health_flow_args]
states_for_fresh = [state_models.State.objects.get(
    name=elem['name']) for elem in fresh_flow_args]
states_for_inquiry = [state_models.State.objects.get(
    name=elem['name']) for elem in inquiry_flow_args]
##########################################################################

##########################################################################
# Triggers
triggers = [
    'notified_insurer',
    'notification_from_insurer',
    'notified_customer',
    'notification_from_customer',
    'documents_uploaded',
    'close_unsuccessful',
    'no_conversation',
    'qc_completed',
    'qc_completed',
    'uploaded_to_account',
]
for t_name in triggers:
    trig, created = state_models.Trigger.objects.get_or_create(name=t_name)
print "Triggers created"
##########################################################################

##########################################################################
# Conditions
conditions = [
    {'name': 'Is all documents received from customer?',
        'module': 'is_all_documents_received', },
    {'name': 'Is request accepted in notification from insurer?',
        'module': 'is_request_accepted', },
    {'name': 'Is request rejected in notification from insurer?',
        'module': 'is_request_rejected', },
    {'name': 'Is additional requirement in notification from insurer?',
        'module': 'is_additional_requirement', },
    {'name': 'Is appointment scheduled by the insurer?',
        'module': 'is_appointment_scheduled', },
    {'name': 'Is appointment accepted by the customer?',
        'module': 'is_appointment_accepted', },
    {'name': 'Is appointment rejected by the customer?',
        'module': 'is_appointment_rejected', },
    {'name': 'Is appointment successfully completed by the customer?',
        'module': 'is_appointment_successful', },
    {'name': 'Is payment pending by the customer?',
        'module': 'is_payment_pending', },
    {'name': 'Is payment done by the customer?',
        'module': 'is_payment_done', },
    {'name': 'Is passed quality check?',
        'module': 'is_passed_quality_check', },
    {'name': 'Is failed quality check?',
        'module': 'is_failed_quality_check', },
    {'name': 'Is policy dispatched?',
        'module': 'is_policy_dispatched', },
    {'name': 'Is backend team activity',
        'module': 'is_backend_team_activity', },
    {'name': 'Is frontend team activity',
        'module': 'is_frontend_team_activity', },
]
for c_dict in conditions:
    condition, created = state_models.Condition.objects.get_or_create(**c_dict)
print "Conditions created"
##########################################################################

##########################################################################
# Transitions
transition_args = [
    {
        'trigger': 'close_unsuccessful',
        'source': '*',
        'destination': 'Close unresolved',
    },
    {
        'trigger': 'notified_insurer',
        'source': 'New',
        'destination': 'Insurer intimated',
        'conditions': ['is_backend_team_activity'],
    },
    {
        'trigger': 'notification_from_insurer',
        'source': 'New',
        'destination': 'Insurer intimated',
        'conditions': ['is_backend_team_activity'],
    },
    {
        'trigger': 'notified_customer',
        'source': 'Insurer intimated',
        'destination': 'Documents pending',
        'conditions': ['is_frontend_team_activity'],
    },
    {
        'trigger': 'notified_customer',
        'source': 'Documents pending',
        'destination': 'Documents received',
        'conditions': ['is_all_documents_received', 'is_frontend_team_activity'],
    },
    {
        'trigger': 'notification_from_customer',
        'source': 'Documents pending',
        'destination': 'Documents received',
        'conditions': ['is_all_documents_received', 'is_frontend_team_activity'],
    },
    {
        'trigger': 'documents_uploaded',
        'source': 'Documents received',
        'destination': 'Documents uploaded',
        'conditions': ['is_backend_team_activity'],
    },
    {
        'trigger': 'notified_insurer',
        'source': 'Documents uploaded',
        'destination': 'Insurer decision pending',
        'conditions': ['is_backend_team_activity'],
    },
    {
        'trigger': 'notification_from_insurer',
        'source': 'Insurer decision pending',
        'destination': 'Request accepted',
        'conditions': ['is_request_accepted', 'is_backend_team_activity'],
    },
    {
        'trigger': 'notification_from_insurer',
        'source': 'Insurer decision pending',
        'destination': 'Request rejected',
        'conditions': ['is_request_rejected', 'is_backend_team_activity'],
    },
    {
        'trigger': 'notification_from_insurer',
        'source': 'Insurer decision pending',
        'destination': 'Documents pending',
        'conditions': ['is_additional_requirement', 'is_backend_team_activity'],
    },
    {
        'trigger': 'notified_customer',
        'source': 'New',
        'destination': 'Documents pending',
        'conditions': ['is_frontend_team_activity'],
    },
    {
        'trigger': 'notification_from_customer',
        'source': 'New',
        'destination': 'Documents pending',
        'conditions': ['is_frontend_team_activity'],
    },
    {
        'trigger': 'notified_insurer',
        'source': 'New',
        'destination': 'Insurer decision pending',
        'conditions': ['is_backend_team_activity'],
    },
    {
        'trigger': 'notified_insurer',
        'source': 'New',
        'destination': 'Appointment scheduled',
        'conditions': ['is_appointment_scheduled', 'is_backend_team_activity'],
    },
    {
        'trigger': 'notified_customer',
        'source': 'Appointment scheduled',
        'destination': 'Appointment accepted',
        'conditions': ['is_appointment_accepted', 'is_frontend_team_activity'],
    },
    {
        'trigger': 'notified_customer',
        'source': 'Appointment scheduled',
        'destination': 'New',
        'conditions': ['is_appointment_rejected', 'is_frontend_team_activity'],
    },
    {
        'trigger': 'notified_customer',
        'source': 'Appointment accepted',
        'destination': 'Insurer decision pending',
        'conditions': ['is_appointment_successful', 'is_frontend_team_activity'],
    },
    {
        'trigger': 'notification_from_insurer',
        'source': 'Insurer decision pending',
        'destination': 'Payment pending',
        'conditions': ['is_request_accepted', 'is_payment_pending', 'is_backend_team_activity'],
    },
    {
        'trigger': 'notification_from_insurer',
        'source': 'Insurer decision pending',
        'destination': 'Documents pending',
        'conditions': ['is_request_accepted', 'is_payment_done', 'is_backend_team_activity'],
    },
    {
        'trigger': 'notified_customer',
        'source': 'Payment pending',
        'destination': 'Documents pending',
        'conditions': ['is_payment_done', 'is_frontend_team_activity'],
    },
    {
        'trigger': 'notification_from_customer',
        'source': 'Payment pending',
        'destination': 'Documents pending',
        'conditions': ['is_payment_done', 'is_frontend_team_activity'],
    },
    {
        'trigger': 'notified_insurer',
        'source': 'New',
        'destination': 'Policy pending',
        'conditions': ['is_backend_team_activity'],
    },
    {
        'trigger': 'notification_from_insurer',
        'source': 'New',
        'destination': 'Policy pending',
        'conditions': ['is_backend_team_activity'],
    },
    {
        'trigger': 'notification_from_insurer',
        'source': 'Policy pending',
        'destination': 'QC pending',
        'conditions': ['is_backend_team_activity'],
    },
    {
        'trigger': 'qc_completed',
        'source': 'QC pending',
        'destination': 'Policy approved',
        'conditions': ['is_passed_quality_check', 'is_frontend_team_activity'],
    },
    {
        'trigger': 'qc_completed',
        'source': 'QC pending',
        'destination': 'New',
        'conditions': ['is_failed_quality_check', 'is_frontend_team_activity'],
    },
    {
        'trigger': 'uploaded_to_account',
        'source': 'Policy approved',
        'destination': 'Policy uploaded',
        'conditions': ['is_frontend_team_activity'],
    },
    {
        'trigger': 'notified_customer',
        'source': 'Policy uploaded',
        'destination': 'Policy dispatch pending',
        'conditions': ['is_frontend_team_activity'],
    },
    {
        'trigger': 'notified_insurer',
        'source': 'Policy dispatch pending',
        'destination': 'Policy dispatched',
        'conditions': ['is_policy_dispatched', 'is_backend_team_activity'],
    },
    {
        'trigger': 'notification_from_insurer',
        'source': 'Policy dispatch pending',
        'destination': 'Policy dispatched',
        'conditions': ['is_policy_dispatched', 'is_backend_team_activity'],
    },
    {
        'trigger': 'notified_customer',
        'source': 'Policy dispatched',
        'destination': 'Close resolved',
        'conditions': ['is_frontend_team_activity'],
    },
    {
        'trigger': 'notified_customer',
        'source': 'Policy dispatched',
        'destination': 'Health card pending',
        'conditions': ['is_frontend_team_activity'],
    },
    {
        'trigger': 'notified_insurer',
        'source': 'Health card pending',
        'destination': 'Insurer intimated',
        'conditions': ['is_backend_team_activity'],
    },
    {
        'trigger': 'notification_from_insurer',
        'source': 'Insurer intimated',
        'destination': 'Health card received',
        'conditions': ['is_backend_team_activity'],
    },
    {
        'trigger': 'uploaded_to_account',
        'source': 'Health card received',
        'destination': 'Health card uploaded',
        'conditions': ['is_frontend_team_activity'],
    },
    {
        'trigger': 'notified_customer',
        'source': 'Health card uploaded',
        'destination': 'Close resolved',
        'conditions': ['is_frontend_team_activity'],
    },
    {
        'trigger': 'notification_from_customer',
        'source': 'New',
        'destination': 'Close resolved',
        'after': ['update_ticket_with_details']
    },
    {
        'trigger': 'notified_customer',
        'source': 'New',
        'destination': 'Close resolved',
        'after': ['update_ticket_with_details']
    },
    {
        'trigger': 'notification_from_customer',
        'source': 'New',
        'destination': 'Close resolved',
    },
    {
        'trigger': 'notified_customer',
        'source': 'New',
        'destination': 'Close resolved',
    },
]

transitions = []
for t in transition_args:
    transition = state_models.Transition.objects.create_from_args(**t)
    transitions.append(transition)

transitions_for_claim = transitions[0:11]

transitions_for_cancel = transitions[0:1]
transitions_for_cancel.extend(transitions[4:10])
transitions_for_cancel.extend(transitions[11:13])

transitions_for_refund = [transitions[0], transitions[8], transitions[13]]

transitions_for_endorsement = transitions[0:1]
transitions_for_endorsement.extend(transitions[4:10])
transitions_for_endorsement.extend(transitions[11:13])

transitions_for_offline = transitions[0:1]
transitions_for_offline.extend(transitions[4:10])
transitions_for_offline.extend(transitions[14:22])

transitions_for_approved = transitions[0:1]
transitions_for_approved.extend(transitions[22:32])

transitions_for_approved_health = transitions[0:1]
transitions_for_approved_health.extend(transitions[22:31])
transitions_for_approved_health.extend(transitions[32:37])

transitions_for_fresh = transitions[0:1]
transitions_for_fresh.extend(transitions[37:39])

transitions_for_inquiry = transitions[0:1]
transitions_for_inquiry.extend(transitions[39:41])
print "Transitions created"
##########################################################################

##########################################################################
# NonTransitions
non_transition_args = [
    {
        'trigger': 'no_conversation',
        'after': ['send_no_conversation_notification'],
    },
]
non_transitions = []
for t in non_transition_args:
    non_transition = state_models.NonTransition.objects.create_from_args(**t)
    non_transitions.append(non_transition)

##########################################################################
# Machine
machine_claim, created = state_models.Machine.objects.get_or_create(
    name='claim_generic', initial_state=states[3])
machine_claim.states.add(*states_for_claim)
machine_claim.transitions.add(*transitions_for_claim)
machine_claim.non_transitions.add(*non_transitions)

machine_cancel, created = state_models.Machine.objects.get_or_create(
    name='cancel_generic', initial_state=states[3])
machine_cancel.states.add(*states_for_cancel)
machine_cancel.transitions.add(*transitions_for_cancel)
machine_cancel.non_transitions.add(*non_transitions)

machine_refund, created = state_models.Machine.objects.get_or_create(
    name='refund_generic', initial_state=states[3])
machine_refund.states.add(*states_for_refund)
machine_refund.transitions.add(*transitions_for_refund)
machine_refund.non_transitions.add(*non_transitions)

machine_endorsement, created = state_models.Machine.objects.get_or_create(
    name='endorsement_generic', initial_state=states[3])
machine_endorsement.states.add(*states_for_endorsement)
machine_endorsement.transitions.add(*transitions_for_endorsement)
machine_endorsement.non_transitions.add(*non_transitions)

machine_offline, created = state_models.Machine.objects.get_or_create(
    name='offline_generic', initial_state=states[3])
machine_offline.states.add(*states_for_offline)
machine_offline.transitions.add(*transitions_for_offline)
machine_offline.non_transitions.add(*non_transitions)

machine_approved, created = state_models.Machine.objects.get_or_create(
    name='approved_generic', initial_state=states[3])
machine_approved.states.add(*states_for_approved)
machine_approved.transitions.add(*transitions_for_approved)
machine_approved.non_transitions.add(*non_transitions)

machine_approved_health, created = state_models.Machine.objects.get_or_create(
    name='approved_health_generic', initial_state=states[3])
machine_approved_health.states.add(*states_for_approved_health)
machine_approved_health.transitions.add(*transitions_for_approved_health)
machine_approved_health.non_transitions.add(*non_transitions)

machine_fresh, created = state_models.Machine.objects.get_or_create(
    name='fresh_generic', initial_state=states[3])
machine_fresh.states.add(*states_for_fresh)
machine_fresh.transitions.add(*transitions_for_fresh)
machine_fresh.non_transitions.add(*non_transitions)

machine_inquiry, created = state_models.Machine.objects.get_or_create(
    name='inquiry_generic', initial_state=states[3])
machine_inquiry.states.add(*states_for_inquiry)
machine_inquiry.transitions.add(*transitions_for_inquiry)
machine_inquiry.non_transitions.add(*non_transitions)

print "Machines created"
##########################################################################

##########################################################################
# MachineRouters
mr1 = state_models.MachineRouter.objects.create(
    serial=1,
    machine=machine_claim,
    query={'ticket_category': 'CLAIM'}
)

mr2 = state_models.MachineRouter.objects.create(
    serial=1,
    machine=machine_cancel,
    query={'ticket_category': 'CANCEL', 'product_type': 'motor'}
)

mr3 = state_models.MachineRouter.objects.create(
    serial=1,
    machine=machine_cancel,
    query={'ticket_category': 'CANCEL', 'product_type': 'health'}
)

mr4 = state_models.MachineRouter.objects.create(
    serial=1,
    machine=machine_endorsement,
    query={'ticket_category': 'ENDORSEMENT', 'product_type': 'motor'}
)

mr5 = state_models.MachineRouter.objects.create(
    serial=1,
    machine=machine_endorsement,
    query={'ticket_category': 'ENDORSEMENT', 'product_type': 'health'}
)

mr6 = state_models.MachineRouter.objects.create(
    serial=1,
    machine=machine_refund,
    query={'ticket_category': 'REFUND', 'product_type': 'motor'}
)

mr7 = state_models.MachineRouter.objects.create(
    serial=1,
    machine=machine_refund,
    query={'ticket_category': 'REFUND', 'product_type': 'health'}
)

mr8 = state_models.MachineRouter.objects.create(
    serial=1,
    machine=machine_offline,
    query={'ticket_category': 'OFFLINE', 'product_type': 'motor'}
)

mr9 = state_models.MachineRouter.objects.create(
    serial=1,
    machine=machine_offline,
    query={'ticket_category': 'OFFLINE', 'product_type': 'health'}
)

mr10 = state_models.MachineRouter.objects.create(
    serial=1,
    machine=machine_approved,
    query={'ticket_category': 'APPROVED', 'product_type': 'motor'}
)

mr11 = state_models.MachineRouter.objects.create(
    serial=1,
    machine=machine_approved_health,
    query={'ticket_category': 'APPROVED', 'product_type': 'health'}
)

mr12 = state_models.MachineRouter.objects.create(
    serial=1,
    machine=machine_fresh,
    query={'ticket_category': 'FRESH'}
)

mr13 = state_models.MachineRouter.objects.create(
    serial=1,
    machine=machine_inquiry,
    query={'ticket_category': 'INQUIRY'}
)
print "MachineRouters created"
##########################################################################

##########################################################################
# State Disposition Trigger mapping

# cancel-flow
sdt_map_params = [
    {'state': 'New', 'disposition': 'NOTIFICATION_TO_CUSTOMER',
        'trigger': 'notified_customer', 'machine': machine_cancel},
    {'state': 'New', 'disposition': 'NOTIFICATION_FROM_CUSTOMER',
        'trigger': 'notification_from_customer', 'machine': machine_cancel},
    {'state': 'New', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_cancel},
    {'state': 'New', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_cancel},

    {'state': 'Documents pending', 'disposition': 'NOTIFICATION_FROM_CUSTOMER',
        'trigger': 'notification_from_customer', 'machine': machine_cancel},
    {'state': 'Documents pending', 'disposition': 'NOTIFICATION_TO_CUSTOMER',
        'trigger': 'notified_customer', 'machine': machine_cancel},
    {'state': 'Documents pending', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_cancel},
    {'state': 'Documents pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_cancel},

    {'state': 'Documents received', 'disposition': 'DOCUMENTS_UPLOADED',
        'trigger': 'documents_uploaded', 'machine': machine_cancel},
    {'state': 'Documents received', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_cancel},
    {'state': 'Documents received', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_cancel},

    {'state': 'Documents uploaded', 'disposition': 'NOTIFICATION_TO_INSURER',
        'trigger': 'notified_insurer', 'machine': machine_cancel},
    {'state': 'Documents uploaded', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_cancel},
    {'state': 'Documents uploaded', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_cancel},

    {'state': 'Insurer decision pending', 'disposition': 'NOTIFICATION_FROM_INSURER',
        'trigger': 'notification_from_insurer', 'machine': machine_cancel},
    {'state': 'Insurer decision pending', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_cancel},
    {'state': 'Insurer decision pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_cancel},
]
for elem in sdt_map_params:
    sdt, created = state_models.StateDisposition.objects.get_or_create(
        state=state_models.State.objects.get(name=elem['state']),
        machine=machine_cancel,
        disposition=core_models.Disposition.objects.get(
            value=elem['disposition']),
        trigger=state_models.Trigger.objects.get(name=elem['trigger'])
    )
print "state-disposition-trigger mapping completed - cancel flow"

# refund-flow
sdt_map_params = [
    {'state': 'New', 'disposition': 'NOTIFICATION_TO_INSURER',
        'trigger': 'notified_insurer', 'machine': machine_refund},
    {'state': 'New', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_refund},
    {'state': 'New', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_refund},

    {'state': 'Insurer decision pending', 'disposition': 'NOTIFICATION_FROM_INSURER',
        'trigger': 'notification_from_insurer', 'machine': machine_refund},
    {'state': 'Insurer decision pending', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_refund},
    {'state': 'Insurer decision pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_refund},
]
for elem in sdt_map_params:
    sdt, created = state_models.StateDisposition.objects.get_or_create(
        state=state_models.State.objects.get(name=elem['state']),
        machine=machine_refund,
        disposition=core_models.Disposition.objects.get(
            value=elem['disposition']),
        trigger=state_models.Trigger.objects.get(name=elem['trigger'])
    )
print "state-disposition-trigger mapping completed - refund flow"

# claim-flow
sdt_map_params = [
    {'state': 'New', 'disposition': 'NOTIFICATION_TO_INSURER',
        'trigger': 'notified_insurer', 'machine': machine_claim},
    {'state': 'New', 'disposition': 'NOTIFICATION_FROM_INSURER',
        'trigger': 'notification_from_insurer', 'machine': machine_claim},
    {'state': 'New', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_claim},
    {'state': 'New', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_claim},

    {'state': 'Insurer intimated', 'disposition': 'NOTIFICATION_TO_CUSTOMER',
        'trigger': 'notified_customer', 'machine': machine_claim},
    {'state': 'Insurer intimated', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_claim},
    {'state': 'Insurer intimated', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_claim},

    {'state': 'Documents pending', 'disposition': 'NOTIFICATION_FROM_CUSTOMER',
        'trigger': 'notification_from_customer', 'machine': machine_claim},
    {'state': 'Documents pending', 'disposition': 'NOTIFICATION_TO_CUSTOMER',
        'trigger': 'notified_customer', 'machine': machine_claim},
    {'state': 'Documents pending', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_claim},
    {'state': 'Documents pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_claim},

    {'state': 'Documents received', 'disposition': 'DOCUMENTS_UPLOADED',
        'trigger': 'documents_uploaded', 'machine': machine_claim},
    {'state': 'Documents received', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_claim},
    {'state': 'Documents received', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_claim},

    {'state': 'Documents uploaded', 'disposition': 'NOTIFICATION_TO_INSURER',
        'trigger': 'notified_insurer', 'machine': machine_claim},
    {'state': 'Documents uploaded', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_claim},
    {'state': 'Documents uploaded', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_claim},

    {'state': 'Insurer decision pending', 'disposition': 'NOTIFICATION_FROM_INSURER',
        'trigger': 'notification_from_insurer', 'machine': machine_claim},
    {'state': 'Insurer decision pending', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_claim},
    {'state': 'Insurer decision pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_claim},
]
for elem in sdt_map_params:
    sdt, created = state_models.StateDisposition.objects.get_or_create(
        state=state_models.State.objects.get(name=elem['state']),
        machine=machine_claim,
        disposition=core_models.Disposition.objects.get(
            value=elem['disposition']),
        trigger=state_models.Trigger.objects.get(name=elem['trigger'])
    )
print "state-disposition-trigger mapping completed - claim flow"

# endorsement-flow
sdt_map_params = [
    {'state': 'New', 'disposition': 'NOTIFICATION_TO_CUSTOMER',
        'trigger': 'notified_customer', 'machine': machine_endorsement},
    {'state': 'New', 'disposition': 'NOTIFICATION_FROM_CUSTOMER',
        'trigger': 'notification_from_customer', 'machine': machine_endorsement},
    {'state': 'New', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_endorsement},
    {'state': 'New', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_endorsement},

    {'state': 'Documents pending', 'disposition': 'NOTIFICATION_FROM_CUSTOMER',
        'trigger': 'notification_from_customer', 'machine': machine_endorsement},
    {'state': 'Documents pending', 'disposition': 'NOTIFICATION_TO_CUSTOMER',
        'trigger': 'notified_customer', 'machine': machine_endorsement},
    {'state': 'Documents pending', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_endorsement},
    {'state': 'Documents pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_endorsement},

    {'state': 'Documents received', 'disposition': 'DOCUMENTS_UPLOADED',
        'trigger': 'documents_uploaded', 'machine': machine_endorsement},
    {'state': 'Documents received', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_endorsement},
    {'state': 'Documents received', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_endorsement},

    {'state': 'Documents uploaded', 'disposition': 'NOTIFICATION_TO_INSURER',
        'trigger': 'notified_insurer', 'machine': machine_endorsement},
    {'state': 'Documents uploaded', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_endorsement},
    {'state': 'Documents uploaded', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_endorsement},

    {'state': 'Insurer decision pending', 'disposition': 'NOTIFICATION_FROM_INSURER',
        'trigger': 'notification_from_insurer', 'machine': machine_endorsement},
    {'state': 'Insurer decision pending', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_endorsement},
    {'state': 'Insurer decision pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_endorsement},
]
for elem in sdt_map_params:
    sdt, created = state_models.StateDisposition.objects.get_or_create(
        state=state_models.State.objects.get(name=elem['state']),
        machine=machine_endorsement,
        disposition=core_models.Disposition.objects.get(
            value=elem['disposition']),
        trigger=state_models.Trigger.objects.get(name=elem['trigger'])
    )

sdt_map_params = [
    {'state': 'New', 'disposition': 'NOTIFICATION_TO_INSURER',
        'trigger': 'notified_insurer', 'machine': machine_offline},
    {'state': 'New', 'disposition': 'NOTIFICATION_FROM_INSURER',
        'trigger': 'notification_from_insurer', 'machine': machine_offline},
    {'state': 'New', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_offline},
    {'state': 'New', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_offline},

    {'state': 'Appointment scheduled', 'disposition': 'NOTIFICATION_FROM_CUSTOMER',
        'trigger': 'notification_from_customer', 'machine': machine_offline},
    {'state': 'Appointment scheduled', 'disposition': 'NOTIFICATION_TO_CUSTOMER',
        'trigger': 'notified_customer', 'machine': machine_offline},
    {'state': 'Appointment scheduled', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_offline},
    {'state': 'Appointment scheduled', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_offline},

    {'state': 'Appointment accepted', 'disposition': 'NOTIFICATION_TO_CUSTOMER',
        'trigger': 'notified_customer', 'machine': machine_offline},
    {'state': 'Appointment accepted', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_offline},
    {'state': 'Appointment accepted', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_offline},

    {'state': 'Insurer decision pending', 'disposition': 'NOTIFICATION_FROM_INSURER',
        'trigger': 'notification_from_insurer', 'machine': machine_offline},
    {'state': 'Insurer decision pending', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_offline},
    {'state': 'Insurer decision pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_offline},

    {'state': 'Payment pending', 'disposition': 'NOTIFICATION_FROM_CUSTOMER',
        'trigger': 'notification_from_customer', 'machine': machine_offline},
    {'state': 'Payment pending', 'disposition': 'NOTIFICATION_TO_CUSTOMER',
        'trigger': 'notified_customer', 'machine': machine_offline},
    {'state': 'Payment pending', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_offline},
    {'state': 'Payment pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_offline},

    {'state': 'Documents pending', 'disposition': 'NOTIFICATION_FROM_CUSTOMER',
        'trigger': 'notification_from_customer', 'machine': machine_offline},
    {'state': 'Documents pending', 'disposition': 'NOTIFICATION_TO_CUSTOMER',
        'trigger': 'notified_customer', 'machine': machine_offline},
    {'state': 'Documents pending', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_offline},
    {'state': 'Documents pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_offline},

    {'state': 'Documents received', 'disposition': 'DOCUMENTS_UPLOADED',
        'trigger': 'documents_uploaded', 'machine': machine_offline},
    {'state': 'Documents received', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_offline},
    {'state': 'Documents received', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_offline},

    {'state': 'Documents uploaded', 'disposition': 'NOTIFICATION_TO_INSURER',
        'trigger': 'notified_insurer', 'machine': machine_offline},
    {'state': 'Documents uploaded', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_offline},
    {'state': 'Documents uploaded', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_offline},

    {'state': 'Insurer decision pending', 'disposition': 'NOTIFICATION_FROM_INSURER',
        'trigger': 'notification_from_insurer', 'machine': machine_offline},
    {'state': 'Insurer decision pending', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_offline},
    {'state': 'Insurer decision pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_offline},
]
for elem in sdt_map_params:
    sdt, created = state_models.StateDisposition.objects.get_or_create(
        state=state_models.State.objects.get(name=elem['state']),
        machine=machine_offline,
        disposition=core_models.Disposition.objects.get(
            value=elem['disposition']),
        trigger=state_models.Trigger.objects.get(name=elem['trigger'])
    )
print "state-disposition-trigger mapping completed - offline flow"

sdt_map_params = [
    {'state': 'New', 'disposition': 'NOTIFICATION_TO_INSURER',
        'trigger': 'notified_insurer', 'machine': machine_approved},
    {'state': 'New', 'disposition': 'NOTIFICATION_FROM_INSURER',
        'trigger': 'notification_from_insurer', 'machine': machine_approved},
    {'state': 'New', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_approved},
    {'state': 'New', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_approved},

    {'state': 'Policy pending', 'disposition': 'NOTIFICATION_FROM_INSURER',
        'trigger': 'notification_from_insurer', 'machine': machine_approved},
    {'state': 'Policy pending', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_approved},
    {'state': 'Policy pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_approved},

    {'state': 'QC pending', 'disposition': 'QUALITY_CHECK_COMPLETED',
        'trigger': 'qc_completed', 'machine': machine_approved},
    {'state': 'QC pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_approved},

    {'state': 'Policy approved', 'disposition': 'UPLOADED_TO_ACCOUNT',
        'trigger': 'uploaded_to_account', 'machine': machine_approved},
    {'state': 'Policy approved', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_approved},

    {'state': 'Policy uploaded', 'disposition': 'NOTIFICATION_TO_CUSTOMER',
        'trigger': 'notified_customer', 'machine': machine_approved},
    {'state': 'Policy uploaded', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_approved},
    {'state': 'Policy uploaded', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_approved},

    {'state': 'Policy dispatch pending', 'disposition': 'NOTIFICATION_FROM_INSURER',
        'trigger': 'notification_from_insurer', 'machine': machine_approved},
    {'state': 'Policy dispatch pending', 'disposition': 'NOTIFICATION_TO_INSURER',
        'trigger': 'notified_insurer', 'machine': machine_approved},
    {'state': 'Policy dispatch pending', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_approved},
    {'state': 'Policy dispatch pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_approved},

    {'state': 'Policy dispatched', 'disposition': 'NOTIFICATION_TO_CUSTOMER',
        'trigger': 'notified_customer', 'machine': machine_approved},
    {'state': 'Policy dispatched', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_approved},
]
for elem in sdt_map_params:
    sdt, created = state_models.StateDisposition.objects.get_or_create(
        state=state_models.State.objects.get(name=elem['state']),
        machine=machine_approved,
        disposition=core_models.Disposition.objects.get(
            value=elem['disposition']),
        trigger=state_models.Trigger.objects.get(name=elem['trigger'])
    )
print "state-disposition-trigger mapping completed - approved flow"

sdt_map_params = [
    {'state': 'New', 'disposition': 'NOTIFICATION_TO_INSURER',
        'trigger': 'notified_insurer', 'machine': machine_approved_health},
    {'state': 'New', 'disposition': 'NOTIFICATION_FROM_INSURER',
        'trigger': 'notification_from_insurer', 'machine': machine_approved_health},
    {'state': 'New', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_approved_health},
    {'state': 'New', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_approved_health},

    {'state': 'Policy pending', 'disposition': 'NOTIFICATION_FROM_INSURER',
        'trigger': 'notification_from_insurer', 'machine': machine_approved_health},
    {'state': 'Policy pending', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_approved_health},
    {'state': 'Policy pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_approved_health},

    {'state': 'QC pending', 'disposition': 'QUALITY_CHECK_COMPLETED',
        'trigger': 'qc_completed', 'machine': machine_approved_health},
    {'state': 'QC pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_approved_health},

    {'state': 'Policy approved', 'disposition': 'UPLOADED_TO_ACCOUNT',
        'trigger': 'uploaded_to_account', 'machine': machine_approved_health},
    {'state': 'Policy approved', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_approved_health},

    {'state': 'Policy uploaded', 'disposition': 'NOTIFICATION_TO_CUSTOMER',
        'trigger': 'notified_customer', 'machine': machine_approved_health},
    {'state': 'Policy uploaded', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_approved_health},
    {'state': 'Policy uploaded', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_approved_health},

    {'state': 'Policy dispatch pending', 'disposition': 'NOTIFICATION_FROM_INSURER',
        'trigger': 'notification_from_insurer', 'machine': machine_approved_health},
    {'state': 'Policy dispatch pending', 'disposition': 'NOTIFICATION_TO_INSURER',
        'trigger': 'notified_insurer', 'machine': machine_approved_health},
    {'state': 'Policy dispatch pending', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_approved_health},
    {'state': 'Policy dispatch pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_approved_health},

    {'state': 'Policy dispatched', 'disposition': 'NOTIFICATION_TO_CUSTOMER',
        'trigger': 'notified_customer', 'machine': machine_approved_health},
    {'state': 'Policy dispatched', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_approved_health},
    {'state': 'Policy dispatched', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_approved_health},

    {'state': 'Health card pending', 'disposition': 'NOTIFICATION_TO_INSURER',
        'trigger': 'notified_insurer', 'machine': machine_approved_health},
    {'state': 'Health card pending', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_approved_health},
    {'state': 'Health card pending', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_approved_health},

    {'state': 'Insurer intimated', 'disposition': 'NOTIFICATION_FROM_INSURER',
        'trigger': 'notification_from_insurer', 'machine': machine_approved_health},
    {'state': 'Insurer intimated', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_approved_health},
    {'state': 'Insurer intimated', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_approved_health},

    {'state': 'Health card received', 'disposition': 'UPLOADED_TO_ACCOUNT',
        'trigger': 'uploaded_to_account', 'machine': machine_approved_health},
    {'state': 'Health card received', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_approved_health},

    {'state': 'Health card uploaded', 'disposition': 'NOTIFICATION_TO_CUSTOMER',
        'trigger': 'notified_customer', 'machine': machine_approved_health},
    {'state': 'Health card uploaded', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_approved_health},
    {'state': 'Health card uploaded', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_approved_health},
]
for elem in sdt_map_params:
    sdt, created = state_models.StateDisposition.objects.get_or_create(
        state=state_models.State.objects.get(name=elem['state']),
        machine=machine_approved_health,
        disposition=core_models.Disposition.objects.get(
            value=elem['disposition']),
        trigger=state_models.Trigger.objects.get(name=elem['trigger'])
    )
print "state-disposition-trigger mapping completed - approved health flow"


sdt_map_params = [
    {'state': 'New', 'disposition': 'NOTIFICATION_TO_CUSTOMER',
        'trigger': 'notified_customer', 'machine': machine_fresh},
    {'state': 'New', 'disposition': 'NOTIFICATION_FROM_CUSTOMER',
        'trigger': 'notification_from_customer', 'machine': machine_fresh},
    {'state': 'New', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_fresh},
    {'state': 'New', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_fresh},
]
for elem in sdt_map_params:
    sdt, created = state_models.StateDisposition.objects.get_or_create(
        state=state_models.State.objects.get(name=elem['state']),
        machine=machine_fresh,
        disposition=core_models.Disposition.objects.get(
            value=elem['disposition']),
        trigger=state_models.Trigger.objects.get(name=elem['trigger'])
    )
print "state-disposition-trigger mapping completed - fresh flow"


sdt_map_params = [
    {'state': 'New', 'disposition': 'NOTIFICATION_TO_CUSTOMER',
        'trigger': 'notified_customer', 'machine': machine_inquiry},
    {'state': 'New', 'disposition': 'NOTIFICATION_FROM_CUSTOMER',
        'trigger': 'notification_from_customer', 'machine': machine_inquiry},
    {'state': 'New', 'disposition': 'NO_CONVERSATION',
        'trigger': 'no_conversation', 'machine': machine_inquiry},
    {'state': 'New', 'disposition': 'CLOSE_UNRESOLVED',
        'trigger': 'close_unsuccessful', 'machine': machine_inquiry},
]
for elem in sdt_map_params:
    sdt, created = state_models.StateDisposition.objects.get_or_create(
        state=state_models.State.objects.get(name=elem['state']),
        machine=machine_inquiry,
        disposition=core_models.Disposition.objects.get(
            value=elem['disposition']),
        trigger=state_models.Trigger.objects.get(name=elem['trigger'])
    )
print "state-disposition-trigger mapping completed - inquiry flow"
##########################################################################

"""
CORE APP MODELS, ALREADY ADDED!!!!!!!!!!!!
############################################################################################################
# Queues
rt_queue = core_models.Queue(name='Resolution Team', code='resolution-team')
rt_queue.save(None)
rm_queue = core_models.Queue(name='Relationship Team', code='relationship-team')
rm_queue.save(None)
backend_queue = core_models.Queue(name='Backend Team', code='backend-team')
backend_queue.save(None)
print "Queues created"
############################################################################################################

############################################################################################################
# Extra user creation and adding to queues
print "Creating users"
advisor = core_models.User.objects.get(username='advisor')
system = core_models.User.objects.get(username='system')
advisor1 = core_models.User.objects.create_user(system, 'advisor1@gmail.com', advisor.role, 'advisor1', 'asd')
advisor2 = core_models.User.objects.create_user(system, 'advisor2@gmail.com', advisor.role, 'advisor2', 'asd')
advisor3 = core_models.User.objects.create_user(system, 'advisor3@gmail.com', advisor.role, 'advisor3', 'asd')
advisor4 = core_models.User.objects.create_user(system, 'advisor4@gmail.com', advisor.role, 'advisor4', 'asd')
advisor5 = core_models.User.objects.create_user(system, 'advisor5@gmail.com', advisor.role, 'advisor5', 'asd')
advisor6 = core_models.User.objects.create_user(system, 'advisor6@gmail.com', advisor.role, 'advisor6', 'asd')
print "Extra users created"

print "Adding users to queues"
rt_queue = core_models.Queue.objects.get(code='resolution-team')
rt_queue.users.add(*[advisor1, advisor2])
rm_queue = core_models.Queue.objects.get(code='relationship-team')
rm_queue.users.add(*[advisor3, advisor4])
backend_queue = core_models.Queue.objects.get(code='backend-team')
backend_queue.users.add(*[advisor5, advisor6])
print "Users added to queue"
############################################################################################################

############################################################################################################
# QueueRouters
qr1 = core_models.QueueRouter(queue=rt_queue, serial=1, module='is_rt_queue')
qr1.save()
qr2 = core_models.QueueRouter(queue=rm_queue, serial=1, module='is_rm_queue')
qr2.save()
print "QueueRouters created"
############################################################################################################

############################################################################################################
# Dispositions and SubDispositions
noconv_subdisposition_params = [
    {'value': 'NO_CONVERSATION', 'verbose': 'No Conversation'},
    {'value': 'CALLBACK_LATER', 'verbose': 'Callback Later'},
    {'value': 'BUSY', 'verbose': 'Busy'},
    {'value': 'ENGAGED', 'verbose': 'Engaged'},
    {'value': 'OUT_OF_COVERAGE', 'verbose': 'Out of Coverage'},
    {'value': 'RINGING', 'verbose': 'Ringing'},
    {'value': 'SWITCHED_OFF', 'verbose': 'Switched Off'}
]
noconv_subdisp_list = []
for elem in noconv_subdisposition_params:
    subd, created = core_models.SubDisposition.objects.get_or_create(**elem)
    noconv_subdisp_list.append(subd)
noconv_disp, created = core_models.Disposition.objects.get_or_create(
    verbose='No conversation',
    value='NO_CONVERSATION',
)
noconv_disp.sub_dispositions.add(*noconv_subdisp_list)

notification_subdisp_params = [
    {'value': 'SENT_EMAIL', 'verbose': 'Sent Email'},
    {'value': 'SENT_SMS', 'verbose': 'Sent SMS'},
    {'value': 'INCOMING_CALL', 'verbose': 'Incoming Call'},
    {'value': 'OUTGOING_CALL', 'verbose': 'Outgoing Call'},
]
notification_subdisp_list = []
for elem in notification_subdisp_params:
    subd, created = core_models.SubDisposition.objects.get_or_create(**elem)
    notification_subdisp_list.append(subd)
notif_succ_disp, created = core_models.Disposition.objects.get_or_create(
    verbose='Notification to Insurer',
    value='NOTIFICATION_TO_INSURER',
)
notif_succ_disp.sub_dispositions.add(*notification_subdisp_list)

subdisp_params = [
    {'value': 'RECEIVED_EMAIL', 'verbose': 'Received Email'},
    {'value': 'RECEIVED_SMS', 'verbose': 'Received SMS'},
    {'value': 'INCOMING_CALL', 'verbose': 'Incoming Call'},
    {'value': 'OUTGOING_CALL', 'verbose': 'Outgoing Call'},
]
subdisp_list = []
for elem in subdisp_params:
    subd, created = core_models.SubDisposition.objects.get_or_create(**elem)
    subdisp_list.append(subd)
disp, created = core_models.Disposition.objects.get_or_create(
    verbose='Notification from Insurer',
    value='NOTIFICATION_FROM_INSURER',
)
disp.sub_dispositions.add(*subdisp_list)

subdisp_params = [
    {'value': 'RECEIVED_EMAIL', 'verbose': 'Received Email'},
    {'value': 'RECEIVED_SMS', 'verbose': 'Received SMS'},
    {'value': 'INCOMING_CALL', 'verbose': 'Incoming Call'},
]
subdisp_list = []
for elem in subdisp_params:
    subd, created = core_models.SubDisposition.objects.get_or_create(**elem)
    subdisp_list.append(subd)
disp, created = core_models.Disposition.objects.get_or_create(
    verbose='Notification from Customer',
    value='NOTIFICATION_FROM_CUSTOMER',
)
disp.sub_dispositions.add(*subdisp_list)

subdisp_params = [
    {'value': 'SENT_EMAIL', 'verbose': 'Sent Email'},
    {'value': 'SENT_SMS', 'verbose': 'Sent SMS'},
    {'value': 'INCOMING_CALL', 'verbose': 'Incoming Call'},
    {'value': 'OUTGOING_CALL', 'verbose': 'Outgoing Call'},
]
subdisp_list = []
for elem in subdisp_params:
    subd, created = core_models.SubDisposition.objects.get_or_create(**elem)
    subdisp_list.append(subd)
disp, created = core_models.Disposition.objects.get_or_create(
    verbose='Notification to Customer',
    value='NOTIFICATION_TO_CUSTOMER',
)
disp.sub_dispositions.add(*notification_subdisp_list)

subdisp_params = [
    {'value': 'SENT_EMAIL', 'verbose': 'Sent Email'},
    {'value': 'UPLOADED_ON_WEBSITE', 'verbose': 'Uploaded on website'},
]
subdisp_list = []
for elem in subdisp_params:
    subd, created = core_models.SubDisposition.objects.get_or_create(**elem)
    subdisp_list.append(subd)
disp, created = core_models.Disposition.objects.get_or_create(
    verbose='Documents uploaded',
    value='DOCUMENTS_UPLOADED',
)
disp.sub_dispositions.add(*subdisp_list)

subdisp_params = [
    {'value': 'INVALID_TICKET', 'verbose': 'Invalid Ticket'},
    {'value': 'DUPLICATE_TICKET', 'verbose': 'Duplicate Ticket'},
    {'value': 'CUSTOMER_REQUESTED_CLOSURE', 'verbose': 'Closing on Customer Request'},
]
subdisp_list = []
for elem in subdisp_params:
    subd, created = core_models.SubDisposition.objects.get_or_create(**elem)
    subdisp_list.append(subd)
disp, created = core_models.Disposition.objects.get_or_create(
    verbose='Close unresolved',
    value='CLOSE_UNRESOLVED',
)
disp.sub_dispositions.add(*subdisp_list)

subdisp_params = [
    {'value': 'QC_APPROVED', 'verbose': 'Quality check approved'},
    {'value': 'QC_REJECTED', 'verbose': 'Quality check rejected'},
]
subdisp_list = []
for elem in subdisp_params:
    subd, created = core_models.SubDisposition.objects.get_or_create(**elem)
    subdisp_list.append(subd)
disp, created = core_models.Disposition.objects.get_or_create(
    verbose='Quality check completed',
    value='QUALITY_CHECK_COMPLETED',
)
disp.sub_dispositions.add(*subdisp_list)

subdisp_params = [
    {'value': 'UPLOADED_ON_WEBSITE', 'verbose': 'Uploaded on website'},
]
subdisp_list = []
for elem in subdisp_params:
    subd, created = core_models.SubDisposition.objects.get_or_create(**elem)
    subdisp_list.append(subd)
disp, created = core_models.Disposition.objects.get_or_create(
    verbose='Uploaded to account',
    value='UPLOADED_TO_ACCOUNT',
)
disp.sub_dispositions.add(*subdisp_list)

print "Dispositions, subdispositions created"
############################################################################################################

# # Create SMS Templates
sms1 = core_models.CommunicationTemplate(
    name="New Ticket",
    category=core_models.CommunicationTemplate.SMS,
    template="Dear Mr {customer_name}, We have received your {request_type} Request.
    Your request No is {request_no}. We will revert to you soon - Coverfox",
)
sms1.save(None)
sms1_router, created = core_models.CommunicationTemplateRouter.objects.get_or_create(
    template=sms1,
    params={'state':'New'},
    serial=1,
)

sms2 = core_models.CommunicationTemplate(
    name="Close resolved",
    category=core_models.CommunicationTemplate.SMS,
    template="Dear Mr {customer_name}, Your Request No {request_no} is resolved.
    Kindly provide feedback on your experience. Thanks - Coverfox",
)
sms2.save(None)
sms2_router, created = core_models.CommunicationTemplateRouter.objects.get_or_create(
    template=sms2,
    params={'state':'Close resolved'},
    serial=1,
)

sms3 = core_models.CommunicationTemplate(
    name="Pending Documents",
    category=core_models.CommunicationTemplate.SMS,
    template="Dear Mr {customer_name}, We are awaiting your pending documents to process the case further - Coverfox",
)
sms3.save(None)
sms3_router, created = core_models.CommunicationTemplateRouter.objects.get_or_create(
    template=sms3,
    params={'state':'Documents pending'},
    serial=1,
)

sms4 = core_models.CommunicationTemplate(
    name="Dispatch",
    category=core_models.CommunicationTemplate.SMS,
    template="Dear Mr {customer_name}, your document has been dispatched. If you do not
    receive the document in 7 days, please contact us - Coverfox",
)
sms4.save(None)
sms4_router, created = core_models.CommunicationTemplateRouter.objects.get_or_create(
    template=sms4,
    params={'state':'Policy dispatched'},
    serial=1,
)

sms5 = core_models.CommunicationTemplate(
    name="Ringing / No Conversation",
    category=core_models.CommunicationTemplate.SMS,
    template="Dear Mr {customer_name}, We tried calling you on {customer_mobile} but
    were unable to reach you. Please call back on {callback_number}- Coverfox"
)
sms5.save(None)
sms5_router, created = core_models.CommunicationTemplateRouter.objects.get_or_create(
    template=sms5,
    params={'trigger': 'no_conversation'},
    serial=1,
)
sms6 = core_models.CommunicationTemplate(
    name="Appoitnment Scheduled - Medical",
    category=core_models.CommunicationTemplate.SMS,
    template="Dear Mr {customer_name}, Your medical appointment has been scheduled
    on {appointment_time} at {appointment_location}. - Coverfox",
)
sms6.save(None)
sms6_router, created = core_models.CommunicationTemplateRouter.objects.get_or_create(
    template=sms6,
    params={'state': 'Appointment scheduled', 'product_type': 'health'},
    serial=1,
)

sms7 = core_models.CommunicationTemplate(
    name="Appointment Scheduled - Motor Offline",
    category=core_models.CommunicationTemplate.SMS,
    template="Dear Mr {customer_name}, Your car inspection has been scheduled on
    {appointment_time} at {appointment_location}. - Coverfox",
)
sms7.save(None)
sms7_router, created = core_models.CommunicationTemplateRouter.objects.get_or_create(
    template=sms7,
    params={'state': 'Appointment scheduled', 'product_type': 'car'},
    serial=1,
)

sms8 = core_models.CommunicationTemplate(
    name="Payment Pending - Loading",
    category=core_models.CommunicationTemplate.SMS,
    template="Dear Mr {customer_name}, We await the premium loading amount of Rs.
    {pending_amount}. Request you to kindly make the payment at earliest - Coverfox",
)
sms8.save(None)
sms8_router, created = core_models.CommunicationTemplateRouter.objects.get_or_create(
    template=sms8,
    params={'state': 'Payment pending', 'product_type': 'health'},
    serial=1,
)

sms9 = core_models.CommunicationTemplate(
    name="Payment Pending - Motor Offline",
    category=core_models.CommunicationTemplate.SMS,
    template="Dear Mr {customer_name}, We await the premium amount of Rs.
    {pending_amount}. Request you to kindly make the payment at earliest.- Coverfox",
)
sms9.save(None)
sms9_router, created = core_models.CommunicationTemplateRouter.objects.get_or_create(
    template=sms9,
    params={'state': 'Payment pending', 'product_type': 'car'},
    serial=1,
)
############################################################################################################
"""
