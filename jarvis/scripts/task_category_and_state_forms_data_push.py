import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()

import datetime
from core import models as core_models
from statemachine import models as sm_models
from alfred import models as alfred_models


TICKET_TAGS = [
    'ESCALATION',
    'COMPLAINT'
]

for item in TICKET_TAGS:
    alfred_models.TicketTag.objects.create(name=item)

ticket_priority = [
    {'name': 'CRITICAL', 'verbose': 'Critical'},
    {'name': 'HIGH', 'verbose': 'High'},
    {'name': 'NORMAL', 'verbose': 'Normal'},
    {'name': 'LOW', 'verbose': 'Low'},
]

for item in ticket_priority:
    tp = alfred_models.TicketPriority()
    for k, v in item.items():
        setattr(tp, k, v)
    tp.save()
    print tp

ticket_categories = [
    'OFFLINE_SALE',
    'CANCEL',
    'REFUND',
    'CLAIM',
    'ENDORSEMENT',
    'APPROVED',
    'FRESH',
    'INQUIRY',
]

[alfred_models.TicketCategory.objects.create(
    name=item) for item in ticket_categories]

queue_choices = {
    'rt': 'Resolution Team',
    'rm': 'Relationship Team',
    'backend': 'Backend Team',
    'tu': 'Ticket update queue',
}


def get_queue(key):
    return core_models.Queue.objects.get(name=queue_choices[key])

stateformrouter = [
    # 'state_name', 'machine_name', 'form_name', 'template_name', 'query'
    [
        'New',
        'cancel_generic',
        'CancelInitialForm',
        'cancel_initial_form',
        None
    ],
    [
        'Documents pending',
        'cancel_generic',
        'CancelCustomerDocumentPendingForm',
        'cancel_customer_document_pending_form',
        None
    ],
    [
        'Documents received',
        'cancel_generic',
        'CancelCustomerDocumentUploadPendingForm',
        'cancel_customer_document_upload_pending_form',
        None
    ],
    [
        'Insurer decision pending',
        'cancel_generic',
        'CancelInsurerDecisionPendingForm',
        'cancel_insurer_decision_pending_form',
        None
    ],


    [
        'New',
        'refund_generic',
        'RefundInitialForm',
        'refund_initial_form',
        None
    ],
    [
        'Insurer decision pending',
        'refund_generic',
        'RefundInsurerConfirmationPendingForm',
        'refund_insurer_confirmation_pending_form',
        None
    ],
    [
        'Request accepted',
        'refund_generic',
        'RefundReceivedForm',
        'refund_received_form',
        None
    ],

    [
        'Insurer intimated',
        'claim_generic',
        'ClaimMotorInsurerIntimatedForm',
        'claim_motor_insurer_intimated_form',
        {'product_type': 'motor'}
    ],
    [
        'Insurer intimated',
        'claim_generic',
        'ClaimHealthInsurerIntimatedForm',
        'claim_health_insurer_intimated_form',
        {'product_type': 'health'}
    ],
    [
        'Documents pending',
        'claim_generic',
        'ClaimMotorCustomerDocumentUploadPendingForm',
        'claim_motor_customer_document_upload_pending_form',
        {'product_type': 'motor'}
    ],
    [
        'Documents pending',
        'claim_generic',
        'ClaimHealthCustomerDocumentUploadPendingForm',
        'claim_health_customer_document_upload_pending_form',
        {'product_type': 'health'}
    ],
    [
        'Documents uploaded',
        'claim_generic',
        'ClaimDocumentUploadedForm',
        'claim_document_uploaded_form',
        None
    ],
    [
        'Insurer decision pending',
        'claim_generic',
        'ClaimInsurerDecisionPendingForm',
        'claim_insurer_decision_pending_form',
        None
    ],

    [
        'Request accepted',
        'claim_generic',
        'ClaimAccepted',
        'claim_accepted',
        None
    ],
    [
        'Request rejected',
        'claim_generic',
        'ClaimRejected',
        'claim_rejected',
        None
    ],
    [
        'New',
        'endorsement_generic',
        'EndorsementIntiialForm',
        'endorsement_initial_form',
        None
    ],
    [
        'Documents pending',
        'endorsement_generic',
        'EndorsementCustomerDocumentPendingForm',
        'endorsement_customer_document_pending_form',
        None
    ],
    [
        'Documents received',
        'endorsement_generic',
        'EndorsementDocumentUploadPendingForm',
        'endorsement_document_upload_pending_form',
        None
    ],
    [
        'Documents uploaded',
        'endorsement_generic',
        'EndorsementInsurerNotificationPendingForm',
        'endorsement_insurer_notification_pending_form',
        None
    ],
    [
        'Insurer decision pending',
        'endorsement_generic',
        'EndorsementInsurerConfirmationPendingForm',
        'endorsement_insurer_confirmation_pending_form',
        None
    ],

    [
        'QC pending',
        'approved_generic',
        'ApprovedPolicyQcPendingForm',
        'approved_policy_qc_pending_form',
        None
    ],
    [
        'Policy uploaded',
        'approved_generic',
        'ApprovedPolicyUploadedForm',
        'approved_policy_uploaded_form',
        None
    ],
    [
        'Policy dispatch pending',
        'approved_generic',
        'ApprovedPolicyDispatchPendingForm',
        'approved_policy_dispatch_pending_form',
        None
    ],
    [
        'Policy dispatched',
        'approved_generic',
        'ApprovedPolicyDispatchedForm',
        'approved_policy_dispatched_form',
        None
    ],

    [
        'QC pending',
        'approved_health_generic',
        'ApprovedPolicyQcPendingForm',
        'approved_policy_qc_pending_form',
        None
    ],
    [
        'Policy uploaded',
        'approved_health_generic',
        'ApprovedPolicyUploadedForm',
        'approved_policy_uploaded_form',
        None
    ],
    [
        'Policy dispatch pending',
        'approved_health_generic',
        'ApprovedPolicyDispatchPendingForm',
        'approved_policy_dispatch_pending_form',
        None
    ],
    [
        'Policy dispatched',
        'approved_health_generic',
        'ApprovedPolicyDispatchedForm',
        'approved_policy_dispatched_form',
        None
    ],

    [
        'New',
        'offline_generic',
        'OfflineInitialForm',
        'offline_initial_form',
        None
    ],
    [
        'Insurer decision pending',
        'offline_generic',
        'OfflineInsurerDecisionPendingForm',
        'offline_insurer_decision_pending_form',
        None
    ],
    [
        'Appointment scheduled',
        'offline_generic',
        'OfflineAppointmentScheduledForm',
        'offline_appointment_scheduled_form',
        None
    ],
    [
        'Appointment accepted',
        'offline_generic',
        'OfflineAppointmentAcceptedForm',
        'offline_appointment_accepted_form',
        None
    ],
    [
        'Documents pending',
        'offline_generic',
        'OfflineCustomerDocumentPendingForm',
        'offline_customer_document_pending_form',
        None
    ],
    [
        'Payment pending',
        'offline_generic',
        'OfflinePaymentPendingForm',
        'offline_payment_pending_form',
        None
    ],
    [
        'Request rejected',
        'offline_generic',
        'OfflineRequestRejectedForm',
        'offline_request_rejected_form',
        None
    ],
]


for row in stateformrouter:
    print row
    state = sm_models.State.objects.get(name=row[0])
    machine = sm_models.Machine.objects.get(name=row[1])
    name = row[2]
    template = row[3]
    query = row[4]
    sfr, _ = alfred_models.StateFormRouter.objects.get_or_create(
        name=name, template=template, query=query)
    sfr.state.add(state)
    sfr.machine.add(machine)
    print sfr

task_categories = [
    # 'name', 'queue key'
    ['Request Customer Documents', ['rm', 'rt']],
    ['Receive Customer Documents', ['rm', 'rt']],
    ['Upload Customer Documents', ['backend']],
    ['Insurer Claim Decision Pending', ['backend']],
    ['Automatic Notification to Customer', ['rm', 'rt']],
    ['Notify Insurer', ['backend']],
    ['Policy Cancel confirmation', ['backend']],
    ['Confirm Refund Status', ['backend']],
    ['Policy Endosrsement Confirmation', ['backend']],
    ['Get PDF copy from Insurer', ['backend']],
    ['Get Health Card from Insurer', ['backend']],
    ['Policy QC', ['rm', 'rt']],
    ['Upload Policy to My Account', ['rm', 'rt']],
    ['Welcome Greeting', ['rm', 'rt']],
    ['Policy Dispatch', ['rm', 'rt']],
    ['Health Card Dispatch', ['rm', 'rt']],
    ['Upload Health Card to My Account', ['rm', 'rt']],
    ['Schedule Appointment', ['backend']],
    ['Receive payment from customer', ['rm', 'rt']],
    ['Notify Customer', ['rm', 'rt']],
    ['Ask for Medical/Inspection result', ['backend']],
    ['Customer Followup for medical/inspection', ['rm', 'rt']],
    ['Pick Incoming Call', ['rm', 'rt', 'backend']],
    ['Enter Customer Inputs', ['rm', 'rt']],
]

for row in task_categories:
    name = row[0]
    print name
    tc = core_models.TaskCategory(name=name)
    tc.save()
    queues = [get_queue(key) for key in row[1]]
    tc.queue.add(*queues)
    print tc

state_machine_task_categories = [
    {
        'state': sm_models.State.objects.get(name='New'),
        'machine': sm_models.Machine.objects.get(name='cancel_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Request Customer Documents')
    },
    {
        'state': sm_models.State.objects.get(name='Documents pending'),
        'machine': sm_models.Machine.objects.get(name='cancel_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Receive Customer Documents')
    },
    {
        'state': sm_models.State.objects.get(name='Documents received'),
        'machine': sm_models.Machine.objects.get(name='cancel_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Upload Customer Documents')
    },
    {
        'state': sm_models.State.objects.get(name='Documents uploaded'),
        'machine': sm_models.Machine.objects.get(name='cancel_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Notify Insurer')
    },
    {
        'state': sm_models.State.objects.get(name='Insurer decision pending'),
        'machine': sm_models.Machine.objects.get(name='cancel_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Policy Cancel confirmation')
    },
    {
        'state': sm_models.State.objects.get(name='Request accepted'),
        'machine': sm_models.Machine.objects.get(name='cancel_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Automatic Notification to Customer')
    },
    {
        'state': sm_models.State.objects.get(name='Request rejected'),
        'machine': sm_models.Machine.objects.get(name='cancel_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Automatic Notification to Customer')
    },

    {
        'state': sm_models.State.objects.get(name='New'),
        'machine': sm_models.Machine.objects.get(name='refund_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Notify Insurer')
    },
    {
        'state': sm_models.State.objects.get(name='Insurer decision pending'),
        'machine': sm_models.Machine.objects.get(name='refund_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Confirm Refund Status')
    },
    {
        'state': sm_models.State.objects.get(name='Request accepted'),
        'machine': sm_models.Machine.objects.get(name='refund_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Automatic Notification to Customer')
    },


    {
        'state': sm_models.State.objects.get(name='New'),
        'machine': sm_models.Machine.objects.get(name='claim_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Notify Insurer')
    },
    {
        'state': sm_models.State.objects.get(name='Insurer intimated'),
        'machine': sm_models.Machine.objects.get(name='claim_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Request Customer Documents')
    },
    {
        'state': sm_models.State.objects.get(name='Documents pending'),
        'machine': sm_models.Machine.objects.get(name='claim_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Receive Customer Documents')
    },
    {
        'state': sm_models.State.objects.get(name='Documents received'),
        'machine': sm_models.Machine.objects.get(name='claim_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Upload Customer Documents')
    },
    {
        'state': sm_models.State.objects.get(name='Documents uploaded'),
        'machine': sm_models.Machine.objects.get(name='claim_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Notify Insurer')
    },
    {
        'state': sm_models.State.objects.get(name='Insurer decision pending'),
        'machine': sm_models.Machine.objects.get(name='claim_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Insurer Claim Decision Pending')
    },
    {
        'state': sm_models.State.objects.get(name='Request accepted'),
        'machine': sm_models.Machine.objects.get(name='claim_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Automatic Notification to Customer')
    },
    {
        'state': sm_models.State.objects.get(name='Request rejected'),
        'machine': sm_models.Machine.objects.get(name='claim_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Automatic Notification to Customer')
    },

    {
        'state': sm_models.State.objects.get(name='New'),
        'machine': sm_models.Machine.objects.get(name='endorsement_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Request Customer Documents')
    },
    {
        'state': sm_models.State.objects.get(name='Documents pending'),
        'machine': sm_models.Machine.objects.get(name='endorsement_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Receive Customer Documents')
    },
    {
        'state': sm_models.State.objects.get(name='Documents received'),
        'machine': sm_models.Machine.objects.get(name='endorsement_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Upload Customer Documents')
    },
    {
        'state': sm_models.State.objects.get(name='Documents uploaded'),
        'machine': sm_models.Machine.objects.get(name='endorsement_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Notify Insurer')
    },
    {
        'state': sm_models.State.objects.get(name='Insurer decision pending'),
        'machine': sm_models.Machine.objects.get(name='endorsement_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Policy Endosrsement Confirmation')
    },
    {
        'state': sm_models.State.objects.get(name='Request accepted'),
        'machine': sm_models.Machine.objects.get(name='endorsement_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Automatic Notification to Customer')
    },
    {
        'state': sm_models.State.objects.get(name='Request rejected'),
        'machine': sm_models.Machine.objects.get(name='endorsement_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Automatic Notification to Customer')
    },

    {
        'state': sm_models.State.objects.get(name='New'),
        'machine': sm_models.Machine.objects.get(name='approved_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Get PDF copy from Insurer')
    },
    {
        'state': sm_models.State.objects.get(name='Policy pending'),
        'machine': sm_models.Machine.objects.get(name='approved_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Get PDF copy from Insurer')
    },
    {
        'state': sm_models.State.objects.get(name='QC pending'),
        'machine': sm_models.Machine.objects.get(name='approved_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Policy QC')
    },
    {
        'state': sm_models.State.objects.get(name='Policy approved'),
        'machine': sm_models.Machine.objects.get(name='approved_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Upload Policy to My Account')
    },
    {
        'state': sm_models.State.objects.get(name='Policy uploaded'),
        'machine': sm_models.Machine.objects.get(name='approved_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Welcome Greeting')
    },
    {
        'state': sm_models.State.objects.get(name='Policy dispatch pending'),
        'machine': sm_models.Machine.objects.get(name='approved_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Policy Dispatch')
    },
    {
        'state': sm_models.State.objects.get(name='Policy dispatched'),
        'machine': sm_models.Machine.objects.get(name='approved_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Automatic Notification to Customer')
    },

    {
        'state': sm_models.State.objects.get(name='New'),
        'machine': sm_models.Machine.objects.get(name='approved_health_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Get PDF copy from Insurer')
    },
    {
        'state': sm_models.State.objects.get(name='Policy pending'),
        'machine': sm_models.Machine.objects.get(name='approved_health_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Get PDF copy from Insurer')
    },
    {
        'state': sm_models.State.objects.get(name='QC pending'),
        'machine': sm_models.Machine.objects.get(name='approved_health_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Policy QC')
    },
    {
        'state': sm_models.State.objects.get(name='Policy approved'),
        'machine': sm_models.Machine.objects.get(name='approved_health_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Upload Policy to My Account')
    },
    {
        'state': sm_models.State.objects.get(name='Policy uploaded'),
        'machine': sm_models.Machine.objects.get(name='approved_health_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Welcome Greeting')
    },
    {
        'state': sm_models.State.objects.get(name='Policy dispatch pending'),
        'machine': sm_models.Machine.objects.get(name='approved_health_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Policy Dispatch')
    },
    {
        'state': sm_models.State.objects.get(name='Policy dispatched'),
        'machine': sm_models.Machine.objects.get(name='approved_health_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Get Health Card from Insurer')
    },
    {
        'state': sm_models.State.objects.get(name='Health card pending'),
        'machine': sm_models.Machine.objects.get(name='approved_health_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Health Card Dispatch')
    },
    {
        'state': sm_models.State.objects.get(name='Health card received'),
        'machine': sm_models.Machine.objects.get(name='approved_health_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Upload Health Card to My Account')
    },
    {
        'state': sm_models.State.objects.get(name='Health card uploaded'),
        'machine': sm_models.Machine.objects.get(name='approved_health_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Automatic Notification to Customer')
    },

    {
        'state': sm_models.State.objects.get(name='New'),
        'machine': sm_models.Machine.objects.get(name='offline_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Schedule Appointment')
    },
    {
        'state': sm_models.State.objects.get(name='Documents pending'),
        'machine': sm_models.Machine.objects.get(name='offline_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Receive Customer Documents')
    },
    {
        'state': sm_models.State.objects.get(name='Documents received'),
        'machine': sm_models.Machine.objects.get(name='offline_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Upload Customer Documents')
    },
    {
        'state': sm_models.State.objects.get(name='Documents uploaded'),
        'machine': sm_models.Machine.objects.get(name='offline_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Notify Insurer')
    },
    {
        'state': sm_models.State.objects.get(name='Insurer decision pending'),
        'machine': sm_models.Machine.objects.get(name='offline_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Ask for Medical/Inspection result')
    },
    {
        'state': sm_models.State.objects.get(name='Appointment scheduled'),
        'machine': sm_models.Machine.objects.get(name='offline_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Notify Customer')
    },
    {
        'state': sm_models.State.objects.get(name='Appointment accepted'),
        'machine': sm_models.Machine.objects.get(name='offline_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Customer Followup for medical/inspection')
    },
    {
        'state': sm_models.State.objects.get(name='Payment pending'),
        'machine': sm_models.Machine.objects.get(name='offline_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Receive payment from customer')
    },
    {
        'state': sm_models.State.objects.get(name='Request rejected'),
        'machine': sm_models.Machine.objects.get(name='offline_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Automatic Notification to Customer')
    },

    {
        'state': sm_models.State.objects.get(name='New'),
        'machine': sm_models.Machine.objects.get(name='fresh_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Enter Customer Inputs')
    },

    {
        'state': sm_models.State.objects.get(name='New'),
        'machine': sm_models.Machine.objects.get(name='inquiry_generic'),
        'task_category': core_models.TaskCategory.objects.get(name='Enter Customer Inputs')
    },
]

for row in state_machine_task_categories:
    print 'row--------------------------', row
    smtc = alfred_models.StateMachineTaskCategory()
    for k, v in row.items():
        setattr(smtc, k, v)
    smtc.save()
    print smtc

SUB_REQUESTS_MAP = {
    "ENDORSEMENT": [
        ('NAME_CHANGE', 'Name Change', 5),
        ('ADDRESS_CHANGE', 'Address Change', 5),
        ('REGISTRATION_NO._CHANGE', 'Registration No. Change', 8),
        ('REC_CHANGE', 'REC Change', 8),
        ('E_&_C_INCLUSION', 'E & C Inclusion', 8),
        ('VEHICLE_DETAILS_CHANGE', 'Vehicle Details Change', 5),
        ('CUBIC_CAPACITY_CHANGE', 'Cubic Capacity Change', 8),
        ('HYPOTHECATION_LEASE_HPA', 'Hypothecation\Lease\HPA', 5),
        ('POLICY_PERIOD_CHANGE', 'Policy Period Change', 8),
        ('CHANGE_OF_IDV', 'Change of IDV', 8),
        ('EXTENSION_OF_GEOGRAPHICAL_AREA', 'Extension of Geographical Area', 8),
        ('INCLUSION_OF_ACCESSORIES', 'Inclusion of Accessories', 8),
        ('AAI_DISCOUNT', 'AAI Discount', 8),
        ('VOLUNTARY_DEDUCTIBLE_CHANGE', 'Voluntary Deductible Change', 8),
        ('EXTRA_BENEFIT_INCLUSION', 'Extra Benefit Inclusion', 8),
        ('NCB_SLAB_CHANGE', 'NCB Slab Change', 8),
        ('NOMINEE_CHANGE', 'Nominee Change', 5),
        ('OWNERSHIP_TRANSFER', 'Ownership Transfer', 8),
        ('FLOATER_TO_INDIVIDUAL', 'Floater To Individual', 8),
        ('MEMBER_DELETION_ADDITION', 'Member Deletion/ Addition', 8),
        ('DELETION_OF_VOLUNTARY_DEDUCTIBLE', 'Deletion Of Voluntary Deductible', 8),
        ('OTHERS_ENDORSEMENT', 'Others', 8),
    ],
    "CANCEL": [
        ('NON_DELIVERY_OF_VEHICLE', 'Non-Delivery of Vehicle', 14),
        ('SALE_OF_VEHICLE', 'Sale of Vehicle', 14),
        ('DOUBLE_INSURANCE_POLICY', 'Double Insurance Policy', 14),
        ('CHEQUE_DISHONOUR', 'Cheque Dishonour', 14),
        ('OTHERS_CANCEL', 'Others', 14),
    ],
    "OFFLINE_SALE": [
        ('PORTABILITY_CASE', 'Portability Case', 14),
        ('BREAK_IN_CASE', 'Break in Case', 14),
        ('OTHERS_OFFLINE_SALE', 'Others', 14),
    ],
    "CLAIM": [
        ('REIMBURSEMENT_THIRD_PARTY', 'Reimbursement - Third Party', 15),
        ('REIMBURSEMENT_OWN_DAMAGE', 'Reimbursement - Own Damage', 15),
        ('REIMBURSEMENT_THEFT', 'Reimbursement - Theft', 15),
        ('REIMBURSEMENT', 'Reimbursement', 15),
        ('CASHLESS', 'Cashless', 1),
        ('OTHERS_CLAIM', 'Others', 15),
    ],
    "REFUND": [
        ('OTHERS_REFUND', 'Others', 15),
    ],
    "APPROVED": [
        ('OTHERS_APPROVED', 'Others', 7),
    ],
    "FRESH": [
        ('OTHERS_FRESH', 'Others', 2),
    ],
    "INQUIRY": [
        ('QUERY', 'Query', 2),
        ('COMPLAINT', 'Complaint', 2),
        ('FEEDBACK', 'Feedback', 2),
        ('SALES_LEAD', 'Sales Lead', 2),
        ('OTHERS_INQUIRY', 'Others', 2),
    ],
}


for k, v in SUB_REQUESTS_MAP.items():
    for item in v:
        srt = alfred_models.SubRequestType.objects.create(
            name=item[0],
            verbose=item[1],
            request_type=alfred_models.TicketCategory.objects.get(name=k),
            eta=datetime.timedelta(days=item[2])
        )


DOCUMENTCATEGORY_REQUEST_MAP = [
    ['others', 'ENDORSEMENT'],
    ['customer request email', 'ENDORSEMENT'],
    ['Registration certificate', 'ENDORSEMENT'],
    ['Duly filled Form 29/30', 'ENDORSEMENT'],
    ['Propodal form or EDI', 'ENDORSEMENT'],
    ['NOC from previous owner', 'ENDORSEMENT'],
    ['Previous policy copyright', 'ENDORSEMENT'],
    ['Transfer fees', 'ENDORSEMENT'],
    ['NCB difference', 'ENDORSEMENT'],
    ['Inspection Report', 'ENDORSEMENT'],
    ['Zonewise premium difference', 'ENDORSEMENT'],
    ['Dealer letter', 'ENDORSEMENT'],
    ['Invoice of new vehicle', 'ENDORSEMENT'],
    ['Adjusted Premium pro-rata', 'ENDORSEMENT'],
    ['Previous policy copyright', 'ENDORSEMENT'],
    ['Explainantion from producer', 'ENDORSEMENT'],
    ['others', 'CLAIM'],
    ['Intimation email or letter', 'CLAIM'],
    ['Policy copyright', 'CLAIM'],
    ['Premium receipt', 'CLAIM'],
    ['Copy of FIR Charge Sheet', 'CLAIM'],
    ['Driver Statement', 'CLAIM'],
    ['Address of Police Station', 'CLAIM'],
    ['Hospital Address', 'CLAIM'],
    ['Doctor letter', 'CLAIM'],
    ['Post mortem report', 'CLAIM'],
    ['Vakalatnama of owner & driver', 'CLAIM'],
    ['Details of Loss to TP Vehicle or other property', 'CLAIM'],
    ['Driving licence copyright', 'CLAIM'],
    ['Registration Certificate Original and Xerox', 'CLAIM'],
    ['Surveyor details', 'CLAIM'],
    ['Driving licence copyright', 'CLAIM'],
    ['Repair estimation/ Final Bill', 'CLAIM'],
    ['Bill and Cash memoryview', 'CLAIM'],
    ['Payment receipt with stamp', 'CLAIM'],
    ['FIR or Panchanama', 'CLAIM'],
    ['Verification of Road Tax', 'CLAIM'],
    ['FIR or Panchanama', 'CLAIM'],
    ['Police investigation report', 'CLAIM'],
    ['Assesment of loss', 'CLAIM'],
    ['Inventory of articles kept in bank-locker', 'CLAIM'],
    ['Driving licence copyright', 'CLAIM'],
    ['Registration Certificate Original and zerox', 'CLAIM'],
    ['Key of vehicle', 'CLAIM'],
    ['Untrace certificate from police station', 'CLAIM'],
    ['Claim format', 'CLAIM'],
    ['Hospital Bill', 'CLAIM'],
    ['Payment Receipt', 'CLAIM'],
    ['Pharmacy Bill', 'CLAIM'],
    ['Investigation or lab report', 'CLAIM'],
    ['Lab bills', 'CLAIM'],
    ['Intimation email or letter', 'CLAIM'],
    ['Bill break up', 'CLAIM'],
    ['Discharge card or summary', 'CLAIM'],
    ['OT notes, ICP, Nursing chart', 'CLAIM'],
    ['Prescriptions', 'CLAIM'],
    ['Cancelled cheque copyright', 'CLAIM'],
    ['others', 'OFFLINE_SALE'],
    ['Proposal form or EDI', 'OFFLINE_SALE'],
    ['cheque/ cash/ online payment', 'OFFLINE_SALE'],
    ['ID proof', 'OFFLINE_SALE'],
    ['Address proof', 'OFFLINE_SALE'],
    ['IT return', 'OFFLINE_SALE'],
    ['salary slip', 'OFFLINE_SALE'],
    ['medical documents', 'OFFLINE_SALE'],
    ['previous year policy copyright', 'OFFLINE_SALE'],
    ['no claim detail', 'OFFLINE_SALE'],
    ['portability format', 'OFFLINE_SALE'],
    ['medical documents', 'OFFLINE_SALE'],
    ['mandate letter', 'OFFLINE_SALE'],
    ['Approved inspection report', 'OFFLINE_SALE'],
    ['Registration certificate', 'OFFLINE_SALE'],
    ['calculation sheet', 'OFFLINE_SALE'],
    ['Original Cover note.', 'CANCEL'],
    ['others', 'CANCEL'],
    ['Letter form Financier', 'CANCEL'],
    ['Transfered Registration certificate', 'CANCEL'],
    ['Original policy for cancellation', 'CANCEL'],
    ['Insurance proof (new policy)', 'CANCEL'],
    ['Original Cover note.', 'CANCEL'],
    ['Letter form Financier(if applicable)', 'CANCEL'],
    ['Insurance proof (new policy)', 'CANCEL'],
    ['TP policy copyright', 'CANCEL'],
    ['cancelled cheuque', 'CANCEL'],
    ['customer request email', 'CANCEL'],
    ['others', 'REFUND'],
    ['others', 'APPROVED'],
    ['others', 'FRESH'],
    ['others', 'INQUIRY'],
]

for item in DOCUMENTCATEGORY_REQUEST_MAP:
    doccat = item[0]
    ticket_category = item[1]
    dc, _ = alfred_models.DocumentCategory.objects.get_or_create(name=doccat)
    dc.request_type.add(
        alfred_models.TicketCategory.objects.get(name=ticket_category))

# DUMMY_POLICY: will be used for Fresh tickets creation
alfred_models.Policy.objects.create(
    policy_number='DUMMY',
    data={}
)
