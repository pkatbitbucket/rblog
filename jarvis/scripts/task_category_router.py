import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()

import core.models as core_models

harshad = core_models.User.objects.get(username='harshad')
swapnil = core_models.User.objects.get(username='harshad')
ravi = core_models.User.objects.get(username='ravi')
atmaram = core_models.User.objects.get(username='atmaram')
sonika = core_models.User.objects.get(username='sonika')
tejas = core_models.User.objects.get(username='tejasloke')
queue_map = {
    'CLAIM1': ['Backend Motor Claim Team', 'backend_motor_claim', 'motor', 'CLAIM', [harshad], 3],
    'CLAIM2': ['Backened Health Claim Team', 'backend_health_claim', 'health', 'CLAIM', [ravi], 3],
    'CLAIM3': ['Backend Travel Claim Team', 'backend_travel_claim', 'travel', 'CLAIM', [ravi], 3],
    'APPROVED1': ['Backend Motor Approved Team', 'backend_motor_approved', 'motor', 'APPROVED', [atmaram], 3],
    'APPROVED2': ['Backend Health Approved Team', 'backend_health_approved', 'health', 'APPROVED', [swapnil], 3],
    'APPROVED3': ['Backend Travel Approved Team', 'backend_travel_approved', 'travel', 'APPROVED', [swapnil], 3],
    'ENDORSEMENT1': ['Backend Motor Generic Team', 'backend_motor_generic', 'motor', '', [sonika], 2],
    'ENDORSEMENT2': ['Backend Health Generic Team', 'backend_health_generic', 'health', '', [tejas], 2],
    'ENDORSEMENT3': ['Backend Travel Generic Team', 'backend_travel_generic', 'travel', '', [tejas], 2],
    'CANCEL1': ['Backend Motor Generic Team', 'backend_motor_generic', 'motor', '', [sonika], 2],
    'CANCEL2': ['Backend Health Generic Team', 'backend_health_generic', 'health', '', [tejas], 2],
    'CANCEL3': ['Backend Travel Generic Team', 'backend_travel_generic', 'travel', '', [tejas], 2],
    'REFUND1': ['Backend Motor Generic Team', 'backend_motor_generic', 'motor', '', [sonika], 2],
    'REFUND2': ['Backend Health Generic Team', 'backend_health_generic', 'health', '', [tejas], 2],
    'REFUND3': ['Backend Travel Generic Team', 'backend_travel_generic', 'travel', '', [tejas], 2],
}

for k, v in queue_map.items():
    q, _ = core_models.Queue.objects.get_or_create(name=v[0], code=v[1])
    q.users.add(v[4][0])
    print v, q.id
    print "-------------"

    params = {'product_type': v[2]}
    if v[3]:
        params.update({'ticket_category': v[3]})
    qr, _ = core_models.QueueRouter.objects.get_or_create(serial=v[5], params=params,
                                                          queue=core_models.Queue.objects.get(name=v[0]))
    print qr.id
    print "---------------------\n"
