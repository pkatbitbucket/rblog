#!/bin/bash
# Bash script to setup server
if [ -z "$1" ]
  then
    echo "Please provide location for setup"
    exit 1
fi

sudo apt-get -y update
sudo apt-get -y install git htop zsh
sudo chsh -s $(which zsh) ubuntu
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
sudo apt-get -y install nginx redis-server npm
sudo apt-get -y install zsh libzmq1 libzmq-dev make automake bison build-essential python-dev python-virtualenv openssl libssl-dev libevent-dev
sudo apt-get -y install libpython2.7-dev libpq-dev libncurses5-dev libxml2-dev libxslt1-dev
sudo apt-get -y install postgresql postgresql-contrib
sudo apt-get -y install graphviz libgraphviz-dev pkg-config
sudo apt-get -y install libmagic-dev lib32ncurses5-dev

# To be able to use postgres Hstorefield
sudo apt-get -y install postgis postgresql-9.3-postgis-scripts

# Make directory and cd into it
cd /home/www/
mkdir "$@"

git clone git@bitbucket.org:glitterbug/jarvis.git "$@"
cd "$@"

virtualenv envproj
source envproj/bin/activate

pip install -r requirements.txt

npm config set strict-ssl false
npm install
sudo npm install -g webpack
################

