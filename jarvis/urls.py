from django.conf.urls import include, url
from django.contrib.auth.views import login, logout
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

from coverfox.mail import views as mail_views

admin.autodiscover()


urlpatterns = [
    url(r'^accounts/login/$', login,
        {'template_name': "crm/login.html"}, name='login'),
    url(r'^accounts/logout/$', logout, {'next_page': '/'}, name='logout'),
    url(r'^nimda/doc/', include('django.contrib.admindocs.urls')),
    url(r'^nimda/', admin.site.urls),

    url(r'^google-registration/$', mail_views.registration,
        name='google_registration'),
    url(r'^mail/', include('coverfox.mail.urls')),
    url(r'^crm/', include('crm.urls')),
    url(r'', include('core.urls')),
] + static(
    settings.STATIC_URL, document_root=settings.STATIC_ROOT
) + static(
    settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
)
