var path = require('path');
var ContextReplacementPlugin = require("webpack/lib/ContextReplacementPlugin");
var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");
var Clean = require('clean-webpack-plugin');

var alfred_app_path = path.join(__dirname, "../static/alfred/assets/");
var app_lib_path = path.join(__dirname, "../static/crm/");
var alfred_build_path = path.join(__dirname, "../static/crm/build/")

module.exports = {
    context: __dirname,
    entry: {
        advisor: alfred_app_path + "js/advisor.js",
        common: ['jquery', 'knockout', 'signals', 'hasher', 'crossroads', 'knockout-validation', 'cookie', 'bstore', 'cutils']
    },
    resolve: {
        modulesDirectories: [
            './node_modules',
        ],
        alias: {
            'jquery' : app_lib_path + 'jquery-2.1.4.min.js',
            'underscore': app_lib_path + 'underscore-min-1.8.3.js',
            'knockout': app_lib_path + 'knockout-3.3.0.js',
            'signals': app_lib_path + 'signals-1.0.0.js',
            'hasher': app_lib_path + 'hasher-1.2.0.js',
            'crossroads': app_lib_path + 'crossroads-0.12.0.js',
            'knockout-validation': app_lib_path + 'knockout.validation-2.0.2.js',
            'cookie' : app_lib_path + 'cookies/cookies.js',
            'bstore' : app_lib_path + 'common/js/bstore.js',
            'cutils' : app_lib_path + 'common/js/utils.js',
            'components' : app_lib_path + 'components',
            'socket.io' : app_lib_path + 'socket.io.js',

            'router' : app_lib_path + 'common/js/router.js',
            'urls': alfred_app_path + 'js/urls',
            'models': alfred_app_path + 'js/models',
            'templates': alfred_app_path + 'js/templates',
        }
    },
    resolveLoader: {root: path.join(__dirname, "./node_modules")},

    module: {
        noParse: [
            '/jquery-2\.1\.4\.min/',
            '/knockout-3\.3\.0/',
        ],
        preLoaders: [
        ],
        loaders: [
            { test: /knockout-3.3.0.js/, loader: "imports?require=>__webpack_require__" },
        ]
    },

    plugins: [
        new CommonsChunkPlugin("common", "common.[hash].js"),
        new Clean([alfred_build_path]),
        function() {
            this.plugin("done", function(stats) {
                require("fs").writeFileSync(
                    path.join(alfred_build_path, "stats.json"),
                    JSON.stringify(stats.toJson())
                );
                require("fs").utimesSync(
                    path.join(__dirname, "settings.py"),
                    new Date(),
                    new Date()
                );

            });
        },
    ],

    jshint: {
    },

    output: {
        path:  alfred_build_path,
        publicPath : '../static/crm/build/',
        pathInfo : true,
        filename: "[name].[hash].bundle.js",
    },
};
