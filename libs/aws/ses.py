import boto.ses
import mimetypes
from email import encoders
from email.utils import COMMASPACE
from email.mime.multipart import MIMEMultipart
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.text import MIMEText

class SES():
    AWS_ACCESS_KEY_ID = 'AKIAJLLDXMWCHG3CNIMA'
    AWS_SECRET_ACCESS_KEY = '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'

    def __init__(self, from_email=None):
        """
        From email should be of type <something>@cfoutreach.in
        """
        self.conn = conn = boto.ses.connect_to_region(
            'us-east-1',
            aws_access_key_id = self.AWS_ACCESS_KEY_ID,
            aws_secret_access_key = self.AWS_SECRET_ACCESS_KEY
        )
        self.from_email = from_email or 'Rupy from Coverfox <rupy@cfoutreach.in>'

    def send_mail(self, subject, to_email, cc_email=[], bcc_email=[], reply_to_email=[], text=None, html=None, attachments=None):
        """
        Usage :
        Either give html or give text, not both
        subject : plain mail text
        text : text of the e-mail, can be none if html if passed
        html : html of e-mail if sending html email, can be none if no html is used
        attachment : file content and not the file object, f.read()
        filename : filename of attachment
        to_email : can be a list or one email-id
        cc_email : list of CC emails or string
        bcc_email : list of bcc_emails or string
        """

        if not attachments:
            return self.conn.send_email(self.from_email, subject, html or text, to_email, cc_email, bcc_email, 'text' if text else 'html', reply_to_email)
        else:
            message = MIMEMultipart()
            message_alt = MIMEMultipart('alternative')
            if text:
                message_alt.attach(MIMEText(text, 'plain'))
            if html:
                message_alt.attach(MIMEText(html, 'html'))
            message.attach(message_alt)

            message['Subject'] = subject
            message['From'] = self.from_email
            if isinstance(to_email, (list, tuple)):
                message['To'] = COMMASPACE.join(to_email)
            else:
                message['To'] = to_email

            message.preamble = 'You will not see this in a MIME-aware mail reader.\n'
            print 'message: ', message.as_string()

            for attachment in attachments:
                ctype, encoding = mimetypes.guess_type(attachment)

                if ctype is None or encoding is not None:
                    ctype = 'application/octet-stream'

                maintype, subtype = ctype.split('/', 1)
                print "MainType:", maintype
                print "SubType:", subtype

                if maintype == 'text':
                    fp = open(attachment)
                    part = MIMEText(fp.read(), _subtype=subtype)
                elif maintype == 'image':
                    fp = open(attachment, 'rb')
                    part = MIMEImage(fp.read(), _subtype=subtype)
                elif maintype == 'audio':
                    fp = open(attachment, 'rb')
                    part = MIMEAudio(fp.read(), _subtype=subtype)
                else:
                    fp = open(attachment, 'rb')
                    part = MIMEBase(maintype, subtype)
                    part.set_payload(fp.read())

                fp.close()

                #encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment; filename=attachment.txt')
                message.attach(part)

            return self.conn.send_raw_email(self.from_email, message.as_string(), destinations=to_email)
