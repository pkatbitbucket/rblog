from django.conf import settings

from boto.s3.connection import S3Connection
from boto.s3.key import Key


class S3Upload(object):

    def __init__(self, bucket_name, aws_access_key=None, aws_secret_key=None):
        if settings.USE_AMAZON_S3:
            if not aws_access_key:
                aws_access_key = settings.AWS_ACCESS_KEY
            if not aws_secret_key:
                aws_secret_key = settings.AWS_SECRET_KEY
            connection = S3Connection(aws_access_key, aws_secret_key)
            if not connection.lookup(bucket_name):
                connection.create_bucket(bucket_name)
            self.bucket = connection.get_bucket(bucket_name)

    def upload(self, key_name, content):
        key = Key(self.bucket, key_name)
        if hasattr(content, 'temporary_file_path'):
            key.set_contents_from_filename(content.temporary_file_path())
        else:
            key.set_contents_from_file(content)

        return "https://%s.s3.amazonaws.com/%s" % (self.bucket.name, key_name)

    def delete(self, name):
        self.bucket.delete_key(name)

    def url(self, name, url_expiry=None):
        if not url_expiry:
            url_expiry = 60000
        cname = name.split("/", 3)[3]
        return Key(self.bucket, cname).generate_url(url_expiry)
