import os
import json
from django.conf import settings


def context_processor(request):
    try:
        with open(os.path.join(settings.BASE_DIR, './static/crm/build/stats.json')) as f:
            j = json.loads(f.read())
            settings.CONFIG['ALFRED_WP_ASSETS'] = j['assetsByChunkName']
            settings.CONFIG['ALFRED_WP_ASSETS_HASH'] = j['hash']
    except:
        pass
    return settings.CONFIG
