import time

import requests
from requests import request

from django.core.cache import cache


API_ENDPOINT = 'https://api.neverbounce.com/v3'
API_USERNAME = 'nyG2soKt'
API_SECRET_KEY = 'px9DBPJG2UjimqN'
AUTH = (API_USERNAME, API_SECRET_KEY)
TOKEN_CACHE_KEY = 'neverbounce_token'


class EmailValidator(object):
    RESULT_CODES = ['Valid', 'Invalid', 'Disposable address', 'Catch-all', 'Unknown']

    @property
    def access_token(self):
        if not cache.get(TOKEN_CACHE_KEY):
            self._generate_token()
        return cache.get(TOKEN_CACHE_KEY)

    def _generate_token(self):
        url = '{}/{}'.format(API_ENDPOINT, 'access_token')
        data = {
            'grant_type': 'client_credentials',
            'scope': 'basic user',
        }
        response_data = self._send_request(
            method='POST',
            url=url,
            data=data,
            auth=AUTH,
        )
        cache.set(TOKEN_CACHE_KEY, response_data['access_token'], int(response_data['expires_in']) - (3 * 60))

    def verify(self, email):
        url = '{}/{}'.format(API_ENDPOINT, 'single')
        data = {
            'access_token': self.access_token,
            'email': email,
        }
        response_data = self._send_request(
            method='POST',
            url=url,
            data=data,
        )
        return self.RESULT_CODES[response_data['result']]

    def get_remaining_credits(self):
        url = '{}/{}'.format(API_ENDPOINT, 'account')
        data = {
            'access_token': self.access_token,
        }
        response_data = self._send_request(
            method='POST',
            url=url,
            data=data,
        )
        return int(response_data['credits'])

    @classmethod
    def _send_request(cls, retries=3, **kwargs):
        """
        Prepares and send the request object. Retires specified number of times before raising an exception.
        :param retries: number of retires to be made before raising an exception
        :type retries: int
        :param kwargs: dictionary of parameters to be passed for request
        :rtype : request object
        """
        retries -= 1
        # print(request_type, kwargs)
        try:
            if not retries < 0:
                _request = request(**kwargs)
                if 400 <= _request.status_code <= 599:
                    raise Exception('Error raised by NeverBounce.\nStatus Code: {}\nError: {}'.format(
                        _request.status_code,
                        _request.text
                    ))
                data = _request.json()
                if 'success' in data and not data['success']:
                    raise Exception('Error occurred in api call to NeverBounce.\n'
                                    'Error Message: {}'.format(_request.text))
                return data
            raise Exception('Unable to send request to NeverBounce')
        except requests.ConnectionError:
            # print('Connection error occurred. Retrying in 3 seconds')
            time.sleep(3)
            return cls._send_request(retries, **kwargs)
        except requests.Timeout:
            # print('Connection timed out. Retrying in 3 seconds')
            time.sleep(3)
            return cls._send_request(retries, **kwargs)
        except Exception as e:
            raise RuntimeError(e)


email_validator = EmailValidator()
