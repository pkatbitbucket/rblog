# imports #
import StringIO
import datetime
import time
import csv
from django.db import models
from django.http import HttpResponse

import xlrd
from xlwt import Formula, Workbook, XFStyle, Borders, Pattern, Font, Alignment

EXCEL_COLOR_MAP = {
    'rgb(0, 0, 0)': -7,
    'rgba(0, 0, 0, 0)': -7,
    'rgb(255, 255, 255)': 2,
    'rgb(255, 0, 0)': 3,
    'rgb(0, 255, 0)': 4,
    'rgb(0, 0, 255)': 5,
    'rgb(255, 255, 0)': 6,
    'rgb(255, 0, 255)': 7,
    'rgb(0, 255, 255)': 8,
    'rgb(128, 0, 0)': 9,
    'rgb(0, 128, 0)': 10,
    'rgb(0, 0, 128)': 11,
    'rgb(128, 128, 0)': 12,
    'rgb(128, 0, 128)': 13,
    'rgb(0, 128, 128)': 14,
    'rgb(192, 192, 192)': 15,
    'rgb(128, 128, 128)': 16,
    'rgb(153, 153, 255)': 17,
    'rgb(153, 51, 102)': 18,
    'rgb(255, 255, 204)': 19,
    'rgb(204, 255, 255)': 20,
    'rgb(102, 0, 102)': 21,
    'rgb(255, 128, 128)': 22,
    'rgb(0, 102, 204)': 23,
    'rgb(204, 204, 255)': 24,
    'rgb(0, 0, 128)': 25,
    'rgb(255, 0, 255)': 26,
    'rgb(255, 255, 0)': 27,
    'rgb(0, 255, 255)': 28,
    'rgb(128, 0, 128)': 29,
    'rgb(128, 0, 0)': 30,
    'rgb(0, 128, 128)': 31,
    'rgb(0, 0, 255)': 32,
    'rgb(0, 204, 255)': 33,
    'rgb(204, 255, 255)': 34,
    'rgb(204, 255, 204)': 35,
    'rgb(255, 255, 153)': 36,
    'rgb(153, 204, 255)': 37,
    'rgb(255, 153, 204)': 38,
    'rgb(204, 153, 255)': 39,
    'rgb(255, 204, 153)': 40,
    'rgb(51, 102, 255)': 41,
    'rgb(51, 204, 204)': 42,
    'rgb(153, 204, 0)': 43,
    'rgb(255, 204, 0)': 44,
    'rgb(255, 153, 0)': 45,
    'rgb(255, 102, 0)': 46,
    'rgb(102, 102, 153)': 47,
    'rgb(150, 150, 150)': 48,
    'rgb(0, 51, 102)': 49,
    'rgb(51, 153, 102)': 50,
    'rgb(0, 51, 0)': 51,
    'rgb(51, 51, 0)': 52,
    'rgb(153, 51, 0)': 53,
    'rgb(153, 51, 102)': 54,
    'rgb(51, 51, 153)': 55,
    'rgb(51, 51, 51)': 56,
}


def render_excel(col_title_list, data_row_list, filename, write_to_file=False, style=False):
    export_wb = Workbook()
    export_sheet = export_wb.add_sheet('Export')
    col_idx = 0
    for col_title in col_title_list:
        export_sheet.write(0, long(col_idx), col_title)
        col_idx += 1
    row_idx = 1
    for row_item_list in data_row_list:
        col_idx = 0
        for current_cell in row_item_list:
            if style:
                current_value = current_cell['value']
                cell_style = current_cell['style']
            if current_value:
                if isinstance(current_value, datetime.datetime):
                    current_value = xlrd.xldate.xldate_from_datetime_tuple(
                        (
                            current_value.year, current_value.month,
                            current_value.day, current_value.hour,
                            current_value.minute, current_value.second
                        ),
                        0
                    )
                elif isinstance(current_value, datetime.date):
                    current_value = xlrd.xldate.xldate_from_date_tuple(
                        (
                            current_value.year,
                            current_value.month,
                            current_value.day
                        ),
                        0
                    )
                elif isinstance(current_value, datetime.time):
                    current_value = xlrd.xldate.xldate_from_time_tuple(
                        (
                            current_value.hour,
                            current_value.minute,
                            current_value.second
                        )
                    )
                elif isinstance(current_value, models.Model):
                    current_value = str(current_value)

                if type(current_value) == str and current_value.startswith('formula-'):
                    export_sheet.write(long(row_idx), long(col_idx), Formula(current_value.replace('formula-', '')))
                else:
                    try:
                        custom_xf = css2excel(cell_style)
                        export_sheet.write(long(row_idx), long(col_idx), current_value, custom_xf)
                    except:
                        print "Warning: Cannot parse cell value", current_cell
                        style = XFStyle()
                        style.num_format_str = 'M/D/YY'
                        export_sheet.write(long(row_idx), long(col_idx), current_value, style)
            col_idx += 1
        row_idx += 1
    if write_to_file:
        export_wb.save(filename)
        return filename
    else:
        output = StringIO.StringIO()
        export_wb.save(output)
        output.seek(0)
        response = HttpResponse(output.getvalue())
        response['Content-Type'] = 'application/vnd.ms-excel'
        response['Content-Disposition'] = 'attachment; filename=%s' % filename
        return response


def css2excel(css):
    style = XFStyle()
    style.num_format_str = 'M/D/YY'
    if css:
        fnt = Font()
        borders = Borders()
        pattern = Pattern()
        align = Alignment()
        alignment_dict = {
            'left': align.HORZ_LEFT,
            'right': align.HORZ_RIGHT,
            'center': align.HORZ_CENTER,
            'justified': align.HORZ_JUSTIFIED,
        }

        process_css = {
            'font-family': [fnt, "name", lambda x: x.split(",")[0]],
            'color': [fnt, "colour_index", lambda x: EXCEL_COLOR_MAP.get(x, 0) + 8],
            'font-weight': [fnt, "bold", lambda x: x.upper() == 'BOLD'],
            'text-align': [align, "horz", lambda x: alignment_dict[x]],
            'background-color': [pattern, "pattern_fore_colour", lambda x: EXCEL_COLOR_MAP.get(x, 16) + 8],
        }
        # TODO process_css -> css
        for i in process_css.keys():
            setattr(process_css[i][0], process_css[i][1], process_css[i][2](css[i]))

        style.font = fnt
        borders.left = Borders.THIN
        borders.right = Borders.THIN
        borders.top = Borders.THIN
        borders.bottom = Borders.THIN
        style.borders = borders
        style.pattern = pattern
        style.pattern.pattern = 1
        style.alignment = align

    return style


class XlError(Exception):
    def __init__(self, value):
        self.value = value

    def __unicode__(self):
        return repr(self.value)


class Xl2Python(object):
    """
    Usage:
    class Uploader(Xl2Python):
        pass

        def clean_mobile(self, val):
            if len(val) < 10:
                raise XlError("Less than 10 characters")
            if len(val) > 10:
                return str(int(val[-10:]))
            else:
                return str(int(val))

    # View
    def upload_view(request):
        upload_field_map = [
            'agent_id',
            ('app_id', 'float'),     # Cast into float field
            'fsc_bank_name',
            'fsc_branch_name',
            'fsc_source_code',
            None,                   # Not Needed
            'jet_decision',
            'product_code',
            ('trigger_date', 'datetime','%Y-%m-%d %H:%M:%S'),   # Datetime field
            ('closure_date', 'date', '%Y/%m/%d'),               # Date field
        ]

        xlm = Uploader(upload_field_map, request.FILES['ufile'])
        if xlm.is_valid():
            print xlm.cleaned_data
        else:
            print xlm.errors

    Supported Types : datetime, date, float, int
    To add more supported types, just keep adding parameters on my one, and passing it to field_<supported_type> method
    Write your clean_<column> methods to clean a column, return the modified value
    Write your clean_row method(self, row_data) to clean each row after all columns are cleaned,
    must return modified row_data

    """

    @property
    def nrows(self):
        if self.dtype == "XLS":
            return self.import_sheet.nrows
        if self.dtype == "CSV":
            return len(self.import_sheet)

    @property
    def ncols(self):
        if self.dtype == "XLS":
            return self.import_sheet.ncols
        if self.dtype == "CSV":
            return len(self.import_sheet[0])

    def cell_data(self, i, j):
        if self.dtype == "XLS":
            return self.import_sheet.cell_value(rowx=i, colx=j)
        if self.dtype == "CSV":
            return self.import_sheet[i][j]

    def __init__(self, field_map, file_handle, dtype="XLS", delimiter='|', quotechar=None):
        self.dtype = dtype
        if self.dtype == "XLS":
            self.import_wb = xlrd.open_workbook(file_contents=file_handle.read())
            self.import_sheet = self.import_wb.sheet_by_index(0)
        if self.dtype == "CSV":
            self.import_sheet = []
            sp = csv.reader(
                file_handle,
                delimiter='|',
                quotechar='\x07',
                lineterminator='\n',
                doublequote=False,
                skipinitialspace=False,
                quoting=csv.QUOTE_NONE,
                escapechar='\\'
            )
            for r in sp:
                print r
                self.import_sheet.append(r)
            print self.import_sheet

        self.field_map = field_map
        self.cleaned_data = []
        self.errors = []

    def is_valid(self):
        for rx in range(1, self.nrows):
            if not self.cell_data(rx, 0):
                continue
            try:
                object_value_dict = {}
                for cx in [i for i in range(0, self.ncols) if self.field_map[i]]:
                    cell_value = self.cell_data(rx, cx)

                    fvalue = self.field_map[cx]
                    if type(fvalue) == tuple:
                        fname = fvalue[0]
                        field_type = fvalue[1]
                        params = fvalue[2:]
                        field_cleaner = getattr(self, 'field_%s' % field_type)
                        cell_value = field_cleaner(cell_value, *params)
                    else:
                        fname = fvalue

                    cleaner = getattr(self, 'clean_%s' % fname, None)
                    if cleaner:
                        cell_value = cleaner(cell_value)

                    object_value_dict[fname] = cell_value

                row_cleaner = getattr(self, 'clean_row', None)
                if row_cleaner:
                    object_value_dict = row_cleaner(object_value_dict.copy())

                self.cleaned_data.append(object_value_dict)

            except Exception, e:
                self.errors.append({'row': rx, 'column': cx, 'error': str(e)})

        if self.errors:
            return False
        else:
            return True

    def field_datetime(self, val, format):
        if type(val) == float:
            return datetime.datetime(*xlrd.xldate_as_tuple(val, self.import_wb.datemode))
        else:
            return datetime.datetime.fromtimestamp(time.mktime(time.strptime(val, format)))

    def field_date(self, val, format):
        if type(val) == float:
            return datetime.datetime(*xlrd.xldate_as_tuple(val, self.import_wb.datemode)).date()
        else:
            return datetime.datetime.fromtimestamp(time.mktime(time.strptime(val, format))).date()

    def field_float(self, val):
        return float(val)

    def field_int(self, val):
        return int(val)
