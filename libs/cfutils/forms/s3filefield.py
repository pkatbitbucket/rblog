from django.forms import FileField


class S3EnabledFileField(FileField):

    def __init__(
        self,
        pattern,
        pattern_values,
        *args,
        **kwargs
    ):
        super(S3EnabledFileField, self).__init__(*args, **kwargs)
