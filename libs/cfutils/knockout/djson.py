from django.db import models
import django.db.models.options as options

options.DEFAULT_NAMES = options.DEFAULT_NAMES + ('djson_exclude', 'djson_extra_callables')


class Djson(object):

    def djson(self, instance=None):
        # Create instance of Djson to do monkey patch with it.
        djson_instance = Djson()
        if instance:
            self = instance

        serialized = {}
        if getattr(self._meta, 'djson_exclude', None):
            exclude_fields = self._meta.djson_exclude
        else:
            exclude_fields = []
        # directly related fields
        for field in self._meta.fields:
            if field.name in exclude_fields:
                continue

            fname = field.name
            if field.name == 'id':
                fname = 'pk'

            if field.related_model:
                if getattr(self, fname):
                    # Monkey patch method djson if not exist.
                    if not hasattr(getattr(self, fname), "djson"):
                        getattr(self, fname).djson = djson_instance.djson

                    serialized[fname] = getattr(self, fname).djson(getattr(self, fname))
                else:
                    serialized[fname] = None
            else:
                if getattr(self, fname):
                    if field.__class__ is models.DateTimeField:
                        val = getattr(self, fname).isoformat()

                    elif field.__class__ is models.FileField:
                        val = {'url': getattr(self, fname).url, 'name': getattr(self, fname).name}

                    elif field.__class__ is models.DecimalField:
                        val = str(getattr(self, fname))

                    else:
                        val = getattr(self, fname)

                else:
                    if field.__class__ is models.BooleanField:
                        val = False
                    else:
                        val = None
                serialized[fname] = val

        # many to many related fields
        for field, _ in self._meta.get_m2m_with_model():
            if field.name in exclude_fields:
                continue

            serialized[field.name] = []
            for item in getattr(self, field.name).all():
                if not hasattr(item, "djson"):
                    item.djson = djson_instance.djson

                serialized[field.name].append(item.djson(item))

        if getattr(self._meta, 'djson_extra_callables', None):
            extra_callables = self._meta.djson_extra_callables
        else:
            extra_callables = ['__unicode__']

        for c in extra_callables:
            serialized[c] = getattr(self, c)()

        return serialized
