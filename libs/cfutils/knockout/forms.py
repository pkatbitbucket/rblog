from django import template
from django.template import loader
from django import forms
from django.utils.safestring import mark_safe
import json


class KnockoutForm(forms.Form):

    def knockoutize(self, *args, **kwargs):
        for fname in self.fields.keys():
            if getattr(self.fields[fname], 'choices', None):
                self.fields[fname].widget.attrs.update({'data-bind': 'checked: %s' % fname})
            else:
                self.fields[fname].widget.attrs.update({'data-bind': 'value: %s' % fname})

    def get_form(self):
        rendered_form = loader.render_to_string(
            self.Meta.template, {
                'form': self
            }
        )
        return rendered_form

    def as_html(self):
        html = """<div class="row m-0 p-20" data-bind="css: { 'has-error': error }">"""
        for field_name, field in self.fields.items():
            fname = field.__class__.__name__
            html += getattr(self, "field_%s" % fname)(field_name, field)

        html += """
            <div class="m-l-10 row error alert-danger" data-bind="text: errors.__all__()"></div>
        </div>"""
        return mark_safe(html)

    def field_IntegerField(self, field_name, field):
        template_string = """
            <div class="form-group fg-float">
                <div class="fg-line" data-bind="css: {'fg-toggled': {{field_name}} }">
                    <input type="number" class="form-control" data-bind="value: {{field_name}}" type="text"/>
                </div>
                <label class="fg-label">{{verbose_name|title}}</label>
                <small class="help-block" data-bind="text: errors.{{field_name}}()"></small>
            </div>
        """
        temp = template.Template(template_string)
        context = template.Context({
            'field_name': field_name,
            'verbose_name': field.label
        })
        return temp.render(context)

    def field_CharField(self, field_name, field):
        return getattr(self, "widget_%s" % field.widget.__class__.__name__)(field_name, field)

    def widget_TextArea(self, field_name, field):
        template_string = """
            <div class="form-group fg-float">
                <div class="fg-line">
                    <textarea class="form-control" data-bind="value: {{field_name}}"></textarea>
                </div>
                <label class="fg-label">{{verbose_name|title}}</label>
                <small class="help-block" data-bind="text: errors.{{field_name}}()"></small>
            </div>
        """
        temp = template.Template(template_string)
        context = template.Context({
            'field_name': field_name,
            'verbose_name': field.label
        })
        return temp.render(context)

    def widget_TextInput(self, field_name, field):
        template_string = """
            <div class="form-group fg-float">
                <div class="fg-line" data-bind="css: {'fg-toggled': {{field_name}} }">
                    <input class="form-control" data-bind="value: {{field_name}}" type="text"/>
                </div>
                <label class="fg-label">{{verbose_name|title}}</label>
                <small class="help-block" data-bind="text: errors.{{field_name}}()"></small>
            </div>
        """
        temp = template.Template(template_string)
        context = template.Context({
            'field_name': field_name,
            'verbose_name': field.label
        })
        return temp.render(context)

    def field_BooleanField(self, field_name, field):
        template_string = """
            <div class="checkbox">
                <label>
                    <input type="checkbox" data-bind="checked: {{field_name}}">
                    <i class="input-helper"></i>
                    {{verbose_name}}
                </label>
            </div>
        """
        temp = template.Template(template_string)
        context = template.Context({
            'field_name': field_name,
            'verbose_name': field.label
        })
        return temp.render(context)

    def field_ModelMultipleChoiceField(self, field_name, field):
        js_model = self.Meta.field_jsmodel.get(field_name)
        template_string = """
            <div class="form-group fg-float">
                <label>{{verbose_name|title}}</label>
                <span class="fg-line" data-bind="foreach: $root.{{js_model}}View.{{js_model}}List">
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" data-bind="checkedValue: $data, checked: $parent.{{field_name}}" />
                        <i class="input-helper"></i><span data-bind="text: __unicode__"></span>
                    </label>
                </span>
                <small class="help-block" data-bind="text: errors.{{field_name}}()"></small>
            </div>
        """
        temp = template.Template(template_string)
        context = template.Context({
            'field_name': field_name,
            'js_model': js_model,
            'verbose_name': field.label
        })
        return mark_safe(temp.render(context))

    def field_ModelChoiceField(self, field_name, field):
        js_model = self.Meta.field_jsmodel.get(field_name)
        template_string = """
            <div class="form-group fg-float">
                <div class="fg-line">
                    <div class="select">
                        <select class="form-control" data-bind="options: $root.{{js_model}}View.{{js_model}}List,
                            optionsText: function(item) {
                                return item.__unicode__
                            },
                            value: {{field_name}},
                            optionsCaption: '{{verbose_name|title}}'
                        ">
                        </label>
                        </select>
                    </div>
                </div>
                <small class="help-block" data-bind="text: errors.{{field_name}}()"></small>
            </div>
        """
        temp = template.Template(template_string)
        context = template.Context({
            'field_name': field_name,
            'js_model': js_model,
            'verbose_name': field.label
        })
        return temp.render(context)

    def field_ChoiceField(self, field_name, field):
        choices = json.dumps([{'value': c[0], 'verbose': c[1]} for c in field.choices])
        template_string = """
            <div class="form-group fg-float">
                <div class="fg-line">
                    <div class="select">
                        <select class="form-control" data-bind="options: {{choices}},
                            optionsText: 'verbose',
                            optionsValue: 'value',
                            value: {{field_name}},
                            optionsCaption: '{{verbose_name|title}}'
                        ">
                        </label>
                        </select>
                    </div>
                </div>
                <small class="help-block" data-bind="text: errors.{{field_name}}()"></small>
            </div>
        """
        temp = template.Template(template_string)
        context = template.Context({
            'field_name': field_name,
            'verbose_name': field.label,
            'choices': choices
        })
        return temp.render(context)

    def field_MultipleChoiceField(self, field_name, field):
        choices = json.dumps([{'value': c[0], 'verbose': c[1]} for c in field.choices])
        template_string = """
            <div class="form-group fg-float">
                <label>{{verbose_name|title}}</label>
                <span class="fg-line" data-bind="foreach: {{choices}}">
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" data-bind="checkedValue: value, checked: $parent.{{field_name}}" />
                        <i class="input-helper"></i><span data-bind="text: verbose"></span>
                    </label>
                </span>
                <small class="help-block" data-bind="text: errors.{{field_name}}()"></small>
            </div>
        """
        temp = template.Template(template_string)
        context = template.Context({
            'field_name': field_name,
            'verbose_name': field.label,
            'choices': choices
        })
        return temp.render(context)

    def field_DateField(self, field_name, field):
        return self.field_CharField(field)

    def field_TimeField(self, field_name, field):
        return self.field_CharField(field_name, field)

    def field_SlugField(self, field_name, field):
        return self.field_CharField(field_name, field)

    def field_DateTimeField(self, field_name, field):
        return self.field_CharField(field_name, field)

    def field_EmailField(self, field_name, field):
        return self.field_CharField(field_name, field)

    def field_URLField(self, field_name, field):
        return self.field_CharField(field_name, field)

    def field_ComboField(self, field_name, field):
        return self.field_CharField(field_name, field)

    def field_FloatField(self, field_name, field):
        return self.field_IntegerField(field_name, field)

    def jsmodel(self):
        fields = []
        field_jsmodel = self.Meta.field_jsmodel

        for fname, f in self.fields.items():
            multiple = False

            if f.__class__.__name__ in ['ModelMultipleChoiceField', 'MultipleChoiceField']:
                multiple = True
            fields.append([fname, multiple, field_jsmodel.get(fname)])

        model_name = field_jsmodel['self']
        rest_url = self.Meta.rest_url

        template_string = """

        function {{model_name}}(data) {
            var self = this;
            self.pk = null;
            self.errors = {};
            self.errors['__all__'] = ko.observable();
            self.__unicode__ = ko.observable();
            {% for fieldName, multiple, jsmodel in fields %}
                {% if multiple %}
                    self.{{fieldName}} = ko.observableArray();
                {% else %}
                    self.{{fieldName}} = ko.observable();
                {% endif %}
                self.errors.{{fieldName}} = ko.observable();
            {% endfor %}

            self.update = function(data) {
                self.pk = data.pk;
                self.__unicode__(data.__unicode__);
                {% for fieldName, multiple, jsmodel in fields %}
                    {% if multiple %}
                    if(data.{{fieldName}}) {
                        {% if jsmodel %}
                        self.{{fieldName}}([])
                        for(var i=0; i< data.{{fieldName}}.length; i++) {
                            item = {{jsmodel}}View.get_by_pk(data.{{fieldName}}[i].pk)
                            if(item){
                                self.{{fieldName}}.push(item);
                            }
                        }

                        {% else %}
                            self.{{fieldName}}(data.{{fieldName}});
                        {% endif %}
                    }
                    {% else %}
                    if(data.{{fieldName}}) {
                        {% if jsmodel %}
                            self.{{fieldName}}({{jsmodel}}View.get_by_pk(data.{{fieldName}}.pk));
                        {% else %}
                            self.{{fieldName}}(data.{{fieldName}});
                        {% endif %}
                    }
                    {% endif %}
                {% endfor %}
                self._data = data;
            }
            if(data) {
                self.update(data);
            }

            self.rest_url = '{{rest_url}}';

            self.is_selected = ko.observable(false);
            self.is_editing = ko.observable(false);
            self.is_updating = ko.observable(false);
            self.error = ko.observable(false);

            var mapping = {
                'ignore': ["is_updating", "is_editing", "is_selected", "error", "rest_url", "__ko_mapping__"]
            }
            self.set_data = function (data) {
                self.update(data);
                self.error(false);
                for(var f in self.errors) {
                    self.errors[f](null);
                }
                self.errors['__all__'](null);
            };
            self.set_errors = function (data) {
                for(var f in data.errors) {
                    self.errors[f](data.errors[f]);
                }
                self.error(true);

            };
            self.getJSON = function () {
                return ko.mapping.toJSON(self, mapping);
            };
            self.getJS = function () {
                return JSON.parse(self.getJSON());
            };
        }
        {{model_name}}.prototype = new Router();
        {{model_name}}.LIST = function(callback){
            _router = new _Router();
            _router.LIST("{{rest_url}}", false, callback);
        }
        {{model_name}}.model_name = '{{model_name}}';
        function {{model_name}}ViewModel() {
            var self = this;
            self.{{model_name|lower}}List = ko.observableArray();
            self.selected{{model_name}} = ko.observable();

            self.new{{model_name}} = function() {
                {{model_name|lower}}View.selected{{model_name}}(new {{model_name}}());
            }

            self.save{{model_name}} = function() {
                var {{model_name|lower}} = {{model_name|lower}}View.selected{{model_name}}();
                if({{model_name|lower}}.pk) {
                    {{model_name|lower}}.PUT(function(){
                        {{model_name|lower}}View.selected{{model_name}}(null);
                    });
                } else {
                    {{model_name|lower}}.CREATE(function(){
                        {{model_name|lower}}View.add{{model_name}}({{model_name|lower}});
                        {{model_name|lower}}View.selected{{model_name}}(null);
                    });
                }
            }
            self.delete{{model_name}} = function({{model_name|lower}}){
                if(confirm('Are you sure you want to delete this {{model_name}}?')){
                    {{model_name|lower}}.DELETE();
                    {{model_name|lower}}View.remove{{model_name}}({{model_name|lower}});
                    {{model_name|lower}}View.selected{{model_name}}(null);
                }
            }

            self.get_by_pk = function(pk) {
                var obj = null;
                for(var i=0;i < self.{{model_name|lower}}List().length; i++){
                    if(self.{{model_name|lower}}List()[i].pk == pk){
                        obj = self.{{model_name|lower}}List()[i];
                        break;
                    }
                }
                return obj
            }

            self.add{{model_name}} = function({{model_name|lower}}) {
                self.{{model_name|lower}}List.push({{model_name|lower}});
            };
            self.remove{{model_name}} = function({{model_name|lower}}){
                self.{{model_name|lower}}List.remove({{model_name|lower}})
            };
        }
        var {{model_name|lower}}View = new {{model_name}}ViewModel();

        """
        temp = template.Template(template_string)
        context = template.Context({
            'model_name': model_name,
            'fields': fields,
            'rest_url': rest_url,
        })
        return temp.render(context)
