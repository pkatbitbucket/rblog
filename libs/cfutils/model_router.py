import importlib


class RouterUtils(object):
    """Utils to get Router.
    This is a general purpose routing utility which uses boolean functions (conditions)
    return a router from a list of routers.
    """

    def __init__(self, app_name):
        """
        Args:
            app_name (string): Name of the app which contains the router and object_model
        """
        self.app_name = app_name

    def get_matching_router(self, router_queryset, rdata):
        """Return matching router based on params passed during initialization.
        Args:
            router_queryset (queryset): router queryset
            rdata (dictionary): data used for query matching .This is passed as kwargs
                to the condition functions
        """
        for router in router_queryset.filter(conditions__isnull=False).order_by('-score'):
            if self._is_match(router, rdata, router_queryset):
                return router
        raise ValueError("Matching router doesn't exist")

    def _is_match(self, router, rdata, router_queryset):
        for condition in router.conditions.all():
            match_func = self._get_func(condition)
            if not match_func(**rdata):
                return False
        return True

    def _get_func(self, condition):
        i = importlib.import_module(condition.module)
        return getattr(i, condition.func_name)
