import copy
import json
import re
import time
from urlparse import urljoin

import requests
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.core.serializers.json import DjangoJSONEncoder
from requests import request

if settings.NUDGESPOT:
    API_KEY = '16f27c73328b02d310d3cda5a88c3092'  # live account
else:
    API_KEY = 'f79d9dc779fd802aa38a191ac705b439'  # test account

AUTH = ('api', API_KEY)
BASE_URL = 'https://api.nudgespot.com/201507/'
HEADERS = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
}

email_re = re.compile(
    r'([A-Z0-9]+([+._-]?[A-Z0-9]+)*)'
    r'@'
    r'((?:[A-Z0-9-]+\.)*\w[A-Z0-9-]{0,66})\.([A-Z]{2,6}(?:\.[A-Z]{2})?)',
    re.IGNORECASE
)


class NudgespotException(BaseException):
    pass


class Nudge(object):
    _recent_request = None
    success_codes = [200, 201, 202]

    def __init__(self, _id):
        """
        Initialises with the id provided else sets empty string
        :param _id: id of the user for nudgespot identification
        :type _id: str
        """
        self.id = _id

    def get_user_request(self):
        """
        :rtype : request object
        """
        data = {'uid': self.id}
        return self._send_request(method='GET',
                                  url=BASE_URL + 'subscribers',
                                  auth=AUTH,
                                  headers=HEADERS,
                                  params=data)

    def get_user_data(self):
        """
        Gets the user data from nudgespot and returns in json
        :return: user data
        :rtype : json dictionary
        """
        if self.exists():
            return self._recent_request.json()
        return {}

    def exists(self):
        """
        Validation check of the user
        :rtype : bool
        """
        response = self.get_user_request()
        if response.status_code == 200:
            return True
        elif response.status_code == 404:
            return False
        else:
            self._raise_response_error()

    def create_user(self, contacts=None, properties=None):
        """
        Creates the user with the id given while initialising. Takes optional dictionary of contacts
        :param contacts: dictionary with contact_type and contact_value as the keys and contact info as values
        takes optional subscription_status key
        :type contacts: list
        :type properties: dict
        :return: :raise ValueError: if user already exists
        :rtype : bool
        """
        user_data = self._build_user_data(contacts=contacts, properties=properties)
        if user_data:
            response = self._send_request(method='POST',
                                          url=urljoin(BASE_URL, 'subscribers'),
                                          auth=AUTH,
                                          headers=HEADERS,
                                          data=user_data)
            if response.status_code in self.success_codes:
                return True
            elif response.status_code == 409:
                raise ValueError('User already exists')
            else:
                self._raise_response_error()
        else:
            raise ValueError('There is no proper data to create the user')

    def update_user(self, contacts=None, properties=None):
        """
        Creates the user with the id given while initialising. Takes optional dictionary of contacts
        :param contacts: dictionary with contact_type and contact_value as the keys and contact info as values
        takes optional subscription_status key
        :type contacts: list
        :type properties: dict
        :return: :raise ValueError: if user already exists
        :rtype : bool
        """
        user_data = self._build_user_data(contacts=contacts, properties=properties)
        if user_data:
            response = self._send_request(method='PUT',
                                          url=urljoin(BASE_URL, 'subscribers/{}'.format(self.id)),
                                          auth=AUTH,
                                          headers=HEADERS,
                                          data=user_data)
            if response.status_code in self.success_codes:
                return True
            elif response.status_code == 404:
                raise ValueError('User does not exists')
            else:
                self._raise_response_error()
        else:
            raise ValueError('There is no proper data to update the user')

    def update_or_create_user(self, contacts=None, properties=None):
        """
        Creates the user with the id given while initialising. Takes optional dictionary of contacts
        :param contacts: dictionary with contact_type and contact_value as the keys and contact info as values
        takes optional subscription_status key
        :type contacts: list
        :type properties: dict
        :return: :raise ValueError: if user already exists
        :rtype : bool
        """
        user_data = self._build_user_data(contacts=contacts, properties=properties)
        if user_data:
            response = self._send_request(method='POST',
                                          url=urljoin(BASE_URL, 'subscribers/identify'),
                                          auth=AUTH,
                                          headers=HEADERS,
                                          data=user_data)
            if response.status_code in self.success_codes:
                return True
            else:
                self._raise_response_error()
        else:
            raise ValueError('There is no proper data to update/create the user')

    def send_event(self, event_name, event_data={}):
        """
        Pushes the event to nudgespot
        :param event_name: name of the event. should follow the rules of variable naming in python
        :type event_name: str
        :param event_data: optional dictionary of event data
        :type event_data: dict
        :rtype : bool
        """
        phone = str(event_data.pop('mobile', ''))
        email = str(event_data.pop('email', ''))

        if phone or email:
            try:
                self.subscribe(**{phone: 'phone', email: 'email'})
            except:
                pass

        event_data['communication'] = event_data.get('communication', 'sms')

        event_name.replace(' ', '_')
        timestamp = event_data.pop('timestamp', '')
        data = {
            'activity': {
                'subscriber': {
                    'uid': self.id,
                },
                'event': event_name,
                'properties': event_data,
            },
        }

        if timestamp:
            data['activity']['timestamp'] = timestamp

        data = json.dumps(data, cls=DjangoJSONEncoder)
        response = self._send_request(method='POST',
                                      url=urljoin(BASE_URL, 'activities'),
                                      auth=AUTH,
                                      headers=HEADERS,
                                      data=data)

        if response.status_code in self.success_codes:
            return True
        else:
            self._raise_response_error()

    def unsubscribe(self, **kwargs):
        """
        Unsubscribe the contacts of a user
        :rtype : bool or None
        :param kwargs: dictionary with contact value as key and contact type as value
        :type kwargs: dict
        """
        dum = [{'contact_value': value,
                'contact_type': _type,
                'subscription_status': 'inactive'} for value, _type in kwargs.items()]
        contacts = dum

        for contact in dum:
            if not any(contact['contact_value'] in contact_data['contact_value'] for contact_data in
                       self.get_user_data()['contact']):
                contacts.remove(contact)

        if contacts:
            return self.update_or_create_user(contacts=contacts)

    def unsubscribe_mobiles(self):
        mobiles = {contact_data['contact_value']: 'phone' for contact_data in self.get_user_data()['contact'] if
                   contact_data['contact_type'] == 'phone'}
        return self.unsubscribe(**mobiles)

    def unsubscribe_emails(self):
        emails = {contact_data['contact_value']: 'email' for contact_data in self.get_user_data()['contact'] if
                  contact_data['contact_type'] == 'email'}
        return self.unsubscribe(**emails)

    def unsubscribe_all(self):
        """
        Unsubscribe all the contacts of a user
        :rtype : bool or None
        """
        contacts = {contact_data['contact_value']: contact_data['contact_type'] for contact_data in
                    self.get_user_data()['contact']}
        return self.unsubscribe(**contacts)

    def subscribe(self, **kwargs):
        """
        Subscribe the contacts of a user
        :param kwargs: dictionary with contact value as key and contact type as value
        :type kwargs: dict
        :rtype : bool or None
        """
        contacts = [{'contact_value': value,
                     'contact_type': _type,
                     'subscription_status': 'active'} for value, _type in kwargs.items()]
        if contacts:
            return self.update_or_create_user(contacts=contacts)

    def subscribe_mobiles(self):
        mobiles = {contact_data['contact_value']: 'phone' for contact_data in self.get_user_data()['contact'] if
                   contact_data['contact_type'] == 'phone'}
        return self.subscribe(**mobiles)

    def subscribe_emails(self):
        emails = {contact_data['contact_value']: 'email' for contact_data in self.get_user_data()['contact'] if
                  contact_data['contact_type'] == 'email'}
        return self.subscribe(**emails)

    def subscribe_all(self):
        """
        Subscribe all the contacts of a user
        :rtype : bool or None
        """
        contacts = {contact_data['contact_value']: contact_data['contact_type'] for contact_data in
                    self.get_user_data()['contact']}
        return self.subscribe(**contacts)

    @staticmethod
    def is_email(email):
        """
        Validation check for email
        :param email: email id
        :type email: str
        :rtype : bool
        """
        try:
            validate_email(email)
        except ValidationError:
            return False
        else:
            return True

    @staticmethod
    def is_phone(phone):
        # ToDo: Currently validates for indian number. Need to generalise for international numbers
        """
        Validation check for mobile
        :param phone: phone number with or with country code
        :type phone: int
        :rtype : bool
        """
        if re.match(r'^\+?\d{0,4}[7-9]\d{9}$', str(phone)):
            return True
        else:
            return False

    @staticmethod
    def normalize_mobile(mobile):
        # ToDo: Currently normalizes to indian number. Need to generalise for international numbers
        """
        Normalise the given mobile number
        :rtype : str | None
        :param mobile: phone number with or with country code
        :type mobile: int
        """
        mobile = str(mobile)
        if 9 < len(mobile) <= 15:
            if re.match(r'^\+?\d{0,4}[7-9]\d{9}$', mobile):
                return '+91{}'.format(re.search('([7-9]\d{9}$)', mobile).group())
            else:
                return None
        else:
            return None

    @staticmethod
    def normalize_email(email):
        """
        Normalise the given email
        :return: first match of an email in the string
        :rtype : str | None
        :param email: string containing email
        :type email: str
        """
        s = email_re.search(email)
        if s:
            return s.group()

    @classmethod
    def normalize_contact(cls, contact, contact_type):
        if contact_type == 'phone':
            return cls.normalize_mobile(contact)
        if contact_type == 'email':
            return cls.normalize_email(contact)
        raise ValueError('{} is not a valid contact type'.format(contact_type))

    def _build_user_data(self, contacts=None, properties=None):
        if contacts is None and properties is None:
            raise ValueError('Need something to create/update the user')

        if contacts is None:
            contacts = []

        if properties is None:
            properties = {}

        main_properties = [
            'first_name',
            'last_name',
            'name'
        ]

        data = {
            'subscriber': {
                'uid': self.id,
                'contact': [],
                'properties': {},
            }
        }

        initial_data = copy.deepcopy(data)

        for contact_data in contacts:
            contact_value = contact_data['contact_value']
            contact_type = contact_data['contact_type']
            contact_value = self.normalize_contact(contact_value, contact_type)
            subscription_status = contact_data.get('subscription_status', 'active')

            if contact_value:
                if getattr(self, 'is_{}'.format(contact_type))(str(contact_value)):
                    contact_data = {
                        'contact_type': contact_type,
                        'contact_value': contact_value,
                        'subscription_status': subscription_status,
                    }
                    data['subscriber']['contact'].append(contact_data)
                    prop = 'contact_{}'.format(contact_type)
                    properties[prop] = contact_value
                else:
                    raise ValueError('{} is not a valid contact'.format(contact_value))

        for prop in properties:
            if prop in main_properties:
                data['subscriber'][prop] = properties[prop]
            else:
                data['subscriber']['properties'][prop] = properties[prop]

        if initial_data != data:
            return json.dumps(data, cls=DjangoJSONEncoder)

    def _raise_response_error(self):
        raise NudgespotException('Unknown response received from nudgespot.\n'
                                 'Status Code: {}\nError: {}'.format(self._recent_request.status_code,
                                                                     self._recent_request.text))

    def _send_request(self, retries=3, **kwargs):
        """
        Prepares and send the request object. Retires specified number of times before raising an exception.
        :param retries: number of retires to be made before raising an exception
        :type retries: int
        :param kwargs: dictionary of parameters to be passed for request
        :rtype : request object
        """
        retries -= 1
        # print(request_type, kwargs)
        try:
            if not retries < 0:
                _request = request(**kwargs)
                if 500 <= _request.status_code <= 599:
                    raise NudgespotException(
                        '''Error raised by nudgespot.
                        Status Code: {}
                        Error: {}'''.format(_request.status_code, _request.text)
                    )
                self._recent_request = _request
                return _request
            raise NudgespotException('Unable to send request to Nudgespot')
        except requests.ConnectionError:
            # print('Connection error occurred. Retrying in 3 seconds')
            time.sleep(3)
            return self._send_request(retries, **kwargs)
        except requests.Timeout:
            # print('Connection timed out. Retrying in 3 seconds')
            time.sleep(3)
            return self._send_request(retries, **kwargs)
        except Exception as e:
            raise RuntimeError(e)


if __name__ == '__main__':
    pass
