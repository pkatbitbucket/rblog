import inspect
import logging

import redis
import json

from django.conf import settings


def tracer(func):
    """
    Decorator to get module and function from where the decorated function is called.
    When used, adds another keyword argument 'called_from' to decorated function's parameters.
    called_from = { 'module': module_name, 'line': line_from_where_called, 'function': function_name }
    """

    def wrapped(*args, **kwargs):
        frame = inspect.getframeinfo(inspect.currentframe().f_back)
        called_from = {
            'module': str(frame[0]),
            'line': str(frame[1]),
            'function': str(frame[2]),
        }
        return func(called_from=called_from, *args, **kwargs)
    return wrapped


class Publisher(object):
    """Publisher to add/update/remove data from redis datastore.
    channel - Channels on which the websocket server listens to for data. Generally it's 'user_<id>' for each user.
    command - Command sent to websocket server. Can be add/remove/update.
    obj - the object which is added/removed from a channel.
    """
    rclient = redis.Redis(host=settings.JARVIS_REDIS_HOST, port=settings.JARVIS_REDIS_PORT)
    logger = logging.getLogger('redis')

    @classmethod
    def _log(cls, command, channel, data, called_from):
        """Log data pushed to redis along with calling function and data passed."""
        ldata = 'Command- {command}, Channel - {channel}, Data - {data}, Module - {module}, Line - {line}, Function - {function}'
        cls.logger.info(
            ldata
            .format(
                command=command,
                channel=channel,
                data=data,
                module=called_from['module'],
                line=called_from['line'],
                function=called_from['function']
            )
        )

    @classmethod
    @tracer
    def publish_object(cls, state, channel, obj, called_from):
        """Push an object to the given channel, and update <channel_to_object> and <object_to_channel> mapping."""
        obj_class = obj.__class__.__name__.lower()
        command = '%s_%s' % (obj_class, state)
        cls.rclient.publish(channel, json.dumps({'command': command, 'data': obj.id}))
        cls.rclient.zadd('%s_%s_channels' % (obj_class, obj.id), channel, 1)
        cls._log(command, channel, obj.id, called_from)

    @classmethod
    @tracer
    def remove_object(cls, channel, obj, called_from):
        """Remove an object from the given channel, and update <channel_to_object> and <object_to_channel> mapping."""
        obj_class = obj.__class__.__name__.lower()
        command = '%s_remove' % obj_class
        cls.rclient.publish(channel, json.dumps({'command': command, 'data': obj.id}))
        cls.rclient.zrem(channel, '%s_%s' % (obj_class, obj.id))
        cls.rclient.zrem('%s_%s_channels' % (obj_class, obj.id), channel)
        cls._log(command, channel, obj.id, called_from)

    @classmethod
    @tracer
    def update_object(cls, obj, called_from):
        """Update object data in the db store. Also repush to all channels which have subscribed to that object."""
        obj_class = obj.__class__.__name__.lower()
        cls.rclient.set('%s_%s' % (obj_class, obj.id), json.dumps(obj.mini_json()))
        cls._log('%s_update' % obj_class, None, obj.id, called_from)
        # command = '%s_push' % obj_class
        # channels = cls.rclient.zrange('%s_%s_channels' % (obj_class, obj.id), 0, -1)
        # message = json.dumps({'command': command, 'data': obj.id})
        # pipe = cls.rclient.pipeline(transaction=True)
        # pipe.set('%s_%s' % (obj_class, obj.id), json.dumps(obj.mini_json()))
        # FIXME: STOPPED Updating the object in UI for sometime
        # for channel in channels:
        #    pipe.publish(channel, message)
        # print message
        # pipe.execute()

    @classmethod
    @tracer
    def remove_object_from_all(cls, obj, called_from):
        """Remove an object from all channels it has been pushed to, and update respective mappings."""
        obj_class = obj.__class__.__name__.lower()
        command = '%s_remove' % obj_class
        channels = cls.rclient.zrange('%s_%s_channels' % (obj_class, obj.id), 0, -1)

        message = json.dumps({'command': command, 'data': obj.id})
        pipe = cls.rclient.pipeline(transaction=True)
        for channel in channels:
            pipe.publish(channel, message)
            pipe.zrem(channel, '%s_%s' % (obj_class, obj.id))
            pipe.zrem('%s_%s_channels' % (obj_class, obj.id), channel)
        pipe.execute()
        cls._log(command, None, obj.id, called_from)

    @classmethod
    @tracer
    def publish(cls, channel, command, data, called_from):
        cls.rclient.publish(channel, json.dumps({'command': command, 'data': data}))
        cls._log(command, channel, data, called_from)

    @classmethod
    @tracer
    def add_to_list(cls, key, value, called_from):
        if isinstance(value, list):
            cls.rclient.zadd(key, **{i: 1 for i in value})
        else:
            cls.rclient.zadd(key, value, 1)
        cls._log('add_to_list', None, json.dumps({key: value}), called_from)

    @classmethod
    @tracer
    def update_advisor_available_count(cls, advisor_id, is_busy, queue, called_from):
        """Updates the available count in queue."""
        command = 'update_available_count'
        if is_busy:
            cls.rclient.lrem('available_%s' % queue, advisor_id)
        else:
            cls.rclient.rpush('available_%s' % queue, advisor_id)
        cls.rclient.publish('system', json.dumps({'command': command, 'data': queue}))
        cls._log(
            command, 'system',
            json.dumps({'queue': queue, 'users': cls.rclient.lrange('available_%s' % queue, 0, -1)}),
            called_from
        )
