import six
import functools
import json

from django.conf import settings
from django.conf.urls import patterns, url
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.http import QueryDict
from django.http.response import JsonResponse

from .constants import OK
from .exceptions import MethodNotImplemented, Unauthorized
from .exceptions import NotFound
from .resources import Resource


class validate(object):
    def __init__(self, func):
        self.func = func

    def __call__(self, obj, *args, **kwargs):
        # if validation is required and Django form is available, use it for validation
        if not obj.form_cls:
            return Exception("form_cls cannot be None")

        obj.form = obj.form_cls(obj.data)
        if obj.form.is_valid():
            return self.func(obj, *args, **kwargs)
        else:
            return JsonResponse({'success': False, 'errors': obj.form.errors})

    def __get__(self, obj, cls):
        return functools.partial(self.__call__, obj)


class DjangoResource(Resource):
    form_cls = None
    form_template = None
    """
    A Django-specific ``Resource`` subclass.

    Doesn't require any special configuration, but helps when working in a
    Django environment.
    """

    def __init__(self, request, *args, **kwargs):
        super(DjangoResource, self).__init__(request, *args, **kwargs)
        self.form = None

    def is_authenticated(self):
        if self.request_method() == 'GET':
            return True

        if self.request.user.is_authenticated():
            return True

        return False

    def handle(self, endpoint, *args, **kwargs):
        """
        A convenient dispatching method, this centralized some of the common
        flow of the views.

        This wraps/calls the methods the user defines (``list/detail/create``
        etc.), allowing the user to ignore the
        authentication/deserialization/serialization/response & just focus on
        their data/interactions.

        :param endpoint: The style of URI call (typically either ``list`` or
            ``detail``).
        :type endpoint: string

        :param args: (Optional) Any positional URI parameter data is passed
            along here. Somewhat framework/URL-specific.

        :param kwargs: (Optional) Any keyword/named URI parameter data is
            passed along here. Somewhat framework/URL-specific.

        :returns: A response object
        """
        self.endpoint = endpoint
        method = self.request_method()

        # Use ``.get()`` so we can also dodge potentially incorrect
        # ``endpoint`` errors as well.
        if method not in self.http_methods.get(endpoint, {}):
            raise MethodNotImplemented(
                "Unsupported method '{0}' for {1} endpoint.".format(
                    method,
                    endpoint
                )
            )

        if not self.is_authenticated():
            raise Unauthorized()

        self.data = self.request_body()
        if self.form_cls:
            self.form = self.form_cls(self.data)

        view_method = getattr(self, self.http_methods[endpoint][method])
        data = view_method(*args, **kwargs)
        serialized = self.serialize(method, endpoint, data)

        status = self.status_map.get(self.http_methods[endpoint][method], OK)
        return self.build_response(serialized, status=status)

    def request_body(self):
        """ If request is of the format:
        { name: "NEWNAME", permissions: [{'pk': 1, name:"PERM1"}, {'pk': 2, name:"PERM2"}]
        This come when you use jquery.ajax and the post data is a JSON string
        """
        data = {}
        if self.request.body:
            body_data = json.loads(self.request.body)
        else:
            return data

        for k, v in body_data.items():
            if isinstance(v, list):
                data[k] = []
                for item in body_data[k]:
                    if 'pk' in item.keys():
                        data[k].append(item['pk'])
                    else:
                        data[k].append(item)
            elif isinstance(v, dict):
                if 'pk' in v.keys():
                    data[k] = v['pk']
            else:
                data[k] = v

        return data

    def request_form_body(self):
        """ If request is of the format:
        name: "NEWNAME"
        permissions[]: [1,2]
        This come when you use jquery.ajax and the post data is dict, instead
        of a Json string
        """
        data = self.request.POST or self.request.GET or QueryDict(self.request.body)
        qd = data.copy()
        for k in qd.keys():
            if k.endswith('[]'):
                klist = qd.pop(k)
                for kval in klist:
                    qd.appendlist(k[:-2], kval)
        return qd

    def serialize(self, method, endpoint, data):
        """
        A convenience method for serializing data for a response.

        If called on a list-style endpoint, this calls ``serialize_list``.
        Otherwise, it will call ``serialize_detail``.

        :param method: The HTTP method of the current request
        :type method: string

        :param endpoint: The endpoint style (``list`` or ``detail``)
        :type endpoint: string

        :param data: The body for the response
        :type data: string

        :returns: A serialized version of the data
        :rtype: string
        """
        if isinstance(data, HttpResponse):
            return data

        if endpoint == 'list':
            # Create is a special-case, because you POST it to the collection,
            # not to a detail.
            if method == 'POST':
                return self.serialize_detail(data)

            return self.serialize_list(data)
        return self.serialize_detail(data)

    def prepare(self, data):
        """
        Given an item (``object`` or ``dict``), this will potentially go through
        & reshape the output based on ``self.prepare_with`` object.

        :param data: An item to prepare for serialization
        :type data: object or dict

        :returns: A potentially reshaped dict
        :rtype: dict
        """
        if isinstance(data, HttpResponse):
            return data
        return self.preparer.prepare(data)

    # Because Django.
    @classmethod
    def as_list(self, *args, **kwargs):
        return csrf_exempt(super(DjangoResource, self).as_list(*args, **kwargs))

    @classmethod
    def as_detail(self, *args, **kwargs):
        return csrf_exempt(super(DjangoResource, self).as_detail(*args, **kwargs))

    def is_debug(self):
        # By default, Django-esque.
        return settings.DEBUG

    def build_response(self, data, status=200):
        # By default, Django-esque.
        resp = HttpResponse(data, content_type='application/json')
        resp.status_code = status
        return resp

    def build_error(self, err):
        # A bit nicer behavior surrounding things that don't exist.
        if isinstance(err, (ObjectDoesNotExist, Http404)):
            err = NotFound(msg=six.text_type(err))

        return super(DjangoResource, self).build_error(err)

    @classmethod
    def build_url_name(cls, name, name_prefix=None):
        """
        Given a ``name`` & an optional ``name_prefix``, this generates a name
        for a URL.

        :param name: The name for the URL (ex. 'detail')
        :type name: string

        :param name_prefix: (Optional) A prefix for the URL's name (for
            resolving). The default is ``None``, which will autocreate a prefix
            based on the class name. Ex: ``BlogPostResource`` ->
            ``api_blog_post_list``
        :type name_prefix: string

        :returns: The final name
        :rtype: string
        """
        if name_prefix is None:
            name_prefix = 'api_{0}'.format(
                cls.__name__.replace('Resource', '').lower()
            )

        name_prefix = name_prefix.rstrip('_')
        return '_'.join([name_prefix, name])

    @classmethod
    def urls(cls, name_prefix=None):
        """
        A convenience method for hooking up the URLs.

        This automatically adds a list & a detail endpoint to your URLconf.

        :param name_prefix: (Optional) A prefix for the URL's name (for
            resolving). The default is ``None``, which will autocreate a prefix
            based on the class name. Ex: ``BlogPostResource`` ->
            ``api_blogpost_list``
        :type name_prefix: string

        :returns: A ``patterns`` object for ``include(...)``
        """
        return patterns(
            '',
            url(r'^$', cls.as_list(), name=cls.build_url_name('list', name_prefix)),
            url(r'^(?P<pk>\d+)/$', cls.as_detail(), name=cls.build_url_name('detail', name_prefix)),
        )


class DjangoModelResource(DjangoResource):
    model = None

    # GET /
    def list(self):
        return self.model.objects.all()

    # GET /pk/
    def detail(self, pk):
        return self.model.objects.get(id=pk)

    # POST /
    @validate
    def create(self):
        return self.form.save()

    # DELETE /pk/
    def delete(self, pk):
        self.model.objects.get(id=pk).delete()

    # PUT /pk/
    def update(self, pk):
        obj = get_object_or_404(self.model, id=pk)
        oform = self.form_cls(self.data, instance=obj)
        if oform.is_valid():
            obj = oform.save()
            return obj
        else:
            return JsonResponse({'success': False, 'errors': oform.errors})
