import types
from functools import partial

from django.db.models import Q


def search_func(self, q, k, queries):
    if self.data.get(k):
        query = Q()
        for v in queries:
            query = query | Q(**{v: self.data.get(k)})
        q = q & query
    return q


def searcher(sdata, searcher_class, model):
    q = Q()
    res = []
    searcher = searcher_class(sdata)
    for key in sdata.iterkeys():
        dispatcher = getattr(searcher, 'search_%s' % key, False)
        if dispatcher:
            q = dispatcher(q)
    if q and len(q):
        res = model.objects.filter(q).distinct()
    return res


class FilterMaker(object):
    """
    Create searcher for a django model.
    Usage:
        FilterMaker.get_searcher(fmap, model)
        model :- Django model for which searcher is to be created.
        fmap :- search_field_name to query map, that is used to create the searcher_class.
    A typical `fmap` looks like this

    fmap = {
        'customer_name': [
            'contacts__first_name__icontains',
            'contacts__middle_name__icontains',
            'contacts__last_name__icontains',
        ],
        'policy_number': ['policy_number__endswith'],
    }
    This will create a Q() object which looks like this (data = search data dictionary)
    q = (
        Q(contacts__first_name__icontains=data['customer_name']) |
        Q(contacts__middle_name__icontains=data['customer_name']) |
        Q(contacts__last_name__icontains=data['customer_name'])
    ) &
    (
        Q(policy_number__endswith=data.get('policy_number'))
    )
    """
    class Filter(object):

        def __init__(self, sdata):
            self.data = sdata

    @classmethod
    def get_searcher(cls, fmap, model):
        searcher_class = cls._get_filter(fmap)
        return partial(searcher, searcher_class=searcher_class, model=model)

    @classmethod
    def _get_filter(cls, fmap):
        f = cls.Filter
        for k, queries in fmap.items():
            func = partial(search_func, k=k, queries=queries)
            setattr(f, 'search_%s' % k, types.MethodType(func, None, f))
        return f
