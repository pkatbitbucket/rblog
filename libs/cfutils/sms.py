import time
import os
import hashlib
import csv
from poster.encode import multipart_encode
from poster.streaminghttp import register_openers
import urllib
import urllib2
from utils.csv import UnicodeWriter


def send_sms(to, msg, mask="ICICIPRU"):
    p = "http://enterprise.smsgupshup.com/GatewayAPI/rest"

    if len(to) > 10 and isinstance(to, list):
        register_openers()
        at_one_time = 100000

        if not isinstance(msg, list):
            msg = [msg for i in range(len(to))]

        zipped = zip(to, msg)
        for bucket in [
                zipped[i: i + at_one_time]
                for i in range(0, len(zipped))
                if i % at_one_time == 0
        ]:
            csv.register_dialect(
                'gupshup',
                delimiter=',',
                quoting=csv.QUOTE_ALL
            )
            filename = os.path.join("/tmp/", "%s.csv" % hashlib.md5(str(time.time())).hexdigest())
            file_stream = open(filename, 'wb')
            writer = UnicodeWriter(file_stream, dialect=csv.get_dialect('gupshup'))
            writer.writerow(["PHONE", "MESSAGE"])
            if isinstance(msg, list):
                for i_to, i_msg in bucket:
                    writer.writerow([i_to, "%s" % i_msg])

            file_stream.close()
            wfile_stream = open(filename, 'rb')

            datagen, headers = multipart_encode({
                "file": wfile_stream,
                'method': 'xlsUpload',
                'filetype': 'csv',
                'msg_type': 'text',
                'mask': mask,
                'v': '1.1',
                'userid': '2000058874',
                'password': 'glitterfuck',
            })
            request = urllib2.Request(url=p, data=datagen, headers=headers)
            res = urllib2.urlopen(request).read()

    else:
        if isinstance(to, list):
            to = ",".join(to)

        data = {
            'msg': msg,
            'send_to': to,
            'v': '1.1',
            'userid': '2000058874',
            'password': 'glitterfuck',
            'msg_type': 'text',
            'method': 'sendMessage',
            'mask': mask,
        }
        querystring = urllib.urlencode(data)
        request = urllib2.Request(url=p, data=querystring)
        res = urllib2.urlopen(request).read()

    return res
