from django.conf.urls import *
from django.contrib.comments.urls import urlpatterns


urlpatterns += patterns('comments.views.comments',
    url(r'^ajax/post/$', 'ajax_post_comment', name='ajax-comments-post-comment'),
)
