from __future__ import unicode_literals

import logging

import requests

from celery_app import app
from utils import log_error

__author__ = 'parag'

logger = logging.getLogger(__name__)

FRESHDESK_ENDPOINT = "http://coverfox.freshdesk.com"
FRESHDESK_KEY = "6AjiTpbNUeRfQjLvZ4xs"


@app.task(queue='freshdesk')
def create_ticket(cc_emails=None, **kwargs):
    if not any(data for data in ('description', 'description_html')):
        raise ValueError('pass atleast description or description_html')
    payload = {'cc_emails': ','.join(cc_emails)} if cc_emails else {}
    defaults = {
        'priority': 1,
        'status': 2,
    }
    defaults.update(kwargs)
    payload['helpdesk_ticket'] = {key: value for key, value in defaults.iteritems()}

    response = _send_request.delay(
        method='post',
        url=FRESHDESK_ENDPOINT + '/helpdesk/tickets.json',
        json=payload,
    )

    return response.get()


@app.task(queue='freshdesk')
def view_ticket(ticket_id):
    response = _send_request.delay(
        method='get',
        url='{}/helpdesk/tickets/{}.json'.format(FRESHDESK_ENDPOINT, ticket_id),
    )

    return response.get()


@app.task(queue='freshdesk')
def update_ticket(ticket_id, cc_emails=None, **kwargs):
    if not any(data for data in ('description', 'description_html')):
        raise ValueError('pass atleast description or description_html')
    payload = {'cc_emails': ','.join(cc_emails)} if cc_emails else {}
    defaults = {
        'priority': 1,
        'status': 3,
    }
    defaults.update(kwargs)
    payload['helpdesk_ticket'] = {key: value for key, value in defaults.iteritems()}

    response = _send_request.delay(
        method='put',
        url='{}/helpdesk/tickets/{}.json'.format(FRESHDESK_ENDPOINT, ticket_id),
        json=payload,
    )

    resp = response.get()
    if resp:
        resp['ticket']['status'] = defaults['status']
        resp['helpdesk_ticket'] = resp['ticket']

    return resp


@app.task(queue='freshdesk_requests', rate_limit='1000/h')
def _send_request(**kwargs):
    response = requests.request(auth=(FRESHDESK_KEY, "X"), allow_redirects=False, **kwargs)

    if 'Retry-After' in response.headers:
        return {'retry_after': int(response.headers['Retry-After'])}

    if 'maintenance' in response.text:
        return {'retry_after': 1 * 60 * 60}

    if response.ok:
        return response.json()

    log_error(
        logger,
        msg="Freshdesk Request Failed\nRequest:\nUrl: {}\nBody: {}\n\nResponse:\nStatus: {}\nText: {}".format(
            response.request.url, response.request.body, response.status_code, response.text
        )
    )
