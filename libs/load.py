import os
from django.db.models.loading import AppCache
from django.db import transaction
from core.models import *
from seo.models import *
from blog.models import *
from django.utils.datastructures import SortedDict

def reloadall():
    cache = AppCache()

    curdir = os.getcwd()

    for app in cache.get_apps():
        f = app.__file__
        if f.startswith(curdir) and f.endswith('.pyc'):
            os.remove(f)
        __import__(app.__name__)
        reload(app)

    cache.app_store = SortedDict()
    cache.app_models = SortedDict()
    cache.app_errors = {}
    cache.handled = {}
    cache.loaded = False

@transaction.commit_manually
def flushall():
    transaction.commit()

