import base64
import codecs
import cStringIO
import csv
import datetime
import errno
import hashlib
import httplib
import json
import logging
import numbers
import os
import re
import socket
import ssl
import sys
import tempfile
import time
import traceback
import urllib
import urllib2
import urlparse
import unidecode
import zipfile
import threading

import requests
from statsd import StatsClient

from functools import wraps
from collections import namedtuple
from contextlib import contextmanager

from dateutil.relativedelta import relativedelta

from collections import OrderedDict
from threading import currentThread
from wsgiref.util import FileWrapper

import boto
from django.core.cache import cache
import pdfkit

from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.utils import timezone
from django.db import connection
from poster.encode import multipart_encode
from poster.streaminghttp import register_openers

from psycopg2.extensions import adapt
from geoip2 import errors as geoip2_errors
from geoip2 import models as geoip2_models
from geoip2.webservice import Client

from celery_app import app
from coverfox.serializers import DjangoQuerysetJSONEncoder
from xml2json import Xml2Json

logger = logging.getLogger(__name__)
extlogger = logging.getLogger('extensive')


def lock_name_to_int(lock_name):
    """converts lock_name into integer
    This is needed to create a pg_advisory_lock
    Can return negative values, but is acceptable
    """
    return lock_name.__hash__()


def acquire_exclusive_lock(lock_name, log_after_seconds=1):
    cursor = connection.cursor()
    lock_id = lock_name_to_int(lock_name)
    command = "select pg_advisory_lock(%d)" % lock_id
    start = time.time()
    cursor.execute(command)
    acquire = cursor.fetchone()[0]
    time_taken = time.time() - start
    if time_taken > log_after_seconds:
        extlogger.info("ERROR: lock %s - %s getting released in %s seconds (more than %s seconds)" % (
            lock_name, lock_id, time_taken, log_after_seconds
        ))
        print "ERROR: lock %s - %s getting released in %s seconds (more than %s seconds)" % (
            lock_name, lock_id, time_taken, log_after_seconds
        )
    return acquire


def release_exclusive_lock(lock_name):
    cursor = connection.cursor()
    lock_id = lock_name_to_int(lock_name)
    command = "select pg_advisory_unlock(%d)" % lock_id
    cursor.execute(command)
    release = cursor.fetchone()[0]
    if not release:
        raise Exception("Not able to release lock %s - %s" % (lock_name, lock_id))
    return release


def slugify(str):
    str = unidecode.unidecode(str).lower()
    return re.sub(r'\W+', '-', str)


def group_by(lst, key, func=None):
    if not lst:
        return
    rdict = {}
    if not func:
        if type(lst[0]) == dict:
            for l in lst:
                if not rdict.get(l[key]):
                    rdict[l[key]] = []
                rdict[l[key]].append(l)
        else:
            for l in lst:
                if not rdict.get(getattr(l, key)):
                    rdict[getattr(l, key)] = []
                rdict[getattr(l, key)].append(l)
    else:  # key is callable and so lst cannot be a dict #TODO: Handle class methods, static methods both
        for l in lst:
            v = getattr(getattr(l, key), func)()
            if not rdict.get(v):
                rdict[v] = []
            rdict[v].append(l)

    return rdict


def create_logger(name=None):
    if name is None:
        name = settings.SETTINGS_FILE_FOLDER.namebase
    name = name.replace('/', '.')
    log = logging.getLogger(name)
    log.setLevel(logging.INFO)
    return log

blog_subscribe_logger = create_logger("blog_subscribe")
request_logger = create_logger("transaction_logs/integration")
motor_logger = create_logger("transaction_logs/motor_integration")
travel_logger = create_logger("transaction_logs/travel_integration")
api_logger = create_logger("transaction_logs/apis")
quote_logger = create_logger("transaction_logs/quotes")
motor_suds_logger = create_logger("transaction_logs/motor_suds")
lms_logger = create_logger("lms_requests/lms")
clear_transaction_signal = create_logger("account_manager/clear_transaction_signal")
lms_leads_logger = create_logger("lms_requests/lms_leads")
lmsdev_leads_logger = create_logger("lms_requests/lmsdev_leads")
fastlane_logger = create_logger("transaction_logs/fastlane")
lms_histories_logger = create_logger("lms_requests/lms_histories")
term_logger = create_logger("quote_logs/term")
motor_caching = create_logger("transaction_logs/motor_caching")
payment_logger = create_logger("transaction_logs/payment")
# error_logger = create_logger("error")
# response_logger = create_logger("response")


def errors_to_json(form_errors):
    content = dict((key, [unicode(v) for v in values])
                   for key, values in form_errors.items())
    response = ({"success": False, "errors": content})
    return json.dumps(response)

"""
import boto

def put_in_new_bucket(bucket_name, key_name, file_path):
    s3 = boto.connect_s3()
    bucket = s3.create_bucket(bucket_name)
    key = bucket.new_key(key_name)
    key.set_contents_from_filename(file_path)
    key.set_acl('public-read')
    return "Ok"

def put_in_bucket(bucket_name, key_name, file_path):
    s3 = boto.connect_s3()
    bucket = s3.get_bucket(bucket_name)
    key = bucket.new_key(key_name)
    key.set_contents_from_filename(file_path)
    key.set_acl('public-read')
    return "Ok"

def copy_file(bucket_name, key_name, file_path):
    s3 = boto.connect_s3()
    key = s3.get_bucket(bucket_name).get_key(key_name)
    key.get_contents_to_filename(file_path)
    return "Ok"

def move_file(from_bucket, from_key, to_bucket, to_key):
    s3 = boto.connect_s3()
    key = s3.get_bucket(from_bucket).get_key(from_key)
    new_key = key.copy(to_bucket, to_key)
    if new_key.exists:
        key.delete()
    return "Ok"

def delete_file(bucket_name, key_name):
    s3 = boto.connect_s3()
    key = s3.get_bucket(bucket_name).get_key(key_name)
    key.delete()
    return "Ok"
"""

"""
def solr_tags(fields, q='*:*'):
    s = solr.SolrConnection(settings.SOLR_ROOT)
    res = s.raw_query(q=q, wt='json', facet='true', facet_field=fields)
    result = json.loads(res)['facet_counts']['facet_fields']
    r = []
    for k,v in result.items():
        r.extend(v)
    response = dict([(r[i],r[i+1]) for i in range(len(r)-1)[::2]])
    return response

def solr_paginator(q, start,rows):
    response = {}
    conn = solr.SolrConnection(settings.SOLR_ROOT)
    try:
        res = conn.query(q)
        numFound = int(res.results.numFound)
        results = res.next_batch(start=start,rows=rows).results
    except:
        numFound = 0
        results = []
    response['results'] = [dict(element) for element in results]
    response['count'] = numFound
    response['num_found'] = len(response['results'])
    response['has_prev'] = True
    response['has_next'] = True
    if start <= 0:
        response['has_prev'] = False
    if (start + rows) >= numFound:
        response['has_next'] = False
    return response

def add_data(data, include_fields=[]):
    if include_fields:
        for k,v in data.items():
            if k not in include_fields:
               data.pop(k)
    solr_add(**data)

def solr_add(**data_dict):
    s = solr.SolrConnection(settings.SOLR_ROOT)
    print data_dict
    s.add(**data_dict)
    s.commit()
    s.close()

def solr_delete(id):
    s = solr.SolrConnection(settings.SOLR_ROOT)
    s.delete(id)
    s.commit()
    s.close()

def solr_search(q, fields=None, highlight=None,
                  score=True, sort=None, sort_order="asc", **params):
    s = solr.SolrConnection(settings.SOLR_ROOT)
    response = s.query(q, fields, highlight, score, sort, sort_order, **params)
    return response

def solr_tags(fields, q='*:*'):
    s = solr.SolrConnection(settings.SOLR_ROOT)
    res = s.raw_query(q=q, wt='json', facet='true', facet_field=fields)
    result = json.loads(res)['facet_counts']['facet_fields']
    r = []
    for k,v in result.items():
        r.extend(v)
    response = dict([(r[i],r[i+1]) for i in range(len(r)-1)[::2]])
    return response

def solr_time(t):
    dt = datetime.datetime.strptime(t, "%Y-%m-%d %H:%M:%S")
    tt = time.mktime(dt.timetuple())+1e-6*dt.microsecond
    return time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime(tt))
"""

# #########   CSV Writer and Reader for any encoding ##################


class UTF8Recoder:
    """
    Iterator that reads an encoded stream and reencodes the input to UTF-8
    """

    def __init__(self, f, encoding):
        self.reader = codecs.getreader(encoding)(f)

    def __iter__(self):
        return self

    def next(self):
        return self.reader.next().encode("utf-8")


class UnicodeReader:
    """
    A CSV reader which will iterate over lines in the CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        f = UTF8Recoder(f, encoding)
        self.reader = csv.reader(f, dialect=dialect, **kwds)

    def next(self):
        row = self.reader.next()
        return [unicode(s, "utf-8") for s in row]

    def __iter__(self):
        return self


class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([s.encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        data = self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

        return data

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

############################################

# DEPRECATED : use cf_cns.notfy() instead.
def send_sms(to, msg, mask="CVRFOX"):
    if settings.DEBUG:
        print msg
        return
    p = "http://enterprise.smsgupshup.com/GatewayAPI/rest"

    if len(to) > 10 and type(to) == type([]):
        register_openers()
        at_one_time = 100000

        if type(msg) != type([]):
            msg = [msg for i in range(len(to))]

        zipped = zip(to, msg)
        for bucket in [zipped[i:i + at_one_time] for i in range(0, len(zipped)) if i % at_one_time == 0]:
            csv.register_dialect('gupshup', delimiter=',',
                                 quoting=csv.QUOTE_ALL)
            filename = os.path.join("/tmp/", "%s.csv" %
                                    hashlib.md5(str(time.time())).hexdigest())
            file_stream = open(filename, 'wb')
            writer = UnicodeWriter(
                file_stream, dialect=csv.get_dialect('gupshup'))
            writer.writerow(["PHONE", "MESSAGE"])
            if isinstance(msg, type([])):
                for i_to, i_msg in bucket:
                    writer.writerow([i_to, "%s" % i_msg])

            file_stream.close()
            wfile_stream = open(filename, 'rb')

            datagen, headers = multipart_encode({
                "file": wfile_stream,
                'method': 'xlsUpload',
                'filetype': 'csv',
                'msg_type': 'text',
                'mask': mask,
                'v': '1.1',
                'userid': '2000140660',
                'password': 'trottingfox',
            })
            request = urllib2.Request(url=p, data=datagen, headers=headers)
            res = urllib2.urlopen(request).read()
            # response_logger.info("Response %s" % (res))

    else:
        if type(to) == type([]):
            to = ",".join(to)

        data = {
            'msg': msg,
            'send_to': to,
            'v': '1.1',
            'userid': '2000140660',
            'password': 'trottingfox',
            'msg_type': 'text',
            'method': 'sendMessage',
            'mask': mask,
        }
        querystring = urllib.urlencode(data)
        request = urllib2.Request(url=p, data=querystring)
        res = urllib2.urlopen(request).read()
        # response_logger.info("Response %s" % (res))

    return res


def user_has_any_group(user, group_list):
    return user.is_authenticated() and (user.is_superuser or user.groups.filter(name__in=group_list).exists())


def profile_type_only(*user_type):
    return user_passes_test(lambda user: user_has_any_group(user, user_type))


def review_tracking_required(fn):
    def wrapped(request, *args, **kwargs):
        tracker = request.TRACKER
        if not tracker.activity_set.filter(event='INSTANT_REVIEW'):
            raise PermissionDenied
        else:
            return fn(request, *args, **kwargs)
    return wraps(fn)(wrapped)


def unescape(s):
    s = s.replace("&lt;", "<")
    s = s.replace("&gt;", ">")
    s = s.replace("&amp;", "&")
    s = s.replace("&quot;", "\"")
    return s


def superuser_only(function):
    def _inner(request, *args, **kwargs):
        if not request.user.is_superuser:
            raise PermissionDenied
        return function(request, *args, **kwargs)
    return _inner


def send_file(request):
    """
    Send a file through Django without loading the whole file into
    memory at once. The FileWrapper will turn the file object into an
    iterator for chunks of 8KB.
    """
    filename = __file__  # Select your file here.
    wrapper = FileWrapper(file(filename))
    response = HttpResponse(wrapper, content_type='text/plain')
    response['Content-Length'] = os.path.getsize(filename)
    return response


def send_zipfile(request):
    """
    Create a ZIP file on disk and transmit it in chunks of 8KB,
    without loading the whole file into memory. A similar approach can
    be used for large dynamic PDF files.
    """
    temp = tempfile.TemporaryFile()
    archive = zipfile.ZipFile(temp, 'w', zipfile.ZIP_DEFLATED)
    for index in range(10):
        filename = __file__  # Select your files here.
        archive.write(filename, 'file%d.txt' % index)
    archive.close()
    wrapper = FileWrapper(temp)
    response = HttpResponse(wrapper, content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename=test.zip'
    response['Content-Length'] = temp.tell()
    temp.seek(0)
    return response


_requests = {}


def get_queryset_timer(func):
    from django.db import connection, reset_queries

    def wrapper(*args, **kwargs):
        reset_queries()
        qs = func(*args, **kwargs)
        list(qs.values())
        all_queries = map(lambda q: float(q['time']), connection.queries)
        print all_queries, sum(all_queries)
        return qs
    return wrapper


def get_request():
    return _requests.get(currentThread())


def to_namedtuple(obj):
    if hasattr(obj, 'keys'):
        return namedtuple('NamedTuple', obj.keys())(**obj)
    elif hasattr(obj, '__iter__'):
        return namedtuple('NamedTuple', obj)(*obj)
    else:
        raise AssertionError('Only dict type or iter type argument is supported')


def to_nested_dict(dictionary):
    """converts for example qs.values() into nested dictionary"""
    def to_dict(keys, value, dictionary):
        try:
            key = keys.pop(0)
            dictionary[key] = dictionary.get(key, {}) if keys else value
            to_dict(keys, value, dictionary[key])
        except:
            pass

    new_dictionary = {}
    for key, value in dictionary.items():
        to_dict(key.split("__"), value, new_dictionary)
    return new_dictionary


def clean_dict(dictionary):
        return dict((k, v) for k, v in dictionary.iteritems() if v or isinstance(v, (numbers.Real, bool)))


class GlobalRequestMiddleware(object):

    def process_request(self, request):
        _requests[currentThread()] = request


class SNSUtils(object):

    client = None

    @classmethod
    def _get_client(cls):
        if not cls.client:
            if not boto.config.has_section('Boto'):
                boto.config.add_section('Boto')
            boto.config.set('Boto', 'sns_region_name', settings.BOTO_CONFIG['sns_region_name'])
            boto.config.set('Boto', 'sns_region_endpoint', settings.BOTO_CONFIG['sns_region_endpoint'])
            cls.client = boto.connect_sns(settings.BOTO_CONFIG['aws_access_key_id'], settings.BOTO_CONFIG['aws_secret_access_key'])
        return cls.client

    @classmethod
    def send_lead_message(cls, message, internal=False, request_ip='', request=None):
        message['request_ip'] = request_ip
        is_production_data = True
        if not settings.PRODUCTION or internal:
            message.update({'testing': 'Testing'})
            is_production_data = False
        cls.publish_message.apply_async(
            [cls], kwargs={'data': message, 'is_production_data': is_production_data, 'request': request}, queue='sns')

    @classmethod
    @app.task
    def publish_message(cls, data, is_production_data, request=None):
        from leads import models as leads_models
        from motor_product import forms as motor_forms
        from health_product import forms as health_forms
        from travel_product import forms as travel_forms
        from core import forms as core_forms
        from core import activity
        lead = leads_models.Lead.objects.create(data=data, is_production=is_production_data)

        if settings.PRODUCTION and not is_production_data:
            return

        data.update({'cf_id': lead.id})

        message = json.dumps(data)
        logg = lms_leads_logger if is_production_data else lmsdev_leads_logger
        logg.info(message)
        product = core_forms.RequirementForm.get_product_name(data['campaign'].lower())
        if product in ['bike', 'car']:
            fd = motor_forms.MotorFDForm(data=data).get_fd()
        elif product in ['health']:
            fd = health_forms.HealthFDForm(data={u'proposer_{}'.format(key): value for key, value in data.items()}).get_fd()
        elif product in ['travel']:
            fd = travel_forms.TravelFDForm(data={u'self_{}'.format(key): value for key, value in data.items()}).get_fd()
        else:
            fd = {}

        try:
            act = activity.create_activity(
                activity_type=activity.SUCCESS,
                product=product,
                page_id=data['label'],
                form_name='',
                form_variant='',
                form_position='',
                data=fd,
                request_dict=request
            )
            data['requirement_id'] = act.requirement.id
        except:
            logger.info('Requirement not generated for lead: {}'.format(lead.id))
        else:
            logger.info('LEAD SAVE SUCCESS (%s)' % lead.id)
        # act.extra = data
        # act.save(update_fields=['extra'])


class LMSUtils(object):
    datetime_format = '%Y-%m-%d %H:%M:%S %Z'
    tracked_events = ("proposal_failed", "bought_failed", "payment_failed",
                      "result_page", "buy_page", "buy-form",
                      "inspection_created", "bought_success", "rollover_not_allowed",
                      'quote_form_home_view', 'results_page_view')

    @classmethod
    @app.task(queue='lms_events')
    def post_to_lms(cls, url, data):
        try:
            msg = {
                'data': json.dumps(data, cls=DjangoQuerysetJSONEncoder)
            }
            response = requests.post(url, data=msg)
            if response.status_code != 200:
                lms_logger.error(str(response.status_code) + " " + str(response.raw))
        except:
            stacktrace = traceback.format_exc()
            lms_logger.error(stacktrace)

    @classmethod
    def send_lms_event(cls, event, product_type, tracker, data, mobile=None, email=None, internal=False, request_ip=''):
        event = event.lower()
        if event not in cls.tracked_events:
            raise NotImplementedError('Event "%s" is not implemented' % event)

        purl = settings.LMS_EVENT_URL
        data['request_ip'] = request_ip

        if event == "bought_success":
            data.update({'product_type': product_type})
            data['mobile'] = mobile
            data['email'] = email
            data['tracker_id'] = tracker.id
            purl = settings.LMS_BOUGHT_SUCCESS_URL
        elif event == 'rm_assignment':
            data.update({
                'event': event,
            })
        else:
            data.update({'product_type': product_type})
            data['event'] = event
            data['mobile'] = mobile
            data['email'] = email
            data['tracker_id'] = tracker.id

        if not (internal and purl.startswith('http://lms.coverfox.com')):
            pass

    @classmethod
    def convert_datetime(cls, dt):
        if isinstance(dt, datetime.datetime):
            if not timezone.is_aware(dt):
                dt = timezone.make_aware(dt)
            dt = dt.astimezone(timezone.get_current_timezone())
            return dt.strftime(cls.datetime_format)
        return dt


def log_error(logger, request=None, msg=None):
    request = request or get_request()
    if not msg:
        msg = "Internal Server Error %s" % request.path if request else ""

    logger.error(msg, exc_info=sys.exc_info(), extra={'status_code': 500,
                                                      'request': request})


def get_property(data, prop, is_data=False):
    """
    Input:
        1) data: object, dictionary, list, tuples, or any complex combination of these
        2) prop: string (property separated by dots)

    Output:
        If value found returns value, else None

    Examples:
    1)  data: <transaction object>
        prop:
            a) To get value of this `transaction.quote.vehicle.model.name` ==> prop should be 'quote.vehicle.model.name'
            b) Similarly `transaction.raw['user_form_details']['vehicle_reg_no']` ==> 'raw.user_form_details.vehicle_reg_no'

    2)  data: data = {'abc': [{'xyz': [{'abc': 'parag'}]}]}  OR
              data = {'abc': ({'xyz': ({'abc': 'parag'}, )}, )}
        prop:
            Instead of data['abc'][0]['xyz'][0]['abc'] ==> 'parag'
            Simply do get_property(data, 'abc.0.xyz.0.abc') ==> 'parag'
    """
    pr = filter(None, prop.split('.'))

    for p in pr:
        nextprop = '.'.join(pr[1:])

        # object
        if hasattr(data, p):
            return get_property(getattr(data, p), nextprop, True)

        # dictionary or OrderedDict
        elif type(data) == dict or isinstance(data, OrderedDict):
            ret = get_property(data[p], nextprop, True) if p in data else None
            return ret

        # list or tuple
        elif type(data) == list or type(data) == tuple:
            try:
                return get_property(data[int(p)], nextprop, True)
            except (ValueError, TypeError, IndexError):
                return None

        else:
            return None

    if is_data:
        return data
    else:
        return None


def fastlane(reg_no):
    def connect_patched(self):
        sock = socket.create_connection((self.host, self.port), self.timeout, self.source_address)
        if self._tunnel_host:
            self.sock = sock
            self._tunnel()
        self.sock = ssl.wrap_socket(sock, self.key_file, self.cert_file, ciphers="DEFAULT:!ECDH")
        # self.sock = ssl.wrap_socket(sock, self.key_file, self.cert_file, ssl_version=ssl.PROTOCOL_TLSv1)
    if getattr(settings, 'HTTPS_CONNECTION_PATCH', True):
        httplib.HTTPSConnection.connect = connect_patched
    try:
        flrequest = urllib2.Request("%s?regn_no=%s" % (settings.FAST_LANE_URL, reg_no))
        base64string = base64.encodestring('%s:%s' % (
            settings.FAST_LANE_BASIC_USERNAME, settings.FAST_LANE_BASIC_PASSWORD)
        ).replace('\n', '')
        flrequest.add_header("Authorization", "Basic %s" % base64string)
        r = urllib2.urlopen(flrequest)
    except:
        raise
    response = r.read()
    data = Xml2Json(response)
    return data.result


def html_to_pdf(html_path, pdf_path=None):
    pdfkit_path = settings.WKTHMLTOPDF_PATH
    config = pdfkit.configuration(wkhtmltopdf=pdfkit_path)

    # Reset CSS. http://yui.yahooapis.com/3.2.0/build/cssreset/reset-min.css
    if settings.STATIC_ROOT:
        css_path = os.path.join(settings.STATIC_ROOT, 'global/css/pdfkitreset.css')
    else:
        css_path = os.path.abspath('../static/global/css/pdfkitreset.css')

    return pdfkit.from_file(html_path, pdf_path, configuration=config, css=css_path)


def get_short_url(actual_url, params=None):
    url_parts = list(urlparse.urlparse(actual_url))
    if not str(url_parts[0]).startswith('http'):
        url_parts[0] = 'http'

    query = dict(urlparse.parse_qsl(url_parts[4]))
    if params:
        query.update(params)
        url_parts[4] = urllib.urlencode(query)

    actual_url = urlparse.urlunparse(url_parts)
    post_url = 'http://cvfx.in/get-short-url/'
    response = requests.post(post_url,
                             data={'actual_url': actual_url},
                             headers={'Content-Type': 'application/x-www-form-urlencoded'})

    if response.ok:
        try:
            return u"http://cvfx.in/{0}".format(str(json.loads(response.text)['shorturl_id']))
        except:
            log_error(logger)
    return ''


def camel_case_to_snake_case(string):
    sub = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', string)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', sub).lower()


def convert_datetime_to_timezone(dt):
    if isinstance(dt, datetime.datetime):
        if not timezone.is_aware(dt):
            dt = timezone.make_aware(dt)
        dt = dt.astimezone(timezone.get_current_timezone())
    return dt


def any_permission_satisfied(*permissions):
    """

    """
    def check_perms(user):
        for permission in permissions:
            if not isinstance(permission, (list, tuple)):
                perms = (permission, )
            else:
                perms = permission
            # First check if the user has the permission (even anon users)
            if user.has_perms(perms):
                break
        else:
            return False
        return True
    return user_passes_test(check_perms)


def convert_to_unicode(obj, *args):
    """ return the unicode representation of obj """
    try:
        return unicode(obj, *args)
    except UnicodeDecodeError:
        # obj is byte string
        ascii_text = str(obj).encode('string_escape')
        return unicode(ascii_text)


def convert_to_str(obj):
    """ return the byte string representation of obj """
    try:
        return str(obj)
    except UnicodeEncodeError:
        # obj is unicode
        return unicode(obj).encode('unicode_escape')


def get_vars_from_template(temp_string):
    y = re.compile('{{(.*?)\}}')
    return y.findall(temp_string)


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def years_ago(years, from_date=None):
    if from_date is None:
        from_date = datetime.now()
    return from_date - relativedelta(years=years)


def myadapt(val):
    if val is None:
        return 'NULL'
    else:
        return adapt(val)


def ip2location(ip_address, dictionary=True):
    if not settings.TRACE_IP:
        return

    cache_key = u'ip_details_{}'.format(ip_address)
    city = None
    if cache_key not in cache:
        client = Client(
            user_id=settings.MAXMIND_USERID,
            license_key=settings.MAXMIND_LICENSE_KEY,
            timeout=5,
        )
        try:
            city = client.city(ip_address=ip_address)
        except geoip2_errors.AddressNotFoundError:
            cache_timeout = 30 * 24 * 60 * 60
        except requests.Timeout:
            cache_timeout = None
        except:
            log_error(logger, msg='Error in ip tracking')
            cache_timeout = 5 * 60
        else:
            cache_timeout = 2 * 365 * 24 * 60 * 60

        if cache_timeout:
            cache.set(cache_key, city, cache_timeout)
    else:
        city = cache.get(cache_key)

    if isinstance(city, geoip2_models.City) and dictionary:
        try:
            state = city.raw['subdivisions'][0]['names']['fr']
        except:
            state = None
        return {
            'city': city.city.name,
            'state': state,
            'country': city.country.name,
            'continent': city.continent.name,
            'pincode': city.postal.code,
            'isp': city.traits.isp,
            'latitude': city.location.latitude,
            'longitude': city.location.longitude,
        }
    return city


def get_cache_key(model, extra=None):
    app = model._meta.app_label
    model_name = model.__class__.__name__
    pk = model.pk

    if extra:
        return "%s_%s_%s_%s" % (app, model_name, pk, extra)
    else:
        return "%s_%s_%s" % (app, model_name, pk)


@contextmanager
def pg_advisory_lock(lock_name, log_after_seconds=1):
    lock_id = lock_name_to_int(lock_name)
    start = time.time()
    with connection.cursor() as c:
        c.execute("SELECT pg_advisory_lock(%s)", [lock_id])
        time_taken = time.time() - start
        if time_taken > log_after_seconds:
            logger.debug("lock %s - %s getting released in %s seconds (more than %s seconds)" % (
                lock_name, lock_id, time_taken, log_after_seconds)
            )
        try:
            yield
        finally:
            c.execute("SELECT pg_advisory_unlock(%s)", [lock_id])


def clean_name(name):
    """
    :param name: Full name (string)
    :return: clean name (string)

    a.  remove salutation (if any)
    b.  if first_name < 3 characters then pick last_name else pick first_name
    """

    try:
        name_parts = str(name).split(' ')
        salutation = re.compile(r'^(?:mr|ms|miss|mrs|dr)\.?$', re.IGNORECASE)
        is_salutation = salutation.match(name_parts[0])
        name_parts = name_parts[1:] if is_salutation else name_parts
        name = name_parts[-1] if len(name_parts[0]) < 3 else name_parts[0]
    except:
        log_error(logger, msg='Unknown Error Received while cleaning full name')
    return name


class RequireProductionTrue(logging.Filter):
    def filter(self, record):
        return settings.PRODUCTION


class RequireProductionFalse(logging.Filter):
    def filter(self, record):
        return not settings.PRODUCTION


class CustomFileHandler(logging.FileHandler):
    """
    Filehandler that maintains nested file structure based on the logger name
    """
    def __init__(self, log_dir=None, mode='a', encoding=None, delay=0):
        self.mode = mode
        self.encoding = encoding
        self.delay = delay
        # We don't open the stream, but we still need to call the
        # Handler constructor to set level, formatter, lock etc.
        logging.Handler.__init__(self)
        self.stream = None
        self._streams = {}
        self.log_dir = log_dir or settings.LOG_PATH

    def emit(self, record):
        log_names = record.name.split('.')
        log_name = u'{}.log'.format(log_names[-1])
        log_names = log_names[:-1]
        self.log_folder = os.path.join(self.log_dir, '/'.join(log_names))
        self.baseFilename = os.path.join(self.log_folder, log_name)
        self.stream = self._open()
        super(CustomFileHandler, self).emit(record)

    def _open(self):
        if self.baseFilename in self._streams:
            return self._streams[self.baseFilename]

        try:
            os.makedirs(self.log_folder)
        except OSError:
            _, e, _ = sys.exc_info()
            if e.errno == errno.EEXIST:
                # folder already exists
                pass
            else:
                raise
        stream = super(CustomFileHandler, self)._open()
        self._streams[self.baseFilename] = stream
        return stream

    def close(self):
        for stream in self._streams.values():
            self.stream = stream
            super(CustomFileHandler, self).close()


def get_statsd():
    tlocal = threading.local()
    if not getattr(tlocal, 'statsd', None):
        tlocal.statsd = StatsClient(host=settings.STATSD_HOST,
                                    port=settings.STATSD_PORT,
                                    prefix=settings.STATSD_PREFIX,
                                    maxudpsize=settings.STATSD_MAXUDPSIZE)
    return tlocal.statsd


statsd = get_statsd()
