SUBSCRIBER_LIST = {
    'HEALTH_QUALIFIER_REPORT': [],
    'MARKETING_REPORT': [],
    'LEAD_PRODUCTIVITY_REPORT': [],
    'ADVISOR_PRODUCTIVITY_REPORT': [],
    'MOTOR_LEAD_PRODUCTIVITY_REPORT_MINI': [],
    'HEALTH_QUALIFIER_REPORT_MINI': [],
    'MOTOR_QUALIFIER_REPORT': []
}

ALERT_SUBSCRIBERS = {
    'car': ['gulshan@coverfoxmail.com'],
    'bike': ['gulshan@coverfoxmail.com'],
    'health': ['gulshan@coverfoxmail.com'],
    'travel': ['gulshan@coverfoxmail.com'],
    'lms-developers': [
        'vishesh@coverfoxmail.com',
        'sanket@coverfoxmail.com',
        'gulshan@coverfoxmail.com',
        'sanjay@coverfoxmail.com',
    ],
}

try:
    from local_email_subscribers import SUBSCRIBER_LIST  # noqa
except ImportError:
    pass
