import datetime

from leads import models as leads_models
from core import models as core_models


def countit(start, end=datetime.datetime.now()):
    lead_network_map = {}
    leadmobiles = set()
    for l in leads_models.Lead.objects.filter(
        created_on__gte=start, created_on__lt=end
    ).iterator():
        ldata = l.data
        leadmobiles.add(ldata.get('mobile'))
        if ldata.get('mobile'):
            if ldata.get('network', '') in lead_network_map.keys():
                lead_network_map[ldata.get('network', '')] += 1
            else:
                lead_network_map[ldata.get('network', '')] = 1
    # print "Number of leads according to Lead Table with mobile numbers\n"
    print "Leads", len(leadmobiles)

    # ################### Total Phone objects created #########################
    phones = set(core_models.Phone.objects.filter(
        created_date__gte=start, created_date__lt=end
    ).distinct('number').values_list('number', flat=True))

    print "Phones: ", len(phones)

    # ########################## INCOMING SECTION #############################
    distinct_visitor_phones_reqs = core_models.Requirement.objects.filter(
        created_date__gte=start,
        created_date__lt=end,
        current_state__isnull=False
    ).filter(
        mid__source='INCOMING'
    ).values_list('visitor__phones__number', flat=True)

    distinct_user_phones_reqs = core_models.Requirement.objects.filter(
        created_date__gte=start,
        created_date__lt=end,
        current_state__isnull=False
    ).filter(
        mid__source='INCOMING'
    ).values_list('user__phones__number', flat=True)

    incomingmobiles = set()
    incomingmobiles.update(set(
        distinct_user_phones_reqs
    ))
    incomingmobiles.update(set(
        distinct_visitor_phones_reqs
    ))
    print "INCOMING", len(incomingmobiles)
    # ######################## NON INCOMING SECTION ###########################

    distinct_visitor_phones_reqs = core_models.Requirement.objects.filter(
        created_date__gte=start,
        created_date__lt=end,
        current_state__isnull=False
    ).exclude(
        mid__source='INCOMING'
    ).values_list('visitor__phones__number', flat=True)

    distinct_user_phones_reqs = core_models.Requirement.objects.filter(
        created_date__gte=start,
        created_date__lt=end,
        current_state__isnull=False
    ).exclude(
        mid__source='INCOMING'
    ).values_list('user__phones__number', flat=True)

    nonincomingmobiles = set()
    nonincomingmobiles.update(set(
        distinct_user_phones_reqs
    ))
    nonincomingmobiles.update(set(
        distinct_visitor_phones_reqs
    ))

    print "Requirements", len(nonincomingmobiles)
    overlap = len(incomingmobiles.intersection(leadmobiles))
    requirements = len(leadmobiles) + len(incomingmobiles) - overlap
    print "Requirement expected", requirements
    onfloor = len(incomingmobiles) + len(nonincomingmobiles)
    print "Requirements on Floor", onfloor
    print "Overlap (incoming/leads)", overlap
    leadmobiles_in_phone_model = core_models.Phone.objects.filter(number__in=leadmobiles).distinct('number')
    print "Things we can recover", len(leadmobiles) - leadmobiles_in_phone_model.count()
    print "Extra leads in Phone models", core_models.Phone.objects.filter(
        created_date__gte=start,
        created_date__lt=end,
    ).exclude(number__in=leadmobiles.union(incomingmobiles)).distinct('number').count()

    data_in_system = len(leadmobiles) + len(incomingmobiles) - overlap
    print "Mobiles in system(leads+incoming)", data_in_system
    non_web_affiliate_requirements = core_models.Requirement.objects.filter(
        mid__category__in=['ValueDirect', 'firstoption'],
        created_date__gte=start,
        created_date__lt=end,
    ).count()
    old_created_phones = set(core_models.Phone.objects.filter(
        created_date__lt=start,
        number__in=leadmobiles.union(incomingmobiles),
    ).values_list('number', flat=True))
    todaymobilesinsystem = data_in_system - len(old_created_phones)
    return {
        'leadmobiles': leadmobiles,
        'incomingmobiles': incomingmobiles,
        'nonincomingmobiles': nonincomingmobiles,
        'phones': phones,
        'onfloor': onfloor,
        'overlap': overlap,
        'todaymobilesinsystem': todaymobilesinsystem,
        'data_in_system': data_in_system,
        'non_web_affiliate_requirements': non_web_affiliate_requirements,
    }
