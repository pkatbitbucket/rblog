//const APP_HOST = "50.116.46.185";
//const APP_PORT = 80;
const APP_HOST = "0.0.0.0";
const APP_PORT = 8081;

const REDIS_PORT = 6381;
const REDIS_HOST = 'localhost';

const DIRECT_DATA_PUSH_COMMANDS = [
    'notify_user', 'remove_incoming_call_popup', 'remove_transfer_call_popup',
    'advisor_changed_status', 'requirement_remove', 'requirement_transfer_lead',
    'task_remove', 'task_transfer_lead',
];
// indirect -> 'requirement_FRESH_new', 'requirement_FRESH_ringing', 'requirement_QUALIFIED'

var _ = require('underscore');
const redis = require('redis');

var winston = require('winston');
function formatter(args) {
    var dateTimeComponents = new Date()
    dateTimeComponents = dateTimeComponents.toLocaleDateString() + ' ' + dateTimeComponents.toLocaleTimeString();
    var logMessage = dateTimeComponents + ' - ' + args.level + ': ' + args.message;
    return logMessage;
}
winston.add(winston.transports.File, { filename: '/mnt/logs/nodejs.log', timestamp: true, json: false, formatter: formatter});

function getDistinctElementList(inputList) {
    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }
    return inputList.filter(onlyUnique);
}

var http = require("http").createServer();
http.listen(APP_PORT, APP_HOST);
var io = require('socket.io').listen(http);

const rclient = redis.createClient(REDIS_PORT, 'localhost');
winston.info("Server listening on port %s", APP_HOST)

const publisher = redis.createClient(REDIS_PORT, 'localhost');
var USERS = [];
var SOCKET_TO_USER = {};

io.sockets.on('connection', function(socket) {
    const subscriber = redis.createClient(REDIS_PORT, 'localhost');

    socket.on('subscribe_to_channels', function(msg){
        if(USERS.indexOf(msg.user) == -1) {
            USERS.push(msg.user);
            SOCKET_TO_USER[socket.id] = msg.user;
        } else {
            if(msg.disallow_multiple) {
                socket.disconnect();
                wiston.info('User:', msg.user, ' exists has a socket attached');
            }
        }

        _.each(msg.channels, function(channel, index){
            subscriber.subscribe(channel);
            var new_reqs = rclient.zrange(channel, 0, -1, function(err, new_reqs){
                _.each(new_reqs, function(rid, index){
                    rclient.get('requirement_'+rid, function(err, reply){
                        winston.info("Redis push on", msg.user, " message:", reply);
                        socket.emit('update', {'channel': channel, 'msg': {'command': 'FRESH_new', 'data': JSON.parse(reply)}});
                    });
                });
            });
        });
        var queues = rclient.keys('available_*', function(err, queues) {
            _.each(queues, function(queue_name, index){
                var elems = rclient.lrange(queue_name, 0, -1, function(err, elems){
                    count = getDistinctElementList(elems).length;
                    if(count < 6){
                        count = 0;
                    }
                    socket.emit('update', {'campaign': 'system', 'msg': {'command': 'update_available_count', 'data': {'queue_name': queue_name, 'count': count}}})
                });
            });
        });

        subscriber.on("message", function(channel, message) {
            function socketEmit(channel, command, data){
                socket.emit('update', {'campaign' : channel, 'msg': {'command': command, 'data': data}});
            }
            winston.info("Redis push on", channel, " message:", message);
            console.log("Redis push on", channel, " message:", message);
            var rdata = JSON.parse(message);
            if (DIRECT_DATA_PUSH_COMMANDS.indexOf(rdata.command) > -1){
                socketEmit(channel, rdata.command, rdata.data);
            }
            else if(rdata.command == 'update_available_count'){
                queue_name = 'available_' + rdata.data;
                var elems = rclient.lrange(queue_name, 0, -1, function(err, elems){
                    count = getDistinctElementList(elems).length;
                    if(count < 3){
                        count = 0;
                    }
                    socket.emit('update', {'campaign': 'system', 'msg': {'command': 'update_available_count', 'data': {'queue_name': queue_name, 'count': count}}})
                });
            }
            else{
                rclient.get('requirement_' + rdata.data, function(err, reply){
                    socketEmit(channel, rdata.command, JSON.parse(reply));
                });
            }
        });
    });

    socket.on('call_transfer', function(msg){
        publisher.set('calltransfer_mobile_'+msg.mobile, msg.rid, function(err, reply){
        });
    });

    socket.on('message', function(msg) {
        winston.info("Message ", msg);
    });

    socket.on('disconnect', function() {
        for(var i = 0; i < USERS.length; i++) {
            if (USERS[i] === SOCKET_TO_USER[socket.id]) {
                USERS.splice(i, 1);
            }
        }
        var userId = SOCKET_TO_USER[socket.id];
        delete SOCKET_TO_USER[socket.id];
        var queues = rclient.keys('available_*', function(err, queues) {
            _.each(queues, function(queue_name, index){
                rclient.lrem(queue_name, 1, userId, function(err, response){
                });
                var elems = rclient.lrange(queue_name, 0, -1, function(err, elems){
                    count = getDistinctElementList(elems).length;
                    if(count < 3){
                        count = 0;
                    }
                    queue = queue_name.split('_')[1];
                    publisher.publish('system', JSON.stringify({'command': 'update_available_count', 'data': queue}));
                });
            });
        });
        subscriber.quit();
    });
});
