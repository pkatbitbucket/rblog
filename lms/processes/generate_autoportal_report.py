#!/usr/bin/env python
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")  # noqa
import django
import pytz
import datetime
from django.conf import settings
from django.utils import timezone
from dateutil.relativedelta import relativedelta

django.setup()  # noqa

from email_subscribers import SUBSCRIBER_LIST
from django.core.mail import send_mail
from core import models as core_models
from motor_product import models as motor_models

base_url = settings.SITE_URL if not settings.DEBUG else "http://localhost:8000"
from_date = (datetime.datetime.now(pytz.timezone("Asia/Calcutta")) - relativedelta(days=+2)).replace(
    hour=0, minute=0, second=0, microsecond=0).astimezone(pytz.utc)
to_date = datetime.datetime.now(pytz.timezone("Asia/Calcutta")).replace(
    hour=0, minute=0, second=0, microsecond=0).astimezone(pytz.utc)

rtype = 'AUTOPORTAL_LEADS_REPORT'
filename = "report_%s_%s_%s_created_on_%s.csv" % (
    rtype,
    from_date.strftime("%d-%m-%Y"),
    to_date.strftime("%d-%m-%Y"),
    datetime.datetime.now().strftime("%d-%m-%Y_%H:%M")
)
f = open(os.path.join(settings.UPLOAD_FOLDER, filename), 'w')

ap_report_url = base_url + '/uploads/' + filename

reqs = core_models.Requirement.objects.filter(
    activity__mid__source='Autoportal'
)

phones = set(reqs.values_list('user__phones__number', flat=True))
phones.update(set(reqs.values_list('visitor__phones__number', flat=True)))
paid_sources = [
    'Overdrive',
    'SEARCH',
    'affiliate',
    'SearchNetwork',
    'Yahoo',
    'cardekho',
    'facebook',
    'facebook-exp',
    'displaynetwork',
    'GSP',
    'justdial',
    'Team-BHP',
    'DISPLAY',
    'yahoo_search',
    'whitedwarf',
    'Autoportal',
    'sensedigital_email',
    'CarTrade',
    'Youtube-Trueview',
    'jagoinvestor',
    'Jago',
    'Search-unassisted'
]

row = 'Requirement Id,Mobile,Product,Created Time, Make, Model, Variant\n'
f.write(row)
for mob in phones:
    if mob:
        req_ids = set(core_models.Requirement.objects._all().filter(
            user__phones__number=mob
        ).values_list('id', flat=True))
        req_ids = req_ids.union(core_models.Requirement.objects._all().filter(
            visitor__phones__number=mob
        ).values_list('id', flat=True))
        req = core_models.Requirement.objects._all().filter(id__in=req_ids).order_by('created_date')[0]
        before_reqs_activities = core_models.Activity.objects.filter(
            requirement=req,
            created__lte=req.created_date,
            mid__source__in=paid_sources,
        ).order_by('-created')
        print req.id
        fd = req.fd.flatten() if req.fd else {}
        vehicle_details = None
        if fd:
            vehicle_id = fd.get('vehicle_master__car', '')
            if vehicle_id and vehicle_id != 'None':
                vehicle_details = motor_models.VehicleMaster.objects.filter(pk=vehicle_id).first()

        if before_reqs_activities.exists():
            attibuted_source = before_reqs_activities[0].mid.source
        else:
            attibuted_source = 'Autoportal'

        if attibuted_source == 'Autoportal' and req.kind.product == 'car':
            if vehicle_details:
                row = str(req.id) + ',' + str(mob) + ',' + str(req.kind.product) + ',' + str(
                    timezone.localtime(req.created_date)) + ',' + str(
                    getattr(vehicle_details.make, 'name', 'NA')) + ',' + str(
                    getattr(vehicle_details.model, 'name', 'NA')) + ',' + str(
                    getattr(vehicle_details, 'variant', 'NA')) + '\n'
            else:
                row = str(req.id) + ',' + str(mob) + ',' + str(req.kind.product) + ',' + str(
                    timezone.localtime(req.created_date)) + '\n'
            f.write(row)

f.close()
email_body = "Below are the links for reports generated at %s \nAutoPortal report from %s to %s : \n %s" % (
    datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p'), from_date.strftime("%d %b %Y"),
    to_date.strftime("%d %b %Y"),
    ap_report_url)

send_mail('Report generated at %s' % (datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p')), email_body,
          settings.DEFAULT_FROM_EMAIL, SUBSCRIBER_LIST.get(rtype, []), fail_silently=False)

print "Mail sent for Health Report at %s" % (datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p'))
