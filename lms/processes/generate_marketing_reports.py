#!/usr/bin/env python
import os
import datetime
import settings
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()
import pytz
from django.utils import timezone
from dateutil.relativedelta import relativedelta
from email_subscribers import SUBSCRIBER_LIST
from django.core.mail import send_mail
import report_utils as rutils


base_url = settings.SITE_URL if not settings.DEBUG else "http://localhost:8000"
from_date = datetime.datetime.now(pytz.timezone("Asia/Calcutta")).replace(hour=0, minute=0, second=0, microsecond=0).astimezone(pytz.utc)
to_date = timezone.now()
rtype = 'MARKETING_REPORT'

filename = rutils.get_lead_report(rtype, from_date, to_date)

report_url = base_url + '/uploads/' + filename

email_body = "Below are the links for reports generated at %s \n\nMarketing report from %s to %s : \n %s" % (datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p'), from_date.strftime("%d %b %Y"), to_date.strftime("%d %b %Y"), report_url)

send_mail('Marketing Report generated at %s' % (datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p')), email_body, settings.DEFAULT_FROM_EMAIL, SUBSCRIBER_LIST.get(rtype, []), fail_silently=False)

print "Mail sent for Marketing Report at %s" % (datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p'))
