#!/usr/bin/env python
import os
import datetime
import settings

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()

import pytz
from django.utils import timezone
from dateutil.relativedelta import relativedelta
from email_subscribers import SUBSCRIBER_LIST
from django.core.mail import send_mail
import report_utils as rutils
import sales_report_utils as srutils

base_url = settings.SITE_URL if not settings.DEBUG else "http://localhost:8000"
lpr_from_date = (datetime.datetime.now(pytz.timezone("Asia/Calcutta"))).replace(day=1, hour=0, minute=0, second=0, microsecond=0).astimezone(pytz.utc)
sales_from_date = (datetime.datetime.now(pytz.timezone("Asia/Calcutta")) - relativedelta(months=+2)).replace(day=1, hour=0, minute=0, second=0, microsecond=0).astimezone(pytz.utc)
to_date = datetime.datetime.now(pytz.timezone("Asia/Calcutta")).replace(hour=0, minute=0, second=0, microsecond=0).astimezone(pytz.utc)
csc_to_date = datetime.datetime.now(pytz.timezone("Asia/Calcutta")).replace(hour=23, minute=59, second=59).astimezone(pytz.utc)
lpr_rtype = 'LEAD_PRODUCTIVITY_REPORT'
sales_rtype = 'SALES_REPORT'

# ######   Lead Productivity report starts   #######
lpr_filename = rutils.get_lead_report(lpr_rtype, lpr_from_date, to_date)

lpr_report_url = base_url + '/uploads/' + lpr_filename
# #######   Lead Productivity Report ends   #################

# #######   Sales Report starts   ###########

sales_filename = ""#srutils.get_sales_report(sales_from_date, to_date)
# group_names, users_group = putils.get_motor_users()
# productivity_filenames = "\n".join(['%s/static/uploads/%s' % (base_url, putils.advisor_productivity_dashboard(
#     users, group_names[i].replace(" ", "_"), (lpr_from_date, to_date))
# ) for i, users in enumerate(users_group)])

sales_report_url = ""#base_url + '/uploads/' + sales_filename


# #######   Sales Report ends   ###########

email_body = "Below are the links for reports generated today at %s \n\nLead Productivity report from %s to %s : \n %s\nSales Report using Transaction from %s to %s : \n%s\n\nRegards,\nLMS Tech" % (datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p'), lpr_from_date.strftime("%d %b %Y"), to_date.strftime("%d %b %Y"), lpr_report_url, sales_from_date.strftime("%d %b %Y"), to_date.strftime("%d %b %Y"), sales_report_url)

# productivity_email_body = """Below are the links for advisor productivity, reports generated today at %s \n\nAdvisor Productivity report from %s to %s :\n%s\n\nRegards,\nLMS Tech""" % (
#     datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p'), lpr_from_date.strftime("%d %b %Y"),
#     to_date.strftime("%d %b %Y"), productivity_filenames
# )

print datetime.datetime.now()
print email_body

print datetime.datetime.now()
# print productivity_email_body

# send_mail('Advisor Productivity Report', productivity_email_body, settings.DEFAULT_FROM_EMAIL, SUBSCRIBER_LIST.get('ADVISOR_PRODUCTIVITY_REPORT', ['gulshan@coverfoxmail.com', 'vishesh@coverfox.com', 'sanket@coverfoxmail.com']), fail_silently=False)

send_mail('Last day Report', email_body, settings.DEFAULT_FROM_EMAIL, SUBSCRIBER_LIST.get(lpr_rtype, []), fail_silently=False)

csc_rtype = "CALLBACK_LATER_REPORT"
csc_filename = rutils.get_lead_report(csc_rtype, lpr_from_date, csc_to_date)

csc_report_url = base_url + '/uploads/' + csc_filename

csc_email_body = "Below are the link for report generated today at %s \n\nCustomer Scheduled Callback report from upto %s : \n %s\n\nRegards,\nLMS Tech" % (datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p'), to_date.strftime("%d %b %Y"), csc_report_url)
print datetime.datetime.now()
print email_body

print datetime.datetime.now()

send_mail('Customer Scheduled Callback Report', csc_email_body, settings.DEFAULT_FROM_EMAIL, SUBSCRIBER_LIST.get(lpr_rtype, []), fail_silently=False)