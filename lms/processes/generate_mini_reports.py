
#!/usr/bin/env python
import os
import datetime
import settings

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()

import pytz
from django.utils import timezone
from dateutil.relativedelta import relativedelta
from email_subscribers import SUBSCRIBER_LIST
from django.core.mail import send_mail
import report_utils as rutils


base_url = settings.SITE_URL if not settings.DEBUG else "http://localhost:8000"

from_date = datetime.datetime.now(pytz.timezone("Asia/Calcutta")).replace(hour=0, minute=0, second=0, microsecond=0).astimezone(pytz.utc)
to_date = timezone.now()

""" Motor Lead Productivity report Mini"""
rtype = 'MOTOR_LEAD_PRODUCTIVITY_REPORT_MINI'

lpr_filename = rutils.get_lead_report(rtype, from_date, to_date)

lpr_report_url = base_url + '/uploads/' + lpr_filename

""" Motor qualifier report"""

motor_qualifier_mini_rtype = 'MOTOR_QUALIFIER_REPORT_MINI'

mqr_filename = rutils.get_lead_report(motor_qualifier_mini_rtype, from_date, to_date)

qr_report_url = base_url + '/uploads/' + mqr_filename

email_body = "Below are the links for reports generated at %s \n\nMotor Lead Producitivity Mini report from %s to %s : \n %s\n\nMotor Qualifier Mini report from %s to %s : \n %s" % (datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p'), from_date.strftime("%d %b %Y"), to_date.strftime("%d %b %Y"), lpr_report_url, from_date.strftime("%d %b %Y"), to_date.strftime("%d %b %Y"), qr_report_url)

send_mail('Motor mini Reports generated at %s' % (datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p')), email_body, settings.DEFAULT_FROM_EMAIL, SUBSCRIBER_LIST.get(rtype, []), fail_silently=False)

print "Mail sent for Motor Report at %s" % (datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p'))

""" Health Lead Productivity report Mini"""

rtype = 'HEALTH_LEAD_PRODUCTIVITY_REPORT_MINI'


filename = rutils.get_lead_report(rtype, from_date, to_date)

lpr_report_url = base_url + '/uploads/' + filename

""" Health qualifier report"""

rtype = 'HEALTH_QUALIFIER_REPORT_MINI'

filename = ""#rutils.get_lead_report(rtype, from_date, to_date)

hqr_report_url = ""#base_url + '/uploads/' + filename

email_body = "Below are the links for reports generated at %s \n\nHealth Lead Producitivity Mini report from %s to %s : \n %s\n\nHealth Qualifier Mini report from %s to %s : \n %s" % (datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p'), from_date.strftime("%d %b %Y"), to_date.strftime("%d %b %Y"), lpr_report_url, from_date.strftime("%d %b %Y"), to_date.strftime("%d %b %Y"), hqr_report_url)

send_mail('Health mini Reports generated at %s' % (datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p')), email_body, settings.DEFAULT_FROM_EMAIL, SUBSCRIBER_LIST.get(rtype, []), fail_silently=False)

print "Mail sent for Report at %s" % (datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p'))