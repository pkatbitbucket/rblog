#!/usr/bin/env python
import os
import datetime
import settings
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()
from django.utils import timezone
import pytz
from dateutil.relativedelta import relativedelta
from email_subscribers import SUBSCRIBER_LIST
from django.core.mail import send_mail
import report_utils as rutils


base_url = settings.SITE_URL if not settings.DEBUG else "http://localhost:8000"
from_date = (datetime.datetime.now(pytz.timezone("Asia/Calcutta")) - relativedelta(months=+1)).replace(day=1, hour=0, minute=0, second=0, microsecond=0).astimezone(pytz.utc)
to_date = datetime.datetime.now(pytz.timezone("Asia/Calcutta")).replace(hour=0, minute=0, second=0, microsecond=0).astimezone(pytz.utc)

rtype = 'MOTOR_QUALIFIER_REPORT'
mqr_filename = rutils.get_lead_report(rtype, from_date, to_date)

qr_report_url = base_url + '/uploads/' + mqr_filename

email_body = "Below are the links for reports generated at %s \nMotor Qualifier Mini report from %s to %s : \n %s" % (datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p'), from_date.strftime("%d %b %Y"), to_date.strftime("%d %b %Y"), qr_report_url)

send_mail('Motor Qualifier Report generated at %s' % (datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p')), email_body, settings.DEFAULT_FROM_EMAIL, SUBSCRIBER_LIST.get(rtype, []), fail_silently=False)

print "Mail sent for Motor Report at %s" % (datetime.datetime.now().strftime('%-d %b %Y, %I:%M %p'))
