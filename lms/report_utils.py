import os
import csv
from django.db.models import Q
from core import models as core_models
from crm import models as crm_models
import datetime
import time
import pytz
from django.conf import settings
from django.utils.timezone import localtime

REPORT_TYPE = [
    ('LEAD_PRODUCTIVITY_REPORT', 'Lead Productivity Report'),
    ('MOTOR_LEAD_PRODUCTIVITY_REPORT_MINI', 'Motor Lead Productivity Report Mini'),
    ('HEALTH_LEAD_PRODUCTIVITY_REPORT_MINI', 'Health Lead Productivity Report Mini'),
    ('MARKETING_REPORT', 'Marketing Report'),
    ('MOTOR_QUALIFIER_REPORT', 'Motor Qualifier Report'),
    ('HEALTH_QUALIFIER_REPORT', 'Health Qualifier Report'),
    ('MOTOR_QUALIFIER_REPORT_MINI', 'Motor Qualifier Report Mini'),
    ('HEALTH_QUALIFIER_REPORT_MINI', 'Health Qualifier Report Mini'),
]

PAID_NETWORKS = [
    'SEARCH', 'justdial', 'facebook', 'Autoportal', 'MySmartPrice', 'Yahoo', 'Team-BHP', 'GSP',
    'DISPLAY', 'affiliate', 'CarTrade', 'cardekho', 'DisplayNetwork', 'taboola', 'SearchNetwork',
    'truebill', 'jago', 'Overdrive', 'justdial-app', 'adsurfmedia'
]

UNPAID_NETWORKS = ['ORGANIC', 'INCOMING', 'email', '2015leads', 'im_email', 'im_sms', 'email.']

REPORT_TYPE_VERBOSE_MAP = {item[0]: item[1] for item in REPORT_TYPE}


def get_lead_report(rtype, from_date, to_date, queues=None, request_user=None):
    phones = core_models.Phone.objects.filter(Q(created_date__gte=from_date) & Q(created_date__lte=to_date)).distinct(
        'number').values_list('number', flat=True)
    if not queues:
        queues = crm_models.Queue.objects.all()

    REPORT_TYPE_QUERY = {
        'LEAD_PRODUCTIVITY_REPORT': {
            'query': core_models.Requirement.objects.filter(
                Q(created_date__gte=from_date) & Q(created_date__lte=to_date) &
                Q(Q(user__phones__number__in=phones) | Q(visitor__phones__number__in=phones))
            ).distinct(),
            'headings_list': [
                'id', 'mobile', 'email', 'state', 'assigned_to', 'advisor_head_disposition', 'advisor_sub_disposition',
                'last_call_time', 'next_call_time', 'created_on', 'campaign', 'kind_kind', 'kind_sub_kind',
                'kind_product', 'assigned_to_time', 'click_call_time', 'initial_device', 'initial_network',
                'initial_ad_category', 'latest_paid_network', 'latest_paid_ad_category', 'first_unpaid_network',
                'first_unpaid_ad_category', 'is_qualified', 'device', 'brand', '_mid_source', '_mid_category', 'label',
                'keyword', 'keyword_category', 'referer', 'triggered_page', 'attempts_made', 'vt_matchtype',
                '_mid_content', 'vt_placement', 'vt_target', 'im_medium', 'im_content', '_mid_campaign',
                'landing_page_url', 'gclid', 'calculated_city', 'past_policy_expiry_date'
            ],
        },
        'CALLBACK_LATER_REPORT': {
            'query': core_models.Requirement.objects.filter(
                Q(next_crm_task__scheduled_time__lte=to_date) & Q(current_state__parent_state__name="QUALIFIED")
            ).distinct(),
            'headings_list': [
                'id', 'mobile', 'email', 'state', 'assigned_to', 'advisor_head_disposition', 'advisor_sub_disposition',
                'last_call_time', 'next_call_time', 'created_on', 'campaign', 'kind_kind', 'kind_sub_kind',
                'kind_product', 'assigned_to_time', 'click_call_time', 'initial_device', 'initial_network', 'device',
                'brand', '_mid_source', '_mid_category', 'label', 'keyword', 'keyword_category', 'referer',
                'triggered_page', 'attempts_made', 'vt_matchtype', '_mid_content', 'vt_placement', 'vt_target',
                'im_medium', 'im_content', 'landing_page_url', 'gclid', 'calculated_city', 'past_policy_expiry_date'
            ],
        },
        'MOTOR_LEAD_PRODUCTIVITY_REPORT_MINI': {
            'query': core_models.Requirement.objects.filter(
                Q(Q(kind__product__icontains='car') | Q(kind__product__icontains='motor')) &
                Q(Q(created_date__gte=from_date) & Q(created_date__lte=to_date))
            ).distinct(),
            'headings_list': [
                'id', 'mobile', 'email', 'state', 'assigned_to', 'advisor_head_disposition', 'advisor_sub_disposition',
                'last_call_time', 'next_call_time', 'created_on', 'assigned_to_time', 'click_call_time',
                'initial_device', '_mid_source', 'campaign', 'payment_date', 'past_policy_expiry_date', 'is_qualified',
                'initial_network', 'initial_ad_category', 'latest_paid_network', 'latest_paid_ad_category',
                'first_unpaid_network', 'first_unpaid_ad_category'
            ],
        },
        'HEALTH_LEAD_PRODUCTIVITY_REPORT_MINI': {
            'query': core_models.Requirement.objects.filter(
                Q(created_date__gte=from_date) &
                Q(created_date__lte=to_date) &
                Q(kind__product='health')
            ).distinct(),
            'headings_list': [
                'id', 'mobile', 'email', 'state', 'assigned_to', 'advisor_head_disposition', 'advisor_sub_disposition',
                'last_call_time', 'next_call_time', 'created_on', 'assigned_to_time', 'click_call_time',
                'initial_device', '_mid_source', 'campaign', 'payment_date', 'past_policy_expiry_date', 'is_qualified',
                'initial_network', 'initial_ad_category', 'latest_paid_network', 'latest_paid_ad_category',
                'first_unpaid_network', 'first_unpaid_ad_category'
            ],
        },
        'MARKETING_REPORT': {
            'query': core_models.Requirement.objects.filter(
                Q(Q(user__phones__number__in=phones) | Q(visitor__phones__number__in=phones)) &
                Q(created_date__gte=from_date) &
                Q(created_date__lte=to_date)
            ).distinct(),
            'headings_list': [
                'id', 'mobile', 'created_on', '_mid_campaign', '_mid_source', '_mid_content', '_mid_medium',
                '_mid_brand', 'assigned_to', '_mid_category', 'next_crm_task_assigned_to', 'advisor_head_disposition',
                'advisor_sub_disposition', 'next_call_time', 'campaign', 'kind_kind', 'kind_sub_kind', 'kind_product',
                'assigned_to_time', 'click_call_time', 'device', 'state', 'activity_count', 'past_policy_expiry_date',
                'is_qualified', 'initial_network', 'initial_ad_category', 'latest_paid_network', 'latest_paid_ad_category',
                'first_unpaid_network', 'first_unpaid_ad_category'
            ],
        },
        'MOTOR_QUALIFIER_REPORT': {
            'query': core_models.Requirement.objects.filter(
                    Q(kind__product__in=['motor', 'car'])
                ).filter(
                Q(
                    crm_activities__in=crm_models.Activity.objects.filter(
                        start_time__range=(from_date, to_date),
                        sub_disposition__value__in=['CALL_TRANSFER', 'COLD_TRANSFER', 'INSPECTION_REQUIRED']
                    )
                ) |
                Q(
                    Q(created_date__gte=from_date) &
                    Q(created_date__lte=to_date)
                )
            ).distinct('id'),
            'headings_list': [
                'id', 'mobile', 'email', 'name', 'state', 'assigned_to', 'last_call_time', 'next_call_time',
                'advisor_head_disposition', 'advisor_sub_disposition', 'is_customer', 'customer_products', 'created_on',
                'past_policy_expiry_date', 'assigned_to_time', 'click_call_time', 'initial_device', 'network',
                'campaign', 'transfer_date', 'transferred_to', 'transfer_from', 'qualifier_assigned_to',
                'assigned_date_to_closure_team', 'click_call_date_closure', 'is_sale_happened', 'payment_date',
                'qualifier_sub_disposition', 'initial_network', 'initial_ad_category', 'latest_paid_network',
                'latest_paid_ad_category', 'first_unpaid_network', 'first_unpaid_ad_category'
            ]
        },
        'HEALTH_QUALIFIER_REPORT': {
            'query': core_models.Requirement.objects.filter(
                Q(kind__product='health') &
                Q(created_date__gte=from_date) & Q(created_date__lte=to_date)
            ).distinct(),
            'headings_list': [
                'id', 'mobile', 'email', 'name', 'state', 'assigned_to', 'last_call_time', 'next_call_time',
                'advisor_head_disposition', 'advisor_sub_disposition', 'is_customer', 'customer_products', 'created_on',
                'assigned_to_time', 'click_call_time', 'initial_device', 'network', 'campaign', 'transfer_date',
                'transferred_to', 'transfer_from', 'assigned_date_to_closure_team', 'click_call_date_closure',
                'is_sale_happened', 'payment_date', 'past_policy_expiry_date'
            ]
        },
        'MOTOR_QUALIFIER_REPORT_MINI': {
            'query': core_models.Requirement.objects.filter(
                Q(kind__product__in=['motor', 'car'])
            ).filter(
                Q(
                    crm_activities__in=crm_models.Activity.objects.filter(
                        start_time__range=(from_date, to_date),
                        sub_disposition__value__in=['CALL_TRANSFER', 'COLD_TRANSFER', 'INSPECTION_REQUIRED']
                    )
                ) |
                Q(created_date__gte=from_date) & Q(created_date__lte=to_date)
            ).distinct('id'),
            'headings_list': [
                'id', 'mobile', 'email', 'name', 'state', 'assigned_to', 'last_call_time', 'next_call_time',
                'past_policy_expiry_date', 'advisor_head_disposition', 'advisor_sub_disposition', 'created_on',
                'assigned_to_time', 'click_call_time', 'initial_device', 'network', 'campaign', 'transfer_date',
                'transferred_to', 'transfer_from', 'assigned_date_to_closure_team', 'click_call_date_closure',
                'qualifier_assigned_to', 'is_sale_happened', 'payment_date', 'qualifier_sub_disposition'
            ]
        },
        'HEALTH_QUALIFIER_REPORT_MINI': {
            'query': core_models.Requirement.objects.filter(
                Q(kind__product='health') &
                Q(created_date__gte=from_date) &
                Q(created_date__lte=to_date)
            ).distinct(),
            'headings_list': [
                'id', 'mobile', 'email', 'name', 'state', 'assigned_to', 'last_call_time', 'next_call_time',
                'advisor_head_disposition', 'advisor_sub_disposition', 'created_on', 'assigned_to_time',
                'click_call_time', 'initial_device', 'network', 'campaign', 'transfer_date', 'transferred_to',
                'past_policy_expiry_date', 'assigned_date_to_closure_team', 'click_call_date_closure',
                'is_sale_happened', 'payment_date'
            ]
        },
    }

    filename = "report_%s_%s_%s_created_on_%s.csv" % (
        rtype,
        from_date.strftime("%d-%m-%Y"),
        to_date.strftime("%d-%m-%Y"),
        datetime.datetime.now().strftime("%d-%m-%Y_%H:%M")
    )
    f = open(os.path.join(settings.UPLOAD_FOLDER, filename), 'w')
    csv_writer = csv.writer(f)

    start_time = time.time()

    HEADINGS_DICT = {
        "id": ["Requirement Id", "_s_id"],
        'kind_kind': ['Requirement Kind Id', '_kind_kind'],
        'kind_sub_kind': ['Requirement Sub Kind', '_kind_sub_kind'],
        'kind_product': ['Requirement Product', '_kind_product'],
        "mobile": ["Mobile", "primary_phone"],
        "name": ["Name", "_c_full_name"],
        "email": ["Email", "primary_email"],
        "state": ["State", lambda x: x.current_state.name if x.current_state else ''],
        "assigned_to": ["Assigned to", lambda x: x.owner.dialer_username if x.user else ''],
        "advisor_head_disposition": ["Advisor Head Disposition", lambda x: x.crm_activities.filter(
            ~Q(user__dialer_username="system")).last().disposition.verbose if x.crm_activities.filter(
            ~Q(user__dialer_username="system")) and x.crm_activities.filter(
            ~Q(user__dialer_username="system")).last().sub_disposition else ''],
        "advisor_sub_disposition": ["Advisor Sub Dispostion", lambda x: x.crm_activities.filter(
            ~Q(user__dialer_username="system")).last().sub_disposition.verbose if x.crm_activities.filter(
            ~Q(user__dialer_username="system")).last() and x.crm_activities.filter(
            ~Q(user__dialer_username="system")).last().sub_disposition else ''],
        "last_call_time": ["Last Call Date", lambda x: localtime(x.last_crm_activity.start_time).strftime(
            "%d/%m/%Y %H:%M:%S") if x.last_crm_activity and x.last_crm_activity.start_time else ''],
        "next_call_time": ["Next Call Time", lambda x: localtime(x.next_crm_task.scheduled_time).strftime(
            "%d/%m/%Y %H:%M:%S") if x.next_crm_task and x.next_crm_task.scheduled_time else ''],
        "next_crm_task_assigned_to": [
            "Next Task Assigned",
            lambda x: x.next_crm_task.assigned_to.dialer_username if x.next_crm_task and x.next_crm_task.assigned_to else ''
        ],
        "utm_campaign": ["UTM Campaign", "_mid_campaign"],
        "utm_source": ["UTM Source", "_mid_source"],
        "utm_keyword": ["UTM Keyword", "_act_term"],
        "utm_term": ["UTM Term", "_act_term"],
        "im_source": ["IM Source", "_mid_source"],
        "im_medium": ["IM Medium", "_mid_medium"],
        "im_campaign": ["IM Campaign", "_mid_campaign"],
        "im_content": ["IM Content", "_mid_content"],
        "im_term": ["IM Term", "_act_term"],
        "created_on": ["Created on Date", lambda x: localtime(x.created_date).strftime("%d/%m/%Y %H:%M:%S")],
        "device": ["Device", "_act_device"],
        "brand": ["Brand", "_mid_brand"],
        "network": ["Network", "_mid_source"],
        "ad-category": ["Ad-Category", "_mid_category"],
        "label": ["Label", "_act_label"],
        "keyword": ["Keyword", "_act_term"],
        "keyword_category": ["Keyword Category", "_act_term_category"],
        "referer": ["Referer", "_act_referer"],
        "triggered_page": ["Triggered Page", "_act_url"],
        "campaign": ["Campaign", 'campaign'],
        "attempts_made": ["Attempts made", lambda x: x.crm_activities.count()],
        "vt_adposition": ["vt_adposition", "_act_label"],
        "vt_creative": ["vt_creative", "_mid_content"],
        "vt_keyword": ["vt_keyword", "_act_term"],
        "vt_matchtype": ["vt_matchtype", "_act_match_type"],
        "vt_placement": ["vt_placement", "_act_vt_placement"],
        "vt_source": ["vt_source", "_mid_source"],
        "vt_target": ["vt_target", "_act_vt_target"],
        "landing_page_url": ["landing_page_url", "_act_landing_url"],
        "gclid": ["gclid", "_act_gclid"],
        "calculated_city": ["calculated_city", "_act_city"],
        "product_type": ["Product Type", lambda x: x.kind.product.lower() if x.kind.product else ''],
        "is_sale_happened": ["Is Sale happened", lambda x: '1' if x.payment.filter(status="success") else '0'],
        "payment_date": [
            "Payment Date",
            lambda x: x.payment.filter(status="success").latest('id').modified_date if x.payment.filter(
                status="success") else ''
        ],
        "assigned_to_time": [
            "Assigned Time",
            lambda x: localtime(x.tasks.order_by('id')[0].pushed_time).strftime(
                "%d/%m/%Y %H:%M:%S") if x.tasks.order_by('id') and x.tasks.order_by('id')[0].pushed_time else ''
        ],
        "click_call_time": ["Click call Time", lambda x: localtime(
            x.crm_activities.exclude(user__dialer_username='system').order_by('id')[0].start_time).strftime(
            "%d/%m/%Y %H:%M:%S") if x.crm_activities.order_by('id') and x.crm_activities.order_by('id')[
            0].start_time else ''],
        "initial_device": ["Initial Device", lambda x: initial_activity.device if initial_activity else ''],
        "initial_network": ["Initial Network", '_initial_source'],
        "initial_ad_category": ["Initial Ad-Category", '_initial_category'],
        "is_customer": ["Is Customer", "is_customer"],
        "customer_products": ["Customer Products", "customer_products"],
        "transfer_date": ["Transfer Date", 'trans_qual_end_time'],
        "transfer_from": ["Transfer From", 'trans_qual_user'],
        "transferred_to": ["Tranferred to", '_closure_assigned_to'],
        "assigned_date_to_closure_team": ["Assigned Date to Closure Team", '_closure_created_on'],
        "qualifier_assigned_to": ["Qualifier Assigned To", 'qualifier_assigned_to'],
        "click_call_date_closure": ["Click Call Date(Closure)", '_closure_pushed_time'],
        "past_policy_expiry_date": ["Past Policy Expiry Date", "past_policy_expiry_date"],
        '_mid_campaign': ['MID Campaign', '_mid_campaign'],
        '_mid_source': ['MID Source', "_mid_source"],
        '_mid_content': ["MID Content", '_mid_content'],
        '_mid_medium': ['MID Medium', '_mid_medium'],
        '_mid_brand': ['MID Brand', '_mid_brand'],
        '_mid_category': ['MID Category', '_mid_category'],
        'activity_count': ['Advisor Activity Count', 'activity_count'],
        'qualifier_sub_disposition': ['Qualifier Moved to bucket', 'qualifier_sub_disposition'],
        'is_qualified': ['Is Qualified', 'is_qualified'],
        'latest_paid_network': ['Latest Paid Network', '_paid_source'],
        'latest_paid_ad_category': ['Latest Paid Ad-Category', '_paid_category'],
        'first_unpaid_network': ['First Unpaid Network', '_unpaid_source'],
        'first_unpaid_ad_category': ['First Unpaid Ad-Category', '_unpaid_category'],
    }

    REPORT_QUERY = REPORT_TYPE_QUERY[rtype]['query']
    HEADINGS = []
    for item in REPORT_TYPE_QUERY[rtype]['headings_list']:
        HEADINGS.append(HEADINGS_DICT[item])

    csv_writer.writerow([j[0] for j in HEADINGS])
    print "total objects = > " + str(REPORT_QUERY.count())
    qualifier_advisors = crm_models.Advisor.objects.filter(
        Q(Q(queues__name__icontains='qualifier') | Q(queues__name='Car Justdial Cardekho')) &
        Q(queueadvisor__is_manager=False)
    ).distinct()
    bucket_display_val = {
        '': 'NA',
        'CALL_TRANSFER': 'Warm Transfer',
        'INSPECTION_REQUIRED': 'Offline Case',
        'COLD_TRANSFER': 'Cold Transfer'
    }
    closure_advisors = crm_models.Advisor.objects.filter(
        queueadvisor__is_manager=False
    ).exclude(
        Q(queues__name__icontains='qualifier') |
        Q(queues__name='Car Justdial Cardekho') |
        Q(dialer_username="system")
    ).distinct()

    for requirement in REPORT_QUERY:
        print requirement.id
        print datetime.datetime.now()
        qualifier_task = None
        closure_task = None
        kind = requirement.kind
        if 'QUALIFIER' in rtype:
            qualifier_task = requirement.crm_activities.filter(
                Q(disposition__value='QUALIFIED') &
                Q(
                    Q(sub_disposition__value='CALL_TRANSFER') |
                    Q(sub_disposition__value='INSPECTION_REQUIRED') |
                    Q(sub_disposition__value='COLD_TRANSFER')
                )
            ).first()

        closure_task = requirement.tasks.filter(assigned_to__in=closure_advisors).first()

        r_activity = requirement.last_web_activity
        if r_activity:
            r_mid = r_activity.mid
        else:
            r_mid = requirement.mid
        user = requirement.user or requirement.visitor
        if user:
            if not user.primary_phone:
                pass
        initial_activity = core_models.Activity.objects.filter(
            requirement=requirement).order_by('id')[0] if core_models.Activity.objects.filter(
            requirement=requirement) else None

        paid_activity = core_models.Activity.objects.filter(
            requirement=requirement,
            mid__source__in=PAID_NETWORKS
        ).last()

        unpaid_activity = core_models.Activity.objects.filter(
            requirement=requirement,
            mid__source__in=UNPAID_NETWORKS
        ).first()
        arr = []
        for head, attr in HEADINGS:
            try:
                if isinstance(attr, str):
                    if attr.startswith('_c_'):
                        arr.append(getattr(user, attr[3:], ''))
                    elif attr.startswith('_s_'):
                        arr.append(getattr(requirement, attr[3:], ''))
                    elif attr.startswith('_mid_'):
                        arr.append(getattr(r_mid, attr[5:], ''))
                    elif attr.startswith('_act_'):
                        arr.append(getattr(r_activity, attr[5:], ''))
                    elif attr.startswith('_kind_'):
                        arr.append(getattr(kind, attr[6:], ''))
                    elif attr.startswith('_paid_'):
                        arr.append(getattr(paid_activity.mid, attr[6:], ''))
                    elif attr.startswith('_unpaid_'):
                        if unpaid_activity:
                            arr.append(getattr(unpaid_activity.mid, attr[8:], 'DIRECT'))
                        else:
                            arr.append('')
                    elif attr.startswith('_initial_'):
                        arr.append(getattr(initial_activity.mid, attr[9:], ''))
                    elif attr.startswith('_closure_', ):
                        if attr[9:] == 'assigned_to':
                            arr.append(getattr(getattr(closure_task, attr[9:], ''), 'dialer_username', ''))
                        else:
                            dt = getattr(closure_task, attr[9:], '')
                            if dt:
                                dt = localtime(dt).strftime("%d/%m/%Y %H:%M:%S")
                            arr.append(dt)
                    else:
                        if attr == "is_customer":
                            arr.append(
                                "True" if core_models.Policy.objects.filter(user=requirement.user) > 1 else "False")
                        elif attr == "customer_products":
                            policies = core_models.Policy.objects.filter(user=requirement.user)
                            products = ""
                            if policies.count() > 1:
                                for policy in policies:
                                    products += policy.requirement_sold.kind.product + " "
                            arr.append(products)
                        elif attr == "primary_phone":
                            arr.append(user.primary_phone)
                        elif attr == "primary_email":
                            if user:
                                arr.append(user.primary_email)
                            else:
                                arr.append('')
                        elif attr == 'activity_count':
                            arr.append(requirement.crm_activities.count())
                        elif attr == 'trans_qual_end_time':
                            cdt = getattr(qualifier_task, attr[11:], '')
                            if cdt:
                                cdt = localtime(cdt).strftime("%d/%m/%Y %H:%M:%S")
                            arr.append(cdt)
                        elif attr == 'trans_qual_user':
                            arr.append(getattr(getattr(qualifier_task, attr[11:], ''), 'dialer_username', ''))
                        elif attr == 'qualifier_sub_disposition':
                            calculated_key = getattr(getattr(qualifier_task, 'sub_disposition', ''), 'value', '')
                            arr.append(bucket_display_val[calculated_key])
                        elif attr == 'campaign':
                            crm_activities = requirement.tasks.order_by('-id')
                            if crm_activities:
                                arr.append(crm_activities[0].queue.name)
                            else:
                                arr.append('')
                        elif attr == 'qualifier_assigned_to':
                            qualifier_activity = requirement.crm_activities.filter(user__in=qualifier_advisors).first()
                            if qualifier_activity:
                                arr.append(qualifier_activity.user.dialer_username)
                            else:
                                arr.append('')
                        elif attr == 'past_policy_expiry_date':
                            arr.append(requirement.extra.get('past_policy_expiry_date', 'NA'))
                        elif attr == 'is_qualified':
                            arr.append('True' if requirement.state_history.filter(parent_state__name='QUALIFIED') else 'False')
                        else:
                            arr.append('')
                elif type(attr) == type(lambda x: x):
                    arr.append(attr(requirement))
                else:
                    arr.append('')
            except:
                arr.append("")
                pass
        csv_writer.writerow([unicode(s).encode("utf-8") for s in arr])
    print "Report Calculation", time.time() - start_time
    f.close()
    return filename
