import os
import csv
from django.db.models import Q
from core import models as core_models
from crm import models as crm_models
from health_product.models import Transaction as HealthTransaction
from motor_product.models import Transaction as MotorTransaction
from travel_product.models import Transaction as TravelTransaction
import datetime
import time
from django.conf import settings
from django.utils.timezone import localtime

REPORT_TYPE = [
    ('SALES_REPORT', 'Sales Report Using Transaction'),
]

PAID_NETWORKS = [
    'SEARCH', 'justdial', 'facebook', 'Autoportal', 'MySmartPrice', 'Yahoo', 'Team-BHP', 'GSP',
    'DISPLAY', 'affiliate', 'CarTrade', 'cardekho', 'DisplayNetwork', 'taboola', 'SearchNetwork',
    'truebill', 'jago', 'Overdrive', 'justdial-app', 'adsurfmedia'
]

UNPAID_NETWORKS = ['ORGANIC', 'INCOMING', 'email', '2015leads', 'im_email', 'im_sms', 'email.']

REPORT_TYPE_VERBOSE_MAP = {item[0]: item[1] for item in REPORT_TYPE}

MIDNIGHT = datetime.datetime.today().replace(hour=23, minute=59, second=59)


def get_sales_report(from_date, to_date):
    transaction_models = [MotorTransaction]
    transactions = []
    for tm in transaction_models:
        transactions = tm.objects.filter(
            Q(
                Q(
                    Q(status="COMPLETED") |
                    Q(status="MANUAL COMPLETED")
                ) &
                Q(
                    Q(Q(payment_on__gte=from_date) & Q(payment_on__lte=to_date)) |
                    Q(Q(created_on__gte=from_date) & Q(created_on__lte=to_date))
                )
            ) &
            Q(vehicle_type="Private Car")
        ).distinct()

    headings_list = [
        'id', 'mobile', 'name', 'email', 'state', 'assigned_to', 'advisor_head_disposition', 'advisor_sub_disposition',
        'kind_kind', 'kind_sub_kind', 'kind_product', 'transaction_status', 'is_customer', 'customer_products',
        'last_call_time', 'next_call_time', 'utm_campaign', 'created_on', 'assigned_to_time', 'click_call_time',
        'initial_device', 'initial_network', 'device', 'brand', 'network', 'ad-category', 'label', 'keyword',
        'keyword_category', 'referer', 'triggered_page', 'campaign', 'attempts_made', 'im_medium', 'im_content',
        'landing_page_url', 'gclid', 'calculated_city', 'transaction_id', 'transaction_created_on', 'payment_mode',
        'transaction_payment_date', 'product_type', 'make', 'model', 'rto', 'insurer', 'premium_paid', 'mode',
        'initial_ad_category', 'latest_paid_network', 'next_crm_task_assigned_to', 'past_policy_expiry_date',
        'first_unpaid_ad_category', 'first_unpaid_network', 'click_call_date_closure', 'latest_paid_ad_category',
        'transaction_modified_on'
    ]

    filename = "report_%s_%s_%s_created_on_%s.csv" % (
        "Sales_Report",
        from_date.strftime("%d-%m-%Y"),
        to_date.strftime("%d-%m-%Y"),
        datetime.datetime.now().strftime("%d-%m-%Y_%H:%M")
    )
    f = open(os.path.join(settings.UPLOAD_FOLDER, filename), 'w')
    csv_writer = csv.writer(f)

    start_time = time.time()

    HEADINGS_DICT = {
        "id": ["Transaction Id", "_txn_id"],
        'kind_kind': ['Requirement Kind Id', '_kind_kind'],
        'kind_sub_kind': ['Requirement Sub Kind', '_kind_sub_kind'],
        'kind_product': ['Requirement Product', '_kind_product'],
        'transaction_status': ['Transaction Status', '_txn_status'],
        "mobile": ["Mobile", "_c_mobile"],
        "name": ["Name", "_c_full_name"],
        "email": ["Email", "_c_email"],
        "state": ["State", lambda x: x.current_state.name if x.current_state else ''],
        "assigned_to": ["Assigned to", lambda x: x.owner.dialer_username if x.owner else ''],
        "advisor_head_disposition": ["Advisor Head Disposition", lambda x: x.crm_activities.filter(
            ~Q(user__dialer_username="system")).last().disposition.verbose if x.crm_activities.filter(
            ~Q(user__dialer_username="system")) and x.crm_activities.filter(
            ~Q(user__dialer_username="system")).last().sub_disposition else ''],
        "advisor_sub_disposition": ["Advisor Sub Dispostion", lambda x: x.crm_activities.filter(
            ~Q(user__dialer_username="system")).last().sub_disposition.verbose if x.crm_activities.filter(
            ~Q(user__dialer_username="system")).last() and x.crm_activities.filter(
            ~Q(user__dialer_username="system")).last().sub_disposition else ''],
        "last_call_time": ["Last Call Date", lambda x: localtime(x.last_crm_activity.start_time).strftime(
            "%d/%m/%Y %H:%M:%S") if x.last_crm_activity and x.last_crm_activity.start_time else ''],
        "next_call_time": ["Next Call Time", lambda x: localtime(x.next_crm_task.scheduled_time).strftime(
            "%d/%m/%Y %H:%M:%S") if x.next_crm_task and x.next_crm_task.scheduled_time else ''],
        "next_crm_task_assigned_to": [
            "Next Task Assigned",
            lambda x: x.next_crm_task.assigned_to.dialer_username if x.next_crm_task and x.next_crm_task.assigned_to else ''
        ],
        "utm_campaign": ["UTM Campaign", "_mid_campaign"],
        "im_medium": ["IM Medium", "_mid_medium"],
        "im_content": ["IM Content", "_mid_content"],
        "created_on": ["Created on Date", lambda x: localtime(x.created_date).strftime("%d/%m/%Y %H:%M:%S")],
        "device": ["Device", "_act_device"],
        "brand": ["Brand", "_mid_brand"],
        "network": ["Network", "_mid_source"],
        "ad-category": ["Ad-Category", "_mid_category"],
        "label": ["Label", "_act_label"],
        "keyword": ["Keyword", "_act_term"],
        "keyword_category": ["Keyword Category", "_act_term_category"],
        "referer": ["Referer", "_act_referer"],
        "triggered_page": ["Triggered Page", "_act_url"],
        "campaign": ["Campaign", 'campaign'],
        "attempts_made": ["Attempts made", lambda x: x.crm_activities.count()],
        "landing_page_url": ["landing_page_url", "_act_landing_url"],
        "gclid": ["gclid", "_act_gclid"],
        "product_type": ["Product Type", lambda x: x.kind.product.lower() if x.kind.product else ''],
        "assigned_to_time": [
            "Assigned Time",
            lambda x: localtime(x.tasks.order_by('id')[0].pushed_time).strftime(
                "%d/%m/%Y %H:%M:%S") if x.tasks.order_by('id') and x.tasks.order_by('id')[0].pushed_time else ''
        ],
        "click_call_time": ["Click call Time", lambda x: localtime(
            x.crm_activities.exclude(user__dialer_username='system').order_by('id')[0].start_time).strftime(
            "%d/%m/%Y %H:%M:%S") if x.crm_activities.order_by('id') and x.crm_activities.order_by('id')[
            0].start_time else ''],
        "initial_device": ["Initial Device", lambda x: initial_activity.device if initial_activity else ''],
        "initial_network": ["Initial Network", '_initial_source'],
        "initial_ad_category": ["Initial Ad-Category", '_initial_category'],
        "is_customer": ["Is Customer", "is_customer"],
        "customer_products": ["Customer Products", "customer_products"],
        "click_call_date_closure": ["Click Call Date(Closure)", '_closure_pushed_time'],
        "past_policy_expiry_date": ["Past Policy Expiry Date", "past_policy_expiry_date"],
        "calculated_city": ["calculated_city", "_act_city"],
        "transaction_id": ["transaction_id", "_txn_transaction_id"],
        "transaction_created_on": ["Transaction created_on", "_txn_created_on"],
        "transaction_modified_on": ["Transaction modified_on", "_txn_updated_on"],
        "transaction_payment_date": ["Payment Date", '_txn_payment_on'],
        "insurer": ["Insurer", "_txn_insurer.title"],
        "premium_paid": ["Premium Paid", "_txn_premium_paid"],
        "payment_mode": ["Payment Mode", "_quote_type"],
        "mode": ["Mode", 'paymentmode'],
        "make": ["Make", "_vehicle_make"],
        "model": ["Model", "_vehicle_model"],
        "rto": ["RTO", "_quote_rto"],
        'latest_paid_network': ['Latest Paid Network', '_paid_source'],
        'latest_paid_ad_category': ['Latest Paid Ad-Category', '_paid_category'],
        'first_unpaid_network': ['First Unpaid Network', '_unpaid_source'],
        'first_unpaid_ad_category': ['First Unpaid Ad-Category', '_unpaid_category'],
    }

    HEADINGS = []
    for item in headings_list:
        HEADINGS.append(HEADINGS_DICT[item])

    csv_writer.writerow([j[0] for j in HEADINGS])
    qualifier_advisors = crm_models.Advisor.objects.filter(
        Q(Q(queues__name__icontains='qualifier') | Q(queues__name='Car Justdial Cardekho')) &
        Q(queueadvisor__is_manager=False)
    ).distinct()

    closure_advisors = crm_models.Advisor.objects.filter(
        queueadvisor__is_manager=False
    ).exclude(
        Q(queues__name__icontains='qualifier') |
        Q(queues__name='Car Justdial Cardekho') |
        Q(dialer_username="system")
    ).distinct()
    print "total objects = > " + str(transactions.count())

    for obj in transactions:
        print obj.id
        user = obj.proposer
        r_mid = None
        r_activity = None
        kind = None
        initial_activity = None
        paid_activity = None
        unpaid_activity = None
        qualifier_task = None
        closure_task = None
        quote = obj.quote
        vehicle = quote.vehicle if quote else None

        requirement = core_models.Requirement.objects.search_by_mobile(
            obj.proposer.mobile, include_concluded=True
        ).filter(
            Q(kind__product__in=['motor', 'car', 'bike']) &
            Q(created_date__lt=(obj.payment_on or obj.created_on))
        ).exclude(current_state__parent_state__name__in=['DUPLICATE', 'DUMMY', 'FRESH']).last()

        if requirement:
            r_mid = requirement.mid
            kind = requirement.kind
            phones = core_models.Phone.objects.filter(number=user.mobile)
            visitor_ids = phones.values_list('visitor_id', flat=True)
            user_ids = phones.values_list('user_id', flat=True)
            customer_activities =  core_models.Activity.objects.filter(
                Q(created__lte=(obj.payment_on or obj.created_on)) &
                Q(created__gte= datetime.datetime(day=20,year=2016, month=2)) &
                ~Q(ip_address__in=['182.72.40.134', '182.74.245.218', '58.68.3.58', '124.124.12.137'])&
                Q(Q(user_id__in=user_ids) | Q(tracker_id__in=visitor_ids))
            ).order_by('created')
            r_activity = customer_activities.last()
            initial_activity = customer_activities.first()

            qualifier_task = requirement.crm_activities.filter(
                Q(disposition__value='QUALIFIED') &
                Q(
                    Q(sub_disposition__value='CALL_TRANSFER') |
                    Q(sub_disposition__value='INSPECTION_REQUIRED') |
                    Q(sub_disposition__value='COLD_TRANSFER')
                )
            ).first()

            closure_task = requirement.tasks.filter(assigned_to__in=closure_advisors).first()

            if r_activity:
                r_mid = r_activity.mid
            else:
                r_mid = requirement.mid

            paid_activity = customer_activities.filter(
                mid__source__in=PAID_NETWORKS
            ).last()

            unpaid_activity = customer_activities.filter(
                mid__source__in=UNPAID_NETWORKS
            ).first()

        arr = []
        for head, attr in HEADINGS:
            try:
                if isinstance(attr, str):
                    if attr.startswith('_txn_'):
                        arr.append(getattr(obj, attr[5:], ''))
                    elif attr.startswith('_vehicle_'):
                        arr.append(getattr(vehicle, attr[9:], ''))
                    elif attr.startswith('_quote_'):
                        arr.append(getattr(quote, attr[7:], ''))
                    elif attr.startswith('_c_'):
                        if attr[3:] == "full_name":
                            arrappend(getattr(user, 'first_name', '') + ' ' + getattr(user, 'last_name', ''))
                        else:
                            arr.append(getattr(user, attr[3:], ''))
                    elif attr.startswith('_s_'):
                        arr.append(getattr(requirement, attr[3:], ''))
                    elif attr.startswith('_mid_'):
                        arr.append(getattr(r_mid, attr[5:], ''))
                    elif attr.startswith('_act_'):
                        arr.append(getattr(r_activity, attr[5:], ''))
                    elif attr.startswith('_kind_'):
                        arr.append(getattr(kind, attr[6:], ''))
                    elif attr.startswith('_paid_'):
                        arr.append(getattr(paid_activity.mid, attr[6:], ''))
                    elif attr.startswith('_unpaid_'):
                        if unpaid_activity:
                            arr.append(getattr(unpaid_activity.mid, attr[8:], 'DIRECT'))
                        else:
                            arr.append('')
                    elif attr.startswith('_initial_'):
                        arr.append(getattr(initial_activity.mid, attr[9:], ''))
                    elif attr.startswith('_closure_', ):
                        if attr[9:] == 'assigned_to':
                            arr.append(getattr(getattr(closure_task, attr[9:], ''), 'dialer_username', ''))
                        else:
                            dt = getattr(closure_task, attr[9:], '')
                            if dt:
                                dt = localtime(dt).strftime("%d/%m/%Y %H:%M:%S")
                            arr.append(dt)
                    else:
                        if attr == "is_customer":
                            arr.append(
                                "True" if core_models.Policy.objects.filter(user=requirement.user) > 1 else "False")
                        elif attr == "customer_products":
                            policies = core_models.Policy.objects.filter(user=requirement.user)
                            products = ""
                            if policies.count() > 1:
                                for policy in policies:
                                    products += policy.requirement_sold.kind.product + " "
                            arr.append(products)
                        elif attr == 'activity_count':
                            arr.append(requirement.crm_activities.count())
                        elif attr == 'trans_qual_end_time':
                            cdt = getattr(qualifier_task, attr[11:], '')
                            if cdt:
                                cdt = localtime(cdt).strftime("%d/%m/%Y %H:%M:%S")
                            arr.append(cdt)
                        elif attr == 'trans_qual_user':
                            arr.append(getattr(getattr(qualifier_task, attr[11:], ''), 'dialer_username', ''))
                        elif attr == 'campaign':
                            crm_activities = requirement.tasks.order_by('-id')
                            if crm_activities:
                                arr.append(crm_activities[0].queue.name)
                            else:
                                arr.append('')
                        elif attr == 'past_policy_expiry_date':
                            arr.append(requirement.extra.get('past_policy_expiry_date', 'NA'))
                        elif attr == 'is_qualified':
                            arr.append('True' if requirement.state_history.filter(parent_state__name='QUALIFIED') else 'False')
                        elif attr == "paymentmode":
                            arr.append(quote.raw_data.get('payment_mode') if quote else 'NA')
                        else:
                            arr.append('')
                elif type(attr) == type(lambda x: x):
                    arr.append(attr(requirement))
                else:
                    arr.append('')
            except:
                arr.append("!!Error Value!!")
                pass
        csv_writer.writerow([unicode(s).encode("utf-8") for s in arr])
    print "Report Calculation", time.time() - start_time
    f.close()
    return filename
