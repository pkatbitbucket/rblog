var ko = require('knockout')
var urls = require('urls')
var store = require("models/store");


var CreateTicketForm = function(params) {
    var self = this;
    self.create_ticket_form = params.form;
    self.policy = ko.observable(false);
    self.create_ticket_success = ko.observable(false);
    self.policy_id = ko.observable(params.policy_id);
    self.ticket_id = ko.observable();
    self.params = params;
    if(params.ticket_id){
        self.ticket_id(params.ticket_id());
    }
}
CreateTicketForm.prototype.create_ticket = function(){
    var self = this;
    var create_ticket_form = $('#createTicketFormId' + self.params.policy_id);
    create_ticket_data = create_ticket_form.serializeObject();
    $.ajax({
        type: "POST",
        url: (function(){
            if(self.ticket_id()){
                return UPDATE_TICKET_FORM_URL.replace('7979797979', self.ticket_id())
            }
            else{
                return CREATE_TICKET_FORM_URL
            }
        })(),
        data: create_ticket_data,
        success: function(data) {
            responseData = jQuery.parseJSON(data);
            if(responseData.success) {
                self.create_ticket_success(true);
                alert("New Ticket created with Ticket ID - " + responseData.response.ticket_id);
                if(responseData.response.next_task_user_id == user_id){
                    urls.hasher.setHash('ticket/' + responseData.response.ticket_id);
                }
                else{
                    urls.hasher.setHash('ticket-list');
                }
            }else{
                alert("Error!");
                $.each(responseData.errors, function(error_name, error_message){
                    alert($("#" + error_name).html() + " " + error_message);
                })
            }
        },
    });
}

module.exports = CreateTicketForm;
