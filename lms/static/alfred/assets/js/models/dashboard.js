var ko = require("knockout");
var urls = require("urls");
var store = require("models/store");
// TODO: Create a separate panel map using a backend call

var dashboard_panels_map = {
    'Issue': {
        'Complaints': ['Complaint'],
        'Escalations': ['Escalation'],
    },
    'Priority': {
        'Critical': ['Critical'],
        'High': ['High'],
        'Normal': ['Normal'],
        'Low': ['Low'],
    },
    'Due Date': {
        'Overdue >15 Days': ['inf-15 days in past'],
        'Overdue 8-15 Days': ['15-8 days in past'],
        'Overdue 4-7 Days': ['7-4 days in past'],
        'Overdue 1-3': ['3-1 days in past'],
        'Due Today': ['today'],
        'Due 1-3 Days': ['1-3 days in future'],
        'Due 4-7 Days': ['4-7 days in future'],
        'Due 8-15 Days': ['8-15 days in future'],
        'Due 15> Days': ['15-inf days in future'],
    },
    'Status': {
        'Assigned': ['New'],
        'Pending Customer Notification': ['Request accepted', 'Request rejected', 'Insurer intimated'],
        'Pending Customer Information': ['Documents pending'],
        'Pending Ops Information': ['Documents received', 'Documents uploaded', 'Insurer decision pending'],
        'Closed Successfully': ['Close resolved'],
        'Closed Unsuccessfully': ['Close unresolved'],
    },
    'Request Type': {
        'Endorsement': ['Endorsement'],
        'Claim': ['Claim'],
        'Cancel': ['Cancel'],
        'Refund': ['Refund'],
        'Offline': ['Offline'],
        'Clean Flow': ['Approved'],
    },
}
if(USER_ROLE == 'MANAGER'){
    dashboard_panels_map['Next Task Assigned'] = {};
    for(var i=0; i<ADVISORS_LIST.length; i++){
        dashboard_panels_map['Next Task Assigned'][ADVISORS_LIST[i]] = [ADVISORS_LIST[i]];
    }
}

subscr_dashboard_panels_map = {
    'Claim': {
        'New': ['New'],
        'Insurer intimated': ['Insurer intimated'],
        'Documents pending': ['Documents pending'],
        'Documents received': ['Documents received'],
        'Documents uploaded': ['Documents uploaded'],
        'Insurer decision pending': ['Insurer decision pending'],
        'Request accepted': ['Request accepted'],
        'Request rejected': ['Request rejected'],
        'Close resolved': ['Close resolved'],
        'Close unresolved': ['Close unresolved'],
    },
    'Cancel': {
        'New': ['New'],
        'Documents pending': ['Documents pending'],
        'Documents received': ['Documents received'],
        'Documents uploaded': ['Documents uploaded'],
        'Insurer decision pending': ['Insurer decision pending'],
        'Request accepted': ['Request accepted'],
        'Request rejected': ['Request rejected'],
        'Close resolved': ['Close resolved'],
        'Close unresolved': ['Close unresolved'],
    },
    'Refund': {
        'New': ['New'],
        'Insurer decision pending': ['Insurer decision pending'],
        'Request accepted': ['Request accepted'],
        'Close resolved': ['Close resolved'],
        'Close unresolved': ['Close unresolved'],
    },
    'Endorsement': {
        'New': ['New'],
        'Documents pending': ['Documents pending'],
        'Documents received': ['Documents received'],
        'Documents uploaded': ['Documents uploaded'],
        'Insurer decision pending': ['Insurer decision pending'],
        'Request accepted': ['Request accepted'],
        'Request rejected': ['Request rejected'],
        'Close resolved': ['Close resolved'],
        'Close unresolved': ['Close unresolved'],
    },
}

var DashboardPanelList = function(route) {
    var self = this;
    self.tickets = store.ticketStore.tickets;
    filterQuery = (function(filter_on, query_string, filter_objs) {
        if(typeof(filter_objs) == 'undefined'){
            filter_objs = self.tickets()
        }

        return ko.utils.arrayFilter(filter_objs, function (filter_obj) {
            if(ko.isObservable(filter_obj[filter_on])){
                if(typeof(filter_obj[filter_on]) == 'undefined'){
                    return false
                }
                return (filter_obj[filter_on]().toLowerCase().indexOf(query_string.toLowerCase()) > -1)
            }else{
                return (filter_obj[filter_on].toLowerCase().indexOf(query_string.toLowerCase()) > -1)
            }
        });
    });

    filterDateDiff = (function(filter_on, query_string, filter_objs) {
        if(typeof(filter_objs) == 'undefined'){
            filter_objs = self.tickets()
        }

        return ko.utils.arrayFilter(filter_objs, function (filter_obj) {
            if(typeof(filter_obj[filter_on]) == 'undefined'){
                return false
            }
            filter_obj_date = new Date(filter_obj[filter_on].split(" ")[0].split("/").reverse().join("-"))
            filter_obj_date.setHours(0, 0, 0, 0);
            var now_date = new Date()
            now_date.setHours(0)
            now_date.setMinutes(0)
            now_date.setSeconds(0)
            now_date.setMilliseconds(0)
            if(query_string == 'today'){
                return (filter_obj_date.getTime() == now_date.getTime())
            }
            date_diff = query_string.split(" ")[0]
            time_period_type = query_string.split(" ")[1]
            when = query_string.split(time_period_type + " ")[1]
            switch(when){
                case "in future":
                    var future_start_date = new Date()
                    future_start_date.setHours(0)
                    future_start_date.setMinutes(0)
                    future_start_date.setSeconds(0)
                    future_start_date.setMilliseconds(0)
                    var future_end_date = new Date()
                    future_end_date.setHours(0)
                    future_end_date.setMinutes(0)
                    future_end_date.setSeconds(0)
                    future_end_date.setMilliseconds(0)
                    if((date_diff == 'all')){
                        return (filter_obj_date > now_date)
                    }
                    future_start_date.setDate(future_start_date.getDate() + parseInt(date_diff.split("-")[0]))
                    if(date_diff.split("-")[1] != 'inf'){
                        future_end_date.setDate(future_end_date.getDate() + parseInt(date_diff.split("-")[1]))
                        return ((future_start_date <= filter_obj_date) && (filter_obj_date <= future_end_date))
                    }
                    return (future_start_date <= filter_obj_date)
                case "in past":
                    var past_start_date = new Date()
                    past_start_date.setHours(0)
                    past_start_date.setMinutes(0)
                    past_start_date.setSeconds(0)
                    past_start_date.setMilliseconds(0)
                    var past_end_date = new Date()
                    past_end_date.setHours(0)
                    past_end_date.setMinutes(0)
                    past_end_date.setSeconds(0)
                    past_end_date.setMilliseconds(0)
                    if((date_diff == 'all')){
                        return (filter_obj_date < now_date)
                    }
                    past_end_date.setDate(past_end_date.getDate() - parseInt(date_diff.split("-")[1]))
                    if(date_diff.split("-")[0] != 'inf'){
                        past_start_date.setDate(past_start_date.getDate() - parseInt(date_diff.split("-")[0]))
                        return ((past_start_date <= filter_obj_date) && (filter_obj_date <= past_end_date))
                    }
                    return (filter_obj_date <= past_end_date)
                default:
                    console("incorrect when " + when)
                    return false
            }
        });
    });

    filterIn = (function(filter_on, query_string, filter_objs) {
        if(typeof(filter_objs) == 'undefined'){
            filter_objs = self.tickets()
        }

        return ko.utils.arrayFilter(filter_objs, function (filter_obj) {
            value = false
            if(typeof(filter_obj[filter_on]) == 'undefined'){
                return false
            }
            if(filter_obj[filter_on]){
                for(i=0;i<filter_obj[filter_on].length;i++){
                    value = (filter_obj[filter_on][i].toLowerCase().indexOf(query_string.toLowerCase()) > -1)
                    if(value){
                        break
                    }
                }
                return value
            }else{
                return false
            }
        });
    });

    makeCounts = (function(panel_name, subscr_panel_name) {
        if(typeof(subscr_panel_name) == 'undefined'){
            subscr_panel_name = ''
            tile_map = dashboard_panels_map[panel_name]
        }else{
            tile_map = subscr_dashboard_panels_map[subscr_panel_name]
        }

        tile_counts = {}
        $.each(tile_map, function(tile_name, tile_options){
            tile_counts[tile_name] = 0
            for(var i=0;i<tile_options.length;i++){
                switch(panel_name){
                    case "Due Date":
                        tile_counts[tile_name] += filterDateDiff("due_date_ticket", tile_options[i], filterQuery('request', subscr_panel_name)).length
                        break
                    case "Status":
                        tile_counts[tile_name] += filterQuery("state", tile_options[i], filterQuery('request', subscr_panel_name)).length
                        break
                    case "Issue":
                        tile_counts[tile_name] += filterIn("tags", tile_options[i], filterQuery('request', subscr_panel_name)).length
                        break
                    case "Next Task Assigned":
                        tile_counts[tile_name] += filterQuery("task_assigned_to", tile_options[i], filterQuery('request', subscr_panel_name)).length
                        break
                    case "Priority":
                        tile_counts[tile_name] += filterQuery("priority", tile_options[i], filterQuery('request', subscr_panel_name)).length
                        break
                    case "Request Type":
                        tile_counts[tile_name] += filterQuery('request', tile_options[i], filterQuery('request', subscr_panel_name)).length
                        break
                    default:
                        console.log("Incorrect Name " + panel_name)
                }
            }
        });
        return tile_counts
    });

    createDashboardPanel = (function(panel_name){
        panel_counts = {
            'name': panel_name,
            'is_expanded': false,
            'tiles': [],
        }
        total_count = 0
        $.each(makeCounts(panel_name), function(tile_name, tile_count){
            panel_counts.tiles.push({'name': tile_name, 'count': tile_count})
            total_count += tile_count
        })
        panel_counts.name = panel_name + " - " + total_count
        return panel_counts
    })

    createDashboardSubscrPanel = (function(subscr_panel_name){
        subscr_panel_counts = {
            'name': "Status for " + subscr_panel_name + " - " + filterQuery('request', subscr_panel_name).length,
            'is_expanded': false,
            'tiles': [],
        }
        $.each(makeCounts("Status", subscr_panel_name), function(tile_name, tile_count){
            subscr_panel_counts.tiles.push({'name': tile_name, 'count': tile_count})
        })
        return subscr_panel_counts
    })

    self.panels = ko.pureComputed(function() {
        dashboard_panels = []

        $.each(dashboard_panels_map, function(panel_name, tile_options){
            dashboard_panels.push(createDashboardPanel(panel_name))
        });

        $.each(subscr_dashboard_panels_map, function(subscr_panel_name, tile_options){
            dashboard_panels.push(createDashboardSubscrPanel(subscr_panel_name))
        });

        return dashboard_panels;
    }, self);
}

ko.components.register('dashboard-panel', {
    viewModel: function(params) {
        var self = this;
        self.panel = params.value;
        self.show_detailed_view = ko.observable(false)
        self.open_detailed_view = (function(self){
            if(self.show_detailed_view())
                self.show_detailed_view(false)
            else
                self.show_detailed_view(true)
        })
    },
    template: require("raw!templates/dashboard-panel.html")
});

ko.components.register('dashboard-panel-tile', {
    viewModel: function(params) {
        var self = this;
        self.panel_tile = params.value;
        self.panel_verbose = params.filters.verbose.split(" - ")[0].split(" for ")[0].split(" ").join("_");
        self.panel_option = params.filters.option.split(" ").join("_");

        self.show_tickets = function() {
            filter_json = self.panel_verbose + ":" + self.panel_option;
            if(params.filters.verbose.split(" - ")[0].split(" for ").length == 2){
                for(verbose in dashboard_panels_map['Status']){
                    if(dashboard_panels_map['Status'][verbose].indexOf(self.panel_option) > -1){
                        self.panel_option = verbose
                    }
                }

                request_panel_verbose = 'Request Type';
                request_panel_option = params.filters.verbose.split(" - ")[0].split(" for ")[1];
                filter_json = self.panel_verbose + ":" + self.panel_option + "+" + request_panel_verbose + ":" + request_panel_option
            }
            urls.hasher.setHash('ticket-list/' + filter_json);
        }
    },
    template: require("raw!templates/dashboard-panel-tile.html")
});


module.exports = DashboardPanelList;
