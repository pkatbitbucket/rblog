var ko = require('knockout');
var urls = require("urls");

var SingleDocument = function(data) {
    var self = this;
    self.pk = ko.observable();
    self.category = ko.observable({});
    self.fileDetails = ko.observable();
    self.file = ko.observable();
    if(data) {
        self.pk(data.pk);
        if(data.category){
            self.category(data.category);
        }
        self.file(data.file);
    }
}

var TicketDocument = function(params){
    var self = this;
    var sd = params.suggested_documents();
    self.suggested_documents = ko.observableArray();

    var docs = sd || [];
    for(var i=0; i < docs.length; i++){
        self.suggested_documents.push(new SingleDocument(docs[i]));
    }
    self.document_categories = ko.observableArray(params.document_categories() || []);
    self.existing_documents = ko.observableArray([]);
    for(var i=0; i<params.existing_documents().length; i++){
        self.existing_documents.push(new SingleDocument(params.existing_documents()[i]));
    }

    self.addDocument = function() {
        self.suggested_documents.push(
            new SingleDocument()
        );
    };
    self.downloadDoc = function(doc_id){
        $.get(S3_DOC_DOWNLOAD_URL.replace("6666666666", doc_id()), function(data){
            window.open(data.document_url, '_blank');
        });
    }
    self.deleteExistingDoc = function(document){
        self.existing_documents.remove(document);
    }
    self.removeDocument = function(document) {
        self.suggested_documents.remove(document);
    };
    params.suggested_documents(self.suggested_documents());
    params.existing_documents(self.existing_documents());
    self.existing_documents.subscribe(function(){
        params.existing_documents(self.existing_documents());
    });
    self.suggested_documents.subscribe(function(){
        params.suggested_documents(self.suggested_documents());
    });
}

module.exports = TicketDocument;
