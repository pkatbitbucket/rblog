var ko = require('knockout');
var urls = require("urls");
var store = require("models/store");
var ticket = require("models/ticket");


var IncomingCall = function(){
    var self = this;
    self.tickets = ko.observableArray();
    self.close_incoming_call_popup = function(){
        if(confirm('Are you sure you want to close?')){
            return 'modal';
        }
    }
    self.openIncomingPopup = function(phone, additionalParams){
        $('#incoming_call_modal').modal('show');
        $.ajax({
            type: 'GET',
            url: INCOMING_TICKET_CALL_POPUP,
            data: {
                'phone': phone,
            },
            success: function(data){
                self.tickets([]);
                for(var i in data.tickets){
                    self.tickets.push(new ticket(data.tickets[i]));
                }
            },
        });
        console.log('Inside openIncomingPopup-------------------------------------', phone, additionalParams);
    }
}

module.exports = IncomingCall;
