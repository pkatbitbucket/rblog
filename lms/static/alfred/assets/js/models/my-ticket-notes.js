var ko = require('knockout')
var store = require("models/store");


var MyTicketNotes = function(data) {
    var self = this;
    self.my_notes = ko.observable().extend({
        rateLimit: {timeout: 2000, method: "notifyWhenChangesStop"}
    });
    self.ticket_id = ko.observable(false);
    self.ticket_id(data.value());
    var key = 'my_notes_' + USERNAME;
    self.action_url = NOTES_FORM_URL.replace('9999999999', self.ticket_id()).replace('8888888888', key);
    self.my_notes(data.my_notes());
    self.save = function(){
        var key = 'my_notes_' + USERNAME;
        data = {
            'csrfmiddlewaretoken': CSRF_MIDDLEWARE_TOKEN
        }
        data[key] = self.my_notes();
        $.ajax({
            type: "POST",
            url: self.action_url,
            data: data,
            success: function(data) {
                resp = JSON.parse(data);
            },
        });
    }
    ko.computed(function() {
        self.my_notes();
        self.save();
    });

}

module.exports = MyTicketNotes;
