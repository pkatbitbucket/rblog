var Router = require('router');

urls = new Router({
    routes: [{
            url: '',
            params: { page: 'ticket-list'}
        }, {
            url: 'ticket/{ticket_id}',
            params: { page: 'ticket-detail'}
        }, {
            url: 'some',
            params: { page: 'some-page'}
        }, {
            url: 'about',
            params: { page: 'about-page'}
        }, {
            url: 'policy-list',
            params: { page: 'policy-list'}
        }, {
            url: 'dashboard',
            params: { page: 'dashboard'}
        }, {
            url: 'ticket-list',
            params: { page: 'ticket-list'}
        }, {
            url: 'ticket-list/{filter_json}',
            params: { page: 'ticket-list'}
        },
    ]
});
module.exports = urls;
