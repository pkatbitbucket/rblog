var ko = require('knockout');
var store = require('models/store');
var Mail = require('./mail');
var Thread = require('./thread');

function ThreadStore() {
    var self = this;
    self.prefix = "mail";
    self.local = "threadStore";
    self.threads = ko.observableArray([]);
    self.cached_threads = ko.observableArray([]);
    self.mails = ko.observableArray([]);
    self.cached_mails = ko.observableArray([]);

    self.get_object = function(id){
        for(var i=0; i<store.threadStore.threads().length; i++){
            var obj = store.threadStore.threads()[i];
            if(obj.id == id){
                return obj;
            }
        }
        return null;
    };

    self.push = function(mail) {
        var mailObj = ko.utils.arrayFirst(self.mails(), function(item){
            return item.id == mail.id;
        });
        if(!mailObj){
            mailObj = new Mail(mail);
        } else {
            mailObj.update(mail);
            return;
        }
        self.mails.push(mailObj);

        var threadObj = ko.utils.arrayFirst(self.threads(), function(item) {
            return item.id == mail.thread.id;
        });

        if(!threadObj) {
            threadObj = new Thread(mail.thread.id);
            self.threads.push(threadObj);
        }
        threadObj.add(mailObj);
        var cachedThreadObj = ko.utils.arrayFirst(self.cached_threads(), function(item) {
            return item.id == mail.thread.id;
        });

        if(!cachedThreadObj) {
            cachedThreadObj = new Thread(mail.thread.id);
            self.cached_threads.push(threadObj);
        }
        cachedThreadObj.add(mailObj);
    }

    self.cache_mail = function(mail_data) {
        var mail = ko.utils.arrayFirst(self.mails(), function(item){
            return item.id == mail_data.id;
        });
        if(!mail) {
            var mail = new Mail(mail_data);
            self.cached_mails.push(mail);
            self.mails.push(mail);
        }
        else{
            mail.update(mail_data);
        }
        return mail;
    }

    self.init = function(){
        $.get('/mail/get-initial-mails/?page_no=1', function(data){
            for(var i=0; i<data.mails.length; i++){
                self.push(data.mails[i]);
            }
        });
    };

}
store.add(ThreadStore);

module.exports = ThreadStore;
