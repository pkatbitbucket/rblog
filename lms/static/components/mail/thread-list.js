var ko = require("knockout");
var store = require("models/store");
var ThreadModel = require("./thread");
var Mail = require("./mail");

ko.components.register('thread-detail', {
    viewModel: function(params){
        var self = this;
        self.thread = params.value;
    },
    template: require("raw!./thread-detail.html")
});
module.exports = function() {
    var self = this;
    self.threads = store.threadStore.threads;
    self.filtered_threads = ko.observableArray([]);
    self.is_open = ko.observable(false);
    self.searchQuery = ko.observable();
    self.mails_page = ko.observable(1);
    self.num_pages = ko.observable(2);

    self.open = function(){
        if(self.is_open()){
            self.is_open(false);
        } else {
            self.is_open(true);
        }
    }

    self.enterKeyPress = function(d,e){
        e.keyCode === 13;
        return true;
    }

    self.threadsList = ko.computed(function(){
        var x = self.filtered_threads();
        if(!self.filtered_threads().length){
            x = self.threads();
        }
        return x;
    });

    self.count = ko.computed(function(){
        var unread_count = 0;
        ko.utils.arrayForEach(self.threadsList(), function(item){
            var unread = false;
            ko.utils.arrayForEach(item.sorted_mails(), function(mail){
                if(mail.is_unread()){
                    unread = true;
                }
            });
            if(unread) {
                unread_count += 1;
            }
        });
        return unread_count;
    });

    self.fetchSearchResults = function(d, e){
        if(e.keyCode === 13 && self.searchQuery()){
            $.get('/mail/get-searched-mails?q=' + self.searchQuery(), function(data){
                self.filtered_threads([]);
                for(var i=0; i<data.mails.length; i++){
                    var mail_obj = store.threadStore.cache_mail(data.mails[i]);
                    var thread_obj = store.threadStore.get_object(data.mails[i].id);
                    if(!thread_obj){
                        thread_obj = new ThreadModel(data.mails[i].id);
                    }
                    thread_obj.add(mail_obj);
                    self.filtered_threads.push(thread_obj);
                }
                self.mails_page(data.page_no);
                self.num_pages(data.num_pages);

            });
        }
        return true;
    };

    self.selectedThread = ko.observable();
    self.show_compose_mail = ko.observable(false);

    self.toggle_compose_mail = function(){
        self.selectedThread('');
        self.show_compose_mail(!self.show_compose_mail());
    };

    self.back_to_inbox = function(){
        self.filtered_threads([]);
        self.searchQuery('');
        self.show_compose_mail(false);
    };
    self.change_mails_page = function(param){
        var url;
        if(self.searchQuery()){
            url = '/mail/get-searched-mails?q=' + self.searchQuery() + '&';
        }
        else{
            url = '/mail/get-initial-mails?';
        }
        var abc = url + 'page_no=' + (parseInt(self.mails_page()) + param).toString();
        $.get(url + 'page_no=' + (parseInt(self.mails_page()) + param).toString() , function(data){
            self.filtered_threads([]);
            for(var i=0; i<data.mails.length; i++){
                var mail_obj = store.threadStore.cache_mail(data.mails[i]);
                var thread_obj = store.threadStore.get_object(data.mails[i].id);
                if(!thread_obj){
                    thread_obj = new ThreadModel(data.mails[i].id);
                }
                thread_obj.add(mail_obj);
                self.filtered_threads.push(thread_obj);
            }
            self.mails_page(data.page_no);
            self.num_pages(data.num_pages);
        });

    };

    self.selectThread = function(thread) {
        if(self.selectedThread() && self.selectedThread().id == thread.id){
            self.selectedThread(null);
        }
        else{
            unread_thread = false;
            for(var i=0; i< thread.sorted_mails().length; i++){
                var m = thread.sorted_mails()[i];
                m.get_full_mail();
            }
            self.selectedThread(thread);
        }
        self.show_compose_mail(false);
    }

    self.mark_mail_sent = function(){
        debugger;
    };
};
