/////////// AMEYO INTEGRATION CODE GOES HERE   ////////////////

var crmOrigin;
var ameyoBaseUrl = "http://192.168.2.100:8888/";

function initialize() {
    crmOrigin = window.location.href;
    iframeUrl = ameyoBaseUrl + "ameyowebaccess/toolbar-hz.htm?";
    iframeUrl = iframeUrl + "origin=" + crmOrigin;

    $('#ameyoIframe').attr('src', iframeUrl);
    setTimeout(function(){
        doLogin(USERNAME, USERNAME);
    }, 4000);
}

function logout() {
    doLogout();
}
function getExtensionInfo(){
    var ext_info = {
        name: EXTENSION_NO + "_DefaultVR",
        phone: "1"
    };
    return ext_info;
}

function dialGivenNumber(number){
    mobile_no = number.slice(-10);
    mobile_no = "{{num_prefix}}"+mobile_no;
    doDial(mobile_no);
}

$("#popup_call_form").on('click', ".ameyo-call-btn" , function(){
    var cd_id = $(this).next().text();
    if(!$(this).hasClass("disabled")){
        var mobile_no = $(this).attr("id").split("_")[2];
        dialGivenNumber(mobile_no);
    }
});
$("#popup_call_form").on('click', "#manual-dial-button", function(){
    dialGivenNumber($('#manual-dial-input').val());
});

$('#logout_id').click(function(){
    doLogout();
});

initialize();