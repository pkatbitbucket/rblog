var disposition_url;
var CRTOBJECTID;
var call_type;
var IN_CALL = false;
function customShowCrm(phone, additionalParams) {
    if(IN_CALL) {
        return;
    }
    IN_CALL = true;
    setTimeout(function(){
        IN_CALL = false;
    }, 5000);
    customerPhone = phone;
    var p = JSON.parse(additionalParams);

    call_type = 'outgoing';
    CRTOBJECTID = p.crtObjectId;
    if(p.associationType.indexOf('transfer') > -1){
        call_type = 'transfer';
    }

    // TODO: Hack for voice blaster
    if(p.associationType == 'transfer.to.campaign.association'){
        call_type = 'incoming'
    }

    if((call_type == 'transfer')){
        $('#transfer_call_popup').modal({'show':true, "backdrop": "static"});
        $.get("/get-transfer-call-popup/?mobile=" + phone + "&crtObjectId=" + CRTOBJECTID + "&call_type=" + call_type + "&associationType=" + p.associationType, function(gdata){
            $("#transfer_call_response").html(gdata.message);
            $(".take_to_incoming_call_form").unbind().click(function(){
                tid = $(this).attr('id').split('_').pop();
                $('#transfer_call_popup').modal('hide');
                window.open("/incoming-task-form/?task=" + tid + "&mobile=" + phone + "&crtObjectId=" + CRTOBJECTID + "&html=true");
            });
            $("#close_transfer_call_response_popup").on("click", function(){
                if(confirm("Do you really want to Cancel?")){
                    $('#transfer_call_popup').modal('hide');
                }
            });
        });
    }
    else if((p.associationType.indexOf('inbound') > -1) || !(p.userId == USERNAME) || (call_type == 'incoming')){
        call_type = 'incoming';
        $('#incoming_call_popup').modal({'show':true, "backdrop": "static"});
        $.get("/get-incoming-call-popup/?mobile=" + phone + "&crtObjectId=" + CRTOBJECTID + "&call_type=" + call_type + "&associationType=" + p.associationType, function(gdata){
            console.log(gdata);

            $("#incoming_call_response").html(gdata.message);

            $(".take_to_incoming_call_form").unbind().click(function(){
                tid = $(this).attr('id').split('_').pop();

                $('#incoming_call_popup').modal('hide');
                window.open("/incoming-task-form/?task=" + tid + "&mobile=" + phone + "&crtObjectId=" + CRTOBJECTID + "&html=true");
            });

            $("#close_incoming_call_response_popup").on("click", function(){
                if(confirm("Do you really want to Cancel?")){
                    $('#incoming_call_popup').modal('hide');
                }
            });

        });
    }

    $('.ameyo-call-btn').addClass('disabled');
    disposition_url = "http://192.168.2.100:8888/dacx/dispose?phone=" + phone + "&sessionId=" + p.sessionId + "&campaignId=" + p.campaignId + "&crtObjectId=" + CRTOBJECTID + "&userCrtObjectId=" + p.userCrtObjectId + "&dispositionCode=Bought";

}

function handleTransferToAQ(reason) {
    $('#transfer-call').trigger('click');
    $('#id_later_time').val($('#call_form_response #qualifier_next_call_time').val());
    $('#id_head_disposition').val('QUALIFIED');
    $('#id_head_disposition').trigger('change');
    $('#id_disposition').val('CALL_TRANSFER');
    $("#id_is_customer_scheduled_1").prop("checked", true)
    handleHangup();
    $('#save_call_form').submit();
}

function handleTransferToUser(reason) {
    handleHangup();
}

function logUserActivity(status){
    $.post("/ajax/advisor-ameyo-activity/", {'csrfmiddlewaretoken': CSRF_MIDDLEWARE_TOKEN, 'username': USERNAME, 'status': status});
}

function handleLogin(reason) {
    if(reason == 'success'){
        logUserActivity('Login');
    }
}
function handleLogout(reason) {
    logUserActivity('Logout');
}
function handleOnLoad() {
}
function handleLoginStatus(status) {
}
function handleForceLogin(reason) {
}
function handleSelectExtension(status) {
}
function handleModifyExtension(status) {
}
function handleSelectCampaign(reason) {
}
function handleAutoCallOn(status) {
    logUserActivity('AutoCallOn');
}
function handleAutoCallOff(status) {
    logUserActivity('AutoCallOff');
}
function handleReady(status) {
    logUserActivity(status);
}
function handleBreak(status) {
    logUserActivity(status);
}
function handleHangup(reason) {
    end = new Date().getTime()
}
function handleCallAttempted(reason) {
    start = new Date().getTime()
    end = null
}
function handleTransferToPhone(reason) {
    end = new Date().getTime()
}
function handleTransferInCall(reason) {
    end = new Date().getTime()
}
function handleTransferToIVR(reason) {
    end = new Date().getTime()
}
function handleTransferToCampaign(reason) {
    end = new Date().getTime()
}
function handleConferWithPhone(reason) {
}
function handleConferWithTPV(reason) {
}
function handleConferWithUser(reason) {
}
function handleConferWithLocalIVR(reason) {
}

customIntegration = {};
customIntegration.showCrm = customShowCrm;
customIntegration.loginHandler = handleLogin;
customIntegration.forceLoginHandler = handleForceLogin;
customIntegration.logoutHandler = handleLogout;
customIntegration.onLoadHandler = handleOnLoad;
customIntegration.loginStatusHandler = handleLoginStatus;
customIntegration.selectExtensionHandler = handleSelectExtension;
customIntegration.modifyExtensionHandler = handleModifyExtension;
customIntegration.selectCampaignHandler = handleSelectCampaign;
customIntegration.autoCallOnHandler = handleAutoCallOn;
customIntegration.autoCallOffHandler = handleAutoCallOff;
customIntegration.readyHandler = handleReady;
customIntegration.breakHandler = handleBreak;
customIntegration.hangupHandler = handleHangup;
customIntegration.transferToPhoneHandler = handleTransferToPhone;
customIntegration.transferInCallHandler = handleTransferInCall;
customIntegration.transferToAQHandler = handleTransferToAQ;
customIntegration.transferToIVRHandler = handleTransferToIVR;
customIntegration.transferToUserHandler = handleTransferToUser;
customIntegration.transferToCampaignHandler = handleTransferToCampaign;
customIntegration.conferWithPhoneHandler = handleConferWithPhone;
customIntegration.conferWithTPVHandler = handleConferWithTPV;
customIntegration.conferWithUserHandler = handleConferWithUser;
customIntegration.conferWithLocalIVRHandler = handleConferWithLocalIVR;

registerCustomFunction("showCrm", customIntegration);
registerCustomFunction("loginHandler", customIntegration);
registerCustomFunction("logoutHandler", customIntegration);
registerCustomFunction("onLoadHandler", customIntegration);
registerCustomFunction("loginStatusHandler", customIntegration);
registerCustomFunction("forceLoginHandler", customIntegration);
registerCustomFunction("selectExtensionHandler", customIntegration);
registerCustomFunction("modifyExtensionHandler", customIntegration);
registerCustomFunction("selectCampaignHandler", customIntegration);
registerCustomFunction("autoCallOnHandler", customIntegration);
registerCustomFunction("autoCallOffHandler", customIntegration);
registerCustomFunction("readyHandler", customIntegration);
registerCustomFunction("breakHandler", customIntegration);
registerCustomFunction("hangupHandler", customIntegration);
registerCustomFunction("transferToPhoneHandler", customIntegration);
registerCustomFunction("transferInCallHandler", customIntegration);
registerCustomFunction("transferToAQHandler", customIntegration);
registerCustomFunction("transferToIVRHandler", customIntegration);
registerCustomFunction("transferToUserHandler", customIntegration);
registerCustomFunction("transferToCampaignHandler", customIntegration);
registerCustomFunction("conferWithPhoneHandler", customIntegration);
registerCustomFunction("conferWithTPVHandler", customIntegration);
registerCustomFunction("conferWithUserHandler", customIntegration);
registerCustomFunction("conferWithLocalIVRHandler", customIntegration);
