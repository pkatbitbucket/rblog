// Create IE + others compatible event handler

// attach postMessage event to handler
if (window.attachEvent) {
	window.attachEvent('onmessage', processPostMessage);
} else {
	window.addEventListener('message', processPostMessage, false);
}

// Registration of custom Functions
var customFunctions = new Array();

function registerCustomFunction(key, value) {
	customFunctions[key] = value;
};

function processPostMessage(event) {
	/*
	 * if (event.origin !== "http://localhost:8888/") { return; }
	 */
	var theObject = JSON.parse(event.data);
	if (theObject.method == 'showCrm') {
		var phone = theObject.phone;
		var additionalParams = theObject.additionalParams;
		if (customFunctions['showCrm']) {
			customFunctions['showCrm'].showCrm(phone, additionalParams);
		} else {
			showCrm(phone, additionalParams);
		}
	} else if (theObject.method == 'intializeUI') {
		try {
			var uiElementIds = theObject.uiElementIds;
			var disabledUiIds = configureUI(uiElementIds);
			hideUI(disabledUiIds);
		} catch (e) {
		}
	} else if (theObject.method == 'intializeLoginCredentials') {
		try {
			var loginCredential = getLoginInfo();
			setLoginInfo(loginCredential);
		} catch (e) {
		}
	} else if (theObject.method == 'intializeExtensionInfo') {
		try {
			var extensionInfo = getExtensionInfo();
			setExtensionInfo(extensionInfo);
		} catch (e) {
		}
	} else if (theObject.method == 'logoutHandler') {
		var reason = theObject.reason;
		if (customFunctions['logoutHandler']) {
			customFunctions['logoutHandler'].logoutHandler(reason);
		} else {
			logoutHandler(reason);
		}
	} else if (theObject.method == 'loginHandler') {
		var reason = theObject.reason;
		if (customFunctions['loginHandler']) {
			customFunctions['loginHandler'].loginHandler(reason);
		} else {
			loginHandler(reason);
		}
	} else if (theObject.method == 'onLoadHandler') {
		if (customFunctions['onLoadHandler']) {
			customFunctions['onLoadHandler'].onLoadHandler();
		} else {
			onLoadHandler();
		}
	} else if (theObject.method == 'loginStatusHandler') {
		var reason = theObject.reason;
		if (customFunctions['loginStatusHandler']) {
			customFunctions['loginStatusHandler'].loginStatusHandler(reason);
		} else {
			loginStatusHandler(reason);
		}
	} else if (theObject.method == 'forceLoginHandler') {
		var reason = theObject.reason;
		if (customFunctions['forceLoginHandler']) {
			customFunctions['forceLoginHandler'].forceLoginHandler(reason);
		} else {
			forceLoginHandler(reason);
		}
	}
}

function showCrm(phone, additionalParams) {
}

function loginHandler(reason) {
	// alert("logged In" + reason);
}

function logoutHandler(reason) {
	// alert("logout In" + reason);
}

function loginStatusHandler(status) {

}

function forceLoginHandler(reason) {
	// alert("logged In" + reason);
}

function doLogin(username, password, authPolicy) {
	var theObject = {
		method : 'doLogin',
		username : username,
		password : password,
		authPolicy : authPolicy
	};
	var message = JSON.stringify(theObject);
	doPostMessage(message);
}

function doForceLogin() {
	var theObject = {
		method : 'doForceLogin',
	};
	var message = JSON.stringify(theObject);
	doPostMessage(message);
}

function doLogout() {
	var theObject = {
		method : 'doLogout'
	};
	var message = JSON.stringify(theObject);
	doPostMessage(message);
}
function onLoadHandler() {
	// alert("loaded");
}
function populateNumberInDialBox(phone) {
	var theObject = {
		method : 'populateNumberInDialBox',
		phone : phone,
	};
	var message = JSON.stringify(theObject);
	doPostMessage(message);
}

function doDial(phone, customerId, additionalParams) {
	var theObject = {
		method : 'doDial',
		phone : phone,
		additionalParams : additionalParams
	};
	var message = JSON.stringify(theObject);
	doPostMessage(message);
}

function setLoginInfo(loginCredential) {

	var theObject = {
		method : 'setLoginCredentials',
		userId : loginCredential.userName,
		password : loginCredential.password
	};
	var message = JSON.stringify(theObject);
	doPostMessage(message);

}

function setExtensionInfo(extensionInfo) {

	var theObject = {
		method : 'setExtensionMetadata',
		extensionName : extensionInfo.name,
		extensionPhone : extensionInfo.phone
	};
	var message = JSON.stringify(theObject);
	doPostMessage(message);

}
function hideUI(uiElements) {
	var theObject = {
		method : 'configureUI',
		uiElements : uiElements
	};
	var message = JSON.stringify(theObject);
	doPostMessage(message);
}

function doPostMessage(message) {

	var theIframe = document.getElementById("ameyoIframe");
	var origin = ameyoBaseUrl;
	theIframe.contentWindow.postMessage(message, origin);
}
