$(document).ready(function(){

    $('#reset_password_tab').on('click', '#reset_password_click', function(){
        $('#reset_password_click').hide()
        password_reset_form = $("#password_reset_form")
        serialized_password_reset_form = password_reset_form.serialize()
        $.post("/ajax/password-reset/", serialized_password_reset_form, function(data){
            responseJSON = JSON.parse(data)
            if(responseJSON.success){
                alert('Yo!!\nPassword was successfully reset')
                $('#response').val(responseJSON.message)
            }
            else{ // in case we want to do some magic with failed resets
                alert('Fail!!\nPassword was not reset\nKuch toh fhata h!!!')
                $('#response').val(responseJSON.message)
            }
        });
    });

});