        var setSelector = "div#field_list ul#fields";

        /////////////////////////////////////////////////////////////////
        /////  YOU PROBABLY WON'T NEED TO EDIT BELOW  ///////////////////
        /////////////////////////////////////////////////////////////////

        // function that restores the list order from a cookie
        function setOrder(list_order) {
            var list = $(setSelector);
            if (list == null) return

            var IDs = list_order;

            // fetch current order
            var items = list.sortable("toArray");

            // make array from current order
            var rebuild = new Array();
            for ( var v=0, len=items.length; v<len; v++ ){
                rebuild[items[v]] = items[v];
            }

            for (var i = 0, n = IDs.length; i < n; i++) {

                // item id from saved order
                var itemID = IDs[i];

                if (itemID in rebuild) {

                    // select item id from current order
                    var item = rebuild[itemID];

                    // select the item according to current order
                    var child = $("ul.ui-sortable").children("#" + item);

                    // select the item according to the saved order
                    var savedOrd = $("ul.ui-sortable").children("#" + itemID);

                    // remove all the items
                    child.remove();

                    // add the items in turn according to saved order
                    // we need to filter here since the "ui-sortable"
                    // class is applied to all ul elements and we
                    // only want the very first!  You can modify this
                    // to support multiple lists - not tested!
                    $("ul.ui-sortable").filter(":first").append(savedOrd);
                }
            }
        }

        // code executed when the document loads
        $(function() {
            // here, we allow the user to sort the items
            $(setSelector).sortable({
                axis: "y",
                cursor: "move",
            });

            // here, we reload the saved order
        });
