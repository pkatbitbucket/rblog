from django.conf.urls import include, url, patterns
from django.contrib.auth.views import login, logout
from django.contrib import admin

from django.conf import settings

admin.autodiscover()


urlpatterns = [
    url(r'^honcho/', admin.site.urls),
    url(r'^accounts/login/$', login,
        {'template_name': "login.html"}, name='login'),
    url(r'^accounts/logout/$', logout, {'next_page': '/accounts/login/'}, name='logout'),
    url(r'^nimda/doc/', include('django.contrib.admindocs.urls')),
    url(r'^nimda/', admin.site.urls),
    url(
        r'^grappelli/(?P<path>.*)$', 'django.views.static.serve',
        {
            'document_root': settings.SETTINGS_FILE_FOLDER.joinpath(
                '../static/grappelli/'
            ),
            'show_indexes': False
        }
    ),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^shinchan/', include('cfrecordings.urls')),
    url(r'', include('crm.urls')),
]

if settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    urlpatterns += staticfiles_urlpatterns()
    import debug_toolbar
    urlpatterns += patterns('', url(r'^__debug__/', include(debug_toolbar.urls)), )
