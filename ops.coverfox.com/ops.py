from __future__ import absolute_import
from importd import d, debug, e, env
import os
import sys
import commands
from path import path
import djcelery
import envdir
envdir.open()  # opens and applies varaibles in ./envdir folder

PROD = env("IS_PROD", False)
if PROD:
    CACHE_BACKEND = 'django_elasticache.memcached.ElastiCache'
    CACHE_LOCATION = 'olivaw-memcache.qntizu.cfg.euc1.cache.amazonaws.com:11211'
else:
    CACHE_BACKEND = 'django.core.cache.backends.db.DatabaseCache'
    CACHE_LOCATION = 'cache_table'

SETTINGS_FILE_FOLDER = path(__file__).parent.abspath()
sys.path.extend(env("PYTHONPATH", "").split(":"))

# celery configuration
djcelery.setup_loader()
d(
    # Django Stuff
    DEBUG=not PROD,
    INSTALLED_APPS=[
        "django.contrib.contenttypes",
        "django.contrib.auth",
        "django.contrib.sessions",
        "django.contrib.staticfiles",
        "django.contrib.sites",
        "django.contrib.sitemaps",
        # "django.contrib.flatpages",

        # "django_hstore",
        "django.contrib.admin",

        # debug stuff
        "debug:devserver",
        "debug:debug_toolbar",
        "django_statsd",

        'djcelery',

        "core",
        #  "coverfox",
        "opsapp",
    ],
    ADMINS=(
        ("Amit Upadhyay", "upadhyay@gmail.com"),
        ("Hitul Mistry", "hitul.mistry@coverfoxmail.com"),
        ("Devendra Rane", "dev@coverfoxmail.com"),
    ),
    SITE_ID=1,
    STATIC_URL=debug(
        "/static/", "https://s3-eu-west-1.amazonaws.com/static.humanlly.com/"
    ),
    STATICFILES_DIRS=(os.path.join(SETTINGS_FILE_FOLDER, '../static'),),
    LOGIN_URL="/admin/",
    TEMPLATE_CONTEXT_PROCESSORS=(
        "django.contrib.auth.context_processors.auth",
        "debug:django.core.context_processors.debug",
        "importd.esettings",
        "django.core.context_processors.i18n",
        "django.core.context_processors.media",
        "django.core.context_processors.static",
        "django.core.context_processors.tz",
        "django.contrib.messages.context_processors.messages",
    ),
    MIDDLEWARE_CLASSES=[
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
        'debug:debug_toolbar.middleware.DebugToolbarMiddleware',
        'debug:devserver.middleware.DevServerMiddleware',
        'django_statsd.middleware.GraphiteRequestTimingMiddleware',
        'django_statsd.middleware.GraphiteMiddleware',
    ],
    STATICFILES_FINDERS=(
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
        'django.contrib.staticfiles.finders.FileSystemFinder',
    ),
    USE_TZ=True,
    TIME_ZONE="Asia/Kolkata",
    CACHES={
        'default': {
            'BACKEND': CACHE_BACKEND,
            'LOCATION': CACHE_LOCATION,
            'TIMEOUT': 24 * 60 * 60,
        },
        'compress_cache': {
            'BACKEND': CACHE_BACKEND,
            'LOCATION': CACHE_LOCATION + '10',
        },
        'health-results': {
            'BACKEND': CACHE_BACKEND,
            "LOCATION": CACHE_LOCATION + '1',
        },
        'motor-results': {
            'BACKEND': CACHE_BACKEND,
            "LOCATION": CACHE_LOCATION + '2',
        }
    },
    ALLOWED_HOSTS=['ops.coverfox.com', '127.0.0.1'],
    DATABASES={
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': env('DB_NAME', 'coverfox'),
            'USER': env('DB_USER', 'root'),
            'PASSWORD': env('DB_PASSWORD', ''),
            'HOST': env('DB_HOST', ''),
            'PORT': env('DB_POST', ''),
        }
    },

    TEMPLATE_DIRS=[d.dotslash("../base/templates")],
    AUTH_USER_MODEL='core.User',

    # coverfox stuff
    SETTINGS_FILE_FOLDER=path("."),
    LOG_PATH=SETTINGS_FILE_FOLDER.joinpath('../logs'),

    # ops.coverfox stuff
    TESTING="test" in sys.argv,
    COMMIT_ID=e(commands.getoutput("git rev-parse HEAD").strip()),
    CARBON_UDP_IP=debug("127.0.0.1", prod="grafana.coverfox.com"),
    CARBON_UDP_PORT=2003,
    SEND_STATS_KEYS=["coverfoxops"],
    SURL_REGEXERS={"text_file": "[\w.]+\.txt"},
    DEPLOY_LOG_DIR=debug("/home/coverfox/deploys/", "/tmp/deploys/"),

    BROKER_URL='redis://localhost:6381/1',

    # statsd
    STATSD_HOST='localhost',
    STATSD_PORT=8125,
    STATSD_PREFIX='ops',
    STATSD_MAXUDPSIZE=512,
    STATSD_CLIENT=debug(
        'django_statsd.clients.toolbar', prod='django_statsd.clients.normal'
    ),
    STATSD_MODEL_SIGNALS=True,
    STATSD_CELERY_SIGNALS=True,

    debug=dict(
        # DEVSERVER stuff
        DEVSERVER_TRUNCATE_SQL=False,
        DEVSERVER_AUTO_PROFILE=True,
        DEVSERVER_MODULES=[
            'devserver.modules.sql.SQLRealTimeModule',
            'devserver.modules.sql.SQLSummaryModule',

            'devserver.modules.profile.ProfileSummaryModule',
            'devserver.modules.ajax.AjaxDumpModule',
            'devserver.modules.profile.MemoryUseModule',
            'devserver.modules.cache.CacheSummaryModule',
            'devserver.modules.profile.LineProfilerModule',
        ],

        # django-debug-toolbar stuff
        DEBUG_TOOLBAR_PANELS=(
            'debug_toolbar.panels.version.VersionDebugPanel',
            'debug_toolbar.panels.timer.TimerDebugPanel',
            'debug_toolbar.panels.headers.HeaderDebugPanel',
            'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
            'debug_toolbar.panels.template.TemplateDebugPanel',
            'debug_toolbar.panels.sql.SQLDebugPanel',
            'debug_toolbar.panels.signals.SignalDebugPanel',
            'django_statsd.panel.StatsdPanel',
            # these two panels are there in doc, but dont actually exist in code
            # 'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
            # 'debug_toolbar.panels.logger.LoggingPanel',
        ),
        STATSD_PATCHES=[
            'django_statsd.patches.db',
            # 'django_statsd.patches.cache',
        ],
    )
)
if __name__ == "__main__":
    d.main()
