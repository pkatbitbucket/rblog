# Get excel.
"""
Replace space in files with _
find -name "* *" -type f | rename 's/ /_/g'

Replace space in directories with _
find -name "* *" -type d | rename 's/ /_/g'
"""
import sys
import xlrd
import os
import json
import datetime


sys.path.append("../base/")
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from core.models import FinPolicy
directory = "/home/glitterbug/coverfox/docs/Commission_statements/Commission_Statement_2015-201/"
date = datetime.datetime(1899, 12, 30)


# Pandas utility.
def find_duplicated_in_row(dataframe, column):
    return dataframe[column].duplicated()


def get_columns(dataframe):
    return dataframe.columns.values


# def insert(files, co_dir, ins_comp, mapping, skiprows):
#     for file in files:
#         print("Reading %s" % file)
#         worksheet = pandas.read_excel(os.path.join(co_dir, file), skiprows=skiprows, dtypes=str)
#         # Get policy numbers.
#         policy_column = ""
#         start_date_column = ""
#         end_date_column = ""
#         policy_holder_name_column = ""
#         product_name_column = ""
#         gross_premium_column = ""
#         worksheet_columns = worksheet.columns.values
#         for key, val in mapping.iteritems():
#             if key == "policy_number":
#                 for item in val:
#                     if item in worksheet_columns:
#                         policy_column = item
#
#             if key == "start_date":
#                 for item in val:
#                     if item in worksheet_columns:
#                         start_date_column = item
#
#             if key == "end_date":
#                 for item in val:
#                     if item in worksheet_columns:
#                         end_date_column = item
#
#             if key == "policy_holder_name":
#                 for item in val:
#                     if item in worksheet_columns:
#                         policy_holder_name_column = item
#
#             if key == "product_name":
#                 for item in val:
#                     if item in worksheet_columns:
#                         product_name_column = item
#
#             if key == "gross_premium":
#                 for item in val:
#                     if item in worksheet_columns:
#                         product_name_column = item
#
#         columns = []
#         final_mapping = {}
#         if policy_column:
#             columns.append(policy_column)
#             final_mapping["policy_number"] = len(columns) - 1
#
#         if start_date_column:
#             columns.append(start_date_column)
#             final_mapping["start_date"] = len(columns) - 1
#
#         if policy_holder_name_column:
#             columns.append(policy_holder_name_column)
#             final_mapping["policy_holder_name"] = len(columns) - 1
#
#         if end_date_column:
#             columns.append(end_date_column)
#             final_mapping["end_date"] = len(columns) - 1
#
#         if policy_holder_name_column:
#             columns.append(policy_holder_name_column)
#             final_mapping["policy_holder_name"] = len(columns) - 1
#
#         if product_name_column:
#             columns.append(product_name_column)
#             final_mapping["product_name"] = len(columns) - 1
#
#         if gross_premium_column:
#             columns.append(gross_premium_column)
#             final_mapping["gross_premium"] = len(columns) - 1
#
#         filtered_data =  worksheet[columns]
#         for index, row in filtered_data.iterrows():
#             new_map = final_mapping
#             n_m = {}
#             for key, val in new_map.iteritems():
#                 n_m[key] = row[val]
#
#             if not n_m:
#                 continue
#
#             if pandas.isnull(n_m["policy_number"]):
#                 continue
#
#             if not n_m["policy_number"]:
#                 continue
#
#             if isinstance(n_m["policy_number"], numpy.float64):
#                 n_m["policy_number"] = str(numpy.int64(n_m["policy_number"]))
#
#             if isinstance(n_m["policy_number"], numpy.float32):
#                 n_m["policy_number"] = str(numpy.int64(n_m["policy_number"]))
#
#             if isinstance(n_m["policy_number"], numpy.float16):
#                 n_m["policy_number"] = str(numpy.int64(n_m["policy_number"]))
#
#             n_m["policy_number"] = str(n_m["policy_number"])
#             n_m["policy_number"] = n_m["policy_number"].strip()
#             n_m["insurer_company"] = ins_comp
#             FinPolicy.objects.create(
#                 **n_m
#             )


def insert(files, co_dir, ins_comp, mapping, skiprows, formats=[], convert=None, reformat=None):
    print("STARTING FOR INS COMPANY %s" % ins_comp)
    for file in files:
        print("Reading %s" % file)
        # worksheet = pandas.read_excel(os.path.join(co_dir, file), skiprows=skiprows, dtypes=str)
        xl_workbook = xlrd.open_workbook(os.path.join(co_dir, file))
        sheet_names = xl_workbook.sheet_names()
        worksheet = xl_workbook.sheet_by_name(sheet_names[0])

        row = 0
        if skiprows:
            row = skiprows[0]

        columns = worksheet.row(row)
        worksheet_columns = []
        for col in columns:
            worksheet_columns.append(col.value)

        inserted = 0
        new_mapping = {}
        # Fix rows.
        for field, value in mapping.iteritems():
            for item in value:
                if item in worksheet_columns:
                    new_mapping[field] = item

        for row in range(row + 1, worksheet.nrows):
            i = 0
            col_map = {}
            for col in worksheet_columns:
                row_data = worksheet.row(row)
                col_map[col] = row_data[i].value
                i += 1

            n_m = {}
            for db_field, excel_field in new_mapping.iteritems():
                n_m[db_field] = col_map[excel_field]

            if not n_m:
                continue

            if convert:
                for operation in convert:
                    if operation["field"] in n_m:
                        func = operation["reformat_func"]
                        n_m[operation["field"]] = func(n_m[operation["field"]], col_map)

            if "policy_number" not in n_m:
                print(n_m)
                print(new_mapping)
                print(worksheet_columns)
                raise Exception("Policy number not found")

            if not n_m["policy_number"]:
                continue

            if isinstance(n_m["policy_number"], float):
                print("WARNING GOT %s" % n_m["policy_number"])
                print("After converson %s" % int(n_m["policy_number"]))
                n_m["policy_number"] = int(n_m["policy_number"])

            n_m["extra"] = json.dumps(col_map)

            if "start_date" in n_m:
                if isinstance(n_m["start_date"], float):
                    # Convert into datetime.
                    tm_delta = datetime.timedelta(int(n_m["start_date"]))
                    n_m["start_date"] = tm_delta + date

                elif "start_dt_format" in formats:
                    if n_m["start_date"]:
                        for dt_format in formats["start_dt_format"]:
                            try:
                                n_m["start_date"] = datetime.datetime.strptime(
                                    n_m["start_date"], dt_format
                                )
                                break
                            except:
                                pass

            if "end_date" in n_m:
                if isinstance(n_m["end_date"], float):
                    # Convert into datetime.
                    tm_delta = datetime.timedelta(int(n_m["end_date"]))
                    n_m["end_date"] = tm_delta + date

                elif "end_dt_format" in formats:
                    if n_m["end_date"]:
                        for dt_format in formats["end_dt_format"]:
                            try:
                                n_m["end_date"] = datetime.datetime.strptime(
                                    n_m["end_date"], dt_format
                                )
                                break
                            except:
                                pass

            if "gross_premium" in n_m:
                if isinstance(n_m["gross_premium"], str) or \
                        isinstance(n_m["gross_premium"], unicode):
                    try:
                        print(n_m["gross_premium"])
                        n_m["gross_premium"] = float(n_m["gross_premium"])
                    except:
                        n_m["gross_premium"] = None

            if "net_premium" in n_m:
                if isinstance(n_m["net_premium"], str) or \
                        isinstance(n_m["net_premium"], unicode):
                    try:
                        print(n_m["net_premium"])
                        n_m["net_premium"] = float(n_m["net_premium"])
                    except:
                        n_m["net_premium"] = None

            n_m["policy_number"] = str(n_m["policy_number"])
            n_m["policy_number"] = n_m["policy_number"].strip()
            n_m["insurer_company"] = ins_comp
            n_m["filename"] = file

            # # Reformat datetime.
            # if "start_date" in n_m:
            #     if n_m["start_date"]:
            #         n_m["start_date"] = datetime.datetime.strptime(
            #             n_m["start_date"],
            #             start_dt_format
            #         )
            #     else:
            #         del n_m["start_date"]
            #
            # if "end_date" in n_m:
            #     if n_m["end_date"]:
            #         n_m["end_date"] = datetime.datetime(
            #             n_m["end_date"],
            #             end_dt_format
            #         )
            #     else:
            #         del n_m["end_date"]
            #
            if reformat:
                n_m = reformat(n_m, col_map)

            FinPolicy.objects.create(**n_m)
            inserted += 1

        if not inserted:
            print(worksheet_columns)
            print(co_dir)
            raise Exception(file)


# ----------------------------------------------------------------------------
def ifco_tokio_get_full_name(n_m, row_data):
    insurer_name = ""
    if "GIVNAME" in row_data:
        insurer_name += row_data["GIVNAME"]

    if "SURNAME" in row_data:
        insurer_name += " " + row_data["SURNAME"]

    n_m["policy_holder_name"] = insurer_name

    if "start_date" in n_m:
        if "GN" in n_m["start_date"]:
            del n_m["start_date"]

    return n_m


# -- Ifko tokio
mapping = {
    "policy_number": ["POLICYNO", "POLNUM"],
    "start_date": ["DATE"],
    "end_date": [],
    "policy_holder_name": ["SURNAME"],
    "product_name": ["OUR_PREM", "TRNAMT_LCE01"],
    "gross_premium": [],
    "net_premium": ["TRNAMT_LCE01"]
}
formats = {
    "start_dt_format": ["%Y-%m-%d"]
}
ins_dir = os.path.join(directory, "IFFKO_TOKIO")
files = os.listdir(ins_dir)
insert(files, ins_dir, "IFFKO_TOKIO", mapping, [], reformat=ifco_tokio_get_full_name)


# ----------------------------------------------------------------------------
# -- Universal
mapping = {
    "policy_number": ["POLICY Number"],
    "start_date": [],
    "end_date": [],
    "policy_holder_name": ["INSURED NAME"],
    "product_name": [],
    "gross_premium": ["PREMIUIM"],
}
ins_dir = os.path.join(directory, "Universal")
files = os.listdir(ins_dir)
insert(files, ins_dir, "Universal", mapping, [1])


# ----------------------------------------------------------------------------
# -- Raheja
mapping = {
    "policy_number": ["Policy Number"],
    "start_date": ["Contract Commencement date"],
    "end_date": ["EXPIRY_DATE"],
    "policy_holder_name": ["Client Name"],
    "product_name": [],
    "gross_premium": ["Premium"]
}
dt_format = "%d/%m/%Y"
formats = {
    "start_dt_format": [dt_format],
    "end_dt_format": [dt_format]
}
ins_dir = os.path.join(directory, "Raheja_QBE")
files = os.listdir(ins_dir)
insert(files, ins_dir, "Raheja_QBE", mapping, [1], formats=formats)


# ----------------------------------------------------------------------------
# --Max Bupa
mapping = {
    "policy_number": ["Policy Number"],
    "start_date": [],
    "end_date": [],
    "policy_holder_name": ["Customer Name"],
    "product_name": [],
    "gross_premium": [],
    "net_premium": []
}
ins_dir = os.path.join(directory, "Max_Bupa")
files = os.listdir(ins_dir)
insert(files, ins_dir, "Max_Bupa", mapping, [])


# ----------------------------------------------------------------------------
# -- Liberty
mapping = {
    "policy_number": ["PolicyNo/Endorsement No."],
    "start_date": [],
    "end_date": [],
    "policy_holder_name": ["CLIENT NAME"],
    "product_name": ["Product Description"],
    "gross_premium": [],
    "net_premium": ["OD Premium"]
}
ins_dir = os.path.join(directory, "Liberty")
files = os.listdir(ins_dir)
insert(files, ins_dir, "Liberty", mapping, [1])


# ----------------------------------------------------------------------------
# -- HDFC_Ergo
# TODO: #premium
mapping = {
    "policy_number": ["POLICY_NO", "POLICYNUMBER", "CUSTOMER NAME"],
    "start_date": [],
    "end_date": [],
    "policy_holder_name": ["CUSTOMER NAME"],
    "product_name": [],
    "gross_premium": []
}
ins_dir = os.path.join(directory, "HDFC_Ergo")
files = os.listdir(ins_dir)
insert(files, ins_dir, "HDFC_Ergo", mapping, [1])


# ----------------------------------------------------------------------------
# Offline Bajaj
# Todo: calculate gross #premium with service tax.
mapping = {
    "policy_number": ["Policy Number", "POLICY_REFERENCE", "POLICY Number"],
    "start_date": ["Date"],
    "end_date": [],
    "policy_holder_name": ["Partner Desc", "CUSTOMER NAME", "Customer Name"],
    "product_name": ["Product Desc", "Product Name"],
    "gross_premium": [],
    "net_premium": ["Net Premium"]
}
dt_format = "%d %b %y"
formats = {
    "start_dt_format": [dt_format]
}
bajaj_dir = os.path.join(directory, "Bajaj_Allianz", "Offline")
files = os.listdir(bajaj_dir)
insert(files, bajaj_dir, "Bajaj_Allianz", mapping, [1], formats=formats)

# -- Bajaj online and offline.
mapping = {
    "policy_number": ["Policy Number", "POLICY_REFERENCE", "POLICY Number", "POLICY_REF"],
    "start_date": [],
    "end_date": [],
    "policy_holder_name": ["Partner Desc", "CUSTOMER NAME"],
    "product_name": ["Product Desc"],
    "gross_premium": ["Gross Premium"],
    "net_premium": ["NET PREMIUM", "Net Premium", "NET_PREMIUM", "PREMIUM FOR COMMISSION"]
}
dt_format = "%d/%m/%Y"
formats = {
    "start_dt_format": [dt_format],
    "end_dt_format": [dt_format]
}
bajaj_dir = os.path.join(directory, "Bajaj_Allianz", "Websales")
files = os.listdir(bajaj_dir)
insert(files, bajaj_dir, "Bajaj_Allianz", mapping, [1], formats)


# ----------------------------------------------------------------------------
# -- Future Generali
mapping = {
    "policy_number": ["POLICY_NO"],
    "start_date": [],
    "end_date": [],
    "policy_holder_name": ["Policy Name"],
    "product_name": ["Net Premium"],
    "gross_premium": ["TOTAL_PREM"]
}
ins_dir = os.path.join(directory, "Furure_Generali")
files = os.listdir(ins_dir)
insert(files, ins_dir, "Furure_Generali", mapping, [])


# ----------------------------------------------------------------------------
# TODO: Parse first_name & Last name., Add new #premium.
# -- Cigna_TTK
mapping = {
    "policy_number": ["Policy Number", "E4941-POLICY-NUMBER"],
    "start_date": ["ISSUE-DATE", "E4941-ISSUE-DATE"],
    "end_date": [],
    "policy_holder_name": ["CLNAME"],
    "product_name": ["POLICY-TYPE"],
    "net_premium": ["Net Premium", "E4941-NET-PREM"],
    "gross_premium": []
}
dt_format = "%d-%m-%Y"
formats = {
    "start_dt_format": [dt_format]
}


def cigna_tk_reformat(n_m, row_data):
    insurer_name = ""
    if "E4952-FIRST-NAME" in row_data:
        insurer_name += row_data["E4952-FIRST-NAME"]

    if "E4952-LAST-NAME" in row_data:
        insurer_name += " " + row_data["E4952-LAST-NAME"]

    n_m["policy_holder_name"] = insurer_name
    return n_m

ins_dir = os.path.join(directory, "Cigna_TTK")
files = os.listdir(ins_dir)
insert(files, ins_dir, "Cigna_TTK", mapping, [1], formats=formats, reformat=cigna_tk_reformat)


# ----------------------------------------------------------------------------
def bharti_exa_reformat_date(date, row_data):
    date = str(date)
    year = date[0:4]
    month = date[4:6]
    day = date[6:8]

    return "%s-%s-%s" % (year, month, day)


def bharti_axa_add_insurer_mame(n_m, row_data):
    insurer_name = ""
    if "CLIENT_NAME" in row_data:
        insurer_name += row_data["CLIENT_NAME"]

    if "Client Name" in row_data:
        insurer_name += row_data["Client Name"]

    if "CLIENT_SURNAME" in row_data:
        insurer_name += " " + row_data["CLIENT_SURNAME"]

    if "Client Surname" in row_data:
        insurer_name += " " + row_data["Client Surname"]

    # Add 1 week to end date.
    n_m["end_date"] += n_m["end_date"] + datetime.timedelta(days=7)
    n_m["policy_holder_name"] = insurer_name
    return n_m

mapping = {
    "policy_number": ["Policy No", "POLICY_NO"],
    "start_date": [],
    "end_date": [],
    "policy_holder_name": ["CLNAME", "CLIENT_NAME"],
    "product_name": [],
    "gross_premium": ["BagiPremium"]
}
ins_dir = os.path.join(directory, "Bhart_Axa", "statement1")
files = os.listdir(ins_dir)
insert(files, ins_dir, "Bhart_Axa", mapping, [1])


# -- Bharti AXA
mapping = {
    "policy_number": ["Policy No", "POLICY_NO"],
    "start_date": ["POLICY_FROM"],
    "end_date": ["POLICY_RENEWAL_DATE"],
    "policy_holder_name": ["CLNAME", "CLIENT_NAME"],
    "product_name": [],
    "gross_premium": ["Gross_Commission"]
}
dt_format = "%Y%-m%-d"
formats = {
    "start_dt_format": [dt_format],
    "end_dt_format": [dt_format]
}
ins_dir = os.path.join(directory, "Bhart_Axa", "statement2")
files = os.listdir(ins_dir)
convert_field_before_convert = [
    {"field": "start_date",
     "reformat_func": bharti_exa_reformat_date},
    {"field": "end_date",
     "reformat_func": bharti_exa_reformat_date}
]
insert(files, ins_dir, "Bhart_Axa", mapping, [1], convert=convert_field_before_convert)


# ----------------------------------------------------------------------------
# -- Religare
# No way to predict premium.
mapping = {
    "policy_number": ["Policy #"],
    "start_date": ["Commission Due Date"],
    "end_date": [],
    "policy_holder_name": ["Policy Name"],
    "product_name": ["Prod Name"],
    "gross_premium": ["Policy Premium"]
}
dt_format = "%m/%d/%Y"
formats = {
    "start_dt_format": [dt_format],
    "end_dt_format": [dt_format]
}
ins_dir = os.path.join(directory, "Religare")
files = os.listdir(ins_dir)
insert(files, ins_dir, "Religare", mapping, [1], formats)
# ----------------------------------------------------------------------------

# -- Star health
mapping = {
    "policy_number": ["POLH_NO", "Policy Number / Endorsement Number"],
    "start_date": [],
    "end_date": [],
    "policy_holder_name": ["POLH_ASSR_NAME", "Policy Holder Name"],
    "product_name": ["Prod Name", "Product Name"],
    "gross_premium": ["POLH_PREM_LC_1"]
}
ins_dir = os.path.join(directory, "Star_health")
files = os.listdir(ins_dir)
insert(files, ins_dir, "Star_health", mapping, [])


# ----------------------------------------------------------------------------
# -- L&T
# TODO: Need to confirm  Premium Amount field. #premium
# TODO: Need to add date time manually.
mapping = {
    "policy_number": ["Policy no"],
    "start_date": [],
    "end_date": [],
    "policy_holder_name": ["Policy Holder Name"],
    "product_name": ["Product"],
    "gross_premium": ["Premium Amount"]
}
ins_dir = os.path.join(directory, "L_&_T")
files = os.listdir(ins_dir)
insert(files, ins_dir, "L_&_T", mapping, [])


# ----------------------------------------------------------------------------
# -- Oriantal
mapping = {
    "policy_number": ["POLICY Number"],
    "start_date": [],
    "end_date": [],
    "policy_holder_name": ["INSURED NAME"],
    "product_name": [],
    "gross_premium": ["PREMIUIM"]
}
ins_dir = os.path.join(directory, "Oriental")
files = os.listdir(ins_dir)
insert(files, ins_dir, "Oriental", mapping, [1])


# ----------------------------------------------------------------------------
# Insert Apollo.
# TODO: Need to reformat excel.
# --Apollo
mapping = {
    "policy_number": ["Policy Code"],
    "start_date": [],
    "end_date": [],
    "policy_holder_name": ["Policy Holder Name"],
    "product_name": ["Product Name"],
    "gross_premium": ["Gross Premium"]
}

apollo_dir = os.path.join(directory, "Apollo")
files = os.listdir(apollo_dir)
insert(files, apollo_dir, "Apollo", mapping, [])


# ----------------------------------------------------------------------------
# -- Reliance online
mapping = {
    "policy_number": ["Policy Number", "Policy #"],
    "start_date": ["Policy Start Date"],
    "end_date": ["Policy end date"],
    "policy_holder_name": ["Name of Policy Holder", "Policy Name"],
    "product_name": ["Prod Name"],
    "gross_premium": [],
    "net_premium": ["Net Premium"],
    "mobile": ["Mobile No"],
    "registration_no": ["Register No"]
}
dt_format = "%d-%m-%Y"
formats = {
    "start_dt_format": [dt_format],
    "end_dt_format": [dt_format]
}

ins_dir = os.path.join(directory, "Reliance", "Online")
files = os.listdir(ins_dir)
insert(files, ins_dir, "Reliance", mapping, [], formats)


# --- Reliance Offline
mapping = {
    "policy_number": ["Policy Number"],
    "start_date": [],
    "end_date": [],
    "policy_holder_name": ["Insured Name", "Name Of Insured"],
    "product_name": ["Prod Name"],
    "gross_premium": ["Premium Amount", "Premium for payout"]
}
ins_dir = os.path.join(directory, "Reliance", "Offline")
files = os.listdir(ins_dir)
insert(files, ins_dir, "Reliance", mapping, [])
# ----------------------------------------------------------------------------


# -- Tata AIG
mapping = {
    "policy_number": ["Policy No", "Policy Number"],
    "start_date": ["Policy Effective Date"],
    "end_date": [],
    "policy_holder_name": ["Insured name"],
    "product_name": ["Prod Name"],
    "gross_premium": ["Gross Premiuim", "Gross Premium Amount"]
}
dt_format = "%d/%m/%Y"
formats = {
    "start_dt_format": [dt_format],
    "end_dt_format": [dt_format]
}
ins_dir = os.path.join(directory, "Tata_AIG")
files = os.listdir(ins_dir)
insert(files, ins_dir, "Tata_AIG", mapping, [], formats)


# ----------------------------------------------------------------------------
# -- New india
mapping = {
    "policy_number": ["Policy Number"],
    "start_date": ["Effective Policy date"],
    "end_date": ["Policy Expiry date"],
    "policy_holder_name": ["Insured Name"],
    "product_name": [],
    "gross_premium": ["Premiuim Amount"]
}
dt_format = "%d/%m/%Y"
formats = {
    "start_dt_format": [dt_format],
    "end_dt_format": [dt_format]
}
ins_dir = os.path.join(directory, "New_India")
files = os.listdir(ins_dir)
insert(files, ins_dir, "New_India", mapping, [], formats)
