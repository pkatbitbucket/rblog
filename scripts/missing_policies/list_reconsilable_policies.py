import sys
import os
import ast
import csv

import pandas

sys.path.append("../../base/")
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from core.models import FinPolicy, Policy

csv_file = "missing_policy_data.csv"
pol_missing_csv = "finance_sheet_missing.csv"
pol_missing_file = open(pol_missing_csv, "wb")
missing_policy_writer = csv.writer(
    pol_missing_file,
    delimiter=',',
    quotechar='|', quoting=csv.QUOTE_MINIMAL
)
missing_policy_writer.writerow(["policy_no", "start_date"])
pol_missing_csv_new_report = open("finance_sheet_missing_new.csv", "wb")
pol_missing_csv_new_report_writer = csv.writer(
    pol_missing_csv_new_report,
    delimiter=',',
    quotechar='|', quoting=csv.QUOTE_MINIMAL
)
pol_missing_csv_new_report_writer.writerow(
    ["policy_id", "product", "transaction_id", "missing_fields", "found_fields"]
)
csv = pandas.read_csv(csv_file)


for index, item in csv.iterrows():
    policy_id = item["policy_id"]
    product = item["product"]
    transaction_id = item["transaction_id"]
    missing_fields = ast.literal_eval(item["missing_fields"])

    if "policy_number" not in missing_fields:
        policy_instance = Policy.objects.filter(id=policy_id).first()
        if not policy_instance:
            pol_missing_csv_new_report_writer.writerow(
                list(item)
            )
            continue

        # Get policy number.
        fin_policy_instance = FinPolicy.objects.filter(
            policy_number=policy_instance.policy_number
        ).first()

        if not fin_policy_instance:
            missing_policy_writer.writerow(
                [
                    policy_instance.policy_number,
                    policy_instance.issue_date
                ]
            )

            pol_missing_csv_new_report_writer.writerow(
                list(item)
            )
            continue

        transaction = None
        found = []
        if product == "health":
            transaction = policy_instance.health_transaction

        if product == "motor":
            if "vehicle_registration_number" in missing_fields:
                if fin_policy_instance.registration_no:
                    found += "vehicle_registration_number"

            transaction = policy_instance.motor_transactions

        if product == "travel":
            transaction = policy_instance.travel_transactions

        if "premium" in missing_fields:
            if fin_policy_instance.gross_premium or fin_policy_instance.net_premium:
                found += "premium"

        item = list(item)
        if found:
            item.append(found)

        pol_missing_csv_new_report_writer.writerow(
            item
        )

pol_missing_file.close()
pol_missing_csv_new_report.close()
