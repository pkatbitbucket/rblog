import sys
import os
import csv

sys.path.append("../../base/")
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
django.setup()

from django.db.models import Q

from core.models import FinPolicy
from motor_product.models import Transaction as motor_product
from health_product.models import Transaction as health_product
from travel_product.models import Transaction as travel_product
from travel_product.exceptions import InvalidFormData

transaction_csv = open("transaction_recons.csv", "w")
transaction_csv_writer = csv.writer(
    transaction_csv,
    delimiter=',',
)
transaction_csv_writer.writerow(
    [
        "transaction_id",
        "found",
        "product"
        "status_of_transaction"
    ]
)


def create_report(transactions, product):
    for transaction in transactions:
        fin_policy = FinPolicy.objects.filter(
            policy_number=transaction.policy_number
        ).first()

        found = []
        if fin_policy:
            # Check fields.
            try:
                if not transaction.policy_start_date:
                    if fin_policy.start_date:
                        found.append("policy_start_date")
            except InvalidFormData:
                pass

            try:
                if transaction.policy_end_date:
                    if fin_policy.end_date:
                        found.append("policy_end_date")
            except InvalidFormData:
                pass

            if hasattr(transaction, "vehicle_registration_number"):
                if transaction.vehicle_registration_number():
                    if fin_policy.registration_no:
                        found.append("vehicle_registration_number")

            if transaction.premium_paid:
                if fin_policy.gross_premium or fin_policy.net_premium:
                    found.append("premium_paid")

            str_found = ""
            for item in found:
                str_found += item
                str_found += ";"

            str_found = str_found[0:-1]

            print([
                transaction.transaction_id,
                str_found,
                transaction.status
            ])
            transaction_csv_writer.writerow(
                [
                    transaction.transaction_id,
                    str_found,
                    product,
                    transaction.status
                ]
            )

# Search for transactions in motor_product.
filtered_motor_transaction = motor_product.objects.filter(
    Q(
        policy_start_date__isnull=True,
        policy_end_date__isnull=True,
        premium_paid=''
    ),
    ~Q(policy_number='')
)

create_report(filtered_motor_transaction, "MOTOR")
print("MOTOR")


filtered_health_transaction = health_product.objects.filter(
    Q(
        policy_start_date__isnull=True,
        policy_end_date__isnull=True,
        premium_paid=''
    ),
    ~Q(policy_number='')
)

create_report(filtered_health_transaction, "HEALTH")
print("HEALTH")

filtered_travel_transaction = travel_product.objects.filter(
    ~Q(policy_number='')
)

create_report(filtered_travel_transaction, "TRAVEL")
print("TRAVEL")
