from dateutil import tz

SIGKILL = False
SIGUSR1 = False
SIGUSR2 = False

INTERNAL_IPS = [
    '127.0.0.1',
    '182.72.40.134',
    '182.74.245.218',
    '58.68.3.58',
    '124.124.12.137',
]

TABLES = {
    'nginx': {
        'debug': 'debug_nginx',
        'prod': 'nginx'
    },
    'activity': {
        'debug': 'debug_activity',
        'prod': 'activity'
    }
}
timezone = tz.gettz('Asia/Kolkata')

ERROR_LOG_PATH = '/logs/activity-error-logs/'
