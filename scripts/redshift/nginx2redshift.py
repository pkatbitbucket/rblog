from putils import File2X, RedshiftWriter


class NginxParser(object):
    def parse_line(self, line):
        pass


def main():
    f2rs = File2X(
        spool_file="/var/spool/nginx2redshift.spool",
        filename="/var/log/nginx/access.log",
        parser=NginxParser(),
        timeout=5,
        writer=RedshiftWriter(table="nginx", batch_size=1000),
    )
    try:
        f2rs.run()
    except KeyboardInterrupt:
        print "Done."


if __name__ == '__main__':
    main()
