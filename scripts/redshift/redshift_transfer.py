import re
import sys
import signal
import traceback
import json
import time
import os
import copy
import argparse
import logging
import collections

from datetime import datetime
from dateutil import tz

import setproctitle

import global_var
import utils

logging.basicConfig(filename='redshift.log', level=logging.ERROR)
logger = logging.getLogger(__name__)
parser = argparse.ArgumentParser(
    description='Redshift transfer functions'
)
# ---------------------------------------------------------------------
# Compiled regexes
DATETIME_MATCH_REGEX = r'[0-9]{2}\/.*\/[0-9]{4}\:[0-9]{2}\:[0-9]{2}\:[0-9]{2}\s\+[\d]+'
datetime_parser = re.compile(DATETIME_MATCH_REGEX)

GROUP_BY_QUOTE_REGEX = r'\".*\"'
group_by_quote = re.compile(GROUP_BY_QUOTE_REGEX)

# ---------------------------------------------------------------------

# Add arguments
parser.add_argument(
    '--func', type=str, help='Function name', required=True)

parser.add_argument(
    '--log_file', type=str, help='Function name', required=False)

parser.add_argument(
    '--dir_path', type=str, help='Function name', required=False)

parser.add_argument(
    '--kinesis_stream', type=str, help='Function name', required=False
)

parser.add_argument(
    '--kinesis_shard', type=str, help='Function name', required=False
)

parser.add_argument(
    '--filter_files', type=str, help='Log directory filter files. Example *.log, *.gz etc',
    required=False
)

parser.add_argument(
    '--debug', action='store_true', default=False
)

args = parser.parse_args()
q_row = []
tail_queue = utils.Queue()


def create_param(params):
    fields = {
        "adposition": "adposition",
        "device": "device_type",
        "device_model": "device_model",
        "network": "network",
        "category": "network_category",
        "utm_campaign": "utm_campaign",
        "utm_medium": "utm_medium",
        "utm_source": "utm_source",
        "utm_term": "utm_term",
        "keyword": "keyword",
        "gclid": "gclid",
        "creative": "creative",
        "source": "source",
        "utm_content": "utm_content"
    }

    new_params = copy.copy(utils.get_field_params())

    for key, val in params.iteritems():
        if key in fields:
            new_params[fields[key]] = val[0]

    return new_params


def parser_default_val():
    return '-'


def parser(log, format="str"):
    nginx_regex = re.compile(
        r'(?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) \- (?P<remoteuser>[\-a-zA-Z0-9\_\\\&\+\!]+)'
        r' \[(?P<request_time>.*)\] \"(?P<httpmethod>GET|POST|HEAD|PUT|DELETE|OPTIONS|CONNECT)?(\s)'
        r'?(?P<url>[^\s]+)(\")?(\s)?(HTTP\/[\d]+\.[\d]+)?([\s])?([\d]{3})?(\")? '
        r'(?P<httpstatus>[\d]{3}) (?P<bytessent>[\d]+) \"(?P<referer>[^\"]+)?\"'
        r' \"(?P<useragent>[^\"]+)?(\")?',
        re.IGNORECASE
    )
    nginx_server_info_regex = re.compile(
        r'([A-Za-z]{3}\s(\s)?[0-9]{1,2}\s[0-9]{1,2}\:[0-9]{1,2}'
        r'\:[0-9]{1,2}\s)(?P<server_name>[a-zA-Z0-9\-\_]+)'
        r'\s(?P<server_tag>[a-zA-Z0-9\-\_]+)?'
    )
    data = {}
    data.update(nginx_regex.search(log).groupdict())

    server_info_search_result = nginx_server_info_regex.search(log)
    if server_info_search_result:
        server_info = nginx_server_info_regex.search(log).groupdict()
    else:
        server_info = {}

    data.update(server_info)
    if 'sever_name' not in data:
        data['server_name'] = ''

    if 'server_tag' not in data:
        data['server_tag'] = ''

    dt_format = "%d/%b/%Y:%H:%M:%S +0000"
    dt = datetime.strptime(data['request_time'], dt_format)
    dt = dt.replace(tzinfo=tz.gettz('UTC')).astimezone(global_var.timezone)
    data['request_time'] = dt.strftime("%Y-%m-%d %H:%M:%S")
    if 'useragent' not in data:
        data['useragent'] = ''

    data["created_at"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    ip = data['ip']
    data['is_internal'] = 't' if ip in global_var.INTERNAL_IPS else 'f'

    # row = ""
    # if format =a= "str":
    #     row = get_row_format().format(
    #         **data
    #     )
    # elif format == "tuple":
    #     row = data.values()
    # elif format == "json":
    #     return data

    default_dict = collections.defaultdict(parser_default_val)
    default_dict.update(data)
    return default_dict


def read_file(log_file=None, file_format="txt", spool_manager=None, debug=False):
    """
     create table nginx(
        id bigint identity(0, 1),
        server_name varchar(100) not null,
        server_tag varchar(100),
        ip varchar(20),
        http_method varchar(10),
        http_status integer,
        remote_user varchar(1000),
        request_time timestamp not null,
        url varchar(3500),
        referer varchar(3501),
        user_agent varchar(3000),
        bytes_sent integer,
        created_at timestamp,
        is_internal bool
     );
    """
    table = 'nginx'
    table_name = utils.get_table_name(table, debug=debug)
    query = """
        INSERT INTO %s (server_name, server_tag, ip, http_method, http_status,
        remote_user, request_time, url, referer, user_agent, bytes_sent, created_at,
        is_internal, raw_log, parse_error)
    """ % table_name
    with utils.FileReader(log_file, "%s.spool" % log_file, file_format) as file_reader:
        writer = utils.RedshitWriter(
            query,
            spool_manager=file_reader.spool_manager,
            buffer_size=1000,
            timeout=1000,
        )

        for log, spool_data in file_reader.get_next_line():
            try:
                if log is None:
                    break

                row = parser(log, format="json")
            except:
                logger.error(
                    "Exception occurred while parsing log (%s)" % log,
                    exc_info=True
                )
                writer.write(
                    (
                        '',
                        '',
                        '',
                        '',
                        '0',
                        '',
                        datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        '',
                        '',
                        '',
                        '0',
                        datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        'f',
                        log,
                        't'
                    ),
                    spool_data,
                    err=True
                )
            else:
                writer.write(
                    (
                        row['server_name'],
                        row['server_tag'],
                        row['ip'],
                        row['httpmethod'],
                        row['httpstatus'],
                        row['remoteuser'],
                        row['request_time'],
                        row['url'],
                        row['referer'],
                        row['useragent'],
                        row['bytessent'],
                        row['created_at'],
                        row['is_internal'],
                        log,
                        'f'
                    ),
                    spool_data
                )

    return True


def parse_nginx_dir(directory, spool_manager=None, filter_pattern=None):
    if not os.path.isdir(directory):
        raise Exception("Invalid directory %s path" % directory)

    filename, pointer = spool_manager.get_file_point_file()
    for log_file in utils.get_next_file(directory, filter_pattern=filter_pattern):
        if filename and filename == log_file and pointer == 100:
            continue

        spool_manager.write_to_spool(0, log_file)

        logger.info("Loading %s" % log_file)
        # print("Loading %s" % log_file)
        if log_file.endswith(".gz"):
            # Get log file.
            writen = read_file(
                log_file=os.path.join(directory, log_file),
                spool_manager=spool_manager,
                file_format="gzip"
            )
        else:
            writen = read_file(
                log_file=os.path.join(directory, log_file),
                spool_manager=spool_manager,
                file_format="txt"
            )

        if writen:
            spool_manager.write_to_spool(100, log_file)


def tail_log(log_file_path, debug=False):
    table = 'nginx'
    table_name = utils.get_table_name(table, debug=debug)
    query = """
        INSERT INTO %s (server_name, server_tag, ip, http_method, http_status,
        remote_user, request_time, url, referer, user_agent, bytes_sent, created_at,
        is_internal, raw_log, parse_error)
    """ % table_name

    spool_file = "tail_log.spool"
    spool_manager = utils.SpoolManager(spool_file)
    writer = utils.RedshitWriter(query, spool_manager, buffer_size=1000, timeout=120)
    with utils.FileTail(log_file_path, spool_file) as file_tail:
        for log, spool_data in file_tail.get_next_line():
            if not log:
                time.sleep(1)
                continue

            try:
                if log:
                    row = parser(log, format="json")
            except:
                logger.exception(
                    "Exception occurred while parsing log (%s)" % log
                )
                writer.write(
                    (
                        '',
                        '',
                        '',
                        '',
                        '0',
                        '',
                        datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        '',
                        '',
                        '',
                        '0',
                        datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        'f',
                        log,
                        't'
                    ),
                    spool_data
                )
                continue
            else:
                writer.write(
                    (
                        row['server_name'],
                        row['server_tag'],
                        row['ip'],
                        row['httpmethod'],
                        row['httpstatus'],
                        row['remoteuser'],
                        row['request_time'],
                        row['url'],
                        row['referer'],
                        row['useragent'],
                        row['bytessent'],
                        row['created_at'],
                        row['is_internal'],
                        log,
                        'f'
                    ),
                    spool_data
                )


def insert_user_data():
    user_data = utils.get_user_full_data()
    query = "INSERT INTO users VALUES "
    for user in user_data:
        # Create query
        query += "('%s', '%s', '%s),'" % \
            (
                user["id"],
                user["email"],
                user["username"]
            )
        query += ","

        if query[-1] == ",":
            query = query[0:-1]

        query += ";"


def sync_activity_log_tail(log_file):
    # Do kinesis connection.
    kinesis_instance = utils.Kinesis("", stream="activity")
    with utils.FileTail(log_file, "activity_log.spool", spool_write=True) as follower:
        for line, _ in follower.get_next_line():
            if not line or line == "\n":
                continue

            # TODO: Need to write converter to modify logs to proper format.
            # print(line.replace("\n", ""))
            kinesis_instance.put_records(line)


def sync_activity_log(log_file):
    # Do kinesis connection.
    kinesis_instance = utils.Kinesis("", stream="activity")
    with utils.FileTail(log_file, "activity_log_file_sync.spool", spool_write=True) as follower:
        for line, _ in follower.get_next_line():
            if not line or line == "\n":
                break

            # TODO: Need to write converter to modify logs to proper format.
            # print(line.replace("\n", ""))
            kinesis_instance.put_records(line)

        return True


def parse_kinesis_dir(directory, spool_manager=None, filter_pattern=None):
    if not os.path.isdir(directory):
        raise Exception("Invalid directory %s path" % directory)

    if not spool_manager:
        spool_manager = utils.SpoolManager('activity_dir.spool')

    filename, pointer = spool_manager.get_file_point_file()
    next_file = False

    # Check if spool file found in directory.
    if filename and not os.path.isfile(os.path.join(directory, filename)):
        filename = None

    for log_file in utils.get_next_file(directory, filter_pattern=filter_pattern):
        is_spool_has_file = filename and filename == log_file and pointer != 100
        if not filename or next_file or is_spool_has_file:
            if not filename or next_file:
                spool_manager.write_to_spool(0, log_file)

            logger.info("Loading %s" % log_file)
            print("Loading %s" % log_file)
            writen = sync_activity_log(
                os.path.join(directory, log_file)
            )

            if writen:
                spool_manager.write_to_spool(100, log_file)

            next_file = True

        elif filename and filename == log_file and pointer == 100:
            next_file = True
            continue


def consume_kinesis_stream(stream, shard=None, starting_point=None, shard_spool=None, debug=False):
    kinesis_instance = utils.Kinesis(
        "Consumer1", stream, shard=shard,
        starting_point=starting_point, shard_spool=shard_spool
    )
    activity_actions = utils.ActivityActions(spool_manager=None, kenesis_manager=kinesis_instance)
    for record in kinesis_instance.get_next_record():
        for item in record:
            error = False
            try:
                activity = utils.parse_kinesis_stream(item["Data"])
            except:
                with utils.ErrorLogManager('activity-error-logs') as em:
                    em.write_to_file(item["Data"])
            else:
                activity_actions._insert_activity(
                    activity,
                    item["SequenceNumber"],
                    error=error
                )

        time.sleep(1)


def print_stream_shards(stream):
    kinesis_instance = utils.Kinesis()
    print(json.dumps(kinesis_instance.list_shards(stream)))


def run():
    if args.func == "read_file":
        if not args.log_file:
            raise Exception("Log file is required.")

        read_file(args.log_file, debug=args.debug)

        return

    if args.func == "parse_nginx_dir":
        if not args.dir_path:
            raise Exception("Nginx directory path required")

        filter_pattern = None
        if args.filter_files:
            filter_pattern = args.filter_files

        with utils.SpoolManager("nginx_dir.spool") as spool_manager:
            parse_nginx_dir(args.nginx_dir, filter_pattern=filter_pattern, spool_manager=spool_manager)

        return

    if args.func == "parse_activity_log_dir":
        if not args.dir_path:
            raise Exception("Activity log directory path required")

        filter_pattern = None
        if args.filter_files:
            filter_pattern = args.filter_files

        with utils.SpoolManager("activity_dir.spool") as spool_manager:
            parse_kinesis_dir(args.dir_path, filter_pattern=filter_pattern, spool_manager=spool_manager)

        return

    if args.func == "tail_nginx_log":
        setproctitle.setproctitle("tail_nginx_log")
        if not args.log_file:
            raise Exception("To do tail specifify log path")

        tail_log(
            args.log_file,
            debug=args.debug
        )
        return

    if args.func == "sync_activity_log_tail":
        setproctitle.setproctitle("activity_consumer")
        if not args.log_file:
            raise Exception("To do activity sync specify log path")

        sync_activity_log_tail(
            args.log_file
        )

        return

    if args.func == "consume_kinesis_stream":
        if not args.kinesis_stream:
            raise Exception("To consume kinesis stream provide stream name")

        try:
            debug = args.debug
            spool_manager = utils.SpoolManager(
                "kinesis_sync.spool"
            )
            _, starting_point = spool_manager.get_file_point_file(pointer_str=False)

            consume_kinesis_stream(
                args.kinesis_stream,
                starting_point=starting_point,
                shard=args.kinesis_shard,
                shard_spool=spool_manager,
                debug=debug
            )
        except:
            traceback.print_exc()

        return

    if args.func == "kinesis_shards":
        if not args.kinesis_stream:
            raise Exception("Kinesis stream provide kiness stream")

        print_stream_shards(args.kinesis_stream)
        return

    raise Exception("Invalid function")


def kill_signal_handler(signal, frame):
    print('You pressed Ctrl+C!')
    global_var.SIGKILL = True

    sys.exit(0)


def usr2_signal_handler(signal, frame):
    global_var.SIGUSR2 = True

signal.signal(signal.SIGINT, kill_signal_handler)
signal.signal(signal.SIGUSR2, usr2_signal_handler)
run()
