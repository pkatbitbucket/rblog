$("#btnInsuranceType").on("click",function(e){
    var productType = $("#divInsuranceType").text();
    var url="";
    $.each(linkList,function(index, row){
        if(row.name +" Insurance" == productType){
            url = row.link;
            return;
        }
    });
    utils.gtm_push("AboutUsDropDownQuotes",
        "About Us - Get Quotes",
        productType,
        "", {},
        function () {
            window.location.href = url;
        }
    );
    window.location.href = url;
});

$("#cbInsuranceType li").on("click",function(){
    $(this).parents(".form-row").find("button").prop("disabled",false);
});



// waypoints js for header theme change
$('.waypoint').each(function(){
    var _this = this;
    var inview = new Waypoint({
        element: _this,
        handler: function (direction) {
            $('#top-bar').toggleClass($(this.element).attr('id'), direction === 'down');},
        offset: '64px'
    });

    var offview = new Waypoint({
        element: _this,
        handler: function (direction) {
            $('#top-bar').toggleClass($(this.element).attr('id'), direction === 'up');},
        offset: function() {  return -$(this.element).outerHeight(); }
    });
});

function initControls(){
    var life = $("#lifeCarousel");
    life.owlCarousel({
        navigation : false, // Show next and prev buttons
        slideSpeed : 1000,
        paginationSpeed : 400,
        singleItem:true,
        autoPlay:10000,
        autoplayHoverPause:true
    });
    $(".anchor.next").click(function(){
        life.trigger('owl.next');
    });

    $(".anchor.prev").click(function(){
        life.trigger('owl.prev');
    });
    utils.createDropDowns('#cbInsuranceType','#cbInsuranceType ul li');
}

$(document).ready(function(){
    initControls();
});