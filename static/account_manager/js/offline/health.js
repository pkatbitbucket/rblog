/**
 * Created by hemant on 7/01/16.
 */


CF = {

    post: function(){
        $('#submit_form').addClass('disabled');
        var O = this;
        $.post('',$('#offline_form').serialize(), // serializes the form's elements.
           function(data){
            console.log(data); // show response from the php script.
               if(data.status === 'ok'){
                    $('#offline_form').hide();
                    var after = '<p>Transaction ID: ' + data.transaction_id +
                                '<br><a href="">create new transaction</a></p>';
                    $('.alert.alert-success').removeClass('hide').html('<b>Bingo!</b> <br>Transaction created successfully.').after(after);
               }
               else{
                    O.mapServerErrors(data.errors);
               }
               $('#submit_form').removeClass('disabled');
           }
        );
    },

    mapServerErrors: function (form_errors) {

            var frm = $('#offline_form'),
                frm_fields = [],
                b = frm.serializeArray();
            for (var i in b) frm_fields.push(b[i].name);

            var err_all = '',
                vdict = {};
            for (var field in  form_errors) {
                if ($.inArray(field, frm_fields) >= 0) {         //if that field is in form fields
                    vdict[field] = form_errors[field].join('<br/>');
                }
                else {
                    err_all +='<b>'+ field + '</b> : ' + form_errors[field].join('* <br/>') + '<br>';
                }
            }

            $('.form-group.has-error .help-block').remove();
            $('.form-group.has-error').removeClass('has-error');
            for(var k in vdict){
                $('input[name="'+ k +'"], select[name="'+ k +'"]').closest('.form-group')
                    .addClass('has-error')
                    .children('div').append('<span class="help-block">'+ vdict[k] +'</span>');
            }

            $('.form_error_1').hide().empty();
            if(err_all !== ''){
                $('.form_error_1').fadeIn().html(err_all);
            }
        },

    bind_events: function () {
        var O = this;
        $('.dateinput').datepicker({format: "yyyy-mm-dd", autoclose: true, todayHighlight: true});
        // update policy end date to 1 year
        $('#id_policy_start_date').on('change',function(){
            var start_date = $('#id_policy_start_date').datepicker('getDate');
            start_date.setYear(start_date.getFullYear() + 1)
            $('#id_policy_end_date').datepicker( "setDate", start_date);
        });

        $('select').select2();

        // submit button click handler.
        $('#submit_form').on('click', function (e) {
            e.preventDefault();

            if(!O.get_insurers_data().length){
                $('.form_error_1').fadeIn().html('Select at least one member to be insured.');
                setTimeout(function(){$('.form_error_1').fadeOut()},8000);
                return;
            }
            O.get_medical_data()
            O.post();
        });

        //get insurance plans by insurer
        $('#id_insurer').on('change',function(){
            $.get('/user/cfsupport/get-health-plans/',{'insurer_id': this.value},function(d){
                $('#id_plan_name').empty().select2({placeholder: "Select a Plan"});
                if(d.status === 'ok') {
                    $.each(d.results,function(i, o){
                            var option = $('<option value="' + o.id + '">'+ o.title +'</option>').data('slabs', o.slabs);
                            $('#id_plan_name').append(option);
                        });
                }
            });
        });

        //set slabs by plan type
        $('#id_plan_name').on('change',function(){
            var slabs = $('#id_plan_name option:selected').data('slabs');
            if(slabs){
                $('#id_slab_id').empty().select2({placeholder: "Select a Premium"});
                $.each(slabs, function(i, o){
                    var option = $('<option value="' + o.id + '">'+ o.sum +'</option>');
                    $('#id_slab_id').append(option);
                });
            }
        });

    },

    manage_members: function(){
        var O = this;
        var cont = $('.members_cont');

        $('input[name="proposer_gender"]').on('change',function(){
             if($('input[name="proposer_gender"]:checked').val() == 'MALE'){
                 $('.cf_spouse').text('Wife');
             }
            else{
                 $('.cf_spouse').text('Husband');
             }
        });

        $('.add_daughter, .add_son').on('click', function(){
            var txt = $(this).hasClass('add_son')?'Son':'Daughter',

            p = '<p><label><input type="checkbox" checked value="'+ txt.toLocaleLowerCase() +'" > '+ txt +'</label>';
            p += '<select><option value="">Select age</option><option value="1 months">1 months</option><option value="2 months">2 months</option><option value="3 months">3 months</option><option value="4 months">4 months</option><option value="5 months">5 months</option><option value="6 months">6 months</option><option value="7 months">7 months</option><option value="8 months">8 months</option><option value="9 months">9 months</option><option value="10 months">10 months</option><option value="11 months">11 months</option><option value="1 years">1 years</option><option value="2 years">2 years</option><option value="3 years">3 years</option><option value="4 years">4 years</option><option value="5 years">5 years</option><option value="6 years">6 years</option><option value="7 years">7 years</option><option value="8 years">8 years</option><option value="9 years">9 years</option><option value="10 years">10 years</option><option value="11 years">11 years</option><option value="12 years">12 years</option><option value="13 years">13 years</option><option value="14 years">14 years</option><option value="15 years">15 years</option><option value="16 years">16 years</option><option value="17 years">17 years</option><option value="18 years">18 years</option><option value="19 years">19 years</option><option value="20 years">20 years</option><option value="21 years">21 years</option><option value="22 years">22 years</option><option value="23 years">23 years</option><option value="24 years">24 years</option><option value="25 years">25 years</option></select></p>';
            p = $(p);
            cont.append(p);
            p.find('input').on('change',function(){p.remove();});
            $('select').select2();
        });

        cont.on('change','input[type="checkbox"], select', function(){
            O.health_reset = true;
            $('#id_medical_conditions').trigger('change');
        });
    },

    get_insurers_data:function(){
        data = [];
        $('.members_cont p').each(function(){
            var p = $(this);
            if(p.find('input')[0].checked && p.find('select').val()){
                data.push({'person': p.find('label').text().trim(), 'age': p.find('select').val()})
            }
        });
        $('#id_members_json').val(JSON.stringify(data));
        return data;
    },

    health_reset : false,
    health_conditions: function(){
        var O = this;
        $('#id_medical_conditions').on('change', function(){
            var opts = $('#id_medical_conditions option:selected');
            $('.affected_members > div').addClass('remove');

            $.each(opts, function(i, e){
                var e = $(e), mem = '', F=$('.health_'+ e.val());

                if(F.length && !O.health_reset){F.removeClass('remove');return;}
                O.health_reset = false;

                $.each(O.get_insurers_data(), function(i, d){
                    mem += '<label><input type="checkbox" value="'+ d.person +'"> '+ d.person + '</label>';
                });
                var member = '<div class="health_'+ e.val() +'">' +
                        '<p>'+ e.text() +'</p>' + mem +
                        '</div>'
                $('.affected_members').append(member);
            });

            $('.affected_members > div.remove').remove();
        });
    },
    get_medical_data: function () {
        var medical_cond = [];
        $('#id_medical_conditions option:selected').each(function () {
            var persons = []
            $('.health_' + this.value + ' input:checked').each(function () {
                persons.push(this.value)
            });
            if (persons.length) {
                medical_cond.push({
                    id: this.value,
                    text: this.text,
                    person: persons
                })
            }
        })
        $('#id_health_json').val(JSON.stringify(medical_cond));
    },

    init: function () {
        var O = this;
        $(document).on('ready', function () {
            O.bind_events();
            O.manage_members();
            O.health_conditions();
        });
    }
}

CF.init();