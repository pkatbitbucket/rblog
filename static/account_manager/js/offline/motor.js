/**
 * Created by vinay on 12/11/15.
 */
var quote_details = {};
var proposal_details = {};
var form_posted = false;


var set_date_pickers = function () {
    $('.dateinput').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true
    })
};

var set_search_selectors = function () {
    $('select').select2();
};

var post_details = function (callback) {
    if (form_posted) {
        console.log('form is already being submitted');
    } else {
        form_posted = true;
        $('#success').addClass('hide');
        $('#failure').addClass('hide');
        $.post(
            '',
            {
                'data': JSON.stringify({'quote_details': quote_details, 'proposal_details': proposal_details}),
                'csrfmiddlewaretoken': CSRF_TOKEN
            },
            function (e) {
                if (e.success) {
                    $('#success').removeClass('hide').text('Transaction ' + e.transaction_id + ' created successfully');
                } else {
                    $('#failure').removeClass('hide')
                }
                $('#quote_form_details').html(e.quote_form_details);
                $('#proposal_form_details').html(e.proposal_form_details);
            },
            'json'
        ).always(
            function () {
                $('[id^="quote"]').removeClass('hide');
                $('[id^="proposal"]').addClass('hide');
                init();
                if(callback){
                    callback();
                }
            }
        );
        form_posted = false;
    }
};

$('#quote_submit').on('click', function (e) {
    e.preventDefault();
    var quote_form = $('#quote_form');
    var data = quote_form.serializeArray();
    for (var i in data) {
        if (data.hasOwnProperty(i)) {
            quote_details[data[i].name] = data[i].value;
        }
    }
    $('[id^="quote"]').addClass('hide');
    $('[id^="proposal"]').removeClass('hide');
    set_search_selectors();
});

$('#proposal_submit').on('click', function (e) {
    e.preventDefault();
    $('button').addClass('disabled');
    var proposal_form = $('#proposal_form');
    var data = proposal_form.serializeArray();
    for (var i in data) {
        if (data.hasOwnProperty(i)) {
            proposal_details[data[i].name] = data[i].value;
        }
    }
    post_details(function () {
        $('button').removeClass('disabled');
    });
});

$('#proposal_back').on('click', function (e) {
    e.preventDefault();
    quote_details = {};
    $('[id^="proposal"]').addClass('hide');
    $('[id^="quote"]').removeClass('hide');
    set_search_selectors();
});

var init = function () {
    set_date_pickers();
    set_search_selectors();
};

init();
