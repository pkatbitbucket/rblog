$(document).ready(function(){
    // loading screen for animation setup
    $('#content-wrapper').addClass('hide');
    $('.my-modal').addClass('hide');

    setTimeout(function(){
        $('.loading-screen').addClass('cf-hide');
        $('#content-wrapper').removeClass('hide');
        $('.my-modal').removeClass('hide');
        //animation init for policy cards loading
        if($('.policy-window')[0]){
            $('#all-plans').addClass('shown');
            setTimeout(function(){
                $('.policy-window').removeClass('policy-hide');
            },200);
        }
    }, 300);

    // toggle sidenav for mobile devices
    function toggleSideNav($ele) {
        var $sideNavEle = $('.left-col');
        if($ele.css('display')=='block') {
            console.log($ele.css('display'));
            if (!($ele.hasClass('active'))) {
                $ele.addClass('active');
                $sideNavEle.addClass('active');
            }
            else {
                $ele.removeClass('active');
                $sideNavEle.removeClass('active');
            }
        }
    }
    $(document).on('click','#toggle-nav',function(){
        toggleSideNav($(this));
    });

    if($('.policy-window')[0]){

        function noPlansCheck($plans,context) {
            var contextText = context;
            var $noPlanMsg = $('#no-plans');
            var $msgContextEle = $('#no-plans h4 > span');
            $noPlanMsg.addClass('animate-hide');
            $noPlanMsg.find('#redirect-btn').attr('href','/').text('Click here');
            if(!($plans.size() > 0)) {
                var btnUrl='/';
                var propositionHeading ='';
                var companiesHeading ='';
                var proposition = [];
                var $propositionList = $noPlanMsg.find('.proposition ul');
                $noPlanMsg.removeClass('animate-hide');
                $noPlanMsg.find('.companies img').addClass('hide');
                $noPlanMsg.find('.companies img.'+context).removeClass('hide');
                $noPlanMsg.find('.companies,.proposition').removeClass('hide');
                if(context=='health'){
                    proposition = [
                        "Don't pay extra for features you don't need",
                        "Get end to end assistance with proposal form",
                        "Make sure you are not insured more than necessary",
                        "Get priority help if you make a claim"
                    ];
                    btnUrl='/health-plan/';
                    propositionHeading='Health Insurance Plans starting @ Rs 6 / day';
                    companiesHeading='Compare policies from 20+ companies';
                }
                if(context=='car'){
                    proposition = [
                        "Get a Dedicated Claims Manager",
                        "Cashless Settlement in nearest garage",
                        "Free towing and pick-up services",
                        "Guaranteed End to End Claims Assistance"
                    ];
                    btnUrl='/motor/car-insurance/';
                    propositionHeading='Get 100% Claims support with Coverfox';
                    companiesHeading='Choose plans from top companies';
                }
                if(context=='bike'){
                    proposition = [
                        "Dedicated Relationship Manager",
                        "Quickest claim settlement",
                        "100% Genuine Bike Parts"
                    ];
                    btnUrl='/motor/twowheeler-insurance/';
                    propositionHeading='Additional Benefits form Coverfox [Free]';
                    companiesHeading='Choose plans from top companies';
                }
                if(context=='travel'){
                    proposition=[
                        'Compare cheapest quotes from top insurers.',
                        'Buy & print the policy online in less than 5 minutes',
                        'Get Cashless Emergency treatment in local hospitals.',
                        '100% post-sale service & assistance (endorsements, extensions and claims)',
                        'Visa Consulate approved plans.',
                        'Expert advice to help you choose the best plan'
                    ];
                    btnUrl='/travel-insurance/';
                    propositionHeading='CHEAP. FAST. BEST Travel Insurance products!';
                    companiesHeading='Compare Travel Insurance Plans from Top Companies';
                }
                if(['health', 'car', 'bike', 'travel'].indexOf(context) == -1){
                    proposition = [];
                    $noPlanMsg.find('.companies,.proposition').addClass('hide');
                    btnUrl='/';
                    propositionHeading='';
                    companiesHeading='';
                    if(context=='all'){
                        contextText='';
                    }
                }
                // add proposition/logos/headers to no plans message
                var btnText='Buy '+contextText+' Insurance';
                $msgContextEle.text(contextText);
                $noPlanMsg.find('.proposition h5').text(propositionHeading);
                $noPlanMsg.find('.companies h5').text(companiesHeading);
                $noPlanMsg.find('#redirect-btn').attr('href',btnUrl).text(btnText);
                $propositionList.empty();
                for(var i=0;i<proposition.length;i++) {
                    $propositionList.append('<li>'+proposition[i]+'</li>');
                }
            }
        }
        function Navigate(){
            var $innerSideNavElements = $('.side-nav li.active ul li');
            var $allPlansEle = $('#all-plans');
            var $policyCards = $('.policy-card');
            var $policyCategory = $('#policy-category');
            var $policyWindow = $('.policy-window');
            var self = this;
            self.animateShow = function(){
                $('body').addClass('hide-overflow');
                $policyWindow.addClass('policy-hide');
                setTimeout(function(){
                    $('body').removeClass('hide-overflow');
                    $policyWindow.removeClass('policy-hide');
                },200);
            };
            self.closeCard = function($ele){
                var $visibleCard = $ele.parents('.policy-card');
                $visibleCard.removeClass('open');
            };
            self.openCard = function($ele){
                var $card = $ele.parents('.policy-card');
                if($card.hasClass('open')) {
                    $card.removeClass('open');
                }
                else {
                    $policyCards.removeClass('open');
                    var scrollComp = $('.header-wrapper').outerHeight() + 10;
                    var offset = $card.offset().top - scrollComp;
                    $card.addClass('open');
                    scrollTo(document.body, offset, 400);
                }
            };
            self.showAllPlans = function () {
                toggleSideNav($('#toggle-nav'));
                $innerSideNavElements.removeClass('active');
                $policyCards.removeClass('hide open');
                if(!($allPlansEle.hasClass('shown'))) {
                    $allPlansEle.addClass('shown');
                    self.animateShow();
                }
                $policyCategory.text('My Policies '+'('+ $policyCards.size() +')');
                noPlansCheck($policyCards,'all');
                scrollTo(document.body, 0, 400);
            };
            self.filterPlans = function($ele) {
                toggleSideNav($('#toggle-nav'));
                var context = $ele.attr('id');
                var $filteredItems = $policyCards.filter('.'+context);
                if(!($ele.hasClass('active'))) {
                    $innerSideNavElements.removeClass('active');
                    $ele.addClass('active');
                    $policyCategory.text(context+' Policies '+'('+ $filteredItems.size() +')');
                    $policyCards.addClass('hide').removeClass('open');
                    $filteredItems.removeClass('hide');
                    self.animateShow();
                    $allPlansEle.removeClass('shown');
                    noPlansCheck($filteredItems,context);
                    scrollTo(document.body, 0, 400);
                }
            };
        }
        /*
        function invokeEditModal ($ele) {
            var identifier = $ele.parents('.policy-card').attr('');
            var $modal = $('#edit-name');
            $modal.addClass('active');
            var $closeModalEle = $modal.find('.close-modal');
            var $submitModalEle = $modal.find('.submit');
            var $inputModalEle = $modal.find('input.pol-name');
            var $errorModalEle = $modal.find('.error-text');
            $errorModalEle.addClass('hide');
            $submitModalEle.click(function(){
                $errorModalEle.addClass('hide').text('');
                $submitModalEle.text('Processing...');
                var data = {
                    name:$inputModalEle.val(),
                    id: identifier
                };
                console.log(JSON.stringify(data));
                $.post(" ", {'data' : JSON.stringify(data), 'csrfmiddlewaretoken' : csrf_token}, function(data){
                    console.log('i am here');
                    if (data.success){
                        $modal.removeClass('active');
                    }
                    else{
                        console.log('POST ERROR', data);
                        $submitModalEle.text('Submit');
                        $errorModalEle.removeClass('hide').text('Some thing went wrong. Please try again');
                    }
                }, 'json');
            });
            $closeModalEle.click(function(){
                $modal.removeClass('active');
            });
        }
        */
        function invokeClaimModal ($ele,context) {
            var $modal = $('#initiate-claim');
            var $closeModalEle = $modal.find('.close-modal');
            var $submitModalEle = $modal.find('.submit');
            var $errorEle = $modal.find('.error-text');
            var $claimSub = $modal.find('input[name="subject"]');
            var $claimInfo = $modal.find('textarea[name="description"]');
            var $claimNum = $modal.find('input[name="mobile"]');
            var $claimQuerryContact = $modal.find('.claim-querry-contact');
            var $responseMsg = $modal.find('.r-msg');
            $modal.addClass('active');
            var $card = $ele.parents('.policy-card');
            // get information from card
            var formData = getPolicyData($ele);
            // fill information in modal
            $modal.find('.policy-num').text(formData.policyNum);
            $modal.find('input[name="mobile_2"]').val(formData.custMobile);
            $modal.find('.policy-category').text(formData.policyCategory);
            $modal.find('input[name="policy_category"]').val(formData.policyCategory);
            $modal.find('.policy-name').text(formData.policyName);
            $modal.find('.policy-expiry').text(formData.policyExpiry);
            $modal.find('.policy-covers').text(formData.policyCovers);
            $modal.find('input[name="claim_transaction_id"]').val(formData.transactionId);

            $submitModalEle.text('Initiate a Claim');
            
            // dynamic call/claim response messaging js
            var claims = claimsMessaging(context);
            $claimQuerryContact.html(claims.name+' on <strong>'+claims.contact+'</strong>');
            $responseMsg.text(claims.message);

            // insurer logo on messaging
            var imgSrc = '/static/account_manager/img/insurers/'+formData.insurerSlug+'.jpg';
            var imgAlt = formData.insurerTitle;
            $modal.find('ul.policy-info > li.logo img').attr({'src':imgSrc,'alt':imgAlt});
            

            $closeModalEle.click(function(){
                $modal.removeClass('active');
            });
            $modal.click(function(event){
                if(event.target == $modal[0]){
                    $modal.removeClass('active');
                }
            });
            // claim raise logic and form submission
            if (formData.isClaimed =='True') {
                console.log('Already Claimed');
                // initiate already claimed msg
                claimAlreadyRegistered();
            }
            else if(formData.isClaimed =='False') {
                console.log('Not Claimed');
                // initiate claim form
                claimFormInit();
            }

            // messaging and form show/hide functions
            function claimAlreadyRegistered(){
                $modal.find('.back-msg').show();
                $modal.find('.success-msg').hide();
                $modal.find('.front-form').hide();
            }
            function claimFormInit() {
                $modal.find('.success-msg').hide();
                $modal.find('.back-msg').hide();
                $modal.find('.front-form').show();
                $errorEle.text('').hide();
                $claimNum.val(formData.custMobile);
                $claimInfo.val('');
                $claimSub.val('');
            }
        }



        $("#initiate-claim .submit").click(function () {
            var $modal = $('#initiate-claim');
            var $errorEle = $modal.find('.error-text');
            var $claimSub = $modal.find('input[name="subject"]');
            var $claimInfo = $modal.find('textarea[name="description"]');
            var $claimNum = $modal.find('input[name="mobile"]');
            var transaction_id = $modal.find('input[name="claim_transaction_id"]').val();

            $modal.find('.policy-category').text();

            var cnum = $claimNum.val();
            var cinfo = $claimInfo.val();
            var csub = $claimSub.val();
            $errorEle.text('').hide();
            var res = true;
            // data to be sent
            var data = {
                category: $modal.find('input[name="policy_category"]').val(),
                description: cinfo,
                subject: csub,
                mobile_1: cnum,
                mobile_2: $modal.find('input[name="mobile_2"]').val(),
                transaction_id: transaction_id,
            };

            if(!(validation.isNotEmpty(cinfo))) {
                res = false;
                $("#description-error").text("This field is required.").show();
            }

            if(!(validation.isNotEmpty(csub))) {
                res = false;
                $("#subject-error").text("This field is required.").show();
            }

            if(!(validation.isNumber(cnum))) {
                res = false;
                $("#mobile-error").text("Please enter a 10 digit mobile number.").show();
            }

            if(!(validation.isNumber(cnum)) || cnum.length != 10) {
                res = false;
                $("#mobile-error").text("Please enter a 10 digit mobile number.").show();
            }

            if(res) {
                console.log(JSON.stringify(data));
                $(this).text('Processing...');
                $.post('/user/submit-claim/', {'data': JSON.stringify(data), 'csrfmiddlewaretoken': csrf_token}, function (data) {
                    console.log('i am here');
                    if (data.success) {
                        console.log("post success");
                        claimSuccess();
                    }
                    else {
                        console.log('POST ERROR', data);
                        $errorEle.text('Something went wrong! Please try again.');
                        $(this).text('Initiate a Claim');
                    }
                }, 'json');
            }


            function claimSuccess(){
                $(this).text('Initiate a Claim');
                $('#'+transaction_id).val('True');
                $modal.find('.success-msg').show();
                $modal.find('.front-form').hide();
            }
        });



        function getPolicyData($ele){
            var $form = $ele.parents('.policy-card').find('form');
            console.log($form.find('input[name="PolicyCategory"]')[0].value);
            var formData = {
                policyNum: $form.find('input[name="PolicyNum"]').val() || 'NA',
                policyCategory: $form.find('input[name="PolicyCategory"]').val() || 'NA',
                policyName: $form.find('input[name="PolicyName"]').val() || 'NA',
                policyExpiry: $form.find('input[name="PolicyExpiry"]').val() || 'NA',
                policyCovers: $form.find('input[name="PolicyCovers"]').val() || 'NA',
                insurerSlug: $form.find('input[name="InsurerSlug"]').val() || 'NA',
                insurerTitle: $form.find('input[name="InsurerTitle"]').val() || 'NA',
                isClaimed: $form.find('input[name="IsClaimed"]').val() || 'False',
                custMobile: $form.find('input[name="CustMobile"]').val() || 'NA',
                transactionId: $form.find('input[name="TransactionId"]').val() || 'NA',
            };
            //console.log($form.find('input[name="PolicyNum"]'),formData);
            return formData;
        }
        /* ------------------- page navigation part of this app -------------------------*/
        // initiate policy list page js
        var navigate = new Navigate();
        noPlansCheck($('.policy-card'),'all');

        //policy open/close mechanism
        $(document).on('click','.policy-card a.open-btn',function(event){
            event.preventDefault();
            navigate.openCard($(this));
        });
        $(document).on('click','.policy-card a.close-btn',function(event){
            event.preventDefault();
            navigate.closeCard($(this));
        });
        // policy filter mechanism
        $(document).on('click','.side-nav li.active > a',function(event){
            event.preventDefault();
            navigate.showAllPlans();
        });
        $(document).on('click','.side-nav li.active ul li',function(){
            navigate.filterPlans($(this));
        });
        /* Edit policy Custom Name
        $(document).on('click','.policy-card .edit-name',function(){
            invokeEditModal($(this));
        });
        */
        // Claim for policy
        $(document).on('click','.policy-card .initiate-claim',function(){
            var context = $(this).attr('data-context');
            invokeClaimModal($(this),context);
        });
    }
    /* --------- Policy details view tab functionality -------- */
    if($('.tab')[0]) {
        $(document).on('click','.tab .tab-item',function(){
            var $this = $(this);
            var tab_id = $this.attr('data-tab');
            var parentTabNav = $this.parents('.tab');
            parentTabNav.children().each(function(){
                $(this).removeClass('active');
            });
            $this.addClass('active');
            parentTabNav.siblings('.tab-content').each(function(){
                $(this).addClass('hide');
                if($(this).hasClass(tab_id)){
                    $(this).removeClass('hide');
                }
            });
        });
    }

    /* -------- Pending profile view edit functionality --------- */
    /*
    if($('.profile-window')[0]){
        $(document).on('click','.field .edit',function(){
            var $this = $(this);
            $this.parents('.field').addClass('active');
        });
    }
    */
});



// Validations

var validation = {
    isEmailAddress: function(str) {
        var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return pattern.test(str);
    },
    isNotEmpty: function(str) {
        var pattern = /\S+/;
        return pattern.test(str);
    },
    isNumber: function(str) {
        var pattern = /[7-9]{1}\d{9}/;
        return pattern.test(str);
    },
    isName: function(str) {
        var pattern = /^[a-zA-Z ]{2,30}$/;
        return pattern.test(str);
    }
};

// page scroll javascript
function scrollTo(element, to, duration) {
    var start = element.scrollTop,
        change = to - start,
        currentTime = 0,
        increment = 20;

    var animateScroll = function(){
        currentTime += increment;
        var val = Math.easeInOutExpo(currentTime, start, change, duration);
        element.scrollTop = val;
        if(currentTime < duration) {
            setTimeout(animateScroll, increment);
        }
    };
    animateScroll();
};
//t = current time
//b = start value
//c = change in value
//d = duration
Math.easeInOutExpo = function (t, b, c, d) {
    t /= d/2;
    if (t < 1) return c/2 * Math.pow( 2, 10 * (t - 1) ) + b;
    t--;
    return c/2 * ( -Math.pow( 2, -10 * t) + 2 ) + b;
};