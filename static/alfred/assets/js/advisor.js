var ko = require('knockout');
var urls = require('urls');
var ameyo_int_custom = require('./ameyo-integration-custom');
var note_chat = require('components/note-chat/note-chat')

//TODO: fix the ameyo and other calling library
wmodel = require('./models/incoming-call');
window.incoming_call = new wmodel();


/* Include all stores here. This the entry point for all stores */
require("models/ticket-store");
require("components/mail/store");

ko.components.register('home', {
    viewModel: require('models/home'),
    template: require("raw!templates/home.html")
});

ko.components.register('thread-list', {
    viewModel: require('components/mail/thread-list'),
    template: require("raw!components/mail/thread-list.html")
});

ko.components.register('ticket-detail', {
    viewModel: require('models/ticket-detail'),
    template: require("raw!templates/ticket-detail.html")
});

ko.components.register('some-page', {
    viewModel: require('models/some'),
    template: require("raw!templates/some.html")
});

ko.components.register('document-upload', {
    viewModel: require('models/document'),
    template: require("raw!templates/document_form.html")
});

ko.components.register('about-page', {
    template: require('raw!templates/about.html')
});

ko.components.register('ameyo-toolbar', {
    viewModel: require('models/ameyo-toolbar'),
    template: require("raw!templates/ameyo-toolbar.html")
});

ko.components.register('incoming-call', {
    viewModel: {instance: window.incoming_call},
    template: require("raw!templates/incoming-call.html")
});

ko.components.register('ticket-list', {
    viewModel: require('models/ticket-list'),
    template: require("raw!templates/ticket-list.html")
});

ko.components.register('dashboard', {
    viewModel: require('models/dashboard'),
    template: require("raw!templates/dashboard.html")
});

ko.components.register('open-ticket', {
    viewModel: require('models/open-ticket'),
    template: require("raw!templates/open-ticket.html")
});

ko.components.register('search-ticket', {
    viewModel: require('models/search-ticket'),
    template: require("raw!templates/search-ticket.html")
});

ko.components.register('my-ticket-notes', {
    viewModel: require('models/my-ticket-notes'),
    template: require("raw!templates/my-ticket-notes.html")
});

ko.components.register('policy-summary', {
    viewModel:  require('models/policy-summary'),
    template: require("raw!templates/policy-summary.html")
});

ko.components.register('create-ticket', {
    viewModel: require('models/create-ticket'),
    template: require("raw!templates/create-ticket.html")
});

ko.components.register('note-chat', {
    viewModel: note_chat.viewModel,
    template: note_chat.template
});

ko.applyBindings({ route: urls.currentRoute });
