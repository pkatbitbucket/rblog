var ko = require('knockout');
var urls = require("urls");
var store = require("models/store");


var AmeyoToolbar = function(params){
    var self = this;
    self.iframeUrl = ko.observable(AMEYO_TOOLBAR_URL + "origin=" + window.location.origin);

    self.showCallBtn = ko.observable(params.showCallBtn);
    self.customer_number = params.customer_number;
    self.customer_name = params.customer_name;

    self.dialGivenNumber = function(number){
        doDial(number);
    }
}

module.exports = AmeyoToolbar;
