var ko = require('knockout')
var store = require("models/store");


var InternalNotesForm = function(data) {
    var self = this;
    self.internal_notes_form = ko.observable(false);
    self.ticket = ko.observable(false);
    self.auto_save = ko.observable(true);
    self.auto_save_class = ko.computed(function() {
        if(self.auto_save()){
            return 'btn-success';
        }else{
            return 'btn-danger';
        }
    });
    self.auto_save_text = ko.computed(function() {
        if(self.auto_save()){
            return 'AUTO SAVE ON';
        }else{
            return 'AUTO SAVE OFF';
        }
    });
    self.show_manual_submit = ko.computed(function() {
        if(self.auto_save()){
            return false;
        }else{
            return true;
        }
    });
    self.ticket_id = ko.observable(false);
    self.ticket_id(data.value())
    self.toggle_auto_save = (function(){
        if(self.auto_save()){
            self.auto_save(false)
        }else{
            submit_internal_notes_form()
            self.auto_save(true)
        }
    })
    $.get(INTERNAL_NOTES_FORM_URL.replace("9999999999", self.ticket_id()), function(data) {
        self.internal_notes_form(data.internal_notes_form);
    })

    var internal_notes_form_id = '#internalNotesFormId' + self.ticket_id()
    var internal_notes_form_btn_id = '#internalNotesFormBtnId' + self.ticket_id()
    var timeout;

    var submit_internal_notes_form = (function() {
        var internal_notes_form = $(internal_notes_form_id);
        $.ajax({
            type: "POST",
            url: internal_notes_form.attr('action'),
            data: internal_notes_form.serialize(),
            success: function(data) {
                responseData = jQuery.parseJSON(data)
                if(responseData.success) {
                    $("#notes_last_updated_on").html("Last Updated on - " + responseData.response.last_updated_on)
                }else{
                    alert("Error!")
                    $.each(responseData.errors, function(error_name, error_message){
                        alert($("#" + error_name + "_errors").html() + " " + error_message)
                    })
                }
            },
        });
    })

    $(document).on('click', internal_notes_form_btn_id, function(e) {
        e.preventDefault();
        submit_internal_notes_form()
    });

    var autoSaveNotes = function() {
        if(self.auto_save()){
            submit_internal_notes_form()
        }
    }

    $(document).on('keyup', "#id_notes", function() {
        $("#notes_last_updated_on").html('Typing....')
        if(timeout) {
            clearTimeout(timeout);
            timeout = null;
        }
        timeout = setTimeout(autoSaveNotes, 1000);
    });

}

module.exports = InternalNotesForm;