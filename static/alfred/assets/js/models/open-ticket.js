var ko = require('knockout')

var PolicySearchForm = function(route) {
    var self = this;
    self.ticket_id = ko.observable();
    if(route.ticket_id){
        self.ticket_id(route.ticket_id());
    }
    self.policies = ko.observableArray([]);
    self.policy_number = ko.observable();
    self.customer_mobile = ko.observable();
    self.customer_email = ko.observable();
    self.results_count = ko.observable('NA');
    self.create_ticket_form = ko.observable();
    self.show_create_ticket_form = ko.observable(false);

    self.set_show_create_ticket = function(policy_pk) {
        for(i=0;i<self.policies().length;i++) {
            self.policies()[i].show_create_ticket(false)
            if(self.policies()[i].pk == policy_pk)
                self.policies()[i].show_create_ticket(true)
        }
    };


    self.search_policy = function(){
         $.ajax({
            type: "POST",
            url: POLICY_SEARCH_URL,
            data: {
                'policy_number': self.policy_number(),
                'customer_mobile': self.customer_mobile(),
                'customer_email': self.customer_email(),
                'csrfmiddlewaretoken': CSRF_MIDDLEWARE_TOKEN
            },
            success: function(data) {
                self.show_create_ticket_form(false);
                responseData = JSON.parse(data);
                if(responseData.success) {
                    self.results_count(responseData.response.count);
                    self.policies(responseData.response.results)
                    for(i=0;i<self.policies().length;i++) {
                        if(!self.policies()[i].data){
                            self.policies()[i].data = {};
                        }
                        self.policies()[i].show_create_ticket = ko.observable(false)
                        self.policies()[i].open_create_ticket = (function(data) {
                            self.set_show_create_ticket(data.policy.pk)
                            return true
                        })
                    }
                }
            },
        });
    }

    self.get_ticket_form = function(policy_id){
        debugger;
        $.ajax({
            type: 'GET',
            url: (function(){
                if(self.ticket_id()){
                    return UPDATE_TICKET_FORM_URL.replace('7979797979', self.ticket_id())
                }
                else{
                    return CREATE_TICKET_FORM_URL
                }
            })(),
            data: {
                'policy_id': policy_id,
                'policy_number': (function (){
                    if(self.policy_number()){
                        return "MANUAL-" + self.policy_number()
                    }
                    return ""
                }),
            },
            success: function(data){
                self.create_ticket_form(data);
                self.show_create_ticket_form(true);
            }
        });
    }

    self.force_create_policy = function(params){
        self.get_ticket_form(params.policy_id);
    };
}

module.exports = PolicySearchForm;
