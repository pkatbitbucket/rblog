var ko = require('knockout')
var store = require("models/store");
var ticket = require("models/ticket");
var urls = require("urls");

var TicketSearchForm = function(params) {
    var self = this;
    self.ticket_search_form = ko.observable(false);
    self.tickets = ko.observableArray(params.searched_tickets() || []);
    /*
    $.get(TICKET_SEARCH_FORM_URL, function(data) {
        console.log("----ticket search form----")
        console.log(data)
        self.ticket_search_form(data.ticket_search_form);
    });
    */

    self.clear_filters = function(){
        tickets = [];
        pushed_tickets = [];
        for(var i=0; i<params.searched_tickets().length; i++){
            ti = params.searched_tickets()[i];
            if((store.ticketStore.myticket_ids().indexOf(ti.id) > -1) && (pushed_tickets.indexOf(ti.id) < 0)){
                pushed_tickets.push(ti.id);
                ti.hide = false;
                tickets.push(ti);
            }
        }
        params.searched_tickets([]);
        params.searched_tickets(tickets);
        urls.hasher.setHash('ticket-list/');
    }

    /*
    $(document).on('submit', '#ticketSearchForm', function(e) {
        var ticket_search_form = $('#ticketSearchForm');
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: ticket_search_form.attr('action'),
            data: ticket_search_form.serialize(),
            success: function(data) {
                responseData = jQuery.parseJSON(data)
                if(responseData.success) {
                    console.log("success")
                    tickets = [];
                    for(var i=0; i<params.searched_tickets().length; i++){
                        ti = params.searched_tickets()[i];
                        if(store.ticketStore.myticket_ids().indexOf(ti.id) > -1){
                            ti.hide = true;
                            tickets.push(ti);
                        }
                    }
                    for(var i=0; i<responseData.response.results.length; i++){
                        ti = responseData.response.results[i];
                        ti['hide'] = false;
                        tickets.push(new ticket(ti));
                    }
                    params.searched_tickets([]);
                    params.searched_tickets(tickets);
                    for(var i=0; i<params.searched_tickets().length; i++){
                        console.warn(params.searched_tickets()[i].hide);
                    }
                    for(i=0;i<self.tickets().length;i++) {
                        self.tickets()[i].show_create_ticket = ko.observable(false)
                        self.tickets()[i].open_create_ticket = (function(data) {
                            self.set_show_create_ticket(data.ticket.pk)
                            return true
                        })
                    }
                }else{
                    alert("Error!")
                }
            },
        });
    });
    */

}

module.exports = TicketSearchForm;
