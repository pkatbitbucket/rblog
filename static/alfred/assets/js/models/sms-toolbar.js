var ko = require('knockout')
var store = require("models/store");


var SendSMSForm = function(data) {
    var self = this;
    var y = [];
    self.ticket_id = ko.observable(false);
    self.ticket_id(data.value())
    self.template_id = ko.observable();
    self.sms_templates = data.sms_templates().template_objs;
    self.params_map = data.sms_templates().params_map;
    self.sms_success_msg = ko.observable();
    self.sms_text = ko.computed(function(){
        self.sms_success_msg("");
        for(var i=0; i<data.sms_templates().template_objs.length; i++){
            if(data.sms_templates().template_objs[i].pk == self.template_id()){
                sms_text = data.sms_templates().template_objs[i].template;
                y = sms_text.split(/{|}/);
                for(var i=0; i<y.length; i++){
                    if(self.params_map[y[i]] != undefined){
                        y[i] = "<div class='pull-left m-l-5'><div class='fg-line'><input class='form-control input-sm' id='id_sms_var_" + y[i] + "' placeholder='" + y[i] + "' value='" + self.params_map[y[i]] + "'></div></div>";
                    }
                    else{
                        y[i] = "<strong class='pull-left p-t-5'>" + y[i] + "</strong>"
                    }
                }
                return y.join('')
            }
        }
    });
    self.send_sms = function() {
        for(var key in self.params_map){
            var x = document.getElementById('id_sms_var_' + key);
            if(x){
                self.params_map[key] = x.value;
            }
        }
        $.ajax({
            type: "POST",
            url: SEND_SMS_FORM_URL.replace("9999999999", self.ticket_id()),
            data: {
                'template': self.template_id(),
                'params_map': JSON.stringify(self.params_map),
                'csrfmiddlewaretoken': CSRF_MIDDLEWARE_TOKEN,
            },
            success: function(data) {
                var resp = JSON.parse(data);
                if(resp.success && resp.response.success){
                    self.sms_success_msg("SMS sent successfully");
                }
                else{
                    self.sms_success_msg("SMS could not be sent, inform it to Admin");
                }
            },
        });
    };
}

module.exports = SendSMSForm;
