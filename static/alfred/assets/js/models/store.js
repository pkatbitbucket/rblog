var ko = require('knockout');
var io = require('socket.io');

var socket = io(TICKET_NODE_URL);

key_store_map = {};

console.log("Loading Store service...");
socket.on('connect', function() {
    console.log("Connected!");
    socket.emit('subscribe_to_channels', {'channels' : channels, 'userId' : user_id});
});

socket.on('update', function(message) {
    console.log("Update received ", message.msg.command, message.msg.data);
    var commandArgs = message.msg.command.split('_');
    var appName = commandArgs[0];
    var command = commandArgs[1];
    for(msg_type in key_store_map) {
        if(appName == msg_type){
            key_store_map[msg_type][command](message.msg.data);
        }
    }
});

socket.on('disconnect', function() {
    console.log("Disconnected!");
});

var sget = function(prefix, command, data) {
    data['app'] = prefix;
    data['command'] = command;
    socket.emit('message', data);
};

var add_store = function(store){
    // msg_type : the string to identify the data pushed from backend
    // e.g. if msg_type=ticket then all data pushed from node backend
    // which start with "ticket_" withh move to this store
    //
    // key : the key using which this store will be referenced e.g. store.ticketStore
    //
    // store : the store object which should have add method to add
    // objects to specific type
    var dstore = new store();
    if(dstore.init) {
        dstore.init();
    }
    key_store_map[dstore.prefix] = dstore;
    storeContext[dstore.local] = dstore;

    // Add a generic get method to stores
    dstore.sget = function(command, data) {
        sget(dstore.prefix, command, data);
    }
}


var storeContext = {
    add : add_store,
}

module.exports = storeContext;
