var ko = require('knockout')
var Ticket = require('models/ticket');
var store = require('models/store')
var urls = require('urls')

ko.components.register('conversation-history', {
    viewModel: require('models/conversation-history'),
    template: require("raw!templates/conversation-history.html")
});

ko.components.register('sms-toolbar', {
    viewModel: require('models/sms-toolbar'),
    template: require("raw!templates/sms-toolbar.html")
});

ko.bindingHandlers.selectPicker = {
    init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        $(element).selectpicker();
    }
};

var Disposition = function(ticket, state_form_name) {
    var self = this;
    self.errors = [];
    self.state_form_success = ko.observable(false);
    self.head_disposition = ko.observable();
    self.sub_dispositions = ko.observableArray();
    self.task_form = ko.observable();
    self.head_disposition.subscribe(function(){
        self.sub_dispositions([]);
    });
    self.error_message = ko.observable();
    self.save = function(params) {
        if(!(params.ticket().next_task_user_id() == user_id) && self.head_disposition() && !(self.head_disposition().pk in [1, 7])){
            self.error_message("You are not allowed to dispose with this disposition");
        }
        else{
            for(var i=0; i<params.suggested_documents().length; i++){
                if(!params.suggested_documents()[i].category().pk){
                    self.error_message("Select valid document category in the attached documents");
                    return 0;
                }
            }
            self.error_message("");
            self.state_form_success(true);
            if(self.head_disposition()){
                hdisposition = self.head_disposition().pk;
            }
            else{
                hdisposition = "";
            }
            sdispositions = ko.utils.arrayMap(self.sub_dispositions(), function(item){return item.pk});
            disposition_data = {
                'head_disposition': hdisposition,
                'sub_dispositions': sdispositions,
            };
            var state_form = $('#state_form');
            var state_form_data = state_form.serializeObject();
            $.ajax({
                type: "POST",
                cache: false,
                url: SAVE_STATE_FORM_URL.replace('7777777777', ticket().id).replace('DUMMY_STATE_FORM', state_form_name()),
                data: {
                    'state_form_data': JSON.stringify(state_form_data),
                    'existing_documents': ko.toJSON(params.existing_documents()),
                    'suggested_documents': ko.toJSON(params.suggested_documents()),
                    'disposition_data': JSON.stringify(disposition_data),
                    'csrfmiddlewaretoken': CSRF_MIDDLEWARE_TOKEN,
                },
                success: function(data){
                    if(data.success){
                        state_form.cleanFormErrors();
                        console.log('Form submitted successfully');
                        self.task_form(data.task_form);
                        ticket().update(data.ticket);
                        $('#id_next_task_time').datetimepicker({format: "DD/MM/YYYY hh:mm:ss"});
                    }
                    else{
                        self.state_form_success(false);
                        state_form.populateFormErrors(data.errors);
                        $.each(data.errors, function(k, v){
                            self.error_message(k + ": " + v);
                        });
                        console.log('Form submitted with errors returned', data);
                    }
                },
            });
        }
    };
}

var TicketDetail = function(route) {
    var self = this;
    self.ticket = ko.observable();
    self.ticket_status_form = ko.observable();
    self.state_form = ko.observable();
    self.state_form_name = ko.observable();
    self.task_form_success = ko.observable();
    self.sms_templates = ko.observable();

    self.dispositionList = ko.observableArray();
    self.conv_histories = ko.observableArray();

    self.existing_documents = ko.observableArray();
    self.suggested_documents = ko.observableArray();
    self.document_categories = ko.observableArray();
    self.chat_notes = ko.observableArray([]);
    self.my_notes = ko.observable();
    self.chat_notes_post_url = NOTES_FORM_URL.replace('9999999999', route.ticket_id).replace('8888888888', 'chat_notes');
    self.dispositionState = new Disposition(self.ticket, self.state_form_name);

    self.get_ticket_details = function(){
        $.get(TICKET_DETAIL_URL.replace("9999999999", route.ticket_id), function(data){
            self.ticket(store.ticketStore.cache_ticket(data.ticket));
            self.ticket_status_form(data.ticket_status_form);
            self.state_form(data.state_form);
            self.state_form_name(data.state_form_name);
            self.chat_notes(JSON.parse(data.chat_notes));
            self.my_notes(data.my_notes);
            self.sms_templates(data.sms_templates);

            self.dispositionList(data.dispositions);

            self.document_categories(data.document_categories);
            suggested_documents = [];
            existing_documents_category = []

            for(var i = 0; i < data.existing_documents.length; i++){
                existing_documents_category.push(data.existing_documents[i].category.pk);
            }

            for(var index in data.suggested_documents){
                if(existing_documents_category.indexOf(data.suggested_documents[index].category.pk) < 0){
                    suggested_documents.push(data.suggested_documents[index])
                }
            }
            self.existing_documents(data.existing_documents);
            self.suggested_documents(suggested_documents);
        })

        $.get(GET_CONV_HISTORIES.replace("6969696969", route.ticket_id), function(data){
            if(data.success)
                self.conv_histories(data.activities);
            else
                alert("kuch toh fhata!!!")
        })
    };
    self.get_ticket_details();
    self.refresh = function(){
        if(self.task_form_success()){
            self.get_ticket_details();
            self.task_form_success(false);
            $('#confirm_close_ticket_modal').modal('hide');
        }
    }
    self.goto_ticket_list = function(){
        urls.hasher.setHash('ticket-list');
    }


    $(document).on('submit', '#ticket_status_form', function(e){
        var ticket_status_form = $('#ticket_status_form');
        e.preventDefault();
        console.log(ticket_status_form.serialize());
        $.ajax({
            type: "POST",
            url: ticket_status_form.attr('action'),
            data: ticket_status_form.serialize(),
            success: function(data){
                data = JSON.parse(data);
                console.log("Ticket Status Form Submitted", data);
            },
        });

    });

    $(document).on('submit', '#task_form', function(e){
        self.task_form_success(true);
        var task_form = $('#task_form');
        e.preventDefault();
        console.log($('#id_next_task_time').val());
        $.ajax({
            type: "POST",
            url: task_form.attr('action'),
            data: task_form.serialize(),
            success: function(data){
                data = JSON.parse(data);
                if(data.success){
                    console.log("Task Form Submitted", data);
                    task_form.cleanFormErrors();
                    $('#confirm_close_ticket_modal').modal('show');
                }
                else{
                    task_form.populateFormErrors();
                    self.task_form_success(false);
                    console.log("Task form submitted with errors returned", data);
                }
            },
        });

    });
}

module.exports = TicketDetail;
