var ko = require("knockout");
var urls = require("urls");
var store = require("models/store");

ko.components.register('ticket-summary', {
    viewModel: function(params) {
        var self = this;
        self.ticket = params.value;
        self.isIncomingPopup = ko.observable(params.isIncomingPopup);
        self.filter_panels = ko.observableArray([]);
        if(params.filters){
            self.filter_panels = params.filters;
        }
        self.areFiltersTrue = ko.pureComputed(function(){
            for(var j in self.filter_panels()){
                options_to_filter = filter_panels_map[j].properties[self.filter_panels()[j].selectedOption()]
                for(opt in options_to_filter){
                    if(filter_functions_map[self.filter_panels()[j].filter_function()](
                        self.filter_panels()[j].key(), options_to_filter[opt], [self.ticket]
                    ).length == 0){
                        return false
                    }
                }
            }
            return true
        })
        self.proceedWithTicket = function(ticket_id){
            window.open('/advisor/dashboard/#/ticket/' + ticket_id);
       }
    },
    template: require("raw!templates/ticket-summary.html")
});

ko.components.register('filter-element', {
    viewModel: function(params) {
        var self = this;
        self.availableOptions = params.value.availableOptions;
        self.selectedOption = params.value.selectedOption;
        self.panel_name = params.value.panel_name;
    },
    template: require("raw!templates/filter-element.html")
});

filter_panels_map = [
    {'filter_function': 'filterQuery',
     'key': 'priority',
     'properties': {'All': ['All'],
                    'Critical': ['Critical'],
                    'High': ['High'],
                    'Low': ['Low'],
                    'Normal': ['Normal']},
     'verbose': 'Priority'},

    {'filter_function': 'filterQuery',
     'key': 'state',
     'properties': {'All': ['All'],
                    'Assigned': ['New'],
                    'Closed Successfully': ['Close resolved'],
                    'Closed Unsuccessfully': ['Close unresolved'],
                    'Pending Customer Information': ['Documents pending'],
                    'Pending Customer Notification': ['QC pending', 'Request accepted', 'Request rejected', 'Insurer intimated'],
                    'Pending Ops Information': ['Policy pending', 'Documents received', 'Documents uploaded', 'Insurer decision pending']},
     'verbose': 'Status'},

    {'filter_function': 'filterIn',
     'key': 'tags',
     'properties': {'All': ['All'],
                    'Complaints': ['Complaint'],
                    'Escalations': ['Escalation']},
     'verbose': 'Issue'},

    {'filter_function': 'filterQuery',
     'key': 'request',
     'properties': {'All': ['All'],
                    'Approved': ['Approved'],
                    'Cancel': ['Cancel'],
                    'Claim': ['Claim'],
                    'Clean Flow': ['Approved'],
                    'Endorsement': ['Endorsement'],
                    'Refund': ['Refund'],
                    'Fresh': ['Fresh'],
                    'Inquiry': ['Inquiry'],
                    'Offline': ['Offline']},
     'verbose': 'Request Type'},

    {'filter_function': 'filterDateDiff',
     'key': 'due_date_ticket',
     'properties': {'All': ['All'],
                    'Overdue >15 Days': ['inf-15 days in past'],
                    'Overdue 8-15 Days': ['15-8 days in past'],
                    'Overdue 4-7 Days': ['7-4 days in past'],
                    'Overdue 1-3': ['3-1 days in past'],
                    'Due Today': ['today'],
                    'Due 1-3 Days': ['1-3 days in future'],
                    'Due 4-7 Days': ['4-7 days in future'],
                    'Due 8-15 Days': ['8-15 days in future'],
                    'Due 15> Days': ['15-inf days in future']},
     'verbose': 'Due Date'}
]

filter_panels_map.push(
    {
        'filter_function': 'filterQuery',
        'key': 'task_assigned_to',
        'properties': {'All': ['All'],},
        'verbose': 'Next Task Assigned'
    }
)
ADVISORS_LIST.subscribe(function(){
    for(var i=0; i<ADVISORS_LIST().length; i++){
        filter_panels_map[filter_panels_map.length-1]['properties'][ADVISORS_LIST()[i]] = [ADVISORS_LIST()[i]];
    }
});

sorting_panels_map = {
    "Sort By": {
        'Ticket ID - Asc':
        function(a, b){
            if (parseInt(a['id']) < parseInt(b['id'])) return -1;
            if (parseInt(b['id']) < parseInt(a['id'])) return 1;
            return 0;
        },
        'Ticket ID - Desc':
        function(a, b){
            if (parseInt(a['id']) < parseInt(b['id'])) return 1;
            if (parseInt(b['id']) < parseInt(a['id'])) return -1;
            return 0;
        },
        'Priority - Low to Critical':
        function(a,b){
            var priority_list = ['Low', 'Normal', 'High', 'Critical'];
            var compA = $.inArray(a['priority'](), priority_list);
            var compB = $.inArray(b['priority'](), priority_list);
            if (compA < compB) return -1;
            if (compA > compB) return 1;
            return 0;
        },
        'Priority - Critical to Low':
        function(a,b){
            var priority_list = ['Low', 'Normal', 'High', 'Critical'];
            var compA = $.inArray(a['priority'](), priority_list);
            var compB = $.inArray(b['priority'](), priority_list);
            if (compA < compB) return 1;
            if (compA > compB) return -1;
            return 0;
        },
        'Status - New to Closed':
        function(a,b){
            var state_list = ['Policy pending','Documents received','Documents uploaded','Insurer decision pending','All','Close resolved','QC pending','Policy approved','Policy uploaded','Policy dispatch pending','Policy dispatched','Request accepted','Request rejected','Insurer intimated','Close unresolved','New','Documents pending'];
            var compA = $.inArray(a['state'](), state_list);
            var compB = $.inArray(b['state'](), state_list);
            if (compA < compB) return -1;
            if (compA > compB) return 1;
            return 0;
        },
        'Status - Closed to New':
        function(a,b){
            var state_list = ['Policy pending','Documents received','Documents uploaded','Insurer decision pending','All','Close resolved','QC pending','Policy approved','Policy uploaded','Policy dispatch pending','Policy dispatched','Request accepted','Request rejected','Insurer intimated','Close unresolved','New','Documents pending'];
            var compA = $.inArray(a['state'](), state_list);
            var compB = $.inArray(b['state'](), state_list);
            if (compA < compB) return 1;
            if (compA > compB) return -1;
            return 0;
        },
        'Last Updated':
        function(a, b){
            if (a['last_modified_on'] < b['last_modified_on']) return 1;
            if (b['last_modified_on'] < a['last_modified_on']) return -1;
            return 0;
        },
        'Due Date':
        function(a, b){
            if (a['due_date_ticket'] < b['due_date_ticket']) return 1;
            if (b['due_date_ticket'] < a['due_date_ticket']) return -1;
            return 0;
        },
    },
}

filter_functions_map = {
    'filterQuery':
    function(filter_on, query_string, filter_objs) {
        if(typeof(filter_objs) == 'undefined'){
            filter_objs = self.tickets()
        }
        if(query_string == 'All'){
            return filter_objs
        }

        return ko.utils.arrayFilter(filter_objs, function (filter_obj) {
            if(ko.isObservable(filter_obj[filter_on])){
                if(typeof(filter_obj[filter_on]) == 'undefined'){
                    return false
                }
                return (filter_obj[filter_on]().toLowerCase().indexOf(query_string.toLowerCase()) > -1)
            }else{
                return (filter_obj[filter_on].toLowerCase().indexOf(query_string.toLowerCase()) > -1)
            }
        });
    },

    'filterDateDiff':
    function(filter_on, query_string, filter_objs) {
        if(typeof(filter_objs) == 'undefined'){
            filter_objs = self.tickets()
        }
        if(query_string == 'All'){
            return filter_objs
        }

        return ko.utils.arrayFilter(filter_objs, function (filter_obj) {
            if(typeof(filter_obj[filter_on]) == 'undefined'){
                return false
            }
            filter_obj_date = new Date(filter_obj[filter_on]().split(" ")[0].split("/").reverse().join("-"))
            var now_date = new Date()
            now_date.setHours(0)
            now_date.setMinutes(0)
            now_date.setSeconds(0)
            now_date.setMilliseconds(0)
            if(query_string == 'today'){
                return (filter_obj_date.getTime() == now_date.getTime())
            }
            date_diff = query_string.split(" ")[0]
            time_period_type = query_string.split(" ")[1]
            when = query_string.split(time_period_type + " ")[1]
            switch(when){
                case "in future":
                    var future_start_date = new Date()
                    future_start_date.setHours(0)
                    future_start_date.setMinutes(0)
                    future_start_date.setSeconds(0)
                    future_start_date.setMilliseconds(0)
                    var future_end_date = new Date()
                    future_end_date.setHours(0)
                    future_end_date.setMinutes(0)
                    future_end_date.setSeconds(0)
                    future_end_date.setMilliseconds(0)
                    if((date_diff == 'all')){
                        return (filter_obj_date > now_date)
                    }
                    future_start_date.setDate(future_start_date.getDate() + parseInt(date_diff.split("-")[0]))
                    if(date_diff.split("-")[1] != 'inf'){
                        future_end_date.setDate(future_end_date.getDate() + parseInt(date_diff.split("-")[1]))
                        return ((future_start_date <= filter_obj_date) && (filter_obj_date <= future_end_date))
                    }
                    return (future_start_date <= filter_obj_date)
                case "in past":
                    var past_start_date = new Date()
                    past_start_date.setHours(0)
                    past_start_date.setMinutes(0)
                    past_start_date.setSeconds(0)
                    past_start_date.setMilliseconds(0)
                    var past_end_date = new Date()
                    past_end_date.setHours(0)
                    past_end_date.setMinutes(0)
                    past_end_date.setSeconds(0)
                    past_end_date.setMilliseconds(0)
                    if((date_diff == 'all')){
                        return (filter_obj_date < now_date)
                    }
                    past_end_date.setDate(past_end_date.getDate() - parseInt(date_diff.split("-")[1]))
                    if(date_diff.split("-")[0] != 'inf'){
                        past_start_date.setDate(past_start_date.getDate() - parseInt(date_diff.split("-")[0]))
                        return ((past_start_date <= filter_obj_date) && (filter_obj_date <= past_end_date))
                    }
                    return (filter_obj_date <= past_end_date)
                default:
                    console("incorrect when " + when)
                    return false
            }
        });
    },

    'filterIn':
    function(filter_on, query_string, filter_objs) {
        if(typeof(filter_objs) == 'undefined'){
            filter_objs = self.tickets()
        }
        if(query_string == 'All'){
            return filter_objs
        }

        return ko.utils.arrayFilter(filter_objs, function (filter_obj) {
            value = false
            if(typeof(filter_obj[filter_on]) == 'undefined'){
                return false
            }
            if(filter_obj[filter_on]){
                for(i=0;i<filter_obj[filter_on].length;i++){
                    value = (filter_obj[filter_on][i].toLowerCase().indexOf(query_string.toLowerCase()) > -1)
                    if(value){
                        break
                    }
                }
                return value
            }else{
                return false
            }
        });
    }
}

var TicketList = function(route) {
    var self = this;
    self.tickets = store.ticketStore.tickets;
    self.INITIAL_VISIBLE_COUNT = 50;
    self.INCREMENT_VISIBLE_COUNT = 10;
    self.ticket_show_limit = ko.observable(self.INITIAL_VISIBLE_COUNT)
    self.show_more_tickets = function() {
        self.ticket_show_limit(self.ticket_show_limit() + self.INCREMENT_VISIBLE_COUNT)
    }

    self.filter_panels = ko.observableArray([]);
    self.sort_panels = ko.observableArray([]);
    self.filters = [];
    if(route.filter_json){
        filter_jsons = route.filter_json.split("+");
        for(var i_f in filter_jsons){
            filter_json = filter_jsons[i_f];
            if(filter_json){
                var verbose = filter_json.split(":")[0].split("_").join(" ");
                var option = filter_json.split(":")[1].split("_").join(" ");
            }
            else{
                var verbose = '';
                var option = '';
            }
            var data = {};
            data.verbose = verbose;
            data.option = option;
            self.filters.push(data);
        }
    }
    for (var i in filter_panels_map){
        availableOptions = [];
        for (option in filter_panels_map[i]['properties']){
            availableOptions.push(option);
        }
        var data = {'availableOptions': availableOptions, 'selectedOption': 'All',
        'panel_name': filter_panels_map[i]['verbose'], 'key': filter_panels_map[i]['key'],
        'filter_function': filter_panels_map[i]['filter_function']};

        for(var j in self.filters){
            if(self.filters[j].verbose == filter_panels_map[i].verbose){
                data.selectedOption = self.filters[j].option;
            }
        }
        var obj = new FilterPanel(data);
        self.filter_panels().push(obj);
    }

    for (var panel in sorting_panels_map){
        availableOptions = [];
        for (option in sorting_panels_map[panel]){
            availableOptions.push(option);
        }
        var data = {'availableOptions': availableOptions, 'selectedOption': 'Ticket ID - Asc', 'panel_name': panel,
        'key': '', 'filter_function': ''};
        var obj = new FilterPanel(data);
        self.sort_panels().push(obj);
    }
    self.filtered_tickets = ko.pureComputed(function(){
        for (var i in self.sort_panels()){
            self.tickets().sort(sorting_panels_map[self.sort_panels()[i].panel_name()][self.sort_panels()[i].selectedOption()]);
        }
        return self.tickets()
    })
    self.tickets(self.filtered_tickets());
}

var FilterPanel = function(data) {
    var self = this;
    self.availableOptions = ko.observable(data.availableOptions);
    self.selectedOption = ko.observable(data.selectedOption);
    self.panel_name = ko.observable(data.panel_name);
    self.key = ko.observable(data.key);
    self.filter_function = ko.observable(data.filter_function);
}

module.exports = TicketList;
