var ko = require('knockout');
var store = require('models/store');
var Ticket = require('models/ticket');

ADVISORS_LIST = ko.observableArray();

function TicketStore() {
    var self = this;
    self.prefix = "ticket";
    self.local = "ticketStore"
    // Tickets that belong to me and are pushed from node are in self.tickets
    self.tickets = ko.observableArray([]);

    // Tickets that are searched or randomly accessed, are a part of self.cached_tickets
    // We will implement a logic to optimize this so that older cached_tickets can be removed
    self.cached_tickets = ko.observableArray([]);

    //TODO: GROT myticket_ids
    self.myticket_ids = ko.observableArray([]);
    self.push = function(data) {
        if(data.task_assigned_to && ADVISORS_LIST().indexOf(data.task_assigned_to) < 0){
            ADVISORS_LIST.push(data.task_assigned_to);
        }
        var existingTicket = ko.utils.arrayFirst(self.cached_tickets(), function(item){
            return item.id == data.id;
        });

        // Check if ticket is availalble in self.tickets()
        // If ticket is not available in self.tickets() but in self.cached_tickets (which
        // means the ticket was in the system because if search or direct link)
        // then we need to insert the ticket in the self.tickets() array
        var ticket_in_my_tickets = ko.utils.arrayFirst(self.tickets(), function(item){
            return item.id == data.id;
        });

        if(existingTicket){
            existingTicket.update(data);
            if(!ticket_in_my_tickets) {
                self.tickets.push(existingTicket);
            }
        } else{
            var new_ticket = new Ticket(data);
            self.tickets.push(new_ticket);
            self.cached_tickets.push(new_ticket)
        }
        self.myticket_ids.push(data.id);
    }

    self.cache_ticket = function(ticket_data) {
        var ticket = ko.utils.arrayFirst(self.cached_tickets(), function(item){
            return item.id == ticket_data.id;
        });
        if(ticket) {
            ticket.update(ticket_data);
        } else {
            var ticket = new Ticket(ticket_data);
            self.cached_tickets.push(ticket);
        }
        return ticket;
    }

    self.remove = function(data){
        var existingTicket = ko.utils.arrayFirst(self.tickets(), function(item){
            return item.id == data.id;
        });
        if(existingTicket){
            self.tickets.remove(existingTicket)
        }
        else{
            console.log("No tickets to remove");
        }
        self.myticket_ids.remove(data.id)
    }
}
store.add(TicketStore);

module.exports = TicketStore;
