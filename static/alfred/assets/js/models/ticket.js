var ko = require('knockout');
var urls = require("urls");

var Ticket = function(data) {
    //TODO: 1> All data which might change because of backend-push or user behavior, should be observable.
    //      2> All data that is just for representation etc. can be inside self.data
    var self = this;
    var params = [
        'id',
        'category_id',
        'created_by_id',
        'created_on',
        'data',
        'start_date',
        'last_modified_by_id',
        'is_active',
        'machine_id',
        'owner_id',
        'product_type',
        'hide',
        'owner',
    ]
    var observableParams = [
        'request',
        'sub_request',
        'status',
        'state',
        'next_task',
        'is_taken',
        'priority',
        'tags',
        'due_date_task',
        'last_modified_on',
        'task_assigned_to',
        'name',
        'email',
        'mobile',
        'policy_number',
        'policy_document',
        'due_date_ticket',
        'next_task_user_id',
    ]
    for(var i=0; i< observableParams.length; i++){
        if(observableParams[i] in data) {
            self[observableParams[i]] = ko.observable(data[observableParams[i]]);
        }
    }

    for(var i=0; i< params.length; i++){
        self[params[i]] = null;
    }

    self.show_detail_view = function(){
        urls.hasher.setHash('ticket/' + self.id);
    }

    self.update = function(data){
        for(var i=0; i< params.length; i++){
            if(params[i] in data) {
                self[params[i]] = data[params[i]];
            }
        }
        for(var i=0; i< observableParams.length; i++){
            if(observableParams[i] in data) {
                self[observableParams[i]](data[observableParams[i]]);
            }
        }
    }

    if(data) {
        self.update(data);
    }
}

module.exports = Ticket;
