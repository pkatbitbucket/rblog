ko.validation.configure({
    errorMessageClass: 'error',
    decorateElement : true,
    errorElementClass: 'has-error',
    insertMessages: false
});

var LeadCard = function(){
    var self = this;

    if(document.location.href.indexOf("/articles/health-insurance/")!=-1)
        self.mode = "HEALTH";
    else if(document.location.href.indexOf("/articles/car-insurance/")!=-1)
        self.mode = "MOTOR";
    else
        self.mode = "GENERAL";

    self.loading = ko.observable(false);
    self.thanksMessage = ko.observable(false);

    self.mobileNo = ko.observable(Cookies.get('mobileNo')).extend({
        required:{
            params: true,
            message: "Please enter your mobile number"
        },
        pattern: {
            params: '^([1-9])([0-9]){9}$',
            message: "Please enter a valid mobile number"
        }
    });
    self.policyType = ko.observable().extend({
        required:{
            params: true,
            message: "Please select one option"
        }
    });

    self.showLeadCard = ko.observable(!self.mobileNo());
    self.policy_type_options = [
        {'id':'opt1', value:'new', text:' Buy policy for a new car'},
        {'id':'opt2', value:'renew', text:' Renew existing policy'},
        {'id':'opt3', value:'expired', text:' Renew expired policy'},
    ];

    self.validate = function(){
        self.mobileNo.isModified(true);
        self.policyType.isModified(true);
        var mobile_valid = self.mobileNo.isValid();
        var policy_selected = (self.mode!='MOTOR')?true:self.policyType();
        return mobile_valid && policy_selected;
    };

    var dnd_waiver = function() {
        return confirm("I hereby authorize Coverfox to communicate with me on the given number for my Insurance needs. I am aware that this authorization will override my registry under NDNC.");
    };

    self.callMe = function(label){
        if(self.validate()) {
            if(!dnd_waiver()){
                return false;
            }
            self.loading(true);
            var lms_campaign = 'Health';
            var motor_policy_type = '';
            if(self.mode=="MOTOR") {
                motor_policy_type = self.policyType();
                lms_campaign = 'Motor'
            }

            var post_data = {
                'mobile': self.mobileNo(),
                'campaign' : lms_campaign,
                'triggered_page': window.location.href,
                'label': label,
                'policy_type': motor_policy_type
            };
            $.ajax({
                url: '/leads/save-call-time/',
                type: 'POST',
                data: {'data':JSON.stringify( post_data), 'csrfmiddlewaretoken': csrf_token},
                success: function(res) {
                    Cookies.set('mobileNo', self.mobileNo(), {expires:604800});
                    if(window.dataLayer) {
                        window.dataLayer.push({
                            'event': 'GAEvent',
                            'eventCategory': self.mode,
                            'eventAction': 'CallScheduled',
                            'eventLabel': label,
                            'eventValue': 4
                        });
                    }

                    self.thanksMessage(true);
                    self.loading(false);
                    setTimeout(function() {
                        self.showLeadCard(false);
                    }, 10000);
                },
                error: function() {
                    setTimeout(function() {
                        self.loading(false);
                        setTimeout(function() {
                            self.mobileNo.error('Error while submitting the call request');
                        }, 50);
                    }, 400);
                }
            });
        }
    };
};

ko.applyBindings(new LeadCard());
