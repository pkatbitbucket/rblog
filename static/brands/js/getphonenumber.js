function submit_phone_number(){
    var post_data = {
        'campaign': $('#campaign').val(),
        'label' : $('#insurer_slug').val(),
        'mobile': $('#mobile_number').val(),
        'triggred_page' : window.location.href,
        'device' : 'Desktop'
    };
    $.ajax({
                url: '/leads/save-call-time/',
                type: 'POST',
                data: {'data':JSON.stringify(post_data), 'csrfmiddlewaretoken': CSRF_TOKEN},
                success: function(res) {
                    $('.com_box2').html('<p>Our customer care team will contact you shortly</p>')
                },
                error: function(res){
                    $('.com_box2').html('<p>Sorry! seems like our server is down! We\'ll get back to you as soon as its up</p>')
                }
            })
        }
