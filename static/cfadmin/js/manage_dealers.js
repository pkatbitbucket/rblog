var ViewModel = function() {
    var self = this;
    self.table_data = ko.observableArray();
    self.dealership_data = ko.observableArray();
}

var vm = new ViewModel();
$(document).ready(function() {
    ko.applyBindings(vm);
});


function DealershipChanged(element){
    $.get(BASE_URL+"?return=json&dealership_id="+element.value, function( data ) {
        vm.table_data.removeAll();
        for(x in data['dealer_data']){
            vm.table_data.push(data['dealer_data'][x]);
        }
        vm.dealership_data.removeAll();
        vm.dealership_data.push(data['dealership_data']);
    });
}