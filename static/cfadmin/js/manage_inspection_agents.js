$(document).ready(function() {
    $('#inspection_agent_table').DataTable({
        columnDefs: [{
            targets: [1, 2, 3],
            "searchable": false,
        }, {
            targets: [3],
            "sortable": false,
        }]
    });
});