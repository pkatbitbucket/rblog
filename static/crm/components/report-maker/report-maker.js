var ko = require('knockout');
var template = require('raw!./report-maker.html');

var ReportMaker = function(params){
    var self = this;
    self.filter_param = ko.observable();
    self.from_date = ko.observable();
    self.to_date = ko.observable();

    self.headings = ko.observableArray(
        [
            ['Xyz', 'xyz'],
            ['Ayz', 'ayz'],
            ['Byz', 'byz'],
        ]
    );
    debugger;
}

module.exports = {
    viewModel: ReportMaker,
    template: template
};
