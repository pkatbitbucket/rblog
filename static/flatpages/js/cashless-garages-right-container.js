var next_page_url = '/motor/car-insurance/';
        function skipForm(label){
            window.dataLayer.push({
                'event': 'GAEvent',
                'hitType': 'event',
                'eventCategory': 'MOTOR',
                'eventAction': 'hero_skip_'+ label,
                'eventLabel': 'lp_switch',
                'eventValue': 0
            });
            window.location = next_page_url;
            return false;
        }

        var csrf_token;
        $(document).ready(function(){
            $('.contact-me-error').hide();
            $.get("/ajax/get-token/", function(data){
                csrf_token = data.csrf;
            }, 'json');

            var policy_type = 'old';
            Cookies.set('policyExpired', false);
            Cookies.set('isNewVehicle', false);
            $(document).on('click', 'ul.form li', function(){
                var formelements = $('ul.form li');
                formelements.removeClass('active');
                var context = $(this);
                context.addClass('active');
                policy_type = context.data("type");
                if(policy_type=="old"){
                    Cookies.set('policyExpired', false);
                    Cookies.set('isNewVehicle', false);
                    next_page_url = '/motor/car-insurance/';
                }
                if(policy_type=="new"){
                    Cookies.set('policyExpired', false);
                    Cookies.set('isNewVehicle', true);
                    next_page_url = '/motor/car-insurance/';
                }
                if(policy_type=="expired"){
                    Cookies.set('policyExpired', true);
                    Cookies.set('isNewVehicle', false);
                    next_page_url = '/lp/car-insurance/renew-expired-policy/';
                }
            });
            $(document).on('click', '#show_quote_btn', function(){
                window.dataLayer.push({
                    'event': 'GAEvent',
                    'hitType': 'event',
                    'eventCategory': 'MOTOR',
                    'eventAction': 'policy_type_' + policy_type,
                    'eventLabel': 'lp_switch',
                    'eventValue': 0
                });
                window.dataLayer.push({
                    'event': 'GAEvent',
                    'hitType': 'event',
                    'eventCategory': 'MOTOR',
                    'eventAction': 'hero_form_submit',
                    'eventLabel': 'lp_switch',
                    'eventValue': 0
                });

                var context = $(this);
                $('.contact-me-error').hide();
                var cnum = $('#contact-me-num').val();
                var res = null;
                if(cnum){
                    if (cnum.length == 10){
                        res = cnum.match(/[7-9]{1}\d{9}/);
                        if(res){
                            var cmodel =  {
                                'mobile' : cnum,
                                'campaign': 'Motor',
                                'label': 'lp_switch',
                                'policy_type': policy_type,
                                'triggered_page': window.location.href
                            };
                            Cookies.set('mobileNo', cnum);
                            context.addClass('disabled').text('Just a moment...');
                            $.post("/leads/save-call-time/", {'data' : JSON.stringify(cmodel), 'csrfmiddlewaretoken' : csrf_token}, function(data){
                                if (data.success){
                                    if (window.dataLayer){
                                        window.dataLayer.push({'gtm_mobile': cnum});
                                        window.dataLayer.push({
                                            'eventCategory': 'MOTOR',
                                            'eventAction': 'CallScheduled',
                                            'eventLabel': 'lp_car_buy',
                                            'eventValue': 4,
                                            'hitType': 'event',
                                            'event': 'GAEvent',
                                            // 'eventCallback': function(){
                                            //     window.location = next_page_url;
                                            // }
                                        });
                                        window.location = next_page_url;
                                    }else{
                                        window.location = next_page_url;
                                    }
                                }
                                else{
                                    console.log('POST ERROR', data);
                                    window.location = next_page_url;
                                }
                            }, 'json');
                        }
                        else{
                            $('.contact-me-error').show();
                        }
                    }
                    else{
                        $('.contact-me-error').show();
                    }
                }else{
                    window.location = next_page_url;
                }
            })
        });