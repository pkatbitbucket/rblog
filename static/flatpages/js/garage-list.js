
var utils = function(){ };

utils.createSelect = function(element, data){
    var placeHolder = $(element).attr('data-placeholder');
    $(element).html(utils.createOption(data,placeHolder));
    var configParamsObj = {
        minimumResultsForSearch: 1,
    }
    $(element).select2(configParamsObj);
    $(element).parent().find('.select2-choice').css({
        'padding-top': '13px!important'
    })
};


// Create Option string for select
utils.createOption = function(dataset,defaultValue){
    var optionStr = "<option></option>";
    $.each(dataset, function(key, value){
        var id = value.id? value.id:key;
        var text = value.name? value.name:value;
        optionStr += "<option id="+ id + " name="+ text+ " value="+ JSON.stringify(value).trim() + ">"+ text + "</option>"
    });
    return optionStr;
}


$(function(){
    var cashless_garages_insurance_form = $("form[name='cashless-garages-insurance']");

    $("select").select2();  // select-compononet

    get_details().done(function(response){
      utils.createSelect($("select[name='state']"),response.states);
    });


    $("select[name='state']").on('change', function(){
      var state = $(this).val();

      utils.createSelect($("select[name='make']"),{}); // reset select-i/p's
      $("select").not("select[name='state']").parent().removeClass("selected");
      $("select").not("select[name='state']").parent().find(".on-select-label").addClass("hide");

      cashless_garages_insurance_form.find(":input[name='city']").val('');
      cashless_garages_insurance_form.find(":input[name='make']").val('');
      cashless_garages_insurance_form.find(":input[name='state']").val(state);

      $("select[name='cities']").html();
      get_details().done(function(response){
          utils.createSelect($("select[name='cities']"),response.cities);
      });

    });

    $(".select-ip").on("change", function(e){
    	  e.preventDefault();
        var select = $(this);
        $("#garages-pagination").empty();

        if(select.val()){
          $(this).parent().addClass('selected');

          $(this).parent().find('.select2-choice').css({
              'padding-top': ''
          });

          $(this).parent().find(".on-select-label").removeClass("hide");

        }else{
          $(this).select2("data","");
          $(this).parent().removeClass('selected');
          $(this).parent().find(".on-select-label").addClass("hide");
        }
    });

    $("select[name='cities']").on('change', function(){

        $("select[name='make']").parent().removeClass("selected");
        $("select[name='make']").parent().find(".on-select-label").addClass("hide");

        var city = $(this).val();
        cashless_garages_insurance_form.find(":input[name='make']").val('');
        cashless_garages_insurance_form.find(":input[name='city']").val(city);

        $("select[name='make']").html();
        get_details().done(function(response){
            utils.createSelect($("select[name='make']"),response.makes);
        });

    });

    // For "CAR-BRAND = MAKE"
    $("select[name='make']").on('change', function(){
        var make = $(this).val();
        var garage_template = $(".garages-template");
        var garages='';

        cashless_garages_insurance_form.find(":input[name='make']").val(make);

        get_details().done(function(response){
            garage_template.parent().removeClass("hide");
            $.each(response.garages, function(key,val){

              garage_template.find(".index").html(key+1);
              garage_template.find("h3").html(val.name);
              garage_template.find(".address").html(val.address);
              garage_template.find(".location").html(response.city+", "+response.state);
              garages+= garage_template.html();
            });

            var total_garages = response.garages.length;
            $("#garages").parent().find("h4").show().html(total_garages+' Garages');
            $("#garages").html(garages);

            // call to pagination
            if(response.garages.length>8)
              updateItems();
        });

    });


    function get_details(){

      return $.post(
          '/car-insurance/cashless-garages/',
          cashless_garages_insurance_form.serializeArray()
        );
    }

    //$("#pt1").click();



    // Pagination Code
    var perPage = 8;
    // now setup pagination
    var $pagination = $("#garages-pagination");

    $pagination.pagination({
        itemsOnPage: perPage,
        cssStyle: "light-theme",
        onPageClick: function(pageNumber) { // this is where the magic happens
            // someone changed page, lets hide/show trs appropriately
            var showFrom = perPage * (pageNumber - 1);
            var showTo = showFrom + perPage;
            items.hide() // first hide everything, then show for the new page
                 .slice(showFrom, showTo).show();
        }
    });
    // we need an updateItems function that we'll call both initially and
    // whenever we add or remove any items. You'll need to ensure that this
    // function is called whenever your items dynamically change
    function updateItems() {
        items = $("#garages .static-item");
        // we'll update the number of items that the pagination element expects
        // using a method from the simplePagination documentation: updateItems
        $pagination.pagination("updateItems", items.length);

        // Select 1st page always.
        $pagination.pagination("selectPage", 1);
    };
});
