$(function() {
    var nav = $('.goldform');

    var clone = nav.clone();

    nav.addClass('original');
    clone.addClass('clone');
    nav.after(clone);
    $('.goldform.clone').hide();

    offset = $('.goldform.original').offset();
    var fromtop = offset.top;
    $(window).scroll(function() {
        var doc = $(this);
        var dist = $(this).scrollTop();

        if (dist >= fromtop) {
            $('.goldform.clone').show();
            $('.goldform.original').addClass('stick-to-top');
        } else {
            $('.goldform.original').removeClass('stick-to-top');
            $('.goldform.clone').hide();
        }
    });


    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function validatePhone(phone) {
        var matched = phone.match('[0-9]{10}');
        return matched;
    }


    $(document).on('click', '.btn-submit', function(e) {
        e.preventDefault();
        var context = $(this);
        $('.error').hide();

        var valid = true,
            name = $("#name").val(),
            email = $("#email").val(),
            phone = $("#phone").val(),
            CSRF_TOKEN = $("input[name = csrfmiddlewaretoken]").val();

        if (!name) {
            valid = false;
            $('#name-error').show();
        }


        if (!validateEmail(email)) {
            valid = false;
            $('#email-error').show();
        }

        if (!validatePhone(phone)) {
            valid = false;
            $('#phone-error').show();
        }




        if (valid) {
            var post_data = {
                'campaign': 'Health',
                'device': 'Desktop',
                'label': 'Gold_Registration',
                'name': name,
                'email': email,
                'mobile': phone,
                'triggered_page': window.location.href

            };
            context.text("Submitting ...");


            $.post('/leads/save-call-time/', {
                data: JSON.stringify(post_data),
                csrfmiddlewaretoken: CSRF_TOKEN
            }, function() {
                $(".input-form").hide();
                $(".input-form-success").show();
            });
        }

    });


});