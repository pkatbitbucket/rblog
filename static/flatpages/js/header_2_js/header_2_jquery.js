$(document).ready(function(){
    $(window).bind('scroll', function () {
        var miniBarHeight = $('#mini-top-bar').outerHeight();
        if ($(window).scrollTop() > miniBarHeight) {
            $('#top-bar').addClass('fixed');
            if ($(window).scrollTop() > 300) {
                $('.pop-contact').removeClass('reveal');
            }
        } else {
            $('#top-bar').removeClass('fixed');
        }
    });

    $(".nav-toggle").on("click",function(){
        console.log('nav toggle clicked', this);
        if($(this).hasClass('active')){
            $(this).removeClass("active");
            $(".nav-menu").removeClass("active");
        }
        else{
            $(this).addClass("active");
            $(".nav-menu").addClass("active");
        }
    });
    $(".pop-contact label").on("click", function(e){
        if($(this).parent().hasClass("reveal")) {
            $(this).parent().removeClass("reveal");
        }
        else {
            $(this).parent().addClass("reveal");
        }
        e.stopImmediatePropagation();
    });

    $(document).on("click", function(e) {
        e.stopPropagation();
        if ($(e.target).closest('.pop-contact').length==0) {
            $(".pop-contact").removeClass("reveal");
        }
    });
});