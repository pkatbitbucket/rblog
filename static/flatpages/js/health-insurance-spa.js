$(function(){

    var graphDom = d3.select("#SpeedoMeterGraph"), percentage;

    var health_insu_questions = [
        { question: "Do you have Enough money in Bank Account ?", index: 1, selected:null },
        { question: "Do you have great Health ?",  index: 2 , selected:null },
        { question: "Do you have Company Health Cover ?",  index: 3, selected:null },
        { question: "Can you afford to pay Hospital Expenses from your Pocket ?",  index: 4, selected:null },
        { question: "Can you easily get money from relatives/friends incase of Medical Emergency ?",  index: 5, selected:null },
        { question: "Do you want to pass on the burden of paying hospital bills to someone else ?",  index: 6, selected:null },
        { question: "Can you afford to pay 2-3% of your yearly income in health insurance ?",  index: 7, selected:null },
        { question: "You agree that health issues and health related expenses are rising ?",  index: 8, selected:null },
        { question: "You are sure that great health is extremelly important to successful life ?",  index: 9, selected:null },
        { question: "Your Health Insurance is inadequate as of now ?",  index: 10, selected:null },
    ];
    /*var report =['Your savings, or the lack of them, is an important factor to decide whether or not you can afford an urgent medical procedure, at any point in your life. If you can afford a sudden medical expense easily, you may not require health insurance. But if a random hospitalization dents a hole in your savings, health insurance is a great option for you.',
                 'It is common knowledge that insurance should be purchased when you\'re at the best of your health. This makes sure you are covered for more things for a long time, at a considerably lesser premium. Based on your current health condition, we guage whether or not you need insurance.',
                 'Employers offer health insurance as part of your cost to company package. If you already have this, you can afford to skip buying insurance. However, we must put up a disclaimer: corporate insurance covers limited things, gives a limited cover, and offers no tax benefits.',
                 'If you don\'t have health insurance and have a day job, think carefully about this. Can your savings and income provide for your family\'s and your expenses against medical emergencies at any time? If you can, you don\'t need insurance.',
                 'People usually have family and friends to fall back on, who can raise money for them when in a medical emergency. If you do too, you can eliminate the need of insuring your health.',
                 'Depending on someone else for your hospital bills can prove to be very taxing for that person. If you have someone who can foot your bills and you are willing to let them bite the bullet for you, you may not need insurance.',
                 'If you can afford to commit 2-3% of you yearly income to health insurance, you can afford to buy yourself a decent plan that covers you when needed. ',
                 'There is staistical data to prove that with each year the medical expenses start costing a whopping 15% more. A simple appendix surgery or 24 hours in a hospital room will cost much more in the coming years. If you agree, you have the sensibilities to understand that insurance is a need of the hour.',
                 'You need good health not only to succeed but also to enjoy the luxuries that come along. It is important for a peaceful family life as well. If you agree with this, there is a good chance that you see the need for insurance to maintain that good health.',
                 'Compare your health insurance plan with what others have to offer. Check whether the premium you pay covers you for things you need. Check whether the company will settle claims without hassles. Do you have the best you can get? If you do, you\'re fine. But if you don\'t, you need to revisit your insurance policy.',
                 'You seem to be in great position. As of now you are in SAFE ZONE. Probably Health Insurance is not a big priority thing for you, but its better to once check if you need it or not.'
                 ];*/

    var health_insurance = function(health_insu_questions){

        var self = this;
        self.health_insu_questions = ko.observableArray(ko.utils.arrayMap(health_insu_questions,function(question){
            return { question: question.question, index: question.index, selected: ko.observable(question.selected) };
        }));

        var upperButton = false;
        var downButton = false;


        ko.utils.arrayForEach(self.health_insu_questions(), function (row,index) {
            row.selected.subscribe(function(change){
                self.speedoMeterGraph();
                self.triggerNextQuestion();
            });
        });

        self.preventNext = ko.observable(true);
        self.preventPrevious = ko.observable(true);

        self.speedoMeterGraph = function(){
            percentage = self.calculatePercentage();

            //  Percentage to LocalStorage
            localStorage.setItem('percentage',percentage);

            NeedleObject.animateOn(NeedleChartObject.chart, percentage/100)
        };


        self.calculatePercentage = function(){

            var percentageFlag = 0;
            var scoreRange = [
                { 'yes':0 , 'no':10 },
                { 'yes':0 , 'no':10 },
                { 'yes':0 , 'no':10 },
                { 'yes':0 , 'no':10 },
                { 'yes':0 , 'no':10 },
                { 'yes':10 , 'no':0 },
                { 'yes':10 , 'no':0 },
                { 'yes':10 , 'no':0 },
                { 'yes':10 , 'no':0 },
                { 'yes':10 , 'no':0 },
            ];

            ko.utils.arrayForEach(self.health_insu_questions(), function (row) {
                var selectedValue = row.selected();

                if(selectedValue){
                    percentageFlag+= scoreRange[row.index-1][selectedValue];
                }
            });
            return 100-percentageFlag;
        }

        self.triggerNextQuestion = function(){     
            setTimeout(function(){
                nextQuestion();
            },200);
        }

        self.navigationStatus = function(){
            var currentQuestionNumber = self.questionNumberToShow();
            var currentQuestionIndex = currentQuestionNumber - 1;

            var previousQuestion = self.doesQuestionExist(currentQuestionIndex,'Previous');
            var NextQuestion = self.doesQuestionExist(currentQuestionIndex,'Next');

            var preventPreviousFlag = (previousQuestion)? false : true;
            var preventNextFlag = (NextQuestion)? false : true;

            if(self.isOptionSelected(currentQuestionIndex)){

                self.preventPrevious(preventPreviousFlag);
                self.preventNext(preventNextFlag);

            }else{
                self.preventPrevious(preventPreviousFlag);
                self.preventNext(true);
            }

        }
        self.reportText = ko.observable('');
        nextQuestion = function(){
            var currentQuestionNumber = self.questionNumberToShow();
            var currentQuestionIndex = currentQuestionNumber - 1;

            var question = self.doesQuestionExist(currentQuestionIndex,'Next');

            
            if(self.isOptionSelected(currentQuestionIndex)){
              $('.question_container .list:eq('+currentQuestionIndex+')').addClass('hide');
                if(question){
                      self.questionNumberToShow(currentQuestionNumber+1);
                      $('.question_container .list:eq('+currentQuestionNumber+')').removeClass('hide');
                    }
                else  {
                    var report;
                    if(percentage > 70){
                      $('.btn_tool').addClass('hide');
                      report = "You seem to be in great position. As of now you are in SAFE ZONE. Probably Health Insurance is not a big priority thing for you, but its better to once check if you need it or not. <p>Have a chat with our Health Insurance experts. There is no compulsion to buy.</p> ";  
                    }
                    else if(percentage > 40){
                      report = "At this moment you are into IMPROVEMENT ZONE in regard to health insurance. Its better to pass on this risk of paying the the bills to someone else.<p>Have a chat with our Health Insurance experts. There is no compulsion to buy.</p>";
                    }
                    else {
                      report = "Your Health Insurance requirement is very high. You are in DANGER ZONE. Better pass on the risk of paying the bills to insurance companies.<p>Let our Health Insurance experts suggest the best policy for you. There is no compulsion to buy.</p>";
                    }
                    
                    self.reportText(report);
                    $('.show_result').removeClass('hide');
                    $('.infinite_carousel').addClass('hide');
                    $('.question_container').addClass('hide');
                }

                downButton = true;
                upperButton = !downButton;
            }

            self.navigationStatus();
        };

        previousQuestion = function(){
            var currentQuestionNumber = self.questionNumberToShow();
            var currentQuestionIndex = currentQuestionNumber - 1;

            var question = self.doesQuestionExist(currentQuestionIndex,'Previous');

            if(question){
                    self.questionNumberToShow(currentQuestionNumber-1);
                     previousQuestionIndex = currentQuestionIndex-1;
                    $('.question_container .list:eq('+currentQuestionIndex+')').addClass('hide');
                    $('.question_container .list:eq('+previousQuestionIndex+')').removeClass('hide');
                  }


            upperButton = true;
            downButton = !upperButton;

            self.navigationStatus();
        };

        self.doesQuestionExist = function(index,aaa){

            var questionIndex = (aaa == 'Next')? index+1 : index-1;
            return self.health_insu_questions().hasOwnProperty(questionIndex);
        };

        self.isOptionSelected = function(questionIndex){
            return self.health_insu_questions()[questionIndex].selected();
        };


        self.questionNumberToShow = ko.observable(1);
        self.progressBar = ko.observable(0);
        self.progressStatus = ko.observable(0);
        var i = 1;
        self.questionToShow = ko.computed(function() {
            var questionNumber = self.questionNumberToShow();

            if(questionNumber > i) i = questionNumber;      
            self.progressStatus(i);
            self.progressBar(10*i);
            return ko.utils.arrayFilter(self.health_insu_questions(), function(question) {
                return question.index == questionNumber;
            });
        });
        /*self.progressBar = ko.computed(function(){
          console.log("Progress ", (i-1)*10);
          return (i-1)*10;
        });*/

        // Animation callbacks for the planets list
        this.showPlanetElement = function(elem) {
            if (elem.nodeType === 1){
                $(elem).hide().slideDown();
            }
        };

        this.hidePlanetElement = function(elem){
            if (elem.nodeType === 1){
                $(elem).slideUp(function(){ $(elem).remove(); });
            };
        }
    };

    ko.applyBindings(new health_insurance(health_insu_questions));




    // Speedo MeterGraph
    var width = 360,height = 230,radius = 150, translateX = 180, translateY = 205, barWidth = 60, NeedleChartObject, NeedleObject;
    function speedometer(percentage){

      var Needle,chart, arc, arcEndRad, arcStartRad, chartInset, degToRad, el, endPadRad, margin,  numSections, padRad, percToDeg, percToRad, percent, sectionIndx, sectionPerc, startPadRad, svg, totalPercent, _i, chartColor;

      percent = (percentage == 100)? 0.9999 : percentage/100;
      numSections = 3;
      sectionPerc = [.25,.15,.10];
      chartColor = ['danger','improve','safe']; // chart colors
      padRad = 0.01;
      chartInset = 0;
      totalPercent = .75;
      $('#SpeedoMeterGraph').html('');

      el = d3.select('#SpeedoMeterGraph');

      margin = {
        top: 180,
        right: 0,
        bottom: 0,
        left: 0
      };

      percToDeg = function(perc) {
        return perc * 360;
      };

      percToRad = function(perc) {
        return degToRad(percToDeg(perc));
      };

      degToRad = function(deg) {
        return deg * Math.PI / 180;
      };

      svg = el.append('svg').attr('width', width + margin.left + margin.right).attr('height', height);

      // chart = svg.append('g').attr('transform', "translate(" + ((width / 2) + margin.left) + ", " + margin.top + ")");
      chart = svg.append('g').attr('transform', "translate("+translateX+","+translateY+")");

      for (sectionIndx = _i = 1; 1 <= numSections ? _i <= numSections : _i >= numSections; sectionIndx = 1 <= numSections ? ++_i : --_i) {

        arcStartRad = percToRad(totalPercent);
        arcEndRad = arcStartRad + percToRad(sectionPerc[_i-1]);
        totalPercent += sectionPerc[_i-1];
        startPadRad = sectionIndx === 0 ? 0 : padRad / 2;
        endPadRad = sectionIndx === numSections ? 0 : padRad / 2;

        arc = d3.svg.arc().outerRadius(radius - chartInset).innerRadius(radius - chartInset - barWidth).startAngle(arcStartRad + startPadRad).endAngle(arcEndRad - endPadRad);
        chart.append('path').attr('class', "arc chart-color " + chartColor[_i-1]).attr('d', arc);
      }

      Needle = (function() {
        function Needle(len, radius) {
          this.len = len;
          this.radius = radius;
        }

        Needle.prototype.drawOn = function(el, perc) {
          el.append('circle').attr('class', 'needle-center').attr('cx', 0).attr('cy', 0).attr('r', 0);
          el.append('path').attr('class', 'needle').attr('d', this.mkCmd(perc));
          return el.append("g").attr("transform", function(d) { return "translate(503.30034380936354,137.03177417919602)"; });
          //return el;
        };

        Needle.prototype.animateOn = function(el, perc) {
          var self;
          self = this;                                        // 3000
          return el.transition().delay(500).ease('elastic').duration(0).selectAll('.needle').tween('progress', function() {
            return function(percentOfPercent) {
              var progress;
              progress = percentOfPercent * perc;
              return d3.select(this).attr('d', self.mkCmd(progress));
            };
          });
        };

        Needle.prototype.mkCmd = function(perc) {
          var centerX, centerY, leftX, leftY, rightX, rightY, thetaRad, topX, topY;
          thetaRad = percToRad(perc / 2);
          centerX = 0;
          centerY = 0;
          topX = centerX - this.len * Math.cos(thetaRad);
          topY = centerY - this.len * Math.sin(thetaRad);
          leftX = centerX - this.radius * Math.cos(thetaRad - Math.PI / 5);
          leftY = centerY - this.radius * Math.sin(thetaRad - Math.PI / 5);
          rightX = centerX - this.radius * Math.cos(thetaRad + Math.PI / 5);
          rightY = centerY - this.radius * Math.sin(thetaRad + Math.PI / 5);
          return "M " + leftX + " " + leftY + " L " + topX + " " + topY + " L " + rightX + " " + rightY;

        };

        return Needle;

      })();

      return { Needle:Needle, chart:chart };

    };


    // Handling Responsive
    function responsiveHandling(){
      var windowWidth = $(this).width();

      if(windowWidth <= 767)
        width = 140,height = 110,radius = 50, translateX = 50, translateY = 100, barWidth = 13;
      if(windowWidth <= 1185)
        width = 210,height = 110,radius = 80, translateX = 80, translateY = 100, barWidth = 13;
      else
        width = 350,height = 208,radius = 150, translateX = 180, translateY = 205, barWidth = 60;

      percentage = localStorage.getItem('percentage');
      percentage = (percentage)? percentage : 100;

      NeedleChartObject = speedometer(percentage);
      NeedleObject = new NeedleChartObject.Needle(200, 3);
      NeedleObject.drawOn(NeedleChartObject.chart,  percentage/100);
    }

    // Clear LocalStorage OnLoad
    localStorage.removeItem('percentage');
    responsiveHandling();

    // Window Resize
    $(window).resize(function(){
      responsiveHandling();
    });

});

