
var custom_utils = function(){ };

custom_utils.createSelect = function(element, data){
    var placeHolder = $(element).attr('data-placeholder');
    $(element).html(custom_utils.createOption(data,placeHolder));
    var configParamsObj = {
        minimumResultsForSearch: 1,
    }
    $(element).select2(configParamsObj);
    $(element).parent().find('.select2-choice').css({
        'padding-top': '13px!important'
    })
};


// Create Option string for select
custom_utils.createOption = function(dataset,defaultValue){
    var optionStr = "<option></option>";
    $.each(dataset, function(key, value){
        var id = value.id? value.id:key;
        var text = value.name? value.name:value;
        optionStr += "<option id="+ id + " name="+ text+ " value="+ JSON.stringify(value).trim() + ">"+ text + "</option>"
    });
    return optionStr;
}


$(function(){
    var network_hospitals_form = $("form[name='network-hospitals']");

    $("select").select2();  // select-compononet

    get_details().done(function(response){
        custom_utils.createSelect($("select[name='state']"),response.states);
    });


    $("select[name='state']").on('change', function(){
        var state = $(this).val();

        $("select").not("select[name='state']").parent().removeClass("selected");
        $("select").not("select[name='state']").parent().find(".on-select-label").addClass("hide");

        network_hospitals_form.find(":input[name='city']").val('');
        network_hospitals_form.find(":input[name='state']").val(state);

        $("select[name='cities']").html();
        get_details().done(function(response){
            custom_utils.createSelect($("select[name='cities']"),response.cities);
        });

    });

    $(".select-ip").on("change", function(e){
        e.preventDefault();
        var select = $(this);
        $("#hospitals-pagination").empty();

        if(select.val()){
          $(this).parent().addClass('selected');

          $(this).parent().find('.select2-choice').css({
              'padding-top': ''
          });

          $(this).parent().find(".on-select-label").removeClass("hide");

        }else{
          $(this).select2("data","");
          $(this).parent().removeClass('selected');
          $(this).parent().find(".on-select-label").addClass("hide");
        }
    });

    $("select[name='cities']").on('change', function(){

        var city = $(this).val();
        var hospital_template = $(".hospital-template");
        var hospitals='';

        network_hospitals_form.find(":input[name='city']").val(city);

        get_details().done(function(response){
            hospital_template.parent().removeClass("hide");
            $.each(response.hospitals, function(key,val){
                hospital_template.find(".index").html(key+1);
                hospital_template.find("h3").html(val.name);
                hospital_template.find(".address").html(val.address);
                hospital_template.find(".location").html(response.city+", "+response.state);
                hospital_template.find(".phone span").html(val.phone);
                hospitals+= hospital_template.html();
            });

            var total_hospitals = response.hospitals.length;
            $("#hospitals").parent().find("h4").show().html(total_hospitals+' Hospitals');
            $("#hospitals").html(hospitals);

            // call to pagination
            if(response.hospitals.length>8)
                updateItems();
        });

    });


    function get_details(){

        return $.post(
            '/health-insurance/network-hospitals/',
            network_hospitals_form.serializeArray()
        );
    }

    // Pagination Code
    var perPage = 8;
    // now setup pagination
    var $pagination = $("#hospitals-pagination");

    $pagination.pagination({
        itemsOnPage: perPage,
        cssStyle: "light-theme",
        onPageClick: function(pageNumber) { // this is where the magic happens
            // someone changed page, lets hide/show trs appropriately
            var showFrom = perPage * (pageNumber - 1);
            var showTo = showFrom + perPage;
            items.hide() // first hide everything, then show for the new page
                 .slice(showFrom, showTo).show();
        }
    });
    // we need an updateItems function that we'll call both initially and
    // whenever we add or remove any items. You'll need to ensure that this
    // function is called whenever your items dynamically change
    function updateItems() {
        items = $("#hospitals .static-item");
        // we'll update the number of items that the pagination element expects
        // using a method from the simplePagination documentation: updateItems
        $pagination.pagination("updateItems", items.length);

        // Select 1st page always.
        $pagination.pagination("selectPage", 1);
    }
});
