var next_page_url = '/motor/car-insurance/';
if(!$.isEmptyObject(next_page_override)){
    next_page_url = next_page_override;
}

function ClickToCall(label){
    window.dataLayer.push({
        'event': 'GAEvent',
        'hitType': 'event',
        'eventCategory': 'MOTOR',
        'eventAction': 'click_to_call',
        'eventLabel': page_id + ' Label_' + label,
        'eventValue': 0
    });
    window.location = 'tel:' + toll_free_no;
    return false;
}

function CrossOver(label){
    window.dataLayer.push({
        'event': 'GAEvent',
        'hitType': 'event',
        'eventCategory': 'MOTOR',
        'eventAction': 'crossover',
        'eventLabel': page_id + ' Label_' + label,
        'eventValue': 0
    });
    window.location.href = '/lp/bike-insurance/buy/';
    return false;
}

function skipForm(label){
    window.dataLayer.push({
        'event': 'GAEvent',
        'hitType': 'event',
        'eventCategory': 'MOTOR',
        'eventAction': 'hero_skip_'+ label,
        'eventLabel': page_id,
        'eventValue': 0
    });
    window.location = next_page_url;
    return false;
}

