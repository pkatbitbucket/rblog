npm install
current=`pwd`
echo "-------------------Building DesktopV2-------------------------"
cd $current/desktopv2
ln -sf ../node_modules
../node_modules/grunt-cli/bin/grunt

echo "-------------------Building DesktopV2_steps-------------------------"
cd $current/desktopv2_steps
ln -sf ../node_modules
../node_modules/grunt-cli/bin/grunt

echo "-------------------Building MobileV2-------------------------"
cd $current/mobilev2
ln -sf ../node_modules
../node_modules/grunt-cli/bin/grunt

echo "-------------------Building MobileV2_steps-------------------------"
cd $current/mobilev2_steps
ln -sf ../node_modules
../node_modules/grunt-cli/bin/grunt

echo "-------------------Building Landing Pages-------------------------"
cd $current/js
ln -sf ../node_modules
../node_modules/grunt-cli/bin/grunt
