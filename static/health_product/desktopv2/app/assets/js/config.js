'use strict';
require.config({
    baseUrl : '/static/health_product/desktopv2/app',
    paths: {
        'text': '/static/health_product/desktopv2/app/lib/require/text',
        'knockout': '/static/health_product/desktopv2/app/lib/knockout/knockout-3.3.0',
        'knockout_validation': '/static/health_product/desktopv2/app/lib/knockout/knockout.validation',
        'knockout-amd-helpers': '/static/health_product/desktopv2/app/lib/knockout/knockout-amd-helpers',
        'knockout-history': '/static/health_product/desktopv2/app/lib/knockout/knockout-history',
        'knockout-router': '/static/health_product/desktopv2/app/lib/knockout/knockout-router',
        'models': '/static/health_product/desktopv2/app/assets/js/models/global/models',
        'login': '/static/health_product/desktopv2/app/assets/js/models/global/login',
        'contact': '/static/health_product/desktopv2/app/assets/js/models/global/contact',
        'utils': '/static/health_product/desktopv2/app/assets/js/models/global/utils',
        'svg': '/static/health_product/desktopv2/app/lib/svg/svg',
        'bsvg' : '/static/health_product/desktopv2/app/assets/js/scripts/bsvg',
        'step1': '/static/health_product/desktopv2/app/assets/js/models/steps/step1',
        'step2': '/static/health_product/desktopv2/app/assets/js/models/steps/step2',
        'step3': '/static/health_product/desktopv2/app/assets/js/models/steps/step3',
        'expert_card':'/static/health_product/desktopv2/app/assets/js/models/steps/expert_card',
        'results' : '/static/health_product/desktopv2/app/assets/js/models/steps/results',
        'offline': '/static/health_product/desktopv2/app/assets/js/models/steps/offline',
        'scheduler': '/static/health_product/desktopv2/app/assets/js/models/steps/scheduler',
        'cookie' : '/static/health_product/desktopv2/app/lib/cookies/cookies',
        'bstore' : '/static/health_product/desktopv2/app/assets/js/scripts/bstore',
        'custom' : '/static/health_product/desktopv2/app/assets/js/scripts/custom',
        'pincheck' : '/static/health_product/desktopv2/app/assets/js/scripts/pincheck',
        'jquery' : '/static/health_product/desktopv2/app/lib/jquery/cf-jquery',
        'response' : '/static/health_product/desktopv2/app/lib/zlib/inflate_stream',
        'main': '/static/health_product/desktopv2/app/assets/js/main',
    },
    shim: {
        'knockout_validation' : {
            deps : ['knockout']
        },
        'knockout-amd-helpers': {
            deps: ['knockout']
        },
        'knockout-history': {
            deps: ['knockout']
        },
        'knockout-router': {
            deps: ['knockout', 'knockout-history', 'knockout-amd-helpers']
        },

    },
    urlArgs: 'v=1.0.0.0'
});

define(['main'], function(main) {
    main.run();
});
