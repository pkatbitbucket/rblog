define(['knockout', 'jquery', 'cookie', 'utils', 'knockout_validation'], function(ko, $, Cookie, utils) {

    function Login() {

        var self = this;
        self.loginMode = ko.observable(true);
        self.tabMode = ko.observable('REGISTRATION'); //LOGIN, REGISTRATION
        self.ajaxErrors = ko.observable({});
        self.username = ko.observable(null);
        self.profile_image = ko.observable(null);
        self.context_menu = ko.observable(false);
        if(Cookie.get('source')) {
            self.referer = Cookie.get('source');
        } else {
            self.referer = 'default';
        }
        if(Cookie.get('campaign')) {
            self.campaign = Cookie.get('campaign');
        } else {
            self.campaign = 'default';
        }

        utils.hide_chat_box();
        self.loginMode.subscribe(function(val){
            if(val == false){
                utils.show_chat_box();
                utils.event_log('Results', 'Viewed', 2);
                utils.gtm_event_log('results_visited');
                utils.gtm_event_log(self.referer + '_results_visited');
                utils.gtm_event_log(self.campaign + '_results_visited');
                utils.page_view('/vp/health/initiate-results/', 'VP-HEALTH-INITIATE-RESULTS');
            }
        });
        self.toggleContextMenu = function(){
            if(self.context_menu()){
                self.context_menu(false);
            } else {
                self.context_menu(true);
            }
        };
        self.login_email = ko.observable().extend({
            email: true,
            required: {
                params:true,
                message: "Please enter valid email"
            }
        });
        self.login_password = ko.observable().extend({
            required: {
                params:true,
                message: "password is required"
            }
        });
        self.registration_name = ko.observable().extend({
            required: {
                params:true,
                message: "Name is required"
            }
        });
        self.registration_email = ko.observable().extend({
            email: true,
            required: {
                params:true,
                message: "Please enter valid email"
            }
        });
        self.registrationErrors = ko.validation.group([self.registration_email, self.registration_name]);
        self.loginErrors = ko.validation.group([self.login_email, self.login_password]);

        var olark = olark || undefined;
        var update_profile = function() {
            var cprofile = Cookie.get('profile');
            if(cprofile) {
                var uObj = undefined;
                try {
                    uObj = JSON.parse(cprofile);
                } catch (err){
                    cprofile = cprofile.replace(/"/,'').replace(/"$/, '');
                    uObj = JSON.parse(cprofile);
                }
                if(uObj.name) {
                    self.username(uObj.name);
                } else {
                    self.username('Logged in');
                }
                if(uObj.photo){
                    self.profile_image(uObj.photo);
                }
                if(window.olark) {
                    if (uObj.email){
                        window.olark('api.visitor.updateEmailAddress', {emailAddress: uObj.email});
                    }
                    if(uObj.name){
                        window.olark('api.visitor.updateFullName', {fullName: uObj.name});
                    }
                }
                self.loginMode(false);
                utils.event_log('LoginPageLogged', 'v1', 1);
            }
        };
        update_profile();

        self.unSetLoginMode = function(){
            self.loginMode(false);
            Cookie.set('hide_login','1');
        };
        self.hidepopup = function(data, event){
            if(event.keyCode === 27)
            self.loginMode(false);
        };

        self.setTabMode = function(mode){
            self.tabMode(mode);
        };

        self.google_btn_click = function(){
            gplus_login();
        };

        self.skip_login_click = function(){
            on_success('coverfox', null);
            return false;
        };

        self.facebook_btn_click = function(){
            fb_login();
        };

        self.login_user = function(){
            $.post(PRODUCT_USER_LOGIN, {'csrfmiddlewaretoken': CSRF_TOKEN, 'email': self.login_email(), 'password': self.login_password()}, function (data) {
                if(data.success){
                    if(data.response.success){
                        var msg = data.response.mesg;
                        on_success('coverfox', msg.email, msg.new_user);
                    } else {
                        self.registrationErrors.showAllMessages(false);
                        var edata = {};
                        if(data.response.error_msg){
                            edata['password'] = data.response.error_msg;
                        } else {
                            for(var k in data.response.errors){
                                edata[k] = data.response.errors[k][0];
                            }
                        }
                        self.ajaxErrors({'login':edata});
                    }
                } else {
                    self.registrationErrors.showAllMessages(false);
                    var edata = {};
                    for(var k in data.errors){
                        edata[k] = data.errors[k][0];
                    }
                    self.ajaxErrors({'login':edata});
                }
            }, 'json');
            event.preventDefault();
        };

        self.register_user = function(){
            console.log("REGISTRATION ERRORS", self.registrationErrors());
            if(!self.registrationErrors().length == 0) {
                self.registrationErrors.showAllMessages();
                return false;
            }
            $.post(USER_REGISTRATION_URL, {'csrfmiddlewaretoken': CSRF_TOKEN, 'name': self.registration_name(), 'email': self.registration_email()}, function (data) {
                if(data.success){
                    if(data.response.success){
                        var msg = data.response.mesg;
                        on_success('coverfox', msg.email, true);
                    }
                } else {
                    self.registrationErrors.showAllMessages(false);
                    var edata = {};
                    for(var k in data.errors){
                        edata[k] = data.errors[k][0];
                    }
                    self.ajaxErrors({'registration':edata});
                    console.log("EDATA", edata);
                }
            }, 'json');
            event.preventDefault();
        };

        var on_success = function(auth_source, email_id, new_user) {
            utils.gtm_data_log({'gtm_email': email_id});
            utils.page_view('/vp/health/passed-login-popup/', 'VP-HEALTH-PASSED-LOGIN-POPUP');
            if(new_user){
                //NEWLY REGISTERED USER
                utils.event_log('signup', 'results');
                utils.page_view('/vp/health/signed-up/', 'VP-HEALTH-SIGNED-UP');
                utils.gtm_event_log('gtm_email_signup');
                utils.gtm_event_log(self.campaign + '_email_signup');
            } else {
                //OLD EXISTING USER
                utils.event_log('login', 'results');
                utils.page_view('/vp/health/login/', 'VP-HEALTH-LOGIN');
                utils.gtm_event_log('gtm_email_login');
                utils.gtm_event_log(self.campaign + '_email_signup');
            }
            update_profile();
        };

        self.afterRender = function(){
            (function() {
                var e = document.createElement('script');
                e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
                e.async = true;
                document.getElementById('fb-root').appendChild(e);
            }());

            (function() {
                var po = document.createElement('script');
                po.type = 'text/javascript'; po.async = true;
                po.src = 'https://apis.google.com/js/client:plusone.js?onload=onLoadCallback';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(po, s);
            })();
        };

        // -------------- Facebook Signin ------------------
        var fb_response_callback = function(response, fb_access_token) {
            var data = {};
            data['fb_data'] = JSON.stringify(response);
            data['csrfmiddlewaretoken'] = CSRF_TOKEN;
            data['facebook_access_token'] = fb_access_token;
            $.post(FACEBOOK_REGISTRATION_URL, data, function(res_data){
                var data = JSON.parse(res_data);
                if (data.success) {
                    on_success('facebook', data.email, data.new_user);
                }else {
                    on_success('facebook', null);
                }
            });
        };

        var google_response_callback = function(obj){
            var data = {};
            data['gdata'] = JSON.stringify(obj);
            data['csrfmiddlewaretoken'] = CSRF_TOKEN;
            data['google_code'] = google_auth_result['code'];
            data['google_access_token'] = google_auth_result['access_token'];
            $.post(GOOGLE_REGISTRATION_URL, data, function(res_data){
                var data = JSON.parse(res_data);
                if (data.success) {
                    on_success('google', data.email, data.new_user);
                }else {
                    on_success('google', null);
                }
            });
        };

        var google_response_error = function(){
            on_success('google', null);
        };

        window.fbAsyncInit = function() {
            FB.init({
                appId   : '545910808767230',
                oauth   : true,
                status  : true, // check login status
                cookie  : true, // enable cookies to allow the server to access the session
                xfbml   : true // parse XFBML
            });
        };

        var fb_login = function(){
            FB.login(function(response) {
                if (response.authResponse) {
                    var fb_access_token = response.authResponse.accessToken;
                    user_id = response.authResponse.userID; //get FB UID

                    FB.api('/me', function(response) {
                        fb_response_callback(response, fb_access_token);
                    });
                } else {
                    //user hit cancel button
                }
            }, {
                scope: 'user_birthday,user_relationships,email'
            })
        };

        // -------------- Google Signin ------------------

        function onLoadCallback() {
            gapi.client.setApiKey(GOOGLE_API_KEY); //set your API KEY
        }

        gplus_login = function() {
            var myParams = {
                'callback': loginFinishedCallback,
                'clientid': GOOGLE_OAUTH_KEY,
                'cookiepolicy': 'single_host_origin',
                'state' : CSRF_TOKEN,
                'application_name' : 'coverfox',
                'scope' : 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email'
            };
            gapi.auth.signIn(myParams);
        };

        var google_auth_result;
        function loginFinishedCallback(authResult) {
            if (authResult) {
                if(authResult['status']['signed_in']) {
                    if (authResult['error'] == undefined){
                        google_auth_result = authResult;
                        gapi.client.load('plus','v1', loadProfile);  // Trigger request to get the email address.
                    } else {
                        google_response_error();
                    }
                }
            } else {
                return;
            }
        }

        function loadProfile(){
            var request = gapi.client.plus.people.get({'userId': 'me'});
            request.execute(google_response_callback);
        }
    }

    var loginInstance = new Login();
    return loginInstance;
});
