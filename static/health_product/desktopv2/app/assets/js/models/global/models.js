define(['knockout', 'jquery', 'step2', 'utils', 'response', 'offline', 'cookie'], function (ko, $, step2, utils, response, offline, Cookies) {

    var shortlist = ko.observableArray([]);
    var resultTabs = ko.observableArray([]);
    var shortlistTab = ko.observableArray([]);
    var currentTab = ko.observable();
    var brand_id = ko.observable(Cookies.get('brand_id'));

    shortlist.subscribe(function(){
        var cdetails = [];
        var tplans = [];
        ko.utils.arrayForEach(shortlist(), function(plan){
            tplans.push("Members: " + utils.member_verbose(plan.uidata.members, true));
            tplans.push(plan.insurer_title + " - " + plan.product_title + " | " + plan.sum_assured + " | " + plan.premium);
        });
        cdetails.push("Shortlist Plans: ", tplans.join('\n'));
        utils.notify(cdetails.join('\n'));

    });
    currentTab.subscribe(function(ntab){
        if(['', null, undefined].indexOf(ntab) == -1){
            utils.event_log('CurrentTab', ntab.type, 0);
            if(ntab.uidata().members) {
                var cdetails = [];
                cdetails.push("Tab: " + ntab.type);
                cdetails.push("Members: " + utils.member_verbose(ntab.uidata().members, true));
                var tplans = [];
                var i=0;
                ko.utils.arrayForEach(ntab.plans(), function(plan){
                    if(i < 5) {
                        tplans.push(plan.insurer_title + " - " + plan.product_title + " | " + plan.sum_assured + " | " + plan.premium);
                    }
                    i++;
                });
                cdetails.push("Top Plans: ", tplans.join('\n'));
                utils.notify(cdetails.join('\n'));
            }
        }
    });
    var filters = ko.computed(function() {
        var filters = [
        {
            "name" : 'room_rent',
            "title" : "Room Rent Limit",
            "descTitle" : "What is Room Rent Limit?",
            "desc" : "Room Rent Limit is a Limit applied by Insurance Cos on the the choice of Hospital Room you can opt for. The limit could be on the Room Charges or on the Room Category. The lesser than cappings the better.",
            "type":"radio",
            "values" : [{'title':'No Limit','value':'NO_LIMIT'},{'title':'At least private room','value':'PRIVATE'}],
        },
        {
            "name" : 'copay',
            "title": "Copay",
            "descTitle":"",
            "desc": "",
            "type":"checkbox",
            "values": [{'title':'No Copay','value':'NO_COPAY','descTitle':'What is Copay?','desc':'Copay is your share (in %) on the final payable claim amount, that you have to bear. Insurance companies will deduct this share and then settle the claim. Lower the copay the better.'},]
        },
        {
            "name" : 'ped',
            "title": "Pre-Existing Diseases",
            "descTitle":"What are Pre-Existing Diseases?",
            "desc": "Pre-Existing Diseases would include various health conditions including ones diagnosed by a doctor, any major hospitalization in the past, ailments for which one is taking medication. Such ailments are covered after certain time duration. The lower the waiting periods, the better.",
            "type":"radio",
            "values": [{'title':'Covered after 2 years','value':'24'}, {'title':'Covered after 3 years','value':'36'}, {'title':'Covered after 4 years','value':'48'}],
        }];
        if(!step2.is_topup()) {
            filters.push({
                "name" : 'plan_benefits',
                "title": "Plan Benefits",
                "descTitle":"",
                "desc": "",
                "type":"checkbox",
                "values": [{
                    'title':'Restore Benefits',
                    'name':'restore_benefits',
                    'value':'available',
                    'descTitle':'What is Restore Benefit?',
                    'desc':'Some plans reinstate your coverage in case you exhaust your cover in a policy year.'
                }, {
                    'title':'Critical Illness',
                    'name':'critical_illness_coverage',
                    'value':'available',
                    'descTitle':'What is Critical Illness?',
                    'desc':'Critical illnesses like Cancer / Stroke cost more. So some plans provide an additional lumpsum in case of diagnosis of a critical illness. However, you get higher and cost effective coverage, if you buy a specific critical illness or top-up plans, available separately.'
                }, {
                    'title':'Maternity Cover',
                    'name':'maternity_cover',
                    'value':'available',
                    'descTitle':'What is Maternity Cover?',
                    'desc':'Medical Treatment Expenses traceable to childbirth (including caesarean sections). It also includes expenses towards lawful medical termination of pregnancy.'
                }],
            });
        }
        return filters;
    });
    var SidebarFilter = function (data) {
        var self = this;
        self.name = data.name;
        self.title = data.title;
        self.desc = data.desc;
        self.descTitle = data.descTitle;
        self.values = data.values;
        self.type = data.type;
        self.selectedValues = ko.observableArray([]);
        self.selectValue = function(data){
            utils.event_log('FilterUsed', self.title, 0);
            //var data = data[0];
            if(self.type=='radio'){
                if(self.selectedValues().length === 0){
                    self.selectedValues.push(data);
                } else if(self.selectedValues()[0] == data){
                    self.selectedValues([]);
                } else {
                    self.selectedValues([]);
                    self.selectedValues.push(data);
                }
            }
            else{
                if(self.selectedValues.indexOf(data) > -1)
                    self.selectedValues.remove(data);
                else
                    self.selectedValues.push(data);
            }
        };
        self.deselectAll = function () {
            self.selectedValues([]);
        };
        self.getFilterName = function(data){
            if(self.type=="radio"){
                return self.title;
            } else {
                if(data.title=='No Copay'){ return 'Copay';}
                else{ return data.title; }
            }
        };
        self.getFilterValue = function(data){
            if(self.type == "radio") {
                return data.title;
            } else {
                if(data.value=='NO_COPAY'){ return 'No Copay'; }
                else{ return data.value; }
            }
        };
        self.getCSS = function(){
            return (self.type=='radio') ? 'cf-radio' : 'selected';
        };
    };
    var Hospital = function (data) {
        var self = this;
        self.address = data.address;
        self.id = data.id;
        self.lat = data.lat;
        self.lng = data.lng;
        self.name = data.name;
        self.insurers = data.insurers;
    };
    var pincode_hospital_map = ko.observableArray();
    var get_hospitals_for_tab = function (tabs) {
        var pdata = {};
        pdata.pincodes = ko.utils.arrayMap(tabs, function (tab) {
            return tab.uidata().pincode;
        });
        var json_promise = $.post('/health-plan/hospitals/by-pincode/', {
            'csrfmiddlewaretoken': CSRF_TOKEN,
            'data': JSON.stringify(pdata),
            'activity_id': utils.ACTIVITY_ID,
        }, function (res) {
            var parsedResponse = response.parseResponse(res);
            var pin_ho_map = [];
            var push_hosp = function(hosp) {
                var h = new Hospital(hosp);
                pin_ho_map.push({
                    'pincode': pincode,
                    'hospital': h
                });
            };
            for (var pincode in parsedResponse) {
                ko.utils.arrayForEach(parsedResponse[pincode].hospitals, push_hosp);
            }
            pincode_hospital_map(pin_ho_map);
        });
    };
    var Plan = function (data, uidata) {
        var self = this;
        self.hospital_list_updated = "Hospital list updated on MAR-16";
        self.uidata = uidata;
        self.total_score = data.total_score;
        self.slab_id = data.slab_id;
        self.product_id = data.product_id;
        self.insurer_id = data.insurer_id;
        self.max_age = data.max_age;
        self.five_year_projection = data.five_year_projection;
        self.product_title = data.product_title;
        self.insurer_title = data.insurer_title;
        self.transaction_permission = data.transaction_permission;
        self.sum_assured = parseInt(data.sum_assured);
        self.deductible = parseInt(data.deductible);
        self.special_feature = data.special_feature.split('\n');
        self.premium = parseFloat(data.premium);
        self.cpt_id = data.cpt_id;
        self.policy_wordings = data.policy_wordings;
        self.attr_list = data.score;
        self.attr = {};
        self.general = data.general;
        self.upgraded_sum_assured = ko.computed(function(){
            return self.sum_assured + self.deductible;
        });
        var med_summary = {};
        for (var i = 0; i < data.general.conditions.length; i++) {
            var cond = data.general.conditions[i];
            if (!(cond.period in med_summary)) {
                med_summary[cond.period] = [];
            }
            med_summary[cond.period].push(cond);
        }
        self.medical_summary = [];
        for (var period in med_summary) {
            var conds = med_summary[period];
            self.medical_summary.push({'period': period, 'conditions': conds});
        }
        self.periodToVerbose = function (period) {
            var verbose = 'Covered after ' + (period / 12).toFixed(2) + ' years';
            if (period == 1) {
                verbose = "Covered after 30 days";
            }
            if (period == 12) {
                verbose = "Covered after 1 year";
            }
            return verbose;
        };

        self.never_covered = ['Cosmetic Surgery', 'Lasik eye treatments', 'Drugs or alcohol abuse', 'Genetic Disorders', 'Self inflicted injuries', 'War related injuries'];
        self.ped = data.ped;
        self.standard_exclusions = data.standard_exclusions;
        self.recommended = data.recommended;
        if (self.five_year_projection.length != 5) {
            self.sum_assured_after_5_years = null;
            self.total_premium_paid_in_5_years = null;
        } else {
            self.sum_assured_after_5_years = self.five_year_projection[4][2];
            self.total_premium_paid_in_5_years = 0;
            ko.utils.arrayForEach(self.five_year_projection, function (i, d) {
                self.total_premium_paid_in_5_years += d[1];
            });
        }
        self.five_year_cover = self.five_year_projection.map(function (item) {
            return item[2];
        });
        self.five_year_premium = self.five_year_projection.map(function (item) {
            return item[1];
        });
        ko.utils.arrayForEach(data.score, function (listd) {
            self.attr[listd[0]] = listd[1];
        });
        self.score = parseInt(data.total_score);
        self.medical_score = parseInt(data.medical_score);
        self.visible = ko.observable(true);

        self.get_hospitals_for_plan = function () {
            var hospitals = ko.utils.arrayFilter(pincode_hospital_map(), function (ele) {
                return ele.pincode == self.uidata.pincode;
            });
            var hlist = ko.utils.arrayMap(hospitals, function (hosp) {
                return {
                    'hospital': hosp.hospital,
                    'available': hosp.hospital.insurers.indexOf(self.insurer_id)
                };
            });
            return {
                'count': ko.utils.arrayFilter(hlist, function (h) {
                    return h.available > -1;
                }).length,
                'list': hlist,
                'available_list': ko.utils.arrayFilter(hlist, function (h) {
                    return h.available > -1;
                })
            };
        };
        self.attr_list_high = ko.utils.arrayFilter(self.attr_list, function (item) {
            return item[1].importance == 'HIGH';
        });
        self.attr_list_mid = ko.utils.arrayFilter(self.attr_list, function (item) {
            return item[1].importance == 'MID';
        });
        self.attr_list_low = ko.utils.arrayFilter(self.attr_list, function (item) {
            return item[1].importance == 'LOW';
        });
        self.getAttr = function (plan, attr_name) {
            return self.attr[attr_name];
        };
        self.current_plan_tab = ko.observable('plan-benefits');

        self.comparePlan = function(p){
            if((self.slab_id == p.slab_id) && utils.deepCompare(self.uidata, p.uidata)){
                return true;
            } else {
                return false;
            }
        };

        self.planInShortlist = ko.computed(function(){
            if(shortlist().length === 0){
                return false;
            }
            var cmp = false;
            for(var i=0;i<shortlist().length;i++){
                cmp = self.comparePlan(shortlist()[i]);
                if(cmp){
                    return cmp;
                }
            }
            return cmp;
        });
    };

    var update_selection = function () {
        var data = ko.utils.arrayMap(shortlist(), function(plan){
            return {
                'slab_id': plan.slab_id,
                'uidata' : plan.uidata,
            };
        });
        var request = $.post('/health-plan/save-details/shortlist/?hot=true', {
            'csrfmiddlewaretoken': CSRF_TOKEN,
            'data': JSON.stringify({
                'shortlist': data
            })
        }, function (res) {
        });
    };


    var ResultTab = function (data, render) {
        var self = this;
        self.render = typeof render !== 'undefined' ? render : true;//differentiate it from Shortlisted plans
        //base observables
        self.uidata = ko.observable(data.uidata);
        self.plans = ko.observableArray(
            ko.utils.arrayMap(data.plans, function (item) {
                var p = new Plan(item,data.uidata);
                return p;
            })
        );
        self.online_plans = ko.observableArray();
        self.offline_plans = ko.observableArray();
        self.plans.subscribe(function(plans) {
            var online_plans = [];
            var offline_plans = [];
            ko.utils.arrayForEach(plans, function(plan) {
                if(plan.transaction_permission == 'ONLINE')
                    online_plans.push(plan);
                else
                    offline_plans.push(plan);
            });
            self.online_plans(online_plans);
            self.offline_plans(offline_plans);
        });
        self.plans.valueHasMutated();

        self.offline_plans_visible = ko.observable(false);
        self.show_offline_plans = function() {
            self.offline_plans_visible(true);
        };
        self.offline_insurers = ko.computed(function() {
            var offline_insurer_ids = [];
            var offline_insurers = [];
            ko.utils.arrayForEach(self.offline_plans(), function(plan) {
                if(offline_insurer_ids.indexOf(plan.insurer_id) == -1)
                {
                    offline_insurer_ids.push(plan.insurer_id);
                    offline_insurers.push({insurer_id: plan.insurer_id, insurer_title: plan.insurer_title});
                }
            });
            return offline_insurers;
        });

        self.visiblePlans = ko.computed(function () {
            return ko.utils.arrayFilter(self.online_plans(), function (item) {
                return item.visible();
            });
        });
        //SORT
        self.availableSorts = ['Recommended', 'Premium (high-low)', 'Premium (low-high)'];
        self.selectedSort = ko.observable('Recommended');
        self.sortOption=function(selectedVal){
            self.sortOptionsVisible(!self.sortOptionsVisible());
            self.selectedSort(selectedVal);
        };
        self.sortOptionsVisible = ko.observable(false);
        self.toggleSortOptions = function () {
            self.sortOptionsVisible(!self.sortOptionsVisible());
        };

        var sort_subscription = self.selectedSort.subscribe(function(newVal){
            switch(newVal){
                case 'Recommended':
                    self.plans.sort(function(p1,p2){ return (parseInt(p2.total_score) - parseInt(p1.total_score));});
                    break;
                case 'Premium (high-low)':
                    self.plans.sort(function(p1,p2){ return (parseInt(p2.premium) - parseInt(p1.premium));});
                    break;
                case 'Premium (low-high)':
                    self.plans.sort(function(p1,p2){ return (parseInt(p1.premium) - parseInt(p2.premium));});
                    break;
            }
        });
        self.sum_assured = ko.observable(data.uidata.sum_assured);
        self.deductible = ko.observable(data.uidata.deductible);
        self.selectedPlan = ko.observable(null);
        self.plansToCompare = ko.observableArray([]);
        self.removeFromCompare = function(plan){
            self.plansToCompare.remove(function(p){
                return p == plan;
            });
        };
        //other dependent or independent variables
        self.type = data.type || '';

        self.filter = {};
        self.filters_list = ko.computed(function() {
            return ko.utils.arrayMap(filters(), function (item) {
                var fltr = new SidebarFilter(item);
                self.filter[fltr.name] = fltr.selectedValues;
                return fltr;
            });
        });
        //Premium Related
        self.filter.maxPremium = ko.observable();
        self.premiumOptionsVisible = ko.observable(false);
        self.togglePremiumOptions = function () {
            self.premiumOptionsVisible(!self.premiumOptionsVisible());
        };
        self.setPremium = function (data) {
            self.filter.maxPremium(data[1]);
            self.premiumOptionsVisible(false);
        };
        self.premiumOptions = ko.observableArray([]);
        self.initializePremiumOptions = function () {
            if (self.plans().length === 0) {
                return [];
            }
            var premium_list = ko.utils.arrayMap(self.plans(), function (item) {
                return item.premium;
            });
            var max_premium = parseInt((Math.max.apply(Math, premium_list)) / 1000) + 1;
            var min_premium = parseInt((Math.min.apply(Math, premium_list)) / 1000) + 1;
            var incr_premium = 1;
            if ((max_premium - min_premium) > 10) {
                incr_premium = parseInt((max_premium - min_premium) / 5);
            }
            var popts = [];
            for (var i = min_premium; i < max_premium + incr_premium; i += incr_premium) {
                popts.push(["less than " + i * 1000, i * 1000]);
            }
            self.filter.maxPremium(popts[popts.length - 1][1]);
            self.premiumOptionsVisible(false);
            self.premiumOptions(popts);
        };
        self.initializePremiumOptions();
        self.titleForPremium = ko.computed(function () {
            var m = ko.utils.arrayFirst(self.premiumOptions(), function (item) {
                return item[1] == self.filter.maxPremium();
            });
            if (m) {
                return m[0];
            }
        });
        //Deductible
        self.deductible = step2.deductible;
        self.deductible_options = step2.deductible_options;
        self.upgraded_options = ko.computed(function(){
            upgraded_sum_list = [];
            upgraded_options = [];
            deductible_plans = ko.utils.arrayFilter(self.plans(),function(item){
                return item.deductible == self.deductible();
            });
            ko.utils.arrayForEach(deductible_plans, function(item){
                if(upgraded_sum_list.indexOf(item.upgraded_sum_assured()) == -1){upgraded_sum_list.push(item.upgraded_sum_assured())}
            });
            upgraded_sum_list.sort(function(a,b){return a-b});
            upgraded_options.push({"name": "All", "id": 0});
            for (var i=0; i < upgraded_sum_list.length; i++){
                upgraded_sum = upgraded_sum_list[i]/100000;
                upgraded_sum_details = {"name": upgraded_sum.toString()+" Lacs", "id": upgraded_sum_list[i]};
                upgraded_options.push(upgraded_sum_details)
            }
            return upgraded_options;
        });
        self.upgraded_sum_assured = ko.observable().extend({
            lstore: {key:'upgraded_sum_assured'},
            required: {
                onlyIf:step2.is_topup,
                message: 'Please select your upgraded sum assured'
            }
        });
        if(step2.is_topup() && self.upgraded_options().length > 0){
            self.upgraded_sum_assured(self.upgraded_options()[0]["id"]);
        }
        self.deductibleOptionsVisible = ko.observable(false);
        self.upgradedOptionsVisible = ko.observable(false);
        self.setDeductible = function(data){
            self.deductible(data.id);
            self.deductibleOptionsVisible(false);
        };
        self.setUpgradedSum = function(data){
            self.upgraded_sum_assured(data.id);
            self.upgradedOptionsVisible(false);
        };
        self.toggleDeductibleOptions = function(){
            self.deductibleOptionsVisible(!self.deductibleOptionsVisible());
        };
        self.toggleUpgradedOptions = function(){
            self.upgradedOptionsVisible(!self.upgradedOptionsVisible());
        };
        self.titleForDeductible = ko.computed(function(){
            var m = ko.utils.arrayFirst(self.deductible_options, function(item){
                return item.id == self.deductible();
            });
            if(m) return m.name;
        });
        self.titleForUpgradedSum = ko.computed(function(){
            var m = ko.utils.arrayFirst(self.upgraded_options(), function(item){
                return item.id == self.upgraded_sum_assured();
            });
            if(m) return m.name;
        });
        //Cover Related
        self.coverOptions = [
            ["upto 2 Lacs", 100000],
            ["2 Lacs to 2.5 Lacs", 200000],
            ["3 Lacs to 3.5 Lacs", 300000],
            ["4 Lacs to 4.5 Lacs", 400000],
            ["5 Lacs to 5.5 Lacs", 500000],
            ["6 Lacs to 6.5 Lacs", 600000],
            ["7 Lacs to 7.5 Lacs", 700000],
            ["8 Lacs to 8.5 Lacs", 800000],
            ["9 Lacs to 9.5 Lacs", 900000],
            ["10 Lacs to 15 Lacs", 1000000],
            ["15 Lacs +", 1550000]
        ];
        self.coverOptionsVisible = ko.observable(false);
        self.toggleCoverOptions = function () {
            self.coverOptionsVisible(!self.coverOptionsVisible());
        };
        self.titleForCover = ko.computed(function () {
            var m = ko.utils.arrayFirst(self.coverOptions, function (item) {
                return item[1] == self.sum_assured();
            });
            if (m) {
                return m[0];
            }
        });
        self.setCover = function (data) {
            self.sum_assured(data[1]);
            self.coverOptionsVisible(false);
            self.premiumOptionsVisible(false);
        };
        //Brand related
        self.filter.selectedBrands = ko.observableArray([]);
        self.getAllBrands = ko.computed(function () {
            brandsArray = [];
            for (var i = 0; i < self.plans().length; i++) {
                var currentBrand = self.plans()[i].insurer_title;
                if (brandsArray.indexOf(currentBrand) == -1) {
                    brandsArray.push(currentBrand);
                }
                if(self.plans()[i].insurer_id == brand_id()){
                    self.filter.selectedBrands.push(self.plans()[i].insurer_title);
                }
            }
            return brandsArray;
        });

        self.showAllPlans = function(){
            for (var i = 0; i < self.plans().length; i++) {
                var currentBrand = self.plans()[i].insurer_title;
                if (brandsArray.indexOf(currentBrand) == -1) {
                    brandsArray.push(currentBrand);
                }
            }
            self.filter.selectedBrands(brandsArray);
            Cookies.expire('brand_id');
            Cookies.expire('brand');
            brand_id(false);
        };
        self.toggleBrand = function (data) {
            if(self.filter.selectedBrands.indexOf(data) > -1)
                self.filter.selectedBrands.remove(data);
            else
                self.filter.selectedBrands.push(data);
        };

        self.selectPlan = function (plan) {
            if(self.selectedPlan() == plan)
                self.selectedPlan(null);
            else
                self.selectedPlan(plan);

            var status = ["Plan opened : " + plan.insurer_title + " - " + plan.product_title];
            status.push(plan.sum_assured + " | " + plan.premium);
            utils.notify(status.join("\n"));
            utils.event_log('PlanOpened', plan.insurer_title, 0);
            var nPlanMembers = plan.uidata.members.length;
            var vizury_plan_opened = {
                'vizury_category': 'HEALTH',
                'po_price': plan.premium,
                'po_name': plan.insurer_title + ' - ' + plan.product_title,
                'po_sub_category': nPlanMembers==1?'individual':'family',
                'po_insurer_img_url': window.location.origin+'/static/health_product/img/insurer/insurer_' + plan.insurer_id + '/on.png'
            };
            utils.gtm_data_log(vizury_plan_opened);
            utils.gtm_event_log('vizury_plan_opened');
        };
        self.addToCompare = function (plan) {
            if (self.plansToCompare().indexOf(plan) == -1) {
                if (self.plansToCompare().length < 3) {
                    self.plansToCompare.push(plan);
                }
            }
            else {
                self.plansToCompare.remove(plan);
            }
        };

        self.removeFromShortlist = function (plan) {
            shortlist.remove(function(p){
                return p.comparePlan(plan);
            });
            update_selection();
        };

        self.addToShortlist = function (plan) {
            if (!plan.planInShortlist()) {
                shortlist.push(plan);
                utils.event_log('PlanShortlisted', plan.insurer_title, 0);
                update_selection();
                vizury_shortlist();
            }
        };

        var vizury_shortlist = function() {
            var vizury_plans = {
                'vizury_category': 'HEALTH'
            };
            for(var i=0; i<shortlist().length && i<3; i++) {
                var plan = shortlist()[i];
                var nPlanMembers = plan.uidata.members.length;
                var j = i+1;
                vizury_plans['shortlist'+j+'_price'] = plan.premium;
                vizury_plans['shortlist'+j+'_name'] = plan.insurer_title + ' - ' + plan.product_title;
                vizury_plans['shortlist'+j+'_sub_category'] = nPlanMembers==1?'individual':'family';
                vizury_plans['shortlist'+j+'_insurer_img_url'] = window.location.origin+'/static/health_product/img/insurer/insurer_' + plan.insurer_id + '/on.png';
            }
            utils.gtm_data_log(vizury_plans);
            utils.gtm_event_log('vizury_shortlist');
            utils.gtm_event_log('PlanShortlisted');
        };

        var create_transaction = function(plan){
            var post_data = utils.post_buy_data(plan);
            post_data.slab_id = plan.slab_id;
            post_data.cpt_id = plan.cpt_id;
            post_data.tparty = TPARTY;
            $.post('/health-plan/transaction/create/', {'csrfmiddlewaretoken' : CSRF_TOKEN, 'data' : JSON.stringify(post_data)}, function(data){
                utils.gtm_data_log({'transaction_id': data.transaction_id});
                window.location.href = "/health-plan/buy/" + data.transaction_id;
            }, 'json');
            return false;
        };

        self.buy = function(plan){
            var status = ["Buying in progress"];
            post_data = {'csrfmiddlewaretoken' : CSRF_TOKEN,
                         'age': plan.max_age,
                         'slab_id': plan.slab_id}
            $.post("/health-plan/get-processing-status/", post_data , function(data){
                if(data.status){
                    processing_status = data.status;
                }
                else{
                    processing_status = 'BACKEND_NOT_INTEGRATED';
                }
                status.push(plan.insurer_title + " - " + plan.product_title);
                status.push(plan.sum_assured + (plan.deductible>0 ? " - " + plan.deductible : "") + " | " + plan.premium);
                status.push(processing_status);
                utils.notify(status.join("\n"));

                if(processing_status == 'BACKEND_NOT_INTEGRATED') {
                    utils.event_log('Offline', processing_status, 0);
                    ko.router.navigate(utils.ACTIVITY_ID+"/offline", {'trigger': true});
                }

                if(processing_status == 'OFFLINE_COVER_TOO_HIGH') {
                    utils.event_log('Offline', processing_status, 0);
                    ko.router.navigate(utils.ACTIVITY_ID+"/cover-offline", {'trigger': true});
                }

                if(processing_status == 'OFFLINE_AGE_TOO_HIGH') {
                    utils.event_log('Offline', processing_status, 0);
                    ko.router.navigate(utils.ACTIVITY_ID+"/age-offline", {'trigger': true});
                }

                if(processing_status == 'FILL_FORM' || processing_status == 'MEDICAL_REQUIRED') {
                    utils.event_log('TransactionInitiated', processing_status, 0);
                    create_transaction(plan);
                }
            });
        };

        self.loadingNewPlans = ko.observable(false);
        self.filterPlans = ko.computed(function() {
            self.loadingNewPlans(true);
            ko.utils.arrayForEach(self.plans(), function(p) {
                p.visible(true);
                //PED
                if(p.visible()){
                    //console.log("P.E.D: ",self.filter.ped());
                    if(self.filter.ped().length === 0){ p.visible(true);   }
                    else{
                        p.visible(self.filter.ped()[0].value==p.ped.waiting_period);
                    }
                }
                //ROOM_LIMIT
                //filter values: NO_LIMIT,PRIVATE
                //attr values: NO_LIMIT,CLASSA,CLASSB,CLASSC,CLASSD
                if(p.visible()){
                    if(self.filter.room_rent().length === 0){ p.visible(true);}
                    else{
                        //console.log("ROOM LIMIT: ",self.filter.room_rent(),"____",p.attr.room_rent.value.meta);
                        if(self.filter.room_rent()[0].value == "NO_LIMIT"){
                            p.visible( (p.attr.room_rent.value.meta.toLowerCase() == 'no_limit' ) );
                        }
                        else if(self.filter.room_rent()[0].value == "PRIVATE"){
                            p.visible( ['classa', 'classb', 'classc', 'no_limit'].indexOf(p.attr.room_rent.value.meta.toLowerCase()) > -1 );
                        }
                    }
                }
                //PLAN BENEFITS:
                //options: Restore benefits, critical illness, maternity cover
                if(self.filter.plan_benefits && p.visible()){
                    if(self.filter.plan_benefits().length === 0) {
                        p.visible(true);
                    } else {
                        var len = self.filter.plan_benefits().length;
                        var make_it_visible = true;
                        for(var i=0;i<len;i++){
                            make_it_visible = (make_it_visible) && (p.attr[self.filter.plan_benefits()[i].name].available);
                        }
                        p.visible(make_it_visible);
                    }
                }
                //NO COPAY
                if(p.visible()){
                    if(self.filter.copay().length === 0) {
                        p.visible(true);
                    } else {
                        if(self.filter.copay()[0].value == 'NO_COPAY'){
                            p.visible((p.attr.copay.value.toLowerCase() == '0%'));
                        }
                    }
                }
                if(p.visible()){
                    if(self.filter.maxPremium() >= p.premium) {
                        p.visible(true);
                    } else {
                        p.visible(false);
                    }
                }

                if(p.visible()){
                    if(self.filter.selectedBrands().length===0 || self.filter.selectedBrands().indexOf(p.insurer_title) > -1) {
                        p.visible(true);
                    } else {
                        p.visible(false);
                    }
                }
            });
            setTimeout(function(){
                self.loadingNewPlans(false);
            }, 230);
            if (step2.is_topup()){
                ko.utils.arrayForEach(self.visiblePlans(), function(item){
                    if(self.upgraded_sum_assured() != 0){
                        if(item.upgraded_sum_assured() != self.upgraded_sum_assured()){
                            item.visible(false);
                        }
                    }
                });
            }
        });

        self.getTabTitle = function () {
            var tabTitle;
            switch (self.type) {
                case "combined":
                case "family":
                case "parents":
                    tabTitle = utils.member_verbose(self.uidata().members);
                    break;
                case "shortlisted":
                    tabTitle = "Your shortlisted plans";
            }
            return tabTitle;
        };
        var T1;
        var sum_assured_subscription = self.sum_assured.subscribe(function (new_sa) {
            var pdata = utils.post_data();
            pdata.sum_assured = new_sa;
            self.selectedSort('Recommended');
            reload_plans(pdata);
        });
        var deductible_subscription = self.deductible.subscribe(function (new_deductible) {
            var pdata = utils.post_data();
            pdata.deductible = new_deductible;
            Cookies.set('deductible',new_deductible);
            self.selectedSort('Recommended');
            reload_plans(pdata);
        });
        self.upgraded_sum_assured.subscribe(function(new_upgraded_sum){
            self.loadingNewPlans(true);
            ko.utils.arrayForEach(self.plans(), function(item){
                if(new_upgraded_sum == 0){
                    item.visible(true);
                }
                else{
                    if(self.deductible() == item.deductible && new_upgraded_sum == item.upgraded_sum_assured()){
                        item.visible(true);
                    }
                    else{
                        item.visible(false);
                    }
                }
            });
            self.initializePremiumOptions();
        });
        var reload_plans = function(pdata){
            self.loadingNewPlans(true);
            T1 = new Date();
            var json_promise = $.post('/health-plan/get-score/' + utils.ACTIVITY_ID + '/', {
                'csrfmiddlewaretoken': CSRF_TOKEN,
                'data': JSON.stringify(pdata),
                'result_type': self.type,
                'filters': JSON.stringify(FILTERS) //TODO: What is this FILTERS doing! GROT
            }, function (res) {
                ko.utils.arrayForEach(self.filters_list(), function (fltr) {
                    fltr.selectedValues([]);
                });
                var parsedResponse = response.parseResponse(res);
                var scoreData = parsedResponse.score_data;
                //console.log("parsed Response > ", parsedResponse);
                var data = scoreData[0];
                self.uidata(data.uidata);
                self.plans(ko.utils.arrayMap(data.plans, function (item) {
                    var p = new Plan(item, data.uidata);
                    return p;
                }));
                if(step2.is_topup() && self.upgraded_options().length > 0){
                    self.upgraded_sum_assured(self.upgraded_options()[0]["id"]);
                }
                self.sum_assured(data.uidata.sum_assured);
                self.selectedPlan(null);
                self.plansToCompare([]);
                get_hospitals_for_tab([self]);
                setTimeout(function () {
                    self.loadingNewPlans(false);
                }, 400);
                self.initializePremiumOptions();

                var T2 = new Date();
                utils.load_time('ResultsTabLoading', 'v1', parseInt((T2-T1)/1000), 'APP');
            });
        };
        self.dispose = function() {
            self.visiblePlans.dispose();
            sort_subscription.dispose();
            self.titleForPremium.dispose();
            self.titleForDeductible.dispose();
            self.titleForCover.dispose();
            self.getAllBrands.dispose();
            self.filterPlans.dispose();
            sum_assured_subscription.dispose();
            deductible_subscription.dispose();
        };
    };
    return {
        Plan: Plan,
        Hospital: Hospital,
        ResultTab: ResultTab,
        resultTabs: resultTabs,
        currentTab: currentTab,
        update_selection: update_selection,
        shortlist: shortlist,
        get_hospitals_for_tab: get_hospitals_for_tab,
        brand_id: brand_id,
    };
});
