define(['knockout', 'jquery', 'utils', 'cookie', 'contact', 'login', 'knockout_validation', 'bstore',], function(ko, $, utils, Cookies, contact, login){
    var ExpertCard = function(){
        var self = this;
        self.count = ko.observable(0);
        self.showInvalidMobileError = ko.observable(false);
        self.loading = ko.observable(false);
        self.thanksMessage = ko.observable(false);

        self.aferCardRender = function(elements){
            self.count(self.count()+1);
        };
        self.expertImage = ko.observable('/static/health_product/desktopv2/app/assets/img/experts/experts-flipper.gif');

        self.leadOverlay = ko.observable(false);

        self.mobileNo = ko.observable(Cookies.get('mobileNo')).extend({
            required:{
                params: true,
                message: "Please enter your mobile number"
            },
            pattern: {
                params: '^([1-9])([0-9]){9}$',
                message: "Please enter a valid mobile number"
            }
        });

        if(self.mobileNo()){
            self.showLeadCard = ko.observable(false);
        } else {
            self.showLeadCard = ko.observable(true);
        }

        self.email = ko.observable().extend({
            required:{
                params: true,
                message: "Please enter a valid email address"
            },
            email: true
        });

        self.sendBuyForm = ko.observable(false);
        self.selectedPlan = ko.observable();
        self.currentPlan = ko.computed(function() {
            if(self.selectedPlan()) {
                return self.selectedPlan().insurer_title + ' - ' + self.selectedPlan().product_title;
            }
            else {
                return "";
            }
        });
        self.plan_to_buy = '';
        self.src = '';
        self.isBuyScreen = function() {
            //return results.overlayMode() == 'buy_plan';
        };


        self.validate = function(){
            var valid = true;
            self.mobileNo.isModified(true);

            valid = valid && self.mobileNo.isValid();
            return valid;
        };

        self.callMe = function(label, waiver_popup){
            if(self.validate()) {
                if(waiver_popup ? false :  !contact.dnd_waiver()){
                    return false;
                }
                Cookies.set('mobileNo', self.mobileNo());
                self.showInvalidMobileError(false);
                var post_to_lms = function(extra_data) {
                    extra_data = extra_data || {};
                    var post_data = {
                        'campaign' : 'Health',
                        'device' : 'Desktop',
                        'mobile': self.mobileNo(),
                        'activity_id': utils.ACTIVITY_ID,
                        'triggered_page': window.location.href,
                        'label': label || 'results_expert_card'
                    };
                    for(k in extra_data){
                        post_data[k] = extra_data[k];
                    }
                    self.loading(true);
                    $.ajax({
                        url: '/leads/save-call-time/',
                        type: 'POST',
                        data: {'data':JSON.stringify( post_data), 'csrfmiddlewaretoken': CSRF_TOKEN},
                        success: function(res) {
                            var src='desktop';
                            utils.gtm_data_log({'gtm_mobile': self.mobileNo()});
                            if(window.dataLayer) {
                                window.dataLayer.push({
                                    'event': 'GAEvent',
                                    'eventCategory': 'HEALTH',
                                    'eventAction': 'CallScheduled',
                                    'eventLabel': label || 'desktop_expert_card',
                                    'eventValue': 4
                                });
                            }

                            setTimeout(function() {
                                self.leadOverlay(false);
                                login.unSetLoginMode();
                                self.thanksMessage(true);
                                self.loading(false);
                                setTimeout(function() {
                                    self.showLeadCard(false);
                                }, 3000);
                            }, 250);
                        },
                        error: function() {
                            setTimeout(function() {
                                self.loading(false);
                                setTimeout(function() {
                                    self.mobileNo.error('Error while submitting the call request');
                                }, 50);
                            }, 400);
                        }
                    });//INNER.AJAX
                }
                var user_data = utils.post_data();
                $.ajax({
                    url: '/health-plan/get-city-by-pincode/',
                    type: 'POST',
                    data: {'data':JSON.stringify(utils.post_data()), 'csrfmiddlewaretoken': CSRF_TOKEN},
                    success: function(res) {
                        res = JSON.parse(res);
                        user_data['user_city']=res['user_city'];
                        user_data['parents_city']=res['parents_city'];
                        post_to_lms({user_data: user_data});
                    },//OUTER.SUCCESS
                    error: function(){
                        post_to_lms();
                    }
                });//OUTER.AJAX
            }
            else{
                self.showInvalidMobileError(true);
            }
        };

        self.isVisible = function() {
            return self.leadOverlay() || self.thanksMessage();
        };
        self.hide = function() {
            self.leadOverlay(false);
            self.thanksMessage(false);
        }

    }

    return new ExpertCard();
});

