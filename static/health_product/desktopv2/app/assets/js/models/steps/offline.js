define(['jquery', 'knockout', 'scheduler','cookie', 'knockout_validation'], function($, ko, scheduler, Cookies) {

    ko.validation.init({
        insertMessages: false,
        decorateInputElement : true,
        registerExtenders: true,
        messagesOnModified: true,
        messageTemplate: null,
        grouping: { deep: true }
    });

    contact_model = ko.observable();

    var contact_submit = function(){
        big_errors = ko.validation.group(contact_model(), { deep: true});
        if(big_errors().length > 0) {
            console.log(big_errors());
            big_errors.showAllMessages();
        }
        else{
            var data = JSON.parse(ko.toJSON(contact_model()));
            data['campaign']='Health';
            data['label']='offline';
            data['triggered_page']=window.location.href;
            $.post("/leads/save-call-time/", {'data' : JSON.stringify(data), 'csrfmiddlewaretoken' : CSRF_TOKEN}, function(data){
                if (data.success){
                    //TODO: Capture other details to be sent to caller
                    $('#before_submit').addClass('hide');
                    $('#after_submit').removeClass('hide');
                }
                else{
                    console.log('POST ERROR', data);
                }
            }, 'json');

        }
    };
    var initialize = function(activity_id){
        var cmodel = new scheduler.contactData;
        cmodel.initialize();
        contact_model(cmodel);
    };

    return {
        initialize: initialize,
        contact_model : contact_model,
        contact_submit : contact_submit
    }
});
