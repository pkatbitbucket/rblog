define(['knockout', 'step1', 'step2', 'step3', 'jquery', 'response', 'utils', 'models', 'offline', 'login', 'contact', 'cookie','expert_card', 'custom', 'bsvg'], function(ko, step1, step2, step3, $, response, utils, models, offline, login, contact, Cookie,expert_card) {
    var exper_card_count = ko.observable(0);

    var base_type = ko.observable("results");
    var throttledCurrentTab = ko.computed(function() {
        return models.currentTab();
    }).extend({ rateLimit: 200 });
    var resultPageNudge = ko.observable(false);
    var pre_shortlist_tab;

    var changeTab = function(data){
        shortlistMode(false);
        models.currentTab(data);
        models.currentTab().loadingNewPlans(true);
        setTimeout(function(){models.currentTab().loadingNewPlans(false);}, 200);
    };
    var current_post_data;
    var attr_titles = ko.observable();

    var T1;
    var initialize_result_page = function(res) {
        var parsedResponse = response.parseResponse(res);
        current_post_data = parsedResponse.uidata;

        //Initialize data
        step1.initialize_members(step1.members, current_post_data.members);
        step1.gender(current_post_data.gender);
        step2.product_type(current_post_data.product_type);
        step2.deductible(current_post_data.deductible);
        step2.medical_conditions(current_post_data.medical_conditions);
        step3.userPincode(current_post_data.pincode);
        step3.parentsPincode(current_post_data.parents_pincode);

        // utils.notify(["Complete Search: "+ step1.gender()+" - "+utils.member_verbose(step1.members(), true), "Pincode: "+step3.userPincode()+ " | Parents pincode:" + step3.parentsPincode(), "Hospital service: "+step2.service_type()]);

        var scoreData = parsedResponse.score_data;
        var tabs = [];
        var max_plans_id = 0;
        for (var i = 0; i < scoreData.length; i++) {
            tabs.push(new models.ResultTab(scoreData[i]));
            max_plans_id = (tabs[i].plans().length < tabs[max_plans_id].plans().length) ? max_plans_id : i;
        }
        models.resultTabs(tabs);
        models.get_hospitals_for_tab(tabs);
        models.currentTab(models.resultTabs()[max_plans_id]);

        var vizury_tab = tabs.length>1 ? tabs[1] : tabs[0];
        var vizury_plans = {
            'vizury_category': 'HEALTH'
        };
        for(var i=0; i<vizury_tab.plans().length && i<3; i++) {
            var plan = vizury_tab.plans()[i];
            var j = i+1;
            var nPlanMembers = plan.uidata.members.length;
            vizury_plans['city'] = plan.uidata.city.name;
            vizury_plans['product'+j+'_name'] = plan.insurer_title + ' - ' + plan.product_title;
            vizury_plans['product'+j+'_price'] = plan.premium;
            vizury_plans['product'+j+'_sub_category'] = nPlanMembers==1?'individual':'family';
            vizury_plans['product'+j+'_insurer_img_url'] = window.location.origin+'/static/health_product/img/insurer/insurer_' + plan.insurer_id + '/on.png';
        }
        utils.gtm_data_log(vizury_plans);
        utils.gtm_event_log('vizury_results');

        shortlistMode(false);

        var first_plan = models.currentTab().plans()[0];

        var get_attr_obj = function() {
            var t = [];
            if(first_plan == undefined){
                return t;
            }
            ko.utils.arrayForEach(first_plan.attr_list, function(item){
                if(['MID', 'LOW'].indexOf(item[1].importance) > -1) {
                    t.push([item[0], item[1].title]);
                }
            });
            return t;
        };
        attr_titles(get_attr_obj());
        var T2 = new Date();
        utils.load_time('ResultsLoading', 'v1', parseInt((T2-T1)/1000), 'APP');
    };

    function setup(ACTIVITY_ID){
        models.resultTabs([]);
        models.currentTab(null);
        utils.ACTIVITY_ID = ACTIVITY_ID;
        T1 = new Date();
        var json_promise = $.post('/health-plan/get-score/'+utils.ACTIVITY_ID+'/', {
            'csrfmiddlewaretoken' : CSRF_TOKEN,
            'data': JSON.stringify({}),
            'filters': JSON.stringify(FILTERS)
        }, function(res){
            initialize_result_page(res);
            get_shortlisted_plans();
        });
    }

    var compareMode = ko.observable(false);
    var modifyDetailsMode = ko.observable(false);
    var shortlistMode = ko.observable(false);
    var setCompareMode = function(){
        compareMode(true);
        document.getElementsByTagName('body')[0].className+=' scroll-disable';
        var status = ["Comparing plans"];
        $.each(models.currentTab().plansToCompare(), function(i, plan){
            status.push(plan.insurer_title + " - " + plan.product_title + " | " + plan.sum_assured + " | " + plan.premium);
            utils.event_log('ComparePlan', plan.insurer_title, 0);
        });
        utils.notify(status.join('\n'));
    };
    var setShortlistMode = function(){
        if(shortlistMode())
            return;
        pre_shortlist_tab = models.currentTab();
        var stab = new models.ResultTab({
            uidata: {},
            plans : [],
            type : "shortlisted",
        }, false);
        models.currentTab(stab);
        models.currentTab().plans(models.shortlist());
        shortlistMode(true);
        login.toggleContextMenu();
        var cdetails = [];
        cdetails.push("Tab: Shortlisted");
        var tplans = [];
        ko.utils.arrayForEach(models.currentTab().plans(), function(plan){
            tplans.push("Members: " + utils.member_verbose(plan.uidata.members, true));
            tplans.push(plan.insurer_title + " - " + plan.product_title + " | " + plan.sum_assured + " | " + plan.premium);
            tplans.push("-----------------------------------------");
        });
        cdetails.push("Shortlisted Plans: ", tplans.join('\n'));
        utils.notify(cdetails.join('\n'));
    };
    models.shortlist.subscribe(function(){
        if(shortlistMode() && models.currentTab()) {
            models.currentTab().plans(models.shortlist());
        }
    });
    var setModifyDetailsMode = function(){
        var urlToRedirect = (step2.is_topup()===true)?'/health-insurance/super-topup':'/health-insurance'

        window.location.href = urlToRedirect;

        /*
        modifyDetailsMode(true);
        utils.event_log('ModifyDetailsInitiated', 'v1', 0);
        document.getElementsByTagName('body')[0].className+=' scroll-disable';
        */
    };
    var unsetShortlistMode = function(){
        if(shortlistMode()) {
            var stab = models.currentTab();
            shortlistMode(false);
            models.currentTab(pre_shortlist_tab);
            stab.dispose();
        }
    };
    var resetCompareMode = function(){
        compareMode(false);
        document.body.className = document.body.className.replace("scroll-disable","");

    };
    var resetModifyDetailsMode = function(){
        modifyDetailsMode(false);
        document.body.className = document.body.className.replace("scroll-disable","");
    };
    var throttledCompareMode = ko.computed(function() {
        return compareMode();
    }).extend({ rateLimit: 500 });

    var get_shortlisted_plans = function(){
        var json_promise = $.get('/health-plan/get-details/shortlist/', {
            'csrfmiddlewaretoken' : CSRF_TOKEN,
        }, function(res){
            var parsedResponse = response.parseResponse(res);
            var stlist = [];
            ko.utils.arrayForEach(parsedResponse.shortlist, function(item){
                var plan = new models.Plan(item.plans[0], item.uidata);
                if (!plan.planInShortlist()) {
                    stlist.push(plan);
                }
            });
            models.shortlist(stlist);
        });
    };

    var reloadResults = function(){
        step1.members.valueHasMutated();
        var pdata = utils.post_data();
        if(utils.deepCompare(current_post_data, pdata)){
            resetModifyDetailsMode();
            return;
        }
        if(step1.validate() && step2.validate() && step3.validate()){
            models.currentTab(null);
            if(!utils.deepCompare(current_post_data.members, pdata.members)){
                models.shortlist([]);
                models.update_selection();
            }
            shortlistMode(false);
            resetModifyDetailsMode();
            T1 = new Date();
            var json_promise = $.post('/health-plan/get-score/'+utils.ACTIVITY_ID+'/', {
                'csrfmiddlewaretoken' : CSRF_TOKEN,
                'data': JSON.stringify(pdata),
                'filters': JSON.stringify(FILTERS),
                'result_type': 'INIT'
            }, function(res){
                initialize_result_page(res);
            });
        }
    };
    var set_base_type = function(btype){
        ko.router.navigate(utils.ACTIVITY_ID, {trigger: true});
    };

    var cache = {
        results : false,
        offline : false
    };

    return {
        initialize : function(ACTIVITY_ID, base) {
            console.log("BASE:", base, "ACTIVITY_ID:", ACTIVITY_ID);
            if(['', null, undefined].indexOf(base) > -1){
                base = 'results';
            }
            console.log("BASE TYPE ===>", base);
            if(base == 'results'){
                utils.page_view('/vp/health/results/', 'VP-HEALTH-RESULTS');
                base_type('results');
                if(!cache.results) {
                    //Jquery and other Javascript
                    document.getElementsByTagName('body')[0].className+=' results-page';
                    // improve scroll performance
                    var body = document.body, timer;
                    window.addEventListener('scroll', function() {
                        clearTimeout(timer);
                        if(!body.classList.contains('disable-hover')) {
                            body.classList.add('disable-hover');
                        }
                        timer = setTimeout(function(){
                            body.classList.remove('disable-hover');
                        },350);
                    }, false);
                    // improve scroll performance
                    setup(ACTIVITY_ID);
                }
                cache.results = true;
            }
            if(['offline', 'cover-offline', 'age-offline'].indexOf(base) > -1) {
                base_type(base);
                if(!cache.offline) {
                    offline.initialize();
                }
                cache.offline = true;
            }
            var login_mode = Cookie.get('login_mode')=='email'? 'email': 'mobile';
            var login_template = {
                'email': {name: 'global/login', data: {login: login}, afterRender: login.afterRender},
                'mobile': {name: 'global/login_lms', data: {login: login} }
            }[login_mode];

            //If cookie for relmanager is set, or cookie to hide login is set, skip login
            if((TPARTY == 'jd') || Cookie.get('hide_login') || Cookie.get('mobileNo')){
                login.loginMode(false);
                utils.event_log('LoginPageSkipped', 'v1', 0);
                utils.page_view('/vp/health/login-popup-skipped/', 'VP-HEALTH-LOGIN-POPUP-SKIPPED');
            } else {
                login.loginMode(true);
                utils.event_log('LoginPageShown', 'v1', 0);
                utils.page_view('/vp/health/login-popup-shown/', 'VP-HEALTH-LOGIN-POPUP-SHOWN');
            }
            return {
                'tparty' : TPARTY,
                'base_type' : base_type,
                'set_base_type': set_base_type,
                'login_template': login_template,
                'offline' : offline,
                'step1' : step1,
                'step2' : step2,
                'step3' : step3,
                'utils': utils,
                'tabs' : models.resultTabs,
                'currentTab' : models.currentTab,
                'shortlist' : models.shortlist,
                'changeTab' : changeTab,
                'resultPageNudge':resultPageNudge,
                'throttledCurrentTab' : throttledCurrentTab,
                'compareMode':compareMode,
                'setCompareMode':setCompareMode,
                'setShortlistMode':setShortlistMode,
                'shortlistMode':shortlistMode,
                'resetCompareMode':resetCompareMode,
                'attr_titles':attr_titles,
                'throttledCompareMode':throttledCompareMode,
                'setModifyDetailsMode':setModifyDetailsMode,
                'unsetShortlistMode': unsetShortlistMode,
                'modifyDetailsMode':modifyDetailsMode,
                'resetModifyDetailsMode':resetModifyDetailsMode,
                'reloadResults':reloadResults,
                'login': login,
                'contact': contact,
                'expert_card' : expert_card,
                'brand_id' : models.brand_id,
                'health_toll_free': health_toll_free
            };
        }
    };
});
