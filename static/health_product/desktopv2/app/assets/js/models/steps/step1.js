define(['knockout-router', 'knockout','knockout_validation'], function(router, ko) {

    ko.validation.configure({
        errorMessageClass: 'error',
        decorateElement : true,
        errorElementClass: 'has-error',
        insertMessages: false
    });

    var MAX_KIDS = 7;
    var KIDS_AGE_LIST = [];
    var ADULTS_AGE_LIST = [];
    (function() {
        /* Fills KIDS_AGE_LIST and ADULTS_AGE_LIST
         * in the format [{id: '0-2', name: '2 months'}, {id: '25-0', name: '25 years'}] */

        var i, max_kid_age = 25, min_adult_age=18, max_adult_age=100;
        function ko_format(age_in_years, age_in_months) {
            var age_code = get_age_code(age_in_years, age_in_months);
            return {id: age_code, name: get_display_age(age_code)};
        }
        for(i=0; i<12; i++)
            KIDS_AGE_LIST.push(ko_format(0, i));
        for(i=1; i<=max_kid_age; i++)
            KIDS_AGE_LIST.push(ko_format(i, 0));
        for(i=min_adult_age; i<=max_adult_age; i++)
            ADULTS_AGE_LIST.push(ko_format(i, 0));
    })();
    function get_age_code(age_in_years, age_in_months) {
        if([undefined, null, ''].indexOf(age_in_years) > -1 ||
           [undefined, null, ''].indexOf(age_in_months) > -1)
            return '';
        return age_in_years + '-' + age_in_months;
    }
    function get_age_in_years_months(age_code) {
        if(!age_code) return [undefined, undefined];
        var split_age = age_code.split('-');
        var age_in_years = parseInt(split_age[0]);
        var age_in_months = parseInt(split_age[1]);
        return [age_in_years, age_in_months];
    }
    function get_display_age(age) {
        if(!age) return '';
        var split_age = get_age_in_years_months(age);
        var age_in_years = split_age[0];
        var age_in_months = split_age[1];
        var val = (age_in_years > 0) ? age_in_years : age_in_months;
        var unit = (age_in_years > 0) ? 'year' : 'month';
        unit = unit + (val == 1 ? '' : 's');
        var display_age = val + ' ' + unit;
        return display_age;
    }

    function Member(type, selected, person, displayName) {
        var self = this;
        self.selected = ko.observable(selected);
        self.person = person;
        self.id = self.person.toLowerCase();
        self.type = type;
        self.age_in_years = ko.observable();
        self.age_in_months = ko.observable();
        self.age = ko.observable().extend({
            required: {
                onlyIf: self.selected,
                message: 'Please select age of insured member'
            }
        });
        self.age.subscribe(function(val) {
            var split_age = get_age_in_years_months(val);
            self.age_in_years(split_age[0]);
            self.age_in_months(split_age[1]);
        });
        self.ageList = (type == 'kid') ? KIDS_AGE_LIST : ADULTS_AGE_LIST;
        self.error = ko.observable();
        self.displayName = displayName || person;
        self.toggleSelect = function () {
            self.selected(!self.selected());
        };
    }

    function stepOneViewModel() {
        var self = this;
        self.range = function (a, b, step) {
            var A = [];
            A[0] = a;
            step = step || 1;
            while(a+step<=b){
                A[A.length] = a+= step;
            }
            return A;
        };
        self.gender = ko.observable().extend({
            lstore : {
                key : 'gender'
            },
            required: {
                params:true,
                message: "Please select your gender"
            }
        });
        self.you = new Member('adult', false, 'You');
        self.spouse = new Member('adult', false, 'Spouse', ko.computed(function(){
            switch(self.gender()) {
                case 'MALE': return 'Your Wife';
                case 'FEMALE': return 'Your Husband';
                default: return 'Your Spouse';
            }
        }));
        self.father = new Member('adult', false, 'Father');
        self.mother = new Member('adult', false, 'Mother');
        var user_family = [self.you, self.spouse];
        var parents = [self.father, self.mother];
        var adults = user_family.concat(parents);

        self.addKid = function(person) {
            var kid = new Member('kid', true, person);
            var no_of_kids = ko.utils.arrayFilter(self.members(), function(member) {
                return member.person == person;
            }).length;
            kid.id = person.toLowerCase() + (no_of_kids + 1);
            self.members.push(kid);

            // remove the kid when deselected
            kid.selected.subscribe(function(val) {
                self.members.remove(kid);
            });

            return kid;
        };

        // pass copy of adults, else kids will be added to this same array
        self.members = ko.observableArray(adults.slice());
        self.members.extend({
            lstore : {
                key : 'members',
                dump: function(nval){
                    var mList = [];
                    ko.utils.arrayForEach(nval, function(m){
                        if(m.selected()) {
                            mList.push({
                                age: m.age(),
                                person: m.person
                            });
                        }
                    });
                    return ko.toJSON(mList);
                },
                init: function(obj, mList) {
                    try {
                    ko.utils.arrayForEach(mList, function(m){
                        if(m.person == 'son' || m.person == 'daughter') {
                            var mem = ko.utils.arrayFirst(obj(), function(mem) {
                                return mem.person == m.person;
                            });
                            mem.age(m.age);
                            mem.selected(m.selected);
                        }
                        else {
                            var kid = self.addKid(m.person, obj);
                            kid.age(m.age);
                        }
                    });
                    }
                    catch(e) {}
                }
            },
            validation : [
                {
                    'validator' : function(obj){
                        return ko.utils.arrayFilter(obj, function(m) {
                            return m.selected();
                        }).length > 0;
                    },
                    'message' : 'Select at least one member to insure'
                },
                {
                    'validator' : function(obj){
                        return ko.utils.arrayFilter(obj, function(m) {
                            return m.type == 'adult' && m.selected() === true;
                        }).length > 0;
                    },
                    'message' : 'Select at least one adult to insure'
                }
            ]
        });

        self.initialize_members = function(obj, members) {
            for(var i = obj().length-1; i>=0; i--)
                obj()[i].selected(false);
            ko.utils.arrayForEach(members, function(m){
                var age = get_age_code(m.age, m.age_in_months);
                if(m.person == 'son' || m.person == 'daughter') {
                    var kid = self.addKid(m.person, obj);
                    kid.age(age);
                }
                else {
                    var mem = ko.utils.arrayFirst(obj(), function(mem) {
                        return mem.person.toLowerCase() == m.person.toLowerCase();
                    });
                    mem.age(age);
                    mem.selected(true);
                }
            });
            obj.valueHasMutated();
        };

        self.selectMember = function(member){
            if (member.selected()) {
                if(member.type == 'kid')
                {
                    self.members.remove(member);
                    return member.selected(false);
                }
                else {
                    return member.selected(false);
                }
            }
            else {
                return member.selected(true);
            }
        };
        self.selectGender = function(gen) {
            self.gender(gen);
        };

        self.getSelectedMembers = ko.computed(function(){
            return ko.utils.arrayFilter(self.members(), function(member) { return member.selected();});
        });
        self.getSelectedKids = ko.computed(function(){
            return ko.utils.arrayFilter(self.members(), function(member) { return member.type == 'kid';});
        });

        self.cumulativeAgeError = ko.observable().extend({
            validation: [
            {
                validator: function(obj){
                    return ko.utils.arrayFilter(self.getSelectedMembers(), function(m) {
                        return !m.age();
                    }).length === 0;
                },
                'message':"Please select respective member's age"
            }]
        });

        self.step1Errors = ko.validation.group([self.members,self.gender, self.cumulativeAgeError]);
        self.validate = function() {
            valid = true;
            if(self.step1Errors().length !== 0) {
                self.step1Errors.showAllMessages(true);
                valid = false;
            }
            return valid;
        };
    }

    var stepOneViewModelInstance = new stepOneViewModel();
    return stepOneViewModelInstance;
});
