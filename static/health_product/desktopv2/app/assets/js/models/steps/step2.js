define(['knockout-router', 'knockout', 'step1', 'knockout_validation','bstore'], function(router, ko, step1View) {

    // TODO: fetch available deductibles from DB
    function get_deductible_options() {
        return [
            {name: '1 Lac', id: 100000},
            {name: '2 Lacs', id: 200000},
            {name: '3 Lacs', id: 300000},
            {name: '4 Lacs', id: 400000},
            {name: '5 Lacs', id: 500000},
            {name: '6 Lacs', id: 600000},
            {name: '7 Lacs', id: 700000},
            {name: '8 Lacs', id: 800000},
            {name: '9 Lacs', id: 900000},
            {name: '10 Lacs', id: 1000000}
        ];
    }

    function stepTwoViewModel() {
        var self = this;

        // value updated from results
        self.product_type = ko.observable('INDEMNITY').extend({
            lstore: {key: 'product_type'},
            required: true
        });
        self.is_topup = ko.computed(function() {
            return ['TOPUP', 'SUPER_TOPUP'].indexOf(self.product_type()) != -1;
        });

        self.deductible_options = get_deductible_options();
        self.deductible = ko.observable().extend({
            lstore: {key : 'deductible'},
            required: {
                onlyIf: self.is_topup,
                message:  'Please select your existing sum assured'
            }
        });
        self.medical_conditions = ko.observable();
        self.validate = function() {
            self.step2Errors = ko.validation.group([self.deductible]);
            valid = true;
            if(self.step2Errors().length !== 0) {
                self.step2Errors.showAllMessages();
                valid = false;
            }
            return valid;
        };
    }
    return new stepTwoViewModel();
});
