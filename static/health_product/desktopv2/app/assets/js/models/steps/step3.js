define(['knockout-router', 'knockout', 'step1', 'pincheck', 'knockout_validation'], function(router, ko, step1View, pincheck) {

    function stepThreeViewModel() {
        var self = this;
        self.userPincode = ko.observable().extend({
            lstore : {key : 'pincode'},
            required: {
                params:true,
                message: "Please select your area pincode"
            },
            validation : {
                validator : function(val){
                    return pincheck.isValid(val);
                },
                message : "Please enter a valid pincode"
            }
        });
        self.parentsPincode = ko.observable().extend({
            lstore : {key : 'parents_pincode'},
            required: {
                params:true,
                message: "Please select your parents' area pincode"
            },
            validation : {
                validator : function(val){
                    return pincheck.isValid(val);
                },
                message: "Please enter a valid pincode"
            }
        });
        self.hasParents = function() {
            var par = ko.utils.arrayFilter(step1View.getSelectedMembers(), function(member) {
                return member.person == 'Father' || member.person == 'Mother';
            });
            return par.length > 0;
        };
        self.hasFamily = function() {
            var family = ko.utils.arrayFilter(step1View.getSelectedMembers(), function(member) {
                return member.person == 'You' || member.person == 'Spouse' || member.type == 'kid';
            });
            return family.length > 0;
        };

        self.getErrorGroup = function(){
            if(self.hasParents() && self.hasFamily()){
                self.step3Errors = ko.validation.group([self.userPincode, self.parentsPincode]);
            }
            if(!self.hasParents() && self.hasFamily()) {
                self.step3Errors = ko.validation.group([self.userPincode]);
            }
            if(self.hasParents() && !self.hasFamily()) {
                self.step3Errors = ko.validation.group([self.parentsPincode]);
            }
        };
        self.validate = function() {
            valid = true;
            self.getErrorGroup();
            if(self.step3Errors().length !== 0) {
                self.step3Errors.showAllMessages();
                valid = false;
            }
            return valid;
        };
    }
    return new stepThreeViewModel();
});
