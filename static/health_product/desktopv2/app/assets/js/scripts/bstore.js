define(['knockout', 'cookie'], function (ko, Cookie) {
    //Function to check for "", null, undefined, [], {}
    var hasOwnProperty = Object.prototype.hasOwnProperty;

    function isEmpty(obj) {

        // null and undefined are "empty"
        if (obj == null) return true;

        // Assume if it has a length property with a non-zero value
        // that that property is correct.
        if (obj.length > 0)    return false;
        if (obj.length === 0)  return true;

        // Otherwise, does it have any properties of its own?
        // Note that this doesn't handle
        // toString and valueOf enumeration bugs in IE < 9
        for (var key in obj) {
            if (hasOwnProperty.call(obj, key)) return false;
        }

        return true;
    }

    ko.extenders.store = function (target, settings) {
        var load_data = function() {
            var init = Cookie.get(settings.key);
            if(init) {
                var jinit;
                try {
                    jinit = JSON.parse(init);
                } catch (e) {
                    jinit = init;
                }
                if(settings.init)
                    settings.init(target, jinit);
                else
                    target(jinit);
            }
        };
        target.reload = load_data;
        target.reload();

        var save_data = function(newValue) {
            var store_val;
            if(settings.dump){
                store_val = settings.dump(newValue);
            } else {
                try {
                    store_val = ko.toJSON(newValue);
                } catch (e) {
                    store_val = newValue;
                }
            }
            Cookie.set(settings.key, store_val);
        };
        target.save = save_data;
        target.subscribe(save_data);
    };

    if(window.localStorage) {
        ko.extenders.lstore = function(target, settings) {
            var load_data = function() {
                var init_val = window.localStorage.getItem(settings.key);
                if(init_val) {
                    try {
                        init_val = JSON.parse(init_val);
                    } catch(e) {}
                    if(settings.init)
                        settings.init(target, init_val);
                    else
                        target(init_val);
                }
            };
            target.reload = load_data;
            target.reload();

            var save_data = function(newValue) {
                var store_val;
                if(settings.dump)
                    store_val = settings.dump(newValue);
                else if(typeof newValue === 'string')
                    store_val = newValue;
                else {
                    try {
                        store_val = ko.toJSON(newValue);
                    } catch(e) {
                        store_val = newValue;
                    }
                }
                window.localStorage.setItem(settings.key, store_val);
            };
            target.save = save_data;
            var save_sub = target.subscribe(save_data);

            var init_dispose = target.dispose;
            target.dispose = function() {
                save_sub.dispose();
                if(init_dispose) init_dispose.apply(target, arguments);
            };
        };
    }
    else
        ko.extenders.lstore = ko.extenders.store;
});
