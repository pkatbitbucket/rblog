define(['jquery', 'knockout', 'svg'], function ($, ko, SVG) {
    ko.bindingHandlers.svgChart = {
        update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
            var dataPoints = valueAccessor().items,
            max = Math.max.apply(null, dataPoints),
            min = Math.min.apply(null, dataPoints),
            range = max-min;
            if(range == 0){
                range = 1;
            }
            var k = 100/range;
            for (var i = 0; i < dataPoints.length; i++) {
                var div = document.createElement('div');
                div.style.left = i*20+"%";
                div.style.bottom = ((dataPoints[i]-min)*k)+50+"px";
                div.className = "dataPoint";
                var innerdiv = document.createElement('div');
                innerdiv.className = "dataPointDetail";
                var text = document.createTextNode("INR "+ dataPoints[i]);
                innerdiv.appendChild(text);
                div.appendChild(innerdiv);
                element.appendChild(div);
            }
            var canvas = SVG(element);
            var divs = element.getElementsByClassName('dataPoint');
            for(var i = 0 ; i<divs.length-1; i++){
                var line = canvas.line(divs[i].offsetLeft, divs[i].offsetTop, divs[i+1].offsetLeft, divs[i+1].offsetTop).stroke({ width: 2, color: 'rgb(255, 123, 94)' });
            }
        },
    };
});
