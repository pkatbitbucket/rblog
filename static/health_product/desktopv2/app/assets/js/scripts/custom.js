define(['jquery', 'knockout'], function ($, ko) {
    ko.bindingHandlers.numeric = {
        init: function (element, valueAccessor) {
            $(element).on("keydown", function (event) {
                // Allow: backspace, delete, tab, escape, and enter
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                    // Allow: Ctrl+A
                    (event.keyCode == 65 && event.ctrlKey === true) ||
                        // Allow: . ,
                        (event.keyCode == 188 || event.keyCode == 190 || event.keyCode == 110) ||
                            // Allow: home, end, left, right
                            (event.keyCode >= 35 && event.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }
                else {
                    // Ensure that it is a number and stop the keypress
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                    }
                }
            });
        }
    };

    ko.bindingHandlers.inr_currency = {
        update: function(element, valueAccessor, allBindingsAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor());
            var x = value.toString();
            var afterPoint = '';
            if(x.indexOf('.') > 0)
                afterPoint = x.substring(x.indexOf('.'), x.length);
            x = Math.floor(x);
            x = x.toString();
            var lastThree = x.substring(x.length-3);
            var otherNumbers = x.substring(0, x.length-3);
            if(otherNumbers != '')
                lastThree = ',' + lastThree;
            var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
            var prefix = allBindingsAccessor().currencyPrefix || "Rs.";
            ko.bindingHandlers.text.update(element, function() { return prefix + " " + res; });
        },
    };

    ko.bindingHandlers.currency_word = {
        update: function(element, valueAccessor, allBindingsAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor());
            var x = parseInt(value)/100000;
            if(x % 1 != 0) { //check if there are after decimal digits
                res = "Rs. " + x.toFixed(1) + " Lacs";
            } else {
                res = "Rs. " + x + " Lacs";
            }
            ko.bindingHandlers.text.update(element, function() { return res; });
        },
    };

    ko.bindingHandlers.currency_word_withoutINR = {
        update: function(element, valueAccessor, allBindingsAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor());
            var x = parseInt(value)/100000;
            if(x % 1 != 0) { //check if there are after decimal digits
                res = x.toFixed(1) + " Lacs";
            } else {
                res = x + " Lacs";
            }
            ko.bindingHandlers.text.update(element, function() { return res; });
        },
    };

    ko.bindingHandlers.trimText = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var trimmedText = ko.computed(function () {
                var defaultMaxLength = 20;
                var minLength = 5;
                var untrimmedText = ko.utils.unwrapObservable(valueAccessor());
                var maxLength = ko.utils.unwrapObservable(allBindingsAccessor().trimTextLength) || defaultMaxLength;
                if (maxLength < minLength) maxLength = minLength;
                var text = untrimmedText.length > maxLength ? untrimmedText.substring(0, maxLength - 1) + '...' : untrimmedText;
                return text;
            });
            ko.applyBindingsToNode(element, {
                text: trimmedText
            }, viewModel);

            return {
                controlsDescendantBindings: true
            };
        }
    };
});
