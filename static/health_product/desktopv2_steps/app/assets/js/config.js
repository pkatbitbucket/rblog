'use strict';
require.config({
    baseUrl : '/static/health_product/desktopv2_steps/app',
    paths: {
        'text': '/static/health_product/desktopv2_steps/app/lib/require/text',
        'knockout': '/static/health_product/desktopv2_steps/app/lib/knockout/knockout-3.3.0',
        'knockout_validation': '/static/health_product/desktopv2_steps/app/lib/knockout/knockout.validation',
        'knockout-amd-helpers': '/static/health_product/desktopv2_steps/app/lib/knockout/knockout-amd-helpers',
        'knockout-history': '/static/health_product/desktopv2_steps/app/lib/knockout/knockout-history',
        'knockout-router': '/static/health_product/desktopv2_steps/app/lib/knockout/knockout-router',
        'utils': '/static/health_product/desktopv2_steps/app/assets/js/models/global/utils',
        'user_info': '/static/health_product/desktopv2_steps/app/assets/js/models/steps/user_info',
        'medical': '/static/health_product/desktopv2_steps/app/assets/js/models/steps/medical_conditions',
        'contact': '/static/health_product/desktopv2_steps/app/assets/js/models/global/contact',
        'cookie' : '/static/health_product/desktopv2_steps/app/lib/cookies/cookies',
        'bstore' : '/static/health_product/desktopv2_steps/app/assets/js/scripts/bstore',
        'events': '/static/health_product/desktopv2_steps/app/assets/js/models/global/events',
        'mixpanel-actions' :'/static/health_product/desktopv2_steps/app/assets/js/models/global/mixpanel-actions',
        'pincheck' : '/static/health_product/desktopv2_steps/app/assets/js/scripts/pincheck',
        'jquery' : '/static/health_product/desktopv2_steps/app/lib/jquery/jquery-1.11.0',
        'chosen' : '/static/health_product/desktopv2_steps/app/lib/jquery/chosen/chosen.jquery-1.4.2',
        'main': '/static/health_product/desktopv2_steps/app/assets/js/main'
    },
    shim: {
        'knockout_validation' : {
            deps : ['knockout']
        },
        'knockout-amd-helpers': {
            deps: ['knockout']
        },
        'knockout-history': {
            deps: ['knockout']
        },
        'knockout-router': {
            deps: ['knockout', 'knockout-history', 'knockout-amd-helpers']
        },
        'chosen': {
            deps: ['jquery']
        }
    }
});

define(['main', 'bstore', 'knockout-amd-helpers'], function(main) {
    main.run();
});
