define(['knockout', 'jquery', 'cookie', 'utils', 'knockout_validation'], function(ko, $, Cookies, utils) {

    function Contact() {
        var self = this;

        self.dnd_waiver = function() {
            return confirm("I hereby authorize Coverfox to communicate with me on the given number for my Insurance needs. I am aware that this authorization will override my registry under NDNC.");
        };

        self.mobile = ko.observable().extend({
            required: true,
            minLength: 10,
            maxLength: 10,
            pattern: {
                message: 'Invalid phone number',
                params: /[7-9]{1}\d{9}/
            }
        });
        self.in_process = ko.observable(false);
        self.done = ko.observable(false);
        self.submit_error = ko.observable(false);
        self.cerror = ko.validation.group([self.mobile]);
        self.mobile.subscribe(function(val){
            if(["", undefined, null].indexOf(val) > -1){
                self.cerror.showAllMessages(false);
            }
        });
        self.change = function(){
            self.done(false);
        };
        self.submit = function(source){
            self.cerror.showAllMessages(true);
            if(self.cerror().length === 0){
                if(!self.dnd_waiver()) {
                    return false;
                }
                var post_to_lms = function(extra_data){
                    extra_data = extra_data || {};
                    var cmodel =  {
                        'mobile' : self.mobile(),
                        'device': 'Desktop',
                        'campaign':'Health',
                        'label': source || 'search-details',
                        'triggered_page': window.location.href
                    };
                    for(var k in extra_data){
                        cmodel[k] = extra_data[k];
                    }

                    Cookies.set('mobileNo', self.mobile());
                    self.in_process(true);
                    utils.notify("*************************");
                    utils.notify("CALL CUSTOMER: " + self.mobile());
                    utils.notify("*************************");
                    utils.gtm_data_log({'gtm_mobile': self.mobile()});
                    utils.event_log('CallScheduled', 'TOP', 4);
                    window.dataLayer.push({
                        'event': 'virtual_page_view',
                        'virtual_page_path': '/vp/health/call-scheduled/contact-page/',
                        'virtual_page_title': 'VP-HEALTH-CALL-SCHEDULED-CONTACT-PAGE'
                    });
                    $.post("/leads/save-call-time/", {'data' : ko.toJSON(cmodel), 'csrfmiddlewaretoken' : CSRF_TOKEN}, function(data){
                        if (data.success){
                            self.in_process(false);
                            self.done(true);
                            setTimeout(function() {
                                self.done(false);
                            }, 3000);
                        }
                        else{
                            self.submit_error(true);
                            console.log('POST ERROR', data);
                            setTimeout(function() {
                                self.submit_error(false);
                            }, 3000);
                        }
                    }, 'json');
                };
                var user_data = utils.post_data();
                $.ajax({
                    url: '/health-plan/get-city-by-pincode/',
                    type: 'POST',
                    data: {'data':JSON.stringify(utils.post_data()), 'csrfmiddlewaretoken': CSRF_TOKEN},
                    success: function(res) {
                        res = JSON.parse(res);
                        user_data.user_city = res.user_city;
                        user_data.parents_city = res.parents_city;
                        post_to_lms({user_data:user_data});
                    },//OUTER.SUCCESS
                    error: function(){
                        post_to_lms();
                    }
                });
            }
            utils.show_chat_box();
        };
    }

    var contactInstance = new Contact();
    return contactInstance;
});
