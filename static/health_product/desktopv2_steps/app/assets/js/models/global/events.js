define(['mixpanel-actions'], function(actions) {

    // Custom Event Creator

    var cust_ev = function(ev, data) {
        var evt = new CustomEvent(ev, {
            detail: data
        });
        window.dispatchEvent(evt);
    };

    var add_listener = function(evname, func) {
        window.addEventListener(evname, function(e) {
            console.log(e.type, e.detail);
            if (func) {
                func(e);
            }
        });
    };

    // var mixpanel_track = function(event,param){
    //     var param = (param)? param : ''
    //     mixpanel.track(event, param)
    // }


    add_listener(actions.Health_Step_1_Submitted, function(e) {
        // mixpanel_track('Health Step 1 Submitted');
    });

    add_listener(actions.Health_Step_2_Submitted, function(e) {
        // mixpanel_track('Health Step 2 Submitted');
    });

    add_listener(actions.Health_Step_3_Submitted, function(e) {
        // mixpanel_track('Health Step 3 Submitted');
    });

    add_listener(actions.Health_Step_4_Submitted, function(e) {
       // mixpanel_track('Health Step 4 Submitted');
    });

    add_listener(actions.Health_Results_Reached, function(e) {
        // mixpanel_track('Health results Reached');
    });

    add_listener(actions.Health_Results_Buy, function(e) {
        // mixpanel_track('Health Results Buy');
    });

    add_listener(actions.Health_Proposer_1_Submit, function(e) {
        // mixpanel_track('Health Proposer 1 Submit');
    });

    add_listener(actions.Health_Proposer_2_Submit, function(e) {
        // mixpanel_track('Health Proposer 2 Submit');
    });

    add_listener(actions.Health_Proposer_3_Submit, function(e) {
        // mixpanel_track('Health Proposer 3 Submit');
    });

    add_listener(actions.Health_Proposer_4_Submit, function(e) {
        // mixpanel_track('Health Proposer 4 Submit');
    });

    add_listener(actions.Health_Payment_Gateway, function(e) {
        // mixpanel_track('Health Payment Gateway');
    });

    add_listener(actions.Health_Payment_Success, function(e) {
        // mixpanel_track('Health Payment Success');
    });

    add_listener(actions.Health_Payment_Faliure, function(e) {
        // mixpanel_track('Health Payment Faliure');
    });


    return {
        cust_ev: cust_ev
    };



});
