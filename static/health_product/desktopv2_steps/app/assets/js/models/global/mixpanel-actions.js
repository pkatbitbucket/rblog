define([], function() {

 	return {
		Health_LP_Submitted:'Health_LP_Submitted',
		Health_Step_1_Submitted:'Health_Step_1_Submitted',
		Health_Step_2_Submitted:'Health_Step_2_Submitted',
		Health_Step_3_Submitted:'Health_Step_3_Submitted',
		Health_Step_4_Submitted:'Health_Step_4_Submitted',
		Health_Results_Reached:'Health_Results_Reached',
		Health_Results_Buy:'Health_Results_Buy',
		Health_Proposer_1_Submit:'Health_Proposer_1_Submit',
		Health_Proposer_2_Submit:'Health_Proposer_2_Submit',
		Health_Proposer_3_Submit:'Health_Proposer_3_Submit',
		Health_Proposer_4_Submit:'Health_Proposer_4_Submit',
		Health_Payment_Gateway:'Health_Payment_Gateway',
		Health_Payment_Success:'Health_Payment_Success',
		Health_Payment_Faliure:'Health_Payment_Faliure',
 	}
})
