define(['knockout', 'user_info', 'medical', 'contact', 'utils', 'jquery', 'cookie', 'events', ], function(ko, user_info, medical, contact, utils, $, Cookie, events) {

    //  'mixpanel-actions' = actions

    // A list of all the steps. These would also be url fragments for the initial steps.
    var stage_hashes = ["search", "medical-conditions"];
    user_info.stage = 1;
    medical.stage = 2;

    // add window.replaceHash --> update hash without new entry in history
    (function(namespace) { // Closure to protect local variable "var hash"
        if ('replaceState' in history) {
            namespace.replaceHash = function(newhash) {
                if ((''+newhash).charAt(0) !== '#') newhash = '#' + newhash;
                history.replaceState('', '', newhash);
            };
        }
        else {
            var hash = location.hash;
            namespace.replaceHash = function(newhash) {
                if (location.hash !== hash) history.back();
                location.hash = newhash;
            };
        }
    })(window);


    function set_current_stage_no(stage_no) {
        var stage_hash = stage_hashes[stage_no - 1];
        set_current_stage(stage_hash);
    }
    function set_current_stage(stage_hash) {
        var stage_id = stage_hashes.indexOf(stage_hash);
        if((stage_id == -1) ||
           (stage_id == 1 && !user_info.validate()))
        {
            stage_id = 0;
            window.replaceHash(stage_hashes[0]);
            // events.cust_ev(actions.Health_Step_1_Submitted,{});
        }
        ko.router.navigate(stage_hashes[stage_id]);
        current_stage(stage_id + 1);
        window.scrollTo(0, 0);
    }

    function topup_info() {
        var self = this;
        var topup_video_src = 'https://www.youtube.com/embed/mFrU-PkFzME?rel=0';
        self.topup_video_src = ko.observable();
        self.visible = ko.observable(false);
        var hide_on_escape = function(e) {
            var keyCode = e.keyCode || e.which;
            if(keyCode == 27) self.hide();
        };
        self.show = function() {
            document.body.style.overflow = "hidden";
            // load video only if pop-up is opened
            if(!self.topup_video_src()) {
                self.topup_video_src(topup_video_src);
            }
            self.visible(true);
            if(document.addEventListener)
                document.addEventListener('keydown', hide_on_escape);
        };
        self.hide = function() {
            self.visible(false);
            document.body.style.overflow = "inherit";
            if(document.removeEventListener)
                document.removeEventListener('keydown', hide_on_escape);
        };
    }

    var current_stage = ko.observable();

    current_stage.subscribe(function(val){
        if (val == 1){
            window.dataLayer.push({
                'event': 'virtual_page_view',
                'healthvpv': '/vp/health/step1',
            });
        }
        else if(val == 2){
            window.dataLayer.push({
                'event': 'healthvpvmedicalconditions',
                'healthvpv': '/vp/health/step2/medical_conditions',
            });
        }
    });

    function post_to_lms() {
        $.post("/leads/save-call-time/",
               {'data' : ko.toJSON(utils.post_data()), 'csrfmiddlewaretoken' : CSRF_TOKEN},
               function(data){});
    }

    function next_stage() {
        if(current_stage() === 1 && user_info.validate()) {
            set_current_stage_no(current_stage() + 1);
            utils.notify("Members - " + utils.member_verbose(user_info.getSelectedMembers(), true));
            utils.notify("Product type - " + ko.unwrap(user_info.product_type));
            utils.notify("Deductible - " + ko.unwrap(user_info.deductible));
            utils.notify("User Pincode - " + ko.unwrap(user_info.pincode));
            utils.notify("Parents' Pincode - " + ko.unwrap(user_info.parents_pincode));
            if(user_info.email())
                post_to_lms();  // email will be saved from cookies
        }
    }

    function prev_stage() {
        var productType = JSON.parse(localStorage.Health_form_new).product_type
        if(productType=='SUPER_TOPUP'){
            window.location = '/health-insurance/super-topup'
        }else{
            window.location = '/health-insurance'
        }
        // if(current_stage() > 1)
        //     set_current_stage_no(current_stage()-1);
    }

    function get_quotes(){
        var valid = user_info.validate();
        medical.quotes_called(true);
        valid = valid && medical.validate();
        utils.notify("Medical Conditions: " + ko.unwrap(medical.medical_conditions_verbose));
        if(valid){
            // Mix Panel Event Trigger
            // events.cust_ev(actions.Health_Step_2_Submitted,{});
            if(user_info.email())
                post_to_lms();  // email will be saved from cookies
            $.post(
                   '/health-plan/save-details/results/?hot=true',
                   {'csrfmiddlewaretoken' : CSRF_TOKEN, 'data': ko.toJSON(utils.post_data())},
                   function(res) {
                       var activity_id = JSON.parse(res).activity_id;
                       window.location.href = 'results/' + '#' + activity_id;
                   }
            );
        }
        medical.quotes_called(false);
    }
    medical.go_to_results.subscribe(function(val) {
        if(val) get_quotes();
    });

    var CustomAlertMsg = function(){
        var self = this;
        self.visible = ko.observable(false);
        self.name = '';
        self.close = function(){
            self.visible(false);
            Cookie.expire('show_custom_alert');
        };
    };
    var custom_alert_msg = new CustomAlertMsg();
    if(Cookie.get('mobileNo') && Cookie.get('show_custom_alert') == 'True'){
        custom_alert_msg.visible(true);
    }



    var RManager = function(){
        var self = this;
        self.visible = ko.observable(false);
        self.name = '';
        self.close = function(){
            self.visible(false);
            Cookie.expire('relmanager');
        };
    };
    var rmanager = new RManager();
    if(Cookie.get('mobileNo') && Cookie.get('relmanager')){
        rmanager.visible(true);
        rmanager.name = Cookie.get('relmanager');
        utils.notify(Cookie.get('relmanager') + " needs to call this customer on " + Cookie.get('mobileNo'));
    }

    return {
        initialize : function(stage_hash) {
            set_current_stage(stage_hash);
            var T2 = new Date();
            var page_load_time = parseInt((T2 - T1)/1000);
            utils.load_time('ApplicationPageLoading', 'v1', page_load_time, 'CFSTATIC');
            return {
                user_info: user_info,
                medical: medical,
                contact: contact,
                utils: utils,
                current_stage: current_stage,
                next_stage: next_stage,
                prev_stage: prev_stage,
                get_quotes: get_quotes,
                custom_alert_msg : custom_alert_msg,
                health_toll_free: health_toll_free,
                topup_info:  new topup_info(),
            };
        },
    };
});
