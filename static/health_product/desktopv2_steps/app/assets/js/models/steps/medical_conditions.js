define(['knockout', 'user_info', 'utils','jquery', 'chosen'], function(ko, user_info, utils, $) {
    var go_to_results = ko.observable();

    ko.bindingHandlers.chosen = {
        init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
            // ko.bindingHandlers.options.init.apply(this, arguments);
            $elem = $(element);
            options = {
                width: '100%',
                search_contains: true,
            };
            $elem.chosen(options);
            if(allBindings.has('callback'))
                $elem.on('change', allBindings.get('callback'));
        },
        update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
            valueAccessor()();
            $(element).trigger('chosen:updated');
        }
    };

    function MedicalCondition(data) {
        var self = this;
        self.id = data.id;
        self.medical_condition = data.medical_condition;
        self.transaction_permission = data.permission;
        self.frequency = data.frequency;
        self.selected = ko.observable(false);
        self.suffering_members = ko.computed(function() {
            if(!self.transaction_permission && self.selected()) {
                return ko.utils.arrayMap(user_info.getSelectedMembers(), function(mem) {
                    return new UserMedical(mem);
                });
            }
        }).extend({
            validation: {
                validator: function(members) {
                    return ko.utils.arrayFilter(members, function(med_mem) {
                        return med_mem.selected();
                    }).length > 0;
                },
                message: "Please select atleast one member"
            }
        });
        // suppress the errors unless manually validated
        self.suffering_members.subscribe(function() {
            self.suffering_members.isModified(false);
        });
        self.validate = function() {
            self.suffering_members.isModified(true);
            return !self.suffering_members.error();
        };
    }

    function UserMedical(member) {
        var self = this;
        self.member = member;
        self.selected = ko.observable();
        self.toggleSelect = function() {
            self.selected(!self.selected());
        };
    }

    function MedicalConditionsViewModel() {
        var self = this;
        self.quotes_called = ko.observable(false);
        self.has_disease = ko.observable('');
        self.has_disease.subscribe(function(new_value){
            if(new_value == 'no'){
                if(all_meds){
                    ko.utils.arrayForEach(all_meds, function(med){
                        med.selected(false);
                    });
                }
                if(self.selected_rare_meds){
                    self.selected_rare_meds.removeAll();
                }
            }
        });
        self.has_disease.extend({
            lstore: 'has_disease'
        });
        self.go_to_results = go_to_results;
        self.frequent_meds = [];
        self.rare_meds = [];
        self.meds_map = {};
        ko.utils.arrayForEach(medical_conditions, function(d) {
            var med = new MedicalCondition(d);
            self.meds_map[med.id] = med;
            if(med.frequency == 'HIGH')
                self.frequent_meds.push(med);
            else
                self.rare_meds.push(med);
        });
        var all_meds = self.frequent_meds.concat(self.rare_meds);
        self.selected_frequent_meds = ko.computed(function() {
            return ko.utils.arrayFilter(self.frequent_meds, function(d) {
                return d.selected();
            });
        }).extend({
            throttle: 50,
            store: {
                key: 'med_freq',
                dump: function(nval) {
                    nval = nval || [];
                    var selected_ids = [];
                    for(var i=0; i<nval.length; i++)
                        selected_ids.push(nval[i].id);
                    return ko.toJSON(selected_ids);
                },
                init: function(obj, vals) {
                    ko.utils.arrayForEach(vals, function(val){
                        var d = ko.utils.arrayFirst(self.frequent_meds, function(d) {
                            return val == d.id;
                        });
                        if(d) d.selected(true);
                    });
                }
            },
        });
        self.selected_rare_meds = ko.observableArray().extend({
            store: {
                key: 'med_rare',
                init: function(obj, med_ids) {
                    ko.utils.arrayForEach(med_ids, function(id) {
                        self.meds_map[id].selected(true);
                    });
                    obj(med_ids);
                }
            }
        });
        // Handle selection from jquery-chosen
        self.rare_meds_event = function(evt, params) {
            if(params.deselected)
                self.meds_map[params.deselected].selected(false);
            else
                self.meds_map[params.selected].selected(true);
        };
        self.selected_meds = ko.computed(function() {
            return ko.utils.arrayFilter(all_meds, function(med) {
                return med.selected();
            });
        });
        self.selected_meds.subscribe(function(meds){
            if(meds && meds.length == 0 && self.quotes_called()){
                self.has_disease('no');
            }
        })
        self.medical_conditions_verbose = ko.computed(function() {
            return ko.utils.arrayMap(self.selected_meds(), function(med) {
                return med.medical_condition;
            });
        });
        self.medical_conditions = ko.computed(function() {
            return ko.utils.arrayMap(self.selected_meds(), function(med) {
                return med.id;
            });
        });
        utils.medical_data.medical_conditions_verbose = self.medical_conditions_verbose;
        utils.medical_data.medical_conditions = self.medical_conditions;
        self.blocker_meds = ko.computed(function() {
            return ko.utils.arrayFilter(self.selected_meds(), function(d) {
                return !d.transaction_permission;
            });
        });

        self.members_med_popup = ko.observable();
        self.continue_popup = ko.observable();
        self.optimize_popup = ko.computed(function() {
            return self.members_med_popup() || self.continue_popup();
        });
        self.hide_popups = function() {
            self.members_med_popup(false);
            self.continue_popup(false);
        };
        self.hide_popup_esc =function(data, event){
            if(event.keyCode === 27){
            self.members_med_popup(false);
            self.continue_popup(false);
            }
        };

        self.optimize_step1 = function() {
            $('.call_back_popup').focus();
            if(user_info.getSelectedMembers().length > 1){
                self.members_med_popup(true);
            }
            else {
                var mem = user_info.getSelectedMembers()[0];
                ko.utils.arrayForEach(self.blocker_meds(), function(med) {
                    med.suffering_members()[0].selected(true);
                });
                self.optimize_step2();
            }
        };
        self.optimize_step2 = function() {
            var valid_step1 = ko.utils.arrayFilter(self.blocker_meds(), function(med) {
                return !med.validate();
            }).length === 0;
            if(valid_step1) {
                $('.call_back_popup').focus();
                self.members_med_popup(false);
                self.continue_popup(true);
            }
        };
        self.valid_members = ko.observable();
        self.invalid_members = ko.computed(function() {
            var invalid_members = [];
            ko.utils.arrayForEach(self.blocker_meds(), function(med) {
                ko.utils.arrayForEach(med.suffering_members() || [], function(mem_med) {
                    if(mem_med.selected() && invalid_members.indexOf(mem_med.member) == -1)
                        invalid_members.push(mem_med.member);
                });
            });
            var valid_kids = 0, valid_you_spouse = 0;
            var valid_members = ko.utils.arrayFilter(user_info.getSelectedMembers(), function(mem) {
                var valid = (invalid_members.indexOf(mem) == -1);
                if(valid) {
                    if(mem.type == 'kid')
                        valid_kids++;
                    if(mem.id == 'you' || mem.id == 'spouse')
                        valid_you_spouse++;
                }
                return valid;
            });
            if(valid_kids > 0 && valid_you_spouse === 0) {
                var new_valid_members = [];
                ko.utils.arrayForEach(valid_members, function(mem) {
                    if(mem.type == 'kid')
                        invalid_members.push(mem);
                    else
                        new_valid_members.push(mem);
                });
                valid_members = new_valid_members;
            }

            self.valid_members(valid_members);
            return invalid_members;
        });
        self.invalid_members_verbose = ko.computed(function() {
            return utils.member_verbose(self.invalid_members());
        });
        self.valid_members_verbose = ko.computed(function() {
            if(self.valid_members().length == 1)
                return utils.member_verbose(self.valid_members());
            else
                return self.valid_members().length + " members";
        });
        self.valid_disease_selected = ko.computed(function(){
           if(self.quotes_called() && self.selected_meds().length == 0 && self.has_disease() == 'yes'){
                return false;
            }
            else {
                return true;
            }
        });

        //***************************** REACT PART *******************************//
        self.handleReactJSON = function(invalid_members){
            var healthStore = JSON.parse(localStorage.Health_form_new);
            var reactMembersJSON = healthStore.members;
            for(var i = invalid_members.length-1; i>=0; i--){
                var member =  invalid_members[i];
                for(var j=0; j<reactMembersJSON.length; j++){

                    var reactJSONMember = reactMembersJSON[j];
                    var age = reactJSONMember.age+'-'+reactJSONMember.age_in_months;

                    if(reactJSONMember.person == member.person && age==member.age() && reactJSONMember.type==member.type){
                        reactMembersJSON.splice(j,1)
                        break;
                    }
                }
            }

            var memberCategoriesToReset = healthStore.members_state
            var gender =  healthStore.gender

            // Reset Member Categories
            var memberCategories = {}
            Object.keys(memberCategoriesToReset).map(function(memberCategory){
                    var memberState = memberCategoriesToReset[memberCategory];
                    memberState.checked = false
                    memberState.count = 0
                    memberCategories[memberCategory] = memberState;
            })

            // Set Spouse displayName
            if(memberCategories['spouse'] && gender!=undefined){
                memberCategories['spouse'].displayName = (gender=='MALE')? 'Your Wife':'Your Husband'
            }

            reactMembersJSON.map(function(element){

                if(memberCategories[element.personAlias]){
                    memberCategories[element.personAlias].checked = true
                    memberCategories[element.personAlias].count = memberCategories[element.personAlias].count+1
                }

            })


            healthStore.members = reactMembersJSON
            localStorage.setItem('Health_form_new', JSON.stringify(healthStore));

        }
        //***************************** REACT PART *******************************//

        self.continue_with_valid_members = function() {
            self.quotes_called(true);
            self.continue_popup(false);
            var invalid_members = self.invalid_members();
            for(var i = invalid_members.length-1; i>=0; i--)
                invalid_members[i].selected(false);

            self.handleReactJSON(invalid_members)


            ko.utils.arrayForEach(self.blocker_meds(), function(med) {
                med.selected(false);
            });
            var selected_rare_meds = ko.utils.arrayFilter(self.rare_meds, function(med) {
                return med.selected();
            });
            self.selected_rare_meds(ko.utils.arrayMap(selected_rare_meds, function(med) {
                return med.id;
            }));
            go_to_results(true);
        };

        self.validate = function() {
            if((self.selected_meds().length == 0 && self.has_disease() == 'yes') || self.has_disease() == ''){
                return false;
            }
            if(self.blocker_meds().length === 0)
                return true;
            self.optimize_step1();
            return false;
        };
    }

    return new MedicalConditionsViewModel();
});
