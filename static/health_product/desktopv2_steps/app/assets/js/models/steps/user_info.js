define(['knockout', 'pincheck', 'jquery', 'knockout_validation'], function(ko, pincheck, $) {

    ko.validation.configure({
        errorMessageClass: 'error',
        decorateElement : true,
        errorElementClass: 'has-error',
        insertMessages: false
    });

    var MAX_KIDS = 7;
    var KIDS_AGE_LIST = [];
    var ADULTS_AGE_LIST = [];
    (function() {
        /* Fills KIDS_AGE_LIST and ADULTS_AGE_LIST
         * in the format [{id: '0-2', name: '2 months'}, {id: '25-0', name: '25 years'}] */

        var i, max_kid_age = 25, min_adult_age=18, max_adult_age=100;
        function ko_format(age_in_years, age_in_months) {
            var age_code = get_age_code(age_in_years, age_in_months);
            return {id: age_code, name: get_display_age(age_code)};
        }
        for(i=0; i<12; i++)
            KIDS_AGE_LIST.push(ko_format(0, i));
        for(i=1; i<=max_kid_age; i++)
            KIDS_AGE_LIST.push(ko_format(i, 0));
        for(i=min_adult_age; i<=max_adult_age; i++)
            ADULTS_AGE_LIST.push(ko_format(i, 0));
    })();
    function get_age_code(age_in_years, age_in_months) {
        if([undefined, null, ''].indexOf(age_in_years) > -1 ||
           [undefined, null, ''].indexOf(age_in_months) > -1)
            return '';
        return age_in_years + '-' + age_in_months;
    }
    function get_age_in_years_months(age_code) {
        if(!age_code) return [undefined, undefined];
        var split_age = age_code.split('-');
        var age_in_years = parseInt(split_age[0]);
        var age_in_months = parseInt(split_age[1]);
        return [age_in_years, age_in_months];
    }
    function get_display_age(age) {
        if(!age) return '';
        var split_age = get_age_in_years_months(age);
        var age_in_years = split_age[0];
        var age_in_months = split_age[1];
        var val = (age_in_years > 0) ? age_in_years : age_in_months;
        var unit = (age_in_years > 0) ? 'year' : 'month';
        unit = unit + (val == 1 ? '' : 's');
        var display_age = val + ' ' + unit;
        return display_age;
    }

    // TODO: fetch available deductibles from DB
    function get_deductible_options() {
        return [
            {name: '1 Lac', id: 100000},
            {name: '2 Lacs', id: 200000},
            {name: '3 Lacs', id: 300000},
            {name: '4 Lacs', id: 400000},
            {name: '5 Lacs', id: 500000},
            {name: '6 Lacs', id: 600000},
            {name: '7 Lacs', id: 700000},
            {name: '8 Lacs', id: 800000},
            {name: '9 Lacs', id: 900000},
            {name: '10 Lacs', id: 1000000}
        ];
    }

    function Member(type, selected, person, displayName) {
        var self = this;
        self.selected = ko.observable(selected);
        self.person = person;
        self.id = self.person.toLowerCase();
        self.type = type;
        self.displayName = displayName || person;
        self.age_in_years = ko.observable();
        self.age_in_months = ko.observable();
        self.ageList = (type == 'kid') ? KIDS_AGE_LIST : ADULTS_AGE_LIST;
        self.age = ko.computed({
            read: function() {
                return get_age_code(self.age_in_years(), self.age_in_months());
            },
            write: function(val) {
                var split_age = get_age_in_years_months(val);
                self.age_in_years(split_age[0]);
                self.age_in_months(split_age[1]);
            }
        }).extend({
            required: {
                onlyIf: self.selected,
                message: 'Please select age of insured member'
            }
        });
        self.display_age = ko.computed(function(){ 
            if(self.age()){
                var obj = ko.utils.arrayFirst(self.ageList, function(a) {
                    return a.id == self.age();
                });
                if(obj){
                    return obj.name;
                }
            }
            return '';
        });
        self.vperson = ko.computed(function() {
            if(self.type == 'adult')
                return ko.unwrap(self.displayName);
            else
                return ko.unwrap(self.displayName) + ' (' + self.display_age() + ')';
        });
        self.toggleSelect = function () {
            self.selected(!self.selected());
        };
    }

    function SearchViewModel() {
        var self = this;

        self.email = ko.observable().extend({
            email: true,
            store: {key: 'email'}
        });
        self.product_type = ko.observable('INDEMNITY').extend({
            lstore: {key: 'product_type'}
        });
        self.is_topup = ko.computed(function(){
            return ['TOPUP', 'SUPER_TOPUP'].indexOf(self.product_type()) !== -1;
        });

        self.deductible = ko.observable().extend({
            lstore: {key: 'deductible'},
            required: {
                onlyIf: self.is_topup,
                message: "Please enter the existing sum assured"
            }
        });
        self.deductible_options = get_deductible_options();

        self.gender = ko.observable().extend({
            lstore : {key : 'gender'},
            required: {
                params:true,
                message: "Please select your gender"
            }
        });

        self.max_kids = MAX_KIDS;
        self.you = new Member('adult', false, 'You');
        self.spouse = new Member('adult', false, 'Spouse', ko.computed(function(){
            switch(self.gender()) {
                case 'MALE': return 'Your Wife';
                case 'FEMALE': return 'Your Husband';
                default: return 'Your Spouse';
            }
        }));
        self.father = new Member('adult', false, 'Father');
        self.mother = new Member('adult', false, 'Mother');
        var user_family = [self.you, self.spouse];
        var parents = [self.father, self.mother];
        var adults = user_family.concat(parents);

        self.addKid = function(person) {
            var kid = new Member('kid', true, person);
            var no_of_kids = ko.utils.arrayFilter(self.members(), function(member) {
                return member.person == person;
            }).length;
            kid.id = person.toLowerCase() + (no_of_kids + 1);
            self.members.push(kid);

            // remove the kid when deselected
            kid.selected.subscribe(function(val) {
                self.members.remove(kid);
            });

            return kid;
        };

        // pass copy of adults, else kids will be added to this same array
        self.members = ko.observableArray(adults.slice());
        self.members.extend({
            lstore : {
                key : 'members',
                dump: function(nval){
                    var mList = [];
                    ko.utils.arrayForEach(nval, function(m){
                        if(m.selected()) {
                            mList.push({
                                age: m.age_in_years()+'-'+m.age_in_months(),
                                person: m.person
                            });
                        }
                    });
                    return ko.toJSON(mList);
                },
                init: function(obj, mList) {
                    try {
                    ko.utils.arrayForEach(mList, function(m){
                        if(m.person == 'son' || m.person == 'daughter') {
                            var kid = self.addKid(m.person, obj);
                            kid.age(m.age);
                        }
                        else {
                            var mem = ko.utils.arrayFirst(obj(), function(mem) {
                                return mem.person == m.person;
                            });
                            mem.age(m.age);
                            mem.selected(true);
                        }
                    });
                    }
                    catch(e) {}
                }
            }
        });
        self.getSelectedMembers = ko.computed(function(){
            return ko.utils.arrayFilter(self.members(), function(member) { return member.selected();});
        });
        self.getSelectedKids = ko.computed(function(){
            return ko.utils.arrayFilter(self.members(), function(member) { return member.type == 'kid';});
        });
        if(self.getSelectedMembers().length === 0 )
            self.you.selected(true);

        self.member_text = ko.computed(function(){
            if(self.getSelectedMembers().length > 1){
                return 'any of the members';
            } else {
                var member = self.getSelectedMembers()[0]
                if(member){
                    if(member.id == 'you'){
                        return 'you';
                    }
                    else {
                        return ("your " + member.id)
                    }
                }
            }
        });
        self.family_text = ko.computed(function(){
            if(self.getSelectedMembers().length > 1){
                return " family's";
            } else {
                var member = self.getSelectedMembers()[0]
                if(member){
                    if(member.id == 'you'){
                        return '';
                    }
                    else {
                        return " " + member.id + "'s"
                    }
                }
            }
        })

        self.cumulativeError = ko.observable().extend({
            validation: [
                {
                    'validator' : function(obj){
                        return ko.utils.arrayFilter(self.members(), function(m) {
                            return m.selected();
                        }).length > 0;
                    },
                    'message' : 'Select at least one member to insure'
                },
                {
                    'validator' : function(obj){
                        return self.getSelectedMembers().length > self.getSelectedKids().length;
                    },
                    'message' : 'Select at least one adult to insure'
                },
                {
                    validator: function(obj){
                        return ko.utils.arrayFilter(self.getSelectedMembers(), function(m) {
                            return !m.age();
                        }).length === 0;
                    },
                    'message':"Please select respective member's age"
                },
                {
                    validator: function(obj) {
                        var valid =  (self.getSelectedKids().length === 0 ||
                                      (self.you.selected() || self.spouse.selected())
                                      );
                        if(self.getSelectedKids().length == 1)
                            this.message = "Please select one of the kid's parent";
                        else
                            this.message = "Please select one of kids' parent";
                        return valid;
                    }
                }
            ]
        });

        self.user_pin_required = ko.computed(function() {
            var pin_required = false;
            ko.utils.arrayForEach(user_family, function(mem) {
                pin_required = pin_required || mem.selected();
            });
            pin_required = pin_required || (self.members().length > adults.length); // if kids selected;
            return pin_required;
        });
        self.parents_pin_required = ko.computed(function() {
            var pin_required = false;
            ko.utils.arrayForEach(parents, function(mem) {
                pin_required = pin_required || mem.selected();
            });
            return pin_required;
        });

        self.pincode = ko.observable().extend({
            lstore : {key : 'pincode'},
            required: {
                onlyIf: self.user_pin_required,
                message: "Please select your area pincode"
            },
            validation : {
                validator : pincheck.isValid,
                message : "Please enter a valid pincode"
            }
        });
        self.parents_pincode = ko.observable().extend({
            lstore : {key : 'parents_pincode'},
            required: {
                onlyIf: self.parents_pin_required,
                message: "Please select your parents' area pincode"
            },
            validation : {
                validator : pincheck.isValid,
                message: "Please enter a valid pincode"
            }
        });

        /* Find better logic when to use this,
         * else data will be autofilled from activity on refresh.
         * Maybe try passing activity id from the results page
        // Load data from activity if available
        var initialize_members = function(obj, members) {
            // reverse iteration because kids are popped out when de-selected
            for(var i = obj().length-1; i>=0; i--)
                obj()[i].selected(false);

            // temporary try-catch
            // members cookie format changed, will raise error if user has old cookie stored
            try {
            ko.utils.arrayForEach(members, function(m){
                var age = get_age_code(m.age, m.age_in_months);
                if(m.type=='adult') {
                    var mem = ko.utils.arrayFirst(obj(), function(mem) {
                        return mem.person == m.person;
                    });
                    mem.age(age);
                    mem.selected(true);
                }
                else {
                    var kid = self.addKid(m.person, obj);
                    kid.age(age);
                }
            });
            } catch(e){}
        };
        $.get(
            '/health-plan/get-ui-data/',
            function(res) {
                var json_data = JSON.parse(res);
                if(json_data) {
                    for(var k in json_data) {
                        if(k == 'members') {
                            initialize_members(self.members, json_data[k]);
                        }
                        else {
                            self[k](json_data[k]);
                        }
                    }
                }
            }
        );
        */

        var errors = ko.validation.group([self.email, self.members, self.gender, self.deductible,
                                          self.cumulativeError, self.pincode, self.parents_pincode]);
        self.validate = function() {
            self.members.valueHasMutated();
            valid = true;
            if(errors().length > 0) {
                errors.showAllMessages(true);
                valid = false;
            }
            return valid;
        };
    }

    var searchViewModelInstance = new SearchViewModel();
    return searchViewModelInstance;
});
