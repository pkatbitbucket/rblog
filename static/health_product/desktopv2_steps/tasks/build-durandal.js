﻿module.exports = function (grunt) {
    grunt.registerMultiTask('durandal', "Grunt Durandal Builder - Build durandal project using a custom require config and a custom almond", function () {
        //#region Global Properties
        var _ = grunt.util._,
            path = require('path'),
            requirejs = require('requirejs'),

            extensions = {
                script: ".js",
                view: ".html",
                all: [".js", ".html"]
            };

            defaultRequireConfig = {
                name: 'lib/require/almond-custom',
                inlineText: true,
                stubModules: ['../app/lib/require/text'],

                paths: {
                    'text': 'lib/require/text',
                    'knockout': 'lib/knockout/knockout-3.3.0',
                    'knockout_validation': 'lib/knockout/knockout.validation',
                    'knockout-amd-helpers': 'lib/knockout/knockout-amd-helpers',
                    'knockout-history': 'lib/knockout/knockout-history',
                    'knockout-router': 'lib/knockout/knockout-router',
                    'login': 'assets/js/models/global/login',
                    'utils': 'assets/js/models/global/utils',
                    'user_info': 'assets/js/models/steps/user_info',
                    'medical': 'assets/js/models/steps/medical_conditions',
                    'contact' : 'assets/js/models/global/contact',
                    'cookie' : 'lib/cookies/cookies',
                    'bstore' : 'assets/js/scripts/bstore',
                    'events': 'assets/js/models/global/events',
                    'mixpanel-actions' :'assets/js/models/global/mixpanel-actions',
                    'pincheck' : 'assets/js/scripts/pincheck',
                    'custom' : 'assets/js/scripts/custom',
                    'jquery' : 'lib/jquery/jquery-1.11.0',
                    'chosen' : 'lib/jquery/chosen/chosen.jquery-1.4.2',
                    'main' : 'assets/js/main',
                    'config' : 'assets/js/config',
                    'steps/index': 'app/assets/js/models/steps/index',
                },
                findNestedDependencies: true,
                preserveLicenseComments: false,
                keepBuildDir: true,
                optimize: "uglify2",
                uglify: {
                    options: {
                        compress: {
                            drop_console: true
                        }
                    },
                },
                //generateSourceMaps: true,
                //optimize: "none",
                pragmas: {
                    build: true
                },
                wrap: true,
                shim: {
                    'knockout_validation' : {
                        deps : ['knockout']
                    },
                    'knockout-amd-helpers': {
                        deps: ['knockout']
                    },
                    'knockout-history': {
                        deps: ['knockout']
                    },
                    'knockout-router': {
                        deps: ['knockout', 'knockout-history', 'knockout-amd-helpers']
                    },
                    'chosen': {
                        deps: ['jquery']
                    },
                },
            };

        //#endregion

        //#region Private Methods

        function ensureRequireConfig(params) {
            if (params.includeMain)
                params.insertRequire.push("config");//TODO: DOUBT if this is main

            params.insertRequire = _.uniq(params.insertRequire);
            params.includes = _.uniq(params.includes);
            params.excludes = _.uniq(params.excludes);

            if (params.paths)
                params.paths = _.extend({}, defaultRequireConfig.paths, params.paths);

            if (params.pragmas)
                params.pragmas = _.extend({}, defaultRequireConfig.pragmas, params.pragmas);
        }

        function includePath(array, config, url) {
            if (url.indexOf(config.out) !== -1 || url.indexOf("durandal/amd") !== -1)
                return;

            var ext = path.extname(url);
            url = path.relative(config.baseUrl, url);
            url = url.replace(/\\/g, "/");

            var pathToReplace = _.chain(config.paths)
                                .map(function (_path, key) { return { key: key, path: _path }; })
                                .filter(function (_path) { return url.indexOf(_path.path) !== -1; })
                                .max(function (_path) { return _path.path.length; })
                                .value();

            if (pathToReplace)
                url = url.replace(pathToReplace.path, pathToReplace.key);

            if (ext === ".html") {
                url = "text!" + url;
            }
            else if (ext === ".js") {
                url = url.replace(new RegExp("\\" + ext + "$"), "");
            }
            else
                return;

            array.push(url);
        }

        //#endregion
        var done = this.async(),
            config,
            params = this.options({
                baseUrl: "app",
                out: "build/js/main-built.js",
                mainPath: "config.js",
                include: [],
                exclude: [],
                insertRequire: [],
                loglevel: 4,
                includeMain: true,
            });

        ensureRequireConfig(params);
        config = _.extend({}, defaultRequireConfig, params);

        this.files.forEach(function (file) {
            file.src.forEach(_.partial(includePath, config.include, config));
        });

        grunt.log.ok("+++++++++++++++++++++++++++", config);

        requirejs.optimize(
            config,
            function (response) {
                if (params.loglevel === "verbose")
                    grunt.log.write(response);

                grunt.log.ok(params.out + " created !");

                done(true);
            },
            function (error) {
                grunt.log.error(error);

                done(false);
            }
        );
    });
};
