module.exports = function (grunt) {
    grunt.initConfig({
        copy: {
            images_main: {
                src: '../img/**',
                dest: '../build/img/',
                expand : true
            },
            fonts : {
                src : '../fonts/*',
                dest : '../build/fonts/'
            }

        },
        concat : {
            css_main_product : {
                src : [
                    '../css/bootstrap.css',
                    '../css/medical_screen.css',
                    '../css/new_buy.css',
                    '../css/jquery.datepick.css',
                    '../css/style.css',
                    '../../css/user-logged.css'
                ],
                dest : '../build/css/main-built-concat.css'
            },

        },
        cssmin : {
            css_main_product : {
                src : '../build/css/main-built-concat.css',
                dest : '../build/css/main-built.css'
            },
        },
        buy: {
            dist: {
                src: [
                    '../../../static/health_product/js/lib/knockout/knockout-3.3.0',
                    '../../../static/health_product/js/lib/require/text',
                    '../../../static/health_product/js/lib/knockout/knockout.validation',
                    '../../../static/health_product/js/lib/knockout/ko.observableDictionary',
                    '../../../static/health_product/js/lib/jquery/jquery-1.11.0',
                    '../../../static/health_product/js/lib/jquery/jquery.plugin',
                    '../../../static/health_product/js/lib/jquery/jquery.datepick-5.0.0',
                    '../../../static/health_product/js/scripts/jdatepicker',
                    '../../../static/health_product/js/scripts/custom',
                    '../../../static/health_product/js/app/viewmodels/utils',
                    '../../../static/health_product/js/lib/bootstrap/tooltip',
                    '../../../static/health_product/js/lib/crossroads/signals',
                    '../../../static/health_product/js/lib/crossroads/hasher',
                    '../../../static/health_product/js/app/viewmodels/age_offline',
                    '../../../static/health_product/js/app/viewmodels/medical_case',
                    '../../../static/health_product/js/insurers/*.*',
                ],
            }
        },
        uglify : {
            options: {
                compress: {
                    drop_console: true
                }
            },
        }
    });

    grunt.loadTasks('tasks');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-newer');

    grunt.registerTask('default', [
        'newer:concat:css_main_product',
        'newer:cssmin:css_main_product',
        'newer:copy',
        'buy',
    ]);
};
