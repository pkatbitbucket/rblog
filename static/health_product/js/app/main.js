requirejs.config({
    paths: {
        'async': '/static/health_product/js/lib/requirejs-plugins/async',
        //'gmaps' : '/static/health_product/js/scripts/gmaps',
        'gmaps': '/static/health_product/js/scripts/gmaps-stub',
        'text': '/static/health_product/js/lib/require/text',
        'durandal':'/static/health_product/js/lib/durandal/js',
        'plugins' : '/static/health_product/js/lib/durandal/js/plugins',
        'transitions' : '/static/health_product/js/lib/durandal/js/transitions',
        //'knockout': '/static/health_product/js/lib/knockout/knockout-3.0.0.debug',
        'knockout': '/static/health_product/js/lib/knockout/knockout-3.0.0',
        'knockout_validation': '/static/health_product/js/lib/knockout/knockout.validation',
        'knockout_dictionary': '/static/health_product/js/lib/knockout/ko.observableDictionary',
        'bootstrap-tooltip': '/static/health_product/js/lib/bootstrap/tooltip',
        'bootstrap-transition': '/static/health_product/js/lib/bootstrap/transition',
        'bootstrap-collapse': '/static/health_product/js/lib/bootstrap/collapse',
        'bootstrap-dropdown': '/static/health_product/js/lib/bootstrap/dropdown',
        'jquery': '/static/health_product/js/lib/jquery/jquery-1.11.0',
        'amplify': '/static/health_product/js/lib/amplify/amplify',
        'scripts' : '/static/health_product/js/scripts',
        'jquerytransit' : '/static/health_product/js/lib/jquery/transit',
        'jquerypep' : '/static/health_product/js/lib/jquery/jquery.pep',
        'jquerycookie' : '/static/health_product/js/lib/jquery/jquery.cookie',
        'typeahead' : '/static/health_product/js/lib/jquery/typeahead',
        'response' : '/static/health_product/js/lib/zlib/inflate_stream'
    },
    shim: {
        'knockout_validation' : {
            deps : ['knockout']
        },
        'knockout_dictionary' : {
            deps : ['knockout'],
            exports : 'knockout_dictionary'
        },
        'bootstrap-tooltip': {
            deps: ['jquery'],
            exports: '$.fn.tooltip'
        },
        'bootstrap-collapse': {
            deps: ['jquery'],
            exports: '$.fn.collapse'
        },
        'bootstrap-transition': {
            deps: ['jquery'],
            exports: '$.fn.emulateTransitionEnd'
        },
        'bootstrap-dropdown': {
            deps: ['jquery'],
            exports: '$.fn.dropdown'
        },
        'jquerytransit' : {
            deps : ['jquery']
        },
        'jquerypep' : {
            deps : ['jquery']
        },
        'jquerycookie' : {
            deps : ['jquery']
        },
        'amplify' : {
            deps : ['jquery'],
            exports : 'amplify',
        },
        'typeahead' : {
            deps : ['jquery']
        },
    },
    urlArgs: 'v=1.0.0.0'
});

define(['durandal/system', 'durandal/app', 'durandal/viewLocator', 'scripts/custom', 'scripts/bage', 'scripts/bslider', 'scripts/bstore'],  function (system, app, viewLocator) {
    //>>excludeStart("build", true);
    system.debug(true);
    //>>excludeEnd("build");

    app.title = 'Coverfox Insurance';

    //specify which plugins to install and their configuration
    app.configurePlugins({
        router:true
    });

    app.start().then(function () {
        //Replace 'viewmodels' in the moduleId with 'views' to locate the view.
        //Look for partial views in a 'views' folder in the root.
        viewLocator.useConvention();

        //Show the app by setting the root view model for our application.
        app.setRoot('viewmodels/shell');
    });
});
