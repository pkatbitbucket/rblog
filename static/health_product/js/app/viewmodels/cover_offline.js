define(['jquery', 'knockout', 'viewmodels/global', 'viewmodels/offline_model', 'viewmodels/utils', 'knockout_validation'], function($, ko, global, offline_model, utils) {

    ko.validation.init({
        insertMessages: false,
        decorateInputElement : true,
        registerExtenders: true,
        messagesOnModified: true,
        messageTemplate: null,
        grouping: { deep: true }
    });

    var transaction_id = null;
    var initialize_UI = function(){
        $('html').css('overflow', 'auto');
        $('.side-nav').transition({'width' : 0}, function(){
            $('.side-nav').hide().css({'width' : ''});
        });
        $('#wrapper').transition({"padding-left":""});
        $("#medical_container").show();

    };

    contact_model = ko.observable();

    var contact_submit = function(){
        big_errors = ko.validation.group(contact_model(), { deep: true});
        if(big_errors().length > 0) {
            console.log(big_errors());
            big_errors.showAllMessages();
        }
        else{
            var data = ko.toJSON(contact_model());
            console.log('=======================DATA=========', data);
            $.post("/leads/save-call-time/", {'data' : data, 'csrfmiddlewaretoken' : CSRF_TOKEN}, function(data){
                if (data.success){
                    console.log('SUCCESS');
                    $('#before_submit').addClass('hide');
                    $('#after_submit').removeClass('hide');
                }
                else{
                    console.log('POST ERROR', data);
                }
            }, 'json');

        }
    };

    return {
        contact_model : contact_model,
        contact_submit : contact_submit,
        global : global,
        activate : function(txn_id){
            transaction_id = txn_id;
            if($("#buy_container").length > 0){
                initialize_UI();
            }
            var promise = 'Ok';
            var cmodel = new offline_model.contactData;
            cmodel.initialize();
            contact_model(cmodel);
            return promise;
        },
        attached : function(view, parent, settings){
            //console.log("View Loaded", $(view));
            initialize_UI();
        },
        deactivate : function(view, parent){
            $('.side-nav').show();
            $('#wrapper').transition({"padding-left":"250px"});
            $("#medical_container").hide();
        }
    }
});
