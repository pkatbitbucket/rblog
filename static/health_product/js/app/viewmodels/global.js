define(['knockout'], function(ko) {
    'use strict';

    var members = ko.observableArray([]).extend({store: "members"});
    var hospital_service = ko.observable("").extend({store: "hospital_service"});
    var pincode = ko.observable().extend({store: "pincode"});
    var hospitals = ko.observableArray([]).extend({store: "hospitals"});
    var city = ko.observable({}).extend({store: "city"});
    var gender = ko.observable("").extend({store: "gender"});
    var sum_assured = ko.observable().extend({ notify: 'always' });
    var slab = ko.observable({}).extend({store: "slab"});
    //Used to update result set used without value, using only valueHasMutated()
    var calculate_result_set = ko.observable();

    //proxy model to make viewmodels accessible in shell
    var step1_proxy = ko.observable();
    var step2_proxy = ko.observable();
    var step3_proxy = ko.observable();
    var results_proxy = ko.observable();

    var results_set = [];

    var deepCompare = function(x, y) {
        var leftChain, rightChain;

        function compare2Objects (x, y) {
            var p;

            // remember that NaN === NaN returns false
            // and isNaN(undefined) returns true
            if (isNaN(x) && isNaN(y) && typeof x === 'number' && typeof y === 'number') {
                return true;
            }

            // Compare primitives and functions.
            // Check if both arguments link to the same object.
            // Especially useful on step when comparing prototypes
            if (x === y) {
                return true;
            }

            // Works in case when functions are created in constructor.
            // Comparing dates is a common scenario. Another built-ins?
            // We can even handle functions passed across iframes
            if ((typeof x === 'function' && typeof y === 'function') ||
                (x instanceof Date && y instanceof Date) ||
                    (x instanceof RegExp && y instanceof RegExp) ||
                        (x instanceof String && y instanceof String) ||
                            (x instanceof Number && y instanceof Number)) {
                return x.toString() === y.toString();
            }

            // At last checking prototypes as good a we can
            if (!(x instanceof Object && y instanceof Object)) {
                return false;
            }

            if (x.isPrototypeOf(y) || y.isPrototypeOf(x)) {
                return false;
            }

            if (x.constructor !== y.constructor) {
                return false;
            }

            if (x.prototype !== y.prototype) {
                return false;
            }

            // check for infinitive linking loops
            if (leftChain.indexOf(x) > -1 || rightChain.indexOf(y) > -1) {
                return false;
            }

            // Quick checking of one object beeing a subset of another.
            // todo: cache the structure of arguments[0] for performance
            for (p in y) {
                if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
                    return false;
                }
                else if (typeof y[p] !== typeof x[p]) {
                    return false;
                }
            }

            for (p in x) {
                if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
                    return false;
                }
                else if (typeof y[p] !== typeof x[p]) {
                    return false;
                }

                switch (typeof (x[p])) {
                    case 'object':
                        case 'function':

                        leftChain.push(x);
                    rightChain.push(y);

                    if (!compare2Objects (x[p], y[p])) {
                        return false;
                    }

                    leftChain.pop();
                    rightChain.pop();
                    break;

                    default:
                        if (x[p] !== y[p]) {
                        return false;
                    }
                    break;
                }
            }

            return true;
        }

        if (arguments.length < 1) {
            return true; //Die silently? Don't know how to handle such case, please help...
            // throw "Need two or more arguments to compare";
        }

        for (var i = 1, l = arguments.length; i < l; i++) {

            leftChain = []; //todo: this can be cached
            rightChain = [];

            if (!compare2Objects(arguments[0], arguments[i])) {
                return false;
            }
        }

        return true;
    };

    var get_member = function(person) {
        var matches = [];
        ko.utils.arrayForEach(members(), function(member) {
            if(member.person == person){
                matches.push(member);
            }
        });
        return matches;
    };

    var step1_notify = function() {
        var verbose = [];
        var you = get_member('self');
        var spouse = get_member('spouse');
        var father = get_member('father');
        var mother = get_member('mother');
        var sons = get_member('son');
        var daughters = get_member('daughter');

        var status = [gender()];
        if(you.length > 0){
            var age = you[0].age;
            status.push('You : ' + ' ('+age+')');
        }
        if(spouse.length > 0){
            var age = spouse[0].age;
            status.push('Spouse : ' + ' ('+age+')');
        }
        if(father.length > 0){
            var age = father[0].age;
            status.push('Father : ' + ' ('+age+')');
        }
        if(mother.length > 0){
            var age = mother[0].age;
            status.push('Mother : ' + ' ('+age+')');
        }
        if(sons.length > 0){
            var sages = [];
            $.each(sons, function(i,s){
                var age = s.age;
                sages.push('('+age+')');
            });
            status.push('Sons : [ '+sages.join(' | ')+' ]');
        }
        if(daughters.length > 0){
            var sages = [];
            $.each(daughters, function(i,s){
                var age = s.age;
                sages.push('('+age+')');
            });
            status.push('Daughter : [ '+sages.join(' | ')+' ]');
        }
        return status.join(", ");
    };

    var step2_notify = function() {
        var status = ["pincode: "+ pincode()];
        status.push("Healthcare : "+ hospital_service());
        return status.join(", ");
    };

    var results_notify = function() {
        var status = ['SA:' + sum_assured()];
        return status.join(", ");
    };

    var compare_plans_notify = function(plans) {
        var status = ["Comparing plans"];
        $.each(plans, function(i, plan){
            status.push(plan.insurer_title + " - " + plan.product_title + " | " + plan.sum_assured + " | " + plan.premium);
        });
        return status.join("\n");
    };

    var top_plans_notify = function(plans) {
        var status = ["Top plans in search"];
        $.each(plans, function(i, plan){
            if(i < 4) {
                status.push(plan.insurer_title + " - " + plan.product_title + " | " + plan.sum_assured + " | " + plan.premium);
            }
        });
        return status.join("\n");
    };

    var plan_notify = function(plan) {
        var status = ["Plan opened : " + plan.insurer_title + " - " + plan.product_title];
        status.push(plan.sum_assured + " | " + plan.premium);
        return status.join("\n");
    };

    var buy_notify = function(plan) {
        var status = ["Buying in progress"];
        status.push(plan.insurer_title + " - " + plan.product_title);
        status.push(plan.sum_assured + " | " + plan.premium);
        status.push(plan.post_processing.status);
        return status.join("\n");
    };

    var step2_verbose = ko.computed(function() {
        var hservice = hospital_service();
        var status = '';
        if(hservice == 'BASIC'){
            status = "Basic Health care";
        }
        if(hservice == 'PRIVATE_ANY'){
            status = "Pvt. room in any hospital";
        }
        if(hservice == 'PRIVATE_HIGH_END'){
            status = "Pvt. room in a high-end hospital";
        }
        return status
    });

    var step1_verbose = ko.computed(function() {
        var you = get_member('self').length;
        var spouse = get_member('spouse').length;
        var father = get_member('father').length;
        var mother = get_member('mother').length;
        var sons = get_member('son').length;
        var daughters = get_member('daughter').length;

        var status = [];
        if(you > 0){
            status.push('You');
        }
        if(spouse > 0){
            status.push('Your Spouse');
        }
        if(father > 0 && mother > 0){
            status.push('Your Parents');
        }else if(father > 0){
            status.push('Your Father');
        }else if(mother > 0){
            status.push('Your Mother');
        }
        if(sons > 0){
            if(sons == 1){
                status.push('Your Son');
            }else {
                status.push('Your ' + sons + ' Sons');
            }
        }
        if(daughters > 0){
            if(daughters == 1){
                status.push('Your Son');
            }else {
                status.push('Your ' + daughters + ' daughters');
            }
        }

        var verbose = "";
        if(status.length==1){
            verbose = status[0];
        }else{
            verbose = status.slice(0, status.length-1).join(", ");
            verbose += " And " + status[status.length-1];
        }

        verbose = "Insurance for " + verbose;
        return verbose
    });

    Object.defineProperty(Array, "range", {
        writable: false, configurable: false, enumerable: true,
        value: function(start, end, step, includeEndpoint) {
            var length = start;
            step = step === undefined? 1: step;
            if (arguments.length == 1) {
                start = 0;
            } else {
                length = Math.ceil((end-start)/step);
            }
            var current = start;
            var array = (new Array(length + ((includeEndpoint||false)?1:0))).join("|").split("|");

            return array.map(function() {
                var value = current;
                current += step;
                return value;
            });
        }
    });

    var post_data = function() {
        var hlist = [];
        $.each(hospitals(), function(i, hosp){
            hlist.push(hosp.id);
        });
        return {
            members: ko.toJS(members),
            city: city(),
            gender: gender(),
            hospital_service: hospital_service(),
            pincode: pincode(),
            hospitals: hlist,
            sum_assured: sum_assured()
        };
    };

    var get_recommended_sum_assured = ko.computed(function() {
        var segment_dict = [
            [Array.range(18,31), {
                'base_cover' : 100000, 'S' : 50000, 'S_DP' : 50000, 'M' : 100000,
                'M_DP' : 150000, 'M_DC' : 200000, 'M_DP_DC' : 300000, 'BASIC' : 0,
                'PRIVATE_ANY' : 50000, 'PRIVATE_HIGH_END' : 100000, 'METRO' : 50000
            }],

            [Array.range(31,41), {
                'base_cover' : 100000, 'S' : 50000, 'S_DP' : 100000, 'M' : 100000,
                'M_DP' : 150000, 'M_DC' : 200000, 'M_DP_DC' : 300000, 'BASIC' : 0,
                'PRIVATE_ANY' : 50000, 'PRIVATE_HIGH_END' : 100000, 'METRO' : 50000
            }],

            [Array.range(41,46), {
                'base_cover' : 150000, 'S' : 50000, 'S_DP' : 100000, 'M' : 150000,
                'M_DP' : 200000, 'M_DC' : 200000, 'M_DP_DC' : 300000, 'BASIC' : 0,
                'PRIVATE_ANY' : 100000, 'PRIVATE_HIGH_END' : 150000, 'METRO' : 50000
            }],

            [Array.range(46,51), {
                'base_cover' : 200000, 'S' : 50000, 'S_DP' : 150000, 'M' : 150000,
                'M_DP' : 200000, 'M_DC' : 250000, 'M_DP_DC' : 350000, 'BASIC' : 0,
                'PRIVATE_ANY' : 100000, 'PRIVATE_HIGH_END' : 150000, 'METRO' : 50000
            }],

            [Array.range(51,56), {
                'base_cover' : 250000, 'S' : 50000, 'S_DP' : 150000, 'M' : 200000,
                'M_DP' : 200000, 'M_DC' : 250000, 'M_DP_DC' : 350000, 'BASIC' : 0,
                'PRIVATE_ANY' : 100000, 'PRIVATE_HIGH_END' : 150000, 'METRO' : 50000
            }],

            [Array.range(56,61), {
                'base_cover' : 250000, 'S' : 50000, 'S_DP' : 200000, 'M' : 200000,
                'M_DP' : 250000, 'M_DC' : 300000, 'M_DP_DC' : 500000, 'BASIC' : 0,
                'PRIVATE_ANY' : 100000, 'PRIVATE_HIGH_END' : 150000, 'METRO' : 50000
            }],

            [Array.range(61,66), {
                'base_cover' : 300000, 'S' : 50000, 'S_DP' : 200000, 'M' : 200000,
                'M_DP' : 250000, 'M_DC' : 300000, 'M_DP_DC' : 500000, 'BASIC' : 0,
                'PRIVATE_ANY' : 100000, 'PRIVATE_HIGH_END' : 150000, 'METRO' : 50000
            }],

            [Array.range(66,71), {
                'base_cover' : 350000, 'S' : 50000, 'S_DP' : 200000, 'M' : 250000,
                'M_DP' : 300000, 'M_DC' : 350000, 'M_DP_DC' : 500000, 'BASIC' : 0,
                'PRIVATE_ANY' : 100000, 'PRIVATE_HIGH_END' : 150000, 'METRO' : 50000
            }],

            [Array.range(71,120), {
                'base_cover' : 400000, 'S' : 50000, 'S_DP' : 250000, 'M' : 250000,
                'M_DP' : 300000, 'M_DC' : 350000, 'M_DP_DC' : 500000, 'BASIC' : 0,
                'PRIVATE_ANY' : 100000, 'PRIVATE_HIGH_END' : 150000, 'METRO' : 50000
            }]
        ];

        var get_sa_by_city = function(data, filter_list){
            if(data['city'][0] == 'METRO'){
                filter_list.push('METRO');
            }
        };

        var get_sa_by_family = function(data, filter_list) {
            var dependency = 'S';
            if(data['family']){
                dependency = 'M';
            }

            if('adults' in data) {
                if(data['adults'] != 0){
                    dependency = dependency + "_DP";
                }
            }

            if('kids' in data) {
                if(data['kids'] != 0){
                    dependency = dependency + "_DC";
                }
            }

            filter_list.push(dependency);
        };

        var get_sa_by_service_type = function(data, filter_list) {
            if(data['service_type']){
                filter_list.push(data['service_type']);
            }
        };

        //noinspection FunctionWithInconsistentReturnsJS
        var get_sa = function(){
            /*Data in the following format:
                data = {
                'sum_assured' : 200000,
                'age': '28',
                'city' : ['METRO','MUMBAI'],
                'service_type' : 'PRIVATE_ANY',
                'family' : true,
                'adults' : 2,
                'kids' : 2,
                }
                */
            if(members().length == 0) {
                return;
            }
            if(members().length > 1){
                var family = true;
                var max_age = 0;
                var kids = 0;
                var adults = 0;
                $.each(members(), function(index, member){
                    if(parseInt(member.age) > max_age){
                        max_age = parseInt(member.age);
                    }
                    if((member.person == 'son') || (member.person == 'daughter')){
                        kids += 1;
                    } else {
                        adults += 1;
                    }
                });
            } else {
                var kids = 0;
                var adults = 0;
                var family = false;
                var member = members()[0];
                var max_age = parseInt(member.age);
            }

            var data = {
                'age' : max_age,
                'city' : [city().type, city().name],
                'service_type' : hospital_service(),
                'kids' : kids,
                'family' : family,
                'adults' : adults
            };

            var _sum_assured = 0;
            var filter_list = [];
            var segment = [];
            var sa_dict = {};

            if(!('age' in data)){
                return 0;
            }

            for(var i=0; i < segment_dict.length; i++){
                var sd = segment_dict[i];
                if(sd[0].indexOf(parseInt(data['age'])) >= 0){
                    segment = sd[0];
                    sa_dict = sd[1];
                    _sum_assured = sa_dict['base_cover'];
                }
            }

            get_sa_by_city(data, filter_list);
            get_sa_by_family(data, filter_list);
            get_sa_by_service_type(data, filter_list);

            for(var i=0; i<filter_list.length; i++){
                var fl = filter_list[i];
                _sum_assured += sa_dict[fl];
            }
            //Exception Capping off at 5,00,000
            if(data['age'] <= 45 && _sum_assured > 500000) {
                _sum_assured = 500000
            }
            if(_sum_assured >= 700000) {
                _sum_assured = 700000
            }
            return _sum_assured
        };

        return get_sa();
    });

    return {
        members : members,
        gender : gender,
        city : city,
        hospital_service : hospital_service,
        pincode : pincode,
        hospitals : hospitals,

        step1_verbose : step1_verbose,
        step2_verbose : step2_verbose,
        step1_proxy : step1_proxy,
        step2_proxy : step2_proxy,
        step3_proxy : step3_proxy,
        results_proxy : results_proxy,
        results_set : results_set,

        step1_notify : step1_notify,
        step2_notify : step2_notify,
        results_notify : results_notify,
        top_plans_notify : top_plans_notify,
        compare_plans_notify : compare_plans_notify,
        plan_notify : plan_notify,
        buy_notify : buy_notify,

        sum_assured : sum_assured,
        calculate_result_set : calculate_result_set,
        deepCompare : deepCompare,
        get_recommended_sum_assured : get_recommended_sum_assured,
        post_data : post_data
    }
});
