define(['jquery', 'knockout', 'viewmodels/offline_model', 'utils','hasher', 'knockout_validation'], function($, ko, offline_model, utils, hasher) {

    ko.validation.init({
        insertMessages: false,
        decorateInputElement : true,
        registerExtenders: true,
        messagesOnModified: true,
        messageTemplate: null,
        grouping: { deep: true }
    });


    var transaction_id = null;

    var initialize_UI = function(){
        $('html').scrollTop(0);
        $('html').css('overflow', 'auto');
        $("#medical_container").show();
    };

    questionArray = ko.observableArray();
    ageArray = ko.observableArray();

    contact_model = ko.observable();

    age_medical_reason = ko.computed(function(){
        return ageArray().length != 0;
    });
    disease_medical_reason = ko.computed(function(){
        return questionArray().length != 0;
    });

    var contact_submit = function(){
        big_errors = ko.validation.group(contact_model(), { deep: true});
        if(big_errors().length > 0) {
            big_errors.showAllMessages();
        }
        else{
            var data = (contact_model());
            data['mobile'] = contact_model().mobile();
            data['campaign'] = 'Health';
            data['label'] = 'offline';
            data['triggered_page'] = window.location.href;
            data['device'] = 'Desktop';
            data['transaction_id'] = transaction_id;

            $.post("/leads/save-call-time/", {
                'data' : JSON.stringify(data),
                'source': 'transaction_medical',
                'csrfmiddlewaretoken' : CSRF_TOKEN,
            }, function(data){
                if (data.success){
                    $('#before_submit').addClass('hide');
                    $('#after_submit').removeClass('hide');
                    hasher.replaceHash("medical-submitted");
                }
                else{
                }
            }, 'json');

        }
    };

    return {
        contact_model : contact_model,
        contact_submit : contact_submit,
        age_medical_reason: age_medical_reason,
        disease_medical_reason: disease_medical_reason,
        activate : function(txn_id){
            transaction_id = txn_id;
            if($("#buy_container").length > 0){
                initialize_UI();
            }
            var durl = "/health-plan/to-medical/get-data/?r=" + new Date().getTime();
            var data_to_post =  {
                'transaction_id': transaction_id,
                'csrfmiddlewaretoken' : CSRF_TOKEN
            };
            var promise = $.post(durl, data_to_post, function(post_resp){
                questionArray.removeAll();
                resp = $.parseJSON(post_resp);
                if (resp.success) {
                    data_list = ['Transaction ID' + transaction_id];
                    var cmodel = new offline_model.contactData;
                    cmodel.initialize();
                    contact_model(cmodel);
                    $.each(resp.data.question_list, function(i, elem){
                        if($.isEmptyObject(elem.reason)){
                            elem.reason = null;
                        }
                        questionArray.push(elem);
                        data_list.push('Q. ' + elem.question + ' A. ' + elem.answer);
                    });
                    ageArray(resp.data.age_list);
                }
                else {
                    //window.location = "/articles/";
                }
            });
            return promise;
        },
        attached : function(view, parent, settings){
            olark('api.box.show');
        }
    }
});
