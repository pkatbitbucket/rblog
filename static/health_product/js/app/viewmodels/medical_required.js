define(['jquery', 'knockout', 'viewmodels/global','viewmodels/offline_model', 'viewmodels/utils', 'knockout_validation'], function($, ko, global, offline_model, utils) {

    ko.validation.init({
        insertMessages: false,
        decorateInputElement : true,
        registerExtenders: true,
        messagesOnModified: true,
        messageTemplate: null,
        grouping: { deep: true }
    });


    var transaction_id = null;

    var initialize_UI = function(){
        $('html').css('overflow', 'auto');
        $('#wrapper').transition({"padding-left":""});
        $("#medical_container").show();

    };

    questionArray = ko.observableArray();

    contact_model = ko.observable();

    var contact_submit = function(){
        big_errors = ko.validation.group(contact_model(), { deep: true});
        if(big_errors().length > 0) {
            console.log(big_errors());
            big_errors.showAllMessages();
        }
        else{
            var data = ko.toJSON(contact_model());
            console.log('=======================DATA=========', data);
            //data['']
            //                        'mobile' : cnum,
            //            'campaign': 'health-flipkart-tax',
            //            'label': 'lp_best_expert'

            $.post("/leads/save-call-time/", {'data' : data, 'csrfmiddlewaretoken' : CSRF_TOKEN}, function(data){
                if (data.success){
                    console.log('SUCCESS');
                    $('#before_submit').addClass('hide');
                    $('#after_submit').removeClass('hide');
                }
                else{
                    console.log('POST ERROR', data);
                }
            }, 'json');

        }
    };

    return {
        contact_model : contact_model,
        contact_submit : contact_submit,
        global : global,
        activate : function(txn_id){
            transaction_id = txn_id;
            if($("#buy_container").length > 0){
                initialize_UI();
            }
            var durl = "/health-plan/to-medical/get-data/?r=" + new Date().getTime();
            var data_to_post =  {
                'transaction_id': transaction_id,
                'csrfmiddlewaretoken' : CSRF_TOKEN
            };
            questionArray([]);
            var promise = $.post(durl, data_to_post, function(post_resp){
                questionArray.removeAll();
                resp = $.parseJSON(post_resp);
                console.log('===================DATA========== TEST', resp);
                if (resp.success) {
                    data_list = ['Transaction ID' + transaction_id];
                    var cmodel = new offline_model.contactData;
                    cmodel.initialize();
                    contact_model(cmodel);
                    $.each(resp.data.question_list, function(i, elem){
                        if($.isEmptyObject(elem.reason)){
                            elem.reason = null;
                        }
                        questionArray.push(elem);
                        data_list.push('Q. ' + elem.question + ' A. ' + elem.answer);
                    });
                }
                else {
                    console.log('Ooops');
                    //window.location = "/articles/";
                }
            });
            //console.log("Loaded Buy IndexJS");
            return promise;
        },
        attached : function(view, parent, settings){
            //console.log("View Loaded", $(view));
            olark('api.box.show');
            initialize_UI();
            console.log('$$$$$$$$$$$$000000');
        },
        deactivate : function(view, parent){
            $('#wrapper').transition({"padding-left":"250px"});
            $("#medical_container").hide();
        }
    }
});
