define(['plugins/router', 'jquery', 'knockout', 'viewmodels/global', 'viewmodels/utils', 'viewmodels/results_model',
        'viewmodels/new_step1', 'viewmodels/step2', 'viewmodels/step3', 'viewmodels/step3_model', 'response',
        'jquerycookie', 'jquerytransit', 'jquerypep', 'typeahead', 'bootstrap-tooltip',
        'bootstrap-collapse', 'bootstrap-transition', 'bootstrap-dropdown'],
    function(router, $, ko, global, utils, resultModel, step1, step2, step3, hospitalModel, response) {
        var plans = ko.observableArray([]);
        var selected_plan = ko.observable({});
        var compare_plans = ko.observableArray([]);
        var medical_comparison = ko.observableArray([]);
        var current_set_post_data = {};

        var current_view = ko.observable("SUMMARY");//possible values 'SUMMARY', 'MEDICAL', 'FIVEYEAR'
        var set_current_view = function(cview){
            current_view(cview);
        };


        var get_plans = function(post_data){
            var plist = [];
            $('#loading').show();
            $('#loading').find('#tick1').removeClass('hide');
            plans.removeAll();
            compare_plans([]);
            selected_plan({});
            step3.hospitals([]);
            global.hospitals([]);
            var npath = create_new_path(results_cached_id);
            router.navigate(npath);
            utils.notify([global.step1_notify(), global.step2_notify(), global.results_notify()]);
            utils.event_log('Completed Product Results', [global.step1_notify(), global.step2_notify(), global.results_notify()]);

            var json_promise = $.post('/health-plan/get-score/', {'csrfmiddlewaretoken' : CSRF_TOKEN, 'data': JSON.stringify(post_data)}, function(score_data){
                $('#loading #tick2').removeClass('hide');
                score_data = response.parseResponse(score_data);
                global.hospitals(score_data.hospitals);
                update_hospitals();
                global.city(score_data.city_obj);
                $.each(score_data.plans, function(index, data){
                    var plan = new resultModel.Plan(data);
                    plist.push(plan);
                });
                $('#resultsgrid').css('opacity', 0);
                /*plist.sort(function(left, right){
                 return left.score == right.score ? 0 : (left.score < right.score ? 1 : -1);
                 });*/
                plans(plist);
                utils.notify(global.top_plans_notify(plist));

                $('#zero_results').hide();
                if(score_data.plans.length == 0 ){
                    $('#loading #tick3').removeClass('hide');
                    if(AUTHENTICATED) {
                        $('#loading').hide();
                        $('#registration_modal').addClass('fade');
                    } else {
                        $('#loading').show();
                        $('#registration_modal').removeClass('fade');
                    }
                    $('#loading #results_load').html("Searching again based on new preferences...");
                    $('#zero_results').show();
                }
            });
            return json_promise;
        };

        global.calculate_result_set.subscribe(function(){
            if(!global.sum_assured()){
                global.sum_assured(global.get_recommended_sum_assured());
            }
            var post_data = global.post_data();
            if(!global.deepCompare(current_set_post_data, post_data)) {
                get_plans(post_data);
            }
            current_set_post_data = post_data;
        });

        var results_loaded = 0;
        var resultRendered = function(){
            results_loaded += 1;
            if(results_loaded == plans().length){
                relayout(plans());
                results_loaded = 0;
            }
        };

        var set_initial_state = function(){
            if(results_ptype == 'compare'){
                var cplans = [];
                $.each(plans(), function(i, p){
                    if([results_c1, results_c2, results_c3].indexOf(p.cpt_id.toString()) >= 0) {
                        cplans.push(p);
                    }
                });
                compare_plans(cplans);
                set_comparison_overlay();
            } else if(results_ptype == 'details'){
                var splan = {};
                $.each(plans(), function(i, p){
                    if(p.cpt_id == results_c1){
                        splan = p;
                    }
                });
                select_plan(splan);
            } else {
                selected_plan({});
                remove_compare_overlay();
                $(".result-tab").show();
            }
        };

        var relayout = function(plist){
            $.each(plist, function(i, p){
                var ele = $('#resultsgrid .cf-result#cpt_'+p.cpt_id);
                $(ele).css('position', 'absolute');
                $(ele).css('top', i*100); //to set gap between result cards
            });
            $('#resultsgrid').css('opacity', 1);
            $('#loading #tick3').removeClass('hide');
            setTimeout(function(){
                if(!viewAttached) {
                    set_initial_state();
                }
                if(window.jQuery.cookie('referrer')){
                    if(window.jQuery.cookie('referrer').indexOf('bigdecisions') >= 0) {
                        //if(window.jQuery.cookie('referrer').indexOf('localhost') >= 0) {
                        $('#coverFilter #sliderLabels, #topCoverFilter #sliderLabels').addClass('hide');
                        $('ul#sliderStations').css('opacity', 1);
                    }
                }
                if(AUTHENTICATED) {
                    $('#loading').hide();
                    $('#registration_modal').addClass('fade');
                } else {
                    $('#loading').show();
                    $('#registration_modal').removeClass('fade');
                }
                $('#loading #results_load').html("Searching again based on new preferences...");
                viewAttached = true;
            }, 1000);
            $(".cf-medical-details .ttip").tooltip({
                'container': '#resultsgrid',
                'placement' : 'right',
                'html' : true
            });
        };

        var reload_plans = function(){
            if(!step1.MemberModel.validate()){
                event.preventDefault();
                return false;
            }

            $('#edit-info-overlay').fadeOut();
            /* Setting up global state */
            global.members(step1.MemberModel.serialized_members());
            global.gender(step1.MemberModel.gender());

            global.pincode(step2.pincode());
            global.hospital_service(step2.HService.type());

            global.hospitals(step3.get_selected_hospitals());

            /* Setting SA will trigger getting new plans */
            global.calculate_result_set.valueHasMutated();
        };

        var compare_clear = function() {
            compare_plans.removeAll();
        };
        var compare_select = function(plan) {
            $('.cf-result .cf-compare-checkbox').tooltip('hide');
            $('.cf-result .cf-compare-checkbox').tooltip('destroy');
            $(".cf-compare-btn").tooltip('hide');
            $(".cf-compare-btn").tooltip('destroy');
            if(compare_plans().length >= 3){
                $('#cpt_'+plan.cpt_id+" .cf-compare-checkbox").tooltip({
                    'title' : 'Only 3 plans can be compared at a time',
                    'trigger' : 'manual',
                    'placement' : 'right',
                    'container' : '#results_page'
                }).tooltip('show');
                return false;
            } else {
                compare_plans.push(plan);
            }
        };

        var compare_deselect = function(plan) {
            $('.cf-result .cf-compare-checkbox').tooltip('hide');
            $('.cf-result .cf-compare-checkbox').tooltip('destroy');
            $(".cf-compare-btn").tooltip('hide');
            $(".cf-compare-btn").tooltip('destroy');
            var index = compare_plans.indexOf(plan);
            if (index > -1) {
                compare_plans.splice(index, 1);
            }
        };

        compare_plans.subscribe(function(){
            var terms_list = {};
            var compare_obj = {};
            var compare_list = {};
            var comparison = [];
            if(compare_plans().length <= 1){
                return false;
            }

            $.each(compare_plans(), function(i, plan){
                $.each(plan.general.conditions, function(i, pobj) {
                    terms_list[pobj.pterm.id] = {'id' : pobj.pterm.id, 'name' : pobj.pterm.name};
                    if(!(pobj.pterm.id in compare_obj)){
                        compare_obj[pobj.pterm.id] = "";
                    }
                    compare_obj[pobj.pterm.id] += "-" + pobj.period;
                });
            });

            $.each(compare_obj, function(pid, period){
                if(!(period in compare_list)){
                    compare_list[period] = [];
                }
                compare_list[period].push(terms_list[pid]);
            });

            $.each(compare_list, function(period, conditions){
                if(conditions.length > 3){
                    var i = 0;
                    for(; i < parseInt(conditions.length/3);i++){
                        comparison.push({
                            'periods' : period.split("-").slice(1),
                            'conditions' : conditions.slice(3*i, (3*i)+3)
                        });
                    }
                    if(conditions.length%3 !=0) {
                        comparison.push({
                            'periods' : period.split("-").slice(1),
                            'conditions' : conditions.slice((3*i), conditions.length)
                        });
                    }
                } else {
                    comparison.push({
                        'periods' : period.split("-").slice(1),
                        'conditions' : conditions
                    });
                }
            });
            medical_comparison(comparison);
        });

        var sort_by_score = function(stype){ //stype = true/false
            var sort_plans = plans();
            sort_plans.sort(function(left, right){
                if(stype) {
                    return left.score == right.score ? 0 : (left.score < right.score ? -1 : 1);
                } else {
                    return left.score == right.score ? 0 : (left.score < right.score ? 1 : -1);
                }
            });
            relayout(sort_plans);
        };
        var sort_by_premium = function(stype){
            var sort_plans = plans();
            sort_plans.sort(function(left, right){
                if(stype) {
                    return left.premium == right.premium ? 0 : (left.premium < right.premium ? -1 : 1);
                } else {
                    return left.premium == right.premium ? 0 : (left.premium < right.premium ? 1 : -1);
                }
            });
            relayout(sort_plans);
        };
        var sort_by_sum_assured = function(stype){
            var sort_plans = plans();
            sort_plans.sort(function(left, right){
                if(stype) {
                    return left.sum_assured == right.sum_assured ? 0 : (left.sum_assured < right.sum_assured ? -1 : 1);
                } else {
                    return left.sum_assured == right.sum_assured ? 0 : (left.sum_assured < right.sum_assured ? 1 : -1);
                }
            });
            relayout(sort_plans);
        };

        var buy_this_plan = function(plan){
            utils.notify(global.buy_notify(plan));
            console.log("BUY FLAG >>>>", plan.post_processing);
            if(plan.post_processing.status == 'BACKEND_NOT_INTEGRATED') {
                router.navigate("offline");
            }

            if(plan.post_processing.status == 'OFFLINE_COVER_TOO_HIGH') {
                router.navigate("cover-offline");
            }

//        if(plan.post_processing.status == 'OFFLINE_AGE_TOO_HIGH' || plan.post_processing.status == 'MEDICAL_REQUIRED') {
            if(plan.post_processing.status == 'OFFLINE_AGE_TOO_HIGH') {
                router.navigate("age-offline");
            }

            if(plan.post_processing.status == 'FILL_FORM' || plan.post_processing.status == 'MEDICAL_REQUIRED') {
                create_transaction(plan);
            }
            return false;
        };

        var buy_selected_plan = function(plan){
            utils.notify(global.buy_notify(plan));
            if(plan.post_processing.status == 'BACKEND_NOT_INTEGRATED') {
                router.navigate("offline");
            }

            if(plan.post_processing.status == 'OFFLINE_COVER_TOO_HIGH') {
                router.navigate("cover-offline");
            }

//        if(plan.post_processing.status == 'OFFLINE_AGE_TOO_HIGH' || plan.post_processing.status == 'MEDICAL_REQUIRED') {
            if(plan.post_processing.status == 'OFFLINE_AGE_TOO_HIGH') {
                router.navigate("age-offline");
            }

            if(plan.post_processing.status == 'FILL_FORM' || plan.post_processing.status == 'MEDICAL_REQUIRED') {
                create_transaction(plan);
            }
            return false;
        };

        var create_transaction = function(plan){
            var post_data = global.post_data();
            post_data['slab_id'] = plan.slab_id;
            post_data['cpt_id'] = plan.cpt_id;
            $.post('/health-plan/transaction/create/', {'csrfmiddlewaretoken' : CSRF_TOKEN, 'data' : JSON.stringify(post_data)}, function(data){
//                if (data.transaction_id == 'APOLLO_HACK'){
//                    //JUST FOR APOLLO
//                    window.location = data.redirect_url;
//                }else{
//                    window.location = "/health-plan/buy/" + data.transaction_id;
//                }
                window.location = "/health-plan/buy/" + data.transaction_id;
            }, 'json');
            return false;
        };

        var create_new_path = function(results_cached_id) {
            var upath = router.activeInstruction().fragment;
            var parts_path = upath.split('/');
            parts_path[1] = results_cached_id;
            return parts_path.join('/');
        };

        var select_plan = function(plan) {
            if(selected_plan() != plan){
                set_current_view('SUMMARY');
            }
            selected_plan(plan);
            router.navigate('results/'+results_cached_id+'/details/'+plan.cpt_id+'/');
            $('#results_page').scrollTop(0);
            utils.notify(global.plan_notify(plan));
        };

        var deselect_plan = function() {
            selected_plan({});
            router.navigate('results/'+results_cached_id+'/');
        };

        var back_from_compare = function() {
            remove_compare_overlay();
            $(".result-tab").show();
            router.navigate('results/'+results_cached_id+'/');
        };

        var set_comparison_overlay = function (){
            setTimeout(function(){
                utils.notify(global.compare_plans_notify(compare_plans()));
                $(".result-tab").hide();
                $(".list-group-item").removeClass('active');
                $('.compare_factors_tab').addClass('active');
                $('ul#compare-parameters').removeClass('fame');
                $('#compareCards').clone().appendTo('#overlay');
                $('#overlay #compareCards .plan').css({y: 0});
                $('#overlay').show();
                $('#overlay #compareCards').wrap( "<div class='screen_holder'></div>");
                setTimeout(function(){
                    $('body').scrollTop(0);
                    $('#overlay #compareCards .plan').fadeIn();
                    $('ul#compare-parameters').addClass('fame');
                }, 200);
                $("#overlay #compare_medical_container .ttip").tooltip({
                    'container': '#overlay #compare_medical_coverage',
                    'placement' : 'right',
                    'html' : true
                });
                $('#page-wrapper').hide();

                $('.side_bar_content').not('#side_bar_compare').transition({left:"-100%", opacity:0},400, function(){$(this).hide()});
                $("#side_bar_compare").show().transition({left : 0, opacity : 1}, 400);
                $('.top-compare-tab').show();
                $('#resultsnav').transition({y:-50},200);
            }, 500);
        };

        var remove_compare_overlay = function(){
            $('#overlay').fadeOut(200).html("");
            $('#page-wrapper').show();
            $('.side_bar_content').not('#side_bar_results, #spaces').transition({left:"-100%", opacity:0},400, function(){$(this).hide()});
            $('.top-compare-tab').hide();
            $("#side_bar_results, #spaces").show().transition({left : 0, opacity : 1}, 400);
            $('#resultsnav').transition({y:0},200);
        };

        var update_hospitals = function(){
            if(global.hospitals() && step3.hospitals().length == 0){
                $.each(global.hospitals(), function(i, d){
                    var h = new hospitalModel.Hospital(d.id, d.name, d.address, d.lat, d.lng, true);
                    step3.hospitals.push(h);
                });
            }
        };

        var update_on_activate = function(){
            update_hospitals();
            step1.MemberModel.initialize();
            global.calculate_result_set.valueHasMutated();
        };

        var viewAttached = false;
        var results_cached_id;
        var results_ptype;
        var results_c1;
        var results_c2;
        var results_c3;
        var results_viewsmodel = {
            global : global,
            plans : plans,
            step1 : step1,
            step2 : step2,
            step3 : step3,
            selected_plan : selected_plan,
            compare_plans : compare_plans,
            current_view : current_view,
            set_current_view : set_current_view,
            reload_plans : reload_plans,
            medical_comparison : medical_comparison,
            compare_select : compare_select,
            compare_deselect : compare_deselect,
            compare_clear : compare_clear,
            sort_by_score : sort_by_score,
            sort_by_premium : sort_by_premium,
            sort_by_sum_assured : sort_by_sum_assured,
            select_plan : select_plan,
            deselect_plan : deselect_plan,
            buy_this_plan : buy_this_plan,
            buy_selected_plan : buy_selected_plan,
            resultRendered : resultRendered,
            back_from_compare : back_from_compare,

            activate : function(results_event, ptype, c1, c2, c3){
                results_c1 = null;
                results_c2 = null;
                results_c3 = null;

                $('#loading #results_load').show();
                if(!AUTHENTICATED) {
                    $("#registration_modal").removeClass("fade");
                }
                results_cached_id = results_event;
                results_ptype = ptype;
                if(results_ptype == 'compare'){
                    results_c1 = c1;
                    results_c2 = c2;
                    if(c3){
                        results_c3 = c3;
                    }
                }
                if(results_ptype == 'details') {
                    results_c1 = c1;
                }
                if(viewAttached) {
                    set_initial_state();
                }
            },
            attached : function(view, parent, settings){
                utils.event_log('triggered_results');
                olark('api.box.show');

                $(view).show();
                $('.call-prompts').removeClass('hide');
                $('body').on('click', '.contact-me', function(){
                    $("#c2c_close").trigger('click');
                    router.navigate('contact/' + results_cached_id);
                });

                $('body').on('click', '.btn-c2c', function(){

                    var context = $(this);
                    context.addClass('disabled').text('Submitting...');

                    $('#contact-me-error').hide().text('');
                    var cnum = $('#contact-me-num').val();
                    var res = null;
                    if(cnum && cnum.length == 10){
                        res = cnum.match(/[7-9]{1}\d{9}/);
                        if(res){
                            var cmodel =  {
                                'mobile' : cnum,
                                'device': window.jQuery.cookie('device'),
                                'label': 'new_results'
                            };
                            $.post("/leads/save-call-time/", {'data' : ko.toJSON(cmodel), 'csrfmiddlewaretoken' : CSRF_TOKEN}, function(data){
                                if (data.success){
                                    console.log('SUCCESS');
                                    context.removeClass('disabled').text('Submit');
                                    $('#c2c-thanks').addClass('reveal');
                                    $('.call-prompts').fadeOut();
                                    $('#c2cbar-overlay').hide();
                                }
                                else{
                                    console.log('POST ERROR', data);
                                    context.removeClass('disabled').text('Submit');
                                }
                            }, 'json');
                        }
                    }
                    if($.isEmptyObject(res)){
                         console.log('INVALID PHONE NUMBER');
                         context.removeClass('disabled').text('Submit');
                        $('#contact-me-error').show().text('Invalid Phone Number');
                    }

                });

                sidebarWidth = $('.side-nav').width();
                $('#overlay').removeClass('hide');

                $('#overlay').scroll(function(){
                    var twidth = $('#overlay').scrollTop();
                    $('#compareCards .fixtop').css('top', twidth);
                });

                $(document).on('mouseout', '.cf-result', function(){
                    $('.tooltip').hide();
                });

                $(document).on('click', '#close_change_cover', function(){
                    $(this).parent().parent().removeClass('selected');
                });

                $(document).on('click', '#show_change_cover', function(){
                    if($(this).parent().hasClass('selected')){
                        $(this).parent().removeClass('selected');
                    } else {
                        $(this).parent().addClass('selected');
                    }
                });

                $(document).on('click', '.cf-compare-btn', function(event){
                    $('.cf-result .cf-compare-checkbox').tooltip('hide');
                    $('.cf-result .cf-compare-checkbox').tooltip('destroy');
                    $(".cf-compare-btn").tooltip('hide');
                    $(".cf-compare-btn").tooltip('destroy');

                    if(compare_plans().length < 2){
                        $(".cf-compare-btn").tooltip({
                            'title' : 'Select atleast 2 plans to compare',
                            'trigger' : 'manual',
                            'placement' : 'bottom'
                        }).tooltip('show');
                        return false;
//                    event.preventDefault();
                    }

                    var compare_cpts = [];
                    $.each(compare_plans(), function(i, p){
                        compare_cpts.push(p.cpt_id);
                    });
                    router.navigate('results/'+results_cached_id+'/compare/'+compare_cpts.join('/')+'/');
                    event.preventDefault();
                });

                $(document).on('click', '#plan-summary', function(event) {
                    $('#overlay #plansummarycontainer').removeClass('closeps');
                    $('#overlay #planmedicalcoverage').removeClass('open');
                    $('.tabheader').removeClass('active');
                    $(this).addClass('active');
                    event.preventDefault();
                });

                $(document).on('click', '.edit-info', function(event) {
                    $('#edit-info-overlay').fadeOut().removeClass('hide').fadeIn();
                    event.preventDefault();
                });

                $(document).on('click', '.buy_plan', function(event) {
                    var cpt_id = $(this).attr('data_plan').split('_')[1];
                    var splan = $.grep(plans(), function(p){ return p.cpt_id == cpt_id; });
                    buy_selected_plan(splan[0]);
                    event.preventDefault();
                });

                $(document).on('click', '.compare_factors_tab', function(event){
                    $('#overlay #compare_important_features').removeClass('closeps');
                    $('#overlay #compare_medical_coverage').fadeOut();

                    $('#side_bar_compare .list-group-item').removeClass('active');
                    $('.top-compare-tab a').removeClass('active');
                    $(this).addClass('active');
                    event.preventDefault();
                });

                $(document).on('click', '.compare_medical_tab', function(event) {
                    $('#overlay #compare_important_features').addClass('closeps');
                    $('#overlay #compare_medical_coverage').fadeIn();

                    $('#side_bar_compare .list-group-item').removeClass('active');
                    $('.top-compare-tab a').removeClass('active');
                    $(this).addClass('active');
                    $("#overlay .specificDiseases .rowheader, #overlay #common_conditions").tooltip({
                        'container': '.specificDiseases',
                        'placement' : 'right',
                        'html' : true
                    });
                    $("#overlay .rowheader.sumheader").tooltip({
                        'placement' : 'right',
                        'html' : true
                    });
                    event.preventDefault();
                });

                window.jQuery('#edit-info-overlay #edit-hospitals #search_hospital').typeahead({
                    name: 'hospitals',
                    minLength : 3,
                    valueKey : 'verbose',
                    limit : 30,
                    remote : {
                        url : '/health-plan/hospitals/auto-complete/?csrfmiddlewaretoken=%CSRF_TOKEN&term=%QUERY&type=country&r=%RANDOM',
                        replace: function(url, query) {
                            var stype = $("input[name=stype]:radio:checked").val();
                            return url.replace('%QUERY', query).replace("%RANDOM", new Date().getTime()).replace("%CSRF_TOKEN", CSRF_TOKEN);
                        },
                        filter : function(data){
                            data = response.parseResponse(data.data);
                            var filtered_resp = $.map(data.hospitals, function(h, i){
                                return h;
                            });
                            if(data.hospitals.length == 0){
                                filtered_resp = [{'id': -1, 'verbose' : 'No hospitals found'}];
                            }
                            return filtered_resp;
                        }
                    }
                });

                window.jQuery('#edit-info-overlay #edit-hospitals #search_hospital').on('typeahead:selected', function (object, d) {
                    if(d.id == -1){
                        window.jQuery('#edit-info-overlay #edit-hospitals #search_hospital').typeahead('setQuery','');
                        return true;
                    }
                    $("#edit-info-overlay #edit-hospitals #search_hospital").typeahead('setQuery','');
                    var h = new hospitalModel.Hospital(d.id, d.name, d.address, d.lat, d.lng, true, true);

                    exists = false;
                    $.each(step3.hospitals(), function(i, hosp){
                        if(hosp.id == h.id) {
                            hosp.selected(true);
                            exists = true;
                        }
                    });
                    if(!exists){
                        step3.hospitals.unshift(h);
                    }
                });
                var promise = $.post('/health-plan/initialize-results/', {
                    'csrfmiddlewaretoken' : CSRF_TOKEN, 'rtracker': results_cached_id
                }, function(res){
                    if(res.data){
                        global.members(res.data.members);
                        global.hospital_service(res.data.hospital_service);
                        global.pincode(res.data.pincode);
                        global.hospitals(res.data.hospitals);
                        global.city(res.data.city);
                        global.gender(res.data.gender);
                        global.sum_assured(res.data.sum_assured);
                    }
                    if(res.activity_id != results_cached_id) {
                        results_cached_id = res.activity_id;
                        var npath = create_new_path(results_cached_id);
                        router.navigate(npath);
                    }
                    update_on_activate();
                }, 'json');
                return promise;
            }
        };
        global.results_proxy(results_viewsmodel);
        return results_viewsmodel;
    });
