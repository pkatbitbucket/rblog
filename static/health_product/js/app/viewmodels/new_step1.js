define(['plugins/router', 'jquery', 'knockout', 'viewmodels/step1_model', 'viewmodels/global', 'viewmodels/utils', 'knockout_validation', 'jquerytransit'], function(router, $, ko, model, global, utils, ) {

    ko.validation.init({
        insertMessages: false,
        decorateInputElement : true,
        registerExtenders: true,
        messagesOnModified: true,
        messageTemplate: null,
        errorElementClass : 'error_border',
    });
    var age_error = ko.observable("");
    var MemberModel = function () {
        var self = this;
        self.gender = ko.observable("");//Gender of 'you'
        self.members = ko.observableArray([]);

        var kid_id = 1;
        self.add = function (person, verbose, selected, age) {
            if(person.toLowerCase() == 'son' || person.toLowerCase() == "daughter"){
                id = person.toLowerCase() + kid_id;
                kid_id += 1;
            } else {
                id = person.toLowerCase();
            }
            if(person.toLowerCase() == 'son' || person.toLowerCase() == "daughter"){
                minAge = 1;
                maxAge = 24;
            }
            if(person.toLowerCase() == 'father' || person.toLowerCase() == "mother"){
                minAge = 21;
                maxAge = 100;
            }
            if(person.toLowerCase() == 'spouse' || person.toLowerCase() == "self"){
                minAge = 21;
                maxAge = 100;
            }
            var m = new model.Member( id, person, verbose, selected, age, minAge, maxAge);
            self.members.push(m);
            return m;
        };

        self.select_gender = function(ptype){
            self.gender(ptype);
            if(ptype == "MALE"){
                self.spouse.display("Wife");
            }
            if(ptype == "FEMALE"){
                self.spouse.display("Husband");
            }
        };

        self.gender_done = function(){
            $('#page-wrapper').removeClass('hide');
            $('#genderSelect').fadeOut(function(){$('#genderSelect').addClass('hide');});
            router.navigate('step1/members', false);
        };

        self.selected_members = function(){
            return self.members().filter(function(ele){ return ele.selected()});
        };
        self.number_of_adults = function(){
            var num = 0;
            $.each(self.selected_members(), function(i, elem){
                if(elem.person.toLowerCase() == 'father' || elem.person.toLowerCase() == "mother" ||
                    elem.person.toLowerCase() == 'spouse' || elem.person.toLowerCase() == "self"){
                    num = num + 1;
                }
            });
            return num;
        };

        self.age_fields = function () {
            var dfields = [];
            $.each(self.selected_members(), function(i, m){
                dfields.push(m.age);
                m.validate_now(true);
            });
            return dfields;
        };

        self.serialized_members = function(){
            var smembers = [];
            $.each(self.members(), function(i, m){
                if(m.selected()){
                    smembers.push({
                        id : m.id,
                        person : m.person,
                        verbose : m.verbose,
                        age : m.age()
                    });
                }
            });
            return smembers;
        };

        self.remove = function (member) {
            self.members.remove(member);
        };

        self.deselect = function (member) {
            member.selected(false);
            if(member.person.toLowerCase() == 'son' || member.person.toLowerCase() == "daughter"){
                self.remove(member);
            }
        };

        self.select = function(member) {
            if(member.selected()){
                return true;
            }
            member.selected(true);
        };

        self.getKids = ko.computed(function() {
            var kids = [];
            ko.utils.arrayForEach(self.members(), function(member){
                if(member.person.toLowerCase() == 'son' || member.person.toLowerCase() == "daughter"){
                    kids.push(member);
                }
            });
            return kids;
        });

        self.getMemberById = function(mid){
            var smember = null;
            $.each(self.selected_members(), function(i, member){
                if(mid == member.id){
                    smember = member;
                    return false;
                }
            });
            return smember
        };

        self.you = self.add('self', "You", false);
        self.spouse = self.add('spouse', "Spouse", false);
        self.father = self.add('father', "Father", false);
        self.mother = self.add('mother', "Mother", false);

        self.initialize = function() {
            if(global.members().length != 0){
                //Purge all sons first so that new ones are not added
                var remove_kids = [];
                $.each(self.members(), function(i, mem){
                    if(mem.person == 'son' || mem.person == 'daughter') {
                        remove_kids.push(mem);
                    }
                });
                self.members.removeAll(remove_kids);
                $.each(global.members(), function(i, member){
                    if(['self', 'spouse', 'father', 'mother'].indexOf(member.person) >= 0){
                        $.each(self.members(), function(i, mem){
                            if(mem.person == member.person){
                                mem.selected(true);
                                mem.age(member.age);
                            }
                        });
                    } else {
                        self.add(member.person, member.verbose, true, member.age);
                    }
                });
            } else {
                //Initially only select you
                self.you.selected(true);
            }
            if(global.gender() != ""){
                self.select_gender(global.gender());
            } else {
                global.gender('MALE');
                self.select_gender(global.gender());
            }
        };

        self.validate = function() {
            var verrors = ko.validation.group(self.age_fields());
            verrors.showAllMessages();
            if(verrors().length > 0 || self.selected_members().length == 0){
                return false;
            }

            //Age difference check
            age_error("");

            // Between You/Spouse and Kids
            var min_first_gen_age = null;
            self_age = self.getMemberById('self') ? self.getMemberById('self').age() : null;
            spouse_age = self.getMemberById('spouse') ? self.getMemberById('spouse').age() : null;
            if(self_age && spouse_age) {
                min_first_gen_age = Math.min(self_age, spouse_age);
            } else {
                min_first_gen_age = self_age || spouse_age;
            }
            var max_kids_age = 0;
            if(self.getKids().length > 0){
                $.each(self.getKids(), function(i,k){
                    max_kids_age = Math.max(k.age(), max_kids_age);
                });
            }

            // Between You and Your Parents
            var min_second_gen_age = null;

            father_age = self.getMemberById('father') ? self.getMemberById('father').age() : null;
            mother_age = self.getMemberById('mother') ? self.getMemberById('mother').age() : null;


            if(father_age && mother_age) {
                min_second_gen_age = Math.min(father_age, mother_age);
            } else {
                min_second_gen_age = father_age || mother_age;
            }
            if(min_first_gen_age && (max_kids_age != 0)){
                if(min_first_gen_age - max_kids_age < 18){
                    age_error("You & your spouse should be at least 18 years older than your children");
                }
            }
            else if(min_second_gen_age && self_age){
                if(min_second_gen_age - self_age < 18){
                    age_error("Your parents should be at least 18 years older than you");
                }
            }
            else if(min_second_gen_age && (max_kids_age != 0)){
                if(min_second_gen_age - max_kids_age < 36){
                    age_error("Your parents should be at least 36 years older than your children");
                }
            }
            else if(max_kids_age != 0 && !(min_first_gen_age || min_second_gen_age)){
                age_error("Please select at least one adult, as children cannot be insured without an adult");
            }

            //Check for more than 2 adults
            if(self.number_of_adults() > 2){
                age_error("Maximum 2 adults can be insured under a single policy. <br />" +
                    "It's recommended to insure your parents under a separate policy.");
            }


            if(age_error() != ""){
                return false;
            }

            return true;
        }
    };

    viewAttached = false;
    MemberModel = new MemberModel();
    var step1_viewsmodel = {
        age_error : age_error,
        MemberModel : MemberModel,
        nextView : function(){

            if(!MemberModel.validate()){
                return false;
            }
            global.members(MemberModel.serialized_members());
            global.gender(MemberModel.gender());

            utils.notify(global.step1_notify());
            utils.event_log('Completed Step 1', global.step1_notify());
            router.navigate("step2");
        },
        activate : function(){
            //console.log("ROUTER CONFIG PAGE > ", router.activeInstruction().config.page);
            MemberModel.initialize();
            if(['', 'step1'].indexOf(router.activeInstruction().fragment) >= 0) {
                $('#genderSelect').removeClass('hide').fadeIn();
                $('#page-wrapper').addClass('hide');
            }
            if(!viewAttached){
                global.hospitals([]);
            }
        },
        attached : function(view, parent, settings){
            windowheight = $(window).height();
            utils.event_log('Visited Step 1');
            router.on("router:route:activating", function(){
                console.log("Router activating event on step1 attach", router.activeInstruction().fragment);
                if(['', 'step1'].indexOf(router.activeInstruction().fragment) >= 0) {
                    $('#page-wrapper').addClass('hide');
                    $('#genderSelect').removeClass('hide').fadeIn();
                }
                if(router.activeInstruction().fragment == 'step1/members'){
                    $('#page-wrapper').removeClass('hide');
                    $('#genderSelect').addClass('hide').fadeOut();
                }
            });
            var verrors = ko.validation.group(MemberModel.age_fields());
            verrors.showAllMessages(false);

            viewAttached = true;
        }
    };
    global.step1_proxy(step1_viewsmodel);
    return step1_viewsmodel;
});
