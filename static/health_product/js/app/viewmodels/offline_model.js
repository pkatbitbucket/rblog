define(['knockout'], function(ko) {
        'use strict';

//        var transaction_model = null;
//
//        var init = function(tmodel) {
//            transaction_model = tmodel;
//        }

        var TimeData = function(data, frotime){
            var self = this;
            self.slot = ko.observable(data);
            self.slot_selected = ko.observable();
            self.frozen = ko.observable(frotime);
            self.time_slot_click = function(){
                if(self.slot_selected() == 'Yes'){
                    self.slot_selected('');
                }
                else{
                    self.slot_selected('Yes');
                }
            };
        };

        var ContactData = function(){
            var self = this;
            self.mobile = ko.observable('').extend({
                required: true,
                pattern: {
                    message: 'Please enter a valid phone number.',
                    params: /^D?(\d{10})$/
                }
            });
            self.todayText = ko.observable();
            self.tommText = ko.observable();
            self.afterText = ko.observable();

            self.todaySlots = ko.observableArray();
            self.tommSlots = ko.observableArray([
                new TimeData('10am - 12pm', 'No'),
                new TimeData('12pm - 2pm', 'No'),
                new TimeData('2pm - 4pm', 'No'),
                new TimeData('4pm - 6pm', 'No')
            ]);

            self.afterSlots = ko.observableArray([
                new TimeData('10am - 12pm', 'No'),
                new TimeData('12pm - 2pm', 'No'),
                new TimeData('2pm - 4pm', 'No'),
                new TimeData('4pm - 6pm', 'No')
            ]);

            self.initialize = function(){
                var dd = new Date();
                var today = new Date();
                var tomorrow = new Date();
                var after = new Date();
                if (dd.getDay() == 7){
                    today.setDate(dd.getDate()+1);
                    tomorrow.setDate(dd.getDate()+2);
                    after.setDate(dd.getDate()+3);
                }
                else if (dd.getDay() == 6){
                    today.getDate();
                    tomorrow.setDate(dd.getDate()+2);
                    after.setDate(dd.getDate()+3);

                }
                else if (dd.getDay() == 5){
                    today.getDate();
                    tomorrow.setDate(dd.getDate()+1);
                    after.setDate(dd.getDate()+3);
                }
                else {
                    today.getDate();
                    tomorrow.setDate(dd.getDate()+1);
                    after.setDate(dd.getDate()+2);

                }
                var today_text = today.toDateString().split(' ')[1] + ' ' + today.toDateString().split(' ')[2] + ', ' + today.toDateString().split(' ')[0];
                var tomm_text = tomorrow.toDateString().split(' ')[1] + ' ' + tomorrow.toDateString().split(' ')[2] + ', ' + tomorrow.toDateString().split(' ')[0];
                var after_text = after.toDateString().split(' ')[1] + ' ' + after.toDateString().split(' ')[2] + ', ' + after.toDateString().split(' ')[0];

                self.todayText(today_text);
                self.tommText(tomm_text);
                self.afterText(after_text);

                var dtime = new Date;
                var ctime = dtime.getHours();
                if(ctime < 10){
                    self.todaySlots([
                        new TimeData('10am - 12pm', 'No'),
                        new TimeData('12pm - 2pm', 'No'),
                        new TimeData('2pm - 4pm', 'No'),
                        new TimeData('4pm - 6pm', 'No')
                    ]);
                }
                else if((ctime >= 10) && ctime < 12){
                    self.todaySlots([
                        new TimeData('10am - 12pm', 'Yes'),
                        new TimeData('12pm - 2pm', 'Yes'),
                        new TimeData('2pm - 4pm', 'No'),
                        new TimeData('4pm - 6pm', 'No')
                    ]);
                }
                else if((ctime >= 12) && ctime < 14){
                    self.todaySlots([
                        new TimeData('10am - 12pm', 'Yes'),
                        new TimeData('12pm - 2pm', 'Yes'),
                        new TimeData('2pm - 4pm', 'Yes'),
                        new TimeData('4pm - 6pm', 'No')
                    ]);
                }
                else {
                    self.todaySlots([
                        new TimeData('10am - 12pm', 'Yes'),
                        new TimeData('12pm - 2pm', 'Yes'),
                        new TimeData('2pm - 4pm', 'Yes'),
                        new TimeData('4pm - 6pm', 'Yes')
                    ]);
                }
            };
        };

        return {
            contactData: ContactData,
            timeData : TimeData
        };
    }
);
