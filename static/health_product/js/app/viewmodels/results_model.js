define(['knockout'],
    function(ko) {
        'use strict';

        // represent a single member item
        var Plan = function (data) {
            var self = this;
            self.slab_id = data.slab_id;
            self.product_id = data.product_id;
            self.insurer_id = data.insurer_id;

            self.post_processing = data.post_processing_flag;

            self.five_year_projection = data.five_year_projection;
            self.product_title = data.product_title;
            self.insurer_title = data.insurer_title;
            self.sum_assured = parseInt(data.sum_assured);
            self.premium = parseFloat(data.premium);
            self.cpt_id = data.cpt_id;
            self.extra = data.extra;
            self.attr_list = data.score;
            self.attr_list.push([ 'portability',
                {
                    'available' : true,
                    'importance' : 'MID',
                    'title' : 'Portability',
                    'value' : 'Available',
                    'verbose' : 'Portability: A policy holder desirous of porting (shifting) his policy to us ' +
                        'shall apply at least 45 days before the premium renewal date of his existing policy. ' +
                        'The accrued benefits and time bound exclusions will also be transferred without any ' +
                        'interruption. Portability will be provided in accordance to IRDA guidelines issued ' +
                        'from time to time.'
                }
            ]);
            self.attr = {};
            self.general = data.general;

            var medical_summary = {};
            for(var i=0; i < data.general.conditions.length; i++){
                var cond = data.general.conditions[i];
                if(!(cond.period in medical_summary)){
                    medical_summary[cond.period] = [];
                }
                medical_summary[cond.period].push(cond);
            }

            self.medical_summary = [];
            for(var period in medical_summary) {
                var conds = medical_summary[period];
                if(conds.length > 3) {
                    var i=0;
                    for(;i < parseInt(conds.length/3);i++){
                        self.medical_summary.push({
                            'period' : period,
                            'conditions' : conds.slice(3*i, (3*i)+3),
                        });
                    }
                    if(conds.length%3 != 0) {
                        self.medical_summary.push({
                            'period' : period,
                            'conditions' : conds.slice((3*i), conds.length),
                        });
                    }
                } else {
                    self.medical_summary.push({'period' : period, 'conditions' : conds});
                }
            }

            self.ped = data.ped;
            self.standard_exclusions = data.standard_exclusions;
            self.recommended = data.recommended;

            if(self.five_year_projection.length != 5) {
                self.sum_assured_after_5_years = null;
                self.total_premium_paid_in_5_years = null;
            } else {
                self.sum_assured_after_5_years = self.five_year_projection[4][2];
                self.total_premium_paid_in_5_years = 0;
                $.each(self.five_year_projection, function(i, d){
                    self.total_premium_paid_in_5_years += d[1];
                });
            }

            ko.utils.arrayForEach(data.score, function(listd) {
                self.attr[listd[0]] = listd[1];
            });
            self.score = parseInt(data.total_score);
            self.medical_score = parseInt(data.medical_score);
            self.covered_hospitals = data.covered_hospitals;

            self.is_hospital_covered = function(data){
                for(var i=0; i < self.covered_hospitals.length; i++){
                    if(self.covered_hospitals[i].id == data.id){
                        return true;
                    }
                }
                return false;
            };

            self.stars = Array.apply(null, new Array(parseInt(Math.round(self.score/10)/2))).map(function (_, i) {return '';});
            if((parseInt(Math.round(self.score/10)/2)*2) < Math.round(self.score/10)){
                self.stars.push('half-');
            }
            if(self.stars.length < 5){
                Array.apply(null, new Array(5 - self.stars.length)).map(function (_, i) {self.stars.push('empty-')});
            }

            self.plus = Array.apply(null, new Array(parseInt(Math.round(self.medical_score/10)/2))).map(function (_, i) {return '';});
            if((parseInt(Math.round(self.medical_score/10)/2)*2) < Math.round(self.medical_score/10)){
                self.plus.push('half-');
            }
            if(self.plus.length < 5){
                Array.apply(null, new Array(5 - self.plus.length)).map(function (_, i) {self.plus.push('empty-')});
            }
        };

        return {
            Plan: Plan
        };
    }
);
