define(['plugins/router', 'viewmodels/global', 'amplify', 'viewmodels/utils', 'jquerycookie', 'bootstrap-dropdown'], function (router, global, amplify, utils) {
    return {
        router: router,
        global : global,
        activate: function () {
            return router.map([
                { route: ['', 'step1(/:members)'], moduleId: 'viewmodels/new_step1', title: 'Coverfox - Buy health insurance STEP : 1', nav: true, page : 1 },
                { route: 'step2', moduleId: 'viewmodels/step2', title: 'Coverfox - Buy health insurance STEP : 2', nav: true, page : 2  },
                { route: 'step3', moduleId: 'viewmodels/step3', title: 'Coverfox - Buy health insurance STEP : 3', nav: true, page : 3  },
                { route: 'results/:results_event(/:ptype)(/:c1)(/:c2)(/:c3)', moduleId: 'viewmodels/new_results', title: 'Coverfox - Buy health insurance - Results', nav: true, page : 4  },
                { route: 'buy/:transationId', moduleId: 'viewmodels/buy', title: 'Coverfox - Fill your details', nav: true, page : 5  },
                { route: 'medical/:transationId', moduleId: 'viewmodels/medical_required', title: 'Coverfox - Fill your details', nav: true, page : 6  },
                { route: 'offline', moduleId: 'viewmodels/offline', title: 'Coverfox - Buy Offline', nav: true, page : 7 },
                { route: 'age-offline', moduleId: 'viewmodels/age_offline', title: 'Coverfox - Buy Offline', nav: true, page : 8 },
                { route: 'cover-offline', moduleId: 'viewmodels/cover_offline', title: 'Coverfox - Buy Offline', nav: true, page : 9 },
                { route: 'contact/:act_id', moduleId: 'viewmodels/contact', title: 'Coverfox - Schedule A Call', nav: true, page : 10 },
            ]).buildNavigationModel()
            .mapUnknownRoutes('viewmodels/new_step1', 'not-found')
            .activate();
        },
        attached : function (){
            /*ipad bug for virtual keypad and fixed positions on scroll*/
            $(document).on('focus','input', function(){
                setTimeout(function(){
                    $(window).scrollTop($(window).scrollTop()+1);
                }, 200);
            });
            $(document).on('blur', 'input', function(){
                setTimeout(function(){
                    $(window).scrollTop($(window).scrollTop()-1);
                }, 200);
            });


            $(document).ready(function(){

                $('.google_btn').click(function(){
                    gplus_login();
                });

                $('.skip_login').click(function(){
                    AUTHENTICATED = true;
                    on_success('coverfox', null);
                    return false;
                });

                $('.facebook_btn').click(function(){
                    fb_login();
                });

                $(".user-tab li").click(function(){
                    var id_informa = $(this).attr('id');
                    $(".user-tab li").removeClass('active');
                    $('.user-tab-content').addClass('hidden');
                    $('#' + id_informa).addClass('active');
                    $('#' + id_informa + '_div').removeClass('hidden');
                });

                $("#user-type-tab").find("li").click(function (e) {
                    $('#user-type-tab').find('li').removeClass('active');
                    $(this).addClass('active');
                    var target = $(this).data("target");
                    console.log(target);
                    $(".user-tab-pane").hide();
                    $("#"+target).show();
                    e.preventDefault();
                });

                $("#user_registration_form").submit(function(event){
                    $.post(USER_REGISTRATION_URL, $("#user_registration_form").serialize(), function (data) {
                        if(data.success){
                            if(data.response.success){
                                var msg = data.response.mesg;
                                AUTHENTICATED = true;
                                on_success('coverfox', msg.email);
                            }
                        }else {
                            $(".form_errors").text("");
                            for(var k in data.errors){
                                $("#user_reg_error_"+k).text(data.errors[k][0]);
                            }
                        }
                    }, 'json');
                    event.preventDefault();
                });

                $("#user_login_form").submit(function(event){
                    $.post(PRODUCT_USER_LOGIN, $("#user_login_form").serialize(), function (data) {
                        if(data.success){
                            if(data.response.success){
                                var msg = data.response.mesg;
                                AUTHENTICATED = true;
                                on_success('coverfox', msg.email);
                            }else{
                                if(data.response.error_msg){
                                    $('#user_login_error_password').text(data.response.error_msg);
                                }
                            }
                        }else {
                            $(".form_errors").text("");
                            for(var k in data.errors){
                                $("#user_login_error_"+k).text(data.errors[k][0]);
                            }
                        }
                    }, 'json');
                    event.preventDefault();
                })

            });


            var on_success = function(auth_source, email_id) {
                $('#loading').hide();
                $('#registration_modal').addClass('fade');

                // c2c js
                var call_overlay = false;
                setTimeout(function(){
                    $('body').mouseleave(function(){
                        call_overlay = true;
                        setTimeout(function(){
                            if(call_overlay) {
                                $('#c2cbar-overlay').addClass('open');
                                $('.prompt-item-right,.prompt-item-left').addClass('expand');
                            }
                        }, 200);
                    });
                }, 4000);
                $("#c2c_close").click(function(){
                    call_overlay = false;
                    $('#c2cbar-overlay').removeClass('open');
                    $('.prompt-item-right, .prompt-item-left').removeClass('expand');
                    $('body').unbind('mouseleave');
                });

                if(window.jQuery.cookie('profile')) {
                    $('.user-profile-details').removeClass('hide');

                    var uObj = JSON.parse(window.jQuery.cookie('profile'));
                    if(uObj.name) {
                        $('.user-profile-name').text(uObj.name);
                    } else {
                        $('.user-profile-name').text('Logged in');
                    }
                    if(uObj.photo){
                        $('.user-profile-photo').attr('src', uObj.photo);
                    }
                    if(AUTHENTICATED){
                        $('.user-profile-register').addClass('hide');
                    } else {
                        $('.user-profile-register').removeClass('hide');
                    }
                    if(olark) {
                        if (uObj.email){
                            olark('api.visitor.updateEmailAddress', {emailAddress: uObj.email});
                        }
                        if(uObj.name){
                            olark('api.visitor.updateFullName', {fullName: uObj.name});
                        }
                    }
                } else {
                    $('.user-profile-details').addClass('hide');
                }

                //Google conversion js load
                var oldDocWrite = document.write;
                document.write = function(node){
                    $("body").append(node);
                };
                $.getScript("http://www.googleadservices.com/pagead/conversion.js", function() {
                    setTimeout(function() {
                        document.write = oldDocWrite
                    }, 100)
                });
            };
            //TODO: popup on login and change details
            $('.user-profile-register, .user-profile-details').addClass('hide');


            // -------------- Facebook Signin ------------------

            var fb_response_callback = function(response, fb_access_token) {
                var data = {};
                data['fb_data'] = JSON.stringify(response);
                data['csrfmiddlewaretoken'] = CSRF_TOKEN;
                data['facebook_access_token'] = fb_access_token;
                $.post(FACEBOOK_REGISTRATION_URL, data, function(res_data){
                    var data = $.parseJSON(res_data);
                    if (data.success) {
                        AUTHENTICATED = true;
                        on_success('facebook', data.email);
                    }else {
                        AUTHENTICATED = true;
                        on_success('facebook', null);
                    }
                });
            };

            var google_response_callback = function(obj){
                var data = {};
                data['gdata'] = JSON.stringify(obj);
                data['csrfmiddlewaretoken'] = CSRF_TOKEN;
                data['google_code'] = google_auth_result['code'];
                data['google_access_token'] = google_auth_result['access_token'];
                $.post(GOOGLE_REGISTRATION_URL, data, function(res_data){
                    var data = $.parseJSON(res_data);
                    if (data.success) {
                        AUTHENTICATED = true;
                        on_success('google', data.email);
                    }else {
                        AUTHENTICATED = true;
                        on_success('google', null);
                    }
                });
            };

            var google_response_error = function(){
                AUTHENTICATED = true;
                on_success('google', null);
            };

            window.fbAsyncInit = function() {
                FB.init({
                    appId   : '545910808767230',
                    oauth   : true,
                    status  : true, // check login status
                    cookie  : true, // enable cookies to allow the server to access the session
                    xfbml   : true // parse XFBML
                });
            };

            var fb_login = function(){
                FB.login(function(response) {
                    if (response.authResponse) {
                        var fb_access_token = response.authResponse.accessToken;
                        user_id = response.authResponse.userID; //get FB UID

                        FB.api('/me', function(response) {
                            fb_response_callback(response, fb_access_token);
                        });
                    } else {
                        //user hit cancel button
                    }
                }, {
                    scope: 'user_birthday,user_relationships,email'
                })
            };

            (function() {
                var e = document.createElement('script');
                e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
                e.async = true;
                document.getElementById('fb-root').appendChild(e);
            }());

            // -------------- Google Signin ------------------

            function onLoadCallback() {
                gapi.client.setApiKey(GOOGLE_API_KEY); //set your API KEY
            }

            (function() {
                var po = document.createElement('script');
                po.type = 'text/javascript'; po.async = true;
                po.src = 'https://apis.google.com/js/client:plusone.js?onload=onLoadCallback';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(po, s);
            })();

            gplus_login = function() {
                var myParams = {
                    'callback': loginFinishedCallback,
                    'clientid': GOOGLE_OAUTH_KEY,
                    'cookiepolicy': 'single_host_origin',
                    'state' : CSRF_TOKEN,
                    'application_name' : 'coverfox',
                    'scope' : 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email'
                };
                gapi.auth.signIn(myParams);
            };

            var google_auth_result;
            function loginFinishedCallback(authResult) {
                if (authResult) {
                    if(authResult['status']['signed_in']) {
                        if (authResult['error'] == undefined){
                            google_auth_result = authResult;
                            gapi.client.load('plus','v1', loadProfile);  // Trigger request to get the email address.
                        } else {
                            google_response_error();
                        }
                    }
                } else {
                    return;
                }
            }

            function loadProfile() {
                var request = gapi.client.plus.people.get({'userId': 'me'});
                request.execute(google_response_callback);
            };

            setInterval(function(){
                if(olark) {
                    var cd = new Date().toISOString().split(".")[0];
                    var d = cd.split("T")[0].split('-');
                    var t = cd.split("T")[1];
                    olark('api.chat.updateVisitorStatus', {snippet: 'Last seen: ' + [d[2], d[1], d[0]].join('-') + " " + t});
                }
            }, 60000);

            if(!amplify.store('pending')){
                $('.user-profile-pending').addClass('hide');
            }
        }
    };
});
