define(['knockout', 'knockout_validation'], function(ko) {
        ko.validation.init({
            insertMessages: false,
            decorateInputElement : true,
            registerExtenders: true,
            messagesOnModified: true,
            messageTemplate: null,
            errorElementClass : 'error_border'
        });
        'use strict';

        // represent a single member item
        var Member = function (id, person, verbose, selected, age, minAge, maxAge) {
            var self = this;
            self.id = id;
            self.person = person;
            self.verbose = verbose;
            self.display = ko.observable(verbose);
            self.selected = ko.observable(selected);
            self.validate_now = ko.observable(false);
            self.age = ko.observable(age).extend({
                validation: {
                    validator: function(val) {
                        if (self.validate_now()){
                            if(val){
                                return true;
                            } else {
                                return false;
                            }
                        }
                        else {
                            return true;
                        }
                    },
                    message: 'This field is required'
                }
            });
            self.minAge = minAge;
            self.maxAge = maxAge;
        };

        return {
            Member: Member
        };
    }
);
