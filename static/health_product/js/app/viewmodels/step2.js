define(['plugins/router', 'jquery', 'knockout', 'scripts/pincheck', 'viewmodels/global', 'viewmodels/utils', 'knockout_validation', 'jquerytransit'], function(router, $, ko, pincheck, global, utils) {

    ko.validation.init({
        insertMessages: false,
        decorateInputElement : true,
        registerExtenders: true,
        messagesOnModified: true,
        messageTemplate: null,
        errorElementClass : 'error_border',
    });
    var errors = ko.observableArray();
    var HospitalService = function(service) {
        var self = this;
        self.type = ko.observable(service).extend({
            required : {
                message : "Select the type of room you would want in case of hospitalization",
            },
        });

        self.selectService  =  function(service) {
            self.type(service);
        };

        self.isSelected = function(service) {
            if(self.type() == service){
                return true;
            } else {
                return false;
            }
        };
    };

    //Initialization from localstore
    var pincode;
    if(global.pincode()) {
        pincode = ko.observable(global.pincode());
    } else {
        pincode = ko.observable();
    }
    pincode.extend({
        validation : [{
            validator : function(val){
                return pincheck.isValid(val);
            },
            message : "Please enter a valid pincode"
        }]
    });

    if(global.hospital_service()) {
        var HService = new HospitalService(global.hospital_service());
    } else {
        var HService = new HospitalService("");
    }

    var step2_viewsmodel = {
        HService : HService,
        pincode : pincode,
        nextView : function(){
            var verrors = ko.validation.group([pincode, HService.type]);
            verrors.showAllMessages();
            if(verrors().length > 0){
                return false;
            }

            //If pincode changes, empty hosital list completely
            if(global.pincode() != pincode()){
                if(global.step3_proxy()){
                    global.step3_proxy().hospitals([]);
                }
                global.hospitals([]);
            }
            global.pincode(pincode());
            global.hospital_service(HService.type());
            utils.notify(global.step2_notify());
            utils.event_log('Completed Step 2', global.step2_notify());
            router.navigate("step3");
        },
        activate : function(){
        },
        attached : function(view, parent, settings){
            utils.event_log('Visited Step 2');
            $(view).show();
            windowheight = $(window).height();
        }
    };
    global.step2_proxy(step2_viewsmodel);
    return step2_viewsmodel;
});
