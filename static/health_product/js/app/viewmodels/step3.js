define(['plugins/router','jquery', 'knockout', 'viewmodels/global', 'viewmodels/utils', 'viewmodels/step3_model', 'gmaps', 'response', 'knockout_validation', 'typeahead', 'jquerytransit'], function(router, $, ko, global, utils, hospitalModel, gmaps, response) {

    ko.validation.init({
        insertMessages: false,
        decorateInputElement : true,
        registerExtenders: true,
        messagesOnModified: true,
        messageTemplate: null,
        errorElementClass : 'error_border',
    });
    var errors = ko.observableArray();
    var hospitals = ko.observableArray([]).extend({
        validation : [{
            validator : function(val){
                if(val.length > 10) {
                    if(get_selected_hospitals().length < 5){
                        return false;
                    } else {
                        return true;
                    }
                }
            },
            message : "Please select atleast 5 hospitals"
        }]
    });
    var city_name = ko.observable("");
    var pincode;

    var get_selected_hospitals = function(){
        var selected_hospitals = [];
        $.each(hospitals(), function(i, h){
            if(h.selected()){
                selected_hospitals.push({
                    id : h.id,
                    name : h.name,
                    address : h.address,
                    lat : h.lat,
                    lng : h.lng,
                })
            }
        });
        return selected_hospitals;
    };

    var pre_selected_hospital_length = function(){
        var slen = 0;
        $.each(hospitals(), function(i, h){
            if(h.pre_selected){
                slen += 1;
            }
        });
        return slen;
    }

    var map;
    var center_marker;
    var distanceWidget;

    function CanvasProjectionOverlay() {}
    CanvasProjectionOverlay.prototype = new google.maps.OverlayView();
    CanvasProjectionOverlay.prototype.constructor = CanvasProjectionOverlay;
    CanvasProjectionOverlay.prototype.onAdd = function(){};
    CanvasProjectionOverlay.prototype.draw = function(){};
    CanvasProjectionOverlay.prototype.onRemove = function(){};
    var canvasProjectionOverlay;

    var city = hospitalModel.city;

    var infoWindow = new gmaps.maps.InfoWindow();
    var geocoder = new gmaps.maps.Geocoder();

    var markers = new Array();

    var INDIA_CENTER = new gmaps.maps.LatLng(23,79);
    var CITY_CENTER = INDIA_CENTER;

    var selected_hospital_icon = {
        url: "/static/health_product/img/hospital_marker_selected.png",
        size: new gmaps.maps.Size(38, 38),
        origin: new gmaps.maps.Point(0, 0),
        anchor: new gmaps.maps.Point(10, 10),
        scaledSize: new gmaps.maps.Size(19, 19),
        optimized : true,
        animation: gmaps.maps.Animation.DROP,
    };
    var deselected_hospital_icon = {
        url: "/static/health_product/img/hospital_marker.png",
        size: new gmaps.maps.Size(38, 38),
        origin: new gmaps.maps.Point(0, 0),
        anchor: new gmaps.maps.Point(10, 10),
        scaledSize: new gmaps.maps.Size(19, 19),
        optimized : true,
        animation: gmaps.maps.Animation.DROP,
    };

    var hideOverlay = function() {
        $("#HospitalsInfoWindow").hide();
    }

    var showDetails = function(hospital){
        if(hospital.marker){
            $("#HospitalsInfoWindow").show();
            var point = canvasProjectionOverlay.getProjection().fromLatLngToContainerPixel(hospital.marker.getPosition());
            var map_container_width = $('#hospitals-map').width() + parseInt($('#hospitals-map').css('margin-left')) + parseInt($('#hospitals-map').css('margin-right'));
            var map_container_height = $('#hospitals-map').height();
            if(point.x < map_container_width && point.x > 0 && point.y < map_container_height){
                $("#HospitalsInfoWindow").stop().fadeIn("50").html("<div style='width:200px;'><strong>"+hospital.name+"</strong><br/><span style='font-size:12px;'>"+hospital.address+"</span></div>").transition({ top:point.y-25, left:point.x+160, y:'-100%' });
            } else {
                $("#HospitalsInfoWindow").hide();
            }
        } else {
            $("#HospitalsInfoWindow").hide();
        }
    };

    var select = function(hospital){
        hospital.selected(true);
        if(hospital.marker){
            hospital.marker.setIcon(selected_hospital_icon);
            hospital.marker.setAnimation(gmaps.maps.Animation.BOUNCE);
            setTimeout(function(){ hospital.marker.setAnimation(null); }, 2150);
        }
    }

    var deselect = function(hospital){
        hospital.selected(false);
        if(hospital.marker){
            hospital.marker.setIcon(deselected_hospital_icon);
            hospital.marker.setAnimation(gmaps.maps.Animation.BOUNCE);
            setTimeout(function(){ hospital.marker.setAnimation(null); }, 2150);
        }
    }

    var placeHospital = function(hospital){
        if(hospital.lat != null && hospital.lng != null){

            var position = new gmaps.maps.LatLng(hospital.lat, hospital.lng);
            if(hospital.selected()){
                var hospital_icon = selected_hospital_icon;
            } else {
                var hospital_icon = deselected_hospital_icon;
            }
            var marker = new gmaps.maps.Marker({
                map: map,
                icon: hospital_icon,
                position: position
            });
            hospital.marker = marker;
            markers.push(marker);
            gmaps.maps.event.addListener(marker, 'mouseover', function() {
                showDetails(hospital);
            });
            gmaps.maps.event.addListener(marker, 'mouseout', function() {
                hideOverlay();
            });
        }
    }

    var setBounds = function(){
        var bounds = new gmaps.maps.LatLngBounds();
        bounds.extend(CITY_CENTER);
        var added = 0;
        $.each(hospitals(), function(index, h){
            if((h.lat != null) && (h.lng != null)){
                if(added <= 10) {
                    //Gets distance between two coordinates in miles, 3956.6 miles is earth's radius
                    var distance = gmaps.maps.geometry.spherical.computeDistanceBetween(CITY_CENTER, new gmaps.maps.LatLng(h.lat, h.lng), 3956.6);
                    if(distance < 150) { //200 miles
                        var position = new gmaps.maps.LatLng(h.lat, h.lng);
                        bounds.extend(position);
                        added += 1;
                    }
                }
            }
        });
        map.setZoom(15);
        map.fitBounds(bounds);
        google.maps.event.addListenerOnce(map, 'idle', function() {
            map.fitBounds(bounds);
            gmaps.maps.event.clearListeners(map, 'idle');
            //distanceWidget.set('position', map.getCenter());
            //map.setZoom(12);
        });
    }

    var initializeStep = function(){
        /* remove all markers and empty the array */
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];

        var initialized = false;
        if(!$.isEmptyObject(global.hospitals())){
            initialized = true;
            $.each(global.hospitals(), function(i, d){
                var h = new hospitalModel.Hospital(d.id, d.name, d.address, d.lat, d.lng, true, true, false);
                hospitals.push(h);
                placeHospital(h);
            });
        }

        $.post('/health-plan/hospitals/by-pincode/', {'csrfmiddlewaretoken' : CSRF_TOKEN, 'pincode' : pincode}, function(data){
            setTimeout(function(){
                $('#loading #step3_load').hide();
                $("#loading").hide();
            }, 1000);
            data = response.parseResponse(data);
            $.each(data.hospitals, function(index, d){
                pre_selected = false;
                if(initialized) {
                    exists = false;
                    $.each(hospitals(), function(i, hosp){
                        if(hosp.id == d.id){
                            exists = true;
                        }
                    });
                    if(!exists){
                        var h = new hospitalModel.Hospital(d.id, d.name, d.address, d.lat, d.lng, false, false, false);
                        hospitals.push(h);
                        placeHospital(h);
                    }
                } else {
                    var selected = false;
                    if(index < 10) {
                        selected = true;
                        pre_selected = true;
                    }
                    var h = new hospitalModel.Hospital(d.id, d.name, d.address, d.lat, d.lng, selected, pre_selected, false);
                    hospitals.push(h);
                    placeHospital(h);
                }
            });

            city.name = data.city;
            city.type = data.city_type;
            city.id = data.city_id;
            city_name(data.city);
            CITY_CENTER = new gmaps.maps.LatLng(data.city_lat, data.city_lng);
            console.log("MAP PAAN TO ", CITY_CENTER);
            map.panTo(CITY_CENTER);
            center_marker.setMap(null);
            center_marker = new gmaps.maps.Marker({
                map: map,
                position: CITY_CENTER,
                title : "pincode : " + pincode,
            });
            setBounds();
        });
    }

    var findPincode = function() {
        geocoder.geocode({ 'address' : pincode + ", India"}, function(results, status) {
            if (status == gmaps.maps.GeocoderStatus.OK) {
                CITY_CENTER = results[0].geometry.location;
            } else {
                console.log("Google maps fail : ", status);
            }
            //Create map even if you dont find the pincode
            createMap();
        });
    }

    var createMap = function(){
        var styles = [{
            "stylers": [
                { "hue": "#0091ff" },
                { "saturation": -68 }
            ]
        }];
        var styledMap = new gmaps.maps.StyledMapType(styles, {'name' : 'Styled Map'});
        var mapOptions = {
            zoom: 13,
            mapTypeControlOptions : {
                mapTypeIds : [gmaps.maps.MapTypeId.ROADMAP, 'map_ishtyle']
            },
            center: CITY_CENTER,
            panControl: true,
            zoomControl: true,
            scrollwheel: false,
            disableDoubleClickZoom : true,
            draggable : false,
            keyboardShortcuts : false,
        };
        if(!map) {
            map = new gmaps.maps.Map(document.getElementById("hospitals-map"), mapOptions);
            map.mapTypes.set('map_ishtyle', styledMap);
            $(window).resize(function(){
                google.maps.event.trigger(map, 'resize');
                setBounds();
            });
            map.setMapTypeId('map_ishtyle');
            /*distanceWidget = new gmaps.DistanceWidget(map, function(){
                map.setCenter(distanceWidget.position);
            });*/

            canvasProjectionOverlay = new CanvasProjectionOverlay();
            canvasProjectionOverlay.setMap(map);

        }

        if(center_marker) {
            canvasProjectionOverlay.setMap(null);
            center_marker.setMap(null);
        }
        center_marker = new gmaps.maps.Marker({
            map: map,
            position: CITY_CENTER,
            title : "pincode : " + pincode,
        });
        canvasProjectionOverlay.setMap(map);
        //distanceWidget.set('position', CITY_CENTER);

        initializeStep();
    };
    var viewAttached = false;
    var step3_viewsmodel = {
        city_name : city_name,
        hospitals : hospitals,
        select : select,
        deselect : deselect,
        showDetails : showDetails,
        hideOverlay : hideOverlay,
        get_selected_hospitals : get_selected_hospitals,
        pre_selected_hospital_length : pre_selected_hospital_length,
        activate : function(){
            $('#loading .step_loader_text').hide();
            $('#loading #step3_load').show();
            $("#loading").show();
            hospitals([]);
            if(global.pincode()){
                pincode = global.pincode();
            } else {
                pincode = ""; //TODO: Raise error, Push user to step2
                router.navigate('step1');
            }
            if(viewAttached) {
                findPincode();
            }
        },
        nextView: function(){
            utils.event_log('Completed Step 3');
            var verrors = ko.validation.group([hospitals]);
            verrors.showAllMessages();
            if(verrors().length > 0){
                return false;
            }
            global.city(city);
            global.hospitals(get_selected_hospitals());

            global.sum_assured(global.get_recommended_sum_assured());
            $('#loading #results_load').show();
            if(!AUTHENTICATED) {
                $("#registration_modal").removeClass("fade");
            }
            var post_data = global.post_data();
            $.post('/health-plan/get-score/', {'csrfmiddlewaretoken' : CSRF_TOKEN, 'data': JSON.stringify(post_data), 'warmup' : true}, function(res){
                router.navigate("results/"+res.activity_id);
            }, 'json');
        },
        attached : function(view, parent, settings){
            utils.event_log('Visited Step 3');
            var verrors = ko.validation.group([hospitals]);
            verrors.showAllMessages(false);
            $('#page3 #hospitals-map').css('height', $(window).height() - $('msg_container').outerHeight() - 330);
            findPincode();
            $(view).show();

            window.jQuery('.step3 #search_hospital').typeahead({
                minLength : 3,
                name: 'hospitals',
                limit : 30,
                valueKey : 'verbose',
                remote : {
                    url : '/health-plan/hospitals/auto-complete/?csrfmiddlewaretoken=%CSRF_TOKEN&term=%QUERY&type=%TYPE&pin=%PIN&city=%CITY&r=%RANDOM',
                    replace: function(url, query) {
                        //var stype = $("input[name=stype]:radio:checked").val();
                        var stype = 'country';
                        return url.replace('%QUERY', query).replace('%PIN', pincode).replace('%TYPE', stype).replace("%CITY", city.name).replace("%RANDOM", new Date().getTime()).replace("%CSRF_TOKEN", CSRF_TOKEN);
                    },
                    filter : function(data){
                        data = response.parseResponse(data.data);
                        var filtered_response = $.map(data.hospitals, function(h, i){
                            return h;
                        });
                        if(data.hospitals.length == 0){
                            filtered_response = [{'id': -1, 'verbose' : 'No hospitals found'}];
                        }
                        return filtered_response;
                    }
                },
            });
            window.jQuery('.step3 #search_hospital').on('typeahead:selected', function (object, d) {
                if(d.id == -1){
                    window.jQuery('.step3 #search_hospital').typeahead('setQuery','');
                    return true;
                }
                window.jQuery('.step3 #search_hospital').typeahead('setQuery','');
                var h = new hospitalModel.Hospital(d.id, d.name, d.address, d.lat, d.lng, true, true);

                exists = false;
                $.each(hospitals(), function(i, hosp){
                    if(hosp.id == h.id){
                        hosp.selected(true);
                        exists = true;
                    }
                });
                if(!exists){
                    hospitals.unshift(h);
                    placeHospital(h);
                }
            });
            //hospitalSearchWrapper gets height only after typeahead initialization
            $('#page3 #hospitalsList .hospital-list-group').css('height', $('#hospitals-map').outerHeight() - $('#hospitalsList .hospitalSearchWrapper').outerHeight());
            viewAttached = true;
        }
    }
    global.step3_proxy(step3_viewsmodel);
    return step3_viewsmodel;
});
