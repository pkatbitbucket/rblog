define(['knockout'],
    function(ko) {
        'use strict';

        // Singleton for city
        var City = function(id, name, type) {
            var self = this;
            self.id = id;
            self.name = name;
            self.type = type;//Allowed values "METRO", ""
        };
        var city = new City('', '', '');

        // represent a single hospital item
        var Hospital = function(id, name, address, lat, lng, selected, pre_selected, added) {
            this.id = id;
            this.name = name;
            this.address = address;
            this.lat = lat;
            this.lng = lng;
            this.selected = ko.observable(selected);
            this.added = added; //Tracking the ones added later by typeahead
            this.pre_selected = pre_selected; //Tracking the ones selected initially
            this.marker = null;
        };

        return {
            Hospital: Hospital,
            city : city
        };
    }
);
