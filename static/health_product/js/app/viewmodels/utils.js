define([], function() {
    return {
        'notify' : function(message_list) {
            if(olark) { //olark in global namespace
                if(!(message_list instanceof Array)){
                    message_list = [message_list];
                }
                olark('api.chat.sendNotificationToOperator', {body : message_list.join('\n')});
            }
        }
    }
});
