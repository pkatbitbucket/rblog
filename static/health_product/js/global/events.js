define(['mixpanel_actions'], function(actions) {

    // Custom Event Creator

    var cust_ev = function(ev, data) {
        var evt = new CustomEvent(ev, {
            detail: data
        });
        window.dispatchEvent(evt);
    };

    var add_listener = function(evname, func) {
        window.addEventListener(evname, function(e) {
            console.log(e.type, e.detail);
            if (func) {
                func(e);
            }
        });
    };

    // var mix_panel_track = function(event,param){
    //     var param = (param)? param : ''
    //     mixpanel.track(event, param)
    // }


    return {
        cust_ev: cust_ev
    };



});
