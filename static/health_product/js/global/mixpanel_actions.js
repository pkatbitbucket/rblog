
 function MixPanelEvents() {
 	return {
 		AddPassengerCover:'AddPassengerCover',

		'Health_LP_Submitted':{
			name : 'Health LP Submitted',
		},

		'Health_Step_1_Submitted':{
			name : 'Health Step 1 Submitted',
		},

		'Health_Step_2_Submitted':{
			name : 'Health Step 2 Submitted',
		},

		'Health_Step_3_Submitted':{
			name : 'Health Step 3 Submitted',
		},

		'Health_Step_4_Submitted':{
			name : 'Health Step 4 Submitted',
		},

		'Health_Results_Reached':{
			name : 'Health results Reached',
		},

		'Health_Results_Buy':{
			name : 'Health Results Buy',
		},

		'Health_Proposer_1_Submit':{
			name : 'Health Proposer 1 Submit',
		},

		'Health_Proposer_2_Submit':{
			name : 'Health Proposer 2 Submit',
		},

		'Health_Proposer_3_Submit':{
			name : 'Health Proposer 3 Submit',
		},

		'Health_Proposer_4_Submit':{
			name : 'Health Proposer 4 Submit',
		},

		'Health_Payment_Gateway':{
			name : 'Health Payment Gateway',
		},

		'Health_Payment_Success':{
			name : 'Health Payment Success',
		},

		'Health_Payment_Faliure':{
			name : 'Health Payment Faliure',
		},
 	}
}
