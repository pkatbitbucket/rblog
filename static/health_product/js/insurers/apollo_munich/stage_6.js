define(['jquery', 'knockout'], function($, ko) {
        'use strict';

        var check_dict = ko.observableDictionary();

        var transaction_model = ko.observable(null);
        var conditional_array = ko.observableArray();

        var init = function(buyView, curr_stage_id) {
//            console.log('CURRENT STAGE ID >>>>>>>>>>>>>>>>>', curr_stage_id);
            transaction_model(buyView.transaction_model());
            buyView.stage_validations[curr_stage_id] = validation;
        };

        var validation = function(stage_no){
            var validation_list = [];
            var stage = transaction_model().stages()[stage_no-1];

            $.each(stage.segments(), function(j,segment){
                $.each(segment.rows(), function(k, row){
                    $.each(row.elements(), function(l, e){
                        validation_list.push(e);
                    });
                });
            });
            var errors = ko.validation.group(validation_list, {deep : true});

            return {
                valid : $.isEmptyObject(errors()),
                errors : errors,
            };
        }

        return {
            init : init,
            check_dict : check_dict
        };
    }
);
