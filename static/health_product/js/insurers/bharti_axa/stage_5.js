define(['jquery', 'knockout'], function($, ko) {
        'use strict';

        ko.bindingHandlers.MiniMultipleChoiceField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var question_string = '';
                question_string += '<div data-bind="if : ' + valueAccessor().data.only_if + '">';
                question_string += '        <div class="quest-pad-mini col-sm-2 ' + valueAccessor().data.css_class + '">';
                question_string += '            <div data-bind="foreach: obj.options()">';
                question_string += '                <label class="control-label ">';
                question_string += '                    <input data-bind="checked : $parent.obj.value, ' +
                    'attr : {\'value\' : $data.id , \'name\' : $parent.obj.code}' + valueAccessor().data.event +'" type="radio"/>';
                question_string += '                    <span data-bind="text: $data.name"></span>';
                question_string += '                </label>&nbsp;&nbsp;';
                question_string += '            </div>';
                question_string += '          <p data-bind="validationMessage: obj.value" class="error"></p>';
                question_string += '        </div>';
                question_string += '</div>';

                $(element).append(question_string);
                var vmodel = {
                    obj : valueAccessor(),
                    root_context : bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };
        ko.bindingHandlers.MiniDropDownField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var question_string = '';
                question_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '" class="col-sm-2">';
                question_string += '    <div class="quest-pad-mini ' + valueAccessor().data.css_class + '">';
                question_string += '            <select class="form-control" data-bind="options: obj.options(), ' +
                    'optionsText: \'name\', optionsValue: \'id\', value: obj.value' + valueAccessor().data.event + '"></select>';
                question_string += '        <p data-bind="validationMessage: obj.value" class="error"></p>';
                question_string += '    </div>';
                question_string += '</div>';
                $(element).append(question_string);

                var vmodel = {
                    obj : valueAccessor(),
                    root_context : bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };
        var stage_id = ko.observable(null);
        var check_dict = ko.observableDictionary();

        var transaction_model = ko.observable(null);
        var conditional_array = ko.observableArray();

        var init = function(buyView, curr_stage_id) {
            transaction_model(buyView.transaction_model());
            buyView.stage_validations[curr_stage_id] = validation;
            any_yes_check();
        };

        var validation = function(stage_no){
            var validation_list = [];
            var stage = transaction_model().stages()[stage_no-1];

            $.each(stage.segments(), function(j,segment){
                $.each(segment.rows(), function(k, row){
                    $.each(row.elements(), function(l, e){
                        if(e.code() == 'details'){
                            if(check_dict.get(segment.tag())()){
                                validation_list.push(e);
                            }
                        }
                        else {
                            validation_list.push(e);
                        }
                    });
                });
            });
            var errors = ko.validation.group(validation_list, {deep : true});

            return {
                valid : $.isEmptyObject(errors()),
                errors : errors,
            };
        }

        var any_yes_check = function(){
            var which_stage = 'stage_5';

            $.each(check_dict.keys(), function(i, elem){
                check_dict.remove(elem);
            });
            $.each(transaction_model().stages(), function(i, stage){
                if (stage.template() == which_stage){
                    $.each(stage.segments(), function(j, segment){
                        var check_code = segment.segment().split('_question_')[1];
                        if(segment.details().trigger_any_yes_check){
                            $.each(segment.rows(), function(k, row){
                                $.each(row.elements(), function(l, e){
                                    if(e.field_type() == 'MultipleChoiceField'){
                                        if (e.value() == 'Yes'){
                                            //Push the question that triggers conditional questions
                                            if(segment.tag() == 'combo'){
                                                check_code = segment.segment().split('_question_')[1];
                                                check_dict.push(check_code, true);
                                            }else{
                                                check_code = e.code();
                                                check_dict.push(check_code.split('_')[0], true);
                                                //Push the individual element with member code
                                                check_dict.push(check_code, true);
                                                conditional_array.push(check_code);
                                                //Push the conditional question code
                                                check_dict.push(segment.tag(), true);
                                            }
                                        }
                                    } //MultipleChocieEnd
                                    if(e.field_type() == 'DropDownField' && e.value()){
                                        if (e.value() != 'NONE'){
                                            //Push the question that triggers conditional questions
                                            if(segment.tag() == 'combo'){
                                                check_code = segment.segment().split('_question_')[1];
                                                check_dict.push(check_code, true);
                                            }else{
                                                check_code = e.code();
                                                check_dict.push(check_code.split('_')[0], true);
                                                //Push the individual element with member code
                                                check_dict.push(check_code, true);
                                                //Push the conditional question code
                                                check_dict.push(segment.tag(), true);
                                            }
                                        }
                                    } //DropDownField
                                });
                            });
                        }
//                        console.log('>>>>>> TAG >>>>>>>>>', segment.tag());
//                        check_tag(segment.tag(), segment.tag());
                    });
                }
            });
            $.each(transaction_model().stages(), function(i, stage){
                $.each(stage.segments(), function(j, segment){
                    if(segment.details().conditional == 'YES'){
                        var header_flag = false;
                        $.each(segment.rows(), function(k, row){
                            $.each(row.elements(), function(l, e){
                                if($.inArray(e.code().split('_')[0], check_dict.keys()) >= 0){
                                    e.is_required(true);
                                    header_flag = true;
                                }
                                else{
                                    e.is_required(false);
                                }
                            });
                        });
                        if(header_flag){
                            segment.show_header(true);
                        }else{
                            segment.show_header(false);
                        }
                    }
                });
            });

            //Always return true in order to continue processing a radio box
            return true;
        };
        return {
            init : init,
            check_dict : check_dict,
            any_yes_check : any_yes_check,
        };
    }
);
