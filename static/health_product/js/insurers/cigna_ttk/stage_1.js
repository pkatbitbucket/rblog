define(['jquery', 'knockout'], function($, ko) {
        'use strict';

        var last_stage = ko.observable(null);

        var transaction_model = ko.observable(null);
        var is_other_nationality = ko.observable(false);


        var init = function(buyView, curr_stage_id) {
            transaction_model(buyView.transaction_model());
            buyView.stage_validations[curr_stage_id] = validation;
            last_stage(buyView.last_stage());
            $.each(transaction_model().stages(), function(i, stage){
                if(stage.template() == curr_stage_id){
                    var element = stage.segments()[0].rows()[3].elements()[0]
                    if(element.data.code == 'nationality' && element.value() == 'OT'){
                        is_other_nationality(true)
                    }
                    element.value.subscribe(function(new_value){
                        if(new_value=='OT'){
                            is_other_nationality(true);
                        } else {
                            is_other_nationality(false);
                        }
                    })
                }
            })
        };

        var validation = function(stage_no){
            var validation_list = [];
            var stage = transaction_model().stages()[stage_no-1];
            var t_and_c_failed = false;
            $.each(stage.segments(), function(j,segment){
                $.each(segment.rows(), function(k, row){
                    $.each(row.elements(), function(l, e){
                        // to ensure that is_nationality field is taken into validation
                        if(e.data.code == 'nationality_value' && !is_other_nationality()){
                            // do nothing
                        }
                        else {
                            validation_list.push(e);
                        }

                        if(e.code() == 't_and_c' && e.value() == 'No'){
                            t_and_c_failed = true;
                        }
                    });
                });
            });
            if(t_and_c_failed){
                return {
                    valid: false,
                    errors: null,
                    error_message:'You must <strong>agree</strong> to the terms and conditions of the <strong>policy</strong> and the <strong>website</strong>'
                }
            }
            var errors = ko.validation.group(validation_list, {deep : true});

            return {
                valid : $.isEmptyObject(errors()),
                errors : errors,
            };
        }

        return {
            init : init,
            is_other_nationality: is_other_nationality
        };
    }
);
