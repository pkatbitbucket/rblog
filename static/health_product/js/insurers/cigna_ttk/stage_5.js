define(['jquery', 'knockout'], function($, ko) {
        'use strict';

        ko.bindingHandlers.MiniMultipleChoiceField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var question_string = '';
                question_string += '<div data-bind="if : ' + valueAccessor().data.only_if + '">';
                //question_string += '        <div class="quest-pad-mini col-sm-2 ' + valueAccessor().data.css_class + '">';
                question_string += '            <div data-bind="foreach: obj.options()">';
                question_string += '                <label class="control-label ">';
                question_string += '                    <input data-bind="checked : $parent.obj.value, ' +
                    'attr : {\'value\' : $data.id , \'name\' : $parent.obj.code}' + valueAccessor().data.event +'" type="radio"/>';
                question_string += '                    <span data-bind="text: $data.name"></span>';
                question_string += '                </label>&nbsp;&nbsp;';
                question_string += '            </div>';
                question_string += '          <p data-bind="validationMessage: obj.value" class="error"></p>';
                //question_string += '        </div>';
                question_string += '</div>';
                question_string += '<div data-bind="ifnot : ' + valueAccessor().data.only_if + '">';
                question_string += '    NA';
                question_string += '</div>';

                var summary_string = '';
                summary_string += '<div data-bind="if : ' + valueAccessor().data.only_if + '">';
                summary_string += '    <div class="summary-field">';
                summary_string += '        <span data-bind="text: obj.display_value"></span>';
                summary_string += '    </div>';
                summary_string += '</div>';
                summary_string += '<div data-bind="ifnot : ' + valueAccessor().data.only_if + '">';
                summary_string += '    NA';
                summary_string += '</div>';

                $(element).append(allBindingsAccessor().summary ? summary_string : question_string);
                var vmodel = {
                    obj : valueAccessor(),
                    root_context : bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };

        ko.bindingHandlers.MiniDropDownField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var question_string = '';
                question_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                //question_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                question_string += '            <select class="form-control" data-bind="options: obj.options(), ' +
                    'optionsText: \'name\', optionsValue: \'id\', value: obj.value' + valueAccessor().data.event + '"></select>';
                question_string += '        <p data-bind="validationMessage: obj.value" class="error"></p>';
                //question_string += '    </div>';
                question_string += '</div>';
                $(element).append(question_string);

                var vmodel = {
                    obj : valueAccessor(),
                    root_context : bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };

        ko.bindingHandlers.MiniTextField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = "";
                //dyna_string += '<div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                dyna_string += '    <div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '        <label style="font-size:12px;" class="control-label" data-bind="text: obj.data.label"></label>';
                dyna_string += '        <textarea class="form-control" rows="3" data-bind="value: obj.value"></textarea>';
                dyna_string += '      <p data-bind="validationMessage: obj.value" class="error"></p>';
                dyna_string += '    </div>';
                //dyna_string += '</div>';
                $(element).append(dyna_string);
                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };

        var check_dict = ko.observableDictionary();

        var transaction_model = ko.observable(null);
        var conditional_array = ko.observableArray();

        var init = function(buyView, curr_stage_id) {
            transaction_model(buyView.transaction_model());
            buyView.stage_validations[curr_stage_id] = validation;
            any_yes_check();
        };

        var atleast_one_yes_error = [];

        var validation = function(stage_no){
            var validation_list = [];
            var stage = transaction_model().stages()[stage_no-1];

            $.each(stage.segments(), function(j,segment){
                $.each(segment.rows(), function(k, row){
                    $.each(row.elements(), function(l, e){
                        if(e.code() == 'details'){
                            if(check_dict.get(segment.tag())()){
                                validation_list.push(e);
                            }
                        }
                        else {
                            validation_list.push(e);
                        }
                    });
                });
            });
            var errors = ko.validation.group(validation_list, {deep : true});

            var insured_members = transaction_model().insured_members;
            var error_message;
            if(atleast_one_yes_error.length > 0) {
                error_message = '<strong>Oh snap!</strong>&nbsp;&nbsp;On the basis of your answers, following members must say "Yes" to atleast one of the diseases:';
                error_message += '<ul>';
                for(var m = 0; m < atleast_one_yes_error.length; m++)
                    error_message += '<li>' + insured_members.get(atleast_one_yes_error[m])() + '</li>';
                error_message += '</ul>';
            }

            return {
                valid : $.isEmptyObject(errors()) && (atleast_one_yes_error.length === 0),
                errors : errors,
                error_message: error_message
            };
        };

        var any_yes_check = function(){
            var which_stage = 'stage_5';
            var current_stage;
            var conditional_yes_answers = [];

            $.each(check_dict.keys(), function(i, elem){
                check_dict.remove(elem);
            });
            $.each(transaction_model().stages(), function(i, stage){
                if (stage.template() == which_stage){
                    current_stage = stage;
                    $.each(stage.segments(), function(j, segment){
                        var check_code = segment.segment().split('_question_')[1];
                        if(segment.details().trigger_any_yes_check){
                            $.each(segment.rows(), function(k, row){
                                $.each(row.elements(), function(l, e){
                                    if(e.field_type() == 'MultipleChoiceField'){
                                        if (e.value() == 'Yes'){
                                            //Push the question that triggers conditional questions
                                            if(segment.details().conditional == 'NO'){
                                                check_code = e.code();
                                                check_dict.push(check_code.split('_')[0], true);
                                                //Push the individual element with member code
                                                //check_dict.push(check_code, true);
                                                conditional_array.push(check_code);
                                                //Push the conditional question code
                                                //check_dict.push(segment.tag(), true);
                                            }
                                        }
                                    } //MultipleChocieEnd
                                    if(e.field_type() == 'DropDownField' && e.value()){
                                        if (e.value() != 'NONE'){
                                            //Push the question that triggers conditional questions
                                            if(segment.tag() == 'combo'){
                                                check_code = segment.segment().split('_question_')[1];
                                                check_dict.push(check_code, true);
                                            }else{
                                                check_code = e.code();
                                                check_dict.push(check_code.split('_')[0], true);
                                                //Push the individual element with member code
                                                check_dict.push(check_code, true);
                                                //Push the conditional question code
                                                check_dict.push(segment.tag(), true);
                                            }
                                        }
                                    } //DropDownField
                                });
                            });
                        }
                        else {
                            $.each(segment.rows(), function(k, row){
                                $.each(row.elements(), function(l, e){
                                    if (e.value() == 'Yes'){
                                        if(segment.details().conditional == 'YES'){
                                                conditional_yes_answers.push(e.code().split('_')[0]);
                                        }
                                    }
                                });
                            });
                        }
//                        console.log('>>>>>> TAG >>>>>>>>>', segment.tag());
//                        check_tag(segment.tag(), segment.tag());
                    });
                }
            });
            atleast_one_yes_error = check_dict.keys();
            for(var i = 0; i < atleast_one_yes_error.length; ++i) {
                var k = atleast_one_yes_error[i];
                if(conditional_yes_answers.indexOf(k) != -1) {
                    atleast_one_yes_error.pop(k);
                }
            }
            $.each(transaction_model().stages(), function(i, stage){
                $.each(stage.segments(), function(j, segment){
                    if(segment.details().conditional == 'YES'){
                        var header_flag = false;
                        $.each(segment.rows(), function(k, row){
                            $.each(row.elements(), function(l, e){
                                if($.inArray(e.code().split('_')[0], check_dict.keys()) >= 0){
                                    e.is_required(true);
                                    header_flag = true;
                                }
                                else{
                                    e.is_required(false);
                                }
                            });
                        });
                        if(header_flag){
                            segment.show_header(true);
                        }else{
                            segment.show_header(false);
                        }
                    }
                });
            });

            //Always return true in order to continue processing a radio box
            return true;
        };


        return {
            init : init,
            any_yes_check : any_yes_check,
            check_dict : check_dict
        };
    }
);
