define(['jquery','knockout', 'knockout_validation'], function($,ko) {
//define(['knockout', 'knockout_validation', 'jquery-datepicker'], function(ko) {
        ko.validation.init({
            insertMessages: false,
            decorateInputElement: true,
            registerExtenders: true,
            messagesOnModified: true,
            messageTemplate: null,
            errorElementClass: 'contains-error'
        });
        'use strict';
        var DateTextField = function(data){
            var self = this;
            self.field_type = ko.observable('DateTextField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.value = ko.observable(data.default).extend({
                validation: {
                    validator: function(val) {
                        if(!self.is_required()){return true;}
                        val=val.split("/");
                        val=val[1]+"/"+val[0]+"/"+val[2];
                        return (new Date(val)) != "Invalid Date";
                    },
                    message: 'Please enter a valid date in DD/MM/YYYY format'
                }
            }
            );
            self.display_value = ko.computed(function() {
                return this.value();
            }, this);
        }

        var DateField = function(data) {
            var self = this;
            self.format = "dd/mm/yyyy";
            self.display_format = "dd-M-yyyy";
            self.field_type = ko.observable('DateField');
            self.data = data;

            var dp = $.datepick;
            var dparse = dp.parseDate.bind(dp);
            var dformat = dp.formatDate.bind(dp);

            if(data.default_date){
                self.default_date = data.default_date && dparse(self.format, data.default_date);
            }
            else if(data.default_age) {
                self.default_date= new Date();
                self.default_date.setFullYear(self.default_date.getFullYear() - data.default_age);
            }
            var min_date = data.min_date && dparse(self.format, data.min_date);
            if(data.max_age) {
                var min_dob = dp.add(dp.add(dp.today(), -data.max_age,'y'), 1, 'd');
                min_date = (min_date && (min_date > min_dob)) ? min_date : min_dob;
            }
            self.min_date = ko.observable(min_date);

            var max_date = data.max_date && dparse(self.format, data.max_date);
            if(data.min_age || data.min_age === 0) {
                var max_dob = dp.add(dp.today(), -data.min_age,'y');
                max_date = (max_date && (max_date < max_dob)) ? max_date : max_dob;
            }
            self.max_date = ko.observable(max_date);

            self.trigger = '<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>';
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.value = ko.observable(data.default).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                },
                validation: {
                    validator: function(val) {
                        if(!val) {
                            return !self.is_required();
                        }
                        var d, min_date, max_date;
                        try {
                            d = dparse(self.format, val);
                        }
                        catch(err) {
                            this.message = 'Please enter a valid date';
                            return false;
                        }
                        if(self.min_date()) {
                            is_valid = (d >= self.min_date());
                            min_date = dformat(self.display_format, self.min_date());
                            this.message = is_valid ? '': 'Please select a date on or after ' + min_date;
                        }
                        if(self.max_date()) {
                            is_valid = is_valid && (d <= self.max_date());
                            max_date = dformat(self.display_format, self.max_date());
                            this.message = is_valid ? '': 'Please select a date on or before ' + max_date;
                        }
                        if(!is_valid && self.min_date() && self.max_date()) {
                            this.message = 'Please select a date between ' + min_date + ' and ' + max_date;
                        }
                        if(!is_valid) {
                            this.message = '';
                            setTimeout(function(){ self.value(''); } ,50)
                        }
                        return is_valid;
                    },
                    message: 'Please enter a valid date (Allowed age: ' + data.min_age + ' to ' + data.max_age + ')'
                }
            });
            self.age = ko.computed(function(){
                if(self.value()){
                    var mmddyy = self.value().split('/');
                    var ddmmyy = mmddyy[1]+'/'+mmddyy[0]+'/'+mmddyy[2];
                    var birth = new Date(ddmmyy);
                    var today = new Date();
                    var age_str, age_in_years,
                    ydiff = today.getFullYear() - birth.getFullYear(),
                    mdiff = today.getMonth() - birth.getMonth(),
                    ddiff = today.getDate() - birth.getDate();

                    age_in_years = ydiff;
                    if(mdiff < 0 || (mdiff == 0 && ddiff < 0)) --age_in_years;
                    if (age_in_years > 0) {
                        age_str = age_in_years + (age_in_years == 1 ? ' year' : ' years');
                    }
                    else {
                        if(ydiff == 1)  mdiff = 12 + mdiff;
                        var age_mths = mdiff - (ddiff >= 0 ? 0 : 1);
                        age_str = age_mths + (age_mths == 1 ? ' mth' : ' mths');
                    }
                    return age_str;
                }
                else return null;
            });
            self.display_value = ko.pureComputed({
                read: function() {
                    var d = dparse(self.format, self.value());
                    return dformat(self.display_format, d);
                },
                write: function(val) {
                    try {
                        var d = dparse(self.display_format, val);
                        val = dformat(self.format, d);
                        self.value(val);
                    }
                    catch(err) {
                        self.value(val);
                    }
                },
            });
        };

        var IntegerField = function(data){
            var self = this;
            self.field_type = ko.observable('IntegerField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            var validations = {
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                },
                validation: {
                    validator: function(val) {
                        if(self.is_required()){
                            if(val && val.trim() != val) {
                                val = val.trim();
                                self.value(val);
                            }
                            var integer = /^[0-9]+$/;
                            var decimal = /^[0-9]+(\.[0-9]+)?$/;
                            if(integer.test(val)){
                                return true;
                            }
                            else if(decimal.test(val)) {
                                self.value(Math.round(val).toString());
                                return true;
                            }
                            else {
                                this.message = "Please enter a valid number";
                                return false;
                            }
                        }
                        else return true;
                    }
                }
            };
            if(data.max_length)
                validations.maxLength = data.max_length;
            self.value = ko.observable(data.default).extend(validations);
            self.sub_text = ko.observable(data.sub_text);
            self.display_value = ko.computed(function() {
                var res = this.value();
                if(data.sub_text){
                    res = this.value() + " " + this.sub_text();
                }
                return res
            }, this);
        };
        var MobileField = function(data){
            var self = this;
            self.field_type = ko.observable('MobileField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.value = ko.observable(data.default).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                },
                pattern: {
                    message: 'Please enter a valid phone number.',
                    params: /^[9,8,7]{1}\d{9}$/
                }
            });
            self.display_value = ko.computed(function() {
                return this.value();
            }, this);
        };

        ko.extenders.uppercase = function(target, option) {
            target.subscribe(function(newValue) {
                target(newValue.toUpperCase());
            });
            return target;
        };

        var PANField = function(data){
            var self = this;
            self.field_type = ko.observable('PANField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.value = ko.observable(data.default).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                },
                pattern: {
                    message: 'Please enter a valid PAN(in capital letters) No.',
                    params: /(?:[A-Z]{5}[0-9]{4}[A-Z]{1}$)/
                },
                uppercase: true
            });
            self.display_value = ko.computed(function() {
                return this.value();
            }, this);
        };

        var PANorAdhaarField = function(data){
            var self = this;
            self.field_type = ko.observable('PANorAdhaarField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.value = ko.observable(data.default).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                },
                pattern: {
                    message: 'Please enter a valid PAN(in capital letters)/Aadhaar Card No.',
                    params: /(?:[A-Z]{5}[0-9]{4}[A-Z]{1}$|^\d{12}$)/
                },
                uppercase: true
            });
            self.display_value = ko.computed(function() {
                return this.value();
            }, this);
        };

        var LandlineField = function(data){
            var self = this;
            self.field_type = ko.observable('LandlineField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.std_code = ko.observable(data.default_std_code);
            self.landline = ko.observable(data.default_landline);
            self.display_value = ko.computed(function(){
                var res = '';
                if(self.std_code() || self.landline()){
                    res = parseInt(self.std_code() + self.landline());
                }
                return res;
            }).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                },
                pattern: {
                    message: 'Please enter a valid 10 digit phone number.',
                    params: /^D?(\d{10})$/
                }
            });
        };

        var CharField = function(data){
            var self = this;
            self.field_type = ko.observable('CharField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.value = ko.observable(data.default).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                },
                minLength: data.min_length,
                maxLength: data.max_length,
                email: data.email
            });
            self.sub_text = ko.observable(data.sub_text);
            self.display_value = ko.computed(function() {
                var res = this.value();
                if(data.sub_text){
                    res = this.value() + " " + this.sub_text();
                }
                return res
            }, this);
        };

        var TextField = function(data){
            var self = this;
            self.field_type = ko.observable('TextField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.value = ko.observable(data.default).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                },
                minLength: data.min_length,
                maxLength: data.max_length
            });
            self.display_value = ko.computed(function() {
                return this.value();
            }, this);
        };

        var NameField = function(data){
            var self = this;
            self.field_type = ko.observable('NameField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.value = ko.observable(data.default).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                },
                minLength: data.min_length,
                maxLength: data.max_length,
                pattern: {
                    message: 'Only alphabets and spaces are allowed.',
                    params: /^[a-zA-Z ]*$/
                },
                validation: {
                    validator: function(val) {
                        if(val !== val.trim()) self.value(val.trim());
                        return true;
                    }
                }
            });
            self.sub_text = ko.observable(data.sub_text);
            self.display_value = ko.computed(function() {
                var res = this.value();
                if(data.sub_text){
                    res = this.value() + " " + this.sub_text();
                }
                return res
            }, this);
        };

        var StrictCharField = function(data){
            var self = this;
            self.field_type = ko.observable('StrictCharField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.value = ko.observable(data.default).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                },
                minLength: data.min_length,
                maxLength: data.max_length,
                pattern: {
                    message: 'Non-alphabet characters are not allowed',
                    params: /^[a-zA-Z]*$/
                }
            });
            self.sub_text = ko.observable(data.sub_text);
            self.display_value = ko.computed(function() {
                var res = this.value();
                if(data.sub_text){
                    res = this.value() + " " + this.sub_text();
                }
                return res
            }, this);
        };

        var MultipleChoiceField = function(data){
            var self = this;
            self.field_type = ko.observable('MultipleChoiceField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.options = ko.observableArray(data.options);
            self.value = ko.observable(data.default).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                }
            });
            self.display_value = ko.computed(function(){
                var res = self.value();
                $.each(self.options(), function(i, elem){
                    if(self.value() == elem.id){
                        res = elem.name;
                    }
                });
                return res;
            });
        };

        var MultipleAnswerField = function(data){
            var self = this;
            self.field_type = ko.observable('MultipleAnswerField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.options = ko.observableArray(data.options);
            self.value = ko.observable(data.default).extend({
                required: {
                    onlyIf: function(){
                        return self.is_required();
                    }
                }
            });
            self.display_value = ko.computed(function(){
                var res = self.value();
                $.each(self.options(), function(i, elem){
                    if(self.value() == elem.id){
                        res = elem.name;
                    }
                });
                return res;
            });
        };

        var DropDownField = function(data){
            var self = this;
            self.field_type = ko.observable('DropDownField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.template_option = ko.observable(data.template_option);
            self.is_required = ko.observable(data.required);
            self.options = ko.observableArray(data.options);
            self.value = ko.observable(data.default).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                }
            });
            self.display_value = ko.computed(function(){
                var res = self.value();
                $.each(self.options(), function(i, elem){
                    if(self.value() == elem.id){
                        res = elem.name;
                    }
                });
                return res;
            });
        };



        var SearchableDropDownField = function(data){
            var self = this;
            self.field_type = ko.observable('SearchableDropDownField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.options = ko.observableArray(data.options);

            self.show_dropdown = ko.observable(false);
            self.has_input_focus = ko.observable(false);
            self.input_value = ko.observable();

            self.value = ko.observable(data.default).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                }
            });
            self.display_value = ko.computed(function(){
                var res = self.value();
                for(var i=0;i<self.options().length;++i){
                    if(self.value() == self.options()[i].id){
                        return self.options()[i].name;
                    }
                }
                return null;
            });

            self.filtered_items = ko.computed(function(){
                if(!self.input_value()){
                    return self.options();
                }
                var arr = [];
                $.each(self.options(),function(i,option){
                    var split_array = option.name.split(' ');
                    var pat = "^"+self.input_value();
                    var reg = new RegExp(pat,'i');
                    for(var i=0;i<split_array.length;i++){
                        if(reg.test(split_array[i])){
                            arr.push(option);
                            break;
                        }
                    }
                });
                return arr;
            });
            self.toggleDropDown = function(){
                self.show_dropdown(!self.show_dropdown());
                self.has_input_focus(self.show_dropdown());
                self.input_value("");
            }
            self.valueSelected = function(data){
                self.value(data.id);
                self.show_dropdown(false);
            }
        };


        var TwoDropDownField = function(data){
            var self = this;
            self.field_type = ko.observable('TwoDropDownField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.options1 = ko.observableArray(data.options1);
            self.options2 = ko.observableArray(data.options2);
            self.value1 = ko.observable(data.default1).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                }
            });
            self.display_value1 = ko.computed(function(){
                var res = self.value1();
                $.each(self.options1(), function(i, elem){
                    if(self.value1() == elem.id){
                        res = elem.name;
                    }
                });
                return res;
            });
            self.value2 = ko.observable(data.default2).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                }
            });
            self.display_value2 = ko.computed(function(){
                var res = self.value2();
                $.each(self.options2(), function(i, elem){
                    if(self.value2() == elem.id){
                        res = elem.name;
                    }
                });
                return res;
            });
        };


        var DropDownDependantField = function(data){
            var self = this;
            self.field_type = ko.observable('DropDownDependantField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.options1 = ko.observableArray(data.options1);
            self.options2 = ko.observableArray(data.options2);
            self.value1 = ko.observable(data.default1).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                }
            });
            self.display_value1 = ko.computed(function(){
                var res = self.value1();
                $.each(self.options1(), function(i, elem){
                    if(self.value1() == elem.id){
                        res = elem.name;
                    }
                });
                return res;
            });
            self.dependant_value = ko.computed(function(){
                var res = 0;
                $.each(self.options2(), function(i, elem){
                    if(self.value1() == elem.id){
                        res = elem.name;
                    }
                });
                return res;
            });
        };

        var TwoSearchableDDDField = function(data){
            var self = this;
            self.field_type = ko.observable('TwoSearchableDDDField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.options1 = ko.observableArray(data.options1);
            self.options2 = ko.observableArray(data.options2);

            self.show_dropdown1 = ko.observable(false);
            self.has_input_focus1 = ko.observable(false);

            self.show_dropdown2 = ko.observable(false);
            self.has_input_focus2 = ko.observable(false);

            self.input_value1 = ko.observable();
            self.input_value2 = ko.observable();

            self.value1 = ko.observable(data.default1).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                }
            });
            self.display_value1 = ko.computed(function(){
                var res = self.value1();
                $.each(self.options1(), function(i, elem){
                    if(self.value1() == elem.id){
                        res = elem.name;
                    }
                });
                return res;
            });
            self.dependant_options = ko.computed(function(){
                var res = 0;
                $.each(self.options2(), function(i, elem){
                    if(self.value1() == elem.id){
                        res = elem.name;
                    }
                });
                return res;
            });
            self.dependant_value = ko.observable(data.default2).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                }
            });
            self.display_value2 = ko.computed(function(){
                var res = self.dependant_value();
                $.each(self.options2(), function(i, elem){
                    if(self.value1() == elem.id){
                        $.each(elem.name, function(j, dep){
                            if(self.dependant_value() == dep.id){
                                res = dep.name;
                            }
                        });
                    }
                });
                return res;
            });


            self.filtered_items1 = ko.computed(function(){
                if(!self.input_value1()){
                    return self.options1();
                }
                var arr = [];
                $.each(self.options1(),function(i,option){
                    var split_array = option.name.split(' ');
                    var pat = "^"+self.input_value1();
                    var reg = new RegExp(pat,'i');
                    for(var i=0;i<split_array.length;i++){
                        if(reg.test(split_array[i])){
                            arr.push(option);
                            break;
                        }
                    }
                });
                return arr;
            });

            self.filtered_items2 = ko.computed(function(){
                if(!self.input_value2()){
                    return self.dependant_options();
                }
                var arr = [];
                $.each(self.dependant_options(),function(i,option){
                    var split_array = option.name.split(' ');
                    var pat = "^"+self.input_value2();
                    var reg = new RegExp(pat,'i');
                    for(var i=0;i<split_array.length;i++){
                        if(reg.test(split_array[i])){
                            arr.push(option);
                            break;
                        }
                    }
                });
                return arr;
            });

            self.toggleDropDown1 = function(){
                self.show_dropdown1(!self.show_dropdown1());
                self.has_input_focus1(self.show_dropdown1());
                self.input_value1("");
            }
            self.value1Selected = function(data){
                self.value1(data.id);
                self.dependant_value(null);
                self.show_dropdown1(false);
                self.show_dropdown2(true);
                self.has_input_focus2(true);
            }

            self.toggleDropDown2 = function(){
                self.show_dropdown2(!self.show_dropdown2());
                self.has_input_focus2(self.show_dropdown2());
                self.input_value2("");
            }
            self.value2Selected = function(data){
                self.show_dropdown2(false);
                self.dependant_value(data.id);
            }
        };


        var TwoDropDownDependantField = function(data){
            var self = this;
            self.field_type = ko.observable('TwoDropDownDependantField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.options1 = ko.observableArray(data.options1);
            self.options2 = ko.observableArray(data.options2);
            self.value1 = ko.observable(data.default1).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                }
            });
            self.display_value1 = ko.computed(function(){
                var res = self.value1();
                $.each(self.options1(), function(i, elem){
                    if(self.value1() == elem.id){
                        res = elem.name;
                    }
                });
                return res;
            });
            self.dependant_options = ko.computed(function(){
                var res = 0;
                $.each(self.options2(), function(i, elem){
                    if(self.value1() == elem.id){
                        res = elem.name;
                    }
                });
                return res;
            });
            self.dependant_value = ko.observable(data.default2).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                }
            });
            self.display_value2 = ko.computed(function(){
                var res = self.dependant_value();
                $.each(self.options2(), function(i, elem){
                    if(self.value1() == elem.id){
                        $.each(elem.name, function(j, dep){
                            if(self.dependant_value() == dep.id){
                                res = dep.name;
                            }
                        });
                    }
                });
                return res;
            });
        };

        var PremiumDiscountField = function(data) {
            var self = this;
            self.field_type = ko.observable('PremiumDiscountField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.options1 = ko.observableArray(data.options1);
            self.options2 = ko.observableArray(data.options2);
            self.base_premiums = ko.observable(data.base_premiums);
            self.discounted_premiums = ko.observable(data.discounted_premiums);
            self.premium = data.premium;
            self.value1 = ko.observable(data.default1).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                }
            });
            self.display_value1 = ko.computed(function(){
                var res = self.value1();
                $.each(self.options1(), function(i, elem){
                    if(self.value1() == elem.id){
                        res = elem.name;
                    }
                });
                return res;
            });
            self.dependant_value = ko.computed(function(){
                var res = 0;
                $.each(self.options2(), function(i, elem){
                    if(self.value1() == elem.id){
                        res = elem.name;
                    }
                });
                return res;
            });
            self.undiscounted_premium = ko.computed(function(){
                return self.base_premiums()[self.value1()];
            });
            self.actual_premium = ko.computed(function(){
                return self.discounted_premiums()[self.value1()];
            });

            var update_premium = function(val) {
                transaction_model().premium(val);
                if(self.value1() == 1){transaction_model().subscription('ANNUAL PREMIUM');}
                if(self.value1() == 2){transaction_model().subscription('TWO YEARS PREMIUM');}
                if(self.value1() == 3){transaction_model().subscription('THREE YEARS PREMIUM');}
            };
            self.actual_premium.subscribe(update_premium);

            // Update premium once object is assigned to transaction_model observable
            var tmodel_subscription = transaction_model.subscribe(function() {
                if(transaction_model() && transaction_model().premium) {
                    update_premium(self.actual_premium());
                    tmodel_subscription.dispose();
                }
            });
        };

        var MultipleSelectField = function(data){
            var self = this;
            self.field_type = ko.observable('MultipleSelectField');
            self.data = data;
            self.code = ko.observable(data.code);
            self.is_required = ko.observable(data.required);
            self.options = ko.observableArray(data.options);
            self.value = ko.observable(data.default).extend({
                required: {
                    onlyIf: function() {
                        return self.is_required();
                    }
                }
            });
            self.display_value = ko.computed(function(){
                return self.value() ? self.value().join(', ') : '';
            });
            self.summary_value = self.display_value();
        };

        ko.bindingHandlers.DateTextField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = "";
                dyna_string += '<div data-bind="visible: '+valueAccessor().data.only_if +'">';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                dyna_string += '        <label class="control-label" data-bind="text: obj.data.label"></label>';
                dyna_string += '        <input type="text" class="form-control" data-bind="value: obj.value" placeholder="DD/MM/YYYY">';
                dyna_string += '      <p data-bind="validationMessage: obj.value" class="error"></p>';
                dyna_string += '    </div>';
                dyna_string += '</div>';

                var summary_string = "";
                summary_string += '<div data-bind="visible: '+valueAccessor().data.only_if +'">';
                summary_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                summary_string += '        <strong data-bind="text: obj.data.label"></strong>';
                summary_string += '        <div data-bind="text: obj.display_value"></div>';
                summary_string += '    </div>';
                summary_string += '</div>';

                $(element).append(allBindingsAccessor().summary ? summary_string : dyna_string);
                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };

        ko.bindingHandlers.FreeField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = "";
                dyna_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                dyna_string += '        <label class="control-label" data-bind="text: obj.data.label"></label>';
                dyna_string += '        <input type="text" class="form-control" data-bind="value: obj.value">';
                dyna_string += '      <p data-bind="validationMessage: obj.value" class="error"></p>';
                dyna_string += '    </div>';
                dyna_string += '</div>';

                var static_string = "";
                static_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                static_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                static_string += '        <label class="control-label" data-bind="text: obj.data.label"></label>';
                static_string += '        <div data-bind="if: obj.value">';
                static_string += '          <div class="summary-field" data-bind="text: obj.value"></div>';
                static_string += '        </div>';
                static_string += '        <div data-bind="ifnot: obj.value">';
                static_string += '        -';
                static_string += '        </div>';
                static_string += '      <p data-bind="validationMessage: obj.value" class="error"></p>';
                static_string += '    </div>';
                static_string += '</div>';

                var question_string = '';
                question_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                question_string += '    <div class="row mnone qstn">';
                question_string += '        <div class="quest-pad bottom-margin col-sm-10">';
                question_string += '            <span class="q-point">Q</span>';
                question_string += '            <span data-bind="html: obj.data.label"></span>';
                question_string += '            <div data-bind="if: obj.data.tooltip">';
                question_string += '                <a style="position:relative;" class="toolsy_tippy" ' +
                    'data-bind="attr: {\'data-original-title\': obj.data.tooltip}">';
                question_string += '                    <span class="toolbadge">?</span>';
                question_string += '                </a>';
                question_string += '            </div>';
                question_string += '        </div>';
                question_string += '        <div class="quest-pad col-sm-4 ' + valueAccessor().data.css_class + '">';
                question_string += '        <input type="text" class="form-control" data-bind="value: obj.value">';
                question_string += '          <p data-bind="validationMessage: obj.value" class="error"></p>';
                question_string += '        </div>';
                question_string += '    </div>';
                question_string += '</div>';

                var summary_string = "";
                summary_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                summary_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                summary_string += '        <strong data-bind="text: obj.data.label"></strong>';
                summary_string += '        <div data-bind="text: obj.value"></div>';
                summary_string += '    </div>';
                summary_string += '</div>';

                if(allBindingsAccessor().summary) {
                    $(element).append(summary_string);
                }
                else if (valueAccessor().data.template_option == 'static') {
                    $(element).append(static_string);
                }
                else if (valueAccessor().data.template_option == 'question') {
                    $(element).append(question_string);
                }
                else{
                    $(element).append(dyna_string);
                }
                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };

        ko.bindingHandlers.DateField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = '';
                dyna_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                dyna_string += '        <label class="control-label" data-bind="text: obj.data.label"></label>';
                dyna_string += '<span data-bind="if: obj.age()"> (Age: <strong data-bind="text: obj.age"></strong>)</span><br/>';
                dyna_string += '        <div class="input-group date">';
                dyna_string += '            <input type="text" class="form-control" data-bind="datepicker: obj.display_value, dpFormat: obj.display_format, dpStartDate:obj.min_date, dpEndDate: obj.max_date, dpTrigger: obj.trigger,dpDefaultDate: obj.default_date, value:obj.display_value">';
                dyna_string += '        </div>';
                dyna_string += '    <p data-bind="validationMessage: obj.value" class="error"></p>';
                dyna_string += '    </div> ';
                dyna_string += '</div> ';

                var static_string = '';
                static_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                static_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                static_string += '      <label class="control-label" data-bind="text: obj.data.label"></label>';
                static_string += '<span data-bind="if: obj.age()"> (<strong data-bind="text: obj.age()"></strong>)</span><br/>';
                static_string += '       <div class="summary-field" data-bind="text: obj.display_value"></div>';
                static_string += '   </div>';
                static_string += '</div>';

                if (valueAccessor().data.template_option == 'static'){
                    $(element).append(static_string);
                }else{
                    $(element).append(dyna_string);
                }
                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };



        ko.bindingHandlers.SearchableDropDownField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = '';
                dyna_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                dyna_string += '        <label class="control-label" data-bind="text: obj.data.label"></label>';
                dyna_string += '<div class="chosen-container-cover" data-bind="css:{\'active\':obj.show_dropdown()}, click: obj.toggleDropDown">';
                dyna_string += '<div class="chosen-container" data-bind="css:{\'active\':obj.show_dropdown()}, click: function() {}, clickBubble: false">';
                dyna_string += '    <div class="chosen-single" data-bind="click: obj.toggleDropDown">';
                dyna_string += '        <span data-bind="text: obj.display_value() ? obj.display_value(): \'Select\'"></span>';
                dyna_string += '        <div><b></b></div>';
                dyna_string += '    </div>';
                dyna_string += '    <div class="chosen-results" data-bind="visible:obj.show_dropdown">';
                dyna_string += '        <input  data-bind="textInput: obj.input_value,hasFocus: obj.has_input_focus"></input>';
                dyna_string += '        <div class="no-results-error" data-bind="visible: obj.filtered_items().length == 0,html:\'No results match \<strong\>\'+ obj.input_value()+ \'\<strong\>\'">';
                dyna_string += '        </div>';
                dyna_string += '        <ul data-bind="foreach: obj.filtered_items">';
                dyna_string += '            <li data-bind="text: $data.name,click: $parent.obj.valueSelected"></li>';
                dyna_string += '        </ul>';
                dyna_string += '    </div>';
                dyna_string += '</div>';
                dyna_string += '</div>';
                dyna_string += '        <p data-bind="validationMessage: obj.value" class="error"></p>';
                dyna_string += '    </div>';
                dyna_string += '</div>';

                var question_string = '';
                question_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                question_string += '    <div class="row mnone qstn">';
                question_string += '        <div class="quest-pad bottom-margin col-sm-10">';
                question_string += '            <span class="q-point">Q</span>';
                question_string += '            <span data-bind="html: obj.data.label"></span>';
                question_string += '            <div data-bind="if: obj.data.tooltip">';
                question_string += '                <a style="position:relative;" class="toolsy_tippy" ' +
                    'data-bind="attr: {\'data-original-title\': obj.data.tooltip}">';
                question_string += '                    <span class="toolbadge">?</span>';
                question_string += '                </a>';
                question_string += '            </div>';
                question_string += '        </div>';
                question_string += '        <div class="quest-pad col-sm-'+ valueAccessor().data.span +' '
                    + valueAccessor().data.css_class + '">';
                question_string += '            <select class="form-control" data-bind="options: obj.options(), ' +
                    'optionsText: \'name\', optionsValue: \'id\', value: obj.value' + valueAccessor().data.event + '"></select>';
                question_string += '            <p data-bind="validationMessage: obj.value" class="error"></p>';
                question_string += '        </div>';
                question_string += '    </div>';
                question_string += '</div>';

                if (valueAccessor().data.template_option == 'static') {
                    $(element).append(static_string);
                }
                else if (valueAccessor().data.template_option == 'question') {
                    $(element).append(question_string);
                }
                else{
                    $(element).append(dyna_string);
                }
                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };
        ko.bindingHandlers.DropDownField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = '';
                dyna_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                dyna_string += '        <label class="control-label" data-bind="text: obj.data.label"></label>';
                dyna_string += '        <select class="form-control" data-bind="options: obj.options(), ' +
                    'optionsText: \'name\', optionsValue: \'id\', value: obj.value' + valueAccessor().data.event + ', optionsCaption: \'Select\'"></select>';
                dyna_string += '        <p data-bind="validationMessage: obj.value" class="error"></p>';
                dyna_string += '    </div>';
                dyna_string += '</div>';

                var static_string = '';
                static_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                static_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                static_string += '        <label class="control-label" data-bind="text: obj.data.label"></label>';
                static_string += '        <div class="summary-field"  data-bind="text:obj.display_value"></div>';
                static_string += '        <p data-bind="validationMessage: obj.value" class="error"></p>';
                static_string += '    </div>';
                static_string += '</div>';

                var question_string = '';
                question_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                question_string += '    <div class="row mnone qstn">';
                question_string += '        <div class="quest-pad bottom-margin col-sm-10">';
                question_string += '            <span class="q-point">Q</span>';
                question_string += '            <span data-bind="html: obj.data.label"></span>';
                question_string += '            <div data-bind="if: obj.data.tooltip">';
                question_string += '                <a style="position:relative;" class="toolsy_tippy" ' +
                    'data-bind="attr: {\'data-original-title\': obj.data.tooltip}">';
                question_string += '                    <span class="toolbadge">?</span>';
                question_string += '                </a>';
                question_string += '            </div>';
                question_string += '        </div>';
                question_string += '        <div class="quest-pad col-sm-'+ valueAccessor().data.span +' '
                    + valueAccessor().data.css_class + '">';
                question_string += '            <select class="form-control" data-bind="options: obj.options(), ' +
                    'optionsText: \'name\', optionsValue: \'id\', value: obj.value' + valueAccessor().data.event + '"></select>';
                question_string += '            <p data-bind="validationMessage: obj.value" class="error"></p>';
                question_string += '        </div>';
                question_string += '    </div>';
                question_string += '</div>';

                if (valueAccessor().data.template_option == 'static') {
                    $(element).append(static_string);
                }
                else if (valueAccessor().data.template_option == 'question') {
                    $(element).append(question_string);
                }
                else{
                    $(element).append(dyna_string);
                }
                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };

        ko.bindingHandlers.MultipleChoiceField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = '';
                dyna_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                dyna_string += '        <div data-bind="if: obj.data.tooltip">';
                dyna_string += '            <label class="control-label" data-bind="html: obj.data.label"></label>';
                dyna_string += '            <a style="position:relative;" class="toolsy_tippy" ' +
                    'data-bind="attr: {\'data-original-title\': obj.data.tooltip}">';
                dyna_string += '                <span class="toolbadge">?</span>';
                dyna_string += '            </a>';
                dyna_string += '        </div>';
                dyna_string += '        <div data-bind="ifnot: obj.data.tooltip">';
                dyna_string += '            <label class="control-label" data-bind="html: obj.data.label"></label>';
                dyna_string += '        </div>';
                dyna_string += '        <div data-bind="if: obj.data.helptext">';
                dyna_string += '            <p class="text-muted" data-bind="html: obj.data.helptext"></p>';
                dyna_string += '        </div>';
                dyna_string += '        <div data-bind="foreach: obj.options() ">';
                dyna_string += '            <label class="control-label ">';
                dyna_string += '                <input data-bind="checked: $parent.obj.value, ' +
                    'attr: {\'value\': $data.id , \'name\': $parent.obj.code}" type="radio"/>';
                dyna_string += '                <span data-bind="text: $data.name"></span>';
                dyna_string += '            </label>&nbsp;&nbsp;';
                dyna_string += '        </div>';
                dyna_string += '      <p data-bind="validationMessage: obj.value" class="error"></p>';
                dyna_string += '    </div>';
                dyna_string += '</div>';

                var question_string = '';
                question_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                question_string += '    <div class="row mnone">';
                question_string += '        <div class="quest-pad col-sm-10">';
                question_string += '            <span class="q-point">Q</span>';
                question_string += '            <span data-bind="html: obj.data.label"></span>';
                question_string += '            <div data-bind="if: obj.data.helptext">';
                question_string += '                <p class="text-muted" data-bind="text: obj.data.helptext"></p>';
                question_string += '            </div>';
                question_string += '            <div data-bind="if: obj.data.tooltip">';
                question_string += '                <a style="position:relative;" class="toolsy_tippy" ' +
                    'data-bind="attr: {\'data-original-title\': obj.data.tooltip}">';
                question_string += '                    <span class="toolbadge">?</span>';
                question_string += '                </a>';
                question_string += '            </div>';
                question_string += '        </div>';
                question_string += '        <div style="padding-bottom:0px;margin-bottom:0px;" class="quest-pad col-sm-10 ' + valueAccessor().data.css_class + '">';
                question_string += '            <div data-bind="foreach: obj.options()">';
                question_string += '                <label class="control-label ">';
                question_string += '                    <input data-bind="checked: $parent.obj.value, ' +
                    'attr: {\'value\': $data.id , \'name\': $parent.obj.code}' + valueAccessor().data.event +'" type="radio"/>';
                question_string += '                    <span data-bind="text: $data.name"></span>';
                question_string += '                </label>';
                question_string += '            </div>';
                question_string += '        <label class="control-label" style="margin-top:10px;" data-bind="html: obj.data.post_text"></label>';
                question_string += '          <p data-bind="validationMessage: obj.value" class="error"></p>';
                question_string += '        </div>';
                question_string += '    </div>';
                question_string += '</div>';

                var term_string = '';
                term_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                term_string += '    <div class="item">';
                term_string += '        <div class="pointer">';
                term_string += '            <img class="img-responsive" src="/static/health_product/img/new_buy/arrow-right-circle.png"/>';
                term_string += '        </div>';
                term_string += '        <div class="agree-field">';
                term_string += '            <span data-bind="html: obj.data.label"></span>';
                term_string += '            <div data-bind="if: obj.data.helptext">';
                term_string += '                <p class="text-muted" data-bind="text: obj.data.helptext"></p>';
                term_string += '            </div>';
                term_string += '            <div data-bind="if: obj.data.tooltip">';
                term_string += '                <a style="position:relative;" class="toolsy_tippy" ' +
                    'data-bind="attr: {\'data-original-title\': obj.data.tooltip}">';
                term_string += '                <span class="toolbadge">?</span>';
                term_string += '                </a>';
                term_string += '            </div>';
                term_string += '            <div data-bind="foreach: obj.options()">';
                term_string += '                <div class="radio">';
                term_string += '                    <label>';
                term_string += '                        <input data-bind="checked: $parent.obj.value, ' +
                    'attr: {\'value\': $data.id , \'name\': $parent.obj.code}" type="radio"/>';
                term_string += '                        <span data-bind="text: $data.name"></span>';
                term_string += '                    </label>';
                term_string += '                </div>';
                term_string += '            </div>';
                term_string += '              <p data-bind="validationMessage: obj.value" class="error"></p>';
                term_string += '        </div>';
                term_string += '    </div>';
                term_string += '</div>';

                if (valueAccessor().data.template_option == 'static') {
                    $(element).append(static_string);
                }
                else if (valueAccessor().data.template_option == 'question'){
                    $(element).append(question_string);
                }
                else if (valueAccessor().data.template_option == 'terms'){
                    $(element).append(term_string);
                }
                else{
                    $(element).append(dyna_string);
                }
                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };

        ko.bindingHandlers.MultipleAnswerField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = '';
                dyna_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span +' '
                    + valueAccessor().data.css_class + '">';
                dyna_string += '        <label class="control-label" data-bind="text: obj.data.label"></label>';
                dyna_string += '        <div data-bind="if: obj.data.tooltip">';
                dyna_string += '            <a style="position:relative;" class="toolsy_tippy" ' +
                    'data-bind="attr: {\'data-original-title\': obj.data.tooltip}">';
                dyna_string += '            <span class="toolbadge">?</span>';
                dyna_string += '            </a>';
                dyna_string += '        </div>';
                dyna_string += '        <div data-bind="foreach: obj.options()">';
                dyna_string += '            <label class="control-label ">';
                dyna_string += '                <input data-bind="checked: $parent.obj.value, ' +
                    'attr: {\'value\': $data.id, \'name\': \'question_\' + $parent.obj.code}" type="checkbox"/>';
                dyna_string += '                <span data-bind="text:$data.name"></span>';
                dyna_string += '            </label>&nbsp;&nbsp;';
                dyna_string += '     </div>';
                dyna_string += '      <p data-bind="validationMessage: obj.value" class="error"></p>';
                dyna_string += '    </div>';
                dyna_string += '</div>';

                var question_string = '';
                question_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                question_string += '    <div class="row mnone qstn">';
                question_string += '        <div class="quest-pad bottom-margin col-sm-10">';
                question_string += '            <span class="q-point">Q</span>';
                question_string += '            <span data-bind="html: obj.data.label"></span>';
                question_string += '            <div data-bind="if: obj.data.tooltip">';
                question_string += '                <a style="position:relative;" class="toolsy_tippy" ' +
                    'data-bind="attr: {\'data-original-title\': obj.data.tooltip}">';
                question_string += '                    <span class="toolbadge">?</span>';
                question_string += '                </a>';
                question_string += '            </div>';
                question_string += '        </div>';
                question_string += '        <div class="quest-pad col-sm-10 ' + valueAccessor().data.css_class + '">';
                question_string += '            <div data-bind="foreach: obj.options()">';
                question_string += '                <label class="control-label ">';
                question_string += '                <input data-bind="checked: $parent.value, ' +
                    'attr: {\'value\': $data.id, \'name\': \'question_\' + $parent.obj.code}' + valueAccessor().data.event + '" type="checkbox"/>';
                question_string += '                <span data-bind="text:$data.name"></span>';
                question_string += '                </label>&nbsp;&nbsp;';
                question_string += '            </div>';
                question_string += '          <p data-bind="validationMessage: obj.value" class="error"></p>';
                question_string += '        </div>';
                question_string += '    </div>';
                question_string += '</div>';

                if (valueAccessor().data.template_option == 'static') {
                    $(element).append(static_string);
                }
                else if (valueAccessor().data.template_option == 'question') {
                    $(element).append(question_string);
                }
                else{
                    $(element).append(dyna_string);
                }
                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };

        ko.bindingHandlers.TextField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = "";
                dyna_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                dyna_string += '        <label class="control-label" data-bind="text: obj.data.label"></label>';
                dyna_string += '        <textarea class="form-control" rows="3" data-bind="value: obj.value"></textarea>';
                dyna_string += '      <p data-bind="validationMessage: obj.value" class="error"></p>';
                dyna_string += '    </div>';
                dyna_string += '</div>';

                var question_string = '';
//                question_string += '<h2 data-bind="text: ko.toJSON(root_context) "></h2>';
                question_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                question_string += '    <div class="row mnone qstn">';
                question_string += '        <div class="quest-pad bottom-margin col-sm-10">';
                question_string += '            <span class="q-point">Q</span>';
                question_string += '            <span data-bind="html: obj.data.label"></span>';
                question_string += '            <div data-bind="if: obj.data.tooltip">';
                question_string += '                <a style="position:relative;" class="toolsy_tippy" ' +
                    'data-bind="attr: {\'data-original-title\': obj.data.tooltip}">';
                question_string += '                    <span class="toolbadge">?</span>';
                question_string += '                </a>';
                question_string += '            </div>';
                question_string += '            <div data-bind="if: obj.data.helptext">';
                question_string += '                <p class="text-muted" data-bind="text: obj.data.helptext"></p>';
                question_string += '            </div>';
                question_string += '        </div>';
                question_string += '        <div class="quest-pad col-sm-10 ' + valueAccessor().data.css_class + '">';
                question_string += '        <textarea class="form-control" rows="3" data-bind="value: obj.value"></textarea>';
                question_string += '          <p data-bind="validationMessage: obj.value" class="error"></p>';
                question_string += '        </div>';
                question_string += '    </div>';
                question_string += '</div>';

                if (valueAccessor().data.template_option == 'static') {
                    $(element).append(static_string);
                }
                else if (valueAccessor().data.template_option == 'question') {
                    $(element).append(question_string);
                }
                else{
                    $(element).append(dyna_string);
                }
                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };

        ko.bindingHandlers.MobileField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = "";
                dyna_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span + ' bottom-margin">';
                dyna_string += '    <label class="control-label" data-bind="text:obj.data.label"></label>';
                //dyna_string += '    </div>';
                //dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span +' bottom-margin">';
                dyna_string += '        <div class="input-group">';
                dyna_string += '            <span class="input-group-addon">+91</span>';
                dyna_string += '            <input type="text" class="form-control" data-bind="value: obj.value">';
                dyna_string += '        </div>';
                dyna_string += '        <span class="small">enter your 10 digit mobile number</span>';
                dyna_string += '        <p data-bind="validationMessage: obj.value" class="error">This field is required</p>';
                dyna_string += '    </div>';
                dyna_string += '</div>';
                $(element).append(dyna_string);
                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };

        ko.bindingHandlers.PANField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = "";
                dyna_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span + ' bottom-margin">';
                dyna_string += '    <label class="control-label" data-bind="text:obj.data.label"></label>';
                dyna_string += '            <input type="text" class="form-control" data-bind="value: obj.value" style="text-transform: uppercase;">';
                dyna_string += '        <p data-bind="validationMessage: obj.value" class="error">This field is required</p>';
                dyna_string += '    </div>';
                dyna_string += '</div>';
                $(element).append(dyna_string);
                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };

        ko.bindingHandlers.PANorAdhaarField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = "";
                dyna_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span + ' bottom-margin">';
                dyna_string += '    <label class="control-label" data-bind="text:obj.data.label"></label>';
                dyna_string += '            <input type="text" class="form-control" data-bind="value: obj.value" style="text-transform: uppercase;">';
                dyna_string += '        <p data-bind="validationMessage: obj.value" class="error">This field is required</p>';
                dyna_string += '    </div>';
                dyna_string += '</div>';
                $(element).append(dyna_string);
                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };

        ko.bindingHandlers.LandlineField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = "";
                dyna_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span + '">';
                dyna_string += '        <label class="control-label" data-bind="text:obj.data.label"></label>';
                dyna_string += '    </div>';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                dyna_string += '        <div class="row">';
                dyna_string += '            <div class="col-xs-4">';
                dyna_string += '                <input type="text" class="form-control" ' +
                    'placeholder="STD" data-bind="value: obj.std_code">';
                dyna_string += '                <span class="small">e.g. "022"</span>';
                dyna_string += '            </div>';
                dyna_string += '            <div class="col-xs-8">';
                dyna_string += '                <input type="text" class="form-control" placeholder="number" ' +
                    'data-bind="value: obj.landline, valueUpdate: [\'input\', \'afterkeydown\']" >';
                dyna_string += '                <span class="small">enter your number</span>';
                dyna_string += '            </div>';
                dyna_string += '          <p data-bind="validationMessage: obj.display_value" class="error"></p>';
                dyna_string += '        </div>';
                dyna_string += '    </div>';
                dyna_string += '</div>';
                $(element).append(dyna_string);
                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };



        ko.bindingHandlers.TwoDropDownField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = '';
                dyna_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                dyna_string += '        <label class="control-label" data-bind="text: obj.data.label"></label>';
                dyna_string += '        <div class="row m-max-wdth">';
                dyna_string += '            <div style="padding:0px 5px 5px 15px;" class="col-xs-'+ valueAccessor().data.span1 +'">';
                dyna_string += '                <select class="form-control" data-bind="options: obj.options1, ' +
                    'optionsText: \'name\', optionsValue: \'id\', value: obj.value1, ' +
                    'optionsCaption: \'Select\' "></select>';
                dyna_string += '            <p data-bind="validationMessage: obj.value1" class="error"></p>';
                dyna_string += '            </div>';
                dyna_string += '            <div style="padding:0px 15px 5px 5px;" class="col-xs-'+ valueAccessor().data.span2 +'">';
                dyna_string += '                <select class="form-control" data-bind="options: obj.options2, ' +
                    'optionsText: \'name\', optionsValue: \'id\', value: obj.value2, ' +
                    'optionsCaption: \'Select\' "></select>';
                dyna_string += '              <p data-bind="validationMessage: obj.value2" class="error"></p>';
                dyna_string += '            </div>';
                dyna_string += '        </div>';
                dyna_string += '    </div>';
                dyna_string += '</div>';
                $(element).append(dyna_string);

                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };

        ko.bindingHandlers.DropDownDependantField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = '';
                dyna_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                dyna_string += '        <div data-bind="if: obj.data.tooltip">';
                dyna_string += '            <label class="control-label" data-bind="html: obj.data.label"></label>';
                dyna_string += '            <a style="position:relative;" class="toolsy_tippy" ' +
                    'data-bind="attr: {\'data-original-title\': obj.data.tooltip}">';
                dyna_string += '                <span class="toolbadge">?</span>';
                dyna_string += '            </a>';
                dyna_string += '        </div>';
                dyna_string += '        <div data-bind="ifnot: obj.data.tooltip">';
                dyna_string += '            <label class="control-label" data-bind="html: obj.data.label"></label>';
                dyna_string += '        </div>';
                dyna_string += '        <div data-bind="text: obj.data.helptext" class="text-muted"></div>';
                dyna_string += '        <div class="row">';
                dyna_string += '            <div style="padding:0px 5px 0px 15px;" class="col-sm-6 dependant-tably-field">';
                dyna_string += '            <label data-bind="text: obj.data.main_label"></label>';
                dyna_string += '                <select class="form-control" data-bind="options: obj.options1, ' +
                    'optionsText: \'name\', optionsValue: \'id\', value: obj.value1, ' +
                    'optionsCaption: \'Select\' "></select>';
                dyna_string += '            <p data-bind="validationMessage: obj.value1" class="error"></p>';
                dyna_string += '            </div>';
                dyna_string += '            <div style="padding:0px 15px 0px 5px;" class="col-sm-6 dependant-tably-field">';
                dyna_string += '                <label data-bind="text: obj.data.dep_label" style="margin-bottom:10px;"></label>';
                dyna_string += '                <strong><span data-bind="text: obj.dependant_value" style="margin-left:15px;"></span>%</strong>';
                dyna_string += '            </div>';
                dyna_string += '        </div>';
                dyna_string += '    </div>';
                dyna_string += '</div>';
                $(element).append(dyna_string);

                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };

        ko.bindingHandlers.TwoDropDownDependantField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = '';
                dyna_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                dyna_string += '        <div class="row">';
                dyna_string += '            <div class="col-sm-'+ valueAccessor().data.span1 +'">';
                dyna_string += '            <label class="control-label" data-bind="text: obj.data.main_label"></label>';
                dyna_string += '                <select class="form-control" data-bind="options: obj.options1, ' +
                'optionsText: \'name\', optionsValue: \'id\', value: obj.value1, ' +
                'optionsCaption: \'Select\' "></select>';
                dyna_string += '            <p data-bind="validationMessage: obj.value1" class="error"></p>';
                dyna_string += '            </div>';
                dyna_string += '            <div class="col-sm-'+ valueAccessor().data.span2 +'">';
                dyna_string += '                <label class="control-label" data-bind="text: obj.data.dep_label"></label>';
                dyna_string += '                <select class="form-control" data-bind="options: obj.dependant_options, ' +
                'optionsText: \'name\', optionsValue: \'id\', value: obj.dependant_value, ' +
                'optionsCaption: \'Select\' "></select>';
                dyna_string += '            </div>';
                dyna_string += '        </div>';
                dyna_string += '    </div>';
                dyna_string += '</div>';
                $(element).append(dyna_string);

                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };



        ko.bindingHandlers.TwoSearchableDDDField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = '';
                dyna_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                dyna_string += '        <div class="row">';
                dyna_string += '            <div style="margin-bottom:15px;" class="col-sm-'+ valueAccessor().data.span1 +'">';
                dyna_string += '            <label data-bind="text: obj.data.main_label" style="font-weight: normal;"></label>';
                dyna_string += '<div class="chosen-container" data-bind="css:{\'active\':obj.show_dropdown1()}">';
                dyna_string += '    <div class="chosen-single" data-bind="click: obj.toggleDropDown1">';
                dyna_string += '        <span data-bind="text: obj.display_value1() ? obj.display_value1(): \'Select\'"></span>';
                dyna_string += '        <div><b></b></div>';
                dyna_string += '    </div>';
                dyna_string += '    <div class="chosen-results" data-bind="visible:obj.show_dropdown1">';
                dyna_string += '        <input  data-bind="textInput: obj.input_value1,hasFocus: obj.has_input_focus1"></input>';
                dyna_string += '        <div class="no-results-error" data-bind="visible: obj.filtered_items1().length == 0,html:\'No results match \<strong\>\'+ obj.input_value1()+ \'\<strong\>\'"></div>';
                dyna_string += '        <ul data-bind="foreach: obj.filtered_items1">';
                dyna_string += '            <li data-bind="text: $data.name,click: $parent.obj.value1Selected"></li>';
                dyna_string += '        </ul>';
                dyna_string += '    </div>';
                dyna_string += '</div>';
                dyna_string += '            <p data-bind="validationMessage: obj.value1" class="error"></p>';
                dyna_string += '            </div>';
                dyna_string += '            <div style="margin-bottom:15px;" class="col-sm-'+ valueAccessor().data.span2 +'">';
                dyna_string += '                <label data-bind="text: obj.data.dep_label" style="font-weight: normal;"></label>';
                dyna_string += '<div class="chosen-container" data-bind="css:{\'active\': obj.show_dropdown2()}">';
                dyna_string += '    <div class="chosen-single" data-bind="click: obj.toggleDropDown2">';
                dyna_string += '        <span data-bind="text: obj.display_value2() ? obj.display_value2(): \'Select\'"></span>'
                dyna_string += '        <div><b></b></div>';
                dyna_string += '    </div>';
                dyna_string += '    <div class="chosen-results" data-bind="visible:obj.show_dropdown2">';
                dyna_string += '        <input  data-bind="textInput: obj.input_value2,hasFocus: obj.has_input_focus2"></input>';
                dyna_string += '        <div class="no-results-error" data-bind="visible: obj.filtered_items2().length == 0,html:\'No results match \<strong\>\'+ obj.input_value2()+ \'\<strong\>\'"></div>';
                dyna_string += '        <ul data-bind="foreach: obj.filtered_items2">';
                dyna_string += '            <li data-bind="text: $data.name,click: $parent.obj.value2Selected"></li>';
                dyna_string += '        </ul>';
                dyna_string += '    </div>';
                dyna_string += '</div>';
                dyna_string += '            <p data-bind="validationMessage: obj.dependant_value" class="error"></p>';
                dyna_string += '            </div>';
                dyna_string += '        </div>';
                dyna_string += '    </div>';
                dyna_string += '</div>';
                $(element).append(dyna_string);

                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };


        ko.bindingHandlers.PremiumDiscountField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = '';
                dyna_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '    <div style="margin-left:10px;" class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                dyna_string += '        <div class="row">';
                dyna_string += '            <div style="padding:0px 5px 0px 15px;" class="col-sm-4 dependant-tably-field">';
                dyna_string += '            <label data-bind="text: obj.data.main_label"></label>';
                dyna_string += '                <select style="position:relative;left:-10px;" class="form-control" data-bind="options: obj.options1, ' +
                    'optionsText: \'name\', optionsValue: \'id\', value: obj.value1"></select>';
                dyna_string += '            <p data-bind="validationMessage: obj.value1" class="error"></p>';
                dyna_string += '            </div>';
                dyna_string += '            <div style="padding:0px 5px 0px 5px;" class="col-sm-2 dependant-tably-field">';
                dyna_string += '                <label data-bind="text: obj.data.dep_label" style="margin-bottom:10px;"></label>';
                dyna_string += '                <strong><span data-bind="text: obj.dependant_value"></span>%</strong>';
                dyna_string += '            </div>';
                dyna_string += '            <div style="padding:0px 5px 0px 5px;" class="col-sm-6 dependant-tably-field">';
                dyna_string += '                <label data-bind="text: obj.data.dep_label1" style="margin-bottom:10px;"></label>';
                dyna_string += '                <strong><span data-bind="visible: obj.undiscounted_premium()!=obj.actual_premium()"><strike data-bind="inr_currency: obj.undiscounted_premium"></strike>&nbsp;</span><span data-bind="inr_currency: obj.actual_premium"></span></strong>';
                dyna_string += '            </div>';
                dyna_string += '        </div>';
                dyna_string += '    </div>';
                dyna_string += '</div>';

                var summary_string = '';
                summary_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                summary_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                summary_string += '        <div class="summary-row">';
                summary_string += '            <div style="padding:0px 5px 0px 10px;" class="col-sm-4 dependant-tably-field">';
                summary_string += '                <label class="summary-label" data-bind="text: obj.data.main_label"></label>';
                summary_string += '                <div data-bind="text: obj.display_value1"></div>';
                summary_string += '            </div>';
                summary_string += '            <div style="padding:0px 5px 0px 10px;" class="col-sm-3 dependant-tably-field">';
                summary_string += '                <label class="summary-label" data-bind="text: obj.data.dep_label"></label>';
                summary_string += '                <div><span data-bind="text: obj.dependant_value"></span>%</div>';
                summary_string += '            </div>';
                summary_string += '            <div style="padding:0px 15px 0px 10px;" class="col-sm-5 dependant-tably-field">';
                summary_string += '                <label class="summary-label" data-bind="text: obj.data.dep_label1"></label>';
                summary_string += '                <div><strike data-bind="visible: obj.undiscounted_premium() != obj.actual_premium()"><span data-bind="inr_currency: obj.undiscounted_premium"></span></strike>&nbsp;<span data-bind="inr_currency: obj.actual_premium"></span></div>';
                summary_string += '            </div>';
                summary_string += '        </div>';
                summary_string += '    </div>';
                summary_string += '</div>';


                $(element).append(allBindingsAccessor().summary ? summary_string : dyna_string);
                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };


        ko.bindingHandlers.MultipleSelectField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = '';
                dyna_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                dyna_string += '        <label class="control-label" data-bind="text: obj.data.label"></label>';
                dyna_string += '        <select class="form-control" data-bind="options: obj.options(), ' +
                    'optionsText: \'name\', optionsValue: \'id\', value: obj.value' + valueAccessor().data.event + ', optionsCaption: \'Select\'" multiple="true"></select>';
                dyna_string += '        <p data-bind="validationMessage: obj.value" class="error"></p>';
                dyna_string += '    </div>';
                dyna_string += '</div>';

                var summary_string = '';
                summary_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                if(!allBindingsAccessor().mini_field) {
                    summary_string += '    <div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                    summary_string += '        <label class="summary-label" data-bind="text: obj.data.label"></label>';
                }
                summary_string += '        <div data-bind="text: obj.summary_value"></div>';
                if(!allBindingsAccessor().mini_field) {
                    summary_string += '    </div>';
                }
                summary_string += '</div>';

                var question_string = '';
                question_string += '<div data-bind="if: ' + valueAccessor().data.only_if + '">';
                question_string += '    <div class="row mnone qstn">';
                question_string += '        <div class="quest-pad bottom-margin col-sm-10">';
                question_string += '            <span class="q-point">Q</span>';
                question_string += '            <span data-bind="html: obj.data.label"></span>';
                question_string += '            <div data-bind="if: obj.data.tooltip">';
                question_string += '                <a style="position:relative;" class="toolsy_tippy" ' +
                    'data-bind="attr: {\'data-original-title\': obj.data.tooltip}">';
                question_string += '                    <span class="toolbadge">?</span>';
                question_string += '                </a>';
                question_string += '            </div>';
                question_string += '        </div>';
                question_string += '        <div class="quest-pad col-sm-'+ valueAccessor().data.span +' '
                    + valueAccessor().data.css_class + '">';
                question_string += '            <select class="form-control" data-bind="options: obj.options(), ' +
                    'optionsText: \'name\', optionsValue: \'id\', value: obj.value' + valueAccessor().data.event + '" multiple="true"></select>';
                question_string += '            <p data-bind="validationMessage: obj.value" class="error"></p>';
                question_string += '        </div>';
                question_string += '    </div>';
                question_string += '</div>';

                if(allBindingsAccessor().summary) {
                    $(element).append(summary_string);
                }
                else if (valueAccessor().data.template_option == 'static') {
                    $(element).append(static_string);
                }
                else if (valueAccessor().data.template_option == 'question') {
                    $(element).append(question_string);
                }
                else{
                    $(element).append(dyna_string);
                }
                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };
        return {
            DateTextField: DateTextField,
            DateField: DateField,
            IntegerField: IntegerField,
            CharField: CharField,
            TextField: TextField,
            MobileField: MobileField,
            PANField: PANField,
            PANorAdhaarField: PANorAdhaarField,
            LandlineField: LandlineField,
            NameField: NameField,
            StrictCharField: StrictCharField,
            DropDownField: DropDownField,
            SearchableDropDownField:SearchableDropDownField,
            DropDownDependantField: DropDownDependantField,
            TwoDropDownDependantField: TwoDropDownDependantField,
            TwoSearchableDDDField: TwoSearchableDDDField,
            TwoDropDownField: TwoDropDownField,
            PremiumDiscountField: PremiumDiscountField,
            MultipleChoiceField: MultipleChoiceField,
            MultipleAnswerField: MultipleAnswerField,
            MultipleSelectField: MultipleSelectField
        };


    }
);
