define(['jquery', 'knockout'], function($, ko) {
        'use strict';

        var last_stage = ko.observable(null);

        var transaction_model = ko.observable(null);

        var init = function(buyView, curr_stage_id) {
            transaction_model(buyView.transaction_model());
            buyView.stage_validations[curr_stage_id] = validation;
            last_stage(buyView.last_stage());
        };

        var validation = function(stage_no){
            var validation_list = [];
            var stage = transaction_model().stages()[stage_no-1];
            var t_and_c_failed = false;
            $.each(stage.segments(), function(j,segment){
                $.each(segment.rows(), function(k, row){
                    $.each(row.elements(), function(l, e){
                        validation_list.push(e);
                        if(e.code() == 't_and_c' && e.value() == 'No'){
                            t_and_c_failed = true;
                        }
                    });
                });
            });
            if(t_and_c_failed){
                return {
                    valid: false,
                    errors: null,
                    error_message:'You must <strong>agree</strong> to the terms and conditions of the <strong>policy</strong> and the <strong>website</strong>'
                }
            }
            var errors = ko.validation.group(validation_list, {deep : true});

            return {
                valid : $.isEmptyObject(errors()),
                errors : errors,
            };
        }

        return {
            init : init
        };
    }
);
