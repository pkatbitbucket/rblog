define(['jquery', 'knockout', 'insurers/custom_fields', 'scripts/pincheck', 'hasher', 'knockout_validation', 'knockout_dictionary', 'bootstrap-tooltip','custom'], function($, ko, cf, pincheck, hasher) {

    ko.validation.init({
        insertMessages: false,
        decorateInputElement : true,
        registerExtenders: true,
        messagesOnModified: true,
        messageTemplate: null,
        grouping: { deep: true }
    });

    function Member(tag){
        var self = this;
        self.dob = ko.observable();
        self.tag = ko.observable(tag);
        self.age_dict = ko.observable();
        self.previous_age = ko.computed(function(){
            if(self.age_dict()){
                var page = self.age_dict()['previous_age'];
                return (page.years > 0 ) ? page.years + " Years" : page.months + " Months";
            }
            else{ return null; }
        });
        self.current_age = ko.computed(function(){
            if(self.age_dict()){
                var cage = self.age_dict()['current_age'];
                return (cage.years > 0 ) ? cage.years + " Years" : cage.months + " Months";
            }
            else{ return null; }
        });
        self.first_name = ko.observable();
        self.last_name = ko.observable();
    }

    var transaction_id = ko.observable();
    var terms_and_conditions="";
    var policy_date_disclaimer = ko.observable();
    var active_stage = ko.observable(1);
    var last_stage = ko.observable();
    var medical_firewall = ko.observable(true);
    var insured_members = ko.observableDictionary();

    var members = ko.observableArray([]);
    //Dictionary which stores validation functions for each stage.
    //key: stage_id (e.g: stage_1) ; value: validation method.
    var stage_validations = {};
    var medical_red_flags = {}; // Flag for each stage


    var PolicyPremium = function(){
        var self = this;

        self.old_premium = ko.observable(null);
        self.new_premium = ko.observable(null);
        self.premium_changed = ko.observable(false);
        self.show_popup = ko.observable(false);
        self.processing_flag = ko.observable(false);
        self.cpt_id = null;
        self.agreed = ko.observable(true);
        self.policy_term = ko.observable(1);

        self.base_premium = ko.observable(null);
        self.discounted_premium = ko.observable(null);

        self.title = ko.observable();
        self.subtitle = ko.observable();
        self.medical_flag = false;
        self.contactNumber = ko.observable().extend({
            required:{
                params: true,
                message: "Please enter your mobile number"
            },
            pattern: {
                params: '^[7-9][0-9]{9}$',
                message: "Please enter a valid mobile number"
            }
        });
        self.validate = function(){
            var valid = true;
            self.contactNumber.isModified(true);

            valid = valid && self.contactNumber.isValid();
            return valid;
        };
        self.button_text = ko.observable('Call Me!');

        self.cancelPremiumChange = function(){
            self.show_popup(false);
        }
        self.hidepopup = function(data, event){
            if(event.keyCode === 27)
            self.show_popup(false);
        }

        self.agreePremiumChange = function(){
            if(self.agreed()){
                var age_list = {}
                $.each(members(),function(i,member){
                    age_list[member.tag()] = member.age_dict()['current_age'];
                });
                var post_data = {
                    'transaction_id': transaction_id(),
                    'new_cpt_id': self.cpt_id,
                    'age_list': age_list,
                    'new_premium':self.new_premium(),
                }
                $.ajax({
                    type: 'POST',
                    url:"/health-plan/agree-premium-change",
                    data:{'data': JSON.stringify(post_data), 'csrfmiddlewaretoken':CSRF_TOKEN},
                    success:function(resp){
                        resp = JSON.parse(resp);
                        if(resp.success){
                            transaction_model().premium(self.new_premium());
                            //save_stage(oldStage);
                            active_stage(newStage);
                            transaction_model().slates()[oldStage-1].class('done');
                            hasher.setHash('stage_'+newStage);
                            var stage = transaction_model().stages()[0];
                            $.each(stage.segments(),function(i,segment){
                                $.each(segment.rows(), function(k, row){
                                    $.each(row.elements(), function(l, elem){
                                        if(elem.field_type() == 'PremiumDiscountField'){
                                            elem.discounted_premiums(self.discounted_premium());
                                            elem.base_premiums(self.base_premium());
                                        }
                                    });
                                });
                            });
                        }
                    },
                });
                self.show_popup(false);
            }
        }

        self.checkPremium = function(){
            self.medical_flag = false;
            self.old_premium(transaction_model().premium());
            self.policy_term( transaction_model().policy_term() ? transaction_model().policy_term() : 1 );
            var dob_list = {};
            var mem_info = {};
            $.each(members(),function(i,member){
                dob_list[member.tag()] = member.dob();
                mem_info[member.tag()] = {'first_name': member.first_name(),
                'last_name': member.last_name(), 'dob': member.dob()}
            });
            var post_data = {
                'transaction_id': transaction_id(),
                'date_of_birth_list': dob_list,
                'policy_term': self.policy_term(),
                'member_details': mem_info,
            }
            $.ajax({
                type: 'POST',
                url:"/health-plan/notify-premium-change",
                data:{'data': JSON.stringify(post_data), 'csrfmiddlewaretoken':CSRF_TOKEN},
                success:function(resp){
                    resp = JSON.parse(resp);
                    if(resp.success){
                        var age_list = resp.age_list;

                        $.each(members(), function(i,member){
                            member.age_dict(age_list[member.tag()]);
                        });
                        self.processing_flag(resp.flag);
                        switch(resp.flag){
                            case 'MAX_AGE_LIMIT':
                                self.show_popup(true);
                                self.title("Apologies, you can't buy this policy online");
                                self.subtitle("Your age is greater than <strong>"+resp.age_limit+"<strong>, so you can't buy this policy online");
                                break;
                            case 'MIN_AGE_LIMIT':
                                self.show_popup(true);
                                self.title("Apologies, you can't buy this policy online");
                                self.subtitle("Your age is less than <strong>"+resp.age_limit+"</strong>, so you can't buy this policy online");
                                break;
                            case 'PROPOSER_INSURER':
                                self.show_popup(true);
                                self.title("Change Proposer");
                                self.subtitle("To buy this policy "+resp.msg);
                                break;
                            case 'HDFC_PARENT_WITH_PROPOSER':
                                self.show_popup(true);
                                self.title("Apologies, you can't buy this policy");
                                self.subtitle("Sorry, HDFC does not allow purchase of parents insurance in the same policy as of the proposer.")
                                break;
                            case 'ONLINE_AGE_LIMIT':
                                self.show_popup(true);
                                self.title("Apologies, you can't buy this policy online");
                                self.subtitle("Your age is more than <strong>"+resp.age_limit+"</strong>, so you can't buy this policy online");
                                break;
                            case 'MEDICAL_REQUIRED':
                                //self.show_popup(false);
                                self.medical_flag = true;
                            default:
                                self.title("PREMIUM CHANGED");
                                self.subtitle("Based on the given birthdate of insured members, premium of the policy has changed");
                                if(resp.base_premium){
                                    self.base_premium(resp.base_premium);
                                    self.discounted_premium(resp.discounted_premium);
                                    self.cpt_id = resp.new_cpt_id[self.policy_term()];
                                    self.new_premium(resp.discounted_premium[self.policy_term()]);
                                    self.premium_changed(self.new_premium() != self.old_premium());
                                    self.show_popup(self.premium_changed());
                                    self.premium_changed() ? self.processing_flag('PREMIUM_CHANGE'):true ;
                                }
                                break;
                        }
                    }
                },
                async:false,
            });
        }
        self.buttonClick = function(){
            if(self.button_text() == 'Call Me!'){
                if(self.validate()){
                    var post_data = {
                        'campaign':'Health',
                        'label': 'offline',
                        'mobile': self.contactNumber(),
                        'triggered_page':window.location.href,
                    }
                    self.button_text('Wait...');
                    $.ajax({
                        url: '/leads/save-call-time/',
                        type: 'POST',
                        data: {'data':JSON.stringify( post_data), 'csrfmiddlewaretoken': CSRF_TOKEN},
                        success: function(res) {
                            self.button_text('Back To Results');
                        }
                    });
                }
            }
            else if(self.button_text() == 'Back To Results'){
                $.ajax({
                    url:'/health-plan/show-results/',
                    type:'POST',
                        data: {'transaction_id':transaction_id(), 'csrfmiddlewaretoken': CSRF_TOKEN},
                        success: function(res) {
                            res = JSON.parse(res);
                            if(res.activity_id){
                                window.location.href = "../results/#"+res.activity_id;
                            }
                        }
                });
            }
        }
    }


    var utils = {
        months: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
    };
    var Row = function(){
        var self = this;
        self.elements = ko.observableArray();
    };

    var Segment = function(data){
        var self = this;
        self.segment = ko.observable(data.segment);
        self.tag = ko.observable(data.tag);
        self.details = ko.observable(data.details);
//        self.show_header = ko.observable(data.show_header);
        self.show_header = ko.observable(false);
        self.rows = ko.observableArray();
    };

    var StageModel = function(data){
        var self = this;
        self.template = ko.observable(data.template);
        self.data = ko.observable(data);
        self.segments = ko.observableArray();
    };

    var SlateModel = function(data){
        var self = this;
        self.id = ko.observable(data.id);
        self.class = ko.observable(data.class);
        self.permaClass = ko.observable(data.class);
        self.name = ko.observable(data.name);
        self.showClass = ko.computed(function(){
            var res = '';
            if (self.class()){
                res = res + self.class();
            }
            if (self.permaClass()){
                res = res + ' ' + self.permaClass();
            }
            return res
        });
        self.description = ko.observable(data.description);
    };

    var TransactionViewModel = function(){
        var self = this;
        self.stages = ko.observableArray();
        self.slates = ko.observableArray();
        self.policy_term = ko.observable();
        self.current_stage = ko.computed(function(){
            var res = null;
            if (self.slates().length >= active_stage() - 1){
                res = self.slates()[active_stage()-1];
                if(res){
                    res.class('active');
                }
            }
            return res
        });

        self.ddn_wizard_visible = ko.observable(false);
        self.ddn_wizard_toggle = function() {
                self.ddn_wizard_visible(!self.ddn_wizard_visible());
        };

        self.initialize = function(data){
            self.plan_data = data.plan_data;
            self.subscription = ko.observable("ANNUAL PREMIUM");
            self.premium = ko.observable(self.plan_data.premium);

            self.proposer_data = data.proposer_data;
            medical_firewall(self.plan_data.medical_firewall);
            medical_red_flags = data.red_flags;
            self.insurer_id = data.plan_data.insurer_id;
            if(data.terms_and_conditions){
                terms_and_conditions = data.terms_and_conditions;
            }
            if(data.policy_date_disclaimer){
                policy_date_disclaimer(data.policy_date_disclaimer);
            }


            last_stage(data.last_stage);

            //Intializing SLATES
            $.each(data.slate_list, function(i, slate){
                var newSlate = new SlateModel(slate);
                self.slates.push(newSlate);
            });

            //SETTING DATA FOR EACH Stage > Segment > Row > Elements
            $.each(data.stages, function(ind, stage){
                var newStage = new StageModel(stage);
                $.each(stage.data, function(i, segment){
                    var newSegment = new Segment(segment);
                    $.each(segment.data, function(j, row){
                        var newRow = new Row();
                        $.each(row, function(k, elem){
                            var obj = eval("new cf."+elem.field_type+"(elem)");
                            if(obj){
                                newRow.elements.push(obj);
                            }
                            if(elem.field_type == 'PremiumDiscountField'){
                                self.policy_term = obj.value1;
                            }
                        });
                        newSegment.rows.push(newRow);
                    });
                    newStage.segments.push(newSegment);
                });
                self.stages.push(newStage);
            });
            var stage = self.stages()[1];
            $.each(stage.segments(), function(j,segment){
                var mem = new Member(segment.tag());
                $.each(segment.rows(), function(k, row){
                    $.each(row.elements(), function(l, e){
                        if(e.code() == 'date_of_birth'){
                            mem.dob = e.value; //here e.value is an observable so i'm just assigning one observable to another and not its value. So when value is changed mem.dob will also change.
                        }
                        if(e.code() == 'first_name'){
                            mem.first_name = e.value;
                        }
                        if(e.code() == 'last_name'){
                            mem.last_name = e.value;
                        }
                    });
                });
                if(mem.dob() != undefined){
                    members.push(mem);
                }
            });

        };
    };

    transaction_model = ko.observable();
    var policy_premium = new PolicyPremium();
    var oldStage = null;
    var newStage = null;

    var go_forward = function(new_stage,old_stage){
        var old_stage_validation = stage_validations['stage_'+old_stage](old_stage);
        if(new_stage > transaction_model().stages().length){
            new_stage= transaction_model().stages().length;
        }
        if(old_stage_validation.valid){
            var can_go = true;
            if(active_stage() == 2){
                policy_premium.checkPremium();
            }
            var next_stage = new_stage;
            for(var i=old_stage+1;i<=new_stage; ++i){
                var val = stage_validations['stage_'+i](i);
                if(!val.valid){
                    next_stage = i;
                    break;
                }
                if(i == 2 && new_stage != 2){
                    policy_premium.checkPremium();
                }
            }
            next_stage = (new_stage <= next_stage) ? new_stage : next_stage;
            newStage = next_stage;
            if(policy_premium.show_popup()){
                hasher.silentReplace('stage_'+old_stage);
                return;
            }
            save_stage(old_stage);
            active_stage(next_stage);
            transaction_model().slates()[old_stage-1].class('done');
            if(next_stage != new_stage){
                hasher.silentReplace('stage_'+next_stage);
            }
        }
        else{
            //SHOW ERRORs. CURRENT STAGE IS INVALID
            var error_message = (old_stage_validation.error_message) ? old_stage_validation.error_message : '<strong>Oh snap!</strong>&nbsp;&nbsp;Check for errors and try to continue again.';
            $("#global-error-text").html(error_message);
            show_error_screen();
            $('#global-error').removeClass('hide').fadeIn().fadeOut(5000);
            hasher.silentReplace('stage_'+old_stage);
            if(old_stage_validation.errors){
                old_stage_validation.errors.showAllMessages();
            }
        }
    }
    var go_backward = function(new_stage,old_stage){
        var old_stage_validation = stage_validations['stage_'+old_stage](old_stage);

        if(old_stage_validation.valid){
            transaction_model().slates()[old_stage-1].class('done');
            save_stage(old_stage);
        }
        else{
            transaction_model().slates()[old_stage-1].class('error');
        }
        active_stage(new_stage);
    }
    var goto_stage = function(new_stage){
        var old_stage = parseInt(active_stage());

        if(new_stage != old_stage){
            window.scrollTo(0,0);
            newStage = new_stage;
            oldStage = old_stage;
            hasher.setHash('stage_'+new_stage);
        }
    }

    var save_stage = function(stage_no){
        var post_data = {
            'csrfmiddlewaretoken' : CSRF_TOKEN,
            'data' : ko.toJSON(transaction_model().stages()[stage_no-1]),
            'checkout_flag' : false,
            'transaction_id' : transaction_id()
        }
        if(stage_no == 2 && policy_premium.medical_flag){
            post_data['medical_flag'] = true;
            var age_list = [];
            $.each(members(),function(i,m){
                age_list.push({'name':m.first_name()+' '+m.last_name(),'age':m.current_age()});
            });
            post_data['age_list'] = JSON.stringify(age_list);
        }
        $('#stage_data').hide();
        $('.loading-stage').removeClass('hide');
        $.post("/health-plan/transaction/stage/save/", post_data , function(data){
            data = JSON.parse(data);
            if(data.success){
                hasher.setHash('stage_'+active_stage());
                $('#stage_data').show();
                $('.loading-stage').addClass('hide');
            }
            else{
                window.location.href = '/health-plan/product/failure/';
            }
        });
    }

    var show_medical_popup = ko.observable();
    var insurance_company_name = ko.observable('');
    var payment_request_sent = ko.observable(false);
    function redirect_url(self, SITE, top, transaction_id){
        var process_url = '/health-plan/transaction/'+ transaction_id +'/final/';
        if(self == top){
            window.location.href = process_url;
        }
        else{
            top.location.href = SITE + process_url;
        }
    };

    var continue_payment = function(){
        var current_stage = parseInt(active_stage());
        var curr_stage_validation = stage_validations['stage_'+current_stage](current_stage);

        var post_data = {
            'csrfmiddlewaretoken' : CSRF_TOKEN,
            'data' : ko.toJSON(transaction_model().stages()[current_stage-1]),
            'checkout_flag' : true,
            'transaction_id' : transaction_id()
        }
        $.post("/health-plan/transaction/stage/save/",post_data,function(data){
            data = JSON.parse(data);
            if(data.success){
                redirect_url(self, SITE, top, transaction_id());
            }
            else{
                window.location.href = '/health-plan/product/failure/';
            }
        });

    }
    var goto_payment = function(){
        var current_stage = parseInt(active_stage());
        var curr_stage_validation = stage_validations['stage_'+current_stage](current_stage);

        if(curr_stage_validation.valid){
            var post_data = {
                'csrfmiddlewaretoken' : CSRF_TOKEN,
                'data' : ko.toJSON(transaction_model().stages()[current_stage-1]),
                'checkout_flag' : true,
                'transaction_id' : transaction_id()
            }
            payment_request_sent(true);
            $.post("/health-plan/transaction/stage/save/",post_data,function(data){
                data = JSON.parse(data);
                if(data.success){
                    medical_required = false;
                    for(var i in data.red_flags){
                        if(data.red_flags[i]){
                            medical_required = true;
                            break;
                        }
                    }
                    if(medical_firewall() && medical_required){
                        hasher.setHash('medical-required');
                    }
                    else if (medical_required){
                        show_medical_popup(true);
                        $('#disclaimer_popup').focus();
                        insurance_company_name(data.insurer_name);
                        return
                    }
                    else{
                        redirect_url(self, SITE, top, transaction_id());
                    }
                }
                else{
                    window.location.href = '/health-plan/product/failure/';
                }
            });
        }
        else{
            var error_message = (curr_stage_validation.error_message) ? curr_stage_validation.error_message : '<strong>Oh snap!</strong>&nbsp;&nbsp;Check for errors and try to continue again.';
            $("#global-error-text").html(error_message);
            show_error_screen();
            $('#global-error').removeClass('hide').fadeIn().fadeOut(5000);
            if(curr_stage_validation.errors){
                curr_stage_validation.errors.showAllMessages();
            }
        }
    }
    var close_medical_popup = function(){
        show_medical_popup(false)
        payment_request_sent(false);
    };
    var close_medical_popup_esc = function(data, event){
        if(event.keyCode === 27){
        show_medical_popup(false);
        payment_request_sent(false);
        }
    }
    var goto_results = function(){
        $.ajax({
            url:'/health-plan/show-results/',
            type:'POST',
            data: {'transaction_id':transaction_id(), 'csrfmiddlewaretoken': CSRF_TOKEN},
            success: function(res) {
                res = JSON.parse(res);
                if(res.activity_id){
                    window.location.href = "../results/#"+res.activity_id;
                }
            }
        });
    }

    var show_error_screen = function(){
        $('.loading-stage').addClass('hide');
        $('#stage_data').show();
    };

    var process_live_data = ko.computed(function(){
        if (transaction_model()){
            var live_stage_id = active_stage();
            var live_proposer_data = ko.observableDictionary();
            var self_nominee_data = ko.observableDictionary();
            $.each(transaction_model().stages(), function(stage_index, stage){
                $.each(stage.segments(), function(j, segment){
                    //PROPOSER SECTION - STARTS
                    if(segment.segment() == 'proposer' && segment.tag() == 'personal'){
                        $.each(segment.rows(), function(k, row){
                            $.each(row.elements(), function(l, e){
                                if($.inArray(e.code(), ['first_name','middle_name','last_name', 'date_of_birth', 'marital_status', 'occupation']) >= 0){
                                    live_proposer_data.push(e.code(), e.value());
                                }
                            });
                        });
                    }
                    //PROPOSER SECTION - ENDS

                    //INSURED SECTION - STARTS
                    if(segment.segment() == 'insured'){
                        var fullname = ' ';
                        $.each(segment.rows(), function(k, row){
                            $.each(row.elements(), function(l, e){
                               //MAP PROPOSER DATA TO SELF INSURED
                               if(segment.tag() == 'self'){
                                   if($.inArray(e.code(), ['first_name','middle_name', 'last_name', 'date_of_birth', 'marital_status', 'occupation']) >= 0){
                                       e.value(live_proposer_data.get(e.code())());
                                   }
                               }
                               //MAPPING ENDS


                                if(e.code() == 'last_name'){
                                    if(!e.value()){
                                    e.value(live_proposer_data.get('last_name')());
                                    }
                                    else
                                        live_proposer_data.push(e.code(), e.value());
                                }
                                if(segment.tag() == 'self'){
                                    if($.inArray(e.code(), live_proposer_data.keys()) >= 0){
                                        if(!e.value()){
                                            e.value(live_proposer_data.get(e.code())());
                                        }
                                    }
                                }
                                if (transaction_model().proposer_data.self_nominee){
                                    if(transaction_model().proposer_data.self_nominee == segment.tag()){
                                        if($.inArray(e.code(), ['first_name', 'last_name', 'date_of_birth']) >= 0){
                                            self_nominee_data.push(e.code(), e.value());
                                        }
                                    }
                                }
                                if($.inArray(e.code(), ['first_name', 'last_name', 'date_of_birth']) >= 0){
                                    if (e.code() == 'first_name' && e.value()){
                                        var strval = e.value().trim();
                                        fullname = strval.charAt(0).toUpperCase() + strval.slice(1) + fullname;
                                    }else if (e.code() == 'last_name' && e.value()){
                                        var strval = e.value().trim();
                                        fullname = fullname + strval.charAt(0).toUpperCase() + strval.slice(1);
                                    }
                                    insured_members.push(segment.tag(), fullname);
                                }
                            });
                        });
                    }
                    //INSURED SECTION - ENDS

                    //NOMINEE SECTION - STARTS
                    // if(segment.segment() == 'nominee'){
                    //     $.each(segment.rows(), function(k, row){
                    //         $.each(row.elements(), function(l, e){
                    //             if(segment.tag() == 'self'){
                    //                 if(self_nominee_data.keys() && e.hasOwnProperty('value')  && !e.value()){
                    //                     if($.inArray(e.code(), self_nominee_data.keys()) >= 0){
                    //                         //e.value(self_nominee_data.get(e.code())());
                    //                     }
                    //                 }
                    //             }else{
                    //                 if($.inArray(e.code(), live_proposer_data.keys()) >= 0){
                    //                         e.value(live_proposer_data.get(e.code())());
                    //                 }
                    //             }
                    //         });
                    //     });
                    // }
                    //NOMINEE SECTION - ENDS

                    if(is_topup && segment.segment() == 'proposer' && segment.tag() == 'address'){
                        pincode_row = segment.rows()[3];
                        $.each(pincode_row.elements(), function(l, e){
                            if(e.code()=='pincode'){
                                new_pincode = e.value();
                                if(new_pincode && new_pincode == previous_pincode && pincode_row.elements()[2].options().length == 0){
                                    if(pincheck.isValid(new_pincode)){
                                        previous_pincode = new_pincode;
                                        post_data = {
                                            'csrfmiddlewaretoken': CSRF_TOKEN,
                                            'transaction_id': transaction_id(),
                                            'pincode': new_pincode
                                        }
                                        $.post('/health-plan/process-pincode/', post_data, function(resp){
                                            data = JSON.parse(resp);
                                            if(data.success){
                                                state_field = pincode_row.elements()[1];
                                                city_field = pincode_row.elements()[2];
                                                state_field.value(data.new_state);
                                                city_field.options(data.new_city_list);
                                            }
                                        });
                                    }
                                }
                                if(new_pincode && new_pincode != previous_pincode){
                                    if(pincheck.isValid(new_pincode)){
                                        previous_pincode = new_pincode;
                                        post_data = {
                                            'csrfmiddlewaretoken': CSRF_TOKEN,
                                            'transaction_id': transaction_id(),
                                            'pincode': new_pincode
                                        }
                                        $.post('/health-plan/process-pincode/', post_data, function(resp){
                                            data = JSON.parse(resp);
                                            if(data.success){
                                                state_field = pincode_row.elements()[1];
                                                city_field = pincode_row.elements()[2];
                                                state_field.value(data.new_state);
                                                city_field.options(data.new_city_list);
                                                city_field.value(undefined);
                                            }
                                        })
                                    }
                                    else{
                                        var error_message = '<strong>Oh snap!</strong>&nbsp;&nbsp;Invalid Pincode';
                                        $("#global-error-text").html(error_message);
                                        show_error_screen();
                                        $('#global-error').removeClass('hide').fadeIn().fadeOut(5000);
                                    }
                                }
                            }
                        })
                    }

                });
            });
        }
    });

//    var only_if_functions = ko.observable({});
//    var register_only_if = function(name, event) {
//        only_if_functions()[name] = event;
//    };


    var initializeBuyPage = function(txn_id) {
        transaction_id(txn_id);
        var durl = "/health-plan/transaction/get-data/?r=" + new Date().getTime();
        var data_to_post =  {
            'transaction_id': transaction_id(),
            'csrfmiddlewaretoken' : CSRF_TOKEN
        };
        var promise = $.post(durl, data_to_post, function(post_resp){
            resp = $.parseJSON(post_resp);
            if (resp.success) {
                var tmodel = new TransactionViewModel();
                tmodel.initialize(resp.data);
                tmodel.insured_members = insured_members;
                transaction_model(tmodel);
                transaction_model.valueHasMutated();

                //SHOW THE FORM ONCE LOADING IS COMPLETED
                $('.loading-process').addClass('hide');
                $('#buy_container').removeClass('hide');

                $('html').css('overflow', 'auto');
                //autofill_init();

                //For Dynamically adjusting the header//
                var calWidth = ($('.wizard-track').width()/ tmodel.slates().length)/$('.wizard-track').width()* 100;
                $('.wizard-track li').css('width',calWidth + '%');
                //$('.wizard-track li.last').css('width','0px');
                $('#tic-toc').on('click',function(){
                    $('#global-error').hide();
                });
            }
            else {
                //window.location = "/articles/";
            }
        });
        return promise;
    };

    var viewAttached = false;
    return {
        utils: utils,
        health_toll_free: HEALTH_TOLL_FREE,
        coverfox_office_address: OFFICE_ADDRESS,
        transaction_model : transaction_model,
        transaction_id : transaction_id,
        active_stage : active_stage,
        last_stage : last_stage,
        insured_members : insured_members,
        policy_date_disclaimer: policy_date_disclaimer,
        policy_premium:policy_premium,
        members: members,
        go_forward : go_forward,
        go_backward: go_backward,
        goto_stage: goto_stage,
        goto_payment: goto_payment,
        goto_results:goto_results,
        stage_validations : stage_validations,
        show_medical_popup: show_medical_popup,
        close_medical_popup: close_medical_popup,
        close_medical_popup_esc: close_medical_popup_esc,
        continue_payment: continue_payment,
        insurance_company_name:insurance_company_name,
        payment_request_sent: payment_request_sent,
        activate : function(txn_id){
            //Resetting form to initialize states
            $("#buy_container").show();
            $('.checkout').addClass('hide');
            return initializeBuyPage(txn_id);
        },
        attached : function(view, parent, settings){
            $('#overlay').removeClass('hide').css({
                'background-color': '#EDEFF0',
                'padding' : '30px'
            });
            //TO HIDE ON LOAD VALIDATION ERRORS : VERY VERY IMPORTANT
            ko.validation.group([transaction_model()], { deep: true}).showAllMessages(false);
            //Seriously!!

            big_errors = ko.validation.group([transaction_model()], { deep: true});
            if(big_errors().length == 0){
            }

            $(document).on('click', '.call_overlay', function(){
                $('#buy_container').hide();
                $('#overlay').html(terms_and_conditions).removeClass('hide').show();
                $(window).scrollTop(0);
            });
            $(document).on('click', '.hide_overlay', function(){
                $('#overlay').addClass('hide').hide();
                $('#buy_container').show();
                var check_val = $(document).scrollTop();
                $(window).scrollTop(check_val);
            });
            $('.toolsy_tippy').tooltip({
                'placement' : 'top',
                'container' : '.main-section'
            });

            //STAGES TO TOGGLE
            $('#slate_' + active_stage()).removeClass('hide');
            //$('.wizard-track li.last').css('width','0px');

            viewAttached = true;
        }
    }
});
