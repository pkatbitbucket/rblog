define(['jquery', 'knockout','insurers/custom_fields'], function($, ko,cf) {
        'use strict';


        var transaction_model = ko.observable(null);
        var buy_view = null;
        var last_stage = ko.observable(null);
        var premium_changed = ko.observable(false);

        var init = function(buyView, curr_stage_id) {
            transaction_model(buyView.transaction_model());
            buy_view = buyView;
            buyView.stage_validations[curr_stage_id] = validation;
            last_stage(buyView.last_stage());
        };

        var validation = function(stage_no){
            var validation_list = [];
            var stage = transaction_model().stages()[stage_no-1];

            /*if(buy_view.active_stage() == stage_no){
                buy_view.policy_premium.checkPremium();
                if(buy_view.policy_premium.premium_changed()){
                    return{
                        valid:false,
                        errors:null,
                        error_message:'Please agree with the premium change',
                    }
                }
            }*/

            $.each(stage.segments(), function(j,segment){
                $.each(segment.rows(), function(k, row){
                    $.each(row.elements(), function(l, e){
                        validation_list.push(e);
                    });
                });
            });

            var errors = ko.validation.group(validation_list, {deep : true});

            return {
                valid : $.isEmptyObject(errors()),
                errors : errors,
            };
        }

        var checkForPremiumChange = function(){
            var premium_changed = false;
            var new_premium = null;
            var old_premium = transaction_model().premium();

            var stage = transaction_model().stages()[1];
            var dob_list = {};

            $.each(stage.segments(), function(j,segment){
                $.each(segment.rows(), function(k, row){
                    $.each(row.elements(), function(l, e){
                        if(e.code() == 'date_of_birth'){
                            if(e.value()){
                                dob_list[segment.tag()] = e.value();
                            }
                            else{
                                return false;
                            }
                        }
                    });
                });
            });
            var post_data = {
                'transaction_id': buy_view.transaction_id(),
                'date_of_birth_list': dob_list,
            }
            $.ajax({
                type: 'POST',
                url:"/health-plan/notify-premium-change",
                data:{'data': JSON.stringify(post_data), 'csrfmiddlewaretoken':CSRF_TOKEN},
                success:function(resp){
                    resp = JSON.parse(resp);
                    if(resp.success){
                        new_premium = resp.premium;
                        if(new_premium != old_premium){
                            premium_changed = true;
                            buy_view.new_premium(new_premium);
                        }
                        else{
                            premium_changed = false;
                            buy_view.new_premium(new_premium);
                        }
                    }
                },
                async:false,
            });
            return premium_changed;
        }

        return {
            init : init
        };
    }
);
