define(['jquery', 'knockout'], function($, ko) {
        'use strict';

        ko.bindingHandlers.MiniMultipleChoiceField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var question_string = '';
                question_string += '<div data-bind="if : ' + valueAccessor().data.only_if + '">';
                //question_string += '        <div class="quest-pad-mini col-sm-2 ' + valueAccessor().data.css_class + '">';
                question_string += '            <div data-bind="foreach: obj.options()">';
                question_string += '                <label class="control-label ">';
                question_string += '                    <input data-bind="checked : $parent.obj.value, ' +
                    'attr : {\'value\' : $data.id , \'name\' : $parent.obj.code}' + valueAccessor().data.event +'" type="radio"/>';
                question_string += '                    <span data-bind="text: $data.name"></span>';
                question_string += '                </label>&nbsp;&nbsp;';
                question_string += '            </div>';
                question_string += '          <p data-bind="validationMessage: obj.value" class="error"></p>';
                question_string += '        </div>';
                //question_string += '</div>';

                $(element).append(question_string);
                var vmodel = {
                    obj : valueAccessor(),
                    root_context : bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };

        ko.bindingHandlers.MiniTwoDropDownField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var question_string = '';
                question_string += '<div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                question_string += '    <div class="row">';
                question_string += '    <span data-bind="text: "' + valueAccessor().data.only_if + '"></span>';
                question_string += '        <div data-bind="if : ' + valueAccessor().data.only_if + '">';
                question_string += '            <div style="padding:0px 5px 0px 15px;" class="col-sm-6">';
                question_string += '            <select class="form-control" data-bind="options: obj.options1, ' +
                    'optionsText: \'name\', optionsValue: \'id\', value: obj.value1, ' +
                    'optionsCaption: \'Select\' "></select>';
                question_string += '            <p data-bind="validationMessage: obj.value1" class="error"></p>';
                question_string += '            </div>';
                question_string += '            <div style="padding:0px 15px 0px 5px;" class="col-sm-6">';
                question_string += '                <select class="form-control" data-bind="options: obj.options2, ' +
                    'optionsText: \'name\', optionsValue: \'id\', value: obj.value2, ' +
                    'optionsCaption: \'Select\' "></select>';
                question_string += '            <p data-bind="validationMessage: obj.value2" class="error"></p>';
                question_string += '            </div>';
                question_string += '        </div>';
                question_string += '    </div>';
                question_string += '</div>';
                $(element).append(question_string);

                var vmodel = {
                    obj : valueAccessor(),
                    root_context : bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };

        ko.bindingHandlers.MiniTextField = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var dyna_string = "";
                //dyna_string += '<div class="col-sm-'+ valueAccessor().data.span +' ' + valueAccessor().data.css_class + '">';
                dyna_string += '    <div data-bind="if: ' + valueAccessor().data.only_if + '">';
                dyna_string += '        <textarea class="form-control" rows="3" data-bind="value: obj.value"></textarea>';
                dyna_string += '      <p data-bind="validationMessage: obj.value" class="error"></p>';
                dyna_string += '    </div>';
                dyna_string += '    <div data-bind="ifnot: ' + valueAccessor().data.only_if + '" style="margin-top:15px;">';
                dyna_string += '    N/A';
                dyna_string += '    </div>';
                //dyna_string += '</div>';
                $(element).append(dyna_string);
                var vmodel = {
                    obj: valueAccessor(),
                    root_context: bindingContext.$root
                };
                ko.applyBindingsToDescendants(vmodel, element);
                return {
                    controlsDescendantBindings: true
                };
            }
        };


        var check_dict = ko.observableDictionary();
        var transaction_model = ko.observable(null);
        var conditional_array = ko.observableArray();
        var code_name_dict = ko.observableDictionary();
        var atleast_one_yes_error = ko.observable(false);

        var init = function(buyView, curr_stage_id) {
            transaction_model(buyView.transaction_model());
            buyView.stage_validations[curr_stage_id] = validation;
            any_yes_check();
        };


        var validation = function(stage_no){
            var validation_list = [];
            var stage = transaction_model().stages()[stage_no-1];


            atleast_one_yes_error(false);
            $.each(stage.segments(), function(j, segment){
                $.each(segment.rows(), function(k, row){
                    $.each(row.elements(), function(l, e){
                        //STORE CODE VS QUESTION TEXT IN DICT
                        if(segment.details().conditional=='NO'){
                            var check_code = segment.segment().split('_question_')[1];
                            code_name_dict.push(check_code, e.data.label);
                        }
                        //END
                        if(e.code() == 'details'){
                            if(check_dict.get(segment.tag())()){
                                validation_list.push(e);
                            }
                        }
                        else {
                            validation_list.push(e);
                        }
                    });
                });
            });

            var errors = ko.validation.group(validation_list, {deep : true});
            return {
                valid : ($.isEmptyObject(errors()) && !atleast_one_yes_error()),
                errors: errors,
            };
        }

        var any_yes_check = function(){
            var which_stage = 'stage_5';

            $.each(check_dict.keys(), function(i, elem){
                check_dict.remove(elem);
            });
            conditional_array([]);
            $.each(transaction_model().stages(), function(i, stage){
                if (stage.template() == which_stage){
                    $.each(stage.segments(), function(j, segment){
                        var check_code = segment.segment().split('_question_')[1];
                        if(segment.details().trigger_any_yes_check){
                            var trigger_answer = 'Yes';
                            if(segment.details().trigger_answer){
                                trigger_answer = segment.details().trigger_answer;
                            }
                            $.each(segment.rows(), function(k, row){
                                $.each(row.elements(), function(l, e){
                                    if(e.field_type() == 'MultipleChoiceField'){
                                        if (e.value() == trigger_answer){
                                            //Push the question that triggers conditional questions
                                            check_code = e.code();
                                            //Push the individual element with member code
                                            check_dict.push(check_code, true);
                                            if (segment.details().conditional == 'NO'){
                                                conditional_array.push(check_code);
                                            }
                                            //Push the conditional question code
                                            check_dict.push(segment.tag(), true);
                                            //conditional_array.push(segment.tag());
                                            }
                                        else if (e.value() == 'No' && segment.details().conditional == 'NO'){
                                            check_code = segment.segment().split('_question_')[1];
                                            $.each(stage.segments(), function(subj, sub_segment){
                                                if(sub_segment.details().parent_question_code ==  check_code) {
//                                                    console.log('=============', sub_segment.details().parent_question_code, check_code);
                                                    $.each(sub_segment.rows(), function (subjk, sub_row) {
                                                        $.each(sub_row.elements(), function (subl, sube) {
                                                            if (sube.field_type() == 'MultipleChoiceField') {
                                                                sube.value('No')
                                                            }
                                                        });
                                                    });
                                                }
                                            });

                                        }
                                    }
                                });
                            });
                        }
//                        console.log('>>>>>> TAG >>>>>>>>>', segment.tag());
//                        check_tag(segment.tag(), segment.tag());
                    });
                }
            });
            $.each(transaction_model().stages(), function(i, stage){
                $.each(stage.segments(), function(j, segment){
                    if(segment.details().conditional == 'YES'){
                        if(segment.details().tier == 1){
                            //If it's a first tier conditional segment dependent on a big question
                            if($.inArray(segment.details().parent_question_code, conditional_array()) >= 0){
                              //If the parent of first tier conditional question is marked 'YES'
//                            console.log('REQUIRED SEGMENTS >>>>>>>>', segment.details().parent_question_code);
                                segment.show_header(true);
                                $.each(segment.rows(), function(k, row){
                                    $.each(row.elements(), function(l, e){
                                        e.is_required(true);
                                    });
                                });
                            }else{
                                //If the parent of first tier conditional question is marked 'NO'
                                segment.show_header(false);
                                $.each(segment.rows(), function(k, row){
                                    $.each(row.elements(), function(l, e){
                                        e.is_required(false);
                                    });
                                });
                            }
                        }else if (segment.details().tier == 2){
                            //If it's a second tier conditional segment
//                            console.log('checking segments', segment.details().parent_question_code);
                            $.each(segment.rows(), function(k, row){
                                segment.show_header(false);
                                $.each(row.elements(), function(l, e){
                                    //Conditional element is dependant on a parent element (not segment)
                                    if($.inArray(e.data.dependant_parent, check_dict.keys()) >= 0){
                                        segment.show_header(true);
                                        e.is_required(true);
                                    }else{
                                        e.is_required(false);
                                    }
                                });
                            });
                        }
                    }
                });
            });

            //Always return true in order to continue processing a radio box
            return true;
        };


        return {
            init : init,
            any_yes_check : any_yes_check,
            check_dict : check_dict
        };
    }
);
