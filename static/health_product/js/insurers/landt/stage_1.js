define(['jquery', 'knockout'], function($, ko) {
        'use strict';

        var last_stage = ko.observable(null);

        var transaction_model = ko.observable(null);

        var init = function(buyView, curr_stage_id) {
            transaction_model(buyView.transaction_model());
            buyView.stage_validations[curr_stage_id] = validation;
            last_stage(buyView.last_stage());
        };

        var error_message = "";
        var marital_age_validation = function(stage_no) {
            var stage = transaction_model().stages()[stage_no-1];
            var gender = null;
            var marital_status = null;
            var dob = null;
            $.each(stage.segments(), function(i, seg) {
                $.each(seg.rows(), function(j, row){
                    $.each(row.elements(), function(k, e){
                        if(e.code()=="gender") {gender = (e.value()==1)?'male':'female';}
                        else if(e.code()=="marital_status") {marital_status = e.value();}
                        else if(e.code()=="date_of_birth") {
                            var d = e.value().split('/');
                            d = d[1]+'/'+d[0]+'/'+d[2];
                            dob = new Date(d);
                        }
                    });
                });
            });
            if(gender && marital_status && dob && marital_status!='SINGLE') {
                var age_diff = (gender=='male') ? 21 : 18;
                var now = new Date();
                var max_dob = new Date(now.getYear()-age_diff, now.getMonth(), now.getDate());
                error_message = 'Marital status should be Single if age is less than <strong>'+age_diff+'</strong> years.';
                return dob < max_dob;
            }
            else { return true; }
        }

        var validation = function(stage_no){
            var validation_list = [];
            var stage = transaction_model().stages()[stage_no-1];

            var marital_status = marital_age_validation(stage_no);
            if(!marital_status){
                return {
                    valid: false,
                    error_message: error_message,
                    errors: null,
                }
            }

            $.each(stage.segments(), function(j,segment){
                $.each(segment.rows(), function(k, row){
                    $.each(row.elements(), function(l, e){
                        validation_list.push(e);
                    });
                });
            });
            var errors = ko.validation.group(validation_list, {deep : true});

            return {
                valid : $.isEmptyObject(errors()),
                errors : errors,
            };
        }

        return {
            init : init
        };
    }
);
