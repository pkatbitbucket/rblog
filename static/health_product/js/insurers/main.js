requirejs.config({
    baseUrl : '/static/health_product/js',
    paths: {
        //'knockout': '/static/health_product/js/lib/knockout/knockout-3.0.0.debug',
        'knockout': 'lib/knockout/knockout-3.3.0',
        'text': 'lib/require/text',
        'knockout_validation': 'lib/knockout/knockout.validation',
        'knockout_dictionary': 'lib/knockout/ko.observableDictionary',
        'jquery': 'lib/jquery/jquery-1.11.0',
        'jquery-plugin': 'lib/jquery/jquery.plugin',
        'jquery-datepicker': 'lib/jquery/jquery.datepick-5.0.0',
        'jdatepicker': 'scripts/jdatepicker',
        'custom' : 'scripts/custom',
        'utils' : 'app/viewmodels/utils',
        //'custom_fields' : 'insurers/custom_fields',
        'bootstrap-tooltip': 'lib/bootstrap/tooltip',
        'viewmodels' : 'app/viewmodels',
        'insurers' : 'insurers',
        'signals': 'lib/crossroads/signals',
        'hasher': 'lib/crossroads/hasher',
    },
    shim: {
        'knockout_validation' : {
            deps : ['knockout']
        },
        'knockout_dictionary' : {
            deps : ['knockout'],
            exports : 'knockout_dictionary'
        },
        'jdatepicker' : {
            deps : ['jquery-datepicker']
        },
        'bootstrap-tooltip': {
            deps: ['jquery'],
        },
        'datepicker': {
            deps: ['jquery'],
        },
        'jquery-plugin' : {
            deps : ['jquery'],
        },
        'jquery-datepicker' : {
            deps : ['jquery','jquery-plugin'],
        },
    },
    urlArgs: 'v='+VERSION_ID
});
define(['jquery', 'knockout', 'utils', 'insurers/custom_fields', 'hasher', 'text!app/views/medical_required.html', 'viewmodels/medical_case',
        'knockout_validation', 'knockout_dictionary', 'bootstrap-tooltip', 'jdatepicker', 'custom'],
       function($, ko, utils, cf, hasher, medical_view, medicalViewModel) {

    var require_list = ['insurers/'+BUY_MODEL+'/new_buy', 'text!insurers/'+BUY_MODEL+'/new_buy.html'];
    var key_list = ['', ''];

    var buyModel = null;
    var page_executed = false;
    var medical_executed = false;
    var args = null;
    var no_of_stages = 0;

    $.each(STAGE_VIEW_MAP, function(k,v){
        //PUSH THE TEMPLATE
        require_list.push('text!insurers/' + k + '.html');
        key_list.push(k);
        key_list.push(v);
        require_list.push('insurers/' + v.split('-')[0]);
        var stage_no = parseInt(v.split('-')[1]);
        no_of_stages = (stage_no > no_of_stages) ? stage_no : no_of_stages;
    });
//    $.each(STAGE_MODEL_MAP, function(i, elem){
//        //PUSH THE MODEL
//        require_list.push('insurers/' + elem);
//    });

    var show_medical_container = function(){
        $('.ploader').hide();
        $('#page-container').hide();
        $('#medical-container').show();
        medicalViewModel.activate(TRANSACTION_ID).done(function(){
            medicalViewModel.attached();
            if(!!ko.dataFor(document.getElementById('medical_container'))) {
                return true;
            } else {
                ko.applyBindings(medicalViewModel, document.getElementById('medical_container'));
            }
        });
    };//END.show_medical_container

    var show_page_container = function(args,initHash){
        page_executed = true;
        if(args){ //STAGES
            var buyModel = args[0];
            var module_arguments = args;
            //If transaction_model is not defined, define it..
            if(buyModel && !buyModel.transaction_model()){

                $('.ploader').hide();
                $('#page-container').show();
                buyModel.activate(TRANSACTION_ID).done(function(){
                    for(var i = 2; i < require_list.length; i++){
                        if(require_list[i].substring(0,4) == 'text'){
                            //IF ARGUMENT IS A TEMPLATE
                            var stage_view = module_arguments[i];
                            var sview = $(stage_view).clone();
                            sview.appendTo($('#page-container'));
                        }
                        else {
                            var splitted_text = require_list[i].split('/');
                            var model_name = splitted_text[splitted_text.length - 1];
                            var stage_id = 'stage_' + key_list[i].split('-')[1];

                            buyModel[model_name] = module_arguments[i];
                            buyModel[model_name].init(buyModel, stage_id);
                        }
                    }
                    //transaction_model(buyModel.transaction_model());
                    //stage_validations = buyModel.stage_validations;

                    var show_ticked = true;
                    var stage_keys = Object.keys(buyModel.stage_validations).sort();

                    for(var j=0;j<stage_keys.length;j++){
                        var stage_no = parseInt(j+1);
                        var val = buyModel.stage_validations[stage_keys[j]](stage_no);
                        if(val.valid && buyModel.transaction_model().slates()[stage_no-1].class()!='active' && show_ticked){
                            buyModel.transaction_model().slates()[stage_no-1].class('done');
                        }
                        if(!val.valid){ show_ticked =false; }
                    }
                    ko.applyBindings(buyModel, document.getElementById('page-container'));
                    buyModel.attached();

                    if(STATIC_MODE){
                        var last_stage = transaction_model().stages().length;
                        buyModel.active_stage(last_stage);
                    }
                    if(initHash && !STATIC_MODE){
                        hasher.replaceHash(initHash);
                        hasher.initialized.add(parseHash);
                        hasher.changed.add(parseHash);
                        hasher.silentReplace = function(hash){
                            hasher.changed.active = false;
                            hasher.replaceHash(hash);
                            hasher.changed.active = true;
                        };
                        hasher.init();
                    }
                });
            }
        }
    };//END.show_page_container


    function parseHash(newHash,oldHash){
        if(STATIC_MODE) return;
        if(newHash.substr(0, 6) == "stage_"){
            var new_stage = (newHash) ? parseInt(newHash.split('_')[1]) : 1;
            var old_stage = (oldHash) ? parseInt(oldHash.split('_')[1]) : 1;
            new_stage = isNaN(new_stage) ? 1 : new_stage;
            old_stage = isNaN(old_stage) ? 1 : old_stage;

            if(new_stage > no_of_stages)
                new_stage = no_of_stages;
            else if(new_stage < 1)
                new_stage = 1;
            var new_stage_hash = 'stage_' + new_stage;
            if(newHash != new_stage_hash)
                hasher.silentReplace(new_stage_hash);

            if(new_stage > old_stage){
                buyModel.go_forward(new_stage,old_stage);
            }
            if(new_stage < old_stage){ // not used 'else' here, because we dont want anything to happen when new_stage==old_stage
                buyModel.go_backward(new_stage,old_stage);
            }
            $('#page-container').show();
            $('#medical-container').hide();
            window.dataLayer.push({
                'event': 'healthvpvproposerform',
                'healthvpv': '/vp/health/proposer'+new_stage ,
            });
        }
        else if(newHash == 'medical-required') {
            $('#before_submit').removeClass('hide');
            $('#after_submit').addClass('hide');
            $('#page-container').hide();
            $('#medical-container').show();
            show_medical_container();
        }
        else {
            if(oldHash)
                hasher.silentReplace(oldHash);
            else
                hasher.replaceHash('stage_1');
        }
    }
    require(require_list, function(){
        args = arguments;
        buyModel = arguments[0];
        var buy_view = arguments[1];
        var module_arguments = arguments;
        var bview = $(buy_view).clone();
        bview.appendTo($('#page-container'));
        $('#page-container').hide();

        var mview = $(medical_view).clone();
        mview.appendTo($('#medical-container'));
        $('#medical-container').hide();

        var r1_executed = false;
        var r2_executed = false;

        var initHash = document.location.hash;
        initHash = (initHash.search(/#\/stage_[0-9]/)==0 || initHash == '#/medical-required') ? initHash.substr(2) : 'stage_1' ;
        show_page_container(arguments,initHash);
    });
});
