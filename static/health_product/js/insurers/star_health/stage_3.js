define(['jquery', 'knockout'], function($, ko) {
        'use strict';

        var last_stage = ko.observable(null);
        var transaction_model = ko.observable(null);

        var init = function(buyView, curr_stage_id) {
            transaction_model(buyView.transaction_model());
            buyView.stage_validations[curr_stage_id] = validation;
            last_stage(buyView.last_stage());
            area_code_setup(curr_stage_id);
        };

        var area_code_setup = function(which_stage) {
            var current_stage = $.grep(transaction_model().stages(), function(stage, i) {
                return stage.template() == which_stage;
            })[0];
            var address_segment = $.grep(current_stage.segments(), function(seg, i) {
                return seg.tag() == "address";
            })[0];
            var city, area, pincode;
            $.each(address_segment.rows(), function(rIndex, row){
                $.each(row.elements(), function(eIndex, e) {
                    if(e.code() == "city")
                        city = e;
                    if(e.code() == "area")
                        area = e;
                    if(e.code() == "pincode")
                        pincode = e;
                });
            });
            city.value.subscribe(function(){
                if(!city.value())
                    return;
                var post_data = {
                    'csrfmiddlewaretoken' : CSRF_TOKEN,
                    'pincode': pincode.value(),
                    'city': city.value()
                };
                area.options([]);
                area.is_required(false);
                $("#global-error").addClass("hide");
                $.ajax({
                    url: '/health-plan/insurer/star_health/fetch-areas/',
                    type: 'POST',
                    data: post_data,
                    success: function(res) {
                        res = JSON.parse(res);
                        if(res.error) {
                            $('#global-error-text').html('<strong>'+res.error+'</strong>');
                            $('#global-error').removeClass('hide').fadeIn();
                        }
                        else{
                            area.options(res.area);
                        }
                    },
                    error: function() {
                        $('#global-error-text').html('Unable to fetch area');
                        $('#global-error').removeClass('hide').fadeIn().fadeOut(5000);
                    }
                });
            });
        };

        var validation = function(stage_no){
            var validation_list = [];
            var stage = transaction_model().stages()[stage_no-1];

            $.each(stage.segments(), function(j,segment){
                $.each(segment.rows(), function(k, row){
                    $.each(row.elements(), function(l, e){
                        if(e.code() == "area"){
                            e.is_required(true);
                        }
                        validation_list.push(e);
                    });
                });
            });
            var errors = ko.validation.group(validation_list, {deep : true});

            return {
                valid : $.isEmptyObject(errors()),
                errors : errors,
            };
        }

        return {
            init : init
        };
    }
);
