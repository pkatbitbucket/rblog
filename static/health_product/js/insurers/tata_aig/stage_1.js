define(['jquery', 'knockout'], function($, ko) {
        'use strict';

        var last_stage = ko.observable(null);

        var transaction_model = ko.observable(null);

        var init = function(buyView, curr_stage_id) {
            transaction_model(buyView.transaction_model());
            buyView.stage_validations[curr_stage_id] = validation;
            last_stage(buyView.last_stage());
        };

        var validation = function(stage_no){
            var validation_list =[];
            var stage = transaction_model().stages()[stage_no-1];

            var id_proof_type = null;
            var nationality_error = null;
            var nationality = null;
            var id_proof_number = null;
            var id_proof_valid = false;

            $.each(stage.segments(), function(j, segment){
                $.each(segment.rows(), function(k, row){
                    $.each(row.elements(), function(l, e){
                        validation_list.push(e);
                        //NATIONALITY and ID PROOF ERROR
                        if (e.code() == 'id_proof_type'){
                            id_proof_type = e.value();
                        }
                        if (e.code() == 'id_proof_number'){
                            id_proof_number = e.value();
                        }
                        if (e.code() == 'nationality'){
                            nationality = e.value();
                        }
                        //NATIONALITY and ID PROOF ERROR
                    });
                });
            });
            var errors = ko.validation.group(validation_list, {deep : true});

            //NATIONALITY and ID PROOF ERROR
            if(!$.isEmptyObject(id_proof_number)){
                if(id_proof_type == 'Pan Card'){
                    if(id_proof_number.match(/\b[A-Za-z]{5}\d{4}[A-Za-z]{1}\b/)){
                        id_proof_valid = true;
                    }
                }
                else if(id_proof_type == 'Passport'){
                    if(id_proof_number.match(/\b[a-zA-Z]{1}\d{7}\b/)){
                        id_proof_valid = true;
                    }
                }
                else{
                    id_proof_valid = true;
                }
            }
            if(id_proof_number && id_proof_valid != true){
                return{
                    valid: false,
                    errors: errors,
                    error_message: 'Oh snap! It seems that the <strong>ID PROOF NUMBER</strong> is INVALID'
                }
            }
            if (nationality != 'Indian'){
                return{
                    valid: false,
                    errors: errors,
                    error_message : 'Sorry! You can only buy this plan if your nationality is <strong>Indian</strong>',
                }
            }

            return{
                valid: ($.isEmptyObject(errors()) && id_proof_valid == true && nationality == 'Indian'),
                errors: errors,
            }
        }

        return {
            init : init
        };
    }
);
