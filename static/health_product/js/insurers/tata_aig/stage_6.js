define(['jquery', 'knockout'], function($, ko) {
        'use strict';

        var check_dict = ko.observableDictionary();
        var transaction_model = ko.observable(null);
        var conditional_array = ko.observableArray();

        var init = function(buyView, curr_stage_id) {
            transaction_model(buyView.transaction_model());
            buyView.stage_validations[curr_stage_id] = validation;
            any_yes_check();

            var stage_no = parseInt(curr_stage_id.split('_')[1]);  //get stage number
            var stage = transaction_model().stages()[stage_no-1];
            //NEW CODE - STARTS
            //INITIALIZING EXISTING POLICY
            var cmodel = stage;
            cmodel['existingPolicies'] = ko.observableArray();
            cmodel['addPolicy'] = function () {
                cmodel['existingPolicies'].push(new ExistingPolicyModel());
            };
            cmodel['removePolicy'] = function (policy) {
                cmodel['existingPolicies'].remove(policy)
            };

            //POPULATING SAVED DATA
            $.each(stage.segments(), function(segment_id, segment){
                if (!$.isEmptyObject(segment.details().existing_policies_data)){
                    $.each(segment.details().existing_policies_data, function(ep_index, ep){
                        var epm = new ExistingPolicyModel();
                        epm.policy_no(ep['policy_no']);
                        epm.insurer(ep['insurer']);
                        epm.start_date(ep['start_date']);
                        epm.end_date(ep['end_date']);
                        epm.sum_insured(ep['sum_insured']);
                        epm.cumulative_bonus(ep['cumulative_bonus']);
                        epm.claims(ep['claims']);
                        cmodel['existingPolicies'].push(epm);
                    });
                }else{
                    var epm = new ExistingPolicyModel();
                    cmodel['existingPolicies'].push(epm);
                }

            });

            transaction_model().stages()[stage_no-1] = cmodel;
            //NEW CODE - ENDS
        };

        function ExistingPolicyModel() {
            var self = this;
            self.policy_switch = ko.computed(function() {
//                return self.isPortability() == 'Yes';
                return true;
            });
            self.policy_no = ko.observable().extend({required : self.policy_switch});
            self.insurer = ko.observable().extend({required : self.policy_switch});
            self.insurer_options = ko.observable([
                {'id' : 'ADKV', 'name': 'Apollo Munich Health Insurance Co Ltd'},
                {'id' : '616301', 'name': 'Bajaj Allianz General Insurance Co Ltd'},
                {'id' : '616302', 'name': 'Cholamandalam Ms General Insurance Co Ltd'},
                {'id' : '616303', 'name': 'Hdfc-Ergo General Insurance Co Ltd'},
                {'id' : '616304', 'name': 'Icici Lombard General Insurance Co Ltd'},
                {'id' : '616305', 'name': 'Iffco-Tokio General Insurance Co Ltd'},
                {'id' : '616306', 'name': 'National Insurance Co Ltd'},
                {'id' : '616307', 'name': 'New India Assurance Co Ltd'},
                {'id' : '616308', 'name': 'Oriental Insurance Co Ltd'},
                {'id' : '616309', 'name': 'Reliance General Insurance Co Ltd'},
                {'id' : '616310', 'name': 'Royal Sundaram Alliance Insurance Co Ltd'},
                {'id' : '616311', 'name': 'Star Health And Allied Insurance Co Ltd'},
                {'id' : '616312', 'name': 'Tata-Aig General Insurance Co Ltd'},
                {'id' : '616313', 'name': 'United India Insurance Co Ltd'},
                {'id' : 'NK', 'name': 'Not Known'}
            ]);
            self.start_date = ko.observable().extend({required : self.policy_switch});
            self.end_date = ko.observable().extend({required : self.policy_switch});
            self.sum_insured = ko.observable().extend({required : self.policy_switch, number : true});
            self.cumulative_bonus = ko.observable().extend({required : self.policy_switch, number : true});
            self.claims = ko.observable().extend({required : self.policy_switch});
        }


        var validation = function(stage_no){
            var validation_list = [];
            var stage = transaction_model().stages()[stage_no-1];

            $.each(stage.segments(), function(j, segment){
                $.each(segment.rows(), function(k, row){
                    $.each(row.elements(), function(l, e){
                        validation_list.push(e);
                    });
                });
                if (check_dict.get('existing_insurance')()){
                    $.each(stage.existingPolicies(), function(ep_index, ep){
                        validation_list.push(ep);
                    });
                }
            });
            var errors = ko.validation.group(validation_list, {deep : true});
            return {
                valid : $.isEmptyObject(errors()),
                errors : errors,
            };
        }

        var any_yes_check = function(){
            var which_stage = 'stage_6';

            $.each(check_dict.keys(), function(i, elem){
                check_dict.remove(elem);
            });
            $.each(transaction_model().stages(), function(i, stage){
                if (stage.template() == which_stage){
                    $.each(stage.segments(), function(j, segment){
                        $.each(segment.rows(), function(k, row){
                            $.each(row.elements(), function(l, e){
                                if(e.code() == 'existing_insurance'){
                                    if (e.value() == 'Yes'){
                                        //Push the question that triggers conditional questions
                                        check_dict.push(e.code(), true);
                                    }
                                } //MultipleChocieEnd
                            });
                        });
//                        console.log('>>>>>> TAG >>>>>>>>>', segment.tag());
//                        check_tag(segment.tag(), segment.tag());
                    });
                }
            });
            $.each(transaction_model().stages(), function(i, stage){
                $.each(stage.segments(), function(j, segment){
                    if(segment.segment() == 'previous_policy'){
                        $.each(segment.rows(), function(k, row){
                            $.each(row.elements(), function(l, e){
                                if(e.code() != 'existing_insurance'){
                                    if($.inArray('existing_insurance', check_dict.keys()) >= 0){
                                        e.is_required(true);
                                    }
                                    else{
                                        e.is_required(false);
                                    }
                                }
                            });
                        });
                    }
                });
            });

            //Always return true in order to continue processing a radio box
            return true;
        };


        return {
            init : init,
            any_yes_check : any_yes_check,
            check_dict : check_dict
        };
    }
);
