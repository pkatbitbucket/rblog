/**
 * Durandal 2.0.1 Copyright (c) 2012 Blue Spire Consulting, Inc. All Rights Reserved.
 * Available via the MIT license.
 * see: http://durandaljs.com or https://github.com/BlueSpire/Durandal for details.
 */
/**
 * The entrance transition module.
 * @module entrance
 * @requires system
 * @requires composition
 * @requires jquery
 */
define(['durandal/system', 'durandal/composition', 'plugins/router', 'jquery', 'jquerytransit'], function(system, composition, router, $) {
    var fadeOutDuration = 100;
    var endValues = {
        marginRight: 0,
        marginLeft: 0,
        opacity: 1
    };
    var clearValues = {
        marginLeft: '',
        marginRight: '',
        opacity: '',
        display: ''
    };

    /**
     * @class EntranceModule
     * @constructor
     */
    var entrance = function(context) {
        return system.defer(function(dfd) {
            function endTransition() {
                dfd.resolve();
            }

            function scrollIfNeeded() {
                if (!context.keepScrollPosition) {
                    $(document).scrollTop(0);
                }
            }

            /** General State Management **/
                $('#overlay').fadeOut().html("");
                $('#page-wrapper').show();
                $("#edit-info-overlay").addClass('hide');
                if(router.activeInstruction().fragment.indexOf('results') != 0){
                    $('.side_bar_content.results_bar').transition({left:"-100%", opacity:0},400, function(){$(this).hide()});
                    $(".side_bar_content").not(".results_bar").show().transition({left : 0, opacity : 1}, 400);
                } else {
                    $('#side_bar_results.side_bar_content').show().transition({left:"0%", opacity:1},400);
                    $(".side_bar_content").not("#side_bar_results").not('#spaces').transition({left : "-100%", opacity : 0}, 400, function(){$(this).hide();});
                    $('#resultsnav').transition({y:0},200);
                }

            if (!context.child) {
                $(context.activeView).fadeOut(fadeOutDuration, endTransition);
            } else {
                var duration = context.duration || 200;
                var fadeOnly = !!context.fadeOnly;

                function startTransition() {
                    scrollIfNeeded();
                    context.triggerAttach();

                    var startValues = {
                        marginLeft: fadeOnly ? '0' : '100px',
                        marginRight: fadeOnly ? '0' : '-100px',
                        opacity: 0,
                        display: 'block'
                    };

                    var $child = $(context.child);

                    $child.css(startValues);
                    $child.animate(endValues, {
                        duration: duration,
                        easing: 'swing',
                        always: function () {
                            $child.css(clearValues);
                            endTransition();
                        }
                    });
                }

                if (context.activeView) {
                    $(context.activeView).fadeOut({ duration: fadeOutDuration, always: startTransition });
                } else {
                    startTransition();
                }
            }
        }).promise();
    };

    return entrance;
});
