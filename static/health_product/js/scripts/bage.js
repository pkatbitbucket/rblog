define(['jquery', 'knockout'], function ($, ko) {
    ko.bindingHandlers.age = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var ageMin = allBindingsAccessor().minAge || 18;
            var ageMax = allBindingsAccessor().maxAge || 100;
            if(allBindingsAccessor().minAge == 0){
                ageMin = 0;
            }
            if(allBindingsAccessor().maxAge == 0){
                ageMax = 0;
            }
            var append = allBindingsAccessor().append || "";

            var currentValue = ko.utils.unwrapObservable(valueAccessor());
            var select_html = "";
            select_html += '<span class="combodate"><div class="column">';
            select_html += '<div class="data-hold">Enter age</div>';
            select_html += '<div class="data-caret"><span class="caret"></span></div>';
            select_html += '<select class="day">';
            for(var d=ageMin; d<=ageMax; d++){
                select_html += '<option value="' + d + '">' + d + '</option>';
            }
            select_html += '</select></div></span>'+append;
            $(element).append(select_html);

            $(element).find('select').change(function(){
                var cvalue = $(this).val();
                $(element).find('.data-hold').text(cvalue);
                var value = valueAccessor();
                value(parseInt(cvalue));
            });

            ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
                $(element).html('');
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var currentValue = ko.utils.unwrapObservable(valueAccessor());
            $(element).find('select').val(currentValue);
            if(currentValue == "" || currentValue == null || currentValue == undefined) {
                $(element).find('.data-hold').text('Enter age');
            } else {
                $(element).find('.data-hold').text(currentValue);
            }
        }
    };
    ko.bindingHandlers.ageDisable = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selected = ko.utils.unwrapObservable(valueAccessor());
            if(selected) {
                $(element).find('select').removeAttr('disabled');
            } else {
                $(element).find('select').attr('disabled', 'disabled');
            }
        }
    }

});
