define(['jquery', 'knockout', 'combodate'], function ($, ko) {
    ko.bindingHandlers.combodate = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var comboFormat = allBindingsAccessor().comboFormat || "DD-MM-YYYY";
            var comboTemplate = allBindingsAccessor().comboTemplate || "D MMM YYYY";
            var comboMinYear = allBindingsAccessor().comboMinYear || 1910;
            var comboMaxYear = allBindingsAccessor().comboMaxYear || new Date().getFullYear();
            var comboYearDescending = allBindingsAccessor().comboYearDescending || true;
            var comboSmartDays = allBindingsAccessor().comboSmartDays || true;
            var comboDDVerbose = allBindingsAccessor().comboDDVerbose || 'Day';
            var comboMMVerbose = allBindingsAccessor().comboMMVerbose || 'Month';
            var comboYYVerbose = allBindingsAccessor().comboYYVerbose || 'Year';

            var currentValue = ko.utils.unwrapObservable(valueAccessor());
            $(element).combodate({
                format : comboFormat,
                template : comboTemplate,
                firstItem : 'name',
                smartDays :comboSmartDays,
                minYear : comboMinYear,
                maxYear : comboMaxYear,
                ddverbose : comboDDVerbose,
                mmverbose : comboMMVerbose,
                yyverbose : comboYYVerbose,
            });
            if(currentValue) {
                $(element).combodate('setValue', currentValue);
            }

            $(element).change(function(){
                var cvalue = $(element).combodate('getValue', comboFormat);
                var value = valueAccessor();
                value(cvalue);
            });

            ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
                $(element).combodate('destroy');
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var currentValue = ko.utils.unwrapObservable(valueAccessor());
            if(currentValue) {
                $(element).combodate('setValue', currentValue);
            }
        }
    };
    ko.bindingHandlers.comboDisable = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selected = ko.utils.unwrapObservable(valueAccessor());
            if(selected) {
                $(element).parent().find('.combodate select').removeAttr('disabled');
            } else {
                $(element).parent().find('.combodate select').attr('disabled', 'disabled');
            }
        }
    }

});
