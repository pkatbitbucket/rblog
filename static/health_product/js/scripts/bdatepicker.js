define(['jquery', 'knockout', 'datepicker'], function ($, ko) {
    ko.bindingHandlers.datepicker = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var dpFormat = allBindingsAccessor().dpFormat || "dd/mm/yyyy";
            var dpStartDate = allBindingsAccessor().dpStartDate;
            var dpEndDate = allBindingsAccessor().dpEndDate;

            $(element).datepicker({
                format: dpFormat,
                keyboardNavigation: true,
                orientation: 'auto',
                startView: 2,
                todayBtn: false,
                todayHighlight: false
            });


            if(dpStartDate){
                $(element).datepicker('setStartDate', dpStartDate);
            }

            if(dpEndDate){
                $(element).datepicker('setEndDate', dpEndDate);
            }

            var currentValue = ko.utils.unwrapObservable(valueAccessor());
            if(currentValue) {
                $(element).datepicker('setDate', currentValue);
            }

            $(element).datepicker().on('changeDate', function(){
                var cvalue = $(element).find('input').val();
                var value = valueAccessor();
                value(cvalue);
            });

            ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
                $(element).datepicker('destroy');
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var currentValue = ko.utils.unwrapObservable(valueAccessor());
            if(currentValue) {
                $(element).datepicker('setDate', currentValue);
            }
        }
    };

});
