define(['jquery', 'knockout', 'jquerypep'], function ($, ko) {
    var scale = [1,2,3,4,5,6,7,8,9,10];
    var LOWER_RANGE = 1;
    var UPPER_RANGE = 2;
    ko.bindingHandlers.slider = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var tstring = "";
            tstring += '<div id="sliderRail">';
            tstring += '    <div id="sliderHandle" class="bounce-trans">';
            tstring += '        <div id="sliderValue"></div>';
            tstring += '    </div>';
            tstring += '</div>';
            tstring += '<ul id="sliderStations" class="cubic-trans"> </ul>';
            tstring += '<div id="sliderLabels" class="cubic-trans">';
            tstring += '    <ul>';
            tstring += '        <li class="toolittle">Too<br/>Little</li>';
            tstring += '        <li class="goodrange active">Good<br/>range</li>';
            tstring += '        <li class="toomuch">Too<br/>Much</li>';
            tstring += '    </ul>';
            tstring += '</div>';
            $(element).append(tstring);

            var currentValue = ko.utils.unwrapObservable(valueAccessor());
            if(!currentValue) {
                currentValue = 3;
            }
            var sliderRail = $(element).find('#sliderRail');
            var sliderHandle = $(element).find('#sliderHandle');
            var sliderStations = $(element).find('#sliderStations');
            var sliderLabels = $(element).find('#sliderLabels');
            var sliderValue = $(element).find('#sliderValue');
            /**** Slider Code ******/
            var scale_parts = [];
            var set_slider_handle = function(posx){
                var at_step;
                var final_pos;
                for(var i=0;i<scale_parts.length-1;i++){
                    if((scale_parts[i] <= posx) && (scale_parts[i+1] > posx)){
                        at_step = i;
                        final_pos = scale_parts[i];
                        break;
                    }
                }
                console.log("$$$$$ posx, scale_parts, at_step", posx, scale_parts[at_step], final_pos);
                var recomm_index = scale.indexOf(currentValue);
                var goodValue = ko.utils.unwrapObservable(allBindingsAccessor().goodValue) || 300000;
                var recomm_index = scale.indexOf(parseInt(goodValue/100000));
                if(recomm_index - at_step > LOWER_RANGE){
                    $(sliderHandle).css('background-color', '#730A0A');
                }
                else if(at_step - recomm_index > UPPER_RANGE - 1){
                    $(sliderHandle).css('background-color', '#008915');
                }
                else{
                    $(sliderHandle).css('background-color', '#FFFFFF');
                }
                $(sliderHandle).css('left', final_pos);
                return [final_pos, at_step];
            };

            var set_slider_area = function(val){
                var point_width = parseInt($(sliderStations).css('width'))/(scale.length+1);
                var scale_index = scale.indexOf(val+1);
                var lower = val - LOWER_RANGE;
                var upper = val + UPPER_RANGE;
                var lower_posx = lower * point_width;
                var upper_posx = upper * point_width;
                $(sliderLabels).find('.toolittle').css('left', '0px').css('width', lower_posx);
                $(sliderLabels).find('.goodrange').css('left', parseInt($(sliderLabels).find('.toolittle').css('width')));
                $(sliderLabels).find('.goodrange').css('width', upper_posx - parseInt($(sliderLabels).find('.toolittle').css('width')));
                $(sliderLabels).find('.toomuch').css(
                    'left', parseInt($(sliderLabels).find('.toolittle').css('width')) +
                        parseInt($(sliderLabels).find('.goodrange').css('width')));
                $(sliderLabels).find('.toomuch').css(
                    'width', parseInt($(sliderLabels).css('width')) - parseInt($(sliderLabels).find(' .toolittle').css('width')
                    ) - parseInt($(sliderLabels).find('.goodrange').css('width')));
            };

            var put_slider_at = function(val){
                console.log("PUT SLIDER AT", val);
                var sval = val + " - " + (val+1);
                $(sliderValue).text("Rs " + sval + " Lacs");
                var point_width = parseInt($(sliderStations).css('width'))/(scale.length-1);
                var posx = (scale.indexOf(val)) * point_width;
                set_slider_handle(posx);
            };

            $(element).data('set_slider_area', set_slider_area);
            $(element).data('put_slider_at', put_slider_at);
            $(element).data('set_slider_handle', set_slider_handle);

            /***********************/
            var event_update = allBindingsAccessor().event_update;
            console.log("EVENT_UPDATEEEEE", event_update);
            var observableValue = valueAccessor();
            var point_width = parseInt($(sliderStations).css('width'))/(scale.length-1);
            $(sliderHandle).css('width', point_width);
            $(sliderLabels).css('width', $(sliderStations).css('width'));
            for(var i=0;i<scale.length;i++){
                var ele = $("<li>"+scale[i]+"</li>");
                var left_pos = point_width*(i);
                scale_parts.push(left_pos);
                $(ele).css('left', left_pos + "px");
                $(sliderStations).append(ele);
            }
            window.jQuery(sliderHandle).pep({
                constrainTo: 'parent',
                useCSSTranslation: false,
                axis: 'x',
                shouldEase: false,
                shouldPreventDefault : true,
                start : function() {
                    $(sliderHandle).removeClass('bounce-trans');
                },
                stop: function(ev, obj){
                    $(sliderHandle).addClass('bounce-trans');
                    var final_pos = null;
                    var vals = obj.getMovementValues();
                    //tolerance is important factor, without this the slider cannot be
                    //dragged across the last scale-point and hence will never stay there.
                    var tolerance = 10;
                    res = set_slider_handle(vals.pos.x+tolerance);
                    final_pos = res[0];
                    at_step = res[1];
                    observableValue(scale[at_step] * 100000);
                    event_update.valueHasMutated();
                    ev.preventDefault();
                }
            });
            window.jQuery(sliderRail).click(function(evt){
                if(evt.target != this){return false;}
                var posX = evt.pageX - $(this).offset().left;
                res = set_slider_handle(posX);
                final_pos = res[0];
                at_step = res[1];
                observableValue(scale[at_step] * 100000);
                event_update.valueHasMutated();
                evt.preventDefault();
            });

            ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
                $(element).html('');
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var sliderRail = $(element).find('#sliderRail');
            var sliderHandle = $(element).find('#sliderHandle');
            var sliderStations = $(element).find('#sliderStations');
            var sliderLabels = $(element).find('#sliderLabels');
            var sliderValue = $(element).find('#sliderValue');

            var currentValue = ko.utils.unwrapObservable(valueAccessor());
            console.log("UPDATINGGGG...", currentValue);

            var goodValue = ko.utils.unwrapObservable(allBindingsAccessor().goodValue) || 300000;
            $(element).data('set_slider_area')(Math.round(goodValue/100000.0));

            var goodValueObservable = allBindingsAccessor().goodValue;
            goodValueObservable.subscribe(function(newVal){
                var goodValue = newVal;
                $(element).data('set_slider_area')(Math.round(newVal/100000.0));
            });

            var fval = Math.round(currentValue/100000.0);
            $(element).data('put_slider_at')(fval);
        }
    };
});
