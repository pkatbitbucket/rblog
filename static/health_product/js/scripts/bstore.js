define(['jquery', 'knockout', 'amplify'], function ($, ko, amplify) {
    //Function to check for "", null, undefined, [], {}
    var isEmpty = function(val){
        if(!val){
            return true;
        }
        if($.isArray(val)){
            if(val.length == 0){
                return true;
            }
        }
        if($.isPlainObject(val)){
            if($.isEmptyObject(val)){
                return true;
            };
        }
        return false;
    }

    ko.extenders.store = function (target, key) {
        var value = amplify.store(key) || target();
        var result = ko.computed({
            read: function(){
                var target_value = target();
                var stored_value = amplify.store(key);

                // On first time reads, target_value will be null/undefined, but store
                // might have some value, in which case, treat this as INITIALIZATION
                // Write store to target and use target value
                if(isEmpty(target_value)) {
                    if(!isEmpty(stored_value)){
                        if(typeof(stored_value) == 'string'){
                            try {
                                var rval = JSON.parse(stored_value);
                            } catch (e) {
                                var rval = stored_value;
                            }
                            target(rval);
                        } else {
                            //if object is not string, it can be number, boolean, null, undefined
                            target(stored_value);
                        }
                    }
                }

                var value = target();

                try {
                    var rval = JSON.parse(value);
                } catch (e) {
                    var rval = value;
                }
                if(rval){
                    return rval;
                } else {
                    return target();
                }
            },
            write: function(value) {
                if(typeof(value) == 'string'){
                    try {
                        var rval = JSON.parse(value);
                    } catch (e) {
                        var rval = value;
                    }
                    target(rval);
                    amplify.store(key, value);
                } else {
                    //if object is not string, it can be number, list, boolean, null, undefined
                    try {
                        var jval = JSON.stringify(value);
                    } catch (e) {
                        var jval = value;
                    }
                    amplify.store(key, jval);
                    target(value);
                }
            }
        });
        return result;
    };
});
