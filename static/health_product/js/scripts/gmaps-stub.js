define(function () {
    /**
     * A distance widget that will display a circle that can be resized and will
     * provide the radius in km.
     *
     * @param {google.maps.Map} map The map to attach to.
     *
     * @constructor
     */
    function DistanceWidget(map, onupdate) {
        this.set('map', map);
        this.set('position', map.getCenter());

        /*var marker = new google.maps.Marker({
            //draggable: true,
            title: 'Move this',
            //icon : "img/center_marker.png",
            //crossOnDrag : false
        });

        google.maps.event.addListener(marker, "dragstart", function(event) {
        });

        google.maps.event.addListener(marker, "dragend", function(event) {
            onupdate();
        });

        // Bind the marker map property to the DistanceWidget map property
        marker.bindTo('map', this);

        // Bind the marker position property to the DistanceWidget position
        // property
        marker.bindTo('position', this);
        */

        var radiusWidget = new RadiusWidget(map, onupdate);

        radiusWidget.bindTo('map', this);

        radiusWidget.bindTo('center', this, 'position');

        this.bindTo('distance', radiusWidget);

        this.bindTo('bounds', radiusWidget);

        this.radiusWidget = radiusWidget;

    }
    DistanceWidget.prototype = new google.maps.MVCObject();

    /**
     * A radius widget that add a circle to a map and centers on a marker.
     *
     * @constructor
     */
    function RadiusWidget(map, onupdate) {
        var circle = new google.maps.Circle({
            strokeWeight: 1,
            strokeColor: '#96AC76',
            fillColor: '#B6D08F',
            fillOpacity: 0.25,
        });

        this.circle = circle;

        // Set the sdragging property to false
        this.set('sdragging', false);

        // Set the distance property value, default to 6 miles = 10Kms
        this.set('distance', 6);

        // Bind the RadiusWidget bounds property to the circle bounds property.
        this.bindTo('bounds', circle);

        // Bind the circle center to the RadiusWidget center property
        circle.bindTo('center', this);

        // Bind the circle map to the RadiusWidget map
        circle.bindTo('map', this);

        // Bind the circle radius property to the RadiusWidget radius property
        circle.bindTo('radius', this);

        // Add the sizer marker
        //this.addSizer_(onupdate);
    }
    RadiusWidget.prototype = new google.maps.MVCObject();


    /**
     * Update the radius when the distance has changed.
     */
    RadiusWidget.prototype.distance_changed = function() {
        this.set('radius', this.get('distance') * 1000);
    };

    /**
     * Add the sizer marker to the map.
     *
     * @private
     */
    RadiusWidget.prototype.addSizer_ = function(onupdate) {
        /*var sizer = new google.maps.Marker({
            draggable: true,
            title: 'Drag this',
            icon : "img/resize_marker.png",
            crossOnDrag : false
        });
        var initial_resizer_position;
        google.maps.event.addListener(sizer, "dragstart", function(event) {
            initial_resizer_position = sizer.getPosition();
            console.log(initial_resizer_position);
        });

        google.maps.event.addListener(sizer, "dragend", function(event) {
            onupdate();
        });

        sizer.bindTo('map', this);
        sizer.bindTo('position', this, 'sizer_position');

        var me = this;
        google.maps.event.addListener(sizer, 'drag', function() {
            // Set the circle distance (radius)
            curr_pos = sizer.getPosition();
            sizer.setPosition(new google.maps.LatLng(initial_resizer_position.nb, curr_pos.ob));
            me.setDistance();
        });*/
    };


    /**
     * Update the center of the circle and position the sizer back on the line.
     *
     * Position is bound to the DistanceWidget so this is expected to change when
     * the position of the distance widget is changed.
     */
    RadiusWidget.prototype.center_changed = function() {
        var bounds = this.get('bounds');

        // Bounds might not always be set so check that it exists first.
        if (bounds) {
            var lng = bounds.getNorthEast().lng();

            // Put the sizer at center, right on the circle.
            var position = new google.maps.LatLng(this.get('center').lat(), lng);
            this.set('sizer_position', position);
        }
    };

    /**
     * Calculates the distance between two latlng points in km.
     * @see http://www.movable-type.co.uk/scripts/latlong.html
     *
     * @param {google.maps.LatLng} p1 The first lat lng point.
     * @param {google.maps.LatLng} p2 The second lat lng point.
     * @return {number} The distance between the two points in km.
     * @private
     */
    RadiusWidget.prototype.distanceBetweenPoints_ = function(p1, p2) {
        if (!p1 || !p2) {
            return 0;
        }

        var R = 6371; // Radius of the Earth in km
        var dLat = (p2.lat() - p1.lat()) * Math.PI / 180;
        var dLon = (p2.lng() - p1.lng()) * Math.PI / 180;
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(p1.lat() * Math.PI / 180) * Math.cos(p2.lat() * Math.PI / 180) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d;
    };


    /**
     * Set the distance of the circle based on the position of the sizer.
     */
    RadiusWidget.prototype.setDistance = function() {
        // As the sizer is being dragged, its position changes.  Because the
        // RadiusWidget's sizer_position is bound to the sizer's position, it will
        // change as well.
        var pos = this.get('sizer_position');
        var center = this.get('center');
        var distance = this.distanceBetweenPoints_(center, pos);

        // Set the distance property for any objects that are bound to it
        this.set('distance', distance);
    };
        return {
            'maps' : google.maps,
            'DistanceWidget' : DistanceWidget,
            'RadiusWidget' : RadiusWidget,
        }
});
