define(['async!https://maps.googleapis.com/maps/api/js?v=3.12&sensor=false&libraries=places,geometry'],
    function () {
        return google.maps;
    }
);
