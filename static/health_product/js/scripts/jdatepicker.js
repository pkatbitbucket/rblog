define(['jquery', 'knockout', 'jquery-datepicker'], function ($, ko) {
    ko.bindingHandlers.datepicker = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var el = $(element);
            var config = {
                showAnim: '',
                todayClass: 'hide',
            };
            function set_config(name, property, def) {
                var val = property || def;
                if(!val) {
                    return;
                }
                config[name] = val;
                if(ko.isObservable(property)) {
                    config[name] = property();
                    property.subscribe(function(val) {
                        config[name] = val;
                        destroy();
                        apply_config();
                    });
                }
            };
            function apply_config() {
                el.datepick(config);
            };
            function destroy() {
                el.datepick('destroy');
            };

            var acc = allBindingsAccessor();
            set_config('dateFormat', acc.dpFormat, "dd-M-yyyy");
            set_config('yearRange', '1900:2100');
            set_config('commandsAsDateFormat', true);
            set_config('prevText', '&lt; M');
            set_config('nextText', 'M &gt;');

            set_config('minDate', acc.dpStartDate);
            set_config('maxDate', acc.dpEndDate);
            set_config('defaultDate', acc.dpDefaultDate);
            set_config('showTrigger', acc.dpTrigger);
            set_config('onSelect', function() {
                valueAccessor()(el.val());
            });
            set_config('onDate', function(date, inMonth) { // inMonth (bool) - date is from the main visible month
                if((date.getDate() == 15) && (date.getMonth()+1 == 8)) {
                    return {dateClass: 'flag', content:'&nbsp;'};
                }
                return {};
            });

            apply_config();

            ko.utils.domNodeDisposal.addDisposeCallback(element, destroy);
        },
    };

});
