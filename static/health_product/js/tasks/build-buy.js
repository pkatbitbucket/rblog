﻿module.exports = function (grunt) {
    grunt.registerMultiTask('buy', "Grunt Builder - Build requirejs project using a custom require config", function () {
        //#region Global Properties

        var _ = grunt.util._,
            path = require('path'),
            requirejs = require('requirejs'),

            extensions = {
                script: ".js",
                view: ".html",
                all: [".js", ".html"]
            }

            defaultRequireConfig = {
                inlineText: true,
                //stubModules: ['../lib/require/text'],

                paths: {
                    'text': '../js/scripts/text',
                    'knockout': '../js/lib/knockout/knockout-3.3.0',
                    'knockout_validation': '../js/lib/knockout/knockout.validation',
                    'knockout_dictionary': '../js/lib/knockout/ko.observableDictionary',
                    'bootstrap-tooltip': '../js/lib/bootstrap/tooltip',
                    'jquery': '../js/lib/jquery/jquery-1.11.0',
                    'jquery-plugin': '../js/lib/jquery/jquery.plugin',
                    'jquery-datepicker': '../js/lib/jquery/jquery.datepick-5.0.0',
                    'jdatepicker': '../js/scripts/jdatepicker',
                    'custom' : '../js/scripts/custom',
                    'custom_fields' : '../js/insurers/custom_fields',
                    'utils' : '../js/app/viewmodels/utils',
                    'bootstrap-tooltip': '../js/lib/bootstrap/tooltip',
                    'signals': '../js/lib/crossroads/signals',
                    'hasher': '../js/lib/crossroads/hasher',
                    'viewmodels' : '../js/app/viewmodels',
                    'medical_view' : 'text!../js/app/views/medical_required.html'
                },
                preserveLicenseComments: false,
                keepBuildDir: true,
                optimize: "uglify2",
                //generateSourceMaps: true,
                //optimize: "none",
                pragmas: {
                    build: true
                },
                wrap: false,
                shim: {
                    'knockout_validation' : {
                        deps : ['knockout'],
                        exports : 'knockout_dictionary'
                    },
                    'knockout_dictionary' : {
                        deps : ['knockout']
                    },
                    'bootstrap-tooltip': {
                        deps: ['jquery'],
                        exports: '$.fn.tooltip'
                    },
                    'jdatepicker' : {
                        deps : ['jquery-datepicker']
                    },
                    'jquery-plugin' : {
                        deps : ['jquery'],
                    },
                    'jquery-datepicker' : {
                        deps : ['jquery','jquery-plugin'],
                    },
                },
            };

        //#endregion

        //#region Private Methods

        function ensureRequireConfig(params) {
            /*if (params.includeMain)
                params.insertRequire.push("main");

            params.insertRequire = _.uniq(params.insertRequire);*/
            params.includes = _.uniq(params.includes);
            params.excludes = _.uniq(params.excludes);

            if (params.paths)
                params.paths = _.extend({}, defaultRequireConfig.paths, params.paths);

            if (params.pragmas)
                params.pragmas = _.extend({}, defaultRequireConfig.pragmas, params.pragmas);
        }

        function includePath(array, config, url) {
            //if (url.indexOf(config.out) !== -1 || url.indexOf("durandal/amd") !== -1)
            //    return;

            var ext = path.extname(url);
            url = path.relative(config.baseUrl, url);
            url = url.replace(/\\/g, "/");

            var pathToReplace = _.chain(config.paths)
                                .map(function (_path, key) { return { key: key, path: _path }; })
                                .filter(function (_path) { return url.indexOf(_path.path) !== -1; })
                                .max(function (_path) { return _path.path.length; })
                                .value();

            if (pathToReplace)
                url = url.replace(pathToReplace.path, pathToReplace.key);

            if (ext === ".html") {
                url = "text!" + url;
            }
            else if (ext === ".js") {
                url = url.replace(new RegExp("\\" + ext + "$"), "");
            }
            else
                return;

            array.push(url);
        }

        //#endregion

        var done = this.async(),
            config,
            params = this.options({
                baseUrl: "../../../static/health_product/js",
                out: "../build/js/main-built-buy.js",
                include: [
                    '../../../static/health_product/js/lib/require/require.js',
                ],
                exclude: [],
                insertRequire: ['insurers/main'],
                loglevel: 0,
            });

        ensureRequireConfig(params);
        config = _.extend({}, defaultRequireConfig, params);

        this.files.forEach(function (file) {
            file.src.forEach(_.partial(includePath, config.include, config));
        });

        grunt.log.ok("+++++++++++++++++++++++++++", config);

        requirejs.optimize(
            config,
            function (response) {
                if (params.loglevel === "verbose")
                    grunt.log.write(response);

                grunt.log.ok(params.out + " created !");

                done(true);
            },
            function (error) {
                grunt.log.error(error);

                done(false);
            }
        );
    });
};
