﻿module.exports = function (grunt) {
    grunt.registerMultiTask('durandal', "Grunt Durandal Builder - Build durandal project using a custom require config and a custom almond", function () {
        //#region Global Properties
        var _ = grunt.util._,
            path = require('path'),
            requirejs = require('requirejs'),

            extensions = {
                script: ".js",
                view: ".html",
                all: [".js", ".html"]
            }

            defaultRequireConfig = {
                name: '../lib/require/almond-custom',
                inlineText: true,
                stubModules: ['../lib/require/text'],

                paths: {
                    'gmaps': '../scripts/gmaps-stub',
                    'text': '../scripts/text',
                    'durandal':'../lib/durandal/js',
                    'plugins' : '../lib/durandal/js/plugins',
                    'transitions' : '../lib/durandal/js/transitions',
                    'knockout': '../lib/knockout/knockout-3.3.0',
                    'knockout_validation': '../lib/knockout/knockout.validation',
                    'bootstrap-tooltip': '../lib/bootstrap/tooltip',
                    'bootstrap-transition': '../lib/bootstrap/transition',
                    'bootstrap-collapse': '../lib/bootstrap/collapse',
                    'bootstrap-dropdown': '../lib/bootstrap/dropdown',
                    'jquery': '../lib/jquery/jquery-1.11.0',
                    'amplify': '../lib/amplify/amplify',
                    'scripts' : '../scripts',
                    'jquerytransit' : '../lib/jquery/transit',
                    'jquerypep' : '../lib/jquery/jquery.pep',
                    'jquerycookie' : '../lib/jquery/jquery.cookie',
                    'typeahead' : '../lib/jquery/typeahead',
                    'response' : '../lib/zlib/inflate_stream'
                },
                preserveLicenseComments: false,
                keepBuildDir: true,
                optimize: "uglify2",
                //generateSourceMaps: true,
                //optimize: "none",
                pragmas: {
                    build: true
                },
                wrap: true,
                shim: {
                    'knockout_validation' : {
                        deps : ['knockout']
                    },
                    'bootstrap-tooltip': {
                        deps: ['jquery'],
                        exports: '$.fn.tooltip'
                    },
                    'bootstrap-collapse': {
                        deps: ['jquery'],
                        exports: '$.fn.collapse'
                    },
                    'bootstrap-transition': {
                        deps: ['jquery'],
                        exports: '$.fn.emulateTransitionEnd'
                    },
                    'bootstrap-dropdown': {
                        deps: ['jquery'],
                        exports: '$.fn.dropdown'
                    },
                    'jquerytransit' : {
                        deps : ['jquery']
                    },
                    'jquerypep' : {
                        deps : ['jquery']
                    },
                    'amplify' : {
                        deps : ['jquery'],
                        exports : 'amplify',
                    },
                    'typeahead' : {
                        deps : ['jquery']
                    },
                },
            };

        //#endregion

        //#region Private Methods

        function ensureRequireConfig(params) {
            if (params.includeMain)
                params.insertRequire.push("main");

            params.insertRequire = _.uniq(params.insertRequire);
            params.includes = _.uniq(params.includes);
            params.excludes = _.uniq(params.excludes);

            if (params.paths)
                params.paths = _.extend({}, defaultRequireConfig.paths, params.paths);

            if (params.pragmas)
                params.pragmas = _.extend({}, defaultRequireConfig.pragmas, params.pragmas);
        }

        function includePath(array, config, url) {
            if (url.indexOf(config.out) !== -1 || url.indexOf("durandal/amd") !== -1)
                return;

            var ext = path.extname(url);
            url = path.relative(config.baseUrl, url);
            url = url.replace(/\\/g, "/");

            var pathToReplace = _.chain(config.paths)
                                .map(function (_path, key) { return { key: key, path: _path }; })
                                .filter(function (_path) { return url.indexOf(_path.path) !== -1; })
                                .max(function (_path) { return _path.path.length; })
                                .value();

            if (pathToReplace)
                url = url.replace(pathToReplace.path, pathToReplace.key);

            if (ext === ".html") {
                url = "text!" + url;
            }
            else if (ext === ".js") {
                url = url.replace(new RegExp("\\" + ext + "$"), "");
            }
            else
                return;

            array.push(url);
        }

        //#endregion
        var done = this.async(),
            config,
            params = this.options({
                baseUrl: "app",
                out: "../build/js/main-built.js",
                mainPath: "app/main.js",
                include: [],
                exclude: [],
                insertRequire: [],
                loglevel: 4,
                includeMain: true,
            });

        ensureRequireConfig(params);
        config = _.extend({}, defaultRequireConfig, params);

        this.files.forEach(function (file) {
            file.src.forEach(_.partial(includePath, config.include, config));
        });

        grunt.log.ok("+++++++++++++++++++++++++++", config);

        requirejs.optimize(
            config,
            function (response) {
                if (params.loglevel === "verbose")
                    grunt.log.write(response);

                grunt.log.ok(params.out + " created !");

                done(true);
            },
            function (error) {
                grunt.log.error(error);

                done(false);
            }
        );
    });
};
