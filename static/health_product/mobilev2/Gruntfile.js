module.exports = function (grunt) {
    grunt.initConfig({
        durandal: {
            dist: {
                src: [
                    'app/assets/js/**/**/*.*',
                    'app/assets/js/**/*.*',
                    'app/lib/**/*.*',
                    'app/lib/**/**/*.*',
                    '!app/lib/routie/*.*',

                    '!app/lib/anima/*.*',
                    '!app/lib/amplify/*.*',
                    '!app/lib/bootstrap/*.*',
                    '!app/lib/jquery/combodate.js',
                    '!app/lib/jquery/jquery-1.11.0.js',
                    '!app/lib/jquery/jquery.cookie.js',
                    '!app/lib/jquery/jquery.pep.js',
                    '!app/lib/jquery/moment.js',
                    '!app/lib/jquery/transit.js',
                    '!app/lib/jquery/typeahead.js',
                    '!app/lib/knockout/knockout-3.0.0.debug.js',
                    '!app/lib/knockout/knockout-3.0.0.js',
                    '!app/lib/knockout/knockout-3.1.0.debug.js',
                    '!app/lib/require/almond-custom.js',
                    '!app/lib/require/r.js',
                    '!app/lib/require/require.js',
                    '!app/lib/require/text.js',
                    '!app/lib/requirejs-plugins/async.js',
                    '!app/lib/requirejs-plugins/depend.js',
                    '!app/lib/requirejs-plugins/font.js',
                    '!app/lib/requirejs-plugins/goog.js',
                    '!app/lib/requirejs-plugins/image.js',
                    '!app/lib/requirejs-plugins/json.js',
                    '!app/lib/requirejs-plugins/mdown.js',
                    '!app/lib/requirejs-plugins/noext.js',
                    '!app/lib/requirejs-plugins/propertyParser.js',
                ],
                dest: 'build/js/main-built.js'
            }
        },
        strip : {
            main : {
                src : 'build/js/main-built.js',
                dest : 'build/js/main-health-built.js',
                options : {
                    nodes : ['console.log', 'debug', 'console.debug', 'console.group', 'console.groupEnd']
                }
            }
        },
        copy: {
            images: {
                cwd: 'app/assets/img',
                src: '**/*',
                dest: 'build/img/',
                expand: true,
            },
            fonts : {
                cwd : 'app/assets/fonts',
                src: "**/*",
                dest : 'build/fonts/',
                expand: true,
            }

        },
        concat : {
            css_main_product : {
                src : [
                    'app/assets/css/bootstrap.min.css',
                    'app/assets/css/medical_screen.css',
                    'app/assets/css/mob-style.css',
                ],
                dest : 'build/css/main-built-concat.css'
            },
        },
        cssmin : {
            css_main_product : {
                src : 'build/css/main-built-concat.css',
                dest : 'build/css/main-built.css'
            },
        },
    });

    grunt.loadTasks('tasks');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-strip');
    grunt.loadNpmTasks('grunt-newer');

    grunt.registerTask('default', ['newer:durandal', 'newer:concat:css_main_product', 'newer:cssmin:css_main_product', 'newer:copy', 'newer:strip']);
};
