'use strict';
require.config({
    baseUrl : '/static/health_product/mobilev2/app',
    paths: {
        'text': '/static/health_product/mobilev2/app/lib/require/text',
        'knockout': '/static/health_product/mobilev2/app/lib/knockout/knockout-3.1.0',
        'knockout_validation': '/static/health_product/mobilev2/app/lib/knockout/knockout.validation',
        'knockout-amd-helpers': '/static/health_product/mobilev2/app/lib/knockout/knockout-amd-helpers',
        'knockout-history': '/static/health_product/mobilev2/app/lib/knockout/knockout-history',
        'knockout-router': '/static/health_product/mobilev2/app/lib/knockout/knockout-router',
        'cookie' : '/static/health_product/mobilev2/app/lib/cookies/cookies',
        'response' : '/static/health_product/mobilev2/app/lib/zlib/inflate_stream',
        'jquery' : '/static/health_product/mobilev2/app/lib/jquery/cf-jquery',
        'bstore' : '/static/health_product/mobilev2/app/assets/js/scripts/bstore',
        'pincheck' : '/static/health_product/mobilev2/app/assets/js/scripts/pincheck',
        'step_nav' : '/static/health_product/mobilev2/app/assets/js/models/step_nav',
        'step1' : '/static/health_product/mobilev2/app/assets/js/models/step1',
        'step2' : '/static/health_product/mobilev2/app/assets/js/models/step2',
        'step3' : '/static/health_product/mobilev2/app/assets/js/models/step3',
        'step_lead' : '/static/health_product/mobilev2/app/assets/js/models/step_lead',
        'results' : '/static/health_product/mobilev2/app/assets/js/models/results',
        'login' : '/static/health_product/mobilev2/app/assets/js/models/login',
        'utils' : '/static/health_product/mobilev2/app/assets/js/models/utils',
        'custom' : '/static/health_product/mobilev2/app/assets/js/scripts/custom',
        'offline_model': '/static/health_product/mobilev2/app/assets/js/models/offline_model'
    },
    shim: {
        'knockout_validation' : {
            deps : ['knockout']
        },
        'knockout-amd-helpers': {
            deps: ['knockout']
        },
        'knockout-history': {
            deps: ['knockout']
        },
        'knockout-router': {
            deps: ['knockout', 'knockout-history', 'knockout-amd-helpers']
        },
    },
    urlArgs: 'v=1.0.0.0'
});

define(['assets/js/main', 'bstore'], function(main) {
    main.run();
});
