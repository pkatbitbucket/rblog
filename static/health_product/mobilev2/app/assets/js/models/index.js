define(['step_nav', 'step1', 'step2', 'step3', 'results', 'step_lead', 'login', 'offline_model', 'jquery', 'knockout-amd-helpers'], function(stepNav, step1, step2, step3, results, stepLead, login, offline_model, $) {

    var contact_model = ko.observable();

    var contact_submit = function(){
        big_errors = ko.validation.group(contact_model(), { deep: true});
        if(big_errors().length > 0) {
            big_errors.showAllMessages();
        }
        else{
            var data = contact_model();
            data.mobile = contact_model().mobile();
            data.campaign = 'Health';
            data.label = 'offline';
            data.triggered_page = window.location.href;
            data.device = 'mobile';

            $.post("/leads/save-call-time/", {
                'data' : JSON.stringify(data),
                'source': 'transaction_medical',
                'csrfmiddlewaretoken' : CSRF_TOKEN,
            }, function(data){
                if (data.success){
                    $('#before_submit').addClass('hide');
                    $('#after_submit').removeClass('hide');
                }
                else{
                }
            }, 'json');

        }
    };

    return {
        initialize : function(activity_id, set_type) {
            var cmodel = new offline_model.contactData();
            cmodel.initialize();
            contact_model(cmodel);
            if(['offline','age-offline','cover-offline'].indexOf(set_type) > -1){
                results.overlayMode(set_type);
            }
            else if ((!activity_id) || activity_id.substr(0,4) == "step") {
                results.overlayMode('');
                var step = activity_id || '';
                var stage = parseInt(step.replace( /^\D+/g, ''));
                if(!(stage >=1 && stage <=3)) {
                    stage = 1;
                }
                stepNav.set_stage(stage);
            }
            else {
                results.overlayMode('');
                var step = activity_id || '';
                stepNav.currentStep(4);
                results.setup(activity_id);
            }
            return {
                'stepNav': stepNav,
                'step1' : step1,
                'step2' : step2,
                'step3' : step3,
                'results': results,
                'stepLead': stepLead,
                'login' : login,
                'health_toll_free': HEALTH_TOLL_FREE,
                'contact_model' : contact_model,
                'contact_submit': contact_submit,
            };
        }
    };
});
