define(['knockout-router', 'knockout', 'jquery', 'response', 'step1', 'step2', 'step3', 'utils', 'custom','knockout_validation', 'knockout-amd-helpers'], function(router, ko, $, response, step1, step2, step3, utils, custom) {

    var global_shortlist = ko.observableArray([]);
    var global_cartItems = ko.observableArray([]);
    // represent a single Plan item in a tab
    var Plan = function (data, uidata) {
        var self = this;
        self.visible = ko.observable(true);
        self.slab_id = data.slab_id;
        self.product_id = data.product_id;
        self.insurer_id = data.insurer_id;
        self.max_age = data.max_age;
        self.five_year_projection = data.five_year_projection;
        self.product_title = data.product_title;
        self.insurer_title = data.insurer_title;
        self.sum_assured = parseInt(data.sum_assured);
        self.deductible = parseInt(data.deductible);
        self.premium = parseFloat(data.premium);
        self.cpt_id = data.cpt_id;
        self.attr_list = data.score;
        self.attr = {};
        self.general = data.general;
        self.uidata = uidata;
        self.member_verbose = utils.member_verbose;

        var medical_summary = {};
        for(var i=0; i < data.general.conditions.length; i++){
            var cond = data.general.conditions[i];
            if(!(cond.period in medical_summary)){
                medical_summary[cond.period] = [];
            }
            medical_summary[cond.period].push(cond);
        }

        self.medical_summary = [];
        for(var period in medical_summary) {
            var conds = medical_summary[period];
            if(conds.length > 3) {
                var i=0;
                for(;i < parseInt(conds.length/3);i++){
                    self.medical_summary.push({
                        'period' : period,
                        'conditions' : conds.slice(3*i, (3*i)+3),
                    });
                }
                if(conds.length%3 !== 0) {
                    self.medical_summary.push({
                        'period' : period,
                        'conditions' : conds.slice((3*i), conds.length),
                    });
                }
            } else {
                self.medical_summary.push({'period' : period, 'conditions' : conds});
            }
        }

        self.ped = data.ped;
        self.standard_exclusions = data.standard_exclusions;
        self.recommended = data.recommended;

        if(self.five_year_projection.length != 5) {
            self.sum_assured_after_5_years = null;
            self.total_premium_paid_in_5_years = null;
        } else {
            self.sum_assured_after_5_years = self.five_year_projection[4][2];
            self.total_premium_paid_in_5_years = 0;
            $.each(self.five_year_projection, function(i, d){
                self.total_premium_paid_in_5_years += d[1];
            });
        }
        self.five_year_cover = self.five_year_projection.map(function(item){
           return item[2];
        });
        ko.utils.arrayForEach(data.score, function(listd) {
            self.attr[listd[0]] = listd[1];
        });
        self.score = parseInt(data.total_score);
        self.medical_score = parseInt(data.medical_score);
        self.covered_hospitals = data.covered_hospitals;

        self.is_hospital_covered = function(data){
            for(var i=0; i < self.covered_hospitals.length; i++){
                if(self.covered_hospitals[i].id == data.id){
                    return true;
                }
            }
            return false;
        };

        self.comparePlan = function(p){
            return ((self.slab_id == p.slab_id) &&
                    utils.deepCompare(self.uidata, p.uidata));
        };

        self.planInCart = ko.computed(function(){
            if(global_cartItems().length === 0){
                return false;
            }
            var cmp = false;
            for(var i=0;i<global_cartItems().length;i++){
                cmp = self.comparePlan(global_cartItems()[i].plan);
                if(cmp){
                    return cmp;
                }
            }
            return cmp;
        });

        self.planInShortlist = ko.computed(function(){
            if(global_shortlist().length === 0){
                return false;
            }
            var cmp = false;
            for(var i=0;i<global_shortlist().length;i++){
                cmp = self.comparePlan(global_shortlist()[i].plan);
                if(cmp){
                    return cmp;
                }
            }
            return cmp;
        });
    };
    function Filter(fdata, rtab) {
        var self = this;
        self.tab = rtab;
        console.log("SETTING SA IN FILTER>>>>>>", fdata.sum_assured);
        self.sum_assured = ko.observable(fdata.sum_assured);
        self.deductible = ko.observable(fdata.deductible);
        self.premium = ko.observable(fdata.premium);
        self.brands = ko.observable(fdata.brands);
        self.room_rent = ko.observable('INIT');
        self.copay = ko.observable(false);
        self.ped = ko.observable('INIT');
        self.ci = ko.observable('INIT');
        self.maternity = ko.observable('INIT');
        self.claim = ko.observable('INIT');
        self.filteredBrands = ko.observableArray(fdata.brands);
        self.premiumLoader = ko.observable(false);
        var reload_plans = function(pdata) {
            var invalid_step = 0;
            if(!step1.validate()) invalid_step = 1;
            else if(!step2.validate()) invalid_step = 2;
            else if(!step3.validate()) invalid_step = 3;
            if(invalid_step > 0) {
                ko.router.navigate("step" + invalid_step, {trigger: true});
                self.tab.filter.premiumLoader(false);
                return false;
            }
            var json_promise = $.post('/health-plan/get-score/'+utils.ACTIVITY_ID+'/', {
                'csrfmiddlewaretoken' : CSRF_TOKEN,
                'data': JSON.stringify(pdata),
                'result_type' : self.tab.type,
                'filters': JSON.stringify(FILTERS)
            }, function(res) {
                score_data = response.parseResponse(res);
                var ndata = score_data.score_data;
                self.tab.selectedPlan(null);
                self.tab.uidata(ndata[0].uidata);
                self.tab.plans(ko.utils.arrayMap(ndata[0].plans, function(item){
                    return new Plan(item, self.tab.uidata());
                }));
                self.tab.filter.sum_assured(parseInt(self.tab.uidata().sum_assured));
                self.tab.filter.premium(self.tab.maxPremium());
                self.tab.filter.brands(self.tab.unique_brand_name());
                self.tab.filter.filteredBrands(self.tab.unique_brand_name());
                self.tab.filter.premiumLoader(false);
                self.tab.UIChanged(true);
            });
        };
        self.sum_assured.subscribe(function(sa) {
            self.premiumLoader(true);
            var pdata = utils.post_data();
            pdata.sum_assured = parseInt(sa);
            reload_plans(pdata);
        });
        self.deductible.subscribe(function(deductible) {
            self.premiumLoader(true);
            step3.deductible(deductible);
            var pdata = utils.post_data();
            pdata.deductible = parseInt(deductible);
            reload_plans(pdata);
        });
    }

    function Tab(data, result) {
        var self = this;
        self.result = result;
        self.selectedPlan = ko.observable();
        self.throttledSelectedPlan = ko.computed(function() {
            return self.selectedPlan();
        }).extend({ throttle: 1000 });
        self.name = data.name;
        self.type = data.type;
        self.uidata = ko.observable(data.gdata.uidata);
        self.plans = ko.observableArray(ko.utils.arrayMap(data.gdata.plans, function(item){
            return new Plan(item, self.uidata());
        }));
        self.UIChanged = ko.observable(false);

        self.unique_brand_name = function(){
            console.log("Updating Brand names");
            return ko.utils.arrayGetDistinctValues(
                ko.utils.arrayMap(self.plans(), function(item){
                return item.insurer_title;
            })).sort();
        };

        self.visiblePlans = ko.computed(function(){
            return ko.utils.arrayFilter(self.plans(), function(item){
                return item.visible();
            });
        });

        var premiums = ko.computed(function(){
            console.log("Updating Premiums");
            return ko.utils.arrayMap(self.plans(), function(item){
                return parseInt(item.premium);
            });
        });
        self.maxPremium = function(){
            console.log("Updating Max Premium");
            return (parseInt(Math.max.apply(Math, premiums())/1000)+1)*1000;
        };
        self.premiumOptions = ko.computed(function(){
            console.log("Updating Premium options");
            var minPremium = (parseInt(Math.min.apply(Math, premiums())/1000)+1)*1000;
            var popts = [];
            var maxp = self.maxPremium();
            for(var i=minPremium; i<=maxp; i+=1000) {
                popts.push(i);
            }
            return popts;
        });
        console.log("UIDATA >>>>>>", self.uidata());
        self.filter = new Filter({
            'sum_assured': parseInt(self.uidata().sum_assured),
            'deductible': parseInt(self.uidata().deductible),
            'premium': self.maxPremium(),
            'brands': self.unique_brand_name()
        }, self);
        self.displayBottomTabs = ko.observable();
        self.displayTabText = ko.computed(function(){
            console.log("Updating Display Tab text");
            var kids = self.uidata().members.filter(function(item){
                return item.displayName == 'son' || item.displayName == 'daughter';
            });
            var adults = self.uidata().members.filter(function(item){
                return item.displayName != 'son' && item.displayName != 'daughter';
            });
            if(self.type == 'parents') {
                if(adults.length == 1){
                    self.displayBottomTabs(adults[0].displayName);
                    return 'Plans for ' + adults[0].displayName;
                } else {
                    self.displayBottomTabs('parents');
                }
                return "For parents";
            }
            if(self.type == 'family') {
                if(adults.length == 1 && kids.length === 0) {
                    self.displayBottomTabs(adults[0].displayName);
                    return 'Plans for ' + adults[0].displayName;
                } else {
                    self.displayBottomTabs('Nuclear family');
                }
                return 'Nuclear family';
            }
            if(self.type == 'combined') {
                self.displayBottomTabs('All members');
                return 'All members';
            }
        });
        self.displayTabHeader = ko.observable();
        self.displayContext = ko.computed(function() {
            console.log("Updating Display Context");
            var members = self.uidata().members;
            var personArray = [];
            for (var j = members.length - 1; j >= 0; j--) {
                personArray.push(members[j].displayName);
            }
            var kids = personArray.filter(function(item){ return item == 'son' || item == 'daughter';});
            var adults = personArray.filter(function(item){ return item != 'son' && item != 'daughter';});
            var adultsString = adults.join(', ').toLowerCase();
            if( kids.length === 0) {
                self.displayTabHeader('Plans for ' + adultsString);
                return 'Plan for ' + adultsString;
            }
            if(kids.length > 1) {
                self.displayTabHeader('Plans for ' + adultsString + ' and ' + kids.length + ' kids');
                return 'Plan for ' + adultsString + ' and ' + kids.length + ' kids';
            }
            if(kids.length == 1) {
                self.displayTabHeader('Plans for ' + adultsString + ' and ' + kids.length + ' kid');
                return 'Plan for ' + adultsString + ' and ' + kids.length + ' kid';
            }
        });

        self.coverOptions = [
            { name: 'upto 2 Lacs', id: 100000 },
            { name: '2 Lacs to 2.5 Lacs', id: 200000 },
            { name: '3 Lacs to 3.5 Lacs', id: 300000 },
            { name: '4 Lacs to 4.5 Lacs', id: 400000 },
            { name: '5 Lacs to 5.5 Lacs', id: 500000 },
            { name: '6 Lacs to 6.5 Lacs', id: 600000 },
            { name: '7 Lacs to 7.5 Lacs', id: 700000 },
            { name: '8 Lacs to 8.5 Lacs', id: 800000 },
            { name: '9 Lacs to 9.5 Lacs', id: 900000 },
            { name: '10 Lacs to 15 Lacs', id: 1000000 },
            { name: '15 Lacs +', id: 1500000 }
        ];

        self.deductibleOptions = [
            {name: '1 Lac', id: 100000},
            {name: '2 Lacs', id: 200000},
            {name: '3 Lacs', id: 300000},
            {name: '4 Lacs', id: 400000},
            {name: '5 Lacs', id: 500000},
            {name: '6 Lacs', id: 600000},
            {name: '7 Lacs', id: 700000},
            {name: '8 Lacs', id: 800000},
            {name: '9 Lacs', id: 900000},
            {name: '10 Lacs', id: 100000}
        ];
        self.filter_applied = ko.observable(false);
        self.filterPlans = ko.computed(function() {
            var flist = [
                {'name': 'maternity_cover', 'property': self.filter.maternity()},
                {'name': 'critical_illness_coverage', 'property': self.filter.ci()}
            ];
            var filter_applied = false;
            console.log("FLIST", flist);
            ko.utils.arrayForEach(self.plans(), function(p) {
                p.visible(true);
                ko.utils.arrayForEach(flist, function(f){
                    if(p.visible()){
                        if(f.property == 'INIT'){
                            p.visible(true);
                        } else {
                            filter_applied = true;
                            if( f.property == p.attr[f.name].available) {
                                p.visible(true);
                            } else {
                                p.visible(false);
                            }
                        }
                    }
                });

                //PED
                if(p.visible()){
                    if(self.filter.ped() == 'INIT'){
                        p.visible(true);
                    } else {
                        filter_applied = true;
                        if(parseInt(self.filter.ped()) >= p.ped.waiting_period) {
                            p.visible(true);
                        } else {
                            p.visible(false);
                        }
                    }
                }

                if(p.visible()){
                    if(!self.filter.copay()){
                        p.visible(true);
                    } else {
                        filter_applied = true;
                            if(p.attr.copay.value.toLowerCase() == 'no copay') {
                                p.visible(true);
                            } else {
                                p.visible(false);
                            }
                    }
                }
                //filter values : NO_LIMIT, PRIVATE, SINGLE
                //attr values :  NO_LIMIT, CLASSA, CLASSB, CLASSC, CLASSD
                if(p.visible()){
                    if(self.filter.room_rent() == 'INIT'){
                        p.visible(true);
                    } else {
                        filter_applied = true;
                        if(self.filter.room_rent() == "NO_LIMIT") {
                            if(p.attr.room_rent.value.meta.toLowerCase() == 'no_limit') {
                                p.visible(true);
                            } else {
                                p.visible(false);
                            }
                        }
                        if(self.filter.room_rent() == "PRIVATE") {
                            if(['no_limit', 'classa', 'classb', 'classc'].indexOf(p.attr.room_rent.value.meta.toLowerCase()) > -1) {
                                p.visible(true);
                            } else {
                                p.visible(false);
                            }
                        }
                    }
                }
                if(p.visible()){
                    if(self.filter.premium() >= p.premium) {
                        p.visible(true);
                    } else {
                        p.visible(false);
                    }
                }

                self.filter_applied(filter_applied);
                if(p.visible()){
                    if(self.filter.filteredBrands().indexOf(p.insurer_title) > -1) {
                        p.visible(true);
                    } else {
                        p.visible(false);
                    }
                }
            });
        }).extend({'throttle': 500});
        self.openPlan = function(plan){
            if(resultsView.tabMode() == 'openPlan') {
                return false;
            }
            self.selectedPlan(plan);
            resultsView.tabMode('openPlan');
            window.scrollTo(0,0);
            self.UIChanged(true);

            utils.event_log('PlanOpened', plan.insurer_title, 0);
            var nPlanMembers = plan.uidata.members.length;
            var vizury_plan_opened = {
                'vizury_category': 'HEALTH',
                'vizury_sub_category': nPlanMembers==1?'individual':'family',
                'po_price': plan.premium,
                'po_name': plan.insurer_title + ' - ' + plan.product_title,
                'po_sub_category': nPlanMembers==1?'individual':'family',
                'po_insurer_img_url': window.location.origin+'/static/health_product/img/insurer/insurer_' + plan.insurer_id + '/on.png'
            };
            utils.gtm_data_log(vizury_plan_opened);
            utils.gtm_event_log('vizury_plan_opened');
        };
        self.closePlan = function(){
            self.selectedPlan(null);
            resultsView.tabMode(null);
            self.current_property('');
            window.scrollTo(0,0);
            self.UIChanged(true);
        };
        self.current_property = ko.observable('');
        self.throttle_current_property = ko.computed(function() {
            return self.current_property();
        }).extend({ throttle: 200 });
        self.opened_descriptions = ko.observableArray([]);

        self.openProperty = function(property){
            if(self.current_property() == property){
                self.current_property('');
            } else {
                self.current_property(property);
            }
            self.opened_descriptions([]);
            // scroll to plan properties header position
            //window.scrollTo(0,180);
        };

        self.propDescription = function(desc) {
            console.log(">>>>> DESC", desc);
            if (self.opened_descriptions.indexOf(desc) > -1){
                self.opened_descriptions.remove(desc);
            }
            else{
                self.opened_descriptions.push(desc);
                console.log('opened description',self.opened_descriptions());
            }
        };
        self.benefitDescription = function(desc) {
            console.log(">>>>> DESC", desc, self.opened_descriptions());
            if (self.opened_descriptions.indexOf(desc[0]) > -1){
                self.opened_descriptions.remove(desc[0]);
            }
            else{
                self.opened_descriptions.push(desc[0]);
                console.log('opened benefit description',self.opened_descriptions());
            }
        };
        self.clearFilters = function(){
            var prefilledBrands = self.filter.brands().slice(0);
            self.filter.room_rent('INIT');
            self.filter.copay(false);
            self.filter.ped('INIT');
            self.filter.ci('INIT');
            self.filter.maternity('INIT');
            self.filter.claim('INIT');
            self.filter.filteredBrands(prefilledBrands);
            tempAlert("Filters cleared", 1500);
        };
    }

    function ShortlistItem(plan) {
        var self = this;
        self.plan = plan;
        self.openInfo = ko.observable(false);
        self.moreInfo = function() {
            self.openInfo(!self.openInfo());
        };
    }
    function CartItem(planContext, plan) {
        var self = this;
        self.planContext = planContext;
        self.plan = plan;
        self.openInfo = ko.observable(false);
        self.moreInfo = function() {
            self.openInfo(!self.openInfo());
        };
    }
    function tempAlert(msg, duration) {
        var el = document.createElement("div");
        el.setAttribute("class","tempAlert");
        el.innerHTML = msg;
        setTimeout(function(){
            el.parentNode.removeChild(el);
        }, duration);
        document.body.appendChild(el);
    }

    function ResultsViewModel() {
        var self = this;
        self.overlayMode = ko.observable(''); // for modal overlay mode
        self.initialMode = ko.observable(false); // for modify details and initial user step1,2,3
        self.tabMode = ko.observable();
        self.currentTab = ko.observable();
        self.throttledCurrentTab = ko.computed(function() {
            return self.currentTab();
        }).extend({ throttle: 200 });
        self.resultTabs = ko.observableArray();
        self.selectedMembers = ko.observableArray([]);
        self.service = ko.observable();
        self.userPincode = ko.observable();
        self.parentsPincode = ko.observable();
        self.userHospitals = [];
        self.parentsHospitals = [];
        self.shortlist = global_shortlist;
        self.cartItems = global_cartItems;
        self.UIChanged = ko.observable(false);
        self.tipAlert = ko.observable(null).extend({
            store: {
                key: 'tipAlert'
            }
        });
        self.tipAlertClose = function(){
            self.tipAlert(false);
        };
        // add tabs to page view

        self.removeOverlayMode = ko.observable(''); // used for remove from cart and remove from shortlist

        self.setRemoveOverlayMode = function(mode) {
            self.removeOverlayMode(mode);
        };
        self.closeRemoveOverlayMode = function() {
            self.removeOverlayMode('');
        };
        self.setOverlayMode = function(mode){
            self.overlayMode(mode);
            if(self.overlayMode() == 'filter_plans'){
                document.getElementById('cover-filter').scrollTop = 0;
            }
        };
        self.closeOverlayMode = function(){
            // TO CLOSE OPEN PLAN INFORMATION FROM CART/SHORTLIST ITEMS
            if(self.overlayMode() == 'shortlisted_plans' && self.shortlist().length > 0){
                for (var i = self.shortlist().length - 1; i >= 0; i--) {
                    self.shortlist()[i].openInfo(false);
                }
            }
            if(self.overlayMode() == 'buy_cart' && self.cartItems().length > 0){
                for (var i = self.cartItems().length - 1; i >= 0; i--) {
                    self.cartItems()[i].openInfo(false);
                }
            }
            // SET OVERLAY TO NONE
            if(['offline','age-offline','cover-offline'].indexOf(self.overlayMode()) > -1 ){
                ko.router.navigate(utils.ACTIVITY_ID, {'trigger': true});
            }
            else self.overlayMode('');
            self.UIChanged(true);
        };

        self.tabUIChanged = ko.computed(function(){
            for(var i=0; i<self.resultTabs().length; i++)
            {
                if(self.resultTabs()[i].UIChanged())
                {
                    self.resultTabs()[i].UIChanged(false);
                    self.UIChanged(true);
                    return true;
                }
            }
            return false;
        });

        self.changeTab = function(tab){
            self.currentTab(tab);
            resultsView.overlayMode('');
            window.scrollTo(0,0);
            console.log("Current Tab>>", self.currentTab());
            self.UIChanged(true);
        };

        self.addToShortlist = function(data) {
            self.shortlist.push(new ShortlistItem(data));
            console.log('Shortlist >> ', self.shortlist());
            tempAlert("Plan added to shortlist", 1500);
            update_selection();
        };
        self.removeShortlistedItem = function(shortlisted_item){
            self.shortlist.remove(shortlisted_item);
            self.removeOverlayMode('');
            tempAlert("Plan removed from shortlist", 1500);
            update_selection();
        };
        var get_shortlisted_plans = function(){
            var json_promise = $.get('/health-plan/get-details/shortlist/', {
                'csrfmiddlewaretoken' : CSRF_TOKEN,
            }, function(res){
                var parsedResponse = response.parseResponse(res);
                console.log("parsed shortlist response > ", parsedResponse);
                var stlist = [];
                ko.utils.arrayForEach(parsedResponse.shortlist, function(item){
                    var plan = new Plan(item.plans[0], item.uidata);
                    if (!plan.planInShortlist()) {
                        stlist.push(new ShortlistItem(plan));
                    }
                });
                self.shortlist(stlist);
            });
        };

        var update_selection = function () {
            var data = ko.utils.arrayMap(self.shortlist(), function(st){
                return {
                    'slab_id': st.plan.slab_id,
                    'uidata' : st.plan.uidata,
                };
            });
            var request = $.post('/health-plan/save-details/shortlist/?hot=true', {
                'csrfmiddlewaretoken': CSRF_TOKEN,
                'data': JSON.stringify({
                    'shortlist': data
                })
            }, function (res) {
            });
        };

        self.addToCart = function(data) {
            var planContext = self.currentTab().displayContext;
            self.cartItems.push(new CartItem(planContext, data));
            tempAlert("Plan added to cart", 1500);
        };
        self.removeFromCart = function(plan){
            self.cartItems.remove(plan);
            self.removeOverlayMode('');
            tempAlert("Plan removed from cart", 1500);
        };
        self.shiftToCart = function(plan){
            self.cartItems.push(plan);
            tempAlert("Plan added to your cart", 1500);
        };
        self.selfRemoveShortlist = function(plan) {
            var match_st = ko.utils.arrayFirst(self.shortlist(), function(st){
                return (st.plan.slab_id == plan.slab_id);
            });
            if(match_st) {
                self.shortlist.remove(match_st);
                self.removeOverlayMode('');
                tempAlert("Plan removed from shortlist", 1500);
                update_selection();
            }
        };

        var create_transaction = function(plan) {
            var post_data = utils.post_buy_data(plan);
            post_data['slab_id'] = plan.slab_id;
            post_data['cpt_id'] = plan.cpt_id;
            post_data['tparty'] = window.TPARTY;
            $.post('/health-plan/transaction/create/', {'csrfmiddlewaretoken' : CSRF_TOKEN, 'data' : JSON.stringify(post_data)}, function(data){
                utils.gtm_data_log({'transaction_id': data.transaction_id});
                window.location.href = "/health-plan/buy/" + data.transaction_id;
            }, 'json');
            return false;
        };

        self.buyPlan = function(plan) {
            post_data = {'csrfmiddlewaretoken' : CSRF_TOKEN,
                         'age': plan.max_age,
                         'slab_id': plan.slab_id}
            $.post("/health-plan/get-processing-status/", post_data , function(data){
                if(data.status){
                    processing_status = data.status;
                }
                else{
                    processing_status = 'BACKEND_NOT_INTEGRATED';
                }
                switch(processing_status){
                    case 'BACKEND_NOT_INTEGRATED':
                        utils.event_log('Offline', processing_status, 0);
                        ko.router.navigate(utils.ACTIVITY_ID+"/offline", {'trigger': true});
                        break;
                    case 'OFFLINE_COVER_TOO_HIGH':
                        utils.event_log('Offline', processing_status, 0);
                        ko.router.navigate(utils.ACTIVITY_ID+"/cover-offline", {'trigger': true});
                        break;
                    case 'OFFLINE_AGE_TOO_HIGH':
                        utils.event_log('Offline', processing_status, 0);
                        ko.router.navigate(utils.ACTIVITY_ID+"/age-offline", {'trigger': true});
                        break;
                    case 'FILL_FORM':
                    case 'MEDICAL_REQUIRED':
                        utils.event_log('TransactionInitiated', processing_status, 0);
                        create_transaction(plan);
                        break;
                }
            });
        };

        var tab_name = {'parents': 'parents', 'family': 'nuclear family', 'combined': 'all members'};
        self.setup = function(ACTIVITY_ID) {
            utils.ACTIVITY_ID = ACTIVITY_ID;
            var json_promise = $.post('/health-plan/get-score/'+utils.ACTIVITY_ID+'/', {
                'csrfmiddlewaretoken' : CSRF_TOKEN,
                'data': JSON.stringify({}),
                'filters': JSON.stringify(FILTERS)
            }, function(res) {
                var score_data = response.parseResponse(res);
                step3.product_type(score_data.uidata.product_type);
                console.log(">>>>", score_data.score_data);
                var gdata = score_data.score_data;
                // PLAN TAB FOR PARENTS
                var tab_list = [];
                var max_plans_id = 0;
                for (var i = 0; i < gdata.length; i++) {
                    var TabData = {
                        name : tab_name[gdata[i].type],
                        type : gdata[i].type,
                        gdata: gdata[i]
                    };
                    console.log('TabData >>>', TabData);
                    var rtab = new Tab(TabData, self);
                    tab_list.push(rtab);
                    max_plans_id = (tab_list[i].plans().length < tab_list[max_plans_id].plans().length) ? max_plans_id : i;
                }
                self.resultTabs(tab_list);
                self.currentTab(self.resultTabs()[max_plans_id]);

                var vizury_tab = self.resultTabs().length>1 ? self.resultTabs()[1] : self.resultTabs()[0];
                var vizury_plans = {
                    'vizury_category': 'HEALTH'
                };
                for(var i=0; i<vizury_tab.plans().length && i<3; i++) {
                    var plan = vizury_tab.plans()[i];
                    var j = i+1;
                    var nPlanMembers = plan.uidata.members.length;
                    vizury_plans['city'] = plan.uidata.city.name;
                    vizury_plans['product'+j+'_name'] = plan.insurer_title + ' - ' + plan.product_title;
                    vizury_plans['product'+j+'_price'] = plan.premium;
                    vizury_plans['product'+j+'_sub_category'] = nPlanMembers==1?'individual':'family';
                    vizury_plans['product'+j+'_insurer_img_url'] = window.location.origin+'/static/health_product/img/insurer/insurer_' + plan.insurer_id + '/on.png';
                }
                utils.gtm_data_log(vizury_plans);
                utils.gtm_event_log('vizury_results');


                console.log('Result tabs', self.resultTabs());
                self.UIChanged(true);

                console.log('current tab',self.currentTab());
                if(self.tipAlert() == null) {
                    setTimeout(function(){
                        self.tipAlert(true);
                    }, 1000);
                }
                get_shortlisted_plans();
                utils.event_log('Results', 'Viewed', 2);
                utils.gtm_event_log('results_visited');
            });
        };
    }
    var resultsView = new ResultsViewModel();
    return resultsView;
});

