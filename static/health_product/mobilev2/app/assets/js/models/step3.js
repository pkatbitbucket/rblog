define(['knockout-router', 'knockout', 'step1', 'pincheck', 'knockout_validation'], function(router, ko, step1View, pincheck) {

    function stepThreeViewModel() {
        var self = this;
        self.product_type = ko.observable('INDEMNITY').extend({
            store: {key: 'product_type'}
        });
        self.selectProductType = function(p){
            self.product_type(p);
        };
        self.is_topup = ko.computed(function() {
            return ['TOPUP', 'SUPER_TOPUP'].indexOf(self.product_type()) != -1;
        });
        self.deductibleOptions = [
            {id: 100000, name: '1 Lac'},
            {id: 200000, name: '2 Lacs'},
            {id: 300000, name: '3 Lacs'},
            {id: 400000, name: '4 Lacs'},
            {id: 500000, name: '5 Lacs'},
            {id: 600000, name: '6 Lacs'},
            {id: 700000, name: '7 Lacs'},
            {id: 800000, name: '8 Lacs'},
            {id: 900000, name: '9 Lacs'},
            {id: 1000000, name: '10 Lacs'}
        ];
        self.deductible = ko.observable().extend({
            store: { key : 'deductible' },
            required: {
                params: true,
                message: 'Please select your existing sum assured'
            }
        });
        self.userPincode = ko.observable() .extend({
            store : { key : 'pincode' },
            required: {
                params:true,
                message: "Please select your area pincode"
            },
            validation : {
                validator : function(val){
                    return pincheck.isValid(val);
                },
                message: "Please enter a valid pincode"
            }
        });
        self.parentsPincode = ko.observable() .extend({
            store : { key : 'parents_pincode' },
            required: {
                params:true,
                message: "Please select your parents' area pincode"
            },
            validation : {
                validator : function(val){
                    return pincheck.isValid(val);
                },
                message: "Please enter a valid pincode"
            }
        });
        self.hasParents = function() {
            return ko.utils.arrayFilter(step1View.getSelectedMembers(), function(member) {
                return member.person == 'Father' || member.person == 'Mother';
            }).length > 0;
        };
        self.hasFamily = function() {
            return ko.utils.arrayFilter(step1View.getSelectedMembers(), function(member) {
                return member.person == 'You' || member.person == 'Spouse' || member.type == 'kid';
            }).length > 0;
        };

        self.getErrorGroup = function(){
            var validation_group = [];
            if(self.is_topup())
                validation_group.push(self.deductible);
            if(self.hasParents() && self.hasFamily()){
                validation_group.push(self.userPincode, self.parentsPincode);
            }
            if(!self.hasParents() && self.hasFamily()) {
                validation_group.push(self.userPincode);
            }
            if(self.hasParents() && !self.hasFamily()) {
                validation_group.push(self.parentsPincode);
            }
            self.step3Errors = ko.validation.group(validation_group);
        };
        self.validate = function() {
            valid = true;
            self.getErrorGroup();
            if(self.step3Errors().length !== 0) {
                self.step3Errors.showAllMessages();
                valid = false;
            }
            return valid;
        };
    }
    return new stepThreeViewModel();
});
