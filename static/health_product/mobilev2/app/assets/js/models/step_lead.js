define(['knockout', 'jquery', 'utils', 'cookie', 'results', 'knockout_validation'], function(ko, $, utils, Cookies, results) {
    function stepLeadViewModel() {
        var self = this;
        self.expertImage = ko.observable('/static/health_product/mobilev2/app/assets/img/experts/experts-flipper.gif');
        self.leadOverlay = ko.observable(false);
        self.thanksMessage = ko.observable(false);
        self.mobileNo = ko.observable(Cookies.get('mobileNo') || Cookies.get('aml_mobile')).extend({
            required: {
                params: true,
                message: "Please enter your mobile number"
            },
            pattern: {
                params: '^([1-9])([0-9]){9}$',
                message: "Please enter a valid number"
            }
        });

        self.email = ko.observable().extend({
            required:{
                params: true,
                message: "Please enter your email address"
            },
            email: true
        });
        self.sendBuyForm = ko.observable(false);
        self.selectedPlan = ko.observable();
        self.currentPlan = ko.computed(function() {
            if(self.selectedPlan()) {
                return self.selectedPlan().insurer_title + ' - ' + self.selectedPlan().product_title;
            }
            else {
                return "";
            }
        });
        self.plan_to_buy = '';
        self.src = '';
        self.isBuyScreen = function() {
            return results.overlayMode() == 'buy_plan';
        };
        self.showLeadCard = ko.observable(!(Cookies.get("mobileNo")));
        self.loading = ko.observable(false);
        self.validate = function() {
            var valid = true;
            self.mobileNo.isModified(true);
            valid = valid &&  self.mobileNo.isValid();
            if(self.isBuyScreen()) {
                self.email.isModified(true);
                valid = valid && self.email.isValid();
            }
            else {
                if(!valid) {
                    alert(self.mobileNo.error());
                }
            }
            return valid;
        };
        self.callMe = function(label) {
            if(self.validate()) {
                if(!confirm("I hereby authorize Coverfox to communicate with me on the given number for my Insurance needs. I am aware that this authorization will override my registry under NDNC.")){
                    return false;
                }
                if(self.mobileNo() == Cookies.get('aml_mobile')) {
                    label += "|prefilled";
                }
                var post_to_lms = function(extra_data){
                    extra_data = extra_data || {};
                    var post_data = {
                        'campaign': 'Health',
                        'mobile': self.mobileNo(),
                        'activity_id': utils.ACTIVITY_ID,
                        'triggered_page': window.location.href,
                        'label': label.split('|').join('_'),
                        'device': 'Mobile'
                    };
                    for(k in extra_data){
                        post_data[k] = extra_data[k];
                    }
                    self.loading(true);
                    if(self.isBuyScreen()) {
                        post_data['email'] = self.email();
                        post_data['slab_id'] = self.selectedPlan().slab_id;
                        post_data['plan_details'] = self.plan_to_buy;
                        post_data['send_buy_form'] = self.sendBuyForm()?'y':'n';
                        post_data['src'] = self.src;
                    }
                    post_data['src'] = 'mobile|' + post_data['src'];

                    $.ajax({
                        url: '/leads/save-call-time/',
                        type: 'POST',
                        data: {'data':JSON.stringify( post_data), 'csrfmiddlewaretoken': CSRF_TOKEN},
                        success: function(res) {
                            Cookies.set('mobileNo', self.mobileNo(), {expires:3600});
                            var src_arr = post_data['src'].split('|');
                            /*  mobile|expert-page|pre-results
                             *  mobile|expert-card|results
                             *  mobile|buy|results|slab-xx
                             *  mobile|buy|shortlisted|slab-xx
                             *  mobile|buy|plan_details|main_page|slab-xx
                             *  mobile|buy|plan_details|bottom_button|slab-xx
                             */
                            var src = 'Mobile';
                            if(src_arr[1] == 'buy') {
                                src += ' buy';
                                //if(src_arr[2] == 'plan_details') { src += ' (plan details - '+src_arr[3]+')'; }
                                //else { src += ' ('+src_arr[2]+')'; }
                            }
                            else { src += ' '+src_arr[1] + ' ('+src_arr[2]+')'; }
                            if(window.dataLayer) {
                                window.dataLayer.push({'gtm_mobile': self.mobileNo()});
                                window.dataLayer.push({
                                    'event': 'GAEvent',
                                    'eventCategory': 'HEALTH',
                                    'eventAction': 'CallScheduled',
                                    'eventLabel': src.split('|').join('_'),
                                    'eventValue': 4
                                });
                            }

                            setTimeout(function() {
                                self.leadOverlay(false);
                                self.thanksMessage(true);
                                self.loading(false);
                                if(self.isBuyScreen()){
                                    results.overlayMode('thank_you');
                                    self.showLeadCard(false);
                                }
                                else{
                                    setTimeout(function(){
                                        self.showLeadCard(false);
                                    },3000);
                                }
                            }, 250);
                        },
                        error: function() {
                            setTimeout(function() {
                                self.loading(false);
                                setTimeout(function() {
                                    self.mobileNo.error('Error while submitting the call request');
                                }, 50);
                            }, 400);
                        }
                    });//INNER.AJAX
                }
                var user_data = utils.post_data();
                $.ajax({
                    url: '/health-plan/get-city-by-pincode/',
                    type: 'POST',
                    data: {'data':JSON.stringify(utils.post_data()), 'csrfmiddlewaretoken': CSRF_TOKEN},
                    success: function(res) {
                        res = JSON.parse(res);
                        user_data['user_city']=res['user_city'];
                        user_data['parents_city']=res['parents_city'];
                        post_to_lms({user_data: user_data});
                    },//OUTER.SUCCESS..
                    error: function(){
                        post_to_lms();
                    }
                });//OUTER.AJAX
            }
        };

        self.nCards = [1,2,3];
        self.adjustLeadCard = function() {
            if(results.UIChanged() && self.showLeadCard()){
                results.UIChanged(false);
                var plans_list = document.getElementById("plans-list");
                if(!plans_list) {
                    return false;
                }
                var plans = [];
                var allPlans = plans_list.getElementsByClassName("plan-card");
                for(var i=0; i<allPlans.length; i++) {
                    if(allPlans[i].offsetParent!==null)
                    {
                        plans.push(allPlans[i]);
                    }
                }
                var expCards = document.getElementsByClassName("expert-card");
                var cards = [];
                for(var i=0; i<expCards.length; i++) {
                    cards.push(expCards[i]);
                    cards[i].style.display = "none";
                }
                var ci = 0;
                if(plans.length < 5) {
                    plans_list.appendChild(cards[ci]);
                    cards[ci++].style.display = "";
                }
                else {
                    plans_list.insertBefore(cards[ci], plans[3]);
                    cards[ci++].style.display = "";
                    if(plans.length > 20) {
                        var mid_i = Math.floor((plans.length)/2);
                        plans_list.insertBefore(cards[ci], plans[mid_i]);
                        cards[ci++].style.display = "";
                    }
                    if(plans.length > 9) {
                        plans_list.appendChild(cards[ci]);
                        cards[ci++].style.display = "";
                    }
                }
            }
        };
        results.UIChanged.subscribe(self.adjustLeadCard);
        self.isVisible = function() {
            return self.leadOverlay() || self.thanksMessage();
        };
        self.hide = function() {
            self.leadOverlay(false);
            self.thanksMessage(false);
        }
    }

    return new stepLeadViewModel();
});
