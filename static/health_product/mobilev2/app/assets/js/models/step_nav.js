define(['knockout-router', 'knockout', 'jquery', 'step1', 'step2', 'step3', 'results', 'utils', 'knockout_validation'], function(router, ko, $, step1View, step2View, step3View, results, utils) {
    function stepNavViewModel() {
        var self = this;
        self.currentStep = ko.observable(1);
        self.manualGoBack = function(){
            if(self.currentStep() === 0){
                step1.gender(null);
            }
            else{
                self.currentStep(self.currentStep()-1);
                ko.router.navigate("step"+self.currentStep(),{trigger: false});
            }
        }
        self.nextStep = function(){
            if(self.currentStep() < 3) {
                self.set_stage(self.currentStep()+1);
                router.navigate("step"+self.currentStep(), {trigger: false});
            }
            else {
                self.showResults(self.currentStep());
            }
        };

        self.modifyDetails = function () {
            results.tipAlert(false);
            results.initialMode(true);
            results.currentTab(null);
            self.currentStep(1);
            router.navigate("step1");
            window.scrollTo(0,0);
        };
        /*self.closeInitialMode = function(){
            results.initialMode(false);
            self.currentStep(4);
            window.scrollTo(0,0);
        };*/
        var stage_VM = {1: step1View, 2: step2View, 3: step3View};
        self.validate_till = function(stage) {
            if(stage > 1) {
                step1View.members.valueHasMutated();
            }
            for(var s = 1; s < stage; ++s) {
                var s_VM = stage_VM[s];
                if(!s_VM.validate()) return s;
            }
            return stage;
        }

        self.set_stage = function (stage) {
            var valid_till = self.validate_till(stage);
            if(valid_till != stage)
                return router.navigate('step'+valid_till, {trigger: true});
            self.currentStep(stage);
            window.scrollTo(0,0);
            if(stage != 4) { // stage != results
                utils.page_view('/vp/health/step'+ self.currentStep()+ '/', 'VP-HEALTH-STEP' + self.currentStep());
                utils.gtm_event_log('vizury_step' + self.currentStep());
            }
        };
        self.resultRedirect = function() {
            if(stepLeadView.thanksMessage()){
                //Logged in and going to results page
                utils.event_log('Login', 'mobile', 0);
            } else {
                // Skipped login and going to results page
                utils.event_log('LoginSkipped', 'mobile', 0);
            }
            window.location.href = 'results/#' + utils.ACTIVITY_ID;
        };

        self.showResults = function(destIdx){
            var valid = true;
            //Validation on next
            if(destIdx == 3){
                valid = step3View.validate();
            }
            if(valid) {
                //self.currentStep(destIdx + 1);
                //window.scrollTo(0,0);
                var request = $.post('/health-plan/save-details/results/?hot=true', {'csrfmiddlewaretoken' : CSRF_TOKEN, 'data': JSON.stringify(utils.post_data())}, function(res){
                    var res = JSON.parse(res);
                    utils.ACTIVITY_ID = res.activity_id;
                    router.navigate(utils.ACTIVITY_ID, {trigger: true});
                });
            }
        };
    }
    return new stepNavViewModel();
});
