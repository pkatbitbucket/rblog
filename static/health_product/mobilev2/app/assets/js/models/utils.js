define(['knockout', 'step1', 'step2', 'step3'], function(ko, step1, step2, step3){
    var post_data = function() {
        var members = ko.utils.arrayMap(ko.unwrap(step1.getSelectedMembers()), function(m){
            return {
                id: m.id,
                person: m.person,
                type: m.type,
                displayName: ko.unwrap(m.displayName),
                age: ko.unwrap(m.age),
                age_in_months: ko.unwrap(m.age_in_months),
            };
        });
        return {
            product_type: ko.unwrap(step3.product_type),
            members: members,
            gender: ko.unwrap(step1.gender),
            deductible: ko.unwrap(step3.deductible),
            pincode: ko.unwrap(step3.userPincode),
            parents_pincode: ko.unwrap(step3.parentsPincode),
        };
    };

    var post_buy_data = function(plan) {
        var members = ko.toJS(plan.uidata.members);
        ko.utils.arrayForEach(members, function(m){
            delete m.ageList;
            delete m.calculate_age;
        });
        return {
            members: members,
            gender: plan.uidata.gender,
            hospital_service: plan.uidata.hospital_service,
            pincode: plan.uidata.pincode,
            city : plan.uidata.city
        };
    };


    var get_member = function(members, person) {
        return ko.utils.arrayFilter(members, function(m) {
            return ko.toJS(m.selected) && m.person.toLowerCase() == person.toLowerCase();
        });
    };

    var member_verbose = function(members, show_age) {
        var sage = show_age || false;
        var you = ko.toJS(get_member(members, 'you'));
        var spouse = ko.toJS(get_member(members,'spouse'));
        var father = ko.toJS(get_member(members,'father'));
        var mother = ko.toJS(get_member(members,'mother'));
        var sons = ko.toJS(get_member(members,'son'));
        var daughters = ko.toJS(get_member(members,'daughter'));

        var status = [];
        var cstatus = [];
        if(you.length > 0){
            status.push('You');
            cstatus.push('You:'+you[0].age);
        }
        if(spouse.length > 0){
            status.push('Your Spouse');
            cstatus.push('Spouse:'+spouse[0].age);
        }
        if(father.length > 0 && mother.length > 0){
            status.push('Your Parents');
            cstatus.push('Father:'+father[0].age);
            cstatus.push('Mother:'+mother[0].age);
        }else if(father.length > 0){
            status.push('Your Father');
            cstatus.push('Father:'+father[0].age);
        }else if(mother.length > 0){
            status.push('Your Mother');
            cstatus.push('Mother:'+mother[0].age);
        }
        if(sons.length > 0){
            if(daughters.length == 0) {
                if(sons.length == 1){
                    status.push('Your Son');
                }else {
                    status.push('Your ' + sons.length + ' Sons');
                }
            }
            var sages = [];
            ko.utils.arrayForEach(sons, function(s){
                sages.push(s.age+"y "+s.age_in_months+"m");
            });
            cstatus.push('Sons: [ '+sages.join(' | ')+' ]');
        }
        if(daughters.length > 0){
            if(sons.length == 0) {
                if(daughters.length == 1){
                    status.push('Your Daughter');
                }else {
                    status.push('Your ' + daughters.length + ' daughters');
                }
            }
            var dages = [];
            ko.utils.arrayForEach(daughters, function(d){
                dages.push(d.age+"y "+d.age_in_months+"m");
            });
            cstatus.push('Daughter: [ '+dages.join(' | ')+' ]');
        }
        if(daughters.length > 0 && sons.length > 0){
            status.push('Your ' + (daughters.length+sons.length) + ' kids');
        }
        if(show_age) {
            return cstatus.join(", ");
        }

        var verbose = "";
        if(status.length==1){
            verbose = status[0];
        }else{
            verbose = status.slice(0, status.length-1).join(", ");
            verbose += " And " + status[status.length-1];
        }

        return verbose.toLowerCase();
    };
    function deepCompare (x, y) {
        var i, l, leftChain, rightChain;

        function compare2Objects (x, y) {
            var p;

            // remember that NaN === NaN returns false
            // and isNaN(undefined) returns true
            if (isNaN(x) && isNaN(y) && typeof x === 'number' && typeof y === 'number') {
                return true;
            }

            // Compare primitives and functions
            // Check if both arguments link to the same object.
            // Especially useful on step when comparing prototypes
            if (x === y) {
                return true;
            }

            // Works in case when functions are created in constructor.
            // Comparing dates is a common scenario. Another built-ins?
            // We can even handle functions passed across iframes
            if ((typeof x === 'function' && typeof y === 'function') ||
                (x instanceof Date && y instanceof Date) ||
                    (x instanceof RegExp && y instanceof RegExp) ||
                        (x instanceof String && y instanceof String) ||
                            (x instanceof Number && y instanceof Number)) {
                return x.toString() === y.toString();
            }

            // At last checking prototypes as good a we can
            if (!(x instanceof Object && y instanceof Object)) {
                return false;
            }

            if (x.isPrototypeOf(y) || y.isPrototypeOf(x)) {
                return false;
            }

            if (x.constructor !== y.constructor) {
                return false;
            }

            if (x.prototype !== y.prototype) {
                return false;
            }

            // Check for infinitive linking loops
            if (leftChain.indexOf(x) > -1 || rightChain.indexOf(y) > -1) {
                return false;
            }

            // Quick checking of one object beeing a subset of another.
            // todo: cache the structure of arguments[0] for performance
            for (p in y) {
                if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
                    return false;
                }
                else if (typeof y[p] !== typeof x[p]) {
                    return false;
                }
            }

            for (p in x) {
                if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
                    return false;
                }
                else if (typeof y[p] !== typeof x[p]) {
                    return false;
                }

                switch (typeof (x[p])) {
                    case 'object':
                        case 'function':

                        leftChain.push(x);
                    rightChain.push(y);

                    if (!compare2Objects (x[p], y[p])) {
                        return false;
                    }

                    leftChain.pop();
                    rightChain.pop();
                    break;

                    default:
                        if (x[p] !== y[p]) {
                        return false;
                    }
                    break;
                }
            }

            return true;
        }

        if (arguments.length < 1) {
            return true; //Die silently? Don't know how to handle such case, please help...
            // throw "Need two or more arguments to compare";
        }

        for (i = 1, l = arguments.length; i < l; i++) {

            leftChain = []; //Todo: this can be cached
            rightChain = [];

            if (!compare2Objects(arguments[0], arguments[i])) {
                return false;
            }
        }

        return true;
    }
    var ACTIVITY_ID;

    var event_log = function(action, label, value) {
        if(window.dataLayer) {
            window.dataLayer.push({
                'event': 'GAEvent',
                'eventCategory': 'HEALTH',
                'eventAction': action,
                'eventLabel': label,
                'eventValue': value
            });
        }
    };
    var gtm_event_log = function(event_name) {
        if(window.dataLayer) {
            window.dataLayer.push({
                'event': event_name
            });
        }
    };
    var gtm_data_log = function(data){
        if(window.dataLayer) {
            window.dataLayer.push(data);
        }
    };
    var page_view = function(page, title) {
        if(window.dataLayer) {
            window.dataLayer.push({
                'event': 'virtual_page_view',
                'virtual_page_path': page,
                'virtual_page_title': title
            });
        }
    };
    var load_time = function(action, label, load_time, source){
        if(window.dataLayer){
            window.dataLayer.push({
                'event': 'ga_timer',
                'timingCategory': source,
                'timingVar': action,
                'timingLabel': label,
                'timingValue': load_time
            });
        }
    };

    return {
        deepCompare: deepCompare,
        post_data: post_data,
        post_buy_data: post_buy_data,
        ACTIVITY_ID : ACTIVITY_ID,
        event_log: event_log,
        gtm_event_log: gtm_event_log,
        gtm_data_log: gtm_data_log,
        page_view: page_view,
        load_time: load_time,
        member_verbose : member_verbose,
    };
});
