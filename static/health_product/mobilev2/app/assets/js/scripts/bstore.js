define(['knockout', 'cookie'], function (ko, Cookie) {
    //Function to check for "", null, undefined, [], {}
    var hasOwnProperty = Object.prototype.hasOwnProperty;

    function isEmpty(obj) {

        // null and undefined are "empty"
        if (obj == null) return true;

        // Assume if it has a length property with a non-zero value
        // that that property is correct.
        if (obj.length > 0)    return false;
        if (obj.length === 0)  return true;

        // Otherwise, does it have any properties of its own?
        // Note that this doesn't handle
        // toString and valueOf enumeration bugs in IE < 9
        for (var key in obj) {
            if (hasOwnProperty.call(obj, key)) return false;
        }

        return true;
    }

    ko.extenders.store = function (target, settings) {
        var init = Cookie.get(settings.key);
        if(init) {
            try {
                var jinit = JSON.parse(init);
            } catch (e) {
                var jinit = init;
            }
            if(settings.init) {
                settings.init(target, jinit);
            } else {
                target(jinit);
            }
        }
        target.subscribe(function(newValue) {
            if(settings.dump){
                var jval = settings.dump(newValue);
            } else {
                try {
                    var jval = ko.toJSON(newValue);
                } catch (e) {
                    var jval = newValue;
                }
            }
            Cookie.set(settings.key, jval);
        });
    };
});
