define(function() {
    var zlib = {};
/** @license zlib.js 2012 - imaya [ https://github.com/imaya/zlib.js ] The MIT License */
(function() {'use strict';var m=void 0,p=!0,s=this;function t(a,d){var b=a.split("."),c=s;!(b[0]in c)&&c.execScript&&c.execScript("var "+b[0]);for(var e;b.length&&(e=b.shift());)!b.length&&d!==m?c[e]=d:c=c[e]?c[e]:c[e]={}};var u="undefined"!==typeof Uint8Array&&"undefined"!==typeof Uint16Array&&"undefined"!==typeof Uint32Array&&"undefined"!==typeof DataView;function v(a){var d=a.length,b=0,c=Number.POSITIVE_INFINITY,e,g,f,l,h,k,n,q,r,w;for(q=0;q<d;++q)a[q]>b&&(b=a[q]),a[q]<c&&(c=a[q]);e=1<<b;g=new (u?Uint32Array:Array)(e);f=1;l=0;for(h=2;f<=b;){for(q=0;q<d;++q)if(a[q]===f){k=0;n=l;for(r=0;r<f;++r)k=k<<1|n&1,n>>=1;w=f<<16|q;for(r=k;r<e;r+=h)g[r]=w;++l}++f;l<<=1;h<<=1}return[g,b,c]};function y(a,d,b){this.w=[];this.t=b?b:32768;this.z=0;this.a=d===m?0:d;this.d=this.e=0;this.input=u?new Uint8Array(a):a;this.c=new (u?Uint8Array:Array)(this.t);this.b=0;this.v=this.l=!1;this.g=0;this.status=A}var A=0;
y.prototype.i=function(a,d){var b=!1;a!==m&&(this.input=a);d!==m&&(this.a=d);for(;!b;)switch(this.status){case A:case 1:var c;var e=m;this.status=1;C(this);if(0>(e=D(this,3)))E(this),c=-1;else{e&1&&(this.l=p);e>>>=1;switch(e){case 0:this.h=0;break;case 1:this.h=1;break;case 2:this.h=2;break;default:throw Error("unknown BTYPE: "+e);}this.status=2;c=m}0>c&&(b=p);break;case 2:case 3:switch(this.h){case 0:var g;var f=m,l=m,h=this.input,k=this.a;this.status=3;if(k+4>=h.length)g=-1;else{f=h[k++]|h[k++]<<
8;l=h[k++]|h[k++]<<8;if(f===~l)throw Error("invalid uncompressed block header: length verify");this.d=this.e=0;this.a=k;this.m=f;this.status=4;g=m}0>g&&(b=p);break;case 1:this.status=3;this.k=F;this.n=G;this.status=4;break;case 2:0>I(this)&&(b=p)}break;case 4:case 5:switch(this.h){case 0:var n;a:{var q=this.input,r=this.a,w=this.c,B=this.b,H=this.m;for(this.status=5;H--;){B===w.length&&(w=J(this,{o:2}));if(r>=q.length){this.a=r;this.b=B;this.m=H+1;n=-1;break a}w[B++]=q[r++]}0>H&&(this.status=6);this.a=
r;this.b=B;n=0}0>n&&(b=p);break;case 1:case 2:0>K(this)&&(b=p)}break;case 6:this.l?b=p:this.status=A}var z,x=this.b;this.v?u?(z=new Uint8Array(x),z.set(this.c.subarray(this.g,x))):z=this.c.slice(this.g,x):z=u?this.c.subarray(this.g,x):this.c.slice(this.g,x);this.buffer=z;this.g=x;return this.buffer};
var L=[16,17,18,0,8,7,9,6,10,5,11,4,12,3,13,2,14,1,15],M=u?new Uint16Array(L):L,N=[3,4,5,6,7,8,9,10,11,13,15,17,19,23,27,31,35,43,51,59,67,83,99,115,131,163,195,227,258,258,258],aa=u?new Uint16Array(N):N,O=[0,0,0,0,0,0,0,0,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,0,0,0],P=u?new Uint8Array(O):O,Q=[1,2,3,4,5,7,9,13,17,25,33,49,65,97,129,193,257,385,513,769,1025,1537,2049,3073,4097,6145,8193,12289,16385,24577],ba=u?new Uint16Array(Q):Q,R=[0,0,0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,12,12,
13,13],S=u?new Uint8Array(R):R,T=new (u?Uint8Array:Array)(288),U,V;U=0;for(V=T.length;U<V;++U)T[U]=143>=U?8:255>=U?9:279>=U?7:8;var F=v(T),W=new (u?Uint8Array:Array)(30),X,Y;X=0;for(Y=W.length;X<Y;++X)W[X]=5;var G=v(W);function D(a,d){for(var b=a.e,c=a.d,e=a.input,g=a.a,f;c<d;){if(e.length<=g)return-1;f=e[g++];b|=f<<c;c+=8}f=b&(1<<d)-1;a.e=b>>>d;a.d=c-d;a.a=g;return f}
function Z(a,d){for(var b=a.e,c=a.d,e=a.input,g=a.a,f=d[0],l=d[1],h,k,n;c<l;){if(e.length<=g)return-1;h=e[g++];b|=h<<c;c+=8}k=f[b&(1<<l)-1];n=k>>>16;a.e=b>>n;a.d=c-n;a.a=g;return k&65535}function C(a){a.u=a.a;a.s=a.d;a.r=a.e}function E(a){a.a=a.u;a.d=a.s;a.e=a.r}
function I(a){function d(){function a(b,c,d){var e,f=this.p,g,h,k;for(h=0;h<b;){e=Z(this,c);if(0>e)throw Error("not enough input");switch(e){case 16:if(0>(k=D(this,2)))throw Error("not enough input");for(g=3+k;g--;)d[h++]=f;break;case 17:if(0>(k=D(this,3)))throw Error("not enough input");for(g=3+k;g--;)d[h++]=0;f=0;break;case 18:if(0>(k=D(this,7)))throw Error("not enough input");for(g=11+k;g--;)d[h++]=0;f=0;break;default:f=d[h++]=e}}this.p=f;return d}var d;for(k=0;k<e;++k){if(0>(d=D(this,3)))throw Error("not enough input");
g[M[k]]=d}f=v(g);l=new (u?Uint8Array:Array)(b);h=new (u?Uint8Array:Array)(c);this.p=0;this.k=v(a.call(this,b,f,l));this.n=v(a.call(this,c,f,h))}var b,c,e,g=new (u?Uint8Array:Array)(M.length),f,l,h,k=0;a.status=3;C(a);b=D(a,5)+257;c=D(a,5)+1;e=D(a,4)+4;if(0>b||0>c||0>e)return E(a),-1;try{d.call(a)}catch(n){return E(a),-1}a.status=4;return 0}
function K(a){var d=a.c,b=a.b,c,e,g,f,l=a.k,h=a.n,k=d.length,n;for(a.status=5;;){C(a);c=Z(a,l);if(0>c)return a.b=b,E(a),-1;if(256===c)break;if(256>c)b===k&&(d=J(a),k=d.length),d[b++]=c;else{e=c-257;f=aa[e];if(0<P[e]){n=D(a,P[e]);if(0>n)return a.b=b,E(a),-1;f+=n}c=Z(a,h);if(0>c)return a.b=b,E(a),-1;g=ba[c];if(0<S[c]){n=D(a,S[c]);if(0>n)return a.b=b,E(a),-1;g+=n}b+f>=k&&(d=J(a),k=d.length);for(;f--;)d[b]=d[b++-g];if(a.a===a.input.length)return a.b=b,-1}}for(;8<=a.d;)a.d-=8,a.a--;a.b=b;a.status=6}
function J(a,d){var b,c=a.input.length/a.a+1|0,e,g,f,l=a.input,h=a.c;d&&("number"===typeof d.o&&(c=d.o),"number"===typeof d.q&&(c+=d.q));2>c?(e=(l.length-a.a)/a.k[2],f=258*(e/2)|0,g=f<h.length?h.length+f:h.length<<1):g=h.length*c;u?(b=new Uint8Array(g),b.set(h)):b=h;a.c=b;return a.c}y.prototype.j=function(){return u?this.c.subarray(0,this.b):this.c.slice(0,this.b)};function $(a){this.input=a===m?new (u?Uint8Array:Array):a;this.a=0;this.f=new y(this.input,this.a);this.c=this.f.c}
$.prototype.i=function(a){var d;if(a!==m)if(u){var b=new Uint8Array(this.input.length+a.length);b.set(this.input,0);b.set(a,this.input.length);this.input=b}else this.input=this.input.concat(a);var c;if(c=this.method===m){var e;var g=this.a,f=this.input,l=f[g++],h=f[g++];if(l===m||h===m)e=-1;else{switch(l&15){case 8:this.method=8;break;default:throw Error("unsupported compression method");}if(0!==((l<<8)+h)%31)throw Error("invalid fcheck flag:"+((l<<8)+h)%31);if(h&32)throw Error("fdict flag is not supported");this.a=g;e=m}c=0>e}if(c)return new (u?Uint8Array:Array);d=this.f.i(this.input,this.a);0!==this.f.a&&(this.input=u?this.input.subarray(this.f.a):this.input.slice(this.f.a),this.a=0);return d};$.prototype.j=function(){return this.f.j()};t("Zlib.InflateStream",$);t("Zlib.InflateStream.prototype.decompress",$.prototype.i);t("Zlib.InflateStream.prototype.getBytes",$.prototype.j);}).call(zlib);
//@ sourceMappingURL=inflate_stream.min.js.map
    var parseResponse = function(data) {
        char_codes = [];
        for(var i = 0; i < data.length - 3; i += 3)
            char_codes.push(parseInt(data.substr(i, 3)));
        data = char_codes;
        if(Uint8Array) {
            data = new Uint8Array(data);
        }
        var decompressed_data = (new zlib.Zlib.InflateStream(data)).decompress();
        var parsed_data = '';
        for(var i = 0; i < decompressed_data.length; ++i) {
            parsed_data += String.fromCharCode(decompressed_data[i]);
        }
        return JSON.parse(parsed_data);
    }
    return {
        'parseResponse': parseResponse
    };
});
