define(['knockout', 'knockout-router', 'knockout-amd-helpers', 'knockout-history'], function(ko) {

    function run() {
        var vm = {
            router: ko.router.vm,
            moduleToShow: ko.observable()
        };

        // Can attach a global subscription to be notified for all route changes.
        function notify_change(fragment, query) {
            console.log("NOTIFY >>>>>", fragment, query, new Date());
        }

        // Can attach a 'route not found' handler, which will be passed a url fragment and query string.
        function notFoundHandler(fragment, query) {
            console.log("URL NOT FOUND >", fragment, query);
        }

        // Configure routing options before defining routes
        ko.router.configure({ hashPrefix: '#', debug: true, notify: notify_change, pushState: false, root: '/health-plan/'});

        // Configure module loader
        ko.bindingHandlers.module.baseDir = "assets/js/models";
        ko.amdTemplateEngine.defaultPath = "assets/js/templates";
        ko.amdTemplateEngine.defaultSuffix = ".tmpl.html";
        ko.amdTemplateEngine.defaultRequireTextPluginName = "text";

        // Define the routes before ko.applyBindings()
        ko.router.map([
            { route: '(:stage_type)(/:activity_id)(/:set_type)', name: 'index', module:'index', template: 'index'},
        ]).mapNotFound({ callback: notFoundHandler }); // can specify a module/template/callback/title

        // Bind the view model
        ko.applyBindings(vm);

        // Should call ko.router.init() after ko.applyBindings() when using
        // the route bindingHandler. otherwise could safely call ko.router.init()
        // once the routes have been defined.
        ko.router.init(); //calls ko.history.start() behind the scenes
    }

    return {
        run: run
    };

});
