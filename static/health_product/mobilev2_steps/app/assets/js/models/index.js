define(['knockout-router', 'step_nav', 'step1', 'step2', 'step3', 'step_lead', 'login'], function(router, stepNav, step1, step2, step3, stepLead, login) {
    return {
        initialize : function(stage_type, activity_id, set_type) {
            var stage;
            if(stage_type == 'results') {
                window.location.href = "results/#" + activity_id;
            }
            else if(!stage_type){
                stage = 1;
            }
            else {
                stage = parseInt(stage_type.replace( /^\D+/g, ''));
                if(!(stage >=1 && stage <=3)) {
                    stage = 1;
                }
            }
            if(stage == 2 && stepLead.isVisible()) {
                //if user pushes back-button from lead page, redirect to results
                // stepNav.currentStep(3);
                router.navigate("step3");
                stepLead.hide();
                stepNav.resultRedirect();
            }
            else {
                stepNav.set_stage(stage);
            }
            return {
                'stepNav': stepNav,
                'step1' : step1,
                'step2' : step2,
                'step3' : step3,
                'stepLead' : stepLead,
                'login' : login
            };
        },
    };
});
