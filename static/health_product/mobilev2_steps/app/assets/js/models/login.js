define(['knockout-router', 'knockout', 'knockout_validation'], function(router, ko) {
    ko.validation.configure({
        errorMessageClass: 'error',
        decorateElement : true,
        errorElementClass: 'has-error',
        insertMessages: false
    });
    function loginViewModel() {
        var self = this;
        self.existingUser = ko.observable(true);
        self.existingUserEmail = ko.observable().extend({ required:true ,email :true });
        self.existingUserPassword = ko.observable();
        self.newUserEmail = ko.observable().extend({ required:true ,email :true });
        self.newUserPassword = ko.observable();
        self.setUser = function(status){
            self.existingUser(status);
        };
        self.existingUserFieldErrors = ko.validation.group([self.existingUserEmail]);
        self.newUserFieldErrors = ko.validation.group([self.newUserEmail]);
        self.existingUserValidate = function(){
            valid = true;
            if(!self.existingUserFieldErrors().length == 0) {
                self.existingUserFieldErrors.showAllMessages();
                valid = false;
            }
            return valid;
        };
        self.newUserValidate = function(){
            valid = true;
            if(!self.newUserFieldErrors().length == 0) {
                self.newUserFieldErrors.showAllMessages();
                valid = false;
            }
            return valid;
        };
    }
    return new loginViewModel();
});
