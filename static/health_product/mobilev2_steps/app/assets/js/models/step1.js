define(['knockout-router', 'knockout', 'knockout_validation'], function(router, ko) {

    ko.validation.configure({
        errorMessageClass: 'error',
        decorateElement : true,
        errorElementClass: 'has-error',
        insertMessages: false
    });

    function ageRange(a, b, step) {
        var A = [];
        step = step || 1;
        while(a<=b){
            A[A.length] = a;
            a+= step;
        }
        return A;
    }
    function Member(type, selected, age, person, ageList, displayName) {
        var self = this;
        self.selected = ko.observable(selected);
        self.person = person;
        self.type = type;
        self.id = self.person.toLowerCase();
        self.age = ko.observable(age).extend({
            validation:{
                'validator': function(n){
                    if( isNaN(n)){
                        return false;
                    } else {
                        return true;
                    }
                },
                'message' : 'Please select age of insured member'
            }
        });
        self.age_in_months = ko.observable(1).extend({
            validation: {
                'validator': function(val) {
                    return !isNaN;
                },
                'message' : 'Please select age of insured member'
            }
        });
        if(type == 'kid') {
            self.age_full = ko.computed(function() {
                return self.age() == 0 ? self.age_in_months() + ' months' : self.age + ' years';
            });
        };
        self.error = ko.observable("");
        if(!displayName){
            self.displayName = person;
        } else {
            self.displayName = displayName;
        }
        if(ageList == null){
            self.ageList = ageRange(0, 25, 1);
        } else {
            self.ageList = ageList;
        }
    };

    function stepOneViewModel() {
        var self = this;

        self.toddlerAgeList = ageRange(0, 11, 1);
        self.gender = ko.observable().extend({
            store : {
                key : 'gender'
            },
            required: {
                params:true,
                message: "Please select your gender"
            }
        });
        self.you = new Member('adult', false, undefined, 'You', ageRange(18, 100, 1), 'You');
        self.spouse = new Member('adult', false, undefined, 'Spouse', ageRange(18, 100, 1), ko.computed(function(){
            if (self.gender() == 'MALE'){
                return 'Wife';
            }
            else if (self.gender() == 'FEMALE') {
                return 'Husband';
            } else {
                return 'Spouse';
            }
        }));
        self.father = new Member('adult', false, undefined, 'Father', ageRange(18, 100, 1));
        self.mother = new Member('adult', false, undefined, 'Mother', ageRange(18, 100, 1));
        var initial_members = [self.you, self.spouse, self.father, self.mother];
        self.members = ko.observableArray(initial_members).extend({
            store : {
                key : 'members',
                init: function(obj, value) {
                    ko.utils.arrayForEach(value, function(v){
                        switch(v.person) {
                            case "You":
                                self.you.selected(v.selected);
                                self.you.age(parseInt(v.age));
                            break;
                            case "Spouse":
                                self.spouse.selected(v.selected);
                                self.spouse.age(parseInt(v.age));
                            break;
                            case "Father":
                                self.father.selected(v.selected);
                                self.father.age(parseInt(v.age));
                            break;
                            case "Mother":
                                self.mother.selected(v.selected);
                                self.mother.age(parseInt(v.age));
                            break;
                            default: //Kids
                                var m = new Member(v.type, v.selected, v.age, v.person, null, v.displayName);
                                m.age_in_months(parseInt(v.age_in_months));
                                obj.push(m);
                        }
                    });
                },
            },
            validation : [
                {
                    'validator' : function(obj){
                        if(ko.utils.arrayFilter(obj, function(m) { return m.selected() == true }).length == 0){
                            return false;
                        } else {
                            return true;
                        }
                    },
                    'message' : 'Select at least one member to insure'

                },{
                    'validator' : function(obj){
                        if(ko.utils.arrayFilter(obj, function(m) { return m.selected() == true && m.type == 'adult' }).length == 0){
                            return false;
                        } else {
                            return true;
                        }
                    },
                    'message' : 'Select at least one adult to insure'
                },{
                    'validator' : function(obj){
                        var kids = ko.utils.arrayFilter(obj, function(member) { return member.type == "kid" && member.selected() == true });
                        if(kids && kids.length>0){
                            if(self.you.selected() == true || self.spouse.selected() == true)
                                return true;
                            else return false

                        }
                        return true;
                    },
                    'message' : 'Please select a parent'
                }
            ]
        });

        self.selectMember = function(member){
            if (member.selected() == true) {
                if(member.type == 'kid')
                {
                    self.members.remove(member);
                    return member.selected(false);
                }
                else {
                    return member.selected(false);
                }
            }
            else {
                return member.selected(true);
            }
        };
        self.selectGender = function(gen) {
            self.gender(gen);
        }

        self.addKid = function(person) {
            var kid = new Member('kid', true, undefined, person, null, person);
            kid.id = kid.id + (self.getSelectedKids().length + 1);
            self.members.push(kid);
        };

        self.getSelectedKids = ko.computed(function(){
            return ko.utils.arrayFilter(self.members(), function(member) { return member.type == "kid" && member.selected() == true });
        });
        self.getSelectedAdults = ko.computed(function(){
            return ko.utils.arrayFilter(self.members(), function(member) { return member.type == "adult" && member.selected() == true });
        });
        self.getSelectedMembers = ko.computed(function(){
            return ko.utils.arrayFilter(self.members(), function(member) { return member.selected() == true });
        });

        self.cumulativeAgeError = ko.observable().extend({
            validation: [
                {
                validator: function(obj){
                    if(ko.utils.arrayFilter(self.getSelectedMembers(), function(m) { return isNaN(m.age()) }).length == 0){
                        return true;
                    } else {
                        return false;
                    }
                },
                'message':"Please select respective member's age"
            },{
                'validator' : function(obj) {
                    if(self.you.selected()){
                        if(self.father.selected()){
                            if((self.father.age() - self.you.age()) < 18){
                                return false;
                            }
                        }
                        if(self.mother.selected()){
                            if((self.mother.age() - self.you.age()) < 18){
                                return false;
                            }
                        }
                    }
                    return true;
                },
                'message' : "You parents should be at least 18 years older than you"
            } , {
                'validator' : function(obj) {
                    if(self.you.selected()){
                        if(self.getSelectedKids().length > 0){
                            var max_kid_age = Math.max.apply(null, self.getSelectedKids().map(function(k){return k.age();}));
                            if((self.you.age()  - max_kid_age) < 18){
                                return false;
                            }
                        }
                    }
                    return true;
                },
                'message' : "Your kids should be at least 18 years younger than you"
            } , {
                'validator' : function(obj) {
                    if(self.father.selected() || self.mother.selected()){
                        var selected_parents = ko.utils.arrayFilter(self.getSelectedAdults(), function(k){return (k == self.father || k == self.mother)});
                        var min_parent_age = Math.min.apply(null, selected_parents.map(function(p){return p.age();}));
                        if(self.getSelectedKids().length > 0){
                            var max_kid_age = Math.max.apply(null, self.getSelectedKids().map(function(k){return k.age();}));
                            if((min_parent_age  - max_kid_age) <= 36){
                                return false;
                            }
                        }
                    }
                    return true;
                },
                'message' : "There should be an age difference for at least 36 years between grandparents and kids"

            }
            ]
        });
        self.step1Errors = ko.validation.group([self.members,self.gender]);
        self.validate = function() {
            valid = true;
            if(!self.step1Errors().length == 0) {
                self.step1Errors.showAllMessages();
                valid = false;
            }
            return valid;
        };
    };

    var stepOneViewModelInstance = new stepOneViewModel();
    return stepOneViewModelInstance;
});
