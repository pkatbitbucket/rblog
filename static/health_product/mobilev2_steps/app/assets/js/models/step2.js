define(['knockout-router', 'knockout', 'step1', 'knockout_validation'], function(router, ko, step1View) {

    function stepTwoViewModel() {
        var self = this;
        self.insuranceFor = ko.computed(function(){
            var allLength = step1View.getSelectedAdults().length + step1View.getSelectedKids().length;
            if(allLength >= 2) {
                if(step1View.getSelectedKids().length ==0){
                    return step1View.getSelectedAdults().length +  ' adults' ;
                }
                else {

                    if(step1View.getSelectedAdults().length == 1 && step1View.getSelectedKids().length == 1) {
                        return step1View.getSelectedAdults().length +  ' adult and '+ step1View.getSelectedKids().length + ' kid' ;
                    }
                    else if(step1View.getSelectedAdults().length == 1 && step1View.getSelectedKids().length > 1) {
                        return step1View.getSelectedAdults().length +  ' adult and '+ step1View.getSelectedKids().length + ' kids' ;
                    }
                    else if(step1View.getSelectedAdults().length > 1 && step1View.getSelectedKids().length == 1) {
                        return step1View.getSelectedAdults().length +  ' adults and '+ step1View.getSelectedKids().length + ' kid' ;
                    }
                    else {
                        return step1View.getSelectedAdults().length +  ' adults and '+ step1View.getSelectedKids().length + ' kids' ;
                    }
                }
            }
            else if(step1View.getSelectedAdults().length == 1) {
                return ko.utils.unwrapObservable(step1View.getSelectedAdults()[0].person);
            }
        });

        self.validate = function() {
            var err_grp = step1View.getSelectedMembers().map(function(m){return m.age});
            err_grp.push(step1View.cumulativeAgeError);
            self.step2Errors = ko.validation.group(err_grp);
            valid = true;
            if(!self.step2Errors().length == 0) {
                self.step2Errors.showAllMessages();
                valid = false;
            }
            return valid;
        };
    };
    return new stepTwoViewModel();
});
