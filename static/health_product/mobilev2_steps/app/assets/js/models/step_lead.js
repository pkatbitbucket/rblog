define(['knockout', 'jquery', 'utils', 'cookie', 'knockout_validation'], function(ko, $, utils, Cookies) {
    function stepLeadViewModel() {
        var self = this;
        self.expert = ko.observable().extend({
                store: {key: 'expert'}
        });
        if(!self.expert()) {
            self.expert((function(){
                var experts = ["amit", "jack", "shweta", "ramitha", "kunal"];
                return experts[Math.floor(Math.random() * experts.length)];
            })());
        }
        self.expertImage = ko.observable('/static/health_product/mobilev2/app/assets/img/experts/'+self.expert()+'_150.jpg');
        self.leadOverlay = ko.observable(false);
        self.thanksMessage = ko.observable(false);
        self.mobileNo = ko.observable(Cookies.get('aml_mobile')).extend({
            required: {
                params: true,
                message: "Please enter your mobile number"
            },
            pattern: {
                params: '^([1-9])([0-9]){9}$',
                message: "Please enter a valid number"
            }

        });
        self.fetch_amlpl_mobile = function() {
            if(!self.mobileNo()) {
                $.get(
                    '/health-plan/amlpl/fetch-number/',
                    function(res) { if(res) { self.mobileNo(res); } }
                );
            }
        };

        self.loading = ko.observable(false);
        self.validate = function() {
            self.mobileNo.isModified(true);
            return self.mobileNo.isValid()
        };
        self.callMe = function(label) {
            if(self.validate()) {
                if(!confirm("I hereby authorize Coverfox to communicate with me on the given number for my Insurance needs. I am aware that this authorization will override my registry under NDNC.")){
                    return false;
                }
                if(self.mobileNo() == Cookies.get('aml_mobile')) {
                    label += "|prefilled";
                }
                var post_data = {
                    'campaign' : 'Health',
                    'mobile': self.mobileNo(),
                    'expert_name': self.expert(),
                    'activity_id': utils.ACTIVITY_ID,
                    'triggered_page': window.location.href
                };
                self.loading(true);
                var post_to_lms = function(extra_data){
                    extra_data = extra_data || {};
                        for(k in extra_data){
                            post_data[k] = extra_data[k];
                        }
                        $.ajax({
                            url: '/leads/save-call-time/',
                            type: 'POST',
                            data:{'data': JSON.stringify(post_data),'csrfmiddlewaretoken' : CSRF_TOKEN},
                            success: function(res) {
                                Cookies.set('mobileNo', self.mobileNo(), {expires:3600});
                                setTimeout(function() {
                                    self.leadOverlay(false);
                                    self.thanksMessage(true);
                                    self.loading(false);
                                }, 250);
                                var src = 'Mobile expert-page (pre-results)';
                                if(window.dataLayer) {
                                    window.dataLayer.push({'gtm_mobile': self.mobileNo()});
                                    window.dataLayer.push({
                                        'event': 'GAEvent',
                                        'eventCategory': 'HEALTH',
                                        'eventAction': 'CallScheduled',
                                        'eventLabel': src.split('|').join('_'),
                                        'eventValue': 4
                                    });
                                }
                            },
                            error: function() {
                                setTimeout(function() {
                                    self.loading(false);
                                    setTimeout(function() {
                                        self.mobileNo.error('Error while submitting the call request');
                                    }, 50);
                                }, 400);
                            }
                        });//INNER.AJAX
                };
                var user_data = utils.post_data();
                $.ajax({
                    url: '/health-plan/get-city-by-pincode/',
                    type: 'POST',
                    data: {'data':JSON.stringify(utils.post_data()), 'csrfmiddlewaretoken': CSRF_TOKEN},
                    success: function(res) {
                        Cookies.set('mobileNo', self.mobileNo());
                        setTimeout(function() {
                            self.leadOverlay(false);
                            self.thanksMessage(true);
                            self.loading(false);
                        }, 250);
                        var src = 'Mobile expert-page (pre-results)';
                        if(window.dataLayer) {
                            window.dataLayer.push({'gtm_mobile': self.mobileNo()});
                            window.dataLayer.push({
                                'event': 'GAEvent',
                                'eventCategory': 'HEALTH',
                                'eventAction': 'CallScheduled',
                                'eventLabel': src.split('|').join('_'),
                                'eventValue': 4
                            });
                        }
                    },
                    error: function() {
                        setTimeout(function() {
                            self.loading(false);
                            setTimeout(function() {
                                self.mobileNo.error('Error while submitting the call request');
                            }, 50);
                        }, 400);
                    }
                });//OUTER.AJAX
            }
        };

        self.isVisible = function() {
            return self.leadOverlay() || self.thanksMessage();
        };
        self.hide = function() {
            self.leadOverlay(false);
            self.thanksMessage(false);
        }
    }

    return new stepLeadViewModel();
});
