define(['knockout', 'step1', 'step2', 'step3'], function(ko, step1, step2, step3){
    var post_data = function() {
        var members = ko.utils.arrayMap(ko.unwrap(step1.getSelectedMembers()), function(m){
            return {
                id: m.id,
                person: m.person,
                type: m.type,
                displayName: ko.unwrap(m.displayName),
                age: ko.unwrap(m.age),
                age_in_months: ko.unwrap(m.age_in_months),
            };
        });
        return {
            product_type: ko.unwrap(step3.product_type),
            members: members,
            gender: ko.unwrap(step1.gender),
            deductible: ko.unwrap(step3.deductible),
            pincode: ko.unwrap(step3.userPincode),
            parents_pincode: ko.unwrap(step3.parentsPincode),
        };
    };
    var event_log = function(action, label, value) {
        if(window.dataLayer) {
            window.dataLayer.push({
                'event': 'GAEvent',
                'eventCategory': 'HEALTH',
                'eventAction': action,
                'eventLabel': label,
                'eventValue': value
            });
        }
    };
    var gtm_event_log = function(event_name) {
        if(window.dataLayer) {
            window.dataLayer.push({
                'event': event_name
            });
        }
    };
    var gtm_data_log = function(data){
        if(window.dataLayer) {
            window.dataLayer.push(data);
        }
    };
    var page_view = function(page, title) {
        if(window.dataLayer) {
            window.dataLayer.push({
                'event': 'virtual_page_view',
                'virtual_page_path': page,
                'virtual_page_title': title
            });
        }
    };
    var load_time = function(action, label, load_time, source){
        if(window.dataLayer){
            window.dataLayer.push({
                'event': 'ga_timer',
                'timingCategory': source,
                'timingVar': action,
                'timingLabel': label,
                'timingValue': load_time
            });
        }
    };
    var ACTIVITY_ID;

    return{
        post_data: post_data,
        event_log: event_log,
        gtm_event_log: gtm_event_log,
        gtm_data_log: gtm_data_log,
        page_view: page_view,
        load_time: load_time,
        ACTIVITY_ID : ACTIVITY_ID
    }
});
