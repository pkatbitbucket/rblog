function cfCarousel(id,time){
    var slideTime = time;
    var $carousel = $(id);
    var items = $carousel.find('.item');
    $(items[0]).addClass('active');
    function autoClicks(){
        var len = items.length-1;
        var toClick = true;
        var key = 0;
        $.each($(items),function(index, row){
            if ($(row).hasClass('active') && toClick){
                key = index>=len? 0 :index+1;
                $(row).removeClass('active');
                $(items[key]).trigger('click');
                toClick = false;
            }
        });
    }
    var slide = setInterval(autoClicks,slideTime);
    $carousel.on("mouseover", function(){
        //stop the interval
        clearInterval(slide);
    });
    $carousel.on("mouseout", function(){
        //and when mouseout start it again
        slide = setInterval(autoClicks,slideTime);
    });

    $carousel.children('.item').on("click",function (e){
        $carousel.children('.item').removeClass('active');
        $(this).addClass('active');
        e.stopPropagation();
        e.preventDefault();
    });
    return {
        'slide': slide,
        'carousel': $carousel
    }
}