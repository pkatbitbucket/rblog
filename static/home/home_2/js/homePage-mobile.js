var ContactForm_mid = {
    policyType:"",
    mobileNo:"",
    preferredTime:"",
    preferredTimeValue:"",
    day:""
};

// Validate Contact Me Form. Both top and middle.
// pos value: top for top, mid for middle
function validateForm(pos){
    var mobileNo, policyType, preferredTime;
    mobileNo = ContactForm_mid.mobileNo;
    preferredTime = ContactForm_mid.preferredTime;
    policyType = ContactForm_mid.policyType;

    if(validation.isMobileNumber(mobileNo) && (preferredTime != "prefered" && preferredTime.length>1) && policyType.length>1){
        $("#btnPreferredTime").prop("disabled",false);
    }
    else{
        $("#btnPreferredTime").prop("disabled","disabled");
    }
}

// Format data and send data to LMS
function createDataForLMS(pos){
    var mobileNo, policyType, preferredTime, preferredTimeValue;
    mobileNo = ContactForm_mid.mobileNo;
    preferredTime = ContactForm_mid.preferredTime;
    policyType = ContactForm_mid.policyType;
    preferredTime = ContactForm_mid.preferredTime;
    preferredTimeValue = ContactForm_mid.preferredTimeValue;
    day = ContactForm_mid.day;


    var campaignType = "";
    var label = "homepage_contactMe_mobile";

    switch(policyType.trim()){
        case "Two": campaignType =  "motor-bike-homepage"; break;
        case "Car": campaignType =  "motor-homepage"; break;
        case "Health": campaignType =  "health-homepage"; break;
        case "Home": campaignType =  "home-homepage"; break;
        case "Travel": campaignType =  "travel-homepage"; break;
    }

    var d = new Date();
    // Add day
    var call_time = (day=="Today"?d.getDate():d.getDate()+1) + "/" + (d.getMonth()+1) + "/" + d.getFullYear();
    // Add time

    var _ptv = preferredTimeValue.substring(0, preferredTimeValue.length-2)

    if(_ptv == '9:30'){
        _ptv = '9:30:00'
    }
    if(_ptv == '12'){
        _ptv = '12:00:00'
    }
    if(_ptv == '3'){
        _ptv = '15:00:00'
    }
    if(_ptv == '5'){
        _ptv = '17:00:00'
    }

    call_time +=" " + _ptv;

    var _lms_data = LMS_DefaultData;
    _lms_data.mobile = mobileNo;
    _lms_data.campaign = campaignType;
    _lms_data.preferred_call_time = call_time;
    _lms_data.label = label;
    _lms_data.triggered_page = window.location.href;

    $.post("/leads/save-call-time/",
        {'data' : JSON.stringify(_lms_data), 'csrfmiddlewaretoken' : CSRF_TOKEN}, function(data){
        if (data.success){
            utils.gtm_push(
                "HomeContactMe",
                "HomePage - Get Quotes",
                "",
                label,
                {
                    "param7": campaignType,
                    "param8": day + " " + preferredTime
                })
        }
    }, 'json');
    console.log(_lms_data)
}

// Used to add Today/ Tomorrow in preferred time select
function setPreferredTimeDayLabel(parent){
    var li_top = $(parent).find("li");
    var current_time = new Date().getHours();
    var tomorrow=[], today = [];

    for(var i = 0;  i<li_top.length; i++){
        var element = $(parent).find("li")[i];
        // fetch Time string 9:30am
        var time = $(element).find("small").text().split(" ")[0].substring(1);
        var am_pm = 0;
        if(time.indexOf("pm")>=1 && time != "12pm"){
            am_pm = 12;
        }
        // remove am/pm
        time = time.substring(0, time.length-2);
        // replace : with .
        time = time.replace(":",".")
        var str = $(element).find("span").text()

        var day = "";
        if(parseInt(time)+ parseInt(am_pm)>parseInt(current_time)){
            day = "Today "
        }
        else{
            day ="Tomorrow ";
        }
        $(element).find("span").text(day+str);

        if(day == "Today ")
            today.push($(element).html())
        else tomorrow.push($(element).html())
    }
    var str = "";
    today.forEach(function(row, index){
        str+="<li>" + row + "</li>";
    })
    tomorrow.forEach(function(row, index){
        str+="<li>" + row + "</li>";
    });

    $(parent).find("ul").html(str)
}
//------------------------------------------------------------------------

function registerEvents(){
    //  To validate if entered number is valid
    $('#txtCMobileNo').on("blur", function(){
        ContactForm_mid.mobileNo =  $(this).val();
        validateForm();
    });

    $("#cbPolicyType li").on("click",function(e){
        ContactForm_mid.policyType =  $(this).text().trim().split(" ")[0];
        validateForm();
    })

    $("#cbPreferredTime li").on("click", function(){
        ContactForm_mid.day = $(this).text().trim().split(" ")[0];
        ContactForm_mid.preferredTime =  $(this).text().trim().split(" ")[1];
        ContactForm_mid.preferredTimeValue = $(this).find("small").text().split(" ")[0].substring(1);
        validateForm();
    });

    $("#cbInsuranceType li").on("click",function(){
        $(this).parents(".form-row").find(".btn").prop("disabled",false);
    });

    $(document).on("click", function(e) {
        e.stopPropagation();
        // for custom select input field
        if ($(e.target).closest('.select-input').length==0) {
            if(!$(e.target).hasClass('active')) {
                $(".select-input.active").removeClass("active").find('ul').slideToggle(300);
            }
        }
    });

    $("#btnInsuranceType").on("click",function(){
        var type = $("#divInsuranceType").text();
        var url="";
        $.each(linkList,function(index, row){
            if(row.name +" Insurance" == type){
                url = row.link;
                return;
            }
        });
        window.location.href = url;
    });

    $("#btnPreferredTime").on("click", function(){
        $("#divContactForm").slideUp(500);
        $("#divThankYou").slideDown(500);
        createDataForLMS("");

        utils.gtm_push("HomeContactMe",
            "HomePage - Contact Me",
            "Contact",
            $("#divSlot").text(), {});

    });
}

(function initForm(){

        // 2 of 3 video flipper
        var vids = $("div.flip-item").get().sort(function(){
            return Math.round(Math.random())-0.5; //random so we get the right +/ combo
       }).slice(0,2)
        $(vids).show();

        function cycleImage(){
            var $active = $('#festive-banner img.active');
            var $next = ($active.next().length > 0) ? $active.next():
            $('#festive-banner :first');
            $active.removeClass('active');
            $next.addClass('active');
        }
        $(document).ready(function(){
            var randomElements = $('#festive-banner img').get().sort(function(){
                return Math.round(Math.random()) - 0.5;
            }).slice(0, 1);
            $(randomElements).addClass('active');
            setInterval(cycleImage, 1000);
        });

    setPreferredTimeDayLabel("#cbPreferredTime");
    setTimeout(function () {
        utils.createDropDowns('#cbPreferredTime','#cbPreferredTime ul li');
    }, 0)

    utils.createDropDowns('#cbInsuranceType','#cbInsuranceType ul li');
    utils.createDropDowns('#cbPolicyType','#cbPolicyType ul li');
    utils.createMobileField("#txtCMobileNo");
    $("#blog-carousel").owlCarousel({
        itemsMobile: [479,1],
        slideSpeed: 200,
        autoPlay: true
    });
    $("#emp-carousel").owlCarousel({
        itemsMobile: [479,1],
        slideSpeed: 200,
        autoPlay: true
    });
    registerEvents();

    //cfCarousel('#emp-carousel',5000);
    // health tax benefit modal logic
    $("#hb-savetax-open").on('click',function(e){
        e.stopPropagation();
        e.preventDefault();
        $("body").addClass("overflow-none");
        $("#hb-savetax").addClass("active");
    });

})();
/******************** YouTube Section *************************/
(function youTubeEvents(){
    var video_track = '';//setInterval(trackVideoStatus(),1000);
    var threshold = 0.25;
    var state = "";

    // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.
    var player;
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
            videoId: '7VxoVVdzkE8',
            playerVars:{rel:0},
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });
    }

    var youtube_Init = setInterval(function(){
        if(window.YT){
            onYouTubeIframeAPIReady();
            clearInterval(youtube_Init)
        }

    },1000);

    // 4. The API will call this function when the video player is ready.
    function onPlayerReady(event) {
        $("#player").fadeIn(300);
    }

    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.
    var done = false;
    function onPlayerStateChange(event) {
        var scroll = "";
        var winScroll = $(window).scrollTop();
            scroll = winScroll>45? "Scrolled ":"";
        var playerState = player.getPlayerState(),
            title = player.getVideoData().title;
        var video_duration = player.getDuration();

        switch(playerState){
            case -1:
                state = "Unstarted";
                break;
            case 0:
                state="Ended";
                clearInterval(video_track)
                break;
            case 1:
                state = "Playing";
                video_track = setInterval(function(){trackVideoStatus()},1000);
                break;
            case 2:
                state = "Paused";
                clearInterval(video_track)
                break;
            case 3:
                state = "Buffering";
                break;
            case 5:
                state = "Video cued";
                clearInterval(video_track)
                break;
        }

        if(playerState==-1 || playerState == 0){
            utils.gtm_push(
                "HomepageTopVideo",
                "Home Page",
                "Top Video " + scroll + state,
                title,
                {}
            )
        }
    }

    function trackVideoStatus(event){
        var scroll = "";
        var winScroll = $(window).scrollTop();
            scroll = winScroll>45? "Scrolled ":"";
        var video_duration = player.getDuration(),
            currentTime = player.getCurrentTime(),
            title = player.getVideoData().title;

        if (video_duration * threshold < currentTime){
            utils.gtm_push(
                "HomepageTopVideo",
                "Home Page",
                "Top Video " + scroll + state,
                title + ' - Viewed ' + threshold*100 + '%' ,
                {}
            );
            threshold += 0.25;
        }
    };


    function stopVideo() {
        player.stopVideo();
    }
})();