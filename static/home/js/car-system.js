/********* private variables *********/
var VEHICLE_TYPE_CAR = "fourwheeler";
var oldCarYears = utils.getPastYears();

var fuel_types =
    [{'code': 'petrol', 'id': 'PETROL','name': 'Petrol'},
     {'code': 'diesel', 'id': 'DIESEL','name': 'Diesel'},
     {'code': 'cng_company', 'id': 'INTERNAL_LPG_CNG','name': 'CNG/LPG Company Fitted'},
     {'code': 'cng_external', 'id': 'PETROL','name': 'CNG/LPG Externally Fitted'}];

var selected_model_id_car,
    selected_fuel_type_id_car = 'PETROL',
    expiredDataSet = utils.getExpiredDateValue(),
    moreThan90Days = false;

/********* private functions *********/
var setUpCarForm = function() {
    var policy_type_value = $('[name=car_policy_type]:checked').val();
    if (policy_type_value == 'new') {
        $('.car-product .new-policy').removeClass('hide');
        $('.car-product .renew-policy').addClass('hide');
        $('.car-product .common-fields').removeClass('hide');
        $('.car-product .expired-policy').addClass('hide');
        $('.car-product .rto-field').addClass('col-md-offset-3');
    } else if (policy_type_value == 'renew' || policy_type_value == 'expired') {
        $('.car-product .new-policy').addClass('hide');
        $('.car-product .renew-policy').removeClass('hide');
        $('.car-product .common-fields').removeClass('hide');
        $('.car-product .expired-policy').addClass('hide');
        $('.car-product .rto-field').removeClass('col-md-offset-3');
    }
    if(policy_type_value == 'expired'){
        $("#expired_car").removeClass("hide");
    }
    else{
        $("#expired_car").addClass("hide");
    }

    populateMobileNumber();
    populateExpiryDate();
};

function populateMobileNumber(){
    var mobNo = Cookies.get("mobileNo");
    if(mobNo && mobNo.length>0){
        var event = $.Event("keydown");
            event.which = 39;
        $("#txtMobileNumber").val(mobNo).trigger(event);
        $("#txtMobileNumber").parents(".cf-special-ip").addClass("selected");
    }
}

function populateExpiryDate(str){
    var year = $("#car_expiry_year :selected").attr("id");
    var month= $("#car_expiry_month :selected").attr("id");
    var monthlen = new Date(year,parseInt(month) ,0).getDate();
    var dates = [];
    var years = [];

    for(var i =1; i<=31; i++)
        dates.push({"id":i,"name":i, "value":i})

    expiredDataSet.years.forEach(function(row){
        years.push({"id":row,"name":row, "value":row});
    });

    utils.createSelect("#car_expiry_date",dates);
    utils.createSelect("#car_expiry_month",expiredDataSet.month);
    utils.createSelect("#car_expiry_year",years);
}

function computeMoreThan90days(){
    if(localStorage[VEHICLE_TYPE_CAR]){
        var _local = JSON.parse(localStorage[VEHICLE_TYPE_CAR]);
        var _month = _local.expiry_month;
        if(_local.expiry_year && _local.expiry_month && _local.expiry_date){
            if(_local.expiry_month.toString().length>2){
                _month = utils.getMonthNameToNumber(_local.expiry_month);
            }

            $('#car_expiry_date').val(_local.expiry_date).trigger("change");
            $('#car_expiry_month').val(_month).trigger("change");
            $('#car_expiry_year').val(_local.expiry_year).trigger("change");

            var _exDate = new Date(_local.expiry_year,_month -1, _local.expiry_date);
            var t_90 = new Date();
            t_90.setDate(t_90.getDate() - 90);
            t_90.setHours(0,0,0,0);

            moreThan90Days = (+_exDate< +t_90);
            $("#moreThan90days_car").attr("checked", moreThan90Days);
            $("#moreThan90days_car").trigger("change");
        }
    }
}

function validateCarExpiredDateValue(){
    var exDate = $("#car_expiry_date option:selected").val();
    var exMonth = $("#car_expiry_month option:selected").val();
    var exYear = $("#car_expiry_year option:selected").val();

    if(exDate && exMonth && exYear){
        var _date = new Date(JSON.parse(exYear).value, JSON.parse(exMonth).id -1, JSON.parse(exDate).value);
        var today = new Date();

        _date.setHours(0,0,0,0);
        today.setHours(0,0,0,0);
        
        if(_date.getDate() != JSON.parse(exDate).value){
            $("#car_expiry_date_error").text("Please select a valid date.")
            $("#car_expiry_date_error").fadeIn();
            return false;
        }
        else if(+_date >= +today){
            $("#car_expiry_date_error").text("Policy expiry date should be past.")
            $("#car_expiry_date_error").fadeIn();
            return false;
        }
        else{
            $("#car_expiry_date_error").text("")
            $("#car_expiry_date_error").fadeOut();
            return true
        }
    }
    else if($("#moreThan90days_car").is(":checked")){
        return true;
    }
    else {
        $("#car_expiry_date_error").text("Please select a valid date.")
        $("#car_expiry_date_error").fadeIn();
        return false;
    }
}

function getVehicles_Car() {
    var url = '/motor/' + VEHICLE_TYPE_CAR + '/api/vehicles/';
    var data = { 'csrfmiddlewaretoken': CSRF_TOKEN }
    utils.getData(url,data,'#vehicle_make_model',"models");
}

function getCarVariants() {
    var select = $("#vehicle_variant");
    $(select).find('option').remove();
    if (selected_model_id_car && selected_fuel_type_id_car) {
        var url = '/motor/' + VEHICLE_TYPE_CAR + '/api/vehicles/' + selected_model_id_car + '/variants/';
        var data = {'csrfmiddlewaretoken': CSRF_TOKEN,'fuel_type': selected_fuel_type_id_car}
        utils.getData(url,data,"#vehicle_variant","variants");
    }
    else{
        utils.createSelect($('#vehicle_variant'),{})
    }
};

function getRTOs_Car() {
    var url = '/motor/' + VEHICLE_TYPE_CAR + '/api/rtos/';
    var data = {'csrfmiddlewaretoken': CSRF_TOKEN}
    utils.getData(url,data,"#vehicle_rto","rtos");
};

function setFuelTypes_Car() {
    utils.createSelect($('#vehicle_fuel_type'),fuel_types);
};

function setRegYears() {
    utils.createSelect($("#vehicle_reg_year"),oldCarYears);
};

function getVehicleDetails_Car(){
    var isCNG =0;
    if($('#vehicle_fuel_type').val())
        isCNG = $('#vehicle_fuel_type').val().indexOf('CNG')>0?1:0;
    return {
        policy_type: $('[name=car_policy_type]:checked').val(),
        expiry_slot: $('[name=expiry_slot]:checked').val(),
        vehicle_make_model: $('#vehicle_make_model').val(),
        vehicle_fuel_type: $('#vehicle_fuel_type').val(),
        vehicle_variant: $('#vehicle_variant').val(),
        vehicle_rto: $('#vehicle_rto :selected').val(),
        reg_year: $('#vehicle_reg_year :selected').text(),
        cngKitValue: $('#txtCNGKit').val()?$('#txtCNGKit').val():undefined,
        isCNGFitted: isCNG
    }
}

function setCarForm(){
    var car = {};
    var cngVal, modelVal, fuelVal, variantVal,rtoVal,yearVal, _expiryDate, _expiryMonth, _expiryYear;
    // Fetch data from LocalStorage
    if(localStorage && localStorage.getItem(VEHICLE_TYPE_CAR)){
        var _local = JSON.parse(localStorage.getItem(VEHICLE_TYPE_CAR));
        car = {
            expiry_slot: JSON.stringify(_local.expirySlot),
            expiry_date:JSON.stringify(_local.expiry_date),
            expiry_month:JSON.stringify(_local.expiry_month),
            expiry_year:JSON.stringify(_local.expiry_year),
            vehicle_fuel_type : JSON.stringify(_local.fuel_type),
            vehicle_variant : JSON.stringify(_local.vehicleVariant),
            vehicle_make_model : JSON.stringify(_local.vehicle),
            vehicle_rto : JSON.stringify(_local.rto_info),
            vehicle_reg_year: JSON.stringify(_local.reg_year),
            cngKitValue: JSON.stringify(_local.cngKitValue),
            isCNGFitted: JSON.stringify(_local.isCNGFitted)
        }

        if(_local.vehicle)
            selected_model_id_car = _local.vehicle.id;
        if(_local.fuel_type)
            selected_fuel_type_id_car = _local.fuel_type.id;
    }
    // Fetch data from Cookie
    else{
         car = {
            expiry_slot: Cookies.get(VEHICLE_TYPE_CAR+'_expiry_slot'),
            expiry_date:Cookies.get(VEHICLE_TYPE_CAR+'_expiryDate'),
            expiry_month:Cookies.get(VEHICLE_TYPE_CAR+'_expiryMonth'),
            expiry_year:Cookies.get(VEHICLE_TYPE_CAR+'_expiryYear'),
            vehicle_fuel_type : utils.validateJSON(Cookies.get(VEHICLE_TYPE_CAR + '_fuel_type')),
            vehicle_variant : utils.validateJSON(Cookies.get(VEHICLE_TYPE_CAR+ '_vehicleVariant')),
            vehicle_make_model : utils.validateJSON(Cookies.get(VEHICLE_TYPE_CAR+'_vehicle')),
            vehicle_rto : Cookies.get(VEHICLE_TYPE_CAR+'_rto_info'),
            vehicle_reg_year: Cookies.get(VEHICLE_TYPE_CAR+ '_reg_year'),
            cngKitValue: Cookies.get(VEHICLE_TYPE_CAR + '_cngKitValue'),
            isCNGFitted: Cookies.get(VEHICLE_TYPE_CAR + '_isCNGFitted')
        }
    }
    if(car.isCNG){
        $("#divCNGKit").fadeIn();
    }

    getCarVariants();
    if(car.cngKitValue){
        cngVal = car.cngKitValue;
    }
    else{
        cngVal = '';
        $("#txtCNGKit").removeClass('selected');
    }

    if(car.vehicle_make_model && car.vehicle_make_model.indexOf('{')>=0){
        modelVal = car.vehicle_make_model;
    }
    else{

        modelVal = $('#vehicle_make_model option:first').val();
        $("#vehicle_make_model").parent().removeClass('selected');
    }

    if(car.vehicle_fuel_type && car.vehicle_fuel_type.indexOf('{')>=0){
        fuelVal = car.vehicle_fuel_type;
    }
    else{
        fuelVal = $('#vehicle_fuel_type option:first').val()
        $("#vehicle_fuel_type").parent().removeClass('selected');
    }

    if(car.vehicle_variant && car.vehicle_variant.indexOf('{')>=0){
        variantVal = car.vehicle_variant;
    }
    else{
        variantVal = $('#vehicle_variant option:first').val()
        $("#vehicle_variant").parent().removeClass('selected');
    }
    if(car.vehicle_rto && car.vehicle_rto.indexOf('{')>=0){
        rtoVal = car.vehicle_rto;
    }
    else{
        rtoVal = $('#vehicle_rto option:first').val()
        $("#vehicle_rto").parent().removeClass('selected');
    }
    if(car.vehicle_reg_year){
        yearVal = '{"id":' + car.vehicle_reg_year + ',"name":'+ car.vehicle_reg_year + ',"value":'+ car.vehicle_reg_year + '}';
    }
    else{
        yearVal = $('#vehicle_reg_year option:first').val()
        $("#vehicle_reg_year").parent().removeClass('selected');
    }

    if(car.expiry_year){
        _expiryYear = car.expiry_year;
    }
    else{
        _expiryYear = $('#car_expiry_year option:first').val()
        $("#car_expiry_year").parent().removeClass('selected');
    }

    if(car.expiry_month){
        _expiryMonth = car.expiry_month;
    }
    else{
        _expiryMonth = $('#car_expiry_month option:first').val()
        $("#car_expiry_month").parent().removeClass('selected');
    }

    if(car.expiry_date){
        _expiryDate = car.expiry_date;
    }
    else{
        _expiryDate = $('#car_expiry_date option:first').val()
        $("#car_expiry_date").parent().removeClass('selected');
    }

    setTimeout(function(){
        $('#vehicle_make_model').select2().select2('val',modelVal);
        $('#vehicle_fuel_type').select2().select2('val',fuelVal);
        $('#vehicle_variant').select2().select2('val',variantVal);
        $('#vehicle_rto').select2().select2('val',rtoVal);
        $('#vehicle_reg_year').select2().select2('val',yearVal);
        $('#car_expiry_date').select2().select2("val",_expiryDate);
        $('#car_expiry_month').select2().select2("val",_expiryMonth);
        $('#car_expiry_year').select2().select2("val",_expiryYear);

        if(car.vehicle_fuel_type && car.vehicle_fuel_type.indexOf("external")>1){
            $("#txtCNGKit").val(cngVal);
            $("#divCNGKit").show();
        }
        utils.showLabelForTextField($("#txtCNGKit"));
        utils.showLabelForInput($("#vehicle_make_model"));
        utils.showLabelForInput($("#vehicle_fuel_type"));
        utils.showLabelForInput($("#vehicle_variant"));
        utils.showLabelForInput($("#vehicle_rto"));
        utils.showLabelForInput($("#vehicle_reg_year"));
        utils.showLabelForInput($('#car_expiry_date'));
        utils.showLabelForInput($('#car_expiry_month'));
        utils.showLabelForInput($('#car_expiry_year'));
        setTimeout(function(){$(".loading").removeClass("loading");},0)
        computeMoreThan90days();
    },500);
}

var submitCarForm = function() {

    var vehicle = getVehicleDetails_Car(),
        redirect_url = '/motor/car-insurance/#results';

    var data = {
        'isNewVehicle': (vehicle.policy_type == "new") ? '1' : '0',
        'vehicle': vehicle.vehicle_make_model,
        'vehicleVariant': vehicle.vehicle_variant,
        'fuel_type': vehicle.vehicle_fuel_type,
        'rto_info': vehicle.vehicle_rto,
        'cngKitValue': vehicle.cngKitValue,
        'isCNGFitted': vehicle.isCNGFitted
    };

    if (vehicle.policy_type == "renew" || vehicle.policy_type == "expired") {
        data['reg_year'] = vehicle.reg_year;
        data['expirySlot'] = vehicle.expiry_slot;
    }
    else if(vehicle.policy_type == "new") {
        var today = new Date();
        today.setDate(today.getDate() + 1);
        data["policy_start_date"] = today.getDate();
        data["policy_start_month"] = utils.getMonthNumberToName(today.getMonth());

        today.setMonth(today.getMonth() - 2);
        data["man_month"] = utils.getMonthNumberToName(today.getMonth());
        data["man_year"] = today.getFullYear();
    }

    if (vehicle.policy_type == "expired") {
        if($("#moreThan90days_car").is(":checked")){
            var t_100 = new Date();
            t_100.setDate(t_100.getDate() - 100);
            data['expiry_date'] = t_100.getDate();
            data["expiry_month"] = utils.getMonthNumberToName(t_100.getMonth());
            data["expiry_year"] = t_100.getFullYear();
        }
        else{
            data['expiry_date'] = $("#car_expiry_date :selected").attr("id");
            data["expiry_month"] =$("#car_expiry_month :selected").attr("name");
            data["expiry_year"] =$("#car_expiry_year :selected").attr("id");
        }

        Cookies.set("lms_campaign","motor-offlinecases");
        Cookies.set("mark_product_category","commonAncestorContainer");
    }

    $.post("/motor/submit-landing-page/", data, function(response) {
        var response = JSON.parse(response);
        if (response.success) {
            var mobNo = $("#txtMobileNumber").val();
            if(mobNo.length>0){

                Cookies.set("mobileNo", mobNo);

                data["campaign"] = "motor-homepage";
                data["label"] = "homepage";
                data["mobile"] = mobNo;

                if(vehicle.policy_type == 'expired'){
                    data["lms_campaign"] = "motor-offlinecases";
                }

                console.warn(data);

                $.post("/leads/save-call-time/",{
                    'data' : JSON.stringify(data), 
                    'csrfmiddlewaretoken' : CSRF_TOKEN
                }, function(res){});
            }

            if (localStorage) {
                if(vehicle.policy_type == "expired"){
                    response.data["policyExpired"] =true;
                }
                else if(vehicle.policy_type == "new"){
                    response.data["man_month"] = data.man_month.name;
                    response.data["man_year"] = data.man_year;
                }

                localStorage.setItem(VEHICLE_TYPE_CAR, JSON.stringify(response.data));
            } else {
                for (var item in response.data) {
                    var value = response.data[item];
                    if (value != null && typeof(value) == "object") {
                        value = JSON.stringify(value);
                    }
                    Cookies.set(VEHICLE_TYPE_CAR + item, value);
                }
            }

            // Send data to Google Analytics
            var gtm_data = {}
            //if(vehicle.policy_type != "expired"){
            gtm_data = {
                param1: JSON.parse(data.vehicle).name,
                param2: JSON.parse(data.fuel_type).name,
                param3: JSON.parse(data.vehicleVariant).name,
                param4: JSON.parse(data.rto_info).name
            }
            //}

            if (vehicle.policy_type != "new")
                gtm_data.param5= data.reg_year;
            if(data.isCNGFitted)
                gtm_data.param6 = data.cngKitValue;


            utils.gtm_push("HomeCarEventGetQuote",
                "Homepage Car - Get Quote",
                vehicle.policy_type + " Car",
                " ", gtm_data,
                function(){
                    window.location.href = redirect_url;
                }
            );
        }
    });
};

 var validateCarForm = function() {
    var valid = true;
    $('.error-text').hide();
    var vehicle = {
        policy_type: $('[name=car_policy_type]:checked').val(),
        expiry_slot: $('[name=expiry_slot]:checked').val(),
        expiry_date: $("#car_expiry_date :selected").attr("id"),
        expiry_month:$("#car_expiry_month :selected").attr("name"),
        expiry_year:$("#car_expiry_year :selected").attr("id"),
        vehicle_make_model: $('#vehicle_make_model :selected').attr("id"),
        vehicle_fuel_type: $('#vehicle_fuel_type :selected').attr("id"),
        vehicle_variant: $('#vehicle_variant :selected').attr("id"),
        vehicle_rto: $('#vehicle_rto :selected').attr("id"),
        reg_year: $('#vehicle_reg_year :selected').attr("id"),
        cngKitValue: $('#txtCNGKit').val()
    },
    whatsapp_checked = $("#whatsapp-checkbox").attr("checked");

    if (!vehicle.vehicle_make_model || !vehicle.vehicle_fuel_type || !vehicle.vehicle_variant) {
        valid = false;
        $('#vehicle_detail_error').fadeIn();
    }
    if (!vehicle.vehicle_rto) {
        valid = false;
        $('#rto_error').fadeIn();
    }
    if ((vehicle.policy_type != "new") && !vehicle.reg_year) {
        valid = false;
        $('#reg_date_error').fadeIn();
    }

    if($('#vehicle_fuel_type :selected').attr("name") == "CNG/LPG Externally Fitted" && vehicle.cngKitValue == 0){
        valid = false;
        $('#cng_error').fadeIn();
    }

    if (vehicle.policy_type == "expired") {
        if(!validateCarExpiredDateValue())
            valid = false;
    }

    var cngreg = /^[1-9]\d*$/;

    if($('#vehicle_fuel_type :selected').attr("name") == "CNG/LPG Externally Fitted"){
        if(cngreg.test(vehicle.cngKitValue) && parseInt(vehicle.cngKitValue) <= 40000 ){
            $('#cng_error').fadeOut();
        }
        else{
            valid = false;
            $('#cng_error').fadeIn();
        }
    }
    if(!validateMobileNumber())
        valid = false;

    return valid;
};

var validateMobileNumber = function(){
    var mobNo = $("#txtMobileNumber").val().replace(/\s+/,'');
    var valid = false;
    var regex = /^[\d]+$/;
    if(mobNo.length == 0){
        valid = true;
    }
    else {
        valid = validation.isMobileNumber(mobNo);
    }

    if(!valid){
        $("#txtMobileNumber_error").fadeIn();
    }
    else{$("#txtMobileNumber_error").fadeOut();}
    return valid;
}

/********* Event functions *********/
$('input[name="car_policy_type"]').change(function() {
    setUpCarForm();
});

$("#vehicle_make_model").on("change",function() {
    selected_model_id_car = $(this).children(":selected").attr("id");
    $('#vehicle_fuel_type').select2("val", "");
    getCarVariants();
});

$("#vehicle_fuel_type").on("change",function() {
    selected_fuel_type_id_car = $(this).children(":selected").attr("id");
    getCarVariants();
    var selected_fuel =  $(this).children(":selected").text().trim();
    if(selected_fuel=="CNG/LPG Externally Fitted"){
        $("#divCNGKit").removeClass("hide").fadeIn();
        if($('#txtCNGKit').val().length == 0){
            ///$('#txtCNGKit').val(0);
            $('#txtCNGKit').parent().addClass("selected").removeClass("hide")
        }
    }
    else{
        $('#divCNGKit').fadeOut();
        $('#txtCNGKit').val('');
    }
});

$("#vehicle_variant").on("change",function(){
    if($("option:selected", this).attr("id") >0 || $("option:selected", this).attr("id").length>0)
        $('#vehicle_detail_error').fadeOut();
});

$("#vehicle_rto").on("change", function(){
    if($("option:selected", this).attr("id") >0 || $("option:selected", this).attr("id").length>0)
        $('#rto_error').fadeOut();
});

$("#vehicle_reg_year").on("change",function () {
    if($("option:selected", this).attr("id") >0 || $("option:selected", this).attr("id").length>0)
        $('#reg_date_error').fadeOut();
})

$('#car_submit').on('click', function(event) {
    event.preventDefault();
    if (validateCarForm())
        submitCarForm();
});

$("#txtCNGKit").on("focus", function(){
    if($(this).val()<=0)
        $(this).val('');
});
$("#txtCNGKit").on("blur",function(){
    if($(this).val()<=0){
        $(this).val('');
    }
    validateCarForm();
});

// $("#car_expiry_date,#car_expiry_month,#car_expiry_year").on("change", function(){
//     //validateCarExpiredDateValue();
// });

$("#car_expiry_date").on("change", function(){
    if($("option:selected", this).attr("id")>0){
        var err = "#"+ $(this).attr("id")+ "_error";
        $(err).fadeOut();
    }
});


$("#moreThan90days_car").on("change", function(){
    if($(this).is(":checked")){
        $("#expiryCalendar_car").slideUp();
    }
    else{
        $("#expiryCalendar_car").slideDown();
    }
});

$("#txtMobileNumber").on("blur", function(){
    if($(this).val().length == 0){
        $(this).parents(".cf-special-ip").removeClass("selected");
    }
    validateMobileNumber();

});

/********* Init functions *********/
(function initCarForm(){
    // Fetch data and assign to controls
    setUpCarForm();
    getRTOs_Car();
    getVehicles_Car();
    setFuelTypes_Car();
    setRegYears();
    setCarForm();
})();
