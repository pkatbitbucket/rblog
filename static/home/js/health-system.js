var formName = "Health_form"

window.onunload = function(){}

ko.validation.configure({
    errorMessageClass: 'error',
    decorateElement : true,
    errorElementClass: 'has-error',
    insertMessages: false
});

function Member(type, selected, age, person, ageList, displayName) {
    var self = this;
    self.selected = ko.observable(selected);
    self.person = person;
    self.id = self.person.toLowerCase();
    self.type = type;
    var age_full = ko.observable(age).extend({
        validation:{
            'validator': function(n){
                if(!self.selected()){
                    return true;
                }
                if(self.type != 'kid'){
                    return true;
                }
                if(['', undefined, null].indexOf(n) > -1){
                    return false;
                } else {
                    return true;
                }
            },
            'message' : 'Please select age of insured member'
        }
    });
    if(type == 'kid') {
        self.age_full = age_full;
        self.age = ko.observable();
        var i = 1;
        var A = [];
        while(i < 12){
            A.push(i + " months");
            i++;
        }
        i = 1;
        while(i <= 25){
            A.push(i + " years");
            i++;
        }
        self.ageList = A;
    } else {
        self.age = age_full;
        self.ageList = ageList;
    }
    self.age_in_months = ko.observable(1);
    if(type == 'kid') {
        self.calculate_age = ko.computed(function(){
            if(String(self.age_full()).indexOf('years') > -1){
                self.age(parseInt(self.age_full()));
                self.age_in_months(0);
            } else {
                self.age_in_months(parseInt(self.age_full()));
                self.age(0);
            }
        });
    }

    self.error = ko.observable("");
    if(!displayName){
        self.displayName = person;
    } else {
        self.displayName = displayName;
    }
}

function stepOneViewModel() {
    var self = this;
    self.range = function (a, b, step) {
        var A = [];
        A[0] = a;
        step = step || 1;
        while(a+step<=b){
            A[A.length] = a+= step;
        }
        return A;
    };
    self.initialize_members = function(obj, value) {
        var kid_id = 1;
        if(value.length > 0){
            self.you.selected(false);
        }
        ko.utils.arrayForEach(value, function(v){
            switch(v.person) {
                case "You":
                    self.you.selected(v.selected);
                self.you.age(parseInt(v.age));
                self.ageList = self.range(18, 100);
                break;
                case "Spouse":
                    self.spouse.selected(v.selected);
                self.spouse.age(parseInt(v.age));
                self.ageList = self.range(18, 100);
                break;
                case "Father":
                    self.father.selected(v.selected);
                self.father.age(parseInt(v.age));
                self.ageList = self.range(18, 100);
                break;
                case "Mother":
                    self.mother.selected(v.selected);
                self.mother.age(parseInt(v.age));
                self.ageList = self.range(18, 100);
                break;
                default:
                    var m = new Member('kid', v.selected, v.age, v.person, v.ageList, v.displayName);
                if(['son', 'daughter'].indexOf(v.person.toLowerCase()) > -1) {
                    if(v.age_in_months != 0){
                        m.age_in_months(parseInt(v.age_in_months));
                        m.age_full(v.age_in_months + " months");
                    } else {
                        m.age_in_months(0);
                        m.age_full(v.age_full);
                    }
                    m.id = v.person.toLowerCase() + kid_id;
                    kid_id++;
                }
                obj.push(m);
            }
        });
    };
    self.gender = ko.observable().extend({
        store : {
            key : 'gender'
        },
        required: {
            params:true,
            message: "Please select your gender"
        }
    });

    self.product_type = ko.observable("INDEMNITY").extend({
        store : {
            key : 'product_type'
        },
        required: {
            params:true,
            message: "Please select a product type"
        }
    });

    self.deductible = ko.observable().extend({
        validation:[{
            validator: function(obj){
                var valid = false;
                if(self.product_type() == "SUPER_TOPUP"){
                    if(self.deductible())
                        valid = true;
                }
                else{
                    valid = true
                }
                return valid;
            },
            'message': "Please select sum assured"
        }]
    });

    self.you = new Member('adult', true, undefined, 'You', self.range(18, 100), 'You');
    self.spouse = new Member('adult', false, undefined, 'Spouse', self.range(18, 100), ko.computed(function(){
        if (self.gender() == 'MALE'){
            return 'Your Wife';
        }
        else if (self.gender() == 'FEMALE') {
            return 'Your Husband';
        } else {
            return 'Your Spouse';
        }
    }));
    self.father = new Member('adult', false, undefined, 'Father', self.range(18, 100));
    self.mother = new Member('adult', false, undefined, 'Mother', self.range(18, 100));
    self.members = ko.observableArray([self.you, self.spouse, self.father, self.mother]).extend({
        store : {
            key : 'members',
            init: self.initialize_members,
            dump: function(nval){
                var nval = ko.toJS(nval);
                var mList = []
                ko.utils.arrayForEach(nval, function(m){
                    delete m['ageList'];
                    delete m['error'];
                    if(m.selected){
                        mList.push(m);
                    }
                });
                return ko.toJSON(mList);
            }
        },
        validation : [
            {
            'validator' : function(obj){
                if(ko.utils.arrayFilter(obj, function(m) { return m.selected() == true }).length == 0){
                    return false;
                } else {
                    return true;
                }
            },
            'message' : 'Select atleast one member to insure'

        },{
            'validator' : function(obj){
                if(ko.utils.arrayFilter(obj, function(m) { return m.selected() == true && m.type == 'adult' }).length == 0){
                    return false;
                } else {
                    return true;
                }
            },
            'message' : 'Select atleast one adult to insure'
        },{
            validator: function(obj){
                var kids = ko.utils.arrayFilter(obj, function(member) { return member.type == "kid" && member.selected() == true });
                if(kids && kids.length>0){
                    if(self.you.selected() == true || self.spouse.selected() == true)
                        return true;
                    else return false
                }
                return true;
            }, 
            "message":"Please select a parent"
        }]
    });

    self.selectMember = function(member){
        if (member.selected() == true) {
            if(member.type == 'kid')
                {
                    self.members.remove(member);
                    //return member.selected(false);
                }
                else {
                    //return member.selected(false);
                }
        }
        else {
            //return member.selected(true);
        }
        return true;
    };
    self.selectGender = function(gen) {
        self.gender(gen);
    }

    self.addKid = function(person, age) {
        var kid = new Member('kid', true, age, person, self.range(1, 25), person);
        kid.id = person.toLowerCase() + (self.getSelectedKids().length + 1);
        self.members.push(kid);
        $(".health-product select").select2();
    };

    self.getSelectedKids = ko.computed(function(){
        return ko.utils.arrayFilter(self.members(), function(member) { return member.type == "kid" && member.selected() == true });
    });
    self.getSelectedAdults = ko.computed(function(){
        return ko.utils.arrayFilter(self.members(), function(member) { return member.type == "adult" && member.selected() == true });
    });
    self.getAdults = ko.computed(function(){
        return ko.utils.arrayFilter(self.members(), function(member) { return member.type == "adult"});
    });
    self.getSelectedMembers = ko.computed(function(){
        return ko.utils.arrayFilter(self.members(), function(member) { return member.selected() == true });
    });

    self.cumulativeAgeError = ko.observable().extend({
        validation: [{
            validator: function(obj){
                var bad_members = ko.utils.arrayFilter(self.getSelectedMembers(), function(m) {
                    if(m.age_full == undefined) {
                        return isNaN(m.age())
                    } else {
                        return ((isNaN(m.age()) || (m.age() == 0)) && (isNaN(m.age_in_months()) || (m.age_in_months() == 0)))
                    }
                });
                if(bad_members.length == 0){
                    return true;
                } else {
                    return false;
                }
            },
            'message':"Please select respective member's age"
        }]
    });
    self.step1Errors = ko.validation.group([self.members,self.gender, self.deductible, self.cumulativeAgeError, self.product_type]);
    self.validate = function() {
        valid = true;
        if(!self.step1Errors().length == 0) {
            self.step1Errors.showAllMessages(true);
            valid = false;
        }
        return valid;
    };
};

var pincheck = window.pincheck();
function stepThreeViewModel() {
    var self = this;
    self.userPincode = ko.observable().extend({
        store : {
            key : 'pincode'
        },
        required: {
            params:true,
            message: "Please select your area pincode"
        },
        validation : {
            validator : function(val){
                return pincheck.isValid(val);
            },
            message : "Please enter a valid pincode"
        }
    });
    self.parentsPincode = ko.observable().extend({
        store : {
            key : 'parents_pincode'
        },
        required: {
            params:true,
            message: "Please select parents area pincode"
        },
        validation : {
            validator : function(val){
                return pincheck.isValid(val);
            },
            message : "Please enter a valid pincode"
        }
    });
    self.hasParents = function() {
        var par = ko.utils.arrayFilter(step1.getSelectedMembers(), function(member) { return member.person == 'Father' || member.person == 'Mother'  });
        if (par.length > 0) {
            return true;
        }
        else {
            return false;
        }
    };
    self.hasFamily = function() {
        var par = ko.utils.arrayFilter(step1.getSelectedMembers(), function(member) { return member.person == 'You' || member.person == 'Spouse' || member.type == 'kid'  });
        if (par.length > 0) {
            return true;
        }
        else {
            return false;
        }
    };

    self.getErrorGroup = function(){
        if(self.hasParents() && self.hasFamily()){
            self.step3Errors = ko.validation.group([self.userPincode, self.parentsPincode]);
        }
        if(!self.hasParents()) {
            self.step3Errors = ko.validation.group([self.userPincode]);
        }
        if(self.hasParents() && !self.hasFamily()) {
            self.step3Errors = ko.validation.group([self.parentsPincode]);
        }
    };
    self.validate = function() {
        valid = true;
        self.getErrorGroup();
        if(self.step3Errors() && !self.step3Errors().length == 0) {
            self.step3Errors.showAllMessages();
            valid = false;
        }
        return valid;
    };
};

function ExpertViewModel() {
    var self = this;
    self.email = ko.observable().extend({
        store : {
            key : 'email'
        },
        email: true,
        required: {
            params:true,
            message: "Please enter valid email"
        }
    });
    self.mobileNo = ko.observable().extend({
        store : {
            key : 'mobileNo'
        },
        required:{
            params: true,
            message: "Please enter your mobile number"
        },
        pattern: {
            params: '^([1-9])([0-9]){9}$',
            message: "Please enter a valid mobile number"
        }
    });

    self.getErrorGroup = function(){
        if(step_lead.currentStep() == 2){
            self.ExpertErrors = ko.validation.group([self.email, self.mobileNo]);
        } else {
            self.ExpertErrors = ko.validation.group([]);
        }
    };
    self.validate = function() {
        valid = true;
        self.getErrorGroup();
        if(!self.ExpertErrors().length == 0) {
            self.ExpertErrors.showAllMessages();
            valid = false;
        }
        return valid;
    };
}

var step1 = new stepOneViewModel();
var step3 = new stepThreeViewModel();
var expert = new ExpertViewModel();

function stepLeadModel() {
    var self = this;
    self.change_current_step = function(x){
        self.currentStep(x);
    };
    self.show_loader = ko.observable(false);
    self.currentStep = ko.observable(0);
    self.currentStepVerbose = ko.observable('0');
    self.currentStepVerbose.subscribe(function(val){
        self.currentStep(parseInt(val));
    });
    self.post_data = function() {
        var members = ko.toJS(step1.getSelectedMembers());
        ko.utils.arrayForEach(members, function(m){
            delete m.ageList;
            delete m.calculate_age;
        });
        return {
            members: members,
            gender: step1.gender(),
            product_type: step1.product_type(),
            deductible: step1.deductible(),
            pincode: step3.userPincode(),
            parents_pincode: step3.parentsPincode()
        };
    };

    self.loadLocalStoreData = function(){
        var memberList = [];
        if(localStorage){
            if(localStorage.gender)
                step1.gender(localStorage.gender);
            if(localStorage.product_type)
                step1.product_type(localStorage.product_type);
            if(localStorage.deductible)
                step1.deductible(localStorage.deductible);
            if(localStorage.pincode)
                step3.userPincode(localStorage.pincode);
            if(localStorage.parents_pincode)
            step3.parentsPincode(localStorage.parents_pincode);

            if(localStorage.members){
                var m = JSON.parse(localStorage.members);
                var memberListObj = [];
                var isYouSelected = false;

                m.forEach(function(row, index){
                    var age = row.age.split('-');
                    if(age[0]>0){
                        step1[row.person.toLowerCase()].age(parseInt(age[0]));
                        step1[row.person.toLowerCase()].selected(true);
                    }
                    else{
                        if(row.person.toLowerCase() == "son" || row.person.toLowerCase() == "daughter")
                            step1.addKid(row.person, age[1] + " months");
                    }

                    if(row.person.toLowerCase() == "you"){
                        isYouSelected = true;
                    }
                });

                if(!isYouSelected){
                    step1.you.selected(false);
                }
            }

            utils.showLabelForInput($("#cbSumAssured"));
            $("#selfpin").trigger("keyup");
            $("#parentspin").trigger("keyup");
        }
    },
    self.saveAndLoadResults = function(){
        var valid = step3.validate();
        Cookies.set('hide_login', 'True', { expires: 300 });
        if(valid){
            var memberList = [];
            // Write data to Local Storage
            for (var row in self.post_data()){
                if (self.post_data().hasOwnProperty(row)) {
                    if(self.post_data()[row] && row.toUpperCase() != "members".toUpperCase()){
                        localStorage.setItem(row, self.post_data()[row]);
                    }
                    else if(self.post_data()[row] && row.toUpperCase() == "members".toUpperCase()){
                        self.post_data()[row].forEach(function(m, index){
                            var age = m.age && m.age>0? m.age + "-0": "0-" + m.age_in_months;
                            memberList.push({"person":m.person, age: age})
                        });
                        localStorage.setItem(row, JSON.stringify(memberList));
                    }
                }
            }

            var request = $.post('/health-plan/save-details/results/?hot=true',
                {'csrfmiddlewaretoken' : CSRF_TOKEN, 'data': JSON.stringify(self.post_data())},
            function(response){

            //  MixPanel Events
            //utils.MixPanelTrack(MixPanelEvents.Health_Home_Product.name, MixPanelEvents.Health_Home_Product.params);

                // set values to Localstorage/Cookie
                if (localStorage) {
                    localStorage.setItem(formName, JSON.stringify(self.post_data()));
                } else {
                    for (var item in jsonData) {
                        var value = response.data[item];
                        if (value != null && typeof(value) == "object") {
                            value = JSON.stringify(value);
                        }
                        Cookies.set(formName + item, value);
                    }
                }

                var res = JSON.parse(response);
                var gtm_data = {}
                var memberList = "";
                $.each(step1.members(),function(index, row){
                    if(row.selected()){
                        var age = row.age()>0?row.age():"0." +row.age_in_months();
                        memberList += row.person + " - " + age + " || ";
                    }
                });

                gtm_data.param1 = memberList.substring(0, memberList.length-4)

                utils.gtm_push("HomeHealthEventGetQuote",
                    "Homepage Health - Get Quote",
                    step1.gender(),
                    " ", gtm_data,
                    function(){
                        window.location.href ="/health-plan/#medical-conditions"
                    }
                );
            });
        }
    }
    self.sendDataToLms = function(){
        jsonData = self.post_data();
        jsonData['campaign'] = 'Health';
        jsonData['label'] = 'page_id';
        jsonData['triggered_page'] = window.location.href;
        $.ajax({
            type: "POST",
            url: "/leads/save-call-time/",
            data: {'data':JSON.stringify(jsonData),'csrfmiddlewaretoken':CSRF_TOKEN},
            success: function(response){
                //alert("..success..");
                // if (window.dataLayer){
                //     window.dataLayer.push({
                //         'event': 'GAEvent',
                //         'hitType': 'event',
                //         'eventCategory': 'HEALTH',
                //         'eventAction': 'CallScheduled',
                //         'eventLabel': 'page_id',
                //         'eventValue': 4
                //     });
                // }
            }
        });
    };

    self.nextStep = function(){
        var valid1 = true;
        var adultSelected = false;
        var genderSelected = false;
        var missingAge = false;
        step1.members.valueHasMutated();
        if(parseInt(self.currentStep()) == 0){
            valid1 = step1.validate();
            if(step1.gender())
                genderSelected = true;
            else
                genderSelected = false;
            $.each(step1.members(), function(index, row){
                if(row.selected() && row.type == "adult"){
                    adultSelected = true;
                }
                if(row.selected() && !(row.age() || (row.age()>0 || row.age_in_months()>0))){
                    missingAge = true;
                }
            });

            valid1 =valid1 && genderSelected && adultSelected && !missingAge?true:false;

            var valid3 = step3.validate();

            //var valid4 = expert.validate();
            if(valid1 && valid3){
                self.show_loader(true);
                self.sendDataToLms();
                self.saveAndLoadResults();
            }
        }
    }
}

var step_lead = new stepLeadModel();

vm = {
    'step_lead' : step_lead,
    'step1': step1,
    'step3': step3,
    'expert' : expert
};

ko.applyBindings(vm, document.getElementById('health_widget'));

// PinCode
(function(){
    $("#selfpin, #parentspin").on("keydown", function(e){
        if(!utils.isNumberKey(e))
            e.preventDefault();
    });

    var sumInsurred = [{id:"1 Lac", label: "1 Lac", value:"100000"}]
    for (var i=2; i<=10; i++)
        sumInsurred.push({id:i + " Lacs", label: i + " Lacs", value:i*100000});

    utils.createSelect_new("#cbSumAssured", sumInsurred);
    step_lead.loadLocalStoreData();
    $(".health-product select").select2();
    setTimeout(function(){$(".loading").removeClass("loading");},0);
})();
