var homeformName = "Home_Form";

/********* Private functions *********/
function validate_Home(){
    $('.error-text').fadeOut();
    var value = $("#home_value :selected").val(),
        built_area = $('#home_built-area').val(),
        email = $('#home_inputEmail').val(),
        phone = $('#home_inputContact').val();

    var valid = true;

    if (value.length<1) {
        valid = false;
        $('.home_value-error').fadeIn();
    }

    if (!validation.isNotEmpty(built_area) || !validation.isNumber(built_area)) {
        valid = false;
        $('.home_built-up-error').fadeIn();
    }

    if (!validation.isEmailAddress(email)) {
        valid = false;
        $('.home_email-error').fadeIn();
    }

    if (!validation.isNotEmpty(phone) || !validation.isNumber(phone) || phone.length !== 10) {
        valid = false;
        $('.home_mobile-error').fadeIn();
    }
    return valid;
}

// Fetch data from LocalStorage and set it to form
function setHomeForm(){
    var email, phone, buildArea, contentValue, ownership;
    if(localStorage && localStorage.getItem(homeformName)){
        var _local = JSON.parse(localStorage.getItem(homeformName));
        email = _local.email;
        phone = _local.mobile;
        contentValue = _local.Value_of_content;
        buildArea = _local.Built_area;
        ownership = _local.ownership;
    }
    // Fetch data from Cookie
    else{
        email= Cookies.get(homeformName +'_email'),
        phone= Cookies.get(homeformName + '_mobile'),
        contentValue= Cookies.get(homeformName + 'Value_of_content'),
        buildArea= Cookies.get(homeformName + 'Build_area'),
        ownership= Cookies.get(homeformName + 'ownership')
    }

    $('#home_value').select2().select2('val',contentValue);
    $('#home_built-area').val(buildArea);
    $("#home_inputContact").val(phone);
    $("#home_inputEmail").val(email);
    $("input[name=ownership][value='"+ownership+"']").attr("checked",true);

    if(contentValue)
        utils.showLabelForInput($('#home_value'))
    if(buildArea)
        utils.showLabelForTextField($('#home_built-area'))
    if(email)
        utils.showLabelForTextField($("#home_inputContact"))
    if(phone)
        utils.showLabelForTextField($("#home_inputEmail"))
    setTimeout(function(){$(".loading").removeClass("loading");},0);
}

/********* Event functions *********/
$('#home_inputContact').on("blur",function(){
    if(validation.isMobileNumber($(this).val()))
        $(this).parents('.prnone').find('.error-text').fadeOut();
    if($(this).val().length == 0){
        $(this).parents(".cf-special-ip").removeClass("selected");
    }
});

$("#home_inputEmail").on("blur",function(){
    if(validation.isEmailAddress($(this).val()))
        $(this).parents('.prnone').find('.error-text').fadeOut();
    if($(this).val().length == 0){
        $(this).parents(".cf-special-ip").removeClass("selected");
    }
});

$("#home_built-area").on("blur", function(){
    if($(this).val()>0)
        $(this).parents('.prnone').find('.error-text').fadeOut();
    else{
        $(this).parents(".cf-special-ip").removeClass("selected");
    }
});

$("#home_built-area").on("keydown", function(e){
    if(!utils.isNumberKey(e)) {
        e.preventDefault();
    }
})

$("#home_value").on("change",function(){
    if($("option:selected", this).attr('id') > 0){
        $('.home_value-error').fadeOut();
    }
});

$("#home_submit").on("click",function(event) {
    event.preventDefault();
    var jsonData = {};
    var formData = $("#home_form").serializeArray();

    if (validate_Home()) {
        $('#home_submit').addClass('loading').text("Submitting please wait ..");
        $.each(formData, function() {
            if (jsonData[this.name]) {
                if (!jsonData[this.name].push) {
                    jsonData[this.name] = [jsonData[this.name]];
                }
                jsonData[this.name].push(this.value || '');
            } else {
                jsonData[this.name] = this.value || '';
            }
        });
        jsonData['campaign'] = 'Home';
        jsonData['lms_campaign'] = 'Home';
        jsonData['label'] = 'home_homepage';
        jsonData['device'] = 'Desktop';
        jsonData['triggered_page'] = window.location.href;

        $.ajax({
            type: "POST",
            url: "/leads/save-call-time/",
            data: {
                'data': JSON.stringify(jsonData),
                'csrfmiddlewaretoken': CSRF_TOKEN
            },
            success: function(response) {

            //  MixPanel Events
            //utils.MixPanelTrack(MixPanelEvents.House_Home_Product.name, MixPanelEvents.House_Home_Product.params);

                $('#home_submit').addClass('done').text("Thanks! We will get back to you soon!");
                $('#home_submit').prop("disabled","disabled");

                // set values to Localstorage/Cookie
                if (localStorage) {
                    localStorage.setItem(homeformName, JSON.stringify(jsonData));
                } else {
                    for (var item in jsonData) {
                        var value = response.data[item];
                        if (value != null && typeof(value) == "object") {
                            value = JSON.stringify(value);
                        }
                        Cookies.set(homeformName + item, value);
                    }
                }

                // Send data to Google Analytics
                var value = JSON.parse($("#home_value :selected").val()).value,
                    built_area = $('#home_built-area').val(),
                    email = $('#home_inputEmail').val(),
                    phone = $('#home_inputContact').val();
                var gtm_data = {
                    param1: value,
                    param2: built_area
                }
                utils.gtm_push("HomeHouseEventGetQuote",
                    "Homepage Home - Get Quote",
                    $("input[name=ownership]:checked").val(),
                    " ", gtm_data);
            }
        });
    }
});

/********* Main functions *********/
(function initHomeForm(){
    var arrHomeValue = [{id:"1 Lac", name: "1 Lac", value:"1 Lac"}]
    for (var i=2; i<=10; i++)
        arrHomeValue.push({id:i + " Lacs", name: i + " Lacs", value:i + " Lacs"})
    utils.createSelect($("#home_value"),arrHomeValue);
    utils.createMobileField('#home_inputContact');
    setHomeForm();
})();
