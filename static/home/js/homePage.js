var slider1, slider2;
var ContactForm_top = {
    policyType:"",
    mobileNo:"",
    preferredTime:"",
    preferredTimeValue:"",
    day:""
};
var ContactForm_mid = {
    policyType:"",
    mobileNo:"",
    preferredTime:"",
    preferredTimeValue:"",
    day:""
};


var ContactFormSUmbitted = {
    top:false,
    middle:false,
}

var random= parseInt(Math.random()*100)%2;
var secondBanner = random==0?1:0;

// Validate Contact Me Form. Both top and middle.
// pos value: top for top, mid for middle
function validateForm(pos){
    var mobileNo, policyType, preferredTime;
    if(pos == "top"){
        ContactFormSUmbitted.top = true
        mobileNo = ContactForm_top.mobileNo;
        preferredTime = ContactForm_top.preferredTime;
        policyType = ContactForm_top.policyType;
    }
    else{
        ContactFormSUmbitted.middle = true
        mobileNo = ContactForm_mid.mobileNo;
        preferredTime = ContactForm_mid.preferredTime;
        policyType = ContactForm_mid.policyType;
    }
    if(validation.isMobileNumber(mobileNo) && (preferredTime != "prefered" && preferredTime.length>1) && policyType.length>1){
        $("#btnContactMe_"+pos).prop("disabled",false);

        if(ContactFormSUmbitted.middle)
            var mobileNoOn = 'Home Page - Contact Form Mid';
        else
            var mobileNoOn = 'Home Page - Contact Form Top';


        Cookies.set('mobileNo', mobileNo);
        Cookies.set('mobileNoOn', mobileNoOn);
        Cookies.set('mobileNoURL', window.location.href);
    }
    else{
        $("#btnContactMe_"+pos).prop("disabled","disabled");
    }
}

// Format data and send data to LMS
function createDataForLMS(pos){
    var mobileNo, policyType, preferredTime, preferredTimeValue;
    if(pos== "top"){
        mobileNo = ContactForm_top.mobileNo;
        preferredTime = ContactForm_top.preferredTime;
        policyType = ContactForm_top.policyType;
        preferredTime = ContactForm_top.preferredTime;
        preferredTimeValue = ContactForm_top.preferredTimeValue;
        day = ContactForm_top.day;

    }
    else{
        mobileNo = ContactForm_mid.mobileNo;
        preferredTime = ContactForm_mid.preferredTime;
        policyType = ContactForm_mid.policyType;
        preferredTime = ContactForm_mid.preferredTime;
        preferredTimeValue = ContactForm_mid.preferredTimeValue;
        day = ContactForm_mid.day;
    }

    var campaignType = "";
    var label = "homepage_contactMe_" +(pos=="top"?"top":"middle");

    switch(policyType.trim()){
        case "Two Wheeler": campaignType =  "motor-bike-homepage"; break;
        case "Car": campaignType =  "motor-homepage"; break;
        case "Health": campaignType =  "health-homepage"; break;
        case "Home": campaignType =  "home-homepage"; break;
        case "Travel": campaignType =  "travel-homepage"; break;
        case "Term Life": campaignType =  "term-homepage"; break;
    }

    var d = new Date();
    // Add day
    var preferred_call_time = (day=="Today"?d.getDate():d.getDate()+1) + "/" + (d.getMonth()+1) + "/" + d.getFullYear();
    // Add time

    var call_time=(day=="Today"?d.getDate():d.getDate()+1)+"/"+(d.getMonth()+1)+"/"+d.getFullYear();
    var _ptv = preferredTimeValue.substring(0, preferredTimeValue.length-2)

    if(_ptv == '9:30'){
        _ptv = '9:30:00'
    }
    if(_ptv == '12'){
        _ptv = '12:00:00'
    }
    if(_ptv == '3'){
        _ptv = '15:00:00'
    }
    if(_ptv == '5'){
        _ptv = '17:00:00'
    }

    call_time +=" " + _ptv;

    var _lms_data = LMS_DefaultData;
    _lms_data.mobile = mobileNo;
    _lms_data.campaign = campaignType;
    _lms_data.preferred_call_time = call_time;
    _lms_data.label = label;
    _lms_data.triggered_page = window.location.href;

    $.post("/leads/save-call-time/",
        {'data' : JSON.stringify(_lms_data), 'csrfmiddlewaretoken' : CSRF_TOKEN}, function(data){
        if (data.success){
            utils.gtm_push(
                "HomeContactMe",
                "HomePage - Get Quotes",
                "",
                label,
                {
                    "param7": campaignType,
                    "param8": day + " " + preferredTime
                })
        }
    }, 'json');
}

// Used to add Today/ Tomorrow in preferred time select
function setPreferredTimeDayLabel(parent){
    var li_top = $(parent).find("li");
    var current_time = new Date().getHours();
    var tomorrow=[], today = [];

    for(var i = 0;  i<li_top.length; i++){
        var element = $(parent).find("li")[i];
        // fetch Time string 9:30am
        var time = $(element).find("small").text().split(" ")[0].substring(1);
        var am_pm = 0;
        if(time.indexOf("pm")>=1 && time != "12pm"){
            am_pm = 12;
        }
        // remove am/pm
        time = time.substring(0, time.length-2);
        // replace : with .
        time = time.replace(":",".")
        var str = $(element).find("span").text()

        var day = "";
        if(parseInt(time)+ parseInt(am_pm)>parseInt(current_time)){
            day = "Today "
        }
        else{
            day ="Tomorrow ";
        }
        $(element).find("span").text(day+str);

        if(day == "Today ")
            today.push($(element).html())
        else tomorrow.push($(element).html())
    }
    var str = "";
    today.forEach(function(row, index){
        str+="<li>" + row + "</li>";
    })
    tomorrow.forEach(function(row, index){
        str+="<li>" + row + "</li>";
    });

    $(parent).find("ul").html(str)
}

function toggleBannerImage(product){
    if(product  == "Two Wheeler")
        product = "bike";

    var random= parseInt(Math.random()*100)%2;
    var productLink=false;
    switch(product.toLowerCase()){
        case "bike":
        case "travel":
        case "home":
            random = 0;
            break;
        case "health":
            productLink = true;
    }
    // health tax saving creative banners logic
    var right_banner = "/static/home/home_2/img/banners/savetax/hb-right-tab-"+product.toLowerCase()+".png"
    var top_banner = "/static/home/home_2/img/banners/savetax/hb-top-tab-"+product.toLowerCase()+".png"
    $("#top_banner_"+product.toLowerCase()+" + a").click(function(e){
        e.preventDefault();
        e.stopPropagation();
        $('#Menu_lis li')[2].click();
    });
    if(productLink){
        $("#display-banner a#product-tab").addClass('hide');
        $("#display-banner a#product-link").attr("href","/"+product.toLowerCase()+"-plan").removeClass("hide");
    }
    else {
        $("#display-banner a#product-link").addClass('hide');
        $("#display-banner a#product-tab").removeClass('hide');
    }
    // Regular banner image display logic
    //var right_banner = "/static/home/home_2/img/banners/right-col-"+product.toLowerCase()+"-banner-"+(random+1)+".jpg"
    //var top_banner = "/static/home/home_2/img/banners/top-bar-"+product.toLowerCase()+"-banner-"+(random+1)+".jpg"


    $("#display-banner").css("background-image", "url('"+right_banner+"')");
    $("#top_banner_"+product.toLowerCase()).attr("src", top_banner);
}
/********* Event functions *********/
function registerEvents(){
    $('#Menu_lis li').bind('click', function() {
        var showBanner = false;
        $(this).parent().find('.active').removeClass('active');
        $(this).addClass('active');
        var context = $(this),
            target = context.data('item');
            $('.header-wrapper').attr('class', 'header-wrapper');
            $('.header-wrapper').addClass(target);
            $('.product-form-menu ul li').removeClass('active');
            $('.product-form-item').removeClass('reveal');
            $('.product-form-item.' + target).addClass('reveal');
            context.addClass('active');
        var section = $(this).text().trim();

        /* uncomment below two lines for #display-banner images */
        //toggleBannerImage(section);
        //showBanner =  true;
        switch(section){
            case "Health":
                utils.loadScript("/static/lp-health/snippets/full_form/js/pincheck.js");
                utils.loadScript("/static/lp-health/snippets/full_form/js/knockout-3.1.0.js",function(){
                    utils.loadScript("/static/lp-health/snippets/full_form/js/knockout.validation.js",function(){
                        utils.loadScript("/static/home/js/health-system.min.js?v1");
                    });
                });
                break;
            case "Travel":
             /*   utils.loadScript("/static/home/js/moment.min.js", function(){
                    utils.loadScript("/static/home/js/pikaday.js", function(){
                        utils.loadScript("/static/home/js/travel-system.js?v1");
                    });
                });*/
                break;
            case "Home":
                utils.loadScript("/static/home/js/home-system.min.js?v1");
                break;
        }

        if(showBanner)
            $("#display-banner").addClass("reveal");
        else
            $("#display-banner").removeClass("reveal");
    });

    //  To validate if entered number is valid
    $('#txtCMobileNo_mid, #txtCMobileNo_top').on("blur", function(){
        var pos = "";
        if($(this).attr("id").indexOf("top")>1){
            pos = "top";
            ContactForm_top.mobileNo =  $(this).val();
        }
        else{
            pos = "mid";
            ContactForm_mid.mobileNo =  $(this).val();
        }
        if(validation.isMobileNumber($(this).val()))
            $(this).parent().find(".error").fadeOut()
        else $(this).parent().find(".error").fadeIn()
        validateForm(pos);
    });

    $("#cbPolicyType_top li, #cbPolicyType_mid li").on("click",function(e){
        var pos = "";
        if($(this).parents(".select-input").attr("id").indexOf("top")>1){
            pos = "top";
            ContactForm_top.policyType =  $(this).text().replace("Insurance","");
        }
        else{
            pos = "mid";
            ContactForm_mid.policyType =  $(this).text().replace("Insurance","");
        }
        validateForm(pos);
    })

    $("#cbPreferredTime_mid li,#cbPreferredTime_top li").on("click", function(){
        var pos = "";
        if($(this).parents(".select-input").attr("id").indexOf("top")>1){
            pos = "top";
            ContactForm_top.day = $(this).text().trim().split(" ")[0];
            ContactForm_top.preferredTime =  $(this).text().trim().split(" ")[1];
            ContactForm_top.preferredTimeValue = $(this).find("small").text().split(" ")[0].substring(1);
        }
        else{
            pos = "mid";
            ContactForm_mid.day = $(this).text().trim().split(" ")[0];
            ContactForm_mid.preferredTime =  $(this).text().trim().split(" ")[1];
            ContactForm_mid.preferredTimeValue = $(this).find("small").text().split(" ")[0].substring(1);
        }
        validateForm(pos);
    });

    $("#cbInsuranceType li").on("click",function(){
        $(this).parents(".form-row").find(".btn").prop("disabled",false);
    })

    var slideTime = 3000;

    function autoClicks(){
        var len = $('.handle').children().length-1;
        var toClick = true;
        var key = 0;
        $.each($('.handle').children(),function(index, row){
            if ($(row).hasClass('active') && toClick){
                key = index>=len? 0 :index+1;
                $(row).removeClass('active');
                $($('.handle').children()[key]).trigger('click');
                toClick = false;
            }
        });
    }

    var slide = setInterval(autoClicks,slideTime);
    $("#divSlider").parent().on("mouseover", function(){
        //stop the interval
        clearInterval(slide);
    });
    $("#divSlider").parent().on("mouseout", function(){
        //and when mouseout start it again
        slide = setInterval(autoClicks,slideTime);
    });

    $('.handle i').on("click",function (e){
        $('.handle').children().removeClass('active');
        $(this).addClass('active');
        var slider = $("#divSlider");

        e.preventDefault();
        e.stopPropagation();

        $.each($('.handle').children(), function(index,row){
            if($(row).hasClass('active')){
                $(slider).find('.prev').removeClass('prev');
                $(slider).find('.active').removeClass('active').addClass('prev');
                $($(slider).children()[index]).addClass('active');
            }
        });
    });

    $("#btnInsuranceType").on("click",function(){
        var type = $("#divInsuranceType").text();
        var url="";
        $.each(linkList,function(index, row){
            if(row.name +" Insurance" == type){
                url = row.link;
                return;
            }
        });

        var _posted = utils.gtm_push("HomeDropDownQuotes",
            "HomePage - Get Quotes",
            $("#divInsuranceType").text(),
            "", {},
            function () {
                window.location.href = url;
            });

        // Fallback for cases where ad block does not allow redirection.
        setTimeout(function(){window.location.href = url;}, 1000)
    });

    $("#btnContactMe_top,#btnContactMe_mid").on("click", function(){
        var pos = "";
        if($(this).attr("id").indexOf('top')>=1)
            pos = "top";
        else pos="mid";

        $("#divContactForm_"+pos).slideUp(500);
        $("#divThankYou_"+pos).slideDown(500);

        createDataForLMS(pos);

        utils.gtm_push("HomeContactMe",
            "HomePage - Contact Me",
            "Contact",
            $("#divSlot").text(), {});
    });

    $(".video-article a").on("click", function(e){
        e.preventDefault();
        e.stopPropagation();
        var url = $(this).attr("href");
        $("#video-frame").addClass("active");
        $("#frameVideo").attr("src",url);
    });
    $("#divCloseFrame").on("click", function(){
        $("#video-frame").removeClass("active");
        $("#frameVideo").attr("src","");
    });

    $(window).on("keyup", function(e){
        var keyCode = e.keyCode ? e.keyCode : e.which;
        if(keyCode==27)
            $("#divCloseFrame").click();
    });

    $(document).on("click", function(e) {
        e.stopPropagation();
        // for custom select input field
        if ($(e.target).closest('.select-input').length==0) {
            if(!$(e.target).hasClass('active')) {
                $(".select-input.active").removeClass("active").find('ul').slideToggle(300);
            }
        }
    });
}

(function init(){

    setPreferredTimeDayLabel("#cbPreferredTime_top");
    setPreferredTimeDayLabel("#cbPreferredTime_mid");

    utils.createMobileField("#txtCMobileNo");
    utils.createMobileField("#txtCMobileNo_top");

    setTimeout(function(){
        utils.createDropDowns('#cbPreferredTime_top','#cbPreferredTime_top ul li');
        utils.createDropDowns('#cbPreferredTime_mid','#cbPreferredTime_mid ul li');
    }, 0)

    utils.createDropDowns('#cbInsuranceType','#cbInsuranceType ul li');
    utils.createDropDowns('#cbPolicyType_top','#cbPolicyType_top ul li');
    utils.createDropDowns('#cbPolicyType_mid','#cbPolicyType_mid ul li');
    registerEvents();

    var _staticUrl = "&utm_campaign=Yahoo-Preroll-Health";

    if(window.location.href.indexOf(_staticUrl)>0)
    {
        //health
        $('#Menu_lis li[data-item="health-product"]').click();
    }
    else if(window['PRODUCT'] == 'term'){
         // opening term tab on /term-insurance
        $('#Menu_lis li[data-item="life-product"]').click();
    }
    else
    {
        //default motor
        $('#Menu_lis li[data-item="car-product"]').click();
    }

    slider2 = cfCarousel('#emp-carousel',5000);
    /*
    // heroes header video js
    $(window).bind('scroll', function () {
        var miniBarHeight = $('#mini-top-bar').outerHeight();
        var videoOpenHeight = $('#cfox-hero-vid').outerHeight() -84;
        if ($(window).scrollTop() > miniBarHeight) {
            $('#cfox-hero-vid-teaser').addClass('reveal');
            $('#cfox-hero-vid .video-section').addClass('fix-top');
        } else {
            $('#cfox-hero-vid-teaser').removeClass('reveal');
            $('#cfox-hero-vid .video-section').removeClass('fix-top');
        }
    });
    */
    $(window).bind('scroll.custom',function(){
     var i_carousel_scroll = $('#i_sec').offset().top - ($(window).height())/1.5;
        if($(window).scrollTop() >= i_carousel_scroll) {
            slider1 = cfCarousel('#i-carousel',3000);
            $(window).off('scroll.custom');
        }
    });
    // health tax banner click logic
    /*
    $("#display-banner a#product-link").click(function(e){
            e.stopPropagation();
        });
    $("#display-banner a#product-tab").click(function(e){
            e.preventDefault();
            e.stopPropagation();
            $('#Menu_lis li')[2].click();
        });
    */

    // 2 of 3 video flipper
        var vids = $("div.flip-item").get().sort(function(){
            return Math.round(Math.random())-0.5; //random so we get the right +/ combo
       }).slice(0,2)
        $(vids).show();
    //banner flipper init
        setInterval(cycleImage, 7000);
    })();
    // banner flipper function
    function cycleImage(){
        var $active = $('#festive-banner .festive-item.active, #festive-small-banner .festive-item.active');
        var $next = ($active.next().length > 0) ? $active.next():
        $('#festive-banner .festive-item:first, #festive-small-banner .festive-item:first');
        $active.removeClass('active');
        $next.addClass('active');
    }


/********* UI Global Control Events **********/
$('select').change(function(){
    var selectedId = $(this).children(":selected").attr("id");
    if(selectedId && (selectedId>0 || selectedId.length>1)){
        $(this).parent().addClass('selected');
        $(this).parent().find('.select2-choice').css({
            'padding-top': ''
        })
        utils.showLabelForInput($(this));
    }
    else{
        $(this).parent().removeClass('selected');
    }
});
$('input[type=number]').on("keydown",function(e){
    var keyCode = e.keyCode ? e.keyCode : e.which;
    if(!utils.isNumberKey(e) || (($(this).val().trim().length == 0 || $(this).val() == 0) && keyCode == 40))
        e.preventDefault();
});

$('input[type=tel], input[type=number], input[type=text]').on("keyup",function(e){
    utils.showLabelForTextField($(this))
});
