var _residency_duration_list = [
    {id:"less_6_month", name:"Less than 6 months", value:"Less than 6 months"},
    {id:"more_6_month", name:"More than 6 months", value:"More than 6 months"}
];
var booleanOptions = [
    {label: 'Yes',value: true},
    {label:'No',value:false}
];
var _regions = [
        {label: "Worldwide",value: "W"},
        {label: "Worldwide excluding US/Canada",value: "W_NO_US_CANADA"}
    ];
var _countryList = [];
var travel_form = {
    _durationValue:"30",
    _isResident:'{"label":"Yes", "value":true}',
    _ntravelers:0,
    _residentFor:"",
    _selectedCountriesSlugs:[],
    _selectedRegion:null,
    _startDate:"",
    _endDate:"",
    _travelerAges: [],
    _tripTypeValue:"SINGLE",
    _tripStartFromIndia:"Yes"
}

//-------------------------------------------------------------

function getResidentValue(value,src){
    // src = get || localStore
    var  ret = "";
    if(src == "get"){
        booleanOptions.forEach(function(row, index){
            if(row.label == value)
                ret = row;
        });
    }
    else{
        if(value){
            ret = JSON.parse(value).label;
        }
    }
    return ret;
}

function getAnnualDestinationValue(value, src){
    var ret="";
    if(src == "get"){
        _regions.forEach(function(row, index){
            if(row.value == value)
                ret = row;
        });
    }
    else{
        if(value){
            ret = value.value;
        }
    }
    return ret;
}

var startPicker, endpicker, annualPicker;
function initPikaday(sDate, eDate, aDate){
    var startDate,
        endDate,
        annualdate,
        currentDate = new Date(),
        maxMonth_start = currentDate.getMonth()+6 >11? currentDate.getMonth()+6 - 11:currentDate.getMonth()+6,
        maxMonth_end = maxMonth_start+2 >11? maxMonth_start+2 - 11:maxMonth_start+2,
        maxYear_start = maxMonth_start<currentDate.getMonth()?currentDate.getFullYear() +1: currentDate.getFullYear(),
        maxYear_end = maxMonth_end<currentDate.getMonth()?currentDate.getFullYear() +1: currentDate.getFullYear(),
        updateStartDate = function() {
            endPicker.setMinDate(startDate);
        },
        updateEndDate = function() {},
        updateAnnualDate = function(){};
        startPicker = new Pikaday({
            field: document.getElementById('trip_start_date'),
            minDate: new Date(),
            format:"DD-MM-YYYY",
            onSelect: function() {
                startDate = this.getDate();
                updateStartDate();
                travel_form._startDate = startPicker.getMoment().format("YYYY-MM-DD");
                $("#trip_start_date").trigger("keyup");
                $("#trip_start_date").prop("readolny", true)
            }
        }),
        endPicker = new Pikaday({
            field: document.getElementById('trip_end_date'),
            minDate: new Date(),
            format:"DD-MM-YYYY",
            onSelect: function() {
                endDate = this.getDate();
                updateEndDate();
                travel_form._endDate = endPicker.getMoment().format("YYYY-MM-DD");
                $("#trip_end_date").trigger("keyup");
            }
        }),
        annualPicker = new Pikaday({
            field: document.getElementById('trip_annual_date'),
            minDate: new Date(),
            format:"DD-MM-YYYY",
            onSelect: function() {
                startDate = this.getDate();
                updateStartDate();
                travel_form._startDate = annualPicker.getMoment().format("YYYY-MM-DD");
                $("#trip_annual_date").trigger("keyup");
            }
        });

        var _startDate = startPicker.getDate(),
            _endDate = endPicker.getDate(),
            _annualDate = annualPicker.getDate();

    if (_startDate) {
        startDate = _startDate;
        updateStartDate();
    }

    if (_endDate) {
        endDate = _endDate;
        updateEndDate();
    }

    if(_annualDate){
        startDate = _annualDate;
        updateAnnualDate();
    }

    if(aDate){
        annualPicker.setDate(adate)
    }
    else{
        if(sDate){
            var d = sDate.split("-");
            startDate = new Date(sDate);
            startPicker.setDate(startDate);
            startPicker.hide();
        }
        if(eDate){
            var d = eDate.split("-");
            endDate = new Date(eDate);
            endPicker.setDate(endDate);
            endPicker.hide();
        }
    }
}

function loadCountryList(callback){
    $.getJSON("static/home/js/country_list.json")
    .success(function(data){
        _countryList = data;
        callback();
    })
    .fail(function(data,error){console.error('err: ', error);});
}

function updateAge(self){
    var index = $(self).attr("id").split('_')[1];
    if(isNaN(index)){
        index = 1;
    }

    if(isNaN($(self).val()))
        $(self).val("");
    travel_form._travelerAges[index-1] = $(self).val().toString();

    if(index == 1){
        $("#traveller_age").val($(self).val());
        $("#traveller_age").trigger("keyup");
        $("#traveller_1_age").val($(self).val());
    }
}

function validateTravelForm(){
    var valid = true;

    var m = validate_age();
    if(m.length>0){
        if(travel_form._tripTypeValue == "SINGLE"){
            valid = false;
            $("#traveller_1_age").parent().parent().next().text(m);
            $("#traveller_1_age").parent().parent().next().fadeIn();
        }
        else{
            $("#traveller_age").parent().next().text(m);
            $("#traveller_age").parent().next().fadeIn();
        }
        valid = false;
    }
    else{
        $("#traveller_age").parent().next().fadeOut();
        $("#traveller_1_age").parent().parent().next().fadeOut();
    }

    if ($("[name=residency_validity]:checked").val().trim().toUpperCase() == "NO" &&
        !$("#residency_duration").val()){
            valid = false;
            $("#residency_duration").parent().next().fadeIn();
    }
    else{
        $("#residency_duration").parent().next().fadeOut();
    }

    if(travel_form._tripTypeValue == "SINGLE"){
        if(!($("#travel_Travelling_To").val() && $("#travel_Travelling_To").val().length>0)){
        $("#travel_Travelling_To").parent().next().fadeIn();
            valid = false;
        }
        else
            $("#travel_Travelling_To").parent().next().fadeOut();

        var _startDate, _endDate;
        if(!($("#trip_start_date").val() && $("#trip_start_date").val().length>0)){
            $("#trip_start_date").parent().next().fadeIn();
            valid = false;
        }
        else{
            $("#trip_start_date").parent().next().fadeOut();
            _startDate = travel_form._startDate
        }

        if(!($("#trip_end_date").val() && $("#trip_end_date").val().length>0)){
            $("#trip_end_date").parent().next().fadeIn();
            valid = false;
        }
        else{
            $("#trip_end_date").parent().next().fadeOut();
            _endDate = travel_form._endDate;
        }

        if(new Date(_startDate) > new Date(_endDate)){
            $("#trip_start_date").parent().next().fadeIn();
            $("#trip_start_date").parent().next().text("Select valid dates.")
            valid = false;
        }

        if(!($("#traveller_count").val() && $("#traveller_count").val().length>0)){
            $("#traveller_count").parent().next().fadeIn();
            valid = false;
        }
        else
            $("#traveller_count").parent().next().fadeOut();
    }
    else{
        if(!($("#travel_annual_countryList").val() && $("#travel_annual_countryList").val().length>0)){
            $("#travel_annual_countryList").parent().next().fadeIn();
            valid = false;
        }
        else
            $("#travel_annual_countryList").parent().next().fadeOut();


        if(!($("#trip_longedst_duration").val() && $("#trip_longedst_duration").val().length>0)){
            $("#trip_longedst_duration").parent().next().fadeIn();
            valid = false;
        }
        else
            $("#trip_longedst_duration").parent().next().fadeOut();

        if(!($("#trip_annual_date").val() && $("#trip_annual_date").val().length>0)){
            $("#trip_annual_date").parent().next().fadeIn();
            valid = false;
        }
        else
            $("#trip_annual_date").parent().next().fadeOut();
    }

    return valid;
}

function validate_age(){
    var message = "";
    var adultAvailable = false;
    var ageMissing = 0;
    var ages = travel_form._travelerAges;
    if(travel_form._tripTypeValue == "SINGLE"){
        for (var i = 0; i <parseInt(travel_form._ntravelers); i++){
            if(ages[i] !== undefined && ages[i].length < 1){
                ageMissing++;
            }
            else if(travel_form._ntravelers == 1){
                if(travel_form._travelerAges[0] >= 1){
                    adultAvailable=true;
                }
            }
            else {
                if(ages[i]>=18){
                    adultAvailable = true;
                }
            }
        }
        if(ageMissing >0){
            if(travel_form._ntravelers == 1)
                return "Please enter traveler’s age";
            else
                return "Please enter age of the " +ageMissing+ " travelers";
        }
        else if(!adultAvailable){
            if(travel_form._ntravelers>1)
                return "Don't send your kids alone in the world. At least have one adult above the age of 18 accompany them.";
            // else
            //     return "Traveler must be atleast 18 years old to buy travel insurance.";
        }
    }
    else{
        if(!ages[0] || (ages[0] && ages[0].length == 0)){
            return "Please enter traveler’s age";
        }
        else if(ages[0]<18){
            return "Traveler must be atleast 18 years old to buy travel insurance.";
        }
    }
    return message;
}

function fetchLocalStorage(){
    if(localStorage && localStorage.travel_form){
        var keys = Object.keys(travel_form);
        var store = {};
        keys.forEach(function(row, index){
            if(row == "_travelerAges" && localStorage[row]){
                try{
                    store[row] = JSON.parse(localStorage[row]);
                }
                catch(e){
                    store[row] = localStorage[row].substring(1, localStorage[row].length-1).split(",");
                }
            }
            else if(row == "_isResident"){
                store[row] = localStorage[row];
            }
            else{
                if(localStorage[row])
                    store[row] = JSON.parse(localStorage[row]);
            }
        });

        $("input[name=trip_validity][value="+store._tripTypeValue.toLowerCase()+"]").prop("checked",true);

        // Set Country List
        $.each($("#travel_Travelling_To"), function(){
            $(this).select2("val", store._selectedCountriesSlugs);
        });

        // Set Start date and End date
        initPikaday(store._startDate, store._endDate);

        // Set traveler Count
        $("#traveller_count").select2().select2("val",store._ntravelers);

        // Set travellers age
        var ages = store._travelerAges;
        for(var i = 0; i< parseInt(store._ntravelers); i++){
            $("#traveller_"+(i+1)+"_age").val(ages[i]);
            $("#traveller_"+(i+1)+"_age").trigger("blur");
        }
        $("#traveller_age").val(store._travelerAges[0]);

        $("#trip_longedst_duration").select2().select2("val", store._durationValue);

        $("#travel_annual_countryList").select2().select2("val", getAnnualDestinationValue(store._selectedRegion), "localStorage");

        // trigger events
        $("#traveller_count").trigger("change");
        $("input[name=trip_validity][value="+store._tripTypeValue.toLowerCase()+"]").trigger("change");
        $("#traveller_age").trigger("keyup");
        $("#trip_longedst_duration").trigger("change");
        $("#travel_Travelling_To").trigger("change");
    }
    else{
        initPikaday();
        $(".trip_single_form").trigger("change");
    }
}

function toggleBtnState(){
    if(($("[name=residency_validity]:checked").val()== "No" &&
        $("[name=oci_validity]:checked").val() == "No") ||
        $("[name=trip_fromIndia_validity]:checked").val() == "No"){
            $("#travel_submit").attr("disabled",true);
    }
    else{
        $("#travel_submit").attr("disabled",false);
    }
}

//-------------------------------------------------------------
// event function

    // Select events
    $("#traveller_count").on("change", function(e){
        var prevCount = travel_form._ntravelers;
        var count  = $(this).val();
        var labelText = '';
        if(count == 1){
            $("#traveller_1_age").parent().prev().text("Your Age");
        }
        else{
            $("#traveller_1_age").parent().prev().text("Traveler Ages (You, Spouse, Kids only)");
        }

        $("#travellersAge").fadeIn().removeClass("hide");
        for(var i=1; i<=6; i++){
            if(i<=count){
                $("#traveller_"+i+"_age").fadeIn().removeClass("hide");
                travel_form._travelerAges[i-1] = $("#traveller_"+i+"_age").val();
            }
            else
                $("#traveller_"+i+"_age").fadeOut(function(){
                    $("#traveller_"+i+"_age").addClass("hide");
                });
        }

        travel_form._ntravelers = count;
    });

    $("#trip_longedst_duration").on("change", function(){
        travel_form._durationValue = $(this).val();
    });

    $("#residency_duration").on("change", function(){
        travel_form._residentFor = $(this).val();
    });

    $("#travel_Travelling_To").on("change", function(){
        travel_form._selectedCountriesSlugs = [];
        if($(this).val())
            $(this).val().forEach(function(row, index){
                travel_form._selectedCountriesSlugs.push(row);
            });
    });

    $("#travel_annual_countryList").on("change", function(){
        travel_form._selectedRegion = getAnnualDestinationValue($(this).val(), "get");
    });

    // Radio button events

    $("input[name=trip_fromIndia_validity]").on("change", function(){
        var self = this;
        travel_form._tripStartFromIndia = $(self).val();
        if($(self).val() == "No"){
            $("#trip_fromIndia_error").slideDown();
        }
        else{
            $("#trip_fromIndia_error").slideUp();
        }
        toggleBtnState();
    });

    $("input[name=residency_validity]").on("change", function(){
        var self = this;
        travel_form._isResident = getResidentValue($(self).val(),"get");

        if($(self).val() == "No"){
            $("#div_oci_section").slideDown();
        }
        else{
            $("#div_oci_section").slideUp();
            $("input[name=oci_validity]:first").trigger("click");
        }
    });

    $("input[name=oci_validity]").on("change", function(){
        var self = this;
        if($(self).val() == "No"){
            $("#traveller_resident_error").slideDown();
            $("#traveller_resident").slideUp();
        }
        else{
            $("#traveller_resident").slideDown();
            $("#traveller_resident_error").slideUp();
        }
        toggleBtnState();
    });

    $("input[name=trip_validity]").on("change", function(e){
        travel_form._tripTypeValue = $(this).val().toUpperCase();
        if($(this).val().toUpperCase() == "Single".toUpperCase()){
            $(".trip_single_form").slideDown(150);
            $(".trip_annual_form").slideUp(150);
            if(travel_form._startDate)
                startPicker.setDate(travel_form._startDate);
            if(travel_form._endDate)
                endPicker.setDate(travel_form._endDate);
            else
                $("#trip_end_date").val("");
        }
        else{
            $(".trip_single_form").slideUp(150);
            $(".trip_annual_form").slideDown(150);
            if(travel_form._startDate)
                annualPicker.setDate(travel_form._startDate);
        }
    });

    $("#travel_submit").on("click", function(e){
        var valid = validateTravelForm();

        if(valid){

            //  MixPanel Events
            //utils.MixPanelTrack(MixPanelEvents.Travel_Home_Product.name, MixPanelEvents.Travel_Home_Product.params);

            localStorage.travel_form = JSON.stringify(travel_form);
            if(travel_form._tripTypeValue == "MULTI"){
                travel_form._ntravelers = 1;
            }
            var temp = travel_form._travelerAges;
            travel_form._travelerAges = [];
            for (var i = 0; i<travel_form._ntravelers; i++)
                travel_form._travelerAges.push(temp[i]);

            var keys = Object.keys(travel_form);
            keys.forEach(function(row, index){
                var val = "";
                if (row == "_isResident"){
                    if(typeof(travel_form[row]) == typeof({}))
                        val = JSON.stringify(travel_form[row]);
                    else val = travel_form[row];
                }
                else{
                    if(!(travel_form._tripTypeValue == "MULTI" && row == "_endDate"))
                        val = JSON.stringify(travel_form[row]);
                }
                localStorage[row] = val;
            });
            window.location.href =  "/travel-insurance/#/results";
        }
    });

    $("#trip_start_date, #trip_start_date").on("keyup",function(){
        if($(this).val().length>0){
            utils.showLabelForTextField($(this));
        }
    });
    

    $("#traveller_age").on("keydown", function(e){
        var keyCode = e.keyCode ? e.keyCode : e.which;
        if(!utils.isNumberKey(e) || (($(this).val().trim().length == 0 || $(this).val() == 0) && keyCode == 40))
            e.preventDefault();
    });

//-------------------------------------------------------------
// init function
function initTravelForm(){
    var travellerCount = [];
    var longestDuration_list = [
        {id:"30days", label:"30 days", value:"30"},
        {id:"45days", label:"45 days", value:"45"},
        {id:"60days", label:"60 days", value:"60"},
    ];

    for (var i = 1; i <=6; i++) {
        travellerCount.push({"id":i, "name":i, "value":i});

        $("#traveller_"+i+"_age").on("keydown", function(e){
            var keyCode = e.keyCode ? e.keyCode : e.which;
            if(!utils.isNumberKey(e) || (($(this).val().trim().length == 0 || $(this).val() == 0) && keyCode == 40))
                e.preventDefault();
        });
    };

    loadCountryList(function(){
        utils.createSelect_new($("#travel_Travelling_To"),_countryList);
        setTimeout(fetchLocalStorage(),0);
    });
    utils.createSelect_new($("#traveller_count"),travellerCount, Infinity);
    utils.createSelect_new($("#trip_longedst_duration"), longestDuration_list, Infinity);
    utils.createSelect_new($("#residency_duration"),_residency_duration_list);
    utils.createSelect_new($("#travel_annual_countryList"),_regions, Infinity);

    $("#s2id_travel_Travelling_To").addClass("cf-special-ip");
    $("#s2id_autogen15").on("blur",function(){
        utils.showLabelForTextField($("#countrySection_singleTrip"));
    });
    $(".radioContainer input").attr("disabled", false);
    if(localStorage && localStorage.travel_form){
        var r = getResidentValue(localStorage._isResident,"localStorage");
        $("input[name=residency_validity][value="+r +"]").prop("checked",true);
        $("input[name=residency_validity][value="+r +"]").trigger("change");
        if(r == "No"){
            $("#residency_duration").select2().select2("val", localStorage._residentFor);
        }
    }
}

(function(){initTravelForm();})();
