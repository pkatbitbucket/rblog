// Global validation regex collection
var LMS_DefaultData ={
    "campaign": "",
    "label": "",
    "triggered_page": "",
    "mobile": "",
    "mode": "assisted",
};
var validation = {
    isEmailAddress: function(str) {
        var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return pattern.test(str);
    },
    isNotEmpty: function(str) {
        var pattern = /\S+/;
        return pattern.test(str);
    },
    isNumber: function(str) {
        var pattern = /^\d+$/;
        return pattern.test(str);
    },
    isMobileNumber: function(str){
        var pattern = /[7-9][\d]{9}/;
        return pattern.test(str);
    },
    isName: function(str) {
        var pattern = /^[a-zA-Z ]{2,30}$/;
        return pattern.test(str);
    }
};

// Used for Lead form - Buy Insurance.
var linkList = [
    {name: 'Car', link:'/motor/car-insurance/'},
    {name: 'Two Wheeler', link:'/motor/twowheeler-insurance/'},
    {name: 'Home', link:'/home-insurance/'},
    {name: 'Travel', link:'/travel-insurance/'},
    {name: 'Health', link:'/health-plan/'},
    {name: 'Term Life', link:'/term-insurance/'},
]

var monthList = [
    {id:"1",name:"January"} ,
    {id:"2",name:"February"} ,
    {id:"3",name:"March"} ,
    {id:"4",name:"April"} ,
    {id:"5",name:"May"} ,
    {id:"6",name:"June"},
    {id:"7",name:"July"} ,
    {id:"8",name:"August"} ,
    {id:"9",name:"September"} ,
    {id:"10",name:"October"} ,
    {id:"11",name:"November"} ,
    {id:"12",name:"December"}];

try{
// Setting default configuration here or you can set through configuration object as seen below
$.fn.select2.defaults = $.extend($.fn.select2.defaults, {
    allowClear: true, // Adds X image to clear select
    closeOnSelect: true, // Only applies to multiple selects. Closes the select upon selection.
    placeholder: 'Select...',
    minimumResultsForSearch: 1 // Removes search when there are 15 or fewer options
});
}catch(ex){}

function utils(){}
// Decrypt Response
utils.parseResponse = function(encrypted) {
    if(encrypted instanceof Object) return encrypted
    try{
        return JSON.parse(encrypted);
    } catch(e){
        var key = CryptoJS.enc.Hex.parse(encrypted.slice(0, 64));
        var iv = CryptoJS.enc.Hex.parse(encrypted.slice(-32));
        var cipher = CryptoJS.lib.CipherParams.create({
            ciphertext: CryptoJS.enc.Base64.parse(encrypted.slice(64, -32))
        });
        var result = CryptoJS.AES.decrypt(cipher, key, {iv: iv, mode: CryptoJS.mode.CFB});
        return JSON.parse(result.toString(CryptoJS.enc.Utf8));
    }
};

utils.getMonthList = function(){
    return monthList.map(function(m){return m.name;});
}

// Get Previous Years
utils.getPastYears = function(){
    var yearArr = [];
    var today = new Date();
    var year = today.getFullYear();

    // Polulate last 14 years.
    for(var i =1; i<15; i++)
        yearArr.push({'id':year - i,'name':year - i, 'value':year - i});

    // You can buy renew policy 60 days in advance. So populate current year if T>= 31st Decc. - 60
    var yearEnd = new Date(year, 11,31);
    yearEnd.setDate(yearEnd.getDate() - 60);
    today.setHours(0,0,0,0);
    yearEnd.setHours(0,0,0,0);

    // Compare date by converting it to integer. date.getTime(). Comparing obj will always give false, due to reference.
    if(+today > +yearEnd){
        yearArr.unshift({'id':year,'name':year, 'value':year})
    }

    return yearArr;
}

utils.getExpiredDateValue = function(){
    var t_1 = new Date();
    var t_90 = new Date();

    t_1.setDate(t_1.getDate() - 1);
    t_90.setDate(t_90.getDate() - 90);

    var month = [], years=[];
    // Overlapping years
    if(t_90.getMonth() > t_1.getMonth()){
        for (var i = t_90.getMonth(); i<=11; i++){
            month.push(monthList[i]);
        }
        for (var i = 0; i<=t_1.getMonth(); i++){
            month.push(monthList[i]);
        }
    }
    else{
        for (var i = t_90.getMonth(); i<=t_1.getMonth(); i++){
            month.push(monthList[i]);
        }
    }

    if(t_1.getFullYear() != t_90.getFullYear()){
        years.push(t_1.getFullYear());
        years.push(t_90.getFullYear());
    }
    else{
        years.push(t_1.getFullYear());
    }

    return {
        minDate: t_90,
        maxDate: t_1,
        month: month,
        years: years
    }
}

utils.getMonthNameToNumber = function(month){
    var _month = null;
    if (month){
        return monthList.find(function(item){
            return (item.name.toLowerCase().trim() == month.toLowerCase().trim())
        }).id -1;
    }else{
        return undefined;
    }
}

utils.getMonthNumberToName = function(month){
    return monthList[month]?monthList[month].name:undefined;
}

// Calls API and gets data
utils.getData = function(url, data,elementID, model, callback, initialValue, isAutoComplete, showSearchBox){
    $.post(url, data,
    function(response) {
        var response = utils.parseResponse(response);
        if(elementID){
            var defaultOption = $(elementID).attr('data-placeholder')
            if(response.data[model] === undefined)
                response.data[model] = {};

            if(isAutoComplete){
                try{
                    utils.createTypeAhead(elementID, response.data[model], defaultOption, initialValue);
                }
                catch(ex){
                    // Fallback for cases where cookie value is available but scripts are not loaded.
                    utils.createSelect(elementID, response.data[model], showSearchBox, initialValue);
                }
            }
            else
                utils.createSelect(elementID, response.data[model], showSearchBox, initialValue);
        }

        if(callback) callback(response.data[model]);
    });
}

utils.getDefaultSelectedValue = function (str){
    return JSON.stringify("{ id:0, value:" + str + "}");
}

utils.isNumberKey = function(e){
    var keyCode = e.keyCode ? e.keyCode : e.which;
    return  ((keyCode<=57 && keyCode != 32) || // Num keys above Qwerty Key and Special keys like backspace and tab
            (keyCode>=96 && keyCode<=105) ||   // Num pad keys
            (keyCode >=112 && keyCode <= 145)) //
}

utils.createSelect = function(element, data, showSearchBox, initialValue){
    var placeHolder = $(element).attr('data-placeholder');
    $(element).html(utils.createOption(data,placeHolder));
    var showBox = Infinity;
    if(showSearchBox === undefined) showBox = 1;
    else showBox = isNaN(showSearchBox)?Infinity:showSearchBox;
    var configParamsObj = {
        minimumResultsForSearch: showBox,
    }

    $(element).select2(configParamsObj);

    $(element).parent().find('.select2-choice').css({
        'padding-top': '13px!important'
    });

    // to hide the search field and focusser field
    if(showSearchBox<0){
        $(element)
            .select2 ('container')
            .find ('.select2-search, .select2-focusser')
            .addClass ('hide') ;
    }

    if(initialValue){
        setTimeout(function(){
            $(element)
                .val(JSON.stringify(initialValue))
                .trigger("change");
            }
        ,100);
    }
}

utils.createSelect_new = function(element, data, showSearchBox, initialValue){
    var placeHolder = $(element).attr('data-placeholder');
    $(element).html(utils.createOption_new(data,placeHolder));
    var showBox = Infinity;
    if(showSearchBox === undefined) showBox = 1;
    else showBox = isNaN(showSearchBox)?Infinity:showSearchBox;
    var configParamsObj = {
        minimumResultsForSearch: showBox
    }
    $(element).select2(configParamsObj);

    $(element).parent().find('.select2-choice').css({
        'padding-top': '13px!important'
    });

    // to hide the search field and focusser field
    if(showSearchBox<0){
        $(element)
            .select2 ('container')
            .find ('.select2-search, .select2-focusser')
            .addClass ('hide') ;
    }

    if(initialValue){
        setTimeout(function(){
            $(element)
                .val(typeof(initialValue)=="object"?JSON.stringify(initialValue): initialValue)
                .trigger("change");
            }
        ,100);
    }
}

// Create selectivity searchable list
utils.createTypeAhead = function(element,data, placeholder, initialValue){
    var el = $(element).autocomplete({
        source: data.map(function(o){
            return {
                key:o.id,
                value:o.name,
                "data-value":JSON.stringify(o)
            }
        }),
        delay:50
    });

    if(initialValue){
        $(element)
            .val(initialValue.name)
            .data('autocomplete')
        if($(element).data('autocomplete'))
            $(element)
                .data('autocomplete')
                ._trigger('select');
    }
}


// Create Option string for select
utils.createOption = function(dataset,defaultValue){
    var optionStr = "<option></option>";
    //"<option id=0 value='" +utils.getDefaultSelectedValue(defaultValue) +"'>"+ defaultValue+" </option>";
    $.each(dataset, function(key, value){
        var id = value.id? value.id:key;
        var text = value.name? value.name:value;
        optionStr += "<option id="+ id + " name='"+ text+ "' value='"+ JSON.stringify(value).trim() + "'>"+ text + "</option>"
    });
    return optionStr;
}


// Create Option string for select. Value is not stringified
utils.createOption_new = function(dataset,defaultValue){
    var optionStr = "<option></option>";
    $.each(dataset, function(key, value){
        var id = value.id? value.id:key;
        var text = value.label? value.label:value.name || value;
        optionStr += "<option id="+ id + " name='"+ text+ "' value='"+ (value.value || value) + "'>"+ text + "</option>"
    });
    return optionStr;
}

// Validate string is JSON for JSON.parse and removes extra quotes
utils.validateJSON = function(str){
    if(str && str.split('{').indexOf>0)
        if(str.split('{')[0].length>0){
            str = utils.validateJSON(str.substring(1,str.length-1));
            return JSON.parse(str)
        }
    return str;
}

// Loads scripts dynamically.
utils.loadScript = function(url, callback){
    var scripts = utils.getLoadedScripts();
    var lastIndex = url.lastIndexOf("/");
    var isLoaded = false;
    scripts.forEach(function(row, index){
        if(row == url.substring(lastIndex+1)){
            isLoaded = true;
            return;
        }
    });
    if(!isLoaded){
        //$(".reveal").addClass("loading");
        // Adding the script tag to the head as suggested before
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;
        script.name="url";

        // Then bind the event to the callback function.
        // There are several events for cross browser compatibility.
        script.onreadystatechange = callback;
        script.onload = callback;

        // Fire the loading
        head.appendChild(script);
    }
};

utils.getLoadedScripts = function(){
    var scriptSrc = [];
    var scripts = $('script').filter(function () {
            return $(this).attr('src')
        });
    $.each(scripts,function(index, row){
        var lastIndex = row.src.lastIndexOf("/");
        scriptSrc.push(row.src.substring(lastIndex + 1));
    });
    return scriptSrc;
}

utils.createMobileField = function(control){
    $(control).on("keydown",function(e){
        var keyCode = e.keyCode ? e.keyCode : e.which;
        if(!utils.isNumberKey(e))
            e.preventDefault();
    });

    $(control).on("blur",function(){
        if(isNaN($(this).val())){
            $(this).val("");
        }
    });
}

utils.createDropDowns = function(parent, child){
    $(parent).on("click", function(e){
        $(parent).find('ul').slideToggle(300);
        if($(parent).hasClass('active')){
            $(parent).removeClass('active')
        }
        else{
            // Close any custom dropdown, if open
            $(".select-input.active").find("ul").click();
            $(parent).addClass('active')
        }
        e.preventDefault();
        e.stopPropagation();
    })
    $(child).on("click",function(e){
        var isFirefox = typeof InstallTrigger !== 'undefined';
        var isIE = false || !!document.documentMode;
        var p = $(child).parent();
        $(p).find('active').removeClass('active');
        //$(child).addClass('active');
        $(p).slideToggle(300);

        if($(this)[0].textContent)
            $($(parent).children()[1]).text($(this)[0].textContent.trim().split("\n")[0]);
        else
            $($(parent).children()[1]).text($(this)[0].innerText.split("\n")[0]);

        $(parent).hasClass('active')? $(parent).removeClass('active') : $(parent).addClass('active')
        e.stopPropagation();
    });
}

utils.showLabelForInput = function(self){
    if($("option:selected",self).val() && $("option:selected",self).val().length>0){
        $(self).parent().addClass('selected')
    }
    else{
        $(self).parent().removeClass('selected');
    }
}
utils.showLabelForTextField = function(self){
    var label = $(self).parents('.cf-special-ip').find('label');
    $(self).parent().addClass('selected');
    if(label.length ==0 && $(self).parent().hasClass('input-mobile')){
        label = $(self).parent().prev();
    }

    if(!$(self).val() || ($(self).val() && $(self).val().trim().length === 0)){
        $(label).fadeOut(400,function(){
            $(self).parents('.cf-special-ip').removeClass("selected");
        });
    }
    else{
        $(self)
            .parents('.cf-special-ip')
            .addClass("selected")

        if(!$(self).parents('.cf-special-ip').find("label").is(":visible")){
            $(self)
                .parents('.cf-special-ip')
                .find("label")
                .fadeIn();
        }
    }
}

utils.gtm_push = function(event, category, action, label, params, callback){
    var data = {
        "event":event,
        "category":category,
        "action":action,
        "label":label,
    };
    for(param in params){
        data[param] = params[param]
    }
    if(callback) {
        data.eventCallback= callback;
    }

    // For cases where GTM doesn't respond, redirect after a second.
    setTimeout(callback, 1000);
    if(window.dataLayer){
        dataLayer.push(data);
    }
};

utils.MixPanelTrack = function(event,param){
     // mixpanel.track(event, param);
};