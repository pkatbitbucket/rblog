function youTubeEvents(element, readyStateCallback){
    var video_track = '';
    var threshold = 0.25;
    var state = "";
    var player;

    // initialize events
    function onYouTubeIframeAPIReady() {
        player = new YT.Player(element, {
            videoId: '7VxoVVdzkE8',
            playerVars:{rel:0},
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });
    }

    // Call init once YouTube API JS is loaded
    var youtube_Init = setInterval(function(){
        if(window.YT){
            onYouTubeIframeAPIReady();
            clearInterval(youtube_Init)
        }

    },1000);

    // Make player visible once it is ready
    function onPlayerReady(event) {
        //$("#"+element).fadeIn(300);
        event.target.playVideo();
        readyStateCallback();
    }

    function onPlayerStateChange(event) {  
        var scroll = "";
        var winScroll = $(window).scrollTop();
            scroll = winScroll>45? "Scrolled ":"";    
        var playerState = player.getPlayerState(),
            title = player.getVideoData().title;
        var video_duration = player.getDuration();

        switch(playerState){
            case -1:
                state = "Unstarted";
                break;
            case 0: 
                state="Ended";
                clearInterval(video_track)
                break;
            case 1: 
                state = "Playing";
                video_track = setInterval(function(){trackVideoStatus()},1000);
                break;
            case 2: 
                state = "Paused";
                clearInterval(video_track)
                break;
            case 3: 
                state = "Buffering";
                break;
            case 5: 
                state = "Video cued";
                clearInterval(video_track)
                break;
        }

        // Push data to GTM on Unstarted/ Ended state
        if(playerState==-1 || playerState == 0){
            utils.gtm_push(
                "HomepageTopVideo", 
                "Home Page", 
                "Top Video " + scroll + state,
                title,
                {}
            )    
        }
    }

    // Track video progress and push it in GTM
    function trackVideoStatus(event){
        var scroll = "";
        var winScroll = $(window).scrollTop();
            scroll = winScroll>45? "Scrolled ":"";    
        var video_duration = player.getDuration(),
            currentTime = player.getCurrentTime(),
            title = player.getVideoData().title;

        if (video_duration * threshold < currentTime){
            utils.gtm_push(
                "HomepageTopVideo", 
                "Home Page", 
                "Top Video " + scroll + state,
                title + ' - Viewed ' + threshold*100 + '%' ,
                {}
            );
            threshold += 0.25;
        }
    };

    //
    function stopVideo() {
        player.stopVideo();
    }
}