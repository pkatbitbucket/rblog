module.exports = {
	entry: {
		'bike-system': "./js/bike-system.js",
		'car-system': "./js/car-system.js",
		'health-system':"./js/health-system.js",
		'home-system': "./js/home-system.js",
		'travel-system': "./js/travel-system.js",
	},
	output: {
		path: "./js/",
		filename: "[name].min.js"
	}
};