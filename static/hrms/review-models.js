function ReviewModel(data) {
    var self = this;
    self.review_users = [];
    var relation_mapping = {
        'self': 'Self',
        'peers': 'Peer',
        'bosses': 'Boss',
        'reporters': 'Reporter'
    };
    data.self = [data.self];
    for(var relation_type in relation_mapping) {
        var related_users = data[relation_type];
        var relation = relation_mapping[relation_type];
        for(var i=0; i<related_users.length; i++) {
            var user = related_users[i];
            user.relation = relation;
            user = new ReviewedUser(user);
            self.review_users.push(user);
            if(relation_type == 'self') {
                self.reviewer = user;
            }
        }
    }

    self.current_tab = ko.observable(self.review_users[0]);

    self.activate_tab = function(user) {
        self.current_tab(user);
    };
}

function ReviewedUser(data) {
    var self = this;
    self.id = data.id;
    self.name = data.name;
    self.title = data.title;
    self.reviewer_relation = data.relation;
    self.done = data.done;
    self.sections = ko.utils.arrayMap(data.sections, function(section) {
        return new Section(section);
    });
    self.review_status = ko.observable();
    self.save_review = function() {
        var save_url = '/reviews/ajax/save-review-data/user/' + self.id + '/';
        var review_data = [];
        for(var i=0; i<self.sections.length; i++) {
            var section = self.sections[i];
            for(var j=0; j<section.questions.length; j++) {
                var question = section.questions[j];
                var response_id = question.response() ? question.response().id : null;
                review_data.push({
                    question_id: question.id,
                    section_id: section.id,
                    response_id: response_id,
                    dynamic: section.dynamic
                });
            }
        }

        console.log("SAVED_DATA", review_data);
        $.post(save_url, {'data': JSON.stringify(review_data)})
            .done(function(res) {                                
                if(res.error){ 
                    self.review_status('fields-missing');
                }
                else{
                    self.review_status('success');
                }
            })
            .fail(function() {
                self.review_status('failed');               
            })
            .always(function() {                
                setTimeout(function() {
                    self.review_status(null);
                }, 3000);
            });
    };
}

function Section(data) {
    var self = this;
    self.id = data.id;
    self.name = data.name;
    self.dynamic = data.dynamic;
    self.questions = ko.utils.arrayMap(data.questions, function(question) {
        return new Question(question);
    });
}

function Question(data) {
    var self = this;
    self.id = data.id;
    self.text = data.text;
    self.response = ko.observable();
    self.choices = ko.utils.arrayMap(data.choices, function(choice) {
        var question_choice = new QuestionChoices(choice);
        if(question_choice.id == data.answer) {
            self.response(question_choice);
        }
        return question_choice;
    });
    self.choose_response = function(choice) {
        self.response(choice);
    };
}

function QuestionChoices(data) {
    var self = this;
    self.id = data.id;
    self.text = data.text;
}
