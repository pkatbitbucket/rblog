/// <reference path="./typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')
import CommonUtils = require('./common/utils/CommonUtils')
import TermLifeResults = require('./life-product/Results')

CommonUtils.loadCustomUtils()

ReactDOM.render(<TermLifeResults />, document.getElementById('life-results'))