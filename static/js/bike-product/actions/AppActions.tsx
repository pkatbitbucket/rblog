import BaseActions = require('../../common/BaseActions')

class Action{
	
	receiveVehicleMakes(callback: Function,value: any) {
		return { value: value, callback: callback }
	}

	fetchVehicleModels(makeId: number){
		return { makeId : makeId}
	}

	receiveVehicleModels(value: any) {
		return { value: value }
	}

	fetchVehicleVariants(modelId: number) {
		return { modelId: modelId }
	}

	receiveVehicleVariants(value: any) {
		return { value: value }
	}

	receiveVehicleInfo(value, callback?:Function){
		return { value: value, callback: callback }
	}

	fetchVehicleMakes(callback?: Function) {
		return {callback: callback}
	}

	fetchRTOs(){
		return
	}

	selectMake(value, isFetchModels, callback?: Function){
		return { value: value, isFetchModels:isFetchModels, callback: callback }
	}

	selectModel(value, isFetchVariants, callback?:Function){
		return { value: value, isFetchVariants:isFetchVariants, callback: callback }
	}

	selectVariant(value, callback?:Function){
		return { value: value, callback: callback }
	}

	isNewVehicle(callback?:Function){
		return {callback: callback}
	}

	clearVehicleInfo(callback?:Function){
		return {callback: callback}
	}
	
	vehicleInfoFetchFailed(data:any, callback?:Function){
		return {data:data, callback: callback}
	}

	setExpired(value){
		return value
	}
	
	setExpiryDate(value){
		return value
	}

	setNCBClaimsMade(value){
		return value
	}

	setPreviousNCB(value){
		return value
	}

	receiveRTOs(value){
		return { value : value }
	}

	selectRTO(value, callback?:Function){
		return { value: value, callback: callback }
	}

	setRegistrationNumber(regNoArray: string[]){
		return {regNoArray: regNoArray}
	}

	fetchVehicleInfo(callback?:Function){
		return {callback: callback}
	}

	fetchPlans() {
		return
	}

	fetchQuote(fetchPlanAfterward?: boolean, sendFastLaneFeedback?:boolean) {
		return { fetchPlanAfterward: fetchPlanAfterward, sendFastLaneFeedback: sendFastLaneFeedback }
	}

	fetchForInsurer(insurerSlug: string){
		return insurerSlug
	}

	getInsurers() {
		return
	}

	receiveInsurers(data: any) {
		return { value: data}
	}

	receiveQuote(quoteId:string){
		return quoteId
	}

	receivePlans(plans:any[], finished:boolean){
		return { plans: plans, finished: finished }
	}

	receiveForInsurer(plan: any, insurerSlug: string, errorCode:string, errorMsg:string) {
		return { plan: plan,insurerSlug: insurerSlug, errorCode:errorCode, errorMsg:errorMsg}
	}

	abortAsync(){
		return
	}

	setResultParams(quoteId: string, zeroDep: boolean, idv: number){
		return { quoteId: quoteId, zeroDep: zeroDep, idv: idv }
	}

	setDepreciationWaiver(flag: boolean){
		return flag
	}

	setIDV(value: number){
		return value
	}

	setTenure(value: number){
		return value
	}

	buyPlan(slug: string) {
		return slug
	}

	invalidateQuoteId(isEmit: boolean){
		return {isEmit: isEmit}
	}

	receiveProposal(transactionId: string){
		return {transactionId : transactionId}
	}
	setRegistrationDate(date:string, month:string, year:string){
		return {date:date, month:month, year:year}
	}

	fetchProposalJSON(insurer: string, transactionId: string) {
		return { insurer: insurer, transactionId: transactionId }
	}

	receiveProposalJSON(data: any){
		return {data: data}
	}

	postProposalData(data:any){
		return {data:data}
	}

	setTransactionParams(slug:string, id: string){
		return {slug:slug, id: id}
	}

	//expects date as moment object
	fetchUpdatedTransactionWithSubmit(data:any){
		return {data:data}
	}

	makePayment(){
		return
	}

	fetchQuoteParams(quoteId:string){
		return {quoteId:quoteId}
	}

	receiveQuoteParams(data:any){
		return {data:data}
	}

	shareQuotes(quoteId:string, url:string, shareType:string, value:string){
		return { quoteId:quoteId, url:url, shareType: shareType, value:value}
	}

	receiveShortURL(shortUrl:string){
		return {shortUrl : shortUrl}
	}

	receiveUpdatedTransactionWithSubmit(data:any){
		return { data : data}
	}

	setFlowType(type:number, callback?:Function){
		return {type:type, callback:callback}
	}

	receivePaymentReponse(data:any, path:string){
		return {data:data, path:path}
	}
	redirectToGateWay(){
		return
	}
	acceptNewProposalPremium(){
		return
	}
	resetProposalPremiumDiff(){
		return
	}
	resetProposalData(){
		return
	}
	pushLeadsToLMS(label: string, offlineCase?: boolean){
		return {label: label, offlineCase: offlineCase}
	}
	callMe(){
		return
	}
	clearModelList(){
		return
	}
	clearVariantList(){
		return
	}
}

export = BaseActions.createAction(Action)
