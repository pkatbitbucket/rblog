import React = require('react')
import BaseComponent = require('../common/BaseComponent')
import BikeQuoteForm = require('./components/BikeQuoteForm')
import BikeFallBackQuoteForm = require('./components/BikeFallBackQuoteForm')
import stashable = require('../common/utils/stashable')

class Bike extends React.Component<any, any>{
	refs: any;
	constructor(props){
		super(props)
		this.state = {stage: 1}
	}

	componentDidMount(){
		stashable.show()
	}

	handleStageChange(changeValue:number = 1){
		this.setState({ stage: this.state.stage + changeValue })
	}

	render() {
		return (
			<div className='bike_product'>
				{ this.state.stage == 1 ? <BikeQuoteForm onChangeStage={this.handleStageChange.bind(this)}/> : null }
				{ this.state.stage == 2 ? <BikeFallBackQuoteForm onChangeStage={this.handleStageChange.bind(this)}/> : null }
			</div>
		)
	}
}

export = Bike