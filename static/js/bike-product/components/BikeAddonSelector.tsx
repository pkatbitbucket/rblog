import React = require('react')
import ReactDOM = require('react-dom')
import ButtonCheckBox = require('../../common/new-components/ButtonCheckbox')
import MultiSelect = require('../../common/react-component/MultiSelect')
import Currency = require('../../common/new-components/CurrencyFormatter')

interface IState {
}

interface IProps {
	idvOptions?: any,
	selectedIDV?:number,
	onIDVChange?:Function,
	tenureOptions?:any,
	zeroDepOptions: any,
	selectedTenure?:number,
	isZeroDepAvailable?:boolean,
	onTenureChange?:Function,
	isZeroDepsSelected?:boolean,
	onZeroDepsChange?:Function,
	maxIDV?:number
}

class BikeAddonSelector extends React.Component<IProps, IState>{
	constructor(props: IProps) {
		super(props)
	}

	handleIDVChange(option: any) {
		if (this.props.selectedIDV != option.value){
			if (this.props.onIDVChange) {
				this.props.onIDVChange(option.value)
			}
		}
	}

	handleTenureChange(option: any) {
		if (this.props.selectedTenure != option.value) {
			if (this.props.onTenureChange) {
				this.props.onTenureChange(option.value)
			}
		}
	}
	handleZeroDepsChange(option: any) {
		if (this.props.onZeroDepsChange) {
			this.props.onZeroDepsChange(option.value)
		}
	}
	
	render() {
		
		var idvOptionsRenderer = function(data : any, configObject : any){
			if(data.value == 0 || data.value == this.props.maxIDV)
				return <span>{data.key}</span> 
			else
				return <Currency value={data.value}/>
		}

		var IDVDesc = <span>Insured Declared Value is that calculated value of your bike, which would be reimbursed to you in case of theft or total damage. This is independent of the limit of accidental repairs or the resale value of the bike. The default IDV is suggested unless you live in a theft prone area or drive excessively (or recklessly) on highways!</span>
		return (
			<div className='bike-addon-selector'>
				{/* Zero Dep will also be a multiselect dropdown 
				<ButtonCheckBox label="Zero Dep" tabindex={0} onChange={this.handleZeroDepsChange.bind(this)} isChecked={this.props.isZeroDepsSelected}/> */}
				{
					this.props.isZeroDepAvailable == true ?
					<MultiSelect customClass='single_row_dd bike_addon bike_addon--zero-dep' header="Zero Dep" footer={<span>Whenever there is a claim settlement, an extra amount is supposed to be paid depending on the standard depreciation logic applied on parts involved in the repair, based on how old your bike is. <br /> If you do not want to pay anything in case of simple repairs, then you should opt for Zero Depreciation.</span>}
						placeHolder="Change Zero Dep" labelText="Zero Dep" dataSource={this.props.zeroDepOptions} 
						onChange={this.handleZeroDepsChange.bind(this) } values={this.props.isZeroDepsSelected} isSearchable={false}
						selectedElementClass="active"/>
						:null
				}
				
				<MultiSelect customClass='single_row_dd bike_addon bike_addon--idv' header="CHANGE IDV"
					footer= {IDVDesc}
					placeHolder="Change IDV"  labelText="IDV" dataSource={this.props.idvOptions} 
					onChange={this.handleIDVChange.bind(this) } values={this.props.selectedIDV} isSearchable={false}
					selectedElementClass="active" optionsRenderer={idvOptionsRenderer.bind(this)}/>
				{/*<MultiSelect header="CHANGE TENURE" footer="Some insurers allow you to buy a policy for upto 3 years at once." placeHolder="Change Tenure"  labelText="TENURE" 
									dataSource={this.props.tenureOptions}  onChange={this.handleTenureChange.bind(this) } values={this.props.selectedTenure} 
									isSearchable={false}/>*/}
			</div>
		);
	}
}


export = BikeAddonSelector