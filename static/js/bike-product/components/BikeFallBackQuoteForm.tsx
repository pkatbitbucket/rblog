import React = require('react')
import moment = require('moment')
import BaseComponent = require('../../common/BaseComponent')
import AppActions = require('../actions/AppActions')
import TransitionUtils = require('../../common/utils/TransitionUtils')
import GTMEventFuns = require('../utils/GTMEventFuns')

import VehicleStore = require('../stores/VehicleStore')
import RegistryStore = require('../stores/RegistryStore')
import PolicyStore = require('../stores/PolicyStore')
import ResultStore = require('../stores/ResultStore')

import ExpiryOptions = require('../../common/constants/ExpiryOptions')
import Button = require('../../common/react-component/Button')
import RTOSelector = require('../../common/new-components/RTOSelector')
import RadioInput = require('../../common/new-components/RadioInput')
import Modal = require('../../common/new-components/Modal')
import LRCard = require('../../common/new-components/LRCard')
import BikeSearchBox = require('./BikeSearchBox')
import URLS = require('../../common/constants/URLS')

const STORES = [VehicleStore, RegistryStore, PolicyStore]
const BIKE_ERROR_MSG = 'Oh, we can’t proceed without your bike details. Do your bit?'
interface IQuoteFormState {
	make?: string;
	model?: string;
	variant?: string;
	regYear?: string;
	rto?: string;
	isPolicyExpired?: boolean;
	openModal?: string;
	rtoList?: any[];
	isValid?: boolean;
	validationMsg?: string;
	quoteId?: string;
	flowType:number;
	isLoading?:boolean;
	bikeMsg?:string;
	rtoMsg?:string;
	expiryMsg?:string;
	makeList: any[];
	modelList: any[];
	variantList: any[];
}

function isNull(value){
	return value == undefined || value == ""
}

function getStateFromStores(): any {
	return {
		make:VehicleStore.getSelectedMakeVerbose(),
		model:VehicleStore.getSelectedModelVerbose(),
		variant:VehicleStore.getSelectedVariantVerbose(),
		regYear:RegistryStore.getRegistrationYear(),
		rto:RegistryStore.getSelectedRTO(),
		isPolicyExpired:PolicyStore.isExpired(),
		rtoList:RegistryStore.getRTOList(),
		quoteId: ResultStore.getQuoteId(),
		flowType: ResultStore.getFlowType(),
		makeList: VehicleStore.getMakes(),
		modelList: VehicleStore.getModels(),
		variantList: VehicleStore.getVariants()
	}
}

class BikeFallBackQuoteForm extends BaseComponent {
	refs: any
	state: IQuoteFormState
	rtoList:any
	yearList:number[]
	goToResults: boolean
	constructor(props) {
		super(props, {
			stores: STORES,
			getStateFromStores: getStateFromStores
		})
	}
	componentDidMount(){
		this.goToResults = false
		if(this.state.flowType == undefined || this.state.flowType == 0){
			this.state.flowType == 2
			AppActions.setFlowType(2)
		}

		if(this.state.flowType == 1)
			//this.openModal('bike-selector')
			setTimeout(()=>{this.openModal('bike-selector')}, 500)
	}
	componentDidUpdate(){
		if(this.goToResults && this.state.quoteId){
			this.goToResults = false
			//setTimeout(() => { window['_ENTRY_MODULE_'] === 'CMS' ? TransitionUtils.jumpTo(URLS.BIKE_RESULTS + '/' + this.state.quoteId) : TransitionUtils.transitTo(URLS.BIKE_RESULTS + '/' + this.state.quoteId) }, 0)
			setTimeout(() => { TransitionUtils.transitTo(URLS.BIKE_RESULTS + '/' + this.state.quoteId) }, 0)
		}
	}
	componentWillMount(){
		AppActions.fetchRTOs()
		AppActions.fetchVehicleMakes()
	}
	handlePolicyExpiryChange(){
		var isValid = this.refs["PolicyExpiryControl"].isValid(true)
		if(isValid){
			var value = this.refs["PolicyExpiryControl"].getValue()
			AppActions.setExpired(value)
			this.openModal('')
			GTMEventFuns.continueAfterPolicyExpired(value)
			this.setState({
				expiryMsg:''
			})
		}
	}
	handleBuyNow(){
		var flowType = ResultStore.getFlowType()
		this.goToResults = this.isValid(true)
		//AppActions.sendFastlaneFeedback()


		if(this.goToResults){
			GTMEventFuns.onBuy()	// GTM event
			this.setState({isLoading:true})
			if(flowType == 2){
				AppActions.invalidateQuoteId(true)
				AppActions.fetchQuote(true, false) // do not send fast lane feedback as user has not come through fast lane
			}
			else {
				AppActions.invalidateQuoteId(true)
				AppActions.fetchQuote(true, true) // send fastlane feedback
			}
		}
	}

	handleRegNo(edit?:boolean){
		if(edit===true){
			GTMEventFuns.editQuoteFormBikeReg()
		}else{
			GTMEventFuns.knowBikeReg()
		}

		AppActions.setFlowType(0, () => {
			//TransitionUtils.transitTo(URLS.BIKE_LANDING)
			this.props.onChangeStage(-1)
		})
		
	}
	handleMakeChange(make:number){
		//AppActions.selectMake(make, true)
		AppActions.fetchVehicleModels(make)
	}
	handleModelChange(model:number){
		//AppActions.selectModel(model, true)
		AppActions.fetchVehicleVariants(model)
	}
	handleVariantChange(variant:string) {
		//AppActions.selectVariant(variant)
	}
	handleRegYearChange(regYear:string){
		//var currentMonth = moment().month() + 1
		//AppActions.setRegistrationDate("1", currentMonth.toString(), regYear)
	}
	handleBikeSelectionComplete(makeId, modelId, variantId, regYear){
		AppActions.selectMake(makeId, false)
		AppActions.selectModel(modelId, false)
		AppActions.selectVariant(variantId)
		if(regYear){
			var currentMonth = moment().month() + 1
			AppActions.setRegistrationDate("1", currentMonth.toString(), regYear)
		}

		GTMEventFuns.continueAfterEditBikeDetails()

		// if(this.state.flowType == 2)
		// 	this.openModal('rto-selector')
		// else{
		// 	this.openModal('expiry-selector')
		// }

		this.setState({
			bikeMsg:''
		})
	}

	handleBikeSearchBack(step:number){
		if(step == 2){
			this.setState({
				modelList: [],
				variantList:[]
			})
			AppActions.clearModelList()
			AppActions.clearVariantList()
		}
		if(step == 3){
			this.setState({
				variantList:[]
			})
			AppActions.clearVariantList()
		}
	}

	handleBikeSearchBoxClose(isCloseButtonPressed:boolean = false){
		if(isCloseButtonPressed){
			this.openModal('')
				this.setState({
					modelList:[],
					variantList:[]
				})
			AppActions.clearModelList()
			AppActions.clearVariantList()
		}
	}

	handleRTOSelect(){
		var isValid = this.refs["RTOSelectorControl"].isValid(true)
		if(isValid){
			var rto = this.refs["RTOSelectorControl"].getValue()
			AppActions.selectRTO(rto)
			this.setState({
				rtoMsg:''
			})
			this.openModal("")
			//this.openModal("expiry-selector")
		}
	}

	handleRTOChange(value:string, isCityPresent:boolean){
		if(isCityPresent){
			AppActions.selectRTO(value)
			this.setState({
				rtoMsg:''
			})
			this.openModal("")
			//this.openModal("expiry-selector")

			GTMEventFuns.continueAfterRTO()
		}
	}

	openModal(modal:string){
		if(this.state.flowType != 2 && modal == 'rto-selector')
			return
		if(modal == 'bike-selector'){
			AppActions.clearModelList()
			AppActions.clearVariantList()
			this.setState({
				modelList: [],
				variantList: []
			})
		}

		this.setState({
			openModal: modal
		})
	}

	public isValid(isShowErrors:boolean = false){
		var isValid = true
		var bikeMsg = ""
		var rtoMsg = ""
		var expiryMsg = ""
		/*if(!this.refs["BikeSearchBox"].isValid(true)){
			this.openModal('bike-selector')
			isValid = false
		}
		else if(!this.refs['RTOSelectorControl'].isValid(true)){
			this.openModal('rto-selector')
			isValid = false
		}
		else if(!this.refs['PolicyExpiryControl'].isValid(true)){
			this.openModal('expiry-selector')
			isValid = false
		}*/
		if(this.state.make == undefined || this.state.make == ""){
			isValid = false
			bikeMsg = BIKE_ERROR_MSG
		} else if (this.state.model == undefined || this.state.model == ""){
			isValid = false
			bikeMsg = BIKE_ERROR_MSG
		} else if (this.state.variant == undefined || this.state.variant == ""){
			isValid = false
			bikeMsg = BIKE_ERROR_MSG
		} else if (this.state.regYear == undefined || this.state.regYear == ""){
			isValid = false
			bikeMsg = BIKE_ERROR_MSG
		} else if (this.state.rto == undefined || this.state.rto == ""){
			isValid = false
			rtoMsg = 'Oh, we can’t proceed without knowing the RTO. Do your bit?'
		} else if (this.state.isPolicyExpired == undefined){
			isValid = false
			expiryMsg = 'Select whether your policy has expired, please?'
		}

		if(isShowErrors)
			this.setState({ 
				isValid: isValid, 
				bikeMsg: bikeMsg,
				rtoMsg:rtoMsg,
				expiryMsg:expiryMsg
			})
		return isValid
	}

	render() {
		var fastLaneStatus = VehicleStore.getFastlaneSuccessStatus()
		if(!this.yearList || this.yearList.length === 0){
			this.yearList = RegistryStore.getYears()
		}

		var rtoName = RegistryStore.getSelectedRTOVerbose()
		var rtoId
		var currRTOState = this.state.rto.split('-')[0]
		var currRTOCode = this.state.rto.split('-')[1]
		if(!rtoName && this.state.rto){
			for(var i = 0; i < this.state.rtoList.length; i++){
				rtoId = this.state.rtoList[i].id.split('-')
				if(rtoId[0] == currRTOState && (rtoId[1] == ("0" + currRTOCode).slice(-2) || rtoId[1] == currRTOCode)){
					rtoName = this.state.rtoList[i].name
					break
				}
				// special case of DL-12S etc in DL. in the RTO list we have DL-12 and not 12C. Hence matching as below
				else if(rtoId[0] == "DL" && currRTOState == "DL" && rtoId[1] == currRTOCode.substring(0,2)){
					rtoName = this.state.rtoList[i].name
					break
				}
				//handling case if user enters GJ-05 then it should match with GJ 5
				else if(rtoId[0] == "GJ" && currRTOState == "GJ" && rtoId[1] == currRTOCode.substring(1,2) && currRTOCode.substring(0,1) == "0"){
					rtoName = this.state.rtoList[i].name
					break
				}
			}
			
		}

		var selectBikeText = this.state.make && this.state.model && this.state.variant && this.state.regYear? 
								//this.state.regYear + " " + this.state.make + " " + this.state.model + " " + this.state.variant 
								<div>
									<div>{this.state.regYear + " " + this.state.make + " " + this.state.model}</div>
									<div className="small">{this.state.variant}</div>
								</div>
								:"Which bike do you ride?"


		var selectBikeRTOText = rtoName ? 
									<div>
										<div>{rtoName.split("-")[0] + "-" + rtoName.split("-")[1]}</div>
										<div className="small">{rtoName.split("-")[2]}</div>
									</div>
								
								:"Bike registration RTO"

		var expiryText = this.state.isPolicyExpired != undefined ?
						 (this.state.isPolicyExpired == true ? 'Yes' : "No")
						 : "Previous policy expired?" 
		var heightClass = this.state.regYear ||this.state.rto  ? ' fill_height' : ''

		// this should be a component. We need an icon component. So we can use <Icon type='down_arrow' />
		var downArrow = <span className='icon--down_arrow' />
		var plusAdd = <span className='icon--add_plus'>+</span>
		return (
			<div>
				<div className='form-row first'>
					<div className="form-field-fallback">
						<div className="bike_make_name">
							<LRCard customClass={'quote-form-item first ' + (this.state.regYear ? '' : 'is_empty') + heightClass} left={selectBikeText} right={plusAdd}
							 onClick={this.openModal.bind(this, 'bike-selector') } label="Your Bike"/>
						{
							this.state.isValid == false && this.state.bikeMsg != "" ?
							<div className={'error-label'}>{this.state.bikeMsg}</div>
							:null
						}
						</div>
						<div className="bike_reg_city_name">
							<LRCard customClass={'quote-form-item ' + (this.state.rto ? '' : 'is_empty') + heightClass} left={selectBikeRTOText} right={plusAdd} 
							onClick={this.openModal.bind(this, 'rto-selector') } label="Registration RTO"/>
						{
							this.state.isValid == false && this.state.rtoMsg != "" ?
							<div className={'error-label'}>{this.state.rtoMsg}</div>
							:null
						}
						</div>
						<div className="bike_policy_exp_date">
						<LRCard customClass={'quote-form-item last ' + (this.state.isPolicyExpired  != undefined? ''  : 'is_empty') + heightClass} left={expiryText} 
							right={downArrow} onClick={this.openModal.bind(this, 'expiry-selector') } label="Previous Policy Expired?"/>
						{
							this.state.isValid == false && this.state.expiryMsg != "" ?
							<div className={'error-label'}>{this.state.expiryMsg}</div>
							:null
						}
						{
							this.state.openModal === "expiry-selector" ?
							<Modal type="popup" isOpen={this.state.openModal === "expiry-selector"} isCloseOnOutSideClick={true} 
								isShowCloseIcon={false} size="large" onClose={this.openModal.bind(this, '')} header="Is your policy expired?" customHeaderClass="popup-header"
								isPreventScrolling={false} footer="Stress is for larger problems, not expired policies. Let us help you renew online instantly." customFooterClass="modal_footer">
								<RadioInput ref="PolicyExpiryControl" optionsList={ExpiryOptions} value={this.state.isPolicyExpired} 
									 validations={['required']} customClass="bike_expiry_selector" onChange={this.handlePolicyExpiryChange.bind(this)}/>
							</Modal>
							:null
						}
							
						</div>
						<div className='fallback_links'>
							{
							/*fastLaneStatus != 0 ?
								<Button customClass={'w--button--link'} title="Wait. I know my bike number."  onClick={this.handleRegNo.bind(this,false) }/>
								: <Button customClass={'w--button--link'} title="I want to edit my bike number."  onClick={this.handleRegNo.bind(this,true) }/>*/
								this.state.flowType == 2 ?
								<Button customClass={'w--button--link'} title="Wait. I know my bike number."  onClick={this.handleRegNo.bind(this,false) }/>
								: null
							}
						</div>
					</div>
					{
						this.state.openModal === "rto-selector" ?
						<div>
							<div>
								<div>{this.state.make ? this.state.make : null}</div>
								<div>{this.state.model ? this.state.model : null}</div>
								<div>{this.state.variant ? this.state.variant : null}</div>
								<div>{this.state.regYear ? this.state.regYear : null}</div>
								<Button customClass={'w--button--link'} title="Edit." onClick={this.openModal.bind(this, 'bike-selector') }/>
							</div>
							<div>
								<div>{this.state.rto ? this.state.rto : null}</div>
								<Button customClass={'w--button--link'} title="Edit." onClick={this.openModal.bind(this, 'rto-selector') }/>
							</div>
						</div>
						: null
					}
					{
						this.state.openModal === "bike-selector" ?
						<BikeSearchBox ref="BikeSearchBox" onMakeChange={this.handleMakeChange.bind(this) }
							onModelChange={this.handleModelChange.bind(this) } onVariantChange={this.handleVariantChange.bind(this) }
							selectedMake={this.state.make} selectedModel={this.state.model} selectedVariant={this.state.variant}
							isShowRegYear={true} onRegYearChange={this.handleRegYearChange.bind(this)} selectedRegYear={this.state.regYear}
							yearList={this.yearList} onComplete={this.handleBikeSelectionComplete.bind(this)}
							onClose={this.handleBikeSearchBoxClose.bind(this)} isOpen={this.state.openModal === "bike-selector"} makeList={this.state.makeList} modelList={this.state.modelList} 
							variantList={this.state.variantList} onBack={this.handleBikeSearchBack.bind(this)}/>
							: null
					}
					
					{
						this.state.openModal === "rto-selector" ?
						<Modal isOpen={this.state.openModal === "rto-selector"} isCloseOnOutSideClick={false} 
							isShowCloseIcon={true} size="large" onClose={this.openModal.bind(this, '')}>
							<div className="quote-so">
								{this.state.regYear ?
									<div>
										<button className="w--button w--button--empty w--button--orange quote-so__edit"
										onClick={this.openModal.bind(this,"bike-selector")}>Edit</button>
										<div className="quote-so__key">Bike</div>
										<div className="quote-so__value">{selectBikeText}</div>
									</div>
									: null
								}
							</div>
							<RTOSelector ref="RTOSelectorControl" selectedRTO={this.state.rto} rtoList={this.state.rtoList} validations={[{ name: 'required', message: 'Oh, we can’t proceed without knowing the RTO. Do your bit?' }]}
								onChange={this.handleRTOChange.bind(this)}/>
							<Button customClass={'w--button--orange w--button--large'} title='Continue' onClick={this.handleRTOSelect.bind(this)}/>
						</Modal>
						:null
					}
				
						
				</div>
				<div className='form-row'>
					{/* Any set of buttons appearing horizontally should always be inside a button group. */}
					<div className='w--button_group'>
						<Button customClass={'w--button--orange w--button--large'} title='Buy Now'
						onClick={this.handleBuyNow.bind(this) } type={this.state.isLoading == true ? 'loading': ''}/>
					</div>
					
				</div>
				{/*<a>I am feeling lucky</a>*/}
			</div>
		)
	}
}

export = BikeFallBackQuoteForm