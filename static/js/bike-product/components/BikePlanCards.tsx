import React = require('react')
import LRCard = require('../../common/new-components/LRCard')
import Spinner = require('../../common/react-component/Spinner')
import Button = require('../../common/react-component/Button')
import Currency = require('../../common/new-components/CurrencyFormatter')

interface IPlan{
	insurerSlug: string;
	errorCode?: string;
	errorMsg?: string;
	premium?: number;
	idv?: number;
	isDepreciationWaiver?: boolean;
	requestedTenureAvailable?:boolean;
}

interface IInsurer {
	slug?: string;
	premium?: number;
}

interface IProps {
	insurersList?: IInsurer[];
	hasFetchFinished?:boolean;
	planList?: IPlan;
	isZeroDepsSelected?:boolean;
	selectedTenure?:number;
	onBuy?:Function;
	onRetry?:Function;
	retryInsurersSlug?:string[];
}

interface IState {
	buySlug?:string;
	reveal?: boolean;
	retryInsurersSlug?: string[];
}

class BikePlanCards extends React.Component<IProps, IState> {
	constructor(props:IProps) {
		super(props)
		this.state = {
			buySlug: '',
			retryInsurersSlug: []
		}
	}

	componentDidMount() {
		var self = this
		setTimeout(function(){
			self.setState({
				reveal: true
			},
				function(){
					TweenMax.staggerFromTo(".bike-plan-card", 2, { y: 20, opacity: 0 }, { y: 0, opacity: 1, delay: 0.5, ease: Elastic.easeOut, force3D: true }, 0.1, function(){
						TweenMax.staggerTo('.contact-me-box,.sms-email-link-wrapper', 2, { y: 0, opacity: 1, ease: Elastic.easeOut, force3D: true }, 0.1);
					});	
				}
			)
		}, 350)
	}

	onBuy(insurer:any){
		this.setState({
			buySlug: insurer.slug
		},
		function(){
			if(this.props.onBuy){
				this.props.onBuy(insurer)
			}
		})
	}

	onRetry(slug:string){
		/*var retrySlugs = this.state.retrySlugs
		if (retrySlugs.indexOf(slug) == -1){
			retrySlugs.push(slug)
			this.setState({
				retrySlugs: retrySlugs
			})
		}*/
		var retryInsurersSlug = this.state.retryInsurersSlug
		retryInsurersSlug.push(slug)
		this.setState({
			retryInsurersSlug: retryInsurersSlug
		},
		function(){
			this.props.onRetry(slug)
		})
	}

	getPlanCardJSX(plan: IPlan, insurer: IInsurer, hasFetchFinished: boolean = false) {
		let clickHandler

		let left = <div className='bike-plan-card--left'>
				<img key={'Logo ' + insurer.slug} src={'/static/img/common/insurers/motor/' + insurer.slug + '.png'}  alt={insurer.slug} />
				{plan && plan.premium ? 
				<Currency customClass='bike-plan-card--idv' value={plan.idv} label="IDV"/>
					/*<p>{"IDV Rs. " + plan.idv}</p> */
					: null}
			</div>

		let right
		if (plan && plan.premium && this.props.retryInsurersSlug.indexOf(insurer.slug) == -1) {
			right = <div key={'Button ' + insurer.slug}><Button disabled={this.state.buySlug != ''} type={this.state.buySlug == insurer.slug ? 'loading' : ''}
				customClass="w--button--empty w--button--small bike-price" title={<Currency value={plan.premium} />}/></div>
			clickHandler = this.onBuy.bind(this, insurer)
		}
		else if (hasFetchFinished && this.props.retryInsurersSlug.indexOf(insurer.slug) == -1) {
			right = <div key={'ErrorMsg ' + insurer.slug} className='quote-fail-error'>
								{
									plan && plan.errorCode ?
									<span>{plan.errorMsg}</span>
									:<span>Pfffbt. Insurer server down.</span>
								}
								{
									!plan || (plan && plan.errorCode!= undefined && plan.errorCode!= "" && plan.errorCode != "RTO_MAPPING_NOT_FOUND" && plan.errorCode != "INSURER_AGE_RESTRICTED") ?
									<span className='quote-fail-try-again'>
										TRY AGAIN
									</span>
									: null
								}
					</div>
				clickHandler = this.onRetry.bind(this, insurer.slug)
		}
		else if (!hasFetchFinished || this.props.retryInsurersSlug.indexOf(insurer.slug) > -1) {
			right = <div key={'Spinner ' + insurer.slug}><Spinner/></div>
		}
		return (
			<div key={insurer.slug} className='bike-plan-card'>
				<LRCard left={left} right={right} onClick={clickHandler}/>
			</div>
		)
	}

	render(){
		var planCardWithoutPremium: JSX.Element[] = []
		var buyablePlanCards: JSX.Element[] = []
		var noQuotePlanCards:JSX.Element[] = []
		var noZeroDepsPlanCards: JSX.Element[] = []
		var noTenurePlanCards: JSX.Element[] = []
		var card:JSX.Element

		for (var i = 0; i < this.props.insurersList.length; i++){
			if (this.props.insurersList[i]['is_active'] == true){
				//buyable plans
				let isPlanReceived = this.props.planList[this.props.insurersList[i]['slug']] !== undefined
				var isErrorPlan = isPlanReceived && this.props.planList[this.props.insurersList[i]['slug']]['errorCode'] != undefined

				/*if ((isPlanReceived || this.props.hasFetchFinished == false) && !isErrorPlan) {
					card = this.getPlanCardJSX(this.props.planList[this.props.insurersList[i]['slug']], this.props.insurersList[i], this.props.hasFetchFinished)

					if (!isErrorPlan && isPlanReceived && this.props.isZeroDepsSelected && !this.props.planList[this.props.insurersList[i]['slug']].isDepreciationWaiver) {
						noZeroDepsPlanCards.push(card)
					} else if (!isErrorPlan && isPlanReceived && !this.props.planList[this.props.insurersList[i]['slug']].requestedTenureAvailable) {
						noTenurePlanCards.push(card)
					} else if (!isErrorPlan && this.props.insurersList[i]['premium']){
						buyablePlanCards.push(card)
					} else {
						planCardWithoutPremium.push(card)
					}
				}
				//No quotes found plans
				else if (isErrorPlan || this.props.hasFetchFinished == true) {
					card = this.getPlanCardJSX(this.props.planList[this.props.insurersList[i]['slug']], this.props.insurersList[i],this.props.hasFetchFinished)
					noQuotePlanCards.push(card)
				}*/

				card = this.getPlanCardJSX(this.props.planList[this.props.insurersList[i]['slug']], this.props.insurersList[i],this.props.hasFetchFinished)
				if (!isErrorPlan && isPlanReceived && this.props.isZeroDepsSelected && !this.props.planList[this.props.insurersList[i]['slug']].isDepreciationWaiver) {
					noZeroDepsPlanCards.push(card)
				} else if (!isErrorPlan && isPlanReceived && !this.props.planList[this.props.insurersList[i]['slug']].requestedTenureAvailable) {
					noTenurePlanCards.push(card)
				} else if (!isErrorPlan && isPlanReceived && this.props.insurersList[i]['premium']){
					buyablePlanCards.push(card)
				} 
				else if(!this.props.hasFetchFinished){
					planCardWithoutPremium.push(card)
				}
				else {
					noQuotePlanCards.push(card)
				}
			}
		}

		return (
			<div className={'bike-plans-wrapper clearfix ' + (this.state.reveal? 'reveal':'')} >
				{ buyablePlanCards.length > 0 ? buyablePlanCards : null }

				{/*noZeroDepsPlanCards.length > 0 ?
					<div className='no-results-message'>
						<h3>NO RESULTS</h3>
						<p>No plans meet your criteria</p>
					</div>
					: null*/
				}

				{
					noZeroDepsPlanCards.length > 0 ?
					<div className='bike-results-row'>
							<h3 className='bike-results-row--title'>
								{buyablePlanCards.length > 0 ? 
								<span>These insurers do not provide zero depreciation cover for your requirement</span>
									: <span>No zero depreciation cover available for your requirement.</span>
								}
								
							</h3>
							<p className='bike-results-row--desc'>But you can still go ahead and buy these plans without zero depreciation.</p>
							<div>{noZeroDepsPlanCards}</div>
					</div>
					:null
				}
				{
					noTenurePlanCards.length > 0 ?
					<div className='bike-results-row'>
						<h3 className='bike-results-row--title'><span>Insurers without {this.props.selectedTenure} years plans</span></h3>
						<p className='bike-results-row--desc'>Following insurers do not provide {this.props.selectedTenure} year(s) plans.
							You can still go ahead and buy 1 year plans from these insurers.
						</p>
						{noTenurePlanCards}
					</div>
					: null
				}
				{
					planCardWithoutPremium.length > 0 ?
					<div className='bike-results-row'>
							{buyablePlanCards.length > 0 ? <div><h3 className='bike-results-row--title'><span>Twiddle your thumbs as we load all plans.</span></h3><p className='bike-results-row--desc'></p></div> : null}
							<div>{planCardWithoutPremium}</div>

					</div>
					: null
				}
				{
					noQuotePlanCards.length > 0 ?
					<div className='bike-results-row'>
						{
						buyablePlanCards.length !== 0 || noZeroDepsPlanCards.length !== 0 || noTenurePlanCards.length!== 0 || planCardWithoutPremium.length !== 0 ?
						<div>
							<h3 className='bike-results-row--title'><span>These insurers did not return any quotes</span></h3>
							<p className='bike-results-row--desc'>They seem to be snoozing or are unable to provide insurance for your requirement.</p>
						</div>
						:
						<div className='no-results-message'>
							<h3>WE GOT NOTHING :(</h3>
							<p>
											Sorry! We could not fetch any quotes for you.
											 It could either mean something’s wrong or maybe it’s just not our day.
											 Please share your mobile number so that our advisors can sort this out.
							</p>
							{/* <a>TRY AGAIN</a>
							<a>CONTACT US</a>.*/}
						</div>
						}
						<div>
						{noQuotePlanCards}
						</div>
					</div>
					: null
				}
			</div>
		)
	}
}

export = BikePlanCards