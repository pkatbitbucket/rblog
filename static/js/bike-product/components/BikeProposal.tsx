import React = require('react')
import ReactDOM = require('react-dom')
import Cookie = require('react-cookie')
import assign = require('object-assign')
import BaseComponent = require('../../common/BaseComponent')
import AppActions = require('../actions/AppActions')

import CIDV = require('../utils/CIDV')
import ProposalForm = require('../../common/json-jsx-parser/ProposalForm')
import Parser = require('../../common/json-jsx-parser/parser')
import CSSMap = require('../constants/css-map')
import GTMEventFuns = require('../utils/GTMEventFuns')
/*import JSONInput = require('../constants/SampleJSON')*/

import ResultStore = require('../stores/ResultStore')
import RegistryStore = require('../stores/RegistryStore')
import VehicleStore = require('../stores/VehicleStore')

import LRCard = require('../../common/new-components/LRCard')
import Currency = require('../../common/new-components/CurrencyFormatter')
import Modal = require('../../common/new-components/Modal')
import Spinner = require('../../common/react-component/Spinner')
import Button = require('../../common/react-component/Button')
import URLS = require('../../common/constants/URLS')
import TransitionUtils = require('../../common/utils/TransitionUtils')

const STORES = [ResultStore]
const MOBILE_NUMBER_COOKIE = 'mobileNo'
const EMAIL_NUMBER_COOKIE = 'email'

function getStateFromStore(){
	return {
		plan: ResultStore.getSelectedPlan(),
		tenure: ResultStore.getSelectedTenure(),
		jsonInput: ResultStore.getProposalJSON(),
		preFilledValueMap:ResultStore.getProposalValueMap(),
		proposalSummary:ResultStore.getProposalSummary(),
		transactionId:ResultStore.getTransactionId(),
		slug:ResultStore.getSelectedPlan(),
		isShowError:ResultStore.getIsShowProposalError(),
		premiumChangedOverThreshold:ResultStore.getProposalPremiumChangedOverThreshold(),
		premiumChangedBelowThreshold:ResultStore.getProposalPremiumChangedBelowThreshold(),
		isFetchingProposalData:ResultStore.getIsFetchingProposalData()
	}
}

interface IProps{
	onChangeStage: Function;
}

interface IState{
	plan?: any;
	tenure?: number;
	jsonInput?:any;
	preFilledValueMap:any;
	proposalSummary:any;
	transactionId:string;
	slug:string;
	isShowError:boolean,
	premiumChangedOverThreshold:number,
	premiumChangedBelowThreshold:number,
	isFetchingProposalData:boolean
}

class BikeProposal extends BaseComponent {
	refs: any
	state: IState
	props:IProps
	constructor(props) {
		super(props, {
			stores: STORES,
			getStateFromStores: getStateFromStore,
			loadFromURLAction: AppActions.setTransactionParams,
			path: URLS.BIKE_PROPOSAL,
			urlParams: ['slug', 'transactionId'],
		})
	}
	componentDidMount(){
		setTimeout(AppActions.fetchProposalJSON.bind(null, this.props['params']['slug'], this.props['params']['transactionId']), 0)
	}
	componentWillUnmount(){
	}
	onMakePayment(data){
		GTMEventFuns.bikePayment(this.state.slug)
		AppActions.redirectToGateWay()
	}
	handlePushData(data){
		if(data.cust_phone)
			Cookie.save(MOBILE_NUMBER_COOKIE, data.cust_phone)
		if(data.cust_email)
			Cookie.save(EMAIL_NUMBER_COOKIE, data.cust_email)
		AppActions.postProposalData(data)
	}
	handleBackToResults(){
		this.resetProposalData()
		TransitionUtils.transitTo(URLS.BIKE_RESULTS + '/' + this.state.proposalSummary['quoteId'])
	}
	handleBackToResultOnPremiumChange(){
		this.resetProposalPremiumDiff()
		this.handleBackToResults()
	}
	handleAcceptNewPremium(){
		AppActions.acceptNewProposalPremium()
	}

	resetProposalPremiumDiff(){
		AppActions.resetProposalPremiumDiff()
	}

	handleStageSubmit(stage){
		GTMEventFuns.onStageChange(stage,this.state.preFilledValueMap,this.state.plan)
	}

	handleEditStage(stage){
		GTMEventFuns.onEditStage(stage,this.state.preFilledValueMap,this.state.plan)
	}
	resetProposalData(){
		AppActions.resetProposalData()
	}

	render() {
		var leftIC, rightIC, leftDC,rightDC,bike	
		
		if(this.state.proposalSummary){
			var flowType = this.state.proposalSummary['flowType']
			bike = this.state.proposalSummary["make"] + " " + this.state.proposalSummary["model"] + " " + this.state.proposalSummary["variant"]
			leftIC = <div><img src={'/static/img/common/insurers/motor/' + this.state.plan + '.png'}  alt={this.state.plan} />
						<Currency value ={this.state.proposalSummary["idv"]} label="IDV"/>
					</div>
			rightIC = 	<div>
							<div className='summary_premium text-right'> 
							<Currency value ={this.state.proposalSummary["premium"]} />
							</div>
							{
								this.state.premiumChangedBelowThreshold > 0 ?
								<div className="premium_msg">{'Your premium has increased by Rs.' + Math.abs(this.state.premiumChangedBelowThreshold) }</div>
								: 
									this.state.premiumChangedBelowThreshold < 0 ?
									<div className="premium_msg">{'Your premium has decreased by Rs.' + Math.abs(this.state.premiumChangedBelowThreshold) }</div>
									: null

							}
						</div>
						
			
			leftDC = <div>
						{
							flowType == 2 ?
						<div><span className='fwb'>RTO: </span>{this.state.proposalSummary["rtoInfo"]["rto_code"] + '(' + this.state.proposalSummary["rtoInfo"]["rto_name"] + ')'}</div>
						: <div><span className='fwb'>Registration Number: </span>{this.state.proposalSummary["regNo"]}</div>
						}
						<div><span className='fwb'>Bike: </span>{bike}</div>
					</div>
			rightDC = <div><div><span className='fwb'>Zero Dep: </span> {(this.state.proposalSummary.zeroDep?"Yes": "No") }</div><div><span className='fwb'>Tenure: </span> {this.state.tenure + " year"}</div></div>
		}

		return (	
			<div>
				{ this.state.jsonInput != undefined && this.state.isFetchingProposalData == false?
					<div>
						<div key={this.state.plan.insurerSlug} className='bike-proposal-summary'>
							<LRCard customClass='summary__plan' left={leftIC} right={rightIC}/>
							<LRCard customClass='summary__quote' left={leftDC} right={rightDC}/>
							<Button key="submit" customClass="w--button--link w--button--small" title={'\u2190 Back to results'}
								onClick={this.handleBackToResults.bind(this) }/>
						</div>
						<ProposalForm formData={Parser.parseJSON(this.state.jsonInput, this.state.preFilledValueMap, CSSMap, 'bike')} CIDV={CIDV} 
							action={this.onMakePayment.bind(this)} onPushData={this.handlePushData.bind(this)} preFilledValueMap={this.state.preFilledValueMap}  
								onStageSubmit={ this.handleStageSubmit.bind(this) }  onEditStage={ this.handleEditStage.bind(this) }
								premiumAmount={this.state.proposalSummary["premium"]} />
					</div>
				: <Spinner customClass="loader"/> }
				{
					this.state.isShowError == true ?
					<Modal isOpen={this.state.isShowError} header="ALERT: Insurer down, we repeat, insurer down!" 
						isCloseOnOutSideClick={false} isCloseOnEscape={false} isShowCloseIcon={false} customHeaderClass="modal-alert__header" customClass="modal--alert modal--alert--error">
							  <p>We aren't able to contact the insurer to continue with your payment. You can either try again, or choose another insurer. Don't worry about losing your progress, we've saved all your details!</p>
							  <div className="modal--alert__footer">
								<Button customClass={'w--button--empty w--button--thin button--primary'} title='Back to Results' onClick={this.handleBackToResults.bind(this) }/>
							  </div>
					</Modal>
					: null
				}
				{
					this.state.proposalSummary && this.state.premiumChangedOverThreshold > 0 ?
					<Modal isOpen={this.state.proposalSummary && this.state.premiumChangedOverThreshold > 0} header="Premium changed" 
						isCloseOnOutSideClick={false} isCloseOnEscape={false} isShowCloseIcon={false} customHeaderClass="modal--alert__header" customClass="modal--alert " customParentClass="modal--opaque_background ">
							  <p> Hi, your premium has increased by Rs.{this.state.premiumChangedOverThreshold}. <strong> New premium is Rs.{this.state.proposalSummary["premium"]} </strong>. You can either continue and make payment or go back to results and select another insurer.</p>
							  <div className="modal--alert__footer">
								<Button customClass={'w--button--empty w--button--thin'} title='Continue' onClick={this.handleAcceptNewPremium.bind(this) }/>
								<Button customClass={'w--button--link w--button--thin'} title='Back to Results' onClick={this.handleBackToResultOnPremiumChange.bind(this) }/>
							</div>
					</Modal>
					: null
				}
				{this.state.proposalSummary && this.state.proposalSummary.status.complete && this.state.proposalSummary.status.success ? <Modal isOpen={true} header="This transaction has already been completed."
					isCloseOnOutSideClick={false} isCloseOnEscape={false} isShowCloseIcon={false} customHeaderClass="modal--alert__header" customClass="modal--alert">
					{/*<div>View success page <a href={this.state.proposalSummary.status.success_link}>here</a></div>*/}
				</Modal> : null}
			</div>
		)
	}
}

export = BikeProposal