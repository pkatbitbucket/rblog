import React = require('react')
import BaseComponent = require('../../common/BaseComponent')
import AppActions = require('../actions/AppActions')
import TransitionUtils = require('../../common/utils/TransitionUtils')
import GTMEventFuns = require('../utils/GTMEventFuns')

import VehicleStore = require('../stores/VehicleStore')
import RegistryStore = require('../stores/RegistryStore')
import PolicyStore = require('../stores/PolicyStore')
import ResultStore = require('../stores/ResultStore')

import ExpiryOptions = require('../../common/constants/ExpiryOptions')
import RegNo = require('../../common/new-components/RegistrationNoInput3')
import MultiSelect = require('../../common/react-component/MultiSelect')
import Button = require('../../common/react-component/Button')
import URLS = require('../../common/constants/URLS')

interface IState {
	regNo?: string[];
	isPolicyExired?: boolean;
	fastlaneStatus?: number;
	quoteId?: string;
	regErrorVisible?: boolean;
	isFetchingQuote?:boolean
}

function getStateFromStores(): any {
	return {
		regNo: RegistryStore.getRegistrationNumber(),
		fastlaneStatus: VehicleStore.getFastlaneSuccessStatus(),
		isPolicyExired: PolicyStore.isExpired(),
		quoteId: ResultStore.getQuoteId()
	}
}

class BikeQuoteForm extends BaseComponent {
	refs: any;
	state: IState;
	fetchQuoteLater:boolean = false
	constructor(props) {
		super(props, {
			stores: [VehicleStore, RegistryStore, PolicyStore, ResultStore],
			getStateFromStores: getStateFromStores
		})
	}
	componentDidMount(){
		AppActions.getInsurers()
		/*var flowType = ResultStore.getFlowType()
		if(flowType == 2)
			this.goToFallBackFlow(true, false)*/
		TweenMax.to(".will-fade", 0.5, { opacity: 1, ease: Back.easeIn }, 0);
	}
	componentWillUpdate(nextProps, nextState){
		/*if(this.state.isFetchingQuote == true && this.state.quoteId != undefined){
			this.state.isFetchingQuote = false
		}*/
		if(this.state.fastlaneStatus == 1 && this.fetchQuoteLater == true){
			this.fetchQuoteLater = false
			AppActions.fetchQuote()
			this.state.isFetchingQuote = true
		}
	}
	componentDidUpdate(prevProps, prevState){
		if(prevState.isFetchingQuote == true  && (this.state.quoteId != undefined
			|| (prevState.fastlaneStatus == 2 && this.state.fastlaneStatus == 0))){
			this.setState({isFetchingQuote: false})
			setTimeout(()=>{this.handleBuyNow()},0)
		}
	}

	handleValidRegNo(regNo:any){
		AppActions.setRegistrationNumber(regNo)
	}
	handleRegNoChange(){
		AppActions.invalidateQuoteId(true)
		var regErrorVisible = this.refs.regNo.showError()
		this.setState({
			regErrorVisible: regErrorVisible
		})
		if(this.state.regNo)
			AppActions.clearVehicleInfo()
	}
	handlePolicyExpiryChange(option:any){
		AppActions.invalidateQuoteId(true)
		AppActions.setExpired(option.value)
	}
	handleBuyNow(){
		var isValidRegNo = this.refs.regNo && this.refs.regNo.isValid(true) 
		var isValid = isValidRegNo && this.refs.expirySelect && this.refs.expirySelect.isValid(true)

		// handling UI part of moving error msg above dont know your bike number
		this.setState({
			regErrorVisible: !isValidRegNo
		})

		if (!isValid)
			return

		if (this.state.quoteId && this.state.fastlaneStatus === 1) {
			GTMEventFuns.successfulFastlaneSubmission()
			var self = this
			TweenMax.to(".will-fade", 0.5, { opacity: 0, ease: Back.easeIn }, 0.1);
			TweenMax.staggerTo(".bike-quote-form .form-row", 0.5, { opacity: 0, y: -200, ease: Back.easeIn}, 0.1, function(){
				//window['_ENTRY_MODULE_'] === 'CMS' ? TransitionUtils.jumpTo(URLS.BIKE_RESULTS + '/' + self.state.quoteId) : TransitionUtils.transitTo(URLS.BIKE_RESULTS + '/' + self.state.quoteId)
				TransitionUtils.transitTo(URLS.BIKE_RESULTS + '/' + self.state.quoteId)	
			});
		} else if (this.state.fastlaneStatus === 0) {
			GTMEventFuns.wrongBikeRegSubmission()
			this.goToFallBackFlow(false, false)
		} else if (this.state.fastlaneStatus === 3 && this.state.regNo && this.state.isPolicyExired != undefined) {
			this.fetchQuoteLater = true
			this.handleValidRegNoBlur()
		} else {
			this.setState({ isFetchingQuote: true }, AppActions.fetchQuote)
		}
	}

	handleValidRegNoBlur(){
		if(this.state.regNo)
			this.setState({ fastlaneStatus: 2 }, AppActions.fetchVehicleInfo)
	}

	goToFallBackFlow(isUserAction?:boolean, isClearVehicleInfo:boolean = false){
		if(isClearVehicleInfo){
			GTMEventFuns.idkBikeRegNumber()		// GTM Event
			AppActions.clearVehicleInfo()
			AppActions.invalidateQuoteId(true)
		}
		AppActions.setFlowType(isUserAction ? 2 : 1)

		this.props.onChangeStage(1)
		//TransitionUtils.transitTo(URLS.BIKE_FALLBACK)
	}


	render() {
		var loading = this.state.isFetchingQuote || (this.state.fastlaneStatus == 2 && this.fetchQuoteLater)
		return (
			<div className='bike-quote-form'>
				<div className='form-row first'>
					<RegNo customClass='w--reg_no_input_minimal' ref="regNo" 
						onValid={this.handleValidRegNo.bind(this) } onChange={this.handleRegNoChange.bind(this) } defaultValue={this.state.regNo}
						onEnterPress={this.handleBuyNow.bind(this)} placeholder={'Enter your bike number'}
						labelText={'Your bike number'} onValidBlur={this.handleValidRegNoBlur.bind(this)}
						disabled={loading} errorRegNoMsg="Oh. Looks like that bike number is not valid. Check again?" inCompleteRegNoMsg="Oh, we can’t proceed without your bike number. Do your bit?"/>
					<MultiSelect customClass='bike_expired_dropdown' header='Has your previous policy expired?' footer="Stress is for larger problems, not expired policies. Let us help you renew online instantly." 
						ref="expirySelect" dataSource={ExpiryOptions} onChange={this.handlePolicyExpiryChange.bind(this)} labelText="Has your previous policy expired?" placeHolder="Has your previous policy expired?" values={this.state.isPolicyExired} 
						isSearchable={false} validations={[{name:'required', message:'Select whether your policy has expired, please?'}]}/>
					<div className={'fallback_links ' + (this.state.regErrorVisible? 'push_down':'')}>
						<Button customClass={'w--button--link'} onClick={this.goToFallBackFlow.bind(this, true, true) } title="Not sure of your bike number?" />
					</div>
				</div>
				<div className='form-row'>
					{/* Any set of buttons appearing horizontally should always be inside a button group. */}
					<div className='w--button_group'>
						<Button customClass={'w--button--orange w--button--large'} title='Buy Now' onClick={this.handleBuyNow.bind(this) } disabled={loading} type={loading ? 'loading' : ''}/>
						{/*<Button customClass={'w--button--orange w--button--large'} title='Buy Now' onClick={this.handleBuyNow.bind(this) } type={this.state.fastlaneStatus === 2 ? 'loading' : ''}/>*/}
					</div>
				</div>
				{/*<a>I am feeling lucky</a>*/}
			</div>
		)
	}
}

export = BikeQuoteForm