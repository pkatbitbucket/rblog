import React = require('react')
import BaseComponent = require('../../common/BaseComponent')
import AppActions = require('../actions/AppActions')
import Helpers = require('../../common/utils/Helpers')
import TransitionUtils = require('../../common/utils/TransitionUtils')
import moment = require('moment')
import GTMEventFuns = require('../utils/GTMEventFuns')

import VehicleStore = require('../stores/VehicleStore')
import RegistryStore = require('../stores/RegistryStore')
import PolicyStore = require('../stores/PolicyStore')
import ResultStore = require('../stores/ResultStore')

import BikeSummaryCard = require('./BikeSummaryCard')
// import BikeSummaryCard = require('../../common/new-components/BikeSummaryCard')
import BikePlanCards = require('./BikePlanCards')
import ContactMeBox = require('../../common/new-components/ContactMeBox')
import ShareResultsBox = require('../../common/new-components/ShareResultsBox')
import Modal = require('../../common/new-components/Modal')
import BikeAddonSelector = require('./BikeAddonSelector')
import BikeQuoteForm = require('./BikeQuoteForm')
import BikeSearchBox = require('./BikeSearchBox')
import Spinner = require('../../common/react-component/Spinner')
import URLS = require('../../common/constants/URLS')

const STORES = [VehicleStore, RegistryStore, PolicyStore, ResultStore]

interface IResultsState {
	plans?:any;
	hasFetchFinished?: boolean;
	isSharingPlans?:boolean;
	regNo?:string[];
	rto?:string;
	variant?:string;
	isExpired?:boolean;
	idv?:number;
	selectedTenure?:number;
	zeroDep?:boolean;
	isZeroDepAvailable?:boolean;
	isWrongBike?:boolean;
	selectedMake?:string;
	selectedModel?:string;
	selectedVariant?:string;
	retryInsurersSlug?:string[];
	//isQuoteFormPopUp?: boolean;
	selectedPlan?: string;
	transactionId?: string;
	quoteId?:string,
	shortUrl?:string,
	hasFastLaneModelChanged?:boolean;
	maxIDV?:number;
	makeList: any[];
	modelList: any[];
	variantList: any[];
}

function getStateFromStores(): any {
	return {
		plans:ResultStore.getPlans(),
		hasFetchFinished: ResultStore.hasFetchFinished(),
		regNo:RegistryStore.getRegistrationNumber(),
		rto:RegistryStore.getSelectedRTOVerbose(),
		isExpired:PolicyStore.isExpired(),
		idv:ResultStore.getSelectedIDV(),
		selectedTenure:ResultStore.getSelectedTenure(),
		zeroDep:ResultStore.isDepreciationWaiver(),
		isZeroDepAvailable:ResultStore.getZeroDepAvailablity(),
		selectedMake:VehicleStore.getSelectedMakeVerbose(),
		selectedModel:VehicleStore.getSelectedModelVerbose(),
		selectedVariant:VehicleStore.getSelectedVariantVerbose(),
		retryInsurersSlug:ResultStore.getRetryInsurersSlug(),
		selectedPlan:ResultStore.getSelectedPlan(),
		transactionId:ResultStore.getTransactionId(),
		quoteId:ResultStore.getQuoteId(),
		shortUrl:ResultStore.getShortUrl(),
		hasFastLaneModelChanged: VehicleStore.hasFastLaneModelChanged(),
		maxIDV:ResultStore.getMaxIDV(),
		makeList: VehicleStore.getMakes(),
		modelList: VehicleStore.getModels(),
		variantList: VehicleStore.getVariants()
	}
}

class BikeResults extends BaseComponent {
	refs: any;
	state: IResultsState;
	buyPlan: boolean;
	insurersList: any = [];
	tenureOptions: any = [];
	zeroDepOptions: any = [];
	yearList:any =[]
	constructor(props) {
		super(props, {
			stores: STORES,
			getStateFromStores: getStateFromStores,
			loadFromURLAction: AppActions.setResultParams,
			path: URLS.BIKE_RESULTS,
			urlParams: ['quoteId'],
			queryParams: ['zeroDep', 'idv']
		})
		AppActions.fetchQuoteParams(this.props.params.quoteId)
		AppActions.resetProposalData()
	}
	componentDidMount() {
		TweenMax.from(this.refs['bikeSummaryCard'].refs['bikeSummary'], 2, { y: -20, opacity: 0, ease: Elastic.easeOut, force3D: true }, 0.1);
		TweenMax.staggerTo('.contact-me-box,.sms-email-link-wrapper', 0, { y: 30, opacity: 0 }, 0.1);


	}
	componentWillMount(){
		this.buyPlan = false
		GTMEventFuns.virtualPageView()
	}
	componentDidUpdate(){
		if(this.buyPlan && this.state.selectedPlan && this.state.transactionId){
			setTimeout(TransitionUtils.transitTo.bind(null, URLS.BIKE_PROPOSAL + '/' + this.state.selectedPlan + '/' + this.state.transactionId), 0)
			this.buyPlan = false
		}
	}
	handleBuy(insurer:any){
		this.buyPlan = true

		GTMEventFuns.clickOnInsurer(insurer)
		AppActions.buyPlan(insurer.slug)
	}
	handleRetry(slug:string){
		AppActions.fetchForInsurer(slug)
	}
	handleShare(){
		this.setState({ isSharingPlans: !this.state.isSharingPlans })
	}
	handleIDVChange(value:number){
		GTMEventFuns.changeIDV(value)

		AppActions.setIDV(value)
	}
	handleTenureChange(value:number){
		AppActions.setTenure(value)
	}
	handleZeroDepsChange(value:boolean){

		GTMEventFuns.zeroDepChanged(value)

		AppActions.setDepreciationWaiver(value)
	}
	handleEditRegNo(isOpen:boolean){

		GTMEventFuns.editBikeReg()
		//this.setState({ isQuoteFormPopUp: isOpen })
		/*var flowType = ResultStore.getFlowType()
		if(flowType == 2)
			TransitionUtils.transitTo(URLS.BIKE_FALLBACK)
		else
			TransitionUtils.transitTo(URLS.BIKE_LANDING)*/
		TransitionUtils.jumpTo(URLS.BIKE_LANDING)
	}
	handleWrongBike(value:boolean, isCloseButtonPressed:boolean = false){

		if(value == true){

			// Case where clicked on "NotYour Bike"
			GTMEventFuns.notYourBike()
			AppActions.clearModelList()
			AppActions.clearVariantList()
			AppActions.fetchVehicleMakes(function(){
				this.setState({
					modelList: [],
					variantList: [],
					isWrongBike: true
				})
			}.bind(this))
		} else {
			if (isCloseButtonPressed) {
				this.setState({
					modelList: [],
					variantList: []
				})
				AppActions.clearModelList()
				AppActions.clearVariantList()
			}

			this.setState({
				isWrongBike: false
			})
		}
	}
	handleMakeChange(id:number){
		//AppActions.selectMake(id)
		AppActions.fetchVehicleModels(id)
	}
	handleModelChange(id:number){
		//AppActions.selectModel(id)
		AppActions.fetchVehicleVariants(id)
	}
	handleVariantChange(id: number) {
		//AppActions.selectVariant(id)
	}
	handleRegYearChange(regYear:string){
		//var currentMonth = moment().month() + 1
		//AppActions.setRegistrationDate("1", currentMonth.toString(), regYear)
	}

	handleBikeSelectionComplete(makeId, modelId, variantId, regYear){
		AppActions.selectMake(makeId, false)
		AppActions.selectModel(modelId, false)
		AppActions.selectVariant(variantId)
		if(regYear){
			var currentMonth = moment().month() + 1
			AppActions.setRegistrationDate("1", currentMonth.toString(), regYear)
		}
		var flowType = ResultStore.getFlowType()
		var isSendFeedback = flowType == 0 || flowType == 1 ? true : false
		AppActions.fetchQuote(true, isSendFeedback)
	}

	handleShareResults(shareType:string, value:string){
		AppActions.shareQuotes(this.state.quoteId, window.location.href, shareType, value)
	}

	handleCloseOnContinue(){
		// Case where clicked on "CONTINUE"
		GTMEventFuns.resubmitAfterNotYourBike()
	}
	handeCall(mobileNo:string){
		AppActions.callMe()
	}

	handleBikeSearchBack(step:number){
		if(step == 2){
			this.setState({
				modelList: [],
				variantList:[]
			})
			AppActions.clearModelList()
			AppActions.clearVariantList()
		}
		if(step == 3){
			this.setState({
				variantList:[]
			})
			AppActions.clearVariantList()
		}
	}

	render() {
		if(!this.yearList || this.yearList.length === 0){
			this.yearList = RegistryStore.getYears()
		}

		var flowType = ResultStore.getFlowType()
		var displayRegNo = this.state.regNo ? this.state.regNo.join('') : this.state.rto
		if(flowType == 2){
			displayRegNo = this.state.rto
		}

		if (!this.insurersList || this.insurersList.length == 0) {
			this.insurersList = ResultStore.getInsurers()
		}

		var idvOptions = ResultStore.getIDVData()

		if(!this.tenureOptions || this.tenureOptions.length == 0){
			this.tenureOptions = ResultStore.getTenureData()
		}
		if(!this.zeroDepOptions || this.zeroDepOptions.length == 0){
			this.zeroDepOptions = ResultStore.getZeroDepData()
		}

		var isPremiumPlanPresent:boolean = false
		for(var keys in this.state.plans){
			if(this.state.plans[keys].premium != undefined && this.state.plans[keys].premium != 0){
				isPremiumPlanPresent = true
				break
			}
		}

		var isAfterMarchEnd = false
		var currDate = moment()
		var marchEnd = moment('31/03/2016', 'DD/MM/YYYY', true)

		isAfterMarchEnd = currDate.isAfter(marchEnd) ? true : false

		return (
			<div className='bike-results-wrapper'>
				<BikeSummaryCard ref='bikeSummaryCard' make={this.state.selectedMake} model={this.state.selectedModel} variant={this.state.selectedVariant} regNo={displayRegNo}
					isExpired={this.state.isExpired} onEdit={this.handleEditRegNo.bind(this, true)} onWrongBike={this.handleWrongBike.bind(this,true)}/>
				{
					this.state.isWrongBike == true ?
					<BikeSearchBox onClose={this.handleWrongBike.bind(this, false) } onMakeChange={this.handleMakeChange.bind(this)}  closeOnContinue={ this.handleCloseOnContinue.bind(this)}
					onModelChange={this.handleModelChange.bind(this)} onVariantChange={this.handleVariantChange.bind(this)}
					selectedMake={this.state.selectedMake} selectedModel={this.state.selectedModel} selectedVariant={this.state.selectedVariant}
					isShowRegYear={this.state.hasFastLaneModelChanged} yearList={this.yearList} isOpen={this.state.isWrongBike == true}
					onRegYearChange={this.handleRegYearChange.bind(this)} onComplete={this.handleBikeSelectionComplete.bind(this)} makeList={this.state.makeList} 
					modelList={this.state.modelList} variantList={this.state.variantList} onBack={this.handleBikeSearchBack.bind(this)}/> : null

				}

				{
					isPremiumPlanPresent == true ?
					<BikeAddonSelector isZeroDepsSelected={this.state.zeroDep} selectedIDV={this.state.idv}
						selectedTenure={this.state.selectedTenure} idvOptions={idvOptions} tenureOptions={this.tenureOptions}
						onIDVChange={this.handleIDVChange.bind(this)} onTenureChange={this.handleTenureChange.bind(this)}
						onZeroDepsChange={this.handleZeroDepsChange.bind(this) } zeroDepOptions={this.zeroDepOptions}
						maxIDV={this.state.maxIDV} isZeroDepAvailable={this.state.isZeroDepAvailable}/>
					:null
				}


				{this.insurersList && this.insurersList.length ? <BikePlanCards hasFetchFinished={this.state.hasFetchFinished} insurersList={this.insurersList}
					planList={this.state.plans} selectedTenure={this.state.selectedTenure} isZeroDepsSelected={this.state.zeroDep}
					onBuy={this.handleBuy.bind(this)} onRetry={this.handleRetry.bind(this)} retryInsurersSlug={this.state.retryInsurersSlug}/> : <Spinner customClass="loader" />}

				{/*this.state.isQuoteFormPopUp == true ?
					<Modal isOpen={this.state.isQuoteFormPopUp} size="large" onClose={this.handleEditRegNo.bind(this,false)}
						isCloseOnOutSideClick={false} isShowCloseIcon={true}>
						<BikeQuoteForm/>
					</Modal> : null*/ }
				{
				isAfterMarchEnd === true ?
				<div className='bike-results-prompt'>There! Which plan do you like the most? Choose a plan and pay. It’s that simple.</div>
				:
				<div className="mkting_banner visible-lg">
					<h4>INSURANCE PREMIUM IS INCREASING FROM 1ST APRIL 2016</h4>
					<span>With IRDAI revising third-party premium rates, bike insurance premium price may increase from 1st April 2016. We suggest you to renew your policy before 31st March, to avoid paying more later.</span>
				</div>
				}
				{
					isPremiumPlanPresent == true ?
					<div className='sms-email-link-wrapper'><a className='sms-email-link' onClick={this.handleShare.bind(this)}>Share these results</a></div>
					:null
				}
				{ this.state.isSharingPlans == true ? <ShareResultsBox shareUrl={this.state.shortUrl} onClose={this.handleShare.bind(this)} onShare={this.handleShareResults.bind(this)}/> : null }

				<ContactMeBox onCall={this.handeCall.bind(this)} subTitle="Why not ask for some expert advice from our specialist about your bike insurance needs? We will give you a call in less than 15 minutes."/>
			</div>
		)
	}
}

export = BikeResults