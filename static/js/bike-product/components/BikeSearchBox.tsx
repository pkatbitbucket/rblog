import React = require('react')
import SelectBox = require('../../common/new-components/SelectBox')
import Button = require('../../common/react-component/Button')
import Modal = require('../../common/new-components/Modal')
import MultiSelect = require('../../common/react-component/MultiSelect')
import RegistrationYearSelector = require('../../common/new-components/RegistrationYearSelector')
import VehicleStore = require('../stores/VehicleStore')
import Spinner = require('../../common/react-component/Spinner')

interface IProps {
	makeList?: any[];
	modelList?: any[];
	variantList?: any[];
	selectedMake?: string;
	selectedModel?: string;
	selectedVariant?: string;
	onClose?: Function;
	onMakeChange?: Function;
	onModelChange?: Function;
	onVariantChange?: Function;
	onRegYearChange?: Function;
	isShowRegYear?: boolean;
	selectedRegYear?: string;
	yearList?: number[];
	onComplete?:Function;
	ref?:any;
	isOpen?:boolean;
	closeOnContinue?:Function;
	onBack?:Function
}

interface IState{
	step?: number;
	selectedMake?: string;
	selectedMakeId?: number;
	selectedModel?: string;
	selectedModelId?: number;
	selectedVariant?:string;
	selectedVariantId?:number;
	selectedRegYear?:string;
	isValid?:boolean;
	validationMsg?:string
}

class BikeSearchBox extends React.Component<IProps, IState> {
	refs: any;
	//optionsList: any = []
	constructor(props:IProps){
		super(props)
		this.state = {
			step : 1,
			/*selectedMake: props.selectedMake,
			selectedModel:props.selectedModel,
			selectedVariant:props.selectedVariant,
			selectedRegYear:props.selectedRegYear,*/
			isValid:true,
			validationMsg:""
		}
	}

	static defaultProps = {
		isShowRegYear:false
	}

	public isValid(isShowErrors:boolean = false){
		var isValid = true
		var validationMsg = ""
		if(this.state.selectedMake == undefined || this.state.selectedMake == ""){
			isValid = false
			validationMsg = "Please select a make."
		}
		else if(this.state.selectedModel == undefined || this.state.selectedModel == ""){
			isValid = false
			validationMsg = "Please select a model."
		}
		else if(this.state.selectedVariant == undefined || this.state.selectedVariant == ""){
			isValid = false
			validationMsg = "Please select a variant."
		}
		else if(this.props.isShowRegYear && (this.state.selectedRegYear == undefined || this.state.selectedRegYear == "")){
			isValid = false
			validationMsg = "Please select a registration year."
		}
		else {
			isValid = true
			validationMsg = ""
		}
		if(isShowErrors){
			this.setState({
				isValid:isValid,
				validationMsg:validationMsg
			})
		}
		return isValid
	}

	/*componentWillReceiveProps(nextProps:IProps){
		this.setState({
			selectedMake: nextProps.selectedMake,
			selectedModel: nextProps.selectedModel,
			selectedVariant: nextProps.selectedVariant,
			selectedRegYear: nextProps.selectedRegYear
		})
	}*/

	componentDidUpdate(prevProps:IProps, prevState:IState){
		if(prevState.step == 3 && this.props.variantList && this.props.variantList.length == 1){
			this.skipVariantSelection()
		}
	}
	
	skipVariantSelection(){
		if(this.props.isShowRegYear){
			this.setState({
					step: 4, 
					selectedVariant: VehicleStore.getSelectedVariantVerbose(), 
					selectedVariantId:VehicleStore.getSelectedVariant()}
			)
		}
		else {
			this.setState({
				selectedVariant: VehicleStore.getSelectedVariantVerbose(), 
				selectedVariantId:VehicleStore.getSelectedVariant()
			},
			this.handleComplete.bind(this)
			)
		}
	}

	handleMakeChange(make: any) {
		this.setState({ selectedMake: make['name'], selectedMakeId:make['id'], selectedModel: null, selectedModelId:null,  selectedVariant: null, selectedVariantId:null })
		this.changeStep(1)
		if (this.props.onMakeChange){
			this.props.onMakeChange(make['id'])
		}
	}
	handleModelChange(isFetchModelValue:boolean, model: any) {
		var modelId
		//validation is performed only when isFetchModelValue is false as in other case the state is not yet set
		if(!isFetchModelValue || (this.state.selectedModel != "" && this.state.selectedModel != undefined)){
			if(!isFetchModelValue){
				this.setState({ selectedModel:model['name'],selectedModelId: model['id'],selectedVariant: null , selectedVariantId:null})
				modelId = model['id']
			}
			//this is case where user has  selected the value from drop down.
			else{
				this.setState({ selectedModel: this.refs['OtherModels'].getSelectedLabels(), selectedModelId: this.refs['OtherModels'].getValues(), selectedVariant: null, selectedVariantId:null})
				modelId =  this.refs['OtherModels'].getValues()
			}

			this.changeStep(1)
			if (this.props.onModelChange) {
				this.props.onModelChange(modelId)
			}

			this.setState({
				isValid:true,
				validationMsg:''
			})
		}
		else {
			this.setState({
				isValid:false,
				validationMsg:'Please select a model.'
			})
		}
	}

	handleModelDDLChange(model:any){
		this.setState({
			selectedModel:model['name'],
			selectedModelId:model['id'],
			selectedVariant:null,
			selectedVariantId:null,
			isValid:true,
			validationMsg:''
		})
	}

	handleVariantChange(variant: any) {
		//this.state.selectedVariant = variant['name']
		this.setState({
			selectedVariant: variant['name'],
			selectedVariantId: variant['id']
		})

		if (this.props.onVariantChange) {
			this.props.onVariantChange(variant['id'])
		}
		if(!this.props.isShowRegYear)
			this.handleComplete()	
		else
			this.changeStep(1)
	}

	handleRegYearChange(regYear:string, isTopYear:boolean){
		this.setState({ selectedRegYear: regYear }, function(){
			if (this.props.onRegYearChange) {
				this.props.onRegYearChange(regYear)
			}
			if(isTopYear){
				this.handleComplete()
			}
		})
	}

	handleComplete(){
		if(this.isValid(true)){
			this.handleClose()
			if(this.props.onComplete)
				this.props.onComplete(this.state.selectedMakeId, this.state.selectedModelId, this.state.selectedVariantId, this.state.selectedRegYear)
		}
	}

	handleClose(isCallBack:boolean = false, isCloseButtonPress:boolean = false){
		this.setState({
			step:1,
			selectedMake:null,
			selectedModel:null,
			selectedVariant:null
		})
		if(!isCallBack){
			// if its a callback means bike selction modal has already been closed
			this.refs.bikeSelectModal.handleClose()

			// For GTM purpose
			if(this.props.closeOnContinue)
				this.props.closeOnContinue()
		}

		if(this.props.onClose){
			this.props.onClose(isCloseButtonPress)
		}
	}

/*	handleClick(value:number){
		switch(this.state.step){
			case 1:
				this.handleMakeChange(value)
				break
			case 2:
				this.handleModelChange(value)
				break
			case 3:
				this.handleVariantChange(value)
				break
		}
	}*/

	changeStep(value:number = 1){
		this.setState({
			step: this.state.step + value
		})
	}

	handleBack(){
		if(this.props.onBack){
			this.props.onBack(this.state.step)
		}
		this.changeStep(-1)
	}

	formMakes(makes:any):JSX.Element{
		var makeBox:JSX.Element
		var popularOptions:JSX.Element[] = []
		var otherOptions:JSX.Element[] = []
		for (let i = 0; i < makes.length; i++) {
			if(makes[i]['is_popular']){
				let popularOption = <div className='cf--pill cf--pill--logo' key={'ScrollableOption ' + i} onClick={this.handleMakeChange.bind(this, makes[i]) }>
										<img src={'/static/img/icons/bike_icons/make_' + makes[i]['id'] +'.png'} /> 
										<span>{makes[i]['name']}</span>
									</div>
				popularOptions.push(popularOption)
			}
			else{
				let otherOption = <div className='cf--pill' key={'ScrollableOption ' + i} onClick={this.handleMakeChange.bind(this, makes[i]) }>
										<span>{makes[i]['name']}</span>
									</div>
				otherOptions.push(otherOption)
			}


		}

		makeBox =
					<div>
							<h3 className='modal_header modal_header--title'>Select your bike manufacturer</h3>
							<h5 className='modal_header modal_header--hr'><span>TOP MANUFACTURERS</span></h5>
							<div>
							{
							popularOptions.length > 0 ?
								popularOptions :
								<Spinner />
							}
							</div>
							<h5 className='modal_header modal_header--hr'><span>OTHER MANUFACTURERS</span></h5>
							<div>
							{
							otherOptions.length > 0 ?
								otherOptions :
								<Spinner />
							}
							</div>
					</div>
		return makeBox
	}

	formModels(models:any):JSX.Element{
		var modelBox:JSX.Element
		var popularOptions:JSX.Element[] = []
		var otherOptions:any = []

		var isShowTopModels = false
		if(models.length > 12)
			isShowTopModels = true

		for (let i = 0; i < models.length; i++) {
			if(models[i]['is_popular'] || !isShowTopModels){
				let popularOption = <div className='cf--pill' key={'ScrollableOption ' + i} onClick={this.handleModelChange.bind(this, false, models[i]) }><span>{models[i]['name']}</span></div>
				popularOptions.push(popularOption)
			}
			else {
				otherOptions.push(models[i])
			}
		}

		var dropDownPlaceholder = popularOptions.length > 0 ? 'Other ' + this.state.selectedMake + ' Bikes' : 'All ' + this.state.selectedMake + ' Bikes' 

		modelBox =
					<div>
							<span className="modal_back_arrow" onClick={this.handleBack.bind(this)}>{'\u2190'}</span>
							<h3 className='modal_header modal_header--title'>{'Which ' + this.state.selectedMake + ' bike do you ride?'}</h3>
							{
								isShowTopModels == true && popularOptions.length > 0 ?
					<h5  className='modal_header modal_header--hr'><span>{'TOP ' + this.state.selectedMake  + ' BIKES'}</span></h5>
									: null
							}
							<div>
								{popularOptions}
							</div>
							<div>
								{
									popularOptions.length == 0 && otherOptions.length == 0 ?
									<Spinner />
									:null
								}
							</div>
							{
								isShowTopModels == true ?
									<div className="other_vehicle_models">
										{
										otherOptions.length > 0 ?
											<MultiSelect ref="OtherModels"  dataSource={otherOptions} isSearchable={true}
											placeHolder={dropDownPlaceholder} labelText={'Your ' + this.state.selectedMake + ' bike'}
											optionsText="name" optionsValue="id"
											onChange={this.handleModelDDLChange.bind(this) } values={this.state.selectedModelId}/> 
											: <Spinner />
										}
										<Button customClass={'w--button--orange w--button--large'} title='Continue'
											onClick={this.handleModelChange.bind(this, true)}/>
									</div>
									: null
							}
					</div>
		return modelBox
	}

	formVariants(variants:any):JSX.Element{
		var variantBox:JSX.Element
		var options:JSX.Element[] = []
		for (let i = 0; i < variants.length; i++) {
			let option = <div className='cf--pill cf--pill--large' key={'ScrollableOption ' + i} onClick={this.handleVariantChange.bind(this, variants[i]) }><span>{variants[i]['name']}</span></div>
			options.push(option)
		}
		variantBox =
					<div>
							<span className="modal_back_arrow" onClick={this.handleBack.bind(this)}>{'\u2190'}</span>
							<h3 className='modal_header modal_header--title'>{'Which ' + this.state.selectedModel + ' model do you ride?'}</h3>
							<div>
								{
									options.length > 0 ?
									options
									: <Spinner/>
								}
							</div>
					</div>
		return variantBox
	}

	render(){
		let box
		switch(this.state.step){
			case 1:
				box = this.formMakes(this.props.makeList || [])
				break
			case 2:
				box = this.formModels(this.props.modelList || [])
				break
			case 3:
				box = this.formVariants(this.props.variantList || [])
				break
			case 4:
				box =
						<div>
							<span className="modal_back_arrow" onClick={this.handleBack.bind(this)}>{'\u2190'}</span>
							<h3 className='modal_header modal_header--title'>In which year was your bike registered?</h3>
							<RegistrationYearSelector selectedYear={this.state.selectedRegYear} yearList={this.props.yearList}
								onChange={this.handleRegYearChange.bind(this)} validations={['required']}/>
							<Button customClass={'w--button--orange w--button--large'} title='Continue'
								onClick={this.handleComplete.bind(this)}/>
						</div>
				break
		}


		return (
			<Modal ref="bikeSelectModal" customClass='vehicle_modal' isShowCloseIcon={true} isOpen={this.props.isOpen}
				  isCloseOnOutSideClick={false} isCloseOnEscape={true}
				onClose={this.handleClose.bind(this, true, true)}>
				<h4 className="vehicle_selected_header">
					{this.state.selectedMake && this.state.step > 1 ? <span>{this.state.selectedMake}</span> : null}
					{this.state.selectedModel && this.state.step > 2 ? <span>{this.state.selectedModel}</span> : null}
					{this.state.selectedVariant && this.state.step > 3? <span>{this.state.selectedVariant}</span> : null}
				</h4>
				<div>{box}</div>
				{
					this.state.isValid == false ?
					<div className="error-label">{this.state.validationMsg}</div>
					: null
				}
			</Modal>
		)
	}
}

export = BikeSearchBox