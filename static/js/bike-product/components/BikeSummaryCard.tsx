import React = require('react')
import LRCard = require('../../common/new-components/LRCard')

interface IProps {
	make?:string,
	model:string,
	variant:string,
	regNo:string,
	isExpired:boolean,
	onEdit?:Function,
	onWrongBike?: Function;
	ref:any;
}

class BikeSummaryCard extends React.Component<IProps, any> {
	constructor(props){
		super(props)
	}
	handleEdit(){
		if(this.props.onEdit){
			this.props.onEdit()
		}
	}
	handleWrongBike(){
		if(this.props.onWrongBike){
			this.props.onWrongBike()
		}
	}

	render(){
		var model = this.props.make ? this.props.make + " " + this.props.model : this.props.model
		let LL = <div>
					<h3>{model}</h3>
					<h4>{this.props.variant}</h4>
				</div>
		let LR = <a onClick={this.handleWrongBike.bind(this)}>NOT YOUR BIKE?</a>
		let RL = <div>
					<h3>{this.props.regNo}</h3>
					<h4>{this.props.isExpired ? "Policy Expired" : "Policy Not Expired"}</h4>
				</div>
		let RR = <a  onClick={this.handleEdit.bind(this)}>EDIT</a>

		return (
			<div ref='bikeSummary' className='bike-results-summary'>
				<LRCard left={LL} right={LR}/>
				<LRCard left={RL} right={RR}/>
			</div>
		)
	}
}

export = BikeSummaryCard