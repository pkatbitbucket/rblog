var NCBOptions = [
    {
        key: '0%',
        value: 0
    }, {
        key: '20%',
        value: 20
    }, {
        key: '25%',
        value: 25
    }, {
        key: '35%',
        value: 35
    }, {
        key: '45%',
        value: 45
    }, {
        key: '50%',
        value: 50
    }, {
        key: "I don't know my NCB",
        value: 'U'
    }
]

var AgeWiseNCB = {
    0: 0,
    1: 0,
    2: 20,
    3: 25,
    4: 35,
    5: 45,
    6: 50
}

export = {
    NCBOptions: NCBOptions,
    AgeWiseNCB: AgeWiseNCB
}