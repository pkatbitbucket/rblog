export = [
    {
        "title": "Primary Details",
        "id": "api-key-pd-container",
        "submitLabel": "Next",
        "editLabel": "Edit",
        "type": "container",
        "isOpen": true,
        "children": [
            {
                "minlength": 0,
                "title": "Name",
                "type": "text",
                "required": true,
                "label": null,
                "validators": [
                    {
                        "message": "Name should start with an alphabet.",
                        "expression": "^[\\w\\.\\'\\`\\-\\s]+$",
                        "name": "regex"
                    }
                ],
                "hidden": false,
                "placeholder": "Your Name",
                "id": "id_name",
                "maxlength": 50
            },
            {
                "minlength": 0,
                "title": "Email",
                "type": "text",
                "required": true,
                "label": null,
                "validators": [
                    {
                        "message": "Please enter a valid Email Address.",
                        "expression": "^([A-Z|a-z|0-9](\\.|_){0,1})+[A-Z|a-z|0-9]\\@([A-Z|a-z|0-9])+((\\.){0,1}[A-Z|a-z|0-9]){2}\\.[a-z]{2,3}$",
                        "name": "regex"
                    }
                ],
                "hidden": false,
                "placeholder": "Email Address",
                "id": "id_email",
                "maxlength": null
            },
            {
                "minlength": 0,
                "title": "Phone",
                "type": "mobile",
                "required": true,
                "label": null,
                "validators": [
                    {
                        "message": "Invalid Phone number",
                        "expression": "^([7-9][\\d]{9})$",
                        "name": "regex"
                    }
                ],
                "hidden": false,
                "placeholder": "10 digit mobile number",
                "id": "id_phone_number",
                "maxlength": null
            }
        ],
        "desc": "Fill the simple form on the left and "
    },
    {
        "title": "Basic Details",
        "id": "api-key-bd-container",
        "submitLabel": "Next",
        "editLabel": "Edit",
        "type": "container",
        "isOpen": true,
        "children": [
            {
                "title": null,
                "type": "radio",
                "required": true,
                "label": "Gender",
                "options": [
                    {
                        "value": "Male",
                        "key": "Male"
                    },
                    {
                        "value": "Female",
                        "key": "Female"
                    }
                ],
                "validators": [],
                "hidden": false,
                "placeholder": null,
                "id": "id_gender"
            },
            {
                "title": null,
                "type": "radio",
                "required": true,
                "label": "Married",
                "options": [
                    {
                        "value": "Yes",
                        "key": "Yes"
                    },
                    {
                        "value": "No",
                        "key": "No"
                    }
                ],
                "validators": [],
                "hidden": false,
                "placeholder": null,
                "id": "id_married"
            },
            {
                "title": null,
                "type": "datepicker",
                "format": "YYYY-MM-DD",
                "required": true,
                "label": "Date of Birth",
                "max_date": "1998-02-16",
                "validators": [],
                "hidden": false,
                "min_date": "1916-02-16",
                "placeholder": null,
                "id": "id_date_of_birth"
            },
            {
                "minlength": 10,
                "title": "PAN Number",
                "type": "text",
                "required": true,
                "label": "PAN Number",
                "validators": [
                    {
                        "message": "Please enter a valid PAN Number.",
                        "expression": "^[A-Z]{5}[0-9]{4}[A-Z]{1}",
                        "name": "regex"
                    }
                ],
                "customValidation": ['sampleHide'],
                "hidden": false,
                "placeholder": "PAN Number",
                "id": "id_pan_card",
                "maxlength": 10
            },
            {
                "id": "sub-container-nominee",
                "label": "Nominee Details",
                "title": "Nominee",
                "type": "sub-container",
                "children": [
                    {
                        "minlength": 0,
                        "title": "Nominee Name",
                        "type": "text",
                        "required": true,
                        "label": "Name",
                        "validators": [
                            {
                                "message": "Name should start with an alphabet.",
                                "expression": "^[\\w\\.\\'\\`\\-\\s]+$",
                                "name": "regex"
                            }
                        ],
                        "hidden": false,
                        "placeholder": "Nominee Name",
                        "id": "id_name",
                        "maxlength": null
                    },
                    {
                        "minlength": 2,
                        "title": "Age",
                        "type": "text",
                        "required": true,
                        "label": null,
                        "validators": [
                            {
                                "message": "Only numeric characters are allowed.",
                                "expression": "^[0-9]+$",
                                "name": "regex"
                            },
                            {
                                "message": "Nominee must be more than 18 years old",
                                "expression": "^(1[89]|[2-9][0-9])$",
                                "name": "regex"
                            }
                        ],
                        "hidden": false,
                        "placeholder": "Age",
                        "id": "id_age",
                        "maxlength": 2
                    },
                    {
                        "title": null,
                        "type": "radio",
                        "required": true,
                        "label": "Nominee Relationship",
                        "options": [
                            {
                                "value": "Male",
                                "key": "Male"
                            },
                            {
                                "value": "Female",
                                "key": "Female"
                            }
                        ],
                        "validators": [],
                        "hidden": false,
                        "placeholder": null,
                        "id": "id_relationship"
                    }
                ]
            }
        ],
        "desc": "if any"
    },
    {
        "title": "Vehicle Details",
        "id": "api-key-vd-container",
        "submitLabel": "Next",
        "editLabel": "Edit",
        "type": "container",
        "isOpen": true,
        "children": [
            {
                "title": null,
                "type": "datepicker",
                "format": "YYYY-MM-DD",
                "required": true,
                "label": "Date of Birth",
                "max_date": "1998-02-16",
                "validators": [],
                "hidden": false,
                "min_date": "1916-02-16",
                "placeholder": null,
                "id": "id_past_policy_expiry_date"
            },
            {
                "title": null,
                "type": "select",
                "required": true,
                "label": "Nominee Relationship",
                "options": [
                    {
                        "value": "Male",
                        "key": "Male"
                    },
                    {
                        "value": "Female",
                        "key": "Female"
                    }
                ],
                "validators": [],
                "hidden": false,
                "placeholder": null,
                "id": "id_past_policy_insurer"
            },
            {
                "minlength": 0,
                "title": "Previous Policy Number",
                "type": "text",
                "required": true,
                "label": null,
                "validators": [],
                "hidden": false,
                "placeholder": "Previous Policy Number",
                "id": "id_past_policy_number",
                "maxlength": 100
            },
            {
                "id": "ncb-claim-container",
                "label": "",
                "type": "sub-container",
                "children": [
                    {
                        "title": "Have you claimed in the past year?",
                        "type": "radio",
                        "required": true,
                        "label": null,
                        "options": [
                            {
                                "value": "Yes",
                                "key": "Yes"
                            },
                            {
                                "value": "No",
                                "key": "No"
                            }
                        ],
                        "validators": [],
                        "hidden": false,
                        "placeholder": "Have you claimed in the past year?",
                        "id": "id_claimed_last_year"
                    },
                    {
                        "title": "Previous Policy NCB",
                        "type": "radio",
                        "required": true,
                        "label": null,
                        "options": [
                            {
                                "value": "Yes",
                                "key": "Yes"
                            },
                            {
                                "value": "No",
                                "key": "No"
                            }
                        ],
                        "validators": [],
                        "hidden": false,
                        "placeholder": "Previous Policy NCB",
                        "id": "id_previous_ncb"
                    }
                ],
                "title": "Previous Policy NCB"
            },
            {
                "id": "bike-loan-container",
                "label": "",
                "type": "sub-container",
                "children": [
                    {
                        "title": "Bike currently on loan",
                        "type": "radio",
                        "required": true,
                        "label": null,
                        "options": [
                            {
                                "value": "Yes",
                                "key": "Yes"
                            },
                            {
                                "value": "No",
                                "key": "No"
                            }
                        ],
                        "validators": [],
                        "hidden": false,
                        "placeholder": "Bike currently on loan",
                        "id": "id_is_car_on_loan"
                    },
                    {
                        "title": "Loan Provider",
                        "type": "radio",
                        "required": true,
                        "label": null,
                        "options": [
                            {
                                "value": "Yes",
                                "key": "Yes"
                            },
                            {
                                "value": "No",
                                "key": "No"
                            }
                        ],
                        "validators": [],
                        "hidden": false,
                        "placeholder": "Loan Provider",
                        "id": "id_loan_provider"
                    }
                ],
                "title": "Loan Provider"
            }
        ],
        "desc": "if any"
    },
    {
        "title": "Address Details",
        "id": "api-key-ad-container",
        "submitLabel": "Next",
        "editLabel": "Edit",
        "type": "container",
        "isOpen": true,
        'children':[
            {
                'id': "address_tab",
                'type': 'tabbed-container',
                'children': [
                    {
                        "id": "registration_address",
                        "title": "Registration Address",
                        "type": "sub-container",
                        "isOpen": true,
                        "children": [
                            {
                                "minlength": null,
                                "min_value": 100000,
                                "title": "Pincode",
                                "type": "numeric",
                                "defaultValue": 400072,
                                "required": true,
                                "label": "Pincode",
                                "max_value": 999999,
                                "validators": [
                                    {
                                        "message": "Please enter a valid Area Pincode.",
                                        "expression": "^((1[1-9])|([2-9][0-9]))[0-9]{4}",
                                        "name": "regex"
                                    }
                                ],
                                "hidden": false,
                                "placeholder": "Pincode",
                                "id": "id_pincode",
                                "maxlength": null
                            },
                            {
                                "minlength": 0,
                                "title": "House/Building/Street",
                                "type": "text",
                                "required": true,
                                "label": null,
                                "validators": [],
                                "hidden": false,
                                "placeholder": "House/Building/Street",
                                "id": "id_address_field_one",
                                "maxlength": 100
                            },
                            {
                                "minlength": 0,
                                "title": "landmark",
                                "type": "text",
                                "required": true,
                                "label": null,
                                "validators": [],
                                "hidden": false,
                                "placeholder": "landmark",
                                "id": "id_landmark",
                                "maxlength": 100
                            },
                            {
                                "minlength": 0,
                                "title": "City",
                                "type": "text",
                                "required": true,
                                "label": null,
                                "validators": [],
                                "hidden": false,
                                "placeholder": "City",
                                "id": "id_city",
                                "maxlength": 100
                            },
                            {
                                "minlength": 0,
                                "title": "State",
                                "type": "text",
                                "required": true,
                                "label": null,
                                "validators": [],
                                "hidden": false,
                                "placeholder": "State",
                                "id": "id_state",
                                "maxlength": 100
                            },
                            {
                                "title": "CA Same",
                                "type": "radio",
                                "required": true,
                                "label": "CA Same",
                                "options": [
                                    {
                                        "value": true,
                                        "key": "Yes"
                                    },
                                    {
                                        "value": false,
                                        "key": "No"
                                    }
                                ],
                                "validators": [],
                                "hidden": false,
                                "placeholder": null,
                                "id": "id_gender"
                            }
                        ],
                        "desc": "if any"
                    },
                    {
                        "id": "communication_address",
                        "title": "Communication Address",
                        "type": "sub-container",
                        "isOpen": true,
                        "children": [
                            {
                                "minlength": null,
                                "min_value": 100000,
                                "title": "Pincode",
                                "type": "numeric",
                                "defaultValue": 400073,
                                "required": true,
                                "label": "Pincode",
                                "max_value": 999999,
                                "validators": [
                                    {
                                        "message": "Please enter a valid Area Pincode.",
                                        "expression": "^((1[1-9])|([2-9][0-9]))[0-9]{4}",
                                        "name": "regex"
                                    }
                                ],
                                "hidden": false,
                                "placeholder": "Pincode",
                                "id": "id_pincode2",
                                "maxlength": null
                            },
                            {
                                "minlength": 0,
                                "title": "House/Building/Street",
                                "type": "text",
                                "required": true,
                                "label": null,
                                "validators": [],
                                "hidden": false,
                                "placeholder": "House/Building/Street",
                                "id": "id_address_field_one2",
                                "maxlength": 100
                            },
                            {
                                "minlength": 0,
                                "title": "landmark",
                                "type": "text",
                                "required": true,
                                "label": null,
                                "validators": [],
                                "hidden": false,
                                "placeholder": "landmark",
                                "id": "id_landmark2",
                                "maxlength": 100
                            },
                            {
                                "minlength": 0,
                                "title": "City",
                                "type": "text",
                                "required": true,
                                "label": null,
                                "validators": [],
                                "hidden": false,
                                "placeholder": "City",
                                "id": "id_city2",
                                "maxlength": 100
                            },
                            {
                                "minlength": 0,
                                "title": "State",
                                "type": "text",
                                "required": true,
                                "label": null,
                                "validators": [],
                                "hidden": false,
                                "placeholder": "State",
                                "id": "id_state2",
                                "maxlength": 100
                            },
                            {
                                "title": "CA Same",
                                "type": "radio",
                                "required": true,
                                "label": "CA Same",
                                "options": [
                                    {
                                        "value": true,
                                        "key": "Yes"
                                    },
                                    {
                                        "value": false,
                                        "key": "No"
                                    }
                                ],
                                "validators": [],
                                "hidden": false,
                                "placeholder": null,
                                "id": "id_gender2"
                            }
                        ],
                        "desc": "if any"
                    }
                ]
            }
        ]
    }
]