import moment = require('moment')
import BaseStore = require('../../common/BaseStore')
import Actions = require('../actions/AppActions')
import MONTHS = require('../../common/constants/Months')
import APIUtils = require('../utils/APIUtils')
import KEYS = require('../constants/StorageMap')
import NCBConfig = require('../constants/NCBConfig')
import RegistryStore = require('./RegistryStore')

declare var isNaN: {
	(value: string | number):boolean
}

class PolicyStore extends BaseStore{
	constructor(_namespace:string){
		super(_namespace)
		this.registerDispatcherHandler(this._actionHandler)
		this.init(true)
	}

	getExpiryDate(): any{
		let DD = this.getSelected(KEYS.EXPIRY_DATE)
		let MM = this.getSelected(KEYS.EXPIRY_MONTH)
		let YYYY = this.getSelected(KEYS.EXPIRY_YEAR)
		return DD && MM && YYYY ? moment(("0"+DD).slice(-2)+'-'+MM+'-'+YYYY, 'DD-MM-YYYY') : moment()
	}

	getNewPolicyStartDate():any{
		// this date is expiry date + 1
		let DD = this.getSelected(KEYS.EXPIRY_DATE)
		let MM = this.getSelected(KEYS.EXPIRY_MONTH)
		let YYYY = this.getSelected(KEYS.EXPIRY_YEAR)
		return DD && MM && YYYY ? moment(("0"+ DD).slice(-2)+'-'+MM+'-'+YYYY, 'DD-MM-YYYY').add(1, 'day') : moment()
	}

	isExpired(): string{
		return this.getSelected(KEYS.POLICY_EXPIRED)
	}
	isNCBClaimed(): string{
		return this.getSelected(KEYS.IS_CLAIMED_LAST_YEAR)
	}
	isEligibleForNCB(): boolean{
		let isExpired = this.isExpired() || false
		let expiryDate = this.getExpiryDate()
		return !isExpired || (isExpired && this.isNotNinetyDaysOld(expiryDate))
	}
	getNCBList(): any[]{
		return NCBConfig.NCBOptions
	}
	getPreviousNCB(): number{
		let ncb = this.getSelected(KEYS.NCB_UNKNOWN)
		return ncb ? 'U' : this.getSelected(KEYS.PREVIOUS_NCB)
	}
	getQuoteRequestData(){
		return {
			"isClaimedLastYear": this.getSelected(KEYS.IS_CLAIMED_LAST_YEAR) || 0,
			"previousNCB": this.getSelected(KEYS.PREVIOUS_NCB),
			"isExpired": this.isExpired() ? 'true' : 'false',
			"ncb_unknown": this.getSelected(KEYS.NCB_UNKNOWN),
			"pastPolicyExpiryDate": this.getExpiryDate().format('DD-MM-YYYY'),
			"newPolicyStartDate": this.getNewPolicyStartDate().format('DD-MM-YYYY')
		}
	}

	private init(preventEmit?:boolean){
		//let obj = {}
		//obj[KEYS.POLICY_EXPIRED]=false
		//obj[KEYS.IS_CLAIMED_LAST_YEAR]='0'
		//this.setPolicyExpiry(false, true)
		this.setNCBClaimed('0', true)
		//this.setSelected(obj, preventEmit)
	}
	private isNotNinetyDaysOld(date: any):boolean{
		return moment().diff(date, 'days')<90
	}
	private setPolicyExpiry(isExpired: boolean, preventEmit?: boolean) {
		if(isExpired){
			this.setExpiryDate(moment().subtract(1, 'day'), true)
		}else{
			this.setExpiryDate(moment().add(7, 'day'), true)
		}
		this.setSelectedValue(KEYS.POLICY_EXPIRED, isExpired, preventEmit)
	}
	private setExpiryDate(date: any, preventEmit?: boolean){
		let obj = {}
		obj[KEYS.EXPIRY_DATE] = date.date()
		//obj[KEYS.EXPIRY_MONTH] = MONTHS[date.month()]
		obj[KEYS.EXPIRY_MONTH] = date.month()+1
		obj[KEYS.EXPIRY_YEAR] = date.year()
		this.setSelected(obj, preventEmit)
		if(!this.isEligibleForNCB())
			this.setPreviousNCB(NCBConfig.AgeWiseNCB[0], true)
	}
	private setNCBClaimed(flag: string, preventEmit?: boolean){
		let isClaimed = parseInt(flag)
		let obj = {}
		obj[KEYS.PREVIOUS_NCB] = isClaimed ? 0 : this.getDefaultNCB()
		obj[KEYS.IS_CLAIMED_LAST_YEAR] = isClaimed
		obj[KEYS.NCB_UNKNOWN] = true
		this.setSelected(obj, preventEmit)
	}
	private setPreviousNCB(value: string | number, preventEmit?: boolean) {
		let unknown = isNaN(value)
		let obj = {}
		if(unknown){
			obj[KEYS.NCB_UNKNOWN] = true
			obj[KEYS.PREVIOUS_NCB] = this.getDefaultNCB()
		}else{
			obj[KEYS.NCB_UNKNOWN] = undefined
			obj[KEYS.PREVIOUS_NCB] = value
		}
		this.replaceSelected(obj, preventEmit)
	}
	private getDefaultNCB():number{
		let ncb = 25
		try{
			let today = moment()
			let regDate = RegistryStore.getRegistrationDate()
			let diff = today.diff(regDate, 'years')
			if(6>diff)
				ncb = NCBConfig.AgeWiseNCB[diff]
			else
				ncb = NCBConfig.AgeWiseNCB[6]
		}catch(e){}
		return ncb
	}
	private updateQuoteParams(data:any){
		var quoteParams = data.quote_parameters
		var obj = {}
		obj[KEYS.NCB_UNKNOWN] = quoteParams.ncb_unknown == "true" ? true : false
		obj[KEYS.PREVIOUS_NCB] = parseInt(quoteParams.previousNCB)
		obj[KEYS.IS_CLAIMED_LAST_YEAR] = parseInt(quoteParams.isClaimedLastYear)

		obj[KEYS.POLICY_EXPIRED] = quoteParams.isExpired == 'true' ? true : false

		var expDate = quoteParams.pastPolicyExpiryDate.split('-')
		obj[KEYS.EXPIRY_DATE] = expDate[0]
		obj[KEYS.EXPIRY_MONTH] = expDate[1]
		obj[KEYS.EXPIRY_YEAR] = expDate[2]

		this.setSelected(obj,true)
	}

 	private _actionHandler:Function = (action:any) => {
		 switch(action.actionType) {
			 case Actions.setExpired['CONST']:
				this.setPolicyExpiry(action.value, true)
				break

			 case Actions.setExpiryDate['CONST']:
				this.setExpiryDate(action.value)
				break

			 case Actions.setNCBClaimsMade['CONST']:
				this.setNCBClaimed(action.value)
				break

			 case Actions.setPreviousNCB['CONST']:
				this.setPreviousNCB(action.value)
				break

			 case Actions.receiveVehicleInfo['CONST']:
				this.waitFor(RegistryStore)
				this.setNCBClaimed('0', true)
				break

			 case Actions.setRegistrationDate['CONST']:
			 	this.waitFor(RegistryStore)
				this.setNCBClaimed('0', true)
				break
			case Actions.receiveQuoteParams['CONST']:
				this.updateQuoteParams(action.data)
				break
		}
	}
}

export = new PolicyStore(KEYS.PARENT)
