import moment = require('moment')
import BaseStore = require('../../common/BaseStore')
import Actions = require('../actions/AppActions')
import MONTHS = require('../../common/constants/Months')
import DateUtils = require('../utils/DateUtils')
import APIUtils = require('../utils/APIUtils')
import KEYS = require('../constants/StorageMap')

class RegistryStore extends BaseStore{
	private _rtos: any[];	//id, name
	private _rtoMap: any;

	constructor(_namespace:string){
		super(_namespace)
		this.registerDispatcherHandler(this._actionHandler)
	}

	getRTOList(): any[] {
		return this._rtos || []
	}
	getYears(): number[] {
		return DateUtils.getRegistrationYears()
	}
	getSelectedRTO(): string{
		let selected = this.getSelected(KEYS.RTO.PARENT)
		return selected?selected[KEYS.RTO.ID]:''
	}
	getSelectedRTOVerbose(): string{
		let selected = this.getSelected(KEYS.RTO.PARENT)
		return selected?selected[KEYS.RTO.NAME]:''
	}
	getRegistrationDate(): any{
		let DD = this.getSelected(KEYS.REGISTRATION_DATE)
		let MM = this.getSelected(KEYS.REGISTRATION_MONTH)
		let YYYY = this.getSelected(KEYS.REGISTRATION_YEAR)
		return DD && MM && YYYY ? moment(("0"+DD).slice(-2)+'-'+MM+'-'+YYYY, 'DD-MM-YYYY') : null
	}
	getRegistrationYear(): string{
		let YYYY = this.getSelected(KEYS.REGISTRATION_YEAR)
		return YYYY ? YYYY :''
	}
	getManufacturingDate(): any{
		let DD = this.getSelected(KEYS.MANUFACTURING_DATE)
		let MM = this.getSelected(KEYS.MANUFACTURING_MONTH)
		let YYYY = this.getSelected(KEYS.MANUFACTURING_YEAR)
		return DD && MM && YYYY ? moment(("0"+DD).slice(-2)+'-'+MM+'-'+YYYY, 'DD-MM-YYYY') : null
	}
	getRegistrationNumber(): string[]{
		return this.getSelected(KEYS.REGISTRATION_NUMBER)
	}

	getFormattedRegNoArray(){
		var regNo = this.getSelected(KEYS.REGISTRATION_NUMBER)

		if(!regNo)
			return undefined

		var rto = regNo[1]
		if(regNo[0] != "GJ" && regNo[0] != "DL"){
			rto = ("0" + rto).slice(-2)
		}
		regNo[1] = rto
		return regNo
	}

	getQuoteRequestData(){
		let regNo = this.getFormattedRegNoArray()
		let regDate = this.getRegistrationDate()
		let manDate = this.getManufacturingDate()
		return {
			"registrationNumber": regNo ? regNo : this.getSelectedRTO().split('-').concat(['0','0']),
			"formatted_reg_number": regNo ? regNo.join('-') : '',
			"registrationDate": regDate ? regDate.format('DD-MM-YYYY') : '',
			"manufacturingDate": manDate ? manDate.format('DD-MM-YYYY') : ''
		}
	}

	private setRTOs(rtos):void{
		this._rtos = rtos
		this._rtoMap = {}
		for(let index in rtos){
			this._rtoMap[rtos[index].id] = rtos[index].name
		}
		this.emitChange()
	}
	private getToggledRTOIdFormat(rtoId: string){
		let rto = rtoId.split('-')
		if (parseInt(rto[1])<10){
			if(rto[1].length===2)
				rto[1] = rto[1].charAt(1)
			else
				rto[1] = '0' + rto[1]
		}
		return rto.join('-')
	}
	private setRegistryInfo(data){
		let regDate = data.fastlane.response.result.vehicle['regn_dt'].split('/')
		this.setSelectedRegistrationDate(regDate[2], regDate[1], regDate[0], true)
		this.setManufacturingDate(regDate[2], regDate[1], regDate[0], true)
	}
	private setSelectedRTO(rtoId:string, preventEmit?:boolean):void{
		let obj = {}
		obj[KEYS.RTO.PARENT]={}

		if (typeof this._rtoMap[rtoId] !== 'undefined'){
			obj[KEYS.RTO.PARENT][KEYS.RTO.ID] = rtoId
			obj[KEYS.RTO.PARENT][KEYS.RTO.NAME] = this._rtoMap[rtoId]
		}else{
			let toggledRTOId = this.getToggledRTOIdFormat(rtoId)
			if (typeof this._rtoMap[toggledRTOId] !== 'undefined') {
				obj[KEYS.RTO.PARENT][KEYS.RTO.ID] = toggledRTOId
				obj[KEYS.RTO.PARENT][KEYS.RTO.NAME] = this._rtoMap[toggledRTOId]
			}
		}
		this.replaceSelected(obj, preventEmit);
	}
	private setSelectedRegistrationDate(year:string, month?:string, date?:string, preventEmit?:boolean):void{
		let obj = {}
		obj[KEYS.REGISTRATION_DATE] = date
	//	obj[KEYS.REGISTRATION_MONTH] = month ? MONTHS[parseInt(month) - 1] : undefined
		obj[KEYS.REGISTRATION_MONTH] =  month
		obj[KEYS.REGISTRATION_YEAR] = year
		this.setSelected(obj, preventEmit)
	}
	private setManufacturingDate(year: string, month?: string, date?: string, preventEmit?: boolean): void {
		let obj = {}
		obj[KEYS.MANUFACTURING_DATE] = date
		//obj[KEYS.MANUFACTURING_MONTH] = month ? MONTHS[parseInt(month) - 1] : undefined
		obj[KEYS.MANUFACTURING_MONTH] = month
		obj[KEYS.MANUFACTURING_YEAR] = year
		this.setSelected(obj, preventEmit)
	}
	private selectRTOFromRegNo(regNo:any[]):void{
		var rtoCode = regNo[1]
		rtoCode = isNaN(rtoCode) ? rtoCode.substr(0, 1) : rtoCode

		let obj = {}
		obj[KEYS.RTO.PARENT]={}
		obj[KEYS.RTO.PARENT][KEYS.RTO.ID] = regNo[0] + '-' + rtoCode
		this.setSelected(obj)
		//this.setSelectedRTO(regNo[0] + '-' + rtoCode)
	}
	private updateQuoteParams(data:any){
		var obj ={}
		var rtoInfo = data.rto_info
		var quoteParams = data.quote_parameters
		obj[KEYS.RTO.PARENT] = {}
		obj[KEYS.RTO.PARENT][KEYS.RTO.ID] = rtoInfo['rto_code']
		obj[KEYS.RTO.PARENT][KEYS.RTO.NAME] = rtoInfo['rto_code'] + '-' + rtoInfo['rto_name']

		obj[KEYS.REGISTRATION_NUMBER] = quoteParams['registrationNumber[]']

		var regDate = quoteParams.manufacturingDate.split('-')
		obj[KEYS.REGISTRATION_DATE] = regDate[0]
		obj[KEYS.REGISTRATION_MONTH] = regDate[1]
		obj[KEYS.REGISTRATION_YEAR] = regDate[2]

		obj[KEYS.MANUFACTURING_DATE] = regDate[0]
		obj[KEYS.MANUFACTURING_MONTH] = regDate[1]
		obj[KEYS.MANUFACTURING_YEAR] = regDate[2]
		this.setSelected(obj,true)
	}

	private _actionHandler:Function = (action:any) => {
		switch(action.actionType) {
			case Actions.fetchRTOs['CONST']:
				if(!this._rtos)
					APIUtils.getRTOs()
				break

			case Actions.receiveRTOs['CONST']:
				this.setRTOs(action.value)
		 		break

		 	case Actions.receiveVehicleInfo['CONST']:
		 		this.setRegistryInfo(action.value)
		 		break

		 	case Actions.selectRTO['CONST']:
				this.setSelectedRTO(action.value)
				var regNo = [action.value.split('-')[0], action.value.split('-')[1], 0, 0]
				this.setSelectedValue(KEYS.REGISTRATION_NUMBER,regNo, true)
				if(action.callback)
				  action.callback()
				break

			case Actions.setRegistrationNumber['CONST']:
				this.setSelectedValue(KEYS.REGISTRATION_NUMBER, action.regNoArray, true)
				this.selectRTOFromRegNo(action.regNoArray)
				break

			case Actions.fetchVehicleInfo['CONST']:
				
				APIUtils.getVehicleInfo(this.getSelected(KEYS.REGISTRATION_NUMBER).join(''))
				break

			case Actions.setRegistrationDate['CONST']:
				this.setSelectedRegistrationDate(action.year, action.month, action.date, true)
				this.setManufacturingDate(action.year, action.month, action.date)
				break

			case Actions.receiveQuoteParams['CONST']:
				this.updateQuoteParams(action.data)
				break
		}
	}
}


export = new RegistryStore(KEYS.PARENT)
