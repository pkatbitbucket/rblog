import moment = require('moment')
import assign = require('object-assign')
import BaseStore = require('../../common/BaseStore')
import Actions = require('../actions/AppActions')
import APIUtils = require('../utils/APIUtils')
import KEYS = require('../constants/StorageMap')
import NCBConfig = require('../constants/NCBConfig')

import VehicleStore = require('./VehicleStore')
import RegistryStore = require('./RegistryStore')
import PolicyStore = require('./PolicyStore')
import CommonUtils = require('../../common/utils/CommonUtils')

declare var CSRF_TOKEN

declare var parseFloat: {
	(value: string | number): number
}

const FETCH_MAX_LIMIT = 8
const SERVICE_TAX_RATE = 0.145
const MAX_PREMIUM_CHANGE_THRESHOLD_PERCENT = 2.5 // this is percentage
const MIN_PREMIUM_CHANGE_THRESHOLD = 1 // this is abs value
const MAX_REDIRECT_TRIES = 50

class ResultStore extends BaseStore{
	private _getInsurersList: boolean
	private _fetchPlansAfterwards: boolean
	private _insurers: any[]
	private _quoteId: any
	private _asyncRequestTimeout: any
	private _asyncPromise: any
	private _fetchCounter: number
	private _rawPlans: any[]
	private _plans: any
	private _selectedPlan: string
	private _selectedCover: string[]
	private _finishedFetch: boolean
	private _maxOfMinIDV: number
	private _minOfMaxIDV: number
	private _maxIDV: number
	private _retryInsurersSlug: string[]
	private _zeroDepRange: any
	private _shortUrl:string = ''
	private _flowType:number = 0 // 0 - fastlane, 1 - fastLane failed, 2 - i dunno reg no
	private _isZeroDepAvailable = false
	private _proposalJSON: any
	private _isFetchingProposalData:boolean = true
	private _proposalValueMap: any
	private _proposalSummary: any
	private _proposalTranId: string
	private _proposalQuoteData: any
	private _isPaymentFailed:boolean = false
	private _paymentError:any = {}
	private _isPaymentResponseReceived:boolean = false
	private _proposalFormData:any = {}
	private _paymentGatewayPath:string
	private _isShowErrorInProposal:boolean = false
	private _proposalPremiumChangedOverThreshold:number = 0
	private _proposalPremiumChangedBelowThreshold:number = 0
	private _paymentRedirectTries:number = 0
	private _paymentRedirectInterval:any
	private _isAcceptedNewPremium:boolean = true
	private _isRejectedNewPremium:boolean = false

	constructor(_namespace:string){
		super(_namespace)
		this.registerDispatcherHandler(this._actionHandler)
		this.init(true)
	}

	getPlans(): any{
		return this._plans
	}
	getSelectedPlan(): any{
		return this._selectedPlan
	}
	getInsurers(): any[]{
		return this._insurers
	}
	hasFetchFinished(): boolean{
		return this._finishedFetch
	}
	getSelectedIDV(): number{
		return this.getSelected(KEYS.IDV) ? parseInt(this.getSelected(KEYS.IDV)): 0
	}
	getSelectedTenure(): number{
		return this.getSelected(KEYS.TENURE) || 1
	}
	isDepreciationWaiver(): boolean{
		return !!this.getSelected(KEYS.IS_DEPRECIATION_WAIVER)
	}
	getZeroDepAvailablity():boolean {
		return this._isZeroDepAvailable
	}
	getRetryInsurersSlug(): string[]{
		return this._retryInsurersSlug.slice()
	}
	getMaxIDV(){
		return this._maxIDV || 99999999
	}

	getIDVData(): any[]{
		let options = [{ key: 'For Best Deal', value: 0 }]
		if(this._maxOfMinIDV && this._maxOfMinIDV != 0 && this._maxOfMinIDV != this._maxIDV)
			options.push({ key: 'Rs. ' + this._maxOfMinIDV, value: this._maxOfMinIDV })		
		if(this._minOfMaxIDV && this._minOfMaxIDV != 0 && this._minOfMaxIDV != this._maxIDV)
			options.push({ key: 'Rs. ' + this._minOfMaxIDV, value: this._minOfMaxIDV })		
		options.push({ key: 'Highest Across Insurers', value: this._maxIDV || 99999999 })

		// returing sorted options accordin to IDV
		return options.sort(function(objA, objB){
			return objA.value - objB.value
		})
	}
	getTenureData(): any[]{
		return [
			{ key: '1 Year', value: 1 },
			{ key: '2 Years', value: 2 },
			{ key: '3 Years', value: 3 }]
	}
	getZeroDepData(): any[]{
		let yesSubtext, noSubtext
		if(this._zeroDepRange){
			yesSubtext = "Values from " + this._zeroDepRange['withMin'] + " to " + this._zeroDepRange['withMax']
			noSubtext = "Values from " + this._zeroDepRange['withoutMin'] + " to " + this._zeroDepRange['withoutMax']
		}
		return [{key:'Yes', value:true, subtext: yesSubtext},
				{key:'No', value:false, subtext: noSubtext}]
	}
	getQuoteId(){
		return this._quoteId
	}
	getTransactionId(){
		return this._proposalTranId
	}
	getShortUrl(){
		return this._shortUrl
	}
	getFlowType(){
		return this._flowType
	}
	getQuoteParams(): any{
		var quote = {
			"quoteId": this._quoteId ? this._quoteId : "",
			"isNCBCertificate":0,
			"addon_isDepreciationWaiver": CommonUtils.isTrue(this.isDepreciationWaiver()) ? 1 : 0 ,
			"addon_isKeyReplacement":0,
			"addon_isInvoiceCover":0,
			"addon_isNcbProtection":0,
			"addon_is247RoadsideAssistance":0,
			"addon_isEngineProtector":0,
			"addon_isDriveThroughProtected":0,
			"extra_paPassenger":0,
			"extra_isLegalLiability": 0,
			"extra_isAntiTheftFitted": 0,
			"extra_isMemberOfAutoAssociation": 0,
			"extra_isTPPDDiscount": 0,
			"extra_user_dob": null,
			"discountCode":null,
			"expirySlot":7,
			"flowType":this.getFlowType()
		}

		quote = assign(quote, VehicleStore.getQuoteRequestData(),
			RegistryStore.getQuoteRequestData(), PolicyStore.getQuoteRequestData())

		return quote
	}

	getProposalJSON(){
		return this._proposalJSON
	}
	getProposalValueMap(){
		return this._proposalValueMap
	}
	getProposalSummary(){
		return this._proposalSummary
	}
	getIsShowProposalError(){
		return this._isShowErrorInProposal
	}
	getProposalPremiumChangedOverThreshold(){
		return this._proposalPremiumChangedOverThreshold
	}
	getProposalPremiumChangedBelowThreshold(){
		return this._proposalPremiumChangedBelowThreshold
	}
	getIsFetchingProposalData(){
		return this._isFetchingProposalData
	}

	private init(preventEmit?: boolean){
		this.abortAsync()
		this._getInsurersList = false
		this._fetchPlansAfterwards = false
		//this._quoteId = undefined
		this._proposalTranId = undefined
		this._asyncRequestTimeout = undefined
		this._asyncPromise = undefined
		this._fetchCounter = 0
		this._rawPlans = []
		this._plans = {}
		this._selectedCover = []
		this._finishedFetch = false
		this._maxOfMinIDV = undefined
		this._minOfMaxIDV = undefined
		this._maxIDV = undefined
		this._retryInsurersSlug = []
		this._zeroDepRange = undefined
		if(!preventEmit){
			let obj = {}
			obj[KEYS.IDV] = 0
			obj[KEYS.TENURE] = 1
			this.setZeroDep(false)
			this.setSelected(obj)
		}
	}
	private calculateIDVValues(){
		if (!this._finishedFetch || !this._rawPlans || !this._rawPlans.length || this._minOfMaxIDV)
			return
		let maxOfMinIDV = this._maxOfMinIDV || this._rawPlans[0]['minIDV']
		let minOfMaxIDV = this._minOfMaxIDV || this._rawPlans[0]['maxIDV']
		let maxIDV = this._maxIDV || this._rawPlans[0]['maxIDV']
		for (let index = 1; index < this._rawPlans.length; index++) {

			if(this._rawPlans[index].maxIDV == undefined || this._rawPlans[index].minIDV == undefined)
				continue

			maxOfMinIDV = maxOfMinIDV == undefined || this._rawPlans[index]['minIDV'] > maxOfMinIDV ? this._rawPlans[index]['minIDV'] : maxOfMinIDV
			minOfMaxIDV = minOfMaxIDV == undefined || this._rawPlans[index]['maxIDV'] < minOfMaxIDV ? this._rawPlans[index]['maxIDV'] : minOfMaxIDV
			maxIDV = maxIDV == undefined || this._rawPlans[index]['maxIDV'] > maxIDV ? this._rawPlans[index]['maxIDV'] : maxIDV
		}
		this._maxOfMinIDV = maxOfMinIDV
		this._minOfMaxIDV = minOfMaxIDV
		this._maxIDV = maxIDV
	}
	private calculateZeroDepValues(){
		if (!this._finishedFetch || !this._rawPlans || !this._rawPlans.length || !this._zeroDepRange)
			return
		let originalVal = this.isDepreciationWaiver()
		let zeroDepRange = {}
		this.setZeroDep(false)
		let plans = this.getMassagedPlans(this._rawPlans)
		let min, max
		for(let key in plans){
			if(plans[key].premium == undefined)
				continue

			min = min ? ((plans[key].premium < min) ? plans[key].premium : min) : plans[key].premium
			max = max ? ((plans[key].premium > max) ? plans[key].premium : max) : plans[key].premium
		}
		zeroDepRange['withoutMin'] = min
		zeroDepRange['withoutMax'] = max
		this.setZeroDep(false)
		plans = this.getMassagedPlans(this._rawPlans)
		min = undefined
		max = undefined
		for (let key in plans) {
			if(plans[key].premium == undefined)
				continue

			min = min ? ((plans[key].premium < min) ? plans[key].premium : min) : plans[key].premium
			max = max ? ((plans[key].premium > max) ? plans[key].premium : max) : plans[key].premium
		}
		zeroDepRange['withMin'] = min
		zeroDepRange['withMax'] = max
		this.setZeroDep(originalVal)
		this._zeroDepRange = zeroDepRange
	}
	private getPremiumFromCovers(covers:any, serviceTaxRate:number) {
		let cover = 0, serviceTax = 0
		for (let index in covers) {
			if (covers[index].default || this._selectedCover.indexOf(covers[index].id) >= 0) {
				cover = cover + parseFloat(covers[index].premium)
				serviceTax = serviceTax + parseFloat(covers[index].premium * serviceTaxRate)
			}
		}
		return (cover + serviceTax)
	}
	private calculateAddonCovers(plan: any) {
		return this.getPremiumFromCovers(plan.premiumBreakup.addonCovers, SERVICE_TAX_RATE)
	}

	private calculateBasicCovers(plan: any) {
		return this.getPremiumFromCovers(plan.premiumBreakup.basicCovers, SERVICE_TAX_RATE)
	}

	private calculateDiscounts(plan: any) {
		return this.getPremiumFromCovers(plan.premiumBreakup.discounts, SERVICE_TAX_RATE)
	}

	private calculateSpecialDiscounts(plan: any) {
		return this.getPremiumFromCovers(plan.premiumBreakup.specialDiscounts, SERVICE_TAX_RATE)
	}
	private hasZeroDep(plan: any): boolean{
		let hasZeroDep = false
		let addons = plan.premiumBreakup.addonCovers
		for(let counter in addons){
			if(addons[counter]['id'] === 'isDepreciationWaiver' && addons[counter]['premium'] > 0){
				hasZeroDep = true
				break
			}
		}
		return hasZeroDep
	}
	private fetchQuote(sendFeedback: boolean = false){
		let quoteParams = this.getQuoteParams()
		if(!quoteParams.vehicleId){
			setTimeout(this.fetchQuote.bind(this, sendFeedback), 200)
			return
		}
		this._rawPlans = null
		this._finishedFetch = false
		this._plans = {}
		this.abortAsync()
		sendFeedback ? APIUtils.sendFastlaneFeedback(this.getSelected(KEYS.RDS_ID),
			VehicleStore.hasFastlaneDataChanged(), VehicleStore.getSelectedModel(), VehicleStore.getSelectedVariant(),
			APIUtils.getQuote.bind(null, quoteParams)) : APIUtils.getQuote(quoteParams)
	}
	private fetchPlans(isClearCache:boolean = false){
		this._fetchCounter = 0
		this.callAsyncAPI(isClearCache)
		if (this._asyncRequestTimeout) {
			clearInterval(this._asyncRequestTimeout)
		}
		this._asyncRequestTimeout = setInterval(this.callAsyncAPI.bind(this, isClearCache), 2500)
	}
	private setupPlans(){
			if(!this._plans)
				this._plans = {}
			this._plans = assign(this._plans, this.getMassagedPlans(this._rawPlans))
			this.calculateIDVValues()
			this.calculateZeroDepValues()
	}
	private getMassagedPlans(rawPlans: any[]){
		let plans = {}
		this._isZeroDepAvailable = false
		for (let index in rawPlans) {
			let rawPlan = rawPlans[index]
			let plan: any = {}
			if(rawPlan.errorCode != undefined){
				plan.insurerSlug = rawPlan.insurerSlug
				plan.errorCode = rawPlan.errorCode
				plan.errorMsg = rawPlan.errorMsg
				plans[rawPlan.insurerSlug] = plan
			}
			else {
				let isZeroDepEnabled = this.isDepreciationWaiver()			
				plan.insurerName = rawPlan.insurerName
				plan.insurerSlug = rawPlan.insurerSlug
				plan.idv = rawPlan.calculatedAtIDV
				plan.addonCoversAmount = this.calculateAddonCovers(rawPlan)
				plan.basicCoversAmount = this.calculateBasicCovers(rawPlan)
				plan.discountAmount = this.calculateDiscounts(rawPlan)
				plan.premium = Math.round(plan.addonCoversAmount + plan.basicCoversAmount + plan.discountAmount)
				plan.serviceTax = Math.round((plan.premium / (1 + SERVICE_TAX_RATE)) * (SERVICE_TAX_RATE))
				plan.hasZeroDep = this.hasZeroDep(rawPlan)
				plan.isDepreciationWaiver = CommonUtils.isTrue(isZeroDepEnabled) ? plan.hasZeroDep : false
				plan.requestedTenureAvailable =  true //this.getSelectedTenure()==1 || !Math.round(Math.random())	
				plans[rawPlan.insurerSlug] = plan

				// checking if any of the plans has zero dep available
				this._isZeroDepAvailable = this._isZeroDepAvailable || plan.hasZeroDep
			}	
		}
		this.sortPlans(plans)
		return plans
	}
	private sortPlans(plans):any{
		if(!this._insurers)
			return
		for(let index in this._insurers){
			this._insurers[index]['premium'] = plans[this._insurers[index]['slug']] && plans[this._insurers[index]['slug']].errorCode == undefined ? 
												plans[this._insurers[index]['slug']]['premium'] : 999999999 + index
		}
		this._insurers.sort(function(a, b){
			let ap = a.premium ? a.premium : 999999999
			let bp = b.premium ? b.premium : 999999999
			return ap - bp
		})
	}
	private processAsyncResponse(newPlans:any[], finished:boolean){
		this._rawPlans = newPlans
		this._fetchCounter++
		this._finishedFetch = finished
		if (this._finishedFetch || this._fetchCounter >= FETCH_MAX_LIMIT) {
			this._finishedFetch = true
			clearInterval(this._asyncRequestTimeout)
		}
		this.setupPlans()
	}
	private abortAsync(){
		if(this._asyncPromise)
			this._asyncPromise.abort()
		if(this._asyncRequestTimeout)
			clearInterval(this._asyncRequestTimeout)
	}
	private callAsyncAPI(isClearCache){
		this._asyncPromise = APIUtils.fetchAsync(this._quoteId, isClearCache)
	}
	private setZeroDep(flag:boolean){
		this.setSelectedValue(KEYS.IS_DEPRECIATION_WAIVER, CommonUtils.isTrue(flag) ? 1 : 0, true)
		let index = this._selectedCover ? this._selectedCover.indexOf('isDepreciationWaiver') : -1
		if(!CommonUtils.isTrue(flag) && index >= 0){
			this._selectedCover.splice(index, 1)
		} else if (CommonUtils.isTrue(flag) && index < 0) {
			this._selectedCover.push('isDepreciationWaiver')
		}
	}
	private processProposalData(data: any){
		this._proposalSummary = {}
		this._proposalSummary["status"] = data.transaction_status
		data = data.data
		this._proposalSummary["make"] = data.form_details.master_make
		this._proposalSummary["model"] = data.form_details.master_model
		this._proposalSummary["variant"] = data.form_details.master_variant
		this._proposalSummary["regNo"] = data.form_details.registration_no
		this._proposalSummary["premium"] = Math.round(data.quote_response.final_premium)
		this._proposalSummary["idv"] = data.quote_response.calculatedAtIDV
		this._proposalSummary["zeroDep"] = CommonUtils.isTrue(data.quote_parameters.addon_isDepreciationWaiver) && this.hasZeroDep(data.quote_response)
		this._proposalSummary['flowType'] = data.quote_parameters['flowType']
		this._proposalSummary['rtoInfo'] = data.form_details.rto_info
		this._proposalSummary['quoteId'] = data.quote_parameters['quoteId']
		this._proposalQuoteData = data.quote_parameters
		//the key comes as registrationNumber[] because while posting thorugh superagent array is sent like this.. need to check why
		this._proposalQuoteData['registrationNumber'] = data.quote_parameters['registrationNumber[]']

		this._proposalJSON = data.form_details.form
		this._proposalValueMap = data.user_form_details
		this._proposalFormData = data.user_form_details
	}
	private processTransactionUpdate(data:any){
		let hasChanged = false
		if(data.registrationDate != undefined && this._proposalQuoteData['registrationDate']!= data.registrationDate){
			this._proposalQuoteData['registrationDate']=data.registrationDate
			this._proposalQuoteData['manufacturingDate']=data.registrationDate
			hasChanged = true
		}
		if(data.pastPolicyExpiryDate != undefined && this._proposalQuoteData['pastPolicyExpiryDate']!=data.pastPolicyExpiryDate){
			this._proposalQuoteData['pastPolicyExpiryDate']=data.pastPolicyExpiryDate
			hasChanged = true
		}
		if(data.previousNCB != undefined && this._proposalQuoteData['previousNCB']!=data.previousNCB){
			this._proposalQuoteData['previousNCB']=data.previousNCB
			hasChanged = true
		}
		if (data.isClaimedLastYear != undefined && this._proposalQuoteData['isClaimedLastYear'] != data.isClaimedLastYear) {
			this._proposalQuoteData['isClaimedLastYear'] = data.isClaimedLastYear
			hasChanged = true
		}
		if(hasChanged == true)
			APIUtils.updateTransaction(this._proposalTranId, this._proposalQuoteData, true)
		else 	
		{
			this._isPaymentResponseReceived = false
			APIUtils.makePayment(this._selectedPlan, this._proposalTranId, this._proposalFormData)
		}
	}

	private updateQuoteParams(data:any){
		var quoteParams = data.quote_parameters
		this._flowType = parseInt(quoteParams['flowType'])
	}

	private handlePaymentResponse(data:any, path:string){
		this._isPaymentFailed = !data.success
		if(!this._isPaymentFailed)
			this._paymentGatewayPath = path
		else
			this._paymentGatewayPath = undefined
	}

	private resetProposalData(){
		 this._proposalJSON = undefined
		 this._isFetchingProposalData = true
		 this._proposalValueMap = undefined
		 this._proposalSummary = undefined
		 this._proposalTranId = undefined
		 this._proposalQuoteData = undefined
		 this._isPaymentFailed = false
		 this._paymentError = undefined
		 this._isPaymentResponseReceived = false
		 this._proposalFormData = undefined
		 this._paymentGatewayPath = undefined
		 this._isShowErrorInProposal = false
		 this._proposalPremiumChangedOverThreshold = 0
		 this._proposalPremiumChangedBelowThreshold = 0
		 this._paymentRedirectTries =0
		 if(this._paymentRedirectInterval)
		 	clearInterval(this._paymentRedirectInterval)

		 this._isAcceptedNewPremium = true
		 this._isRejectedNewPremium = false
	}



 	private _actionHandler:Function = (action:any) => {
		switch(action.actionType) {
			case Actions.receiveVehicleInfo['CONST']:
			 	//this.waitFor(VehicleStore, PolicyStore, RegistryStore)
			 	//this.fetchQuote()
				break
				
			case Actions.getInsurers['CONST']:
				APIUtils.getActiveInsurers()
				break

			case Actions.setExpired['CONST']:
				this._quoteId = undefined
				this.emitChange()
				break

			case Actions.fetchQuote['CONST']:
				this._quoteId = undefined
				//this.emitChange()
			 	this.fetchQuote(action.sendFastLaneFeedback)
			 	this._fetchPlansAfterwards = !!(action.fetchPlanAfterward)
				break

			case Actions.fetchPlans['CONST']:
				this.waitFor(VehicleStore)
				this._getInsurersList = !!(action.getInsurersList && !(this._insurers || this._insurers.length))
				this._getInsurersList ? APIUtils.getActiveInsurers() : this.fetchPlans()
				break

			case Actions.fetchForInsurer['CONST']:
				this._retryInsurersSlug.push(action.value)
				this._asyncPromise = APIUtils.fetchForInsurer(this.getQuoteParams(), action.value, true)
				this.emitChange()
				break

			case Actions.receiveInsurers['CONST']:
				this._insurers = action.value ? action.value.slice() : []
				this._getInsurersList ? this.fetchPlans() : this.emitChange()
				break

			case Actions.receiveQuote['CONST']:
				this._quoteId = action.value
				if(this._fetchPlansAfterwards){
					this._fetchPlansAfterwards = false
					this.fetchPlans()
				}
				this.emitChange()
				break

			case Actions.receivePlans['CONST']:
				this.processAsyncResponse(action.plans, action.finished)
				this.emitChange()
				break

			case Actions.invalidateQuoteId['CONST']:
				this._quoteId = undefined
				if(action.isEmit)
					this.emitChange()

			case Actions.receiveForInsurer['CONST']:
				let slugIndex = this._retryInsurersSlug.indexOf(action.insurerSlug)
				if (slugIndex > -1) {
					this._retryInsurersSlug.splice(slugIndex)
				}
				if(action.plan){
					this._rawPlans.push(action.plan)
					this.setupPlans()
				}
				else if(action.errorCode && action.errorMsg){
					var plan = {}
					plan['insurerSlug'] = action.insurerSlug
					plan['errorCode'] = action.errorCode
					plan['errorMsg'] = action.errorMsg
					this._rawPlans.push(plan)
					this.setupPlans()
				}
				this.emitChange()
				break

			case Actions.abortAsync['CONST']:
				this.abortAsync()
				break

			case Actions.selectMake['CONST']:
			 	this.init()
			 	break

			 case Actions.setResultParams['CONST']:
			 	this._quoteId = action.quoteId
			 	//APIUtils.fetchQuoteParameters(this._quoteId)
			 	action.idv != undefined ? this.setSelectedValue(KEYS.IDV, action.idv, true) : null
			 	action.zeroDep != undefined ? this.setZeroDep(action.zeroDep) : null
			 	this._getInsurersList = !(this._insurers && this._insurers.length)
				this._getInsurersList ? APIUtils.getActiveInsurers() : this.fetchPlans()
			 	break

			case Actions.setDepreciationWaiver['CONST']:
				this.setZeroDep(action.value)
				this.setupPlans()
				this.emitChange()
				break

			case Actions.setIDV['CONST']:
				console.log("SET IDV")
				this.setSelectedValue(KEYS.IDV, action.value, true)
				this._fetchPlansAfterwards = true
				this.fetchQuote()
				this.emitChange()
				break

			case Actions.setTenure['CONST']:
				this.setSelectedValue(KEYS.TENURE, action.value)
				break

			case Actions.buyPlan['CONST']:
				this._proposalTranId = undefined
				this.emitChange()
				this.abortAsync()
				this._selectedPlan = action.value
				APIUtils.gotoTransaction(this.getQuoteParams(), this._selectedPlan)
				break

			case Actions.fetchQuoteParams['CONST']:
				APIUtils.fetchQuoteParameters(action.quoteId)
				break

			case Actions.shareQuotes['CONST']:
				APIUtils.shareQuotes(action.quoteId, action.url, action.shareType, action.value)
				break

			case Actions.receiveShortURL['CONST']:
				this._shortUrl = action.shortUrl
				this.emitChange()
				break

			case Actions.receiveQuoteParams['CONST']:
				this.updateQuoteParams(action.data)
				break

			case Actions.callMe['CONST']:
				APIUtils.sendLeadsToLMS("MOTOR", "call-form", CommonUtils.simpleClone(this.getQuoteParams()), false)
				break

			case Actions.setFlowType['CONST']:
				this._flowType = action.type
				if(action.callback)
					action.callback()
				break

			case Actions.receiveProposal['CONST']:
				this.waitFor(VehicleStore, RegistryStore, PolicyStore)
				//let jsonUrl = action.url
				this._proposalTranId = action.transactionId
				this.emitChange()
				break

			case Actions.fetchProposalJSON['CONST']:
				this._selectedPlan = action.insurer
				this._proposalTranId = action.transactionId
				this._isFetchingProposalData = true
				APIUtils.getBikeProposalJSON(this._proposalTranId, this._selectedPlan)
				this.emitChange()
				break

			case Actions.receiveProposalJSON['CONST']:
				this._isFetchingProposalData = false
				this.processProposalData(action.data)
				this.emitChange()
				break

			case Actions.postProposalData['CONST']:
				 this._proposalFormData = action.data
				APIUtils.postProposalData(this._proposalTranId, this._selectedPlan, action.data)
				break

			case Actions.makePayment['CONST']:
				this._isPaymentResponseReceived = false
				APIUtils.makePayment(this._selectedPlan, this._proposalTranId, this._proposalFormData)
				break

			case Actions.setTransactionParams['CONST']:
				this._selectedPlan = action.slug
				this._proposalTranId = action.id
				break

			case Actions.fetchUpdatedTransactionWithSubmit['CONST']:
				this._isAcceptedNewPremium = true
				this._isRejectedNewPremium = false
				this.processTransactionUpdate(action.data)
				break
			case Actions.receiveUpdatedTransactionWithSubmit['CONST']:
				this._isPaymentResponseReceived = false
				APIUtils.makePayment(this._selectedPlan, this._proposalTranId, this._proposalFormData)

				var premium: number = parseInt(action.data['quote_response']['final_premium'])
				if(premium - this._proposalSummary["premium"]  > (MAX_PREMIUM_CHANGE_THRESHOLD_PERCENT * this._proposalSummary["premium"] /100.0))
				{
				this._proposalPremiumChangedOverThreshold = Math.abs(premium - this._proposalSummary["premium"])
				this._isAcceptedNewPremium = false
				}
				else if(Math.abs(premium -this._proposalSummary["premium"]) > MIN_PREMIUM_CHANGE_THRESHOLD)
					this._proposalPremiumChangedBelowThreshold = premium - this._proposalSummary["premium"]

				this._proposalSummary["premium"] = premium

				this.emitChange()
				break
			case Actions.receivePaymentReponse['CONST']:
				this._isPaymentResponseReceived = true
				this.handlePaymentResponse(action.data, action.path)
				break
			case Actions.redirectToGateWay['CONST']:
				this._paymentRedirectInterval = setInterval(function(){
													this._paymentRedirectTries++
													if(this._paymentRedirectTries >= MAX_REDIRECT_TRIES || this._isRejectedNewPremium == true)
														clearInterval(this._paymentRedirectInterval)

													if(!this._isPaymentResponseReceived || this._isAcceptedNewPremium == false)
														return
													else{
														clearInterval(this._paymentRedirectInterval)
														this._isShowErrorInProposal = this._isPaymentFailed
														if(!this._isPaymentFailed && this._paymentGatewayPath != undefined)
															window.location.href = this._paymentGatewayPath
														this.emitChange()
													}
												}.bind(this),2000)
				break
				
			case Actions.acceptNewProposalPremium['CONST']:
				this._proposalPremiumChangedOverThreshold = 0
				this._proposalPremiumChangedBelowThreshold = 0
				this._isAcceptedNewPremium = true
				this.emitChange()
				break

			case Actions.resetProposalPremiumDiff['CONST']:
				this._proposalPremiumChangedOverThreshold = 0
				this._proposalPremiumChangedBelowThreshold = 0
				this._isRejectedNewPremium = true
				break

			case Actions.pushLeadsToLMS['CONST']:
				APIUtils.sendLeadsToLMS("MOTOR", action.label, CommonUtils.simpleClone(this._proposalFormData), action.offlineCase)
				break

			case Actions.resetProposalData['CONST']:
				this.resetProposalData()
				break
		}
	}
}

export = new ResultStore(KEYS.PARENT)