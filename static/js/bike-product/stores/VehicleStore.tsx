import moment = require('moment')
import BaseStore = require('../../common/BaseStore')
import Set = require('../../common/Set')
import Cookie = require('react-cookie')
import Actions = require('../actions/AppActions')
import MONTHS = require('../../common/constants/Months')
import APIUtils = require('../utils/APIUtils')
import GTMUtils = require('../utils/GTMUtils')
import KEYS = require('../constants/StorageMap')
import RegistryStore = require('./RegistryStore')
import PolicyStore = require('./PolicyStore')

var PopularBikes = []

class VehicleStore extends BaseStore{
	private _makes: any[];		//id, name
	private _models: any[];		//id, name
	private _variants: any[];	//id, name
	private _selectedMake: any;
	private _fStatus: number;

	constructor(_namespace:string){
		super(_namespace)
		this.registerDispatcherHandler(this._actionHandler)
		this.init(true)
	}

	private init(preventEmit?:boolean){
		this._fStatus = 3
		let obj = {}
		obj[KEYS.IS_NEW_VEHICLE] = 0
		obj[KEYS.IS_USED_VEHICLE] = 0
		obj[KEYS.IDV] = 0
		obj[KEYS.IDV_ELECTRICAL] = 0
		obj[KEYS.IDV_NON_ELECTRICAL] = 0
		obj[KEYS.VOLUNTARY_DEDUCTIBLE] = 0
		obj[KEYS.HAS_OPTIMIZED_QUOTES] = false
		obj[KEYS.FASTLANE_MODEL_CHANGED] = this.getSelected(KEYS.FASTLANE_MODEL_CHANGED) || false
		obj[KEYS.FASTLANE_VARIANT_CHANGED] = this.getSelected(KEYS.FASTLANE_VARIANT_CHANGED) || false
		this.setSelected(obj, preventEmit)
	}

	getMakes(): any[] {
		return this._makes ? this._makes.slice() : []
	}
	getModels(): any[] {
		return this._models ? this._models.slice() : []
	}
	getVariants(): any[] {
		return this._variants ? this._variants.slice() : []
	}
	getSelectedMake(): number{
		let selected = this.getSelected(KEYS.MAKE.PARENT)
		return selected?selected[KEYS.MAKE.ID]:''
	}
	getSelectedModel(): number{
		let selected = this.getSelected(KEYS.VEHICLE.PARENT)
		return selected?selected[KEYS.VEHICLE.ID]:''
	}
	getSelectedVariant(): number{
		let selected = this.getSelected(KEYS.VEHICLE_VARIANT.PARENT)
		return selected?selected[KEYS.VEHICLE_VARIANT.ID]:''
	}
	getSelectedMakeVerbose(): string{
		let selected = this.getSelected(KEYS.MAKE.PARENT)
		return selected?selected[KEYS.MAKE.NAME]:''
	}
	getSelectedModelVerbose(): string{
		let selected = this.getSelected(KEYS.VEHICLE.PARENT)
		return selected?selected[KEYS.VEHICLE.NAME]:''
	}
	getSelectedVariantVerbose(): string{
		let selected = this.getSelected(KEYS.VEHICLE_VARIANT.PARENT)
		return selected?selected[KEYS.VEHICLE_VARIANT.NAME]:''
	}
	getFastlaneSuccessStatus(): number{
		let status = this.getSelected(KEYS.FASTLANE_SUCCESS)
		return isNaN(status) ? this._fStatus : status
	}
	hasFastlaneDataChanged(): boolean{
		return this.getSelected(KEYS.FASTLANE_MODEL_CHANGED)
			|| this.getSelected(KEYS.FASTLANE_VARIANT_CHANGED)
	}
	hasFastLaneModelChanged():boolean{
		return this.getSelected(KEYS.FASTLANE_MODEL_CHANGED)
	}
	getQuoteRequestData(){
 		return {
 			"vehicleId" : this.getSelectedVariant(),
 			"isNewVehicle" : this.getSelected(KEYS.IS_NEW_VEHICLE),
 			"isUsedVehicle" : this.getSelected(KEYS.IS_USED_VEHICLE),
 			"rds_id" : this.getSelected(KEYS.RDS_ID),
 			"idv" : this.getSelected(KEYS.IDV),
 			"idvElectrical" : this.getSelected(KEYS.IDV_ELECTRICAL),
 			"idvNonElectrical" : this.getSelected(KEYS.IDV_NON_ELECTRICAL),
 			"voluntaryDeductible" : this.getSelected(KEYS.VOLUNTARY_DEDUCTIBLE),
 			"isCNGFitted":0,
		  	"cngKitValue":0,
 			"fastlane_success" : this.getSelected(KEYS.FASTLANE_SUCCESS),
 			"fastlane_model_changed" : this.getSelected(KEYS.FASTLANE_MODEL_CHANGED),
 			"fastlane_fueltype_changed" : false,
 			"fastlane_variant_changed" : this.getSelected(KEYS.FASTLANE_VARIANT_CHANGED)
 		}
 	}

	private getFilteredBikeList(vehicles){
		let map = {}
		let popularPositions = []
		for(let index in vehicles){
			map[vehicles[index]['id']+''] = index
		}
		for(let index in PopularBikes){
			let position = map[PopularBikes[index]['id']]
			if(!isNaN(position) && position>=0){
				popularPositions.push(position)
			}
		}
		popularPositions.sort(function(a, b) { return b - a })
		for(let index in popularPositions){
			vehicles.splice(popularPositions[index], 1)
		}
		return PopularBikes.concat(vehicles)
	}
	private setVehicleModels(vehicles):void{
		this._models = vehicles
		let selectedVehicle = this.getSelected(KEYS.VEHICLE.PARENT)
		if(selectedVehicle && selectedVehicle[KEYS.VEHICLE.ID] && !selectedVehicle[KEYS.VEHICLE.NAME]){
			let obj = {}
			obj[KEYS.VEHICLE.ID]=selectedVehicle[KEYS.VEHICLE.ID]
			for (let index = 0; this._models.length > index; index++) {
				if (this._models[index]['id'] == obj[KEYS.VEHICLE.ID]) {
					obj[KEYS.VEHICLE.NAME] = this._models[index]['name']
					break
				}
			}
			this.setSelectedValue(KEYS.VEHICLE.PARENT, obj, true)
		}
		this.emitChange()
	}
	private setVariants(variants):void{
		this._variants = variants
		if(this._variants && this._variants.length === 1)
			this.setSelectedVariant(this._variants[0]['id'], true)
		this.emitChange()
	}
	private setVehicleInfoData(data):void{
		let obj = {}
		obj[KEYS.FASTLANE_SUCCESS] = 1
		obj[KEYS.RDS_ID] = data['rds_id']
		obj[KEYS.FASTLANE_VEHICLE_MODEL_ID] = data['vehicle_model_id']
		obj[KEYS.FASTLANE_VEHICLE_MASTER_ID] = data['vehicle_master_id']

		obj[KEYS.VEHICLE.PARENT]={}
		obj[KEYS.VEHICLE.PARENT][KEYS.VEHICLE.ID] = data['vehicle_model_id']
		obj[KEYS.VEHICLE.PARENT][KEYS.VEHICLE.NAME] =  data['vehicle_make'] + ' ' + data['vehicle_model']

		obj[KEYS.VEHICLE_VARIANT.PARENT]={}
		obj[KEYS.VEHICLE_VARIANT.PARENT][KEYS.VEHICLE_VARIANT.ID] = data['vehicle_master_id']
		obj[KEYS.VEHICLE_VARIANT.PARENT][KEYS.VEHICLE_VARIANT.NAME] = data['vehicle_variant'] + '(' + data['vehicle_cc'] + ' CC)'
		
		obj[KEYS.RTO.PARENT] = {}
		obj[KEYS.RTO.PARENT][KEYS.RTO.ID] = data['rto_id']
		obj[KEYS.RTO.PARENT][KEYS.RTO.NAME] = data['rto_name']

		this.setSelected(obj, true)
	}
	private setSelectedMake(makeId: number, isFetchModels:boolean): void {
		if(makeId == this.getSelectedMake() && this._models && this._models.length)
			return
		
		this.clearSelected(KEYS.VEHICLE.PARENT, true)
		this.clearSelected(KEYS.VEHICLE_VARIANT.PARENT, true)
		let makeName = ''
		for(let index in this._makes){
			if(this._makes[index]['id']===makeId){
				makeName = this._makes[index]['name']
			}
		}

		var obj = {}
		obj[KEYS.MAKE.PARENT]={}
		obj[KEYS.MAKE.PARENT][KEYS.MAKE.ID] = makeId
		obj[KEYS.MAKE.PARENT][KEYS.MAKE.NAME] = makeName
		this.setSelected(obj)
		/*this._selectedMake = {
			id: makeId,
			name: makeName
		}*/
		//this.emitChange()
		if(isFetchModels){
			this._models = []
			this._variants = []
			APIUtils.getVehicleModels(makeId)
		}
	}
	private setSelectedModel(modelId: number, isFetchVariants:boolean, preventEmit?: boolean): void {
		if(modelId == this.getSelectedModel())
			return
		
		this.clearSelected(KEYS.VEHICLE_VARIANT.PARENT, true)
		let obj = {}
		obj[KEYS.VEHICLE.PARENT]={}
		obj[KEYS.VEHICLE.PARENT][KEYS.VEHICLE.ID]=modelId
		for (let index = 0; this.getModels().length > index; index++) {
			if (this.getModels()[index].id == modelId) {
				obj[KEYS.VEHICLE.PARENT][KEYS.VEHICLE.NAME] = this.getModels()[index]['name']
				break
			}
		}
		this.replaceSelected(obj, preventEmit)

		if(isFetchVariants){
			this._variants=[]
			APIUtils.getVehicleVariants(modelId)
		}
	}
	private setSelectedVariant(variantId: number, preventEmit?:boolean):void{
		let obj = {}
		obj[KEYS.VEHICLE_VARIANT.PARENT]={}
		obj[KEYS.VEHICLE_VARIANT.PARENT][KEYS.VEHICLE_VARIANT.ID]=variantId
		for(let index=0; this.getVariants().length>index; index++){
			if(this.getVariants()[index].id==variantId){
				obj[KEYS.VEHICLE_VARIANT.PARENT][KEYS.VEHICLE_VARIANT.NAME] = this.getVariants()[index]['name']
				break
			}
		}
		this.replaceSelected(obj, preventEmit)
	}
	private setIncorrectFastlane(model?: boolean, variant?: boolean) {
		let obj = {}

		if (model) {
			let fastlanedata = this.getSelected(KEYS.FASTLANE_VEHICLE_MODEL_ID)
			let currentdata = this.getSelected(KEYS.VEHICLE.PARENT)
			obj[KEYS.FASTLANE_MODEL_CHANGED] = currentdata ? (currentdata[KEYS.VEHICLE.ID] != fastlanedata) : false
		}
		if (variant) {
			let fsData = this.getSelected(KEYS.FASTLANE_VEHICLE_MASTER_ID)
			let cuData = this.getSelected(KEYS.VEHICLE_VARIANT.PARENT)
			obj[KEYS.FASTLANE_VARIANT_CHANGED] = cuData ? (cuData[KEYS.VEHICLE_VARIANT.ID] != fsData) : false
		}

		this.setSelected(obj, true)
	}
	private updateQuoteParams(data:any){
		var quoteParams = data.quote_parameters
		let obj = {}
		obj[KEYS.IS_NEW_VEHICLE] = parseInt(quoteParams.isNewVehicle)
		obj[KEYS.IS_USED_VEHICLE] = parseInt(quoteParams.isUsedVehicle)
		obj[KEYS.IDV] = parseInt(quoteParams.idv)
		obj[KEYS.IDV_ELECTRICAL] = parseInt(quoteParams.idvElectrical)
		obj[KEYS.IDV_NON_ELECTRICAL] = parseInt(quoteParams.idvNonElectrical)
		obj[KEYS.VOLUNTARY_DEDUCTIBLE] = parseInt(quoteParams.voluntaryDeductible)
		obj[KEYS.HAS_OPTIMIZED_QUOTES] = false
		obj[KEYS.FASTLANE_MODEL_CHANGED] = quoteParams.fastlane_model_changed == 'true' ? true : false
		obj[KEYS.FASTLANE_VARIANT_CHANGED] =  quoteParams.fastlane_variant_changed == 'true' ? true : false
	
		obj[KEYS.FASTLANE_SUCCESS] = parseInt(quoteParams.fastlane_success)
		obj[KEYS.RDS_ID] = parseInt(quoteParams.rds_id)
		obj[KEYS.FASTLANE_VEHICLE_MODEL_ID] =  parseInt(data['vehicle_model_id'])
		obj[KEYS.FASTLANE_VEHICLE_MASTER_ID] = parseInt(quoteParams['vehicleId'])
		obj[KEYS.FASTLANE_MODEL_CHANGED] = quoteParams['fastlane_model_changed'] == 'true' ? true : false
		obj[KEYS.FASTLANE_VARIANT_CHANGED] = quoteParams['fastlane_variant_changed'] == 'true' ? true : false

		obj[KEYS.MAKE.PARENT]={}
		obj[KEYS.MAKE.PARENT][KEYS.MAKE.ID] = -1
		obj[KEYS.MAKE.PARENT][KEYS.MAKE.NAME] = data['vehicle_make_name']

		obj[KEYS.VEHICLE.PARENT]={}
		obj[KEYS.VEHICLE.PARENT][KEYS.VEHICLE.ID] = parseInt(data['vehicle_model_id'])
		obj[KEYS.VEHICLE.PARENT][KEYS.VEHICLE.NAME] =  data['vehicle_model_name']

		obj[KEYS.VEHICLE_VARIANT.PARENT]={}
		obj[KEYS.VEHICLE_VARIANT.PARENT][KEYS.VEHICLE_VARIANT.ID] = parseInt(quoteParams['vehicleId'])
		obj[KEYS.VEHICLE_VARIANT.PARENT][KEYS.VEHICLE_VARIANT.NAME] = data['vehicle_variant'] + '(' + data['vehicle_cc'] + ' CC)'

		this.setSelected(obj,true)
	}

 	private _actionHandler:Function = (action:any) => {
		 switch(action.actionType) {
			 case Actions.fetchVehicleMakes['CONST']:
				 if (!this._makes){
					 APIUtils.getVehicleMakes(action.callback)
				 } else {
				 	if(action.callback)
				 		action.callback()
				 }
		 	 	break

			 case Actions.receiveVehicleMakes['CONST']:
				this._makes = action.value
				if(action.callback)
					action.callback()
				this.emitChange()
		 		break

			 case Actions.receiveVehicleModels['CONST']:
				this.setVehicleModels(action.value)
		 		break

			 case Actions.receiveVehicleVariants['CONST']:
		 		this.setVariants(action.value)
		 		break
			 	
			 case Actions.receiveVehicleInfo['CONST']:
			 	this.waitFor(PolicyStore, RegistryStore)
				this.setVehicleInfoData(action.value)
				break

			 case Actions.selectMake['CONST']:
			 	this.setSelectedMake(action.value, action.isFetchModels)
			 	this.setIncorrectFastlane(true)
		 		if(action.callback)
		 			action.callback()
			 	break

			 case Actions.selectModel['CONST']:
				this.setSelectedModel(action.value, action.isFetchVariants)
			 	this.setIncorrectFastlane(true)
		 		if(action.callback)
		 			action.callback()
		 		break

			 case Actions.selectVariant['CONST']:
				this.setSelectedVariant(action.value, false)
			 	this.setIncorrectFastlane(false, true)
				if (action.callback)
					action.callback()
				break

			 case Actions.isNewVehicle['CONST']:
			 	this.clearSelected(undefined, true)
				this.init(true)
				this.setSelectedValue(KEYS.IS_NEW_VEHICLE, 1)
				if (action.callback)
					action.callback()
				break

			 case Actions.fetchVehicleInfo['CONST']:
			 	this._fStatus = 2
			 	break;

			 case Actions.clearVehicleInfo['CONST']:
				this.clearSelected(undefined, true)
				this.init()
				if(action.callback)
					action.callback()
			 	break

			 case Actions.vehicleInfoFetchFailed['CONST']:
			 	this.setSelectedValue(KEYS.FASTLANE_SUCCESS, 0)
			 	this.setSelectedValue(KEYS.IS_NEW_VEHICLE, 0)
			 	this.setSelectedValue(KEYS.RDS_ID, action.data ? action.data.rds_id : '')
			 	if (action.callback)
					action.callback()
				this.emitChange()
			 	break
			 case Actions.receiveQuoteParams['CONST']:
			 	this.updateQuoteParams(action.data)
			 	break
			 case Actions.fetchVehicleModels['CONST']:
			 	APIUtils.getVehicleModels(action.makeId)
			 	break
			 case Actions.fetchVehicleVariants['CONST']:
			 	APIUtils.getVehicleVariants(action.modelId)
			 	break
			 case Actions.clearModelList['CONST']:
			 	this._models = []
			 	break
			 case Actions.clearVariantList['CONST']:
			 	this._variants = []
			 	break
		}
	}
}

export = new VehicleStore(KEYS.PARENT)