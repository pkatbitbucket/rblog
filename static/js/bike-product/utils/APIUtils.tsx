import request = require('superagent')
import qs = require('qs')
import Cookie = require('react-cookie')
import Set = require('../../common/Set')
import CommonAPIs = require('../../common/utils/APIUtils')
import AppActions = require('../actions/AppActions')

declare var CSRF_TOKEN: string
var VEHICLE_TYPE = 'twowheeler';

function htmlSubmit(path: string, data?: any, method?: string) {
	method = method || "post"

    let form = document.createElement("form")
    form.setAttribute("method", method)
    form.setAttribute("action", path)
    for (let key in data) {
        if (data.hasOwnProperty(key)) {
            let hiddenField = document.createElement("input")
            hiddenField.setAttribute("type", "hidden")
            hiddenField.setAttribute("name", key)
            hiddenField.setAttribute("value", data[key])

            form.appendChild(hiddenField)
		}
    }
    //document.body.appendChild(form)
    form.submit()
}

function getVehicleMakes(callback:Function){
	request
		.get('/motor/' + VEHICLE_TYPE + '/api/vehicle-make/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.end(function(err, res){
			if(res.ok){
				callback(JSON.parse(res.text).data)
			}else{
				console.log("Something went wrong", err)
			}
		});
}

function getVehicleModels(makeId: string | number, callback:Function){
	request
		.get('/motor/' + VEHICLE_TYPE + '/api/vehicle-model/' + makeId)
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.end(function(err, res){
			if(res.ok){
				callback(JSON.parse(res.text).data)
			}else{
				console.log("Something went wrong", err)
			}
		});
}

function getVehicleVariants(modelId: string | number, callback:Function){
	request
		.post('/motor/' + VEHICLE_TYPE + '/api/vehicles/' + modelId + '/variants/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.send({ 'csrfmiddlewaretoken': CSRF_TOKEN })
		.end(function(err, res){
			if(res.ok){
				callback(JSON.parse(res.text).data.variants)
			} else{
				console.log("Something went wrong", err)
			}
		});
}

function getRTOs(callback:Function){
	request
		.post('/motor/' + VEHICLE_TYPE + '/api/rtos/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.send({ 'csrfmiddlewaretoken': CSRF_TOKEN })
		.end(function(err, res){
			if(res.ok){
				callback(JSON.parse(res.text).data.rtos)
			} else {
				console.log("Something went wrong", err)
				if(err.status!=404)
					setTimeout(getRTOs.bind(null, callback), 1000)
			}
		});
}

function getVehicleInfo(regNo: string, callback: Function, failureCallback: Function) {
	request
		.post('/motor/' + VEHICLE_TYPE + '/api/vehicle-info/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.send({
			'name': regNo,
			'csrfmiddlewaretoken': CSRF_TOKEN
		})
		.end(function(err, res) {
			if(res){
				if (res.ok)
					res = JSON.parse(res.text)
				if (res.data && !!res.data['vehicle_master_id'] && !!res.data['vehicle_model_id'] && !!res.data.fastlane.response.result.vehicle['regn_dt'])
					callback(res.data)
				else
					failureCallback(res.data)
			} else {
				failureCallback()
				console.log("Something went wrong", err)
			}
		})
}

function getActiveInsurers(callback: Function) {
	request
		.get('/apis/motor/' + VEHICLE_TYPE + '/active-insurers/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.end(function(err, res) {
			if(res){
				if (res.ok){
					res = JSON.parse(res.text)
					callback(res.data)
				}else{
					console.log("Something went wrong", err)
				}
			} else {
				console.log("Something went wrong", err)
			}
		})
}

function getQuote(quote: any, callback: Function) {
	quote.csrfmiddlewaretoken = CSRF_TOKEN
	request
		.post('/motor/' + VEHICLE_TYPE + '/api/quotes-async/')
		.type('application/x-www-form-urlencoded')
		.send(qs.stringify(quote, { arrayFormat: 'brackets' }))
		.end(function(err, res) {
			if (res.ok) {
				var response = JSON.parse(res.text);
				callback(response.data["quoteId"])
			} else {
				console.log("Something went wrong", err);
				//setTimeout(function(){getQuote(quote, callback)}, 500)
			}
		})
}

function fetchAsync(quoteId: string, isClearCache:boolean, callback: Function):any {
	var req = request
		.post('/motor/' + VEHICLE_TYPE + '/api/quotes-async/' + quoteId + '/?force_quote_refresh=' + isClearCache.toString())
		.type('application/x-www-form-urlencoded')
		.send({
			'csrfmiddlewaretoken': CSRF_TOKEN
		})
		.end(function(err, res) {
			if (res.ok) {
				var response = JSON.parse(res.text)
				response.data ? callback(response.data.premiums, response.data.finished) : callback([], true)
			} else {
				console.log(err, "Something went wrong")
			}
		})
	return req
}

function fetchForInsurer(quote: any, insurerSlug: string, isClearCache:boolean, callback: Function){
	quote.csrfmiddlewaretoken = CSRF_TOKEN
	var req = request
		.post('/motor/' + VEHICLE_TYPE + '/api/quotes/' + quote.quoteId + '/refresh/' + insurerSlug + '/?force_quote_refresh=' + isClearCache.toString())
		.type('application/x-www-form-urlencoded')
		.send(qs.stringify(quote, { arrayFormat: 'brackets' }))
		.end(function(err, res) {
			if (res.ok) {
				var response = JSON.parse(res.text)
				callback(response.data.quote, insurerSlug, response.errorCode, response.errorMessage)
			} else {
				console.log("Something went wrong", err);
			}
		})
	return req
}

function gotoTransaction(quote: any, insurerSlug: string, callback: Function) {
    quote.csrfmiddlewaretoken = CSRF_TOKEN
    request
		.post('/motor/' + VEHICLE_TYPE + '/api/confirm/' + quote.quoteId + '/' + insurerSlug + '/')
		.type('application/x-www-form-urlencoded')
		.send(qs.stringify(quote, { arrayFormat: 'brackets' }))
		.end(function(err, res) {
			if (res.ok) {
				let response = JSON.parse(res.text)
				//let formURL = '/motor/' + VEHICLE_TYPE + '/api/' + insurerSlug + '/desktopForm/' + response.data.transactionId + '/'
				callback(response.data.transactionId)
			} else {
				console.log("Something went wrong", err);
				//setTimeout(function(){gotoTransaction(quote, insurerSlug, callback)}, 500)
			}
		})
}

function getBikeProposalJSONTest(callback: Function) {
    request
		.get('/motor/test-component-page/')
		.type('application/x-www-form-urlencoded')
		.end(function(err, res) {
			if (res.ok) {
				let response = JSON.parse(res.text)
				if(response)
					callback(response)
			} else {
				console.log("Something went wrong", err);
			}
		})
}

function getBikeProposalJSON(tranId:string, insurerSlug:string, callback: Function) {
    request
		.get('/motor/' + VEHICLE_TYPE + '/api/' + insurerSlug + '/desktopForm/' + tranId + '/')
		.type('application/x-www-form-urlencoded')
		.set('Accept', 'application/json')
		.end(function(err, res) {
			if (res.ok) {
				let response = JSON.parse(res.text)
				if(response){
					callback(response)
				}
			} else {
				console.log("Something went wrong", err);
			}
		})
}

function postProposalData(tranId:string, insurerSlug:string, data:any) {
	data['csrfmiddlewaretoken'] = CSRF_TOKEN
    request
	.post('/motor/' + VEHICLE_TYPE + '/api/' + insurerSlug + '/desktopForm/' + tranId + '/')
	.set('Content-Type', 'application/x-www-form-urlencoded')
	.send(data).end(function(err, res){ })
}

function makePayment(slug: string, transactionId: string, data: any, callback:Function) {
	var path = '/motor/' + VEHICLE_TYPE + '/' + slug + '/submitForm/' + transactionId + '/'
	data['csrfmiddlewaretoken'] = CSRF_TOKEN
	request
	.post(path)
	.set('Content-Type', 'application/x-www-form-urlencoded')
	.send(data).end(function(err, res){
		if(res.ok){
			callback(res.body, path)
			/*if(res.success)
				window.location.href = path
			else
				alert("SORRY :( Try Again!")*/
		}
	})
}

function updateTransaction(transactionId: string, data: any, isClearCache:boolean, callback:Function) {	
	data['csrfmiddlewaretoken'] = CSRF_TOKEN
    request
		.post('/motor/' + VEHICLE_TYPE + '/api/transaction/' + transactionId + '/refresh-quote/?force_quote_refresh=' + isClearCache.toString())
		.type('application/x-www-form-urlencoded')
		.send(qs.stringify(data, { arrayFormat: 'brackets' }))
		.end(function(err, res) {
			if (res.ok) {
				callback(res.body.data, callback)
			} else {
				console.log("Something went wrong", err)
			}
		})
}

function sendFastlaneFeedback(rds_id: number, confirmation_type: string, data: any, callback?:Function){
	request
		.post('/motor/' + VEHICLE_TYPE + '/api/vehicle-confirmation/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.send({
			'data': JSON.stringify(data),
			'confirmation_type' : confirmation_type,
			'rds_id': rds_id,
			'csrfmiddlewaretoken': CSRF_TOKEN
		})
		.end(function(err, res){
			if(callback)
				callback()
		});
}

function getAreaFromPinCode(insurerSlug:string, pincode:string, callback?:Function){
	    request
		.post('/motor/' + VEHICLE_TYPE + '/api/pincode-details/' + insurerSlug + '/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.send({
	        'pincode' : pincode,
	        'csrfmiddlewaretoken' : CSRF_TOKEN
			})
		.end(function(err, res){
			if(callback)
				callback()
		});
}

function getCitiesFromState(insurerSlug:string, state:string, callback?:Function){
	    request
		.post('/motor/' + VEHICLE_TYPE + '/api/cities/' + insurerSlug + '/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.send({
	        'state' : state,
	        'csrfmiddlewaretoken' : CSRF_TOKEN
			})
		.end(function(err, res){
			if (res.ok) {
					if(res.body)
						callback(res.body.data.cities)
				} else {
					console.log("Something went wrong", err)
				}
		});
}
function fetchQuoteParameters(quoteId:string, callback?:Function){
	    request
		.post('/motor/' + VEHICLE_TYPE + '/api/quotes/' + quoteId + '/')
		.set('Content-Type', 'application/x-www-form-urlencoded')

		.send({
	        'csrfmiddlewaretoken' : CSRF_TOKEN
			})
		.end(function(err, res){
			if (res.ok) {
				callback(res.body.data)
			} else {
				console.log("Something went wrong", err)
			}
		});
}

function shareQuotes(quoteId:string, url:string, type:string, value:string, callback?:Function){
	var phoneNumber = ''
	var emailId = ''
	var data = {}
	data['url'] = url
	data['csrfmiddlewaretoken'] = CSRF_TOKEN
	data['quote_id'] = quoteId
	switch(type){
		case 'SMS':
			data['type'] = 'message'
			data['number'] = value
			break
		case 'EMAIL':
		 	data['type'] = 'email'
		 	data['email'] = value
		 	break
		case 'URL':
			data['type'] = 'shortlink'
			break
	}
    request
	.post('/motor/quotes/share/')
	.set('Content-Type', 'application/x-www-form-urlencoded')
	.send(data)
	.end(function(err, res){
		if (res.ok) {
				callback(res.body.shorturl)
			} else {
				console.log("Something went wrong", err)
			}
	});
}


function sendLeadsToLMS(campaign: string, label: string, data:any, offlineCase?:boolean, callback?: Function){
	data["campaign"] = campaign
    data["label"] = label
    data["mobile"] = Cookie.load('mobileNo')
    data["email"] = Cookie.load('email')
    
    if(!data["mobile"] && !data["email"]){
    	if(callback)
    		callback()
    	return
    }

	if(offlineCase)
        data["lms_campaign"] = "motor-offlinecases";

    request
	.post('/leads/save-call-time/')
	.set('Content-Type', 'application/x-www-form-urlencoded')
	.send({ 'data' : JSON.stringify(data), 'csrfmiddlewaretoken' : CSRF_TOKEN })
	.end(function(err, res){
		if(callback)
			callback()
	});
}

var APIs = {
	getVehicleMakes: function(callback?:Function){
		getVehicleMakes(AppActions.receiveVehicleMakes.bind(this, callback))
	},
	getVehicleModels: function(makeId){
		getVehicleModels(makeId, AppActions.receiveVehicleModels)
	},
	getVehicleVariants: function(modelId) {
		getVehicleVariants(modelId, AppActions.receiveVehicleVariants)
	},
	getRTOs: function(){
		getRTOs(AppActions.receiveRTOs)
	},
	getVehicleInfo: function(regNo: string) {
		getVehicleInfo(regNo, AppActions.receiveVehicleInfo, AppActions.vehicleInfoFetchFailed)
	},
	sendFastlaneFeedback: function(rdsId: number, dataChanged: boolean, modelId: number, masterId: number, callback?: Function){
		let data = {
			'vehicle_model_id': modelId,
			'vehicle_master_id': masterId
		}
		let confirmationType = dataChanged ? 'changed' : 'accepted'
		sendFastlaneFeedback(rdsId, confirmationType, data, callback)
	},
	getQuote: function(quote: any){
		getQuote(quote, AppActions.receiveQuote)
	},
	fetchAsync: function(quoteId: string, isClearCache:boolean){
		return fetchAsync(quoteId, isClearCache, AppActions.receivePlans)
	},
	fetchForInsurer: function(quote: any, insurerSlug: string,isClearCache:boolean){
		return fetchForInsurer(quote, insurerSlug, isClearCache, AppActions.receiveForInsurer)
	},
	getActiveInsurers: function(callback?: Function){
		getActiveInsurers(function(data){
			AppActions.receiveInsurers(data)
		})
	},
	gotoTransaction: function(quote: any, insurerSlug: string){
		gotoTransaction(quote, insurerSlug, AppActions.receiveProposal)
	},
	updateTransaction: function(transationId: string, data: any, isClearCache:boolean){
		updateTransaction(transationId, data, isClearCache, AppActions.receiveUpdatedTransactionWithSubmit)
	},
	getBikeProposalJSON:function(tranId:string, insurerSlug:string){
		getBikeProposalJSON(tranId, insurerSlug, AppActions.receiveProposalJSON)
	},
	shareQuotes: function(quoteId:string, url:string, type:string, value:string ){
		shareQuotes(quoteId, url, type, value, AppActions.receiveShortURL)
	},
	getBikeProposalJSONTest: getBikeProposalJSONTest,
	sendLeadsToLMS: sendLeadsToLMS,
	fetchQuoteParameters:function(quoteId:string){
		fetchQuoteParameters(quoteId, AppActions.receiveQuoteParams)
	},
	postProposalData: postProposalData,
	getAreaFromPinCode: getAreaFromPinCode,
	getCitiesFromState: getCitiesFromState,
	makePayment: function(slug: string, transactionId: string, data: any){
		makePayment(slug, transactionId, data, AppActions.receivePaymentReponse)
	}
}

if (!CSRF_TOKEN)
	CommonAPIs.getCSRF()

export = APIs