import Container = require('../../common/json-jsx-parser/Container')
import TRIGGERS = require('../../common/json-jsx-parser/triggers')
import APIUtils = require('./APIUtils')
import moment = require('moment')
import ResultStore = require('../stores/ResultStore')
import Actions = require('../actions/AppActions')

export = function(self: Container){
	var triggers = {
		/*'LastYearClaimChangeDependency': function(value) {
			let child = self.getChild('previous_ncb')
			value == "1" ? child.hide() : child.show()
		},*/
		'OnLoanChangeDependency': function(value) {
			let child = self.getChild('financier-name')
			value == "1" ? child.show() : child.hide()
		},
		/*'AddressDependency': function(value){
			let child = self.getChild('registration_address_container')
			value === true ? child.hide() : child.show()
		},*/
		'GenderMaritalStatusNomineeDependency': function(value) {
			let nomineeDDL = self.getChild('nominee_relationship')
			let nomineeList = JSON.parse(JSON.stringify(nomineeDDL.props.childProps.dataSource))
			let genderControl = self.getChild('cust_gender')
			let maritalControl = self.getChild('cust_marital_status')
			let gender = genderControl.getValue()
			let maritalStatus = maritalControl.getValue()
			let foundIndex = -1
			if (gender && maritalStatus) {
				nomineeDDL.addMutatedProps("disabled", false)
			}
			if (gender == "Female" && maritalStatus == "1") {
				for (let i = 0; i < nomineeList.length; i++) {
					if (nomineeList[i].key == "Wife") {
						foundIndex = i
						break
					}
				}
				nomineeList.splice(foundIndex, 1)
				foundIndex = -1
			}
			if (gender == "Male" && maritalStatus == "1") {
				for (let i = 0; i < nomineeList.length; i++) {
					if (nomineeList[i].key == "Husband") {
						foundIndex = i
						break
					}
				}
				nomineeList.splice(foundIndex, 1)
				foundIndex = -1
			}
			if (maritalStatus == "0") {
				var husbandIndex = -1
				var wifeIndex = -1
				for (let i = 0; i < nomineeList.length; i++) {
					if (nomineeList[i].key == "Husband") {
						husbandIndex = i
					}
				}
				nomineeList.splice(husbandIndex, 1)
				for (let i = 0; i < nomineeList.length; i++) {
					if (nomineeList[i].key == "Wife") {
						wifeIndex = i
					}
				}
				nomineeList.splice(wifeIndex, 1)
			}
			nomineeDDL.addMutatedProps("dataSource", nomineeList)

		},
		'PinCodeCommAddressDependency': function(value) {
			/*var insurerSlug = ResultStore.getSelectedPlan()
			var pinCodeControl = self.getChild(value)
			var pinCode = pinCodeControl.getValue()
			var handlePinCodeResponse = function(insurerSlug:string, state:string){
				APIUtils.getCitiesFromState(insurerSlug,state)
			}
			APIUtils.getAreaFromPinCode(insurerSlug,pinCode,handlePinCodeResponse.bind(this,insurerSlug))*/
		},
		'PinCodeRegAddressDependency': function(value) {
			/*var insurerSlug = ResultStore.getSelectedPlan()
			var pinCodeControl = self.getChild(value)
			var pinCode = pinCodeControl.getValue()
			var handlePinCodeResponse = function(insurerSlug:string, state:string){
				APIUtils.getCitiesFromState(insurerSlug,state)
			}
			APIUtils.getAreaFromPinCode(insurerSlug,pinCode,handlePinCodeResponse.bind(this,insurerSlug))*/
		},
		'CheckForNewQuote': function(value) {
			/*var regDateControl = self.getChild("registration_date")
			var expDateControl = self.getChild("past_policy_expiry_date")
			var ncb = self.getChild("previous_ncb").getValue()
			var data = {
				"registrationDate": regDateControl.getValue(),
				"pastPolicyExpiryDate": expDateControl.getValue(),
				"previousNCB": ncb.getValue(),
				"pastPolicyDateFormat": expDateControl.props.childProps["format"],
				"regDateFormat": regDateControl.props.childProps["format"]
			}
			Actions.fetchUpdatedTransaction(data)*/
		},
		//used on reg address same as comm address field
		'AddressDependency': function(value) {
			var tabbedContainer = self.getChild("address_tab_container")
			let regSubContainer = self.getChild('registration_address_container')
			if (value == "0") {
				regSubContainer.show()
				tabbedContainer.showTab(1)
			}
			else {
				regSubContainer.hide()
				tabbedContainer.hideTab(1)
			}
		},
		'handleRegAddress':function(value){
			var regStateControl = self.getChild("reg_add_state")
			var initialVal = regStateControl.props.childProps["values"]
			var tabbedContainer = self.getChild("address_tab_container")
			let regSubContainer = self.getChild('registration_address_container')
			if(initialVal != value){
				regSubContainer.show()
				tabbedContainer.showTab(1)
			}
			else {
				regSubContainer.hide()
				tabbedContainer.hideTab(1)
			}
		},
		'ParentNomineeAgeDependencies': function(value) {
			var nomineeRelationControl = self.getChild("nominee_relationship")
			var relation = nomineeRelationControl.getValue()
			if (relation == "Father" || relation == "Mother") {
				var dobControl = self.getChild("cust_dob")
				var dobFormat = dobControl.props.childProps["format"]
				if (!dobFormat) {
					dobFormat = "DD-MM-YYYY"
				}
				var dob = moment(dobControl.getValue(), dobFormat, true)

				var customerAge = moment().diff(dob, "years", true)
				if (value - customerAge < 18) {
					return false
				}
			}
		},
		'ChildNomineeAgeDependencies': function(value) {
			var nomineeRelationControl = self.getChild("nominee_relationship")
			var relation = nomineeRelationControl.getValue()
			if (relation == "Son" || relation == "Daughter") {
				var dobControl = self.getChild("cust_dob")
				var dobFormat = dobControl.props.childProps["format"]
				if (!dobFormat) {
					dobFormat = "DD-MM-YYYY"
				}
				var dob = moment(dobControl.getValue(), dobFormat, true)

				var customerAge = moment().diff(dob, "years", true)
				if (customerAge - value < 18) {
					return false
				}
			}
		},
		'GetCommCitiesFromState': function(value){
			var cityControl = self.getChild("add_city")
			var callback = function(cityList:any){
				cityControl.addMutatedProps("dataSource", cityList)
			}
			var insurerSlug = ResultStore.getSelectedPlan()
			APIUtils.getCitiesFromState(insurerSlug, value, callback)
		},
		'GetRegCitiesFromState': function(value){
			var cityControl = self.getChild("reg_add_city")
			var callback = function(cityList:any){
				cityControl.addMutatedProps("dataSource", cityList)
			}
			var insurerSlug = ResultStore.getSelectedPlan()
			APIUtils.getCitiesFromState(insurerSlug, value, callback)
		},
		'NCBClaimVisibility': function(value){
			var calendarControl = self.getChild("past_policy_expiry_date")
			var pastPolicyDate = calendarControl ? calendarControl.getValue() : undefined
			var format = calendarControl ? calendarControl.props.childProps['format'] : 'DD-MM-YYYY'
			var claimedLastYearControl = self.getChild("claimed_last_year")
			var isClaimedLastYear = claimedLastYearControl ? claimedLastYearControl.getValue() : undefined
			var pastPolicyInsurerControl = self.getChild("past_policy_insurer")
			var pastPolicyNumberControl = self.getChild("past_policy_number")
			var ncbControl = self.getChild("previous_ncb")
			if(!format)
				format = "DD-MM-YYYY"
			if(ncbControl && pastPolicyDate != undefined && moment().startOf('day').diff(moment(pastPolicyDate,format).startOf('day'), 'days') > 90){
				if(ncbControl)
					ncbControl.hide() 
				if(claimedLastYearControl)
					claimedLastYearControl.hide() 
				if(pastPolicyInsurerControl)
					pastPolicyInsurerControl.hide()
				if(pastPolicyNumberControl)
					pastPolicyNumberControl.hide()
			}
			else if(ncbControl && isClaimedLastYear != undefined && isClaimedLastYear == "1"){
				if(ncbControl)
					ncbControl.hide()
				if(claimedLastYearControl)
					claimedLastYearControl.show()
				if(pastPolicyInsurerControl)
					pastPolicyInsurerControl.show()
				if(pastPolicyNumberControl)
					pastPolicyNumberControl.show() 
			}
			else{
				if(claimedLastYearControl)
					claimedLastYearControl.show() 
				if(ncbControl)
					ncbControl.show()
				if(pastPolicyInsurerControl)
					pastPolicyInsurerControl.show()
				if(pastPolicyNumberControl)
					pastPolicyNumberControl.show() 
			}
		}
	}
	
	triggers[TRIGGERS.ON_AFTER_SUBMIT_CONTAINER] = function(id) {
		var calendarControl = self.getChild("past_policy_expiry_date")
		var pastPolicyExpiryDate = calendarControl ? calendarControl.getValue() : undefined
		var format = calendarControl ? calendarControl.props.childProps['format'] : 'DD-MM-YYYY'
		pastPolicyExpiryDate = moment(pastPolicyExpiryDate, format)

		Actions.pushLeadsToLMS("proposal-form", moment().startOf('day').isAfter(pastPolicyExpiryDate))
	}

	triggers[TRIGGERS.ON_BEFORE_SUBMIT_CONTAINER] = function(id) {
		var tabbedContainer
		var subContainer0
		var subContainer1
		switch (id) {
			case "address_container":
				tabbedContainer = self.getChild("address_tab_container")
				subContainer0 = self.getChild("communication_address_container") //
				subContainer1 = self.getChild("registration_address_container")
				if(!subContainer0 || !subContainer1)
				{
					return true
				}
				else {
					return handleTabbedContainer(tabbedContainer, subContainer0, subContainer1)
				}
				break;
			case "vehicle_details_container":
			   var isMoveAhead = false
			   tabbedContainer = self.getChild("vehicle_details_tabbedcontainer")			 
			   subContainer0 = self.getChild("vehicle_details_subcontainer") //
			   subContainer1 = self.getChild("past_policy_subcontainer")
			   
			   if(!subContainer0 || !subContainer1)
			   {
			   	isMoveAhead = true
			   }
			   else {
			   	isMoveAhead = handleTabbedContainer(tabbedContainer, subContainer0, subContainer1)
			   }

			   if(isMoveAhead){
			   	/*var callback = function(){
			   		Actions.makePayment()
			   	}*/
			   	var changedData = handleChangeQuote()
			   	Actions.fetchUpdatedTransactionWithSubmit(changedData)
			   }	   
			   return isMoveAhead
			   break;
			default:
				break;
		}
		return true
	}

	triggers[TRIGGERS.BEFORE_PUSH_DATA_TRIGGER] = function(value){
		switch (value){
			
		}
	}

	triggers[TRIGGERS.ON_BEFORE_TAB_CHANGE] = function(value) {
	}

	triggers[TRIGGERS.ON_BEFORE_TABBED_CONTAINER_SUMMARY_RENDER] = function(id) {
		switch (id) {
			case "address_tab_container":
				var addressTabContainer = self.getChild(id)
				return addressTabContainer.getHiddenTabs()
				break
		}
		return false
	}

	function handleChangeQuote(){
		var regDateControl = self.getChild("registration_date")
		var expDateControl = self.getChild("past_policy_expiry_date")
		var ncbControl = self.getChild("previous_ncb")
		var claimedLastYearControl = self.getChild("claimed_last_year")
		var data = {}
		if(regDateControl){
			data['registrationDate'] = regDateControl.getValue()
		}
		if(expDateControl){
			data['pastPolicyExpiryDate'] = expDateControl.getValue()
		}
		if(ncbControl){
			data['previousNCB'] = ncbControl.getValue()
		}
		if(claimedLastYearControl){
			data['isClaimedLastYear'] = claimedLastYearControl.getValue() == "1" ? 1 : 0
			if(claimedLastYearControl.getValue() == "1"){
				data['previousNCB'] = 0
			}
		}
		return data
	}

	function handleHiddenTabsSummary(id){
		var tabbedContainer = self.getChild(id)
		return tabbedContainer.getHiddenTabs()
	}

	//This function handles the logic of shifting tabs of tabbed container when NEXT is pressed
	function handleTabbedContainer(tabbedContainer, subContainer0 , subContainer1){
		var currentTab = tabbedContainer.getSelectedTab()
		var visitedTabs = tabbedContainer.getVisitedTabs()
		switch(currentTab){
			case 0:
			let isValid0 = subContainer0.validate(true)
			var hiddenTabs = tabbedContainer.getHiddenTabs()
			if(isValid0){
				if(subContainer1 && hiddenTabs.indexOf(1) == -1){
					tabbedContainer.goToTab(1)
					return false
				}
				else {
					return true
				}
				/*if(subContainer1 && hiddenTabs.indexOf(1) == -1 && !subContainer1.validate(false, true, false)){
					tabbedContainer.goToTab(1)
					return false
				}
				else {
					tabbedContainer.goToTab(1)
					return false
				}*/
			}
			else {
				return false
			}
			break
			case 1:
			let isValid1 = subContainer1.validate(true)
			if(isValid1){
				if(subContainer0 && !subContainer0.validate(false, true, false)){
					tabbedContainer.goToTab(0)
					return false
				}
				else {
					return true
				}
			}
			else {
				return false
			}
			break
			default:
				return false
				break
		}
	}
	return triggers
}