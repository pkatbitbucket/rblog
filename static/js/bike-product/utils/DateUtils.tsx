var utils = {
	getCurrentYear: function(){
		return (new Date()).getFullYear()
	},
	getCurrentMonth: function(){
		return (new Date()).getMonth()
	},
	getRegistrationYears: function(noOfItems=13){
		var y = []
		var cy = utils.getCurrentYear(),
			cm = utils.getCurrentMonth()
		if(cm < 9){
			cy--
		}
		var obj = {}
		for(var i = cy; i >= cy-noOfItems;i--){
			obj['key'] = i
			obj['value'] = i
			y.push(JSON.parse(JSON.stringify(obj)))
		}
		return y
	},
	getManufacturingYears: function(){
		var y = []
		var cy = utils.getCurrentYear()

		for(var i = cy; i > cy-2;i--){
			y.push(i)
		}
		return y
	},

}

export = utils