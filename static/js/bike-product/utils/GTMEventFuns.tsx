
import VehicleStore = require('../stores/VehicleStore')
import RegistryStore = require('../stores/RegistryStore')
import PolicyStore = require('../stores/PolicyStore')
import ResultStore = require('../stores/ResultStore')

import GTMUtils = require("./GTMUtils");

class GTMEventFuns {
	// GTMUtils

	getAdditionalParams(){
		var params = [];
			params.push(VehicleStore.getSelectedModelVerbose())	// vehicle type
			params.push('NA')									// Fuel type
			params.push(VehicleStore.getSelectedVariantVerbose())	// Variant
			params.push(RegistryStore.getSelectedRTOVerbose())	// Reg Place
			params.push(RegistryStore.getRegistrationYear())	// Reg Year

		return params
	}

	// HomePage
	successfulFastlaneSubmission(){
		var isExpired = PolicyStore.isExpired()?'Expired Policy':'Not Expired Policy'
		var params = this.getAdditionalParams()

		GTMUtils.gtmEvent('HomepageTopFastlaneBikeEventGetQuote','Homepage Top Fastlane Bike - Get Quote',isExpired,null,params)
	}

	wrongBikeRegSubmission(){
		var isExpired = PolicyStore.isExpired()?'Expired Policy':'Not Expired Policy'

		GTMUtils.gtmEvent('HomepageTopFastlaneBikeEventProductPage','Homepage Top Fastlane Bike - Redirect Product',isExpired,'Fastlane Not Found')
	}

	idkBikeRegNumber(){
		GTMUtils.gtmEvent('HomepageTopFastlaneBikeEventProductPage','Homepage Top Fastlane Bike - Redirect Product','Fastlane','User Number Not Known')
	}

	knowBikeReg(){
		GTMUtils.gtmEvent('HomepageTopFastlaneBikeEventKnowNumber','Homepage Top Fastlane Bike - Know Number','I Know My Number')
	}

	editQuoteFormBikeReg(){
		GTMUtils.gtmEvent('HomepageTopFastlaneBikeEventEditNumber','Homepage Top Fastlane Bike - Edit Number','I Want to Edit My Number')
	}

	continueAfterEditBikeDetails(){
		var params = this.getAdditionalParams()

		GTMUtils.gtmEvent('HomeTopBikeEventMakeModelYear','Homepage Bike - Bike Details','Make Model & Variant',null,params)
	}

	continueAfterRTO(){
		var params = this.getAdditionalParams()

		GTMUtils.gtmEvent('HomeTopBikeEventRTO','Homepage Bike - RTO Details','Make Model Variant & RTO',null,params)
	}

	continueAfterPolicyExpired(expired:boolean){
		var isExpired = (expired===true)?'Expired Policy':'Not Expired Policy'
		var params = this.getAdditionalParams()

		GTMUtils.gtmEvent('HomeTopBikeEventExpiredNotExpired','Homepage Bike - Expiry Details',isExpired,null,params)
	}

	onBuy(){
		var isExpired = PolicyStore.isExpired()?'Expired Policy':'Not Expired Policy'
		var params = this.getAdditionalParams()

		GTMUtils.gtmEvent('HomeTopBikeEventGetQuote','Homepage Bike - Get Quote',isExpired,null,params)
	}

	// ****************** ResultsPage

	notYourBike(){
		GTMUtils.gtmEvent('BikeEventNotYourBike','Bike - Not Your Bike','Clicked')
	}
	
	// E and NE
	resubmitAfterNotYourBike(){

		var isExpired = PolicyStore.isExpired()?'Expired Policy':'Not Expired Policy'
		var params = this.getAdditionalParams()

		GTMUtils.gtmEvent('BikeEventResultsChangeModel','Bike - Bike Details Changed',isExpired,null,params)
	}

	editBikeReg(){
		GTMUtils.gtmEvent('BikeEventRegNumberEdit','Bike - Registration Number Edit','Clicked')
	}

	// E and NE
	zeroDepChanged(yes?:boolean){
		// both on yes and no
		var isExpired = PolicyStore.isExpired()?'Expired Policy':'Not Expired Policy'
		var category = (yes===true)?'Bike - Add Zero Depreciation':'Bike - Remove Zero Depreciation'

		GTMUtils.gtmEvent('BikeEventZeroDepreciation',category,isExpired)
	}

	// E and NE
	changeIDV(value){
		// for best deal, numeric amount, highest across insurers
		var isExpired = PolicyStore.isExpired()?'Expired Policy':'Not Expired Policy'

		var label = undefined
		switch(value){
			case 0:
			label = 'For The Best Deal'
			break;

			case 99999999:
			label = 'Highest Across Insurers'
			break;

			default:
			label = value
			break
		}



		GTMUtils.gtmEvent('BikeEventEditIDV','Bike - Edit IDV',isExpired,label)
	}

	// E and NE
	clickOnInsurer(insurer:any){
		var isExpired = PolicyStore.isExpired()?'Expired Policy':'Not Expired Policy'
		var idv = ResultStore.getSelectedIDV()
		var zeroDepBoolean = ResultStore.isDepreciationWaiver()
		var zeroDep = (zeroDepBoolean===true)?'Zero Depreciation Added':'Zero Depreciation Not Added'

		GTMUtils.gtmEvent('BikeEventBuyPlan','Bike - Buy Plan',isExpired,insurer.title,['',zeroDep,idv,'','',insurer.premium])
	}


	// ****************** Proposal HomePage

	// All - E and NE

	onStageChange(stage,propsalData,policyName){
		var isExpired = PolicyStore.isExpired()?'Expired Policy':'Not Expired Policy'

		switch (stage) {
			case "primary-detail-container":
				this.submitPrimaryDetails(isExpired,propsalData,policyName)
				break;

			case "basic-detail-container":
				this.submitBasicDetails(isExpired,propsalData,policyName)
				break;

			case "address_container":
				this.submitAddressDetails(isExpired,propsalData,policyName)
				break;

			case "vehicle_details_container":
				this.submitVehicleDetails(isExpired,propsalData,policyName)
				break;
		}
	}

	submitPrimaryDetails(isExpired,propsalData,policyName){

		GTMUtils.gtmEvent('BikeEventProposerForm','Bike - Proposer Primary Details Filled',isExpired,policyName)
	}

	submitBasicDetails(isExpired,propsalData,policyName){
		GTMUtils.gtmEvent('BikeEventProposerForm','Bike - Proposer Basic Details Filled',isExpired,policyName)
	}

	submitVehicleDetails(isExpired,propsalData,policyName){

		var extraParam = []
			extraParam.push((propsalData.claimed_last_year=="0")?'No Claim':'Claim')
			extraParam.push(propsalData.previous_ncb)

		GTMUtils.gtmEvent('BikeEventProposerForm','Bike - Proposer Vehicle Details Filled',isExpired,policyName,extraParam)
	}

	submitAddressDetails(isExpired,propsalData,policyName){
		GTMUtils.gtmEvent('BikeEventProposerForm','Bike - Proposer Address Filled',isExpired,policyName)
	}

	bikePayment(policyName){
		var isExpired = PolicyStore.isExpired()?'Expired Policy':'Not Expired Policy'
		GTMUtils.gtmEvent('BikeEventMakePayment','Bike - Make Payment',isExpired,policyName)
	}

	virtualPageView(){
		var obj = { event:'virtual_page_view',virtual_page_path:'/vp/bike/initiate-results/',virtual_page_title:'VP-BIKE-INITIATE-RESULTS' }
		GTMUtils.gtmEve(obj)
	}

	onEditStage(stage,propsalData,policyName){
		var isExpired = PolicyStore.isExpired()?'Expired Policy':'Not Expired Policy'

		switch (stage) {
			case "primary-detail-container":
				this.editPrimaryDetails(isExpired,propsalData,policyName)
				break;

			case "basic-detail-container":
				this.editBasicDetails(isExpired,propsalData,policyName)
				break;

			case "address_container":
				this.editAddressDetails(isExpired,propsalData,policyName)
				break;

			case "vehicle_details_container":
				this.editVehicleDetails(isExpired,propsalData,policyName)
				break;
		}
	}
	
	editPrimaryDetails(isExpired,propsalData,policyName){
		GTMUtils.gtmEvent('BikeEventEditProposerForm','Bike - Proposer Primary Details Edit',isExpired,policyName)
	}

	editBasicDetails(isExpired,propsalData,policyName){
		GTMUtils.gtmEvent('BikeEventEditProposerForm','Bike - Proposer Basic Details Edit',isExpired,policyName)
	}

	editVehicleDetails(isExpired,propsalData,policyName){
		GTMUtils.gtmEvent('BikeEventEditProposerForm','Bike - Proposer Vehicle Details Edit',isExpired,policyName)
	}

	editAddressDetails(isExpired,propsalData,policyName){
		GTMUtils.gtmEvent('BikeEventEditProposerForm','Bike - Proposer Address Details Edit',isExpired,policyName)
	}

}

export = new GTMEventFuns()