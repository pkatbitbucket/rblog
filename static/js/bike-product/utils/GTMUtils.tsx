function ga(data) {
    if (!window['dataLayer'] || typeof data !== 'object')
        return
    window['dataLayer'].push(data);
}

class GTMUtils {
    gtmEvent(event, category, action, label?:any, extraParams?:any) {
        var labelF = (label)?label : ''
        var obj = {
            'event': event,
            'category': category,
            'action': action,
            'label': labelF
        }

        if(extraParams){
             extraParams.forEach(function(elem,index){
                 obj['param'+(index+1)] = elem
             })   
        }

        ga(obj)
    }

    // With different custom attributes
    gtmEve(obj){
        ga(obj)
    }
}

export = new GTMUtils()