// Polyfill for Array.Find
(function(){
    if (!Array.prototype.find) {
      Array.prototype.find = function(predicate) {
        if (this === null) {
          throw new TypeError('Array.prototype.find called on null or undefined');
        }
        if (typeof predicate !== 'function') {
          throw new TypeError('predicate must be a function');
        }
        var list = Object(this);
        var length = list.length >>> 0;
        var thisArg = arguments[1];
        var value;

        for (var i = 0; i < length; i++) {
          value = list[i];
          if (predicate.call(thisArg, value, i, list)) {
            return value;
          }
        }
        return undefined;
      };
    }
})();

(function(){
    /********* private variables *********/
    $.fn.select2.defaults.shouldFocusInput = function(instance){
        return true;
    };

    var car_data = {
        expiry_slot:"",
        expiry_date:"",
        expiry_month:"",
        expiry_year:"",
        vehicle_fuel_type :"",
        vehicle_variant :"",
        vehicle_make_model :"",
        vehicle_rto :"",
        vehicle_reg_year:"",
        cngKitValue:"",
        isCNGFitted:""
    }

    if(typeof SITE_URL === "undefined"){
        SITE_URL = '';
    }

    var VEHICLE_TYPE_CAR = "fourwheeler";
    var oldCarYears = utils.getPastYears();
    var hero_email_month_field=false;
    var model_list = [];
    var rto_list = [];
    var fuel_types =
        [{'code': 'petrol', 'id': 'PETROL','name': 'Petrol'},
         {'code': 'diesel', 'id': 'DIESEL','name': 'Diesel'},
         {'code': 'cng_company', 'id': 'INTERNAL_LPG_CNG','name': 'CNG/LPG Company Fitted'},
         {'code': 'cng_external', 'id': 'PETROL','name': 'CNG/LPG Externally Fitted'}];

    var selected_model_id_car,
        selected_fuel_type_id_car = 'PETROL';

    /********* private functions *********/
    var setUpCarForm = function() {
        var policy_type_value = $('[name=car_policy_type]:checked').val();
        car_data.policy_type = policy_type_value;
        if (policy_type_value == 'new') {
            $('.car-product .new-policy').addClass('hide');
            $('.car-product .renew-policy').removeClass('hide');
            $('.car-product .common-fields').removeClass('hide');
            $('.car-product .expired-policy').addClass('hide');
            $('.car-product .rto-field').addClass('col-md-offset-3');
        } else if (policy_type_value == 'used' || policy_type_value == 'renew' || policy_type_value == 'expired') {
            $('.car-product .new-policy').addClass('hide');
            $('.car-product .renew-policy').removeClass('hide');
            $('.car-product .common-fields').removeClass('hide');
            $('.car-product .expired-policy').addClass('hide');
            $('.car-product .rto-field').removeClass('col-md-offset-3');
        }

        initExpirePolicyForm();
        if(policy_type_value == 'expired'){
            $("#expired_car").removeClass("hide");
        }
        else{
            $("#expired_car").addClass("hide");
        }
    };

    function isWebView(){
        var c = document.cookie.split("webview=")[1];
        var webView = c?c.split(";")[0]:"";
        webView = webView && webView == 'true'? true:false;
        return webView;
    }

    function getVehicles_Car(callback) {
        var url = SITE_URL +'/motor/' + VEHICLE_TYPE_CAR + '/api/vehicles/';
        var data = { 'csrfmiddlewaretoken': CSRF_TOKEN }
        var el_id = '#vehicle_make_model';

        // If webview, show typeahead instead of select2
        $(el_id + (isWebView()?"":"_text")).parent().hide();

        utils.getData(url,
            data,
            el_id + (isWebView()?"_text":""),
            "models",
            function(response){
                model_list = response;
                if(callback) callback();
            },
            car_data.vehicle_make_model,
            isWebView()
        );
    }

    function getCarVariants() {
        var select = $("#vehicle_variant");
        $(select).find('option').remove();

        if (selected_model_id_car && selected_fuel_type_id_car) {
            var url =   SITE_URL +
                        '/motor/' + VEHICLE_TYPE_CAR +
                        '/api/vehicles/' + selected_model_id_car +
                        '/variants/';

            var data = {'csrfmiddlewaretoken': CSRF_TOKEN,'fuel_type': selected_fuel_type_id_car}
            utils.getData(  url,
                            data,
                            "#vehicle_variant",
                            "variants",
                            undefined,
                            car_data.vehicle_variant,
                            false,
                            isWebView()?-1:undefined);
        }
        else{
            utils.createSelect($('#vehicle_variant'),{})
        }
    };

    function getRTOs_Car() {
        var url = SITE_URL + '/motor/' + VEHICLE_TYPE_CAR + '/api/rtos/';
        var data = {'csrfmiddlewaretoken': CSRF_TOKEN}
        var el_id = "#vehicle_rto";
        $(el_id + (isWebView()?"":"_text")).parent().hide();

        utils.getData(url,
            data,
            el_id + (isWebView()?"_text":""),"rtos",
            function(res){
                rto_list = res;
            },
            car_data.vehicle_rto,
            isWebView());
    };

    function setFuelTypes_Car() {
        utils.createSelect( $('#vehicle_fuel_type'),
                            fuel_types,
                            isWebView()?-1:undefined,
                            car_data.vehicle_fuel_type);
    };

    function setRegYears() {
        utils.createSelect_new( $("#vehicle_reg_year"),
                                oldCarYears,
                                isWebView()?-1:undefined,
                                parseInt(car_data.vehicle_reg_year));
    };

    function setCarForm(callback){
        var cngVal, modelVal, fuelVal, variantVal,rtoVal,yearVal, _expirtDate, _expiryMonth, _expiryYear;
        // Fetch data from LocalStorage
        if(localStorage && localStorage.getItem(VEHICLE_TYPE_CAR)){
            var _local = JSON.parse(localStorage.getItem(VEHICLE_TYPE_CAR));

            car_data["expiry_slot"]        = _local.expirySlot
            car_data["expiry_date"]        = _local.expiry_date
            car_data["expiry_month"]       = _local.expiry_month
            car_data["expiry_year"]        = _local.expiry_year
            car_data["vehicle_fuel_type"]  = _local.fuel_type
            car_data["vehicle_variant"]    = _local.vehicleVariant
            car_data["vehicle_make_model"] = _local.vehicle
            car_data["vehicle_rto"]        = _local.rto_info
            car_data["vehicle_reg_year"]   = _local.reg_year
            car_data["cngKitValue"]        = _local.cngKitValue
            car_data["isCNGFitted"]        = _local.isCNGFitted

            if(_local.vehicle)
                selected_model_id_car = _local.vehicle.id;
            if(_local.fuel_type)
                selected_fuel_type_id_car = _local.fuel_type.id;
        }
        // Fetch data from Cookie
        else{
            car_data["expiry_slot"]        = Cookies.get(VEHICLE_TYPE_CAR+'_expiry_slot'),
            car_data["expiry_date"]        = Cookies.get(VEHICLE_TYPE_CAR+'_expiryDate'),
            car_data["expiry_month"]       = Cookies.get(VEHICLE_TYPE_CAR+'_expiryMonth'),
            car_data["expiry_year"]        = Cookies.get(VEHICLE_TYPE_CAR+'_expiryYear'),
            car_data["vehicle_fuel_type"]  = utils.validateJSON(Cookies.get(VEHICLE_TYPE_CAR + '_fuel_type')),
            car_data["vehicle_variant"]    = utils.validateJSON(Cookies.get(VEHICLE_TYPE_CAR+ '_vehicleVariant')),
            car_data["vehicle_make_model"] = utils.validateJSON(Cookies.get(VEHICLE_TYPE_CAR+'_vehicle')),
            car_data["vehicle_rto"]        = Cookies.get(VEHICLE_TYPE_CAR+'_rto_info'),
            car_data["vehicle_reg_year"]   = Cookies.get(VEHICLE_TYPE_CAR+ '_reg_year'),
            car_data["cngKitValue"]        = Cookies.get(VEHICLE_TYPE_CAR + '_cngKitValue'),
            car_data["isCNGFitted"]        = Cookies.get(VEHICLE_TYPE_CAR + '_isCNGFitted')
        }

        if(car_data.vehicle_make_model){
            selected_model_id_car = car_data.vehicle_make_model.id;
            getCarVariants();
        }

        if(car_data.isCNG){
            $("#divCNGKit").fadeIn();
        }

        if(!car_data.cngKitValue){
            $("#txtCNGKit").removeClass('selected');
        }

        if(!(car_data.vehicle_make_model && JSON.stringify(car_data.vehicle_make_model).indexOf('{')>=0)){
            $("#vehicle_make_model").parent().removeClass('selected');
        }

        if(!(car_data.vehicle_fuel_type && JSON.stringify(car_data.vehicle_fuel_type).indexOf('{')>=0)){
            $("#vehicle_fuel_type").parent().removeClass('selected');
        }

        if(!(car_data.vehicle_variant && JSON.stringify(car_data.vehicle_variant).indexOf('{')>=0)){
            $("#vehicle_variant").parent().removeClass('selected');
        }

        if(!(car_data.vehicle_rto && JSON.stringify(car_data.vehicle_rto).indexOf('{')>=0)){
            $("#vehicle_rto").parent().removeClass('selected');
        }

        if(!car_data.vehicle_reg_year){
            $("#vehicle_reg_year").parent().removeClass('selected');
        }

        if(!car_data.expiry_year){
            $("#car_expiry_year").parent().removeClass('selected');
        }

        if(!car_data.expiry_month){
            $("#car_expiry_month").parent().removeClass('selected');
        }

        if(!car_data.expiry_date){
            $("#car_expiry_date").parent().removeClass('selected');
        }

        if(callback) callback();
    }

    function initExpirePolicyForm(){
        var years = [];
        var months = utils.getMonthList();
        var dates = [];
        var showSearchBox = isWebView()?-1:undefined;
        var today = new Date();

        years.push(today.getFullYear());
        years.push(today.getFullYear()-1);

        for(var i=1;i<=31;i++) dates.push(i);
        utils.createSelect_new($("#car_expiry_date"),dates, showSearchBox , car_data.expiry_date);
        utils.createSelect_new($("#car_expiry_month"),months, showSearchBox, car_data.expiry_month);
        utils.createSelect_new($("#car_expiry_year"),years, showSearchBox, car_data.expiry_year);
    }

    var submitCarForm = function(otp, verified) {
        if(typeof tparty == 'undefined'){
            tparty = "";
        }
        var vehicle = car_data,
            redirect_url = SITE_URL+'/motor/car-insurance/'+tparty+'#';
            redirect_url += (isWebView()?'/':'') + 'results';
        var data = {
            'isNewVehicle': (vehicle.policy_type == "new") ? '1' : '0',
            'vehicle': JSON.stringify(vehicle.vehicle_make_model),
            'vehicleVariant': JSON.stringify(vehicle.vehicle_variant),
            'fuel_type': JSON.stringify(vehicle.vehicle_fuel_type),
            'rto_info': JSON.stringify(vehicle.vehicle_rto),
            'cngKitValue': vehicle.cngKitValue,
            'isCNGFitted': vehicle.isCNGFitted,
            'reg_year': vehicle.vehicle_reg_year,
        };

        if (vehicle.policy_type == "used" ) {
            var t_100 = new Date();
            t_100.setDate(t_100.getDate() - 100);
            data['isUsedVehicle'] = 1;
            data['expiry_date'] = t_100.getDate();
            data["expiry_month"] = utils.getMonthNumberToName(t_100.getMonth());
            data["expiry_year"] = t_100.getFullYear();
        }

        if (vehicle.policy_type == "expired") {
            //redirect_url = '/lp/car-insurance/renew-expired-policy/';
            data['expiry_date'] = car_data.expiry_date;
            data["expiry_month"] = car_data.expiry_month;
            data["expiry_year"] = car_data.expiry_year;

            Cookies.set("lms_campaign","motor-offlinecases");
            Cookies.set("mark_product_category","commonAncestorContainer");
        }

        var redirect_flag = true;
        if(otp){
            data['otp_status'] = verified? 'verified': 'not verified';
            redirect_flag = verified;
        }

        var gtm_data = {};
        $.post(SITE_URL+"/motor/submit-landing-page/", data, function(response) {
            response = JSON.parse(response);
            if (response.success) {
                if (localStorage) {
                    if(vehicle.policy_type == "expired"){
                        response.data["policyExpired"] =true;
                    }
                    localStorage.setItem(VEHICLE_TYPE_CAR, JSON.stringify(response.data));
                } else {
                    for (var item in response.data) {
                        var value = response.data[item];
                        if (value != null && typeof(value) == "object") {
                            value = JSON.stringify(value);
                        }
                        Cookies.set(VEHICLE_TYPE_CAR + item, value);
                    }
                }

                Cookies.set('mobileNoOn', 'LP - View Quotes with number');
                push_gtm(response, "LP-BUY-Onile", false);
            }
        });
        function push_gtm(response, title, flag){
            // Send data to Google Analytics
           // if(vehicle.policy_type != "expired")
                gtm_data = {
                    param1: car_data.vehicle_make_model.name,
                    param2: car_data.vehicle_fuel_type.name,
                    param3: car_data.vehicle_variant.name,
                    param4: car_data.vehicle_rto.name
                }

            if (vehicle.policy_type != "new")
                gtm_data.param5= data.reg_year;
            if(data.isCNGFitted)
                gtm_data.param6 = data.cngKitValue;
            if(flag){
                    gtm_data.param7 = "Email Address Given";
                    gtm_data.param8 = month;
                }
            utils.gtm_push("CarEventGetQuote",
                title+" Car - Get Quote",
                vehicle.policy_type + " Car",
                " ", gtm_data,
                function(){
                    window.location.href = redirect_url;
                });
        }
    };

    function validateIsEmpty(value){
        // to handle cases where value is 0
        return value === undefined || value === null || value.toString().trim().length === 0
    }

    var validateCarExpiredDateValue = function(){
        var exDate = car_data.expiry_date;
        var exMonth = utils.getMonthNameToNumber(car_data.expiry_month);
        var exYear = car_data.expiry_year;

        if(exDate && !validateIsEmpty(exMonth) && exYear){
            var _date = new Date(exYear, exMonth,exDate);
            var today = new Date();

            _date.setHours(0,0,0,0);
            today.setHours(0,0,0,0);

            if(_date.getDate() != exDate){
                $("#car_expiry_error").text("Please select a valid date.")
                $("#car_expiry_error").fadeIn();
                return false;
            }
            else if(+_date >= +today){
                $("#car_expiry_error").text("Policy expiry date should be past.")
                $("#car_expiry_error").fadeIn();
                return false;
            }
            else{
                $("#car_expiry_error").text("")
                $("#car_expiry_error").fadeOut();
                return true
            }
        }
        else {
            $("#car_expiry_error").text("Please select a valid date.")
            $("#car_expiry_error").fadeIn();
            return false;
        }
    }

    var validateCarRTO = function(){
        var valid = true;
        if (!car_data.vehicle_rto || (
            car_data.vehicle_rto && car_data.vehicle_rto.toString().length == 0)) {
            valid = false;
            $('#rto_error').fadeIn();
        }
        return valid;
    }

    var validateCngKitValue = function(){
        var cngreg = /^[0-9]\d*$/;
        var valid = true;

        if(cngreg.test(car_data.cngKitValue) && parseInt(car_data.cngKitValue) <= 40000 ){
            $('#cng_error')
                .fadeOut();
        }
        else if(car_data.cngKitValue == 0){
            valid = false;
            $('#cng_error').fadeIn();
        }
        else{
            valid = false;
            $('#cng_error')
                .text("Please enter a valid amount under 40000.")
                .fadeIn();
        }

        return valid;
    }

    var validateCarForm = function() {
        var valid = true;
        $('.error-text').hide();

        if (!car_data.vehicle_make_model || !car_data.vehicle_fuel_type || !car_data.vehicle_variant) {
            valid = false;
            $('#vehicle_detail_error').fadeIn();
        }

        if (!car_data.vehicle_reg_year) {
            valid = false;
            $('#reg_date_error').fadeIn();
        }

        if( car_data.vehicle_fuel_type &&
            car_data.vehicle_fuel_type.name == "CNG/LPG Externally Fitted"){

            valid = validateCngKitValue() && valid;
        }

        valid = valid && validateCarRTO();

        if(car_data.policy_type === "expired")
            valid = validateCarExpiredDateValue() && valid;

        return valid;
    };

    var getDataFromURL = function(){
        if(window.location.search){
            var urlParams = window.location.search.substring(1).split("&");
            var params = {}
            urlParams.forEach(function(item){
                var _temp = item.split("=");
                params[_temp[0]] = _temp[1];
            });
            return populateFromURL(params);
        }
    }

    var populateFromURL = function(data){
        if(data.modelId){
            var value = searchInVehicleList(data.modelId);
        }
        return value;
    }

    var searchInVehicleList = function(sId){
        var list = $("#vehicle_make_model").find("option");
        var retVal = null;

        model_list.forEach(function(obj){
            if(obj.id == sId){
                retVal = JSON.stringify(obj);
                return;
            }
        })
        return retVal;
    }

    /********* Event functions *********/
    function registerEvents(){
        $('input[name="car_policy_type"]').change(function() {
            setUpCarForm();
        });

        $("#vehicle_make_model").on("change",function() {
            selected_model_id_car = $(this).children(":selected").attr("id");
            car_data.vehicle_make_model = JSON.parse($(this).children(":selected").val());
            car_data.vehicle_variant = undefined;
            getCarVariants();
        });

        $("#vehicle_fuel_type").on("change",function() {
            selected_fuel_type_id_car = $(this).children(":selected").attr("id");
            car_data.vehicle_fuel_type = JSON.parse($(this).children(":selected").val());
            car_data.vehicle_variant = undefined;
            getCarVariants();
            var selected_fuel =  $(this).children(":selected").text().trim();
            if(selected_fuel=="CNG/LPG Externally Fitted"){
                $("#divCNGKit").removeClass("hide").fadeIn();
                if($('#txtCNGKit').val().length == 0){
                    ///$('#txtCNGKit').val(0);
                    $('#txtCNGKit').parent().addClass("selected").removeClass("hide")
                }
            }
            else{
                $('#divCNGKit').fadeOut();
                $('#txtCNGKit').val('');
            }
        });

        $("#vehicle_variant").on("change",function(){
            if(  $("option:selected", this).attr("id") &&
                ($("option:selected", this).attr("id") >0 || $("option:selected", this).attr("id").length>0)){
                car_data.vehicle_variant = JSON.parse($("option:selected", this).val());
                $('#vehicle_detail_error').fadeOut();
            }
        });

        $("#vehicle_rto").on("change", function(){
            if($("option:selected", this).attr("id") &&
                ($("option:selected", this).attr("id") >0 || $("option:selected", this).attr("id").length>0)){
                car_data.vehicle_rto = JSON.parse($("option:selected", this).val());
                $('#rto_error').fadeOut();
            }
        });

        $("#vehicle_reg_year").on("change",function () {
            if($("option:selected", this).attr("id") >0 || $("option:selected", this).attr("id").length>0){
                car_data.vehicle_reg_year = JSON.parse($("option:selected", this).val());
                $('#reg_date_error').fadeOut();
            }
        })
        var cnum;
        var cmail;
        var month;
        $(".btn").off();
        $('#submit_car_form').off().on('click', function(e) {
            if (validateCarForm()){
                submitCarForm(false, false)
            }
        });

        $("#txtCNGKit").on("focus", function(){
            if($(this).val()<=0){
                $(this).val('');
            }
        });
        $("#txtCNGKit").on("blur",function(){
            if($(this).val()<=0){
                $(this).val('');
            }
            else{
                car_data.cngKitValue = $(this).val();
            }
        });

        $("#car_expiry_date, #car_expiry_month, #car_expiry_year").on("change", function(){
            var key = $(this).attr("id").replace("car_","");
            car_data[key] = $("option:selected", this).val();
        });

        $("[id$='_text']").on("autocompletechange change", function(e,ui){
            if(ui && ui.item){
                var k = $(this).attr("id").replace("_text","");
                car_data[k] = JSON.parse(ui.item["data-value"]?ui.item["data-value"]:ui.item.value);

                if(k === "vehicle_make_model"){
                    selected_model_id_car = typeof(car_data[k])=== "string"?JSON.parse(car_data[k]).id:car_data[k].id;
                    car_data.vehicle_variant = undefined;
                    getCarVariants();
                }
            }
        });

        $("[id$='_text']").on("keyup", function(){
            utils.showLabelForTextField("#" + $(this).attr("id"));
        })

        // This is for auto complete.
        // If user enters incorrect value or enters complete name, values should be detched accordingly.
        $("#vehicle_make_model_text").on("blur", function(){
            var model = $(this).val();
            var match = model_list.find(function(m){
                return m.name.toLowerCase().trim() === model.toLowerCase().trim();
            });

            if(!match){
                car_data.vehicle_variant = undefined;
                car_data.vehicle_make_model = undefined;
                $("#vehicle_variant").val(undefined).trigger("change");
            }
            else{
                car_data.vehicle_make_model = match;
            }

            selected_model_id_car = match?match.id:undefined;
            getCarVariants();

        });
        $("#vehicle_rto_text").on("blur", function(){
            var model = $(this).val();
            var match = rto_list.find(function(m){
                return m.name.toLowerCase().trim() === model.toLowerCase().trim();
            });

            if(!match){
                car_data.vehicle_rto = undefined;
            }
            else{
                car_data.vehicle_rto = match;
            }
        });
    }

    /********* Init functions *********/
    (function initCarForm(){
        // Fetch data and assign to controls
        setUpCarForm();
        setCarForm(function(){
            getRTOs_Car();
            getVehicles_Car();
            setFuelTypes_Car();
            setRegYears();
            getCarVariants();
            setTimeout(function(){
                $("[id$='_text']").trigger("keyup");
            },500)
        });
        registerEvents();
        utils.createMobileField("#contact-me-num");
    })();
})()