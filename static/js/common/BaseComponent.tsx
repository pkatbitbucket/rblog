/// <reference path="../typings/tsd.d.ts" />
/*
  Since you're here again, you might as well consider
  implementing generics. It's for your own good!

  BaseProps:
  	stores: array of stores to listen to
  	getStateFromStores: method to get updated states on every listen
  	path: URL pathname for current component
  	queryParams: components states to be used as url search parameters. Ordered.
  	urlParams: components states to be used as url pathname. Ordered.
	loadFromURLAction: method to call on load of this component to pass all the values of urlParams and queryParams passed in the URL.
		Method arguments ordered as urlParams first and then queryParams.
  
  Global:
  	preventInternalRouting: as the name suggests

  Class methods:
  	show: to render a react component such that it gets all the url params of parent component
  	_updateDataFromURL: to read all urlParams and queryParams from current URL and call the loadFromURLAction method
  	_updateURL: to update current URL with changed states values. By default called as a callback of setState method, when a components listens to a store
  	_getPathname: called by _updateURL
  	_getNextSearchQuery: called by _updateURL
  	_onChange: fires when listening to a store. Gets and sets latest states from getStateFromStore method 
  	_preventRouting: controls routing on the basis of it's return value
*/
import React = require('react')
import assign = require('object-assign')
import BaseStore = require('./BaseStore')
import TransitionUtils = require('./utils/TransitionUtils')

window['preventInternalRouting'] = false

interface BaseProps{
	stores: BaseStore[];
	getStateFromStores: Function;
	path?: string;
	queryParams?: string[];
	urlParams?: string[];
	loadFromURLAction?: Function;
}

class BaseComponent extends React.Component<any, any> {
	constructor(props, protected bProps:BaseProps) {
		super(props)
		this._onChange = this._onChange.bind(this)
		this.state = bProps.getStateFromStores() || {}

		if(bProps.stores && bProps.stores.length>0){
			//binding store listners
			let subComponentDidMount = this.componentDidMount.bind(this)
			this.componentDidMount = () => {
				for (let index = 0; index < bProps.stores.length; index++){
					bProps.stores[index].addChangeListener(this._onChange)
				}
				if(bProps.loadFromURLAction){
					this._updateDataFromURL()
				}else{
					this._updateURL()
				}
				subComponentDidMount()
			};

			//removing store listners
			let subComponentWillUnmount = this.componentWillUnmount.bind(this)
			this.componentWillUnmount = () => {
				for (let index = 0; index < bProps.stores.length; index++) {
					bProps.stores[index].removeChangeListener(this._onChange)
				}
				this._dropQueryParams()
				subComponentWillUnmount()
			};
		}
	}

	protected componentDidMount() { }
	protected componentWillUnmount() { }

	protected show(component: any, ...cProps: any[]) {
		let props = { params: this.props.params }
		if(cProps){
			for (let index = 0; index < cProps.length; index++){
				assign(props, cProps[index])
			}
		}
		return React.createElement(component, props)
	}

	private _updateDataFromURL() {
		if (this._preventRouting())
			return
		
		let urlData = TransitionUtils.getQueryValuesFromURL()
		if (this.bProps.urlParams) {
			for (let count = 0; count < this.bProps.urlParams.length; count++) {
				let tState = this.bProps.urlParams[count]
				if (this.props.params && this.props.params[tState])
					urlData[tState] = this.props.params[tState]
			}
		}

		let params = []
		let l1 = this.bProps.urlParams ? this.bProps.urlParams.length : 0
		let l2 = this.bProps.queryParams ? this.bProps.queryParams.length : 0
		let index = 0
		for (index; index < l1; index++)
			params[index] = urlData[this.bProps.urlParams[index]]
		for (index; index < l1+l2; index++)
			params[index] = urlData[this.bProps.queryParams[index-l1]]

		setTimeout(() => { this.bProps.loadFromURLAction.apply(null, params) }, 0)
	}

	private _updateURL() {
		if (this._preventRouting())
			return

		let query = this._getNextSearchQuery()
		let pathnameCurrent = window.location.pathname
		let pathnameNext = this._getPathname(this.state)
		if (pathnameCurrent !== pathnameNext || query && window.location.search !== query) {
			TransitionUtils.redirectTo(pathnameNext + query)
		}
	}

	private _getPathname(obj):string{
		let url = this.bProps.path || ''
		let params = this.bProps.urlParams || []

		if (url.charAt(url.length - 1) != '/')
			url += '/';
		for (let index = 0; index < params.length; index++) {
			if (!!obj[params[index]]) {
				url += obj[params[index]] + '/'
			}
		}
		return url.substr(0, url.length - 1)
	}

	private _getNextSearchQuery():string{
		let query = ''
		let params = this.bProps.queryParams || []
		let currentQueryParams = TransitionUtils.getQueryValuesFromURL()
		for (let index = 0; index < params.length; index++) {
			let key = params[index]
			if (this.state[key] != undefined) {
				currentQueryParams[key] = this.state[key]
			}else{
				delete currentQueryParams[key]
			}
		}
		return TransitionUtils.objectToQueryString(currentQueryParams)
	}

	private _dropQueryParams(){
		if (this._preventRouting() || !this.bProps.queryParams)
			return

		let params = this.bProps.queryParams || []
		let queryParams = TransitionUtils.getQueryValuesFromURL()
		for (var key in queryParams) {
			if(params.indexOf(key)>=0)
				delete queryParams[key]
		}
		let query = TransitionUtils.objectToQueryString(queryParams)

		if (query && window.location.search != query) {
			TransitionUtils.redirectTo(window.location.pathname + query)
		}
	}

	private _onChange() {
		this.setState(this.bProps.getStateFromStores(), this._updateURL.bind(this))
	}

	private _preventRouting(){
		return window['preventInternalRouting'] || !(this.bProps.queryParams && this.bProps.queryParams.length)
			|| (this.bProps.urlParams && this.bProps.urlParams.length && !this.bProps.path)
	}
}

export = BaseComponent