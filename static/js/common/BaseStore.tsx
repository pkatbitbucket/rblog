/// <reference path="../typings/tsd.d.ts" />
import EventEmitter = require('events')
import assign = require('object-assign')
import Dispatcher = require('./Dispatcher')
import StorageUtils = require('./utils/StorageUtils')
import CommonUtils = require('./utils/CommonUtils')

var EE:any = EventEmitter

/*
_namespace is for creating a event identifier for 'this' store.

_localstorage is fallback option for browser's LocalStorage.
	It also speeds up the emit event process.

_dispatcherIndex is 'this' store's dispatcher identifier.
	It is used in waiting for other stores actions.

_CHANGE_EVENT is the change event identifier of 'this' store.

If this was clear enough, the names of the methods of this class are
	quiet self-explanatory. Okbye. :P 
*/
class BaseStore{
	private static _localstorage: any = {}

	private _CHANGE_EVENT: string;
	private _dispatcherIndex: any;
	constructor(protected _namespace){
		//code to handle Safari browser Private browsing mode
		if (typeof localStorage === 'object') {
		    try {
		        localStorage.setItem('localStorage', '1');
		        localStorage.removeItem('localStorage');
		    } catch (e) {
		    	//overridding and setting setItem as blank function so that it doesnt break 
		        Storage.prototype.setItem = function() {};
		    }
		}
		//
		BaseStore.setContext(this, _namespace)
	}
	
	static setContext(context:BaseStore, _namespace:string){
		context._CHANGE_EVENT = "ch_eve_st_" + _namespace
		if(!BaseStore._localstorage[_namespace]){
				BaseStore._localstorage[_namespace] = StorageUtils.getValueFromLocalStorage(_namespace) || {}
		}
	}

	static blockDispatcher(context:BaseStore){
		context.unregister()
	}

	static registerDispatcher(context:any){
		context.registerDispatcherHandler(context._actionHandler)
	}

	public addChangeListener(callback:Function) {
		EE.prototype.on(this._CHANGE_EVENT, callback)
	}

	public removeChangeListener(callback:Function) {
		EE.prototype.removeListener(this._CHANGE_EVENT, callback)
 	}

 	protected emitChange(){
		EE.prototype.emit(this._CHANGE_EVENT)
	}

	protected registerDispatcherHandler(callback){
		this._dispatcherIndex = Dispatcher.register(callback)
	}

	protected waitFor(...stores:BaseStore[]){
		let array = []
		for (let index = 0; index < stores.length; index++)
			array.push(stores[index]._dispatcherIndex)
		Dispatcher.waitFor(array)
	}

	protected setSelected(obj: any, preventEmit?: boolean): void{
		assign(BaseStore._localstorage[this._namespace], CommonUtils.simpleClone(obj))
		if (!preventEmit){
			this.emitChange()
		}
		this._updateLocalStorage()
	}

	protected setSelectedValue(key:string, value:any, preventEmit?:boolean):void{
		let obj = {}
		obj[key] = value
		this.setSelected(obj, preventEmit)
	}

	protected getSelected(field?: string): any{
		let data = BaseStore._localstorage[this._namespace]
		if(field)
			data = data[field]
		return CommonUtils.simpleClone(data)
	}

	protected clearSelected(field?: string, preventEmit?: boolean): void{
		if(field){
			delete BaseStore._localstorage[this._namespace][field]
		}else{
			BaseStore._localstorage[this._namespace] = {}
		}
		if(!preventEmit){
			this.emitChange()
		}
		this._updateLocalStorage()
	}

	protected replaceSelected(obj: any, preventEmit?: boolean){
		for(let field in obj){
			if (obj.hasOwnProperty(field)){
				BaseStore._localstorage[this._namespace][field] = CommonUtils.simpleClone(obj[field])
			}
	    }
		if (!preventEmit){
			this.emitChange()
		}
		this._updateLocalStorage()
	}

	private unregister(): void{
		Dispatcher.unregister(this._dispatcherIndex)
	}

	private _updateLocalStorage(){
		StorageUtils.setValuesInLocalStorage(this._namespace, BaseStore._localstorage[this._namespace])
	}
}

export = BaseStore