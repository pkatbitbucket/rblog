/// <reference path="../typings/tsd.d.ts" />
import createBrowserHistory = require('history/lib/createBrowserHistory')
window['_history'] = createBrowserHistory()
export = window['_history']