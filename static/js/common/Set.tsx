class Set{
	private _data: any[]
	constructor(entry?:string[] | number[] | string | number){
		this._data = []
		if(entry)
			this.add(entry)
	}

	public has(value: string | number):boolean{
		return this._data.indexOf(value)>-1
	}

	public indexOf(value: string | number):number{
		return this._data.indexOf(value)
	}

	public get(index: number):string | number{
		return this._data[index]
	}

	public length():number{
		return this._data.length
	}

	public add(entry:string[] | number[] | string | number):number{
		let count = 0
		if(typeof entry === 'string' || typeof entry === 'number'){
			if(!this.has(entry)){
				this._data.push(entry)
				count = 1
			}
		}else if(Object.prototype.toString.call(entry) === '[object Array]'){
			for(let index=0; entry.length>index; index++){
				if(!this.has(entry[index])){
					this._data.push(entry[index])
					count++
				}
			}
		}
		return count
	}

	public remove(entry:string[] | number[] | string | number):number{
		let count = 0
		if(typeof entry === 'string' || typeof entry === 'number'){
			let position = this.indexOf(entry)
			if(position>-1){
				this._data.splice(position, 1)
				count = 1
			}
		}else if(Object.prototype.toString.call(entry) === '[object Array]'){
			for(let index=0; entry.length>index; index++){
				let position = this.indexOf(entry[index])
				if(position>-1){
					this._data.splice(position, 1)
					count++
				}
			}
		}
		return count
	}
}

export = Set