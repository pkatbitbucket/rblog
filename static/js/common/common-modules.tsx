/// <reference path="../typings/tsd.d.ts" />
import React = require('react')
import ReactDOM = require('react-dom')
import ReactRouter = require('react-router')
import ReactTG = require('react-addons-css-transition-group')
import Cookie = require('react-cookie')
import assign = require('object-assign')
import EE = require('events')
import moment = require('moment')
import superagent = require('superagent')
import qs = require('qs')
import CommonUtils = require('./utils/CommonUtils')

CommonUtils.loadCustomUtils()

var req: any = require
var requireContext = req.context('../common', true, /\.tsx?$/)
requireContext.keys().map(requireContext)

const WIDGET_MAP = {
	'car-new': require('bundle?lazy&name=car!../motor-product/landing-car'),
	'car-old': require('bundle?lazy&name=car!../motor-product/car'),
	'bike-qf': require('bundle?lazy&name=bike!../bike-product/bike'),
	'bike-fb': require('bundle?lazy&name=bike!../bike-product/components/BikeFallBackQuoteForm'),
	'bike-rp': require('bundle?lazy&name=bike!../bike-product/components/BikeResults'),
	'bike-pf': require('bundle?lazy&name=bike!../bike-product/components/BikeProposal'),
	'health-im': require('bundle?lazy&name=health!../new-health-product/health-indemnity'),
	'health-st': require('bundle?lazy&name=health!../new-health-product/health-supertopup'),
	'travel-st': require('bundle?lazy&name=travel!../travel-product/travel-singletrip'),
	'travel-mt': require('bundle?lazy&name=travel!../travel-product/travel-multitrip'),
	'life-qf': require('bundle?lazy&name=life!../life-product/term-life')
}

window['cfLoadWidget'] = function(widgetName: string, containerId: string, props: any){
	let func = WIDGET_MAP[widgetName]
	let node = document.getElementById(containerId)
	if(func && node){
		let cProps = typeof props === 'object' ? props : {}
		func(function(Component){
			ReactDOM.render(<Component {...cProps}/>, node)
		})
	}else{
		console.warn("Either widget-name or container-id is not valid.")
	}
}

export = {
	React: React,
	ReactDOM: ReactDOM,
	ReactRouter: ReactRouter,
	ReactTG: ReactTG,
	Cookie: Cookie,
	assign: assign,
	EE: EE,
	moment: moment,
	superagent: superagent,
	qs: qs
}