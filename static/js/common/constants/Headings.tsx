import React = require('react')
export = {
	CAR: <span>Insuring your car couldn't be simpler. Your policy could be in your inbox in 7 minutes.</span>,
	BIKE : <span>80% of our customers insure their bike online in less than 4 minutes. It's <i>that</i> simple.</span> ,
	HEALTH : <span>Tailor-made plans, filtered down just for your requirements.</span> ,
	HEALTH_SUPER_TOPUP: <span>Upgrade your exisiting policy cover by Rs. 10 Lacs for premium as less as Rs. 878.</span>,
	TRAVEL : <span>Travel plans that are so cheap, you'd not think twice before making a trip.</span> ,
	TRAVEL_MULTITRIP : <span>One policy, multiple trips. Globetrotting made easy.</span> ,
	TERM_LIFE : <span>Get covered at unbelievably low premiums for high sums. It can't get <i>any</i> better.</span>
}