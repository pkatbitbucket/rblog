export = {
    1001:{
        error_code: 1001,
        message: 'Transaction is already done',
        description: 'Hey {CN}, the transaction you are trying to make payment for, is already completed.<br/>If you have not received your policy document yet, please email our Customer Service Unit at <a href="mailto:support@coverfox.com">support@coverfox.com</a> or call us on the Service Toll Free <strong>18002099970</strong>.'
    },
    1002:{
        error_code: 1002,
        message: '{IN} services are down at the moment',
        description: 'Hey {CN}, there seems to be some problem connecting to the {IN} systems for generating your policy. We apologize for this inconvenience.<br/><br/>You could also proceed by selecting another insurer. All the information you have entered will be saved.<br/><br/>Our Sales Team has been notified about the issue and someone will call you shortly to assist you with the payment.<br/><br/>If you wish to contact us, please call us on our Toll Free Number <strong>18002099940</strong>.'
    },
    1003:{
        error_code: 1003,
        message: 'Payment Failed',
        description: 'Hey {CN}, there was a problem in processing the payment.<br/><br/>You can proceed to pay again and complete the transaction. It is possible that your account was debited with Rs. {PA}, in which case, please do not make the payment again.<br/><br/>Our Sales Team has been notified about the issue and someone will call you shortly to assist you with the payment.<br/><br/>If you wish to contact us, please call us on our Toll Free Number <strong>18002099940</strong>.'
    },
    1004:{
        error_code: 1004,
        message: '{IN} services are down at the moment',
        description: 'Hey {CN}, it appears that the insurer you selected is currently unavailable. This could be temporary and you can click on the RETRY button to try again.<br/><br/>You could also proceed by selecting another insurer. All the information you have entered will be saved.<br/><br/>Our Sales Team has been notified about the issue and someone will call you shortly to assist you with the purchase.<br/><br/>If you wish to contact us, please call us on our Toll Free Number <strong>18002099940</strong>.'
    },
    1005:{
        error_code: 1005,
        message: 'Renewal with same insurer not allowed',
        description: 'Hey {CN}, we are sorry. You have an existing policy from <Insurer Name> and currently they do not allow to renew in such cases on Coverfox.<br/><br/>You could proceed by selecting another insurer. All the information you have entered will be saved.<br/><br/>Our Sales Team has been notified about the issue and someone will call you shortly to assist you with the purchase.<br/><br/>If you wish to contact us, please call us on our Toll Free Number <strong>18002099940</strong>.'
    } 
}