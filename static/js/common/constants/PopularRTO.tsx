export = {
    "New Delhi": [
        {
            "id": "DL-3",
            "name": "South Delhi"
        },
        {
            "id": "DL-2",
            "name": "New Delhi"
        },
        {
            "id": "DL-9",
            "name": "South West Delhi"
        },
        {
            "id": "DL-1",
            "name": "Delhi North Hill Road"
        },
        {
            "id": "DL-4",
            "name": "Delhi West Raja Garden"
        },
        {
            "id": "DL-10",
            "name": "West Delhi"
        },
        {
            "id": "DL-8",
            "name": "North West Delhi"
        },
        {
            "id": "DL-12",
            "name": "South West Delhi Vasant Vihar"
        },
        {
            "id": "DL-7",
            "name": "Delhi East"
        },
        {
            "id": "DL-11",
            "name": "North West Delhi"
        },
        {
            "id": "DL-13",
            "name": "East Delhi Shahdara"
        },
        {
            "id": "DL-6",
            "name": "Delhi Central Sarai Kale Khan"
        },
        {
            "id": "DL-5",
            "name": "Delhi North Loni Road"
        }
    ],
    "Mumbai": [
        {
            "id": "MH-01",
            "name": "Mumbai Tardeo"
        },
        {
            "id": "MH-02",
            "name": "Mumbai Andheri"
        },
        {
            "id": "MH-04",
            "name": "Thane Mumbai"
        },
        {
            "id": "MH-03",
            "name": "Mumbai Worli"
        },
        {
            "id": "MH-43",
            "name": "Navi Mumbai"
        },
        {
            "id": "MH-05",
            "name": "Kalyan Mumbai"
        },
        {
            "id": "MH-48",
            "name": "Virar Vasai Mumbai"
        },
        {
            "id": "MH-47",
            "name": "Borivali Mumbai"
        }
    ],
    "Bangalore": [
        {
            "id": "KA-03",
            "name": "Bangalore Indiranagar"
        },
        {
            "id": "KA-04",
            "name": "Bangalore Yashwanthpur"
        },
        {
            "id": "KA-01",
            "name": "Bangalore Kormangala"
        },
        {
            "id": "KA-05",
            "name": "Bangalore Jayanagar"
        },
        {
            "id": "KA-51",
            "name": "Bangalore Electronic City"
        },
        {
            "id": "KA-53",
            "name": "Bangalore Krishnarajpuram"
        },
        {
            "id": "KA-02",
            "name": "Bangalore Rajajinagar"
        },
        {
            "id": "KA-41",
            "name": "Bangalore Kengeri"
        },
        {
            "id": "KA-50",
            "name": "Bangalore Yalahanka"
        },
        {
            "id": "KA-60",
            "name": "RT Nagar Bangalore"
        },
        {
            "id": "KA-61",
            "name": "Marathahalli Bangalore"
        },
        {
            "id": "KA-10",
            "name": "Bangalore Chamrajpeth"
        },
        {
            "id": "KA-52",
            "name": "Nelamangala Bangalore"
        },
        {
            "id": "KA-43",
            "name": "Devanahalli Bangalore"
        }
    ],
    "Pune": [
        {
            "id": "MH-12",
            "name": "Pune"
        },
        {
            "id": "MH-14",
            "name": "Pimpri-Chinchwad"
        }
    ],
    "Gurgaon": [
        {
            "id": "HR-26",
            "name": "Gurgaon"
        },
        {
            "id": "DL-15",
            "name": "Gurgaon"
        },
        {
            "id": "HR-55",
            "name": "Gurgaon"
        },
        {
            "id": "HR-72",
            "name": "Gurgaon"
        }
    ],
    "Chennai": [
        {
            "id": "TN-09",
            "name": "Chennai KK Nagar"
        },
        {
            "id": "TN-01",
            "name": "Chennai Ayanavaram"
        },
        {
            "id": "TN-10",
            "name": "Chennai Virugambakkam"
        },
        {
            "id": "TN-22",
            "name": "Meenambakkam Chennai"
        },
        {
            "id": "TN-07",
            "name": "Chennai Thiruvanmiyur"
        },
        {
            "id": "TN-02",
            "name": "Chennai Annanagar"
        },
        {
            "id": "TN-11",
            "name": "Chennai Tambaram"
        },
        {
            "id": "TN-18",
            "name": "Chennai Red Hills"
        },
        {
            "id": "TN-04",
            "name": "Chennai Kolathur"
        },
        {
            "id": "TN-06",
            "name": "Chennai Mandaveli"
        },
        {
            "id": "TN-13",
            "name": "Chennai Ambattur"
        },
        {
            "id": "TN-12",
            "name": "Chennai Poonamallee"
        },
        {
            "id": "TN-85",
            "name": "Chennai Southwest"
        },
        {
            "id": "TN-05",
            "name": "Chennai NO"
        },
        {
            "id": "TN-03",
            "name": "Chennai Tondiarpet"
        },
        {
            "id": "TN-14",
            "name": "Chennai Sholinganallur"
        },
        {
            "id": "TN-01",
            "name": "Chennai"
        }
    ],
    "Hyderabad": [
        {
            "id": "AP-09",
            "name": "Hyderabad"
        },
        {
            "id": "AP-11",
            "name": "Malakpet Hyderabad East"
        },
        {
            "id": "TS-09",
            "name": "Hyderabad Central"
        },
        {
            "id": "AP-13",
            "name": "Tolichowki Hyderabad West"
        },
        {
            "id": "AP-12",
            "name": "Kisanbagh Hyderabad South"
        },
        {
            "id": "TS-14",
            "name": "Hyderabad"
        },
        {
            "id": "TS-13",
            "name": "Tolichowki Hyderabad West"
        },
        {
            "id": "TS-11",
            "name": "Malakpet Hyderabad East"
        }
    ],
    "Ahmedabad": [
        {
            "id": "GJ-1",
            "name": "Ahmedabad"
        }
    ],
    "Kolkata": [
        {
            "id": "WB-02",
            "name": "Kolkata"
        },
        {
            "id": "WB-06",
            "name": "Kolkata"
        },
        {
            "id": "WB-01",
            "name": "Kolkata"
        },
        {
            "id": "WB-04",
            "name": "Kolkata"
        },
        {
            "id": "WB-08",
            "name": "Kolkata"
        },
        {
            "id": "WB-10",
            "name": "Kolkata"
        },
        {
            "id": "WB-09",
            "name": "Kolkata"
        },
        {
            "id": "WB-03",
            "name": "Kolkata"
        },
        {
            "id": "WB-07",
            "name": "Kolkata"
        },
        {
            "id": "WB-05",
            "name": "Kolkata"
        }
    ],
    "Faridabad": [
        {
            "id": "HR-51",
            "name": "Faridabad"
        },
        {
            "id": "DL-16",
            "name": "Faridabad"
        },
        {
            "id": "HR-38",
            "name": "Faridabad"
        }
    ]
}