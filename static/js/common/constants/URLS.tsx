var LANDING = {
	CAR_LANDING: '/home/car',
	CAR_PRODUCT: '/motor/car-insurance/',
	CAR_RESULTS: '/motor/car-insurance/#results',
	BIKE_LANDING: '/home/bike',
	BIKE_FALLBACK: '/home/bike/product',
	BIKE_RESULTS: '/home/bike/results',
	BIKE_PROPOSAL: '/home/bike/buyplan',
	BIKE_PRODUCT: '/motor/twowheeler-insurance/',
	TRAVEL_SINGLETRIP: '/home/travel/single-trip',
	TRAVEL_MULTITRIP: '/home/travel/multi-trip',
	HEALTH_INDEMNITY: '/home/health/indemnity',
	HEALTH_SUPERTOPUP: '/home/health/super-topup',
	LIFE_LANDING: '/home/life'
}

var CMS = {
	CAR_LANDING: '/car-insurance',
	CAR_PRODUCT: '/motor/car-insurance/',
	CAR_RESULTS: '/motor/car-insurance/#results',
	BIKE_LANDING: '/two-wheeler-insurance',
	BIKE_FALLBACK: '/two-wheeler-insurance/product',
	BIKE_RESULTS: '/two-wheeler-insurance/results',
	BIKE_PROPOSAL: '/two-wheeler-insurance/buyplan',
	TRAVEL_SINGLETRIP: '/travel-insurance',
	TRAVEL_MULTITRIP: '/travel-insurance/multi-trip',
	HEALTH_INDEMNITY: '/health-insurance',
	HEALTH_SUPERTOPUP: '/health-insurance/super-topup',
	LIFE_LANDING: '/term-insurance'
}

export = CMS