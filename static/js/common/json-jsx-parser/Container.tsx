import React = require('react')
import ReactDOM = require('react-dom')
import assign = require('object-assign')
import Button = require('../react-component/Button')
import TRIGGERS = require('./triggers')

interface IProps {
	babies: any[];
	childProps: any;
	title: string;
	editLabel: string;
	submitLabel: string;
	status: number;
	onSubmit: any;
	onEdit: any;
	identifier?: string;
	customClass?:string;
	getCIDVMap: Function;
	pushData: Function;
	showSummary: boolean; 
	summaryLabel?:string;
}

class Container extends React.Component<IProps, any> {
	refs: any;
	constructor(props: IProps) {
		super(props)
		this.CIDVTrigger = this.CIDVTrigger.bind(this)
		this.state = {
			status: props.status,
			error: false,
			errorMessage: null
		}
	}

	public getChild(ref, iterableRefs: any = this.refs) {
		for (let key in iterableRefs) {
			let refs = iterableRefs[key].getRef()
			if (key === ref){
				return iterableRefs[key]
			}
			else if(refs){
				let result = this.getChild(ref, refs)
				if(result)
					return result
			}
			else {
				continue
			}
		}
	}

	public validate(isShowErrors:boolean = true, isValidateHidden:boolean = false, isAlwaysValidateCIDV:boolean = false): boolean {
		let valid = true
		for(let index in this.refs){
			let wrapper:any = this.refs[index]
			valid = valid && wrapper.validate(isShowErrors,isValidateHidden,isAlwaysValidateCIDV)
			/*if (!valid)
				break*/
		}
		return valid
	}

	protected componentWillReceiveProps(nextProps){
		this.state.status = parseInt(nextProps.status)
	}

	protected CIDVTrigger(value: any, triggerName: string, errorMessage?: string) {
		let returnVal = true
		let trigger = this.props.getCIDVMap(this)[triggerName]
		if (triggerName === TRIGGERS.PUSH_DATA_TRIGGER) {
			this.props.pushData(value)
		} else if (triggerName === TRIGGERS.ON_BEFORE_TABBED_CONTAINER_SUMMARY_RENDER && trigger) {
			returnVal = trigger(value) 
		} else if (trigger) {
			returnVal = trigger(value) 
			returnVal = typeof returnVal === 'undefined' ? true : !!returnVal
			this.setState({error: returnVal, errorMessage: errorMessage})
		}
		return returnVal
	}

	protected onSubmit() {
		if(!this.validate())
			return
		if (!this.CIDVTrigger(this.props.identifier, TRIGGERS.ON_BEFORE_SUBMIT_CONTAINER))
			return
		if(this.props.onSubmit)
			this.props.onSubmit(this.props['identifier'])
		this.CIDVTrigger(this.props.identifier, TRIGGERS.ON_AFTER_SUBMIT_CONTAINER)
	}

	protected onEdit() {
		if (!this.CIDVTrigger(this.props.identifier, TRIGGERS.ON_BEFORE_EDIT_CONTAINER))
			return
		if(this.props.onEdit)
			this.props.onEdit(this.props['identifier'])
		this.CIDVTrigger(this.props.identifier, TRIGGERS.ON_AFTER_EDIT_CONTAINER)
	}

	render() {
		let hidden = false
		let container
		if(this.state.status < 1)
			hidden = true
		if(this.props.showSummary){
			let JSX = []
			for (let index in this.refs) {
				let verboseData = this.refs[index]['getValueVerbose']()
				let text:any = ""
				if(verboseData[2] && verboseData[2] == true){
					text = verboseData[1]
				}else if(verboseData[1] != undefined && verboseData[1] !== ""){
					text = <div className='kv'><span className='kv__key'>{verboseData[0]}: </span><span className='kv__value'>{verboseData[1]}</span></div>
				}
				if(text !== "")
					JSX.push(<div key={index}>{text}</div>)
			}
			container = <div id={this.props.identifier} className='container_summary'><div className='container_summary__title'>{this.props.summaryLabel}<Button key="edit" customClass={'w--button--link w--button--thin'} title={this.props['childProps'].editLabel} onClick={this.onEdit.bind(this) }/></div><div className='container_summary__items'>{JSX}</div></div>
			hidden = true
		}

		let children = []
		for(let index in this.props.babies){
			let Comp = this.props.babies[index]['component']
			let props = this.props.babies[index]['props']
			let obj = { 'CIDVTrigger': this.CIDVTrigger, forceUpdateParent: this.forceUpdate.bind(this) }
			children.push(<Comp {...assign(obj, props)}/>)
		}
		container = <div className={this.props.customClass}>{hidden ? container : null}<div hidden={hidden}>
			{children}
			<div className='error-label' hidden={this.state.error}>{this.state.errorMessage}</div>
			<Button key="submit" customClass="w--button--empty w--button--small w--button--thin proposal_container_btn" title={this.props['childProps'].submitLabel} onClick={this.onSubmit.bind(this) }/>
		</div></div>

		return container
	}
}

export = Container