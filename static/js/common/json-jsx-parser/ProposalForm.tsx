import React = require('react')
import ReactDOM = require('react-dom')
import assign = require('object-assign')
import Checkbox = require('../new-components/ButtonCheckbox')
import Currency = require('../new-components/CurrencyFormatter')
import Button = require('../react-component/Button')
import CommonUtils = require('../utils/CommonUtils')

interface IProps{
	formData: any;
	CIDV: any;
	action: any;
	onPushData?:Function,
	preFilledValueMap?:any,
	isAddReviewSection?:boolean,
	reviewSectionText?:string,
	onEditStage?:any,
	onStageSubmit?:any,
	premiumAmount?:number
}

interface IState{
	containerStatus?: any;
	filledFormData?: any;
	declaration?: boolean;
	loadingPG?: boolean;
}

class ProposalForm extends React.Component<IProps, IState> {
	refs:any
	constructor(props) {
		super(props)
		this.state = { containerStatus: {}, filledFormData: props.preFilledValueMap, declaration: true, loadingPG: false }
		this.initContainerStatus(props.formData)
	}

	static defaultProps = {
		isAddReviewSection: true,
		reviewSectionText:'Review and pay'
	}
	/*
	 * 1 - Open, 0 - Review, -1 - Closed
	 */
	initContainerStatus(formData){
		let nextStatus = 1
		for(let index in formData){
			let currentProps = formData[index]['props']
			this.state.containerStatus[currentProps['identifier']] = {index: index, status: 0}
		}
	}

	updateContainerStatus(){
		var isInValidFound = false
		var containerStatus = {}
		var index = 0
		for(let key in this.refs){
			if(isInValidFound){
				containerStatus[key] = {index: index, status: -1}
			}
			//if container is invalid open it OR if all containers are valid open the last container
			else if(!this.refs[key].validate(false,true) || index == Object.keys(this.refs).length - 1){
				isInValidFound = true
				containerStatus[key] = {index: index, status: 1}
			}
			else {
				containerStatus[key] = {index: index, status: 0}
			}
			index = index + 1
		}
		this.setState({
			containerStatus:containerStatus
		})
	}

	componentDidMount(){
		this.updateContainerStatus()
		this.validate(false,false,true)
	}

	onSubmit = (key) => {
		let containerStatus = this.state.containerStatus
		containerStatus[key]['status'] = 0
		let nextIndex = parseInt(containerStatus[key]['index']) + 1
		for (let index in containerStatus) {
			if (containerStatus[index]['index'] == nextIndex
				&& containerStatus[index]['status'] !== null) {
				containerStatus[index]['status'] = 1
				break
			}
		}

		if(this.props.onStageSubmit){
			this.props.onStageSubmit(key)
		}

		this.setState({ containerStatus: containerStatus })
	}

	onEdit = (key) => {
		let containerStatus = this.state.containerStatus
		if(containerStatus[key]['status'] === -1)
			return
		containerStatus[key]['status'] = 1
		let min = parseInt(containerStatus[key]['index'])
		for (let index in containerStatus){
			if (containerStatus[index]['index'] > min
				&& containerStatus[index]['status'] !== null){
				containerStatus[index]['status'] = -1
			}
		}

		if(this.props.onEditStage){
			this.props.onEditStage(key)
		}

		this.setState({ containerStatus: containerStatus })
	}

	pushData = (obj: any) => {
		if(obj != undefined){
			this.state.filledFormData = assign(this.state.filledFormData, obj)
			if(this.props.onPushData)
				this.props.onPushData(this.state.filledFormData)
			}
	}

	onMakePayment = () => {
		var isValid  = this.validate()
		if(isValid){
			if(this.props.onPushData){
				this.props.onPushData(this.state.filledFormData)
			}
			this.props.action(this.state.filledFormData)
			this.setState({ loadingPG: true })
		}
		
	}

	onClickDeclaration = () => {
		this.setState({ declaration: !this.state.declaration })
	}

	protected validate(isShowErrors:boolean = true, isValidateHidden:boolean = false, isAlwaysValidateCIDV:boolean = false): boolean {
		let valid = true
		for(let index in this.refs){
			let wrapper:any = this.refs[index]
			valid = wrapper.validate(isShowErrors,isValidateHidden,isAlwaysValidateCIDV) && valid 
			/*if (!valid)
				break*/
		}
		return valid
	}

	render() {
		var self = this
		let showSummary = true
		let newComponents = []
		let containers = []
		for(let key in this.state.containerStatus){
			showSummary = showSummary && (this.state.containerStatus[key]['status'] === 0)
		}
		for(let index in this.props.formData){
			let Comp = this.props.formData[index]['component']
			let props = this.props.formData[index]['props']
			let status = this.state.containerStatus[props['identifier']]
			let visibilityStatus = status['status'] === 0 ? 'done' : ( status['status'] === 1 ? 'active' : 'inactive')
			let obj = {}
			if(status['status'] !== null){
				obj['status'] = status['status']
				obj['onSubmit'] = this.onSubmit
				obj['onEdit'] = this.onEdit
				obj['getCIDVMap'] = this.props.CIDV
				obj['pushData'] = this.pushData
				obj['showSummary'] = showSummary
			}
			containers.push(<div className={'stage-list-item ' + visibilityStatus} key={index} onClick={this.onEdit.bind(this, props.identifier)}>{props.title}</div>)	
			newComponents.push(<Comp {...assign(obj, props)}/>)
		}

		//this is just a read only html element. Does not have any functionality
		if(this.props.isAddReviewSection){
			containers.push(<div className={'stage-list-item '} key={this.props.formData.length} >{this.props.reviewSectionText}</div>)
		}
		
		var checkBoxLabel = 'I declare that the information provided above is true and I authorize Coverfox Insurance Broking Pvt. Ltd. to represent me at Insurance Companies or Agencies for my Insurance needs.'
		//var makePaymentTitle = <Currency value={this.props.premiumAmount} label="Make payment of "/>
		var formattedPremiumAmount = CommonUtils.moneyFormat(this.props.premiumAmount)
		var makePaymentTitle = "MAKE PAYMENT (Rs." + formattedPremiumAmount + ")"
		return (
			<div className='bike-proposal-wrapper clearfix'> 
				<div hidden={showSummary} className='stage-list'>{containers}</div>
				<div className={'stage '+ (showSummary? 'summary':'')}>
				<div className={'stage-items '+ (showSummary? 'summary-wrapper':'')}>{newComponents}</div>
				{showSummary ? <div><Checkbox customClass='declaration_checkbox' label={checkBoxLabel} isChecked={this.state.declaration} onChange={this.onClickDeclaration} />
						<Button title={makePaymentTitle} customClass='w--button--orange w--button--large payment_btn' type={this.state.loadingPG ? 'loading' : ''} disabled={this.state.loadingPG || !this.state.declaration} onClick={this.onMakePayment} /></div> : null}
				</div>
			</div>
		)
	}
}

export = ProposalForm