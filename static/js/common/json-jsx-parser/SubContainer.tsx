import React = require('react')
import ReactDOM = require('react-dom')
import assign = require('object-assign')
import Button = require('../react-component/Button')

interface IProps {
	babies: any[];
	childProps: any;
	title: string;
	isOpen: boolean;
	identifier?: string;
	CIDVTrigger: Function;
	customClass?: string;
	isSummaryCommaSeperated?: boolean;
	hidden?:boolean;
	summaryLabel?:string;
}

class SubContainer extends React.Component<IProps, any> {
	static defaultProps = {
		isSummaryCommaSeperated : false,
		hidden: false
	}

	constructor(props: IProps) {
		super(props)
		this.state = {
			isOpen: props['childProps'].isOpen,
			hidden: props.hidden
		}
	}

	componentWillReceiveProps(nextProps:IProps){
		this.setState({
			hidden: nextProps.hidden
		})
	}

	getRef(){
		return this.refs
	}

	getValueVerbose() {
		let values = []
		let isJSX: boolean = false
		for (let index in this.refs) {
			let text = this.refs[index]['getValueVerbose']()
			if(text[1]==undefined || text[1]==="")
				continue
			let data = this.props.isSummaryCommaSeperated ? text[1] : <div className='kv' key={index}>{text[0] ? <span className='kv__key'>{text[0] + ": "}</span> : null}<span className='kv__value'>{text[1]}</span></div>
			values.push(data)
		}
		let output = this.props.isSummaryCommaSeperated ? values.join(', ') : <div key={this.props.identifier + '_summary'}>{values}</div>
		return [<div className='subcontainer_title'>{this.props.summaryLabel}</div>, output, !this.props.isSummaryCommaSeperated]
	}

	isHidden():boolean {
		return this.state.hidden
	}

	validate(isShowErrors:boolean = true, isValidateHidden:boolean = false, isAlwaysValidateCIDV:boolean = false, isValidateHiddenChildren:boolean = false): boolean {
		let valid = true
		if(this.state.hidden && !isValidateHidden)
			return valid
		for (let index in this.refs) {
			let wrapper: any = this.refs[index]
			valid = wrapper.validate(isShowErrors,isValidateHiddenChildren,isAlwaysValidateCIDV) && valid 
			/*if (!valid)
				break*/
		}
		return valid
	}

	hide() {
		this.setState({ hidden: true })
	}
	
	show(){
		this.setState({ hidden: false })
	}

	render() {
		let childrens = []
		for (let index in this.props.babies) {
			let Comp = this.props.babies[index]['component']
			let props = this.props.babies[index]['props']
			let obj = { 'CIDVTrigger': this.props.CIDVTrigger }
			childrens.push(<Comp {...assign(obj, props)}/>)
		}
		return <div className={this.props.customClass} hidden={this.state.hidden}>{childrens}</div>
	}
}

export = SubContainer