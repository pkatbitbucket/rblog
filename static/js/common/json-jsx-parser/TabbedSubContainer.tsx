import React = require('react')
import ReactDOM = require('react-dom')
import assign = require('object-assign')
import TRIGGERS = require('./triggers')

interface IProps {
	babies: any[];
	childProps: any;
	identifier: string;
	CIDVTrigger: Function;
	forceUpdateParent: any;
	customClass?:string;
	summaryLabel?:string;
}

interface IState{
	selectedTab?: number;
	visitedTabsIndices?:number[],
	hiddenTabs?:number[]
}

class TabbedSubContainer extends React.Component<IProps, IState> {
	MAX_INDEX: number;
	constructor(props: IProps) {
		super(props)
		this.goToTab = this.goToTab.bind(this)
		this.state = {
			selectedTab: 0,
			visitedTabsIndices:[0],
			hiddenTabs:[]
		}
		this.MAX_INDEX = props.babies.length - 1
	}

	getRef() {
		return this.refs
	}

	getSelectedTab(){
		return this.state.selectedTab
	}

	getVisitedTabs(){
		return this.state.visitedTabsIndices
	}

	getHiddenTabs(){
		return this.state.hiddenTabs
	}

	hideTab(tabIndex:number){
		var hiddenTabs = this.state.hiddenTabs
		if(hiddenTabs.indexOf(tabIndex) == -1){
			hiddenTabs.push(tabIndex)
		}
		this.setState({
			hiddenTabs:hiddenTabs
		})
	}
	showTab(tabIndex:number){
		var hiddenTabs = this.state.hiddenTabs
		var foundIndex = -1
		foundIndex = hiddenTabs.indexOf(tabIndex)
		hiddenTabs.splice(foundIndex,1)
		this.setState({
			hiddenTabs:hiddenTabs
		})
	}

	goToTab(index){
		var visitedTabsIndices = this.state.visitedTabsIndices
		if (index >= 0 && index <= this.MAX_INDEX && this.props.CIDVTrigger({ id: this.props.identifier, index: index }, TRIGGERS.ON_BEFORE_TAB_CHANGE)) {
			if(visitedTabsIndices.indexOf(index) == -1){
				visitedTabsIndices.push(index)
			} 
			this.setState({ selectedTab: index }, () => {
				this.props.forceUpdateParent()
				this.props.CIDVTrigger({ id: this.props.identifier, index: index }, TRIGGERS.ON_AFTER_TAB_CHANGE)
			})
		}
	}

	getValueVerbose() {
		var style = {fontWeight:'bold'}
		/* true - tabs, false - all tabs one after other, [1,3] - specific indices to be displayed */
		let triggerResponse = this.props.CIDVTrigger(this.props.identifier, TRIGGERS.ON_BEFORE_TABBED_CONTAINER_SUMMARY_RENDER)
		if (triggerResponse === true) {
			let tabs = []
			let subcontainer = []
			for (let index in this.props.babies) {
				index = parseInt(index)
				let refKey = this.props.babies[index]['props']['identifier']
				let text = this.refs[refKey]['getValueVerbose']()
				subcontainer.push(<div style={style} key={index} hidden={index !== this.state.selectedTab}>{text[1]}</div>)
				tabs.push(this.getTabJSX(this.props.babies[index]['props']['summaryLabel'], index))
			}
			return ['', <div><div style={style}>{tabs}</div><div>{subcontainer}</div></div>, true]
		}else{
			let divs = []
			for (let index in this.props.babies) {
				index = parseInt(index)
				if(triggerResponse !== false && triggerResponse.indexOf(index) > -1)
					continue
				let refKey = this.props.babies[index]['props']['identifier']
				let text = this.refs[refKey]['getValueVerbose']()
				divs.push(<div  key={index}><div>{text}</div></div>)
			}
			return ['',<div>{divs}</div>, true]
		}
	}

	validate(isShowErrors:boolean = true, isValidateHidden:boolean = false, isAlwaysValidateCIDV:boolean = false): boolean {
		let valid = true
		for (let index in this.refs) {
			let wrapper: any = this.refs[index]
			valid = wrapper.validate(isShowErrors,isValidateHidden,isAlwaysValidateCIDV) && valid 
			/*if (!valid)
				break*/
		}
		return valid
	}

	getTabJSX(title, index){
		return <div key={index} onClick={this.goToTab.bind(this, index) }
			className={'w--tabs__tab ' + (index === this.state.selectedTab ? 'active' : '')}>{ title }</div >
	}

	render() {
		let tabs = []
		let childrens = []
		for (let index in this.props.babies) {
			index = parseInt(index)
			let Comp = this.props.babies[index]['component']
			let props = this.props.babies[index]['props']
			props["hidden"] = (index !== this.state.selectedTab)
			let obj = { 'CIDVTrigger': this.props.CIDVTrigger }

			if(this.state.hiddenTabs.indexOf(index) == -1){
				tabs.push(this.getTabJSX(props.title, index))
			}
			childrens.push(<div key={props.key} hidden={index !== this.state.selectedTab}><Comp {...assign(obj, props) }/></div>)
		}
		return (
				<div className={this.props.customClass}>
					<div className='w--tabs'>
						{
							tabs.length > 1 ?
							tabs
							:null
						}
					</div>
					<div>{childrens}</div>
				</div>
			)
	}
}

export = TabbedSubContainer