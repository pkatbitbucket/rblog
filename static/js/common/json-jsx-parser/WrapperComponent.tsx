import React = require('react')
import ReactDOM = require('react-dom')
import assign = require('object-assign')
import TRIGGERS = require('./triggers')
import Helpers = require('../utils/Helpers')
interface IProps{
	childProps: any;
	childElement: any;
	title: string;
	eventProp: string;
	valueProp: string;
	validation?: string[];
	customValidation?: string[];
	key?: string;
	ref?: string;
	type: string;
	identifier?: string;
	CIDVTrigger: Function;
	htmlId?:string;
	customClass?:string;
	displayValueMap?:any;
	summaryLabel?:string;
}

const REF_PREFIX = '_wrapped'

class WrapperComponent extends React.Component<IProps, any> {
	refs:any
	constructor(props: IProps){
		super(props)
		this.state = {hidden: false}
		this.bindEvents()
		this.validate = this.validate.bind(this)
		this.state[props.valueProp] = props.childProps[props.valueProp]
		this.state.mutatedProps = {}
		//this.triggerPushData(props.childProps[props.valueProp])
	}

	getRef(){
		return false
	}

	bindEvents(){
		let specialCases = ['text', 'mobile', 'email', 'numeric']
		if (specialCases.indexOf(this.props.type) >= 0 && this.props.eventProp !== 'onBlur') {
			this.onEvent = this.onEvent.bind(this)
			this['onBlur'] = this.pushDataEvent.bind(this)
		} else {
			let event = this.onEvent.bind(this)
			let push = this.pushDataEvent.bind(this)
			this.onEvent = function(value){
				event(value)
				push(value)
			}
		}
	}

	getValueFromUseCase(rawValue, isPushData:boolean = false){
		let outVal
		switch (this.props.type) {
			case "calendar":
				var format = this.props.childProps['format']
				outVal = rawValue.format(format, true)
				break
			case "select":
				var valueField = this.props.childProps['optionsValue']
				if(valueField == undefined){
					valueField = "value"
				}
				outVal = rawValue[valueField]
				break
			case "registration-number":
				outVal = rawValue.join('-')
				break
			case "text":
				if(isPushData)
					outVal = rawValue.trim()
				else
					outVal = Helpers.trimAdditionalSpaces(rawValue)
				break
			case "numeric":
				if(isPushData)
					outVal = rawValue.trim()
				else
					outVal = Helpers.trimAdditionalSpaces(rawValue)
				break
			case "email":
				if(isPushData)
					outVal = rawValue.trim()
				else
					outVal = Helpers.trimAdditionalSpaces(rawValue)
				break
			case "mobile":
				if(isPushData)
					outVal = rawValue.trim()
				else
					outVal = Helpers.trimAdditionalSpaces(rawValue)
				break
			default:
				outVal = rawValue
				break
		}
		return outVal
	}

	onEvent(value:any){
		let obj = {}
		obj[this.props.valueProp] = this.getValueFromUseCase(value)
		this.state[this.props.valueProp] = obj[this.props.valueProp]
		this.setState(obj)
	}

	pushDataEvent(value){
		if(!this.validate())
			return
		this.props.CIDVTrigger(this.props.identifier, TRIGGERS.BEFORE_PUSH_DATA_TRIGGER)
		this.triggerPushData(this.getValueFromUseCase(value, true))
	}

	triggerPushData(value: any){
		if(value!= "" && value!= undefined){
			let obj = {}
			obj[this.props.identifier] = value
			this.props.CIDVTrigger(obj, TRIGGERS.PUSH_DATA_TRIGGER)
		}
	}

	getValue(){
		return this.state[this.props.valueProp]
	}
		
	getVerboseDataByUseCase(){
		var returnVal
		switch (this.props.type) {
			case "select":
				returnVal = this.refs[REF_PREFIX + this.props.identifier].getSelectedLabels()
				returnVal = returnVal ? returnVal.toString() : ''
				break;
			default:
				returnVal = this.getValue()
				break;
		}
		return returnVal
	}

	getValueVerbose(){
		var value = this.getVerboseDataByUseCase()
		value = this.props.displayValueMap && this.props.displayValueMap[value] ? this.props.displayValueMap[value] : value
		
		//in case of hidden component its verbose value will not be shown
		if(this.state.hidden)
			value = ""

		return [this.props.summaryLabel, value, false]
	}

	addMutatedProps(propsString:string, value:any){
		//used to dynamically mutate child props and re-render child with new props
		var mutatedProps = this.state.mutatedProps
		mutatedProps[propsString] = value
		this.setState({
			mutatedProps: mutatedProps
		})
	}

	validate(isShowErrors:boolean = true, isValidateHidden:boolean = false, isAlwaysValidateCIDV:boolean = false): boolean{
		if (this.state.hidden && !isValidateHidden)
			return true
		let valid = true
		let node: any = this.refs[REF_PREFIX + this.props.identifier]
		switch (this.props.type) {
			default:
				valid = node.isValid(isShowErrors)
				break
		}
		if(valid || isAlwaysValidateCIDV)
			valid = this.validateCIDV() && valid
		return valid
	}

	validateCIDV(): boolean{
		let valid = true
		let validations = this.props.customValidation
		if (!validations || !validations.length || !this.props.CIDVTrigger || this.state.hidden)
			return valid
		for (let index in validations) {
			let tValidation = typeof validations[index] === 'string' ? validations[index] : validations[index]['name']
			let tValidationMsg = typeof validations[index] === 'string' ? undefined : validations[index]['message']
			valid = valid && this.props.CIDVTrigger(this.state[this.props.valueProp], tValidation, tValidationMsg)
			if(!valid)
				break
		}
		return valid
	}

	hide() {
		this.setState({ hidden: true })
	}
	
	show(){
		this.setState({ hidden: false })
	}

	isHidden(){
		return this.state.hidden
	}

	render(){
		var obj = {}
		obj['onBlur'] = this['onBlur']
		obj[this.props.eventProp] = this.onEvent
		obj[this.props.valueProp] = this.state[this.props.valueProp]
		obj['ref'] = REF_PREFIX + this.props.identifier
		var props = assign({}, this.props.childProps, obj, this.state.mutatedProps)
		return (<div id={this.props['htmlId']} className={this.props.customClass} hidden={this.state.hidden}>{React.createElement(this.props.childElement, props) }</div>)
	}
}

export = WrapperComponent