import ProposalForm = require('./ProposalForm')
import FormContainer = require('./Container')
import TabbedSubContainer = require('./TabbedSubContainer')
import FormSubcontainer = require('./SubContainer')
import Button = require('../react-component/Button')
import TextInput = require('../new-components/TextInput')
import NumberInput = require('../new-components/NumberInput')
import EmailInput = require('../new-components/EmailInput')
import MobileInput = require('../new-components/MobileInput')
import RadioInput = require('../new-components/RadioInput')
import SelectInput = require('../react-component/MultiSelect')
import DOBPicker = require('../react-component/DOBPicker')
import Calendar = require('../react-component/Calendar')
import Checkbox = require('../new-components/ButtonCheckbox')
import RegistrationNumberInput = require('../new-components/RegistrationNoInput3')

export = {
	'form': {
		component: ProposalForm,
		mappedComponent:'ProposalForm',
		propsMap: [],
		isWrapped: false
	},
	'container': {
		component: FormContainer,
		mappedComponent:'FormContainer',
		propsMap: [
			{ propKey: 'title', jsonKey: 'title' },
			{ propKey: 'editLabel', jsonKey: 'editLabel' },
			{ propKey: 'submitLabel', jsonKey: 'submitLabel' },
			{ propKey: 'isOpen', jsonKey: 'isOpen' },
			{ propKey: 'children', jsonKey: 'children' },
		],
		isWrapped: false
	},
	'tabbed-container': {
		component: TabbedSubContainer,
		mappedComponent:'TabbedSubContainer',
		propsMap: [],
		isWrapped: false
	},
	'sub-container': {
		component: FormSubcontainer,
		mappedComponent:'FormSubcontainer',
		propsMap: [
			{propKey: 'title', jsonKey: 'title'},
			{propKey: 'isSummaryCommaSeperated', jsonKey: 'isSummaryCommaSeperated'}
		],
		isWrapped: false
	},
	'button': {
		component: Button,
		mappedComponent:'Button',
		propsMap: [
			{propKey: 'title', jsonKey: 'title'}
		],
		valueProp: '',
		eventProp: 'onClick',
		isWrapped: true
	},
	'text': {
		component: TextInput,
		mappedComponent:'TextInput',
		propsMap: [
			{propKey: 'label', jsonKey: 'title'},
			{propKey: 'value', jsonKey: 'defaultValue'},
			{propKey: 'placeholder', jsonKey: 'placeholder'},
			{propKey: 'validations', jsonKey: 'validators'},
			{propKey: 'maxLength', jsonKey: 'maxLength'},
			{propKey: 'description', jsonKey: 'description' }
		],
		valueProp: 'value',
		eventProp: 'onChange',
		isWrapped: true,
	},
	'numeric': {
		component: NumberInput,
		mappedComponent:'NumberInput',
		propsMap: [
			{propKey: 'label', jsonKey: 'title'},
			{propKey: 'value', jsonKey: 'defaultValue'},
			{propKey: 'placeholder', jsonKey: 'placeholder'},
			{propKey: 'maxLength', jsonKey: 'maxLength'},
			{propKey: 'validations', jsonKey: 'validators'},
			{propKey: 'description', jsonKey: 'description' }
		],
		valueProp: 'value',
		eventProp: 'onChange',
		isWrapped: true
	},
	'email': {
		component: EmailInput,
		mappedComponent:'EmailInput',
		propsMap: [
			{propKey: 'label', jsonKey: 'title'},
			{propKey: 'value', jsonKey: 'defaultValue'},
			{propKey: 'placeholder', jsonKey: 'placeholder'},
			{propKey: 'validations', jsonKey: 'validators'},
			{propKey: 'description', jsonKey: 'description' }
		],
		valueProp: 'value',
		eventProp: 'onChange',
		isWrapped: true
	},
	'mobile': {
		component: MobileInput,
		mappedComponent:'MobileInput',
		propsMap: [
			{propKey: 'label', jsonKey: 'title'},
			{propKey: 'value', jsonKey: 'defaultValue'},
			{propKey: 'placeholder', jsonKey: 'placeholder'},
			{propKey: 'validations', jsonKey: 'validators'},
			{propKey: 'description', jsonKey: 'description' }
		],
		valueProp: 'value',
		eventProp: 'onChange',
		isWrapped: true
	},
	'radio': {
		component: RadioInput,
		mappedComponent:'RadioInput',
		propsMap: [
			{propKey: 'value', jsonKey: ''},
			{propKey: 'optionsList', jsonKey: 'options'},
			{propKey: 'labelText', jsonKey: 'title'},
			{propKey: 'validations', jsonKey: 'validators'},

		],
		valueProp: 'value',
		eventProp: 'onChange',
		isWrapped: true
	},
	'select': {
		component: SelectInput,
		mappedComponent:'SelectInput',
		propsMap: [
			{propKey: 'labelText', jsonKey: 'title'},
			{propKey: 'dataSource', jsonKey: 'options'},
			{propKey: 'values', jsonKey: 'defaultValue'},
			{propKey: 'placeHolder', jsonKey: 'placeholder'},
			{propKey: 'validations', jsonKey: 'validators'},
			{propKey: 'isSearchable', jsonKey: 'isSearchable'},
			{propKey: 'disabled', jsonKey: 'disabled'},
			{ propKey: 'optionsText', jsonKey: 'optionsText' },
			{ propKey: 'optionsValue', jsonKey: 'optionsValue' },
			{ propKey: 'disabled', jsonKey: 'isDisabled' }
		],
		valueProp: 'values',
		eventProp: 'onChange',
		isWrapped: true
	},
	'calendar': {
		component: Calendar,
		mappedComponent:'Calendar',
		propsMap: [
			{propKey: 'defaultDate', jsonKey: 'defaultDate'},
			{propKey: 'minDate', jsonKey: 'minDate'},
			{propKey: 'maxDate', jsonKey: 'maxDate'},
			{propKey: 'labelText', jsonKey: 'title'},
			{propKey: 'validations', jsonKey: 'validators'},
			{propKey: 'format', jsonKey: 'format'},
			{propKey: 'defaultDisplayMonthDate', jsonKey: 'defaultDisplayMonthDate'},
			{propKey: 'prevMonths', jsonKey: 'prevMonths'},
			{propKey: 'nextMonths', jsonKey: 'nextMonths'}
		],
		valueProp: 'defaultDate',
		eventProp: 'onSelectDate',
		isWrapped: true
	},
	'checkbox': {
		component: Checkbox,
		mappedComponent:'Checkbox',
		propsMap: [
			{propKey: 'isChecked', jsonKey: 'defaultValue'},
			{propKey: 'label', jsonKey: 'title'},
			{propKey: 'validations', jsonKey: 'validators'}
		],
		valueProp: 'defaultValue',
		eventProp: 'onChange',
		isWrapped: true
	},
	'registration-number': {
		component: RegistrationNumberInput,
		mappedComponent:'RegistrationNumberInput',
		propsMap: [
			{propKey: 'defaultValue', jsonKey: 'defaultValue'},
			{propKey: 'validations', jsonKey: 'validators'},
			{propKey: 'placeholder', jsonKey: 'placeholder'},
			{propKey: 'labelText', jsonKey: 'title'},
			{propKey: 'isRefreshWithExternalValue', jsonKey: 'isRefreshWithExternalValue'}
		],
		valueProp: 'defaultValue',
		eventProp: 'onValidBlur',
		isWrapped: true
	}
}