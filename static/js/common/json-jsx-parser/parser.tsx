import React = require('react')
import ReactDOM = require('react-dom')
import ParseMap = require('./parse-map')
import WrapperComponent = require('./WrapperComponent')
 

const TYPE = 'type'
const COMPONENT = 'component'
const PROPS_MAP = 'propsMap'
const PROP_KEY = 'propKey'
const JSON_KEY = 'jsonKey'
const CHILDREN = 'children'
const IS_WRAPPED = 'isWrapped'
const ID = 'id'
const CHILD_PROPS = 'childProps'
const VALUE_PROP  = 'valueProp'
const EVENT_PROP = 'eventProp'
const CHILD_ELEMENT = 'childElement'
const CHILD_ELEMENTS = 'babies'
const HTML_ID = 'htmlId'
const CUSTOM_CLASS = 'customClass'
const PARENT_COMPONENT = 'container'

function parseJSON(input: any, preFilledValueMap:any, cssMap:any, moduleName?:string): JSX.Element[] {
	var components = []
	for(let i = 0; i < input.length; i++){
		let outputObj: any = {}
		let props = {}
		let child = input[i]
		/*let {component: componentClass, propsMap: propsMap,
			eventProp: eventProp, valueProp: valueProp,
			isWrapped: isWrapped} = ParseMap[child[TYPE]]*/
        var map = ParseMap[child[TYPE]] 
        if(map){
            var componentClass = map.component 
            var componentName = map.mappedComponent
            var propsMap = map.propsMap
            var eventProp = map.eventProp
            var valueProp = map.valueProp
            var isWrapped = map.isWrapped
        } else {
            //if no mapping found of that component then skip it
            continue
        }

		let childProps = {}
		for (let j = 0; j < propsMap.length; j++) {
			let {propKey: propKey, jsonKey: jsonKey} = propsMap[j]
			if (child.hasOwnProperty(jsonKey)) {
				childProps[propKey] = child[jsonKey]
			}
		}

		if (child['customValidation'])
			props['customValidation'] = child['customValidation']
		
		props[CHILD_PROPS] = childProps
        props[CHILD_PROPS][CUSTOM_CLASS] = moduleName + "_" + componentName
        props['title'] = child['title']
        props['summaryLabel'] = child['summaryLabel'] != undefined ? child['summaryLabel'] : child['title']
        props['identifier'] = child[ID]
        props['key'] = child[ID]
        props['ref'] = child[ID]
        props['type'] = child[TYPE]
        props['customClass'] = moduleName + "_" + componentName  
        props[HTML_ID] = cssMap.hasOwnProperty(child[ID]) ? cssMap[child[ID]][HTML_ID] : child[ID]

        if (isWrapped){
            props[CHILD_ELEMENT] = componentClass
            props[EVENT_PROP] = eventProp
            props[VALUE_PROP] = valueProp
            props['displayValueMap'] = child['displayValueMap'] ? child['displayValueMap'] : undefined
            
            if(preFilledValueMap[child[ID]])
                props[CHILD_PROPS][valueProp] = preFilledValueMap[child[ID]]

            outputObj.component = WrapperComponent
        } else {
            if (!child.hasOwnProperty(CHILDREN))
                continue
            props[CHILD_ELEMENTS] = parseJSON(child[CHILDREN], preFilledValueMap, cssMap, moduleName)
            outputObj.component = componentClass
        }

        outputObj.props = props
		components.push(outputObj)
	}
	return components
}


function getContainerList(input: any):any{
    var containers = []
    for(let i = 0; i < input.length; i++){
        if(input[i][TYPE] == PARENT_COMPONENT){
            let item = {}
            item["title"] = input[i]["title"]
            item["identifier"] = input[i][ID]
            containers.push(item)
        }
    }
    return containers
}

export = { parseJSON: parseJSON,
            getContainerList: getContainerList}