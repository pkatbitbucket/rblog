export = {
	ON_BEFORE_TABBED_CONTAINER_SUMMARY_RENDER: '__renderTabbedContainerSummary$$',
	PUSH_DATA_TRIGGER: '__pushFormDataFromWrapper$$',
	BEFORE_PUSH_DATA_TRIGGER: '__beforePushFormDataFromWrapper$$',
	ON_BEFORE_TAB_CHANGE: '__beforeTabTabbedSubContainer$$',
	ON_AFTER_TAB_CHANGE: '__afterTabTabbedSubContainer$$',
	ON_BEFORE_SUBMIT_CONTAINER: '__fireOnBeforeContainerSubmit$$',
	ON_AFTER_SUBMIT_CONTAINER: '__fireOnAfterContainerSubmit$$',
	ON_BEFORE_EDIT_CONTAINER: '__fireOnBeforeContainerEdit$$',
	ON_AFTER_EDIT_CONTAINER: '__fireOnAfterContainerEdit$$'
}