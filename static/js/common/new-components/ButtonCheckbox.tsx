/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')

interface IState{
	isChecked: boolean
}

interface IProps{
	value?:string | number | boolean,
	isChecked?: boolean,
	isDisabled?:boolean,
	onChange?: Function,
	customClass?: string,
	label?:string,
	key?:any,
	tabindex?: number
}

class ButtonCheckbox extends React.Component<IProps,IState> {	
	static defaultProps = {
			isChecked: false,
			isDisabled:false,
			customClass: '',
			label:'',
			tabindex:0
	}

	static propTypes = {
		isChecked:React.PropTypes.bool,
		isDisabled:React.PropTypes.bool,
		value: React.PropTypes.any,
		onChange: React.PropTypes.func,
		customClass: React.PropTypes.string,
		label:React.PropTypes.string,
		key: React.PropTypes.string,
		tabindex:React.PropTypes.number
	}


	componentWillReceiveProps(nextProps: IProps) {
		if (nextProps.isChecked!==undefined && nextProps.isChecked!==null) {
			this.setState({
				isChecked: nextProps.isChecked
			})
		}
	}

	constructor(props: IProps) {		
		super(props)	
		var isChecked : boolean
		isChecked = false
		if (props.isChecked) {
			isChecked = props.isChecked
		}
		this.state = {
			isChecked: isChecked
		}
	}

	handleChange(){
		var isChecked : boolean
		isChecked = !this.state.isChecked

		if(!this.props.isDisabled){
			this.setState({
				isChecked: isChecked
			},
				function(){
					if(this.props.onChange){
						this.props.onChange(isChecked, this.props.value)
					}
				}
			)	
		}
	}

	onKeyPress(e){
		let ENTER = 13
		if (e.keyCode == ENTER) {
			if(!this.props.isDisabled){
				this.handleChange()
			}
		}
	}

	render() {	
		var style = { background: this.state.isChecked == true ? 'grey' : 'white', maxWidth:'100px', border:'1px solid black', cursor:this.props.isDisabled == true?'auto':'pointer'}
		return (
			<div className={'w--check_box '+ (this.state.isChecked? 'checked ':'')+ (this.props.customClass? this.props.customClass:'')} tabIndex={this.props.tabindex} ref='ButtonCheckBox' onKeyUp={this.onKeyPress.bind(this) } onClick={this.handleChange.bind(this) } >
						{this.props.label}
			</div>
			)
	}

}

export = ButtonCheckbox