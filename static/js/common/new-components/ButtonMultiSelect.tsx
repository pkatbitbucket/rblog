import React = require('react')
import ReactDOM = require('react-dom')
import ButtonCB = require('./ButtonCheckbox')
import listensToClickOutside = require('./Dependencies/OnClickHandler')

interface IProps{
	label?: string;
	title?: string;
	tooltip?: string;
	value?: string | number | boolean | string[] | number[] | boolean[];
	onChange?: Function;
	customClass?: string;
	options: any[];
	multiSelect?: boolean;
	keyField?: string;
	valueField?: string;
	isBlankAllowed?:boolean
}

interface IState{
	selectedIndices?: any[];
	clicked?: boolean;
}

class ButtonMultiSelect extends React.Component<IProps, IState> {
	static defaultProps = {
		value: [],
		multiSelect: false,
		keyField: 'key',
		valueField: 'value',
		isBlankAllowed:false
	}

	private clickOutsideHandler: listensToClickOutside

	constructor(props) {
		super(props)
		var value:any = []
		if(props.value){
			if (props.value.constructor === Array) {
				value = props.value
			}
			else {
				value.push(props.value)
			}
		}
		this.state = {
			selectedIndices: [],
			clicked: false
		}
		this.updateSelectedIndices(this.props)
	}

	componentDidMount() {
        this.clickOutsideHandler = new listensToClickOutside(ReactDOM.findDOMNode(this.refs['BMS']), ()=>{
        	this.setState({clicked: false})
        })
    }

    componentWillUnmount() {
        this.clickOutsideHandler.dispose()
    }

    componentWillReceiveProps(nextProps){
		this.updateSelectedIndices(nextProps)
    }

    updateSelectedIndices(props:IProps){
    	let value:any = props.value
		if (value == undefined)
			return
    	if(typeof value !== 'object')
    		value = [value]
    	let selectedIndices = []
    	for(let index in props.options){
			if (value.indexOf(props.options[index][props.valueField])>=0){
				selectedIndices.push(index)
				if(props.multiSelect)
					break
			}
    	}
		this.state.selectedIndices = selectedIndices
    }

	toggleDropdown(){
		this.setState({clicked: !this.state.clicked})
	}

	close(){
		this.setState({clicked: false})
	}

	onKeyEnterPress(e){
		let ENTER = 13
		if (e.keyCode == ENTER) {
			this.toggleDropdown()
		}
	}

	onKeyNavigation(e){
		let LEFT = 37
		let UP = 38
		let RIGHT = 39
		let DOWN = 40
	}

	handleChange(checked:boolean, value:string | number | boolean){
		let selectedIndices = this.state.selectedIndices.slice()
		let isClicked: boolean = true
		let selectedIndex
		for (let index in this.props.options) {
			if (value === this.props.options[index][this.props.valueField]) {
				selectedIndex = index
				break
			}
		}

		if(!checked){
			let index = selectedIndices.indexOf(selectedIndex)

			//Not splicing if this.props.isBlankAllowed is false and selected indices has only 1 element
			if (this.props.isBlankAllowed || (!this.props.isBlankAllowed && selectedIndices.length > 1)){
				selectedIndices.splice(index, 1)
			}
			isClicked = false
		}else{
			if(this.props.multiSelect){
				selectedIndices.push(selectedIndex)
			}else{
				selectedIndices = [selectedIndex]
				isClicked = false
			}
		}

		this.setState({
			selectedIndices: selectedIndices,
			clicked: isClicked
		})

		let output = []
		for (let index in selectedIndices) {
			output.push(this.props.options[selectedIndices[index]][this.props.valueField])
		}
		output = this.props.multiSelect ? output : output[0]
		
		if(this.props.onChange)
			this.props.onChange(output)
	}

	render(){
		let options = []
		for(let index in this.props.options){
			let key = this.props.options[index][this.props.keyField]
			let value = this.props.options[index][this.props.valueField]
			let checked = this.state.selectedIndices.indexOf(index)>=0
			options.push(<ButtonCB tabindex={0} key={value+""} onChange={this.handleChange.bind(this)} label={key} value={value} isChecked={checked}/>)
		}

		var selected = this.state.selectedIndices && this.state.selectedIndices.length
		var selectedLabel = null
		if(selected){
			selectedLabel = []
			for (let index in this.state.selectedIndices) {
				selectedLabel.push(this.props.options[this.state.selectedIndices[index]][this.props.keyField])
			}
		}

		return (
			<div ref="BMS">
				<div tabIndex={0} onClick={this.toggleDropdown.bind(this)} onKeyUp={this.onKeyEnterPress.bind(this)}>
					<span className={selected ? "" : ""}>{this.props.label}</span>
					{selected ? <span>{selectedLabel}</span> : null}
				</div>
				{this.state.clicked ? (<div onKeyUp={this.onKeyNavigation.bind(this)}>
					{this.props.label ? <div>{this.props.title}</div> : null}
					<div>{options}</div>
					{this.props.tooltip ? <div>{this.props.tooltip}</div> : null}
				</div>) : null}
			</div>
		)
	}
}

export = ButtonMultiSelect