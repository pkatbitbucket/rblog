import React = require('react')
import ReactDOM = require('react-dom')
import Modal = require('./Modal')
import EmailInput = require('./EmailInput')
import MobileInput = require('./MobileInput')
import Button = require('../react-component/Button')
import Cookie = require('react-cookie')

interface IState {
	contactType?: string,
	mobileNo?: string,
	emailId?: string,
	isContacted?:boolean
}

interface IProps {
	title?:string;
	subTitle?:string;
	onContactMe?:Function;
	mobileNo?:string;
	emailId?: string;
	callMeAPI?: any;
	emailMeAPI?: any;
	onCall?:Function
}

const EMAIL_BUTTON_MSG: string = 'EMAIL ME'
const CALL_BUTTON_MSG: string = 'CALL ME'
const EMAIL_SUCCESS_MSG: string = 'We will contact you with the best quotes via email. Thank You'
const CALL_SUCCESS_MSG: string = 'Our expert will call you soon. Thank You.'
const TITLE: string = 'STILL UNABLE TO DECIDE?'
const SUB_TITLE: string = 'Why not ask for some expert advice from our specialist about your bike insurance needs? We will give you a call in less than 15 minutes.'

class ContactMeBox extends React.Component<IProps, IState>{
	refs: any
	static defaultProps = {
		title:'STILL UNABLE TO DECIDE?',
		subTitle: 'Why not ask for some expert advice from our specialist about your insurance needs? <>We will give you a call in less than 15 minutes.',
		mobileNo:'',
		emailId:''
	}
	static propTypes = {
		title:React.PropTypes.string,
		subTitle: React.PropTypes.string,
		onContactMe: React.PropTypes.func,
		mobileNo: React.PropTypes.string,
		emailId: React.PropTypes.string
	}

	constructor(props: IProps) {
		super(props)
		this.state = {
			contactType: 'CALL',
			mobileNo: props.mobileNo,
			emailId: props.emailId,
			isContacted: false
		}
	}

	componentWillReceiveProps(nextProps:IProps){
		var mobileNo:string = this.state.mobileNo
		var emailId:string = this.state.emailId

		if (nextProps.mobileNo){
			mobileNo = nextProps.mobileNo
		}
		if (nextProps.emailId) {
			emailId = nextProps.emailId
		}
		this.setState({
			mobileNo:mobileNo,
			emailId:emailId
		})
	}

	handleContactMe() {
		var isValid: boolean = false
		var isContacted: boolean = false
		switch (this.state.contactType) {
			case 'CALL':
				isValid = this.refs['MobileInput'].isValid(true)
				var mobileNo = this.refs['MobileInput'].getValue()
				if (isValid) {
					Cookie.save("mobileNo", mobileNo)
					if(this.props.onCall)
						this.props.onCall(mobileNo)

					isContacted = true
				}
				break
			case 'EMAIL':
				isValid = this.refs['EmailInput'].isValid(true)
				if (isValid) {
					//email API
					isContacted = true
				}
				break
		}

		if (isContacted) {
			this.setState({
				isContacted: isContacted
			},
				function() {
					if (this.props.onContactMe) {
						this.props.onContactMe()
					}
				}
			)
		}
	}

	render() {
		var buttonText:string = ""
		var successMsg:string = ""
		if (this.state.contactType == 'CALL') {
			buttonText = CALL_BUTTON_MSG
			successMsg = CALL_SUCCESS_MSG
		}
		else if (this.state.contactType == 'EMAIL') {
			buttonText = EMAIL_BUTTON_MSG
			successMsg = EMAIL_SUCCESS_MSG
		}

		return (
				<div className='contact-me-box'>
					<div className='contact-me-box--header'>
						{
							this.props.title ?
							<h3>{this.props.title}</h3>
							:null
						}
						{
							this.props.subTitle ?
							<p>{this.props.subTitle}</p>
							: null
						}
					</div>
					<div className='contact-me-box--widget'>
						<div className='contact-me-field'>
							{
							this.state.contactType == 'CALL' && this.state.isContacted == false ?
								<MobileInput ref='MobileInput' value={this.state.mobileNo} validations= {[{ name: 'required', message: 'Oh, we can’t proceed without your mobile number. Do your bit?' }, 
									{ name: 'mobile', message: 'Oh. Looks like that mobile number is not valid. Check again?' }]}
								placeholder="Enter your 10 digit mobile number" label="MOBILE NUMBER"/>
								: null
							}
							{
							this.state.contactType == 'EMAIL' && this.state.isContacted == false ?
							<EmailInput ref='EmailInput' value={this.state.emailId} validations= {['required', 'email']}
								placeholder="Enter your email id" label="EMAIL ID"/>
								: null
							}
						</div>
						
						{
						this.state.isContacted == false ?
							<Button title='Call Me' customClass="w--button--orange w--button--small contact-me-btn" onClick={this.handleContactMe.bind(this) }>{buttonText}</Button>
							: null
						}
						
						{
						this.state.isContacted == true?
							<div className='contact-me-success'>{successMsg}</div>
							:null
						}
					</div>
				</div>
		);
	}
}


export = ContactMeBox