/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')

interface IProps {
	value: number,
	currency?: string,
	formattingType?:string,
	label?:string,
	customClass?:string
}

class CurrencyFormatter extends React.Component<IProps, {}> {
	static defaultProps = {
		value : 0,
		currency: "Rupee",
		formattingType:"Indian",
		label:''
	}

	static propTypes = {
		value:React.PropTypes.number,
		currency:React.PropTypes.string,
		formattingType:React.PropTypes.string,
		label:React.PropTypes.string
	}

	constructor(props: IProps) {
		super(props)
	}

	formatMoney(value:number){
		var returnValue
		switch(this.props.formattingType){
			case "Standard":
				returnValue = value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
				break
			case "Indian":
				var valueWasNegative = false;
		           if (value < 0) {
		               value = value * -1;
		               valueWasNegative = true;
		           }
		           var x:any = value.toString();
		           var res = x;
		           if (x.length > 3) {
		               var afterPoint = '';
		               if (x.indexOf('.') > 0)
		                   afterPoint = x.substring(x.indexOf('.'), x.length);
		               x = Math.floor(x);
		               x = x.toString();
		               var lastThree = x.substring(x.length - 3);
		               var otherNumbers = x.substring(0, x.length - 3);
		               if (otherNumbers != '')
		                   lastThree = ',' + lastThree;
		               res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree
		           }
				          
	               var sign = valueWasNegative ? '-' : ''
	               returnValue = sign + " " + res
				break
			default:
				returnValue = value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
				break
		}
		return returnValue	
	}

	getCurrencySymbol(){
		var returnSymbol
		switch(this.props.currency){
			case "Rupee":
				returnSymbol = '/static/img/global/rupee-symbol-sprite.png'
				break
			default:
				returnSymbol = ''
				break
		}
		return returnSymbol	
	}
	
	render() {
		return (
			<div className={this.props.customClass}>
				<label>{this.props.label} </label>
				<span className="rupee_icon">Rs.</span>
				<span>{this.formatMoney(this.props.value)}</span>
			</div>
		)
	}

}

export = CurrencyFormatter