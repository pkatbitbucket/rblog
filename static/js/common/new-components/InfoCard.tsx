import React = require('react')
import ReactDOM = require('react-dom')

interface IState {
	isOpen?:boolean
}

interface IProps {
	imageUrl?:string,
	headerText?:string,
	messageText?:string,
	buttonText?:string,
	subButtonText?:string,
	onButtonClick?:Function,
	onSubButtonClick?:Function,
	headerClass?:string
}

class InfoCard extends React.Component<IProps, IState>{

	static defaultProps = {
		headerClass:''
	}
	static propTypes = {
		imageUrl: React.PropTypes.string,
		headerText: React.PropTypes.string,
		messageText: React.PropTypes.string,
		buttonText: React.PropTypes.string,
		subButtonText:React.PropTypes.string,
		onButtonClick:React.PropTypes.func,
		onSubButtonClick:React.PropTypes.func,
		headerClass:React.PropTypes.string
	}

	constructor(props: IProps) {
		super(props)
		this.state = {
			isOpen:false
		}
	}

	handleBtnClick(event:any){
		if (this.props.onButtonClick){
			this.props.onButtonClick()
		}
	}

	handleSubBtnClick(event:any){
		if (this.props.onSubButtonClick) {
			this.props.onSubButtonClick()
		}
	}

	render() {
		return (
			<div>
				{
				this.props.imageUrl ? 
				<img src={this.props.imageUrl}>
				</img>
				:null
				}
				<h5 className={this.props.headerClass}>{this.props.headerText}</h5>
				<p>{this.props.messageText}</p>
				{
					this.props.buttonText ? 
					<button onClick={this.handleBtnClick.bind(this)}>{this.props.buttonText}</button>
					: null
				}
				{
					this.props.subButtonText ?
					<a onClick={this.handleSubBtnClick.bind(this) }>{this.props.subButtonText}</a>
					:null
				}
			</div>
		);
	}
}


export = InfoCard