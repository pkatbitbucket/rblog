import React = require('react')

interface IProps{
	left?: any;
	right?: any;
	top?: any;
	bottom?: any;
	onClick?: any;
	disabled?: boolean;
	customClass?: any;
	key?: any;
	label?:string;
}

class LRCard extends React.Component<IProps, any> {
	static defaultProps = {
		disabled : false,
		label:''
	}
	render(){
		return (
			<div key={this.props.key ? this.props.key:''} onClick={this.props.disabled ? null : this.props.onClick} className={'w--lrcard ' + (this.props.customClass ? this.props.customClass : '') }>
				{this.props.top ? <div className='w--lrcard--top'>{this.props.top}</div> : null}
				<div className='w--lrcard--middle'>
					{this.props.left ? <div className='w--lrcard--left'><span className="label_minimised">{this.props.label}</span> {this.props.left}</div> : null}
					{this.props.right ? <div className='w--lrcard--right'>{this.props.right}</div> : null}
				</div>
				{this.props.bottom ? <div className='w--lrcard--bottom'>{this.props.bottom}</div> : null}
			</div>
		)
	}
}

export = LRCard