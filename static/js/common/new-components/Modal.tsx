import React = require('react')
import ReactDOM = require('react-dom')
import OutsideClickHandler = require('./Dependencies/OnClickHandler')

interface IState {
	isOpen?:boolean
}

interface IProps {
	isOpen?:boolean;
	customClass?:string;
	type?: string;
	size?:string;
	children?:any;
	onClose?:Function;
	isCloseOnOutSideClick?:boolean;
	isCloseOnEscape?:boolean;
	header?:JSX.Element | string;
	footer?: JSX.Element | string;
	customHeaderClass?:string;
	customFooterClass?: string;
	ref?: string;
	isShowCloseIcon?:boolean;
	customParentClass?: string;
	isPreventScrolling?: boolean;
}

class Modal extends React.Component<IProps, IState>{
	private clickOutsideHandler: OutsideClickHandler

	static defaultProps = {
		isOpen:false,
		customClass:'',
		size:'small',
		isCloseOnOutSideClick:true,
		isCloseOnEscape:true,
		customHeaderClass:'',
		customFooterClass:'',
		isShowCloseIcon:false,
		isPreventScrolling:true
	}
	static propTypes = {
		isOpen: React.PropTypes.bool,
		customClass: React.PropTypes.string,
		size: React.PropTypes.string,
		onClose:React.PropTypes.func,
		isCloseOnOutSideClick:React.PropTypes.bool,
		isCloseOnEscape:React.PropTypes.bool,
		header:React.PropTypes.any,
		footer: React.PropTypes.any,
		customHeaderClass:React.PropTypes.string,
		customFooterClass:React.PropTypes.string,
		isShowCloseIcon:React.PropTypes.bool,
		isPreventScrolling:React.PropTypes.bool
	}

	constructor(props: IProps){
		super(props)
		var isOpen = false
		if(props.isOpen != undefined){
			isOpen = props.isOpen
		}
		this.state = {
			isOpen: isOpen
		}
	}

	componentWillReceiveProps(nextProps:IProps){
		var isOpen = false
		if (nextProps.isOpen != undefined) {
			isOpen = nextProps.isOpen
		}
		this.setState({
			isOpen: isOpen
		})
	}

	componentDidMount() {
		if(this.props.isCloseOnOutSideClick)
	        this.clickOutsideHandler = new OutsideClickHandler(ReactDOM.findDOMNode(this.refs['Modal']), () => {
				this.handleClose(true)
	        })

	    if(this.props.isCloseOnEscape)
       		window.addEventListener('keydown',this.handleKeyPress.bind(this))

       	if(this.props.type !=='popup'){
       		TweenMax.from(this.refs['modalContent'], 1, { y: 200, opacity: 0, ease: Elastic.easeOut.config(1,0.5), force3D: true }, 0.2);
		}
		

       	if(this.props.isPreventScrolling){
   		 	var element = document.body
   		 	//if(this.state.isOpen || this.props.isOpen)
   			element.classList.add("modal-open")
       	}
    }

    componentWillUnmount() {
    	if(this.clickOutsideHandler)
        	this.clickOutsideHandler.dispose()

		window.removeEventListener('keydown', this.handleKeyPress.bind(this))

		var element = document.body
		element.classList.remove("modal-open")
    }

    componentWillUpdate(nextProps, nextState){
    	var element = document.body
    	if(nextProps.isOpen == true && this.props.isPreventScrolling){
    		element.classList.add("modal-open")
    	}
    	else {
    		element.classList.remove("modal-open")
    	}
    }

    handleKeyPress(event:any){
    	//escape key
    	if(this.refs['Modal'] && event.keyCode == 27 && this.props.isCloseOnEscape){
    		this.handleClose()
    	}
    }

    handleClose(isOutSideClick:boolean = false){
		if (this.refs['Modal'] && (!isOutSideClick || (isOutSideClick && this.props.isCloseOnOutSideClick))) {
			this.setState({
				isOpen: false
			},
			function() {
				if (this.props.onClose) {
					this.props.onClose()
				}
			})
		}
    }

	render() {
		return (
			<div ref='Modal'>
				<div ref='modalBg' className={"cf-modal " + (this.props.type == 'popup'? 'cf-modal--popup ':'') + (this.state.isOpen ? 'reveal ' : '') + (this.props.customParentClass ? this.props.customParentClass : '') }>
					<div ref='modalContent' className={"cf-modal__content " + (this.props.customClass ? this.props.customClass : '') + ' ' + (this.props.size) }>
						{this.props.isShowCloseIcon == true ? <div className='cf-modal__close' onClick={this.handleClose.bind(this, false) }></div> : null}
						{this.props.header ? <div className={this.props.customHeaderClass}>{this.props.header}</div> : null}
						{this.props.children}
						{this.props.footer ? <div className={this.props.customFooterClass}>{this.props.footer}</div> : null}
					</div>
				</div>
			</div>
		);
	}
}
	

export = Modal