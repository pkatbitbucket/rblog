import React = require('react')
import ReactDOM = require('react-dom')
import moment = require('moment')
import Modal = require('./Modal')

const MONTHS = moment.months()

interface IState {
	isModalOpen?:boolean,
	selectedDay?:number,
	selectedMonth?:number,
	selectedYear?:number,
	selectedDate?:any,
	isValid?:boolean,
	validationMsg?:string
}

interface IProps {
	minDate?:any,
	maxDate?:any
	selectedDate?:any
}

class DatePickerModal extends React.Component<IProps, IState>{

	//by default we allow dates from 01 Jan 1900 to 31 Dec (10 years + todays Year) 
	static defaultProps = {
		minDate:moment('1900-01-01',"YYYY-MM-DD", true),
		maxDate: moment().add('year',10).month(11).date(30),
		selectedDate:moment()
	}
	static propTypes = {
		minDate: React.PropTypes.any,
		maxDate: React.PropTypes.any,
		selectedDate:React.PropTypes.any
	}

	constructor(props: IProps) {
		super(props)
		this.state = {
			isModalOpen:false,
			selectedDay: moment(props.selectedDate).date(),
			selectedMonth: moment(props.selectedDate).month(),
			selectedYear: moment(props.selectedDate).year(),
			selectedDate:moment(props.selectedDate),
			isValid:true
		}
	}

	componentWillReceiveProps(nextProps: IProps) {
		var selectedDay:number = this.state.selectedDay 
		var selectedMonth:number = this.state.selectedMonth
		var selectedYear: number = this.state.selectedYear
		var selectedDate: any = this.state.selectedDate
		if (nextProps.selectedDate != undefined) {
			selectedDay = moment(nextProps.selectedDate).date()
			selectedMonth = moment(nextProps.selectedDate).month()
			selectedYear = moment(nextProps.selectedDate).year()
			selectedDate = moment(nextProps.selectedDate)
		}
		this.setState({
			selectedDay: selectedDay,
			selectedMonth: selectedMonth,
			selectedYear: selectedYear,
			selectedDate: selectedDate,
			isValid:true
		})
	}

	toggleModalDisplay(){
		var isModalOpen: boolean = this.state.isModalOpen
		this.setState({
			isModalOpen: !isModalOpen,
			selectedDay: moment(this.state.selectedDate).date(),
			selectedMonth: moment(this.state.selectedDate).month(),
			selectedYear: moment(this.state.selectedDate).year(),
			isValid: true,
			validationMsg:''
		})
	}

	selectDateSection(section:string, sectionValue: number) {
		switch(section){
			case 'day':
				this.setState({
					selectedDay: sectionValue,
					isValid: true,
					validationMsg:''
				})
			break
			case 'month':
				this.setState({
					selectedMonth: sectionValue,
					isValid: true,
					validationMsg: ''
				})
			break
			case 'year':
				this.setState({
					selectedYear: sectionValue,
					isValid: true,
					validationMsg: ''
				})
			break
		}
	}

	updateDateSection(section: string, changeValue:number) {
		switch (section) {
			case 'day':
				var day:number
				day = this.state.selectedDay + changeValue
				day = day < 1 ? 31 : day
				day = day > 31 ? 1 : day
				this.setState({
					selectedDay: day,
					isValid: true,
					validationMsg: ''
				})
				break
			case 'month':
				var month:number
				month = this.state.selectedMonth + changeValue
				month = month < 0 ? 11 : month
				month = month > 11 ? 0 : month
				this.setState({
					selectedMonth: month,
					isValid: true,
					validationMsg: ''
				})
				break
			case 'year':
			var year:number
			year = this.state.selectedYear + changeValue
			year = year > this.props.maxDate.year() ? this.props.maxDate.year() : year
			year = year < this.props.minDate.year() ? this.props.minDate.year() : year
				this.setState({
					selectedYear: year,
					isValid: true,
					validationMsg: ''
				})
				break
		}
	}

	formDays(){
		var currDay = this.state.selectedDay
		var displayDay:number
		var days:any = []
		var day: JSX.Element
		var dateClass:string

		var upArrow = <div onClick={this.updateDateSection.bind(this, 'day', +1) }>^</div>
		var downArrow = <div onClick={this.updateDateSection.bind(this, 'day', -1) }>v</div>
		days.push(upArrow)
		for (var i = -1; i <= 1; i++){
			displayDay = currDay + i
			displayDay = displayDay < 1 ? 31 : displayDay
			displayDay = displayDay > 31 ? 1 : displayDay
			dateClass = currDay == displayDay ? 'selected-date' : 'unselected-date'
			day = <div key={'Day_' + displayDay} onClick={this.selectDateSection.bind(this, 'day', displayDay) } className={dateClass}>{displayDay}</div>
			days.push(day)
		}
		days.push(downArrow)
		return days
	}

	formMonths() {
		var currMonth = this.state.selectedMonth
		var displayMonth:number
		var months: any = []
		var month: JSX.Element
		var dateClass :string

		var upArrow = <div onClick={this.updateDateSection.bind(this, 'month', +1)}>^</div>
		var downArrow = <div onClick={this.updateDateSection.bind(this, 'month', -1) }>v</div>
		months.push(upArrow)
		for (var i = -1; i <= 1; i++) {
			displayMonth = currMonth + i
			displayMonth = displayMonth < 0 ? 11 : displayMonth 
			displayMonth = displayMonth > 11 ? 0 : displayMonth
			dateClass = currMonth == displayMonth ? 'selected-date' : 'unselected-date'
			month = <div key={'Month_' + displayMonth} onClick={this.selectDateSection.bind(this, 'month', displayMonth) } className={dateClass}>{ MONTHS[displayMonth]}</div >
			months.push(month)
		}
		months.push(downArrow)
		return months
	}

	formYears() {
		var currYear = this.state.selectedYear
		var displayYear:number
		var years: any = []
		var year: JSX.Element
		var dateClass: string

		var upArrow = <div onClick={this.updateDateSection.bind(this, 'year', +1) }>^</div>
		var downArrow = <div onClick={this.updateDateSection.bind(this, 'year',-1) }>v</div>
		years.push(upArrow)
		for (var i = -1; i <= 1; i++) {
			displayYear = currYear + i
			dateClass = currYear == displayYear ? 'selected-date' : 'unselected-date'
			year = <div key={'Year_' + displayYear} onClick={this.selectDateSection.bind(this, 'year', displayYear) }  className={dateClass}>{displayYear}</div>
			years.push(year)
		}
		years.push(downArrow)
		return years
	}

	validate(dateString?:string) {
		var _dateString: string
		if(!dateString){
			var day = this.state.selectedDay
			var month = this.state.selectedMonth
			var year = this.state.selectedYear
			_dateString = year + "-" + ("0" + (month + 1)).slice(-2) + "-" + ("0" + day).slice(-2)
		}
		else {
			_dateString = dateString
		}
		
		var validationMsg: string
		var isValidDate: boolean = moment(_dateString, "YYYY-MM-DD", true).isValid()
		if (!isValidDate){
			validationMsg = "Invalid Date"
		}
		
		var isAfterMinDate:boolean = true
		var isBeforeMaxDate:boolean = true

		if (moment(_dateString, "YYYY-MM-DD", true).isBefore(this.props.minDate)) {
			isAfterMinDate = false 
			validationMsg = 'Dates before ' + moment(this.props.minDate).format('DD MMM YYYY') + ' are not allowed.'
		}
		if (moment(_dateString, "YYYY-MM-DD", true).isAfter(this.props.maxDate)) {
			isBeforeMaxDate = false
			validationMsg = 'Dates after ' + moment(this.props.maxDate).format('DD MMM YYYY') + ' are not allowed.'
		}
		return {
			isValid: isValidDate && isAfterMinDate && isBeforeMaxDate,
			validationMsg: validationMsg
		}
	}

	getDateString(){
		var day = this.state.selectedDay
		var month = this.state.selectedMonth
		var year = this.state.selectedYear
		var dateString: string = year + "-" + ("0" + (month + 1)).slice(-2) + "-" + ("0" + day).slice(-2)
		return dateString
	}

	setDate(){
		var dateString = this.getDateString()
		var validationObj = this.validate(dateString)
		if (validationObj.isValid) {
			this.setState({
				selectedDate: moment(dateString, "YYYY-MM-DD", true),
				isModalOpen: false,
				isValid: true,
				validationMsg:''
			})
		}
		else {
			this.setState({
				isValid: validationObj.isValid,
				validationMsg: validationObj.validationMsg
			})
		}
	}

	render() {
		var days = this.formDays()
		var months = this.formMonths()
		var years = this.formYears()
		return (
			<div>
				<span>{this.state.selectedDate.format("MMM DD,YYYY")}</span>
				<button onClick={this.toggleModalDisplay.bind(this)}>open</button>
				{
					this.state.isModalOpen == true ?
					<Modal header="Select a date" isOpen={this.state.isModalOpen} onClose={this.toggleModalDisplay.bind(this)} >
						<div> 
							<div className="date-div">
								{days}
							</div>
							<div className="date-div">
								{months}
							</div>
							<div className="date-div">
								{years}
							</div>
						</div>
						<button onClick={this.setDate.bind(this) }>Done</button>
						{
							this.state.isValid == false ?
							<div className="error-label">
								{this.state.validationMsg}
							</div>
							:null
						}
					</Modal>
					: null
				}
				
			</div>
		)
	}
}


export = DatePickerModal