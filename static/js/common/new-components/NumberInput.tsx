/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')
import Validations = require('./Validations')

interface IState {
	value?: string | number,
	isValid?: boolean,
	validationMsg?:string,
	isShowErrors?:boolean,
	isFocused?: boolean
}

interface IProps {
	value?: string | number,
	onChange?: Function,
	onValid?: string,
	label?: string,
	customClass?: string,
	placeholder?: string,
	isValidateOnBlur?: boolean,
	validations?:any,
	errorClass?:string,
	ref?:string,
	onBlur?:Function,
	maxLength?: number;
	description?: string;
}
interface IValidation {
	isValid: boolean,
	message: string
}

class NumberInput extends React.Component<IProps, IState> {
	static defaultProps = {
		value: '',
		label: '',
		customClass: '',
		placeholder: '',
		isValidateOnBlur: true,
		validations:[],
		errorClass:''
	}

	static propTypes = {
		onChange: React.PropTypes.func,
		onValid: React.PropTypes.func,
		label: React.PropTypes.string,
		customClass: React.PropTypes.string,
		placeholder: React.PropTypes.string,
		isValidateOnBlur: React.PropTypes.bool,
		validations:React.PropTypes.any,
		errorClass:React.PropTypes.string,
		onBlur:React.PropTypes.func,
		maxLength:React.PropTypes.number
	}

	constructor(props: IProps) {
		super(props)
		var value
		if (props.value) {
			value = props.value
		}
		this.state = {
			value: value,
			isValid: true,
			validationMsg: '',
			isShowErrors: false
		}
	}

	componentWillReceiveProps(nextProps: IProps) {
		this.setState({
			value: nextProps.value
		})
	}

	handleChange(event: any) {
		var value: any = event.target.value
		//replacing non numbers with blank
		value = value.replace(/\D/g,'')
		
		//logic to handle max length.. 
		if(this.props.maxLength && value.length > this.props.maxLength){
			event.target.value = value.substring(0, this.props.maxLength)
			return
		}

		var validationObj: IValidation		
		validationObj = Validations.validationHandler(value, this.props.validations)
		this.setState({
			value: value,
			isValid: validationObj['isValid'],
			validationMsg: validationObj['message'],
			isShowErrors:false
		},function(){
			if (this.props.onChange){
				this.props.onChange(value, validationObj)
			}
		})
	}
	handleFocus(event: any) {
		this.setState({
			isFocused: true
		})
	}

	handleBlur() {
		var isShowErrors: boolean = false
		if (this.props.isValidateOnBlur == true) {
			isShowErrors = true
		}

		var validationObj: IValidation
		validationObj = Validations.validationHandler(this.state.value, this.props.validations)
		this.setState({
			isValid: validationObj['isValid'],
			validationMsg: validationObj['message'],
			isShowErrors: isShowErrors,
			isFocused: false
		}, function() {
			if(this.props.onBlur){
				this.props.onBlur(this.state.value)
			}
			if (this.state.isValid && this.props.onValid) {
				this.props.onValid(this.state.value)
			}
		})
	}

	public showValidations(){
		this.setState({
			isShowErrors: true
		})
	}

	public isValid(isShowErrors:boolean = false){
		var validationObj: IValidation
		validationObj = Validations.validationHandler(this.state.value, this.props.validations)
		if (isShowErrors){
			this.setState({
				isShowErrors:true,
				isValid: validationObj['isValid'],
				validationMsg: validationObj['message']
			})
		}
		return validationObj.isValid
	}

	render() {
		var isShowFloatingLabel = (this.state.value != "" && this.state.value != undefined && this.state.value != null) || (this.state.isFocused)
		var labelClass = isShowFloatingLabel ? 'w--text_input--label-minimized' : 'w--text_input--label-full'
		var parentClass = this.state.isFocused ? 'is_focused' : ''
		var hasError = (this.state.isValid == false && this.state.isShowErrors == true)
		return (
			<div className={'w--text_input ' + parentClass + " " + this.props.customClass +
				" " + (hasError ? 'has-error' : '') }>
				<div className={'w--text_input--label ' + labelClass}>
					{isShowFloatingLabel ? (this.props.label ? this.props.label : this.props.placeholder) : this.props.placeholder }
					</div>
				<input type="text" value={this.state.value != undefined ? this.state.value + '' : ''} onChange={this.handleChange.bind(this) } onFocus={this.handleFocus.bind(this) } onBlur={this.handleBlur.bind(this) } />
				<label className="text-description">{this.props.description && this.state.isFocused ? this.props.description : null} </label>
				<hr />
				{
				hasError ?
					<div className={'error-label ' + (this.props.errorClass) }>{this.state.validationMsg}</div>
					: null
				}
			</div>
		)
	}

}

export = NumberInput