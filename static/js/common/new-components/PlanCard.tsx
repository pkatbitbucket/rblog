import React = require('react')
import LRCard = require('./LRCard')
import Spinner = require('../react-component/Spinner')
import Button = require('../react-component/Button')

interface IProps{
	insurerSlug: string;
	imgSrc: string;
	price?: number;
	idv?: number;
	error?: string;
	onBuy?: any;
	onRetry?: any;
	disabled?: boolean;
	key?: any;
}

interface IState{
	status: number; // -1:Error, 0:Loading Quotes, 1:Showing Quotes, 2:Buying
}

class PlanCard extends React.Component<IProps, IState> {
	constructor(props) {
		super(props)
		this.state = {
			status: this.getStatus(props)
		}
	}

	getStatus(props){
		let status
		
		if(this.state && this.state.status===2)
			status = 2
		else if(props.price>0)
			status = 1
		else if(props.error)
			status = -1
		else
			status = 0

		return status
	}

	componentWillReceiveProps(nextProps){
		this.setState({status: this.getStatus(nextProps)})
	}

	onBuy(){
		this.setState({status:2}, this.props.onBuy)
	}

	onRetry(){
		this.setState({status:0}, this.props.onRetry)
	}

	render(){
		let left = <div>
				<img src={this.props.imgSrc} alt={this.props.insurerSlug} style={{'width':'100px','height':'50px'}} /><br/>
				{this.state.status>0 ? <span>{"IDV Rs. "+this.props.idv}</span> : null}
			</div>

		let right
		switch(this.state.status){
			case -1:
				right = <div><span>{this.props.error}</span><br/><a onClick={this.onRetry.bind(this)}>TRY AGAIN</a></div>
				break
			case 0:
				right = <div><Spinner/></div>
				break
			case 1:
				right = <div><Button disabled={this.props.disabled} customClass="w--button--orange w--button--small" title={"Rs. "+this.props.price} onClick={this.onBuy.bind(this)}/></div>
				break
			case 2:
				right = <div><Button disabled={true} customClass="w--button--orange w--button--small centered" type="loading"/></div>
				break
		}

		return (
			<div style={{'width':'350px','height':'120px','padding':'10px'}}>
				<LRCard left={left} right={right}/>
			</div>
		)
	}
}

export = PlanCard