/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')
import Validations = require('./Validations')

import MultiSelect = require('../react-component/MultiSelect')
import LRCard = require('./LRCard')
import PopularRTO = require('../constants/PopularRTO')

interface IState {
	selectedRTO?: string,
	selectedCity?:string,
	isValid?: boolean,
	validationMsg?: string,
	isShowErrors?: boolean
}

interface IProps {
	selectedRTO?: string,
	rtoList?: any[],
	famousRTOList?:any[],
	onChange?: Function,
	title?: string,
	customClass?: string,
	placeholder?: string,
	validations?: any,
	errorClass?: string,
	ref?: string,
	onCityChange?:Function
}
interface IValidation {
	isValid: boolean,
	message: string
}

class RTOSelector extends React.Component<IProps, IState> {
	topCities:any = []

	static defaultProps = {
		title: 'Select your registration RTO',
		customClass: '',
		placeholder: 'Search for RTO...',
		validations: [],
		errorClass: ''
	}

	componentWillReceiveProps(nextProps: IProps) {
		var selectedRTO: string = this.state.selectedRTO
		if (nextProps.selectedRTO != undefined) {
			selectedRTO = nextProps.selectedRTO
		}
		this.setState({
			selectedRTO: selectedRTO
		})
	}

	constructor(props: IProps) {
		super(props)
		var selectedRTO: string
		if (props.selectedRTO != undefined) {
			selectedRTO = props.selectedRTO
		}
		this.state = {
			selectedRTO: selectedRTO,
			selectedCity: "",
			isValid: true,
			validationMsg: '',
			isShowErrors: false
		}
		this.topCities = this.formTopCities(PopularRTO)
	}

	formTopCities(topRTO:any){
		var topCities = []
		for(var city in topRTO){
			topCities.push(city)
		}
		return topCities
	}

	handleRTOChange(isCityPresent:boolean, option: any) {
		
		var selectedCity = this.state.selectedCity
		if(!isCityPresent)
			selectedCity = ""

		this.setState({
			selectedRTO:option.id,
			selectedCity:selectedCity
		},
		function(){
			if (this.props.onChange){
				this.props.onChange(option.id, isCityPresent)
			}
		})
	}

	handleCityChange(cityName:string){
		this.setState({
			selectedCity:cityName,
			selectedRTO:""
		},
		function(){
			if (this.props.onCityChange){
				this.props.onCityChange(cityName)
			}
		})
	}

	public getValue(){
		return this.state.selectedRTO
	}

	public showValidations() {
		this.setState({
			isShowErrors: true
		})
	}

	public isValid(isShowErrors: boolean = false) {
		var validationObj: IValidation
		validationObj = Validations.validationHandler(this.state.selectedRTO, this.props.validations)
		if (isShowErrors) {
			this.setState({
				isShowErrors: true,
				isValid: validationObj['isValid'],
				validationMsg: validationObj['message']
			})
		}
		return validationObj.isValid
	}

	public formFamousRTOList(city:string){
		if(!city)
			return

		var famousRTOList:any = []
		famousRTOList = PopularRTO[city]
		var famousRTOJSX:JSX.Element[] = []
		for(var i = 0; i < famousRTOList.length; i++){
			var isItemSelected = this.state.selectedRTO === famousRTOList[i]["id"]
			var rto = <div><h5>{famousRTOList[i]["id"]}</h5>
						<p>{famousRTOList[i]["name"]}</p></div>
			let rtoLRCardJSX:JSX.Element = <LRCard key={"FamousRTO_" + i} onClick={this.handleRTOChange.bind(this, true, famousRTOList[i]) } 
				customClass={'popular-item ' + (isItemSelected? 'selected':'')} left={rto} />
			famousRTOJSX.push(rtoLRCardJSX)
		}
		return famousRTOJSX
	}

	private formFamousCitesList(){
		var cityJSX:JSX.Element
		var famousCityJSX:JSX.Element[] = []
		for(var index in this.topCities){
			var isItemSelected = this.state.selectedCity === this.topCities[index]
			cityJSX = <div> <h5>{this.topCities[index]}</h5> </div>
			let cityLRCardJSX:JSX.Element = <LRCard key={"FamousCity_" + this.topCities[index]} 
				onClick={this.handleCityChange.bind(this, this.topCities[index]) } 
				customClass={'popular-item ' + (isItemSelected? 'selected':'')} left={cityJSX} />
			famousCityJSX.push(cityLRCardJSX)
		}
		return famousCityJSX
	}

	private handleBackToCities(){
		this.setState({
			selectedCity:"",
			selectedRTO:""
		})	
	}

	render() {
		var famousCities = this.formFamousCitesList()
		var famousRTO = this.formFamousRTOList(this.state.selectedCity)
		return (
			<div className='rto-selector'>
					<h5  className='modal_header modal_header--title'>Select your bike registration RTO</h5>
					{<MultiSelect dataSource={this.props.rtoList} values={this.state.selectedRTO} labelText="Selected RTO" placeHolder="Search for a RTO..."
											onChange={this.handleRTOChange.bind(this, false)}  optionsText='name' optionsValue='id'/>}
					<h5  className='modal_header modal_header--muted'>Select from the top RTOs below or search from the options above.</h5>
					{
						this.state.selectedCity && !this.state.selectedRTO ?
						<div>
							<h5  className="modal_header modal_header--nav">
								<a className="modal_back_arrow" onClick={this.handleBackToCities.bind(this)}>{'\u2190'}</a>
								<span>{"RTO for " + this.state.selectedCity}</span>
							</h5>
							<div className='popular-items popular-items--grid'>
								{famousRTO}
							</div>
						</div>
						: null
					}
					{
						!this.state.selectedCity || (this.state.selectedCity && this.state.selectedRTO) ?
						<div className='popular-items'>
							{famousCities}
						</div>
						: null
					}
					{
						this.state.isValid == false && this.state.isShowErrors == true ?
						<div className={'error-label ' + (this.props.errorClass)}>{this.state.validationMsg}</div>
						: null
					}
			</div>
		)
	}

}

export = RTOSelector