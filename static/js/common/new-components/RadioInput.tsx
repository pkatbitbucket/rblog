/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')
import Validations = require('./Validations')

interface IState {
	value?: any,
	isValid?: boolean,
	validationMsg?:string,
	isShowErrors?:boolean
}

interface IProps {
	value?: any,
	optionsList?:any,
	optionsKey?:string,
	optionsValue?:string,
	onChange?: Function,
	labelText?: string,
	customClass?: string,
	validations?:any,
	errorClass?:string,
	ref?:string
}
interface IValidation {
	isValid: boolean,
	message: string
}

class RadioInput extends React.Component<IProps, IState> {
	static defaultProps = {
		value: '',
		labelText: '',
		customClass: '',
		validations:[],
		errorClass:'',
		optionsKey:'key',
		optionsValue:'value'
	}

	static propTypes = {
		value: React.PropTypes.any,
		onChange: React.PropTypes.func,
		labelText: React.PropTypes.string,
		customClass: React.PropTypes.string,
		validations:React.PropTypes.any,
		errorClass:React.PropTypes.string
	}


	componentWillReceiveProps(nextProps: IProps) {
		var value:string = this.state.value
		if (nextProps.value != undefined) {
			value = nextProps.value
		}
		this.setState({
			value: value
		})
	}

	constructor(props: any) {
		super(props)
		var value: string
		value = undefined
		if (props.value != undefined) {
			value = props.value
		}
		this.state = {
			value: value,
			isValid: true,
			validationMsg: '',
			isShowErrors: false
		}
	}

	handleChange(value: any) {
		var validationObj: IValidation		
		validationObj = Validations.validationHandler(value, this.props.validations)
		this.setState({
			value: value,
			isValid: validationObj['isValid'],
			validationMsg: validationObj['message'],
			isShowErrors:false
		},function(){
			if (this.props.onChange){
				this.props.onChange(value, validationObj)
			}
		})
	}

	public showValidations(){
		this.setState({
			isShowErrors: true
		})
	}

	public isValid(isShowErrors:boolean = false){
		var validationObj: IValidation
		validationObj = Validations.validationHandler(this.state.value, this.props.validations)
		if (isShowErrors){
			this.setState({
				isShowErrors:true,
				isValid: validationObj['isValid'],
				validationMsg: validationObj['message']
			})
		}
		return validationObj.isValid
	}

	public getValue(){
		return this.state.value
	}

	formRadioOptions() :JSX.Element[]{
		var optionStyle = {}
		var options:JSX.Element[] = []
		for(var i = 0; i < this.props.optionsList.length; i++){
			let option:JSX.Element = 
				<div className={'w--radio__option ' + ((this.state.value === this.props.optionsList[i][this.props.optionsValue]) ? 'radio_selected' : '') } key={'RadioOption' + i} onClick={this.handleChange.bind(this, this.props.optionsList[i][this.props.optionsValue]) }>
					{this.props.optionsList[i][this.props.optionsKey]}
				</div>		
			options.push(option) 	
		}
		return options
	}

	render() {
		return (
			<div className={'w--radio ' + (this.props.customClass ? this.props.customClass:'')}>
				<div className='w--radio__label'>{this.props.labelText}</div>
				<div className='w--radio__options'>{this.formRadioOptions() }</div>		
				
				{
					this.state.isValid == false && this.state.isShowErrors == true ?
					<div className={'error-label ' + (this.props.errorClass)}>{this.state.validationMsg}</div>
					: null
				}
			</div>
		)
	}

}

export = RadioInput