/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')
import Validations = require('./Validations')
import MultiSelect = require('../react-component/MultiSelect')

interface IState {
	selectedYear?: number,
	isValid?: boolean,
	validationMsg?: string,
	isShowErrors?: boolean
}

interface IProps {
	selectedYear?: string,
	yearList?: number[],
	onChange?: Function,
	title?: string,
	customClass?: string,
	validations?: any,
	errorClass?: string,
	ref?: string
}
interface IValidation {
	isValid: boolean,
	message: string
}

class RegYearSelector extends React.Component<IProps, IState> {
	static defaultProps = {
		title: 'Select registration year',
		customClass: '',
		validations: [],
		errorClass: ''
	}

	componentWillReceiveProps(nextProps: IProps) {
		var selectedYear: number = this.state.selectedYear
		if (nextProps.selectedYear != undefined) {
			selectedYear = parseInt(nextProps.selectedYear)
		}
		this.setState({
			selectedYear: selectedYear
		})
	}

	constructor(props: IProps) {
		super(props)
		var selectedYear: number
		if (props.selectedYear != undefined) {
			selectedYear = parseInt(props.selectedYear)
		}
		this.state = {
			selectedYear: selectedYear,
			isValid: true,
			validationMsg: '',
			isShowErrors: false
		}
	}

	handleYearChange(isTopYear:boolean, option:any) {
		this.setState({
			selectedYear:option.value
		},
		function(){
			this.props.onChange(option.value.toString(), isTopYear)
		})
	}
	public getValue(){
		return this.state.selectedYear
	}

	public showValidations() {
		this.setState({
			isShowErrors: true
		})
	}

	public isValid(isShowErrors: boolean = false) {
		var validationObj: IValidation
		validationObj = Validations.validationHandler(this.state.selectedYear, this.props.validations)
		if (isShowErrors) {
			this.setState({
				isShowErrors: true,
				isValid: validationObj['isValid'],
				validationMsg: validationObj['message']
			})
		}
		return validationObj.isValid
	}

	render() {
		var allYears = this.props.yearList
		var topYears:any[] = []
		var otherYears:any[] = []
		allYears.sort(function(a:any, b:any){return b.value - a.value}) //sorting descending order
		topYears = allYears.slice(0,7)
		otherYears = allYears.slice(7,allYears.length)

		var topYearContainers: JSX.Element[] = []
		var topYearClass
		var isSelectedYearFound = false
		for(var i = 0; i < topYears.length; i++){
			if(topYears[i].value == this.state.selectedYear){
				isSelectedYearFound = true
				topYearClass = 'cf--pill cf--pill--small selected'
			}
			else{
				topYearClass = 'cf--pill cf--pill--small'
			}
			let container = <div className={topYearClass} key={"TopYears" + i} onClick={this.handleYearChange.bind(this,true, topYears[i])}>{topYears[i].key}</div>
			topYearContainers.push(container)
		} 

		//var isSelectedYearInOtherYears = otherYears.indexOf(this.state.selectedYear) > -1
		return (
			<div>
				{topYearContainers}
				<MultiSelect customClass={'modal_reg_year '+ (this.state.selectedYear && !isSelectedYearFound? 'selected':'')} dataSource={otherYears} 
					isSearchable={false} values={this.state.selectedYear} 
					 placeHolder="Other" onChange={this.handleYearChange.bind(this, false)}/>
				{
					this.state.isValid == false && this.state.isShowErrors == true ?
					<div className={'error-label ' + (this.props.errorClass)}>{this.state.validationMsg}</div>
					: null
				}
			</div>
		)
	}

}

export = RegYearSelector