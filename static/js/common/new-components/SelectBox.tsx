import React = require('react')
import Modal = require('./Modal')

interface IProps{
	options: any[];
	value?: string | number | boolean;
	highlightOptions?: any[];
	keyField?: string;
	valueField?: string;
	searchEnabled?: boolean;
	highlightLabel?: string;
	onChange?: any;
	key?: string;
}

interface IState{
	options?: any;
	highlights?: any;
	searchKey?: string;
	filteredOptions?: any;
	selectedValue?: string | number | boolean;
}

class SelectBox extends React.Component<IProps, IState> {
	static defaultProps = {
		keyField: 'key',
		valueField: 'value',
		searchEnabled: false,
		highlightLabel: ''
	}

	constructor(props){
		super(props)
		this.state = this.getUpdatedStates(props)
		this.state.searchKey = null
		this.state.filteredOptions = null
	}

	getUpdatedStates(props:IProps){
		let highlight = props.highlightOptions && props.highlightOptions.length
		return {
			options: props.options.slice(),
			highlights: highlight ? props.highlightOptions.slice() : undefined,
			selectedValue: props.value
		}
	}

	componentWillReceiveProps(nextProps:IProps){
		this.setState(this.getUpdatedStates(nextProps))
		if(this.state.searchKey)
			this.filter(this.state.searchKey)
	}

	filter(key:string):void{
		let fOptions = []
		key = key.toLowerCase()
		for(let index in this.state.highlights){
			let option = this.state.highlights[index]
			if (option[this.props.keyField].toLowerCase().indexOf(key) >= 0
				|| option[this.props.valueField].toLowerCase().indexOf(key) >= 0) {
				fOptions.push(option)
			}
		}
		for(let index in this.state.options){
			let option = this.state.options[index]
			if (option[this.props.keyField].toLowerCase().indexOf(key) >= 0
				|| option[this.props.valueField].toLowerCase().indexOf(key) >= 0) {
				fOptions.push(option)
			}
		}
		this.setState({filteredOptions:fOptions})
	}

	onSearchChange(e){
		this.setState({searchKey: e.target.value})
		this.filter(e.target.value)
	}

	handleSelect(value){
		this.setState({selectedValue: value})
		if(this.props.onChange)
			this.props.onChange(value)
	}

	render(){
		let search
		if(this.props.searchEnabled){
			search = <div>
						<input type="text" value={this.state.searchKey} onChange={this.onSearchChange.bind(this)} />
					</div>
		}

		let highlight
		if(this.props.searchEnabled && !this.state.searchKey
			&& this.state.highlights && this.state.highlights.length){
			let options = []
			for(let index in this.state.highlights){
				let option = this.state.highlights[index]
				options.push(<div key={option[this.props.valueField]} onClick={this.handleSelect.bind(this, option[this.props.valueField])}>{option[this.props.keyField]}</div>)
			}

			highlight = <div>
				<span>{this.props.highlightLabel}</span>
				{options}
			</div>
		}

		let listOptions
		if(!this.props.searchEnabled || (this.props.searchEnabled && this.state.searchKey)){
			let list = this.state.searchKey ? this.state.filteredOptions : this.state.options
			let options = []
			
			for(let index in list){
				let option = list[index]
				options.push(<div key={option[this.props.valueField]} onClick={this.handleSelect.bind(this, option[this.props.valueField])}>{option[this.props.keyField]}</div>)
			}

			listOptions = <div>
				{options.length ? options : <div>No Options Available</div>}
			</div>
		}

		return (
			<div>
				{search}
				{highlight}
				{listOptions}
			</div>
		)
	}
}

export = SelectBox