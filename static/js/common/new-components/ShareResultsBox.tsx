import React = require('react')
import ReactDOM = require('react-dom')
import Modal = require('./Modal')
import EmailInput = require('./EmailInput')
import MobileInput = require('./MobileInput')
import TextInput = require('./TextInput')
import TabSwitcher =require('./TabSwitcher')

interface IState {
	shareType?:string,
	mobileNo?:string,
	emailId?:string,
	sharedStates?:any
}

interface IProps {
	shareUrl:string,
	onShare?:Function,
	mobileNo?:string,
	emailId?:string,
	onClose?:Function
}
const SHARE_TYPES =['SMS', 'EMAIL', 'URL']
const SMS_SUB_MSG: string = 'You will receive an sms with a link to this page'
const EMAIL_SUB_MSG: string = 'You will receive an email with a link to this page'
const URL_SUB_MSG: string = ''
const SMS_SUCCESS_MSG: string = 'A sms with the link has been sent to your mobile number'
const EMAIL_SUCESS_MSG: string = 'An email with the link has been sent to your email id'
const URL_SUCCESS_MSG: string = 'Link copied to clipboard'
const SMS_BUTTON_MSG:string = 'SHARE'
const EMAIL_BUTTON_MSG:string = 'SHARE'
const URL_BUTTON_MSG:string = 'COPY'

class ShareBox extends React.Component<IProps, IState>{
	refs:any


	static defaultProps = {
		mobileNo:'',
		emailId:''
	}
	static propTypes = {
		shareUrl:React.PropTypes.string,
		onShare:React.PropTypes.func,
		mobileNo:React.PropTypes.string,
		emailId:React.PropTypes.string,
		onClose:React.PropTypes.func
	}

	constructor(props: IProps) {
		super(props)
		this.state = {
			shareType: 'SMS',
			mobileNo: props.mobileNo,
			emailId: props.emailId,
			sharedStates:[]
		}
		this.props.onShare(URL ,'')
	}

	componentWillReceiveProps(nextProps: IProps) {
		var mobileNo: string = this.state.mobileNo
		var emailId: string = this.state.emailId

		if (nextProps.mobileNo) {
			mobileNo = nextProps.mobileNo
		}
		if (nextProps.emailId) {
			emailId = nextProps.emailId
		}
		this.setState({
			mobileNo: mobileNo,
			emailId: emailId
		})
	}

	handleTypeChange(type:string){
		this.setState({
			shareType:type
		})
	}

	handleShare(){
		var sharedStates:any = this.state.sharedStates
		var isValid:boolean = false
		var isSuccessfulShare:boolean = false
		var value = ''
			switch (this.state.shareType) {
				case 'SMS':
					var mobileInput =  this.refs['MobileInput']
					isValid = mobileInput.isValid(true)
					if (isValid && sharedStates.indexOf(this.state.shareType) == -1) {
						value = mobileInput.getValue()
					//call sms API
						sharedStates.push(this.state.shareType)
						isSuccessfulShare = true
					}
					break
				case 'EMAIL':
					var emailInput = this.refs['EmailInput']
					isValid = emailInput.isValid(true)
					if (isValid && sharedStates.indexOf(this.state.shareType) == -1) {
						value = emailInput.getValue()
					//call email API
						sharedStates.push(this.state.shareType)
						isSuccessfulShare = true
					}
					break
				case 'URL':
					isValid = true
					if (isValid && sharedStates.indexOf(this.state.shareType) == -1) {
					//copy to clipboard code
						this.copyToClipBoard()
						sharedStates.push(this.state.shareType)
						isSuccessfulShare = true
					}
					break
			}

			if (isSuccessfulShare) {
				this.setState({
					sharedStates: sharedStates
				},
					function() {
						if (this.props.onShare) {
							this.props.onShare(this.state.shareType , value)
						}
					}
				)
			}
	}

	copyToClipBoard(){
		var copyTextarea:any = document.querySelector('.share-short-url input')
		copyTextarea.select()
		var successful = document.execCommand('copy')
	}

	handleClose(){
		if(this.props.onClose){
			this.props.onClose()
		}
	}

	render() {
		var subMsg:string = ""
		var buttonText:string = ""
		var successMsg:string = ""
		if(this.state.shareType == 'SMS'){
			subMsg = SMS_SUB_MSG
			buttonText = SMS_BUTTON_MSG
			successMsg = SMS_SUCCESS_MSG
		}
		else if (this.state.shareType == 'EMAIL')
		{
			subMsg = EMAIL_SUB_MSG
			buttonText =EMAIL_BUTTON_MSG
			successMsg = EMAIL_SUCESS_MSG
		}
		else if (this.state.shareType == 'URL') {
			subMsg = URL_SUB_MSG
			buttonText = URL_BUTTON_MSG
			successMsg = URL_SUCCESS_MSG
		}

		return (

			<div className="share_modal_wrapper">
			<Modal isOpen={true} onClose={this.handleClose.bind(this)} isShowCloseIcon={true} isCloseOnOutSideClick={true} isCloseOnEscape={true}>
				<div>
					SHARE RESULTS
					<TabSwitcher onTabChange={this.handleTypeChange.bind(this) } tabList={SHARE_TYPES} />
					{
					this.state.shareType == 'SMS' && this.state.sharedStates.indexOf('SMS') == -1 ?
					<MobileInput ref='MobileInput' validations= {[{ name: 'required', message: 'Oh, we can’t proceed without your mobile number. Do your bit?' },
						{ name: 'mobile', message: 'Oh. Looks like that mobile number is not valid. Check again?' }]} placeholder="Mobile Number" label="MOBILE NUMBER"/>
					:null
					}
					{
					this.state.shareType == 'EMAIL' && this.state.sharedStates.indexOf('EMAIL') == -1 ?
					<EmailInput ref='EmailInput'  validations= {[{ name: 'required', message: 'Oh, we can’t proceed without your email address. Do your bit?' },
						{ name: 'email', message: 'Oh. Looks like that email address is not valid. Check again?' }]}  placeholder="Email Address" label="EMAIL ADDRESS" />
					:null
					}
					{
					this.state.shareType == 'URL' ?
						<TextInput customClass='share-short-url' label='Permanent Link' value={this.props.shareUrl}  />
						: null
					}
					{
					this.state.sharedStates.indexOf(this.state.shareType) == -1 ?
					<button className="w--button w--button--empty w--button--thin" onClick={this.handleShare.bind(this) }>{buttonText}</button>
					:null
					}
					{
					this.state.sharedStates.indexOf(this.state.shareType) == -1 ?
					<div className="sub_msg">{subMsg}</div>
					:
					<div>{successMsg}</div>
					}

				</div>
			</Modal>
			</div>
		);
	}
}


export = ShareBox