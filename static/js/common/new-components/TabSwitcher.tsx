import React = require('react')
import ReactDOM = require('react-dom')

interface IState {
	selectedTab?:string
}

interface IProps {
	tabList:any,
	onTabChange:Function,
	customTabClass?:string,
	customSelectedTabClass?:string,
	selectedTab?:string
}

class TabSwitcher extends React.Component<IProps, IState>{

	static defaultProps = {
		customTabClass:'',
		customSelectedTabClass:''
	}
	static propTypes = {
		tabList: React.PropTypes.array,
		onTabChange: React.PropTypes.func,
		customTabClass: React.PropTypes.string,
		customSelectedTabClass:React.PropTypes.string,
		selectedTab:React.PropTypes.string
	}

	constructor(props: IProps) {
		super(props)
		var selectedTab = this.props.tabList[0]
		if (props.selectedTab){
			selectedTab = props.selectedTab
		}
		this.state = {
			selectedTab: selectedTab
		}
	}

	componentWillReceiveProps(nextProps: IProps) {
		var selectedTab = this.state.selectedTab
		if (nextProps.selectedTab) {
			selectedTab = nextProps.selectedTab
		}
		this.setState({
			selectedTab:selectedTab
		})
	}

	handleTabChange(tabName:string){
		this.setState({
			selectedTab:tabName
		},
		 function(){
			 if (this.props.onTabChange) {
				 this.props.onTabChange(tabName)
		 	}
		 }
		)
	}

	formTabs(){
		var tabs:any = []
		var tab:JSX.Element

		for (var i = 0; i < this.props.tabList.length; i++){
			tab = <span key={'tab_' + this.props.tabList[i]}
				className={'tab ' + this.props.customTabClass + (this.state.selectedTab == this.props.tabList[i] ? (' selected-tab ' + this.props.customSelectedTabClass) : '') }
				onClick={this.handleTabChange.bind(this, this.props.tabList[i]) }>{this.props.tabList[i]}</span>
			tabs.push(tab)
		}
		return tabs
	}

	render() {
		var tabs = this.formTabs()
		return (
			<div className="share_tabs">
				{tabs}
			</div>
		)
	}
}


export = TabSwitcher