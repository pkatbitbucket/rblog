/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')
import Validations = require('./Validations')

interface IState {
	value?: string,
	isValid?: boolean,
	validationMsg?:string,
	isShowErrors?:boolean,
	isFocused?: boolean
}

interface IProps {
	value?: string,
	onChange?: Function,
	onValid?: string,
	onBlur?:Function,
	label?: string,
	customClass?: string,
	placeholder?: string,
	isValidateOnBlur?: boolean,
	validations?:any,
	errorClass?:string,
	ref?:string,
	maxLength?:number,
	disabled?:boolean,
	onKeyUp?:Function,
	description?: string;
}
interface IValidation {
	isValid: boolean,
	message: string
}

class TextInput extends React.Component<IProps, IState> {
	static defaultProps = {
		value: '',
		label: '',
		customClass: '',
		placeholder: 'Enter value',
		isValidateOnBlur: true,
		validations: [],
		errorClass:'',
		disabled:false
	}

	static propTypes = {
		value: React.PropTypes.string,
		onChange: React.PropTypes.func,
		onValid: React.PropTypes.func,
		onBlur:React.PropTypes.func,
		label: React.PropTypes.string,
		customClass: React.PropTypes.string,
		placeholder: React.PropTypes.string,
		isValidateOnBlur: React.PropTypes.bool,
		validations:React.PropTypes.any,
		errorClass:React.PropTypes.string,
		disabled:React.PropTypes.bool,
		onKeyUp:React.PropTypes.func
	}


	componentWillReceiveProps(nextProps: IProps) {
		this.setState({
			value: nextProps.value
		})
	}

	constructor(props: any) {
		super(props)
		this.state = {
			value: props.value,
			isValid: true,
			validationMsg: '',
			isShowErrors: false
		}
	}

	handleChange(event: any) {
		var value: string = event.target.value
		//logic to handle max length.. 
		if(this.props.maxLength && value.length > this.props.maxLength){
			event.target.value = value.substring(0, this.props.maxLength)
			return
		}

		var validationObj: IValidation		
		validationObj = Validations.validationHandler(value, this.props.validations)
		this.setState({
			value: value,
			isValid: validationObj['isValid'],
			validationMsg: validationObj['message'],
			isShowErrors:false
		},function(){
			if (this.props.onChange){
				this.props.onChange(value, validationObj)
			}
		})
	}

	handleFocus(event:any) {
		this.setState({
			isFocused: true
		})
	}

	handleBlur(event:any) {
		var isShowErrors: boolean = false
		if (this.props.isValidateOnBlur == true) {
			isShowErrors = true
		}

		var validationObj: IValidation
		validationObj = Validations.validationHandler(this.state.value, this.props.validations)
		this.setState({
			isValid: validationObj['isValid'],
			validationMsg: validationObj['message'],
			isShowErrors: isShowErrors,
			isFocused: false
		}, function() {
			if(this.props.onBlur){
				this.props.onBlur(this.state.value)
			}
			if (this.state.isValid && this.props.onValid) {
				this.props.onValid(this.state.value)
			}
		})
	}
	handleKeyPress(event:any){
		if(this.props.onKeyUp){
			this.props.onKeyUp(event)
		}
	}

	public showValidations(){
		this.setState({
			isShowErrors: true
		})
	}

	public isValid(isShowErrors:boolean = false){
		var validationObj: IValidation
		validationObj = Validations.validationHandler(this.state.value, this.props.validations)
		if (isShowErrors){
			this.setState({
				isShowErrors:true,
				isValid: validationObj['isValid'],
				validationMsg: validationObj['message']
			})
		}
		return validationObj.isValid
	}
	render() {
		var isShowFloatingLabel = (this.state.value != "" && this.state.value != undefined && this.state.value != null) || (this.state.isFocused)
		var labelClass = isShowFloatingLabel ? 'w--text_input--label-minimized':'w--text_input--label-full'
		var parentClass = this.state.isFocused? 'is_focused':''
		var hasError = (this.state.isValid == false && this.state.isShowErrors == true)
		return (
			<div className={'w--text_input ' + parentClass + " " + this.props.customClass +
				" " + (hasError ? 'has-error':'')}>
				<div className={'w--text_input--label '+ labelClass}>
					{isShowFloatingLabel ? (this.props.label ? this.props.label : this.props.placeholder) : this.props.placeholder }
				</div>
				<input ref='inputField' type="text" value={this.state.value} onChange={this.handleChange.bind(this) } 
					onFocus={this.handleFocus.bind(this)} onBlur={this.handleBlur.bind(this) }
					onKeyUp={this.handleKeyPress.bind(this) } disabled={this.props.disabled ? true : false} 
					spellCheck={false}/>
				{this.props.description?
				<label className={"text-description " + (this.state.isFocused ? 'reveal':'')}>{this.props.description}</label>:null}
				<hr />
				{
					hasError ?
					<div className={'error-label ' + (this.props.errorClass)}>{this.state.validationMsg}</div>
					: null
				}
			</div>
		)
	}

}

export = TextInput