/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')
import validation = require('./utils/Validation')
var ValidationObj = new validation()

interface IValidationResult{
	valid: boolean;
	errorMessage: string;
}

class baseInput extends React.Component <any, any> {

	static defaultProps = {
		type:'text',
		defaultValue: '',
		placeholder:'',
		// defaultClass: 'cf-ip-text',
		validations: [],
		maxLength: 524288
	}

	constructor (props) {
		super(props)

		var validationArray = this.props.validations;
		var fieldName = (this.props.fieldName)? this.props.fieldName : '';
		var val = this.props.defaultValue;
		var validationResult: IValidationResult = ValidationObj.isValid(validationArray, { 'value':val, 'fieldName':fieldName });

		this.state = {
			'inputValue': val,
			'isValid': validationResult.valid,
			'errorMessage':validationResult.errorMessage,
			'showErrors':props.showErrors || false,
		}
	}

	componentWillReceiveProps(nextProps){
		this.setState({
			showErrors: nextProps.showErrors,
			inputValue: nextProps.defaultValue,
		})
	}

	isValid(){
		return this.state.isValid;
	}

	getValue(){
		return this.state.inputValue;
	}	

	checkValid(val){

		var validationArray = this.props.validations;
		var fieldName = (this.props.fieldName)? this.props.fieldName : '';

		return ValidationObj.isValid(validationArray, { 'value':val, 'fieldName':fieldName });
		
	}

	showErrors(){
		var showErrors = (!this.isValid())? true : false;

		this.setState({
			showErrors: showErrors,
		})
	}

	handlechange(evt:any){
		var val = evt.target.value;

		//logic to handle max length.. 
		if(this.props.maxLength && val.length > parseInt(this.props.maxLength)){
			evt.target.value = val.substring(0, this.props.maxLength)
			return
		}
		var validationResult = this.checkValid(val);
		
		this.setState({
				'inputValue' : val,
				'isValid' : validationResult.valid,
				'errorMessage':validationResult.errorMessage,
				'showErrors':false,
		}, function(){
			
			// Prop function for API request or else
			this.props.onChange(evt);
		});

	}

	handleBlur(evt:any){
		if(this.props.onBlur){
			this.props.onBlur(evt)
		}
	}

	render(){
		var self = this;
		var errorMessage = (this.props.errorMessage)?this.props.errorMessage:self.state.errorMessage;
		return (
			<div className="input_wrapper clearfix">
				{
				this.props.label != undefined && this.props.label!="" && this.props.label!=null?
				<label> {this.props.label} </label>
				: null
				}
				<input ref='baseInput' onChange={ self.handlechange.bind(self) } onBlur={self.handleBlur.bind(self) } value={ this.state.inputValue } maxLength={this.props.maxLength} type={ this.props.type } placeholder={ this.props.placeholder } min={this.props.min} max={this.props.max}/>
				{ self.state.showErrors? <div className='w--error' >{ errorMessage }</div>:null }
			</div>
		);
	}
}

export = baseInput




