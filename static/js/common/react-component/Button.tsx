/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')
import Spinner = require('./Spinner')


interface IProps {
	title?: any;
	type?: string;
	customClass?: string;
	onClick?: Function;
	disabled?: boolean;
	member?: string;
	key?: string;
	customValueAttr?:any;
}

class Button extends React.Component<IProps, any> {

	static defaultProps = {
		title: 'Submit'
	}

	static propTypes = {
		title: React.PropTypes.any,
		customClass: React.PropTypes.string,
		type: React.PropTypes.string,
		onClick: React.PropTypes.func
	}

	constructor(props) {
		super(props)
	}

	handleClick(evt) {
		var customAttr = (this.props.customValueAttr!=undefined)?this.props.customValueAttr:undefined
		if(this.props.onClick){
			this.props.onClick(evt,customAttr);
		}
	}

	render() {
		var title = this.props.title,
			type = this.props.type,
			customClass = this.props.customClass+ ' ' + (type? type: '')
			var tooltip = typeof(title) == 'string' ? title.toString() : '' 
		return (
			<button disabled={this.props.disabled} title={tooltip} onClick={this.handleClick.bind(this)} className={'w--button ' + (customClass ? customClass : '') }>
				{type == 'loading' ? <Spinner /> : title}
			</button>
		);
	}

}


export = Button