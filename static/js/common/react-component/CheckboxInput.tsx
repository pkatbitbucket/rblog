/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')

class Checkbox extends React.Component<any, any>{

	static defaultProps = {
		name:'ipCheckbox',
		defaultValue : '',
		label: '',
		className: 'checkboxComponent',
		validations: ['required']
	}

	static propTypes = {
		name: React.PropTypes.string,
		defaultValue: React.PropTypes.any,
		label: React.PropTypes.any,
		validations:React.PropTypes.any
	}

	constructor (props) {
		super(props)
		
		this.state = {
			'checked': (this.props.checked === true)? true : false,
		};
	}


	componentWillReceiveProps(newprops){
		
		this.setState({
			'checked':(newprops.checked === true)? true : false,
		})
	}

	isSelected(){
		return (this.state.checked)? true : false;
	}

	isChecked(){
		return (this.state.checked)? true : false;
	}


	handleChange(evt){
		var checked = this.state.checked;
		
		this.setState({
			'checked': (!checked)? true : false,
		}, function() {
			if(this.props.onChange)
				this.props.onChange(this.state.checked,this.props.defaultValue)
		});
	}

	render(){

		var self = this;
		
		return (
			<div>
				<input type='checkbox' name='ipCheckbox' checked={ self.state.checked } onClick={ self.handleChange.bind(self) } className={ this.props.checkboxComponent } disabled={ this.props.disabled }  />
				{ self.props.label }
			</div>
		);
	}

}

export = Checkbox