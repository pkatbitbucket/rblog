import React = require('react');
import RadioControl = require('./RadioControl')
import isEqual = require('lodash.isequal')

class ControlGroup extends React.Component<any,any>{
	constructor(props:any){
		super(props)
		this.state = {
			selectedRadio: props.selectedValue
		}
	}

	static defaultProps = {
		controlType: 'radio',
		layout: 'vertical'
	}

	handleRadioChange(item:any) {
		this.setState({
			selectedRadio: item.value
		})
		this.props.onUpdate(item)
	}

	render(){
		var layout = this.props.layout
		var controlType = this.props.controlType
		var classString = "controlgroup " + layout + " " + controlType + " " + this.props.customClass
		var propitems = this.props.items
		var selectedRadio = this.state.selectedRadio

		var items



		for (var i = 0; i < propitems.length; i++) {
			if (controlType == 'radio') {
				items = propitems.map(function(item, i) {
					return <RadioControl key={i} item={item} change={this.handleRadioChange.bind(this)} selected={item.value == selectedRadio} />
				}, this)
			}
			else {

			}
		};


		return (
			<div className={classString}>
				{items}
				</div>
		)
	}
}

export = ControlGroup