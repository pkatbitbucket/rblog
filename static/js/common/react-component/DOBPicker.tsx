/// <reference path="../../typings/tsd.d.ts" />


//issues - on pressing tab in first box control goes to 3rd box... and not to the second drop down
import React = require('react')
import ReactDOM = require('react-dom')
import listensToClickOutside = require('./Dependencies/OnClickHandler')
import helpers = require('./utils/HelperFunctions')
import moment = require('moment')
import RichMultiSelect = require('./MultiSelect')

var months = []
var month = {}

moment.months().map(function(data: any, i: number) {
	month = {
		'key': data,
		'value': i
	}
	months.push(month)
})

class Input extends React.Component<any,any>{
	constructor(props: any){
		super(props)
		var isVisited = false
		if (props.value != ""){
			// if default value is not blank means the box has been visited by user in the past
			isVisited = true 
		}
		this.state = {
			value:props.value,
			isVisited: isVisited
			//isValid:true
		}
	}

	componentWillReceiveProps(nextProps:any){
		var isVisited = this.state.isVisited
		// if default value is not blank means the box has been visited by user in the past
		if (nextProps.value != "") {
			isVisited = true
		}
		this.setState({
			value: nextProps.value,
			isVisited: isVisited
		})
	}

	handleChange(event:any){
		event.target.value = event.target.value.replace(/ /g, "") // spaces not allowed
		this.setState({
			value:event.target.value
		},
		function(){
			this.props.onBlur(true)
			if (this.props.onChange) {
				this.props.onChange()
			}
		})

		if (event.target.value.length == this.props.maxLength && 
			event.target.nextElementSibling!=null) {
				event.target.nextElementSibling.focus()
		}
		
	}

	handleBlur(event: any) {
		this.props.onBlur()
	}

	getState(){
		return this.state
	}

	getValue(){
		return this.state.value
	}

	handleFocus(){
	if(!this.state.isVisited)
		this.setState({
			isVisited:true
		})
	}

	render(){
		var inputStyle = {
			border: this.state.isValid == false ? '2px solid red' : '',
			}
			
		return(
			<input style={inputStyle}
				type="text" 
				value={this.state.value}
				maxLength={this.props.maxLength}
				placeholder={this.props.placeHolder}
				onChange={this.handleChange.bind(this)}
				onBlur={this.handleBlur.bind(this)}
				onFocus={this.handleFocus.bind(this)}
				/>
			)
	}
}

class DOBPicker extends React.Component<any,any> {
	refs:any
	static defaultProps ={
		validationMessage: "Invalid Date"

	}

	static propTypes = {
		validationMessage: React.PropTypes.string,
		onChange:React.PropTypes.any,
		value:React.PropTypes.any
	}

	constructor(props: any){
		super(props)
		this.state = {
			isValid:true
		}
	}

	getDateString(){
		var year: string = this.refs['yearInput'].getState().value
		var day: string = this.refs['dayInput'].getState().value
		var month: string = ""
		if (this.refs['monthInput'] && this.refs['monthInput'].getValues() != undefined) {
			// returns an array.. in our case since its not a multiselect we need only 0th value
			month = this.refs['monthInput'].getValues()
		}
		else {
			month = "-1"
		}

		var dateString: string
		dateString = year + '-' + ("0" + (parseInt(month) + 1)).slice(-2) + '-' + ("0" + day).slice(-2) 
		return dateString
	}

	getDate(){
		return moment(this.getDateString())
	}

	setValidity(isSetOnlyValid: boolean = false) {
		//case when validity state is to be changed to valid if the previous state was inValid but not 
		//the opposite case. THis is used onChange event of childs.. if the previous state of the child was 
		//invalid but now onChange it has become valid then change state. However if the previous state was 
		//Valid and now the state has changed to InValid do not change the state since the user may have not
		//entered the complete value yet... he is still in process of changing.. so wait for complete input
		// and onBlur event set InValid state if the value entered is Invalid
		if (!isSetOnlyValid || (isSetOnlyValid && this.state.isValid == false)) {
			if (this.refs['dayInput'].getState().isVisited && this.refs['yearInput'].getState().isVisited) {
				var isValid = this.validate()
				this.setState({
					isValid: isValid
				})
			}
		}
	}

	handleMonthChange(){
		this.setValidity()
		var yearNode : any  = ReactDOM.findDOMNode(this.refs['yearInput'])
		yearNode.focus()
		
		var isValid = false
		if (this.refs['dayInput'].getState().isVisited && this.refs['yearInput'].getState().isVisited) {
			var isValid = this.validate()
		}
		if (this.props.onChange) {
			this.props.onChange(this.getDate(), isValid)
		}
	}

	handleDateChange(){
		var isValid = false
		if (this.refs['dayInput'].getState().isVisited && this.refs['yearInput'].getState().isVisited) {
			var isValid = this.validate()
		}
		if (this.props.onChange) {
			this.props.onChange(this.getDate(), isValid)
		}
	}

	handleYearChange(){
		var isValid = false
		if (this.refs['dayInput'].getState().isVisited && this.refs['yearInput'].getState().isVisited) {
			var isValid = this.validate()
		}
		if (this.props.onChange) {
			this.props.onChange(this.getDate(), isValid)
		}
	}

	validate() {
		var selectedDate = this.getDateString()
		return moment(selectedDate, "YYYY-MM-DD", true).isValid()

	}

	validateYear(year: string){
		var isValid
		var dateRegExp = /^[0-9]{4}$/
		isValid = dateRegExp.test(year)
		return isValid
	}

	render(){
		
		var errorDivStyle = {
			visibility: this.state.isValid== false ? 'visible' : 'hidden',
								border:'2px solid red'
								}

		var date =""
		var month = -1
		var year = ""	
		var value
		if (this.props.value != undefined) {
			value = moment(this.props.value)
			date = value.date()
			month = value.month()
			year = value.year()
		}
		else {
			if (this.refs['dayInput']){
				date = this.refs['dayInput'].getValue()
			}
			if (this.refs['monthInput']) {
				month = this.refs['monthInput'].getValues()
			}
			if (this.refs['yearInput']) {
				year = this.refs['yearInput'].getValue()
			}
		}	

		return (
			<div ref='DOBPicker'>
				<Input ref='dayInput'
					maxLength={2}
					placeHolder={'date'}
					onBlur={this.setValidity.bind(this)}
					value={date}
					onChange={this.handleDateChange.bind(this) }
					/>
				<RichMultiSelect ref='monthInput'
					dataSource={months}
					placeHolder={'Select birth month...'}
					searchKeys={['key', 'value']}
					onChange={this.handleMonthChange.bind(this)}
					values={month}
					/>
				<Input ref='yearInput'
					maxLength={4}
					placeHolder={'year'}
					onBlur={this.setValidity.bind(this) }
					value={year}
					onChange={this.handleYearChange.bind(this) }
					/>
				<br />
				<br />
				<div style={errorDivStyle}>
					{this.props.validationMessage}
				</div>
			</div>
			)
	}
}

export = DOBPicker