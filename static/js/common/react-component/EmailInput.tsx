/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')
import BaseInput = require('./BaseInput')
 
class EmailInput extends React.Component<any, any> {	
	refs:any;

	static defaultProps = {
		defaultValue: '',
		placeholder:'email',
		fieldName: 'Email',
		validations: ['required','email'],
	}

	static propTypes = {
		defaultValue: React.PropTypes.any,
		validations: React.PropTypes.any,
	}

	isValid(){
		return this.refs.email.isValid();
	}

	getValue(){
		return this.refs.email.getValue();
	}

	showErrors(){
		this.refs.email.showErrors()
	}

	handleChange(evt){
		/*var isValid = this.refs.email.state.isValid;
		var value = evt.target.value;

		if(isValid){
			console.log('fire an API');
			// Make an API Call	
		}*/

		if(this.props.onChange)
			this.props.onChange(evt);

	}

	render(){

		var self = this;
		
		return (
			<BaseInput onChange={ self.handleChange.bind(self) } placeholder={ self.props.placeholder } ref='email' type='email' defaultValue={ self.props.defaultValue } fieldName={ this.props.fieldName } validations={ self.props.validations }  label={this.props.label}/>
		);
	}

}


export = EmailInput