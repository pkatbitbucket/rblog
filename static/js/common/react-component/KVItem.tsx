import React = require('react')
import ReactDOM = require('react-dom')

class KVItem extends React.Component<any,any>{
	refs:any

	constructor(props){
		super(props)
		this.state = {
			connectorLeft: 0,
		}
	}

	componentDidMount() {
		var newConnectorLeft = ReactDOM.findDOMNode(this.refs['connectorTitle'])["offsetWidth"]
		var newConnectorRight = ReactDOM.findDOMNode(this.refs['connectorValue'])["offsetWidth"]
		this.setState({
			connectorLeft: newConnectorLeft + 'px',
			connectorRight: newConnectorRight + 'px'
		});
	}

	render() {
		var connectorStyle = {
			left: this.state.connectorLeft,
			right: this.state.connectorRight,
		};
		return (
			<div className="kv-item-wrapper">
				<div className={"kv-item small " + this.props.customClass}>
					<div className="connector" style={connectorStyle}></div>
					<div className="key">
						<span ref="connectorTitle">{this.props.title}</span>
						</div>
					<div className="value">
						<span ref="connectorValue">{this.props.value}</span>
						</div>
					</div>
				</div>
		);
	}
}

export = KVItem