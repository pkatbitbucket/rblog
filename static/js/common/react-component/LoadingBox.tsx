import React = require('react')

class LoadingBox extends React.Component<any,any>{
	render() {
		return (
			<div className="loading-box-wrapper ">
				<img src={'/static/travel_product/v1/src/img/loading.png'} />
				<div className="loading-box-title">{this.props.title}</div>
				<div className="loading-box-subtitle">{this.props.subtitle}</div>
			</div>
		)
	}
}

export = LoadingBox