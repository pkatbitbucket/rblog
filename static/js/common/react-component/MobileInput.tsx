/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')
import BaseInput = require('./BaseInput')

class MobileInput extends React.Component<any, any> {	
	refs:any;

	static defaultProps = {
		defaultValue: '',
		fieldName:'mobile',
		validations: ['required','mobile'],
		maxLength:10
	}

	static propTypes = {
		// name: React.PropTypes.string,
		defaultValue: React.PropTypes.any,
		validations: React.PropTypes.any,
	}


	// Called when NEW Number - is passed
	componentWillReceiveProps(newprops){
		var existingMobile = this.state.value;
		var newMobile = newprops.defaultValue;

		var mobileToSet = '';
		if(newMobile===undefined || newMobile===null){
			mobileToSet = existingMobile
		}else{
			mobileToSet = newMobile
		}

		this.setState({
			value:mobileToSet,
		});
	} 

	constructor(props) {
		super(props);
		this.state = {
			value: props.defaultValue
		}
	}
	
	isValid(){
		return this.refs.mobileno.isValid();
	}

	getValue(){
		return this.refs.mobileno.getValue();
	}

	showErrors(){
		this.refs.mobileno.showErrors();
	}

	handleChange(evt){
		var isValid = this.refs.mobileno.state.isValid;
		
		if(this.props.onChange)
			this.props.onChange(evt)

		if(isValid){
			if(this.props.onValid)
				this.props.onValid(evt)
		}
	}

	render(){

		var self = this;
		return (
			<div className="mobile_no_container">
				<span>+91</span>
				<BaseInput onChange={ self.handleChange.bind(self) }  maxLength={this.props.maxLength}  ref='mobileno' type='text' showErrors={this.state.showErrors} defaultValue={ self.state.value } fieldName={ this.props.fieldName } validations={ self.props.validations }  placeholder={ this.props.placeholder }  label={this.props.label}/>
			</div>
		);
	}

}


export = MobileInput