import React = require('react')

const styles = {'display': 'block', 'paddingiRght': '15px'};

class ModalComponent extends React.Component<any, any> {


	static defaultProps = {
		title: undefined,
		contentMarkup:undefined,
	}

	constructor(props) {
		super(props);
	}

	getTitleBlock(){
		var titleBlock: any;
		var title = this.props.title

		if(title){
			titleBlock = <h4>{title}</h4>;
		}

		return titleBlock;
	}

	render(){
		var titleBlock = this.getTitleBlock()
		var contentMarkup = this.props.contentMarkup;

		return (
			<div className="modal" role="dialog" aria-labelledby="mySmallModalLabel" style={ styles }> 
				<div className="modal-dialog"> 
					<div className="modal-content"> 
						<div className="modal-der"> 
							<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button> 
							{ titleBlock }
						</div> 
						{ contentMarkup }
					</div>
				</div> 
			</div>
		)
	}

}

export = ModalComponent