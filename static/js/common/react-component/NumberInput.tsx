/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')
import BaseInput = require('./BaseInput')
 
class NumberInput extends React.Component<any, any> {	
	refs:any;

	static defaultProps = {
		defaultValue: '',
		fieldName:'number',
		validations: ['required','integer'],
		maxLength: 524288,
		placeholder:''
	}
	
	static propTypes = {
		// name: React.PropTypes.string,
		defaultValue: React.PropTypes.any,
		validations: React.PropTypes.any,
		maxLength:React.PropTypes.number,
		onBlur: React.PropTypes.any
	}

	constructor() {
		super();
		this.state = {
			showErrors: false,
			errorMessage:false,
		}
	}


	isValid(){
		return this.refs.numeric.isValid();
	}

	showErrors(errorMessage?:string){
		var showErrors =  this.isValid()? false : true;
		var customErrorMessage = (errorMessage)? errorMessage : this.state.errorMessage
		this.setState({
			showErrors: showErrors,
			errorMessage:customErrorMessage
		})
	}

	getValue(){
		return this.refs.numeric.getValue()
	}

	handleChange(evt){
		var isValid = this.refs.numeric.state.isValid;

		if(isValid){
			this.setState({
				showErrors:false
			})
		}
		//evt.target.value
		if(this.props.onChange)
			this.props.onChange(evt)

		if (isValid && this.props.onValid){
			this.props.onValid(evt)
		}
	}

	handleBlur(evt) {
		if (this.props.onBlur) {
			this.props.onBlur(evt)
		}
	}

	render(){

		var self = this;
		
		return (
			<BaseInput onChange={ self.handleChange.bind(self) }  onBlur={this.handleBlur.bind(this) } ref='numeric' type='number' showErrors={this.state.showErrors} maxLength={this.props.maxLength} defaultValue={ self.props.defaultValue } fieldName={ this.props.fieldName } validations={ self.props.validations } placeholder={this.props.placeholder} label={this.props.label} min={this.props.min} max={this.props.max} errorMessage={ this.state.errorMessage } />
		);
	}

}


export = NumberInput