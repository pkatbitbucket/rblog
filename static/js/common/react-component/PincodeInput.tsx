/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')
import BaseInput = require('./BaseInput')
import InputComponent = require('../new-components/TextInput')
import APIUtils = require("../../new-health-product/utils/APIUtils");


class PincodeComponent extends React.Component <any,any>{
	refs: any;
	static defaultProps = {
		defaultValue: '',
		fieldName: 'Pincode',
		placeholder:'pincode',
		validations: [],
	}

	static propTypes = {
		fieldName: React.PropTypes.string,
		defaultValue: React.PropTypes.string,
		validations: React.PropTypes.any,
	}

	constructor(props) {
		super(props)

		var self = this
		var pincode = this.props.defaultValue	
		this.state = {
			isValid: self.verifyPincode(pincode),
			showErrors: false,
			errorMessage: null,
			pincodeValue:pincode,
		}
	}

	componentWillReceiveProps(newProps){

		var pincode = newProps.defaultValue
		var self = this
		this.setState({
				isValid: self.verifyPincode(pincode),
				pincodeValue:pincode,
			})
	}

	isValid(){
		return this.state.isValid;
	}

	showErrors(errorMessage){

		var errorMessage = (errorMessage)? errorMessage : 'Please enter valid Pincode';

		if(!this.isValid())
			this.setState({
				showErrors: true,
				errorMessage:errorMessage,
			})

	}

	handlePincodeChange(pincode){
		var self = this;
		var isValid = this.verifyPincode(pincode);

		var stateObj = {
			isValid:isValid,
			errorMessage:'Please enter valid Pincode',
			pincodeValue:pincode,
		}

		if(!isValid && this.state.showErrors===true){
			stateObj['showErrors'] = true
		}else{
			stateObj['showErrors'] = false
		}
		

		this.setState(stateObj, function(){
			if(self.props.handlePincodeChange)
				self.props.handlePincodeChange(pincode)
		})
	}

	verifyPincode(pincode){
		return APIUtils.verifyPincode(pincode);
	}

	render(){
		var self = this;

		return (
			<div>
				<InputComponent value={ self.state.pincodeValue } placeholder={ self.props.placeholder }  onChange={ self.handlePincodeChange.bind(self) } ref='pincode' maxLength={6}  />

				{ !this.state.isValid?
					this.state.showErrors===true?(<div className='w--error'> { self.state.errorMessage } </div>):null
					:
					null 
				} 
			</div>
		)
	}
}

export = PincodeComponent