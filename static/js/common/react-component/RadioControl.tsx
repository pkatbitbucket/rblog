import React = require('react')

class RadioControl extends React.Component<any, any> {
	handleClick(item:any){
		this.props.change(item)
	}

	render(){
		//var change = this.props.change
		//var selected = this.props.selected
		var item = this.props.item
		var desc
		var classname = 'radio-control ' + (this.props.selected ? 'selected' : '')

		if (item.desc) {
			desc = <div className="desc">{item.desc}</div>
		}
		return (
			<div onClick={this.handleClick.bind(this,item)} className={classname}>
				<div className="label">
				{item.label}
				</div>
				{desc}
				</div>
		)
	}
}

export = RadioControl