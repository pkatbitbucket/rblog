/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')
import validation = require('./utils/Validation')
var ValidationObj = new validation()

var style = {
	'display': 'inline-block',
	'margin': '8px 8px 8px 0',
}

class InputRadio extends React.Component<any, any> {

	static defaultProps = {
		name: 'ipRadio',
		hasError: false,
		errorClass: 'error hide',
		// defaultClass: 'cf-ip-radio',
		label: '',
		data: [
			{
				text: 'A',
				value: 'a',
			},
			{
				text: 'B',
				value: 'b'
			},
			{
				text: 'C',
				value: 'c'
			}
		],
		// selectedValue: 'b',	//	specify Value to select
		readOnly: false,
		validations: ['required'],
	}

	static propTypes = {
		name: React.PropTypes.string,
		type: React.PropTypes.string,
		errorClass: React.PropTypes.string,
		data: React.PropTypes.any,
		selectedValue: React.PropTypes.any,
		readOnly: React.PropTypes.any,
		validations: React.PropTypes.any,
	}

	constructor(props) {
		super(props)

		var validationArray = this.props.validations;

		var valueToSelect = '';
		if(this.props.selectedValue){
			valueToSelect = this.props.selectedValue;
		}else{
			if(this.props.selectedValue===undefined || this.props.selectedValue===null){
				valueToSelect = this.props.selectedValue
			}else{
				valueToSelect = ''
			}

		}

		// var valueToSelect = (this.props.selectedValue) ? this.props.selectedValue : null; //this.props.data[0]['value'];
		var jsonData = this.selectValue(valueToSelect);
		var fieldName = (this.props.fieldName) ? this.props.fieldName : '';
		var validationResult = ValidationObj.isValid(validationArray, { 'value': valueToSelect, 'fieldName': fieldName });

		this.state = {
			'options': jsonData,
			'inputValue': valueToSelect,
			'isValid': validationResult.valid,
			'errorMessage':validationResult.errorMessage,
			'showErrors':false,
		};
	}

	componentWillReceiveProps(nextProps){
		let jsonData = this.selectValue(nextProps.selectedValue)
		let fieldName = (nextProps.fieldName) ? nextProps.fieldName : ''
		let value = nextProps.selectedValue
		var validationResult = ValidationObj.isValid(nextProps.validations, { 'value': value, 'fieldName': fieldName })

		this.setState({
			'options': jsonData,
			'inputValue': value,
			'isValid': validationResult.valid,
		})
	}

	isValid() {
		return this.state.isValid;
	}

	getValue(){
		return this.state.inputValue;
	}

	showErrors(errorMessage){
		var isValid = this.isValid();
		var customError = (errorMessage)? errorMessage : this.state.errorMessage;

		this.setState({
			showErrors:!isValid,
			errorMessage: customError,
		});
	}

	selectValue(selectedVal) {
		var jsonData = JSON.parse(JSON.stringify(this.props.data));

		// Populating Selected Value
		jsonData.map(function(element, index) {
			element['selected'] = (element['value'] == selectedVal) ? true : false;
		});

		return jsonData;

	}

	handleChange(evt) {
		var value = evt.target.value;
		var validationArray = this.props.validations;
		var fieldName = (this.props.fieldName) ? this.props.fieldName : '';
		var jsonData = this.selectValue(value);
		var validationResult = ValidationObj.isValid(validationArray, { 'value': value, 'fieldName': fieldName });

		this.setState({
			'options': jsonData,
			'inputValue': value,
			'isValid': validationResult.valid,
			'showErrors':false,
			'errorMessage': validationResult.errorMessage,
		}, function() {

			if (this.props.onChange) {
				this.props.onChange(evt);
			}

		});

	}

	render() {
		var self = this;
		var radioInputs = this.state.options.map(function(element, index) {
			var randId = Math.random().toString(36).substr(2, 5)
			return (
				<div className='w--radio_input_item' key={index} style={ style } >
						<input id={randId} type='radio' name={ self.props.name } value={ element.value } checked={ element.selected } key={ index } disabled={ self.props.readOnly } onChange={ self.handleChange.bind(self) } />
						<label htmlFor={randId} ><span className='radio_handle'></span>{ element.text }</label>
					</div>
			);
		});

		return (
			<div className='w--radio_input'>
				<label>{ this.props.label }</label>
				{ radioInputs }
				{ self.state.showErrors ? <p className='fs w--error' >{ self.state.errorMessage }</p> : null }
				</div>
		);
	}

}


export = InputRadio