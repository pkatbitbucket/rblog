import React = require('react')

class TopSiteBar extends React.Component<any, any> {
	render() {
		return (
			<header>
				<div className="header-wrapper">
					{/*<div className="hamburger-menu">

					</div>*/}
					<div className="logo">
						<a href='/'><img src={this.props.imageUrl} alt="Logo" /></a>
						</div>
					<div className="page-title">
						{this.props.pageTitle}
						</div>
					<div className="bar-text pull-right">
						Call us (Toll Free) for assistance: <strong>{this.props.tollFreeNumber}</strong>
						</div>
					</div>
				</header>
		);
	}
}

export = TopSiteBar