/// <reference path="../../typings/tsd.d.ts" />


// ************** NOT TO BE USED *****  INSTEAD USE MultiSelect.tsx ****************
import React = require('react')
import ReactDOM = require('react-dom')
import listensToClickOutside = require('./Dependencies/OnClickHandler')
import helpers = require('./utils/HelperFunctions')
const OPTIONS_TEXT = 'optionsText'
const OPTIONS_VALUE = 'optionsValue'
const SEARCH_KEYS = 'searchKeys'
const OPTIONS_RENDERER = 'optionsRenderer'
var helperObj = new helpers()
 
class RichSelect extends React.Component<any, any> {	


	static defaultProps = {
			ref : '',
			optionsText:'key',
			optionsValue:'value',
			searchKeys:['type', 'value'],
			dropDownDisplayBoxStyle : {
				colour: 'grey'
			},
			containerStyle : {
				border: '1px solid blue',
				width: 'auto',
				height: '200px',
				overflowY:'auto'

			},
			optionsRenderer: function(data : any, configObject : any) {			
				return helperObj.getPropertyValue(data, configObject, OPTIONS_TEXT)				
			},
			dataSource: [
				{
					'key': 'one',
					'value': 1,
					'type':'test'
				},
				{
					'key': 'two',
					'value': 2,
					'type': 'test'
				},
				{
					'key': 'three',
					'value': 3
				}
			]
	}

	static propTypes = {
			ref: React.PropTypes.string,
			optionsText: React.PropTypes.string,
			optionsValue: React.PropTypes.any,
			searchKeys : helperObj.validateStringArray,
			dropDownDisplayBoxStyle : React.PropTypes.any,
			containerStyle :  React.PropTypes.any ,
			optionsRenderer: React.PropTypes.any,
			dataSource:  React.PropTypes.any.isRequired
	}

	private clickOutsideHandler: listensToClickOutside

	componentDidMount() {
        this.clickOutsideHandler = new listensToClickOutside(ReactDOM.findDOMNode(this.refs['RichSelect']), () => {
			this.toggleDisplay(true)
        })
    }

    componentWillUnmount() {
        this.clickOutsideHandler.dispose()
    }

	constructor(props) {		
		super(props)	
		this.state = {
			isOpen: false,
			selectedValue:'',
			selectedLabel: '',
		 	dataSource: this.props.dataSource
		}
	}

	// Exposed Functions
	getValue(){
		return this.state.selectedValue;
	}

	getLabel(){
		return this.state.selectedLabel;
	}

	getData(isFiltered : boolean = false){
		return isFiltered == true ? this.state.dataSource :this.props.dataSource;
	}
	// Exposed Functions

	toggleDisplay(isOnlyClose: boolean = false) {
		this.setState({
			isOpen: isOnlyClose == true ? false : !this.state.isOpen
			//isOpen: isOnlyClose ? false : !this.state.isOpen // CHECK WHY THIS DOESNT WORK
		})
	}

	clearValue() {
		this.setState({
			selectedValue: '',
			selectedLabel: ''
		})
	}

	setValue(option) {		
		var optionValue = helperObj.getPropertyValue(option, this.props, OPTIONS_VALUE)
		var optionTemplate = this.props.optionsRenderer(option, this.props)
		this.setState({
			selectedValue: optionValue,
			selectedLabel: optionTemplate
		})

		this.toggleDisplay()
	}

	search(event){
		var searchQuery = event.target.value
		var allOptions = this.props.dataSource
		var searchedList = []
		var optionsText
		var _this = this;
		var isCheckOptionsText = true
		searchedList = this.props.dataSource.filter(function(option){

			try {
				optionsText = helperObj.getPropertyValue(option, _this.props, OPTIONS_TEXT)
			}
			catch (e){
				isCheckOptionsText = false  
				//the function throws an error if the key doenst exist in dataSource
			}

			if (isCheckOptionsText && optionsText.toString().indexOf(searchQuery) >= 0) { //checking for optionsText 
				return true;
			}
			else { // checking for search keys			
				if (_this.props.hasOwnProperty(SEARCH_KEYS) && _this.props[SEARCH_KEYS].constructor === Array) {					
					for (var key of _this.props[SEARCH_KEYS]) {						
						if(typeof(key) != 'object'){
							if (option.hasOwnProperty(key) &&  option[key].toString().indexOf(searchQuery) >= 0) { //checking for text 
								return true								
							}
						}
						else {
							throw new Error('searchKeys cannot be an object. Invalid value.')				
						}
						
					}
				}
			}
		});

		this.setState({
			dataSource: searchedList,
			selectedValue:'',
			isOpen: true
		})
	}

	

	populateDropDown(option: any , i:number) {
		var  option= this.props.optionsRenderer(option, this.props)
		return (<span key={'dropDownListElement_' + i } onClick={this.setValue.bind(this, option) }>
									{option}
								 <br /></span>)
	
	}

	render() {		
		var dropDownContentStyle = { visibility: this.state.isOpen == true? 'visible' : 'hidden'}
		var caretUpStyle = { visibility: this.state.isOpen == true ? 'visible' : 'hidden' }
		var caretDownStyle = { visibility: this.state.isOpen == true ? 'hidden' : 'visible' }
		var _this = this
		var selectedLabel =  this.state.selectedLabel == '' ? 'Select an option...' : this.state.selectedLabel 
		var renderedDropDown = this.state.dataSource.map(function(option: any, i: number) {
									return (_this.populateDropDown(option, i))})
		
		return (
			<div ref='RichSelect' style={this.props.containerStyle}>
						<div style={this.props.dropDownDisplayBoxStyle}>
						<div type='text'>
							{selectedLabel}
						</div>	

					<span onClick={this.toggleDisplay.bind(this)} >
								<i className='fa fa-2x fa-caret-down' style={caretDownStyle}> </i>	
								<i className='fa fa-2x fa-caret-up' style={caretUpStyle}> </i>								
							</span>
						<span>
							<i className='fa fa-2x fa-times' onClick={this.clearValue.bind(this) } > </i>
						</span>
						</div>

						<div  style={dropDownContentStyle}>
						<div>
							<input type="text" placeholder="Search..." onChange={this.search.bind(this)}/>
						</div>
						<div>							
								{renderedDropDown}			
						</div>
						</div>
				</div>
			)
	}

}

export = RichSelect