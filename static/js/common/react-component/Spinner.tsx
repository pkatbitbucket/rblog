/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')


interface IProps {
	customClass?: string;
}

class Spinner extends React.Component<IProps, any> {

	static propTypes = {
		customClass: React.PropTypes.string,
	}

	constructor(props) {
		super(props)
	}


	render() {
		var customClass = this.props.customClass

		return (
			<div className={"w--spinner " + (customClass ? customClass : '') }>
                <div className="bounce1"></div>
                <div className="bounce2"></div>
                <div className="bounce3"></div>
			</div>
		);
	}

}


export = Spinner