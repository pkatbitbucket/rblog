/// <reference path="../../typings/tsd.d.ts" />
import React = require('react')

interface ISummaryItemProps{
	onClick?: any;
	label: string;
	value: any;
	editLabel?: string;
	key?: string | number;
}

class SummaryItem extends React.Component<ISummaryItemProps, any> {	
	render(){
		let editLabel = this.props.editLabel || 'Edit'

		return (<div className='summary_item_wrapper' onClick={this.props.onClick}>
					<div className='summary_item_label'>
						{this.props.label}
					</div>
					<div className='summary_item_value'>
						{this.props.value}
					</div>
					<div className='summary_item_edit'>
						{editLabel}
					</div>
				</div>)
	}
}

export = SummaryItem