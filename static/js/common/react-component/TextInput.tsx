/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')
import BaseInput = require('./BaseInput')
 
class TextInput extends React.Component<any, any> {	
	refs:any;

	static defaultProps = {
		defaultValue: '',
		fieldName: 'Name',
		placeholder:'',
		validations: ['required','alphaSpace'],
	}

	static propTypes = {
		// name: React.PropTypes.string,
		defaultValue: React.PropTypes.string,
		validations: React.PropTypes.any,
		onChange:React.PropTypes.any,
		onBlur:React.PropTypes.any
	}

	constructor() {
		super();
		this.state = {
			showErrors: false
		}
	}


	isValid(){
		return this.refs.text.isValid();
	}

	showErrors(){
		var showErrors =  this.isValid()? false : true;
		
		this.setState({
			showErrors: showErrors
		})
	}

	handleChange(evt){
		
		var isValid = this.refs.text.state.isValid;
		//evt.target.value
		if(this.props.onChange){
			this.props.onChange(evt)
		}
		if(isValid){
			// Make an API Call	
		}
	}

	handleBlur(evt){
		if(this.props.onBlur){
			this.props.onBlur(evt)
		}
	}
	render(){
		return (
			<BaseInput onChange={ this.handleChange.bind(this) } onBlur={this.handleBlur.bind(this) } ref='text' type='text' showErrors={this.state.showErrors} defaultValue={ this.props.defaultValue } fieldName={ this.props.fieldName } validations={ this.props.validations } placeholder={this.props.placeholder}  label={this.props.label} maxLength= { this.props.maxLength } />
		);
	}

}


export = TextInput