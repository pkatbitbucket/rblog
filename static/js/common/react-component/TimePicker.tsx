/// <reference path="../../typings/tsd.d.ts" />

import React = require('react')
import ReactDOM = require('react-dom')
import listensToClickOutside = require('./Dependencies/OnClickHandler')
import helpers = require('./utils/HelperFunctions')
import moment = require('moment')

class TimeList extends React.Component<any,any> {
	render(){
		var self = this
		var timeListStyle = {cursor:'pointer', maxHeight:'70px'}
		var startTime = moment(this.props.startTime)
		var endTime = moment(this.props.endTime)
		var step = this.props.step
		var timeEntries = []
		var currTime
		while(moment(startTime).isBefore(moment(endTime)) ||
				moment(startTime).isSame(moment(endTime), "seconds")){
			timeEntries.push(<span 
				onClick={self.props.onClick.bind(self, moment(startTime)) }
					key={startTime.format("hh:mm a")}>
				 {startTime.format("hh:mm a")} <br /> </span>)
			startTime.add(step, "minutes")
		}
		return (
			<div style={timeListStyle}>
				{timeEntries}
			</div>
			)
	}
}

class TimeInputBox extends React.Component<any,any> {
	constructor(props : any){
		super(props)
		this.state = {
			inputTime : moment(this.props.selectedTime).format("hh:mm a")
		}
	}

	componentWillReceiveProps(nextProps : any){
			this.setState({
				inputTime:  moment(nextProps.selectedTime).format("hh:mm a")
			})
	}

	setInputTextTime(event: any){
		var inputTime = event.target.value
		this.setState({
				inputTime: inputTime
			})
	}

	handleBlur(isKeyDown: boolean , event: any) {
		var inputTime = event.target.value
		if ((isKeyDown && (event.keyCode == 13 ||event.keyCode == 9)) || !isKeyDown) { //Enter key
			if (moment(inputTime, "hh:mm a").isValid()) {
				this.props.onClick()
				this.props.updateParentState(moment(inputTime, "hh:mm a"))
			}
			else {
				this.setState({
					inputTime:  moment(this.props.selectedTime).format("hh:mm a")
				})
			}
	    }
	}
 
	render(){
		return (
			<div>
				<input ref='TimeBox' type="text"
					onClick={this.props.onClick.bind(this) }
					 value={this.state.inputTime}
					 onBlur={this.handleBlur.bind(this, false) }
					 onKeyUp={this.handleBlur.bind(this, true) }
					 onChange={this.setInputTextTime.bind(this) }
					 />	
			</div>
			)
	}
}

class TimePicker extends React.Component<any,any> {
	static defaultProps = {
		startTime: moment(),
		endTime: moment(moment().add(100,"minutes")),
		step: 21
	}

	static propTypes = {
		startTime: React.PropTypes.any,
		endTime: React.PropTypes.any,
		step: React.PropTypes.number
	}

	constructor(props){
		super(props)
		this.state = {
			selectedTime : moment(),
			isOpen: true
		}
	}
/*
	private clickOutsideHandler: listensToClickOutside

	componentDidMount() {
        this.clickOutsideHandler = new listensToClickOutside(ReactDOM.findDOMNode(this.refs['TimePicker']), () => {
			this.toggleDropDown(true)
        })
    }

    componentWillUnmount() {
        this.clickOutsideHandler.dispose()
    }*/

	toggleDropDown(isOnlyCose: boolean = false){
		this.setState({
			isOpen: isOnlyCose == true? false : !this.state.isOpen
		})
	}

	setTime(time: any){
		this.setState({
			selectedTime: moment(time)
		})
	}

	render(){
		var timeListContainerStyle = { visibility: this.state.isOpen == true? 'visible' : 'hidden'}
		return (
			<div key={'TimePicker'} ref={'TimePicker'}>
				<TimeInputBox 
					selectedTime={this.state.selectedTime} 
					startTime={this.props.startTime} 
					endTime={this.props.endTime}
					onClick={this.toggleDropDown.bind(this)}
					updateParentState={this.setTime.bind(this)}
				/>
				<div style={timeListContainerStyle}>
					<TimeList
						startTime={this.props.startTime}
						endTime={this.props.endTime}
						step={this.props.step}
						onClick={this.setTime.bind(this) }/>
				</div>
			</div>
			)
	}
}


export = TimePicker