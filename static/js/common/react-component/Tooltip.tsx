/// <reference path="../../typings/tsd.d.ts" />
import React = require('react')

interface ITooltipProps{
	value: any;
}

class Tooltip extends React.Component<ITooltipProps, any> {	
	render(){
		return (<div className='w--tooltip'>					
					<span className='w--tooltip_handle'>?</span>
					<div className='w--tooltip_popup'>
						{this.props.value}
					</div>
				</div>)
	}
}

export = Tooltip