/// <reference path="../../../typings/tsd.d.ts" />
class ErrorMessages {

	private validationRulesmapping = {
		'alphaSpace':'input',
		'email':'input',
		'mobile':'input',
		'integer': 'input',
		'required':'required',
		'max':'maxIp',
		'min':'minIp',
	}

	public messageTemplates(fieldName, limit){
		let validField:any = 'Enter valid {fieldName}'
		let minfield:any = 'Minimum {limit} required'
		let maxfield:any = 'Maximum {limit} required'

		return {
			input : validField.supplant({ 'fieldName':fieldName }),
			required : (fieldName.length>0)? validField.supplant({ 'fieldName':fieldName }) : 'This field is required',
			minIp:minfield.supplant({ 'limit':limit }),
			maxIp:maxfield.supplant({ 'limit':limit }),
		};
	}
	
	public fetchMessage(validationResult,fieldName){
		
		var validationType = validationResult.validationType;
		var limit = validationResult.limit;
		
		var templateName = this.validationRulesmapping[validationType]
		var errorMessage = this.messageTemplates(fieldName,limit)[templateName];
		
		return errorMessage;
	}
}

export = ErrorMessages;