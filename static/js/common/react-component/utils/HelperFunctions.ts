/// <reference path="../../../typings/tsd.d.ts" />
import moment = require('moment')

class Helper { 

	public getPropertyValue(propertyObject: any, configObject: any, propertyName: string ) {
		var propertyValue
		if (typeof (propertyObject) != 'object') {
			propertyValue = propertyObject
		}
		else {
			if (configObject.hasOwnProperty(propertyName) && configObject[propertyName] != undefined &&
				configObject[propertyName] != null && configObject[propertyName] != '') {
				var tempProperty = configObject[propertyName]			
				if (propertyObject.hasOwnProperty(tempProperty)) {
					propertyValue = propertyObject[tempProperty]
				}
				else {
					throw new Error(`Datasource does not have property ${tempProperty}`)
				}
			}
			else {
				throw new Error(`Invalid ${propertyName} property`)
			}
		}		
		return propertyValue
	}

	public validateStringArray(props : any, propName : string, componentName: string){
		if(props[propName]){
			if(props[propName].constructor === Array){
				for(var element of props[propName]){
					if(typeof(element) != 'string'){
						return new Error(`${propName} not a valid string array`)
					}	
				}
			}
			else {
				return new Error(`${propName} not a valid string array`)
			}
		} 

	}

	//checks if a moment object exits in a moment object array. Returns Index if exists else -1
	public doesMomentObjectExist(checkObj : any, containingObj: any){
		var index = -1
		if(moment.isMoment(checkObj)){
			for(var i = 0; i < containingObj.length; i++){
				if(moment.isMoment(containingObj[i]) && checkObj.isSame(containingObj[i], 'day')){
					index = i
					break
				}
			}
		}
		return index
	}

	public getUniqueArray(array) {
    var a = array.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
}
}

export = Helper;