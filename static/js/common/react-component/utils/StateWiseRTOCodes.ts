/// <reference path="../../../typings/tsd.d.ts" />
var rtoCodes =
{
    "AN": {
        "value": [
            "01",
            "02"
        ],
            "includes": [],
                "excludes": []
    },
    "AP": {
        "value": [
            "01",
            "38"
        ],
            "includes": [],
                "excludes": []
    },
    "AR": {
        "value": [
            "01",
            "14"
        ],
            "includes": [],
                "excludes": []
    },
    "AS": {
        "value": [
            "01",
            "30"
        ],
            "includes": [],
                "excludes": [
                    26,
                    27,
                    28,
                    29
                ]
    },
    "BR": {
        "value": [
            "01",
            "53"
        ],
            "includes": [],
                "excludes": [
                    48,
                    49,
                    51
                ]
    },
    "CG": {
        "value": [
            "01",
            "19"
        ],
            "includes": [],
                "excludes": []
    },
    "CH": {
        "value": [
            "01",
            "04"
        ],
            "includes": [],
                "excludes": []
    },
    "DD": {
        "value": [
            "02",
            "03"
        ],
        "includes": [],
                "excludes": []
    },
    "DL": {
        "value": [
            "01",
            "13"
        ],
        "includes": [
            "01C", "02C", "03C", "04C", "05C",
            "06C", "07C", "08C", "09C",
            "1C", "2C", "3C", "4C", "5C",
            "6C", "7C", "8C", "9C", "10C",
            "11C", "12C", "13C", "14C",
            "01S", "02S", "03S", "04S", "05S",
            "06S", "07S", "08S", "09S",
            "1S", "2S", "3S", "4S", "5S",
            "6S", "7S", "8S", "9S", "10S",
            "11S", "12S", "13S", "14S"
        ],
                "excludes": []
    },
    "DN": {
        "value": [
            "09",
            "09"
        ],
            "includes": [],
                "excludes": []
    },
    "GA": {
        "value": [
            "01",
            "09"
        ],
            "includes": [],
                "excludes": []
    },
    "GJ": {
        "value": [
            "01",
            "30"
        ],
            "includes": [],
                "excludes": []
    },
    "HP": {
        "value": [
            "01",
            "76"
        ],
            "includes": [],
                "excludes": [
                    59,
                    60,
                    61,
                    70,
                    75
                ]
    },
    "HR": {
        "value": [
            "01",
            "80"
        ],
            "includes": [],
                "excludes": [
                    57,
                    64,
                    75,
                    78
                ]
    },
    "JH": {
        "value": [
            "01",
            "22"
        ],
            "includes": [],
                "excludes": []
    },
    "JK": {
        "value": [
            "01",
            "22"
        ],
            "includes": [],
                "excludes": []
    },
    "KA": {
        "value": [
            "01",
            "64"
        ],
            "includes": [],
                "excludes": [
                    58,
                    59,
                    60,
                    61,
                    62,
                    63
                ]
    },
    "KL": {
        "value": [
            "01",
            "65"
        ],
            "includes": [],
                "excludes": []
    },
    "LD": {
        "value": [
            "01",
            "01"
        ],
            "includes": [],
                "excludes": []
    },
    "MH": {
        "value": [
            "01",
            "50"
        ],
            "includes": [],
                "excludes": []
    },
    "ML": {
        "value": [
            "01",
            "56"
        ],
            "includes": [],
                "excludes": [
                    11,
                    12,
                    13,
                    14,
                    15,
                    16,
                    17,
                    18,
                    19,
                    20,
                    21,
                    22,
                    23,
                    24,
                    25,
                    26,
                    27,
                    28,
                    29,
                    30,
                    31,
                    32,
                    33,
                    34,
                    35,
                    36,
                    37,
                    38,
                    39,
                    40,
                    41,
                    42,
                    43,
                    44,
                    45,
                    46,
                    47,
                    48,
                    49,
                    50,
                    51,
                    52,
                    53,
                    54,
                    55
                ]
    },
    "MN": {
        "value": [
            "01",
            "04"
        ],
            "includes": [],
                "excludes": []
    },
    "MP": {
        "value": [
            "01",
            "69"
        ],
            "includes": [],
                "excludes": [
                    29,
                    55,
                    56,
                    57,
                    58,
                    59,
                    60,
                    61,
                    62,
                    63,
                    64,
                    65,
                    68
                ]
    },
    "MZ": {
        "value": [
            "01",
            "08"
        ],
            "includes": [],
                "excludes": []
    },
    "NL": {
        "value": [
            "01",
            "08"
        ],
            "includes": [],
                "excludes": []
    },
    "OD": {
        "value": [
            "01",
            "35"
        ],
            "includes": [],
                "excludes": []
    },
    "OR": {
        "value": [
            "01",
            "31"
        ],
            "includes": [],
                "excludes": []
    },
    "PB": {
        "value": [
            "01",
            "75"
        ],
            "includes": [],
                "excludes": []
    },
    "PY": {
        "value": [
            "01",
            "04"
        ],
            "includes": [],
                "excludes": []
    },
    "RJ": {
        "value": [
            "01",
            "52"
        ],
            "includes": [],
                "excludes": [
                    39,
                    40,
                    43,
                    44,
                    46,
                    48,
                    49,
                    50,
                    51
                ]
    },
    "SK": {
        "value": [
            "01",
            "05"
        ],
            "includes": [],
                "excludes": []
    },
    "TG": {
        "value": [
            "01",
            "15"
        ],
            "includes": [],
                "excludes": []
    },
    "TN": {
        "value": [
            "01",
            "88"
        ],
            "includes": [],
                "excludes": [
                    8,
                    17,
                    26,
                    35,
                    44,
                    53,
                    62,
                    71,
                    80,
                    82,
                    83,
                    84,
                    85,
                    86,
                    87
                ]
    },
    "TR": {
        "value": [
            "01",
            "04"
        ],
            "includes": [],
                "excludes": []
    },
    "TS": {
        "value": [
            "01",
            "16"
        ],
            "includes": [],
                "excludes": []
    },
    "UA": {
        "value": [
            "01",
            "13"
        ],
            "includes": [],
                "excludes": []
    },
    "UK": {
        "value": [
            "01",
            "13"
        ],
            "includes": [],
                "excludes": []
    },
    "UP": {
        "value": [
            "01",
            "96"
        ],
            "includes": [],
                "excludes": [
                    28,
                    29,
                    38,
                    39,
                    48,
                    49,
                    59,
                    68,
                    69,
                    88,
                    89
                ]
    },
    "WB": {
        "value": [
            "01",
            "79"
        ],
            "includes": [],
                "excludes": [
                    17,
                    21,
                    27,
                    28,
                    35,
                    43,
                    45,
                    46,
                    47,
                    48,
                    49,
                    50,
                    75
                ]
    }
}

export = rtoCodes