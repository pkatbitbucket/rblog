/// <reference path="../../../typings/tsd.d.ts" />
import ValidationRules =  require('./ValidationRules');
import ErrorMessages = require('./ErrorMessages');

const validationRules = new ValidationRules();
const errorMessages = new ErrorMessages();

interface IResponse{
	valid:boolean;
	errorMessage:string;
}

class Validation { 

	public isValid(validationArray : any, data : any):any{
		
		if(typeof(validationArray) == 'object' ){

			var response:IResponse = {	valid:null,
										errorMessage:null
										};
			var fieldName = data.fieldName;

			// Validation Thing
			var validationResult = validationArray.map(function(validation){
			var validationRule = null;
			var limit =null;

				// Will run in case of Max and Min
				if(typeof(validation) == 'object'){
					validationRule = Object.keys(validation)[0]
					limit = validation[validationRule]
				}else{
					validationRule = validation;
				}

				return { 'validationType' : validationRule, 'valid' : validationRules[validationRule](data.value, limit), 'limit':limit };

			});


			response.valid = validationResult.every(function(element,index) {
				return (element.valid == true);
			});

			response.errorMessage = (!response.valid)? this.getErrorMessage(validationResult, fieldName) : null;

			return response;

		}else{
			throw('Error - given validation Object is not an Array')
		}

	}

	public getErrorMessage(validationResults: any, fieldName: string){
		
		for(var key in validationResults){
			var validationResult = validationResults[key];

			if(!validationResult.valid){
				return errorMessages.fetchMessage(validationResult,fieldName);
				break;
			}
		}
	}

}

export = Validation;