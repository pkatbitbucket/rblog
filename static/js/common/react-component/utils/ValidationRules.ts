/// <reference path="../../../typings/tsd.d.ts" />
declare var RegExp: {
	new (pattern: any, flags?: string): RegExp;
	    (pattern: any, flags?: string): RegExp;
}

class ValidationRules { 

	public doesValueExist(data:any){
		
		if(data===undefined || data===null){
			return false
		}else{
			if(data.length>0)
				return true
			else
				return false
		}
	}

	public alphaSpace(data : any){

		if(!this.doesValueExist(data))
			return true;

		if(data.trim().length>0){
			var alphaSpace = new RegExp(/^[a-zA-Z ]*$/g);
			return alphaSpace.test(data);
		}else{
			return false;
		}
			
	}

	public required(data: any){
		if(data===undefined || data===null)
			return false;

		return (data.toString().trim().length>0)? true : false;	
	}

	public email(email: any){

		// No Value
		if(!this.doesValueExist(email))
			return true;
		

		if(email.trim().length>0){
			
			var emailRegex = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/g);
			return emailRegex.test(email);
		}else{
			return false;
		}
	}

	public mobile(mobile: any){

		// No Value
		if(!this.doesValueExist(mobile))
			return true;

		if(mobile.trim().length>0 && 11>mobile.trim().length){
			
			var mobileRegex = new RegExp(/^[7-9][\d]{9}$/);
			return mobileRegex.test(mobile);

		}else{
			return false
		}
	}

	public integer(integer: any){
		
		// No Value
		if(!this.doesValueExist(integer))
			return true;

		if(integer.trim().length>0){

			var IntegerRegex = new RegExp(/^\d+$/g);
			return IntegerRegex.test(integer);
		}else{
			return false
		}
	}

	public min(integer:any, minLimit:any){
		minLimit = parseInt(minLimit);
		// No Value
		if(!this.doesValueExist(integer))
			return true;

		if(integer>=minLimit){
			return true
		}else{
			return false
		}

	}
	
	public max(integer:any, maxLimit:any){
		maxLimit = parseInt(maxLimit);

		// No Value
		if(!this.doesValueExist(integer))
			return true;

		if(integer <=maxLimit){
			return true
		}else{
			return false
		}

	}
}

export = ValidationRules