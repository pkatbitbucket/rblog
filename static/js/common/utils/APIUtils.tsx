import request = require('superagent')

function getCSRF(callback?: Function) {
	request.get('/ajax/get-token').end(function(err, res){
		res = JSON.parse(res.text)
		if (res.csrf) {
			window['CSRF_TOKEN'] = res.csrf
			if(callback)
				callback()
		}
	})
}

function getAreaPlaceHolder(callback){
    request	
		.post('/motor/customer-location-rto/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.send({ 'csrfmiddlewaretoken': window['CSRF_TOKEN'] })
		.end(function(err, res){
			if(res.ok){
				callback(res.body.placeholder)
			} else{
				console.log("Something went wrong", err)
			}
		});
}

export = {
	getCSRF: getCSRF,
	getAreaPlaceHolder: getAreaPlaceHolder
}