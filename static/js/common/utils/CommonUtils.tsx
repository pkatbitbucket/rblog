/// <reference path="../../typings/tsd.d.ts" />

declare var isNaN: {
	(value: string | number | boolean): boolean;
}

declare var parseInt: {
	(value: string | number | boolean): number;
}

function _loadSupplant(){
	String.prototype['supplant'] = function (o:any) {
	    return this.replace(/{([^{}]*)}/g, function (a, b) {
	            var r = o[b];
	            return typeof r === 'string' || typeof r === 'number' ? r : a
	        }
	    )
	}
}

function loadScript(url: string, callback?: Function){
    var script: any = document.createElement("script")
    script.type = "text/javascript";

    if (script.readyState){  //IE
        script.onreadystatechange = function(){
            if (script.readyState == "loaded" ||
                    script.readyState == "complete"){
                script.onreadystatechange = null;
                callback();
            }
        };
    } else {  //Others
        script.onload = callback
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

var utils = {
	isTrue: function(value: string | number | boolean){
		let type = typeof value
		return (typeof value === 'string' && (value == "true" || (isNaN(value) && value != "false") || (!isNaN(value) && !!parseInt(value))))
			|| ((typeof value === 'number' || typeof value === 'boolean') && value)
	},
	simpleClone: function(obj:any):any{
		let type = typeof obj
		if(type==='undefined' || type==='boolean' || type==='number' || type==='string' || type==='function' || obj===null)
			return obj
		return JSON.parse(JSON.stringify(obj))
	},
	loadCustomUtils: function():void{
		_loadSupplant()
	},
	loadScript: loadScript,
	moneyFormat: function(value){
		var returnValue
		var valueWasNegative = false;
           if (value < 0) {
               value = value * -1;
               valueWasNegative = true;
           }
           var x:any = value.toString();
           var res = x;
           if (x.length > 3) {
               var afterPoint = '';
               if (x.indexOf('.') > 0)
                   afterPoint = x.substring(x.indexOf('.'), x.length);
               x = Math.floor(x);
               x = x.toString();
               var lastThree = x.substring(x.length - 3);
               var otherNumbers = x.substring(0, x.length - 3);
               if (otherNumbers != '')
                   lastThree = ',' + lastThree;
               res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree
           }
		          
           var sign = valueWasNegative ? '-' : ''
           returnValue = sign + " " + res
           return returnValue
	}
}

export = utils