class Helpers {
	formatCurrency(value: number) {
		return value.toFixed(0).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
	}
	trimAdditionalSpaces(value:any){
		return value.replace(/\s+/g, " ")
	}
}

export = new Helpers()