import request = require('superagent')

var lmsUtils = {
    sendToLMS: function(data:any, label:string, extras:any, campaign:string, device:string, callback?:Function) {
        var jsonData = {};
        jsonData['campaign'] = campaign;
        jsonData['label'] = label;
        jsonData['device'] = device;
        jsonData['triggered_page'] = window.location.href;
        jsonData['data'] = data;
        for (var key in extras) {
            jsonData[key] = extras[key];
        }

        if (jsonData['mobile'] && jsonData['mobile'] != '') {
            document.cookie = 'mobileNo=' + jsonData['mobile']
        }
        else {
            document.cookie = 'mobileNo='
        }
        document.cookie = 'lms_campaign=' + campaign
        
        request
            .post('/leads/save-call-time/')
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .send({
                'data': JSON.stringify(jsonData),
                'csrfmiddlewaretoken': window['CSRF_TOKEN']
            })
            .end(function(err, res) {
                if (res.ok) {
                    if(callback){
                        callback()
                    }
                } else {
                    console.log("Something went wrong", err)
                }
            });
    }
};

export = lmsUtils