/// <reference path="../../typings/tsd.d.ts" />
import assign = require('object-assign')

var StorageUtils = {
	setValuesInLocalStorage: function(form:string, obj:any){
		if(!!Storage){
			/*let data = localStorage.getItem(form)
			data = !data ? {} : JSON.parse(data)
	        assign(data, obj)*/
		    localStorage.setItem(form, JSON.stringify(obj))
		}
	},
	getValueFromLocalStorage: function(form:string, field?:string){
		if(!!Storage){
			let data = localStorage.getItem(form)
	        return !data?undefined:field?JSON.parse(data)[field]:JSON.parse(data)
		}
	},
	clearValueFromLocalStorage: function(form:string, field?:string){
		if(!!Storage){
			if(field){
				let data = localStorage.getItem(form)
				if (data) {
					data = JSON.parse(data)
					delete data[field]
					localStorage.setItem(form, JSON.stringify(data))
				}
			}else{
				localStorage.removeItem(form)
			}
		}
	}
};

export = StorageUtils;