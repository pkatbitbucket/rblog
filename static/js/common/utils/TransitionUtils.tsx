/// <reference path="../../typings/tsd.d.ts" />
import history = require('../History')

var TransitionUtils = {
	getQueryValuesFromURL: function(){
		let search = window.location.search.substr(1);
		return search?JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g,'":"') + '"}',
			function(key, value) { return key===""?value:decodeURIComponent(value) }):{};
	},
	objectToQueryString: function(obj:any){
		if(typeof obj !== 'object')
			return
		let query = ''
		for (var key in obj) {
			query += '&' + key + '=' + obj[key]
		}
		return query ? '?' + query.substr(1) : ''
	},
	redirectTo: function(url:string, preserveQueryParams?:boolean){
		if(history.replaceState){
			let search='';
			if(preserveQueryParams)
				search=window.location.search;
			history.replaceState(null, url+search)
		}
	},
	transitTo: function(url:string, preserveQueryParams?:boolean){
		if(history.pushState){
			let search='';
			if(preserveQueryParams)
				search=window.location.search;
			history.pushState(null, url+search)
		}
	},
	jumpTo: function(url:string, hash?:string, searchObj?:{}, isOpenInNewTab?:boolean){
		let search = '';
		if(searchObj){
			for(let key in searchObj){
				if(searchObj.hasOwnProperty(key)){
					search+='&'+key+"="+searchObj[key]
				}
			}
		}

		if(hash)
			url+='#'+hash
		if(!!search)
			url+='?'+search.substr(1)

		if (isOpenInNewTab){		
			var win = window.open(url, '_blank')
			if (win) {
				//Browser has allowed it to be opened
				win.focus();
			} else {
				//if popups are not enabled then it will open in the same tab
				window.location.href = url
			}
		}
		else{
			window.location.href = url
		}
		
	},

	goBack: function(){
		window.history.back();
	}
};

export = TransitionUtils;