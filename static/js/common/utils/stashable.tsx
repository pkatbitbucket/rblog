const IDENTIFIER_CLASS_NAME = 'stashable'
const REVERSE_IDENTIFIER_CLASS_NAME = 'reverse-stashable'
const CLASS_NAME_TO_BE_TOGGLED = 'hide'

var STASHABLE_NODES: any = []
var REVERSE_STASHABLE_NODES: any = []
var hidden = false

function init(){
	STASHABLE_NODES = document.getElementsByClassName(IDENTIFIER_CLASS_NAME)
	REVERSE_STASHABLE_NODES = document.getElementsByClassName(REVERSE_IDENTIFIER_CLASS_NAME)
	hidden = false
}

function hide(){
	if(hidden)
		return
	for(let index=0; index < STASHABLE_NODES.length; index++){
		if(STASHABLE_NODES[index].className.indexOf(' ' + CLASS_NAME_TO_BE_TOGGLED) < 0){
			STASHABLE_NODES[index].className += ' ' + CLASS_NAME_TO_BE_TOGGLED
		}
	}
	for(let index=0; index < REVERSE_STASHABLE_NODES.length; index++)
		REVERSE_STASHABLE_NODES[index].className = REVERSE_STASHABLE_NODES[index].className.replace(' ' + CLASS_NAME_TO_BE_TOGGLED, '')
	hidden = !hidden
}

function show(){
	if(!hidden)
		return
	for(let index=0; index < REVERSE_STASHABLE_NODES.length; index++){
		if(REVERSE_STASHABLE_NODES[index].className.indexOf(' ' + CLASS_NAME_TO_BE_TOGGLED) < 0){
			REVERSE_STASHABLE_NODES[index].className += ' ' + CLASS_NAME_TO_BE_TOGGLED
		}
	}
	for(let index=0; index < STASHABLE_NODES.length; index++)
		STASHABLE_NODES[index].className = STASHABLE_NODES[index].className.replace(' ' + CLASS_NAME_TO_BE_TOGGLED, '')
	hidden = !hidden
}

export = {
	init: init,
	hide: hide,
	show: show
}