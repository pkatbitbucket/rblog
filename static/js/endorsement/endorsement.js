var submit_model = function (DATA) {
    var self = this;
    self.policy_number = ko.observable(DATA['policy_number']);
    self.policy_name = ko.observable(DATA['policy_name']);
    self.customer_name = ko.observable(DATA['customer_name']);
    self.customer_email = ko.observable(DATA['customer_email']);
    self.product = ko.observable(DATA['product']);
    self.insurer = ko.observable(DATA['insurer']);
    self.endorsement_type = ko.observable(DATA['endorsement_type']);
    self.field = ko.observable(DATA['fields']);
    self.current_value = ko.observable(DATA['current_value']);
    self.endorsed_value = ko.observable(DATA['endorsed_value']);

    self.product_options = Object.keys(ENDORSEMENT_MAP);
    self.insurer_options = ko.computed(function () {
        if (self.product())
            return INSURERS[self.product()];
        else
            return []
    });
    self.endorsement_type_options = ko.computed(function () {
        if (self.product())
            return Object.keys(ENDORSEMENT_MAP[self.product()]);
        else
            return []
    });
    self.field_options = ko.computed(function () {
        if (self.product() && self.endorsement_type())
            return ENDORSEMENT_MAP[self.product()][self.endorsement_type()];
        else
            return []
    });
    self.current_value_is_visible = ko.computed(function () {
        return self.endorsement_type() != 'addition';
    });
    self.endorsed_value_is_visible = ko.computed(function () {
        return self.endorsement_type() != 'deletion';
    });
    self.validate_form = function () {
        var validation_group = ko.validation.group(self);
        validation_group.showAllMessages();
        var errors = ($('p.validationMessage').filter(':visible').length > 0);
        return !errors;
    };

};

var DATA = DATA || INITIALS;
var FORM = new submit_model(DATA);
ko.applyBindings(FORM);
ko.validation.group(FORM).showAllMessages(false);

$(document).on('submit', '#endorsement_form', function(e) {
        $('#endorsement_submit').prop('disabled', true);
    }
);
