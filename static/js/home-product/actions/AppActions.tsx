import AppDispatcher = require('../../common/Dispatcher')
import ActionEnum = require('../constants/ActionEnum')

export = {
	sampleDispatcher: function(value:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType: ActionEnum.SAMPLE,
			value:value,
			callback:callback
		})
	}
}