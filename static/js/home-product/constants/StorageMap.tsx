var SAMPLE = {
	PARENT: 'fourwheeler',
	REGISTRATION_NUMBER:'reg_number',
	REGISTRATION_YEAR: 'reg_year',
	REGISTRATION_MONTH: 'reg_month',
	REGISTRATION_DATE: 'reg_date',
	VEHICLE: {
		PARENT: 'vehicle',
		ID: 'id',
		NAME: 'name'
	},
	VEHICLE_VARIANT: {
		PARENT: 'vehicleVariant',
		ID: 'id',
		NAME: 'name',
		FUEL_TYPE: 'fuel_type'
	}
}

export = {SAMPLE:SAMPLE}