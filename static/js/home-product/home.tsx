/// <reference path="../typings/tsd.d.ts" />
import React = require('react')
import BaseComponent = require('../common/BaseComponent')
import AppActions = require('./actions/AppActions')
import HomeStore = require('./stores/HomeStore')

interface IHomeState {

}

function getStateFromStores(): any {
	return {
		app: 'samplestate'
	}
}

const PATH = '/components/home/'
const QUERY_PARAMS = ['app']
const STORES = [HomeStore]

class Health extends BaseComponent {
	refs: any;
	state: IHomeState;
	constructor(props) {
		super(props, {
			stores:STORES,
			getStateFromStores: getStateFromStores,
			path:PATH,
			queryParams:QUERY_PARAMS
		})
	}
	render() {
		return (
			<div className="home-product product-form-item reveal">
                <h5>Compare and buy home insurance</h5>
                	<form>
                		{/*component code here*/}
					</form>
                <button id="home_submit" className="btn orange big">View Quotes</button>
				</div>
		);
	}
}

export = Health;