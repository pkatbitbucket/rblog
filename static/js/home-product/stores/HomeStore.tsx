import BaseStore = require('../../common/BaseStore')
import ActionEnum = require('../constants/ActionEnum')
import StorageMap = require('../constants/StorageMap')
import APIUtils = require('../utils/APIUtils')

var KEYS = StorageMap.SAMPLE;

class HomeStore extends BaseStore{
	private _privateField: any[];

	constructor(_namespace:string){
		super(_namespace)
		this.registerDispatcherHandler(this._actionHandler)
	}

	//public getters here
	getData(): any[] {
		return this._privateField || []
	}
	getRegistrationNumber(): string{
		return this.getSelected(KEYS.REGISTRATION_NUMBER)
	}

	//private setters here
	private setData(data){
		this._privateField = data
		this.emitChange()
	}

 	private _actionHandler:Function = (action:any) => {
		 switch(action.actionType) {
		 	// add actions here
			 case ActionEnum.SAMPLE:
				this.setData(action.value)
		 		if(action.callback)
		 			action.callback()
		 		break
		}
	}
}

var homeStore: HomeStore;

export = (function(){
	if(!homeStore){
		homeStore = new HomeStore(KEYS.PARENT)
		// API calls to initiate data, if any
	}
	return homeStore
}())