$(function(){
	var emailRegex = /^([A-Z0-9]+([+._-]?[A-Z0-9]+)*)@((?:[A-Z0-9-]+\.)*\w[A-Z0-9-]{0,66})\.([A-Z]{2,6}(?:\.[A-Z]{2})?)$/i;
	var mobileRegex = /^[7-9][0-9]{9}$/;
	var nameRegex = /^[a-z\.\'\`\s]+$/i;
	var header = $('#top-bar');
	var formNode = $('#header-form-wrapper');
	function setBodyOverflow(hidden){
		$(document).find("body").css({"overflow" : hidden ? 'hidden' : 'auto'})
	}
	function showError(id, text){
		var selector = $('#' + id).parents('.input_group').find('.w--error').removeClass('hide');
		if(text)
			selector.text(text);
		return true
	}
	function hideError(id){
		$('#' + id).parents('.input_group').find('.w--error').addClass('hide');
		return false
	}

	function hideAllErrors(){
		// Get all error fields that does not have "Hide" class
		var errLbls = $(".input_group .w--error").not('.hide');
		$.each(errLbls, function(index, err){
			if(!$(err).hasClass("hide")){
				$(err).addClass("hide")
			}
		});

		// Special handling for global error message for login
		$(".loginErrorField").addClass("hide");
		return false;
	}

	function init(){
		$.cookie = $.cookie || function(){};
		var mobileNo = $.cookie("mobileNo");
		if(mobileNo){
			$('#mobile-number-input').val(mobileNo)
		}
		var email = $.cookie('email');
		if(email){
			$('#email-input').val(email)
		}
	}
	//login form
	function toggleLoginForm(){
		var login = $('#login-form');
		if(login.hasClass('active')){
			$('#login-form').removeClass('active');
			$('#forgot-password-form').addClass('active')
		}else{
			$('#forgot-password-form').removeClass('active');
			$('#login-form').addClass('active')
		}
	}
	//otp form
	function toggleOTPForm(){
		var otp_form = $('#login-otp-form');
		var contact_form = $('#login-contact-form');
		if(otp_form.hasClass('active')){
			contact_form.addClass('active');
			contact_form.removeClass('hide');
			otp_form.addClass('hide');
			otp_form.removeClass('active');
		}else{
			contact_form.addClass('hide');
			contact_form.removeClass('active');
			otp_form.addClass('active');
			otp_form.removeClass('hide');
		}
	}
	function submitForm(form, callback){
		form = $(form);
        $.post(form.attr('action'), form.serialize()).done(callback);
	}
	function onResponseForgotPassword(data){
        var form = $('#forgot-password-form');
		data = JSON.parse(data);
        if(data.success && data.response.success){
            form.find('.success-msg').removeClass('hide');
            form.find('.fp-form-item').addClass('hide');
            $('#reset-password-email').text($("#forgot-password-email-field").val());
        } else {
        	var error = '';
            for(var k in data.errors)
            	error += '<div>' + data.errors[k][0] + '</div>'
            form.find('#forgot-password-error').html(error).removeClass('hide')
        }
	}
	header.on('click', '.close', function(e){
		hideAllErrors();

		$(e.target).parents('.header-popup').removeClass('reveal');
		setBodyOverflow(false)
	});
	header.on('click', '.open-login-form', function(e){
    	e = e || window.event;
    	e.preventDefault();
    	$('#login-popup').addClass('reveal');
		setBodyOverflow(true)
	});
	header.on('click', '.open-contact-form', function(e){
    	e = e || window.event;
    	e.preventDefault();

		toggleContactForm(0);

    	$('#contact-popup').addClass('reveal');
    	header.removeClass('hamburger');
		setBodyOverflow(true)
	});
	header.on('click', '.open-claim-form', function(e){
    	e = e || window.event;
    	e.preventDefault();
    	$('#claim-popup').addClass('reveal');
    	header.removeClass('hamburger');
		setBodyOverflow(true)
	});
	//header.on('click', '#login-form .w--button', function(e){
	header.on('click', '#login-contact-form .w--button', function(e){
		e.preventDefault();
		contact = $("#login-form input[name='contact']").val().trim();
		var formjson = {
			contact: contact
		};
		//var formjson = {
		//	email: $("#login-form input[name='username']").val(),
		//	password: $("#login-form input[name='password']").val()
		//};

		if(validateLoginForm(formjson)){
			//formjson['csrfmiddlewaretoken'] = CSRF_TOKEN;
			$.post('/otp/send/',
				$("#otp_contact_form").serialize(),
				function(response){
					$('#otp_popup_contact').text(contact);
					toggleOTPForm();
				}).fail(function(ex){
					var res = JSON.parse(ex.msg);
					//var res = JSON.parse(ex.responseText);
					if(!res.success){
						$("#login-form .loginErrorField").text(res.message).removeClass("hide");
					}
				});
			//$.post('/api/auth/login/',
			//	formjson,
			//	function(response){
			//		location.reload();
			//	}).fail(function(ex){
			//		var res = JSON.parse(ex.responseText);
			//		if(!res.success){
			//			$("#login-form .loginErrorField").text(res.message).removeClass("hide");
			//		}
			//	})
		}

		return false;
	});
	//formNode.on('click', '.login-toggle', toggleLoginForm);
	formNode.on('click', '.otp-toggle', toggleOTPForm);
	formNode.on('submit', '#forgot-password-form', function(e){
		e.preventDefault();
		submitForm(this, onResponseForgotPassword)
	});

	$(document).on("keyup", function(e){
		if(e.keyCode === 27)
			$(document).find(".close").trigger("click");
		e.stopPropagation();
	});

	//contact us form
	var callMeSubmitted = false;
	var emailMeSubmitted = false;
	function toggleContactForm(step){
		var contactFromIds = ['contact-step-call', 'contact-step-email', 'contact-step-thanks'];
		$('#' + contactFromIds.join(', #')).removeClass('active');
		$('#' + contactFromIds[step]).addClass('active');
		if(callMeSubmitted)
			$('#' + contactFromIds[2] + ' .go-to-call').addClass('hide');
		if(emailMeSubmitted)
			$('#' + contactFromIds[2] + ' .go-to-email').addClass('hide')
	}
	function sendLeadsToLMS(data){
		data = data || {};
		data["campaign"] =  data["campaign"] || "motor-homepage";	//need to confirm
		data["label"] = "homepage";			//need to confirm
		if($.cookie("mobileNo"))
			data["mobileNo"] = $.cookie("mobileNo");

		$.post('/leads/save-call-time/', { 'csrfmiddlewaretoken' : CSRF_TOKEN, 'data' : JSON.stringify(data) })
	}
	function sendNotification(data, callback){
		data = data || {};
		data["campaign"] = "motor-homepage";	//need to confirm
		data["label"] = "homepage";			//need to confirm
		if($.cookie("mobileNo"))
			data["mobileNo"] = $.cookie("mobileNo");

		$.post('/leads/notify-customer-feedback/',
			{ 'csrfmiddlewaretoken' : CSRF_TOKEN, 'data' : JSON.stringify(data) },
			function(response){
				if (callback) callback(response);
			})
	}
	function onSubmitCallMe(e){
		e.preventDefault();
		var invalid = false;
		var mobileNo = $('#mobile-number-input').val();
		var purpose = $('#purpose-input').val();

		if(purpose){
			hideError('purpose-input')
		}
		else{
			showError('purpose-input');
			invalid = true;
		}

		invalid = invalid || !validateMobile('mobile-number-input');

		if(invalid)
			return;

		$.cookie('mobileNo', mobileNo);

		sendLeadsToLMS({'mobile': mobileNo, 'label': purpose});
		if(purpose == "service"){
			var notificationObj = computeNotificationObject({
				'mobileNo': mobileNo,
				'purpose': purpose,
				'source': "contact-phone"
			});
			sendNotification(notificationObj);
		}

		callMeSubmitted = true;
		toggleContactForm(2)
	}
	function onSubmitEmailMe(e){
		e.preventDefault();
		var invalid = false;
		var sendTo = $(".sendTo").text().trim();
		var name = $('#email-me-form #name-input').val();
		var email = $('#email-me-form #email-input').val();
		var message = $('#email-me-form #message-input').val();
		var subject = $('#email-me-form #subject-input').val();

		invalid = nameRegex.test(name) ? (hideError('name-input') || invalid) : showError('name-input');
		invalid = !validateEmail("email-me-form #email-input");
		invalid = message ? (hideError('message-input') || invalid) : showError('message-input');
		invalid = subject? (hideError('subject-input') || invalid) : showError('subject-input');
		if(invalid)
			return;

		$.cookie('email', email);
		var formjson = {
			'name': name,
			'email': email,
			'body': message,
			'subject': subject,
			'source': "contact-email"
		};
		var notificationObj = computeNotificationObject(formjson);

		sendNotification(notificationObj);
		emailMeSubmitted = true;
		toggleContactForm(2)
	}
	formNode.on('click', '.go-to-call', toggleContactForm.bind(null, 0));
	formNode.on('click', '.go-to-email', toggleContactForm.bind(null, 1));
	formNode.on('submit', '#contact-me-form', onSubmitCallMe);
	formNode.on('submit', '#email-me-form', onSubmitEmailMe);

	//mobile hamburger menu
	function toggleHamburgerMenu(){
		header.hasClass('hamburger') ? header.removeClass('hamburger') : header.addClass('hamburger')
	}

	header.on('click', '.hamburger-menu, #flyout-overlay, .header-flyout .close', toggleHamburgerMenu);

	function computeClaimMessage(){
		var now = new Date().getHours();
		var msg = "We will ";
		if(now<7){
			msg += "contact you before 11am today";
		}
		else if(now < 19){
			msg += "call you within 3 hours";
		}
		else{
			msg += "call you before 11am tomorrow"
		}
		return msg;
	}

	function computeNotificationObject(_dataObj){
		var notificationObj = {
			subject:"",
			body:"",
			mobileNo:"",
			email:"",
			source:"",
			label:"",
			name:""
		};
		for(var k in _dataObj){
			notificationObj[k] = _dataObj[k];
		}

		return notificationObj;
	}

	function validateEmail(el_id){
		var _email = $("#" + el_id).val();
		var valid = true;
		if(_email.toString().length==0){
			showError(el_id);
			valid = false;
		}
		else if(!emailRegex.test(_email)){
			//showError(el_id, "Oh. Looks like that email address is not valid. Check again?");
			showError(el_id);
			valid = false;
		}
		else{
			hideError(el_id)
		}
		return valid;
	}

	function validateMobile(el_id){
		var mobileNo = $("#" + el_id).val();
		var valid = true;
		if(!mobileNo || (mobileNo && mobileNo.toString().length == 0)){
			showError(el_id);
			valid = false;
		}
		else if(!mobileRegex.test(mobileNo)){
			//showError(el_id, "Oh. Looks like that mobile number is not valid. Check again?");
			showError(el_id);
			valid = false;
		}
		else{
			hideError(el_id);
		}

		return valid;
	}

	function validateClaimForm(data){
		var valid = true;
		var textRegex = /^[\w\s\.-]+$/;

		valid = validateMobile("claim-form #number-input");

		if(!textRegex.test(data.subject)){
			valid = false;
			showError("claim-form #subject-input");
		}
		else{
			hideError("claim-form #subject-input");
		}

		if(!textRegex.test(data.body)){
			valid = false;
			showError("claim-form #message-input");
		}
		else{
			hideError("claim-form #message-input");
		}

		return valid;
	}

	function validateLoginForm(formjson){
		var valid = true;
		var loginForm = $("#login-form");
		valid = validateEmail("login-form [name='contact']") || validateMobile("login-form [name='contact']");
		if (!valid) {
			showError("login-form [name='contact']");
		} else {
			hideError("login-form [name='contact']");
		}
		//if(!formjson.password){
		//	valid = false;
		//	showError("login-form [name='password']");
		//}
		//else{
		//	hideError("login-form [name='password']");
		//}
		return valid;
	}

	$("#claim-form").on("submit", function(e){
		var formjson = {};
		formjson.subject = $("#claim-form #subject-input").val();
		formjson.body = $("#claim-form #message-input").val();
		formjson.mobileNo = $("#claim-form #number-input").val();
		formjson.source = "claim";

		var message = computeClaimMessage();
		if(validateClaimForm(formjson)){
			var notificationObj = computeNotificationObject(formjson);
			sendNotification(notificationObj, function (data) {
				$("#claim-success-msg").text(message);
				$("#claim-step-form").removeClass("active");
				$("#claim-thanks").addClass("active");
			});
		}

		e.preventDefault();
		return false;
	});
	init()
});
