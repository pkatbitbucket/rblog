$('document').ready(function(){

    /*$('#scrollStp').waypoint({
        element:this,
        handler: function(direction) {
            if(direction === 'down'){
                $(window).unbind('scroll');
            }
        },
        offset:($(window).height()/2) + 'px'
    });*/

    $("#reset-btn").on('click',function(){
        $('#wp3').addClass('active');
    });
    $("#reset-btn").on('click',function(){
        $('#wp1').removeClass('active');
        $('#wp2').removeClass('active');
        $('#wp3').removeClass('active');
        $('.time-line').removeClass('wp1').removeClass('wp2').removeClass('wp3');
        //$(window).scrollTop(0);
    });


    // press carousel slider
    var paginationContainer = $('#press-pagination');
    var paginationContainerArray = new Array();
    var OWLinit = false;
    var owlPress = $("#owl-press");
    owlPress.owlCarousel({
        navigation: false, // Show next and prev buttons
        pagination:true,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        afterInit: afterOWLinit, // do some work after OWL init
        afterAction: afterAction
    });

    function afterAction() {
        if (OWLinit){ updateClass(this.owl.currentItem); }
    }

    function afterOWLinit() {
        $.each(this.owl.userItems, function (i) {
            var imgSrc = $(this).find('img').attr('src'),
                imgTitle = $(this).find('img').attr('title');
            var a = $("<a href='#' class='intro__item'><img title='"+imgTitle+"'src='"+imgSrc+"'></a>");
            paginationContainerArray.push(a);
            // bind click event for new pgination item
            a.bind('click', function (event) {
                event.preventDefault();
                this.goto = i;
                updateClass(this.goto); // add/remove .active class
                owlPress.trigger('owl.goTo', i);
            });
            paginationContainer.append(a);
        });
        updateClass(this.owl.currentItem);
        OWLinit = true;
    }

    function updateClass(elem) {
        $.each(paginationContainerArray, function () {
            this.removeClass('active');
        });
        paginationContainerArray[elem].addClass('active');
    }

  // testimonial carousel slider
    var testimonialCarousel = $("#owl-testimonial");

    testimonialCarousel.owlCarousel({
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,
      navigationText:false,
      items : 1, //10 items above 1000px browser width
      itemsDesktop : [1000,1], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,1], // betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0
      itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
    });

    var fivekeyCarousel = $("#owl-fivekey");
    fivekeyCarousel.owlCarousel({
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,
      navigationText:false,
      items : 1, //10 items above 1000px browser width
      itemsDesktop : [1000,1], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,1], // betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0
      itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
    });

    $(".f-arr").click(function(){
        testimonialCarousel.trigger('owl.next');
    });

    $(".b-arr").click(function(){
        testimonialCarousel.trigger('owl.prev');
    });

    $(".fivekey-f-arr").click(function(){
        fivekeyCarousel.trigger('owl.next');
    });

    $(".fivekey-b-arr").click(function(){
        fivekeyCarousel.trigger('owl.prev');
    });

    // Hide infogram modal when clicked outside
    $(document).on("click", "#five-key-modal", function(e){
        if($(this).hasClass("reveal")){
            if(e.target.id === $(this).attr("id")){
                $(this).find(".close").click();
            }
        }
    })
});

$(function(){
    var loadFiveImg = true
    $('.tab-content__header').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('.tab-content__header').removeClass('active');
        $('.tab-content__panel').removeClass('active');

        $(this).addClass('active');
        $("#"+tab_id).addClass('active');
    });

    $("#infographic-widget").click(function(){
        if(loadFiveImg){
            var images = document.querySelectorAll("#owl-fivekey img[data-src]");
            for(var index=0; index<images.length; index++){
                images[index].setAttribute('src', images[index].getAttribute('data-src'));
            }
            loadFiveImg = false
        }
        $(document.body).addClass('no-scroll').removeAttr('style')
        $("#five-key-modal").removeClass('hide').addClass('reveal')
    })
    $("#close-inforgraphic").click(function(){
        $(document.body).removeClass('no-scroll').removeAttr('style')
        $("#five-key-modal").removeClass('reveal').addClass('hide')
    })

});
