var valid = true;
var rtoList = {};
var rtoCompleteList = {};
var VEHICLE_TYPE = "fourwheeler";

function moneyFormat(str){
    return str.split("").reverse().map(function(item, index){
        if((index + 1)%3 === 0)
            return ","  + item;
        return item;
    }).reverse().join("");
}

function getRTOList(){
    var url = '/motor/' + VEHICLE_TYPE + '/api/rtos/';
    var data = {'csrfmiddlewaretoken': CSRF_TOKEN};
    utils.getData(url,data,undefined,"rtos", function (response){
        var _id = response.map(function(r){return r.id});
        _id.forEach(function(r){
            var _arr = r.split("-");
            if(!rtoList[_arr[0]])
                rtoList[_arr[0]] = [];
            rtoList[_arr[0]].push(_arr[1]);
        });

        rtoCompleteList = response;
    });
}

getRTOList();

function hideModal(){
    window.location.href = window.location.href.toString().split("#")[0];
}

function setBodyContent(obj){
    var html = '<h2>'+obj.message+'</h2>'

    if(obj.response_code === 100){
        var premium =  moneyFormat(parseInt(obj.data.min).toFixed(0));
        html += '<p>Minimum quote value: '+premium+'</p>'
    } else if(obj.response_code === 103){
        html += '<p>Error: '+obj.data.errorItems+'</p>'
    }
    $(".modalDialog .body").html(html)
    $(".modalDialog").addClass('target').show()
}

$("#btnSubmit").on("click", function(e){
    var reg_no = $("#id_name").val().replace(/-/g,"").toUpperCase();

    if(!commonUtilities.RegistrationNumberValidation(reg_no)){
        $("#id_name").parent().find(".error").text("Please enter a valid registration number.")
        return false;
    }

    var formjson = {
        "csrfmiddlewaretoken": CSRF_TOKEN,
        "name": reg_no
    }
    $("#btnSubmit").attr("disabled", true);
    $(".spinner").removeClass("hide");
    $("#btn_submit_text").addClass("hide");
    var url = window.location.pathname.toString();
    $.ajax({
        type: 'POST',
        url: url,
        data: formjson,
        dataType: 'json',
        success: function(data) {
            setBodyContent(data);
        },
        error: function(data){
            var resp = JSON.parse(data.responseText);
            setBodyContent(data);
        }
    });
});

$("#id_name").on("blur",function(){
    valid = commonUtilities.RegistrationNumberValidation(this.value.trim().toUpperCase());
    $(this).parent().find(".error").text(valid?"":"Please enter a valid registration number.")
});