var VEHICLE_TYPE = "fourwheeler";
var rtoList = {};
var rtoCompleteList = {};
var card_id_rejected = false;

function getVehicleMaster(){
    var url = '/motor/' + VEHICLE_TYPE + '/api/vehicles/';
    var data = {'csrfmiddlewaretoken': CSRF_TOKEN};
    utils.getData(url,data,'#id_vehicle_master',"models");
}

function getVehicleVariant(id){
    if(id){
        var url = '/motor/' + VEHICLE_TYPE + '/api/vehicles/' + id + '/variants/';
        var data = {'csrfmiddlewaretoken': CSRF_TOKEN}
        utils.getData(url,data,"#id_vehicle_variant","variants");
    }
}

function getRTOList(){
    var url = '/motor/' + VEHICLE_TYPE + '/api/rtos/';
    var data = {'csrfmiddlewaretoken': CSRF_TOKEN};
    utils.getData(url,data,undefined,"rtos", function (response){
        var _id = response.map(function(r){return r.id});
        _id.forEach(function(r){
            var _arr = r.split("-");
            if(!rtoList[_arr[0]])
                rtoList[_arr[0]] = [];
            rtoList[_arr[0]].push(_arr[1]);
        });

        rtoCompleteList = response;
    });
}

function initDatePickers(){
    var regDate, policyEndDate, dob;
    var today = new Date();
    var tomorrow = new Date();
    var yesterday = new Date();
    var yearRange = [];
    var minYear;
    var maxYear;
    var defaultDOBDate;
    var maxExDate = new Date(today.getFullYear()+1, 11, 31);

    tomorrow.setDate(tomorrow.getDate() + 1);
    yesterday.setDate(yesterday.getDate() - 1);
    maxYear = today.getFullYear() - 18;
    minYear = today.getFullYear() - 100;
    defaultDOBDate = new Date(maxYear, today.getMonth(), today.getDate());

    regDate = new Pikaday({
        field: $("#id_vehicle_registration_date")[0],
        maxDate: yesterday ,
        format:"DD/MM/YYYY",
    });

    policyEndDate = new Pikaday({
        field: $("#id_past_policy_end_date")[0],
        minDate:tomorrow,
        maxDate: maxExDate,
        format:"DD/MM/YYYY"
    });

    dob = new Pikaday({
        field: $("#id_birth_date")[0],
        maxDate: defaultDOBDate,
        format:"DD/MM/YYYY",
        defaultDate: defaultDOBDate,
        yearRange: [minYear, maxYear]
    })
}

function initSelect(){
    $("#id_vehicle_master").select2();
    $("#id_vehicle_variant").select2();
    $("#id_birth_month").html(utils.createOption_new(moment.months()))
}

function registerEvents(){
    $("#id_vehicle_master").on("change", function(){
        var id = JSON.parse($("option:selected", this).val()).id;
        getVehicleVariant(id);
    });

    $("#id_birth_date, #id_past_policy_end_date, #id_vehicle_registration_date").on("change", function(){
        $(this).parent().find("label").addClass("cal-label");
    });

    $('#rsa-policy-form .input-text input[id^="id_"]').on("blur", function(){
        if(!isEmpty(this.value)){
            hideError(this);
        }
    });

    $("#rsa-policy-form select").on("change", function(){
        if(!isEmpty($("option:selected", this).val())){
            hideError(this);
        }
    })

    $("#id_card_id, #id_registration_number").on("blur", function(){
        var data = {};
        var card_id = $("#id_card_id").val().toUpperCase();
        var reg_no = $("#id_registration_number").val().toUpperCase();

        $("#id_card_id").val(card_id);
        $("#id_registration_number").val(reg_no);

        if(!isEmpty(card_id)){
            if(validateCardId(card_id)){
                data.card_id = card_id;
                $("#id_card_id").parent().find(".error").text("")
            }
            else{
                $("#id_card_id").parent().find(".error").text("Please enter a valid card number.")
            }
        }

        if(!isEmpty(reg_no)){
            if(vaidateRegistrationNumber(reg_no)){
                data.registration_number = reg_no;
            }
        }

        if(data.card_id || data.registration_number){
            validateCardAndRefistrationNo(data);
        }
    })

    $("#btnSubmit").on("click", function(){
        var valid = true;
        var formjson = {};
        formjson['csrfmiddlewaretoken'] = CSRF_TOKEN;

        // Validate if all inputs are filled.
        var inputs = $('#rsa-policy-form .input-text input[id^="id_"]');
        for(var i = 0; i<inputs.length; i++){
            var _id = $(inputs[i]).attr("id");
            if(isEmpty(inputs[i].value)){
                valid = false;
                showEmptyError(inputs[i]);
            }
            else{
                var keyName = _id.replace("id_", "");
                $(inputs[i]).parent().find(".error").text("");
                formjson[keyName] = inputs[i].value;
            }
        }

        // Validate all dropdown
        $.each($("select[id^='id_']"),function(index,s){
            if(isEmpty($(s).val())){
                valid = false;
                showEmptyError(s);
            }
        });

        if(!valid)
            return false;

        // Validate Moble number
        var mobNo = $("#id_mobile").val()
        if(!validateMobileNumber(mobNo)){
            valid = false;
            $("#id_mobile").parent().find(".error").text("Please enter a valid mobile number.")
        }

        // Validate Email number
        var emailID = $("#id_email").val()
        if(!validateEmail(emailID)){
            valid = false;
            $("#id_email").parent().find(".error").text("Please enter a valid email id.")
        }

        // validate pincode
        var pincode = $("#id_pincode").val();
        if(!validatePinCode(pincode)){
            valid = false;
            $("#id_pincode").parent().find(".error").text("Please enter a valid pincode.")
        }

        // Validate Card Id
        var cardId = $("#id_card_id").val();
        if(!validateCardId(cardId)){
            valid = false;
            $("#id_card_id").parent().find(".error").text("Please enter a valid card id.")
        }
        else{
            $("#id_card_id").parent().find(".error").text("");
        }

        var reg_no = $("#id_registration_number").val();
        if(!vaidateRegistrationNumber(reg_no)){
            valid = false;
            $("#id_card_id").parent().find(".error").text("Please enter a valid registration number.")
        }
        else{
            $("#id_card_id").parent().find(".error").text("");
        }

        valid = !card_id_rejected && valid;
        if(valid){
            cleanFormData(formjson);
            submitForm(formjson);
        }
    });

    $("#btn_buy_now").on("click", function(){
        computeAndSaveLocalStorage();
    });

    $("#id_policy_expired").on("click", function(){
        $("#modal").addClass("target").fadeIn();
    });

    $("#close-modal").on("click", function(){
        $("#modal").fadeOut(function(){
            $(this).removeClass("target");
            $("#id_policy_expired").attr("checked", false);
        });
    });

    $("#id_mobile").on("blur", function(){
        if(!validateMobileNumber($(this).val())){
            showEmptyError(this, "Please enter a valid mobile number");
        }
    });

    $("#id_email").on("blur", function(){
        if(!validateEmail($(this).val())){
            showEmptyError(this, "Please enter a valid email id");
        }
    });

    $("#id_pincode").on("blur", function(){
        if(!validatePinCode($(this).val())){
            showEmptyError(this, "Please enter a valid pincode");
        }
    });
}

function showEmptyError(el,err){
    var errMsg = $(el).attr("id").replace(/_/g," ").replace("id ", "");
    errMsg = errMsg.charAt(0).toUpperCase() + errMsg.substring(1);
    errMsg += " cannot be left blank."

    errMsg = err || errMsg;
    if($(el).parent().find(".error").length>0)
        $(el).parent().find(".error").text(errMsg)
    else
        $(el).parent().parent().find(".error").text(errMsg)
}

function hideError(el){
    if($(el).parent().find(".error").length>0)
        $(el).parent().find(".error").text("")
    else
        $(el).parent().parent().find(".error").text("");
}

function cleanFormData(data){
    data["address_line1"] = data["correspondence_address"];
    data["address_line2"] = "";
    data["address_line3"] = "";
    data["landmark"] = "";
    data["middle_name"] = ""
    delete data["correspondence_address"];

    data["registration_number"] = data["registration_number"].toString().replace(/-/g,"");

    if(data.full_name){
        var _nameArr = data.full_name.trim().split(/\s/g);
        data.first_name = _nameArr[0];
        delete _nameArr[0];
        data.last_name = _nameArr.join(" ").trim();
        delete data["full_name"];
    }

    data["vehicle_master"] = JSON.parse($("#id_vehicle_variant option:selected").val()).id;
}

function submitForm(data){
    $("#btnSubmit").attr("disabled", true);
    $(".spinner").removeClass("hide");
    $("#btn_submit_text").addClass("hide");

    var url = '/ggc/';
    $.post(url, data, function(response){
        $("#rsa_policy_link").attr("href", response.data.rsa_policy);
        $("#divRSADoc").slideUp().removeClass("hide");
        $("body").animate({
            scrollTop: 0
        }, 600);
        $("#divForm").slideUp("slow", function(){
            $("#divRSADoc").slideDown("slow")
        });
    })
};

function initializeUI(){
    initDatePickers();
    initSelect();
    var dpIds = ["#id_birth_date","#id_past_policy_end_date", "#id_vehicle_registration_date"];

    dpIds.forEach(function(el){
        $(el).attr("readonly", true);
        if(!isEmpty($(el).val())){
            $(el).parent().find("label").addClass("cal-label");
        }
    });

    $("#id_policy_expired").attr("checked", false);
}

(function(){
    initializeUI();
    getVehicleMaster();
    registerEvents();
    getRTOList();
})();


function validateMobileNumber(mobileNo){
    var regex = /^[7-9][\d]{9}$/;
    return regex.test(mobileNo);
}

function validateEmail(emailId){
    var regex = /^([A-Z0-9]+([+._-]?[A-Z0-9]+)*)@((?:[A-Z0-9-]+\.)*\w[A-Z0-9-]{0,66})\.([A-Z]{2,6}(?:\.[A-Z]{2})?)$/i;
    return regex.test(emailId);
}

function validatePinCode(pincode){
    var regex = /^((1[1-9])|([2-9][0-9]))[0-9]{4}$/;
    return regex.test(pincode);
}

function isEmpty(data){
    if(!data || data.trim().length === 0){
        return true;
    }
    return false;
}

function validateCardId(cardId){
    var regex = /^CFR2201[F|U]\d{5}$/;
    return regex.test(cardId);
}

function validateCardAndRefistrationNo(data, callback){
    data['csrfmiddlewaretoken'] = CSRF_TOKEN;
    var url = '/leaflet-campaign/validate/';
    $.post(url, data, function(response){
        if(response.is_used || response.is_valid === false){
            card_id_rejected = true;
            if(response.messages.card_id)
                $("#id_card_id").parent().find(".error").text(response.messages.card_id);
            else if(response.messages.registration_number)
                $("#id_registration_number").parent().find(".error").text(response.messages.registration_number);
        }
        else{
            card_id_rejected = false;
            $("#id_card_id").parent().find(".error").text("");
            $("#id_registration_number").parent().find(".error").text("")
        }

    }).fail(function(error){

    });
}


function vaidateRegistrationNumber(reg_no){
    var valid = commonUtilities.RegistrationNumberValidation(reg_no);
    $("#id_registration_number").parent().find(".error").text(valid?"":"Please enter a valid registration number");
    return valid;
}

function computeAndSaveLocalStorage(){
    var _local = {};

    // Registration number
    var reg_no = $("#id_registration_number").val().replace(/-/g,"");
    var state = reg_no.substring(0,2) || "";

    var numArr = reg_no.substring(2).split(/[a-z]/i).filter(function(a){if(a) return a });
    var rto = reg_no.substring(2).split(/\d/).filter(function(a){if(a) return a })[0] || "";

    var district = numArr[0] || "";
    var series = numArr[1] || "";

    _local.reg_number = [state, district, rto, series];
    _local.formatted_reg_number = _local.reg_number.join("-");

    // Registration Date
    var reg_date = $("#id_vehicle_registration_date").val().split("/");
    _local.reg_year = reg_date[2];
    _local.reg_month = moment.months()[(parseInt(reg_date[1]) -1)];
    _local.reg_date = reg_date[0];

    // Expiry Date
    var today = new Date();
    today.setDate(today.getDate() - 1);
    _local.policyExpired = true;
    _local.expiry_date = today.getDate();
    _local.expiry_month = moment.months()[today.getMonth()];
    _local.expiry_year = today.getFullYear();

    //
    _local.isNewVehicle = 0;
    _local.isUsedVehicle = 0;

    // Vehicle Details
    var vehicle = $("#id_vehicle_master option:selected").val();
    _local.vehicle = vehicle?JSON.parse(vehicle):undefined;

    if($("#id_vehicle_variant option:selected").val()){
        var variant = JSON.parse($("#id_vehicle_variant option:selected").val());
        _local.vehicleVariant = variant;
        _local.fuel_type = {
            code: variant.fuel_type.toLowerCase(),
            id: variant.fuel_type.toUpperCase(),
            name: variant.fuel_type.toLowerCase()
        };
    }

    // Rto
    var rto = rtoCompleteList.filter(function(r){
        return r.id === state + "-" + district;
    })[0];
    _local.rto_info = rto;

    // Date of Birth
    if($("#id_birth_date").val()){
        var dob = $("#id_birth_date").val().split("/");
        _local.user_dob_date = dob[0];
        _local.user_dob_month = dob[1];
        _local.user_dob_year = dob[2];
    }

    // Contact details
    document.cookie ='mobileNo=' + $("#id_mobile").val() + '; path=/';
    document.cookie ='email=' + $("#id_email").val() + '; path=/';

    localStorage.setItem("fourwheeler", JSON.stringify(_local));

    window.location.href = location.origin + "/motor/car-insurance/#";
}
