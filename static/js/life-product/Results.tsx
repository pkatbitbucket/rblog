import React = require('react')
import ReactDOM = require('react-dom')
import BaseComponent = require('../common/BaseComponent')
import TransitionUtils = require('../common/utils/TransitionUtils')
import ReactTransitionGroup = require('react-addons-css-transition-group')
import AppActions = require('./actions/AppActions')
import TermLifeStore = require('./stores/TermLifeStore')
import ResultStore = require('./stores/ResultStore')
import ValidationStore = require('./stores/ValidationStore')
import Constants = require('./constants/constants')
import LoadingBox = require('../common/react-component/LoadingBox')

import TopSiteBar = require('../common/react-component/ResultsTopSiteBar')
import PlanCard = require('./components/PlanCard')
import Modal = require('./components/LifeModals')
import PlanDetails = require('./components/PlanDetails')
import RedirectBox = require('./components/RedirectBox')
import ResultsHeader = require('./components/ResultsHeader')

import GTMEvents = require('./utils/GTMEvents')

/*const PATH = '/components/car/'
const URL_PARAMS = []
const QUERY_PARAMS = []*/
const STORES = [TermLifeStore, ResultStore, ValidationStore]

interface ITermLifeState {
	activeModal:string,
	selectedDetailsIndex:number,
	buyIndex:number,
	quotePlans:any,
	quoteFormData:any,
	activityId:string,
	isLoading:boolean,
	stopRedirect:boolean
}

function getStateFromStores(): any {
	
	return {
		quotePlans: ResultStore.getPlans(),
		quoteFormData:ResultStore.getQuoteFormData(),
		isLoading: ResultStore.getIsLoading()
	}
}

class TermLifeResults extends BaseComponent {
	data:any
	quoteFormData:any
	quotePlans:any
	refs: any
	state: ITermLifeState
	constructor(props) {
		super(props, {
			stores: STORES,
			getStateFromStores: getStateFromStores,
		})
		GTMEvents.resultsPageLoaded()
	}

	onSumAssuredChange(option: any) {
		GTMEvents.editSumAssured(option.value)
		AppActions.setSumAssured(option.value)
	}
	onMaturityAgeChange(valueObj: any) {
		GTMEvents.editMaturityAge(valueObj.value)
		AppActions.setMaturityAge(valueObj.value)
	}
	onViewPlanBrochure(){
		GTMEvents.onViewPlanBrochureClick(this.state.selectedDetailsIndex)
	}
	handleEditQuoteForm(){
		GTMEvents.editQuoteForm()
		TransitionUtils.jumpTo('/term-insurance', '')
	}
	preventRedirect(){
			this.setState({
				stopRedirect: true,
				activeModal: ''
			})	
	}

	handleBuy(index:number) {
		if (this.state.activeModal == 'details'){
			GTMEvents.onDetailsBuyClick(index)
		}
		else {
			GTMEvents.onBuyClick(index)
		}

		var self = this
		self.setState({
			buyIndex:index,
			activeModal: 'redirecting'
		})
			setTimeout(function() {
				if (self.state.stopRedirect == true) {
					GTMEvents.onCancelInsurerSiteRedirect(index)
					self.setState({
						stopRedirect: false
					})
				}
				else {
					AppActions.sendBuyLead(self.state.quotePlans[index])
					TransitionUtils.jumpTo(self.state.quotePlans[index].buy_url, '', {}, true)
					GTMEvents.onInsurerSiteRedirect(index)
					self.setState({
						stopRedirect: false,
						activeModal: ''
					})
				}	
			}, 2000)
		}

	handleDetails(index:number) {
		GTMEvents.onSeeDetailsClick(index)
		this.setState({
			selectedDetailsIndex:index,
			activeModal:'details'
		})
	}
	handleDetailsClose(){
		this.setState({
			selectedDetailsIndex: -1,
			activeModal: ''
		})
	}

	render() {
		var self = this
		var featuresMasterList = ResultStore.getFeaturesList()
		var coverText = ""
		var sumAssuredOptions = Constants.sumAssuredOptions

		for (var i = 0; i < sumAssuredOptions.length; i++){
			if (sumAssuredOptions[i].value == self.state.quoteFormData.sum_assured){
				coverText = sumAssuredOptions[i].key
				break
			}
		}

		var planCards = []
		planCards = self.state.quotePlans.map(function(plan: any, i: number) {
			return <PlanCard key={'TermLifePlanCard' + i} plan={plan} hideActions={false} canBuy={true} 
				onBuy={self.handleBuy.bind(self, i) } onDetails={self.handleDetails.bind(self, i) } featuresMasterList={featuresMasterList}/>
		})

		var planCardWrapper = <div className="plans-wrapper">
				{(self.state.quotePlans.length > 0) ?
				 <div className='plan-card-header-wrapper'>
					<div className="plan-card-header">
						<p>Showing {self.state.quotePlans.length} plans for cover of Rs {coverText}.<span className="sub pull-right">Prices inclusive of taxes</span></p>
						{planCards}
					</div>
				</div> 
				: 
				<div className='unbuyable-container'>
				<div className='unbuyable-plans-message grid-row'>
					<p className="text-center">Darn! It looks like we could not find any plans specific to your requirements. But we are sure our specialists can work something 
							out for you.Please call us on our toll free number <strong> 1800-209-9960</strong>.
					</p>
				</div>
				</div>
				}
					{/*<ContactCard email={false} phone={true} label='results-page' />*/}
			</div> 
	
			return (
			<div>
				{
					self.state.activeModal == 'details' ?
					<Modal size="medium" isOpen={self.state.activeModal == 'details'} onClose={self.handleDetailsClose.bind(self) } >
						<PlanDetails plan={self.state.quotePlans[self.state.selectedDetailsIndex]}  onClose={self.handleDetailsClose.bind(self) }
								onBuy={self.handleBuy.bind(self, self.state.selectedDetailsIndex) } featuresMasterList={featuresMasterList} onViewPlanBrochure={self.onViewPlanBrochure.bind(self)}/>
					</Modal>
					:
					''
				}
				<ReactTransitionGroup transitionName="loading-box" transitionEnterTimeout={500} transitionLeaveTimeout={300}>
					{
					self.state.activeModal == 'redirecting' ?
							<RedirectBox key='Redirecting-Box' title={"You will be redirected to " + self.state.quotePlans[self.state.buyIndex].insurer.name + " website to complete your purchase."} 
								 subtitle='Please Wait...' onCancelClick={self.preventRedirect.bind(self) } cancelButtonText='Cancel'/>
					:
					null
					}
				</ReactTransitionGroup>
				
				{
					self.state.isLoading == true || self.state.activeModal == 'redirecting' ?
						<div className="body-bg-blurred " />
						: null	
				}
			
				 <ReactTransitionGroup transitionName="loading-box" transitionEnterTimeout={500} transitionLeaveTimeout={300}>
						{self.state.isLoading == true ?
							<LoadingBox key='Loading-Box' title="We are fetching the best quotes from all top term life insurers" subtitle="This might take a few seconds" />
							: null}
				</ReactTransitionGroup>
				
				<ReactTransitionGroup  transitionName="results-header" transitionEnterTimeout={500} transitionLeaveTimeout={300}>
				{self.state.isLoading == false && self.state.activeModal != 'redirecting' ?
					<ResultsHeader key='Header-Box' quoteFormData={self.state.quoteFormData} handleEdit={self.handleEditQuoteForm.bind(self) } 
							onSumAssuredChange={self.onSumAssuredChange.bind(self) } onMaturityAgeChange={self.onMaturityAgeChange.bind(this)}
					 />
				: null}
				</ReactTransitionGroup>

				<ReactTransitionGroup transitionName="plans" transitionEnterTimeout={500} transitionLeaveTimeout={300}>	
					{self.state.isLoading == false && self.state.activeModal != 'redirecting' ?
						<div key='Plan-Cards'>
							{planCardWrapper}
						</div>
					: null}
				</ReactTransitionGroup>
			</div>
		)
	}
}

export = TermLifeResults