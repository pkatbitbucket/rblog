import AppDispatcher = require('../../common/Dispatcher')
import ActionEnum = require('../constants/ActionEnum')

export = {
/*	setName: function(value){
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_NAME,
			value:value
		})
	},*/
	setGender: function(value) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_GENDER,
			value: value
		})
	},
	setAge: function(value) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_AGE,
			value: value
		})
	},
	setEmailId: function(value) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_EMAIL_ID,
			value: value
		})
	},
	setPhoneNo: function(value) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_PHONE_NO,
			value: value
		})
	},
	setTobaccoConsumption: function(value) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_TOBACCO_CONSUMPTION,
			value: value
		})
	},
	setSalary: function(value) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_SALARY,
			value: value
		})
	},
	sendToLMS: function() {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SEND_TO_LMS
		})
	},
	submitQuoteForm: function(callback?: Function) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SUBMIT_QUOTE_FORM,
			callback: callback
		})
	},
	setSumAssured: function(value:number) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SUM_ASSURED_CHANGED,
			value:value
		})
	},
	setMaturityAge: function(value:number) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.MATURITY_AGE_CHANGED,
			value: value
		})
	},
	sendBuyLead: function(value: any) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SEND_BUY_LEAD,
			value: value
		})
	}

}