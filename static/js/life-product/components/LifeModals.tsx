import React = require('react');

class Modal extends React.Component<any,any>{

	componentDidMount() {
		var self = this

		//adding modal-open class on body so that scroll on body is DISABLED
		var element = document.body
		element.classList.add("modal-open")

        //handling closing of modal on escape key press
        window.addEventListener("keydown", self.handleKeyPress.bind(self))
    }

    componentWillUnmount() {
		var self = this

		//removing modal-open class from body so that scroll on body is ENABLED
		var element = document.body
		element.classList.remove("modal-open")
        window.removeEventListener("keydown", self.handleKeyPress.bind(self))
    }

    handleKeyPress(event) {
    	debugger
    	//closing on escape key
		if(event.keyCode == 27){
			if (this.props.onClose){
				this.props.onClose()
			}
		}
    }

	render() {
		return (
			<div className={"modal-wrapper " + (this.props.isOpen ? 'reveal' : '') }>
				<div className={"modal-content " + (this.props.customClass ? this.props.customClass : '') + ' ' + (this.props.size ? this.props.size : 'small') }>
					{this.props.children}
					</div>
				</div>
		);
	}
}

export = Modal