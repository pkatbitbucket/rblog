import React = require('react')

class PlanCard extends React.Component<any,any>{
	render() {
		var plan = this.props.plan
		var featuresMasterList = this.props.featuresMasterList
		var freeFeaturesList = []
		var paidFeaturesList = []
		plan.features.map(function(planfeature: string[]){
			var slug: string = planfeature[0]
			var type: string = planfeature[1]
			var element = <div className="rw-text" key={'feature ' + slug}> {featuresMasterList[slug].name}</div>
			if (type == 'free')
				freeFeaturesList.push(element)
			else if(type == 'paid')
				paidFeaturesList.push(element)
		})

		return (
			<div className={"plan-card-wrapper " + (this.props.customClass ? this.props.customClass : '') }>
				<div className="plan-card">
					<div className="plan-column column-1" onClick={this.props.onDetails}>
						<div className="insurer">
							<img src={plan.insurer.logo} className='img-responsive' alt={plan.insurer.name} />
							
						</div>
					</div>
					<div className="plan-column features-column column-2">
						<span className="plan-header">TENURE</span>
						{plan.term + ' years'}
						<div className="feature-tip">
							<h3>TENURE</h3>
								Policy tenure is the number of years for which the term insurance policy is applicable. The sum assured is paid out if the insured dies during the policy tenure.
						</div>
					</div>
					<div className="plan-column features-column column-3">
						<span className="plan-header">FREE FEATURES</span>
							{freeFeaturesList.length > 0 ? freeFeaturesList : <div> NA </div>}
						<div className="feature-tip">
							<h3>FREE FEATURES</h3>
								These features (riders) are part of the base plan and you do not have to pay separately for them.
						</div>
					</div>
					<div className="plan-column features-column column-4">
						<span className="plan-header">PAID FEATURES</span>
							{paidFeaturesList.length > 0 ? paidFeaturesList : <div> NA </div>}
						<div className="feature-tip">
							<h3>PAID FEATURES</h3>
								These features (riders) are not part of the base plan and you have to pay separately for them.
						</div>
					</div>
					<div className="plan-column column-5">
						<div className="btn orange-flat btn-inline-label" onClick={this.props.onBuy}>
	                        <span className="btn-label">Buy For</span>
							<span>Rs.{plan.premium.annual.label}</span>
							<div className="frequency">per year</div>
						</div>
					</div>
					<div className="plan-name">
						Plan: <b> {plan.insurer.name} {plan.name} </b>
						&nbsp; &nbsp;
						Claims Settled:  <b> {plan.insurer.claims_settled + '%'} </b>
							{(this.props.canBuy == true && !this.props.hideActions) ? <span className="see-details pull-right" onClick={this.props.onDetails}>
								See Details
								</span> : null }

						{/*(!this.props.hideActions) ? <div className={"compare-btn pull-right " + (plan.is_selected ? 'selected' : '') } onClick={this.props.onAddToCompare}>
						{plan.is_selected ? [<span>&#x2713; </span>, 'Added to compare'] : '+ Add to compare'}</div> : null*/}
					</div>
				</div>
			</div>
		);
	}

}

export = PlanCard