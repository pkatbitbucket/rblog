import React = require('react')
import PlanCard = require('./PlanCard')

class PlanModal extends React.Component<any,any> {
	render() {
		var plancard = <PlanCard hideActions={true} canBuy={true} plan={this.props.plan} onBuy={this.props.onBuy} featuresMasterList={this.props.featuresMasterList }/>
		var plandetails = []

		var frequencies = this.props.plan.frequency.join(' / ')

		var ridersList = []
		var riderName
		for (var i = 0; i < this.props.plan.features.length; i++) {
			var feature = this.props.plan.features[i]
			if(feature[1] == 'paid'){
				riderName = this.props.featuresMasterList[feature[0]].name
				ridersList.push(<div key={'Rider ' + i}>{riderName}</div>)
			}
		}

		
		plandetails.push(<div className='detail-section-feature' key={'FeatureHeader1'}>
							<div className='feature-header'>AGE AND SUM ASSURED UP TO WHICH NO MEDICAL REQUIRED</div>
								<div className='compare-feature'>
									<div className="c_cover">{this.props.plan.medical_required_condition}</div>
								</div>
							</div>)
		plandetails.push(<div className='detail-section-feature' key={'FeatureHeader2'}>
							<div className='feature-header'>BROCHURE / POLICY WORDING</div>
								<div className='compare-feature'>
									<div className="c_cover"><a className="vb"  onClick={this.props.onViewPlanBrochure} href={this.props.plan.insurer.policy_wording} target="_blank">View Brochure</a> </div>
									</div>
			</div>)
		plandetails.push(<div className='detail-section-feature' key={'FeatureHeader3'}>
							<div className='feature-header'>PREMIUM FREQUENCY</div>
								<div className='compare-feature'>
									<div className="c_cover">{frequencies}</div>
									</div>
			</div>)
		plandetails.push(<div className='detail-section-feature' key={'FeatureHeader4'}>
							<div className='feature-header'>RIDERS AVAILABLE</div>
								<div className='compare-feature'>
									<div className="c_cover">{ridersList.length > 0 ? ridersList : 'NA'}</div>
									</div>
			</div>)
		
		var exclusionsList = this.props.plan.exclusions.map(function(data:string, i:number){
			return <li  key={'Exclusion_' + i}> {data} </li>
		})
		var aboutUsList = this.props.plan.insurer.about_us.map(function(data: string, i:number) {
			return <li key={'AboutUs_' + i}> {data} </li>
		})


		plandetails.push(<div className="detail-section-feature"  key={'FeatureHeader5'}>
							<div className="feature-header"> EXCLUSIONS</div>
							<div className="compare-feature"><ul>
								{exclusionsList}
								<li className="disclaimer">
									For exact wordings, Kindly refer <a className="vb"  onClick={this.props.onViewPlanBrochure} href={this.props.plan.insurer.policy_wording} target="_blank">plan brochure</a>
								</li>
							</ul></div>
							<div className="feature-header">ABOUT</div>
							<div className="compare-feature"><ul>{aboutUsList}</ul></div>
						</div>)

		return (
			<div className='plan-popup-container'>
				<div className="modal-header">
					<h2>Plan Details</h2>
					<div className="close-btn" onClick={this.props.onClose}>X</div>
				</div>
				<div className="modal-inner-content">
					{plancard}
					<div className="detail-section">
						{plandetails}
					</div>
				</div>
				
			</div>
		)
	}
}

export = PlanModal