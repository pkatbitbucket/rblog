import React = require('react')

class RedirectBox extends React.Component<any,any>{
	render() {
		return (
			<div className="redirect-box-wrapper ">
				<img src={'/static/travel_product/v1/src/img/loading.png'} />
				<div className="redirect-box-title">{this.props.title}</div>
				<div className="redirect-box-subtitle">{this.props.subtitle}</div>
				<button className="w--button w--button--orange w--button--large btn--action" onClick={this.props.onCancelClick}>{this.props.cancelButtonText}</button>
			</div>
		)
	}
}

export = RedirectBox