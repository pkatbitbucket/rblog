import React = require('react')
import KVitem = require('../../common/react-component/KVItem')
import MultiSelect = require('../../common/react-component/MultiSelect')
import Constants = require('../constants/constants')

class ResultsHeader extends React.Component<any,any>{

	render() {
		var quoteFormData = this.props.quoteFormData
		var maturityAgeOptions = []
		for (var i = this.props.quoteFormData.age + 5; i <= 80; i++){
			maturityAgeOptions.push({key:i, value:i})
		}

		var salary = ""
		var salaryOptions = Constants.salaryOptions
		for (var j = 0; j < salaryOptions.length;j++){
			if (salaryOptions[j].value == quoteFormData.salary){
				salary = salaryOptions[j].key
				break
			}
		}

		var gender = ""
		var genderOptions = Constants.genderOptions
		for (var k = 0; k < genderOptions.length; k++) {
			if (genderOptions[k].value == quoteFormData.gender) {
				gender = genderOptions[k].label
				break
			}
		}
		return (

			<div className= {"results-header-wrapper " + (this.props.customClass ? this.props.customClass : '') }>

				<div className="results-header-block">
					<h3 className="block-title">Showing Results For <button onClick={this.props.handleEdit} className="empty-btn light">Edit</button></h3>
					<KVitem title='Age' customClass='dark' value={quoteFormData.age} />
					<KVitem title='Salary' customClass='dark' value={salary} />
					<KVitem title='Gender' customClass='dark' value={gender} />
					<KVitem title='Consume tobacco/smoke cigarettes' customClass='dark' value={quoteFormData.tobacco_user ?'Yes': 'No'} />
				</div>

				<div className="results-header-block">
					<h3 className="block-title">SUM ASSURED</h3>
					<p className="sub">We have selected optimal sum assured amount, you can modify it if required. Remember your
						cover cannot exceed 20 times your salary</p>
					<MultiSelect dataSource={Constants.sumAssuredOptions} optionsText='key' optionsValue='value' values={quoteFormData.sum_assured}
						onChange={this.props.onSumAssuredChange.bind(this) } labelText="Sum Assured" isSearchable={false}  customClass="white-bg"
						header="SUM ASSURED"/>
				</div>
				<div className="results-header-block">
					<h3 className="block-title">INSURED UP TO AGE </h3>
					<p className="sub">Upto what age do you want to be insured?</p>
					<MultiSelect dataSource={maturityAgeOptions} optionsText='key' optionsValue='value' values={quoteFormData.maturity_age}
						onChange={this.props.onMaturityAgeChange.bind(this) } labelText="Maturity Age" isSearchable={false}  customClass="white-bg"
						header="MATURITY AGE"/>
				</div>
			</div>
		)
	}
}

export = ResultsHeader