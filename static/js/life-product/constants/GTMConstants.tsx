export = {
	RESULTS_PAGE_LOADED: {
		EVENT: 'TermLifeEventResultsLoaded',
		VIRTUAL_PAGE_PATH: '/vp/termlife/initiate-results',
		VIRTUAL_PAGE_TITLE: 'VP-TERM-LIFE-RESULTS'
	},
	VIEW_QUOTES: {
		EVENT: 'HomeTermLifeEventGetQuote',
		CATEGORY: 'Homepage Term Life - Get Quote',
		ACTION: 'Term Life Quote Viewed',
		LABEL: ''
	},
	VIEW_QUOTES_MOBILE_NUMBER_SUBMIT: {
		EVENT: 'HomeTermLifeEventNumberSubmitted',
		CATEGORY: 'TERM LIFE',
		ACTION: 'CallScheduled',
		LABEL: 'home-page'
	},
	EDIT_QUOTE_FORM: {
		EVENT: 'TermLifeEventEditResults',
		CATEGORY: 'Term Life - Edit Results',
		ACTION: 'Edit Clicked',
		LABEL: ''
	},
	EDIT_SUM_ASSURED: {
		EVENT: 'TermLifeEventSumAssuredChanged',
		CATEGORY: 'Term Life - Change Sum Assured',
		ACTION: 'Sum Assured Changed'
	},
	EDIT_MATURITY_AGE: {
		EVENT: 'TermLifeEventMaturityAgeChanged',
		CATEGORY: 'Term Life - Change Maturity Age',
		ACTION: 'Maturity Age Changed'
	},
	BUY_CLICK: {
		EVENT: 'TermLifeEventBuyForClicked',
		CATEGORY: 'Term Life - Buy For',
		LABEL: ''
	},
	SEE_DETAILS_POPUP: {
		EVENT: 'TermLifeEventSee Details',
		CATEGORY: 'Term Life - See Details',
		LABEL: ''
	},
	SEE_DETAILS_BUY_CLICK: {
		EVENT: 'TermLifeEventBuyForSeeDetails',
		CATEGORY: 'Term Life - Buy For See Details',
		LABEL: ''
	},
	VIEW_PLAN_BROCHURE: {
		EVENT: 'TermLifeEventViewBrochure',
		CATEGORY: 'Term Life - View Brochure',
		LABEL: ''
	},
	CANCEL_INSURER_SITE_REDIRECT: {
		EVENT: 'TermLifeEventBuyForCanceled',
		CATEGORY: 'Term Life - Buy For Canceled',
		LABEL: ''
	},
	REDIRECTED_TO_INSURER_SITE: {
		EVENT: 'TermLifeEventBuyForInsurerRedirect',
		CATEGORY: 'Term Life - Buy For Insurer Redirect',
		LABEL: ''
	},
}