export = {
	sumAssuredOptions: [
		{
			key: '10 lakhs',
			value: 1000000
		},
		{
			key: '20 lakhs',
			value: 2000000
		},
		{
			key: '25 lakhs',
			value: 2500000
		},
		{
			key: '50 lakhs',
			value: 5000000
		},
		{
			key: '60 lakhs',
			value: 6000000
		},
		{
			key: '75 lakhs',
			value: 7500000
		},
		{
			key: '1 crore',
			value: 10000000
		},
		{
			key: '2 crore',
			value: 20000000
		},
		{
			key: '3 crore',
			value: 30000000
		},
		{
			key: '4 crore',
			value: 40000000
		},
	],

	genderOptions:[{
		label: 'Male',
		value: 'M',
	},
	{
		label: 'Female',
		value: 'F',
	}
	],

	LMSContants:{
		campaign: 'term',
		label:'home-page',
		device:'Desktop'
	},
	salaryOptions: [{
		key: '3-5 lakhs',
		value: 300000
	},
		{
			key: '5-10 lakhs',
			value: 500000
		},
		{
			key: '10+ lakhs',
			value: 1000000
		}
	]
}