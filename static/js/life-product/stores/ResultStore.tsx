import moment = require('moment')
import BaseStore = require('../../common/BaseStore')
import TermLifeStore = require('./TermLifeStore')
import ActionEnum = require('../constants/ActionEnum')
import StorageMap = require('../constants/StorageMap')
import Constants = require('../constants/constants')
import LMSUtils = require('../../common/utils/LMSUtils')
import APIUtils = require('../utils/APIUtils')

var KEYS = StorageMap.LIFE

class ResultStore extends BaseStore {
	_quotePlans //array of plans
	_quoteFormData// object of quote form data
	_featuresMasterList //array of all features
	_activityId
	_isLoading = false
	constructor(_namespace: string) {
		super(_namespace)
		this.registerDispatcherHandler(this._actionHandler)
		this.init()
	}

	//getters
	public getPlans() {
		return this._quotePlans
	}
	public getQuoteFormData() {
		return this._quoteFormData
	}
	public getFeaturesList() {
		return this._featuresMasterList
	}
	public getIsLoading() {
		return this._isLoading
	}
	//

	//setters
	private init(){
		if(window["responseData"] && window["responseData"].data){
			var responseData = window["responseData"]
			this._quoteFormData = responseData.data.quote_form
			this._quotePlans = responseData.data.plan_list
			this._featuresMasterList = responseData.data.features
			this._activityId = responseData.activity_id
		}
	}

	private setLoading(value:boolean, isPreventEmit:boolean = false){
		this._isLoading = value
		if (!isPreventEmit){
			this.emitChange()
		}
	}

	private refreshStoreData(data:any){
		
		this._quotePlans = data.data.plan_list
		this._quoteFormData = data.data.quote_form
		this.setLoading(false, true)
    	this.emitChange()
	}
	
	private formQuoteRequest(){
		var quoteFormData = {}

		if (this._quoteFormData != undefined){
			quoteFormData["gender"] = this._quoteFormData["gender"]
			quoteFormData["age"] = this._quoteFormData["age"]
			quoteFormData["tobacco_user"] = this._quoteFormData["tobacco_user"]
			quoteFormData["salary"] = this._quoteFormData["salary"]
			quoteFormData["mobile"] = this._quoteFormData["mobile"]
			quoteFormData["email"] = this._quoteFormData["email"]
			quoteFormData["sum_assured"] = this._quoteFormData["sum_assured"]
			quoteFormData["maturity_age"] = this._quoteFormData["maturity_age"]
		}
		else {
			quoteFormData["gender"] = TermLifeStore.getGender()
			quoteFormData["age"] = TermLifeStore.getAge()
			quoteFormData["tobacco_user"] = TermLifeStore.getTobaccoConsumption()
			quoteFormData["salary"] = TermLifeStore.getSalary()
			quoteFormData["mobile"] = TermLifeStore.getPhoneNo()
			quoteFormData["email"] = TermLifeStore.getEmailId()
		}
		
		return quoteFormData
	}

	private postQuoteFormData(callback?: Function) {
    //massage data here according to that required by post call
  /*  interface IquoteFormData {
      gender: string,
      age: number,
      tobacco_user: boolean,
      salary: number,
      mobile: string
    }*/
		this.setLoading(true)
		var quoteFormData = this.formQuoteRequest()
		var plans = APIUtils.postLifeQuoteFormData(quoteFormData, callback)
  }

  	private refreshQuoteDataMaturity(value:number){
		this.setLoading(true)
		var quoteFormData = this.formQuoteRequest()
		quoteFormData['maturity_age'] = value
		var plans = APIUtils.refreshLifeQuoteFormData(quoteFormData, this._activityId, this.refreshStoreData.bind(this))
  	}

	private refreshQuoteDataSumAssured(value: number) {
		this.setLoading(true)
		var quoteFormData = this.formQuoteRequest()
		quoteFormData['sum_assured'] = value
		var plans = APIUtils.refreshLifeQuoteFormData(quoteFormData, this._activityId, this.refreshStoreData.bind(this))
	}

	private sendBuyLead(data:any){
		APIUtils.sendBuyLead(data,this._activityId)
	}

	private _actionHandler: Function = (action: any) => {
		switch (action.actionType) {
	      case ActionEnum.SUBMIT_QUOTE_FORM:
	        this.postQuoteFormData(action.callback)
	        break
		  case ActionEnum.SUM_ASSURED_CHANGED:
			  this.refreshQuoteDataSumAssured(action.value)
			break
		  case ActionEnum.MATURITY_AGE_CHANGED:
			this.refreshQuoteDataMaturity(action.value)
			break
		  case ActionEnum.SEND_BUY_LEAD:
			  this.sendBuyLead(action.value)
			break
		}
	}
}

var resultStore: ResultStore

export = (function() {
  if (!resultStore) {
    resultStore = new ResultStore(KEYS.PARENT)
	}
  return resultStore
} ())