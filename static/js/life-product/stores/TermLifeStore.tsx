import moment = require('moment')
import BaseStore = require('../../common/BaseStore')
//import ValidationStore = require('./ValidationStore')
import ActionEnum = require('../constants/ActionEnum')
import StorageMap = require('../constants/StorageMap')
import Constants = require('../constants/constants')
import LMSUtils = require('../../common/utils/LMSUtils')
import APIUtils = require('../utils/APIUtils')

var KEYS = StorageMap.LIFE

class TermLifeStore extends BaseStore{
	constructor(_namespace:string){
		super(_namespace)
		this.registerDispatcherHandler(this._actionHandler)
	}

	//getters
	/*public getName(){
		return this.getSelected(KEYS.NAME)
	}*/
	public getGender() {
		return this.getSelected(KEYS.GENDER)
	}
	public getAge(){
		return this.getSelected(KEYS.AGE)
	}
	public getEmailId() {
		return this.getSelected(KEYS.EMAIL_ID)
	}
	public getPhoneNo() {
		return this.getSelected(KEYS.PHONE_NO)
	}
	public getTobaccoConsumption() {
		return this.getSelected(KEYS.IS_TOBACCO_CONSUMED)
	}
	public getSalary() {
		return this.getSelected(KEYS.SALARY)
	}
	public getQuoteFormData(){
		return this.getSelected()
	}
	//

	//setters
/*	private setName(name:string){
		
		let obj = {}
		obj[KEYS.NAME] = name
		this.setSelected(obj)
	}*/
	private setGender(value: string) {
		let obj = {}
		obj[KEYS.GENDER] = value
		this.setSelected(obj)
	}
	private setAge(age: any) {
		let obj = {}
		obj[KEYS.AGE] = parseInt(age)
		this.setSelected(obj)
	}
	private setPhoneNo(value: string) {
		let obj = {}
		obj[KEYS.PHONE_NO] = value
		this.setSelected(obj)
	}
	private setEmailId(value: string) {
		let obj = {}
		obj[KEYS.EMAIL_ID] = value
		this.setSelected(obj)
	}
	private setTobaccoConsumption(value: boolean) {
		let obj = {}
		obj[KEYS.IS_TOBACCO_CONSUMED] = value
		this.setSelected(obj)
	}
	private setSalary(value: number) {
		let obj = {}
		obj[KEYS.SALARY] = value
		this.setSelected(obj)
	}
	//

	//Others
	private sendToLMS(){
		var termLifeQuoteObj = this.getQuoteFormData()
		LMSUtils.sendToLMS({}, Constants.LMSContants.label, termLifeQuoteObj, Constants.LMSContants.campaign, Constants.LMSContants.device)
	}

 	private _actionHandler:Function = (action:any) => {
		 switch(action.actionType) {
			/* case ActionEnum.SET_NAME:
				 this.setName(action.value)
				 break*/
			 case ActionEnum.SET_GENDER:
				 this.setGender(action.value)
				 break
			 case ActionEnum.SET_AGE:
				 this.setAge(action.value)
				 break
			 case ActionEnum.SET_PHONE_NO:
				 this.setPhoneNo(action.value)
				 break
			 case ActionEnum.SET_EMAIL_ID:
				 this.setEmailId(action.value)
				 break
			 case ActionEnum.SET_TOBACCO_CONSUMPTION:
				 this.setTobaccoConsumption(action.value)
				 break
			 case ActionEnum.SET_SALARY:
				 this.setSalary(action.value)
				 break
			 case ActionEnum.SEND_TO_LMS:
				 this.sendToLMS()
				 break
		}
	}
}

var termLifeStore: TermLifeStore;

export = (function(){
	if (!termLifeStore) {
		termLifeStore = new TermLifeStore(KEYS.PARENT)
	}
	return termLifeStore
}())