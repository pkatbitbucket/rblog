import BaseStore = require('../../common/BaseStore')
import TermLifeStore = require('./TermLifeStore')
import ActionEnum = require('../constants/ActionEnum')
import StorageMap = require('../constants/StorageMap')
import CommonUtils = require('../../common/utils/CommonUtils')
import ValidationRulesDef = require('../../common/react-component/utils/ValidationRules')
import moment = require('moment')

var ValidationRules = new ValidationRulesDef()
var KEYS = StorageMap.LIFE;

class ValidationStore extends BaseStore {
	constructor(_namespace: string) {
		super(_namespace)
		this.registerDispatcherHandler(this._actionHandler)
	}

	//public getters here
	getIsValid() {
		//	var nameError = this.getNameError()
			var genderError = this.getGenderError()
		//	var dobError = this.getDOBError()
			//var sumAssuredError = this.getSumAssuredError()
			var phoneNoError = this.getPhoneNoError()
			var emailError = this.getEmailError()
			var getAgeError = this.getAgeError()
			var salaryError = this.getSalaryError()
			var tobaccoConsumptionError = this.getTobaccoConsumptionError()
			if (genderError['isValid'] == true 
				&& phoneNoError['isValid'] == true && emailError['isValid'] == true
				&& getAgeError['isValid'] == true && salaryError['isValid'] == true 
				&& tobaccoConsumptionError['isValid'] == true) {
				return true
			}
			else {
				return false
			}
	}

	/*getNameError() {
		var errorObj = {}
		errorObj['isValid'] = true
		errorObj['errorMessage'] = ""

		var name = TermLifeStore.getName()

		if (!ValidationRules.alphaSpace(name)) {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "Only alphabets and spaces are allowed in name."
		}
		else if(name == undefined || name == null || name == "") {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "Please enter your name."
		}
		return errorObj
	}*/
	getGenderError() {
		var errorObj = {}
		errorObj['isValid'] = true
		errorObj['errorMessage'] = ""

		var gender = TermLifeStore.getGender()
		if (gender == undefined || gender == null) {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "Select your gender."
		}
		return errorObj
	}
	/*getDOBError(enteredDOB:any = null) { 
		//enteredDOB is the one user can see on the screen, the same is not present in 
		//Local Storage as we save the value in Local storage when the DOB is valid
		var errorObj = {}
		errorObj['isValid'] = true
		errorObj['errorMessage'] = ""

		var dob = TermLifeStore.getDOB()
		if ((dob == undefined || dob == null || dob == "") && 
			(enteredDOB == undefined || enteredDOB == null || enteredDOB == "")) {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "Please enter a valid DOB."
		}
		else if (dob.isAfter(moment())) {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "DOB cannot be beyond."
		}
		return errorObj
	}*/
	getAgeError(){
		var errorObj = {}
		errorObj['isValid'] = true
		errorObj['errorMessage'] = ""

		var age = TermLifeStore.getAge()
		if (age == undefined || age == null || age == "") {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "Select your age."
		}
		else if (parseInt(age) < 18) {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "You should be more than 18 years old to buy term insurance."
		}
		else if (parseInt(age) > 65) {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "You should be less than 65 years old to buy term insurance."
		}
		return errorObj
	}

	/*getSumAssuredError() {
		var errorObj = {}
		errorObj['isValid'] = true
		errorObj['errorMessage'] = ""

		var sumAssured = TermLifeStore.getSumAssured()
		if ((sumAssured == undefined || sumAssured == null || sumAssured == "")) {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "Please enter cover value."
		}
		return errorObj
	}*/
	getPhoneNoError() {
		var errorObj = {}
		errorObj['isValid'] = true
		errorObj['errorMessage'] = ""

		var phoneNo = TermLifeStore.getPhoneNo()
		if ((phoneNo == undefined || phoneNo == null || phoneNo == "") || !ValidationRules.mobile(phoneNo)) {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "Please enter a valid mobile number."
		}
		return errorObj
	}
	getEmailError() {
		
		var errorObj = {}
		errorObj['isValid'] = true
		errorObj['errorMessage'] = ""

		var emailId = TermLifeStore.getEmailId()
		if (!ValidationRules.email(emailId)) {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "Please enter a valid email address."
		}
		return errorObj
	}
	getTobaccoConsumptionError() {
		var errorObj = {}
		errorObj['isValid'] = true
		errorObj['errorMessage'] = ""

		var isTobaccoConsumed = TermLifeStore.getTobaccoConsumption()
		
		if (isTobaccoConsumed == undefined || isTobaccoConsumed == null) {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "Please tell us if you smoke or consume tobacco."
		}
		return errorObj
	}
	getSalaryError() {
		var errorObj = {}
		errorObj['isValid'] = true
		errorObj['errorMessage'] = ""

		var salary = TermLifeStore.getSalary()
		if ((salary == undefined || salary == null || salary == "")) {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "Please enter your salary."
		}
		return errorObj
	}
	//

	//private setters here
	//

	private _actionHandler: Function = (action: any) => {

		switch (action.actionType) {
			// add actions here

		}
		if (action.callback) {
			action.callback()
		}
	}
}

var validationStore: ValidationStore;

export = (function() {
	if (!validationStore) {
		validationStore = new ValidationStore(KEYS.PARENT)
		// API calls to initiate data, if any
	}
	return validationStore
} ())