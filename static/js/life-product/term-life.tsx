import React = require('react')
import BaseComponent = require('../common/BaseComponent')
import TransitionUtils = require('../common/utils/TransitionUtils')
import AppActions = require('./actions/AppActions')
import TermLifeStore = require('./stores/TermLifeStore')
import ValidationStore = require('./stores/ValidationStore')
import ResultStore = require('./stores/ResultStore')

import Constants = require('./constants/constants')
import MembersAge = require('./constants/MembersAge')
import YesNoObject = require('../common/constants/YesNoObject')
import TextInput = require('../common/react-component/TextInput')
import Button = require('../common/react-component/Button')
import NumberInput = require('../common/react-component/NumberInput')
import MultiSelect = require('../common/react-component/MultiSelect')
import EmailInput = require('../common/new-components/EmailInput')
import MobileInput = require('../common/new-components/MobileInput')

import GTMEvents = require('./utils/GTMEvents')
/*const PATH = '/components/car/'
const URL_PARAMS = []
const QUERY_PARAMS = []*/
const STORES = [TermLifeStore, ValidationStore, ResultStore]

interface ITermLifeState{
	//name:string,
	//sumAssured:number,
	gender:string,
	age:number,
//	dob:any,
	phoneNo:string,
	emailId:string,
	salary:number,
	isTobaccoConsumed:boolean,
	//isSubmitted:boolean,
	isShowErrors:boolean,
	buttonType:any,
	ShowErrors:string,
}

function getStateFromStores():any{
	return {
		//name:TermLifeStore.getName(),
		//sumAssured:TermLifeStore.getSumAssured(),
		gender:TermLifeStore.getGender(),
		age:TermLifeStore.getAge(),
		//dob:TermLifeStore.getDOB(),
		phoneNo:TermLifeStore.getPhoneNo(),
		emailId:TermLifeStore.getEmailId(),
		salary:TermLifeStore.getSalary(),
		isTobaccoConsumed:TermLifeStore.getTobaccoConsumption()
	}
}

class TermLife extends BaseComponent {
	refs: any
	state: ITermLifeState
	constructor(props){
		super(props, {
			stores: STORES,
			getStateFromStores: getStateFromStores,
		})

		this.state.buttonType = undefined
		this.state.ShowErrors = 'step1'
	}
	/*onNameChange(event:any){
		
		AppActions.setName(event.target.value)
	}*/
	onGenderChange(item: any) {
		AppActions.setGender(item.value)
	}
	/*onSumAssuredChange(option: any) {
		AppActions.setSumAssured(option.value)
	}*/
	
	onAgeChange(age:any){
		AppActions.setAge(age.value)
	}
	onPhoneNoChange(mobile: any) {
		AppActions.setPhoneNo(mobile)
	}
	onEmailIdChange(email: any) {
		AppActions.setEmailId(email)
	}

	onTobaccoConsumptionChange(item:any){
		AppActions.setTobaccoConsumption(item.value)
	}
	onSalaryChange(option: any) {
		AppActions.setSalary(option.value)
	}

	onSubmit() {

		var genderError = ValidationStore.getGenderError()
		var ageError = ValidationStore.getAgeError()
		var emailError = ValidationStore.getEmailError()
		var tobaccoConsumptionError = ValidationStore.getTobaccoConsumptionError()
		var salaryError = ValidationStore.getSalaryError()
		
		var phoneNoError = ValidationStore.getPhoneNoError()


		if (ValidationStore.getIsValid()) {

			this.setState({
				buttonType:'loading'
			},function(){
				AppActions.sendToLMS()
				//TransitionUtils.jumpTo('/term-insurance/quotes', '')
				AppActions.submitQuoteForm(this.postSubmit.bind(this))
				GTMEvents.viewQuotes()
				GTMEvents.viewQuotesMobileSubmit()
			})
		}else {

			if(genderError['isValid'] === true && ageError['isValid'] === true && salaryError['isValid'] === true && tobaccoConsumptionError['isValid']===true){

				this.setState({
					ShowErrors:'step2',
					isShowErrors: true
				})
			}else{
				this.setState({
					ShowErrors:'step1',
					isShowErrors: true
				})
			}
		}
	}

	postSubmit(redirectUrl:string){
		if (redirectUrl!=undefined && redirectUrl!=null){
			TransitionUtils.jumpTo(redirectUrl, '')
		}
	}

	render() {
	//	var nameError = ValidationStore.getNameError()
		var genderError = ValidationStore.getGenderError()
		//var dobError = ValidationStore.getDOBError(this.state.dob)
		var ageError = ValidationStore.getAgeError()
		//var sumAssuredError = ValidationStore.getSumAssuredError()
		var phoneNoError = ValidationStore.getPhoneNoError()
		var emailError = ValidationStore.getEmailError()
		var tobaccoConsumptionError = ValidationStore.getTobaccoConsumptionError()
		var salaryError = ValidationStore.getSalaryError()
		var errorStyle = { color: 'red' }
		return (
			<div>
				<div className="form-row first w--term_form">		
					<div className="term_gender_select">
							<MultiSelect optionsText='label' dataSource={ Constants.genderOptions } optionsValue='value' values={this.state.gender} header="Select your gender" onChange={this.onGenderChange.bind(this) } validations={['required']} placeHolder="Your Gender" labelText="Your Gender"  isSearchable={false} />
							{
								genderError['isValid'] == false && this.state.isShowErrors == true ?
								<div className="w--error"> {genderError['errorMessage']} </div>
								:
								''
							}
					</div>

					<div className="term_age_select">

							<MultiSelect optionsText='display' dataSource={ MembersAge } optionsValue='value' values={this.state.age} header="Your Age" onChange={this.onAgeChange.bind(this) } validations={['required']} placeHolder="Your Age" labelText="Your Age"  isSearchable={true} />	
							{
							ageError['isValid'] == false && this.state.isShowErrors == true ?
								<div className="w--error"> {ageError['errorMessage']} </div>
							:
							''
							}
					</div>

					<div className="term_income_select">
						<MultiSelect dataSource={Constants.salaryOptions} values={this.state.salary} 
						onChange={this.onSalaryChange.bind(this) } placeHolder="Your Annual Income" labelText="Your Annual Income" isSearchable={false} optionsText='key' optionsValue='value' header='Your Annual Income' footer="We need your Income information in order to assess your life cover"  />		
						{
						salaryError['isValid'] == false && this.state.isShowErrors == true ?
							<div  className="w--error"> {salaryError['errorMessage']} </div>
							:
							''
						}
					</div>

					<div className="term_smoking_select">
						<MultiSelect dataSource={YesNoObject} values={this.state.isTobaccoConsumed} onChange={this.onTobaccoConsumptionChange.bind(this) } labelText="Cigarette/Tobacco"   layout="horizontal" optionsText='label' optionsValue='value' isSearchable={false} header='Do you smoke or consume tobacco?' placeHolder="Cigarette/Tobacco" />
						{
							tobaccoConsumptionError['isValid'] == false && this.state.isShowErrors == true ?
							<div className="w--error"> {tobaccoConsumptionError['errorMessage']} </div>
							:
							''
						}
					</div>

				
				{
					genderError['isValid'] === true && ageError['isValid'] === true && salaryError['isValid'] === true && tobaccoConsumptionError['isValid']===true ?
					(
						<div className="term_contact_fields">	
							<div>
								<MobileInput label="MOBILE NUMBER" value={this.state.phoneNo} onChange={this.onPhoneNoChange.bind(this) } placeholder='Your Mobile Number' validations={['mobile']}/>

								{
								phoneNoError['isValid'] == false && this.state.isShowErrors == true && this.state.ShowErrors =='step2' ?
									<div className="w--error"> {phoneNoError['errorMessage']} </div>
									:
									''
								}
							</div>

							<div>
								<EmailInput value={this.state.emailId} onChange={this.onEmailIdChange.bind(this) } placeholder='Email Id (optional) ' validations={['email']} />
								{
								emailError['isValid'] == false && this.state.isShowErrors == true ?
									<div className="w--error"> {emailError['errorMessage']} </div>
									:
									''
								}
							</div>

						</div>
					):
					null
				}
				</div>
				<div className="form-row">
						<Button customClass="w--button w--button--orange w--button--large centered" onClick={this.onSubmit.bind(this) } title='Buy Now' type={ (this.state.buttonType)?'loading':'' } />
				</div>
			</div>
			)
	}
}

export = TermLife;