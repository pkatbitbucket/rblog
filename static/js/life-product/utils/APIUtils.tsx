import request = require('superagent')
//import Set = require('../../common/Set')
import CommonAPIs = require('../../common/utils/APIUtils')


declare var CSRF_TOKEN: string

class APIUtils {
	public postLifeQuoteFormData(data: any, callback?: Function) {
		request
			.post('/term-insurance/form/')
			.set('X-CSRFToken', CSRF_TOKEN)
			.set('Accept', 'application/json')
			.send(data)
			.end(function(err, res) {
				if (res && res.ok) {
					if(callback){
						callback(res.body.redirect_url)
					}
					return res.body
				}
				else {
					console.log('Err..Something went wrong...')
				}
			})
	}

	public refreshLifeQuoteFormData(data: any,activityId:string, callback?: Function) {
			
		request
			.post('/term-insurance/' + activityId + '/')
			.set('X-CSRFToken', CSRF_TOKEN)
			.set('Accept', 'application/json')
			.send(data)
			.end(function(err, res) {
				if (res && res.ok) {
					if (callback) {
						callback(res.body)
					}
					return res.body
				}
				else {
					console.log('Err..Something went wrong...')
				}
			})
	}

	public sendBuyLead(data: any, activityId: string) {
			request
			.post('/term-insurance/create-redirect-record/' + activityId + '/')
			.set('X-CSRFToken', CSRF_TOKEN)
			.set('Accept', 'application/json')
			.send(data)
			.end(function(err, res) {
				if (res && res.ok) {
				}
				else {
					console.log('Err..Something went wrong...')
				}
			})
	}
}

export = (function() {
	return  new APIUtils()
	}
 ())
