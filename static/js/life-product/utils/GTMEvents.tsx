import GTMUtils = require('./GTMUtils')
import GTMConstants = require('../constants/GTMConstants')
import ResultStore = require('../stores/ResultStore')
import TermLifeStore = require('../stores/TermLifeStore')

class GTMEvents {

	public resultsPageLoaded(){
		GTMUtils.gtmPageLoadEvents(GTMConstants.RESULTS_PAGE_LOADED.EVENT, GTMConstants.RESULTS_PAGE_LOADED.VIRTUAL_PAGE_PATH, GTMConstants.RESULTS_PAGE_LOADED.VIRTUAL_PAGE_TITLE)
	}

	public viewQuotes(){
		var emailId = TermLifeStore.getEmailId()
		var phoneNo = TermLifeStore.getPhoneNo()
		var extraParams = {
			param1: TermLifeStore.getGender(),
			param2: TermLifeStore.getAge(),
			param3: TermLifeStore.getSalary(),
			param4: TermLifeStore.getTobaccoConsumption() == true ? 'Smoker' : 'Non Smoker',
			param5: phoneNo != undefined && phoneNo != null && phoneNo != '' ? 'Mobile Number Given' : 'Mobile Number Not Given',
			param6: emailId != undefined && emailId != null && emailId!= '' ? 'Email ID Given' : 'Email ID Not Given' 

		}
		GTMUtils.gtmInteractionEvents('HomeTopTermLifeEventGetQuote', 'Homepage Term Life - Get Quote', 'Term Life Quote Viewed','', extraParams)
	}

	public viewQuotesMobileSubmit(){
		GTMUtils.gtmInteractionEvents(GTMConstants.VIEW_QUOTES_MOBILE_NUMBER_SUBMIT.EVENT, GTMConstants.VIEW_QUOTES_MOBILE_NUMBER_SUBMIT.CATEGORY, GTMConstants.VIEW_QUOTES_MOBILE_NUMBER_SUBMIT.ACTION, GTMConstants.VIEW_QUOTES_MOBILE_NUMBER_SUBMIT.LABEL)
	}

	public editQuoteForm(){
		GTMUtils.gtmInteractionEvents(GTMConstants.EDIT_QUOTE_FORM.EVENT, GTMConstants.EDIT_QUOTE_FORM.CATEGORY, GTMConstants.EDIT_QUOTE_FORM.ACTION, GTMConstants.EDIT_QUOTE_FORM.LABEL)
	}

	public editSumAssured(sumAssured:number){
		var quoteFormData = ResultStore.getQuoteFormData()
		GTMUtils.gtmInteractionEvents(GTMConstants.EDIT_SUM_ASSURED.EVENT, GTMConstants.EDIT_SUM_ASSURED.CATEGORY, GTMConstants.EDIT_SUM_ASSURED.ACTION, sumAssured)
	}

	public editMaturityAge(maturityAge:number) {
		var quoteFormData = ResultStore.getQuoteFormData()
		GTMUtils.gtmInteractionEvents(GTMConstants.EDIT_MATURITY_AGE.EVENT, GTMConstants.EDIT_MATURITY_AGE.CATEGORY, GTMConstants.EDIT_MATURITY_AGE.ACTION, maturityAge)
	}

	public onBuyClick(index:number) {
		var planList = ResultStore.getPlans()
		var currentPlan = planList[index]
		var extraParams = this.formPlanData(currentPlan, index)
		GTMUtils.gtmInteractionEvents(GTMConstants.BUY_CLICK.EVENT, GTMConstants.BUY_CLICK.CATEGORY, currentPlan.insurer.name + ' - ' + currentPlan.name ,GTMConstants.BUY_CLICK.LABEL, extraParams)
	}

	public onSeeDetailsClick(index: number) {
		var planList = ResultStore.getPlans()
		var currentPlan = planList[index]
		var extraParams = this.formPlanData(currentPlan, index)
		GTMUtils.gtmInteractionEvents(GTMConstants.SEE_DETAILS_POPUP.EVENT, GTMConstants.SEE_DETAILS_POPUP.CATEGORY, currentPlan.insurer.name + ' - ' + currentPlan.name, GTMConstants.SEE_DETAILS_POPUP.LABEL, extraParams)
	}

	public onDetailsBuyClick(index: number) {
		var planList = ResultStore.getPlans()
		var currentPlan = planList[index]
		var extraParams = this.formPlanData(currentPlan, index)
		GTMUtils.gtmInteractionEvents(GTMConstants.SEE_DETAILS_BUY_CLICK.EVENT, GTMConstants.SEE_DETAILS_BUY_CLICK.CATEGORY, currentPlan.insurer.name + ' - ' + currentPlan.name, GTMConstants.SEE_DETAILS_BUY_CLICK.LABEL, extraParams)
	}

	public onViewPlanBrochureClick(index: number) {
		var planList = ResultStore.getPlans()
		var currentPlan = planList[index]
		var extraParams = this.formPlanData(currentPlan, index)
		GTMUtils.gtmInteractionEvents(GTMConstants.VIEW_PLAN_BROCHURE.EVENT, GTMConstants.VIEW_PLAN_BROCHURE.CATEGORY, currentPlan.insurer.name + ' - ' + currentPlan.name, GTMConstants.VIEW_PLAN_BROCHURE.LABEL, extraParams)
	}

	public onCancelInsurerSiteRedirect(index: number) {
		var planList = ResultStore.getPlans()
		var currentPlan = planList[index]
		var extraParams = this.formPlanData(currentPlan, index)
		GTMUtils.gtmInteractionEvents(GTMConstants.CANCEL_INSURER_SITE_REDIRECT.EVENT, GTMConstants.CANCEL_INSURER_SITE_REDIRECT.CATEGORY, currentPlan.insurer.name + ' - ' + currentPlan.name, GTMConstants.CANCEL_INSURER_SITE_REDIRECT.LABEL, extraParams)
	}

	public onInsurerSiteRedirect(index: number) {
		var planList = ResultStore.getPlans()
		var currentPlan = planList[index]
		var extraParams = this.formPlanData(currentPlan, index)
		GTMUtils.gtmInteractionEvents(GTMConstants.REDIRECTED_TO_INSURER_SITE.EVENT, GTMConstants.REDIRECTED_TO_INSURER_SITE.CATEGORY, currentPlan.insurer.name + ' - ' + currentPlan.name, GTMConstants.REDIRECTED_TO_INSURER_SITE.LABEL, extraParams)
	}

	private formPlanData(plan:any, index:number){
		var quoteFormData = ResultStore.getQuoteFormData()
		var data = {
			param1: quoteFormData["sum_assured"],
			param2: quoteFormData["maturity_age"],
			param3: quoteFormData["age"],
			param4: quoteFormData["salary"],
			param5: quoteFormData["gender"],
			param6: quoteFormData["tobacco_user"] == true ? 'Smoker' : 'Non Smoker',
			param7: plan.premium.annual.value,
			param8: index + 1
		}
		return data
	}
}

export = new GTMEvents()