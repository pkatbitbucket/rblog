class GTMUtils {
    public gtmInteractionEvents(gtmEvent, gtmCategory, gtmAction, gtmLabel, gtmExtraParams?) {
        var data = {
            "event": gtmEvent,
            "category": gtmCategory,
            "action": gtmAction,
            "label": gtmLabel,
        }
        for (var param in gtmExtraParams) {
            data[param] = gtmExtraParams[param]
        }
        window['dataLayer'].push(data)
    }

    public gtmPageLoadEvents(gtmEvent, gtmVirtualPagePath, gtmVirtualPageTite) {
        var data = {
            "event": gtmEvent,
            "virtual_page_path": gtmVirtualPagePath,
            "virtual_page_title": gtmVirtualPageTite
        }
        window['dataLayer'].push(data)
    }
}

export = new GTMUtils()