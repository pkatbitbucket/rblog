import BaseActions = require('../../common/BaseActions')

class Action {

	receiveVehicles(value) {
		return { value: value }
	}

	receiveVariants(value) {
		return { value: value }
	}

	receiveFuelTypes(value, callback?:Function) {
		return { value: value, callback: callback }
	}

	receiveVehicleInfo(value, callback?:Function) {
		return { value: value, callback: callback }
	}

	selectVehicle(value, callback?) {
		return { value: value, callback: callback }
	}

	selectFuelType(value, callback?) {
		return { value: value, callback: callback }
	}

	selectVariant(value, callback?) {
		return { value: value, callback: callback }
	}

	clearVehicleInfo(callback?:Function) {
		return { callback: callback }
	}

	vehicleInfoFetchFailed(callback?:Function) {
		return { callback: callback }
	}

	setCNGKitValue(value?: number) {
		return { value: value }
	}

	isNewCar(callback?:Function) {
		return { callback: callback }
	}

	processViewQuotes(callback:Function) {
		return { callback: callback }
	}

	sendFastlaneFeedback() {
		return
	}

	setExpired(value: string) {
		return { value: value }
	}

	setExpiryDate(value: any) {
		return { value: value }
	}

	setNCBClaimsMade(value: string) {
		return { value: value }
	}

	setPreviousNCB(value: string | number) {
		return { value: value }
	}

	receiveRegistryInfo(value) {
		return { value: value }
	}

	receiveRTOs(value) {
		return { value: value }
	}

	selectRTO(value, callback?) {
		return { value: value, callback: callback }
	}
	
	setRegistrationNumber(regNoArray: string[]) {
		return { regNoArray: regNoArray }
	}

	fetchVehicleInfo(callback?:Function) {
		return { callback: callback }
	}

	selectYear(value, callback?) {
		return { value: value, callback: callback }
	}
}

export = BaseActions.createAction(Action)
