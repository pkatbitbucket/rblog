import React = require('react')
import moment = require('moment')
import Cookie = require('react-cookie')
import BaseComponent = require('../common/BaseComponent')
import TransitionUtils = require('../common/utils/TransitionUtils')
import GTMUtils = require('./utils/GTMUtils')

import AppActions = require('./actions/AppActions')

import VehicleStore = require('./stores/VehicleStore')
import RegistryStore = require('./stores/RegistryStore')
import CarPolicyStore = require('./stores/CarPolicyStore')

import YNS = require('./constants/YesNoSwitchable')
import RegistrationNoInputBox = require('../common/new-components/RegistrationNoInput3')
import Calendar = require('../common/react-component/Calendar')
import RadioInput = require('../common/react-component/RadioInput')
import MultiSelect = require('../common/react-component/MultiSelect')
import Button = require('../common/react-component/Button')
import MobileInput = require('../common/new-components/MobileInput')
import NumberInput = require('../common/new-components/NumberInput')
import SummaryItem = require('../common/react-component/SummaryItem')
import Tooltip = require('../common/react-component/Tooltip')

const PRODUCT_URL = '/motor/car-insurance/'

const STORES = [VehicleStore, RegistryStore, CarPolicyStore]
const MOBILE_NUMBER_COOKIE = 'mobileNo'

interface ICarState{
	regNo?: string[];
	regYear: string;
	rto: string;
	vehicles: any[];
	variants: any[];
	fuelTypes: any[];
	model: string;
	variant: string;
	modelVerbose: string;
	variantVerbose: string;
	fuelType: string;
	isExpired: number;
	expiryDate: any;
	ncbList: any[];
	isEligibleForNCB: boolean;
	ncbClaimsMade: number;
	previousNCB: number;
	fastlaneStatus: number;
	mobileNo?: string;
	cngKitValue: number;
	step: number;
}

/*
	fastlane_status:
		0: failure, jump to product page
		1: success, proceed to next step
		2: loading, wait for API response
		3: unattempted, throw error if prerequisite data not available
*/

function getStateFromStores():any{
	return {
		regNo: RegistryStore.getRegistrationNumber(),
		regYear: RegistryStore.getRegistrationYear(),
		rto: RegistryStore.getSelectedRTO(),

		vehicles:VehicleStore.getVehicles(),
		variants:VehicleStore.getVariants(),
		fuelTypes:VehicleStore.getFuelTypes(),
		model:VehicleStore.getSelectedVehicle(),
		variant:VehicleStore.getSelectedVariant(),
		modelVerbose:VehicleStore.getSelectedVehicleVerbose(),
		variantVerbose:VehicleStore.getSelectedVariantVerbose(),
		fuelType:VehicleStore.getSelectedFuelType(),
		fastlaneStatus: VehicleStore.getFastlaneSuccessStatus(),
		cngKitValue:VehicleStore.getCNGKitValue(),

		isExpired:CarPolicyStore.isExpired(),
		expiryDate:CarPolicyStore.getExpiryDate(),
		isEligibleForNCB:CarPolicyStore.isEligibleForNCB(),
		ncbClaimsMade:CarPolicyStore.isNCBClaimed(),
		ncbList:CarPolicyStore.getNCBList(),
		previousNCB:CarPolicyStore.getPreviousNCB(),

		mobileNo:Cookie.load(MOBILE_NUMBER_COOKIE)
	}
}

class Car extends BaseComponent {
	refs: any;
	state: ICarState;
	constructor(props){
		super(props, {
			stores: STORES,
			getStateFromStores: getStateFromStores
		})
		this.state.step = 1
	}
	onChangeModel(obj) {
		AppActions.selectVehicle(obj.id)
	}
	onChangeRTO(obj) {
		AppActions.selectRTO(obj.id)
	}
	onChangeVariant(obj) {
		AppActions.selectVariant(obj.id)
	}
	onChangeFuelType(obj) {
		AppActions.selectFuelType(obj.code)
	}
	onChangeExpiryDate(date: any) {
		AppActions.setExpiryDate(date)
	}
	onNCBClaimRadioChange(e) {
		AppActions.setNCBClaimsMade(e.target.value)
	}
	onChangeNCB(obj) {
		AppActions.setPreviousNCB(obj.value)
	}
	onChangeMobile = (value) => {
		this.setState({ mobileNo: value })
	}
	onValidMobile = (value) => {
		Cookie.save(MOBILE_NUMBER_COOKIE, value)
		this.setState({ mobileNo: value })
	}
	onValidRegNo(regNoArray) {
		if(this.state.regNo && this.state.regNo.join('')===regNoArray.join(''))
			return
		AppActions.setRegistrationNumber(regNoArray)
	}
	onChangeRegNo(){
		if(this.state.regNo){
			AppActions.clearVehicleInfo()
		}
	}
	onExpiryChange(e) {
		AppActions.setExpired(e.target.value)
	}
	onChangeCNGKitValue(value){
		AppActions.setCNGKitValue(value)
	}
	processStepOne() {
		if (this.state.fastlaneStatus===1) {
			this.goToStep(this.state.step + 1)
		} else if (this.state.fastlaneStatus === 0) {
			var regNumber = this.state.regNo;

			// Clear localStorage to make a fresh entry except registration number
			AppActions.clearVehicleInfo()
			AppActions.setRegistrationNumber(regNumber)

			TransitionUtils.jumpTo(PRODUCT_URL)
		} else if (this.state.regNo) {
			AppActions.fetchVehicleInfo(this.next.bind(this))
			this.setState({ fastlaneStatus: 2 })
		} else {
			this.refs.regNo.isValid(true)
		}
	}
	processStepTwo(){
		let isValid = this.refs.model.isValid(true) && this.refs.fuel.isValid(true) && this.refs.variant.isValid(true)
			&& ((this.state.fuelType === 'cng_lpg_external' && this.refs.cngKitValue.isValid(true))
			|| this.state.fuelType !== 'cng_lpg_external')

		if (isValid){
			AppActions.sendFastlaneFeedback()
			this.goToStep(this.state.step + 1)
		}
	}
	processStepThree(){
		if (typeof this.state.isExpired !== 'undefined'){
			let count = this.state.isEligibleForNCB ? 1 : 2
			this.goToStep(this.state.step + count)
		}else{
			this.refs.expiredRadio.showErrors()
		}
	}
	processStepFour(){
		let isValid = typeof this.state.ncbClaimsMade !== 'undefined'
			&& (this.state.ncbClaimsMade===1
			|| (this.state.ncbClaimsMade === 0 && typeof this.state.previousNCB !== 'undefined'))

		if (isValid) {
			this.goToStep(this.state.step + 1)
		}else{
			if(typeof this.state.ncbClaimsMade === 'undefined')
				this.refs.ncbRadio.showErrors()
			else
				this.refs.ncbSelect.isValid(true)
		}
	}
	processStepFive(){
		let isValid = (this.state.mobileNo || (!this.state.mobileNo && !this.refs.mobileNo.getValue()))

		// Mobile number should be mandatory for Team-BHP-widget
		if(this.props.isMobileMandatory && (!this.state.mobileNo || this.state.mobileNo.length < 10)){
			this.refs.mobileNo.isValid(true);
			isValid = false;
		}

		if(isValid){
			AppActions.processViewQuotes(function(){
				TransitionUtils.jumpTo(PRODUCT_URL, 'results')
			})
		}else{
			this.refs.mobileNo.isValid(true)
		}
	}
	processGTM(step: number) {
		if (this.state.step > step) {
			GTMUtils.fsEditClick(step)
			return
		}

		let data = VehicleStore.getGTMData()
		switch (step) {
			case 2:
				GTMUtils.fsStepOne()
				break;
			case 3:
				GTMUtils.fsStepTwo(data.changed, data.model, data.fuel, data.variant, data.rto, data.regyear)
				break;
			case 4:
				GTMUtils.fsStepThree(data.changed, data.model, data.fuel, data.variant, data.rto, data.regyear, data.expirydate)
				break;
			case 5:
				GTMUtils.fsStepFour(data.changed, data.model, data.fuel, data.variant, data.rto, data.regyear, data.expirydate, data.ncb)
				break;
		}
	}
	goToProduct(isNew:boolean){
		let redirect = TransitionUtils.jumpTo.bind(null, PRODUCT_URL)
		isNew ? AppActions.isNewCar(redirect) : AppActions.clearVehicleInfo(redirect)
		GTMUtils.unknownRegistrationNumber(isNew)
	}
	goToStep(index:number){
		if(index<6 && index>0){
			this.processGTM(index)
			this.setState({ step: index })
		}
	}
	prev(){
		this.goToStep(this.state.step - 1)
	}
	next(){
		switch (this.state.step) {
			case 1:	this.processStepOne()
					break
			case 2:	this.processStepTwo()
					break
			case 3:	this.processStepThree()
					break
			case 4:	this.processStepFour()
					break
			case 5: this.processStepFive()
					break
		}
	}
	getButtonText():string{
		let str = 'Continue'
		if(this.state.step===2)
			str = 'Confirm'
		else if (this.state.step===5)
			str = 'View Quotes'
		return str
	}
	getSummary():any{
		let summary = []
		if(this.state.step > 1){
			summary.push(
				<SummaryItem key="st1" label="Registration Number" value={this.state.regNo.join('-')} onClick={this.goToStep.bind(this, 1)}/>
			)
		}
		if(this.state.step > 2){
			let carModelValue = <div className='car_model_summary'>{this.state.modelVerbose}<br/><span className='variant'>{this.state.variantVerbose}</span></div>
			summary.push(
				<SummaryItem key="st2" label="Your Car" value={carModelValue} onClick={this.goToStep.bind(this, 2)}/>
			)
		}
		if(this.state.step > 3){
			let expiryValue = this.state.isExpired? this.state.expiryDate.format('DD MMM YYYY'):'Policy not expired'
			summary.push(
				<SummaryItem key="st3" label="Policy Expired" value={expiryValue} onClick={this.goToStep.bind(this, 3)}/>
			)
		}
		if(this.state.step > 4 && this.state.isEligibleForNCB){
			let ncb_unknown:any = 'U'
			let ncbValue = 'Not Applicable'
			if(this.state.ncbClaimsMade===0)
				ncbValue = this.state.previousNCB === ncb_unknown ? this.state.ncbList[this.state.ncbList.length-1]['key'] : this.state.previousNCB + '%'
			summary.push(
				<SummaryItem key="st4" label="NCB" value={ncbValue} onClick={this.goToStep.bind(this, 4)}/>
			)
		}
		return summary
	}
	getForm():any{
		let formItem
		if(this.state.step===1){
			let regTT = <span>Enter your car's registration number <br/> for e.g. MH 02 BX 0377.</span>
			formItem = (<div className='reg_no_wrapper'>
							<RegistrationNoInputBox ref="regNo" disabled={this.state.fastlaneStatus === 2} onChange={this.onChangeRegNo.bind(this) } onValid={this.onValidRegNo.bind(this) } defaultValue={this.state.regNo} onEnterPress={this.next.bind(this) } placeholder="Enter your car number" errorRegNoMsg="Oh. Looks like that car number is not valid. Check again?" inCompleteRegNoMsg="Oh, we can’t proceed without your car number. Do your bit?" labelText="Your car number"/>
			</div>)

		} else if (this.state.step === 2) {
			formItem = (<div>
				<div className="car_selector_header"><p className='flg'>We have fetched your car details based on your registration number. <br/> <strong>Please confirm.</strong> </p></div>
					<div className="car_selector_row">
						<MultiSelect customClass='car_selector_dd' ref="model" labelText="Your Car" placeHolder="Select your car" optionsText="name" optionsValue="id" dataSource={this.state.vehicles} values={this.state.model} onChange={this.onChangeModel.bind(this) } validations={[{name:'required',message:'Car Model is mandatory to generate quotes'}]} groupBy="type" />
						<MultiSelect customClass='car_selector_dd' ref="fuel" labelText="Fuel Type" placeHolder="Select fuel type" optionsText="name" optionsValue="code" dataSource={this.state.fuelTypes} values={this.state.fuelType} onChange={this.onChangeFuelType.bind(this) } validations={[{name:'required',message:'Fuel Type is mandatory to generate quotes'}]} />
						<MultiSelect customClass='car_selector_dd' ref="variant" labelText="Variant" placeHolder="Select variant" optionsText="name" optionsValue="id" dataSource={this.state.variants} values={this.state.variant} onChange={this.onChangeVariant.bind(this) } validations={[{name:'required',message:'Car Variant is mandatory to generate quotes.'}]} />
					</div>
					{this.state.fuelType === 'cng_lpg_external' ? <div className="cng_lpg_input">
						<NumberInput ref="cngKitValue" label="CNG KIT VALUE" placeholder="Value of CNG kit fitted in your car" onChange={this.onChangeCNGKitValue} value={this.state.cngKitValue} validations={[{ name: 'required', message: 'Oh, we can’t proceed without knowing the CNG kit value. Do your bit?' }, { name: 'minValue', message: 'CNG kit value must be greater than zero.', minValue: 1 }, { name: 'maxValue', message: 'CNG kit value cannot be greater than 40000.', maxValue: 40000 }]} />
					</div> : null}
				</div>)

		}else if(this.state.step===3){
			formItem = (<div>
					<div className="claims_selector_row">
						<p className='flg'>Has your car policy already expired?</p>
						<RadioInput ref="expiredRadio" data={YNS} onChange={this.onExpiryChange.bind(this) } selectedValue={this.state.isExpired} />
					</div>
					{this.state.isExpired ? <div className='expiry_date_selector_row'>
						<Calendar
							maxDate={moment().add(-1, 'day') }
							minDate={moment().add(-2, 'year').add(-1, 'day')}
							labelText='Policy Expiry Date'
							placeHolder='Choose Expiry Date'
							prevMonths={1}
							nexMonths={0}
							defaultDate={this.state.expiryDate}
							onSelectDate={this.onChangeExpiryDate.bind(this)}
							buttonText='EDIT'/>
					</div> : null}
				</div>)
		}else if(this.state.step===4){
			let ncbTT = "No claim bonus (NCB) is the discount you get for every claim free year. Please check the accurate value in your previous policy, to get right discount and to avoid claim rejection."
			formItem = (<div>
					<div className="claims_selector_row">
						<p><strong>One last question.</strong></p>
						<p className='flg'>Have you made a claim in the previous year?</p>
						<RadioInput ref="ncbRadio" data={YNS} onChange={this.onNCBClaimRadioChange.bind(this) } selectedValue={this.state.ncbClaimsMade} />
					</div>
					{0===this.state.ncbClaimsMade ? <div className="ncb_selector_row"><p className='flg'>Previous No Claim Bonus (NCB)</p>
						<div className='ncb_selector_wrapper'>
							<Tooltip value={ncbTT}/>
							<MultiSelect ref="ncbSelect" customClass='ncb_dropdown' labelText="Previous NCB" isSearchable={false} optionsText="key" optionsValue="value" dataSource={this.state.ncbList} values={this.state.previousNCB} onChange={this.onChangeNCB.bind(this) } validations={[{name:'required',message:'No Claim Bonus (NCB) is mandatory to generate quotes'}]} />
						</div>
					</div> : ''}
				</div>)
		}else if(this.state.step===5){
			formItem = (<div>
					<p className='flg'>You could share your Mobile Number so that we can send you policy related information</p>
					<div className='mobile_number_input_wrapper'>
						<MobileInput	ref="mobileNo"
										label="MOBILE NUMBER"
										placeholder="Your mobile number"
										onChange={this.onChangeMobile}
										onValid={this.onValidMobile}
										value={this.state.mobileNo}
										validations={[{ name: 'mobile', message: 'Oh. Looks like that mobile number is not valid. Check again?' }, {name:'required', message:"Oh, We can't proceed without knowing your contact number. Do your bit?"}]} />
					</div>
				</div>)
		}
		return formItem
	}
	render() {
		let summary = this.getSummary()
		let formItem = this.getForm()
		let iDontKnow
		if (this.state.step === 1){
			iDontKnow  = <div>
					<Button customClass= 'w--button--orange w--button--empty secondary_button centered' title= { "I don't know my registration number"} onClick= { this.goToProduct.bind(this, false) } />
					<Button customClass= 'w--button--orange w--button--empty secondary_button centered' title= { "I have a new car"} onClick= { this.goToProduct.bind(this, true) } />
				</div>
		}

		let loading = this.state.fastlaneStatus === 2
		return (
			<div className='w--car row'>
				{this.state.step == 1? <div className='w--car-header col-md-12'>
					<h5>Compare and buy car insurance online</h5>

				</div>:null}
				{summary ? <div className='w--car_summary col-md-4 col-sm-4'>
					{summary}
				</div>:null}
				<div className={'w--car_form ' + (this.state.step == 1? 'col-sm-12 centered':'col-sm-8 boxed')}>
					{formItem}
	                <Button customClass={'w--button--orange w--button--large ' + (this.state.step == 1? 'centered':'')} type={loading ? 'loading' : ''} disabled={loading} title={this.getButtonText() } onClick={this.next.bind(this) }/>
	                {iDontKnow}
				</div>
            </div>
		)
	}
}

export = Car;