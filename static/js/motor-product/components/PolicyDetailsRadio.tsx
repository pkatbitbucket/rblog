import React = require('react')
import YNS = require('../constants/YesNoSwitchable')
import RadioInput = require('../../common/new-components/RadioInput')

interface IProps{
	isExpired: number;
	isEligibleForNCB: boolean;
	ncbClaimsMade: number;
	tDate: string;
	onChange: Function;
	handleClose: Function;
	ref: any;
}

class PolicyDetailsRadio extends React.Component<IProps, any> {
	refs:any
	constructor(props: IProps){
		super(props)
		this.state = {
			isValid:true, 
			validationMsg: ''
		}
		this.state.index = props.isEligibleForNCB == undefined || props.isExpired == undefined ? null : (props.isExpired ? (props.isEligibleForNCB ? 2 : 1) : 0)
	}
	componentWillReceiveProps(nextProps){
		this.state.index = nextProps.isEligibleForNCB == undefined || nextProps.isExpired == undefined ? null : (nextProps.isExpired ? (nextProps.isEligibleForNCB ? 2 : 1): 0)
	}
	handleChangeNCB = (value) => {
		this.setState({
			valid:true,
			validationMsg:''
		})
		this.props.onChange({
			ncbClaimsMade: value
		})
		this.props.handleClose()
	}
	handleChangeExpiry(index){
		this.setState({
			index: index,
			valid:true,
			validationMsg:''
		},function(){
			this.props.onChange({
				isExpired: index === 0 ? false : true,
				isEligibleForNCB: index !== 1 ? true : false
			})	
		})

		if(index==1)
			this.props.handleClose()
	}
	getNCBJSX(){
		return <div className='car_policy_details__ncbitem'>
				<RadioInput ref="ClaimedInput" labelText='Have you made a claim in the previous year?' optionsList={YNS} 
				optionsKey="text" validations={[{ name: 'required', message: 'Select whether you made a claim last year, please?'}]} value={this.props.ncbClaimsMade} onChange={this.handleChangeNCB}/>
			</div>
	}
	isValid(isShowErrors:boolean = false){
		var isValid = true
		var validationMsg = ""
		if(this.state.index == undefined){
			var isValid = false
			validationMsg = "Please select an option"
		}
		else if(this.state.index != undefined && this.state.index!=1 && !this.refs['ClaimedInput'].isValid(true)){
			var isValid = false
			validationMsg = ""
		}

		if(isShowErrors){
			this.setState({
				isValid: isValid,
				validationMsg: validationMsg
			})
		}
		return isValid
	}

	isValidExpiry(isShowErrors:boolean = false){
		var isValid = true
		var validationMsg = ""
		if(this.state.index == undefined){
			var isValid = false
			validationMsg = "Please select an option"
		}

		if(isShowErrors){
			this.setState({
				isValid: isValid,
				validationMsg: validationMsg
			})
		}
		return isValid
	}

	isValidNCBClaim(isShowErrors:boolean = false){
		var isValid = true
		var validationMsg = ""
		if(this.state.index != undefined && this.state.index!=1 && !this.refs['ClaimedInput'].isValid(isShowErrors)){
			var isValid = false
			validationMsg = ""
		}

		if(isShowErrors){
			this.setState({
				isValid: isValid,
				validationMsg: validationMsg
			})
		}
		return isValid
	}

	render() {
		let ncbJSX = this.props.isEligibleForNCB ? this.getNCBJSX() : null
		let selectedJSX = <span><strong>o</strong></span>
		let unselectedJSX = <span>o</span>
		return (
			<div className='car_policy_details'>
				<div className={'car_policy_details__item ' + (this.state.index === 0 ? 'selected' : '') } 
					onClick={this.handleChangeExpiry.bind(this, 0) }>{"Not yet expired"}</div>
				{this.state.index === 0 ? ncbJSX : null}
				<div className={'car_policy_details__item ' + (this.state.index === 1 ? 'selected' : '') } 
					onClick={this.handleChangeExpiry.bind(this, 1) }>{" Expired on or before " + this.props.tDate}</div>
				<div className={'car_policy_details__item ' + (this.state.index === 2 ? 'selected' : '') } 
					onClick={this.handleChangeExpiry.bind(this, 2) }>{" Expired after " + this.props.tDate}</div>
				{this.state.index === 2 ? ncbJSX : null}
				
				{
					this.state.isValid == false ?
					<div className={'error-label'}>{this.state.validationMsg}</div>
					: null
				}
			</div>
		)
	}
}

export = PolicyDetailsRadio