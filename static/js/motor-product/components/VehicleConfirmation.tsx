import React = require('react')
import Cookie = require('react-cookie')
import BaseComponent = require('../../common/BaseComponent')
import TransitionUtils = require('../../common/utils/TransitionUtils')
import URLS = require('../../common/constants/URLS')

import GTMUtils = require('../utils/GTMUtils')
import AppActions = require('../actions/AppActions')
import VehicleStore = require('../stores/VehicleStore')
import RegistryStore = require('../stores/RegistryStore')
import CarPolicyStore = require('../stores/CarPolicyStore')

import LRCard = require('../../common/new-components/LRCard')
import MultiSelect = require('../../common/react-component/MultiSelect')
import Button = require('../../common/react-component/Button')
import MobileInput = require('../../common/new-components/MobileInput')
import NumberInput = require('../../common/new-components/NumberInput')

interface IProps{
	showContactField?: boolean;
	isContactMandatory?: boolean;
}

interface IState{
	regNo?: string[];
	vehicles?: any[];
	fuelTypes?: any[];
	vehicleVariants?: any[];
	selectedFuelType?: string | number;
	selectedModel?: string | number;
	selectedVariant?: string | number;
	cngKitValue?: string | number;
	isExpired?: boolean;
	isEligibleForNCB?: boolean;
	ncbClaimsMade?: boolean;
	mobileNo?: string | number;
}

const STORES = [VehicleStore/*, RegistryStore, CarPolicyStore*/]
const MOBILE_NUMBER_COOKIE = 'mobileNo'

function getStateFromStores(){
	return {
		regNo: RegistryStore.getRegistrationNumber(),

		vehicles: VehicleStore.getVehicles(),
		fuelTypes: VehicleStore.getFuelTypes(),
		vehicleVariants: VehicleStore.getVariants(),
		selectedModel: VehicleStore.getSelectedVehicle(),
		selectedFuelType: VehicleStore.getSelectedFuelType(),
		selectedVariant: VehicleStore.getSelectedVariant(),
		cngKitValue: VehicleStore.getCNGKitValue(),

		isExpired: CarPolicyStore.isExpired(),
		isEligibleForNCB:CarPolicyStore.isEligibleForNCB(),
		ncbClaimsMade: CarPolicyStore.isNCBClaimed()
	}
}

class VehicleConfirmation extends BaseComponent {
	refs: any;
	props: IProps;
	state: IState;
	static defaultProps = {
		showContactField: true,
		isContactMandatory: false
	}
	constructor(props){
		super(props, {
			stores: STORES,
			getStateFromStores: getStateFromStores
		})
		this.state.mobileNo = Cookie.load(MOBILE_NUMBER_COOKIE)
	}
	componentDidMount(){
		if(!this.state.selectedVariant){
			this.refs.vehicleVariant.openDropDown()
		}
	}
	is(val: any): string{
		return val ? 'Yes' : 'No'
	}
	goToResults(){
		GTMUtils.successfulFastlaneSubmitHome()
		AppActions.processViewQuotes(TransitionUtils.jumpTo.bind(null, URLS.CAR_RESULTS))
	}
	onChangeModel(obj){
		AppActions.selectVehicle(obj.id)
	}
	onChangeFuelType(obj){
		AppActions.selectFuelType(obj.code)
	}
	onChangeVariant(obj){
		AppActions.selectVariant(obj.id)
	}
	onChangeCNGKitValue = (value) => {
		AppActions.setCNGKitValue(value)
	}
	onChangeMobile = (value) => {
		this.setState({ mobileNo: value })
	}
	onValidMobile = (value) => {
		Cookie.save(MOBILE_NUMBER_COOKIE, value)
		this.setState({ mobileNo: value })
	}
	isValid = (showErrors:boolean = false) => {
		var isValid = this.refs.model.isValid(showErrors) && this.refs.fuel.isValid(showErrors) && this.refs.vehicleVariant.isValid(showErrors)
		if (isValid && this.refs.cngKitValue)
			isValid = this.refs.cngKitValue.isValid(showErrors)
		if (isValid && this.refs.mobileNo)
			isValid = this.refs.mobileNo.isValid(showErrors)
		return isValid
	}
	handleBuyNow = () => {
		AppActions.sendFastlaneFeedback()
		this.isValid(true) ? this.goToResults() : null
	}

	render() {
		var left = <div className="car_info">
			<span className="car_info__label">YOUR CAR NUMBER</span>
			<span className="car_info__data">{this.state.regNo ? this.state.regNo.join('').toUpperCase() : null}</span>
		</div>
		var right = <div className="car_info">
			<span className="car_info__label">PREVIOUS POLICY</span>
			<span className="car_info__data">Expired: {this.is(this.state.isExpired)} {this.state.isEligibleForNCB ? '| Claimed: ' + this.is(this.state.ncbClaimsMade) : null}</span>
		</div>

		return (
			<div className="form-row first car_confirmation">
				<div className="car_form_header">
					<h2>Did we get your car details right?</h2>
					<span>We fetched the car details based on your car number.</span>
					<span>Just check if everything is alright.</span>
				</div>
				<div className="car_prefilled_data">
					{left}
					{right}
				</div>
				<div>
					<div className="car_multiselect_group">
						<MultiSelect customClass='car_name_dropdown' ref="model" labelText="YOUR CAR" placeHolder="Select your car" optionsText="name" optionsValue="id" header="Select car make and model" dataSource={this.state.vehicles} values={this.state.selectedModel} onChange={this.onChangeModel} validations={[{ name: 'required', message: 'Oh, we can’t proceed without knowing your car model. Do your bit?' }]} groupBy="type" />
						<MultiSelect customClass='car_fuel_dropdown' ref="fuel" labelText="CAR FUEL TYPE" placeHolder="Select fuel type" optionsText="name" optionsValue="code" optionsLabel="label" header="Select fuel type" isSearchable={false} dataSource={this.state.fuelTypes} values={this.state.selectedFuelType} onChange={this.onChangeFuelType} validations={[{ name: 'required', message: 'Oh, we can’t proceed without knowing the fuel type in your car. Do your bit?' }]} />
						<MultiSelect customClass='car_variant_dropdown' ref="vehicleVariant" labelText="CAR VARIANT" placeHolder="Select variant" optionsText="name" optionsValue="id" header="Select car variant" dataSource={this.state.vehicleVariants} values={this.state.selectedVariant} onChange={this.onChangeVariant} isSearchable={this.state.vehicleVariants && this.state.vehicleVariants.length >= 15} validations={[{ name: 'required', message: 'Oh, we can’t proceed without knowing your car variant. Do your bit?' }]} />
					</div>
					<div className="input_field_wrapper">
						{this.state.selectedFuelType === 'cng_lpg_external' ? <div>
							<NumberInput ref="cngKitValue" label="CNG KIT VALUE" placeholder="Value of CNG kit fitted in your car" onChange={this.onChangeCNGKitValue} value={this.state.cngKitValue} validations={[{ name: 'required', message: 'Oh, we can’t proceed without knowing the CNG kit value. Do your bit?' }, { name: 'minValue', message: 'CNG kit value must be greater than zero.', minValue: 1 }, { name: 'maxValue', message: 'CNG kit value cannot be greater than 40000.', maxValue: 40000 }]} />
						</div> : null}
						{this.props.showContactField ? <div>
							<MobileInput ref="mobileNo" label="MOBILE NUMBER" placeholder={'Your mobile number' + (this.props.isContactMandatory ? '' : ' (optional)') } onChange={this.onChangeMobile} onValid={this.onValidMobile} value={this.state.mobileNo} validations={this.props.isContactMandatory ? [{ name: 'mobile', message: 'Oh. Looks like that mobile number is not valid. Check again?' }, 'required'] : [{ name: 'mobile', message: 'Oh. Looks like that mobile number is not valid. Check again?' }]} />
						</div> : null}
					</div>
				</div>
				<div className='form-row'>
					<span className="status_label">Everything looks good?</span>
					<div className='w--button_group'>
						<Button customClass={'w--button--orange w--button--large'} title='Buy Now' onClick={this.handleBuyNow}/>
					</div>
				</div>
			</div>
		)
	}
}

export = VehicleConfirmation