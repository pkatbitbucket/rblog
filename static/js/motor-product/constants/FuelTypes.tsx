export = [{
			'id': 'PETROL',
			'name': 'Petrol',
			'code': 'petrol',
			'label': 'Petrol | CNG/LPG: No'
		},
		{
			'id': 'DIESEL',
			'name': 'Diesel',
			'code': 'diesel',
			'label': 'Diesel'
		},
		{
			'id': 'INTERNAL_LPG_CNG',
			'name': 'CNG/LPG company fitted',
			'code': 'cng_lpg_internal',
			'label': 'CNG/LPG Internal'
		},
		{
			'id': 'PETROL',
			'name': 'CNG/LPG externally fitted',
			'code': 'cng_lpg_external',
			'label': 'CNG/LPG External'
		}];