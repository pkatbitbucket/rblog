import React = require('react')
import moment = require('moment')
import BaseComponent = require('../common/BaseComponent')
import TransitionUtils = require('../common/utils/TransitionUtils')

import GTMUtils = require('./utils/GTMUtils')
import AppActions = require('./actions/AppActions')
import VehicleStore = require('./stores/VehicleStore')
import RegistryStore = require('./stores/RegistryStore')
import CarPolicyStore = require('./stores/CarPolicyStore')

import PolicyDetailsRadio = require('./components/PolicyDetailsRadio')
import VehicleConfimation = require('./components/VehicleConfirmation')
import YNS = require('./constants/YesNoSwitchable')
import RegistrationNoInputBox = require('../common/new-components/RegistrationNoInput3')
import Button = require('../common/react-component/Button')
import Modal = require('../common/new-components/Modal')
import stashable = require('../common/utils/stashable')
import URLS = require('../common/constants/URLS')

const STORES = [VehicleStore, RegistryStore, CarPolicyStore]

interface ICarState{
	regNo?: string[];
	isExpired: number;
	isEligibleForNCB: boolean;
	ncbClaimsMade: number;
	fastlaneStatus: number;
	showPDR?: boolean;
	regErrorVisible?: boolean;
	isValid?: boolean;
	expiryValidationMsg?: string;
	showConfirmation?: boolean;
}

function getStateFromStores():any{
	return {
		regNo: RegistryStore.getRegistrationNumber(),		
		fastlaneStatus: VehicleStore.getFastlaneSuccessStatus(),
		isExpired:CarPolicyStore.isExpired(),
		isEligibleForNCB:CarPolicyStore.isEligibleForNCB(),
		ncbClaimsMade:CarPolicyStore.isNCBClaimed(),
	}
}

class Car extends BaseComponent {
	refs: any;
	state: ICarState;
	constructor(props){
		super(props, {
			stores: STORES,
			getStateFromStores: getStateFromStores
		})
		this.state.showPDR = false
		this.state.showConfirmation = false
	}
	componentDidMount(){
		stashable.show()
	}
	componentDidUpdate(prevProps, prevState) {
		if (prevState.fastlaneStatus === 2 && this.state.fastlaneStatus < 2)
			setTimeout(this.handleBuyNow.bind(this), 0)
	}
	onValidRegNo = (regNoArray) => {
		if(this.state.regNo && this.state.regNo.join('')===regNoArray.join(''))
			return
		AppActions.setRegistrationNumber(regNoArray)
	}
	onChangeRegNo = () => {
		// handling UI part of moving error msg above dont know your bike number
		var regErrorVisible = this.refs.regNo.showError()
		this.setState({
			regErrorVisible: regErrorVisible
		})
		if(this.state.regNo)
			AppActions.clearVehicleInfo()
	}
	togglePDR = () => {
		this.setState({showPDR : !this.state.showPDR})
	}
	toggleConfirmation = () => {
		this.setState({showConfirmation : !this.state.showConfirmation})
	}
	closePDR = () => {
		this.setState({showPDR : false})
	}
	openPDR = () => {
		this.setState({showPDR: true})
	}
	handlePDRChange = (obj: any) => {
		this.setState({
			expiryValidationMsg:''
		})

		if(obj.ncbClaimsMade !== undefined){
			AppActions.setNCBClaimsMade(obj.ncbClaimsMade)
			AppActions.setPreviousNCB(obj.ncbClaimsMade ? 0 : 'U')
		}else{
			let date = !obj.isExpired ? moment().add(7, 'day') : (obj.isEligibleForNCB ? moment().add(-1, 'day') : moment().add(-92, 'day'))
			AppActions.setExpired(obj.isExpired ? '1' : '0')
			AppActions.setExpiryDate(date)
		}
	}
	handleBuyNow(){
		if(!this.isValid(true))
			return

		if (this.state.fastlaneStatus === 1) {
			this.toggleConfirmation()
		} else if (this.state.fastlaneStatus === 0) {
			GTMUtils.fastlaneFailedHome()
			TransitionUtils.jumpTo(URLS.CAR_PRODUCT)
		} else if (this.state.regNo && this.state.isExpired != undefined) {
			this.setState({ fastlaneStatus: 2 }, AppActions.fetchVehicleInfo)
		}
	}

	isValid(isShowError:boolean = true){
		var expiryValidationMsg = ""
		var isValidRegNo = this.refs.regNo.isValid(true) 
		var isValidExpiry = this.refs['PolicyExpirySelector'].isValidExpiry(false)
		var isValidNCBClaim = true

		if(isValidRegNo && isValidExpiry == true)
			 isValidNCBClaim = this.refs['PolicyExpirySelector'].isValidNCBClaim(true)
		else if(isValidRegNo && isValidExpiry == false)
			expiryValidationMsg = 'Select whether your policy has expired, please?'

		var isValid = isValidRegNo && isValidExpiry && isValidNCBClaim

		// handling UI part of moving error msg above dont know your car number
		var regErrorVisible = !isValidRegNo

		if(isShowError){
			this.setState({
				regErrorVisible:regErrorVisible,
				isValid: isValid,
				expiryValidationMsg:expiryValidationMsg,
				showPDR: !isValidNCBClaim
			})
		}
		return isValid
	}
	goToResults(){
		GTMUtils.successfulFastlaneSubmitHome()
		AppActions.processViewQuotes(TransitionUtils.jumpTo.bind(null, URLS.CAR_RESULTS))
	}
	goToProduct(isNew:boolean){
		GTMUtils.unknownRegistrationNumberHome(isNew)

		let redirect = TransitionUtils.jumpTo.bind(null, URLS.CAR_PRODUCT)
		isNew ? AppActions.isNewCar(redirect) : AppActions.clearVehicleInfo(redirect)
	}
	render() {
		let loading = this.state.fastlaneStatus === 2
		return (
			<div>
				<div className='form-row first'>
					<RegistrationNoInputBox customClass='w--reg_no_input_minimal' ref="regNo" onValid={this.onValidRegNo} onChange={this.onChangeRegNo.bind(this)} 
						defaultValue={this.state.regNo} placeholder="Enter your car number" labelText="Your car number" disabled={loading}
						errorRegNoMsg="Oh. Looks like that car number is not valid. Check again?" inCompleteRegNoMsg="Oh, we can’t proceed without your car number. Do your bit?"/>
					<div className='car_expired_dropdown'>
						<div className='car_expired_dropdown__handle' onClick={loading ? null : this.openPDR}>
						{this.state.isExpired == undefined ? <div className='placeholder'>Has your previous policy expired?</div> : 
							<div>
								<div className="label_minimised">Previous Policy</div>
								<span>{this.state.isExpired ? "Expired: Yes" : "Expired: No"}</span>
									{this.state.ncbClaimsMade != undefined ? 
										<span>{<span className='muted'> | </span>}{this.state.ncbClaimsMade ? "Claimed: Yes" : "Claimed: No"}</span> 
										: null}
							</div>
						}
						<div className='car_expired_dropdown__arrow'></div>
						</div>
						{!this.state.showConfirmation ? <Modal type='popup' isOpen={this.state.showPDR}  onClose={this.closePDR} isCloseOnOutSideClick={true} isPreventScrolling={false}
							footer="Stress is for larger problems, not expired policies. Let us help you renew online instantly." customFooterClass="modal_footer">
							<p className='popup-header'>Your Previous Policy</p>
							<PolicyDetailsRadio ref="PolicyExpirySelector" onChange={this.handlePDRChange} isExpired={this.state.isExpired} isEligibleForNCB={this.state.isEligibleForNCB} 
								ncbClaimsMade={this.state.ncbClaimsMade} tDate={moment().add(-90, 'day').format('DD MMM YYYY') }
								handleClose={this.closePDR}/>
						</Modal> : null}
						<div className='fs w--error'>
							{!this.state.isValid && this.state.expiryValidationMsg ?
								this.state.expiryValidationMsg
								: null
							}
						</div>
					</div>
					
					<div className={'fallback_links '  + (this.state.regErrorVisible? 'push_down':'')}>
						<Button customClass={'w--button--link'} onClick={ this.goToProduct.bind(this, false) } title="Not sure of your car number?" />
						<Button customClass={'w--button--link'} onClick={ this.goToProduct.bind(this, true) } title="Bought a new car?" />
					</div>
				</div>

				<div className='form-row'>
					<div className='w--button_group'>
						<Button customClass={'w--button--orange w--button--large'} title='Buy Now' onClick={this.handleBuyNow.bind(this) } disabled={loading} type={this.state.fastlaneStatus === 2 ? 'loading' : ''}/>
					</div>
				</div>

				{this.state.showConfirmation ? <Modal customClass='full_bg' customParentClass='modal--opaque_background' isOpen={this.state.showConfirmation} onClose={this.toggleConfirmation} isPreventScrolling={true} isShowCloseIcon={true} isCloseOnOutSideClick={false} isCloseOnEscape={false}>
					<VehicleConfimation/>
				</Modal> : null}
			</div>
		)
	}
}

export = Car