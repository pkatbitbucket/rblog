import moment = require('moment')
import BaseStore = require('../../common/BaseStore')
import MONTHS = require('../../common/constants/Months')
import AppActions = require('../actions/AppActions')
import APIUtils = require('../utils/APIUtils')
import StorageMap = require('../constants/StorageMap')
import NCBConfig = require('../constants/NCBConfig')
import RegistryStore = require('./RegistryStore')

declare var isNaN: {
	(value: string | number):boolean
}

var KEYS = StorageMap.FOUR_WHEELER

class CarPolicyStore extends BaseStore{
	constructor(_namespace:string){
		super(_namespace)
		this.registerDispatcherHandler(this._actionHandler)
	}

	getExpiryDate(): any{
		let DD = this.getSelected(KEYS.EXPIRY_DATE)
		let MMMM = this.getSelected(KEYS.EXPIRY_MONTH)
		let YYYY = this.getSelected(KEYS.EXPIRY_YEAR)
		return DD && MMMM && YYYY ? moment(("0"+DD).slice(-2)+'-'+MMMM+'-'+YYYY, 'DD-MMMM-YYYY') : moment()
	}
	isExpired(): string{
		return this.getSelected(KEYS.POLICY_EXPIRED)
	}
	isNCBClaimed(): string{
		return this.getSelected(KEYS.IS_CLAIMED_LAST_YEAR)
	}
	isEligibleForNCB(): boolean{
		let isExpired = this.isExpired() || false
		let expiryDate = this.getExpiryDate()
		return !isExpired || (isExpired && this.isNotNinetyDaysOld(expiryDate))
	}
	getNCBList(): any[]{
		return NCBConfig.NCBOptions
	}
	getPreviousNCB(): number{
		let ncb = this.getSelected(KEYS.NCB_UNKNOWN)
		return ncb ? 'U' : this.getSelected(KEYS.PREVIOUS_NCB)
	}

	private init(preventEmit?:boolean){
		let obj = {}
		if (!this.getSelected(KEYS.POLICY_EXPIRED))
			this.setPolicyExpiry('0', true)
		if (!this.getSelected(KEYS.IS_CLAIMED_LAST_YEAR))
			this.setNCBClaimed('0', true)
		this.setSelected(obj, preventEmit)
	}
	private isNotNinetyDaysOld(date: any):boolean{
		return moment().diff(date, 'days')<90
	}
	private setPolicyExpiry(flag: string, preventEmit?: boolean){
		let isExpired = parseInt(flag)
		if(isExpired){
			this.setExpiryDate(moment().add(-1, 'day'), true)
		}else{
			this.setExpiryDate(moment().add(7, 'day'), true)
		}
		this.setSelectedValue(KEYS.POLICY_EXPIRED, isExpired, preventEmit)
	}
	private setExpiryDate(date: any, preventEmit?: boolean){
		let obj = {}
		obj[KEYS.EXPIRY_DATE] = date.date()
		obj[KEYS.EXPIRY_MONTH] = MONTHS[date.month()]
		obj[KEYS.EXPIRY_YEAR] = date.year()
		this.clearSelected(KEYS.IS_CLAIMED_LAST_YEAR, true)
		this.setSelected(obj, preventEmit)
		if(!this.isEligibleForNCB())
			this.setPreviousNCB(NCBConfig.AgeWiseNCB[0], true)
	}
	private setNCBClaimed(flag: string, preventEmit?: boolean){
		let isClaimed = parseInt(flag)
		let obj = {}
		//obj[KEYS.PREVIOUS_NCB] = isClaimed ? 0 : this.getDefaultNCB()
		obj[KEYS.IS_CLAIMED_LAST_YEAR] = isClaimed
		//obj[KEYS.NCB_UNKNOWN] = true
		this.setSelected(obj)
	}
	private setPreviousNCB(value: string | number, preventEmit?: boolean) {
		let unknown = isNaN(value)
		let obj = {}
		if(unknown){
			obj[KEYS.NCB_UNKNOWN] = true
			obj[KEYS.PREVIOUS_NCB] = this.getDefaultNCB()
		}else{
			obj[KEYS.NCB_UNKNOWN] = undefined
			obj[KEYS.PREVIOUS_NCB] = value
		}
		this.replaceSelected(obj, preventEmit)
	}
	private getDefaultNCB():number{
		let ncb = 25
		try{
			let today = moment()
			let regDate = RegistryStore.getRegistrationDate()
			let diff = today.diff(regDate, 'years')
			if(6>diff)
				ncb = NCBConfig.AgeWiseNCB[diff]
			else
				ncb = NCBConfig.AgeWiseNCB[6]
		}catch(e){}
		return ncb
	}

 	private _actionHandler:Function = (action:any) => {
		 switch(action.actionType) {
			 case AppActions.setExpired['CONST']:
				this.setPolicyExpiry(action.value)
				break

			 case AppActions.setExpiryDate['CONST']:
				this.setExpiryDate(action.value)
				break

			 case AppActions.setNCBClaimsMade['CONST']:
				this.setNCBClaimed(action.value)
				break

			 case AppActions.setPreviousNCB['CONST']:
				this.setPreviousNCB(action.value)
				break

			 case AppActions.processViewQuotes['CONST']:
				let ncb: any = this.getPreviousNCB()
				ncb == undefined || ncb === 'U' ? this.setPreviousNCB('U') : null
				break
		}
	}
}

export = new CarPolicyStore(KEYS.PARENT)