import moment = require('moment')
import BaseStore = require('../../common/BaseStore')
import AppActions = require('../actions/AppActions')
import MONTHS = require('../../common/constants/Months')
import DateUtils = require('../utils/DateUtils')
import APIUtils = require('../utils/APIUtils')
import StorageMap = require('../constants/StorageMap')

var KEYS = StorageMap.FOUR_WHEELER
var isFetching = false

class RegistryStore extends BaseStore{
	private _rtos: any[];	//id, name
	private _rtoMap: any;

	constructor(_namespace:string){
		super(_namespace)
		this.registerDispatcherHandler(this._actionHandler)
	}

	getRTOs(): any[] {
		return this._rtos || []
	}
	getYears(): number[] {
		return DateUtils.getRegistrationYears()
	}
	getSelectedRTO(): string{
		let selected = this.getSelected(KEYS.RTO.PARENT)
		return selected?selected[KEYS.RTO.ID]:''
	}
	getRegistrationYear(): number{
		let year = this.getSelected(KEYS.REGISTRATION_YEAR)
		return year ? parseInt(year) : undefined
	}
	getRegistrationNumber(): string{
		return this.getSelected(KEYS.REGISTRATION_NUMBER)
	}
	getRegistrationDate(): any{
		let DD = this.getSelected(KEYS.REGISTRATION_DATE)
		let MMMM = this.getSelected(KEYS.REGISTRATION_MONTH)
		let YYYY = this.getSelected(KEYS.REGISTRATION_YEAR)
		return DD && MMMM && YYYY ? moment(("0"+DD).slice(-2)+'-'+MMMM+'-'+YYYY, 'DD-MMMM-YYYY') : null
	}

	private setRTOs(rtos):void{
		this._rtos = rtos
		this._rtoMap = {}
		for(let index in rtos){
			this._rtoMap[rtos[index].id] = rtos[index].name
		}
		this.emitChange()
	}
	private getToggledRTOIdFromat(rtoId: string){
		let rto = rtoId.split('-')
		if (parseInt(rto[1])<10){
			if(rto[1].length===2)
				rto[1] = rto[1].charAt(1)
			else
				rto[1] = '0' + rto[1]
		}
		return rto.join('-')
	}
	private setRegistryInfo(data){
		let regDate = data.fastlane.response.result.vehicle['regn_dt'].split('/')
		this.setSelectedRegistrationDate(regDate[2], regDate[1], regDate[0], true)
		this.setManufacturingDate(regDate[2], regDate[1], regDate[0], true)
	}
	private setSelectedRTO(rtoId:string, preventEmit?:boolean):void{
		let obj = {}
		obj[KEYS.RTO.PARENT]={}

		if (typeof this._rtoMap[rtoId] !== 'undefined'){
			obj[KEYS.RTO.PARENT][KEYS.RTO.ID] = rtoId
			obj[KEYS.RTO.PARENT][KEYS.RTO.NAME] = this._rtoMap[rtoId]
		}else{
			let toggledRTOId = this.getToggledRTOIdFromat(rtoId)
			if (typeof this._rtoMap[toggledRTOId] !== 'undefined') {
				obj[KEYS.RTO.PARENT][KEYS.RTO.ID] = toggledRTOId
				obj[KEYS.RTO.PARENT][KEYS.RTO.NAME] = this._rtoMap[toggledRTOId]
			}
		}
		this.replaceSelected(obj, preventEmit);
	}
	private setSelectedRegistrationDate(year:string, month?:string, date?:string, preventEmit?:boolean):void{
		let obj = {}
		obj[KEYS.REGISTRATION_DATE] = date
		obj[KEYS.REGISTRATION_MONTH] = month ? MONTHS[parseInt(month) - 1] : undefined
		obj[KEYS.REGISTRATION_YEAR] = year
		this.replaceSelected(obj, preventEmit)
	}
	private setManufacturingDate(year: string, month?: string, date?: string, preventEmit?: boolean): void {
		let obj = {}
		obj[KEYS.MANUFACTURING_DATE] = date
		obj[KEYS.MANUFACTURING_MONTH] = month ? MONTHS[parseInt(month) - 1] : undefined
		obj[KEYS.MANUFACTURING_YEAR] = year
		this.replaceSelected(obj, preventEmit)
	}
	private selectRTOFromRegNo(regNo:any[]):void{
		var rtoCode2 = regNo[1]
		rtoCode2 = isNaN(rtoCode2) ? rtoCode2.substr(0, 1) : rtoCode2
		this.setSelectedRTO(regNo[0] + '-' + rtoCode2)
	}

	private _actionHandler:Function = (action:any) => {
		switch(action.actionType) {
			case AppActions.receiveRTOs['CONST']:
				this.setRTOs(action.value)
		 		break

		 	case AppActions.receiveRegistryInfo['CONST']:
		 		isFetching = false
		 		this.setRegistryInfo(action.value)
		 		break

		 	case AppActions.selectRTO['CONST']:
				this.setSelectedRTO(action.value)
				if(action.callback)
				  action.callback()
				break

			case AppActions.setRegistrationNumber['CONST']:
				isFetching = false
				this.setSelectedValue(KEYS.REGISTRATION_NUMBER, action.regNoArray, true)
				this.selectRTOFromRegNo(action.regNoArray)
				break

			case AppActions.fetchVehicleInfo['CONST']:
				if(!isFetching){
					isFetching = true
					APIUtils.getVehicleInfo(this.getSelected(KEYS.REGISTRATION_NUMBER).join(''), action.callback)
				}
				break

			case AppActions.selectYear['CONST']:
				this.setSelectedRegistrationDate(action.value, undefined, undefined, true)
				this.setManufacturingDate(action.value)
				if(action.callback)
					action.callback()
				break
		}
	}
}

var registryStore:RegistryStore;
if(!registryStore){
	registryStore = new RegistryStore(KEYS.PARENT)
	APIUtils.getInitialStateRegistry()
	let regNo = registryStore.getRegistrationNumber();
	if(regNo){
		isFetching = true
		APIUtils.getVehicleInfo(regNo)
	}
}

export = registryStore