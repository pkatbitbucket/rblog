import BaseStore = require('../../common/BaseStore')
import Set = require('../../common/Set')
import Cookie = require('react-cookie')
import MONTHS = require('../../common/constants/Months')
import AppActions = require('../actions/AppActions')
import APIUtils = require('../utils/APIUtils')
import GTMUtils = require('../utils/GTMUtils')
import StorageMap = require('../constants/StorageMap')
import PopularCars = require('../constants/PopularCars')
import FuelTypes = require('../constants/FuelTypes')
import RegistryStore = require('./RegistryStore')
import CarPolicyStore = require('./CarPolicyStore')

var KEYS = StorageMap.FOUR_WHEELER;

class VehicleStore extends BaseStore{
	private _vehicles: any[] = PopularCars;	//id, name
	private _variants: any[];				//id, name, fuel_type
	private _fuelTypes: any[] = FuelTypes;	//code, name, id

	constructor(_namespace:string){
		super(_namespace)
		this.registerDispatcherHandler(this._actionHandler)
		this.init(true)
	}

	private init(preventEmit?:boolean){
		let obj = {}
		obj[KEYS.IS_CNG_FITTED] = 0
		obj[KEYS.IS_NEW_VEHICLE] = 0
		obj[KEYS.IS_USED_VEHICLE] = 0
		obj[KEYS.IDV] = 0
		obj[KEYS.IDV_ELECTRICAL] = 0
		obj[KEYS.IDV_NON_ELECTRICAL] = 0
		obj[KEYS.VOLUNTARY_DEDUCTIBLE] = 0
		obj[KEYS.HAS_OPTIMIZED_QUOTES] = false
		obj[KEYS.FASTLANE_MODEL_CHANGED] = this.getSelected(KEYS.FASTLANE_MODEL_CHANGED) || false
		obj[KEYS.FASTLANE_FUELTYPE_CHANGED] = this.getSelected(KEYS.FASTLANE_FUELTYPE_CHANGED) || false
		obj[KEYS.FASTLANE_VARIANT_CHANGED] = this.getSelected(KEYS.FASTLANE_VARIANT_CHANGED) || false
		this.setSelected(obj, preventEmit)
	}

	getVehicles(): any[] {
		return this._vehicles || []
	}
	getVariants(): any[] {
		let variants = []
		let fuelType = this.getSelected(KEYS.FUEL_TYPE.PARENT)
		/*filtering variants on the basis of fuel type*/
		if (fuelType && this._variants && this._variants.length > 0) {
			fuelType=fuelType.id
			for(let index in this._variants){
				if(fuelType===this._variants[index]['fuel_type']){
					variants.push(this._variants[index])
				}
			}
		}else{
			variants = this._variants ? this._variants.slice() : variants
		}
		return variants
	}
	getFuelTypes(): any[] {
		return this._fuelTypes || []
	}
	getSelectedVehicle(): number{
		let selected = this.getSelected(KEYS.VEHICLE.PARENT)
		return selected?selected[KEYS.VEHICLE.ID]:''
	}
	getSelectedVehicleVerbose(): string{
		let selected = this.getSelected(KEYS.VEHICLE.PARENT)
		return selected?selected[KEYS.VEHICLE.NAME]:''
	}
	getSelectedFuelType(): string{
		let selected = this.getSelected(KEYS.FUEL_TYPE.PARENT)
		return selected?selected[KEYS.FUEL_TYPE.CODE]:''
	}
	getSelectedVariant(): number{
		let selected = this.getSelected(KEYS.VEHICLE_VARIANT.PARENT)
		return selected?selected[KEYS.VEHICLE_VARIANT.ID]:''
	}
	getSelectedVariantVerbose(): string{
		let selected = this.getSelected(KEYS.VEHICLE_VARIANT.PARENT)
		return selected?selected[KEYS.VEHICLE_VARIANT.NAME]:''
	}

	getFastlaneSuccessStatus(): number{
		let status = this.getSelected(KEYS.FASTLANE_SUCCESS)
		return isNaN(status) ? 3 : status
	}
	getCNGKitValue(): number{
		return this.getSelected(KEYS.CNG_KIT_VALUE)
	}
	isFastlaneDataChanged(): boolean{
		return this.getSelected(KEYS.FASTLANE_MODEL_CHANGED)
			|| this.getSelected(KEYS.FASTLANE_FUELTYPE_CHANGED)
			|| this.getSelected(KEYS.FASTLANE_VARIANT_CHANGED)
	}
	getGTMData():any{
		let mobileNo = Cookie.load('mobileNo')

		let isExpired = !!this.getSelected(KEYS.POLICY_EXPIRED)
		let expiryDate
		if (isExpired) {
			let DD = this.getSelected(KEYS.EXPIRY_DATE)
			let MMMM = this.getSelected(KEYS.EXPIRY_MONTH)
			let YYYY = this.getSelected(KEYS.EXPIRY_YEAR)

			expiryDate = DD && MMMM && YYYY ? DD + '-' + MONTHS.indexOf(MMMM) + '-' + YYYY : null
		}

		let rto = this.getSelected(KEYS.RTO.PARENT)
		rto = rto ? rto[KEYS.RTO.NAME] : ''

		let regYear = this.getSelected(KEYS.REGISTRATION_YEAR)
		let model = this.getSelectedVehicleVerbose()
		let fuel = this.getSelectedFuelType()
		let variant = this.getSelectedVariantVerbose()
		let fsChanged = this.isFastlaneDataChanged()

		let claimed = this.getSelected(KEYS.IS_CLAIMED_LAST_YEAR)
		let ncb = this.getSelected(KEYS.PREVIOUS_NCB)

		return {
			mobile: mobileNo,
			expired: isExpired,
			expirydate: expiryDate,
			rto: rto,
			regyear: regYear,
			model: model,
			fuel: fuel,
			variant: variant,
			changed: fsChanged,
			ncb: claimed ? null : ncb
		}
	}

	private getFilteredCarsList(vehicles){
		let map = {}
		let popularPositions = []
		for(let index in vehicles){
			map[vehicles[index]['id']+''] = index
		}
		for(let index in PopularCars){
			let position = map[PopularCars[index]['id']]
			if(!isNaN(position) && position>=0){
				popularPositions.push(position)
			}
		}
		popularPositions.sort(function(a, b) { return b - a })
		for(let index in popularPositions){
			vehicles.splice(popularPositions[index], 1)
		}
		return PopularCars.concat(vehicles)
	}
	private setVehicles(vehicles):void{
		this._vehicles = this.getFilteredCarsList(vehicles)
		let selectedVehicle = this.getSelected(KEYS.VEHICLE.PARENT)
		if(selectedVehicle && selectedVehicle[KEYS.VEHICLE.ID] && !selectedVehicle[KEYS.VEHICLE.NAME]){
			let obj = {}
			obj[KEYS.VEHICLE.ID]=selectedVehicle[KEYS.VEHICLE.ID]
			for (let index = 0; this._vehicles.length > index; index++) {
				if (this._vehicles[index]['id'] == obj[KEYS.VEHICLE.ID]) {
					obj[KEYS.VEHICLE.NAME] = this._vehicles[index]['name']
					break
				}
			}
			this.setSelectedValue(KEYS.VEHICLE.PARENT, obj, true)
		}
		this.emitChange()
	}
	private setVariants(variants):void{
		this._variants = variants
		this.emitChange()
	}
	private setFuelTypes(fuelTypeSet:Set){
		this._fuelTypes = []
		for(let index in FuelTypes){
			if(fuelTypeSet.has(FuelTypes[index].id)){
				this._fuelTypes.push(FuelTypes[index])
			}
		}
		this.emitChange()
	}
	private setVehicleInfoData(data, callback?:Function):void{
		let obj = {}
		obj[KEYS.FASTLANE_SUCCESS] = 1
		obj[KEYS.RDS_ID] = data['rds_id']
		obj[KEYS.FASTLANE_VEHICLE_MODEL_ID] = data['vehicle_model_id']
		obj[KEYS.FASTLANE_VEHICLE_MASTER_ID] = data['vehicle_master_id']
		this.setSelected(obj, true)
		this.setSelectedVehicle(data['vehicle_model_id'], true,
			()=>{
				if (data['vehicle_master_id'])
					this.setSelectedVariant(data['vehicle_master_id'])
				if(callback)
					callback()
			})
	}
	private setSelectedVehicle(modelId: string, preventEmit?: boolean, callback?: Function): void {
		let obj = {}
		this._variants=[]
		obj[KEYS.VEHICLE.PARENT]={}
		obj[KEYS.VEHICLE.PARENT][KEYS.VEHICLE.ID]=modelId
		for (let index = 0; this.getVehicles().length > index; index++) {
			if (this.getVehicles()[index].id == modelId) {
				obj[KEYS.VEHICLE.PARENT][KEYS.VEHICLE.NAME] = this.getVehicles()[index]['name']
				break
			}
		}
		this.clearSelected(KEYS.FUEL_TYPE.PARENT, true)
		this.clearSelected(KEYS.VEHICLE_VARIANT.PARENT, true)
		this.replaceSelected(obj, preventEmit)
		APIUtils.getCarVariants(modelId, callback)
	}
	private setSelectedVariant(variantId:string, preventEmit?:boolean):void{
		let obj = {}
		obj[KEYS.VEHICLE_VARIANT.PARENT]={}
		obj[KEYS.VEHICLE_VARIANT.PARENT][KEYS.VEHICLE_VARIANT.ID]=variantId
		for(let index=0; this.getVariants().length>index; index++){
			if(this.getVariants()[index].id==variantId){
				obj[KEYS.VEHICLE_VARIANT.PARENT][KEYS.VEHICLE_VARIANT.NAME] = this.getVariants()[index]['name']
				obj[KEYS.VEHICLE_VARIANT.PARENT][KEYS.VEHICLE_VARIANT.FUEL_TYPE] = this.getVariants()[index]['fuel_type']
				break
			}
		}
		let fuelType = this.getSelected(KEYS.FUEL_TYPE.PARENT)
		if(!fuelType || !fuelType[KEYS.FUEL_TYPE.CODE]){
			this.setSelectedFuelTypeById(obj[KEYS.VEHICLE_VARIANT.PARENT][KEYS.VEHICLE_VARIANT.FUEL_TYPE], true)
		}
		this.replaceSelected(obj, preventEmit)
	}
	private setSelectedFuelTypeById(fuelTypeId:string, preventEmit?:boolean):void{
		let obj = {}
		obj[KEYS.FUEL_TYPE.PARENT]={}
		obj[KEYS.FUEL_TYPE.PARENT][KEYS.FUEL_TYPE.ID]=fuelTypeId
		for (let index = 0; this.getFuelTypes().length > index; index++){
			if(this.getFuelTypes()[index].id==fuelTypeId){
				obj[KEYS.FUEL_TYPE.PARENT][KEYS.FUEL_TYPE.CODE] = this.getFuelTypes()[index]['code']
				obj[KEYS.FUEL_TYPE.PARENT][KEYS.FUEL_TYPE.NAME] = this.getFuelTypes()[index]['name']
				break
			}
		}
		let fueltype = obj[KEYS.FUEL_TYPE.PARENT][KEYS.FUEL_TYPE.NAME] ? obj[KEYS.FUEL_TYPE.PARENT][KEYS.FUEL_TYPE.NAME].toLowerCase() : ''
		obj[KEYS.IS_CNG_FITTED] = (fueltype === 'petrol' || fueltype === 'diesel') ? 0 : 1
		this.replaceSelected(obj, preventEmit)
	}
	private setSelectedFuelType(fuelTypeCode:string, preventEmit?:boolean):void{
		let obj = {}
		obj[KEYS.FUEL_TYPE.PARENT] = {}
		obj[KEYS.FUEL_TYPE.PARENT][KEYS.FUEL_TYPE.CODE] = fuelTypeCode
		for (let index = 0; this.getFuelTypes().length > index; index++){
			if(this.getFuelTypes()[index].code==fuelTypeCode){
				obj[KEYS.FUEL_TYPE.PARENT][KEYS.FUEL_TYPE.ID] = this.getFuelTypes()[index]['id']
				obj[KEYS.FUEL_TYPE.PARENT][KEYS.FUEL_TYPE.NAME] = this.getFuelTypes()[index]['name']
				break
			}
		}
		let fueltype = obj[KEYS.FUEL_TYPE.PARENT][KEYS.FUEL_TYPE.NAME] ? obj[KEYS.FUEL_TYPE.PARENT][KEYS.FUEL_TYPE.NAME].toLowerCase() : ''
		obj[KEYS.IS_CNG_FITTED] = (fueltype === 'petrol' || fueltype === 'diesel') ? 0 : 1
		this.clearSelected(KEYS.CNG_KIT_VALUE, true)
		this.replaceSelected(obj, preventEmit)
	}
	private setCNGKitValue(value:number):void{
		let obj = {}
		if (typeof value === 'undefined') {
			this.clearSelected(KEYS.CNG_KIT_VALUE, true)
		}else{
			obj[KEYS.CNG_KIT_VALUE] = value
			this.setSelected(obj)
		}
	}
	private setIncorrectFastlane(model?: boolean, fuel?: boolean, variant?: boolean) {
		let obj = {}

		if (model) {
			let fastlanedata = this.getSelected(KEYS.FASTLANE_VEHICLE_MODEL_ID)
			let currentdata = this.getSelected(KEYS.VEHICLE.PARENT)
			obj[KEYS.FASTLANE_MODEL_CHANGED] = currentdata ? (currentdata[KEYS.VEHICLE.ID] != fastlanedata) : false
		}
		if (fuel) {
			obj[KEYS.FASTLANE_FUELTYPE_CHANGED] = true
		}
		if (variant) {
			let fsData = this.getSelected(KEYS.FASTLANE_VEHICLE_MASTER_ID)
			let cuData = this.getSelected(KEYS.VEHICLE_VARIANT.PARENT)
			obj[KEYS.FASTLANE_VARIANT_CHANGED] = cuData ? (cuData[KEYS.VEHICLE_VARIANT.ID] != fsData) : false
			if (!obj[KEYS.FASTLANE_VARIANT_CHANGED])
				obj[KEYS.FASTLANE_FUELTYPE_CHANGED] = false
		}

		this.setSelected(obj, true)
	}
	private processViewQuotes(callback: Function){
		let data = this.getGTMData()
		if (data.mobile)
			GTMUtils.mobileNumberSubmitted('homepage')

		GTMUtils.successfulFastlaneSubmit(data.model, data.fuel, data.variant,
			data.rto, data.regyear, data.expirydate, data.mobile)
		
		APIUtils.sendLeadsToLMS(this.getSelected(), data.expired, callback)
	}

 	private _actionHandler:Function = (action:any) => {
		 switch(action.actionType) {
			 case AppActions.receiveVehicles['CONST']:
				this.setVehicles(action.value)
		 		break

			 case AppActions.receiveVariants['CONST']:
		 		this.setVariants(action.value)
		 		break

		 	 case AppActions.receiveFuelTypes['CONST']:
			 	this.setFuelTypes(action.value)
			 	break
			 	
			 case AppActions.receiveVehicleInfo['CONST']:
			 	this.waitFor(RegistryStore)
				this.setVehicleInfoData(action.value, action.callback)
				break

			 case AppActions.selectVehicle['CONST']:
				this.setSelectedVehicle(action.value)
			 	this.setIncorrectFastlane(true)
		 		if(action.callback)
		 			action.callback()
		 		break

			 case AppActions.selectFuelType['CONST']:
				this.clearSelected(KEYS.VEHICLE_VARIANT.PARENT, true)
		 		this.setSelectedFuelType(action.value)
			 	this.setIncorrectFastlane(false, true)
				if (action.callback)
					action.callback()
				break

			 case AppActions.selectVariant['CONST']:
				this.setSelectedVariant(action.value)
			 	this.setIncorrectFastlane(false, false, true)
				if (action.callback)
					action.callback()
				break

			 case AppActions.setCNGKitValue['CONST']:
				this.setCNGKitValue(action.value)
				break

			 case AppActions.isNewCar['CONST']:
			 	this.clearSelected(undefined, true)
				this.init(true)
				this.setSelectedValue(KEYS.IS_NEW_VEHICLE, 1)
				if (action.callback)
					action.callback()
				break

			 case AppActions.clearVehicleInfo['CONST']:
				this.clearSelected(undefined, true)
				this.init()
				if(action.callback)
					action.callback()
			 	break

			 case AppActions.sendFastlaneFeedback['CONST']:
			 	APIUtils.sendFastlaneFeedback(this.getSelected(KEYS.RDS_ID), this.isFastlaneDataChanged(),
			 		this.getSelectedVehicle(), this.getSelectedVariant())
			 	break

			 case AppActions.processViewQuotes['CONST']:
			 	this.waitFor(CarPolicyStore)
			 	this.processViewQuotes(action.callback)
			 	break

			 case AppActions.vehicleInfoFetchFailed['CONST']:
			 	this.setSelectedValue(KEYS.FASTLANE_SUCCESS, 0)
			 	this.setSelectedValue(KEYS.IS_NEW_VEHICLE, 0)
			 	GTMUtils.fastlaneFailed()
			 	if (action.callback)
					setTimeout(action.callback, 0)
			 	break
		}
	}
}

var vehicleStore:VehicleStore;
if(!vehicleStore){
	vehicleStore = new VehicleStore(KEYS.PARENT)
	APIUtils.getInitialStateCar()
	let selectedVehicle = vehicleStore.getSelectedVehicle()
	if(selectedVehicle)
		APIUtils.getCarVariants(selectedVehicle)
}

export = vehicleStore