import request = require('superagent')
import Cookie = require('react-cookie')
import Set = require('../../common/Set')
import CommonAPIs = require('../../common/utils/APIUtils')
import AppActions = require('../actions/AppActions')

declare var CSRF_TOKEN: string
var VEHICLE_TYPE = 'fourwheeler';

function getVehicles(callback:Function){
	request
		.post('/motor/' + VEHICLE_TYPE + '/api/vehicles/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.send({
			'csrfmiddlewaretoken': CSRF_TOKEN
		})
		.end(function(err, res){
			if(res.ok){
				callback(JSON.parse(res.text).data.models)
			}else{
				console.log("Something went wrong", err)
				if(err.status!=404)
					setTimeout(getVehicles.bind(null, callback), 1000)
			}
		});
}

function getRTOs(callback:Function){
	request
		.post('/motor/' + VEHICLE_TYPE + '/api/rtos/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.send({
			'csrfmiddlewaretoken': CSRF_TOKEN
		})
		.end(function(err, res){
			if(res.ok){
				callback(JSON.parse(res.text).data.rtos)
			}else{
				console.log("Something went wrong", err)
				if(err.status!=404)
					setTimeout(getRTOs.bind(null, callback), 1000)
			}
		});
}

function getVehicleInfo(regNo:string, callback:Function, failureCallback:Function) {
	request
		.post('/motor/' + VEHICLE_TYPE + '/api/vehicle-info/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.send({
			'name': regNo,
			'csrfmiddlewaretoken': CSRF_TOKEN
		})
		.end(function(err, res) {
			if(res){
				if (res.ok)
					res = JSON.parse(res.text)
				if (res.data && !!res.data['vehicle_model_id'] && !!res.data.fastlane.response.result.vehicle['regn_dt'])
					callback(res.data)
				else
					failureCallback()
			} else {
				failureCallback()
				console.log("Something went wrong", err)
			}
		})
}

function getCarVariants(modelId:string | number, callback:Function){
	request
		.post('/motor/' + VEHICLE_TYPE + '/api/vehicles/' + modelId + '/variants/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.send({
			'csrfmiddlewaretoken': CSRF_TOKEN
		})
		.end(function(err, res){
			if(res.ok){
				let response = JSON.parse(res.text)
				let variants = response.data.variants
				callback(variants)
			}
			else{
				console.log("Something went wrong", err)
			}
		});
}

function sendFastlaneFeedback(rds_id: number, confirmation_type: string, data: any, callback?:Function){
	request
		.post('/motor/' + VEHICLE_TYPE + '/api/vehicle-confirmation/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.send({
			'data': JSON.stringify(data),
			'confirmation_type' : confirmation_type,
			'rds_id': rds_id,
			'csrfmiddlewaretoken': CSRF_TOKEN
		})
		.end(function(err, res){
			console.log("Fastlane Feedback")
		});
}

function sendLeadsToLMS(data:any, isExpired:boolean, callback: Function){
    data["mobile"] = Cookie.load('mobileNo')
	if (!data["mobile"]){
    	if(callback)
    		callback()
    	return
	}
	data["campaign"] = window['CAMPAIGN'] ? window['CAMPAIGN'] : "motor-homepage"
    data["label"] = window['PAGE_ID'] ? window['PAGE_ID'] : "homepage"
	if(isExpired){
        data["lms_campaign"] = "motor-offlinecases";
    }

    request
	.post('/leads/save-call-time/')
	.set('Content-Type', 'application/x-www-form-urlencoded')
	.send({
        'data' : JSON.stringify(data), 
        'csrfmiddlewaretoken' : CSRF_TOKEN
		})
	.end(function(err, res){
		if(callback)
			callback()
	});
}

var APIs = {
	getInitialStateCar: function(){
		getVehicles(AppActions.receiveVehicles)
	},
	getInitialStateRegistry: function(){
		getRTOs(AppActions.receiveRTOs)
	},
	getVehicleInfo: function(regNo: string, callback?: Function) {
		getVehicleInfo(regNo, function(data){
			AppActions.receiveRegistryInfo(data)
			AppActions.receiveVehicleInfo(data, callback)
		}, function(){
			AppActions.vehicleInfoFetchFailed(callback)
		})
	},
	getCarVariants: function(modelId: string | number, callback?: Function) {
		getCarVariants(modelId, function(variants) {
			AppActions.receiveVariants(variants)
			let fuelSet: Set = new Set()
			for (let index in variants) {
				fuelSet.add(variants[index]['fuel_type'])
			}
			AppActions.receiveFuelTypes(fuelSet)
			if(callback)
				callback()
		})
	},
	sendFastlaneFeedback: function(rdsId: number, dataChanged: boolean, modelId: number, masterId: number){
		let data = {
			'vehicle_model_id': modelId,
			'vehicle_master_id': masterId
		}
		let confirmationType = dataChanged ? 'changed' : 'accepted'
		sendFastlaneFeedback(rdsId, confirmationType, data)
	},
	sendLeadsToLMS: sendLeadsToLMS
}

export = (function(){
	if (!CSRF_TOKEN)
		CommonAPIs.getCSRF()
	return APIs
}())