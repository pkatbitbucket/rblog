var utils = {
	getCurrentYear: function(){
		return (new Date()).getFullYear()
	},
	getCurrentMonth: function(){
		return (new Date()).getMonth()
	},
	getRegistrationYears: function(){
		var y = []
		var cy = utils.getCurrentYear(),
			cm = utils.getCurrentMonth()
		if(cm < 9){
			cy--
		}
		for(var i = cy; i >= cy-15;i--){
			y.push(i)
		}
		return y
	},
	getManufacturingYears: function(){
		var y = []
		var cy = utils.getCurrentYear()

		for(var i = cy; i > cy-2;i--){
			y.push(i)
		}
		return y
	},

}

export = utils