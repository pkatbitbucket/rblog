function ga(data){
    if (!window['dataLayer'] || typeof data !== 'object')
        return
    window['dataLayer'].push(data);
}

class GTMUtils{

    static desktopEditEvents = [
        { event: 'HomepageFastlaneCarEventRegEdit', action: 'Registration Number' },
        { event: 'HomepageFastlaneCarEventModelEdit', action: 'Your Car' },
        { event: 'HomepageFastlaneCarEventExpiryEdit', action: 'Policy Expiry' },
        { event: 'HomepageFastlaneCarEventNCBEdit', action: 'NCB' }
    ]
	
	successfulFastlaneSubmit(model:string, fuel:string, variant:string, rto:string, regYear:any, expiryDate:any, mobileNo:string | number){
		ga({
            'event': 'HomepageTopFastlaneCarEventGetQuote',
            'category': 'Homepage Top Fastlane Car - Get Quote',
            'action': expiryDate ? 'Expired Policy' : 'Not Expired Policy',
            'label': '',
            'param1': model+'',
            'param2': fuel+'',
            'param3': variant+'',
            'param4': rto,
            'param5': regYear,
            'param6': expiryDate,
            'param7': '',
            'param8': mobileNo ? 'Mobile Number given' : 'Mobile Number Not Given'
        })
	}

	fastlaneFailed(){
		ga({
            'event': 'HomepageTopFastlaneCarEventProductPage',
            'category': 'Homepage Top Fastlane Car - Redirect Product',
            'label': '',
            'action': 'Fastlane Not Found'
        })
	}

	unknownRegistrationNumber(isNewCar:boolean){
		ga({
            'event': 'HomepageFastlaneCarEventProductPage',
            'category': 'Homepage Fastlane Car - Redirect Product',
            'label': isNewCar ? 'I Have A New Car' : 'User Number Not Known',
            'action': ''
        })
	}

	mobileNumberSubmitted(label: string){
		ga({
            'event': 'GAEvent',
            'eventCategory': 'MOTOR',
            'eventAction': 'CallScheduled',
            'eventLabel': label
        })
	}

    fsStepOne(){
        ga({
            'event': 'HomepageFastlaneCarEventStepone',
            'category': 'Homepage Fastlane Car - Number Accepted',
            'action': 'Number Confirmed'
        })
    }

    fsStepTwo(changed: boolean, model: string, fuel: string, variant: string, rto: string, regYear: number) {
        ga({
            'event': 'HomepageFastlaneCarEventSteptwo',
            'category': 'Homepage Fastlane Car - Model Confirmed',
            'action': 'Number Confirmed',
            'label': changed ? model + " : " + fuel + " : " + variant : 'Without Change',
            'param1': model + '',
            'param2': fuel + '',
            'param3': variant + '',
            'param4': rto,
            'param5': regYear
        })
    }

    fsStepThree(changed: boolean, model: string, fuel: string, variant: string, rto: string, regYear: number, expiryDate: string) {
        ga({
            'event': 'HomepageFastlaneCarEventStepthree',
            'category': 'Homepage Fastlane Car - Expiry Confirmed',
            'action': expiryDate ? "Expired Policy" : "Not Expired Policy" ,
            'label': changed ? model + " : " + fuel + " : " + variant : 'Without Change',
            'param1': model + '',
            'param2': fuel + '',
            'param3': variant + '',
            'param4': rto,
            'param5': regYear,
            'param6': expiryDate
        })
    }

    fsStepFour(changed: boolean, model: string, fuel: string, variant: string, rto: string, regYear: number, expiryDate: string, ncb: any) {
        ga({
            'event': 'HomepageFastlaneCarEventStepfour',
            'category': 'Homepage Fastlane Car - Claim Confirmed',
            'action': expiryDate ? "Expired Policy" : "Not Expired Policy",
            'label': ncb !== undefined && ncb!==null ? ncb : 'Claim Made',
            'param1': model + '',
            'param2': fuel + '',
            'param3': variant + '',
            'param4': rto,
            'param5': regYear,
            'param6': expiryDate
        })
    }

    fsEditClick(step){
        var data = GTMUtils.desktopEditEvents[step-1]
        if(!data)
            return
        ga({
            'event': data.event,
            'category': 'Homepage Fastlane Car - Edit',
            'action': data.action,
            'label': 'Edit Clicked'
        })
    }


    //*********************** HOME PAGE

    unknownRegistrationNumberHome(isNewCar:boolean){
        ga({
            'event': 'HomepageTopFastlaneCarEventProductPage',
            'category': 'Homepage Top Fastlane Car - Redirect Product',
            'label': isNewCar ? 'I Have A New Car' : 'User Number Not Known',
            'action': isNewCar ?'Car Fastlane': '',
        })
    }

    successfulFastlaneSubmitHome(){

    }

    fastlaneFailedHome(){
        
    }

}

var gtmUtils: GTMUtils;

export = (function(){
	if (!gtmUtils)
		gtmUtils = new GTMUtils()
	return gtmUtils
}())