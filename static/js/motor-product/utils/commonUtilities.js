var commonUtilities = function(){}

commonUtilities.RegistrationNumberValidation = function (reg_no){
    var validChar = /^[a-z0-9-]+$/i;
    var valid = true;
    if(validChar.test(reg_no)){
        reg_no = reg_no.replace(/-/g,"");

        var state = reg_no.substring(0,2);
        var numArr = reg_no.substring(2).split(/[a-z]/i).filter(function(a){if(a) return a });
        var rto = reg_no.substring(2).split(/\d/).filter(function(a){if(a) return a })[0] || "";
        
        var district = numArr[0];
        var series = numArr[1] || "";
        
        var allowExtraChar = ["DL", "RJ"];
        var allowBlankRto = ["UK", "GJ", "UA"]

        // Validate if length is same for input and parse value
        if((state + district + rto +  series) !== reg_no){
            valid = false;
        }

        // Check if state exists in RTO list
        else if(!rtoList[state]){
            valid = false;
        }

        // If rto and series are not available, special handling for GJ11
        else if(!rto && !series){
            if(allowBlankRto.indexOf(state)<0){
                valid = false;
            }
            else if(district.length <=1){
                valid = false;
            }
            else if(district.length>6){
                valid = false;
            }

        }
        else if(rto && series){
            if(rtoList[state].filter(function(a){return parseInt(a) === parseInt(district); }).length === 0){
                valid = false;
            }

            else if(rto.toString().length>4){
                valid = false;
            }

            else if(rto.toString().length > 3 && allowedExtraChar.indexOf(state) < 0){
                valid = false;
            }
        }
        else if(!rto || !series){
            valid = false;
        }
    }
    else{
        valid = false;
    }
    return valid;
}