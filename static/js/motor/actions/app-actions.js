var AppConstants = require('../constants/app-constants.js')
var AppDispatcher = require('../dispatchers/app-dispatcher.js')

var appActions = {

	viewQuotes: function(status){
		AppDispatcher.dispatch({
			actionType: AppConstants.VIEW_QUOTES,
			status: status
		})
	},

	sendfastlaneFeedback: function(){
		AppDispatcher.dispatch({
			actionType: AppConstants.SEND_FASTLANE_FEEDBACK
		})
	},

	receiveVehicleInfo : function(data, callback){
		AppDispatcher.dispatch({
			actionType: AppConstants.RECIEVE_VEHICLE_INFO,
			data: data,
			callback: callback
		})
	},

	recieveVehicles : function(vehicles){
		AppDispatcher.dispatch({
			actionType: AppConstants.RECIEVE_VEHICLES,
			vehicles: vehicles
		})
	},

	recieveRTOs : function(rtos){
		AppDispatcher.dispatch({
			actionType: AppConstants.RECIEVE_RTOS,
			rtos: rtos
		})
	},

	clearVehicleInfo : function(isNewVehicle, callback){
		AppDispatcher.dispatch({
			actionType: AppConstants.CLEAR_SELECTED_DATA,
			isNewVehicle: isNewVehicle,
			callback: callback
		})
	},

	setRegistrationNumber: function(regNo){
		AppDispatcher.dispatch({
			actionType: AppConstants.SET_REGISTRATION_NUMBER,
			value: regNo
		})
	},

	selectRTO: function(rto){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_RTO,
			rto: rto
		})
	},

	selectRTOfromId: function(rtoid){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_RTO_FROM_ID,
			rtoid: rtoid
		})
	},

	recieveVariants : function(variants, callback){
		AppDispatcher.dispatch({
			actionType: AppConstants.RECIEVE_VARIANTS,
			variants: variants,
			callback: callback
		})
	},

	selectVehicle: function(vehicle, callback){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_VEHICLE,
			vehicle: vehicle,
			callback:callback
		})
	},

	deselectVehicle: function(){
		AppDispatcher.dispatch({
			actionType: AppConstants.DESELECT_VEHICLE,
		})
	},

	selectVariant: function(variant){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_VARIANT,
			variant: variant
		})
	},

	selectFuelType: function(fuel_type){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_FUEL_TYPE,
			fuel_type: fuel_type
		})
	},

	setCNGKitValue: function(value){
		AppDispatcher.dispatch({
			actionType: AppConstants.SET_CNG_VALUE,
			value: value
		})
	},

	selectRegistrationYear: function(year, quote_type){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_REGISTRATION_YEAR,
			year: year,
			quote_type: quote_type
		})
	},

	selectNCB: function(ncb, callback, isNewNCB){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_NCB,
			isNewNCB: isNewNCB,
			ncb: ncb,
			callback: callback
		})
	},

	setClaimStatus: function(status, isNewNCB){
		AppDispatcher.dispatch({
			actionType: AppConstants.SET_CLAIM_STATUS,
			isNewNCB: isNewNCB,
			status: status
		})
	},

	setQuoteType: function(quote_type, callback){
		AppDispatcher.dispatch({
			actionType: AppConstants.SET_QUOTE_TYPE,
			quote_type: quote_type,
			callback: callback
		})
	},

	setExpiry: function(quote_type, callback){
		AppDispatcher.dispatch({
			actionType: AppConstants.SET_EXPIRY,
			quote_type: quote_type,
			callback: callback
		})
	},

	recievePlans : function(plans, quoteId, fetch_counter, finished){
		AppDispatcher.dispatch({
			actionType: AppConstants.RECIEVE_PLANS,
			plans: plans,
			quoteId: quoteId,
			fetch_counter:fetch_counter,
			finished:finished
		})
	},

	sortPlans: function(criteria){
		AppDispatcher.dispatch({
			actionType: AppConstants.SORT_PLANS,
			criteria: criteria
		})
	},

	removeAllPlans: function(){
		AppDispatcher.dispatch({
			actionType: AppConstants.REMOVE_ALL_PLANS,
		})
	},

	startFetchingPlans: function(){
		AppDispatcher.dispatch({
			actionType: AppConstants.START_FETCHING_PLANS,
		})
	},

	selectPlan: function(plan, callback){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_PLAN,
			plan: plan,
			callback:callback
		})
	},

	deselectPlan: function(plan){
		AppDispatcher.dispatch({
			actionType: AppConstants.DESELECT_PLAN,
		})
	},
	toggleAddon: function(addonid, callback){
		AppDispatcher.dispatch({
			actionType: AppConstants.TOGGLE_ADDON,
			addonid:addonid,
			callback:callback
		})
	},
	setIDV: function(idv, callback){
		AppDispatcher.dispatch({
			actionType: AppConstants.SET_IDV,
			idv:idv,
			callback:callback
		})
	},
	setDate: function(type, value, callback, fastlane){
		AppDispatcher.dispatch({
			actionType: AppConstants.SET_DATE,
			type:type,
			value:value,
			callback: callback,
			fastlane: fastlane
		})
	}

}

module.exports = appActions;
