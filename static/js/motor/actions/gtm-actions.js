var GTMPush = require('../utils/gtmutils').gtm_push
var AppStore = require('../stores/app-store')
var VehicleStore = require('../stores/vehicle-store')
var RTOStore = require('../stores/rto-store')
var DateStore = require('../stores/date-store')
var AdditionalCoverStore = require('../stores/additional-cover-store')
var vehicleType = require('../constants/app-constants').vehicle_type_verbose

var getUserData = function(){

	var sa = AdditionalCoverStore.getSelectedAddonNames() || []

	return {
		AppData: AppStore.getData(),
		AllAddons: AdditionalCoverStore.getAddons(),
		SelectedAddons: sa.join('|'),
		SelectedVehicle: VehicleStore.getSelectedVehicle()? VehicleStore.getSelectedVehicle().name:'',
		SelectedVariant: VehicleStore.getSelectedVariant()? VehicleStore.getSelectedVariant().name:'',
		SelectedFuelType: VehicleStore.getSelectedVariant()? VehicleStore.getSelectedVariant().fuel_type:'',
		SelectedRTO: RTOStore.getSelectedRTO()? RTOStore.getSelectedRTO().name:'',
		SelectedRegYear: DateStore.getSelectedRegYear()
	}
}

var getActionFromQuoteType = function(){
	var quote_type = getUserData().AppData.quoteType
	var action
	if(quote_type == 'renew'){
		action = 'Renew '+vehicleType
	}
	if(quote_type == 'new'){
		action = 'New '+vehicleType
	}
	if(quote_type == 'expired'){
		action = 'Expired '+vehicleType
	}
	return action;
}

var getBuyPlanData = function(plan, index){
	var userData = getUserData()
	var action = getActionFromQuoteType()
	var policyName = plan.insurerName
	var zeroDepAddon = userData.AllAddons.filter(function(item){
		return item.id == 'addon_isDepreciationWaiver'
	})[0]
	var SelectedAddons = userData.SelectedAddons
	var extras = {}
	extras.param2 = (zeroDepAddon.is_selected? 'Zero Depreciation Added':'Zero Depreciation Not Added')
	extras.param4 = SelectedAddons
	extras.param5 = index + 1

	return {
		action:action,
		policyName: policyName,
		extras: extras
	}
}

var gtm_actions = {
	selectInsuranceType: function(){
		var action = getActionFromQuoteType();
		GTMPush(vehicleType+'EventMobileInsuranceType', vehicleType+' Mobile - Insurance Type', action, '/motor/car-insurance/', {})
	},

	seeQuoteDetailsOnResults: function(showSummary){
		var action = getActionFromQuoteType();
		if(showSummary){
			GTMPush(vehicleType+'EventMobileSeeCarDetails', vehicleType+' Mobile - See Car Details', action, 'Details Clicked', {})
		}else{
			GTMPush(vehicleType+'EventMobileSeeCarDetails', vehicleType+' Mobile - See Car Details', action, 'Hide Clicked', {})
		}
	},

	viewQuotes: function(){
		var action = getActionFromQuoteType();
		var extras = {}
		var userData = getUserData()
		extras.param1 = userData.SelectedVehicle
		extras.param2 = userData.SelectedFuelType
		extras.param3 = userData.SelectedVariant
		extras.param4 = userData.SelectedRTO
		extras.param5 = userData.SelectedRegYear
		var previousNCB = userData.AppData.previousNCB
		var hasClaimedPastyear = userData.AppData.hasClaimedPastYear

		if(hasClaimedPastyear){
			extras.param6 = 'Claimed in past year'
		}
		else{
			if(previousNCB){
				extras.param6 = previousNCB.value
			}
		}
		GTMPush(vehicleType+'EventMobileGetQuote', vehicleType+' Mobile - Get Quote', action, '', extras)
	},

	toggleAddon: function(addonid){
		var action = getActionFromQuoteType(), userAction

		var addon = getUserData().AllAddons.filter(function(item){
			return item.id == addonid
		})[0]

		if(addon){
			if(addon.id == 'addon_isDepreciationWaiver'){
				var category = (addon.is_selected? 'Add Zero Depreciation':'Remove Zero Depreciation')
				GTMPush(vehicleType+'EventMobileZeroDepreciation', vehicleType+' Mobile - ' + category, action, '', {})
			}
			else{
				userAction = (addon.is_selected? 'Checked':'Unchecked')
				GTMPush(vehicleType+'EventMobileAdditionalCovers', vehicleType+' Mobile - Additional Covers', action, addon.name+ ' ' + userAction, {})
			}
		}
	},

	updateQuote: function(){
		var action = getActionFromQuoteType()
		GTMPush(vehicleType+'EventMobileOptimizeQuote', vehicleType+' Mobile - Optimize Quote', action, 'Optimize Quote', {})
	},

	clickEditIDV: function(){
		var action = getActionFromQuoteType()
		GTMPush(vehicleType+'EventMobileEditIDV', vehicleType+' Mobile - Edit IDV', action, 'Edit Clicked', {})
	},


	updateIDV: function(){
		var action = getActionFromQuoteType()
		var idv = getUserData().AppData.idv
		GTMPush(vehicleType+'EventMobileEditIDV', vehicleType+' Mobile - Edit IDV', action, idv, {})
	},

	seePremiumDetails: function(plan, index){
		var buyPlanData = getBuyPlanData(plan, index)
		GTMPush(vehicleType+'EventMobileSeePremium', vehicleType+' Mobile - See Premium Breakup', buyPlanData.action, buyPlanData.policyName, buyPlanData.extras)
	},

	backToResultsFromBreakup: function(){
		var action = getActionFromQuoteType()
		GTMPush(vehicleType+'EventMobileBackResults', vehicleType+' Mobile - Back To Results Premium Popup', action, '', {})
	},

	buyPlan: function(plan, index){
		var buyPlanData = getBuyPlanData(plan, index)
		GTMPush(vehicleType+'EventMobileBuyPlan', vehicleType+' Mobile - Buy Plan', buyPlanData.action, buyPlanData.policyName, buyPlanData.extras)
	}
}

module.exports = gtm_actions