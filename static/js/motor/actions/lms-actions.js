var Cookie = require('react-cookie')
var request = require('superagent')

var lms_actions = {
        sendToLMS: function(data, label, extras, callback){
             var jsonData = {};
                jsonData['campaign'] = 'MOTOR';
                jsonData['label'] = label;
                jsonData['device'] = 'Mobile';
                jsonData['triggered_page'] = window.location.href;
                jsonData['data'] = data;

                for (var key in extras){
                    jsonData[key] = extras[key];
                }

                request
					.post('/leads/save-call-time/')
					.set('Content-Type', 'application/x-www-form-urlencoded')
					.send({
						'data': JSON.stringify(jsonData),
                        'csrfmiddlewaretoken': CSRF_TOKEN
					})
					.end(function(err, res){
						if(callback){
							callback();	
						}
					});
        }
    };


module.exports = lms_actions