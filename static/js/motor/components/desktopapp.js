var React = require('react');
var SiteTopBar = require('./layout/sitebar');

var vehicle_type_verbose = (VEHICLE_TYPE == "fourwheeler")? "Car":"Bike";


// App Components
var PolicyTypeSelector = require('./policy-type-selector')
var VehicleSelector = require('./vehicle-selector')


var desktopapp = React.createClass({

	render: function() {
		return (
			<div>
				<SiteTopBar pagetitle={vehicle_type_verbose+' Insurance'}></SiteTopBar>
				<div className="body-bg"></div>

				<div className="app-container">
					<div className="app-quote-form">
						<h1>Buy {vehicle_type_verbose} Insurance</h1>
						<PolicyTypeSelector />
						<VehicleSelector />
					</div>
				</div>

			</div>
		);
	}

});

module.exports = desktopapp;