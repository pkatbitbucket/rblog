/// <reference path="../../../typings/tsd.d.ts" />

import React = require('react')
import RTOCodes = require('./StateWiseRTOCodes')


class RegistrationNoInput2 extends React.Component<any, any> {
	refs: any
	stateCodeRegExp = /^[a-z]{1,2}$/i
	rtoCodeRegExp = /^[0-9]{1,2}$/
	seriesCodeRegExp = /^[a-z]{1,3}$/i
	lastNumberRegExp = /^[0-9]{1,4}$/
	seperator = "-"

	constructor(props:any){
		super(props)
		var value = ""
		if(props.defaultValue != undefined){
			value = props.defaultValue.join('-')
		}
		this.state= {
			value:value,
			isValid:true,
			isShowError: false
		}
	}

	static defaultProps = {
		seriesCodeExceptions:["UK","UA","GJ"],
		rtoCodeOverrides:["UK","UA","GJ","DL"],
		disabled:false
	}

	static propTypes = {
		onValid:React.PropTypes.any,
		onChange:React.PropTypes.any,
		seriesCodeExceptions:React.PropTypes.any,
		rtoCodeOverrides:React.PropTypes.any,
		defaultValue:React.PropTypes.any,
		disabled:React.PropTypes.bool
	}

	componentWillReceiveProps(nextProps: any) {
		var value
		if(nextProps.defaultValue != undefined){
			value = nextProps.defaultValue.join('-')
		}
		else if(this.state.value != undefined){
			value = this.state.value
		}
		else {
			value = ""
		}
		this.setState({
			value:value
		})
	}

	bifurcateRegNo(regNo :string){
		var regNoTemp = regNo.replace(/-/g, "")
		var regNoArray = regNo.split('-')
		var stateCode
		var rtoCode
		var seriesCode
		var lastNumber
		var rtoCodeMaxLength
		var seriesCodeMaxLength = 3
		

		stateCode = regNoTemp.substring(0, 2).toUpperCase()
		regNoTemp = regNoTemp.slice(2)

		// if its a case of DL, GJ, UA, UK then we dont use our logic to form the rtoCode but we 
		// form rto code on the basis of where user has entered the hyphen
		if(this.props.rtoCodeOverrides.indexOf(stateCode) > -1 && regNoArray[1]!= undefined && regNoArray[1]!=""){
			rtoCode = regNoArray[1]
			// if the formed rto Code is valid at this position we go ahead with it or else we try to match 
			//it at the next position
			if((rtoCode.length < 2 && this.rtoCodeRegExp.test(rtoCode)) ||  this.validateRTOCode(stateCode,rtoCode)){
				regNoTemp = regNoTemp.slice(regNoArray[1].length)
			}
			else
			{
				var tempLastChar = rtoCode.slice(rtoCode.length-1, rtoCode.length)
				//while matching at 3rd position we can have 2 cases.. 1 of special cases like
				//GJ, UA, UK where 3rd place can be digits (see this.props.seriesCodeExceptions) 
				// or can be letters also
				if((regNoArray[2]==undefined || regNoArray[2]=="") 
					&& this.props.seriesCodeExceptions.indexOf(stateCode) > -1 
					&& (this.seriesCodeRegExp.test(tempLastChar) || this.lastNumberRegExp.test(tempLastChar))){
					rtoCode = rtoCode.slice(0, rtoCode.length - 1)
					regNoTemp = regNoTemp.slice(regNoArray[1].length-1)

				}
				// 2nd case of normal states where 3rd place can only be letters
				else if((regNoArray[2]==undefined || regNoArray[2]=="") 
						&& this.props.seriesCodeExceptions.indexOf(stateCode) == -1 
						&& this.seriesCodeRegExp.test(tempLastChar)){
					rtoCode = rtoCode.slice(0, rtoCode.length - 1)
					regNoTemp = regNoTemp.slice(regNoArray[1].length-1)
				}
				// if we cannot find best match for 3rd place we go ahead with the original formed rtoCode
				// and an error will be shown to the user when the validation logic runs in the next function
				else {
					regNoTemp = regNoTemp.slice(regNoArray[1].length)
				}
			}	
		}
		//case of non overrising rto code states (NON DL, UK, UA, GJ) wherein we form the rtoCode
		// on the basis of parsing logic
		else{
			rtoCodeMaxLength = this.getRTOCodeMaxLength(stateCode)
			while (rtoCodeMaxLength > 0 && regNoTemp != ""){
				rtoCode = regNoTemp.substring(0, rtoCodeMaxLength).toUpperCase()
				if(rtoCode.length < 2 && this.rtoCodeRegExp.test(rtoCode)){
					regNoTemp = regNoTemp.slice(rtoCodeMaxLength)
					break
				}
				else if (this.validateRTOCode(stateCode,rtoCode))
				{
					regNoTemp = regNoTemp.slice(rtoCodeMaxLength)
					break
				}
				rtoCodeMaxLength = rtoCodeMaxLength - 1
			}

			// if the rto code  is not valid still we need some thing in place of rto code on screen. So we take 1 character and 
			// remaining character will be checked for 3rd and 4th place
			if(rtoCodeMaxLength == 0 && regNoTemp != ""){
				regNoTemp = regNoTemp.slice(1)
			}
		}

		while (seriesCodeMaxLength > 0 && regNoTemp != "") {
			seriesCode = regNoTemp.substring(0, seriesCodeMaxLength).toUpperCase()
			if (this.seriesCodeRegExp.test(seriesCode)){
				regNoTemp = regNoTemp.slice(seriesCodeMaxLength)
				break
			}
			seriesCodeMaxLength = seriesCodeMaxLength - 1
		}
		// if the series code  is not valid still we need some thing in place of series code on screen. 
		//So we take 1 character and 
		// remaining characters will be checked for 4th place
		// 1 exception is for GJ and UA wherin 3rd place can be empty hence in that case we 
		//dont take any character for 3rd place if its invalid but if they are valid for 4th place
		if(seriesCodeMaxLength == 0 && regNoTemp != ""){
			if(this.props.seriesCodeExceptions.indexOf(stateCode) > -1 && this.lastNumberRegExp.test(seriesCode)){
				seriesCode = undefined
			}
			else{
				regNoTemp = regNoTemp.slice(1)
			}
		}

		if(regNoTemp != ""){
			lastNumber = regNoTemp.substring(0, regNoTemp.length).toUpperCase()
		}
		return [stateCode, rtoCode, seriesCode, lastNumber]
	}

	getRTOCodeMaxLength(stateCode:string){
				var maxLength = 2
				if(stateCode && stateCode.toUpperCase() == "DL"){
					maxLength = 3
				}
				return maxLength
	}

	handleChange(event:any){
		//event.target.value = event.target.value
		var value = event.target.value
		value = value.toUpperCase() //converting to upper case
		value = value.replace(/ /g, ""); //replacing spaces
		//valuesArray = value.split('-')

		var isValid = this.validate(value)
		var isShowError = this.checkShowError(value)
		
		if (!isShowError && isValid) {
			value = this.addSeparator(value)
		}
		else if (isShowError && !isValid) {
			value = this.addSeparator(value)
			//value  = this.setBestMatch(value)
			isValid = this.validate(value)
			isShowError = this.checkShowError(value)
		}

		if(this.state.value != value){
			this.setState({
				value: value,
				isValid:isValid,
				isShowError: isShowError
			},function(){
				
				this.props.onChange()
				if(this.isValid()){
					this.props.onValid(this.getValue())
				}
			})
		}
	}
	//public getters
	public getValue(){
		return this.state.value.split('-')
	}

	public isValid(isShowError: boolean = false){
		var valuesArray = this.state.value.split('-')
		var isValid = this.state.isValid
		var isComplete = false
		if(valuesArray.length == 4 && valuesArray[3] != "" && valuesArray[3]!=undefined){
			isComplete = true
		}
		else if(valuesArray[0] != undefined && valuesArray[0]!="" 
				&& this.props.seriesCodeExceptions.indexOf(valuesArray[0]) > -1 && valuesArray.length == 3 
				&& valuesArray[2] != "" && valuesArray[2]!=undefined){
			isComplete = true
		}

		if((!isValid || !isComplete) && isShowError){
			this.setState({
				isValid:false,
				isShowError: true
			})
		}
		return (isComplete && isValid)
	}
	//

	addSeparator(value: string) {
		
		var valuesArray = this.bifurcateRegNo(value)
		var regNo = ""

		for(var i = 0; i < valuesArray.length; i++){
			if(valuesArray[i] != undefined){
				if(i > 0){
					regNo = regNo + this.seperator
				}
				regNo = regNo + valuesArray[i]
			}
		}
		return regNo
	}

	validate(value: string, isComplete:boolean = false){
		var valuesArray = value.split("-")
		var isSeriesCodeBlank = false
		if(valuesArray[valuesArray.length -1] == "" && !isComplete){
			//if last element is blank ignore it. This is in case where user is still entering the no
			//like MH-02- , in this case the array formed is ["MH", "02",""]. hence last element is popped.valuesArray
			// this will not be done in case the isComplete flag is passed as true. That is if say user presses submit button
			// after entering MH-02-
			valuesArray.pop()
		}
		var isValid = true
		if (valuesArray[0]!=undefined){
			isValid = isValid && this.validateStateCode(valuesArray[0])
		}
		if (valuesArray[1] != undefined) {
			isValid = isValid && this.validateRTOCode(valuesArray[0], valuesArray[1])
		}
		if (valuesArray[2] != undefined) {
			var tempIsValid

			// this is to handle special case of UA and GJ wherein thrid place can be empty. Hence if element
			// at third place is inValid we check whether it is valid for 4th place and if the state code is 
			//either GJ or UA then we say its valid
			tempIsValid = isValid && this.validateAgainstRegEx(this.seriesCodeRegExp, valuesArray[2])
			if(isValid && !tempIsValid && this.props.seriesCodeExceptions.indexOf(valuesArray[0]) > -1
				 && this.validateAgainstRegEx(this.lastNumberRegExp, valuesArray[2])){
				isValid = true
				isSeriesCodeBlank = true
			}
			else {
				isValid = tempIsValid
			}
		}
		if (valuesArray[3] != undefined) {
			if(!isSeriesCodeBlank){
				isValid = isValid && this.validateAgainstRegEx(this.lastNumberRegExp, valuesArray[3])
			}
			else {
				// since if third place is blank then user cannot enter 4 places.. this is an erorr since we are getting 4 places
				//as valuesArray[3] is not undefined
				isValid = false
			}
			
		}
		return isValid
	}

	checkShowError(value: string) {
		
		var valuesArray = value.split("-")
		var noOfSections = valuesArray.length
		var isShowErrorState = false
		var isShowErrorRTO = false
		var isShowErrorSeries = false
		var isShowErrorLastNumber = false
		
		if (valuesArray[0] != undefined) {
			isShowErrorState = (valuesArray[0].length == 2 || !this.stateCodeRegExp.test(valuesArray[0]) || noOfSections == 2)
							&& !this.validateStateCode(valuesArray[0])
		}
		if (valuesArray[1] != undefined) {
			isShowErrorRTO = (valuesArray[1].length == 2 || !this.rtoCodeRegExp.test(valuesArray[1]) || noOfSections == 3)
				&& (!this.validateStateCode(valuesArray[0]) || !this.validateRTOCode(valuesArray[0], valuesArray[1]))
		}	
		if (valuesArray[2] != undefined) {
			isShowErrorSeries = (!this.validateStateCode(valuesArray[0]) || !this.validateRTOCode(valuesArray[0], valuesArray[1])
					|| (!this.validateAgainstRegEx(this.seriesCodeRegExp, valuesArray[2]) && (!this.validateAgainstRegEx(this.lastNumberRegExp, valuesArray[2]) 
								|| this.props.seriesCodeExceptions.indexOf(valuesArray[0]) == -1 ) ))
		}
		if (valuesArray[3] != undefined) {
			isShowErrorLastNumber = (!this.validateStateCode(valuesArray[0]) || !this.validateRTOCode(valuesArray[0], valuesArray[1])
				|| !this.validateAgainstRegEx(this.seriesCodeRegExp, valuesArray[2]) || !this.validateAgainstRegEx(this.lastNumberRegExp, valuesArray[3]))
		}
		return (isShowErrorState || isShowErrorRTO || isShowErrorSeries || isShowErrorLastNumber)
	}

	setBestMatch(value:string){
		var regNo = ""
		var valuesArray = value.split("-")
		if (valuesArray[0] != undefined && valuesArray[0] != "" && this.validateStateCode(valuesArray[0])) {
			regNo = valuesArray[0] + this.seperator
		}
		else if (valuesArray[0] != undefined && valuesArray[0] != "") {
			regNo = valuesArray[0]
		}

		if (valuesArray[2] == undefined && valuesArray[1] != undefined) {
			if (!this.validateRTOCode(valuesArray[0], valuesArray[1])
				&&
				this.validateRTOCode(valuesArray[0], valuesArray[1].substring(0, valuesArray[1].length -1))
				&& this.validateAgainstRegEx(this.seriesCodeRegExp, valuesArray[1].slice(-1))
			) {
				regNo = regNo + valuesArray[1].substring(0, valuesArray[1].length - 1) + this.seperator + valuesArray[1].slice(-1)
			}
			else {
				regNo = regNo + valuesArray[1]
			}
		}
		else if (valuesArray[1] != undefined && this.validateRTOCode(valuesArray[0], valuesArray[1])) {
			regNo = regNo + valuesArray[1] + this.seperator
		}
		else if (valuesArray[1] != undefined) {
			regNo = regNo + valuesArray[1]
		}


		if (valuesArray[2] != undefined && valuesArray[3] == undefined) {
			if(!this.validateAgainstRegEx(this.seriesCodeRegExp, valuesArray[2])
				&&
				this.validateAgainstRegEx(this.seriesCodeRegExp, valuesArray[2].substring(0, valuesArray[2].length - 1))
				&& this.validateAgainstRegEx(this.lastNumberRegExp, valuesArray[2].slice(-1))
				){
				regNo = regNo + valuesArray[2].substring(0, valuesArray[2].length - 1) + this.seperator + valuesArray[2].slice(-1)
			}
			else {
				regNo = regNo + valuesArray[2]
			}
		}
		else if (valuesArray[2] != undefined && this.validateAgainstRegEx(this.seriesCodeRegExp, valuesArray[2])) {
			regNo = regNo + valuesArray[2] + this.seperator
		}
		else if (valuesArray[2] != undefined) {
			regNo = regNo + valuesArray[2]
		}

		if (valuesArray[3] != undefined) {
			regNo = regNo + valuesArray[3]
		}
		return regNo
	}

	validateStateCode(stateCode: string) {
		var isValid
		if (RTOCodes[stateCode.toUpperCase()] == undefined) {
			isValid = false
		}
		else {
			isValid = true
		}
		return isValid
	}

	validateRTOCode(stateCode: string, rtoCode: string) {
		var isValid
		if (stateCode && RTOCodes[stateCode]) {
			if ((
				this.rtoCodeRegExp.test(rtoCode) &&
				parseInt(rtoCode) >= RTOCodes[stateCode].value[0] &&
				parseInt(rtoCode) <= RTOCodes[stateCode].value[1] &&
				RTOCodes[stateCode].excludes.indexOf(parseInt(rtoCode)) <= -1)
				||
				RTOCodes[stateCode].includes.indexOf(rtoCode.toString()) > -1
			) {
				isValid = true
			}
			else {
				isValid = false
			}
		}
		else {
			isValid = this.rtoCodeRegExp.test(rtoCode)
		}
		return isValid
	}

	validateAgainstRegEx(regExp: any, testExp: string) {
		return regExp.test(testExp)
	}


	render(){
		
		var textBoxStyle = {width:'365px'}
		var errorStyle = { border: '2px solid red', width: '365px' }
		if (!this.state.isValid && this.state.isShowError) {
			textBoxStyle = errorStyle
		}

		return (
			<div className="w--reg_no_input">
				<input type="text"  style={textBoxStyle}
					value={this.state.value}
					onChange={this.handleChange.bind(this)}
					placeholder={'eg. MH-02-BX-0377'}
					//onKeyUp={this.handleChange.bind(this)}
					disabled={this.props.disabled}
					/>

				{!this.state.isValid && this.state.isShowError ?
				<p className='fs w--error'>
					Please enter a valid Car Registration Number
					</p> : null
				}

			</div>
			)
	}
}

export = RegistrationNoInput2