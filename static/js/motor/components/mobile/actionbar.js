import React from 'react'
import Transition from 'react-addons-css-transition-group'
import Button from './ui/button'
import Switch from './ui/toggleswitch'
import Modal from './ui/modal'
import HalfModal from './ui/halfmodal'
import AddonItem from './addonitem'
import ListItem from './ui/listitem'
import TransitionTimeout from '../../constants/transition-timeout'
import ACStore from '../../stores/additional-cover-store'
import AppStore from '../../stores/app-store'
import PlanStore from '../../stores/plan-store'
import AppActions from '../../actions/app-actions'
import GTMActions from '../../actions/gtm-actions'
import RenderInBody from './ui/renderinbody'
import IDVSelector from './idvselector'
import utils from '../../utils/apputils'
import Apiutils from '../../utils/apiutils'

function getStateFromStores(){
	return {
		addons: ACStore.getAddons(),
		selectedAddons: ACStore.getSelectedAddons(),
		idv: AppStore.getData().idv,
		maxIDV: PlanStore.getMaxIDV(),
		minIDV: PlanStore.getMinIDV()
	}
}

var actionbar = React.createClass({

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		ACStore.addChangeListener(this._onChange)
		AppStore.addChangeListener(this._onChange)
		PlanStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		ACStore.removeChangeListener(this._onChange)
		AppStore.removeChangeListener(this._onChange)
		PlanStore.removeChangeListener(this._onChange)
	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	openAddonSelector: function(){
		this.setState({
			modal:'addons'
		})
	},

	closeAddonSelector: function(){
		this.setState({
			modal:null
		})
	},

	openIDVselector: function(){
		this.setState({
			modal:'idv'
		}, function(){
			GTMActions.clickEditIDV()
		})
	},

	closeIDVSelector: function(){
		this.setState({
			modal:null
		})
	},

	closeModal: function(){
		this.setState({
			modal:null
		})
	},

	toggleAddon: function(addon){
		AppActions.toggleAddon(addon.id, function(){
			GTMActions.toggleAddon(addon.id)	
		})

		AppActions.sortPlans()
		
	},

	toggleZeroDep: function(){
		AppActions.toggleAddon('addon_isDepreciationWaiver', function(){
			GTMActions.toggleAddon('addon_isDepreciationWaiver')
		})

		AppActions.sortPlans()
	},

	handleIDVChange: function(){
		var idv = this.refs.idvValue.getDOMNode().value

		if(idv == ''){
			idv = 0
		}
		if((!isNaN(idv) && idv < this.state.maxIDV && idv > this.state.minIDV) || idv == 0){
			this.setState({
				modal:null
			})
			AppActions.setIDV(idv, function(){
				GTMActions.updateIDV()
				AppActions.startFetchingPlans()
				Apiutils.getPlans(appStore.getQuoteRequest())
			})
		}
		else{
			this.setState({
				idvError:true
			})
		}
	},

	handleIDVUpdate: function(){
		this.closeIDVSelector()
	},

	render: function() {
		var sa = this.state.selectedAddons

		var selectedAddonIds = sa.map(function(addon){
			return addon.id
		})

		var zeroDepSelected = (selectedAddonIds.indexOf('addon_isDepreciationWaiver') > -1)


		var numberOfAddonsSelected = sa.length

		if(zeroDepSelected){
			numberOfAddonsSelected = numberOfAddonsSelected-1
		}

		var addonSelectorContent = this.state.addons.map(function(addon, index){
			return <AddonItem key={index} addon={addon} onToggle={this.toggleAddon.bind(this, addon)} />
		}, this)

		var IDVLeft = <div>
		<span className='block-title'>IDV</span>
		<span className='block-subtitle'>{this.state.idv == 0? 'Change':'Rs.' + utils.numberToINR(parseInt(this.state.idv))}</span>
		</div>

		var ZeroDepLeft  

		if(zeroDepSelected){
			ZeroDepLeft = <div><span className='action'>Remove</span> Zero Depreciation</div>
		}
		else{
			ZeroDepLeft = <div><span className='action'>Add</span> Zero Depreciation</div>
		}

		return (
			<div className={'actionbar '+(this.props.customClass? this.props.customClass:'')}>
				<ListItem onClick={this.toggleZeroDep} customClass='thin inline' left={ZeroDepLeft} />
				<ListItem customClass='thin inline' onClick={this.openAddonSelector} left='Additional Covers' />
				<ListItem customClass='thin inline' left={IDVLeft} onClick={this.openIDVselector} />

				<RenderInBody>
				<Transition transitionName='modal' {...TransitionTimeout} >
					{this.state.modal == 'addons'? <Modal onClose={this.closeAddonSelector} header="Select Addons" content={addonSelectorContent} />:null }
				</Transition>
				</RenderInBody>

				<RenderInBody>
				<Transition transitionName='modal' {...TransitionTimeout} >
					{this.state.modal == 'idv'? <Modal customClass='modal-idv' onClose={this.closeIDVSelector} header="Change your car IDV" hangingContent={<ListItem customClass='thin card full-width' left="Current IDV" right={this.state.idv==0? 'For best deal':'Rs.' + utils.numberToINR(parseInt(this.state.idv))} />} content={<IDVSelector onUpdate={this.handleIDVUpdate} />} />:null }
				</Transition>
				</RenderInBody>
				
			</div>
		)
	}

})

module.exports = actionbar
