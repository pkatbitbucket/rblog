import React from 'react'
import ListItem from './ui/listitem'
import Switch from './ui/toggleswitch'


var addonitem = React.createClass({

	getInitialState: function() {
		return {}
	},

	toggleDesc: function(){
		this.setState({
			showdesc: (this.state.showdesc? false:true)
		});
	},

	render: function() {
		var addon = this.props.addon

		var addonLeft = (<div onClick={this.toggleDesc} >
				{addon.name}<span className='q-mark'>{this.state.showdesc? '-':'?'}</span>
			</div>)
		return (
			<div className='addon-selector-item'>
				<ListItem customClass="thin" left={addonLeft} right={<Switch on={addon.is_selected} onClick={this.props.onToggle} />}/>
				{this.state.showdesc? <ListItem customClass="thin" left={<span className='small-text'>{addon.desc}</span>} />:null}
			</div>
		);
	}

});


module.exports = addonitem