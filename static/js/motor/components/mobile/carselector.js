import React from 'react'
import Transition from 'react-addons-css-transition-group';
import TransitionTimeout from '../../constants/transition-timeout'
import VehicleStore from '../../stores/vehicle-store'
import AppActions from '../../actions/app-actions'

import ListItem from './ui/listitem'
import Button from './ui/button'
import ModalSelector from './ui/modalselector'
import PopularVehiclesGrid from './popularvehiclesgrid'

import RenderInBody from './ui/renderinbody';

function getStateFromStores(){
	return {
		vehicles: VehicleStore.getVehicles(),
		fuelTypes: VehicleStore.getFuelTypes(),
		variants: VehicleStore.getVariants(),
		selectedVehicle: VehicleStore.getSelectedVehicle(),
		selectedVariant: VehicleStore.getSelectedVariant(),
		selectedFuelType: VehicleStore.getSelectedFuelType()
	}
}

var all_fuel_types = [
	{
		key:'Petrol',
		value: 'PETROL'
	},
	{
		key:'Diesel',
		value: 'DIESEL'
	},
	{
		key:'CNG/LPG Company fitted',
		value: 'INTERNAL_LPG_CNG'
	},
	{
		key:'CNG/LPG Externally fitted',
		value: 'EXTERNAL_LPG_CNG'
	}
]



var carselector = React.createClass({

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		VehicleStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		VehicleStore.removeChangeListener(this._onChange)	
	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	openCarModelSelector: function(){
		this.setState({modal: 'models'})
	},

	closeCarModelSelector: function(){
		this.setState({modal: null})	
	},

	openFuelTypeSelector: function(){
		this.setState({modal: 'fuel_type'})
	},

	closeFuelTypeSelector: function(){
		AppActions.deselectVehicle()
		this.setState({modal: null})
		var self = this
		setTimeout(function(){
			self.openCarModelSelector()
		}, 200)
	},

	openCarVariantSelector: function(){
		this.setState({modal: 'variants'})
	},

	closeVariantSelector: function(){
		this.setState({modal: null})
		var self = this
		setTimeout(function(){
			if(self.state.fuelTypes.length > 1){
				self.openFuelTypeSelector()
			}
			else{
				self.openCarModelSelector()	
			}
		}, 200)
	},

	onModelSelect: function(model){
		var self = this
		self.setState({
			loadingVariants: true
		});
		AppActions.selectVehicle(model, function(){
			self.setState({modal: null})
			if(self.state.fuelTypes.length > 1){
				self.openFuelTypeSelector()
			}
			else{
				self.openCarVariantSelector()	
			}
			self.setState({
				loadingVariants: false
			});
		})
	},

	onFuelTypeSelect: function(fuel_type){
		AppActions.selectFuelType(fuel_type.value)
		this.openCarVariantSelector()
	},

	onVariantSelect: function(variant){
		AppActions.selectVariant(variant)
		this.setState({
			modal:null
		});
	},

	render: function() {

		var fuelTypes = this.state.fuelTypes
		var variants = this.state.variants

		var fuelTypeOptions = []


		// Dirty Code commence
		for (var i = 0; i < all_fuel_types.length; i++) {
			if(fuelTypes.indexOf(all_fuel_types[i].value) > -1){
				fuelTypeOptions.push(all_fuel_types[i])
			}
		};


		// Dirty Code ends		

		var selectedFuelType = this.state.selectedFuelType
		var selectedFuelTypeVerbose

		if(selectedFuelType){
			selectedFuelTypeVerbose = all_fuel_types.filter(function(item){
				return  selectedFuelType == item.value
			})[0].key;
		}
		

		var forwardArrow = <img className='forward-arrow spaced-both' src={STATIC_URL + 'img/motor/mobile/forward.png'} />

		var showVariantsModal = false
		var showFuelTypesModal = false


		var addIcon = <div className='add-icon'>+</div>
		var editIcon = <div className='edit-icon'>Edit</div>

		var right = addIcon
		var left = 'Which car do you drive?'

		

		if(this.state.selectedVehicle && this.state.selectedVariant){
			right = editIcon
			left = (<div>
				<span className="left-title">Your car</span>
				<span className="left-sub">{this.state.selectedVehicle.name+' '+this.state.selectedVariant.name}</span>
				</div>
				)
		}


		var vehicleTitle = <div>
			<span className='block-title'>What car do you drive?</span>
			<span className='block-subtitle'>Choose from the list of popular cars below or search for your car above.</span>
		</div>

		var fuelTypeTitle = <div>
			<span className='block-title'>What fuel do you use in your <strong>{this.state.selectedVehicle? this.state.selectedVehicle.name:null}</strong> ?</span>
		</div>

		var variantTitle = <div>
			<span className='block-title'>Which variant of the <strong>{selectedFuelTypeVerbose} {this.state.selectedVehicle? this.state.selectedVehicle.name:null}</strong> do you own?</span>
		</div>



		return (
			<div className="car-selector-wrapper">
				<RenderInBody>
					<Transition transitionName='modal' {...TransitionTimeout} >
						{this.state.modal == 'models'? <ModalSelector 
														overlayLoading={this.state.loadingVariants} 
														showSearch={true} 
														searchText="Search and select your car"  
														onClose={this.closeCarModelSelector} 
														options={this.state.vehicles} 
														optionsText='name' 
														onSelect={this.onModelSelect} 
														customContent={<PopularVehiclesGrid onSelect={this.onModelSelect} />}
														customHangingContent={<ListItem customClass='thin card full-width' left={vehicleTitle} />} />:null }
					</Transition>
				</RenderInBody>

				<RenderInBody>
					<Transition transitionName='modal' {...TransitionTimeout} >
						{this.state.modal == 'fuel_type'? <ModalSelector 
															onClose={this.closeFuelTypeSelector} 
															showSearch={false} 
															header="Select the fuel used in the car" 
															options={fuelTypeOptions}   
															optionsText='key' 
															onSelect={this.onFuelTypeSelect} 
															customHangingContent={<ListItem customClass='thin card full-width' left={fuelTypeTitle} />}/>:null }
					</Transition>
				</RenderInBody>

				<RenderInBody>
					<Transition transitionName='modal' {...TransitionTimeout} >
						{this.state.modal == 'variants'? <ModalSelector 
															onClose={this.closeVariantSelector} 
															showSearch={false} 
															header="Select the car variant" 
															options={variants}  
															optionsText='name' 
															onSelect={this.onVariantSelect} 
															customHangingContent={<ListItem customClass='thin card full-width' left={variantTitle} />} />:null }
					</Transition>
				</RenderInBody>

				<ListItem left={left} right={right} onClick={this.openCarModelSelector} />
			</div>
		)
	}

});

module.exports = carselector
