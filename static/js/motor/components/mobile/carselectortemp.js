import React from 'react'
import Transition from 'react-addons-css-transition-group'
import TransitionTimeout from '../../constants/transition-timeout'
import VehicleStore from '../../stores/vehicle-store'
import AppActions from '../../actions/app-actions'

import ListItem from './ui/listitem'
import Button from './ui/button'
import Modal from './ui/modal'
import ModalSelector from './ui/modalselector'
import PopularVehiclesGrid from './popularvehiclesgrid'

import RenderInBody from './ui/renderinbody';

function getStateFromStores(){
	return {
		vehicles: VehicleStore.getVehicles(),
		fuelTypes: VehicleStore.getFuelTypes(),
		variants: VehicleStore.getVariants(),
		selectedVehicle: VehicleStore.getSelectedVehicle(),
		selectedVariant: VehicleStore.getSelectedVariant(),
		selectedFuelType: VehicleStore.getSelectedFuelType(),
		cngKitValue: VehicleStore.getCNGKitValue()
	}
}

var all_fuel_types = [
	{
		key:'Petrol',
		value: 'PETROL'
	},
	{
		key:'Diesel',
		value: 'DIESEL'
	},
	{
		key:'CNG/LPG Company fitted',
		value: 'INTERNAL_LPG_CNG'
	},
	{
		key:'CNG/LPG Externally fitted',
		value: 'EXTERNAL_LPG_CNG'
	}
]



var carselector = React.createClass({

	getInitialState: function() {
		var states = getStateFromStores()
		states.errorCNG = false
		states.errorMessage = null
		return states
	},

	componentDidMount: function() {
		VehicleStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		VehicleStore.removeChangeListener(this._onChange)	
	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	openCarModelSelector: function(){
		this.setState({modal: 'models'})
	},

	closeCarModelSelector: function(){
		this.setState({modal: null})	
	},

	openFuelTypeSelector: function(){
		this.setState({modal: 'fuel_type'})
	},

	closeFuelTypeSelector: function(){
		this.setState({modal: null})
	},

	openCarVariantSelector: function(){
		this.setState({modal: 'variants'})
	},

	closeVariantSelector: function(){
		this.setState({modal: null})
	},

	onModelSelect: function(model){
		var self = this
		self.setState({
			loadingVariants: true
		});
		AppActions.selectVehicle(model, function(){
			self.setState({modal: null})
			if(self.state.fuelTypes.length > 1){
				self.openFuelTypeSelector()
			}
			else{
				self.openCarVariantSelector()	
			}
			self.setState({
				loadingVariants: false
			});
		})
	},

	onFuelTypeSelect: function(fuel_type){
		if(fuel_type.value !== 'EXTERNAL_LPG_CNG'){
			AppActions.selectFuelType(fuel_type.value)
			this.openCarVariantSelector()
		}else{
			this.setState({modal: 'cngkit'})
		}
	},

	onCNGKitValueUpdate: function(){
		var value = this.refs.cngkitvalue.value
		if(value<1 || value>40000){
			this.setState({ 
				errorCNG: true,
				errorMessage: value>40000 ? "Maximum permissible value for CNG LPG Kit is 40000" : "CNG LPG Kit Value cannot be zero or negative"
			})
		}else{
			AppActions.selectFuelType('EXTERNAL_LPG_CNG')
			AppActions.setCNGKitValue(value)
			this.openCarVariantSelector()
		}
	},

	resetError: function(e){
		if(e.target.value<1 || e.target.value>40000)
			this.setState({ errorCNG: true })
		else
			this.setState({ errorCNG: false })
	},

	onVariantSelect: function(variant){
		AppActions.selectVariant(variant)
		this.setState({
			modal:null
		});
	},

	render: function() {

		var fuelTypes = this.state.fuelTypes
		var variants = this.state.variants

		var fuelTypeOptions = []


		// Dirty Code commence
		for (var i = 0; i < all_fuel_types.length; i++) {
			if(fuelTypes.indexOf(all_fuel_types[i].value) > -1){
				fuelTypeOptions.push(all_fuel_types[i])
			}
		};


		// Dirty Code ends		

		var selectedFuelType = this.state.selectedFuelType
		var selectedFuelTypeVerbose

		if(selectedFuelType){
			selectedFuelTypeVerbose = all_fuel_types.filter(function(item){
				return  selectedFuelType == item.value
			})[0].key;
		}
		

		var forwardArrow = <img className='forward-arrow spaced-both' src={STATIC_URL + 'img/motor/mobile/forward.png'} />

		var showVariantsModal = false
		var showFuelTypesModal = false


		var addIcon = <div className='add-icon'>+</div>
		var editIcon = <div className='edit-icon'>Edit</div>

		var left = 'Which car do you drive?'
		var modelleft = 'Which car do you drive?'
		var fuelleft = 'Choose Fuel Type'
		var variantleft = 'Choose Variant'


		

		if(this.state.selectedVehicle && this.state.selectedVariant){
			left = (<div>
				<span className="left-title">Your car</span>
				<span className="left-sub">{this.state.selectedVehicle.name+' '+this.state.selectedVariant.name}</span>
				</div>
				)
		}


		if(this.state.selectedVehicle){
			modelleft = (<div>
				<span className="left-title">Your car</span>
				<span className="left-sub">{this.state.selectedVehicle.name}</span>
				</div>
				)
		}

		if(this.state.selectedVariant){
			variantleft = (<div>
				<span className="left-title">Car Variant</span>
				<span className="left-sub">{this.state.selectedVariant.name}</span>
				</div>
				)
		}

		if(this.state.selectedFuelType){
			fuelleft = (<div>
				<span className="left-title">Fuel Type</span>
				<span className="left-sub">{selectedFuelTypeVerbose}</span>
				</div>
				)
		}


		var vehicleTitle = <div>
			<span className='block-title'>What car do you drive?</span>
			<span className='block-subtitle'>Choose from the list of popular cars below or search for your car above.</span>
		</div>

		var fuelTypeTitle = <div>
			<span className='block-title'>What fuel do you use in your <strong>{this.state.selectedVehicle? this.state.selectedVehicle.name:null}</strong> ?</span>
		</div>

		var variantTitle = <div>
			<span className='block-title'>Which variant of the <strong>{selectedFuelTypeVerbose} {this.state.selectedVehicle? this.state.selectedVehicle.name:null}</strong> do you own?</span>
		</div>

		if(this.state.modal === 'cngkit'){
			var emStyle = {
				color: '#f00',
				textAlign: 'center',
				fontSize: '12px'
			}
			var cngkitvalue = <input ref='cngkitvalue' onChange={this.resetError} min={1} max={40000} type='number' defaultValue={this.state.cngKitValue} />
			var nextbutton = <Button onClick={this.onCNGKitValueUpdate}>Continue</Button>
			var cngkit = (<div>
							<ListItem customClass='thin left-center' left={cngkitvalue} />
							{this.state.errorCNG ? <p className="fs error" style={emStyle}>{this.state.errorMessage}</p> : null }
							<ListItem customClass='thin left-center' left={nextbutton} />
						</div>)
		}

		return (
			<div className="car-selector-wrapper">
				<RenderInBody>
					<Transition transitionName='modal' {...TransitionTimeout} >
						{this.state.modal == 'models'? <ModalSelector 
														overlayLoading={this.state.loadingVariants} 
														showSearch={true} 
														searchText="Search and select your car"  
														onClose={this.closeCarModelSelector} 
														options={this.state.vehicles} 
														optionsText='name' 
														onSelect={this.onModelSelect} 
														customContent={<PopularVehiclesGrid onSelect={this.onModelSelect} />}
														customHangingContent={<ListItem customClass='thin card full-width' left={vehicleTitle} />} />:null }
					</Transition>
				</RenderInBody>

				<RenderInBody>
					<Transition transitionName='modal' {...TransitionTimeout} >
						{this.state.modal == 'fuel_type'? <ModalSelector 
															onClose={this.closeFuelTypeSelector} 
															showSearch={false} 
															header="Select the fuel used in the car" 
															options={fuelTypeOptions}   
															optionsText='key' 
															onSelect={this.onFuelTypeSelect} 
															customHangingContent={<ListItem customClass='thin card full-width' left={fuelTypeTitle} />}/>:null }
					</Transition>
				</RenderInBody>

				<RenderInBody>
					<Transition transitionName='modal' {...TransitionTimeout} >
						{this.state.modal == 'cngkit'? <Modal 
														onClose={this.openFuelTypeSelector}
														header="Enter your Car's CNG Kit value"
														content={cngkit} /> : null}
					</Transition>
				</RenderInBody>

				<RenderInBody>
					<Transition transitionName='modal' {...TransitionTimeout} >
						{this.state.modal == 'variants'? <ModalSelector 
															onClose={this.closeVariantSelector} 
															showSearch={false} 
															header="Select the car variant" 
															options={variants}  
															optionsText='name' 
															onSelect={this.onVariantSelect} 
															customHangingContent={<ListItem customClass='thin card full-width' left={variantTitle} />} />:null }
					</Transition>
				</RenderInBody>

				<ListItem customClass="thin" left={modelleft} right={this.state.selectedVehicle ? editIcon : addIcon} onClick={this.openCarModelSelector} />
				<ListItem customClass="thin" left={fuelleft} right={this.state.selectedFuelType ? editIcon : addIcon} onClick={this.openFuelTypeSelector} />
				<ListItem customClass="thin" left={variantleft} right={this.state.selectedVariant ? editIcon : addIcon} onClick={this.openCarVariantSelector} />
			</div>
		)
	}

});

module.exports = carselector
