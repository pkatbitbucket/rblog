var React = require('react');
import ListItem from './ui/listitem'
import VehicleStore from '../../stores/vehicle-store'
import AppActions from '../../actions/app-actions'

function getStateFromStores(){
	return {
		cngKitValue: VehicleStore.getCNGKitValue()
	}
}

var cngvalueinput = React.createClass({

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		VehicleStore.addChangeListener(this._onChange)
		var valid = false;
		var reg = /^[1-9]\d*$/
		if(reg.test(this.state.cngKitValue)){
			if(this.state.cngKitValue <= 40000){
				valid = true
			}			
		}

		this.setState({
				cngValueValid: valid
			});
	},

	componentWillUnmount: function() {
		VehicleStore.removeChangeListener(this._onChange)	
	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	handleChange: function(event){
		AppActions.setCNGKitValue(event.target.value)

		var valid = false
		var reg = /^[1-9]\d*$/
		if(reg.test(event.target.value)){
			if(event.target.value <= 40000){
				valid = true
			}			
		}

		this.setState({
				cngValueValid: valid
			});
	},

	render: function() {
		var left = (<div>
			<span className="left-title">CNG/LPG Kit Value</span>
			<span className="left-sub"><input ref='cngkit' value={this.state.cngKitValue} className='mobileNo' placeholder={'CNG Kit Value'} type='tel' onChange={this.handleChange} /></span>
			<span className="left-subtitle">Enter the value of the CNG/LPG kit fitted in your car</span>
		</div>)

		return (
			<div>
				<ListItem left={left} />
				{this.props.showError && !this.state.cngValueValid? <ListItem customClass='xthin error' left={'Please enter a valid CNG Kit value less than 40000.'} />:null}
			</div>
		);
	}

});

module.exports = cngvalueinput;