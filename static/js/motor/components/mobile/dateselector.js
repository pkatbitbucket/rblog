var React = require('react')
import AppStore from '../../stores/app-store'
import DateStore from '../../stores/date-store'
import VehicleStore from '../../stores/vehicle-store'
import Button from './ui/button'
import moment from 'moment'
import AppActions from '../../actions/app-actions'
import ListItem from './ui/listitem'
import NCBSelector from './ncbselector'

function getStateFromStores(){
	return {
		allDates: DateStore.getAllDates(),
		expiredBeforeNintyDays: DateStore.isExpiredBeforeNintyDays(),
		showRegDate: VehicleStore.hasFastlaneDataChanged(),
		appData: AppStore.getData()
	}
}

var dateselector = React.createClass({

	getInitialState: function() {
		return getStateFromStores()
	},
	componentDidMount: function() {
		AppStore.addChangeListener(this._onChange)
		DateStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this._onChange)
		DateStore.removeChangeListener(this._onChange)
	},

	componentWillUpdate: function(nextProps, nextState){
		if(this.state.appData.isNCBCertificate !== nextState.appData.isNCBCertificate
			|| this.state.appData.newNCB !== nextState.appData.newNCB){
			nextState.error = null
		}
	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	handleNCBChange: function(){
		var changed = true;
		if  (this.state.appData.isUsedVehicle != 1 &&
			((this.state.appData.previousNCB && this.state.appData.previousNCB.value !== this.refs.previousNCB.getOriginalNCB()) ||
			(this.refs.previousNCB && this.refs.previousNCB.getClaimedValue() !== this.state.appData.hasClaimedPastYear))){

			this.refs.previousNCB.setOriginalNCB();
			this.refs.previousNCB.setClaimedValue();
		}else if(this.state.appData.isUsedVehicle == 1 && ((this.state.appData.newNCB && this.state.appData.newNCB.value !== this.refs.newNCB.getOriginalNCB()) ||
				(this.refs.newNCB.getClaimedValue() !== this.state.appData.isNCBCertificate))){
			this.refs.newNCB.setOriginalNCB();
			this.refs.newNCB.setClaimedValue();
		}else{
			changed = false;
		}
		return changed;
	},

	handleDateChange: function(){
		if(this.state.appData.isNewVehicle == 0 || this.state.appData.isUsedVehicle == 1){
			var regDate
			var pypeDate = this.refs.pypeDate ? this.refs.pypeDate.value : null

			// Registration Date
			if(this.state.showRegDate){
				regDate = this.refs.regDate.value
				if(!regDate){
					this.setState({
						error: 'Please enter a valid registration date.'
					})
					return false;
				}
				regDate = moment(regDate, "YYYY-MM-DD")
				if(moment().startOf('day').diff(regDate, 'months') < 6){
					this.setState({
						error: 'Registration date should be atleast more than 6 months in the past'
					})
					return false;
				}
			}

			// Past Polict Expired Date
			if(this.refs.pypeDate && this.refs.pypeDate.value){
				if(moment(this.refs.pypeDate.value).isBefore(this.state.allDates.registrationDate)){
					this.setState({
						error: 'Policy Expiry Date cannot be before registration date'
					})
					return false;
				}

				// Policy Expiry date cannot be more than 60 days
				else if(moment().add(60, "days").isBefore(this.refs.pypeDate.value)){
					var t_60 = moment(this.refs.pypeDate.value).add(-60, "days");

					this.setState({
						error: 	'You will be allowed to renew your policy on or after ' +
								t_60.format("DD MMMM, YYYY") +
								'. Please call our Toll Free Number '+ TOLL_FREE +' for any assistance.'
					})
					return false;
				}
			}

			if(this.refs.pypeDate && !pypeDate){
				this.setState({
					error: 'Please enter a valid previous policy expiry date.'
				})
				return false;
			}

			if(this.refs.newNCB && this.state.appData.isNCBCertificate && this.state.appData.newNCB == undefined){
				this.setState({
					error: 'Please select your new No Claim Bonus(NCB) value.'
				})
				return false;
			}

			pypeDate = this.refs.pypeDate ? moment(pypeDate, "YYYY-MM-DD") : null

			var self = this
			var changed = (this.state.showRegDate && this.state.allDates.registrationDate.diff(regDate, 'days') != 0) ||
							(this.refs.pypeDate && this.state.allDates.pastPolicyExpiryDate.diff(pypeDate, 'days') != 0) ||
							this.handleNCBChange(); // Check if NCB is changed or not

			if(changed){
				if(this.state.showRegDate)
					AppActions.setDate('reg', regDate, function(){})
				if(this.refs.pypeDate)
					AppActions.setDate('pyp', pypeDate, function(){})
			}

			setTimeout(function(){
				self.props.onUpdate(changed)
			}, 300)
		}

		if(this.state.appData.isNewVehicle == 1){
			var npsDate = this.refs.npsDate.value
			var manDate = this.refs.manDate.value

			if(!npsDate){
				this.setState({
					error: 'Please enter a valid new policy start date.'
				})
				return
			}

			if(!manDate){
				this.setState({
					error: 'Please enter a valid manufacturing date.'
				})
				return
			}

			npsDate = moment(npsDate, "YYYY-MM-DD")

			if(npsDate.startOf('day') < moment().startOf('day')){
				this.setState({
					error: 'New policy start date cannot be in the past'
				})
				return
			}

			if(npsDate.diff(moment(), 'days') > 45){
				this.setState({
					error: 'New policy start date cannot be more than 45 days in future'
				})
				return
			}

			manDate = moment(manDate, "YYYY-MM-DD")

			if(moment().startOf('day').diff(manDate, 'months') > 6){
				this.setState({
					error: 'Manufacturing date cannot be more than 6 months in past'
				})
				return
			}

			var self = this

			var self = this
			var changed = true
			if (this.state.allDates.newPolicyStartDate.diff(npsDate, 'days') == 0 ||
				this.state.allDates.manufacturingDate.diff(manDate, 'days') == 0 ){
				changed = false
			}

			AppActions.setDate('nps', npsDate, function(){})
			AppActions.setDate('man', manDate, function(){})

			setTimeout(function(){
				self.props.onUpdate(changed)
			}, 300)
		}

		this.setState({
			error:null
		})
	},

	render: function() {
		var dates = this.state.allDates;
		var ds;

		if(this.state.appData.isNewVehicle == 0 || this.state.appData.isUsedVehicle == 1){
			var t_180 = moment().add(-180, "days");
			var noOfYears = moment().month()>9? 15:14;
			var minRegDate = moment((moment().year() - noOfYears) + "-01-01")

			var t_60 = moment().add(60, "days");

			var minManDate = moment().add(-2, "years");

			ds= <div className='date-selector'>
				{
					this.state.showRegDate ?
						<ListItem
							customClass='thin'
							left={
									<div>
										<span>{'Exact registration date of your car'}</span>
										<input
											ref='regDate'
											min= {minRegDate.format('YYYY-MM-DD')}
											max= {t_180.format('YYYY-MM-DD')}
											type='date'
											defaultValue={dates.registrationDate.format('YYYY-MM-DD')} />
									</div>
								} />
					: null}

				{this.state.appData.isUsedVehicle == 1 ? <NCBSelector ref="newNCB" forNewNCB={true} /> :
				<div><ListItem
					customClass='thin'
					left={
							<div>
								<span>{'Previous year policy expiry date'}</span>
								<input
									ref='pypeDate'
									min= {this.state.allDates.registrationDate? this.state.allDates.registrationDate.format('YYYY-MM-DD'): moment().add(-16, years)}
									max= {t_60.format('YYYY-MM-DD')}
									type='date'
									defaultValue={dates.pastPolicyExpiryDate.format('YYYY-MM-DD')} />
							</div>
						} />{this.state.expiredBeforeNintyDays ? null : <NCBSelector ref="previousNCB" />}</div>}
				{this.state.error ? <ListItem customClass='xthin error' left={this.state.error} /> : null}
			</div>
		}

		if(this.state.appData.isNewVehicle == 1){
			ds= <div className='date-selector'>
				<ListItem
					customClass='thin'
					left={
							<div>
								<span>{'When do you want your policy to start?'}</span>
								<input
									ref='npsDate'
									type='date'
									min= {moment().add(1, "days").format('YYYY-MM-DD')}
									max= {moment().add(7, "days").format('YYYY-MM-DD')}
									defaultValue={dates.newPolicyStartDate.format('YYYY-MM-DD')} />
							</div>
						} />

				<ListItem
					customClass='thin'
					left={
							<div>
								<span>{'When was your car manufactured?'}</span>
								<input
									ref='manDate'
									min= {minManDate.format('YYYY-MM-DD')}
									max={moment().format('YYYY-MM-DD')}
									type='date'
									defaultValue={dates.manufacturingDate.format('YYYY-MM-DD')} />
							</div>
						} />
				{this.state.error? <ListItem customClass='xthin error' left={this.state.error} />:null}
			</div>
		}


		return (
			<div className="date-selector-wrapper">
				<h3></h3>
				<p className='subtitle'>Need a little more information about your car before you buy.</p>
				{ds}
				<div className="action-block">
					<Button onClick={this.handleDateChange} >Update Quotes</Button>
					{/*<Button onClick={this.props.close} >I will fill this later</Button> */}
				</div>
			</div>
		)
	}

})

module.exports = dateselector
