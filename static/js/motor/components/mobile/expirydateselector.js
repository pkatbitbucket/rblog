var React = require('react')
import Transition from 'react-addons-css-transition-group'
import TransitionTimeout from '../../constants/transition-timeout'
import AppStore from '../../stores/app-store'
import DateStore from '../../stores/date-store'
import moment from 'moment'
import AppActions from '../../actions/app-actions'
import ListItem from './ui/listitem'
import Modal from './ui/modal'
import Button from './ui/button'

import RenderInBody from './ui/renderinbody'

function getStateFromStores(){
	return {
		allDates: DateStore.getAllDates(),
		appData: AppStore.getData()
	}
}

var expirydateselector = React.createClass({

	getInitialState: function() {
		var states = getStateFromStores()
		states.min = moment().subtract(2, 'year').subtract(1, 'day')
		states.max = moment().subtract(1, 'days')
		states.error = false
		states.errorMsg = ''
		if(this.props.hideWrapper)
			states.modal = 'expiryDate'
		return states
	},

	componentWillReceiveProps: function(nextProps){
		if(nextProps.hideWrapper)
			this.setState({modal:'expiryDate'})
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this._onChange)
		DateStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this._onChange)
		DateStore.removeChangeListener(this._onChange)
	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	openExpiryDateSelector: function(){
		this.setState({
			modal:'expiryDate'
		});
	},

	closeExpiryDateSelector: function(update){
		var callback
		if(update){
			callback = this.props.onSelect
		}else{
			callback = this.props.onBack
		}
		this.setState({
			modal: null
		}, callback);
	},

	resetError: function(){
		this.setState({
			error: false,
			errorMsg:""
		})
	},

	handleUpdate: function(){
		var pypeDate = this.refs.pypeDate.value
		pypeDate = moment(pypeDate, "YYYY-MM-DD")

		// Check if policy expiry date is before registration date
		if(pypeDate.isBefore(this.state.allDates.registrationDate)){
			this.setState({
				error:true,
				errorMsg: 'Policy expiry date cannot be before car registration date'
			});
			return
		}
		else if(moment().startOf('day').isAfter(pypeDate)){
			AppActions.setDate('pyp', pypeDate, function(){}, this.props.hideWrapper)
			this.closeExpiryDateSelector(true)
		}
		else{
			this.setState({
				error: true
			})
		}
	},

	render: function() {
		var errorMessageStyle = {
			color: '#f00',
			textAlign: 'center',
			fontSize: '12px'
		}
		var dateRight = 'Choose'
		var updateButtonText = this.props.buttonText || 'Update'

		if(this.state.allDates.pastPolicyExpiryDate){
			dateRight = this.state.allDates.pastPolicyExpiryDate.format('DD-MM-YYYY')
		}

		var updateButton = <Button onClick={this.handleUpdate}>{updateButtonText}</Button>
		var dateinput = <input 	ref='pypeDate'
								onChange={this.resetError}
								min= {this.state.allDates.registrationDate? this.state.allDates.registrationDate.format('YYYY-MM-DD'): moment().add(-16, "years").format("YYYY-MM-DD")}
								max={this.state.max.format('YYYY-MM-DD')}
								type='date'
								defaultValue={
												this.state.allDates.pastPolicyExpiryDate?
												this.state.allDates.pastPolicyExpiryDate.format('YYYY-MM-DD')
												:moment().format('YYYY-MM-DD')
											} />

		var ds = <div>
				<ListItem customClass='thin left-center' left={dateinput} />
				{
					this.state.error ? <p className="fs error" style={errorMessageStyle}>
					{
						this.state.errorMsg ?
						this.state.errorMsg
						: 'Expiry date must be a past date'
					}
					</p>
					: null
				}
				{updateButton ?
					<ListItem customClass='thin left-center' left={updateButton} />	: null
				}
				</div>

		return (
			<div className="expiry-date-selector-wrapper">
				<ListItem left="Previous policy expiry date" right={dateRight} onClick={this.openExpiryDateSelector} />
				<RenderInBody>
					<Transition transitionName='modal' {...TransitionTimeout} >
						{
							this.state.modal == 'expiryDate'?
								<Modal 	onClose={this.closeExpiryDateSelector.bind(this, false)}
										header="Previous policy expiry date"
										content={ds} />
							:null
						}
					</Transition>
				</RenderInBody>
			</div>
		);
	}
});

module.exports = expirydateselector;