var React = require('react')
var moment = require('moment')
import Transition from 'react-addons-css-transition-group'
import Button from './ui/button'
import ListItem from './ui/listitem'
import OldCarSelector from './carselectortemp'
import RenderInBody from './ui/renderinbody'
import Modal from './ui/modal'
import TransitionTimeout from '../../constants/transition-timeout'
import settings from '../../constants/configuration'
import Apiutils from '../../utils/apiutils'
import AppActions from '../../actions/app-actions'
import RegistrationNoInput from './RegistrationNoInput'
import ExpiryDateSelector from './expirydateselector'
import DateStore from '../../stores/date-store'
import AppStore from '../../stores/app-store'
import RTOStore from '../../stores/rto-store'
import VehicleStore from '../../stores/vehicle-store'
import TopBar from './ui/sitebar'
import GTMUtils from '../../utils/gtmutils'

var RegInput = React.createClass({
	getInitialState: function(){
		return {
			fastlaneStatus: false,
			regNo: this.props.value
		}
	},
	nextStep: function(){
		if(this.refs.regNo.isValid()){
			Apiutils.getVehicleInfo(this.state.regNo.join(''), this.onFastlaneSuccess, this.abort.bind(null, 0))
			this.setState({
				fastlaneStatus: true
			})
		}else{
			this.refs.regNo.isValid(true)
		}
	},
	onFastlaneSuccess: function(){
		GTMUtils.fsStepOne()
		this.props.onContinue()
	},
	onChangeRegNo: function(value){
		AppActions.clearVehicleInfo(true)
		this.state.regNo = this.refs.regNo.getValue()
	},
	abort: function(status){
		AppActions.setRegistrationNumber([]);
		GTMUtils.fastlaneStepedOut(status)
		AppActions.clearVehicleInfo((status===2), function(){
			window.location.hash = '/quoteform'
		})
	},
	onValid: function(regNoArray){
		this.state.regNo = regNoArray
		this.props.onUpdate(regNoArray)
	},
	componentWillReceiveProps: function(nextProps){
		this.setState({
			fastlaneStatus : false
		})
	},
	render: function() {
		var buttonType = this.state.fastlaneStatus ? 'loading' : ''
		return (
			<div className={"fl-reg-input " + (this.props.highlight?  '':'low')}>
				<br/>
				<h2>Compare and buy car insurance online</h2>
				<h5>Enter your Car’s Registration Number to get started.</h5>
				<RegistrationNoInput ref="regNo" disabled={this.state.fastlaneStatus} onValid={this.onValid} onChange={this.onChangeRegNo} defaultValue={this.state.regNo.slice()} />
				<ListItem customClass='fl-list' left={<div><Button type={buttonType} disabled={this.state.fastlaneStatus} customClass="large fl-main" onClick={this.nextStep} >Continue</Button>
				<Button customClass="empty fl-grey" onClick={this.abort.bind(null, 1)} >I don’t know my registration number</Button>
				<Button customClass="empty fl-grey" onClick={this.abort.bind(null, 2)} style={{ marginTop : '0px!important' }}>I have a new car</Button></div>} />
			</div>
		);
	}

}); 

var ExpiredSelector = React.createClass({
	getInitialState: function(){
		return {
			showDateSelector: false
		}
	},
	reset: function(){
		this.setState({
			showDateSelector: false
		})
	},
	onExpiredSelect: function(){
		GTMUtils.fsStepTwo(true)
		AppActions.setExpiry('expired', this.setState.bind(this, {
			showDateSelector: true
		}))
	},
	onNotExpiredSelect: function(){
		GTMUtils.fsStepTwo(false)
		AppActions.setExpiry('renew')
		this.props.onContinue()
	},
	onSelect: function(val){
		GTMUtils.fsStepTwoOne()
		this.props.onContinue(val)
	},
	render: function() {
		var item
		if(this.state.showDateSelector){
			item = 	(<div>
						<ExpiryDateSelector hideWrapper={true} onSelect={this.onSelect} onBack={this.reset} buttonText="Continue"/>
					</div>)
		}else{
			item = (<div><h2>Has your car policy already expired?</h2>
					<div className="action-buttons">
						<ul>
							<li onClick={this.onExpiredSelect}>Yes</li>
							<li onClick={this.onNotExpiredSelect}>No</li>
						</ul>
					</div></div>)
		}

		return (
			<div className={"fl-expired-input fl-modal " + (this.props.highlight?  '':'low')}>
				{this.props.highlight ? item : null}
			</div>
		);
	} 

});

var CarSelector = React.createClass({
	getInitialState: function(){
		return {
			error: false
		}
	},
	nextStep: function(){
		if(this.props.emptyFields && this.props.emptyFields.length>0){
			this.setState({error:true})
		}else{
			AppActions.sendfastlaneFeedback()
			this.props.onContinue()
		}
	},
	componentWillReceiveProps: function(nextProps){
		this.setState({error:(this.state.error && nextProps.emptyFields.length>0)})
	},
	render: function() {
		var errorMessageStyle = {
			color: '#f00',
			textAlign: 'center',
			fontSize: '12px'
		}
		return (
			<div className={'fl-car-selector ' + (this.props.highlight?  '':'low')}>
				<h2>
					We fetched the car details based on your car registration number.
				</h2>

				<div className="fl-modal">
					<OldCarSelector />
				</div>
				{this.state.error ? <p className="fs error" style={errorMessageStyle}>{"Please enter " + this.props.emptyFields.join(", ") + " to continue"}</p> : null}
				<ListItem customClass='fl-list' left={<div><Button customClass="large fl-main" onClick={this.nextStep} >{this.props.goToResults ? 'View Quotes' : 'Confirm'}</Button></div>} />
			</div>
		);
	}
});

var NCBSelector = React.createClass({

	getInitialState: function() {
		return {
			modal: null,
			error: false
		};
	},

	componentWillReceiveProps: function(nextProps){
		this.setState({error:false})
	},

	openNCBModal: function(){
		this.setState({
			modal:'ncb'
		});
	},

	closeNCBModal: function(){
		this.setState({
			modal:null
		});
	},

	setClaimStatus: function(value){
		AppActions.setClaimStatus(value)
		if(value)
			this.props.onContinue()
		else
			this.openNCBModal()
	},

	next: function(allowPass){
		if(allowPass)
			AppActions.viewQuotes('unclaimed')
		else
			this.setState({error:true})
	},

	selectNCB: function(value){
		AppActions.selectNCB(value)
	},

	render: function() { 
		var errorMessageStyle = {
			color: '#f00',
			textAlign: 'center',
			fontSize: '12px'
		}
		var selectedValue = this.props.selectedIndex
		var NCBUnknown = this.props.NCBUnknown
		var ncbOptions = settings.getConfiguration().ncb_options
		var allowPass = NCBUnknown || (!NCBUnknown && selectedValue !== undefined && selectedValue !== null)
		var allNcb = ncbOptions.map(function(item, index){
			return <Button customClass={"empty "+ (!NCBUnknown && item.value === selectedValue ? 'active' : '')} key={index} onClick={this.selectNCB.bind(null, item)} >{item.key}</Button>
		}, this)

		var ncbselector = (<div>
			<div className="button-group ol"> 
				{allNcb}
				<Button customClass={"empty big " + (NCBUnknown?'active':'')} onClick={this.selectNCB.bind(null, null)}>I don’t know</Button>
				{this.state.error ? <p className="fs error" style={errorMessageStyle}>Please select Previous NCB Value from above</p> : null}
				<Button customClass="final big" onClick={this.next.bind(null, allowPass)}>View Quotes</Button>
			</div>

		</div>)

		var ncbmodal = <Modal customClass='no-header' onClose={this.closeNCBModal} >
			<div className="ncb-modal">
				<h3>
					Since you haven’t claimed in the past year, <br/>you are eligible for a <br />No Claim Bonus Discount
				</h3>

				<h2>
					What is the NCB <br />mentioned in your <br />previous policy?
				</h2>
				{ncbselector}
			</div>
		</Modal>

		return (
			<div className={'fl-ncb-selector '+ (this.props.highlight?  '':'low')}>
				<h2>
					One last question! <br />This one is simple.
				</h2>

				<div className="fl-ncb-input fl-modal">
					<h2>Have you made a claim in the previous year?</h2>
					<div className="action-buttons">
						<ul>
							<li onClick={this.setClaimStatus.bind(this, true)}>Yes</li>
							<li onClick={this.setClaimStatus.bind(this, false)}>No</li>
						</ul>
					</div>
				</div>

				<RenderInBody>
					<Transition transitionName='modal' {...TransitionTimeout} >
						{this.state.modal == 'ncb'? ncbmodal:null }
					</Transition>
				</RenderInBody>

			</div>
		);
	}

});

function getUpdatedStates(){
	var quoteRequest
	try{
		quoteRequest = AppStore.getQuoteRequest()
	}catch(e){
		quoteRequest = {}
	}
	return {
		reg: RTOStore.getRegistrationNumber(),
		quoteType: AppStore.getData().quoteType,
		expirydate: DateStore.getAllDates().pastPolicyExpiryDate,
		NCBUnknown: quoteRequest.ncb_unknown,
		previousNCB: quoteRequest.previousNCB,
		selectedVehicle: VehicleStore.getSelectedVehicle(),
		selectedVariant: VehicleStore.getSelectedVariant(),
		selectedFuelType: VehicleStore.getSelectedFuelType(),
		policyExpired : DateStore.isPolicyExpired()
	}
}

var fastlane_form = React.createClass({
	getInitialState: function() {
		var states = getUpdatedStates()
		states.currentStep='reg'
		states.visitedSteps=[]
		GTMUtils.fastlane_home()
		return states
	},
	_onChange: function(){
		this.setState(getUpdatedStates())
	},
	componentDidMount: function(){
		AppStore.addChangeListener(this._onChange)
		DateStore.addChangeListener(this._onChange)
		RTOStore.addChangeListener(this._onChange)
		VehicleStore.addChangeListener(this._onChange)
	},
	componentWillUnmount: function(){
		AppStore.removeChangeListener(this._onChange)
		DateStore.removeChangeListener(this._onChange)
		RTOStore.removeChangeListener(this._onChange)
		VehicleStore.removeChangeListener(this._onChange)
	},
	goToStep: function(step){
		var x = this.state.visitedSteps
		x.push(this.state.currentStep)
		this.setState({
			visitedSteps: x,
			currentStep:step
		});
		if(step==='car'){
			GTMUtils.fsOnEnterStepThree(this.state.policyExpired)
		}
	},
	goToResults: function(status){
		AppActions.viewQuotes(status)
	},
	regUpdate: function(value){
		AppActions.setRegistrationNumber(value)
	},
	resetSummary: function(item){
		var x = this.state.visitedSteps
		x.splice(x.indexOf(item), x.length)

		this.setState({
			visitedSteps:x,
			currentStep: item
		});
		
		if(item === 'reg' || item === 'expired'){
			GTMUtils.fsEditClick(item == 'reg' ? 0 : 1)
		}
	},
	getEmptyFieldsList: function(){
		var list = []
		if(!this.state.selectedVehicle){
			list.push("Your Car")
		}
		if(!this.state.selectedVariant){
			list.push("Fuel Type")
		}
		if(!this.state.selectedFuelType){
			list.push("Car Variant")
		}
		return list
	},
	render: function() {
		var editIcon = <div className='edit-icon'>Edit</div>
		
		var regleft
		if(this.state.visitedSteps.indexOf('reg') > -1){
			regleft = (<div>
					<span className="left-title">Registration Number</span>
					<span className="left-sub upper">{this.state.reg.join('-')}</span>
				</div>)
		}

		var expiredleft
		if(this.state.visitedSteps.indexOf('expired') > -1){
			expiredleft = (<div>
				<span className="left-title">Policy Expiry Date</span>
				<span className="left-sub">{this.state.quoteType==='expired' ? this.state.expirydate.format('DD-MM-YYYY') : "Policy Not Expired"}</span>
			</div>)
		}

		var carLeft
		if(this.state.visitedSteps.indexOf('car') > -1 && this.state.selectedVehicle && this.state.selectedVariant){
			carLeft = (<div>
				<span className="left-title">Your car</span>
				<span className="left-sub">{this.state.selectedVehicle.name+' '+this.state.selectedVariant.name}</span>
				</div>)
		}

		var isNotEligibleForNCB = this.state.quoteType === "expired" && this.state.expirydate && moment().diff(this.state.expirydate, 'days')>=90

		return (
			<div className='page'>
				<TopBar />
				<div className="summary">
					{this.state.visitedSteps.indexOf('reg') > -1? <ListItem customClass="thin card full lol" left={regleft} right={editIcon} onClick={this.resetSummary.bind(this, 'reg')} />:null} 
					{this.state.visitedSteps.indexOf('expired') > -1? <ListItem customClass="thin card full lol" left={expiredleft} right={editIcon} onClick={this.resetSummary.bind(this, 'expired')} />:null}
					{this.state.visitedSteps.indexOf('car') > -1? <ListItem customClass="thin card full lol" left={carLeft} right={editIcon} onClick={this.resetSummary.bind(this, 'car')} />:null}
				</div>
				<RegInput highlight={this.state.currentStep == 'reg'} value={this.state.reg} onUpdate={this.regUpdate} onContinue={this.goToStep.bind(this, 'expired')} />
				<ExpiredSelector highlight={this.state.currentStep == 'expired'} onContinue={this.goToStep.bind(this,'car')} />
				<CarSelector highlight={this.state.currentStep == 'car'} emptyFields={this.getEmptyFieldsList()} onContinue={isNotEligibleForNCB ? this.goToResults.bind(null, 'expired') : this.goToStep.bind(this,'ncb')} goToResults={isNotEligibleForNCB} />
				{isNotEligibleForNCB ? null : <NCBSelector NCBUnknown={this.state.NCBUnknown} selectedIndex={this.state.previousNCB} highlight={this.state.currentStep == 'ncb'} onContinue={this.goToResults.bind(null, 'claimed')} />}
			</div>
		);
	}
});

module.exports = fastlane_form;
