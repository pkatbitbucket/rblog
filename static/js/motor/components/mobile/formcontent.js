var React = require('react')
import AppStore from '../../stores/app-store'
import VehicleStore from '../../stores/vehicle-store'
import LMSActions from '../../actions/lms-actions'
import RTOStore from '../../stores/rto-store' 
import DateStore from '../../stores/date-store'
import QuoteTypeSelector from './quotetypeselector'
import CarSelector from './carselector'
import RTOSelector from './rtoselector'
import NCBSelector from './ncbselector'
import ExpiryDateSelector from './expirydateselector'
import RegistrationYearSelector from './regyearselector'
import PhoneNumberInput from './phonenumberinput'
import CNGValueInput from './cngvalueinput'
import ListItem from './ui/listitem'
import Button from './ui/button'
import Cookie from 'react-cookie'
var GTMEventPush = require('../../utils/gtmutils').gtm_event_push


var networksForMobileNumber = ['search', 'gsp', 'y!search']

function getStateFromStores(){
	return {
		isNewVehicle: AppStore.getData().isNewVehicle,
		selectedVariant: VehicleStore.getSelectedVariant(),
		selectedFuelType: VehicleStore.getSelectedFuelType(),
		selectedRTO: RTOStore.getSelectedRTO(),
		selectedRegYear: DateStore.getSelectedRegYear(),
		quoteType: AppStore.getData().quoteType,
		selectedPreviousNCB: AppStore.getData().previousNCB
	}
}


// temp shit

var smoothScroll = function(target, offset) {
    var scrollContainer = document.getElementById('content');
    var targetY = 0
    if(offset){
        targetY -= offset;
    }
    do {
        if (target == scrollContainer) break;
        targetY += target.offsetTop;
    } while (target = target.offsetParent);

    var scroll = function(c, a, b, i) {
        i++; if (i > 30) return;
        c.scrollTop = a + (b - a) / 30 * i;
        setTimeout(function(){ scroll(c, a, b, i); }, 10);
    }
    scroll(scrollContainer, scrollContainer.scrollTop, targetY, 0);
}

var formcontent = React.createClass({
	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this._onChange)
		VehicleStore.addChangeListener(this._onChange)
		RTOStore.addChangeListener(this._onChange)
		DateStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this._onChange)
		VehicleStore.removeChangeListener(this._onChange)
		RTOStore.removeChangeListener(this._onChange)
		DateStore.removeChangeListener(this._onChange)
	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	showQuotes: function(){
		this.setState({
			checkForErrors: true
		}, function(){
			var errorElements = document.getElementsByClassName('error');
    		if(errorElements.length){
        		smoothScroll(errorElements[0], 150);
    		}
		});




		// this is shit. Please refactor. TODO

		var self = this;

		function submit(cngKitValue){
			if((cngKitValue && cngKitValue.state.cngValueValid) || !cngKitValue)
				{
					if(self.state.isNewVehicle == 0){
						if(self.state.selectedVariant && self.state.selectedRTO && self.state.selectedRegYear && self.state.selectedPreviousNCB){
							self.props.onSubmit()
						}
					}
					else{
						if(self.state.selectedVariant && self.state.selectedRTO){
							self.props.onSubmit()
						}
					}
				}
		}

		if((this.refs.mobileNo && this.refs.mobileNo.state.phoneNumberValid) || !this.refs.mobileNo){
			submit(this.refs.cngKitValue)
			if(this.refs.mobileNo){
				LMSActions.sendToLMS(AppStore.getQuoteRequest(), 'quote-form', {mobile:Cookie.load('mobileNo')})
				GTMEventPush('GAEvent', 'MOTOR', 'CallScheduled', 'quote-form')
			}
		}

		// shit ends

		
	},

	render: function() {

		var network = Cookie.load('network')
		network = network.toLowerCase()

		var askForPhoneNumber

		if(networksForMobileNumber.indexOf(network) > -1){
			askForPhoneNumber = true
		}

		return (
			<div>
				<QuoteTypeSelector />
				<CarSelector />
				{(this.state.checkForErrors && !this.state.selectedVariant)? <ListItem customClass='xthin error' left={'Please select your car'} />:null}
				<RTOSelector />
				{(this.state.checkForErrors && !this.state.selectedRTO)? <ListItem customClass='xthin error' left={'Please select the RTO'} />:null}

				{(this.state.selectedFuelType == 'EXTERNAL_LPG_CNG')? <CNGValueInput ref='cngKitValue' showError={this.state.checkForErrors} />:null}

				{this.state.quoteType =='expired'? <ExpiryDateSelector />:null}
				{this.state.isNewVehicle == 0? <div>
					<RegistrationYearSelector />
					{(this.state.checkForErrors && !this.state.selectedRegYear)? <ListItem customClass='xthin error' left={'Please select the registration year'} />:null}
					<NCBSelector />
					{(this.state.checkForErrors && !this.state.selectedPreviousNCB)? <ListItem customClass='xthin error' left={'Please select Previous No Claim Bonus (NCB)'} />:null}
				</div>:null}

				{
					askForPhoneNumber? <PhoneNumberInput ref='mobileNo' showError={this.state.checkForErrors} />:null
				}
				
				<ListItem customClass="thin" left={<Button customClass="large" onClick={this.showQuotes} >View Quotes</Button>} />
			</div>
		);
	}

});

module.exports = formcontent;