var React = require('react')
import AppStore from '../../stores/app-store'
import PlanStore from '../../stores/plan-store'
import Tabs from './ui/tabs'
import Tab from './ui/tab'
import ListItem from './ui/listitem'
import Button from './ui/button'
import Spinner from './ui/spinner'
import Apiutils from '../../utils/apiutils'
import AppActions from '../../actions/app-actions'
import GTMActions from '../../actions/gtm-actions'


function getStateFromStores(){
	return {
		idv: AppStore.getData().idv,
		maxIDV: PlanStore.getMaxIDV(),
		minIDV: PlanStore.getMinIDV()
	}
}

var idvselector = React.createClass({
	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this._onChange)
		PlanStore.addChangeListener(this._onChange)
		this.setState({
			localIDV:this.state.idv
		})
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this._onChange)
		PlanStore.removeChangeListener(this._onChange)
	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	setIDV: function(idv){
		this.setState({
			localIDV: idv
		})
	},

	handleIDVInput: function(evt){
		if(evt.target.value != ''){
			this.setIDV(evt.target.value)	
		}
	},

	setCustomIDV: function(){
		if(this.state.idv == 0){
			this.setState({
				localIDV: this.state.minIDV
			})
		}else{
			this.setState({
				localIDV: this.state.idv
			})
		}
	},

	handleIDVUpdate: function(){
		var newIDV = this.state.localIDV

		if(newIDV == ''){
			newIDV = 0
		}
		if((!isNaN(newIDV) && newIDV <= this.state.maxIDV && newIDV >= this.state.minIDV) || newIDV == 0){
			this.setState({
				idvError:false,
				updating:true
			})

			AppActions.setIDV(newIDV, function(){
				GTMActions.updateIDV()
			})
			var self = this
			setTimeout(function(){
				self.props.onUpdate()
				AppActions.startFetchingPlans()
				Apiutils.getPlans(AppStore.getQuoteRequest())
			}, 300)
		}
		else{
			this.setState({
				idvError:true
			})
		}

	},

	render: function(){

		var IDVSelectorContent = <div>
			<p className='sub'>Please enter an IDV between <br /> {this.state.minIDV} and {this.state.maxIDV}.</p>
			<p><input type='text' ref='idvValue' onChange={this.handleIDVInput} defaultValue={this.state.idv > 0? this.state.idv:this.state.minIDV} /></p>
			{this.state.idvError? <p className='error'>Please enter a valid IDV with the limits mentioned above</p>:null}
		</div>

		var pageContent
		if(this.state.localIDV == 0 && this.state.idv == 0){
			pageContent = <ListItem customClass='narrow' left={<p className='sub'>IDV is automatically set to get you the best quotes from each insurer.</p>} />
		}

		if(this.state.localIDV == 0 && this.state.idv != 0){
			pageContent = <ListItem customClass='narrow' left={<p className='sub'>This will set the IDV automatically to get you the best quotes from each insurer.</p>} />
		}

		if(this.state.localIDV != 0){
			pageContent = <ListItem customClass='narrow' left={IDVSelectorContent} />
		}	


		var aboutIDVcontent = (<div>
			<h4>What is IDV?</h4>
			<p>IDV or Insured declared value is the calculated value of your car which would be reimbursed to you in case of theft or total damage.</p>
			<h4>Why is the IDV different for each insurer?</h4>
			<p>{'We have set your cars IDV for the best deal from each insurer, hence the difference.'}</p>
			<h4>How does this impact my policy or claims?</h4>
			<p>The IDV will not impact the limit or amount of claim in case of accidental repairs.</p>
			<h4>{'Should I set the IDV to a higher or a lower value?'}</h4>
			<p>IDV or Insured declared value is the calculated value of your car which would be reimbursed to you in case of theft or total damage. This value has nothing to do with the limit of accidental repairs or resale value of the car. Higher IDV would result in a higher premium amount. We recommend going with the default IDV suggested unless you live in a theft prone area or drive excessively on highways.</p>
		</div>)

		var updateButton = <Button onClick={this.handleIDVUpdate}>Update Quotes</Button>

		if(this.state.localIDV == 0 && this.state.idv == 0){
			updateButton = null
		}

		if(this.state.updating){
			updateButton = <Button><Spinner customClass='white' /></Button>
		}

		return (
			<div className="idv-selector-wrapper">
				<div>
					<Tabs>
						<Tab active={this.state.localIDV == 0} onClick={this.setIDV.bind(this, 0)}>For Best Deal</Tab>
						<Tab active={this.state.localIDV != 0} onClick={this.setCustomIDV}>Choose your own IDV</Tab>
					</Tabs>
				</div>

				{pageContent}
				{updateButton? 
					<ListItem customClass='thin left-center' left={updateButton} />	: null
				}

				<div className="about-idv-content">
					<ListItem customClass="card" left={aboutIDVcontent} />
				</div>
			</div>
		)
	}

})

module.exports = idvselector