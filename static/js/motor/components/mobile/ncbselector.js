import React from 'react'
import Transition from 'react-addons-css-transition-group'
import ListItem from './ui/listitem'
import Switch from './ui/toggleswitch'
import settings from '../../constants/configuration'
import ModalSelector from './ui/modalselector'
import Button from './ui/button'
import AppActions from '../../actions/app-actions'
import AppStore from '../../stores/app-store'

function getStateFromStores(){
	var data = AppStore.getData()
	return {
		data: data,
		newNCB: data.newNCB,
		previousNCB: data.previousNCB || 0,
		hasClaimedPastYear: data.hasClaimedPastYear,
		isNCBCertificate: data.isNCBCertificate || 0
	}
}

var ncbselector = React.createClass({
	getDefaultProps: function(){
		return {
			forNewNCB: false
		}
	},
	getInitialState: function() {
		var states = getStateFromStores();
		states.oldNCB = this.props.forNewNCB ? states.data.newNCB : (states.data.previousNCB ? AppStore.getData().previousNCB.value : 0);
		states.oldClaimedValue = this.props.forNewNCB ? false : (states.data.hasClaimedPastYear || false);
		states.sLabel = this.props.forNewNCB ? "Do you have NCB Certificate?" : "Have you made an insurance claim for this car in the past year?";
		states.oLabel = this.props.forNewNCB ? "New No Claim Bonus (NCB)" : "Previous No Claim Bonus (NCB)";
		return states
	},

	componentWillMount:function(){
		if(this.props.forNewNCB){
			AppActions.setClaimStatus(false, true)
		}else if(this.state.previousNCB == undefined){
			AppActions.selectNCB()
		}
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this._onChange)
	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	handleClaimStatusChange: function(value){
		if(this.props.forNewNCB){
			value ? null : AppActions.selectNCB(null, undefined, true)
		}
		AppActions.setClaimStatus(value, this.props.forNewNCB ? true : false)
	},

	onNCBSelect: function(value){
		AppActions.selectNCB(value, undefined, this.props.forNewNCB)
	},

	getOriginalNCB: function(){
		return this.state.oldNCB;
	},

	setOriginalNCB: function(){
		this.state.oldNCB = this.props.forNewNCB ? this.state.data.newNCB : (this.state.data.previousNCB ? AppStore.getData().previousNCB.value : 0)
	},

	getClaimedValue: function(){
		return this.state.oldClaimedValue;
	},

	setClaimedValue: function(){
		this.state.oldClaimedValue = this.props.forNewNCB ? this.state.data.isNCBCertificate : (this.state.data.hasClaimedPastYear || false);
	},

	render: function() {
		var ncbOptions = settings.getConfiguration().ncb_options
		var claimRight = <Switch onClick={this.handleClaimStatusChange} on={this.props.forNewNCB ? this.state.isNCBCertificate : this.state.hasClaimedPastYear} />

		var moreIcon = <img className='more-icon' src={STATIC_URL + 'img/motor/mobile/more.png'} />

		var right = moreIcon

		var allNcb = ncbOptions.map(function(item, index){
			var buttonType = "empty"
			if((!this.props.forNewNCB && this.state.previousNCB && this.state.previousNCB.value == item.value)
					|| (this.props.forNewNCB && this.state.newNCB && this.state.newNCB.value == item.value)){
				buttonType = "empty active"
			}
			return this.props.forNewNCB && item.value === 0 ? null : <Button customClass={buttonType} key={index} onClick={this.onNCBSelect.bind(this, item)} >{item.key}</Button>
		}, this)

		var showNCBSelector = (!this.props.forNewNCB && !this.state.hasClaimedPastYear) || (this.props.forNewNCB && this.state.isNCBCertificate)

		var ncbselector = showNCBSelector ? <div>
			<span>{this.state.oLabel}</span>
			<div className="button-group">
				{allNcb}
			</div>
		</div> : null

		return (
			<div className="ncb-selector-wrapper">
				<ListItem left={this.state.sLabel} right={claimRight} />
				{ ncbselector ? <ListItem left={ncbselector} /> : null}
			</div>
		);
	}

});

module.exports = ncbselector;