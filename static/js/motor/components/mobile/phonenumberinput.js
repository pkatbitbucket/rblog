var React = require('react');
import ListItem from './ui/listitem'
import Cookie from 'react-cookie'

var phonenumberinput = React.createClass({

	getInitialState: function() {
		return {
			phoneNumberValid: false
		};
	},

	componentDidMount: function() {
		this.handleChange()
	},

	handleChange: function(){
		var value = this.refs.mobileNo.value
		var reg = /[7-9]{1}\d{9}/

		if(reg.test(value)){
			this.setState({
				phoneNumberValid: true
			});
			Cookie.save('mobileNo', value, {path: '/'})
		}
		else{
			this.setState({
				phoneNumberValid: false
			});
		}

	},

	render: function() {

		var mobileNo = Cookie.load('mobileNo')

		var left = <div>
				<span className="left-sub"><input ref='mobileNo' defaultValue={mobileNo} className='mobileNo' placeholder='Enter 10 digit phone number' type='tel' onChange={this.handleChange} /></span>
				<span className="left-subtitle">For SMS quotes and important communication</span>
				</div>

		return (
			<div>
				<ListItem left={left} />
				{this.props.showError && !this.state.phoneNumberValid? <ListItem customClass='xthin error' left={'Please enter a valid 10 digit phone number'} />:null}
			</div>
		);
	}

});

module.exports = phonenumberinput;