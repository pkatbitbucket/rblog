import React from 'react';
import ListItem from './ui/listitem'
import utils from '../../utils/apputils'
import Button from './ui/button'
import Spinner from './ui/spinner'


var plancard = React.createClass({
	getInitialState: function() {
		return {}
	},

	render: function() {
		var plan = this.props.plan
		var mode = this.props.mode
		var loading = this.props.loading
		var addons = this.props.selectedAddons
		var addonNames, selectedAddonsInPlan, selectedAddonsInPlanIds, selectedAddonsNotInPlan

		if(addons && addons.length > 0){

			addonNames = addons.map(function(item){
				return item.name
			})

			selectedAddonsInPlan = plan.premiumBreakup.addonCovers.filter(function(item){
				return item.is_selected
			})

			selectedAddonsInPlanIds = selectedAddonsInPlan.map(function(item){
				return item.id
			})

			selectedAddonsNotInPlan = addons.filter(function(item){
				return selectedAddonsInPlanIds.indexOf(item.id.replace('addon_','')) == -1
			})

			selectedAddonsInPlan = selectedAddonsInPlan.map(function(item, index){
				return <ListItem key={index} customClass={'thin addon-item'} left={item.name} right={'+ Rs.'+utils.numberToINR(parseInt(item.premium + item.premium*plan.serviceTaxRate))} />
			})

			selectedAddonsNotInPlan = selectedAddonsNotInPlan.map(function(item, index){
				return <ListItem key={index} customClass={'thin addon-item not-available'} left={item.name} right='Not Avaliable' />
			})

		}


		var onTap = this.props.onTap

		var planLogo = <div>
			<img className='insurer-logo' src={STATIC_URL+'/img/motor/insurers/'+plan.insurerSlug+'.png'} />
		</div>

		var planLeft = (<div onClick={onTap}>
			<span className="block-title">{planLogo}</span>
			<span className="block-subtitle dim">IDV: {'Rs. '+utils.numberToINR(plan.calculatedAtIDV)}</span>
		</div>)

		//var planMiddle = <div>{'Rs. ' + utils.numberToINR(plan.totalPremium)}</div>

		var planRight;
		if(loading){
			planRight = <Button customClass="empty no-border"><Spinner customClass='orange narrow' /></Button>
		}else{
			planRight = <div><Button customClass='inline-label' onClick={this.props.onPriceClick}>
				<span className='button-label'>Buy for</span>
				{'Rs. ' + utils.numberToINR(plan.totalPremium)}
			</Button>{plan.insurerSlug==='oriental'?<span className="buy-caption">Cashless claims not available</span>:null}</div>
		}


		return (
			<div className='plancard-wrapper'>
				<ListItem customClass={'plan-item card '+ ((addons && addons.length>0)? 'has-addons':'')} left={planLeft} right={planRight} />
				{selectedAddonsInPlan}
				{selectedAddonsNotInPlan}
			</div>
		);
	}

});



module.exports = plancard
