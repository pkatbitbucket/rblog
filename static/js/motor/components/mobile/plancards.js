import React from 'react'
import Transition from 'react-addons-css-transition-group'
import TransitionTimeout from '../../constants/transition-timeout'
import PlanStore from '../../stores/plan-store'
import AppStore from '../../stores/app-store'
import VehicleStore from '../../stores/vehicle-store'
import DateStore from '../../stores/date-store'
import ACStore from '../../stores/additional-cover-store'
import ListItem from './ui/listitem'
import Button from './ui/button'
import Spinner from './ui/spinner'
import Modal from './ui/modal'
import PlanCard from './plancard'
import AppActions from '../../actions/app-actions'
import GTMActions from '../../actions/gtm-actions'
import PremiumBreakup from './premium-breakup'
import RenderInBody from './ui/renderinbody'
import Apiutils from '../../utils/apiutils'

import DateSelector from './dateselector'
import HalfModal from './ui/halfmodal'

var currentPlan = null

function getStateFromStores(){
	return {
		plans: PlanStore.getPlans(),
		selectedPlan: PlanStore.getSelectedPlan(),
		selectedAddons: ACStore.getSelectedAddons(),
		fetching: PlanStore.getFetchingPlansStatus(),
		sortedOnce: PlanStore.getInitialSortStatus(),
		rogueInsurers: PlanStore.getRogueInsurers(),
		fastlaneStatus: VehicleStore.getFastlaneStatus(),
		selectedVehicle: VehicleStore.getSelectedVehicle(),
		quotesUpdatedParent: DateStore.getUpdateStatus(),
		hideDateSelector: AppStore.hideDateSelector()
	}
}


var plancards = React.createClass({
	getInitialState: function() {
		var states = getStateFromStores()
		states.showOptimizationPopup = false
		states.quotesUpdated = false
		return states
	},

	componentDidMount: function() {
		PlanStore.addChangeListener(this._onChange)
		ACStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		PlanStore.removeChangeListener(this._onChange)
		ACStore.removeChangeListener(this._onChange)

	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	handleTap: function(plan, index){
		AppActions.selectPlan(plan, function(){
			GTMActions.seePremiumDetails(plan, index)
		})
		this.setState({selectedPlanIndex: index});
	},

	handleBreakupClose: function(){
		AppActions.deselectPlan()
		GTMActions.backToResultsFromBreakup()
	},

	closeDateSelector: function(callback){
		this.setState({showOptimizationPopup:false}, callback)
	},

	onUpdateQuotes: function(changed){
		this.closeDateSelector()
		GTMActions.updateQuote()
		if(changed){
			currentPlan = null
			AppActions.startFetchingPlans()
			Apiutils.getPlans(AppStore.getQuoteRequest())
			this.setState({quotesUpdated:true})
		}else if(currentPlan){
			this.buyPlan(currentPlan)
		}else{
			console.log("Something went wrong!")
		}
	},

	handleBuyClick: function(plan){
		if(this.state.fastlaneStatus && !this.state.quotesUpdated && !this.state.showOptimizationPopup && !this.state.hideDateSelector){
			this.setState({showOptimizationPopup:true})
			currentPlan = plan
		}else{
			this.closeDateSelector(this.buyPlan.bind(this, plan))
		}
	},

	buyPlan: function(plan){
		var self = this
		this.setState({
			loading:true
		});

		if(plan){
			this.setState({
				selectedPlanForBuy:plan
			})
			GTMActions.buyPlan(plan, plan.index)
			Apiutils.gotoTransaction(AppStore.getQuoteRequest(), plan, function(){
				setTimeout(function(){
					self.setState({
						loading:false
					})
				}, 300)
			})
		}
		else{
			GTMActions.buyPlan(this.state.selectedPlan, this.state.selectedPlanIndex)
			Apiutils.gotoTransaction(AppStore.getQuoteRequest(), this.state.selectedPlan, function(){
				setTimeout(function(){
					self.setState({
						loading:false
					})
				}, 300)
			})
		}


	},

	render: function() {

		var plans = this.state.plans

		var planCardElements
		if(plans.length > 0){
			planCardElements = plans.map(function(plan, index){
				var isLoading = (!this.state.sortedOnce || (this.state.loading && (this.state.selectedPlanForBuy && this.state.selectedPlanForBuy.insurerSlug == plan.insurerSlug)))
				return <PlanCard loading={isLoading} selectedAddons={this.state.selectedAddons} plan={plan} onPriceClick={this.handleBuyClick.bind(this, plan)} key={index} onTap={this.handleTap.bind(this, plan, index)} />
			}, this)
		}


		var loadingMessage = <div className='loading-message'><span>
				{this.state.quotesUpdatedParent || this.state.quotesUpdated ? "We are fetching updated quotes based on the changes you just made" : "We are fetching the best quotes from all top car insurers for your " + this.state.selectedVehicle.name }
			</span><Spinner /></div>
		var rogueMessage = <div className='rogue-message'><span>We did not get a quote from the following insurers. <strong>{this.state.rogueInsurers.join(', ')}</strong>. An insurer might not return a quote because
									<ol><li>Your location or vehicle model is not serviced by the insurer.</li>
									<li>The insurer does not provide insurance after a certain age of the vehicle.</li>
									<li>The insurer is not reachable to provide live quotes currently.</li></ol> </span></div>

		return (
			<div className='plan-cards-wrapper'>
				{planCardElements}
				{this.state.fetching? <ListItem customClass='loading-card' left={loadingMessage} />:null}
				{!this.state.fetching? <ListItem left={rogueMessage} />:null}
				<RenderInBody>
					<Transition transitionName='modal' {...TransitionTimeout}>
						{this.state.selectedPlan? <Modal customClass='premium-breakup-modal' onClose={this.handleBreakupClose} header='Premium Breakup' content={<PremiumBreakup plan={this.state.selectedPlan} />} hangingContent={<PlanCard mode='buy' loading={this.state.loading} onPriceClick={this.buyPlan.bind(null, this.state.selectedPlan)} plan={this.state.selectedPlan} />}></Modal>:null }
					</Transition>
				</RenderInBody>
				<Transition transitionName='fade' {...TransitionTimeout} >
					{ this.state.showOptimizationPopup ? <HalfModal onClose={this.closeDateSelector} content={<DateSelector onUpdate={this.onUpdateQuotes} close={this.closeDateSelector} />} /> : null}
				</Transition>
			</div>
		)
	}

});


module.exports = plancards
