var React = require('react');
var ReactDOM = require('react-dom');
import RTOStore from '../../stores/rto-store'
import ListItem from './ui/listitem'

function closest(el, fn) {
    return el && (
        fn(el) ? el : closest(el.parentNode, fn)
    );
}

function getStateFromStores(){
	return {
		popularRTOs: RTOStore.getPopularRTOs()
	}
}

var popularrtosgrid = React.createClass({

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		RTOStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		RTOStore.removeChangeListener(this._onChange)	
	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	handleCityClick: function(city){
		var self = this;
		this.setState({
			activeCity:city.name
		}, function(){
			var topPos = ReactDOM.findDOMNode(self.refs[city.name]).offsetTop;
			var parent = closest(ReactDOM.findDOMNode(self.refs[city.name]), function (el) {
			    return el.className.indexOf('modal-content') > -1;
			});
  			parent.scrollTop = topPos-10;
		});
	},

	handleRTOClick: function(rto){
		this.props.onSelect(rto.split(' - ')[0])
	},

	render: function() {
		var popularRTOs = this.state.popularRTOs
		var popularCities = []

		for (var key in popularRTOs) {
			var items = popularRTOs[key]
			var uniqueItems = []
			for (var i = 0; i < items.length; i++) {
				var item = items[i].split('-')[0]
				if(uniqueItems.indexOf(item) == -1){
					uniqueItems.push(item)
				}
			};
			popularCities.push({
				name: key,
				items: uniqueItems,
				rtos: popularRTOs[key].sort().map(function(rto, index){
					return <ListItem key={index} left={rto} customClass="thin" onClick={this.handleRTOClick.bind(null, rto)} />
				}, this)
			})

		};



		popularCities = popularCities.map(function(city, index){
			var titleSuper = city.items.join(',')
			var title = city.name

			var rtos = city.rtos

			return <div ref={city.name} key={index} className={"grid-item popular-grid grid-rto "+ ((this.state.activeCity == title)? 'grid-active':'')} onClick={this.handleCityClick.bind(this, city)}>
					{(this.state.activeCity == title)? null:<span className='grid-title-super'>{titleSuper}</span>}
					<span className='grid-title'>{title}</span>
					{(this.state.activeCity == title)? <div>{rtos}</div>:null}
				</div>
		}, this)

		
		return (
			<div className='grid-wrapper'>
				<ListItem customClass='thin left-center' left={'Top Cities'} />
				{popularCities}
			</div>

		);
	}

});

module.exports = popularrtosgrid;