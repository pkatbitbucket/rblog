var React = require('react');
import VehicleStore from '../../stores/vehicle-store'
import ListItem from './ui/listitem'


function getStateFromStores(){
	return {
		popularVehicles: VehicleStore.getPopularVehicles()
	}
}

var popularvehiclesgrid = React.createClass({

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		VehicleStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		VehicleStore.removeChangeListener(this._onChange)	
	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	handleClick: function(item){
		this.props.onSelect(item)
	},

	render: function() {

		var pv = this.state.popularVehicles.sort(function(left, right){
			return (left.rank > right.rank) ? 1 : (left.rank < right.rank) ? -1 : 0;
		})

		pv = pv.map(function(item, index){

			var i = item.name.split(' ')

			var titleSuper = i[0]
			var title = i.slice(-1*(i.length-1)).join(' ')

			return <div key={index} className="grid-item popular-grid" onClick={this.handleClick.bind(this, item)}>
					<span className='grid-title-super'>{titleSuper}</span>
					<span className='grid-title'>{title}</span>
				</div>
		}, this)

		return (
			<div className='grid-wrapper'>
				<ListItem customClass='thin left-center' left={'Popular Cars'} />
				{pv}
			</div>

		);
	}

});

module.exports = popularvehiclesgrid;