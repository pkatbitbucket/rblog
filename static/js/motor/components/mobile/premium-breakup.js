import React from 'react'
import ListItem from './ui/listitem'
import PlanCard from './plancard'
import utils from '../../utils/apputils'


var premiumBreakup = React.createClass({

	render: function() {
		var plan = this.props.plan

		var BasicCovers = []


		for (var i = 0; i < plan.premiumBreakup.basicCovers.length; i++) {
			var item = plan.premiumBreakup.basicCovers[i]
			if((item.is_selected || item.default == 1) && item.premium > 0) {
				BasicCovers.push(<ListItem key={i} customClass='thin' left={item.name} right={utils.numberToINR(item.premium)} />)
			}
		};

		var AddonCovers = []

		for (var i = 0; i < plan.premiumBreakup.addonCovers.length; i++) {
			var item = plan.premiumBreakup.addonCovers[i]
			if((item.is_selected || item.default == 1) && item.premium > 0){
				AddonCovers.push(<ListItem key={i} customClass='thin' left={item.name} right={utils.numberToINR(item.premium)} />)
			}
		};

		var Discounts = []

		for (var i = 0; i < plan.premiumBreakup.discounts.length; i++) {
			var item = plan.premiumBreakup.discounts[i]
			if((item.is_selected || item.default == 1) && item.premium < 0){
				Discounts.push(<ListItem key={i} customClass='thin' left={item.name} right={utils.numberToINR(item.premium)} />)
			}
		};



		return (
			<div className='premium-breakup-wrapper'>
				{BasicCovers.length > 0? <ListItem customClass='thin header' left='Basic Covers' />:null}
				{BasicCovers}
				{AddonCovers.length > 0? <ListItem customClass='thin header' left='Addon Covers' />:null}
				{AddonCovers} 
				{Discounts.length > 0? <ListItem customClass='thin header' left='Discounts' />:null}
				{Discounts}
				<ListItem customClass='thin header' left='Taxes' />
				<ListItem customClass='thin' left='Service Tax' right={utils.numberToINR(plan.totalServiceTax)} />
			</div>
		);
	}

});


module.exports = premiumBreakup
