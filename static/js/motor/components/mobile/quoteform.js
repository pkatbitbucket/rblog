import React from 'react'
import CarSelector from './carselector'
import RTOSelector from './rtoselector'
import NCBSelector from './ncbselector'
import RegistrationYearSelector from './regyearselector'
import FormContent from './formcontent'
import Constants from '../../constants/app-constants'
import PageTitle from './ui/pagetitle'
import ListItem from './ui/listitem'
import IconText from './ui/icontext'
import Button from './ui/button'
import AppActions from '../../actions/app-actions'
import Apiutils from '../../utils/apiutils'
import GTMActions from '../../actions/gtm-actions'
import AppStore from '../../stores/app-store'
import VehicleStore from '../../stores/vehicle-store'
import RTOStore from '../../stores/rto-store'
import DateStore from '../../stores/date-store'
import TopBar from './ui/sitebar'
import Cookie from 'react-cookie'


function getStateFromStores(){
	return {
		isNewVehicle: AppStore.getData().isNewVehicle,
		selectedVariant: VehicleStore.getSelectedVariant(),
		selectedRTO: RTOStore.getSelectedRTO(),
		selectedRegYear: DateStore.getSelectedRegYear(),
	}
}

var quoteform = React.createClass({

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount:function() {
		
		AppStore.addChangeListener(this._onChange)
		VehicleStore.addChangeListener(this._onChange)
		RTOStore.addChangeListener(this._onChange)
		DateStore.addChangeListener(this._onChange)

		var params = this.props.params
		var quotetype = params[2]
		
		if(quotetype == 'renew' || quotetype == 'new' || quotetype == 'expired'){
			AppActions.setQuoteType(quotetype, function(){
				GTMActions.selectInsuranceType()
			})
		}
		else{
			this.transitionTo('/')
		}
		
		if(quotetype == 'expired'){
			Cookie.save('lms_campaign', 'motor-offlinecases', {path: '/'})
		}
	},

	componentWillUnmount:function() {
		AppStore.removeChangeListener(this._onChange)	
		VehicleStore.removeChangeListener(this._onChange)
		DateStore.removeChangeListener(this._onChange)
		RTOStore.removeChangeListener(this._onChange)	
	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	transitionTo: function(route){
		window.location.hash = '/quote/'+route
	},

	handleSubmit: function(){
		window.location.hash = '/results'	
		GTMActions.viewQuotes()
	},

	makePhoneCall: function(){
		window.location.href = "tel:"+TOLL_FREE
	},

	triggerEmail: function(){
		var link = "mailto:"+Constants.vehicle_type_verbose+"@coverfox.com"
   		window.location.href = link;
	},

	render: function() {
		var params = this.props.params

		var pageTitle, formContent

		var quotetype = params[2]
		var quotemode = params[3]

		var quoteFormTitle = 'Renew Existing '+Constants.vehicle_type_verbose+' Insurance'

		if(quotetype == 'new'){
			quoteFormTitle = 'Insurance for New '+Constants.vehicle_type_verbose
		}

		if(quotetype == 'expired'){
			quoteFormTitle = 'Renew Expired '+Constants.vehicle_type_verbose+' Insurance'
		}

		var forwardArrow = <img className='forward-arrow' src={STATIC_URL + 'img/motor/mobile/forward.png'} />

		var emailLeft = (<div>
			<span className="block-title">Forward email</span>
			<span className="block-subtitle dim">{'Just forward your previous year policy email to ' +Constants.vehicle_type_verbose+'@coverfox.com'}</span>
		</div>)

		var phoneLeft = (<div>
			<span className="block-title">Call Us</span>
			<span className="block-subtitle dim">{'Call us on our toll free number '+ TOLL_FREE+' and we will help you through the process.'}</span>
		</div>
			)


		if(!quotemode && (quotetype == 'renew' || quotetype == 'expired')){
			formContent = (<div>
				<ListItem left={<IconText icon={<img src={STATIC_URL + 'img/motor/mobile/form.png'} />} text='Fill form for Instant Quotes' />} right={forwardArrow} onClick={this.transitionTo.bind(this, 'renew/form')} />
				<ListItem left={<IconText icon={<img src={STATIC_URL + 'img/motor/mobile/upload.png'} />} text='Upload previous policy document' />} right={forwardArrow} onClick={this.transitionTo.bind(this, 'renew/upload')} />
				{/*<ListItem customClass="list-header" left='Lazier than this?' />
				<ListItem customClass="secondary" left={emailLeft} /> 
				<ListItem customClass="secondary" left={phoneLeft} /> */}
			</div>)
			pageTitle = <PageTitle ref='pagetitle' align='left' subtitle='Please select how you would to proceed' title={quoteFormTitle} theme='light' />
		}
		
		if(quotemode == 'form'){
			formContent = <FormContent onSubmit={this.handleSubmit}/>
			pageTitle = <PageTitle ref='pagetitle' align='left' title={quoteFormTitle} theme='light' />
		}

		var phoneLeft = <div className='phone-item'><img src={STATIC_URL + 'img/motor/mobile/phone.png'} /> Call us (toll free)</div>
		var emailLeft = <div className='email-item'><img src={STATIC_URL + 'img/motor/mobile/email.png'} /> Email Us</div>
		

		return (
			<div className='page'>
				<TopBar />
				{pageTitle}
				<div className="quote-form">
					{formContent}
					<ListItem customClass="secondary" left={phoneLeft} right={TOLL_FREE} onClick={this.makePhoneCall} />
					<ListItem customClass="secondary" left={emailLeft} right={Constants.vehicle_type_verbose+'@coverfox.com'} onClick={this.triggerEmail} />
				</div>
			</div>
		)
	}

});

module.exports = quoteform;
