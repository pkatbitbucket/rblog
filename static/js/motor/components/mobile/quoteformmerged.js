import React from 'react'
import FormContent from './formcontent'
import Constants from '../../constants/app-constants'
import PageTitle from './ui/pagetitle'
import ListItem from './ui/listitem'
import IconText from './ui/icontext'
import GTMActions from '../../actions/gtm-actions'
import TopBar from './ui/sitebar'
import Cookie from 'react-cookie'
import GTMUtils from '../../utils/gtmutils'


function getStateFromStores(){
	return {}
}

var quoteform = React.createClass({

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount:function() {

		// var params = this.props.params
		// var quotetype = params[2]
		
		// if(quotetype == 'renew' || quotetype == 'new' || quotetype == 'expired'){
		// 	AppActions.setQuoteType(quotetype, function(){
		// 		GTMActions.selectInsuranceType()
		// 	})
		// }
		// else{
		// 	this.transitionTo('/')
		// }
		
		// if(quotetype == 'expired'){
		// 	Cookie.save('lms_campaign', 'motor-offlinecases', {path: '/'})
		// }


		// ************************* MOVE TO AN APPROPRIATE LOCATION *********************
            
        if (VEHICLE_TYPE == 'fourwheeler'){
            GTMUtils.page_view('/vp/motor/step1/', 'VP-MOTOR-STEP1');
        }else{
            GTMUtils.page_view('/vp/bike/step1/', 'VP-BIKE-STEP1');
        }
        // ***************************************************************************************
       


	},

	componentWillUnmount:function() {
	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	handleSubmit: function(){
		window.location.hash = '/results'	
		GTMActions.viewQuotes()
	},

	makePhoneCall: function(){
		window.location.href = "tel:"+TOLL_FREE
	},

	triggerEmail: function(){
		var link = "mailto:"+Constants.vehicle_type_verbose+"@coverfox.com"
   		window.location.href = link;
	},

	render: function() {
		var emailLeft = (<div>
			<span className="block-title">Forward email</span>
			<span className="block-subtitle dim">{'Just forward your previous year policy email to ' +Constants.vehicle_type_verbose+'@coverfox.com'}</span>
		</div>)

		var phoneLeft = (<div>
			<span className="block-title">Call Us</span>
			<span className="block-subtitle dim">{'Call us on our toll free number '+ TOLL_FREE+' and we will help you through the process.'}</span>
		</div>
			)


		var phoneLeft = <div className='phone-item'><img src={STATIC_URL + 'img/motor/mobile/phone.png'} /> Call us (toll free)</div>
		var emailLeft = <div className='email-item'><img src={STATIC_URL + 'img/motor/mobile/email.png'} /> Email Us</div>
		

		return (
			<div className='page'>
				<TopBar />
				<div className="quote-form">
					<FormContent onSubmit={this.handleSubmit} />
					<ListItem customClass="secondary" left={phoneLeft} right={TOLL_FREE} onClick={this.makePhoneCall} />
					<ListItem customClass="secondary" left={emailLeft} right={Constants.vehicle_type_verbose+'@coverfox.com'} onClick={this.triggerEmail} />
				</div>
			</div>
		)
	}

});

module.exports = quoteform;
