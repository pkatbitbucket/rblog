
'use strict'
import React from 'react'
import ReactDOM from 'react-dom'
import Transition from 'react-addons-css-transition-group'
import Constants from '../../constants/app-constants'
import ListItem from './ui/listitem'
import ModalSelector from './ui/modalselector'
import PageTitle from './ui/pagetitle'
import TopBar from './ui/sitebar'
import GTMUtils from '../../utils/gtmutils'

var mountedTimeout

class quotetype extends React.Component {

	constructor(props){
		super(props)
		this.state = {
			modalOpen: false,
			mounted: false
		}
	}

	componentDidMount() {
		var self = this
	
		mountedTimeout = setTimeout(function(){
			self.setState({
				mounted: true
			})
		},300)

		// ************************* MOVE TO AN APPROPRIATE LOCATION *********************
            
        if (VEHICLE_TYPE == 'fourwheeler'){
            GTMUtils.page_view('/vp/motor/step1/', 'VP-MOTOR-STEP1');
        }else{
            GTMUtils.page_view('/vp/bike/step1/', 'VP-BIKE-STEP1');
        }
        // ***************************************************************************************
       
		
	}

	componentWillUnmount() {
		clearTimeout(mountedTimeout)
	}

	
	transitionTo(route){
		window.location.hash = '/quote/'+route
	}

	openModal(){
		this.setState({modalOpen: true})
	}

	closeModal(){
		this.setState({modalOpen: false})
	}

	onSelect(item){
		console.log(item)
		this.closeModal()
	}

	makePhoneCall(){
		window.location.href = "tel:"+TOLL_FREE
	}

	triggerEmail(){
		var link = "mailto:"+Constants.vehicle_type_verbose+"@coverfox.com"
   		window.location.href = link
	}

	handleExpired(){
		window.location.href = '/lp/car-insurance/renew-expired-policy/'
	}

	render() {


		var phoneLeft = <div className='phone-item'><img src={STATIC_URL + 'img/motor/mobile/phone.png'} /> Call us (toll free)</div>
		var emailLeft = <div className='email-item'><img src={STATIC_URL + 'img/motor/mobile/email.png'} /> Email Us</div>
		var forwardArrow = <img className='forward-arrow' src={STATIC_URL + 'img/motor/mobile/forward.png'} />


		return (
			<div className='page'>
				{/* Topbar */}
				<TopBar />

				<PageTitle ref='pagetitle' title={'Buy '+Constants.vehicle_type_verbose+' Insurance Online'} theme='light' />

				<Transition transitionName='modal'>
					{this.state.modalOpen? <ModalSelector options={options} optionsText='key' onSelect={this.onSelect.bind(this)} />:null }
				</Transition>

				<div className={"page-content " + (this.state.mounted? 'reveal':'') } >
				<ListItem customClass="card" left={'Renew '+Constants.vehicle_type_verbose+' Insurance'} right={forwardArrow} onClick={this.transitionTo.bind(this, 'renew/form')} />
				<ListItem customClass="card" left={'Policy already expired'} right={forwardArrow} onClick={this.transitionTo.bind(this, 'expired/form')} />
				<ListItem customClass="card" left={'Insurance for New '+Constants.vehicle_type_verbose} right={forwardArrow} onClick={this.transitionTo.bind(this, 'new/form')} />
				

				{/* toll free and email */}
				<ListItem customClass="secondary" left={phoneLeft} right={TOLL_FREE} onClick={this.makePhoneCall} />
				<ListItem customClass="secondary" left={emailLeft} right={Constants.vehicle_type_verbose+'@coverfox.com'} onClick={this.triggerEmail} />
				</div>

				{/*<ListItem left='Transfer Insurance of Second Hand Car' right='' onClick={this.transitionTo.bind(this, 'secondhand/split')} />*/}

				{/* Footer */}

			</div>
		)
	}
}

module.exports = quotetype



