var React = require('react')
var ControlGroup = require('../ui/controlgroup')
import AppStore from '../../stores/app-store'
import AppActions from '../../actions/app-actions'
import GTMActions from '../../actions/gtm-actions'
import Cookie from 'react-cookie'

var quoteTypeOptions = [{
		label: "Renew Car Insurance",
		value: "renew"
	},
	{
		label: "Policy already expired",
		value: "expired"
	},
	{
		label: "Insurance for new car",
		value: "new"
	}]

function getStateFromStores(){
	return {
		quoteType: AppStore.getData().quoteType || 'renew',
	}
}

var quotetypeselector = React.createClass({

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this._onChange)	
	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	handleUpdate: function(item){
		AppActions.setQuoteType(item.value, function(){
			GTMActions.selectInsuranceType()
		})

		if(item.value == 'expired'){
			Cookie.save('lms_campaign', 'motor-offlinecases', {path: '/'})
		}
	},

	render: function() {

		var qt = this.state.quoteType

		var selectedQuoteTypeOption = quoteTypeOptions.filter(function(item){
			return item.value == qt
		})[0]

		return (
			<div className="quote-type-selector-wrapper">
				<ControlGroup layout="vertical" items={quoteTypeOptions} selectedValue={selectedQuoteTypeOption} onUpdate={this.handleUpdate} />
			</div>	
		);
	}

});

module.exports = quotetypeselector;