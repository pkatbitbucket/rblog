import React from 'react'
import Transition from 'react-addons-css-transition-group'
import TransitionTimeout from '../../constants/transition-timeout'
import DateStore from '../../stores/date-store'
import AppStore from '../../stores/app-store'
import AppActions from '../../actions/app-actions'

import ListItem from './ui/listitem'
import ModalSelector from './ui/modalselector'
import Button from './ui/button'
import RenderInBody from './ui/renderinbody'

function getStateFromStores(){
	return {
		regYears: DateStore.getRegYears(),
		quoteType: AppStore.getData().quoteType,
		selectedRegistrationDate: DateStore.getAllDates().registrationDate,
	}
}

var regyearselector = React.createClass({

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		DateStore.addChangeListener(this._onChange)
		AppStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		DateStore.removeChangeListener(this._onChange)	
		AppStore.removeChangeListener(this._onChange)
	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	openRegYearSelector: function(){
		this.setState({modal: 'regyears'})
	},

	closeRegYearSelector:function(){
		this.setState({modal: null})	
	},

	onRegYearSelect:function(year){
		this.setState({
			modal: null
		})
		AppActions.selectRegistrationYear(year, this.state.quoteType)
	},

	render: function() {
		var regYears = this.state.regYears
		var selectedYear
		if(this.state.selectedRegistrationDate){
			selectedYear = this.state.selectedRegistrationDate.year()	
		}
		

		var topRegYears = regYears.slice(0,5).map(function(year, index){
			var buttonType = "empty"
			if(selectedYear == year){
				buttonType = "empty active"
			}
			return <Button customClass={buttonType} key={index} onClick={this.onRegYearSelect.bind(this, year)} >{year}</Button>
		}, this);

		if(selectedYear && (regYears.slice(0,5).indexOf(selectedYear) == -1)){
			topRegYears.push(<Button customClass="empty active" key={selectedYear} onClick={this.onRegYearSelect.bind(this, selectedYear)} >{selectedYear}</Button>)
		}

		topRegYears.push(<Button customClass="empty" key='others' onClick={this.openRegYearSelector} >Other</Button>)


		var regselector = (<div>
			<span>Car registration year</span>
			<div className="button-group">
				{topRegYears}
			</div>
		</div>)
	
		return (
			<div className="reg-year-selector-wrapper">
				<RenderInBody>
					<Transition transitionName='modal' {...TransitionTimeout}>
						{this.state.modal == 'regyears'? <ModalSelector type='grid' customClass='reg-item' header="Select Registration Year" options={regYears} onClose={this.closeRegYearSelector} onSelect={this.onRegYearSelect} />:null }
					</Transition>
				</RenderInBody>

				<ListItem left={regselector} />
			</div>
		)
	}

});




module.exports = regyearselector
