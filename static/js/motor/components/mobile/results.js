import React from 'react'
import Transition from 'react-addons-css-transition-group'
import TransitionTimeout from '../../constants/transition-timeout'
import AppStore from '../../stores/app-store'
import PlanStore from '../../stores/plan-store'
import DateStore from '../../stores/date-store'
import VehicleStore from '../../stores/vehicle-store'
import Apiutils from '../../utils/apiutils'
import Summarycard from './summarycard'
import ActionBar from './actionbar'
import FormContent from './formcontent'
import DateSelector from './dateselector'
import PlanCards from './plancards'
import AppActions from '../../actions/app-actions'
import GTMActions from '../../actions/gtm-actions'
import HalfModal from './ui/halfmodal'
import TopBar from './ui/sitebar'
import PageTitle from './ui/pagetitle'
import RenderInBody from './ui/renderinbody'
import Modal from './ui/modal'
import GTMUtils from '../../utils/gtmutils'

var updatedDatesTimeout

function getStateFromStores(){
	return {
		quoteRequest: AppStore.getQuoteRequest(),
		plans: PlanStore.getPlans(),
		hasUpdatedDates: DateStore.getUpdateStatus(),
		fastlaneStatus: VehicleStore.getFastlaneStatus(),
		hideDateSelector: AppStore.hideDateSelector()
	}
}


var results = React.createClass({
	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this._onChange)
		PlanStore.addChangeListener(this._onChange)
		DateStore.addChangeListener(this._onChange)

		var previousNCB = AppStore.getData().previousNCB
		var isNewVehicle = AppStore.getData().isNewVehicle

		if(typeof previousNCB === 'undefined' && isNewVehicle != 1){
			window.location.hash = ''
		}
		else{
			AppActions.startFetchingPlans()
			Apiutils.getPlans(AppStore.getQuoteRequest())

			if(!this.state.fastlaneStatus && !this.state.hasUpdatedDates && !this.state.hideDateSelector){
				var self = this;
				updatedDatesTimeout = setTimeout(function(){
					self.openDateSelector()
				}, 5000)
			}

			// ******** MOVE THIS LATER TO AN APPROPRIATE LOCATION *******
	        if (VEHICLE_TYPE == 'fourwheeler') {
	            GTMUtils.page_view('/vp/motor/initiate-results/', 'VP-MOTOR-INITIATE-RESULTS');
	        } else {
	            GTMUtils.page_view('/vp/bike/initiate-results/', 'VP-BIKE-INITIATE-RESULTS');
	        }
	        // *****************************************************************
		}
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this._onChange)
		PlanStore.removeChangeListener(this._onChange)
		DateStore.removeChangeListener(this._onChange)
		clearTimeout(updatedDatesTimeout)
		Apiutils.abortAsyncRequest()
	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	openDateSelector: function(){
		this.setState({
			modal:'dates'
		});
	},

	closeDateSelector: function(){
		this.setState({
			modal:null
		});
	},

	openEditModal: function(){
		this.setState({
			modal:'edit'
		});
	},

	closeEditModal: function(){
		this.setState({
			modal:null
		});
	},

	handleUpdate: function(changed){
		this.closeDateSelector()
		GTMActions.updateQuote()
		if(changed){
			AppActions.startFetchingPlans()
			Apiutils.getPlans(AppStore.getQuoteRequest())
		}
	},

	handleEditSave: function(){

	},

	handleEditClose: function(){
		this.closeEditModal()
	},

	render: function() {
		return(
			<div className='results-wrapper page'>
				<TopBar />
				{/*<PageTitle ref='pagetitle' theme='topbar' />*/}
				<Summarycard onEditClick={this.openEditModal} />
				<ActionBar customClass={this.state.plans.length > 0? 'reveal':''} />
				<PlanCards />
				<Transition transitionName='fade' {...TransitionTimeout} >
					{this.state.modal == 'dates'? <HalfModal content={<DateSelector onUpdate={this.handleUpdate} close={this.closeDateSelector} />} />:null}
				</Transition>
				<RenderInBody>
					<Transition transitionName='modal' {...TransitionTimeout} >
						{(this.state.modal == 'edit')? <Modal onClose={this.handleEditClose} header='Edit Details' content={<FormContent onSubmit={this.handleEditSave}/>} ></Modal>:null }
					</Transition>
				</RenderInBody>
			</div>
		);
	}

});

module.exports = results;

