import React from 'react'
import Transition from 'react-addons-css-transition-group'
import TransitionTimeout from '../../constants/transition-timeout'
import RTOStore from '../../stores/rto-store'
import AppActions from '../../actions/app-actions'
import RenderInBody from './ui/renderinbody';
import PopularRTOsGrid from './popularrtosgrid'
//var moment = require('moment')

import ListItem from './ui/listitem'
import ModalSelector from './ui/modalselector'


function getStateFromStores(){
	return {
		rtos: RTOStore.getRTOS(),
		popularRTOs: RTOStore.getPopularRTOs(),
		selectedRTO: RTOStore.getSelectedRTO()
	}
}

class rtoselector extends React.Component {
	constructor(props){
		super(props)
		this._onChange = this._onChange.bind(this)
		this.state = getStateFromStores()
	}

	componentDidMount() {
		RTOStore.addChangeListener(this._onChange)
		// var today = moment()
	}

	componentWillUnmount() {
		RTOStore.removeChangeListener(this._onChange)	
	}

	_onChange(){
		this.setState(getStateFromStores())
	}

	openRTOSelector(){
		this.setState({modal: 'rto'})
	}

	closeRTOSelector(){
		this.setState({modal: null})
	}

	onRTOSelect(rto){
		this.setState({
			modal: null
		});
		AppActions.selectRTO(rto)
	}

	selectRTOfromId(rtoid){
		this.setState({
			modal: null
		});
		AppActions.selectRTOfromId(rtoid)
	}



	render() {

		var popularRTOs = this.state.popularRTOs

		var categories = []

		for (var i = popularRTOs.length - 1; i >= 0; i--) {
			if(categories.indexOf(popularRTOs[i].category) == -1){
				categories.push(popularRTOs[i].category)
			}
		};

		var addIcon = <div className='add-icon'>+</div>
		var editIcon = <div className='edit-icon'>Edit</div>

		var right = addIcon
		var left = 'Select car registration RTO'

		if(this.state.selectedRTO){
			right = editIcon
			left = (<div>
				<span className="left-title">Car registration RTO</span>
				<span className="left-sub">{this.state.selectedRTO.name}</span>
				</div>
				)
		}

		var rtoTitle = <div>
			<span className='block-title'>Which RTO was your car registered in?</span>
			<span className='block-subtitle'>Choose from the list of top cities below or search for the RTO above.</span>
		</div>



		return (
			<div className="car-selector-wrapper">
				{/*<Transition transitionName='modal'>
					{this.state.modal == 'rto'? <ModalSelector searchText="Type to search for your RTO" onClose={this.closeRTOSelector.bind(this)} header="Select your RTO" categories={categories} searchOptions={this.state.rtos} options={this.state.popularRTOs} optionsText='name' onSelect={this.onRTOSelect.bind(this)} />:null }
				</Transition> */}

				<RenderInBody>
					<Transition transitionName='modal' {...TransitionTimeout}>
						{this.state.modal == 'rto'? <ModalSelector 
													uid='rto-selector-content'
													showSearch={true} 
													customContent={<PopularRTOsGrid onSelect={this.selectRTOfromId.bind(this)} />} 
													customHangingContent={<ListItem customClass='thin card full-width' left={rtoTitle} />}
													searchText="Type to search for your RTO" 
													onClose={this.closeRTOSelector.bind(this)} 
													header="Select car registration RTO" 
													options={this.state.rtos} 
													optionsText='name' 
													onSelect={this.onRTOSelect.bind(this)} />:null }
					</Transition>
				</RenderInBody>

				<ListItem left={left} right={right} onClick={this.openRTOSelector.bind(this)} />
			</div>
		)
	}
}

module.exports = rtoselector
