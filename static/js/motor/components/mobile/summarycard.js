import React from 'react';
import AppStore from '../../stores/app-store'
import VehicleStore from '../../stores/vehicle-store'
import RTOStore from '../../stores/rto-store'
import DateStore from '../../stores/date-store'
import ListItem from './ui/listitem'
import Button from './ui/button'
import GTMActions from '../../actions/gtm-actions'
var moment = require('moment')

function getStateFromStores(){
	var states = {
		vehicle: VehicleStore.getSelectedVehicle(),
		variant: VehicleStore.getSelectedVariant(),
		rto: RTOStore.getSelectedRTO(),
		regNo: RTOStore.getRegistrationNumber(),
		dates: DateStore.getAllDates(),
		otherAppData: AppStore.getData(),
		fastlaneStatus: VehicleStore.getFastlaneStatus()
	}
	if(!states.vehicle || !states.variant || !states.rto)
		window.location.hash = ''
	return states
}


var summarycard = React.createClass({

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this._onChange)
		VehicleStore.addChangeListener(this._onChange)
		RTOStore.addChangeListener(this._onChange)
		DateStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this._onChange)
		VehicleStore.removeChangeListener(this._onChange)
		RTOStore.removeChangeListener(this._onChange)
		DateStore.removeChangeListener(this._onChange)
	},

	toggleFullSummary: function() {
		this.setState({
			showFullSummary: (this.state.showFullSummary? false:true)
		}, function(){
			GTMActions.seeQuoteDetailsOnResults(this.state.showFullSummary)
		});
	},

	_onChange: function(){
		this.setState(getStateFromStores())
	},

	render: function() {

		var summaryLeft = (<div>
			<span className="block-title">{this.state.vehicle.name + ' ' + this.state.variant.name}</span>
			<span className="block-subtitle dim">{this.state.rto.name}</span>
		</div>)

		var otherAppData = this.state.otherAppData

		var ncbSection
		if(otherAppData.isNewVehicle == 0 && otherAppData.isUsedVehicle != 1 && moment().diff(this.state.dates.pastPolicyExpiryDate, 'days')<90){
			var hideNCBValue = otherAppData.hasClaimedPastYear || otherAppData.NCBUnknown
			ncbSection = (<div>
					<ListItem customClass='xthin' left="Claimed in the previous year" right={otherAppData.hasClaimedPastYear?'Yes':'No'}/>
					{hideNCBValue ? null : <ListItem customClass='xthin' left="Previous No Claim Bonus (NCB)" right={otherAppData.previousNCB? otherAppData.previousNCB.key:''}/>}
				</div>)
		}else if(otherAppData.isUsedVehicle == 1){
			var showNCBValue = otherAppData.isNCBCertificate && otherAppData.newNCB != undefined
			ncbSection = (<div>
					<ListItem customClass='xthin' left="NCB Certificate" right={otherAppData.isNCBCertificate? 'Yes' : 'No' }/>
					{showNCBValue ? <ListItem customClass='xthin' left="New No Claim Bonus (NCB)" right={otherAppData.newNCB ? otherAppData.newNCB.key : ''}/> : null}
				</div>)
		}

		var regNo
		if(this.state.regNo && this.state.regNo.length>0){
			regNo = (<div>
					<ListItem customClass='xthin' left="Registration Number" right={this.state.regNo.join('-')}/>
				</div>)
		}

		return (
			<div className='summary-card'>
				<div className="summary-card-top">
					<ListItem customClass='thin' left={summaryLeft} right={<Button customClass="empty small" onClick={this.toggleFullSummary} >{this.state.showFullSummary? 'Hide':'Details'}</Button>}/>
				</div>
				<div className={"summary-card-details " + (this.state.showFullSummary? 'reveal':'')}>
					{regNo}
					{ncbSection}
					{!this.state.fastlaneStatus && (otherAppData.isNewVehicle == 0)? <ListItem customClass='xthin' left="Registration date" right={this.state.dates.registrationDate.format('DD-MM-YYYY')}/>:null}
					{(otherAppData.isNewVehicle == 0 && otherAppData.isUsedVehicle != 1)? <ListItem customClass='xthin' left="Previous policy expiry date"  right={this.state.dates.pastPolicyExpiryDate.format('DD-MM-YYYY')}/>:null}
					{(otherAppData.isNewVehicle == 1)? <ListItem customClass='xthin' left="Manufacturing date"  right={this.state.dates.manufacturingDate.format('DD-MM-YYYY')}/>:null}
					{(otherAppData.isNewVehicle == 1)? <ListItem customClass='xthin' left="New policy start date"  right={this.state.dates.newPolicyStartDate.format('DD-MM-YYYY')}/>:null}
					{/*<ListItem customClass='xthin' left={<Button customClass="empty" onClick={this.props.onEditClick}>Edit</Button>} />*/}
				</div>
			</div>
		);
	}

});


module.exports = summarycard
