import React from 'react';

class button extends React.Component {
	render() {
		var otherAttr = {}
		var content = this.props.children
		if(this.props.type === 'loading'){
			content = (<div className="spinner white">
			                <div className="bounce1"></div>
			                <div className="bounce2"></div>
			                <div className="bounce3"></div>
						</div>)
		}

		if(this.props.style)
			otherAttr.style = this.props.style
		
		return (
			<button disabled={this.props.disabled} onClick={this.props.onClick} className={"button "+ (this.props.customClass? this.props.customClass:"") } {...otherAttr}>
				{content}
			</button>
		);
	}
}

module.exports = button
