import React from 'react';
import OptionsPanel from './optionspanel';

class categorisedoptions extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			currentCategory: this.props.collapseAll? null:this.props.categories[0]
		}
	}

	setCategory(category){
		console.log(category);
		this.setState({
			currentCategory: category
		});
	}

	render() {
		var categories = this.props.categories;
		var options = this.props.options;
		var optionsText = this.props.optionsText;
		var categoryText = this.props.categoryText;


		categories = categories.map(function(item, index){

			var _options = options.filter(function(option){
				return option[categoryText] == item
			});



			_options = <OptionsPanel showOthers={false} options={_options} optionsText={optionsText} onSelect={this.props.onSelect.bind(this)} />
			

			return <div key={index}>
				<div className="option-category" onClick={this.setCategory.bind(this, item)} >
					{item}
				</div>
				{(this.state.currentCategory == item)? 
					_options:null
				}


			</div>
		}, this);


		return (
			<div>
				{categories}
			</div>
		);
	}
}

module.exports = categorisedoptions;
