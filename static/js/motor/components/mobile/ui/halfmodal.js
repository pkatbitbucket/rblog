import React from 'react'
import Transition from 'react-addons-css-transition-group'

class halfModal extends React.Component {
	constructor(props){
		super(props)
		this.state = {}
	}

	componentDidMount() {
		setTimeout(function(){
			document.body.style.overflow = "hidden"		
		}, 600)
	}

	componentWillUnmount(){
		document.body.style.overflow = "auto"	
	}

	render() {
		var onClose = this.props.onClose || function(){}
		return (
			<div className='half-modal-wrapper'>
				<div className="half-modal-cover" onClick={onClose}></div>
				<div className="half-modal-content">
					{this.props.content}
					{this.props.children}
				</div>
			</div>
		);
	}
}

module.exports = halfModal
