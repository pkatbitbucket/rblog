import React from 'react';

class icontext extends React.Component {
	render() {
		var icon = this.props.icon
		var text = this.props.text

		return (
			<span className="icon-text">
				{icon} {text}
			</span>
		);
	}
}

module.exports = icontext
