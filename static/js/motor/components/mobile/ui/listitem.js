import React from 'react';

class listitem extends React.Component {
	

	render() {
		var onClick = this.props.onClick
		var right = this.props.right
		var middle = this.props.middle

		return (
			<div className={'list-item '+ (this.props.customClass? this.props.customClass:'') + (!onClick? ' no-tap':'') + (right? ' has-right':'') + (middle? ' has-middle':'')} onClick={this.props.onClick} >
				{this.props.left? <div className="item-left">
					{this.props.left}
				</div>:null}
				{this.props.middle? <div className="item-middle">
					{this.props.middle}
				</div>:null}
				{this.props.right? <div className="item-right">
					{this.props.right}
				</div>:null}
				{this.props.bottom? <div className="item-bottom">
					{this.props.bottom}
				</div>:null}
			</div>
		);
	}
}

module.exports = listitem
