import React from 'react';
import ReactDOM from 'react-dom';
import ViewHeader from './viewheader';
import Spinner from './spinner';


class modal extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			contentTopPos: 0
		}
	}
	componentDidMount() {
		setTimeout(function(){
			document.getElementById('content').style.overflow = "hidden"
			document.getElementById('content').style.position = "fixed"		
		}, 600)

		var self = this
		setTimeout(function(){
			self.setState({
				mounted: true 
			});
		}, 500)
	}

	componentDidUpdate(prevProps, prevState) {
		var node = ReactDOM.findDOMNode(this.refs.hanging)
		var header = ReactDOM.findDOMNode(this.refs.header)
		var pos = 0
		
		if(node){
			pos+=node.offsetHeight
		}
		if(header){
			pos+=header.offsetHeight
		}

		if(prevState.contentTopPos !== pos){
			this.setState({
				contentTopPos: pos
			});
		}
	}

	componentWillUnmount(){
		document.getElementById('content').style.overflow = "scroll"	
		document.getElementById('content').style.position = "relative"		
	}

	render() {
		var content = this.props.content
		var children = this.props.children
		var modalheader = this.props.header

		var style={
			top: this.state.contentTopPos + 'px'
		}

		return (
			<div className={"modal " + (this.props.customClass? this.props.customClass:'')}>
				<ViewHeader ref='header'>
					<div className="header-back-btn" onClick={this.props.onClose}></div>
					<div className="header-title">{modalheader}</div>
				</ViewHeader>
				{this.props.hangingContent? <div ref="hanging" className='hanging-content'>{this.props.hangingContent}</div>:null}
				
				{this.state.mounted? <div className="modal-content" id={this.props.id? this.props.id:''} style={style} >
					{content}
					{children}
				</div>:<div className='full-screen-spinner'><Spinner /></div>}
			</div>
		);
	}
}

module.exports = modal
