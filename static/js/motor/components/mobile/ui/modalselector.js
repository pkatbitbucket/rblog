import React from 'react';
import ReactDOM from 'react-dom';
import ViewHeader from './viewheader';
import ListItem from './listitem';
import Modal from './modal';
import Spinner from './spinner';
import OptionsPanel from './optionspanel';
import CategorisedOptions from './categorisedoptions';

var t1


var modalselector = React.createClass({

	getInitialState: function() {
		return {
			options: []
		}
	},

	componentDidMount: function() {
		var self = this
		t1 = setTimeout(function(){
			self.setState({
				options: self.props.options 
			});
		}, 450)
	},

	componentWillUnmount: function() {
	},

	handleKeywordChange: function(evt){
		var keyword = evt.target.value.trim().toLowerCase().replace(/[^a-zA-Z0-9]/g, "");
		this.setState({
			keyword:keyword
		});
	},

	handleClick: function(item){
		this.props.onSelect(item)
	},

	handleSearchFocus: function(){
		var self = this
		setTimeout(function(){
			self.setState({
				searchHasFocus:true
			});
		}, 100)
		
	},

	handleSearchBlur: function(){
		var self = this
		self.setState({
			searchHasFocus:false
		});
		self.clearKeyword(false)
	},

	clearKeyword: function(focusAfterClearing = true){
		var searchInput = ReactDOM.findDOMNode(this.refs['search-input'])
		searchInput.value = ''	
		if(focusAfterClearing){
			searchInput.focus()	
		}
		this.setState({
			keyword:''
		});
	},

	render: function() {
		var placeholder = this.props.searchText
		var showSearch = this.props.showSearch
		var customContent = this.props.customContent
		var optionsText = this.props.optionsText
		var overlayLoading = this.props.overlayLoading
		var customHangingContent = this.props.customHangingContent
		var optionsText = this.props.optionsText
		var allOptions = this.state.options
		var keyword = this.state.keyword




		var modalcontent

		var onClose = this.props.onClose

		if(this.state.searchHasFocus){
			onClose = this.handleSearchBlur
		}

		if(keyword && keyword.length > 0){
			var filteredOptions = allOptions.filter(function(option){
					if(optionsText){
						return option[optionsText].toLowerCase().replace(/[^a-zA-Z0-9]/g, "").indexOf(keyword) > -1
					}
					else{
						return option.toString().toLowerCase().replace(/[^a-zA-Z0-9]/g, "").indexOf(keyword) > -1
					}
				})
		}
		else{
			var filteredOptions = allOptions
		}





		if(customContent){
			modalcontent = customContent
		}

		if(!customContent || this.state.searchHasFocus){
			modalcontent = filteredOptions.map(function(option, index){
				if(optionsText){ 
					var title = option[optionsText];
				}
				else{
					var title = option;
				}
				return <ListItem customClass={"thin " + (this.props.type == 'grid'? 'inline ':'') + (this.props.customClass? this.props.customClass:'')} key={index} left={title} onClick={this.handleClick.bind(this, option)} />
			}, this)
		}

		var searchBar = <div className="search-bar">
							<input ref="search-input" type="text" onFocus={this.handleSearchFocus} placeholder={placeholder} className="bar-input" onChange={this.handleKeywordChange} />
							{(this.state.keyword && this.state.keyword !== '')? <div onClick={this.clearKeyword} className="clear-keyword">X</div>:null}
						</div>

		var hangingContent = (this.state.searchHasFocus? null:customHangingContent)
				
		var header = searchBar

		if(!showSearch){
			header = this.props.header
		}
		
		return (
			<Modal id={this.props.uid} header={header} hangingContent={hangingContent} onClose={onClose} >
				{modalcontent}
				{overlayLoading? <div className='full-screen-spinner'><Spinner /></div>:null}
			</Modal>
		);
	}

});

module.exports = modalselector;


