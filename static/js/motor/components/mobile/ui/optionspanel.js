import React from 'react';
import ListItem from './listitem';

class optionspanel extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			showOthers: this.props.showOthers
		}
	}

	toggleOthers(){
		this.setState({
			showOthers: this.state.showOthers? false:true
		});
	}

	onSelect(option){
		this.props.onSelect(option);
	}

	render() {

		var options = this.props.options;
		var showOthers = this.state.showOthers;
		var optionsText = this.props.optionsText;

		var popularoptions = options.filter(function(option){
			return option.is_popular;
		});

		var otherOptions = options.filter(function(option){
			return !option.is_popular;
		});

		popularoptions = popularoptions.map(function(option){
			if(optionsText){
				var title = option[optionsText];
			}
			else{
				var title = option;
			}

			return <ListItem customClass={"thin " + (this.props.type == 'grid'? 'inline':'')} key={title} left={title} right='' onClick={this.onSelect.bind(this, option)} />
		}, this);

		// if(popularoptions.length > 0 && popularoptions.length < options.length){
		// 	popularoptions.push(<ListItem customClass="list-header" left={'Others'} />);
		// }

		otherOptions = otherOptions.map(function(option){
			if(optionsText){
				var title = option[optionsText];
			}
			else{
				var title = option;
			}
			return <ListItem customClass={"thin " + (this.props.type == 'grid'? 'inline':'')} key={title} left={title} right='' onClick={this.onSelect.bind(this, option)} />
		}, this);

		return (
			<div className="options-panel">
				{popularoptions.length>0? <div className="options-popular">
					<ListItem customClass="list-header" left={this.props.popularHeader} />
					{popularoptions}
					
				</div>:null}

				{(this.state.showOthers || popularoptions.length == 0)?
				<div className={"options-others " + (this.props.type? this.props.type:'')}>
					{otherOptions}
				</div>:null}
			</div>	
		);
	}
}

module.exports = optionspanel;
