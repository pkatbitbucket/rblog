import React from 'react';

class pagetitle extends React.Component {

	render() {


		return (
			<div className={"page-title-wrapper " + (this.props.align? (this.props.align+' '):'') + (this.props.theme? (this.props.theme+' '):'')}>
				<div className="page-title-cover"></div>
				<div className="page-title" >
					{this.props.title}
				</div>
				<div className="page-subtitle">
					{this.props.subtitle}
				</div>
			</div>
		);
	}
}


module.exports = pagetitle