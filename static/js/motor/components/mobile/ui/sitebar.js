var React = require('react');
var APPUtils = require('../../../utils/apputils');


var bar = React.createClass({

	getInitialState: function() {
		return {
			style:{} 
		};
	},

	componentDidMount: function() {
		document.getElementById('content').addEventListener("scroll", this.revealTopBar);
	}, 

	componentWillUnmount: function() {
		document.getElementById('content').removeEventListener("scroll", this.revealTopBar);
	},

	revealTopBar: function(){
		var scrollTop = document.getElementById('content').scrollTop
		
		this.setState({
			style: {
				backgroundColor: 'rgba(255, 112, 76,'+ scrollTop/48 +')'
			}
		});
		
		
		
	},

	render: function() {
		var tparty = APPUtils.getValueFromQueryParam('tparty')
		var cardekho = tparty && tparty.toLowerCase()==='cardekho' ? <img style={{width:'160px', 'float':'right', 'paddingRight':'15px', 'paddingTop': '5px'}} src={STATIC_URL + "motor_product/img/cardekho-logo-v3.png"} alt="Cardekho.com"/> : null
		return (
			<header>
				<div className="mobile-header-wrapper">
					<div className="logo" style={{'width':'100%'}}>
						<a href='/'><img src={STATIC_URL + 'img/logos/white_205_42.png'} alt="Logo" /></a> 
						{cardekho}
					</div>
					<div className="page-title">
						{this.props.pagetitle}
					</div>
				</div>						
			</header>	
		);
	}
});

module.exports = bar;