import React from 'react';

class spinner extends React.Component {
	render() {
		return (
			<div className={"spinner "+ (this.props.customClass? this.props.customClass:'')}>
                <div className="bounce1"></div>
                <div className="bounce2"></div>
                <div className="bounce3"></div>
            </div>
		);
	}
}

module.exports = spinner