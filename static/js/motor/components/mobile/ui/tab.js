var React = require('react');

var tab = React.createClass({

	render: function() {
		return (
			<li onClick={this.props.onClick} className={this.props.active? 'active':''}>
				{this.props.children}
			</li>
		);
	}

});

module.exports = tab;