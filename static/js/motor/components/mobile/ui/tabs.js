var React = require('react');

var tabs = React.createClass({

	render: function() {
		return (
			<ul className='tabs'>
				{this.props.children}
			</ul>
		);
	}

});

module.exports = tabs;