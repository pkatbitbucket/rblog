import React from 'react'

class toggleswitch extends React.Component {

	constructor(props){
		super(props)
		this.state = {
			on: this.props.on || false
		}
	}

	handleClick(){
		var self = this
		self.setState({
			on: (self.state.on? false:true)
		}, function(){
			self.props.onClick(self.state.on)	
		});	
	}

	render() {

		return (
			<div className={"toggle-switch-wrapper " + (this.state.on? 'on':'off')} onClick={this.handleClick.bind(this)}>
				<div className="toggle-switch" ></div>
				<div className="toggle-switch-text">{this.state.on? 'Yes':'No'}</div>
			</div>
		)
	}
}

module.exports = toggleswitch
