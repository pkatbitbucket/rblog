import React from 'react';

class viewheader extends React.Component {
	render() {
		return (
			<div className="view-header">
				{this.props.children}
			</div>
		);
	}
}


module.exports = viewheader;