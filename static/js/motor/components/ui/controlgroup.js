var React = require('react');
var RadioControl = require('./radiocontrol')
var isEqual = require('lodash.isequal')

var controlgroup = React.createClass({

	getInitialState: function() {
		return {
			selectedRadio: this.props.selectedValue
		};
	},

	getDefaultProps: function() {
		return {
			controlType: 'radio',
			layout: 'vertical'
		}
	},

	handleRadioChange: function(item){
		this.setState({
			selectedRadio: item
		})
		this.props.onUpdate(item)
	},
	render: function() {
		var layout = this.props.layout
		var controlType = this.props.controlType
		var classString = "controlgroup " + layout + " " + controlType + " " + (this.props.customClass? this.props.customClass:'')
		var propitems = this.props.items
		var selectedRadio = this.state.selectedRadio

		var items



		for (var i = 0; i < propitems.length; i++) {
			if(controlType == 'radio'){
				items = propitems.map(function(item, i){
					return <RadioControl key={i} item={item} change={this.handleRadioChange} selected={isEqual(selectedRadio, item)} />
				}, this)
			}
			else{

			}
		};


		return (
			<div className={classString}>
				{items}
			</div>
		)
	}

})

module.exports = controlgroup