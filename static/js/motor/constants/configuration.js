var vehicle_type = VEHICLE_TYPE;

    var Configuration = {};

    Configuration.fourwheeler = {
        paPassengerAmounts:[0, 10000, 50000, 100000, 200000],

        vd_options: [{
            key: 'No Deductible',
            value: 0,
            saveupto: 0,
        }, {
            key: 'Rs. 2,500',
            value: 2500,
            saveupto: 750,
        }, {
            key: 'Rs. 5,000',
            value: 5000,
            saveupto: 1500,
        }, {
            key: 'Rs. 7,500',
            value: 7500,
            saveupto: 2000,
        }, {
            key: 'Rs. 15,000',
            value: 15000,
            saveupto: 2500,
        }],
        addons: [{
            name: 'Zero Depreciation',
            id: 'addon_isDepreciationWaiver',
            desc: 'In case of every claim settlement, an amount is supposed to be paid from your pocket depending on the standard depreciation logic applied on the parts involved in the repair. If you do not want any amount to be paid from your end in case of simple repairs then go for Zero Depreciation.'
        }, {
            name: 'Invoice Cover',
            id: 'addon_isInvoiceCover',
            desc: 'In case of theft or total damage to your car, you are only eligible for reimbursement up to the Insured declared value of your car, which will be very less than the Invoice value. In case of such an event, selecting Invoice cover makes you eligible for full Invoice amount reimbursement.'
        }, {
            name: '24x7 Roadside Assistance',
            id: 'addon_is247RoadsideAssistance',
            desc: 'Road side assistance provides support for basic on-road breakdown situations like tyre change, battery jump start, emergency fuel, medical assistance etc which are not covered under Insurance. As the price is very nominal, it is a good to have add-on.'
        }, {
            name: 'Engine Protector',
            id: 'addon_isEngineProtector',
            desc: 'When the Engine of the car is submerged in a water logged area, using or cranking the engine can result in engine ceasing. This is not covered under regular Insurance. Engine protector covers such non-accidental exclusions related to your engine. It is a must buy for luxury cars where engine is very costly & is placed at low ground clearance.'
        }, {
            name: 'NCB Protection',
            id: 'addon_isNcbProtection',
            desc: 'No Claim Bonus or NCB is the discount you get on your premium for every consecutive claim free year. This can go up to 50% (or even higher) for 5 or more claim free years. This can also get down to zero with even a single claim, so if you have a good NCB then opt for NCB protection to preserve your NCB even after you make a claim.'
        }, ],
        is_cng_kit_valid: true,
        vehicle_type: 'fourwheeler',
        lms_default_campaign: 'MOTOR',
        ga_category: 'MOTOR'
    };

    Configuration.twowheeler = {
        paPassengerAmounts:[0, 10000, 50000, 100000],
        vd_options: [{
            key: 'No Deductible',
            value: 0,
            saveupto: 0,
        }, {
            key: 'Rs. 500',
            value: 500,
            saveupto: 50,
        }, {
            key: 'Rs. 750',
            value: 750,
            saveupto: 75,
        }, {
            key: 'Rs. 1,000',
            value: 1000,
            saveupto: 125,
        }, {
            key: 'Rs. 1,500',
            value: 1500,
            saveupto: 200,
        }, {
            key: 'Rs. 3,000',
            value: 3000,
            saveupto: 250,
        }],
        addons: [{
            name: 'Zero Depreciation',
            id: 'addon_isDepreciationWaiver',
            desc: 'In case of every claim settlement, an amount is supposed to be paid from your pocket depending on the standard depreciation logic applied on the parts involved in the repair. If you do not want any amount to be paid from your end in case of simple repairs then go for Zero Depreciation.'
        }, ],
        is_cng_kit_valid: false,
        vehicle_type: 'twowheeler',
        lms_default_campaign: 'MOTOR-BIKE',
        ga_category: 'BIKE'
    };

    var common = {
        age_wise_previous_ncb: {
            0: 0,
            1: 0,
            2: 20,
            3: 25,
            4: 35,
            5: 45,
            6: 50
        },

        ncb_options: [{
                key: '0%',
                value: 0
            }, {
                key: '20%',
                value: 20
            }, {
                key: '25%',
                value: 25
            }, {
                key: '35%',
                value: 35
            }, {
                key: '45%',
                value: 45
            }, {
                key: '50%',
                value: 50
            }

        ],

        boolean_options: [{
            key: 'No',
            value: 0
        }, {
            key: 'Yes',
            value: 1
        }],

        occupation_options: [{
            key: 'Doctors registered with Government',
            value: 5
        }, {
            key: 'Central / State Government Employees',
            value: 4
        }, {
            key: 'Teacher in Govt. recognized Institutions',
            value: 3
        }, {
            key: 'Defense and Para Military Service',
            value: 2
        }, {
            key: 'Practicing Chartered Accountant',
            value: 1
        }, {
            key: 'Others',
            value: 0
        }, ],

    };



    var getConfiguration = function() {
        var config = Configuration[vehicle_type];
        for(var key in common){
            config[key] = common[key]
        }
        return config;
    };

    module.exports =  {
        getConfiguration: getConfiguration,
    }