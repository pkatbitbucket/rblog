module.exports = {
		vehicles: ['Maruti Swift',
				'Maruti Wagon R',
				'Hyundai i10',
				'Maruti Swift Dzire',
				'Maruti Alto',
				'Honda City',
				'Honda New City',
				'Hyundai Santro Xing',
				'Hyundai i20',
				'Hyundai Elite i20'],

		rtos: {
			'New Delhi' :[ 
				'DL-3 - South Delhi',
				'DL-2 - New Delhi',
				'DL-9 - South West Delhi',
				'DL-1 - Delhi North Hill Road',
				'DL-4 - Delhi West Raja Garden',
				'DL-10 - West Delhi',
				'DL-8 - North West Delhi',
				'DL-12 - South West Delhi Vasant Vihar',
				'DL-7 - Delhi East',
				'DL-11 - North West Delhi',
				'DL-13 - East Delhi Shahdara',
				'DL-6 - Delhi Central Sarai Kale Khan',
				'DL-5 - Delhi North Loni Road'
			],
			'Mumbai': [
				'MH-01 - Mumbai Tardeo',
				'MH-02 - Mumbai Andheri',
				'MH-04 - Thane Mumbai',
				'MH-03 - Mumbai Worli',
				'MH-43 - Navi Mumbai',
				'MH-05 - Kalyan Mumbai',
				'MH-48 - Virar Vasai Mumbai',
				'MH-47 - Borivali Mumbai'
			],
			'Bangalore': [
				'KA-03 - Bangalore Indiranagar',
				'KA-04 - Bangalore Yashwanthpur',
				'KA-01 - Bangalore Kormangala',
				'KA-05 - Bangalore Jayanagar',
				'KA-51 - Bangalore Electronic City',
				'KA-53 - Bangalore Krishnarajpuram',
				'KA-02 - Bangalore Rajajinagar',
				'KA-41 - Bangalore Kengeri',
				'KA-50 - Bangalore Yalahanka',
				'KA-60 - RT Nagar Bangalore',
				'KA-61 - Marathahalli Bangalore',
				'KA-10 - Bangalore Chamrajpeth',
				'KA-52 - Nelamangala Bangalore',
				'KA-43 - Devanahalli Bangalore'
			],
			'Pune':[
				'MH-12 - Pune',
				'MH-14 - Pimpri-Chinchwad'
			],
			'Gurgaon':[
				'HR-26 - Gurgaon',
				'DL-15 - Gurgaon',
				'HR-55 - Gurgaon',
				'HR-72 - Gurgaon'
			],
			'Chennai': [
				'TN-09 - Chennai KK Nagar',
				'TN-01 - Chennai Ayanavaram',
				'TN-10 - Chennai Virugambakkam',
				'TN-22 - Meenambakkam Chennai',
				'TN-07 - Chennai Thiruvanmiyur',
				'TN-02 - Chennai Annanagar',
				'TN-11 - Chennai Tambaram',
				'TN-18 - Chennai Red Hills',
				'TN-04 - Chennai Kolathur',
				'TN-06 - Chennai Mandaveli',
				'TN-13 - Chennai Ambattur',
				'TN-12 - Chennai Poonamallee',
				'TN-85 - Chennai Southwest',
				'TN-05 - Chennai NO',
				'TN-03 - Chennai Tondiarpet',
				'TN-14 - Chennai Sholinganallur',
				'TN-01 - Chennai'],
			'Hyderabad': [
				'AP-09 - Hyderabad',
				'AP-11 - Malakpet Hyderabad East',
				'TS-09 - Hyderabad Central',
				'AP-13 - Tolichowki Hyderabad West',
				'AP-12 - Kisanbagh Hyderabad South',
				'TS-14 - Hyderabad',
				'TS-13 - Tolichowki Hyderabad West',
				'TS-11 - Malakpet Hyderabad East'
			],
			'Ahmedabad':['GJ-1 - Ahmedabad'],
			'Kolkata':[
				'WB-02 - Kolkata',
				'WB-06 - Kolkata',
				'WB-01 - Kolkata',
				'WB-04 - Kolkata',
				'WB-08 - Kolkata',
				'WB-10 - Kolkata',
				'WB-09 - Kolkata',
				'WB-03 - Kolkata',
				'WB-07 - Kolkata',
				'WB-05 - Kolkata'
				],
			'Faridabad':[
				'HR-51 - Faridabad',
				'DL-16 - Faridabad',
				'HR-38 - Faridabad'
			],
		},
		
}
