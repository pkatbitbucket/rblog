'use strict'

var React = require('react/addons')
import Transition from 'react-addons-css-transition-group'

var App = require('./components/desktopapp')
var Results = require('./components/desktopresults')
var apiutils = require('./utils/apiutils')

//apiutils.getInitialData();


// Router
var Router = require('react-router')
var DefaultRoute = Router.DefaultRoute
var Route = Router.Route
var RouteHandler = Router.RouteHandler



var AppWrapper = React.createClass({
	render: function(){
    var key = Math.floor((Math.random() * 100) + 1)
		return (
      <Transition transitionName='example'>
			 <RouteHandler key={key} />
      </Transition>
      
		)
	}
})





var routes = (
  <Route name="app" path="/" handler={AppWrapper}>
    <Route name="results" handler={Results}/>
    <DefaultRoute handler={App}/>
  </Route>
);

Router.run(routes, function (Handler) {
  React.render(<Handler/>, document.getElementById('content'));
});


