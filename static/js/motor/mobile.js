'use strict'
import React from 'react';
import ReactDOM from 'react-dom';
import Transition from 'react-addons-css-transition-group';
import TransitionTimeout from './constants/transition-timeout';
import QuoteType from './components/mobile/quotetype';
import QuoteForm from './components/mobile/quoteform';
import QuoteFormMerged from './components/mobile/quoteformmerged';
import Fastlane from './components/mobile/fastlane_form';
import Results from './components/mobile/results';

import APPUtils from './utils/apputils';
import APIUtils from './utils/apiutils';
APIUtils.getInitialData();


// Safari, in Private Browsing Mode, looks like it supports localStorage but all calls to setItem
// throw QuotaExceededError. We're going to detect this and just silently drop any calls to setItem
// to avoid the entire page breaking, without having to do a check at each usage of Storage.
if (typeof localStorage === 'object') {
    try {
        localStorage.setItem('localStorage', 1);
        localStorage.removeItem('localStorage');
    } catch (e) {
        Storage.prototype._setItem = Storage.prototype.setItem;
        Storage.prototype.setItem = function() {};
    }
}

var networkValue = ['search', 'gsp', 'y!search']

class App extends React.Component {
	render() {
		var Child;
		var route = this.props.route
		var mainroute = route.split('/')[1]
		if(mainroute && mainroute.indexOf('?')){
			mainroute = mainroute.split('?')[0]
		}
	    switch (mainroute) {
	      case 'results':
	      		Child = Results
	      		break
	      case 'quote':
	      		Child = QuoteForm
	      		break
	      case 'quoteform':
	      		Child = QuoteFormMerged
	      		break
	      default:
	      		Child = Fastlane
	    }

	    var key = Math.floor((Math.random() * 100) + 1)
	    var transitionName = (key%2 == 0)? 'slide-from-right':'slide-from-left'

		return (
			<Transition transitionName='slide-from-right' {...TransitionTimeout} >
				 <Child key={key} params={route.split('/')} />
      		</Transition>
		);
	}
}

function render () {
	var route = window.location.hash.substr(1)

	var network = APPUtils.getValueFromQueryParam('network')
	if(network && networkValue.indexOf(network.toLowerCase())>-1){
		route = '/quoteform'
	}

	if(route.length > 0){
		window.location.hash = route.charAt(0) !== '/' ? '/'+route : route
	}

	ReactDOM.render(<App route={route} />, document.getElementById('content'))
}

window.addEventListener('hashchange', render)

render()