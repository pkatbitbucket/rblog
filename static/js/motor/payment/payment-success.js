(function(){
    var timeout = null;
    var count = 0;
    function getRSAPolicy(){
        console.log("Calling RSA")
        var url =  '/motor/check-rsa-status/';
        $.post(url,{ 'csrfmiddlewaretoken': window.CSRF_TOKEN }, function(response){
            if(response.status && response.status === 'complete'){
                showRSAPolicyRow();
                updateRSALink(response)
            }
            else{
                initTimeout();
            }
        })
    }

    // Show RSA policy only for fourwheeler
    function initTimeout(){
        console.log("Init Timeout", window.VEHICLE_TYPE)
        if(window.VEHICLE_TYPE === "fourwheeler"){
            var delay = count === 0? 0: 5000;
            timeout = setTimeout(function(){
                if(++count <= 6){
                    getRSAPolicy();
                }
                else{
                    showRSAErrorMessage();
                }
            }, delay);
        }
    }

    function showRSAErrorMessage(){
        showRSAPolicyRow();
        $("#lblRSAError")
            .text("You will receive RSA policy document on the registered email address.")
            .removeClass('hide')
    }

    function showRSAPolicyRow(){
        $("#divRsaPolicy").removeClass('hide');
    }

    function updateRSALink(response){
        $("#RSAPolicyLink")
            .attr("href", response.rsa_doc)
            .removeClass('hide')
    }

    initTimeout();
})()