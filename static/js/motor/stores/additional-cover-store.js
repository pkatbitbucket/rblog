var AppConstants = require('../constants/app-constants.js')
var AppDispatcher = require('../dispatchers/app-dispatcher.js')
var APIutils = require('../utils/apiutils')
import settings from '../constants/configuration'



var apputils = require('../utils/apputils')

var EventEmitter = require('events').EventEmitter
var assign = require('object-assign')

var CHANGE_EVENT = "change"

var _addons = settings.getConfiguration().addons

// pa passenger premium
var _selectedPAP = 0

var toggleAddon = function(addonid, callback){
	var addon = _addons.filter(function(addon){
		return addon.id == addonid
	})[0]

	if(addon){
		if(addon.is_selected){
			addon.is_selected = false	
		}
		else{
			addon.is_selected = true
		}
	}

	if(callback){
		callback()
	}
	
}

var deselectAddons = function(){
	for (var i = _addons.length - 1; i >= 0; i--) {
		_addons[i].is_selected = false
	}
}

var populateFromLocalStorage = function(){
	if(typeof(Storage) !== "undefined") {
		if(localStorage.getItem(VEHICLE_TYPE)){
			var data = JSON.parse(localStorage.getItem(VEHICLE_TYPE))

			for (var i = _addons.length - 1; i >= 0; i--) {
				if(data[_addons[i].id] == '1'){
					_addons[i].is_selected = true
				}
				else{
					_addons[i].is_selected = false	
				}
			};
		}
	}
}

var saveToLocalStorage = function(){
	if(typeof(Storage) !== "undefined") {
		var data = localStorage.getItem(VEHICLE_TYPE);
        if(!data) {
            data = {};
        } else {
            data = JSON.parse(data);
        }

        for (var i = _addons.length - 1; i >= 0; i--) {
        	if(_addons[i].is_selected){
        		data[_addons[i].id] = '1'
        	}
        	else{
        		data[_addons[i].id] = '0'	
        	}
        };

		localStorage.setItem(VEHICLE_TYPE, JSON.stringify(data));
	}
}

var deselectAddon

var additionalCoverStore = assign({}, EventEmitter.prototype, {
	emitChange: function(){
		this.emit(CHANGE_EVENT)
	},

	addChangeListener: function(callback){
		this.on(CHANGE_EVENT, callback)
	},

	removeChangeListener: function(callback) {
    	this.removeListener(CHANGE_EVENT, callback)
 	},

 	getSelectedPAP: function(){
 		return _selectedPAP
 	},

 	getAddons: function(){
 		return _addons
 	},

 	getSelectedAddons: function(){
 		return _addons.filter(function(item){
 			return item.is_selected
 		})
 	},

 	getSelectedAddonIds: function(){
 		return additionalCoverStore.getSelectedAddons().map(function(item){
 			return item.id
 		})
 	},

 	getSelectedAddonNames: function(){
 		return additionalCoverStore.getSelectedAddons().map(function(item){
 			return item.name
 		})
 	},

 	dispatcherIndex: AppDispatcher.register(function(action) {
 		 switch(action.actionType) {
 		 	case AppConstants.TOGGLE_ADDON:
 		 		toggleAddon(action.addonid, action.callback)
 		 		saveToLocalStorage()
 		 		additionalCoverStore.emitChange()
 		 		break

 		 	case AppConstants.SELECT_VEHICLE:
 		 		deselectAddons()
 		 		saveToLocalStorage()
 		 		additionalCoverStore.emitChange()
 		 		break
 		 }
 	})
})


populateFromLocalStorage()

module.exports = additionalCoverStore
