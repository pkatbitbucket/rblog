var AppConstants = require('../constants/app-constants.js')
var AppDispatcher = require('../dispatchers/app-dispatcher.js')
var APIutils = require('../utils/apiutils')
var GTMUtils = require('../utils/gtmutils')
var settings = require('../constants/configuration')
var DateStore = require('./date-store')
var RTOStore = require('./rto-store')
var VehicleStore = require('./vehicle-store')
var PlanStore = require('./plan-store')
var LMSActions = require('../actions/lms-actions')
var Cookie = require('react-cookie')
var moment = require('moment')
var apputils = require('../utils/apputils')
var EventEmitter = require('events').EventEmitter
var assign = require('object-assign')

var CHANGE_EVENT = "change"

var _addons = settings.getConfiguration().addons

/* --- NCB Logic Block--- */

var ncb_options = settings.getConfiguration().ncb_options
var age_wise_previous_ncb = settings.getConfiguration().age_wise_previous_ncb

var _newNCB
var _previousNCB
var _NCBUnknown = false
var _hasUserChangedNCB = false
var _hasClaimedPastYear
var _isNCBCertificate
var _isNewVehicle = 0
var _isUsedVehicle = 0
var _quoteType = 'renew'
var _idv = 0
var _idvElectrical = 0
var _idvNonElectrical = 0
var _vd = 0
var _quoteId
var _timeoutGTMC = null

var init = function(){
	_newNCB = null
	_previousNCB = null
	_NCBUnknown = false
	_hasUserChangedNCB = false
	_hasClaimedPastYear = null
	_isNCBCertificate = null
	_isNewVehicle = 0
	_quoteType = 'renew'
	_idv = 0
	_idvElectrical = 0
	_idvNonElectrical = 0
	_vd = 0
	_quoteId = null
}

init()

var setNCBFromAge = function(){
	var regDate = DateStore.getAllDates().registrationDate
	var vehicleAge = moment().diff(regDate, 'years');

	if (vehicleAge < 7) {
        var ncbValue = age_wise_previous_ncb[vehicleAge];
    }
    else {
        var ncbValue = 50;
    }

    _previousNCB = ncb_options.filter(function(item){
    	return item.value == ncbValue;
    })[0];

}

var setQuoteType = function(quote_type, callback){
	switch(quote_type){
		case 'renew':
			_isNewVehicle = 0
			break
		case 'new':
			_isNewVehicle = 1
			break
		case 'expired':
			_isNewVehicle = 0
			break
	}

	_quoteType = quote_type

	if(callback){
		callback()
	}
}

var getDefaultNCB = function(){
		var ncbValue = 25
		try{
			var regDate = DateStore.getAllDates().registrationDate
			var today = moment()
			var diff = today.diff(regDate, 'years')
			if(6>diff){
				ncbValue = age_wise_previous_ncb[diff]
			}else{
				ncbValue = age_wise_previous_ncb[6]
			}
		}catch(e){}

		var ncb = ncb_options.filter(function(item){
		    	return item.value == ncbValue;
		    })[0];

		return ncb
	}

var setPreviousNCB = function(value){
	if(value){
		_previousNCB = value
		_NCBUnknown = false
	}else{
		setNCBFromAge()
		_NCBUnknown = true
	}

	_hasUserChangedNCB = true
	saveToLocalStorage()
}

var setClaimStatus = function(status, isNewNCB){
	if(isNewNCB){
		_isNCBCertificate = status
	}else{
		_hasClaimedPastYear = status
		if(_hasClaimedPastYear == true){
			_previousNCB = ncb_options.filter(function(item){
				return item.value == 0
			})[0]
		}else{
			var d = appStore.getGTMData()
			GTMUtils.fsClaimMadeClick(d.isExpired, d.model, d.fuel, d.variant, d.rto, d.regYear, d.expiryDate)
		}
	}

	saveToLocalStorage()
}

var saveToLocalStorage = function(){
	if(typeof(Storage) !== "undefined") {

		var data = localStorage.getItem(VEHICLE_TYPE);
        if(!data) {
            data = {};
        } else {
            data = JSON.parse(data);
        }

        if(_isNewVehicle == 0){
        	data.isNewVehicle = 'false'
        }

        if(_isNewVehicle == 1){
        	data.isNewVehicle = 'true'
        }

        data.newNCB = _newNCB != undefined ? _newNCB.value : undefined

        if(_previousNCB){
        	data.previousNCB = _previousNCB.value
        }

        if(_hasClaimedPastYear){
        	data.isClaimedLastYear = _hasClaimedPastYear? 1:0
        }

        if(_isNCBCertificate != undefined){
        	data.isNCBCertificate = _isNCBCertificate ? '1' : '0'
        }

        if(_idv || _idv == 0){
        	data.idv = _idv
        }

        data.ncb_unknown = _NCBUnknown
        data.quoteType = _quoteType

		localStorage.setItem(VEHICLE_TYPE, JSON.stringify(data));
	}
}


var populateFromLocalStorage = function(){
	if(typeof(Storage) !== "undefined") {
		if(localStorage.getItem(VEHICLE_TYPE)){
			var data = JSON.parse(localStorage.getItem(VEHICLE_TYPE))

			if(data.isNewVehicle == 'false' || data.isNewVehicle == false){
				_isNewVehicle = 0
			}
			if(data.isNewVehicle == 'true' || data.isNewVehicle == true){
				_isNewVehicle = 1
			}
			if(data.isUsedVehicle != undefined){
				_isUsedVehicle = data.isUsedVehicle
			}
			if(data.previousNCB || data.previousNCB == 0){
				_previousNCB = ncb_options.filter(function(item){
		 				return item.value == data.previousNCB
		 			})[0]
			}
			if(data.newNCB || data.newNCB == 0){
				_newNCB = ncb_options.filter(function(item){
		 				return item.value == data.newNCB
		 			})[0]
			}

			if(data.ncb_unknown !== undefined)
				_NCBUnknown = data.ncb_unknown

			if(data.isClaimedLastYear){
				_hasClaimedPastYear = (data.isClaimedLastYear == '1')? true:false
				if(_hasClaimedPastYear == true){
 		 			_previousNCB = ncb_options.filter(function(item){
 		 				return item.value == 0
 		 			})[0]
 		 		}
			}

			if(data.isNCBCertificate){
				_isNCBCertificate = data.isNCBCertificate == '1' ? true : false
			}

			if(data.idv || data.idv == 0){
				_idv = parseInt(data.idv)
			}

			if(data.quoteType){
				_quoteType = data.quoteType
			}
		}
	}
}

populateFromLocalStorage()

var clearLocalStorage = function(isNewVehicle){
	init()
	if(isNewVehicle){
		_isNewVehicle = 1
		_quoteType = 'new'
	}else{
		_isNewVehicle = 0
		_quoteType = 'renew'
	}
	saveToLocalStorage()
};

(function(){
	if(DateStore.getSelectedRegYear() && !DateStore.isExpiredBeforeNintyDays() && _previousNCB == undefined && _isUsedVehicle != 1){
		setNCBFromAge()
		_NCBUnknown = true
		saveToLocalStorage()
	}
}())

var handleGTM = function(status){
	var d = appStore.getGTMData()
	if(status==='claimed'){
		GTMUtils.fvq_claimed(d.isExpired, d.model, d.fueltype, d.variant, d.rto, d.regYear, d.expiryDate)
	}else if(status==='unclaimed'){
		GTMUtils.fvq_unclaimed(d.isExpired, d.ncb, d.model, d.fueltype, d.variant, d.rto, d.regYear, d.expiryDate)
	}else if(status==='expired'){
		GTMUtils.fvq_expired(d.fastlaneDataChanged, d.model, d.fueltype, d.variant, d.rto, d.regYear)
	}
}

var stampGTMCookies = function(notFinishedYet){
	var data = appStore.getGTMData()

	var pid = data.model +"_"+ data.fueltype +"_"+ data.variant +"_"+ data.rto +"_"+ data.regYear

    if(DateStore.getUpdateStatus()){
    	var dates = DateStore.getAllDates()
    	pid +=  '_' + dates.registrationDate.date() + '_' + dates.pastPolicyExpiryDate.format('DD-MMMM-YYYY') + '_' + data.ncb
    }

    var plan = PlanStore.getPlans() ? PlanStore.getPlans()[0] : null
    var premium = plan ? plan.totalPremium : ''
    var insurer = plan ? plan.insurerShortName : ''
	var insurerimageurl = STATIC_URL + '/img/motor/insurers/' + insurer + '.png'
	var carimageurl = STATIC_URL + '/img/car-outline.png'
	var options = { maxAge: 86400, path: '/', domain: '.coverfox.com' }
	Cookie.save('pid', pid, options)
	Cookie.save('premium', premium, options)
	Cookie.save('insurer', insurer, options)
	Cookie.save('insurer_logo', insurerimageurl, options)
	Cookie.save('qlp', window.location.href, options)
	Cookie.save('carimage', carimageurl, options)
    GTMUtils.gtm_push(notFinishedYet?'vizurycookie30secquoteload':'vizurycookiequoteload', '', '', '', {})
}

var appStore = assign({}, EventEmitter.prototype, {
	emitChange: function(){
		this.emit(CHANGE_EVENT)
	},

	addChangeListener: function(callback){
		this.on(CHANGE_EVENT, callback)
	},

	removeChangeListener: function(callback) {
    	this.removeListener(CHANGE_EVENT, callback)
 	},

 	hideDateSelector: function(){
 		if(VehicleStore.hasFastlaneDataChanged())
 			return false

 		//return DateStore.isExpiredBeforeNintyDays() || (!_NCBUnknown && DateStore.isExpiredUnderNintyDays())
 		return VehicleStore.getFastlaneStatus() && (!_NCBUnknown && DateStore.isExpiredUnderNintyDays())
 	},

 	getData: function(){
 		return {
 			newNCB: _newNCB,
 			previousNCB: _previousNCB,
 			hasClaimedPastYear: _hasClaimedPastYear,
 			isNCBCertificate: _isNCBCertificate,
 			isNewVehicle: _isNewVehicle,
 			isUsedVehicle: _isUsedVehicle,
 			idv: _idv,
 			quoteId: _quoteId,
 			quoteType: _quoteType,
 			NCBUnknown: _NCBUnknown
 		}
 	},

 	getGTMData : function(){
		var data = {}
		var expiryDate = DateStore.getAllDates().pastPolicyExpiryDate
		try{
			data.model = VehicleStore.getSelectedVehicle().name
			data.fueltype = VehicleStore.getSelectedFuelType()
			data.variant = VehicleStore.getSelectedVariant().name
			data.rto = RTOStore.getSelectedRTO().name
			data.regYear = DateStore.getSelectedRegYear()
			data.isExpired = DateStore.isPolicyExpired()
			data.ncb = _NCBUnknown ? undefined : (_previousNCB ? _previousNCB.value : _previousNCB)
			data.fastlaneDataChanged = VehicleStore.fastlaneDataEdited()
			data.expiryDate = expiryDate ? expiryDate.format('DD-MM-YYYY') : undefined
		}catch(e){
			console.log("GTM: Something went wrong!")
		}
		return data
	},

 	getQuoteRequest: function(){
 		var variant = VehicleStore.getSelectedVariant()
 		var rto = RTOStore.getSelectedRTO()
 		var quote = {
 		  "csrfmiddlewaretoken": CSRF_TOKEN,
		  "extra_paPassenger": 0,
		  "cngKitValue": parseInt(VehicleStore.getCNGKitValue()),
		  "isNewVehicle": _isNewVehicle,
		  "quoteId": "",
		  "idvNonElectrical": _idvNonElectrical,
		  "isClaimedLastYear": _hasClaimedPastYear? 1:0,
		  "registrationNumber": rto ? rto.id.split('-').concat(['0','0']) : ['0', '0', '0', '0'],
		  "idvElectrical": _idvElectrical,
		  "voluntaryDeductible": _vd,
		  "vehicleId": variant ? variant.id : null,
		  "idv": _idv,
		  "extra_isLegalLiability": 0,
		  "previousNCB": _isNewVehicle == 0 && _previousNCB ? _previousNCB.value : 0,
		  "newNCB": _isUsedVehicle == 1 && _newNCB ? _newNCB.value : 0,
		  "extra_isAntiTheftFitted": 0,
		  "extra_isMemberOfAutoAssociation": 0,
		  "ncb_unknown": _NCBUnknown,
		  "isUsedVehicle": _isUsedVehicle,
		  "isExpired": _isUsedVehicle == 1 ? true : DateStore.isPolicyExpired(),
		  "rds_id": VehicleStore.getRdsId()
		}

		quote["isCNGFitted"] = VehicleStore.getCNGFittedStatus()

		if(DateStore.getAllDates().registrationDate){
			quote["registrationDate"] = DateStore.getAllDates().registrationDate.format('DD-MM-YYYY')
		}

		if(_isNewVehicle == 0){
			quote["manufacturingDate"] = quote["registrationDate"]
		}

		if(_isUsedVehicle == 1)
			quote["isNCBCertificate"] = _isNCBCertificate ? 1 : 0;

		if(_isNewVehicle == 1){
			if(DateStore.getAllDates().manufacturingDate){
				quote["manufacturingDate"] = DateStore.getAllDates().manufacturingDate.format('DD-MM-YYYY')
			}
		}

		if(DateStore.getAllDates().pastPolicyExpiryDate){
			quote["pastPolicyExpiryDate"] = DateStore.getAllDates().pastPolicyExpiryDate.format('DD-MM-YYYY')
		}

		if(DateStore.getAllDates().newPolicyStartDate){
			quote["newPolicyStartDate"] = DateStore.getAllDates().newPolicyStartDate.format('DD-MM-YYYY')
		}

		for (var i = _addons.length - 1; i >= 0; i--) {
			quote[_addons[i].id] = (_addons[i].is_selected? 1:0)
		};

		if(_quoteId){
			quote['quoteId'] = _quoteId
		}

		quote = assign(quote, VehicleStore.getQuoteRequestData(), RTOStore.getQuoteRequestData())

		return quote

 	},

 	dispatcherIndex: AppDispatcher.register(function(action) {
 		 switch(action.actionType) {
 		 	case AppConstants.VIEW_QUOTES:
 		 		handleGTM(action.status)
 		 		window.location.hash = '/results'
 		 		break

 		 	case AppConstants.CLEAR_SELECTED_DATA:
 		 		AppDispatcher.waitFor([DateStore.dispatcherIndex, RTOStore.dispatcherIndex])
 		 		clearLocalStorage(action.isNewVehicle)
 		 		appStore.emitChange()
 		 		if(action.callback)
 		 			action.callback()
 		 		break

 		 	case AppConstants.SET_QUOTE_TYPE:
 		 		setQuoteType(action.quote_type, action.callback)
 		 		saveToLocalStorage()
 		 		appStore.emitChange()
 		 		break

 		 	case AppConstants.SET_EXPIRY:
 		 		setQuoteType(action.quote_type, action.callback)
 		 		saveToLocalStorage()
 		 		appStore.emitChange()
 		 		break

 		 	case AppConstants.SELECT_NCB:
 		 		if(action.isNewNCB){
 		 			_newNCB = action.ncb
 		 			saveToLocalStorage()
 		 		}else{
 		 			setPreviousNCB(action.ncb)
 		 		}
 		 		appStore.emitChange()
 		 		break

 		 	case AppConstants.SET_CLAIM_STATUS:
 		 		setClaimStatus(action.status, action.isNewNCB)
 		 		appStore.emitChange()
 		 		break

 		 	case AppConstants.SELECT_REGISTRATION_YEAR:
 		 		AppDispatcher.waitFor([DateStore.dispatcherIndex])
				if(!_hasUserChangedNCB){
 		 			setNCBFromAge()
 		 		}
 		 		saveToLocalStorage()
				appStore.emitChange()
				break

			case AppConstants.START_FETCHING_PLANS:
				if(_timeoutGTMC)
					clearTimeout(_timeoutGTMC)
				break

			case AppConstants.RECIEVE_PLANS:
 		 		AppDispatcher.waitFor([PlanStore.dispatcherIndex])
				_quoteId = action.quoteId
				if(Cookie.load('mobileNo')){
					LMSActions.sendToLMS(appStore.getQuoteRequest(), 'results-page', {mobile: Cookie.load('mobileNo')})
				}
				if(!PlanStore.getFetchingPlansStatus()){
					if(_timeoutGTMC)
						clearTimeout(_timeoutGTMC)
					stampGTMCookies()
				}else if(!_timeoutGTMC){
					_timeoutGTMC = setTimeout(stampGTMCookies.bind(null, true), 30000)
				}
				appStore.emitChange()
				break

			case AppConstants.SET_IDV:
 		 		_idv = action.idv
 		 		if(action.callback){
 		 			action.callback()
 		 		}
 		 		saveToLocalStorage()
				appStore.emitChange()
				break

			case AppConstants.SET_DATE:
				if(action.fastlane && 'pyp'===action.type){
					if(moment().diff(action.value, 'days')>=90){
						_previousNCB = age_wise_previous_ncb[0]
						_hasClaimedPastYear = true
					}else{
						_hasClaimedPastYear = null
						_previousNCB = null

					}
					saveToLocalStorage()
				}
				break

			case AppConstants.SEND_FASTLANE_FEEDBACK:
				var d = appStore.getGTMData()
				var mmv = d.model + " " + d.variant
 		 		GTMUtils.fsConfirmData(d.fastlaneDataChanged, d.isExpired, mmv,
 		 			d.model, d.fuel, d.variant, d.rto, d.regYear, d.expiryDate)
			 	break
 		 }
 	})
})


module.exports = appStore
