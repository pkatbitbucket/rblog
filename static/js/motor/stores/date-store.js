var AppConstants = require('../constants/app-constants.js')
var AppDispatcher = require('../dispatchers/app-dispatcher.js')
var moment = require('moment')

var EventEmitter = require('events').EventEmitter
var assign = require('object-assign')

var dateUtils = require('../utils/dateutils')
var CHANGE_EVENT = "change"

var _registrationDate,
	_manufacturingDate,
	_pastPolicyExpiryDate,
	_newPolicyStartDate,
	_selectedRegYear,
	_hasUpdated,
	_policyExpired,
	_userUpdatedDates = []

var init = function(){
	_registrationDate = null
	_manufacturingDate = null
	_pastPolicyExpiryDate = null
	_newPolicyStartDate = null
	_selectedRegYear = null
	_hasUpdated = null
	_policyExpired = false
	_userUpdatedDates = []
}

init()

var setDefaultRegDate = function(){
		if(_selectedRegYear){
			_registrationDate = moment()
			_registrationDate.date(1).year(_selectedRegYear)
		}else{
			_registrationDate = null
		}
		_manufacturingDate = _registrationDate
		setPYP(moment().add(7, 'days'))
		_policyExpired = false
	};


var setDefaultNewVehicleDates= function(){
		_registrationDate = moment()
		_newPolicyStartDate = moment().add(1, 'days')
		_manufacturingDate = moment().subtract(2,'months')
		_manufacturingDate = _manufacturingDate.startOf('month')
		_pastPolicyExpiryDate = undefined
		_policyExpired = false
	};

var setDefaultExpiredVehicleDates= function(){
		if(_selectedRegYear){
			_registrationDate = moment()
			_registrationDate.date(1).year(_selectedRegYear)
		}
		_manufacturingDate = _registrationDate
		if(!_pastPolicyExpiryDate || _pastPolicyExpiryDate.startOf('day').isAfter(moment().startOf('day')) ){
			setPYP(moment().subtract(1, 'days'))
		}
		_policyExpired = true
	};



var resetDates = function(quote_type){
	switch(quote_type){
		case 'renew':
			setDefaultRegDate()
			break
		case 'new':
			setDefaultNewVehicleDates()
			break
		case 'expired':
			setDefaultExpiredVehicleDates()
			break
	}
}

var setRegistryDate = function(data){
	resetDates('renew')
	var regDate = data.fastlane.response.result.vehicle['regn_dt'].split('/')
	_registrationDate = moment()
	_registrationDate.date(regDate[0]).month(parseInt(regDate[1])-1).year(regDate[2])
	_manufacturingDate = _registrationDate
	saveToLocalStorage()
}

var setPYP = function(value){
	var today = moment().startOf('day');
	_pastPolicyExpiryDate = value;
	_newPolicyStartDate = _pastPolicyExpiryDate.isAfter(today) ? _pastPolicyExpiryDate.clone().add(1, 'day') : today.add(1, 'day');
}

var saveToLocalStorage = function(){
	if(typeof(Storage) !== "undefined") {

		var data = localStorage.getItem(VEHICLE_TYPE);
        if(!data) {
            data = {};
        } else {
            data = JSON.parse(data);
        }

        if(_registrationDate){
        	var d = _registrationDate.format('DD-MMMM-YYYY')
        	data.reg_date = d.split('-')[0]
        	data.reg_month = d.split('-')[1]
        	data.reg_year = d.split('-')[2]
        }else{
        	data.reg_date = undefined
        	data.reg_month = undefined
        	data.reg_year = undefined
        }

        if(_manufacturingDate){
        	var d = _manufacturingDate.format('DD-MMMM-YYYY')
        	data.man_date = d.split('-')[0]
        	data.man_month = d.split('-')[1]
        	data.man_year = d.split('-')[2]
        }else{
        	data.man_date = undefined
        	data.man_month = undefined
        	data.man_year = undefined
        }

        if(_pastPolicyExpiryDate){
        	var d = _pastPolicyExpiryDate.format('DD-MMMM-YYYY')
        	data.expiry_date = d.split('-')[0]
        	data.expiry_month = d.split('-')[1]
        	data.expiry_year = d.split('-')[2]
        }else{
        	data.expiry_date = undefined
        	data.expiry_month = undefined
        	data.expiry_year = undefined
        }

        if(_newPolicyStartDate){
        	var d = _newPolicyStartDate.format('DD-MMMM-YYYY')
        	data.policy_start_date = d.split('-')[0]
        	data.policy_start_month = d.split('-')[1]
        	data.policy_start_year = d.split('-')[2]
        }else{
        	data.policy_start_date = undefined
        	data.policy_start_month = undefined
        	data.policy_start_year = undefined
        }

        if(_policyExpired!==undefined && _policyExpired!==null)
        	data.policyExpired = _policyExpired

		localStorage.setItem(VEHICLE_TYPE, JSON.stringify(data));
	}
}


var populateFromLocalStorage = function(){
	if(typeof(Storage) !== "undefined") {
		if(localStorage.getItem(VEHICLE_TYPE)){
			var data = JSON.parse(localStorage.getItem(VEHICLE_TYPE))

			if(data.reg_date && data.reg_month && data.reg_year){
				var date = data.reg_date+'-'+data.reg_month+'-'+data.reg_year
				_registrationDate = moment(date, 'DD-MMMM-YYYY')
			}

			if(data.expiry_date && data.expiry_month && data.expiry_year){
				var date = data.expiry_date+'-'+data.expiry_month+'-'+data.expiry_year
				setPYP(moment(date, 'DD-MMMM-YYYY'))
			}

			if(data.man_month && data.man_year){
				var man_date = data.man_date
				if(!data.man_date){
					man_date = '01'
				}
				var date = man_date+'-'+data.man_month+'-'+data.man_year
				_manufacturingDate = moment(date, 'DD-MMMM-YYYY')
				if(!_registrationDate){
					_registrationDate = _manufacturingDate.subtract(1, 'days')
				}
			}

			if(data.policy_start_date && data.policy_start_month && data.policy_start_year){
				var date = data.policy_start_date+'-'+data.policy_start_month+'-'+data.policy_start_year
				_newPolicyStartDate = moment(date, 'DD-MMMM-YYYY')
			}

			if(data.policyExpired!==undefined)
				_policyExpired = data.policyExpired
		}
	}
}

var clearLocalStorage = function(isNewVehicle){
	if(typeof(Storage) !== "undefined") {
		localStorage.removeItem(VEHICLE_TYPE);
	}
	init()
	resetDates(isNewVehicle?'new':'renew')
	saveToLocalStorage()
}

populateFromLocalStorage()

var dateStore = assign({}, EventEmitter.prototype, {

	emitChange: function(){
		this.emit(CHANGE_EVENT)
	},

	addChangeListener: function(callback){
		this.on(CHANGE_EVENT, callback)
	},

	removeChangeListener: function(callback) {
		this.removeListener(CHANGE_EVENT, callback)
	},

	getRegYears: function(){
		return dateUtils.reg_years()
	},

	getSelectedRegYear: function(){
		if(_registrationDate){
			return _registrationDate.get('year')
		}else{
			return null
		}
	},

	getAllDates: function(){
		return {
			registrationDate:_registrationDate,
			manufacturingDate:_manufacturingDate,
			pastPolicyExpiryDate:_pastPolicyExpiryDate,
			newPolicyStartDate:_newPolicyStartDate
		}
	},

	getUpdateStatus: function(){
		return _hasUpdated
	},

	isPolicyExpired: function(){
		return _policyExpired
	},

	isExpiredBeforeNintyDays: function(){
		return _policyExpired && _pastPolicyExpiryDate && !moment().startOf('day').add(-90, 'day').isBefore(_pastPolicyExpiryDate)
	},

	isExpiredUnderNintyDays: function(){
		return _policyExpired && _pastPolicyExpiryDate && moment().startOf('day').add(-90, 'day').isBefore(_pastPolicyExpiryDate)
	},

	dispatcherIndex: AppDispatcher.register(function(action) {
		switch(action.actionType) {
			case AppConstants.CLEAR_SELECTED_DATA:
 		 		clearLocalStorage(action.isNewVehicle)
 		 		break

			case AppConstants.SELECT_REGISTRATION_YEAR:
				_selectedRegYear = action.year

				if(action.quote_type == "renew"){
					setDefaultRegDate()
				}

				if(action.quote_type == "expired"){
					setDefaultExpiredVehicleDates()
				}

				saveToLocalStorage()
				dateStore.emitChange()
				break

			case AppConstants.SET_EXPIRY:
				if(action.quote_type === 'renew'){
					_policyExpired = false
					setPYP(moment().add(7, 'days'))
				}else if(action.quote_type === 'expired'){
					_policyExpired = true
					setPYP(moment().subtract(1, 'days'))
				}
				saveToLocalStorage()
				break

			case AppConstants.RECIEVE_VEHICLE_INFO:
 		 		setRegistryDate(action.data)
				dateStore.emitChange()
 		 		break

			case AppConstants.SET_QUOTE_TYPE:
				resetDates(action.quote_type)
				_hasUpdated = false
				saveToLocalStorage()
				dateStore.emitChange()
				if(action.callback)
					action.callback()
				break

			case AppConstants.SET_DATE:
				var type = action.type
				var value = action.value
				var callback = action.callback
				switch(type){
					case 'nps':
						_newPolicyStartDate = value
						break
					case 'man':
						_manufacturingDate = value
						break
					case 'reg':
						_registrationDate = value
						break
					case 'pyp':
						setPYP(value)
						break
				}

				if(['nps', 'man', 'reg', 'pyp'].indexOf(type) > -1 && _userUpdatedDates.indexOf(type) == -1){
					_userUpdatedDates.push(type)
				}

				if(callback){
					callback()
				}

				_hasUpdated = true
				saveToLocalStorage()
				dateStore.emitChange()
				break

		}
	})
})


module.exports = dateStore
