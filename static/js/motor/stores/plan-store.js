var AppConstants = require('../constants/app-constants.js')
var AppDispatcher = require('../dispatchers/app-dispatcher.js')
var APIutils = require('../utils/apiutils')

var AddonStore = require('./additional-cover-store')

var apputils = require('../utils/apputils')

var EventEmitter = require('events').EventEmitter
var assign = require('object-assign')

var allInsurers = {
	'bajaj-allianz':'Bajaj Allianz',
	'bharti-axa': 'Bharti Axa',
	'future-generali': 'Future Generali',
	'hdfc-ergo': 'HDFC ERGO',
	'iffco-tokio': 'IFFCO TOKIO',
	'l-t': 'L & T',
	'liberty-videocon':'Liberty Videocon',
	'new-india':'New India',
	'oriental': 'Oriental',
	'reliance':'Reliance',
	'tata-aig':'TATA AIG',
	'universal-sompo':'Universal Sompo'
}

var CHANGE_EVENT = "change"

var SERVICE_TAX_RATE = 0.14

var _plans = []
var _sortedOnce = false
var _fetchingPlans = false

var rogueInsurers = function(){

    var ri = []

    var insurersFromPlans = _plans.map(function(plan){
        return plan.insurerSlug
    })

    for(var key in allInsurers){
        if(insurersFromPlans.indexOf(key) == -1){
            ri.push(allInsurers[key])
        }
    }

    return ri
}



var selectPlan = function(plan, callback){
	_plans = _plans.map(function(item){
		if(plan.index == item.index){
			item.is_selected = true
		}
		else{
			item.is_selected = false
		}

		return item
	})

    if(callback){
        callback()
    }
}

var deselectPlan = function(){
	planStore.getSelectedPlan().is_selected = false
}


var calculatePAP = function(plan){
	var pap = 0, st = 0
	if (plan.paPassengerAmounts) {
            pap = plan.premiumBreakup.paPassengerAmounts[AddonStore.getSelectedPAP()];
        }
    st = pap * plan.serviceTaxRate;
    return Math.round(pap + st);
}

var calculateAddonCovers = function(plan){
	return get_premium_from_covers(plan.premiumBreakup.addonCovers,plan.serviceTaxRate)
}

var calculateBasicCovers = function(plan){
	return get_premium_from_covers(plan.premiumBreakup.basicCovers,plan.serviceTaxRate)
}

var calculateDiscounts = function(plan){
	return get_premium_from_covers(plan.premiumBreakup.discounts,plan.serviceTaxRate)
}

var calculateSpecialDiscounts = function(plan){
	return get_premium_from_covers(plan.premiumBreakup.specialDiscounts,plan.serviceTaxRate)
}

var get_premium_from_covers = function(covers, serviceTaxRate) {
    var c = 0,
        st = 0;
    for (var i = 0; i < covers.length; i++) {
        if (covers[i].is_selected) {
            c = c + parseFloat(covers[i].premium);
            st = st + parseFloat(covers[i].premium*serviceTaxRate);
        }
    }
    return Math.round(c + st);
};

var setPlanDefaults = function(plan){



	plan.premiumBreakup.basicCovers = plan.premiumBreakup.basicCovers.map(function(item){
		if(item.default == 1){
			item.is_selected = true
		}
		return item
	})

	plan.premiumBreakup.discounts = plan.premiumBreakup.discounts.map(function(item){
		if(item.default == 1){
			item.is_selected = true
		}
		return item
	})

	plan.premiumBreakup.specialDiscounts = plan.premiumBreakup.specialDiscounts.map(function(item){
		if(item.default == 1){
			item.is_selected = true
		}
		return item
	})
}


var sortPlans = function(criteria) {

    var addon_weight = 100,
    idv_weight = 1,
    premium_weight = 10;
    if(criteria == 'Premium'){
        addon_weight = 0;
        idv_weight = 0;
    }

    if(criteria == 'IDV'){
        addon_weight = 0;
        premium_weight = 0;
    }

    var maximumPremium = 0;
    var maximumIdv = 0;

    _plans = _plans.map(function(plan){
    	if (maximumPremium < parseFloat(plan.totalPremium)) {
            maximumPremium = parseFloat(plan.totalPremium);
        }
        if (maximumIdv < parseFloat(plan.calculatedAtIDV)) {
            maximumIdv = parseFloat(plan.calculatedAtIDV);
        }

        var addons_for_plan = plan.premiumBreakup.addonCovers.filter(function(item){
            return item.is_selected
        })

        // var addons_for_plan = plan.addonCovers().map(function(plan) {
        //     return plan.id;
        // });
        var selected_addons_available = addons_for_plan.length;

        // ko.utils.arrayForEach(selected_addons(), function(addon) {
        //     if (addons_for_plan.indexOf(addon) > -1) {
        //         selected_addons_available += 1
        //     }
        // });
        plan.selectedAddonsAvailable = selected_addons_available;

        return plan;

    })

    _plans = _plans.map(function(plan){
    	var cp = parseFloat(plan.totalPremium);
        var pr = (maximumPremium / cp);
        var ar = plan.selectedAddonsAvailable;
        var idvr = (plan.calculatedAtIDV / maximumIdv);
        var rank = addon_weight * ar + idv_weight * idvr + premium_weight * pr;

        plan.rank = rank;

        return plan
    })

    

    _plans.sort(function(left, right) {
        return left.rank == right.rank ? 0 : (left.rank > right.rank ? -1 : 1);
    });

};

var setupPlans = function(plans){
	plans = plans.map(function(plan, index){

		plan.insurerShortName = allInsurers[plan.insurerSlug]

		var serviceTaxRate = plan.serviceTaxRate;
        if (!serviceTaxRate) {
            plan.serviceTaxRate = SERVICE_TAX_RATE;
        }

        setPlanDefaults(plan)

        addSelectedAddonsToPlan(AddonStore.getSelectedAddonIds(),plan)

		plan.addonCoversAmount = calculateAddonCovers(plan)
		plan.specialDiscountAmount = calculateSpecialDiscounts(plan)
		plan.discountAmount = calculateDiscounts(plan)
		plan.basicCoversAmount = calculateBasicCovers(plan)
		plan.papAmount = calculatePAP(plan)




		plan.totalPremium = plan.addonCoversAmount + plan.specialDiscountAmount + plan.discountAmount + plan.basicCoversAmount + plan.papAmount
        plan.totalServiceTax = Math.round((plan.totalPremium/(1 + plan.serviceTaxRate))*(plan.serviceTaxRate))

		return plan

	})

    return plans
}


var addSelectedAddonsToPlan = function(selectedAddons, plan){

	selectedAddons = selectedAddons.map(function(item){
		return item.replace('addon_','')
	})

	var planAddons = plan.premiumBreakup.addonCovers;
    for (var j = 0; j < planAddons.length; j++) {
        if (selectedAddons.indexOf(planAddons[j].id) > -1) {
            planAddons[j].is_selected = true
        } else {
            planAddons[j].is_selected = false
        }
    }

    for (var j = 0; j < planAddons.length; j++) {
        if (planAddons[j].is_selected == false) continue;
        if (plan.premiumBreakup.dependentCovers && plan.premiumBreakup.dependentCovers[planAddons[j].id]) {
            var dependentCovers = plan.premiumBreakup.dependentCovers[planAddons[j].id];
            for (var i = 0; i < dependentCovers.length; i++) {
                for (var m = 0; m < planAddons.length; m++) {
                    if (planAddons[m].id == dependentCovers[i]) {
                        planAddons[m].is_selected = planAddons[j].is_selected;
                    }
                }
            }
        }
    }

    return plan;
}






var planStore = assign({}, EventEmitter.prototype, {
	emitChange: function(){
		this.emit(CHANGE_EVENT)
	},

	addChangeListener: function(callback){
		this.on(CHANGE_EVENT, callback)
	},

	removeChangeListener: function(callback) {
    	this.removeListener(CHANGE_EVENT, callback)
 	},

 	getPlans: function(){
 		return _plans
 	},

    getMaxIDV: function(){
        if(_plans.length > 0){
            return parseInt((Math.max.apply(Math, _plans.map(function(item) {
                return parseInt(item.maxIDV);
            }))) / 1000) * 1000;
        }
        else{
            return 0;
        }
    },

    getMinIDV: function(){
        if (_plans.length > 0) {
            return parseInt((Math.min.apply(Math, _plans.map(function(item) {
                return parseInt(item.minIDV);
            })) + 1000) / 1000) * 1000;
        } else {
            return 0;
        }
    },

 	getSelectedPlan: function(){
 		return _plans.filter(function(plan){
 			return plan.is_selected
 		})[0]
 	},

    getInitialSortStatus: function(){
        return _sortedOnce
    },

    getFetchingPlansStatus: function(){
        return _fetchingPlans
    },

    getRogueInsurers: function(){
        return rogueInsurers()
    },

 	dispatcherIndex: AppDispatcher.register(function(action) {
 		 switch(action.actionType) {

 		 	case AppConstants.RECIEVE_PLANS:
                var newPlans = action.plans
                var fetch_counter = action.fetch_counter
                var finished = action.finished

                newPlans = setupPlans(newPlans)

                for (var i = 0; i < newPlans.length; i++) {
                    _plans.push(newPlans[i])
                };

                if(_plans.length >= 8 || fetch_counter == 2 || finished){
                   if(!_sortedOnce){
                        sortPlans()
                       _sortedOnce = true 
                   }
                }

                _plans = _plans.map(function(plan, index){
                    plan.index = index
                    return plan
                })

                if(finished || fetch_counter == 8){
                    _fetchingPlans = false
                }
 		 		
 		 		planStore.emitChange()
                
 		 		break

            case AppConstants.START_FETCHING_PLANS:
                _fetchingPlans = true
                _plans = []
                _sortedOnce = false
                planStore.emitChange()

            case AppConstants.SORT_PLANS:
                sortPlans(action.criteria)
                planStore.emitChange()
                break

 		 	case AppConstants.SELECT_PLAN:
 		 		selectPlan(action.plan, action.callback)
 		 		planStore.emitChange()
 		 		break

 		 	case AppConstants.DESELECT_PLAN:
 		 		deselectPlan()
 		 		planStore.emitChange()
 		 		break

 		 	case AppConstants.REMOVE_ALL_PLANS:
 		 		_plans = []
 		 		planStore.emitChange()
 		 		break

 		 	case AppConstants.TOGGLE_ADDON:
 		 		AppDispatcher.waitFor([AddonStore.dispatcherIndex])
 		 		_plans = setupPlans(_plans)
 		 		planStore.emitChange()
 		 		break

 		 }
 	})
})


module.exports = planStore
