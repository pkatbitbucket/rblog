var AppConstants = require('../constants/app-constants.js')
var AppDispatcher = require('../dispatchers/app-dispatcher.js')
var APIutils = require('../utils/apiutils')
var Popular = require('../constants/popular.js')
var DateStore = require('./date-store')
var apputils = require('../utils/apputils')

var EventEmitter = require('events').EventEmitter
var assign = require('object-assign')

var CHANGE_EVENT = "change"

var _rtos = []
var _regNo
var _popularRTOs = {}

var init = function(){
	_regNo = []

	for(var index=0; index<_rtos.length; index++){
		if(_rtos[index].is_selected)
			_rtos[index].is_selected = false
	}
}

init()

var selectRTO = function(rto){
	var s = _rtos.filter(function(item){
		return item.is_selected
	})[0] 

	if(s){
		s.is_selected = false	
	}

	var v = _rtos.filter(function(item){
		return item.id == rto.id
	})

	if(v.length > 0){
		v[0].is_selected = true
	}
}

var selectRTOfromID = function(id){
	var rto = _rtos.filter(function(item){
		return item.id == id
	})[0] 

	if(rto){
		selectRTO(rto)
	}
}

var selectRTOByRegistrationNumber = function(regNo){
	var selectedRTO

	if(regNo[1] && isNaN(regNo[1]))
		regNo[1] = regNo[1].substr(0,1)

	var id = regNo[0]+'-'+regNo[1]
	for(var index=0; index<_rtos.length; index++){
		var item = _rtos[index]
		if(item.is_selected){
			item.is_selected = false
		}
		if(item.id == id){
			item.is_selected = true
			selectedRTO = item
		}
	}

	return selectedRTO
}

var setupPopularRTOs = function(){
	_popularRTOs = Popular.rtos
}

var setRegistrationNumber = function(regNo){
	_regNo = regNo.slice()
	selectRTOByRegistrationNumber(_regNo.slice())
}

// var setupRTOS = function(){
// 	if(_rtos.length){
// 		for (var i = _rtos.length - 1; i >= 0; i--) {
// 			if(_rtos[i].id == 'MH-01' || _rtos[i].id == 'MH-02' || _rtos[i].id == 'MH-43' || _rtos[i].id == 'MH-03' || _rtos[i].id == 'MH-04' || _rtos[i].id == 'MH-05' ){
// 				_rtos[i].category = 'Mumbai'
// 				_popularRTOs.push(_rtos[i])
// 			}			
// 			if(_rtos[i].id == 'DL-1' || _rtos[i].id == 'DL-10' || _rtos[i].id == 'DL-11' || _rtos[i].id == 'DL-12' || _rtos[i].id == 'DL-13' ){
// 				_rtos[i].category = 'Delhi'
// 				_popularRTOs.push(_rtos[i])
// 			}
// 		};
// 	}
// }

var populateFromLocalStorage = function(callback){
	if(typeof(Storage) !== "undefined") {
		if(localStorage.getItem(VEHICLE_TYPE)){
			var data = JSON.parse(localStorage.getItem(VEHICLE_TYPE))
			var rto = data.rto_info
			
			if(data.reg_number){
				_regNo = data.reg_number
			}

			if(rto){
				selectRTO(rto)
				callback()
			}
			else{
				callback()
			}
		}
		else{
			callback()
		}
	}
	else{
		callback()	
	}
}	


var saveToLocalStorage = function(){
	if(typeof(Storage) !== "undefined") {
		var data = localStorage.getItem(VEHICLE_TYPE);
        if(!data) {
            data = {};
        } else {
            data = JSON.parse(data);
        }
        if(_regNo.length===4){
        	data.reg_number = _regNo
        }
        if(rtoStore.getSelectedRTO()){
        	data.rto_info = rtoStore.getSelectedRTO()
        }
		localStorage.setItem(VEHICLE_TYPE, JSON.stringify(data));
	}
}

populateFromLocalStorage(function(){})

var rtoStore = assign({}, EventEmitter.prototype, {

	emitChange: function(){
		this.emit(CHANGE_EVENT)
	},

	addChangeListener: function(callback){
		this.on(CHANGE_EVENT, callback)
	},

	removeChangeListener: function(callback) {
    	this.removeListener(CHANGE_EVENT, callback)
 	},

	getQuoteRequestData: function(){
 		return {
 			formatted_reg_number : _regNo.join('-')
 		}
 	},

 	getRTOS: function(){
 		return _rtos
 	},

 	getPopularRTOs: function(){
 		return _popularRTOs
 	},

 	getRegistrationNumber: function(){
 		return _regNo.slice()
 	},

 	getSelectedRTO: function(){
 		var rto = _rtos.filter(function(item){
 			return item.is_selected
 		})[0]

 		if(rto){
 			return rto
 		}

 		if(typeof(Storage) !== "undefined") {
			if(localStorage.getItem(VEHICLE_TYPE)){
				var data = JSON.parse(localStorage.getItem(VEHICLE_TYPE))
				var rto = data.rto_info
				return rto
			}
		}


 	},

 	dispatcherIndex: AppDispatcher.register(function(action) {
 		 switch(action.actionType) {
 		 	case AppConstants.CLEAR_SELECTED_DATA:
 		 		AppDispatcher.waitFor([DateStore.dispatcherIndex])
 		 		action.isNewVehicle ? init() : null
 		 		saveToLocalStorage()
 		 		break

 		 	case AppConstants.RECIEVE_RTOS:
 		 		_rtos = action.rtos
 		 		setupPopularRTOs()
 		 		if(_regNo && _regNo.length>0){
 		 			selectRTOByRegistrationNumber(_regNo)
 		 		}
 		 		populateFromLocalStorage(function(){
 		 			rtoStore.emitChange()
 		 		})
 		 		break

 		 	case AppConstants.SET_REGISTRATION_NUMBER:
 		 		setRegistrationNumber(action.value)
 		 		saveToLocalStorage()
 		 		rtoStore.emitChange()
 		 		break

 		 	case AppConstants.SELECT_RTO:
 		 		selectRTO(action.rto)
 		 		saveToLocalStorage()
 		 		rtoStore.emitChange()
 		 		break

 		 	case AppConstants.SELECT_RTO_FROM_ID:
 		 		selectRTOfromID(action.rtoid)
 		 		saveToLocalStorage()
 		 		rtoStore.emitChange()
 		 		break

 		 }
 	})
})


module.exports = rtoStore
