var AppConstants = require('../constants/app-constants.js')
var Popular = require('../constants/popular.js')
var AppDispatcher = require('../dispatchers/app-dispatcher.js')
var APIutils = require('../utils/apiutils')
var DateStore = require('./date-store')

var apputils = require('../utils/apputils')

var EventEmitter = require('events').EventEmitter
var assign = require('object-assign')

var CHANGE_EVENT = "change"

var _vehicles = []
var _variants
var _filtered_variants
var _fuel_types
var _selected_fuel_type
var _cng_kit_value
var _is_cng_fitted
var _rds_id
var _fastlane_success
var _fastlane_model_changed
var _fastlane_fueltype_changed
var _fastlane_variant_changed
var _fastlane_vehicle_model_id
var	_fastlane_vehicle_master_id

var init = function(resetFastlaneFlag){
	_variants = []
	_filtered_variants = []
	_fuel_types = []
	_selected_fuel_type = null
	_cng_kit_value = 0
	_is_cng_fitted = 0
	_rds_id = null
	_fastlane_model_changed = false
	_fastlane_fueltype_changed = false
	_fastlane_variant_changed = false
	_fastlane_vehicle_model_id = null
	_fastlane_vehicle_master_id = null

	if(resetFastlaneFlag)
		_fastlane_success = false

	for(var index=0; index<_vehicles.length; index++){
		if(_vehicles[index].is_selected)
			_vehicles[index].is_selected = false
	}
}

init(true)

// The following is needed for mapping with the desktop code so that if a link is shared on mobile it works.
var fuel_type_list = 
			[{
                'id': 'PETROL',
                'name': 'Petrol',
                'code': 'petrol',
            },
            {
                'id': 'DIESEL',
                'name': 'Diesel',
                'code': 'diesel',
            },
            {
                'id': 'INTERNAL_LPG_CNG',
                'name': 'CNG/LPG company fitted',
                'code': 'cng_lpg_internal',
            },
            {
                'id': 'PETROL',
                'name': 'CNG/LPG externally fitted',
                'code': 'cng_lpg_external',
            }];



var setupVehicles = function(){
	var popularVehicles = Popular.vehicles

	if(popularVehicles.length>0){
		_vehicles = _vehicles.map(function(item){
			if(popularVehicles.indexOf(item.name) > -1){
				item.is_popular = true
				item.rank = popularVehicles.indexOf(item.name)
			}

			return item
		})
	}
}

var selectVehicle = function(vehicle, callback){

	init(false)

	var v = _vehicles.filter(function(item){
		return item.id == vehicle.id
	})

	if(v.length > 0){
		v[0].is_selected = true
	}

	APIutils.getVariants(vehicleStore.getSelectedVehicle().id, callback)
}


var selectVariant = function(variant){
	var s = _variants.filter(function(item){
		return item.is_selected
	})[0]

	if(s){
		s.is_selected = false	
	}

	var v = _variants.filter(function(item){
		return item.id == variant.id
	})

	if(v.length > 0){
		v[0].is_selected = true
		if(!_selected_fuel_type){
			_selected_fuel_type = v[0].fuel_type
		}
	}
	
}


var setFuelTypesFromVariants = function(){

	if(_variants.length > 0){
		var fuel_types = [];
		for (var i = _variants.length - 1; i >= 0; i--) {
			var variant = _variants[i]
			if(fuel_types.indexOf(variant.fuel_type) == -1 && variant.fuel_type !== 'CNG' && variant.fuel_type !== 'LPG' ){
				fuel_types.push(variant.fuel_type);
			}
		};
		if(fuel_types.indexOf('PETROL') > -1){
			fuel_types.push('EXTERNAL_LPG_CNG')
		}

		_fuel_types = fuel_types

	}

}

var selectFuelType = function(fuel_type){
	_selected_fuel_type = fuel_type
	if(_selected_fuel_type.indexOf('CNG') > -1){
		_is_cng_fitted = 1
	}
	else{
		_is_cng_fitted = 0
	}
}

var setVehicleInfo = function(data, callback){
	selectVehicle({id:data['vehicle_model_id']},
		function(){
			_fastlane_success = true
			_rds_id = data['rds_id']
			_fastlane_vehicle_model_id = data['vehicle_model_id']
			_fastlane_vehicle_master_id = data['vehicle_master_id']
			selectVariant({id:_fastlane_vehicle_master_id})
			selectFuelType(vehicleStore.getSelectedVariant().fuel_type)
			if(callback)
				callback()
			saveToLocalStorage()
		})
}

var populateFromLocalStorage = function(callback){
	if(typeof(Storage) !== "undefined") {
		if(localStorage.getItem(VEHICLE_TYPE)){
			var data = JSON.parse(localStorage.getItem(VEHICLE_TYPE))
			var vehicle = data.vehicle;
			var variant = data.vehicleVariant;
			var fuelType = data.fuel_type;

			if(vehicle && variant){
				selectVehicle(vehicle, function(){
					selectVariant(variant)
					callback()
				})
			}
			else{
				callback()
			}

			if(data.cngKitValue){
				_cng_kit_value = data.cngKitValue
				callback()
			}

			if(data.isCNGFitted){
				_is_cng_fitted = data.isCNGFitted
				callback()
			}

			if(data.fastlane_success !== undefined)
				_fastlane_success = data.fastlane_success

			if(_fastlane_success){
	        	_fastlane_model_changed = data.fastlane_model_changed
	        	_fastlane_variant_changed = data.fastlane_variant_changed
	        	_fastlane_fueltype_changed = data.fastlane_fueltype_changed
	        }

			if(data.fuel_type){
				switch (data.fuel_type.code) {
				    case 'petrol':
				        selectFuelType('PETROL')
				        break;
				    case 'diesel':
				        selectFuelType('DIESEL')
				        break;
				    case 'cng_lpg_internal':
				        selectFuelType('INTERNAL_LPG_CNG')
				        break;
				    case 'cng_lpg_external':
				        selectFuelType('EXTERNAL_LPG_CNG')
				        break;
				}
			}
		}
		else{
			callback()
		}
	}
	else{
		callback()	
	}
}	



var saveToLocalStorage = function(){
	if(typeof(Storage) !== "undefined") {

		var data = localStorage.getItem(VEHICLE_TYPE);
        if(!data) {
            data = {};
        } else {
            data = JSON.parse(data);
        }
        if(_vehicles && _vehicles.length>0){
        	var vehicle =  _vehicles.filter(function(item){
	 			return item.is_selected
	 		})[0]
        	data.vehicle = vehicle
        }else{
        	data.vehicle = undefined
        }
        if(_variants && _variants.length>0){
	        var variant =  _variants.filter(function(item){
	 			return item.is_selected
	 		})[0]
        	data.vehicleVariant = variant
        }else{
        	data.vehicleVariant = undefined
        }
        if(_cng_kit_value){
        	data.cngKitValue = _cng_kit_value
        }else{
        	data.cngKitValue=undefined
        }
        if(_is_cng_fitted == 1 || _is_cng_fitted == 0){
        	data.isCNGFitted = _is_cng_fitted
        }else{
        	data.isCNGFitted = 0
        }
        data.fastlane_success = _fastlane_success
        if(data.fastlane_success){
        	data.fastlane_model_changed = _fastlane_model_changed
        	data.fastlane_variant_changed = _fastlane_variant_changed
        	data.fastlane_fueltype_changed = _fastlane_fueltype_changed
        }
        if(_selected_fuel_type){
        	switch (_selected_fuel_type) {
			    case 'PETROL':
			        data.fuel_type = fuel_type_list[0]
			        break;
			    case 'DIESEL':
			        data.fuel_type = fuel_type_list[1]
			        break;
			    case 'INTERNAL_LPG_CNG':
			        data.fuel_type = fuel_type_list[2]
			        break;
			    case 'EXTERNAL_LPG_CNG':
			        data.fuel_type = fuel_type_list[3]
			        break;
			}
        }else{
        	data.fuel_type = undefined
        }
		localStorage.setItem(VEHICLE_TYPE, JSON.stringify(data));
	}
}

var clearLocalStorage = function(isNewVehicle){
	init(true)
	saveToLocalStorage()
}

var vehicleStore = assign({}, EventEmitter.prototype, {
	emitChange: function(){
		this.emit(CHANGE_EVENT)
	},

	addChangeListener: function(callback){
		this.on(CHANGE_EVENT, callback)
	},

	removeChangeListener: function(callback) {
    	this.removeListener(CHANGE_EVENT, callback)
 	},

 	getQuoteRequestData: function(){
 		return {
 			fastlane_success : _fastlane_success,
 			fastlane_model_changed : _fastlane_model_changed,
 			fastlane_fueltype_changed : _fastlane_fueltype_changed,
 			fastlane_variant_changed : _fastlane_variant_changed
 		}
 	},

 	getFastlaneStatus: function(){
 		return _fastlane_success
 	},

 	fastlaneDataEdited : function(){
 		return _fastlane_model_changed || _fastlane_fueltype_changed || _fastlane_variant_changed
 	},

 	hasFastlaneDataChanged: function(){
 		return !_fastlane_success || (_fastlane_success && (_fastlane_model_changed || _fastlane_fueltype_changed || _fastlane_variant_changed))
 	},

 	getVehicles: function(){
 		return _vehicles
 	},

 	getPopularVehicles: function(){
 		return _vehicles.filter(function(item){
 			return item.is_popular
 		});
 	},

 	getSelectedVehicle: function(){
 		var vehicle =  _vehicles.filter(function(item){
 			return item.is_selected
 		})[0]

 		if(vehicle){
 			return vehicle;
 		}

 		if(typeof(Storage) !== "undefined") {
			if(localStorage.getItem(VEHICLE_TYPE)){
				var data = JSON.parse(localStorage.getItem(VEHICLE_TYPE))
				var vehicle = data.vehicle
				return vehicle
			}
		}

 	},

 	getVariants: function(){
 		if(_selected_fuel_type){
 			var sft = _selected_fuel_type
 			if(sft == 'EXTERNAL_LPG_CNG'){
 				sft = 'PETROL'
 			}
 			return _variants.filter(function(item){
 				return item.fuel_type == sft
 			});
 		}
 		else{
 			return _variants
 		}
 	},

 	getSelectedFuelType: function(){
 		return _selected_fuel_type
 	},


 	getSelectedVariant: function(){
 		var variant =  _variants.filter(function(item){
 			return item.is_selected
 		})[0]

 		if(variant){
 			return variant;
 		}

 		if(typeof(Storage) !== "undefined") {
			if(localStorage.getItem(VEHICLE_TYPE)){
				var data = JSON.parse(localStorage.getItem(VEHICLE_TYPE))
				var variant = data.vehicleVariant
				return variant
			}
		}
 	},

 	getFuelTypes: function(){
 		return _fuel_types
 	},

 	getCNGFittedStatus: function(){
 		return _is_cng_fitted
 	},

 	getRdsId: function(){
 		return _rds_id;
 	},

 	getCNGKitValue: function(){
 		return _cng_kit_value
 	},

 	dispatcherIndex: AppDispatcher.register(function(action) {
 		 switch(action.actionType) {
 		 	case AppConstants.CLEAR_SELECTED_DATA:
 		 		AppDispatcher.waitFor([DateStore.dispatcherIndex])
 		 		clearLocalStorage(action.isNewVehicle)
 		 		break

 		 	case AppConstants.RECIEVE_VEHICLES:
 		 		_vehicles = action.vehicles
 		 		setupVehicles()
 		 		populateFromLocalStorage(function(){
 		 			vehicleStore.emitChange()	
 		 		})
 		 		break

 		 	case AppConstants.RECIEVE_VEHICLE_INFO:
 		 		setVehicleInfo(action.data, action.callback)
 		 		break

 		 	case AppConstants.SELECT_VEHICLE:
 		 		_fastlane_model_changed = (action.vehicle.id != _fastlane_vehicle_model_id)
 		 		selectVehicle(action.vehicle, action.callback)
 		 		saveToLocalStorage()
 		 		vehicleStore.emitChange()
 		 		break

 		 	case AppConstants.SET_CNG_VALUE:
 		 		_cng_kit_value = action.value + ''
 		 		saveToLocalStorage()
 		 		vehicleStore.emitChange()
 		 		break

 		 	case AppConstants.DESELECT_VEHICLE:
 		 		init(false)
 		 		saveToLocalStorage()
 		 		vehicleStore.emitChange()
 		 		break

 		 	case AppConstants.RECIEVE_VARIANTS:
 		 		_variants = action.variants
 		 		setFuelTypesFromVariants()
 		 		vehicleStore.emitChange()
 		 		if(action.callback)
 		 			action.callback()
 		 		// The funny fix. Two emit changes, though not technically wrond but bad, bad pattern.
 		 		vehicleStore.emitChange()
 		 		break

 		 	case AppConstants.SELECT_VARIANT:
 		 		_fastlane_variant_changed = (action.variant.id != _fastlane_vehicle_master_id)
 		 		if(!_fastlane_variant_changed)
 		 			_fastlane_fueltype_changed = false
 		 		selectVariant(action.variant)
 		 		saveToLocalStorage()
 		 		vehicleStore.emitChange()
 		 		break

 		 	case AppConstants.SELECT_FUEL_TYPE:
 		 		_fastlane_fueltype_changed = true
 		 		selectFuelType(action.fuel_type)
 		 		saveToLocalStorage()
 		 		vehicleStore.emitChange()
 		 		break

 		 	case AppConstants.SEND_FASTLANE_FEEDBACK:
			 	APIutils.sendFastlaneFeedback(_rds_id, vehicleStore.fastlaneDataEdited(),
			 		vehicleStore.getSelectedVehicle().id, vehicleStore.getSelectedVariant().id)
			 	break
 		 }
 	})
})


populateFromLocalStorage(function(){})

module.exports = vehicleStore