var request = require('superagent')
var qs = require('qs')

var AppActions = require('../actions/app-actions')


var rawvehicles;
var rawrtos;
var rawvariants;
var fetchedPlans = [], latestPlans=[], finished;
var quoteId;
var activityId;

function getVehicleInfo(regNo, callback, failureCallback) {
	request
		.post('/motor/' + VEHICLE_TYPE + '/api/vehicle-info/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.send({
			'name': regNo,
			'csrfmiddlewaretoken': CSRF_TOKEN
		})
		.end(function(err, res) {
			if(res){
				if (res.ok)
					res = JSON.parse(res.text)
				if (res.data && !!res.data['vehicle_master_id'] && !!res.data['vehicle_model_id'] && !!res.data.fastlane.response.result.vehicle['regn_dt'])
					callback(res.data)
				else
					failureCallback()
			} else {
				failureCallback()
				console.log("Something went wrong", err)
			}
		})
}

function getVariants(vehicleId, callback){

	request
		.post('/motor/' + VEHICLE_TYPE + '/api/vehicles/' + vehicleId + '/variants/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.send({
			'csrfmiddlewaretoken': CSRF_TOKEN
		})
		.end(function(err, res){
			if(res.ok){
				var response = JSON.parse(res.text);
				rawvariants = response.data.variants;

				if(callback){
					callback();
				}
			}
			else{
				console.log("Something went wrong", err);
			}
		});
}


function getVehicles(callback){
	request
		.post('/motor/' + VEHICLE_TYPE + '/api/vehicles/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.send({
			'csrfmiddlewaretoken': CSRF_TOKEN
		})
		.end(function(err, res){
			if(res.ok){
				var response = JSON.parse(res.text);
				rawvehicles = response.data.models;
				if(callback){
					callback();
				}
			}
			else{
				console.log("Something went wrong", err);
			}
		});
}

function getRTOs(callback){

	request
		.post('/motor/' + VEHICLE_TYPE + '/api/rtos/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.send({
			'csrfmiddlewaretoken': CSRF_TOKEN
		})
		.end(function(err, res){
			if(res.ok){
				var response = JSON.parse(res.text);
				rawrtos = response.data.rtos;
				if(callback){
					callback();
				}
			}
			else{
				console.log("Something went wrong", err);
			}
		});
}


function getPlansOld(quote, callback) {
	request
		.post('/motor/' + VEHICLE_TYPE + '/api/quotes/')
		.type('application/x-www-form-urlencoded')
		.send(qs.stringify(quote, { arrayFormat: 'brackets' }))
		.end(function(err, res){
			if(res.ok){
				var response = JSON.parse(res.text);
				rawplans = response.data.premiums
				quoteId = response.data.quoteId;
				if(callback){
					callback();
				}
			}
			else{
				console.log("Something went wrong", err);
			}
		})
}

var fetch
var req
var max_fetch = 8
var fetch_counter = 0



function getPlans(quote, callback){
	fetchedPlans = []
	latestPlans = []
	finished = false
	quoteId = null
	if(fetch){
		clearInterval(fetch)
	}
	if(req){
		req.abort()
	}

	request
		.post('/motor/' + VEHICLE_TYPE + '/api/quotes-async/')
		.type('application/x-www-form-urlencoded')
		.send(qs.stringify(quote, { arrayFormat: 'brackets' }))
		.end(function(err, res){
			if(res.ok){
				var response = JSON.parse(res.text);

				quoteId = response.data["quoteId"];
				fetch_counter = 0

				if(!response.success){
					finished = true
					if(callback)
						callback()
					return
				}

				var fetch_async = function(){
					req = request
						.post('/motor/' + VEHICLE_TYPE + '/api/quotes-async/' + quoteId +'/')
						.type('application/x-www-form-urlencoded')
						.send(qs.stringify(quote, { arrayFormat: 'brackets' }))
						.end(function(err, res){
							if(res.ok){
								response = JSON.parse(res.text)

								var allCurrentInsurers = fetchedPlans.map(function(item){
									return item.insurerSlug
								})

								latestPlans = response.data.premiums.filter(function(item){
			                        return allCurrentInsurers.indexOf(item.insurerSlug) == -1 ;
			                    });

			                    for (var i = 0; i < latestPlans.length; i++) {
			                    	fetchedPlans.push(latestPlans[i])
			                    };

								finished = response.data.finished

								fetch_counter++

								if(callback){
									callback()
								}

								if(finished || fetch_counter == max_fetch){
									clearInterval(fetch)
								}

							}
							else{
								console.log(err, "Something went wrong")
							}
					})
				}

				fetch_async()
				fetch = setInterval(fetch_async, 2500)

			}
			else{
				console.log("Something went wrong", err);
			}
		})


}

function sendFastlaneFeedback(rds_id: number, confirmation_type: string, data: any, callback?:Function){
	request
		.post('/motor/' + VEHICLE_TYPE + '/api/vehicle-confirmation/')
		.set('Content-Type', 'application/x-www-form-urlencoded')
		.send({
			'data': JSON.stringify(data),
			'confirmation_type' : confirmation_type,
			'rds_id': rds_id,
			'csrfmiddlewaretoken': CSRF_TOKEN
		})
		.end(function(err, res){
			console.log("Fastlane Feedback")
		});
}

function gotoTransaction(quote, plan, callback){
    request
		.post('/motor/' + VEHICLE_TYPE + '/api/confirm/' + quote.quoteId + '/' + plan.insurerSlug + '/')
		.type('application/x-www-form-urlencoded')
		.send(qs.stringify(quote, { arrayFormat: 'brackets' }))
		.end(function(err, res){
			if(res.ok){
				var response = JSON.parse(res.text);
				var form_url = '/motor/' + VEHICLE_TYPE + '/' + plan.insurerSlug + '/desktopForm/' + response.data.transactionId + '/';
				if(callback){
					callback();
				}
				window.location.href = form_url

			}
			else{
				console.log("Something went wrong", err);
			}
		})
}

module.exports = {
	getInitialData: function(){
		getVehicles(function(){
				AppActions.recieveVehicles(rawvehicles);
			})
		getRTOs(function(){
				AppActions.recieveRTOs(rawrtos);
			})
	},

	getVehicleInfo: function(regNo, callback, failureCallback){
		getVehicleInfo(regNo, function(data){
			AppActions.receiveVehicleInfo(data, callback)
		}, failureCallback)
	},

	getVariants: function(vehicleId, callback){
		getVariants(vehicleId, function(){
			AppActions.recieveVariants(rawvariants, callback);
		})
	},

	getPlans: function(quote){
		getPlans(quote, function(){
			AppActions.recievePlans(latestPlans, quoteId, fetch_counter, finished);
		})
	},

	gotoTransaction: gotoTransaction,

	abortAsyncRequest: function(){
		if(req){
			req.abort()
		}
		if(fetch){
			clearInterval(fetch)
		}
	},

	sendFastlaneFeedback: function(rdsId, dataChanged, modelId, masterId){
		var data = {
			'vehicle_model_id': modelId,
			'vehicle_master_id': masterId
		}
		var confirmationType = dataChanged ? 'changed' : 'accepted'
		sendFastlaneFeedback(rdsId, confirmationType, data)
	}
}
