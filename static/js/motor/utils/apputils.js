var utils = {
	getSelectedItemIndex: function(items){
		var l = items.length
		for (var i = 0; i < l; i++) {
			if(items[i].is_selected == true){
				return i
			}
		}
		return -1
	},
	deselectItem: function(items){
		var selected_index = utils.getSelectedItemIndex(items)
		if(selected_index > -1){
			items[selected_index].is_selected = false
		}
	},
	numberToINR: function(number){
		var valueWasNegative = false;
        if (number < 0) {
            number = number * -1;
            valueWasNegative = true;
        }
        var x = number.toString();
        var res;
        var afterPoint = '';
        if (x.indexOf('.') > 0)
            afterPoint = x.substring(x.indexOf('.'), x.length);
        x = Math.floor(x);
        x = x.toString();
        var lastThree = x.substring(x.length - 3);
        var otherNumbers = x.substring(0, x.length - 3);
        if (otherNumbers != '')
            lastThree = ',' + lastThree;
        res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

        var sign = valueWasNegative ? '-' : '';
        return sign + " " + res;
	},
	getValueFromQueryParam: function(key){
		var queryString = window.location.search.substr(1)
		var queryParams = queryString ? queryString.split('&') : null
		for(var index in queryParams){
			if(queryParams[index].startsWith(key+'='))
				return queryParams[index].split('=')[1]
		}
	}
}

module.exports = utils