var utils = {

	current_year: function(){
		var today = new Date()
		return today.getFullYear()
	},
	current_month: function(){
		var today = new Date()
		return today.getMonth()
	},
	reg_years: function(){
		var y = []
		var cy = utils.current_year(),
			cm = utils.current_month()
		if(cm < 9){
			cy--
		}
		for(var i = cy; i >= cy-15;i--){
			y.push(i)
		}
		return y
	},
	man_years: function(){
		var y = []
		var cy = utils.current_year()

		for(var i = cy; i > cy-2;i--){
			y.push(i)
		}
		return y
	},

}

module.exports = utils