var gtm_push = function(gtm_event, gtm_category, gtm_action, gtm_label, gtm_extra_params){
        
        var data = {
            "event":gtm_event,
            "category":gtm_category,
            "action":gtm_action,
            "label":gtm_label,
        }
        for(var param in gtm_extra_params){
            data[param] = gtm_extra_params[param]
        }

        dataLayer.push(data)
    }

var gtm_event_push = function(gtm_event, gtm_category, gtm_action, gtm_label, gtm_extra_params){
        
        var data = {
            "event":gtm_event,
            "eventCategory":gtm_category,
            "eventAction":gtm_action,
            "eventLabel":gtm_label,
        }
        for(var param in gtm_extra_params){
            data[param] = gtm_extra_params[param]
        }

        dataLayer.push(data)
    }


var page_view = function(page, title) {
    if (window.dataLayer) {
        window.dataLayer.push({
            'event': 'virtual_page_view',
            'virtual_page_path': page,
            'virtual_page_title': title
        });
    }
};

var fastlane_home = function(){
    if(!window.dataLayer)
        return
    var data = {
        'event' : 'CarEventMobileFastlaneStepZero',
        'virtual_page_path' : '/vp/motor/step1/fastlane',
        'virtual_page_title' : 'VP-MOTOR-STEP1-FASTLANE'
    }
    window.dataLayer.push(data)
}

var fastlane_view_quotes = function(eventName, category, action, label, p1,p2,p3,p4,p5,p6){
    if(!window.dataLayer)
        return
    var data = {
        'event' : eventName,
        'category' : category,
        'action' : action,
        'label' : label,
        'param1' : p1,
        'param2' : p2,
        'param3' : p3,
        'param4' : p4,
        'param5' : p5,
        'param6' : p6
    }
    window.dataLayer.push(data)
}

var fvq_claimed = function(isExpired, model, fuel, variant, rto, regyear, expirydate){
    var expiryval = isExpired ? 'Expired Policy' : 'Not Expired Policy'
    fastlane_view_quotes(
        'FastlaneCarEventMobileClaimMadeQuotes',
        'Mobile Fastlane Car - Get Quotes - Claim Made',
        expiryval,
        'Claim Made',
        model,
        fuel,
        variant,
        rto,
        regyear,
        expirydate
    )
}

var fvq_unclaimed = function(isExpired, ncb, model, fuel, variant, rto, regyear, expirydate){
    var expiryval = isExpired ? 'Expired Policy' : 'Not Expired Policy'
    var ncbval = ncb!==undefined ? ncb : 'NCB Unknown'
    fastlane_view_quotes(
        'FastlaneCarEventMobileNCB',
        'Mobile Fastlane Car - Get Quotes - Claim Not Made',
        expiryval,
        ncbval,
        model,
        fuel,
        variant,
        rto,
        regyear,
        expirydate
    )
}

var fvq_expired = function(fastlaneDataChanged, model, fuel, variant, rto, regyear){
    var changeval = fastlaneDataChanged ? 'With Change' : 'Without Change'
    fastlane_view_quotes(
        'FastlaneCarEventMobileCarDetails',
        'Mobile Fastlane Car - Get Quotes - Greater Than 90 Days',
        'Expired Policy',
        changeval,
        model,
        fuel,
        variant,
        rto,
        regyear,
        'More than 90 days'
    )
}

var fsConfirmData = function(hasDataChanged, isExpired, data, model, fuel, variant, rto, regyear, expirydate){
    var fsData = hasDataChanged ? data : 'Without Change'
    var expiry = isExpired ? 'Expired Policy' : 'Not Expired Policy'
    fastlane_view_quotes(
        'FastlaneCarEventMobileCarDetails',
        'Mobile Fastlane Car - Model Confirmed',
        expiry,
        fsData,
        model,
        fuel,
        variant,
        rto,
        regyear,
        expirydate
    )
}

var fsClaimMadeClick = function(isExpired, model, fuel, variant, rto, regyear, expirydate){
    var expiry = isExpired ? 'Expired Policy' : 'Not Expired Policy'
    fastlane_view_quotes(
        'FastlaneCarEventMobileClaimMade',
        'Mobile Fastlane Car - Claim Confirmed',
        expiry,
        'No Claim',
        model,
        fuel,
        variant,
        rto,
        regyear,
        expirydate
    )
}

var stepOutReason = ['Fastlane Not Found', 'User Number Not Known', 'I Have A New Car']
var fastlaneStepedOut = function(status){
    if(!window.dataLayer)
        return
    var data = {
        'event' : 'FastlaneCarEventMobileProductPage',
        'category' : 'Mobile Fastlane Car - Redirect Product',
        'action' : 'Fastlane',
        'label' : stepOutReason[status]
    }
    window.dataLayer.push(data)
}

var fsStepOne = function(){
    if(!window.dataLayer)
        return
    var data = {
        'event' : 'FastlaneCarEventMobileStepone',
        'category' : 'Mobile Fastlane Car - Number Accepted',
        'action' : 'Number Confirmed'
    }
    window.dataLayer.push(data)
}

var fsStepTwo = function(isExpired){
    if(!window.dataLayer)
        return
    var data = {
        'event' : 'FastlaneCarEventMobileSteptwo',
        'category' : 'Mobile Fastlane Car - Expiry Confirmed',
        'action' : isExpired ? 'Expired Policy' : 'Not Expired Policy'
    }
    window.dataLayer.push(data)
}

var fsStepTwoOne = function(){
    if(!window.dataLayer)
        return
    var data = {
        'event' : 'FastlaneCarEventMobileSteptwoone',
        'category' : 'Mobile Fastlane Car - Expiry Date',
        'action' : 'Expired Policy',
        'label' : 'Date Submitted'
    }
    window.dataLayer.push(data)
}

var fsOnEnterStepThree = function(isExpired){
    if(!window.dataLayer)
        return
    var data = {
        'event' : 'FastlaneCarEventMobileStepthree',
        'category' : 'Mobile Fastlane Car - Car Details Page',
        'action' : isExpired ? 'Expired Policy' : 'Not Expired Policy'
    }
    window.dataLayer.push(data)
}

var fsEditClick = function(step){
    if(!window.dataLayer)
        return
    var data = {
        'event' : 'FastlaneCarEventMobileDateEdit',
        'category' : 'Mobile Fastlane Car - Edit Date',
        'action' : step === 0 ? 'Registration Number' : 'Policy Expiry Date',
        'label' : 'Edit Clicked'
    }
    window.dataLayer.push(data)
}

module.exports = {
    gtm_push: gtm_push,
    page_view : page_view,
    gtm_event_push : gtm_event_push,
    fastlane_home : fastlane_home,
    fvq_unclaimed : fvq_unclaimed,
    fvq_claimed :fvq_claimed,
    fvq_expired : fvq_expired,
    fastlaneStepedOut : fastlaneStepedOut,
    fsStepOne : fsStepOne,
    fsStepTwo : fsStepTwo,
    fsStepTwoOne : fsStepTwoOne,
    fsOnEnterStepThree : fsOnEnterStepThree,
    fsEditClick : fsEditClick,
    fsConfirmData : fsConfirmData,
    fsClaimMadeClick : fsClaimMadeClick
}