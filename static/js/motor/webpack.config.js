var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: {
        mobile: "./mobile.js"
    },
    output: {
        path: './build',
        filename: '[name].[hash].bundle.js'
    },
    module: {
        loaders: [
            { test: /\.jsx?$/, loader: 'babel-loader' },
            { test: /\.png$/, loader: "file-loader" },
            { test: /\.jpg$/, loader: "file-loader" },
            { test: /\.tsx?$/, loader: "typescript-simple-loader" }
        ],
    },
    resolve: {
        extensions: ['', '.js', '.jsx','.ts', '.tsx']
    },
    cache: true,
    devtool: '#source-map',
    plugins: [
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        function() {
            this.plugin("done", function(stats) {
                require("fs").writeFileSync(
                    path.join(__dirname, "./build", "stats.json"),
                    JSON.stringify(stats.toJson())
                );
                require("fs").utimesSync(
                    path.join(__dirname, "../../../base/settings.py"),
                    new Date(),
                    new Date()
                );
            });
        }
    ]
};
