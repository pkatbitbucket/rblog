import AppDispatcher = require('../../common/Dispatcher')
import ActionEnum = require('../constants/ActionEnum')

import AppActions = require('../actions/AppActions')

export = {
	setProductType: function(value:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_PRODUCT_TYPE,
			value:value,
			callback:callback
		})
	},

	setGender: function(value: any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.SET_GENDER,
			value:value,
			callback:callback
		})
	},

	setMembers: function(value:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.SET_MEMBERS,
			value:value,
			callback:callback
		})
	},

	setSelectedMembers: function(value:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.SET_SELECTED_MEMBERS,
			value:value,
			callback:callback
		})
	},

	setPincode: function(value:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.SET_PINCODE,
			value:value,
			callback:callback
		})
	},

	clearPincode: function(value?:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.CLEAR_PINCODE,
			value:value,
			callback:callback
		})
	},

	clearParentsPincode: function(value?:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.CLEAR_PARENTS_PINCODE,
			value:value,
			callback:callback
		})
	},

	setParentsPincode: function(value:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.SET_PARENTS_PINCODE,
			value:value,
			callback:callback
		})
	},

	setEmail: function(value:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.SET_EMAIL,
			value:value,
			callback:callback
		})
	},

	setDeductible: function(value:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.SET_DEDUCTIBLE,
			value:value,
			callback:callback
		})
	},

	setParentsSelected: function(value:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.SET_PARENTS_SELECTED,
			value:value,
			callback:callback
		})
	},

	setMobile: function(mobile:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.SET_MOBILE,
			value:mobile,
			callback:callback
		})
	},

	addMember: function(memberType:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.ADD_MEMBER,
			value:memberType,
			callback:callback
		})
	},

	removeMember: function(memberState:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.REMOVE_MEMBER,
			value:memberState,
			callback:callback
		})
	},

	setMedicalConditions: function(value:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.SET_MEDICAL_CONDITIONS,
			value:value,
			callback:callback
		})
	},

	setDiseaseToDeclareFlag: function(value:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.SET_DISEASE_TO_DECLARE_FLAG,
			value:value,
			callback:callback
		})
	},

	getActivityId: function(value?:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.GET_ACTIVITY_ID,
			value:value,
			callback:callback
		})
	},

	setAllMedicalConditions: function(value:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.SET_DISEASES,
			value:value,
			callback:callback
		})
	},

	removeSingleKid: function(value:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.REMOVE_ONE_KID,
			value:value,
			callback:callback
		})
	},


	setAge: function(value:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.SET_AGE,
			value:value,
			callback:callback
		})
	},

	setContinue: function(value?:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.SET_CONTINUE,
			value:value,
			callback:callback
		})
	},

	clearOptionalEmail: function(value?:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.CLEAR_OPTIONAL_EMAIL,
			value:value,
			callback:callback
		})
	},

	clearOptionalMobile: function(value?:any, callback?:Function){
		AppDispatcher.dispatch({
			actionType:ActionEnum.CLEAR_OPTIONAL_MOBILE,
			value:value,
			callback:callback
		})
	},

}