import React = require('react')

import AppActions = require('../actions/AppActions')

// Constants
import MembersAge = require('../constants/MembersAge')

import Checkbox  = require('../../common/new-components/ButtonCheckbox');
import Button = require('../../common/react-component/Button')
import Select = require('../../common/react-component/MultiSelect')
import Modal = require('../../common/new-components/Modal')


class BaseMembersComponent extends React.Component <any, any>{
	refs: any;

	static defaultProps = {
		displayBlock:false,
		totalNoOfChildrens:7,
	}
	
	constructor(props){
		super(props)
		var membersState = this.props.membersState;
		var members = this.props.members;

		this.state = {
			membersState:membersState,
			members:members,
			step:'step1',
			showStep1Errors:false,
			showStep2Errors:false,
		}
	}

	// Called when NEW MEMBERS - gets added
	componentWillReceiveProps(newprops){
		this.setState({
			membersState:newprops.membersState,
			members:newprops.members,
		});
	}

	addMember(memberCategory:any){
		if(this.props.onAddMember)
			this.props.onAddMember(memberCategory)
	}

	removeMember(memberCategory:any){
		if(this.props.onRemoveMember)
			this.props.onRemoveMember(memberCategory)
	}

	handleCheckboxChange(checked:boolean,memberCategory?:any){
		if(checked===true){
			this.addMember(memberCategory)
		}else{
			this.removeMember(memberCategory)
		}
	}

	addOneKid(kidType){
		this.addMember(kidType)
	}

	removeOneKid(kidType){
		if(this.props.onRemoveOneKid)
			this.props.onRemoveOneKid(kidType)
	}

	getStep1Block(){
		var self = this
		var membersState = this.state.membersState

		var totalNoOfSons = membersState['son'].count
		var totalNoOfDaughter = membersState['daughter'].count

		var displayAddKidButton = (totalNoOfSons + totalNoOfDaughter <this.props.totalNoOfChildrens)? true : false

		return Object.keys(membersState).map(function(member,index){

			var disableKidCheckbox = (displayAddKidButton===false && membersState[member].type=='kid' && membersState[member].checked===false)? true : false

			return (
				<div className="m-list__data" key={ index } >
					<div className="m-list__data__checkbox">
						<Checkbox label={ membersState[member].displayName } isChecked={ membersState[member].checked } value={ membersState[member].personAlias } onChange={ self.handleCheckboxChange.bind(self) } isDisabled={ disableKidCheckbox } /> 
					</div>
					{
						membersState[member].checked === true && membersState[member].type=='kid'?
							<div className="action_add_member">
								<span className="add_link" onClick={self.removeOneKid.bind(self,membersState[member].personAlias)} >-</span>
								<span className="add_count">{ membersState[member].count }</span>

								{
									displayAddKidButton === true?
									(
										<span className="add_link" onClick={self.addOneKid.bind(self,membersState[member].personAlias)} >+</span>
									)
									:null
								}
							</div>
						:
							null
					}
				</div>
			)
		})
	}

	onAgeSelect(key){
		var value = this.refs[key].getValues();

		if(this.props.onAgeChange)
			this.props.onAgeChange(key,value)
	}

	getStep2Block(){
		var self = this
		var members = this.state.members
		var serializedMembers = [];
		var memberSerial = ['you','spouse','father','mother','son','daughter'];
		var updatedMembers = []
		members.map(function(member){
        	if(!Array.isArray(serializedMembers[member.personAlias])){
		 		serializedMembers[member.personAlias]=[]
           	}
            serializedMembers[member.personAlias].push(member)
		})

		memberSerial.map(function(person){
			if(serializedMembers[person]!=undefined){
				serializedMembers[person].map(function(member){
					updatedMembers.push(member)	
				})
			}
		})

		return updatedMembers.map(function(member,index){
			return (
				<div className="m-list__data" key={ index } >
					<span className='m-list__data__label' >{ member.displayName }</span>
					<div className='m-list__data__field'>
						<Select ref={ member.key } validations={['required']} optionsText='display' onChange={ self.onAgeSelect.bind(self,member.key) } placeHolder='Select Age' optionsValue='value' dataSource={ MembersAge[member.type]} values={ member.value } customClass='sleek' />
						{
							self.state.showStep2Errors === true?
							(
								(self.props.Step2Validation['valid']===false && self.props.Step2Validation['errorType']=='individual' && member.value === undefined)?
									<div className="w--error">Please select the age.</div>
								:
									null
							)
							:null
						}
					</div>
				</div>
			)
		})
	}

	onContinue(){
		if(this.props.Step1ValidationStatus.valid === true){
			this.setState({
				step:'step2',
				showStep1Errors:false,	
			})
		}else{
			this.setState({
				showStep1Errors:true,	
			})
		}

		if(this.props.onContinue){
			this.props.onContinue()
		}
	}
	
	onDone(){

		if(this.props.Step2Validation.valid===true){	

			this.setState({
				step:'step1',
				showStep2Errors:false
			},function(){
				if(this.props.onStep2Done)
					this.props.onStep2Done()
			})
		}else{
			this.setState({
				showStep2Errors:true
			})
		}
	}

	goBack(){
		this.setState({
			step:'step1'	
		})
	}


	handleModalClose(){
		if(this.props.onMemberModalClose){
			this.props.onMemberModalClose()
		}
	}

	render(){

		var MembersDashBoard = this.getStep1Block()
		var step2Block = this.getStep2Block()
		var memberBlock = undefined
		var title = (this.state.step == 'step1')?'Who do you want to insure?':'Please enter the age of selected members.'
		
			if(this.state.step == 'step1'){
				memberBlock = (
					<div>
						<div className="m-list">
							{ MembersDashBoard }
						</div>
						<div className="group-btn">
							<Button customClass="w--button--orange" title='CONTINUE' onClick={ this.onContinue.bind(this) } />
						</div>
						
						{
							this.state.showStep1Errors === true?
							(
								<div className="w--error">
								{ 
									this.props.Step1ValidationStatus.valid === false?
										<span>{ this.props.Step1ValidationStatus.errorMessage }</span>
									:
										null
								}
								</div>
							):
							null
						}
					</div>
				)
			}else{
				memberBlock = (
					<div>
						<div className='m-list'>
							{step2Block}
							{
								this.state.showStep2Errors === true?
								(
									<div>
									{
										this.props.Step2Validation.valid === false && this.props.Step2Validation.errorType == 'group' ?
											<div className="w--error">{ this.props.Step2Validation.errorMessage }</div>
										:
											null
									}
									</div>
								):
								null
							}
						</div>
						
						<div className="group-btn">
							<Button customClass="w--button--orange" title='DONE' onClick={ this.onDone.bind(this) } />
							<Button customClass="w--button--link" onClick={ this.goBack.bind(this) } title='Back' />
						</div>
					</div>
				)
			}
		

		return (
			<div>
			{
				this.props.displayBlock===true?
				(
					<div>
						<Modal ref='MemberPopup' onClose={ this.handleModalClose.bind(this) } children={memberBlock} isOpen={true} header={ title } isShowCloseIcon={true} size='' customHeaderClass="modal_header" />  
					</div>
				):
				null	
			}
			</div>
		)
	}
}

export = BaseMembersComponent