var membersAge = []


// Adults age
membersAge['adult']=[]

for(var i=18; i<=100; i++){
	membersAge['adult'].push({
		display:i+' years',
		value:i
	})
}


// Kids age
membersAge['kid']=[]

//populating months
for(var i=1; i<12; i++){
	var month = (i>1)? 'months':'month';
	membersAge['kid'].push({
		display: i+' '+month,
		value:i,
	})
}


//populating years
for(var i=1; i<=25; i++){
	membersAge['kid'].push({
		display: i+' years',
		value:i*12,
	})
}


export = membersAge;