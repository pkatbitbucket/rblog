export = [
	{
		text:'New Health Policy',
		detailed_text:'For those who have not bought a health policy yet',
		value: 'INDEMNITY'
	},
	{
		text:'Super Top-up',
		detailed_text:'Have an existing policy? Upgrade your cover to Rs. 10 Lacs for as less as Rs. 878!',
		value: 'SUPER_TOPUP'
	}
]