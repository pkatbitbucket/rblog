var HEALTH = {
	PARENT: 'Health_form_new',
	DEDUCTIBLE:'deductible',
	GENDER: 'gender',
	PINCODE: 'pincode',
	PARENTS_PINCODE: 'parents_pincode',
	PRODUCT_TYPE: 'product_type' ,
	PARENTS_SELECTED:'parents_selected',
	MEMBERS: {
		PARENT: 'members',
		ID: 'id',
		PERSON: 'person',
		TYPE: 'type',
		AGE_FULL:'age_full',
		AGE_IN_MONTHS: 'age_in_months',
		AGE:'age',
		DISPLAY_NAME:'displayName',
	},
	ALL_MEMBERS:'allMembers',
	EMAIL:'email',
	MEDICAL_CONDITIONS:'medical_conditions',
	DISEASE_TO_DECLARE_FLAG:'disease_to_declare',
	MOBILE:'mobile',
	MEMBERS_STATE:'members_state',
	CONTINUE:'continue',
}

export = {HEALTH:HEALTH}