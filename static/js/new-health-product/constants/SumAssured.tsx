var sumAssuredArray = []

for(var i=100000, j=1; i <=1000000; i+=100000, j++){
	var sumAssuredSuffix = (j>1)?'Lacs':'Lac';
	sumAssuredArray.push({
		display: j+' '+sumAssuredSuffix,
		value: i
	})
}

export = sumAssuredArray;