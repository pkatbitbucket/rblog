/// <reference path="../typings/tsd.d.ts" />
import React = require('react')
import BaseComponent = require('../common/BaseComponent')
import TransitionUtils = require('../common/utils/TransitionUtils')

// All Components
import RadioInput = require('../common/new-components/RadioInput')
import EmailComponent = require('../common/new-components/EmailInput')
import MobileComponent = require('../common/new-components/MobileInput')
import Modal = require('../common/new-components/Modal')
import Button = require('../common/react-component/Button')
import Select = require('../common/react-component/MultiSelect')
import MembersSection = require('./composite-components/BaseMembersComponent')
import PincodeComponent = require('../common/react-component/PincodeInput')

// Actions
import AppActions = require('./actions/AppActions')

// Stores
import HealthStore = require('./stores/HealthStore')
import MembersStore = require('./stores/MembersStore')
import ValidationStore = require('./stores/ValidationStore')
import MemberValidation = require('./stores/MemberValidation')

// API
import APIUtils = require("./utils/APIUtils")
import GTMUtils = require("./utils/GTMUtils")

//	Constants
import Gender = require('./constants/Gender')
import ProductTypes = require('./constants/ProductTypes')
let defaultProductType = ProductTypes[0]['value'];

interface IHealthState {
	email: any;
	parentsPincode: number;
	parentsSelected: any;
	pincode: number;
	members: any;
	gender: string;
	mobile:any;
	showErrors:any;
	membersState:any;
	Step1Validation:any;
	Step2Validation:any;
	displayMembersBlock:any;
	buttonType:boolean;
	openPincodeBlock:any;
	onContinue:boolean;
	membersApartFormParents:boolean;
}

function getStateFromStores(): any {
	return {
		gender: HealthStore.getGender(),
		members : MembersStore.getSelectedMembers(),
		parentsSelected: MembersStore.getParentsSelected(),
		membersApartFormParents: MembersStore.membersApartFormParents(),
		pincode: MembersStore.getPincode(),
		parentsPincode: MembersStore.getParentsPincode(),
		email: HealthStore.getEmail(),
		mobile: HealthStore.getMobile(),
		membersState: MembersStore.getMembersState(),
		Step1Validation: MemberValidation.step1Validation(),
		Step2Validation: MemberValidation.validate(),
		onContinue: MembersStore.getOnContinue()
	}
}

const STORES = [HealthStore,MembersStore]

class HealthIndemnity extends BaseComponent {
	refs: any;
	state: IHealthState;
	constructor(props) {
		super(props, {
			stores:STORES,
			getStateFromStores: getStateFromStores
		});

		this.state.showErrors = false
		this.state.displayMembersBlock = false
		this.state.buttonType = undefined
		// this.state.pincode = undefined
		this.state.openPincodeBlock = false
	}

	componentWillMount(){
		// AppActions.clearPincode()
		AppActions.setProductType(defaultProductType)
	}

	handelGenderChange(){
		var gender = this.refs['gender'].getValues();
		AppActions.setGender(gender)	// Emit change prevented
	}

	onAddMember(memberCategory:any){
		AppActions.addMember(memberCategory)
	}

	onRemoveMember(memberCategory:any){
		AppActions.removeMember(memberCategory)
	}

	handleMembersDisplay(show:boolean){
		this.setState({
			displayMembersBlock:(show===true)? true:false
		})
	}

	onRemoveOneKid(kidType){
		AppActions.removeSingleKid(kidType)
	}

	onAgeChange(key,age){
		AppActions.setAge({ key:key,age:age })
	}

	onStep2Done(){
		this.handleMembersDisplay(false)
	}

	handleOptionalMobile(mobile){
		AppActions.setMobile(mobile);
	}

	handleOptionalEmail(email){
		AppActions.setEmail(email);
	}

	getMemberValidation(){
		var isValid = {
			valid:true,
			errorMessage:null,
		}

		// Members Block
		var Step1Validation = this.state.Step1Validation
		var Step2Validation = this.state.Step2Validation

		if(Step1Validation.valid===false || Step2Validation.valid===false ){
			
			isValid.valid = false
			isValid.errorMessage = 'Please provide complete information of the members you would like to insure.'	
		}

		return isValid;
	}

	getPincodeValidation(){
		var isValid = {
			valid:true,
			errorMessage:null,
		}

		var pincode = ValidationStore.PincodeValidation()
		var parentsPincode =  ValidationStore.parentsPincodeValidation()

		if(pincode.valid===false || parentsPincode.valid===false){
			isValid.valid = false
			isValid.errorMessage = 'Please select your area pincode.'
		}

		return isValid

	}

	onViewQuotes(){
		var gender = this.refs.gender;

		var isGenderValid = ValidationStore.genderValidation();
		var memberValidation = this.getMemberValidation();
		var pincodeValidation = this.getPincodeValidation();

		if(isGenderValid.valid && memberValidation.valid && pincodeValidation.valid){

			// Optional Email
			var optionalEmail = this.refs.optionalEmail;
			if(!optionalEmail.isValid())
				optionalEmail.isValid(true)

			// Optional Mobile
			var optionalMobile = this.refs.optionalMobile;
			if(!optionalMobile.isValid())
				optionalMobile.isValid(true)

			if(!optionalEmail.isValid() || !optionalMobile.isValid()){
				this.setState({
					showErrors:true
				})
			}else{
				this.setState({
					showErrors:false,
					buttonType:'loading'
				},function(){
					// alert('SUCCESS')
					GTMUtils.onBuy()
					AppActions.getActivityId()	
					TransitionUtils.jumpTo('/health-plan/#medical-conditions')
				})
			}

		}else{
			this.setState({
				showErrors:true
			})
		}

	}

	onMemberModalClose(){
		this.handleMembersDisplay(false);
	}

	onValidPincode(pincode){
		AppActions.setPincode(pincode);
	}

	onValidParentPincode(pincode){
		AppActions.setParentsPincode(pincode);
	}

	handlelModalClose(open){
		this.setState({
			openPincodeBlock:open
		})
	}

	openModal(){
		this.handlelModalClose(true)
	}
	closeModal(){
		this.handlelModalClose(false)
	}

	handleParentPincode(pincode){
		var pincodeRef = this.refs.parentsPincode;

		if(pincodeRef.isValid()===true){
			this.onValidParentPincode(pincode);	
		}else{
			AppActions.clearParentsPincode()
		}
	}

	handlePincodeChange(pincode){
		var pincodeRef = this.refs.pincode;

		if(pincodeRef.isValid()===true){
				this.onValidPincode(pincode);	
		}else{
			AppActions.clearPincode()
		}
	}

	onContinue(){
		AppActions.setContinue()
	}

	handlePincodeValiation(){
		var isPincodeValid = true
		var isParentsPincodeValid = true

		var pincode = this.refs.pincode;
		var parentsPincode = this.refs.parentsPincode;

			isPincodeValid = pincode.isValid();
			if(!isPincodeValid)
				pincode.showErrors()

			isParentsPincodeValid = (this.state.parentsSelected)? parentsPincode.isValid(): true;
			if(!isParentsPincodeValid)
				parentsPincode.showErrors()

		if(isPincodeValid && isParentsPincodeValid){
			this.closeModal()
		}
	}

	clearEmail(){
		var isValid = this.refs.optionalEmail.isValid()
		if(!isValid){
			AppActions.clearOptionalEmail()
		}
	}

	clearMobile(){
		var isValid = this.refs.optionalMobile.isValid()
		if(!isValid){
			AppActions.clearOptionalMobile()
		}
	}

	render(){
		var self = this
		var isGenderValid = ValidationStore.genderValidation()
		var memberValidation = this.getMemberValidation()
		var pincodeValidation = this.getPincodeValidation()
		var displayOptionalFields = this.getMemberValidation().valid && this.getPincodeValidation().valid
		var membersString = ''
		var PincodeModalBlock
		if(this.state.parentsSelected===true && this.state.membersApartFormParents===true){
			PincodeModalBlock = (
				<div>
					<div className='modal_header'>Please select your and your parents' pincode</div>
					<div className='pincode_fields'>
						<PincodeComponent name='pincode' ref='pincode' validations={ ['required'] } placeholder='Your Pincode' fieldName='pincode' defaultValue={ this.state.pincode } handlePincodeChange={ this.handlePincodeChange.bind(this) } />
						{
							this.state.parentsSelected===true?
							(
								
								<PincodeComponent name='parentsPincode' ref='parentsPincode' validations={ ['required'] } placeholder="Parents' Pincode" defaultValue={ this.state.parentsPincode } fieldName='pincode' handlePincodeChange={ this.handleParentPincode.bind(this) }  />
							):
							null
						}
						<Button title='DONE' customClass="w--button--orange"  onClick={ this.handlePincodeValiation.bind(this) } />
					</div>
				</div>
			)	
		}

		Object.keys(self.state.membersState).map(function(memberKey){
			if(self.state.membersState[memberKey].checked===true){

				if(self.state.membersState[memberKey].type=='kid'){
					var kidsToDisplay = (self.state.membersState[memberKey].count>1)? self.state.membersState[memberKey].count+' '+self.state.membersState[memberKey].displayName+'s' : self.state.membersState[memberKey].displayName

					membersString+=kidsToDisplay+', '
				}else{
					membersString+=self.state.membersState[memberKey].displayName+', '
				}
			}
		});

		membersString = membersString.slice(0,-2)

		return(
			<div>
				<div className="form-row w--health_form first">
					<div className="health_gender_select">

						<Select ref='gender' optionsText='label' dataSource={ Gender } optionsValue='value' values={this.state.gender} header="Select your gender" onChange={this.handelGenderChange.bind(this) } validations={['required']} placeHolder="Your Gender" labelText="Your Gender"  isSearchable={false} />

						{
							this.state.showErrors === true?
							(
								isGenderValid.valid === false ? <div className="w--error">{ isGenderValid. errorMessage }</div>:null
							) : null
						}
					</div>
					<div  className="health_member_select">
						<div className="w--member_select">
							<div className="member_select_handle" onClick={ this.handleMembersDisplay.bind(this,true) }>
							{
								this.state.onContinue===true && this.state.members.length>0?
								(
									<div>
										<div className="label_minimised">Members to be insured</div>
										{ membersString }
									</div>
								):
								(
									<div className="label_full">Who do you want to insure?</div>
								)
							}
								<span className="member_select_icon">+</span>
							</div>
							<div className='membersStateBlock'>
								<MembersSection membersState={ this.state.membersState } members={ this.state.members } onAddMember={ this.onAddMember.bind(this) } onRemoveMember={ this.onRemoveMember.bind(this) } onRemoveOneKid={ this.onRemoveOneKid.bind(this) } Step1ValidationStatus={ this.state.Step1Validation } onAgeChange={ this.onAgeChange.bind(this) } Step2Validation={ this.state.Step2Validation } parentsSelected={ this.state.parentsSelected } displayBlock={ this.state.displayMembersBlock } onStep2Done={ this.onStep2Done.bind(this) } onValidPincode={ this.onValidPincode.bind(this) } onValidParentPincode={ this.onValidParentPincode.bind(this) } pincode={ this.state.pincode } parentsPincode={ this.state.parentsPincode } onMemberModalClose={ this.onMemberModalClose.bind(this) } productType={ defaultProductType } onContinue={ this.onContinue.bind(this) } />
							</div>
						</div>
						{
							this.state.showErrors === true?
							(
								memberValidation.valid ===false?<div className="w--error">{ memberValidation.errorMessage }</div>:null
							):null
						}
					</div>
					<div className='health_pincode_dropdown'>
					{
						this.state.parentsSelected===true && this.state.membersApartFormParents==true?
						(
							<div>
								<div className='health_pincode_dropdown__handle' onClick={this.openModal.bind(this)}>
									{
										pincodeValidation.valid ===false?
										(
											<div className='placeholder'>Area Pincode</div>
										):
										(
											<div>
												<div className="label_minimised">Area Pincode</div>{ this.state.pincode }
												{ this.state.parentsPincode!=undefined && this.state.parentsSelected===true?<span><span className='muted'> | </span>{this.state.parentsPincode}</span>:null }
											</div>		
										)
									}
									<div className='health_pincode_dropdown__arrow'>+</div>
								</div>
								{
									this.state.openPincodeBlock===true?
									(
										<Modal isShowCloseIcon={true} isOpen={true}  onClose={this.closeModal.bind(this)} isCloseOnOutSideClick={true} children={ PincodeModalBlock } >
										</Modal>
									):
									null
								}
							</div>
						)
						:
						(
							<div className="health_pincode_field">
							{
								this.state.membersApartFormParents===true || this.state.members.length==0?
								(
									<PincodeComponent name='pincode' ref='pincode' validations={ ['required'] } placeholder='Your Pincode' fieldName='pincode' defaultValue={ this.state.pincode } handlePincodeChange={ this.handlePincodeChange.bind(this) } />
								)
								:
								(
									<PincodeComponent name='parentsPincode' ref='parentsPincode' validations={ ['required'] } placeholder="Parents' Pincode" defaultValue={ this.state.parentsPincode } fieldName='pincode' handlePincodeChange={ this.handleParentPincode.bind(this) }  />
								)
							}
							</div>
						)
					}

					{
					this.state.showErrors === true?
						(
							pincodeValidation.valid ===false?<div className="w--error">{ pincodeValidation.errorMessage }</div>:null
						):null
					}
					</div>
					{ 
						displayOptionalFields===true?
						(
							<div className="health_contact_fields">
								<div>
									<MobileComponent label="MOBILE NUMBER" placeholder='Mobile (optional)' value={ this.state.mobile } ref='optionalMobile' onValid={ this.handleOptionalMobile.bind(this) } validations={[{name:'mobile',message:'Please enter a valid mobile number'}]} onChange={ this.clearMobile.bind(this) } />
								</div>
								<div>
									<EmailComponent label="EMAIL ADDRESS" placeholder='Email (optional)' value={ this.state.email } ref='optionalEmail' onChange={ this.clearEmail.bind(this) } onValid={ this.handleOptionalEmail.bind(this) } validations={[{name:'email',message:'Please enter a valid email address'}]} />
								</div>
							</div>
						)
						:
						null

					}
				</div>
				<div className="form-row">
					<Button title='BUY NOW' onClick={this.onViewQuotes.bind(this)} customClass="w--button--orange w--button--large" type= { (this.state.buttonType)?'loading':'' }  />
				</div>
			</div>
		)	
	}
}

export = HealthIndemnity;
