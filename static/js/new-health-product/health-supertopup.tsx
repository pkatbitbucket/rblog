	/// <reference path="../typings/tsd.d.ts" />
import React = require('react')
import BaseComponent = require('../common/BaseComponent')
import TransitionUtils = require('../common/utils/TransitionUtils')

// All Components
import RadioInput = require('../common/new-components/RadioInput')
import EmailComponent = require('../common/new-components/EmailInput')
import MobileComponent = require('../common/new-components/MobileInput')
import Button = require('../common/react-component/Button')
import Select = require('../common/react-component/MultiSelect')
import MembersSection = require('./composite-components/BaseMembersComponent')
import Modal = require('../common/new-components/Modal')

// Actions
import AppActions = require('./actions/AppActions')

// Stores
import HealthStore = require('./stores/HealthStore')
import MembersStore = require('./stores/MembersStore')
import ValidationStore = require('./stores/ValidationStore')
import MemberValidation = require('./stores/MemberValidation')

// API
import APIUtils = require("./utils/APIUtils")
import GTMUtils = require("./utils/GTMUtils")

//	Constants
import SumAssured = require('./constants/SumAssured')
import Gender = require('./constants/Gender')
import ProductTypes = require('./constants/ProductTypes')
import Headings = require('../common/constants/Headings')
let defaultProductType = ProductTypes[1]['value'];
let defaultPincode = '421202';
let defaultParentsPincode = '421202';
let heading = Headings.HEALTH_SUPER_TOPUP

interface IHealthState {
	email: any;
	parentsPincode: number;
	parentsSelected: any;
	pincode: number;
	members: any;
	gender: string;
	mobile:any;
	showErrors:any;
	membersState:any;
	Step1Validation:any;
	Step2Validation:any;
	displayMembersBlock:any;
	sumAssured:any;
	sumAssuredValidation:any;
	productType:any;
	buttonType:boolean;
	onContinue:boolean;
}

function getStateFromStores(): any {
	return {
		gender: HealthStore.getGender(),
		members : MembersStore.getSelectedMembers(),
		parentsSelected: MembersStore.getParentsSelected(),
		pincode: MembersStore.getPincode(),
		parentsPincode: MembersStore.getParentsPincode(),
		email: HealthStore.getEmail(),
		mobile: HealthStore.getMobile(),
		membersState: MembersStore.getMembersState(),
		sumAssured : MembersStore.getSumAssured(),
		Step1Validation: MemberValidation.step1Validation(),
		Step2Validation: MemberValidation.validate(),
		sumAssuredValidation: ValidationStore.sumAssuredValidation(),
		onContinue: MembersStore.getOnContinue()
	}
}

const STORES = [HealthStore,MembersStore]

class Health extends BaseComponent {
	refs: any;
	state: IHealthState;
	constructor(props) {
		super(props, {
			stores:STORES,
			getStateFromStores: getStateFromStores
		});

		this.state.showErrors = false
		this.state.displayMembersBlock = false
		this.state.buttonType = undefined
	}

	componentWillMount(){
		// AppActions.setPincode(defaultPincode)
		// AppActions.setParentsPincode(defaultParentsPincode);
		AppActions.setProductType(defaultProductType)
	}

	handelGenderChange(){
		var gender = this.refs['gender'].getValues();
		AppActions.setGender(gender)	// Emit change prevented
	}

	onAddMember(memberCategory:any){
		AppActions.addMember(memberCategory)
	}

	onRemoveMember(memberCategory:any){
		AppActions.removeMember(memberCategory)
	}

	handleMembersDisplay(show:boolean){
		this.setState({
			displayMembersBlock:(show===true)? true:false
		})
	}

	onRemoveOneKid(kidType){
		AppActions.removeSingleKid(kidType)
	}

	onAgeChange(key,age){
		AppActions.setAge({ key:key,age:age })
	}

	onStep2Done(){
		this.handleMembersDisplay(false)
	}

	handleOptionalMobile(mobile){
		AppActions.setMobile(mobile);
	}

	handleOptionalEmail(email){
		AppActions.setEmail(email);
	}

	getMemberValidation(){
		var isValid = {
			valid:true,
			errorMessage:null,
		}

		// Members Block
		var Step1Validation = this.state.Step1Validation
		var Step2Validation = this.state.Step2Validation
		var pincode = ValidationStore.PincodeValidation()
		var parentsPincode =  ValidationStore.parentsPincodeValidation()

		if(Step1Validation.valid===false || Step2Validation.valid===false || pincode.valid===false || parentsPincode.valid===false){
			
			isValid.valid = false
			isValid.errorMessage = 'Please provide complete information of the members you would like to insure.'	
		}

		return isValid;
	}

	onViewQuotes(){
		var gender = this.refs.gender;
		
		var isGenderValid = ValidationStore.genderValidation()
		var memberValidation = this.getMemberValidation()
		var sumAssuredValidation = ValidationStore.sumAssuredValidation()

		if(isGenderValid.valid && memberValidation.valid && sumAssuredValidation.valid){

			// Optional Email
			var optionalEmail = this.refs.optionalEmail;
			if(!optionalEmail.isValid())
				optionalEmail.isValid(true)

			// Optional Mobile
			var optionalMobile = this.refs.optionalMobile;
			if(!optionalMobile.isValid())
				optionalMobile.isValid(true)

			if(!optionalEmail.isValid() || !optionalMobile.isValid()){
				this.setState({
					showErrors:true
				})
			}else{
				this.setState({
					showErrors:false,
					buttonType:'loading'
				},function(){
					
					GTMUtils.onBuy()
					AppActions.getActivityId()	
					TransitionUtils.jumpTo('/health-plan/#medical-conditions')
				})
			}

		}else{
			this.setState({
				showErrors:true
			})
		}

	}

	handelSumAssured(sumAssured){
		AppActions.setDeductible(sumAssured.value);
	}

	onMemberModalClose(){
		this.handleMembersDisplay(false);
	}

	onValidPincode(pincode){
		AppActions.setPincode(pincode);
	}

	onValidParentPincode(pincode){
		AppActions.setParentsPincode(pincode);
	}

	handleClose(){
		TransitionUtils.transitTo('/health-insurance/')
	}

	onContinue(){
		AppActions.setContinue()
	}

	clearEmail(){
		var isValid = this.refs.optionalEmail.isValid()
		if(!isValid){
			AppActions.clearOptionalEmail()
		}
	}

	clearMobile(){
		var isValid = this.refs.optionalMobile.isValid()
		if(!isValid){
			AppActions.clearOptionalMobile()
		}
	}

	render(){
		var self = this
		var isGenderValid = ValidationStore.genderValidation();
		var memberValidation = this.getMemberValidation();
		var sumAssuredValidation = ValidationStore.sumAssuredValidation()
		var membersString = ''

		Object.keys(self.state.membersState).map(function(memberKey){
			if(self.state.membersState[memberKey].checked===true){

				if(self.state.membersState[memberKey].type=='kid'){
					var kidsToDisplay = (self.state.membersState[memberKey].count>1)? self.state.membersState[memberKey].count+' '+self.state.membersState[memberKey].displayName+'s' : self.state.membersState[memberKey].displayName

					membersString+=kidsToDisplay+', '
				}else{
					membersString+=self.state.membersState[memberKey].displayName+', '
				}
			}
		});

		membersString = membersString.slice(0,-2)

		var mainBlock = (
			<div>
				<div className="form-row w--health_form first">
					<div className="health_gender_select">

						<Select ref='gender' optionsText='label' dataSource={ Gender } optionsValue='value' values={this.state.gender} header="Select your Gender" onChange={this.handelGenderChange.bind(this) } validations={['required']} placeHolder="Your Gender" labelText="Your Gender"  isSearchable={false} />

						{
							this.state.showErrors === true?
							(
								isGenderValid.valid === false ? <div className="w--error">{ isGenderValid. errorMessage }</div>:null
							) : null
						}
					</div>
					<div  className="health_member_select">
						<div className="w--member_select">
							<div className="member_select_handle" onClick={ this.handleMembersDisplay.bind(this,true) }>
							{
								this.state.onContinue===true && this.state.members.length>0?
								(
									<div>
										<div className="label_minimised">Members to be insured</div>
										{ membersString }
									</div>
								):
								(
									<div className="label_full">Who do you want to insure?</div>
								)
							}
								<span className="member_select_icon">+</span>
							</div>
							<div className='membersStateBlock'>
								<MembersSection membersState={ this.state.membersState } members={ this.state.members } onAddMember={ this.onAddMember.bind(this) } onRemoveMember={ this.onRemoveMember.bind(this) } onRemoveOneKid={ this.onRemoveOneKid.bind(this) } Step1ValidationStatus={ this.state.Step1Validation } onAgeChange={ this.onAgeChange.bind(this) } Step2Validation={ this.state.Step2Validation } parentsSelected={ this.state.parentsSelected } displayBlock={ this.state.displayMembersBlock } onStep2Done={ this.onStep2Done.bind(this) } onValidPincode={ this.onValidPincode.bind(this) } onValidParentPincode={ this.onValidParentPincode.bind(this) } pincode={ this.state.pincode } parentsPincode={ this.state.parentsPincode } onMemberModalClose={ this.onMemberModalClose.bind(this) } productType={ defaultProductType } onContinue={ this.onContinue.bind(this) } />

									{
									this.state.showErrors === true?
										(
											memberValidation.valid ===false?<div className="w--error">{ memberValidation.errorMessage }</div>:null
										):null
									}
							</div>
						</div>
					</div>
					<div className="health_supertopup_dropdown">
							<Select ref='sumAssured' header="Existing sum assured" footer="What amount are you covered for (sum assured) from existing policy?" optionsText='display' optionsValue='value' labelText="Existing sum assured" placeHolder='Select existing sum assured' validations={['required']} values={ this.state.sumAssured } dataSource={ SumAssured } isSearchable={ false } onChange={ this.handelSumAssured.bind(this) } />
						{
							this.state.showErrors === true && sumAssuredValidation.valid === false?
							(
								<div className="w--error">{ sumAssuredValidation.errorMessage }</div>
							):null
						}
					</div>
					{
						this.getMemberValidation().valid===true && ValidationStore.sumAssuredValidation().valid===true?
						(
							<div className="health_contact_fields">
								<div>
									<MobileComponent placeholder='Mobile (optional)' value={ this.state.mobile } ref='optionalMobile' onValid={ this.handleOptionalMobile.bind(this) } validations={[{name:'mobile',message:'Please enter a valid mobile number'}]} onChange={ this.clearMobile.bind(this) } />
								</div>
								<div>
									<EmailComponent placeholder='Email (optional)' value={ this.state.email } ref='optionalEmail' onValid={ this.handleOptionalEmail.bind(this) } validations={[{name:'email',message:'Please enter a valid email address'}]} onChange={ this.clearEmail.bind(this) }  />
								</div>
							</div>
						)
						:
						null

					}
				</div>
				<div className="form-row">
					<Button title='BUY NOW' onClick={this.onViewQuotes.bind(this)} customClass="w--button--orange w--button--large" type= { (this.state.buttonType)?'loading':'' }  />
				</div>
			</div>
		)

		return (
			<Modal customClass="full_bg" header={heading} ref='MemberPopup' children={mainBlock} isOpen={true} isShowCloseIcon={true} size='large' onClose={ this.handleClose.bind(this) } customHeaderClass="modal_header--hero"/>  
		)
	}
}

export = Health;