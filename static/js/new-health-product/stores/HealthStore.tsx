import Cookie = require('react-cookie')

import BaseStore = require('../../common/BaseStore')
import MembersStore = require('./MembersStore')

// Constants
import ActionEnum = require('../constants/ActionEnum')
import StorageMap = require('../constants/StorageMap')
import ProductTypes = require('../constants/ProductTypes')

import AppActions = require('../actions/AppActions')

import APIUtils = require('../utils/APIUtils')

var KEYS = StorageMap.HEALTH;

const HEALTH_MOBILE_COOKIE = 'health_mobile_cookie';

class HealthStore extends BaseStore{
	private _diseases: any[];

	constructor(_namespace:string){
		super(_namespace)
		this.registerDispatcherHandler(this._actionHandler)
		this.init(true)
	}

	private init(preventEmit?:boolean){
		let obj = {}
		obj[KEYS.PRODUCT_TYPE] = this.getSelected(KEYS.PRODUCT_TYPE) || ProductTypes[0]['value']
		localStorage.setItem('product_type', JSON.stringify(obj[KEYS.PRODUCT_TYPE]))

		obj[KEYS.GENDER] = this.getSelected(KEYS.GENDER) || undefined;
		localStorage.setItem('gender', JSON.stringify(obj[KEYS.GENDER]))

		obj[KEYS.MEDICAL_CONDITIONS] = this.getSelected(KEYS.MEDICAL_CONDITIONS) || [];
		this.setSelected(obj, preventEmit)
	}

	// Getter Functions

	getProductType(){
		return this.getSelected(KEYS.PRODUCT_TYPE);
	}

	getGender(){
		return this.getSelected(KEYS.GENDER);
	}

	getEmail(){
		return this.getSelected(KEYS.EMAIL);
	}

	getMobile(){
		return this.getSelected(KEYS.MOBILE);
	}

	getMedicalConditions(){
		return this.getSelected(KEYS.MEDICAL_CONDITIONS)
	}

	getDiseaseToDeclareFlag(){
		return this.getSelected(KEYS.DISEASE_TO_DECLARE_FLAG)
	}

	getAllData(){
		return this.getSelected();
	}

	getAllDiseases(){
		return this._diseases;
	}

	// Setter Functions

	_setProductType(productType: string){
		localStorage.setItem('product_type', productType)
		this.setSelectedValue(KEYS.PRODUCT_TYPE,productType)
	}

	_setGender(gender: string){
		localStorage.setItem('gender', JSON.stringify(gender))
		this.setSelectedValue(KEYS.GENDER,gender,true)
		MembersStore.updateMembersOnGenderUpdate(gender);
	}


	_setMedicalConditions(diseases:any){
		this.setSelectedValue(KEYS.MEDICAL_CONDITIONS, diseases)
	}

	_setDiseaseToDeclareFlag(flag:any){
		if(flag == 'no'){
			this.setSelectedValue(KEYS.DISEASE_TO_DECLARE_FLAG, flag, true)
			this._setMedicalConditions([])		// reset medial conditions to []
		}else{
			this.setSelectedValue(KEYS.DISEASE_TO_DECLARE_FLAG, flag)
		}
	}

	_setDiseases(diseases:any){
		this._diseases = diseases;
		this.emitChange()
	}

	_setEmail(email:any){
		this.setSelectedValue(KEYS.EMAIL,email);
	}

	_setMobile(mobile:any){
		var userHealthData = this.getAllData();
		userHealthData.mobile = mobile;

		userHealthData['members_state']=undefined

		APIUtils.sendToSaveCallTime(userHealthData);
		Cookie.save(HEALTH_MOBILE_COOKIE,mobile);
		this.setSelectedValue(KEYS.MOBILE, mobile)
	}

	_clearOptionalEmail(){
		this.clearSelected(KEYS.EMAIL)
	}

	_clearOptionalMobile(){
		this.clearSelected(KEYS.MOBILE)
	}

	// To Get Transaction Id
	_getActivityId(){
		var userHealthData = this.getAllData();

		if(this.getProductType()=='SUPER_TOPUP'){
			localStorage.setItem('pincode', '421202')
			localStorage.setItem('parents_pincode', '421202')
		}else{

			var membersApartFormParents = MembersStore.membersApartFormParents()

			if(membersApartFormParents===true){
				localStorage.setItem('pincode', userHealthData.pincode)
				if(userHealthData.parents_selected===true)
					localStorage.setItem('parents_pincode', userHealthData.parents_pincode)
			}else{
				localStorage.setItem('pincode', userHealthData.parents_pincode)
				localStorage.setItem('parents_pincode', userHealthData.parents_pincode)
			}
		}
		// For sending email to leads/save-call-time API
		// APIUtils.sendToSaveCallTime(userHealthData);

		// APIUtils.getActivityId(userHealthData);
	}

 	private _actionHandler:Function = (action:any) => {
		 switch(action.actionType) {
		 	// add actions here
			case ActionEnum.SET_PRODUCT_TYPE:
				this._setProductType(action.value)
				break

			case ActionEnum.SET_GENDER:
				this._setGender(action.value)
				break

			case ActionEnum.SET_EMAIL:
				this._setEmail(action.value)
				break

			case ActionEnum.SET_MEDICAL_CONDITIONS:
				this._setMedicalConditions(action.value)
				break

			case ActionEnum.SET_DISEASE_TO_DECLARE_FLAG:
				this._setDiseaseToDeclareFlag(action.value)
				break

			case ActionEnum.GET_ACTIVITY_ID:
				this._getActivityId()
				break

			case ActionEnum.SET_DISEASES:
				this._setDiseases(action.value)
				break

			case ActionEnum.SET_MOBILE:
				this._setMobile(action.value)
				break

			case ActionEnum.CLEAR_OPTIONAL_EMAIL:
				this._clearOptionalEmail()
				break

			case ActionEnum.CLEAR_OPTIONAL_MOBILE:
				this._clearOptionalMobile()
				break
		}
	}
}

var healthStore: HealthStore;

export = (function(){
	if(!healthStore){
		healthStore = new HealthStore(KEYS.PARENT)
		// API calls to initiate data, if any

		// APIUtils.getAllMedicalConditions(AppActions.setAllMedicalConditions);
	}
	return healthStore
}())