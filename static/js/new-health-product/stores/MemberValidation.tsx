import BaseStore = require('../../common/BaseStore')

import ActionEnum = require('../constants/ActionEnum')
import StorageMap = require('../constants/StorageMap')

import APIUtils = require('../utils/APIUtils')

import HealthStore = require('./HealthStore')
import MembersStore = require('./MembersStore')


var KEYS = StorageMap.HEALTH;

class MemberValidation extends BaseStore{

	private _privateField: any[];

	constructor(_namespace:string){
		super(_namespace)
	}

	step1Validation(){
		var self = this
		var validationResult: any = { valid:true, errorMessage:null }
		var selectedMembers = MembersStore.getSelectedMembers()
		var isMemberSelected = this.isMemberSelected(selectedMembers);

		if(isMemberSelected){	
			var adultSelected = this.isAdultSelected(selectedMembers);

			if(adultSelected){

				var kids = [], parents = [], grandParents = [];

				selectedMembers.map(function(member){
					if(member.type == 'kid'){
						kids.push(member.value)
					}else{
						// Case of Grand Parents
						if(member.key=='father' || member.key=='mother'){
							grandParents.push(member.value)
						}else{
							// Case of Parents
							parents.push(member.value);
						}
					}
				});

				
				if(kids.length>0){
					if(parents.length==0){
						validationResult.valid = false
						validationResult.errorMessage = 'Please select at least 1 parent to be insured.'
					}
				}				

			}else{
				validationResult.valid = false
				validationResult.errorMessage = 'The policy should include atleast 1 adult member.'
			}

		}else{
			validationResult.valid = false
			validationResult.errorMessage = 'Please select atleast 1 member to be insured.'
		}

		return validationResult;
	}

	// Member Validation

	validate(){
		var self = this
		var validationResult: any = { valid:false, errorMessage:null }
		var selectedMembers = MembersStore.getSelectedMembers()


		var isEveryMemberValid = this.isEveryMemberValid(selectedMembers);
		// Every Member should be self validated
		if(isEveryMemberValid){

			var isMemberSelected = this.isMemberSelected(selectedMembers);

			if(isMemberSelected){

				var adultSelected = this.isAdultSelected(selectedMembers);
				
				// Atleast 1 Adult should be selected
				if(adultSelected){

					var ageDifferenceValidation = this.ageDifferenceValidation(selectedMembers);
					if(ageDifferenceValidation.valid === true){
						validationResult.valid = true
						validationResult.errorMessage = null
					}else{
						validationResult.valid = false
						validationResult.errorMessage = ageDifferenceValidation.errorMessage;
					}

				}else{
					validationResult.valid = false
					validationResult.errorMessage = 'The policy should include atleast 1 adult member.'
				}
			}else{
				validationResult.valid = false
				validationResult.errorMessage = 'Please select atleast 1 member to be insured.'
			}

			// Set flag to show group error
			validationResult.errorType='group';

		}else{
			validationResult.valid = false;
			validationResult.errorType='individual';	// Set flag to show individual error
			validationResult.errorMessage = null;
		}

		return validationResult;

	}

	isMemberSelected(selectedMembers){
		return (selectedMembers.length>0)? true : false
	}

	isAdultSelected(selectedMembers){
		var isSelected = false;
		
		selectedMembers.map(function(element){
			if(!isSelected){
				isSelected = (element.type == 'adult')? true:false;
			}
		});

		return isSelected;
	}

	isEveryMemberValid(members){
		var isValid = members.every(function(element,index) {
			var isValid = true;

			if(element.checked === true){
				var membersAge = element.value;

				if(membersAge!= undefined && membersAge!=null){
					var age = parseInt(membersAge);

					isValid = (age>=0)? true : false;
				}else{
					isValid = false
				}
			}

			return (isValid === true);
		});

		return isValid;
	}

	ageDifferenceValidation(selectedMembers){
		var kids = [], parents = [], grandParents = [];

		selectedMembers.map(function(member){
			if(member.type == 'kid'){
				kids.push(member.value)
			}else{
				// Case of Grand Parents
				if(member.key=='father' || member.key=='mother'){
					grandParents.push(member.value)
				}else{
					// Case of Parents
					parents.push(member.value);
				}
			}
		});


		// CASE where you were selected
		if(parents.length>0){

			// You and Grand Parents
			if(grandParents.length>0){
				var youngerThanGrandParents = true;

				parents.forEach(function(parent){
					grandParents.forEach(function(grandParent){
						if(youngerThanGrandParents){
							youngerThanGrandParents = (grandParent-parent >= 18)? true : false
						}
					})
				})

				if(youngerThanGrandParents == false)
					return { valid: false, errorMessage: 'Your parents should be at least 18 years older to you and your spouse.' }
			}

			// You and Kids
			if(kids.length>0){

				var elderThanKids = true;

				parents.forEach(function(parent){
					kids.forEach(function(kid){
						if(kid>11){
							var kidAgeInYears = kid/12;

							if(elderThanKids)
								elderThanKids = (parent-kidAgeInYears >= 18)? true : false
						}
					})
				})

				if(elderThanKids===false)
					return { valid: false, errorMessage: 'Your children should be at least 18 years younger to you and your spouse.' }

			}

			// If only Parents is selected and everything is Valid
			return {valid:true, errorMessage: null};
		}



		// CASE where grandParents and Kids were selected
		// if(grandParents.length>0){
		// 	var youngerkids = true

		// 	if(kids.length>0){
		// 		grandParents.forEach(function(grandParent){
		// 			kids.forEach(function(kid){
		// 				if(kid>11){
		// 					var kidAgeInYears = kid/12;
		// 					if(youngerkids)
		// 						youngerkids = (grandParent-kidAgeInYears > 36)? true : false
		// 				}
		// 			})
		// 		})
		// 	}

		// 	return { valid:youngerkids, errorMessage:'Age gap between father OR mother and kids should be greater than 36years' };

		// }

		if(kids.length>0){
			if(parents.length==0){
				return { valid: false, errorMessage: 'Please select atleast 1 member to be insured.' }
			}
		}

		// CASE if only GrandParents is selected
		return {valid:true, errorMessage: null};

	}

 	private _actionHandler:Function = (action:any) => {
		 switch(action.actionType) {
		 	// add actions here
			
			// case ActionEnum.SET_MEMBERS:
			// 	this.setMembers(action.value)
			// 	break
				
		}
	}
}

var memberValidation: MemberValidation;

export = (function(){
	if(!memberValidation){
		memberValidation = new MemberValidation(KEYS.PARENT)
		// API calls to initiate data, if any
	}
	return memberValidation
}())