import BaseStore = require('../../common/BaseStore')

import ActionEnum = require('../constants/ActionEnum')
import StorageMap = require('../constants/StorageMap')


import MemberCategories = require('../constants/MemberCategories')

import APIUtils = require('../utils/APIUtils')

interface INewMember{
	id: string;
	personAliasAlias: string;
	displayName: string;
	age: string|number;
	checked: boolean;
	type: string;
	key: any;
	refKey: string;
	value: any;
	age_in_months: any;
} 

var KEYS = StorageMap.HEALTH;

class MembersStore extends BaseStore{

	private _privateField: any[];

	constructor(_namespace:string){
		super(_namespace)
		this.registerDispatcherHandler(this._actionHandler)
		this.init(true)
	}

	private init(preventEmit?:boolean){
		let obj = {}
		let memberCategories = JSON.parse(JSON.stringify(MemberCategories))
			memberCategories['you'].checked = true
		let member = []
		let defaultMember = memberCategories['you']
			member.push(defaultMember)

		// Case where GENDER is selected
		let gender =  this.getSelected(KEYS.GENDER)
			if(gender!=undefined){
				memberCategories['spouse'].displayName = (gender=='MALE')? 'Wife':'Husband'
			}

		obj[KEYS.MEMBERS.PARENT] = this.getSelectedMembers() || member
		this.handelMembersSoloStorage(obj[KEYS.MEMBERS.PARENT])

		obj[KEYS.MEMBERS_STATE] = this.getMembersState() || memberCategories
		obj[KEYS.PARENTS_SELECTED] = this.getParentsSelected() || false
		obj[KEYS.CONTINUE] = this.getOnContinue() || false

		this.setSelected(obj, preventEmit)
	}

	getMembersState(){
		return (this.getSelected(KEYS.MEMBERS_STATE))? JSON.parse(JSON.stringify(this.getSelected(KEYS.MEMBERS_STATE))) : undefined
	}

	getSelectedMembers(){
		return (this.getSelected(KEYS.MEMBERS.PARENT))? JSON.parse(JSON.stringify(this.getSelected(KEYS.MEMBERS.PARENT))) : undefined
	}

	getPincode(){
		return this.getSelected(KEYS.PINCODE);
	}

	getParentsPincode(){
		return this.getSelected(KEYS.PARENTS_PINCODE)
	}

	getSumAssured(){
		return this.getSelected(KEYS.DEDUCTIBLE)
	}

	getParentsSelected(){
		return this.getSelected(KEYS.PARENTS_SELECTED);
	}

	membersApartFormParents(){
		var selectedMembers = this.getSelectedMembers()
		var isParentsSelected = this.getParentsSelected()

		var membersApartFormParents = false
		selectedMembers.map(function(member){
			if(member.key=='father' || member.key=='mother'){
				// do nothing
			}else{
				membersApartFormParents = true
			}
		})

		return membersApartFormParents;
	}


	getOnContinue(){
		return this.getSelected(KEYS.CONTINUE)
	}

	// Setter Functions

	_setOnContinue(){
		return this.setSelectedValue(KEYS.CONTINUE,true)
	}

	_setMembersState(allMembers:any){
		let memberCategories = JSON.parse(JSON.stringify(MemberCategories))
		let gender =  this.getSelected(KEYS.GENDER)

		// Set Spouse displayName
		if(memberCategories['spouse'] && gender!=undefined){
			memberCategories['spouse'].displayName = (gender=='MALE')? 'Wife':'Husband'
		}

		allMembers.map(function(element){

			if(memberCategories[element.personAlias]){
				memberCategories[element.personAlias].checked = true
				memberCategories[element.personAlias].count = memberCategories[element.personAlias].count+1
			}

		})

		this.setSelectedValue(KEYS.MEMBERS_STATE,JSON.parse(JSON.stringify(memberCategories)))
	}

	handelMembersSoloStorage(selectedMembers){
		var members = []
		selectedMembers.map(function(member){
			let obj:any = {}
				obj.person = member.person
				obj.age=member.age+'-'+member.age_in_months

				members.push(obj)
		});
		localStorage.setItem('members', JSON.stringify(members))
	}

	_setSelectedMembers(selectedMembers: any){

		var members=[]
		this.handelMembersSoloStorage(selectedMembers)
		this.setSelectedValue(KEYS.MEMBERS.PARENT,JSON.parse(JSON.stringify(selectedMembers)), true);
		this._setParentsSelected(selectedMembers)	// Incase parents are selected

		this._setMembersState(selectedMembers)
	}

	_setDeductible(deductible: any){

		if(deductible===undefined){
			this.clearSelected(KEYS.DEDUCTIBLE);
		}else{
			localStorage.setItem('deductible', JSON.stringify(deductible))
			this.setSelectedValue(KEYS.DEDUCTIBLE,deductible);
		}
	}

	_setPincode(pincode:any){
		localStorage.setItem('pincode', JSON.stringify(pincode))
		this.setSelectedValue(KEYS.PINCODE,pincode);
	}

	clearPincode(){
		this.clearSelected(KEYS.PINCODE,true);
	}

	clearParentsPincode(){
		this.clearSelected(KEYS.PARENTS_PINCODE,true);
	}

	_setParentsPincode(pincode:any){
		localStorage.setItem('parents_pincode', JSON.stringify(pincode))
		this.setSelectedValue(KEYS.PARENTS_PINCODE,pincode);
	}

	_setParentsSelected(selectedMembers){
		// Change emit prevented - will get fire by "setMembers" function
		let selectionFlag = false

		selectedMembers.map(function(member) {
			if(member.personAlias=='father' || member.personAlias=='mother'){
				selectionFlag = true
			}
		})

		this.setSelectedValue(KEYS.PARENTS_SELECTED,selectionFlag, true);
	}

	_setAge(keyAgeObj){
		var self = this;
		var key = keyAgeObj.key
		var age = keyAgeObj.age
		var members = this.getSelectedMembers();

		var updatedMembers = members.map(function(member){
			if(member.key == key){

				// And if validation is true
				member.value = age;

				/*
					If ADULT = populate "age" 
						OR 
					if KID = populate "age In Months"
				*/
				if(member.type == 'adult'){
					member.age = age
					member.age_in_months = 0
				}else{
					if(age <=11){
						member.age=0;
						member.age_in_months = age;
					}else{
						var selectedValueInYears = (age/12);	// converting months into years

						member.age=selectedValueInYears
						member.age_in_months = 0
					}
					
				}
			}
			return member
		})

		this._setSelectedMembers(updatedMembers)
	}

	updateMembersOnGenderUpdate(gender){
		var self = this;
		var members = this.getSelectedMembers();

		var updatedMembers = members.map(function(member){
			if(member.key == 'spouse'){
				member.displayName = (gender == 'MALE')? 'Wife' : 'Husband';
			}

			return member
		})

		this._setSelectedMembers(updatedMembers)
	}

	addMember(memberType:string){

		var self = this;
		var members = this.getSelectedMembers();

		var newMember: INewMember;
		var newMemberKey = 	Date.now();	//this.getKidKey(kidType);
		var memberCategories = JSON.parse(JSON.stringify(MemberCategories));

			newMember = memberCategories[memberType]
			newMember.checked = true;

		if(newMember.type == 'kid'){
			newMember.key = newMemberKey;			
		}

		members.push(newMember);

		this._setOnContinue()
		this._setSelectedMembers(members);
	}


	removeMember(memberState){

		var membersState = this.getSelectedMembers();

		var indexesToRemove = membersState.map(function(element, index){

			if(element.personAlias == memberState){
				return index;
			}
		})
		
		indexesToRemove.reverse().forEach(function(index){
			if(index!=undefined)
				membersState.splice(index,1);
		})
		
		this._setOnContinue()
		this._setSelectedMembers(membersState);
	}

	removeSingleKid(kidType){
		var membersState = this.getSelectedMembers()
		var indexFound = false
		var indexToRemove = undefined

		for(var i=membersState.length-1; i>=0; i--){
			
			if(membersState[i].personAlias == kidType && indexFound===false){
				indexFound = true
				indexToRemove = i
			}
		}

		if(indexToRemove!=undefined)
			membersState.splice(indexToRemove,1);

		this._setSelectedMembers(membersState);
	}

 	private _actionHandler:Function = (action:any) => {
		 switch(action.actionType) {
		 	// add actions here
			
			case ActionEnum.SET_SELECTED_MEMBERS:
				this._setSelectedMembers(action.value)
				break

			case ActionEnum.SET_DEDUCTIBLE:
				this._setDeductible(action.value)
				break

			case ActionEnum.SET_PINCODE:
				this._setPincode(action.value)
				break

			case ActionEnum.SET_PARENTS_PINCODE:
				this._setParentsPincode(action.value)
				break

			case ActionEnum.SET_PARENTS_SELECTED:
				this._setParentsSelected(action.value)
				break

			case ActionEnum.ADD_MEMBER:
				this.addMember(action.value)
				break

			case ActionEnum.REMOVE_MEMBER:
				this.removeMember(action.value)
				break

			case ActionEnum.REMOVE_ONE_KID:
				this.removeSingleKid(action.value)
				break	

			case ActionEnum.SET_AGE:
				this._setAge(action.value)
				break		

			case ActionEnum.CLEAR_PINCODE:
				this.clearPincode()
				break		

			case ActionEnum.CLEAR_PARENTS_PINCODE:
				this.clearParentsPincode()
				break		

			case ActionEnum.SET_CONTINUE:
				this._setOnContinue()
				break
		}
	}
}

var membersStore: MembersStore;

export = (function(){
	if(!membersStore){
		membersStore = new MembersStore(KEYS.PARENT)
		// API calls to initiate data, if any
	}
	return membersStore
}())