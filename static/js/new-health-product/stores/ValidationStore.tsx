import BaseStore = require('../../common/BaseStore')

import ActionEnum = require('../constants/ActionEnum')
import StorageMap = require('../constants/StorageMap')

import APIUtils = require('../utils/APIUtils')

import HealthStore = require('./HealthStore')
import MembersStore = require('./MembersStore')


var KEYS = StorageMap.HEALTH;

class ValidationStore extends BaseStore{

	private _privateField: any[];

	constructor(_namespace:string){
		super(_namespace)
	}


	productTypeValidation(){
		var productionValidtion = { valid: false, errorMessage: 'Please select Product Type' }
		if(HealthStore.getProductType())
			productionValidtion.valid = true;

		return productionValidtion;
	}

	genderValidation(){
		var genderValidtion = { valid: false, errorMessage: 'Please select your gender.' }
		if(HealthStore.getGender())
			genderValidtion.valid = true;

		return genderValidtion;	
	}

	PincodeValidation(){
		var productType = HealthStore.getProductType();
		var membersApartFormParents = MembersStore.membersApartFormParents()
		var pincode = { valid: false, errorMessage: 'Please enter the area pincode.' }

		if(productType == 'SUPER_TOPUP'){
			pincode.valid = true
		}else{
			if(membersApartFormParents===true){
				if(MembersStore.getPincode())
					pincode.valid = true;
			}else{
				pincode.valid = true;
			}
		}

		return pincode;	
	}

	parentsPincodeValidation(){
		var productType = HealthStore.getProductType();
		var pincode = { valid: false, errorMessage: 'Please enter the area pincode.' }

		if(MembersStore.getParentsSelected() && productType != 'SUPER_TOPUP'){

			if(MembersStore.getParentsPincode())
				pincode.valid = true;

		}else{
			pincode.valid = true;
		}

		return pincode;	
	}

	sumAssuredValidation(){
		var productType = HealthStore.getProductType();

		var validationResult = { valid: false, errorMessage: 'Please select the existing sum assured.' };

		if(productType == 'SUPER_TOPUP'){

			if(MembersStore.getSumAssured())
				validationResult.valid = true

		}else{
			validationResult.valid = true
		}

		return validationResult;
	}

	// Optional email
	emailValidation(){
		var emailValid = { valid: true, errorMessage: 'Please enter your email address.' }

		if(HealthStore.getEmail())
			emailValid.valid = true

		return emailValid;
	}

	diseasesToDeclare(){
		var diseaseToDeclareValid = { valid: true, errorMessage:'Please select one option to proceed.' }
		var diseaseToDeclareFlag = HealthStore.getDiseaseToDeclareFlag();
				
		if(diseaseToDeclareFlag === undefined)
			diseaseToDeclareValid.valid = false;
		
		return diseaseToDeclareValid;
	}

	medicalConditionValidation(){
		var medicalConditionValid = { valid: true, errorMessage:'Please select atleast one disease to declare.' }
		var diseaseToDeclareFlag = HealthStore.getDiseaseToDeclareFlag();
		
		if(diseaseToDeclareFlag == 'yes'){
			var medicalConditions = HealthStore.getMedicalConditions();
			
			if(medicalConditions.length==0)
				medicalConditionValid.valid = false;

		}

		return medicalConditionValid;
	}






	// Member Validation

 	private _actionHandler:Function = (action:any) => {
		 switch(action.actionType) {
		 	// add actions here
			
			// case ActionEnum.SET_MEMBERS:
			// 	this.setMembers(action.value)
			// 	break
				
		}
	}
}

var validationStore: ValidationStore;

export = (function(){
	if(!validationStore){
		validationStore = new ValidationStore(KEYS.PARENT)
		// API calls to initiate data, if any
	}
	return validationStore
}())