
import Member = require("../stores/MembersStore");
import Health = require("../stores/HealthStore");


function ga(data){
    if (!window['dataLayer'] || typeof data !== 'object')
        return
    window['dataLayer'].push(data);
}

class GTMUtils{


    onBuy(){
        var  members = Member.getSelectedMembers()
                var membersArray = members.map(function(member){
                    return member.age+'-'+member.age_in_months
                })
        ga({
            'event': 'HomeTopHealthEventGetQuote',
            'category': 'Homepage Top Health - Get Quote',
            'action': Health.getGender(),
            'label': '',
            'param1':membersArray.join('||')
        })
    }
}

var gtmUtils: GTMUtils;

export = (function(){
	if (!gtmUtils)
		gtmUtils = new GTMUtils()
	return gtmUtils
}())