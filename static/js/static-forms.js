function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validatePhone(phone) {
    var re = /^[7-9][\d]{9}$/;
    return re.test(phone);
}

function validateName(name){
    var re = /^[a-z]+$/i;
    return re.test(name);
}

window.getCookie = function(name) {
  match = document.cookie.match(new RegExp(name + '=([^;]+)'));
  if (match) return match[1];
}

var gtm_push = function(gtm_event, gtm_category, gtm_action, gtm_label, gtm_extra_params){
    var data = {
        "event":gtm_event,
        "eventCategory":gtm_category,
        "eventAction":gtm_action,
        "eventLabel":gtm_label,
    }
    for(var param in gtm_extra_params){
        data[param] = gtm_extra_params[param]
    }

    dataLayer.push(data)
}


function sendDataToLMS(data, label, mobile, extras, callback) {
    try {
        var jsonData = {};
        jsonData['campaign'] = 'blog';
        jsonData['label'] = label;
        jsonData['mobile'] = mobile;
        jsonData['device'] = 'Desktop';
        jsonData['triggered_page'] = window.location.href;
        jsonData['data'] = data;

        for (var key in extras) {
            jsonData[key] = extras[key];
        }

        $.post('/leads/save-call-time/', {
            'data': JSON.stringify(jsonData),
            'csrfmiddlewaretoken': CSRF_TOKEN
        }, function(response) {
            callback();
            console.log("LMS data sent.");
        });
    } catch (e) {
        console.log(e, "Error sending data to LMS");
    }
}


$('.phone-number-form').submit(function(e){
    var form = $(e.target);
    var phone = form.find('input[name=phone]')[0].value;
    var lmsData = {};
    if(validatePhone(phone)){
        if(form.find('input[name=name]')[0]){
            var name = form.find('input[name=name]')[0].value;
            if(validateName(name)){
                lmsData.name = name;
            }
            else{
                form.find('.err').show()
                form.find('.success').hide()
                return false;
            }
        }
        form.find('.err').hide()
        sendDataToLMS(lmsData, 'blog', phone, {page_type: PAGE_TYPE}, function(){
            form.find('.success').show()
        })
        var gtm_category = getCookie('mark_product_category') || '';
        gtm_push('GAEvent', gtm_category, 'CallScheduled', PAGE_TYPE);
        form.find('.input-text, .btn').hide();
    }
    else{
        form.find('.err').show()
        form.find('.success').hide()
    }
    e.preventDefault();
});


$('.email-form').submit(function(e){
    var form = $(e.target);
    var name = form.find('input[name=name]')[0].value;
    var email = form.find('input[name=email]')[0].value;

    if(validateEmail(email) && name){
        form.find('.err').hide()
        sendDataToLMS({name:name}, 'blog', '', {email:email, page_type: PAGE_TYPE}, function(){
            form.find('.success').show()
        })
        var gtm_category = getCookie('mark_product_category') || '';
        gtm_push('GAEvent', gtm_category, 'EmailSubmit', PAGE_TYPE);
        form.find('.input-text, .btn').hide();
    }
    else{
        form.find('.err').show()
        form.find('.success').hide()
    }

    e.preventDefault();
});

$(document).ready(function(){
    $("#rs-fixed-widget").removeClass("reveal");
    var scrollLock = false;
    $(window).scroll(function () { 
        if(scrollLock == false)
        {
            if ($(window).scrollTop() > $('body').height() / 2) {
                $('#rs-fixed-widget').addClass("reveal");
            }
        }
        else 
        {
            $('#rs-fixed-widget').removeClass("reveal");
        } 
    });
    $("#close-subscribe").click(function(e){
        $('#rs-fixed-widget').removeClass("reveal");
        scrollLock = true;
    });
});
