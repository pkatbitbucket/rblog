import React = require('react')
import ReactDOM = require('react-dom')
import moment = require('moment')
import ReactRouter = require('react-router')
import history = require('./common/History')
import CommonUtils = require('./common/utils/CommonUtils')

import listensToClickOutside = require('./common/react-component/Dependencies/OnClickHandler')
import helpers = require('./common/react-component/utils/HelperFunctions')
import isEqual = require('lodash.isequal')
import Validations = require('./common/new-components/Validations')

import Widget = require('./widget/components/Widget')


import URLS = require('./common/constants/URLS')
import Car = require('./motor-product/landing-car')
import BikeQF = require('./bike-product/components/BikeQuoteForm')
import BikeFallBack = require('./bike-product/components/BikeFallBackQuoteForm')
import BikeRP = require('./bike-product/components/BikeResults')
import BikePF = require('./bike-product/components/BikeProposal')
import Health_Indemnity = require('./new-health-product/health-indemnity')
import Health_SuperTopup = require('./new-health-product/health-supertopup')
import Travel_SingleTrip = require('./travel-product/travel-singletrip')
import Travel_MultiTrip = require('./travel-product/travel-multitrip')
import TermLife = require('./life-product/term-life')

CommonUtils.loadCustomUtils()

var RR: any = ReactRouter
var Router = RR.Router
var Route = RR.Route
var Redirect = RR.Redirect
var IndexRedirect = RR.IndexRedirect


var WidgetRouter = (
	<Router history={history}>
		<Route path="/home/" component={Widget}>
			<IndexRedirect to={URLS.CAR_LANDING} />
			<Route path={URLS.CAR_LANDING} component={Car}></Route>
			<Route path={URLS.BIKE_LANDING} component={BikeQF}></Route>
			<Route path={URLS.BIKE_FALLBACK} component={BikeFallBack}></Route>
			<Route path={URLS.BIKE_RESULTS + "/:quoteId"} component={BikeRP}></Route>
			<Route path={URLS.BIKE_PROPOSAL + "/:slug/:transactionId"} component={BikePF}></Route>
			<Route path={URLS.TRAVEL_SINGLETRIP} component={Travel_SingleTrip}></Route>
			<Route path={URLS.TRAVEL_MULTITRIP} component={Travel_MultiTrip}></Route>
			<Route path={URLS.HEALTH_INDEMNITY} component={Health_Indemnity}></Route>
			<Route path={URLS.HEALTH_SUPERTOPUP} component={Health_SuperTopup}></Route>
			<Route path={URLS.LIFE_LANDING} component={TermLife}></Route>
		</Route>
		<Redirect from="/" to={URLS.CAR_LANDING} />
		<Redirect from={URLS.BIKE_RESULTS} to={URLS.BIKE_LANDING} />
		<Redirect from={URLS.BIKE_PROPOSAL} to={URLS.BIKE_LANDING} />
	</Router>
)

ReactDOM.render(WidgetRouter, document.getElementById('content'))
