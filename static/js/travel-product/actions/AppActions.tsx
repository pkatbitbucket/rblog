import AppDispatcher = require('../../common/Dispatcher')
import ActionEnum = require('../constants/ActionEnum')

export = {
	setTripType: function(value:string, callback?:Function){
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_TRIP_TYPE,
			value:value,
			callback:callback
		})
	},
	setSelectedCountries: function(value: any, callback?: Function) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_SELECTED_COUNTRIES,
			value: value,
			callback: callback
		})
	},
	setSelectedRegion: function(value: any, callback?: Function) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_SELECTED_REGION,
			value: value,
			callback: callback
		})
	},
	setstartDate: function(value: any, callback?: Function) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_START_DATE,
			value: value,
			callback: callback
		})
	},
	setEndDate: function(value: any, callback?: Function) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_END_DATE,
			value: value,
			callback: callback
		})
	},
	setNoOfTravellers: function(value: any, callback?: Function) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_NO_OF_TRAVELLERS,
			value: value,
			callback: callback
		})
	},
	setLongestTripDuration: function(value: any, callback?: Function) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_LONGEST_TRIP_DURATION,
			value: value,
			callback: callback
		})
	},
	setTravellerAges: function(value: any, callback?: Function) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_TRAVELLER_AGES,
			value: value,
			callback: callback
		})
	},
	setTravellerResidentLocation: function(value: any, callback?: Function) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_TRAVELLER_RESIDENT_LOCATION,
			value: value,
			callback: callback
		})
	},
	setTripStartLocation: function(value: any, callback?: Function) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_TRIP_START_LOCATION,
			value: value,
			callback: callback
		})
	},
	setOCIPIOCardInfo: function(value: any, callback?: Function) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_OCI_PIO_CARD_INFO,
			value: value,
			callback: callback
		})
	},
	setIndianResidenceDuration: function(value: any, callback?: Function) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.SET_INDIAN_RESIDENCE_DURATION,
			value: value,
			callback: callback
		})
	},
	validate: function(callback?:Function) {
		AppDispatcher.dispatch({
			actionType: ActionEnum.VALIDATE,
			value: undefined,
			callback: callback
		})
	},
	GTMViewTravelQuotes: function() {
		AppDispatcher.dispatch({
			actionType: ActionEnum.GTM_VIEW_TRAVEL_QUOTES
		})
	}
}