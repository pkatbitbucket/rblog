/// <reference path="../../typings/tsd.d.ts" />
import React = require('react')
import ReactDOM = require('react-dom')
import Button = require('../../common/react-component/Button')
import TravellerAges = require('./TravellerAges')
import TravelStore = require('../stores/TravelStore')
import Modal = require('../../common/new-components/Modal')
import moment = require('moment')

import listensToClickOutside = require('../../common/new-components/Dependencies/OnClickHandler')

class AddTravelers extends React.Component<any,any> {
	refs:any
	static defaultProps = {
		travellerAges:[],
		defaultOpen:false,
	}

	static propTypes = {
		travellerAges:  React.PropTypes.any,
		defaultOpen: React.PropTypes.any,
	}

	constructor(props){

		super(props);
		var self = this
		this.state = {
			displayBlock:false,
			onDone:false,
			showError:false,
			noOfTravellersToDisplay:self.props.travellerAges.length,
			travellerAges:self.props.travellerAges,
			showErrors:true,
		}
	}

	private clickOutsideHandler: listensToClickOutside
	/*componentDidMount(){
		var self = this

		//handling closing of calendar when clicked outside
        this.clickOutsideHandler = new listensToClickOutside(ReactDOM.findDOMNode(this.refs['addTravellerRevealBlock']), () => {
				// this.toggleAddTravelerBlock(false)
        })

		window.addEventListener("scroll", self.handlePageScroll.bind(self))
	}

	componentWillUnmount(){
		var self = this
		self.clickOutsideHandler.dispose()

		window.removeEventListener("scroll", self.handlePageScroll.bind(self))
	}

    handlePageScroll(){
		var datePickerNode = ReactDOM.findDOMNode(this.refs['addTravellerRevealBlock'])
		if (this.state.displayBlock && datePickerNode.getBoundingClientRect().top <= 0) {
			// this.toggleAddTravelerBlock(false)
		}
    }*/

	componentWillReceiveProps(newProps){
		var defaultOpen = newProps.defaultOpen

		if(this.state.onDone===false && defaultOpen===true && newProps.validationStatus['isValid']===false){
			// Will hit when TripEndDate gets selected and Travellers aren't selected yet
			// this.toggleAddTravelerBlock(true);

		}else if(newProps.validationStatus['isValid']===true){
			if(this.state.onDone===true){
				this.toggleAddTravelerBlock(false);
			}
		}else{
			// Case - already in open state and Invalid data
			if(this.state.onDone===true){
				this.setState({
					showError:true,
				})		
			}
		}

		this.setState({
			travellerAges:newProps.travellerAges,
			noOfTravellersToDisplay:newProps.travellerAges.length,
		})

	}

	addTraveler(){
		var self = this
		var newTravellerAges = this.state.travellerAges;
			newTravellerAges.push(null)
		this.setState({
			noOfTravellersToDisplay: self.state.noOfTravellersToDisplay+1,
			travellerAges:newTravellerAges,
		})
	}

	removeMember(indexToRemove){
		var totalTravellers = (this.state.noOfTravellersToDisplay>1)?this.state.noOfTravellersToDisplay-1:this.state.noOfTravellersToDisplay

		var travellerAges = this.state.travellerAges
			travellerAges.splice(indexToRemove,1)

		this.setState({
			noOfTravellersToDisplay: totalTravellers,
			travellerAges:travellerAges,
		})
	}

	toggleAddTravelerBlock(show){

		this.setState({
			displayBlock:(show===true)? true : false,
		})
	}

	isValid(){
		return this.refs['AgeInput'].isValid();
	}

	onDone(){
		
		var isValid = this.refs['AgeInput'].isValid();

		if(isValid){
			this.setState({
				onDone:true,
				showError:false,
			},function(){
				if(this.props.onDone){
					this.props.onDone(this.state.travellerAges)
				}		
			})		
		}else{
			this.refs['AgeInput'].showError()
		}	

	}

	onCancel(){
		var self = this;
		var travellerAges = TravelStore.getTravellerAges()
		this.setState({
			onDone:false,
			travellerAges:travellerAges,
			noOfTravellersToDisplay:travellerAges.length,
			displayBlock:false
		},function(){
			// self.toggleAddTravelerBlock(false);
			// if(self.props.onCancel){
			// 	self.props.onCancel()
			// }
		})
	}

	handelAgeChange(travellerAges){
		this.setState({
			travellerAges:travellerAges
		})
	}

	render(){
		
		// var showHideFlag = (this.state.displayBlock ===true)?'reveal':''
		// var showHideDp = 'member_select_dd '+showHideFlag
		var travellerAgesFlag = []
			this.state.travellerAges.map(function(age){
				if(age!=null)
					travellerAgesFlag.push(age)
			})

		var addTravelerBlock = (
			<div ref='addTravellerRevealBlock'>
				<div></div>
				<div className="m-list">
					<TravellerAges ref='AgeInput' noOfTravellersToDisplay={ this.state.noOfTravellersToDisplay } defaultAges={this.state.travellerAges} isSearchable={false} maxLength={2} onRemoveMember={ this.removeMember.bind(this) } onAgeChange={ this.handelAgeChange.bind(this) }  showErrors={ this.state.showErrors }  />
				{
					(this.state.noOfTravellersToDisplay <6)?
						( <Button customClass="w--button w--button--link" title='+ Add another traveller' onClick={this.addTraveler.bind(this)} />  )
					:
					null
				}
				</div>
				<div className="group-btn">
					<Button customClass="w--button--orange" title='DONE' onClick={this.onDone.bind(this)}  /> 
					<Button customClass="w--button--link" title='Cancel' onClick={this.onCancel.bind(this)}  />
				</div>
				{
					this.state.showError===true?
						this.props.validationStatus['isValid'] === false ?(<div className="w--error">{ this.props.validationStatus['errorMessage'] }</div>):null
					:
						null
				}
			</div>
		)

		return (
			<div className="w--member_select">
				<div className="member_select_handle" onClick={this.toggleAddTravelerBlock.bind(this,true)}>
					{
						travellerAgesFlag.length>0?
						(
							<div>
								<div className="label_minimised">Travellers</div> 
								<div> {travellerAgesFlag.length} </div>
							</div>
						):
						(
							<div className="label_full">Add Travellers</div> 
						)
					}
					<span className="member_select_icon">+</span> 
				</div>
				{
					this.state.displayBlock===true?
					(
						<Modal children={ addTravelerBlock } size='' isShowCloseIcon={true} isOpen={true}  onClose={this.onCancel.bind(this)} isCloseOnOutSideClick={false} header='Add travellers. You, spouse and kids only ' customHeaderClass="modal_header"/>
					):null
				}
			</div>
		)
			
	}
}

export = AddTravelers;