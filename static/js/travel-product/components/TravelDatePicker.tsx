/// <reference path="../../typings/tsd.d.ts" />
import React = require('react')
import ReactDOM = require('react-dom')
import Calendar = require('../../common/react-component/Calendar')

import moment = require('moment')

class TravelDatePicker extends React.Component<any,any> {

	static defaultProps = {
		tripStartDate:undefined,
		tripEndDate:undefined,
	}

	static PropTypes = {
		tripStartDate:React.PropTypes.any,
		tripEndDate:React.PropTypes.any,
	}
	
	constructor(props){
		super(props)
		this.state = {
			defaultDateOpen:false
		}
	}

	handelStartDate(startDate){
		var self = this;
		this.setState({
			defaultDateOpen:true
		},function(){
			if(self.props.onStartDateSelect)
				self.props.onStartDateSelect(startDate)	
		})

	}

	handelEndDate(endDate){
		var self = this

		this.setState({
			defaultDateOpen:false,
		},function(){
			if(self.props.onEndDateSelect)
				self.props.onEndDateSelect(endDate)
		})
		
	}

	render(){
		console.log(this.state)
		var dateSectionClass = 'col-sm-12'
		var startDateSelectClass = this.props.tripStartDate == undefined || this.props.tripStartDate == null 
									|| this.props.tripStartDate == "" ? 'empty' : ''
		var endDateSelectClass = this.props.tripEndDate == undefined || this.props.tripEndDate == null 
			|| this.props.tripEndDate == "" ? 'empty' : ''

		var minTripEndDate = this.props.tripStartDate == undefined ? moment().subtract("day", 1) : this.props.tripStartDate
		var endDateDisplayMonth = minTripEndDate

		var highlightedDates = [];

		if(this.props.tripStartDate!=undefined){
			highlightedDates.push({
						date: this.props.tripStartDate,
						description:'Trip Start Date',
					})
		}
		if(this.props.tripStartDate!=undefined){
			highlightedDates.push({
						date: this.props.tripEndDate,
						description:'Trip End Date',
					})
		}

		// var defaultDateOpen = (this.props.tripStartDate!=undefined && this.props.tripEndDate===undefined)? true : false
		
		return (
			<div>
				<Calendar labelText="Trip Start Date" minDate={moment().subtract("day", 1) } defaultDate={this.props.tripStartDate} selectedClass={startDateSelectClass}  prevMonths={0} nextMonths={1} onSelectDate={ this.handelStartDate.bind(this) } highlightedDates={ highlightedDates } />
				
				<Calendar labelText="Trip End Date" minDate={this.props.tripStartDate == undefined ? moment().subtract("day", 1) : this.props.tripStartDate} defaultDate={this.props.tripEndDate} selectedClass={endDateSelectClass} defaultDisplayMonthDate={endDateDisplayMonth} prevMonths={0} nextMonths={1} onSelectDate={ this.handelEndDate.bind(this) }  highlightedDates={ highlightedDates } defaultOpen={ this.state.defaultDateOpen } />
		
			</div>
		)
	}
}

export = TravelDatePicker;