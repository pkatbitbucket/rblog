/// <reference path="../../typings/tsd.d.ts" />
import React = require('react')
import ReactDOM = require('react-dom')
import Calendar = require('../../common/react-component/Calendar')
import listensToClickOutside = require('../../common/new-components/Dependencies/OnClickHandler')
import moment = require('moment')

class TravelDatePicker extends React.Component<any,any> {

	static defaultProps = {
		tripStartDate:undefined,
		tripEndDate:undefined,
	}

	static PropTypes = {
		tripStartDate:React.PropTypes.any,
		tripEndDate:React.PropTypes.any,
	}
	
	constructor(props){
		super(props)
		this.state = {
			defaultDateOpen:false,
			dateProcessing:'start',
			tripStartDate:this.props.tripStartDate,
			tripEndDate:this.props.tripEndDate
		}
	}

	componentWillReceiveProps(newProps){
		this.setState({
			tripStartDate:newProps.tripStartDate,
			tripEndDate:newProps.tripEndDate
		})
	}

	private clickOutsideHandler: listensToClickOutside
	componentDidMount(){
		var self = this

		//handling closing of calendar when clicked outside
        this.clickOutsideHandler = new listensToClickOutside(ReactDOM.findDOMNode(this.refs['datePicker']), () => {
				this.setState({
					defaultDateOpen: false
		    	})
        })

		window.addEventListener("scroll", self.handlePageScroll.bind(self))
	}

	componentWillUnmount(){
		var self = this
		self.clickOutsideHandler.dispose()

		window.removeEventListener("scroll", self.handlePageScroll.bind(self))
	}

    handlePageScroll(){
		var datePickerNode = ReactDOM.findDOMNode(this.refs['datePicker'])
		if (this.state.defaultDateOpen && datePickerNode.getBoundingClientRect().top <= 0) {
			this.setState({
				defaultDateOpen: false
	    	})
		}
    }

	handleDateSelect(date){
		var dateProcessing = this.state.dateProcessing;

		if(dateProcessing=='start'){
			this.handelStartDate(date)
		}else{
			this.handelEndDate(date)
		}
	}

	handelStartDate(startDate){
		var self = this;
		this.setState({
			dateProcessing:'end',
			tripEndDate:undefined,
		},function(){
			if(self.props.onStartDateSelect)
				self.props.onStartDateSelect(startDate)	
		})
	}


	handelEndDate(endDate){
		var self = this

		this.setState({
			defaultDateOpen:false,
		},function(){
			if(self.props.onEndDateSelect)
				self.props.onEndDateSelect(endDate)
		})
		
	}


	open(startEndFlag){

		this.setState({
			defaultDateOpen:true,
			dateProcessing:startEndFlag,
		})	
	}

	editDate(startEndFlag){
		this.setState({
			dateProcessing:startEndFlag,
		})
	}

	render(){
		
		var dateSectionClass = 'col-sm-12'
		var startDateSelectClass = this.state.tripStartDate == undefined || this.state.tripStartDate == null 
									|| this.state.tripStartDate == "" ? 'empty' : ''
		var endDateSelectClass = this.state.tripEndDate == undefined || this.state.tripEndDate == null 
			|| this.state.tripEndDate == "" ? 'empty' : ''

		var minTripEndDate = this.state.tripStartDate == undefined ? moment().subtract("day", 1) : this.state.tripStartDate
		var endDateDisplayMonth = (this.state.dateProcessing=='start')? undefined : minTripEndDate

		var minDate = (this.state.dateProcessing=='start')? moment().subtract("day", 1) : this.state.tripStartDate == undefined ? moment().subtract("day", 1) : this.state.tripStartDate

		var highlightedDates = [];

		if(this.state.tripStartDate!=undefined){
			highlightedDates.push({
						date: this.state.tripStartDate,
						description:'Trip Start Date',
					})
		}
		if(this.state.tripEndDate!=undefined){
			highlightedDates.push({
						date: this.state.tripEndDate,
						description:'Trip End Date',
					})
		}

		var StartDatelabelClass = this.state.tripStartDate != undefined ? ((this.state.dateProcessing == 'start' && this.state.defaultDateOpen ===true ) ? 'label_minimised hide' :'label_minimised') : ((this.state.defaultDateOpen===true && this.state.dateProcessing=='start')?'label_full trip_active':'label_full')

		var EndDatelabelClass = this.state.tripEndDate != undefined ? ((this.state.dateProcessing ==  'end' && this.state.defaultDateOpen===true ) ? 'label_minimised hide' :'label_minimised')  : ((this.state.defaultDateOpen===true && this.state.dateProcessing=='end')?'label_full trip_active':'label_full')

		var startDate = (this.state.tripStartDate != undefined) ? moment(this.state.tripStartDate, 'YYYY-MM-DD').format('DD MMM YYYY'):''
		var endDate = (this.state.tripEndDate!=undefined)? moment(this.state.tripEndDate,'YYYY-MM-DD').format('DD MMM YYYY'):''

		var displayMonthDate
		if(this.state.dateProcessing=='start'){
			displayMonthDate = (this.state.tripStartDate!=undefined)? this.state.tripStartDate:undefined;
		}else{
			displayMonthDate = (this.state.tripEndDate!=undefined)? this.state.tripEndDate:undefined;
		}

		return (
			<div className="w--calendar w--calendar-column-2" ref='datePicker'>
				<div>
					<div className="w--calendar_handle left" onClick={ this.open.bind(this,'start') }>
						<div className={StartDatelabelClass}>Trip Start Date</div> 
						<div className={(this.state.defaultDateOpen===true && this.state.dateProcessing=='start')?'trip_active':''} > {startDate} </div> 
						
						<span>
							<i className='fa fa-2x fa-times' > </i>
						</span>
					</div>
					<div className="w--calendar_arrow">&rarr;</div>
					<div className="w--calendar_handle right" onClick={ this.open.bind(this,'end') }>
						<div className={EndDatelabelClass}>Trip End Date</div> 
						<div className={(this.state.defaultDateOpen===true && this.state.dateProcessing=='end')?'trip_active':''} > {endDate} </div> 
						
						<span>
							<i className='fa fa-2x fa-times' > </i>
						</span>
					</div>
				</div>
				<div className={"w--calendar_dd "+(this.state.defaultDateOpen===true?'reveal':'')}>
		
					<Calendar highlightedDates={ highlightedDates } onSelectDate={ this.handleDateSelect.bind(this) } defaultOpen={ true } minDate={ minDate } selectedClass='' prevMonths={0} nextMonths={1} displayCalendarHandle={ false } isCloseOnOutSideClickAndScroll={false} defaultDisplayMonthDate={ displayMonthDate } />

				</div>
			</div>
		)
	}
}

export = TravelDatePicker;