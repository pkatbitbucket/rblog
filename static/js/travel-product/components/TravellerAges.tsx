/// <reference path="../../typings/tsd.d.ts" />
import React = require('react')
import ReactDOM = require('react-dom')
import NumberInput = require('../../common/react-component/NumberInput')

class TravellerAges extends React.Component<any, any> {	
	refs:any
	static propTypes = {
		noOfTravellersToDisplay:React.PropTypes.number,
		defaultAges:React.PropTypes.array,
		onChange:React.PropTypes.any,
		maxLength:React.PropTypes.number
	}

	static defaultProps = {
		maxLength:2
	}



	getValues(){
		var age
		var ageArray = []
		for (var i = 0; i < this.props.noOfTravellersToDisplay; i++) {

			if(this.refs['NumberInput_' + i].isValid()){
				age = this.refs['NumberInput_' + i].getValue()
				if (age != undefined && age != "" && !isNaN(parseInt(age))) {
					ageArray.push(parseInt(age))
				}
			}else{
				this.refs['NumberInput_' + i].showErrors()
			}
		}

		return ageArray
	}

	isValid(){
		var valid = true
		for (var i = 0; i < this.props.noOfTravellersToDisplay; i++) {
			if(!this.refs['NumberInput_' + i].isValid() && valid===true){
				valid = false
			}
		}

		return valid;
	}

	removeMember(index){
		if(this.props.onRemoveMember){
			this.props.onRemoveMember(index)
		}	
	}

	handleChange(ref){
		var ages = []
		var rowsToDisplay = this.props.noOfTravellersToDisplay
		for (var i = 0; i < rowsToDisplay; i++){
			ages.push(this.refs['NumberInput_'+i].getValue());
		}

		if(this.props.onAgeChange)
			this.props.onAgeChange(ages)
	}

	showError(){
		for (var i = 0; i < this.props.noOfTravellersToDisplay; i++) {
			if(!this.refs['NumberInput_' + i].isValid()){
				this.refs['NumberInput_' + i].showErrors('Please enter traveller age.')
			}
		}
	}

	render(){
		var age
		var ageInputArray = []
		var labelText = "Your Age"
		var rowsToDisplay = this.props.noOfTravellersToDisplay
		
		for (var i = 0; i < rowsToDisplay; i++){
			age = undefined
			if (this.props.defaultAges && this.props.defaultAges[i]){
				if (!isNaN(this.props.defaultAges[i]) && !isNaN(parseInt(this.props.defaultAges[i]))) {
					age = parseInt(this.props.defaultAges[i])
				}
			}

			var row  = (
				<div className="m-list__data" key={'NumberInput_'+i} >
					<span className="m-list__data__label">Traveller {i+1}</span>
					<div className="m-list__data__field">
						<NumberInput ref={'NumberInput_' + i} maxLength={this.props.maxLength}  defaultValue={age} validations={['required','integer', { max: 99 }, { min: 1 }]} min={1} placeholder='Age (Years)' onChange={ this.handleChange.bind(this,'NumberInput_'+i) } />
					{ i!=0?(<span className="m-list__data__remove" onClick={ this.removeMember.bind(this,i) } >X</span>):null }
					</div>
				</div>
			)

			ageInputArray.push(row)
		}
 		return(
			<div>
				{ageInputArray}
			</div>

		)
	}
}

export = TravellerAges