enum ACTION {
	SET_TRIP_TYPE=4001,
	SET_SELECTED_COUNTRIES,
	SET_SELECTED_REGION,
	SET_START_DATE,
	SET_END_DATE,
	SET_NO_OF_TRAVELLERS,
	SET_LONGEST_TRIP_DURATION,
	SET_TRAVELLER_AGES,
	SET_TRAVELLER_RESIDENT_LOCATION,
	SET_TRIP_START_LOCATION,
	SET_OCI_PIO_CARD_INFO,
	SET_INDIAN_RESIDENCE_DURATION,
	VALIDATE,
	GTM_VIEW_TRAVEL_QUOTES
}

export = ACTION