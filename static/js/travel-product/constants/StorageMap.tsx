var TRAVEL = {
	PARENT: 'travel',
	TRIP_TYPE: '_tripTypeValue',
	NO_OF_TRAVELLERS: '_ntravelers',
	TRAVELLER_AGES: '_travelerAges',
	LONGEST_TRIP_DURATION:'_durationValue',
	SELECTED_COUNTRIES: '_selectedCountriesSlugs',
	SELECTED_REGION: '_selectedRegion',
	TRIP_START_DATE: '_startDate',
	TRIP_END_DATE: '_endDate',
	IS_RESIDENT:'_isResident',
	INDIAN_RESIDENT_PERIOD:'_residentFor',
	DOES_HAEV_OCI_PIO_CARD:'_ocipioCard',
	IS_TRIP_START_INDIA:'_tripStartIndia'
}

export = { TRAVEL: TRAVEL }