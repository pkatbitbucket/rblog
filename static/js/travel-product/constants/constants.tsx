export = {
	tripType: [
		{
			label: "Single Trip Policy",
			value: "SINGLE",
		},
		{
			label: "Annual Trip Policy",
			value: "MULTI"
		}
	],

	countries: ["Argentina", "Australia", "Austria",
		"Bahamas", "Belgium",
		"Brazil", "Canada",
		"China", "Colombia",
		"Cuba", "Denmark",
		"Egypt", "Europe",
		"France", "Germany",
		"Greece", "Indonesia",
		"Ireland", "Italy",
		"Japan", "Malaysia",
		"Maldives", "Mauritius",
		"Mexico", "Nepal",
		"Netherlands", "New Zealand",
		"Nigeria", "Norway",
		"Philippines", "Poland",
		"Portugal", "Qatar",
		"Russia", "Saudi Arabia",
		"Schengen Countries", "Singapore",
		"South Africa", "Spain",
		"Sri Lanka", "Sweden",
		"Switzerland", "Tanzania",
		"Thailand", "Turkey",
		"Uganda", "Ukraine",
		"United Arab Emirates (UAE)", "United Kingdom (UK)",
		"United States of America (USA)", "Vatican City (Holy See)",
		"Zimbabwe"],

	regions: [
		{
			label: "Worldwide",
			value: "W"
		},
		{
			label: "Worldwide excluding US/Canada",
			value: "W_NO_US_CANADA"
		}
	],

	durationOptions: [
		{
			key: "30 days",
			value: 30
		},
		{
			key: "45 days",
			value: 45
		},
		{
			key: "60 days",
			value: 60
		}],

	noOfTravellers: [
		1, 2, 3, 4, 5, 6
	],

	YesNoObject: [
		{ 
		  label:"Yes",
		  value: true
		},
		{ 
			label:"No",
			value: false
		}
	],

	travellerResidenceDurationOptions: [{
		label: 'Less than 6 months',
		value: 'LESS_THAN_6'
	},
	{
		label: 'More than 6 months',
		value: 'MORE_THAN_6'
	}
	],

}