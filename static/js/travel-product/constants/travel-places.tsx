export = [  
   {  
      "key":"AFRICA",
      "value":"africa",
      "priority":"Other"
   },
   {  
      "key":"ASIA",
      "value":"asia",
      "priority":"Other"
   },
   {  
      "key":"Afghanistan",
      "value":"afghanistan",
      "priority":"Other"
   },
   {  
      "key":"Albania",
      "value":"albania",
      "priority":"Other"
   },
   {  
      "key":"Algeria",
      "value":"algeria",
      "priority":"Other"
   },
   {  
      "key":"Andorra",
      "value":"andorra",
      "priority":"Other"
   },
   {  
      "key":"Angola",
      "value":"angola",
      "priority":"Other"
   },
   {  
      "key":"Antigua and Barbuda",
      "value":"antigua-and-barbuda",
      "priority":"Other"
   },
   {  
      "key":"Argentina",
      "value":"argentina",
      "priority":"Other"
   },
   {  
      "key":"Armenia",
      "value":"armenia",
      "priority":"Other"
   },
   {  
      "key":"Australia",
      "value":"australia",
      "priority":"Top Countries"
   },
   {  
      "key":"Austria",
      "value":"austria",
      "priority":"Other"
   },
   {  
      "key":"Azerbaijan",
      "value":"azerbaijan",
      "priority":"Other"
   },
   {  
      "key":"Bahamas",
      "value":"bahamas",
      "priority":"Other"
   },
   {  
      "key":"Bahrain",
      "value":"bahrain",
      "priority":"Other"
   },
   {  
      "key":"Bangladesh",
      "value":"bangladesh",
      "priority":"Other"
   },
   {  
      "key":"Barbados",
      "value":"barbados",
      "priority":"Other"
   },
   {  
      "key":"Belarus",
      "value":"belarus",
      "priority":"Other"
   },
   {  
      "key":"Belgium",
      "value":"belgium",
      "priority":"Other"
   },
   {  
      "key":"Belize",
      "value":"belize",
      "priority":"Other"
   },
   {  
      "key":"Benin",
      "value":"benin",
      "priority":"Other"
   },
   {  
      "key":"Bhutan",
      "value":"bhutan",
      "priority":"Other"
   },
   {  
      "key":"Bolivia",
      "value":"bolivia",
      "priority":"Other"
   },
   {  
      "key":"Bosnia and Herzegovina ",
      "value":"bosnia-and-herzegovina",
      "priority":"Other"
   },
   {  
      "key":"Botswana",
      "value":"botswana",
      "priority":"Other"
   },
   {  
      "key":"Brazil",
      "value":"brazil",
      "priority":"Other"
   },
   {  
      "key":"Brunei",
      "value":"brunei",
      "priority":"Other"
   },
   {  
      "key":"Bulgaria",
      "value":"bulgaria",
      "priority":"Other"
   },
   {  
      "key":"Burkina",
      "value":"burkina",
      "priority":"Other"
   },
   {  
      "key":"Burkina Faso",
      "value":"burkina-faso",
      "priority":"Other"
   },
   {  
      "key":"Burundi",
      "value":"burundi",
      "priority":"Other"
   },
   {  
      "key":"Cabo Verde",
      "value":"cabo-verde",
      "priority":"Other"
   },
   {  
      "key":"Cambodia",
      "value":"cambodia",
      "priority":"Other"
   },
   {  
      "key":"Cameroon",
      "value":"cameroon",
      "priority":"Other"
   },
   {  
      "key":"Canada",
      "value":"canada",
      "priority":"Other"
   },
   {  
      "key":"Cape Verde",
      "value":"cape-verde",
      "priority":"Other"
   },
   {  
      "key":"Central African Republic",
      "value":"central-african-republic",
      "priority":"Other"
   },
   {  
      "key":"Chad",
      "value":"chad",
      "priority":"Other"
   },
   {  
      "key":"Chile",
      "value":"chile",
      "priority":"Other"
   },
   {  
      "key":"China",
      "value":"china",
      "priority":"Other"
   },
   {  
      "key":"Colombia",
      "value":"colombia",
      "priority":"Other"
   },
   {  
      "key":"Comoros",
      "value":"comoros",
      "priority":"Other"
   },
   {  
      "key":"Costa Rica",
      "value":"costa-rica",
      "priority":"Other"
   },
   {  
      "key":"Cote d'Ivoire",
      "value":"cote-divoire",
      "priority":"Other"
   },
   {  
      "key":"Croatia",
      "value":"croatia",
      "priority":"Other"
   },
   {  
      "key":"Cuba",
      "value":"cuba",
      "priority":"Other"
   },
   {  
      "key":"Cyprus",
      "value":"cyprus",
      "priority":"Other"
   },
   {  
      "key":"Czech Republic",
      "value":"czech-republic",
      "priority":"Other"
   },
   {  
      "key":"Democratic Republic of the Congo",
      "value":"congo-democratic-republic-of-the",
      "priority":"Other"
   },
   {  
      "key":"Denmark",
      "value":"denmark",
      "priority":"Other"
   },
   {  
      "key":"Djibouti",
      "value":"djibouti",
      "priority":"Other"
   },
   {  
      "key":"Dominica",
      "value":"dominica",
      "priority":"Other"
   },
   {  
      "key":"Dominican Republic",
      "value":"dominican-republic",
      "priority":"Other"
   },
   {  
      "key":"Ecuador",
      "value":"ecuador",
      "priority":"Other"
   },
   {  
      "key":"Egypt",
      "value":"egypt",
      "priority":"Other"
   },
   {  
      "key":"El Salvador",
      "value":"el-salvador",
      "priority":"Other"
   },
   {  
      "key":"England",
      "value":"england",
      "priority":"Other"
   },
   {  
      "key":"Equatorial Guinea",
      "value":"equatorial-guinea",
      "priority":"Other"
   },
   {  
      "key":"Eritrea",
      "value":"eritrea",
      "priority":"Other"
   },
   {  
      "key":"Estonia",
      "value":"estonia",
      "priority":"Other"
   },
   {  
      "key":"Ethiopia",
      "value":"ethiopia",
      "priority":"Other"
   },
   {  
      "key":"Europe",
      "value":"europe",
      "priority":"Other"
   },
   {  
      "key":"Fiji",
      "value":"fiji",
      "priority":"Other"
   },
   {  
      "key":"Finland",
      "value":"finland",
      "priority":"Other"
   },
   {  
      "key":"France",
      "value":"france",
      "priority":"Other"
   },
   {  
      "key":"Gabon",
      "value":"gabon",
      "priority":"Other"
   },
   {  
      "key":"Gambia",
      "value":"gambia",
      "priority":"Other"
   },
   {  
      "key":"Georgia",
      "value":"georgia",
      "priority":"Other"
   },
   {  
      "key":"Germany",
      "value":"germany",
      "priority":"Other"
   },
   {  
      "key":"Ghana",
      "value":"ghana",
      "priority":"Other"
   },
   {  
      "key":"Greece",
      "value":"greece",
      "priority":"Other"
   },
   {  
      "key":"Grenada",
      "value":"grenada",
      "priority":"Other"
   },
   {  
      "key":"Guatemala",
      "value":"guatemala",
      "priority":"Other"
   },
   {  
      "key":"Guinea",
      "value":"guinea",
      "priority":"Other"
   },
   {  
      "key":"Guinea-Bissau",
      "value":"guinea-bissau",
      "priority":"Top Countries"
   },
   {  
      "key":"Guyana",
      "value":"guyana",
      "priority":"Other"
   },
   {  
      "key":"Haiti",
      "value":"haiti",
      "priority":"Other"
   },
   {  
      "key":"Honduras",
      "value":"honduras",
      "priority":"Other"
   },
   {  
      "key":"Hong Kong",
      "value":"hong-kong",
      "priority":"Other"
   },
   {  
      "key":"Hungary",
      "value":"hungary",
      "priority":"Other"
   },
   {  
      "key":"Iceland",
      "value":"iceland",
      "priority":"Other"
   },
   {  
      "key":"Indonesia",
      "value":"indonesia",
      "priority":"Other"
   },
   {  
      "key":"Iran",
      "value":"iran",
      "priority":"Other"
   },
   {  
      "key":"Iraq",
      "value":"iraq",
      "priority":"Other"
   },
   {  
      "key":"Ireland",
      "value":"ireland",
      "priority":"Other"
   },
   {  
      "key":"Israel",
      "value":"israel",
      "priority":"Other"
   },
   {  
      "key":"Italy",
      "value":"italy",
      "priority":"Other"
   },
   {  
      "key":"Ivory Coast",
      "value":"ivory-coast",
      "priority":"Other"
   },
   {  
      "key":"Jamaica",
      "value":"jamaica",
      "priority":"Other"
   },
   {  
      "key":"Japan",
      "value":"japan",
      "priority":"Other"
   },
   {  
      "key":"Jordan",
      "value":"jordan",
      "priority":"Other"
   },
   {  
      "key":"Kazakhstan",
      "value":"kazakhstan",
      "priority":"Other"
   },
   {  
      "key":"Kenya",
      "value":"kenya",
      "priority":"Other"
   },
   {  
      "key":"Kiribati",
      "value":"kiribati",
      "priority":"Other"
   },
   {  
      "key":"Kosovo",
      "value":"kosovo",
      "priority":"Other"
   },
   {  
      "key":"Kuwait",
      "value":"kuwait",
      "priority":"Other"
   },
   {  
      "key":"Kyrgyzstan",
      "value":"kyrgyzstan",
      "priority":"Other"
   },
   {  
      "key":"Laos",
      "value":"laos",
      "priority":"Other"
   },
   {  
      "key":"Latvia",
      "value":"latvia",
      "priority":"Other"
   },
   {  
      "key":"Lebanon",
      "value":"lebanon",
      "priority":"Other"
   },
   {  
      "key":"Lesotho",
      "value":"lesotho",
      "priority":"Other"
   },
   {  
      "key":"Liberia",
      "value":"liberia",
      "priority":"Other"
   },
   {  
      "key":"Libya",
      "value":"libya",
      "priority":"Other"
   },
   {  
      "key":"Liechtenstein",
      "value":"liechtenstein",
      "priority":"Other"
   },
   {  
      "key":"Lithuania",
      "value":"lithuania",
      "priority":"Other"
   },
   {  
      "key":"Luxembourg",
      "value":"luxembourg",
      "priority":"Other"
   },
   {  
      "key":"Macedonia",
      "value":"macedonia",
      "priority":"Other"
   },
   {  
      "key":"Madagascar",
      "value":"madagascar",
      "priority":"Other"
   },
   {  
      "key":"Malawi",
      "value":"malawi",
      "priority":"Other"
   },
   {  
      "key":"Malaysia",
      "value":"malaysia",
      "priority":"Other"
   },
   {  
      "key":"Maldives",
      "value":"maldives",
      "priority":"Other"
   },
   {  
      "key":"Mali",
      "value":"mali",
      "priority":"Other"
   },
   {  
      "key":"Malta",
      "value":"malta",
      "priority":"Other"
   },
   {  
      "key":"Marshall Islands",
      "value":"marshall-islands",
      "priority":"Other"
   },
   {  
      "key":"Mauritania",
      "value":"mauritania",
      "priority":"Other"
   },
   {  
      "key":"Mauritius",
      "value":"mauritius",
      "priority":"Other"
   },
   {  
      "key":"Mexico",
      "value":"mexico",
      "priority":"Other"
   },
   {  
      "key":"Micronesia",
      "value":"micronesia",
      "priority":"Other"
   },
   {  
      "key":"Moldova",
      "value":"moldova",
      "priority":"Other"
   },
   {  
      "key":"Monaco",
      "value":"monaco",
      "priority":"Other"
   },
   {  
      "key":"Mongolia",
      "value":"mongolia",
      "priority":"Other"
   },
   {  
      "key":"Montenegro",
      "value":"montenegro",
      "priority":"Other"
   },
   {  
      "key":"Morocco",
      "value":"morocco",
      "priority":"Other"
   },
   {  
      "key":"Mozambique",
      "value":"mozambique",
      "priority":"Other"
   },
   {  
      "key":"Myanmar (Burma)",
      "value":"myanmar-burma",
      "priority":"Other"
   },
   {  
      "key":"NORTH AMERICA",
      "value":"north-america",
      "priority":"Other"
   },
   {  
      "key":"Namibia",
      "value":"namibia",
      "priority":"Other"
   },
   {  
      "key":"Nauru",
      "value":"nauru",
      "priority":"Other"
   },
   {  
      "key":"Nepal",
      "value":"nepal",
      "priority":"Other"
   },
   {  
      "key":"Netherlands",
      "value":"netherlands",
      "priority":"Other"
   },
   {  
      "key":"New Zealand",
      "value":"new-zealand",
      "priority":"Other"
   },
   {  
      "key":"Nicaragua",
      "value":"nicaragua",
      "priority":"Other"
   },
   {  
      "key":"Niger",
      "value":"niger",
      "priority":"Other"
   },
   {  
      "key":"Nigeria",
      "value":"nigeria",
      "priority":"Other"
   },
   {  
      "key":"North Korea",
      "value":"north-korea",
      "priority":"Other"
   },
   {  
      "key":"Norway",
      "value":"norway",
      "priority":"Other"
   },
   {  
      "key":"OCEANIA",
      "value":"oceania",
      "priority":"Other"
   },
   {  
      "key":"Oman",
      "value":"oman",
      "priority":"Other"
   },
   {  
      "key":"Pakistan",
      "value":"pakistan",
      "priority":"Other"
   },
   {  
      "key":"Palau",
      "value":"palau",
      "priority":"Other"
   },
   {  
      "key":"Palestine",
      "value":"palestine",
      "priority":"Other"
   },
   {  
      "key":"Panama",
      "value":"panama",
      "priority":"Other"
   },
   {  
      "key":"Papua New Guinea",
      "value":"papua-new-guinea",
      "priority":"Other"
   },
   {  
      "key":"Paraguay",
      "value":"paraguay",
      "priority":"Other"
   },
   {  
      "key":"Peru",
      "value":"peru",
      "priority":"Other"
   },
   {  
      "key":"Philippines",
      "value":"philippines",
      "priority":"Other"
   },
   {  
      "key":"Poland",
      "value":"poland",
      "priority":"Other"
   },
   {  
      "key":"Portugal",
      "value":"portugal",
      "priority":"Other"
   },
   {  
      "key":"Qatar",
      "value":"qatar",
      "priority":"Other"
   },
   {  
      "key":"Republic of the Congo",
      "value":"congo-republic-of-the",
      "priority":"Other"
   },
   {  
      "key":"Romania",
      "value":"romania",
      "priority":"Other"
   },
   {  
      "key":"Russia",
      "value":"russia",
      "priority":"Other"
   },
   {  
      "key":"Rwanda",
      "value":"rwanda",
      "priority":"Other"
   },
   {  
      "key":"SOUTH AMERICA",
      "value":"south-america",
      "priority":"Other"
   },
   {  
      "key":"Samoa",
      "value":"samoa",
      "priority":"Other"
   },
   {  
      "key":"San Marino",
      "value":"san-marino",
      "priority":"Other"
   },
   {  
      "key":"Sao Tome and Principe",
      "value":"sao-tome-and-principe",
      "priority":"Other"
   },
   {  
      "key":"Saudi Arabia",
      "value":"saudi-arabia",
      "priority":"Other"
   },
   {  
      "key":"Schengen",
      "value":"schengen",
      "priority":"Other"
   },
   {  
      "key":"Scotland",
      "value":"scotland",
      "priority":"Other"
   },
   {  
      "key":"Senegal",
      "value":"senegal",
      "priority":"Other"
   },
   {  
      "key":"Serbia",
      "value":"serbia",
      "priority":"Other"
   },
   {  
      "key":"Seychelles",
      "value":"seychelles",
      "priority":"Other"
   },
   {  
      "key":"Sierra Leone",
      "value":"sierra-leone",
      "priority":"Other"
   },
   {  
      "key":"Singapore",
      "value":"singapore",
      "priority":"Other"
   },
   {  
      "key":"Slovakia",
      "value":"slovakia",
      "priority":"Other"
   },
   {  
      "key":"Slovenia",
      "value":"slovenia",
      "priority":"Other"
   },
   {  
      "key":"Solomon Islands",
      "value":"solomon-islands",
      "priority":"Other"
   },
   {  
      "key":"Somalia",
      "value":"somalia",
      "priority":"Other"
   },
   {  
      "key":"South Africa",
      "value":"south-africa",
      "priority":"Other"
   },
   {  
      "key":"South Korea",
      "value":"south-korea",
      "priority":"Other"
   },
   {  
      "key":"South Sudan",
      "value":"south-sudan",
      "priority":"Other"
   },
   {  
      "key":"Spain",
      "value":"spain",
      "priority":"Top Countries"
   },
   {  
      "key":"Sri Lanka",
      "value":"sri-lanka",
      "priority":"Other"
   },
   {  
      "key":"St. Kitts and Nevis",
      "value":"st-kitts-and-nevis",
      "priority":"Other"
   },
   {  
      "key":"St. Lucia",
      "value":"st-lucia",
      "priority":"Other"
   },
   {  
      "key":"St. Vincent and The Grenadines",
      "value":"st-vincent-and-the-grenadines",
      "priority":"Other"
   },
   {  
      "key":"Sudan",
      "value":"sudan",
      "priority":"Other"
   },
   {  
      "key":"Suriname",
      "value":"suriname",
      "priority":"Other"
   },
   {  
      "key":"Swaziland",
      "value":"swaziland",
      "priority":"Other"
   },
   {  
      "key":"Sweden",
      "value":"sweden",
      "priority":"Other"
   },
   {  
      "key":"Switzerland",
      "value":"switzerland",
      "priority":"Top Countries"
   },
   {  
      "key":"Syria",
      "value":"syria",
      "priority":"Other"
   },
   {  
      "key":"Taiwan",
      "value":"taiwan",
      "priority":"Other"
   },
   {  
      "key":"Tajikistan",
      "value":"tajikistan",
      "priority":"Other"
   },
   {  
      "key":"Tanzania",
      "value":"tanzania",
      "priority":"Other"
   },
   {  
      "key":"Thailand",
      "value":"thailand",
      "priority":"Other"
   },
   {  
      "key":"Timor-Leste",
      "value":"timor-leste",
      "priority":"Other"
   },
   {  
      "key":"Togo",
      "value":"togo",
      "priority":"Other"
   },
   {  
      "key":"Trinidad and Tobago",
      "value":"trinidad-and-tobago",
      "priority":"Other"
   },
   {  
      "key":"Tunisia",
      "value":"tunisia",
      "priority":"Other"
   },
   {  
      "key":"Turkey",
      "value":"turkey",
      "priority":"Other"
   },
   {  
      "key":"Turkmenistan",
      "value":"turkmenistan",
      "priority":"Other"
   },
   {  
      "key":"Uganda",
      "value":"uganda",
      "priority":"Other"
   },
   {  
      "key":"Ukraine",
      "value":"ukraine",
      "priority":"Other"
   },
   {  
      "key":"UAE",
      "value":"united-arab-emirates",
      "priority":"Other"
   },
   {  
      "key":"UK",
      "value":"united-kingdom",
      "priority":"Top Countries"
   },
   {  
      "key":"USA",
      "value":"usa-united-states-of-america",
      "priority":"Top Countries"
   },
   {  
      "key":"Uruguay",
      "value":"uruguay",
      "priority":"Other"
   },
   {  
      "key":"Uzbekistan",
      "value":"uzbekistan",
      "priority":"Other"
   },
   {  
      "key":"Vatican City",
      "value":"vatican-city-holy-see",
      "priority":"Other"
   },
   {  
      "key":"Venezuela",
      "value":"venezuela",
      "priority":"Other"
   },
   {  
      "key":"Vietnam",
      "value":"vietnam",
      "priority":"Other"
   },
   {  
      "key":"Worldwide",
      "value":"worldwide",
      "priority":"Other"
   },
   {  
      "key":"Yemen",
      "value":"yemen",
      "priority":"Other"
   },
   {  
      "key":"Zambia",
      "value":"zambia",
      "priority":"Other"
   },
   {  
      "key":"Zimbabwe",
      "value":"zimbabwe",
      "priority":"Other"
   }
]