import BaseStore = require('../../common/BaseStore')
import ActionEnum = require('../constants/ActionEnum')
import StorageMap = require('../constants/StorageMap')
import CommonUtils = require('../../common/utils/CommonUtils')
import APIUtils = require('../utils/APIUtils')
import GTMUtils = require('../utils/GTMUtils')

import moment = require('moment')

var KEYS = StorageMap.TRAVEL;

class TravelStore extends BaseStore{
	constructor(_namespace:string){
		super(_namespace)
		this.registerDispatcherHandler(this._actionHandler)
		
		var obj = {}
		//setting default values
		var noOfTravellers = this.getNoOfTravellers()
		var tripType = this.getTripType()
		if (isNaN(noOfTravellers)){
			this.setNoOfTravellers(1)
		}
		if (tripType === undefined || tripType === null || tripType == ""){
			this.setTripType('SINGLE')
		}

		var isIndianResident = this.getIsIndianResident()
		if (isIndianResident == undefined){
			this.setIsIndianResident(true)
		}

		var isTripStartIndia = this.getIsTripStartIndia()
		if (isTripStartIndia == undefined) {
			this.setIsTripStartIndia(true)
		}

		var selectedCountries = this.getSelectedCountries() || []
			obj[KEYS.SELECTED_COUNTRIES] = selectedCountries
			this.setSelected(obj,true)

		if(!this.getTravellerAges()){
			obj[KEYS.TRAVELLER_AGES] = [null]
			this.setSelected(obj,true)			
		}
		
	}

	//public getters here
	getTripType():string {
		return this.getSelected(KEYS.TRIP_TYPE)
	}
	getSelectedCountries(): any {
		return this.getSelected(KEYS.SELECTED_COUNTRIES)
	}
	getSelectedRegion(): string {
		return this.getSelected(KEYS.SELECTED_REGION)
	}
	getTripStartDate(): any {
		
		let tripStartDate = undefined
		let tripStartDateString = this.getSelected(KEYS.TRIP_START_DATE)
		if (tripStartDateString!=undefined){
			tripStartDate = moment(this.getSelected(KEYS.TRIP_START_DATE), "YYYY-MM-DD")
		}
		return tripStartDate
	}
	getTripEndDate(): any {
		
		let tripEndDate = undefined
		let tripEndDateString = this.getSelected(KEYS.TRIP_END_DATE)
		if (tripEndDateString != undefined) {
			tripEndDate = moment(this.getSelected(KEYS.TRIP_END_DATE), "YYYY-MM-DD")
		}
		return tripEndDate
	}

	getNoOfTravellers(): number {
		return this.getSelected(KEYS.NO_OF_TRAVELLERS)
	}

	getLongestTripDuration(): string {
		return this.getSelected(KEYS.LONGEST_TRIP_DURATION)
	}
	getTravellerAges(): any {
		return this.getSelected(KEYS.TRAVELLER_AGES)
	}
	getIsIndianResident(): boolean {
		return this.getSelected(KEYS.IS_RESIDENT)
	}
	getIndianResidenceDuration():number { 
		return this.getSelected(KEYS.INDIAN_RESIDENT_PERIOD)
	}
	getDoesHaveOCIPIOCard(): boolean {
		return this.getSelected(KEYS.DOES_HAEV_OCI_PIO_CARD)
	}
	getIsTripStartIndia(): boolean {
		return this.getSelected(KEYS.IS_TRIP_START_INDIA)
	}
	getNoOfTripDays(){
		let startDate = this.getTripStartDate()
		let endDate = this.getTripEndDate()
		let noOfTripDays = -1
		if (moment.isMoment(startDate) && moment.isMoment(endDate) && !moment(endDate).isBefore(startDate)){
			noOfTripDays = endDate.diff(startDate, 'days') + 1
		}
		return noOfTripDays
	}
	getIsStopSubmission(){
		let isTripStartIndia = this.getIsTripStartIndia()
		let isIndianResident = this.getIsIndianResident()
		let hasOCIPIOCard = this.getDoesHaveOCIPIOCard()
		if (!isTripStartIndia || (!isIndianResident && !hasOCIPIOCard)){
			return true
		}
		else {
			return false
		}
	}

	//private setters here
	private setTripType(tripType: string) {
		let tripTypeObj = {}
		let noOfTravellersObj = {}
		let travellerAgesObj = {}
		let EndDateObj = {}
		let TravellerAgesArray
		tripTypeObj[KEYS.TRIP_TYPE] = tripType

		if (tripType == "MULTI"){
			noOfTravellersObj[KEYS.NO_OF_TRAVELLERS] = 1
			this.setSelected(noOfTravellersObj, true)

			EndDateObj[KEYS.TRIP_END_DATE] = undefined
			this.setSelected(EndDateObj, true)

			TravellerAgesArray = this.getTravellerAges()
			if (TravellerAgesArray != undefined && TravellerAgesArray != null && TravellerAgesArray.length > 0){
				travellerAgesObj[KEYS.TRAVELLER_AGES] = [TravellerAgesArray[0]]
				this.setSelected(travellerAgesObj, true)
			}
		}
		this.setSelected(tripTypeObj)
	}

	private setSelectedCountries(selectedCountries: any) {
		let obj = {}
		obj[KEYS.SELECTED_COUNTRIES] = selectedCountries
		this.setSelected(obj)
	}

	private setSelectedRegion(selectedRegion: any) {
		let obj = {}
		obj[KEYS.SELECTED_REGION] = selectedRegion.value
		this.setSelected(obj)
	}

	private setStartDate(startDate: any) {
		let obj = {}
		obj[KEYS.TRIP_START_DATE] = startDate.format("YYYY-MM-DD")
		this.clearSelected(KEYS.TRIP_END_DATE,true)
		this.setSelected(obj)
	}

	private setEndDate(endDate: any) {
		let obj = {}
		obj[KEYS.TRIP_END_DATE] = endDate.format("YYYY-MM-DD")
		this.setSelected(obj)
	}
	private setNoOfTravellers(noOfTravellers: number) {
		let noOfTravellersObj = {}
		// let travellerAgesObj = {}
		noOfTravellersObj[KEYS.NO_OF_TRAVELLERS] = noOfTravellers

		// // if ages given are more than no of travellers... splice that array
		// var travellerAges = this.getTravellerAges()
		// if (travellerAges != undefined && travellerAges != null && travellerAges.length > 0 && travellerAges.length > noOfTravellers){
		// 	travellerAges = travellerAges.slice(0, noOfTravellers)
		// 	travellerAgesObj[KEYS.TRAVELLER_AGES] = travellerAges
		// }

		this.setSelected(noOfTravellersObj, true)
		// this.setSelected(travellerAgesObj)
	}
	private setTravellerAges(travellerAges: any) {
		let obj = {}
		obj[KEYS.TRAVELLER_AGES] = travellerAges

		this.setNoOfTravellers(travellerAges.length)
		this.setSelected(obj)
	}
	private setLongestTripDuration(longestTripDuration: any) {
		let obj = {}
		obj[KEYS.LONGEST_TRIP_DURATION] = parseInt(longestTripDuration.value)
		this.setSelected(obj)
	}
	private setIsIndianResident(isIndianResident: string | boolean) {
		let objOCIPIO = {}
		let objIsResident = {}
		objIsResident[KEYS.IS_RESIDENT] = CommonUtils.isTrue(isIndianResident)
		if (CommonUtils.isTrue(isIndianResident) == false){
			objOCIPIO[KEYS.DOES_HAEV_OCI_PIO_CARD] = true
			this.setSelected(objOCIPIO, true)
		}	
		this.setSelected(objIsResident)
	}
	private setIndianResidenceDuration(IndianResidenceDuration:any) {
		let obj = {}
		obj[KEYS.INDIAN_RESIDENT_PERIOD] = IndianResidenceDuration.value
		this.setSelected(obj)
	}
	private setOCIPIOCardInfo(doesHaveOCIPIOCard: string | boolean) {
		let obj = {}
		obj[KEYS.DOES_HAEV_OCI_PIO_CARD] = CommonUtils.isTrue(doesHaveOCIPIOCard)
		this.setSelected(obj)
	}
	private setIsTripStartIndia(isTripStartIndia: string | boolean) {
		let obj = {}
		obj[KEYS.IS_TRIP_START_INDIA] = CommonUtils.isTrue(isTripStartIndia)
		this.setSelected(obj)
	}
	public GTMViewQuotes(){
		var travellerAges = this.getTravellerAges()
		travellerAges = travellerAges.join('||')

		var destinations
		var tripType = this.getTripType()
		if (tripType == 'SINGLE'){
			destinations = this.getSelectedCountries()
			destinations = destinations.join('||')
		}
		else {
			destinations = this.getSelectedRegion()
		}
		

		var daysToStartDate
		var startDate = this.getTripStartDate()
		startDate = startDate.startOf('day')
		var today = moment().startOf('day')
		daysToStartDate  = startDate.diff(today, 'days') 

		var isResident = this.getIsIndianResident()
		var residentFor = this.getIndianResidenceDuration()

		var extra_params = {
			param1: destinations,
			param2: travellerAges,
			param3: (isResident)?'Yes':'No',
			param4:'Yes',
			param5:'Yes',
		}
			// param4: (daysToStartDate > 2) ? "More than 2 days" : "Under 2 days",
			// param6: (daysToStartDate > 2) ? "Show All Results" : "PED Form"


		// if (isResident) {
			 GTMUtils.gtm_push('HomeTopTravelEventGetQuote', 'Homepage Top Travel - Get Quote', 'Generate Quote', '', extra_params)
		// }
		// else{
		// 	extra_params['param5'] = residentFor
		// 	GTMUtils.gtm_push('EventTravelNRIViewQuotes', 'Travel - NRI View Quotes', tripType, '', extra_params)
		// }
	}

 	private _actionHandler:Function = (action:any) => {
 		
		 switch(action.actionType) {
		 	// add actions here
			 case ActionEnum.SET_TRIP_TYPE:
				 this.setTripType(action.value)
		 		break
			 case ActionEnum.SET_SELECTED_COUNTRIES:
				 this.setSelectedCountries(action.value)
				 break
			 case ActionEnum.SET_SELECTED_REGION:
				 this.setSelectedRegion(action.value)
				 break
			 case ActionEnum.SET_START_DATE:
				 this.setStartDate(action.value)
				 break
			 case ActionEnum.SET_END_DATE:
				 this.setEndDate(action.value)
				 break
			 case ActionEnum.SET_NO_OF_TRAVELLERS:
				 this.setNoOfTravellers(action.value)
				 break
			 case ActionEnum.SET_LONGEST_TRIP_DURATION:
				 this.setLongestTripDuration(action.value)
				 break
			 case ActionEnum.SET_TRAVELLER_RESIDENT_LOCATION:
				 this.setIsIndianResident(action.value)
				 break
			 case ActionEnum.SET_INDIAN_RESIDENCE_DURATION:
				 this.setIndianResidenceDuration(action.value)
				 break
			 case ActionEnum.SET_TRAVELLER_AGES:
				 this.setTravellerAges(action.value)
				 break
			 case ActionEnum.SET_OCI_PIO_CARD_INFO:
				 this.setOCIPIOCardInfo(action.value)
				 break
			 case ActionEnum.SET_TRIP_START_LOCATION:
				 this.setIsTripStartIndia(action.value)
				 break
			 case ActionEnum.GTM_VIEW_TRAVEL_QUOTES:
				 // this.GTMViewQuotes()
				 break
		}
		if (action.callback){
			 action.callback()
		}
	}
}

var travelStore:TravelStore;

export = (function(){
	if(!travelStore){
		travelStore = new TravelStore(KEYS.PARENT)
		// API calls to initiate data, if any
	}
	return travelStore
}())