import BaseStore = require('../../common/BaseStore')
import TravelStore = require('./TravelStore')
import ActionEnum = require('../constants/ActionEnum')
import StorageMap = require('../constants/StorageMap')
import CommonUtils = require('../../common/utils/CommonUtils')
import moment = require('moment')

var KEYS = StorageMap.TRAVEL;

class ValidationStore extends BaseStore {
	isPerformAllValidations
	constructor(_namespace: string) {
		super(_namespace)
		this.registerDispatcherHandler(this._actionHandler)
		this.isPerformAllValidations = false
	}

	//public getters here
	getIsValid(performValidation?:boolean){

		var isPerformAllValidations = (performValidation===undefined)?this.isPerformAllValidations : performValidation

		if (isPerformAllValidations) 
		{
			var tripType = TravelStore.getTripType()
			var datesErrorObj = this.getTravelDatesError(true)
			var countriesErrorObj = this.getCountriesError(true)
			var regionsErrorObj = this.getRegionsError(true)
			var agesErrorObj = this.getAgesError(true)
			var longestTripDurationError = this.getLongestTripDurationError(true)
			var isIndianResident = TravelStore.getIsIndianResident()
			var OCIPIOCard = TravelStore.getDoesHaveOCIPIOCard()
			var indianResidenceDuration = TravelStore.getIndianResidenceDuration()
			var startDate = TravelStore.getTripStartDate()
			var longestTrip = TravelStore.getLongestTripDuration()
			if (tripType == "SINGLE") 
			{
				if (countriesErrorObj['isValid'] == false || datesErrorObj['isValid'] == false || agesErrorObj['isValid'] == false) 
				{
					return false
				}
				else if (isIndianResident == false && OCIPIOCard == true && indianResidenceDuration == undefined) {
					return false
				}
				else {
					return true
				}
			}
			else if (tripType == "MULTI") 
			{
				if (regionsErrorObj['isValid'] == false || longestTripDurationError ['isValid'] == false || startDate == undefined || longestTrip == undefined || agesErrorObj['isValid'] == false) {
					return false
				}
				else if (isIndianResident == false && OCIPIOCard == true && indianResidenceDuration == undefined) {
					return false
				}
				else {
					return true
				}
			}
			else {
				return false
			}
		}
		else {
			return false
		}
	}

	getTravelDatesError(performValidation?:any){
		var tripType = TravelStore.getTripType()
		var datesErrorObj = {}
		datesErrorObj['isValid'] = true
		datesErrorObj['errorMessage'] = ""
		var performValidation = (performValidation===undefined)? this.isPerformAllValidations : performValidation;


		var tripStartDate = TravelStore.getTripStartDate()
		var tripEndDate = TravelStore.getTripEndDate()

		if (performValidation && 
				(tripType == "SINGLE" && (tripStartDate == undefined || tripEndDate == undefined))) {
			datesErrorObj['isValid'] = false
			datesErrorObj['errorMessage'] = "Please select your travel start and end dates"

		}else if(performValidation && (tripType == "MULTI" && tripStartDate == undefined)){
			datesErrorObj['isValid'] = false
			datesErrorObj['errorMessage'] = "Please select your travel date"			
		}
		else if (tripType == "SINGLE" && tripStartDate != undefined && tripEndDate != undefined &&
			moment(tripEndDate).isBefore(tripStartDate)){
			datesErrorObj['isValid'] = false
			// datesErrorObj['errorMessage'] = "Hello time traveler! End date cannot come before start date."
		}
		return datesErrorObj
	}

	getCountriesError(performValidation?:any){
		var errorObj = {}
		errorObj['isValid'] = true
		errorObj['errorMessage'] = ""
		var performValidation = (performValidation===undefined)? this.isPerformAllValidations : performValidation;

		var selectedCountries = TravelStore.getSelectedCountries()
		if(performValidation && 
				(selectedCountries == undefined || 
					selectedCountries == null || selectedCountries.length <=0)){
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "Please tell us where are you travelling to?"

		}
		return errorObj
	}

	getRegionsError(performValidation?:any) {
		var errorObj = {}
		errorObj['isValid'] = true
		errorObj['errorMessage'] = ""

		var performValidation = (performValidation===undefined)? this.isPerformAllValidations : performValidation;
		var selectedRegion = TravelStore.getSelectedRegion()
		if (performValidation &&
			(selectedRegion == undefined ||
				selectedRegion == null || selectedRegion == "")) {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "Please select a region"
		}
		return errorObj
	}

	getAgesError(performValidation?:any){
		var errorObj = {}
		errorObj['isValid'] = true
		errorObj['errorMessage'] = ""
		var performValidation = (performValidation===undefined)? this.isPerformAllValidations : performValidation;

		var agesArrayTemp = TravelStore.getTravellerAges()
		var agesArray = []
		agesArrayTemp.map(function(age){
			if(age!=null){
				agesArray.push(age)
			}
		})


		var noOfTravellers = TravelStore.getNoOfTravellers()
		var tripType = TravelStore.getTripType()
		if (performValidation &&
			(agesArray == undefined ||
				agesArray == null || agesArray.length <= 0 ||
				agesArray.length < noOfTravellers)) {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = 'Enter Age'
		}
		else if (performValidation){
			var isFoundAdult = false
			var isAboveAge = false
			var isBelowOneYear = false
			for(var age of agesArray){
				if(parseInt(age) > 17){
					isFoundAdult = true				
				}
				if (parseInt(age) > 99) {
					isAboveAge = true
				}
				if(parseInt(age) <= 0){
					isBelowOneYear = true
				}
			}
			if (isAboveAge == true) {
				errorObj['isValid'] = false
				errorObj['errorMessage'] = "Please enter a valid age below 100 years"

			}
			else if(isBelowOneYear == true){
				errorObj['isValid'] = false
				errorObj['errorMessage'] = "Invalid age.Traveller age(s) must be 1 year or above"	
			}
			else if (tripType == "MULTI" && isFoundAdult == false) {
				errorObj['isValid'] = false
				errorObj['errorMessage'] = "Traveller must be atleast 18 years old to buy an annual multitrip travel policy"

			}
			else if (isFoundAdult == false && noOfTravellers > 1) {
				errorObj['isValid'] = false
				errorObj['errorMessage'] = "The policy should include atleast 1 adult member"
			}
		}
		return errorObj
	}

	getLongestTripDurationError(performValidation?:any){
		var errorObj = {}
		errorObj['isValid'] = true
		errorObj['errorMessage'] = ""

		var longestTripDuration = TravelStore.getLongestTripDuration()
		var performValidation = (performValidation===undefined)? this.isPerformAllValidations : performValidation;

		if (performValidation &&
			(longestTripDuration == undefined ||
				longestTripDuration == null || longestTripDuration == "")) {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "Please select duration of the longest trip you plan to take"
		}
		return errorObj
	}

	getOCIPIOError(performValidation?:any) {
		var performValidation = (performValidation===undefined)? this.isPerformAllValidations : performValidation;
		var errorObj = {}
		errorObj['isValid'] = true
		errorObj['errorMessage'] = ""

		var selectedOCIPIO = TravelStore.getDoesHaveOCIPIOCard()
		if (performValidation &&
			(selectedOCIPIO == undefined || selectedOCIPIO == null)) {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "Please select how long the travellers have been in India"
		}
		return errorObj
	}

	getIndianResidenceDurationError(performValidation?:any) {
		var performValidation = (performValidation===undefined)? this.isPerformAllValidations : performValidation;
		var errorObj = {}
		errorObj['isValid'] = true
		errorObj['errorMessage'] = ""

		var indianResidenceDuration = TravelStore.getIndianResidenceDuration()
		if (performValidation &&
			(indianResidenceDuration == undefined || indianResidenceDuration == null)) {
			errorObj['isValid'] = false
			errorObj['errorMessage'] = "Please select a option"
		}
		return errorObj
	}


	//

	//private setters here
	//

	private _actionHandler: Function = (action: any) => {
		
		switch (action.actionType) {
			// add actions here
			case ActionEnum.VALIDATE:
				this.isPerformAllValidations = true
				validationStore.emitChange()
				break
		}
		if (action.callback) {
			action.callback()
		}
	}
}

var validationStore: ValidationStore;

export = (function() {
	if (!validationStore) {
		validationStore = new ValidationStore(KEYS.PARENT)
		// API calls to initiate data, if any
	}
	return validationStore
} ())