/// <reference path="../typings/tsd.d.ts" />
import React = require('react')
import BaseComponent = require('../common/BaseComponent')
import MultiSelect = require('../common/react-component/MultiSelect')
import NumberInput = require('../common/react-component/NumberInput')
import Calendar = require('../common/react-component/Calendar')
import Button = require('../common/react-component/Button')
import TravellerAges = require('./components/TravellerAges')
import RadioInput = require('../common/new-components/RadioInput')
import TravelDatePicker = require('./components/TravelDatePickerNew')
import AddTravelers = require('./components/AddTravelers');
import Modal = require('../common/new-components/Modal')

import Constants = require('./constants/constants')
import membersAge = require('./constants/MembersAge')
import AppActions = require('./actions/AppActions')
import TravelStore = require('./stores/TravelStore')
import ValidationStore = require('./stores/ValidationStore')
import TransitionUtils = require('../common/utils/TransitionUtils')
import moment = require('moment')

import Headings = require('../common/constants/Headings')
let heading = Headings.TRAVEL_MULTITRIP

var defaultTripType = Constants.tripType[1].value
var CountriesList = require('./constants/travel-places')
var countries = []
for (var i = 0; i < CountriesList.length; i++){
	let countryObj = {}
	countryObj['key'] = CountriesList[i][1]
	countryObj['value'] = CountriesList[i][0]
	countryObj['priority'] = 'low';
	countries.push(countryObj)
}

import CommonUtils = require('../common/utils/CommonUtils')
interface ITravelState {
	tripType:String,
	selectedCountries:any,
	selectedRegion: string,
	tripStartDate:any,
	tripEndDate:any,
	noOfTravellers:number,
	longestTripDuration:number,
	travellerAges:any,
	isIndianResident:boolean,
	doesHaveOCIPIOCard: boolean,
	indianResidenceDuration: string,
	isTripStartIndia: boolean,
	noOfTripDays:number,
	isStopSubmission:boolean,
	AddTravelersDefaultOpen:boolean,
	buttonType:string,
	showErrorsOfStep:string,
}

function getStateFromStores(): any {
	return {
		tripType: TravelStore.getTripType(),
		selectedCountries: TravelStore.getSelectedCountries(),
		tripStartDate: TravelStore.getTripStartDate(),
		tripEndDate: TravelStore.getTripEndDate(),
		noOfTravellers:TravelStore.getNoOfTravellers(),
		travellerAges:TravelStore.getTravellerAges(),
		longestTripDuration:TravelStore.getLongestTripDuration(),
		selectedRegion:TravelStore.getSelectedRegion(),
		isIndianResident: TravelStore.getIsIndianResident(),
		indianResidenceDuration:TravelStore.getIndianResidenceDuration(),
		doesHaveOCIPIOCard:TravelStore.getDoesHaveOCIPIOCard(),
		isTripStartIndia:TravelStore.getIsTripStartIndia(),
		noOfTripDays:TravelStore.getNoOfTripDays(),
		isStopSubmission:TravelStore.getIsStopSubmission()
	}
}

const STORES = [TravelStore, ValidationStore]

class Travel extends BaseComponent {
	refs: any;
	state: ITravelState;
	constructor(props) {
		super(props, {
			stores:STORES,
			getStateFromStores: getStateFromStores
		})

		this.state.AddTravelersDefaultOpen = false
		this.state.showErrorsOfStep = 'step1'
	}

	componentWillMount(){
		AppActions.setTripType(defaultTripType)
	}

	onRegionSelect(value: any) {
		AppActions.setSelectedRegion(value)
	}
	
	onStartDateSelect(value: any) {
		AppActions.setstartDate(value)
	}

	onLongestTripDurationSelect(value: any) {
		
		AppActions.setLongestTripDuration(value)
	}
	onResidenceLocationChange(item: any) {
		
		AppActions.setTravellerResidentLocation(item)
	}
	onChangeOCIPIOCard(item:any){
		AppActions.setOCIPIOCardInfo(item)
	}
	onIndianResidenceDurationChange(value:any){
		AppActions.setIndianResidenceDuration(value)
	}
	onTripStartLocationChange(item:any){
		AppActions.setTripStartLocation(item)
	}

	onTravellerAgesChange(item:any){
		var ageArray = []
			ageArray.push(item.value)
		AppActions.setTravellerAges(ageArray)
	}
	
	onSubmit(){
		var isValid = ValidationStore.getIsValid(true)
		var displayStep2Flag = this.displayStep2()

		if(isValid){
			AppActions.validate(this.routeToResults.bind(this))
		}else{
			var showErrorsOfStep

			if(displayStep2Flag===true){
				showErrorsOfStep = 'step2'
			}else{
				showErrorsOfStep = 'step1'
			}
			this.setState({
				showErrorsOfStep
			},function(){
				AppActions.validate(this.routeToResults.bind(this))
			})
		}
	}

	routeToResults(){
		var isValid = ValidationStore.getIsValid()
		if(isValid===true){
			this.setState({
				buttonType:'loading'
			},function(){
				TravelStore.GTMViewQuotes()
				TransitionUtils.jumpTo(window['TRAVEL_RESULT_PAGE']+'#/results')
			})	
		}
	}

	displayStep2(){

		var datesErrorObj = ValidationStore.getTravelDatesError(true)
		var regionsErrorObj = ValidationStore.getRegionsError(true)
		var agesErrorObj = ValidationStore.getAgesError(true)
		var longestTripErrorObj = ValidationStore.getLongestTripDurationError(true)		

		return (regionsErrorObj['isValid'] === true && datesErrorObj['isValid'] === true && longestTripErrorObj['isValid'] === true && agesErrorObj['isValid'] === true)? true : false
	}

	goBack(){
		TransitionUtils.transitTo('/travel-insurance/')
	}

	render() {

		// Validate on Submit
		var datesErrorObj = ValidationStore.getTravelDatesError()
		var regionsErrorObj = ValidationStore.getRegionsError()
		var OCIPIOErrorObj = ValidationStore.getOCIPIOError()
		var residenceDurationErrorObj = ValidationStore.getIndianResidenceDurationError()
		var agesErrorObj = ValidationStore.getAgesError()
		var longestTripErrorObj = ValidationStore.getLongestTripDurationError()

		var travellerAge = (this.state.travellerAges===undefined)? undefined : this.state.travellerAges[0]
		var dateSectionClass = 'col-sm-6'
		var startDateSelectClass = this.state.tripStartDate == undefined || this.state.tripStartDate == null 
									|| this.state.tripStartDate == "" ? 'empty' : ''
		var endDateSelectClass = this.state.tripEndDate == undefined || this.state.tripEndDate == null 
			|| this.state.tripEndDate == "" ? 'empty' : ''
		var minTripEndDate = this.state.tripStartDate == undefined ? moment().subtract("day", 1) : this.state.tripStartDate
		var endDateDisplayMonth = minTripEndDate
		var displayStep2Flag = this.displayStep2()

		var multiTripBlock = (
			<div>
                <div className="form-row first w--travel_form">
            		<div className ="multi_trip_select">
						<MultiSelect optionsText="label" dataSource={Constants.regions} values={this.state.selectedRegion} onChange={this.onRegionSelect.bind(this) } placeHolder="Where would you be travelling to?" header='Select region' labelText="Travelling to" isSearchable={false}/>

						{
						regionsErrorObj['isValid'] == false ?
						<div className="w--error"> {regionsErrorObj['errorMessage']} </div>
							:
							''
						}
					</div>
					<div className="travel_date_select">	
					<Calendar labelText="Trip Start Date" minDate={moment().subtract("day", 1) } onSelectDate={this.onStartDateSelect.bind(this) } defaultDate={this.state.tripStartDate} selectedClass={startDateSelectClass}  prevMonths={0} nextMonths={1}/>
					{
						datesErrorObj['isValid'] == false ?
						<div className="w--error"> {datesErrorObj['errorMessage']} </div>
						:
						''
					}
					</div>
					
					<div className="travel_trip_duration">
						<MultiSelect dataSource={Constants.durationOptions} values={this.state.longestTripDuration} placeHolder="Longest trip" onChange={this.onLongestTripDurationSelect.bind(this) } labelText="Longest Trip" isSearchable={false}  header='Duration of your longest trip.' />
						{
							longestTripErrorObj['isValid'] == false ?
							<div className="w--error"> {longestTripErrorObj['errorMessage']} </div>
							:
							''
						}
					</div>


					<div className="traveller_age_select">

						<MultiSelect dataSource={membersAge} optionsValue='value' optionsKey='key' values={travellerAge} placeHolder="Your age"
							onChange={this.onTravellerAgesChange.bind(this) } labelText="Your Age" isSearchable={true} header='Your Age' />

						{
							agesErrorObj['isValid'] === false ?
							<div className="w--error"> {agesErrorObj['errorMessage']} </div>
							:
							''
						}
					</div>


					{ 
						displayStep2Flag === true?
						(
							<div className="travel_info_selector">
								<div className="column-2">
									<RadioInput optionsList={Constants.YesNoObject} value={this.state.isIndianResident} onChange={this.onResidenceLocationChange.bind(this) } optionsKey='label' optionsValue='value' customClass="small" labelText="Are you resident of India?"></RadioInput>
								</div>
								<div className="column-2">
									<RadioInput optionsList={Constants.YesNoObject} optionsKey='label' optionsValue='value'  value={this.state.isTripStartIndia} onChange={this.onTripStartLocationChange.bind(this) } customClass="small" labelText="Is this trip starting from India?"></RadioInput>
									{
									this.state.isTripStartIndia === false ?
										<div className="w--error">Insurance companies only cover trips which will be starting from India.
										</div>
											:''
									}
								</div>
								<div>
									{
										this.state.isIndianResident === false ?
										<div className="nri_block">
											<RadioInput optionsList={Constants.YesNoObject} optionsKey='label' optionsValue='value' value={this.state.doesHaveOCIPIOCard} onChange={this.onChangeOCIPIOCard.bind(this) } customClass="small" labelText="Do you have OCI/PIO card?"></RadioInput>
									
												{
												OCIPIOErrorObj['isValid'] == false ?
													<div className="w--error"> {OCIPIOErrorObj['errorMessage']} </div>
													:
													''
												}

												{
												this.state.doesHaveOCIPIOCard != undefined && this.state.doesHaveOCIPIOCard === true ?
														<div>
															<span className="grey_label">How long have you been in India?</span>
															<MultiSelect optionsText="label" dataSource={Constants.travellerResidenceDurationOptions} values={this.state.indianResidenceDuration}
																onChange={this.onIndianResidenceDurationChange.bind(this) } labelText="Selected Duration" isSearchable={false} placeHolder="Select Duration..." header='Select duration'  />
															{
															residenceDurationErrorObj['isValid'] == false && this.state.showErrorsOfStep=='step2' ?
															<div className="w--error"> {residenceDurationErrorObj['errorMessage']} </div>
																:
																''
															}
														</div>
													:
													<div className="w--error">
														None of the insurance companies cover travellers who are not a
														resident of India and do not have an OCI/PIO card.
													</div>
												}	
										</div>
										:
										''
									}
								</div>

							</div>	
						)
						:
						null
					}
				</div>
				<div className="form-row">
					<Button customClass="w--button w--button--orange w--button--large" disabled={this.state.isStopSubmission == true ? true : false} onClick={this.onSubmit.bind(this) } title='BUY NOW' type={ (this.state.buttonType)?'loading':'' } />
				</div>
			</div>
		);

		return(
			<Modal header={heading} customClass="full_bg" ref='MemberPopup' children={multiTripBlock} isOpen={true} isShowCloseIcon={true} size='large' onClose={ this.goBack } customHeaderClass="modal_header--hero" />
		)

	}
}

export = Travel