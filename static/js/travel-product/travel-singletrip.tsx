/// <reference path="../typings/tsd.d.ts" />
import React = require('react')
import BaseComponent = require('../common/BaseComponent')
import MultiSelect = require('../common/react-component/MultiSelect-new')
import NumberInput = require('../common/react-component/NumberInput')
import Calendar = require('../common/react-component/Calendar')
import Button = require('../common/react-component/Button')
import TravellerAges = require('./components/TravellerAges')
import RadioInput = require('../common/new-components/RadioInput')
import TravelDatePicker = require('./components/TravelDatePickerNew')
import AddTravelers = require('./components/AddTravelers');

import Constants = require('./constants/constants')
import AppActions = require('./actions/AppActions')
import TravelStore = require('./stores/TravelStore')
import ValidationStore = require('./stores/ValidationStore')
import TransitionUtils = require('../common/utils/TransitionUtils')
import moment = require('moment')
var CountriesList = require('./constants/travel-places')
var defaultTripType = Constants.tripType[0].value

var countries = []
for (var i = 0; i < CountriesList.length; i++){
	let countryObj = {}
	countryObj['key'] = CountriesList[i][1]
	countryObj['value'] = CountriesList[i][0]
	countryObj['priority'] = 'low';
	countries.push(countryObj)
}

import CommonUtils = require('../common/utils/CommonUtils')
interface ITravelState {
	tripType:String,
	selectedCountries:any,
	selectedRegion: string,
	tripStartDate:any,
	tripEndDate:any,
	noOfTravellers:number,
	longestTripDuration:number,
	travellerAges:any,
	isIndianResident:boolean,
	doesHaveOCIPIOCard: boolean,
	indianResidenceDuration: string,
	isTripStartIndia: boolean,
	noOfTripDays:number,
	isStopSubmission:boolean,
	AddTravelersDefaultOpen:boolean,
	buttonType:string,
	showErrors:any,
	showErrorsOfStep:string,
}

function getStateFromStores(): any {
	return {
		tripType: TravelStore.getTripType(),
		selectedCountries: TravelStore.getSelectedCountries(),
		tripStartDate: TravelStore.getTripStartDate(),
		tripEndDate: TravelStore.getTripEndDate(),
		noOfTravellers:TravelStore.getNoOfTravellers(),
		travellerAges:TravelStore.getTravellerAges(),
		longestTripDuration:TravelStore.getLongestTripDuration(),
		selectedRegion:TravelStore.getSelectedRegion(),
		isIndianResident: TravelStore.getIsIndianResident(),
		indianResidenceDuration:TravelStore.getIndianResidenceDuration(),
		doesHaveOCIPIOCard:TravelStore.getDoesHaveOCIPIOCard(),
		isTripStartIndia:TravelStore.getIsTripStartIndia(),
		noOfTripDays:TravelStore.getNoOfTripDays(),
		isStopSubmission:TravelStore.getIsStopSubmission()
	}
}
/*
const PATH = '/components/travel/'
const QUERY_PARAMS = ['app']*/
const STORES = [TravelStore, ValidationStore]

//Constants to MOVE


//

class Travel extends BaseComponent {
	refs: any;
	state: ITravelState;
	constructor(props) {
		super(props, {
			stores:STORES,
			getStateFromStores: getStateFromStores
		})

		this.state.AddTravelersDefaultOpen = false
		this.state.showErrors = false
		this.state.showErrorsOfStep = 'step1'
	}

	componentWillMount(){
		AppActions.setTripType(defaultTripType)
	}

	onTripTypeChange(item:any){
		AppActions.setTripType(item.value)
	}

	defaultStepIsValid(){
		
		var datesErrorObj = ValidationStore.getTravelDatesError(true)
		var countriesErrorObj = ValidationStore.getCountriesError(true)
		var agesErrorObj = ValidationStore.getAgesError(true)
		
		return (datesErrorObj['isValid']==true && countriesErrorObj['isValid']===true && agesErrorObj['isValid']===true)? true:false		
	}

	onCountrySelect(value:string){
		let selectedCountries = []
		selectedCountries = this.refs['CountrySelector'].getValues()

		AppActions.setSelectedCountries(selectedCountries)
	}

	onStartDateSelect(value: any) {
		AppActions.setstartDate(value,this.onCancel.bind(this))
	}
	onEndDateSelect(value: any) {
		AppActions.setEndDate(value,this.handleAddTravelerDefaultOpen.bind(this))
	}

	handleAddTravelerDefaultOpen(){
		var datesErrorObj = ValidationStore.getTravelDatesError(true)
		var countriesErrorObj = ValidationStore.getCountriesError(true)
		var agesErrorObj = ValidationStore.getAgesError(true)

		if(datesErrorObj['isValid']===true && countriesErrorObj['isValid']===true && agesErrorObj['isValid']===false){
			this.setState({
				AddTravelersDefaultOpen:true
			})
		}
	}

	onResidenceLocationChange(item: any) {
		
		AppActions.setTravellerResidentLocation(item)
	}
	onChangeOCIPIOCard(item:any){
		AppActions.setOCIPIOCardInfo(item)
	}
	onIndianResidenceDurationChange(value:any){
		AppActions.setIndianResidenceDuration(value)
	}
	onTripStartLocationChange(item:any){
		AppActions.setTripStartLocation(item)
	}
	onDone(ageArray:any){
		var noOfTravellers = ageArray.length;
		var self = this;

		var newAgeArray = ageArray.map(function(age){
			return parseInt(age);
		})
		
		AppActions.setTravellerAges(newAgeArray,function(){
			self.setState({
				AddTravelersDefaultOpen : false,
			})
		})
		
	}
	onSubmit(){
		var isValid = ValidationStore.getIsValid(true)
		var displayStep2Flag = this.defaultStepIsValid()

		if(isValid){
			AppActions.validate(this.routeToResults.bind(this))
		}else{
			var showErrorsOfStep

			if(displayStep2Flag===true){
				showErrorsOfStep = 'step2'
			}else{
				showErrorsOfStep = 'step1'
			}
			this.setState({
				showErrors:true,
				showErrorsOfStep
			},function(){
				AppActions.validate(this.routeToResults.bind(this))
			})
		}

	}
	routeToResults(){
		var isValid = ValidationStore.getIsValid()
		if(isValid===true){
			this.setState({
				buttonType:'loading'
			},function(){
				TravelStore.GTMViewQuotes()
				TransitionUtils.jumpTo(window['TRAVEL_RESULT_PAGE']+'#/results')
				// window.location = window['TRAVEL_RESULT_PAGE']+'#/results'
			})	
		}
	}

	onCancel(){
		this.setState({
			AddTravelersDefaultOpen : false,
		})
	}

	render() {

		// Validate on Submit
		var datesErrorObj = ValidationStore.getTravelDatesError()
		var countriesErrorObj = ValidationStore.getCountriesError()
		var OCIPIOErrorObj = ValidationStore.getOCIPIOError()
		var residenceDurationErrorObj = ValidationStore.getIndianResidenceDurationError()
		var agesErrorObj = ValidationStore.getAgesError()
		var longestTripErrorObj = ValidationStore.getLongestTripDurationError()

		//	Validate on each change
		var ageIsValidFlag = ValidationStore.getAgesError(true);	


		var dateSectionClass = this.state.tripType == "SINGLE" ? 'col-sm-12' : 'col-sm-6'
		var startDateSelectClass = this.state.tripStartDate == undefined || this.state.tripStartDate == null 
									|| this.state.tripStartDate == "" ? 'empty' : ''
		var endDateSelectClass = this.state.tripEndDate == undefined || this.state.tripEndDate == null 
			|| this.state.tripEndDate == "" ? 'empty' : ''
		var minTripEndDate = this.state.tripStartDate == undefined ? moment().subtract("day", 1) : this.state.tripStartDate
		var endDateDisplayMonth = minTripEndDate
		var displayStep2Flag = this.defaultStepIsValid()
		var multiCountryClass = (this.state.selectedCountries.length>0)?'multi_country_dropdown is_selected':'multi_country_dropdown'

		return (
			<div>
                <div className="form-row first w--travel_form">
            		<div className ={multiCountryClass}>
					<MultiSelect ref='CountrySelector' dataSource={CountriesList} groupBy='priority' values={this.state.selectedCountries} multiSelect={true} 
						onChange={this.onCountrySelect.bind(this) } placeHolder="Which countries are you visiting?" labelText="Travelling to" visibleGroups={['Top Countries']} groupPriorities={['Top Countries']} minimisedLabels={ true } />
						{
							countriesErrorObj['isValid'] == false  && this.state.showErrors===true ?
							<div className="w--error"> {countriesErrorObj['errorMessage']} </div>
							:
							''
						}
					</div>
					<div className="travel_date_picker">	
					<TravelDatePicker tripStartDate={this.state.tripStartDate} onStartDateSelect={ this.onStartDateSelect.bind(this) } onEndDateSelect={ this.onEndDateSelect.bind(this) } tripEndDate={this.state.tripEndDate} />
					{
						datesErrorObj['isValid'] == false && this.state.showErrors===true ?
						<div className="w--error"> {datesErrorObj['errorMessage']} </div>
						:
						''
					}
					</div>
					<div className="traveller_select">
					<AddTravelers travellerAges={this.state.travellerAges} onDone={ this.onDone.bind(this) } validationStatus={ ageIsValidFlag } isValid={ ageIsValidFlag } onCancel={ this.onCancel.bind(this) } />
					{
						agesErrorObj['isValid'] === false && this.state.showErrors===true ?
						<div className="w--error">Please select number of travellers.</div>
						:
						''
					}
					</div>

					{ 
						displayStep2Flag === true?
						(
							<div className="travel_info_selector">
								<div className="column-2">
									<RadioInput optionsList={Constants.YesNoObject} value={this.state.isIndianResident} onChange={this.onResidenceLocationChange.bind(this) } optionsKey='label' optionsValue='value' customClass="small" labelText="Are all travellers resident of India?"></RadioInput>
								</div>
								<div className="column-2">
									<RadioInput optionsList={Constants.YesNoObject} optionsKey='label' optionsValue='value'  value={this.state.isTripStartIndia} onChange={this.onTripStartLocationChange.bind(this) } customClass="small" labelText="Is this trip starting from India?"></RadioInput>
									{
									this.state.isTripStartIndia === false ?
										<div className="w--error">Insurance companies only cover trips which will be starting from India.
										</div>
											:''
									}
								</div>
								<div>
									{
										this.state.isIndianResident === false ?
										<div className="nri_block">
											<RadioInput optionsList={Constants.YesNoObject} optionsKey='label' optionsValue='value' value={this.state.doesHaveOCIPIOCard} onChange={this.onChangeOCIPIOCard.bind(this) } customClass="small" labelText="Do all travellers have OCI/PIO card?"></RadioInput>
									
												{
												OCIPIOErrorObj['isValid'] == false && this.state.showErrors===true ?
													<div className="w--error"> {OCIPIOErrorObj['errorMessage']} </div>
													:
													''
												}

												{
												this.state.doesHaveOCIPIOCard != undefined && this.state.doesHaveOCIPIOCard === true ?
														<div>
															<span className="grey_label">How long have you been in India?</span>
															<MultiSelect optionsText="label" dataSource={Constants.travellerResidenceDurationOptions} values={this.state.indianResidenceDuration}
																onChange={this.onIndianResidenceDurationChange.bind(this) } labelText="Selected Duration" isSearchable={false} placeHolder="Select duration" header='Select duration'  />
															{
															residenceDurationErrorObj['isValid'] == false && this.state.showErrors===true && this.state.showErrorsOfStep=='step2' ?
															<div className="w--error"> {residenceDurationErrorObj['errorMessage']} </div>
																:
																''
															}
														</div>
													:
													<div className="w--error">
														None of the insurance companies cover travellers who are not a
														resident of India and do not have an OCI/PIO card.
													</div>
												}	
										</div>
										:
										''
									}
								</div>

							</div>	
						)
						:
						null
					}
				</div>
				<div className="form-row">
					<Button customClass="w--button w--button--orange w--button--large" disabled={this.state.isStopSubmission == true ? true : false} onClick={this.onSubmit.bind(this) } title='BUY NOW' type={ (this.state.buttonType)?'loading':'' } />
				</div>
			</div>
		);
	}
}

export = Travel