declare var isEqual: {
    (value: any,
        other: any,
        customizer?: IsEqualCustomizer,
        thisArg?: any
    ): boolean;
}

interface IsEqualCustomizer {
    (value: any, other: any, indexOrKey?: number | string): boolean;
}

declare module "lodash.isequal" {
    export = isEqual;
}
