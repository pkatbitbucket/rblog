declare var load: {(name:string, doNotParse?:any[]):string}
declare var save: {(name:string, val:string | number, opt?:any[]):void}
declare var remove: {(name:string, opt?:any[]):void}
declare var plugToRequest: {(req:any, res:any):void}
declare var setRawCookie: {(cookie:any):void}

declare module "react-cookie" {
	export = {
		load: load,
		save: save,
		remove: remove,
		plugToRequest: plugToRequest,
		setRawCookie: setRawCookie
	}
}