/*
 * In html, where ever you want the facebook button, insert the following code:
 *          <div id="fb-root"></div>
 *          <img class="facebook_btn img-responsive" style="cursor: pointer;" src="{{STATIC_URL}}img/sign_in_with_facebook.png"/>
 *
 * To get a google button, put :
 *          <img class="google_btn img-responsive" style="cursor: pointer;" src="{{STATIC_URL}}img/sign_in_with_google.png"/>
 *
 */


// -------------- Facebook Signin ------------------

window.fbAsyncInit = function() {
    FB.init({
        appId   : '545910808767230',
        oauth   : true,
        status  : true, // check login status
        cookie  : true, // enable cookies to allow the server to access the session
        xfbml   : true // parse XFBML
    });
};

var fb_login = function(){
    FB.login(function(response) {
        if (response.authResponse) {
            var fb_access_token = response.authResponse.accessToken;
            user_id = response.authResponse.userID; //get FB UID

            FB.api('/me', function(response) {
                fb_response_callback(response, fb_access_token);
            });
        } else {
            //user hit cancel button
        }
    }, {
        scope: 'user_birthday,user_relationships,email'
    })
};

(function() {
    var e = document.createElement('script');
    e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
    e.async = true;
    document.getElementById('fb-root').appendChild(e);
}());

// -------------- Google Signin ------------------

function onLoadCallback() {
    gapi.client.setApiKey(GOOGLE_API_KEY); //set your API KEY
}

(function() {
    var po = document.createElement('script');
    po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/client:plusone.js?onload=onLoadCallback';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(po, s);
})();

gplus_login = function() {
    var myParams = {
        'callback': loginFinishedCallback,
        'clientid': GOOGLE_OAUTH_KEY,
        'cookiepolicy': 'single_host_origin',
        'state' : CSRF_TOKEN,
        'application_name' : 'coverfox',
        'scope' : 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email'
    };
    gapi.auth.signIn(myParams);
};

var google_auth_result;
function loginFinishedCallback(authResult) {
    if (authResult) {
        if(authResult['status']['signed_in']) {
            if (authResult['error'] == undefined){
                google_auth_result = authResult;
                gapi.client.load('plus','v1', loadProfile);  // Trigger request to get the email address.
            } else {
                google_response_error();
            }
        }
    } else {
        return;
    }
}

function loadProfile() {
    var request = gapi.client.plus.people.get({'userId': 'me'});
    request.execute(google_response_callback);
};
