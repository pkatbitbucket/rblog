var path = require('path');
var webpack = require('webpack');
var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");

module.exports = {
    entry: {
        common: ['./common/common-modules.tsx'],
        widget: "./widget/entry.tsx",
        results: "./ResultsEntry.tsx"
    },
    output: {
        path: './build',
        filename: '[name].[hash].bundle.js',
        publicPath: (process.env.STATIC_URL || '/static/') + 'js/build/'
    },
    module: {
        loaders: [
            { test: /\.png$/, loader: "file-loader" },
            { test: /\.jpg$/, loader: "file-loader" },
            { test: /\.tsx?$/, loader: "typescript-simple-loader" },
            { test: /\.jsx?$/, loader: 'babel-loader' }
        ],
    },
    resolve: {
        extensions: ['', '.js', '.jsx','.ts', '.tsx']
    },
    plugins: [
        new CommonsChunkPlugin("common", "common.[hash].bundle.js"),
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        function() {
            this.plugin("done", function(stats) {
                require("fs").writeFileSync(
                    path.join(__dirname, "./build", "stats.json"),
                    JSON.stringify(stats.toJson())
                );
                require("fs").utimesSync(
                    path.join(__dirname, "../../base/settings.py"),
                    new Date(),
                    new Date()
                );
            });
        }
    ]
};
