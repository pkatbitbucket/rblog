import React = require('react')
import TransitionUtils = require('../../common/utils/TransitionUtils')
import URLS = require('../../common/constants/URLS')
import Button = require('../../common/react-component/Button')

var STATIC_URL:string = window['STATIC_URL']
const INDEXED_URLS = [
	URLS.HEALTH_SUPERTOPUP,
	URLS.TRAVEL_MULTITRIP,
]

class HealthTravelWidget extends React.Component<any, any> {
	constructor(props){
		super(props)	
	}
	goTo(evt,index){
		TransitionUtils.transitTo(INDEXED_URLS[index])
	}
	render() {
		return (
			<div>
				<div className="col-md-3 col-xs-6 xs-collapse">
					<div className="banner-content">
						<img className="circle-icon" src={STATIC_URL+'img/icons/icon_express-renewal.png'} data-cms-attr='src:image_url_disp_banner_icon1' />
						<h5 data-cms-content='disp_sec_header1'>Express Renewal</h5>
						<p data-cms-content='disp_sec_text1'>This got to be the<br/> fastest way to renew your<br/> insurance online</p>
						<Button data-cms-attr="href:disp_btn1" customClass="btn btn--white btn--lg btn--block" onClick={ this.goTo } customValueAttr={0} title='GET STARTED' />
					</div>
				</div>
				<div className="col-md-3 col-xs-6 xs-collapse">
					<div className="banner-content">
						<img className="circle-icon" src={STATIC_URL+'img/icons/icon_thirdparty.png'} data-cms-attr='src:image_url_disp_banner_icon2' />
						<h5 data-cms-content='disp_sec_header2'>Third Party Insurance</h5>
						<p data-cms-content='disp_sec_text2'>Buy third party insurance for<br/> your bike online. If that's<br/> what floats your boat.</p>
						<Button data-cms-attr="href:disp_btn2" customClass="btn btn--white btn--lg btn--block" onClick={ this.goTo } customValueAttr={1} title='GET STARTED' />
					</div>
				</div>
			</div>
		);
	}
}

export = HealthTravelWidget