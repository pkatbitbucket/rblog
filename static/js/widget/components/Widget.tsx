import React = require('react')
import TransitionUtils = require('../../common/utils/TransitionUtils')
import Spinner = require('../../common/react-component/Spinner')
import URLS = require('../../common/constants/URLS')
import Headings = require('../../common/constants/Headings')
import stashable = require('../../common/utils/stashable')
import ReactCSSTransitionGroup = require('react-addons-css-transition-group')

const ACTIVE_URLS = [
	URLS.CAR_LANDING,
	URLS.BIKE_LANDING,
	URLS.HEALTH_INDEMNITY,
	URLS.TRAVEL_SINGLETRIP,
	URLS.LIFE_LANDING,
	URLS.BIKE_FALLBACK,
	URLS.HEALTH_SUPERTOPUP,
	URLS.TRAVEL_MULTITRIP
]

const PASSIVE_URLS = [
	URLS.BIKE_RESULTS,
	URLS.BIKE_PROPOSAL,
]

const HEADINGS = [
	Headings.CAR,
	Headings.BIKE,
	Headings.HEALTH,
	Headings.TRAVEL,
	Headings.TERM_LIFE,
	Headings.BIKE,
	Headings.HEALTH_SUPER_TOPUP,
	Headings.TRAVEL_MULTITRIP
]

const COMPONENT_ON_DEMAND = [
	require('bundle?lazy&name=car!../../motor-product/landing-car'),
	require('bundle?lazy&name=bike!../../bike-product/bike'),
	require('bundle?lazy&name=health!../../new-health-product/health-indemnity'),
	require('bundle?lazy&name=travel!../../travel-product/travel-singletrip'),
	require('bundle?lazy&name=life!../../life-product/term-life')
]

const COMPONENT_LOADING = '__COMPONENT_LOADING__'

class Widget extends React.Component<any, any> {
	constructor(props){
		super(props)
		this.state = {index: undefined, component: undefined}
	}
	componentWillMount(){
		stashable.init()
	}

	getActiveIndex(){
		for(let index in ACTIVE_URLS){
			if(this.props.history.isActive(ACTIVE_URLS[index]))
				return +index
		}
	}
	getPassiveIndex(){
		for(let index in PASSIVE_URLS){
			if(window.location.pathname.indexOf(PASSIVE_URLS[index]) > -1)
				return +index
		}
	}
	goTo(index){
		
		if (this.props.route.updateURL){
			//window['_ENTRY_MODULE_'] === 'CMS' ? TransitionUtils.jumpTo(ACTIVE_URLS[index] + '/') : TransitionUtils.transitTo(ACTIVE_URLS[index])
			TransitionUtils.transitTo(ACTIVE_URLS[index])
		}else{
			window[COMPONENT_LOADING] = true
			COMPONENT_ON_DEMAND[index]((Component) => {
				window[COMPONENT_LOADING] = false
				this.setState({ component: <Component/> })
			})
		}
		this.setState({index: index})
	}
	hideNodes(){
		stashable.hide()
	}
	showNodes(){
		stashable.show()
	}

	render() {
		let sIndex = this.state.index == undefined ? this.getActiveIndex() : this.state.index
		let pIndex = this.getPassiveIndex() 
		let hidden = window.location.pathname!=='/' && pIndex != undefined
		/* I know I have done a lot of sins, but this can't be the worst one.' */
		let showSpinner = window[COMPONENT_LOADING]
		hidden ? this.hideNodes() : this.showNodes()
		
		let transitionName = 'slide-from-right'
		let headline = sIndex != undefined ? HEADINGS[sIndex] : HEADINGS[0]
		let child = this.state.index == undefined || this.getActiveIndex() ==5 || pIndex != undefined ? this.props.children : this.state.component
		return (
			<div>
				<h1 className='hero_header will-fade' hidden={hidden}>
					<ReactCSSTransitionGroup transitionName={ transitionName } transitionEnterTimeout={500} transitionLeaveTimeout={300}>
						<span className='hero_header__content' key={'_key_header' + sIndex} >{headline}</span>
					</ReactCSSTransitionGroup>
				</h1>
                {/*<p className='hero_sub hide'><span className='icon'><img src="{{ STATIC_URL }}/img/common/btn_play.png" alt="" /></span><span><a href='#'>See how, in 20 seconds</a></span></p>*/}
				<ul hidden={hidden} className="widget-nav will-fade">
                    <li className={sIndex === 0 || (window.location.pathname === '/' && this.state.index == undefined) ? 'active' : ''} onClick={this.goTo.bind(this, 0)} data-hover="Car">Car</li>
                    <li className={sIndex === 1 || sIndex === 5 ? 'active' : ''} onClick={this.goTo.bind(this, 1)} data-hover="Bike">Bike</li>
                    <li className={sIndex === 2 ? 'active' : ''} onClick={this.goTo.bind(this, 2)} data-hover="Health">Health</li>
                    <li className={sIndex === 3 ? 'active' : ''} onClick={this.goTo.bind(this, 3)} data-hover="Travel">Travel</li>
                    <li className={sIndex === 4 ? 'active' : ''} onClick={this.goTo.bind(this, 4)} data-hover="Term Life">Term Life</li>
                </ul>
                {hidden || sIndex > 5 ? child : <div className='widget_space'>
					<ReactCSSTransitionGroup transitionName={ transitionName } transitionEnterTimeout={500} transitionLeaveTimeout={300}>
						<div key={'_key' + sIndex}>{ showSpinner ? <Spinner/> : child }</div>
					</ReactCSSTransitionGroup>
				</div>}
			</div>
		);
	}
}

export = Widget