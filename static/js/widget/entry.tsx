/// <reference path="../typings/tsd.d.ts" />
import React = require('react')
import ReactDOM = require('react-dom')
import ReactRouter = require('react-router')
import history = require('../common/History')
import CommonUtils = require('../common/utils/CommonUtils')

var RR: any = ReactRouter
var Router = RR.Router
var Route = RR.Route
var Redirect = RR.Redirect
var IndexRedirect = RR.IndexRedirect
var IndexRoute = RR.IndexRoute

import URLS = require('../common/constants/URLS')
import Widget = require('./components/Widget')
import HealthTravelWidget = require('./components/Health-Travel-Widget')

CommonUtils.loadCustomUtils()

const COMPONENT_LOADER = '__COMPONENT_LOADING__'

const load = bundle => (location, cb) => {
	window[COMPONENT_LOADER] = true
	bundle(component => {
		window[COMPONENT_LOADER] = false
		cb(null, component)
	})
}

const Car = load(require('bundle?lazy&name=car!../motor-product/landing-car'))
const CarOld = load(require('bundle?lazy&name=car!../motor-product/car'))
const BikeQF = load(require('bundle?lazy&name=bike!../bike-product/bike'))
//const BikeFallBack = load(require('bundle?lazy&name=bike!../bike-product/components/BikeFallBackQuoteForm'))
const BikeRP = load(require('bundle?lazy&name=bike!../bike-product/components/BikeResults'))
const BikePF = load(require('bundle?lazy&name=bike!../bike-product/components/BikeProposal'))
const Health_Indemnity = load(require('bundle?lazy&name=health!../new-health-product/health-indemnity'))
const Health_SuperTopup = load(require('bundle?lazy&name=health!../new-health-product/health-supertopup'))
const Travel_SingleTrip = load(require('bundle?lazy&name=travel!../travel-product/travel-singletrip'))
const Travel_MultiTrip = load(require('bundle?lazy&name=travel!../travel-product/travel-multitrip'))
const TermLife = load(require('bundle?lazy&name=life!../life-product/term-life'))

const entryPath = '/'
const contentDivId = window['_ENTRY_MODULE_'] === 'CMS' ? 'cms-content' : 'content'
const widgetProps = {updateURL: false}


const WidgetRouter = (
	<Router history={history}>
		<Route path={entryPath} component={Widget} {...widgetProps}>
			<IndexRoute getComponent={Car}/>
			<Route path={URLS.CAR_LANDING} getComponent={Car} ></Route>
			<Route path={URLS.BIKE_LANDING} getComponent={BikeQF}></Route>
			{/*<Route path={URLS.BIKE_FALLBACK} getComponent={BikeFallBack}></Route>*/}
			<Route path={URLS.BIKE_RESULTS + "/:quoteId"} getComponent={BikeRP}></Route>
			<Route path={URLS.BIKE_PROPOSAL + "/:slug/:transactionId"} getComponent={BikePF}></Route>
			<Route path={URLS.TRAVEL_SINGLETRIP} getComponent={Travel_SingleTrip}></Route>
			<Route path={URLS.TRAVEL_MULTITRIP} getComponent={Travel_MultiTrip}></Route>
			<Route path={URLS.HEALTH_INDEMNITY} getComponent={Health_Indemnity}></Route>
			<Route path={URLS.HEALTH_SUPERTOPUP} getComponent={Health_SuperTopup}></Route>
			<Route path={URLS.LIFE_LANDING} getComponent={TermLife}></Route>
		</Route>
		<Redirect from="/" to={URLS.CAR_LANDING} />
		<Redirect from={URLS.BIKE_RESULTS} to={URLS.BIKE_LANDING} />
		<Redirect from={URLS.BIKE_PROPOSAL} to={URLS.BIKE_LANDING} />
	</Router>
)

/*var isOnTop
function reposition(onTop: boolean): void{
	if(isOnTop === onTop)
		return
	ReactDOM.unmountComponentAtNode(document.getElementById(!onTop ? contentDivId : 'content-bottom'))
	ReactDOM.render(WidgetRouter, document.getElementById(onTop ? contentDivId : 'content-bottom'))
	isOnTop = onTop
}

var ON_TOP_URLS = [
	URLS.BIKE_RESULTS,
	URLS.BIKE_PROPOSAL
]
function isOnTopURL(): boolean{
	let is = false
	for(let key in ON_TOP_URLS){
		if(window.location.pathname.indexOf(ON_TOP_URLS[key]) > -1){
			is = true
			break
		}
	}
	return is
}

window.addEventListener('scroll', function(e){
	let body = document.body
	let html = document.documentElement
	let height = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight )
	reposition(window.scrollY < height-2000 || isOnTopURL())
})

reposition(true)*/
ReactDOM.render(WidgetRouter, document.getElementById(contentDivId))
