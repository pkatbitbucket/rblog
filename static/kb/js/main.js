// var md = window.markdownit();

var heading_regex = /([a-zA-Z\d])+:$/

var default_opts = {
    basePath: '/static/cfblog/',
    clientSideStorage: false,
    localStorageName: '{{ request.path|slugify }}',
    useNativeFullscreen: false,
    parser: marked,
    file: {
        name: 'epiceditor',
        defaultContent: '',
        autoSave: 100
    },
    theme: {
        base: 'themes/base/epiceditor.css',
        preview: 'themes/preview/bartik.css',
        editor: 'themes/editor/epic-dark.css'
    },
    button: {
        preview: true,
        fullscreen: true,
        bar: "auto"
    },
    focusOnLoad: true,
    shortcut: {
        modifier: 18,
        fullscreen: 70,
        preview: 80
    },
    string: {
        togglePreview: 'Toggle Preview Mode',
        toggleEdit: 'Toggle Edit Mode',
        toggleFullscreen: 'Enter Fullscreen'
    },
    autogrow: true
};

var ViewModel = function() {
    var self = this;
    self.kb_data = ko.observableArray();
    self.kb_display = ko.observableArray();
    self.current_edit = ko.observable(0);
    self.current_edit_data = ko.observable({});
    self.convert_to_markdown = function(data) {
        return marked(data());
    }
    self.changeMainItem = function(data) {
        self.kb_display.removeAll();
        self.kb_display.push(data);
    }
    self.getItemDetails = function(data) {
        $.ajax({
            type: "GET",
            url: '/kb/' + data.id(),
            contentType: 'application/json; charset=utf-8',
            dataType: 'text',
            success: function(data) {
                return_data = JSON.parse(data);
                temp_data = ko.mapping.fromJS(return_data);
                for (temp in self.kb_display())
                    if (self.kb_display()[temp].data.parent_count() >= temp_data.data.parent_count())
                        self.kb_display().splice(temp, self.kb_display().length)
                self.kb_display.push(temp_data)
                if (self.kb_display().length > 2) {
                    self.kb_display.remove(self.kb_display()[0]);
                }
                url = "/kb/";
                try {
                    url += vm.kb_display()[0].data.id() + "/";
                    url += vm.kb_display()[1].data.id() + "/";
                } catch (e) {}
                window.history.pushState("", "", url);
            },
            async: false,
        });

    }
    self.edit_this = function(data) {
        temp_data = ko.toJSON(data);
        temp_data = ko.mapping.fromJSON(temp_data);
        self.current_edit_data(temp_data.data);
        self.current_edit(data.data.id());
        // Epic editor initialization
        var opts = default_opts;
        opts.container = 'item_markdown_' + data.data.id();
        opts.file.defaultContent = temp_data.data.content();
        editor = new EpicEditor(opts).load();
        editor.on('update', function() {
            self.current_edit_data().content(editor.getFiles().epiceditor.content);
        });
    }
    self.save_changes = function(data, data_index) {
        $.ajax({
            type: "PUT",
            url: '/kb/' + data.data.id() + '/',
            contentType: 'application/json; charset=utf-8',
            data: ko.toJSON({
                'flag': 'edit_node',
                'put_data': self.current_edit_data
            }),
            dataType: 'text',
            success: function(return_data) {
                data.data.title(vm.current_edit_data().title());
                data.data.content(vm.current_edit_data().content());
                if (data_index() > 0) {
                    parent_data = vm.kb_display()[data_index() - 1];
                    for (child in parent_data.children()) {
                        if (parent_data.children()[child].id() == data.data.id())
                            parent_data.children()[child].title(vm.current_edit_data().title());
                    }
                }
                self.current_edit(0);
                self.current_edit_data({});
            },
            async: false,
        });
    }
    self.addNewChild = function(parent_data) {
        parent_data.children.push({
            'title': ko.observable(''),
            'content': ko.observable(''),
            'id': ko.observable(0),
        })
    }
    self.saveNewItem = function(data, parent_data) {
        post_data = {
            'parent_id': parent_data.data.id,
            'title': data.title,
        }
        $.ajax({
            type: "POST",
            url: '/kb/',
            contentType: 'application/json; charset=utf-8',
            data: ko.toJSON({
                'flag': 'new_node',
                'post_data': post_data
            }),
            dataType: 'text',
            success: function(return_data) {
                parent_data.children.remove(data);
                return_data = ko.mapping.fromJSON(return_data);
                parent_data.children.push({
                    'id': return_data.data.id,
                    'title': return_data.data.title,
                })
            },
            async: false,
        });
    }
    self.cancelNewItem = function(data, parent_data) {
        parent_data.children.remove(data);
    }
    self.ItemMoved = function(data) {
        $.ajax({
            type: "PUT",
            url: '/kb/' + data.data.id() + '/',
            contentType: 'application/json; charset=utf-8',
            data: ko.toJSON({
                'flag': 'move',
                'move_data': data.children
            }),
            dataType: 'text',
            success: function(return_data) {},
            async: false,
        });
    }
    self.check_if_active = function(data_id) {
        for (x in self.kb_display())
            if (self.kb_display()[x].data.id() == data_id())
                return true;
        return false;
    }
    self.check_if_header = function(title) {
        if (title().match(heading_regex))
            return true;
        return false;
    }
}

var vm = new ViewModel();
$(document).ready(function() {
    ko.applyBindings(vm);
    loadInitialData();
})
