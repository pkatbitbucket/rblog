var VEHICLE_TYPE_BIKE = "twowheeler";
var oldBikeYear = utils.getPastYears(),
	selected_model_id_bike,
    selected_fuel_type_id_bike = "PETROL",
    _fuel_type = '{"code": "petrol", "id": "PETROL","name": "Petrol"}',
    bike=[];

/************ private functions ***********/
function getBikes() {
    var url = '/motor/' + VEHICLE_TYPE_BIKE + '/api/vehicles/';
    var data = {'csrfmiddlewaretoken': CSRF_TOKEN};
    utils.getData(url,data,"#bike_make_model",'models');
}

function getRegYears_Bike() {
    $("#bike_reg_year").html(utils.createOption(oldBikeYear,$("#bike_reg_year").attr('data-placeholder')));
    utils.createSelect($("#bike_reg_year"),oldBikeYear);
}

function getRtos_Bike(){
	var url = '/motor/' + VEHICLE_TYPE_BIKE + '/api/rtos/';
    var data = {'csrfmiddlewaretoken': CSRF_TOKEN};
    utils.getData(url,data,"#bike_rto",'rtos');
}

function getBikeVariant(){
    $('#bike_variant').find('option').remove();
    if (selected_model_id_bike && selected_fuel_type_id_bike) {
        var url = '/motor/' + VEHICLE_TYPE_BIKE + '/api/vehicles/' + selected_model_id_bike + '/variants/';
        var data = {'csrfmiddlewaretoken': CSRF_TOKEN,'fuel_type': selected_fuel_type_id_bike}
        utils.getData(url,data,"#bike_variant","variants");
    }
     else{
        $('#bike_variant').html(utils.createOption({},$('#bike_variant').attr('data-placeholder')));
    }
}

// Validation of form
function validateBikeForm(){
    var valid = true;
    $('.error-text').hide();
    var bike = {
        policy_type: $('[name=bike_policy_type]:checked').val(),
        bike_make_model: $('#bike_make_model :selected').val(),
        bike_variant: $('#bike_variant :selected').val(),
        bike_rto: $('#bike_rto :selected').val(),
        reg_year: $('#bike_reg_year :selected').attr("name")
    }

    if ((bike.policy_type != "new") && !bike.reg_year) {
        valid = false;
        $('#bike_reg_date_error').fadeIn();
    }
    if (bike.policy_type == "expired") {
        bike["expiry_month"] = $("#bike_policy_month :selected").attr("name");
        bike["expiry_year"] = $("#bike_policy_year :selected").attr("id");
        bike["expiry_date"] = $("#bike_policy_date :selected").attr("id");

        var expiryYear = $("#bike_policy_year :selected").val(),
            expiryMonth = $("#bike_policy_month :selected").val(),
            expiryDate = $("#bike_policy_date :selected").val();
        if(expiryYear.length <1){
            valid = false;
            $("#bike_policy_year_error").fadeIn()
        }
        if(expiryMonth.length <1){
            valid = false;
            $("#bike_policy_month_error").fadeIn()
        }
        if(expiryDate.length <1){
            valid = false;
            $("#bike_policy_date_error").fadeIn()
        }
        if(expiryYear.length >=1 && expiryMonth.length >=1 && expiryDate >=1){
            var d = new Date(expiryYear, expiryMonth, expiryDate),
                today = new Date();
                valid = false;
        }
    }
    if(bike.reg_year > bike.expiry_year){
        valid = false;
        $("#bike_policy_year_error").text("Cannot be before registration year.")
        $("#bike_policy_year_error").fadeIn()
    }
    if (!bike.bike_make_model || !bike.bike_variant) {
        valid = false;
        $('#bike_detail_error').fadeIn();
    }

    if (!bike.bike_rto) {
        valid = false;
        $('#bike_rto_error').fadeIn();
    }
    return valid;
}

// Gets data from DOM
var getFormDetails_Bike = function(){
    return{
        policy_type: $('[name=bike_policy_type]:checked').val(),
        bike_make_model: $('#bike_make_model :selected').val(),
        bike_variant: $('#bike_variant :selected').val(),
        bike_rto: $('#bike_rto :selected').val(),
        reg_year: $('#bike_reg_year :selected').text(),
        fuel_type: _fuel_type
    }
}

// Submits data to server
var submitBikeForm = function() {
    var redirect_url = '/motor/twowheeler-insurance/#results';
    var bike = getFormDetails_Bike();
    var data = {
        'isNewVehicle': (bike.policy_type == "new") ? '1' : '0',
        'vehicle': bike.bike_make_model,
        'vehicleVariant': bike.bike_variant,
        'fuel_type': bike.fuel_type,
        'rto_info': bike.bike_rto
    };

    if (bike.policy_type != "new") {
        data['reg_year'] = bike.reg_year;
        data['expirySlot'] = bike.expiry_slot;
    }

    if (bike.policy_type == "expired") {
        data["expiry_month"] = $("#bike_policy_month :selected").attr("name");
        data["expiry_year"] = $("#bike_policy_year :selected").attr("id");
        data["expiry_date"] = $("#bike_policy_date :selected").attr("id");
    }
    $.post("/motor/submit-landing-page/", data, function(response) {
        var response = JSON.parse(response);
        if (response.success) {

            // //  MixPanel Events
            // utils.MixPanelTrack(MixPanelEvents.Bike_Home_Product.name, MixPanelEvents.Bike_Home_Product.params);

            if (localStorage) {
                localStorage.setItem(VEHICLE_TYPE_BIKE, JSON.stringify(response.data));
            } else {
                for (var item in response.data) {
                    var value = response.data[item];
                    if (value != null && typeof(value) == "object") {
                        value = JSON.stringify(value);
                    }
                    Cookies.set(VEHICLE_TYPE_BIKE + item, value);
                }
            }

            // Send data to Google Analytics

            var gtm_data = {
                param1: JSON.parse(data.vehicle).name,
                param2: JSON.parse(data.fuel_type).name,
                param3: JSON.parse(data.vehicleVariant).name,
                param4: JSON.parse(data.rto_info).name
            }

            if(bike.policy_type != "new")
                gtm_data.param5= bike.reg_year;

            if(bike.policy_type == "expired"){
                gtm_data.param6 =  data.expiry_year +"-"+data.expiry_month + "-" + data.expiry_date;
            }

            utils.gtm_push("HomeBikeEventGetQuote",
                "Homepage Bike - Get Quote",
                bike.policy_type + " Bike",
                " ", gtm_data,
                function(){window.location.href = redirect_url;}
            );
        }
    });

};

var setUpBikeForm = function() {
    var policy_type_value = $('[name=bike_policy_type]:checked').val();
    if (policy_type_value == 'new') {
        $('.bike-product .new-policy').removeClass('hide');
        $('.bike-product .renew-policy').addClass('hide');
        $('.bike-product .common-fields').removeClass('hide');
        $('.bike-product .expired-policy').addClass('hide');
        $('.bike-product .rto-field').addClass('col-md-offset-3');
    } else if (policy_type_value == 'renew') {
        $('.bike-product .new-policy').addClass('hide');
        $('.bike-product .renew-policy').removeClass('hide');
        $('.bike-product .common-fields').removeClass('hide');
        $('.bike-product .expired-policy').addClass('hide');
        $('.bike-product .rto-field').removeClass('col-md-offset-3');
    } else if (policy_type_value == 'expired') {
        $('.bike-product .expired-policy').removeClass('hide');
        $('.bike-product .renew-policy').addClass('hide');
        $('.bike-product .renew-policy').removeClass('hide');
        $('.bike-product .common-fields').removeClass('hide');
        $('.bike-product .rto-field').removeClass('col-md-offset-3');
        initExpirePolicyForm();
    }
};

function initExpirePolicyForm(){
    var years = [];
    for(var i = new Date().getFullYear(); i>=1999; i--)
        years.push({'id':i,'name':i, 'value':i});

    utils.createSelect($("#bike_policy_year"),years);
    utils.createSelect($("#bike_policy_month"),monthList);
    utils.createSelect($("#bike_policy_date"),{});

    disableExpiryYear();
    disableExpiryMonth();
    disableExpiryDate();
}

function populatePolicyDate(str){
    var year = $("#bike_policy_year :selected").attr("id");
    var month= $("#bike_policy_month :selected").attr("id");
    var monthlen = new Date(year,parseInt(month) ,0).getDate();
    var dates = [];

    for(var i =1; i<=monthlen; i++)
        dates.push({"id":i,"name":i, "value":i})
    utils.createSelect($("#bike_policy_date"),dates);
}

function setBikeForm(){
    var bike = {};
    var modalVal,fuelVal,variantVal,rtoVal,yearVal, expiryDateVal, expiryMonthVal, expiryYearVal;
    // Fetch data from LocalStorage
    if(localStorage && localStorage.getItem(VEHICLE_TYPE_BIKE)){
        var _local = JSON.parse(localStorage.getItem(VEHICLE_TYPE_BIKE));
        bike = {
            expiry_slot: JSON.stringify(_local.expirySlot),
            expiry_date: JSON.stringify(_local.expiry_date),
            expiry_month: JSON.stringify(_local.expiry_month),
            expiry_year: JSON.stringify(_local.expiry_year),
            vehicle_fuel_type : JSON.stringify(_local.fuel_type),
            vehicle_variant : JSON.stringify(_local.vehicleVariant),
            vehicle_make_model : JSON.stringify(_local.vehicle),
            vehicle_rto : JSON.stringify(_local.rto_info),
            vehicle_reg_year: JSON.stringify(_local.reg_year)
        }
        if(_local.vehicle)
            selected_model_id_bike = _local.vehicle.id;
    }
    // Fetch data from Cookie
    else{
         bike = {
            expiry_slot: Cookies.get(VEHICLE_TYPE_BIKE+'_expiry_slot'),
            expiry_date: Cookies.get(VEHICLE_TYPE_BIKE+'_expiry_date'),
            expiry_month:Cookies.get(VEHICLE_TYPE_BIKE+'_expiry_month'),
            expiry_year: Cookies.get(VEHICLE_TYPE_BIKE+'_expiry_year'),
            vehicle_fuel_type : utils.validateJSON(Cookies.get(VEHICLE_TYPE_BIKE + '_fuel_type')),
            vehicle_variant : utils.validateJSON(Cookies.get(VEHICLE_TYPE_BIKE+ '_vehicleVariant')),
            vehicle_make_model : utils.validateJSON(Cookies.get(VEHICLE_TYPE_BIKE+'_vehicle')),
            vehicle_rto : Cookies.get(VEHICLE_TYPE_BIKE+'_rto_info'),
            vehicle_reg_year: Cookies.get(VEHICLE_TYPE_BIKE+ '_reg_year')
        }
    }

    getBikeVariant();
    if(bike.vehicle_make_model){
        modalVal = bike.vehicle_make_model;
    }
    else{
        modalVal = $('#bike_make_model option:first').val()
        $("#bike_make_model").parent().removeClass('selected');
    }

    if(bike.vehicle_variant){
        variantVal = bike.vehicle_variant;
    }
    else{
        variantVal = $('#bike_variant option:first').val()
        $("#bike_variant").parent().removeClass('selected');
    }

    if(bike.vehicle_rto){
        rtoVal = bike.vehicle_rto;
    }
    else{
        rtoVal = $('#bike_rto option:first').val()
        $("#bike_rto").parent().removeClass('selected');
    }
    if(bike.vehicle_reg_year){
        yearVal = '{"id":' + bike.vehicle_reg_year + ',"name":'+ bike.vehicle_reg_year + ',"value":'+ bike.vehicle_reg_year + '}';
    }
    else{
        yearVal = $('#bike_reg_year option:first').val()
        $("#bike_reg_year").parent().removeClass('selected');
    }
    if(bike.expiry_date){
        expiryDateVal = '{"id":' + bike.expiry_date + ',"name":'+ bike.expiry_date + ',"value":'+ bike.expiry_date + '}';
    }
    else{
        expiryDateVal = $('#bike_policy_date option:first').val()
        $("#bike_policy_date").parent().removeClass('selected');
    }
    if(bike.expiry_month){
        $.each(monthList,function(index, row){
            if(row.name == bike.expiry_month.substring(1,bike.expiry_month.length-1))
                expiryMonthVal = '{"id":"'+row.id+'","name":'+bike.expiry_month + '}';
        });
    }
    else{
        expiryMonthVal = $('#bike_policy_month option:first').val()
        $("#bike_policy_month").parent().removeClass('selected');
    }
    if(bike.expiry_year>1900){
        expiryYearVal = '{"id":' + bike.expiry_year + ',"name":'+ bike.expiry_year + ',"value":'+ bike.expiry_year + '}';
        $('#rbBikeExpired').click();
    }
    else{
        expiryYearVal = $('#bike_policy_year option:first').val()
        $("#bike_policy_year").parent().removeClass('selected');
    }

    // Set cookie data to controls
    setTimeout(function(){
        $('#bike_make_model').select2().select2('val',modalVal);
        $('#bike_fuel_type').select2().select2('val',fuelVal);
        $('#bike_variant').select2().select2('val',variantVal);
        $('#bike_rto').select2().select2('val',rtoVal);
        $('#bike_reg_year').select2().select2('val',yearVal);
        $('#bike_policy_year').select2().select2('val',expiryYearVal);
        $('#bike_policy_month').select2().select2('val',expiryMonthVal);

        populatePolicyDate();
        setTimeout(function(){
            $("#bike_policy_date").select2().select2('val',expiryDateVal);
        },0);

        utils.showLabelForInput($("#bike_make_model"));
        utils.showLabelForInput($("#bike_variant"));
        utils.showLabelForInput($("#bike_rto"));
        utils.showLabelForInput($("#bike_reg_year"));
        utils.showLabelForInput($("#bike_policy_date"));
        utils.showLabelForInput($("#bike_policy_month"));
        utils.showLabelForInput($("#bike_policy_year"));
        setTimeout(function(){$(".loading").removeClass("loading");},0)
    },500);
}

function disableExpiryYear(){
    if($("#bike_reg_year option:selected").val()){
        var year = JSON.parse($("#bike_reg_year option:selected").val()).name;
        var newData = [];
        for(var i = new Date().getFullYear(); i>= year; i--)
            newData.push({'id':i,'name':i, 'value':i});
        utils.createSelect($("#bike_policy_year"), newData);
        utils.createSelect($("#bike_policy_month"),monthList);
        utils.createSelect($("#bike_policy_date"),{});
    }
}

function disableExpiryMonth(){
    if($("#bike_policy_year option:selected").val()){
        var year = JSON.parse($("#bike_policy_year option:selected").val()).name,
            currentDate = new Date();
        if(year == currentDate.getFullYear()){
            var month = currentDate.getMonth() +1;
            var newData = [];
            $.each(monthList, function(index, row){
                if(row.id <= month)
                    newData.push(row);
            });
        utils.createSelect($("#bike_policy_month"),newData);
        }
        else{
            utils.createSelect($("#bike_policy_month"),monthList);
        }
    }
}

function disableExpiryDate(){
    if($("#bike_policy_month option:selected").val()){
        var month = JSON.parse($("#bike_policy_month option:selected").val()).id,
            currentDate = new Date();
        var year = JSON.parse($("#bike_policy_year option:selected").val()).name;
        var date = currentDate.getDate();
        var monthlen = new Date(currentDate.getFullYear(),parseInt(month) ,0).getDate();
        var dates = [];
        var newData = [];

        for(var i =1; i<=monthlen; i++)
            dates.push({"id":i,"name":i, "value":i})

        if(month == currentDate.getMonth() +1 && year == currentDate.getFullYear()){
            $.each(dates, function(index, row){
                if(row.id < date)
                    newData.push(row);
            });
            utils.createSelect($("#bike_policy_date"),newData);
        }
        else{
            utils.createSelect($("#bike_policy_date"),dates);
        }
    }
}
/************ events section ***********/
$('#bike_make_model').on("change",function(){
	selected_model_id_bike = $(this).children(":selected").attr("id");
    getBikeVariant();
});

$("#bike_variant").on("change", function(){
    if($("option:selected",this).attr("id")>0 && $("#bike_make_model :selected").attr("id") > 0)
        $('#bike_detail_error').fadeOut();
});

$("#bike_rto").on("change",function(){
    if($("option:selected",this).attr("id") && 
        ($("option:selected",this).attr("id")>0 || $("option:selected",this).attr("id").length>0))
        $('#bike_rto_error').fadeOut();
});

$("#bike_reg_year").on("change",function(){
    if($("option:selected",this).attr("id")>0)
        $('#bike_reg_date_error').fadeOut();
    disableExpiryYear();
});

$('#bike_submit').on('click', function(event) {
    event.preventDefault();
    if (validateBikeForm()) {
        submitBikeForm();
    }
});

$('input[name="bike_policy_type"]').change(function() {
    setUpBikeForm();
});

$("#bike_policy_month,#bike_policy_year").on("change", function(){
    if($("option:selected", this).attr("id")>0){
        var err = "#"+ $(this).attr("id")+ "_error";
        $(err).fadeOut();
    }
    $("#bike_policy_date").find('option').remove();
    populatePolicyDate();

    if($(this).attr("id") == "bike_policy_year"){
        disableExpiryMonth();
    }
    else if($(this).attr("id") == "bike_policy_month"){
        disableExpiryDate();
    }
});

$("#bike_policy_date").on("change", function(){
    if($("option:selected", this).attr("id")>0){
        var err = "#"+ $(this).attr("id")+ "_error";
        $(err).fadeOut();
    }
});
/************ init ***********/
(function initBikeForm(){
    getBikes();
    getRegYears_Bike();
    getRtos_Bike();
    setUpBikeForm();
    initExpirePolicyForm();
    setBikeForm();
})();
