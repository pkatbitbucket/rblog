var CF=(function(){
    var init = function(tp) {
        var body = document.body;
        var containers = document.querySelectorAll('.coverfox_container');
        var base_url = 'https://www.coverfox.com';

        for(var i=0; i<containers.length; i++)
        {
            container = containers[i];
            var iframe = document.createElement('iframe');
            iframe.id = 'cf_iframe'+i;
            iframe.className = 'cf_iframe';
            iframe.style.width = '100%';
            iframe.style.border = '0px solid transparent';
            iframe.style.overflowY = 'auto';
            iframe.src = base_url+'/lp/health-insurance/tp/'+tp+'/?iframe_id='+iframe.id;
            container.appendChild(iframe);
        }
        var messenger = document.createElement('script');
        messenger.type = 'text/javascript';
        messenger.src = base_url+'/static/lp-health/tp_widget/tp_messenger.js';
        body.appendChild(messenger);
    };
    return {
        init : init
    }
})();
