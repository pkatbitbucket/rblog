(function() {
    window.addEventListener('message', function(e) {
        var msg = JSON.parse(e.data);
        if(msg['height']) {
            var cf_iframe = document.getElementById(msg['id']);
            cf_iframe.style.height=msg['height'];
        }
        else if(msg['url'])
            document.location.href = msg['url'];
    }
);
})()
