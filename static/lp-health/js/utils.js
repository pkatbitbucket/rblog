/**
 * Created by vinay on 8/13/15.
 */

var mobileNo = Cookies.get('mobileNo');
var email = Cookies.get('email');
var num_obj = $('#contact-me-num');
var email_obj = $('#contact-me-email');

function set_val(obj, val) {
    if(!obj.val()) obj.val(val);
}
if(mobileNo) {
    set_val(num_obj, mobileNo);
}
if(email) {
    set_val(email_obj, email);
}

$('[id^="show-quote-btn"]').on('click', function (e) {
    e.preventDefault();
    //console.log(next_page_url, campaign, page_id);
    var context = $(this);
    id = context.attr('id');
    suffix = id.charAt(id.length -1);
    //console.log(id, suffix);
    $('.contact-me-error').hide();
    var name_flag = true;
    var email_flag = true;
    var num_flag = true;
    var cmodel =  {                
                'campaign': campaign,
                'label': page_id,
                'triggered_page': window.location.href
            };
    if(NAME_MANDATORY){
        var cname = $('#contact-me-name'+ suffix ).val().trim();
        if (!cname) {
            $('#name-error'+ suffix ).show();
        }
        var re_name = cname.match(/^[A-Z]+(?:\s[A-Z]+)*$/i);
        if ($.isEmptyObject(re_name)) {
            $('#name-error' + suffix ).show();
        }
        if(re_name){ 
            cmodel['name'] = cname;
            name_flag = true;
        }
        else{name_flag = false;}    
    }

    if(MOBILE_MANDATORY){
        var cnum = $('#contact-me-num'+ suffix ).val().trim();
        if (!cnum) {
            $('#num-error'+ suffix ).show();
        }
        var re_num = cnum.match(/^[7-9]\d{9}$/);
        if ($.isEmptyObject(re_num)) {
            $('#num-error'+ suffix ).show();
        }
        if(re_num){ 
            cmodel['mobile'] = cnum;
            Cookies.set('mobileNo', cnum);
            num_flag = true;
        }
        else{ num_flag= false; }
    }
    if(EMAIL_MANDATORY){
        var cmail = $('#contact-me-email'+ suffix ).val().trim();
        if (!cmail) {
            $('#email-error'+ suffix ).show();
        }
        var re_email = cmail.match(/^([A-Z0-9]+([+-._]?[A-Z0-9]+)*)@((?:[A-Z0-9-]+\.)*\w[A-Z0-9-]{0,66})\.([A-Z]{2,6}(?:\.[A-Z]{2})?)$/i);
        if ($.isEmptyObject(re_email)) {
            $('#email-error'+ suffix ).show();
        }
        if (re_email){ 
            cmodel['email'] = cmail;
            Cookies.set('email', cmail);
            email_flag = true;
        }
        else{ email_flag= false; } 
    }
    if (name_flag && num_flag && email_flag) {
        context.addClass('disabled').text('Just a moment...');
        $.post("/leads/save-call-time/",
                {
                    'data' : JSON.stringify(cmodel),
                    'csrfmiddlewaretoken' : CSRF_TOKEN
                },
                function(data){
                    if (data.success){

                        if (window.dataLayer){
                            //window.dataLayer.push({'gtm_mobile': cnum});
                            window.dataLayer.push({
                                'event': 'GAEvent',
                                'hitType': 'event',
                                'eventCategory': campaign,
                                'eventAction': 'CallScheduled',
                                'eventLabel': page_id,
                                'eventValue': 4                                   
                             });
                            redirection();
                        } else {
                            redirection();
                        }
                    }
                    else{
                        console.log('POST ERROR', data);
                        redirection();
                    }
                },
                'json'
        );
    }
});

function redirection(){
    if(next_page_url == "THANK-YOU"){
        $(".f-input").addClass("hide");
        $(".thank-you").removeClass("hide");
    }
    else {
        window.location.href = next_page_url;
   }
}
