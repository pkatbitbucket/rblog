var CF=(function(){
    var init = function(tp) {
        var body = document.body;
        var container_types = ['coverfox_container', 'cf_topup_container', 'cfmotor_container'];
        var urls = {
            'coverfox_container': base_url+'/lp/health-insurance/tp/'+tp+'/',
            'cf_topup_container': base_url+'/lp/health-insurance/plan/topup/tp/'+tp+'/',
            'cfmotor_container': base_url+'/lp/car-insurance/tp/'+tp+'/'
        };

        var container_id = 0;
        for(var i=0; i<container_types.length; i++) {
            var container_type = container_types[i];
            var url = urls[container_type];
            var containers = document.querySelectorAll('.' + container_type);
            for(var j=0; j<containers.length; j++) {
                container = containers[j];
                var iframe = document.createElement('iframe');
                iframe.id = 'cf_iframe' + container_id;
                iframe.className = 'cf_iframe';
                iframe.style.width = '100%';
                iframe.style.border = '0px solid transparent';
                iframe.style.overflowY = 'auto';
                iframe.src = url + '?iframe_id='+iframe.id;
                container.appendChild(iframe);
                container_id++;
            }
        }

        var messenger = document.createElement('script');
        messenger.type = 'text/javascript';
        messenger.src = base_url+'/static/lp-health/tp_widget/tp_messenger.js?ver=1';
        body.appendChild(messenger);

        FB_remarketing();
    };
    var FB_remarketing = function() {
        (function() {
        var _fbq = window._fbq || (window._fbq = []);
        if (!_fbq.loaded) {
            var fbds = document.createElement('script');
            fbds.async = true;
            fbds.src = '//connect.facebook.net/en_US/fbds.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fbds, s);
            _fbq.loaded = true;
        }
        _fbq.push(['addPixelId', '338928289619965']);
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(['track', 'PixelInitialized', {}]);
    };
    return {
        init : init
    };
})();
