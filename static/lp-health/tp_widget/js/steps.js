ko.validation.configure({
    errorMessageClass: 'error',
    decorateElement : true,
    errorElementClass: 'has-error',
    insertMessages: false
});

function Member(type, selected, age, person, ageList, displayName) {
    var self = this;
    self.selected = ko.observable(selected);
    self.person = person;
    self.id = self.person.toLowerCase();
    self.type = type;
    var age_full = ko.observable(age).extend({
        validation:{
            'validator': function(n){
                if(!self.selected()){
                    return true;
                }
                if(self.type != 'kid'){
                    return true;
                }
                if(['', undefined, null].indexOf(n) > -1){
                    return false;
                } else {
                    return true;
                }
            },
            'message' : 'Please select age of insured member'
        }
    });
    if(type == 'kid') {
        self.age_full = age_full;
        self.age = ko.observable();
        var i = 1;
        var A = [];
        while(i < 12){
            A.push(i + " months");
            i++;
        }
        i = 1;
        while(i <= 25){
            A.push(i + " years");
            i++;
        }
        self.ageList = A;
    } else {
        self.age = age_full;
        self.ageList = ageList;
    }
    self.age_in_months = ko.observable(1);
    if(type == 'kid') {
        self.calculate_age = ko.computed(function(){
            if(String(self.age_full()).indexOf('years') > -1){
                self.age(parseInt(self.age_full()));
                self.age_in_months(0);
            } else {
                self.age_in_months(parseInt(self.age_full()));
                self.age(0);
            }
        });
    }

    self.error = ko.observable("");
    if(!displayName){
        self.displayName = person;
    } else {
        self.displayName = displayName;
    }
};

function stepOneViewModel() {
    var self = this;
    self.range = function (a, b, step) {
        var A = [];
        A[0] = a;
        step = step || 1;
        while(a+step<=b){
            A[A.length] = a+= step;
        }
        return A;
    }
    self.initialize_members = function(obj, value) {
        var kid_id = 1;
        if(value.length > 0){
            self.you.selected(false);
        }
        ko.utils.arrayForEach(value, function(v){
            switch(v.person) {
                case "You":
                    self.you.selected(v.selected);
                self.you.age(parseInt(v.age));
                self.ageList = self.range(18, 100);
                break;
                case "Spouse":
                    self.spouse.selected(v.selected);
                self.spouse.age(parseInt(v.age));
                self.ageList = self.range(18, 100);
                break;
                case "Father":
                    self.father.selected(v.selected);
                self.father.age(parseInt(v.age));
                self.ageList = self.range(18, 100);
                break;
                case "Mother":
                    self.mother.selected(v.selected);
                self.mother.age(parseInt(v.age));
                self.ageList = self.range(18, 100);
                break;
                default:
                    var m = new Member('kid', v.selected, v.age, v.person, v.ageList, v.displayName);
                if(['son', 'daughter'].indexOf(v.person.toLowerCase()) > -1) {
                    if(v.age_in_months != 0){
                        m.age_in_months(parseInt(v.age_in_months));
                        m.age_full(v.age_in_months + " months");
                    } else {
                        m.age_in_months(0);
                        m.age_full(v.age_full);
                    }
                    m.id = v.person.toLowerCase() + kid_id;
                    kid_id++;
                }
                obj.push(m);
            }
        });
    }
    self.gender = ko.observable().extend({
        store : {
            key : 'gender'
        },
        required: {
            params:true,
            message: "Please select your gender"
        }
    });
    self.you = new Member('adult', true, undefined, 'You', self.range(18, 100), 'You');
    self.spouse = new Member('adult', false, undefined, 'Spouse', self.range(18, 100), ko.computed(function(){
        if (self.gender() == 'MALE'){
            return 'Your Wife';
        }
        else if (self.gender() == 'FEMALE') {
            return 'Your Husband';
        } else {
            return 'Your Spouse';
        }
    }));
    self.father = new Member('adult', false, undefined, 'Father', self.range(18, 100));
    self.mother = new Member('adult', false, undefined, 'Mother', self.range(18, 100));
    self.members = ko.observableArray([self.you, self.spouse, self.father, self.mother]).extend({
        store : {
            key : 'members',
            init: self.initialize_members,
            dump: function(nval){
                var nval = ko.toJS(nval);
                var mList = []
                ko.utils.arrayForEach(nval, function(m){
                    delete m['ageList'];
                    delete m['error'];
                    if(m.selected){
                        mList.push(m);
                    }
                });
                return ko.toJSON(mList);
            }
        },
        validation : [
            {
            'validator' : function(obj){
                if(ko.utils.arrayFilter(obj, function(m) { return m.selected() == true }).length == 0){
                    return false;
                } else {
                    return true;
                }
            },
            'message' : 'Select atleast one member to insure'

        },{
            'validator' : function(obj){
                if(ko.utils.arrayFilter(obj, function(m) { return m.selected() == true && m.type == 'adult' }).length == 0){
                    return false;
                } else {
                    return true;
                }
            },
            'message' : 'Select atleast one adult to insure'
        }
        ]
    });

    self.selectMember = function(member){
        if (member.selected() == true) {
            if(member.type == 'kid')
                {
                    self.members.remove(member);
                    //return member.selected(false);
                }
                else {
                    //return member.selected(false);
                }
        }
        else {
            //return member.selected(true);
        }
        return true;
    };
    self.selectGender = function(gen) {
        self.gender(gen);
    }

    self.addKid = function(person) {
        var kid = new Member('kid', true, undefined, person, self.range(1, 25), person);
        kid.id = person.toLowerCase() + (self.getSelectedKids().length + 1);
        self.members.push(kid);
    };

    self.getSelectedKids = ko.computed(function(){
        return ko.utils.arrayFilter(self.members(), function(member) { return member.type == "kid" && member.selected() == true });
    });
    self.getSelectedAdults = ko.computed(function(){
        return ko.utils.arrayFilter(self.members(), function(member) { return member.type == "adult" && member.selected() == true });
    });
    self.getAdults = ko.computed(function(){
        return ko.utils.arrayFilter(self.members(), function(member) { return member.type == "adult"});
    });
    self.getSelectedMembers = ko.computed(function(){
        return ko.utils.arrayFilter(self.members(), function(member) { return member.selected() == true });
    });

    self.cumulativeAgeError = ko.observable().extend({
        validation: [{
            validator: function(obj){
                var bad_members = ko.utils.arrayFilter(self.getSelectedMembers(), function(m) {
                    if(m.age_full == undefined) {
                        return isNaN(m.age())
                    } else {
                        return ((isNaN(m.age()) || (m.age() == 0)) && (isNaN(m.age_in_months()) || (m.age_in_months() == 0)))
                    }
                })
                if(bad_members.length == 0){
                    return true;
                } else {
                    return false;
                }
            },
            'message':"Please select respective member's age"
        },{
            'validator' : function(obj) {
                if(self.you.selected()){
                    if(self.father.selected()){
                        if((self.father.age() - self.you.age()) < 18){
                            return false;
                        }
                    }
                    if(self.mother.selected()){
                        if((self.mother.age() - self.you.age()) < 18){
                            return false;
                        }
                    }
                }
                return true;
            },
            'message' : "You parents should be atleast 18 years older than you"
        } , {
            'validator' : function(obj) {
                if(self.you.selected()){
                    if(self.getSelectedKids().length > 0){
                        var max_kid_age = Math.max.apply(null, self.getSelectedKids().map(function(k){return k.age();}));
                        if((self.you.age()  - max_kid_age) < 18){
                            return false;
                        }
                    }
                }
                return true;
            },
            'message' : "Your kids should be atleast 18 years younger than you"
        }, {
            'validator' : function(obj) {
                if(self.father.selected() || self.mother.selected()){
                    var selected_parents = ko.utils.arrayFilter(self.getSelectedAdults(), function(k){return (k == self.father || k == self.mother)});
                    var min_parent_age = Math.min.apply(null, selected_parents.map(function(p){return p.age();}));
                    if(self.getSelectedKids().length > 0){
                        var max_kid_age = Math.max.apply(null, self.getSelectedKids().map(function(k){return k.age();}));
                        if((min_parent_age  - max_kid_age) <= 36){
                            return false;
                        }
                    }
                }
                return true;
            },
            'message' : "There should be an age difference for at least 36 years between grandparents and kids"

        }]
    });
    self.step1Errors = ko.validation.group([self.members,self.gender, self.cumulativeAgeError]);
    self.validate = function() {
        valid = true;
        if(!self.step1Errors().length == 0) {
            self.step1Errors.showAllMessages(true);
            valid = false;
        }
        return valid;
    };
};

function stepTwoViewModel() {
    var self = this;
    self.is_topup = IS_TOPUP;

    if(!self.is_topup) {
        self.service_type = ko.observable().extend({
            store : {
                key : 'service_type'
            },
            validation: {
                'validator':function(ser){
                    if(!ser){
                        return false;
                    } else {
                        return true;
                    }
                },
                'message': 'Please select service preference'
            }
        });
        self.selectService = function(set){
            self.service_type(set);
        };
        var validation_group = [self.service_type];
    }
    else {
        self.deductible_options = [100000, 200000, 300000, 400000, 500000, 600000, 700000, 800000, 900000, 1000000];
        self.deductible = ko.observable().extend({
            store: {
                key: 'deductible'
            },
            required: {
                params:true,
                message: "Please select your existing sum assured"
            },
        });
        var validation_group = [self.deductible];
    }

    self.validate = function() {
        self.step2Errors = ko.validation.group(validation_group);
        valid = true;
        if(!self.step2Errors().length == 0) {
            self.step2Errors.showAllMessages();
            valid = false;
        }
        return valid;
    };
};

var pincheck = pincheck();
function stepThreeViewModel() {
    var self = this;
    self.userPincode = ko.observable().extend({
        store : {
            key : 'pincode'
        },
        required: {
            params:true,
            message: "Please select your area pincode"
        },
        validation : {
            validator : function(val){
                return pincheck.isValid(val);
            },
            message : "Please enter a valid pincode"
        }
    });
    self.parentsPincode = ko.observable().extend({
        store : {
            key : 'parents_pincode'
        },
        required: {
            params:true,
            message: "Please select parents area pincode"
        },
        validation : {
            validator : function(val){
                return pincheck.isValid(val);
            },
            message : "Please enter a valid pincode"
        }
    });
    self.hasParents = function() {
        var par = ko.utils.arrayFilter(step1.getSelectedMembers(), function(member) { return member.person == 'Father' || member.person == 'Mother'  });
        if (par.length > 0) {
            return true;
        }
        else {
            return false;
        }
    };
    self.hasFamily = function() {
        var par = ko.utils.arrayFilter(step1.getSelectedMembers(), function(member) { return member.person == 'You' || member.person == 'Spouse' || member.type == 'kid'  });
        if (par.length > 0) {
            return true;
        }
        else {
            return false;
        }
    };

    self.getErrorGroup = function(){
        if(self.hasParents() && self.hasFamily()){
            self.step3Errors = ko.validation.group([self.userPincode, self.parentsPincode]);
        }
        if(!self.hasParents() && self.hasFamily()) {
            self.step3Errors = ko.validation.group([self.userPincode]);
        }
        if(self.hasParents() && !self.hasFamily()) {
            self.step3Errors = ko.validation.group([self.parentsPincode]);
        }
    };
    self.validate = function() {
        valid = true;
        self.getErrorGroup();
        if(!self.step3Errors().length == 0) {
            self.step3Errors.showAllMessages();
            valid = false;
        }
        return valid;
    };
};

function ExpertViewModel() {
    var self = this;
    self.email = ko.observable().extend({
        store : {
            key : 'email'
        },
        email: true,
        required: {
            params: EMAIL_REQUIRED,
            message: "Please enter valid email"
        }
    });
    self.mobile_required = MOBILE_REQUIRED;
    self.mobileNo = ko.observable().extend({
        store : {
            key : 'mobileNo'
        },
        required:{
            params: self.mobile_required,
            message: "Please enter your mobile number"
        },
        pattern: {
            params: '^([1-9])([0-9]){9}$',
            message: "Please enter a valid mobile number"
        }
    });

    self.getErrorGroup = function(){
        if(step_lead.currentStep() == 2){
            self.ExpertErrors = ko.validation.group([self.email, self.mobileNo]);
        } else {
            self.ExpertErrors = ko.validation.group([]);
        }
    };
    self.validate = function() {
        valid = true;
        self.getErrorGroup();
        if(!self.ExpertErrors().length == 0) {
            self.ExpertErrors.showAllMessages();
            valid = false;
        }
        return valid;
    };
}

var step1 = new stepOneViewModel();
var step2 = new stepTwoViewModel();
var step3 = new stepThreeViewModel();
var expert = new ExpertViewModel();

function stepLeadModel() {
    var self = this;
    self.change_current_step = function(x){
        self.currentStep(x);
    };
    self.show_proposition= ko.observable(true);
    self.show_loader = ko.observable(false);
    self.show_thanks_msg = ko.observable(false);
    self.currentStep = ko.observable(0);
    self.currentStepVerbose = ko.observable('0');
    self.currentStepVerbose.subscribe(function(val){
        self.currentStep(parseInt(val));
    });
    self.closeProposition = function(){
        self.show_proposition(false);
    };
    self.post_data = function() {
        var members = ko.toJS(step1.getSelectedMembers());
        ko.utils.arrayForEach(members, function(m){
            delete m.ageList;
            delete m.calculate_age;
        });
        var post_data = {
            members: members,
            gender: step1.gender(),
            pincode: step3.userPincode(),
            parents_pincode: step3.parentsPincode()
        };

        if(step2.is_topup)
            post_data.deductible = step2.deductible();
        else
            post_data.hospital_service = step2.service_type();

        return post_data;
    };
    self.saveAndLoadResults = function(){
        var valid = step3.validate();
        if(valid){
            var request = $.post('/health-plan/save-details/results/?hot=true', {'csrfmiddlewaretoken' : CSRF_TOKEN, 'data': JSON.stringify(self.post_data())}, function(res){
                res = JSON.parse(res);
                var search_url = CF_URL + "/health-plan" + (step2.is_topup ? '/plan/topup' : '');
                var results_url = search_url + "/results/?network="+TP+"&category=third_party#"+res.activity_id;
                console.log(results_url);
                step_lead.show_thanks_msg(REDIRECT_DELAY && true);
                setTimeout(function() {
                    Cookies.set('hide_login','1');
                    sendMessage({url:results_url});
                }, REDIRECT_DELAY);
            });
        }
        console.log('validation & post success');
    };

    self.sendDataToLms = function(){
        jsonData = self.post_data();
        jsonData['mobile'] = expert.mobileNo();
        jsonData['email'] = expert.email();
        jsonData['campaign'] = CAMPAIGN;
        jsonData['label'] = EVENT_LABEL;
        jsonData['triggered_page'] = TP || window.parent.location.href;

        $.ajax({
            type: "POST",
            url: "/leads/save-call-time/",
            data: {'data':JSON.stringify(jsonData),'csrfmiddlewaretoken':CSRF_TOKEN},
            complete: function(r){
                Cookies.set('mobileNo', expert.mobileNo());
                Cookies.set('email', expert.email());
                Cookies.set('lms_campaign', 'HEALTH');
                if (window.dataLayer) {
                    window.dataLayer.push({
                        'event': 'GAEvent',
                        'hitType': 'event',
                        'eventCategory': 'HEALTH',
                        'eventAction': 'CallScheduled',
                        'eventLabel': 'tp_health_form',
                        'eventValue': 4
                    });
                }
                self.saveAndLoadResults();
            }
        });
    };

    self.nextStep = function(){
        var valid = true;
        step1.members.valueHasMutated();
        if(parseInt(self.currentStep()) == 0){
            valid = step1.validate();
            if(valid){
                self.currentStep(2); // this is 1 or 2, 1 if you want to hide expert advice section, else 2
                self.currentStepVerbose("2"); // this is 1 or 2, 1 if you want to hide expert advice section, else 2
                return;
            }
        }
        if(parseInt(self.currentStep()) == 1){
            var valid2 = step2.validate();
            var valid3 = step3.validate();
            if(valid2 && valid3){
                self.show_loader(true);
                self.saveAndLoadResults();
            }
        }
        if(parseInt(self.currentStep()) == 2){
            var valid2 = step2.validate();
            var valid3 = step3.validate();
            var valid4 = expert.validate();
            if(valid2 && valid3 && valid4){
                self.show_loader(true);
                self.sendDataToLms();
            }
        }
    };
    self.lastStep = function() {
      self.currentStep(0);
    };
}
var step_lead = new stepLeadModel();

vm = {
    'step_lead' : step_lead,
    'step1': step1,
    'step2': step2,
    'step3': step3,
    'expert' : expert
};

ko.applyBindings(vm, document.getElementById('health_widget'));

function sendMessage(msg) {
    msg['id'] = IFRAME_ID;
    msg = JSON.stringify(msg);
    window.parent.postMessage(msg, '*');
}
document.body.style.overflowY = "hidden";
setInterval( function() { sendMessage({height:document.body.clientHeight+'px'});}, 200);
