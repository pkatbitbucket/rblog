/********* private variables *********/
var VEHICLE_TYPE_CAR = "fourwheeler";
var oldCarYears = utils.getPastYears();
var hero_email_month_field=false;
var fuel_types =
    [{'code': 'petrol', 'id': 'PETROL','name': 'Petrol'},
     {'code': 'diesel', 'id': 'DIESEL','name': 'Diesel'},
     {'code': 'cng_company', 'id': 'INTERNAL_LPG_CNG','name': 'CNG/LPG Company Fitted'},
     {'code': 'cng_external', 'id': 'PETROL','name': 'CNG/LPG Externally Fitted'}];

var selected_model_id_car,
    selected_fuel_type_id_car = 'PETROL';

/********* private functions *********/
var setUpCarForm = function() {
    var policy_type_value = $('[name=car_policy_type]:checked').val();
    if (policy_type_value == 'new') {
        $('.car-product .new-policy').removeClass('hide');
        $('.car-product .renew-policy').addClass('hide');
        $('.car-product .common-fields').removeClass('hide');
        $('.car-product .expired-policy').addClass('hide');
        $('.car-product .rto-field').addClass('col-md-offset-3');
    } else if (policy_type_value == 'renew') {
        $('.car-product .new-policy').addClass('hide');
        $('.car-product .renew-policy').removeClass('hide');
        $('.car-product .common-fields').removeClass('hide');
        $('.car-product .expired-policy').addClass('hide');
        $('.car-product .rto-field').removeClass('col-md-offset-3');
    } else if (policy_type_value == 'expired') {
        $('.car-product .expired-policy').removeClass('hide');
        $('.car-product .new-policy').addClass('hide');
        $('.car-product .renew-policy').addClass('hide');
        $('.car-product .common-fields').addClass('hide');
    }
};

function getVehicles_Car() {
    var url = '/motor/' + VEHICLE_TYPE_CAR + '/api/vehicles/';
    var data = { 'csrfmiddlewaretoken': CSRF_TOKEN }
    utils.getData(url,data,'#vehicle_make_model',"models");
}

function getCarVariants() {
    var select = $("#vehicle_variant");
    $(select).find('option').remove();
    if (selected_model_id_car && selected_fuel_type_id_car) {
        var url = '/motor/' + VEHICLE_TYPE_CAR + '/api/vehicles/' + selected_model_id_car + '/variants/';
        var data = {'csrfmiddlewaretoken': CSRF_TOKEN,'fuel_type': selected_fuel_type_id_car}
        utils.getData(url,data,"#vehicle_variant","variants");
    }
    else{
        utils.createSelect($('#vehicle_variant'),{})
    }
};

function getRTOs_Car() {
    var url = '/motor/' + VEHICLE_TYPE_CAR + '/api/rtos/';
    var data = {'csrfmiddlewaretoken': CSRF_TOKEN}
    utils.getData(url,data,"#vehicle_rto","rtos");
};

function setFuelTypes_Car() {
    utils.createSelect($('#vehicle_fuel_type'),fuel_types);
};

function setRegYears() {
    utils.createSelect($("#vehicle_reg_year"),oldCarYears);
};

function getVehicleDetails_Car(){
    var isCNG =0;
    if($('#vehicle_fuel_type').val())
        isCNG = $('#vehicle_fuel_type').val().indexOf('CNG')>0?1:0;
    return {
        policy_type: $('[name=car_policy_type]:checked').val(),
        expiry_slot: $('[name=expiry_slot]:checked').val(),
        vehicle_make_model: $('#vehicle_make_model').val(),
        vehicle_fuel_type: $('#vehicle_fuel_type').val(),
        vehicle_variant: $('#vehicle_variant').val(),
        vehicle_rto: $('#vehicle_rto :selected').val(),
        reg_year: $('#vehicle_reg_year :selected').text(),
        cngKitValue: $('#txtCNGKit').val()?$('#txtCNGKit').val():undefined,
        isCNGFitted: isCNG
    }
}

function setCarForm(){
    var bike = {};
    var cngVal, modelVal, fuelVal, variantVal,rtoVal,yearVal;
    // Fetch data from LocalStorage
    if(localStorage && localStorage.getItem(VEHICLE_TYPE_CAR)){
        var _local = JSON.parse(localStorage.getItem(VEHICLE_TYPE_CAR));
        bike = {
            expiry_slot: JSON.stringify(_local.expirySlot),
            vehicle_fuel_type : JSON.stringify(_local.fuel_type),
            vehicle_variant : JSON.stringify(_local.vehicleVariant),
            vehicle_make_model : JSON.stringify(_local.vehicle),
            vehicle_rto : JSON.stringify(_local.rto_info),
            vehicle_reg_year: JSON.stringify(_local.reg_year),
            cngKitValue: JSON.stringify(_local.cngKitValue),
            isCNGFitted: JSON.stringify(_local.isCNGFitted)
        }

        if(_local.vehicle)
            selected_model_id_car = _local.vehicle.id;
        if(_local.fuel_type)
            selected_fuel_type_id_car = _local.fuel_type.id;
    }
    // Fetch data from Cookie
    else{
         bike = {
            expiry_slot: Cookies.get(VEHICLE_TYPE_CAR+'_expiry_slot'),
            vehicle_fuel_type : utils.validateJSON(Cookies.get(VEHICLE_TYPE_CAR + '_fuel_type')),
            vehicle_variant : utils.validateJSON(Cookies.get(VEHICLE_TYPE_CAR+ '_vehicleVariant')),
            vehicle_make_model : utils.validateJSON(Cookies.get(VEHICLE_TYPE_CAR+'_vehicle')),
            vehicle_rto : Cookies.get(VEHICLE_TYPE_CAR+'_rto_info'),
            vehicle_reg_year: Cookies.get(VEHICLE_TYPE_CAR+ '_reg_year'),
            cngKitValue: Cookies.get(VEHICLE_TYPE_CAR + '_cngKitValue'),
            isCNGFitted: Cookies.get(VEHICLE_TYPE_CAR + '_isCNGFitted')
        }
    }
    if(bike.isCNG){
        $("#divCNGKit").fadeIn();
    }

    getCarVariants();
    if(bike.cngKitValue){
        cngVal = bike.cngKitValue;
    }
    else{
        cngVal = '';
        $("#txtCNGKit").removeClass('selected');
    }

    if(bike.vehicle_make_model && bike.vehicle_make_model.indexOf('{')>=0){
        modelVal = bike.vehicle_make_model;
    }
    else{

        modelVal = $('#vehicle_make_model option:first').val();
        $("#vehicle_make_model").parent().removeClass('selected');
    }

    if(bike.vehicle_fuel_type && bike.vehicle_fuel_type.indexOf('{')>=0){
        fuelVal = bike.vehicle_fuel_type;
    }
    else{
        fuelVal = $('#vehicle_fuel_type option:first').val()
        $("#vehicle_fuel_type").parent().removeClass('selected');
    }

    if(bike.vehicle_variant && bike.vehicle_variant.indexOf('{')>=0){
        variantVal = bike.vehicle_variant;
    }
    else{
        variantVal = $('#vehicle_variant option:first').val()
        $("#vehicle_variant").parent().removeClass('selected');
    }
    if(bike.vehicle_rto && bike.vehicle_rto.indexOf('{')>=0){
        rtoVal = bike.vehicle_rto;
    }
    else{
        rtoVal = $('#vehicle_rto option:first').val()
        $("#vehicle_rto").parent().removeClass('selected');
    }
    if(bike.vehicle_reg_year){
        yearVal = '{"id":' + bike.vehicle_reg_year + ',"name":'+ bike.vehicle_reg_year + ',"value":'+ bike.vehicle_reg_year + '}';
    }
    else{
        yearVal = $('#vehicle_reg_year option:first').val()
        $("#vehicle_reg_year").parent().removeClass('selected');
    }

    setTimeout(function(){
        $('#vehicle_make_model').select2().select2('val',modelVal);
        $('#vehicle_fuel_type').select2().select2('val',fuelVal);
        $('#vehicle_variant').select2().select2('val',variantVal);
        $('#vehicle_rto').select2().select2('val',rtoVal);
        $('#vehicle_reg_year').select2().select2('val',yearVal);
        if(bike.vehicle_fuel_type && bike.vehicle_fuel_type.indexOf("external")>1){
            $("#txtCNGKit").val(cngVal);
            //utils.showLabelForTextField($("#txtCNGKit"))
            $("#divCNGKit").show();
        }
        utils.showLabelForTextField($("#txtCNGKit"));
        utils.showLabelForInput($("#vehicle_make_model"));
        utils.showLabelForInput($("#vehicle_fuel_type"));
        utils.showLabelForInput($("#vehicle_variant"));
        utils.showLabelForInput($("#vehicle_rto"));
        utils.showLabelForInput($("#vehicle_reg_year"));
        setTimeout(function(){$(".loading").removeClass("loading");},0)
    },500);
}

var submitCarForm = function() {
    var vehicle = getVehicleDetails_Car(),
        redirect_url = '/motor/car-insurance/#results';
    // var mixpanel_data={
    //     'Insurance Type':  vehicle.policy_type+' Car Policy',
    //     'Network':network,
    //     'Category':category,
    // };
    // if(vehicle.policy_type != "expired"){
    //     mixpanel_data['Car Type'] = JSON.parse(vehicle.vehicle_make_model).name;
    //     mixpanel_data['Fuel Type'] = JSON.parse(vehicle.vehicle_fuel_type).name;
    //     mixpanel_data['Car Variant'] = JSON.parse(vehicle.vehicle_variant).name;
    //     mixpanel_data['Car City'] = JSON.parse(vehicle.vehicle_rto).name;
    // }
    // if (vehicle.policy_type == "renew")
    //     mixpanel_data['Car year'] = vehicle.reg_year;

    var data = {
        'isNewVehicle': (vehicle.policy_type == "new") ? '1' : '0',
        'vehicle': vehicle.vehicle_make_model,
        'vehicleVariant': vehicle.vehicle_variant,
        'fuel_type': vehicle.vehicle_fuel_type,
        'rto_info': vehicle.vehicle_rto,
        'cngKitValue': vehicle.cngKitValue,
        'isCNGFitted': vehicle.isCNGFitted
    };

    if (vehicle.policy_type == "renew") {
        data['reg_year'] = vehicle.reg_year;
        data['expirySlot'] = vehicle.expiry_slot;
    }

    if (vehicle.policy_type == "expired") {
        redirect_url = '/lp/car-insurance/renew-expired-policy/';
    }
    var gtm_data = {};
    $.post("/motor/submit-landing-page/", data, function(response) {
        response = JSON.parse(response);
        if (response.success) {
            //console.log("out LocalStorage", data);
            if (localStorage) {
                //console.log("in LocalStorage");
                localStorage.setItem(VEHICLE_TYPE_CAR, JSON.stringify(response.data));
            } else {
                for (var item in response.data) {
                    var value = response.data[item];
                    if (value != null && typeof(value) == "object") {
                        value = JSON.stringify(value);
                    }
                    Cookies.set(VEHICLE_TYPE_CAR + item, value);
                }
            }

            Cookies.set('mobileNo', cnum);
            Cookies.set('mobileNoURL', window.location.href);

            if(hero_phone_mandatory == "YES"){

                Cookies.set('mobileNoOn', 'LP - View Quotes with number');
                // mixpanel_data['Form Placement'] = 'lp-car-buy-direct-online';
                // mixpanel_data['Phone Number'] = cnum;
                // mixpanel_data['Phone Number Status'] = 'Phone Number Given';
                // mixpanel.track('Car LP Submitted',mixpanel_data);
            }
            else if(hero_email_month_field){
                Cookies.set('mobileNoOn', 'LP - View Quotes with number');
                // mixpanel_data['Form Placement'] = 'lp-car-buy-online-v2';
                // mixpanel_data['Email'] = cmail;
                // mixpanel_data['Phone Number Status'] = 'Phone Number Not Given';
                // mixpanel_data['Policy Expiry Month'] = month;
                // mixpanel.track('Car LP Submitted',mixpanel_data);

                data['email'] = cmail;
                data['month'] = month;
                data['campaign'] = 'motor';

                $.post("/leads/save-call-time/", {'data' : JSON.stringify(data),'csrfmiddlewaretoken' : CSRF_TOKEN},
                    function(response)
                    {
                        response = JSON.parse(response);
                        if (response.success) {
                            console.log("success");
                            // Send data to Google Analytics
                            Cookies.set('email', cmail);
                            push_gtm(response,"LP-BUY-Onile-V2", true);
                        }
                });

            }
            else{
                Cookies.set('mobileNoOn', 'LP - View Quotes with number');
                // mixpanel_data['Form Placement'] = 'lp-car-buy-online';
                // mixpanel_data['Phone Number Status'] = 'Phone Number Not Given';
                // mixpanel.track('Car LP Submitted',mixpanel_data);
                push_gtm(response, "LP-BUY-Onile", false);
            }

        }
    });
    function push_gtm(response, title, flag){
        // Send data to Google Analytics
        if(vehicle.policy_type != "expired")
            gtm_data = {
                param1: JSON.parse(data.vehicle).name,
                param2: JSON.parse(data.fuel_type).name,
                param3: JSON.parse(data.vehicleVariant).name,
                param4: JSON.parse(data.rto_info).name
            }

        if (vehicle.policy_type != "new")
            gtm_data.param5= data.reg_year;
        if(data.isCNGFitted)
            gtm_data.param6 = data.cngKitValue;
        if(flag){
                gtm_data.param7 = cmail;
                gtm_data.param8 = month;
            }
        var _posted = utils.gtm_push("CarEventGetQuote",
            title+" Car - Get Quote",
            vehicle.policy_type + " Car",
            " ", gtm_data );

        if(_posted){ window.location.href = redirect_url;}
    }

    if(hero_phone_mandatory == "YES"){

        data['mobile'] = cnum;
        data['campaign'] = 'motor';

        $.post("/leads/save-call-time/", {'data' : JSON.stringify(data),'csrfmiddlewaretoken' : CSRF_TOKEN},
            function(response)
            {
                response = JSON.parse(response);
                if (response.success) {
                    // Send data to Google Analytics
                    Cookies.set('mobileNo', cnum);
                    var gtm_data = {}
                    if(vehicle.policy_type != "expired")
                        gtm_data = {
                            param1: JSON.parse(data.vehicle).name,
                            param2: JSON.parse(data.fuel_type).name,
                            param3: JSON.parse(data.vehicleVariant).name,
                            param4: JSON.parse(data.rto_info).name
                        }
                    if (vehicle.policy_type != "new")
                        gtm_data.param5= data.reg_year;
                    if(data.isCNGFitted)
                        gtm_data.param6 = data.cngKitValue;

                    gtm_data.param7 = data.mobile;

                    var _posted  = utils.gtm_push("CarEventGetQuote",
                        "LP-BUY-Direct-Online Car - Get Quote",
                        vehicle.policy_type + " Car",
                        " ", gtm_data );

                    if(_posted){ window.location.href = redirect_url;}
                }
        });
    }


};

 var validateCarForm = function() {
    var valid = true;
    $('.error-text').hide();
    var vehicle = {
        policy_type: $('[name=car_policy_type]:checked').val(),
        expiry_slot: $('[name=expiry_slot]:checked').val(),
        vehicle_make_model: $('#vehicle_make_model :selected').attr("id"),
        vehicle_fuel_type: $('#vehicle_fuel_type :selected').attr("id"),
        vehicle_variant: $('#vehicle_variant :selected').attr("id"),
        vehicle_rto: $('#vehicle_rto :selected').attr("id"),
        reg_year: $('#vehicle_reg_year :selected').attr("id"),
        cngKitValue: $('#txtCNGKit').val()
    },
    whatsapp_checked = $("#whatsapp-checkbox").attr("checked");

    if (vehicle.policy_type != "expired") {
        if (!vehicle.vehicle_make_model || !vehicle.vehicle_fuel_type || !vehicle.vehicle_variant) {
            valid = false;
            $('#vehicle_detail_error').fadeIn();
        }
        if (!vehicle.vehicle_rto) {
            valid = false;
            $('#rto_error').fadeIn();
        }
        if ((vehicle.policy_type == "renew") && !vehicle.reg_year) {
            valid = false;
            $('#reg_date_error').fadeIn();
        }

        if($('#vehicle_fuel_type :selected').attr("name") == "CNG/LPG Externally Fitted" && vehicle.cngKitValue == 0){
            valid = false;
            $('#cng_error').fadeIn();
        }
    }
    return valid;
};

/********* Event functions *********/
$('input[name="car_policy_type"]').change(function() {
    setUpCarForm();
});

$("#vehicle_make_model").on("change",function() {
    selected_model_id_car = $(this).children(":selected").attr("id");
    getCarVariants();
});

$("#vehicle_fuel_type").on("change",function() {
    selected_fuel_type_id_car = $(this).children(":selected").attr("id");
    getCarVariants();
    var selected_fuel =  $(this).children(":selected").text().trim();
    if(selected_fuel=="CNG/LPG Externally Fitted"){
        $("#divCNGKit").removeClass("hide").fadeIn();
        if($('#txtCNGKit').val().length == 0){
            ///$('#txtCNGKit').val(0);
            $('#txtCNGKit').parent().addClass("selected").removeClass("hide")
        }
    }
    else{
        $('#divCNGKit').fadeOut();
        $('#txtCNGKit').val('');
    }
});

$("#vehicle_variant").on("change",function(){
    if($("option:selected", this).attr("id") >0 || $("option:selected", this).attr("id").length>0)
        $('#vehicle_detail_error').fadeOut();
});

$("#vehicle_rto").on("change", function(){
    if($("option:selected", this).attr("id") >0 || $("option:selected", this).attr("id").length>0)
        $('#rto_error').fadeOut();
});

$("#vehicle_reg_year").on("change",function () {
    if($("option:selected", this).attr("id") >0 || $("option:selected", this).attr("id").length>0)
        $('#reg_date_error').fadeOut();
})
var cnum;
var cmail;
var month;
$('#car_submit').on('click', function(event) {
    event.preventDefault();
    if( hero_phone_mandatory == 'YES' ){
        $('.contact-me-error').hide();
        cnum = $('#contact-me-num').val();
        var res = cnum.match(/[7-9]{1}\d{9}/);
        if(cnum.length != 10 || !res)  {
            $('.contact-me-error').show();
            return;
        }
    }
    if( hero_email_month_field){
        $('.contact-me-error').hide();
        month = $('#month').val();
        cmail = $('#contact-me-email').val();
        var res = cmail.match(/^([A-Z0-9]+([+-._]?[A-Z0-9]+)*)@((?:[A-Z0-9-]+\.)*\w[A-Z0-9-]{0,66})\.([A-Z]{2,6}(?:\.[A-Z]{2})?)$/i);
        if(month == 0){
            $('#month-error').show();
            return;
        }
        if(!res)  {
            $('#email-error').show();
            return;
        }
    }
    if (validateCarForm())
        submitCarForm();
});

$("#txtCNGKit").on("focus", function(){
    if($(this).val()<=0)
        $(this).val('');
});
$("#txtCNGKit").on("blur",function(){
    if($(this).val()<=0){
        $(this).val('');
    }
});

/********* Init functions *********/
(function initCarForm(){
    // Fetch data and assign to controls
    setUpCarForm();
    getRTOs_Car();
    getVehicles_Car();
    setFuelTypes_Car();
    setRegYears();
    setCarForm();
})();
