/********* private variables *********/

$.fn.select2.defaults.shouldFocusInput = function(instance){
    return false;
};

var VEHICLE_TYPE_CAR = "fourwheeler";
var oldCarYears = utils.getPastYears();
var hero_email_month_field=false;
var fuel_types =
    [{'code': 'petrol', 'id': 'PETROL','name': 'Petrol'},
     {'code': 'diesel', 'id': 'DIESEL','name': 'Diesel'},
     {'code': 'cng_company', 'id': 'INTERNAL_LPG_CNG','name': 'CNG/LPG Company Fitted'},
     {'code': 'cng_external', 'id': 'PETROL','name': 'CNG/LPG Externally Fitted'}];

var selected_model_id_car,
    selected_fuel_type_id_car = 'PETROL';

/********* private functions *********/
var setUpCarForm = function() {
    initExpirePolicyForm();
    var policy_type_value = $('[name=car_policy_type]:checked').val();
    if (policy_type_value == 'new') {
        $('.car-product .new-policy').removeClass('hide');
        $('.car-product .renew-policy').addClass('hide');
        $('.car-product .common-fields').removeClass('hide');
        $('.car-product .expired-policy').addClass('hide');
        $('.car-product .rto-field').addClass('col-md-offset-3');
    } else if (policy_type_value == 'renew' || policy_type_value == 'expired') {
        $('.car-product .new-policy').addClass('hide');
        $('.car-product .renew-policy').removeClass('hide');
        $('.car-product .common-fields').removeClass('hide');
        $('.car-product .expired-policy').addClass('hide');
        $('.car-product .rto-field').removeClass('col-md-offset-3');
    } /*else if (policy_type_value == 'expired') {
        $('.car-product .expired-policy').removeClass('hide');
        $('.car-product .new-policy').addClass('hide');
        $('.car-product .renew-policy').addClass('hide');
        $('.car-product .common-fields').addClass('hide');
    }*/

    if(policy_type_value == 'expired'){
        $("#expired_car").removeClass("hide");
    }
    else{
        $("#expired_car").addClass("hide");
    }

};

function populateExpiryDate(str){
    var year = $("#car_expiry_year :selected").attr("id");
    var month= $("#car_expiry_month :selected").attr("id");
    var monthlen = new Date(year,parseInt(month) ,0).getDate();
    var dates = [];

    for(var i =1; i<=monthlen; i++)
        dates.push({"id":i,"name":i, "value":i})
    utils.createSelect($("#car_expiry_date"),dates);
}

function getVehicles_Car() {
    var url = '/motor/' + VEHICLE_TYPE_CAR + '/api/vehicles/';
    var data = { 'csrfmiddlewaretoken': CSRF_TOKEN }
    utils.getData(url,data,'#vehicle_make_model',"models");
}

function getCarVariants() {
    var select = $("#vehicle_variant");
    $(select).find('option').remove();
    if (selected_model_id_car && selected_fuel_type_id_car) {
        var url = '/motor/' + VEHICLE_TYPE_CAR + '/api/vehicles/' + selected_model_id_car + '/variants/';
        var data = {'csrfmiddlewaretoken': CSRF_TOKEN,'fuel_type': selected_fuel_type_id_car}
        utils.getData(url,data,"#vehicle_variant","variants");
    }
    else{
        utils.createSelect($('#vehicle_variant'),{})
    }
};

function getRTOs_Car() {
    var url = '/motor/' + VEHICLE_TYPE_CAR + '/api/rtos/';
    var data = {'csrfmiddlewaretoken': CSRF_TOKEN}
    utils.getData(url,data,"#vehicle_rto","rtos");
};

function setFuelTypes_Car() {
    utils.createSelect($('#vehicle_fuel_type'),fuel_types);
};

function setRegYears() {
    utils.createSelect($("#vehicle_reg_year"),oldCarYears);
};

function getVehicleDetails_Car(){
    var isCNG =0;
    if($('#vehicle_fuel_type').val())
        isCNG = $('#vehicle_fuel_type').val().indexOf('CNG')>0?1:0;
    return {
        policy_type: $('[name=car_policy_type]:checked').val(),
        expiry_slot: $('[name=expiry_slot]:checked').val(),
        vehicle_make_model: $('#vehicle_make_model').val(),
        vehicle_fuel_type: $('#vehicle_fuel_type').val(),
        vehicle_variant: $('#vehicle_variant').val(),
        vehicle_rto: $('#vehicle_rto :selected').val(),
        reg_year: $('#vehicle_reg_year :selected').text(),
        cngKitValue: $('#txtCNGKit').val()?$('#txtCNGKit').val():undefined,
        isCNGFitted: isCNG
    }
}

function setCarForm(){
    var car = {};
    var cngVal, modelVal, fuelVal, variantVal,rtoVal,yearVal, _expirtDate, _expiryMonth, _expiryYear;
    // Fetch data from LocalStorage
    if(localStorage && localStorage.getItem(VEHICLE_TYPE_CAR)){
        var _local = JSON.parse(localStorage.getItem(VEHICLE_TYPE_CAR));
        car = {
            expiry_slot: JSON.stringify(_local.expirySlot),
            expiry_date:JSON.stringify(_local.expiry_date),
            expiry_month:JSON.stringify(_local.expiry_month),
            expiry_year:JSON.stringify(_local.expiry_year),
            vehicle_fuel_type : JSON.stringify(_local.fuel_type),
            vehicle_variant : JSON.stringify(_local.vehicleVariant),
            vehicle_make_model : JSON.stringify(_local.vehicle),
            vehicle_rto : JSON.stringify(_local.rto_info),
            vehicle_reg_year: JSON.stringify(_local.reg_year),
            cngKitValue: JSON.stringify(_local.cngKitValue),
            isCNGFitted: JSON.stringify(_local.isCNGFitted)
        }

        if(_local.vehicle)
            selected_model_id_car = _local.vehicle.id;
        if(_local.fuel_type)
            selected_fuel_type_id_car = _local.fuel_type.id;
    }
    // Fetch data from Cookie
    else{
         car = {
            expiry_slot: Cookies.get(VEHICLE_TYPE_CAR+'_expiry_slot'),
            expiry_date:Cookies.get(VEHICLE_TYPE_CAR+'_expiryDate'),
            expiry_month:Cookies.get(VEHICLE_TYPE_CAR+'_expiryMonth'),
            expiry_year:Cookies.get(VEHICLE_TYPE_CAR+'_expiryYear'),
            vehicle_fuel_type : utils.validateJSON(Cookies.get(VEHICLE_TYPE_CAR + '_fuel_type')),
            vehicle_variant : utils.validateJSON(Cookies.get(VEHICLE_TYPE_CAR+ '_vehicleVariant')),
            vehicle_make_model : utils.validateJSON(Cookies.get(VEHICLE_TYPE_CAR+'_vehicle')),
            vehicle_rto : Cookies.get(VEHICLE_TYPE_CAR+'_rto_info'),
            vehicle_reg_year: Cookies.get(VEHICLE_TYPE_CAR+ '_reg_year'),
            cngKitValue: Cookies.get(VEHICLE_TYPE_CAR + '_cngKitValue'),
            isCNGFitted: Cookies.get(VEHICLE_TYPE_CAR + '_isCNGFitted')
        }
    }
    if(car.isCNG){
        $("#divCNGKit").fadeIn();
    }

    getCarVariants();
    if(car.cngKitValue){
        cngVal = car.cngKitValue;
    }
    else{
        cngVal = '';
        $("#txtCNGKit").removeClass('selected');
    }

    if(car.vehicle_make_model && car.vehicle_make_model.indexOf('{')>=0){
        modelVal = car.vehicle_make_model;
    }
    else{

        modelVal = $('#vehicle_make_model option:first').val();
        $("#vehicle_make_model").parent().removeClass('selected');
    }

    if(car.vehicle_fuel_type && car.vehicle_fuel_type.indexOf('{')>=0){
        fuelVal = car.vehicle_fuel_type;
    }
    else{
        fuelVal = $('#vehicle_fuel_type option:first').val()
        $("#vehicle_fuel_type").parent().removeClass('selected');
    }

    if(car.vehicle_variant && car.vehicle_variant.indexOf('{')>=0){
        variantVal = car.vehicle_variant;
    }
    else{
        variantVal = $('#vehicle_variant option:first').val()
        $("#vehicle_variant").parent().removeClass('selected');
    }
    if(car.vehicle_rto && car.vehicle_rto.indexOf('{')>=0){
        rtoVal = car.vehicle_rto;
    }
    else{
        rtoVal = $('#vehicle_rto option:first').val()
        $("#vehicle_rto").parent().removeClass('selected');
    }
    if(car.vehicle_reg_year){
        yearVal = '{"id":' + car.vehicle_reg_year + ',"name":'+ car.vehicle_reg_year + ',"value":'+ car.vehicle_reg_year + '}';
    }
    else{
        yearVal = $('#vehicle_reg_year option:first').val()
        $("#vehicle_reg_year").parent().removeClass('selected');
    }
    if(car.expiry_year){
        _expiryYear = car.expiry_year;
    }
    else{
        _expiryYear = $('#car_expiry_year option:first').val()
        $("#car_expiry_year").parent().removeClass('selected');
    }

    if(car.expiry_month){
        _expiryMonth = car.expiry_month;
    }
    else{
        _expiryMonth = $('#car_expiry_month option:first').val()
        $("#car_expiry_month").parent().removeClass('selected');
    }

    if(car.expiry_date){
        _expiryDate = car.expiry_date;
    }
    else{
        _expiryDate = $('#car_expiry_date option:first').val()
        $("#car_expiry_date").parent().removeClass('selected');
    }

    setTimeout(function(){
        $('#vehicle_make_model').select2().select2('val',modelVal);
        $('#vehicle_fuel_type').select2().select2('val',fuelVal);
        $('#vehicle_variant').select2().select2('val',variantVal);

        $('#vehicle_rto').select2().select2('val',rtoVal);

        $('#vehicle_reg_year').select2().select2('val',yearVal);

        $('#car_expiry_date').select2().select2("val",_expiryDate);
        $('#car_expiry_month').select2().select2("val",_expiryMonth);
        $('#car_expiry_year').select2().select2("val",_expiryYear);

        if(car.vehicle_fuel_type && car.vehicle_fuel_type.indexOf("external")>1){
            $("#txtCNGKit").val(cngVal);
            //utils.showLabelForTextField($("#txtCNGKit"))
            $("#divCNGKit").show();
        }
        utils.showLabelForTextField($("#txtCNGKit"));
        utils.showLabelForInput($("#vehicle_make_model"));
        utils.showLabelForInput($("#vehicle_fuel_type"));
        utils.showLabelForInput($("#vehicle_variant"));
        utils.showLabelForInput($("#vehicle_rto"));
        utils.showLabelForInput($("#vehicle_reg_year"));
        utils.showLabelForInput($('#car_expiry_date'));
        utils.showLabelForInput($('#car_expiry_month'));
        utils.showLabelForInput($('#car_expiry_year'));
        setTimeout(function(){$(".loading").removeClass("loading");},0)
    },500);
}

function initExpirePolicyForm(){
    var years = [];
    for(var i = new Date().getFullYear(); i>=1999; i--)
        years.push({'id':i,'name':i, 'value':i});

    utils.createSelect_new($("#car_expiry_year"),years);
    utils.createSelect_new($("#car_expiry_month"),monthList);
    utils.createSelect_new($("#car_expiry_date"),{});

    disableExpiryYear();
    disableExpiryMonth();
    disableExpiryDate();
}
function disableExpiryYear(){
    if($("#vehicle_reg_year option:selected").val()){
        var year = JSON.parse($("#vehicle_reg_year option:selected").val()).name;
        var newData = [];
        for(var i = new Date().getFullYear(); i>= year; i--)
            newData.push({'id':i,'name':i, 'value':i});
        utils.createSelect($("#car_expiry_year"), newData);
        utils.createSelect($("#car_expiry_month"),monthList);
        utils.createSelect($("#car_expiry_date"),{});
    }
}

function disableExpiryMonth(){
    if($("#car_expiry_year option:selected").val()){
        var year = JSON.parse($("#car_expiry_year option:selected").val()).name,
            currentDate = new Date();
        if(year == currentDate.getFullYear()){
            var month = currentDate.getMonth() +1;
            var newData = [];
            $.each(monthList, function(index, row){
                if(row.id <= month)
                    newData.push(row);
            });
        utils.createSelect($("#car_expiry_month"),newData);
        }
        else{
            utils.createSelect($("#car_expiry_month"),monthList);
        }
    }
}

function disableExpiryDate(){
    if($("#car_expiry_month option:selected").val()){
        var month = JSON.parse($("#car_expiry_month option:selected").val()).id,
            currentDate = new Date();
        var year = JSON.parse($("#car_expiry_year option:selected").val()).name;
        var date = currentDate.getDate();
        var monthlen = new Date(currentDate.getFullYear(),parseInt(month) ,0).getDate();
        var dates = [];
        var newData = [];

        for(var i =1; i<=monthlen; i++)
            dates.push({"id":i,"name":i, "value":i})

        if(month == currentDate.getMonth() +1 && year == currentDate.getFullYear()){
            $.each(dates, function(index, row){
                if(row.id < date)
                    newData.push(row);
            });
            utils.createSelect($("#car_expiry_date"),newData);
        }
        else{
            utils.createSelect($("#car_expiry_date"),dates);
        }
    }
}

var submitCarForm = function() {
    var vehicle = getVehicleDetails_Car(),
        redirect_url = '/motor/car-insurance/#results';
    /*var mixpanel_data={
        'Insurance Type':  vehicle.policy_type+' Car Policy',
        'Network':network,
        'Category':category,
    };*/
   // if(vehicle.policy_type != "expired"){
        // mixpanel_data['Car Type'] = JSON.parse(vehicle.vehicle_make_model).name;
        // mixpanel_data['Fuel Type'] = JSON.parse(vehicle.vehicle_fuel_type).name;
        // mixpanel_data['Car Variant'] = JSON.parse(vehicle.vehicle_variant).name;
        // mixpanel_data['Car City'] = JSON.parse(vehicle.vehicle_rto).name;
    //}
    // if (vehicle.policy_type == "renew" || vehicle.policy_type == "expired")
    //     mixpanel_data['Car year'] = vehicle.reg_year;
    // if(vehicle.policy_type == "expired"){
    //     mixpanel_data['Expiry Date'] = $("#car_expiry_date :selected").attr("id");
    //     mixpanel_data["Expiry Month"]=$("#car_expiry_month :selected").attr("name");
    //     mixpanel_data["Expiry Year"] =$("#car_expiry_year :selected").attr("id");
    // }
    var data = {
        'isNewVehicle': (vehicle.policy_type == "new") ? '1' : '0',
        'vehicle': vehicle.vehicle_make_model,
        'vehicleVariant': vehicle.vehicle_variant,
        'fuel_type': vehicle.vehicle_fuel_type,
        'rto_info': vehicle.vehicle_rto,
        'cngKitValue': vehicle.cngKitValue,
        'isCNGFitted': vehicle.isCNGFitted
    };

    if (vehicle.policy_type == "renew" || vehicle.policy_type == "expired") {
        data['reg_year'] = vehicle.reg_year;
        data['expirySlot'] = vehicle.expiry_slot;
    }

    if (vehicle.policy_type == "expired") {
        //redirect_url = '/lp/car-insurance/renew-expired-policy/';
        data['expiry_date'] = $("#car_expiry_date :selected").attr("id");
        data["expiry_month"] =$("#car_expiry_month :selected").attr("name");
        data["expiry_year"] =$("#car_expiry_year :selected").attr("id");

        Cookies.set("lms_campaign","motor-offlinecases");
        Cookies.set("mark_product_category","commonAncestorContainer");
    }
    var gtm_data = {};
    $.post("/motor/submit-landing-page/", data, function(response) {
        response = JSON.parse(response);
        if (response.success) {
            if (localStorage) {
                if(vehicle.policy_type == "expired"){
                    response.data["policyExpired"] =true;
                }
                localStorage.setItem(VEHICLE_TYPE_CAR, JSON.stringify(response.data));
            } else {
                for (var item in response.data) {
                    var value = response.data[item];
                    if (value != null && typeof(value) == "object") {
                        value = JSON.stringify(value);
                    }
                    Cookies.set(VEHICLE_TYPE_CAR + item, value);
                }
            }

            Cookies.set('mobileNo', cnum);
            Cookies.set('mobileNoURL', window.location.href);

            if(hero_phone_mandatory == "YES"){

                Cookies.set('mobileNoOn', 'LP - View Quotes with number');
                // mixpanel_data['Form Placement'] = 'lp-car-buy-direct-online';
                // mixpanel_data['Phone Number'] = cnum;
                // mixpanel_data['Phone Number Status'] = 'Phone Number Given';
                // mixpanel.track('Car LP Submitted',mixpanel_data);

                data['mobile'] = cnum;
                data['campaign'] = 'motor';

                $.post("/leads/save-call-time/", {'data' : JSON.stringify(data),'csrfmiddlewaretoken' : CSRF_TOKEN},
                    function(response)
                    {
                        response = JSON.parse(response);
                        if (response.success) {
                            // Send data to Google Analytics
                            Cookies.set('mobileNo', cnum);
                            var gtm_data = {}
                            //if(vehicle.policy_type != "expired")
                                gtm_data = {
                                    param1: JSON.parse(data.vehicle).name,
                                    param2: JSON.parse(data.fuel_type).name,
                                    param3: JSON.parse(data.vehicleVariant).name,
                                    param4: JSON.parse(data.rto_info).name
                                }
                            if (vehicle.policy_type != "new")
                                gtm_data.param5= data.reg_year;
                            if(data.isCNGFitted)
                                gtm_data.param6 = data.cngKitValue;

                            gtm_data.param7 = data.mobile;

                            if (window.dataLayer){
                                window.dataLayer.push({'gtm_mobile': cnum});
                                window.dataLayer.push({
                                    'event': 'GAEvent',
                                    'hitType': 'event',
                                    'eventCategory': 'MOTOR',
                                    'eventAction': 'CallScheduled',
                                    'eventLabel': page_id,
                                    'eventValue': 4
                                 });
                            }

                            utils.gtm_push("CarEventGetQuote",
                                "LP-BUY-Direct-Online Car - Get Quote",
                                vehicle.policy_type + " Car",
                                " ", gtm_data,
                                function(){ window.location.href = redirect_url;} );
                        }
                });
            }
            else if(hero_email_month_field){
                Cookies.set('mobileNoOn', 'LP - View Quotes with number');
                // mixpanel_data['Form Placement'] = 'lp-car-buy-online-v2';
                // mixpanel_data['Email'] = cmail;
                // mixpanel_data['Phone Number Status'] = 'Phone Number Not Given';
                // mixpanel_data['Policy Expiry Month'] = month;
                // mixpanel.track('Car LP Submitted',mixpanel_data);

                data['email'] = cmail;
                data['month'] = month;
                data['campaign'] = 'motor';

                $.post("/leads/save-call-time/", {'data' : JSON.stringify(data),'csrfmiddlewaretoken' : CSRF_TOKEN},
                    function(response){
                        response = JSON.parse(response);
                        if (response.success) {
                            // Send data to Google Analytics
                            Cookies.set('email', cmail);
                            push_gtm(response,"LP-BUY-Onile-V2", true);
                        }
                });

            }
            else{
                Cookies.set('mobileNoOn', 'LP - View Quotes with number');
                // mixpanel_data['Form Placement'] = 'lp-car-buy-online';
                // mixpanel_data['Phone Number Status'] = 'Phone Number Not Given';
                // mixpanel.track('Car LP Submitted',mixpanel_data);
                push_gtm(response, "LP-BUY-Onile", false);
            }

        }
    });

    function push_gtm(response, title, flag){
        // Send data to Google Analytics
       // if(vehicle.policy_type != "expired")
            gtm_data = {
                param1: JSON.parse(data.vehicle).name,
                param2: JSON.parse(data.fuel_type).name,
                param3: JSON.parse(data.vehicleVariant).name,
                param4: JSON.parse(data.rto_info).name
            }

        if (vehicle.policy_type != "new")
            gtm_data.param5= data.reg_year;
        if(data.isCNGFitted)
            gtm_data.param6 = data.cngKitValue;
        if(flag){
                gtm_data.param7 = cmail;
                gtm_data.param8 = month;
            }
        utils.gtm_push("CarEventGetQuote",
            title+" Car - Get Quote",
            vehicle.policy_type + " Car",
            " ", gtm_data,
            function(){window.location.href = redirect_url;});
    }
};

 var validateCarForm = function() {
    var valid = true;
    $('.error-text').hide();
    var vehicle = {
        policy_type: $('[name=car_policy_type]:checked').val(),
        expiry_slot: $('[name=expiry_slot]:checked').val(),
        expiry_date: $("#car_expiry_date :selected").attr("id"),
        expiry_month:$("#car_expiry_month :selected").attr("name"),
        expiry_year:$("#car_expiry_year :selected").attr("id"),
        vehicle_make_model: $('#vehicle_make_model :selected').attr("id"),
        vehicle_fuel_type: $('#vehicle_fuel_type :selected').attr("id"),
        vehicle_variant: $('#vehicle_variant :selected').attr("id"),
        vehicle_rto: $('#vehicle_rto :selected').attr("id"),
        reg_year: $('#vehicle_reg_year :selected').attr("id"),
        cngKitValue: $('#txtCNGKit').val()
    },
    whatsapp_checked = $("#whatsapp-checkbox").attr("checked");

    if (!vehicle.vehicle_make_model || !vehicle.vehicle_fuel_type || !vehicle.vehicle_variant) {
        valid = false;
        $('#vehicle_detail_error').fadeIn();
    }
    if (!vehicle.vehicle_rto) {
        valid = false;
        $('#rto_error').fadeIn();
    }
    if ((vehicle.policy_type != "new") && !vehicle.reg_year) {
        valid = false;
        $('#reg_date_error').fadeIn();
    }

    if($('#vehicle_fuel_type :selected').attr("name") == "CNG/LPG Externally Fitted" && vehicle.cngKitValue == 0){
        valid = false;
        $('#cng_error').fadeIn();
    }

    if (vehicle.policy_type == "expired") {
        if(!vehicle.expiry_date){
            valid = false;
            $("#car_expiry_date_error").fadeIn();
        }
        if(!vehicle.expiry_month){
            valid = false;
            $("#car_expiry_month_error").fadeIn();
        }
        if(!vehicle.expiry_year){
            valid = false;
            $("#car_expiry_year_error").fadeIn();
        }
    }
    return valid;
};

/********* Event functions *********/
$('input[name="car_policy_type"]').change(function() {
    setUpCarForm();
});

$("#vehicle_make_model").on("change",function() {
    selected_model_id_car = $(this).children(":selected").attr("id");
    getCarVariants();
});

$("#vehicle_fuel_type").on("change",function() {
    selected_fuel_type_id_car = $(this).children(":selected").attr("id");
    getCarVariants();
    var selected_fuel =  $(this).children(":selected").text().trim();
    if(selected_fuel=="CNG/LPG Externally Fitted"){
        $("#divCNGKit").removeClass("hide").fadeIn();
        if($('#txtCNGKit').val().length == 0){
            ///$('#txtCNGKit').val(0);
            $('#txtCNGKit').parent().addClass("selected").removeClass("hide")
        }
    }
    else{
        $('#divCNGKit').fadeOut();
        $('#txtCNGKit').val('');
    }
});

$("#vehicle_variant").on("change",function(){
    if($("option:selected", this).attr("id") >0 || $("option:selected", this).attr("id").length>0)
        $('#vehicle_detail_error').fadeOut();
});

$("#vehicle_rto").on("change", function(){
    if($("option:selected", this).attr("id") >0 || $("option:selected", this).attr("id").length>0)
        $('#rto_error').fadeOut();
});

$("#vehicle_reg_year").on("change",function () {
    if($("option:selected", this).attr("id") >0 || $("option:selected", this).attr("id").length>0)
        $('#reg_date_error').fadeOut();
})
var cnum;
var cmail;
var month;
$('#car_submit').on('click', function(event) {
    event.preventDefault();
    if( hero_phone_mandatory == 'YES' ){
        $('.contact-me-error').hide();
        cnum = $('#contact-me-num').val();
        var res = cnum.match(/[7-9]{1}\d{9}/);
        if(cnum.length != 10)  {
            $('.contact-me-error').show();
            $('.contact-me-error').children().text("Please enter a valid 10 digit mobile number")
            return;
        }
    }
    if( hero_email_month_field){
        $('.contact-me-error').hide();
        month = $('#month').val();
        cmail = $('#contact-me-email').val();
        var res = cmail.match(/^([A-Z0-9]+([+-._]?[A-Z0-9]+)*)@((?:[A-Z0-9-]+\.)*\w[A-Z0-9-]{0,66})\.([A-Z]{2,6}(?:\.[A-Z]{2})?)$/i);
        if(month == 0){
            $('#month-error').show();
            return;
        }
        if(!res)  {
            $('#email-error').show();
            return;
        }
    }
    if (validateCarForm())
        submitCarForm();
});

$("#txtCNGKit").on("focus", function(){
    if($(this).val()<=0)
        $(this).val('');
});
$("#txtCNGKit").on("blur",function(){
    if($(this).val()<=0){
        $(this).val('');
    }
});

$("#car_expiry_month,#car_expiry_year").on("change", function(){
    if($("option:selected", this).attr("id")>0){
        var err = "#"+ $(this).attr("id")+ "_error";
        $(err).fadeOut();
    }
    $("#car_expiry_date").find('option').remove();
    populateExpiryDate();

    if($(this).attr("id") == "car_expiry_year"){
        disableExpiryMonth();
    }
    else if($(this).attr("id") == "car_expiry_month"){
        disableExpiryDate();
    }
});

$("#car_expiry_date").on("change", function(){
    if($("option:selected", this).attr("id")>0){
        var err = "#"+ $(this).attr("id")+ "_error";
        $(err).fadeOut();
    }
});

/********* Init functions *********/
(function initCarForm(){
    // Fetch data and assign to controls
    setUpCarForm();
    getRTOs_Car();
    getVehicles_Car();
    setFuelTypes_Car();
    setRegYears();
    setCarForm();

    utils.createMobileField("#contact-me-num");

})();
