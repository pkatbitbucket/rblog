var save_lead_callback;
var resend_count = 0;

function close_popup(){
    $('#otp_popup').addClass('hide');
    $('#sent_otp').addClass('hide');
    $('#resend_otp').text('Resend OTP');
}

function send_otp(callback) {
    $('.otp-error').hide();
    $.post("/leads/send-otp/", {'csrfmiddlewaretoken' : CSRF_TOKEN}, function (data) {
        if (data.success) {
            $('#otp_popup').removeClass('hide');
            $('#mob-num').text($('#contact-me-num').val());
            save_lead_callback = callback;
        } else {
            $('.contact-me-error').text('SMS delivery failed. Please try again');
            $('.contact-me-error').show();
        }
    });
}

function resend_otp () {
    $('.otp-error').hide();
    var otp_button = $('#otp_submit_btn');
    var resend_btn = $('#resend_otp');
    if(resend_count > 2){
        $('#resending_otp').addClass('hide');
        resend_btn.addClass('hide');
        $('#sent_otp').addClass('hide');
        return;
    }
    resend_count++;
    resend_btn.addClass('hide');
    $('#sent_otp').addClass('hide');
    $('#resending_otp').removeClass('hide');
    $('#wrong-number').addClass('hide');
    otp_button.addClass('disable');
    otp_button.disabled = false;
    $.post("/leads/send-otp/", {'csrfmiddlewaretoken' : CSRF_TOKEN}, function (data) {
        if (data.success) {
            otp_button.removeClass('disable');
            otp_button.disabled = false;
        } else {
            $('.contact-me-error').text('SMS delivery failed. Please try again');
            $('.contact-me-error').show();
        }
        $('#resending_otp').addClass('hide');
        $('#sent_otp').removeClass('hide');
        $('#wrong-number').removeClass('hide');
        resend_btn.removeClass('hide');
        resend_btn.text('Send Again');
    });
}

function verify_otp (otp) {
    $('.otp-error').hide();
    var otp_button = $('#otp_submit_btn').first();
    otp_button.text('Verifying');
    otp_button.addClass('disable');
    otp_button.disabled = true;
    $.post("/leads/verify-otp/",
        {'data': JSON.stringify({'otp': otp}), 'csrfmiddlewaretoken' : CSRF_TOKEN},
        function (result) {
            if (result.success) {
                window.dataLayer.push({
                    'event': 'VerifyOTP',
                    'hitType': 'event',
                    'category': 'MOTOR',
                    'action': 'Verify OTP',
                    'label': Cookies.get('label') || page_id,
                    'tracker_id': tracker_id,
                    'eventValue': 0
                });
                save_lead_callback(true, true);
            } else {
                $('.otp-error').show();
                otp_button.text('Verify');
                otp_button.removeClass('disable');
                otp_button.disabled = false;
            }
        },
        'json'
    );
}

$(document).on('click', '#otp_submit_btn', function() {
    verify_otp($('#otp').val());
});
