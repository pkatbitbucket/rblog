$(function(){
    // Get user information from cookies if any
    var mobileNo=Cookies.get('mobileNo');
    var name = Cookies.get('name');
    var email = Cookies.get('email');
    if(mobileNo){
        document.getElementById('user_phone').value=mobileNo;
    }
    if(name){
        document.getElementById('user_name').value=name;
    }
    if(email){
        document.getElementById('user_email').value=email;
    }

    // Set inspection time and date options
    var months = ['January','February','March','April','May','June','July','August','September','October','November','Decmeber'];
    var dayOptions = ['Select Day'];
    var timeOptions = ['Select Time'];

    function setDayOptions(){
        var today = new Date();
        var newDate=new Date();
        if(today.getHours()*60+today.getMinutes()> 17*60){
            newDate.setDate(today.getDate()+2);
        }
        else{ newDate.setDate(today.getDate()+1); }

        var i = 0;
        while(i<6){
            if(newDate.getDay()!=0 && newDate.getDay()!=6){
                dayOptions.push(newDate.getDate()+" "+months[newDate.getMonth()]);
                i++;
            }
            newDate.setDate(newDate.getDate()+1);
        }

        var select= document.getElementById('inspection_day');
        for (var i=0;i<dayOptions.length;i++){
            var dayOpt = dayOptions[i];
            var ele = document.createElement("option");
            ele.textContent= dayOpt;
            ele.value = JSON.stringify(dayOpt);
            select.appendChild(ele);
        }
    }
    function setTimeOptions(){
        var select= document.getElementById('inspection_time');
        for (var i=0;i<timeOptions.length;i++){
            var timeOpt = timeOptions[i];
            var ele = document.createElement("option");
            ele.textContent = timeOpt;
            ele.value = JSON.stringify(timeOpt);
            select.appendChild(ele);
        }
    }

//    var today = new Date();
//    var currMins = today.getHours()*60+today.getMinutes();
//    var newHour=0;
//    if(currMins < 10*60 || currMins > 17*60){
//        newHour=10;
//    }
//    else{
//        newHour=today.getHours();
//    }
//    fromTime = newHour>12 ? (newHour-12+" pm") : (newHour+" am") ;
//    newHour++;
//    toTime   = newHour>12 ? (newHour-12+" pm") : (newHour+" am") ;
//    timeOptions.push(fromTime+" - "+toTime);
    setDayOptions();
    setTimeOptions();

    $('#inspection_day').on('change',function(){
        $('#inspection_time').empty();
        timeOptions = ['Select Time'];
        var today = new Date();
        var currMins = today.getHours()*60 + today.getMinutes();
        var newHour = 0;
        if(this.value == this[1].value){
            if(currMins < 10*60 || currMins > 17*60){   newHour = 10;   }
            else{   newHour=today.getHours()+1; }
        }
        else{   newHour = 10;   }
        while(newHour<18){
            fromTime = newHour>12 ? (newHour-12+" pm") : (newHour+" am") ;
            toTime   = (newHour+1)>12 ? (newHour+1-12+" pm") : ((newHour+1)+" am") ;
            timeOptions.push(fromTime+" - "+toTime);
            newHour++;
        }
        setTimeOptions();
    });


    // Set companies to choose from for inspection
    var companyOptions = ['Select Company','Bajaj Allianz','Bharti AXA','The New India Assurance','Universal Sompo','L & T Insurance','IFFCO Tokio','HDFC Ergo'];
    function setCompanyOptions(){
        var select= document.getElementById('policy_company');
        for (var i=0;i<companyOptions.length;i++){
            var opt = companyOptions[i];
            var el = document.createElement("option");
            el.textContent=opt;
            el.value = JSON.stringify(opt);
            select.appendChild(el);
        }
    }
    setCompanyOptions();

    // Date picker for policy expiry date capture
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        endDate:'-1d'
    });

    // choose policy document sending mode
    var showElement="";
    $(document).on('click','.option-list label',function(){
        $('.option-list label').removeClass('active');
        $('.step .document-option').removeClass('reveal');
        showElement = $(this).addClass('active').find('input[type="radio"]').attr('value');
        $('.step' + ' ' + '#'+ showElement).addClass('reveal');
    });

    // choose policy claimed or not
    var policy_claim="no";
    $(document).on('click','#policy_claim label',function(){
        $('#policy_claim label').removeClass('active');
        policy_claim = $(this).addClass('active').find('input[type="radio"]').attr('value');
    });

    // to do when policy expiry date exeeds 90 days threshold
    $('#expiry_date').on('change',function(){
        var date_val = document.getElementById('expiry_date').value.split("/");
        var date= new Date();
        var date1 = new Date(date_val[2],date_val[1]-1,date_val[0]);
        var timeDiff = Math.abs(date.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if(diffDays>=90){
            $('#policy_claim').hide();
            policy_claim="expiry date above 90 days";
        }
        else{
            $('#policy_claim').show();
        }
    });

    // validation
    var validation = {
        isEmailAddress:function(str) {
            var pattern =/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            return pattern.test(str);
        },
        isNotEmpty:function (str) {
            var pattern =/\S+/;
            return pattern.test(str);
        },
        isNumber:function(str) {
            var pattern = /^\d+$/;
            return pattern.test(str);
        },
        isName:function(str) {
            var pattern = /^[a-zA-Z ]{2,30}$/;
            return pattern.test(str);
        },
    };

    var validateForm = function(){
        var valid = true;
        $('.error-text').hide();

        var user_email = document.getElementById("user_email").value,
        user_name = document.getElementById("user_name").value,
        user_phone = document.getElementById("user_phone").value,
        expiry_date = document.getElementById("expiry_date").value,
        add_policy_copy = document.getElementById("add_policy_copy").value,
        add_rc_copy = document.getElementById("add_rc_copy").value,
        company_option = document.getElementById("policy_company").value,
        inspection_day = document.getElementById("inspection_day").value,
        inspection_time = document.getElementById("inspection_time").value,
        inspection_address = document.getElementById("inspection_address").value;

        if(!(user_email && validation.isEmailAddress(user_email))){
            valid = false;
            $('#email_error').show();
        }
        if(!(user_name && validation.isName(user_name) && validation.isNotEmpty(user_name))){
            valid=false;
            $('#name_error').show();
        }
        if(!(user_phone && validation.isNumber(user_phone) && user_phone.length===10)){
            valid = false;
            $('#phone_error').show();
        }
        if(!expiry_date){
            valid = false;
            $('#exp_date_error').show();
        }
        if((showElement==='upload'|| showElement=="")){
            if(add_policy_copy=="" ){
                valid = false;
                $('#add_policy_error').show();
            }
            if(add_rc_copy=="" ){
                valid = false;
                $('#add_rc_error').show();
            }
        }
        if(company_option =='"Select Company"'){
            valid = false;
            $('#select_company_error').show();
        }
        if(inspection_day =='"Select Day"'){
            valid = false;
            $('#inspection_day_error').show();
        }
        if(inspection_time =='"Select Time"'){
            valid = false;
            $('#inspection_time_error').show();
        }
        if(!validation.isNotEmpty(inspection_address)){
            valid=false;
            $('#inspection_address_error').show();
        }
        return valid;
    };

//    $('#expired_form').submit(function(event){
//        event.preventDefault();
//        console.log(">> submitting form <<");
//        var valid = validateForm();
//        console.log("FORM: ",$('#add_policy_copy').prop('files'));
//        if(valid){
//            var jsonData ={};
//            var formData = $(this).serializeArray();
//            console.log(formData);
//            $.each(formData,function(){
//                if (jsonData[this.name]){
//                    if (!jsonData[this.name].push){
//                        jsonData[this.name] = [jsonData[this.name]];
//                    }
//                    jsonData[this.name].push(this.value || '');
//                }
//                else{
//                    jsonData[this.name] = this.value || '';
//                }
//            });
//            console.log("JSON: ",jsonData);
//            jsonData['campaign'] = "Motor";
//            jsonData['label']="lp-inspection";
//
//            $.ajax({
//                url: '/leads/save-call-time/',
//                type: 'POST',
//                data: {'data':JSON.stringify(jsonData), 'csrfmiddlewaretoken': CSRF_TOKEN},
//                success: function(res) {
//                    $('#success_message').addClass('reveal');
//                    $(this).text('Successfuly submited').attr('disabled','disabled');
//                }
//            });
//        }
//    });

    $("#file_submit_form").submit(function(){
        $(this).ajaxSubmit({
            success: function(res) {
                $('#success_message').addClass('reveal');
                $(this).text('Successfuly submited').attr('disabled','disabled');
                console.log("RESULTTTTT", res);
            }
        });
        return false;
    });
    $('#submit').click(function(){
         var valid = validateForm();
         if(valid){

            // MixPanel Events
            // mixpanel.track("Expired Car Submitted", {
            //     'Phone Number Status':'Phone Number Given',
            //     'Phone Number':document.getElementById("user_phone").value,
            // });

            Cookies.set('mobileNo', document.getElementById("user_phone").value);
             var post_data = {
                'campaign':"Motor-Offlinecases",
                'label': showElement,
                'email' : document.getElementById("user_email").value,
                'name' : document.getElementById("user_name").value,
                'mobile' : document.getElementById("user_phone").value,
                'pastPolicyExpiryDate' : document.getElementById("expiry_date").value,
                'company_option' : document.getElementById("policy_company").value,
                'inspection_day' : document.getElementById("inspection_day").value,
                'inspection_time' : document.getElementById("inspection_time").value,
                'policy_claimed' : policy_claim,
                'inspection_address' : document.getElementById("inspection_address").value,
             };
            $('#post_lead_data').val(JSON.stringify(post_data));
            $("#file_submit_form").submit();
         }
    });
});
