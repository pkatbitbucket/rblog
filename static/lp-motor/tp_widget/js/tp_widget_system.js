$(function() {
    var VEHICLE_TYPE = "fourwheeler";
    var oldCarYears = ["2014", "2013", "2012", "2011", "2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002", "2001", "2000", "1999"];

    var fuel_types = [{
            'id': 'PETROL',
            'name': 'Petrol',
            'code': 'petrol',
        }, {
            'id': 'DIESEL',
            'name': 'Diesel',
            'code': 'diesel',
        },
        // {
        //     'id': 'PETROL',
        //     'name': 'CNG/LPG Company Fitted',
        //     'code': 'cng_lpg_internal',
        // },
        // {
        //     'id': 'PETROL',
        //     'name': 'CNG/LPG Externally fitted',
        //     'code': 'cng_lpg_external',
        // }
    ];

    var parseResponse = function(encrypted){
        if(encrypted instanceof Object) return encrypted
        try{
            return JSON.parse(encrypted);
        } catch(e){
            var key = CryptoJS.enc.Hex.parse(encrypted.slice(0, 64));
            var iv = CryptoJS.enc.Hex.parse(encrypted.slice(-32));
            var cipher = CryptoJS.lib.CipherParams.create({
                ciphertext: CryptoJS.enc.Base64.parse(encrypted.slice(64, -32))
            });
            var result = CryptoJS.AES.decrypt(cipher, key, {iv: iv, mode: CryptoJS.mode.CFB});
            return JSON.parse(result.toString(CryptoJS.enc.Utf8));
        }
    };

    var selected_model_id, selected_fuel_type_id = 'PETROL',
        selected_vehicle_id;

    var setUpForm = function() {
        var policy_type_value = document.getElementById("policy_type").value;
        if (policy_type_value == 'new') {
            $('.new-policy').removeClass('hide');
            $('.renew-policy').addClass('hide');
            $('.common-fields').removeClass('hide');
            $('.expired-policy').addClass('hide');
        } else if (policy_type_value == 'renew') {
            $('.new-policy').addClass('hide');
            $('.renew-policy').removeClass('hide');
            $('.common-fields').removeClass('hide');
            $('.expired-policy').addClass('hide');
        } else if (policy_type_value == 'expired') {
            $('.expired-policy').removeClass('hide');
            $('.new-policy').addClass('hide');
            $('.renew-policy').addClass('hide');
            $('.common-fields').addClass('hide');
        }
    };

    document.getElementById("policy_type").onchange = function() {
        setUpForm();
    };

    document.getElementById("vehicle_make_model").onchange = function() {
        selected_model_id = JSON.parse(this.value).id;
        getVariants();
    }

    document.getElementById("vehicle_fuel_type").onchange = function() {
        selected_fuel_type_id = JSON.parse(this.value).id;
        getVariants();
    }

    function getVehicles() {
        $.post('/motor/' + VEHICLE_TYPE + '/api/vehicles/', {
            'csrfmiddlewaretoken': CSRF_TOKEN
        }, function(response) {
            // $("#makes-loading").hide();
            var response = parseResponse(response);
            var select = document.getElementById("vehicle_make_model");
            for (var i = 0; i < response.data.models.length; i++) {
                var opt = response.data.models[i];
                var el = document.createElement("option");
                el.textContent = opt.name;
                el.value = JSON.stringify(opt);
                select.appendChild(el);
            }
        });
    }

    function getVariants() {
        // $("#variants-loading").show();
        var select = document.getElementById("vehicle_variant");
        $(select).find('option').remove();

        var default_el = document.createElement("option");
        default_el.textContent = "Variant";
        default_el.value = "";

        select.appendChild(default_el);

        if (selected_model_id && selected_fuel_type_id) {
            $.post('/motor/' + VEHICLE_TYPE + '/api/vehicles/' + selected_model_id + '/variants/', {
                'csrfmiddlewaretoken': CSRF_TOKEN,
                'fuel_type': selected_fuel_type_id
            }, function(response) {
                // $("#variants-loading").hide();
                var response = parseResponse(response);
                console.log(response);
                for (var i = 0; i < response.data.variants.length; i++) {
                    var opt = response.data.variants[i];
                    var el = document.createElement("option");
                    el.textContent = opt.name;
                    el.value = JSON.stringify(opt);
                    select.appendChild(el);
                }
            });
        }
    }

    function getRTOs() {
        $.post('/motor/' + VEHICLE_TYPE + '/api/rtos/', {
            'csrfmiddlewaretoken': CSRF_TOKEN
        }, function(response) {
            var response = parseResponse(response).data.rtos;
            var select = document.getElementById("vehicle_rto");
            for (var i = 0; i < response.length; i++) {
                var opt = response[i];
                var el = document.createElement("option");
                el.textContent = opt.name;
                el.value = JSON.stringify(opt);
                select.appendChild(el);
            }
        });
    };

    function setFuelTypes() {
        var select = document.getElementById("vehicle_fuel_type");
        for (var i = 0; i < fuel_types.length; i++) {
            var opt = fuel_types[i];
            var el = document.createElement("option");
            el.textContent = opt.name;
            el.value = JSON.stringify(opt);
            select.appendChild(el);
        }
    }

    function setRegYears() {
        var select = document.getElementById("vehicle_reg_year");
        for (var i = 0; i < oldCarYears.length; i++) {
            var opt = oldCarYears[i];
            var el = document.createElement("option");
            el.textContent = opt;
            el.value = opt;
            select.appendChild(el);
        }
    }

    setUpForm();
    getVehicles();
    setFuelTypes();
    getRTOs();
    setRegYears();

    // Validations

    var validation = {
        isEmailAddress: function(str) {
            var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            return pattern.test(str);
        },
        isNotEmpty: function(str) {
            var pattern = /\S+/;
            return pattern.test(str);
        },
        isNumber: function(str) {
            var pattern = /^\d+$/;
            return pattern.test(str);
        },
        isName: function(str) {
            var pattern = /^[a-zA-Z ]{2,30}$/;
            return pattern.test(str);
        }
    };

    var validateForm = function() {
        console.log('Validate form.');

        var valid = true;
        $('.error-text').hide();

        var policy_type = document.getElementById("policy_type").value,
            vehicle_make_model = document.getElementById("vehicle_make_model").value,
            vehicle_fuel_type = document.getElementById("vehicle_fuel_type").value,
            vehicle_variant = document.getElementById("vehicle_variant").value,
            vehicle_rto = document.getElementById("vehicle_rto").value,
            reg_year = document.getElementById("vehicle_reg_year").value,
            whatsapp_checked = $("#whatsapp-checkbox").attr("checked"),
            user_phone = document.getElementById("user_phone").value;


        if (policy_type != "expired") {
            if (!vehicle_make_model || !vehicle_fuel_type || !vehicle_variant) {
                valid = false;
                $('#vehicle_detail_error').show();
            }

            if (!vehicle_rto) {
                valid = false;
                $('#rto_error').show();
            }

            if ((policy_type == "renew") && (!reg_year)) {
                valid = false;
                $('#reg_date_error').show();
            }
        }




        if (whatsapp_checked) {
            if (validation.isNotEmpty(user_phone) && validation.isNumber(user_phone) && user_phone.length == 10) {

            } else {
                valid = false;
                $('#phone_error').show();
            }
        }
        return valid;
    };

    var submitForm = function() {
        var policy_type = document.getElementById("policy_type").value,
            vehicle_make_model = document.getElementById("vehicle_make_model").value,
            vehicle_fuel_type = document.getElementById("vehicle_fuel_type").value,
            vehicle_variant = document.getElementById("vehicle_variant").value,
            vehicle_rto = document.getElementById("vehicle_rto").value,
            reg_year = document.getElementById("vehicle_reg_year").value,
            expiry_slot = document.getElementById("expiry_slot").value,
            user_phone = document.getElementById("user_phone").value,
            redirect_url = '/motor/car-insurance/#results';

        var data = {
            'isNewVehicle': (policy_type == "new") ? '1' : '0',
            'vehicle': vehicle_make_model,
            'vehicleVariant': vehicle_variant,
            'fuel_type': vehicle_fuel_type,
            'rto_info': vehicle_rto,
            'mobileNo': user_phone,
        };

        if (policy_type == "renew") {
            data['reg_year'] = reg_year;
            data['expirySlot'] = expiry_slot;
        }

        if (policy_type == "expired") {
            redirect_url = '/lp/car-insurance/renew-expired-policy/';
        }

        $.post("/motor/submit-landing-page/", data, function(response) {
            var response = JSON.parse(response);

            // Section to store data to localStorage
            if (response.success) {
                if (localStorage) {
                    if(policy_type == "expired"){
                        response.data["policyExpired"] =true;
                    }
                    localStorage.setItem(VEHICLE_TYPE, JSON.stringify(response.data));
                } else {
                    for (var item in response.data) {
                        var value = response.data[item];
                        if (value != null && typeof(value) == "object") {
                            value = JSON.stringify(value);
                        }
                        Cookies.set(VEHICLE_TYPE + item, value);
                    }
                }
                var cnum = $('#user_phone').val();
                if (cnum) {
                    var cmodel = {
                        'mobile': cnum,
                        'campaign': CAMPAIGN,
                        'label': EVENT_LABEL,
                        'policy_type': policy_type,
                        'triggered_page': window.parent.location.href
                    };
                    Cookies.set('mobileNo', cnum);
                    $('#submit').addClass('disabled').text('Just a moment...');
                    $.post("/leads/save-call-time/", {
                        'data': JSON.stringify(cmodel),
                        'csrfmiddlewaretoken': CSRF_TOKEN
                    }, function(data) {
                        if (data.success) {
                            Cookies.set('lms_campaign', 'MOTOR');
                            if (window.dataLayer){
                                window.dataLayer.push({
                                    'event': 'GAEvent',
                                    'hitType': 'event',
                                    'eventCategory': 'MOTOR',
                                    'eventAction': 'CallScheduled',
                                    'eventLabel': 'tp_car_form',
                                    'eventValue': 4,
                                    'eventCallback': function(){
                                        sendMessage({url: redirect_url});
                                    }
                                });
                            }else{
                                sendMessage({url: redirect_url});
                            }

                        } else {
                            console.log('POST ERROR', data);
                            sendMessage({url: redirect_url});
                        }
                    }, 'json');
                } else {
                    sendMessage({url: redirect_url});
                }
            }
        });
    };

    $(document).on('click', '#submit', function() {
        event.preventDefault();
        var isFormValid = validateForm();
        if (isFormValid) {
            document.getElementById('wait-msg').classList.add('active');
            submitForm();

        }
    });

});
function sendMessage(msg) {
    msg['id'] = IFRAME_ID;
    msg = JSON.stringify(msg);
    window.parent.postMessage(msg, '*');
}
document.body.style.overflowY = "hidden";
setInterval( function() { sendMessage({height:document.body.clientHeight+'px'});}, 200);
