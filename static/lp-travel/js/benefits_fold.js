if (typeof CSRF_TOKEN != 'undefined'){
    var CSRF_TOKEN;
}
var next_page_url = '/travel-insurance/';
if(!$.isEmptyObject(next_page_override)){
    next_page_url = next_page_override;
}
$(function() {
    if ($.isEmptyObject(CSRF_TOKEN)){
        $.get("/ajax/get-token/", function(data){
            CSRF_TOKEN = data.csrf;
        }, 'json');
    }
    $('body').on('click', '.call_me_btn_expert', function(){
        var context = $(this);
        $('.contact-me-error-expert').hide().text('');
        var cnum = $('#contact-me-num-expert').val();
        var res = null;
        if(cnum && cnum.length == 10){
            res = cnum.match(/[7-9]{1}\d{9}/);
            if(res){
                var cmodel =  {
                    'mobile' : cnum,
                    'campaign' : 'Travel',
                    'label': 'on_scroll_lowest_quotes'
                };
                Cookies.set('mobileNo', cnum);
                context.addClass('disabled').text('Just a moment...');
                $.post("/leads/save-call-time/", {'data' : JSON.stringify(cmodel), 'csrfmiddlewaretoken' : CSRF_TOKEN}, function(data){
                    if (data.success){
                        if(window.dataLayer){
                            window.dataLayer.push({'gtm_mobile': cnum});
                            window.dataLayer.push({
                                'event': 'GAEvent',
                                'hitType': 'event',
                                'eventCategory': 'TRAVEL',
                                'eventAction': 'CallScheduled',
                                'eventLabel': 'on_scroll_lowest_quotes',
                                'eventValue': 4,
                                'eventCallback': function(){
                                    window.location = '/travel-insurance/?callback=true';
                                }
                            });
                        }else{
                            window.location = '/travel-insurance/';
                        }
                    }
                    else{
                        console.log('POST ERROR', data);
                        window.location = '/travel-insurance/';
                    }
                }, 'json');
            }
        }
        if($.isEmptyObject(res)){
            context.removeClass('disabled').text('Get Lowest Quotes');
            $('.contact-me-error-expert').show().text('Please enter a valid 10 digit phone number');
        }
    });
}); //main function ends
