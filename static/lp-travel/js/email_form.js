function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

$(function() {

    var next_page_url = '/motor/car-insurance/';
    if(!$.isEmptyObject(next_page_override)){
        next_page_url = next_page_override;
    }
    $('.contact-me-error').hide();

    $(document).on('click', '.hero-form li', function(){
        var formelements = $('.hero-form li');
        formelements.removeClass('active');
        var context = $(this);
        context.addClass('active');
        policy_type = context.data("type");
        if(policy_type=="old"){
            Cookies.set('fourwheeler_policyExpired', false);
            next_page_url = '/motor/car-insurance/';
        }
        if(policy_type=="new"){
            Cookies.set('fourwheeler_policyExpired', false);
            Cookies.set('fourwheeler_isNewVehicle', true);
            next_page_url = '/motor/car-insurance/';
        }
        if(policy_type=="expired"){
            Cookies.set('fourwheeler_policyExpired', true);
            Cookies.set('fourwheeler_isNewVehicle', false);
            next_page_url = '/lp/car-insurance/renew-expired-policy/';
        }
    });
    $(document).on('click', '#show_quote_btn', function(){
        window.dataLayer.push({
            'event': 'GAEvent',
            'hitType': 'event',
            'eventCategory': 'MOTOR',
            'eventAction': 'policy_type_' + policy_type,
            'eventLabel': page_id,
            'eventValue': 0
        });
        window.dataLayer.push({
            'event': 'GAEvent',
            'hitType': 'event',
            'eventCategory': 'MOTOR',
            'eventAction': 'hero_form_submit',
            'eventLabel': page_id,
            'eventValue': 0
        });

        var context = $(this);
        $('.contact-me-error').hide();
        var cemail = $('#contact-me-email').val();
        if(cemail) {
            if (validateEmail(cemail)) {
                var cmodel = {
                    'email': cemail,
                    'campaign': 'Motor',
                    'label': page_id,
                    'policy_type': policy_type,
                    'triggered_page': window.location.href
                };
                Cookies.set('email', cemail);
                context.addClass('disabled').text('Just a moment...');
                $.post("/leads/save-email/", {
                    'data': JSON.stringify(cmodel),
                    'csrfmiddlewaretoken': csrf_token
                }, function (data) {
                    if (data.success) {
                        window.location = next_page_url + '?get_quotes=true';
                    }
                    else {
                        console.log('POST ERROR', data);
                        window.location = next_page_url;
                    }
                }, 'json');
            } else {
                $('.contact-me-error').show();
            }
        }else{
            window.location = next_page_url;
        }
    })
});
