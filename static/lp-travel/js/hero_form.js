if (typeof CSRF_TOKEN == 'undefined'){
    var CSRF_TOKEN;
}

$(function() {
    if ($.isEmptyObject(CSRF_TOKEN)){
        $.get("/ajax/get-token/", function(data){
            CSRF_TOKEN = data.csrf;
        }, 'json');
    }

    var next_page_url = '/travel-insurance/';
    if(!$.isEmptyObject(next_page_override)){
        next_page_url = next_page_override;
    }
    $('.contact-me-error').hide();

    $(document).on('click', '#show_quote_btn', function(){

        var context = $(this);
        $('.contact-me-error').hide();
        var cnum = $('#contact-me-num').val();
        var res = null;
        if(cnum){
            if (cnum.length == 10){
                res = cnum.match(/[7-9]{1}\d{9}/);
                if(res){
                    var cmodel =  {
                        'mobile' : cnum,
                        'campaign': 'Travel',
                        'label': page_id,
                        'triggered_page': window.location.href
                    };
                    Cookies.set('mobileNo', cnum);
                    context.addClass('disabled').text('Just a moment...');
                    $.post("/leads/save-call-time/", {'data' : JSON.stringify(cmodel), 'csrfmiddlewaretoken' : CSRF_TOKEN}, function(data){
                        if (data.success){
                            if (window.dataLayer){
                                window.dataLayer.push({'gtm_mobile': cnum});
                                window.dataLayer.push({
                                    'event': 'GAEvent',
                                    'hitType': 'event',
                                    'eventCategory': 'TRAVEL',
                                    'eventAction': 'CallScheduled',
                                    'eventLabel': page_id,
                                    'eventValue': 4,
                                     'eventCallback': function(){
                                         window.location = next_page_url + '?callback=true';
                                     }
                                });
                            }else{
                                window.location = next_page_url;
                            }
                        }
                        else{
                            console.log('POST ERROR', data);
                            window.location = next_page_url;
                        }
                    }, 'json');
                }
                else{
                    $('.contact-me-error').show();
                }
            }
            else{
                $('.contact-me-error').show();
            }
        }else{
            if (hero_phone_mandatory == 'YES'){
                $('.contact-me-error').show();
            }else{
                window.location = next_page_url;
            }
        }
    })
});
