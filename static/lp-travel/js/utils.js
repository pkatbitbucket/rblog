var next_page_url = '/travel-insurance/';
if(!$.isEmptyObject(next_page_override)){
    next_page_url = next_page_override;
}

function ClickToCall(label){
    window.dataLayer.push({
        'event': 'GAEvent',
        'hitType': 'event',
        'eventCategory': 'TRAVEL',
        'eventAction': 'click_to_call',
        'eventLabel': page_id + ' Label_' + label,
        'eventValue': 0
    });
    window.location = 'tel:' + toll_free_no;
    return false;
}

function skipForm(label){
    window.dataLayer.push({
        'event': 'GAEvent',
        'hitType': 'event',
        'eventCategory': 'TRAVEL',
        'eventAction': 'hero_skip_'+ label,
        'eventLabel': page_id,
        'eventValue': 0
    });
    window.location = next_page_url;
    return false;
}

