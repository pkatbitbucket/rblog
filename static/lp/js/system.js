
function cfhasClass(ele,cls) {
  return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}

function cfaddClass(ele,cls) {
  if (!cfhasClass(ele,cls)) ele.className += " "+cls;
}

function cfremoveClass(ele,cls) {
  if (cfhasClass(ele,cls)) {
    var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
    ele.className=ele.className.replace(reg,' ');
  }
}