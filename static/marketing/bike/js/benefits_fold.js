if (typeof CSRF_TOKEN != 'undefined'){
    var CSRF_TOKEN;
}
var next_page_url = '/motor/twowheeler-insurance/';
if (typeof next_page_override != 'undefined'){
    if(!$.isEmptyObject(next_page_override)){
        next_page_url = next_page_override;
    }
}
var mobileNo = Cookies.get('mobileNo');
var num_obj = $('#contact-me-num-expert');
function set_num(v) {
    if(!num_obj.val()) num_obj.val(v);
}
if(mobileNo) {
    set_num(mobileNo);
}
$(function() {
    if ($.isEmptyObject(CSRF_TOKEN)){
        $.get("/ajax/get-token/", function(data){
            CSRF_TOKEN = data.csrf;
        }, 'json');
    }
    $('body').on('click', '.call_me_btn_expert', function(){
        var context = $(this);
        $('.contact-me-error-expert').hide().text('');
        var cnum = $('#contact-me-num-expert').val();
        var res = null;
        if(cnum && cnum.length == 10){
            res = cnum.match(/[7-9]{1}\d{9}/);
            if(res){
                var cmodel =  {
                    'mobile' : cnum,
                    'campaign' : 'BIKE',
                    'label': 'on_scroll_lowest_quotes'
                };
                Cookies.set('mobileNo', cnum);
                context.addClass('disabled').text('Just a moment...');
                $.post("/leads/save-call-time/", {'data' : JSON.stringify(cmodel), 'csrfmiddlewaretoken' : CSRF_TOKEN}, function(data){
                    if (data.success){
                        if(window.dataLayer){
                            window.dataLayer.push({'gtm_mobile': cnum});
                            window.dataLayer.push({
                                'event': 'GAEvent',
                                'hitType': 'event',
                                'eventCategory': 'BIKE',
                                'eventAction': 'CallScheduled',
                                'eventLabel': 'on_scroll_lowest_quotes',
                                'eventValue': 4,
                                'eventCallback': function(){
                                    window.location = '/motor/car-insurance/?callback=true';
                                }
                            });
                        }else{
                            window.location = '/motor/car-insurance/';
                        }
                    }
                    else{
                        console.log('POST ERROR', data);
                        window.location = '/motor/car-insurance/';
                    }
                }, 'json');
            }
        }
        if($.isEmptyObject(res)){
            context.removeClass('disabled').text('Get Lowest Quotes');
            $('.contact-me-error-expert').show().text('Please enter a valid 10 digit phone number');
        }
    });
}); //main function ends
