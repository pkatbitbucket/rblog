if (typeof CSRF_TOKEN == 'undefined'){
    var CSRF_TOKEN;
}
$(function() {
    if ($.isEmptyObject(CSRF_TOKEN)){
        $.get("/ajax/get-token/", function(data){
            CSRF_TOKEN = data.csrf;
        }, 'json');
    }

    var next_page_url = '/motor/twowheeler-insurance/';
    if (typeof next_page_override != 'undefined'){
        if(!$.isEmptyObject(next_page_override)){
            next_page_url = next_page_override;
        }
    }
    $('.contact-me-error').hide();

    $(document).on('click', '.hero-form li', function(){
        var formelements = $('.hero-form li');
        formelements.removeClass('active');
        var context = $(this);
        context.addClass('active');
        policy_type = context.data("type");
        if(policy_type=="old"){
            Cookies.set('twowheeler_policyExpired', false);
            next_page_url = '/motor/twowheeler-insurance/';
        }
        if(policy_type=="new"){
            Cookies.set('twowheeler_policyExpired', false);
            Cookies.set('twowheeler_isNewVehicle', true);
            next_page_url = '/motor/twowheeler-insurance/';
        }
        if(policy_type=="expired"){
            Cookies.set('twowheeler_policyExpired', true);
            Cookies.set('twowheeler_isNewVehicle', false);
            next_page_url = '/motor/twowheeler-insurance/';
        }
    });
    $(document).on('click', '#show_quote_btn', function(){
        window.dataLayer.push({
            'event': 'GAEvent',
            'hitType': 'event',
            'eventCategory': 'BIKE',
            'eventAction': 'policy_type_' + policy_type,
            'eventLabel': page_id,
            'eventValue': 0
        });
        window.dataLayer.push({
            'event': 'GAEvent',
            'hitType': 'event',
            'eventCategory': 'BIKE',
            'eventAction': 'hero_form_submit',
            'eventLabel': page_id,
            'eventValue': 0
        });

        var context = $(this);
        $('.contact-me-error').hide();
        var cnum = $('#contact-me-num').val();
        var res = null;
        if(cnum){
            if (cnum.length == 10){
                res = cnum.match(/[7-9]{1}\d{9}/);
                if(res){
                    var cmodel =  {
                        'mobile' : cnum,
                        'campaign': 'Motor',
                        'label': page_id,
                        'policy_type': policy_type,
                        'triggered_page': window.location.href
                    };
                    Cookies.set('mobileNo', cnum);
                    context.addClass('disabled').text('Just a moment...');
                    $.post("/leads/save-call-time/", {'data' : JSON.stringify(cmodel), 'csrfmiddlewaretoken' : CSRF_TOKEN}, function(data){
                        if (data.success){
                            if (window.dataLayer){
                                window.dataLayer.push({'gtm_mobile': cnum});
                                window.dataLayer.push({
                                    'event': 'GAEvent',
                                    'hitType': 'event',
                                    'eventCategory': 'BIKE',
                                    'eventAction': 'CallScheduled',
                                    'eventLabel': page_id,
                                    'eventValue': 4,
                                    'eventCallback': function(){
                                         window.location = next_page_url + '?callback=true';
                                     }
                                });
                            }else{
                                window.location = next_page_url;
                            }
                        }
                        else{
                            console.log('POST ERROR', data);
                            window.location = next_page_url;
                        }
                    }, 'json');
                }
                else{
                    $('.contact-me-error').show();
                }
            }
            else{
                $('.contact-me-error').show();
            }
        }else{
            if (hero_phone_mandatory == 'YES'){
                $('.contact-me-error').show();
            }else{
                window.location = next_page_url;
            }
        }
    })
});
