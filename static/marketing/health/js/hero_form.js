if (typeof CSRF_TOKEN == 'undefined'){
    var CSRF_TOKEN;
}
var next_page_url = '/health-plan/';
if (typeof next_page_override != 'undefined'){
    if(!$.isEmptyObject(next_page_override)){
        next_page_url = next_page_override;
    }
}
$(function() {
    if ($.isEmptyObject(CSRF_TOKEN)){
        $.get("/ajax/get-token/", function(data){
            CSRF_TOKEN = data.csrf;
        }, 'json');
    }
    $('body').on('click', '#call-me-btn', function(){

        var context = $(this);
        $('#hero-error').addClass('hide');
        $('#hero-loading').removeClass('hide');
        var cnum = $('#contact-me-num').val();
        var cnum = cnum.trim();
        var res = null;
        if(cnum && cnum.length == 10){
            res = cnum.match(/[7-9]{1}\d{9}/);
            if(res){
                var cmodel =  {
                    'mobile' : cnum,
                    'campaign': 'Health',
                    'label': page_id
                };
                if(page_id == 'lp_health_expert_kavita' || page_id == 'lp_health_protect_cancer'){
                    cmodel['lms_campaign'] = 'health-advisorcallback'; 
                }
                else if(page_id == 'lp_health_tax_saving'){
                    cmodel['lms_campaign'] = 'health-taxsavings';
                }
                Cookies.set('mobileNo', cnum);
                Cookies.set('show_custom_alert', 'True', {expires: 120});
                context.addClass('disabled').text('Just a moment...');
                $.post("/leads/save-call-time/", {'data' : JSON.stringify(cmodel),  'csrfmiddlewaretoken' : CSRF_TOKEN}, function(data){
                    if (data.success){
                        window.dataLayer.push({'gtm_mobile': cnum});
                        window.dataLayer.push({
                            'event': 'GAEvent',
                            'hitType': 'event',
                            'eventCategory': 'HEALTH',
                            'eventAction': 'CallScheduled',
                            'eventLabel': page_id,
                            'eventValue': 4,
                            'eventCallback': function(){
                               if(page_id == 'lp_health_protect_cancer') {
                                    $('#ph-form').addClass('hide');
                                    $('.terms-msg').addClass('hide');
                                    $('#ph-success').removeClass('hide');
                               }
                               else {
                                window.location = next_page_url + '?callback=true';
                               }
                           }
                       });
                    }
                    else{
                        $('#hero-error').removeClass('hide');
                        console.log('POST ERROR', data);
                        //window.location = next_page_url;
                        if(page_id == 'lp_health_protect_cancer') {
                                    $('#ph-form').addClass('hide');
                                    $('.terms-msg').addClass('hide');
                                    $('#ph-success').removeClass('hide');
                       }
                       else {
                        window.location = next_page_url;
                       }
                    }
                }, 'json');
}
}
if($.isEmptyObject(res)){
    $('#hero-error').removeClass('hide');
    $('#hero-loading').addClass('hide');
}
});
var mobileNo = Cookies.get('mobileNo');
var num_obj = $('#contact-me-num');
function set_num(v) {
    if(!num_obj.val()) num_obj.val(v);
}
if(mobileNo) {
    set_num(mobileNo);
}
});
