(function() {
    new CFSlider({
        carousel: '.carousel',
        delay: 7,
        autoplay: true
    });
})();

// represent a single member item
var Member = function (id, person, verbose, selected) {
    var self = this;
    self.id = id;
    self.person = person;
    self.verbose = verbose;
    self.display = ko.observable(verbose);
    self.selected = ko.observable(selected);
};

var MemberModel = function () {
    var self = this;
    self.gender = ko.observable("");//Gender of 'you'
    self.members = ko.observableArray([]);
    self.show_error = ko.observable(false);

    var kid_id = 1;
    self.add = function (person, verbose, selected) {
        if(person.toLowerCase() == 'son' || person.toLowerCase() == "daughter"){
            id = person.toLowerCase() + kid_id;
            kid_id += 1;
        } else {
            id = person.toLowerCase();
        }
        var m = new Member( id, person, verbose, selected);
        self.members.push(m);
        return m;
    };

    self.select_gender = function(ptype){
        self.gender(ptype);
        if(ptype == "MALE"){
            self.spouse.display("Wife");
        }
        if(ptype == "FEMALE"){
            self.spouse.display("Husband");
        }
    };

    self.selected_members = function(){
        return self.members().filter(function(ele){ return ele.selected()});
    };

    self.number_of_adults = function(){
        var num = 0;
        ko.utils.arrayForEach(self.selected_members(), function(i, elem){
            if(elem.person.toLowerCase() == 'father' || elem.person.toLowerCase() == "mother" ||
                elem.person.toLowerCase() == 'spouse' || elem.person.toLowerCase() == "self"){
                num = num + 1;
            }
        });
        return num;
    };

    self.serialized_members = function(){
        var smembers = [];
        ko.utils.arrayForEach(self.members(), function(m){
            if(m.selected()){
                smembers.push({
                    id : m.id,
                    person : m.person,
                    verbose : m.verbose,
                    selected: m.selected()
                });
            }
        });
        return smembers;
    };

    self.remove = function (member) {
        self.members.remove(member);
    };

    var check_errors = function(){
        if(self.selected_members().length == 0){
            self.show_error(true);
        } else {
            self.show_error(false);
        }
    };

    self.deselect = function (member) {
        member.selected(false);
        if(member.person.toLowerCase() == 'son' || member.person.toLowerCase() == "daughter"){
            self.remove(member);
        }
        check_errors();
    };

    self.select = function(member) {
        if(member.selected()){
            return true;
        }
        member.selected(true);
        check_errors();
    };

    self.getKids = ko.computed(function() {
        var kids = [];
        ko.utils.arrayForEach(self.members(), function(member){
            if(member.selected()){
                if(member.person.toLowerCase() == 'son' || member.person.toLowerCase() == "daughter"){
                    kids.push(member);
                }
            }
        });
        return kids;
    });

    self.you = self.add('You', "You", true);
    self.spouse = self.add('Spouse', "Spouse", false);
    self.father = self.add('Father', "Father", false);
    self.mother = self.add('Mother', "Mother", false);

    self.initialize = function() {
        self.gender('MALE');
        check_errors();
    };
    self.continue = function() {
        check_errors();
        Cookies.set('members', JSON.stringify(MemberModel.serialized_members()));
        Cookies.set('gender', MemberModel.gender());
        if(!self.show_error()) {
            window.location = '/health-plan/';
        }
    }
};

var MemberModel = new MemberModel();
MemberModel.initialize();
ko.applyBindings(MemberModel, document.getElementById('mview'));

var overlayTrigger = document.getElementById('irda-auth');
var overlayClose = document.getElementById('overlay-close');
var irdaOverlay = document.getElementById('overlay');

overlayTrigger.addEventListener('click', function() {
    cfaddClass(irdaOverlay, 'open');
}, false);

overlayClose.addEventListener('click', function() {
    cfremoveClass(irdaOverlay, 'open');
}, false);
