var next_page_url = '/health-plan/';
if (typeof next_page_override != 'undefined'){
    if(!$.isEmptyObject(next_page_override)){
        next_page_url = next_page_override;
    }
}
function skipForm(label){
    window.dataLayer.push({
        'event': 'GAEvent',
        'hitType': 'event',
        'eventCategory': 'HEALTH',
        'eventAction': 'hero_skip_'+ label,
        'eventLabel': page_id,
        'eventValue': 0
    });
    if(label == 'main'){
        Cookies.set('hide_login', 'True', { expires: 300 });
    }
    window.location = next_page_url;
    return false;
}

function ClickToCall(label){
    window.dataLayer.push({
        'event': 'GAEvent',
        'hitType': 'event',
        'eventCategory': 'HEALTH',
        'eventAction': 'click_to_call',
        'eventLabel': page_id + ' Label_' + label,
        'eventValue': 0
    });
    window.location = 'tel:' + toll_free_no;
    return false;
}
