function sendDataToGoogleForm(ele, form_url, gdata, hide_ele, unhide_ele, cnum, error_id){

        res = cnum.match(/[7-9]{1}\d{9}/);
        if ((cnum !== "") && (res) && cnum.length == 10) {
            $('#'+error_id).addClass('hide');
            $(ele).attr("disabled",'disabled');
            $(ele).text("Just a moment...");
            $.ajax({
                url: form_url,
                data: gdata,
                type: "POST",
                dataType: "xml",
                statusCode: {
                    0: function (){
                       Cookies.set('cnum', cnum);
                       $.each(unhide_ele,function(i,d){
                        $("#"+d).removeClass('hide');
                       });
                       $.each(hide_ele,function(i,d){
                        $("#"+d).addClass('hide');
                       });

                       if(typeof next_page_url != 'undefined')
                          window.location = next_page_url + '?callback=true';
                    },
                    200: function (){
                       Cookies.set('cnum', cnum);
                       $.each(unhide_ele,function(i,d){
                        $("#"+d).removeClass('hide');
                       });
                       $.each(hide_ele,function(i,d){
                        $("#"+d).addClass('hide');
                       });
                       if(typeof next_page_url != 'undefined'){
                          window.location = next_page_url + '?callback=true';
                       }else{
                          return true;
                       }
                    }
                }
            });
        }
        else {
            $('#'+error_id).removeClass('hide');
        }
    }