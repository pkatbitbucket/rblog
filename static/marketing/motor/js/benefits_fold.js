if (typeof CSRF_TOKEN != 'undefined'){
    var CSRF_TOKEN;
}
var next_page_url = '/motor/car-insurance/';
if (typeof next_page_override != 'undefined'){
    if(!$.isEmptyObject(next_page_override)){
        next_page_url = next_page_override;
    }
}
var mobileNo = Cookies.get('mobileNo');
var num_obj = $('#contact-me-num-expert');
function set_num(v) {
    if(!num_obj.val()) num_obj.val(v);
}
if(mobileNo) {
    set_num(mobileNo);
}
$(function() {
    if ($.isEmptyObject(CSRF_TOKEN)){
        $.get("/ajax/get-token/", function(data){
            CSRF_TOKEN = data.csrf;
        }, 'json');
    }
    if (otp) {
        $('.otp-delivery-error').hide();
        $('.otp-error').hide();
    }

    function send_otp () {
        $.post("/leads/send-otp/", {'csrfmiddlewaretoken' : CSRF_TOKEN}, function (data) {
            if (data.success) {
                $('#otp_popup').removeClass('hide').addClass('show');
            } else {
                $('.otp-delivery-error').show();
                $('#show_quote_btn').first().text(submit_btn_text);
            }
        })
    }

    function save_lead (cmodel, redirect, ga_event) {
        redirect = typeof redirect !== 'undefined' ? redirect : true;
        ga_event = typeof ga_event !== 'undefined' ? ga_event : true;
        $.post("/leads/save-call-time/",
            {'data' : JSON.stringify(cmodel), 'csrfmiddlewaretoken' : CSRF_TOKEN},
            function (data) {
                if (data.success){
                    if (ga_event && window.dataLayer){
                        window.dataLayer.push({'gtm_mobile': cmodel.mobile});
                        window.dataLayer.push({
                            'event': 'GAEvent',
                            'hitType': 'event',
                            'eventCategory': 'MOTOR',
                            'eventAction': 'CallScheduled',
                            'eventLabel': 'on_scroll_lowest_quotes',
                            'eventValue': 4,
                            'eventCallback': function () {
                                if (redirect) {
                                    window.location = '/motor/car-insurance/?callback=true';
                                }
                            }
                        });
                    } else if (redirect) {
                        window.location = '/motor/car-insurance/';
                    }
                } else {
                    console.log('POST ERROR', data);
                    window.location = '/motor/car-insurance/';
                }
            },
            'json'
        );
    }

    $('body').on('click', '.call_me_btn_expert', function(){

        if (otp) {
            $('.otp-delivery-error').hide();
        }

        var pathname = window.location.pathname;
        var queryStrings = pathname.split("/");

        var context = $(this);
        $('.contact-me-error-expert').hide().text('');
        var cnum = $('#contact-me-num-expert').val();
        var res = null;
        if(cnum && cnum.length == 10){
            res = cnum.match(/[7-9]{1}\d{9}/);
            if(res){
                var cmodel =  {
                    'mobile' : cnum,
                    'campaign' : 'MOTOR',
                    'label': 'on_scroll_lowest_quotes'
                };
                Cookies.set('mobileNo', cnum);
                Cookies.set('mobileNoOn', 'LP-'+queryStrings[queryStrings.length-2]+'=Know-Benefits-Form');
                Cookies.set('mobileNoURL', window.location.href);
                Cookies.set('label', 'on_scroll_lowest_quotes');

                context.addClass('disabled').text('Just a moment...');

                if (otp) {
                    send_otp();
                    cmodel.otp_status = 'not verified';
                }

                // mixpanel.track("Car LP Submitted", {
                //     'Phone Number Status':'Phone Number Given',
                //     'Phone Number':cnum,
                //     'Form Placement':'lp_on_scroll_lowest_quotes',
                //     'Network':network,
                //     'Category':category
                // });

                var redirect = true;
                if (otp) {
                    redirect = false
                }
                save_lead(cmodel, redirect, true);

            }
        }
        if($.isEmptyObject(res)){
            context.removeClass('disabled').text('Get Lowest Quotes');
            $('.contact-me-error-expert').show().text('Please enter a valid 10 digit phone number');
        }
    });
}); //main function ends
