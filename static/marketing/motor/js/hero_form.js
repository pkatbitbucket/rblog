if (typeof CSRF_TOKEN == 'undefined'){
    var CSRF_TOKEN;
}
if(typeof SITE_URL === 'undefined'){
    var SITE_URL = '';
}

if (typeof otp == 'undefined'){
    var otp = false;
}

if (typeof tracker_id == 'undefined'){
    var tracker_id = Cookies.get('sessionid');
}

var expiredLandingPageIDs = [
    'lp_expired_car_insurance',
    'lp_expired_car_insurance_mobile'];


var next_page_url = '/motor/car-insurance/';
if (typeof next_page_override != 'undefined'){
    if(!$.isEmptyObject(next_page_override)){
        next_page_url = next_page_override;
    }
}

var submit_btn_text = $('#show_quote_btn').first().text();

function setParameter(key, value) {
    if (localStorage) {
        var store = localStorage.getItem('fourwheeler');
        try {
            store = JSON.parse(store);
        } catch (e) {
            store = {}
        }
        if (!store) store = {};
        store[key] = value;
        localStorage.setItem('fourwheeler', JSON.stringify(store));
    } else {
        Cookies.set('fourwheeler_' + key, value);
    }
}

function initialPolicyType(policy_type){
    var formelements = $('.hero-form li');
    formelements.removeClass('active');
    if(policy_type=="old"){
        ele = $(formelements[0]);
        ele.addClass('active');
        setParameter('policyExpired', 0);
        setParameter('isNewVehicle', 0);
        next_page_url = '/motor/car-insurance/';
    }
    if(policy_type=="new"){
        setParameter('policyExpired', 0);
        setParameter('isNewVehicle', 1);
        ele = $(formelements[1]);
        ele.addClass('active');
        next_page_url = '/motor/car-insurance/';
    }
    if(policy_type=="expired"){
        setParameter('policyExpired', 1);
        setParameter('isNewVehicle', 0);
        ele = $(formelements[2]);
        ele.addClass('active');
        next_page_url = '/motor/car-insurance/';
    }
}

initialPolicyType('old');
if (typeof policy_type !== 'undefined'){
    if (!$.isEmptyObject(policy_type)){
        initialPolicyType(policy_type);
    }
}

$('.contact-me-error').hide();

$(document).on('click', '.hero-form li', function(){
    var formelements = $('.hero-form li');
    formelements.removeClass('active');
    var context = $(this);
    context.addClass('active');
    policy_type = context.data("type");
    if(policy_type=="old"){
        setParameter('policyExpired', 0);
        next_page_url = '/motor/car-insurance/';
    }
    if(policy_type=="new"){
        setParameter('policyExpired', 0);
        setParameter('isNewVehicle', 1);
        next_page_url = '/motor/car-insurance/';
    }
    if(policy_type=="expired"){
        setParameter('policyExpired', 1);
        setParameter('isNewVehicle', 0);
        next_page_url = '/motor/car-insurance/';
    }
});

var mobileNo = Cookies.get('mobileNo');
var num_obj = $('#contact-me-num');

function set_num(v) {
    if(!num_obj.val()) num_obj.val(v);
}

if(mobileNo) {
    set_num(mobileNo);
}

function ga_lead_submit () {
    window.dataLayer.push({
        'event': 'GAEvent',
        'hitType': 'event',
        'eventCategory': 'MOTOR',
        'eventAction': 'policy_type_' + policy_type,
        'eventLabel': page_id,
        'eventValue': 0
    });
    window.dataLayer.push({
        'event': 'GAEvent',
        'hitType': 'event',
        'eventCategory': 'MOTOR',
        'eventAction': 'hero_form_submit',
        'eventLabel': page_id,
        'eventValue': 0
    });
}

function save_lead (otp, verified) {
    redirect = true;
    if(otp){
        cmodel['otp_status'] = verified? 'verified': 'not verified';
        redirect = verified;
    }

    $.post("/leads/save-call-time/",
        {'data' : JSON.stringify(cmodel), 'csrfmiddlewaretoken' : CSRF_TOKEN},
        function (data) {
            if (data.success){
                if (window.dataLayer){
                    window.dataLayer.push({'gtm_mobile': cmodel.mobile});
                    window.dataLayer.push({
                        'event': 'GAEvent',
                        'hitType': 'event',
                        'eventCategory': 'MOTOR',
                        'eventAction': 'CallScheduled',
                        'eventLabel': Cookies.get('label') || page_id,
                        'eventValue': 4,
                        'eventCallback': function () {
                            if (redirect) {
                                window.location = next_page_url + '?callback=true';
                            }
                        }
                    });
                } else if (redirect) {
                    window.location = next_page_url;
                }

            } else {
                console.log('POST ERROR', data);
                // window.location = next_page_url;
            }
        },
        'json'
    );
}
var cmodel = {}
function validateForm(element){
    ga_lead_submit();
    var context = $(element);
    $('.contact-me-error').hide();
    var cnum = $('#contact-me-num').val();
    var res = null;
    if(cnum){
        if (cnum.length == 10){
            res = cnum.match(/[7-9]{1}\d{9}/);
            if(res){
                cmodel =  {
                    'mobile' : cnum,
                    'campaign': 'Motor',
                    'label': page_id,
                    'policy_type': policy_type,
                    'triggered_page': window.location.href,
                    'tracker_id': tracker_id
                };
                if(policy_type=="expired"){
                    cmodel['campaign'] = 'motor-offlinecases';
                    Cookies.set('lms_campaign',cmodel['campaign']);
                }
                Cookies.set('mobileNo', cnum);
                Cookies.set('mobileNoOn', 'buy-direct-online - Top Form');
                Cookies.set('mobileNoURL', window.location.href);
                Cookies.set('label', page_id);

                if(!otp){
                    context.addClass('disabled').text('Just a moment...');
                }


                if(!network) var network = Cookies.get('network') == ''?'direct':Cookies.get('network');
                if(!category) var category = 'motor';

                // Added by: Rajesh Dixit
                // On 22-01-2016
                // Reason: Open expired tab on product page with default value T-1.
                if(expiredLandingPageIDs.indexOf(page_id)>-1){
                    var monthList = ["January","February","March","April","May","June","July","August","September","October","November","December"];
                    var _yesterday = new Date();
                    _yesterday.setDate(_yesterday.getDate() - 1);
                    _yesterday.setHours(0,0,0,0);

                    setParameter("policyExpired", true);
                    setParameter("expiry_date", _yesterday.getDate());
                    setParameter("expiry_month", monthList[_yesterday.getMonth()]);
                    setParameter("expiry_year", _yesterday.getFullYear());
                }
                return true;
            } else {
                $('.contact-me-error').show();
                return false;
            }
        } else {
            $('.contact-me-error').show();
            return false;
        }
    } else {
        if (hero_phone_mandatory == 'YES') {
            $('.contact-me-error').show();
            return false;
        } else {
            window.location = next_page_url;
            return false;
        }
    }
}

$(document).on('click', '#show_quote_btn', function(){
    if(validateForm($(this))){
        save_lead(false, false);
    }
});
$(document).on('click', '#show_quote_btn_otp', function(){
    if(typeof network === "undefined"){
        network = '';
    }
    if(validateForm($(this))){
        if(network != "affiliate"){
            save_lead(true, false);
        }
        send_otp(save_lead);
    }
});


