var next_page_url = '/motor/car-insurance/';
if (typeof next_page_override != 'undefined'){
    if(!$.isEmptyObject(next_page_override)){
        next_page_url = next_page_override;
    }
}

function ClickToCall(label){
    window.dataLayer.push({
        'event': 'GAEvent',
        'hitType': 'event',
        'eventCategory': 'MOTOR',
        'eventAction': 'click_to_call',
        'eventLabel': page_id + ' Label_' + label,
        'eventValue': 0
    });
    window.location = 'tel:' + toll_free_no;
    return false;
}

function skipForm(label){

    // mixpanel.track("Car LP Submitted", {
    //     'Phone Number Status':'Phone Number Not Given',
    //     'Form Placement':'lp_compare_policies',
    //     'Network':network,
    //     'Category':category,
    // });

    window.dataLayer.push({
        'event': 'GAEvent',
        'hitType': 'event',
        'eventCategory': 'MOTOR',
        'eventAction': 'hero_skip_'+ label,
        'eventLabel': page_id,
        'eventValue': 0
    });
    window.location = next_page_url;
    return false;
}


