var $ = django.jQuery;
$('document').ready(function(){
    $('#id_make').change(function(){
        $('#id_model').prop('disabled', 'disabled');
        var selected_make = $('#id_make').val();
        $('#id_model').empty();
        $.ajax({
            type: "POST",
            url: '/motor/populate/motor-product/',
            data: {'make': selected_make},
            success: function(json_data){
                $('#id_model').prop('disabled', false)
                $('#id_model').empty();
                $.each(json_data['models'], function(i, obj){
                    $('#id_model').append($('<option>').text(obj.name).attr('value', obj.id));
                });
            },
        });
    });
})
