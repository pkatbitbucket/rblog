define(['knockout', 'utils','configuration'], function(ko, utils, settings) {
    var form_details = data.form_details;

    var vehicle_type_verbose = VEHICLE_TYPE == 'fourwheeler' ? 'Car':'Bike'; 
    
    var formType = utils.getUrlParams()['type'];
    if(formType) {formType = formType[0];}

    var occupation_list = [];

    if (form_details.occupation_list) {
        for (var k in form_details.occupation_list) {
            occupation_list.push({
                'key': form_details.occupation_list[k],
                'value': k
            });
        }
    }

    var financier_list = [];

    if (form_details.financier_list) {
        for (var l in form_details.financier_list) {
            financier_list.push({
                'value': l,
                'key': form_details.financier_list[l]
            });
        }
    }

    var pyp_insurer_list = [];

    if (form_details.insurers_list) {
        for (var m in form_details.insurers_list) {
            pyp_insurer_list.push({
                'key': form_details.insurers_list[m],
                'value': m
            });
        }
        pyp_insurer_list.sort(function(a, b){
            if(a.key < b.key) return -1;
            if(a.key > b.key) return 1;
            return 0;
        });
        pyp_insurer_list.unshift({'key':"Select your previous policy insurer", 'value': null})
    }

    var state_list = [];

    if (form_details.state_list) {
        for (var m in form_details.state_list) {
            state_list.push({
                'key': form_details.state_list[m],
                'value': m
            });
        }

        state_list.sort(function(a, b){
            if(a.key < b.key) return -1;
            if(a.key > b.key) return 1;
            return 0;
        });
        state_list.unshift({'key':"SELECT STATE", 'value': null});
    }

    var genderList = [
        {key:"Male", value:"Male"},
        {key:"Female", value:"Female"}
    ]
    var maritalList = [
        {key:"Yes", value:"Married"},
        {key:"No", value:"Unmarried"}
    ]

    var relationship_list = ko.observableArray();
    var getRelationship_list = function(gender){
        var _staticList = ["Wife", "Father", "Mother", "Son", "Daughter", "Brother", "Sister", "Other"];
        var _relList =[], _serverList;
        if(form_details.relationship_list){
            _serverList = form_details.relationship_list;
        }

        if(_serverList){
            for(var i=0; i<_staticList.length; i++){
                if(_serverList.indexOf(_staticList[i])>=0 &&
                    !(_staticList[i]== "Wife" || _staticList[i]== "Husband")){
                    _relList.push(_staticList[i]);
                }
            }

            var married = $('input[name=cust_marital_status]').val();
            if(!married){
                married = data.user_form_details.cust_marital_status || "Married";
            }

            if(married && married.toUpperCase() == "Married".toUpperCase()){
                var gen = data.user_form_details.cust_gender;
                if(gender){
                    gen = $('input[name=cust_gender]').val();
                }
                if(!gen)
                    gen = "Male";
                if(gen.toUpperCase() == "Male".toUpperCase()){
                    _relList.unshift("Wife")
                }
                else{
                    _relList.unshift("Husband");
                }
            }
            // if(_relList.indexOf("Relationship")<0)
            //     _relList.unshift("Relationship");
            relationship_list(_relList);
        }
    }
    getRelationship_list();

    function setTermsandConditions(){
        var title = "I declare that the information provided above is true and I authorize Coverfox Insurance Broking Pvt. Ltd. to represent me at Insurance Companies or Agencies for my Insurance needs."; 
        return title;
    }

    var moreThan90Days = function(){
        if(data.quote_parameters.pastPolicyExpiryDate){
            var past_policy_end_date = utils.stringToDate(data.quote_parameters.pastPolicyExpiryDate, 'dd-mm-yyyy', '-');
            var today = new Date();
            past_policy_end_date.setHours(0, 0, 0, 0);
            today.setHours(0, 0, 0, 0);

            var _diff = utils.getDaysBetweenDates(past_policy_end_date, today);

            return !(_diff>90);
        }
        else{
            return false;
        }
    }

    function setReviewDetailsText(){
        return "Review" + (isExpired()? " Details": " and Make Payment");
    }

    function isExpired(){
        // Check if vehicle is not new.
        if(data.quote_parameters.isNewVehicle === 0){
            if(VEHICLE_TYPE == "fourwheeler" && data.quote_parameters.pastPolicyExpiryDate){
                var past_policy_end_date = utils.stringToDate(data.quote_parameters.pastPolicyExpiryDate, 'dd-mm-yyyy', '-');
                var today = new Date();
                past_policy_end_date.setHours(0, 0, 0, 0);
                today.setHours(0, 0, 0, 0);

                return today > past_policy_end_date? true: false;
            }
        }
        return false;
    }

    isExpired();

    var getForm = function() {
        return [{
            'name': 'Contact Details',
            'type': 'container',
            'id': 'proposer',
            'cssclass': 'stage-header card',
            'children': [{
                    'summaryName': 'Name',
                    'type': 'container',
                    'id': 'name',
                    'cssclass': 'stage-molecule',
                    'children': [{
                        'id': 'cust_first_name',
                        'name': 'cust_first_name',
                        'type': 'text',
                        'validators': ['NonEmpty','validateName', 'startWithAlphaNumeric', 'mandateAlphaNumericChar', 'NameValidation_bharti'],
                        'maxlength': 60,
                        'clean': 'UpperCase',
                        'title': 'First Name',
                        'desc': 'Name mentioned in the RC copy of the Car/Bike',
                        'cssClass': 'uppercase inline-label inline-mobile'
                    }, {
                        'id': 'cust_last_name',
                        'name': 'cust_last_name',
                        'type': 'text',
                        'validators': ['NonEmpty','validateName', 'NameValidation_bharti'],
                        'maxlength': 60,
                        'clean': 'UpperCase',
                        'title': 'Last Name',
                        'cssClass': 'uppercase inline-label inline-mobile'
                    }, ]
                }, {
                    'summaryName': 'Email',
                    'type': 'container',
                    'id': 'email',
                    'cssclass': 'stage-molecule',

                    'children': [{
                        'id': 'cust_email',
                        'name': 'cust_email',
                        'type': 'text',
                        'validators': ['NonEmpty', 'EmailValidation'],
                        'maxlength': 50,
                        'clean': 'LowerCase',
                        'title': 'Email Address',
                        'desc': 'Policy copy will be mailed on this E-mail ID',
                        'cssClass': 'lowercase inline-label large',
                    }],
                }, {
                    'summaryName': 'Phone',
                    'type': 'container',
                    'id': 'phone',
                    'cssclass': 'stage-molecule',

                    'children': [{
                        'id': 'cust_phone',
                        'name': 'cust_phone',
                        'type': 'tel',
                        'validators': ['NonEmpty', 'PhoneValidation','mobileValidation_bajaj'],
                        'clean': 'PhoneNumberClean',
                        'maxlength': 10,
                        'desc': 'You will receive all important messages on this number. No spam guaranteed.',
                        'title': '10 Digit Mobile Number',
                        'cssClass': 'inline-label phone-input',
                    }, ],
                },

                {
                    'name': 'Go to Step 2',
                    'type': 'button',
                    'id': 'goto-step2',
                    'cssclass': 'btn orange large',
                    'title': 'Continue to Basic Details',
                    'clickHandler': 'go_to_next_stage',
                    'extra':{
                        'nextStage': 'proposer-details'
                    }
                },{
                    'id': 'inspection-message',
                    'name': 'inspection-message',
                    'type': 'label',
                    'visible': false,
                    'title':"We will call you within 30 minutes to fix the location and time for vehicle inspection. <br/>Please complete this form in the meanwhile."
                }
            ]
        },{
            'name': 'Basic Details',
            'type': 'container',
            'id': 'proposer-details',
            'cssclass': 'stage-header card',
            'children': [ {
                    'name': 'Gender',
                    'summaryName': 'Gender',
                    'type': 'container',
                    'id': 'gender',
                    'cssclass': 'stage-molecule container-horizontal',
                    'children': [
                    // {
                    //     'id': 'cust_gender',
                    //     'name': 'cust_gender',
                    //     'type': 'select',
                    //     'options': ['Male', 'Female'],
                    // }, ],

                    {
                        'id': 'cust_gender',
                        'name': 'cust_gender',
                        'type': 'radiobox',
                        'options': genderList,
                        'validators':['ValidateRadioBox',],
                        'onchange':getRelationship_list
                    }, ],
                }, {
                    'name': 'Married',
                    'summaryName': 'Married',
                    'type': 'container',
                    'id': 'mstatus',
                    'cssclass': 'stage-molecule container-horizontal',
                    'children': [
                    // {
                    //     'id': 'cust_marital_status',
                    //     'name': 'cust_marital_status',
                    //     'type': 'select',
                    //     'options': ['Married', 'Unmarried'],
                    //     'clean': 'BinarySelectField',
                    // }, ],

                    {
                        'id': 'cust_marital_status',
                        'name': 'cust_marital_status',
                        'type': 'radiobox',
                        'onchange':getRelationship_list,
                        'options': maritalList,
                        'validators':['ValidateRadioBox',]
                    }, ],
                }, {

                    'summaryName': 'Date Of Birth',
                    'type': 'container',
                    'id': 'dob',
                    'cssclass': 'stage-molecule container-horizontal',
                    'name': 'Date of birth',
                    'children': [
                    // {
                    //     'id': 'cust_dob',
                    //     'name': 'cust_dob',
                    //     'type': 'text',
                    //     'validators': ['DOBValidation',],
                    //     'maxlength': 10,
                    //     'title': 'Date Of Birth',
                    //     'clean': 'DOBClean',
                    //     'cssClass': 'inline-label'
                    // },
                    {
                        'id': 'cust_dob',
                        'name': 'cust_dob',
                        'type': 'datePicker',
                        'validators': ['DOBValidation',],
                        'title': 'Date Of Birth',
                        'clean': 'DOBClean',
                        'cssClass': 'inline-label',
                        'dependents': ['nominee_age'],
                    }, ],
                },

                // {
                //     'name': 'Occupation',
                //     'summaryName': 'Occupation',
                //     'type': 'container',
                //     'id': 'occupation',
                //     'cssclass': 'stage-molecule container-horizontal',
                //     'children': [{
                //         'id': 'cust_occupation',
                //         'name': 'cust_occupation',
                //         'type': 'select',
                //         'options': occupation_list,
                //         'optionsText': 'key',
                //         'optionsValue': 'value',
                //     }, ],
                // },

                {
                    'summaryName': 'PAN Number',
                    'type': 'container',
                    'id': 'pan',
                    'cssclass': 'stage-molecule',
                    'children': [{
                        'id': 'cust_pan',
                        'name': 'cust_pan',
                        'type': 'text',
                        'validators': ['NonEmpty', 'PANValidation'],
                        'maxlength': 10,
                        'title': 'PAN number',
                        'clean': 'UpperCase',
                        'cssClass': 'uppercase inline-label'
                    }, ],
                }, {
                    'name': 'Nominee Details',
                    'summaryName': 'Nominee',
                    'type': 'container',
                    'id': 'nominee',
                    'cssclass': 'stage-molecule',
                    'children': [{
                        'id': 'nominee_name',
                        'name': 'nominee_name',
                        'type': 'text',
                        'validators': ['NonEmpty', 'NomineeNameValidation'],
                        'maxlength': 50,
                        'title': 'Nominee Name',
                        'cssClass': 'inline-label'
                    }, {
                        'id': 'nominee_age',
                        'name': 'nominee_age',
                        'otherFieldChanged': 'TriggerNomineeValidation',
                        'type': 'text',
                        'validators': ['NonEmpty', 'NomineeAgeValidation','validateNomineeCustomerAges'],
                        'maxlength': 2,
                        'title': 'Age',
                        'cssClass': 'inline-label small'
                    }, {
                        'id': 'nominee_relationship',
                        'name': 'nominee_relationship',
                        'type': 'select',
                        'validators': ['ValidRelationship', 'NomineeSelectValidation'],
                        'options': relationship_list,
                        'isObservable':'true',
                        'placeholder':"Relationship",
                        'dependents': ['nominee_age'],
                    }, ]
                },

                {
                    'name': 'Go to Step 2',
                    'type': 'button',
                    'id': 'goto-step2',
                    'cssclass': 'btn orange large',
                    'title': 'Continue to Vehicle Details',
                    'clickHandler': 'go_to_next_stage',
                    'extra':{
                        'nextStage': 'vehicle-details'
                    }
                }


            ]
        }, {
            'name': 'Vehicle Details',
            'type': 'container',
            'id': 'vehicle-details',
            'cssclass': 'stage-header card',
            'children': [{
                    'name':vehicle_type_verbose + ' Registration number',
                    'summaryName':vehicle_type_verbose + ' Registration number',
                    'type': 'container',
                    'id': 'registration_number',
                    'cssclass': 'stage-molecule container-horizontal',
                    'children': [{
                        'id': 'vehicle_reg_no',
                        'name': 'vehicle_reg_no',
                        'type': 'registrationNumber',
                        'validators': ['NonEmpty','RegistrationNumberValidation'], // 'NonEmpty','RegistrationNumberValidation' ADDED from index.js
                        'maxlength': 14,
                        'title': vehicle_type_verbose + ' Registration Number',
                        'clean': 'RegistrationNumberClean',
                        'cssClass': 'inline-label'
                    }, ]
                }, {
                    'name': 'AAI Membership Number',
                    'type': 'container',
                    'id': 'aan',
                    'visible': false,
                    'cssclass': 'stage-molecule',
                    'children': [{
                        'id': 'aan_number',
                        'name': 'aan_number',
                        'type': 'text',
                        'validators': ['NonEmpty'],
                        'maxlength': 30,
                        'title': 'AAI Membership Number',
                        'clean': 'NoSpace',
                    }, ]
                }, {
                    'summaryName': 'Previous policy',
                    'type': 'container',
                    'id': 'pyp-details',
                    'cssclass': 'stage-subheader no-title',
                    "visible" : moreThan90Days(),
                    'children': [ {
                        'name': 'Previous policy insurer',
                        'type': 'container',
                        'id': 'pyp-insurer',
                        'cssclass': 'stage-molecule',
                        'children': [{
                            'id': 'past_policy_insurer',
                            'name': 'past_policy_insurer',
                            'type': 'searchselect',
                            'options': pyp_insurer_list,
                            'disallowed_insurers': form_details.disallowed_insurers,
                            'optionsText': 'key',
                            'optionsValue': 'value',
                            'validators':['NonEmpty']
                        }, ],
                    },{
                        'type': 'container',
                        'id': 'pyp-number',
                        'cssclass': 'stage-molecule',
                        'children': [{
                            'id': 'past_policy_number',
                            'name': 'past_policy_number',
                            'type': 'text',
                            'validators': ['NonEmpty','PolicyNumberValidation', 'validatePolicyNumberLength'],
                            'maxlength': 30,
                            'clean': 'NoSpaceWithUpperCase',
                            'title': 'Previous Policy Number',
                            'cssClass': 'uppercase inline-label large'
                        }, ]
                    },  {
                        'name': 'Number of claims',
                        'type': 'container',
                        'id': 'pyp-claims',
                        'cssclass': 'stage-molecule',
                        'visible': false,
                        'children': [{
                            'id': 'past_policy_claims',
                            'name': 'past_policy_claims',
                            'type': 'text',
                            'validators': ['NonEmpty', 'NumberValidation'],
                            'maxlength': 2,
                            'clean': 'NoSpace',
                        }, ]
                    }, {
                        'name': 'Total claim amount',
                        'type': 'container',
                        'id': 'pyp-claims-amount',
                        'cssclass': 'stage-molecule',
                        'visible': false,
                        'children': [{
                            'id': 'past_policy_claims_amount',
                            'name': 'past_policy_claims_amount',
                            'type': 'text',
                            'validators': ['NonEmpty', 'NumberValidation'],
                            'maxlength': 8,
                            'clean': 'NoSpace',
                        }, ]
                    }, ]
                }, {
                    'summaryName': 'Engine and Chassis numbers',
                    'type': 'container',
                    'id': 'body-details',
                    'cssclass': 'stage-molecule hide',
                    'children': [{
                        'id': 'vehicle_engine_no',
                        'name': 'vehicle_engine_no',
                        'type': 'text',
                        'validators': ['engineLengthValidation', 'ValidateEngineChassisNumber'],
                        'maxlength': 25,
                        'title': vehicle_type_verbose + ' Engine Number (Last 7 Digits)',
                        'clean': 'NoSpaceWithUpperCase',
                        'desc': 'You can find this in the RC copy',
                        'cssClass': 'uppercase inline-label medium hide'


                    }, {
                        'id': 'vehicle_chassis_no',
                        'name': 'vehicle_chassis_no',
                        'type': 'text',
                        'validators': ['chassisLengthValidation','ValidateEngineChassisNumber'],
                        'maxlength': 25,
                        'title': vehicle_type_verbose + ' Chassis Number (Last 7 Digits)',
                        'clean': 'NoSpaceWithUpperCase',
                        'cssClass': 'uppercase inline-label medium hide'
                    }, ],
                }, {
                    'summaryName': 'Financier',
                    'type': 'container',
                    'id': 'financier-details',
                    'cssclass': 'stage-subheader',
                    'children': [{
                        'id': 'is_car_financed',
                        'name': 'is_car_financed',
                        'type': 'checkbox',
                        'checked': false,
                        'cssclass': 'stage-molecule',
                        'clean': 'BooleanToOneZero',
                        'title': 'Is your '+ vehicle_type_verbose +' currently on loan?',
                        'dependents': ['financier-name'],
                    }, {
                        'name': 'Loan From',
                        'type': 'container',
                        'id': 'financier-name',
                        'otherFieldChanged': 'toggleFinancierSelection',
                        'visible': false,
                        'cssclass': 'stage-molecule',
                        'desc': 'Please choose your financier from the dropdown list below.',
                        'children': [{
                            'id': 'car-financier',
                            'name': 'car-financier',
                            'type': 'searchselect',
                            'options': financier_list,
                            'optionsText': 'key',
                            'optionsValue': 'value',
                        }, ],
                    }, {

                        'type': 'container',
                        'id': 'financier-name-text',
                        'otherFieldChanged': 'toggleFinancierSelection',
                        'visible': false,
                        'cssclass': 'stage-molecule',
                        'children': [{
                            'id': 'car-financier-text',
                            'name': 'car-financier-text',
                            'type': 'text',
                            'title': 'Name of loan provider',
                            'cssClass': 'inline-label large',
                            'validators': ['NonEmpty'],
                            'maxlength': 30,
                        }, ],
                    }, ]
                },

                {
                    'name': 'Go to Step 3',
                    'type': 'button',
                    'id': 'goto-step3',
                    'cssclass': 'btn orange large',
                    'title': 'Continue to Address',
                    'clickHandler': 'go_to_next_stage',
                    'extra':{
                        'nextStage': 'contact-details'
                    }
                }
            ]
        }, {
            'name': 'Address',
            'type': 'container',
            'id': 'contact-details',
            'cssclass': 'stage-header card',
            'children': [{
                'summaryName': 'Contact Address',
                'type': 'container',
                'id': 'add-details',
                'cssclass': 'stage-subheader no-title',
                'children': [{
                    'type': 'container',
                    'id': 'pincode',
                    'cssclass': 'stage-molecule',
                    'children': [{
                        'id': 'add_pincode',
                        'name': 'add_pincode',
                        'type': 'text',
                        'validators': ['NonEmpty', 'PincodeValidation'],
                        'maxlength': 6,
                        'dependents': ['location'],
                        'title': 'Area Pincode',
                        'cssClass': 'inline-label'
                    }, ]
                },

                {
                    'type': 'container',
                    'id': 'house',
                    'cssclass': 'stage-molecule',
                    'children': [{
                        'id': 'add_house_no',
                        'name': 'add_house_no',
                        'type': 'text',
                        'validators': ['NonEmpty'],
                        'maxlength': 30,
                        'title': 'House Number',
                        'cssClass': 'inline-label'
                    }, {
                        'id': 'add_building_name',
                        'name': 'add_building_name',
                        'type': 'text',
                        'validators': ['NonEmpty', 'validateAddressReliance'],
                        'maxlength': 50,
                        'title': 'Building Name',
                        'cssClass': 'inline-label'
                    }, ]
                }, {
                    'type': 'container',
                    'id': 'street',
                    'cssclass': 'stage-molecule',
                    'children': [{
                        'id': 'add_street_name',
                        'name': 'add_street_name',
                        'type': 'text',
                        'validators': ['NonEmpty', 'validateAddressReliance'],
                        'maxlength': 50,
                        'title': 'Street Name',
                        'cssClass': 'inline-label'
                    }, {
                        'id': 'add_landmark',
                        'name': 'add_landmark',
                        'type': 'text',
                        'validators': ['NonEmpty', 'validateAddressReliance'],
                        'maxlength': 50,
                        'title': 'Landmark',
                        'cssClass': 'inline-label'
                    }, ]
                }, {
                    'name': 'City and State',
                    'type': 'container',
                    'id': 'add_city_state',
                    'cssclass': 'stage-molecule',
                    'visible': false,
                    'children': [{
                        'id': 'add_city_text',
                        'name': 'add_city_text',
                        'type': 'text',
                        'validators': ['NonEmpty'],
                        'maxlength': 30,
                        'title': 'City'
                    }, {
                        'id': 'add_state_text',
                        'name': 'add_state_text',
                        'type': 'text',
                        'validators': ['NonEmpty'],
                        'maxlength': 30,
                        'title': 'State'
                    }, ]
                }, {
                    'name': 'Address Line 3',
                    'type': 'container',
                    'id': 'address_extra_line',
                    'cssclass': 'stage-molecule',
                    'visible': false,
                    'children': [{
                        'id': 'add_extra_line',
                        'name': 'add_extra_line',
                        'type': 'text',
                        'validators': ['NonEmpty'],
                        'maxlength': 30,
                        'title': 'Address',
                        'cssClass': 'inline-label'
                    }, ]
                },  {
                    'name': 'Location',
                    'type': 'container',
                    'id': 'location',
                    'cssclass': 'stage-molecule',
                    'otherFieldChanged': 'fetchLocationDetails',
                    'visible': false,

                    'children': [{
                        'id': 'add_state',
                        'name': 'add_state',
                        'type': 'text',
                        'validators':['NonEmptySelect'],
                        'maxlength': 30,
                        'title': 'State',
                        'cssClass': 'input-disabled inline-label'
                    }, {
                        'id': 'add_district',
                        'name': 'add_district',
                        'type': 'text',
                        'validators': ['NonEmpty'],
                        'maxlength': 30,
                        'title': 'District',
                        'cssClass': 'input-disabled inline-label',
                    }, {
                        'id': 'add_city',
                        'name': 'add_city',
                        'type': 'searchselect',
                        'options': [],
                        'optionsText': 'name',
                        'optionsValue': 'id',
                        'visible': false,
                        'validators':['NonEmptySelect']
                    }, ]
                }, {
                    'name': 'Location',
                    'type': 'container',
                    'id': 'location_state_city',
                    'cssclass': 'stage-molecule',
                    'visible': false,

                    'children': [{
                        'id': 'loc_add_state',
                        'name': 'loc_add_state',
                        'type': 'searchselect',
                        'options': state_list,
                        'optionsText': 'key',
                        'optionsValue': 'value',
                        'dependents': ['loc_add_city'],
                        'validators':['NonEmptySelect']
                    }, {
                        'id': 'loc_add_city',
                        'name': 'loc_add_city',
                        'type': 'searchselect',
                        'options': [],
                        'optionsText': 'name',
                        'optionsValue': 'id',
                        'otherFieldChanged': 'fetchCitiesForState',
                        'validators':['NonEmptySelect'],
                        'visible': false,
                    }, ]
                }, ]
            }, {
                'id': 'add-same',
                'name': 'add-same',
                'type': 'checkbox',
                'checked': true,
                'dependents': ['reg-add-details'],
                'clean': 'BooleanToOneZero',
                'title': 'Address mentioned in RC book is same as above address'
            }, {
                'name': vehicle_type_verbose + ' Registration Address',
                'type': 'container',
                'id': 'reg-add-details',
                'cssclass': 'stage-subheader',
                'visible': false,
                'otherFieldChanged': 'toggleRegistrationAddress',
                'children': [
                {
                    'type': 'container',
                    'id': 'reg_pincode',
                    'cssclass': 'stage-molecule',
                    'children': [{
                        'id': 'reg_add_pincode',
                        'name': 'reg_add_pincode',
                        'type': 'text',
                        'validators': ['NonEmpty', 'PincodeValidation'],
                        'maxlength': 6,
                        'dependents': ['reg_location'],
                        'title': 'Area Pincode',
                        'cssClass': 'inline-label'
                    }, ]
                },

                {
                    'type': 'container',
                    'id': 'reg_house',
                    'cssclass': 'stage-molecule',
                    'children': [{
                        'id': 'reg_add_house_no',
                        'name': 'reg_add_house_no',
                        'type': 'text',
                        'validators': ['NonEmpty'],
                        'maxlength': 30,
                        'title': 'House Number',
                        'cssClass': 'inline-label'
                    }, {
                        'id': 'reg_add_building_name',
                        'name': 'reg_add_building_name',
                        'type': 'text',
                        'validators': ['NonEmpty', 'validateAddressReliance'],
                        'maxlength': 50,
                        'title': 'Building Name',
                        'cssClass': 'inline-label'
                    }, ]
                }, {
                    'type': 'container',
                    'id': 'reg_street',
                    'cssclass': 'stage-molecule',
                    'children': [{
                        'id': 'reg_add_street_name',
                        'name': 'reg_add_street_name',
                        'type': 'text',
                        'validators': ['NonEmpty', 'validateAddressReliance'],
                        'maxlength': 50,
                        'title': 'Street Name',
                        'cssClass': 'inline-label'
                    }, {
                        'id': 'reg_add_landmark',
                        'name': 'reg_add_landmark',
                        'type': 'text',
                        'validators': ['NonEmpty', 'validateAddressReliance'],
                        'maxlength': 50,
                        'title': 'Landmark',
                        'cssClass': 'inline-label'
                    }, ]
                }, {
                    'name': 'City and State',
                    'type': 'container',
                    'id': 'reg_add_city_state',
                    'cssclass': 'stage-molecule',
                    'visible': false,
                    'children': [{
                        'id': 'reg_add_city_text',
                        'name': 'reg_add_city_text',
                        'type': 'text',
                        'validators': ['NonEmpty'],
                        'maxlength': 30,
                        'title': 'City'
                    }, {
                        'id': 'reg_add_state_text',
                        'name': 'reg_add_state_text',
                        'type': 'text',
                        'validators': ['NonEmpty'],
                        'maxlength': 30,
                        'title': 'State'
                    }, ]
                }, {
                    'name': 'Address Line 3',
                    'type': 'container',
                    'id': 'reg_address_extra_line',
                    'cssclass': 'stage-molecule',
                    'visible': false,
                    'children': [{
                        'id': 'reg_add_extra_line',
                        'name': 'reg_add_extra_line',
                        'type': 'text',
                        'validators': ['NonEmpty'],
                        'maxlength': 30,
                        'title': 'Address'
                    }, ]
                }, {
                    'name': 'Location',
                    'type': 'container',
                    'id': 'reg_location',
                    'cssclass': 'stage-molecule',
                    'otherFieldChanged': 'fetchLocationDetails',
                    'visible': false,
                    'children': [{
                        'id': 'reg_add_state',
                        'name': 'reg_add_state',
                        'type': 'text',
                        'validators': ['NonEmpty'],
                        'maxlength': 30,
                        'title':'State',
                        'cssClass': 'input-disabled inline-label'
                    }, {
                        'id': 'reg_add_district',
                        'name': 'reg_add_district',
                        'type': 'text',
                        'validators': ['NonEmpty'],
                        'maxlength': 30,
                        'title':'District',
                        'cssClass': 'input-disabled inline-label'
                    }, {
                        'id': 'reg_add_city',
                        'name': 'reg_add_city',
                        'type': 'searchselect',
                        'options': [],
                        'optionsText': 'name',
                        'optionsValue': 'id',
                        'visible': false,
                        'validators':['NonEmpty']
                    }, ]
                }, {
                    'name': 'Location',
                    'type': 'container',
                    'id': 'reg_location_state_city',
                    'cssclass': 'stage-molecule',
                    'visible': false,

                    'children': [{
                        'id': 'loc_reg_add_state',
                        'name': 'loc_reg_add_state',
                        'type': 'searchselect',
                        'options': state_list,
                        'optionsText': 'key',
                        'optionsValue': 'value',
                        'dependents': ['loc_reg_add_city'],
                    }, {
                        'id': 'loc_reg_add_city',
                        'name': 'loc_reg_add_city',
                        'type': 'searchselect',
                        'options': [],
                        'optionsText': 'name',
                        'optionsValue': 'id',
                        'otherFieldChanged': 'fetchCitiesForState',
                        'visible': false,
                    }, ]
                }, ]
            },

            {
                    'name': 'Go to Step 4',
                    'type': 'button',
                    'id': 'goto-step4',
                    'cssclass': 'btn orange large',
                    'title': setReviewDetailsText(),
                    'clickHandler': 'go_to_next_stage',
                    'extra':{
                        'nextStage': 'accept_terms_block'
                    }
                }

            ],
        }, {
            'name': 'Terms And Conditions',
            'type': 'container',
            'id': 'accept_terms_block',
            'cssclass': 'stage-header card',
            'visible': "!is_submit()",
            'children': [{
                'type': 'container',
                'id': 'accept_terms_container',
                'validators': ['TermsAndConditionsValidation'],
                'cssclass': 'stage-subheader',
                'children': [{
                    'id': 'accept_terms',
                    'name': 'accept_terms',
                    'type': 'checkbox',
                    'checked': true,
                    'title': setTermsandConditions()
                }]
            },]
        },]
    };

    return {
        getForm: getForm,
        pyp_insurer_list:pyp_insurer_list,
        financier_list:financier_list,
        occupation_list:occupation_list,
        state_list:state_list


    }
});
