define([
        'knockout', 'models', 'crypt', 'utils', 'configuration','form-models', './general-form', './others-form',
        'components','rtoStateMapping', 'events', 'actions', 'constants', 'fastlane', 'liveOffline', './summarymap'],
    function(
        ko, models, crypt, utils, settings, formmodels, generalform, othersform,
        components, rtoStateMapping, events, actions, constants, fastlane, liveOffline, summarymap) {

    var $ = require('npm-zepto');
    var autosaving = ko.observable(false);
    var form_wait = ko.observable(false);
    var pageLoaded = ko.observable(false);
    var showDetails = ko.observable(false);
    var is_submit = ko.observable(false);
    var proposalCreated = ko.observable(false);
    var isPageRendered = ko.observable(false);
    var formType = utils.getUrlParams()['type'];
    var isSilentReset = ko.observable(false);
    if(formType) {formType = formType[0];}

    var refreshQuoteStatus = ko.observable();
    var refreshError= ko.observable();
    var refreshCount = 0;
    var premiumDifference = ko.observable(0);

    var cdChecked = ko.observable('CD_ACCOUNT');
    var cdBalance = ko.observable(cd_balance);
    var cd_form_error = ko.observable();
    var isFastlaneFieldVisible = ko.observable(false);

    isFastlaneFieldVisible.subscribe(function(newValue){
        if(typeof(newValue) === "string" && newValue === "true"){
            isFastlaneFieldVisible(true);
        }
        if(newValue){
            showEngineChassisNumber();
        }
    });

    window.addEventListener("load", function(){
        isPageRendered(true);
    });

    // Used to Update quote. Used when quote link is shared and quote's value updates.
    function refreshQuote(){

        $.post('/motor/'+ VEHICLE_TYPE +'/api/transaction/' + transaction_id + '/refresh/',

        { 'csrfmiddlewaretoken': CSRF_TOKEN },
        function(response) {
            response = crypt.parseResponse(response);

            if(response.errorCode === null){
                var new_plan = new models.Plan(response.data);
                var plan_totalPremium = plan().calculatedTotalPremium();
                var _plan = plan();

                // Assign new_plan to plan and calculate total premium.
                // Calculating total premium without assigning skips basic covers and gives big difference.
                plan(new_plan);
                updateBasicCovers();
                var newPlan_totalPremium = plan().calculatedTotalPremium();

                premiumDifference(parseInt(newPlan_totalPremium) - parseInt(plan_totalPremium));

                if(Math.abs(premiumDifference()) <=1){
                    plan(_plan);
                    updateBasicCovers();
                }
                refreshQuoteStatus("Success");
            }

            else{
                initRefreshQuote();
                computeRefreshError(response);
            }


            if(plan().bypassInspection !== '1'){
               showInspectionMessage();

            }
        });
    }

    function computeRefreshError(response){
        refreshError({
            errorCode: response.errorCode,
            errorItems: response.errorItems.slice(),
            errorMessage: response.errorMessage
        });
    }

    function initRefreshQuote(){
        var delay = 3000;
        refreshQuoteStatus("Pending");
        var refreshInterval = setTimeout(function(){
            if(++refreshCount < 20)
                refreshQuote();
        }, delay);
    }

    refreshQuote();

    var get_next_ncb = function(ncb) {
        var next_ncb = 50;
        if (ncb != 50) {
            for (var i = 0; i < constants.ncb_options.length; i++) {
                if (constants.ncb_options[i].value == ncb) {
                    next_ncb = constants.ncb_options[i + 1].value
                }
            };
        }
        return next_ncb
    }

    var current_city_list;
    utils.show_chat_box();
    var allAddons = settings.getConfiguration().addons;

    var scroll_offset = 64;
    if(IS_MOBILE == 'True'){
        scroll_offset = 0;
    }

    var showFooter = ko.observable(false);
    var expired_90days = ko.observable(false);
    var stages = ['proposer', 'proposer-details', 'vehicle-details', 'contact-details', 'accept_terms_block'];

    var currentStage = ko.observable();
    var currentStageButtonField = ko.computed(function(){
        var buttonField;
        try{
            var children = currentStage().children;
            for (var i = 0; i < children.length; i++) {
                if(children[i].Type == 'button'){
                    buttonField = children[i];
                }
            };
        }
        catch(e){

        }
        return buttonField;
    });

    currentStage.subscribe(function(newStage) {
        var stageIndex = stages.indexOf(newStage.Id);
        for (var i = 0; i < stages.length; i++) {
            if (i < stageIndex) {
                formview.getFieldById(stages[i]).showEditButton(true);
            } else {
                formview.getFieldById(stages[i]).showEditButton(false);
            }
        };

        if (newStage.Id == 'accept_terms_block') {
            showFooter(true);
        }
        else{
            showFooter(false);
        }
    });

    var indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

    var sanitize = function(fields){
        var sanitizedFields = {}
        for(var key in fields){
            var value = fields[key]
            if(key == 'cust_marital_status'){
                if(value) {
                    fields[key] = value.indexOf("Un")>=0?'No':'Yes';
                }
            }

            if(key == 'cust_phone'){
                if(value) {
                    fields[key] = '+91 '+value;
                }
            }

            if(key == 'past_policy_insurer'){
                var insurer = generalform.pyp_insurer_list.filter(function(item){
                    return item.value == value;
                })[0];
                if(insurer){
                    insurer = insurer.key;
                    fields[key] = insurer;
                }
            }

            if(key == 'loc_add_state'){

                var state = generalform.state_list.filter(function(item){
                    return item.value == value;
                })[0];


                if(state){
                    state = state.key;
                    fields[key] = state;
                }
            }

            if(key == 'loc_add_city' || key == 'add_city'){

                var city = current_city_list.filter(function(item){
                    return item.id == value;
                })[0];

                if(city){
                    city = city.name;
                    fields[key] = city;
                }
            }

            if(key == 'is_car_financed'){
                delete fields[key]
            }

            if(key == 'car-financier'){
                var financier = generalform.financier_list.filter(function(item){
                    return item.value == value;
                })[0];
                if(financier){
                    financier = financier.key;
                    fields[key] = financier;
                }
            }

            sanitizedFields[key] = fields[key]
        }

        if(window.isFastLaneData && data.quote_parameters['isNewVehicle'] == '0'){
            delete sanitizedFields['vehicle_engine_no'];
            delete sanitizedFields['vehicle_chassis_no'];
        }

        if(data.quote_parameters['isNewVehicle'] == '1'){
            delete sanitizedFields['past_policy_insurer'];
        }

        return sanitizedFields
    }


    var extraMethods = {
        NomineeSelectValidation:function(data, error){
            if(!data){
                error("Please select your relationship with the Nominee.");
                return false;
            }
            else{
                error("");
                return true;
            }
        },
        ValidateRadioBox: function(data, error){
            if(data && data.length>0){
                error("");
                return true;
            }
            else{
                error("Please select a value.");
                return false;
            }

        },
        PolicyNumberValidation:function(data, error){
            // should be at least 5 char and must have at least 1 alphamuneric
            var reg = /^[a-zA-Z0-9-]+$/;
            if(!reg.test(data)){
                error("Please enter a valid Previous Policy Number");
                return false;
            }

            if(data.length <5){
                error("Policy number must be at least 5 characters.")
            }
            return true;
        },
        ValidRelationship: function(data, error){
            var relationship = data || window.data.user_form_details.nominee_relationship;
            if(relationship && !this.relationship_error_shown){
                if(formview.getFieldById('nominee_relationship').OptionsList().indexOf(relationship) > -1){
                    return this.relationship_error_shown = true;
                } else {
                    error("Unfortunately this insurer doesn't allow " + relationship.toLowerCase() + " to be your nominee. Please select a different nominee.");
                    return false;
                }
            } else{
                return true;
            }
        },

        PreviousInsurerValidation:function(data, error){
            var disallowed_insurers = formview.getFieldById("past_policy_insurer").extra.disallowed_insurers;

            if (indexOf.call(Object.keys(disallowed_insurers), data) >= 0) {
                //error("We are sorry. You have an existing policy from " + disallowed_insurers[data] + " and currently it's allowed to renew it on Coverfox. You can buy immediately by selecting another insurer. All the information you entered is saved. Our representative will call you shortly to assist you with the purchase. If you wish to contact us, call us on our Toll Free Number " + CAR_TOLL_FREE);
                return false;
            } else {
                return true;
            }
        },

        mandateAlphaNumericChar:function(data, error){
            var reg = /(?=[a-zA-Z0-9]).+/;
            if(!reg.test(data)){
                error("Invalid value entered");
                return false;
            }
            return true;
        },
        NomineeAgeValidation: function(data, error) {
            var valid = true;
            if (data){
                var regex = /^[0-9]+$/;
                if(!regex.test(data)){
                    error("Please enter a valid age for the Nominee.")
                    valid = false;
                }
                else if(parseInt(data) < 18){
                    error("Nominee must be more than 18 years old.")
                    valid = false;
                }
            }
            else{
                error("Nominee Age cannot be left blank.");
                valid = false;
            }
            return valid;
        },
        validateNomineeCustomerAges: function(data,  error) {
            var cust_dob = 0,
                cust_age = 0,
                nom_age = 0;

            cust_dob = formview.getFieldById("cust_dob").Value();

            // Handling for cases where user has selected DOB on results page in Discount section.
            // cust_dob is hidden and does not have any value
            if(!cust_dob && data && window.data.quote_parameters)
                cust_dob = window.data.quote_parameters.extra_user_dob;

            if(cust_dob && cust_dob.length>1){
                cust_dob =utils.stringToDate(cust_dob, 'dd-mm-yyyy', '-');
            }
            if(typeof(cust_dob)=== "object"){
                cust_age = utils.calculateAgeFromDate(cust_dob);
                nom_age = formview.getFieldById("nominee_age").Value();

                var rel = formview.getFieldById("nominee_relationship").Value();
                if(rel){
                    rel = rel.toLowerCase();
                    switch(rel.toUpperCase()){
                        case "Son".toUpperCase():
                        case "Daughter".toUpperCase():
                        case "Child".toUpperCase():
                        case "Grandchild".toLowerCase():
                            if(nom_age>cust_age){
                                error("Your " + rel +" cannot be older than you.");
                                return false;
                            }
                            else{
                                if(cust_age - parseInt(nom_age)<18){
                                    error("Your "+rel+" should be at least 18 years younger to you. ");
                                    return false;
                                }
                            }
                            break;
                        case "Father".toUpperCase():
                        case "Mother".toUpperCase():
                        case "Parent".toUpperCase():
                        case "Grandparent".toLowerCase():
                            if(nom_age<cust_age){
                                error("Your " + rel +" cannot be younger to you.");
                                return false;
                            }
                            else{
                                if(parseInt(nom_age) - cust_age <18){
                                    error("Your " + rel + " should be at least 18 years older than you.");
                                    return false;
                                }
                            }
                            break;
                        case "Guardian".toUpperCase():
                        case "Legal Guardian".toUpperCase():
                            if(nom_age<cust_age){
                                error("Your "+rel+" cannot be younger.")
                                return false;
                            }
                            break;
                    }
                }
            }
            else{
                return false;
            }
            error("");
            return true;
        },
        NomineeNameValidation: function(data, error){
            if(data){
                var nameRegex = /^[a-z\s]*$/gi;
                if(!nameRegex.test(data)){
                    error("Please enter a valid name.");
                    return false;
                }
            }
            return true;
        },
        TriggerNomineeValidation: function(field, id, value){
            field.validators.forEach(function(row, index){
                if ((typeof(row) == 'function') && (!row(field.getValue(), field.errorMessage))) {
                    field.error(true);
                    return;
                } else if ((typeof(row) == 'string') && field.parent.extraMethods.hasOwnProperty(row)) {
                    if (!field.parent.extraMethods[row](field.getValue(), field.errorMessage)) {
                        field.error(true);
                        return;
                    }
                }
                field.error(false);
            })
        },
        PANValidation: function(data, error) {
            //var matched = data.match('^[A-Z0-9]{10}');
            var matched = data.match('[A-Z]{5}[0-9]{4}[A-Z]{1}');
            if (matched) return true;
                error('Please enter a valid PAN Number.');
            return false;
        },
        TermsAndConditionsValidation: function(data, error) {
            if (!data.accept_terms)
                error('Please accept the above terms and conditions to proceed.');
            return data.accept_terms;
        },
        DOBClean: function(field, value) {
            if(value){
                return value;
            }
        },
        ValidateEngineChassisNumber:function(data, error){
            var reg = /^[A-Za-z0-9*]+$/;
            if(!reg.test(data)){
                error("Invalid value entered");
                return false;
            }
            else{
                error("");
                return true;
            }
        },
        engineLengthValidation:function(data, error){
            if(data!= undefined){
                var regAsterisk = /\*/g;
                if(data.replace(regAsterisk, '').length == 0){
                    error("Engine Number cannot be left blank.");
                    return false;
                }
                else{
                    error("");
                    return true;
                }
            }
            else{
                error("Engine Number cannot be left blank.");
                return false;
            }
        },
        chassisLengthValidation:function(data, error){
            if(data!= undefined){
                var regAsterisk = /\*/g;
                if(data.replace(regAsterisk, '').length == 0){
                    error("Chassis Number cannot be left blank.");
                    return false;
                }
                else{
                    error("");
                    return true;
                }
            }
            else{
                error("Chassis Number cannot be left blank.");
                return false;
            }
        },
        startWithAlphaNumeric: function(data, error){
            var reg = /^[A-Za-z0-9]/
            if(!reg.test(data)){
                error("Name should start with an alphabet.");
                return false;
            }
            return true;
        },
        validateName: function(data, error){
            var reg = /^[a-z\.\'\`-\s]+$/gi;
            if(!reg.test(data)){
                error("Invalid data entered.");
                return false;
            }
            return true;
        },
        DOBValidation: function(data, error) {
            if (data == "") {
                error("This field cannot be empty.");
                return false;
            }
            if (data) {
                if (isNaN(data.split('-')[0]) || isNaN(data.split('-')[1]) || isNaN(data.split('-')[2])) {
                    error("Please enter a valid date.");
                    return false;
                }
                var cust_dob = utils.stringToDate(data, 'dd-mm-yyyy', '-'),
                    cust_age = utils.calculateAgeFromDate(cust_dob);

                // For case 31st feb, JS Date obj will return 3rd March.
                if(cust_dob.getMonth()+1 != data.split('-')[1]){
                    error("Please enter a valid Date of Birth");
                    return false;
                }

                if (cust_age < 18) {
                    error("You must be at least 18 years old to buy a car insurance. ");
                    return false;
                }
                else if(cust_age >100){
                    error("You are "+ cust_age+" years old. Really?");
                    return false;
                }

                // Check if user has deleted any field's value after updation to model.
                var numbox = document.getElementsByTagName("number-inputbox");
                var mySelect= document.getElementsByTagName("my-select");

                if(numbox && numbox.length>0){
                    for(var i=0;  i< numbox.length; i++){
                        if(numbox[i].children.length>0 && numbox[i].children[0].value.trim().length == 0){
                            error("This field cannot be empty.");
                            return false;
                        }
                    }
                }

                if(mySelect && mySelect.length>0){
                    var e = mySelect[0].children;
                    if(e[0] && e[0].value.length == 0){
                        error("This field cannot be empty.");
                        return false;
                    }
                }
            } else {
                error("Please enter a valid date.");
                return false;
            }
            error("");
            return true;
        },
        RegistrationNumberValidation: function(registration_no, error) {
            var regNo = document.getElementsByTagName("registration-number-widget");

            var valid = true;
            if(regNo && regNo.length >0){
                var _list = regNo[0].children;

                // If Data is present
                if(_list && _list.length >0){
                    if (_list.state.value && _list.state.value.toString().length  == 0) {
                        valid = false;
                    }
                    else{
                        var regexState = /[A-Z]+$/i;
                        if(!regexState.test(_list.state.value)){
                            valid = false;
                        }
                    }

                    if (_list.rto.value && _list.rto.value.toString().length  == 0) {
                        valid = false;
                    }
                    else{
                        var regexDistrict = /^[a-z0-9]+$/i;
                        if(!regexDistrict.test(_list.rto.value)){
                            valid = false;
                        }
                    }

                    if (_list.series.value && _list.series.value.toString().length == 0) {
                        valid = false;
                    }
                    else{

                        // Updated regex to allow "-" and "."
                        // This will be removed once proper requirements are documented.
                        // This is a work around to handle cases like "UK-15-0226"
                        var regexSeries = /^[A-Z\.]+$/i;
                        if(!regexSeries.test(_list.series.value)){
                            valid = false;
                        }
                    }

                    if (_list.number.value && _list.number.value.toString().length  == 0) {
                        valid = false;
                    }
                    else{
                        var regexNumber = /^\d+$/;
                        if(!regexNumber.test(_list.number.value)){
                            valid = false;
                        }
                    }

                    if(!valid){
                        error("Please enter a valid Registration number.")
                        return false;
                    }
                    else{
                        error("");
                    }
                }
                var rto = data.form_details.rto_location_info;
                if (rto.split('-').length == 1) {
                    rto = rto.substring(0, 2) + '-' + rto.substring(2);
                }
                var reg_state = rto.split('-')[0];
                var reg_rto_number = parseInt(rto.split('-')[1]);
                var form_reg_state = registration_no.split('-')[0];
                var form_reg_rto_number = parseInt(registration_no.split('-')[1]);
                if ((reg_state === form_reg_state) && (reg_rto_number === form_reg_rto_number)) {
                    return true;
                }

                error('This registration number doesn\'t belong to selected RTO.');
                return false;
            }
            else {
                return true;
            }

        },
        RegistrationNumberClean: function(field, value) {
            var registrationStr = value.trim().replace(/ /g, "").replace(/-/g, "");
            var part1 = registrationStr.match("^[a-zA-Z]{2}");
            var part4 = registrationStr.match("[0-9]{1,4}$");

            registrationStr = registrationStr.substring(part1.index + part1[0].length, part4.index);
            part1 = part1[0];
            part4 = part4[0];

            while (part4.length < 4) part4 = '0' + part4;

            var part2 = registrationStr.match("^[0-9]{1,2}");
            registrationStr = registrationStr.substr(part2.index + part2[0].length);
            part2 = part2[0];

            while (part2.length < 2) part2 = '0' + part2;

            var part3 = registrationStr;

            var newvalue = (part1 + '-' + part2 + '-' + part3 + '-' + part4).toUpperCase();

            field.cleanedValue(newvalue);
            return newvalue;
        },
        PSDateClean: function(field, value) {
            var dateStr = value['policy_start_dd'] + '-' + value['policy_start_mm'] + '-' + value['policy_start_yyyy'];
            value = {
                'policy_start_date': dateStr,
            }
            return value;
        },
        PSDateValidation: function(data, error) {
            var policy_start_date = utils.stringToDate(data.policy_start_date, 'dd-mm-yyyy', '-');
            var today = new Date();
            policy_start_date.setHours(0, 0, 0, 0);
            today.setHours(0, 0, 0, 0);
            if (today > policy_start_date) {
                error("Policy should start either today or after today.");
                return false;
            }

            return true;
        },
        PYPDateValidation: function(data, error) {
            var past_policy_end_date = utils.stringToDate(data.past_policy_end_date, 'dd-mm-yyyy', '-');
            var today = new Date();
            past_policy_end_date.setHours(0, 0, 0, 0);
            today.setHours(0, 0, 0, 0);
            if (today > past_policy_end_date) {
                error("Looks like your policy has already expired.");
                return false;
            }

            return true;
        },

        toggleFinancierSelection: function(field, id, value) {
            if (id == 'is_car_financed') {
                field.visible(value === 1);
            }
        },
        toggleRegistrationAddress: function(field, id, value) {
            if (id == 'add-same') {
                field.visible(!(value === 1));
            }
        },
        fetchCitiesForState: function(field, id, value) {
            if ((id == 'loc_add_state') || (id == 'loc_reg_add_state')) {
                if (data.form_details.is_cities_fetch) {
                    $.post('/motor/' + VEHICLE_TYPE + '/api/cities/' + insurer_slug + '/', {
                        'state': value,
                        'csrfmiddlewaretoken': CSRF_TOKEN
                    }, function(response) {
                        var isReFormList = false
                        var key = "name"
                        response = crypt.parseResponse(response);
                        if (response.data.cities) {
                            //checking if these keys are present else changing to default values of key and value
                            if(response.data.cities[0]["name"] == undefined && response.data.cities[0]["key"] != undefined)
                              {
                                isReFormList = true
                                key = "key"
                              }

                            response.data.cities.sort(function (a, b, key){
                                if(a[key] < b[key]) return -1;
                                if(a[key] > b[key]) return 1;
                                return 0;
                            });

                            if(isReFormList){
                                var formedList = []
                                for(var i = 0; i < response.data.cities.length; i++){
                                    var obj = {};
                                    obj["name"] = response.data.cities[i]["key"];
                                    obj["id"] = response.data.cities[i]["value"];
                                    formedList.push(JSON.parse(JSON.stringify(obj)));
                                    current_city_list = formedList;
                                }
                            }
                            else {
                                current_city_list = response.data.cities;
                            }

                            // Add default value
                            current_city_list.unshift({"id":null, "name":"SELECT CITY"});

                            field.OptionsList(current_city_list);
                            field.Value(data.user_form_details[field.Id]);
                            field.visible(true);
                        }
                    });
                }
            }
        },
        fetchLocationDetails: function(field, id, value) {
            if (id == 'add_pincode') {
                field.parent.getFieldById("goto-step3").disabled(true);
                $.post('/motor/' + VEHICLE_TYPE + '/api/pincode-details/' + insurer_slug + '/', {
                    'pincode': value,
                    'csrfmiddlewaretoken': CSRF_TOKEN
                }, function(response) {
                    field.parent.getFieldById("goto-step3").disabled(false);
                    response = crypt.parseResponse(response);
                    field.parent.getFieldById('add_state').Value(response.data.province_name);
                    field.parent.getFieldById('add_district').Value(response.data.district_name);

                    if (data.form_details.is_cities_fetch) {
                        $.post('/motor/' + VEHICLE_TYPE + '/api/cities/' + insurer_slug + '/', {
                            'state': response.data.province_name,
                            'csrfmiddlewaretoken': CSRF_TOKEN
                        }, function(res) {
                            res = crypt.parseResponse(res);
                            if (res.data.cities) {
                                res.data.cities.sort(function(a, b){
                                    if(a.name < b.name) return -1;
                                    if(a.name > b.name) return 1;
                                    return 0;
                                });

                                // Adding default value for city list
                                current_city_list = res.data.cities;
                                current_city_list.unshift({"id":null, "name":"SELECT CITY"});

                                // Find id of city to be sselected by default
                                var _selectedCity = 0;
                                res.data.cities.forEach(function(row){
                                    if(row.name.toUpperCase() == response.data.district_name.toUpperCase()){
                                        _selectedCity = row.id;
                                        return;
                                    }
                                });

                                field.parent.getFieldById('add_city').OptionsList(res.data.cities);
                                field.parent.getFieldById('add_city').Value(_selectedCity);
                                field.parent.getFieldById('add_city').visible(true)
                                field.visible(true);
                            }
                        });
                    }
                    else{
                        field.visible(true);
                    }
                });
            }

            if (id == 'reg_add_pincode') {
                $.post('/motor/' + VEHICLE_TYPE + '/api/pincode-details/' + insurer_slug + '/', {
                    'pincode': value,
                    'csrfmiddlewaretoken': CSRF_TOKEN
                }, function(response) {
                    response = crypt.parseResponse(response);
                    field.parent.getFieldById('reg_add_state').Value(response.data.province_name);
                    field.parent.getFieldById('reg_add_district').Value(response.data.district_name);

                    if (data.form_details.is_cities_fetch) {
                        $.post('/motor/' + VEHICLE_TYPE + '/api/cities/' + insurer_slug + '/', {
                            'state': response.data.province_name,
                            'csrfmiddlewaretoken': CSRF_TOKEN
                        }, function(res) {
                            res = crypt.parseResponse(res);
                            if (res.data.cities) {
                                res.data.cities.sort(function(a, b){
                                    if(a.name < b.name) return -1;
                                    if(a.name > b.name) return 1;
                                    return 0;
                                });

                                res.data.cities.unshift({"id":null, "name":"Choose a value"});

                                // Find id of city to be sselected by default
                                var _selectedCity = 0;
                                res.data.cities.forEach(function(row){
                                    if(row.name.toUpperCase() == response.data.district_name.toUpperCase()){
                                        _selectedCity = row.id;
                                    }
                                });

                                field.parent.getFieldById('reg_add_city').OptionsList(res.data.cities);
                                field.parent.getFieldById('reg_add_city').Value(_selectedCity);
                                field.parent.getFieldById('reg_add_city').visible(true)
                                field.visible(true);
                            }
                        });
                    }
                    else{
                        field.visible(true);
                    }
                });
            }
        },
        NoSpaceWithUpperCase: function(field, value) {
            return value.replace(/ /g, "").toUpperCase();
        },
        cleanGeneralFormData: function(formdata) {
            if (data.quote_parameters['isNewVehicle'] == '0') {
                formdata['past_policy_insurer_city'] = data.form_details.rto_location_info;
                formdata['past_policy_cover_type'] = '1000007'; // For package policy

                if(expired_90days()){
                    // Code for form data to be here
                }
            }

            formdata['csrfmiddlewaretoken'] = CSRF_TOKEN;
            return formdata;
        },
        cleanNewVehicleForm: function(formdata) {
            formdata.getFieldById('pyp-details').visible(false);
            formdata.getFieldById('registration_number').visible(false);
            resetEngineChassisNumber();
            setTimeout(function(){isFastlaneFieldVisible(true)},0);
        },
        cleanUsedVehicleForm: function(formdata) {
            formdata.getFieldById('pyp-details').visible(false);
        },
        get_proposer_summary: function(data){
            var summary;
            var children = data.children;
            var name = data.children[0];
            return summary;
        },

        get_stage_summary: function(stage){
            var stage = formview.getFieldById(stage);
            var summary = '', fields = {};
            var children = stage.children;

            if(children.length){
                for (var i = 0; i < children.length; i++) {
                    if(children[i].getValue){
                        var childFields = children[i].getValue();
                        if(typeof childFields == 'object'){
                            for(var key in childFields){

                                // Only populate fields that are visible
                                if(children[i].visible())
                                    fields[key] = childFields[key];
                            }
                        }
                    }
                };

                fields = sanitize(fields);
                summary = "<ul class='summary-items'>"
                for(var key in fields){
                    if(summarymap.map[key] && fields[key]){
                        summary += "<li><span class='key'>"+summarymap.map[key]+":</span> "+fields[key]+"</li>"
                    }
                }
                summary += "</ul>"
                summary += "<div class='hide'>"+JSON.stringify(fields)+"</div>"
            }
            return summary;
        },

        go_to_next_stage: function(params) {
            var stage = params.extra.nextStage;
            extraMethods.go_to_stage(stage);

            var inspectionPassed = (INSPECTION_STATUS == 'DOCUMENT_APPROVED');

            if(stage == 'accept_terms_block' && ((!isPolicyExpired() && !isUsedVehicle()) || inspectionPassed || plan().bypassInspection == '1')) {
                if(cd_enabled && cd_balance)
                {
                    cd_account(true);
                }
                else{
                    createProposal();
                }

            }

            var formValue = formview.getValue();
            if (formview.getFieldById('phone')) {
                var lmsData = JSON.parse(JSON.stringify(formview.getValue()));
                lmsData['make'] = data.form_details.master_make;
                lmsData['model'] = data.form_details.master_model;
                lmsData['variant'] = data.form_details.master_variant;
                $.extend(lmsData, data.quote_parameters);

                var rto = data.quote_parameters['registrationNumber[]'][0] + '-' + data.quote_parameters['registrationNumber[]'][1]
                var insurer = data.quote_response.insurerSlug;
                var mobile = formview.getFieldById('cust_phone').Value();
                var _vehicleType = settings.getConfiguration().vehicle_type;

                // Validate if Offline case and if its first stage
                if (liveOffline.validateExpiredFlow(_vehicleType,insurer,rto,mobile, data.quote_parameters.pastPolicyExpiryDate) &&
                    stage == 'proposer-details'){
                    var extra = {
                        quote_url: window.location.href,
                        campaign:"motor-offlinecases",
                        lms_campaign:"motor-offlinecases",
                        //event: 'inspection_created'
                    }

                    var _cookie = document.cookie.split("lms_offline");
                    if(_cookie.length != 2){
                        var _today = new Date();
                        _today.setDate(_today.getDate() + 7);
                        extra.event = 'inspection_created_frontend';
                        document.cookie = "lms_offline=true; expires=" + _today + ";path=/"
                    }

                    utils.lmsUtils.sendGeneric(lmsData, "proposal-form", mobile, extra, function(){});
                }
                else{
                    utils.lmsUtils.sendData(lmsData, "proposal-form");
                }
            }
        },

        go_to_stage: function(stage) {
            var nextStage = formview.getFieldById(stage);
            var stageIndex = stages.indexOf(stage);
            var currentStageIndex = stages.indexOf(currentStage().Id);
            var currentStageId = currentStage().Id;
            var vehicleType = settings.getConfiguration().vehicle_type == "fourwheeler"?"Car":"Bike";
            var insuranceType = (quote.isNewVehicle == '1')?'New':'Renew';
            insuranceType += " " + vehicleType;

            if(nextStage.Id === "contact-details" && !isRenewAllowed()){
                showRenewalErrorMsg(true);
                var selectedInsurerID = formview.getFieldById("past_policy_insurer").Value() || 0;
                var selectedInsurerName = data.form_details.insurers_list[selectedInsurerID];
                var _local = JSON.parse(localStorage.getItem(VEHICLE_TYPE)) || {};
                _local.past_policy_insurer_name = selectedInsurerName;
                localStorage.setItem(VEHICLE_TYPE, JSON.stringify(_local));
                return false;
            }

            if (stageIndex < currentStageIndex) {
                currentStage().State('collapsed');
                nextStage.State('');
                currentStage(nextStage);
                if (stageIndex > 0) {
                    setTimeout(function() {
                        utils.smoothScroll(document.getElementById('container-' + stages[stageIndex - 1]), scroll_offset);
                    }, 500);
                }
            } else {
                if ((currentStage().hasValidData()) && (stageIndex - currentStageIndex == 1)) {
                    var currentStageId = currentStage().Id;
                    var vehicleType = settings.getConfiguration().vehicle_type == "fourwheeler"?"Car":"Bike";
                    var insuranceType = (quote.isNewVehicle == '1')?'New':'Renew';
                    if(isPolicyExpired())
                        insuranceType = "Expired";
                    insuranceType += " " + vehicleType;

                    /****************** GA Events ******************/
                    var stageName = $($("#container-"+currentStageId).children()[0].children[2]).text();
                    events.gtm_push(vehicleType+"EventProposerForm", vehicleType + " - Proposer "+stageName+" Filled", insuranceType, plan().insurerName);
                    /****************** GA Events ******************/

                    currentStage().State('collapsed');
                    currentStage().isDone(true);
                    nextStage.State('');
                    currentStage(nextStage);
                    if (stageIndex > 0) {

                        setTimeout(function() {
                            if(stageIndex == (stages.length-1)){
                                utils.smoothScroll(document.getElementById('container-' + stages[0]), scroll_offset);
                            }
                            else{
                                utils.smoothScroll(document.getElementById('container-' + stages[stageIndex - 1]), scroll_offset);
                            }
                        }, 500);
                    }
                }
                else{
                    var errors = document.getElementsByClassName('has-error');
                    if(errors.length){
                        utils.smoothScroll(errors[0], 250);
                    }
                }
            }
            if(stage == "accept_terms_block"){
                this.saveFormDetails();
            }
        },

        saveFormDetails: function () {
            if(!isSilentReset()){
                var buyForm = $('#form');
                var formjson = buyForm.serialize();
                if(isFastlaneFieldVisible()){
                    formjson += '&isFastlaneFieldVisible=' + isFastlaneFieldVisible();
                }

                autosaving(true);
                $.post('/motor/' + VEHICLE_TYPE + '/' + insurer_slug + '/desktopForm/' + transaction_id + '/', formjson, function(response) {
                    autosaving(false);
                });
            }
        }
    }

// **************** Duplicate section ********************
    var quote = data.quote_parameters,
        plan = ko.observable();

        plan(new models.Plan(data.quote_response));

    shareableBaseUrl = QUOTE_SHARE_URL.replace('quote_id', quote.quoteId);

    var shareableQuoteUrl = shareableBaseUrl;
    shareableQuoteUrl += "?";
    var addonnumber = 0;
    for (var index in allAddons) {
        var addon = allAddons[index];
        if (quote[addon.id] == '1') {
            addonnumber += Math.pow(2, index);
        }
    }
    shareableQuoteUrl += 'addons=' + addonnumber;

    if (quote['extra_isLegalLiability'] == '1') {
        shareableQuoteUrl += '&ll=1';
    }

    paPassengerMap = {
        '0': 0,
        '10000': 1,
        '50000': 2,
        '100000': 3,
        '200000': 4,
    }
    paTag = paPassengerMap[quote['extra_paPassenger']];
    if (paTag) {
        shareableQuoteUrl += '&pa=' + paTag;
    }

    utils.motorOlark.sendBuyFormNotification(document.URL);

    var selected_addons = ko.observableArray([]);
    selected_addons([]);

    for (var key in quote) {
        if (key.indexOf('addon_') > -1) {
            if (quote[key] == '1') {
                selected_addons.push(key.replace('addon_', ''));
            }
        }
    }

    var add_selected_addons_to_plan = function(selected_addons, plan) {
        var planAddons = plan.addonCovers();
        for (var j = 0; j < planAddons.length; j++) {
            if (selected_addons.indexOf(planAddons[j].id) > -1) {
                planAddons[j].is_selected('1');
            } else {
                planAddons[j].is_selected('0');

            }
        }
        for (var j = 0; j < planAddons.length; j++) {
            if (planAddons[j].is_selected() == '0') continue;
            if (plan.dependentCovers && plan.dependentCovers[planAddons[j].id]) {
                var dependentCovers = plan.dependentCovers[planAddons[j].id];
                for (var i = 0; i < dependentCovers.length; i++) {
                    for (var m = 0; m < planAddons.length; m++) {
                        if (planAddons[m].id == dependentCovers[i]) {

                            planAddons[m].is_selected(planAddons[j].is_selected());
                        }

                    }
                }
            }
        }
        plan.addonCovers.valueHasMutated();
    };
    add_selected_addons_to_plan(selected_addons(), plan());
    plan.subscribe(function(){
        add_selected_addons_to_plan(selected_addons(), plan());
    })

    var basic_covers = [];
    function updateBasicCovers(){
        basic_covers = plan().basicCovers();
        for (var i = basic_covers.length - 1; i >= 0; i--) {
            if (basic_covers[i].id == "legalLiabilityDriver") {
                basic_covers[i].is_selected(parseInt(quote.extra_isLegalLiability) || parseInt(basic_covers[i].default));
            }
        }
    }
    updateBasicCovers();
// ******************** Duplicate Section ends here ********************
    var insurerform;
    try {
        insurerform = require("./insurers/"+insurer_slug+"-form.js");
    }
    catch (err) {
        insurerform = othersform;
        console.info(err.message);
    }

    for (var k in insurerform.extraMethods) {
        extraMethods[k] = insurerform.extraMethods[k];
    }

    var form_details = data.form_details;
    var formview = new formmodels.FormClass(extraMethods);
    formview.putFields(generalform.getForm(), formview.tree);

    extraMethods.cleanInsurerForm(formview, plan());

    if (quote.isNewVehicle == '1') {
        extraMethods.cleanNewVehicleForm(formview);
    }

    if (quote.isUsedVehicle == '1') {
        extraMethods.cleanUsedVehicleForm(formview);
    }

    // Fetching value from Server and setting it to controls
    var user_form_details = data.user_form_details;
    for (var index in formview.fields) {
        var field = formview.fields[index];
        var field_data = user_form_details[field.Id];
        if (field_data) {
            if (field.Type == 'checkbox') {
                field.Value(field_data == "true" || field_data === true);
            } else {
                field.Value(field_data);
            }
        } else {
            if (field.Type == 'searchselect') {
                field.Value(null);
            }
        }
    }

    // Fetch state based on RTO
    var _rtoName = data.quote_parameters["registrationNumber[]"][0];
    var _rtoNum = data.quote_parameters["registrationNumber[]"][1];
    var _state = rtoStateMapping.getStateName(_rtoName);
    var _stateId = 0;

    for (var m in data.form_details.state_list){
        if(_state && data.form_details.state_list[m] == _state.toString().toUpperCase()){
            _stateId = m;
            break;
        }
    }

    // Auto populate State and cities based on RTO selected
    if( !formview.getFieldById("loc_add_state").Value() ||
        (formview.getFieldById("loc_add_state").Value() && formview.getFieldById("loc_add_state").Value().length == 0)){

        formview.getFieldById("loc_add_state").Value(_stateId);
    }

    var fastLaneData = ko.observable()
    var formattedRegNumber = ko.observable();
    var oldValue = [];

    function showInspectionMessage(){
        var rto = data.quote_parameters['registrationNumber[]'][0] + '-' + data.quote_parameters['registrationNumber[]'][1]
        var insurer = data.quote_response.insurerSlug;
        var _vehicleType = settings.getConfiguration().vehicle_type;

        if(plan().bypassInspection === 0){
           if (liveOffline.liveOfflineList.rtos.indexOf(rto)>=0 &&
                liveOffline.liveOfflineList.insurers.indexOf(insurer)>=0 &&
                _vehicleType == 'fourwheeler' &&
                isPolicyExpired() && formType !== 'dealer'){

                formview.getFieldById("inspection-message").visible(true);
            }

        }
    }


    // Check if localStorage has a valid registration number.
    // If yes, assume it came from fastlane homepage,
    // Hide registration Field and display number on left panel
    function isFastlaneSuccesFlow(){
        if ((data.quote_parameters.fastlane_success == "true" || data.quote_parameters.fastlane_success == "0" ) &&
            data.quote_parameters.formatted_reg_number &&
            data.quote_parameters.formatted_reg_number.length > 0){

            formattedRegNumber(data.quote_parameters.formatted_reg_number);

            // Hide registration number field
            formview.getFieldById("registration_number").visible(false);
            formview.getFieldById("vehicle_reg_no").Value(formattedRegNumber());
        }
        else{
            formattedRegNumber("");
        }
    }

    showInspectionMessage();

    window.isFastLaneData = true;
    var timeOutForFastLane = null;
    formview.getFieldById("vehicle_reg_no").Value.subscribe(function(newValue){
        var regYear = data.quote_parameters.registrationDate.split('-')[2];

        // Check if field is visible and call API
        // Call should only be made if registration number is changed
        if (newValue &&
            !formview.getFieldById("vehicle_reg_no").error()
            && isPageRendered()
            && (data.quote_parameters.fastlane_success == "false" ||
                data.quote_parameters.fastlane_success === "0" ||
                data.quote_parameters.fastlane_success === undefined
                )
            && !isFastlaneFieldVisible()){

            if(fastLaneData()){
                oldValue = fastLaneData();
            }

            // to abort call and show fields if fastlane is taking too long.
            timeOutForFastLane = setTimeout(function(){
                fastLaneData({});
            },5000);

            formview.getFieldById("goto-step3").isProcessing(true);

            fastlane.getFastLaneData(newValue, fastLaneData);
        }
    });

    fastLaneData.subscribe(function(newValue){
        // Clearing timeout if data is received before 5sec.
        window.clearTimeout(timeOutForFastLane);
        formview.getFieldById("goto-step3").isProcessing(false);
        var eng_no = '', chasi_no = '';
        if(!isFastlaneFieldVisible()){
            if (newValue &&
                newValue.hasOwnProperty("eng_no") &&
                newValue.vehicle_master_id == data.quote_parameters.vehicleId){

                eng_no = newValue.eng_no;
                chasi_no = newValue.chasi_no;
            }

            formview.getFieldById("vehicle_engine_no").Value(eng_no);
            formview.getFieldById("vehicle_chassis_no").Value(chasi_no);
        }

        // Irrespective to flow, if length is less than min length, make field visible and reset values.
        if (!validateEngineChassisMinLength()){
            resetEngineChassisNumber();
            isFastlaneFieldVisible(true);
        }

        // Show engine/chassis number if user has updated model of vehicle in fastlane flow
        if ((data.quote_parameters.fastlane_success === "true" &&
            data.quote_parameters.fastlane_model_changed === "true")){

            resetEngineChassisNumber();
            isFastlaneFieldVisible(true);
        }
    });

    function validateEngineChassisMinLength(){
        var engine_no = formview.getFieldById("vehicle_engine_no");
        var chassis_no = formview.getFieldById("vehicle_chassis_no");
        var retValue = extraMethods.engineLengthValidation(engine_no.Value(), engine_no.error) &&
                extraMethods.chassisLengthValidation(chassis_no.Value(), chassis_no.error);
        return  retValue;
    }

    function showEngineChassisNumber(){
        var engine_no = formview.getFieldById("vehicle_engine_no");
        var chassis_no = formview.getFieldById("vehicle_chassis_no");

        formview.getFieldById("body-details").cssclass("stage-molecule");

        engine_no.cssclass("uppercase inline-label medium");
        chassis_no.cssclass("uppercase inline-label medium");

        engine_no.error(false);
        chassis_no.error(false);

        window.isFastLaneData = false;

        isFastlaneFieldVisible();
    }

    function resetEngineChassisNumber(){
        formview.getFieldById("vehicle_engine_no").Value("");
        formview.getFieldById("vehicle_chassis_no").Value("");
    }

    var renewNotAllowedMessage = ko.observable("");
    var showRenewalErrorMsg = ko.observable(false);

    //var insurerAllowRenew = ["Bharti AXA", "L&T General", "Liberty Videocon", "Universal Sompo"];
    var isRenewAllowed = ko.computed(function(){
        var selectedInsurerID = formview.getFieldById("past_policy_insurer").Value() || 0;
        var selectedInsurerName = data.form_details.insurers_list[selectedInsurerID];
        var disabledList = data.form_details.disallowed_insurers
        var returnValue = true;

        if(selectedInsurerName && disabledList){
            for(var i in disabledList){
                if(selectedInsurerName.toLowerCase() === disabledList[i].toLowerCase()){
                    returnValue = false;
                }
            }

            if(!returnValue){
                var firstName = formview.getFieldById("cust_first_name").Value();
                var lastName = formview.getFieldById("cust_last_name").Value();
                var msg = "Hey "+ firstName.trim() + " " + lastName.trim() +
                    ", we are sorry. You have an existing policy from "+
                    selectedInsurerName + " and currently they do not allow to renew in such cases on Coverfox. <br/><br/>"+
                    "You could proceed by selecting another insurer. All the information you have entered will be saved.<br/><br/>"+
                    "Our Sales Team has been notified about the issue and someone will call you shortly to assist you with the purchase.<br/><br/>"+
                    "If you wish to contact us, please call us on our Toll Free Number " + CAR_TOLL_FREE;

                renewNotAllowedMessage(msg);
            }
        }

        return returnValue;
    });

    var hidePopup = function(){
        showRenewalErrorMsg(false);
    }

    var isBackToResultClicked = false;
    // Send data to LMS and reroute to results page.
    function gotoResultsPage () {
        if(!isBackToResultClicked){
            isBackToResultClicked = true;
            var formjson = getSerializedForm();
            utils.lmsUtils.sendEvent("rollover_not_allowed",formjson, function(){
                top.location.href = shareableQuoteUrl;
            });
        }
    }

    function isPolicyExpired(){
        if(data.quote_parameters.pastPolicyExpiryDate){
            var past_policy_end_date = utils.stringToDate(data.quote_parameters.pastPolicyExpiryDate, 'dd-mm-yyyy', '-');
            var today = new Date();

            past_policy_end_date.setHours(0, 0, 0, 0);
            today.setHours(0, 0, 0, 0);

            var _diff = utils.getDaysBetweenDates(past_policy_end_date, today);

            if(_diff>=89||isUsedVehicle()){
                //TODO Please create proper flow for used car.
                expired_90days(true);
            }

            if(VEHICLE_TYPE === "fourwheeler"){
                if(_diff>0){
                    utils.setCookieForExpired();
                    return true;
                }
            }

        }
        return false;
    }

    function isUsedVehicle(){
        return data.quote_parameters.isUsedVehicle == 1?true:false;
    }

    function setDefaultPYPDetails(_formdata){
        // Set default value as first eight digits of transaction id
        var randomPolicyNumber = transaction_id.replace(/-/g,'').substring(0,8);
        var newIndiaInsurerId = "";

        // Set ddefault value as New India
        if(data.form_details.insurers_list){
            var _iList = window.data.form_details.insurers_list;
            for (var key in _iList){
                if(_iList[key].indexOf("New India") >=0 ){
                    newIndiaInsurerId = key;
                    break;
                }
            }
        }
        _formdata["past_policy_insurer"] = newIndiaInsurerId;
        _formdata["past_policy_number"] = randomPolicyNumber;

        return _formdata;
    }

    function cleanEngineClassisNumberBeforeSubmit(){
        var engineNo= formview.getFieldById("vehicle_engine_no");
        var chassisNo = formview.getFieldById("vehicle_chassis_no");
        var regex = /\*/g;

        engineNo.Value(engineNo.Value().replace(regex,""));
        chassisNo.Value(chassisNo.Value().replace(regex,""));
    }

    if (quote.isNewVehicle == '1') {
        extraMethods.cleanNewVehicleForm(formview);
    }

    var getSerializedForm = function(){
        var formjson = formview.getValue();
        formjson = extraMethods.cleanGeneralFormData(formjson);
        formjson = extraMethods.cleanInsurerFormData(formjson, plan());
        return formjson;
    }

    var redirectToPaymentGateway = function(){
        if (formview.hasValidData() && proposalCreated()) {
            cleanEngineClassisNumberBeforeSubmit();
            var formjson = formview.getValue();

            if (quote.isNewVehicle == '1') {
                formjson['vehicle_reg_no'] = data.form_details.registration_no;
            }
            else if(formattedRegNumber() && formattedRegNumber().length >0){
                formjson['vehicle_reg_no'] = formattedRegNumber();
            }

            formjson = extraMethods.cleanGeneralFormData(formjson);
            formjson = extraMethods.cleanInsurerFormData(formjson, plan());

            if(expired_90days())
                formjson = setDefaultPYPDetails(formjson);

            var insuranceType = (quote.isNewVehicle == '1') ? 'New':'Renew';
            if(isPolicyExpired()) insuranceType = "Expired";

            // Event on Successfull Payment
            events.cust_ev(actions.MakePayment,{
                defaults: { 'event_label':insurer_title, insuranceType: insuranceType }
            });

            var submit_buy_url = '/motor/' + VEHICLE_TYPE + '/' + insurer_slug + '/submitForm/' + transaction_id + '/';

            utils.lmsUtils.sendData(JSON.parse(JSON.stringify(formjson)), "buy-form", function(){
                var domain = (self == top) ? "" : SITE_URL;
                top.location.href = domain + submit_buy_url;
            });

        }
    }

    proposalCreated.subscribe(function(newvalue){
        if(newvalue && newvalue!= 'failed' && form_wait()){
            redirectToPaymentGateway()
        }

        if(newvalue == 'failed' && form_wait()){
            showProposalFailurePopup();
        }
    });

    form_wait.subscribe(function(newvalue){
        if(newvalue && proposalCreated() && proposalCreated() != 'failed'){
            redirectToPaymentGateway()
        }

        if(newvalue && proposalCreated() == 'failed'){
            showProposalFailurePopup();
        }
    });

    var createProposal = function(){
        proposalCreated(false);
        if (formview.hasValidData()) {
            cleanEngineClassisNumberBeforeSubmit();
            var formjson = formview.getValue();

            if (quote.isNewVehicle == '1') {
                formjson['vehicle_reg_no'] = data.form_details.registration_no;
            }else if(formattedRegNumber() && formattedRegNumber().length >0){
                formjson['vehicle_reg_no'] = formattedRegNumber();
            }

            formjson = extraMethods.cleanGeneralFormData(formjson);
            formjson = extraMethods.cleanInsurerFormData(formjson, plan());

            if(expired_90days())
                formjson = setDefaultPYPDetails(formjson);

            var submit_buy_url = '/motor/' + VEHICLE_TYPE + '/' + insurer_slug + '/submitForm/' + transaction_id + '/';
            $.ajax({
                url: submit_buy_url,
                data: formjson,
                type: "POST",
                success: function(response, status) {
                    if (response.success) {
                        proposalCreated(true);
                    }
                    else{
                        proposalCreated('failed');
                    }
                }
            });
        }

    }

    var popup = ko.observable();
    var cd_account = ko.observable(false);

    var showProposalFailurePopup = function(){
        popup('proposalFailed');
        form_wait(false);
    }

    // This is for GTM. They want plp value.
    var setURLCookie = function (){
        var expires = new Date();
        expires.setDate(expires.getDate() + 1);
        document.cookie ='plp='+ window.location.href +'; path=/; domain=.coverfox.com; expires='+ expires.toUTCString() + ";";
    }

    if(plan().bypassInspection === '1'){
        formview.getFieldById('goto-step3').title('Review and make payment');
    }
    var initiateInspection = function(){
        if (formview.hasValidData()) {
            form_wait(true);
            cleanEngineClassisNumberBeforeSubmit();
            var formjson = formview.getValue();
            formjson = extraMethods.cleanGeneralFormData(formjson);
            formjson = extraMethods.cleanInsurerFormData(formjson, plan());
            formjson['transaction'] = transaction_id

            var lmsData = JSON.parse(JSON.stringify(formview.getValue()));

            lmsData['make'] = data.form_details.master_make;
            lmsData['model'] = data.form_details.master_model;
            lmsData['variant'] = data.form_details.master_variant;
            $.extend(lmsData, data.quote_parameters);

            utils.lmsUtils.sendData(lmsData, "proposal-form", function(){
                top.location.href = '/motor/inspection-form/' + transaction_id + '/';
            });

            events.gtm_push("CarEventInitiateInspection", "Car - Initiate Inspection", "Expired Car", "Inspection Clicked");
        }
    }

    var initializeStageUI = function(){
        //var current_stage_set = true;
        var active_stage = null;
        for (var i = 0; i < stages.length-1; i++) {
            var stage = formview.getFieldById(stages[i]);
            // If renew is not allowed and form filled till step 3, set vehicle-details as current tag
            if ((
                (stage.Id === "vehicle-details" && !isRenewAllowed()) ||

                // If every form is filled, set default stage as contact-details
                (stage.Id === "contact-details") ||

                // If any stage has invalid data, set it as current stage
                !stage.hasValidData(true)) &&

                // If current stage is already set, don't set override it
                !active_stage) {

                active_stage = stage;
            }
            // If stage has valid data then set isDone to be true
            else if(stage.hasValidData(true)){
                stage.isDone(true);
            }
        };

        return active_stage;
    }

    var initializeForm = function() {
        isFastlaneSuccesFlow();
        setURLCookie();
        // Set values received from server
        if (data.user_form_details.vehicle_chassis_no &&
            data.user_form_details.vehicle_chassis_no.length>0){
            formview.getFieldById("vehicle_chassis_no").Value(data.user_form_details.vehicle_chassis_no);
        }

        if (data.user_form_details.vehicle_engine_no &&
            data.user_form_details.vehicle_engine_no.length>0){
            formview.getFieldById("vehicle_engine_no").Value(data.user_form_details.vehicle_engine_no);
        }

        // Check if value received from satisfies minimum length check
        if (!validateEngineChassisMinLength() &&
            (data.quote_parameters.fastlane_success == "true" || data.quote_parameters.fastlane_success === "1")){
            resetEngineChassisNumber();
            isFastlaneFieldVisible(true);
        }

        if (data.user_form_details.isFastlaneFieldVisible)
            isFastlaneFieldVisible(data.user_form_details.isFastlaneFieldVisible)

        var _cStage = initializeStageUI();
        if(TRANSACTION_STATUS === 'FORM_EXPIRED' || TRANSACTION_STATUS === 'COMPLETED' ){
            formview.getFieldById('contact-details').isDone(true)
            formview.getFieldById('accept_terms_block').visible(false);
            currentStage(formview.getFieldById('accept_terms_block'));
            showFooter(false);
        }
        else if(_cStage){
            currentStage(_cStage);
        }
        else if (!currentStage()) {
            currentStage(formview.getFieldById('contact-details'));
        }
        currentStage().State('');

        setTimeout(function() {
            pageLoaded(true);
            $('.footer-innerwrap').addClass('reveal');

            // Update fastlane data on page load if registration number is present and engine number is not present
            /*
                Case: Enter wrong registration number and refresh.
                Since, both fields are hidden by default, if wrong registration is not validated on load,
                it is accepted and form can be submitted without Engine/Chassis number
            */
            if (data.user_form_details.vehicle_reg_no &&
                data.user_form_details.vehicle_reg_no.length>0){
                formview.getFieldById("vehicle_reg_no").Value(formview.getFieldById("vehicle_reg_no").Value());
            }

            // Hide error on page load
            formview.getFieldById("past_policy_insurer").error(false);
            formview.getFieldById("add_city").error(false);
            formview.getFieldById("reg_add_city").error(false);
        }, 500);
        /*********************************** VIZURY BUY PAGE DATA **********************************/
        var vizury_buy_plan_data = {
            'vizury_category': 'MOTOR',
            'po_sub_category': 'fourwheeler',
            'po_name': plan().insurerName,
            'po_price': plan().calculatedTotalPremium(),
            'po_insurer_img_url': window.location.origin + '/static/motor_product/desktopMVP/app/assets/img/insurer/' + plan().insurerId + '.png'
        };
        utils.gtm_data_log(vizury_buy_plan_data);
        utils.gtm_event_log('vizury_buy_page');
        /*******************************************************************************************/
        var self = this;
        setTimeout(function() {
            ko.computed(function() {
                var formValue = formview.getValue();
                self.saveFormDetails();
            })
        }, 1000);
    }

    var submitForm = function(){
            form_wait(true);
    }

    var paymentType = ko.observable();
    var showPaymentMore = ko.observable(false);

    var submitCDDetails = function(event){
        cd_errors = [];
        if(cdChecked()=='CD_ACCOUNT'){
            this_form = $('#capture_payment');
            form_data = this_form.serializeArray();
            form_obj = {};
            total_amt = 0;
            $.each(form_data, function(i, elem){
                form_obj[elem["name"]] = elem['value'];
                if(elem['name'].indexOf('amt')!=-1 && elem['value'])
                total_amt += parseInt(elem['value']);
            });
            if(!paymentType())
            {
                cd_form_error("Select a Payment Type");
                return false;
            }
            if(total_amt < plan().calculatedTotalPremium()){
                cd_form_error("Amount not equals Total amt.");
            }
            else{
                submit_url = "/motor/cdaccount/cd-debit/"+ transaction_id + '/';
                $.post(submit_url, this_form.serialize(), function(response){
                    resonse_data = response;
                    if(resonse_data.success){
                        cd_account(false);
                        createProposal();
                    }
                    else{
                        cd_form_error(resonse_data.errors);
                    }
                });
            }
        }
        else{
            cd_account(false);
            createProposal();
        }

    }
    return {
        Form: formview,
        form_details: form_details,
        quote: quote,
        plan: plan,
        CSRF_TOKEN: CSRF_TOKEN,
        form_wait: form_wait,
        pageLoaded: pageLoaded,
        go_to_stage: extraMethods.go_to_stage,
        get_stage_summary: extraMethods.get_stage_summary,
        showFooter: showFooter,
        shareableQuoteUrl:shareableQuoteUrl,
        currentStageButtonField:currentStageButtonField,
        get_next_ncb:get_next_ncb,
        saveFormDetails: extraMethods.saveFormDetails,
        is_submit: is_submit,
        showDetails:showDetails,
        expired_90days:expired_90days,
        formType:formType,
        popup:popup,
        formattedRegNumber:formattedRegNumber,
        initialize: initializeForm,
        isPolicyExpired: isPolicyExpired,
        premiumDifference:premiumDifference,
        inspectionRequested: Boolean(INSPECTION_STATUS),
        inspectionPassed: INSPECTION_STATUS == 'DOCUMENT_APPROVED',
        inspectionFailed: INSPECTION_STATUS == 'DOCUMENT_REJECTED' || INSPECTION_STATUS == 'PAYMENT_REJECTED',
        inspectionPending: INSPECTION_STATUS == 'PENDING' || INSPECTION_STATUS == 'DOCUMENT_UPLOADED',
        initiateInspection: initiateInspection,
        submitForm: submitForm,
        cd_account: cd_account,
        cdChecked: cdChecked,
        paymentType: paymentType,
        showPaymentMore: showPaymentMore,
        submitCDDetails: submitCDDetails,
        cd_form_error: cd_form_error,
        cdBalance: cdBalance,
        renewNotAllowedMessage:renewNotAllowedMessage,
        showRenewalErrorMsg:showRenewalErrorMsg,
        isRenewAllowed:isRenewAllowed,
        gotoResultsPage:gotoResultsPage,
        hidePopup:hidePopup,
        isUsedVehicle:isUsedVehicle
    }
});
