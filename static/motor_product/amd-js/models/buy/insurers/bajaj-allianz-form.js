define(['knockout'], function(ko) {

    var extraMethods = {
        cleanInsurerForm: function (form) {
            if (data.quote_parameters.extra_user_dob) {
                form.getFieldById('dob').visible(false);
            }
            
            form.getFieldById('is_car_financed').dependents = ['financier-name-text'];

            form.getFieldById('pan').visible(false);
            form.getFieldById('add_pincode').dependents = [];
            form.getFieldById('reg_add_pincode').dependents = [];
            form.getFieldById('add-same').visible(false);
        },
        cleanInsurerFormData: function(formdata) {
            if (data.quote_parameters.extra_user_dob) {
                formdata['cust_dob'] = data.quote_parameters.extra_user_dob;
            }

            formdata['car-financier'] = formdata['car-financier-text'];

            formdata['cust_occupation'] = '';
            formdata['cust_pan'] = '';
            formdata['add_state'] = '';
            formdata['add_district'] = '';

            delete formdata['car-financier-text'];
            return formdata;
        },
        mobileValidation_bajaj: function(data, error){
            if(data == "9999999999"){
                error("Please enter a valid Phone Number.");
                return false;
            }
            else{
                error("");
                return true;
            }
        }
    }

    return {
        extraMethods: extraMethods,
    }
});