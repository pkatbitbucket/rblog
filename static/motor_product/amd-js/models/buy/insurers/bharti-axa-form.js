define(['knockout'], function(ko) {

    var extraMethods = {
        cleanInsurerForm: function(form) {
            if (data.quote_parameters.extra_user_dob) {
                form.getFieldById('dob').visible(false);
            }

            form.getFieldById('is_car_financed').dependents = ['financier-name-text'];

            form.getFieldById('pan').visible(false);
            form.getFieldById('add_building_name').visible(false);
            form.getFieldById('address_extra_line').visible(true);

            // form.getFieldById('house').Name('Address Line 1');
            // form.getFieldById('street').Name('Address Line 2');
            form.getFieldById('address_extra_line').Name('City');
            //form.getFieldById('address_extra_line').title('City');

            form.getFieldById('add_street_name').title('Address Line 2');
            form.getFieldById('add_landmark').visible(false);
            
            form.getFieldById('add_house_no').maxlength(30);
            form.getFieldById('add_street_name').maxlength(30);
            form.getFieldById('add_extra_line').maxlength(30);
            
            form.getFieldById('add_house_no').title('Address Line 1');
            form.getFieldById('add_pincode').dependents = [];
            form.getFieldById('add-same').visible(false);

            form.getFieldById('location_state_city').Name('State');
            form.getFieldById('loc_add_state').dependents = [];
            form.getFieldById('location_state_city').visible(true);
        },
        cleanInsurerFormData: function(formdata) {
            if (data.quote_parameters.extra_user_dob) {
                formdata['cust_dob'] = data.quote_parameters.extra_user_dob;
            }

            formdata['car-financier'] = formdata['car-financier-text'];
            formdata['cust_pan'] = '';
            formdata['add_building_name'] = '';
            formdata['add_city'] = formdata['add_extra_line'];
            formdata['add_landmark'] = ''
            formdata['reg_add_building_name'] = '';
            formdata['reg_add_landmark'] = '';
            formdata['add_state'] = formdata['loc_add_state'];
            formdata['add_district'] = '';

            delete formdata['car-financier-text'];
            delete formdata['add_extra_line'];
            delete formdata['loc_add_state'];

            if (!formdata['add_house_no'] || 
                (formdata['add_house_no'] && formdata['add_house_no'].length < 2)) 
                formdata['add_house_no'] = '--';
            
            if (!formdata['add_street_name'] || 
                (formdata['add_street_name'] && formdata['add_street_name'].length < 2)) 
                formdata['add_street_name'] = '--';
            
            if (!formdata['add_city'] || 
                (formdata['add_city'] && formdata['add_city'].length < 2)) 
                formdata['add_city'] = '--';
            
            return formdata;
        },
        engineLengthValidation: function(data, error) {
            return engineChassisLengthValidation(data, error, "Engine", 5);
        },
        chassisLengthValidation: function(data, error) {
            return engineChassisLengthValidation(data, error, "Chassis", 5);
        },
        NameValidation_bharti: function(data, error){
            if (data && data.length<2){
                error("Name should be of at least 2 characters");
                return false;
            }
            error("");
            return true;
        }
    }

    function engineChassisLengthValidation(data, error, fieldName, minLength){
        if(data !== undefined){
            var regAsterisk = /\*/g;
            var regSpecialChar = /[^\w\s\*]/;
            var matched = data.replace(regAsterisk, '').length >= parseInt(minLength);
            var errorMessage = fieldName + " Number should be at least "+minLength+" characters long.";

            if(data.length == 0){
                errorMessage = fieldName + " Number cannot be left blank.";
            }
            else if(regSpecialChar.test(data)){
                errorMessage = fieldName + " Number can only contain alphabets and numbers.";
            }
            else if (!matched){ 
                errorMessage = fieldName + " Number should be at least "+minLength+" characters long";
                errorMessage += regAsterisk.test(data)?" excluding asterisk (*).":".";
            }
            else{ errorMessage=""; }

            if (errorMessage.length ==0) return true;
        }
        else{
            errorMessage = fieldName + " Number cannot be left blank.";
        }
        error(errorMessage);
        return false;
    }

    return {
        extraMethods: extraMethods,
    }
});