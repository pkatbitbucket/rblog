define(['knockout'], function(ko) {

    var extraMethods = {
        cleanInsurerForm: function (form, plan) {
            if (data.quote_parameters.extra_user_dob) {
                form.getFieldById('dob').visible(false);
            }

            form.getFieldById('add_city').visible(true);
            form.getFieldById('reg_add_city').visible(true);
            form.getFieldById('add_pincode').dependents = [];
            form.getFieldById('reg_add_pincode').dependents = [];

            form.getFieldById('location').visible(false);
            form.getFieldById('reg_location').visible(false);

            form.getFieldById('location_state_city').visible(true);
            form.getFieldById('reg_location_state_city').visible(true);

            form.getFieldById('is_car_financed').dependents = ['financier-name-text'];

            // Update max length
            form.getFieldById('add_building_name').maxlength(30);
            form.getFieldById('add_street_name').maxlength(30);
            form.getFieldById('add_landmark').maxlength(30);

            form.getFieldById('reg_add_building_name').maxlength(30);
            form.getFieldById('reg_add_street_name').maxlength(30);
            form.getFieldById('reg_add_landmark').maxlength(30);

            if (plan.calculatedTotalPremium() < 50000) {
                form.getFieldById('pan').visible(false);
            }
        },
        cleanInsurerFormData: function(formdata, plan) {
            if (data.quote_parameters.extra_user_dob) {
                formdata['cust_dob'] = data.quote_parameters.extra_user_dob;
            }

            formdata['add_state'] = formdata['loc_add_state'];
            formdata['add_district'] = '';
            formdata['add_city'] = formdata['loc_add_city'];
            formdata['reg_add_state'] = formdata['loc_reg_add_state'] || formdata['loc_add_state'];
            formdata['reg_add_district'] = '';
            formdata['reg_add_city'] = formdata['loc_reg_add_city'] || formdata['loc_add_city'];
            formdata['car-financier'] = formdata['car-financier-text'];

            if (plan.calculatedTotalPremium() < 50000) {
                var lastNameInitial = formdata["cust_last_name"].charAt(0);
                var alphabetReg = /[a-z]/i;
                if(!alphabetReg.test(lastNameInitial)){
                    lastNameInitial = "A";
                }
                formdata['cust_pan'] = "AAAP" + lastNameInitial + "1234A";
            }

            delete formdata['car-financier-text'];
            delete formdata['loc_add_state'];
            delete formdata['loc_add_city'];
            delete formdata['loc_reg_add_state'];
            delete formdata['loc_reg_add_city'];
            return formdata;
        },
    }

    return {
        extraMethods: extraMethods,
    }
});
