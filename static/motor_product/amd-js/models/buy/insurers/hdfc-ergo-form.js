define(['knockout'], function(ko) {

    var extraMethods = {
        cleanInsurerForm: function(form, plan) {
            if (data.quote_parameters.extra_user_dob) {
                form.getFieldById('dob').visible(false);
            }

            if (plan.calculatedTotalPremium() < 50000) {
                form.getFieldById('pan').visible(false);
            }

            form.getFieldById('add_city').visible(true);
            form.getFieldById('reg_add_city').visible(true);
            form.getFieldById('financier-details').visible(false);
            
            form.getFieldById('add_pincode').dependents = [];
            form.getFieldById('reg_add_pincode').dependents = [];

            form.getFieldById('location').visible(false);
            form.getFieldById('reg_location').visible(false);

            form.getFieldById('location_state_city').visible(true);
            form.getFieldById('reg_location_state_city').visible(true);
        },
        cleanInsurerFormData: function(formdata, plan) {
            formdata['is_car_financed'] = 0;
            
            if (data.quote_parameters.extra_user_dob) {
                formdata['cust_dob'] = data.quote_parameters.extra_user_dob;
            }

            if (plan.calculatedTotalPremium() < 50000) {
                var lastNameInitial = formdata["cust_last_name"].charAt(0);
                var alphabetReg = /[a-z]/i;
                if(!alphabetReg.test(lastNameInitial)){
                    lastNameInitial = "A";
                }
                formdata['cust_pan'] = "AAAP" + lastNameInitial + "1234A";
            }

            formdata['add_state'] = formdata['loc_add_state'];
            formdata['add_district'] = '';
            formdata['add_city'] = formdata['loc_add_city'];
            formdata['reg_add_state'] = formdata['loc_reg_add_state'] || formdata['loc_add_state'];
            formdata['reg_add_district'] = '';
            formdata['reg_add_city'] = formdata['loc_reg_add_city'] || formdata['loc_add_city'];

            delete formdata['loc_add_state'];
            delete formdata['loc_add_city'];
            delete formdata['loc_reg_add_state'];
            delete formdata['loc_reg_add_city'];

            return formdata;
        },
        engineLengthValidation: function(data, error) {
            return engineChassisLengthValidation(data, error,"Engine", 5);
        },
        chassisLengthValidation: function(data, error) {
            return engineChassisLengthValidation(data, error, "Chassis",5);
        },
    }

    function engineChassisLengthValidation(data, error, fieldName, minLength){
        if(data !== undefined){
            var regAsterisk = /\*/g;
            var regSpecialChar = /[^\w\s\*]/;
            var matched = data.replace(regAsterisk, '').length >= parseInt(minLength);
            var errorMessage = fieldName + " Number should be at least "+minLength+" characters long.";

            if(data.length == 0){
                errorMessage = fieldName + " Number cannot be left blank.";
            }
            else if(regSpecialChar.test(data)){
                errorMessage = fieldName + " Number can only contain alphabets and numbers.";
            }
            else if (!matched){ 
                errorMessage = fieldName + " Number should be at least "+minLength+" characters long";
                errorMessage += regAsterisk.test(data)?" excluding asterisk (*).":".";
            }
            else{ errorMessage=""; }

            if (errorMessage.length ==0) return true;
        }
        else{
            errorMessage = fieldName + " Number cannot be left blank.";
        }
        error(errorMessage);
        return false;
    }

    return {
        extraMethods: extraMethods,
    }
});