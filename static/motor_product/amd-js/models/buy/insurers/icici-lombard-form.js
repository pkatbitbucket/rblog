define(['knockout'], function(ko) {

    var extraMethods = {
        cleanInsurerForm: function (form, plan) {
            if (data.quote_parameters.extra_user_dob) {
                form.getFieldById('dob').visible(false);
            }

            if (data.quote_parameters.extra_isMemberOfAutoAssociation == '1') {
                form.getFieldById('aan').visible(true);
            }

            form.getFieldById('add-same').visible(false);

            form.getFieldById('address_extra_line').visible(true);
            form.getFieldById('address_extra_line').Name('City');
            form.getFieldById('add_extra_line').title('City');

            form.getFieldById('add_pincode').dependents = [];
            form.getFieldById('reg_add_pincode').dependents = [];

            form.getFieldById('location_state_city').Name('State');
            form.getFieldById('loc_add_state').dependents = [];
            form.getFieldById('location_state_city').visible(true);

            if (plan.calculatedTotalPremium() < 50000) {
                form.getFieldById('pan').visible(false);
            }
        },
        cleanInsurerFormData: function(formdata, plan) {
            if (data.quote_parameters.extra_user_dob) {
                formdata['cust_dob'] = data.quote_parameters.extra_user_dob;
            }

            formdata['add_city'] = formdata['add_extra_line'];
            formdata['cust_occupation'] = '';
            formdata['add_state'] = formdata['loc_add_state'];
            formdata['add_district'] = '';

            if (plan.calculatedTotalPremium() < 50000) {
                var lastNameInitial = formdata["cust_last_name"].charAt(0);
                var alphabetReg = /[a-z]/i;
                if(!alphabetReg.test(lastNameInitial)){
                    lastNameInitial = "A";
                }
                formdata['cust_pan'] = "AAAP" + lastNameInitial + "1234A";
            }

            delete formdata['add_extra_line'];
            delete formdata['loc_add_state'];
            return formdata;
        },
    }

    return {
        extraMethods: extraMethods,
    }
});