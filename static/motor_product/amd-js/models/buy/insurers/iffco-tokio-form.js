define(['knockout'], function(ko) {

    var extraMethods = {
        cleanInsurerForm: function(form, plan) {
            if (data.quote_parameters.extra_user_dob) {
                form.getFieldById('dob').visible(false);
            }

            if (data.quote_parameters.extra_isMemberOfAutoAssociation == '1') {
                form.getFieldById('aan').visible(true);
            }
            
            form.getFieldById('add_city').visible(true);
            form.getFieldById('reg_add_city').visible(true);
            form.getFieldById('financier-details').visible(false);
            form.getFieldById('add_landmark').maxlength(30);
            form.getFieldById('add_street_name').maxlength(30);
            form.getFieldById('add_building_name').maxlength(30);
            form.getFieldById('add_house_no').maxlength(30);

            form.getFieldById('add_pincode').dependents = [];
            form.getFieldById('reg_add_pincode').dependents = [];

            form.getFieldById('location').visible(false);
            form.getFieldById('reg_location').visible(false);

            form.getFieldById('location_state_city').visible(true);
            form.getFieldById('reg_location_state_city').visible(true);

            if (plan.calculatedTotalPremium() < 50000) {
                form.getFieldById('pan').visible(false);
            }
        },
        cleanInsurerFormData: function(formdata, plan) {
            if (data.quote_parameters.extra_user_dob) {
                formdata['cust_dob'] = data.quote_parameters.extra_user_dob;
            }

            formdata['is_car_financed'] = 0;
            if (formdata['vehicle_reg_no']) {
                var reg_no_parts = formdata['vehicle_reg_no'].split('-');
                if (reg_no_parts[2].length == 3) {
                    reg_no_parts[1] = reg_no_parts[1] + reg_no_parts[2].substr(0, 1);
                    reg_no_parts[2] = reg_no_parts[2].substr(1);
                }
                var reg_no = reg_no_parts.join('-');
                formdata['vehicle_reg_no'] = reg_no;
            }

            formdata['add_state'] = formdata['loc_add_state'];
            formdata['add_district'] = '';
            formdata['add_city'] = formdata['loc_add_city'];
            formdata['reg_add_state'] = formdata['loc_reg_add_state'] || formdata['loc_add_state'];
            formdata['reg_add_district'] = '';
            formdata['reg_add_city'] = formdata['loc_reg_add_city'] || formdata['loc_add_city'];

            if (plan.calculatedTotalPremium() < 50000) {
                var lastNameInitial = formdata["cust_last_name"].charAt(0);
                var alphabetReg = /[a-z]/i;
                if(!alphabetReg.test(lastNameInitial)){
                    lastNameInitial = "A";
                }
                formdata['cust_pan'] = "AAAP" + lastNameInitial + "1234A";
            }

            delete formdata['loc_add_state'];
            delete formdata['loc_add_city'];
            delete formdata['loc_reg_add_state'];
            delete formdata['loc_reg_add_city'];
            return formdata;
        },
    }

    return {
        extraMethods: extraMethods,
    }
});