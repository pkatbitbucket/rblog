define(['knockout'], function(ko) {

    var extraMethods = {
        cleanInsurerForm: function(form) {
            if (data.quote_parameters.extra_user_dob) {
                form.getFieldById('dob').visible(false);
            }

            form.getFieldById('add_city').visible(true);
            form.getFieldById('reg_add_city').visible(true);
            form.getFieldById('pan').visible(false);
            form.getFieldById('add_district').hideContainer(true);
            form.getFieldById('reg_add_district').hideContainer(true);
        },
        cleanInsurerFormData: function(formdata) {
            if (data.quote_parameters.extra_user_dob) {
                formdata['cust_dob'] = data.quote_parameters.extra_user_dob;
            }

            formdata['cust_pan'] = '';
            delete formdata['car-financier-text'];
            return formdata;
        },
        engineLengthValidation: function(data, error) {
            return engineChassisLengthValidation(data, error,"Engine", 7);
        },
        chassisLengthValidation: function(data, error) {
            return engineChassisLengthValidation(data, error,"Chassis", 7);
        },
    }

    function engineChassisLengthValidation(data, error, fieldName, minLength){
        if(data !== undefined){
            var regAsterisk = /\*/g;
            var regSpecialChar = /[^\w\s\*]/;
            var matched = data.replace(regAsterisk, '').length >= parseInt(minLength);
            var errorMessage = fieldName + " Number should be at least "+minLength+" characters long.";

            if(data.length == 0){
                errorMessage = fieldName + " Number cannot be left blank.";
            }
            else if(regSpecialChar.test(data)){
                errorMessage = fieldName + " Number can only contain alphabets and numbers.";
            }
            else if (!matched){
                errorMessage = fieldName + " Number should be at least "+minLength+" characters long";
                errorMessage += regAsterisk.test(data)?" excluding asterisk (*).":".";
            }
            else{ errorMessage=""; }

            if (errorMessage.length ==0) return true;
        }
        else{
            errorMessage = fieldName + " Number cannot be left blank.";
        }
        error(errorMessage);
        return false;
    }

    return {
        extraMethods: extraMethods,
    }
});
