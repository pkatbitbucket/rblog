define(['knockout'], function(ko) {

    var extraMethods = {
        cleanInsurerForm: function (form) {
            if (data.quote_parameters.extra_user_dob) {
                form.getFieldById('dob').visible(false);
            }

            form.getFieldById('is_car_financed').dependents = ['financier-name-text'];

            form.getFieldById('pan').visible(false);
            form.getFieldById('add-same').visible(false);

            form.getFieldById('add_pincode').dependents = [];
            form.getFieldById('reg_add_pincode').dependents = [];

            form.getFieldById('location').visible(false);
            form.getFieldById('location_state_city').visible(true);

            form.getFieldById('accept_terms_block').visible(true);
            var termsHtml = 'I/We have read and agree to the policy <a href="' + window.location.origin + '/static/motor_product/documents/liberty_tc.pdf" target="_blank">terms and conditions.</a>'
            form.getFieldById('accept_terms').title(termsHtml);
        },
        cleanInsurerFormData: function(formdata) {
            if (data.quote_parameters.extra_user_dob) {
                formdata['cust_dob'] = data.quote_parameters.extra_user_dob;
            }

            formdata['cust_pan'] = 'AAAP'+formdata['cust_last_name'][0].toUpperCase()+'1234A';
            formdata['add-same'] = 1;
            formdata['cust_occupation'] = '';

            formdata['add_state'] = formdata['loc_add_state'];
            formdata['add_district'] = '';
            formdata['add_city'] = formdata['loc_add_city'];

            formdata['car-financier'] = formdata['car-financier-text'];

            delete formdata['car-financier-text'];
            delete formdata['loc_add_state'];
            delete formdata['loc_add_city'];
            delete formdata['accept_terms'];
            return formdata;
        },
    }

    return {
        extraMethods: extraMethods,
    }
});
