define(['knockout'], function(ko) {

    var extraMethods = {
        cleanInsurerForm: function (form) {
            if (data.quote_parameters.extra_user_dob) {
                form.getFieldById('dob').visible(false);
            }

            //form.getFieldById('pan').visible(false);
            form.getFieldById('financier-details').visible(false);
            //form.getFieldById('nominee').visible(false);

            form.getFieldById('add_building_name').visible(false);
            form.getFieldById('address_extra_line').visible(true);

            form.getFieldById('house').Name('Address Line 1');
            form.getFieldById('street').Name('Address Line 2');

            form.getFieldById('add_street_name').title('Address');
            form.getFieldById('add_landmark').visible(false);

            form.getFieldById('add_house_no').maxlength(30);
            form.getFieldById('add_street_name').maxlength(30);
            form.getFieldById('add_extra_line').maxlength(30);

            form.getFieldById('add_house_no').title('Address');
            form.getFieldById('add_pincode').dependents = [];
            form.getFieldById('add-same').visible(false);
        },
        cleanInsurerFormData: function(formdata) {
            if (data.quote_parameters.extra_user_dob) {
                formdata['cust_dob'] = data.quote_parameters.extra_user_dob;
            }

            formdata['cust_occupation'] = '';
            formdata['is_car_financed'] = 0;
            formdata['add_building_name'] = '';
            formdata['add_landmark'] = formdata['add_extra_line'];
            formdata['add_state'] = '';
            formdata['add_district'] = '';

            delete formdata['add_extra_line'];
            return formdata;
        },
    }

    return {
        extraMethods: extraMethods,
    }
});
