define(['knockout'], function(ko) {

    var extraMethods = {
        cleanInsurerForm: function (form) {
            if (data.quote_parameters.extra_user_dob) {
                form.getFieldById('dob').visible(false);
            }

            // if (data.quote_parameters.extra_isMemberOfAutoAssociation == '1') {
            //     form.getFieldById('aan').visible(true);
            // }

            form.getFieldById('is_car_financed').dependents = ['financier-name-text'];

            form.getFieldById('pan').visible(false);

            form.getFieldById('add_pincode').dependents = [];
            form.getFieldById('reg_add_pincode').dependents = [];
            form.getFieldById('add-same').visible(false);

            form.getFieldById('location').visible(false);
            form.getFieldById('reg_location').visible(false);

            form.getFieldById('location_state_city').visible(true);
            form.getFieldById('reg_location_state_city').visible(true);
        },
        cleanInsurerFormData: function(formdata) {
            if (data.quote_parameters.extra_user_dob) {
                formdata['cust_dob'] = data.quote_parameters.extra_user_dob;
            }

            formdata['car-financier'] = formdata['car-financier-text'];
            formdata['cust_pan'] = '';
            formdata['cust_occupation'] = '';

            formdata['add_state'] = formdata['loc_add_state'];
            formdata['add_district'] = '';
            formdata['add_city'] = formdata['loc_add_city'];
            formdata['reg_add_state'] = formdata['loc_reg_add_state'];
            formdata['reg_add_district'] = '';
            formdata['reg_add_city'] = formdata['loc_reg_add_city'];

            delete formdata['car-financier-text'];
            delete formdata['loc_add_state'];
            delete formdata['loc_add_city'];
            delete formdata['loc_reg_add_state'];
            delete formdata['loc_reg_add_city'];
            return formdata;
        },
        engineLengthValidation: function(data, error) {
            return engineChassisLengthValidation(data, error,"Engine", 6);
        },
        chassisLengthValidation: function(data, error) {
            return engineChassisLengthValidation(data, error,"Chassis", 6);
        },
    }

    function engineChassisLengthValidation(data, error, fieldName, minLength){
        if(data !== undefined){
            var regAsterisk = /\*/g;
            var regSpecialChar = /[^\w\s\*]/;
            var matched = data.replace(regAsterisk, '').length >= parseInt(minLength);
            var errorMessage = fieldName + " Number should be at least "+minLength+" characters long.";

            if(data.length == 0){
                errorMessage = fieldName + " Number cannot be left blank.";
            }
            else if(regSpecialChar.test(data)){
                errorMessage = fieldName + " Number can only contain alphabets and numbers.";
            }
            else if (!matched){
                errorMessage = fieldName + " Number should be at least "+minLength+" characters long";
                errorMessage += regAsterisk.test(data)?" excluding asterisk (*).":".";
            }
            else{ errorMessage=""; }

            if (errorMessage.length ==0) return true;
        }
        else{
            errorMessage = fieldName + " Number cannot be left blank.";
        }
        error(errorMessage);
        return false;
    }

    return {
        extraMethods: extraMethods,
    }
});
