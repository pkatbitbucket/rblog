define(['knockout'], function(ko) {

    var extraMethods = {
        cleanInsurerForm: function (form, plan) {
            form.getFieldById('add_city').visible(true);
            form.getFieldById('reg_add_city').visible(true);
            form.getFieldById('add_pincode').dependents = [];
            form.getFieldById('reg_add_pincode').dependents = [];

            form.getFieldById('location').visible(false);
            form.getFieldById('reg_location').visible(false);
            // form.getFieldById('nominee').visible(false);

            form.getFieldById('location_state_city').visible(true);
            form.getFieldById('reg_location_state_city').visible(true);

            if (plan.calculatedTotalPremium() < 50000) {
                form.getFieldById('pan').visible(false);
            }
        },
        cleanInsurerFormData: function(formdata, plan) {
            formdata['add_state'] = formdata['loc_add_state'];
            formdata['add_district'] = '';
            formdata['add_city'] = formdata['loc_add_city'];
            formdata['reg_add_state'] = formdata['loc_reg_add_state'] || formdata['loc_add_state'];
            formdata['reg_add_district'] = '';
            formdata['reg_add_city'] = formdata['loc_reg_add_city'] || formdata['loc_add_city'];
            // formdata['nominee_name'] = '';
            // formdata['nominee_age'] = '';
            // formdata['nominee_relationship'] = '';

            if (plan.calculatedTotalPremium() < 50000) {
                var lastNameInitial = formdata["cust_last_name"].charAt(0);
                var alphabetReg = /[a-z]/i;
                if(!alphabetReg.test(lastNameInitial)){
                    lastNameInitial = "A";
                }
                formdata['cust_pan'] = "AAAP" + lastNameInitial + "1234A";
            }

            delete formdata['loc_add_state'];
            delete formdata['loc_add_city'];
            delete formdata['loc_reg_add_state'];
            delete formdata['loc_reg_add_city'];
            return formdata;
        },
        validateAddressReliance:function(data, error){
            if(data.length >35){
                error("Address should be less than 35 characters");
                return false;
            }
            return true;
        },
        validatePolicyNumberLength: function(data, error){
            if(data && data.length <8){
                error("Policy number must be at least 8 characters.");;
                return false;
            }
            return true;
        }
    }

    return {
        extraMethods: extraMethods,
    }
});
