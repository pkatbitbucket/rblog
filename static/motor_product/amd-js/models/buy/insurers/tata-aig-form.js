define(['knockout'], function(ko) {

    var extraMethods = {
        cleanInsurerForm: function (form, plan) {
            if (data.quote_parameters.extra_user_dob) {
                form.getFieldById('dob').visible(false);
            }

            if (data.quote_parameters.isClaimedLastYear == '1') {
                form.getFieldById('pyp-claims').visible(true);
                form.getFieldById('pyp-claims-amount').visible(true);
            }

            form.getFieldById('accept_terms_block').visible(true);
            var termsHtml = 'I / We declare that the information provided in this application is correct. I / We have read, understood and accept the <a href="' + window.location.origin + '/static/motor_product/documents/terms_and_conditions.pdf" target="_blank">terms and conditions</a> for this purchase. I / We confirm compliance to AML / KYC guidelines.'
            form.getFieldById('accept_terms').title(termsHtml);

            if (plan.calculatedTotalPremium() < 50000) {
                form.getFieldById('pan').visible(false);
            }

            if(data.quote_parameters.isClaimedLastYear === '1'){
                form.getFieldById('pyp-claims').visible(false);
                form.getFieldById('pyp-claims-amount').visible(false);
            }
        },
        cleanInsurerFormData: function(formdata, plan) {
            if (data.quote_parameters.extra_user_dob) {
                formdata['cust_dob'] = data.quote_parameters.extra_user_dob;
            }

            if (plan.calculatedTotalPremium() < 50000) {
                var lastNameInitial = formdata["cust_last_name"].charAt(0);
                var alphabetReg = /[a-z]/i;
                if(!alphabetReg.test(lastNameInitial)){
                    lastNameInitial = "A";
                }
                formdata['cust_pan'] = "AAAP" + lastNameInitial + "1234A";
            }

            // if claimed in past year, set default values:
            // No. of Claims: 1
            // Claim amount: 15000
            if(data.quote_parameters.isClaimedLastYear === '1'){
                formdata["past_policy_claims"] = 1;
                formdata["past_policy_claims_amount"] = 15000
            }

            delete formdata['accept_terms'];
            return formdata;
        },
    }

    return {
        extraMethods: extraMethods,
    }
});