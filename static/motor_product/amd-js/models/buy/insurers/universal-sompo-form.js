define(['knockout'], function(ko) {

    var extraMethods = {
        cleanInsurerForm: function(form) {
            if (data.quote_parameters.extra_user_dob) {
                form.getFieldById('dob').visible(false);
            }

            if (data.quote_parameters.extra_isMemberOfAutoAssociation == '1') {
                form.getFieldById('aan').visible(true);
            }

            form.getFieldById('is_car_financed').dependents = ['financier-name-text'];
            
            form.getFieldById('add-same').visible(false);
            form.getFieldById('pan').visible(false);

            form.getFieldById('add_pincode').dependents = [];
            form.getFieldById('reg_add_pincode').dependents = [];

            form.getFieldById('location').visible(false);
            form.getFieldById('location_state_city').visible(true);

            form.getFieldById('add_landmark').maxlength(49);
            form.getFieldById('reg_add_landmark').maxlength(49);
            
        },
        cleanInsurerFormData: function(formdata) {
            if (data.quote_parameters.extra_user_dob) {
                formdata['cust_dob'] = data.quote_parameters.extra_user_dob;
            }

            formdata['cust_occupation'] = '';
            formdata['add-same'] = 1;
            formdata['cust_pan'] = '';

            formdata['add_state'] = formdata['loc_add_state'];
            formdata['add_district'] = '';
            formdata['add_city'] = formdata['loc_add_city'];

            formdata['car-financier'] = formdata['car-financier-text'];

            delete formdata['car-financier-text'];
            delete formdata['loc_add_state'];
            delete formdata['loc_add_city'];
            return formdata;
        },
    }

    return {
        extraMethods: extraMethods,
    }
});