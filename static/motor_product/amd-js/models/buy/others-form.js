define(['knockout'], function(ko) {

    var extraMethods = {
        cleanInsurerForm: function (form) {
            if (data.quote_parameters.extra_user_dob) {
                form.getFieldById('dob').visible(false);
            }
        },
        cleanInsurerFormData: function(formdata) {
            if (data.quote_parameters.extra_user_dob) {
                formdata['cust_dob'] = data.quote_parameters.extra_user_dob;
            }
            return formdata;
        },
    }

    return {
        extraMethods: extraMethods,
    }
});