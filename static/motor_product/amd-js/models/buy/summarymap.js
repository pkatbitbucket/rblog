define([], function() {


	map = {
        'cust_first_name': 'First Name',
        'cust_last_name': 'Last Name',
        'cust_gender': 'Gender',
        'cust_phone': 'Mobile',
        'cust_dob': 'Date of Birth',
        'cust_occupation': 'Occupation',
        'cust_email': 'Email',
        'cust_pan': 'PAN Number',
        'cust_marital_status': 'Married',
        'nominee_name': 'Nominee Name',
        'nominee_age': 'Nominee Age',
        'nominee_relationship': 'Nominee Relationship',

        'vehicle_reg_no': 'Registration Number',
        'vehicle_rto': 'RTO',
        'vehicle_reg_date': 'Registration Date',
        'vehicle_engine_no': 'Engine Number',
        'vehicle_chassis_no': 'Chassis Number',
        'past_policy_insurer': 'Previous policy Insurer',
        'past_policy_number': 'Previous policy Number',
        'is_car_financed': '1',
        'car-financier': 'Financier',
        'car-financier-text': 'Financier',

        'add_house_no': 'House Number',
        'add_building_name': 'Building Name',
        'add_street_name': 'Street Name',
        'add_landmark': 'Landmark',
        'add_pincode': 'Pincode',
        'add_state': 'State',
        'add_district': 'District',
        'add_city': 'City',
        'loc_add_state': 'State',
        'loc_add_district': 'District',
        'loc_add_city': 'City',
        'add-same': '',
        'reg_add_house_no': 'House Number',
        'reg_add_building_name': 'Building Name',
        'reg_add_street_name': 'Street Name',
        'reg_add_landmark': 'Landmark',
        'reg_add_pincode': 'Pincode',
        'reg_add_state': 'State',
        'reg_add_district': 'District',
        'reg_add_city': 'City',
    }


    var render = function(fields){

    }

	return {
		map:map,
		render:render
    }
});