define(["knockout", "crypt", "viewmodels","components", "configuration"], 
	function(ko, crypt, viewmodels,components, settings){
		var VEHICLE_TYPE = "fourwheeler";
		var POLICY_TYPE = "Renew";
		var isAdmin = false;
		var quote = viewmodels.quote;
		var fastlane = {
			isProcessing: ko.observable(false),
			data: ko.observableArray([]),
			isSuccess:ko.observable(true)
		}

		var variant_list = ko.observableArray([]);
        var vehicle_list = ko.observableArray([]);
        var filtered_variants_list = ko.observableArray([]);
        var RTO_list = ko.observableArray([]);
        var filtered_variants_list = ko.observableArray([]);
        var fuel_type_list = ko.observableArray(settings.getConfiguration().fuel_type_list);

		var registrationNumber = {
			initialValue: ko.observable(),
			selectedValue: ko.observable(),
			enableState:true,
			enableSeries: true
		}

		registrationNumber.initialValue("mh14ck2127");

		registrationNumber.selectedValue.subscribe(function(newValue){
			if(newValue){
				getFastlaneData(newValue);
			}
		});

		fastlane.data.subscribe(function(newValue){
			if(newValue){
				fastlane.isProcessing(true);
				fastlane.isSuccess(false);
			}
		})

		var policyExpired = ko.observable();

		function setOld(){
			POLICY_TYPE = "Renew";
		}

		function setNew(){
			POLICY_TYPE = "New";
		}

		function setExpired(){
			POLICY_TYPE = "Expired";
		}

		function setUsed(){
			POLICY_TYPE = "Used";
		}

		function vehicle_type_verbose(){
			return VEHICLE_TYPE == "fourwheeler"?"Car":"Bike";
		}

		function checkForErrors(){
			return  true;
		}

		function isStepValid(){
			return true;
		}

		function goToResults(){

		}

		function getFastlaneData(regNo){
			var url = '/motor/' + VEHICLE_TYPE + '/api/vehicle-info/';
			var data = { "registration_number":regNo };
            var isCrypted = false;
            var result = {};

            fastlane.isProcessing(true);
            getData(url, data, isCrypted, fastlane.data);
		}

		function getRtoList(){
			var url = '/motor/' + VEHICLE_TYPE + '/api/rtos/';
			var data = {};
			var isCrypted = true;
			getData(url, data, isCrypted, RTO_list);
		}

		function getVehicleList(){
			var url = '/motor/' + VEHICLE_TYPE + '/api/vehicles/';
			var data = {};
			var isCrypted = true;
			getData(url, data, isCrypted, vehicle_list);
		}

		function getVariantList(){
			var url = '/motor/' + VEHICLE_TYPE + '/api/vehicles/' + quote.vehicle().id + '/variants/';
			var data = { }
			getData(url, data, isCrypted, variant_list);
		}

		function setVariantList(){
	        if(quote.fuel_type()){
	            var selected_fuel_type_id = quote.fuel_type().id;
	            var filtered_variants = variant_list().filter(function(item){
	                return item.fuel_type == selected_fuel_type_id
	            });
	            filtered_variants_list(filtered_variants);
	        }
	    }


		function getData(url, data, isCrypted, returnVariable){
			data.csrfmiddlewaretoken = CSRF_TOKEN
			$.post(url,data, function(response) {
                if(isCrypted){
                	response = crypt.parseResponse(response);
                }
                if(typeof(response) == "string")
                	returnVariable(JSON.parse(response));
                else 
                	returnVariable(response);
                console.log(returnVariable);
            });
		}

		function initialize(){
			getVehicleList();
			getRtoList();
		}

		(function(){
			initialize();
		})()

		return{
			"quote":quote,
			"vehicle_type_verbose":vehicle_type_verbose,
			"setNew":setNew,
			"setOld":setOld,
			"setExpired":setExpired,
			"setUsed":setUsed,
			"policyExpired":policyExpired,
			"isAdmin":isAdmin,
			"checkForErrors":checkForErrors,
			"isStepValid":isStepValid,
			"goToResults":goToResults,
			"registrationNumber":registrationNumber,
			"variant_list": variant_list,
            "fuel_type_list": fuel_type_list,
            "vehicle_list": vehicle_list,
            "getVariantList": getVariantList,
            "setVariantList":setVariantList,
            "RTO_LIST": RTO_list,
            "filtered_variants_list":filtered_variants_list,

            // viewmodel variables
            "availableRegYear": viewmodels.years,
            "days": viewmodels.days,
            "expiryYears": viewmodels.expiryYears,
            "twExpiryYears": viewmodels.twExpiryYears,
            "months": viewmodels.months,
            "fastlane":fastlane
		}
	}
);