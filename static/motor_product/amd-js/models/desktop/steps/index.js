define(['knockout', 'models', 'viewmodels', 'utils', 'crypt','cookie', 'configuration', 'events', 'actions','components'], function(ko, models, viewmodels, utils, crypt, Cookies, settings, events, actions) {

    //  ../../../../global/mixpanel-actions = mixpanelActions

    var $ = require('npm-zepto');

    // Special Affiliate Case
    var affiliate_campaign = AFFILIATE;

    var vehicle_type_verbose = {
        fourwheeler: 'Car',
        twowheeler: 'Bike'
    };

    function ValidateEmail(data){
        var reg = /^[a-zA-Z0-9.!#$%&'+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9]{2}(?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)$/;
        return reg.test(data);
    }

    function ValidateMobile(data){
        var reg = /[7-9][\d]{9}/;
        var valid = false;
        if(data.length == 0){
            valid = true;
        }
        else {
            valid = reg.test(data);
        }

        return valid;
    }

    function validateExpiryDate(){
        if(quote.expiry_date() && quote.expiry_month() && quote.expiry_year()){
            var monthNumber = utils.monthNametoNumber(quote.expiry_month());
            var _d = new Date(quote.expiry_year(), monthNumber, quote.expiry_date());
            var today = new Date();

            _d.setHours(0,0,0,0);
            today.setHours(0,0,0,0);

            if(_d.getDate() !== parseInt(quote.expiry_date())){
                return false;
            }
            else if(+_d< +today){
                return true;
            }
        }
        return false;
    }

    utils.show_chat_box();

    var policyExpired = ko.observable(false).extend({
        store: {
            key: 'policyExpired'
        }
    });

    var setPolicyTypeBasedOnQuote = function(){
        var policyDate = quote.pastPolicyExpiryDate();
        if(policyDate){
            var arr = policyDate.split('-');
            var _d = new Date(arr[2], arr[1]-1, arr[0]);
            if(_d< new Date()){
                policyExpired(true);
            }
        }
    }

    var quote = viewmodels.quote,
        variant_list = ko.observableArray([]),
        vehicle_list = ko.observableArray([]),
        filtered_variants_list = ko.observableArray([]),
        moreThan90days = ko.observable(false),
        expiredFor90days = ko.computed(function(){
            var today = new Date();
            var month = utils.monthNametoNumber(quote.expiry_month());
            var date = new Date(quote.expiry_year(), month, quote.expiry_date());

            date.setHours(0,0,0,0);
            today.setHours(0,0,0,0);

            var _diff = utils.getDaysBetweenDates(date, today);
            var result = (_diff>=90);
            return result;
        }), 
        RTO_list = ko.observableArray([]),
        fuel_type_list = ko.observableArray(settings.getConfiguration().fuel_type_list),
        prev_fuel_type = ko.observable(quote.fuel_type());

    if(!quote.isUsedVehicle()){
        quote.isUsedVehicle(false);
    }

    setPolicyTypeBasedOnQuote();

    var cngKitValueValid = ko.computed(function(){

        var valid = false;
        var reg = /^[1-9]\d*$/;
        if(reg.test(quote.cngKitValue())){
            if(parseInt(quote.cngKitValue()) <= 40000){
                valid = true;
            }
        }

        return valid;


    });

    // Olark notification subscribers
    quote.vehicle.subscribe(function() {
        for (var key in quote) {
            if (key.indexOf('addon_') > -1) {
                quote[key](0);
            }
        }
        utils.motorOlark.sendStartPageNotification(quote);
    });

    quote.fuel_type.subscribe(function() {
        utils.motorOlark.sendStartPageNotification(quote);
        quote.variant("");
    });

    quote.variant.subscribe(function() {
        utils.motorOlark.sendStartPageNotification(quote);
    });

    quote.rto_info.subscribe(function() {
        utils.motorOlark.sendStartPageNotification(quote);
    });

    quote.reg_year.subscribe(function() {
        utils.motorOlark.sendStartPageNotification(quote);
    });

    var POLICY_TYPE = ko.computed(function(){
        if(quote.isNewVehicle())
            return "New";
        else if(quote.isUsedVehicle())
            return "Used";
        else{
            if(policyExpired()){
                return "Expired";
            }
            else{
                return "Renew";
            }
        }
    });

    var setNew = function(){
        quote.isNewVehicle(true);
        policyExpired(false);
        quote.isUsedVehicle(false);
    };

    var setOld = function(){
        quote.isNewVehicle(false);
        policyExpired(false);
        quote.isUsedVehicle(false);
    };

    var setUsed = function() {
        quote.isUsedVehicle(true);
        quote.isNewVehicle(false);
        policyExpired(false);
    };

    var setExpired = function() {
        policyExpired(true);
        quote.isUsedVehicle(false);
        quote.isNewVehicle(false);

        var date  = new Date();
        date.setDate(date.getDate()-1);
        quote.expiry_date(date.getDate());
        quote.expiry_month(utils.monthNumberToName(date.getMonth()));
        quote.expiry_year(date.getFullYear());
    };

    policyExpired.subscribe(function(new_value) {
        if (new_value) {
            quote.isNewVehicle(false);
            document.cookie ='lms_campaign=motor_offlinecases; path=/'
        }
    });

    function resetParameters() {
        var current = new Date(),
            current_year = current.getFullYear(),
            current_month = current.getMonth();

        var current_month_verbose = viewmodels.months[current_month];

        var man_date = new Date();
        man_date.setMonth(man_date.getMonth() - 2);

        var man_month = man_date.getMonth();
        var man_month_verbose = viewmodels.months[man_month];

        quote.idv(0);
        quote.previousNCB(null);
        quote.isClaimedLastYear(0);
        quote.voluntaryDeductible(0);
        quote.idvElectrical(0);
        quote.idvNonElectrical(0);
        quote.hasOptimizedQuotes(false);
        quote.hasAddedDiscounts(false);
        quote.extra_user_occupation(null);
        quote.extra_user_dob_date(null);
        quote.extra_user_dob_month(null);
        quote.extra_user_dob_year(null);
        quote.extra_isAntiTheftFitted(0);
        quote.extra_isMemberOfAutoAssociation(0);
        quote.extra_isTPPDDiscount(0);
        quote.reg_month(current_month_verbose);
        quote.reg_date('01');
        quote.man_month(man_month_verbose);
        quote.man_year(man_date.getFullYear());

        if (!policyExpired()) {
            quote.resetExpiryDate();
        }

        // Reset discount code
        if (window.location.hash != "#corporate") {
            quote.discountCode(null);
        }
    }

    function resetPolicyDate(){
        // Reset date parameters.
        var today = new Date();
        switch(POLICY_TYPE()){
            case "New": 
                today.setDate(today.getDate() + 1);
                quote.policy_start_date(today.getDate());
                quote.policy_start_month(utils.monthNumberToName(today.getMonth()));
                setPolicyDate(today);
                break;
            case "Renew": 
                today.setDate(today.getDate() + 7);
                setPolicyDate(today);
                moreThan90days(false);
                break;
            case "Expired":
                if(moreThan90days()){
                    today.setDate(today.getDate() - 100);
                    setPolicyDate(today)
                }
                break;
            case "Used":
                setPolicyDate(today);
                break;

        }
    }

    function setPolicyDate(today){
        quote.expiry_date(today.getDate());
        quote.expiry_month(utils.monthNumberToName(today.getMonth()));
        quote.expiry_year(today.getFullYear());
    }

    // validations
    var isRegistrationDateValid = ko.computed(function() {
        var valid = true;
        if (quote.registrationDate()) {
            var date = new Date(), // today
                firstDayOfPreviousMonth = utils.firstDayOfPreviousMonth(date),
                ndaysFromRegistration = utils.getDaysBetweenDates(utils.stringToDate(quote.registrationDate(), 'dd-mm-yyyy', '-'), firstDayOfPreviousMonth);
            if (ndaysFromRegistration < 180) {
                valid = false;
            }
        }
        return valid;
    });

    var isManufacturingDateValid = ko.computed(function() {
        var valid = true;

        if (quote.isNewVehicle()) {
            if (quote.manufacturingDate()) {
                var date = new Date(),
                    man_date = utils.stringToDate(quote.manufacturingDate(), 'dd-mm-yyyy', '-');
                if (man_date > date) {
                    valid = false;
                }
            }
        }
        return valid;
    });

    var checkForErrors = ko.observable(false);

    var isEmailValid = ko.computed(function(){
        var valid = true;
        if(quote.email() && quote.email() != ''){
            valid = ValidateEmail(quote.email());
        }
        return valid;
    });

    var isMobileValid = ko.computed(function(){
        var valid = true;
        if(quote.mobile() && quote.mobile() != ''){
            valid = ValidateMobile(quote.mobile());
        }
        return valid;
    });

    var isStepValid = ko.computed(function() {

        var valid = true;
        if (!quote.vehicle() || !quote.fuel_type() || !quote.variant() || !quote.rto_info() ) {
            valid = false;
        }

        if(quote.fuel_type() && quote.fuel_type().code && quote.fuel_type().code.toLowerCase().indexOf('external') > -1 && !cngKitValueValid()){
            valid = false;
        }

        if (!quote.isNewVehicle()) {
            if (!quote.reg_year()) {
                valid = false;
            }
        }

        if(!isMobileValid()){
            valid = false;
        };

        if(policyExpired() && !validateExpiryDate() && !moreThan90days()){
            quote.expiry_errorMessage("Policy expiry date should be past.");
            quote.expiry_error(true);
            valid = false;
        }
        else{
            quote.expiry_errorMessage("");
            quote.expiry_error(false);
        }

        return valid;
    });


    var resetFastlaneFlags = function(){
        quote.fastlane_success(0);
        quote.ncb_unknown(true);
        quote.formatted_reg_number("");
        quote.fastlane_model_changed(false);
        quote.fastlane_variant_changed(false);
        quote.fastlane_fueltype_changed(false);
    }

    function resetExpiredDate(){
        var _90day = new Date();
        _90day.setDate(_90day.getDate() - 90);
        _90day.setHours(0,0,0,0);
        var _diff = utils.getDaysBetweenDates(utils.utils.stringToDate(quote.pastPolicyExpiryDate()),_90day);
        if(_diff >90){
            moreThan90days(true);
        }

    }

    var goToResults = function() {
        resetFastlaneFlags();
        checkForErrors(true);
        if (isStepValid()) {
            resetParameters();
            resetPolicyDate();

            if(policyExpired())
                utils.setCookieForExpired();

            if (quote.mobile()) {
                var data = {
                    vehicle: quote.vehicle().name,
                    variant: quote.variant().name,
                    rto_info: quote.rto_info().name,
                    fuel_type: quote.fuel_type().name,
                    reg_year: quote.reg_year(),
                    quote: JSON.parse(ko.toJSON(quote))
                };

                if(policyExpired()){
                    data.lms_campaign = "motor-offlinecases";
                }

                utils.lmsUtils.sendGeneric(data, 'step1', quote.mobile(), {
                    "past_policy_expiry_date": quote.pastPolicyExpiryDate()
                }, function() {});
            }

            if (quote.email() && quote.email() != '') {
                var data = {
                    vehicle: quote.vehicle().name,
                    variant: quote.variant().name,
                    quote: JSON.parse(ko.toJSON(quote))
                };
                utils.lmsUtils.sendEmail(data, 'step1', quote.email(), {
                    "past_policy_expiry_date": quote.pastPolicyExpiryDate()
                }, function() {

                });
            }


            // MixPanel Actions

            // if(VEHICLE_TYPE == 'twowheeler'){
            //     events.cust_ev(mixpanelActions.BikeResultsReached, quote)
            // }else{
            //     if (quote.isNewVehicle()) {
            //         events.cust_ev(mixpanelActions.CarProductPageSubmittedNewPolicy, quote)
            //     }else{
            //         events.cust_ev(mixpanelActions.CarProductPageSubmittedRenewPolicy, quote)
            //     }
            // }


            events.cust_ev(actions.ViewQuotes, quote);

            ko.router.navigate('results', {
                trigger: true
            });
        }
    };

    var goToExpired = function() {
        var data = {};
        var vtv =  (VEHICLE_TYPE == 'fourwheeler' ? 'Car':'Bike');
        var new_verbose_params = { is_new: quote.isNewVehicle() , is_expired : policyExpired };

        data['param7'] = quote.email() && quote.email().length>0?"Email ID Given":"Email ID Not Given";

        events.gtm_push(vtv+'EventGetQuote', vtv+' - Get Quote', 'Expired '+vtv,'', data);

        // MixPanel Actions
        // events.cust_ev(mixpanelActions.ExpiredCarLPViewed, quote)

        var expired_url = '/lp/car-insurance/renew-expired-policy/';
        if (TPARTY) {
            expired_url += '?tparty=' + TPARTY;
        }
        window.location.href = expired_url;
    };

    var get_vehicles = function() {
        $.post('/motor/' + VEHICLE_TYPE + '/api/vehicles/', {
            'csrfmiddlewaretoken': CSRF_TOKEN
        }, function(response) {
            response = crypt.parseResponse(response);
            vehicle_list(response.data.models);
        });
    };

    var set_fuel_types_from_variants = function(){
        var variants = variant_list();
        var fuel_types = [];
        for (var i = variants.length - 1; i >= 0; i--) {
            var fuel_type = variants[i].fuel_type

            if(fuel_types.indexOf(fuel_type) == -1){
                fuel_types.push(fuel_type);
            }
        };

        var all_fuel_types = settings.getConfiguration().fuel_type_list;
        var new_fuel_types = []
        for (var i = 0; i < all_fuel_types.length; i++) {
            if(fuel_types.indexOf(all_fuel_types[i].id) > -1) {
                new_fuel_types.push(all_fuel_types[i]);
            }
        };

        fuel_type_list(new_fuel_types);

        if(quote.fuel_type() && prev_fuel_type() && $.grep(fuel_type_list(),function(d){return d.id === prev_fuel_type().id}).length>=1){
            quote.fuel_type(prev_fuel_type());
        }
        else{
            quote.fuel_type(fuel_type_list()[0]);
        }
        set_variants();
    }

    var set_variants = function(){
        if(quote.fuel_type()){
            var selected_fuel_type_id = quote.fuel_type().id;
            var filtered_variants = variant_list().filter(function(item){
                return item.fuel_type == selected_fuel_type_id
            });

            filtered_variants_list(filtered_variants);
        }
    }

    var get_variants = function(callback) {
	    var prev_fuel_type = quote.fuel_type();
        variant_list([]);
        quote.variant(null);
        $.post('/motor/' + VEHICLE_TYPE + '/api/vehicles/' + quote.vehicle().id + '/variants/', {
            'csrfmiddlewaretoken': CSRF_TOKEN
        }, function(response) {
            response = crypt.parseResponse(response);
            variant_list(response.data.variants);
            set_fuel_types_from_variants();
            if(callback)
                callback();
        });
    };

    var get_RTOs = function() {
        $.post('/motor/' + VEHICLE_TYPE + '/api/rtos/', {
            'csrfmiddlewaretoken': CSRF_TOKEN
        }, function(response) {
            response = crypt.parseResponse(response);
            RTO_list(response.data.rtos);

        });
    };

    quote.fuel_type.subscribe(function(new_value) {
        prev_fuel_type(new_value);
        if (new_value && new_value.code && new_value.code.indexOf('cng') > -1) {
            quote.isCNGFitted(1);
            if(new_value.code.indexOf('internal') > -1){
                quote.cngKitValue(0);
            }
        } else {
            quote.isCNGFitted(0);
            quote.cngKitValue(0);
        }
    });

    return {
        initialize: function(stage_type) {
            // ************************* REQUIRED ANALYTICS CODE - DO NOT DELETE *********************
            utils.gtm_data_log({
                'vizury_category': settings.getConfiguration().ga_category,
                'vizury_sub_category': settings.getConfiguration().vehicle_type
            });
            utils.gtm_event_log('vizury_step1');
            if (settings.getConfiguration().vehicle_type == 'fourwheeler'){
                utils.page_view('/vp/motor/step1/', 'VP-MOTOR-STEP1');
            }else{
                utils.page_view('/vp/bike/step1/', 'VP-BIKE-STEP1');
            }
            // ***************************************************************************************
            $('body,html').removeClass("recede hide-overflow");

            resetParameters();
            viewmodels.populateExpiredYears(true, true);

            if (quote.isNewVehicle() == null) {
                setOld();
            }

            if (!IS_ADMIN && quote.isUsedVehicle()) {
                setOld();
            }

            get_vehicles();
            get_RTOs();

            var variant = quote.variant();
            if (quote.vehicle()) {
                get_variants(function(){
                    quote.variant(variant);
                });
            }

            var _diff = utils.getDaysBetweenDates(utils.stringToDate(quote.pastPolicyExpiryDate(), 'dd-mm-yyyy', '-'), new Date());
            if(_diff>90){
                moreThan90days(true);
            }

            var CAR_TOLL_FREE = "";

            setTimeout(function() {
                $('.footer-innerwrap').addClass('reveal');
            }, 500);

            return {
                isAdmin: IS_ADMIN,
                setNew: setNew,
                setOld: setOld,
                setExpired: setExpired,
                setUsed: setUsed,
                policyExpired: policyExpired,
                quote: quote,
                months: viewmodels.months,
                years: viewmodels.years,
                days: viewmodels.days,
                expiryYears: viewmodels.expiryYears,
                expiryMonths: viewmodels.expiryMonths,
                twExpiryYears: viewmodels.twExpiryYears,
                newCarYears: viewmodels.newCarYears,
                goToResults: goToResults,
                goToExpired: goToExpired,
                variant_list: variant_list,
                fuel_type_list: fuel_type_list,
                vehicle_list: vehicle_list,
                get_variants: get_variants,
                RTO_LIST: RTO_list,
                checkForErrors: checkForErrors,
                isStepValid: isStepValid,
                isEmailValid:isEmailValid,
                isMobileValid:isMobileValid,
                VEHICLE_TYPE: VEHICLE_TYPE,
                vehicle_type_verbose: vehicle_type_verbose,
                affiliate_campaign:affiliate_campaign,
                cngKitValueValid:cngKitValueValid,
                isEmailValid:isEmailValid,
                filtered_variants_list:filtered_variants_list,
                set_variants:set_variants,
                moreThan90days:moreThan90days
            }
        }
    };
});
