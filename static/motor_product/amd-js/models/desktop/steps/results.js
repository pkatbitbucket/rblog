define(['knockout', 'models', 'viewmodels', 'utils', 'crypt', 'cookie', 'configuration',
        'events', 'constants', 'actions','../../../../global/mixpanel-actions', 'liveOffline','components'],
    function(ko, models, viewmodels, utils, crypt, Cookies, settings,
            events, appconstants, actions,mixpanelActions, liveOffline, components) {

    var allAvailableInsurers = {
        'fourwheeler':[
            {
                slug: 'bajaj-allianz',
                name: 'Bajaj Allianz'
            },
            {
                slug:'bharti-axa',
                name:'Bharti Axa'
            },
            {
                slug:'future-generali',
                name: 'Future Generali'
            },
            {
                slug: 'hdfc-ergo',
                name: 'HDFC ERGO'
            },
            // {
            //     slug: 'iffco-tokio',
            //     name: 'IFFCO TOKIO'
            // },
            {
                slug: 'l-t',
                name: 'L & T'
            },
            {
                slug:'new-india',
                name: 'New India'
            },
            {
                slug:'reliance',
                name:'Reliance'
            },
            {
                slug:'tata-aig',
                name: 'TATA AIG'
            },
            {
                slug:'universal-sompo',
                name:'Universal Sompo'
            },
            {
                slug:'oriental',
                name: 'Oriental'
            },
            {
                slug:'liberty-videocon',
                name: 'Liberty Videocon'
            }
        ],

        "twowheeler":[
            {
                slug: 'bajaj-allianz',
                name: 'Bajaj Allianz'
            },
            {
                slug:'bharti-axa',
                name:'Bharti Axa'
            },
            {
                slug: 'hdfc-ergo',
                name: 'HDFC ERGO'
            },
            {
                slug: 'iffco-tokio',
                name: 'IFFCO TOKIO'
            },
            {
                slug:'new-india',
                name: 'New India'
            },
            {
                slug:'universal-sompo',
                name:'Universal Sompo'
            },
        ]
    };

    var affiliate_campaign = AFFILIATE;
    var $ = require('npm-zepto');

    utils.show_chat_box();

    var sort_criteria = ko.observable('Best Match');
    var loadingText = ko.observable();
    var moreThan90days = ko.observable(false);
    sort_criteria.subscribe(function(newvalue){
        sortPlans(newvalue);

        events.cust_ev(actions.SortPlans, {
            defaults: event_defaults,
            sortCriteria: newvalue
        });
    });

    var vehicle_type_verbose = {
        fourwheeler: 'car',
        twowheeler: 'bike'
    };

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    // special message
    var special_message = {
        html: capitalizeFirstLetter(vehicle_type_verbose[VEHICLE_TYPE]) + " Insurance Service Tax is increasing from 1st June 2015. <strong>Renew immediately</strong>. <br />Beneficial for customers whose insurance is expiring before 15th July.",
        visible: ko.observable(false),
        hidden_by_user: ko.observable(false),
        condition: function() {
            return !quote.isNewVehicle();
        }
    };

    special_message.visible.subscribe(function(value) {
        if (value) {
            $('body').addClass('recede');
        } else {
            $('body').removeClass('recede');
        }
    });

    var toll_free_number = CAR_TOLL_FREE;

    var allAddons = settings.getConfiguration().addons,
    selected_plan = ko.observable();

    // utils
    var verboseAmount = utils.verboseAmount,
        constants = appconstants;

    var get_next_ncb = function(ncb) {
        var next_ncb = 50;
        if (ncb != 50) {
            for (var i = 0; i < constants.ncb_options.length; i++) {
                if (constants.ncb_options[i].value == ncb) {
                    next_ncb = constants.ncb_options[i + 1].value
                }
            }
        }
        return next_ncb
    }

    var ui_states = {
        progress_bar_width: ko.observable(0),
        has_quote_modified_once: ko.observable(false).extend({
            store: {
                key: 'quote_modified'
            }
        }),
        has_seen_claim_popup: ko.observable(false).extend({
            store: {
                key: 'claim_popup'
            }
        }),
        has_seen_reg_popup: ko.observable(false).extend({
            store: {
                key: 'reg_popup'
            }
        }),
        has_seen_optimize_popup: ko.observable(false).extend({
            store: {
                key: 'optimize_popup'
            }
        }),
        has_optimized_quotes: ko.observable(false).extend({
            store: {
                key: 'optimized_quotes'
            }
        }),
        is_sorting: ko.observable(false),
        click_buy: ko.observable(false),
        click_edit: ko.observable(false),
        is_mobile_number_valid: ko.observable(true),
        request_call_success: ko.observable(false),
        custom_idv_mode: ko.observable(false),
        default_idv_mode: ko.observable(false)
    };


    ui_states.default_idv_mode.subscribe(function(new_value) {
        ui_states.custom_idv_mode(!ui_states.default_idv_mode());

        if (new_value === true) {
            quote_form.idv(0);
        }
    });

    ui_states.custom_idv_mode.subscribe(function(new_value) {
        ui_states.default_idv_mode(!ui_states.custom_idv_mode());

        if (new_value === true) {
            if (quote_form.idv() === 0) {
                quote_form.idv(min_idv());
            } else {
                quote_form.idv(quote.idv());
            }
        }
    });


    var ui_switches = {
        //dark_overlay_on: ko.observable(false),
        addon_popup: ko.observable(false),
        discounts_popup: ko.observable(false),
        reg_month_popup: ko.observable(false),
        has_claimed_popup: ko.observable(false),
        verify_popup: ko.observable(false),
        modify_popup: ko.observable(false),
        premium_breakup_popup: ko.observable(false),
        share_results_popup: ko.observable(false),
        loading_popup: ko.observable(false),
        idv_popup: ko.observable(false),
        accessories_popup: ko.observable(false),
        ncb_popup: ko.observable(false),
        final_confirm_popup: ko.observable(false),


        show_popup: function(popup) {
            ui_switches[popup](true);
            $('body,html').addClass("hide-overflow");
            var allPopups = document.getElementsByClassName('popup-wrapper');
            for (var i = 0; i < allPopups.length; i++) {
                allPopups[i].scrollTop = 0;
            };

            // For Additional cover click on "Accessories"
            if(popup == "accessories_popup"){
                events.cust_ev(actions.OpenAccessoriesPopup, {
                    defaults: event_defaults,
                });
            }

            //   ui_switches.dark_overlay_on(true);
        },

        hide_popup: function(popup) {
            ui_switches[popup](false);
            $('body,html').removeClass("hide-overflow");
            //ui_switches.dark_overlay_on(false);
        }
    };

    var reset_ui = function() {
        for (var key in ui_switches) {
            if (key != 'show_popup' && key != 'hide_popup') {
                ui_switches.hide_popup(key);
            }
        }
    };

    var policyRefresh = ko.observable(null);

    var loadingTransaction = ko.observable(null);
    var activityID = ko.observable();
    var selected_addons = ko.observableArray([]);

    var app_actions = {
        edit_car_details:function(){
            var _vehicle = VEHICLE_TYPE == "fourwheeler"?"Car":"Bike";
            var _policyType = JSON.parse(localStorage[VEHICLE_TYPE]).isNewVehicle?"New": "Renew";
            var _local = JSON.parse(localStorage[VEHICLE_TYPE])
            if(_local.policyExpired === undefined){
                if(_local.expiry_date && _local.expiry_month && _local.expiry_year){
                    var _monthNum = utils.monthNametoNumber(_local.expiry_month);
                    var _exDate = new Date(_local.expiry_year, _monthNum, _local.expiry_date)
                    if (_exDate<new Date()){
                        _policyType = "Expired";
                    }
                }
            }
            else if (_local.policyExpired){
                _policyType = "Expired";
            }

            var _gtm_event = _vehicle + "EventEditCarDetails",
                _gtm_category = _vehicle + " - Edit Car Details",
                _gtm_action = _policyType + " " + _vehicle,
                _gtm_label = "Edit Clicked"

            events.gtm_push(_gtm_event, _gtm_category, _gtm_action, _gtm_label, {});

            ko.router.navigate('', {
                trigger: true
            });

        },
        show_discounts_popup: function(){
            ui_switches.show_popup('discounts_popup');

            if(quote.hasAddedDiscounts()){
                events.cust_ev(actions.ChangeDiscountsPopup, {
                    defaults: event_defaults,
                });
            }else{
                events.cust_ev(actions.OpenDiscountsPopup, {
                    defaults: event_defaults,
                });
            }
        },
        show_idv_popup: function(){
            ui_switches.show_popup('idv_popup');
            events.cust_ev(actions.OpenIDVPopup, {
                    defaults: event_defaults,
                });
        },
        show_policy_details_popup: function(){
            events_on.on_edit = true;
            ui_states.click_edit(true);
            events.cust_ev(actions.OpenEditPolicy, {
                defaults: event_defaults,
            });
            selectedPlan(null);
            ui_switches.show_popup('ncb_popup');

            if(localStorage[VEHICLE_TYPE]){
                if(!(JSON.parse(localStorage[VEHICLE_TYPE]).policyExpired == false && moreThan90days())){
                    quote_form.expiry_error("");
                }
            }
        },
        show_share_results: function(){
            ui_switches.show_popup('share_results_popup');
            events.cust_ev(actions.ShareQuotes, {
                defaults: event_defaults,
            });
        },
        cancel_share_results: function(){
            ui_switches.hide_popup('share_results_popup');
        },
        prepare_buy: function(plan, event) {
            events_on.on_premium = true;
            setup_quote_form();
            selected_plan(plan);
            ui_switches.show_popup('verify_popup');
            event_extra_parameters(plan,'OpenPremiumBreakupPopup');
        },
        cancel_refresh: function(){
            events_on.on_premium = false;
            ui_switches.hide_popup('verify_popup');
            events.cust_ev(actions.ClosePremiumBreakupPopup,{
                defaults: event_defaults,
            });
        },
        quick_buy_plan: function(plan){
            events_on.on_buy = true;
            selectedPlan(plan);
            if(!quote.hasOptimizedQuotes()){
                ui_states.click_buy(true);
                ui_switches.show_popup('ncb_popup');
                event_extra_parameters(plan,'BuyPlan');
            }
            else{
                var quoteId = quote.quoteId;

                liveOffline.sendInspectionLeadtoLMS(plan, quote, shareableUrl());
                utils.motorOlark.sendSelectPlanNotification(plan);
                event_extra_parameters(plan,'BuyPlan');

                // *********************************** VIZURY PLAN OPENED **********************************
                var vizury_plan_opened = {
                    'vizury_category': settings.getConfiguration().ga_category,
                    'po_sub_category': settings.getConfiguration().vehicle_type,
                    'po_name': plan.insurerName,
                    'po_price': plan.calculatedTotalPremium(),
                    'po_insurer_img_url': window.location.origin + '/static/motor_product/desktopMVP/app/assets/img/insurer/' + plan.insurerSlug + '.png'
                };
                utils.gtm_data_log(vizury_plan_opened);
                utils.gtm_event_log('vizury_plan_opened');

                // *******************************************************************************************

                policyRefresh('start');
                var old_premium = plan.calculatedTotalPremium(), new_premium;

                // For new flow, dont send pastPolicyExpiryDate
                if(quote.isNewVehicle()){
                    delete quote.pastPolicyExpiryDate;
                }

                loadingTransaction(true);
                $.post('/motor/' + VEHICLE_TYPE + '/api/confirm/' + quoteId + '/' + plan.insurerSlug + '/', JSON.parse(ko.toJSON(quote)), function(response) {
                    response = crypt.parseResponse(response);

                    var form_url = '/motor/' + VEHICLE_TYPE + '/' + plan.insurerSlug + '/desktopForm/' + response.data.transactionId + '/';
                    if (TPARTY) {
                        form_url += '?tparty=' + TPARTY;
                    }
                    var queryDict = {}
                    location.search.substr(1).split("&").forEach(function(item) {queryDict[item.split("=")[0]] = item.split("=")[1]})
                    if(queryDict.type=='dealer'){
                        form_url += '?type=dealer';
                    }
                    window.location.href = form_url;
                });
            };
        },
        change_accessories: function() {
            if(!quote_form.idv_electrical()){
                quote_form.idv_electrical(0);
            }
            if(!quote_form.idv_non_electrical()){
                quote_form.idv_non_electrical(0);
            }
            quote.idvElectrical(parseInt(quote_form.idv_electrical()));
            quote.idvNonElectrical(parseInt(quote_form.idv_non_electrical()));
            event_defaults.idvElectrical = quote_form.idv_electrical();
            event_defaults.idvNonElectrical = quote_form.idv_non_electrical();
            events.cust_ev(actions.UpdateAccessories,{
                defaults: event_defaults,
            });
            ui_switches.hide_popup('accessories_popup');
            get_quotes();
        },
        cancel_change_accessories: function() {
            setup_quote_form();
            ui_switches.hide_popup('accessories_popup');
        },
        change_idv: function() {
            if(quote_form.idv() && quote_form.idv() != '' && quote_form.idv() <= max_idv() && quote_form.idv() >= min_idv() || (ui_states.default_idv_mode() && quote_form.idv() == 0) ) {
                quote.idv(parseInt(quote_form.idv()));
                ui_switches.hide_popup('idv_popup');
                events.cust_ev(actions.UpdateIDV, {
                    defaults: event_defaults,
                    idvValue: quote.idv()
                });
                get_quotes();
            }
        },
        cancel_change_idv: function() {
            setup_quote_form();
            events.cust_ev(actions.CloseIDVPopup, {
                    defaults: event_defaults,
                });
            ui_switches.hide_popup('idv_popup');
        },
        change_ncb: function() {
            if(!validateDate_quote_form())
                return false;

            var changed = false;

            if(quote.isNCBCertificate() != quote_form.has_ncb_certificate().value){
                quote.isNCBCertificate(quote_form.has_ncb_certificate().value);
                if (quote.isNCBCertificate() != 1) {
                    quote.newNCB(0)
                }
                else{
                    quote.newNCB(quote_form.new_ncb().value);
                }
                changed = true;
            }

            if(quote.reg_date() != quote_form.reg_date()){
                quote.reg_date(quote_form.reg_date());
                changed = true;
            }

            if(quote.reg_month() != quote_form.reg_month()){
                quote.reg_month(quote_form.reg_month());
                changed = true;
            }

            if(quote.reg_year() != quote_form.reg_year()){
                quote.reg_year(quote_form.reg_year());
                changed = true;
            }

            if(moreThan90days() && moreThan90days() !== expiredFor90Days_quoteForm()){
                setT_100Day(true);
                updatePolicyExpired_LocalStorage();
                changed = true;

            }
            else{
                if(quote.expiry_year() != quote_form.expiry_year()){
                    quote.expiry_year(quote_form.expiry_year());
                    changed = true;
                }
                if(quote.expiry_month() != quote_form.expiry_month()){
                    quote.expiry_month(quote_form.expiry_month());
                    changed = true;
                }
                if(quote.expiry_date() != quote_form.expiry_date()){
                    quote.expiry_date(quote_form.expiry_date());
                    changed = true;
                }

                var exDate_quoteform = new Date(quote_form.expiry_year(), utils.monthNametoNumber(quote_form.expiry_month()),quote_form.expiry_date())
                var _today = new Date();;
                exDate_quoteform.setHours(0,0,0,0);
                _today.setHours(0,0,0,0);
                if(utils.getDaysBetweenDates(exDate_quoteform, _today)>90){
                    moreThan90days(true);
                    setT_100Day(true);
                }
            }

            if (quote_form.is_claimed_last_year() && quote_form.previous_ncb() &&
                (quote.isClaimedLastYear() != quote_form.is_claimed_last_year().value ||
                quote.previousNCB() != quote_form.previous_ncb().value)){

                quote.isClaimedLastYear(quote_form.is_claimed_last_year().value);
                if (quote.isClaimedLastYear() == 1 || expiredFor90Days()) {
                    quote.previousNCB(0);
                }
                else{
                    quote.previousNCB(quote_form.previous_ncb().value);
                }
                changed = true;
            }

            if(quote.man_month() != quote_form.man_month()){
                quote.man_month(quote_form.man_month());
                changed = true;
            }
            if(quote.man_year() != quote_form.man_year()){
                quote.man_year(quote_form.man_year());
                changed = true;
            }
            if(quote.policy_start_date() != quote_form.policy_start_date()){
                quote.policy_start_date(quote_form.policy_start_date());
                if(quote.isNewVehicle() === 1)
                    changed = true;
            }
            if(quote.policy_start_month() != quote_form.policy_start_month()){
                quote.policy_start_month(quote_form.policy_start_month());
                changed = true;
            }

            // In case of overlapping years, update year.
            // Example 1st Jan, to take coming year and not current year
            var year = quote.policy_start_year() || _today.getFullYear();
            var _policyStartDate = new Date(year,utils.monthNametoNumber(quote_form.policy_start_month()),quote.policy_start_date())
            var _today = new Date();
            _policyStartDate.setHours(0,0,0,0);
            _today.setHours(0,0,0,0);
            if(+_policyStartDate < +_today){
                quote.policy_start_year(_today.getFullYear()+1);
            }

            // Update PolicyExpired value if flow is changed.
            var _local = JSON.parse(localStorage[VEHICLE_TYPE]);
            var _diff = utils.getDaysBetweenDates(utils.stringToDate(quote.pastPolicyExpiryDate(), 'dd-mm-yyyy', '-'), new Date());

            _local.policyExpired = _diff>0?true:false;
            localStorage.setItem(VEHICLE_TYPE, JSON.stringify(_local));

            ui_switches.hide_popup('ncb_popup');
            ui_states.click_buy(false);

            quote.hasSeenOptimizePopup(true);
            quote.hasOptimizedQuotes(true);

            if(changed){
                setup_quote_form();
                get_quotes();
            }
            else{
                if(selectedPlan()){
                    app_actions.quick_buy_plan(selectedPlan())
                }
            }

            ui_switches.hide_popup('ncb_popup');
            ui_states.click_buy(false);

            event_defaults.optimize_quotes_on = optimize_quotes_on();
            events.cust_ev('OptimizeQuotes', {
                defaults: event_defaults
            });

            //  On Optimize Quotes - resetting the "event_on - variables"
            events_on.on_premium = false;
            events_on.on_buy = false;
            events_on.on_edit = false;
        },
        cancel_change_ncb: function() {
            setup_quote_form();
            ui_switches.hide_popup('ncb_popup');
            quote.hasSeenOptimizePopup(true);
            ui_states.click_buy(false);
            ui_states.click_edit(false);

            if(events_on.on_edit){
                events.cust_ev('CloseEditPolicy', {
                    defaults: event_defaults
                });
            }else{
                events.cust_ev('CancelOptimizeQuotes', {
                    defaults: event_defaults
                });
            }

            if(events_on.on_edit)
                events_on.on_edit = false;
            else
                events_on.on_buy = false;

            resetMoreThan90Days();
            quote_form.expiry_date(quote.expiry_date());
            quote_form.expiry_month(quote.expiry_month());
            quote_form.expiry_year(quote.expiry_year());
        },
        request_call: function() {
            if (quote.mobile() && String(quote.mobile()).match('[0-9]{10}') && quote.mobile() != '') {
                ui_states.is_mobile_number_valid(true);
                var data = {
                    vehicle: quote.vehicle().name,
                    variant: quote.variant().name,
                    quote: JSON.parse(ko.toJSON(quote))
                };
                utils.lmsUtils.sendGeneric(data, 'call-form', quote.mobile(), {}, function() {
                    ui_states.request_call_success(true);
                });

                events.cust_ev(actions.CallMe, {
                    defaults: event_defaults,
                });

                // Cookies - Mobile number updated
                Cookies.set('mobileNo', quote.mobile());
                Cookies.set('mobileNoOn', "Results page");
                Cookies.set('mobileNoURL', window.location.href);

            } else {
                ui_states.is_mobile_number_valid(false);
            }
        },
        add_discounts: function() {
            if(validate_form()){
                quote.voluntaryDeductible(parseInt(quote_form.vd().value));

                if(VEHICLE_TYPE == 'fourwheeler'){
                    quote.extra_user_occupation(parseInt(quote_form.extra_user_occupation().value));
                    quote.extra_user_dob_month(quote_form.extra_user_dob_month());
                    quote.extra_user_dob_date(quote_form.extra_user_dob_date());
                    quote.extra_user_dob_year(quote_form.extra_user_dob_year());
                }

                quote.extra_isAntiTheftFitted(parseInt(quote_form.extra_isAntiTheftFitted().value));
                quote.extra_isMemberOfAutoAssociation(parseInt(quote_form.extra_isMemberOfAutoAssociation().value));
                quote.extra_isTPPDDiscount(parseInt(quote_form.extra_isTPPDDiscount().value));
                ui_switches.hide_popup('discounts_popup');
                quote.hasAddedDiscounts(true);
                events.cust_ev(actions.ApplyDiscounts, {
                    defaults: event_defaults,
                    vdValue: quote.voluntaryDeductible()
                });

                get_quotes();
            }
        },
        cancel_add_discounts: function() {
            setup_quote_form();
            ui_switches.hide_popup('discounts_popup');

            events.cust_ev(actions.CloseDiscountsPopup, {
                defaults: event_defaults,
            });
        },
        select_addons: function(sort) {
            selected_addons([]);
            for (var key in quote) {
                if (key.indexOf('addon_') > -1) {
                    if (quote[key]() == '1') {
                        selected_addons.push(key.replace('addon_', ''));
                    }
                }
            }

            for (var i = 0; i < plans().length; i++) {
                var plan = plans()[i];
                add_selected_addons_to_plan(selected_addons(), plan);
            }

            ui_switches.hide_popup('addon_popup');

            if(sort){
                sortPlans(sort_criteria);
            }

        },


        cancel_select_addons: function() {
            setup_quote_form();
            ui_switches.hide_popup('addon_popup');
        },
    };

    var optimize_quotes_on = function(){

        if(events_on.on_premium == true)
            return "on-premium-buy";    // on-premium-buy
        else if(events_on.on_edit == true)
            return "on-edit";           // on-edit
        else if(events_on.on_buy == true)
            return "on-buy";            // normal buy
        else
            return "on-load";           // onload
    };

    var event_extra_parameters = function(plan,custom_event) {
        event_defaults.buy_plan_on = buy_plan_on();

        // sort criteria
        event_defaults.sortCriteria = sort_criteria();

        // zero depreciation
        event_defaults.ZeroDepStatus = (quote['addon_isDepreciationWaiver']() == 1)? 'Zero Depreciation Added' : 'Zero Depreciation Not Added';

        // discount added
        event_defaults.DiscountStatus = (quote.hasAddedDiscounts())? 'Discount Applied' : 'Discount Not Applied';

        // selected addons
        event_defaults.additionalCovers = selected_addons().join("|");

        // plan position
        event_defaults.plan_position = plan.index + 1;

        // insurer name
        event_defaults.insurer = plan.insurerName;

        events.cust_ev(actions[custom_event],{
            defaults: event_defaults,
        });
    }

    var buy_plan_on = function(){

        if(events_on.on_premium == true)
            return (!quote.hasOptimizedQuotes())? "on-no-quotes-premium-buy" : "on-quotes-premium-buy"; // on-premium-buy
        else if(events_on.on_buy == true)
            return (!quote.hasOptimizedQuotes())? "on-no-quotes-buy" : "on-quotes-buy"; //  normal buy
    };

    var close_popup_on = function(){

        if(ui_states.click_edit()){
            return "on-edit"; // on-edit
        }
        else if( (ui_states.click_buy() && ui_switches.verify_popup()) == true ){
            return "on-premium-buy"; // on-premium-buy
        }
        else if(ui_switches.idv_popup() == true){
            return "on-buy"; //  normal buy
        }
    }

    var add_selected_addons_to_plan = function(selected_addons, plan) {
        var planAddons = plan.addonCovers();
        for (var j = 0; j < planAddons.length; j++) {
            if (selected_addons.indexOf(planAddons[j].id) > -1) {
                planAddons[j].is_selected('1');

            } else {
                planAddons[j].is_selected('0');
            }
        }
        for (var j = 0; j < planAddons.length; j++) {
            if (planAddons[j].is_selected() == '0') continue;
            if (plan.dependentCovers && plan.dependentCovers[planAddons[j].id]) {
                var dependentCovers = plan.dependentCovers[planAddons[j].id];
                for (var i = 0; i < dependentCovers.length; i++) {
                    for (var m = 0; m < planAddons.length; m++) {
                        if (planAddons[m].id == dependentCovers[i]) {
                            planAddons[m].is_selected(planAddons[j].is_selected());
                        }
                    }
                }
            }
        }
        plan.addonCovers.valueHasMutated();
    };

    var quote = viewmodels.quote,
        plans = ko.observableArray([]),
        selectedPlan = ko.observable();
        shareableUrl = ko.observable();
        shareableBaseUrl = null;

    function resetMoreThan90Days(){
        // Reset Quote Form date pareams
        var _t = new Date()
        _t.setHours(0,0,0,0);
        var _diff = utils.getDaysBetweenDates(utils.stringToDate(quote.pastPolicyExpiryDate(), 'dd-mm-yyyy', '-'), _t);
        moreThan90days(_diff>90);
    }

    // Set Default Reg date to T-180 if current year is selected.
    function setRegistrationDateForCurrentYear(){
        var regDate = new Date(quote.reg_year(), utils.monthNametoNumber(quote.reg_month()), quote.reg_date());
        var today = new Date();

        regDate.setHours(0,0,0,0);
        today.setHours(0,0,0,0);

        var _diff = utils.getDaysBetweenDates(regDate, today);
        if(_diff<180){
            quote.reg_month(utils.monthNumberToName(0));
        }
    }

    var event_defaults = {
        is_new: quote.isNewVehicle(),
        vehicle_type: VEHICLE_TYPE,
        is_expired: function(){

            var policyExpired = false;
            var _local = JSON.parse(localStorage[VEHICLE_TYPE]);
            if(localStorage){
                policyExpired = _local.policyExpired;
            }
            if(policyExpired === undefined){
                if(_local.expiry_date && _local.expiry_month && _local.expiry_year){
                    var _monthNum = utils.monthNametoNumber(_local.expiry_month);
                    var _exDate = new Date(_local.expiry_year, _monthNum, _local.expiry_date)
                    if (_exDate<new Date()){
                        policyExpired = true;
                    }
                }
            }

            return (policyExpired)? true : false;
        },
    };

    var events_on = {
        on_edit : false,
        on_buy : false,
        on_premium : false,
        cancel_on_edit : false,     //  !events_on.on_edit,
        cancel_on_buy : false,      //  !events_on.on_buy,
        cancel_on_premium : false,  //  !events_on.on_premium,
    }

    quote.hasSeenOptimizePopup.subscribe(function(new_value){
        if(new_value == true){
            loadingText('We are fetching updated quotes based on the changes you just made.');
        }
    });

    var expiredFor90Days = ko.computed(function() {
        var _diff = utils.getDaysBetweenDates(utils.stringToDate(quote.pastPolicyExpiryDate(), 'dd-mm-yyyy', '-'), new Date());
        var ret = null;
        if (_diff > 90) {
            ret = true;
        }
        else {
            ret = false;
        }
        return ret;
    });

    var expiredFor90Days_quoteForm = ko.observable(expiredFor90Days());

    var is_expired = ko.computed(function(){
        var today = new Date();
        today.setHours(0, 0, 0, 0);
        var _diff = utils.getDaysBetweenDates(utils.stringToDate(quote.pastPolicyExpiryDate(), 'dd-mm-yyyy', '-'), today);
        if(_diff > 0){
            utils.setCookieForExpired();
            return true;
        }
    });

    is_expired.subscribe(function(new_value){
        if(new_value){
            utils.setCookieForExpired();
        }
    });

    var max_idv = ko.computed(function() {
        if (plans().length) {
            return parseInt((Math.max.apply(Math, plans().map(function(item) {
                return parseInt(item.maxIDV);
            }))) / 1000) * 1000;
        } else {
            return 0;
        }
    });
    var min_idv = ko.computed(function() {
        if (plans().length) {
            return parseInt((Math.min.apply(Math, plans().map(function(item) {
                return parseInt(item.minIDV);
            })) + 1000) / 1000) * 1000;
        } else {
            return 0;
        }
    });

    var emailQuotes = function() {
        var top_plans = [];
        var all_plans = plans();

        for (var index in all_plans) {
            var plan = all_plans[index];
            var final_plan = {
                'premium': plan.calculatedPremium(),
                'insurer': plan.insurerSlug,
                'covers': [],
            }

            for (var i = 0; i < plan.basicCovers().length; i++) {
                if (plan.basicCovers()[i].is_selected() == "1") {
                    if (plan.basicCovers()[i].id == "legalLiabilityDriver") {
                        final_plan['covers'].push(plan.basicCovers()[i].id);
                    }
                }
            }

            for (var i = 0; i < plan.addonCovers().length; i++) {
                if (plan.addonCovers()[i].is_selected() == "1") {
                    final_plan['covers'].push(plan.addonCovers()[i].id);
                }
            }

            for (var i = 0; i < plan.discounts().length; i++) {
                if (plan.discounts()[i].is_selected() == "1") {
                    final_plan['covers'].push(plan.discounts()[i].id);
                }
            }

            for (var i = 0; i < plan.special_discounts().length; i++) {
                if (plan.special_discounts()[i].is_selected() == "1") {
                    final_plan['covers'].push(plan.special_discounts()[i].id);
                }
            }

            top_plans.push(final_plan);
            // if (index == 2) break;
        }

        var commonQuote = JSON.parse(ko.toJSON(quote));
        commonQuote['csrfmiddlewaretoken'] = CSRF_TOKEN;
        commonQuote['quotes'] = JSON.stringify(top_plans);

        $.post('/motor/' + VEHICLE_TYPE + '/api/quotes/email/', commonQuote, function(response) {
            response = crypt.parseResponse(response);
        });
    }

    var sortPlans = function(criteria) {
        ui_states.is_sorting(true);
        setTimeout(function(){
            var addon_weight = 100,
                idv_weight = 1,
                premium_weight = 10,
                employee_discount = 1000;
            if(criteria == 'Premium'){
                addon_weight = 0;
                idv_weight = 0;
            }

            if(criteria == 'IDV'){
                addon_weight = 0;
                premium_weight = 0;
            }

            var maximumPremium = 0;
            var maximumIdv = 0;
            ko.utils.arrayForEach(plans(), function(plan) {
                if (maximumPremium < parseFloat(plan.calculatedPremium())) {
                    maximumPremium = parseFloat(plan.calculatedPremium());
                }
                if (maximumIdv < parseFloat(plan.calculatedAtIDV)) {
                    maximumIdv = parseFloat(plan.calculatedAtIDV);
                }
                var addons_for_plan = plan.addonCovers().map(function(plan) {
                    return plan.id;
                });
                var selected_addons_available = 0;
                ko.utils.arrayForEach(selected_addons(), function(addon) {
                    if (addons_for_plan.indexOf(addon) > -1) {
                        selected_addons_available += 1
                    }
                });
                plan.selectedAddonsAvailable = selected_addons_available;
            });

            ko.utils.arrayForEach(plans(), function(plan) {
                var cp = parseFloat(plan.calculatedPremium());
                var pr = (maximumPremium / cp);
                var ar = plan.selectedAddonsAvailable;
                var idvr = (plan.calculatedAtIDV / maximumIdv);
                var ed = plan.employeeDiscount().premium;
                var rank =  (addon_weight * ar) +
                            (idv_weight * idvr) +
                            (premium_weight * pr) +
                            (employee_discount * -1 * (ed || 0));
                plan.rank(rank);
            });

            plans.sort(function(left, right) {
                return left.rank() == right.rank() ? 0 : (left.rank() > right.rank() ? -1 : 1);
            });

            for (var i = 0; i < plans().length; i++) {
                plans()[i].index = i;
            };

            utils.motorOlark.sendAddOnsNotification(quote);
            utils.motorOlark.sendPlansNotification(plans());

            computeQuoteShareUrl();
            ui_states.is_sorting(false);
        }, 300);
    };

    var computeQuoteShareUrl = function() {
        var shareableQuoteUrl = shareableBaseUrl;
        shareableQuoteUrl += "?";
        var addonnumber = 0;
        for (var index in allAddons) {
            var addon = allAddons[index];
            if (quote[addon.id]() == '1') {
                addonnumber += Math.pow(2, index);
            }
        }
        shareableQuoteUrl += 'addons=' + addonnumber;

        if (quote['extra_isLegalLiability']() == '1') {
            shareableQuoteUrl += '&ll=1';
        }

        paPassengerMap = {
            '0': 0,
            '10000': 1,
            '50000': 2,
            '100000': 3,
            '200000': 4,
        }
        paTag = paPassengerMap[quote['extra_paPassenger']()];
        if (paTag) {
            shareableQuoteUrl += '&pa=' + paTag;
        }

        shareableUrl(shareableQuoteUrl);
    };

    var quote_form = {

        // modify popup
        is_claimed_last_year: ko.observable(),
        has_ncb_certificate: ko.observable(),
        previous_ncb: ko.observable(),
        new_ncb: ko.observable(),

        is_cng_kit_valid: ko.observable(),
        cng_fitted: ko.observable(),
        cng_kit_value: ko.observable(),

        // idv popup
        idv: ko.observable(),
        idv_electrical: ko.observable(),
        idv_non_electrical: ko.observable(),

        // discounts popup
        vd: ko.observable(),
        extra_user_occupation: ko.observable(),
        extra_user_dob_date: ko.observable(),
        extra_user_dob_month: ko.observable(),
        extra_user_dob_year: ko.observable(),
        extra_user_dob_formattedDate:ko.observable(),
        extra_user_dob_returnValue: ko.observable(),
        extra_isAntiTheftFitted: ko.observable(0),
        extra_isMemberOfAutoAssociation: ko.observable(0),
        extra_isTPPDDiscount: ko.observable(0),
        // error fields
        occupation_error: ko.observable(),
        dob_error:ko.observable(),

        // addons popup
        addon_isDepreciationWaiver: ko.observable(),
        addon_isInvoiceCover: ko.observable(),
        addon_is247RoadsideAssistance: ko.observable(),
        addon_isEngineProtector: ko.observable(),
        addon_isNcbProtection: ko.observable(),

        // registration popup
        reg_date: ko.observable('01'),
        reg_month: ko.observable('January'),
        reg_year: ko.observable(null),
        reg_error: ko.observable(),

        // pyp expiry in popup
        expiry_date: ko.observable(null),
        expiry_month: ko.observable(null),
        expiry_year: ko.observable(null),
        expiry_error:  ko.observable(""),

        // manufacturing popup
        man_month: ko.observable(),
        man_year: ko.observable(),
        man_error:ko.observable(),

        // policy start popup
        policy_start_date: ko.observable(),
        policy_start_month: ko.observable(),
        policy_start_date_list:ko.observableArray(),
        policy_selected_date:ko.observable(),
        policy_start_error:ko.observable()

    };

    var setup_quote_form = function() {
        setRegistrationDateForCurrentYear();
        quote_form.is_claimed_last_year(constants.boolean_options.filter(function(item) {
            return item.value == quote.isClaimedLastYear();
        })[0]);

        quote_form.has_ncb_certificate(constants.boolean_options.filter(function(item) {
            return item.value == quote.isNCBCertificate();
        })[0]);

        quote_form.previous_ncb(constants.ncb_options.filter(function(item) {
            return item.value == quote.previousNCB();
        })[0]);

        quote_form.new_ncb(constants.new_ncb_options.filter(function(item) {
            return item.value == quote.newNCB();
        })[0]);

        quote_form.is_cng_kit_valid(constants.is_cng_kit_valid);

        if (quote.isCNGFitted() == 1 && quote.cngKitValue() > 0) {
            quote_form.cng_fitted(constants.cng_fitted_options[2]);
            quote_form.cng_kit_value(quote.cngKitValue());
        }

        if (quote.isCNGFitted() == 1 && quote.cngKitValue() == 0) {
            quote_form.cng_fitted(constants.cng_fitted_options[1]);
            quote_form.cng_kit_value(quote.cngKitValue());
        }

        if (quote.isCNGFitted() == 0) {
            quote_form.cng_fitted(constants.cng_fitted_options[0]);
        }

        quote_form.idv(quote.idv());
        quote_form.idv_electrical(quote.idvElectrical());
        quote_form.idv_non_electrical(quote.idvNonElectrical());

        if (parseInt(quote_form.idv()) == 0) {
            ui_states.default_idv_mode(true);
        }
        else {
            ui_states.custom_idv_mode(true);
        }

        quote_form.vd(constants.vd_options.filter(function(item) {
            return item.value == quote.voluntaryDeductible();
        })[0]);
        quote_form.occupation_error("");
        quote_form.dob_error("");

        quote_form.extra_user_occupation(constants.occupation_options.filter(function(item) {
            return item.value == quote.extra_user_occupation();
        })[0]);

        quote_form.extra_isAntiTheftFitted(constants.boolean_options.filter(function(item) {
            return item.value == quote.extra_isAntiTheftFitted();
        })[0]);

        quote_form.extra_isMemberOfAutoAssociation(constants.boolean_options.filter(function(item) {
            return item.value == quote.extra_isMemberOfAutoAssociation();
        })[0]);

        quote_form.extra_isTPPDDiscount(constants.boolean_options.filter(function(item) {
            return item.value == quote.extra_isTPPDDiscount();
        })[0]);

        if (quote.extra_user_dob_date() && quote.extra_user_dob_month() && quote.extra_user_dob_year()){

            var dob_month = isNaN(quote.extra_user_dob_month()) ? utils.monthToNumber(quote.extra_user_dob_month()):quote.extra_user_dob_month();
            quote_form.extra_user_dob_formattedDate(quote.extra_user_dob_date() + '-' + dob_month + '-' + quote.extra_user_dob_year());
        }

        // setup reg date
        quote_form.reg_date(quote.reg_date());
        quote_form.reg_month(quote.reg_month());
        quote_form.reg_year(quote.reg_year());
        quote_form.reg_error("");

        // setup man date
        quote_form.man_month(quote.man_month());
        quote_form.man_year(quote.man_year());
        quote_form.man_error(false);

        // setup expiry date
        quote_form.expiry_date(quote.expiry_date());
        quote_form.expiry_month(quote.expiry_month());
        quote_form.expiry_year(quote.expiry_year());
        quote_form.expiry_error("");

        // setup policy start date
        quote_form.policy_start_date(quote.policy_start_date());
        quote_form.policy_start_month(quote.policy_start_month());
        quote_form.policy_start_date_list(viewmodels.policy_date_list);
        quote_form.policy_selected_date(fetchDefaultPolicyDate());
        quote_form.policy_start_error("");

        var _diff = utils.getDaysBetweenDates(utils.stringToDate(quote.pastPolicyExpiryDate(), 'dd-mm-yyyy', '-'), new Date());
        viewmodels.populateExpiredYears((_diff>0));
    };

    function fetchDefaultPolicyDate(){
        var selectedDate = quote.policy_start_date() +" "+ quote.policy_start_month();
        if(quote_form.policy_start_date_list().indexOf(selectedDate) <0){
            return quote_form.policy_start_date_list()[0];
        }
        return quote.policy_start_date() +" "+ quote.policy_start_month();
    }

    quote_form.policy_selected_date.subscribe(function(newValue){
        var value = newValue.split(" ");
        quote_form.policy_start_date(value[0]);
        quote_form.policy_start_month(value[1]);
    });

    quote_form.expiry_date.subscribe(function(new_value){
        validateDate_quote_form();
    });

    quote_form.expiry_month.subscribe(function(new_value){
        validateDate_quote_form();
    });

    quote_form.expiry_year.subscribe(function(new_value){
        validateDate_quote_form();
    });

    quote_form.extra_user_dob_formattedDate.subscribe(function(newValue){
        if(newValue){
            var _str = newValue.split('-');
            quote_form.extra_user_dob_date(_str[0]);
            quote_form.extra_user_dob_month(_str[1]);
            quote_form.extra_user_dob_year(_str[2]);

            DOBValidate(newValue, quote_form.dob_error);
        }
    });

    quote_form.extra_user_dob_returnValue.subscribe(function(newValue){
        if(newValue.data && newValue.data._formattedDate){
            quote_form.extra_user_dob_formattedDate(newValue.data._formattedDate);
        }
        else{
            quote_form.dob_error("Please enter valid date of birth.")
        }
    });

    quote_form.reg_date.subscribe(function(newValue){
        validateDate_quote_form();
    });

    quote_form.reg_month.subscribe(function(newValue){
        validateDate_quote_form();
    });

    quote_form.reg_year.subscribe(function(newValue){
        validateDate_quote_form();
    });

    quote_form.man_year.subscribe(function(newValue){
        validateDate_quote_form();
    });

    quote_form.man_month.subscribe(function(newValue){
        validateDate_quote_form();
    });

    quote_form.policy_start_date.subscribe(function(newValue){
        validateDate_quote_form();
    });

    quote_form.policy_start_month.subscribe(function(newValue){
        validateDate_quote_form();
    });

    function validateDate_quote_form(){
        var _today  = new Date();
        _today.setHours(0,0,0,0);
        var valid = true;

        if(!quote.isNewVehicle()){
            var _local = JSON.parse(localStorage[VEHICLE_TYPE]);

            // Registration Date
            var _regMonth = utils.monthNametoNumber(quote_form.reg_month());
            var _regDate = new Date(quote_form.reg_year(),_regMonth, quote_form.reg_date());
            var _regDiff = utils.getDaysBetweenDates(_regDate, _today);
            if(_regDate.getDate() != quote_form.reg_date()){
                quote_form.reg_error("Please select a valid date");
                valid = false;
            }
            // Registration date should be more than T-180
            else if(_regDiff<181){
                var _minRegDate = new Date();
                _minRegDate.setHours(0,0,0,0);
                _minRegDate.setDate(_minRegDate.getDate() - 180);
                var dateStr = _minRegDate.getDate() + " " + utils.monthNumberToName(_minRegDate.getMonth()) + " " + _minRegDate.getFullYear();
                quote_form.reg_error("Registration date should be before " + dateStr)
                valid = false;
            }
            else{
                quote_form.reg_error("");
            }

            // Expired date
            var _expiredMonth = isNaN(quote_form.expiry_month())? utils.monthNametoNumber(quote_form.expiry_month()):quote_form.expiry_month()-1;
            var _expiredDate = new Date(quote_form.expiry_year(),_expiredMonth, quote_form.expiry_date());
            var _expiredDiff = utils.getDaysBetweenDates(_expiredDate, _today);
            var pastDate = false;

            _today.setHours(0,0,0,0);

            // Is entered date for Policy Expiry Date in past
            if(_expiredDiff >0 || moreThan90days()){
                pastDate = true;
            }

            // Policy Expiry Date cannot be more than T+60
            else if(_expiredDiff < -60){
                var _t = new Date(_expiredDate.getTime());
                _t.setDate(_t.getDate() - 60);
                quote_form.expiry_error("You will be allowed to renew your policy on or after "+
                    _t.getDate() + " " +utils.monthNumberToName(_t.getMonth()) + ", " + _t.getFullYear() +
                    ". Please call our Toll Free Number " +CAR_TOLL_FREE    + " for any assistance.");

                if(!moreThan90days())
                    valid = false;
                return valid;
            }

            // Set moreThan90Days for quote form to toggle NCB and Claimed question
            expiredFor90Days_quoteForm((_expiredDiff >89));
            var dayStr = Math.abs(_expiredDiff) !=0? "in " + Math.abs(_expiredDiff) +" days." : "today.";

            // Policy Expired date cannot be before Registration Date
            if(+_expiredDate < +_regDate){
                quote_form.expiry_error("Policy expiry date cannot be before registration date.");
                valid = false;
            }

            // Invalid date: 31st Feb.
            else if(_expiredDate.getDate() != quote_form.expiry_date()){
                quote_form.expiry_error("Please select a valid date");
                valid = false;
            }

            // If user enters renew flow and put a past date.
            // Show a soft validation warning
            else if(_local.policyExpired == false && (moreThan90days() || pastDate)){
                quote_form.expiry_error("Your policy has already expired. Quotes will be shown accordingly.");
            }

            // If user enters Expired flow and put a future date.
            // Show a soft validation warning
            else if(_local.policyExpired && !pastDate && !moreThan90days()){
                quote_form.expiry_error("Your policy will expire " + dayStr + " Quotes will be shown accordingly.");
            }
            else{
                quote_form.expiry_error("")
            }
        }
        else{
            var year = quote.policy_start_year() || _today.getFullYear();
            var policyDate = new Date(year, utils.monthNametoNumber(quote_form.policy_start_month()), quote_form.policy_start_date())
            var manDate = new Date(quote_form.man_year(), utils.monthNametoNumber(quote_form.man_month()), 1);

            policyDate.setHours(0,0,0,0);
            _today.setHours(0,0,0,0);
            // Handling for 1st Jan
            if(+policyDate < +_today){
                policyDate.setFullYear(policyDate.getFullYear() +1);
            }

            var _diff = utils.getDaysBetweenDates(policyDate, manDate);
            var _minManDate = new Date();
            var _maxManDate = new Date();
            _minManDate.setFullYear(_minManDate.getFullYear()-2);
            _maxManDate.setDate(1);
            _minManDate.setHours(0,0,0,0);
            _maxManDate.setHours(0,0,0,0);

            if(+manDate <= +_minManDate){
                var dateStr = utils.monthNumberToName(_minManDate.getMonth()) + ", " + _minManDate.getFullYear();
                quote_form.man_error("Manufacturing date must be after " + dateStr);
                valid = false;
            }
            else if(+manDate > +_maxManDate){
                quote_form.man_error("Manufacturing date must be in past.");
                valid = false;
            }else if(_diff>=0){
                quote_form.man_error("Manufacturing date must be before Policy start date.");
                valid = false;
            }
            else{
                quote_form.man_error("");
            }

            // Policy Dates for New flow
            if(localStorage[VEHICLE_TYPE] && JSON.parse(localStorage[VEHICLE_TYPE]).isNewVehicle == 1){
                if(quote_form.policy_start_date_list().indexOf(quote_form.policy_selected_date()) <0){
                    quote_form.policy_selected_date(quote_form.policy_start_date_list()[0]);
                }
            }
        }

        return valid;
    }

    function setT_100Day(flag){
        var exdate = new Date();
        exdate.setHours(0,0,0,0);

        if(flag){
            exdate.setDate(exdate.getDate() - 100);
        }
        else{
            exdate.setDate(exdate.getDate() - 89);
        }

        quote.expiry_date(exdate.getDate());
        quote.expiry_month(utils.monthNumberToName(exdate.getMonth()));
        quote.expiry_year(exdate.getFullYear());
    }

    moreThan90days.subscribe(function(newValue){
        validateDate_quote_form();
    });

    var DOBValidate = function(data, error) {
            if (data == "") {
                error("This field cannot be empty.");
                return false;
            }
            if (data) {
                if (isNaN(data.split('-')[0]) || isNaN(data.split('-')[1]) || isNaN(data.split('-')[2])) {
                    error("Please enter a valid date.");
                    return false;
                }

                var cust_dob = utils.stringToDate(data, 'dd-mm-yyyy', '-'),
                    cust_age = utils.calculateAgeFromDate(cust_dob);

                // For case 31st feb, JS Date obj will return 3rd March.
                if(cust_dob.getMonth()+1 != data.split('-')[1]){
                    error("Please enter a valid Date of Birth");
                    return false;
                }

                if (cust_age < 18) {
                    error("You must be at least 18 years old to buy a car insurance. ");
                    return false;
                }
                else if(cust_age >100){
                    error("You are "+ cust_age+" years old. Really?");
                    return false;
                }

                // Check if user has deleted any field's value after updation to model.
                var numbox = document.getElementsByTagName("number-inputbox");
                var mySelect= document.getElementsByTagName("my-select");

                if(numbox && numbox.length>0){
                    for(var i=0;  i< numbox.length; i++){
                        if(numbox[i].children.length>0 && numbox[i].children[0].value.trim().length == 0){
                            error("This field cannot be empty.");
                            return false;
                        }
                    }
                }

                if(mySelect && mySelect.length>0){
                    var e = mySelect[0].children;
                    if(e[0] && e[0].value.length == 0){
                        error("This field cannot be empty.");
                        return false;
                    }
                }
            } else {
                error("Please enter a valid date.");
                return false;
            }
            error("");
            return true;
        }

    var validate_form = function(){
        var date_valid = true, occupation_valid = true;

        if(VEHICLE_TYPE == 'fourwheeler'){
            if(quote_form.extra_user_occupation() && quote_form.extra_user_occupation().value>=0){
                quote_form.occupation_error("");
                occupation_valid = true;
            }
            else{
                quote_form.occupation_error("Please select your occupation.");
                occupation_valid = false;
            }
            date_valid = DOBValidate(quote_form.extra_user_dob_formattedDate(), quote_form.dob_error)
        }
        return date_valid && occupation_valid;
    }

    var fetch;
    var hasSortedOnce = ko.observable(false);
    var getQuotesFinished = ko.observable(false);

    var showFullCard = ko.computed(function(){
        if(selected_addons().length > 0 || quote.hasAddedDiscounts() || quote.extra_isLegalLiability() == 1 || quote.extra_paPassenger() > 0){
            return true
        }
        else{
            return false
        }
    });

    var timeoutGTMC = null;
    var stampGTMCookies = function(notFinishedYet){
        var pid=quote.vehicle()['name'] + '_' +
                quote.fuel_type()['name'] + '_' +
                quote.variant()['name'] + '_' +
                quote.rto_info()['name'] + '_' +
                quote.reg_year();

        pid+=quote.hasOptimizedQuotes() ? '_' + quote.reg_date() + '_' + quote.expiry_date()+'-'+quote.expiry_month()+'-'+quote.expiry_year() + '_' + quote.previousNCB() : '';
        var premium = plans()[0].calculatedPremium();
        var insurer = plans()[0].insurerSlug;
        var insurerimageurl = STATIC_URL + '/img/motor/insurers/' + insurer + '.png';
        var carimageurl = STATIC_URL + '/img/car-outline.png';
        var options = { expires: 86400, path: '/', domain: '.coverfox.com' };
        Cookies.set('pid', pid, options);
        Cookies.set('premium', premium, options);
        Cookies.set('insurer', insurer, options);
        Cookies.set('insurer_logo', insurerimageurl, options);
        Cookies.set('qlp', window.location.href, options);
        Cookies.set('carimage', carimageurl, options);
        events.gtm_push(notFinishedYet?'vizurycookie30secquoteload':'vizurycookiequoteload', '', '', '', {});
    }

    var get_quotes = function() {
        window.scrollTo(0, 0);
        //special_message.visible(false);
        reset_ui();

        if(timeoutGTMC){
            clearTimeout(timeoutGTMC);
        }

        if(fetch){
            clearInterval(fetch);
        }

        getQuotesFinished(false);

        // Set previous NCB based on vehicle age (calculated from Registration Date) if the user has not set the value manually

        utils.motorOlark.sendQuoteNotification(quote, shareableUrl());
        var json_quote = JSON.parse(ko.toJSON(quote));
        plans([]);

        var numberReg = /^\d+$/g;
        var alphaReg = /[a-z]/gi;

        // Remove any character
        if(!numberReg.test(json_quote.registrationNumber[1])){
            json_quote.registrationNumber[1] = json_quote.registrationNumber[1].replace(alphaReg,'');
        }


        var max_fetch = 20
        $.post('/motor/' + VEHICLE_TYPE + '/api/quotes-async/', json_quote, function(response) {

            response = crypt.parseResponse(response);
            quote.quoteId = response.data["quoteId"];
            shareableBaseUrl = QUOTE_SHARE_URL.replace('quote_id', quote.quoteId);

            var fetch_counter = 0;
            hasSortedOnce(false);

            var fetch_async = function(){
                $.post('/motor/' + VEHICLE_TYPE + '/api/quotes-async/'+quote.quoteId+'/', json_quote, function(resp){
                    resp = crypt.parseResponse(resp);
                    var finished = resp.data.finished;
                    var allPremiums = resp.data["premiums"];
                    var allCurrentInsurers = plans().map(function(plan){
                        return plan.insurerSlug
                    });
                    var onlyNewPremiums = allPremiums.filter(function(item){
                        return allCurrentInsurers.indexOf(item.insurerSlug) == -1 ;
                    });

                    var quoteId = response.data["quoteId"];
                    quote.quoteId = quoteId;
                    var _local = JSON.parse(localStorage.getItem(VEHICLE_TYPE));

                    for (var i = 0; i < onlyNewPremiums.length; i++) {

                        // Do not show insurer that is selected as previous insurer and does not allow renew
                        if(_local.past_policy_insurer_name &&
                            onlyNewPremiums[i].insurerName.toLowerCase().indexOf(_local.past_policy_insurer_name.toLowerCase())>-1){
                            // skip this insurer
                        }
                        else if(!onlyNewPremiums[i].errorCode){
                            plans.push(new models.Plan(onlyNewPremiums[i]));
                        }
                    };

                    if(plans().length >= 8 || fetch_counter == 2 || finished){

                        if(!hasSortedOnce()){
                            sortPlans(sort_criteria);
                            setTimeout(function(){
                                hasSortedOnce(true);
                            }, 300)

                        }
                    }
                    app_actions.select_addons();
                    set_legal_liability(quote.extra_isLegalLiability());
                    set_pa_passenger();

                    if(fetch_counter == 0 && !quote.hasOptimizedQuotes() && !quote.fastlane_success()){
                        setTimeout(function() {
                            ui_switches.show_popup('ncb_popup');
                        }, 2000);
                    }

                    fetch_counter++;

                    if(finished || fetch_counter == max_fetch){
                        if(timeoutGTMC){
                            clearTimeout(timeoutGTMC);
                        }
                        clearInterval(fetch);
                        getQuotesFinished(true);
                        stampGTMCookies();
                    }else if(!timeoutGTMC){
                        timeoutGTMC = setTimeout(stampGTMCookies.bind(null, true), 30000);
                    }
                })
            };

            fetch_async();

            fetch = setInterval(fetch_async, 2500);

            //clearInterval(bar);
            //ui_states.progress_bar_width(100);

            // setTimeout(function() { ui_switches.loading_popup(false); }, 300);

            // response = crypt.parseResponse(response);
            // console.log("Response >>>", response);

            // var allPremiums = response.data["premiums"];
            // var quoteId = response.data["quoteId"];
            // quote.quoteId = quoteId;

            // shareableBaseUrl = QUOTE_SHARE_URL.replace('quote_id', quoteId);

            // plans(allPremiums.map(function(item) {
            //     return new models.Plan(item);
            // }));

            // app_actions.select_addons();

            // // ********************** VIZURY TOP3 PLANS REQUIRED FOR ANALYTICS - DO NOT DELETE *****************
            // var vizury_plans = {
            //     'vizury_category': settings.getConfiguration().ga_category
            // };
            // for (var i = 0; i < plans().length && i < 3; i++) {
            //     var plan = plans()[i];
            //     var j = i + 1;
            //     vizury_plans['product' + j + '_name'] = plan.insurerName;
            //     vizury_plans['product' + j + '_price'] = plan.calculatedTotalPremium();
            //     vizury_plans['product' + j + '_sub_category'] = settings.getConfiguration().vehicle_type;
            //     vizury_plans['product' + j + '_insurer_img_url'] = window.location.origin + '/static/motor_product/desktopMVP/app/assets/img/insurer/' + plan.insurerId + '.png';
            // }
            // console.log('********************* VIZURY PLANS *******************', vizury_plans);
            // utils.gtm_data_log(vizury_plans);
            // utils.gtm_event_log('vizury_results');
            // // *****************************************************************

            // // ******** MOTOR RESULTS GA PAGE VIEW FIRED - DO NOT DELETE *******
            // if (settings.getConfiguration().vehicle_type == 'fourwheeler') {
            //     utils.page_view('/vp/motor/initiate-results/', 'VP-MOTOR-INITIATE-RESULTS');
            // } else {
            //     utils.page_view('/vp/bike/initiate-results/', 'VP-BIKE-INITIATE-RESULTS');
            // }
            // // *****************************************************************

            // set_legal_liability(quote.extra_isLegalLiability());
            // set_pa_passenger();

            // if(!quote.hasOptimizedQuotes()){
            //      setTimeout(function() {
            //         ui_switches.show_popup('ncb_popup');
            //     }, 2000);
            // }
        });
    };

    // desktop stuff
    var popup_settings = {
        parent_element: ko.observable(),
        active: ko.observable(false)
    };

    quote_form.vd.subscribe(function(new_value) {
        $('.discount-circle').toggleClass('flip');
    });

    quote_form.extra_user_occupation.subscribe(function(new_value){
        if(new_value && new_value.value>=0){
            quote_form.occupation_error("");
        }
        else{
            quote_form.occupation_error("Please select your occupation.");
        }
    });

    quote.extra_paPassenger.subscribe(function(value) {
        for (var i = 0; i < plans().length; i++) {
            var plan = plans()[i];
            plan.paPassenger(quote.extra_paPassenger());
        }

        event_defaults.PassengerCover = value;
        events.cust_ev(actions.UpdatePassengerCover, {
            defaults: event_defaults,
        });
    });

    var trigger_addon = ko.observable();

    var add_this_addon = function(addon) {
        var id = addon.id;
        if (quote[id]() == 1) {
            quote[id](0);

            if(addon.id == constants.addons[0].id){
                events.cust_ev(actions.RemoveZeroDep, {
                    defaults: event_defaults,
                });
            }
            else{
                events.cust_ev(actions.DeselectAddon, {
                    defaults: event_defaults,
                    addonName: addon.name
                });
            }
        } else {
            quote[id](1);

            if(addon.id == constants.addons[0].id){
                events.cust_ev(actions.AddZeroDep, {
                    defaults: event_defaults,
                });
            }
            else{
                events.cust_ev(actions.SelectAddon, {
                    defaults: event_defaults,
                    addonName: addon.name
                });
            }
        }

        selected_addons([]);

        for (var key in quote) {
            if (key.indexOf('addon_') > -1) {
                if (quote[key]() == '1') {
                    selected_addons.push(key.replace('addon_', ''));
                }
            }
        }

        events.cust_ev('select_addon', selected_addons().join(','));

        for (var i = 0; i < plans().length; i++) {
            var plan = plans()[i];
            add_selected_addons_to_plan(selected_addons(), plan);
        }

        sortPlans(sort_criteria());
    };

    var set_legal_liability = function(value) {
        for (var i = 0; i < plans().length; i++) {
            var plan = plans()[i];
            var basicCovers = plan.basicCovers();
            for (var j = 0; j < basicCovers.length; j++) {
                if (basicCovers[j].id == "legalLiabilityDriver") {
                    basicCovers[j].is_selected(value || basicCovers[j].default);
                }
            }
        }
        computeQuoteShareUrl();
    };

    var set_pa_passenger = function(value) {
        for (var i = 0; i < plans().length; i++) {
            var plan = plans()[i];
            plan.paPassenger(quote.extra_paPassenger());
        }
    };

    quote.extra_paPassenger.subscribe(function(newvalue) {
        computeQuoteShareUrl();
    });

    // toggle legal liability
    var toggle_ll = function() {
        var new_value;
        if (quote.extra_isLegalLiability() == 0) {
            new_value = 1;
            events.cust_ev(actions.AddDriverCover, {
                defaults: event_defaults,
            });
        } else {
            new_value = 0;
            events.cust_ev(actions.RemoveDriverCover, {
                defaults: event_defaults,
            });
        }
        quote.extra_isLegalLiability(new_value);
        set_legal_liability(new_value);

        sortPlans(sort_criteria);
    };

    var pap_control = ko.observable(false);

    var toggle_pap = function() {

        if (quote.extra_paPassenger() > 0) {
            pap_control(false);
            quote.extra_paPassenger(0);
        } else {
            if(pap_control()){
                pap_control(false);
            }
            else{
                pap_control(true);
            }
        }

        if(pap_control() && quote.extra_paPassenger() == 0){
            // passenger cover - checked = click
            events.cust_ev(actions.AddPassengerCover, {
                defaults: event_defaults,
            });
        }else{
            events.cust_ev(actions.RemovePassengerCover, {
                defaults: event_defaults,
            });
        }
    };

    var coverMap = {};
    for(var index in constants.addons) {
        var cover = constants.addons[index];
        coverMap[cover.id.replace('addon_', '')] = cover.name;
    }


    var rogueInsurers = ko.computed(function(){
        var ri = []

        for (var i = allAvailableInsurers[VEHICLE_TYPE].length - 1; i >= 0; i--) {
            var plan = plans().filter(function(plan){
                return plan.insurerSlug == allAvailableInsurers[VEHICLE_TYPE][i].slug
            })[0]

            if(!plan){
                ri.push(allAvailableInsurers[VEHICLE_TYPE][i])
            }
        };

        return ri;
    });

    function updatePolicyExpired_LocalStorage(){
        if(localStorage[VEHICLE_TYPE]){
            var today = new Date();
            var _local = JSON.parse(localStorage[VEHICLE_TYPE]);
            _local.policyExpired = (+utils.stringToDate(quote.pastPolicyExpiryDate(), 'dd-mm-yyyy', '-') < +today);
            localStorage.setItem(VEHICLE_TYPE, JSON.stringify(_local));
        }
    }

    function hideMarketingBanner(){
        // Marketing banner should hide from 1st April and hence date is hardcoded.
        var bannerEndDate = new Date(2016, 2,30);
        var today = new Date();
        today.setHours(0,0,0,0);
        return +today > +bannerEndDate
    }

    // ui_switches.discounts_popup.subscribe(function() {
    //     popup_settings.active(true);
    //     popup_settings.parent_element(document.getElementsByClassName("app-discounts")[0]);
    // });

    // popup_settings.parent_element.subscribe(function(newValue) {
    //     console.log("POPUP SETTINGS PARENT ELEMENT", newValue);
    // });

    // end desktop stuff
    return {
        initialize: function() {
            var isQuoteValid = quote.vehicle() && quote.registrationDate() && quote.rto_info();

            if(!isQuoteValid){
                window.location.hash = "#"
                return;
            }

            loadingText('We are fetching the best quotes from all top '+vehicle_type_verbose[VEHICLE_TYPE]+' insurers for your ' + quote.vehicle().name);
            quote.hasSeenOptimizePopup(false);
            window.scrollTo(0, 0);
            if (quote.previousNCB() == null) {
                var vehicleAge = utils.calculateAgeFromDate(utils.stringToDate(quote.registrationDate(), 'dd-mm-yyyy', '-'));
                if (vehicleAge < 7) {
                    quote.previousNCB(constants.age_wise_previous_ncb[vehicleAge]);
                }
                else {
                    quote.previousNCB(50);
                }
            }

            if(quote.newNCB() == null){
                quote.newNCB(0);
            }

            if(isNaN(quote.extra_user_dob_month())){
                dob_month = ('0' + (viewmodels.months.indexOf(quote.extra_user_dob_month()) + 1)).slice(-2)
            }
            else{
                dob_month = ('0' + quote.extra_user_dob_month()).slice(-2)
            }

            quote.extra_user_dob_month(dob_month);

            if(quote.extra_user_dob() || parseInt(quote.voluntaryDeductible()) > 0){
                quote.hasAddedDiscounts(true);
            }



            setTimeout(function() {
                $('.footer-innerwrap').addClass('reveal');
            }, 500);

            viewmodels.populateExpiredYears(JSON.parse(localStorage[VEHICLE_TYPE]).policyExpired);

            setup_quote_form();
            get_quotes();
            resetMoreThan90Days();

            // Reset policy date if its before T+1
            var today = new Date();
            if(quote.isNewVehicle() == 1){
                if (+utils.stringToDate(quote.newPolicyStartDate(), 'dd-mm-yyyy', '-') < +today){
                    today.setDate(today.getDate() + 1);
                    quote.policy_start_month(utils.monthNumberToName(today.getMonth()));
                    quote.policy_start_date(today.getDate())
                }
            }
            else{
                updatePolicyExpired_LocalStorage();
            }

            // ******** MOTOR RESULTS GA PAGE VIEW FIRED - DO NOT DELETE *******
            if (settings.getConfiguration().vehicle_type == 'fourwheeler') {
                utils.page_view('/vp/motor/initiate-results/', 'VP-MOTOR-INITIATE-RESULTS');
            } else {
                utils.page_view('/vp/bike/initiate-results/', 'VP-BIKE-INITIATE-RESULTS');
            }
            // *****************************************************************

            return {
                special_message: special_message,
                VEHICLE_TYPE: VEHICLE_TYPE,
                vehicle_type_verbose: vehicle_type_verbose,
                expiredFor90Days: expiredFor90Days,
                expiredFor90Days_quoteForm:expiredFor90Days_quoteForm,
                quote: quote,
                quote_form: quote_form,
                constants: constants,
                ui_states: ui_states,
                ui_switches: ui_switches,
                app_actions: app_actions,
                plans: plans,
                selectedPlan: selectedPlan,
                months: viewmodels.months,
                days: viewmodels.days,
                years: viewmodels.years,
                man_years: viewmodels.man_years,
                expiryYears: viewmodels.expiryYears,
                expiryMonths: viewmodels.expiryMonths,
                dobYears: viewmodels.dob_years(1940),
                policyRefresh: policyRefresh,
                loadingTransaction: loadingTransaction,
                popup_settings: popup_settings,
                add_this_addon: add_this_addon,
                selected_addons: selected_addons,
                coverMap: coverMap,
                selected_plan: selected_plan,
                max_idv: max_idv,
                min_idv: min_idv,
                verboseAmount: verboseAmount,
                shareableUrl: shareableUrl,
                toll_free_number: toll_free_number,
                toggle_ll: toggle_ll,
                toggle_pap: toggle_pap,
                pap_control: pap_control,
                emailQuotes: emailQuotes,
                arr_diff: utils.arr_diff,
                get_next_ncb: get_next_ncb,
                sort_criteria:sort_criteria,
                loadingText:loadingText,
                affiliate_campaign:affiliate_campaign,
                hasSortedOnce:hasSortedOnce,
                showFullCard:showFullCard,
                getQuotesFinished:getQuotesFinished,
                rogueInsurers:rogueInsurers,
                liveOfflineInsurers:liveOffline.liveOfflineList.insurers,
                liveOfflineRTOs:liveOffline.liveOfflineList.rtos,
                is_expired:is_expired,
                moreThan90days: moreThan90days,
                hideMarketingBanner:hideMarketingBanner
            }
        }
    }
});
