define(['knockout', 'models', 'viewmodels', 'utils', 'crypt', 'cookie', 'configuration', 'components'],
    function(ko, models, viewmodels, utils, crypt, Cookies, settings) {
    var $ = require('npm-zepto');

    var vehicle_type_verbose = {
        fourwheeler: 'car',
        twowheeler: 'bike'
    };
    var isFormSubmitted = ko.observable(false);
    var affiliate_campaign = AFFILIATE;

    var toll_free_number = CAR_TOLL_FREE;

    var constants = {
        reg_month_options: viewmodels.months,
        reg_year_options: viewmodels.years
    };

    var quote_type = ko.observable(null),
        reset_quote = function() {
            quote_type(null);
        },

        quote = viewmodels.quote,
        vehicle_list = ko.observableArray([]),
        variant_list = ko.observableArray([]),
        variant_search_keyword = ko.observable(null),
        vehicle_search_keyword = ko.observable(null),
        RTO_list = ko.observableArray([]),
        RTO_search_keyword = ko.observable(null);

    quote_type.subscribe(function(new_value) {
        isFormSubmitted(false);
        switch (new_value) {
            case 'renew':
                quote.isNewVehicle(false);
                break;
            case 'new':
                quote.isNewVehicle(true);
                break;
            case 'expired':
                quote.isNewVehicle(false);
                if(VEHICLE_TYPE == "fourwheeler"){
                    var expired_url = '/lp/car-insurance/renew-expired-policy/';
                    if (TPARTY) {
                        expired_url += '?tparty=' + TPARTY;
                    }
                    window.location.href = expired_url;
                }
                break;
            case null:
                quote.isNewVehicle(null);
                break;
        }
    });

    var ui_switches = {
        dark_overlay_on: ko.observable(false),
        rel_popup: ko.observable(false),
        whatsapp_popup: ko.observable(false),
        search_vehicle_popup: ko.observable(false),
        search_variant_popup: ko.observable(false),
        search_RTO_popup: ko.observable(false),
        error_popup: ko.observable(false),
        errors: ko.observable(false),

        show_popup: function(popup) {
            ui_switches[popup](true);
            ui_switches.dark_overlay_on(true);
        },

        hide_popup: function(popup) {
            ui_switches[popup](false);
            ui_switches.dark_overlay_on(false);
        }
    };

    var isEmpty = function(val){
        return val === undefined ||  val === null || val.toString().trim().length === 0;
    }

    var validateMobileNumber = function(mNum){
        if(isEmpty(mNum))
            return false;
        else{
            var mReg = /^[7-9][0-9]{9}$/;
            return mReg.test(mNum);
        }
    }

    var ui_functions = {
        select_vehicle: function(data) {
            quote.vehicle(data);
            ui_switches.hide_popup('search_vehicle_popup');
        },
        select_RTO: function(data) {
            quote.rto_info(data);
            ui_switches.hide_popup('search_RTO_popup');
        },
        select_variant: function(data) {
            quote.variant(data);
            ui_switches.hide_popup('search_variant_popup');
        },
        check_for_errors: function() {
            ui_switches.errors(false);
            if(!validateMobileNumber(quote.mobile())){
                ui_switches.errors(true);
            }

            if(!quote.isNewVehicle()){
                if (!quote.vehicle() || !quote.variant() || !quote.reg_year() || !quote.rto_info()) {
                    ui_switches.errors(true);
                }
            }
            else if (!quote.vehicle() || !quote.variant() || !quote.rto_info()) {
                ui_switches.errors(true);
            }
        },
        go_to_results: function() {
            ui_functions.check_for_errors();
            if (!ui_switches.errors()) {
                reset_parameters();
                var data = {
                    vehicle: quote.vehicle().name,
                    variant: quote.variant().name,
                    rto_info: quote.rto_info().name,
                    reg_year: quote.reg_year(),
                    quote: JSON.parse(ko.toJSON(quote))
                };

                utils.lmsUtils.sendGeneric(data, 'step1', quote.mobile(), {
                    "past_policy_expiry_date": quote.pastPolicyExpiryDate()
                }, function() {
                    isFormSubmitted(true);
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                });
                // ko.router.navigate('results', {
                //     trigger: true
                // });
            }
        }
    };


    quote.vehicle.subscribe(function() {
        get_variants();
    });

    ui_switches.search_vehicle_popup.subscribe(function(new_value) {
        if (new_value) {
            vehicle_search_keyword(null);
            quote.variant(null);
        }
    });

    ui_switches.search_RTO_popup.subscribe(function(new_value) {
        if (new_value) {
            RTO_search_keyword(null);
        }
    });

    ui_switches.search_variant_popup.subscribe(function(new_value) {
        if (new_value) {
            variant_search_keyword(null);
        }
    });

    var ui_states = {
        has_quote_modified_once: ko.observable(false).extend({
            store: {
                key: 'quote_modified'
            }
        }),
    };

    function reset_parameters() {
        var current = new Date(),
            current_year = current.getFullYear(),
            current_month = current.getMonth();

        var current_month_verbose = viewmodels.months[current_month];

        quote.idv(0);
        quote.previousNCB(null);
        quote.isClaimedLastYear(0);
        quote.voluntaryDeductible(0);
        quote.idvElectrical(0);
        quote.idvNonElectrical(0);
        quote.reg_month('January');
        quote.reg_date('01');
        quote.man_month(current_month_verbose);
        quote.man_year(current_year);
        quote.resetExpiryDate();
        ui_states.has_quote_modified_once(false);
        quote.isCNGFitted(0);
        quote.cngKitValue(0);

        for (var key in quote) {
            if (key.indexOf('addon_') > -1) {
                quote[key](0);
            }
        }
    }

    var get_vehicles = function() {
        $.post('/motor/' + VEHICLE_TYPE + '/api/vehicles/', {
            'csrfmiddlewaretoken': CSRF_TOKEN
        }, function(response) {
            response = crypt.parseResponse(response);
            vehicle_list(response.data.models);
        });
    };

    var get_variants = function() {
        variant_list([]);
        $.post('/motor/' + VEHICLE_TYPE + '/api/vehicles/' + quote.vehicle().id + '/variants/', {
            'csrfmiddlewaretoken': CSRF_TOKEN
        }, function(response) {
            response = crypt.parseResponse(response);
            variant_list(response.data.variants);
        });
    };

    var get_RTOs = function() {
        $.post('/motor/' + VEHICLE_TYPE + '/api/rtos/', {
            'csrfmiddlewaretoken': CSRF_TOKEN
        }, function(response) {
            response = crypt.parseResponse(response);
            RTO_list(response.data.rtos);
        });
    };

    var filtered_vehicle_list = ko.computed(function() {
        var list = vehicle_list(),
            keyword = vehicle_search_keyword();

        if (list.length && keyword) {
            return ko.utils.arrayFilter(list, function(item) {
                return item.name.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
            });
        } else {
            return list;
        }
    });

    var filtered_RTO_list = ko.computed(function() {
        var list = RTO_list(),
            keyword = RTO_search_keyword();

        if (list.length && keyword) {
            keyword = keyword.replace(/[^a-zA-Z0-9]/g, '');
            return ko.utils.arrayFilter(list, function(item) {
                var item_name = item.name.replace(/[^a-zA-Z0-9]/g, '');
                return item_name.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
            });
        } else {
            return list;
        }
    });

    var filtered_variant_list = ko.computed(function() {
        var list = variant_list(),
            keyword = variant_search_keyword();

        if (list.length && keyword) {
            keyword = keyword.replace(/[^a-zA-Z0-9]/g, '');
            return ko.utils.arrayFilter(list, function(item) {
                var item_name = item.name.replace(/[^a-zA-Z0-9]/g, '');
                return item_name.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
            });
        } else {
            return list;
        }
    });

    return {
        initialize: function(stage_type) {

            // ************************* REQUIRED ANALYTICS CODE - DO NOT DELETE *********************
            utils.gtm_data_log({
                'vizury_category': settings.getConfiguration().ga_category,
                'vizury_sub_category': settings.getConfiguration().vehicle_type
            });
            utils.gtm_event_log('vizury_step1');
            if (settings.getConfiguration().vehicle_type == 'fourwheeler'){
                utils.page_view('/vp/motor/step1/', 'VP-MOTOR-STEP1');
            }else{
                utils.page_view('/vp/bike/step1/', 'VP-BIKE-STEP1');
            }
            // ***************************************************************************************

            get_vehicles();
            get_RTOs();

            if (quote.vehicle()) {
                get_variants();
            }

            switch(quote.isNewVehicle()){
                case true:
                    quote_type('new');
                    break;
                case false:
                    quote_type('renew');
                    break;
            }

            return {
                quote: quote,
                quote_type: quote_type,
                reset_quote: reset_quote,
                filtered_vehicle_list: filtered_vehicle_list,
                filtered_variant_list: filtered_variant_list,
                vehicle_search_keyword: vehicle_search_keyword,
                RTO_search_keyword: RTO_search_keyword,
                variant_search_keyword: variant_search_keyword,
                filtered_RTO_list: filtered_RTO_list,
                ui_switches: ui_switches,
                ui_functions: ui_functions,
                constants: constants,
                VEHICLE_TYPE: VEHICLE_TYPE,
                vehicle_type_verbose: vehicle_type_verbose,
                toll_free_number:toll_free_number,
                affiliate_campaign:affiliate_campaign,
                isFormSubmitted:isFormSubmitted
            }
        }
    };
});
