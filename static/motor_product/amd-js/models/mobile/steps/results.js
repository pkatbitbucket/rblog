define(['knockout', 'models', 'viewmodels', 'utils', 'crypt', 'cookie', 'configuration', 'components'], function(ko, models, viewmodels, utils, crypt, Cookies, settings) {


    var insurers = utils.insurers,
        allAddons = settings.getConfiguration().addons;

    var affiliate_campaign = AFFILIATE;

    var $ = require('npm-zepto');


    var vehicle_type_verbose = {
        fourwheeler: 'car',
        twowheeler: 'bike'
    };


    var toll_free_number = CAR_TOLL_FREE;

    var constants = {
        age_wise_previous_ncb: {
            0: 0,
            1: 0,
            2: 20,
            3: 25,
            4: 35,
            5: 45,
            6: 50
        },

        ncb_options: [{
                key: '0%',
                value: 0
            }, {
                key: '20%',
                value: 20
            }, {
                key: '25%',
                value: 25
            }, {
                key: '35%',
                value: 35
            }, {
                key: '45%',
                value: 45
            }, {
                key: '50%',
                value: 50
            }

        ],

        boolean_options: [{
            key: 'No',
            value: 0
        }, {
            key: 'Yes',
            value: 1
        }],

        addons: settings.getConfiguration().addons,
        cng_fitted_options: settings.getConfiguration().cng_fitted_options,
        vd_options: settings.getConfiguration().vd_options,
        is_cng_kit_valid: settings.getConfiguration().is_cng_kit_valid,
    };


    var ui_states = {
        progress_bar_width: ko.observable(0),
        has_quote_modified_once: ko.observable(false).extend({
            store: {
                key: 'quote_modified'
            }
        }),
        custom_idv_mode: ko.observable(false),
        default_idv_mode: ko.observable(false),
        request_call_success: ko.observable(false),
        is_mobile_number_valid:ko.observable(true),

    };

    ui_states.default_idv_mode.subscribe(function() {
        ui_states.custom_idv_mode(!ui_states.default_idv_mode());
    });

    ui_states.custom_idv_mode.subscribe(function() {
        ui_states.default_idv_mode(!ui_states.custom_idv_mode());
    });


    var ui_switches = {
        //dark_overlay_on: ko.observable(false),
        addon_popup: ko.observable(false),
        discounts_popup: ko.observable(false),
        modify_popup: ko.observable(false),
        premium_breakup_popup: ko.observable(false),
        loading_popup: ko.observable(false),
        idv_popup: ko.observable(false),
        final_confirm_popup: ko.observable(false),


        show_popup: function(popup) {
            ui_switches[popup](true);
            //   ui_switches.dark_overlay_on(true);
        },

        hide_popup: function(popup) {
            ui_switches[popup](false);
            //ui_switches.dark_overlay_on(false);
        }


    };

    var reset_ui = function() {
        for (var key in ui_switches) {
            if (key != 'show_popup' && key != 'hide_popup') {
                ui_switches.hide_popup(key);
            }
        }
    };

    var policyRefresh = ko.observable(null);
    var activityID = ko.observable();
    var selected_addons = ko.observableArray([]);

    var app_actions = {
        modify_quote: function() {
            quote.isClaimedLastYear(quote_form.is_claimed_last_year().value);
            quote.previousNCB(quote_form.previous_ncb().value);
            if (quote_form.cng_fitted().value > 0) {
                quote.isCNGFitted(1);
            } else {
                quote.isCNGFitted(0);
            }

            if (quote_form.cng_kit_value() > 0) {
                quote.cngKitValue(quote_form.cng_kit_value());
            } else {
                quote.cngKitValue(0);
            }


            ui_switches.hide_popup('modify_popup');
            ui_states.has_quote_modified_once(true);


            get_quotes();



        },

        request_call: function() {
            var matched = quote.mobile().match('[0-9]{10}');

            if (matched) {
                ui_states.is_mobile_number_valid(true);
                var data = {
                    vehicle: quote.vehicle().name,
                    variant: quote.variant().name,
                    quote: JSON.parse(ko.toJSON(quote))
                };
                utils.lmsUtils.sendGeneric(data, 'call-form', quote.mobile(), {device: 'Mobile'}, function() {
                    ui_states.request_call_success(true);
                });

            } else {
                ui_states.is_mobile_number_valid(false);
            }
        },


        cancel_modify_quote: function() {
            setup_quote_form();
            buy_initiated(false);
            ui_switches.hide_popup('modify_popup');
        },
        final_confirm: function() {
            quote.isClaimedLastYear(quote_form.is_claimed_last_year().value);
            quote.previousNCB(quote_form.previous_ncb().value);
            if (quote_form.cng_fitted().value > 0) {
                quote.isCNGFitted(1);
            } else {
                quote.isCNGFitted(0);
            }

            if (quote_form.cng_kit_value() > 0) {
                quote.cngKitValue(quote_form.cng_kit_value());
            } else {
                quote.cngKitValue(0);
            }
            var quoteId = quote.quoteId;
            policyRefresh('start');
            ui_switches.hide_popup('premium_breakup_popup');
            console.log("policyRefresh >>>", policyRefresh());
            $.post('/motor/' + VEHICLE_TYPE + '/api/quotes/' + quoteId + '/refresh/' + selectedPlan().insurerSlug + '/', JSON.parse(ko.toJSON(quote)), function(response) {
                response = crypt.parseResponse(response);
                policyRefresh('done');
                console.log("MODIFIED RESPONSE >>>>", response);
                console.log(response);
                activityID(response.data.activityId);
                console.log("ACTIVITY ID >>>>>>>>>>>>>>>>>>>>>", activityID());
                var refreshedPlan = new models.Plan(response.data.quote);
                console.log("SELECTED ADDONS >>>>>>", selected_addons());


                add_selected_addons_to_plan(selected_addons(), refreshedPlan);

                console.log("TOTAL PREMIUM", refreshedPlan.calculatedTotalPremium());
                selectedPlan(refreshedPlan);
                ui_switches.hide_popup('modify_popup');
                ui_switches.show_popup('premium_breakup_popup');

                console.log("SELECTED PLAN >>>>", selectedPlan());
            });



            //get_quotes();
        },

        cancel_confirm: function() {
            setup_quote_form();
            policyRefresh(null);
            ui_switches.hide_popup('final_confirm_popup');
            ui_switches.hide_popup('premium_breakup_popup');
        },

        change_idv: function() {
            quote.idv(parseInt(quote_form.idv()));
            quote.idvElectrical(parseInt(quote_form.idv_electrical()));
            quote.idvNonElectrical(parseInt(quote_form.idv_non_electrical()));
            ui_switches.hide_popup('idv_popup');
            get_quotes();
        },

        cancel_change_idv: function() {
            setup_quote_form();
            ui_switches.hide_popup('idv_popup');
        },
        show_premium_breakup: function(plan) {
            selectedPlan(plan);
            ui_switches.show_popup('premium_breakup_popup');
        },

        hide_premium_breakup: function(plan) {
            if (buy_initiated()) {
                get_quotes();
            }
            ui_switches.hide_popup('premium_breakup_popup');
            buy_initiated(false);
        },

        add_discounts: function() {
            quote.voluntaryDeductible(parseInt(quote_form.vd().value));
            ui_switches.hide_popup('discounts_popup');
            get_quotes();
        },
        cancel_add_discounts: function() {
            setup_quote_form();
            ui_switches.hide_popup('discounts_popup');

        },
        select_addons: function() {
            quote.addon_isDepreciationWaiver(quote_form.addon_isDepreciationWaiver() ? 1 : 0);
            quote.addon_isInvoiceCover(quote_form.addon_isInvoiceCover() ? 1 : 0);
            quote.addon_is247RoadsideAssistance(quote_form.addon_is247RoadsideAssistance() ? 1 : 0);
            quote.addon_isEngineProtector(quote_form.addon_isEngineProtector() ? 1 : 0);
            quote.addon_isNcbProtection(quote_form.addon_isNcbProtection() ? 1 : 0);

            selected_addons([]);

            for (var key in quote) {
                if (key.indexOf('addon_') > -1) {
                    if (quote[key]() == '1') {
                        selected_addons.push(key.replace('addon_', ''));
                    }
                }
            }

            console.log("SELECTED ADDONS", selected_addons());
            console.log("PLANS >>>>>", plans());


            for (var i = 0; i < plans().length; i++) {
                var plan = plans()[i];
                add_selected_addons_to_plan(selected_addons(), plan);
            }

            ui_switches.hide_popup('addon_popup');
            sortPlans();
        },
        cancel_select_addons: function() {
            setup_quote_form();
            ui_switches.hide_popup('addon_popup');
        },
        confirm_buy: function() {
            $.post('/motor/' + VEHICLE_TYPE + '/api/confirm/' + quote.quoteId + '/' + selectedPlan().insurerSlug + '/', JSON.parse(ko.toJSON(quote)), function(response) {
                response = crypt.parseResponse(response);
                var form_url = '/motor/' + VEHICLE_TYPE + '/' + selectedPlan().insurerSlug + '/desktopForm/' + response.data.transactionId + '/';
                if (TPARTY) {
                    form_url += '?tparty=' + TPARTY;
                }
                window.location.replace(form_url);
            });
        },

        prepare_buy: function(plan) {
            selectedPlan(plan);

            // *********************************** VIZURY PLAN OPENED **********************************
            var vizury_plan_opened = {
                'vizury_category': 'MOTOR',
                'po_sub_category': 'fourwheeler',
                'po_name': plan.insurerName,
                'po_price': plan.calculatedTotalPremium(),
                'po_insurer_img_url': window.location.origin+'/static/motor_product/desktopMVP/app/assets/img/insurer/' + plan.insurerId + '.png'
            };
            utils.gtm_data_log(vizury_plan_opened);
            utils.gtm_event_log('vizury_plan_opened');
            // *******************************************************************************************
            reset_ui();
            buy_initiated(true);
            ui_switches.show_popup('modify_popup');
        },
        go_to_transaction: function(plan) {
            // *********************************** VIZURY BUY PAGE DATA **********************************
            var vizury_buy_plan_data = {
                'vizury_category': 'MOTOR',
                'po_sub_category': 'fourwheeler',
                'po_name': plan.insurerName,
                'po_price': plan.calculatedTotalPremium(),
                'po_insurer_img_url': window.location.origin+'/static/motor_product/desktopMVP/app/assets/img/insurer/' + plan.insurerId + '.png'
            };
            utils.gtm_data_log(vizury_buy_plan_data);
            utils.gtm_event_log('vizury_buy_page');
            // *******************************************************************************************

            $.post('/motor/' + VEHICLE_TYPE + '/api/confirm/' + quote.quoteId + '/' + selectedPlan().insurerSlug + '/', JSON.parse(ko.toJSON(quote)), function(response) {
                response = crypt.parseResponse(response);
                var form_url = '/motor/' + VEHICLE_TYPE + '/' + selectedPlan().insurerSlug + '/desktopForm/' + response.data.transactionId + '/';
                if (TPARTY) {
                    form_url += '?tparty=' + TPARTY;
                }
                window.location.href = form_url;
            });
        },
    };


    var buy_initiated = ko.observable(false);




    var add_selected_addons_to_plan = function(selected_addons, plan) {
        var planAddons = plan.addonCovers();
        console.log("ADDONS", planAddons);
        for (var j = 0; j < planAddons.length; j++) {
            if (selected_addons.indexOf(planAddons[j].id) > -1) {
                planAddons[j].is_selected('1');


            } else {
                planAddons[j].is_selected('0');

            }
        }
        for (var j = 0; j < planAddons.length; j++) {
            if (planAddons[j].is_selected() == '0') continue;
            console.log(planAddons[j].id);
            console.log(planAddons[j].is_selected());
            if (plan.dependentCovers && plan.dependentCovers[planAddons[j].id]) {
                var dependentCovers = plan.dependentCovers[planAddons[j].id];
                console.log("DEPENDENT >>>>>>", dependentCovers);
                for (var i = 0; i < dependentCovers.length; i++) {
                    for (var m = 0; m < planAddons.length; m++) {
                        if (planAddons[m].id == dependentCovers[i]) {
                            console.log("LOOP planadd-on-id >>", planAddons[m]);
                            console.log(planAddons[j].is_selected());
                            planAddons[m].is_selected(planAddons[j].is_selected());
                        }
                        planAddons.map(function(item) {
                            console.log(dependentCovers[i], "map >>> ", item.id, item.is_selected());
                        });
                    }
                }
            }
        }
        plan.addonCovers.valueHasMutated();
    };






    var quote = viewmodels.quote,
        plans = ko.observableArray([]),
        selectedPlan = ko.observable();

    var sortPlans = function() {
        var maximumPremium = 0;
        ko.utils.arrayForEach(plans(), function(plan) {
            if (maximumPremium < parseFloat(plan.calculatedPremium())) {
                maximumPremium = parseFloat(plan.calculatedPremium());
            }
            var addons_for_plan = plan.addonCovers().map(function(plan) {
                return plan.id;
            });
            var selected_addons_available = 0;
            ko.utils.arrayForEach(selected_addons(), function(addon) {
                if (addons_for_plan.indexOf(addon) > -1) {
                    selected_addons_available += 1
                }
            });
            plan.selectedAddonsAvailable = selected_addons_available;
        });

        ko.utils.arrayForEach(plans(), function(plan) {
            var cp = parseFloat(plan.calculatedPremium());
            var pr = (maximumPremium / cp);
            var ar = plan.selectedAddonsAvailable;
            var idvr = 0;
            if (quote.idv() != 0) {
                if (plan.calculatedAtIDV >= quote.idv()) idvr = 1;
            }
            var rank = 100 * ar + 10 * idvr + pr;
            plan.rank = rank;
        });

        plans.sort(function(left, right) {
            return left.rank == right.rank ? 0 : (left.rank > right.rank ? -1 : 1)
        });
    }

    var selectThisPlan = function(plan) {
        selectedPlan(plan);
        ui_switches.show_popup('premium_breakup_popup');
    };


    var quote_form = {

        // modify popup
        is_claimed_last_year: ko.observable(),
        previous_ncb: ko.observable(),

        is_cng_kit_valid: ko.observable(),
        cng_fitted: ko.observable(),
        cng_kit_value: ko.observable(),

        // idv popup
        idv: ko.observable(),
        idv_electrical: ko.observable(),
        idv_non_electrical: ko.observable(),

        // discounts popup
        vd: ko.observable(),

        // addons popup

        addon_isDepreciationWaiver: ko.observable(),
        addon_isInvoiceCover: ko.observable(),
        addon_is247RoadsideAssistance: ko.observable(),
        addon_isEngineProtector: ko.observable(),
        addon_isNcbProtection: ko.observable()
    };



    var setup_quote_form = function() {


        quote_form.is_claimed_last_year(constants.boolean_options.filter(function(item) {
            return item.value == quote.isClaimedLastYear();
        })[0]);

        quote_form.previous_ncb(constants.ncb_options.filter(function(item) {
            return item.value == quote.previousNCB();
        })[0]);

        quote_form.is_cng_kit_valid(constants.is_cng_kit_valid);

        if (quote.isCNGFitted() == 1 && quote.cngKitValue() > 0) {

            quote_form.cng_fitted(constants.cng_fitted_options[2]);
            quote_form.cng_kit_value(quote.cngKitValue());

        }

        if (quote.isCNGFitted() == 1 && quote.cngKitValue() == 0) {

            quote_form.cng_fitted(constants.cng_fitted_options[1]);
            quote_form.cng_kit_value(quote.cngKitValue());

        }

        if (quote.isCNGFitted() == 0) {

            quote_form.cng_fitted(constants.cng_fitted_options[0]);

        }

        quote_form.idv(quote.idv());

        quote_form.idv_electrical(quote.idvElectrical());
        quote_form.idv_non_electrical(quote.idvNonElectrical());

        if (parseInt(quote_form.idv()) == 0) {
            ui_states.default_idv_mode(true);
        } else {
            ui_states.custom_idv_mode(true);
        }

        quote_form.vd(constants.vd_options.filter(function(item) {
            return item.value == quote.voluntaryDeductible();
        })[0]);


        // setup addons
        quote_form.addon_isDepreciationWaiver(quote.addon_isDepreciationWaiver() == 1 ? true : false);
        quote_form.addon_isInvoiceCover(quote.addon_isInvoiceCover() == 1 ? true : false);
        quote_form.addon_is247RoadsideAssistance(quote.addon_is247RoadsideAssistance() == 1 ? true : false);
        quote_form.addon_isEngineProtector(quote.addon_isEngineProtector() == 1 ? true : false);
        quote_form.addon_isNcbProtection(quote.addon_isNcbProtection() == 1 ? true : false);



    };



    var get_quotes = function() {

        reset_ui();


        //quoteForm.voluntaryDeductible(quote.voluntaryDeductible());



        // var _date = new Date();
        // var _diff = utils.getDaysBetweenDates(utils.stringToDate(quote.pastPolicyExpiryDate(), 'dd-mm-yyyy', '-'), _date);
        // if(_diff > 90){
        //     expiredFor90Days(true);
        //     quote.isClaimedLastYear(1);
        // }




        // Set previous NCB based on vehicle age (calculated from Registration Date) if the user has not set the value manually







        var json_quote = JSON.parse(ko.toJSON(quote));
        plans([]);

        console.log("REQUEST >>", json_quote);

        // loading bar

        ui_states.progress_bar_width(0);

        ui_switches.loading_popup(true);
        plans([]);


        setTimeout(function() {
            ui_states.progress_bar_width(50);
        }, 300);
        var bar = setInterval(function() {
            ui_states.progress_bar_width(ui_states.progress_bar_width() + 1.3);
        }, 1000);


        $.post('/motor/' + VEHICLE_TYPE + '/api/quotes/', json_quote, function(response) {

            clearInterval(bar);
            ui_states.progress_bar_width(100);

            setTimeout(function() {
                ui_switches.loading_popup(false);
            }, 300);


            response = crypt.parseResponse(response);
            console.log("Response >>>", response);

            var allPremiums = response.data["premiums"];

            var quoteId = response.data["quoteId"];
            quote.quoteId = quoteId;

            //shareableUrl(QUOTE_SHARE_URL.replace('quote_id', quoteId));

            // var mcv = Math.min.apply(Math, $.map(allPremiums, function (item) {
            //     return parseInt(item.calculatedAtIDV);
            // }));

            // minimumCalculatedIDV(mcv);

            plans($.map(allPremiums, function(item) {
                if (item.exShowroomPrice) {
                    quote.exShowroomPrice(item.exShowroomPrice);
                }
                return new models.Plan(item);
            }));

            app_actions.select_addons();


            // ********************** VIZURY TOP3 PLANS REQUIRED FOR ANALYTICS - DO NOT DELETE *****************
            var vizury_plans = {
                'vizury_category': settings.getConfiguration().ga_category
            };
            for(var i=0; i < plans().length && i<3; i++) {
                var plan = plans()[i];
                var j = i+1;
                vizury_plans['product'+j+'_name'] = plan.insurerName;
                vizury_plans['product'+j+'_price'] = plan.calculatedTotalPremium();
                vizury_plans['product'+j+'_sub_category'] = settings.getConfiguration().vehicle_type;
                vizury_plans['product'+j+'_insurer_img_url'] = window.location.origin+'/static/motor_product/desktopMVP/app/assets/img/insurer/' + plan.insurerId + '.png';
            }
            console.log('********************* VIZURY PLANS *******************', vizury_plans);
            utils.gtm_data_log(vizury_plans);
            utils.gtm_event_log('vizury_results');
            // *****************************************************************




            // ******** MOTOR RESULTS GA PAGE VIEW FIRED - DO NOT DELETE *******
            if (settings.getConfiguration().vehicle_type == 'fourwheeler'){
                utils.page_view('/vp/motor/initiate-results/', 'VP-MOTOR-INITIATE-RESULTS');
            }else{
                utils.page_view('/vp/bike/initiate-results/', 'VP-BIKE-INITIATE-RESULTS');
            }
            // *****************************************************************

        });

    };


    var confirm_buy = function() {
        console.log("confirmBuy");
        console.log(JSON.parse(ko.toJSON(quote)));
        utils.event_log('confirm_buy', quote.policyExpired() ? 'Expired Flow' : 'Normal Flow', 0);
        $.post('/motor/' + VEHICLE_TYPE + '/api/confirm/' + quote.quoteId + '/' + selectedPlan().insurerSlug + '/', JSON.parse(ko.toJSON(quote)), function(response) {
            response = crypt.parseResponse(response);
            transactionID(response.data.transactionId);
            window.location.replace('/motor/' + VEHICLE_TYPE + '/' + selectedPlan().insurerSlug + '/desktopForm/' + transactionID() + '/');
            console.log(transactionID());
        });
    };



    return {
        initialize: function() {
            window.scrollTo(0, 0);
            if (quote.previousNCB() == null) {
                var vehicleAge = utils.calculateAgeFromDate(utils.stringToDate(quote.registrationDate(), 'dd-mm-yyyy', '-'));
                if (vehicleAge < 7) {
                    quote.previousNCB(constants.age_wise_previous_ncb[vehicleAge]);
                } else {
                    quote.previousNCB(50);
                }
            }
            setup_quote_form();
            get_quotes();


            return {
                quote: quote,
                quote_form: quote_form,
                constants: constants,
                ui_states: ui_states,
                ui_switches: ui_switches,
                app_actions: app_actions,
                selectThisPlan: selectThisPlan,
                plans: plans,
                selectedPlan: selectedPlan,
                months: viewmodels.months,
                days: viewmodels.days,
                years: viewmodels.years,
                expiryYears: viewmodels.expiryYears,
                policyRefresh: policyRefresh,
                buy_initiated: buy_initiated,
                VEHICLE_TYPE: VEHICLE_TYPE,
                vehicle_type_verbose: vehicle_type_verbose,
                toll_free_number: toll_free_number,
                affiliate_campaign:affiliate_campaign,


            };
        }
    };
});