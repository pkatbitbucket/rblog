var VEHICLE_TYPE = "fourwheeler";
var _api = new com.coverfox.apis().motor;

var setUpCarForm = function() {
    var policy_type_value = $('[name=car_policy_type]:checked').val();
    switch(policy_type_value){
    	case "new":
    		$("#expired").slideUp();
    		$("#divRegYear").fadeOut();
    		break;
		case "renew":
			$("#expired").slideUp();
			$("#divRegYear").fadeIn();
			break;
		case "expired":
			$("#expired").slideDown();
			$("#divRegYear").fadeIn();
			break;
    }
};

function getModels(){
    _api.getModels(VEHICLE_TYPE, function(response){
    	utils.createSelect2(response, "#car_make_model");
    });
}

function getFuelTypes(){
	_api.getFuelTypes(VEHICLE_TYPE, function(response){
    	utils.createSelect2(response, "#car_fuel_type");
    });
}

function getCarVariants(){
	var fuel_type = $('#car_fuel_type option:selected').data("value");
	var model = $("#car_make_model option:selected").data("value");
	if(fuel_type && model){
		_api.getVariants(VEHICLE_TYPE, model, fuel_type, function(response){
        	utils.createSelect2(response, "#car_variant");
        });
	}
}

function getRTOs(){
	_api.getRtos('twowheeler', function (response) {
    	utils.createSelect2(response, "#car_rto");
    });
}

function getPastRegistrationYear(){
	utils.createSelect2(utils.getPastYears(), "#car_reg_year");
}

function getExpiredDates(){
	var exDate = utils.getExpiredDateValue();
	var dates = new Array(31).fill(null).map(function(item, index){return index+1; });

	utils.createSelect2(dates,"#car_policy_date");
	utils.createSelect2(exDate.month,"#car_policy_month");
	utils.createSelect2(exDate.years,"#car_policy_year");
}

function validateVehicleDetails(vehicle){
	var valid = true;
	if (!vehicle.vehicle_make_model || !vehicle.vehicle_fuel_type || !vehicle.vehicle_variant) {
        valid = false;
        $('#car_detail_error').fadeIn();
    }

    return valid;
}

function validateRTO(vehicle){
	var valid = true;
    if (!vehicle.vehicle_rto) {
        valid = false;
        $('#car_rto_error').fadeIn();
    }
    return valid;
}

function validateCNGKit(vehicle){
	var cngreg = /^[1-9]\d*$/;
	var valid = true;

    if($('#car_fuel_type :selected').attr("name") == "CNG/LPG Externally Fitted" && vehicle.cngKitValue == 0){
        valid = false;
        $('#cng_error').fadeIn();
    }

    if($('#car_fuel_type :selected').attr("name") == "CNG/LPG Externally Fitted"){
        if(cngreg.test(vehicle.cngKitValue) && parseInt(vehicle.cngKitValue) <= 40000 ){
            $('#cng_error').fadeOut();
        }
        else{
            valid = false;
            $('#cng_error').fadeIn();
        }
    }
    return valid;
}

function validateRegistrationYear(vehicle){
	var valid = true;
	if ((vehicle.policy_type != "new") && !vehicle.reg_year) {
        valid = false;
        $('#car_reg_date_error').fadeIn();
    }
    return valid;
}

function validateModileNumber(){
	var regex =  /^[7-9][\d]{9}$/;
	var valid = true;
	var value = $("#txtMobileNo").val();
	if(value && value.length>0){
		valid = regex.test(value);
		if(!valid)
			$('#car_error_mobile').fadeIn();
	}
	return valid;
}

function validateCarExpiredDateValue(){
    var exDate = $("#car_expiry_date option:selected").val();
    var exMonth = $("#car_expiry_month option:selected").val();
    var exYear = $("#car_expiry_year option:selected").val();

    if(exDate && exMonth && exYear){
        var _date = new Date(JSON.parse(exYear).value, JSON.parse(exMonth).id -1, JSON.parse(exDate).value);
        var today = new Date();

        _date.setHours(0,0,0,0);
        today.setHours(0,0,0,0);
        
        if(_date.getDate() != JSON.parse(exDate).value){
            $("#car_expiry_date_error").text("Please select a valid date.")
            $("#car_expiry_date_error").fadeIn();
            return false;
        }
        else if(+_date >= +today){
            $("#car_expiry_date_error").text("Policy expiry date should be past.")
            $("#car_expiry_date_error").fadeIn();
            return false;
        }
        else{
            $("#car_expiry_date_error").text("")
            $("#car_expiry_date_error").fadeOut();
            return true
        }
    }
    else if($("#moreThan90days_car").is(":checked")){
        return true;
    }
    else {
        $("#car_expiry_date_error").text("Please select a valid date.")
        $("#car_expiry_date_error").fadeIn();

        return false;
    }
}

function validateCarForm() {
    var valid = true;
    $('.error-text').hide();
    var vehicle = getVehicleData();

    valid &= validateVehicleDetails(vehicle);
    valid &= validateRTO(vehicle);
    valid &= validateCNGKit(vehicle);
	valid &= validateRegistrationYear(vehicle);
	valid &= validateModileNumber();

    if (vehicle.policy_type === "expired") {
        valid &= validateCarExpiredDateValue();
    }

    return valid === 0?false:true;
}

function getVehicleData(){
	return {
        policy_type: $('[name=car_policy_type]:checked').val(),
        expiry_slot: $('[name=expiry_slot]:checked').val(),
        expiry_date: $("#car_expiry_date option:selected").attr("id"),
        expiry_month:$("#car_expiry_month option:selected").attr("name"),
        expiry_year:$("#car_expiry_year :selected").attr("id"),
        vehicle_make_model: $('#car_make_model option:selected').data("value"),
        vehicle_fuel_type: $('#car_fuel_type option:selected').data("value"),
        vehicle_variant: $('#car_variant option:selected').data("value"),
        vehicle_rto: $('#car_rto option:selected').data("value"),
        reg_year: $('#car_reg_year option:selected').attr("id"),
        cngKitValue: $('#txtCNGKit').val()
    }
}

function initformUI(){
	utils.createSelect2([],"#car_make_model");
	utils.createSelect2([],"#car_fuel_type");
	utils.createSelect2([],"#car_variant");
	utils.createSelect2([],"#car_rto");
	utils.createSelect2([],"#car_reg_year");
	utils.createSelect2([],"#car_policy_date");
	utils.createSelect2([],"#car_policy_month");
	utils.createSelect2([],"#car_policy_year");

	$('[name=car_policy_type]')[0].click();
}

function gtm_push(data){
	var gtm_data = {}
    gtm_data = {
        param1: JSON.parse(data.vehicle).name,
        param2: JSON.parse(data.fuel_type).name,
        param3: JSON.parse(data.vehicleVariant).name,
        param4: JSON.parse(data.rto_info).name
    }

    if (data.policy_type != "new")
        gtm_data.param5= data.reg_year;

    if(data.isCNGFitted)
        gtm_data.param6 = data.cngKitValue;

    gtm_data["event"] = "TeamBHPCarEventGetQuote";
    gtm_data["category"] = "TeamBHP Car - Get Quote";
    gtm_data["action"] = data.policy_type + " Car";
    gtm_data["label"] = " ";
    
    if(!window.dataLayer)
    	window.dataLayer = [];

    window.dataLayer.push(gtm_data);
}

function cleanFormData(data, vehicle){
	if (vehicle.policy_type == "renew" || vehicle.policy_type == "expired") {
        data['reg_year'] = vehicle.reg_year;
        data['expirySlot'] = 7;
    }
    else if(vehicle.policy_type == "new") {
        var today = new Date();
        today.setDate(today.getDate() + 1);
        data["policy_start_date"] = today.getDate();
        data["policy_start_month"] = utils.getMonthNumberToName(today.getMonth());

        today.setMonth(today.getMonth() - 2);
        data["man_month"] = utils.getMonthNumberToName(today.getMonth());
        data["man_year"] = today.getFullYear();
    }

    if (vehicle.policy_type == "expired") {
        if($("#moreThan90days_car").is(":checked")){
            var t_100 = new Date();
            t_100.setDate(t_100.getDate() - 100);
            data['expiry_date'] = t_100.getDate();
            data["expiry_month"] = utils.getMonthNumberToName(t_100.getMonth());
            data["expiry_year"] = t_100.getFullYear();
        }
        else{
            data['expiry_date'] = $("#car_expiry_date :selected").attr("id");
            data["expiry_month"] =$("#car_expiry_month :selected").attr("name");
            data["expiry_year"] =$("#car_expiry_year :selected").attr("id");
        }

        Cookies.set("lms_campaign","motor-teambhp");
    }

    var mobNo = $("#txtMobileNo").val();
    if(mobNo.length>0){

        Cookies.set("mobileNo", mobNo);

        data["campaign"] = "motor-teambhp";
        data["label"] = "teambhp";
        data["mobile"] = mobNo;

        if(vehicle.policy_type == 'expired'){
            data["lms_campaign"] = "motor-teambhp";
        }
    }
}

function sendLeadToLMS(data){
    $.post("/leads/save-call-time/",{
        'data' : JSON.stringify(data), 
        //'csrfmiddlewaretoken' : CSRF_TOKEN
    }, function(res){});
}

var submitCarForm = function() {

    var vehicle = getVehicleData(),
        redirect_url = '/motor/car-insurance/#results';

    var data = {
        'isNewVehicle': (vehicle.policy_type == "new") ? '1' : '0',
        'vehicle': JSON.stringify(vehicle.vehicle_make_model),
        'vehicleVariant': JSON.stringify(vehicle.vehicle_variant),
        'fuel_type': JSON.stringify(vehicle.vehicle_fuel_type),
        'rto_info': JSON.stringify(vehicle.vehicle_rto),
        'cngKitValue': vehicle.cngKitValue,
        'isCNGFitted': vehicle.isCNGFitted || 0
    };

    cleanFormData(data, vehicle);  
    gtm_push(data);
    sendLeadToLMS();
    console.log(data);

    localStorage.setItem(VEHICLE_TYPE, JSON.stringify(data));

    _api.getQuotes( VEHICLE_TYPE, 
    				vehicle.vehicle, 
    				vehicle.fuel_type, 
    				vehicle.vehicleVariant, 
    				vehicle.rto_info, 
    				vehicle.reg_year, 
    				vehicle.policy_type);
};


(function initializePage(){
	registerEvents(function(){
		initformUI();
	});
	getModels();
	getFuelTypes();
	getRTOs();
	getPastRegistrationYear();
	getExpiredDates();
})();
