function registerEvents(callback){
	$('input[name="car_policy_type"]').on("change",function() {
		console.log("Changed")
	    setUpCarForm();
	});

	$("#car_make_model").on("change",function() {
	    selected_model_id_car = $(this).children(":selected").attr("id");
	    $('#car_fuel_type').select2("val", "");
	    getCarVariants();
	});

	$("#car_fuel_type").on("change",function() {
	    selected_fuel_type_id_car = $(this).children(":selected").attr("id");
	    getCarVariants();
	    var selected_fuel =  $(this).children(":selected").text().trim();
	    if(selected_fuel=="CNG/LPG Externally Fitted"){
	        $("#divCNGKit").removeClass("hide").fadeIn();
	        if($('#txtCNGKit').val().length == 0){
	            $('#txtCNGKit').parent().addClass("selected").removeClass("hide")
	        }
	    }
	    else{
	        $('#divCNGKit').fadeOut();
	        $('#txtCNGKit').val('');
	    }
	});

	$("#car_variant").on("change",function(){
	    if($("option:selected", this).attr("id") >0 || $("option:selected", this).attr("id").length>0)
	        $('#car_detail_error').fadeOut();
	});

	$("#car_rto").on("change", function(){
	    if($("option:selected", this).attr("id") >0 || $("option:selected", this).attr("id").length>0)
	        $('#rto_error').fadeOut();
	});

	$("#car_reg_year").on("change",function () {
	    if($("option:selected", this).attr("id") >0 || $("option:selected", this).attr("id").length>0)
	        $('#reg_date_error').fadeOut();
	})

	$('#car_submit').on('click', function(event) {
	    event.preventDefault();
	    if (validateCarForm())
	        submitCarForm();
	});

	$("#txtCNGKit").on("focus", function(){
	    if($(this).val()<=0)
	        $(this).val('');
	});

	$("#txtCNGKit").on("blur",function(){
	    if($(this).val()<=0){
	        $(this).val('');
	    }
	    validateCarForm();
	});

	$("#car_expiry_date").on("change", function(){
	    if($("option:selected", this).attr("id")>0){
	        var err = "#"+ $(this).attr("id")+ "_error";
	        $(err).fadeOut();
	    }
	});

	$("#moreThan90days_car").on("change", function(){
	    if($(this).is(":checked")){
	        $("#expiryCalendar_car").slideUp();
	    }
	    else{
	        $("#expiryCalendar_car").slideDown();
	    }
	});

	$("#txtMobileNumber").on("blur", function(){
	    if($(this).val().length == 0){
	        $(this).parents(".cf-special-ip").removeClass("selected");
	    }
	    validateMobileNumber();
	});

	if(callback){
		callback();
	}
}