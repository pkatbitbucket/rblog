(function initializeSelect2(){
	$.fn.select2.defaults = $.extend($.fn.select2.defaults, {
	    allowClear: true, // Adds X image to clear select
	    closeOnSelect: true, // Only applies to multiple selects. Closes the select upon selection.
	    minimumResultsForSearch: 4 // Removes search when there are 15 or fewer options
	})
})();

var utils = function(){}

utils.createSelect2 = function (data, element, callback){
	var optionHtml = utils.createOptions(data);
	$(element).html(optionHtml);
	var option = {placeholder: $(element).data("placeholder")}
	$(element).select2(option);
}

utils.createOptions = function (data){
	var optionStr = "<option></option>";
    data.forEach(function(row, index){
    	var id = row.id || index;
    	var val = row.name || row;
        optionStr += "<option id="+ id + 
	        " name='"+ val + 
	        "' value='"+ val + 
	        "' data-value='" + JSON.stringify(row) +
	        "'>"+ val + "</option>"
    });
    return optionStr;
}

utils.getPastYears = function(){
    var yearArr = [];
    var today = new Date();
    var year = today.getFullYear();

    // Polulate last 14 years.
    for(var i =1; i<15; i++)
        yearArr.push({'id':year - i,'name':year - i, 'value':year - i});

    // You can buy renew policy 60 days in advance. So populate current year if T>= 31st Decc. - 60
    var yearEnd = new Date(year, 11,31);
    yearEnd.setDate(yearEnd.getDate() - 60);
    today.setHours(0,0,0,0);
    yearEnd.setHours(0,0,0,0);

    // Compare date by converting it to integer. date.getTime(). Comparing obj will always give false, due to reference.
    if(+today > +yearEnd){
        yearArr.unshift({'id':year,'name':year, 'value':year})
    }

    return yearArr;
}

var monthList = [
    "January" ,
    "February" ,
    "March" ,
    "April" ,
    "May" ,
    "June",
    "July" ,
    "August" ,
    "September" ,
    "October" ,
    "November" ,
    "December"];

utils.getMonthNumberToName = function(month){
	return monthList[month];
}


utils.getExpiredDateValue = function(){
    var t_1 = new Date();
    var t_90 = new Date();

    t_1.setDate(t_1.getDate() - 1);
    t_90.setDate(t_90.getDate() - 90);

    var month = [], years=[];
    // Overlapping years
    if(t_90.getMonth() > t_1.getMonth()){
        for (var i = t_90.getMonth(); i<=11; i++){
            month.push(monthList[i]);
        }
        for (var i = 0; i<=t_1.getMonth(); i++){
            month.push(monthList[i]);
        }
    }
    else{
        for (var i = t_90.getMonth(); i<=t_1.getMonth(); i++){
            month.push(monthList[i]);
        }
    }

    if(t_1.getFullYear() != t_90.getFullYear()){
        years.push(t_1.getFullYear());
        years.push(t_90.getFullYear());
    }
    else{
        years.push(t_1.getFullYear());
    }

    return {
        minDate: t_90,
        maxDate: t_1,
        month: month,
        years: years
    }
}