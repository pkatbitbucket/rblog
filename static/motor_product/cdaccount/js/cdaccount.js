$(document).ready(function(){
    $('.edit').on('click', function(){
        this_elem = $(this);
        url = '/motor/cdaccount/cd-transaction/' + this_elem.attr('data-account')+'/';
        $.ajax({
                url:url,
                type:'get',
                success:function(data){
                    $('.cd_transaction').html(data);
                    $('.cd_transaction').show();
                    $('.transaction_date').datepicker({dateFormat: 'dd-mm-yy'});
                }
            });

    });

    $('.cd_transaction').on('submit','form',function(e){
        e.preventDefault();
        this_form = $(this);
        this_parent = this_form.parents('.cd_transaction');
        data = this_form.serialize();
        $.ajax({
            url:this_form.attr('action'),
            type:'post',
            data:data,
            success: function(data){
                if(data['success']){
                    this_form.parents('.cd_transaction').hide();
                    this_form.parents('.cd_transaction').html('');
                    account_elem = '#cdaccount_'+data['account_id'];
                    $(account_elem).find('.account_balance').text(data['balance']);
                }
                else{
                    this_form.find('.errors').html('');
                    $.each(data['errors'], function(i,err){
                        this_form.find('.errors').append("<div>"+i+": "+err+"</div>");
                    });

                }
            }


        });
    });

});