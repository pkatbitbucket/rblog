var currentStep = ko.observable(null);

function changeStep() {
    var stepStr = window.location.hash;
    if (stepStr == "#step1" || stepStr == "#step2") {
        currentStep(stepStr.split('#')[1]);
    } else {
        window.location.hash = "#step1";
    }
}

$(window).on('hashchange', function () {
    changeStep();
});

var Validations = {
    isEmailAddress: function (str) {
        var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return pattern.test(str);
    },
    isMobileNumber: function (str) {
        var pattern = /^\d{10}$/;
        return pattern.test(str);
    },
}

function User(email, mobile) {
    this.email = ko.observable(email);
    this.mobile = ko.observable(mobile);
    this.password = ko.observable(null);
    this.confirmPassword = ko.observable(null);
    this.errors = ko.observable(0);
    this.serverError = ko.observable(null);
}

User.prototype = {

    initialize: function () {
        var self = this;
        self.email.subscribe(function(newvalue) {
            if (!Validations.isEmailAddress(newvalue)) {
                self.errors(self.errors() | (1 << 0));
            } else {
                self.errors(self.errors() & ~(1 << 0));
            }
        });
        self.mobile.subscribe(function(newvalue) {
            if (!Validations.isMobileNumber(newvalue)) {
                self.errors(self.errors() | (1 << 1));
            } else {
                self.errors(self.errors() & ~(1 << 1));
            }
        });
        self.password.subscribe(function(newvalue) {
            if (!newvalue) {
                self.errors(self.errors() & ~(1 << 2));
            }
        });
        self.confirmPassword.subscribe(function(newvalue) {
            if (self.password() && self.password() != newvalue) {
                self.errors(self.errors() | (1 << 2));
            } else {
                self.errors(self.errors() & ~(1 << 2));
            }
        });
    },

    changeDetails: function () {
        var self = this;
        if (!Validations.isEmailAddress(self.email()) || !Validations.isMobileNumber(self.mobile())) {
            return;
        }
        var message = {'email' : self.email(), 'mobile': self.mobile()}
        if (self.password() && self.password() == self.confirmPassword()) {
            message['password'] = self.password();
        }
        message["csrfmiddlewaretoken"] = CSRF_TOKEN;
        $.post('/cardekho/change-details/', message, function (response) {
            response = JSON.parse(response);
            if (response.success) {
                console.log("User details saved.");
                USER_EMAIL = self.email();
                USER_MOBILE = self.mobile();

                LMS.profileUpdate(USER_ID, self.mobile(), self.email());
            } else {
                window.location.reload();
            }
        });
    },
}

var UserDetailsView = {
    visible: ko.computed(function () {
        return (currentStep() == "step1");
    }),
    user: null,

    initialize: function (router) {
        var self = UserDetailsView;
        self.user = new User(USER_EMAIL, USER_MOBILE);
        self.user.initialize();
        router(true);
    },

    submitStepOne: function () {
        console.log("Step one submitted.");
        UserDetailsView.user.changeDetails();
        window.location.hash = "#step2"
    }
}

function Vehicle(vehicleType, model, fuelType, variant, pastPolicyExpiryDate, upload) {
    this.id = null;
    this.model = model;
    this.fuelType = fuelType;
    this.variant = variant;
    this.vehicleType = vehicleType;
    this.pastPolicyExpiryDate = pastPolicyExpiryDate;
    this.upload = ko.observable(upload);
    this.canGetQuotes = false;
    this.isPolicyExpired = false;

    var dStrs = pastPolicyExpiryDate.split('-');
    var pypExpiryDate = new Date(parseInt(dStrs[2]), parseInt(dStrs[1]) - 1, parseInt(dStrs[0]));
    var today = new Date();
    var currentDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    var days = Math.floor((pypExpiryDate - currentDate) / (1000 * 3600 * 24));

    if (((days >= 0) || (vehicleType == 'twowheeler')) && (days < 45)) {
        this.canGetQuotes = true;
    }

    if (days < 0) {
        this.isPolicyExpired = true;
    }
}

var VehicleList = function () {
    var self = this;
    self.vehicles = ko.observableArray([]);

    this.initialize = function (router) {
        $.post('/cardekho/leads/', {'csrfmiddlewaretoken': CSRF_TOKEN}, function (response) {
            var response = JSON.parse(response);
            if (response.success) {
                var vehicles = $.map(response.data, function(val) {
                    var vehicleType = "fourwheeler";
                    if (val.vehicle_type == "Twowheeler") {
                        vehicleType = "twowheeler";
                    }
                    var vehicle = new Vehicle(vehicleType, val.model, val.fuel_type, val.variant, val.past_policy_expiry_date, val.upload_url);
                    vehicle.id = val.id;
                    return vehicle;
                });
                self.vehicles(vehicles);
            }
            router(true);
        });
    }

    this.addVehicle = function (vehicle) {
        if (vehicle) {
            $.post('/cardekho/leads/add/', {'csrfmiddlewaretoken': CSRF_TOKEN, 'vehicleId': vehicle.variant.id, 'fuelType': JSON.stringify(vehicle.fuelType), 'pastPolicyExpiryDate': vehicle.pastPolicyExpiryDate}, function (response) {
                var response = JSON.parse(response);
                if (response.success) {
                    vehicle.id = response.data.id
                    self.vehicles.push(vehicle);
                    VehicleDetailsView.addFormVisible(false);

                    LMS.leadUpdate(USER_ID, ko.toJS(vehicle), 'add');
                }
            });
        }
    }

    this.removeVehicle = function () {
        var vehicle = this;
        $.post('/cardekho/leads/delete/', {'csrfmiddlewaretoken': CSRF_TOKEN, 'leadId': vehicle.id}, function (response) {
            var response = JSON.parse(response);
            if (response.success) {
                self.vehicles.remove(vehicle);

                LMS.leadUpdate(USER_ID, ko.toJS(vehicle), 'delete');
            }
        });
    }

    this.getVehicleById = function (id) {
        for (var index in this.vehicles()) {
            var v = this.vehicles()[index];
            if (v.id == id) {
                return v;
            }
        }
    }

    this.quotesForVehicle = function () {
        VehicleDetailsView.gotoQuotes(this);
    }
}

var VehicleDetailsView = {
    visible: ko.computed(function () {
        return (currentStep() == "step2")
    }),

    apis: new com.coverfox.apis(),

    selectedVehicleType: ko.observable(null),

    models: ko.observableArray([]),
    selectedModel: ko.observable(null),

    fueltypes: ko.observableArray([]),
    selectedFuelType: ko.observable(null),

    variants: ko.observableArray([]),
    selectedVariant: ko.observable(null),

    pastPolicyExpiryDate: ko.observable(null),
    addFormVisible: ko.observable(false),

    formError: ko.observable(null),

    vehicleList: null,

    initialize: function (router) {
        var self = VehicleDetailsView;
        self.vehicleList = new VehicleList();
        self.selectedVehicleType.subscribe(function (newvalue) {
            self.apis.motor.getModels(self.selectedVehicleType(), function (data) {
                self.models(data);
            });
        });
        self.selectedModel.subscribe(function (newvalue) {
            if(newvalue) {
                self.apis.motor.getFuelTypes(self.selectedVehicleType(), function (data) {
                    if (self.selectedVehicleType() == "twowheeler") {
                        self.fueltypes(data);
                        self.selectedFuelType(data[0]);
                    } else {
                        self.fueltypes(data);
                    }
                });
            } else {
                self.fueltypes([]);
                self.selectedFuelType(null);
            }
        });
        self.selectedFuelType.subscribe(function (newvalue) {
            if (newvalue) {
                self.apis.motor.getVariants(self.selectedVehicleType(), self.selectedModel(), self.selectedFuelType(), function (data) {
                    self.variants(data);
                });
            } else {
                self.variants([]);
                self.selectedVariant(null);
            }
        });

        $('#pyp_date_picker').datepick({
            'dateFormat': "dd-mm-yyyy",
            onClose: function() {
                $(this).change();
            }
        });

        self.vehicleList.initialize(router);
    },

    addNewVehicle: function () {
        var self = VehicleDetailsView;
        if (self.selectedVehicleType() && self.selectedModel() && self.selectedFuelType() && self.selectedVariant() && self.pastPolicyExpiryDate()) {
            var vehicle = new Vehicle(self.selectedVehicleType(), self.selectedModel(), self.selectedFuelType(), self.selectedVariant(), self.pastPolicyExpiryDate(), null);
            self.vehicleList.addVehicle(vehicle);
            self.formError(null);
            self.selectedVehicleType("fourwheeler");
            self.pastPolicyExpiryDate(null);
        } else {
            self.formError("Please enter valid details.");
        }
    },

    toggleAddForm: function () {
        var self = VehicleDetailsView;
        self.addFormVisible(!self.addFormVisible());
    },

    gotoQuotes: function (vehicle) {
        var vehicleType = vehicle.vehicleType;
        if (localStorage) {
            var vals = localStorage.getItem(vehicleType);
            try {
                vals = JSON.parse(vals);
            } catch (e) {
                vals = {};
            }
            if (!vals) vals = {};
            vals['isNewVehicle'] = false;
            vals['vehicle'] = vehicle.model;
            vals['fuel_type'] = vehicle.fuelType;
            vals['vehicleVariant'] = vehicle.variant;
            vals['vehicleId'] = vehicle.variant.id;
            vals['policyExpired'] = vehicle.isPolicyExpired;

            var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var pypSplit = vehicle.pastPolicyExpiryDate.split('-');
            vals['expiry_date'] = pypSplit[0];
            vals['expiry_month'] = months[parseInt(pypSplit[1])-1];
            vals['expiry_year'] = pypSplit[2];
            localStorage.setItem(vehicleType, JSON.stringify(vals));
        }
        $.post("/cardekho/quotes/", {'csrfmiddlewaretoken': CSRF_TOKEN, 'leadId': vehicle.id}, function (response) {
            var response = JSON.parse(response);
            if (response.success) {
                var win = window.open(response.data, "_blank");
                win.focus();
            }
        });
    }
}

$(document).ready(function() {
    LMS.loginEvent(USER_ID, USER_MOBILE, USER_EMAIL);
    UserDetailsView.initialize(function () {
        ko.applyBindings(UserDetailsView, document.getElementById("userinfo"));
    });
    VehicleDetailsView.initialize(function () {
        ko.applyBindings(VehicleDetailsView, document.getElementById("vehicledetails"));
    });
    changeStep();
});

window.addEventListener("message", function (response) {
    var response = response.data;
    if (response.success) {
        var vehicle = VehicleDetailsView.vehicleList.getVehicleById(response.data.id);
        vehicle.upload(response.data.url);

        LMS.leadUpdate(USER_ID, ko.toJS(vehicle), 'update');
    }
}, false);