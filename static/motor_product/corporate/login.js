var loginView = {
    employeeId: ko.observable(null),
    employeeIdError: ko.observable(false),
    password: ko.observable(null),
    passwordError: ko.observable(false),
    loginError: ko.observable(null),

    initialize: function() {
        loginView.employeeId.subscribe(function (newvalue) {
            if (!newvalue) {
                loginView.employeeIdError(true);
            } else {
                loginView.employeeIdError(false);
            }
        });
        loginView.password.subscribe(function (newvalue) {
            if (!newvalue) {
                loginView.passwordError(true);
            } else {
                loginView.passwordError(false);
            }
        });
    },

    login: function () {
        if (loginView.employeeId() && loginView.password()) {
            $.post(".", {"csrfmiddlewaretoken": CSRF_TOKEN, "username": loginView.employeeId(), "password": loginView.password()}, function(response) {
                var response = JSON.parse(response);
                if (response.success) {
                    window.location.reload();
                } else {
                    loginView.loginError(response.error);
                }
            });
        }
    }
}

$(document).ready(function() {
    loginView.initialize();
    ko.applyBindings(loginView);
});