var ManageView = {
    modalVisible: ko.observable(false),
    transaction_date: ko.observable(null),
    transaction_amount: ko.observable(null),
    transaction_od_amount: ko.observable(null),
    transaction_id: ko.observable(null),

    initialize: function () {
        var self = ManageView;
        self.transaction_id.subscribe(function (newvalue) {
            if (newvalue) {
                self.modalVisible(true);
            }
        });
        self.modalVisible.subscribe(function (newvalue) {
            if (!newvalue) {
                self.transaction_id(null);
            }
        });
    },

    attachStatus: function (id) {
        var self = ManageView;
        self.transaction_id(id);
    },

    detachStatus: function (id) {
        $.post('/cardekho/manage/', {'csrfmiddlewaretoken': CSRF_TOKEN, 'leadId': id, 'operation': 'detach'}, function (response) {
            var response = JSON.parse(response);
            window.location.reload();
        });
    },

    saveStatus: function () {
        var self = ManageView;

        if(!(self.transaction_date() && self.transaction_amount() && self.transaction_id())) {
            return;
        }

        if (!self.AmountValidation(self.transaction_amount())) {
            return;
        }

        if (!self.AmountValidation(self.transaction_od_amount())) {
            return;
        }

        if (!self.DateValidation(self.transaction_date())) {
            return;
        }

        $.post('/cardekho/manage/', {'csrfmiddlewaretoken': CSRF_TOKEN, 'leadId': self.transaction_id(), 'operation': 'attach', 'transaction_date': self.transaction_date(), 'premium_paid': self.transaction_amount(), 'od_premium': self.transaction_od_amount()}, function (response) {
            var response = JSON.parse(response);
            window.location.reload();
        });
        self.modalVisible(false);
    },

    AmountValidation: function(data) {
        var matched = data.match('[1-9][0-9]*');
        if (matched) return true;
        return false;
    },

    DateValidation: function(data) {
        var matched = data.match('[0-3][0-9]-[0-1][0-9]-[0-9]{4}');
        if (matched) return true;
        return false;
    },

    searchQuery: function (d, e) {
        if (e.keyCode === 13) {
            window.location.href = "/cardekho/manage/?query=" + $('#search-text').val();
        }
        return true;
    }
}

$(document).ready(function () {
    $('#transaction_date').datepick({
        'dateFormat': "dd-mm-yyyy",
        onClose: function() {
            $(this).change();
        }
    });
    ManageView.initialize();
    ko.applyBindings(ManageView);
});