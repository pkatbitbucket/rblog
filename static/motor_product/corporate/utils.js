function LMSUtils () {
    this.baseUrl = '/leads/save-call-time/';
}

LMSUtils.prototype = {
    _send: function (data) {
        console.log(data);
        data['campaign'] = 'motor-cardekhoEmp';

        $.post(this.baseUrl, {
            'data': JSON.stringify(data),
            'csrfmiddlewaretoken': CSRF_TOKEN
        }, function(response) {
            console.log("LMS data sent.");
        });
    },

    loginEvent: function (employeeId, mobile, email) {
        var data = {
            'event_type': 'login',
            'employee_id': employeeId,
            'mobile': mobile,
            'email': email,
        }
        this._send(data);
    },

    profileUpdate: function (employeeId, mobile, email) {
        var data = {
            'event_type': 'profile_update',
            'employee_id': employeeId,
            'mobile': mobile,
            'email': email
        }
        this._send(data);
    },

    leadUpdate: function (employeeId, vehicle, status) {
        var event_type;
        if (status == 'add') {
            event_type = 'lead_add';
        } else if (status == 'delete') {
            event_type = 'lead_delete';
        } else if (status == 'update') {
            event_type = 'lead_update';
        }
        var data = {
            'event_type': event_type,
            'employee_id': employeeId,
            'lead_id': vehicle.id,
            'vehicle': vehicle.model,
            'vehicleVariant': vehicle.variant,
            'fuel_type': vehicle.fuelType,
            'pastPolicyExpiryDate': vehicle.pastPolicyExpiryDate,
            'pastPolicyUploadUrl': vehicle.upload,
        }

        this._send(data);
    }
}

var LMS = new LMSUtils();