File.prototype.convertToBase64 = function(callback) {
    var FR = new FileReader();
    FR.onload = function(e) {
        callback(e.target.result)
    };
    FR.readAsDataURL(this);
}

var ViewModel = function() {
    var self = this;
    self.upload_documents = ko.observableArray();
    self.current_uploads = ko.observableArray();
    self.payment_data = ko.observable({
        'cheque_image': ko.observable(initial_payment_data.cheque_image),
        'cheque_number': ko.observable(initial_payment_data.cheque_number),
        'bank_name': ko.observable(initial_payment_data.bank_name),
    });
    self.remarks = ko.observable('');
    self.create_upload_type = function(type, label) {
        try {
            uploaded_documents = existing_documents[type];
        } catch (e) {
            uploaded_documents = [];
        }
        return ko.observable({
            "type": ko.observable(type),
            "label": ko.observable(label),
            "documents": ko.observableArray(uploaded_documents),
        })
    }
    self.initialize_upload = function() {
        self.upload_documents.push(self.create_upload_type("rc-copy", "RC Copy"))
        if(is_used_vehicle){
            self.upload_documents.push(self.create_upload_type("form-29", "Form 29"))
            self.upload_documents.push(self.create_upload_type("form-30", "Form 30"))
        }
        else{
            self.upload_documents.push(self.create_upload_type("last-policy-copy", "Last Policy Copy"))
        }
        self.upload_documents.push(self.create_upload_type("engine-imprint", "Engine Imprint"))
        self.upload_documents.push(self.create_upload_type("chassis-imprint", "Chassis Imprint"))
        self.upload_documents.push(self.create_upload_type("front", "Front"))
        self.upload_documents.push(self.create_upload_type("engine", "Engine"))
        self.upload_documents.push(self.create_upload_type("dashboard", "Dashboard"))
        self.upload_documents.push(self.create_upload_type("rear", "Rear"))
        self.upload_documents.push(self.create_upload_type("lf-angle", "Left Front Angle"))
        self.upload_documents.push(self.create_upload_type("rf-angle", "Right Front Angle"))
        self.upload_documents.push(self.create_upload_type("lr-angle", "Left Rear Angle"))
        self.upload_documents.push(self.create_upload_type("rr-angle", "Right Rear Angle"))
        self.upload_documents.push(self.create_upload_type("odometer", "Odometer"))
        self.upload_documents.push(self.create_upload_type("floor", "Floor"))
        self.upload_documents.push(self.create_upload_type("extra-accessories", "Extra Accessories"))
        self.upload_documents.push(self.create_upload_type("bifuel-kit", "Bifuel Kit"))
        self.upload_documents.push(self.create_upload_type("inspection-report", "Inspection Report"))
    }
    self.handleUploadRequest = function(file, document_type, parent_data) {
        var xhr = new XMLHttpRequest();
        var fd = new FormData();
        fd.append('image', file);
        fd.append('document_type', document_type);
        fd.append('inspection_id', inspection_id);
        var current_upload_index = vm.current_uploads.push({
            "file_name": file.name,
            "progress": ko.observable(0),
        })
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
                response = JSON.parse(xhr.responseText);
                parent_data.push(response)
            }
        };
        xhr.upload.addEventListener('progress', function(evt) {
            if (evt.lengthComputable) {
                var progress = evt.loaded * 100 / evt.total;
                vm.current_uploads()[current_upload_index - 1]['progress'](progress);
            }
        }, false);

        fd.append("content", file);
        xhr.open("POST", '/api/v1/document/', true);
        xhr.setRequestHeader("Cache-Control", "no-cache");
        xhr.setRequestHeader("X-File-Name", file.name);
        xhr.setRequestHeader("X-File-Size", file.size);
        xhr.send(fd);
    }
    self.handleChequeUploadRequest = function(file, parent_data) {
        var xhr = new XMLHttpRequest();
        var fd = new FormData();
        fd.append('cheque_image', file);
        fd.append('uuid', payment_id);
        fd.append('is_ajax', 1);
        var current_upload_index = vm.current_uploads.push({
            "file_name": file.name,
            "progress": ko.observable(0),
        })
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
                response = JSON.parse(xhr.responseText);
                vm.payment_data().cheque_image(response['cheque_image']);
            }
        };
        xhr.upload.addEventListener('progress', function(evt) {
            if (evt.lengthComputable) {
                var progress = evt.loaded * 100 / evt.total;
                vm.current_uploads()[current_upload_index - 1]['progress'](progress);
            }
        }, false);

        fd.append("content", file);
        xhr.open("POST", '/motor/dealer/fourwheeler/payment/'+transaction_id+'/', true);
        xhr.setRequestHeader("Cache-Control", "no-cache");
        xhr.setRequestHeader("X-CSRFToken", csrf_token);
        xhr.setRequestHeader("X-File-Name", file.name);
        xhr.setRequestHeader("X-File-Size", file.size);
        xhr.send(fd);
    }
    self.send_for_approval = function() {
        $.ajax({
            method: "POST",
            beforeSend: function (request)
            {
                request.setRequestHeader("X-SKIP-CSRF", true);
            },
            url: "/api/v1/proposal/inspection/approval/"+inspection_id+'/',
            data: {
                remarks: vm.remarks(),
            }
        })
        .done(function(msg) {
            window.location.replace('/motor/dealer/pending-cases/')
            // Code to send the user to the next step
        });
    }
    self.initialize_upload();
}

var vm = new ViewModel();
$(document).ready(function() {
    ko.applyBindings(vm);
});
