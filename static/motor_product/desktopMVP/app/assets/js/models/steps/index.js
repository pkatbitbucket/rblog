
define(['knockout', 'models', 'viewmodels', 'utils', 'jquery', 'carphotos', 'rtocodes', 'crypt', 'rangeslider', 'components'], function(ko, models, viewmodels, utils, $, carphotos, rtocodes, crypt) {



    var RTO_codes = rtocodes.RTO_CODE_ARRAY;




    var quote = viewmodels.quote;
    //var userVehicle = viewmodels.vehicle;
    var mode = ko.observable(null);
    var make_list = ko.observableArray([]);
    var model_list = ko.observableArray([]);
    var variant_list = ko.observableArray([]);
    var isCarSelectorOpen = ko.observable(false);

    var setExpired = function(){
      quote.isNewVehicle(false);
      quote.policyExpired(true);

    };

    var setNew = function(){
      quote.isNewVehicle(true);
      quote.policyExpired(false);

    };

    var setOld = function(){
      quote.isNewVehicle(false);
      quote.policyExpired(false);

    };

    var resetNewVehicle = function(){
        quote.isNewVehicle(null);
        quote.policyExpired(false);
    };


    function getMakes(){
        $.post('/motor/fourwheeler/api/makes/', {'csrfmiddlewaretoken' : CSRF_TOKEN}, function(response){
            response = crypt.parseResponse(response);
            make_list(response.makes);
        });

    }

    function getModels(){
        quote.model(null);
        quote.variant(null);
        $.post('/motor/fourwheeler/api/makes/'+quote.make().id+'/models/', {'csrfmiddlewaretoken' : CSRF_TOKEN}, function(response){
            response = crypt.parseResponse(response);
            model_list(response.models);
        });

    }

    function getVariants(){
        quote.variant(null);
        $.post('/motor/fourwheeler/api/makes/'+quote.make().id+'/models/'+quote.model().id+'/variants/', {'csrfmiddlewaretoken' : CSRF_TOKEN}, function(response){
            response = crypt.parseResponse(response);
            variant_list(response.variants);
        });
    }

    function setVehicleId(){
        quote.vehicleId(quote.variant().id);
        console.log(quote.vehicleId());
    }



    var goToResults = function(){
        mode('results');
        setTimeout(function(){
            ko.router.navigate('results', {trigger:true});
        }, 600);

    };





    return {
        initialize : function(stage_type){
            console.log("MODE", mode());
            mode(null);
            getMakes();
            return {
                setNew:setNew,
                setExpired:setExpired,
                setOld:setOld,
                resetNewVehicle:resetNewVehicle,
                quote:quote,
                //userVehicle:userVehicle,
                RTO_codes:RTO_codes,
                months:viewmodels.months,
                years:viewmodels.years,
                goToResults:goToResults,
                mode:mode,
                make_list:make_list,
                model_list:model_list,
                variant_list:variant_list,
                getModels:getModels,
                getVariants:getVariants,
                setVehicleId:setVehicleId,
                isCarSelectorOpen:isCarSelectorOpen


            }
        }
    };
});
